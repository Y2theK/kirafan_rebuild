﻿using UnityEngine;
using UnityEngine.UI;

public class AppsFlyerTrackerCallbacks : MonoBehaviour {
    public Text callbacks;

    private void Start() => MonoBehaviour.print((object)"AppsFlyerTrackerCallbacks on Start");

    public void didReceiveConversionData(string conversionData) {
        if (conversionData == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
    }

    public void didReceiveConversionDataWithError(string error) {
        if (error == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
    }

    public void didFinishValidateReceipt(string validateResult) {
        if (validateResult == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
    }

    public void didFinishValidateReceiptWithError(string error) {
        if (error == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);
    }

    public void onAppOpenAttribution(string validateResult) {
        if (validateResult == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
    }

    public void onAppOpenAttributionFailure(string error) {
        if (error == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
    }

    public void onInAppBillingSuccess() => printCallback("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");

    public void onInAppBillingFailure(string error) {
        if (error == null)
            return;
        printCallback("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);
    }

    private void printCallback(string str) {
        if (callbacks == null || string.IsNullOrEmpty(str))
            return;
        callbacks.text = callbacks.text + str + "\n";
    }
}
