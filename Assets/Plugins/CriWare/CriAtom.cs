﻿using AOT;
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

[AddComponentMenu("CRIWARE/CRI Atom")]
public class CriAtom : MonoBehaviour {
    public string acfFile = string.Empty;
    private bool acfIsLoading;
    public CriAtomCueSheet[] cueSheets = new CriAtomCueSheet[0];
    public string dspBusSetting = string.Empty;
    public bool dontDestroyOnLoad;
    private static CriAtomExSequencer.EventCbFunc eventUserCbFunc;
    private static CriAtomExBeatSync.CbFunc beatsyncUserCbFunc;
    public bool dontRemoveExistsCueSheet;

    private static CriAtom instance { get; set; }

    public static void AttachDspBusSetting(string settingName) {
        instance.dspBusSetting = settingName;
        if (!string.IsNullOrEmpty(settingName))
            CriAtomEx.AttachDspBusSetting(settingName);
        else
            CriAtomEx.DetachDspBusSetting();
    }

    public static void DetachDspBusSetting() {
        instance.dspBusSetting = string.Empty;
        CriAtomEx.DetachDspBusSetting();
    }

    public static CriAtomCueSheet GetCueSheet(string name) => instance.GetCueSheetInternal(name);

    public static CriAtomCueSheet AddCueSheet(
      string name,
      string acbFile,
      string awbFile,
      CriFsBinder binder = null) {
        CriAtomCueSheet criAtomCueSheet = instance.AddCueSheetInternal(name, acbFile, awbFile, binder);
        if (Application.isPlaying)
            criAtomCueSheet.acb = instance.LoadAcbFile(binder, acbFile, awbFile);
        return criAtomCueSheet;
    }

    public static CriAtomCueSheet AddCueSheetAsync(
      string name,
      string acbFile,
      string awbFile,
      CriFsBinder binder = null,
      bool loadAwbOnMemory = false) {
        CriAtomCueSheet cueSheet = instance.AddCueSheetInternal(name, acbFile, awbFile, binder);
        if (Application.isPlaying)
            instance.LoadAcbFileAsync(cueSheet, binder, acbFile, awbFile, loadAwbOnMemory);
        return cueSheet;
    }

    public static CriAtomCueSheet AddCueSheet(
      string name,
      byte[] acbData,
      string awbFile,
      CriFsBinder awbBinder = null) {
        CriAtomCueSheet criAtomCueSheet = instance.AddCueSheetInternal(name, string.Empty, awbFile, awbBinder);
        if (Application.isPlaying)
            criAtomCueSheet.acb = instance.LoadAcbData(acbData, awbBinder, awbFile);
        return criAtomCueSheet;
    }

    public static CriAtomCueSheet AddCueSheetAsync(
      string name,
      byte[] acbData,
      string awbFile,
      CriFsBinder awbBinder = null,
      bool loadAwbOnMemory = false) {
        CriAtomCueSheet cueSheet = instance.AddCueSheetInternal(name, string.Empty, awbFile, awbBinder);
        if (Application.isPlaying)
            instance.LoadAcbDataAsync(cueSheet, acbData, awbBinder, awbFile, loadAwbOnMemory);
        return cueSheet;
    }

    public static void RemoveCueSheet(string name) {
        if (instance == null) {
            return;
        }
        instance.RemoveCueSheetInternal(name);
    }

    public static bool CueSheetsAreLoading {
        get {
            if (instance == null) {
                return false;
            }
            foreach (CriAtomCueSheet criAtomCueSheet in instance.cueSheets) {
                if (criAtomCueSheet.IsLoading) {
                    return true;
                }
            }
            return false;
        }
    }

    public static CriAtomExAcb GetAcb(string cueSheetName) {
        foreach (CriAtomCueSheet cueSheet in instance.cueSheets) {
            if (cueSheetName == cueSheet.name)
                return cueSheet.acb;
        }
        Debug.LogWarning(cueSheetName + " is not loaded.");
        return null;
    }

    public static void SetCategoryVolume(string name, float volume) => CriAtomExCategory.SetVolume(name, volume);

    public static void SetCategoryVolume(int id, float volume) => CriAtomExCategory.SetVolume(id, volume);

    public static float GetCategoryVolume(string name) => CriAtomExCategory.GetVolume(name);

    public static float GetCategoryVolume(int id) => CriAtomExCategory.GetVolume(id);

    public static void SetBusAnalyzer(bool sw) {
        if (sw)
            CriAtomExAsr.AttachBusAnalyzer(50, 1000);
        else
            CriAtomExAsr.DetachBusAnalyzer();
    }

    public static CriAtomExAsr.BusAnalyzerInfo GetBusAnalyzerInfo(int bus) {
        CriAtomExAsr.GetBusAnalyzerInfo(bus, out CriAtomExAsr.BusAnalyzerInfo info);
        return info;
    }

    private void Setup() {
        instance = this;
        CriAtomPlugin.InitializeLibrary();
        if (!string.IsNullOrEmpty(acfFile))
            CriAtomEx.RegisterAcf(null, Path.Combine(CriWare.streamingAssetsPath, acfFile));
        if (!string.IsNullOrEmpty(dspBusSetting))
            AttachDspBusSetting(dspBusSetting);
        foreach (CriAtomCueSheet cueSheet in cueSheets)
            cueSheet.acb = LoadAcbFile(null, cueSheet.acbFile, cueSheet.awbFile);
        if (!dontDestroyOnLoad)
            return;
        DontDestroyOnLoad(gameObject);
    }

    private void Shutdown() {
        foreach (CriAtomCueSheet cueSheet in cueSheets) {
            if (cueSheet.acb != null) {
                cueSheet.acb.Dispose();
                cueSheet.acb = null;
            }
        }
        CriAtomPlugin.FinalizeLibrary();
        instance = null;
    }

    private void Awake() {
        if (instance != null) {
            if (instance.acfFile != acfFile) {
                GameObject gameObject = instance.gameObject;
                instance.Shutdown();
                CriAtomEx.UnregisterAcf();
                Destroy(gameObject);
                return;
            }
            if (instance.dspBusSetting != dspBusSetting) {
                AttachDspBusSetting(dspBusSetting);
            }
            instance.MargeCueSheet(cueSheets, dontRemoveExistsCueSheet);
            Destroy(base.gameObject);
        }
    }

    private void OnEnable() {
        if (instance != null) {
            return;
        }
        Setup();
    }

    private void OnDestroy() {
        if (this != instance) {
            return;
        }
        Shutdown();
    }

    private void Update() {
        CriAtomPlugin.criAtomUnitySequencer_ExecuteQueuedEventCallbacks();
        CriAtomPlugin.criAtomUnity_ExecuteQueuedBeatSyncCallbacks();
    }

    public CriAtomCueSheet GetCueSheetInternal(string name) {
        for (int index = 0; index < cueSheets.Length; ++index) {
            CriAtomCueSheet cueSheet = cueSheets[index];
            if (cueSheet.name == name)
                return cueSheet;
        }
        return null;
    }

    public CriAtomCueSheet AddCueSheetInternal(
      string name,
      string acbFile,
      string awbFile,
      CriFsBinder binder) {
        CriAtomCueSheet[] criAtomCueSheetArray = new CriAtomCueSheet[cueSheets.Length + 1];
        cueSheets.CopyTo((Array)criAtomCueSheetArray, 0);
        cueSheets = criAtomCueSheetArray;
        CriAtomCueSheet criAtomCueSheet = new CriAtomCueSheet();
        cueSheets[cueSheets.Length - 1] = criAtomCueSheet;
        criAtomCueSheet.name = !string.IsNullOrEmpty(name) ? name : Path.GetFileNameWithoutExtension(acbFile);
        criAtomCueSheet.acbFile = acbFile;
        criAtomCueSheet.awbFile = awbFile;
        return criAtomCueSheet;
    }

    public void RemoveCueSheetInternal(string name) {
        int index1 = -1;
        for (int index2 = 0; index2 < cueSheets.Length; ++index2) {
            if (name == cueSheets[index2].name)
                index1 = index2;
        }
        if (index1 < 0)
            return;
        CriAtomCueSheet cueSheet = cueSheets[index1];
        if (cueSheet.acb != null) {
            cueSheet.acb.Dispose();
            cueSheet.acb = null;
        }
        CriAtomCueSheet[] destinationArray = new CriAtomCueSheet[cueSheets.Length - 1];
        Array.Copy(cueSheets, 0, destinationArray, 0, index1);
        Array.Copy(cueSheets, index1 + 1, destinationArray, index1, cueSheets.Length - index1 - 1);
        cueSheets = destinationArray;
    }

    private void MargeCueSheet(CriAtomCueSheet[] newCueSheets, bool newDontRemoveExistsCueSheet) {
        if (!newDontRemoveExistsCueSheet) {
            int i = 0;
            while (i < cueSheets.Length) {
                if (Array.FindIndex(newCueSheets, sheet => sheet.name == cueSheets[i].name) < 0)
                    RemoveCueSheet(cueSheets[i].name);
                else
                    ++i;
            }
        }
        foreach (CriAtomCueSheet newCueSheet in newCueSheets) {
            if (GetCueSheetInternal(newCueSheet.name) == null)
                AddCueSheet(null, newCueSheet.acbFile, newCueSheet.awbFile);
        }
    }

    private CriAtomExAcb LoadAcbFile(CriFsBinder binder, string acbFile, string awbFile) {
        if (string.IsNullOrEmpty(acbFile))
            return null;
        string str1 = acbFile;
        if (binder == null && CriWare.IsStreamingAssetsPath(str1))
            str1 = Path.Combine(CriWare.streamingAssetsPath, str1);
        string str2 = awbFile;
        if (!string.IsNullOrEmpty(str2) && binder == null && CriWare.IsStreamingAssetsPath(str2))
            str2 = Path.Combine(CriWare.streamingAssetsPath, str2);
        return CriAtomExAcb.LoadAcbFile(binder, str1, str2);
    }

    private CriAtomExAcb LoadAcbData(byte[] acbData, CriFsBinder binder, string awbFile) {
        if (acbData == null)
            return null;
        string str = awbFile;
        if (!string.IsNullOrEmpty(str) && binder == null && CriWare.IsStreamingAssetsPath(str))
            str = Path.Combine(CriWare.streamingAssetsPath, str);
        return CriAtomExAcb.LoadAcbData(acbData, binder, str);
    }

    private void LoadAcbFileAsync(
      CriAtomCueSheet cueSheet,
      CriFsBinder binder,
      string acbFile,
      string awbFile,
      bool loadAwbOnMemory) {
        if (string.IsNullOrEmpty(acbFile))
            return;
        StartCoroutine(LoadAcbFileCoroutine(cueSheet, binder, acbFile, awbFile, loadAwbOnMemory));
    }

    private IEnumerator LoadAcbFileCoroutine(
      CriAtomCueSheet cueSheet,
      CriFsBinder binder,
      string acbPath,
      string awbPath,
      bool loadAwbOnMemory) {
        cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Loading;
        if (binder == null && CriWare.IsStreamingAssetsPath(acbPath)) {
            acbPath = Path.Combine(CriWare.streamingAssetsPath, acbPath);
        }
        if (!string.IsNullOrEmpty(awbPath) && binder == null && CriWare.IsStreamingAssetsPath(awbPath)) {
            awbPath = Path.Combine(CriWare.streamingAssetsPath, awbPath);
        }
        while (acfIsLoading) {
            yield return new WaitForEndOfFrame();
        }
        using (CriAtomExAcbLoader asyncLoader = CriAtomExAcbLoader.LoadAcbFileAsync(binder, acbPath, awbPath, loadAwbOnMemory)) {
            if (asyncLoader == null) {
                cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Error;
                yield break;
            }
            CriAtomExAcbLoader.Status status;
            while (true) {
                status = asyncLoader.GetStatus();
                cueSheet.loaderStatus = status;
                if (status == CriAtomExAcbLoader.Status.Complete || status == CriAtomExAcbLoader.Status.Error) {
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            if (status != CriAtomExAcbLoader.Status.Error) {
                cueSheet.acb = asyncLoader.MoveAcb();
            }
        }
        yield break;
    }

    private void LoadAcbDataAsync(
      CriAtomCueSheet cueSheet,
      byte[] acbData,
      CriFsBinder awbBinder,
      string awbFile,
      bool loadAwbOnMemory) {
        StartCoroutine(LoadAcbDataCoroutine(cueSheet, acbData, awbBinder, awbFile, loadAwbOnMemory));
    }

    private IEnumerator LoadAcbDataCoroutine(
      CriAtomCueSheet cueSheet,
      byte[] acbData,
      CriFsBinder awbBinder,
      string awbPath,
      bool loadAwbOnMemory) {
        cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Loading;
        if (!string.IsNullOrEmpty(awbPath) && awbBinder == null && CriWare.IsStreamingAssetsPath(awbPath)) {
            awbPath = Path.Combine(CriWare.streamingAssetsPath, awbPath);
        }
        while (acfIsLoading) {
            yield return new WaitForEndOfFrame();
        }
        using (CriAtomExAcbLoader asyncLoader = CriAtomExAcbLoader.LoadAcbDataAsync(acbData, awbBinder, awbPath, loadAwbOnMemory)) {
            if (asyncLoader == null) {
                cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Error;
                yield break;
            }
            CriAtomExAcbLoader.Status status;
            while (true) {
                status = asyncLoader.GetStatus();
                cueSheet.loaderStatus = status;
                if (status == CriAtomExAcbLoader.Status.Complete || status == CriAtomExAcbLoader.Status.Error) {
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            if (status != CriAtomExAcbLoader.Status.Error) {
                cueSheet.acb = asyncLoader.MoveAcb();
            }
        }
        yield break;
    }

    [MonoPInvokeCallback(typeof(CriAtomExSequencer.EventCbFunc))]
    public static void SequenceEventCallbackFromNative(string eventString) {
        if (eventUserCbFunc == null)
            return;
        eventUserCbFunc(eventString);
    }

    [MonoPInvokeCallback(typeof(CriAtomExBeatSync.CbFunc))]
    public static void BeatSyncCallbackFromNative(ref CriAtomExBeatSync.Info info) {
        if (beatsyncUserCbFunc == null)
            return;
        beatsyncUserCbFunc(ref info);
    }

    public static void SetEventCallback(CriAtomExSequencer.EventCbFunc func, string separator) {
        IntPtr cbfunc = IntPtr.Zero;
        eventUserCbFunc = func;
        if (func != null)
            cbfunc = Marshal.GetFunctionPointerForDelegate((Delegate)new CriAtomExSequencer.EventCbFunc(SequenceEventCallbackFromNative));
        CriAtomPlugin.criAtomUnitySequencer_SetEventCallback(cbfunc, separator);
    }

    public static void SetBeatSyncCallback(CriAtomExBeatSync.CbFunc func) {
        IntPtr cbfunc = IntPtr.Zero;
        beatsyncUserCbFunc = func;
        if (func != null)
            cbfunc = Marshal.GetFunctionPointerForDelegate((Delegate)new CriAtomExBeatSync.CbFunc(BeatSyncCallbackFromNative));
        CriAtomPlugin.criAtomUnity_SetBeatSyncCallback(cbfunc);
    }
}
