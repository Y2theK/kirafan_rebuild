﻿using System;

[Serializable]
public class CriAtomConfig {
    public string acfFileName = string.Empty;
    public int maxVirtualVoices = 32;
    public int maxVoiceLimitGroups = 32;
    public int maxCategories = 32;
    public int maxSequenceEventsPerFrame = 2;
    public int maxBeatSyncCallbacksPerFrame = 1;
    public CriAtomConfig.StandardVoicePoolConfig standardVoicePoolConfig = new CriAtomConfig.StandardVoicePoolConfig();
    public CriAtomConfig.HcaMxVoicePoolConfig hcaMxVoicePoolConfig = new CriAtomConfig.HcaMxVoicePoolConfig();
    public int outputSamplingRate;
    public bool usesInGamePreview;
    public float serverFrequency = 60f;
    public int asrOutputChannels;
    public bool useRandomSeedWithTime;
    public int categoriesPerPlayback = 4;
    public int maxBuses = 8;
    public int maxParameterBlocks = 1024;
    public bool vrMode;
    public int pcBufferingTime;
    public int iosBufferingTime = 50;
    public bool iosOverrideIPodMusic;
    public int androidBufferingTime = 133;
    public int androidStartBufferingTime = 100;
    public CriAtomConfig.AndroidLowLatencyStandardVoicePoolConfig androidLowLatencyStandardVoicePoolConfig = new CriAtomConfig.AndroidLowLatencyStandardVoicePoolConfig();
    public bool androidUsesAndroidFastMixer = true;
    public CriAtomConfig.VitaAtrac9VoicePoolConfig vitaAtrac9VoicePoolConfig = new CriAtomConfig.VitaAtrac9VoicePoolConfig();
    public CriAtomConfig.Ps4Atrac9VoicePoolConfig ps4Atrac9VoicePoolConfig = new CriAtomConfig.Ps4Atrac9VoicePoolConfig();
    public CriAtomConfig.Ps4Audio3dConfig ps4Audio3dConfig = new CriAtomConfig.Ps4Audio3dConfig();
    public CriAtomConfig.WebGLWebAudioVoicePoolConfig webglWebAudioVoicePoolConfig = new CriAtomConfig.WebGLWebAudioVoicePoolConfig();

    [Serializable]
    public class StandardVoicePoolConfig {
        public int memoryVoices = 16;
        public int streamingVoices = 8;
    }

    [Serializable]
    public class HcaMxVoicePoolConfig {
        public int memoryVoices;
        public int streamingVoices;
    }

    [Serializable]
    public class AndroidLowLatencyStandardVoicePoolConfig {
        public int memoryVoices;
        public int streamingVoices;
    }

    [Serializable]
    public class VitaAtrac9VoicePoolConfig {
        public int memoryVoices;
        public int streamingVoices;
    }

    [Serializable]
    public class Ps4Atrac9VoicePoolConfig {
        public int memoryVoices;
        public int streamingVoices;
    }

    [Serializable]
    public class Ps4Audio3dConfig {
        public bool useAudio3D;
        public CriAtomConfig.Ps4Audio3dConfig.VoicePoolConfig voicePoolConfig = new CriAtomConfig.Ps4Audio3dConfig.VoicePoolConfig();

        [Serializable]
        public class VoicePoolConfig {
            public int memoryVoices;
            public int streamingVoices;
        }
    }

    [Serializable]
    public class WebGLWebAudioVoicePoolConfig {
        public int voices = 16;
    }
}
