﻿using System;

[Serializable]
public class CriAtomCueSheet {
    public string name = string.Empty;
    public string acbFile = string.Empty;
    public string awbFile = string.Empty;
    public CriAtomExAcb acb;
    public CriAtomExAcbLoader.Status loaderStatus;

    public bool IsLoading => loaderStatus == CriAtomExAcbLoader.Status.Loading;

    public bool IsError {
        get {
            if (loaderStatus == CriAtomExAcbLoader.Status.Error)
                return true;
            return !IsLoading && acb == null;
        }
    }
}
