﻿using System;
using System.Runtime.InteropServices;

namespace CriAtomDebugDetail {
    public class Utility {
        public static string PtrToStringAutoOrNull(IntPtr stringPtr) => stringPtr == IntPtr.Zero ? (string)null : Marshal.PtrToStringAuto(stringPtr);
    }
}
