﻿using System;
using System.Runtime.InteropServices;

public static class CriAtomEx {
    public const uint InvalidAisacControlId = 65535;

    public static void RegisterAcf(CriFsBinder binder, string acfPath) => CriAtomEx.criAtomEx_RegisterAcfFile(binder == null ? IntPtr.Zero : binder.nativeHandle, acfPath, IntPtr.Zero, 0);

    public static void RegisterAcf(byte[] acfData) => CriAtomEx.criAtomEx_RegisterAcfData(acfData, acfData.Length, IntPtr.Zero, 0);

    public static void UnregisterAcf() => CriAtomEx.criAtomEx_UnregisterAcf();

    public static void AttachDspBusSetting(string settingName) => CriAtomEx.criAtomEx_AttachDspBusSetting(settingName, IntPtr.Zero, 0);

    public static void DetachDspBusSetting() => CriAtomEx.criAtomEx_DetachDspBusSetting();

    public static void ApplyDspBusSnapshot(string snapshot_name, int time_ms) => CriAtomEx.criAtomEx_ApplyDspBusSnapshot(snapshot_name, time_ms);

    public static int GetNumGameVariables() => CriAtomEx.criAtomEx_GetNumGameVariables();

    public static bool GetGameVariableInfo(ushort index, out CriAtomEx.GameVariableInfo info) {
        using (CriStructMemory<CriAtomEx.GameVariableInfo> criStructMemory = new CriStructMemory<CriAtomEx.GameVariableInfo>()) {
            bool gameVariableInfo = CriAtomEx.criAtomEx_GetGameVariableInfo(index, criStructMemory.ptr);
            info = new CriAtomEx.GameVariableInfo(criStructMemory.bytes, 0);
            return gameVariableInfo;
        }
    }

    public static float GetGameVariable(uint game_variable_id) => CriAtomEx.criAtomEx_GetGameVariableById(game_variable_id);

    public static float GetGameVariable(string game_variable_name) => CriAtomEx.criAtomEx_GetGameVariableByName(game_variable_name);

    public static void SetGameVariable(uint game_variable_id, float game_variable_value) => CriAtomEx.criAtomEx_SetGameVariableById(game_variable_id, game_variable_value);

    public static void SetGameVariable(string game_variable_name, float game_variable_value) => CriAtomEx.criAtomEx_SetGameVariableByName(game_variable_name, game_variable_value);

    public static void SetRandomSeed(uint seed) => CriAtomEx.criAtomEx_SetRandomSeed(seed);

    public static void ResetPerformanceMonitor() => CriAtomEx.criAtom_ResetPerformanceMonitor();

    public static void GetPerformanceInfo(out CriAtomEx.PerformanceInfo info) => CriAtomEx.criAtom_GetPerformanceInfo(out info);

    public static void SetOutputVolume_VITA(float volume) {
    }

    public static bool IsBgmPortAcquired_VITA() => true;

    public static bool IsSoundStopped_IOS() => false;

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomEx_RegisterAcfFile(
      IntPtr binder,
      string path,
      IntPtr work,
      int workSize);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomEx_RegisterAcfData(
      byte[] acfData,
      int acfDataSize,
      IntPtr work,
      int workSize);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_UnregisterAcf();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_AttachDspBusSetting(
      string settingName,
      IntPtr work,
      int workSize);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_DetachDspBusSetting();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_ApplyDspBusSnapshot(string snapshot_name, int time_ms);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomEx_GetNumGameVariables();

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomEx_GetGameVariableInfo(ushort index, IntPtr game_variable_info);

    [DllImport("cri_ware_unity")]
    private static extern float criAtomEx_GetGameVariableById(uint game_variable_id);

    [DllImport("cri_ware_unity")]
    private static extern float criAtomEx_GetGameVariableByName(string game_variable_name);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_SetGameVariableById(
      uint game_variable_id,
      float game_variable_value);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_SetGameVariableByName(
      string game_variable_name,
      float game_variable_value);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx_SetRandomSeed(uint seed);

    [DllImport("cri_ware_unity")]
    private static extern void criAtom_ResetPerformanceMonitor();

    [DllImport("cri_ware_unity")]
    private static extern void criAtom_GetPerformanceInfo(out CriAtomEx.PerformanceInfo info);

    [DllImport("cri_ware_unity")]
    public static extern void criAtom_EnableSlLatencyCheck_ANDROID(bool sw);

    [DllImport("cri_ware_unity")]
    public static extern int criAtom_GetSlBufferConsumptionLatency_ANDROID();

    public enum CharacterEncoding {
        Utf8,
        Sjis,
    }

    public enum SoundRendererType {
        Default = 0,
        Hw1 = 1,
        Native = 1,
        Asr = 2,
        Hw2 = 9,
    }

    public enum VoiceAllocationMethod {
        Once,
        Retry,
    }

    public enum BiquadFilterType {
        Off,
        LowPass,
        HighPass,
        Notch,
        LowShelf,
        HighShelf,
        Peaking,
    }

    public enum ResumeMode {
        AllPlayback,
        PausedPlayback,
        PreparedPlayback,
    }

    public enum PanType {
        Pan3d,
        Pos3d,
        Auto,
    }

    public enum VoiceControlMethod {
        PreferLast,
        PreferFirst,
    }

    public enum Parameter {
        Volume = 0,
        Pitch = 1,
        Pan3dAngle = 2,
        Pan3dDistance = 3,
        Pan3dVolume = 4,
        BusSendLevel0 = 9,
        BusSendLevel1 = 10, // 0x0000000A
        BusSendLevel2 = 11, // 0x0000000B
        BusSendLevel3 = 12, // 0x0000000C
        BusSendLevel4 = 13, // 0x0000000D
        BusSendLevel5 = 14, // 0x0000000E
        BusSendLevel6 = 15, // 0x0000000F
        BusSendLevel7 = 16, // 0x00000010
        BandPassFilterCofLow = 17, // 0x00000011
        BandPassFilterCofHigh = 18, // 0x00000012
        BiquadFilterType = 19, // 0x00000013
        BiquadFilterFreq = 20, // 0x00000014
        BiquadFIlterQ = 21, // 0x00000015
        BiquadFilterGain = 22, // 0x00000016
        EnvelopeAttackTime = 23, // 0x00000017
        EnvelopeHoldTime = 24, // 0x00000018
        EnvelopeDecayTime = 25, // 0x00000019
        EnvelopeReleaseTime = 26, // 0x0000001A
        EnvelopeSustainLevel = 27, // 0x0000001B
        StartTime = 28, // 0x0000001C
        Priority = 31, // 0x0000001F
    }

    public enum Speaker {
        FrontLeft,
        FrontRight,
        FrontCenter,
        LowFrequency,
        SurroundLeft,
        SurroundRight,
        SurroundBackLeft,
        SurroundBackRight,
    }

    public enum Format : uint {
        ADX = 1,
        HCA = 3,
        HCA_MX = 4,
        WAVE = 5,
    }

    public struct FormatInfo {
        public CriAtomEx.Format format;
        public int samplingRate;
        public long numSamples;
        public long loopOffset;
        public long loopLength;
        public int numChannels;
        public uint reserved;
    }

    public struct AisacControlInfo {
        [MarshalAs(UnmanagedType.LPStr)]
        public readonly string name;
        public uint id;

        public AisacControlInfo(byte[] data, int startIndex) {
            if (IntPtr.Size == 4) {
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex)));
                id = BitConverter.ToUInt32(data, startIndex + 4);
            } else {
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex)));
                id = BitConverter.ToUInt32(data, startIndex + 8);
            }
        }
    }

    public struct CuePos3dInfo {
        public float coneInsideAngle;
        public float coneOutsideAngle;
        public float minAttenuationDistance;
        public float maxAttenuationDistance;
        public float sourceRadius;
        public float interiorDistance;
        public float dopplerFactor;
        public ushort distanceAisacControl;
        public ushort listenerBaseAngleAisacControl;
        public ushort sourceBaseAngleAisacControl;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
        public ushort[] reserved;

        public CuePos3dInfo(byte[] data, int startIndex) {
            coneInsideAngle = BitConverter.ToSingle(data, startIndex);
            coneOutsideAngle = BitConverter.ToSingle(data, startIndex + 4);
            minAttenuationDistance = BitConverter.ToSingle(data, startIndex + 8);
            maxAttenuationDistance = BitConverter.ToSingle(data, startIndex + 12);
            sourceRadius = BitConverter.ToSingle(data, startIndex + 16);
            interiorDistance = BitConverter.ToSingle(data, startIndex + 20);
            dopplerFactor = BitConverter.ToSingle(data, startIndex + 24);
            distanceAisacControl = BitConverter.ToUInt16(data, startIndex + 28);
            listenerBaseAngleAisacControl = BitConverter.ToUInt16(data, startIndex + 30);
            sourceBaseAngleAisacControl = BitConverter.ToUInt16(data, startIndex + 32);
            reserved = new ushort[1];
            for (int index = 0; index < 1; ++index)
                reserved[index] = BitConverter.ToUInt16(data, startIndex + 34 + 2 * index);
        }
    }

    public struct GameVariableInfo {
        [MarshalAs(UnmanagedType.LPStr)]
        public readonly string name;
        public uint id;
        public float gameValue;

        public GameVariableInfo(byte[] data, int startIndex) {
            if (IntPtr.Size == 4) {
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex)));
                id = BitConverter.ToUInt32(data, startIndex + 4);
                gameValue = BitConverter.ToSingle(data, startIndex + 8);
            } else {
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex)));
                id = BitConverter.ToUInt32(data, startIndex + 8);
                gameValue = BitConverter.ToSingle(data, startIndex + 12);
            }
        }

        public GameVariableInfo(string name, uint id, float gameValue) {
            this.name = name;
            this.id = id;
            this.gameValue = gameValue;
        }
    }

    public enum CueType {
        Polyphonic,
        Sequential,
        Shuffle,
        Random,
        RandomNoRepeat,
        SwitchGameVariable,
        ComboSequential,
        SwitchSelector,
        TrackTransitionBySelector,
    }

    public struct CueInfo {
        public int id;
        public CriAtomEx.CueType type;
        [MarshalAs(UnmanagedType.LPStr)]
        public readonly string name;
        [MarshalAs(UnmanagedType.LPStr)]
        public readonly string userData;
        public long length;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public ushort[] categories;
        public short numLimits;
        public ushort numBlocks;
        public ushort numTracks;
        public ushort[] reserved;
        public byte priority;
        public byte headerVisibility;
        public byte ignore_player_parameter;
        public byte probability;
        public CriAtomEx.CuePos3dInfo pos3dInfo;
        public CriAtomEx.GameVariableInfo gameVariableInfo;

        public CueInfo(byte[] data, int startIndex) {
            if (IntPtr.Size == 4) {
                id = BitConverter.ToInt32(data, startIndex);
                type = (CriAtomEx.CueType)BitConverter.ToInt32(data, startIndex + 4);
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex + 8)));
                userData = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex + 12)));
                length = BitConverter.ToInt64(data, startIndex + 16);
                categories = new ushort[16];
                for (int index = 0; index < 16; ++index)
                    categories[index] = BitConverter.ToUInt16(data, startIndex + 24 + 2 * index);
                numLimits = BitConverter.ToInt16(data, startIndex + 56);
                numBlocks = BitConverter.ToUInt16(data, startIndex + 58);
                numTracks = BitConverter.ToUInt16(data, startIndex + 60);
                reserved = new ushort[1];
                for (int index = 0; index < 1; ++index)
                    reserved[index] = BitConverter.ToUInt16(data, startIndex + 62 + 2 * index);
                priority = data[startIndex + 64];
                headerVisibility = data[startIndex + 65];
                ignore_player_parameter = data[startIndex + 66];
                probability = data[startIndex + 67];
                pos3dInfo = new CriAtomEx.CuePos3dInfo(data, startIndex + 68);
                gameVariableInfo = new CriAtomEx.GameVariableInfo(data, startIndex + 104);
            } else {
                id = BitConverter.ToInt32(data, startIndex);
                type = (CriAtomEx.CueType)BitConverter.ToInt32(data, startIndex + 4);
                name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex + 8)));
                userData = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex + 16)));
                length = BitConverter.ToInt64(data, startIndex + 24);
                categories = new ushort[16];
                for (int index = 0; index < 16; ++index)
                    categories[index] = BitConverter.ToUInt16(data, startIndex + 32 + 2 * index);
                numLimits = BitConverter.ToInt16(data, startIndex + 64);
                numBlocks = BitConverter.ToUInt16(data, startIndex + 66);
                numTracks = BitConverter.ToUInt16(data, startIndex + 68);
                reserved = new ushort[1];
                for (int index = 0; index < 1; ++index)
                    reserved[index] = BitConverter.ToUInt16(data, startIndex + 70 + 2 * index);
                priority = data[startIndex + 72];
                headerVisibility = data[startIndex + 73];
                ignore_player_parameter = data[startIndex + 74];
                probability = data[startIndex + 75];
                pos3dInfo = new CriAtomEx.CuePos3dInfo(data, startIndex + 76);
                gameVariableInfo = new CriAtomEx.GameVariableInfo(data, startIndex + 112);
            }
        }
    }

    public struct WaveformInfo {
        public int waveId;
        public uint format;
        public int samplingRate;
        public int numChannels;
        public long numSamples;
        public bool streamingFlag;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
        public uint[] reserved;

        public WaveformInfo(byte[] data, int startIndex) {
            waveId = BitConverter.ToInt32(data, startIndex);
            format = BitConverter.ToUInt32(data, startIndex + 4);
            samplingRate = BitConverter.ToInt32(data, startIndex + 8);
            numChannels = BitConverter.ToInt32(data, startIndex + 12);
            numSamples = BitConverter.ToInt64(data, startIndex + 16);
            streamingFlag = BitConverter.ToInt32(data, startIndex + 24) != 0;
            reserved = new uint[1];
            for (int index = 0; index < 1; ++index)
                reserved[index] = BitConverter.ToUInt32(data, startIndex + 28 + 4 * index);
        }
    }

    public struct PerformanceInfo {
        public uint serverProcessCount;
        public uint lastServerTime;
        public uint maxServerTime;
        public uint averageServerTime;
        public uint lastServerInterval;
        public uint maxServerInterval;
        public uint averageServerInterval;
    }

    public struct ResourceUsage {
        public uint useCount;
        public uint limit;
    }
}
