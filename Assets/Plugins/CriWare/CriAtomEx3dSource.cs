﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class CriAtomEx3dSource : IDisposable {
    private IntPtr handle = IntPtr.Zero;

    public CriAtomEx3dSource() {
        CriAtomEx3dSource.Config config = new CriAtomEx3dSource.Config();
        handle = CriAtomEx3dSource.criAtomEx3dSource_Create(ref config, IntPtr.Zero, 0);
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    public void Dispose() => Dispose(true);

    private void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            CriAtomEx3dSource.criAtomEx3dSource_Destroy(handle);
            handle = IntPtr.Zero;
        }
        if (!disposing)
            return;
        GC.SuppressFinalize((object)this);
    }

    public IntPtr nativeHandle => handle;

    public void Update() => CriAtomEx3dSource.criAtomEx3dSource_Update(handle);

    public void ResetParameters() => CriAtomEx3dSource.criAtomEx3dSource_ResetParameters(handle);

    public void SetPosition(float x, float y, float z) {
        CriAtomEx3dSource.CriAtomExVector position;
        position.x = x;
        position.y = y;
        position.z = z;
        CriAtomEx3dSource.criAtomEx3dSource_SetPosition(handle, ref position);
    }

    public void SetVelocity(float x, float y, float z) {
        CriAtomEx3dSource.CriAtomExVector velocity;
        velocity.x = x;
        velocity.y = y;
        velocity.z = z;
        CriAtomEx3dSource.criAtomEx3dSource_SetVelocity(handle, ref velocity);
    }

    public void SetOrientation(Vector3 front, Vector3 top) {
        CriAtomEx3dSource.CriAtomExVector front1;
        front1.x = front.x;
        front1.y = front.y;
        front1.z = front.z;
        CriAtomEx3dSource.CriAtomExVector top1;
        top1.x = top.x;
        top1.y = top.y;
        top1.z = top.z;
        CriAtomEx3dSource.criAtomEx3dSource_SetOrientation(handle, ref front1, ref top1);
    }

    public void SetConeOrientation(float x, float y, float z) {
        CriAtomEx3dSource.CriAtomExVector cone_orient;
        cone_orient.x = x;
        cone_orient.y = y;
        cone_orient.z = z;
        CriAtomEx3dSource.criAtomEx3dSource_SetConeOrientation(handle, ref cone_orient);
    }

    public void SetConeParameter(float insideAngle, float outsideAngle, float outsideVolume) => CriAtomEx3dSource.criAtomEx3dSource_SetConeParameter(handle, insideAngle, outsideAngle, outsideVolume);

    public void SetMinMaxDistance(float minDistance, float maxDistance) => CriAtomEx3dSource.criAtomEx3dSource_SetMinMaxAttenuationDistance(handle, minDistance, maxDistance);

    public void SetDopplerFactor(float dopplerFactor) => CriAtomEx3dSource.criAtomEx3dSource_SetDopplerFactor(handle, dopplerFactor);

    public void SetVolume(float volume) => CriAtomEx3dSource.criAtomEx3dSource_SetVolume(handle, volume);

    public void SetMaxAngleAisacDelta(float maxDelta) => CriAtomEx3dSource.criAtomEx3dSource_SetMaxAngleAisacDelta(handle, maxDelta);

    ~CriAtomEx3dSource() => Dispose(false);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomEx3dSource_Create(
      ref CriAtomEx3dSource.Config config,
      IntPtr work,
      int work_size);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_Destroy(IntPtr ex_3d_source);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_Update(IntPtr ex_3d_source);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_ResetParameters(IntPtr ex_3d_source);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetPosition(
      IntPtr ex_3d_source,
      ref CriAtomEx3dSource.CriAtomExVector position);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetVelocity(
      IntPtr ex_3d_source,
      ref CriAtomEx3dSource.CriAtomExVector velocity);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetOrientation(
      IntPtr ex_3d_source,
      ref CriAtomEx3dSource.CriAtomExVector front,
      ref CriAtomEx3dSource.CriAtomExVector top);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetConeOrientation(
      IntPtr ex_3d_source,
      ref CriAtomEx3dSource.CriAtomExVector cone_orient);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetConeParameter(
      IntPtr ex_3d_source,
      float inside_angle,
      float outside_angle,
      float outside_volume);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetMinMaxAttenuationDistance(
      IntPtr ex_3d_source,
      float min_distance,
      float max_distance);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetDopplerFactor(
      IntPtr ex_3d_source,
      float doppler_factor);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetVolume(IntPtr ex_3d_source, float volume);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomEx3dSource_SetMaxAngleAisacDelta(
      IntPtr ex_3d_source,
      float max_delta);

    public struct Config {
        public int reserved;
    }

    private struct CriAtomExVector {
        public float x;
        public float y;
        public float z;
    }
}
