﻿using CriAtomDebugDetail;
using System;
using System.Runtime.InteropServices;

public static class CriAtomExAcbDebug {
    public static bool GetAcbInfo(CriAtomExAcb acb, out CriAtomExAcbDebug.AcbInfo acbInfo) {
        CriAtomExAcbDebug.AcbInfoForMarshaling acbInfo1;
        bool acbInfo2 = CriAtomExAcbDebug.criAtomExAcb_GetAcbInfo(acb.nativeHandle, out acbInfo1) == 1;
        acbInfo1.Convert(out acbInfo);
        return acbInfo2;
    }

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcb_GetAcbInfo(
      IntPtr acbHn,
      out CriAtomExAcbDebug.AcbInfoForMarshaling acbInfo);

    public struct AcbInfo {
        public string name;
        public uint size;
        public uint version;
        public CriAtomEx.CharacterEncoding characterEncoding;
        public float volume;
        public int numCues;
    }

    private struct AcbInfoForMarshaling {
        public IntPtr namePtr;
        public uint size;
        public uint version;
        public CriAtomEx.CharacterEncoding characterEncoding;
        public float volume;
        public int numCues;

        public void Convert(out CriAtomExAcbDebug.AcbInfo x) {
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.size = size;
            x.version = version;
            x.characterEncoding = characterEncoding;
            x.volume = volume;
            x.numCues = numCues;
        }
    }
}
