﻿using System;
using System.Runtime.InteropServices;

public class CriAtomExAcbLoader : IDisposable {
    private IntPtr handle;

    private CriAtomExAcbLoader(IntPtr handle) {
        this.handle = handle;
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    public static CriAtomExAcbLoader LoadAcbFileAsync(
      CriFsBinder binder,
      string acbPath,
      string awbPath,
      bool loadAwbOnMemory = false) {
        IntPtr num1 = binder == null ? IntPtr.Zero : binder.nativeHandle;
        CriAtomExAcbLoader.LoaderConfig loaderConfig = default(CriAtomExAcbLoader.LoaderConfig);
        loaderConfig.shouldLoadAwbOnMemory = loadAwbOnMemory;
        IntPtr num2 = CriAtomExAcbLoader.criAtomExAcbLoader_Create(ref loaderConfig);
        if (num2 == IntPtr.Zero)
            return (CriAtomExAcbLoader)null;
        return !CriAtomExAcbLoader.criAtomExAcbLoader_LoadAcbFileAsync(num2, num1, acbPath, num1, awbPath) ? (CriAtomExAcbLoader)null : new CriAtomExAcbLoader(num2);
    }

    public static CriAtomExAcbLoader LoadAcbDataAsync(
      byte[] acbData,
      CriFsBinder awbBinder,
      string awbPath,
      bool loadAwbOnMemory = false) {
        IntPtr awb_binder = awbBinder == null ? IntPtr.Zero : awbBinder.nativeHandle;
        CriAtomExAcbLoader.LoaderConfig loaderConfig = default(CriAtomExAcbLoader.LoaderConfig);
        loaderConfig.shouldLoadAwbOnMemory = loadAwbOnMemory;
        IntPtr num = CriAtomExAcbLoader.criAtomExAcbLoader_Create(ref loaderConfig);
        if (num == IntPtr.Zero)
            return (CriAtomExAcbLoader)null;
        return !CriAtomExAcbLoader.criAtomExAcbLoader_LoadAcbDataAsync(num, acbData, acbData.Length, awb_binder, awbPath) ? (CriAtomExAcbLoader)null : new CriAtomExAcbLoader(num);
    }

    public CriAtomExAcbLoader.Status GetStatus() => CriAtomExAcbLoader.criAtomExAcbLoader_GetStatus(handle);

    public CriAtomExAcb MoveAcb() {
        IntPtr handle = CriAtomExAcbLoader.criAtomExAcbLoader_MoveAcbHandle(this.handle);
        return handle != IntPtr.Zero ? new CriAtomExAcb(handle, (byte[])null) : (CriAtomExAcb)null;
    }

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            CriAtomExAcbLoader.criAtomExAcbLoader_Destroy(handle);
            handle = IntPtr.Zero;
        }
        GC.SuppressFinalize((object)this);
    }

    ~CriAtomExAcbLoader() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExAcbLoader_Create([In] ref CriAtomExAcbLoader.LoaderConfig config);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAcbLoader_Destroy(IntPtr acb_loader);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExAcbLoader_LoadAcbFileAsync(
      IntPtr acb_loader,
      IntPtr acb_binder,
      string acb_path,
      IntPtr awb_binder,
      string awb_path);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExAcbLoader_LoadAcbDataAsync(
      IntPtr acb_loader,
      byte[] acb_data,
      int acb_size,
      IntPtr awb_binder,
      string awb_path);

    [DllImport("cri_ware_unity")]
    private static extern CriAtomExAcbLoader.Status criAtomExAcbLoader_GetStatus(IntPtr acb_loader);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExAcbLoader_WaitForCompletion(IntPtr acb_loader);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExAcbLoader_MoveAcbHandle(IntPtr acb_loader);

    public enum Status {
        Stop,
        Loading,
        Complete,
        Error,
    }

    private struct LoaderConfig {
        public bool shouldLoadAwbOnMemory;
    }
}
