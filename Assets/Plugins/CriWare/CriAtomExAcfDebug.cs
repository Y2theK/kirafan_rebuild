﻿using CriAtomDebugDetail;
using System;
using System.Runtime.InteropServices;

public static class CriAtomExAcfDebug {
    public static int GetNumCategories() => CriAtomExAcfDebug.criAtomExAcf_GetNumCategories();

    public static bool GetCategoryInfoByIndex(
      ushort index,
      out CriAtomExAcfDebug.CategoryInfo categoryInfo) {
        CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo1;
        bool categoryInfoByIndex = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfo(index, out categoryInfo1) != 0;
        categoryInfo1.Convert(out categoryInfo);
        return categoryInfoByIndex;
    }

    public static bool GetCategoryInfoByName(
      string name,
      out CriAtomExAcfDebug.CategoryInfo categoryInfo) {
        CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo1;
        bool categoryInfoByName = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfoByName(name, out categoryInfo1) != 0;
        categoryInfo1.Convert(out categoryInfo);
        return categoryInfoByName;
    }

    public static bool GetCategoryInfoById(uint id, out CriAtomExAcfDebug.CategoryInfo categoryInfo) {
        CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo1;
        bool categoryInfoById = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfoById(id, out categoryInfo1) != 0;
        categoryInfo1.Convert(out categoryInfo);
        return categoryInfoById;
    }

    public static bool GetDspBusInformation(ushort index, out CriAtomExAcfDebug.DspBusInfo dspBusInfo) {
        CriAtomExAcfDebug.DspBusInfoForMarshaling dspBusInfo1;
        bool dspBusInformation = CriAtomExAcfDebug.criAtomExAcf_GetDspBusInformation(index, out dspBusInfo1) != 0;
        dspBusInfo1.Convert(out dspBusInfo);
        return dspBusInformation;
    }

    public static int GetNumAisacControls() => CriAtomExAcfDebug.criAtomExAcf_GetNumAisacControls();

    public static bool GetAisacControlInfo(ushort index, out CriAtomExAcfDebug.AisacControlInfo info) {
        CriAtomExAcfDebug.AisacControlInfoForMarshaling info1;
        bool aisacControlInfo = CriAtomExAcfDebug.criAtomExAcf_GetAisacControlInfo(index, out info1) != 0;
        info1.Convert(out info);
        return aisacControlInfo;
    }

    public static uint GetAisacControlIdByName(string name) => CriAtomExAcfDebug.criAtomExAcf_GetAisacControlIdByName(name);

    public static string GetAisacControlNameById(uint id) => Utility.PtrToStringAutoOrNull(CriAtomExAcfDebug.criAtomExAcf_GetAisacControlNameById(id));

    public static int GetNumGlobalAisacs() => CriAtomExAcfDebug.criAtomExAcf_GetNumGlobalAisacs();

    public static bool GetGlobalAisacInfo(ushort index, out CriAtomExAcfDebug.GlobalAisacInfo info) {
        CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info1;
        bool globalAisacInfo = CriAtomExAcfDebug.criAtomExAcf_GetGlobalAisacInfo(index, out info1) != 0;
        info1.Convert(out info);
        return globalAisacInfo;
    }

    public static bool GetGlobalAisacInfoByName(
      string name,
      out CriAtomExAcfDebug.GlobalAisacInfo info) {
        CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info1;
        bool globalAisacInfoByName = CriAtomExAcfDebug.criAtomExAcf_GetGlobalAisacInfoByName(name, out info1) != 0;
        info1.Convert(out info);
        return globalAisacInfoByName;
    }

    public static int GetNumSelectors() => CriAtomExAcfDebug.criAtomExAcf_GetNumSelectors();

    public static bool GetSelectorInfoByIndex(ushort index, out CriAtomExAcfDebug.SelectorInfo info) {
        CriAtomExAcfDebug.SelectorInfoForMarshaling info1;
        bool selectorInfoByIndex = CriAtomExAcfDebug.criAtomExAcf_GetSelectorInfoByIndex(index, out info1) != 0;
        info1.Convert(out info);
        return selectorInfoByIndex;
    }

    public static bool GetSelectorInfoByName(string name, out CriAtomExAcfDebug.SelectorInfo info) {
        CriAtomExAcfDebug.SelectorInfoForMarshaling info1;
        bool selectorInfoByName = CriAtomExAcfDebug.criAtomExAcf_GetSelectorInfoByName(name, out info1) != 0;
        info1.Convert(out info);
        return selectorInfoByName;
    }

    public static bool GetSelectorLabelInfo(
      ref CriAtomExAcfDebug.SelectorInfo selectorInfo,
      ushort index,
      out CriAtomExAcfDebug.SelectorLabelInfo labelInfo) {
        CriAtomExAcfDebug.SelectorLabelInfoForMarshaling label_info;
        CriAtomExAcfDebug.SelectorInfoForMarshaling selectorInfoForMarshaling = new CriAtomExAcfDebug.SelectorInfoForMarshaling()
    {
            index = selectorInfo.index,
            numLabels = selectorInfo.numLabels
        };
        bool selectorLabelInfo = CriAtomExAcfDebug.criAtomExAcf_GetSelectorLabelInfo(ref selectorInfoForMarshaling, index, out label_info) != 0;
        label_info.Convert(out labelInfo);
        return selectorLabelInfo;
    }

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetNumCategories();

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetCategoryInfo(
      ushort index,
      out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetCategoryInfoByName(
      string name,
      out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetCategoryInfoById(
      uint id,
      out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetDspBusInformation(
      ushort index,
      out CriAtomExAcfDebug.DspBusInfoForMarshaling dspBusInfo);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetNumAisacControls();

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetAisacControlInfo(
      ushort index,
      out CriAtomExAcfDebug.AisacControlInfoForMarshaling info);

    [DllImport("cri_ware_unity")]
    private static extern uint criAtomExAcf_GetAisacControlIdByName(string name);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExAcf_GetAisacControlNameById(uint id);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetNumGlobalAisacs();

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetGlobalAisacInfo(
      ushort index,
      out CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetGlobalAisacInfoByName(
      string name,
      out CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetNumSelectors();

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetSelectorInfoByIndex(
      ushort index,
      out CriAtomExAcfDebug.SelectorInfoForMarshaling info);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetSelectorInfoByName(
      string name,
      out CriAtomExAcfDebug.SelectorInfoForMarshaling info);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExAcf_GetSelectorLabelInfo(
      ref CriAtomExAcfDebug.SelectorInfoForMarshaling info,
      ushort labelIndex,
      out CriAtomExAcfDebug.SelectorLabelInfoForMarshaling label_info);

    public struct CategoryInfo {
        public uint groupNo;
        public uint id;
        public string name;
        public uint numCueLimits;
        public float volume;
    }

    public struct DspBusInfo {
        public string name;
        public float volume;
        public float pan3dVolume;
        public float pan3dAngle;
        public float pan3dDistance;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public ushort[] fxIndexes;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public ushort[] busLinkIndexes;
        public ushort busNo;
        public byte numFxes;
        public byte numBusLinks;
    }

    public struct AisacControlInfo {
        public string name;
        public uint id;
    }

    public enum AisacType {
        Normal,
        AutoModulation,
    }

    public struct GlobalAisacInfo {
        public string name;
        public ushort index;
        public ushort numGraphs;
        public CriAtomExAcfDebug.AisacType type;
        public float randomRange;
        public ushort controlId;
    }

    public struct SelectorInfo {
        public string name;
        public ushort index;
        public ushort numLabels;
        public ushort globalLabelIndex;
    }

    public struct SelectorLabelInfo {
        public string selectorName;
        public string labelName;
    }

    private struct CategoryInfoForMarshaling {
        public uint groupNo;
        public uint id;
        public IntPtr namePtr;
        public uint numCueLimits;
        public float volume;

        public void Convert(out CriAtomExAcfDebug.CategoryInfo x) {
            x.groupNo = groupNo;
            x.id = id;
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.numCueLimits = numCueLimits;
            x.volume = volume;
        }
    }

    private struct DspBusInfoForMarshaling {
        public IntPtr namePtr;
        public float volume;
        public float pan3dVolume;
        public float pan3dAngle;
        public float pan3dDistance;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public ushort[] fxIndexes;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public ushort[] busLinkIndexes;
        public ushort busNo;
        public byte numFxes;
        public byte numBusLinks;

        public void Convert(out CriAtomExAcfDebug.DspBusInfo x) {
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.volume = volume;
            x.pan3dVolume = pan3dVolume;
            x.pan3dAngle = pan3dAngle;
            x.pan3dDistance = pan3dDistance;
            x.fxIndexes = fxIndexes;
            x.busLinkIndexes = busLinkIndexes;
            x.busNo = busNo;
            x.numFxes = numFxes;
            x.numBusLinks = numBusLinks;
        }
    }

    private struct AisacControlInfoForMarshaling {
        public IntPtr namePtr;
        public uint id;

        public void Convert(out CriAtomExAcfDebug.AisacControlInfo x) {
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.id = id;
        }
    }

    private struct GlobalAisacInfoForMarshaling {
        public IntPtr namePtr;
        public ushort index;
        public ushort numGraphs;
        public uint type;
        public float randomRange;
        public ushort controlId;
        public ushort dummy;

        public void Convert(out CriAtomExAcfDebug.GlobalAisacInfo x) {
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.index = index;
            x.numGraphs = numGraphs;
            x.type = (CriAtomExAcfDebug.AisacType)type;
            x.randomRange = randomRange;
            x.controlId = controlId;
        }
    }

    private struct SelectorInfoForMarshaling {
        public IntPtr namePtr;
        public ushort index;
        public ushort numLabels;
        public ushort globalLabelIndex;

        public void Convert(out CriAtomExAcfDebug.SelectorInfo x) {
            x.name = Utility.PtrToStringAutoOrNull(namePtr);
            x.index = index;
            x.numLabels = numLabels;
            x.globalLabelIndex = globalLabelIndex;
        }
    }

    private struct SelectorLabelInfoForMarshaling {
        public IntPtr selectorNamePtr;
        public IntPtr labelNamePtr;

        public void Convert(out CriAtomExAcfDebug.SelectorLabelInfo x) {
            x.selectorName = Utility.PtrToStringAutoOrNull(selectorNamePtr);
            x.labelName = Utility.PtrToStringAutoOrNull(labelNamePtr);
        }
    }
}
