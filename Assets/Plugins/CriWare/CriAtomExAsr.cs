﻿using System;
using System.Runtime.InteropServices;

public class CriAtomExAsr {
    public static void AttachBusAnalyzer(int interval, int peakHoldTime) {
        CriAtomExAsr.BusAnalyzerConfig config;
        config.interval = 50;
        config.peakHoldTime = 1000;
        for (int bus_no = 0; bus_no < 8; ++bus_no)
            CriAtomExAsr.criAtomExAsr_AttachBusAnalyzer(bus_no, ref config);
    }

    public static void DetachBusAnalyzer() {
        for (int bus_no = 0; bus_no < 8; ++bus_no)
            CriAtomExAsr.criAtomExAsr_DetachBusAnalyzer(bus_no);
    }

    public static void GetBusAnalyzerInfo(int bus, out CriAtomExAsr.BusAnalyzerInfo info) {
        using (CriStructMemory<CriAtomExAsr.BusAnalyzerInfo> criStructMemory = new CriStructMemory<CriAtomExAsr.BusAnalyzerInfo>()) {
            CriAtomExAsr.criAtomExAsr_GetBusAnalyzerInfo(bus, criStructMemory.ptr);
            info = new CriAtomExAsr.BusAnalyzerInfo(criStructMemory.bytes);
        }
    }

    public static void SetBusVolume(int bus, float volume) => CriAtomExAsr.criAtomExAsr_SetBusVolume(bus, volume);

    public static void SetBusSendLevel(int bus, int sendTo, float level) => CriAtomExAsr.criAtomExAsr_SetBusSendLevel(bus, sendTo, level);

    public static void SetBusMatrix(int bus, int inputChannels, int outputChannels, float[] matrix) => CriAtomExAsr.criAtomExAsr_SetBusMatrix(bus, inputChannels, outputChannels, matrix);

    public static void SetEffectParameter(
      string busName,
      string effectName,
      uint parameterIndex,
      float parameterValue) {
        CriAtomExAsr.criAtomExAsr_SetEffectParameter(busName, effectName, parameterIndex, parameterValue);
        CriAtomExAsr.criAtomExAsr_UpdateEffectParameters(busName, effectName);
    }

    public static float GetEffectParameter(string busName, string effectName, uint parameterIndex) => CriAtomExAsr.criAtomExAsr_GetEffectParameter(busName, effectName, parameterIndex);

    public static bool RegisterEffectInterface(IntPtr afx_interface) => CriAtomExAsr.criAtomExAsr_RegisterEffectInterface(afx_interface);

    public static void UnregisterEffectInterface(IntPtr afx_interface) => CriAtomExAsr.criAtomExAsr_UnregisterEffectInterface(afx_interface);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_AttachBusAnalyzer(
      int bus_no,
      ref CriAtomExAsr.BusAnalyzerConfig config);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_DetachBusAnalyzer(int bus_no);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_GetBusAnalyzerInfo(int bus_no, IntPtr info);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_SetBusVolume(int bus_no, float volume);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_SetBusSendLevel(int bus_no, int sendto_no, float level);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_SetBusMatrix(
      int bus_no,
      int input_channels,
      int output_channels,
      [MarshalAs(UnmanagedType.LPArray)] float[] matrix);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_UpdateEffectParameters(string busName, string effectName);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_SetEffectParameter(
      string busName,
      string effectName,
      uint parameterIndex,
      float parameterValue);

    [DllImport("cri_ware_unity")]
    private static extern float criAtomExAsr_GetEffectParameter(
      string busName,
      string effectName,
      uint parameterIndex);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExAsr_RegisterEffectInterface(IntPtr afx_interface);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsr_UnregisterEffectInterface(IntPtr afx_interface);

    private struct BusAnalyzerConfig {
        public int interval;
        public int peakHoldTime;
    }

    public struct BusAnalyzerInfo {
        public int numChannels;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] rmsLevels;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] peakLevels;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] peakHoldLevels;

        public BusAnalyzerInfo(byte[] data) {
            if (data != null) {
                numChannels = BitConverter.ToInt32(data, 0);
                rmsLevels = new float[8];
                for (int index = 0; index < 8; ++index)
                    rmsLevels[index] = BitConverter.ToSingle(data, 4 + index * 4);
                peakLevels = new float[8];
                for (int index = 0; index < 8; ++index)
                    peakLevels[index] = BitConverter.ToSingle(data, 36 + index * 4);
                peakHoldLevels = new float[8];
                for (int index = 0; index < 8; ++index)
                    peakHoldLevels[index] = BitConverter.ToSingle(data, 68 + index * 4);
            } else {
                numChannels = 0;
                rmsLevels = new float[8];
                peakLevels = new float[8];
                peakHoldLevels = new float[8];
            }
        }
    }
}
