﻿using System;
using System.Runtime.InteropServices;

public class CriAtomExAsrRack : IDisposable {
    public static int defaultRackId;
    private int _rackId = -1;

    public CriAtomExAsrRack(
      CriAtomExAsrRack.Config config,
      CriAtomExAsrRack.PlatformConfig platformConfig) {
        _rackId = CriAtomExAsrRack.criAtomUnityAsrRack_Create(ref config, ref platformConfig);
        if (config.context != IntPtr.Zero)
            Marshal.FreeHGlobal(config.context);
        if (_rackId == -1)
            throw new Exception("CriAtomExAsrRack() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (_rackId != -1) {
            CriAtomExAsrRack.criAtomExAsrRack_Destroy(_rackId);
            _rackId = -1;
        }
        GC.SuppressFinalize((object)this);
    }

    public int rackId => _rackId;

    public static CriAtomExAsrRack.Config defaultConfig {
        get {
            CriAtomExAsrRack.Config defaultConfig;
            defaultConfig.serverFrequency = 60f;
            defaultConfig.numBuses = 8;
            defaultConfig.soundRendererType = CriAtomEx.SoundRendererType.Native;
            defaultConfig.outputRackId = 0;
            defaultConfig.context = IntPtr.Zero;
            defaultConfig.outputChannels = 2;
            defaultConfig.outputSamplingRate = 44100;
            return defaultConfig;
        }
    }

    ~CriAtomExAsrRack() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern int criAtomUnityAsrRack_Create(
      [In] ref CriAtomExAsrRack.Config config,
      [In] ref CriAtomExAsrRack.PlatformConfig platformConfig);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsrRack_Destroy(int rackId);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsrRack_AttachDspBusSetting(
      int rackId,
      string setting,
      IntPtr work,
      int workSize);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsrRack_DetachDspBusSetting(int rackId);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExAsrRack_ApplyDspBusSnapshot(
      int rackId,
      string snapshotName,
      int timeMs);

    public struct Config {
        public float serverFrequency;
        public int numBuses;
        public int outputChannels;
        public int outputSamplingRate;
        public CriAtomEx.SoundRendererType soundRendererType;
        public int outputRackId;
        public IntPtr context;
    }

    public struct PlatformConfig {
        public byte reserved;
    }
}
