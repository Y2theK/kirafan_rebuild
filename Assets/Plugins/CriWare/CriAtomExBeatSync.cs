﻿using System;
using System.Runtime.InteropServices;

public static class CriAtomExBeatSync {
    public static void SetCallback(CriAtomExBeatSync.CbFunc func) => CriAtom.SetBeatSyncCallback(func);

    public struct Info {
        public IntPtr playerHn;
        public uint playbackId;
        public uint barCount;
        public uint beatCount;
    }

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CbFunc(ref CriAtomExBeatSync.Info info);
}
