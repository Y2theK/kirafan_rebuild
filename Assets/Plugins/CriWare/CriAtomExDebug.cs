﻿using System.Runtime.InteropServices;

public static class CriAtomExDebug {
    public static void GetResourcesInfo(out CriAtomExDebug.ResourcesInfo resourcesInfo) => CriAtomExDebug.criAtomExDebug_GetResourcesInfo(out resourcesInfo);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExDebug_GetResourcesInfo(
      out CriAtomExDebug.ResourcesInfo resourcesInfo);

    public struct ResourcesInfo {
        public CriAtomEx.ResourceUsage virtualVoiceUsage;
        public CriAtomEx.ResourceUsage sequenceUsage;
        public CriAtomEx.ResourceUsage sequenceTrackUsage;
        public CriAtomEx.ResourceUsage sequenceTrackItemUsage;
    }
}
