﻿using System.Runtime.InteropServices;

public static class CriAtomExLatencyEstimator {
    public static void InitializeModule() => CriAtomExLatencyEstimator.criAtomLatencyEstimator_Initialize_ANDROID();

    public static void FinalizeModule() => CriAtomExLatencyEstimator.criAtomLatencyEstimator_Finalize_ANDROID();

    public static CriAtomExLatencyEstimator.EstimatorInfo GetCurrentInfo() => CriAtomExLatencyEstimator.criAtomLatencyEstimator_GetCurrentInfo_ANDROID();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomLatencyEstimator_Initialize_ANDROID();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomLatencyEstimator_Finalize_ANDROID();

    [DllImport("cri_ware_unity")]
    private static extern CriAtomExLatencyEstimator.EstimatorInfo criAtomLatencyEstimator_GetCurrentInfo_ANDROID();

    public enum Status {
        Stop,
        Processing,
        Done,
        Error,
    }

    public struct EstimatorInfo {
        public CriAtomExLatencyEstimator.Status status;
        public uint estimated_latency;
    }
}
