﻿using System;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct CriAtomExPlayback {
    public CriAtomExPlayback(uint id)
      : this() {
        this.id = id;
    }

    public void Stop(bool ignoresReleaseTime) {
        if (!ignoresReleaseTime)
            CriAtomExPlayback.criAtomExPlayback_Stop(id);
        else
            CriAtomExPlayback.criAtomExPlayback_StopWithoutReleaseTime(id);
    }

    public void Pause() => CriAtomExPlayback.criAtomExPlayback_Pause(id, true);

    public void Resume(CriAtomEx.ResumeMode mode) => CriAtomExPlayback.criAtomExPlayback_Resume(id, mode);

    public bool IsPaused() => CriAtomExPlayback.criAtomExPlayback_IsPaused(id);

    public bool GetFormatInfo(out CriAtomEx.FormatInfo info) => CriAtomExPlayback.criAtomExPlayback_GetFormatInfo(id, out info);

    public CriAtomExPlayback.Status GetStatus() => CriAtomExPlayback.criAtomExPlayback_GetStatus(id);

    public long GetTime() => CriAtomExPlayback.criAtomExPlayback_GetTime(id);

    public long GetTimeSyncedWithAudio() => CriAtomExPlayback.criAtomExPlayback_GetTimeSyncedWithAudio(id);

    public bool GetNumPlayedSamples(out long numSamples, out int samplingRate) => CriAtomExPlayback.criAtomExPlayback_GetNumPlayedSamples(id, out numSamples, out samplingRate);

    public long GetSequencePosition() => CriAtomExPlayback.criAtomExPlayback_GetSequencePosition(id);

    public int GetCurrentBlockIndex() => CriAtomExPlayback.criAtomExPlayback_GetCurrentBlockIndex(id);

    public bool GetTrackInfo(out CriAtomExPlayback.TrackInfo info) => CriAtomExPlayback.criAtomExPlayback_GetPlaybackTrackInfo(id, out info);

    public void SetNextBlockIndex(int index) => CriAtomExPlayback.criAtomExPlayback_SetNextBlockIndex(id, index);

    public uint id { get; private set; }

    public CriAtomExPlayback.Status status => GetStatus();

    public long time => GetTime();

    public long timeSyncedWithAudio => GetTimeSyncedWithAudio();

    public void Stop() => CriAtomExPlayback.criAtomExPlayback_Stop(id);

    public void StopWithoutReleaseTime() => CriAtomExPlayback.criAtomExPlayback_StopWithoutReleaseTime(id);

    public void Pause(bool sw) => CriAtomExPlayback.criAtomExPlayback_Pause(id, sw);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayback_Stop(uint id);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayback_StopWithoutReleaseTime(uint id);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayback_Pause(uint id, bool sw);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayback_Resume(uint id, CriAtomEx.ResumeMode mode);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExPlayback_IsPaused(uint id);

    [DllImport("cri_ware_unity")]
    private static extern CriAtomExPlayback.Status criAtomExPlayback_GetStatus(uint id);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExPlayback_GetFormatInfo(uint id, out CriAtomEx.FormatInfo info);

    [DllImport("cri_ware_unity")]
    private static extern long criAtomExPlayback_GetTime(uint id);

    [DllImport("cri_ware_unity")]
    private static extern long criAtomExPlayback_GetTimeSyncedWithAudio(uint id);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExPlayback_GetNumPlayedSamples(
      uint id,
      out long num_samples,
      out int sampling_rate);

    [DllImport("cri_ware_unity")]
    private static extern long criAtomExPlayback_GetSequencePosition(uint id);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayback_SetNextBlockIndex(uint id, int index);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetCurrentBlockIndex(uint id);

    [DllImport("cri_ware_unity")]
    private static extern bool criAtomExPlayback_GetPlaybackTrackInfo(
      uint id,
      out CriAtomExPlayback.TrackInfo info);

    public enum Status {
        Prep = 1,
        Playing = 2,
        Removed = 3,
    }

    public struct TrackInfo {
        public uint id;
        public CriAtomEx.CueType sequenceType;
        public IntPtr playerHn;
        public ushort trackNo;
        public ushort reserved;
    }
}
