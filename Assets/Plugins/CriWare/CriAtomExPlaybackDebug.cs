﻿using System.Runtime.InteropServices;

public static class CriAtomExPlaybackDebug {
    public static bool GetParameter(
      CriAtomExPlayback playback,
      CriAtomEx.Parameter parameterId,
      out float value) {
        return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterFloat32(playback.id, (int)parameterId, out value) == 1;
    }

    public static bool GetParameter(
      CriAtomExPlayback playback,
      CriAtomEx.Parameter parameterId,
      out uint value) {
        return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterUint32(playback.id, (int)parameterId, out value) == 1;
    }

    public static bool GetParameter(
      CriAtomExPlayback playback,
      CriAtomEx.Parameter parameterId,
      out int value) {
        return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterSint32(playback.id, (int)parameterId, out value) == 1;
    }

    public static bool GetAisacControl(CriAtomExPlayback playback, uint controlId, out float value) => CriAtomExPlaybackDebug.criAtomExPlayback_GetAisacControlById(playback.id, controlId, out value) == 1;

    public static bool GetAisacControl(
      CriAtomExPlayback playback,
      string controlName,
      out float value) {
        return CriAtomExPlaybackDebug.criAtomExPlayback_GetAisacControlByName(playback.id, controlName, out value) == 1;
    }

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetParameterFloat32(
      uint id,
      int parameterId,
      out float value);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetParameterUint32(
      uint id,
      int parameterId,
      out uint value);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetParameterSint32(
      uint id,
      int parameterId,
      out int value);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetAisacControlById(
      uint id,
      uint controlId,
      out float value);

    [DllImport("cri_ware_unity")]
    private static extern int criAtomExPlayback_GetAisacControlByName(
      uint id,
      string controlName,
      out float value);
}
