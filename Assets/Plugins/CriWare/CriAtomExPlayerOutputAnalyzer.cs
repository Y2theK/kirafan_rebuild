﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class CriAtomExPlayerOutputAnalyzer : IDisposable {
    public const int MaximumSpectrumBands = 512;
    private IntPtr handle = IntPtr.Zero;
    private CriAtomExPlayer player;
    private int numBands = 8;

    public CriAtomExPlayerOutputAnalyzer(
      CriAtomExPlayerOutputAnalyzer.Type[] types,
      CriAtomExPlayerOutputAnalyzer.Config[] configs = null) {
        handle = CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_Create(types.Length, types, configs);
        if (handle == IntPtr.Zero)
            throw new Exception("criAtomExPlayerOutputAnalyzer_Create() failed.");
        if (configs != null)
            numBands = configs[0].num_spectrum_analyzer_bands;
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    public IntPtr nativeHandle => handle;

    public void Dispose() => Dispose(true);

    private void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            DetachExPlayer();
            CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_Destroy(handle);
            handle = IntPtr.Zero;
        }
        if (!disposing)
            return;
        GC.SuppressFinalize((object)this);
    }

    public bool AttachExPlayer(CriAtomExPlayer player) {
        if (player == null || handle == IntPtr.Zero)
            return false;
        DetachExPlayer();
        if (player.GetStatus() != CriAtomExPlayer.Status.Stop)
            return false;
        CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_AttachExPlayer(handle, player.nativeHandle);
        this.player = player;
        return true;
    }

    public void DetachExPlayer() {
        if (player == null || handle == IntPtr.Zero)
            return;
        if (player.GetStatus() != CriAtomExPlayer.Status.Stop) {
            Debug.LogWarning((object)"[CRIWARE] Warning: CriAtomExPlayer is forced to stop for detaching CriAtomExPlayerOutputAnalyzer.");
            player.StopWithoutReleaseTime();
        }
        CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_DetachExPlayer(handle, player.nativeHandle);
        player = (CriAtomExPlayer)null;
    }

    public float GetRms(int channel) => player == null || handle == IntPtr.Zero || player.GetStatus() != CriAtomExPlayer.Status.Playing && player.GetStatus() != CriAtomExPlayer.Status.Prep ? 0.0f : CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_GetRms(handle, channel);

    public void GetSpectrumLevels(ref float[] levels) {
        if (player == null || handle == IntPtr.Zero)
            return;
        if (levels == null || levels.Length < numBands)
            levels = new float[numBands];
        Marshal.Copy(CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_GetSpectrumLevels(handle), levels, 0, numBands);
    }

    ~CriAtomExPlayerOutputAnalyzer() => Dispose(false);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExPlayerOutputAnalyzer_Create(
      int numTypes,
      CriAtomExPlayerOutputAnalyzer.Type[] types,
      CriAtomExPlayerOutputAnalyzer.Config[] configs);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExPlayerOutputAnalyzer_Destroy(IntPtr analyzer);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayerOutputAnalyzer_AttachExPlayer(
      IntPtr analyzer,
      IntPtr player);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExPlayerOutputAnalyzer_DetachExPlayer(
      IntPtr analyzer,
      IntPtr player);

    [DllImport("cri_ware_unity")]
    private static extern float criAtomExPlayerOutputAnalyzer_GetRms(IntPtr analyzer, int channel);

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExPlayerOutputAnalyzer_GetSpectrumLevels(IntPtr analyzer);

    public enum Type {
        LevelMeter,
        SpectrumAnalyzer,
    }

    public struct Config {
        public int num_spectrum_analyzer_bands;

        public Config(int num_spectrum_analyzer_bands = 8) => this.num_spectrum_analyzer_bands = num_spectrum_analyzer_bands;
    }
}
