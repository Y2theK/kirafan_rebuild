﻿using System.Runtime.InteropServices;

public static class CriAtomExSequencer {
    public static void SetEventCallback(CriAtomExSequencer.EventCbFunc func, string separator = "\t") => CriAtom.SetEventCallback(func, separator);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void EventCbFunc(string eventParamsString);
}
