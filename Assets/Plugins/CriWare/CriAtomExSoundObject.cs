﻿using System;
using System.Runtime.InteropServices;

public class CriAtomExSoundObject : IDisposable {
    private IntPtr handle = IntPtr.Zero;

    public CriAtomExSoundObject(bool enableVoiceLimitScope, bool enableCategoryCueLimitScope) {
        if (!CriAtomPlugin.isInitialized)
            throw new Exception("CriAtomPlugin is not initialized.");
        CriAtomExSoundObject.Config config;
        config.enableVoiceLimitScope = enableVoiceLimitScope;
        config.enableCategoryCueLimitScope = enableCategoryCueLimitScope;
        handle = CriAtomExSoundObject.criAtomExSoundObject_Create(ref config, IntPtr.Zero, 0);
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    public IntPtr nativeHandle => handle;

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            CriAtomExSoundObject.criAtomExSoundObject_Destroy(handle);
            handle = IntPtr.Zero;
        }
        GC.SuppressFinalize((object)this);
    }

    public void AddPlayer(CriAtomExPlayer player) => CriAtomExSoundObject.criAtomExSoundObject_AddPlayer(handle, player.nativeHandle);

    public void DeletePlayer(CriAtomExPlayer player) => CriAtomExSoundObject.criAtomExSoundObject_DeletePlayer(handle, player.nativeHandle);

    public void DeleteAllPlayers() => CriAtomExSoundObject.criAtomExSoundObject_DeleteAllPlayers(handle);

    ~CriAtomExSoundObject() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExSoundObject_Create(
      ref CriAtomExSoundObject.Config config,
      IntPtr work,
      int work_size);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExSoundObject_Destroy(IntPtr soundObject);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExSoundObject_AddPlayer(IntPtr soundObject, IntPtr player);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExSoundObject_DeletePlayer(IntPtr soundObject, IntPtr player);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExSoundObject_DeleteAllPlayers(IntPtr soundObject);

    private struct Config {
        public bool enableVoiceLimitScope;
        public bool enableCategoryCueLimitScope;
    }
}
