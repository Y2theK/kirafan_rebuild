﻿using System;
using System.Runtime.InteropServices;

public class CriAtomExStandardVoicePool : CriAtomExVoicePool {
    public CriAtomExStandardVoicePool(
      int numVoices,
      int maxChannels,
      int maxSamplingRate,
      bool streamingFlag,
      uint identifier = 0) {
        _identifier = identifier;
        _numVoices = numVoices;
        _maxChannels = maxChannels;
        _maxSamplingRate = maxSamplingRate;
        CriAtomExVoicePool.VoicePoolConfig voicePoolConfig = new CriAtomExVoicePool.VoicePoolConfig()
    {
            identifier = identifier,
            numVoices = numVoices,
            playerConfig = {
          maxChannels = maxChannels,
          maxSamplingRate = maxSamplingRate,
          streamingFlag = streamingFlag,
          soundRendererType = 2,
          decodeLatency = 0
        }
        };
        _handle = CriAtomExStandardVoicePool.criAtomExVoicePool_AllocateStandardVoicePool(ref voicePoolConfig, IntPtr.Zero, 0);
        if (_handle == IntPtr.Zero)
            throw new Exception("CriAtomExStandardVoicePool() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Atom);
    }

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criAtomExVoicePool_AllocateStandardVoicePool(
      ref CriAtomExVoicePool.VoicePoolConfig config,
      IntPtr work,
      int work_size);
}
