﻿using System;
using System.Runtime.InteropServices;

public abstract class CriAtomExVoicePool : IDisposable {
    public const int StandardMemoryAsrVoicePoolId = 0;
    public const int StandardStreamingAsrVoicePoolId = 1;
    public const int StandardMemoryNsrVoicePoolId = 2;
    public const int StandardStreamingNsrVoicePoolId = 3;
    protected IntPtr _handle = IntPtr.Zero;
    protected uint _identifier;
    protected int _numVoices;
    protected int _maxChannels;
    protected int _maxSamplingRate;

    public static CriAtomExVoicePool.UsedVoicesInfo GetNumUsedVoices(
      CriAtomExVoicePool.VoicePoolId voicePoolId) {
        CriAtomExVoicePool.UsedVoicesInfo numUsedVoices;
        CriAtomExVoicePool.criAtomUnity_GetNumUsedVoices((int)voicePoolId, out numUsedVoices.numUsedVoices, out numUsedVoices.numPoolVoices);
        return numUsedVoices;
    }

    public IntPtr nativeHandle => _handle;

    public uint identifier => _identifier;

    public int numVoices => _numVoices;

    public int maxChannels => _maxChannels;

    public int maxSamplingRate => _maxSamplingRate;

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (_handle != IntPtr.Zero)
            CriAtomExVoicePool.criAtomExVoicePool_Free(_handle);
        GC.SuppressFinalize((object)this);
    }

    public CriAtomExVoicePool.UsedVoicesInfo GetNumUsedVoices() {
        CriAtomExVoicePool.UsedVoicesInfo numUsedVoices;
        CriAtomExVoicePool.criAtomExVoicePool_GetNumUsedVoices(_handle, out numUsedVoices.numUsedVoices, out numUsedVoices.numPoolVoices);
        return numUsedVoices;
    }

    public void AttachDspTimeStretch() {
        CriAtomExVoicePool.ExTimeStretchConfig config;
        config.numDsp = _numVoices;
        config.maxChannels = _maxChannels;
        config.maxSamplingRate = _maxSamplingRate;
        config.config.reserved = 0;
        CriAtomExVoicePool.criAtomExVoicePool_AttachDspTimeStretch(_handle, ref config, IntPtr.Zero, 0);
    }

    public void AttachDspPitchShifter(
      CriAtomExVoicePool.PitchShifterMode mode = CriAtomExVoicePool.PitchShifterMode.Music,
      int windosSize = 1024,
      int overlapTimes = 4) {
        CriAtomExVoicePool.ExPitchShifterConfig config;
        config.numDsp = _numVoices;
        config.maxChannels = _maxChannels;
        config.maxSamplingRate = _maxSamplingRate;
        config.config.mode = (int)mode;
        config.config.windowSize = windosSize;
        config.config.overlapTimes = overlapTimes;
        CriAtomExVoicePool.criAtomExVoicePool_AttachDspPitchShifter(_handle, ref config, IntPtr.Zero, 0);
    }

    public void DetachDsp() => CriAtomExVoicePool.criAtomExVoicePool_DetachDsp(_handle);

    ~CriAtomExVoicePool() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_GetNumUsedVoices(
      int voice_pool_id,
      out int num_used_voices,
      out int num_pool_voices);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExVoicePool_GetNumUsedVoices(
      IntPtr pool,
      out int num_used_voices,
      out int num_pool_voices);

    [DllImport("cri_ware_unity")]
    public static extern void criAtomExVoicePool_Free(IntPtr pool);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExVoicePool_AttachDspTimeStretch(
      IntPtr pool,
      ref CriAtomExVoicePool.ExTimeStretchConfig config,
      IntPtr work,
      int work_size);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExVoicePool_AttachDspPitchShifter(
      IntPtr pool,
      ref CriAtomExVoicePool.ExPitchShifterConfig config,
      IntPtr work,
      int work_size);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomExVoicePool_DetachDsp(IntPtr pool);

    public enum VoicePoolId {
        StandardMemory,
        StandardStreaming,
        LowLatencyMemory,
        LowLatencyStreaming,
        HcaMxMemory,
        HcaMxStreaming,
    }

    public enum PitchShifterMode {
        Music,
        Vocal,
        SoundEffect,
        Speech,
    }

    public struct UsedVoicesInfo {
        public int numUsedVoices;
        public int numPoolVoices;
    }

    protected struct PlayerConfig {
        public int maxChannels;
        public int maxSamplingRate;
        public bool streamingFlag;
        public int soundRendererType;
        public int decodeLatency;
    }

    protected struct VoicePoolConfig {
        public uint identifier;
        public int numVoices;
        public CriAtomExVoicePool.PlayerConfig playerConfig;
    }

    private struct PitchShifterConfig {
        public int mode;
        public int windowSize;
        public int overlapTimes;
    }

    private struct ExPitchShifterConfig {
        public int numDsp;
        public int maxChannels;
        public int maxSamplingRate;
        public CriAtomExVoicePool.PitchShifterConfig config;
    }

    private struct TimeStretchConfig {
        public int reserved;
    }

    private struct ExTimeStretchConfig {
        public int numDsp;
        public int maxChannels;
        public int maxSamplingRate;
        public CriAtomExVoicePool.TimeStretchConfig config;
    }
}
