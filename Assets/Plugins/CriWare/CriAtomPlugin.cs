﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;

public static class CriAtomPlugin {
    public const string criAtomUnityEditorVersion = "Ver.0.21.07";
    private static int initializationCount = 0;
    private static List<IntPtr> effectInterfaceList = (List<IntPtr>) null;
    private static float timeSinceStartup = Time.realtimeSinceStartup;
    private static CriWare.CpuUsage cpuUsage;
    private static int CRIATOMUNITY_PARAMETER_ID_LOOP_COUNT = 0;
    private static ushort CRIATOMPARAMETER2_ID_INVALID = ushort.MaxValue;

    public static void Log(string log) {
    }

    public static bool isInitialized => CriAtomPlugin.initializationCount > 0;

    public static bool GetAudioEffectInterfaceList(out List<IntPtr> effect_interface_list) {
        if (CriAtomPlugin.isInitialized) {
            effect_interface_list = (List<IntPtr>)null;
            return false;
        }
        if (CriAtomPlugin.effectInterfaceList == null)
            CriAtomPlugin.effectInterfaceList = new List<IntPtr>();
        effect_interface_list = CriAtomPlugin.effectInterfaceList;
        return true;
    }

    private static IntPtr GetSpatializerCoreInterfaceFromAtomOculusAudioBridge() {
        System.Type type = System.Type.GetType("CriAtomOculusAudio");
        if (type == null) {
            Debug.LogError((object)"[CRIWARE] ERROR: Cri Atom Oculus Audio Bridge Plugin is not imported.");
        } else {
            MethodInfo method1 = type.GetMethod("SupportCurrentPlatform");
            if (method1 == null) {
                Debug.LogError((object)"[CRIWARE] ERROR: CriAtomOculusAudio.SupportCurrentPlatform method is not found.");
                return IntPtr.Zero;
            }
            if ((bool)method1.Invoke((object)null, (object[])null)) {
                MethodInfo method2 = type.GetMethod("GetSpatializerCoreInterface");
                if (method2 != null)
                    return (IntPtr)method2.Invoke((object)null, (object[])null);
                Debug.LogError((object)"[CRIWARE] ERROR: CriAtomOculusAudio.GetSpatializerCoreInterface method is not found.");
                return IntPtr.Zero;
            }
        }
        return IntPtr.Zero;
    }

    public static void SetConfigParameters(
      int max_virtual_voices,
      int max_voice_limit_groups,
      int max_categories,
      int max_sequence_events_per_frame,
      int max_beatsync_callbacks_per_frame,
      int num_standard_memory_voices,
      int num_standard_streaming_voices,
      int num_hca_mx_memory_voices,
      int num_hca_mx_streaming_voices,
      int output_sampling_rate,
      int num_asr_output_channels,
      bool uses_in_game_preview,
      float server_frequency,
      int max_parameter_blocks,
      int categories_per_playback,
      int num_buses,
      bool vr_mode) {
        IntPtr spatializer_core_interface = IntPtr.Zero;
        if (vr_mode)
            spatializer_core_interface = CriAtomPlugin.GetSpatializerCoreInterfaceFromAtomOculusAudioBridge();
        CriAtomPlugin.criAtomUnity_SetConfigParameters(max_virtual_voices, max_voice_limit_groups, max_categories, max_sequence_events_per_frame, max_beatsync_callbacks_per_frame, num_standard_memory_voices, num_standard_streaming_voices, num_hca_mx_memory_voices, num_hca_mx_streaming_voices, output_sampling_rate, num_asr_output_channels, uses_in_game_preview, server_frequency, max_parameter_blocks, categories_per_playback, num_buses, vr_mode, spatializer_core_interface);
    }

    public static void SetConfigAdditionalParameters_PC(long buffering_time_pc) => CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_PC(buffering_time_pc);

    public static void SetConfigAdditionalParameters_IOS(
      uint buffering_time_ios,
      bool override_ipod_music_ios) {
        CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_IOS(buffering_time_ios, override_ipod_music_ios);
    }

    public static void SetConfigAdditionalParameters_ANDROID(
      int num_low_delay_memory_voices,
      int num_low_delay_streaming_voices,
      int sound_buffering_time,
      int sound_start_buffering_time,
      IntPtr android_context) {
        CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_ANDROID(num_low_delay_memory_voices, num_low_delay_streaming_voices, sound_buffering_time, sound_start_buffering_time, android_context);
    }

    public static void SetConfigAdditionalParameters_VITA(
      int num_atrac9_memory_voices,
      int num_atrac9_streaming_voices,
      int num_mana_decoders) {
    }

    public static void SetConfigAdditionalParameters_PS4(
      int num_atrac9_memory_voices,
      int num_atrac9_streaming_voices,
      bool use_audio3d,
      int num_audio3d_memory_voices,
      int num_audio3d_streaming_voices) {
    }

    public static void SetConfigAdditionalParameters_WEBGL(int num_webaudio_voices) {
    }

    public static void SetMaxSamplingRateForStandardVoicePool(
      int sampling_rate_for_memory,
      int sampling_rate_for_streaming) {
        CriAtomPlugin.criAtomUnity_SetMaxSamplingRateForStandardVoicePool(sampling_rate_for_memory, sampling_rate_for_streaming);
    }

    public static int GetRequiredMaxVirtualVoices(CriAtomConfig atomConfig) => 0 + atomConfig.standardVoicePoolConfig.memoryVoices + atomConfig.standardVoicePoolConfig.streamingVoices + atomConfig.hcaMxVoicePoolConfig.memoryVoices + atomConfig.hcaMxVoicePoolConfig.streamingVoices + atomConfig.androidLowLatencyStandardVoicePoolConfig.memoryVoices + atomConfig.androidLowLatencyStandardVoicePoolConfig.streamingVoices;

    public static void InitializeLibrary() {
        ++CriAtomPlugin.initializationCount;
        if (CriAtomPlugin.initializationCount != 1)
            return;
        if (!CriWareInitializer.IsInitialized())
            Debug.Log((object)"[CRIWARE] CriWareInitializer is not working. Initializes Atom by default parameters.");
        CriFsPlugin.InitializeLibrary();
        CriAtomPlugin.criAtomUnity_Initialize();
        if (CriAtomPlugin.effectInterfaceList != null) {
            for (int index = 0; index < CriAtomPlugin.effectInterfaceList.Count; ++index)
                CriAtomExAsr.RegisterEffectInterface(CriAtomPlugin.effectInterfaceList[index]);
        }
        CriAtomServer.CreateInstance();
        CriAtomListener.CreateSharedNativeListener();
    }

    public static void FinalizeLibrary() {
        --CriAtomPlugin.initializationCount;
        if (CriAtomPlugin.initializationCount < 0) {
            Debug.LogError((object)"[CRIWARE] ERROR: Atom library is already finalized.");
        } else {
            if (CriAtomPlugin.initializationCount != 0)
                return;
            CriAtomListener.DestroySharedNativeListener();
            CriAtomServer.DestroyInstance();
            CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.Atom);
            if (CriAtomPlugin.effectInterfaceList != null) {
                CriAtomPlugin.effectInterfaceList.Clear();
                CriAtomPlugin.effectInterfaceList = (List<IntPtr>)null;
            }
            CriAtomPlugin.criAtomUnity_Finalize();
            CriFsPlugin.FinalizeLibrary();
        }
    }

    public static void Pause(bool pause) {
        if (!CriAtomPlugin.isInitialized)
            return;
        CriAtomPlugin.criAtomUnity_Pause(pause);
    }

    public static CriWare.CpuUsage GetCpuUsage() {
        float realtimeSinceStartup = Time.realtimeSinceStartup;
        if ((double)realtimeSinceStartup - (double)CriAtomPlugin.timeSinceStartup > 1.0) {
            CriAtomEx.PerformanceInfo info;
            CriAtomEx.GetPerformanceInfo(out info);
            CriAtomPlugin.cpuUsage.last = (float)info.lastServerTime * 100f / (float)info.averageServerInterval;
            CriAtomPlugin.cpuUsage.average = (float)info.averageServerTime * 100f / (float)info.averageServerInterval;
            CriAtomPlugin.cpuUsage.peak = (float)info.maxServerTime * 100f / (float)info.averageServerInterval;
            CriAtomEx.ResetPerformanceMonitor();
            CriAtomPlugin.timeSinceStartup = realtimeSinceStartup;
        }
        return CriAtomPlugin.cpuUsage;
    }

    public static ushort GetLoopCountParameterId() {
        ushort nativeParameterId = CriAtomPlugin.criAtomUnity_GetNativeParameterId(CriAtomPlugin.CRIATOMUNITY_PARAMETER_ID_LOOP_COUNT);
        return (int)nativeParameterId != (int)CriAtomPlugin.CRIATOMPARAMETER2_ID_INVALID ? nativeParameterId : throw new Exception("GetNativeParameterId failed.");
    }

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_SetConfigParameters(
      int max_virtual_voices,
      int max_voice_limit_groups,
      int max_categories,
      int max_sequence_events_per_frame,
      int max_beatsync_callbacks_per_frame,
      int num_standard_memory_voices,
      int num_standard_streaming_voices,
      int num_hca_mx_memory_voices,
      int num_hca_mx_streaming_voices,
      int output_sampling_rate,
      int num_asr_output_channels,
      bool uses_in_game_preview,
      float server_frequency,
      int max_parameter_blocks,
      int categories_per_playback,
      int num_buses,
      bool use_ambisonics,
      IntPtr spatializer_core_interface);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_SetConfigAdditionalParameters_PC(long buffering_time_pc);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_SetConfigAdditionalParameters_IOS(
      uint buffering_time_ios,
      bool override_ipod_music_ios);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_SetConfigAdditionalParameters_ANDROID(
      int num_low_delay_memory_voices,
      int num_low_delay_streaming_voices,
      int sound_buffering_time,
      int sound_start_buffering_time,
      IntPtr android_context);

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_Initialize();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_Finalize();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_Pause(bool pause);

    [DllImport("cri_ware_unity")]
    public static extern uint criAtomUnity_GetAllocatedHeapSize();

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnity_ControlDataCompatibility(int code);

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnitySequencer_SetEventCallback(
      IntPtr cbfunc,
      string separator_string);

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnitySequencer_ExecuteQueuedEventCallbacks();

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnity_SetBeatSyncCallback(IntPtr cbfunc);

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnity_ExecuteQueuedBeatSyncCallbacks();

    [DllImport("cri_ware_unity")]
    private static extern void criAtomUnity_SetMaxSamplingRateForStandardVoicePool(
      int sampling_rate_for_memory,
      int sampling_rate_for_streaming);

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnity_BeginLeCompatibleMode();

    [DllImport("cri_ware_unity")]
    public static extern void criAtomUnity_EndLeCompatibleMode();

    [DllImport("cri_ware_unity")]
    public static extern ushort criAtomUnity_GetNativeParameterId(int id);
}
