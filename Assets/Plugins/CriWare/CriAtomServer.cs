﻿using System;
using UnityEngine;

public class CriAtomServer : MonoBehaviour {
    private static CriAtomServer _instance;
    public Action<bool> onApplicationPausePreProcess;
    public Action<bool> onApplicationPausePostProcess;

    public static CriAtomServer instance {
        get {
            CriAtomServer.CreateInstance();
            return CriAtomServer._instance;
        }
    }

    public static void CreateInstance() {
        if (CriAtomServer._instance != null)
            return;
        CriWare.managerObject.AddComponent<CriAtomServer>();
    }

    public static void DestroyInstance() {
        if (CriAtomServer._instance == null)
            return;
        Destroy(CriAtomServer._instance);
    }

    private void Awake() {
        if (CriAtomServer._instance == null)
            CriAtomServer._instance = this;
        else
            Destroy(this);
    }

    private void OnEnable() {
    }

    private void OnDisable() {
        if (CriAtomServer._instance != this)
            return;
        CriAtomServer._instance = (CriAtomServer)null;
    }

    private void OnApplicationPause(bool appPause) => ProcessApplicationPause(appPause);

    private void ProcessApplicationPause(bool appPause) {
        if (onApplicationPausePreProcess != null)
            onApplicationPausePreProcess(appPause);
        CriAtomPlugin.Pause(appPause);
        if (onApplicationPausePostProcess == null)
            return;
        onApplicationPausePostProcess(appPause);
    }
}
