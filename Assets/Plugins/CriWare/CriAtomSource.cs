﻿using System;
using System.Collections;
using UnityEngine;

[AddComponentMenu("CRIWARE/CRI Atom Source")]
public class CriAtomSource : MonoBehaviour {
    protected CriAtomEx3dSource source;
    private Vector3 lastPosition;
    private bool hasValidPosition;
    [SerializeField]
    private bool _playOnStart;
    [SerializeField]
    private string _cueName = string.Empty;
    [SerializeField]
    private string _cueSheet = string.Empty;
    [SerializeField]
    private bool _use3dPositioning = true;
    [SerializeField]
    private bool _loop;
    [SerializeField]
    private float _volume = 1f;
    [SerializeField]
    private float _pitch;
    [SerializeField]
    private bool _androidUseLowLatencyVoicePool;
    [SerializeField]
    private bool need_to_player_update_all = true;

    public CriAtomExPlayer player { protected set; get; }

    public bool playOnStart {
        get => _playOnStart;
        set => _playOnStart = value;
    }

    public string cueName {
        get => _cueName;
        set => _cueName = value;
    }

    public string cueSheet {
        get => _cueSheet;
        set => _cueSheet = value;
    }

    public bool use3dPositioning {
        set {
            _use3dPositioning = value;
            if (player == null)
                return;
            player.Set3dSource(!use3dPositioning ? (CriAtomEx3dSource)null : source);
            SetNeedToPlayerUpdateAll();
        }
        get => _use3dPositioning;
    }

    public bool loop {
        set => _loop = value;
        get => _loop;
    }

    public float volume {
        set {
            _volume = value;
            if (player == null)
                return;
            player.SetVolume(_volume);
            SetNeedToPlayerUpdateAll();
        }
        get => _volume;
    }

    public float pitch {
        set {
            _pitch = value;
            if (player == null)
                return;
            player.SetPitch(_pitch);
            SetNeedToPlayerUpdateAll();
        }
        get => _pitch;
    }

    public float pan3dAngle {
        set {
            player.SetPan3dAngle(value);
            SetNeedToPlayerUpdateAll();
        }
        get => player.GetParameterFloat32(CriAtomEx.Parameter.Pan3dAngle);
    }

    public float pan3dDistance {
        set {
            player.SetPan3dInteriorDistance(value);
            SetNeedToPlayerUpdateAll();
        }
        get => player.GetParameterFloat32(CriAtomEx.Parameter.Pan3dDistance);
    }

    public int startTime {
        set {
            player.SetStartTime((long)value);
            SetNeedToPlayerUpdateAll();
        }
        get => player.GetParameterSint32(CriAtomEx.Parameter.StartTime);
    }

    public long time => player != null ? player.GetTime() : 0L;

    public CriAtomSource.Status status => player != null ? (CriAtomSource.Status)player.GetStatus() : CriAtomSource.Status.Error;

    public bool androidUseLowLatencyVoicePool {
        get => _androidUseLowLatencyVoicePool;
        set => _androidUseLowLatencyVoicePool = value;
    }

    protected void SetNeedToPlayerUpdateAll() => need_to_player_update_all = true;

    protected virtual void InternalInitialize() {
        CriAtomPlugin.InitializeLibrary();
        player = new CriAtomExPlayer();
        source = new CriAtomEx3dSource();
    }

    protected virtual void InternalFinalize() {
        player.Dispose();
        player = (CriAtomExPlayer)null;
        source.Dispose();
        source = (CriAtomEx3dSource)null;
        CriAtomPlugin.FinalizeLibrary();
    }

    private void Awake() => InternalInitialize();

    private void OnEnable() {
        hasValidPosition = false;
        SetInitialParameters();
        SetNeedToPlayerUpdateAll();
    }

    private void OnDestroy() => InternalFinalize();

    protected bool SetInitialSourcePosition() {
        Vector3 position = ((Component) this).transform.position;
        lastPosition = position;
        if (source == null)
            return false;
        source.SetPosition(position.x, position.y, position.z);
        source.Update();
        return true;
    }

    protected virtual void SetInitialParameters() {
        use3dPositioning = use3dPositioning;
        player.Set3dListener(CriAtomListener.sharedNativeListener);
        if (!SetInitialSourcePosition())
            Debug.LogError((object)"[ADX2][SetInitialParameters] source is null.", this);
        player.SetVolume(_volume);
        player.SetPitch(_pitch);
    }

    private void Start() => PlayOnStart();

    private void LateUpdate() {
        Vector3 position = ((Component) this).transform.position;
        Vector3 vector3 = (position - lastPosition) / Time.deltaTime;
        lastPosition = position;
        source.SetPosition(position.x, position.y, position.z);
        if (hasValidPosition)
            source.SetVelocity(vector3.x, vector3.y, vector3.z);
        source.Update();
        hasValidPosition = true;
        if (!need_to_player_update_all)
            return;
        player.UpdateAll();
        need_to_player_update_all = false;
    }

    public void OnDrawGizmos() {
        if (Application.isPlaying && status == CriAtomSource.Status.Playing)
            Gizmos.DrawIcon(((Component)this).transform.position, "Criware/VoiceOn.png");
        else
            Gizmos.DrawIcon(((Component)this).transform.position, "Criware/VoiceOff.png");
    }

    public CriAtomExPlayback Play() => Play(cueName);

    public CriAtomExPlayback Play(string cueName) {
        CriAtomExAcb acb = (CriAtomExAcb) null;
        if (!string.IsNullOrEmpty(cueSheet))
            acb = CriAtom.GetAcb(cueSheet);
        player.SetCue(acb, cueName);
        if (androidUseLowLatencyVoicePool)
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
        else
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
        if (!hasValidPosition) {
            SetInitialSourcePosition();
            hasValidPosition = true;
        }
        if (status == CriAtomSource.Status.Stop)
            player.Loop(_loop);
        return player.Start();
    }

    public CriAtomExPlayback Play(int cueId) {
        CriAtomExAcb acb = (CriAtomExAcb) null;
        if (!string.IsNullOrEmpty(cueSheet))
            acb = CriAtom.GetAcb(cueSheet);
        player.SetCue(acb, cueId);
        if (androidUseLowLatencyVoicePool)
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
        else
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
        if (!hasValidPosition) {
            SetInitialSourcePosition();
            hasValidPosition = true;
        }
        if (status == CriAtomSource.Status.Stop)
            player.Loop(_loop);
        return player.Start();
    }

    private void PlayOnStart() {
        if (!playOnStart || string.IsNullOrEmpty(cueName))
            return;
        StartCoroutine(PlayAsync(cueName));
    }

    private IEnumerator PlayAsync(string cueName) {
        CriAtomExAcb acb = null;
        while (acb == null) {
            if (!string.IsNullOrEmpty(cueSheet)) {
                acb = CriAtom.GetAcb(cueSheet);
            }
            if (acb == null) {
                yield return null;
            }
        }
        player.SetCue(acb, cueName);
        if (androidUseLowLatencyVoicePool) {
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
        } else {
            player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
        }
        if (!hasValidPosition) {
            SetInitialSourcePosition();
            hasValidPosition = true;
        }
        if (status == CriAtomSource.Status.Stop) {
            player.Loop(_loop);
        }
        player.Start();
    }

    public void Stop() => player.Stop();

    public void Pause(bool sw) {
        if (!sw)
            player.Resume(CriAtomEx.ResumeMode.PausedPlayback);
        else
            player.Pause();
    }

    public bool IsPaused() => player.IsPaused();

    public void SetBusSendLevel(string busName, float level) {
        if (player == null)
            return;
        player.SetBusSendLevel(busName, level);
        SetNeedToPlayerUpdateAll();
    }

    public void SetBusSendLevel(int busId, float level) {
        if (player == null)
            return;
        player.SetBusSendLevel(busId, level);
        SetNeedToPlayerUpdateAll();
    }

    public void SetBusSendLevelOffset(string busName, float levelOffset) {
        if (player == null)
            return;
        player.SetBusSendLevelOffset(busName, levelOffset);
        SetNeedToPlayerUpdateAll();
    }

    public void SetBusSendLevelOffset(int busId, float levelOffset) {
        if (player == null)
            return;
        player.SetBusSendLevelOffset(busId, levelOffset);
        SetNeedToPlayerUpdateAll();
    }

    public void SetAisacControl(string controlName, float value) {
        if (player == null)
            return;
        player.SetAisacControl(controlName, value);
        SetNeedToPlayerUpdateAll();
    }

    [Obsolete("Use CriAtomSource.SetAisacControl")]
    public void SetAisac(string controlName, float value) => SetAisacControl(controlName, value);

    public void SetAisacControl(uint controlId, float value) {
        if (player == null)
            return;
        player.SetAisacControl(controlId, value);
        SetNeedToPlayerUpdateAll();
    }

    [Obsolete("Use CriAtomSource.SetAisacControl")]
    public void SetAisac(uint controlId, float value) => SetAisacControl(controlId, value);

    public void AttachToAnalyzer(CriAtomExPlayerOutputAnalyzer analyzer) {
        if (player == null)
            return;
        analyzer.AttachExPlayer(player);
    }

    public void DetachFromAnalyzer(CriAtomExPlayerOutputAnalyzer analyzer) => analyzer.DetachExPlayer();

    public enum Status {
        Stop,
        Prep,
        Playing,
        PlayEnd,
        Error,
    }
}
