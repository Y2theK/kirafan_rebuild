﻿using System;
using System.Collections.Generic;

public static class CriDisposableObjectManager {
    private static List<IDisposable> disposables = new List<IDisposable>();
    private static List<CriDisposableObjectManager.ModuleType> types = new List<CriDisposableObjectManager.ModuleType>();

    public static void Register(IDisposable disposable, CriDisposableObjectManager.ModuleType type) {
        if (CriDisposableObjectManager.disposables.Contains(disposable))
            return;
        CriDisposableObjectManager.disposables.Add(disposable);
        CriDisposableObjectManager.types.Add(type);
    }

    public static bool Unregister(IDisposable disposable) {
        int index = CriDisposableObjectManager.disposables.IndexOf(disposable);
        bool flag = index >= 0;
        if (flag) {
            CriDisposableObjectManager.disposables.RemoveAt(index);
            CriDisposableObjectManager.types.RemoveAt(index);
        }
        return flag;
    }

    public static bool IsDisposed(IDisposable disposable) => !CriDisposableObjectManager.disposables.Contains(disposable);

    public static void CallOnModuleFinalization(CriDisposableObjectManager.ModuleType type) => CriDisposableObjectManager.DisposeAll(type);

    private static int GetLastIndex(CriDisposableObjectManager.ModuleType type) {
        for (int index = CriDisposableObjectManager.disposables.Count - 1; index >= 0; --index) {
            if (CriDisposableObjectManager.types[index] == type)
                return index;
        }
        return -1;
    }

    public static void DisposeAll(CriDisposableObjectManager.ModuleType type) {
        while (true) {
            int lastIndex = CriDisposableObjectManager.GetLastIndex(type);
            if (lastIndex >= 0)
                CriDisposableObjectManager.disposables[lastIndex].Dispose();
            else
                break;
        }
    }

    public enum ModuleType {
        Atom,
        Fs,
        FsWeb,
        Mana,
    }
}
