﻿using System;

public class CriFsBindRequest : CriFsRequest {
    public CriFsBindRequest(
      CriFsBindRequest.BindType type,
      CriFsBinder targetBinder,
      CriFsBinder srcBinder,
      string path) {
        this.path = path;
        switch (type) {
            case CriFsBindRequest.BindType.Cpk:
                bindId = targetBinder.BindCpk(srcBinder, path);
                break;
            case CriFsBindRequest.BindType.Directory:
                bindId = targetBinder.BindDirectory(srcBinder, path);
                break;
            case CriFsBindRequest.BindType.File:
                bindId = targetBinder.BindFile(srcBinder, path);
                break;
            default:
                throw new Exception("Invalid bind type.");
        }
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public string path { get; private set; }

    public uint bindId { get; private set; }

    public override void Stop() {
    }

    public override void Update() {
        if (isDone)
            return;
        switch (CriFsBinder.GetStatus(bindId)) {
            case CriFsBinder.Status.Analyze:
                return;
            case CriFsBinder.Status.Error:
                error = "Error occurred.";
                break;
        }
        Done();
    }

    protected override void Dispose(bool disposing) => CriDisposableObjectManager.Unregister((IDisposable)this);

    public enum BindType {
        Cpk,
        Directory,
        File,
    }
}
