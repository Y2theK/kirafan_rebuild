﻿using System;
using System.Runtime.InteropServices;

public class CriFsBinder : IDisposable {
    private IntPtr handle;

    public CriFsBinder() {
        if (!CriFsPlugin.isInitialized)
            throw new Exception("CriFsPlugin is not initialized.");
        handle = IntPtr.Zero;
        int num = (int) CriFsBinder.criFsBinder_Create(out handle);
        if (handle == IntPtr.Zero)
            throw new Exception("criFsBinder_Create() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            int num = (int) CriFsBinder.criFsBinder_Destroy(handle);
            handle = IntPtr.Zero;
        }
        GC.SuppressFinalize((object)this);
    }

    public uint BindCpk(CriFsBinder srcBinder, string path) {
        uint bindId = 0;
        if (handle != IntPtr.Zero) {
            int num = (int) CriFsBinder.criFsBinder_BindCpk(handle, srcBinder == null ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out bindId);
        }
        return bindId;
    }

    public uint BindDirectory(CriFsBinder srcBinder, string path) {
        uint bindId = 0;
        if (handle != IntPtr.Zero) {
            int num = (int) CriFsBinder.criFsBinder_BindDirectory(handle, srcBinder == null ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out bindId);
        }
        return bindId;
    }

    public uint BindFile(CriFsBinder srcBinder, string path) {
        uint bindId = 0;
        int num = (int) CriFsBinder.criFsBinder_BindFile(handle, srcBinder == null ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out bindId);
        return bindId;
    }

    public uint BindFileSection(
      CriFsBinder srcBinder,
      string path,
      ulong offset,
      int size,
      string sectionName) {
        uint bindId = 0;
        if (handle != IntPtr.Zero) {
            int num = (int) CriFsBinder.criFsBinder_BindFileSection(handle, srcBinder == null ? IntPtr.Zero : srcBinder.nativeHandle, path, offset, size, sectionName, IntPtr.Zero, 0, out bindId);
        }
        return bindId;
    }

    public static void Unbind(uint bindId) {
        if (!CriFsPlugin.isInitialized)
            return;
        CriFsBinder.criFsBinder_Unbind(bindId);
    }

    public static CriFsBinder.Status GetStatus(uint bindId) {
        CriFsBinder.Status status = CriFsBinder.Status.Removed;
        if (CriFsPlugin.isInitialized)
            CriFsBinder.criFsBinder_GetStatus(bindId, out status);
        return status;
    }

    public long GetFileSize(string path) {
        long size = -1;
        return handle != IntPtr.Zero && CriFsBinder.criFsBinder_GetFileSize(handle, path, out size) != 0 ? -1L : size;
    }

    public long GetFileSize(int id) {
        long size = -1;
        return handle != IntPtr.Zero && CriFsBinder.criFsBinder_GetFileSizeById(handle, id, out size) != 0 ? -1L : size;
    }

    public static void SetPriority(uint bindId, int priority) {
        if (!CriFsPlugin.isInitialized)
            return;
        CriFsBinder.criFsBinder_SetPriority(bindId, priority);
    }

    public IntPtr nativeHandle => handle;

    ~CriFsBinder() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_Create(out IntPtr binder);

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_Destroy(IntPtr binder);

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_BindCpk(
      IntPtr binder,
      IntPtr srcBinder,
      string path,
      IntPtr work,
      int worksize,
      out uint bindId);

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_BindDirectory(
      IntPtr binder,
      IntPtr srcBinder,
      string path,
      IntPtr work,
      int worksize,
      out uint bindId);

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_BindFile(
      IntPtr binder,
      IntPtr srcBinder,
      string path,
      IntPtr work,
      int worksize,
      out uint bindId);

    [DllImport("cri_ware_unity")]
    private static extern uint criFsBinder_BindFileSection(
      IntPtr binder,
      IntPtr srcBinder,
      string path,
      ulong offset,
      int size,
      string sectionName,
      IntPtr work,
      int worksize,
      out uint bindId);

    [DllImport("cri_ware_unity")]
    private static extern int criFsBinder_Unbind(uint bindId);

    [DllImport("cri_ware_unity")]
    private static extern int criFsBinder_GetStatus(uint bindId, out CriFsBinder.Status status);

    [DllImport("cri_ware_unity")]
    private static extern int criFsBinder_GetFileSize(IntPtr binder, string path, out long size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsBinder_GetFileSizeById(IntPtr binder, int id, out long size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsBinder_SetPriority(uint bindId, int priority);

    public enum Status {
        None,
        Analyze,
        Complete,
        Unbind,
        Removed,
        Invalid,
        Error,
    }
}
