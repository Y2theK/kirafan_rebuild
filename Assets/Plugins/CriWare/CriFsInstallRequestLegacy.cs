﻿using System;

public class CriFsInstallRequestLegacy : CriFsInstallRequest {
    private CriFsInstaller installer;

    public CriFsInstallRequestLegacy(
      CriFsBinder srcBinder,
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDelegate,
      int installBufferSize) {
        sourcePath = srcPath;
        destinationPath = dstPath;
        this.doneDelegate = doneDelegate;
        progress = 0.0f;
        installer = new CriFsInstaller();
        installer.Copy(srcBinder, srcPath, dstPath, installBufferSize);
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public override void Stop() {
        if (installer == null)
            return;
        installer.Stop();
    }

    public override void Update() {
        if (installer == null)
            return;
        progress = installer.GetProgress();
        switch (installer.GetStatus()) {
            case CriFsInstaller.Status.Busy:
                return;
            case CriFsInstaller.Status.Error:
                progress = -1f;
                error = "Error occurred.";
                break;
        }
        installer.Dispose();
        installer = (CriFsInstaller)null;
        Done();
    }

    protected override void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (installer == null)
            return;
        installer.Dispose();
        installer = (CriFsInstaller)null;
    }
}
