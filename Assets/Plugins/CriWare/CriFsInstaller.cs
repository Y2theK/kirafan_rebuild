﻿using System;
using System.Runtime.InteropServices;

public class CriFsInstaller : IDisposable {
    private byte[] installBuffer;
    private GCHandle installBufferGch;
    private IntPtr handle;

    public CriFsInstaller() {
        if (!CriFsPlugin.isInitialized)
            throw new Exception("CriFsPlugin is not initialized.");
        handle = IntPtr.Zero;
        CriFsInstaller.criFsInstaller_Create(out handle, CriFsInstaller.CopyPolicy.Always);
        if (handle == IntPtr.Zero)
            throw new Exception("criFsInstaller_Create() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            CriFsInstaller.criFsInstaller_Destroy(handle);
            handle = IntPtr.Zero;
        }
        if (installBuffer != null) {
            installBufferGch.Free();
            installBuffer = (byte[])null;
        }
        GC.SuppressFinalize((object)this);
    }

    public void Copy(CriFsBinder binder, string srcPath, string dstPath, int installBufferSize) {
        string src_path = srcPath;
        if (src_path.StartsWith("http:") || src_path.StartsWith("https:"))
            src_path = "net2:" + src_path;
        if (installBufferSize > 0) {
            installBuffer = new byte[installBufferSize];
            installBufferGch = GCHandle.Alloc((object)installBuffer, GCHandleType.Pinned);
            CriFsInstaller.criFsInstaller_Copy(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, src_path, dstPath, installBufferGch.AddrOfPinnedObject(), (long)installBuffer.Length);
        } else
            CriFsInstaller.criFsInstaller_Copy(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, src_path, dstPath, IntPtr.Zero, 0L);
    }

    public void Stop() {
        if (!(handle != IntPtr.Zero))
            return;
        CriFsInstaller.criFsInstaller_Stop(handle);
    }

    public CriFsInstaller.Status GetStatus() {
        CriFsInstaller.Status status = CriFsInstaller.Status.Stop;
        if (handle != IntPtr.Zero)
            CriFsInstaller.criFsInstaller_GetStatus(handle, out status);
        return status;
    }

    public float GetProgress() {
        float progress = 0.0f;
        if (handle != IntPtr.Zero)
            CriFsInstaller.criFsInstaller_GetProgress(handle, out progress);
        return progress;
    }

    public static void ExecuteMain() => CriFsInstaller.criFsInstaller_ExecuteMain();

    ~CriFsInstaller() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern void criFsInstaller_ExecuteMain();

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_Create(
      out IntPtr installer,
      CriFsInstaller.CopyPolicy option);

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_Destroy(IntPtr installer);

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_Copy(
      IntPtr installer,
      IntPtr binder,
      string src_path,
      string dst_path,
      IntPtr buffer,
      long buffer_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_Stop(IntPtr installer);

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_GetStatus(
      IntPtr installer,
      out CriFsInstaller.Status status);

    [DllImport("cri_ware_unity")]
    private static extern int criFsInstaller_GetProgress(IntPtr installer, out float progress);

    public enum Status {
        Stop,
        Busy,
        Complete,
        Error,
    }

    private enum CopyPolicy {
        Always,
    }
}
