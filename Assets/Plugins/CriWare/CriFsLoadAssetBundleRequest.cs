﻿using System;
using UnityEngine;

public class CriFsLoadAssetBundleRequest : CriFsRequest {
    private CriFsLoadFileRequest loadFileReq;
    private AssetBundleCreateRequest assetBundleReq;

    public CriFsLoadAssetBundleRequest(CriFsBinder binder, string path, int readUnitSize) {
        this.path = path;
        loadFileReq = CriFsUtility.LoadFile(binder, path, readUnitSize);
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public string path { get; private set; }

    public AssetBundle assetBundle { get; private set; }

    public override void Update() {
        if (loadFileReq != null) {
            if (!loadFileReq.isDone)
                return;
            if (loadFileReq.error != null) {
                error = "Error occurred.";
                Done();
            } else
                assetBundleReq = AssetBundle.LoadFromMemoryAsync(loadFileReq.bytes);
            loadFileReq = (CriFsLoadFileRequest)null;
        } else if (assetBundleReq != null) {
            if (!((AsyncOperation)assetBundleReq).isDone)
                return;
            assetBundle = assetBundleReq.assetBundle;
            Done();
        } else
            Done();
    }

    protected override void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (loadFileReq == null)
            return;
        loadFileReq.Dispose();
        loadFileReq = (CriFsLoadFileRequest)null;
    }
}
