﻿using System;

public class CriFsLoadFileRequest : CriFsRequest {
    private CriFsLoadFileRequest.Phase phase;
    private CriFsBinder refBinder;
    private CriFsBinder newBinder;
    private uint bindId;
    private CriFsLoader loader;
    private int readUnitSize;
    private long fileSize;

    public CriFsLoadFileRequest(
      CriFsBinder srcBinder,
      string path,
      CriFsRequest.DoneDelegate doneDelegate,
      int readUnitSize) {
        this.path = path;
        this.doneDelegate = doneDelegate;
        this.readUnitSize = readUnitSize;
        if (srcBinder == null) {
            newBinder = new CriFsBinder();
            refBinder = newBinder;
            bindId = newBinder.BindFile(srcBinder, path);
            phase = CriFsLoadFileRequest.Phase.Bind;
        } else {
            newBinder = (CriFsBinder)null;
            refBinder = srcBinder;
            fileSize = srcBinder.GetFileSize(path);
            phase = fileSize >= 0L ? CriFsLoadFileRequest.Phase.Load : CriFsLoadFileRequest.Phase.Error;
        }
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public string path { get; private set; }

    public byte[] bytes { get; private set; }

    protected override void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (loader != null) {
            loader.Dispose();
            loader = (CriFsLoader)null;
        }
        if (newBinder != null) {
            newBinder.Dispose();
            newBinder = (CriFsBinder)null;
        }
        bytes = (byte[])null;
    }

    public override void Stop() {
        if (phase != CriFsLoadFileRequest.Phase.Load || loader == null)
            return;
        loader.Stop();
    }

    public override void Update() {
        if (phase == CriFsLoadFileRequest.Phase.Bind)
            UpdateBinder();
        if (phase == CriFsLoadFileRequest.Phase.Load)
            UpdateLoader();
        if (phase != CriFsLoadFileRequest.Phase.Error)
            return;
        OnError();
    }

    private void UpdateBinder() {
        switch (CriFsBinder.GetStatus(bindId)) {
            case CriFsBinder.Status.Analyze:
                return;
            case CriFsBinder.Status.Complete:
                fileSize = refBinder.GetFileSize(path);
                break;
            default:
                fileSize = -1L;
                break;
        }
        if (fileSize < 0L)
            phase = CriFsLoadFileRequest.Phase.Error;
        else
            phase = CriFsLoadFileRequest.Phase.Load;
    }

    private void UpdateLoader() {
        if (loader == null) {
            loader = new CriFsLoader();
            loader.SetReadUnitSize(readUnitSize);
            bytes = new byte[fileSize];
            loader.Load(refBinder, path, 0L, fileSize, bytes);
        }
        switch (loader.GetStatus()) {
            case CriFsLoader.Status.Stop:
                bytes = (byte[])null;
                break;
            case CriFsLoader.Status.Loading:
                return;
            case CriFsLoader.Status.Error:
                phase = CriFsLoadFileRequest.Phase.Error;
                return;
        }
        phase = CriFsLoadFileRequest.Phase.Done;
        loader.Dispose();
        loader = (CriFsLoader)null;
        if (newBinder != null) {
            newBinder.Dispose();
            newBinder = (CriFsBinder)null;
        }
        Done();
    }

    private void OnError() {
        bytes = (byte[])null;
        error = "Error occurred.";
        refBinder = (CriFsBinder)null;
        if (newBinder != null) {
            newBinder.Dispose();
            newBinder = (CriFsBinder)null;
        }
        if (loader != null) {
            loader.Dispose();
            loader = (CriFsLoader)null;
        }
        phase = CriFsLoadFileRequest.Phase.Done;
        Done();
    }

    private enum Phase {
        Stop,
        Bind,
        Load,
        Done,
        Error,
    }
}
