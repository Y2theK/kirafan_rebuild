﻿using System;
using System.Runtime.InteropServices;

public class CriFsLoader : IDisposable {
    private IntPtr handle;
    private GCHandle dstGch;
    private GCHandle srcGch;

    public CriFsLoader() {
        if (!CriFsPlugin.isInitialized)
            throw new Exception("CriFsPlugin is not initialized.");
        handle = IntPtr.Zero;
        CriFsLoader.criFsLoader_Create(out handle);
        if (handle == IntPtr.Zero)
            throw new Exception("criFsLoader_Create() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public void Dispose() {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (handle != IntPtr.Zero) {
            CriFsLoader.criFsLoader_Destroy(handle);
            handle = IntPtr.Zero;
        }
        if (dstGch.IsAllocated)
            dstGch.Free();
        if (srcGch.IsAllocated)
            srcGch.Free();
        GC.SuppressFinalize((object)this);
    }

    public void Load(
      CriFsBinder binder,
      string path,
      long fileOffset,
      long loadSize,
      byte[] buffer) {
        dstGch = GCHandle.Alloc((object)buffer, GCHandleType.Pinned);
        CriFsLoader.criFsLoader_Load(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, path, fileOffset, loadSize, dstGch.AddrOfPinnedObject(), (long)buffer.Length);
    }

    public void LoadById(CriFsBinder binder, int id, long fileOffset, long loadSize, byte[] buffer) {
        dstGch = GCHandle.Alloc((object)buffer, GCHandleType.Pinned);
        CriFsLoader.criFsLoader_LoadById(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, id, fileOffset, loadSize, dstGch.AddrOfPinnedObject(), (long)buffer.Length);
    }

    public void LoadWithoutDecompression(
      CriFsBinder binder,
      string path,
      long fileOffset,
      long loadSize,
      byte[] buffer) {
        dstGch = GCHandle.Alloc((object)buffer, GCHandleType.Pinned);
        CriFsLoader.criFsLoader_LoadWithoutDecompression(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, path, fileOffset, loadSize, dstGch.AddrOfPinnedObject(), (long)buffer.Length);
    }

    public void LoadWithoutDecompressionById(
      CriFsBinder binder,
      int id,
      long fileOffset,
      long loadSize,
      byte[] buffer) {
        dstGch = GCHandle.Alloc((object)buffer, GCHandleType.Pinned);
        CriFsLoader.criFsLoader_LoadWithoutDecompressionById(handle, binder == null ? IntPtr.Zero : binder.nativeHandle, id, fileOffset, loadSize, dstGch.AddrOfPinnedObject(), (long)buffer.Length);
    }

    public void DecompressData(long srcSize, byte[] srcBuffer, long dstSize, byte[] dstBuffer) {
        srcGch = GCHandle.Alloc((object)srcBuffer, GCHandleType.Pinned);
        dstGch = GCHandle.Alloc((object)dstBuffer, GCHandleType.Pinned);
        CriFsLoader.criFsLoader_DecompressData(handle, srcGch.AddrOfPinnedObject(), srcSize, dstGch.AddrOfPinnedObject(), dstSize);
    }

    public void Stop() {
        if (!(handle != IntPtr.Zero))
            return;
        CriFsLoader.criFsLoader_Stop(handle);
    }

    public CriFsLoader.Status GetStatus() {
        CriFsLoader.Status status = CriFsLoader.Status.Stop;
        if (handle != IntPtr.Zero)
            CriFsLoader.criFsLoader_GetStatus(handle, out status);
        if (status != CriFsLoader.Status.Loading) {
            if (dstGch.IsAllocated)
                dstGch.Free();
            if (srcGch.IsAllocated)
                srcGch.Free();
        }
        return status;
    }

    public void SetReadUnitSize(int unit_size) {
        if (!(handle != IntPtr.Zero))
            return;
        CriFsLoader.criFsLoader_SetReadUnitSize(handle, (long)unit_size);
    }

    ~CriFsLoader() => Dispose();

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_Create(out IntPtr loader);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_Destroy(IntPtr loader);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_Load(
      IntPtr loader,
      IntPtr binder,
      string path,
      long offset,
      long load_size,
      IntPtr buffer,
      long buffer_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_LoadById(
      IntPtr loader,
      IntPtr binder,
      int id,
      long offset,
      long load_size,
      IntPtr buffer,
      long buffer_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_Stop(IntPtr loader);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_GetStatus(IntPtr loader, out CriFsLoader.Status status);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_SetReadUnitSize(IntPtr loader, long unit_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_LoadWithoutDecompression(
      IntPtr loader,
      IntPtr binder,
      string path,
      long offset,
      long load_size,
      IntPtr buffer,
      long buffer_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_LoadWithoutDecompressionById(
      IntPtr loader,
      IntPtr binder,
      int id,
      long offset,
      long load_size,
      IntPtr buffer,
      long buffer_size);

    [DllImport("cri_ware_unity")]
    private static extern int criFsLoader_DecompressData(
      IntPtr loader,
      IntPtr src,
      long src_size,
      IntPtr dst,
      long dst_size);

    public enum Status {
        Stop,
        Loading,
        Complete,
        Error,
    }
}
