﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class CriFsRequest : IDisposable {
    public CriFsRequest.DoneDelegate doneDelegate { get; protected set; }

    public bool isDone { get; private set; }

    public string error { get; protected set; }

    public bool isDisposed { get; protected set; }

    public void Dispose() {
        if (isDisposed)
            return;
        Dispose(true);
        isDisposed = true;
        GC.SuppressFinalize((object)this);
    }

    public virtual void Stop() {
    }

    public YieldInstruction WaitForDone(MonoBehaviour mb) => (YieldInstruction)mb.StartCoroutine(CheckDone());

    protected virtual void Dispose(bool disposing) {
    }

    public virtual void Update() {
    }

    protected void Done() {
        isDone = true;
        if (doneDelegate == null)
            return;
        doneDelegate(this);
    }

    [DebuggerHidden]
    private IEnumerator CheckDone() {
        while (!isDone) {
            yield return null;
        }
    }

    ~CriFsRequest() {
        if (isDisposed)
            return;
        Dispose(false);
        isDisposed = true;
    }

    public delegate void DoneDelegate(CriFsRequest request);
}
