﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CriFsServer : MonoBehaviour {
    private static CriFsServer _instance;
    private List<CriFsRequest> requestList;

    public static CriFsServer instance {
        get {
            CriFsServer.CreateInstance();
            return CriFsServer._instance;
        }
    }

    public int installBufferSize { get; private set; }

    public static void CreateInstance() {
        if (CriFsServer._instance != null)
            return;
        CriWare.managerObject.AddComponent<CriFsServer>();
        CriFsServer._instance.installBufferSize = CriFsPlugin.installBufferSize;
    }

    public static void DestroyInstance() {
        if (CriFsServer._instance == null)
            return;
        Destroy(CriFsServer._instance);
    }

    private void Awake() {
        if (CriFsServer._instance == null) {
            CriFsServer._instance = this;
            requestList = new List<CriFsRequest>();
            requestList.Add(new CriFsRequest());
            requestList.RemoveAt(0);
        } else
            Destroy(this);
    }

    private void OnDestroy() {
        if (CriFsServer._instance != this)
            return;
        foreach (CriFsRequest request in requestList)
            request.Dispose();
        CriFsServer._instance = (CriFsServer)null;
    }

    private void Update() {
        CriFsInstaller.ExecuteMain();
        if (CriFsWebInstaller.isInitialized)
            CriFsWebInstaller.ExecuteMain();
        for (int index = 0; index < requestList.Count; ++index)
            requestList[index].Update();
        requestList.RemoveAll((Predicate<CriFsRequest>)(request => request.isDone || request.isDisposed));
    }

    public void AddRequest(CriFsRequest request) => requestList.Add(request);

    public CriFsLoadFileRequest LoadFile(
      CriFsBinder binder,
      string path,
      CriFsRequest.DoneDelegate doneDelegate,
      int readUnitSize) {
        CriFsLoadFileRequest request = new CriFsLoadFileRequest(binder, path, doneDelegate, readUnitSize);
        AddRequest((CriFsRequest)request);
        return request;
    }

    public CriFsLoadAssetBundleRequest LoadAssetBundle(
      CriFsBinder binder,
      string path,
      int readUnitSize) {
        CriFsLoadAssetBundleRequest request = new CriFsLoadAssetBundleRequest(binder, path, readUnitSize);
        AddRequest((CriFsRequest)request);
        return request;
    }

    public CriFsInstallRequest Install(
      CriFsBinder srcBinder,
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDelegate) {
        CriFsInstallRequestLegacy installRequestLegacy = new CriFsInstallRequestLegacy(srcBinder, srcPath, dstPath, doneDelegate, installBufferSize);
        requestList.Add((CriFsRequest)installRequestLegacy);
        return (CriFsInstallRequest)installRequestLegacy;
    }

    public CriFsInstallRequest WebInstall(
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDelegate) {
        CriFsWebInstallRequest webInstallRequest = new CriFsWebInstallRequest(srcPath, dstPath, doneDelegate);
        requestList.Add((CriFsRequest)webInstallRequest);
        return (CriFsInstallRequest)webInstallRequest;
    }

    public CriFsBindRequest BindCpk(CriFsBinder targetBinder, CriFsBinder srcBinder, string path) {
        CriFsBindRequest request = new CriFsBindRequest(CriFsBindRequest.BindType.Cpk, targetBinder, srcBinder, path);
        AddRequest((CriFsRequest)request);
        return request;
    }

    public CriFsBindRequest BindDirectory(
      CriFsBinder targetBinder,
      CriFsBinder srcBinder,
      string path) {
        CriFsBindRequest request = new CriFsBindRequest(CriFsBindRequest.BindType.Directory, targetBinder, srcBinder, path);
        AddRequest((CriFsRequest)request);
        return request;
    }

    public CriFsBindRequest BindFile(CriFsBinder targetBinder, CriFsBinder srcBinder, string path) {
        CriFsBindRequest request = new CriFsBindRequest(CriFsBindRequest.BindType.File, targetBinder, srcBinder, path);
        AddRequest((CriFsRequest)request);
        return request;
    }
}
