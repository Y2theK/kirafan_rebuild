﻿using System.Runtime.InteropServices;

public static class CriFsUtility {
    public const int DefaultReadUnitSize = 1048576;

    public static CriFsLoadFileRequest LoadFile(string path, int readUnitSize = 1048576) => CriFsServer.instance.LoadFile((CriFsBinder)null, path, (CriFsRequest.DoneDelegate)null, readUnitSize);

    public static CriFsLoadFileRequest LoadFile(
      string path,
      CriFsRequest.DoneDelegate doneDelegate,
      int readUnitSize = 1048576) {
        return CriFsServer.instance.LoadFile((CriFsBinder)null, path, doneDelegate, readUnitSize);
    }

    public static CriFsLoadFileRequest LoadFile(CriFsBinder binder, string path, int readUnitSize = 1048576) => CriFsServer.instance.LoadFile(binder, path, (CriFsRequest.DoneDelegate)null, readUnitSize);

    public static CriFsLoadAssetBundleRequest LoadAssetBundle(string path, int readUnitSize = 1048576) => CriFsUtility.LoadAssetBundle((CriFsBinder)null, path, readUnitSize);

    public static CriFsLoadAssetBundleRequest LoadAssetBundle(
      CriFsBinder binder,
      string path,
      int readUnitSize = 1048576) {
        return CriFsServer.instance.LoadAssetBundle(binder, path, readUnitSize);
    }

    public static CriFsInstallRequest Install(string srcPath, string dstPath) => CriFsUtility.Install((CriFsBinder)null, srcPath, dstPath, (CriFsRequest.DoneDelegate)null);

    public static CriFsInstallRequest Install(
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDeleagate) {
        return CriFsUtility.Install((CriFsBinder)null, srcPath, dstPath, doneDeleagate);
    }

    public static CriFsInstallRequest Install(CriFsBinder srcBinder, string srcPath, string dstPath) => CriFsServer.instance.Install(srcBinder, srcPath, dstPath, (CriFsRequest.DoneDelegate)null);

    public static CriFsInstallRequest Install(
      CriFsBinder srcBinder,
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDeleagate) {
        return CriFsServer.instance.Install(srcBinder, srcPath, dstPath, doneDeleagate);
    }

    public static CriFsInstallRequest WebInstall(
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDeleagate) {
        return CriFsServer.instance.WebInstall(srcPath, dstPath, doneDeleagate);
    }

    public static CriFsBindRequest BindCpk(CriFsBinder targetBinder, string srcPath) => CriFsUtility.BindCpk(targetBinder, (CriFsBinder)null, srcPath);

    public static CriFsBindRequest BindCpk(
      CriFsBinder targetBinder,
      CriFsBinder srcBinder,
      string srcPath) {
        return CriFsServer.instance.BindCpk(targetBinder, srcBinder, srcPath);
    }

    public static CriFsBindRequest BindDirectory(CriFsBinder targetBinder, string srcPath) => CriFsServer.instance.BindDirectory(targetBinder, (CriFsBinder)null, srcPath);

    public static CriFsBindRequest BindDirectory(
      CriFsBinder targetBinder,
      CriFsBinder srcBinder,
      string srcPath) {
        return CriFsServer.instance.BindDirectory(targetBinder, srcBinder, srcPath);
    }

    public static CriFsBindRequest BindFile(CriFsBinder targetBinder, string srcPath) => CriFsServer.instance.BindFile(targetBinder, (CriFsBinder)null, srcPath);

    public static CriFsBindRequest BindFile(
      CriFsBinder targetBinder,
      CriFsBinder srcBinder,
      string srcPath) {
        return CriFsServer.instance.BindFile(targetBinder, srcBinder, srcPath);
    }

    public static void SetUserAgentString(string userAgentString) => CriFsUtility.criFsUnity_SetUserAgentString(userAgentString);

    public static void SetProxyServer(string proxyPath, ushort proxyPort) => CriFsUtility.criFsUnity_SetProxyServer(proxyPath, proxyPort);

    public static void SetPathSeparator(string filter) => CriFsUtility.criFsUnity_SetPathSeparator(filter);

    [DllImport("cri_ware_unity")]
    private static extern bool criFsUnity_SetUserAgentString(string userAgentString);

    [DllImport("cri_ware_unity")]
    private static extern bool criFsUnity_SetProxyServer(string proxyPath, ushort proxyPort);

    [DllImport("cri_ware_unity")]
    private static extern bool criFsUnity_SetPathSeparator(string filter);
}
