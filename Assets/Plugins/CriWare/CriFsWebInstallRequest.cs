﻿using System;
using System.IO;

public class CriFsWebInstallRequest : CriFsInstallRequest {
    private CriFsWebInstaller installer;

    public CriFsWebInstallRequest(
      string srcPath,
      string dstPath,
      CriFsRequest.DoneDelegate doneDelegate) {
        sourcePath = srcPath;
        destinationPath = dstPath;
        this.doneDelegate = doneDelegate;
        progress = 0.0f;
        installer = new CriFsWebInstaller();
        File.Delete(dstPath);
        installer.Copy(srcPath, dstPath);
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Fs);
    }

    public override void Stop() {
        if (installer == null)
            return;
        installer.Stop();
    }

    public override void Update() {
        if (installer == null)
            return;
        CriFsWebInstaller.StatusInfo statusInfo = installer.GetStatusInfo();
        progress = (float)statusInfo.receivedSize / (statusInfo.contentsSize <= 0L ? 1f : (float)statusInfo.contentsSize);
        if (statusInfo.status == CriFsWebInstaller.Status.Busy)
            return;
        if (statusInfo.status == CriFsWebInstaller.Status.Error) {
            progress = -1f;
            error = "[CriFsWebInstallerError]" + statusInfo.error.ToString();
        }
        installer.Dispose();
        installer = (CriFsWebInstaller)null;
        Done();
    }

    protected override void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (installer == null)
            return;
        installer.Dispose();
        installer = (CriFsWebInstaller)null;
    }
}
