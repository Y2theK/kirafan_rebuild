﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

public class CriFsWebInstaller : IDisposable {
    public const int InvalidHttpStatusCode = -1;
    public const long InvalidContentsSize = -1;
    private IntPtr handle = IntPtr.Zero;

    public CriFsWebInstaller() {
        CriFsWebInstaller.criFsWebInstaller_Create(out handle, IntPtr.Zero);
        if (handle == IntPtr.Zero)
            throw new Exception("criFsWebInstaller_Create() failed.");
        CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.FsWeb);
    }

    public static bool isInitialized { get; private set; }

    public static CriFsWebInstaller.ModuleConfig defaultModuleConfig {
        get {
            CriFsWebInstaller.ModuleConfig defaultModuleConfig;
            defaultModuleConfig.numInstallers = 2U;
            defaultModuleConfig.proxyHost = (string)null;
            defaultModuleConfig.proxyPort = (ushort)0;
            defaultModuleConfig.userAgent = (string)null;
            defaultModuleConfig.inactiveTimeoutSec = 300U;
            defaultModuleConfig.allowInsecureSSL = false;
            defaultModuleConfig.platformConfig = CriFsWebInstaller.ModulePlatformConfig.defaultConfig;
            return defaultModuleConfig;
        }
    }

    ~CriFsWebInstaller() => Dispose(false);

    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize((object)this);
    }

    public void Copy(string url, string dstPath) => CriFsWebInstaller.criFsWebInstaller_Copy(handle, url, dstPath);

    public void Stop() {
        if (!(handle != IntPtr.Zero))
            return;
        CriFsWebInstaller.criFsWebInstaller_Stop(handle);
    }

    public CriFsWebInstaller.StatusInfo GetStatusInfo() {
        CriFsWebInstaller.StatusInfo status;
        if (handle != IntPtr.Zero) {
            CriFsWebInstaller.criFsWebInstaller_GetStatusInfo(handle, out status);
        } else {
            status.status = CriFsWebInstaller.Status.Stop;
            status.error = CriFsWebInstaller.Error.Internal;
            status.httpStatusCode = -1;
            status.contentsSize = -1L;
            status.receivedSize = 0L;
        }
        return status;
    }

    public static void InitializeModule(CriFsWebInstaller.ModuleConfig config) {
        if (CriFsWebInstaller.isInitialized) {
            Debug.LogError((object)"[CRIWARE] CriFsWebInstaller module is already initialized.");
        } else {
            CriFsPlugin.InitializeLibrary();
            CriFsWebInstaller.criFsWebInstaller_Initialize(ref config);
            CriFsWebInstaller.isInitialized = true;
        }
    }

    public static void FinalizeModule() {
        if (!CriFsWebInstaller.isInitialized) {
            Debug.LogError((object)"[CRIWARE] CriFsWebInstaller module is not initialized.");
        } else {
            CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.FsWeb);
            CriFsWebInstaller.criFsWebInstaller_Finalize();
            CriFsPlugin.FinalizeLibrary();
            CriFsWebInstaller.isInitialized = false;
        }
    }

    public static void ExecuteMain() => CriFsWebInstaller.criFsWebInstaller_ExecuteMain();

    private void Dispose(bool disposing) {
        CriDisposableObjectManager.Unregister((IDisposable)this);
        if (!(handle != IntPtr.Zero))
            return;
        CriFsWebInstaller.StatusInfo statusInfo = GetStatusInfo();
        if (statusInfo.status != CriFsWebInstaller.Status.Stop) {
            Stop();
            while (true) {
                CriFsWebInstaller.ExecuteMain();
                statusInfo = GetStatusInfo();
                if (statusInfo.status != CriFsWebInstaller.Status.Stop)
                    Thread.Sleep(1);
                else
                    break;
            }
        }
        CriFsWebInstaller.criFsWebInstaller_Destroy(handle);
        handle = IntPtr.Zero;
    }

    [DllImport("cri_ware_unity")]
    private static extern void criFsWebInstaller_Initialize([In] ref CriFsWebInstaller.ModuleConfig config);

    [DllImport("cri_ware_unity")]
    private static extern void criFsWebInstaller_Finalize();

    [DllImport("cri_ware_unity")]
    private static extern void criFsWebInstaller_ExecuteMain();

    [DllImport("cri_ware_unity")]
    private static extern int criFsWebInstaller_Create(out IntPtr installer, IntPtr option);

    [DllImport("cri_ware_unity")]
    private static extern int criFsWebInstaller_Destroy(IntPtr installer);

    [DllImport("cri_ware_unity")]
    private static extern int criFsWebInstaller_Copy(IntPtr installer, string url, string dstPath);

    [DllImport("cri_ware_unity")]
    private static extern int criFsWebInstaller_Stop(IntPtr installer);

    [DllImport("cri_ware_unity")]
    private static extern int criFsWebInstaller_GetStatusInfo(
      IntPtr installer,
      out CriFsWebInstaller.StatusInfo status);

    public enum Status {
        Stop,
        Busy,
        Complete,
        Error,
    }

    public enum Error {
        None,
        Timeout,
        Memory,
        LocalFs,
        DNS,
        Connection,
        SSL,
        HTTP,
        Internal,
    }

    public struct StatusInfo {
        public CriFsWebInstaller.Status status;
        public CriFsWebInstaller.Error error;
        public int httpStatusCode;
        public long contentsSize;
        public long receivedSize;
    }

    public struct ModuleConfig {
        public uint numInstallers;
        [MarshalAs(UnmanagedType.LPStr)]
        public string proxyHost;
        public ushort proxyPort;
        [MarshalAs(UnmanagedType.LPStr)]
        public string userAgent;
        public uint inactiveTimeoutSec;
        public bool allowInsecureSSL;
        public CriFsWebInstaller.ModulePlatformConfig platformConfig;
    }

    public struct ModulePlatformConfig {
        public byte reserved;

        public static CriFsWebInstaller.ModulePlatformConfig defaultConfig {
            get {
                CriFsWebInstaller.ModulePlatformConfig defaultConfig;
                defaultConfig.reserved = (byte)0;
                return defaultConfig;
            }
        }
    }
}
