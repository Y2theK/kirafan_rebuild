﻿namespace CriMana {
    public enum AlphaType {
        CompoOpaq,
        CompoAlphaFull,
        CompoAlpha3Step,
        CompoAlpha32Bit,
    }
}
