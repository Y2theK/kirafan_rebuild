﻿namespace CriMana {
    public struct AudioInfo {
        public uint samplingRate;
        public uint numChannels;
        public uint totalSamples;
    }
}
