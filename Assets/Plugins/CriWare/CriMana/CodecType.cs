﻿namespace CriMana {
    public enum CodecType {
        Unknown = 0,
        SofdecPrime = 1,
        H264 = 5,
    }
}
