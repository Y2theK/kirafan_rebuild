﻿using System;
using System.Reflection;
using UnityEngine;

namespace CriMana.Detail {
    public static class AutoResisterRendererResourceFactories {
        public static void InvokeAutoRegister() {
            foreach (Type nestedType in typeof(AutoResisterRendererResourceFactories).GetNestedTypes(BindingFlags.Public)) {
                if (!nestedType.IsSubclassOf(typeof(RendererResourceFactory))) {
                    Debug.LogError((object)("[CRIWARE] internal logic error. " + nestedType.Name + " is required to be a subclass of RendererResourceFactory."));
                } else {
                    RendererResourceFactoryPriorityAttribute customAttribute = (RendererResourceFactoryPriorityAttribute) Attribute.GetCustomAttribute((MemberInfo) nestedType, typeof (RendererResourceFactoryPriorityAttribute));
                    if (customAttribute == null)
                        Debug.LogError((object)("[CRIWARE] internal logic error. need priority attribute. (" + nestedType.Name + ")"));
                    else
                        RendererResourceFactory.RegisterFactory((RendererResourceFactory)Activator.CreateInstance(nestedType), customAttribute.priority);
                }
            }
        }

        [RendererResourceFactoryPriority(7000)]
        public class RendererResourceFactoryAndroidH264Rgb : RendererResourceFactory {
            public override RendererResource CreateRendererResource(
              int playerId,
              MovieInfo movieInfo,
              bool additive,
              Shader userShader) {
                bool flag1 = movieInfo.codecType == CodecType.H264;
                bool flag2 = !movieInfo.hasAlpha;
                return flag1 && flag2 ? (RendererResource)new RendererResourceAndroidH264Rgb(playerId, movieInfo, additive, userShader) : (RendererResource)null;
            }

            protected override void OnDisposeManaged() {
            }

            protected override void OnDisposeUnmanaged() {
            }
        }

        [RendererResourceFactoryPriority(5050)]
        public class RendererResourceFactoryAndroidSofdecPrimeYuv : RendererResourceFactory {
            public override RendererResource CreateRendererResource(
              int playerId,
              MovieInfo movieInfo,
              bool additive,
              Shader userShader) {
                return movieInfo.codecType == CodecType.SofdecPrime ? (RendererResource)new RendererResourceAndroidSofdecPrimeYuv(playerId, movieInfo, additive, userShader) : (RendererResource)null;
            }

            protected override void OnDisposeManaged() {
            }

            protected override void OnDisposeUnmanaged() {
            }
        }
    }
}
