﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CriMana.Detail {
    public abstract class RendererResource : IDisposable {
        private bool disposed;

        ~RendererResource() => Dispose(false);

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        private void Dispose(bool disposing) {
            if (disposed)
                return;
            if (disposing)
                OnDisposeManaged();
            OnDisposeUnmanaged();
            disposed = true;
        }

        protected abstract void OnDisposeManaged();

        protected abstract void OnDisposeUnmanaged();

        public abstract bool IsPrepared();

        public abstract bool ContinuePreparing();

        public abstract void AttachToPlayer(int playerId);

        public abstract bool UpdateFrame(int playerId, FrameInfo frameInfo);

        public abstract bool UpdateMaterial(Material material);

        public abstract void UpdateTextures();

        public abstract bool IsSuitable(
          int playerId,
          MovieInfo movieInfo,
          bool additive,
          Shader userShader);

        public static uint NextPowerOfTwo(uint x) {
            --x;
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            return x + 1U;
        }

        public static int NextPowerOfTwo(int x) => (int)RendererResource.NextPowerOfTwo((uint)x);

        public static int Ceiling16(int x) => x + 15 & -16;

        public static int Ceiling64(int x) => x + 63 & -64;

        public static int Ceiling256(int x) => x + (int)byte.MaxValue & -256;

        [DllImport("cri_ware_unity")]
        protected static extern bool criManaUnityPlayer_UpdateFrame(
          int player_id,
          int num_textures,
          IntPtr[] tex_ptrs,
          [In, Out] FrameInfo frame_info);

        [DllImport("cri_ware_unity")]
        protected static extern bool criManaUnityPlayer_UpdateTextures(
          int player_id,
          int num_textures,
          [In, Out] IntPtr[] tex_ptrs);
    }
}
