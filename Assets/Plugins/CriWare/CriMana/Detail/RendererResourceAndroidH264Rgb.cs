﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CriMana.Detail {
    public class RendererResourceAndroidH264Rgb : RendererResource {
        private int playerId;
        private bool attached;
        private int width;
        private int height;
        private bool hasAlpha;
        private bool additive;
        private bool useUserShader;
        private Shader shader;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private float[] movieTextureSTRaw = new float[4];
        private Vector4 movieTextureST = Vector4.zero;
        private uint oesTexture;
        private Texture2D texture;

        public RendererResourceAndroidH264Rgb(
          int playerId,
          MovieInfo movieInfo,
          bool additive,
          Shader userShader) {
            if (movieInfo.hasAlpha)
                Debug.LogError((object)"[CRIWARE] H.264 with Alpha is unsupported");
            this.playerId = playerId;
            width = (int)movieInfo.width;
            height = (int)movieInfo.height;
            hasAlpha = movieInfo.hasAlpha;
            this.additive = additive;
            useUserShader = userShader != null;
            shader = userShader == null ? Shader.Find(!hasAlpha ? (!additive ? "CriMana/AndroidH264Rgb" : "CriMana/AndroidH264RgbAdditive") : (!additive ? "Diffuse" : "Diffuse")) : userShader;
            oesTexture = RendererResourceAndroidH264Rgb.criManaUnity_MediaCodecCreateTexture_ANDROID();
            texture = Texture2D.CreateExternalTexture(width, height, (TextureFormat)5, false, false, (IntPtr)(long)oesTexture);
        }

        protected override void OnDisposeManaged() {
        }

        protected override void OnDisposeUnmanaged() {
            if (texture != null) {
                GameObject.Destroy(texture);
                if (attached) {
                    RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecDetachTexture_ANDROID(playerId, oesTexture);
                    attached = false;
                }
                RendererResourceAndroidH264Rgb.criManaUnity_MediaCodecDeleteTexture_ANDROID(oesTexture);
                oesTexture = 0U;
            }
            texture = (Texture2D)null;
            movieTextureSTRaw = (float[])null;
        }

        public override bool IsPrepared() => true;

        public override bool ContinuePreparing() => true;

        public override bool IsSuitable(
          int playerId,
          MovieInfo movieInfo,
          bool additive,
          Shader userShader) {
            bool flag1 = playerId == this.playerId;
            bool flag2 = movieInfo.codecType == CodecType.H264;
            bool flag3 = width == (int) movieInfo.width && height == (int) movieInfo.height;
            bool flag4 = hasAlpha == movieInfo.hasAlpha;
            bool flag5 = this.additive == additive;
            bool flag6 = !useUserShader || userShader == shader;
            return flag1 && flag2 && flag3 && flag4 && flag5 && flag6;
        }

        public override void AttachToPlayer(int playerId) {
            if (this.playerId == playerId) {
                RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecAttachTexture_ANDROID(playerId, oesTexture);
                attached = true;
            } else
                Debug.LogError((object)"[CRIWARE] Internal logic error");
        }

        public override bool UpdateFrame(int playerId, FrameInfo frameInfo) {
            bool flag = RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecUpdateTexture_ANDROID(playerId, frameInfo, movieTextureSTRaw);
            if (flag)
                UpdateMovieTextureST();
            return flag;
        }

        public override bool UpdateMaterial(Material material) {
            material.shader = shader;
            material.mainTexture = (Texture)texture;
            material.SetTexture("_TextureRGB", (Texture)texture);
            material.SetVector("_MovieTexture_ST", movieTextureST);
            return true;
        }

        private void UpdateMovieTextureST() {
            movieTextureST.x = movieTextureSTRaw[0];
            movieTextureST.y = movieTextureSTRaw[1];
            movieTextureST.z = movieTextureSTRaw[2];
            movieTextureST.w = movieTextureSTRaw[3];
        }

        public override void UpdateTextures() {
        }

        [DllImport("cri_ware_unity")]
        private static extern uint criManaUnity_MediaCodecCreateTexture_ANDROID();

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnity_MediaCodecDeleteTexture_ANDROID(uint oes_texture);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_MediaCodecAttachTexture_ANDROID(
          int player_id,
          uint oes_texture);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_MediaCodecDetachTexture_ANDROID(
          int player_id,
          uint oes_texture);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_MediaCodecUpdateTexture_ANDROID(
          int player_id,
          [In, Out] FrameInfo frame_info,
          [MarshalAs(UnmanagedType.LPArray)] float[] movieTextureST);
    }
}
