﻿using System;
using UnityEngine;

namespace CriMana.Detail {
    public class RendererResourceAndroidSofdecPrimeYuv : RendererResource {
        private int width;
        private int height;
        private bool hasAlpha;
        private bool additive;
        private bool useUserShader;
        private Shader shader;
        private Vector4 movieTextureST = Vector4.zero;
        private Texture2D[] textureY;
        private Texture2D[] textureU;
        private Texture2D[] textureV;
        private Texture2D[] textureA;
        private IntPtr[][] nativeTextures;
        private int numTextureSets = 2;
        private int currentTextureSet;
        private int drawTextureSet;
        private int playerID;
        private static int kcnt;

        public RendererResourceAndroidSofdecPrimeYuv(
          int playerId,
          MovieInfo movieInfo,
          bool additive,
          Shader userShader) {
            width = RendererResource.NextPowerOfTwo(RendererResource.Ceiling64((int)movieInfo.width));
            height = RendererResource.NextPowerOfTwo(RendererResource.Ceiling16((int)movieInfo.height));
            hasAlpha = movieInfo.hasAlpha;
            this.additive = additive;
            useUserShader = userShader != null;
            shader = !useUserShader ? Shader.Find(!hasAlpha ? (!additive ? "CriMana/SofdecPrimeYuv" : "CriMana/SofdecPrimeYuvAdditive") : (!additive ? "CriMana/SofdecPrimeYuva" : "CriMana/SofdecPrimeYuvaAdditive")) : userShader;
            UpdateMovieTextureST(movieInfo.dispWidth, movieInfo.dispHeight);
            TextureFormat textureFormat = (TextureFormat) 1;
            textureY = new Texture2D[numTextureSets];
            textureU = new Texture2D[numTextureSets];
            textureV = new Texture2D[numTextureSets];
            textureA = new Texture2D[numTextureSets];
            nativeTextures = new IntPtr[numTextureSets][];
            for (int index = 0; index < numTextureSets; ++index) {
                textureY[index] = new Texture2D(width, height, textureFormat, false, true);
                ((Texture)textureY[index]).wrapMode = (TextureWrapMode)1;
                textureU[index] = new Texture2D(width / 2, height / 2, textureFormat, false, true);
                ((Texture)textureU[index]).wrapMode = (TextureWrapMode)1;
                textureV[index] = new Texture2D(width / 2, height / 2, textureFormat, false, true);
                ((Texture)textureV[index]).wrapMode = (TextureWrapMode)1;
                nativeTextures[index] = new IntPtr[4];
                nativeTextures[index][0] = ((Texture)textureY[index]).GetNativeTexturePtr();
                nativeTextures[index][1] = ((Texture)textureU[index]).GetNativeTexturePtr();
                nativeTextures[index][2] = ((Texture)textureV[index]).GetNativeTexturePtr();
                if (hasAlpha) {
                    textureA[index] = new Texture2D(width, height, textureFormat, false, true);
                    ((Texture)textureA[index]).wrapMode = (TextureWrapMode)1;
                    nativeTextures[index][3] = ((Texture)textureA[index]).GetNativeTexturePtr();
                } else {
                    textureA[index] = (Texture2D)null;
                    nativeTextures[index][3] = IntPtr.Zero;
                }
            }
            playerID = playerId;
        }

        protected override void OnDisposeManaged() {
        }

        protected override void OnDisposeUnmanaged() {
            for (int index = 0; index < numTextureSets; ++index) {
                if (textureY[index] != null) {
                    GameObject.Destroy(textureY[index]);
                    textureY[index] = (Texture2D)null;
                }
                if (textureU[index] != null) {
                    GameObject.Destroy(textureU[index]);
                    textureU[index] = (Texture2D)null;
                }
                if (textureV[index] != null) {
                    GameObject.Destroy(textureV[index]);
                    textureV[index] = (Texture2D)null;
                }
                if (textureA[index] != null) {
                    GameObject.Destroy(textureA[index]);
                    textureA[index] = (Texture2D)null;
                }
            }
        }

        public override bool IsPrepared() => true;

        public override bool ContinuePreparing() => true;

        public override bool IsSuitable(
          int playerId,
          MovieInfo movieInfo,
          bool additive,
          Shader userShader) {
            bool flag1 = movieInfo.codecType == CodecType.SofdecPrime;
            bool flag2 = width >= (int) movieInfo.width && height >= (int) movieInfo.height;
            bool flag3 = hasAlpha == movieInfo.hasAlpha;
            bool flag4 = this.additive == additive;
            bool flag5 = !useUserShader || userShader == shader;
            return flag1 && flag2 && flag3 && flag4 && flag5;
        }

        public override void AttachToPlayer(int playerId) {
        }

        public override bool UpdateFrame(int playerId, FrameInfo frameInfo) {
            bool flag = RendererResource.criManaUnityPlayer_UpdateFrame(playerId, 0, (IntPtr[]) null, frameInfo);
            if (flag) {
                UpdateMovieTextureST(frameInfo.dispWidth, frameInfo.dispHeight);
                drawTextureSet = currentTextureSet;
                currentTextureSet = (currentTextureSet + 1) % numTextureSets;
            }
            return flag;
        }

        public override bool UpdateMaterial(Material material) {
            material.shader = shader;
            material.SetTexture("_TextureY", (Texture)textureY[drawTextureSet]);
            material.SetTexture("_TextureU", (Texture)textureU[drawTextureSet]);
            material.SetTexture("_TextureV", (Texture)textureV[drawTextureSet]);
            material.SetInt("_IsLinearColorSpace", QualitySettings.activeColorSpace != ColorSpace.Linear ? 0 : 1);
            if (hasAlpha)
                material.SetTexture("_TextureA", (Texture)textureA[drawTextureSet]);
            material.SetVector("_MovieTexture_ST", movieTextureST);
            return true;
        }

        private void UpdateMovieTextureST(uint dispWidth, uint dispHeight) {
            float num1 = (long) dispWidth == (long) width ? 1f : (float) (dispWidth - 1U) / (float) width;
            float num2 = (long) dispHeight == (long) height ? 1f : (float) (dispHeight - 1U) / (float) height;
            movieTextureST.x = num1;
            movieTextureST.y = -num2;
            movieTextureST.z = 0.0f;
            movieTextureST.w = num2;
        }

        public override void UpdateTextures() {
            int num_textures = 3;
            if (hasAlpha)
                num_textures = 4;
            RendererResource.criManaUnityPlayer_UpdateTextures(playerID, num_textures, nativeTextures[drawTextureSet]);
        }
    }
}
