﻿using System.Runtime.InteropServices;

namespace CriMana {
    [StructLayout(LayoutKind.Sequential)]
    public class MovieInfo {
        private uint _reserved1;
        private uint _hasAlpha;
        public uint width;
        public uint height;
        public uint dispWidth;
        public uint dispHeight;
        public uint framerateN;
        public uint framerateD;
        public uint totalFrames;
        private uint _codecType;
        private uint _alphaCodecType;
        public uint numAudioStreams;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public AudioInfo[] audioPrm;
        public uint numSubtitleChannels;
        public uint maxSubtitleSize;

        public bool hasAlpha {
            get => _hasAlpha == 1U;
            set => _hasAlpha = !value ? 0U : 1U;
        }

        public CodecType codecType {
            get => (CodecType)_codecType;
            set => _codecType = (uint)value;
        }

        public CodecType alphaCodecType {
            get => (CodecType)_alphaCodecType;
            set => _codecType = (uint)value;
        }
    }
}
