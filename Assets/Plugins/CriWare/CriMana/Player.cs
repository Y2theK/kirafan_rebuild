﻿using AOT;
using CriMana.Detail;
using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CriMana {
    public class Player : IDisposable {
        private const int InvalidPlayerId = -1;
        private static Player updatingPlayer;
        private int playerId = -1;
        private Player.Status requiredStatus;
        private Player.Status nativeStatus;
        private bool isNativeStartInvoked;
        private bool isNativeInitialized;
        private RendererResource rendererResource;
        private MovieInfo _movieInfo = new MovieInfo();
        private FrameInfo _frameInfo = new FrameInfo();
        private bool isMovieInfoAvailable;
        private bool isFrameInfoAvailable;
        private Player.ShaderDispatchCallback _shaderDispatchCallback;
        private bool enableSubtitle;
        private int subtitleBufferSize;
        private CriAtomExPlayer _atomExPlayer;
        private CriAtomEx3dSource _atomEx3Dsource;
        public Player.CuePointCallback cuePointCallback;

        public Player() {
            if (!CriManaPlugin.isInitialized)
                throw new Exception("CriManaPlugin is not initialized.");
            playerId = Player.criManaUnityPlayer_Create();
            additiveMode = false;
            CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Mana);
        }

        public Player(bool advanced_audio_mode, bool ambisonics_mode) {
            if (!CriManaPlugin.isInitialized)
                throw new Exception("CriFsPlugin is not initialized.");
            if (advanced_audio_mode) {
                playerId = Player.criManaUnityPlayer_CreateWithAtomExPlayer();
                _atomExPlayer = new CriAtomExPlayer(Player.criManaUnityPlayer_GetAtomExPlayerHn(playerId));
            }
            if (ambisonics_mode) {
                _atomEx3Dsource = new CriAtomEx3dSource();
                _atomExPlayer.Set3dSource(_atomEx3Dsource);
                _atomExPlayer.Set3dListener(CriAtomListener.sharedNativeListener);
                _atomExPlayer.SetPanType(CriAtomEx.PanType.Pos3d);
                _atomExPlayer.UpdateAll();
            } else
                playerId = Player.criManaUnityPlayer_Create();
            additiveMode = false;
            CriDisposableObjectManager.Register((IDisposable)this, CriDisposableObjectManager.ModuleType.Mana);
        }

        public bool additiveMode { get; set; }

        public bool isFrameAvailable => isFrameInfoAvailable;

        public MovieInfo movieInfo => isMovieInfoAvailable ? _movieInfo : (MovieInfo)null;

        public FrameInfo frameInfo => isFrameAvailable ? _frameInfo : (FrameInfo)null;

        public Player.Status status {
            get {
                if (requiredStatus == Player.Status.Stop && nativeStatus != Player.Status.Stop)
                    return Player.Status.StopProcessing;
                if (nativeStatus != Player.Status.Ready)
                    return nativeStatus;
                return rendererResource != null && rendererResource.IsPrepared() ? Player.Status.Ready : Player.Status.Prep;
            }
        }

        public int numberOfEntries => Player.criManaUnityPlayer_GetNumberOfEntry(playerId);

        public IntPtr subtitleBuffer { get; private set; }

        public int subtitleSize { get; private set; }

        public CriAtomExPlayer atomExPlayer => _atomExPlayer;

        public CriAtomEx3dSource atomEx3DsourceForAmbisonics => _atomEx3Dsource;

        ~Player() => Dispose(false);

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        public void CreateRendererResource(int width, int height, bool alpha) {
            MovieInfo movieInfo = new MovieInfo();
            movieInfo.hasAlpha = alpha;
            movieInfo.width = (uint)width;
            movieInfo.height = (uint)height;
            movieInfo.dispWidth = (uint)width;
            movieInfo.dispHeight = (uint)height;
            movieInfo.codecType = CodecType.SofdecPrime;
            movieInfo.alphaCodecType = CodecType.SofdecPrime;
            Shader userShader = _shaderDispatchCallback != null ? _shaderDispatchCallback(this.movieInfo, additiveMode) : (Shader) null;
            if (rendererResource != null && !rendererResource.IsSuitable(playerId, movieInfo, additiveMode, userShader)) {
                rendererResource.Dispose();
                rendererResource = (RendererResource)null;
            }
            if (rendererResource != null)
                return;
            rendererResource = RendererResourceFactory.DispatchAndCreate(playerId, movieInfo, additiveMode, userShader);
        }

        public void DisposeRendererResource() {
            if (rendererResource == null)
                return;
            rendererResource.Dispose();
            rendererResource = (RendererResource)null;
        }

        public void Prepare() {
            PrepareNativePlayer();
            UpdateNativePlayer();
            DisableInfos();
            requiredStatus = Player.Status.Ready;
        }

        public void Start() {
            if (nativeStatus == Player.Status.Stop || nativeStatus == Player.Status.PlayEnd) {
                PrepareNativePlayer();
                DisableInfos();
                UpdateNativePlayer();
            }
            requiredStatus = Player.Status.Playing;
        }

        public void Stop() {
            if (playerId != -1) {
                Player.criManaUnityPlayer_Stop(playerId);
                UpdateNativePlayer();
            }
            DisableInfos();
            requiredStatus = Player.Status.Stop;
        }

        public void Pause(bool sw) => Player.criManaUnityPlayer_Pause(playerId, !sw ? 0 : 1);

        public bool IsPaused() => Player.criManaUnityPlayer_IsPaused(playerId);

        public bool SetFile(CriFsBinder binder, string moviePath, Player.SetMode setMode = Player.SetMode.New) {
            IntPtr binder1 = binder != null ? binder.nativeHandle : IntPtr.Zero;
            if (binder == null && CriWare.IsStreamingAssetsPath(moviePath))
                moviePath = Path.Combine(CriWare.streamingAssetsPath, moviePath);
            if (setMode != Player.SetMode.New)
                return Player.criManaUnityPlayer_EntryFile(playerId, binder1, moviePath, setMode == Player.SetMode.AppendRepeatedly);
            Player.criManaUnityPlayer_SetFile(playerId, binder1, moviePath);
            return true;
        }

        public bool SetData(byte[] data, long datasize, Player.SetMode setMode = Player.SetMode.New) {
            if (setMode != Player.SetMode.New)
                return Player.criManaUnityPlayer_EntryData(playerId, data, datasize, setMode == Player.SetMode.AppendRepeatedly);
            Player.criManaUnityPlayer_SetData(playerId, data, datasize);
            return true;
        }

        public bool SetContentId(CriFsBinder binder, int contentId, Player.SetMode setMode = Player.SetMode.New) {
            if (binder == null) {
                Debug.LogError((object)"[CRIWARE] CriFsBinder is null");
                return false;
            }
            if (setMode != Player.SetMode.New)
                return Player.criManaUnityPlayer_EntryContentId(playerId, binder.nativeHandle, contentId, setMode == Player.SetMode.AppendRepeatedly);
            Player.criManaUnityPlayer_SetContentId(playerId, binder.nativeHandle, contentId);
            return true;
        }

        public bool SetFileRange(string filePath, ulong offset, long range, Player.SetMode setMode = Player.SetMode.New) {
            if (setMode != Player.SetMode.New)
                return Player.criManaUnityPlayer_EntryFileRange(playerId, filePath, offset, range, setMode == Player.SetMode.AppendRepeatedly);
            Player.criManaUnityPlayer_SetFileRange(playerId, filePath, offset, range);
            return true;
        }

        public void Loop(bool sw) => Player.criManaUnityPlayer_Loop(playerId, !sw ? 0 : 1);

        public void SetSeekPosition(int frameNumber) => Player.criManaUnityPlayer_SetSeekPosition(playerId, frameNumber);

        public void SetSpeed(float speed) => Player.criManaUnityPlayer_SetSpeed(playerId, speed);

        public void SetMaxPictureDataSize(uint maxDataSize) => Player.criManaUnityPlayer_SetMaxPictureDataSize(playerId, maxDataSize);

        public void SetBufferingTime(float sec) => Player.criManaUnityPlayer_SetBufferingTime(playerId, sec);

        public void SetMinBufferSize(int min_buffer_size) => Player.criManaUnityPlayer_SetMinBufferSize(playerId, min_buffer_size);

        public void SetAudioTrack(int track) => Player.criManaUnityPlayer_SetAudioTrack(playerId, track);

        public void SetAudioTrack(Player.AudioTrack track) {
            if (track == Player.AudioTrack.Off) {
                Player.criManaUnityPlayer_SetAudioTrack(playerId, -1);
            } else {
                if (track != Player.AudioTrack.Auto)
                    return;
                Player.criManaUnityPlayer_SetAudioTrack(playerId, 100);
            }
        }

        public void SetSubAudioTrack(int track) => Player.criManaUnityPlayer_SetSubAudioTrack(playerId, track);

        public void SetSubAudioTrack(Player.AudioTrack track) {
            if (track == Player.AudioTrack.Off) {
                Player.criManaUnityPlayer_SetSubAudioTrack(playerId, -1);
            } else {
                if (track != Player.AudioTrack.Auto)
                    return;
                Player.criManaUnityPlayer_SetSubAudioTrack(playerId, 100);
            }
        }

        public void SetExtraAudioTrack(int track) => Player.criManaUnityPlayer_SetExtraAudioTrack(playerId, track);

        public void SetExtraAudioTrack(Player.AudioTrack track) {
            if (track == Player.AudioTrack.Off) {
                Player.criManaUnityPlayer_SetExtraAudioTrack(playerId, -1);
            } else {
                if (track != Player.AudioTrack.Auto)
                    return;
                Player.criManaUnityPlayer_SetExtraAudioTrack(playerId, 100);
            }
        }

        public void SetVolume(float volume) => Player.criManaUnityPlayer_SetVolume(playerId, volume);

        public void SetSubAudioVolume(float volume) => Player.criManaUnityPlayer_SetSubAudioVolume(playerId, volume);

        public void SetExtraAudioVolume(float volume) => Player.criManaUnityPlayer_SetExtraAudioVolume(playerId, volume);

        public void SetBusSendLevel(string bus_name, float level) => Player.criManaUnityPlayer_SetBusSendLevelByName(playerId, bus_name, level);

        public void SetSubAudioBusSendLevel(string bus_name, float volume) => Player.criManaUnityPlayer_SetSubAudioBusSendLevelByName(playerId, bus_name, volume);

        public void SetExtraAudioBusSendLevel(string bus_name, float volume) => Player.criManaUnityPlayer_SetExtraAudioBusSendLevelByName(playerId, bus_name, volume);

        public void SetSubtitleChannel(int channel) {
            enableSubtitle = channel != -1;
            if (enableSubtitle) {
                if (isMovieInfoAvailable)
                    AllocateSubtitleBuffer((int)movieInfo.maxSubtitleSize);
            } else
                DeallocateSubtitleBuffer();
            Player.criManaUnityPlayer_SetSubtitleChannel(playerId, channel);
        }

        public void SetShaderDispatchCallback(
          Player.ShaderDispatchCallback shaderDispatchCallback) {
            _shaderDispatchCallback = shaderDispatchCallback;
        }

        public long GetTime() => Player.criManaUnityPlayer_GetTime(playerId);

        public void SetAsrRackId(int asrRackId) => Player.criManaUnityPlayer_SetAsrRackId(playerId, asrRackId);

        public void Update() {
            if (requiredStatus == Player.Status.Stop) {
                if (nativeStatus == Player.Status.Stop)
                    return;
                UpdateNativePlayer();
            } else {
                switch (nativeStatus) {
                    case Player.Status.Dechead:
                        UpdateNativePlayer();
                        if (nativeStatus != Player.Status.WaitPrep)
                            break;
                        goto case Player.Status.WaitPrep;
                    case Player.Status.WaitPrep:
                        if (!isMovieInfoAvailable) {
                            Player.criManaUnityPlayer_GetMovieInfo(playerId, _movieInfo);
                            isMovieInfoAvailable = true;
                            if (enableSubtitle)
                                AllocateSubtitleBuffer((int)movieInfo.maxSubtitleSize);
                            Shader userShader = _shaderDispatchCallback != null ? _shaderDispatchCallback(movieInfo, additiveMode) : (Shader) null;
                            if (rendererResource != null && !rendererResource.IsSuitable(playerId, _movieInfo, additiveMode, userShader)) {
                                rendererResource.Dispose();
                                rendererResource = (RendererResource)null;
                            }
                            if (rendererResource == null) {
                                rendererResource = RendererResourceFactory.DispatchAndCreate(playerId, _movieInfo, additiveMode, userShader);
                                if (rendererResource == null) {
                                    Stop();
                                    return;
                                }
                            }
                        }
                        if (!rendererResource.IsPrepared()) {
                            rendererResource.ContinuePreparing();
                            if (!rendererResource.IsPrepared())
                                break;
                        }
                        rendererResource.AttachToPlayer(playerId);
                        if (requiredStatus != Player.Status.Ready) {
                            if (requiredStatus == Player.Status.Playing) {
                                Player.criManaUnityPlayer_Start(playerId);
                                isNativeStartInvoked = true;
                                if (isNativeInitialized)
                                    IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
                                IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.INITIALIZE);
                                isNativeInitialized = true;
                                goto case Player.Status.Prep;
                            } else
                                break;
                        } else
                            goto case Player.Status.Prep;
                    case Player.Status.Prep:
                        UpdateNativePlayer();
                        if (nativeStatus != Player.Status.Ready) {
                            if (nativeStatus != Player.Status.Playing)
                                break;
                            goto case Player.Status.Playing;
                        } else
                            goto case Player.Status.Ready;
                    case Player.Status.Ready:
                        if (requiredStatus == Player.Status.Playing) {
                            if (!isNativeStartInvoked) {
                                Player.criManaUnityPlayer_Start(playerId);
                                isNativeStartInvoked = true;
                                if (isNativeInitialized)
                                    IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
                                IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.INITIALIZE);
                                isNativeInitialized = true;
                                goto case Player.Status.Playing;
                            } else
                                goto case Player.Status.Playing;
                        } else
                            break;
                    case Player.Status.Playing:
                        UpdateNativePlayer();
                        if (nativeStatus == Player.Status.Playing) {
                            isFrameInfoAvailable |= rendererResource.UpdateFrame(playerId, _frameInfo);
                            IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.UPDATE);
                            break;
                        }
                        if (nativeStatus != Player.Status.PlayEnd)
                            break;
                        break;
                    case Player.Status.Error:
                        UpdateNativePlayer();
                        break;
                }
                if (nativeStatus != Player.Status.Error)
                    return;
                DisableInfos();
            }
        }

        public void OnWillRenderObject(CriManaMovieMaterial sender) {
            if (status != Player.Status.Ready && status != Player.Status.Playing)
                return;
            rendererResource.UpdateTextures();
            IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.RENDER);
        }

        public bool UpdateMaterial(Material material) => rendererResource != null && isFrameInfoAvailable && rendererResource.UpdateMaterial(material);

        public void IssuePluginEvent(
          Player.CriManaUnityPlayer_RenderEventAction renderEventAction) {
            int num = (int) ((Player.CriManaUnityPlayer_RenderEventAction) CriManaPlugin.renderingEventOffset | renderEventAction | (Player.CriManaUnityPlayer_RenderEventAction) playerId);
            GL.IssuePluginEvent(Player.criWareUnity_GetRenderEventFunc(), num);
        }

        private void Dispose(bool disposing) {
            CriDisposableObjectManager.Unregister((IDisposable)this);
            if (playerId != -1) {
                if (atomExPlayer != null) {
                    _atomExPlayer.Dispose();
                    _atomExPlayer = (CriAtomExPlayer)null;
                }
                if (atomEx3DsourceForAmbisonics != null) {
                    _atomEx3Dsource.Dispose();
                    _atomEx3Dsource = (CriAtomEx3dSource)null;
                }
                Player.criManaUnityPlayer_Destroy(playerId);
                playerId = -1;
            }
            DisposeRendererResource();
            DeallocateSubtitleBuffer();
            cuePointCallback = (Player.CuePointCallback)null;
        }

        private void DisableInfos() {
            isMovieInfoAvailable = false;
            isFrameInfoAvailable = false;
            isNativeStartInvoked = false;
            subtitleSize = 0;
        }

        private void PrepareNativePlayer() {
            if (cuePointCallback != null) {
                int playerId = this.playerId;
                Player.criManaUnityPlayer_SetCuePointCallback(playerId, CuePointCallbackFromNative);
            }
            Player.criManaUnityPlayer_Prepare(playerId);
        }

        private void UpdateNativePlayer() {
            Player.updatingPlayer = this;
            uint subtitleBufferSize = (uint) this.subtitleBufferSize;
            nativeStatus = (Player.Status)Player.criManaUnityPlayer_Update(playerId, subtitleBuffer, ref subtitleBufferSize);
            subtitleSize = (int)subtitleBufferSize;
            Player.updatingPlayer = (Player)null;
            if (!isNativeInitialized || nativeStatus != Player.Status.StopProcessing && nativeStatus != Player.Status.Stop)
                return;
            isNativeInitialized = false;
            IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
        }

        private void AllocateSubtitleBuffer(int size) {
            if (subtitleBufferSize >= size)
                return;
            DeallocateSubtitleBuffer();
            subtitleBuffer = Marshal.AllocHGlobal(size);
            subtitleBufferSize = size;
            subtitleSize = 0;
        }

        private void DeallocateSubtitleBuffer() {
            if (!(subtitleBuffer != IntPtr.Zero))
                return;
            Marshal.FreeHGlobal(subtitleBuffer);
            subtitleBuffer = IntPtr.Zero;
            subtitleBufferSize = 0;
            subtitleSize = 0;
        }

        [MonoPInvokeCallback(typeof(Player.CuePointCallbackFromNativeDelegate))]
        private static void CuePointCallbackFromNative(
          IntPtr ptr1,
          IntPtr ptr2,
          [In] ref EventPoint eventPoint) {
            if (Player.updatingPlayer.cuePointCallback == null)
                return;
            Player.updatingPlayer.cuePointCallback(ref eventPoint);
        }

        [DllImport("cri_ware_unity")]
        private static extern int criManaUnityPlayer_Create();

        [DllImport("cri_ware_unity")]
        private static extern int criManaUnityPlayer_CreateWithAtomExPlayer();

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Destroy(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetFile(
          int player_id,
          IntPtr binder,
          string path);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetContentId(
          int player_id,
          IntPtr binder,
          int content_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetFileRange(
          int player_id,
          string path,
          ulong offset,
          long range);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetData(
          int player_id,
          byte[] data,
          long datasize);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_EntryFile(
          int player_id,
          IntPtr binder,
          string path,
          bool repeat);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_EntryContentId(
          int player_id,
          IntPtr binder,
          int content_id,
          bool repeat);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_EntryFileRange(
          int player_id,
          string path,
          ulong offset,
          long range,
          bool repeat);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_EntryData(
          int player_id,
          byte[] data,
          long datasize,
          bool repeat);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_ClearEntry(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern int criManaUnityPlayer_GetNumberOfEntry(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetCuePointCallback(
          int player_id,
          Player.CuePointCallbackFromNativeDelegate cbfunc);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_GetMovieInfo(int player_id, [Out] MovieInfo movie_info);

        [DllImport("cri_ware_unity")]
        private static extern int criManaUnityPlayer_Update(
          int player_id,
          IntPtr subtitle_buffer,
          ref uint subtitle_size);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Prepare(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Start(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Stop(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSeekPosition(int player_id, int seek_frame_no);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Pause(int player_id, int sw);

        [DllImport("cri_ware_unity")]
        private static extern bool criManaUnityPlayer_IsPaused(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_Loop(int player_id, int sw);

        [DllImport("cri_ware_unity")]
        private static extern long criManaUnityPlayer_GetTime(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern int criManaUnityPlayer_GetStatus(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern IntPtr criManaUnityPlayer_GetAtomExPlayerHn(int player_id);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetAudioTrack(int player_id, int track);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetVolume(int player_id, float vol);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSubAudioTrack(int player_id, int track);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSubAudioVolume(int player_id, float vol);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetExtraAudioTrack(int player_id, int track);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetExtraAudioVolume(int player_id, float vol);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetBusSendLevelByName(
          int player_id,
          string bus_name,
          float level);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSubAudioBusSendLevelByName(
          int player_id,
          string bus_name,
          float level);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetExtraAudioBusSendLevelByName(
          int player_id,
          string bus_name,
          float level);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSubtitleChannel(int player_id, int channel);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetSpeed(int player_id, float speed);

        [DllImport("cri_ware_unity")]
        private static extern void criManaUnityPlayer_SetMaxPictureDataSize(
          int player_id,
          uint max_data_size);

        [DllImport("cri_ware_unity")]
        public static extern void criManaUnityPlayer_SetBufferingTime(int player_id, float sec);

        [DllImport("cri_ware_unity")]
        public static extern void criManaUnityPlayer_SetMinBufferSize(
          int player_id,
          int min_buffer_size);

        [DllImport("cri_ware_unity")]
        public static extern void criManaUnityPlayer_SetAsrRackId(int player_id, int asr_rack_id);

        [DllImport("cri_ware_unity")]
        private static extern IntPtr criWareUnity_GetRenderEventFunc();

        public enum Status {
            Stop,
            Dechead,
            WaitPrep,
            Prep,
            Ready,
            Playing,
            PlayEnd,
            Error,
            StopProcessing,
        }

        public enum SetMode {
            New,
            Append,
            AppendRepeatedly,
        }

        public enum AudioTrack {
            Off,
            Auto,
        }

        public delegate void CuePointCallback(ref EventPoint eventPoint);

        public delegate Shader ShaderDispatchCallback(MovieInfo movieInfo, bool additiveMode);

        private delegate void CuePointCallbackFromNativeDelegate(
          IntPtr ptr1,
          IntPtr ptr2,
          [In] ref EventPoint eventPoint);

        public enum CriManaUnityPlayer_RenderEventAction {
            UPDATE = 0,
            INITIALIZE = 256, // 0x00000100
            RENDER = 512, // 0x00000200
            DESTROY = 768, // 0x00000300
        }
    }
}
