﻿namespace CriMana {
    public static class Settings {
        public static void SetDecodeSkippingEnabled(bool enabled) => CriManaPlugin.criManaUnity_SetDecodeSkipFlag(enabled);
    }
}
