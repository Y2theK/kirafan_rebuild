﻿using UnityEngine;

public class CriManaAmbisonicSource : MonoBehaviour {
    private CriAtomEx3dSource atomEx3DsourceForAmbisonics;
    private Vector3 ambisonicSourceOrientationFront;
    private Vector3 ambisonicSourceOrientationTop;
    private Vector3 lastEulerOfAmbisonicSource;

    private void Update() => UpdateAmbisonicSourceOrientation();

    private void OnEnable() {
        atomEx3DsourceForAmbisonics = ((Component)((Component)this).gameObject.transform.parent).GetComponent<CriManaMovieMaterial>().player.atomEx3DsourceForAmbisonics;
        if (atomEx3DsourceForAmbisonics == null)
            Debug.LogError((object)"atomEx3DsourceForAmbisonics == null");
        else
            ForceUpdateAmbisonicSourceOrientation();
    }

    private void ForceUpdateAmbisonicSourceOrientation() {
        lastEulerOfAmbisonicSource = ((Component)this).transform.eulerAngles;
        RoatateAmbisonicSourceOrientationByTransformOfChild(ref lastEulerOfAmbisonicSource);
        atomEx3DsourceForAmbisonics.SetOrientation(ambisonicSourceOrientationFront, ambisonicSourceOrientationTop);
        atomEx3DsourceForAmbisonics.Update();
    }

    private void UpdateAmbisonicSourceOrientation() {
        if (lastEulerOfAmbisonicSource != base.transform.eulerAngles) {
            ForceUpdateAmbisonicSourceOrientation();
        }
    }

    private void RoatateAmbisonicSourceOrientationByTransformOfChild(ref Vector3 input_euler) {
        Quaternion quaternion = Quaternion.Euler(input_euler);
        float num1 = (float) ((double) quaternion.x * (double) quaternion.x + (double) quaternion.y * (double) quaternion.y + (double) quaternion.z * (double) quaternion.z + (double) quaternion.w * (double) quaternion.w);
        float num2 = (double) num1 > 0.0 ? 2f / num1 : 0.0f;
        float[] numArray = new float[9]
    {
        1f - num2 * (quaternion.y * quaternion.y + quaternion.z * quaternion.z),
        num2 * (quaternion.x * quaternion.y - quaternion.w * quaternion.z),
        num2 * (quaternion.x * quaternion.z + quaternion.w * quaternion.y),
        num2 * (quaternion.x * quaternion.y + quaternion.w * quaternion.z),
        1f - num2 * (quaternion.x * quaternion.x + quaternion.z * quaternion.z),
        num2 * (quaternion.y * quaternion.z - quaternion.w * quaternion.x),
        num2 * (quaternion.x * quaternion.z - quaternion.w * quaternion.y),
        num2 * (quaternion.y * quaternion.z + quaternion.w * quaternion.x),
        1f - num2 * (quaternion.x * quaternion.x + quaternion.y * quaternion.y)
    };

        Vector3 vector3_1 = new Vector3(0f, 0f, 1f);
        Vector3 orientationFront = ambisonicSourceOrientationFront;
        orientationFront.x = (float)((double)numArray[0] * (double)vector3_1.x + (double)numArray[1] * (double)vector3_1.y + (double)numArray[2] * (double)vector3_1.z);
        orientationFront.y = (float)((double)numArray[3] * (double)vector3_1.x + (double)numArray[4] * (double)vector3_1.y + (double)numArray[5] * (double)vector3_1.z);
        orientationFront.z = (float)((double)numArray[6] * (double)vector3_1.x + (double)numArray[7] * (double)vector3_1.y + (double)numArray[8] * (double)vector3_1.z);
        ambisonicSourceOrientationFront = orientationFront;

        Vector3 vector3_2 = new Vector3(0f, 1f, 0f);
        Vector3 sourceOrientationTop = ambisonicSourceOrientationTop;
        sourceOrientationTop.x = (float)((double)numArray[0] * (double)vector3_2.x + (double)numArray[1] * (double)vector3_2.y + (double)numArray[2] * (double)vector3_2.z);
        sourceOrientationTop.y = (float)((double)numArray[3] * (double)vector3_2.x + (double)numArray[4] * (double)vector3_2.y + (double)numArray[5] * (double)vector3_2.z);
        sourceOrientationTop.z = (float)((double)numArray[6] * (double)vector3_2.x + (double)numArray[7] * (double)vector3_2.y + (double)numArray[8] * (double)vector3_2.z);
        ambisonicSourceOrientationTop = sourceOrientationTop;
    }
}
