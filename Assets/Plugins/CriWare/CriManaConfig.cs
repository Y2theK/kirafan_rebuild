﻿using System;

[Serializable]
public class CriManaConfig {
    public int numberOfDecoders = 8;
    public int numberOfMaxEntries = 4;
    public readonly bool graphicsMultiThreaded = true;
    public CriManaConfig.VitaH264PlaybackConfig vitaH264PlaybackConfig = new CriManaConfig.VitaH264PlaybackConfig();

    [Serializable]
    public class VitaH264PlaybackConfig {
        public bool useH264Playback;
        public int maxWidth = 960;
        public int maxHeight = 544;
        public bool getMemoryFromTexture;
    }
}
