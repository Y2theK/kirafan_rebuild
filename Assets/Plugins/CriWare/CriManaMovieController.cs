﻿using UnityEngine;

[AddComponentMenu("CRIWARE/CriManaMovieController")]
public class CriManaMovieController : CriManaMovieMaterial {
    public Renderer target;
    public bool useOriginalMaterial;
    private Material originalMaterial;

    protected override void Start() {
        base.Start();
        if (target == null)
            target = ((Component)this).gameObject.GetComponent<Renderer>();
        if (target == null) {
            Debug.LogError((object)"[CRIWARE] error");
            Object.Destroy((Object)this);
        } else {
            originalMaterial = target.material;
            if (useOriginalMaterial)
                return;
            target.enabled = false;
        }
    }

    protected override void Update() {
        base.Update();
        if (renderMode != CriManaMovieMaterial.RenderMode.OnVisibility || HaveRendererOwner || target == null || !target.isVisible)
            return;
        player.OnWillRenderObject((CriManaMovieMaterial)this);
    }

    protected override void OnDestroy() {
        target.material = originalMaterial;
        if (!useOriginalMaterial)
            target.enabled = false;
        originalMaterial = (Material)null;
        base.OnDestroy();
    }

    protected override void OnMaterialAvailableChanged() {
        if (isMaterialAvailable) {
            target.material = material;
            target.enabled = true;
        } else {
            target.material = originalMaterial;
            if (useOriginalMaterial)
                return;
            target.enabled = false;
        }
    }
}
