﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("CRIWARE/CriManaMovieControllerForUI")]
public class CriManaMovieControllerForUI : CriManaMovieMaterial {
    public Graphic target;
    public bool useOriginalMaterial;
    private Material originalMaterial;

    protected override void Start() {
        base.Start();
        if (target == null)
            target = ((Component)this).gameObject.GetComponent<Graphic>();
        if (target == null) {
            Debug.LogError((object)"[CRIWARE] error");
            Object.Destroy((Object)this);
        } else {
            originalMaterial = target.material;
            if (useOriginalMaterial)
                return;
            ((Behaviour)target).enabled = false;
        }
    }

    protected override void Update() {
        base.Update();
        if (renderMode != CriManaMovieMaterial.RenderMode.OnVisibility || HaveRendererOwner || target == null || !((UIBehaviour)target).IsActive())
            return;
        player.OnWillRenderObject((CriManaMovieMaterial)this);
    }

    protected override void OnDestroy() {
        target.material = originalMaterial;
        if (!useOriginalMaterial)
            ((Behaviour)target).enabled = false;
        originalMaterial = (Material)null;
        base.OnDestroy();
    }

    protected override void OnMaterialAvailableChanged() {
        if (isMaterialAvailable) {
            target.material = material;
            ((Behaviour)target).enabled = true;
        } else {
            target.material = originalMaterial;
            if (useOriginalMaterial)
                return;
            ((Behaviour)target).enabled = false;
        }
    }
}
