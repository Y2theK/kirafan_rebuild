﻿using CriMana;
using UnityEngine;

[AddComponentMenu("CRIWARE/CriManaMovieMaterial")]
public class CriManaMovieMaterial : MonoBehaviour {
    public bool playOnStart;
    public CriManaMovieMaterial.RenderMode renderMode;
    [SerializeField]
    private Material _material;
    [SerializeField]
    private string _moviePath;
    [SerializeField]
    private bool _loop;
    [SerializeField]
    private bool _additiveMode;
    [SerializeField]
    private bool _advancedAudio;
    [SerializeField]
    private bool _ambisonics;
    private bool materialOwn;
    private bool isMonoBehaviourStartCalled;
    private GameObject ambisonicSource;
    private bool unpauseOnApplicationUnpause;

    public string moviePath {
        get => _moviePath;
        set {
            if (isMonoBehaviourStartCalled)
                Debug.LogError((object)"[CRIWARE] moviePath can not be changed. Use CriMana::Player::SetFile method.");
            else
                _moviePath = value;
        }
    }

    public bool loop {
        get => _loop;
        set {
            if (isMonoBehaviourStartCalled)
                Debug.LogError((object)"[CRIWARE] loop property can not be changed. Use CriMana::Player::Loop method.");
            else
                _loop = value;
        }
    }

    public bool advancedAudio {
        get => _advancedAudio;
        set {
            if (isMonoBehaviourStartCalled) {
                Debug.LogError((object)"[CRIWARE] advancedAudio property can not be changed in running.");
            } else {
                if (!value)
                    ambisonics = false;
                _advancedAudio = value;
            }
        }
    }

    public bool ambisonics {
        get => _ambisonics;
        set {
            if (isMonoBehaviourStartCalled)
                Debug.LogError((object)"[CRIWARE] ambisonics property can not be changed in running.");
            else if (!_advancedAudio) {
                Debug.LogError((object)"[CRIWARE] ambisonics property needs for advancedAudio property to be true.");
            } else {
                if (!value) {
                    if (((Component)this).gameObject.transform.childCount > 0) {
                        GameObject gameObject2 = ambisonicSource == null ? ((Component) ((Component) this).gameObject.transform.Find("Ambisonic Source")).gameObject : ambisonicSource;
                        if (gameObject2 != null) {
                            Object.DestroyImmediate((Object)gameObject2);
                        }
                    }
                } else if (ambisonicSource == null) {
                    ambisonicSource = new GameObject();
                    ((Object)ambisonicSource).name = "Ambisonic Source";
                    ambisonicSource.transform.parent = ((Component)this).gameObject.transform;
                    ambisonicSource.transform.position = ((Component)this).gameObject.transform.position;
                    ambisonicSource.transform.rotation = ((Component)this).gameObject.transform.rotation;
                    ambisonicSource.transform.localScale = ((Component)this).gameObject.transform.localScale;
                    ambisonicSource.AddComponent<CriManaAmbisonicSource>();
                }
                _ambisonics = value;
            }
        }
    }

    public bool additiveMode {
        get => _additiveMode;
        set {
            if (isMonoBehaviourStartCalled)
                Debug.LogError((object)"[CRIWARE] additiveMode can not be changed. Use CriMana::Player::additiveMode method.");
            else
                _additiveMode = value;
        }
    }

    public bool isMaterialAvailable { get; private set; }

    public Player player { get; private set; }

    public Material material {
        get => _material;
        set {
            if (value == _material)
                return;
            if (materialOwn) {
                Object.Destroy((Object)_material);
                materialOwn = false;
            }
            _material = value;
            isMaterialAvailable = false;
        }
    }

    protected bool HaveRendererOwner { get; private set; }

    public void Play() => player.Start();

    public void Stop() {
        player.Stop();
        if (!isMaterialAvailable)
            return;
        isMaterialAvailable = false;
        OnMaterialAvailableChanged();
    }

    public void Pause(bool sw) => player.Pause(sw);

    protected virtual void OnMaterialAvailableChanged() {
    }

    protected virtual void OnMaterialUpdated() {
    }

    protected virtual void Awake() {
        player = new Player(_advancedAudio, _ambisonics);
        isMaterialAvailable = false;
    }

    protected virtual void OnEnable() {
    }

    protected virtual void OnDisable() {
    }

    protected virtual void OnDestroy() {
        player.Dispose();
        player = (Player)null;
        material = (Material)null;
    }

    protected virtual void Start() {
        HaveRendererOwner = GetComponent<Renderer>() !=  null;
        if (_material == null)
            CreateMaterial();
        if (!string.IsNullOrEmpty(moviePath))
            player.SetFile((CriFsBinder)null, moviePath);
        player.Loop(loop);
        player.additiveMode = additiveMode;
        if (playOnStart)
            player.Start();
        isMonoBehaviourStartCalled = true;
    }

    protected virtual void Update() {
        player.Update();
        bool flag;
        if (player.isFrameAvailable) {
            flag = player.UpdateMaterial(material);
            if (flag)
                OnMaterialUpdated();
        } else
            flag = false;
        if (isMaterialAvailable != flag) {
            isMaterialAvailable = flag;
            OnMaterialAvailableChanged();
        }
        if (renderMode != CriManaMovieMaterial.RenderMode.Always)
            return;
        player.OnWillRenderObject(this);
    }

    public virtual void RenderMovie() => player.OnWillRenderObject(this);

    protected virtual void OnWillRenderObject() {
        if (renderMode != CriManaMovieMaterial.RenderMode.OnVisibility)
            return;
        player.OnWillRenderObject(this);
    }

    private void OnApplicationPause(bool appPause) => ProcessApplicationPause(appPause);

    private void ProcessApplicationPause(bool appPause) {
        if (appPause) {
            unpauseOnApplicationUnpause = !player.IsPaused();
            if (!unpauseOnApplicationUnpause)
                return;
            player.Pause(true);
        } else {
            if (unpauseOnApplicationUnpause)
                player.Pause(false);
            unpauseOnApplicationUnpause = false;
        }
    }

    protected virtual void OnDrawGizmos() {
        Gizmos.color = player == null || player.status != Player.Status.Playing ? new Color(1f, 1f, 1f, 0.5f) : new Color(1f, 1f, 1f, 0.8f);
        Gizmos.DrawIcon(((Component)this).transform.position, "CriWare/film.png");
        Gizmos.DrawLine(((Component)this).transform.position, new Vector3(0.0f, 0.0f, 0.0f));
    }

    private void CreateMaterial() {
        _material = new Material(Shader.Find("VertexLit"));
        ((Object)_material).name = "CriMana-MovieMaterial";
        materialOwn = true;
    }

    public enum RenderMode {
        Always,
        OnVisibility,
        Never,
    }
}
