﻿using CriMana.Detail;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class CriManaPlugin {
    private static int initializationCount;
    private static bool enabledMultithreadedRendering;
    public static int renderingEventOffset = 1129775104;

    public static bool isInitialized => CriManaPlugin.initializationCount > 0;

    public static bool isMultithreadedRenderingEnabled => CriManaPlugin.enabledMultithreadedRendering;

    public static void SetConfigParameters(
      bool graphicsMultiThreaded,
      int num_decoders,
      int max_num_of_entries) {
        int graphicsDeviceType = (int) SystemInfo.graphicsDeviceType;
        CriManaPlugin.enabledMultithreadedRendering = graphicsMultiThreaded;
        CriWare.criWareUnity_SetRenderingEventOffsetForMana(CriManaPlugin.renderingEventOffset);
        CriManaPlugin.criManaUnity_SetConfigParameters(graphicsDeviceType, CriManaPlugin.enabledMultithreadedRendering, num_decoders, max_num_of_entries);
    }

    [Obsolete("Use CriWareVITA.EnableH264Playback and CriWareVITA.SetH264DecoderMaxSize instead.")]
    public static void SetConfigAdditonalParameters_VITA(
      bool use_h264_playback,
      int width,
      int height) {
    }

    public static void SetConfigAdditonalParameters_ANDROID(bool use_h264_playback) {
        using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("android.os.Build$VERSION")) {
            if (((AndroidJavaObject)androidJavaClass).GetStatic<int>("SDK_INT") < 16)
                return;
            CriManaPlugin.criManaUnity_SetConfigAdditionalParameters_ANDROID(use_h264_playback);
        }
    }

    public static void InitializeLibrary() {
        ++CriManaPlugin.initializationCount;
        if (CriManaPlugin.initializationCount != 1)
            return;
        if (!CriWareInitializer.IsInitialized())
            Debug.Log((object)"[CRIWARE] CriWareInitializer is not working. Initializes Mana by default parameters.");
        CriFsPlugin.InitializeLibrary();
        CriAtomPlugin.InitializeLibrary();
        CriManaPlugin.criManaUnity_Initialize();
        AutoResisterRendererResourceFactories.InvokeAutoRegister();
    }

    public static void FinalizeLibrary() {
        --CriManaPlugin.initializationCount;
        if (CriManaPlugin.initializationCount < 0) {
            Debug.LogError((object)"[CRIWARE] ERROR: Mana library is already finalized.");
        } else {
            if (CriManaPlugin.initializationCount != 0)
                return;
            CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.Mana);
            CriManaPlugin.criManaUnity_Finalize();
            RendererResourceFactory.DisposeAllFactories();
            CriAtomPlugin.FinalizeLibrary();
            CriFsPlugin.FinalizeLibrary();
        }
    }

    public static void SetDecodeThreadPriorityAndroidExperimental(int prio) => CriManaPlugin.criManaUnity_SetDecodeThreadPriority_ANDROID(prio);

    public static void Lock() => CriManaPlugin.criManaUnity_Lock();

    public static void Unlock() => CriManaPlugin.criManaUnity_Unlock();

    [DllImport("cri_ware_unity")]
    private static extern void criManaUnity_SetConfigParameters(
      int graphics_api,
      bool graphics_multi_threaded,
      int num_decoders,
      int num_of_max_entries);

    [DllImport("cri_ware_unity")]
    private static extern void criManaUnity_Initialize();

    [DllImport("cri_ware_unity")]
    private static extern void criManaUnity_Finalize();

    [DllImport("cri_ware_unity")]
    public static extern void criManaUnity_SetDecodeSkipFlag(bool flag);

    [DllImport("cri_ware_unity")]
    public static extern void criManaUnity_Lock();

    [DllImport("cri_ware_unity")]
    public static extern void criManaUnity_Unlock();

    [DllImport("cri_ware_unity")]
    public static extern uint criManaUnity_GetAllocatedHeapSize();

    [DllImport("cri_ware_unity")]
    public static extern void criManaUnity_SetDecodeThreadPriority_ANDROID(int prio);

    [DllImport("cri_ware_unity")]
    private static extern void criManaUnity_SetConfigAdditionalParameters_ANDROID(
      bool enable_h264_playback);
}
