﻿using System;
using System.Runtime.InteropServices;

public class CriStructMemory<Type> : IDisposable {
    private GCHandle gch;

    public CriStructMemory() {
        bytes = new byte[Marshal.SizeOf(typeof(Type))];
        gch = GCHandle.Alloc((object)bytes, GCHandleType.Pinned);
    }

    public byte[] bytes { get; private set; }

    public IntPtr ptr => gch.AddrOfPinnedObject();

    public void Dispose() => gch.Free();
}
