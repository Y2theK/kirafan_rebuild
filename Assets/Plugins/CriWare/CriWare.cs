﻿using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class CriWare {
    private const string scriptVersionString = "2.26.00b1";
    private const int scriptVersionNumber = 36044801;
    public const bool supportsCriFsInstaller = true;
    public const bool supportsCriFsWebInstaller = true;
    public const string pluginName = "cri_ware_unity";
    public const CallingConvention pluginCallingConvention = CallingConvention.Winapi;
    private static GameObject _managerObject;

    public static string streamingAssetsPath => Application.platform == RuntimePlatform.Android ? string.Empty : Application.streamingAssetsPath;

    public static string installTargetPath => Application.platform == RuntimePlatform.IPhonePlayer ? Application.temporaryCachePath : Application.persistentDataPath;

    public static bool IsStreamingAssetsPath(string path) => !Path.IsPathRooted(path) && path.IndexOf(':') < 0;

    public static GameObject managerObject {
        get {
            if (CriWare._managerObject == null) {
                CriWare._managerObject = GameObject.Find("/CRIWARE");
                if (CriWare._managerObject == null)
                    CriWare._managerObject = new GameObject("CRIWARE");
            }
            return CriWare._managerObject;
        }
    }

    [DllImport("cri_ware_unity")]
    public static extern int criWareUnity_SetDecryptionKey(
      ulong key,
      string authentication_file,
      bool enable_atom_decryption,
      bool enable_mana_decryption);

    [DllImport("cri_ware_unity")]
    public static extern int criWareUnity_GetVersionNumber();

    [DllImport("cri_ware_unity")]
    public static extern void criWareUnity_SetRenderingEventOffsetForMana(int offset);

    public static string GetScriptVersionString() => "2.26.00b1";

    public static int GetScriptVersionNumber() => 36044801;

    public static int GetBinaryVersionNumber() => CriWare.criWareUnity_GetVersionNumber();

    public static int GetRequiredBinaryVersionNumber() => 36044801;

    public static bool CheckBinaryVersionCompatibility() {
        if (CriWare.GetBinaryVersionNumber() == CriWare.GetRequiredBinaryVersionNumber())
            return true;
        Debug.LogError((object)"CRI runtime library is not compatible. Confirm the version number.");
        return false;
    }

    public static uint GetFsMemoryUsage() => CriFsPlugin.criFsUnity_GetAllocatedHeapSize();

    public static uint GetAtomMemoryUsage() => CriAtomPlugin.criAtomUnity_GetAllocatedHeapSize();

    public static uint GetManaMemoryUsage() => CriManaPlugin.criManaUnity_GetAllocatedHeapSize();

    public static CriWare.CpuUsage GetAtomCpuUsage() => CriAtomPlugin.GetCpuUsage();

    public struct CpuUsage {
        public float last;
        public float average;
        public float peak;
    }
}
