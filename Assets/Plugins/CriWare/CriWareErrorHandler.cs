﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

[AddComponentMenu("CRIWARE/Error Handler")]
public class CriWareErrorHandler : MonoBehaviour {
    public bool enableDebugPrintOnTerminal;
    public bool enableForceCrashOnError;
    public bool dontDestroyOnLoad = true;
    private static int initializationCount;

    public static string errorMessage { get; set; }

    private void Awake() {
        ++initializationCount;
        if (initializationCount != 1) {
            Destroy(this);
        } else {
            criWareUnity_Initialize();
            criWareUnity_SetForceCrashFlagOnError(enableForceCrashOnError);
            criWareUnity_ControlLogOutput(enableDebugPrintOnTerminal);
            if (!dontDestroyOnLoad)
                return;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void OnEnable() {
    }

    private void Start() {
    }

    private void Update() {
        if (enableDebugPrintOnTerminal)
            return;
        OutputErrorMessage();
    }

    private void OnDestroy() {
        --initializationCount;
        if (initializationCount != 0)
            return;
        criWareUnity_Finalize();
    }

    private static void OutputErrorMessage() {
        IntPtr firstError = criWareUnity_GetFirstError();
        if (firstError == IntPtr.Zero)
            return;
        string stringAnsi = Marshal.PtrToStringAnsi(firstError);
        if (stringAnsi != string.Empty) {
            OutputLog(stringAnsi);
            criWareUnity_ResetError();
        }
        if (errorMessage != null)
            return;
        errorMessage = stringAnsi.Substring(0);
    }

    private static void OutputLog(string errmsg) {
        if (errmsg == null)
            return;
        if (errmsg.StartsWith("E"))
            Debug.LogError((object)("[CRIWARE] Error:" + errmsg));
        else if (errmsg.StartsWith("W"))
            Debug.LogWarning((object)("[CRIWARE] Warning:" + errmsg));
        else
            Debug.Log((object)("[CRIWARE]" + errmsg));
    }

    [DllImport("cri_ware_unity")]
    private static extern void criWareUnity_Initialize();

    [DllImport("cri_ware_unity")]
    private static extern void criWareUnity_Finalize();

    [DllImport("cri_ware_unity")]
    private static extern IntPtr criWareUnity_GetFirstError();

    [DllImport("cri_ware_unity")]
    private static extern void criWareUnity_ResetError();

    [DllImport("cri_ware_unity")]
    private static extern void criWareUnity_ControlLogOutput(bool sw);

    [DllImport("cri_ware_unity")]
    private static extern void criWareUnity_SetForceCrashFlagOnError(bool sw);
}
