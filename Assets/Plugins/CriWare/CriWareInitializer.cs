﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[AddComponentMenu("CRIWARE/Library Initializer")]
public class CriWareInitializer : MonoBehaviour {
    public bool initializesFileSystem = true;
    public CriFsConfig fileSystemConfig = new CriFsConfig();
    public bool initializesAtom = true;
    public CriAtomConfig atomConfig = new CriAtomConfig();
    public bool initializesMana = true;
    public CriManaConfig manaConfig = new CriManaConfig();
    public bool useDecrypter;
    public CriWareDecrypterConfig decrypterConfig = new CriWareDecrypterConfig();
    public bool dontInitializeOnAwake;
    public bool dontDestroyOnLoad;
    private static int initializationCount;

    private void Awake() {
        CriWare.CheckBinaryVersionCompatibility();
        if (dontInitializeOnAwake)
            return;
        Initialize();
    }

    private void OnEnable() {
    }

    private void Start() {
    }

    private void Update() {
    }

    public void Initialize() {
        ++CriWareInitializer.initializationCount;
        if (CriWareInitializer.initializationCount != 1) {
            Destroy(this);
        } else {
            if (initializesFileSystem) {
                CriFsPlugin.SetConfigParameters(fileSystemConfig.numberOfLoaders, fileSystemConfig.numberOfBinders, fileSystemConfig.numberOfInstallers, fileSystemConfig.installBufferSize * 1024, fileSystemConfig.maxPath, fileSystemConfig.minimizeFileDescriptorUsage);
                if (fileSystemConfig.androidDeviceReadBitrate == 0)
                    fileSystemConfig.androidDeviceReadBitrate = 50000000;
                CriFsPlugin.SetConfigAdditionalParameters_ANDROID(fileSystemConfig.androidDeviceReadBitrate);
                CriFsPlugin.InitializeLibrary();
                if (fileSystemConfig.userAgentString.Length != 0)
                    CriFsUtility.SetUserAgentString(fileSystemConfig.userAgentString);
            }
            if (initializesAtom) {
                CriAtomPlugin.SetConfigParameters(Math.Max(atomConfig.maxVirtualVoices, CriAtomPlugin.GetRequiredMaxVirtualVoices(atomConfig)), atomConfig.maxVoiceLimitGroups, atomConfig.maxCategories, atomConfig.maxSequenceEventsPerFrame, atomConfig.maxBeatSyncCallbacksPerFrame, atomConfig.standardVoicePoolConfig.memoryVoices, atomConfig.standardVoicePoolConfig.streamingVoices, atomConfig.hcaMxVoicePoolConfig.memoryVoices, atomConfig.hcaMxVoicePoolConfig.streamingVoices, atomConfig.outputSamplingRate, atomConfig.asrOutputChannels, atomConfig.usesInGamePreview, atomConfig.serverFrequency, atomConfig.maxParameterBlocks, atomConfig.categoriesPerPlayback, atomConfig.maxBuses, atomConfig.vrMode);
                CriAtomPlugin.SetConfigAdditionalParameters_PC((long)atomConfig.pcBufferingTime);
                CriAtomPlugin.SetConfigAdditionalParameters_IOS((uint)Math.Max(atomConfig.iosBufferingTime, 16), atomConfig.iosOverrideIPodMusic);
                if (atomConfig.androidBufferingTime == 0)
                    atomConfig.androidBufferingTime = (int)(4000.0 / (double)atomConfig.serverFrequency);
                if (atomConfig.androidStartBufferingTime == 0)
                    atomConfig.androidStartBufferingTime = (int)(3000.0 / (double)atomConfig.serverFrequency);
                IntPtr android_context = IntPtr.Zero;
                if (atomConfig.androidUsesAndroidFastMixer)
                    android_context = ((AndroidJavaObject)new AndroidJavaClass("com.unity3d.player.UnityPlayer")).GetStatic<AndroidJavaObject>("currentActivity").GetRawObject();
                CriAtomPlugin.SetConfigAdditionalParameters_ANDROID(atomConfig.androidLowLatencyStandardVoicePoolConfig.memoryVoices, atomConfig.androidLowLatencyStandardVoicePoolConfig.streamingVoices, atomConfig.androidBufferingTime, atomConfig.androidStartBufferingTime, android_context);
                CriAtomPlugin.SetConfigAdditionalParameters_VITA(atomConfig.vitaAtrac9VoicePoolConfig.memoryVoices, atomConfig.vitaAtrac9VoicePoolConfig.streamingVoices, !initializesMana ? 0 : manaConfig.numberOfDecoders);
                atomConfig.ps4Audio3dConfig.useAudio3D |= atomConfig.vrMode;
                CriAtomPlugin.SetConfigAdditionalParameters_PS4(atomConfig.ps4Atrac9VoicePoolConfig.memoryVoices, atomConfig.ps4Atrac9VoicePoolConfig.streamingVoices, atomConfig.ps4Audio3dConfig.useAudio3D, atomConfig.ps4Audio3dConfig.voicePoolConfig.memoryVoices, atomConfig.ps4Audio3dConfig.voicePoolConfig.streamingVoices);
                CriAtomPlugin.SetConfigAdditionalParameters_WEBGL(atomConfig.webglWebAudioVoicePoolConfig.voices);
                CriAtomPlugin.InitializeLibrary();
                if (atomConfig.useRandomSeedWithTime)
                    CriAtomEx.SetRandomSeed((uint)DateTime.Now.Ticks);
                if (atomConfig.acfFileName.Length != 0) {
                    string str = atomConfig.acfFileName;
                    if (CriWare.IsStreamingAssetsPath(str))
                        str = Path.Combine(CriWare.streamingAssetsPath, str);
                    CriAtomEx.RegisterAcf((CriFsBinder)null, str);
                }
            }
            if (initializesMana) {
                CriManaPlugin.SetConfigParameters(manaConfig.graphicsMultiThreaded, manaConfig.numberOfDecoders, manaConfig.numberOfMaxEntries);
                CriManaPlugin.SetConfigAdditonalParameters_ANDROID(true);
                CriManaPlugin.InitializeLibrary();
            }
            if (useDecrypter) {
                ulong key = decrypterConfig.key.Length != 0 ? Convert.ToUInt64(decrypterConfig.key) : 0UL;
                string str = decrypterConfig.authenticationFile;
                if (CriWare.IsStreamingAssetsPath(str))
                    str = Path.Combine(CriWare.streamingAssetsPath, str);
                CriWare.criWareUnity_SetDecryptionKey(key, str, decrypterConfig.enableAtomDecryption, decrypterConfig.enableManaDecryption);
            } else
                CriWare.criWareUnity_SetDecryptionKey(0UL, string.Empty, false, false);
            if (!dontDestroyOnLoad)
                return;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(CriWare.managerObject);
        }
    }

    private void OnDestroy() {
        --CriWareInitializer.initializationCount;
        if (CriWareInitializer.initializationCount != 0)
            return;
        if (initializesMana)
            CriManaPlugin.FinalizeLibrary();
        if (initializesAtom) {
            while (CriAtomExLatencyEstimator.GetCurrentInfo().status != CriAtomExLatencyEstimator.Status.Stop)
                CriAtomExLatencyEstimator.FinalizeModule();
            CriAtomPlugin.FinalizeLibrary();
        }
        if (!initializesFileSystem)
            return;
        CriFsPlugin.FinalizeLibrary();
    }

    public static bool IsInitialized() {
        if (CriWareInitializer.initializationCount > 0)
            return true;
        CriWare.CheckBinaryVersionCompatibility();
        return false;
    }

    public static void AddAudioEffectInterface(IntPtr effect_interface) {
        List<IntPtr> effect_interface_list = (List<IntPtr>) null;
        if (!CriAtomPlugin.GetAudioEffectInterfaceList(out effect_interface_list))
            return;
        effect_interface_list.Add(effect_interface);
    }
}
