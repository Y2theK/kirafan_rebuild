﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class HockeyAppAndroid : MonoBehaviour {
    private void Awake() {
        if (instance != null) {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        CreateLogDirectory();
        appID = "567d78011b4c4748bda94b6a3ddb0f49";
        secret = "9fb12de39ea76c50713852f520d5415c";
        if (exceptionLogging && IsConnected()) {
            List<string> logFiles = GetLogFiles();
            if (logFiles.Count > 0) {
                base.StartCoroutine(SendLogs(logFiles));
            }
        }
        serverURL = GetBaseURL();
        int authType = (int)authenticatorType;
        StartCrashManager(serverURL, appID, secret, authType, updateAlert, userMetrics, autoUploadCrashes);
    }

    private void OnEnable() {
        if (exceptionLogging) {
            AppDomain.CurrentDomain.UnhandledException += OnHandleUnresolvedException;
            Application.logMessageReceived += OnHandleLogCallback;
        }
    }

    private void OnDisable() {
        if (exceptionLogging) {
            AppDomain.CurrentDomain.UnhandledException -= OnHandleUnresolvedException;
            Application.logMessageReceived -= OnHandleLogCallback;
        }
    }

    protected void StartCrashManager(string urlString, string appID, string secret, int authType, bool updateManagerEnabled, bool userMetricsEnabled, bool autoSendEnabled) {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        androidJavaClass2.CallStatic("startHockeyAppManager", new object[]
        {
            @static,
            urlString,
            appID,
            secret,
            authType,
            updateManagerEnabled,
            userMetricsEnabled,
            autoSendEnabled
        });
        HockeyAppAndroid.instance = this;
    }

    public static void CheckForUpdate() {
        if (instance != null) {
            AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
            androidJavaClass2.CallStatic("checkForUpdate", new object[]
            {
                @static,
                instance.serverURL,
                instance.appID
            });
        } else {
            Debug.Log("Failed to check for update. SDK has not been initialized, yet.");
        }
    }

    public static void ShowFeedbackForm() {
        if (instance != null) {
            AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
            androidJavaClass2.CallStatic("startFeedbackForm", new object[]
            {
                @static
            });
        } else {
            Debug.Log("Failed to present feedback form. SDK has not been initialized, yet.");
        }
    }

    protected string GetVersionCode() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getVersionCode", new object[0]);
    }

    protected string GetVersionName() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getVersionName", new object[0]);
    }

    protected string GetSdkVersion() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getSdkVersion", new object[0]);
    }

    protected string GetSdkName() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getSdkName", new object[0]);
    }

    protected string GetManufacturer() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getManufacturer", new object[0]);
    }

    protected string GetModel() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getModel", new object[0]);
    }

    protected string GetCrashReporterKey() {
        AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
        return androidJavaClass.CallStatic<string>("getCrashReporterKey", new object[0]);
    }

    protected virtual List<string> GetLogHeaders() {
        List<string> list = new List<string>();
        list.Add("Package: " + packageID);
        string versionCode = GetVersionCode();
        list.Add("Version Code: " + versionCode);
        string versionName = GetVersionName();
        list.Add("Version Name: " + versionName);
        string[] array = SystemInfo.operatingSystem.Split(new char[]
        {
            '/'
        });
        string item = "Android: " + array[0].Replace("Android OS ", string.Empty);
        list.Add(item);
        string manufacturer = GetManufacturer();
        list.Add("Manufacturer: " + manufacturer);
        string model = GetModel();
        list.Add("Model: " + model);
        string crashReporterKey = GetCrashReporterKey();
        list.Add("CrashReporter Key: " + crashReporterKey);
        list.Add("Date: " + DateTime.UtcNow.ToString("ddd MMM dd HH:mm:ss {}zzzz yyyy").Replace("{}", "GMT"));
        return list;
    }

    protected virtual WWWForm CreateForm(string log) {
        WWWForm wwwform = new WWWForm();
        byte[] array = null;
        using (FileStream fileStream = File.OpenRead(log)) {
            if (fileStream.Length > 199800L) {
                string text = null;
                using (StreamReader streamReader = new StreamReader(fileStream)) {
                    streamReader.BaseStream.Seek(fileStream.Length - 199800L, SeekOrigin.Begin);
                    text = streamReader.ReadToEnd();
                }
                List<string> logHeaders = GetLogHeaders();
                string str = string.Empty;
                foreach (string str2 in logHeaders) {
                    str = str + str2 + "\n";
                }
                text = str + "\n[...]" + text;
                try {
                    array = Encoding.Default.GetBytes(text);
                } catch (ArgumentException arg) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("Failed to read bytes of log file: " + arg);
                    }
                }
            } else {
                try {
                    array = File.ReadAllBytes(log);
                } catch (SystemException arg2) {
                    if (Debug.isDebugBuild) {
                        Debug.Log("Failed to read bytes of log file: " + arg2);
                    }
                }
            }
        }
        if (array != null) {
            wwwform.AddBinaryData("log", array, log, "text/plain");
        }
        return wwwform;
    }

    protected virtual void CreateLogDirectory() {
        string text = Application.persistentDataPath + "/logs/";
        try {
            Directory.CreateDirectory(text);
        } catch (Exception ex) {
            if (Debug.isDebugBuild) {
                Debug.Log(string.Concat(new object[]
                {
                    "Failed to create log directory at ",
                    text,
                    ": ",
                    ex
                }));
            }
        }
    }

    protected virtual List<string> GetLogFiles() {
        List<string> list = new List<string>();
        string path = Application.persistentDataPath + "/logs/";
        try {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] files = directoryInfo.GetFiles();
            if (files.Length > 0) {
                foreach (FileInfo fileInfo in files) {
                    if (fileInfo.Extension == ".log") {
                        list.Add(fileInfo.FullName);
                    } else {
                        File.Delete(fileInfo.FullName);
                    }
                }
            }
        } catch (Exception arg) {
            if (Debug.isDebugBuild) {
                Debug.Log("Failed to write exception log to file: " + arg);
            }
        }
        return list;
    }

    protected virtual IEnumerator SendLogs(List<string> logs) {
        string crashPath = "api/2/apps/[APPID]/crashes/upload";
        string url = GetBaseURL() + crashPath.Replace("[APPID]", appID);
        string sdkName = GetSdkName();
        if (sdkName != null) {
            url = url + "?sdk=" + WWW.EscapeURL(sdkName);
        }
        foreach (string log in logs) {
            WWWForm postForm = CreateForm(log);
            string lContent = postForm.headers["Content-Type"].ToString();
            lContent = lContent.Replace("\"", string.Empty);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", lContent);
            using (WWW www = new WWW(url, postForm.data, headers)) {
                yield return www;
                if (string.IsNullOrEmpty(www.error)) {
                    try {
                        File.Delete(log);
                    } catch (Exception arg) {
                        if (Debug.isDebugBuild) {
                            Debug.Log("Failed to delete exception log: " + arg);
                        }
                    }
                }
            }
        }
        yield break;
    }

    protected virtual void WriteLogToDisk(string logString, string stackTrace) {
        string str = DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss_fff");
        string text = logString.Replace("\n", " ");
        string[] array = stackTrace.Split(new char[]
        {
            '\n'
        });
        text = "\n" + text + "\n";
        foreach (string text2 in array) {
            if (text2.Length > 0) {
                text = text + "  at " + text2 + "\n";
            }
        }
        List<string> logHeaders = GetLogHeaders();
        using (StreamWriter streamWriter = new StreamWriter(Application.persistentDataPath + "/logs/LogFile_" + str + ".log", true)) {
            foreach (string value in logHeaders) {
                streamWriter.WriteLine(value);
            }
            streamWriter.WriteLine(text);
        }
    }

    protected virtual string GetBaseURL() {
        string text = string.Empty;
        string text2 = serverURL.Trim();
        if (text2.Length > 0) {
            text = text2;
            if (!text[text.Length - 1].Equals("/")) {
                text += "/";
            }
        } else {
            text = "https://rink.hockeyapp.net/";
        }
        return text;
    }

    protected virtual bool IsConnected() {
        bool result = false;
        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork) {
            result = true;
        }
        return result;
    }

    protected virtual void HandleException(string logString, string stackTrace) {
        WriteLogToDisk(logString, stackTrace);
    }

    public void OnHandleLogCallback(string logString, string stackTrace, LogType type) {
        if (type == LogType.Assert || type == LogType.Exception || type == LogType.Error) {
            HandleException(logString, stackTrace);
        }
    }

    public void OnHandleUnresolvedException(object sender, UnhandledExceptionEventArgs args) {
        if (args == null || args.ExceptionObject == null) {
            return;
        }
        if (args.ExceptionObject.GetType() == typeof(Exception)) {
            Exception ex = (Exception)args.ExceptionObject;
            HandleException(ex.Source, ex.StackTrace);
        }
    }

    protected const string HOCKEYAPP_BASEURL = "https://rink.hockeyapp.net/";

    protected const string HOCKEYAPP_CRASHESPATH = "api/2/apps/[APPID]/crashes/upload";

    protected const int MAX_CHARS = 199800;

    protected const string LOG_FILE_DIR = "/logs/";

    private static HockeyAppAndroid instance;

    [Header("HockeyApp Setup")]
    public string appID = "your-hockey-app-id";

    public string packageID = "your-package-identifier";

    public string serverURL = "your-custom-server-url";

    [Header("Authentication")]
    public HockeyAppAndroid.AuthenticatorType authenticatorType;

    public string secret = "your-hockey-app-secret";

    [Header("Crashes & Exceptions")]
    public bool autoUploadCrashes;

    public bool exceptionLogging = true;

    [Header("Metrics")]
    public bool userMetrics = true;

    [Header("Version Updates")]
    public bool updateAlert = true;

    public enum AuthenticatorType {
        Anonymous,
        HockeyAppEmail,
        HockeyAppUser,
        Validate
    }
}
