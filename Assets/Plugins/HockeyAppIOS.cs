﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HockeyAppIOS : MonoBehaviour {
    private void Awake() {
    }

    private void OnEnable() {
    }

    private void OnDisable() {
    }

    private void StartPlugin() {
    }

    [Obsolete("This is an obsolete method")]
    public static void ShowFeedbackForm() {
    }

    public static void CheckForUpdate() {
    }

    protected virtual List<string> GetLogHeaders() {
        return new List<string>();
    }

    protected virtual WWWForm CreateForm(string log) {
        return new WWWForm();
    }

    protected virtual void CreateLogDirectory() {
    }

    protected virtual List<string> GetLogFiles() {
        return new List<string>();
    }

    protected virtual IEnumerator SendLogs(List<string> logs) {
        string crashPath = "api/2/apps/[APPID]/crashes/upload";
        string url = GetBaseURL() + crashPath.Replace("[APPID]", appID);
        foreach (string log in logs) {
            WWWForm postForm = CreateForm(log);
            string lContent = postForm.headers["Content-Type"].ToString();
            lContent = lContent.Replace("\"", string.Empty);
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", lContent);
            using (WWW www = new WWW(url, postForm.data, headers)) {
                yield return www;
                if (string.IsNullOrEmpty(www.error)) {
                    try {
                        File.Delete(log);
                    } catch (Exception arg) {
                        if (Debug.isDebugBuild) {
                            Debug.Log("Failed to delete exception log: " + arg);
                        }
                    }
                }
            }
        }
        yield break;
    }

    protected virtual void WriteLogToDisk(string logString, string stackTrace) {
    }

    protected virtual string GetBaseURL() {
        return string.Empty;
    }

    protected virtual string GetAuthenticatorTypeString() {
        return string.Empty;
    }

    protected virtual bool IsConnected() {
        return false;
    }

    protected virtual void HandleException(string logString, string stackTrace) {
    }

    public void OnHandleLogCallback(string logString, string stackTrace, LogType type) {
    }

    public void OnHandleUnresolvedException(object sender, UnhandledExceptionEventArgs args) {
    }

    protected const string HOCKEYAPP_BASEURL = "https://rink.hockeyapp.net/";

    protected const string HOCKEYAPP_CRASHESPATH = "api/2/apps/[APPID]/crashes/upload";

    protected const string LOG_FILE_DIR = "/logs/";

    protected const int MAX_CHARS = 199800;

    private static HockeyAppIOS instance;

    [Header("HockeyApp Setup")]
    public string appID = "your-hockey-app-id";

    public string serverURL = "your-custom-server-url";

    [Header("Authentication")]
    public HockeyAppIOS.AuthenticatorType authenticatorType;

    public string secret = "your-hockey-app-secret";

    [Header("Crashes & Exceptions")]
    public bool autoUploadCrashes;

    public bool exceptionLogging = true;

    [Header("Metrics")]
    public bool userMetrics = true;

    [Header("Version Updates")]
    public bool updateAlert = true;

    public enum AuthenticatorType {
        Anonymous,
        Device,
        HockeyAppUser,
        HockeyAppEmail,
        WebAuth
    }
}
