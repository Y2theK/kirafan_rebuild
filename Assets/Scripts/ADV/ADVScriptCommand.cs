﻿namespace ADV {
	public class ADVScriptCommand {
		public virtual void Wait(float m_Sec) { }

		public virtual void GoTo(uint m_ScriptID) { }

		public virtual void PlayBGM(string m_BGMCueName, float m_FadeInSec) { }

		public virtual void PlayBGM(string m_BGMCueName) { }

		public virtual void StopBGM(float m_FadeOutSec) { }

		public virtual void StopBGM() { }

		public virtual void PlayVOICE(string m_ADVCharaID, uint m_CueName) { }

		public virtual void PlayVOICECueName(string m_ADVCharaID, string m_CueName) { }

		public virtual void PlayFULLVOICE(string m_CueName) { }

		public virtual void StopVOICE() { }

		public virtual void WaitVOICE() { }

		public virtual void PlaySE(string m_SeCueName) { }

		public virtual void PlaySE(string m_SeCueName, float m_FadeInSec, float m_FadeOutSec) { }

		public virtual void StopSE() { }

		public virtual void WaitSE() { }

		public virtual void FadeIn(string m_RGBA, float m_Sec) { }

		public virtual void FadeOut(string m_RGBA, float m_Sec) { }

		public virtual void FillScreen(string m_RGBA) { }

		public virtual void Fade(string m_RGBAStart, string m_RGBAEnd, float m_Sec, int m_CurveType) { }

		public virtual void WaitFade() { }

		public virtual void BGVisible(uint m_ID, string m_FileNameWithoutExt) { }

		public virtual void BGScroll(uint m_ID, float m_X, float m_Y, float m_Sec, int m_CurveType) { }

		public virtual void WaitBGScroll(uint m_ID) { }

		public virtual void WaitBGScroll() { }

		public virtual void BGScrollReset(uint m_ID, float m_Sec, int m_CurveType) { }

		public virtual void BGColor(uint m_ID, string m_StartColor, string m_EndColor, float m_Sec, int m_CurveType) { }

		public virtual void BGScale(uint m_ID, float m_Scale, float m_Sec, int m_CurveType) { }

		public virtual void BGPointScale(uint m_ID, float m_Scale, float m_Sec, float m_X, float m_Y, int m_CurveType) { }

		public virtual void WaitBGScale(uint m_ID) { }

		public virtual void WaitBGScale() { }

		public virtual void SetBGAnchor(uint m_ID, int m_Anchor) { }

		public virtual void SetBGPivot(uint m_ID, float m_X, float m_Y) { }

		public virtual void StopShake() { }

		public virtual void Shake(int m_ShakeType, float m_Time) { }

		public virtual void ShakeChara(int m_ShakeType, float m_Time) { }

		public virtual void ShakeBG(int m_ShakeType, float m_Time) { }

		public virtual void ShakeChara(int m_ShakeType, float m_Time, string m_ADVCharaID) { }

		public virtual void CharaShot(string m_ADVCharaID) { }

		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2) { }

		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3) { }

		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4) { }

		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5) { }

		public virtual void CharaShotFade(string m_ADVCharaID, float m_Sec) { }

		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, float m_Sec) { }

		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, float m_Sec) { }

		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, float m_Sec) { }

		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5, float m_Sec) { }

		public virtual void SetCharaShotPosition(int m_ADVCharaPos) { }

		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2) { }

		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3) { }

		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4) { }

		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4, int m_ADVCharaPos5) { }

		public virtual void CharaOutAll() { }

		public virtual void CharaOutAllFade(float m_Sec) { }

		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition) { }

		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType) { }

		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset) { }

		public virtual void CharaOut(string m_ADVCharaID) { }

		public virtual void CharaOut(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType) { }

		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, float m_Sec) { }

		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType) { }

		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset) { }

		public virtual void CharaOutFade(string m_ADVCharaID, float m_Sec) { }

		public virtual void CharaOutFade(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType) { }

		public virtual void SetTarget(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5) { }

		public virtual void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset) { }

		public virtual void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType) { }

		public virtual void CharaOutTarget(int m_EndSide, float m_Sec, int m_CurveType) { }

		public virtual void WaitCharaFade() { }

		public virtual void CharaAlignment(float m_Sec, int m_CurveType) { }

		public virtual void CharaSwap(string m_ADVCharaID1, string m_ADVCharaID2, float m_Sec, int m_CurveType) { }

		public virtual void CharaPosition(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Yoffset) { }

		public virtual void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Sec, int m_CurveType) { }

		public virtual void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Yoffset, float m_Sec, int m_CurveType) { }

		public virtual void WaitCharaMove() { }

		public virtual void CharaRotate(string m_ADVCharaID, float m_PivotX, float m_PivotY, float m_Angle, float m_Sec, int m_CurveType) { }

		public virtual void CharaRotate(string m_ADVCharaID, int m_CharaAnchor, float m_Angle, float m_Sec, int m_CurveType) { }

		public virtual void CharaRotate(string m_ADVCharaID, int m_CharaAnchor, float m_Angle, float m_Sec) { }

		public virtual void CharaRotateReset(string m_ADVCharaID, float m_Sec) { }

		public virtual void WaitCharaRotate(string m_ADVCharaID) { }

		public virtual void WaitCharaRotate() { }

		public virtual void CharaMot(string m_ADVCharaID, string m_MotID) { }

		public virtual void WaitMotion() { }

		public virtual void CharaFace(string m_ADVCharaID, int m_FaceID) { }

		public virtual void CharaPose(string m_ADVCharaID, string m_PoseName) { }

		public virtual void CharaEmotion(string m_ADVCharaID, string m_EmotionID) { }

		public virtual void CharaEmotion(string m_ADVCharaID, string m_EmotionID, int m_EmoPosition) { }

		public virtual void EmotionEnd(string m_ADVCharaID, string m_EmotionID) { }

		public virtual void WaitEmotion(string m_ADVCharaID, string m_EmotionID) { }

		public virtual void EmotionOffsetRotateMode(uint m_Enable) { }

		public virtual void CharaHighlight(string m_ADVCharaID) { }

		public virtual void CharaHighlightAll() { }

		public virtual void CharaHighlightReset(string m_ADVCharaID) { }

		public virtual void CharaHighlightResetAll() { }

		public virtual void CharaShading(string m_ADVCharaID) { }

		public virtual void CharaShadingAll() { }

		public virtual void CharaHighlightTalker(string m_ADVCharaID) { }

		public virtual void CharaPriorityTop(string m_ADVCharaID) { }

		public virtual void CharaPriorityTopSet(string m_ADVCharaID) { }

		public virtual void CharaPriorityBottom(string m_ADVCharaID) { }

		public virtual void CharaPriorityBottomSet(string m_ADVCharaID) { }

		public virtual void CharaPriorityReset(string m_ADVCharaID) { }

		public virtual void CharaTransparency(string m_ADVCharaID, uint m_Alpha, float m_Sec, int m_CurveType) { }

		public virtual void CharaTransparencyAll(uint m_Alpha, float m_Sec, int m_CurveType) { }

		public virtual void CharaTransparencyResetAll() { }

		public virtual void Effect(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate) { }

		public virtual void Effect(string m_effectID, int m_StandPosition) { }

		public virtual void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreen(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectScreen(string m_effectID, int m_ScreenAnchor) { }

		public virtual void EffectLoop(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectLoop(string m_effectID, int m_StandPosition) { }

		public virtual void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreenLoop(string m_effectID, int m_Anchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectScreenLoop(string m_effectID, int m_Anchor) { }

		public virtual void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y) { }

		public virtual void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition) { }

		public virtual void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y) { }

		public virtual void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y) { }

		public virtual void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor) { }

		public virtual void EffectLoopTargetEnd(string m_effectID) { }

		public virtual void EffectEnd(string m_effectID) { }

		public virtual void WaitEffect(string m_effectID) { }

		public virtual void EffectMuteSE() { }

		public virtual void EffectMuteSEReset() { }

		public virtual void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID, sbyte m_IgnoreParticle) { }

		public virtual void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID) { }

		public virtual void EffectLoopWithPreset(int m_StandPosition, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectLoopWithPreset(int m_StandPosition) { }

		public virtual void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreenLoopWithPreset(int m_Anchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectScreenLoopWithPreset(int m_Anchor) { }

		public virtual void Effect_CharaBehind(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate) { }

		public virtual void Effect_CharaBehind(string m_effectID, int m_StandPosition) { }

		public virtual void EffectChara_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectChara_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreen_CharaBehind(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectScreen_CharaBehind(string m_effectID, int m_ScreenAnchor) { }

		public virtual void EffectLoop_CharaBehind(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectLoop_CharaBehind(string m_effectID, int m_StandPosition) { }

		public virtual void EffectCharaLoop_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectCharaLoop_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreenLoop_CharaBehind(string m_effectID, int m_Anchor, float m_X, float m_Y, float m_Rotate) { }

		public virtual void EffectScreenLoop_CharaBehind(string m_effectID, int m_Anchor) { }

		public virtual void EffectLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y) { }

		public virtual void EffectLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_StandPosition) { }

		public virtual void EffectCharaLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y) { }

		public virtual void EffectCharaLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor) { }

		public virtual void EffectScreenLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y) { }

		public virtual void EffectScreenLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_ScreenAnchor) { }

		public virtual void SpriteSet(uint m_ID, string m_FileNameWithoutExt) { }

		public virtual void SpriteVisible(uint m_ID, uint m_VisibleFlg) { }

		public virtual void SpritePos(uint m_ID, float m_X, float m_Y) { }

		public virtual void SpritePos(uint m_ID, float m_X, float m_Y, float m_Sec) { }

		public virtual void SpriteScale(uint m_ID, float m_X, float m_Y) { }

		public virtual void SpriteScale(uint m_ID, float m_X, float m_Y, float m_Sec) { }

		public virtual void SpriteColor(uint m_ID, string m_RGBA) { }

		public virtual void SpriteColor(uint m_ID, string m_RGBA, float m_Sec) { }

		public virtual void SpriteFadeIn(uint m_ID, float m_Sec) { }

		public virtual void SpriteFadeIn(uint m_ID, float m_Sec, float m_X, float m_Y) { }

		public virtual void SpriteFadeOut(uint m_ID, float m_Sec) { }

		public virtual void WaitSpriteFade() { }

		public virtual void WaitSpriteFade(uint m_ID) { }

		public virtual void WaitSpritePos() { }

		public virtual void WaitSpritePos(uint m_ID) { }

		public virtual void WaitSpriteScale() { }

		public virtual void WaitSpriteScale(uint m_ID) { }

		public virtual void WaitSpriteColor() { }

		public virtual void WaitSpriteColor(uint m_ID) { }

		public virtual void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos) { }

		public virtual void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID) { }

		public virtual void CharaTalk(uint m_TextID, int m_FaceID) { }

		public virtual void CharaTalk(uint m_TextID) { }

		public virtual void CharaTalkFullVoice(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos) { }

		public virtual void CharaTalkFullVoice(uint m_TextID, int m_FaceID, string m_EmotionID) { }

		public virtual void CharaTalkFullVoice(uint m_TextID, int m_FaceID) { }

		public virtual void CharaTalkFullVoice(uint m_TextID) { }

		public virtual void CloseTalk() { }

		public virtual void ClearNovelText() { }

		public virtual void NovelAnchorSetting(int m_AnchorID, float m_X, float m_Y) { }

		public virtual void NovelAnchorSettingDefault() { }

		public virtual void AddNovelText(uint m_TextID) { }

		public virtual void NovelInsertLine() { }

		public virtual void SetCaptionDefault() { }

		public virtual void SetCaptionBGStart(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionBGIdle(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionBGEnd(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionBGEaseIn(float sec, float delay, int curveType) { }

		public virtual void SetCaptionBGEaseOut(float sec, float delay, int curveType) { }

		public virtual void SetCaptionTextStart(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionTextIdle(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionTextEnd(float posX, float posY, float sclX, float sclY, string rgba) { }

		public virtual void SetCaptionTextEaseIn(float sec, float delay, int curveType) { }

		public virtual void SetCaptionTextEaseOut(float sec, float delay, int curveType) { }

		public virtual void SetCaptionBGSprite(string fileName, float width, float height) { }

		public virtual void SetCaptionTextAlignment(int anchor) { }

		public virtual void PlayCaption(uint textID) { }

		public virtual void WarpPrepare(string m_ADVCharaID, float m_Sec) { }

		public virtual void WarpPrepareWait() { }

		public virtual void WarpStart(float m_Sec) { }

		public virtual void WarpWait() { }

		public virtual void SetHDRFactor(string m_ADVCharaID, float m_Value, float m_Sec) { }

		public virtual void Enable_PP_Contrast(uint m_Enable) { }

		public virtual void SetPP_Contrast_Level(float m_Value, float m_Sec) { }

		public virtual void Enable_PP_Brightness(uint m_Enable) { }

		public virtual void SetPP_BrightnessLevel(float m_Value, float m_Sec) { }

		public virtual void Enable_PP_Chroma(uint m_Enable) { }

		public virtual void SetPP_ChromaLevel(float m_Value, float m_Sec) { }

		public virtual void Enable_PP_ColorBlend(uint m_Enable) { }

		public virtual void SetPP_ColorBlendColor(uint m_R, uint m_G, uint m_B) { }

		public virtual void SetPP_ColorBlendLevel(float m_Value, float m_Sec) { }

		public virtual void EnableDistorsion_BG(uint m_Enable) { }

		public virtual void SetDistorsionLevel_BG(float m_Value, float m_Sec) { }

		public virtual void Distorsion_CharaClear() { }

		public virtual void Distorsion_CharaAdd(string m_ADVCharaID) { }

		public virtual void Distorsion_CharaRemove(string m_ADVCharaID) { }

		public virtual void EnableDistorsion_Chara(uint m_Enable) { }

		public virtual void SetDistorsionLevel_Chara(float m_Value, float m_Sec) { }

		public virtual void SetPP_BloomThshold(float m_Value, float m_Sec) { }

		public virtual void Enable_ShadeOff(uint m_Enable) { }

		public virtual void Set_ShadeOffLevel(float m_Value, float m_Sec) { }
	}
}
