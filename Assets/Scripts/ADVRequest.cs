﻿using System;
using System.Runtime.InteropServices;
using ADVRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x02000060 RID: 96
[Token(Token = "0x2000039")]
[StructLayout(3)]
public static class ADVRequest
{
	// Token: 0x060001FF RID: 511 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B8")]
	[Address(RVA = "0x1010D3FF0", Offset = "0x10D3FF0", VA = "0x1010D3FF0")]
	public static MeigewwwParam Add(Add param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000200 RID: 512 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B9")]
	[Address(RVA = "0x1010D4144", Offset = "0x10D4144", VA = "0x1010D4144")]
	public static MeigewwwParam Add(string advId, int stepCode, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
