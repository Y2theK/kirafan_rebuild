﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200001B RID: 27
[Token(Token = "0x2000014")]
[StructLayout(3)]
public class ADVTextDB : ScriptableObject
{
	// Token: 0x06000056 RID: 86 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600004A")]
	[Address(RVA = "0x1010D4328", Offset = "0x10D4328", VA = "0x1010D4328")]
	public ADVTextDB()
	{
	}

	// Token: 0x040000CE RID: 206
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000084")]
	public ADVTextDB_Param[] m_Params;
}
