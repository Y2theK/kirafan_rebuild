﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x0200001A RID: 26
[Token(Token = "0x2000013")]
[Serializable]
[StructLayout(0, Size = 32)]
public struct ADVTextDB_Param
{
	// Token: 0x040000CA RID: 202
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x4000080")]
	public uint m_id;

	// Token: 0x040000CB RID: 203
	[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
	[Token(Token = "0x4000081")]
	public string m_charaName;

	// Token: 0x040000CC RID: 204
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x4000082")]
	public string m_text;

	// Token: 0x040000CD RID: 205
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000083")]
	public string m_voiceLabel;
}
