﻿using System;
using System.Runtime.InteropServices;
using AbilityRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x0200005D RID: 93
[Token(Token = "0x2000036")]
[StructLayout(3)]
public static class AbilityRequest
{
	// Token: 0x060001F1 RID: 497 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AA")]
	[Address(RVA = "0x1010D4330", Offset = "0x10D4330", VA = "0x1010D4330")]
	public static MeigewwwParam Release(Release param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F2 RID: 498 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AB")]
	[Address(RVA = "0x1010D44A0", Offset = "0x10D44A0", VA = "0x1010D44A0")]
	public static MeigewwwParam Release(long managedCharacterId, int itemId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F3 RID: 499 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AC")]
	[Address(RVA = "0x1010D4600", Offset = "0x10D4600", VA = "0x1010D4600")]
	public static MeigewwwParam ReleaseSlot(ReleaseSlot param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F4 RID: 500 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AD")]
	[Address(RVA = "0x1010D4770", Offset = "0x10D4770", VA = "0x1010D4770")]
	public static MeigewwwParam ReleaseSlot(long managedAbilityBoardId, int boardSlotIndex, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F5 RID: 501 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AE")]
	[Address(RVA = "0x1010D48D0", Offset = "0x10D48D0", VA = "0x1010D48D0")]
	public static MeigewwwParam Equip(Equip param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F6 RID: 502 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001AF")]
	[Address(RVA = "0x1010D4A80", Offset = "0x10D4A80", VA = "0x1010D4A80")]
	public static MeigewwwParam Equip(long managedAbilityBoardId, int slotIndex, int itemId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F7 RID: 503 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B0")]
	[Address(RVA = "0x1010D4C28", Offset = "0x10D4C28", VA = "0x1010D4C28")]
	public static MeigewwwParam UpgradeSphere(UpgradeSphere param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001F8 RID: 504 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B1")]
	[Address(RVA = "0x1010D4E70", Offset = "0x10D4E70", VA = "0x1010D4E70")]
	public static MeigewwwParam UpgradeSphere(long managedAbilityBoardId, int slotIndex, int srcItemId, int[] materialItemIds, int[] materialItemAmounts, int amount, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
