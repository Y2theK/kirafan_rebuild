﻿namespace AbilityRequestTypes {
    public class Equip {
        public long managedAbilityBoardId;
        public int slotIndex;
        public int itemId;
    }
}
