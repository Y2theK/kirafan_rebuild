﻿namespace AbilityRequestTypes {
    public class ReleaseSlot {
        public long managedAbilityBoardId;
        public int boardSlotIndex;
    }
}
