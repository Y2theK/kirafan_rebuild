﻿namespace AbilityRequestTypes {
    public class UpgradeSphere {
        public long managedAbilityBoardId;
        public int slotIndex;
        public int srcItemId;
        public int[] materialItemIds;
        public int[] materialItemAmounts;
        public int amount;
    }
}
