﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AbilityResponseTypes;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

// Token: 0x02000081 RID: 129
[Token(Token = "0x200005A")]
[StructLayout(3)]
public static class AbilityResponse
{
	// Token: 0x06000349 RID: 841 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000302")]
	[Address(RVA = "0x1010D510C", Offset = "0x10D510C", VA = "0x1010D510C")]
	public static Release Release(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600034A RID: 842 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000303")]
	[Address(RVA = "0x1010D5174", Offset = "0x10D5174", VA = "0x1010D5174")]
	public static ReleaseSlot ReleaseSlot(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600034B RID: 843 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000304")]
	[Address(RVA = "0x1010D51DC", Offset = "0x10D51DC", VA = "0x1010D51DC")]
	public static Equip Equip(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600034C RID: 844 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000305")]
	[Address(RVA = "0x1010D5244", Offset = "0x10D5244", VA = "0x1010D5244")]
	public static UpgradeSphere UpgradeSphere(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
