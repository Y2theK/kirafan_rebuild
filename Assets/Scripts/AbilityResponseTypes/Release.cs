﻿using CommonResponseTypes;
using WWWTypes;

namespace AbilityResponseTypes {
    public class Release : CommonResponse {
        public PlayerItemSummary[] itemSummary;
        public PlayerAbilityBoard[] playerAbilityBoards;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (playerAbilityBoards == null) ? string.Empty : playerAbilityBoards.ToString();
            return str;
        }
    }
}
