﻿using CommonResponseTypes;
using WWWTypes;

namespace AbilityResponseTypes {
    public class ReleaseSlot : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerAbilityBoard[] playerAbilityBoards;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (playerAbilityBoards == null) ? string.Empty : playerAbilityBoards.ToString();
            return str;
        }
    }
}
