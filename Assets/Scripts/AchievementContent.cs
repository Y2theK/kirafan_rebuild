﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000052 RID: 82
[Token(Token = "0x200002D")]
[StructLayout(3)]
public class AchievementContent : MonoBehaviour
{
	// Token: 0x060001D4 RID: 468 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600018D")]
	[Address(RVA = "0x1010D561C", Offset = "0x10D561C", VA = "0x1010D561C")]
	private void Start()
	{
	}

	// Token: 0x060001D5 RID: 469 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600018E")]
	[Address(RVA = "0x1010D5620", Offset = "0x10D5620", VA = "0x1010D5620")]
	private void Update()
	{
	}

	// Token: 0x060001D6 RID: 470 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600018F")]
	[Address(RVA = "0x1010D5624", Offset = "0x10D5624", VA = "0x1010D5624")]
	public AchievementContent()
	{
	}
}
