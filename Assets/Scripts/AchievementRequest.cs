﻿using System;
using System.Runtime.InteropServices;
using AchievementRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x0200005E RID: 94
[Token(Token = "0x2000037")]
[StructLayout(3)]
public static class AchievementRequest
{
	// Token: 0x060001F9 RID: 505 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B2")]
	[Address(RVA = "0x1010D562C", Offset = "0x10D562C", VA = "0x1010D562C")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001FA RID: 506 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B3")]
	[Address(RVA = "0x1010D56F0", Offset = "0x10D56F0", VA = "0x1010D56F0")]
	public static MeigewwwParam Getall([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001FB RID: 507 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B4")]
	[Address(RVA = "0x1010D57B4", Offset = "0x10D57B4", VA = "0x1010D57B4")]
	public static MeigewwwParam Set(Set param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001FC RID: 508 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B5")]
	[Address(RVA = "0x1010D58DC", Offset = "0x10D58DC", VA = "0x1010D58DC")]
	public static MeigewwwParam Set(int achievementId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001FD RID: 509 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B6")]
	[Address(RVA = "0x1010D59F4", Offset = "0x10D59F4", VA = "0x1010D59F4")]
	public static MeigewwwParam Shown(Shown param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060001FE RID: 510 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001B7")]
	[Address(RVA = "0x1010D5AF8", Offset = "0x10D5AF8", VA = "0x1010D5AF8")]
	public static MeigewwwParam Shown(int[] titleTypes, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
