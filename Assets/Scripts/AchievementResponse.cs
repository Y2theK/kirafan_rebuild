﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AchievementResponseTypes;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

// Token: 0x02000082 RID: 130
[Token(Token = "0x200005B")]
[StructLayout(3)]
public static class AchievementResponse
{
	// Token: 0x0600034D RID: 845 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000306")]
	[Address(RVA = "0x1010D5C20", Offset = "0x10D5C20", VA = "0x1010D5C20")]
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600034E RID: 846 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000307")]
	[Address(RVA = "0x1010D5C88", Offset = "0x10D5C88", VA = "0x1010D5C88")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600034F RID: 847 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000308")]
	[Address(RVA = "0x1010D5CF0", Offset = "0x10D5CF0", VA = "0x1010D5CF0")]
	public static Shown Shown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
