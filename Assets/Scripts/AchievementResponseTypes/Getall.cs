﻿using CommonResponseTypes;
using WWWTypes;

namespace AchievementResponseTypes {
    public class Getall : CommonResponse {
        public PlayerAchievement[] achievements;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (achievements == null) ? string.Empty : achievements.ToString();
            return str;
        }
    }
}
