﻿using CommonResponseTypes;
using WWWTypes;

namespace AchievementResponseTypes {
    public class Set : CommonResponse {
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
