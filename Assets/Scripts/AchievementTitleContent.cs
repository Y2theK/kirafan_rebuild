﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000053 RID: 83
[Token(Token = "0x200002E")]
[StructLayout(3)]
public class AchievementTitleContent : MonoBehaviour
{
	// Token: 0x060001D7 RID: 471 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000190")]
	[Address(RVA = "0x1010D5E78", Offset = "0x10D5E78", VA = "0x1010D5E78")]
	private void Start()
	{
	}

	// Token: 0x060001D8 RID: 472 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000191")]
	[Address(RVA = "0x1010D5E7C", Offset = "0x10D5E7C", VA = "0x1010D5E7C")]
	private void Update()
	{
	}

	// Token: 0x060001D9 RID: 473 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000192")]
	[Address(RVA = "0x1010D5E80", Offset = "0x10D5E80", VA = "0x1010D5E80")]
	public AchievementTitleContent()
	{
	}
}
