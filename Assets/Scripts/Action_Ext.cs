﻿using System;

public static class Action_Ext {
    public static void Call(this Action action) {
        action?.Invoke();
    }

    public static void Call<T>(this Action<T> action, T arg) {
        action?.Invoke(arg);
    }

    public static void Call<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2) {
        action?.Invoke(arg1, arg2);
    }

    public static void Call<T1, T2, T3>(this Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3) {
        action?.Invoke(arg1, arg2, arg3);
    }
}
