﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

public class AesCryptUtil {
    private static readonly int blockSize = ProjDepend.CRYPT_AES_CBC_LENGTH;
    private static CipherMode cipherMode = CipherMode.CBC;

    private static byte[] keyStr => ProjDepend.CRYPT_AES_CBC_KEY;

    public static void setCipherMode(CipherMode mode) {
        cipherMode = mode;
    }

    public byte[] encrypt(byte[] message, string ivStr) {
        byte[] dst = new byte[0];
        if (keyStr.Length == 0) {
            return dst;
        }

        byte[] iv = Encoding.UTF8.GetBytes(ivStr.Substring(0, 16));
        using (var rijndael = new RijndaelManaged()) {
            rijndael.BlockSize = blockSize;
            rijndael.Key = keyStr;
            rijndael.IV = iv;
            rijndael.Mode = cipherMode;
            rijndael.Padding = PaddingMode.PKCS7;

            using (var stream = new MemoryStream())
            using (var transform = rijndael.CreateEncryptor(keyStr, iv))
            using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                cryptoStream.Write(message, 0, message.Length);
                cryptoStream.FlushFinalBlock();
                dst = stream.ToArray();
            }
        }
        return dst;
    }

    public byte[] decrypt(byte[] encstr, string ivStr) {
        byte[] dst = new byte[0];
        if (keyStr.Length == 0) {
            return dst;
        }

        byte[] iv = Encoding.UTF8.GetBytes(ivStr.Substring(0, 16));
        using (var rijndael = new RijndaelManaged()) {
            rijndael.BlockSize = blockSize;
            rijndael.Key = keyStr;
            rijndael.IV = iv;
            rijndael.Mode = cipherMode;
            rijndael.Padding = PaddingMode.PKCS7;

            using (var stream = new MemoryStream())
            using (var transform = rijndael.CreateDecryptor(keyStr, iv))
            using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                cryptoStream.Write(encstr, 0, encstr.Length);
                cryptoStream.FlushFinalBlock();
                dst = stream.ToArray();
            }
        }
        return dst;
    }
}
