﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000002 RID: 2
[Token(Token = "0x2000002")]
[StructLayout(3)]
public class AnimationTargetJointInfo : MonoBehaviour
{
	// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000001")]
	[Address(RVA = "0x1010D6DE4", Offset = "0x10D6DE4", VA = "0x1010D6DE4")]
	public void Make(SkinnedMeshRenderer smr)
	{
	}

	// Token: 0x06000002 RID: 2 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000002")]
	[Address(RVA = "0x1010D6F68", Offset = "0x10D6F68", VA = "0x1010D6F68")]
	public void Attach(GameObject treeGO)
	{
	}

	// Token: 0x06000003 RID: 3 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000003")]
	[Address(RVA = "0x1010D6FDC", Offset = "0x10D6FDC", VA = "0x1010D6FDC")]
	public void Attach(Transform[] allBones)
	{
	}

	// Token: 0x06000004 RID: 4 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000004")]
	[Address(RVA = "0x1010D731C", Offset = "0x10D731C", VA = "0x1010D731C")]
	private void Start()
	{
	}

	// Token: 0x06000005 RID: 5 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000005")]
	[Address(RVA = "0x1010D737C", Offset = "0x10D737C", VA = "0x1010D737C")]
	private void Update()
	{
	}

	// Token: 0x06000006 RID: 6 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000006")]
	[Address(RVA = "0x1010D7390", Offset = "0x10D7390", VA = "0x1010D7390")]
	public AnimationTargetJointInfo()
	{
	}

	// Token: 0x04000001 RID: 1
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000001")]
	public string[] m_NameArray;

	// Token: 0x04000002 RID: 2
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000002")]
	private SkinnedMeshRenderer m_SMR;

	// Token: 0x04000003 RID: 3
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000003")]
	private int m_BBTimer;
}
