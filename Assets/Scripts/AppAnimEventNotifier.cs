﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000003 RID: 3
[Token(Token = "0x2000003")]
[StructLayout(3)]
public class AppAnimEventNotifier : MonoBehaviour
{
	// Token: 0x06000007 RID: 7 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000007")]
	[Address(RVA = "0x1010D7398", Offset = "0x10D7398", VA = "0x1010D7398")]
	private void Start()
	{
	}

	// Token: 0x06000008 RID: 8 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000008")]
	[Address(RVA = "0x1010D739C", Offset = "0x10D739C", VA = "0x1010D739C")]
	public void Notify_AppAnimEvent(int id)
	{
	}

	// Token: 0x06000009 RID: 9 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000009")]
	[Address(RVA = "0x1010D73A0", Offset = "0x10D73A0", VA = "0x1010D73A0")]
	public AppAnimEventNotifier()
	{
	}

	// Token: 0x02000004 RID: 4
	[Token(Token = "0x2000CDB")]
	public enum eEvID
	{

	}
}
