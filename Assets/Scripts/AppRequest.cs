﻿using System;
using System.Runtime.InteropServices;
using AppRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x02000061 RID: 97
[Token(Token = "0x200003A")]
[StructLayout(3)]
public static class AppRequest
{
	// Token: 0x06000201 RID: 513 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BA")]
	[Address(RVA = "0x1010D73A8", Offset = "0x10D73A8", VA = "0x1010D73A8")]
	public static MeigewwwParam Health(Health param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000202 RID: 514 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BB")]
	[Address(RVA = "0x1010D746C", Offset = "0x10D746C", VA = "0x1010D746C")]
	public static MeigewwwParam Health([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000203 RID: 515 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BC")]
	[Address(RVA = "0x1010D7530", Offset = "0x10D7530", VA = "0x1010D7530")]
	public static MeigewwwParam Versionget(Versionget param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000204 RID: 516 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BD")]
	[Address(RVA = "0x1010D7684", Offset = "0x10D7684", VA = "0x1010D7684")]
	public static MeigewwwParam Versionget(int platform, string version, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
