﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AppResponseTypes;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

// Token: 0x02000085 RID: 133
[Token(Token = "0x200005E")]
[StructLayout(3)]
public static class AppResponse
{
	// Token: 0x06000351 RID: 849 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030A")]
	[Address(RVA = "0x1010D77F0", Offset = "0x10D77F0", VA = "0x1010D77F0")]
	public static Health Health(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000352 RID: 850 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030B")]
	[Address(RVA = "0x1010D7858", Offset = "0x10D7858", VA = "0x1010D7858")]
	public static Versionget Versionget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
