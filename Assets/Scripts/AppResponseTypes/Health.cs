﻿using CommonResponseTypes;

namespace AppResponseTypes {
    public class Health : CommonResponse {
        public long dbTime;

        public override string ToMessage() {
            string str = base.ToMessage();
            return str + dbTime.ToString();
        }
    }
}
