﻿using CommonResponseTypes;

namespace AppResponseTypes {
    public class Versionget : CommonResponse {
        public int abVer;
        public string resourceVersionHash;
        public string applyUrl;
        public string applys3Url;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += abVer.ToString();
            str += (resourceVersionHash == null) ? string.Empty : resourceVersionHash.ToString();
            str += (applyUrl == null) ? string.Empty : applyUrl.ToString();
            return str + ((applys3Url == null) ? string.Empty : applys3Url.ToString());
        }
    }
}
