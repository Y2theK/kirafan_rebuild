﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000026 RID: 38
[Token(Token = "0x200001E")]
[StructLayout(3)]
public class AppsFlyersHandler : MonoBehaviour
{
	// Token: 0x0600008C RID: 140 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600007C")]
	[Address(RVA = "0x1010D7A20", Offset = "0x10D7A20", VA = "0x1010D7A20")]
	private void Start()
	{
	}

	// Token: 0x0600008D RID: 141 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600007D")]
	[Address(RVA = "0x1010D7B40", Offset = "0x10D7B40", VA = "0x1010D7B40")]
	private void Update()
	{
	}

	// Token: 0x0600008E RID: 142 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600007E")]
	[Address(RVA = "0x1010D7B80", Offset = "0x10D7B80", VA = "0x1010D7B80")]
	public void TrackEvent(string eventName, string eventValue)
	{
	}

	// Token: 0x0600008F RID: 143 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600007F")]
	[Address(RVA = "0x1010D7B90", Offset = "0x10D7B90", VA = "0x1010D7B90")]
	public void TrackRichEvent(string eventName, Dictionary<string, string> eventValue)
	{
	}

	// Token: 0x06000090 RID: 144 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000080")]
	[Address(RVA = "0x1010D7BA0", Offset = "0x10D7BA0", VA = "0x1010D7BA0")]
	public void TrackEvent_Purchase(long revenueValue, string contentType, int contentID = 0, string currency = "JPY")
	{
	}

	// Token: 0x06000091 RID: 145 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000081")]
	[Address(RVA = "0x1010D7D24", Offset = "0x10D7D24", VA = "0x1010D7D24")]
	public void TrackEvent_TutorialClear()
	{
	}

	// Token: 0x06000092 RID: 146 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000082")]
	[Address(RVA = "0x1010D7DD0", Offset = "0x10D7DD0", VA = "0x1010D7DD0")]
	public void didReceiveConversionData(string conversionData)
	{
	}

	// Token: 0x06000093 RID: 147 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000083")]
	[Address(RVA = "0x1010D7E30", Offset = "0x10D7E30", VA = "0x1010D7E30")]
	public void didReceiveConversionDataWithError(string error)
	{
	}

	// Token: 0x06000094 RID: 148 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000084")]
	[Address(RVA = "0x1010D7E34", Offset = "0x10D7E34", VA = "0x1010D7E34")]
	public void onAppOpenAttribution(string validateResult)
	{
	}

	// Token: 0x06000095 RID: 149 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000085")]
	[Address(RVA = "0x1010D7E38", Offset = "0x10D7E38", VA = "0x1010D7E38")]
	public void onAppOpenAttributionFailure(string error)
	{
	}

	// Token: 0x06000096 RID: 150 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000086")]
	[Address(RVA = "0x1010D7E3C", Offset = "0x10D7E3C", VA = "0x1010D7E3C")]
	public AppsFlyersHandler()
	{
	}

	// Token: 0x040000DA RID: 218
	[Token(Token = "0x4000090")]
	public static AppsFlyersHandler Instance;

	// Token: 0x040000DB RID: 219
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000091")]
	public bool m_isDebug;

	// Token: 0x040000DC RID: 220
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000092")]
	public string m_AndroidDevKey;

	// Token: 0x040000DD RID: 221
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000093")]
	public string m_iOSDevKey;

	// Token: 0x040000DE RID: 222
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x4000094")]
	public string m_iOSAppID;

	// Token: 0x040000DF RID: 223
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x4000095")]
	private bool m_tokenSent;
}
