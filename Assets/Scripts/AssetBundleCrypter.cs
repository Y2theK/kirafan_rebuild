﻿using Meige;
using Meige.AssetBundles;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

public class AssetBundleCrypter {
    private void EncryptAssetBase(byte[] source, string exportPath) {
        byte[] pw = ConvertToPW(exportPath, 16);
        byte[] key = ConvertToKey(exportPath);
        new AssetBundleCryptography().EncryptAes(source, key, pw, out byte[] encrypted);

        using (var stream = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
        using (var writer = new BinaryWriter(stream)) {
            int header = UnityEngine.Random.Range(-2147483647, 2147483647);
            writer.Write((int)(header & 0xFFFF00FF | 0x400));
            for (int i = 0; i < pw.Length; i++) {
                pw[i] = (byte)(i + pw[i] + 0x60);
            }
            writer.Write((byte)((header & 0x7F) + pw.Length));
            writer.Write(pw);
            writer.Write(encrypted.Length);
            writer.Write(encrypted);
        }
    }

    private void DecryptAssetBase(string path, byte[] encryptData, out byte[] decryptData) {
        byte[] key = ConvertToKey(path);
        decryptData = null;

        using (var stream = new MemoryStream(encryptData))
        using (var reader = new BinaryReader(stream)) {
            int header = reader.ReadInt32();
            if ((header & 0xFF00) == 0x400) {
                int pwSize = reader.ReadByte() - (header & 0x7F);
                if (pwSize > 0 || encryptData.Length - 5 >= pwSize) {
                    byte[] pw = reader.ReadBytes(pwSize);
                    for (int i = 0; i < pw.Length; i++) {
                        pw[i] = (byte)(pw[i] - i + 0xA0);
                    }
                    int dataSize = reader.ReadInt32();
                    if (dataSize > 0 && dataSize == encryptData.Length - pwSize - 9) {
                        byte[] src = reader.ReadBytes(dataSize);
                        new AssetBundleCryptography().DecryptAes(src, key, pw, out decryptData);
                    }
                }
            }
        }
    }

    private byte[] ConvertToKey(string path) {
        string name = new string(Path.GetFileNameWithoutExtension(path).Reverse().ToArray());
        byte[] first = Encoding.UTF8.GetBytes(name);
        byte[] second = ProjDepend.AB_EK;
        byte[] key = first.Concat(second).ToArray();
        using (var hasher = RIPEMD160.Create()) {
            key = hasher.ComputeHash(key);
            key = key.Concat(second).Take(second.Length).ToArray();
        }
        return key;
    }

    private byte[] ConvertToPW(string path, int num) {
        string file = Path.GetFileName(path);
        file = file.Replace(MeigeResourceManager.AB_EXTENSION, string.Empty);
        string encoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(file));
        if (encoded.Length == 0) {
            throw new Exception("FileName Exception.");
        }
        string pw = "";
        while (pw.Length < num) {
            pw += encoded;
        }
        return Encoding.UTF8.GetBytes(pw.Substring(0, num));
    }

    private void EncryptAssetCustom(byte[] source, string exportPath) {
        int header = UnityEngine.Random.Range(-2147483647, 2147483647);
        byte[] hBytes = BitConverter.GetBytes((int)(header & 0xFFFF00FF | 0x400));
        Encrypt.Main(source, out byte[] encrypted, 1, 100);
        FileStream fs = new FileStream(exportPath, FileMode.Create, FileAccess.Write);
        fs.Write(hBytes, 0, hBytes.Length);
        fs.Write(encrypted, 0, encrypted.Length);
        fs.Close();
    }

    private void DecryptAssetCustom(string path, byte[] encryptData, out byte[] decryptData) {
        decryptData = null;

        using (var stream = new MemoryStream(encryptData))
        using (var reader = new BinaryReader(stream)) {
            if ((reader.ReadInt32() & 0xFF00) == 0x400) {
                byte[] src = reader.ReadBytes(encryptData.Length);
                Decrypt.Main(src, out decryptData, 1, 100);
            }
        }
    }

    public void EncryptAsset(byte[] source, string exportPath) {
        EncryptAssetCustom(source, exportPath);
    }

    public void DecryptAsset(string path, byte[] encryptData, out byte[] decryptData) {
        DecryptAssetCustom(path, encryptData, out decryptData);
    }
}
