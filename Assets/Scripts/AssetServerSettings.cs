﻿using Star;

public class AssetServerSettings {
    private const string m_assetServerURL = "https://asset-krr-prd.star-api.com/{0}/";
    private const bool m_awakeCleanCache = false;

    public static string m_assetServerReleaseURL = string.Empty;

    public static string resourceVersionHash { get; set; }
    public static bool awakeCleanCache => m_awakeCleanCache;

    public static string assetServerURL {
        get {
            if (Version.IsApplying) {
                return m_assetServerReleaseURL;
            }
            return string.Format(m_assetServerURL, resourceVersionHash);
        }
    }
}
