﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000D0 RID: 208
[Token(Token = "0x200009A")]
[StructLayout(3)]
public static class BinaryFileUtility
{
	// Token: 0x060004D4 RID: 1236 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000479")]
	public static void Write<TStruct>(BinaryWriter writer, TStruct s) where TStruct : struct
	{
	}

	// Token: 0x060004D5 RID: 1237 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600047A")]
	public static TStruct Read<TStruct>(BinaryReader reader) where TStruct : struct
	{
		return null;
	}

	// Token: 0x060004D6 RID: 1238 RVA: 0x00002D90 File Offset: 0x00000F90
	[Token(Token = "0x600047B")]
	public static int SizeOf<TStruct>() where TStruct : struct
	{
		return 0;
	}

	// Token: 0x060004D7 RID: 1239 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600047C")]
	[Address(RVA = "0x1010D8F6C", Offset = "0x10D8F6C", VA = "0x1010D8F6C")]
	public static void WriteString(BinaryWriter writer, string str)
	{
	}
}
