﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using UnityEngine;

// Token: 0x02000058 RID: 88
[Token(Token = "0x2000032")]
[StructLayout(3)]
public class BonusWindowGroup : UIGroup
{
	// Token: 0x060001E3 RID: 483 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600019C")]
	[Address(RVA = "0x1010D913C", Offset = "0x10D913C", VA = "0x1010D913C")]
	public void Setup()
	{
	}

	// Token: 0x060001E4 RID: 484 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600019D")]
	[Address(RVA = "0x1010D9170", Offset = "0x10D9170", VA = "0x1010D9170", Slot = "5")]
	public override void Open()
	{
	}

	// Token: 0x060001E5 RID: 485 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600019E")]
	[Address(RVA = "0x1010D9330", Offset = "0x10D9330", VA = "0x1010D9330")]
	public BonusWindowGroup()
	{
	}

	// Token: 0x040001FD RID: 509
	[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
	[Token(Token = "0x4000152")]
	[SerializeField]
	private ScrollViewBase m_Scroll;
}
