﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000D1 RID: 209
[Token(Token = "0x200009B")]
[StructLayout(3)]
public class CSVReader
{
	// Token: 0x060004D8 RID: 1240 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600047D")]
	[Address(RVA = "0x1010D9338", Offset = "0x10D9338", VA = "0x1010D9338")]
	public CSVReader(string path)
	{
	}

	// Token: 0x060004D9 RID: 1241 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600047E")]
	[Address(RVA = "0x1010D9368", Offset = "0x10D9368", VA = "0x1010D9368")]
	public void parse(string path)
	{
	}

	// Token: 0x060004DA RID: 1242 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600047F")]
	[Address(RVA = "0x1010D9540", Offset = "0x10D9540", VA = "0x1010D9540")]
	public string get(int row, int col)
	{
		return null;
	}

	// Token: 0x060004DB RID: 1243 RVA: 0x00002DA8 File Offset: 0x00000FA8
	[Token(Token = "0x6000480")]
	[Address(RVA = "0x1010D96C0", Offset = "0x10D96C0", VA = "0x1010D96C0")]
	public int getRowSize()
	{
		return 0;
	}

	// Token: 0x060004DC RID: 1244 RVA: 0x00002DC0 File Offset: 0x00000FC0
	[Token(Token = "0x6000481")]
	[Address(RVA = "0x1010D96F8", Offset = "0x10D96F8", VA = "0x1010D96F8")]
	public int getColumnSize(int row)
	{
		return 0;
	}

	// Token: 0x060004DD RID: 1245 RVA: 0x00002DD8 File Offset: 0x00000FD8
	[Token(Token = "0x6000482")]
	[Address(RVA = "0x1010D9898", Offset = "0x10D9898", VA = "0x1010D9898")]
	public int getInt(int row, int col)
	{
		return 0;
	}

	// Token: 0x060004DE RID: 1246 RVA: 0x00002DF0 File Offset: 0x00000FF0
	[Token(Token = "0x6000483")]
	[Address(RVA = "0x1010D98C8", Offset = "0x10D98C8", VA = "0x1010D98C8")]
	public float getFloat(int row, int col)
	{
		return 0f;
	}

	// Token: 0x060004DF RID: 1247 RVA: 0x00002E08 File Offset: 0x00001008
	[Token(Token = "0x6000484")]
	[Address(RVA = "0x1010D98F8", Offset = "0x10D98F8", VA = "0x1010D98F8")]
	public double getDouble(int row, int col)
	{
		return 0.0;
	}

	// Token: 0x040002EE RID: 750
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x40001C1")]
	private string m_path;

	// Token: 0x040002EF RID: 751
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40001C2")]
	private ArrayList m_strings;
}
