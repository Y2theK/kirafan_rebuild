﻿using System;
using System.Runtime.InteropServices;
using CharacterRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x02000062 RID: 98
[Token(Token = "0x200003B")]
[StructLayout(3)]
public static class CharacterRequest
{
	// Token: 0x06000205 RID: 517 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BE")]
	[Address(RVA = "0x1010D999C", Offset = "0x10D999C", VA = "0x1010D999C")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000206 RID: 518 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001BF")]
	[Address(RVA = "0x1010D9A28", Offset = "0x10D9A28", VA = "0x1010D9A28")]
	public static MeigewwwParam Getall([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000207 RID: 519 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C0")]
	[Address(RVA = "0x1010D9AB4", Offset = "0x10D9AB4", VA = "0x1010D9AB4")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000208 RID: 520 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C1")]
	[Address(RVA = "0x1010D9B84", Offset = "0x10D9B84", VA = "0x1010D9B84")]
	public static MeigewwwParam Get(string characterId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
