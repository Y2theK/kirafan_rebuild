﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CharacterResponseTypes;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

// Token: 0x02000086 RID: 134
[Token(Token = "0x200005F")]
[StructLayout(3)]
public static class CharacterResponse
{
	// Token: 0x06000353 RID: 851 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030C")]
	[Address(RVA = "0x1010D9C74", Offset = "0x10D9C74", VA = "0x1010D9C74")]
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000354 RID: 852 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030D")]
	[Address(RVA = "0x1010D9CDC", Offset = "0x10D9CDC", VA = "0x1010D9CDC")]
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
