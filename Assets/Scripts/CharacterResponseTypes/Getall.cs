﻿using CommonResponseTypes;
using WWWTypes;

namespace CharacterResponseTypes {
    public class Getall : CommonResponse {
        public Character[] characters;

        public override string ToMessage() {
            string str = base.ToMessage();
            return str + ((characters == null) ? string.Empty : characters.ToString());
        }
    }
}
