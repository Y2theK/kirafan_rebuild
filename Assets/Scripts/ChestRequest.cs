﻿using System;
using System.Runtime.InteropServices;
using ChestRequestTypes;
using Cpp2IlInjected;
using Meige;

// Token: 0x02000063 RID: 99
[Token(Token = "0x200003C")]
[StructLayout(3)]
public static class ChestRequest
{
	// Token: 0x06000209 RID: 521 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C2")]
	[Address(RVA = "0x1010D9E54", Offset = "0x10D9E54", VA = "0x1010D9E54")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020A RID: 522 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C3")]
	[Address(RVA = "0x1010D9F18", Offset = "0x10D9F18", VA = "0x1010D9F18")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020B RID: 523 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C4")]
	[Address(RVA = "0x1010D9FDC", Offset = "0x10D9FDC", VA = "0x1010D9FDC")]
	public static MeigewwwParam GetStep(GetStep param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020C RID: 524 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C5")]
	[Address(RVA = "0x1010DA144", Offset = "0x10DA144", VA = "0x1010DA144")]
	public static MeigewwwParam GetStep(int chestId, int step, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020D RID: 525 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C6")]
	[Address(RVA = "0x1010DA29C", Offset = "0x10DA29C", VA = "0x1010DA29C")]
	public static MeigewwwParam Draw(Draw param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020E RID: 526 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C7")]
	[Address(RVA = "0x1010DA404", Offset = "0x10DA404", VA = "0x1010DA404")]
	public static MeigewwwParam Draw(int chestId, int count, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600020F RID: 527 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C8")]
	[Address(RVA = "0x1010DA55C", Offset = "0x10DA55C", VA = "0x1010DA55C")]
	public static MeigewwwParam Reset(Reset param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000210 RID: 528 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001C9")]
	[Address(RVA = "0x1010DA684", Offset = "0x10DA684", VA = "0x1010DA684")]
	public static MeigewwwParam Reset(int chestId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
