﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ChestResponseTypes;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

// Token: 0x02000087 RID: 135
[Token(Token = "0x2000060")]
[StructLayout(3)]
public static class ChestResponse
{
	// Token: 0x06000355 RID: 853 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030E")]
	[Address(RVA = "0x1010DA7BC", Offset = "0x10DA7BC", VA = "0x1010DA7BC")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000356 RID: 854 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600030F")]
	[Address(RVA = "0x1010DA824", Offset = "0x10DA824", VA = "0x1010DA824")]
	public static GetStep GetStep(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000357 RID: 855 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000310")]
	[Address(RVA = "0x1010DA88C", Offset = "0x10DA88C", VA = "0x1010DA88C")]
	public static Draw Draw(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000358 RID: 856 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000311")]
	[Address(RVA = "0x1010DA8F4", Offset = "0x10DA8F4", VA = "0x1010DA8F4")]
	public static Reset Reset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
