﻿using CommonResponseTypes;
using WWWTypes;

namespace ChestResponseTypes {
    public class Draw : CommonResponse {
        public ChestInfo chest;
        public ChestPrizeResult[] prizeResults;
        public int addPresentAmount;
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon[] managedWeapons;
        public PlayerRoomObject[] managedRoomObjects;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (chest == null) ? string.Empty : chest.ToString();
            str += (prizeResults == null) ? string.Empty : prizeResults.ToString();
            str += addPresentAmount.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            return str;
        }
    }
}
