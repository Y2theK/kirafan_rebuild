﻿using CommonResponseTypes;
using WWWTypes;

namespace ChestResponseTypes {
    public class GetAll : CommonResponse {
        public ChestInfo[] chests;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (chests == null) ? string.Empty : chests.ToString();
            return str;
        }
    }
}
