﻿using CommonResponseTypes;
using WWWTypes;

namespace ChestResponseTypes {
    public class GetStep : CommonResponse {
        public ChestPrize[] prizes;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (prizes == null) ? string.Empty : prizes.ToString();
            return str;
        }
    }
}
