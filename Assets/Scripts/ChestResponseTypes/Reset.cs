﻿using CommonResponseTypes;
using WWWTypes;

namespace ChestResponseTypes {
    public class Reset : CommonResponse {
        public ChestInfo chest;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (chest == null) ? string.Empty : chest.ToString();
            return str;
        }
    }
}
