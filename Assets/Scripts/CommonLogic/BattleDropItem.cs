﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002CE RID: 718
	[Token(Token = "0x200027A")]
	[Serializable]
	[StructLayout(3)]
	public class BattleDropItem
	{
		// Token: 0x06000899 RID: 2201 RVA: 0x00003960 File Offset: 0x00001B60
		[Token(Token = "0x6000805")]
		[Address(RVA = "0x1010DACB4", Offset = "0x10DACB4", VA = "0x1010DACB4")]
		public bool IsDrop()
		{
			return default(bool);
		}

		// Token: 0x0600089A RID: 2202 RVA: 0x00003978 File Offset: 0x00001B78
		[Token(Token = "0x6000806")]
		[Address(RVA = "0x1010DACD0", Offset = "0x10DACD0", VA = "0x1010DACD0")]
		public int GetDropDataNum()
		{
			return 0;
		}

		// Token: 0x0600089B RID: 2203 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000807")]
		[Address(RVA = "0x1010DAD30", Offset = "0x10DAD30", VA = "0x1010DAD30")]
		public BattleDropItem.DropData GetDropDataAt(int index)
		{
			return null;
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000808")]
		[Address(RVA = "0x1010DADA0", Offset = "0x10DADA0", VA = "0x1010DADA0")]
		public BattleDropItem()
		{
		}

		// Token: 0x04000A4A RID: 2634
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000898")]
		public List<BattleDropItem.DropData> Datas;

		// Token: 0x020002CF RID: 719
		[Token(Token = "0x2000D2B")]
		[Serializable]
		public class DropData
		{
			// Token: 0x0600089D RID: 2205 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5A")]
			[Address(RVA = "0x1010DAE10", Offset = "0x10DAE10", VA = "0x1010DAE10")]
			public DropData()
			{
			}

			// Token: 0x04000A4B RID: 2635
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400553C")]
			public int ID;

			// Token: 0x04000A4C RID: 2636
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400553D")]
			public int Num;

			// Token: 0x04000A4D RID: 2637
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400553E")]
			public int ExtNum;
		}
	}
}
