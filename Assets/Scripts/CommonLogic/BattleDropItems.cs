﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002CD RID: 717
	[Token(Token = "0x2000279")]
	[Serializable]
	[StructLayout(3)]
	public class BattleDropItems
	{
		// Token: 0x06000898 RID: 2200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000804")]
		[Address(RVA = "0x1010DAE18", Offset = "0x10DAE18", VA = "0x1010DAE18")]
		public BattleDropItems()
		{
		}

		// Token: 0x04000A49 RID: 2633
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000897")]
		public BattleDropItem[] Items;
	}
}
