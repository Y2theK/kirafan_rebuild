﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002CA RID: 714
	[Token(Token = "0x2000278")]
	[Serializable]
	[StructLayout(3)]
	public class BattleOccurEnemy
	{
		// Token: 0x06000891 RID: 2193 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60007FF")]
		[Address(RVA = "0x1010DAE20", Offset = "0x10DAE20", VA = "0x1010DAE20")]
		public List<BattleOccurEnemy.OneWave> GetWaves()
		{
			return null;
		}

		// Token: 0x06000892 RID: 2194 RVA: 0x00003918 File Offset: 0x00001B18
		[Token(Token = "0x6000800")]
		[Address(RVA = "0x1010DAE28", Offset = "0x10DAE28", VA = "0x1010DAE28")]
		public int GetWaveNum()
		{
			return 0;
		}

		// Token: 0x06000893 RID: 2195 RVA: 0x00003930 File Offset: 0x00001B30
		[Token(Token = "0x6000801")]
		[Address(RVA = "0x1010DAE88", Offset = "0x10DAE88", VA = "0x1010DAE88")]
		public long CalcPoint(int waveIdx)
		{
			return 0L;
		}

		// Token: 0x06000894 RID: 2196 RVA: 0x00003948 File Offset: 0x00001B48
		[Token(Token = "0x6000802")]
		[Address(RVA = "0x1010DB010", Offset = "0x10DB010", VA = "0x1010DB010")]
		public bool IsExistPointEnemy()
		{
			return default(bool);
		}

		// Token: 0x06000895 RID: 2197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000803")]
		[Address(RVA = "0x1010DB188", Offset = "0x10DB188", VA = "0x1010DB188")]
		public BattleOccurEnemy()
		{
		}

		// Token: 0x04000A42 RID: 2626
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000896")]
		public List<BattleOccurEnemy.OneWave> m_Waves;

		// Token: 0x020002CB RID: 715
		[Token(Token = "0x2000D29")]
		[Serializable]
		public class OneWave
		{
			// Token: 0x06000896 RID: 2198 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D58")]
			[Address(RVA = "0x1010DB1FC", Offset = "0x10DB1FC", VA = "0x1010DB1FC")]
			public OneWave()
			{
			}

			// Token: 0x04000A43 RID: 2627
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005536")]
			public List<BattleOccurEnemy.OneEnemy> m_Enemies;
		}

		// Token: 0x020002CC RID: 716
		[Token(Token = "0x2000D2A")]
		[Serializable]
		public class OneEnemy
		{
			// Token: 0x06000897 RID: 2199 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D59")]
			[Address(RVA = "0x1010DB190", Offset = "0x10DB190", VA = "0x1010DB190")]
			public OneEnemy(int enemyID, int enemyLv, int dropID, float scale, long point)
			{
			}

			// Token: 0x04000A44 RID: 2628
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005537")]
			public int m_EnemyID;

			// Token: 0x04000A45 RID: 2629
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005538")]
			public int m_EnemyLv;

			// Token: 0x04000A46 RID: 2630
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005539")]
			public int m_DropID;

			// Token: 0x04000A47 RID: 2631
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x400553A")]
			public float m_Scale;

			// Token: 0x04000A48 RID: 2632
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400553B")]
			public long m_Point;
		}
	}
}
