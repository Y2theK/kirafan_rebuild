﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002D9 RID: 729
	[Token(Token = "0x2000284")]
	[StructLayout(3)]
	public class CalcDropItemListCondition
	{
		// Token: 0x060008AA RID: 2218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000815")]
		[Address(RVA = "0x1010DB270", Offset = "0x10DB270", VA = "0x1010DB270")]
		private CalcDropItemListCondition()
		{
		}

		// Token: 0x060008AB RID: 2219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000816")]
		[Address(RVA = "0x1010DB278", Offset = "0x10DB278", VA = "0x1010DB278")]
		public CalcDropItemListCondition(long systemTime, DropItemTool dropItemTool)
		{
		}

		// Token: 0x04000A6F RID: 2671
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40008BA")]
		public long m_SystemTime;

		// Token: 0x04000A70 RID: 2672
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40008BB")]
		public DropItemTool m_DropItemTool;
	}
}
