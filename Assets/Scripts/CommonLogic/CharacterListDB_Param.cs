﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FA RID: 762
	[Token(Token = "0x200029E")]
	[StructLayout(0, Size = 32)]
	public struct CharacterListDB_Param
	{
		// Token: 0x04000ACF RID: 2767
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40008F7")]
		public int m_CharaID;

		// Token: 0x04000AD0 RID: 2768
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40008F8")]
		public string m_Name;

		// Token: 0x04000AD1 RID: 2769
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40008F9")]
		public int m_NamedType;

		// Token: 0x04000AD2 RID: 2770
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40008FA")]
		public int m_Rare;

		// Token: 0x04000AD3 RID: 2771
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40008FB")]
		public int m_Class;

		// Token: 0x04000AD4 RID: 2772
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40008FC")]
		public int m_Element;
	}
}
