﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200031E RID: 798
	[Token(Token = "0x20002B2")]
	[StructLayout(3)]
	public class CharacterParam
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000982 RID: 2434 RVA: 0x00004350 File Offset: 0x00002550
		// (set) Token: 0x06000983 RID: 2435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000061")]
		public long MngID
		{
			[Token(Token = "0x60008BF")]
			[Address(RVA = "0x1010DB2B0", Offset = "0x10DB2B0", VA = "0x1010DB2B0")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x60008C0")]
			[Address(RVA = "0x1010DB2B8", Offset = "0x10DB2B8", VA = "0x1010DB2B8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000984 RID: 2436 RVA: 0x00004368 File Offset: 0x00002568
		// (set) Token: 0x06000985 RID: 2437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000062")]
		public uint UniqueID
		{
			[Token(Token = "0x60008C1")]
			[Address(RVA = "0x1010DB2C0", Offset = "0x10DB2C0", VA = "0x1010DB2C0")]
			[CompilerGenerated]
			get
			{
				return 0U;
			}
			[Token(Token = "0x60008C2")]
			[Address(RVA = "0x1010DB2C8", Offset = "0x10DB2C8", VA = "0x1010DB2C8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000986 RID: 2438 RVA: 0x00004380 File Offset: 0x00002580
		// (set) Token: 0x06000987 RID: 2439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000063")]
		public int CharaID
		{
			[Token(Token = "0x60008C3")]
			[Address(RVA = "0x1010DB2D0", Offset = "0x10DB2D0", VA = "0x1010DB2D0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60008C4")]
			[Address(RVA = "0x1010DB2D8", Offset = "0x10DB2D8", VA = "0x1010DB2D8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000988 RID: 2440 RVA: 0x00004398 File Offset: 0x00002598
		// (set) Token: 0x06000989 RID: 2441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000064")]
		public int NamedType
		{
			[Token(Token = "0x60008C5")]
			[Address(RVA = "0x1010DB2E0", Offset = "0x10DB2E0", VA = "0x1010DB2E0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60008C6")]
			[Address(RVA = "0x1010DB2E8", Offset = "0x10DB2E8", VA = "0x1010DB2E8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600098A RID: 2442 RVA: 0x000043B0 File Offset: 0x000025B0
		// (set) Token: 0x0600098B RID: 2443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000065")]
		public int RareType
		{
			[Token(Token = "0x60008C7")]
			[Address(RVA = "0x1010DB2F0", Offset = "0x10DB2F0", VA = "0x1010DB2F0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60008C8")]
			[Address(RVA = "0x1010DB2F8", Offset = "0x10DB2F8", VA = "0x1010DB2F8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600098C RID: 2444 RVA: 0x000043C8 File Offset: 0x000025C8
		// (set) Token: 0x0600098D RID: 2445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000066")]
		public int ClassType
		{
			[Token(Token = "0x60008C9")]
			[Address(RVA = "0x1010DB300", Offset = "0x10DB300", VA = "0x1010DB300")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60008CA")]
			[Address(RVA = "0x1010DB308", Offset = "0x10DB308", VA = "0x1010DB308")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x000043E0 File Offset: 0x000025E0
		// (set) Token: 0x0600098F RID: 2447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000067")]
		public int ElementType
		{
			[Token(Token = "0x60008CB")]
			[Address(RVA = "0x1010DB310", Offset = "0x10DB310", VA = "0x1010DB310")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60008CC")]
			[Address(RVA = "0x1010DB318", Offset = "0x10DB318", VA = "0x1010DB318")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06000990 RID: 2448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008CD")]
		[Address(RVA = "0x1010DB320", Offset = "0x10DB320", VA = "0x1010DB320")]
		public CharacterParam()
		{
		}
	}
}
