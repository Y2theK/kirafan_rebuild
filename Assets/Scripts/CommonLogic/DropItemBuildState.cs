﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002D4 RID: 724
	[Token(Token = "0x200027F")]
	[StructLayout(3)]
	public class DropItemBuildState
	{
		// Token: 0x060008A4 RID: 2212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600080F")]
		[Address(RVA = "0x1010DB34C", Offset = "0x10DB34C", VA = "0x1010DB34C")]
		private DropItemBuildState()
		{
		}

		// Token: 0x060008A5 RID: 2213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000810")]
		[Address(RVA = "0x1010DB354", Offset = "0x10DB354", VA = "0x1010DB354")]
		public DropItemBuildState(bool isOpen, long actionTime, int level)
		{
		}

		// Token: 0x04000A5E RID: 2654
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40008A9")]
		public bool m_IsOpen;

		// Token: 0x04000A5F RID: 2655
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40008AA")]
		public long m_ActionTime;

		// Token: 0x04000A60 RID: 2656
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40008AB")]
		public int m_Level;
	}
}
