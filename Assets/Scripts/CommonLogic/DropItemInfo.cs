﻿namespace CommonLogic {
    public struct DropItemInfo {
        public int m_CalcNum;
        public bool m_Max;
        public long m_UpTime;
        public int m_LimitNum;
        public int m_TableID;
        public int m_FirstDropID;

        public void CalcTimeToUp(long ftime) {
            int drops = m_UpTime != 0 ? (int)(ftime / m_UpTime) : 0;
            m_CalcNum = drops;
            m_Max = false;
            if (drops >= m_LimitNum) {
                m_CalcNum = m_LimitNum;
                m_Max = true;
            }
        }
    }
}
