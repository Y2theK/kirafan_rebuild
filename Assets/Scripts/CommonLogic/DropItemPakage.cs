﻿namespace CommonLogic {
    public struct DropItemPakage {
        public int m_Level;
        public int m_LimitNum;
        public int m_ChkTime;
        public DropItemData[] m_Table;

        public int CalcItemIndex() {
            int idx = 0;
            int i = UnityEngine.Random.Range(0, 10000);
            while (true) {
                if (idx >= m_Table.Length) {
                    idx = 0;
                    break;
                }
                i -= m_Table[idx].m_WakeUp;
                if (i <= 0) {
                    break;
                }
                idx++;
            }
            return idx;
        }

        public bool IsChkListUpOneItem() {
            return m_Table.Length <= 1;
        }
    }
}
