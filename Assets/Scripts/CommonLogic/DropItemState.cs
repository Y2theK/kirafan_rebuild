﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002DA RID: 730
	[Token(Token = "0x2000285")]
	[StructLayout(0, Size = 8)]
	public struct DropItemState
	{
		// Token: 0x04000A71 RID: 2673
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40008BC")]
		public int m_ResultNo;

		// Token: 0x04000A72 RID: 2674
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40008BD")]
		public int m_AddNum;
	}
}
