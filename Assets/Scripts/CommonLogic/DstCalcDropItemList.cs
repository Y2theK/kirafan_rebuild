﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002DB RID: 731
	[Token(Token = "0x2000286")]
	[StructLayout(3)]
	public class DstCalcDropItemList
	{
		// Token: 0x060008AC RID: 2220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000817")]
		[Address(RVA = "0x1010DB48C", Offset = "0x10DB48C", VA = "0x1010DB48C")]
		public DstCalcDropItemList()
		{
		}

		// Token: 0x04000A73 RID: 2675
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40008BE")]
		public DropItemState[] m_List;

		// Token: 0x04000A74 RID: 2676
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40008BF")]
		public long m_ActionTime;
	}
}
