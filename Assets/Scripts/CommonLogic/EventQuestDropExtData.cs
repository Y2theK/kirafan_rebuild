﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C9 RID: 713
	[Token(Token = "0x2000277")]
	[StructLayout(3)]
	public class EventQuestDropExtData
	{
		// Token: 0x0600088F RID: 2191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007FD")]
		[Address(RVA = "0x1010DB494", Offset = "0x10DB494", VA = "0x1010DB494")]
		public EventQuestDropExtData(int eventType, int charaID, int[] itemIDs, int[] pluses)
		{
		}

		// Token: 0x06000890 RID: 2192 RVA: 0x00003900 File Offset: 0x00001B00
		[Token(Token = "0x60007FE")]
		[Address(RVA = "0x1010DB4E0", Offset = "0x10DB4E0", VA = "0x1010DB4E0")]
		public int GetPlusNum(int searchItemID)
		{
			return 0;
		}

		// Token: 0x04000A3E RID: 2622
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000892")]
		public int m_EventType;

		// Token: 0x04000A3F RID: 2623
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000893")]
		public int m_CharaID;

		// Token: 0x04000A40 RID: 2624
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000894")]
		public int[] m_ItemIDs;

		// Token: 0x04000A41 RID: 2625
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000895")]
		public int[] m_Pluses;
	}
}
