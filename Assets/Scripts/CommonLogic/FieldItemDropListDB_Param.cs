﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FF RID: 767
	[Token(Token = "0x20002A3")]
	[StructLayout(0, Size = 32)]
	public struct FieldItemDropListDB_Param
	{
		// Token: 0x04000AFD RID: 2813
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000925")]
		public int m_ID;

		// Token: 0x04000AFE RID: 2814
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000926")]
		public int[] m_Num;

		// Token: 0x04000AFF RID: 2815
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000927")]
		public byte[] m_Category;

		// Token: 0x04000B00 RID: 2816
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000928")]
		public int[] m_ObjectID;
	}
}
