﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002BE RID: 702
	[Token(Token = "0x2000271")]
	[StructLayout(3)]
	public class FieldObjDropItem
	{
		// Token: 0x06000880 RID: 2176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007F5")]
		[Address(RVA = "0x1010DB5C8", Offset = "0x10DB5C8", VA = "0x1010DB5C8")]
		public void CheckDropItem(int fresno, int fadd)
		{
		}

		// Token: 0x06000881 RID: 2177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007F6")]
		[Address(RVA = "0x1010DB8F8", Offset = "0x10DB8F8", VA = "0x1010DB8F8")]
		public FieldObjDropItem()
		{
		}

		// Token: 0x04000A18 RID: 2584
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000877")]
		public int m_OptionKey;

		// Token: 0x04000A19 RID: 2585
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000878")]
		public FieldObjDropItem.DropItemState[] m_List;

		// Token: 0x04000A1A RID: 2586
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000879")]
		public FieldObjDropItem.DropItemInfo m_Info;

		// Token: 0x020002BF RID: 703
		[Token(Token = "0x2000D24")]
		public struct DropItemInfo
		{
			// Token: 0x06000882 RID: 2178 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D51")]
			[Address(RVA = "0x10002E5B4", Offset = "0x2E5B4", VA = "0x10002E5B4")]
			public void CalcTimeToUp(long ftime)
			{
			}

			// Token: 0x04000A1B RID: 2587
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400552B")]
			public int m_CalcNum;

			// Token: 0x04000A1C RID: 2588
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x400552C")]
			public bool m_Max;

			// Token: 0x04000A1D RID: 2589
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400552D")]
			public long m_UpTime;

			// Token: 0x04000A1E RID: 2590
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400552E")]
			public int m_LimitNum;

			// Token: 0x04000A1F RID: 2591
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400552F")]
			public int m_TableID;

			// Token: 0x04000A20 RID: 2592
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005530")]
			public int m_FirstDropID;
		}

		// Token: 0x020002C0 RID: 704
		[Token(Token = "0x2000D25")]
		public struct DropItemState
		{
			// Token: 0x04000A21 RID: 2593
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005531")]
			public int m_ResultNo;

			// Token: 0x04000A22 RID: 2594
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005532")]
			public int m_AddNum;
		}
	}
}
