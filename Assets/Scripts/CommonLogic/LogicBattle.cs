﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C1 RID: 705
	[Token(Token = "0x2000272")]
	[StructLayout(3)]
	public static class LogicBattle
	{
		// Token: 0x06000883 RID: 2179 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60007F7")]
		[Address(RVA = "0x1010DB900", Offset = "0x10DB900", VA = "0x1010DB900")]
		public static BattleOccurEnemy CreateOccurEnemy(List<int> waveIDs, List<QuestWaveListData> questWaveList, List<QuestWaveRandomListData> questWaveRandomList)
		{
			return null;
		}

		// Token: 0x06000884 RID: 2180 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60007F8")]
		[Address(RVA = "0x1010DC070", Offset = "0x10DC070", VA = "0x1010DC070")]
		public static EventQuestDropExtData FindEventQuestDropExtData(List<EventQuestDropExtData> eventQuestDropExtList, int eventType, int charaID)
		{
			return null;
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60007F9")]
		[Address(RVA = "0x1010DC1A0", Offset = "0x10DC1A0", VA = "0x1010DC1A0")]
		public static List<BattleDropItems> CreateDropItemsList(BattleOccurEnemy occurEnemy, List<QuestWaveDropItemData> questWaveDropItemList, List<EventQuestDropExtData> eventQuestDropExtList, int eventType, List<int> selfCharaIDs, int friendCharaID)
		{
			return null;
		}

		// Token: 0x04000A23 RID: 2595
		[Token(Token = "0x400087A")]
		public const int DROP_TARGET_NUM = 3;
	}
}
