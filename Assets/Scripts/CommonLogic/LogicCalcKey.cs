﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CommonLogic {
    public class LogicCalcKey {
        private void Encrypt(byte[] src, string encryptKey, string pw, out byte[] dst) {
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] keyBytes = Encoding.UTF8.GetBytes(encryptKey);
                byte[] pwBytes = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateDecryptor(keyBytes, pwBytes))
                using (var stream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(src, 0, src.Length);
                    cryptoStream.FlushFinalBlock();
                    dst = stream.ToArray();
                }
            }

        }

        private void Decrypt(byte[] src, string encryptKey, string pw, out byte[] dst) {
            dst = new byte[src.Length];
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] keyBytes = Encoding.UTF8.GetBytes(encryptKey);
                byte[] pwBytes = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateDecryptor(keyBytes, pwBytes))
                using (var stream = new MemoryStream(src))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read)) {
                    cryptoStream.Read(dst, 0, dst.Length);
                }
            }
        }

        public string MakeKey(byte[] b, string encryptKey, string ivec) {
            byte[] bytes = new byte[b.Length];
            XorShift shift = new XorShift();
            for (int i = 0; i < b.Length; i++) {
                int shifted = b[i] - (int)(shift.Get() & 0x7f);
                if (shifted < 0) {
                    shifted += TableDefine.RdTble.Length;
                }
                bytes[b.Length - 1 - i] = (byte)(shifted % TableDefine.RdTble.Length);
            }
            Encrypt(bytes, encryptKey, ivec, out bytes);
            return Convert.ToBase64String(bytes);
        }

        public string Calc(string k, string encryptKey, string ivec) {
            return Calc(Convert.FromBase64String(k), encryptKey, ivec);
        }

        public string Calc(byte[] b, string encryptKey, string ivec) {
            Decrypt(b, encryptKey, ivec, out byte[] decrypted);
            int max = decrypted.Length - 1;
            if (max >= 0) {
                int res = 0;
                while (true) {
                    if (decrypted[max] != 0) { break; }
                    if (max == 0) {
                        res = decrypted.Length;
                        break;
                    }
                    res++;
                    max--;
                }
                if (res > 0) {
                    byte[] temp = new byte[decrypted.Length - res];
                    for (int i = 0; i < temp.Length; i++) {
                        temp[i] = decrypted[i];
                    }
                    decrypted = temp;
                }
            }
            XorShift shift = new XorShift();
            string ret = "";
            for (int i = decrypted.Length - 1; i >= 0; i--) {
                int rdIndex = decrypted[i] + (int)(shift.Get() & 0x7f);
                ret += TableDefine.RdTble[rdIndex % TableDefine.RdTble.Length].ToString();
            }
            return ret;
        }
    }
}
