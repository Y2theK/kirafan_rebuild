﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002D3 RID: 723
	[Token(Token = "0x200027E")]
	[StructLayout(3)]
	public static class LogicDefine
	{
		// Token: 0x04000A5B RID: 2651
		[Token(Token = "0x40008A6")]
		public const int INVALID_MNG_ID = -1;

		// Token: 0x04000A5C RID: 2652
		[Token(Token = "0x40008A7")]
		public const int INVALID_ID = -1;

		// Token: 0x04000A5D RID: 2653
		[Token(Token = "0x40008A8")]
		public const uint U_INVALID_ID = 0U;
	}
}
