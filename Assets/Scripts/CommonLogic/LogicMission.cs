﻿using System.Collections.Generic;

namespace CommonLogic {
    public static class LogicMission {
        public static int UpdateSystemLogin(MissionValue value, MissionSystemCondition condition) {
            value.m_Rate = condition.m_LoginDays;
            return value.m_Rate;
        }

        public static bool CheckSystemLogin(MissionValue value, MissionSystemCondition condition) {
            return condition.m_LoginDays >= value.m_MaxRate;
        }

        public static int UpdateSystemContinutiyLogin(MissionValue value, MissionSystemCondition condition) {
            return ++value.m_Rate;
        }

        public static bool CheckSystemContinutiyLogin(MissionValue value, MissionSystemCondition condition) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateSystemMissionAllComplete(MissionAllCompleteValue value, List<MissionAllCompleteCondition> condition) {
            int count = 0;
            for (int i = 0; i < condition.Count; i++) {
                if (condition[i].m_Category == value.m_Category && condition[i].m_State > 0) {
                    count++;
                }
            }
            value.m_Rate = count;
            return value.m_Rate;
        }

        public static bool CheckSystemMissionAllComplete(MissionAllCompleteValue value, List<MissionAllCompleteCondition> condition) {
            int count = 0;
            int totalCount = 0;
            for (int i = 0; i < condition.Count; i++) {
                if (condition[i].m_Category == value.m_Category) {
                    totalCount++;
                    if (condition[i].m_State > 0) {
                        count++;
                    }
                }
            }
            return count >= totalCount;
        }

        public static int UpdateSystemUserRank(MissionValue value, MissionSystemCondition condition) {
            value.m_Rate = condition.m_UserLevel;
            return value.m_Rate;
        }

        public static bool CheckSystemUserRank(MissionValue value, MissionSystemCondition condition) {
            return condition.m_UserLevel >= value.m_MaxRate;
        }

        public static int UpdateEditBattlePartyEdit(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditBattlePartyEdit(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditSupportPartyEdit(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditSupportPartyEdit(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditCharaStrength(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditCharaStrength(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditCharaLimitBreak(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditCharaLimitBreak(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditCharaEvolution(MissionEvolutionValue value, MissionEditEvolutionCondition condition) {
            value.m_Rate = condition.m_EvolutionNum >= value.m_CheckNum ? 1 : 0;
            return value.m_Rate;
        }

        public static bool CheckEditCharaEvolution(MissionEvolutionValue value, MissionEditEvolutionCondition condition) {
            return condition.m_EvolutionNum >= value.m_CheckNum;
        }

        public static int UpdateEditWeaponMake(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditWeaponMake(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditWeaponStrength(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckEditWeaponStrength(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateEditWeaponEquipment(MissionValue value, MissionEditWeaponEquipCondition condition) {
            for (int i = 0; i < condition.m_WeaponEquipChanges.Length; i++) {
                if (condition.m_WeaponEquipChanges[i].CheckChanged()) {
                    value.m_Rate++;
                    break;
                }
            }
            return value.m_Rate;
        }

        public static bool CheckEditWeaponEquipment(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateBattleQuestClear(MissionValue value, MissionBattleQuestClearCondition condition) {
            if (condition.m_IsBattle && condition.m_IsClear) {
                value.m_Rate++;
            }
            return value.m_Rate;
        }

        public static bool CheckBattleQuestClear(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateTownCharaTouchItem(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckTownCharaTouchItem(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateTownContentBuild(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckTownContentBuild(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateTownBufBuild(MissionValue value, MissionTownBuildCondition condition) {
            if (condition.m_NextLevel == 1 && condition.m_OpenState == 1) {
                value.m_Rate++;
            }
            return value.m_Rate;
        }

        public static bool CheckTownBufBuild(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateTownIconTap(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckTownIconTap(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateRoomObjectSet(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckRoomObjectSet(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateRoomObjectRemove(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckRoomObjectRemove(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateRoomCharaTouch(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckRoomCharaTouch(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateRoomVisit(MissionValue value) {
            return ++value.m_Rate;
        }

        public static bool CheckRoomVisit(MissionValue value) {
            return value.m_Rate >= value.m_MaxRate;
        }

        public static int UpdateCharaFriendShip(MissionCharaFriendShipValue value, MissionCharaFriendShipCondition condition) {
            for (int i = 0; i < condition.m_NamedTypes.Length; i++) {
                if (condition.m_NamedTypes[i] == value.m_SubCode) {
                    value.m_Rate = condition.m_Levels[i];
                }
            }
            return value.m_Rate;
        }

        public static bool CheckCharaFriendShip(MissionCharaFriendShipValue value, MissionCharaFriendShipCondition condition) {
            for (int i = 0; i < condition.m_NamedTypes.Length; i++) {
                if (condition.m_NamedTypes[i] == value.m_SubCode) {
                    return condition.m_Levels[i] >= value.m_MaxRate;
                }
            }
            return false;
        }
    }
}
