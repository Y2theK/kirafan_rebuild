﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star;

namespace CommonLogic
{
	// Token: 0x020002EB RID: 747
	[Token(Token = "0x2000296")]
	[StructLayout(3)]
	public static class LogicSchedule
	{
		// Token: 0x060008F6 RID: 2294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000861")]
		[Address(RVA = "0x1010DEF54", Offset = "0x10DEF54", VA = "0x1010DEF54")]
		public static void CopyUserCharacterData(UserCharacterData src, ref UserCharacterData dst)
		{
		}

		// Token: 0x060008F7 RID: 2295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000862")]
		[Address(RVA = "0x1010DF2F8", Offset = "0x10DF2F8", VA = "0x1010DF2F8")]
		public static void CopyUserTownData(UserTownData src, ref UserTownData dst)
		{
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000863")]
		[Address(RVA = "0x1010DF52C", Offset = "0x10DF52C", VA = "0x1010DF52C")]
		public static List<int> GetPossibleDropItemList(int tagName, UserCharacterData charaData, UserTownData townData)
		{
			return null;
		}

		// Token: 0x060008F9 RID: 2297 RVA: 0x00003E10 File Offset: 0x00002010
		[Token(Token = "0x6000864")]
		[Address(RVA = "0x1010DFA68", Offset = "0x10DFA68", VA = "0x1010DFA68")]
		private static long SearchNowAccessBuildObject(int nowNameTag, UserCharacterData charaData, UserTownData townData)
		{
			return 0L;
		}

		// Token: 0x060008FA RID: 2298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000865")]
		[Address(RVA = "0x1010DFEA4", Offset = "0x10DFEA4", VA = "0x1010DFEA4")]
		public static void LotteryDropPresent(int nowNameTag, long atTimeTick, UserCharacterData charaData, UserTownData townData, ref List<LogicSchedule.DropPresent> dropPresentList)
		{
		}

		// Token: 0x060008FB RID: 2299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000866")]
		[Address(RVA = "0x1010E07A4", Offset = "0x10E07A4", VA = "0x1010E07A4")]
		public static void LotteryDropPresent(UserScheduleData.ListPack schedule, UserCharacterData charaData, UserTownData townData, int day, long startDaysTimeSec, long endDaysTimeSec, ref List<LogicSchedule.DropPresent> dropPresentList)
		{
		}

		// Token: 0x060008FC RID: 2300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000867")]
		[Address(RVA = "0x1010E0B10", Offset = "0x10E0B10", VA = "0x1010E0B10")]
		public static void LotteryTapItemRef(int nowNameTag, ref int tapItemNo, ref int flag)
		{
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000868")]
		[Address(RVA = "0x1010E0FB8", Offset = "0x10E0FB8", VA = "0x1010E0FB8")]
		public static void LotteryTapItem(int nowNameTag, out int tapItemNo, out int flag)
		{
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000869")]
		[Address(RVA = "0x1010E12D0", Offset = "0x10E12D0", VA = "0x1010E12D0")]
		public static void LotteryTapItem(UserScheduleData.ListPack listPack, long pastDayTimeSec, long nowDayTimeSec, ref int tapItemNo, ref int flag)
		{
		}

		// Token: 0x060008FF RID: 2303 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600086A")]
		[Address(RVA = "0x1010E1694", Offset = "0x10E1694", VA = "0x1010E1694")]
		public static LogicSchedule.Response GetScheduleCore(LogicSchedule.CreateListParam param)
		{
			return null;
		}

		// Token: 0x04000A89 RID: 2697
		[Token(Token = "0x40008D4")]
		public const int TOUCH_ITEM_NEW = 1;

		// Token: 0x04000A8A RID: 2698
		[Token(Token = "0x40008D5")]
		public const int TOUCH_ITEM_SP = 2;

		// Token: 0x04000A8B RID: 2699
		[Token(Token = "0x40008D6")]
		public const int CHANGE_SCHEDULE = 4;

		// Token: 0x04000A8C RID: 2700
		[Token(Token = "0x40008D7")]
		public const int NEW_SCHEDULE = 8;

		// Token: 0x04000A8D RID: 2701
		[Token(Token = "0x40008D8")]
		public const int SCHEDULE_CLEAR = 16;

		// Token: 0x04000A8E RID: 2702
		[Token(Token = "0x40008D9")]
		public const int SCHEDULE_MARK = 32;

		// Token: 0x020002EC RID: 748
		[Token(Token = "0x2000D2C")]
		public enum eType
		{
			// Token: 0x04000A90 RID: 2704
			[Token(Token = "0x4005540")]
			None,
			// Token: 0x04000A91 RID: 2705
			[Token(Token = "0x4005541")]
			New,
			// Token: 0x04000A92 RID: 2706
			[Token(Token = "0x4005542")]
			ChangeDay,
			// Token: 0x04000A93 RID: 2707
			[Token(Token = "0x4005543")]
			Get
		}

		// Token: 0x020002ED RID: 749
		[Token(Token = "0x2000D2D")]
		public class CreateListParam
		{
			// Token: 0x06000900 RID: 2304 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5B")]
			[Address(RVA = "0x1010E2BA8", Offset = "0x10E2BA8", VA = "0x1010E2BA8")]
			public CreateListParam()
			{
			}

			// Token: 0x04000A94 RID: 2708
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005544")]
			public DateTime m_NowTime;

			// Token: 0x04000A95 RID: 2709
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005545")]
			public bool m_FixRoom;

			// Token: 0x04000A96 RID: 2710
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005546")]
			public UserCharacterData m_UserCharacterData;

			// Token: 0x04000A97 RID: 2711
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005547")]
			public UserTownData m_UserTownData;

			// Token: 0x04000A98 RID: 2712
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005548")]
			public string m_LastScheduleTable;

			// Token: 0x04000A99 RID: 2713
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005549")]
			public int m_LastTouchItemResultNo;

			// Token: 0x04000A9A RID: 2714
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x400554A")]
			public int m_LastFlag;
		}

		// Token: 0x020002EE RID: 750
		[Token(Token = "0x2000D2E")]
		public class DropPresent
		{
			// Token: 0x06000901 RID: 2305 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5C")]
			[Address(RVA = "0x1010E0754", Offset = "0x10E0754", VA = "0x1010E0754")]
			public DropPresent(DateTime at, int dropPresentID)
			{
			}

			// Token: 0x06000902 RID: 2306 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5D")]
			[Address(RVA = "0x1010E2BB8", Offset = "0x10E2BB8", VA = "0x1010E2BB8")]
			public DropPresent(DateTime at, int dropPresentID, float mag)
			{
			}

			// Token: 0x04000A9B RID: 2715
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400554B")]
			public int itemNo;

			// Token: 0x04000A9C RID: 2716
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400554C")]
			public long presentAt;

			// Token: 0x04000A9D RID: 2717
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400554D")]
			public float magnification;
		}

		// Token: 0x020002EF RID: 751
		[Token(Token = "0x2000D2F")]
		public struct ScheduleMember
		{
			// Token: 0x04000A9E RID: 2718
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400554E")]
			public long managedPartyMemberId;

			// Token: 0x04000A9F RID: 2719
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400554F")]
			public int scheduleId;

			// Token: 0x04000AA0 RID: 2720
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4005550")]
			public int scheduleTag;

			// Token: 0x04000AA1 RID: 2721
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005551")]
			public long managedFacilityId;

			// Token: 0x04000AA2 RID: 2722
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005552")]
			public int touchItemResultNo;

			// Token: 0x04000AA3 RID: 2723
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005553")]
			public int flag;

			// Token: 0x04000AA4 RID: 2724
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005554")]
			public string scheduleTable;

			// Token: 0x04000AA5 RID: 2725
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005555")]
			public LogicSchedule.DropPresent[] dropPresent;
		}

		// Token: 0x020002F0 RID: 752
		[Token(Token = "0x2000D30")]
		public class Response
		{
			// Token: 0x06000903 RID: 2307 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5E")]
			[Address(RVA = "0x1010E27EC", Offset = "0x10E27EC", VA = "0x1010E27EC")]
			public Response()
			{
			}

			// Token: 0x04000AA6 RID: 2726
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005556")]
			public LogicSchedule.ScheduleMember m_ScheduleMember;
		}
	}
}
