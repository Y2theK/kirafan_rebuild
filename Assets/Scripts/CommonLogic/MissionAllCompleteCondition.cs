﻿namespace CommonLogic {
    public class MissionAllCompleteCondition : MissionConditionBase {
        public int m_Category;
        public int m_State;

        private MissionAllCompleteCondition() { }

        public MissionAllCompleteCondition(int category, int state) {
            m_Category = category;
            m_State = state;
        }
    }
}
