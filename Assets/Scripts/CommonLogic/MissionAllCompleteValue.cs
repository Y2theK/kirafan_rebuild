﻿namespace CommonLogic {
    public class MissionAllCompleteValue : MissionValue {
        public int m_Category;

        private MissionAllCompleteValue() { }

        public MissionAllCompleteValue(int rate, int maxRate, int category) {
            m_Rate = rate;
            m_MaxRate = maxRate;
            m_Category = category;
        }
    }
}
