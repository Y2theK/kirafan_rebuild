﻿namespace CommonLogic {
    public class MissionBattleQuestClearCondition : MissionConditionBase {
        public bool m_IsBattle;
        public bool m_IsClear;

        private MissionBattleQuestClearCondition() { }

        public MissionBattleQuestClearCondition(bool isBattle, bool isClear) {
            m_IsBattle = isBattle;
            m_IsClear = isClear;
        }
    }
}
