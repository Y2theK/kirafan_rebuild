﻿namespace CommonLogic {
    public class MissionCharaFriendShipCondition : MissionConditionBase {
        public int[] m_NamedTypes;
        public int[] m_Levels;

        private MissionCharaFriendShipCondition() { }

        public MissionCharaFriendShipCondition(int[] namedTypes, int[] levels) {
            m_NamedTypes = namedTypes;
            m_Levels = levels;
        }
    }
}
