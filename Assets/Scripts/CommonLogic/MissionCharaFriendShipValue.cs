﻿namespace CommonLogic {
    public class MissionCharaFriendShipValue : MissionValue {
        public int m_SubCode;

        private MissionCharaFriendShipValue() { }

        public MissionCharaFriendShipValue(int rate, int maxRate, int subCode) {
            m_Rate = rate;
            m_MaxRate = maxRate;
            m_SubCode = subCode;
        }
    }
}
