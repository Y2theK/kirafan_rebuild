﻿namespace CommonLogic {
    public class MissionEditEvolutionCondition : MissionConditionBase {
        public int m_EvolutionNum;

        private MissionEditEvolutionCondition() { }

        public MissionEditEvolutionCondition(int evolutionNum) {
            m_EvolutionNum = evolutionNum;
        }
    }
}
