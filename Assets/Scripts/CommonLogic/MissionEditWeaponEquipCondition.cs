﻿namespace CommonLogic {
    public class MissionEditWeaponEquipCondition : MissionConditionBase {
        public WeaponEquipChange[] m_WeaponEquipChanges;

        private MissionEditWeaponEquipCondition() { }

        public MissionEditWeaponEquipCondition(WeaponEquipChange[] weaponEquipChages) {
            m_WeaponEquipChanges = weaponEquipChages;
        }
    }
}
