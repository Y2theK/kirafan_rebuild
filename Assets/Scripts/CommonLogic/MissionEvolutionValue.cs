﻿namespace CommonLogic {
    public class MissionEvolutionValue : MissionValue {
        public int m_CheckNum;

        private MissionEvolutionValue() { }

        public MissionEvolutionValue(int rate, int maxRate, int checkNum) {
            m_Rate = rate;
            m_MaxRate = maxRate;
            m_CheckNum = checkNum;
        }
    }
}
