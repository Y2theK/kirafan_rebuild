﻿namespace CommonLogic {
    public class MissionSystemCondition : MissionConditionBase {
        public int m_LoginDays;
        public int m_ContinutyLoginDays;
        public int m_UserLevel;

        private MissionSystemCondition() { }

        public MissionSystemCondition(int loginDays, int continutiyLoginDays, int userLevel) {
            m_LoginDays = loginDays;
            m_ContinutyLoginDays = continutiyLoginDays;
            m_UserLevel = userLevel;
        }
    }
}
