﻿namespace CommonLogic {
    public class MissionTownBuildCondition : MissionConditionBase {
        public int m_NextLevel;
        public int m_OpenState;

        private MissionTownBuildCondition() { }

        public MissionTownBuildCondition(int nextlevel, int openstate) {
            m_NextLevel = nextlevel;
            m_OpenState = openstate;
        }
    }
}
