﻿namespace CommonLogic {
    public class MissionValue {
        public int m_Rate;
        public int m_MaxRate;

        protected MissionValue() { }

        public MissionValue(int rate, int maxRate) {
            m_Rate = rate;
            m_MaxRate = maxRate;
        }
    }
}
