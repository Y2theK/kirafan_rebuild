﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FE RID: 766
	[Token(Token = "0x20002A2")]
	[StructLayout(0, Size = 12)]
	public struct NamedListDB_Param
	{
		// Token: 0x04000AFA RID: 2810
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000922")]
		public int m_NamedType;

		// Token: 0x04000AFB RID: 2811
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000923")]
		public int m_TitleType;

		// Token: 0x04000AFC RID: 2812
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000924")]
		public int m_DropItemKey;
	}
}
