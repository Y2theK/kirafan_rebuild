﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C7 RID: 711
	[Token(Token = "0x2000275")]
	[StructLayout(3)]
	public class QuestWaveDropItemData
	{
		// Token: 0x0600088E RID: 2190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007FC")]
		[Address(RVA = "0x1010E2EE4", Offset = "0x10E2EE4", VA = "0x1010E2EE4")]
		public QuestWaveDropItemData(int id, int dropItemID, int dropItemNum, float dropProbability)
		{
		}

		// Token: 0x04000A35 RID: 2613
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000889")]
		public int m_DropID;

		// Token: 0x04000A36 RID: 2614
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400088A")]
		public int m_DropItemID;

		// Token: 0x04000A37 RID: 2615
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400088B")]
		public int m_DropItemNum;

		// Token: 0x04000A38 RID: 2616
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400088C")]
		public float m_DropProbability;
	}
}
