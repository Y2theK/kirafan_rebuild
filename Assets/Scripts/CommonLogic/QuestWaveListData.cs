﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C5 RID: 709
	[Token(Token = "0x2000273")]
	[StructLayout(3)]
	public class QuestWaveListData
	{
		// Token: 0x0600088C RID: 2188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007FA")]
		[Address(RVA = "0x1010E2F34", Offset = "0x10E2F34", VA = "0x1010E2F34")]
		public QuestWaveListData(int id, int[] questEnemyIDs, int[] questRandomIDs, int[] questEnemyLvs, int[] dropIDs, float[] dispScales)
		{
		}

		// Token: 0x04000A27 RID: 2599
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400087B")]
		public int m_ID;

		// Token: 0x04000A28 RID: 2600
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400087C")]
		public int[] m_QuestEnemyIDs;

		// Token: 0x04000A29 RID: 2601
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400087D")]
		public int[] m_QuestRandomIDs;

		// Token: 0x04000A2A RID: 2602
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400087E")]
		public int[] m_QuestEnemyLvs;

		// Token: 0x04000A2B RID: 2603
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400087F")]
		public int[] m_DropIDs;

		// Token: 0x04000A2C RID: 2604
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000880")]
		public float[] m_DispScales;
	}
}
