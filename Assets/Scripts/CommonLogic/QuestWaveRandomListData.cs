﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C6 RID: 710
	[Token(Token = "0x2000274")]
	[StructLayout(3)]
	public class QuestWaveRandomListData
	{
		// Token: 0x0600088D RID: 2189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60007FB")]
		[Address(RVA = "0x1010E2F98", Offset = "0x10E2F98", VA = "0x1010E2F98")]
		public QuestWaveRandomListData(int id, int num, float[] probs, int[] questEnemyIDs, int[] questRandomIDs, int[] questEnemyLvs, int[] dropIDs, float[] dispScales)
		{
		}

		// Token: 0x04000A2D RID: 2605
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000881")]
		public int m_ID;

		// Token: 0x04000A2E RID: 2606
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000882")]
		public int m_Num;

		// Token: 0x04000A2F RID: 2607
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000883")]
		public float[] m_Prob;

		// Token: 0x04000A30 RID: 2608
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000884")]
		public int[] m_QuestEnemyIDs;

		// Token: 0x04000A31 RID: 2609
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000885")]
		public int[] m_QuestRandomIDs;

		// Token: 0x04000A32 RID: 2610
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000886")]
		public int[] m_QuestEnemyLvs;

		// Token: 0x04000A33 RID: 2611
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000887")]
		public int[] m_DropIDs;

		// Token: 0x04000A34 RID: 2612
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000888")]
		public float[] m_DispScales;
	}
}
