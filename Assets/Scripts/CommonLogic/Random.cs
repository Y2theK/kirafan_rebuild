﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F4 RID: 756
	[Token(Token = "0x2000298")]
	[StructLayout(3)]
	public static class Random
	{
		// Token: 0x0600090B RID: 2315 RVA: 0x00003E88 File Offset: 0x00002088
		[Token(Token = "0x600086E")]
		[Address(RVA = "0x1010DC068", Offset = "0x10DC068", VA = "0x1010DC068")]
		public static float Range(float min, float max)
		{
			return 0f;
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x00003EA0 File Offset: 0x000020A0
		[Token(Token = "0x600086F")]
		[Address(RVA = "0x1010DB47C", Offset = "0x10DB47C", VA = "0x1010DB47C")]
		public static int Range(int min, int max)
		{
			return 0;
		}
	}
}
