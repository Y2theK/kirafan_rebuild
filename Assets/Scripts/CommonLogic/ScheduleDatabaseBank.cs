﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star;

namespace CommonLogic
{
	// Token: 0x02000302 RID: 770
	[Token(Token = "0x20002A6")]
	[StructLayout(3)]
	internal static class ScheduleDatabaseBank
	{
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600090E RID: 2318 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000058")]
		public static ScheduleHolydayDB_Param[] scheduleHolydayDBs
		{
			[Token(Token = "0x6000871")]
			[Address(RVA = "0x1010E300C", Offset = "0x10E300C", VA = "0x1010E300C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600090F RID: 2319 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000059")]
		public static ScheduleNameDB_Param[] scheduleNameDBs
		{
			[Token(Token = "0x6000872")]
			[Address(RVA = "0x1010DFA00", Offset = "0x10DFA00", VA = "0x1010DFA00")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000910 RID: 2320 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005A")]
		public static ScheduleSetupDB_Param[] scheduleSetupDBs
		{
			[Token(Token = "0x6000873")]
			[Address(RVA = "0x1010E3074", Offset = "0x10E3074", VA = "0x1010E3074")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000911 RID: 2321 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005B")]
		public static NamedListDB_Param[] namedListDBs
		{
			[Token(Token = "0x6000874")]
			[Address(RVA = "0x1010E30DC", Offset = "0x10E30DC", VA = "0x1010E30DC")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000912 RID: 2322 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005C")]
		public static CharacterListDB_Param[] characterListDBs
		{
			[Token(Token = "0x6000875")]
			[Address(RVA = "0x1010E3144", Offset = "0x10E3144", VA = "0x1010E3144")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000913 RID: 2323 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005D")]
		public static FieldItemDropListDB_Param[] fieldItemDropListDBs
		{
			[Token(Token = "0x6000876")]
			[Address(RVA = "0x1010E31AC", Offset = "0x10E31AC", VA = "0x1010E31AC")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000914 RID: 2324 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005E")]
		public static TownObjectListDB_Param[] townObjectListDBs
		{
			[Token(Token = "0x6000877")]
			[Address(RVA = "0x1010E3214", Offset = "0x10E3214", VA = "0x1010E3214")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000915 RID: 2325 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700005F")]
		public static TownObjectLevelUpDB_Param[] townObjectLevelUpDBs
		{
			[Token(Token = "0x6000878")]
			[Address(RVA = "0x1010E327C", Offset = "0x10E327C", VA = "0x1010E327C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000916 RID: 2326 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000060")]
		public static ScheduleSetUpDataBase scheduleSetupDatabase
		{
			[Token(Token = "0x6000879")]
			[Address(RVA = "0x1010E293C", Offset = "0x10E293C", VA = "0x1010E293C")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x00003EB8 File Offset: 0x000020B8
		[Token(Token = "0x600087A")]
		[Address(RVA = "0x1010E32E4", Offset = "0x10E32E4", VA = "0x1010E32E4")]
		public static bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x00003ED0 File Offset: 0x000020D0
		[Token(Token = "0x600087B")]
		[Address(RVA = "0x1010E334C", Offset = "0x10E334C", VA = "0x1010E334C")]
		public static bool Setup(ScheduleHolydayDB_Param[] lholydayDBs, ScheduleNameDB_Param[] lnameDBs, ScheduleSetupDB_Param[] lsetupDBs, NamedListDB_Param[] lnamedListDBs, CharacterListDB_Param[] lcharacterListDBs, FieldItemDropListDB_Param[] lfieldItemDropListDBs, TownObjectListDB_Param[] ltownObjectListDBs, TownObjectLevelUpDB_Param[] ltownObjectLevelUpDBs)
		{
			return default(bool);
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600087C")]
		[Address(RVA = "0x102438A78", Offset = "0x2438A78", VA = "0x102438A78")]
		private static void Copy<TSrc, TDst>(TSrc[] src, ref TDst[] dst)
		{
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600087D")]
		[Address(RVA = "0x1010E4424", Offset = "0x10E4424", VA = "0x1010E4424")]
		private static void Copy(ScheduleHolydayDB_Param[] src, ref ScheduleHolydayDB_Param[] dst)
		{
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600087E")]
		[Address(RVA = "0x1010E4684", Offset = "0x10E4684", VA = "0x1010E4684")]
		private static void Copy(ScheduleNameDB_Param[] src, ref ScheduleNameDB_Param[] dst)
		{
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600087F")]
		[Address(RVA = "0x1010E4D2C", Offset = "0x10E4D2C", VA = "0x1010E4D2C")]
		private static void Copy(ScheduleSetupDB_Param[] src, ref ScheduleSetupDB_Param[] dst)
		{
		}

		// Token: 0x0600091D RID: 2333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000880")]
		[Address(RVA = "0x1010E52A4", Offset = "0x10E52A4", VA = "0x1010E52A4")]
		private static void Copy(NamedListDB_Param[] src, ref NamedListDB_Param[] dst)
		{
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000881")]
		[Address(RVA = "0x1010E5460", Offset = "0x10E5460", VA = "0x1010E5460")]
		private static void Copy(CharacterListDB_Param[] src, ref CharacterListDB_Param[] dst)
		{
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000882")]
		[Address(RVA = "0x1010E5714", Offset = "0x10E5714", VA = "0x1010E5714")]
		private static void Copy(FieldItemDropListDB_Param[] src, ref FieldItemDropListDB_Param[] dst)
		{
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000883")]
		[Address(RVA = "0x1010E591C", Offset = "0x10E591C", VA = "0x1010E591C")]
		private static void Copy(TownObjectListDB_Param[] src, ref TownObjectListDB_Param[] dst)
		{
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000884")]
		[Address(RVA = "0x1010E5ECC", Offset = "0x10E5ECC", VA = "0x1010E5ECC")]
		private static void Copy(TownObjectLevelUpDB_Param[] src, ref TownObjectLevelUpDB_Param[] dst)
		{
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x00003EE8 File Offset: 0x000020E8
		[Token(Token = "0x6000885")]
		[Address(RVA = "0x1010E6180", Offset = "0x10E6180", VA = "0x1010E6180")]
		public static bool Setup(ScheduleHolydayDB_Param[] lholydayDBs, ScheduleNameDB_Param[] lnameDBs, ScheduleSetupDB_Param[] lsetupDBs, NamedListDB_Param[] lnamedListDBs, CharacterListDB_Param[] lcharacterListDBs, FieldItemDropListDB_Param[] lfieldItemDropListDBs, TownObjectListDB_Param[] ltownObjectListDBs, TownObjectLevelUpDB_Param[] ltownObjectLevelUpDBs)
		{
			return default(bool);
		}

		// Token: 0x06000923 RID: 2339 RVA: 0x00003F00 File Offset: 0x00002100
		[Token(Token = "0x6000886")]
		[Address(RVA = "0x1010E6334", Offset = "0x10E6334", VA = "0x1010E6334")]
		public static bool Release()
		{
			return default(bool);
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x00003F18 File Offset: 0x00002118
		[Token(Token = "0x6000887")]
		[Address(RVA = "0x1010E0E28", Offset = "0x10E0E28", VA = "0x1010E0E28")]
		public static ScheduleNameDB_Param GetScheduleName(int id)
		{
			return default(ScheduleNameDB_Param);
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x00003F30 File Offset: 0x00002130
		[Token(Token = "0x6000888")]
		[Address(RVA = "0x1010E63E8", Offset = "0x10E63E8", VA = "0x1010E63E8")]
		public static CharacterListDB_Param GetChara(int charaID)
		{
			return default(CharacterListDB_Param);
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x00003F48 File Offset: 0x00002148
		[Token(Token = "0x6000889")]
		[Address(RVA = "0x1010E653C", Offset = "0x10E653C", VA = "0x1010E653C")]
		public static NamedListDB_Param GetNamed(int namedType)
		{
			return default(NamedListDB_Param);
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x00003F60 File Offset: 0x00002160
		[Token(Token = "0x600088A")]
		[Address(RVA = "0x1010E6698", Offset = "0x10E6698", VA = "0x1010E6698")]
		public static TownObjectListDB_Param GetTownObject(int townObjectID)
		{
			return default(TownObjectListDB_Param);
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x00003F78 File Offset: 0x00002178
		[Token(Token = "0x600088B")]
		[Address(RVA = "0x1010E6824", Offset = "0x10E6824", VA = "0x1010E6824")]
		public static TownObjectLevelUpDB_Param GetTownObjectLevelUp(int levelUpListID, int level)
		{
			return default(TownObjectLevelUpDB_Param);
		}

		// Token: 0x04000B17 RID: 2839
		[Token(Token = "0x400093F")]
		private static ScheduleHolydayDB_Param[] m_ScheduleHolydayDBs;

		// Token: 0x04000B18 RID: 2840
		[Token(Token = "0x4000940")]
		private static ScheduleNameDB_Param[] m_ScheduleNameDBs;

		// Token: 0x04000B19 RID: 2841
		[Token(Token = "0x4000941")]
		private static ScheduleSetupDB_Param[] m_ScheduleSetupDBs;

		// Token: 0x04000B1A RID: 2842
		[Token(Token = "0x4000942")]
		private static NamedListDB_Param[] m_NamedListDBs;

		// Token: 0x04000B1B RID: 2843
		[Token(Token = "0x4000943")]
		private static CharacterListDB_Param[] m_CharacterListDBs;

		// Token: 0x04000B1C RID: 2844
		[Token(Token = "0x4000944")]
		private static FieldItemDropListDB_Param[] m_FieldItemDropListDBs;

		// Token: 0x04000B1D RID: 2845
		[Token(Token = "0x4000945")]
		private static TownObjectListDB_Param[] m_TownObjectListDBs;

		// Token: 0x04000B1E RID: 2846
		[Token(Token = "0x4000946")]
		private static TownObjectLevelUpDB_Param[] m_TownObjectLevelUpDBs;

		// Token: 0x04000B1F RID: 2847
		[Token(Token = "0x4000947")]
		private static ScheduleSetUpDataBase m_ScheduleSetupDatabase;

		// Token: 0x04000B20 RID: 2848
		[Token(Token = "0x4000948")]
		private static bool m_isInitialized;
	}
}
