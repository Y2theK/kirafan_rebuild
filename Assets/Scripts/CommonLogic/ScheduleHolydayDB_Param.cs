﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FB RID: 763
	[Token(Token = "0x200029F")]
	[StructLayout(0, Size = 12)]
	public struct ScheduleHolydayDB_Param
	{
		// Token: 0x04000AD5 RID: 2773
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40008FD")]
		public int m_ID;

		// Token: 0x04000AD6 RID: 2774
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40008FE")]
		public short m_OptionGroup;

		// Token: 0x04000AD7 RID: 2775
		[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
		[Token(Token = "0x40008FF")]
		public short m_Year;

		// Token: 0x04000AD8 RID: 2776
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000900")]
		public byte m_Month;

		// Token: 0x04000AD9 RID: 2777
		[Cpp2IlInjected.FieldOffset(Offset = "0x9")]
		[Token(Token = "0x4000901")]
		public byte m_Day;
	}
}
