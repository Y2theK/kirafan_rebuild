﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FC RID: 764
	[Token(Token = "0x20002A0")]
	[StructLayout(0, Size = 120)]
	public struct ScheduleNameDB_Param
	{
		// Token: 0x04000ADA RID: 2778
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000902")]
		public int m_ID;

		// Token: 0x04000ADB RID: 2779
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000903")]
		public string m_TagName;

		// Token: 0x04000ADC RID: 2780
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000904")]
		public ushort[] m_GroupWakeUp;

		// Token: 0x04000ADD RID: 2781
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000905")]
		public uint[] m_ResultBaseNo;

		// Token: 0x04000ADE RID: 2782
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000906")]
		public short[] m_WakeBaseUp;

		// Token: 0x04000ADF RID: 2783
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000907")]
		public uint[] m_ResultSpNo;

		// Token: 0x04000AE0 RID: 2784
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000908")]
		public short[] m_WakeSpUp;

		// Token: 0x04000AE1 RID: 2785
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000909")]
		public int[] m_SpMessage;

		// Token: 0x04000AE2 RID: 2786
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400090A")]
		public int[] m_BaseMessage;

		// Token: 0x04000AE3 RID: 2787
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400090B")]
		public int m_MoveScriptID;

		// Token: 0x04000AE4 RID: 2788
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400090C")]
		public int m_DropMakeWakeUp;

		// Token: 0x04000AE5 RID: 2789
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400090D")]
		public uint[] m_DropResultNo;

		// Token: 0x04000AE6 RID: 2790
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400090E")]
		public short[] m_DropWakeUp;

		// Token: 0x04000AE7 RID: 2791
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400090F")]
		public int m_LiinkNo;

		// Token: 0x04000AE8 RID: 2792
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4000910")]
		public uint m_Flags;

		// Token: 0x04000AE9 RID: 2793
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000911")]
		public int m_BuildMoveType;

		// Token: 0x04000AEA RID: 2794
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000912")]
		public int m_BuildMoveCode;

		// Token: 0x04000AEB RID: 2795
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000913")]
		public int m_LinkTitle;
	}
}
