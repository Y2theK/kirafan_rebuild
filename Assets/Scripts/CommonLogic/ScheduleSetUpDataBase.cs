﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000303 RID: 771
	[Token(Token = "0x20002A7")]
	[StructLayout(3)]
	public class ScheduleSetUpDataBase
	{
		// Token: 0x0600092A RID: 2346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600088D")]
		[Address(RVA = "0x1010E3534", Offset = "0x10E3534", VA = "0x1010E3534")]
		public void SetUpResource()
		{
		}

		// Token: 0x0600092B RID: 2347 RVA: 0x00003F90 File Offset: 0x00002190
		[Token(Token = "0x600088E")]
		[Address(RVA = "0x1010E6ACC", Offset = "0x10E6ACC", VA = "0x1010E6ACC")]
		private static UserScheduleData.eType ChangeXlsKeyToType(int fmakewark)
		{
			return UserScheduleData.eType.Non;
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x00003FA8 File Offset: 0x000021A8
		[Token(Token = "0x600088F")]
		[Address(RVA = "0x1010E6AE4", Offset = "0x10E6AE4", VA = "0x1010E6AE4")]
		private bool CheckTownBuildToPoint(int fmakewark, ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid, UserTownData ptown)
		{
			return default(bool);
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x00003FC0 File Offset: 0x000021C0
		[Token(Token = "0x6000890")]
		[Address(RVA = "0x1010E6BF4", Offset = "0x10E6BF4", VA = "0x1010E6BF4")]
		private static int GetSecToHolydayType(long timeSec)
		{
			return 0;
		}

		// Token: 0x0600092E RID: 2350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000891")]
		[Address(RVA = "0x1010E29A4", Offset = "0x10E29A4", VA = "0x1010E29A4")]
		public void CreateList(UserCharacterData pcharadata, long fsettime, ref UserScheduleData.ListPack pbase, bool fixroom, UserTownData userTownData, bool isNewCreate)
		{
		}

		// Token: 0x0600092F RID: 2351 RVA: 0x00003FD8 File Offset: 0x000021D8
		[Token(Token = "0x6000892")]
		[Address(RVA = "0x1010E7AE0", Offset = "0x10E7AE0", VA = "0x1010E7AE0")]
		public int ChangeFailTag(int fscheduletag, int ftitle)
		{
			return 0;
		}

		// Token: 0x06000930 RID: 2352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000893")]
		[Address(RVA = "0x1010E7DFC", Offset = "0x10E7DFC", VA = "0x1010E7DFC")]
		public void ChangeSegState(UserScheduleData.Seg pseg, int fchg)
		{
		}

		// Token: 0x06000931 RID: 2353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000894")]
		[Address(RVA = "0x1010E352C", Offset = "0x10E352C", VA = "0x1010E352C")]
		public ScheduleSetUpDataBase()
		{
		}

		// Token: 0x04000B21 RID: 2849
		[Token(Token = "0x4000949")]
		private const int TIME_HOUR_TO_SEC = 3600;

		// Token: 0x04000B22 RID: 2850
		[Token(Token = "0x400094A")]
		public const int SLEEP_TAG = 0;

		// Token: 0x04000B23 RID: 2851
		[Token(Token = "0x400094B")]
		public const int ROOM_TAG = 1;

		// Token: 0x04000B24 RID: 2852
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400094C")]
		private ScheduleSetUpDataBase.ScheduleCategory[] m_CategoryDB;

		// Token: 0x02000304 RID: 772
		[Token(Token = "0x2000D33")]
		public class ScheudlePackBase
		{
			// Token: 0x06000932 RID: 2354 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D63")]
			[Address(RVA = "0x1010E6A10", Offset = "0x10E6A10", VA = "0x1010E6A10")]
			public static ScheduleSetUpDataBase.ScheudlePackBase Create(int fkey)
			{
				return null;
			}

			// Token: 0x06000933 RID: 2355 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D64")]
			[Address(RVA = "0x1010E8E20", Offset = "0x10E8E20", VA = "0x1010E8E20", Slot = "4")]
			public virtual void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup, UserTownData ptown)
			{
			}

			// Token: 0x06000934 RID: 2356 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D65")]
			[Address(RVA = "0x1010E8E24", Offset = "0x10E8E24", VA = "0x1010E8E24")]
			public ScheudlePackBase()
			{
			}

			// Token: 0x04000B25 RID: 2853
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005562")]
			public ScheduleSetupDB_Param[] m_List;
		}

		// Token: 0x02000305 RID: 773
		[Token(Token = "0x2000D34")]
		public class ScheudlePackNormal : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06000935 RID: 2357 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D66")]
			[Address(RVA = "0x1010E990C", Offset = "0x10E990C", VA = "0x1010E990C", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup, UserTownData ptown)
			{
			}

			// Token: 0x06000936 RID: 2358 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D67")]
			[Address(RVA = "0x1010E8E18", Offset = "0x10E8E18", VA = "0x1010E8E18")]
			public ScheudlePackNormal()
			{
			}
		}

		// Token: 0x02000306 RID: 774
		[Token(Token = "0x2000D35")]
		public class ScheudlePackContent : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06000937 RID: 2359 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D68")]
			[Address(RVA = "0x1010E922C", Offset = "0x10E922C", VA = "0x1010E922C")]
			private void CheckTownBuildToContent(ref TownAccessCheck.ChrCheckDataBase pchrparam, List<ScheduleSetUpDataBase.ScheudlePackContent.Up> flist, UserTownData ptown)
			{
			}

			// Token: 0x06000938 RID: 2360 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D69")]
			[Address(RVA = "0x1010E9414", Offset = "0x10E9414", VA = "0x1010E9414", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup, UserTownData ptown)
			{
			}

			// Token: 0x06000939 RID: 2361 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D6A")]
			[Address(RVA = "0x1010E8E08", Offset = "0x10E8E08", VA = "0x1010E8E08")]
			public ScheudlePackContent()
			{
			}

			// Token: 0x02000307 RID: 775
			[Token(Token = "0x200133F")]
			public class Up
			{
				// Token: 0x0600093A RID: 2362 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006508")]
				[Address(RVA = "0x1010E93CC", Offset = "0x10E93CC", VA = "0x1010E93CC")]
				public Up(int fpoint, float fper, int ftitle)
				{
				}

				// Token: 0x04000B26 RID: 2854
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x400757A")]
				public float m_Per;

				// Token: 0x04000B27 RID: 2855
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x400757B")]
				public int m_Index;

				// Token: 0x04000B28 RID: 2856
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x400757C")]
				public int m_Title;
			}
		}

		// Token: 0x02000308 RID: 776
		[Token(Token = "0x2000D36")]
		public class ScheudlePackBuff : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x0600093B RID: 2363 RVA: 0x00003FF0 File Offset: 0x000021F0
			[Token(Token = "0x6005D6B")]
			[Address(RVA = "0x1010E8E2C", Offset = "0x10E8E2C", VA = "0x1010E8E2C")]
			private bool CheckTownBuffToPoint(ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid, UserTownData ptown)
			{
				return default(bool);
			}

			// Token: 0x0600093C RID: 2364 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D6C")]
			[Address(RVA = "0x1010E8F80", Offset = "0x10E8F80", VA = "0x1010E8F80", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup, UserTownData ptown)
			{
			}

			// Token: 0x0600093D RID: 2365 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D6D")]
			[Address(RVA = "0x1010E8E10", Offset = "0x10E8E10", VA = "0x1010E8E10")]
			public ScheudlePackBuff()
			{
			}
		}

		// Token: 0x02000309 RID: 777
		[Token(Token = "0x2000D37")]
		public class ScheduleCategory
		{
			// Token: 0x0600093E RID: 2366 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D6E")]
			[Address(RVA = "0x1010E6A08", Offset = "0x10E6A08", VA = "0x1010E6A08")]
			public ScheduleCategory()
			{
			}

			// Token: 0x04000B29 RID: 2857
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005563")]
			public int m_ListUpNum;

			// Token: 0x04000B2A RID: 2858
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005564")]
			public ScheduleSetUpDataBase.ScheudlePackBase[] m_Pack;
		}

		// Token: 0x0200030A RID: 778
		[Token(Token = "0x2000D38")]
		public class Seg
		{
			// Token: 0x0600093F RID: 2367 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D6F")]
			[Address(RVA = "0x1010E8954", Offset = "0x10E8954", VA = "0x1010E8954")]
			public Seg()
			{
			}

			// Token: 0x04000B2B RID: 2859
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005565")]
			public int m_Type;

			// Token: 0x04000B2C RID: 2860
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005566")]
			public int m_WakeTimeIdx;

			// Token: 0x04000B2D RID: 2861
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005567")]
			public int m_UseTimeNum;

			// Token: 0x04000B2E RID: 2862
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005568")]
			public int m_TagNameID;

			// Token: 0x04000B2F RID: 2863
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005569")]
			public int m_ID;

			// Token: 0x04000B30 RID: 2864
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400556A")]
			public int m_Priority;
		}

		// Token: 0x0200030B RID: 779
		[Token(Token = "0x2000D39")]
		public class ScheduleUpParam
		{
			// Token: 0x06000940 RID: 2368 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D70")]
			[Address(RVA = "0x1010E6EA4", Offset = "0x10E6EA4", VA = "0x1010E6EA4")]
			public ScheduleUpParam(long fmanageid)
			{
			}

			// Token: 0x06000941 RID: 2369 RVA: 0x00004008 File Offset: 0x00002208
			[Token(Token = "0x6005D71")]
			[Address(RVA = "0x1010E7F7C", Offset = "0x10E7F7C", VA = "0x1010E7F7C")]
			public int GetFreeHour()
			{
				return 0;
			}

			// Token: 0x06000942 RID: 2370 RVA: 0x00004020 File Offset: 0x00002220
			[Token(Token = "0x6005D72")]
			[Address(RVA = "0x1010E7FF0", Offset = "0x10E7FF0", VA = "0x1010E7FF0")]
			public int GetFreeHour(int fstart, int fend)
			{
				return 0;
			}

			// Token: 0x06000943 RID: 2371 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D73")]
			[Address(RVA = "0x1010E8074", Offset = "0x10E8074", VA = "0x1010E8074")]
			public void AddSeg(ScheduleSetUpDataBase.Seg pseg)
			{
			}

			// Token: 0x06000944 RID: 2372 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D74")]
			[Address(RVA = "0x1010E8220", Offset = "0x10E8220", VA = "0x1010E8220")]
			public ScheduleSetUpDataBase.Seg GetLastSeg()
			{
				return null;
			}

			// Token: 0x06000945 RID: 2373 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D75")]
			[Address(RVA = "0x1010E82F4", Offset = "0x10E82F4", VA = "0x1010E82F4")]
			private void SortTable()
			{
			}

			// Token: 0x06000946 RID: 2374 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D76")]
			[Address(RVA = "0x1010E84CC", Offset = "0x10E84CC", VA = "0x1010E84CC")]
			private void ReactRoomTag()
			{
			}

			// Token: 0x06000947 RID: 2375 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D77")]
			[Address(RVA = "0x1010E85D0", Offset = "0x10E85D0", VA = "0x1010E85D0")]
			private void MargeSeg()
			{
			}

			// Token: 0x06000948 RID: 2376 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D78")]
			[Address(RVA = "0x1010E878C", Offset = "0x10E878C", VA = "0x1010E878C")]
			private void SetBlankSeg()
			{
			}

			// Token: 0x06000949 RID: 2377 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D79")]
			[Address(RVA = "0x1010E6F30", Offset = "0x10E6F30", VA = "0x1010E6F30")]
			public UserScheduleData.Seg[] CreateSheduleTable(bool fixroom)
			{
				return null;
			}

			// Token: 0x0600094A RID: 2378 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D7A")]
			[Address(RVA = "0x1010E895C", Offset = "0x10E895C", VA = "0x1010E895C")]
			public void CreateSceduleFixSeg(ref ScheduleSetupDB_Param pseg, bool fusetag = true)
			{
			}

			// Token: 0x0600094B RID: 2379 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D7B")]
			[Address(RVA = "0x1010E8B50", Offset = "0x10E8B50", VA = "0x1010E8B50")]
			public void CreateSceduleDeriveSeg(ref ScheduleSetupDB_Param pseg, bool fusetag = true)
			{
			}

			// Token: 0x0600094C RID: 2380 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D7C")]
			[Address(RVA = "0x1010E8CA8", Offset = "0x10E8CA8", VA = "0x1010E8CA8")]
			public void CreateSceduleFreeSeg(ref ScheduleSetupDB_Param pseg, bool fusetag = true)
			{
			}

			// Token: 0x04000B31 RID: 2865
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400556B")]
			public List<ScheduleSetUpDataBase.Seg> m_List;

			// Token: 0x04000B32 RID: 2866
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400556C")]
			public byte[] m_Mask;
		}
	}
}
