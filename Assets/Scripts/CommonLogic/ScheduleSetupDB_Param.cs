﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002FD RID: 765
	[Token(Token = "0x20002A1")]
	[StructLayout(0, Size = 48)]
	public struct ScheduleSetupDB_Param
	{
		// Token: 0x0600090D RID: 2317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000870")]
		[Address(RVA = "0x10002ECE4", Offset = "0x2ECE4", VA = "0x10002ECE4")]
		public void SetUp(ref ScheduleSetupDB_Param pbase)
		{
		}

		// Token: 0x04000AEC RID: 2796
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000914")]
		public int m_ID;

		// Token: 0x04000AED RID: 2797
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000915")]
		public ushort m_BuildPriority;

		// Token: 0x04000AEE RID: 2798
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000916")]
		public int m_ScheduleTagSelf;

		// Token: 0x04000AEF RID: 2799
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4000917")]
		public int m_ScheduleTagOther;

		// Token: 0x04000AF0 RID: 2800
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000918")]
		public int m_Content;

		// Token: 0x04000AF1 RID: 2801
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000919")]
		public ushort m_MakePoint;

		// Token: 0x04000AF2 RID: 2802
		[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
		[Token(Token = "0x400091A")]
		public ushort m_Week;

		// Token: 0x04000AF3 RID: 2803
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400091B")]
		public ushort m_Holyday;

		// Token: 0x04000AF4 RID: 2804
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
		[Token(Token = "0x400091C")]
		public ushort m_BuildType;

		// Token: 0x04000AF5 RID: 2805
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400091D")]
		public int m_StartTime;

		// Token: 0x04000AF6 RID: 2806
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400091E")]
		public int m_EndTime;

		// Token: 0x04000AF7 RID: 2807
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400091F")]
		public int m_StartLife;

		// Token: 0x04000AF8 RID: 2808
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000920")]
		public int m_EndLife;

		// Token: 0x04000AF9 RID: 2809
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000921")]
		public short m_BuildUpPer;
	}
}
