﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200030C RID: 780
	[Token(Token = "0x20002A8")]
	[StructLayout(3)]
	public static class ScheduleTimeUtil
	{
		// Token: 0x0600094D RID: 2381 RVA: 0x00004038 File Offset: 0x00002238
		[Token(Token = "0x6000895")]
		[Address(RVA = "0x1010E9B5C", Offset = "0x10E9B5C", VA = "0x1010E9B5C")]
		public static long GetSystemTimeSec()
		{
			return 0L;
		}

		// Token: 0x0600094E RID: 2382 RVA: 0x00004050 File Offset: 0x00002250
		[Token(Token = "0x6000896")]
		[Address(RVA = "0x1010E9BE4", Offset = "0x10E9BE4", VA = "0x1010E9BE4")]
		public static long GetSystemTimeMs()
		{
			return 0L;
		}

		// Token: 0x0600094F RID: 2383 RVA: 0x00004068 File Offset: 0x00002268
		[Token(Token = "0x6000897")]
		[Address(RVA = "0x1010E9C68", Offset = "0x10E9C68", VA = "0x1010E9C68")]
		public static int GetTimeCalcMode()
		{
			return 0;
		}

		// Token: 0x06000950 RID: 2384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000898")]
		[Address(RVA = "0x1010E9CD0", Offset = "0x10E9CD0", VA = "0x1010E9CD0")]
		public static void SetDebugSpeed(float fspeed)
		{
		}

		// Token: 0x06000951 RID: 2385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000899")]
		[Address(RVA = "0x1010E9D44", Offset = "0x10E9D44", VA = "0x1010E9D44")]
		public static void SetDebugAddTime(int fday, int fhour, int fminute)
		{
		}

		// Token: 0x06000952 RID: 2386 RVA: 0x00004080 File Offset: 0x00002280
		[Token(Token = "0x600089A")]
		[Address(RVA = "0x1010E9DF8", Offset = "0x10E9DF8", VA = "0x1010E9DF8")]
		public static int GetDayTimeMinute(long ftime)
		{
			return 0;
		}

		// Token: 0x06000953 RID: 2387 RVA: 0x00004098 File Offset: 0x00002298
		[Token(Token = "0x600089B")]
		[Address(RVA = "0x1010E27AC", Offset = "0x10E27AC", VA = "0x1010E27AC")]
		public static int GetDayTimeSec(long ftime)
		{
			return 0;
		}

		// Token: 0x06000954 RID: 2388 RVA: 0x000040B0 File Offset: 0x000022B0
		[Token(Token = "0x600089C")]
		[Address(RVA = "0x1010E291C", Offset = "0x10E291C", VA = "0x1010E291C")]
		public static long GetDayKey(long ftime)
		{
			return 0L;
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x000040C8 File Offset: 0x000022C8
		[Token(Token = "0x600089D")]
		[Address(RVA = "0x1010E9E40", Offset = "0x10E9E40", VA = "0x1010E9E40")]
		public static int ChangeUnitTimeMinute(int fday, int fhour, int fminute)
		{
			return 0;
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x000040E0 File Offset: 0x000022E0
		[Token(Token = "0x600089E")]
		[Address(RVA = "0x1010E0AF0", Offset = "0x10E0AF0", VA = "0x1010E0AF0")]
		public static int ChangeUnitTimeSec(int fday, int fhour, int fminute)
		{
			return 0;
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x000040F8 File Offset: 0x000022F8
		[Token(Token = "0x600089F")]
		[Address(RVA = "0x1010E9E58", Offset = "0x10E9E58", VA = "0x1010E9E58")]
		public static long ChangeUnitTimeSecToBase(long fsec)
		{
			return 0L;
		}

		// Token: 0x06000958 RID: 2392 RVA: 0x00004110 File Offset: 0x00002310
		[Token(Token = "0x60008A0")]
		[Address(RVA = "0x1010E9E68", Offset = "0x10E9E68", VA = "0x1010E9E68")]
		public static DateTime ChangeBaseTimeToDateTime(long fbasetime)
		{
			return default(DateTime);
		}

		// Token: 0x06000959 RID: 2393 RVA: 0x00004128 File Offset: 0x00002328
		[Token(Token = "0x60008A1")]
		[Address(RVA = "0x1010E9F20", Offset = "0x10E9F20", VA = "0x1010E9F20")]
		public static long DebugUnitTimeSec(int fmonth, int fday, int fhour, int fminute, int fsec)
		{
			return 0L;
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x00004140 File Offset: 0x00002340
		[Token(Token = "0x60008A2")]
		[Address(RVA = "0x1010EA0A8", Offset = "0x10EA0A8", VA = "0x1010EA0A8")]
		public static DateTime GetManageUnixTime()
		{
			return default(DateTime);
		}

		// Token: 0x0600095B RID: 2395 RVA: 0x00004158 File Offset: 0x00002358
		[Token(Token = "0x60008A3")]
		[Address(RVA = "0x1010EA160", Offset = "0x10EA160", VA = "0x1010EA160")]
		public static DateTime GetManageUniversalTime()
		{
			return default(DateTime);
		}

		// Token: 0x0600095C RID: 2396 RVA: 0x00004170 File Offset: 0x00002370
		[Token(Token = "0x60008A4")]
		[Address(RVA = "0x1010EA218", Offset = "0x10EA218", VA = "0x1010EA218")]
		public static DateTime GetMsTimeToDateTime(long ftimes)
		{
			return default(DateTime);
		}

		// Token: 0x0600095D RID: 2397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008A5")]
		[Address(RVA = "0x1010EA2D4", Offset = "0x10EA2D4", VA = "0x1010EA2D4")]
		public static void SetDebugMode(bool fena)
		{
		}

		// Token: 0x04000B33 RID: 2867
		[Token(Token = "0x400094D")]
		public static readonly DateTime UnitBaseTime;

		// Token: 0x04000B34 RID: 2868
		[Token(Token = "0x400094E")]
		public const long BASE_TIME_SEC = 10000000L;

		// Token: 0x04000B35 RID: 2869
		[Token(Token = "0x400094F")]
		public const long BASE_TIME_MS = 10000L;

		// Token: 0x04000B36 RID: 2870
		[Token(Token = "0x4000950")]
		public const int TIME_HOUR = 3600;

		// Token: 0x04000B37 RID: 2871
		[Token(Token = "0x4000951")]
		public const int DAILY_START_TIME = 4;

		// Token: 0x04000B38 RID: 2872
		[Token(Token = "0x4000952")]
		public static readonly TimeSpan DailyStartTime;

		// Token: 0x04000B39 RID: 2873
		[Token(Token = "0x4000953")]
		public static long m_DebugDeltaTime;

		// Token: 0x04000B3A RID: 2874
		[Token(Token = "0x4000954")]
		public static long ms_BackTimeKey;

		// Token: 0x04000B3B RID: 2875
		[Token(Token = "0x4000955")]
		public static long m_SettingTimeBase;

		// Token: 0x04000B3C RID: 2876
		[Token(Token = "0x4000956")]
		public static bool m_MainDebug;

		// Token: 0x04000B3D RID: 2877
		[Token(Token = "0x4000957")]
		public static bool m_DebugSetUp;

		// Token: 0x04000B3E RID: 2878
		[Token(Token = "0x4000958")]
		public static float m_DebugSpeed;
	}
}
