﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200030D RID: 781
	[Token(Token = "0x20002A9")]
	[StructLayout(3)]
	public static class ScheduleUtil
	{
		// Token: 0x0600095F RID: 2399 RVA: 0x00004188 File Offset: 0x00002388
		[Token(Token = "0x60008A7")]
		[Address(RVA = "0x1010E074C", Offset = "0x10E074C", VA = "0x1010E074C")]
		public static int Range(int min, int max)
		{
			return 0;
		}

		// Token: 0x06000960 RID: 2400 RVA: 0x000041A0 File Offset: 0x000023A0
		[Token(Token = "0x60008A8")]
		[Address(RVA = "0x1010E9904", Offset = "0x10E9904", VA = "0x1010E9904")]
		public static float Range(float min, float max)
		{
			return 0f;
		}

		// Token: 0x06000961 RID: 2401 RVA: 0x000041B8 File Offset: 0x000023B8
		[Token(Token = "0x60008A9")]
		[Address(RVA = "0x1010E27F4", Offset = "0x10E27F4", VA = "0x1010E27F4")]
		public static DateTime ChangeBaseTimeToDateTime(long fbasetime)
		{
			return default(DateTime);
		}

		// Token: 0x06000962 RID: 2402 RVA: 0x000041D0 File Offset: 0x000023D0
		[Token(Token = "0x60008AA")]
		[Address(RVA = "0x1010E6E34", Offset = "0x10E6E34", VA = "0x1010E6E34")]
		public static long SecToTick(long sec)
		{
			return 0L;
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x000041E8 File Offset: 0x000023E8
		[Token(Token = "0x60008AB")]
		[Address(RVA = "0x1010E28AC", Offset = "0x10E28AC", VA = "0x1010E28AC")]
		public static long TickToSec(long tick)
		{
			return 0L;
		}

		// Token: 0x04000B3F RID: 2879
		[Token(Token = "0x4000959")]
		public static readonly DateTime UnitBaseTime;

		// Token: 0x04000B40 RID: 2880
		[Token(Token = "0x400095A")]
		public static readonly long BaseConstantValue;
	}
}
