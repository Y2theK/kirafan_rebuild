﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F1 RID: 753
	[Token(Token = "0x2000297")]
	[StructLayout(3)]
	public class TownAccessCheck
	{
		// Token: 0x06000904 RID: 2308 RVA: 0x00003E28 File Offset: 0x00002028
		[Token(Token = "0x600086B")]
		[Address(RVA = "0x1010DFD68", Offset = "0x10DFD68", VA = "0x1010DFD68")]
		public static bool IsAccessSearchBuild(ref TownAccessCheck.ChrCheckDataBase pbase, ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			return default(bool);
		}

		// Token: 0x06000905 RID: 2309 RVA: 0x00003E40 File Offset: 0x00002040
		[Token(Token = "0x600086C")]
		[Address(RVA = "0x1010EA518", Offset = "0x10EA518", VA = "0x1010EA518")]
		public static bool IsAccessSearchBuild(ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			return default(bool);
		}

		// Token: 0x06000906 RID: 2310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600086D")]
		[Address(RVA = "0x1010EA614", Offset = "0x10EA614", VA = "0x1010EA614")]
		public TownAccessCheck()
		{
		}

		// Token: 0x020002F2 RID: 754
		[Token(Token = "0x2000D31")]
		public class ChrCheckDataBase
		{
			// Token: 0x06000907 RID: 2311 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D5F")]
			[Address(RVA = "0x1010DFBB8", Offset = "0x10DFBB8", VA = "0x1010DFBB8")]
			public ChrCheckDataBase(UserCharacterData pchrdata)
			{
			}

			// Token: 0x06000908 RID: 2312 RVA: 0x00003E58 File Offset: 0x00002058
			[Token(Token = "0x6005D60")]
			[Address(RVA = "0x1010EA61C", Offset = "0x10EA61C", VA = "0x1010EA61C")]
			public bool IsCheckToCharaClass(int ChkParam)
			{
				return default(bool);
			}

			// Token: 0x06000909 RID: 2313 RVA: 0x00003E70 File Offset: 0x00002070
			[Token(Token = "0x6005D61")]
			[Address(RVA = "0x1010EA62C", Offset = "0x10EA62C", VA = "0x1010EA62C")]
			public bool IsCheckToElement(int ChkParam)
			{
				return default(bool);
			}

			// Token: 0x04000AA7 RID: 2727
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005557")]
			public long m_ManageID;

			// Token: 0x04000AA8 RID: 2728
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005558")]
			public int m_Title;

			// Token: 0x04000AA9 RID: 2729
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005559")]
			public int m_ClassID;

			// Token: 0x04000AAA RID: 2730
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400555A")]
			public int m_Element;
		}

		// Token: 0x020002F3 RID: 755
		[Token(Token = "0x2000D32")]
		public struct BuildCheckDataBase
		{
			// Token: 0x0600090A RID: 2314 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D62")]
			[Address(RVA = "0x10002ECEC", Offset = "0x2ECEC", VA = "0x10002ECEC")]
			public void SetBuildParam(UserTownData.BuildObjectData ptowndata)
			{
			}

			// Token: 0x04000AAB RID: 2731
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400555B")]
			public long m_ManageID;

			// Token: 0x04000AAC RID: 2732
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400555C")]
			public int m_ObjID;

			// Token: 0x04000AAD RID: 2733
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x400555D")]
			public int m_Title;

			// Token: 0x04000AAE RID: 2734
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400555E")]
			public int m_ClassID;

			// Token: 0x04000AAF RID: 2735
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400555F")]
			public int m_ElementID;

			// Token: 0x04000AB0 RID: 2736
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005560")]
			public int m_Category;

			// Token: 0x04000AB1 RID: 2737
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005561")]
			public int m_ResourceID;
		}
	}
}
