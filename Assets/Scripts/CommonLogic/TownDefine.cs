﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000313 RID: 787
	[Token(Token = "0x20002AF")]
	[StructLayout(3)]
	public static class TownDefine
	{
		// Token: 0x04000B66 RID: 2918
		[Token(Token = "0x4000980")]
		public const int BUILD_LEVEL_MODEL_CUT = 1;

		// Token: 0x04000B67 RID: 2919
		[Token(Token = "0x4000981")]
		public const int AREA_CTXID = 0;

		// Token: 0x04000B68 RID: 2920
		[Token(Token = "0x4000982")]
		public const int MENU_BUFID = 268435456;

		// Token: 0x04000B69 RID: 2921
		[Token(Token = "0x4000983")]
		public const int AREA_CONTENTID = 536870912;

		// Token: 0x04000B6A RID: 2922
		[Token(Token = "0x4000984")]
		public const int AREA_BUFID = 1073741824;

		// Token: 0x04000B6B RID: 2923
		[Token(Token = "0x4000985")]
		public const int TYPE_MASKID = 1879048192;

		// Token: 0x04000B6C RID: 2924
		[Token(Token = "0x4000986")]
		public const float CHARA_ICON_SIZE = 0.75f;

		// Token: 0x04000B6D RID: 2925
		[Token(Token = "0x4000987")]
		public const int LAYER_OFFSET = 10;

		// Token: 0x04000B6E RID: 2926
		[Token(Token = "0x4000988")]
		public static long ROOM_ACCESS_MNG_ID;

		// Token: 0x04000B6F RID: 2927
		[Token(Token = "0x4000989")]
		public const int AREA_OBJID = 1;

		// Token: 0x02000314 RID: 788
		[Token(Token = "0x2000D3A")]
		public enum eTownLevelUpConditionCategory
		{
			// Token: 0x04000B71 RID: 2929
			[Token(Token = "0x400556E")]
			Gold,
			// Token: 0x04000B72 RID: 2930
			[Token(Token = "0x400556F")]
			Kirara,
			// Token: 0x04000B73 RID: 2931
			[Token(Token = "0x4005570")]
			UserLv,
			// Token: 0x04000B74 RID: 2932
			[Token(Token = "0x4005571")]
			BuildTime
		}
	}
}
