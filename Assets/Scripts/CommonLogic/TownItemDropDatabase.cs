﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000315 RID: 789
	[Token(Token = "0x20002B0")]
	[StructLayout(3)]
	public class TownItemDropDatabase
	{
		// Token: 0x06000967 RID: 2407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008AF")]
		[Address(RVA = "0x1010E3488", Offset = "0x10E3488", VA = "0x1010E3488")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06000968 RID: 2408 RVA: 0x00004200 File Offset: 0x00002400
		[Token(Token = "0x60008B0")]
		[Address(RVA = "0x1010EA9D0", Offset = "0x10EA9D0", VA = "0x1010EA9D0")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06000969 RID: 2409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008B1")]
		[Address(RVA = "0x1010EA9D8", Offset = "0x10EA9D8", VA = "0x1010EA9D8")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008B2")]
		[Address(RVA = "0x1010EAA28", Offset = "0x10EAA28", VA = "0x1010EAA28")]
		public static TownItemDropDatabase.DropItemTool GetKeyIDToDatabase(int fkeyid)
		{
			return null;
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008B3")]
		[Address(RVA = "0x1010EAB78", Offset = "0x10EAB78", VA = "0x1010EAB78")]
		public static TownItemDropDatabase.DropItemTool CalcDropPeformsnce(ref FieldObjDropItem.DropItemInfo pstate, int fkeyid, int flevel)
		{
			return null;
		}

		// Token: 0x0600096C RID: 2412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008B4")]
		[Address(RVA = "0x1010EAD3C", Offset = "0x10EAD3C", VA = "0x1010EAD3C")]
		public TownItemDropDatabase()
		{
		}

		// Token: 0x04000B75 RID: 2933
		[Token(Token = "0x400098A")]
		public static TownItemDropDatabase.DropItemList ms_DropItemDB;

		// Token: 0x02000316 RID: 790
		[Token(Token = "0x2000D3B")]
		public struct DropItemData
		{
			// Token: 0x04000B76 RID: 2934
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005572")]
			public short m_WakeUp;

			// Token: 0x04000B77 RID: 2935
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005573")]
			public int m_ResultNo;
		}

		// Token: 0x02000317 RID: 791
		[Token(Token = "0x2000D3C")]
		public struct DropItemPakage
		{
			// Token: 0x0600096D RID: 2413 RVA: 0x00004218 File Offset: 0x00002418
			[Token(Token = "0x6005D7D")]
			[Address(RVA = "0x10002ECF4", Offset = "0x2ECF4", VA = "0x10002ECF4")]
			public int CalcItemIndex()
			{
				return 0;
			}

			// Token: 0x0600096E RID: 2414 RVA: 0x00004230 File Offset: 0x00002430
			[Token(Token = "0x6005D7E")]
			[Address(RVA = "0x10002ECFC", Offset = "0x2ECFC", VA = "0x10002ECFC")]
			public bool IsChkListUpOneItem()
			{
				return default(bool);
			}

			// Token: 0x04000B78 RID: 2936
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005574")]
			public int m_Level;

			// Token: 0x04000B79 RID: 2937
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005575")]
			public int m_LimitNum;

			// Token: 0x04000B7A RID: 2938
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005576")]
			public int m_ChkTime;

			// Token: 0x04000B7B RID: 2939
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005577")]
			public TownItemDropDatabase.DropItemData[] m_Table;
		}

		// Token: 0x02000318 RID: 792
		[Token(Token = "0x2000D3D")]
		public class DropItemTool
		{
			// Token: 0x0600096F RID: 2415 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D7F")]
			[Address(RVA = "0x1010EAFD8", Offset = "0x10EAFD8", VA = "0x1010EAFD8")]
			public DropItemTool()
			{
			}

			// Token: 0x04000B7C RID: 2940
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005578")]
			public int m_KeyID;

			// Token: 0x04000B7D RID: 2941
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005579")]
			public TownItemDropDatabase.DropItemPakage[] m_Table;
		}

		// Token: 0x02000319 RID: 793
		[Token(Token = "0x2000D3E")]
		public class DropItemList
		{
			// Token: 0x06000970 RID: 2416 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D80")]
			[Address(RVA = "0x1010EA698", Offset = "0x10EA698", VA = "0x1010EA698")]
			public void SetUpResource()
			{
			}

			// Token: 0x06000971 RID: 2417 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D81")]
			[Address(RVA = "0x1010EA690", Offset = "0x10EA690", VA = "0x1010EA690")]
			public DropItemList()
			{
			}

			// Token: 0x04000B7E RID: 2942
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400557A")]
			public TownItemDropDatabase.DropItemTool[] m_DataList;
		}

		// Token: 0x0200031A RID: 794
		[Token(Token = "0x2000D3F")]
		public class OptionPakageKey
		{
			// Token: 0x06000972 RID: 2418 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D82")]
			[Address(RVA = "0x1010EAFE0", Offset = "0x10EAFE0", VA = "0x1010EAFE0")]
			public void MakeOptionKeyData(ref TownItemDropDatabase.DropItemTool pout)
			{
			}

			// Token: 0x06000973 RID: 2419 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D83")]
			[Address(RVA = "0x1010EB7D0", Offset = "0x10EB7D0", VA = "0x1010EB7D0")]
			public OptionPakageKey()
			{
			}

			// Token: 0x04000B7F RID: 2943
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400557B")]
			public int m_KeyID;

			// Token: 0x04000B80 RID: 2944
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400557C")]
			public List<int> m_TableID;
		}

		// Token: 0x0200031B RID: 795
		[Token(Token = "0x2000D40")]
		public class OptionPakage
		{
			// Token: 0x06000974 RID: 2420 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D84")]
			[Address(RVA = "0x1010EADB4", Offset = "0x10EADB4", VA = "0x1010EADB4")]
			public TownItemDropDatabase.OptionPakageKey GetKey(int fkey)
			{
				return null;
			}

			// Token: 0x06000975 RID: 2421 RVA: 0x00004248 File Offset: 0x00002448
			[Token(Token = "0x6005D85")]
			[Address(RVA = "0x1010EAF08", Offset = "0x10EAF08", VA = "0x1010EAF08")]
			public int GetKeyNum()
			{
				return 0;
			}

			// Token: 0x06000976 RID: 2422 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D86")]
			[Address(RVA = "0x1010EAF68", Offset = "0x10EAF68", VA = "0x1010EAF68")]
			public TownItemDropDatabase.OptionPakageKey GetIndexToKey(int findex)
			{
				return null;
			}

			// Token: 0x06000977 RID: 2423 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D87")]
			[Address(RVA = "0x1010EAD44", Offset = "0x10EAD44", VA = "0x1010EAD44")]
			public OptionPakage()
			{
			}

			// Token: 0x04000B81 RID: 2945
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400557D")]
			public List<TownItemDropDatabase.OptionPakageKey> m_List;
		}
	}
}
