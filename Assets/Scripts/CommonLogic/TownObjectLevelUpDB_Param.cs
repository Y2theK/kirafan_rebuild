﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000301 RID: 769
	[Token(Token = "0x20002A5")]
	[StructLayout(0, Size = 32)]
	public struct TownObjectLevelUpDB_Param
	{
		// Token: 0x04000B11 RID: 2833
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000939")]
		public int m_ID;

		// Token: 0x04000B12 RID: 2834
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400093A")]
		public int m_TargetLv;

		// Token: 0x04000B13 RID: 2835
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400093B")]
		public int m_WakeTime;

		// Token: 0x04000B14 RID: 2836
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400093C")]
		public int m_LimitNum;

		// Token: 0x04000B15 RID: 2837
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400093D")]
		public float[] m_Parsent;

		// Token: 0x04000B16 RID: 2838
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400093E")]
		public uint[] m_ResultNo;
	}
}
