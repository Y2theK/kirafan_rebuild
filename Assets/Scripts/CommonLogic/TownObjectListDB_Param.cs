﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000300 RID: 768
	[Token(Token = "0x20002A4")]
	[StructLayout(0, Size = 60)]
	public struct TownObjectListDB_Param
	{
		// Token: 0x04000B01 RID: 2817
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000929")]
		public int m_ID;

		// Token: 0x04000B02 RID: 2818
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400092A")]
		public int m_Category;

		// Token: 0x04000B03 RID: 2819
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400092B")]
		public int m_Arg;

		// Token: 0x04000B04 RID: 2820
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400092C")]
		public int m_ResourceID;

		// Token: 0x04000B05 RID: 2821
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400092D")]
		public int m_TitleType;

		// Token: 0x04000B06 RID: 2822
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400092E")]
		public int m_ElementID;

		// Token: 0x04000B07 RID: 2823
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400092F")]
		public int m_ClassID;

		// Token: 0x04000B08 RID: 2824
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000930")]
		public float m_BuildTime;

		// Token: 0x04000B09 RID: 2825
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000931")]
		public int m_BuffParamID;

		// Token: 0x04000B0A RID: 2826
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000932")]
		public int m_AccessID;

		// Token: 0x04000B0B RID: 2827
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000933")]
		public int m_ResultID;

		// Token: 0x04000B0C RID: 2828
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000934")]
		public short m_MaxNum;

		// Token: 0x04000B0D RID: 2829
		[Cpp2IlInjected.FieldOffset(Offset = "0x2E")]
		[Token(Token = "0x4000935")]
		public short m_MaxLevel;

		// Token: 0x04000B0E RID: 2830
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000936")]
		public short m_EntryCharaNum;

		// Token: 0x04000B0F RID: 2831
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000937")]
		public int m_LevelUpListID;

		// Token: 0x04000B10 RID: 2832
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000938")]
		public int m_ScheduleTagLink;
	}
}
