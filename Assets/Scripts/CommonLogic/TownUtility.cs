﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200031C RID: 796
	[Token(Token = "0x20002B1")]
	[StructLayout(3)]
	public static class TownUtility
	{
		// Token: 0x06000978 RID: 2424 RVA: 0x00004260 File Offset: 0x00002460
		[Token(Token = "0x60008B5")]
		[Address(RVA = "0x1010EB840", Offset = "0x10EB840", VA = "0x1010EB840")]
		public static int SyncFixKey(int fbaseid)
		{
			return 0;
		}

		// Token: 0x06000979 RID: 2425 RVA: 0x00004278 File Offset: 0x00002478
		[Token(Token = "0x60008B6")]
		[Address(RVA = "0x1010EB848", Offset = "0x10EB848", VA = "0x1010EB848")]
		public static int MakeBuildAreaID(int fbaseid, int fsubid = 0)
		{
			return 0;
		}

		// Token: 0x0600097A RID: 2426 RVA: 0x00004290 File Offset: 0x00002490
		[Token(Token = "0x60008B7")]
		[Address(RVA = "0x1010EB854", Offset = "0x10EB854", VA = "0x1010EB854")]
		public static int GetBuildAreaID(int fbuildpoint)
		{
			return 0;
		}

		// Token: 0x0600097B RID: 2427 RVA: 0x000042A8 File Offset: 0x000024A8
		[Token(Token = "0x60008B8")]
		[Address(RVA = "0x1010EB85C", Offset = "0x10EB85C", VA = "0x1010EB85C")]
		public static int ChangeBuildAreaID(int fbuildpoint, int fareaid)
		{
			return 0;
		}

		// Token: 0x0600097C RID: 2428 RVA: 0x000042C0 File Offset: 0x000024C0
		[Token(Token = "0x60008B9")]
		[Address(RVA = "0x1010EB868", Offset = "0x10EB868", VA = "0x1010EB868")]
		public static int MakeBuildPointID(int fbaseid, int fsubid)
		{
			return 0;
		}

		// Token: 0x0600097D RID: 2429 RVA: 0x000042D8 File Offset: 0x000024D8
		[Token(Token = "0x60008BA")]
		[Address(RVA = "0x1010EB878", Offset = "0x10EB878", VA = "0x1010EB878")]
		public static int MakeBuildContentID(int fbaseid)
		{
			return 0;
		}

		// Token: 0x0600097E RID: 2430 RVA: 0x000042F0 File Offset: 0x000024F0
		[Token(Token = "0x60008BB")]
		[Address(RVA = "0x1010EB888", Offset = "0x10EB888", VA = "0x1010EB888")]
		public static int MakeMenuPointID(int fbaseid)
		{
			return 0;
		}

		// Token: 0x0600097F RID: 2431 RVA: 0x00004308 File Offset: 0x00002508
		[Token(Token = "0x60008BC")]
		[Address(RVA = "0x1010EB890", Offset = "0x10EB890", VA = "0x1010EB890")]
		public static eTownMenuType GetMenuPointEnum(int fbaseid)
		{
			return (eTownMenuType)0;
		}

		// Token: 0x06000980 RID: 2432 RVA: 0x00004320 File Offset: 0x00002520
		[Token(Token = "0x60008BD")]
		[Address(RVA = "0x1010EB898", Offset = "0x10EB898", VA = "0x1010EB898")]
		public static int GetPointToBuildType(int fpoint)
		{
			return 0;
		}

		// Token: 0x06000981 RID: 2433 RVA: 0x00004338 File Offset: 0x00002538
		[Token(Token = "0x60008BE")]
		[Address(RVA = "0x1010EB8A0", Offset = "0x10EB8A0", VA = "0x1010EB8A0")]
		public static eTownObjectCategory GetResourceCategory(int fid)
		{
			return eTownObjectCategory.Room;
		}

		// Token: 0x0200031D RID: 797
		[Token(Token = "0x2000D41")]
		public enum eResCategory
		{
			// Token: 0x04000B83 RID: 2947
			[Token(Token = "0x400557F")]
			Field,
			// Token: 0x04000B84 RID: 2948
			[Token(Token = "0x4005580")]
			Room,
			// Token: 0x04000B85 RID: 2949
			[Token(Token = "0x4005581")]
			Area,
			// Token: 0x04000B86 RID: 2950
			[Token(Token = "0x4005582")]
			Buf,
			// Token: 0x04000B87 RID: 2951
			[Token(Token = "0x4005583")]
			AreaFree,
			// Token: 0x04000B88 RID: 2952
			[Token(Token = "0x4005584")]
			BufFree,
			// Token: 0x04000B89 RID: 2953
			[Token(Token = "0x4005585")]
			MenuBuf,
			// Token: 0x04000B8A RID: 2954
			[Token(Token = "0x4005586")]
			Content,
			// Token: 0x04000B8B RID: 2955
			[Token(Token = "0x4005587")]
			Chara,
			// Token: 0x04000B8C RID: 2956
			[Token(Token = "0x4005588")]
			MenuChara
		}
	}
}
