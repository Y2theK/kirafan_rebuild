﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000320 RID: 800
	[Token(Token = "0x20002B4")]
	[StructLayout(3)]
	public class UserCharaUtil
	{
		// Token: 0x06000996 RID: 2454 RVA: 0x00004410 File Offset: 0x00002610
		[Token(Token = "0x60008D3")]
		[Address(RVA = "0x1010EB91C", Offset = "0x10EB91C", VA = "0x1010EB91C")]
		public static CharacterListDB_Param GetIDToCharaParam(int fcharaid)
		{
			return default(CharacterListDB_Param);
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x00004428 File Offset: 0x00002628
		[Token(Token = "0x60008D4")]
		[Address(RVA = "0x1010EB994", Offset = "0x10EB994", VA = "0x1010EB994")]
		public static NamedListDB_Param GetCharaIDToNameParam(int fcharaid)
		{
			return default(NamedListDB_Param);
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008D5")]
		[Address(RVA = "0x1010EBA1C", Offset = "0x10EBA1C", VA = "0x1010EBA1C")]
		public static UserScheduleData.DropState[] GetScheduleDropState(int fcharaid)
		{
			return null;
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008D6")]
		[Address(RVA = "0x1010EBE78", Offset = "0x10EBE78", VA = "0x1010EBE78")]
		public UserCharaUtil()
		{
		}
	}
}
