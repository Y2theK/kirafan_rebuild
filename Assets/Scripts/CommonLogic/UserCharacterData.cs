﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200031F RID: 799
	[Token(Token = "0x20002B3")]
	[StructLayout(3)]
	public class UserCharacterData
	{
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x000043F8 File Offset: 0x000025F8
		// (set) Token: 0x06000992 RID: 2450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000068")]
		public long MngID
		{
			[Token(Token = "0x60008CE")]
			[Address(RVA = "0x1010EBE80", Offset = "0x10EBE80", VA = "0x1010EBE80")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x60008CF")]
			[Address(RVA = "0x1010EBE88", Offset = "0x10EBE88", VA = "0x1010EBE88")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06000994 RID: 2452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000069")]
		public CharacterParam Param
		{
			[Token(Token = "0x60008D0")]
			[Address(RVA = "0x1010EBE90", Offset = "0x10EBE90", VA = "0x1010EBE90")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60008D1")]
			[Address(RVA = "0x1010EBE98", Offset = "0x10EBE98", VA = "0x1010EBE98")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008D2")]
		[Address(RVA = "0x1010EBEA0", Offset = "0x10EBEA0", VA = "0x1010EBEA0")]
		public UserCharacterData()
		{
		}
	}
}
