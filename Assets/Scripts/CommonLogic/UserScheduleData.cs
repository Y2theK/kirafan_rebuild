﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000321 RID: 801
	[Token(Token = "0x20002B5")]
	[StructLayout(3)]
	public class UserScheduleData
	{
		// Token: 0x0600099A RID: 2458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008D7")]
		[Address(RVA = "0x1010EBF14", Offset = "0x10EBF14", VA = "0x1010EBF14")]
		public UserScheduleData()
		{
		}

		// Token: 0x02000322 RID: 802
		[Token(Token = "0x2000D42")]
		public enum eType
		{
			// Token: 0x04000B97 RID: 2967
			[Token(Token = "0x400558A")]
			Non,
			// Token: 0x04000B98 RID: 2968
			[Token(Token = "0x400558B")]
			Sleep,
			// Token: 0x04000B99 RID: 2969
			[Token(Token = "0x400558C")]
			Room,
			// Token: 0x04000B9A RID: 2970
			[Token(Token = "0x400558D")]
			Office,
			// Token: 0x04000B9B RID: 2971
			[Token(Token = "0x400558E")]
			Town,
			// Token: 0x04000B9C RID: 2972
			[Token(Token = "0x400558F")]
			System,
			// Token: 0x04000B9D RID: 2973
			[Token(Token = "0x4005590")]
			End
		}

		// Token: 0x02000323 RID: 803
		[Token(Token = "0x2000D43")]
		public enum eDropCategory
		{
			// Token: 0x04000B9F RID: 2975
			[Token(Token = "0x4005592")]
			Money,
			// Token: 0x04000BA0 RID: 2976
			[Token(Token = "0x4005593")]
			Item,
			// Token: 0x04000BA1 RID: 2977
			[Token(Token = "0x4005594")]
			KRRPoint,
			// Token: 0x04000BA2 RID: 2978
			[Token(Token = "0x4005595")]
			Stamina,
			// Token: 0x04000BA3 RID: 2979
			[Token(Token = "0x4005596")]
			FriendShip
		}

		// Token: 0x02000324 RID: 804
		[Token(Token = "0x2000D44")]
		public class DropState
		{
			// Token: 0x0600099B RID: 2459 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D88")]
			[Address(RVA = "0x1010EBF1C", Offset = "0x10EBF1C", VA = "0x1010EBF1C")]
			public DropState()
			{
			}

			// Token: 0x04000BA4 RID: 2980
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005597")]
			public UserScheduleData.eDropCategory m_Category;

			// Token: 0x04000BA5 RID: 2981
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005598")]
			public int m_CategoryNo;

			// Token: 0x04000BA6 RID: 2982
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005599")]
			public int m_Num;
		}

		// Token: 0x02000325 RID: 805
		[Token(Token = "0x2000D45")]
		public class Seg
		{
			// Token: 0x0600099C RID: 2460 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D89")]
			[Address(RVA = "0x1010EC138", Offset = "0x10EC138", VA = "0x1010EC138")]
			public Seg()
			{
			}

			// Token: 0x04000BA7 RID: 2983
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400559A")]
			public UserScheduleData.eType m_Type;

			// Token: 0x04000BA8 RID: 2984
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400559B")]
			public int m_WakeTime;

			// Token: 0x04000BA9 RID: 2985
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400559C")]
			public int m_UseTime;

			// Token: 0x04000BAA RID: 2986
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x400559D")]
			public int m_TagNameID;
		}

		// Token: 0x02000326 RID: 806
		[Token(Token = "0x2000D46")]
		public class ListPack
		{
			// Token: 0x0600099D RID: 2461 RVA: 0x00004440 File Offset: 0x00002640
			[Token(Token = "0x6005D8A")]
			[Address(RVA = "0x1010EBF24", Offset = "0x10EBF24", VA = "0x1010EBF24")]
			public int GetListNum()
			{
				return 0;
			}

			// Token: 0x0600099E RID: 2462 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005D8B")]
			[Address(RVA = "0x1010EBF50", Offset = "0x10EBF50", VA = "0x1010EBF50")]
			public UserScheduleData.Seg GetTable(int index)
			{
				return null;
			}

			// Token: 0x0600099F RID: 2463 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D8C")]
			[Address(RVA = "0x1010EBFD4", Offset = "0x10EBFD4", VA = "0x1010EBFD4")]
			public ListPack()
			{
			}

			// Token: 0x04000BAB RID: 2987
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400559E")]
			public UserScheduleData.Seg[] m_Table;

			// Token: 0x04000BAC RID: 2988
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400559F")]
			public long m_SettingTime;

			// Token: 0x04000BAD RID: 2989
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40055A0")]
			public long m_ChangeStateTime;
		}

		// Token: 0x02000327 RID: 807
		[Token(Token = "0x2000D47")]
		public class Play
		{
			// Token: 0x060009A0 RID: 2464 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D8D")]
			[Address(RVA = "0x1010EBFDC", Offset = "0x10EBFDC", VA = "0x1010EBFDC")]
			public Play()
			{
			}

			// Token: 0x060009A1 RID: 2465 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D8E")]
			[Address(RVA = "0x1010EC008", Offset = "0x10EC008", VA = "0x1010EC008")]
			public void StackList(UserScheduleData.ListPack plist)
			{
			}

			// Token: 0x04000BAE RID: 2990
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40055A1")]
			public UserScheduleData.ListPack[] m_DayListUp;

			// Token: 0x04000BAF RID: 2991
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40055A2")]
			public long m_PlayTime;

			// Token: 0x04000BB0 RID: 2992
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40055A3")]
			public int m_Version;

			// Token: 0x04000BB1 RID: 2993
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40055A4")]
			public int m_DropItemID;

			// Token: 0x04000BB2 RID: 2994
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40055A5")]
			public float m_DropMagKey;
		}
	}
}
