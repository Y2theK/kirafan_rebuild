﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000328 RID: 808
	[Token(Token = "0x20002B6")]
	[StructLayout(3)]
	public class UserTownData
	{
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060009A2 RID: 2466 RVA: 0x00004458 File Offset: 0x00002658
		// (set) Token: 0x060009A3 RID: 2467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700006A")]
		public long MngID
		{
			[Token(Token = "0x60008D8")]
			[Address(RVA = "0x1010EC140", Offset = "0x10EC140", VA = "0x1010EC140")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x60008D9")]
			[Address(RVA = "0x1010EC148", Offset = "0x10EC148", VA = "0x1010EC148")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x00004470 File Offset: 0x00002670
		[Token(Token = "0x60008DA")]
		[Address(RVA = "0x1010EC150", Offset = "0x10EC150", VA = "0x1010EC150")]
		public int GetBuildObjectDataNum()
		{
			return 0;
		}

		// Token: 0x060009A5 RID: 2469 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008DB")]
		[Address(RVA = "0x1010EC1B0", Offset = "0x10EC1B0", VA = "0x1010EC1B0")]
		public UserTownData.BuildObjectData GetBuildObjectDataAt(int idx)
		{
			return null;
		}

		// Token: 0x060009A6 RID: 2470 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008DC")]
		[Address(RVA = "0x1010EC220", Offset = "0x10EC220", VA = "0x1010EC220")]
		public UserTownData.BuildObjectData GetBuildObjectDataAtBuildMngID(long buildmngid)
		{
			return null;
		}

		// Token: 0x060009A7 RID: 2471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008DD")]
		[Address(RVA = "0x1010EC320", Offset = "0x10EC320", VA = "0x1010EC320")]
		public UserTownData()
		{
		}

		// Token: 0x04000BB3 RID: 2995
		[Token(Token = "0x4000994")]
		public const int Version = 1;

		// Token: 0x04000BB5 RID: 2997
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000996")]
		public List<UserTownData.BuildObjectData> m_BuildObjectDatas;

		// Token: 0x02000329 RID: 809
		[Token(Token = "0x2000D48")]
		public class BuildObjectData
		{
			// Token: 0x060009A8 RID: 2472 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D8F")]
			[Address(RVA = "0x1010EC390", Offset = "0x10EC390", VA = "0x1010EC390")]
			public BuildObjectData()
			{
			}

			// Token: 0x04000BB6 RID: 2998
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40055A6")]
			public long m_ManageID;

			// Token: 0x04000BB7 RID: 2999
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40055A7")]
			public int m_BuildPointIndex;

			// Token: 0x04000BB8 RID: 3000
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40055A8")]
			public int m_ObjID;

			// Token: 0x04000BB9 RID: 3001
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40055A9")]
			public int m_Lv;

			// Token: 0x04000BBA RID: 3002
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40055AA")]
			public bool m_IsOpen;
		}
	}
}
