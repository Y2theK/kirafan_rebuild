﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200032A RID: 810
	[Token(Token = "0x20002B7")]
	[StructLayout(3)]
	public class UserTownUtil
	{
		// Token: 0x060009A9 RID: 2473 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60008DE")]
		[Address(RVA = "0x1010EBAE8", Offset = "0x10EBAE8", VA = "0x1010EBAE8")]
		public static UserScheduleData.DropState GetFieldDropItems(int fkeyid)
		{
			return null;
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x00004488 File Offset: 0x00002688
		[Token(Token = "0x60008DF")]
		[Address(RVA = "0x1010EC398", Offset = "0x10EC398", VA = "0x1010EC398")]
		public static bool CalcTownBuildToDropItemState(ref FieldObjDropItem.DropItemInfo pstate, long fmanageid, UserTownData userTownData)
		{
			return default(bool);
		}

		// Token: 0x060009AB RID: 2475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60008E0")]
		[Address(RVA = "0x1010EC468", Offset = "0x10EC468", VA = "0x1010EC468")]
		public UserTownUtil()
		{
		}
	}
}
