﻿namespace CommonLogic {
    public class WeaponEquipChange {
        private long[] m_BeforeManagedWeaponIds;
        private long[] m_AfterManagedWeaponIds;

        private WeaponEquipChange() { }

        public WeaponEquipChange(long[] beforeManagedWeaponIds, long[] afterManagedWeaponIds) {
            m_BeforeManagedWeaponIds = beforeManagedWeaponIds;
            m_AfterManagedWeaponIds = afterManagedWeaponIds;
        }

        public bool CheckChanged() {
            for (int i = 0; i < m_BeforeManagedWeaponIds.Length; i++) {
                if (m_BeforeManagedWeaponIds[i] != m_AfterManagedWeaponIds[i]) {
                    return true;
                }
            }
            return false;
        }
    }
}
