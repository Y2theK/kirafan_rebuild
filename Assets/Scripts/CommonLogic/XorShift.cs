﻿namespace CommonLogic {
    public class XorShift {
        private uint x = 0x00860134;
        private uint y = 0x4ca1d9bc;
        private uint z = 0x003529fc;
        private uint w = 0x2f0a9c4b;

        public void SetS(uint t0, uint t1, uint t2, uint t3) {
            x = t0;
            y = t1;
            z = t2;
            w = t3;
        }

        public uint Get() {
            uint r = x ^ x << 0xb;
            x = y;
            y = z;
            r = r ^ r >> 8 ^ w ^ w >> 0x13;
            z = w;
            w = r;
            return r;
        }
    }
}
