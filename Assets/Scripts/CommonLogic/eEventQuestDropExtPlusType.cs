﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002C8 RID: 712
	[Token(Token = "0x2000276")]
	[StructLayout(3, Size = 4)]
	public enum eEventQuestDropExtPlusType
	{
		// Token: 0x04000A3A RID: 2618
		[Token(Token = "0x400088E")]
		Self,
		// Token: 0x04000A3B RID: 2619
		[Token(Token = "0x400088F")]
		Friend,
		// Token: 0x04000A3C RID: 2620
		[Token(Token = "0x4000890")]
		Guest,
		// Token: 0x04000A3D RID: 2621
		[Token(Token = "0x4000891")]
		Npc
	}
}
