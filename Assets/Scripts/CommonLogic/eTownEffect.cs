﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000312 RID: 786
	[Token(Token = "0x20002AE")]
	[StructLayout(3, Size = 4)]
	public enum eTownEffect
	{
		// Token: 0x04000B60 RID: 2912
		[Token(Token = "0x400097A")]
		BuildMoney,
		// Token: 0x04000B61 RID: 2913
		[Token(Token = "0x400097B")]
		BuildItem,
		// Token: 0x04000B62 RID: 2914
		[Token(Token = "0x400097C")]
		TouchKRR,
		// Token: 0x04000B63 RID: 2915
		[Token(Token = "0x400097D")]
		BuildLevelUp,
		// Token: 0x04000B64 RID: 2916
		[Token(Token = "0x400097E")]
		AreaCreate,
		// Token: 0x04000B65 RID: 2917
		[Token(Token = "0x400097F")]
		ContentChange
	}
}
