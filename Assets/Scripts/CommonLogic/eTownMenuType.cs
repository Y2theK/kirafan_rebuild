﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x0200030F RID: 783
	[Token(Token = "0x20002AB")]
	[StructLayout(3, Size = 4)]
	public enum eTownMenuType
	{
		// Token: 0x04000B43 RID: 2883
		[Token(Token = "0x400095D")]
		Quest = 1200,
		// Token: 0x04000B44 RID: 2884
		[Token(Token = "0x400095E")]
		Edit,
		// Token: 0x04000B45 RID: 2885
		[Token(Token = "0x400095F")]
		Gacha,
		// Token: 0x04000B46 RID: 2886
		[Token(Token = "0x4000960")]
		Traning,
		// Token: 0x04000B47 RID: 2887
		[Token(Token = "0x4000961")]
		Shop,
		// Token: 0x04000B48 RID: 2888
		[Token(Token = "0x4000962")]
		Mission,
		// Token: 0x04000B49 RID: 2889
		[Token(Token = "0x4000963")]
		Max
	}
}
