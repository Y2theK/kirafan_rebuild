﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002D1 RID: 721
	[Token(Token = "0x200027C")]
	[StructLayout(3, Size = 4)]
	public enum eTownObjectCategory
	{
		// Token: 0x04000A4F RID: 2639
		[Token(Token = "0x400089A")]
		Room,
		// Token: 0x04000A50 RID: 2640
		[Token(Token = "0x400089B")]
		Item,
		// Token: 0x04000A51 RID: 2641
		[Token(Token = "0x400089C")]
		Money,
		// Token: 0x04000A52 RID: 2642
		[Token(Token = "0x400089D")]
		Num,
		// Token: 0x04000A53 RID: 2643
		[Token(Token = "0x400089E")]
		Area,
		// Token: 0x04000A54 RID: 2644
		[Token(Token = "0x400089F")]
		Buf,
		// Token: 0x04000A55 RID: 2645
		[Token(Token = "0x40008A0")]
		Contents,
		// Token: 0x04000A56 RID: 2646
		[Token(Token = "0x40008A1")]
		Menu
	}
}
