﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000310 RID: 784
	[Token(Token = "0x20002AC")]
	[StructLayout(3, Size = 4)]
	public enum eTownObjectType
	{
		// Token: 0x04000B4B RID: 2891
		[Token(Token = "0x4000965")]
		BUILD_ITEM_POP,
		// Token: 0x04000B4C RID: 2892
		[Token(Token = "0x4000966")]
		CHARA_MODEL,
		// Token: 0x04000B4D RID: 2893
		[Token(Token = "0x4000967")]
		DUMMY,
		// Token: 0x04000B4E RID: 2894
		[Token(Token = "0x4000968")]
		CHARA_TOUCH_ITEM,
		// Token: 0x04000B4F RID: 2895
		[Token(Token = "0x4000969")]
		BUILD_ANIME_GROUP,
		// Token: 0x04000B50 RID: 2896
		[Token(Token = "0x400096A")]
		AREA_ANIME_GROUP,
		// Token: 0x04000B51 RID: 2897
		[Token(Token = "0x400096B")]
		TRANING_ICON,
		// Token: 0x04000B52 RID: 2898
		[Token(Token = "0x400096C")]
		CHRA_HUD,
		// Token: 0x04000B53 RID: 2899
		[Token(Token = "0x400096D")]
		GET_ITEM,
		// Token: 0x04000B54 RID: 2900
		[Token(Token = "0x400096E")]
		BUILDING_OBJ_00,
		// Token: 0x04000B55 RID: 2901
		[Token(Token = "0x400096F")]
		BUILDING_OBJ_01
	}
}
