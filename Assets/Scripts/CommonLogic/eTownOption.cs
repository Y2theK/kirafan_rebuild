﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x02000311 RID: 785
	[Token(Token = "0x20002AD")]
	[StructLayout(3, Size = 4)]
	public enum eTownOption
	{
		// Token: 0x04000B57 RID: 2903
		[Token(Token = "0x4000971")]
		Non,
		// Token: 0x04000B58 RID: 2904
		[Token(Token = "0x4000972")]
		System,
		// Token: 0x04000B59 RID: 2905
		[Token(Token = "0x4000973")]
		Area,
		// Token: 0x04000B5A RID: 2906
		[Token(Token = "0x4000974")]
		Buf,
		// Token: 0x04000B5B RID: 2907
		[Token(Token = "0x4000975")]
		Content,
		// Token: 0x04000B5C RID: 2908
		[Token(Token = "0x4000976")]
		HitContent,
		// Token: 0x04000B5D RID: 2909
		[Token(Token = "0x4000977")]
		HitBuf,
		// Token: 0x04000B5E RID: 2910
		[Token(Token = "0x4000978")]
		Point
	}
}
