﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F9 RID: 761
	[Token(Token = "0x200029D")]
	[StructLayout(3, Size = 4)]
	public enum eXlsBuildMoveCheckType
	{
		// Token: 0x04000AC7 RID: 2759
		[Token(Token = "0x40008EF")]
		Normal,
		// Token: 0x04000AC8 RID: 2760
		[Token(Token = "0x40008F0")]
		ResourceID,
		// Token: 0x04000AC9 RID: 2761
		[Token(Token = "0x40008F1")]
		SelfClass,
		// Token: 0x04000ACA RID: 2762
		[Token(Token = "0x40008F2")]
		SelfElement,
		// Token: 0x04000ACB RID: 2763
		[Token(Token = "0x40008F3")]
		StrengBuild,
		// Token: 0x04000ACC RID: 2764
		[Token(Token = "0x40008F4")]
		ProductBuild,
		// Token: 0x04000ACD RID: 2765
		[Token(Token = "0x40008F5")]
		ClassBuild,
		// Token: 0x04000ACE RID: 2766
		[Token(Token = "0x40008F6")]
		ElementBuild
	}
}
