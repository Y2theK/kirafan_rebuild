﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002D2 RID: 722
	[Token(Token = "0x200027D")]
	[StructLayout(3, Size = 4)]
	public enum eXlsDropCalcType
	{
		// Token: 0x04000A58 RID: 2648
		[Token(Token = "0x40008A3")]
		normal,
		// Token: 0x04000A59 RID: 2649
		[Token(Token = "0x40008A4")]
		chara,
		// Token: 0x04000A5A RID: 2650
		[Token(Token = "0x40008A5")]
		build
	}
}
