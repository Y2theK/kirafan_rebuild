﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F6 RID: 758
	[Token(Token = "0x200029A")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakePoint
	{
		// Token: 0x04000AB8 RID: 2744
		[Token(Token = "0x40008E0")]
		Sleep,
		// Token: 0x04000AB9 RID: 2745
		[Token(Token = "0x40008E1")]
		Room,
		// Token: 0x04000ABA RID: 2746
		[Token(Token = "0x40008E2")]
		Content,
		// Token: 0x04000ABB RID: 2747
		[Token(Token = "0x40008E3")]
		Buf,
		// Token: 0x04000ABC RID: 2748
		[Token(Token = "0x40008E4")]
		Menu
	}
}
