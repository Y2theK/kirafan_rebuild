﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F5 RID: 757
	[Token(Token = "0x2000299")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakeTag
	{
		// Token: 0x04000AB3 RID: 2739
		[Token(Token = "0x40008DB")]
		S,
		// Token: 0x04000AB4 RID: 2740
		[Token(Token = "0x40008DC")]
		A,
		// Token: 0x04000AB5 RID: 2741
		[Token(Token = "0x40008DD")]
		B,
		// Token: 0x04000AB6 RID: 2742
		[Token(Token = "0x40008DE")]
		C
	}
}
