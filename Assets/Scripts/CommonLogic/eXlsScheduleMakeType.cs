﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F7 RID: 759
	[Token(Token = "0x200029B")]
	[StructLayout(3, Size = 4)]
	public enum eXlsScheduleMakeType
	{
		// Token: 0x04000ABE RID: 2750
		[Token(Token = "0x40008E6")]
		Fix,
		// Token: 0x04000ABF RID: 2751
		[Token(Token = "0x40008E7")]
		Derive,
		// Token: 0x04000AC0 RID: 2752
		[Token(Token = "0x40008E8")]
		Free,
		// Token: 0x04000AC1 RID: 2753
		[Token(Token = "0x40008E9")]
		Per
	}
}
