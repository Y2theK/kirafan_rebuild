﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace CommonLogic
{
	// Token: 0x020002F8 RID: 760
	[Token(Token = "0x200029C")]
	[StructLayout(3, Size = 4)]
	public enum eXlsTownUseResourceType
	{
		// Token: 0x04000AC3 RID: 2755
		[Token(Token = "0x40008EB")]
		Non,
		// Token: 0x04000AC4 RID: 2756
		[Token(Token = "0x40008EC")]
		ContentNum,
		// Token: 0x04000AC5 RID: 2757
		[Token(Token = "0x40008ED")]
		RoomAdd
	}
}
