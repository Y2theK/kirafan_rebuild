﻿using System;
using WWWTypes;

namespace CommonResponseTypes {
    public class CommonResponse {
        public int resultCode;
        public DateTime serverTime;
        public int serverVersion;
        public string subject;
        public string message;
        public int newAchievementCount;

        public ResultCode GetResult() {
            return (ResultCode)resultCode;
        }

        public virtual string ToMessage() {
            string str = string.Empty;
            str += resultCode.ToString();
            str += serverTime.ToString();
            str += serverVersion.ToString();
            str += (subject == null) ? string.Empty : subject;
            str += (message == null) ? string.Empty : message;
            str += newAchievementCount.ToString();
            return str;
        }
    }
}
