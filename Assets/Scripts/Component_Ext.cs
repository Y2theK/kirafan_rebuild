﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200001E RID: 30
[Token(Token = "0x2000017")]
[StructLayout(3)]
public static class Component_Ext
{
	// Token: 0x0600005C RID: 92 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000050")]
	[Address(RVA = "0x102438A7C", Offset = "0x2438A7C", VA = "0x102438A7C")]
	public static void RemoveComponent<T>(this Component self) where T : Component
	{
	}
}
