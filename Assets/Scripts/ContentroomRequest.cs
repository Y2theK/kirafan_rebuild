﻿using ContentroomRequestTypes;
using Meige;

public static class ContentroomRequest {
    public static MeigewwwParam Membergetall(Membergetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/member/get_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleType", param.titleType);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Membergetall(int titleType, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/member/get_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleType", titleType);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Membersetall(Membersetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/member/set_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleType", param.titleType);
        www.Add("managedCharacterIds", param.managedCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Membersetall(int titleType, long[] managedCharacterIds, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/member/set_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleType", titleType);
        www.Add("managedCharacterIds", managedCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setshown(Setshown param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleTypes", param.titleTypes);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setshown(int[] titleTypes, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/content_room/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("titleTypes", titleTypes);
        www.SetPostCrypt();
        return www;
    }
}
