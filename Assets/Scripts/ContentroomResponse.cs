﻿using ContentroomResponseTypes;
using Meige;
using System.Collections.Generic;
using WWWTypes;

public static class ContentroomResponse {
    public static Membergetall Membergetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Membergetall>(param, dialogType, acceptableResultCodes);
    }

    public static Membersetall Membersetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Membersetall>(param, dialogType, acceptableResultCodes);
    }

    public static Setshown Setshown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Setshown>(param, dialogType, acceptableResultCodes);
    }
}
