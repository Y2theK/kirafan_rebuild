﻿using CommonResponseTypes;

namespace ContentroomResponseTypes {
    public class Membergetall : CommonResponse {
        public long[] managedCharacterIds;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedCharacterIds == null) ? string.Empty : managedCharacterIds.ToString();
            return str;
        }
    }
}
