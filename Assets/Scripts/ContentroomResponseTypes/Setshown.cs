﻿using CommonResponseTypes;
using WWWTypes;

namespace ContentroomResponseTypes {
    public class Setshown : CommonResponse {
        public OfferTitleType[] offerTitleTypes;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            return str;
        }
    }
}
