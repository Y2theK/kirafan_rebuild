﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x0200137C RID: 4988
	internal class AddressAttribute : Attribute
	{
		// Token: 0x04007651 RID: 30289
		public string RVA;

		// Token: 0x04007652 RID: 30290
		public string Offset;

		// Token: 0x04007653 RID: 30291
		public string VA;

		// Token: 0x04007654 RID: 30292
		public string Slot;
	}
}
