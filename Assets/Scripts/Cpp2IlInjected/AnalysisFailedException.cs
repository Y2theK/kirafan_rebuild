﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x02001381 RID: 4993
	internal class AnalysisFailedException : Exception
	{
		// Token: 0x06006568 RID: 25960 RVA: 0x00021966 File Offset: 0x0001FB66
		public AnalysisFailedException(string message) : base(message)
		{
		}
	}
}
