﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x0200137E RID: 4990
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class AttributeAttribute : Attribute
	{
		// Token: 0x04007656 RID: 30294
		public string Name;

		// Token: 0x04007657 RID: 30295
		public string RVA;

		// Token: 0x04007658 RID: 30296
		public string Offset;
	}
}
