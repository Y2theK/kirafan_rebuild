﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x0200137D RID: 4989
	internal class FieldOffsetAttribute : Attribute
	{
		// Token: 0x04007655 RID: 30293
		public string Offset;
	}
}
