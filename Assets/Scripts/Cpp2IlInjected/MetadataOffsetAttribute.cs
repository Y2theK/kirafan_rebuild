﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x0200137F RID: 4991
	internal class MetadataOffsetAttribute : Attribute
	{
		// Token: 0x04007659 RID: 30297
		public string Offset;
	}
}
