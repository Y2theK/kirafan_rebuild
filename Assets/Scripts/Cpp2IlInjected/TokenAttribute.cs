﻿using System;

namespace Cpp2IlInjected
{
	// Token: 0x02001380 RID: 4992
	internal class TokenAttribute : Attribute
	{
		// Token: 0x0400765A RID: 30298
		public string Token;
	}
}
