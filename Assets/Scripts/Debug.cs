﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200004E RID: 78
[Token(Token = "0x2000029")]
[StructLayout(3)]
public static class Debug
{
	// Token: 0x0600019D RID: 413 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000156")]
	[Address(RVA = "0x1010ED0E4", Offset = "0x10ED0E4", VA = "0x1010ED0E4")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100134EF4", Offset = "0x134EF4")]
	public static void Assert(bool condition)
	{
	}

	// Token: 0x0600019E RID: 414 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000157")]
	[Address(RVA = "0x1010ED0E8", Offset = "0x10ED0E8", VA = "0x1010ED0E8")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100134F2C", Offset = "0x134F2C")]
	public static void Assert(bool condition, string message)
	{
	}

	// Token: 0x0600019F RID: 415 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000158")]
	[Address(RVA = "0x1010ED0EC", Offset = "0x10ED0EC", VA = "0x1010ED0EC")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100134F64", Offset = "0x134F64")]
	public static void Assert(bool condition, object message)
	{
	}

	// Token: 0x060001A0 RID: 416 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000159")]
	[Address(RVA = "0x1010ED0F0", Offset = "0x10ED0F0", VA = "0x1010ED0F0")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100134F9C", Offset = "0x134F9C")]
	public static void Assert(bool condition, string message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001A1 RID: 417 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015A")]
	[Address(RVA = "0x1010ED0F4", Offset = "0x10ED0F4", VA = "0x1010ED0F4")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100134FD4", Offset = "0x134FD4")]
	public static void Assert(bool condition, object message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001A2 RID: 418 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015B")]
	[Address(RVA = "0x1010ED0F8", Offset = "0x10ED0F8", VA = "0x1010ED0F8")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013500C", Offset = "0x13500C")]
	public static void AssertFormat(bool condition, string format, params object[] args)
	{
	}

	// Token: 0x060001A3 RID: 419 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015C")]
	[Address(RVA = "0x1010ED0FC", Offset = "0x10ED0FC", VA = "0x1010ED0FC")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135044", Offset = "0x135044")]
	public static void AssertFormat(bool condition, UnityEngine.Object context, string format, params object[] args)
	{
	}

	// Token: 0x060001A4 RID: 420 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015D")]
	[Address(RVA = "0x1010ED100", Offset = "0x10ED100", VA = "0x1010ED100")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013507C", Offset = "0x13507C")]
	public static void Break()
	{
	}

	// Token: 0x060001A5 RID: 421 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015E")]
	[Address(RVA = "0x1010ED104", Offset = "0x10ED104", VA = "0x1010ED104")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001350B4", Offset = "0x1350B4")]
	public static void ClearDeveloperConsole()
	{
	}

	// Token: 0x060001A6 RID: 422 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600015F")]
	[Address(RVA = "0x1010ED108", Offset = "0x10ED108", VA = "0x1010ED108")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001350EC", Offset = "0x1350EC")]
	public static void DebugBreak()
	{
	}

	// Token: 0x060001A7 RID: 423 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000160")]
	[Address(RVA = "0x1010ED10C", Offset = "0x10ED10C", VA = "0x1010ED10C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135124", Offset = "0x135124")]
	public static void DrawLine(Vector3 start, Vector3 end)
	{
	}

	// Token: 0x060001A8 RID: 424 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000161")]
	[Address(RVA = "0x1010ED110", Offset = "0x10ED110", VA = "0x1010ED110")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013515C", Offset = "0x13515C")]
	public static void DrawLine(Vector3 start, Vector3 end, Color color)
	{
	}

	// Token: 0x060001A9 RID: 425 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000162")]
	[Address(RVA = "0x1010ED114", Offset = "0x10ED114", VA = "0x1010ED114")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135194", Offset = "0x135194")]
	public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
	{
	}

	// Token: 0x060001AA RID: 426 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000163")]
	[Address(RVA = "0x1010ED118", Offset = "0x10ED118", VA = "0x1010ED118")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001351CC", Offset = "0x1351CC")]
	public static void DrawLine(Vector3 start, Vector3 end, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F330", Offset = "0x13F330")] Color color, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F368", Offset = "0x13F368")] float duration, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F3A0", Offset = "0x13F3A0")] bool depthTest)
	{
	}

	// Token: 0x060001AB RID: 427 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000164")]
	[Address(RVA = "0x1010ED11C", Offset = "0x10ED11C", VA = "0x1010ED11C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135204", Offset = "0x135204")]
	public static void DrawRay(Vector3 start, Vector3 dir)
	{
	}

	// Token: 0x060001AC RID: 428 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000165")]
	[Address(RVA = "0x1010ED120", Offset = "0x10ED120", VA = "0x1010ED120")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013523C", Offset = "0x13523C")]
	public static void DrawRay(Vector3 start, Vector3 dir, Color color)
	{
	}

	// Token: 0x060001AD RID: 429 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000166")]
	[Address(RVA = "0x1010ED124", Offset = "0x10ED124", VA = "0x1010ED124")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135274", Offset = "0x135274")]
	public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
	{
	}

	// Token: 0x060001AE RID: 430 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000167")]
	[Address(RVA = "0x1010ED128", Offset = "0x10ED128", VA = "0x1010ED128")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001352AC", Offset = "0x1352AC")]
	public static void DrawRay(Vector3 start, Vector3 dir, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F3D8", Offset = "0x13F3D8")] Color color, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F410", Offset = "0x13F410")] float duration, [Attribute(Name = "DefaultValueAttribute", RVA = "0x10013F448", Offset = "0x13F448")] bool depthTest)
	{
	}

	// Token: 0x060001AF RID: 431 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000168")]
	[Address(RVA = "0x1010ED12C", Offset = "0x10ED12C", VA = "0x1010ED12C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001352E4", Offset = "0x1352E4")]
	public static void Log(object o)
	{
	}

	// Token: 0x060001B0 RID: 432 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000169")]
	[Address(RVA = "0x1010ED130", Offset = "0x10ED130", VA = "0x1010ED130")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013531C", Offset = "0x13531C")]
	public static void Log(object message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001B1 RID: 433 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016A")]
	[Address(RVA = "0x1010ED134", Offset = "0x10ED134", VA = "0x1010ED134")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135354", Offset = "0x135354")]
	public static void LogAssertion(object message)
	{
	}

	// Token: 0x060001B2 RID: 434 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016B")]
	[Address(RVA = "0x1010ED138", Offset = "0x10ED138", VA = "0x1010ED138")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013538C", Offset = "0x13538C")]
	public static void LogAssertion(object message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001B3 RID: 435 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016C")]
	[Address(RVA = "0x1010ED13C", Offset = "0x10ED13C", VA = "0x1010ED13C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001353C4", Offset = "0x1353C4")]
	public static void LogAssertionFormat(string format, params object[] args)
	{
	}

	// Token: 0x060001B4 RID: 436 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016D")]
	[Address(RVA = "0x1010ED140", Offset = "0x10ED140", VA = "0x1010ED140")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001353FC", Offset = "0x1353FC")]
	public static void LogAssertionFormat(UnityEngine.Object context, string format, params object[] args)
	{
	}

	// Token: 0x060001B5 RID: 437 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016E")]
	[Address(RVA = "0x1010ED144", Offset = "0x10ED144", VA = "0x1010ED144")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135434", Offset = "0x135434")]
	public static void LogError(object message)
	{
	}

	// Token: 0x060001B6 RID: 438 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600016F")]
	[Address(RVA = "0x1010ED148", Offset = "0x10ED148", VA = "0x1010ED148")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013546C", Offset = "0x13546C")]
	public static void LogError(object message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001B7 RID: 439 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000170")]
	[Address(RVA = "0x1010ED14C", Offset = "0x10ED14C", VA = "0x1010ED14C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001354A4", Offset = "0x1354A4")]
	public static void LogErrorFormat(string format, params object[] args)
	{
	}

	// Token: 0x060001B8 RID: 440 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000171")]
	[Address(RVA = "0x1010ED150", Offset = "0x10ED150", VA = "0x1010ED150")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001354DC", Offset = "0x1354DC")]
	public static void LogErrorFormat(UnityEngine.Object context, string format, params object[] args)
	{
	}

	// Token: 0x060001B9 RID: 441 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000172")]
	[Address(RVA = "0x1010ED154", Offset = "0x10ED154", VA = "0x1010ED154")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135514", Offset = "0x135514")]
	public static void LogException(Exception exception)
	{
	}

	// Token: 0x060001BA RID: 442 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000173")]
	[Address(RVA = "0x1010ED158", Offset = "0x10ED158", VA = "0x1010ED158")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013554C", Offset = "0x13554C")]
	public static void LogException(Exception exception, UnityEngine.Object context)
	{
	}

	// Token: 0x060001BB RID: 443 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000174")]
	[Address(RVA = "0x1010ED15C", Offset = "0x10ED15C", VA = "0x1010ED15C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135584", Offset = "0x135584")]
	public static void LogFormat(string format, params object[] args)
	{
	}

	// Token: 0x060001BC RID: 444 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000175")]
	[Address(RVA = "0x1010ED160", Offset = "0x10ED160", VA = "0x1010ED160")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001355BC", Offset = "0x1355BC")]
	public static void LogFormat(UnityEngine.Object context, string format, params object[] args)
	{
	}

	// Token: 0x060001BD RID: 445 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000176")]
	[Address(RVA = "0x1010ED164", Offset = "0x10ED164", VA = "0x1010ED164")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x1001355F4", Offset = "0x1355F4")]
	public static void LogWarning(object message)
	{
	}

	// Token: 0x060001BE RID: 446 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000177")]
	[Address(RVA = "0x1010ED168", Offset = "0x10ED168", VA = "0x1010ED168")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013562C", Offset = "0x13562C")]
	public static void LogWarning(object message, UnityEngine.Object context)
	{
	}

	// Token: 0x060001BF RID: 447 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000178")]
	[Address(RVA = "0x1010ED16C", Offset = "0x10ED16C", VA = "0x1010ED16C")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x100135664", Offset = "0x135664")]
	public static void LogWarningFormat(string format, params object[] args)
	{
	}

	// Token: 0x060001C0 RID: 448 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000179")]
	[Address(RVA = "0x1010ED170", Offset = "0x10ED170", VA = "0x1010ED170")]
	[Attribute(Name = "ConditionalAttribute", RVA = "0x10013569C", Offset = "0x13569C")]
	public static void LogWarningFormat(UnityEngine.Object context, string format, params object[] args)
	{
	}
}
