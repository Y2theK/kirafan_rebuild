﻿using System.Collections.Generic;

public static class Dictionary_Ext {
    public static void AddOrReplace<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key, TValue value) {
        if (self.ContainsKey(key)) {
            self[key] = value;
        } else {
            self.Add(key, value);
        }
    }
}
