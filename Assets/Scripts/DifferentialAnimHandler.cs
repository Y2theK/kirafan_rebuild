﻿using UnityEngine;

public class DifferentialAnimHandler : MonoBehaviour {
    private static Vector3 INITIAL_POS = Vector3.zero;
    private static Vector3 INITIAL_EULER = new Vector3(270f, 0f, 0f);

    private Transform m_ThisTransform;
    private Animation m_ThisAnimation;
    private AnimationState m_AnimStat;

    private Vector3 m_OldPosition;
    private Vector3 m_NewPosition;
    private Vector3 m_DiffPosition;
    private Vector3 m_OldEulerAngle;
    private Vector3 m_NewEulerAngle;
    private Vector3 m_DiffEulerAngle;

    private void Awake() {
        m_ThisTransform = transform;
        m_ThisAnimation = GetComponent<Animation>();
        m_ThisAnimation.enabled = false;
        m_AnimStat = null;
        if (m_ThisAnimation.clip != null) {
            UpdateAnim(m_ThisAnimation.clip.name, WrapMode.Loop);
        }
    }

	public void Sample(float normalizedTime) {
        if (m_AnimStat == null) {
            return;
        }

        if ((m_AnimStat.wrapMode == WrapMode.Loop && (int)normalizedTime > (int)m_AnimStat.normalizedTime) || (m_AnimStat.wrapMode == WrapMode.Once && normalizedTime < m_AnimStat.normalizedTime)) {
            m_NewPosition = INITIAL_POS;
            m_NewEulerAngle = INITIAL_EULER;
        }
        m_OldEulerAngle = m_NewEulerAngle;
        m_OldPosition = m_NewPosition;
        m_AnimStat.normalizedTime = normalizedTime;
        m_ThisAnimation.enabled = true;
        m_ThisAnimation.Sample();
        m_ThisAnimation.enabled = false;
        m_NewEulerAngle = m_ThisTransform.localEulerAngles;
        m_NewPosition = m_ThisTransform.localPosition;
        m_DiffEulerAngle = m_NewEulerAngle - m_OldEulerAngle;
        m_DiffPosition = m_NewPosition - m_OldPosition;
    }

	public Animation GetAnimation() {
        return m_ThisAnimation;
    }

	public bool IsAvailable() {
        return m_AnimStat != null;
    }

	public Vector3 GetNewPosition() {
        return m_NewPosition;
    }

	public Vector3 GetNewEulerAngle() {
        return m_NewEulerAngle;
    }

	public void Calculate(out Vector3 diffPos, out Vector3 diffEuler, Transform tr) {
        if (m_AnimStat == null) {
            diffPos = Vector3.zero;
            diffEuler = Vector3.zero;
            return;
        }
        diffEuler = m_DiffEulerAngle;
        diffPos = Matrix4x4.TRS(Vector3.zero, tr.rotation, Vector3.one).MultiplyPoint(m_DiffPosition);
    }

	public float GetAnimLength() {
        return m_AnimStat != null ? m_AnimStat.length : 0f;
    }

	public void UpdateAnim(string postureAnimName, WrapMode wrapMode) {
        m_AnimStat = m_ThisAnimation[postureAnimName];
        if (m_AnimStat != null) {
            m_ThisAnimation.Play(postureAnimName);
            m_AnimStat.wrapMode = wrapMode;
            m_NewPosition = INITIAL_POS;
            m_OldPosition = INITIAL_POS;
            m_DiffPosition = Vector3.zero;
            m_NewEulerAngle = INITIAL_EULER;
            m_OldEulerAngle = INITIAL_EULER;
            m_DiffEulerAngle = Vector3.zero;
        }
    }

	public void ResetFrame() {
        if (m_AnimStat != null) {
            m_AnimStat.time = 0f;
            m_NewPosition = INITIAL_POS;
            m_OldPosition = INITIAL_POS;
            m_DiffPosition = Vector3.zero;
            m_NewEulerAngle = INITIAL_EULER;
            m_OldEulerAngle = INITIAL_EULER;
            m_DiffEulerAngle = Vector3.zero;
        }
    }
}
