﻿using Meige;
using System;
using UnityEngine;

public class EffectComponentBase : MonoBehaviour {
    protected const int DEFAULT_LOCATOR_NUM = 1;

    [SerializeField] protected bool m_isActive;

    public EffectLocator[] m_Locators;
    protected bool m_isAlive;
    protected Transform[] m_LocatorTransforms;

    public virtual bool isAlive {
        get => m_isAlive;
        protected set => m_isAlive = value;
    }

    public virtual bool isActive {
        get => m_isActive;
        set => m_isActive = value;
    }

    private void OnApplicationQuit() { }

    public virtual int GetPropertyNum() {
        return 0;
    }

    public virtual int GetArrayNum(int propertyIdx) {
        return 0;
    }

    public virtual object GetValue(int propertyIdx, int arrayIdx) {
        return null;
    }

    public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value) { }

    public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value) { }

    public virtual eEffectAnimTypeCode GetTypeCode(int propertyIdx) {
        return eEffectAnimTypeCode.Invalid;
    }

    public virtual Type GetPropertType(int propertyIdx) {
        return typeof(object);
    }

    public virtual string GetPropertyName(int propertyIdx) {
        return null;
    }

    public virtual EffectRuleBase GetEffectRule() {
        return null;
    }

    public EffectComponentBase Clone() {
        return (EffectComponentBase)MemberwiseClone();
    }

    public virtual int GetLocatorNum() {
        return DEFAULT_LOCATOR_NUM;
    }

    public void PrepareLocator() {
        if (m_Locators == null) {
            m_Locators = new EffectLocator[GetLocatorNum()];
        } else if (m_Locators.Length < GetLocatorNum()) {
            EffectLocator[] array = new EffectLocator[GetLocatorNum()];
            for (int i = 0; i < m_Locators.Length; i++) {
                array[i] = m_Locators[i];
            }
            m_Locators = array;
        }
    }

    public void SetLocator(int idx, EffectLocator locator) {
        m_Locators[idx] = locator;
    }

    public EffectLocator GetLocator(int idx) {
        return m_Locators[idx];
    }

    public void SetLocatorsTransform(int idx, Transform trans) {
        m_LocatorTransforms[idx] = trans;
    }

    public Transform GetLocatorsTransform(int idx) {
        return m_LocatorTransforms[idx];
    }

    protected void InitLocator() {
        PrepareLocator();
        m_LocatorTransforms = new Transform[GetLocatorNum()];
        if (GetLocatorNum() > DEFAULT_LOCATOR_NUM) {
            for (int i = 0; i < GetLocatorNum(); i++) {
                if (m_Locators[i] == null) {
                    GameObject gameObject = new GameObject("LocatorObject[" + i.ToString() + "]");
                    m_Locators[i] = gameObject.AddComponent<EffectLocator>();
                    m_Locators[i].Reset();
                    m_Locators[i].SetOwner(gameObject);
                    m_LocatorTransforms[i] = m_Locators[i].transform;
                    m_LocatorTransforms[i].parent = transform;
                } else {
                    m_LocatorTransforms[i] = m_Locators[i].transform;
                }
            }
        } else {
            m_LocatorTransforms[0] = transform;
        }
    }

    protected void ReleaseLocator() {
        for (int i = 0; i < GetLocatorNum(); i++) {
            if (m_Locators[i] != null) {
                GameObject owner = m_Locators[i].GetOwner();
                if (owner != null) {
                    m_Locators[i].SetOwner(null);
                    Destroy(owner);
                }
            }
        }
    }

    protected virtual void Init() {
        InitLocator();
    }

    protected virtual void Release() {
        ReleaseLocator();
    }

    public virtual bool Shot() {
        return false;
    }

    public virtual void Activate(bool flg) {
        isActive = flg;
    }

    public virtual bool Kill() {
        return false;
    }

    private void Awake() {
        Init();
    }

    private void OnDestroy() {
        Release();
    }
}
