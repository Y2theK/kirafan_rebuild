﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000BA RID: 186
[Token(Token = "0x2000091")]
[CreateAssetMenu]
[StructLayout(3)]
public class EffectInitializer : ScriptableObject
{
	// Token: 0x0600043F RID: 1087 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F5")]
	[Address(RVA = "0x1010EE784", Offset = "0x10EE784", VA = "0x1010EE784")]
	private void Awake()
	{
	}

	// Token: 0x06000440 RID: 1088 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F6")]
	[Address(RVA = "0x1010EE788", Offset = "0x10EE788", VA = "0x1010EE788")]
	private void OnEnable()
	{
	}

	// Token: 0x06000441 RID: 1089 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F7")]
	[Address(RVA = "0x1010EE78C", Offset = "0x10EE78C", VA = "0x1010EE78C")]
	public EffectInitializer()
	{
	}
}
