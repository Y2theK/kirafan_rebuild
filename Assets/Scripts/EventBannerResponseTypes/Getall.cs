﻿using CommonResponseTypes;
using WWWTypes;

namespace EventBannerResponseTypes {
    public class Getall : CommonResponse {
        public EventBanner[] banners;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (banners == null) ? string.Empty : banners.ToString();
            return str;
        }
    }
}
