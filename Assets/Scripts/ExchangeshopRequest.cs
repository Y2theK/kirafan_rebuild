﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ExchangeshopRequestTypes;
using Meige;

// Token: 0x02000066 RID: 102
[Token(Token = "0x200003F")]
[StructLayout(3)]
public static class ExchangeshopRequest
{
	// Token: 0x06000219 RID: 537 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D2")]
	[Address(RVA = "0x1010EEA10", Offset = "0x10EEA10", VA = "0x1010EEA10")]
	public static MeigewwwParam Buy(Buy param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600021A RID: 538 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D3")]
	[Address(RVA = "0x1010EEB80", Offset = "0x10EEB80", VA = "0x1010EEB80")]
	public static MeigewwwParam Buy(long exchangeShopId, int buyAmount, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600021B RID: 539 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D4")]
	[Address(RVA = "0x1010EECE0", Offset = "0x10EECE0", VA = "0x1010EECE0")]
	public static MeigewwwParam Shown(Shown param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600021C RID: 540 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D5")]
	[Address(RVA = "0x1010EEDE4", Offset = "0x10EEDE4", VA = "0x1010EEDE4")]
	public static MeigewwwParam Shown(long[] exchangeShopIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
