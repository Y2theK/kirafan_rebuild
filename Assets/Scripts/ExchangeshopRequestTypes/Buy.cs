﻿namespace ExchangeshopRequestTypes {
    public class Buy {
        public long exchangeShopId;
        public int buyAmount;
    }
}
