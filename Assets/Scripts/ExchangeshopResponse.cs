﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ExchangeshopResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x0200008A RID: 138
[Token(Token = "0x2000063")]
[StructLayout(3)]
public static class ExchangeshopResponse
{
	// Token: 0x0600035D RID: 861 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000316")]
	[Address(RVA = "0x1010EEF04", Offset = "0x10EEF04", VA = "0x1010EEF04")]
	public static Buy Buy(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600035E RID: 862 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000317")]
	[Address(RVA = "0x1010EEF6C", Offset = "0x10EEF6C", VA = "0x1010EEF6C")]
	public static Shown Shown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
