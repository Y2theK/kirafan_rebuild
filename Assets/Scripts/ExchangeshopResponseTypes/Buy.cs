﻿using CommonResponseTypes;
using WWWTypes;

namespace ExchangeshopResponseTypes {
    public class Buy : CommonResponse {
        public ExchangeShop[] exchangeShops;
        public PlayerItemSummary[] itemSummary;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (exchangeShops == null) ? string.Empty : exchangeShops.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
