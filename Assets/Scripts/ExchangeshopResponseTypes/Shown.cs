﻿using CommonResponseTypes;
using WWWTypes;

namespace ExchangeshopResponseTypes {
    public class Shown : CommonResponse {
        public ExchangeShop[] exchangeShops;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (exchangeShops == null) ? string.Empty : exchangeShops.ToString();
            return str;
        }
    }
}
