﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;
using UnityEngine.Rendering;

// Token: 0x02000027 RID: 39
[Token(Token = "0x200001F")]
[StructLayout(3)]
public class FakeReflectionCtrl : MonoBehaviour
{
	// Token: 0x17000021 RID: 33
	// (get) Token: 0x06000098 RID: 152 RVA: 0x000022C8 File Offset: 0x000004C8
	[Token(Token = "0x17000017")]
	public bool isAvailable
	{
		[Token(Token = "0x6000088")]
		[Address(RVA = "0x1010EF14C", Offset = "0x10EF14C", VA = "0x1010EF14C")]
		get
		{
			return default(bool);
		}
	}

	// Token: 0x06000099 RID: 153 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000089")]
	[Address(RVA = "0x1010EF1BC", Offset = "0x10EF1BC", VA = "0x1010EF1BC")]
	public Material GetMaterial()
	{
		return null;
	}

	// Token: 0x0600009A RID: 154 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600008A")]
	[Address(RVA = "0x1010EF1EC", Offset = "0x10EF1EC", VA = "0x1010EF1EC")]
	private void CreateMateria()
	{
	}

	// Token: 0x0600009B RID: 155 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600008B")]
	[Address(RVA = "0x1010EF2A4", Offset = "0x10EF2A4", VA = "0x1010EF2A4")]
	private void Awake()
	{
	}

	// Token: 0x0600009C RID: 156 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600008C")]
	[Address(RVA = "0x1010EF2C8", Offset = "0x10EF2C8", VA = "0x1010EF2C8")]
	private void RegisterShaderParam()
	{
	}

	// Token: 0x0600009D RID: 157 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600008D")]
	[Address(RVA = "0x1010EF79C", Offset = "0x10EF79C", VA = "0x1010EF79C")]
	private void ClearShaderParam()
	{
	}

	// Token: 0x0600009E RID: 158 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600008E")]
	[Address(RVA = "0x1010EF7FC", Offset = "0x10EF7FC", VA = "0x1010EF7FC")]
	private void Reflesh()
	{
	}

	// Token: 0x0600009F RID: 159 RVA: 0x000022E0 File Offset: 0x000004E0
	[Token(Token = "0x600008F")]
	[Address(RVA = "0x1010EFB88", Offset = "0x10EFB88", VA = "0x1010EFB88")]
	private float CalcFadeAlpha(float rate)
	{
		return 0f;
	}

	// Token: 0x060000A0 RID: 160 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000090")]
	[Address(RVA = "0x1010EFBC0", Offset = "0x10EFBC0", VA = "0x1010EFBC0")]
	private void UpdateTime()
	{
	}

	// Token: 0x060000A1 RID: 161 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000091")]
	[Address(RVA = "0x1010EFFFC", Offset = "0x10EFFFC", VA = "0x1010EFFFC")]
	private void Update()
	{
	}

	// Token: 0x060000A2 RID: 162 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000092")]
	[Address(RVA = "0x1010F0038", Offset = "0x10F0038", VA = "0x1010F0038")]
	public FakeReflectionCtrl()
	{
	}

	// Token: 0x040000E0 RID: 224
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000096")]
	[SerializeField]
	private Material m_Material;

	// Token: 0x040000E1 RID: 225
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000097")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Texture m_MaskTexture;

	// Token: 0x040000E2 RID: 226
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000098")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Texture m_MaskTexture2;

	// Token: 0x040000E3 RID: 227
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x4000099")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_BlendMode m_BlendSrc;

	// Token: 0x040000E4 RID: 228
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x400009A")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_BlendMode m_BlendDst;

	// Token: 0x040000E5 RID: 229
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x400009B")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_BlendOp m_BlendOpColor;

	// Token: 0x040000E6 RID: 230
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x400009C")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_BlendOp m_BlendOpAlpha;

	// Token: 0x040000E7 RID: 231
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x400009D")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_HDRFactor;

	// Token: 0x040000E8 RID: 232
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x400009E")]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_RadialRotRate;

	// Token: 0x040000E9 RID: 233
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x400009F")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_RadialRotWidthRate;

	// Token: 0x040000EA RID: 234
	[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
	[Token(Token = "0x40000A0")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_RadialRotDegreeOffset;

	// Token: 0x040000EB RID: 235
	[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
	[Token(Token = "0x40000A1")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_RadialRotLightIntensity;

	// Token: 0x040000EC RID: 236
	[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
	[Token(Token = "0x40000A2")]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_RadialRotLoopNum;

	// Token: 0x040000ED RID: 237
	[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
	[Token(Token = "0x40000A3")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Int m_RadialRotDivideNum;

	// Token: 0x040000EE RID: 238
	[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
	[Token(Token = "0x40000A4")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Int m_RadialRotNum;

	// Token: 0x040000EF RID: 239
	[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
	[Token(Token = "0x40000A5")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Color m_RadialRotColor;

	// Token: 0x040000F0 RID: 240
	[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
	[Token(Token = "0x40000A6")]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_SlideSlopeRate;

	// Token: 0x040000F1 RID: 241
	[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
	[Token(Token = "0x40000A7")]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_SlideSlopeRot;

	// Token: 0x040000F2 RID: 242
	[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
	[Token(Token = "0x40000A8")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_SlideSlopeWidthRate;

	// Token: 0x040000F3 RID: 243
	[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
	[Token(Token = "0x40000A9")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_SlideSlopeLightIntensity;

	// Token: 0x040000F4 RID: 244
	[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
	[Token(Token = "0x40000AA")]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_SlideSlopeLoopNum;

	// Token: 0x040000F5 RID: 245
	[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
	[Token(Token = "0x40000AB")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Int m_SlideSlopeDivideNum;

	// Token: 0x040000F6 RID: 246
	[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
	[Token(Token = "0x40000AC")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Color m_SlideSlopeColor;

	// Token: 0x040000F7 RID: 247
	[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
	[Token(Token = "0x40000AD")]
	private FakeReflectionCtrl.SerializeShaderParam_Vector m_RadialRotWorkParam;

	// Token: 0x040000F8 RID: 248
	[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
	[Token(Token = "0x40000AE")]
	private FakeReflectionCtrl.SerializeShaderParam_Vector m_SlideSlopeWorkParam;

	// Token: 0x040000F9 RID: 249
	[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
	[Token(Token = "0x40000AF")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_Roughness;

	// Token: 0x040000FA RID: 250
	[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
	[Token(Token = "0x40000B0")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Float m_FresnelReflectance;

	// Token: 0x040000FB RID: 251
	[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
	[Token(Token = "0x40000B1")]
	[SerializeField]
	private FakeReflectionCtrl.SerializeShaderParam_Vector m_FakeEyeDir;

	// Token: 0x040000FC RID: 252
	[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
	[Token(Token = "0x40000B2")]
	private FakeReflectionCtrl.SerializeShaderParam_Color m_MeshColor;

	// Token: 0x040000FD RID: 253
	[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
	[Token(Token = "0x40000B3")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingRadialRotReflection;

	// Token: 0x040000FE RID: 254
	[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
	[Token(Token = "0x40000B4")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingRadialRotTexColor;

	// Token: 0x040000FF RID: 255
	[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
	[Token(Token = "0x40000B5")]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingRadialDivide;

	// Token: 0x04000100 RID: 256
	[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
	[Token(Token = "0x40000B6")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingSlideSlopeReflection;

	// Token: 0x04000101 RID: 257
	[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
	[Token(Token = "0x40000B7")]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingSlideSlopeDivide;

	// Token: 0x04000102 RID: 258
	[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
	[Token(Token = "0x40000B8")]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingSlideSlopeTexture;

	// Token: 0x04000103 RID: 259
	[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
	[Token(Token = "0x40000B9")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingCookTorrance;

	// Token: 0x04000104 RID: 260
	[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
	[Token(Token = "0x40000BA")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingCookTorranceTexAlpha;

	// Token: 0x04000105 RID: 261
	[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
	[Token(Token = "0x40000BB")]
	[SerializeField]
	private FakeReflectionCtrl.ShaderVariantParam_Bool m_UsingFadeInOut;

	// Token: 0x04000106 RID: 262
	[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
	[Token(Token = "0x40000BC")]
	[SerializeField]
	private float m_CycleSec;

	// Token: 0x04000107 RID: 263
	[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
	[Token(Token = "0x40000BD")]
	[SerializeField]
	private float m_IntervalSec;

	// Token: 0x04000108 RID: 264
	[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
	[Token(Token = "0x40000BE")]
	private float m_Sec;

	// Token: 0x04000109 RID: 265
	[Cpp2IlInjected.FieldOffset(Offset = "0x154")]
	[Token(Token = "0x40000BF")]
	private bool m_isInterval;

	// Token: 0x0400010A RID: 266
	[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
	[Token(Token = "0x40000C0")]
	[SerializeField]
	[Attribute(Name = "RangeAttribute", RVA = "0x10011E210", Offset = "0x11E210")]
	public float m_RadialRotStartRate;

	// Token: 0x0400010B RID: 267
	[Cpp2IlInjected.FieldOffset(Offset = "0x15C")]
	[Token(Token = "0x40000C1")]
	[SerializeField]
	[Attribute(Name = "RangeAttribute", RVA = "0x10011E250", Offset = "0x11E250")]
	public float m_RadialRotEndRate;

	// Token: 0x0400010C RID: 268
	[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
	[Token(Token = "0x40000C2")]
	[SerializeField]
	[Attribute(Name = "RangeAttribute", RVA = "0x10011E290", Offset = "0x11E290")]
	public float m_SlideSlopeStartRate;

	// Token: 0x0400010D RID: 269
	[Cpp2IlInjected.FieldOffset(Offset = "0x164")]
	[Token(Token = "0x40000C3")]
	[SerializeField]
	[Attribute(Name = "RangeAttribute", RVA = "0x10011E2D0", Offset = "0x11E2D0")]
	public float m_SlideSlopeEndRate;

	// Token: 0x0400010E RID: 270
	[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
	[Token(Token = "0x40000C4")]
	[SerializeField]
	public RangeValueColor m_ColorAnimation;

	// Token: 0x0400010F RID: 271
	[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
	[Token(Token = "0x40000C5")]
	private List<FakeReflectionCtrl.SerializeShaderParamBase> m_ShaderParamList;

	// Token: 0x02000028 RID: 40
	[Token(Token = "0x2000CE3")]
	[Serializable]
	public abstract class SerializeShaderParamBase
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x000022F8 File Offset: 0x000004F8
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000680")]
		public bool isDirty
		{
			[Token(Token = "0x6005CDE")]
			[Address(RVA = "0x1010EFB80", Offset = "0x10EFB80", VA = "0x1010EFB80")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005CDD")]
			[Address(RVA = "0x1010EFFF4", Offset = "0x10EFFF4", VA = "0x1010EFFF4")]
			set
			{
			}
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CDF")]
		[Address(RVA = "0x1010F0AF8", Offset = "0x10F0AF8", VA = "0x1010F0AF8")]
		public SerializeShaderParamBase(string argName)
		{
		}

		// Token: 0x060000A6 RID: 166
		[Token(Token = "0x6005CE0")]
		[Address(Slot = "4")]
		public abstract void Send(Material material);

		// Token: 0x060000A7 RID: 167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE1")]
		[Address(RVA = "0x1010F0B2C", Offset = "0x10F0B2C", VA = "0x1010F0B2C")]
		public void OnValidate()
		{
		}

		// Token: 0x04000110 RID: 272
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40053E4")]
		protected string m_ArgName;

		// Token: 0x04000111 RID: 273
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40053E5")]
		private bool m_isDirty;
	}

	// Token: 0x02000029 RID: 41
	[Token(Token = "0x2000CE4")]
	[Serializable]
	public class SerializeShaderParam<T> : FakeReflectionCtrl.SerializeShaderParamBase
	{
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060000A8 RID: 168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000681")]
		public T value
		{
			[Token(Token = "0x6005CE3")]
			[Address(RVA = "0x1016AE484", Offset = "0x16AE484", VA = "0x1016AE484")]
			get
			{
				return null;
			}
			[Token(Token = "0x6005CE2")]
			[Address(RVA = "0x1016AE474", Offset = "0x16AE474", VA = "0x1016AE474")]
			set
			{
			}
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE4")]
		[Address(RVA = "0x1016AE48C", Offset = "0x16AE48C", VA = "0x1016AE48C")]
		public SerializeShaderParam(string argName)
		{
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE5")]
		[Address(RVA = "0x1016AE4C0", Offset = "0x16AE4C0", VA = "0x1016AE4C0")]
		public SerializeShaderParam(string argName, T defValue)
		{
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE6")]
		[Address(RVA = "0x1016AE508", Offset = "0x16AE508", VA = "0x1016AE508", Slot = "4")]
		public override void Send(Material material)
		{
		}

		// Token: 0x04000112 RID: 274
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40053E6")]
		[SerializeField]
		private T m_Value;
	}

	// Token: 0x0200002A RID: 42
	[Token(Token = "0x2000CE5")]
	[Serializable]
	public class SerializeShaderParam_Int : FakeReflectionCtrl.SerializeShaderParam<int>
	{
		// Token: 0x060000AD RID: 173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE7")]
		[Address(RVA = "0x1010F11F0", Offset = "0x10F11F0", VA = "0x1010F11F0")]
		public SerializeShaderParam_Int(string argName)
		{
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE8")]
		[Address(RVA = "0x1010F0908", Offset = "0x10F0908", VA = "0x1010F0908")]
		public SerializeShaderParam_Int(string argName, int defValue)
		{
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CE9")]
		[Address(RVA = "0x1010F1250", Offset = "0x10F1250", VA = "0x1010F1250", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x0200002B RID: 43
	[Token(Token = "0x2000CE6")]
	[Serializable]
	public class SerializeShaderParam_Float : FakeReflectionCtrl.SerializeShaderParam<float>
	{
		// Token: 0x060000B0 RID: 176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CEA")]
		[Address(RVA = "0x1010F0F98", Offset = "0x10F0F98", VA = "0x1010F0F98")]
		public SerializeShaderParam_Float(string argName)
		{
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CEB")]
		[Address(RVA = "0x1010F0898", Offset = "0x10F0898", VA = "0x1010F0898")]
		public SerializeShaderParam_Float(string argName, float defValue)
		{
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CEC")]
		[Address(RVA = "0x1010F0FF8", Offset = "0x10F0FF8", VA = "0x1010F0FF8", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x0200002C RID: 44
	[Token(Token = "0x2000CE7")]
	[Serializable]
	public class SerializeShaderParam_FloatArray : FakeReflectionCtrl.SerializeShaderParam<float[]>
	{
		// Token: 0x060000B3 RID: 179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CED")]
		[Address(RVA = "0x1010F1094", Offset = "0x10F1094", VA = "0x1010F1094")]
		public SerializeShaderParam_FloatArray(string argName)
		{
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CEE")]
		[Address(RVA = "0x1010F10F4", Offset = "0x10F10F4", VA = "0x1010F10F4")]
		public SerializeShaderParam_FloatArray(string argName, float[] defValue)
		{
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CEF")]
		[Address(RVA = "0x1010F115C", Offset = "0x10F115C", VA = "0x1010F115C", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x0200002D RID: 45
	[Token(Token = "0x2000CE8")]
	[Serializable]
	public class SerializeShaderParam_BlendMode : FakeReflectionCtrl.SerializeShaderParam<BlendMode>
	{
		// Token: 0x060000B6 RID: 182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF0")]
		[Address(RVA = "0x1010F0B38", Offset = "0x10F0B38", VA = "0x1010F0B38")]
		public SerializeShaderParam_BlendMode(string argName)
		{
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF1")]
		[Address(RVA = "0x1010F07C8", Offset = "0x10F07C8", VA = "0x1010F07C8")]
		public SerializeShaderParam_BlendMode(string argName, BlendMode defValue)
		{
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF2")]
		[Address(RVA = "0x1010F0B98", Offset = "0x10F0B98", VA = "0x1010F0B98", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x0200002E RID: 46
	[Token(Token = "0x2000CE9")]
	[Serializable]
	public class SerializeShaderParam_BlendOp : FakeReflectionCtrl.SerializeShaderParam<BlendOp>
	{
		// Token: 0x060000B9 RID: 185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF3")]
		[Address(RVA = "0x1010F0C2C", Offset = "0x10F0C2C", VA = "0x1010F0C2C")]
		public SerializeShaderParam_BlendOp(string argName)
		{
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF4")]
		[Address(RVA = "0x1010F0830", Offset = "0x10F0830", VA = "0x1010F0830")]
		public SerializeShaderParam_BlendOp(string argName, BlendOp defValue)
		{
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF5")]
		[Address(RVA = "0x1010F0C8C", Offset = "0x10F0C8C", VA = "0x1010F0C8C", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x0200002F RID: 47
	[Token(Token = "0x2000CEA")]
	[Serializable]
	public class SerializeShaderParam_Vector : FakeReflectionCtrl.SerializeShaderParam<Vector4>
	{
		// Token: 0x060000BC RID: 188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF6")]
		[Address(RVA = "0x1010F16CC", Offset = "0x10F16CC", VA = "0x1010F16CC")]
		public SerializeShaderParam_Vector(string argName)
		{
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF7")]
		[Address(RVA = "0x1010F0A00", Offset = "0x10F0A00", VA = "0x1010F0A00")]
		public SerializeShaderParam_Vector(string argName, Vector4 defValue)
		{
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF8")]
		[Address(RVA = "0x1010F172C", Offset = "0x10F172C", VA = "0x1010F172C", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000030 RID: 48
	[Token(Token = "0x2000CEB")]
	[Serializable]
	public class SerializeShaderParam_VectorArray : FakeReflectionCtrl.SerializeShaderParam<Vector4[]>
	{
		// Token: 0x060000BF RID: 191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CF9")]
		[Address(RVA = "0x1010F17E8", Offset = "0x10F17E8", VA = "0x1010F17E8")]
		public SerializeShaderParam_VectorArray(string argName)
		{
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFA")]
		[Address(RVA = "0x1010F1848", Offset = "0x10F1848", VA = "0x1010F1848")]
		public SerializeShaderParam_VectorArray(string argName, Vector4[] defValue)
		{
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFB")]
		[Address(RVA = "0x1010F18B0", Offset = "0x10F18B0", VA = "0x1010F18B0", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000031 RID: 49
	[Token(Token = "0x2000CEC")]
	[Serializable]
	public class SerializeShaderParam_Matrix : FakeReflectionCtrl.SerializeShaderParam<Matrix4x4>
	{
		// Token: 0x060000C2 RID: 194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFC")]
		[Address(RVA = "0x1010F12E4", Offset = "0x10F12E4", VA = "0x1010F12E4")]
		public SerializeShaderParam_Matrix(string argName)
		{
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFD")]
		[Address(RVA = "0x1010F1344", Offset = "0x10F1344", VA = "0x1010F1344")]
		public SerializeShaderParam_Matrix(string argName, Matrix4x4 defValue)
		{
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFE")]
		[Address(RVA = "0x1010F13D0", Offset = "0x10F13D0", VA = "0x1010F13D0", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000032 RID: 50
	[Token(Token = "0x2000CED")]
	[Serializable]
	public class SerializeShaderParam_MatrixArray : FakeReflectionCtrl.SerializeShaderParam<Matrix4x4[]>
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005CFF")]
		[Address(RVA = "0x1010F147C", Offset = "0x10F147C", VA = "0x1010F147C")]
		public SerializeShaderParam_MatrixArray(string argName)
		{
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D00")]
		[Address(RVA = "0x1010F14DC", Offset = "0x10F14DC", VA = "0x1010F14DC")]
		public SerializeShaderParam_MatrixArray(string argName, Matrix4x4[] defValue)
		{
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D01")]
		[Address(RVA = "0x1010F1544", Offset = "0x10F1544", VA = "0x1010F1544", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000033 RID: 51
	[Token(Token = "0x2000CEE")]
	[Serializable]
	public class SerializeShaderParam_Color : FakeReflectionCtrl.SerializeShaderParam<Color>
	{
		// Token: 0x060000C8 RID: 200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D02")]
		[Address(RVA = "0x1010F0D20", Offset = "0x10F0D20", VA = "0x1010F0D20")]
		public SerializeShaderParam_Color(string argName)
		{
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D03")]
		[Address(RVA = "0x1010F0970", Offset = "0x10F0970", VA = "0x1010F0970")]
		public SerializeShaderParam_Color(string argName, Color defValue)
		{
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D04")]
		[Address(RVA = "0x1010F0D80", Offset = "0x10F0D80", VA = "0x1010F0D80", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000034 RID: 52
	[Token(Token = "0x2000CEF")]
	[Serializable]
	public class SerializeShaderParam_ColorArray : FakeReflectionCtrl.SerializeShaderParam<Color[]>
	{
		// Token: 0x060000CB RID: 203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D05")]
		[Address(RVA = "0x1010F0E3C", Offset = "0x10F0E3C", VA = "0x1010F0E3C")]
		public SerializeShaderParam_ColorArray(string argName)
		{
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D06")]
		[Address(RVA = "0x1010F0E9C", Offset = "0x10F0E9C", VA = "0x1010F0E9C")]
		public SerializeShaderParam_ColorArray(string argName, Color[] defValue)
		{
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D07")]
		[Address(RVA = "0x1010F0F04", Offset = "0x10F0F04", VA = "0x1010F0F04", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000035 RID: 53
	[Token(Token = "0x2000CF0")]
	[Serializable]
	public class SerializeShaderParam_Texture : FakeReflectionCtrl.SerializeShaderParam<Texture>
	{
		// Token: 0x060000CE RID: 206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D08")]
		[Address(RVA = "0x1010F15D8", Offset = "0x10F15D8", VA = "0x1010F15D8")]
		public SerializeShaderParam_Texture(string argName)
		{
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D09")]
		[Address(RVA = "0x1010F0760", Offset = "0x10F0760", VA = "0x1010F0760")]
		public SerializeShaderParam_Texture(string argName, Texture defValue)
		{
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0A")]
		[Address(RVA = "0x1010F1638", Offset = "0x10F1638", VA = "0x1010F1638", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}

	// Token: 0x02000036 RID: 54
	[Token(Token = "0x2000CF1")]
	[Serializable]
	public class ShaderVariantParam_Bool : FakeReflectionCtrl.SerializeShaderParam<bool>
	{
		// Token: 0x060000D1 RID: 209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0B")]
		[Address(RVA = "0x1010F1944", Offset = "0x10F1944", VA = "0x1010F1944")]
		public ShaderVariantParam_Bool(string argName)
		{
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0C")]
		[Address(RVA = "0x1010F0A90", Offset = "0x10F0A90", VA = "0x1010F0A90")]
		public ShaderVariantParam_Bool(string argName, bool defValue)
		{
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0D")]
		[Address(RVA = "0x1010F19A4", Offset = "0x10F19A4", VA = "0x1010F19A4", Slot = "4")]
		public override void Send(Material material)
		{
		}
	}
}
