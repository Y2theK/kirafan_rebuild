﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000037 RID: 55
[Token(Token = "0x2000020")]
[StructLayout(3)]
public class FakeReflectionCtrlBank : MonoBehaviour
{
	// Token: 0x060000D4 RID: 212 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000093")]
	[Address(RVA = "0x1010F1A54", Offset = "0x10F1A54", VA = "0x1010F1A54")]
	public FakeReflectionCtrl GetFakeReflectionCtrl(FakeReflectionCtrlBank.eType type)
	{
		return null;
	}

	// Token: 0x060000D5 RID: 213 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000094")]
	[Address(RVA = "0x1010F1AA4", Offset = "0x10F1AA4", VA = "0x1010F1AA4")]
	private void Awake()
	{
	}

	// Token: 0x060000D6 RID: 214 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000095")]
	[Address(RVA = "0x1010F1FC4", Offset = "0x10F1FC4", VA = "0x1010F1FC4")]
	public FakeReflectionCtrlBank()
	{
	}

	// Token: 0x04000113 RID: 275
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40000C6")]
	[SerializeField]
	private FakeReflectionCtrl m_Prefab_ForIcon;

	// Token: 0x04000114 RID: 276
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x40000C7")]
	[SerializeField]
	private FakeReflectionCtrl m_Prefab_ForEdit;

	// Token: 0x04000115 RID: 277
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x40000C8")]
	[SerializeField]
	private FakeReflectionCtrl m_Prefab_ForCard;

	// Token: 0x04000116 RID: 278
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x40000C9")]
	[SerializeField]
	private FakeReflectionCtrl m_Prefab_ForSupport;

	// Token: 0x04000117 RID: 279
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x40000CA")]
	[SerializeField]
	private FakeReflectionCtrl m_Prefab_ForItemIcon;

	// Token: 0x04000118 RID: 280
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x40000CB")]
	private FakeReflectionCtrl[] m_FakeReflectionCtrl;

	// Token: 0x02000038 RID: 56
	[Token(Token = "0x2000CF2")]
	public enum eType
	{
		// Token: 0x0400011A RID: 282
		[Token(Token = "0x40053E8")]
		Icon,
		// Token: 0x0400011B RID: 283
		[Token(Token = "0x40053E9")]
		Edit,
		// Token: 0x0400011C RID: 284
		[Token(Token = "0x40053EA")]
		Card,
		// Token: 0x0400011D RID: 285
		[Token(Token = "0x40053EB")]
		Support,
		// Token: 0x0400011E RID: 286
		[Token(Token = "0x40053EC")]
		ItemIcon,
		// Token: 0x0400011F RID: 287
		[Token(Token = "0x40053ED")]
		Max
	}
}
