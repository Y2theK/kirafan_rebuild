﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using FavoriteMemberRequestTypes;
using Meige;

// Token: 0x02000067 RID: 103
[Token(Token = "0x2000040")]
[StructLayout(3)]
public static class FavoriteMemberRequest
{
	// Token: 0x0600021D RID: 541 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D6")]
	[Address(RVA = "0x1010F1FCC", Offset = "0x10F1FCC", VA = "0x1010F1FCC")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600021E RID: 542 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D7")]
	[Address(RVA = "0x1010F2090", Offset = "0x10F2090", VA = "0x1010F2090")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
