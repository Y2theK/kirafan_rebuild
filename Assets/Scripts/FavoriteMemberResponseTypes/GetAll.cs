﻿using CommonResponseTypes;
using WWWTypes;

namespace FavoriteMemberResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerFavoriteMember[] managedFavoriteMembers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedFavoriteMembers == null) ? string.Empty : managedFavoriteMembers.ToString();
            return str;
        }
    }
}
