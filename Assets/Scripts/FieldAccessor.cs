﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000B7 RID: 183
[Token(Token = "0x200008E")]
[StructLayout(3)]
internal sealed class FieldAccessor<TTarget, TField> : IFieldAccessor
{
	// Token: 0x06000439 RID: 1081 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003EF")]
	[Address(RVA = "0x1016AECF0", Offset = "0x16AECF0", VA = "0x1016AECF0")]
	public FieldAccessor(Func<TTarget, TField> getter, Action<TTarget, TField> setter)
	{
	}

	// Token: 0x0600043A RID: 1082 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003F0")]
	[Address(RVA = "0x1016AED34", Offset = "0x16AED34", VA = "0x1016AED34", Slot = "4")]
	public object GetValue(object target)
	{
		return null;
	}

	// Token: 0x0600043B RID: 1083 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F1")]
	[Address(RVA = "0x1016AEE30", Offset = "0x16AEE30", VA = "0x1016AEE30", Slot = "5")]
	public void SetValue(object target, object value)
	{
	}

	// Token: 0x04000253 RID: 595
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x4000199")]
	private readonly Func<TTarget, TField> getter;

	// Token: 0x04000254 RID: 596
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x400019A")]
	private readonly Action<TTarget, TField> setter;
}
