﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using FieldPartyMemberRequestTypes;
using Meige;
using WWWTypes;

// Token: 0x02000068 RID: 104
[Token(Token = "0x2000041")]
[StructLayout(3)]
public static class FieldPartyMemberRequest
{
	// Token: 0x0600021F RID: 543 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D8")]
	[Address(RVA = "0x1010F2294", Offset = "0x10F2294", VA = "0x1010F2294")]
	public static MeigewwwParam Add(Add param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000220 RID: 544 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001D9")]
	[Address(RVA = "0x1010F2398", Offset = "0x10F2398", VA = "0x1010F2398")]
	public static MeigewwwParam Add(PlayerFieldPartyMember fieldPartyMember, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000221 RID: 545 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DA")]
	[Address(RVA = "0x1010F24A8", Offset = "0x10F24A8", VA = "0x1010F24A8")]
	public static MeigewwwParam AddAll(AddAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000222 RID: 546 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DB")]
	[Address(RVA = "0x1010F25AC", Offset = "0x10F25AC", VA = "0x1010F25AC")]
	public static MeigewwwParam AddAll(PlayerFieldPartyMember[] fieldPartyMembers, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000223 RID: 547 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DC")]
	[Address(RVA = "0x1010F26BC", Offset = "0x10F26BC", VA = "0x1010F26BC")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000224 RID: 548 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DD")]
	[Address(RVA = "0x1010F27E4", Offset = "0x10F27E4", VA = "0x1010F27E4")]
	public static MeigewwwParam Get(long managedPartyMemberId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000225 RID: 549 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DE")]
	[Address(RVA = "0x1010F28FC", Offset = "0x10F28FC", VA = "0x1010F28FC")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000226 RID: 550 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001DF")]
	[Address(RVA = "0x1010F2A24", Offset = "0x10F2A24", VA = "0x1010F2A24")]
	public static MeigewwwParam GetAll(long playerId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000227 RID: 551 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E0")]
	[Address(RVA = "0x1010F2B3C", Offset = "0x10F2B3C", VA = "0x1010F2B3C")]
	public static MeigewwwParam Set(Set param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000228 RID: 552 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E1")]
	[Address(RVA = "0x1010F2C40", Offset = "0x10F2C40", VA = "0x1010F2C40")]
	public static MeigewwwParam Set(PlayerFieldPartyMember fieldPartyMember, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000229 RID: 553 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E2")]
	[Address(RVA = "0x1010F2D50", Offset = "0x10F2D50", VA = "0x1010F2D50")]
	public static MeigewwwParam SetAll(SetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022A RID: 554 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E3")]
	[Address(RVA = "0x1010F2E54", Offset = "0x10F2E54", VA = "0x1010F2E54")]
	public static MeigewwwParam SetAll(PlayerFieldPartyMember[] fieldPartyMembers, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022B RID: 555 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E4")]
	[Address(RVA = "0x1010F2F64", Offset = "0x10F2F64", VA = "0x1010F2F64")]
	public static MeigewwwParam Remove(Remove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022C RID: 556 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E5")]
	[Address(RVA = "0x1010F308C", Offset = "0x10F308C", VA = "0x1010F308C")]
	public static MeigewwwParam Remove(long managedPartyMemberId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022D RID: 557 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E6")]
	[Address(RVA = "0x1010F31A4", Offset = "0x10F31A4", VA = "0x1010F31A4")]
	public static MeigewwwParam RemoveAll(RemoveAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022E RID: 558 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E7")]
	[Address(RVA = "0x1010F32A8", Offset = "0x10F32A8", VA = "0x1010F32A8")]
	public static MeigewwwParam RemoveAll(long[] managedPartyMemberIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600022F RID: 559 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E8")]
	[Address(RVA = "0x1010F33B8", Offset = "0x10F33B8", VA = "0x1010F33B8")]
	public static MeigewwwParam Changeschedule(Changeschedule param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000230 RID: 560 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001E9")]
	[Address(RVA = "0x1010F365C", Offset = "0x10F365C", VA = "0x1010F365C")]
	public static MeigewwwParam Changeschedule(long managedPartyMemberId, int scheduleId, int scheduleTag, long managedFacilityId, int touchItemResultNo, int flag, string scheduleTable, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000231 RID: 561 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001EA")]
	[Address(RVA = "0x1010F3918", Offset = "0x10F3918", VA = "0x1010F3918")]
	public static MeigewwwParam ChangescheduleAll(ChangescheduleAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000232 RID: 562 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001EB")]
	[Address(RVA = "0x1010F3A1C", Offset = "0x10F3A1C", VA = "0x1010F3A1C")]
	public static MeigewwwParam ChangescheduleAll(ChangeScheduleMember[] changeScheduleMembers, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000233 RID: 563 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001EC")]
	[Address(RVA = "0x1010F3B2C", Offset = "0x10F3B2C", VA = "0x1010F3B2C")]
	public static MeigewwwParam Itemup(Itemup param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000234 RID: 564 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001ED")]
	[Address(RVA = "0x1010F3D1C", Offset = "0x10F3D1C", VA = "0x1010F3D1C")]
	public static MeigewwwParam Itemup(long managedPartyMemberId, int touchItemResultNo, int amount, int flag, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
