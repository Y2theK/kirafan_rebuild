﻿using WWWTypes;

namespace FieldPartyMemberRequestTypes {
    public class AddAll {
        public PlayerFieldPartyMember[] fieldPartyMembers;
    }
}
