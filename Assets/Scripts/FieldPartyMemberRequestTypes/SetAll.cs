﻿using WWWTypes;

namespace FieldPartyMemberRequestTypes {
    public class SetAll {
        public PlayerFieldPartyMember[] fieldPartyMembers;
    }
}
