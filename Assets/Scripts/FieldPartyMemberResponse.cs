﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using FieldPartyMemberResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x0200008C RID: 140
[Token(Token = "0x2000065")]
[StructLayout(3)]
public static class FieldPartyMemberResponse
{
	// Token: 0x06000360 RID: 864 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000319")]
	[Address(RVA = "0x1010F3F5C", Offset = "0x10F3F5C", VA = "0x1010F3F5C")]
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000361 RID: 865 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031A")]
	[Address(RVA = "0x1010F3FC4", Offset = "0x10F3FC4", VA = "0x1010F3FC4")]
	public static AddAll AddAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000362 RID: 866 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031B")]
	[Address(RVA = "0x1010F402C", Offset = "0x10F402C", VA = "0x1010F402C")]
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000363 RID: 867 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031C")]
	[Address(RVA = "0x1010F4094", Offset = "0x10F4094", VA = "0x1010F4094")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000364 RID: 868 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031D")]
	[Address(RVA = "0x1010F40FC", Offset = "0x10F40FC", VA = "0x1010F40FC")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000365 RID: 869 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031E")]
	[Address(RVA = "0x1010F4164", Offset = "0x10F4164", VA = "0x1010F4164")]
	public static SetAll SetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000366 RID: 870 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600031F")]
	[Address(RVA = "0x1010F41CC", Offset = "0x10F41CC", VA = "0x1010F41CC")]
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000367 RID: 871 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000320")]
	[Address(RVA = "0x1010F4234", Offset = "0x10F4234", VA = "0x1010F4234")]
	public static RemoveAll RemoveAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000368 RID: 872 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000321")]
	[Address(RVA = "0x1010F429C", Offset = "0x10F429C", VA = "0x1010F429C")]
	public static Changeschedule Changeschedule(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000369 RID: 873 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000322")]
	[Address(RVA = "0x1010F4304", Offset = "0x10F4304", VA = "0x1010F4304")]
	public static ChangescheduleAll ChangescheduleAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600036A RID: 874 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000323")]
	[Address(RVA = "0x1010F436C", Offset = "0x10F436C", VA = "0x1010F436C")]
	public static Itemup Itemup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
