﻿using CommonResponseTypes;

namespace FieldPartyMemberResponseTypes {
    public class Add : CommonResponse {
        public long managedPartyMemberId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += managedPartyMemberId.ToString();
            return str;
        }
    }
}
