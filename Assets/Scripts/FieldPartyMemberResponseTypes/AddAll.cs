﻿using System;
using System.Runtime.InteropServices;
using CommonResponseTypes;
using Cpp2IlInjected;

namespace FieldPartyMemberResponseTypes
{
	public class AddAll : CommonResponse {
        public long[] managedPartyMemberIds;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedPartyMemberIds == null) ? string.Empty : managedPartyMemberIds.ToString();
            return str;
        }
	}
}
