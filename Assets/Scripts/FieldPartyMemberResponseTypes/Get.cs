﻿using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes {
    public class Get : CommonResponse {
        public PlayerFieldPartyMember fieldPartyMember;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (fieldPartyMember == null) ? string.Empty : fieldPartyMember.ToString();
            return str;
        }
    }
}
