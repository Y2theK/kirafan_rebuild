﻿using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerFieldPartyMember[] fieldPartyMembers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (fieldPartyMembers == null) ? string.Empty : fieldPartyMembers.ToString();
            return str;
        }
    }
}
