﻿using CommonResponseTypes;
using WWWTypes;

namespace FieldPartyMemberResponseTypes {
    public class Itemup : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerNamedType[] managedNamedTypes;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            return str;
        }
    }
}
