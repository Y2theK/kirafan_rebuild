﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000039 RID: 57
[Token(Token = "0x2000021")]
[StructLayout(3)]
public class FilterRendererBase : MonoBehaviour
{
	// Token: 0x17000024 RID: 36
	// (get) Token: 0x060000D7 RID: 215 RVA: 0x00002310 File Offset: 0x00000510
	// (set) Token: 0x060000D8 RID: 216 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000018")]
	public bool isAvailable
	{
		[Token(Token = "0x6000096")]
		[Address(RVA = "0x1010F46E0", Offset = "0x10F46E0", VA = "0x1010F46E0")]
		[CompilerGenerated]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x6000097")]
		[Address(RVA = "0x1010F46E8", Offset = "0x10F46E8", VA = "0x1010F46E8")]
		[CompilerGenerated]
		private set
		{
		}
	}

	// Token: 0x17000025 RID: 37
	// (get) Token: 0x060000DA RID: 218 RVA: 0x00002328 File Offset: 0x00000528
	// (set) Token: 0x060000D9 RID: 217 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000019")]
	public bool isEnableRender
	{
		[Token(Token = "0x6000099")]
		[Address(RVA = "0x1010F4728", Offset = "0x10F4728", VA = "0x1010F4728")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x6000098")]
		[Address(RVA = "0x1010F46F0", Offset = "0x10F46F0", VA = "0x1010F46F0")]
		set
		{
		}
	}

	// Token: 0x17000026 RID: 38
	// (get) Token: 0x060000DC RID: 220 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x060000DB RID: 219 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001A")]
	public Material material
	{
		[Token(Token = "0x600009B")]
		[Address(RVA = "0x1010F4794", Offset = "0x10F4794", VA = "0x1010F4794")]
		get
		{
			return null;
		}
		[Token(Token = "0x600009A")]
		[Address(RVA = "0x1010F4758", Offset = "0x10F4758", VA = "0x1010F4758")]
		set
		{
		}
	}

	// Token: 0x17000027 RID: 39
	// (get) Token: 0x060000DE RID: 222 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x060000DD RID: 221 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001B")]
	public Camera targetCamera
	{
		[Token(Token = "0x600009D")]
		[Address(RVA = "0x1010F4AD4", Offset = "0x10F4AD4", VA = "0x1010F4AD4")]
		get
		{
			return null;
		}
		[Token(Token = "0x600009C")]
		[Address(RVA = "0x1010F479C", Offset = "0x10F479C", VA = "0x1010F479C")]
		set
		{
		}
	}

	// Token: 0x060000DF RID: 223 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600009E")]
	[Address(RVA = "0x1010F4ADC", Offset = "0x10F4ADC", VA = "0x1010F4ADC")]
	private void Awake()
	{
	}

	// Token: 0x060000E0 RID: 224 RVA: 0x00002340 File Offset: 0x00000540
	[Token(Token = "0x600009F")]
	[Address(RVA = "0x1010F4AE0", Offset = "0x10F4AE0", VA = "0x1010F4AE0")]
	public bool Setup()
	{
		return default(bool);
	}

	// Token: 0x060000E1 RID: 225 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000A0")]
	[Address(RVA = "0x1010F4BB8", Offset = "0x10F4BB8", VA = "0x1010F4BB8")]
	private void CreateRenderObject()
	{
	}

	// Token: 0x060000E2 RID: 226 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000A1")]
	[Address(RVA = "0x1010F483C", Offset = "0x10F483C", VA = "0x1010F483C")]
	public void SetMeshScale(float scale = 1f)
	{
	}

	// Token: 0x060000E3 RID: 227 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000A2")]
	[Address(RVA = "0x1010F5250", Offset = "0x10F5250", VA = "0x1010F5250")]
	public void SetMeshColor(Color color)
	{
	}

	// Token: 0x060000E4 RID: 228 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000A3")]
	[Address(RVA = "0x1010F5378", Offset = "0x10F5378", VA = "0x1010F5378")]
	private void Update()
	{
	}

	// Token: 0x060000E5 RID: 229 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000A4")]
	[Address(RVA = "0x1010F538C", Offset = "0x10F538C", VA = "0x1010F538C")]
	public FilterRendererBase()
	{
	}

	// Token: 0x04000120 RID: 288
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40000CC")]
	private Renderer m_Renderer;

	// Token: 0x04000121 RID: 289
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x40000CD")]
	private MeshFilter m_MeshFilter;

	// Token: 0x04000122 RID: 290
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x40000CE")]
	private Mesh m_Mesh;

	// Token: 0x04000123 RID: 291
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x40000CF")]
	[SerializeField]
	private Camera m_TargetCamera;

	// Token: 0x04000124 RID: 292
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x40000D0")]
	[SerializeField]
	private Material m_Material;

	// Token: 0x04000125 RID: 293
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x40000D1")]
	private GameObject m_GO;

	// Token: 0x04000126 RID: 294
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x40000D2")]
	private float m_OrthographicSize;

	// Token: 0x04000127 RID: 295
	[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
	[Token(Token = "0x40000D3")]
	private float m_OrthographicSizeScale;
}
