﻿using UnityEngine;

public sealed class Frame {
    private static Frame m_Instance;

    private int m_GameFps;
    private int m_MaxSkip;
    private float m_Fps;
    private float m_DeltaOrg;
    private float m_DeltaSecOrg;
    private float m_Delta;
    private float m_DeltaSec;
    private float m_UpdateDelta;
    private int mNowFrameRate;

    public static Frame Instance => m_Instance;

    public float deltaOrg => m_DeltaOrg;
    public float updateDelta => m_UpdateDelta;
    public float delta => m_Delta;
    public float deltaSecOrg => m_DeltaSecOrg;
    public float deltaSec => m_DeltaSec;
    public float fps => m_Fps;

    public static void Create() {
        if (m_Instance == null) {
            m_Instance = new Frame();
        }
        m_Instance.Init();
    }

	public void Init() {
        m_Fps = 0f;
        m_DeltaOrg = 0f;
        m_GameFps = (Application.targetFrameRate > 0) ? Application.targetFrameRate : 30;
        m_MaxSkip = Mathf.RoundToInt(1f / Time.maximumDeltaTime);
    }

	public int GetFps() {
		return mNowFrameRate;
	}

	public float FrameToSecond(float frame) {
		return frame / m_GameFps;
	}

	public int SecondToFrame(float sec) {
		return (int)(m_GameFps * sec);
	}

	public void Update() {
        m_UpdateDelta = Mathf.Min(Time.deltaTime * m_GameFps, m_MaxSkip);
    }

	public void LateUpdate() {
        m_DeltaOrg = Mathf.Min(Time.deltaTime * m_GameFps, m_MaxSkip);
        m_DeltaSecOrg = FrameToSecond(m_DeltaOrg);
        m_Fps = 1f / m_DeltaSecOrg;
        m_Delta = m_DeltaOrg;
        m_DeltaSec = m_DeltaSecOrg;
    }
}
