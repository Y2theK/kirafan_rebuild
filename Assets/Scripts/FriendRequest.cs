﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using FriendRequestTypes;
using Meige;

// Token: 0x02000069 RID: 105
[Token(Token = "0x2000042")]
[StructLayout(3)]
public static class FriendRequest
{
	// Token: 0x06000235 RID: 565 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001EE")]
	[Address(RVA = "0x1010F573C", Offset = "0x10F573C", VA = "0x1010F573C")]
	public static MeigewwwParam Propose(Propose param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000236 RID: 566 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001EF")]
	[Address(RVA = "0x1010F5864", Offset = "0x10F5864", VA = "0x1010F5864")]
	public static MeigewwwParam Propose(long targetPlayerId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000237 RID: 567 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F0")]
	[Address(RVA = "0x1010F597C", Offset = "0x10F597C", VA = "0x1010F597C")]
	public static MeigewwwParam Accept(Accept param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000238 RID: 568 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F1")]
	[Address(RVA = "0x1010F5AA4", Offset = "0x10F5AA4", VA = "0x1010F5AA4")]
	public static MeigewwwParam Accept(long managedFriendId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000239 RID: 569 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F2")]
	[Address(RVA = "0x1010F5BBC", Offset = "0x10F5BBC", VA = "0x1010F5BBC")]
	public static MeigewwwParam Refuse(Refuse param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023A RID: 570 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F3")]
	[Address(RVA = "0x1010F5CE4", Offset = "0x10F5CE4", VA = "0x1010F5CE4")]
	public static MeigewwwParam Refuse(long managedFriendId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023B RID: 571 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F4")]
	[Address(RVA = "0x1010F5DFC", Offset = "0x10F5DFC", VA = "0x1010F5DFC")]
	public static MeigewwwParam Cancel(Cancel param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023C RID: 572 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F5")]
	[Address(RVA = "0x1010F5F24", Offset = "0x10F5F24", VA = "0x1010F5F24")]
	public static MeigewwwParam Cancel(long managedFriendId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023D RID: 573 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F6")]
	[Address(RVA = "0x1010F603C", Offset = "0x10F603C", VA = "0x1010F603C")]
	public static MeigewwwParam Terminate(Terminate param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023E RID: 574 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F7")]
	[Address(RVA = "0x1010F6164", Offset = "0x10F6164", VA = "0x1010F6164")]
	public static MeigewwwParam Terminate(long managedFriendId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600023F RID: 575 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F8")]
	[Address(RVA = "0x1010F627C", Offset = "0x10F627C", VA = "0x1010F627C")]
	public static MeigewwwParam Search(Search param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000240 RID: 576 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001F9")]
	[Address(RVA = "0x1010F6380", Offset = "0x10F6380", VA = "0x1010F6380")]
	public static MeigewwwParam Search(string myCode, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000241 RID: 577 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FA")]
	[Address(RVA = "0x1010F6490", Offset = "0x10F6490", VA = "0x1010F6490")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000242 RID: 578 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FB")]
	[Address(RVA = "0x1010F6680", Offset = "0x10F6680", VA = "0x1010F6680")]
	public static MeigewwwParam GetAll(int type, long managedBattlePartyId, int ignoreSupport, int reload, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
