﻿namespace FriendRequestTypes {
    public class GetAll {
        public int type;
        public long managedBattlePartyId;
        public int ignoreSupport;
        public int reload;
    }
}
