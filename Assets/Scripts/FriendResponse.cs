﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using FriendResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x0200008D RID: 141
[Token(Token = "0x2000066")]
[StructLayout(3)]
public static class FriendResponse
{
	// Token: 0x0600036B RID: 875 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000324")]
	[Address(RVA = "0x1010F68A0", Offset = "0x10F68A0", VA = "0x1010F68A0")]
	public static Propose Propose(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600036C RID: 876 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000325")]
	[Address(RVA = "0x1010F6908", Offset = "0x10F6908", VA = "0x1010F6908")]
	public static Accept Accept(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600036D RID: 877 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000326")]
	[Address(RVA = "0x1010F6970", Offset = "0x10F6970", VA = "0x1010F6970")]
	public static Refuse Refuse(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600036E RID: 878 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000327")]
	[Address(RVA = "0x1010F69D8", Offset = "0x10F69D8", VA = "0x1010F69D8")]
	public static Cancel Cancel(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600036F RID: 879 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000328")]
	[Address(RVA = "0x1010F6A40", Offset = "0x10F6A40", VA = "0x1010F6A40")]
	public static Terminate Terminate(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000370 RID: 880 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000329")]
	[Address(RVA = "0x1010F6AA8", Offset = "0x10F6AA8", VA = "0x1010F6AA8")]
	public static Search Search(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000371 RID: 881 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032A")]
	[Address(RVA = "0x1010F6B10", Offset = "0x10F6B10", VA = "0x1010F6B10")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
