﻿using CommonResponseTypes;
using WWWTypes;

namespace FriendResponseTypes {
    public class GetAll : CommonResponse {
        public FriendList[] friends;
        public FriendList[] guests;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (friends == null) ? string.Empty : friends.ToString();
            str += (guests == null) ? string.Empty : guests.ToString();
            return str;
        }
    }
}
