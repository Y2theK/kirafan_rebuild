﻿using CommonResponseTypes;

namespace FriendResponseTypes {
    public class Propose : CommonResponse {
        public long managedFriendId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += managedFriendId.ToString();
            return str;
        }
    }
}
