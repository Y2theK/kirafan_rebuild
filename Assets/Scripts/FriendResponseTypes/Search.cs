﻿using CommonResponseTypes;
using WWWTypes;

namespace FriendResponseTypes {
    public class Search : CommonResponse {
        public FriendList friend;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (friend == null) ? string.Empty : friend.ToString();
            return str;
        }
    }
}
