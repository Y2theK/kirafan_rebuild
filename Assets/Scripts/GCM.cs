﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x0200004F RID: 79
[Token(Token = "0x200002A")]
[StructLayout(3)]
public class GCM
{
	// Token: 0x060001C1 RID: 449 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017A")]
	[Address(RVA = "0x1010F6D2C", Offset = "0x10F6D2C", VA = "0x1010F6D2C")]
	public static void Register(Action<string> OnRegisterCallback, params string[] senderId)
	{
	}

	// Token: 0x060001C2 RID: 450 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017B")]
	[Address(RVA = "0x1010F6D30", Offset = "0x10F6D30", VA = "0x1010F6D30")]
	public static void Unregister()
	{
	}

	// Token: 0x060001C3 RID: 451 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017C")]
	[Address(RVA = "0x1010F6D34", Offset = "0x10F6D34", VA = "0x1010F6D34")]
	public GCM()
	{
	}

	// Token: 0x040001F1 RID: 497
	[Token(Token = "0x4000149")]
	private const string CLASS_NAME = "com.amazonaws.unity.AWSUnityGCMWrapper";
}
