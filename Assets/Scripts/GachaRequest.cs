﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using GachaRequestTypes;
using Meige;

// Token: 0x0200006A RID: 106
[Token(Token = "0x2000043")]
[StructLayout(3)]
public static class GachaRequest
{
	// Token: 0x06000243 RID: 579 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FC")]
	[Address(RVA = "0x1010F6D3C", Offset = "0x10F6D3C", VA = "0x1010F6D3C")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000244 RID: 580 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FD")]
	[Address(RVA = "0x1010F6E40", Offset = "0x10F6E40", VA = "0x1010F6E40")]
	public static MeigewwwParam GetAll(string gachaIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000245 RID: 581 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FE")]
	[Address(RVA = "0x1010F6F50", Offset = "0x10F6F50", VA = "0x1010F6F50")]
	public static MeigewwwParam Draw(Draw param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000246 RID: 582 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60001FF")]
	[Address(RVA = "0x1010F7208", Offset = "0x10F7208", VA = "0x1010F7208")]
	public static MeigewwwParam Draw(int gachaId, int drawType, int stepCode, bool reDraw, int characterId, bool isFree, bool isRateUp, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000247 RID: 583 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000200")]
	[Address(RVA = "0x1010F74C0", Offset = "0x10F74C0", VA = "0x1010F74C0")]
	public static MeigewwwParam GetBox(GetBox param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000248 RID: 584 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000201")]
	[Address(RVA = "0x1010F75E8", Offset = "0x10F75E8", VA = "0x1010F75E8")]
	public static MeigewwwParam GetBox(int gachaId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000249 RID: 585 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000202")]
	[Address(RVA = "0x1010F7700", Offset = "0x10F7700", VA = "0x1010F7700")]
	public static MeigewwwParam PointToCharacter(PointToCharacter param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024A RID: 586 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000203")]
	[Address(RVA = "0x1010F7868", Offset = "0x10F7868", VA = "0x1010F7868")]
	public static MeigewwwParam PointToCharacter(int gachaId, int characterId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024B RID: 587 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000204")]
	[Address(RVA = "0x1010F79C0", Offset = "0x10F79C0", VA = "0x1010F79C0")]
	public static MeigewwwParam PointToItem(PointToItem param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024C RID: 588 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000205")]
	[Address(RVA = "0x1010F7AC4", Offset = "0x10F7AC4", VA = "0x1010F7AC4")]
	public static MeigewwwParam PointToItem(int[] gachaIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024D RID: 589 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000206")]
	[Address(RVA = "0x1010F7BD4", Offset = "0x10F7BD4", VA = "0x1010F7BD4")]
	public static MeigewwwParam Redraw(Redraw param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024E RID: 590 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000207")]
	[Address(RVA = "0x1010F7C98", Offset = "0x10F7C98", VA = "0x1010F7C98")]
	public static MeigewwwParam Redraw([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600024F RID: 591 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000208")]
	[Address(RVA = "0x1010F7D5C", Offset = "0x10F7D5C", VA = "0x1010F7D5C")]
	public static MeigewwwParam Fix(Fix param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000250 RID: 592 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000209")]
	[Address(RVA = "0x1010F7E20", Offset = "0x10F7E20", VA = "0x1010F7E20")]
	public static MeigewwwParam Fix([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
