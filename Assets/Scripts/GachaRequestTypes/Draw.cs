﻿namespace GachaRequestTypes {
    public class Draw {
        public int gachaId;
        public int drawType;
        public int stepCode;
        public bool reDraw;
        public int characterId;
        public bool isFree;
        public bool isRateUp;
    }
}
