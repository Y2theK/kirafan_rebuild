﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using GachaResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x0200008E RID: 142
[Token(Token = "0x2000067")]
[StructLayout(3)]
public static class GachaResponse
{
	// Token: 0x06000372 RID: 882 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032B")]
	[Address(RVA = "0x1010F7F1C", Offset = "0x10F7F1C", VA = "0x1010F7F1C")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000373 RID: 883 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032C")]
	[Address(RVA = "0x1010F7F84", Offset = "0x10F7F84", VA = "0x1010F7F84")]
	public static Draw Draw(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000374 RID: 884 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032D")]
	[Address(RVA = "0x1010F7FEC", Offset = "0x10F7FEC", VA = "0x1010F7FEC")]
	public static GetBox GetBox(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000375 RID: 885 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032E")]
	[Address(RVA = "0x1010F8054", Offset = "0x10F8054", VA = "0x1010F8054")]
	public static PointToCharacter PointToCharacter(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000376 RID: 886 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600032F")]
	[Address(RVA = "0x1010F80BC", Offset = "0x10F80BC", VA = "0x1010F80BC")]
	public static PointToItem PointToItem(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000377 RID: 887 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000330")]
	[Address(RVA = "0x1010F8124", Offset = "0x10F8124", VA = "0x1010F8124")]
	public static Redraw Redraw(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000378 RID: 888 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000331")]
	[Address(RVA = "0x1010F818C", Offset = "0x10F818C", VA = "0x1010F818C")]
	public static Fix Fix(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
