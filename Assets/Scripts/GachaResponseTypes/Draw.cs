﻿using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes {
    public class Draw : CommonResponse {
        public GachaResult[] gachaResults;
        public GachaBonus[] gachaBonuses;
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public ArousalResult[] arousalResults;
        public OfferTitleType[] offerTitleTypes;
        public PlayerOffer[] offers;
        public int beforeDrawPoint;
        public int afterDrawPoint;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (gachaResults == null) ? string.Empty : gachaResults.ToString();
            str += (gachaBonuses == null) ? string.Empty : gachaBonuses.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (arousalResults == null) ? string.Empty : arousalResults.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (offers == null) ? string.Empty : offers.ToString();
            str += beforeDrawPoint.ToString();
            str += afterDrawPoint.ToString();
            return str;
        }
    }
}
