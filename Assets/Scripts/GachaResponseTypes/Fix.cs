﻿using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes {
    public class Fix : CommonResponse {
        public PlayerItemSummary[] itemSummary;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public ArousalResult[] arousalResults;
        public OfferTitleType[] offerTitleTypes;
        public PlayerOffer[] offers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (arousalResults == null) ? string.Empty : arousalResults.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (offers == null) ? string.Empty : offers.ToString();
            return str;
        }
    }
}
