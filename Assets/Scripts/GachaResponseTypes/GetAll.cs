﻿using CommonResponseTypes;
using System;
using WWWTypes;

namespace GachaResponseTypes {
    public class GetAll : CommonResponse {
        public GachaList[] gachas;
        public FinishedGacha[] finishedGachas;
        public GachaDailyFree[] gachaDailyFrees;
        public DateTime gachaFreeNextRefreshAt;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (gachas == null) ? string.Empty : gachas.ToString();
            str += (finishedGachas == null) ? string.Empty : finishedGachas.ToString();
            str += (gachaDailyFrees == null) ? string.Empty : gachaDailyFrees.ToString();
            str += gachaFreeNextRefreshAt.ToString();
            return str;
        }
    }
}
