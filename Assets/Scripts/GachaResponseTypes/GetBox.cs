﻿using CommonResponseTypes;

namespace GachaResponseTypes {
    public class GetBox : CommonResponse {
        public int[] characters;
        public int[] pickups;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (characters == null) ? string.Empty : characters.ToString();
            str += (pickups == null) ? string.Empty : pickups.ToString();
            return str;
        }
    }
}
