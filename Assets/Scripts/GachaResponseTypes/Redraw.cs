﻿using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes {
    public class Redraw : CommonResponse {
        public GachaResult[] gachaResults;
        public GachaBonus[] gachaBonuses;
        public PlayerItemSummary[] itemSummary;
        public ArousalResult[] arousalResults;
        public int beforeDrawPoint;
        public int afterDrawPoint;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (gachaResults == null) ? string.Empty : gachaResults.ToString();
            str += (gachaBonuses == null) ? string.Empty : gachaBonuses.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (arousalResults == null) ? string.Empty : arousalResults.ToString();
            str += beforeDrawPoint.ToString();
            str += afterDrawPoint.ToString();
            return str;
        }
    }
}
