﻿using UnityEngine;

public static class GameObject_Ext {
    public static void RemoveComponent<T>(this GameObject self) where T : Component {
        Object.Destroy(self.GetComponent<T>());
    }

    public static void SetLayer(this GameObject gameObject, int layerNo, bool needSetChildrens = true) {
        if (gameObject == null) {
            return;
        }
        gameObject.layer = layerNo;
        if (needSetChildrens) {
            foreach (Transform child in gameObject.transform) {
                child.gameObject.SetLayer(layerNo, needSetChildrens);
            }
        }
    }
}
