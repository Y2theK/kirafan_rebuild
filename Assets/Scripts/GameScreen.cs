﻿using UnityEngine;

public class GameScreen {
    private const float GUITEXT_SCALE_BASE = 320f;

    private static GameScreen m_Instance;
    protected ScreenSpace m_screenSpace;
    protected UISpace m_uiSpace;
    protected ASPECT_ADJUST_TYPE m_aspectAdjustType;

    public static void Create() {
        if (m_Instance == null) {
            m_Instance = new GameScreen();
        }
        m_Instance.Init();
    }

    public static GameScreen Instance => m_Instance;
    public ScreenSpace screenSpace => m_screenSpace;
    public UISpace uiSpace => m_uiSpace;
    public ASPECT_ADJUST_TYPE aspectAdjustType => m_aspectAdjustType;

    private void InitScreen() {
        Vector2 screenSize;
        Vector2 uiSpaceSize = ProjDepend.uiSpaceSize;
        ASPECT_ADJUST_TYPE adjustType = ProjDepend.aspectAdjustType;
        switch (Screen.orientation) {
            case ScreenOrientation.Portrait:
                if (Screen.width >= Screen.height) {
                    screenSize.x = Screen.height;
                    screenSize.y = Screen.width;
                } else {
                    screenSize.x = Screen.width;
                    screenSize.y = Screen.height;
                }
                break;
            case ScreenOrientation.LandscapeLeft:
                if (Screen.width >= Screen.height) {
                    screenSize.x = Screen.width;
                    screenSize.y = Screen.height;
                } else {
                    screenSize.x = Screen.height;
                    screenSize.y = Screen.width;
                }
                break;
            case ScreenOrientation.AutoRotation:
                screenSize.x = Screen.height;
                screenSize.y = Screen.width;
                break;
            default:
                screenSize.x = Screen.width;
                screenSize.y = Screen.height;
                break;
        }
        float uiAspect = uiSpaceSize.x / uiSpaceSize.y;
        Vector2 corrected = new Vector2(screenSize.y * uiAspect, screenSize.y);
        if (screenSize.x < corrected.x) {
            corrected.x = screenSize.x;
            corrected.y = screenSize.x / uiAspect;
        }
        Vector2 fixedPos = Vector2.zero;
        Vector2 fixedSize = Vector2.zero;
        if (adjustType == ASPECT_ADJUST_TYPE.STRETCH) {
            fixedSize.x = screenSize.x;
            fixedSize.y = screenSize.y;
        } else if (adjustType == ASPECT_ADJUST_TYPE.CINEMA_SCOPE) {
            fixedPos.x = (screenSize.x - corrected.x) / 2f;
            fixedPos.y = (screenSize.y - corrected.y) / 2f;
            fixedSize.x = corrected.x;
            fixedSize.y = corrected.y;
        }
        Vector2 screenPerUI = screenSize / uiSpaceSize;
        Vector2 fixedPerUI = fixedSize / uiSpaceSize;
        m_aspectAdjustType = adjustType;
        m_screenSpace = new ScreenSpace(screenSize, fixedPos, fixedSize);
        m_uiSpace = new UISpace(uiSpaceSize, screenPerUI, fixedPerUI);
        m_screenSpace.uiSpace = m_uiSpace;
        m_uiSpace.screenSpace = m_screenSpace;
    }

    public void Init() {
        InitScreen();
    }

    public Vector2 ScreenToUI(Vector2 pos) {
        float x = (pos.x - screenSpace.fixedAspectPos.x) / screenSpace.fixedAspectSize.x;
        float y = (pos.y - screenSpace.fixedAspectPos.y) / screenSpace.fixedAspectSize.y;
        return new Vector2(uiSpace.size.x * x, uiSpace.size.y * (1f - y));
    }

    public float GetGUITextX(int x) {
        return x / uiSpace.size.x;
    }

    public float GetGUITextY(int y) {
        return (uiSpace.size.y - y) / uiSpace.size.y;
    }

    public Vector2 GetGUIText(int x, int y) {
        return new Vector2(GetGUITextX(x), GetGUITextY(y));
    }

    public Vector3 GetGUITextScale() {
        return new Vector3(
            GUITEXT_SCALE_BASE / screenSpace.fixedAspectSize.x * uiSpace.fixedAspectPerUI.y,
            GUITEXT_SCALE_BASE / screenSpace.fixedAspectSize.y * uiSpace.fixedAspectPerUI.y,
            1f
        );
    }

    public Vector2 CalcScreenPos(Vector2 xy) {
        return xy * uiSpace.screenPerUI;
    }

    public Vector2 CalcRenderSize(Vector2 wh) {
        return wh * uiSpace.screenPerUI;
    }

    public class ScreenSpace {
        protected Vector2 m_size;
        protected float m_aspect;
        protected Vector2 m_fixedAspectPos;
        protected Vector2 m_fixedAspectSize;
        protected UISpace m_uiSpace;

        public Vector2 size => m_size;
        public float aspect => m_aspect;
        public Vector2 fixedAspectPos => m_fixedAspectPos;
        public Vector2 fixedAspectSize => m_fixedAspectSize;

        public UISpace uiSpace {
            set => m_uiSpace = value;
        }

        public ScreenSpace(Vector2 size, Vector3 fixedAspectPos, Vector3 fixedAspectSize) {
            m_size = size;
            m_aspect = size.x / size.y;
            m_fixedAspectPos = fixedAspectPos;
            m_fixedAspectSize = fixedAspectSize;
        }
    }

    public class UISpace {
        protected Vector2 m_size;
        protected float m_aspect;
        protected Vector2 m_ScreenPerUI;
        protected Vector2 m_fixedAspectPerUI;
        protected ScreenSpace m_screenSpace;

        public Vector2 size => m_size;
        public float aspect => m_aspect;
        public Vector2 screenPerUI => m_ScreenPerUI;
        public Vector2 fixedAspectPerUI => m_fixedAspectPerUI;

        public ScreenSpace screenSpace {
            set => m_screenSpace = value;
        }

        public UISpace(Vector2 size, Vector2 screenPerUI, Vector2 fixedAspectPerUI) {
            m_size = size;
            m_aspect = size.x / size.y;
            m_ScreenPerUI = screenPerUI;
            m_fixedAspectPerUI = fixedAspectPerUI;
        }
    }

    public enum ASPECT_ADJUST_TYPE {
        STRETCH,
        CINEMA_SCOPE,
        DEFAULT = 1
    }
}
