﻿using System;
using System.Reflection;
using UnityEngine;

#if UNITY_IOS
using UnityEngine.iOS;
#endif

public static class Helper
{
	public static void ShowUsingMemory() {
		//TODO: some kind of debug code
	}

	public static void Break(bool b, string msg) {
		//TODO: possibly a Debug.Break() with a Debug.Log() combo?
	}

	public static void Log(string msg) {
        //TODO: possibly a Debug.Log()?
    }

	public static float CalculateLinearTangent(AnimationCurve curve, int index, int toIndex) {
        return (float)(((double)curve[index].value - (double)curve[toIndex].value) / ((double)curve[index].time - (double)curve[toIndex].time));
    }

	public static void RandomInit() {
        MathFunc.InitRandom();
    }

	public static float GetFRandom(float min, float max) {
        return UnityEngine.Random.Range(min, max);
    }

	public static float GetFRandom() {
        return UnityEngine.Random.value;
    }

	public static int GetIRandom(int min, int max) {
        if (min > max) {
            int num = min;
            min = max;
            max = num;
        }
        return GetIRandom() % (max - min + 1) + min;
    }

	public static int GetIRandom() {
        return (int)(UnityEngine.Random.value * 8388607f);
    }

	public static float GetDegreeRandom() {
        return GetFRandom(-180f, 180f);
    }

	public static AnimationState InterpolateAnimationByFrame(Animation anim, string nextAnimName, float blendFrame, WrapMode wrap) {
        return InterpolateAnimation(anim, nextAnimName, Frame.Instance.FrameToSecond(blendFrame), wrap);
    }

	public static AnimationState InterpolateAnimation(Animation anim, string nextAnimName, float blendSec, WrapMode wrap) {
        AnimationState animationState = anim[nextAnimName];
        Break(animationState != null, "[InterpolateAnimation]not found " + nextAnimName);
        if (animationState != null) {
            animationState.wrapMode = (wrap != WrapMode.Once) ? wrap : WrapMode.ClampForever;
            anim.CrossFade(nextAnimName, blendSec, PlayMode.StopSameLayer);
            animationState.time = 0f;
        }
        return animationState;
    }

	public static bool IsAnimationPlaying(AnimationState animState) {
        return animState != null && (animState.wrapMode == WrapMode.Loop || (animState.enabled && animState.time < animState.length));
    }

	public static void PauseAnimation(Animation anim, bool bPause) {
        float speed = bPause ? 0f : 1f;
        foreach (AnimationState state in anim) {
            state.speed = speed;
        }
    }

	public static DifferentialAnimHandler GetShapeAndDifferentialAnimation(out Animation shapeAnim, out Animation differentialAnim, GameObject go) {
        shapeAnim = null;
        differentialAnim = null;
        DifferentialAnimHandler animHandler = go.GetComponentInChildren<DifferentialAnimHandler>();
        if (animHandler != null) {
            differentialAnim = animHandler.GetAnimation();
        }
        foreach (Animation animation in go.GetComponentsInChildren<Animation>()) {
            if (!animation.Equals(differentialAnim)) {
                shapeAnim = animation;
                break;
            }
        }
        return animHandler;
    }

	public static string GetMeshNameByRenderer(Renderer rd) {
        if (rd.gameObject.TryGetComponent(out MeshFilter component)) {
            if (component.sharedMesh != null) {
                return component.sharedMesh.name;
            }
        } else if (rd is SkinnedMeshRenderer skinned && skinned.sharedMesh != null) {
            return skinned.sharedMesh.name;
        }
        return null;
    }

	public static Material SearchMaterial(Renderer[] rdArray, string matName) {
        return SearchMaterial(rdArray, null, matName);
    }

	public static Material SearchMaterial(Renderer[] rdArray, string goName, string matName) {
        foreach (Renderer renderer in rdArray) {
            if (goName == null || renderer.gameObject.name.Equals(goName)) {
                foreach (Material material in renderer.materials) {
                    if (matName == material.name) {
                        return material;
                    }
                    string text = new string(material.name.ToCharArray());
                    if (text.Contains(" (Instance)")) {
                        text = text.Replace(" (Instance)", string.Empty);
                    }
                    if (text == matName) {
                        return material;
                    }
                }
            }
        }
        return null;
    }

	public static void SetRenderOrder(GameObject go, int renderOrder) {
        SetRenderOrder(go.GetComponentsInChildren<Renderer>(), renderOrder);
    }

	public static void SetRenderOrder(Renderer[] mrArray, int renderOrder) {
        foreach (Renderer renderer in mrArray) {
            foreach (Material material in renderer.materials) {
                SetRenderOrder(material, renderOrder);
            }
        }
    }

	public static void SetRenderOrder(Renderer mr, int renderOrder) {
        foreach (Material material in mr.materials) {
            SetRenderOrder(material, renderOrder);
        }
    }

	public static void SetRenderOrder(Material mat, int renderOrder) {
        mat.renderQueue = renderOrder;
    }

	public static void SetRenderColor(Renderer[] mrArray, Color color) {
        foreach (Renderer renderer in mrArray) {
            foreach (Material material in renderer.materials) {
                material.SetColor("_Color", color);
            }
        }
    }

	public static void SetRenderColor(Renderer mr, Color color) {
        foreach (Material material in mr.materials) {
            SetMaterialColor(material, color);
        }
    }

	public static void SetMaterialColor(Material mat, Color color) {
        mat.SetColor("_Color", color);
    }

	public static void SetTextureOffset(Renderer[] mrArray, Vector2 offs) {
        foreach (Renderer renderer in mrArray) {
            foreach (Material material in renderer.materials) {
                material.SetTextureOffset("_MainTex", offs);
            }
        }
    }

	public static void EnableRender(Renderer[] mrArray, bool bEnable) {
        if (mrArray != null) {
            foreach (Renderer renderer in mrArray) {
                if (renderer != null) {
                    renderer.enabled = bEnable;
                }
            }
        }
    }

	public static void EnableRender(Renderer mr, bool bEnable) {
        if (mr != null) {
            mr.enabled = bEnable;
        }
    }

	public static bool CalcProbability(float freq) {
        return freq > 0f && GetFRandom() <= freq;
    }

	public static T CopyComponent<T>(T original, GameObject destination) where T : Component {
        Type type = original.GetType();
        Component component = destination.AddComponent(type);
        FieldInfo[] fields = type.GetFields();
        foreach (FieldInfo fieldInfo in fields) {
            fieldInfo.SetValue(component, fieldInfo.GetValue(original));
        }
        return component as T;
    }

	public static void DestroyAll(GameObject go) {
        DestroyAll(go, 1f);
    }

	public static void DestroyAll(GameObject go, float frame) {
        DestroyAll(go, frame, true);
    }

	public static void DestroyAll(GameObject go, float frame, bool unloadAssets) {
        if (go == null) { return; }

        Transform[] transforms = go.GetComponentsInChildren<Transform>();
        foreach (Transform transform in transforms) {
            GameObject gameObject = transform.gameObject;
            if (gameObject.TryGetComponent(out Rigidbody rigidbody)) {
                UnityEngine.Object.Destroy(rigidbody, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out Camera camera)) {
                UnityEngine.Object.Destroy(camera, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out Light light)) {
                UnityEngine.Object.Destroy(light, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out Animation animation)) {
                foreach (AnimationState state in animation) {
                    animation.RemoveClip(state.clip);
                }
                UnityEngine.Object.Destroy(animation, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out ConstantForce constantForce)) {
                UnityEngine.Object.Destroy(constantForce, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out Renderer renderer)) {
                UnityEngine.Object.Destroy(renderer, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out AudioSource audioSource)) {
                UnityEngine.Object.Destroy(audioSource, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out Collider collider)) {
                UnityEngine.Object.Destroy(collider, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out HingeJoint hingeJoint)) {
                UnityEngine.Object.Destroy(hingeJoint, Frame.Instance.FrameToSecond(frame));
            }
            if (gameObject.TryGetComponent(out ParticleSystem particleSystem)) {
                UnityEngine.Object.Destroy(particleSystem, Frame.Instance.FrameToSecond(frame));
            }
            UnityEngine.Object.Destroy(gameObject, Frame.Instance.FrameToSecond(frame));
        }
        UnityEngine.Object.Destroy(go, Frame.Instance.FrameToSecond(frame));
        if (unloadAssets) {
            Resources.UnloadUnusedAssets();
        }
    }

	public static string CheckAvailableString(string str) {
        if (str != null) {
            return (str.Length > 0) ? str : null;
        }
        return null;
    }

	public static bool CheckFieldTouch(out RaycastHit hitInfo, Vector3 position, int layerMask) {
        Vector3 origin = position;
        origin.y = 100f;
        return Physics.Raycast(origin, Vector3.up * -1f, out hitInfo, 10000f, layerMask);
    }

	public static bool CheckFieldTouch(out RaycastHit[] hitInfoArray, Vector3 position, int layerMask) {
        Vector3 origin = position;
        origin.y = 100f;
        hitInfoArray = Physics.RaycastAll(origin, Vector3.up * -1f, 10000f, layerMask);
        return hitInfoArray.Length > 0;
    }

	public static bool CollisionDetectionByScreenRayScattering(Camera cam, Vector3 cam_pos, Vector2 scrPosS, Vector2 scrPosD, float fPrecisionOfAngle, int layerMask) {
        Ray rayS = cam.ScreenPointToRay(scrPosS);
        Ray rayD = cam.ScreenPointToRay(scrPosD);
        return CollisionDetectionByScreenRayScattering(cam_pos, rayS, rayD, fPrecisionOfAngle, layerMask);
    }

	public static bool CollisionDetectionByScreenRayScattering(Vector3 origin, Ray rayS, Ray rayD, float fPrecisionOfAngle, int layerMask) {
        float angle = Vector3.Angle(rayD.direction, rayS.direction);
        if (Physics.Raycast(origin, rayS.direction, float.PositiveInfinity, layerMask)) {
            return true;
        }
        if (Physics.Raycast(origin, rayD.direction, float.PositiveInfinity, layerMask)) {
            return true;
        }
        for (float a = fPrecisionOfAngle; a < angle; a += fPrecisionOfAngle) {
            if (Physics.Raycast(origin, Vector3.Lerp(rayS.direction, rayD.direction, a / angle).normalized, float.PositiveInfinity, layerMask)) {
                return true;
            }
        }
        return false;
    }

	public static void CalculateWorldPosOfScreen(out Vector3 vCenter, out float width, out float height, Camera cam, Transform camTrfm, float zOffs) {
        Vector3 zero = Vector3.zero;
        zero.y = (float)Screen.height;
        Vector3 direction = cam.ScreenPointToRay(zero).direction;
        Vector3 left = -camTrfm.right;
        Vector3 up = camTrfm.up;
        Vector3 forward = camTrfm.forward;
        Vector3 position = camTrfm.position;
        float fwdFactor = MathFunc.Dot(ref direction, ref forward);
        float leftFactor = MathFunc.Dot(ref direction, ref left);
        float upFactor = MathFunc.Dot(ref direction, ref up);
        width = leftFactor * zOffs / fwdFactor * 2f;
        height = upFactor * zOffs / fwdFactor * 2f;
        width *= GameScreen.Instance.screenSpace.fixedAspectSize.x / GameScreen.Instance.screenSpace.size.x;
        height *= GameScreen.Instance.screenSpace.fixedAspectSize.y / GameScreen.Instance.screenSpace.size.y;
        vCenter = position + forward * zOffs;
    }

	public static bool IsNonRetina() {
#if UNITY_IOS
        switch (Device.generation) {
            case DeviceGeneration.iPhone:
            case DeviceGeneration.iPhone3G:
            case DeviceGeneration.iPhone3GS:
            case DeviceGeneration.iPodTouch1Gen:
            case DeviceGeneration.iPodTouch2Gen:
            case DeviceGeneration.iPodTouch3Gen:
            case DeviceGeneration.iPad1Gen:
            case DeviceGeneration.iPad2Gen:
            case DeviceGeneration.iPadMini1Gen:
                return true;
            default:
                return false;
        }
#else
        return false;
#endif
    }

	public static Color ParseColorCode(string code) {
        if (!string.IsNullOrEmpty(code) && code[0] == '#') {
            string text = code.Replace("#", string.Empty);
            if (text.Length <= 6) {
                text += "FF";
            }
            int value = Convert.ToInt32(text, 16);
            byte[] bytes = BitConverter.GetBytes(value);
            Color result = new Color(bytes[3] / 255f, bytes[2] / 255f, bytes[1] / 255f, bytes[0] / 255f);
            return result;
        }
        return Color.white;
    }

	public static string GetUniqueIDForVendor() {
#if UNITY_IOS
        return Device.vendorIdentifier;
#elif UNITY_ANDROID
        AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = player.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject contentResolver = activity.Call<AndroidJavaObject>("getContentResolver", new object[0]);
        AndroidJavaClass settings = new AndroidJavaClass("android.provider.Settings$Secure");
        return settings.CallStatic<string>("getString", new object[] {
            contentResolver,
            "android_id"
        });
#else
        return null;
#endif
    }

	public static bool IsEmulator() {
#if UNITY_ANDROID
        AndroidJavaClass build = new AndroidJavaClass("android.os.Build");
        if (build == null) { return false; }

        string model = build.GetStatic<string>("MODEL");
        return model == "sdk" || model == "sdk_phone_armv7";
#else
        return false;
#endif
    }

	public enum eTangentMode {
		eTangentMode_Editable,
		eTangentMode_Smooth,
		eTangentMode_Linear,
		eTangentMode_Stepped
	}
}
