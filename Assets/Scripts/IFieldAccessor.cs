﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000B6 RID: 182
[Token(Token = "0x200008D")]
[StructLayout(3)]
public interface IFieldAccessor
{
	// Token: 0x06000437 RID: 1079
	[Token(Token = "0x60003ED")]
	[Address(Slot = "0")]
	object GetValue(object target);

	// Token: 0x06000438 RID: 1080
	[Token(Token = "0x60003EE")]
	[Address(Slot = "1")]
	void SetValue(object target, object value);
}
