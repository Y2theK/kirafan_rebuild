﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

// Token: 0x02000023 RID: 35
[Token(Token = "0x200001C")]
[StructLayout(3)]
public class INetComHandle
{
	// Token: 0x0600007E RID: 126 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000072")]
	[Address(RVA = "0x1010FC644", Offset = "0x10FC644", VA = "0x1010FC644")]
	public void SetCallback(INetComHandle.ResponseCallbak pcallend)
	{
	}

	// Token: 0x0600007F RID: 127 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000073")]
	[Address(RVA = "0x1010FC64C", Offset = "0x10FC64C", VA = "0x1010FC64C")]
	public void CreateComParam()
	{
	}

	// Token: 0x06000080 RID: 128 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000074")]
	[Address(RVA = "0x1010FC85C", Offset = "0x10FC85C", VA = "0x1010FC85C")]
	public void CommonCallback(MeigewwwParam pparam)
	{
	}

	// Token: 0x06000081 RID: 129 RVA: 0x000022B0 File Offset: 0x000004B0
	[Token(Token = "0x6000075")]
	[Address(RVA = "0x1010FCE50", Offset = "0x10FCE50", VA = "0x1010FCE50")]
	public bool IsComEnd()
	{
		return default(bool);
	}

	// Token: 0x06000082 RID: 130 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000076")]
	[Address(RVA = "0x1010FCE8C", Offset = "0x10FCE8C", VA = "0x1010FCE8C")]
	public object GetResponse()
	{
		return null;
	}

	// Token: 0x06000083 RID: 131 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000077")]
	[Address(RVA = "0x1010FCE94", Offset = "0x10FCE94", VA = "0x1010FCE94")]
	public void SetDummyResponse()
	{
	}

	// Token: 0x06000084 RID: 132 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000078")]
	[Address(RVA = "0x1010FCF24", Offset = "0x10FCF24", VA = "0x1010FCF24")]
	public void DummyToCallBack()
	{
	}

	// Token: 0x06000085 RID: 133 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000079")]
	[Address(RVA = "0x1010FCF58", Offset = "0x10FCF58", VA = "0x1010FCF58")]
	public INetComHandle()
	{
	}

	// Token: 0x040000D1 RID: 209
	[Token(Token = "0x4000087")]
	public const int RETRYE_COUNT = 3;

	// Token: 0x040000D2 RID: 210
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x4000088")]
	protected string ApiName;

	// Token: 0x040000D3 RID: 211
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000089")]
	protected bool Request;

	// Token: 0x040000D4 RID: 212
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x400008A")]
	protected MeigewwwParam WWWParam;

	// Token: 0x040000D5 RID: 213
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x400008B")]
	protected INetComHandle.ResponseCallbak m_CallBack;

	// Token: 0x040000D6 RID: 214
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x400008C")]
	protected Type ResponseType;

	// Token: 0x040000D7 RID: 215
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x400008D")]
	protected object ResponseObject;

	// Token: 0x040000D8 RID: 216
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x400008E")]
	protected int m_RertyNum;

	// Token: 0x040000D9 RID: 217
	[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
	[Token(Token = "0x400008F")]
	protected ResponseCommon.DialogType m_RetryType;

	// Token: 0x02000024 RID: 36
	// (Invoke) Token: 0x06000087 RID: 135
	[Token(Token = "0x2000CE2")]
	public delegate void ResponseCallbak(INetComHandle presponse);
}
