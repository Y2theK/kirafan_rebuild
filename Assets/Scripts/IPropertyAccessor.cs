﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000B3 RID: 179
[Token(Token = "0x200008A")]
[StructLayout(3)]
public interface IPropertyAccessor
{
	// Token: 0x0600042F RID: 1071
	[Token(Token = "0x60003E5")]
	[Address(Slot = "0")]
	object GetValue(object target);

	// Token: 0x06000430 RID: 1072
	[Token(Token = "0x60003E6")]
	[Address(Slot = "1")]
	void SetValue(object target, object value);
}
