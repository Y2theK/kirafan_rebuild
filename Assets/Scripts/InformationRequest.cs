﻿using InformationRequestTypes;
using Meige;

public static class InformationRequest {

    public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("information/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("platform", param.platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getall(int platform, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("information/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("platform", platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Read(Read param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/information/read", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("platform", param.platform);
        www.Add("informationIds", param.informationIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Read(int platform, int[] informationIds, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/information/read", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("platform", platform);
        www.Add("informationIds", informationIds);
        www.SetPostCrypt();
        return www;
    }
}
