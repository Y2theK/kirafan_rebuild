﻿using InformationResponseTypes;
using Meige;
using System.Collections.Generic;
using WWWTypes;

public static class InformationResponse {
    public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
    }

    public static Read Read(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Read>(param, dialogType, acceptableResultCodes);
    }
}
