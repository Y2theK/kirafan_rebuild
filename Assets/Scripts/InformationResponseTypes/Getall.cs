﻿using CommonResponseTypes;
using WWWTypes;

namespace InformationResponseTypes {
    public class Getall : CommonResponse {
        public Information[] informations;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (informations == null) ? string.Empty : informations.ToString();
            return str;
        }
    }
}
