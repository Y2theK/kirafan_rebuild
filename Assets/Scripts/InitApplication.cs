﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000D5 RID: 213
[Token(Token = "0x200009E")]
[StructLayout(3)]
public class InitApplication : MonoBehaviour
{
	// Token: 0x0600051B RID: 1307 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60004C0")]
	[Address(RVA = "0x1010FD634", Offset = "0x10FD634", VA = "0x1010FD634")]
	private void Awake()
	{
	}

	// Token: 0x0600051C RID: 1308 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60004C1")]
	[Address(RVA = "0x1010FD638", Offset = "0x10FD638", VA = "0x1010FD638")]
	private void FixedUpdate()
	{
	}

	// Token: 0x0600051D RID: 1309 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60004C2")]
	[Address(RVA = "0x1010FD694", Offset = "0x10FD694", VA = "0x1010FD694")]
	public InitApplication()
	{
	}

	// Token: 0x040002FF RID: 767
	[Token(Token = "0x40001CD")]
	private const float m_waitSec = 1f;

	// Token: 0x04000300 RID: 768
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40001CE")]
	protected float m_waitSecWork;
}
