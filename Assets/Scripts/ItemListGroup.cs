﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using UnityEngine;

// Token: 0x0200005A RID: 90
[Token(Token = "0x2000034")]
[StructLayout(3)]
public class ItemListGroup : UIGroup
{
	// Token: 0x17000057 RID: 87
	// (get) Token: 0x060001E9 RID: 489 RVA: 0x00002868 File Offset: 0x00000A68
	[Token(Token = "0x1700004B")]
	public ItemListGroup.eTab Tab
	{
		[Token(Token = "0x60001A2")]
		[Address(RVA = "0x1010FD69C", Offset = "0x10FD69C", VA = "0x1010FD69C")]
		get
		{
			return ItemListGroup.eTab.Upgrade;
		}
	}

	// Token: 0x060001EA RID: 490 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A3")]
	[Address(RVA = "0x1010FD6A4", Offset = "0x10FD6A4", VA = "0x1010FD6A4")]
	public void Setup()
	{
	}

	// Token: 0x060001EB RID: 491 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A4")]
	[Address(RVA = "0x1010FDB7C", Offset = "0x10FDB7C", VA = "0x1010FDB7C", Slot = "5")]
	public override void Open()
	{
	}

	// Token: 0x060001EC RID: 492 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A5")]
	[Address(RVA = "0x1010FDBC8", Offset = "0x10FDBC8", VA = "0x1010FDBC8")]
	private void ApplyTab(ItemListGroup.eTab tab)
	{
	}

	// Token: 0x060001ED RID: 493 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A6")]
	[Address(RVA = "0x1010FDFC8", Offset = "0x10FDFC8", VA = "0x1010FDFC8")]
	private void OnChangeTabCallBack(int idx)
	{
	}

	// Token: 0x060001EE RID: 494 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60001A7")]
	[Address(RVA = "0x1010FDFCC", Offset = "0x10FDFCC", VA = "0x1010FDFCC")]
	public ItemListGroup()
	{
	}

	// Token: 0x040001FE RID: 510
	[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
	[Token(Token = "0x4000153")]
	[SerializeField]
	private TabGroup m_TabGroup;

	// Token: 0x040001FF RID: 511
	[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
	[Token(Token = "0x4000154")]
	[SerializeField]
	private ScrollViewBase m_Scroll;

	// Token: 0x04000200 RID: 512
	[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
	[Token(Token = "0x4000155")]
	private ItemListGroup.eTab m_Tab;

	// Token: 0x0200005B RID: 91
	[Token(Token = "0x2000D01")]
	public enum eTab
	{
		// Token: 0x04000202 RID: 514
		[Token(Token = "0x4005446")]
		None = -1,
		// Token: 0x04000203 RID: 515
		[Token(Token = "0x4005447")]
		Upgrade,
		// Token: 0x04000204 RID: 516
		[Token(Token = "0x4005448")]
		LimitBreak,
		// Token: 0x04000205 RID: 517
		[Token(Token = "0x4005449")]
		Evolution,
		// Token: 0x04000206 RID: 518
		[Token(Token = "0x400544A")]
		Weapon,
		// Token: 0x04000207 RID: 519
		[Token(Token = "0x400544B")]
		AbilitySphere,
		// Token: 0x04000208 RID: 520
		[Token(Token = "0x400544C")]
		TradeItem,
		// Token: 0x04000209 RID: 521
		[Token(Token = "0x400544D")]
		Etc
	}
}
