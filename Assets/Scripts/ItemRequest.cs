﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemRequestTypes;
using Meige;

// Token: 0x0200006C RID: 108
[Token(Token = "0x2000045")]
[StructLayout(3)]
public static class ItemRequest
{
	// Token: 0x06000255 RID: 597 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600020E")]
	[Address(RVA = "0x1010FDFD4", Offset = "0x10FDFD4", VA = "0x1010FDFD4")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000256 RID: 598 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600020F")]
	[Address(RVA = "0x1010FE0A4", Offset = "0x10FE0A4", VA = "0x1010FE0A4")]
	public static MeigewwwParam Getall(string type, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000257 RID: 599 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000210")]
	[Address(RVA = "0x1010FE184", Offset = "0x10FE184", VA = "0x1010FE184")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000258 RID: 600 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000211")]
	[Address(RVA = "0x1010FE254", Offset = "0x10FE254", VA = "0x1010FE254")]
	public static MeigewwwParam Get(string itemId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
