﻿using CommonResponseTypes;
using WWWTypes;

namespace ItemResponseTypes {
    public class Getall : CommonResponse {
        public Item[] items;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (items == null) ? string.Empty : items.ToString();
            return str;
        }
    }
}
