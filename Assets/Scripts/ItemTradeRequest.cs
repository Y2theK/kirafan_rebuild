﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemTradeRequestTypes;
using Meige;

// Token: 0x0200006D RID: 109
[Token(Token = "0x2000046")]
[StructLayout(3)]
public static class ItemTradeRequest
{
	// Token: 0x06000259 RID: 601 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000212")]
	[Address(RVA = "0x1010FE51C", Offset = "0x10FE51C", VA = "0x1010FE51C")]
	public static MeigewwwParam Getrecipe(Getrecipe param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600025A RID: 602 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000213")]
	[Address(RVA = "0x1010FE5E0", Offset = "0x10FE5E0", VA = "0x1010FE5E0")]
	public static MeigewwwParam Getrecipe([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600025B RID: 603 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000214")]
	[Address(RVA = "0x1010FE6A4", Offset = "0x10FE6A4", VA = "0x1010FE6A4")]
	public static MeigewwwParam Trade(Trade param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600025C RID: 604 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000215")]
	[Address(RVA = "0x1010FE84C", Offset = "0x10FE84C", VA = "0x1010FE84C")]
	public static MeigewwwParam Trade(int recipeId, int indexNo, int count, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
