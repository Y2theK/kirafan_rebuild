﻿namespace ItemTradeRequestTypes {
    public class Trade {
        public int recipeId;
        public int indexNo;
        public int count;
    }
}
