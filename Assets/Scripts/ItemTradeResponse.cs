﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemTradeResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000091 RID: 145
[Token(Token = "0x200006A")]
[StructLayout(3)]
public static class ItemTradeResponse
{
	// Token: 0x0600037D RID: 893 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000336")]
	[Address(RVA = "0x1010FE9FC", Offset = "0x10FE9FC", VA = "0x1010FE9FC")]
	public static Getrecipe Getrecipe(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x0600037E RID: 894 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000337")]
	[Address(RVA = "0x1010FEA64", Offset = "0x10FEA64", VA = "0x1010FEA64")]
	public static Trade Trade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
