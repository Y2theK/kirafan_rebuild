﻿using CommonResponseTypes;
using WWWTypes;

namespace ItemTradeResponseTypes {
    public class Getrecipe : CommonResponse {
        public ItemTradeRecipe[] itemTradeRecipes;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemTradeRecipes == null) ? string.Empty : itemTradeRecipes.ToString();
            return str;
        }
    }
}
