﻿using CommonResponseTypes;
using WWWTypes;

namespace ItemTradeResponseTypes {
    public class Trade : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon[] managedWeapons;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public int totalTradeCount;
        public int monthlyTradeCount;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += totalTradeCount.ToString();
            str += monthlyTradeCount.ToString();
            return str;
        }
    }
}
