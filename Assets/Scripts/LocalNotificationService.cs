﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000050 RID: 80
[Token(Token = "0x200002B")]
[StructLayout(3)]
internal class LocalNotificationService : SingletonMonoBehaviour<LocalNotificationService>
{
	// Token: 0x060001C4 RID: 452 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017D")]
	[Address(RVA = "0x1010FECF4", Offset = "0x10FECF4", VA = "0x1010FECF4")]
	protected new void Awake()
	{
	}

	// Token: 0x060001C5 RID: 453 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017E")]
	[Address(RVA = "0x1010FECF8", Offset = "0x10FECF8", VA = "0x1010FECF8")]
	protected void Start()
	{
	}

	// Token: 0x060001C6 RID: 454 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600017F")]
	[Address(RVA = "0x1010FED00", Offset = "0x10FED00", VA = "0x1010FED00")]
	public static void RegisterForNotifications()
	{
	}

	// Token: 0x060001C7 RID: 455 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000180")]
	[Address(RVA = "0x1010FED0C", Offset = "0x10FED0C", VA = "0x1010FED0C")]
	public static void SimpleNotification(int id, TimeSpan delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "large", [Optional] Color color)
	{
	}

	// Token: 0x060001C8 RID: 456 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000181")]
	[Address(RVA = "0x1010FEFD0", Offset = "0x10FEFD0", VA = "0x1010FEFD0")]
	public static void SimpleNotification(int id, int delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "large", [Optional] Color color)
	{
	}

	// Token: 0x060001C9 RID: 457 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000182")]
	[Address(RVA = "0x1010FEDA4", Offset = "0x10FEDA4", VA = "0x1010FEDA4")]
	public static void SendNotification(int id, long delay, string title, string message, string smallIcon, string bigIcon = "large", [Optional] Color32 bgColor, bool sound = true, bool vibrate = true, bool lights = true)
	{
	}

	// Token: 0x060001CA RID: 458 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000183")]
	[Address(RVA = "0x1010FF024", Offset = "0x10FF024", VA = "0x1010FF024")]
	public static void CancelNotification(int id)
	{
	}

	// Token: 0x060001CB RID: 459 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000184")]
	[Address(RVA = "0x1010FF3C4", Offset = "0x10FF3C4", VA = "0x1010FF3C4")]
	public static void CancellAll()
	{
	}

	// Token: 0x060001CC RID: 460 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000185")]
	[Address(RVA = "0x1010FF460", Offset = "0x10FF460", VA = "0x1010FF460")]
	private void OnApplicationPause(bool isPause)
	{
	}

	// Token: 0x060001CD RID: 461 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000186")]
	[Address(RVA = "0x1010FF470", Offset = "0x10FF470", VA = "0x1010FF470")]
	public LocalNotificationService()
	{
	}
}
