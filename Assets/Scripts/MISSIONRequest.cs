﻿using Meige;
using MISSIONRequestTypes;
using WWWTypes;

public static class MISSIONRequest {
    public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("missionLogs", param.missionLogs);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Set(PlayerMissionLog[] missionLogs, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("missionLogs", missionLogs);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Complete(Complete param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedMissionId", param.managedMissionId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Complete(long managedMissionId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedMissionId", managedMissionId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Refresh(Refresh param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/refresh", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Refresh(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/mission/refresh", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }
}
