﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using MISSIONResponseTypes;
using WWWTypes;

// Token: 0x02000093 RID: 147
[Token(Token = "0x200006C")]
[StructLayout(3)]
public static class MISSIONResponse
{
	// Token: 0x06000381 RID: 897 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600033A")]
	[Address(RVA = "0x1010FFC44", Offset = "0x10FFC44", VA = "0x1010FFC44")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000382 RID: 898 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600033B")]
	[Address(RVA = "0x1010FFCAC", Offset = "0x10FFCAC", VA = "0x1010FFCAC")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000383 RID: 899 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600033C")]
	[Address(RVA = "0x1010FFD14", Offset = "0x10FFD14", VA = "0x1010FFD14")]
	public static Complete Complete(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x06000384 RID: 900 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600033D")]
	[Address(RVA = "0x1010FFD7C", Offset = "0x10FFD7C", VA = "0x1010FFD7C")]
	public static Refresh Refresh(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
