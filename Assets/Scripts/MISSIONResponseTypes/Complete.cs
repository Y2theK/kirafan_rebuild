﻿using CommonResponseTypes;
using WWWTypes;

namespace MISSIONResponseTypes {
    public class Complete : CommonResponse {
        public Player player;
        public PlayerPresent[] presents;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : presents.ToString();
            return str;
        }
    }
}
