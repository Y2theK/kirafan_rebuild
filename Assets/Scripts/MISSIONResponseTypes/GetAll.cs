﻿using CommonResponseTypes;
using WWWTypes;

namespace MISSIONResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerMissionLog[] missionLogs;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (missionLogs == null) ? string.Empty : missionLogs.ToString();
            return str;
        }
    }
}
