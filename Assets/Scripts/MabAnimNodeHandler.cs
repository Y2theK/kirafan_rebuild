﻿using Meige;
using System;
using UnityEngine;

[Serializable]
public class MabAnimNodeHandler {
    public AnimTargetInfo m_Target;
    public AnimNodeCurve[] m_Curves;

    public void MakeTargetInfoForHierarchy(eAnimTargetType type, string name, int refID, int hieIndex) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = hieIndex;
    }

	public void MakeTargetInfoForMesh(eAnimTargetType type, string name, int refID, int msbObjIndex) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = msbObjIndex;
    }

	public void MakeTargetInfoForMaterial(eAnimTargetType type, string name, int refID, int msbMatIndex) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = msbMatIndex;
    }

	public void MakeTargetInfoForTexture(eAnimTargetType type, string name, int refID, int msbMatIndex, eTextureType texType, int layerIdx) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = msbMatIndex;
        m_Target.m_Param1 = (int)texType;
        m_Target.m_Param2 = layerIdx;
    }

	public void MakeTargetInfoForCamera(eAnimTargetType type, string name, int refID, int msbCamIndex) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = msbCamIndex;
    }

	public void MakeTargetInfoForParticle(eAnimTargetType type, string name, int refID, int peIndex) {
        m_Target = new AnimTargetInfo();
        m_Target.m_TargetType = type;
        m_Target.m_TargetName = name;
        m_Target.m_RefID = refID;
        m_Target.m_Param0 = peIndex;
    }

	public void Setup(ref MeigeAnimCtrl.TargetInstance tgtCache, MsbHandler msbHndl) {
        tgtCache.Init();
        switch (m_Target.m_TargetType) {
            case eAnimTargetType.eAnimTarget_MatColor:
            case eAnimTargetType.eAnimTarget_MatColor_R:
            case eAnimTargetType.eAnimTarget_MatColor_G:
            case eAnimTargetType.eAnimTarget_MatColor_B:
            case eAnimTargetType.eAnimTarget_MatColor_A:
                MsbMaterialHandler matHandler = msbHndl.GetMsbMaterialHandlerByName(m_Target.m_TargetName);
                if (matHandler != null) {
                    tgtCache.m_MsbMatWork = matHandler.GetWork();
                }
                break;
            case eAnimTargetType.eAnimTarget_TexCoverageUV:
            case eAnimTargetType.eAnimTarget_TexTranslationUV:
            case eAnimTargetType.eAnimTarget_TexRotateUV:
            case eAnimTargetType.eAnimTarget_TexOffsetUV:
            case eAnimTargetType.eAnimTarget_TexCoverageUV_U:
            case eAnimTargetType.eAnimTarget_TexCoverageUV_V:
            case eAnimTargetType.eAnimTarget_TexTranslationUV_U:
            case eAnimTargetType.eAnimTarget_TexTranslationUV_V:
            case eAnimTargetType.eAnimTarget_TexOffsetUV_U:
            case eAnimTargetType.eAnimTarget_TexOffsetUV_V:
                MsbMaterialHandler matHandlerTex = msbHndl.GetMsbMaterialHandlerByName(m_Target.m_TargetName);
                if (matHandlerTex != null) {
                    int idx = matHandlerTex.m_Src.SearchTexIndex((eTextureType)m_Target.m_Param1, m_Target.m_Param2);
                    if (idx != -1) {
                        tgtCache.m_MsbTextureWork = matHandlerTex.GetWork().m_Texture[idx];
                    }
                }
                break;
            case eAnimTargetType.eAnimTarget_FocalLength:
            case eAnimTargetType.eAnimTarget_CamOrthographicSize:
                if (msbHndl.GetMsbCameraHandlerNum() > 0) {
                    MsbCameraHandler camHandler = msbHndl.GetMsbCameraHandler(0);
                    if (camHandler != null) {
                        tgtCache.m_MsbCameraWork = camHandler.GetWork();
                    }
                }
                break;
            case eAnimTargetType.eAnimTarget_MeshVisibility:
            case eAnimTargetType.eAnimTarget_MeshColor:
            case eAnimTargetType.eAnimTarget_MeshColor_R:
            case eAnimTargetType.eAnimTarget_MeshColor_G:
            case eAnimTargetType.eAnimTarget_MeshColor_B:
            case eAnimTargetType.eAnimTarget_MeshColor_A:
                MsbObjectHandler objHandler = msbHndl.GetMsbObjectHandlerByName(m_Target.m_TargetName);
                if (objHandler != null) {
                    tgtCache.m_MsbObjWork = objHandler.GetWork();
                }
                break;
            case eAnimTargetType.eAnimTarget_PEActive:
                if (msbHndl.GetParticleEmitterNum() > m_Target.m_Param0) {
                    tgtCache.m_MeigeParticleEmitterWork = msbHndl.GetParticleEmitter(m_Target.m_Param0);
                }
                break;
        }
    }

	public void Animate(ref MeigeAnimCtrl.TargetInstance tgtCache, float frame) {
        int idx = 0;
        for (int i = 0; i < m_Curves.Length; i++) {
            for (int j = 0; j < m_Curves[i].m_ComponentCurves.Length; j++) {
                MabCurve curve = m_Curves[i].m_ComponentCurves[j];
                float value = curve.CalcValue(frame);
                switch (m_Target.m_TargetType) {
                    case eAnimTargetType.eAnimTarget_MatColor:
                        if (tgtCache.m_MsbMatWork != null) {
                            tgtCache.m_MsbMatWork.m_Diffuse[idx] = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexCoverageUV:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_CoverageUV[idx] = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexTranslationUV:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_TranslationUV[idx] = idx == 1 ? -value : value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexRotateUV:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_RotateUV = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexOffsetUV:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_OffsetUV[idx] = idx == 1 ? -value : value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_FocalLength:
                        if (tgtCache.m_MsbCameraWork != null) {
                            tgtCache.m_MsbCameraWork.m_FocalLength = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshVisibility:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_bVisibility = (int)value == 1;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_PEActive:
                        if (tgtCache.m_MeigeParticleEmitterWork != null) {
                            tgtCache.m_MeigeParticleEmitterWork.isActive = (int)value == 1;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshColor:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_MeshColor[idx] = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MatColor_R:
                        if (tgtCache.m_MsbMatWork != null) {
                            tgtCache.m_MsbMatWork.m_Diffuse.r = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MatColor_G:
                        if (tgtCache.m_MsbMatWork != null) {
                            tgtCache.m_MsbMatWork.m_Diffuse.g = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MatColor_B:
                        if (tgtCache.m_MsbMatWork != null) {
                            tgtCache.m_MsbMatWork.m_Diffuse.b = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MatColor_A:
                        if (tgtCache.m_MsbMatWork != null) {
                            tgtCache.m_MsbMatWork.m_Diffuse.a = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexCoverageUV_U:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_CoverageUV.x = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexCoverageUV_V:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_CoverageUV.y = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexTranslationUV_U:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_TranslationUV.x = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexTranslationUV_V:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_TranslationUV.y = -value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexOffsetUV_U:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_OffsetUV.x = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_TexOffsetUV_V:
                        if (tgtCache.m_MsbTextureWork != null) {
                            tgtCache.m_MsbTextureWork.m_OffsetUV.y = -value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshColor_R:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_MeshColor.r = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshColor_G:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_MeshColor.g = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshColor_B:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_MeshColor.b = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_MeshColor_A:
                        if (tgtCache.m_MsbObjWork != null) {
                            tgtCache.m_MsbObjWork.m_MeshColor.a = value;
                        }
                        break;
                    case eAnimTargetType.eAnimTarget_CamOrthographicSize:
                        if (tgtCache.m_MsbCameraWork != null) {
                            tgtCache.m_MsbCameraWork.m_OrthographicsSize = value;
                        }
                        break;
                }
                idx++;
            }
        }
    }

	[Serializable]
	public class MabKeyData {
		public eAnimControlType m_CtrlType;
		public float m_Frame;
		public float m_Value;
		public float m_LeftDerivative;
		public float m_RightDerivative;
	}

	[Serializable]
	public class MabCurve {
        protected int m_ProcessIdx;

        public MabKeyData[] m_KeyDatas;

        private void UpdateProcessIndex(float keyframe) {
            float frame = m_KeyDatas[m_KeyDatas.Length - 1].m_Frame;
            while (keyframe < 0f) {
                keyframe += frame;
            }
            while (keyframe > frame) {
                keyframe -= frame;
            }
            int newIdx = m_ProcessIdx;
            if (m_KeyDatas[m_ProcessIdx].m_Frame > keyframe) {
                for (newIdx = m_ProcessIdx - 1; newIdx > 0; newIdx--) {
                    if (m_KeyDatas[newIdx].m_Frame <= keyframe) {
                        break;
                    }
                }
            } else if (m_KeyDatas[m_ProcessIdx + 1].m_Frame < keyframe) {
                for (newIdx = m_ProcessIdx + 1; newIdx < m_KeyDatas.Length - 1; newIdx++) {
                    if (m_KeyDatas[newIdx].m_Frame > keyframe) {
                        break;
                    }
                }
                newIdx -= 1;
            }
            m_ProcessIdx = newIdx;
        }

		public float CalcValue(float keyframe) {
            if (m_KeyDatas.Length <= 1) {
                return m_KeyDatas[0].m_Value;
            }
            UpdateProcessIndex(keyframe);
            MabKeyData currKey = m_KeyDatas[m_ProcessIdx];
			if (currKey.m_CtrlType == eAnimControlType.eAnimControl_Bool || currKey.m_CtrlType == eAnimControlType.eAnimControl_Constant) {
				return currKey.m_Value;
			}
            MabKeyData nextKey = m_KeyDatas[m_ProcessIdx + 1];
            float t = (keyframe - currKey.m_Frame) / (nextKey.m_Frame - currKey.m_Frame);
            float result;
            if (currKey.m_CtrlType == eAnimControlType.eAnimControl_Linear) {
                result = Mathf.Lerp(currKey.m_Value, nextKey.m_Value, t);
            } else {
                result = MathFunc.Hermite_CalcValue(currKey.m_Value, currKey.m_RightDerivative, nextKey.m_Value, nextKey.m_LeftDerivative, t);
            }
            return result;
		}
	}

	[Serializable]
	public class AnimNodeCurve {
		public MabCurve[] m_ComponentCurves;
	}

    [Serializable]
    public class AnimTargetInfo {
		public eAnimTargetType m_TargetType;
		public string m_TargetName;
		public int m_RefID;
		public int m_Param0;
		public int m_Param1;
		public int m_Param2;
	}
}
