﻿using System;

[Serializable]
public class MabHandler {
	public string m_Name;
	public int m_NumOfKeyframe;
	public int m_BaseFPS;
	public float m_AnimTimeBySec;

	public MabAnimNodeHandler[] m_AnimNodeHandlerArray;
	public MabAnimEvent[] m_AnimEvArray;

	[Serializable]
	public class MabAnimEvent {
		public float m_Frame;
		public int[] m_iParam = new int[4];
	}
}
