﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200001C RID: 28
[Token(Token = "0x2000015")]
[StructLayout(3)]
public class MakeEncrypt : MonoBehaviour
{
	// Token: 0x06000057 RID: 87 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600004B")]
	[Address(RVA = "0x101101090", Offset = "0x1101090", VA = "0x101101090")]
	public MakeEncrypt()
	{
	}

	// Token: 0x040000CF RID: 207
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000085")]
	public int m_BaseStringNum;

	// Token: 0x040000D0 RID: 208
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000086")]
	public string[] m_BaseStrings;
}
