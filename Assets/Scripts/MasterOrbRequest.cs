﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using MasterOrbRequestTypes;
using Meige;

// Token: 0x0200006E RID: 110
[Token(Token = "0x2000047")]
[StructLayout(3)]
public static class MasterOrbRequest
{
	// Token: 0x0600025D RID: 605 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000216")]
	[Address(RVA = "0x101101098", Offset = "0x1101098", VA = "0x101101098")]
	public static MeigewwwParam Getall(Getall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600025E RID: 606 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000217")]
	[Address(RVA = "0x101101124", Offset = "0x1101124", VA = "0x101101124")]
	public static MeigewwwParam Getall([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600025F RID: 607 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000218")]
	[Address(RVA = "0x1011011B0", Offset = "0x11011B0", VA = "0x1011011B0")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000260 RID: 608 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000219")]
	[Address(RVA = "0x101101280", Offset = "0x1101280", VA = "0x101101280")]
	public static MeigewwwParam Get(string masterOrbId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
