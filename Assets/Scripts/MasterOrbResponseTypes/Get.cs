﻿using CommonResponseTypes;
using WWWTypes;

namespace MasterOrbResponseTypes {
    public class Get : CommonResponse {
        public MasterOrb[] masterOrbs;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (masterOrbs == null) ? string.Empty : masterOrbs.ToString();
            return str;
        }
    }
}
