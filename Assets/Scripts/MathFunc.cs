﻿using System;
using UnityEngine;

public static class MathFunc {
    public static System.Random rand;

    public static void InitRandom() {
        rand = new System.Random(Environment.TickCount);
    }

    public static float RandomValue() {
        return UnityEngine.Random.value;
    }

    public static int RandomRange(int min, int max) {
        return UnityEngine.Random.Range(min, max + 1);
    }

    public static float RandomRange(float min, float max) {
        return UnityEngine.Random.Range(min, max);
    }

    public static float RandomValueToOtherThread() {
        return (float)rand.NextDouble();
    }

    public static int RandomRangeToOtherThread(int min, int max) {
        return rand.Next() % (max - min + 1) + min;
    }

    public static Vector2 Normalize(ref Vector2 v) {
        float mag = Magnitude(ref v);
        if (mag == 0f) { return Vector2.zero; }
        v /= mag;
        return v;
    }

    public static Vector3 Normalize(ref Vector3 v) {
        float mag = Magnitude(ref v);
        if (mag == 0f) { return Vector3.zero; }
        v /= mag;
        return v;
    }

    public static float Magnitude(ref Vector2 v) {
        return Mathf.Sqrt(SqMagnitude(ref v));
    }

    public static float Magnitude(ref Vector3 v) {
        return Mathf.Sqrt(SqMagnitude(ref v));
    }

    public static float SqMagnitude(ref Vector2 v) {
        return Dot(ref v, ref v);
    }

    public static float SqMagnitude(ref Vector3 v) {
        return Dot(ref v, ref v);
    }

    public static float Distance(ref Vector2 v0, ref Vector2 v1) {
        Vector2 v = v0 - v1;
        return Magnitude(ref v);
    }

    public static float Distance(ref Vector3 v0, ref Vector3 v1) {
        Vector2 v = v0 - v1;
        return Magnitude(ref v);
    }

    public static float SqDistance(ref Vector2 v0, ref Vector2 v1) {
        Vector2 v = v0 - v1;
        return Dot(ref v, ref v);
    }

    public static float SqDistance(ref Vector3 v0, ref Vector3 v1) {
        Vector2 v = v0 - v1;
        return Dot(ref v, ref v);
    }

    public static float Dot(ref Vector2 v0, ref Vector2 v1) {
        return v0.x * v1.x + v0.y * v1.y;
    }

    public static float Dot(ref Vector3 v0, ref Vector3 v1) {
        return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
    }

    public static Vector2 Lerp(ref Vector2 src, ref Vector2 dst, float s) {
        return (dst - src) * s + src;
    }

    public static Vector3 Lerp(ref Vector3 src, ref Vector3 dst, float s) {
        return (dst - src) * s + src;
    }

    public static float PingPong(float t, float len) {
        float factor = t / len;
        int integer = (int)factor;
        float decimals = factor - integer;
        if (integer % 2 == 0) {
            return Mathf.Lerp(0f, len, decimals);
        }
        return Mathf.Lerp(0f, len, 1f - decimals);
    }

    public static float Angle(ref Vector2 dir0, ref Vector2 dir1) {
        float c = Dot(ref dir0, ref dir1) / (Magnitude(ref dir0) * Magnitude(ref dir1));
        float angle = Mathf.Acos(Mathf.Clamp(c, -1f, 1f));
        return float.IsNaN(angle) ? 0f : angle;
    }

    public static float AngleDeg(ref Vector2 dir0, ref Vector2 dir1) {
        return Angle(ref dir0, ref dir1) * Mathf.Rad2Deg;
    }

    public static float Angle(ref Vector3 dir0, ref Vector3 dir1) {
        float c = Dot(ref dir0, ref dir1) / (Magnitude(ref dir0) * Magnitude(ref dir1));
        float angle = Mathf.Acos(Mathf.Clamp(c, -1f, 1f));
        return float.IsNaN(angle) ? 0f : angle;
    }

    public static float AngleDeg(ref Vector3 dir0, ref Vector3 dir1) {
        return Angle(ref dir0, ref dir1) * Mathf.Rad2Deg;
    }

    public static float AngleFast(ref Vector2 nDir0, ref Vector2 nDir1) {
        float c = Dot(ref nDir0, ref nDir1);
        float angle = Mathf.Acos(Mathf.Clamp(c, -1f, 1f));
        return float.IsNaN(angle) ? 0f : angle;
    }

    public static float AngleDegFast(ref Vector2 nDir0, ref Vector2 nDir1) {
        return AngleFast(ref nDir0, ref nDir1) * Mathf.Rad2Deg;
    }

    public static float AngleFast(ref Vector3 nDir0, ref Vector3 nDir1) {
        float c = Dot(ref nDir0, ref nDir1);
        float angle = Mathf.Acos(Mathf.Clamp(c, -1f, 1f));
        return float.IsNaN(angle) ? 0f : angle;
    }

    public static float AngleDegFast(ref Vector3 nDir0, ref Vector3 nDir1) {
        return AngleFast(ref nDir0, ref nDir1) * Mathf.Rad2Deg;
    }

    public static float AngleY(ref Vector3 dir) {
        return Mathf.Atan2(dir.x, dir.z);
    }

    public static float AngleDegY(ref Vector3 dir) {
        return Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
    }

    public static float DegToRad(float deg) {
        while (deg > 180f) {
            deg -= 360f;
        }
        while (deg < -180f) {
            deg += 360f;
        }
        return deg / 180f * 3.1415927f;
    }

    public static void CalcDigits(int cnt, out int digitCnt, ref int[] digits) {
        int i = 0;
        while (cnt > 0) {
            digits[i] = cnt % 10;
            cnt /= 10;
            i++;
        }
        digitCnt = i;
        while (i < digits.Length) {
            digits[i] = 0;
            i++;
        }
    }

    public static void Swap(ref float f1, ref float f2) {
        float tmp = f1;
        f1 = f2;
        f2 = tmp;
    }

    public static void Swap(ref int n1, ref int n2) {
        int tmp = n1;
        n1 = n2;
        n2 = tmp;
    }

    public static void CatmullRom(out float Result, float[] Source, float Rate) {
        CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
    }

    public static void CatmullRom(out float Result, float Source0, float Source1, float Source2, float Source3, float Rate) {
        float rateSqr = Rate * Rate;
        float rateCub = rateSqr * Rate;
        float f0 = 2f * rateSqr - rateCub - Rate;
        float f1 = 3f * rateCub - 5f * rateSqr + 2f;
        float f2 = 4f * rateSqr - 3f * rateCub + Rate;
        float f3 = rateCub - rateSqr;
        Result = (f0 * Source0 + f1 * Source1 + f2 * Source2 + f3 * Source3) * 0.5f;
    }

    public static void CatmullRom(out Vector2 Result, Vector2[] Source, float Rate) {
        CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
    }

    public static void CatmullRom(out Vector2 Result, Vector2 Source0, Vector2 Source1, Vector2 Source2, Vector2 Source3, float Rate) {
        float rateSqr = Rate * Rate;
        float rateCub = rateSqr * Rate;
        float f0 = 2f * rateSqr - rateCub - Rate;
        float f1 = 3f * rateCub - 5f * rateSqr + 2f;
        float f2 = 4f * rateSqr - 3f * rateCub + Rate;
        float f3 = rateCub - rateSqr;
        Result.x = (f0 * Source0.x + f1 * Source1.x + f2 * Source2.x + f3 * Source3.x) * 0.5f;
        Result.y = (f0 * Source0.y + f1 * Source1.y + f2 * Source2.y + f3 * Source3.y) * 0.5f;
    }

    public static void CatmullRom(out Vector3 Result, Vector3[] Source, float Rate) {
        CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
    }

    public static void CatmullRom(out Vector3 Result, Vector3 Source0, Vector3 Source1, Vector3 Source2, Vector3 Source3, float Rate) {
        float rateSqr = Rate * Rate;
        float rateCub = rateSqr * Rate;
        float f0 = 2f * rateSqr - rateCub - Rate;
        float f1 = 3f * rateCub - 5f * rateSqr + 2f;
        float f2 = 4f * rateSqr - 3f * rateCub + Rate;
        float f3 = rateCub - rateSqr;
        Result.x = (f0 * Source0.x + f1 * Source1.x + f2 * Source2.x + f3 * Source3.x) * 0.5f;
        Result.y = (f0 * Source0.y + f1 * Source1.y + f2 * Source2.y + f3 * Source3.y) * 0.5f;
        Result.z = (f0 * Source0.z + f1 * Source1.z + f2 * Source2.z + f3 * Source3.z) * 0.5f;
    }

    public static void CatmullRom(out Vector4 Result, Vector4[] Source, float Rate) {
        CatmullRom(out Result, Source[0], Source[1], Source[2], Source[3], Rate);
    }

    public static void CatmullRom(out Vector4 Result, Vector4 Source0, Vector4 Source1, Vector4 Source2, Vector4 Source3, float Rate) {
        float rateSqr = Rate * Rate;
        float rateCub = rateSqr * Rate;
        float f0 = 2f * rateSqr - rateCub - Rate;
        float f1 = 3f * rateCub - 5f * rateSqr + 2f;
        float f2 = 4f * rateSqr - 3f * rateCub + Rate;
        float f3 = rateCub - rateSqr;
        Result.x = (f0 * Source0.x + f1 * Source1.x + f2 * Source2.x + f3 * Source3.x) * 0.5f;
        Result.y = (f0 * Source0.y + f1 * Source1.y + f2 * Source2.y + f3 * Source3.y) * 0.5f;
        Result.z = (f0 * Source0.z + f1 * Source1.z + f2 * Source2.z + f3 * Source3.z) * 0.5f;
        Result.w = (f0 * Source0.w + f1 * Source1.w + f2 * Source2.w + f3 * Source3.w) * 0.5f;
    }

    public static Vector2 Bezier(Vector2 st, Vector2 ed, Vector2 pt, float t) {
        Vector2 result;
        float inv = 1f - t;
        result.x = st.x * inv * inv + 2f * pt.x * t * inv + ed.x * t * t;
        result.y = st.y * inv * inv + 2f * pt.y * t * inv + ed.y * t * t;
        return result;
    }

    public static Vector3 Bezier(Vector3 st, Vector3 ed, Vector3 pt, float t) {
        Vector3 result;
        float inv = 1f - t;
        result.x = st.x * inv * inv + 2f * pt.x * t * inv + ed.x * t * t;
        result.y = st.y * inv * inv + 2f * pt.y * t * inv + ed.y * t * t;
        result.z = st.z * inv * inv + 2f * pt.z * t * inv + ed.z * t * t;
        return result;
    }

    public static Vector2 BezierTangent(Vector2 st, Vector2 ed, Vector2 pt, float t) {
        Vector2 result;
        result.x = 2f * st.x * (1f - t) + pt.x * (1f - 2f * t) + 2f * ed.x * t;
        result.y = 2f * st.y * (1f - t) + pt.y * (1f - 2f * t) + 2f * ed.y * t;
        return result;
    }

    public static Vector2 Bezier(Vector2 st, Vector2 ed, Vector2 pt, Vector2 pt2, float t) {
        Vector2 result;
        float inv = 1f - t;
        result.x = st.x * inv * inv * inv + 3f * pt.x * t * inv * inv + 3f * pt2.x * t * t * inv + ed.x * t * t * t;
        result.y = st.y * inv * inv * inv + 3f * pt.y * t * inv * inv + 3f * pt2.y * t * t * inv + ed.y * t * t * t;
        return result;
    }

    public static Vector3 Bezier(Vector3 st, Vector3 ed, Vector3 pt, Vector3 pt2, float t) {
        Vector3 result;
        float inv = 1f - t;
        result.x = st.x * inv * inv * inv + 3f * pt.x * t * inv * inv + 3f * pt2.x * t * t * inv + ed.x * t * t * t;
        result.y = st.y * inv * inv * inv + 3f * pt.y * t * inv * inv + 3f * pt2.y * t * t * inv + ed.y * t * t * t;
        result.z = st.z * inv * inv * inv + 3f * pt.z * t * inv * inv + 3f * pt2.z * t * t * inv + ed.z * t * t * t;
        return result;
    }

    public static float Hermite_CalcValue(float p0, float d0, float p1, float d1, float t) {
        float tSqr = t * t;
        float tCub = tSqr * t;
        float f0 = 2f * tCub - 3f * tSqr + 1f;
        float f1 = 3f * tSqr - 2f * tCub;
        float f2 = tCub - 2f * tSqr + t;
        float f3 = tCub - tSqr;
        return f0 * p0 + f1 * p1 + f2 * d0 + f3 * d1;
    }

    public static Vector2 Hermite_CalcValue(Vector2 p0, Vector2 d0, Vector2 p1, Vector2 d1, float t) {
        float tSqr = t * t;
        float tCub = tSqr * t;
        float f0 = 2f * tCub - 3f * tSqr + 1f;
        float f1 = 3f * tSqr - 2f * tCub;
        float f2 = tCub - 2f * tSqr + t;
        float f3 = tCub - tSqr;
        return p0 * f0 + p1 * f1 + d0 * f2 + d1 * f3;
    }

    public static Vector3 Hermite_CalcValue(Vector3 p0, Vector3 d0, Vector3 p1, Vector3 d1, float t) {
        float tSqr = t * t;
        float tCub = tSqr * t;
        float f0 = 2f * tCub - 3f * tSqr + 1f;
        float f1 = 3f * tSqr - 2f * tCub;
        float f2 = tCub - 2f * tSqr + t;
        float f3 = tCub - tSqr;
        return p0 * f0 + p1 * f1 + d0 * f2 + d1 * f3;
    }

    public static Vector4 Hermite_CalcValue_V4(Vector4 p0, Vector4 d0, Vector4 p1, Vector4 d1, float t) {
        float tSqr = t * t;
        float tCub = tSqr * t;
        float f0 = 2f * tCub - 3f * tSqr + 1f;
        float f1 = 3f * tSqr - 2f * tCub;
        float f2 = tCub - 2f * tSqr + t;
        float f3 = tCub - tSqr;
        float x = f0 * p0.x + f1 * p1.x + f2 * d0.x + f3 * d1.x;
        float y = f0 * p0.y + f1 * p1.y + f2 * d0.y + f3 * d1.y;
        float z = f0 * p0.z + f1 * p1.z + f2 * d0.z + f3 * d1.z;
        float w = f0 * p0.w + f1 * p1.w + f2 * d0.w + f3 * d1.w;
        return new Vector4(x, y, z, w);
    }

    public static Quaternion QuaternionFromMatrix(Matrix4x4 mat) {
        return Quaternion.LookRotation(mat.GetColumn(2), mat.GetColumn(1));
    }

    public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint) {
        intersection = Vector3.zero;
        float fSegment = Vector3.Dot(planePoint - linePoint, planeNormal);
        float fLine = Vector3.Dot(lineVec, planeNormal);
        if (fLine != 0f) {
            intersection = linePoint + Vector3.Normalize(lineVec) * fSegment / fLine;
            return true;
        }
        return false;
    }

    public static bool IsLineInRectangle(Vector3 linePoint1, Vector3 linePoint2, Vector3 rectA, Vector3 rectB, Vector3 rectC, Vector3 rectD) {
        if (!IsPointInRectangle(linePoint1, rectA, rectC, rectB, rectD)) {
            if (!IsPointInRectangle(linePoint2, rectA, rectC, rectB, rectD)) {
                bool f0 = AreLineSegmentsCrossing(linePoint1, linePoint2, rectA, rectB);
                bool f1 = AreLineSegmentsCrossing(linePoint1, linePoint2, rectB, rectC);
                bool f2 = AreLineSegmentsCrossing(linePoint1, linePoint2, rectC, rectD);
                bool f3 = AreLineSegmentsCrossing(linePoint1, linePoint2, rectD, rectA);
                return f0 || f1 || f2 || f3;
            }
        }
        return true;
    }

    public static Vector3 AddVectorLength(Vector3 vector, float size) {
        float mag = Vector3.Magnitude(vector);
        return (mag + size) / mag * vector;
    }

    public static bool IsPointInRectangle(Vector3 point, Vector3 rectA, Vector3 rectC, Vector3 rectB, Vector3 rectD) {
        Vector3 sideAC = rectC - rectA;
        Vector3 linePoint = rectA + AddVectorLength(sideAC, -(sideAC.magnitude / 2f));
        Vector3 sideAB = rectB - rectA;
        Vector3 sideAD = rectD - rectA;
        Vector3 proj = ProjectPointOnLine(linePoint, sideAB.normalized, point);
        float projABMag = (proj - point).magnitude;
        proj = ProjectPointOnLine(linePoint, sideAD.normalized, point);
        float projADMag = (proj - point).magnitude;
        return projADMag <= sideAB.magnitude / 2f && projABMag <= sideAD.magnitude / 2f;
    }

    public static Vector3 ProjectPointOnLine(Vector3 linePoint, Vector3 lineVec, Vector3 point) {
        return linePoint + lineVec * Vector3.Dot(point - linePoint, lineVec);
    }

    public static bool AreLineSegmentsCrossing(Vector3 pointA1, Vector3 pointA2, Vector3 pointB1, Vector3 pointB2) {
        Vector3 dirA = (pointA2 - pointA1).normalized;
        Vector3 dirB = (pointB2 - pointB1).normalized;
        if (ClosestPointsOnTwoLines(out Vector3 point1, out Vector3 point2, pointA1, dirA, pointB1, dirB)) {
            int side1 = PointOnWhichSideOfLineSegment(pointA1, pointA2, point1);
            int side2 = PointOnWhichSideOfLineSegment(pointB1, pointB2, point2);
            return side1 == 0 && side2 == 0;
        }
        return false;
    }

    public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2) {
        closestPointLine1 = Vector3.zero;
        closestPointLine2 = Vector3.zero;
        float dot11 = Vector3.Dot(lineVec1, lineVec1);
        float dot12 = Vector3.Dot(lineVec1, lineVec2);
        float dot22 = Vector3.Dot(lineVec2, lineVec2);
        float sqrDiff = dot11 * dot22 - dot12 * dot12;
        if (sqrDiff != 0f) {
            Vector3 diff = linePoint1 - linePoint2;
            float diff1 = Vector3.Dot(lineVec1, diff);
            float diff2 = Vector3.Dot(lineVec2, diff);
            float f1 = (dot12 * diff2 - diff1 * dot22) / sqrDiff;
            float f2 = (dot11 * diff2 - diff1 * dot12) / sqrDiff;
            closestPointLine1 = linePoint1 + lineVec1 * f1;
            closestPointLine2 = linePoint2 + lineVec2 * f2;
            return true;
        }
        return false;
    }

    public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point) {
        Vector3 rhs = linePoint2 - linePoint1;
        Vector3 lhs = point - linePoint1;
        if (Vector3.Dot(lhs, rhs) <= 0f) {
            return 1;
        }
        if (lhs.magnitude <= rhs.magnitude) {
            return 0;
        }
        return 2;
    }
}
