﻿namespace Meige {
    public class AssetBundleNameUtility {
        public static string Encode(string str) {
            str = str.Replace(".muast", string.Empty);
            return AlphaEncode(str) + ".muast";
        }

        public static string Decode(string str) {
            str = str.Replace(".muast", string.Empty);
            return AlphaDecode(str) + ".muast";
        }

        private static string AlphaEncode(string str) {
            return str;
        }

        private static string AlphaDecode(string str) {
            return str;
        }
    }
}
