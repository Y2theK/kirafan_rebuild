﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige.AssetBundles
{
	// Token: 0x02001346 RID: 4934
	[Token(Token = "0x2000CC9")]
	[StructLayout(3)]
	public class AssetBundleCryptUtility
	{
		// Token: 0x06006477 RID: 25719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005C2A")]
		[Address(RVA = "0x1011042C8", Offset = "0x11042C8", VA = "0x1011042C8")]
		public AssetBundleCryptUtility()
		{
		}

		// Token: 0x04007573 RID: 30067
		[Token(Token = "0x4005305")]
		public const int FileHeaderLength = 7;
	}
}
