﻿using System.IO;
using System.Security.Cryptography;

namespace Meige.AssetBundles {
    public class AssetBundleCryptography {
        public void EncryptAes(byte[] src, byte[] key, byte[] pw, out byte[] dst) {
            dst = null;

            using (RijndaelManaged rijndaelManaged = new RijndaelManaged()) {
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.KeySize = 256;
                rijndaelManaged.BlockSize = 128;

                using (var transform = rijndaelManaged.CreateEncryptor(key, pw))
                using (var stream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(src, 0, src.Length);
                    cryptoStream.FlushFinalBlock();
                    dst = stream.ToArray();
                }
            }
        }

        public void DecryptAes(byte[] src, byte[] key, byte[] pw, out byte[] dst) {
            dst = new byte[src.Length];

            using (RijndaelManaged rijndaelManaged = new RijndaelManaged()) {
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.KeySize = 256;
                rijndaelManaged.BlockSize = 128;

                using (var transform = rijndaelManaged.CreateDecryptor(key, pw))
                using (var stream = new MemoryStream(src))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read)) {
                    cryptoStream.Read(dst, 0, dst.Length);
                }
            }
        }
    }
}
