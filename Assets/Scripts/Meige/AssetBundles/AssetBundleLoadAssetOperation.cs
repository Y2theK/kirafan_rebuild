﻿using System;

namespace Meige.AssetBundles {
    public abstract class AssetBundleLoadAssetOperation : AssetBundleLoadOperation {
        protected string m_AssetName;
        protected Type m_Type;

        public string assetName => m_AssetName;

        public abstract T GetAsset<T>() where T : UnityEngine.Object;
        public abstract T[] GetAllAsset<T>() where T : UnityEngine.Object;
    }
}
