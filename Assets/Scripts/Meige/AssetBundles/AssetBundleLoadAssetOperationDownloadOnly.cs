﻿namespace Meige.AssetBundles {
    public class AssetBundleLoadAssetOperationDownloadOnly : AssetBundleLoadOperation {
        protected bool m_isDownloaded;

        public AssetBundleLoadAssetOperationDownloadOnly(string bundleName) {
            m_AssetBundleName = bundleName;
        }

        public override bool Update() {
            if (m_isDownloaded) {
                if (callback != null) {
                    MoveNext();
                }
                return false;
            }
            m_isDownloaded = AssetBundleManager.GetLoadedAssetBundleFlag(m_AssetBundleName, out m_DownloadingError);
            if (m_isDownloaded) {
                return false;
            }
            if (m_DownloadingError != ErrorType.None) {
                MoveNext();
                return false;
            }
            return true;
        }

        public override bool IsDone() {
            if (m_DownloadingError != ErrorType.None) {
                Debug.Log(m_DownloadingError + " : " + m_AssetBundleName);
                return true;
            }
            return m_isDownloaded;
        }
    }
}
