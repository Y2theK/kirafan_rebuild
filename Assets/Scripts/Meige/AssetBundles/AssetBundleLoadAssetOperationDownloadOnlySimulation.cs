﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige.AssetBundles
{
	// Token: 0x0200134E RID: 4942
	[Token(Token = "0x2000CD1")]
	[StructLayout(3)]
	public class AssetBundleLoadAssetOperationDownloadOnlySimulation : AssetBundleLoadAssetOperation
	{
		// Token: 0x0600649D RID: 25757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005C50")]
		[Address(RVA = "0x1012353A8", Offset = "0x12353A8", VA = "0x1012353A8")]
		public AssetBundleLoadAssetOperationDownloadOnlySimulation(string bundleName)
		{
		}

		// Token: 0x0600649E RID: 25758 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C51")]
		[Address(RVA = "0x102351344", Offset = "0x2351344", VA = "0x102351344", Slot = "9")]
		public override T GetAsset<T>()
		{
			return null;
		}

		// Token: 0x0600649F RID: 25759 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C52")]
		[Address(RVA = "0x1023938A0", Offset = "0x23938A0", VA = "0x1023938A0", Slot = "10")]
		public override T[] GetAllAsset<T>()
		{
			return null;
		}

		// Token: 0x060064A0 RID: 25760 RVA: 0x00021558 File Offset: 0x0001F758
		[Token(Token = "0x6005C53")]
		[Address(RVA = "0x1012353B0", Offset = "0x12353B0", VA = "0x1012353B0", Slot = "7")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x060064A1 RID: 25761 RVA: 0x00021570 File Offset: 0x0001F770
		[Token(Token = "0x6005C54")]
		[Address(RVA = "0x1012353C8", Offset = "0x12353C8", VA = "0x1012353C8", Slot = "8")]
		public override bool IsDone()
		{
			return default(bool);
		}
	}
}
