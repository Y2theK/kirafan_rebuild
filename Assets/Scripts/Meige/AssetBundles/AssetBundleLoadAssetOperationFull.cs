﻿using System;
using UnityEngine;

namespace Meige.AssetBundles {
    public class AssetBundleLoadAssetOperationFull : AssetBundleLoadAssetOperation {
        protected AssetBundleRequest m_Request;

        public Type GetObjectType() {
            return m_Type;
        }

        public AssetBundleLoadAssetOperationFull(string bundleName, string assetName, Type type) {
            m_AssetBundleName = bundleName;
            m_AssetName = assetName;
            m_Type = type;
        }

        public override T GetAsset<T>() {
            if (m_Request != null && m_Request.isDone) {
                return m_Request.asset as T;
            }
            return null;
        }

        public override T[] GetAllAsset<T>() {
            if (m_Request != null && m_Request.isDone) {
                T[] array = new T[m_Request.allAssets.Length];
                m_Request.allAssets.CopyTo(array, 0);
                return array;
            }
            return null;
        }

        public override bool Update() {
            if (m_Request != null) {
                MoveNext();
                return callback != null && !m_Request.isDone;
            }
            LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (loadedAssetBundle != null) {
                if (string.IsNullOrEmpty(m_AssetName)) {
                    m_Request = loadedAssetBundle.m_AssetBundle.LoadAllAssetsAsync();
                } else {
                    m_Request = loadedAssetBundle.m_AssetBundle.LoadAssetAsync(m_AssetName, m_Type);
                }
                return callback != null;
            }
            if (m_DownloadingError != ErrorType.None) {
                MoveNext();
                return false;
            }
            return true;
        }

        public override bool IsDone() {
            if (m_Request == null && m_DownloadingError != ErrorType.None) {
                Debug.Log(m_DownloadingError + " : " + m_AssetBundleName);
                return true;
            }
            return m_Request != null && m_Request.isDone;
        }
    }
}
