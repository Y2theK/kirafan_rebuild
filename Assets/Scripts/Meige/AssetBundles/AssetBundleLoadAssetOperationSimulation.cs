﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige.AssetBundles
{
	// Token: 0x0200134A RID: 4938
	[Token(Token = "0x2000CCD")]
	[StructLayout(3)]
	public class AssetBundleLoadAssetOperationSimulation : AssetBundleLoadAssetOperation
	{
		// Token: 0x0600648E RID: 25742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005C41")]
		[Address(RVA = "0x1012358AC", Offset = "0x12358AC", VA = "0x1012358AC")]
		public AssetBundleLoadAssetOperationSimulation(string bundleName, string assetName, Type type)
		{
		}

		// Token: 0x0600648F RID: 25743 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C42")]
		[Address(RVA = "0x102351474", Offset = "0x2351474", VA = "0x102351474", Slot = "9")]
		public override T GetAsset<T>()
		{
			return null;
		}

		// Token: 0x06006490 RID: 25744 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C43")]
		[Address(RVA = "0x102393980", Offset = "0x2393980", VA = "0x102393980", Slot = "10")]
		public override T[] GetAllAsset<T>()
		{
			return null;
		}

		// Token: 0x06006491 RID: 25745 RVA: 0x000214C8 File Offset: 0x0001F6C8
		[Token(Token = "0x6005C44")]
		[Address(RVA = "0x1012358B4", Offset = "0x12358B4", VA = "0x1012358B4", Slot = "7")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x06006492 RID: 25746 RVA: 0x000214E0 File Offset: 0x0001F6E0
		[Token(Token = "0x6005C45")]
		[Address(RVA = "0x1012358CC", Offset = "0x12358CC", VA = "0x1012358CC", Slot = "8")]
		public override bool IsDone()
		{
			return default(bool);
		}

		// Token: 0x0400757D RID: 30077
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400530F")]
		private UnityEngine.Object[] m_SimulatedObjects;
	}
}
