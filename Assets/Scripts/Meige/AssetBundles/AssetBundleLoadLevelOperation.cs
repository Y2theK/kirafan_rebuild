﻿using UnityEngine.SceneManagement;

namespace Meige.AssetBundles {
    public class AssetBundleLoadLevelOperation : AssetBundleLoadOperation {
        protected string m_LevelName;
        protected bool m_IsAdditive;
        protected bool m_allowSceneActivation;

        public string GetLevelName() {
            return m_LevelName;
        }

        public bool IsAdditive() {
            return m_IsAdditive;
        }

        public bool allowSceneActivation() {
            return m_allowSceneActivation;
        }

        public AssetBundleLoadLevelOperation(string assetbundleName, string levelName, bool isAdditive, bool allowSceneActivation) {
            m_AssetBundleName = assetbundleName;
            m_LevelName = levelName;
            m_IsAdditive = isAdditive;
            m_allowSceneActivation = allowSceneActivation;
        }

        public override bool Update() {
            if (m_asyncOperation != null) {
                return false;
            }
            LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (loadedAssetBundle == null) {
                return true;
            }
            m_asyncOperation = SceneManager.LoadSceneAsync(m_LevelName, m_IsAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            m_asyncOperation.allowSceneActivation = m_allowSceneActivation;
            return false;
        }

        public override bool IsDone() {
            if (m_asyncOperation == null) {
                if (m_DownloadingError != ErrorType.None) {
                    Debug.Log(m_DownloadingError + " : " + m_AssetBundleName);
                    return true;
                }
                return false;
            }
            if (m_asyncOperation.allowSceneActivation) {
                return m_asyncOperation.isDone;
            }
            return m_asyncOperation.progress >= 0.9f;
        }
    }
}
