﻿using System;
using System.Collections;
using UnityEngine;

namespace Meige.AssetBundles {
    public abstract class AssetBundleLoadOperation : IEnumerator {
        protected string m_AssetBundleName;
        protected ErrorType m_DownloadingError;
        protected AsyncOperation m_asyncOperation;

        public Action<AssetBundleLoadOperation> callback { get; set; }
        public string assetBundleName => m_AssetBundleName;
        public AsyncOperation asyncOperation => m_asyncOperation;
        public object Current => null;

        public ErrorType GetError() {
            return m_DownloadingError;
        }

        public bool IsError() {
            return m_DownloadingError != ErrorType.None;
        }

        public bool MoveNext() {
            if (!IsDone()) {
                return true;
            }
            callback?.Invoke(this);
            return false;
        }

        public void Reset() { }

        public abstract bool Update();
        public abstract bool IsDone();
    }
}
