﻿using System;

namespace Meige.AssetBundles {
    public class AssetBundleLoadSubAssetOperationFull : AssetBundleLoadAssetOperationFull {
        public AssetBundleLoadSubAssetOperationFull(string bundleName, string assetName, Type type) : base(bundleName, assetName, type) { }

        public override bool Update() {
            if (m_Request != null) {
                MoveNext();
                return callback != null && !m_Request.isDone;
            }
            LoadedAssetBundle loadedAssetBundle = AssetBundleManager.GetLoadedAssetBundle(m_AssetBundleName, out m_DownloadingError);
            if (loadedAssetBundle != null) {
                if (string.IsNullOrEmpty(m_AssetName)) {
                    m_Request = loadedAssetBundle.m_AssetBundle.LoadAllAssetsAsync();
                } else {
                    m_Request = loadedAssetBundle.m_AssetBundle.LoadAssetWithSubAssetsAsync(m_AssetName, m_Type);
                }
                return callback != null;
            }
            return true;
        }
    }
}
