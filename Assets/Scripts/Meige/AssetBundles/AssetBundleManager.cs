﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Meige.AssetBundles {
    public class AssetBundleManager : SingletonMonoBehaviour<AssetBundleManager> {
        private const int MANIFEST_RETRY_COUNT = 5;
        private const int MAX_POOL_AB_VERSION_CACHE_DIRTY_COUNT = 64;

        private static LogMode m_LogMode = LogMode.All;
        private static string m_BaseDownloadingURL = string.Empty;
        private static string[] m_ActiveVariants = new string[0];
        private static AssetBundleManifest m_AssetBundleManifest;
        private static Dictionary<string, Utility.AssetBundleVersionParam> m_IndividualVersion;
        private static Dictionary<string, Utility.AssetBundleVersionParam> m_StreamingAssetsVersion;
        private static bool m_manifestUpdateError;
        private static int m_manifestUpdateErrorCount;
        private static Dictionary<string, LoadedAssetBundle> m_LoadedAssetBundles = new Dictionary<string, LoadedAssetBundle>();
        private static Dictionary<string, bool> m_AssetBundleFlags = new Dictionary<string, bool>();
        private static Dictionary<string, AssetBundleWWW> m_DownloadingWWWs = new Dictionary<string, AssetBundleWWW>();
        private static Dictionary<string, ErrorType> m_DownloadingErrors = new Dictionary<string, ErrorType>();
        private static List<AssetBundleLoadOperation> m_InProgressOperations = new List<AssetBundleLoadOperation>();
        private static Dictionary<string, int> m_ReferencedCounts = new Dictionary<string, int>();
        private static Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]>();

        private AssetBundleVersionCache m_AssetBundleVersionCache = new AssetBundleVersionCache();
        private bool m_isUpdate;

        public AssetBundleVersionCache assetBundleVersionCache => m_AssetBundleVersionCache;
        public static Dictionary<string, AssetBundleWWW> downloadingWWWs => m_DownloadingWWWs;
        public static bool manifestUpdateError => m_manifestUpdateError;
        public static bool isReady { get; private set; }

        public static LogMode logMode {
            get => m_LogMode;
            set => m_LogMode = value;
        }

        public static string BaseDownloadingURL {
            get => m_BaseDownloadingURL;
            set => m_BaseDownloadingURL = value;
        }

        public static string[] ActiveVariants {
            get => m_ActiveVariants;
            set => m_ActiveVariants = value;
        }

        public static AssetBundleManifest AssetBundleManifestObject {
            set => m_AssetBundleManifest = value;
        }

        public static float timeoutSec {
            get => AssetBundleWWW.timeOutSec;
            set => AssetBundleWWW.timeOutSec = value;
        }

        public static void RestartManifestUpdate() {
            m_manifestUpdateError = false;
            m_manifestUpdateErrorCount = 0;
        }

        private static void Log(LogType logType, string text) {
            if (logType == LogType.Info && m_LogMode == LogMode.All) {
                Debug.Log(text);
            } else if (logType == LogType.Warning && m_LogMode == LogMode.All) {
                Debug.LogWarning(text);
            } else if (logType == LogType.Error && (m_LogMode == LogMode.All || m_LogMode == LogMode.JustErrors)) {
                Debug.LogError(text);
            }
        }

        public static void SetIsReady(bool ready) {
            isReady = ready;
        }

        public static string GetBundleVersion(string assetBundleName, bool isStreamingAssets = false) {
            var param = GetBundleVersionParam(assetBundleName, isStreamingAssets);
            return param != null ? param.version : string.Empty;
        }

        public static uint GetBundleCRC(string assetBundleName, bool isStreamingAssets = false) {
            var param = GetBundleVersionParam(assetBundleName, isStreamingAssets);
            return param != null ? param.crc : 0U;
        }

        public static long GetBundleSize(string assetBundleName, bool isStreamingAssets = false) {
            var param = GetBundleVersionParam(assetBundleName, isStreamingAssets);
            return param != null ? param.size : 0L;
        }

        public static Utility.AssetBundleVersionParam GetBundleVersionParam(string assetBundleName, bool isStreamingAssets = false) {
            Utility.AssetBundleVersionParam param = null;
            if (isStreamingAssets && m_StreamingAssetsVersion != null) {
                m_StreamingAssetsVersion.TryGetValue(assetBundleName, out param);
            } else if (m_IndividualVersion != null) {
                m_IndividualVersion.TryGetValue(assetBundleName, out param);
            }
            return param;
        }

        private static string GetStreamingAssetsPath() {
            string dir;
            if (Application.isEditor) {
                dir = Environment.CurrentDirectory.Replace(@"\", "/");
            } else {
                if (Application.isMobilePlatform || Application.isConsolePlatform) {
                    return Application.streamingAssetsPath;
                }
                dir = Application.streamingAssetsPath;
            }
            return $"file://{dir}";
        }

        public static void SetSourceAssetBundleDirectory(string relativePath) {
            BaseDownloadingURL = GetStreamingAssetsPath() + relativePath;
        }

        public static void SetSourceAssetBundleURL(string absolutePath) {
            BaseDownloadingURL = absolutePath + Utility.GetPlatformName() + "/";
        }

        public static void SetDevelopmentAssetBundleServer(string _url) {
            TextAsset textAsset = Resources.Load<TextAsset>("AssetBundleServerURL");
            string text = textAsset == null ? null : textAsset.text.Trim();
            if (text == null || text.Length == 0) {
                SetSourceAssetBundleURL(_url);
            } else {
                SetSourceAssetBundleURL(text);
            }
        }

        public static LoadedAssetBundle GetLoadedAssetBundle(string assetBundleName, out ErrorType error) {
            if (m_DownloadingErrors.TryGetValue(assetBundleName, out error)) {
                return null;
            }
            bool downloading = false;
            m_LoadedAssetBundles.TryGetValue(assetBundleName, out LoadedAssetBundle assetBundle);
            if (assetBundle == null && m_DownloadingWWWs.ContainsKey(assetBundleName)) {
                downloading = true;
            }
            if (!m_Dependencies.TryGetValue(assetBundleName, out string[] dependencies)) {
                return assetBundle;
            }
            foreach (string dependency in dependencies) {
                if (m_DownloadingErrors.TryGetValue(dependency, out error)) {
                    return null;
                }
                m_LoadedAssetBundles.TryGetValue(dependency, out LoadedAssetBundle dependencyBundle);
                if (dependencyBundle == null && m_DownloadingWWWs.ContainsKey(dependency)) {
                    downloading = true;
                }
            }
            if (downloading) {
                error = ErrorType.None;
                return null;
            }
            return assetBundle;
        }

        public static bool GetLoadedAssetBundleFlag(string assetBundleName, out ErrorType error) {
            if (m_DownloadingErrors.TryGetValue(assetBundleName, out error)) {
                return false;
            }
            bool downloading = false;
            m_AssetBundleFlags.TryGetValue(assetBundleName, out bool bundleFlag);
            if (!bundleFlag) {
                if (m_DownloadingWWWs.ContainsKey(assetBundleName)) {
                }
                return false;
            }
            if (!m_Dependencies.TryGetValue(assetBundleName, out string[] dependencies)) {
                return true;
            }
            foreach (string dependency in dependencies) {
                if (m_DownloadingErrors.TryGetValue(dependency, out error)) {
                    return false;
                }
                m_AssetBundleFlags.TryGetValue(dependency, out bundleFlag);
                if (!bundleFlag) {
                    if (m_DownloadingWWWs.ContainsKey(dependency)) {
                    }
                    return false;
                }
            }
            if (downloading) {
                error = ErrorType.None;
                return false;
            }
            return true;
        }

        public static void Initialize(string assetServerURL) {
            if (isReady) { return; }
            SetSourceAssetBundleURL(assetServerURL);
            Instance.StartCoroutine(nameof(UpdateManifest));
        }

        public IEnumerator UpdateManifest() {
            if (m_isUpdate) {
                while (m_isUpdate) {
                    yield return null;
                }
                yield break;
            }
            m_isUpdate = true;
            isReady = false;
            string manifestAssetBundleName = Utility.GetPlatformName();
            string url = Utility.GetStreamingAssetURL(MeigeAssetBundlesUtilityExt.ToBundleHashName(Utility.VersionFileName));
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.timeout = (int)timeoutSec;
            yield return www.SendWebRequest();
            if (string.IsNullOrEmpty(www.error)) {
                Utility.AssetBundleVersion version = null;
                try {
                    string res = Encoding.UTF8.GetString(ZlibUtil.uncompress(www.downloadHandler.data));
                    version = JsonUtility.FromJson<Utility.AssetBundleVersion>(res);
                } catch { }
                if (version != null && version.list != null && version.list.Count > 0) {
                    if (m_StreamingAssetsVersion == null) {
                        m_StreamingAssetsVersion = new Dictionary<string, Utility.AssetBundleVersionParam>(version.list.Count);
                    }
                    m_StreamingAssetsVersion.Clear();
                    foreach (Utility.AssetBundleVersionParam param in version.list) {
                        m_StreamingAssetsVersion[param.name] = param;
                    }
                }
            }
            www.Dispose();
            while (true) {
                url = m_BaseDownloadingURL + MeigeAssetBundlesUtilityExt.ToBundleHashName(Utility.VersionFileName) + "?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
                www = UnityWebRequest.Get(url);
                www.timeout = (int)timeoutSec;
                yield return www.SendWebRequest();
                if (www != null && string.IsNullOrEmpty(www.error) && www.responseCode == 200L) {
                    Utility.AssetBundleVersion version = null;
                    try {
                        string res = Encoding.UTF8.GetString(ZlibUtil.uncompress(www.downloadHandler.data));
                        version = JsonUtility.FromJson<Utility.AssetBundleVersion>(res);
                    } catch { }
                    if (version != null && version.list != null && version.list.Count > 0) {
                        if (m_IndividualVersion == null) {
                            m_IndividualVersion = new Dictionary<string, Utility.AssetBundleVersionParam>(version.list.Count);
                        }
                        m_IndividualVersion.Clear();
                        foreach (Utility.AssetBundleVersionParam param in version.list) {
                            m_IndividualVersion[param.name] = param;
                        }
                        www.Dispose();
                        m_AssetBundleVersionCache.LoadVersionCacheFile();
                        m_AssetBundleVersionCache.RemoveDataNotIncludedList(version);
                        m_AssetBundleVersionCache.SaveVersionCacheFile();
                        break;
                    }
                }
                www.Dispose();
                m_manifestUpdateErrorCount++;
                if (m_manifestUpdateErrorCount >= MANIFEST_RETRY_COUNT) {
                    m_manifestUpdateError = true;
                    while (m_manifestUpdateError) {
                        yield return null;
                    }
                }
            }
            while (true) {
                LoadAssetBundle(manifestAssetBundleName, true);
                AssetBundleLoadManifestOperation operation = new AssetBundleLoadManifestOperation(manifestAssetBundleName, "AssetBundleManifest", typeof(AssetBundleManifest));
                m_InProgressOperations.Add(operation);
                yield return operation;
                UnloadAssetBundle(manifestAssetBundleName);
                UnloadErrors(manifestAssetBundleName);
                if (operation.GetError() == ErrorType.None) {
                    isReady = true;
                    m_isUpdate = false;
                    break;
                }
                m_manifestUpdateErrorCount++;
                if (m_manifestUpdateErrorCount >= MANIFEST_RETRY_COUNT) {
                    m_manifestUpdateError = true;
                    while (m_manifestUpdateError) {
                        yield return null;
                    }
                }
            }
        }

        protected static void LoadAssetBundle(string assetBundleName, bool isLoadingAssetBundleManifest = false, bool isDownloadOnly = false) {
            Log(LogType.Info, "Loading Asset Bundle " + (isLoadingAssetBundleManifest ? "Manifest: " : ": ") + assetBundleName);
            if (!isLoadingAssetBundleManifest && m_AssetBundleManifest == null) {
                Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
                return;
            }
            if (!instance.LoadAssetBundleInternal(assetBundleName, isLoadingAssetBundleManifest, isDownloadOnly) && !isLoadingAssetBundleManifest) {
                LoadDependencies(assetBundleName, isDownloadOnly);
            }
        }

        protected static string RemapVariantName(string assetBundleName) {
            if (!isReady) {
                Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
                Debug.Break();
                return null;
            }
            string[] allAssetBundlesWithVariant = m_AssetBundleManifest.GetAllAssetBundlesWithVariant();
            string[] nameParts = assetBundleName.Split(new[] { '.' });
            int num = int.MaxValue;
            int variantIndex = -1;
            for (int i = 0; i < allAssetBundlesWithVariant.Length; i++) {
                string[] variantParts = allAssetBundlesWithVariant[i].Split(new[] { '.' });
                if (nameParts[0] == variantParts[0]) {
                    int idx = Array.IndexOf(m_ActiveVariants, variantParts[1]);
                    if (idx == -1) {
                        idx = int.MaxValue - 1;
                    }
                    if (idx < num) {
                        num = idx;
                        variantIndex = i;
                    }
                }
            }
            if (num == int.MaxValue - 1) {
                Debug.LogWarning("Ambigious asset bundle variant chosen because there was no matching active variant: " + allAssetBundlesWithVariant[variantIndex]);
            }
            if (variantIndex != -1) {
                return allAssetBundlesWithVariant[variantIndex];
            }
            return assetBundleName;
        }

        protected bool LoadAssetBundleInternal(string assetBundleName, bool isLoadingAssetBundleManifest, bool isDownloadOnly = false) {
            if (!isDownloadOnly) {
                if (m_ReferencedCounts.ContainsKey(assetBundleName)) {
                    m_ReferencedCounts[assetBundleName]++;
                    return true;
                }
                m_ReferencedCounts.Add(assetBundleName, 1);
            }
            if (m_DownloadingWWWs.ContainsKey(assetBundleName)) {
                return true;
            }
            if (isDownloadOnly && !m_AssetBundleFlags.ContainsKey(assetBundleName)) {
                m_AssetBundleFlags.Add(assetBundleName, false);
            }
            AssetBundleWWW assetBundleWWW;
            if (isLoadingAssetBundleManifest) {
                assetBundleWWW = new AssetBundleWWW(m_BaseDownloadingURL, true, assetBundleName);
            } else {
                string encodedName = AssetBundleNameUtility.Encode(assetBundleName);
                assetBundleWWW = new AssetBundleWWW(m_BaseDownloadingURL, false, encodedName);
                assetBundleWWW.isDownloadOnly = isDownloadOnly;
            }
            StartCoroutine(assetBundleWWW.Update());
            m_DownloadingWWWs.Add(assetBundleName, assetBundleWWW);
            return false;
        }

        protected static void LoadDependencies(string assetBundleName, bool isDownloadOnly = false) {
            if (m_AssetBundleManifest == null) {
                Debug.LogError("Please initialize AssetBundleManifest by calling AssetBundleManager.Initialize()");
                return;
            }
            string[] allDependencies = m_AssetBundleManifest.GetAllDependencies(AssetBundleNameUtility.Encode(assetBundleName));
            if (allDependencies.Length == 0) { return; }
            for (int i = 0; i < allDependencies.Length; i++) {
                allDependencies[i] = AssetBundleNameUtility.Decode(allDependencies[i]);
            }
            for (int i = 0; i < allDependencies.Length; i++) {
                allDependencies[i] = RemapVariantName(allDependencies[i]);
            }
            if (!m_Dependencies.ContainsKey(assetBundleName)) {
                m_Dependencies.Add(assetBundleName, allDependencies);
            }
            for (int i = 0; i < allDependencies.Length; i++) {
                instance.LoadAssetBundleInternal(allDependencies[i], false, isDownloadOnly);
            }
        }

        public static void UnloadAssetBundle(string assetBundleName, bool doFlagUnload = true) {
            UnloadAssetBundleInternal(assetBundleName, doFlagUnload);
            UnloadDependencies(assetBundleName, doFlagUnload);
        }

        protected static void UnloadDependencies(string assetBundleName, bool doFlagUnload = true) {
            if (!m_Dependencies.TryGetValue(assetBundleName, out string[] dependencies)) {
                return;
            }
            foreach (string dependency in dependencies) {
                UnloadAssetBundleInternal(dependency, doFlagUnload);
            }
            m_Dependencies.Remove(assetBundleName);
        }

        protected static void UnloadErrors(string assetBundleName) {
            if (m_DownloadingErrors.ContainsKey(assetBundleName)) {
                m_DownloadingErrors.Remove(assetBundleName);
            }
        }

        public static void ClearFlags() {
            m_AssetBundleFlags.Clear();
        }

        protected static void UnloadFlag(string assetBundleName) {
            if (m_AssetBundleFlags.ContainsKey(assetBundleName)) {
                m_AssetBundleFlags.Remove(assetBundleName);
            }
        }

        protected static void UnloadAssetBundleInternal(string assetBundleName, bool doFlagUnload = true) {
            if (doFlagUnload) {
                UnloadFlag(assetBundleName);
            }
            int refs = 1;
            if (m_ReferencedCounts.ContainsKey(assetBundleName)) {
                m_ReferencedCounts[assetBundleName]--;
                refs = m_ReferencedCounts[assetBundleName];
            }
            LoadedAssetBundle loadedAssetBundle = GetLoadedAssetBundle(assetBundleName, out _);
            if (refs <= 0) {
                m_ReferencedCounts.Remove(assetBundleName);
                if (loadedAssetBundle != null) {
                    loadedAssetBundle.m_AssetBundle.Unload(false);
                }
                m_LoadedAssetBundles.Remove(assetBundleName);
                Log(LogType.Info, assetBundleName + " has been unloaded successfully");
            }
        }

        public static bool IsDependence(string assetBundleName) {
            string[] allDependencies = m_AssetBundleManifest.GetAllDependencies(assetBundleName);
            return allDependencies != null && allDependencies.Length > 0;
        }

        private void Update() {
            UpdateProcess();
        }

        private void UpdateProcess() {
            List<string> finishedWWW = new List<string>();
            foreach (string assetBundleName in m_DownloadingWWWs.Keys) {
                AssetBundleWWW www = m_DownloadingWWWs[assetBundleName];
                if (www.error != null) {
                    if (!m_DownloadingErrors.ContainsKey(assetBundleName)) {
                        if (www.error.Contains("Request timeout")) {
                            m_DownloadingErrors.Add(assetBundleName, ErrorType.Timeout);
                            Debug.LogWarning(string.Format("Timeout downloading bundle {0} from {1}: {2}", assetBundleName, www.url, www.error));
                        } else {
                            m_DownloadingErrors.Add(assetBundleName, ErrorType.Unknown);
                            Debug.LogWarning(string.Format("Failed downloading bundle {0} from {1}: {2}", assetBundleName, www.url, www.error));
                        }
                    }
                    finishedWWW.Add(assetBundleName);
                } else if (www.isDone) {
                    if (www.isDownloadOnly) {
                        if (m_AssetBundleFlags.ContainsKey(assetBundleName)) {
                            m_AssetBundleFlags[assetBundleName] = true;
                        }
                    } else {
                        if (www.assetBundle == null) {
                            m_DownloadingErrors.TryAdd(assetBundleName, ErrorType.Unknown);
                            Debug.LogWarning(string.Format("{0} is not a valid asset bundle.", assetBundleName));
                        } else {
                            m_LoadedAssetBundles.TryAdd(assetBundleName, new LoadedAssetBundle(www.assetBundle));
                        }
                    }
                    finishedWWW.Add(assetBundleName);
                }
            }
            foreach (string assetBundleName in finishedWWW) {
                AssetBundleWWW www = m_DownloadingWWWs[assetBundleName];
                StopCoroutine(www.Update());
                www.Dispose();
                m_DownloadingWWWs.Remove(assetBundleName);
            }
            if ((m_DownloadingWWWs.Count == 0 && m_AssetBundleVersionCache.dirtyCount > 0) || m_AssetBundleVersionCache.dirtyCount > MAX_POOL_AB_VERSION_CACHE_DIRTY_COUNT) {
                m_AssetBundleVersionCache.SaveVersionCacheFile();
            }
            int index = 0;
            while (index < m_InProgressOperations.Count) {
                if (!m_InProgressOperations[index].Update()) {
                    string assetBundleName = m_InProgressOperations[index].assetBundleName;
                    m_InProgressOperations.RemoveAt(index);
                    if (m_DownloadingErrors.ContainsKey(assetBundleName)) {
                        m_DownloadingErrors.Remove(assetBundleName);
                    }
                    if (m_Dependencies.TryGetValue(assetBundleName, out string[] dependencies)) {
                        foreach (string dependency in dependencies) {
                            if (m_DownloadingErrors.ContainsKey(dependency)) {
                                m_DownloadingErrors.Remove(dependency);
                            }
                        }
                    }
                } else {
                    index++;
                }
            }
        }

        public static AssetBundleLoadAssetOperation LoadAssetAsync(string assetBundleName, string assetName, Type type, Action<AssetBundleLoadOperation> callback = null) {
            Log(LogType.Info, string.Format("Loading {0} from {1} bundle", assetName, assetBundleName));
            assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName);
            AssetBundleLoadAssetOperation op = new AssetBundleLoadAssetOperationFull(assetBundleName, assetName, type);
            op.callback = callback;
            m_InProgressOperations.Add(op);
            return op;
        }

        public static AssetBundleLoadAssetOperation LoadSubAssetAsync(string assetBundleName, string assetName, Type type, Action<AssetBundleLoadOperation> callback = null) {
            Log(LogType.Info, string.Format("Loading {0} from {1} bundle", assetName, assetBundleName));
            assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName);
            AssetBundleLoadAssetOperation op = new AssetBundleLoadSubAssetOperationFull(assetBundleName, assetName, type);
            op.callback = callback;
            m_InProgressOperations.Add(op);
            return op;
        }

        public static AssetBundleLoadOperation LoadLevelAsync(string assetBundleName, string levelName, bool isAdditive, bool allowSceneActivation = true) {
            Log(LogType.Info, string.Format("Loading {0} from {1} bundle", levelName, assetBundleName));
            assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName);
            AssetBundleLoadOperation op = new AssetBundleLoadLevelOperation(assetBundleName, levelName, isAdditive, allowSceneActivation);
            m_InProgressOperations.Add(op);
            return op;
        }

        public static AssetBundleLoadOperation DownloadOnlyAsync(string assetBundleName, Action<AssetBundleLoadOperation> callback = null) {
            Log(LogType.Info, string.Format("Loading {0} bundle", assetBundleName));
            assetBundleName = RemapVariantName(assetBundleName);
            LoadAssetBundle(assetBundleName, isDownloadOnly: true);
            AssetBundleLoadOperation op = new AssetBundleLoadAssetOperationDownloadOnly(assetBundleName);
            op.callback = callback;
            m_InProgressOperations.Add(op);
            return op;
        }

        public static AssetBundleLoadOperation Retry(AssetBundleLoadOperation assetOperation) {
            string assetBundleName = assetOperation.assetBundleName;
            bool downloadOnly = assetOperation is AssetBundleLoadAssetOperationDownloadOnly; //what is the point of this
            AssetBundleLoadOperation assetBundleLoadOperation = null;
            if (assetOperation is AssetBundleLoadAssetOperationFull loadAssetOperation) {
                string assetName = loadAssetOperation.assetName;
                Type objectType = loadAssetOperation.GetObjectType();
                assetBundleName = RemapVariantName(assetBundleName);
                UnloadDependencies(assetBundleName, downloadOnly);
                UnloadErrors(assetBundleName);
                LoadAssetBundle(assetBundleName, false, false);
                assetBundleLoadOperation = new AssetBundleLoadAssetOperationFull(assetBundleName, assetName, objectType);
                assetBundleLoadOperation.callback = assetOperation.callback;
                m_InProgressOperations.Add(assetBundleLoadOperation);
            } else if (assetOperation is AssetBundleLoadSubAssetOperationFull loadSubAssetOperation) {
                string assetName = loadSubAssetOperation.assetName;
                Type objectType = loadSubAssetOperation.GetObjectType();
                assetBundleName = RemapVariantName(assetBundleName);
                UnloadDependencies(assetBundleName, downloadOnly);
                UnloadErrors(assetBundleName);
                LoadAssetBundle(assetBundleName, false, false);
                assetBundleLoadOperation = new AssetBundleLoadAssetOperationFull(assetBundleName, assetName, objectType);
                assetBundleLoadOperation.callback = assetOperation.callback;
                m_InProgressOperations.Add(assetBundleLoadOperation);
            } else if (assetOperation is AssetBundleLoadLevelOperation loadLevelOperation) {
                string levelName = loadLevelOperation.GetLevelName();
                bool isAdditive = loadLevelOperation.IsAdditive();
                bool allowSceneActivation = loadLevelOperation.allowSceneActivation();
                assetBundleName = RemapVariantName(assetBundleName);
                UnloadDependencies(assetBundleName, downloadOnly);
                UnloadErrors(assetBundleName);
                LoadAssetBundle(assetBundleName, false, false);
                assetBundleLoadOperation = new AssetBundleLoadLevelOperation(assetBundleName, levelName, isAdditive, allowSceneActivation);
                m_InProgressOperations.Add(assetBundleLoadOperation);
            }
            return assetBundleLoadOperation;
        }

        public List<string> RemakeListWithDependencies(List<string> list) {
            List<string> listWithDependencies = new List<string>();
            foreach (string assetBundleName in list) {
                string name = RemapVariantName(assetBundleName);
                if (!listWithDependencies.Contains(name)) {
                    listWithDependencies.Add(name);
                }
                string[] allDependencies = m_AssetBundleManifest.GetAllDependencies(AssetBundleNameUtility.Encode(name));
                foreach (string dependency in allDependencies) {
                    string dependencyName = RemapVariantName(AssetBundleNameUtility.Decode(dependency));
                    if (!listWithDependencies.Contains(dependencyName)) {
                        listWithDependencies.Add(dependencyName);
                    }
                }
            }
            return listWithDependencies;
        }

        public enum LogMode {
            All,
            JustErrors
        }

        public enum LogType {
            Info,
            Warning,
            Error
        }
    }
}
