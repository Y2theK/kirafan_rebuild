﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Meige.AssetBundles {
    public class AssetBundleVersionCache {
        private VersionCache m_VersionCache = new VersionCache();
        private uint m_DirtyCount;
        private object m_LockObj = new object();
        private bool m_ExistsCacheFile;

        public uint dirtyCount => m_DirtyCount;
        public bool existsCacheFile => m_ExistsCacheFile;

        public void Clear() {
            m_VersionCache.m_Dictionary.Clear();
        }

        private Utility.AssetBundleVersionParam SearchVersion(string name) {
            m_VersionCache.m_Dictionary.TryGetValue(name, out var versionParam);
            return versionParam;
        }

        public void SetVersion(string name, string version, uint crc, long size, bool dirtyAdd, bool isEncrypt) {
            lock (m_LockObj) {
                var param = new Utility.AssetBundleVersionParam() {
                    name = name,
                    version = version,
                    crc = crc,
                    size = size,
                    isEncrypt = isEncrypt
                };
                m_VersionCache.m_Dictionary.AddOrReplace(name, param);
                if (dirtyAdd) {
                    m_DirtyCount++;
                }
            }
        }

        public bool GetVersion(string name, out string version, out uint crc, out long size, out bool isEncrypt) {
            lock (m_LockObj) {
                var versionParam = SearchVersion(name);
                if (versionParam != null) {
                    version = versionParam.version;
                    crc = versionParam.crc;
                    size = versionParam.size;
                    isEncrypt = versionParam.isEncrypt;
                } else {
                    version = null;
                    crc = 0;
                    size = 0;
                    isEncrypt = false;
                }
                return versionParam != null;
            }
        }

        public bool RemoveVersion(string name) {
            lock (m_LockObj) {
                return m_VersionCache.m_Dictionary.Remove(name);
            }
        }

        public void RemoveDataNotIncludedList(Utility.AssetBundleVersion version) {
            List<string> deleteList = new List<string>();
            foreach (string key in m_VersionCache.m_Dictionary.Keys) {
                if (!version.list.Any((param) => param.name == key)) {
                    deleteList.Add(key);
                }
            }
            foreach (string key in deleteList) {
                m_VersionCache.m_Dictionary.Remove(key);
            }
        }

        private byte[] GetBase64Binary(string path) {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (BinaryReader reader = new BinaryReader(fs)) {
                return Convert.FromBase64String(reader.ReadString());
            }
        }

        private bool LoadVersionCacheFileInternal(string path) {
            if (!File.Exists(path)) { return false; }
            byte[] content = GetBase64Binary(path);
            if (content == null) { return false; }

            string version = null;
            using (var provider = new DESCryptoServiceProvider())
            using (var stream = new MemoryStream(content))
            using (var transform = provider.CreateDecryptor(Utility.VersionCache_key, Utility.VersionCache_iv))
            using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read))
            using (var sr = new StreamReader(cryptoStream)) {
                version = sr.ReadToEnd();
            }
            if (version != null) {
                VersionCache versionCache = JsonConvert.DeserializeObject<VersionCache>(version);
                if (versionCache.m_FileVersion == Utility.VersionCacheFileVersion) {
                    m_VersionCache = versionCache;
                    m_ExistsCacheFile = true;
                    m_DirtyCount = 0;
                    return true;
                }
            }
            return false;
        }

        public bool LoadVersionCacheFile() {
            lock (m_LockObj) {
                bool loaded = LoadVersionCacheFileInternal(Utility.VersionCachePath);
                if (!loaded) {
                    loaded = LoadVersionCacheFileInternal(Utility.VersionCachePathTmp);
                }
                return loaded;
            }
        }

        public bool SaveVersionCacheFile(bool isNew = true) {
            lock (m_LockObj) {
                m_VersionCache.m_FileVersion = Utility.VersionCacheFileVersion;
                if (File.Exists(Utility.VersionCachePath)) {
                    File.Copy(Utility.VersionCachePath, Utility.VersionCachePathTmp, true);
                }

                string result = null;
                using (var provider = new DESCryptoServiceProvider())
                using (var stream = new MemoryStream())
                using (var transform = provider.CreateEncryptor(Utility.VersionCache_key, Utility.VersionCache_iv))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
                using (var sw = new StreamWriter(cryptoStream)) {
                    sw.Write(JsonConvert.SerializeObject(m_VersionCache));
                    sw.Flush();
                    cryptoStream.FlushFinalBlock();
                    result = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length);
                }
                if (result != null) {
                    string dirName = Path.GetDirectoryName(Utility.VersionCachePath);
                    if (!Directory.Exists(dirName)) {
                        Directory.CreateDirectory(dirName);
                    }
                    using (FileStream fs = new FileStream(Utility.VersionCachePath, isNew ? FileMode.Create : FileMode.Open, FileAccess.Write))
                    using (BinaryWriter writer = new BinaryWriter(fs)) {
                        writer.Write(result);
                        m_ExistsCacheFile = true;
                        m_DirtyCount = 0;
                        return true;
                    }
                }
            }
            return false;
        }

        private class VersionCache {
            public string m_FileVersion;
            public Dictionary<string, Utility.AssetBundleVersionParam> m_Dictionary = new Dictionary<string, Utility.AssetBundleVersionParam>();
        }
    }
}
