﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Meige.AssetBundles {
    public class AssetBundleWWW : IDisposable {
        protected static float m_timeOut = 10f;
        protected static int m_currentLoadCount = 0;
        protected static int m_currentDownloadCount = 0;

        protected UnityWebRequest m_www;
        protected string m_url;
        protected bool m_isManifest;
        protected string m_assetBundleName;
        protected string m_assetBundleHashName;
        protected bool m_isDownloadOnly;
        protected AssetBundle m_assetBundle;
        protected bool m_isDone;
        protected string m_error;
        protected string m_md5;
        protected float m_oldProgress;
        protected float m_timeElapsed;
        protected bool m_isCurrentLoad;
        protected bool m_isCurrentDownload;
        protected bool m_isStreamingAssetsLoad;

        public string url => m_url;
        public bool isManifest => m_isManifest;
        public string assetBundleName => m_assetBundleName;
        public bool isDone => m_isDone;
        public bool isError => !string.IsNullOrEmpty(m_error);
        public string error => m_error;
        public AssetBundle assetBundle => m_assetBundle;
        public bool isStreamingAssetsLoad => m_isStreamingAssetsLoad;

        public static float timeOutSec {
            get => m_timeOut;
            set => m_timeOut = value;
        }

        public bool isDownloadOnly {
            get => m_isDownloadOnly;
            set => m_isDownloadOnly = value;
        }

        public float progress {
            get {
                if (m_www != null) {
                    return m_www.downloadProgress;
                }
                return 0f;
            }
        }

        public AssetBundleWWW(string url, bool isManifest, string assetBundleName = "") {
            Clear();
            m_assetBundleName = assetBundleName;
            m_assetBundleHashName = MeigeAssetBundlesUtilityExt.ToBundleHashName(assetBundleName);
            m_url = url + m_assetBundleHashName;
            m_isManifest = isManifest;
            m_oldProgress = 0;
            m_timeElapsed = 0;
        }

        ~AssetBundleWWW() {
            DecrementLoadCount();
            Dispose();
        }

        public void Dispose() {
            if (m_www == null) { return; }

            if (isError && m_assetBundle) {
                m_assetBundle.Unload(false);
            }
            m_www.Dispose();
            m_www = null;
        }

        protected void Clear() {
            m_assetBundle = null;
            m_isDone = false;
            m_isCurrentLoad = false;
            m_isCurrentDownload = false;
            m_isStreamingAssetsLoad = false;
            m_www = null;
            m_url = null;
            m_assetBundleName = null;
            m_assetBundleHashName = null;
            m_isDownloadOnly = false;
            m_error = null;
            m_md5 = null;
        }

        public IEnumerator Update() {
            if (m_isManifest) {
                yield return LoadManifest();
            } else {
                yield return LoadFile();
            }
        }

        protected IEnumerator LoadFile() {
            AssetbundleCache cache = new AssetbundleCache(m_assetBundleHashName);
            while (!AssetBundleManager.isReady || m_currentLoadCount >= ProjDepend.ASSETBUNDLE_MAX_LOADING_COUNT || MeigeResourceManager.isError) {
                yield return null;
            }
            m_currentLoadCount++;
            m_isCurrentLoad = true;
            cache.ExistAssetbundle();
            string filePath = null;
            string version = null;
            if (cache.isStreamingAssets) {
                string streamingVersion = AssetBundleManager.GetBundleVersion(m_assetBundleHashName, true);
                if (string.IsNullOrEmpty(streamingVersion)) {
                    yield return Job_GetMD5(cache.GetStreamingPath(), (md5) => streamingVersion = md5);
                }
                version = AssetBundleManager.GetBundleVersion(m_assetBundleHashName);
                if (string.IsNullOrEmpty(version) || version == streamingVersion) {
                    if (m_isDownloadOnly) {
                        m_isDone = true;
                        DecrementLoadCount();
                        yield break;
                    }
                    m_isStreamingAssetsLoad = true;
                    filePath = cache.GetStreamingPath();
                } else {
                    filePath = cache.GetFilePath();
                }
            } else {
                filePath = cache.GetFilePath();
            }
            bool isEncrypted = false;
            var param = AssetBundleManager.GetBundleVersionParam(m_assetBundleHashName, m_isStreamingAssetsLoad);
            if (param == null) {
                DecrementLoadCount();
                yield break;
            }
            isEncrypted = param.isEncrypt;
            if (cache.isExists || isStreamingAssetsLoad) {
                if (m_isDownloadOnly) {
                    m_isDone = true;
                    DecrementLoadCount();
                    yield break;
                }
                if (isEncrypted) {
                    yield return cache.LoadAssetbundle(m_isStreamingAssetsLoad);
                    if (cache.bytes != null) {
                        yield return Job(filePath, cache.bytes);
                    }
                } else {
                    yield return LoadFromFileAsync(filePath);
                }
                if (m_assetBundle != null) {
                    m_isDone = true;
                    DecrementLoadCount();
                    yield break;
                }
            }
            yield return cache.CleanCacheAsync();
            DecrementLoadCount();
            while (!AssetBundleManager.isReady || m_currentLoadCount >= ProjDepend.ASSETBUNDLE_MAX_LOADING_COUNT || MeigeResourceManager.isError) {
                yield return null;
            }
            m_currentDownloadCount++;
            m_isCurrentDownload = true;
            string url = m_url + "?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
            m_www = UnityWebRequest.Get(url);
            FileDownloadHandler downloadHandler = new FileDownloadHandler(filePath);
            m_www.downloadHandler = downloadHandler;
            if (m_www == null) {
                SetError("m_www is null.");
                yield break;
            }
            m_www.timeout = (int)timeOutSec;
            yield return m_www.SendWebRequest();
            if (m_www == null) {
                SetError("m_www is null.");
                yield break;
            }
            if (m_www.isNetworkError) {
                SetError("UnityWebRequest error : " + m_www.error);
                yield break;
            }
            long status = m_www.responseCode;
            if (status != 200L) {
                SetError("UnityWebRequest status : " + status);
                yield break;
            }
            if (!m_www.isDone) {
                m_isDone = true;
                DecrementLoadCount();
                yield break;
            }
            yield return Job_GetMD5(filePath);
            bool isUpdate = false;
            while (true) {
                version = AssetBundleManager.GetBundleVersion(m_assetBundleHashName);
                if (!string.IsNullOrEmpty(m_md5) && m_md5 == version) {
                    break;
                }
                if (isUpdate) {
                    SetError("Files of different versions." + m_md5 + " <>" + version);
                    yield break;
                }
                yield return AssetBundleManager.Instance.UpdateManifest();
                yield return new WaitForSeconds(0.5f);
                isUpdate = true;
            }
            cache.SetVersion(isEncrypted);
            bool hasCache = AssetBundleManager.Instance.assetBundleVersionCache.GetVersion(m_assetBundleHashName, out string cacheVersion, out _, out _, out _);
            if (!hasCache || (hasCache && cacheVersion != m_md5)) {
                AssetBundleManager.Instance.assetBundleVersionCache.SetVersion(m_assetBundleHashName, m_md5, 0, AssetBundleManager.GetBundleSize(m_assetBundleHashName), true, isEncrypted);
            }
            if (m_isDownloadOnly) {
                m_isDone = true;
                DecrementLoadCount();
                yield break;
            }
            if (isEncrypted) {
                yield return cache.LoadAssetbundle(m_isStreamingAssetsLoad);
                if (cache.bytes != null) {
                    yield return Job(filePath, cache.bytes);
                }
            } else {
                yield return LoadFromFileAsync(filePath);
            }
            if (m_assetBundle != null) {
                m_isDone = true;
                DecrementLoadCount();
                yield break;
            }
            yield return cache.CleanCacheAsync();
            SetError(string.Format("{0} is not a valid asset bundle.", m_assetBundleName));
        }

        protected IEnumerator LoadManifest() {
            string url = m_url + "?t=" + DateTime.Now.ToString("yyyyMMddHHmmss");
            uint crc = AssetBundleManager.GetBundleCRC(m_assetBundleHashName, false);
            m_www = UnityWebRequestAssetBundle.GetAssetBundle(url, crc);
            m_www.timeout = (int)timeOutSec;
            if (m_www == null) {
                SetError("m_www is null.");
                yield break;
            }
            yield return m_www.SendWebRequest();
            if (m_www == null) {
                SetError("m_www is null.");
                yield break;
            }
            if (m_www.isNetworkError) {
                SetError("UnityWebRequest error : " + m_www.error);
                yield break;
            }
            long status = m_www.responseCode;
            if (status != 200L) {
                SetError("UnityWebRequest status : " + status);
                yield break;
            }
            if (m_www.isDone) {
                m_assetBundle = ((DownloadHandlerAssetBundle)m_www.downloadHandler).assetBundle;
                m_isDone = true;
                DecrementLoadCount();
            }
        }

        protected void SetError(string error) {
            m_error = error;
            m_isDone = true;
            DecrementLoadCount();
        }

        protected void DecrementLoadCount() {
            if (m_isCurrentLoad) {
                m_currentLoadCount--;
                m_isCurrentLoad = false;
            }
            if (m_isCurrentDownload) {
                m_currentDownloadCount--;
                m_isCurrentDownload = false;
            }
        }

        protected IEnumerator LoadFromFileAsync(string path) {
            uint crc = AssetBundleManager.GetBundleCRC(m_assetBundleHashName, isStreamingAssetsLoad);
            AssetBundleCreateRequest req = AssetBundle.LoadFromFileAsync(path, crc);
            yield return req;
            m_assetBundle = req.assetBundle;
        }

        protected IEnumerator LoadFromMemoryAsync(byte[] bytes) {
            if (bytes != null && bytes.Length > 0 && new Utility().IsAssetbundle(bytes)) {
                uint crc = AssetBundleManager.GetBundleCRC(m_assetBundleHashName, isStreamingAssetsLoad);
                AssetBundleCreateRequest req = AssetBundle.LoadFromMemoryAsync(bytes, crc);
                yield return req;
                m_assetBundle = req.assetBundle;
            }
        }

        protected IEnumerator Job_GetMD5(string path) {
            yield return Job_GetMD5(path, (md5) => m_md5 = md5);
        }

        protected IEnumerator Job_GetMD5(string path, Action<string> setResultCallback) {
            JobParam_GetMD5 param = new JobParam_GetMD5() {
                path = path
            };
            Job job = new Job();
            job.SetFunction(DoWorkJob_GetMD5, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            setResultCallback.Invoke((job.GetObject() as JobParam_GetMD5).md5);
        }

        private void DoWorkJob_GetMD5(object obj) {
            JobParam_GetMD5 param = obj as JobParam_GetMD5;
            param.md5 = new Utility().GetMD5(param.path);
        }

        protected IEnumerator Job(string filePath, byte[] src) {
            JobParam param = new JobParam() {
                filePath = filePath,
                src = src
            };
            Job job = new Job();
            job.SetFunction(DoWorkJob, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            if (!job.IsError()) {
                yield return LoadFromMemoryAsync((job.GetObject() as JobParam).dst);
            }
        }

        private void DoWorkJob(object obj) {
            JobParam param = obj as JobParam;
            new AssetBundleCrypter().DecryptAsset(param.filePath, param.src, out param.dst);
        }

        private class JobParam_GetMD5 {
            public string path;
            public string md5;
        }

        private class JobParam {
            public string filePath;
            public byte[] src;
            public byte[] dst;
        }
    }
}
