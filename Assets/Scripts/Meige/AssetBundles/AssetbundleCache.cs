﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Meige.AssetBundles {
    public class AssetbundleCache {
        private const string PREFS_VERSION_KEY = "AssetbundleVersion_";

        private static List<string> m_accessLists = new List<string>();

        private string m_fileName;
        private byte[] m_bytes;
        private bool m_isEncrypted;
        private bool m_isExists;
        private bool m_isStreamingAssets;

        public byte[] bytes => m_bytes;
        public bool isEncrypted => m_isEncrypted;
        public bool isExists => m_isExists;
        public bool isStreamingAssets => m_isStreamingAssets;

        public AssetbundleCache(string fileName) {
            m_fileName = fileName;
        }

        ~AssetbundleCache() {
            m_bytes = null;
        }

        public IEnumerator CleanCacheAsync() {
            if (!string.IsNullOrEmpty(m_fileName)) {
                DeleteVersion(m_fileName);
            }
            string path = GetFilePath();
            if (!Directory.Exists(path) && !File.Exists(path)) {
                yield break;
            }
            CleanCacheAsync_Param param = new CleanCacheAsync_Param() {
                Path = path
            };
            Job job = new Job();
            job.SetFunction(DoWork_CleanCacheAsync, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            if (job.IsError()) {
                Debug.LogWarning("AssetbundleCache.CleanCacheAsync job is error");
            }
        }

        private void DoWork_CleanCacheAsync(object obj) {
            CleanCacheAsync_Param param = obj as CleanCacheAsync_Param;
            if (Directory.Exists(param.Path)) {
                Directory.Delete(param.Path, true);
                while (Directory.Exists(param.Path)) ;
            } else if (File.Exists(param.Path)) {
                File.Delete(param.Path);
                while (File.Exists(param.Path)) ;
            }
        }

        public static bool CleanCache(string bundleName = "") {
            try {
                if (string.IsNullOrEmpty(bundleName)) {
                    DirectoryInfo directoryInfo = new DirectoryInfo(Utility.SavePath);
                    CleanCacheProc(directoryInfo);
                } else {
                    string path = Utility.GetFilePath(bundleName);
                    if (Directory.Exists(path)) {
                        Directory.Delete(path);
                    } else if (File.Exists(path)) {
                        File.Delete(path);
                    }
                    DeleteVersion(bundleName);
                }
            } catch (Exception ex) {
                Debug.LogWarning("AssetbundleCache.CleanCache Exception : " + ex.ToString());
                return false;
            }
            return true;
        }

        public static bool CleanCacheByDirectoryName(string directoryName) {
            try {
                CleanCacheProc(new DirectoryInfo(directoryName));
            } catch (Exception ex) {
                Debug.LogWarning("AssetbundleCache.CleanCacheByDirectoryName Exception : " + ex.ToString());
                return false;
            }
            return true;
        }

        private static void CleanCacheProc(DirectoryInfo dirInfo, string path = "") {
            if (!dirInfo.Exists) { return; }
            foreach (FileInfo file in dirInfo.GetFiles()) {
                DeleteVersion(Path.Combine(path, file.Name));
                file.Delete();
            }
            foreach (DirectoryInfo directory in dirInfo.GetDirectories()) {
                CleanCacheProc(directory, Path.Combine(path, directory.Name));
            }
        }

        public void ExistAssetbundle() {
            m_isExists = false;
            if (File.Exists(GetStreamingPath())) {
                m_isStreamingAssets = true;
            }
            string path = GetFilePath();
            if (File.Exists(path)) {
                FileInfo file = new FileInfo(path);
                long size = AssetBundleManager.GetBundleSize(m_fileName);
                if (size > 0 && size == file.Length) {
                    m_isExists = true;
                    return;
                }
                CleanCache(m_fileName);
            }
        }

        public IEnumerator GetIsEncrypted(bool isStreamingAssetsLoad) {
            string path = isStreamingAssetsLoad ? GetStreamingPath() : GetFilePath();
            if (!File.Exists(path)) {
                yield break;
            }
            while (m_accessLists.Contains(m_fileName)) {
                yield return null;
            }
            m_accessLists.Add(m_fileName);
            GetIsEncryptedParam param = new GetIsEncryptedParam() {
                savePath = path,
            };
            Job job = new Job();
            job.SetFunction(DoWork_GetIsEncrypted, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            m_accessLists.Remove(m_fileName);
            m_isEncrypted = (job.GetObject() as GetIsEncryptedParam).isEncrypted;
        }

        private void DoWork_GetIsEncrypted(object obj) {
            GetIsEncryptedParam param = obj as GetIsEncryptedParam;
            using (FileStream stream = new FileStream(param.savePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                byte[] bytes = new byte[Utility.AssetBundleFileHeader];
                stream.Read(bytes, 0, bytes.Length);
                param.isEncrypted = new Utility().IsAssetbundle(bytes);
            }
        }

        public IEnumerator LoadAssetbundle(bool isStreamingAssetsLoad) {
            bool exists = isStreamingAssetsLoad ? m_isStreamingAssets : m_isExists;
            if (!exists) {
                yield return null;
            }
            string path = isStreamingAssetsLoad ? GetStreamingPath() : GetFilePath();
            if (!File.Exists(path)) {
                DeleteVersion(m_fileName);
                yield break;
            }
            while (m_accessLists.Contains(m_fileName)) {
                yield return null;
            }
            m_accessLists.Add(m_fileName);
            LoadAssetbundleParam param = new LoadAssetbundleParam() {
                savePath = path,
            };
            Job job = new Job();
            job.SetFunction(DoWork_LoadAssetbundle, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            m_accessLists.Remove(m_fileName);
            m_bytes = (job.GetObject() as LoadAssetbundleParam).dst;
        }

        private void DoWork_LoadAssetbundle(object obj) {
            LoadAssetbundleParam param = obj as LoadAssetbundleParam;
            using (FileStream fileStream = new FileStream(param.savePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                param.dst = new byte[fileStream.Length];
                fileStream.Read(param.dst, 0, param.dst.Length);
            }
        }

        public IEnumerator SaveAssetbundle(byte[] bytes) {
            string path = GetFilePath();
            string dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir)) {
                Directory.CreateDirectory(dir);
            }
            while (m_accessLists.Contains(m_fileName)) {
                yield return null;
            }
            m_accessLists.Add(m_fileName);
            SaveAssetbundleParam param = new SaveAssetbundleParam() {
                savePath = path,
                bytes = bytes
            };
            Job job = new Job();
            job.SetFunction(DoWork_SaveAssetbundle, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            m_accessLists.Remove(m_fileName);
        }

        private void DoWork_SaveAssetbundle(object obj) {
            SaveAssetbundleParam param = obj as SaveAssetbundleParam;
            using (FileStream fileStream = new FileStream(param.savePath, FileMode.Create, FileAccess.Write, FileShare.Write)) {
                fileStream.Write(param.bytes, 0, param.bytes.Length);
            }
        }

        public string GetFilePath() {
            return Utility.GetFilePath(m_fileName);
        }

        public string GetStreamingPath() {
            return Utility.GetStreamingPath(m_fileName);
        }

        public void SetVersion(bool isEncrypted) {
            m_isEncrypted = isEncrypted;
        }

        private static void DeleteVersion(string fileName) {
            string key = PREFS_VERSION_KEY + fileName;
            if (PreviewLabs.PlayerPrefs.HasKey(key)) {
                PreviewLabs.PlayerPrefs.DeleteKey(key);
            }
        }

        private class CleanCacheAsync_Param {
            public string Path { get; set; }
        }

        private class GetIsEncryptedParam {
            public string savePath;
            public bool isEncrypted;
        }

        private class LoadAssetbundleParam {
            public string savePath;
            public byte[] dst;
        }

        private class SaveAssetbundleParam {
            public string savePath;
            public byte[] bytes;
        }
    }
}
