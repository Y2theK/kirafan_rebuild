﻿namespace Meige.AssetBundles {
    public enum ErrorType {
        None,
        Timeout,
        Unknown
    }
}
