﻿using System.IO;
using UnityEngine.Networking;

namespace Meige.AssetBundles {
    internal class FileDownloadHandler : DownloadHandlerScript {
        private FileStream m_fs;
        private int m_offset;
        private int m_length;
        private byte[] m_headers;
        private int m_headerLength;
        private bool m_isAssetBundle;

        public bool isAssetBundle => m_isAssetBundle;

        public FileDownloadHandler(string path) {
            string dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir)) {
                Directory.CreateDirectory(dir);
            }
            if (File.Exists(path)) {
                File.Delete(path);
            }
            m_fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.Delete);
            m_headers = new byte[7];
        }

        ~FileDownloadHandler() {
            Dispose();
        }

        public new void Dispose() {
            if (m_fs != null) {
                m_fs.Close();
                m_fs.Dispose();
                m_fs = null;
            }
            base.Dispose();
        }

        protected override bool ReceiveData(byte[] data, int dataLength) {
            if (m_fs != null) {
                m_fs.Write(data, 0, dataLength);
            }
            m_offset += dataLength;
            if (m_headers != null && m_headerLength != m_headers.Length) {
                for (int i = 0; i < m_headers.Length && i < data.Length; i++) {
                    m_headers[m_headerLength] = data[i];
                    m_headerLength++;
                }
            }
            return true;
        }

        protected override void CompleteContent() {
            if (m_fs != null) {
                m_fs.Flush();
                m_fs.Close();
                m_fs = null;
            }
            if (m_headers != null) {
                m_isAssetBundle = new Utility().IsAssetbundle(m_headers);
            }
        }

        protected override void ReceiveContentLength(int contentLength) {
            m_length = contentLength;
        }

        protected override float GetProgress() {
            if (m_length != 0) {
                return (float)m_offset / m_length;
            }
            return 0f;
        }
    }
}
