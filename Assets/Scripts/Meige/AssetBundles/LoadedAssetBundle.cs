﻿using UnityEngine;

namespace Meige.AssetBundles {
    public class LoadedAssetBundle {
        public AssetBundle m_AssetBundle;

        public LoadedAssetBundle(AssetBundle assetBundle) {
            m_AssetBundle = assetBundle;
        }
    }
}
