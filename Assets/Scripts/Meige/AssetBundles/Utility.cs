﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Meige.AssetBundles {
    public class Utility {
        public const string AssetBundlesOutputPath = "AssetBundles";
        public const string VersionFileName = "v.b";
        public const int AssetBundleFileHeader = 7;
        public const int AssetBundleSystemVersion = 4;
        public const int AssetBundleEncryptVersion = 7;
        public const int AssetBundleEncryptRangePercentIni = 1;
        public const int AssetBundleEncryptRangePercentFin = 100;

        public static readonly string SavePath = Path.Combine(Application.persistentDataPath, "a.b2");
        public static readonly string VersionCachePathBase = Path.Combine(Application.persistentDataPath, "vc2");
        public static readonly string VersionCachePath = Path.Combine(VersionCachePathBase, "vc.b");
        public static readonly string VersionCachePathTmp = Path.Combine(VersionCachePathBase, "vc2.b2");
        public static readonly string VersionCacheFileVersion = "0.2";

        public static byte[] VersionCache_key = Encoding.ASCII.GetBytes("abvk" + SystemInfo.deviceUniqueIdentifier.Substring(0, 4));
        public static byte[] VersionCache_iv = Encoding.ASCII.GetBytes("abvi" + SystemInfo.deviceUniqueIdentifier.Substring(0, 4));
        public static byte[] HashPathSrc = new byte[256] {
            0x75, 0x35, 0x32, 0x4d, 0x4e, 0x77, 0x44, 0x73, 0x59, 0x66, 0x73, 0x7a, 0x4d, 0x72, 0x50, 0x47,
            0x32, 0x44, 0x66, 0x56, 0x50, 0x6d, 0x64, 0x4d, 0x34, 0x41, 0x37, 0x5a, 0x37, 0x39, 0x5a, 0x35,
            0x65, 0x47, 0x64, 0x6e, 0x50, 0x32, 0x61, 0x43, 0x32, 0x52, 0x34, 0x56, 0x65, 0x37, 0x65, 0x45,
            0x47, 0x4d, 0x58, 0x67, 0x6d, 0x4a, 0x36, 0x59, 0x66, 0x69, 0x62, 0x55, 0x74, 0x36, 0x62, 0x55,
            0x7a, 0x48, 0x4b, 0x46, 0x48, 0x48, 0x53, 0x32, 0x62, 0x64, 0x32, 0x39, 0x4d, 0x38, 0x50, 0x6e,
            0x47, 0x75, 0x50, 0x63, 0x44, 0x62, 0x65, 0x56, 0x6d, 0x46, 0x5a, 0x37, 0x51, 0x5a, 0x44, 0x77,
            0x74, 0x73, 0x75, 0x75, 0x58, 0x79, 0x66, 0x79, 0x67, 0x59, 0x32, 0x4e, 0x43, 0x4b, 0x47, 0x43,
            0x51, 0x63, 0x37, 0x62, 0x53, 0x65, 0x43, 0x52, 0x55, 0x52, 0x64, 0x66, 0x52, 0x56, 0x33, 0x4d,
            0x56, 0x64, 0x79, 0x37, 0x4e, 0x55, 0x73, 0x6b, 0x32, 0x7a, 0x72, 0x58, 0x52, 0x58, 0x33, 0x70,
            0x52, 0x6d, 0x65, 0x5a, 0x51, 0x38, 0x79, 0x43, 0x7a, 0x73, 0x50, 0x38, 0x51, 0x43, 0x43, 0x57,
            0x48, 0x42, 0x50, 0x6a, 0x67, 0x33, 0x44, 0x4c, 0x79, 0x6b, 0x41, 0x51, 0x65, 0x70, 0x78, 0x50,
            0x52, 0x51, 0x65, 0x6b, 0x33, 0x37, 0x70, 0x50, 0x68, 0x75, 0x4e, 0x53, 0x41, 0x50, 0x54, 0x6d,
            0x33, 0x6b, 0x6a, 0x4a, 0x41, 0x39, 0x6d, 0x4b, 0x62, 0x75, 0x64, 0x64, 0x63, 0x47, 0x65, 0x35,
            0x44, 0x79, 0x7a, 0x75, 0x74, 0x54, 0x75, 0x37, 0x7a, 0x75, 0x54, 0x42, 0x72, 0x46, 0x56, 0x64,
            0x35, 0x78, 0x70, 0x36, 0x44, 0x35, 0x5a, 0x4e, 0x6d, 0x72, 0x56, 0x4e, 0x64, 0x47, 0x4e, 0x42,
            0x52, 0x55, 0x78, 0x7a, 0x52, 0x51, 0x44, 0x35, 0x42, 0x70, 0x64, 0x7a, 0x35, 0x56, 0x37, 0x4e
        };

        public static string GetPlatformName() {
            return GetPlatformForAssetBundles(Application.platform);
        }

        private static string GetPlatformForAssetBundles(RuntimePlatform platform) {
            return platform switch {
                RuntimePlatform.OSXPlayer => "OSX",
                RuntimePlatform.WindowsPlayer => "Windows",
                RuntimePlatform.IPhonePlayer => "iOS",
                RuntimePlatform.WebGLPlayer => "WebGL",
                RuntimePlatform.Android => "Android",
                _ => null
            };
        }

        public static void Initialize() {
#if UNITY_IOS
			Device.SetNoBackupFlag(SavePath);
			Device.SetNoBackupFlag(VersionCachePathBase);
#endif
        }

        public bool IsAssetbundle(byte[] src) {
            if (src != null && src.Length > 6) {
                if (src[0] == 0x55 &&
                    src[1] == 0x6E &&
                    src[2] == 0x69 &&
                    src[3] == 0x74 &&
                    src[4] == 0x79 &&
                    src[5] == 0x46 &&
                    src[6] == 0x53) {
                    return true;
                }
            }
            return false;
        }

        public string GetMD5(string path) {
            string md5;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                md5 = GetMD5Proc(fs);
            }
            return md5;
        }

        public string GetMD5Proc(FileStream fs) {
            byte[] hash;
            using (var hasher = new MD5CryptoServiceProvider()) {
                hash = hasher.ComputeHash(fs);
            }
            return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
        }

        public static string GetFilePath(string fileName) {
            return Path.Combine(SavePath, fileName);
        }

        public static string GetStreamingPath(string fileName) {
#if UNITY_IOS
            string platformPath = Path.Combine(Application.streamingAssetsPath, "iOS");
#elif UNITY_ANDROID
            string platformPath = Path.Combine(Application.streamingAssetsPath, "Android");
#else
            string platformPath = Application.streamingAssetsPath; //other platforms unknown
#endif
            return Path.Combine(platformPath, fileName).Replace("\\", "/");
        }

        public static string GetStreamingAssetURL(string fileName) {
            return $"file://{GetStreamingPath(fileName)}";
        }

        public IEnumerator RemoveOldCacheAsync() {
            List<string> oldPaths = new List<string>() {
                Path.Combine(Application.temporaryCachePath, "a.b/"),
                Path.Combine(Application.temporaryCachePath, "vc.b"),
                Path.Combine(Application.temporaryCachePath, "vc.b2"),
            };
            foreach (string path in oldPaths) {
                RemoveOldCacheParam param = new RemoveOldCacheParam() {
                    Path = path
                };
                Job job = new Job();
                job.SetFunction(DoWork_RemoveOldCache, param);
                JobQueue.Enqueue(job);
                while (!job.IsDone()) {
                    yield return null;
                }
                if (job.IsError()) {
                    yield break;
                }
            }
        }

        private void DoWork_RemoveOldCache(object obj) {
            RemoveOldCacheParam param = obj as RemoveOldCacheParam;
            if (Directory.Exists(param.Path)) {
                Directory.Delete(param.Path, true);
                while (Directory.Exists(param.Path)) ;
            } else if (File.Exists(param.Path)) {
                File.Delete(param.Path);
                while (File.Exists(param.Path)) ;
            }
        }

        public class AssetBundleVersion {
            public List<AssetBundleVersionParam> list;
        }

        public class AssetBundleVersionParam {
            public string name;
            public string version;
            public uint crc;
            public long size;
            public bool isEncrypt;
        }

        private class RemoveOldCacheParam {
            public string Path { get; set; }
        }
    }
}
