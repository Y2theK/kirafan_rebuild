﻿using UnityEngine;

namespace Meige {
    public class CGlobalInstance : MonoBehaviour {
        private static CGlobalInstance ms_Instance;

        public static void CreateInstance() {
            if (ms_Instance == null) {
                GameObject gameObject = new GameObject("MeigeGlobal");
                DontDestroyOnLoad(gameObject);
                ms_Instance = gameObject.AddComponent<CGlobalInstance>();
            }
        }

        public static T EntryClass<T>() where T : Component {
            T instance = ms_Instance.gameObject.GetComponent<T>();
            if (instance == null) {
                instance = ms_Instance.gameObject.AddComponent<T>();
            }
            return instance;
        }

        public static T GetClass<T>() where T : Component {
            return ms_Instance.gameObject.GetComponent<T>();
        }
    }
}
