﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Meige
{
	// Token: 0x02001331 RID: 4913
	[Token(Token = "0x2000CBF")]
	[StructLayout(3)]
	public class CTrophyController : MonoBehaviour
	{
		// Token: 0x0600641A RID: 25626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BEC")]
		[Address(RVA = "0x1012430D0", Offset = "0x12430D0", VA = "0x1012430D0")]
		public static void Create()
		{
		}

		// Token: 0x0600641B RID: 25627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BED")]
		[Address(RVA = "0x101243134", Offset = "0x1243134", VA = "0x101243134")]
		public static void EntryTrophyCustom(CallbackCustomTroph pcustomcallback)
		{
		}

		// Token: 0x0600641C RID: 25628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BEE")]
		[Address(RVA = "0x101243198", Offset = "0x1243198", VA = "0x101243198")]
		public static void EntryCallbackEvent(CallbackCustomEvent peventcallback)
		{
		}

		// Token: 0x0600641D RID: 25629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BEF")]
		[Address(RVA = "0x10124319C", Offset = "0x124319C", VA = "0x10124319C")]
		public static void Unlock(TrophyHandlerUnlock phandle, int ftrophyid)
		{
		}

		// Token: 0x0600641E RID: 25630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF0")]
		[Address(RVA = "0x101243268", Offset = "0x1243268", VA = "0x101243268")]
		private void Update()
		{
		}

		// Token: 0x0600641F RID: 25631 RVA: 0x00021300 File Offset: 0x0001F500
		[Token(Token = "0x6005BF1")]
		[Address(RVA = "0x101243C3C", Offset = "0x1243C3C", VA = "0x101243C3C")]
		private static int ChangeNumberKey(string fkey)
		{
			return 0;
		}

		// Token: 0x06006420 RID: 25632 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005BF2")]
		[Address(RVA = "0x101243654", Offset = "0x1243654", VA = "0x101243654")]
		private CTrophyController.CAchivementState SearchAchivement(int fkeyid)
		{
			return null;
		}

		// Token: 0x06006421 RID: 25633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF3")]
		[Address(RVA = "0x101243CE0", Offset = "0x1243CE0", VA = "0x101243CE0")]
		private void CallbackAchivementList(IAchievementDescription[] plist)
		{
		}

		// Token: 0x06006422 RID: 25634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF4")]
		[Address(RVA = "0x101243E60", Offset = "0x1243E60", VA = "0x101243E60")]
		private void CallbackAchivementState(IAchievement[] plist)
		{
		}

		// Token: 0x06006423 RID: 25635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF5")]
		[Address(RVA = "0x101244034", Offset = "0x1244034", VA = "0x101244034")]
		private void CallbackUnlock(bool success)
		{
		}

		// Token: 0x06006424 RID: 25636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF6")]
		[Address(RVA = "0x1012440B8", Offset = "0x12440B8", VA = "0x1012440B8")]
		public CTrophyController()
		{
		}

		// Token: 0x0400751C RID: 29980
		[Token(Token = "0x40052D9")]
		private static CTrophyController ms_Inst;

		// Token: 0x0400751D RID: 29981
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40052DA")]
		private List<TrophyHandlerBase> m_List;

		// Token: 0x0400751E RID: 29982
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40052DB")]
		private TrophyHandlerBase m_NowPlayHandle;

		// Token: 0x0400751F RID: 29983
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40052DC")]
		private CTrophyController.CAchivementState m_NowAchivement;

		// Token: 0x04007520 RID: 29984
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40052DD")]
		private CallbackCustomTroph m_CallbackCustom;

		// Token: 0x04007521 RID: 29985
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40052DE")]
		private CTrophyController.eStep m_Step;

		// Token: 0x04007522 RID: 29986
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40052DF")]
		private bool m_ListUp;

		// Token: 0x04007523 RID: 29987
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40052E0")]
		private List<CTrophyController.CAchivementState> m_AchivementList;

		// Token: 0x02001332 RID: 4914
		[Token(Token = "0x200130C")]
		private enum eStep
		{
			// Token: 0x04007525 RID: 29989
			[Token(Token = "0x40074F7")]
			InitUp,
			// Token: 0x04007526 RID: 29990
			[Token(Token = "0x40074F8")]
			AcountCheck,
			// Token: 0x04007527 RID: 29991
			[Token(Token = "0x40074F9")]
			ListUp,
			// Token: 0x04007528 RID: 29992
			[Token(Token = "0x40074FA")]
			ListState,
			// Token: 0x04007529 RID: 29993
			[Token(Token = "0x40074FB")]
			Update
		}

		// Token: 0x02001333 RID: 4915
		[Token(Token = "0x200130D")]
		public class CAchivementState
		{
			// Token: 0x06006425 RID: 25637 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006499")]
			[Address(RVA = "0x101243E58", Offset = "0x1243E58", VA = "0x101243E58")]
			public CAchivementState()
			{
			}

			// Token: 0x0400752A RID: 29994
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40074FC")]
			public string m_KeyName;

			// Token: 0x0400752B RID: 29995
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40074FD")]
			public int m_AccessKey;

			// Token: 0x0400752C RID: 29996
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40074FE")]
			public bool m_Complete;
		}
	}
}
