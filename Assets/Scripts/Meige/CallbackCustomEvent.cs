﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001330 RID: 4912
	// (Invoke) Token: 0x06006417 RID: 25623
	[Token(Token = "0x2000CBE")]
	[StructLayout(3, Size = 8)]
	public delegate void CallbackCustomEvent(int fstate);
}
