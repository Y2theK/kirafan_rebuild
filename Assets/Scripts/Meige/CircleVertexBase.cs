﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x020012B0 RID: 4784
	[Token(Token = "0x2000C6B")]
	[StructLayout(3)]
	public static class CircleVertexBase
	{
		// Token: 0x0600615B RID: 24923 RVA: 0x00020238 File Offset: 0x0001E438
		[Token(Token = "0x6005A15")]
		[Address(RVA = "0x101244724", Offset = "0x1244724", VA = "0x101244724")]
		public static int GetIndexFromAngle(float angle)
		{
			return 0;
		}

		// Token: 0x0600615C RID: 24924 RVA: 0x00020250 File Offset: 0x0001E450
		[Token(Token = "0x6005A16")]
		[Address(RVA = "0x1012447B8", Offset = "0x12447B8", VA = "0x1012447B8")]
		public static Vector2 GetVertexFromAngle(float angle)
		{
			return default(Vector2);
		}

		// Token: 0x0400706D RID: 28781
		[Token(Token = "0x4004F52")]
		public const int CircleVertexNum = 1440;

		// Token: 0x0400706E RID: 28782
		[Token(Token = "0x4004F53")]
		public static Vector2[] m_Vertices;
	}
}
