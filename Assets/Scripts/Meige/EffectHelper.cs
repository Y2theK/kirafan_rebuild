﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Meige {
    public class EffectHelper {
        private static ReadOnlyCollection<int> m_EffectAnimTypeCodeComponentNum = Array.AsReadOnly(new int[28] {
            1,
            1,
            1,
            1,
            2,
            3,
            4,
            4,
            4,
            1,
            1,
            2,
            3,
            4,
            4,
            2,
            2,
            4,
            6,
            8,
            8,
            1,
            1,
            2,
            3,
            4,
            4,
            1
        });

        private static ReadOnlyCollection<Type> m_EffectAnimTypeCodeToType = Array.AsReadOnly(new Type[28] {
            typeof(bool),
            typeof(int),
            typeof(long),
            typeof(float),
            typeof(Vector2),
            typeof(Vector3),
            typeof(Vector4),
            typeof(Color),
            typeof(Rect),
            typeof(s32),
            typeof(f32),
            typeof(Float2),
            typeof(Float3),
            typeof(Float4),
            typeof(RGBA),
            typeof(RangePareFloat),
            typeof(RangePareInt),
            typeof(RangePareVector2),
            typeof(RangePareVector3),
            typeof(RangePareVector4),
            typeof(RangePareColor),
            typeof(RangeValueInt),
            typeof(RangeValueFloat),
            typeof(RangeValueVector2),
            typeof(RangeValueVector3),
            typeof(RangeValueVector4),
            typeof(RangeValueColor),
            typeof(Enum)
        });

        private static Dictionary<Type, eEffectAnimTypeCode> m_EffectAnimTypeToTypeCode = new Dictionary<Type, eEffectAnimTypeCode> {
            { typeof(bool), eEffectAnimTypeCode.Bool },
            { typeof(int), eEffectAnimTypeCode.Int },
            { typeof(long), eEffectAnimTypeCode.Long },
            { typeof(float), eEffectAnimTypeCode.Float },
            { typeof(Vector2), eEffectAnimTypeCode.Vector2 },
            { typeof(Vector3), eEffectAnimTypeCode.Vector3 },
            { typeof(Vector4), eEffectAnimTypeCode.Vector4 },
            { typeof(Color), eEffectAnimTypeCode.Color },
            { typeof(Rect), eEffectAnimTypeCode.Rect },
            { typeof(s32), eEffectAnimTypeCode.S32 },
            { typeof(f32), eEffectAnimTypeCode.F32 },
            { typeof(Float2), eEffectAnimTypeCode.Float2 },
            { typeof(Float3), eEffectAnimTypeCode.Float3 },
            { typeof(Float4), eEffectAnimTypeCode.Float4 },
            { typeof(RGBA), eEffectAnimTypeCode.RGBA },
            { typeof(RangePareFloat), eEffectAnimTypeCode.RangePareFloat },
            { typeof(RangePareInt), eEffectAnimTypeCode.RangePareInt },
            { typeof(RangePareVector2), eEffectAnimTypeCode.RangePareVector2 },
            { typeof(RangePareVector3), eEffectAnimTypeCode.RangePareVector3 },
            { typeof(RangePareVector4), eEffectAnimTypeCode.RangePareVector4 },
            { typeof(RangePareColor), eEffectAnimTypeCode.RangePareColor },
            { typeof(RangeValueInt), eEffectAnimTypeCode.RangeValueInt },
            { typeof(RangeValueFloat), eEffectAnimTypeCode.RangeValueFloat },
            { typeof(RangeValueVector2), eEffectAnimTypeCode.RangeValueVector2 },
            { typeof(RangeValueVector3), eEffectAnimTypeCode.RangeValueVector3 },
            { typeof(RangeValueVector4), eEffectAnimTypeCode.RangeValueVector4 },
            { typeof(RangeValueColor), eEffectAnimTypeCode.RangeValueColor },
            { typeof(Enum), eEffectAnimTypeCode.Enum }
        };

        public static int GetComponentNum(eEffectAnimTypeCode typeCode) {
            return m_EffectAnimTypeCodeComponentNum[(int)typeCode];
        }

        public static Type GetTypeFromTypeCode(eEffectAnimTypeCode typeCode) {
            return m_EffectAnimTypeCodeToType[(int)typeCode];
        }

        public static eEffectAnimTypeCode GetTypeCodeFromType(Type type) {
            if (m_EffectAnimTypeToTypeCode.TryGetValue(type, out eEffectAnimTypeCode result)) {
                return result;
            }
            return eEffectAnimTypeCode.Invalid;
        }

        public static int GetTargetNum(object target) {
            eEffectAnimTypeCode typeCode = GetTypeCodeFromType(target.GetType());
            int result = typeCode switch {
                eEffectAnimTypeCode.RangeValueInt => ((RangeValueInt)target).m_Value.Length,
                eEffectAnimTypeCode.RangeValueFloat => ((RangeValueFloat)target).m_Value.Length,
                eEffectAnimTypeCode.RangeValueVector2 => ((RangeValueVector2)target).m_Value.Length,
                eEffectAnimTypeCode.RangeValueVector3 => ((RangeValueVector3)target).m_Value.Length,
                eEffectAnimTypeCode.RangeValueVector4 => ((RangeValueVector4)target).m_Value.Length,
                eEffectAnimTypeCode.RangeValueColor => ((RangeValueColor)target).m_Value.Length,
                _ => 0,
            };
            return result;
        }

        public static T UnBox<T>(object value) {
            return (T)value;
        }

        public static void Copy(ref bool target, int tgtIdx, object value) {
            target = (bool)value;
        }

        public static void Copy(ref sbyte target, int tgtIdx, object value) {
            target = (sbyte)value;
        }

        public static void Copy(ref byte target, int tgtIdx, object value) {
            target = (byte)value;
        }

        public static void Copy(ref short target, int tgtIdx, object value) {
            target = (short)value;
        }

        public static void Copy(ref ushort target, int tgtIdx, object value) {
            target = (ushort)value;
        }

        public static void Copy(ref int target, int tgtIdx, object value) {
            target = (int)value;
        }

        public static void Copy(ref uint target, int tgtIdx, object value) {
            target = (uint)value;
        }

        public static void Copy(ref long target, int tgtIdx, object value) {
            target = (long)value;
        }

        public static void Copy(ref ulong target, int tgtIdx, object value) {
            target = (ulong)value;
        }

        public static void Copy(ref float target, int tgtIdx, object value) {
            target = (float)value;
        }

        public static void Copy(ref double target, int tgtIdx, object value) {
            target = (double)value;
        }

        public static void Copy(ref Vector2 target, int tgtIdx, object value) {
            target = (Vector2)value;
        }

        public static void Copy(ref Vector3 target, int tgtIdx, object value) {
            target = (Vector3)value;
        }

        public static void Copy(ref Vector4 target, int tgtIdx, object value) {
            target = (Vector4)value;
        }

        public static void Copy(ref Color target, int tgtIdx, object value) {
            target = (Color)value;
        }

        public static void Copy(ref Rect target, int tgtIdx, object value) {
            target = (Rect)value;
        }

        public static void Copy(ref s32 target, int tgtIdx, object value) {
            target = (s32)value;
        }

        public static void Copy(ref f32 target, int tgtIdx, object value) {
            target = (f32)value;
        }

        public static void Copy(ref Float2 target, int tgtIdx, object value) {
            target = (Float2)value;
        }

        public static void Copy(ref Float3 target, int tgtIdx, object value) {
            target = (Float3)value;
        }

        public static void Copy(ref Float4 target, int tgtIdx, object value) {
            target = (Float4)value;
        }

        public static void Copy(ref RGBA target, int tgtIdx, object value) {
            target = (RGBA)value;
        }

        public static void Copy(ref RangePareFloat target, int tgtIdx, object value) {
            target.Set((RangePareFloat)value);
        }

        public static void Copy(ref RangePareInt target, int tgtIdx, object value) {
            target.Set((RangePareInt)value);
        }

        public static void Copy(ref RangePareVector2 target, int tgtIdx, object value) {
            target.Set((RangePareVector2)value);
        }

        public static void Copy(ref RangePareVector3 target, int tgtIdx, object value) {
            target.Set((RangePareVector3)value);
        }

        public static void Copy(ref RangePareVector4 target, int tgtIdx, object value) {
            target.Set((RangePareVector4)value);
        }

        public static void Copy(ref RangePareColor target, int tgtIdx, object value) {
            target.Set((RangePareColor)value);
        }

        public static void Copy(ref RangeValueInt target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Copy(ref RangeValueFloat target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Copy(ref RangeValueVector2 target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Copy(ref RangeValueVector3 target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Copy(ref RangeValueVector4 target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Copy(ref RangeValueColor target, int tgtIdx, object value) {
            target[tgtIdx].SetRowValue(value);
        }

        public static void Set(ref bool target, int tgtIdx, int componentIdx, float value) {
            target = value != 0f;
        }

        public static void Set(ref sbyte target, int tgtIdx, int componentIdx, float value) {
            target = (sbyte)value;
        }

        public static void Set(ref byte target, int tgtIdx, int componentIdx, float value) {
            target = (byte)value;
        }

        public static void Set(ref short target, int tgtIdx, int componentIdx, float value) {
            target = (short)value;
        }

        public static void Set(ref ushort target, int tgtIdx, int componentIdx, float value) {
            target = (ushort)value;
        }

        public static void Set(ref int target, int tgtIdx, int componentIdx, float value) {
            target = (int)value;
        }

        public static void Set(ref uint target, int tgtIdx, int componentIdx, float value) {
            target = (uint)value;
        }

        public static void Set(ref long target, int tgtIdx, int componentIdx, float value) {
            target = (long)value;
        }

        public static void Set(ref ulong target, int tgtIdx, int componentIdx, float value) {
            target = (ulong)value;
        }

        public static void Set(ref float target, int tgtIdx, int componentIdx, float value) {
            target = value;
        }

        public static void Set(ref double target, int tgtIdx, int componentIdx, float value) {
            target = (double)value;
        }

        public static void Set(ref Vector2 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Vector3 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Vector4 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Color target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Rect target, int tgtIdx, int componentIdx, float value) {
            if (componentIdx == 0) {
                target.x = value;
            } else if (componentIdx == 1) {
                target.y = value;
            } else if (componentIdx == 2) {
                target.width = value;
            } else {
                target.height = value;
            }
        }

        public static void Set(ref s32 target, int tgtIdx, int componentIdx, float value) {
            target = (int)value;
        }

        public static void Set(ref f32 target, int tgtIdx, int componentIdx, float value) {
            target = value;
        }

        public static void Set(ref Float2 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Float3 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref Float4 target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref RGBA target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref RangePareFloat target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = value;
        }

        public static void Set(ref RangePareInt target, int tgtIdx, int componentIdx, float value) {
            target[componentIdx] = (int)value;
        }

        public static void Set(ref RangePareVector2 target, int tgtIdx, int componentIdx, float value) {
            switch (componentIdx) {
                case 0:
                    target.m_Min.m_Value[0] = value;
                    break;
                case 1:
                    target.m_Min.m_Value[1] = value;
                    break;
                case 2:
                    target.m_Max.m_Value[0] = value;
                    break;
                case 3:
                    target.m_Max.m_Value[1] = value;
                    break;
            }
        }

        public static void Set(ref RangePareVector3 target, int tgtIdx, int componentIdx, float value) {
            switch (componentIdx) {
                case 0:
                    target.m_Min.m_Value[0] = value;
                    break;
                case 1:
                    target.m_Min.m_Value[1] = value;
                    break;
                case 2:
                    target.m_Min.m_Value[2] = value;
                    break;
                case 3:
                    target.m_Max.m_Value[0] = value;
                    break;
                case 4:
                    target.m_Max.m_Value[1] = value;
                    break;
                case 5:
                    target.m_Max.m_Value[2] = value;
                    break;
            }
        }

        public static void Set(ref RangePareVector4 target, int tgtIdx, int componentIdx, float value) {
            switch (componentIdx) {
                case 0:
                    target.m_Min.m_Value[0] = value;
                    break;
                case 1:
                    target.m_Min.m_Value[1] = value;
                    break;
                case 2:
                    target.m_Min.m_Value[2] = value;
                    break;
                case 3:
                    target.m_Max.m_Value[3] = value;
                    break;
                case 4:
                    target.m_Max.m_Value[0] = value;
                    break;
                case 5:
                    target.m_Max.m_Value[1] = value;
                    break;
                case 6:
                    target.m_Max.m_Value[2] = value;
                    break;
                case 7:
                    target.m_Max.m_Value[3] = value;
                    break;
            }
        }

        public static void Set(ref RangePareColor target, int tgtIdx, int componentIdx, float value) {
            switch (componentIdx) {
                case 0:
                    target.m_Min.m_Value[0] = value;
                    break;
                case 1:
                    target.m_Min.m_Value[1] = value;
                    break;
                case 2:
                    target.m_Min.m_Value[2] = value;
                    break;
                case 3:
                    target.m_Max.m_Value[3] = value;
                    break;
                case 4:
                    target.m_Max.m_Value[0] = value;
                    break;
                case 5:
                    target.m_Max.m_Value[1] = value;
                    break;
                case 6:
                    target.m_Max.m_Value[2] = value;
                    break;
                case 7:
                    target.m_Max.m_Value[3] = value;
                    break;
            }
        }

        public static void Set(ref RangeValueInt target, int tgtIdx, int componentIdx, float value) {
            target[tgtIdx] = (int)value;
        }

        public static void Set(ref RangeValueFloat target, int tgtIdx, int componentIdx, float value) {
            target[tgtIdx] = value;
        }

        public static void Set(ref RangeValueVector2 target, int tgtIdx, int componentIdx, float value) {
            target.m_Value[tgtIdx].m_Value[componentIdx] = value;
        }

        public static void Set(ref RangeValueVector3 target, int tgtIdx, int componentIdx, float value) {
            target.m_Value[tgtIdx].m_Value[componentIdx] = value;
        }

        public static void Set(ref RangeValueVector4 target, int tgtIdx, int componentIdx, float value) {
            target.m_Value[tgtIdx].m_Value[componentIdx] = value;
        }

        public static void Set(ref RangeValueColor target, int tgtIdx, int componentIdx, float value) {
            target.m_Value[tgtIdx].m_Value[componentIdx] = value;
        }

        public static float Get(bool target, int tgtIdx, int componentIdx) {
            return target ? 1f : 0f;
        }

        public static float Get(sbyte target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(byte target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(short target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(ushort target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(int target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(uint target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(long target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(ulong target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(float target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(double target, int tgtIdx, int componentIdx) {
            return (float)target;
        }

        public static float Get(Vector2 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Vector3 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Vector4 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Color target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Rect target, int tgtIdx, int componentIdx) {
            if (componentIdx == 0) {
                return target.x;
            } else if (componentIdx == 1) {
                return target.y;
            } else if (componentIdx == 2) {
                return target.width;
            }
            return target.height;
        }

        public static float Get(s32 target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(f32 target, int tgtIdx, int componentIdx) {
            return target;
        }

        public static float Get(Float2 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Float3 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(Float4 target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(RGBA target, int tgtIdx, int componentIdx) {
            return target[componentIdx];
        }

        public static float Get(RangePareFloat target, int tgtIdx, int componentIdx) {
            if (componentIdx < 0 || componentIdx > 1) {
                return 0f;
            }
            return target[componentIdx];
        }

        public static float Get(RangePareInt target, int tgtIdx, int componentIdx) {
            if (componentIdx < 0 || componentIdx > 1) {
                return 0f;
            }
            return target[componentIdx];
        }

        public static float Get(RangePareVector2 target, int tgtIdx, int componentIdx) {
            return componentIdx switch {
                0 => target[0].m_Value[0],
                1 => target[0].m_Value[1],
                2 => target[1].m_Value[0],
                3 => target[1].m_Value[1],
                _ => 0f,
            };
        }

        public static float Get(RangePareVector3 target, int tgtIdx, int componentIdx) {
            return componentIdx switch {
                0 => target[0].m_Value[0],
                1 => target[0].m_Value[1],
                2 => target[0].m_Value[2],
                3 => target[1].m_Value[0],
                4 => target[1].m_Value[1],
                5 => target[1].m_Value[2],
                _ => 0f,
            };
        }

        public static float Get(RangePareVector4 target, int tgtIdx, int componentIdx) {
            return componentIdx switch {
                0 => target[0].m_Value[0],
                1 => target[0].m_Value[1],
                2 => target[0].m_Value[2],
                3 => target[0].m_Value[3],
                4 => target[1].m_Value[0],
                5 => target[1].m_Value[1],
                6 => target[1].m_Value[2],
                7 => target[1].m_Value[3],
                _ => 0f,
            };
        }

        public static float Get(RangePareColor target, int tgtIdx, int componentIdx) {
            return componentIdx switch {
                0 => target[0].m_Value[0],
                1 => target[0].m_Value[1],
                2 => target[0].m_Value[2],
                3 => target[0].m_Value[3],
                4 => target[1].m_Value[0],
                5 => target[1].m_Value[1],
                6 => target[1].m_Value[2],
                7 => target[1].m_Value[3],
                _ => 0f,
            };
        }

        public static float Get(RangeValueInt target, int tgtIdx, int componentIdx) {
            return target[tgtIdx];
        }

        public static float Get(RangeValueFloat target, int tgtIdx, int componentIdx) {
            return target[tgtIdx];
        }

        public static float Get(RangeValueVector2 target, int tgtIdx, int componentIdx) {
            return target.m_Value[tgtIdx].m_Value[componentIdx];
        }

        public static float Get(RangeValueVector3 target, int tgtIdx, int componentIdx) {
            return target.m_Value[tgtIdx].m_Value[componentIdx];
        }

        public static float Get(RangeValueVector4 target, int tgtIdx, int componentIdx) {
            return target.m_Value[tgtIdx].m_Value[componentIdx];
        }

        public static float Get(RangeValueColor target, int tgtIdx, int componentIdx) {
            return target.m_Value[tgtIdx].m_Value[componentIdx];
        }

        public static float Get(object target, int tgtIdx, int componentIdx) {
            return GetTypeCodeFromType(target.GetType()) switch {
                eEffectAnimTypeCode.Bool => Get((bool)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Int => Get((int)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Long => Get((long)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Float => Get((float)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Vector2 => Get((Vector2)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Vector3 => Get((Vector3)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Vector4 => Get((Vector4)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Color => Get((Color)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.S32 => Get((s32)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.F32 => Get((f32)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Float2 => Get((Float2)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Float3 => Get((Float3)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Float4 => Get((Float4)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RGBA => Get((RGBA)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareFloat => Get((RangePareFloat)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareInt => Get((RangePareInt)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareVector2 => Get((RangePareVector2)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareVector3 => Get((RangePareVector3)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareVector4 => Get((RangePareVector4)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangePareColor => Get((RangePareColor)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueInt => Get((RangeValueInt)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueFloat => Get((RangeValueFloat)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueVector2 => Get((RangeValueVector2)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueVector3 => Get((RangeValueVector3)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueVector4 => Get((RangeValueVector4)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.RangeValueColor => Get((RangeValueColor)target, tgtIdx, componentIdx),
                eEffectAnimTypeCode.Enum => Get((int)target, tgtIdx, componentIdx),
                _ => 0f,
            };
        }

        public static string GetHierarchyPath(Transform top, Transform self) {
            string path = self.gameObject.name;
            Transform parent = self.parent;
            while (parent != null && parent != top) {
                path = parent.name + "/" + path;
                parent = parent.parent;
            }
            return path;
        }
    }
}
