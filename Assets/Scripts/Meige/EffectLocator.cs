﻿using UnityEngine;

namespace Meige {
    public class EffectLocator : MonoBehaviour {
        private Transform m_ThisTransform;
        private GameObject m_OwnerGO;

        public void Reset() {
            if (m_ThisTransform == null) { return; }
            m_ThisTransform.localPosition = Vector3.zero;
            m_ThisTransform.localRotation = Quaternion.identity;
            m_ThisTransform.localScale = Vector3.one;
        }

        public void SetOwner(GameObject go) {
            m_OwnerGO = go;
        }

        public GameObject GetOwner() {
            return m_OwnerGO;
        }

        public Transform GetTransform() {
            return m_ThisTransform;
        }

        private void Awake() {
            m_ThisTransform = transform;
        }

        private void Update() { }
    }
}
