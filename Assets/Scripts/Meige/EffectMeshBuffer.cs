﻿using System.Collections.Generic;
using UnityEngine;

namespace Meige {
    public class EffectMeshBuffer : MonoBehaviour {
        protected MeshFilter m_MeshFilter;
        protected Mesh m_Mesh;

        public Topology m_MeshTopology;
        protected Topology m_MeshTopologyWork;

        public int m_maxPrimitiveCount = 512;
        protected int m_maxPrimitiveCountWork;
        protected int m_vTopIndex;
        protected int m_iTopIndex;

        public bool m_isUsableSubUVs;
        public bool m_isUsableNormals;
        public bool m_isUsableUVRegionRepeat;

        protected Vector3[] m_vertices;
        protected Vector3[] m_normals;
        protected Vector2[] m_UVs;
        protected Vector2[] m_SubUVs;
        protected Color[] m_colors;
        protected int[] m_Indices;
        protected List<Vector4> m_RepeatUVRegion;
        protected Vector3[] m_verticesWork;
        protected Vector3[] m_normalsWork;
        protected Vector2[] m_UVsWork;
        protected Vector2[] m_SubUVsWork;
        protected Color[] m_colorsWork;
        protected int[] m_IndicesWork;
        protected List<Vector4> m_RepeatUVRegionWork;

        [SerializeField] protected int m_primitiveCount;

        private HookUpdate m_Hook_Update;
        private HookUpdate m_Hook_OnWillRender;

        private const int MAX_ACTIVE_MESHBUFFER = 32;

        protected List<MeshBuffer> m_MeshBufferList = new List<MeshBuffer>();
        protected List<MeshBuffer> m_ActiveMeshBufferList = new List<MeshBuffer>(MAX_ACTIVE_MESHBUFFER);

        protected bool bInit;

        public delegate void HookUpdate();

        protected bool m_vertsChanged { get; set; }
        protected bool m_normalsChanged { get; set; }
        protected bool m_uvsChanged { get; set; }
        protected bool m_subuvsChanged { get; set; }
        protected bool m_colorsChanged { get; set; }
        protected bool m_IndicesChanged { get; set; }
        protected bool m_RepeatUVRegionChanged { get; set; }
        public bool m_maxTriangleBufferChanged { get; set; }
        public bool bufferChangeFlg { get; set; }

        public int meshBufferCount => m_MeshBufferList.Count;
        public int vertexBufferCount => m_vertices == null ? 0 : m_vertices.Length;
        public int indexBufferCount => m_Indices == null ? 0 : m_Indices.Length;
        public int primitiveCount => m_primitiveCount;

        public bool isUsableSubUVsFix {
            get => m_isUsableSubUVs;
            protected set => m_isUsableSubUVs = value;
        }

        public bool isUsableNormalsFix {
            get => m_isUsableNormals;
            protected set => m_isUsableNormals = value;
        }

        public bool isUsableUVRegionRepeat {
            get => m_isUsableUVRegionRepeat;
            protected set => m_isUsableUVRegionRepeat = value;
        }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public void AddHook_Update(HookUpdate hook) {
            m_Hook_Update += hook;
        }

        public void RemoveHook_Update(HookUpdate hook) {
            m_Hook_Update -= hook;
        }

        public void AddHook_OnWillRender(HookUpdate hook) {
            m_Hook_OnWillRender += hook;
        }

        public void RemoveHook_OnWillRender(HookUpdate hook) {
            m_Hook_OnWillRender -= hook;
        }

        private void Awake() {
            m_vertsChanged = false;
            m_normalsChanged = false;
            m_uvsChanged = false;
            m_subuvsChanged = false;
            m_colorsChanged = false;
            m_IndicesChanged = false;
            m_RepeatUVRegionChanged = false;
            m_vTopIndex = 0;
            m_iTopIndex = 0;
            m_primitiveCount = 0;
        }

        private void Start() {
            EffectObjectManager.Instance.RegisterMeshBufferComponent(this);
            m_MeshFilter = gameObject.AddComponent<MeshFilter>();
            if (m_MeshFilter == null) {
                m_MeshFilter = gameObject.GetComponent<MeshFilter>();
            }
            m_Mesh = m_MeshFilter.mesh;
            m_Mesh.MarkDynamic();
            m_Mesh.SetIndices(null, MeshTopology.Triangles, 0);
            Bounds bounds = m_Mesh.bounds;
            bounds.size = Vector3.one * 10000f;
            m_Mesh.bounds = bounds;
        }

        private void OnDestroy() {
            if (EffectObjectManager.Instance != null) {
                EffectObjectManager.Instance.RemoveMeshBufferComponent(this);
            }
        }

        private void Update() {
            m_primitiveCount = m_Indices == null ? 0 : CalcPrimitiveNum(m_Indices.Length);
            if (m_Hook_Update != null) {
                m_Hook_Update();
            }
        }

        private void OnWillRenderObject() {
            if (m_Hook_OnWillRender != null) {
                m_Hook_OnWillRender();
            }
            MeshUpdate();
        }

        protected virtual void MeshUpdate() {
            Vector3[] vertices = m_vertices;
            Vector3[] normals = m_normals;
            Vector2[] uvs = m_UVs;
            Vector2[] subUvs = m_SubUVs;
            Color[] colors = m_colors;
            int[] indices = m_Indices;
            List<Vector4> repeatUVRegion = m_RepeatUVRegion;
            if (m_maxPrimitiveCountWork > 0 && (m_maxPrimitiveCountWork != m_maxPrimitiveCount || m_MeshTopology != m_MeshTopologyWork)) {
                int max = CalcIndexMax(m_maxPrimitiveCountWork);
                m_verticesWork = new Vector3[max];
                m_UVsWork = new Vector2[max];
                m_colorsWork = new Color[max];
                m_IndicesWork = new int[max];
                if (isUsableSubUVsFix) {
                    m_SubUVsWork = new Vector2[max];
                }
                if (isUsableNormalsFix) {
                    m_normalsWork = new Vector3[max];
                }
                m_maxPrimitiveCountWork = m_maxPrimitiveCount;
                m_MeshTopologyWork = m_MeshTopology;
            }
            if (RenderFlg || bufferChangeFlg) {
                bufferChangeFlg = true;
                if (m_maxPrimitiveCountWork > 0 && m_primitiveCount > m_maxPrimitiveCountWork) {
                    int vIdx = 0;
                    int mIdx = 0;
                    foreach (MeshBuffer meshBuffer in m_ActiveMeshBufferList) {
                        if (vIdx + meshBuffer.vertices.Length >= m_verticesWork.Length || mIdx + meshBuffer.indices.Length >= m_IndicesWork.Length) { break; }

                        if (!meshBuffer.enabled) { continue; }

                        if (m_maxTriangleBufferChanged) {
                            meshBuffer.isDirtyVerts = true;
                            meshBuffer.isDirtyUVs = true;
                            meshBuffer.isDirtyColors = true;
                            meshBuffer.isDirtyIndices = true;
                            if (isUsableSubUVsFix) {
                                meshBuffer.isDirtySubUVs = true;
                            }
                            if (isUsableNormalsFix) {
                                meshBuffer.isDirtyNorms = true;
                            }
                        }

                        if (meshBuffer.isDirtyVerts) {
                            meshBuffer.vertices.CopyTo(m_verticesWork, vIdx);
                            m_vertsChanged = true;
                        }
                        if (meshBuffer.isDirtyNorms) {
                            meshBuffer.normals.CopyTo(m_normalsWork, vIdx);
                            m_normalsChanged = true;
                        }
                        if (meshBuffer.isDirtyUVs) {
                            meshBuffer.UVs.CopyTo(m_UVsWork, vIdx);
                            m_uvsChanged = true;
                        }
                        if (meshBuffer.isDirtyColors) {
                            meshBuffer.colors.CopyTo(m_colorsWork, vIdx);
                            m_colorsChanged = true;
                        }
                        if (meshBuffer.isDirtyIndices) {
                            for (int i = 0; i < meshBuffer.indices.Length; i++) {
                                m_IndicesWork[i + mIdx] = meshBuffer.indices[i] + vIdx;
                            }
                            m_IndicesChanged = true;
                        }
                        if (meshBuffer.isDirtySubUVs) {
                            meshBuffer.SubUVs.CopyTo(m_SubUVsWork, vIdx);
                            m_subuvsChanged = true;
                        }
                        if (meshBuffer.isDirtyRepeatUVRegion) {
                            meshBuffer.repeatUVRegion.CopyTo(m_RepeatUVRegionWork.ToArray(), vIdx);
                            m_RepeatUVRegionChanged = true;
                        }

                        meshBuffer.isDirtyVerts = false;
                        meshBuffer.isDirtyUVs = false;
                        meshBuffer.isDirtyColors = false;
                        meshBuffer.isDirtyIndices = false;
                        if (isUsableSubUVsFix) {
                            meshBuffer.isDirtySubUVs = false;
                        }
                        if (isUsableNormalsFix) {
                            meshBuffer.isDirtyNorms = false;
                        }

                        vIdx += meshBuffer.vertices.Length;
                        mIdx += meshBuffer.indices.Length;
                    }
                    if (m_maxTriangleBufferChanged) {
                        for (int i = mIdx; i < m_IndicesWork.Length; i++) {
                            m_IndicesWork[i] = vIdx;
                        }
                    }
                    vertices = m_verticesWork;
                    normals = m_normalsWork;
                    uvs = m_UVsWork;
                    colors = m_colorsWork;
                    indices = m_IndicesWork;
                    if (isUsableSubUVsFix) {
                        subUvs = m_SubUVsWork;
                    }
                    m_maxTriangleBufferChanged = false;
                }
                if (m_MeshBufferList != null) {
                    bool hasEnabledMesh = false;
                    foreach (MeshBuffer meshBuffer in m_MeshBufferList) {
                        if (meshBuffer.enabled) {
                            hasEnabledMesh = true;
                            break;
                        }
                    }
                    if (!hasEnabledMesh) {
                        RenderFlg = false;
                    }
                }
            }
            if (m_vertsChanged) {
                if (vertices == null) { return; }

                m_Mesh.vertices = vertices;
                if (m_Mesh.vertexCount != vertices.Length) { return; }

                m_vertsChanged = false;
                if (!bInit || m_Mesh.bounds.size.x < 100f) {
                    Bounds bounds = m_Mesh.bounds;
                    bounds.size = Vector3.one * 10000f;
                    m_Mesh.bounds = bounds;
                    bInit = true;
                }
            }
            bool hasVertices = m_Mesh.vertexCount > 0;
            if (m_normalsChanged && hasVertices) {
                m_Mesh.normals = normals;
                m_normalsChanged = false;
            }
            if (m_uvsChanged && hasVertices) {
                m_Mesh.uv = uvs;
                m_uvsChanged = false;
            }
            if (m_colorsChanged && hasVertices) {
                m_Mesh.colors = colors;
                m_colorsChanged = false;
            }
            if (m_IndicesChanged && hasVertices) {
                m_Mesh.SetIndices(indices, m_MeshTopologyTable[(int)m_MeshTopology], 0);
                m_IndicesChanged = false;
            }
            if (isUsableSubUVsFix && m_subuvsChanged && hasVertices) {
                m_Mesh.uv2 = subUvs;
                m_subuvsChanged = false;
            }
            if (isUsableUVRegionRepeat && m_RepeatUVRegionChanged && hasVertices) {
                m_Mesh.SetUVs(3, repeatUVRegion);
                m_RepeatUVRegionChanged = false;
            }
        }

        internal void AddActiveBufferList(MeshBuffer meshBuffer) {
            m_ActiveMeshBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(MeshBuffer meshBuffer) {
            m_ActiveMeshBufferList.Remove(meshBuffer);
        }

        public virtual int CalcPrimitiveNum(int IndicesNum) {
            return m_MeshTopology switch {
                Topology.Triangles => IndicesNum / 3,
                Topology.Quads => IndicesNum / 4,
                Topology.Lines => IndicesNum / 2,
                Topology.Points => IndicesNum,
                _ => 0,
            };
        }

        public virtual int CalcIndexMax(int primitiveNum) {
            return m_MeshTopology switch {
                Topology.Triangles => primitiveNum * 3,
                Topology.Quads => primitiveNum * 4,
                Topology.Lines => primitiveNum * 2,
                Topology.Points => primitiveNum,
                _ => 0,
            };
        }

        public virtual MeshBuffer AddBuffer(int vertexCount, int indexCount) {
            MeshBuffer meshBuffer = new MeshBuffer();
            meshBuffer.vertices = new Vector3[vertexCount];
            meshBuffer.UVs = new Vector2[vertexCount];
            meshBuffer.colors = new Color[vertexCount];
            meshBuffer.indices = new int[indexCount];
            if (isUsableSubUVsFix) {
                meshBuffer.SubUVs = new Vector2[vertexCount];
            }
            if (isUsableNormalsFix) {
                meshBuffer.normals = new Vector3[vertexCount];
            }
            meshBuffer.vTopIndex = m_vTopIndex;
            meshBuffer.iTopIndex = m_iTopIndex;

            m_vTopIndex += vertexCount;
            m_iTopIndex += indexCount;
            Vector3[] vertices = new Vector3[m_vTopIndex];
            Vector2[] uvs = new Vector2[m_vTopIndex];
            Color[] colors = new Color[m_vTopIndex];
            int[] indices = new int[m_iTopIndex];
            Vector2[] subUvs = null;
            if (isUsableSubUVsFix) {
                subUvs = new Vector2[m_vTopIndex];
            }
            Vector3[] normals = null;
            if (isUsableNormalsFix) {
                normals = new Vector3[m_vTopIndex];
            }
            if (meshBufferCount != 0) {
                m_vertices.CopyTo(vertices, 0);
                m_UVs.CopyTo(uvs, 0);
                m_colors.CopyTo(colors, 0);
                m_Indices.CopyTo(indices, 0);
                if (isUsableSubUVsFix) {
                    m_SubUVs.CopyTo(subUvs, 0);
                }
                if (isUsableNormalsFix) {
                    m_normals.CopyTo(normals, 0);
                }
            }
            m_vertices = vertices;
            m_UVs = uvs;
            m_colors = colors;
            m_Indices = indices;
            if (isUsableSubUVsFix) {
                m_SubUVs = subUvs;
            }
            if (isUsableNormalsFix) {
                m_normals = normals;
            }

            meshBuffer.meshBuffer = this;
            meshBuffer.enabled = false;
            m_MeshBufferList.Add(meshBuffer);
            meshBuffer.UpdateVertices();
            meshBuffer.UpdateUVs();
            meshBuffer.UpdateColors();
            meshBuffer.UpdateIndices();
            if (isUsableSubUVsFix) {
                meshBuffer.UpdateSubUVs();
            }
            if (isUsableNormalsFix) {
                meshBuffer.UpdateNormals();
            }
            bufferChangeFlg = true;
            return meshBuffer;
        }

        public virtual void RemoveBuffer(MeshBuffer meshBuffer) {
            meshBuffer.enabled = false;
            int idx = m_MeshBufferList.IndexOf(meshBuffer);
            if (idx > -1) {
                int vLength = m_vertices.Length - meshBuffer.vertices.Length;
                int iLength = m_Indices.Length - meshBuffer.indices.Length;
                m_vTopIndex -= meshBuffer.vertices.Length;
                m_iTopIndex -= meshBuffer.indices.Length;
                if (vLength > 0 && iLength > 0) {
                    m_vertices = new Vector3[vLength];
                    m_UVs = new Vector2[vLength];
                    m_colors = new Color[vLength];
                    m_Indices = new int[iLength];
                    if (isUsableSubUVsFix) {
                        m_SubUVs = new Vector2[vLength];
                    }
                    if (isUsableNormalsFix) {
                        m_normals = new Vector3[vLength];
                    }
                } else {
                    m_vertices = null;
                    m_UVs = null;
                    m_colors = null;
                    m_Indices = null;
                    if (isUsableSubUVsFix) {
                        m_SubUVs = null;
                    }
                    if (isUsableNormalsFix) {
                        m_normals = null;
                    }
                }

                m_MeshBufferList.RemoveAt(idx);

                int vIdx = 0;
                int iIdx = 0;
                foreach (MeshBuffer buffer in m_MeshBufferList) {
                    buffer.vTopIndex = vIdx;
                    buffer.iTopIndex = iIdx;
                    buffer.UpdateVertices();
                    buffer.UpdateUVs();
                    buffer.UpdateColors();
                    buffer.UpdateIndices();
                    if (isUsableSubUVsFix) {
                        buffer.UpdateSubUVs();
                    }
                    if (isUsableNormalsFix) {
                        buffer.UpdateNormals();
                    }
                    bufferChangeFlg = true;
                    vIdx += buffer.vertices.Length;
                    iIdx += buffer.indices.Length;
                }
            }

            if (m_Mesh.triangles != null && m_Mesh.triangles.Length <= 0) {
                m_Mesh.SetIndices(new int[3], MeshTopology.Triangles, 0);
            }
        }

        public virtual void RemoveBuffer(MeshBuffer[] meshBuffer) {
            int vLength = m_vertices.Length;
            int iLength = m_Indices.Length;
            for (int i = 0; i < meshBuffer.Length; i++) {
                meshBuffer[i].enabled = false;
                int idx = m_MeshBufferList.IndexOf(meshBuffer[i]);
                if (idx > -1) {
                    vLength -= meshBuffer[i].vertices.Length;
                    iLength -= meshBuffer[i].indices.Length;
                    m_vTopIndex -= meshBuffer[i].vertices.Length;
                    m_iTopIndex -= meshBuffer[i].indices.Length;
                    m_MeshBufferList.RemoveAt(idx);
                }
            }

            if (vLength > 0 && iLength > 0) {
                m_vertices = new Vector3[vLength];
                m_UVs = new Vector2[vLength];
                m_colors = new Color[vLength];
                m_Indices = new int[iLength];
                if (isUsableSubUVsFix) {
                    m_SubUVs = new Vector2[vLength];
                }
                if (isUsableNormalsFix) {
                    m_normals = new Vector3[vLength];
                }
            } else {
                m_vertices = null;
                m_UVs = null;
                m_colors = null;
                m_Indices = null;
                if (isUsableSubUVsFix) {
                    m_SubUVs = null;
                }
                if (isUsableNormalsFix) {
                    m_normals = null;
                }
            }

            int vIdx = 0;
            int iIdx = 0;
            foreach (MeshBuffer buffer in m_MeshBufferList) {
                buffer.vTopIndex = vIdx;
                buffer.iTopIndex = iIdx;
                buffer.UpdateVertices();
                buffer.UpdateUVs();
                buffer.UpdateColors();
                buffer.UpdateIndices();
                if (isUsableSubUVsFix) {
                    buffer.UpdateSubUVs();
                }
                if (isUsableNormalsFix) {
                    buffer.UpdateNormals();
                }
                bufferChangeFlg = true;
                vIdx += buffer.vertices.Length;
                iIdx += buffer.indices.Length;
            }

            if (m_Mesh.triangles != null && m_Mesh.triangles.Length <= 0) {
                m_Mesh.SetIndices(new int[3], MeshTopology.Triangles, 0);
            }
        }

        internal void SetVertices(MeshBuffer meshBuffer) {
            if (meshBuffer.enabled) {
                meshBuffer.vertices.CopyTo(m_vertices, meshBuffer.vTopIndex);
            } else {
                for (int i = 0; i < meshBuffer.vertices.Length; i++) {
                    m_vertices[i + meshBuffer.vTopIndex] = Vector3.zero;
                }
            }
            meshBuffer.isDirtyVerts = true;
            m_vertsChanged = true;
        }

        internal void SetNormals(MeshBuffer meshBuffer) {
            meshBuffer.normals.CopyTo(m_normals, meshBuffer.vTopIndex);
            meshBuffer.isDirtyNorms = true;
            m_normalsChanged = true;
        }

        internal void SetUVs(MeshBuffer meshBuffer) {
            meshBuffer.UVs.CopyTo(m_UVs, meshBuffer.vTopIndex);
            meshBuffer.isDirtyUVs = true;
            m_uvsChanged = true;
        }

        internal void SetColors(MeshBuffer meshBuffer) {
            if (meshBuffer.enabled) {
                meshBuffer.colors.CopyTo(m_colors, meshBuffer.vTopIndex);
                meshBuffer.isDirtyColors = true;
            }
            m_colorsChanged = true;
        }

        internal void SetIndices(MeshBuffer meshBuffer) {
            for (int i = 0; i < meshBuffer.indices.Length; i++) {
                m_Indices[i + meshBuffer.iTopIndex] = meshBuffer.indices[i] + meshBuffer.vTopIndex;
            }
            meshBuffer.isDirtyIndices = true;
            m_IndicesChanged = true;
        }

        internal void SetSubUVs(MeshBuffer meshBuffer) {
            if (isUsableSubUVsFix) {
                meshBuffer.SubUVs.CopyTo(m_SubUVs, meshBuffer.vTopIndex);
                meshBuffer.isDirtySubUVs = true;
                m_subuvsChanged = true;
            }
        }

        internal void SetRepeatUVRegion(MeshBuffer meshBuffer) {
            if (isUsableUVRegionRepeat) {
                meshBuffer.repeatUVRegion.CopyTo(m_RepeatUVRegion.ToArray(), meshBuffer.vTopIndex);
                meshBuffer.isDirtyRepeatUVRegion = true;
                m_RepeatUVRegionChanged = true;
            }
        }

        protected MeshTopology[] m_MeshTopologyTable = {
            MeshTopology.Triangles,
            MeshTopology.Quads,
            MeshTopology.Lines,
            MeshTopology.Points,
        };

        public enum Topology {
            Triangles,
            Quads,
            Lines,
            Points,
            Max
        }

        public class MeshBuffer {
            internal EffectMeshBuffer meshBuffer;
            internal int vTopIndex;
            internal int iTopIndex;
            internal bool isDirtyVerts;
            internal bool isDirtyNorms;
            internal bool isDirtyUVs;
            internal bool isDirtySubUVs;
            internal bool isDirtyColors;
            internal bool isDirtyIndices;
            internal bool isDirtyRepeatUVRegion;

            public Vector3[] vertices;
            public Vector3[] normals;
            public Vector2[] UVs;
            public Vector2[] SubUVs;
            public Color[] colors;
            public int[] indices;
            public List<Vector4> repeatUVRegion;

            private bool m_enabled;

            public bool enabled {
                get => m_enabled;
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        meshBuffer.RenderFlg = true;
                        meshBuffer.AddActiveBufferList(this);
                    } else {
                        meshBuffer.RemoveActiveBufferList(this);
                    }
                    meshBuffer.m_maxTriangleBufferChanged = true;
                    UpdateVertices();
                    UpdateColors();
                }
            }

            public void Remove() {
                meshBuffer.RemoveBuffer(this);
            }

            public void UpdateVertices() {
                meshBuffer.SetVertices(this);
            }

            public void UpdateNormals() {
                meshBuffer.SetNormals(this);
            }

            public void UpdateUVs() {
                meshBuffer.SetUVs(this);
            }

            public void UpdateSubUVs() {
                meshBuffer.SetSubUVs(this);
            }

            public void UpdateColors() {
                meshBuffer.SetColors(this);
            }

            public void UpdateIndices() {
                meshBuffer.SetIndices(this);
            }

            public void UpdateRepeatUVRegion() {
                meshBuffer.SetRepeatUVRegion(this);
            }

            public void Minimize() {
                for (int i = 0; i < vertices.Length; i++) {
                    vertices[i] = Vector3.zero;
                }
                UpdateVertices();
            }
        }
    }
}
