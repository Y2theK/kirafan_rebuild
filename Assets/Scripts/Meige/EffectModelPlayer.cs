﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x0200120F RID: 4623
	[Token(Token = "0x2000BFD")]
	[StructLayout(3)]
	public class EffectModelPlayer : EffectComponentBase
	{
		// Token: 0x1700064C RID: 1612
		// (get) Token: 0x06005EA2 RID: 24226 RVA: 0x0001EEB8 File Offset: 0x0001D0B8
		// (set) Token: 0x06005EA1 RID: 24225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005EF")]
		public override bool isActive
		{
			[Token(Token = "0x600578D")]
			[Address(RVA = "0x10136CD4C", Offset = "0x136CD4C", VA = "0x10136CD4C", Slot = "7")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600578C")]
			[Address(RVA = "0x10136CC74", Offset = "0x136CC74", VA = "0x10136CC74", Slot = "6")]
			set
			{
			}
		}

		// Token: 0x1700064D RID: 1613
		// (get) Token: 0x06005EA4 RID: 24228 RVA: 0x0001EED0 File Offset: 0x0001D0D0
		// (set) Token: 0x06005EA3 RID: 24227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005F0")]
		public int clipIdx
		{
			[Token(Token = "0x600578F")]
			[Address(RVA = "0x10136CD5C", Offset = "0x136CD5C", VA = "0x10136CD5C")]
			get
			{
				return 0;
			}
			[Token(Token = "0x600578E")]
			[Address(RVA = "0x10136CD54", Offset = "0x136CD54", VA = "0x10136CD54")]
			set
			{
			}
		}

		// Token: 0x1700064E RID: 1614
		// (get) Token: 0x06005EA5 RID: 24229 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005F1")]
		public MsbHandler msbHandler
		{
			[Token(Token = "0x6005790")]
			[Address(RVA = "0x10136CD64", Offset = "0x136CD64", VA = "0x10136CD64")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700064F RID: 1615
		// (get) Token: 0x06005EA6 RID: 24230 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005F2")]
		public MeigeAnimClipHolder[] meigeAnimClipHolder
		{
			[Token(Token = "0x6005791")]
			[Address(RVA = "0x10136CD6C", Offset = "0x136CD6C", VA = "0x10136CD6C")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005EA7 RID: 24231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005792")]
		[Address(RVA = "0x10136CD74", Offset = "0x136CD74", VA = "0x10136CD74")]
		public void SetMaterialColor(Color color)
		{
		}

		// Token: 0x06005EA8 RID: 24232 RVA: 0x0001EEE8 File Offset: 0x0001D0E8
		[Token(Token = "0x6005793")]
		[Address(RVA = "0x10136CD88", Offset = "0x136CD88", VA = "0x10136CD88")]
		public bool Setup(MsbHandler msb, MeigeAnimClipHolder[] animes)
		{
			return default(bool);
		}

		// Token: 0x06005EA9 RID: 24233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005794")]
		[Address(RVA = "0x10136D08C", Offset = "0x136D08C", VA = "0x10136D08C")]
		private void Start()
		{
		}

		// Token: 0x06005EAA RID: 24234 RVA: 0x0001EF00 File Offset: 0x0001D100
		[Token(Token = "0x6005795")]
		[Address(RVA = "0x10136D268", Offset = "0x136D268", VA = "0x10136D268", Slot = "22")]
		public override bool Kill()
		{
			return default(bool);
		}

		// Token: 0x06005EAB RID: 24235 RVA: 0x0001EF18 File Offset: 0x0001D118
		[Token(Token = "0x6005796")]
		[Address(RVA = "0x10136D410", Offset = "0x136D410", VA = "0x10136D410", Slot = "20")]
		public override bool Shot()
		{
			return default(bool);
		}

		// Token: 0x06005EAC RID: 24236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005797")]
		[Address(RVA = "0x10136D4F8", Offset = "0x136D4F8", VA = "0x10136D4F8")]
		private void Update()
		{
		}

		// Token: 0x06005EAD RID: 24237 RVA: 0x0001EF30 File Offset: 0x0001D130
		[Token(Token = "0x6005798")]
		[Address(RVA = "0x10136D754", Offset = "0x136D754", VA = "0x10136D754", Slot = "8")]
		public override int GetPropertyNum()
		{
			return 0;
		}

		// Token: 0x06005EAE RID: 24238 RVA: 0x0001EF48 File Offset: 0x0001D148
		[Token(Token = "0x6005799")]
		[Address(RVA = "0x10136D75C", Offset = "0x136D75C", VA = "0x10136D75C", Slot = "9")]
		public override int GetArrayNum(int propertyIdx)
		{
			return 0;
		}

		// Token: 0x06005EAF RID: 24239 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600579A")]
		[Address(RVA = "0x10136D764", Offset = "0x136D764", VA = "0x10136D764", Slot = "10")]
		public override object GetValue(int propertyIdx, int arrayIdx)
		{
			return null;
		}

		// Token: 0x06005EB0 RID: 24240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600579B")]
		[Address(RVA = "0x10136D7C8", Offset = "0x136D7C8", VA = "0x10136D7C8", Slot = "11")]
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value)
		{
		}

		// Token: 0x06005EB1 RID: 24241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600579C")]
		[Address(RVA = "0x10136D830", Offset = "0x136D830", VA = "0x10136D830", Slot = "12")]
		public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value)
		{
		}

		// Token: 0x06005EB2 RID: 24242 RVA: 0x0001EF60 File Offset: 0x0001D160
		[Token(Token = "0x600579D")]
		[Address(RVA = "0x10136D8C0", Offset = "0x136D8C0", VA = "0x10136D8C0", Slot = "13")]
		public override eEffectAnimTypeCode GetTypeCode(int propertyIdx)
		{
			return eEffectAnimTypeCode.Bool;
		}

		// Token: 0x06005EB3 RID: 24243 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600579E")]
		[Address(RVA = "0x10136D8C8", Offset = "0x136D8C8", VA = "0x10136D8C8", Slot = "14")]
		public override Type GetPropertType(int propertyIdx)
		{
			return null;
		}

		// Token: 0x06005EB4 RID: 24244 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600579F")]
		[Address(RVA = "0x10136D938", Offset = "0x136D938", VA = "0x10136D938", Slot = "15")]
		public override string GetPropertyName(int propertyIdx)
		{
			return null;
		}

		// Token: 0x06005EB5 RID: 24245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057A0")]
		[Address(RVA = "0x10136D980", Offset = "0x136D980", VA = "0x10136D980")]
		public EffectModelPlayer()
		{
		}

		// Token: 0x04006ED5 RID: 28373
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E92")]
		[SerializeField]
		private GameObject m_ModelDataPrefab;

		// Token: 0x04006ED6 RID: 28374
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E93")]
		[SerializeField]
		private MeigeAnimClipHolder[] m_AnimPrefab;

		// Token: 0x04006ED7 RID: 28375
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004E94")]
		[SerializeField]
		private int m_PlayAnimIdx;

		// Token: 0x04006ED8 RID: 28376
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004E95")]
		private GameObject m_MsbHandlerObject;

		// Token: 0x04006ED9 RID: 28377
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004E96")]
		private MsbHandler m_MsbHandler;

		// Token: 0x04006EDA RID: 28378
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004E97")]
		private MeigeAnimClipHolder[] m_Animes;

		// Token: 0x04006EDB RID: 28379
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004E98")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04006EDC RID: 28380
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004E99")]
		private GameObject m_ThisGameObject;

		// Token: 0x04006EDD RID: 28381
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004E9A")]
		public Color m_Color;

		// Token: 0x04006EDE RID: 28382
		[Token(Token = "0x4004E9B")]
		private const string PLAY_KEY = "Take 001";
	}
}
