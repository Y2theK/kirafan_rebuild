﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige {
    public class EffectObjectManager : MonoBehaviour {
        private static EffectObjectManager m_Instance;

        private List<EffectMeshBuffer> m_MeshBufferList = new List<EffectMeshBuffer>();
        private List<EffectRenderer> m_RendererList = new List<EffectRenderer>();

        [NonSerialized] public Transform m_ThisTransform;

        [SerializeField] private bool m_IsDontDestroyOnLoad = true;

        public static EffectObjectManager Instance => m_Instance;

        private void Awake() {
            if (m_Instance != null) {
                Helper.DestroyAll(gameObject);
                return;
            }

            m_Instance = this;
            m_ThisTransform = transform;
            m_ThisTransform.position = Vector3.zero;
            m_ThisTransform.eulerAngles = Vector3.zero;
            m_ThisTransform.localScale = Vector3.one;
            if (m_IsDontDestroyOnLoad) {
                DontDestroyOnLoad(this);
            }
        }

        private void Update() {
            foreach (EffectMeshBuffer effectMeshBuffer in m_MeshBufferList) { //why?
            }
        }

        public bool RegisterMeshBufferComponent(EffectMeshBuffer meshBuffer) {
            m_MeshBufferList.Add(meshBuffer);
            return true;
        }

        public bool RemoveMeshBufferComponent(EffectMeshBuffer meshBuffer) {
            int idx = m_MeshBufferList.IndexOf(meshBuffer);
            if (idx >= 0) {
                m_MeshBufferList.RemoveAt(idx);
                return true;
            }
            return false;
        }

        public bool RegisterRendererComponent(EffectRenderer renderer) {
            m_RendererList.Add(renderer);
            return true;
        }

        public bool RemoveRendererComponent(EffectRenderer renderer) {
            int idx = m_RendererList.IndexOf(renderer);
            if (idx < 0) {
                m_RendererList.RemoveAt(idx);
                return true;
            }
            return false;
        }
    }
}
