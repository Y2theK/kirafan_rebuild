﻿using UnityEngine;

namespace Meige {
    public class EffectRenderer : MonoBehaviour {
        [SerializeField] private Material m_Material;

        private MeshRenderer m_MeshRenderer;
        private bool m_isEnabled = true;
        private int m_SortingOrder;

        private void Start() {
            EffectObjectManager.Instance.RegisterRendererComponent(this);
            if (m_MeshRenderer == null) {
                m_MeshRenderer = gameObject.AddComponent<MeshRenderer>();
                m_MeshRenderer.sortingOrder = m_SortingOrder;
                m_MeshRenderer.enabled = m_isEnabled;
            }
            enabled = m_isEnabled;
        }

        private void OnDestroy() {
            if (EffectObjectManager.Instance != null) {
                EffectObjectManager.Instance.RemoveRendererComponent(this);
            }
        }

        private void OnEnable() {
            if (m_MeshRenderer != null) {
                m_MeshRenderer.enabled = true;
            }
            m_isEnabled = true;
        }

        private void OnDisable() {
            if (m_MeshRenderer != null) {
                m_MeshRenderer.enabled = false;
            }
            m_isEnabled = false;
        }

        public Material material {
            get => m_Material;
            set {
                m_Material = value;
                if (m_MeshRenderer == null) {
                    m_MeshRenderer = gameObject.AddComponent<MeshRenderer>();
                    m_MeshRenderer.sortingOrder = m_SortingOrder;
                }
                m_MeshRenderer.material = m_Material;
            }
        }

        public void SetSortingOrder(int sortingOrder) {
            m_SortingOrder = sortingOrder;
            if (m_MeshRenderer != null) {
                m_MeshRenderer.sortingOrder = m_SortingOrder;
            }
        }
    }
}
