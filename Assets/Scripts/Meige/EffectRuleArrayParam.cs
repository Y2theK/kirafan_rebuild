﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001214 RID: 4628
	[Token(Token = "0x2000C02")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam<TYPE> : EffectRuleParamBase where TYPE : new()
	{
		// Token: 0x06005EDB RID: 24283 RVA: 0x0001F050 File Offset: 0x0001D250
		[Token(Token = "0x60057C6")]
		[Address(RVA = "0x1016AFCE0", Offset = "0x16AFCE0", VA = "0x1016AFCE0", Slot = "4")]
		public eEffectAnimTypeCode GetTypeCode()
		{
			return eEffectAnimTypeCode.Bool;
		}

		// Token: 0x06005EDC RID: 24284 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057C7")]
		[Address(RVA = "0x1016AFCE8", Offset = "0x16AFCE8", VA = "0x1016AFCE8", Slot = "5")]
		public Type GetOrType()
		{
			return null;
		}

		// Token: 0x06005EDD RID: 24285 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057C8")]
		[Address(RVA = "0x1016AFCF0", Offset = "0x16AFCF0", VA = "0x1016AFCF0", Slot = "6")]
		public object GetValue(int tgtIdx)
		{
			return null;
		}

		// Token: 0x06005EDE RID: 24286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057C9")]
		[Address(RVA = "0x1016AFD6C", Offset = "0x16AFD6C", VA = "0x1016AFD6C", Slot = "7")]
		public void SetValue(int tgtIdx, object value)
		{
		}

		// Token: 0x06005EDF RID: 24287 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057CA")]
		[Address(RVA = "0x1016AFDF4", Offset = "0x16AFDF4", VA = "0x1016AFDF4", Slot = "8")]
		public object GetValue(int arrayIdx, int tgtIdx)
		{
			return null;
		}

		// Token: 0x06005EE0 RID: 24288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057CB")]
		[Address(RVA = "0x1016AFE88", Offset = "0x16AFE88", VA = "0x1016AFE88", Slot = "9")]
		public void SetValue(int arrayIdx, int tgtIdx, object value)
		{
		}

		// Token: 0x06005EE1 RID: 24289 RVA: 0x0001F068 File Offset: 0x0001D268
		[Token(Token = "0x60057CC")]
		[Address(RVA = "0x1016AFF1C", Offset = "0x16AFF1C", VA = "0x1016AFF1C", Slot = "10")]
		public bool IsArray()
		{
			return default(bool);
		}

		// Token: 0x06005EE2 RID: 24290 RVA: 0x0001F080 File Offset: 0x0001D280
		[Token(Token = "0x60057CD")]
		[Address(RVA = "0x1016AFF24", Offset = "0x16AFF24", VA = "0x1016AFF24", Slot = "11")]
		public int GetArraySize()
		{
			return 0;
		}

		// Token: 0x06005EE3 RID: 24291 RVA: 0x0001F098 File Offset: 0x0001D298
		[Token(Token = "0x60057CE")]
		[Address(RVA = "0x1016AFF2C", Offset = "0x16AFF2C", VA = "0x1016AFF2C", Slot = "12")]
		public bool ChangeArraySize(int size)
		{
			return default(bool);
		}

		// Token: 0x06005EE4 RID: 24292 RVA: 0x0001F0B0 File Offset: 0x0001D2B0
		[Token(Token = "0x60057CF")]
		[Address(RVA = "0x1016AFF34", Offset = "0x16AFF34", VA = "0x1016AFF34")]
		private static eEffectAnimTypeCode GetTypeCode(Type type)
		{
			return eEffectAnimTypeCode.Bool;
		}

		// Token: 0x06005EE5 RID: 24293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D0")]
		[Address(RVA = "0x1016B0028", Offset = "0x16B0028", VA = "0x1016B0028")]
		public EffectRuleArrayParam()
		{
		}

		// Token: 0x04006EEB RID: 28395
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EA8")]
		private eEffectAnimTypeCode m_TypeCode;

		// Token: 0x04006EEC RID: 28396
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EA9")]
		private Type m_OrType;

		// Token: 0x04006EED RID: 28397
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EAA")]
		public TYPE[] m_Value;
	}
}
