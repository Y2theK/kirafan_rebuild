﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001236 RID: 4662
	[Token(Token = "0x2000C24")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Bool : EffectRuleArrayParam<bool>
	{
		// Token: 0x06005F07 RID: 24327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F2")]
		[Address(RVA = "0x10136E560", Offset = "0x136E560", VA = "0x10136E560")]
		public EffectRuleArrayParam_Bool()
		{
		}
	}
}
