﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001237 RID: 4663
	[Token(Token = "0x2000C25")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Byte : EffectRuleArrayParam<sbyte>
	{
		// Token: 0x06005F08 RID: 24328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F3")]
		[Address(RVA = "0x10136E5B0", Offset = "0x136E5B0", VA = "0x10136E5B0")]
		public EffectRuleArrayParam_Byte()
		{
		}
	}
}
