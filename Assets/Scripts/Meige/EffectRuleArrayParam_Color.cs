﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001244 RID: 4676
	[Token(Token = "0x2000C32")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Color : EffectRuleArrayParam<Color>
	{
		// Token: 0x06005F15 RID: 24341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005800")]
		[Address(RVA = "0x10136E600", Offset = "0x136E600", VA = "0x10136E600")]
		public EffectRuleArrayParam_Color()
		{
		}
	}
}
