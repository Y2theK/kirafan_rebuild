﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001240 RID: 4672
	[Token(Token = "0x2000C2E")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Double : EffectRuleArrayParam<double>
	{
		// Token: 0x06005F11 RID: 24337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FC")]
		[Address(RVA = "0x10136E650", Offset = "0x136E650", VA = "0x10136E650")]
		public EffectRuleArrayParam_Double()
		{
		}
	}
}
