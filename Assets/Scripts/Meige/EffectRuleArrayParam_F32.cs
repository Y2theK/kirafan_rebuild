﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001246 RID: 4678
	[Token(Token = "0x2000C34")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_F32 : EffectRuleArrayParam<f32>
	{
		// Token: 0x06005F17 RID: 24343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005802")]
		[Address(RVA = "0x10136E6A0", Offset = "0x136E6A0", VA = "0x10136E6A0")]
		public EffectRuleArrayParam_F32()
		{
		}
	}
}
