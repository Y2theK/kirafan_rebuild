﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123F RID: 4671
	[Token(Token = "0x2000C2D")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Float : EffectRuleArrayParam<float>
	{
		// Token: 0x06005F10 RID: 24336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FB")]
		[Address(RVA = "0x10136E6F0", Offset = "0x136E6F0", VA = "0x10136E6F0")]
		public EffectRuleArrayParam_Float()
		{
		}
	}
}
