﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001247 RID: 4679
	[Token(Token = "0x2000C35")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Float2 : EffectRuleArrayParam<Float2>
	{
		// Token: 0x06005F18 RID: 24344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005803")]
		[Address(RVA = "0x10136E740", Offset = "0x136E740", VA = "0x10136E740")]
		public EffectRuleArrayParam_Float2()
		{
		}
	}
}
