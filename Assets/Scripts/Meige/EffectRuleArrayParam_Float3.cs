﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001248 RID: 4680
	[Token(Token = "0x2000C36")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Float3 : EffectRuleArrayParam<Float3>
	{
		// Token: 0x06005F19 RID: 24345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005804")]
		[Address(RVA = "0x10136E790", Offset = "0x136E790", VA = "0x10136E790")]
		public EffectRuleArrayParam_Float3()
		{
		}
	}
}
