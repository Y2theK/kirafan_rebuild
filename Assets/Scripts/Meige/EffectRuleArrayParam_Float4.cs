﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001249 RID: 4681
	[Token(Token = "0x2000C37")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Float4 : EffectRuleArrayParam<Float4>
	{
		// Token: 0x06005F1A RID: 24346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005805")]
		[Address(RVA = "0x10136E7E0", Offset = "0x136E7E0", VA = "0x10136E7E0")]
		public EffectRuleArrayParam_Float4()
		{
		}
	}
}
