﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123B RID: 4667
	[Token(Token = "0x2000C29")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Int : EffectRuleArrayParam<int>
	{
		// Token: 0x06005F0C RID: 24332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F7")]
		[Address(RVA = "0x10136E830", Offset = "0x136E830", VA = "0x10136E830")]
		public EffectRuleArrayParam_Int()
		{
		}
	}
}
