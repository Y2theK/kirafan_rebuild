﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123D RID: 4669
	[Token(Token = "0x2000C2B")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Long : EffectRuleArrayParam<long>
	{
		// Token: 0x06005F0E RID: 24334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F9")]
		[Address(RVA = "0x10136E880", Offset = "0x136E880", VA = "0x10136E880")]
		public EffectRuleArrayParam_Long()
		{
		}
	}
}
