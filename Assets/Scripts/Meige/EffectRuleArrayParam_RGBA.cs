﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124A RID: 4682
	[Token(Token = "0x2000C38")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RGBA : EffectRuleArrayParam<RGBA>
	{
		// Token: 0x06005F1B RID: 24347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005806")]
		[Address(RVA = "0x10136E8D0", Offset = "0x136E8D0", VA = "0x10136E8D0")]
		public EffectRuleArrayParam_RGBA()
		{
		}
	}
}
