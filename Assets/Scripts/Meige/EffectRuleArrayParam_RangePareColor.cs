﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001250 RID: 4688
	[Token(Token = "0x2000C3E")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareColor : EffectRuleArrayParam<RangePareColor>
	{
		// Token: 0x06005F21 RID: 24353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580C")]
		[Address(RVA = "0x10136E920", Offset = "0x136E920", VA = "0x10136E920")]
		public EffectRuleArrayParam_RangePareColor()
		{
		}
	}
}
