﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124B RID: 4683
	[Token(Token = "0x2000C39")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareFloat : EffectRuleArrayParam<RangePareFloat>
	{
		// Token: 0x06005F1C RID: 24348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005807")]
		[Address(RVA = "0x10136E970", Offset = "0x136E970", VA = "0x10136E970")]
		public EffectRuleArrayParam_RangePareFloat()
		{
		}
	}
}
