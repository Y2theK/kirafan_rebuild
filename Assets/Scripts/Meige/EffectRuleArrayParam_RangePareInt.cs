﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124C RID: 4684
	[Token(Token = "0x2000C3A")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareInt : EffectRuleArrayParam<RangePareInt>
	{
		// Token: 0x06005F1D RID: 24349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005808")]
		[Address(RVA = "0x10136E9C0", Offset = "0x136E9C0", VA = "0x10136E9C0")]
		public EffectRuleArrayParam_RangePareInt()
		{
		}
	}
}
