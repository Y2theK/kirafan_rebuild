﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124D RID: 4685
	[Token(Token = "0x2000C3B")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareVector2 : EffectRuleArrayParam<RangePareVector2>
	{
		// Token: 0x06005F1E RID: 24350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005809")]
		[Address(RVA = "0x10136EA10", Offset = "0x136EA10", VA = "0x10136EA10")]
		public EffectRuleArrayParam_RangePareVector2()
		{
		}
	}
}
