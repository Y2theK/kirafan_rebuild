﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124E RID: 4686
	[Token(Token = "0x2000C3C")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareVector3 : EffectRuleArrayParam<RangePareVector3>
	{
		// Token: 0x06005F1F RID: 24351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580A")]
		[Address(RVA = "0x10136EA60", Offset = "0x136EA60", VA = "0x10136EA60")]
		public EffectRuleArrayParam_RangePareVector3()
		{
		}
	}
}
