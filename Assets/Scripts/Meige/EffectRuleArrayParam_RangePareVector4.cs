﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200124F RID: 4687
	[Token(Token = "0x2000C3D")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangePareVector4 : EffectRuleArrayParam<RangePareVector4>
	{
		// Token: 0x06005F20 RID: 24352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580B")]
		[Address(RVA = "0x10136EAB0", Offset = "0x136EAB0", VA = "0x10136EAB0")]
		public EffectRuleArrayParam_RangePareVector4()
		{
		}
	}
}
