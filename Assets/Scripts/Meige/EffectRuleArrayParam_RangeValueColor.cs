﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001256 RID: 4694
	[Token(Token = "0x2000C44")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueColor : EffectRuleArrayParam<RangeValueColor>
	{
		// Token: 0x06005F27 RID: 24359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005812")]
		[Address(RVA = "0x10136EB00", Offset = "0x136EB00", VA = "0x10136EB00")]
		public EffectRuleArrayParam_RangeValueColor()
		{
		}
	}
}
