﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001252 RID: 4690
	[Token(Token = "0x2000C40")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueFloat : EffectRuleArrayParam<RangeValueFloat>
	{
		// Token: 0x06005F23 RID: 24355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580E")]
		[Address(RVA = "0x10136EB50", Offset = "0x136EB50", VA = "0x10136EB50")]
		public EffectRuleArrayParam_RangeValueFloat()
		{
		}
	}
}
