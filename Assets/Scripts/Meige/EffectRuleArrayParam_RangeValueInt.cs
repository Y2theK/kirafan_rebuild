﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001251 RID: 4689
	[Token(Token = "0x2000C3F")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueInt : EffectRuleArrayParam<RangeValueInt>
	{
		// Token: 0x06005F22 RID: 24354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580D")]
		[Address(RVA = "0x10136EBA0", Offset = "0x136EBA0", VA = "0x10136EBA0")]
		public EffectRuleArrayParam_RangeValueInt()
		{
		}
	}
}
