﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001253 RID: 4691
	[Token(Token = "0x2000C41")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueVector2 : EffectRuleArrayParam<RangeValueVector2>
	{
		// Token: 0x06005F24 RID: 24356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600580F")]
		[Address(RVA = "0x10136EBF0", Offset = "0x136EBF0", VA = "0x10136EBF0")]
		public EffectRuleArrayParam_RangeValueVector2()
		{
		}
	}
}
