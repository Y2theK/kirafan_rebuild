﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001254 RID: 4692
	[Token(Token = "0x2000C42")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueVector3 : EffectRuleArrayParam<RangeValueVector3>
	{
		// Token: 0x06005F25 RID: 24357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005810")]
		[Address(RVA = "0x10136EC40", Offset = "0x136EC40", VA = "0x10136EC40")]
		public EffectRuleArrayParam_RangeValueVector3()
		{
		}
	}
}
