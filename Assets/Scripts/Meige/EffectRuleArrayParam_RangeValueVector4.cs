﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001255 RID: 4693
	[Token(Token = "0x2000C43")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_RangeValueVector4 : EffectRuleArrayParam<RangeValueVector4>
	{
		// Token: 0x06005F26 RID: 24358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005811")]
		[Address(RVA = "0x10136EC90", Offset = "0x136EC90", VA = "0x10136EC90")]
		public EffectRuleArrayParam_RangeValueVector4()
		{
		}
	}
}
