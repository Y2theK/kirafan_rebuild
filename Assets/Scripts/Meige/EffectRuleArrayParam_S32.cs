﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001245 RID: 4677
	[Token(Token = "0x2000C33")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_S32 : EffectRuleArrayParam<s32>
	{
		// Token: 0x06005F16 RID: 24342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005801")]
		[Address(RVA = "0x10136ECE0", Offset = "0x136ECE0", VA = "0x10136ECE0")]
		public EffectRuleArrayParam_S32()
		{
		}
	}
}
