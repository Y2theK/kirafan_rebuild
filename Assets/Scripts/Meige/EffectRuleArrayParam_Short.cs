﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001239 RID: 4665
	[Token(Token = "0x2000C27")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Short : EffectRuleArrayParam<short>
	{
		// Token: 0x06005F0A RID: 24330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F5")]
		[Address(RVA = "0x10136ED30", Offset = "0x136ED30", VA = "0x10136ED30")]
		public EffectRuleArrayParam_Short()
		{
		}
	}
}
