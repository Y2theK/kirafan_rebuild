﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001238 RID: 4664
	[Token(Token = "0x2000C26")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Ubyte : EffectRuleArrayParam<byte>
	{
		// Token: 0x06005F09 RID: 24329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F4")]
		[Address(RVA = "0x10136ED80", Offset = "0x136ED80", VA = "0x10136ED80")]
		public EffectRuleArrayParam_Ubyte()
		{
		}
	}
}
