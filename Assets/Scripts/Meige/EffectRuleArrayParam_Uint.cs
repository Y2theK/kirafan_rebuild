﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123C RID: 4668
	[Token(Token = "0x2000C2A")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Uint : EffectRuleArrayParam<uint>
	{
		// Token: 0x06005F0D RID: 24333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F8")]
		[Address(RVA = "0x10136EDD0", Offset = "0x136EDD0", VA = "0x10136EDD0")]
		public EffectRuleArrayParam_Uint()
		{
		}
	}
}
