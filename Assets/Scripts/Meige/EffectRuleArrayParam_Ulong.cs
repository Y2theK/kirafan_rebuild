﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123E RID: 4670
	[Token(Token = "0x2000C2C")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Ulong : EffectRuleArrayParam<ulong>
	{
		// Token: 0x06005F0F RID: 24335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FA")]
		[Address(RVA = "0x10136EE20", Offset = "0x136EE20", VA = "0x10136EE20")]
		public EffectRuleArrayParam_Ulong()
		{
		}
	}
}
