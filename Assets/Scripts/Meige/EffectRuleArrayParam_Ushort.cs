﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200123A RID: 4666
	[Token(Token = "0x2000C28")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Ushort : EffectRuleArrayParam<ushort>
	{
		// Token: 0x06005F0B RID: 24331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F6")]
		[Address(RVA = "0x10136EE70", Offset = "0x136EE70", VA = "0x10136EE70")]
		public EffectRuleArrayParam_Ushort()
		{
		}
	}
}
