﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001241 RID: 4673
	[Token(Token = "0x2000C2F")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Vector2 : EffectRuleArrayParam<Vector2>
	{
		// Token: 0x06005F12 RID: 24338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FD")]
		[Address(RVA = "0x10136EEC0", Offset = "0x136EEC0", VA = "0x10136EEC0")]
		public EffectRuleArrayParam_Vector2()
		{
		}
	}
}
