﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001242 RID: 4674
	[Token(Token = "0x2000C30")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Vector3 : EffectRuleArrayParam<Vector3>
	{
		// Token: 0x06005F13 RID: 24339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FE")]
		[Address(RVA = "0x10136EF10", Offset = "0x136EF10", VA = "0x10136EF10")]
		public EffectRuleArrayParam_Vector3()
		{
		}
	}
}
