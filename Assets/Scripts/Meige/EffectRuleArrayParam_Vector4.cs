﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001243 RID: 4675
	[Token(Token = "0x2000C31")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleArrayParam_Vector4 : EffectRuleArrayParam<Vector4>
	{
		// Token: 0x06005F14 RID: 24340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057FF")]
		[Address(RVA = "0x10136EF60", Offset = "0x136EF60", VA = "0x10136EF60")]
		public EffectRuleArrayParam_Vector4()
		{
		}
	}
}
