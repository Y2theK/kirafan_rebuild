﻿using System;

namespace Meige {
    [Serializable]
    public class EffectRuleBase {
        public virtual int GetPropertyNum() {
            return 0;
        }

        public virtual int GetArrayNum(int propertyIdx) {
            return 0;
        }

        public virtual object GetValue(int propertyIdx, int arrayIdx) {
            return null;
        }

        public virtual void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value) { }

        public virtual eEffectAnimTypeCode GetTypeCode(int propertyIdx) {
            return eEffectAnimTypeCode.Invalid;
        }

        public virtual string GetPropertyName(int propertyIdx) {
            return null;
        }

        public virtual EffectRuleParamBase GetParamBase(int propertyIdx) {
            return null;
        }
    }
}
