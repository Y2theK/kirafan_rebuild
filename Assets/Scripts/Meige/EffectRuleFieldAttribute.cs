﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001261 RID: 4705
	[Token(Token = "0x2000C4F")]
	[Attribute(Name = "AttributeUsageAttribute", RVA = "0x10011D674", Offset = "0x11D674")]
	[StructLayout(3)]
	public class EffectRuleFieldAttribute : Attribute
	{
		// Token: 0x06005F5D RID: 24413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005848")]
		[Address(RVA = "0x10136EFEC", Offset = "0x136EFEC", VA = "0x10136EFEC")]
		public EffectRuleFieldAttribute(string path, string fieldName)
		{
		}

		// Token: 0x06005F5E RID: 24414 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005849")]
		[Address(RVA = "0x10136F024", Offset = "0x136F024", VA = "0x10136F024")]
		public string GetPath()
		{
			return null;
		}

		// Token: 0x06005F5F RID: 24415 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600584A")]
		[Address(RVA = "0x10136F02C", Offset = "0x136F02C", VA = "0x10136F02C")]
		public string GetFieldName()
		{
			return null;
		}

		// Token: 0x04006F0E RID: 28430
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004ECB")]
		private string m_Path;

		// Token: 0x04006F0F RID: 28431
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004ECC")]
		private string m_FieldName;
	}
}
