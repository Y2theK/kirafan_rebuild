﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001213 RID: 4627
	[Token(Token = "0x2000C01")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam<TYPE> : EffectRuleParamBase where TYPE : new()
	{
		// Token: 0x06005ED0 RID: 24272 RVA: 0x0001EFD8 File Offset: 0x0001D1D8
		[Token(Token = "0x60057BB")]
		[Address(RVA = "0x1016B5554", Offset = "0x16B5554", VA = "0x1016B5554", Slot = "4")]
		public eEffectAnimTypeCode GetTypeCode()
		{
			return eEffectAnimTypeCode.Bool;
		}

		// Token: 0x06005ED1 RID: 24273 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057BC")]
		[Address(RVA = "0x1016B555C", Offset = "0x16B555C", VA = "0x1016B555C", Slot = "5")]
		public Type GetOrType()
		{
			return null;
		}

		// Token: 0x06005ED2 RID: 24274 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057BD")]
		[Address(RVA = "0x1016B5564", Offset = "0x16B5564", VA = "0x1016B5564", Slot = "6")]
		public object GetValue(int tgtIdx)
		{
			return null;
		}

		// Token: 0x06005ED3 RID: 24275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057BE")]
		[Address(RVA = "0x1016B55B4", Offset = "0x16B55B4", VA = "0x1016B55B4", Slot = "7")]
		public void SetValue(int tgtIdx, object value)
		{
		}

		// Token: 0x06005ED4 RID: 24276 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60057BF")]
		[Address(RVA = "0x1016B560C", Offset = "0x16B560C", VA = "0x1016B560C", Slot = "8")]
		public object GetValue(int arrayIdx, int tgtIdx)
		{
			return null;
		}

		// Token: 0x06005ED5 RID: 24277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057C0")]
		[Address(RVA = "0x1016B565C", Offset = "0x16B565C", VA = "0x1016B565C", Slot = "9")]
		public void SetValue(int arrayIdx, int tgtIdx, object value)
		{
		}

		// Token: 0x06005ED6 RID: 24278 RVA: 0x0001EFF0 File Offset: 0x0001D1F0
		[Token(Token = "0x60057C1")]
		[Address(RVA = "0x1016B56B4", Offset = "0x16B56B4", VA = "0x1016B56B4", Slot = "10")]
		public bool IsArray()
		{
			return default(bool);
		}

		// Token: 0x06005ED7 RID: 24279 RVA: 0x0001F008 File Offset: 0x0001D208
		[Token(Token = "0x60057C2")]
		[Address(RVA = "0x1016B56BC", Offset = "0x16B56BC", VA = "0x1016B56BC", Slot = "11")]
		public int GetArraySize()
		{
			return 0;
		}

		// Token: 0x06005ED8 RID: 24280 RVA: 0x0001F020 File Offset: 0x0001D220
		[Token(Token = "0x60057C3")]
		[Address(RVA = "0x1016B56C4", Offset = "0x16B56C4", VA = "0x1016B56C4", Slot = "12")]
		public bool ChangeArraySize(int size)
		{
			return default(bool);
		}

		// Token: 0x06005ED9 RID: 24281 RVA: 0x0001F038 File Offset: 0x0001D238
		[Token(Token = "0x60057C4")]
		[Address(RVA = "0x1016B56CC", Offset = "0x16B56CC", VA = "0x1016B56CC")]
		private static eEffectAnimTypeCode GetTypeCode(Type type)
		{
			return eEffectAnimTypeCode.Bool;
		}

		// Token: 0x06005EDA RID: 24282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057C5")]
		[Address(RVA = "0x1016B57C0", Offset = "0x16B57C0", VA = "0x1016B57C0")]
		public EffectRuleParam()
		{
		}

		// Token: 0x04006EE8 RID: 28392
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EA5")]
		private eEffectAnimTypeCode m_TypeCode;

		// Token: 0x04006EE9 RID: 28393
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EA6")]
		private Type m_OrType;

		// Token: 0x04006EEA RID: 28394
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EA7")]
		public TYPE m_Value;
	}
}
