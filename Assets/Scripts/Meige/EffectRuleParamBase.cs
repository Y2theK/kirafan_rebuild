﻿using System;

namespace Meige {
    public interface EffectRuleParamBase {
        eEffectAnimTypeCode GetTypeCode();
        Type GetOrType();
        object GetValue(int tgtIdx);
        void SetValue(int tgtIdx, object value);
        object GetValue(int arrayIdx, int tgtIdx);
        void SetValue(int arrayIdx, int tgtIdx, object value);
        bool IsArray();
        int GetArraySize();
        bool ChangeArraySize(int size);
    }
}
