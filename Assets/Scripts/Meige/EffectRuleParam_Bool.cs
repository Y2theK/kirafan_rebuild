﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001215 RID: 4629
	[Token(Token = "0x2000C03")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Bool : EffectRuleParam<bool>
	{
		// Token: 0x06005EE6 RID: 24294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D1")]
		[Address(RVA = "0x10136F034", Offset = "0x136F034", VA = "0x10136F034")]
		public EffectRuleParam_Bool()
		{
		}
	}
}
