﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001216 RID: 4630
	[Token(Token = "0x2000C04")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Byte : EffectRuleParam<sbyte>
	{
		// Token: 0x06005EE7 RID: 24295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D2")]
		[Address(RVA = "0x10136F084", Offset = "0x136F084", VA = "0x10136F084")]
		public EffectRuleParam_Byte()
		{
		}
	}
}
