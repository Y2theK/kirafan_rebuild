﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001223 RID: 4643
	[Token(Token = "0x2000C11")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Color : EffectRuleParam<Color>
	{
		// Token: 0x06005EF4 RID: 24308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DF")]
		[Address(RVA = "0x10136F0D4", Offset = "0x136F0D4", VA = "0x10136F0D4")]
		public EffectRuleParam_Color()
		{
		}
	}
}
