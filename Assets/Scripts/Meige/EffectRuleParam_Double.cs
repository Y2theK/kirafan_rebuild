﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121F RID: 4639
	[Token(Token = "0x2000C0D")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Double : EffectRuleParam<double>
	{
		// Token: 0x06005EF0 RID: 24304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DB")]
		[Address(RVA = "0x10136F124", Offset = "0x136F124", VA = "0x10136F124")]
		public EffectRuleParam_Double()
		{
		}
	}
}
