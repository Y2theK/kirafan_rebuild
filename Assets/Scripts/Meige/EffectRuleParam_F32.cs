﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001225 RID: 4645
	[Token(Token = "0x2000C13")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_F32 : EffectRuleParam<f32>
	{
		// Token: 0x06005EF6 RID: 24310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E1")]
		[Address(RVA = "0x10136F174", Offset = "0x136F174", VA = "0x10136F174")]
		public EffectRuleParam_F32()
		{
		}
	}
}
