﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121E RID: 4638
	[Token(Token = "0x2000C0C")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Float : EffectRuleParam<float>
	{
		// Token: 0x06005EEF RID: 24303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DA")]
		[Address(RVA = "0x10136F1C4", Offset = "0x136F1C4", VA = "0x10136F1C4")]
		public EffectRuleParam_Float()
		{
		}
	}
}
