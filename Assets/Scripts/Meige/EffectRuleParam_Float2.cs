﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001226 RID: 4646
	[Token(Token = "0x2000C14")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Float2 : EffectRuleParam<Float2>
	{
		// Token: 0x06005EF7 RID: 24311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E2")]
		[Address(RVA = "0x10136F214", Offset = "0x136F214", VA = "0x10136F214")]
		public EffectRuleParam_Float2()
		{
		}
	}
}
