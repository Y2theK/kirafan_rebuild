﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001227 RID: 4647
	[Token(Token = "0x2000C15")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Float3 : EffectRuleParam<Float3>
	{
		// Token: 0x06005EF8 RID: 24312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E3")]
		[Address(RVA = "0x10136F264", Offset = "0x136F264", VA = "0x10136F264")]
		public EffectRuleParam_Float3()
		{
		}
	}
}
