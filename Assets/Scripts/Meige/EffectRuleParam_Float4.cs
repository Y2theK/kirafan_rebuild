﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001228 RID: 4648
	[Token(Token = "0x2000C16")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Float4 : EffectRuleParam<Float4>
	{
		// Token: 0x06005EF9 RID: 24313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E4")]
		[Address(RVA = "0x10136F2B4", Offset = "0x136F2B4", VA = "0x10136F2B4")]
		public EffectRuleParam_Float4()
		{
		}
	}
}
