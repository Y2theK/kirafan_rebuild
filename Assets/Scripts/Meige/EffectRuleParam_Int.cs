﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121A RID: 4634
	[Token(Token = "0x2000C08")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Int : EffectRuleParam<int>
	{
		// Token: 0x06005EEB RID: 24299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D6")]
		[Address(RVA = "0x10136F304", Offset = "0x136F304", VA = "0x10136F304")]
		public EffectRuleParam_Int()
		{
		}
	}
}
