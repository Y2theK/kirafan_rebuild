﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121C RID: 4636
	[Token(Token = "0x2000C0A")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Long : EffectRuleParam<long>
	{
		// Token: 0x06005EED RID: 24301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D8")]
		[Address(RVA = "0x10136F354", Offset = "0x136F354", VA = "0x10136F354")]
		public EffectRuleParam_Long()
		{
		}
	}
}
