﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001229 RID: 4649
	[Token(Token = "0x2000C17")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RGBA : EffectRuleParam<RGBA>
	{
		// Token: 0x06005EFA RID: 24314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E5")]
		[Address(RVA = "0x10136F3A4", Offset = "0x136F3A4", VA = "0x10136F3A4")]
		public EffectRuleParam_RGBA()
		{
		}
	}
}
