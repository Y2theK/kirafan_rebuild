﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122F RID: 4655
	[Token(Token = "0x2000C1D")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareColor : EffectRuleParam<RangePareColor>
	{
		// Token: 0x06005F00 RID: 24320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057EB")]
		[Address(RVA = "0x10136F3F4", Offset = "0x136F3F4", VA = "0x10136F3F4")]
		public EffectRuleParam_RangePareColor()
		{
		}
	}
}
