﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122A RID: 4650
	[Token(Token = "0x2000C18")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareFloat : EffectRuleParam<RangePareFloat>
	{
		// Token: 0x06005EFB RID: 24315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E6")]
		[Address(RVA = "0x10136F444", Offset = "0x136F444", VA = "0x10136F444")]
		public EffectRuleParam_RangePareFloat()
		{
		}
	}
}
