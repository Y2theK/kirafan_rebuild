﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122B RID: 4651
	[Token(Token = "0x2000C19")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareInt : EffectRuleParam<RangePareInt>
	{
		// Token: 0x06005EFC RID: 24316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E7")]
		[Address(RVA = "0x10136F494", Offset = "0x136F494", VA = "0x10136F494")]
		public EffectRuleParam_RangePareInt()
		{
		}
	}
}
