﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122C RID: 4652
	[Token(Token = "0x2000C1A")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareVector2 : EffectRuleParam<RangePareVector2>
	{
		// Token: 0x06005EFD RID: 24317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E8")]
		[Address(RVA = "0x10136F4E4", Offset = "0x136F4E4", VA = "0x10136F4E4")]
		public EffectRuleParam_RangePareVector2()
		{
		}
	}
}
