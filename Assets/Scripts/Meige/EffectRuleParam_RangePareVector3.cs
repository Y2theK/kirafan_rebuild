﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122D RID: 4653
	[Token(Token = "0x2000C1B")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareVector3 : EffectRuleParam<RangePareVector3>
	{
		// Token: 0x06005EFE RID: 24318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E9")]
		[Address(RVA = "0x10136F534", Offset = "0x136F534", VA = "0x10136F534")]
		public EffectRuleParam_RangePareVector3()
		{
		}
	}
}
