﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200122E RID: 4654
	[Token(Token = "0x2000C1C")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangePareVector4 : EffectRuleParam<RangePareVector4>
	{
		// Token: 0x06005EFF RID: 24319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057EA")]
		[Address(RVA = "0x10136F584", Offset = "0x136F584", VA = "0x10136F584")]
		public EffectRuleParam_RangePareVector4()
		{
		}
	}
}
