﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001235 RID: 4661
	[Token(Token = "0x2000C23")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueColor : EffectRuleParam<RangeValueColor>
	{
		// Token: 0x06005F06 RID: 24326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F1")]
		[Address(RVA = "0x10136F5D4", Offset = "0x136F5D4", VA = "0x10136F5D4")]
		public EffectRuleParam_RangeValueColor()
		{
		}
	}
}
