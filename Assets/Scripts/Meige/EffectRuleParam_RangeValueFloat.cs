﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001231 RID: 4657
	[Token(Token = "0x2000C1F")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueFloat : EffectRuleParam<RangeValueFloat>
	{
		// Token: 0x06005F02 RID: 24322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057ED")]
		[Address(RVA = "0x10136F624", Offset = "0x136F624", VA = "0x10136F624")]
		public EffectRuleParam_RangeValueFloat()
		{
		}
	}
}
