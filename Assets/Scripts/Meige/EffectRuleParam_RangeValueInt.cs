﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001230 RID: 4656
	[Token(Token = "0x2000C1E")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueInt : EffectRuleParam<RangeValueInt>
	{
		// Token: 0x06005F01 RID: 24321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057EC")]
		[Address(RVA = "0x10136F674", Offset = "0x136F674", VA = "0x10136F674")]
		public EffectRuleParam_RangeValueInt()
		{
		}
	}
}
