﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001232 RID: 4658
	[Token(Token = "0x2000C20")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueVector2 : EffectRuleParam<RangeValueVector2>
	{
		// Token: 0x06005F03 RID: 24323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057EE")]
		[Address(RVA = "0x10136F6C4", Offset = "0x136F6C4", VA = "0x10136F6C4")]
		public EffectRuleParam_RangeValueVector2()
		{
		}
	}
}
