﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001233 RID: 4659
	[Token(Token = "0x2000C21")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueVector3 : EffectRuleParam<RangeValueVector3>
	{
		// Token: 0x06005F04 RID: 24324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057EF")]
		[Address(RVA = "0x10136F714", Offset = "0x136F714", VA = "0x10136F714")]
		public EffectRuleParam_RangeValueVector3()
		{
		}
	}
}
