﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001234 RID: 4660
	[Token(Token = "0x2000C22")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_RangeValueVector4 : EffectRuleParam<RangeValueVector4>
	{
		// Token: 0x06005F05 RID: 24325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057F0")]
		[Address(RVA = "0x10136F764", Offset = "0x136F764", VA = "0x10136F764")]
		public EffectRuleParam_RangeValueVector4()
		{
		}
	}
}
