﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001224 RID: 4644
	[Token(Token = "0x2000C12")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_S32 : EffectRuleParam<s32>
	{
		// Token: 0x06005EF5 RID: 24309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057E0")]
		[Address(RVA = "0x10136F7B4", Offset = "0x136F7B4", VA = "0x10136F7B4")]
		public EffectRuleParam_S32()
		{
		}
	}
}
