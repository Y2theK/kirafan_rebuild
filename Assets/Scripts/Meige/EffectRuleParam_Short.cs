﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001218 RID: 4632
	[Token(Token = "0x2000C06")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Short : EffectRuleParam<short>
	{
		// Token: 0x06005EE9 RID: 24297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D4")]
		[Address(RVA = "0x10136F804", Offset = "0x136F804", VA = "0x10136F804")]
		public EffectRuleParam_Short()
		{
		}
	}
}
