﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001217 RID: 4631
	[Token(Token = "0x2000C05")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Ubyte : EffectRuleParam<byte>
	{
		// Token: 0x06005EE8 RID: 24296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D3")]
		[Address(RVA = "0x10136F854", Offset = "0x136F854", VA = "0x10136F854")]
		public EffectRuleParam_Ubyte()
		{
		}
	}
}
