﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121B RID: 4635
	[Token(Token = "0x2000C09")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Uint : EffectRuleParam<uint>
	{
		// Token: 0x06005EEC RID: 24300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D7")]
		[Address(RVA = "0x10136F8A4", Offset = "0x136F8A4", VA = "0x10136F8A4")]
		public EffectRuleParam_Uint()
		{
		}
	}
}
