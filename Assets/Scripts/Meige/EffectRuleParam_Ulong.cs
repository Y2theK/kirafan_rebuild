﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200121D RID: 4637
	[Token(Token = "0x2000C0B")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Ulong : EffectRuleParam<ulong>
	{
		// Token: 0x06005EEE RID: 24302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D9")]
		[Address(RVA = "0x10136F8F4", Offset = "0x136F8F4", VA = "0x10136F8F4")]
		public EffectRuleParam_Ulong()
		{
		}
	}
}
