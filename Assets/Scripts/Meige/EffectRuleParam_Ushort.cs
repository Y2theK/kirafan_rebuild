﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001219 RID: 4633
	[Token(Token = "0x2000C07")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Ushort : EffectRuleParam<ushort>
	{
		// Token: 0x06005EEA RID: 24298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057D5")]
		[Address(RVA = "0x10136F944", Offset = "0x136F944", VA = "0x10136F944")]
		public EffectRuleParam_Ushort()
		{
		}
	}
}
