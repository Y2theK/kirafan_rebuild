﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001220 RID: 4640
	[Token(Token = "0x2000C0E")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Vector2 : EffectRuleParam<Vector2>
	{
		// Token: 0x06005EF1 RID: 24305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DC")]
		[Address(RVA = "0x10136F994", Offset = "0x136F994", VA = "0x10136F994")]
		public EffectRuleParam_Vector2()
		{
		}
	}
}
