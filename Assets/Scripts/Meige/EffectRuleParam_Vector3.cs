﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001221 RID: 4641
	[Token(Token = "0x2000C0F")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Vector3 : EffectRuleParam<Vector3>
	{
		// Token: 0x06005EF2 RID: 24306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DD")]
		[Address(RVA = "0x10136F9E4", Offset = "0x136F9E4", VA = "0x10136F9E4")]
		public EffectRuleParam_Vector3()
		{
		}
	}
}
