﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001222 RID: 4642
	[Token(Token = "0x2000C10")]
	[Serializable]
	[StructLayout(3)]
	public class EffectRuleParam_Vector4 : EffectRuleParam<Vector4>
	{
		// Token: 0x06005EF3 RID: 24307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60057DE")]
		[Address(RVA = "0x10136FA34", Offset = "0x136FA34", VA = "0x10136FA34")]
		public EffectRuleParam_Vector4()
		{
		}
	}
}
