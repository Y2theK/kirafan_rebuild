﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001258 RID: 4696
	[Token(Token = "0x2000C46")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D434", Offset = "0x11D434")]
	[DisallowMultipleComponent]
	[StructLayout(3)]
	public class EffectSequencer : EffectComponentBase
	{
		// Token: 0x06005F30 RID: 24368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600581B")]
		[Address(RVA = "0x10136FA84", Offset = "0x136FA84", VA = "0x10136FA84")]
		private void Awake()
		{
		}

		// Token: 0x06005F31 RID: 24369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600581C")]
		[Address(RVA = "0x10136FB48", Offset = "0x136FB48", VA = "0x10136FB48")]
		private void Start()
		{
		}

		// Token: 0x06005F32 RID: 24370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600581D")]
		[Address(RVA = "0x10136FE64", Offset = "0x136FE64", VA = "0x10136FE64")]
		private void ComponentAction_Shot(int index)
		{
		}

		// Token: 0x06005F33 RID: 24371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600581E")]
		[Address(RVA = "0x10136FFB4", Offset = "0x136FFB4", VA = "0x10136FFB4")]
		private void ComponentAction_Activate(int index)
		{
		}

		// Token: 0x06005F34 RID: 24372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600581F")]
		[Address(RVA = "0x101370108", Offset = "0x1370108", VA = "0x101370108")]
		private void ComponentAction_Deactivate(int index)
		{
		}

		// Token: 0x06005F35 RID: 24373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005820")]
		[Address(RVA = "0x10137025C", Offset = "0x137025C", VA = "0x10137025C")]
		private void ComponentAction_Kill(int index)
		{
		}

		// Token: 0x06005F36 RID: 24374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005821")]
		[Address(RVA = "0x1013703AC", Offset = "0x13703AC", VA = "0x1013703AC")]
		private void Update()
		{
		}

		// Token: 0x06005F37 RID: 24375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005822")]
		[Address(RVA = "0x101370638", Offset = "0x1370638", VA = "0x101370638")]
		public void SetClip(string name)
		{
		}

		// Token: 0x06005F38 RID: 24376 RVA: 0x0001F110 File Offset: 0x0001D310
		[Token(Token = "0x6005823")]
		[Address(RVA = "0x101370640", Offset = "0x1370640", VA = "0x101370640", Slot = "20")]
		public override bool Shot()
		{
			return default(bool);
		}

		// Token: 0x06005F39 RID: 24377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005824")]
		[Address(RVA = "0x1013708DC", Offset = "0x13708DC", VA = "0x1013708DC", Slot = "21")]
		public override void Activate(bool flg)
		{
		}

		// Token: 0x06005F3A RID: 24378 RVA: 0x0001F128 File Offset: 0x0001D328
		[Token(Token = "0x6005825")]
		[Address(RVA = "0x1013708E8", Offset = "0x13708E8", VA = "0x1013708E8", Slot = "22")]
		public override bool Kill()
		{
			return default(bool);
		}

		// Token: 0x06005F3B RID: 24379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005826")]
		[Address(RVA = "0x101370C40", Offset = "0x1370C40", VA = "0x101370C40")]
		public EffectSequencer()
		{
		}

		// Token: 0x04006EEE RID: 28398
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004EAB")]
		private EffectComponentBase[] m_EffectArray;

		// Token: 0x04006EEF RID: 28399
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004EAC")]
		private Animation m_Animation;

		// Token: 0x04006EF0 RID: 28400
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004EAD")]
		private string m_ClipName;
	}
}
