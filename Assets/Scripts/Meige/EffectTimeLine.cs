﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001259 RID: 4697
	[Token(Token = "0x2000C47")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D4A8", Offset = "0x11D4A8")]
	[DisallowMultipleComponent]
	[StructLayout(3)]
	public class EffectTimeLine : EffectComponentBase
	{
		// Token: 0x06005F3C RID: 24380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005827")]
		[Address(RVA = "0x101370C48", Offset = "0x1370C48", VA = "0x101370C48")]
		private void OnApplicationQuit()
		{
		}

		// Token: 0x17000652 RID: 1618
		// (get) Token: 0x06005F3D RID: 24381 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005F5")]
		public EffectComponentBase[] effectArray
		{
			[Token(Token = "0x6005828")]
			[Address(RVA = "0x101370C4C", Offset = "0x1370C4C", VA = "0x101370C4C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000653 RID: 1619
		// (get) Token: 0x06005F3E RID: 24382 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005F6")]
		public EffectTimeLineAnimation animation
		{
			[Token(Token = "0x6005829")]
			[Address(RVA = "0x101370C54", Offset = "0x1370C54", VA = "0x101370C54")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005F3F RID: 24383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600582A")]
		[Address(RVA = "0x101370C5C", Offset = "0x1370C5C", VA = "0x101370C5C")]
		private void Awake()
		{
		}

		// Token: 0x06005F40 RID: 24384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600582B")]
		[Address(RVA = "0x101370DC8", Offset = "0x1370DC8", VA = "0x101370DC8")]
		private void Start()
		{
		}

		// Token: 0x06005F41 RID: 24385 RVA: 0x0001F140 File Offset: 0x0001D340
		[Token(Token = "0x600582C")]
		[Address(RVA = "0x101370DCC", Offset = "0x1370DCC", VA = "0x101370DCC", Slot = "20")]
		public override bool Shot()
		{
			return default(bool);
		}

		// Token: 0x06005F42 RID: 24386 RVA: 0x0001F158 File Offset: 0x0001D358
		[Token(Token = "0x600582D")]
		[Address(RVA = "0x1013710D4", Offset = "0x13710D4", VA = "0x1013710D4", Slot = "22")]
		public override bool Kill()
		{
			return default(bool);
		}

		// Token: 0x06005F43 RID: 24387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600582E")]
		[Address(RVA = "0x101371494", Offset = "0x1371494", VA = "0x101371494")]
		private void Update()
		{
		}

		// Token: 0x06005F44 RID: 24388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600582F")]
		[Address(RVA = "0x101371768", Offset = "0x1371768", VA = "0x101371768")]
		public void EventActivate(string id)
		{
		}

		// Token: 0x06005F45 RID: 24389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005830")]
		[Address(RVA = "0x101371820", Offset = "0x1371820", VA = "0x101371820")]
		public void EventDeactivate(string id)
		{
		}

		// Token: 0x06005F46 RID: 24390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005831")]
		[Address(RVA = "0x1013718D8", Offset = "0x13718D8", VA = "0x1013718D8")]
		public void EventShot(string id)
		{
		}

		// Token: 0x06005F47 RID: 24391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005832")]
		[Address(RVA = "0x10137198C", Offset = "0x137198C", VA = "0x10137198C")]
		public void EventKill(string id)
		{
		}

		// Token: 0x06005F48 RID: 24392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005833")]
		[Address(RVA = "0x101371A40", Offset = "0x1371A40", VA = "0x101371A40")]
		public EffectTimeLine()
		{
		}

		// Token: 0x04006EF1 RID: 28401
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004EAE")]
		public Transform m_ThisTransform;

		// Token: 0x04006EF2 RID: 28402
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004EAF")]
		public EffectComponentBase[] m_EffectArray;

		// Token: 0x04006EF3 RID: 28403
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004EB0")]
		private EffectTimeLineAnimation m_EffectTimeLineAnimation;

		// Token: 0x04006EF4 RID: 28404
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004EB1")]
		private Dictionary<string, EffectComponentBase> m_EffectDictionary;
	}
}
