﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x0200125D RID: 4701
	[Token(Token = "0x2000C4B")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D51C", Offset = "0x11D51C")]
	[DisallowMultipleComponent]
	[StructLayout(3)]
	public class EffectTimeLineAnimation : MonoBehaviour
	{
		// Token: 0x17000654 RID: 1620
		// (get) Token: 0x06005F4D RID: 24397 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005F7")]
		public Animation animation
		{
			[Token(Token = "0x6005838")]
			[Address(RVA = "0x101371AE8", Offset = "0x1371AE8", VA = "0x101371AE8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000655 RID: 1621
		// (get) Token: 0x06005F4E RID: 24398 RVA: 0x0001F170 File Offset: 0x0001D370
		// (set) Token: 0x06005F4F RID: 24399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005F8")]
		public float animationSpeedScale
		{
			[Token(Token = "0x6005839")]
			[Address(RVA = "0x101371AF0", Offset = "0x1371AF0", VA = "0x101371AF0")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600583A")]
			[Address(RVA = "0x101371AF8", Offset = "0x1371AF8", VA = "0x101371AF8")]
			set
			{
			}
		}

		// Token: 0x06005F50 RID: 24400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600583B")]
		[Address(RVA = "0x101371B00", Offset = "0x1371B00", VA = "0x101371B00")]
		private void Awake()
		{
		}

		// Token: 0x06005F51 RID: 24401 RVA: 0x0001F188 File Offset: 0x0001D388
		[Token(Token = "0x600583C")]
		[Address(RVA = "0x101371BCC", Offset = "0x1371BCC", VA = "0x101371BCC")]
		public float GetTimeMax()
		{
			return 0f;
		}

		// Token: 0x06005F52 RID: 24402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600583D")]
		[Address(RVA = "0x101370E58", Offset = "0x1370E58", VA = "0x101370E58")]
		public void SetTimeNow(float value)
		{
		}

		// Token: 0x06005F53 RID: 24403 RVA: 0x0001F1A0 File Offset: 0x0001D3A0
		[Token(Token = "0x600583E")]
		[Address(RVA = "0x101371E4C", Offset = "0x1371E4C", VA = "0x101371E4C")]
		public float GetTimeNow()
		{
			return 0f;
		}

		// Token: 0x06005F54 RID: 24404 RVA: 0x0001F1B8 File Offset: 0x0001D3B8
		[Token(Token = "0x600583F")]
		[Address(RVA = "0x101371740", Offset = "0x1371740", VA = "0x101371740")]
		public bool IsEndFrame()
		{
			return default(bool);
		}

		// Token: 0x06005F55 RID: 24405 RVA: 0x0001F1D0 File Offset: 0x0001D3D0
		[Token(Token = "0x6005840")]
		[Address(RVA = "0x1013710CC", Offset = "0x13710CC", VA = "0x1013710CC")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06005F56 RID: 24406 RVA: 0x0001F1E8 File Offset: 0x0001D3E8
		[Token(Token = "0x6005841")]
		[Address(RVA = "0x101370E60", Offset = "0x1370E60", VA = "0x101370E60")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06005F57 RID: 24407 RVA: 0x0001F200 File Offset: 0x0001D400
		[Token(Token = "0x6005842")]
		[Address(RVA = "0x10137123C", Offset = "0x137123C", VA = "0x10137123C")]
		public bool Stop()
		{
			return default(bool);
		}

		// Token: 0x06005F58 RID: 24408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005843")]
		[Address(RVA = "0x101371E54", Offset = "0x1371E54", VA = "0x101371E54")]
		private void Update()
		{
		}

		// Token: 0x06005F59 RID: 24409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005844")]
		[Address(RVA = "0x101371AD8", Offset = "0x1371AD8", VA = "0x101371AD8")]
		public EffectTimeLineAnimation()
		{
		}

		// Token: 0x04006F00 RID: 28416
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004EBD")]
		[SerializeField]
		private EffectTimeLineCurve[] m_CurveList;

		// Token: 0x04006F01 RID: 28417
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004EBE")]
		private Animation m_Animation;

		// Token: 0x04006F02 RID: 28418
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004EBF")]
		private float m_AnimationTime;

		// Token: 0x04006F03 RID: 28419
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004EC0")]
		private float m_AnimationSpeedScale;

		// Token: 0x04006F04 RID: 28420
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004EC1")]
		private bool m_isPlaying;

		// Token: 0x04006F05 RID: 28421
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4004EC2")]
		private bool m_isFirstFrame;
	}
}
