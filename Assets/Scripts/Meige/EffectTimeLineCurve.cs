﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200125C RID: 4700
	[Token(Token = "0x2000C4A")]
	[Serializable]
	[StructLayout(3)]
	public class EffectTimeLineCurve
	{
		// Token: 0x06005F4A RID: 24394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005835")]
		[Address(RVA = "0x1013725A4", Offset = "0x13725A4", VA = "0x1013725A4")]
		private void UpdateProcessIndex(float keyframe)
		{
		}

		// Token: 0x06005F4B RID: 24395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005836")]
		[Address(RVA = "0x1013720E8", Offset = "0x13720E8", VA = "0x1013720E8")]
		public void CalcValue(float keyframe)
		{
		}

		// Token: 0x06005F4C RID: 24396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005837")]
		[Address(RVA = "0x1013727B4", Offset = "0x13727B4", VA = "0x1013727B4")]
		public EffectTimeLineCurve()
		{
		}

		// Token: 0x04006EFD RID: 28413
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004EBA")]
		public EffectTimeLineSourceCurve m_SourceCurve;

		// Token: 0x04006EFE RID: 28414
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004EBB")]
		public int m_PropertyID;

		// Token: 0x04006EFF RID: 28415
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004EBC")]
		private int m_ProcessIdx;
	}
}
