﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001260 RID: 4704
	[Token(Token = "0x2000C4E")]
	[Attribute(Name = "AttributeUsageAttribute", RVA = "0x10011D628", Offset = "0x11D628")]
	[StructLayout(3)]
	public class EffectTimeLineFieldAttribute : Attribute
	{
		// Token: 0x06005F5C RID: 24412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005847")]
		[Address(RVA = "0x1013727BC", Offset = "0x13727BC", VA = "0x1013727BC")]
		public EffectTimeLineFieldAttribute(string displayName, TypeCode typeCode)
		{
		}

		// Token: 0x04006F0C RID: 28428
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004EC9")]
		public string m_DisplayName;

		// Token: 0x04006F0D RID: 28429
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004ECA")]
		public TypeCode m_TypeCode;
	}
}
