﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200125A RID: 4698
	[Token(Token = "0x2000C48")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct EffectTimeLineKeyFrame
	{
		// Token: 0x04006EF5 RID: 28405
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004EB2")]
		public float m_Time;

		// Token: 0x04006EF6 RID: 28406
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4004EB3")]
		public float[] m_ComponentValue;
	}
}
