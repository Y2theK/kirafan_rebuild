﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200125B RID: 4699
	[Token(Token = "0x2000C49")]
	[Serializable]
	[StructLayout(3)]
	public class EffectTimeLineSourceCurve
	{
		// Token: 0x06005F49 RID: 24393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005834")]
		[Address(RVA = "0x1013727F8", Offset = "0x13727F8", VA = "0x1013727F8")]
		public EffectTimeLineSourceCurve()
		{
		}

		// Token: 0x04006EF7 RID: 28407
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004EB4")]
		public EffectComponentBase m_Target;

		// Token: 0x04006EF8 RID: 28408
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004EB5")]
		public string m_ParamPropertyName;

		// Token: 0x04006EF9 RID: 28409
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004EB6")]
		public int m_ArrayIdx;

		// Token: 0x04006EFA RID: 28410
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004EB7")]
		public int m_TargetIdx;

		// Token: 0x04006EFB RID: 28411
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004EB8")]
		public int m_ComponentIdx;

		// Token: 0x04006EFC RID: 28412
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004EB9")]
		public EffectTimeLineKeyFrame[] m_KeyFrameArray;
	}
}
