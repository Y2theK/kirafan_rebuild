﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001275 RID: 4725
	[Token(Token = "0x2000C63")]
	[Attribute(Name = "DefaultMemberAttribute", RVA = "0x10011D730", Offset = "0x11D730")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct Float2 : IComparable, IComparable<Float2>, IOperatorFast<Float2>
	{
		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x06006016 RID: 24598 RVA: 0x0001F758 File Offset: 0x0001D958
		// (set) Token: 0x06006015 RID: 24597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000602")]
		public Vector2 value
		{
			[Token(Token = "0x6005901")]
			[Address(RVA = "0x100036560", Offset = "0x36560", VA = "0x100036560")]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x6005900")]
			[Address(RVA = "0x100036554", Offset = "0x36554", VA = "0x100036554")]
			set
			{
			}
		}

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x06006018 RID: 24600 RVA: 0x0001F770 File Offset: 0x0001D970
		// (set) Token: 0x06006017 RID: 24599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000603")]
		public float x
		{
			[Token(Token = "0x6005903")]
			[Address(RVA = "0x100036570", Offset = "0x36570", VA = "0x100036570")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005902")]
			[Address(RVA = "0x100036568", Offset = "0x36568", VA = "0x100036568")]
			set
			{
			}
		}

		// Token: 0x17000661 RID: 1633
		// (get) Token: 0x0600601A RID: 24602 RVA: 0x0001F788 File Offset: 0x0001D988
		// (set) Token: 0x06006019 RID: 24601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000604")]
		public float y
		{
			[Token(Token = "0x6005905")]
			[Address(RVA = "0x100036580", Offset = "0x36580", VA = "0x100036580")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005904")]
			[Address(RVA = "0x100036578", Offset = "0x36578", VA = "0x100036578")]
			set
			{
			}
		}

		// Token: 0x17000662 RID: 1634
		// (get) Token: 0x0600601C RID: 24604 RVA: 0x0001F7A0 File Offset: 0x0001D9A0
		// (set) Token: 0x0600601B RID: 24603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000605")]
		public float Item
		{
			[Token(Token = "0x6005907")]
			[Address(RVA = "0x100036594", Offset = "0x36594", VA = "0x100036594")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005906")]
			[Address(RVA = "0x100036588", Offset = "0x36588", VA = "0x100036588")]
			set
			{
			}
		}

		// Token: 0x0600601D RID: 24605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005908")]
		[Address(RVA = "0x1000365A0", Offset = "0x365A0", VA = "0x1000365A0")]
		public Float2(Float2 value)
		{
		}

		// Token: 0x0600601E RID: 24606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005909")]
		[Address(RVA = "0x1000365AC", Offset = "0x365AC", VA = "0x1000365AC")]
		public Float2(Vector2 value)
		{
		}

		// Token: 0x0600601F RID: 24607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600590A")]
		[Address(RVA = "0x1000365B8", Offset = "0x365B8", VA = "0x1000365B8")]
		public Float2(float x, float y)
		{
		}

		// Token: 0x06006020 RID: 24608 RVA: 0x0001F7B8 File Offset: 0x0001D9B8
		[Token(Token = "0x600590B")]
		[Address(RVA = "0x101376180", Offset = "0x1376180", VA = "0x101376180")]
		public static implicit operator Float2(Vector2 v)
		{
			return default(Float2);
		}

		// Token: 0x06006021 RID: 24609 RVA: 0x0001F7D0 File Offset: 0x0001D9D0
		[Token(Token = "0x600590C")]
		[Address(RVA = "0x101376184", Offset = "0x1376184", VA = "0x101376184")]
		public static implicit operator Vector2(Float2 v)
		{
			return default(Vector2);
		}

		// Token: 0x06006022 RID: 24610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600590D")]
		[Address(RVA = "0x1000365C4", Offset = "0x365C4", VA = "0x1000365C4", Slot = "6")]
		public void Add(Float2 a, Float2 b)
		{
		}

		// Token: 0x06006023 RID: 24611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600590E")]
		[Address(RVA = "0x1000365D8", Offset = "0x365D8", VA = "0x1000365D8", Slot = "7")]
		public void Sub(Float2 a, Float2 b)
		{
		}

		// Token: 0x06006024 RID: 24612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600590F")]
		[Address(RVA = "0x1000365EC", Offset = "0x365EC", VA = "0x1000365EC", Slot = "8")]
		public void Mul(Float2 a, Float2 b)
		{
		}

		// Token: 0x06006025 RID: 24613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005910")]
		[Address(RVA = "0x100036600", Offset = "0x36600", VA = "0x100036600", Slot = "9")]
		public void Mul(Float2 a, float b)
		{
		}

		// Token: 0x06006026 RID: 24614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005911")]
		[Address(RVA = "0x100036614", Offset = "0x36614", VA = "0x100036614", Slot = "10")]
		public void Div(Float2 a, Float2 b)
		{
		}

		// Token: 0x06006027 RID: 24615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005912")]
		[Address(RVA = "0x100036624", Offset = "0x36624", VA = "0x100036624", Slot = "11")]
		public void Add(Float2 b)
		{
		}

		// Token: 0x06006028 RID: 24616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005913")]
		[Address(RVA = "0x100036638", Offset = "0x36638", VA = "0x100036638", Slot = "12")]
		public void Sub(Float2 b)
		{
		}

		// Token: 0x06006029 RID: 24617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005914")]
		[Address(RVA = "0x10003664C", Offset = "0x3664C", VA = "0x10003664C", Slot = "13")]
		public void Mul(Float2 b)
		{
		}

		// Token: 0x0600602A RID: 24618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005915")]
		[Address(RVA = "0x100036660", Offset = "0x36660", VA = "0x100036660", Slot = "14")]
		public void Mul(float b)
		{
		}

		// Token: 0x0600602B RID: 24619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005916")]
		[Address(RVA = "0x100036670", Offset = "0x36670", VA = "0x100036670", Slot = "15")]
		public void Div(Float2 b)
		{
		}

		// Token: 0x0600602C RID: 24620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005917")]
		[Address(RVA = "0x100036684", Offset = "0x36684", VA = "0x100036684", Slot = "16")]
		public void SetRowValue(object o)
		{
		}

		// Token: 0x0600602D RID: 24621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005918")]
		[Address(RVA = "0x10003668C", Offset = "0x3668C", VA = "0x10003668C")]
		public void SetRowValue(float o, int componentIdx)
		{
		}

		// Token: 0x0600602E RID: 24622 RVA: 0x0001F7E8 File Offset: 0x0001D9E8
		[Token(Token = "0x6005919")]
		[Address(RVA = "0x100036698", Offset = "0x36698", VA = "0x100036698", Slot = "4")]
		public int CompareTo(object obj)
		{
			return 0;
		}

		// Token: 0x0600602F RID: 24623 RVA: 0x0001F800 File Offset: 0x0001DA00
		[Token(Token = "0x600591A")]
		[Address(RVA = "0x1000366B4", Offset = "0x366B4", VA = "0x1000366B4", Slot = "5")]
		public int CompareTo(Float2 target)
		{
			return 0;
		}

		// Token: 0x06006030 RID: 24624 RVA: 0x0001F818 File Offset: 0x0001DA18
		[Token(Token = "0x600591B")]
		[Address(RVA = "0x1000366DC", Offset = "0x366DC", VA = "0x1000366DC", Slot = "0")]
		public override bool Equals(object obj)
		{
			return default(bool);
		}

		// Token: 0x06006031 RID: 24625 RVA: 0x0001F830 File Offset: 0x0001DA30
		[Token(Token = "0x600591C")]
		[Address(RVA = "0x1000366F8", Offset = "0x366F8", VA = "0x1000366F8")]
		public bool Equals(Float2 obj)
		{
			return default(bool);
		}

		// Token: 0x06006032 RID: 24626 RVA: 0x0001F848 File Offset: 0x0001DA48
		[Token(Token = "0x600591D")]
		[Address(RVA = "0x100036708", Offset = "0x36708", VA = "0x100036708", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06006033 RID: 24627 RVA: 0x0001F860 File Offset: 0x0001DA60
		[Token(Token = "0x600591E")]
		[Address(RVA = "0x101376600", Offset = "0x1376600", VA = "0x101376600")]
		public static Float2 operator +(Float2 v0)
		{
			return default(Float2);
		}

		// Token: 0x06006034 RID: 24628 RVA: 0x0001F878 File Offset: 0x0001DA78
		[Token(Token = "0x600591F")]
		[Address(RVA = "0x10137619C", Offset = "0x137619C", VA = "0x10137619C")]
		public static Float2 operator +(Float2 v0, Float2 v1)
		{
			return default(Float2);
		}

		// Token: 0x06006035 RID: 24629 RVA: 0x0001F890 File Offset: 0x0001DA90
		[Token(Token = "0x6005920")]
		[Address(RVA = "0x101376604", Offset = "0x1376604", VA = "0x101376604")]
		public static Float2 operator -(Float2 v0)
		{
			return default(Float2);
		}

		// Token: 0x06006036 RID: 24630 RVA: 0x0001F8A8 File Offset: 0x0001DAA8
		[Token(Token = "0x6005921")]
		[Address(RVA = "0x1013761BC", Offset = "0x13761BC", VA = "0x1013761BC")]
		public static Float2 operator -(Float2 v0, Float2 v1)
		{
			return default(Float2);
		}

		// Token: 0x06006037 RID: 24631 RVA: 0x0001F8C0 File Offset: 0x0001DAC0
		[Token(Token = "0x6005922")]
		[Address(RVA = "0x1013761C8", Offset = "0x13761C8", VA = "0x1013761C8")]
		public static Float2 operator *(Float2 v0, Float2 v1)
		{
			return default(Float2);
		}

		// Token: 0x06006038 RID: 24632 RVA: 0x0001F8D8 File Offset: 0x0001DAD8
		[Token(Token = "0x6005923")]
		[Address(RVA = "0x1013761E8", Offset = "0x13761E8", VA = "0x1013761E8")]
		public static Float2 operator *(Float2 v0, float v1)
		{
			return default(Float2);
		}

		// Token: 0x06006039 RID: 24633 RVA: 0x0001F8F0 File Offset: 0x0001DAF0
		[Token(Token = "0x6005924")]
		[Address(RVA = "0x1013761F4", Offset = "0x13761F4", VA = "0x1013761F4")]
		public static Float2 operator /(Float2 v0, Float2 v1)
		{
			return default(Float2);
		}

		// Token: 0x0600603A RID: 24634 RVA: 0x0001F908 File Offset: 0x0001DB08
		[Token(Token = "0x6005925")]
		[Address(RVA = "0x101376610", Offset = "0x1376610", VA = "0x101376610")]
		public static Float2 operator /(Float2 v0, float v1)
		{
			return default(Float2);
		}

		// Token: 0x0600603B RID: 24635 RVA: 0x0001F920 File Offset: 0x0001DB20
		[Token(Token = "0x6005926")]
		[Address(RVA = "0x101376568", Offset = "0x1376568", VA = "0x101376568")]
		public static bool operator ==(Float2 v0, Float2 v1)
		{
			return default(bool);
		}

		// Token: 0x0600603C RID: 24636 RVA: 0x0001F938 File Offset: 0x0001DB38
		[Token(Token = "0x6005927")]
		[Address(RVA = "0x10137661C", Offset = "0x137661C", VA = "0x10137661C")]
		public static bool operator !=(Float2 v0, Float2 v1)
		{
			return default(bool);
		}

		// Token: 0x04006F1A RID: 28442
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004ED7")]
		public Vector2 m_Value;
	}
}
