﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001276 RID: 4726
	[Token(Token = "0x2000C64")]
	[Attribute(Name = "DefaultMemberAttribute", RVA = "0x10011D768", Offset = "0x11D768")]
	[Serializable]
	[StructLayout(0, Size = 12)]
	public struct Float3 : IComparable, IComparable<Float3>, IOperatorFast<Float3>
	{
		// Token: 0x17000663 RID: 1635
		// (get) Token: 0x0600603E RID: 24638 RVA: 0x0001F950 File Offset: 0x0001DB50
		// (set) Token: 0x0600603D RID: 24637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000606")]
		public Vector3 value
		{
			[Token(Token = "0x6005929")]
			[Address(RVA = "0x100036720", Offset = "0x36720", VA = "0x100036720")]
			get
			{
				return default(Vector3);
			}
			[Token(Token = "0x6005928")]
			[Address(RVA = "0x100036714", Offset = "0x36714", VA = "0x100036714")]
			set
			{
			}
		}

		// Token: 0x17000664 RID: 1636
		// (get) Token: 0x06006040 RID: 24640 RVA: 0x0001F968 File Offset: 0x0001DB68
		// (set) Token: 0x0600603F RID: 24639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000607")]
		public float x
		{
			[Token(Token = "0x600592B")]
			[Address(RVA = "0x100036734", Offset = "0x36734", VA = "0x100036734")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600592A")]
			[Address(RVA = "0x10003672C", Offset = "0x3672C", VA = "0x10003672C")]
			set
			{
			}
		}

		// Token: 0x17000665 RID: 1637
		// (get) Token: 0x06006042 RID: 24642 RVA: 0x0001F980 File Offset: 0x0001DB80
		// (set) Token: 0x06006041 RID: 24641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000608")]
		public float y
		{
			[Token(Token = "0x600592D")]
			[Address(RVA = "0x100036744", Offset = "0x36744", VA = "0x100036744")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600592C")]
			[Address(RVA = "0x10003673C", Offset = "0x3673C", VA = "0x10003673C")]
			set
			{
			}
		}

		// Token: 0x17000666 RID: 1638
		// (get) Token: 0x06006044 RID: 24644 RVA: 0x0001F998 File Offset: 0x0001DB98
		// (set) Token: 0x06006043 RID: 24643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000609")]
		public float z
		{
			[Token(Token = "0x600592F")]
			[Address(RVA = "0x100036754", Offset = "0x36754", VA = "0x100036754")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600592E")]
			[Address(RVA = "0x10003674C", Offset = "0x3674C", VA = "0x10003674C")]
			set
			{
			}
		}

		// Token: 0x17000667 RID: 1639
		// (get) Token: 0x06006046 RID: 24646 RVA: 0x0001F9B0 File Offset: 0x0001DBB0
		// (set) Token: 0x06006045 RID: 24645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060A")]
		public float Item
		{
			[Token(Token = "0x6005931")]
			[Address(RVA = "0x100036768", Offset = "0x36768", VA = "0x100036768")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005930")]
			[Address(RVA = "0x10003675C", Offset = "0x3675C", VA = "0x10003675C")]
			set
			{
			}
		}

		// Token: 0x06006047 RID: 24647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005932")]
		[Address(RVA = "0x100036774", Offset = "0x36774", VA = "0x100036774")]
		public Float3(Float3 value)
		{
		}

		// Token: 0x06006048 RID: 24648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005933")]
		[Address(RVA = "0x100036780", Offset = "0x36780", VA = "0x100036780")]
		public Float3(Vector3 value)
		{
		}

		// Token: 0x06006049 RID: 24649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005934")]
		[Address(RVA = "0x10003678C", Offset = "0x3678C", VA = "0x10003678C")]
		public Float3(float x, float y, float z)
		{
		}

		// Token: 0x0600604A RID: 24650 RVA: 0x0001F9C8 File Offset: 0x0001DBC8
		[Token(Token = "0x6005935")]
		[Address(RVA = "0x1013766BC", Offset = "0x13766BC", VA = "0x1013766BC")]
		public static implicit operator Float3(Vector3 v)
		{
			return default(Float3);
		}

		// Token: 0x0600604B RID: 24651 RVA: 0x0001F9E0 File Offset: 0x0001DBE0
		[Token(Token = "0x6005936")]
		[Address(RVA = "0x1013766C0", Offset = "0x13766C0", VA = "0x1013766C0")]
		public static implicit operator Vector3(Float3 v)
		{
			return default(Vector3);
		}

		// Token: 0x0600604C RID: 24652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005937")]
		[Address(RVA = "0x100036798", Offset = "0x36798", VA = "0x100036798", Slot = "6")]
		public void Add(Float3 a, Float3 b)
		{
		}

		// Token: 0x0600604D RID: 24653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005938")]
		[Address(RVA = "0x1000367B0", Offset = "0x367B0", VA = "0x1000367B0", Slot = "7")]
		public void Sub(Float3 a, Float3 b)
		{
		}

		// Token: 0x0600604E RID: 24654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005939")]
		[Address(RVA = "0x1000367C8", Offset = "0x367C8", VA = "0x1000367C8", Slot = "8")]
		public void Mul(Float3 a, Float3 b)
		{
		}

		// Token: 0x0600604F RID: 24655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593A")]
		[Address(RVA = "0x1000367E0", Offset = "0x367E0", VA = "0x1000367E0", Slot = "9")]
		public void Mul(Float3 a, float b)
		{
		}

		// Token: 0x06006050 RID: 24656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593B")]
		[Address(RVA = "0x1000367F8", Offset = "0x367F8", VA = "0x1000367F8", Slot = "10")]
		public void Div(Float3 a, Float3 b)
		{
		}

		// Token: 0x06006051 RID: 24657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593C")]
		[Address(RVA = "0x100036810", Offset = "0x36810", VA = "0x100036810", Slot = "11")]
		public void Add(Float3 b)
		{
		}

		// Token: 0x06006052 RID: 24658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593D")]
		[Address(RVA = "0x100036830", Offset = "0x36830", VA = "0x100036830", Slot = "12")]
		public void Sub(Float3 b)
		{
		}

		// Token: 0x06006053 RID: 24659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593E")]
		[Address(RVA = "0x100036850", Offset = "0x36850", VA = "0x100036850", Slot = "13")]
		public void Mul(Float3 b)
		{
		}

		// Token: 0x06006054 RID: 24660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600593F")]
		[Address(RVA = "0x100036870", Offset = "0x36870", VA = "0x100036870", Slot = "14")]
		public void Mul(float b)
		{
		}

		// Token: 0x06006055 RID: 24661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005940")]
		[Address(RVA = "0x10003688C", Offset = "0x3688C", VA = "0x10003688C", Slot = "15")]
		public void Div(Float3 b)
		{
		}

		// Token: 0x06006056 RID: 24662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005941")]
		[Address(RVA = "0x1000368AC", Offset = "0x368AC", VA = "0x1000368AC", Slot = "16")]
		public void SetRowValue(object o)
		{
		}

		// Token: 0x06006057 RID: 24663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005942")]
		[Address(RVA = "0x1000368B4", Offset = "0x368B4", VA = "0x1000368B4")]
		public void SetRowValue(float o, int componentIdx)
		{
		}

		// Token: 0x06006058 RID: 24664 RVA: 0x0001F9F8 File Offset: 0x0001DBF8
		[Token(Token = "0x6005943")]
		[Address(RVA = "0x1000368C0", Offset = "0x368C0", VA = "0x1000368C0", Slot = "4")]
		public int CompareTo(object obj)
		{
			return 0;
		}

		// Token: 0x06006059 RID: 24665 RVA: 0x0001FA10 File Offset: 0x0001DC10
		[Token(Token = "0x6005944")]
		[Address(RVA = "0x1000368DC", Offset = "0x368DC", VA = "0x1000368DC", Slot = "5")]
		public int CompareTo(Float3 target)
		{
			return 0;
		}

		// Token: 0x0600605A RID: 24666 RVA: 0x0001FA28 File Offset: 0x0001DC28
		[Token(Token = "0x6005945")]
		[Address(RVA = "0x100036910", Offset = "0x36910", VA = "0x100036910", Slot = "0")]
		public override bool Equals(object obj)
		{
			return default(bool);
		}

		// Token: 0x0600605B RID: 24667 RVA: 0x0001FA40 File Offset: 0x0001DC40
		[Token(Token = "0x6005946")]
		[Address(RVA = "0x10003692C", Offset = "0x3692C", VA = "0x10003692C")]
		public bool Equals(Float3 obj)
		{
			return default(bool);
		}

		// Token: 0x0600605C RID: 24668 RVA: 0x0001FA58 File Offset: 0x0001DC58
		[Token(Token = "0x6005947")]
		[Address(RVA = "0x100036944", Offset = "0x36944", VA = "0x100036944", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x0600605D RID: 24669 RVA: 0x0001FA70 File Offset: 0x0001DC70
		[Token(Token = "0x6005948")]
		[Address(RVA = "0x101376B94", Offset = "0x1376B94", VA = "0x101376B94")]
		public static Float3 operator +(Float3 v0)
		{
			return default(Float3);
		}

		// Token: 0x0600605E RID: 24670 RVA: 0x0001FA88 File Offset: 0x0001DC88
		[Token(Token = "0x6005949")]
		[Address(RVA = "0x1013766DC", Offset = "0x13766DC", VA = "0x1013766DC")]
		public static Float3 operator +(Float3 v0, Float3 v1)
		{
			return default(Float3);
		}

		// Token: 0x0600605F RID: 24671 RVA: 0x0001FAA0 File Offset: 0x0001DCA0
		[Token(Token = "0x600594A")]
		[Address(RVA = "0x101376B98", Offset = "0x1376B98", VA = "0x101376B98")]
		public static Float3 operator -(Float3 v0)
		{
			return default(Float3);
		}

		// Token: 0x06006060 RID: 24672 RVA: 0x0001FAB8 File Offset: 0x0001DCB8
		[Token(Token = "0x600594B")]
		[Address(RVA = "0x101376704", Offset = "0x1376704", VA = "0x101376704")]
		public static Float3 operator -(Float3 v0, Float3 v1)
		{
			return default(Float3);
		}

		// Token: 0x06006061 RID: 24673 RVA: 0x0001FAD0 File Offset: 0x0001DCD0
		[Token(Token = "0x600594C")]
		[Address(RVA = "0x101376714", Offset = "0x1376714", VA = "0x101376714")]
		public static Float3 operator *(Float3 v0, Float3 v1)
		{
			return default(Float3);
		}

		// Token: 0x06006062 RID: 24674 RVA: 0x0001FAE8 File Offset: 0x0001DCE8
		[Token(Token = "0x600594D")]
		[Address(RVA = "0x10137673C", Offset = "0x137673C", VA = "0x10137673C")]
		public static Float3 operator *(Float3 v0, float v1)
		{
			return default(Float3);
		}

		// Token: 0x06006063 RID: 24675 RVA: 0x0001FB00 File Offset: 0x0001DD00
		[Token(Token = "0x600594E")]
		[Address(RVA = "0x10137674C", Offset = "0x137674C", VA = "0x10137674C")]
		public static Float3 operator /(Float3 v0, Float3 v1)
		{
			return default(Float3);
		}

		// Token: 0x06006064 RID: 24676 RVA: 0x0001FB18 File Offset: 0x0001DD18
		[Token(Token = "0x600594F")]
		[Address(RVA = "0x101376BA8", Offset = "0x1376BA8", VA = "0x101376BA8")]
		public static Float3 operator /(Float3 v0, float v1)
		{
			return default(Float3);
		}

		// Token: 0x06006065 RID: 24677 RVA: 0x0001FB30 File Offset: 0x0001DD30
		[Token(Token = "0x6005950")]
		[Address(RVA = "0x101376AE4", Offset = "0x1376AE4", VA = "0x101376AE4")]
		public static bool operator ==(Float3 v0, Float3 v1)
		{
			return default(bool);
		}

		// Token: 0x06006066 RID: 24678 RVA: 0x0001FB48 File Offset: 0x0001DD48
		[Token(Token = "0x6005951")]
		[Address(RVA = "0x101376BB8", Offset = "0x1376BB8", VA = "0x101376BB8")]
		public static bool operator !=(Float3 v0, Float3 v1)
		{
			return default(bool);
		}

		// Token: 0x04006F1B RID: 28443
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004ED8")]
		[SerializeField]
		public Vector3 m_Value;
	}
}
