﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001277 RID: 4727
	[Token(Token = "0x2000C65")]
	[Attribute(Name = "DefaultMemberAttribute", RVA = "0x10011D7A0", Offset = "0x11D7A0")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct Float4 : IComparable, IComparable<Float4>, IOperatorFast<Float4>
	{
		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x06006068 RID: 24680 RVA: 0x0001FB60 File Offset: 0x0001DD60
		// (set) Token: 0x06006067 RID: 24679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060B")]
		public Vector4 value
		{
			[Token(Token = "0x6005953")]
			[Address(RVA = "0x100036964", Offset = "0x36964", VA = "0x100036964")]
			get
			{
				return default(Vector4);
			}
			[Token(Token = "0x6005952")]
			[Address(RVA = "0x100036950", Offset = "0x36950", VA = "0x100036950")]
			set
			{
			}
		}

		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x0600606A RID: 24682 RVA: 0x0001FB78 File Offset: 0x0001DD78
		// (set) Token: 0x06006069 RID: 24681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060C")]
		public float x
		{
			[Token(Token = "0x6005955")]
			[Address(RVA = "0x100036978", Offset = "0x36978", VA = "0x100036978")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005954")]
			[Address(RVA = "0x100036970", Offset = "0x36970", VA = "0x100036970")]
			set
			{
			}
		}

		// Token: 0x1700066A RID: 1642
		// (get) Token: 0x0600606C RID: 24684 RVA: 0x0001FB90 File Offset: 0x0001DD90
		// (set) Token: 0x0600606B RID: 24683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060D")]
		public float y
		{
			[Token(Token = "0x6005957")]
			[Address(RVA = "0x100036988", Offset = "0x36988", VA = "0x100036988")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005956")]
			[Address(RVA = "0x100036980", Offset = "0x36980", VA = "0x100036980")]
			set
			{
			}
		}

		// Token: 0x1700066B RID: 1643
		// (get) Token: 0x0600606E RID: 24686 RVA: 0x0001FBA8 File Offset: 0x0001DDA8
		// (set) Token: 0x0600606D RID: 24685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060E")]
		public float z
		{
			[Token(Token = "0x6005959")]
			[Address(RVA = "0x100036998", Offset = "0x36998", VA = "0x100036998")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6005958")]
			[Address(RVA = "0x100036990", Offset = "0x36990", VA = "0x100036990")]
			set
			{
			}
		}

		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x06006070 RID: 24688 RVA: 0x0001FBC0 File Offset: 0x0001DDC0
		// (set) Token: 0x0600606F RID: 24687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700060F")]
		public float w
		{
			[Token(Token = "0x600595B")]
			[Address(RVA = "0x1000369A8", Offset = "0x369A8", VA = "0x1000369A8")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600595A")]
			[Address(RVA = "0x1000369A0", Offset = "0x369A0", VA = "0x1000369A0")]
			set
			{
			}
		}

		// Token: 0x1700066D RID: 1645
		// (get) Token: 0x06006072 RID: 24690 RVA: 0x0001FBD8 File Offset: 0x0001DDD8
		// (set) Token: 0x06006071 RID: 24689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000610")]
		public float Item
		{
			[Token(Token = "0x600595D")]
			[Address(RVA = "0x1000369BC", Offset = "0x369BC", VA = "0x1000369BC")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600595C")]
			[Address(RVA = "0x1000369B0", Offset = "0x369B0", VA = "0x1000369B0")]
			set
			{
			}
		}

		// Token: 0x06006073 RID: 24691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600595E")]
		[Address(RVA = "0x1000369C8", Offset = "0x369C8", VA = "0x1000369C8")]
		public Float4(Float4 value)
		{
		}

		// Token: 0x06006074 RID: 24692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600595F")]
		[Address(RVA = "0x1000369DC", Offset = "0x369DC", VA = "0x1000369DC")]
		public Float4(Vector4 value)
		{
		}

		// Token: 0x06006075 RID: 24693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005960")]
		[Address(RVA = "0x1000369F0", Offset = "0x369F0", VA = "0x1000369F0")]
		public Float4(float x, float y, float z, float w)
		{
		}

		// Token: 0x06006076 RID: 24694 RVA: 0x0001FBF0 File Offset: 0x0001DDF0
		[Token(Token = "0x6005961")]
		[Address(RVA = "0x101376C70", Offset = "0x1376C70", VA = "0x101376C70")]
		public static implicit operator Float4(Vector4 v)
		{
			return default(Float4);
		}

		// Token: 0x06006077 RID: 24695 RVA: 0x0001FC08 File Offset: 0x0001DE08
		[Token(Token = "0x6005962")]
		[Address(RVA = "0x101376C74", Offset = "0x1376C74", VA = "0x101376C74")]
		public static implicit operator Vector4(Float4 v)
		{
			return default(Vector4);
		}

		// Token: 0x06006078 RID: 24696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005963")]
		[Address(RVA = "0x100036A04", Offset = "0x36A04", VA = "0x100036A04", Slot = "6")]
		public void Add(Float4 a, Float4 b)
		{
		}

		// Token: 0x06006079 RID: 24697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005964")]
		[Address(RVA = "0x100036A28", Offset = "0x36A28", VA = "0x100036A28", Slot = "7")]
		public void Sub(Float4 a, Float4 b)
		{
		}

		// Token: 0x0600607A RID: 24698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005965")]
		[Address(RVA = "0x100036A4C", Offset = "0x36A4C", VA = "0x100036A4C", Slot = "8")]
		public void Mul(Float4 a, Float4 b)
		{
		}

		// Token: 0x0600607B RID: 24699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005966")]
		[Address(RVA = "0x100036A70", Offset = "0x36A70", VA = "0x100036A70", Slot = "9")]
		public void Mul(Float4 a, float b)
		{
		}

		// Token: 0x0600607C RID: 24700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005967")]
		[Address(RVA = "0x100036A94", Offset = "0x36A94", VA = "0x100036A94", Slot = "10")]
		public void Div(Float4 a, Float4 b)
		{
		}

		// Token: 0x0600607D RID: 24701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005968")]
		[Address(RVA = "0x100036AB0", Offset = "0x36AB0", VA = "0x100036AB0", Slot = "11")]
		public void Add(Float4 b)
		{
		}

		// Token: 0x0600607E RID: 24702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005969")]
		[Address(RVA = "0x100036ACC", Offset = "0x36ACC", VA = "0x100036ACC", Slot = "12")]
		public void Sub(Float4 b)
		{
		}

		// Token: 0x0600607F RID: 24703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600596A")]
		[Address(RVA = "0x100036AE8", Offset = "0x36AE8", VA = "0x100036AE8", Slot = "13")]
		public void Mul(Float4 b)
		{
		}

		// Token: 0x06006080 RID: 24704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600596B")]
		[Address(RVA = "0x100036B04", Offset = "0x36B04", VA = "0x100036B04", Slot = "14")]
		public void Mul(float b)
		{
		}

		// Token: 0x06006081 RID: 24705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600596C")]
		[Address(RVA = "0x100036B14", Offset = "0x36B14", VA = "0x100036B14", Slot = "15")]
		public void Div(Float4 b)
		{
		}

		// Token: 0x06006082 RID: 24706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600596D")]
		[Address(RVA = "0x100036B30", Offset = "0x36B30", VA = "0x100036B30", Slot = "16")]
		public void SetRowValue(object o)
		{
		}

		// Token: 0x06006083 RID: 24707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600596E")]
		[Address(RVA = "0x100036B38", Offset = "0x36B38", VA = "0x100036B38")]
		public void SetRowValue(float o, int componentIdx)
		{
		}

		// Token: 0x06006084 RID: 24708 RVA: 0x0001FC20 File Offset: 0x0001DE20
		[Token(Token = "0x600596F")]
		[Address(RVA = "0x100036B44", Offset = "0x36B44", VA = "0x100036B44", Slot = "4")]
		public int CompareTo(object obj)
		{
			return 0;
		}

		// Token: 0x06006085 RID: 24709 RVA: 0x0001FC38 File Offset: 0x0001DE38
		[Token(Token = "0x6005970")]
		[Address(RVA = "0x100036B60", Offset = "0x36B60", VA = "0x100036B60", Slot = "5")]
		public int CompareTo(Float4 target)
		{
			return 0;
		}

		// Token: 0x06006086 RID: 24710 RVA: 0x0001FC50 File Offset: 0x0001DE50
		[Token(Token = "0x6005971")]
		[Address(RVA = "0x100036BA0", Offset = "0x36BA0", VA = "0x100036BA0", Slot = "0")]
		public override bool Equals(object obj)
		{
			return default(bool);
		}

		// Token: 0x06006087 RID: 24711 RVA: 0x0001FC68 File Offset: 0x0001DE68
		[Token(Token = "0x6005972")]
		[Address(RVA = "0x100036BBC", Offset = "0x36BBC", VA = "0x100036BBC")]
		public bool Equals(Float4 obj)
		{
			return default(bool);
		}

		// Token: 0x06006088 RID: 24712 RVA: 0x0001FC80 File Offset: 0x0001DE80
		[Token(Token = "0x6005973")]
		[Address(RVA = "0x100036BD8", Offset = "0x36BD8", VA = "0x100036BD8", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06006089 RID: 24713 RVA: 0x0001FC98 File Offset: 0x0001DE98
		[Token(Token = "0x6005974")]
		[Address(RVA = "0x1013771A0", Offset = "0x13771A0", VA = "0x1013771A0")]
		public static Float4 operator +(Float4 v0)
		{
			return default(Float4);
		}

		// Token: 0x0600608A RID: 24714 RVA: 0x0001FCB0 File Offset: 0x0001DEB0
		[Token(Token = "0x6005975")]
		[Address(RVA = "0x101376C9C", Offset = "0x1376C9C", VA = "0x101376C9C")]
		public static Float4 operator +(Float4 v0, Float4 v1)
		{
			return default(Float4);
		}

		// Token: 0x0600608B RID: 24715 RVA: 0x0001FCC8 File Offset: 0x0001DEC8
		[Token(Token = "0x6005976")]
		[Address(RVA = "0x1013771A4", Offset = "0x13771A4", VA = "0x1013771A4")]
		public static Float4 operator -(Float4 v0)
		{
			return default(Float4);
		}

		// Token: 0x0600608C RID: 24716 RVA: 0x0001FCE0 File Offset: 0x0001DEE0
		[Token(Token = "0x6005977")]
		[Address(RVA = "0x101376CD4", Offset = "0x1376CD4", VA = "0x101376CD4")]
		public static Float4 operator -(Float4 v0, Float4 v1)
		{
			return default(Float4);
		}

		// Token: 0x0600608D RID: 24717 RVA: 0x0001FCF8 File Offset: 0x0001DEF8
		[Token(Token = "0x6005978")]
		[Address(RVA = "0x101376CE8", Offset = "0x1376CE8", VA = "0x101376CE8")]
		public static Float4 operator *(Float4 v0, Float4 v1)
		{
			return default(Float4);
		}

		// Token: 0x0600608E RID: 24718 RVA: 0x0001FD10 File Offset: 0x0001DF10
		[Token(Token = "0x6005979")]
		[Address(RVA = "0x101376D20", Offset = "0x1376D20", VA = "0x101376D20")]
		public static Float4 operator *(Float4 v0, float v1)
		{
			return default(Float4);
		}

		// Token: 0x0600608F RID: 24719 RVA: 0x0001FD28 File Offset: 0x0001DF28
		[Token(Token = "0x600597A")]
		[Address(RVA = "0x101376D34", Offset = "0x1376D34", VA = "0x101376D34")]
		public static Float4 operator /(Float4 v0, Float4 v1)
		{
			return default(Float4);
		}

		// Token: 0x06006090 RID: 24720 RVA: 0x0001FD40 File Offset: 0x0001DF40
		[Token(Token = "0x600597B")]
		[Address(RVA = "0x1013771B8", Offset = "0x13771B8", VA = "0x1013771B8")]
		public static Float4 operator /(Float4 v0, float v1)
		{
			return default(Float4);
		}

		// Token: 0x06006091 RID: 24721 RVA: 0x0001FD58 File Offset: 0x0001DF58
		[Token(Token = "0x600597C")]
		[Address(RVA = "0x1013770D8", Offset = "0x13770D8", VA = "0x1013770D8")]
		public static bool operator ==(Float4 v0, Float4 v1)
		{
			return default(bool);
		}

		// Token: 0x06006092 RID: 24722 RVA: 0x0001FD70 File Offset: 0x0001DF70
		[Token(Token = "0x600597D")]
		[Address(RVA = "0x1013771CC", Offset = "0x13771CC", VA = "0x1013771CC")]
		public static bool operator !=(Float4 v0, Float4 v1)
		{
			return default(bool);
		}

		// Token: 0x04006F1C RID: 28444
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004ED9")]
		[SerializeField]
		public Vector4 m_Value;
	}
}
