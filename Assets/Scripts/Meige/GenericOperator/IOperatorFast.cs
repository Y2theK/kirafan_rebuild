﻿namespace Meige.GenericOperator {
    public interface IOperatorFast<T> {
        void Add(T a, T b);
        void Sub(T a, T b);
        void Mul(T a, T b);
        void Mul(T a, float b);
        void Div(T a, T b);
        void Add(T b);
        void Sub(T b);
        void Mul(T b);
        void Mul(float b);
        void Div(T b);
        void SetRowValue(object o);
    }
}
