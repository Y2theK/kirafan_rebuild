﻿using System;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige.GenericOperator
{
	// Token: 0x0200133A RID: 4922
	[Token(Token = "0x2000CC5")]
	[StructLayout(3)]
	public static class Operator<T>
	{
		// Token: 0x06006431 RID: 25649 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C02")]
		[Address(RVA = "0x1016BA014", Offset = "0x16BA014", VA = "0x1016BA014")]
		public static Func<T, T, T> Lambda(Func<ParameterExpression, ParameterExpression, BinaryExpression> op)
		{
			return null;
		}

		// Token: 0x06006432 RID: 25650 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005C03")]
		[Address(RVA = "0x1016BA384", Offset = "0x16BA384", VA = "0x1016BA384")]
		public static Func<T, float, T> LambdaFloat(Func<ParameterExpression, ParameterExpression, BinaryExpression> op)
		{
			return null;
		}

		// Token: 0x04007544 RID: 30020
		[Token(Token = "0x40052F3")]
		private static readonly ParameterExpression x;

		// Token: 0x04007545 RID: 30021
		[Token(Token = "0x40052F4")]
		private static readonly ParameterExpression y;

		// Token: 0x04007546 RID: 30022
		[Token(Token = "0x40052F5")]
		private static readonly ParameterExpression z;

		// Token: 0x04007547 RID: 30023
		[Token(Token = "0x40052F6")]
		private static readonly ParameterExpression w;

		// Token: 0x04007548 RID: 30024
		[Token(Token = "0x40052F7")]
		public static readonly Func<T, T, T> Add;

		// Token: 0x04007549 RID: 30025
		[Token(Token = "0x40052F8")]
		public static readonly Func<T, T, T> Sub;

		// Token: 0x0400754A RID: 30026
		[Token(Token = "0x40052F9")]
		public static readonly Func<T, T, T> Mul;

		// Token: 0x0400754B RID: 30027
		[Token(Token = "0x40052FA")]
		public static readonly Func<T, T, T> Div;

		// Token: 0x0400754C RID: 30028
		[Token(Token = "0x40052FB")]
		public static readonly Func<T, float, T> MulFloat;

		// Token: 0x0400754D RID: 30029
		[Token(Token = "0x40052FC")]
		public static readonly Func<T, T, T, T, T> ProductSum;

		// Token: 0x0400754E RID: 30030
		[Token(Token = "0x40052FD")]
		public static readonly Func<T, T, T, T, T> ProductDifference;
	}
}
