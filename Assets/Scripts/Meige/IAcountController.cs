﻿using UnityEngine;

namespace Meige {
    public class IAcountController : MonoBehaviour {
        private static IAcountController ms_Inst;

        private eState m_State;
        private string m_UserName;
        private string m_UserID;

#if UNITY_ANDROID
		private AndroidGoogleSignIn m_PluginScript;
#endif

        public static void Create() {
            CGlobalInstance.CreateInstance();
            ms_Inst = CGlobalInstance.EntryClass<IAcountController>();
            ms_Inst.m_State = eState.Invalid;
        }

        private void Start() {
#if UNITY_IOS
			if (Social.localUser.authenticated) {
                m_UserName = Social.localUser.userName;
                m_UserID = Social.localUser.id;
                m_State = eState.Online;
                Debug.Log(string.Format("PlatformAccount : {0} : {1}", m_UserName, m_UserID));
            } else {
                Debug.Log("PlatformAccount LogIn Start");
                Social.localUser.Authenticate(CallbackAcount);
            }
#elif UNITY_ANDROID
            if (m_PluginScript == null) {
                m_PluginScript = AndroidGoogleSignIn.Init(gameObject);
            }
#endif
        }

        private void CallbackAcount(bool fsuccess, string str) {
            if (fsuccess) {
                m_UserName = Social.localUser.userName;
                m_UserID = Social.localUser.id;
                Debug.Log(string.Format("PlatformAccount : {0} : {1} : {2}", m_UserName, m_UserID, str));
                m_State = eState.Online;
            } else {
                Debug.LogFormat("PlatformAccount Non : {0}", str);
                m_State = eState.SignOut;
            }
        }

#if UNITY_ANDROID
		private void CallbackSuccess(AndroidGoogleSignInAccount account) {
            m_UserName = account.DisplayName;
            m_UserID = account.Id;
            m_State = eState.Online;
            Debug.Log(string.Format("PlatformAccount : {0} : {1}", m_UserName, m_UserID));
        }

		private void CallbackError(string msg) {
            Debug.LogFormat("PlatformAccount Non : {0}", msg);
            m_State = eState.SignOut;
        }
#endif

        public static void ResetCallback() {
#if UNITY_IOS
			if (!Social.localUser.authenticated && ms_Inst != null) {
                Debug.Log("PlatformAccount LogIn Start");
                Social.localUser.Authenticate(ms_Inst.CallbackAcount);
            }
#elif UNITY_ANDROID
			ms_Inst.m_State = eState.Invalid;
            if (ms_Inst.m_PluginScript == null) {
                ms_Inst.m_PluginScript = AndroidGoogleSignIn.Init(ms_Inst.gameObject);
            }
            if (ms_Inst.m_PluginScript == null) {
                ms_Inst.m_State = eState.SignOut;
				return;
            }
            Debug.Log("PlatformAccount LogIn Start");
            ms_Inst.m_PluginScript.SignIn("624793215742-5picl2ej69ov89ghmcpsgqo7mau36fk2.apps.googleusercontent.com", ms_Inst.CallbackSuccess, ms_Inst.CallbackError);
#else
            ms_Inst.m_State = eState.SignOut;
#endif
        }

        public static eState GetState() {
            return ms_Inst.m_State;
        }

        public static string GetUserName() {
            return ms_Inst.m_UserName;
        }

        public static string GetUserID() {
            return ms_Inst.m_UserID;
        }

        public static bool IsSetup() {
            return ms_Inst != null;
        }

        public enum eState {
            Invalid = -1,
            SignOut,
            SignIn,
            Online
        }
    }
}
