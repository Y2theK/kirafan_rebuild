﻿using System;
using System.Threading;

namespace Meige {
    public class Job {
        private JobState m_state;
        private bool m_isError;
        private int m_jobFlagMask;
        private WaitCallback m_func;
        private object m_arg;

        public Job() {
            Clear();
        }

        public void Clear() {
            m_state = JobState.Wait;
            m_isError = false;
            m_jobFlagMask = 0;
            m_func = null;
            m_arg = null;
        }

        public void SetFunction(WaitCallback func, object arg) {
            m_func = func;
            m_arg = arg;
        }

        public void Invoke() {
            m_state = JobState.Prosessing;
            try {
                m_func?.Invoke(m_arg);
            } catch (Exception) {
                m_isError = true;
            }
            m_state = JobState.Done;
        }

        public bool IsWait() {
            return m_state == JobState.Wait;
        }

        public bool IsProsessing() {
            return m_state == JobState.Prosessing;
        }

        public bool IsDone() {
            return m_state == JobState.Done;
        }

        public bool IsError() {
            return m_isError;
        }

        public void SetJobFlagMask(int flagMask) {
            m_jobFlagMask = flagMask;
        }

        public int GetJobFlagMask() {
            return m_jobFlagMask;
        }

        public object GetObject() {
            return m_arg;
        }

        public enum JobState {
            Wait,
            Prosessing,
            Done
        }
    }
}
