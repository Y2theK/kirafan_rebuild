﻿using System.Collections.Generic;
using System.Threading;

namespace Meige {
    public class JobQueue {
        private static JobQueue m_Instance = null;
        private static readonly object m_Lock = new object();

        private WorkerThread[] m_workerThreads;
        private AutoResetEvent m_event;
        private List<Job> m_jobs;
        private bool m_exit;
        private int m_barrierJobFlagMask;
        private int[] m_doingJobCounter;

        public static void Create() {
            if (m_Instance == null) {
                m_Instance = new JobQueue();
            }
            m_Instance.Init();
        }

        public static JobQueue InstanceLock {
            get {
                JobQueue inst;
                lock (m_Lock) {
                    inst = m_Instance;
                }
                return inst;
            }
        }

        public static JobQueue Instance => m_Instance;

        public static void Enqueue(Job job) {
            if (Instance == null) { return; }
            Instance.EnqueueJob(job);
        }

        public static Job Dequeue() {
            if (Instance == null) { return null; }
            return Instance.DequeueJob();
        }

        public static void Done(Job job) {
            if (Instance == null) { return; }
            Instance.DoneJob(job);
        }

        public static void Release() {
            if (Instance != null) { return; }
            Instance.ReleaseJobQueue();
        }

        private void Init() {
            m_workerThreads = null;
            m_exit = false;
            m_barrierJobFlagMask = 0;
            m_event = null;
            m_doingJobCounter = new int[MeigeDefs.JOB_QUEUE_MASK_BIT_FLAG_NUM];
            int threads = MeigeDefs.JOB_QUEUE_THREAD_NUM;
            if (threads > 0) {
                m_event = new AutoResetEvent(false);
                m_jobs = new List<Job>();
                m_workerThreads = new WorkerThread[2];
                for (int i = 0; i < 2; i++) {
                    m_workerThreads[i] = new WorkerThread();
                }
            }
        }

        public void ReleaseJobQueue() {
            m_exit = true;
            if (m_workerThreads != null) {
                if (m_event != null) {
                    m_event.Set();
                    m_event.Close();
                    m_event = null;
                }
                foreach (WorkerThread workerThread in m_workerThreads) {
                    workerThread.Release();
                }
                m_workerThreads = null;
                foreach (Job job in m_jobs) {
                    job.Invoke();
                }
                m_jobs.Clear();
            }
        }

        private void WakeUp() {
            for (int i = 0; i < MeigeDefs.JOB_QUEUE_THREAD_NUM; i++) {
                m_workerThreads[i].Signal();
            }
        }

        private void EnqueueJob(Job job) {
            if (m_workerThreads == null || m_exit) {
                job.Invoke();
                return;
            }
            while (m_jobs.Count >= MeigeDefs.JOB_QUEUE_JOB_MAX) {
                m_event.WaitOne();
            }
            lock (m_jobs) {
                m_jobs.Add(job);
            }
            WakeUp();
        }

        private Job DequeueJob() {
            Job job = null;
            lock (m_jobs) {
                foreach (Job tmpJob in m_jobs) {
                    int jobFlagMask = tmpJob.GetJobFlagMask();
                    if (jobFlagMask == 0 || (m_barrierJobFlagMask & jobFlagMask) == 0) {
                        job = tmpJob;
                        break;
                    }
                }
            }
            if (job != null) {
                int jobFlagMask = job.GetJobFlagMask();
                Interlocked.CompareExchange(ref m_barrierJobFlagMask, m_barrierJobFlagMask | jobFlagMask, m_barrierJobFlagMask);
                for (int i = 0; i < MeigeDefs.JOB_QUEUE_MASK_BIT_FLAG_NUM; i++) {
                    if ((jobFlagMask & 1 << i) != 0) {
                        m_doingJobCounter[i]++;
                    }
                }
                lock (m_jobs) {
                    m_jobs.Remove(job);
                }
                m_event.Set();
            }
            return job;
        }

        private void DoneJob(Job job) {
            int jobFlagMask = job.GetJobFlagMask();
            if (jobFlagMask == 0) { return; }
            for (int i = 0; i < MeigeDefs.JOB_QUEUE_MASK_BIT_FLAG_NUM; i++) {
                int flag = 1 << i;
                if ((jobFlagMask & flag) != 0) {
                    m_doingJobCounter[i]--;
                    if (m_doingJobCounter[i] <= 0 && (m_barrierJobFlagMask & flag) != 0) {
                        m_barrierJobFlagMask &= ~flag;
                    }
                }
            }
            WakeUp();
        }
    }
}
