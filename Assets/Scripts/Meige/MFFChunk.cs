﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012FF RID: 4863
	[Token(Token = "0x2000CA1")]
	[StructLayout(0, Size = 16)]
	public struct MFFChunk
	{
		// Token: 0x04007404 RID: 29700
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4005218")]
		public uint m_HashKey;

		// Token: 0x04007405 RID: 29701
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4005219")]
		public uint m_BodySize;

		// Token: 0x04007406 RID: 29702
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400521A")]
		public uint m_OffsetToBody;

		// Token: 0x04007407 RID: 29703
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400521B")]
		public byte m_Type;

		// Token: 0x04007408 RID: 29704
		[Cpp2IlInjected.FieldOffset(Offset = "0xD")]
		[Token(Token = "0x400521C")]
		public byte m_cPad;

		// Token: 0x04007409 RID: 29705
		[Cpp2IlInjected.FieldOffset(Offset = "0xE")]
		[Token(Token = "0x400521D")]
		public ushort m_ElementNum;
	}
}
