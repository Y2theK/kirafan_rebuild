﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012FE RID: 4862
	[Token(Token = "0x2000CA0")]
	[StructLayout(0, Size = 16)]
	public struct MFFHeader
	{
		// Token: 0x040073FC RID: 29692
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4005210")]
		public byte m_Magic0;

		// Token: 0x040073FD RID: 29693
		[Cpp2IlInjected.FieldOffset(Offset = "0x1")]
		[Token(Token = "0x4005211")]
		public byte m_Magic1;

		// Token: 0x040073FE RID: 29694
		[Cpp2IlInjected.FieldOffset(Offset = "0x2")]
		[Token(Token = "0x4005212")]
		public byte m_Magic2;

		// Token: 0x040073FF RID: 29695
		[Cpp2IlInjected.FieldOffset(Offset = "0x3")]
		[Token(Token = "0x4005213")]
		public byte m_Magic3;

		// Token: 0x04007400 RID: 29696
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4005214")]
		public uint m_MFFVersion;

		// Token: 0x04007401 RID: 29697
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4005215")]
		public uint m_Version;

		// Token: 0x04007402 RID: 29698
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4005216")]
		public short m_Platform;

		// Token: 0x04007403 RID: 29699
		[Cpp2IlInjected.FieldOffset(Offset = "0xE")]
		[Token(Token = "0x4005217")]
		public ushort m_ChunkCnt;
	}
}
