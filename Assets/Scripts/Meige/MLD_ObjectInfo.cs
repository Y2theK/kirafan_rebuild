﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001300 RID: 4864
	[Token(Token = "0x2000CA2")]
	[StructLayout(0, Size = 8)]
	public struct MLD_ObjectInfo
	{
		// Token: 0x0400740A RID: 29706
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400521E")]
		public ushort m_ID;

		// Token: 0x0400740B RID: 29707
		[Cpp2IlInjected.FieldOffset(Offset = "0x2")]
		[Token(Token = "0x400521F")]
		public ushort m_Size;

		// Token: 0x0400740C RID: 29708
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4005220")]
		public int m_DataOffs;
	}
}
