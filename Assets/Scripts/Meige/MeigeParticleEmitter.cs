﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meige {
    public class MeigeParticleEmitter : EffectComponentBase {
        [SerializeField] private ParticleRule m_Rule;
        [SerializeField] public Material m_Material;

        public RenderSettings m_RenderSettings;

        private Transform m_thisTransform;
        private Matrix4x4 m_Matrix;
        private Matrix4x4 m_OldMatrix;
        private bool m_isMatrixDirty;
        private Vector3[] m_WorkPaths = new Vector3[4];

        private RenderSettings m_RenderSettingsFixed;
        private GameObject m_RenderObject;
        private bool m_RenderObjectIsSelf;

        private PrimBillboardBuffer m_PrimBillboardBuffer;
        private PrimPointBuffer m_PrimPointBuffer;
        private PrimLineBuffer m_PrimLineBuffer;
        private PrimPolyLineBuffer m_PrimPolylineBuffer;
        private PrimConfettiBuffer m_PrimConfettiBuffer;
        private PrimPolyLineBuffer m_PrimRibbonBuffer;
        private PrimBillboardBuffer.BillboardBuffer m_BillboardBuffer;
        private PrimPointBuffer.PointBuffer m_PointBuffer;
        private PrimLineBuffer.LineBuffer m_LineBuffer;
        private PrimPolyLineBuffer.PolyLineBuffer m_PolylineBuffer;
        private PrimConfettiBuffer.ConfettiBuffer m_ConfettiBuffer;
        private PrimPolyLineBuffer.PolyLineBuffer m_RibbonBuffer;
        private EffectRenderer m_EffectRenderer;

        private GameObject m_TailRenderObject;
        private PrimPolyLineBuffer m_PrimTailBuffer;
        private PrimPolyLineBuffer.PolyLineBuffer m_TailBuffer;
        private EffectRenderer m_TailRenderer;

        private List<ParticleUnit> m_ActiveUnitArray = new List<ParticleUnit>();
        private List<ParticleUnit> m_InActiveUnitArray = new List<ParticleUnit>();

        private float m_Time;
        private float m_FrameRate;
        private int m_SortingOrder;
        private int m_Layer;
        private bool m_isInitialized;
        public Vector3 m_offsetRot = Vector3.zero;
        private History m_History;
        private bool m_isLocalTransHistory;

        private delegate void CalcFuncList(ParticleUnit unit);
        public delegate void HookUpdate();

        private CalcFuncList m_AttachFuncList;
        private CalcFuncList m_CalcFuncList;
        private HookUpdate m_UpdateHook_Shot;
        private HookUpdate m_UpdateHook_Before;
        private HookUpdate m_UpdateHook_After;

        public override bool isAlive {
            get => m_isAlive;
            protected set => m_isAlive = value;
        }

        public Vector3 offsetRot {
            get => m_offsetRot;
            set {
                m_offsetRot = value;
                if (m_RenderObject != null) {
                    m_RenderObject.transform.localEulerAngles = m_offsetRot;
                }
            }
        }

        public void AddHook_UpdateShot(HookUpdate hook) {
            m_UpdateHook_Shot += hook;
        }

        public void RemoveHook_UpdateShot(HookUpdate hook) {
            m_UpdateHook_Shot -= hook;
        }

        public void AddHook_UpdateBefore(HookUpdate hook) {
            m_UpdateHook_Before += hook;
        }

        public void RemoveHook_UpdateBefore(HookUpdate hook) {
            m_UpdateHook_Before -= hook;
        }

        public void AddHook_UpdateAfter(HookUpdate hook) {
            m_UpdateHook_After += hook;
        }

        public void RemoveHook_UpdateAfter(HookUpdate hook) {
            m_UpdateHook_After -= hook;
        }

        public float time => m_Time;
        public float framerate => m_FrameRate;

        public ParticleRule rule {
            get => m_Rule;
            set => m_Rule = value;
        }

        public static float GetRandomRadian() {
            return Helper.GetFRandom(-3.141592f, 3.141592f);
        }

        public static float GetRandomDegree() {
            return 57.29578f * GetRandomRadian();
        }

        public void SetLayer(int layer) {
            m_Layer = layer;
            if (m_RenderObject != null) {
                m_RenderObject.layer = layer;
            }
        }

        private void FlushTime() {
            m_Time = Time.deltaTime * rule.m_frameSpeedScale;
            m_FrameRate = Time.deltaTime / Time.fixedDeltaTime * rule.m_frameSpeedScale;
        }

        private void ResetTransform(Transform trans) {
            trans.localPosition = Vector3.zero;
            trans.localEulerAngles = Vector3.zero;
            trans.localScale = Vector3.one;
        }

        private void ChangeParent() {
            if (m_PrimBillboardBuffer) {
                m_PrimBillboardBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimBillboardBuffer.transform);
            }
            if (m_PrimPointBuffer) {
                m_PrimPointBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimPointBuffer.transform);
            }
            if (m_PrimLineBuffer) {
                m_PrimLineBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimLineBuffer.transform);
            }
            if (m_PrimConfettiBuffer) {
                m_PrimConfettiBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimConfettiBuffer.transform);
            }
            if (m_PrimPolylineBuffer) {
                m_PrimPolylineBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimPolylineBuffer.transform);
            }
            if (m_PrimRibbonBuffer) {
                m_PrimRibbonBuffer.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_PrimRibbonBuffer.transform);
            }
            if (m_TailRenderObject) {
                m_TailRenderObject.transform.parent = EffectObjectManager.Instance.m_ThisTransform;
                ResetTransform(m_TailRenderObject.transform);
            }
        }

        public void SetSortingOrder(int order) {
            m_SortingOrder = order;
            if (m_EffectRenderer != null) {
                m_EffectRenderer.SetSortingOrder(m_SortingOrder);
            }
            if (m_TailRenderer != null) {
                m_TailRenderer.SetSortingOrder(m_SortingOrder);
            }
        }

        private void DestroyObject(bool isForce) {
            KillOnlyParticles();
            m_ActiveUnitArray.Clear();
            m_InActiveUnitArray.Clear();
            if (m_History.m_particleType != rule.m_particleType || isForce) {
                if (m_PrimBillboardBuffer) {
                    m_PrimBillboardBuffer.transform.DetachChildren();
                    Destroy(m_PrimBillboardBuffer);
                    m_PrimBillboardBuffer.RemoveBuffer(m_BillboardBuffer);
                    m_PrimBillboardBuffer = null;
                    m_BillboardBuffer = null;
                }
                if (m_PrimPointBuffer) {
                    m_PrimPointBuffer.transform.DetachChildren();
                    Destroy(m_PrimPointBuffer);
                    m_PrimPointBuffer.RemoveBuffer(m_PointBuffer);
                    m_PrimPointBuffer = null;
                    m_PointBuffer = null;
                }
                if (m_PrimLineBuffer) {
                    m_PrimLineBuffer.transform.DetachChildren();
                    Destroy(m_PrimLineBuffer);
                    m_PrimLineBuffer.RemoveBuffer(m_LineBuffer);
                    m_PrimLineBuffer = null;
                    m_LineBuffer = null;
                }
                if (m_PrimPolylineBuffer) {
                    m_PrimPolylineBuffer.transform.DetachChildren();
                    Destroy(m_PrimPolylineBuffer);
                    m_PrimPolylineBuffer.RemoveBuffer(m_PolylineBuffer);
                    m_PrimPolylineBuffer = null;
                    m_PolylineBuffer = null;
                }
                if (m_PrimConfettiBuffer) {
                    m_PrimConfettiBuffer.transform.DetachChildren();
                    Destroy(m_PrimConfettiBuffer);
                    m_PrimConfettiBuffer.RemoveBuffer(m_ConfettiBuffer);
                    m_PrimConfettiBuffer = null;
                    m_ConfettiBuffer = null;
                }
                if (m_PrimRibbonBuffer) {
                    m_PrimRibbonBuffer.transform.DetachChildren();
                    Destroy(m_PrimRibbonBuffer);
                    m_PrimRibbonBuffer.RemoveBuffer(m_RibbonBuffer);
                    m_PrimRibbonBuffer = null;
                    m_RibbonBuffer = null;
                }
            }
            if ((m_History.m_UsingTailFlg != rule.m_UsingTailFlg || m_History.m_particleNum != rule.m_particleNum || isForce) && m_TailRenderObject) {
                m_TailRenderObject.transform.DetachChildren();
                Destroy(m_TailRenderObject);
                m_PrimTailBuffer.RemoveBuffer(m_TailBuffer);
                m_PrimTailBuffer = null;
                m_TailBuffer = null;
            }
            if (m_History.m_particleNum != rule.m_particleNum || isForce) {
                if (m_BillboardBuffer != null) {
                    m_PrimBillboardBuffer.RemoveBuffer(m_BillboardBuffer);
                    m_BillboardBuffer = null;
                }
                if (m_PointBuffer != null) {
                    m_PrimPointBuffer.RemoveBuffer(m_PointBuffer);
                    m_PointBuffer = null;
                }
                if (m_LineBuffer != null) {
                    m_PrimLineBuffer.RemoveBuffer(m_LineBuffer);
                    m_LineBuffer = null;
                }
                if (m_PolylineBuffer != null) {
                    m_PrimPolylineBuffer.RemoveBuffer(m_PolylineBuffer);
                    m_PolylineBuffer = null;
                }
                if (m_ConfettiBuffer != null) {
                    m_PrimConfettiBuffer.RemoveBuffer(m_ConfettiBuffer);
                    m_ConfettiBuffer = null;
                }
                if (m_RibbonBuffer != null) {
                    m_PrimRibbonBuffer.RemoveBuffer(m_RibbonBuffer);
                    m_RibbonBuffer = null;
                }
                if (m_PrimTailBuffer != null) {
                    m_PrimTailBuffer.RemoveBuffer(m_TailBuffer);
                    m_TailBuffer = null;
                }
            }
        }

        private bool Setup() {
            if (rule == null || rule.m_particleNum <= 0) {
                return false;
            }
            DestroyObject(false);
            switch (rule.m_particleType) {
                case eParticleType.eParticleType_Billboard:
                    if (m_PrimBillboardBuffer == null) {
                        m_PrimBillboardBuffer = m_RenderObject.AddComponent<PrimBillboardBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimBillboardBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimBillboard);
                        }
                    }
                    if (m_BillboardBuffer == null) {
                        m_BillboardBuffer = m_PrimBillboardBuffer.AddBuffer(rule.m_particleNum, true);
                    }
                    m_BillboardBuffer.rotateType = rule.m_particleTypeParam.m_billboard.m_RotateType;
                    break;
                case eParticleType.eParticleType_Point:
                    if (m_PrimPointBuffer == null) {
                        m_PrimPointBuffer = m_RenderObject.AddComponent<PrimPointBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimPointBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimPoint);
                        }
                    }
                    if (m_PointBuffer == null) {
                        m_PointBuffer = m_PrimPointBuffer.AddBuffer(rule.m_particleNum);
                    }
                    break;
                case eParticleType.eParticleType_Line:
                    if (m_PrimLineBuffer == null) {
                        m_PrimLineBuffer = m_RenderObject.AddComponent<PrimLineBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimLineBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimLine);
                        }
                    }
                    if (m_LineBuffer == null) {
                        m_LineBuffer = m_PrimLineBuffer.AddBuffer(rule.m_particleNum, rule.m_particleTypeParam.m_line.m_jointNum + 2);
                    }
                    break;
                case eParticleType.eParticleType_PolyLine:
                    if (m_PrimPolylineBuffer == null) {
                        m_PrimPolylineBuffer = m_RenderObject.AddComponent<PrimPolyLineBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimPolylineBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimPolyline);
                        }
                    }
                    if (m_PolylineBuffer == null) {
                        m_PolylineBuffer = m_PrimPolylineBuffer.AddBuffer(rule.m_particleNum, rule.m_particleTypeParam.m_polyLine.m_jointNum + 2);
                    }
                    break;
                case eParticleType.eParticleType_Confetti:
                    if (m_PrimConfettiBuffer == null) {
                        m_PrimConfettiBuffer = m_RenderObject.AddComponent<PrimConfettiBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimConfettiBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimConfetti);
                        }
                    }
                    if (m_ConfettiBuffer == null) {
                        m_ConfettiBuffer = m_PrimConfettiBuffer.AddBuffer(rule.m_particleNum);
                    }
                    break;
                case eParticleType.eParticleType_Ribbon:
                    if (m_PrimRibbonBuffer == null) {
                        m_PrimRibbonBuffer = m_RenderObject.AddComponent<PrimPolyLineBuffer>();
                        if (m_RenderSettingsFixed != null) {
                            m_PrimRibbonBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimRibbon);
                        }
                    }
                    if (m_RibbonBuffer == null) {
                        m_RibbonBuffer = m_PrimRibbonBuffer.AddBuffer(rule.m_particleNum, rule.m_particleTypeParam.m_ribbon.m_jointNum + 2);
                    }
                    break;
            }
            if (rule.m_UsingTailFlg && m_TailRenderObject == null && m_TailRenderer == null) {
                m_TailRenderObject = new GameObject("m_TailObject");
                Transform transform = m_TailRenderObject.transform;
                transform.position = Vector3.zero;
                transform.eulerAngles = Vector3.zero;
                transform.localScale = Vector3.one;
                transform.parent = m_RenderObject.transform;
                m_TailRenderer = m_TailRenderObject.AddComponent<EffectRenderer>();
                if (m_TailRenderer != null) {
                    m_TailRenderer.SetSortingOrder(m_SortingOrder);
                }
                m_PrimTailBuffer = m_TailRenderObject.AddComponent<PrimPolyLineBuffer>();
                if (m_RenderSettingsFixed != null) {
                    m_PrimTailBuffer.SetExternalMeshBuffer(m_RenderSettingsFixed.m_MeshBufferPrimTail);
                }
                m_TailBuffer = m_PrimTailBuffer.AddBuffer(rule.m_particleNum, rule.m_TailComponent.m_tailJointNum + 2);
            }
            m_ActiveUnitArray.Clear();
            m_InActiveUnitArray.Clear();
            for (int i = 0; i < rule.m_particleNum; i++) {
                ParticleUnit particleUnit = new ParticleUnit();
                particleUnit.m_Index = i;
                particleUnit.m_activeFlg = false;
                switch (rule.m_particleType) {
                    case eParticleType.eParticleType_Line:
                        if (rule.m_particleTypeParam.m_line.m_HistoryPointNum == 0) {
                            particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum = (short)(rule.m_particleTypeParam.m_line.m_jointNum + 2);
                        } else {
                            particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum = (short)rule.m_particleTypeParam.m_line.m_HistoryPointNum;
                        }
                        particleUnit.m_particleTypeParam.m_line.m_pPos = new Vector3[particleUnit.m_particleTypeParam.m_line.m_HistoryPointNum];
                        particleUnit.m_particleTypeParam.m_line.m_jointNum = (short)rule.m_particleTypeParam.m_line.m_jointNum;
                        break;
                    case eParticleType.eParticleType_PolyLine:
                        if (rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum == 0) {
                            particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum = (short)(rule.m_particleTypeParam.m_polyLine.m_jointNum + 2);
                        } else {
                            particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum = (short)rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum;
                        }
                        particleUnit.m_particleTypeParam.m_polyLine.m_pPos = new Vector3[particleUnit.m_particleTypeParam.m_polyLine.m_HistoryPointNum];
                        particleUnit.m_particleTypeParam.m_polyLine.m_jointNum = (short)rule.m_particleTypeParam.m_polyLine.m_jointNum;
                        break;
                    case eParticleType.eParticleType_Ribbon:
                        if (rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum == 0) {
                            particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum = (short)(rule.m_particleTypeParam.m_ribbon.m_jointNum + 2);
                        } else {
                            particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum = (short)rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum;
                        }
                        particleUnit.m_particleTypeParam.m_ribbon.m_pPos = new Vector3[particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum];
                        particleUnit.m_particleTypeParam.m_ribbon.m_pVelocity = new Vector3[particleUnit.m_particleTypeParam.m_ribbon.m_HistoryPointNum];
                        particleUnit.m_particleTypeParam.m_ribbon.m_jointNum = (short)rule.m_particleTypeParam.m_ribbon.m_jointNum;
                        break;
                }
                if (rule.m_UsingTailFlg) {
                    particleUnit.m_TailComponent.m_pTailPos = new Vector3[rule.m_TailComponent.m_tailJointNum + 2];
                }
                if (rule.m_UsingPathMoveFlg) {
                    particleUnit.m_PathMoveComponent.m_offsetPos = new Vector3[4];
                }
                m_InActiveUnitArray.Add(particleUnit);
            }
            m_AttachFuncList = null;
            if (rule.m_UsingAccelerationFlg) {
                m_AttachFuncList += AttachFunc_Move_Acceleration;
            }
            if (rule.m_UsingRotationFlg) {
                m_AttachFuncList += AttachFunc_Move_Rotation;
            }
            if (rule.m_UsingTailFlg) {
                m_AttachFuncList += AttachFunc_Move_Tail;
            }
            if (rule.m_UsingPathMoveFlg) {
                m_AttachFuncList += AttachFunc_Move_PathMove;
            }
            if (rule.m_UsingColorCurveFlg) {
                m_AttachFuncList += AttachFunc_ColorAnim_ColorCurve;
            }
            if (rule.m_UsingBlinkFlg) {
                m_AttachFuncList += AttachFunc_ColorAnim_Blink;
            }
            if (rule.m_UsingUVAnimationFlg) {
                m_AttachFuncList += AttachFunc_UVAnim_UVAnimation;
            } else {
                m_AttachFuncList += AttachFunc_UVAnim;
            }
            m_CalcFuncList = null;
            if (rule.m_UsingAccelerationFlg) {
                m_CalcFuncList += CalcFunc_Move_Acceleration;
            }
            if (rule.m_UsingRotationFlg) {
                m_CalcFuncList += CalcFunc_Move_Rotation;
            }
            if (rule.m_UsingPathMoveFlg) {
                m_CalcFuncList += CalcFunc_Move_PathMove;
            }
            m_CalcFuncList += CalcFunc_Move;
            if (rule.m_UsingColorCurveFlg) {
                m_CalcFuncList += CalcFunc_ColorAnim_ColorCurve;
            } else {
                m_CalcFuncList += CalcFunc_ColorAnim_NotColorCurve;
            }
            if (rule.m_UsingBlinkFlg) {
                m_CalcFuncList += CalcFunc_ColorAnim_Blink;
            }
            if (rule.m_UsingUVAnimationFlg) {
                m_CalcFuncList += CalcFunc_UVAnim_UVAnimation;
            }
            m_CalcFuncList += CalcFunc_LifeSpan;
            m_CalcFuncList += CalcFuncMerge;
            if (rule.m_UsingRotationFlg) {
                m_CalcFuncList += CalcFuncMerge_Rotation;
            } else {
                m_CalcFuncList += CalcFuncMerge_NotRotation;
            }
            if (rule.m_UsingTailFlg) {
                m_CalcFuncList += CalcFuncMerge_Tail;
            }
            m_History.m_particleType = rule.m_particleType;
            m_History.m_particleNum = rule.m_particleNum;
            m_History.m_UsingAccelerationFlg = rule.m_UsingAccelerationFlg;
            m_History.m_UsingRotationFlg = rule.m_UsingRotationFlg;
            m_History.m_UsingUVAnimationFlg = rule.m_UsingUVAnimationFlg;
            m_History.m_UsingColorCurveFlg = rule.m_UsingColorCurveFlg;
            m_History.m_UsingTailFlg = rule.m_UsingTailFlg;
            m_History.m_UsingBlinkFlg = rule.m_UsingBlinkFlg;
            m_History.m_UsingPathMoveFlg = rule.m_UsingPathMoveFlg;
            ChangeParent();
            UpdateDirtyMesh(false);
            m_isLocalTransHistory = rule.m_isLocalTrans;
            isAlive = isAlive;
            return true;
        }

        protected override void Release() {
            DestroyObject(true);
            if (m_RenderObjectIsSelf) {
                if (m_TailRenderObject != null) {
                    Destroy(m_TailRenderObject);
                }
                if (m_RenderObject != null) {
                    Destroy(m_RenderObject);
                }
            }
            base.Release();
        }

        private void Start() {
            m_RenderSettingsFixed = m_RenderSettings;
            if (m_RenderSettingsFixed != null) {
                if (m_RenderSettingsFixed.m_MeshBufferPrimBillboard != null) { } //Why?
                if (m_RenderSettingsFixed.m_MeshBufferPrimPoint != null) { }
                if (m_RenderSettingsFixed.m_MeshBufferPrimLine != null) { }
                if (m_RenderSettingsFixed.m_MeshBufferPrimPolyline != null) { }
                if (m_RenderSettingsFixed.m_MeshBufferPrimConfetti != null) { }
                if (m_RenderSettingsFixed.m_MeshBufferPrimRibbon != null) { }
                if (m_RenderSettingsFixed.m_MeshBufferPrimTail != null) { }
                m_EffectRenderer = m_RenderSettingsFixed.m_EffectRenderer;
            }
            if (m_EffectRenderer == null) {
                m_RenderObject = new GameObject("ParticleEmitterRenderObject");
                m_RenderObject.transform.localPosition = Vector3.zero;
                m_RenderObject.transform.localEulerAngles = m_offsetRot;
                m_RenderObject.transform.parent = transform;
                m_EffectRenderer = m_RenderObject.AddComponent<EffectRenderer>();
                m_RenderObjectIsSelf = true;
            } else {
                m_RenderObject = m_EffectRenderer.gameObject;
                m_RenderObjectIsSelf = false;
            }
            m_RenderObject.layer = m_Layer;
            if (m_EffectRenderer != null) {
                m_EffectRenderer.SetSortingOrder(m_SortingOrder);
            }
            m_thisTransform = transform;
            isAlive = false;
            m_isLocalTransHistory = rule.m_isLocalTrans;
            FlushTime();
        }

        public bool KillOnlyParticles() {
            if (!isAlive) {
                return false;
            }
            int count = m_ActiveUnitArray.Count;
            for (int i = 0; i < count; i++) {
                ParticleUnit particleUnit = m_ActiveUnitArray[0];
                particleUnit.m_activeFlg = false;
                ClearUnit(particleUnit);
                m_ActiveUnitArray.Remove(particleUnit);
                m_InActiveUnitArray.Add(particleUnit);
            }
            UpdateDirtyMesh(false);
            isAlive = false;
            return true;
        }

        public override bool Kill() {
            KillOnlyParticles();
            m_isActive = false;
            return true;
        }

        public override bool Shot() {
            m_isMatrixDirty = true;
            return SingleEmit();
        }

        private bool FirstSetting() {
            if (m_isInitialized) {
                return false;
            }
            m_isInitialized = true;
            Setup();
            return true;
        }

        private void Update() {
            if (!m_isInitialized) {
                FirstSetting();
            }
            FlushTime();
            if (rule != null) {
                if (!m_History.IsEqual(rule)) {
                    Setup();
                }
                if (m_isLocalTransHistory != rule.m_isLocalTrans) {
                    ChangeParent();
                    m_isLocalTransHistory = rule.m_isLocalTrans;
                }
            }
            m_isMatrixDirty = true;
            if (m_isActive) {
                SingleEmit();
            }

            if (!isAlive) { return; }

            if (m_BillboardBuffer != null && rule != null && m_BillboardBuffer.rotateType != rule.m_particleTypeParam.m_billboard.m_RotateType) {
                m_BillboardBuffer.rotateType = rule.m_particleTypeParam.m_billboard.m_RotateType;
            }
            if (m_UpdateHook_Before != null) {
                m_UpdateHook_Before();
            }
            if (m_Material != null && m_EffectRenderer != null && m_RenderObjectIsSelf) {
                if (m_Material != m_EffectRenderer.material) {
                    m_EffectRenderer.material = m_Material;
                }
                if (m_TailRenderer != null && m_Material != m_TailRenderer.material) {
                    m_TailRenderer.material = m_Material;
                }
            }
            if (m_isMatrixDirty) {
                m_OldMatrix = m_Matrix;
                if (m_LocatorTransforms[0] != null) {
                    m_Matrix = m_LocatorTransforms[0].localToWorldMatrix;
                } else {
                    m_Matrix = m_thisTransform.localToWorldMatrix;
                }
                m_isMatrixDirty = false;
            }
            for (int i = m_ActiveUnitArray.Count - 1; i >= 0; i--) {
                ParticleUnit particleUnit = m_ActiveUnitArray[i];
                if (!UpdateUnit(particleUnit)) {
                    m_ActiveUnitArray.Remove(particleUnit);
                    m_InActiveUnitArray.Add(particleUnit);
                }
            }
            if (m_UpdateHook_After != null) {
                m_UpdateHook_After();
            }
            if (m_ActiveUnitArray.Count == 0) {
                Kill();
            } else {
                UpdateDirtyMesh(true);
            }
        }

        public bool SingleEmit() {
            if (!m_isInitialized) {
                return false;
            }
            int emitNum = CalcEmitNumberBySingleEmit();
            if (emitNum <= 0) {
                return false;
            }
            if (m_UpdateHook_Shot != null && m_InActiveUnitArray.Count > 0) {
                m_UpdateHook_Shot();
            }
            int count = m_InActiveUnitArray.Count;
            bool flag = false;
            for (int i = 0; i < count; i++) {
                ParticleUnit particleUnit = m_InActiveUnitArray[0];
                emitNum--;
                if (emitNum < 0) { break; }
                if (ActivateParticleUnit(particleUnit)) {
                    m_InActiveUnitArray.Remove(particleUnit);
                    m_ActiveUnitArray.Add(particleUnit);
                    flag = true;
                }
            }
            if (flag) {
                m_isAlive = true;
            }
            return flag;
        }

        private int CalcEmitNumberBySingleEmit() {
            float emitFactor = m_Time * (m_Rule.m_incidentNumberPerSec + Helper.GetFRandom(0f, 1f) * m_Rule.m_incidentRandomLevel);
            if (emitFactor <= 0f) {
                return 0;
            }
            int emitNum = (int)emitFactor;
            float extra = Helper.GetFRandom(0f, 1f);
            if (extra < emitFactor - emitNum) {
                emitNum++;
            }
            return emitNum;
        }

        public static void CalcEmitionTransform(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            switch (type) {
                case eParticleEmitionType.eParticleEmitionType_Point:
                    CalcEmitionTransform_Point(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_Box:
                    CalcEmitionTransform_Box(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_PlaneQuad:
                    CalcEmitionTransform_PlaneQuad(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_PlaneCircle:
                    CalcEmitionTransform_PlaneCircle(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_Sphere:
                    CalcEmitionTransform_Sphere(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_Torus:
                    CalcEmitionTransform_Torus(type, param, ref pos, ref normal);
                    break;
                case eParticleEmitionType.eParticleEmitionType_Cylinder:
                    CalcEmitionTransform_Cylinder(type, param, ref pos, ref normal);
                    break;
            }
        }

        private static void CalcEmitionTransform_Point(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            Quaternion rotation = Quaternion.Euler(GetRandomDegree(), -90f, param.m_point.m_angleRange.Random());
            pos = Vector3.zero;
            normal = rotation * Vector3.right;
        }

        private static void CalcEmitionTransform_Box(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            Vector3 half = 0.5f * new Vector3(param.m_box.m_widthRange.ValueMax, param.m_box.m_heightRange.ValueMax, param.m_box.m_depthRange.ValueMax);
            Vector3 minHalf = -1f * half;
            float x = Helper.GetFRandom(minHalf.x, half.x);
            float y = Helper.GetFRandom(minHalf.y, half.y);
            float z = Helper.GetFRandom(minHalf.z, half.z);
            switch (Helper.GetIRandom() % 6) {
                case 1:
                    pos.x = x;
                    pos.y = y;
                    pos.z = param.m_box.m_depthRange.Random() * -0.5f;
                    normal = Vector3.back;
                    break;
                case 2:
                    pos.x = x;
                    pos.y = param.m_box.m_heightRange.Random() * 0.5f;
                    pos.z = z;
                    normal = Vector3.up;
                    break;
                case 3:
                    pos.x = x;
                    pos.y = param.m_box.m_heightRange.Random() * -0.5f;
                    pos.z = z;
                    normal = Vector3.down;
                    break;
                case 4:
                    pos.x = param.m_box.m_widthRange.Random() * 0.5f;
                    pos.y = y;
                    pos.z = z;
                    normal = Vector3.right;
                    break;
                case 5:
                    pos.x = param.m_box.m_widthRange.Random() * -0.5f;
                    pos.y = y;
                    pos.z = z;
                    normal = Vector3.left;
                    break;
                default:
                    pos.x = x;
                    pos.y = y;
                    pos.z = param.m_box.m_depthRange.Random() * 0.5f;
                    normal = Vector3.forward;
                    break;
            }
        }

        private static void CalcEmitionTransform_PlaneQuad(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            Vector2 half = 0.5f * new Vector2(param.m_planeQuad.m_widthRange.ValueMax, param.m_planeQuad.m_heightRange.ValueMax);
            Vector2 minHalf = half * -1f;
            float x = Helper.GetFRandom(minHalf.x, half.x);
            float y = Helper.GetFRandom(minHalf.y, half.y);
            if (Helper.GetIRandom() % 2 != 0) {
                pos.x = x;
                pos.y = param.m_planeQuad.m_heightRange.Random().value * 0.5f * (Helper.GetIRandom() % 2 * 2 - 1);
            } else {
                pos.x = param.m_planeQuad.m_widthRange.Random().value * 0.5f * (Helper.GetIRandom() % 2 * 2 - 1);
                pos.y = y;
            }
            pos.z = 0f;
            normal = Vector3.forward;
        }

        private static void CalcEmitionTransform_PlaneCircle(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            Quaternion rotation = Quaternion.Euler(0f, 0f, GetRandomDegree());
            pos = rotation * new Vector3(0f, param.m_planeCircle.m_radiusRange.Random(), 0f);
            normal = Vector3.forward;
        }

        private static void CalcEmitionTransform_Sphere(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            Quaternion rotation = Quaternion.Euler(GetRandomDegree(), -90f, param.m_sphere.m_angleRange.Random());
            float radius = Helper.GetFRandom(0f, 1f);
            radius = 1f - radius * radius;
            pos = rotation * new Vector3(param.m_sphere.m_radiusRange.Lerp(radius), 0f, 0f);
            normal = rotation * Vector3.right;
        }

        private static void CalcEmitionTransform_Torus(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            float bigRadiusRange = param.m_torus.m_bigRadiusRange;
            float smallRadius = param.m_torus.m_smallRadiusRange.Random();
            float angle = param.m_torus.m_angleRange.Random() * 2f * 0.017453292f;
            float randomRadian = GetRandomRadian();
            float sinA = Mathf.Sin(angle);
            float cosA = Mathf.Cos(angle);
            float sinR = Mathf.Sin(randomRadian);
            float cosR = Mathf.Cos(randomRadian);
            pos.x = bigRadiusRange * cosA + smallRadius * cosR * cosA;
            pos.y = bigRadiusRange * sinA + smallRadius * cosR * sinA;
            pos.z = smallRadius * sinR;
            normal = pos - new Vector3(bigRadiusRange * cosA, bigRadiusRange * sinA, 0f);
            if (Vector3.SqrMagnitude(normal) <= 0f) {
                normal = pos * -1f;
            } else {
                normal.Normalize();
            }
        }

        private static void CalcEmitionTransform_Cylinder(eParticleEmitionType type, ParticleRule.EmitionParam param, ref Vector3 pos, ref Vector3 normal) {
            float radius = Helper.GetFRandom(0f, 1f);
            radius = 1f - radius * radius;
            normal = Quaternion.Euler(0f, GetRandomDegree(), 0f) * Vector3.forward;
            pos = normal * param.m_cylinder.m_RadiusRange.Lerp(radius);
            pos.y = param.m_cylinder.m_HeightRange.Random();
        }

        private void MakePosAndNormal(ref Vector3 pos, ref Vector3 normal) {
            CalcEmitionTransform(rule.m_emitionType, rule.m_emitionParam, ref pos, ref normal);
            if (rule.m_isRandomEmitDir) {
                normal = Quaternion.Euler(GetRandomDegree(), GetRandomDegree(), 0f) * Vector3.forward;
            }
            if (m_isMatrixDirty) {
                m_OldMatrix = m_Matrix;
                if (m_LocatorTransforms[0] != null) {
                    m_Matrix = m_LocatorTransforms[0].localToWorldMatrix;
                } else {
                    m_Matrix = m_thisTransform.localToWorldMatrix;
                }
                m_isMatrixDirty = false;
            }
            if (!rule.m_isLocalTrans) {
                if (rule.m_particleType != eParticleType.eParticleType_Ribbon) {
                    if (isAlive) {
                        Vector3 old = m_OldMatrix.MultiplyPoint(pos);
                        pos = m_Matrix.MultiplyPoint(pos);
                        pos = Vector3.Lerp(old, pos, Helper.GetFRandom(0f, 1f));
                    } else {
                        pos = m_Matrix.MultiplyPoint(pos);
                    }
                } else if (isAlive) {
                    Vector3 old = m_OldMatrix.MultiplyVector(pos);
                    pos = m_Matrix.MultiplyVector(pos);
                    pos = Vector3.Lerp(old, pos, Helper.GetFRandom(0f, 1f));
                } else {
                    pos = m_Matrix.MultiplyVector(pos);
                }
                normal = m_Matrix.MultiplyVector(normal);
            }
        }

        private bool ActivateParticleUnit(ParticleUnit unit) {
            Vector3 pos = Vector4.zero;
            Vector3 normal = Vector4.zero;
            MakePosAndNormal(ref pos, ref normal);
            unit.m_particleType = rule.m_particleType;
            unit.m_birthPos = pos;
            unit.m_pos = pos;
            unit.m_offset = Vector4.zero;
            unit.m_velocity = normal;
            unit.m_scale = 1f;
            unit.m_lifeSpanType = rule.m_lifeSpanType;
            unit.m_lifeSpanRate = 0f;
            switch (rule.m_lifeSpanType) {
                case eParticleLifeSpanType.eParticleLifeSpanType_Time:
                    unit.m_lifeSpanParam[0] = rule.m_LifeSpanParam.m_Time.m_lifeSpanSecRange.Random();
                    break;
                case eParticleLifeSpanType.eParticleLifeSpanType_Distance:
                    unit.m_lifeSpanParam[0] = rule.m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange.Random();
                    break;
                case eParticleLifeSpanType.eParticleLifeSpanType_HeightRange:
                    unit.m_lifeSpanParam[0] = rule.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange.ValueMin;
                    unit.m_lifeSpanParam[1] = rule.m_LifeSpanParam.m_Height.m_lifeSpanHeightRange.ValueMax;
                    break;
            }
            switch (rule.m_lifeSpanAlpha) {
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_None:
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeOut:
                    unit.m_lifeSpanFadeAlphaRate = 1f;
                    break;
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeInOut:
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeIn:
                    unit.m_lifeSpanFadeAlphaRate = 0f;
                    break;
            }
            AttachFunc(unit);
            unit.m_velocity = normal * rule.m_speedRange.Random();
            unit.m_isLocal = rule.m_isLocalTrans;
            unit.m_initializedFlg = false;
            unit.m_activeFlg = true;
            unit.m_aliveCount = 0;
            return true;
        }

        private bool UpdateUnit(ParticleUnit unit) {
            m_CalcFuncList(unit);
            if (!unit.m_initializedFlg) {
                unit.m_initializedFlg = true;
            }
            unit.m_aliveCount++;
            return unit.m_activeFlg;
        }

        private void AttachFunc(ParticleUnit unit) {
            unit.m_lifeScaleMax = rule.m_lifeScaleRange.Random();
            unit.m_gravityForce = m_thisTransform.lossyScale.x * rule.m_gravityForceRange.Random() * rule.m_gravityDir;
            unit.m_gravityForce.x = -unit.m_gravityForce.x;
            switch (unit.m_particleType) {
                case eParticleType.eParticleType_Billboard: {
                    float factor = UnityEngine.Random.Range(0f, 1f);
                    unit.m_particleTypeParam.m_billboard.m_width = rule.m_particleTypeParam.m_billboard.m_widthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    unit.m_particleTypeParam.m_billboard.m_height = rule.m_particleTypeParam.m_billboard.m_heightRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    break;
                }
                case eParticleType.eParticleType_Point:
                    unit.m_particleTypeParam.m_point.m_size = rule.m_particleTypeParam.m_point.m_sizeRange.Random() * m_thisTransform.lossyScale.x;
                    break;
                case eParticleType.eParticleType_Line:
                    unit.m_particleTypeParam.m_line.m_width = rule.m_particleTypeParam.m_line.m_width * m_thisTransform.lossyScale.x;
                    for (int i = 0; i < rule.m_particleTypeParam.m_line.m_HistoryPointNum; i++) {
                        unit.m_particleTypeParam.m_line.m_pPos[i] = unit.m_pos;
                    }
                    break;
                case eParticleType.eParticleType_PolyLine: {
                    float factor = UnityEngine.Random.Range(0f, 1f);
                    unit.m_particleTypeParam.m_polyLine.m_topWidth = rule.m_particleTypeParam.m_polyLine.m_topWidthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    unit.m_particleTypeParam.m_polyLine.m_endWidth = rule.m_particleTypeParam.m_polyLine.m_endWidthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    for (int i = 0; i < rule.m_particleTypeParam.m_polyLine.m_HistoryPointNum; i++) {
                        unit.m_particleTypeParam.m_polyLine.m_pPos[i] = unit.m_pos;
                    }
                    break;
                }
                case eParticleType.eParticleType_Confetti: {
                    float factor = UnityEngine.Random.Range(0f, 1f);
                    unit.m_particleTypeParam.m_confetti.m_width = rule.m_particleTypeParam.m_confetti.m_widthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    unit.m_particleTypeParam.m_confetti.m_height = rule.m_particleTypeParam.m_confetti.m_heightRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    break;
                }
                case eParticleType.eParticleType_Ribbon: {
                    float factor = UnityEngine.Random.Range(0f, 1f);
                    unit.m_particleTypeParam.m_ribbon.m_topWidth = rule.m_particleTypeParam.m_ribbon.m_topWidthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    unit.m_particleTypeParam.m_ribbon.m_endWidth = rule.m_particleTypeParam.m_ribbon.m_endWidthRange.Lerp(factor) * m_thisTransform.lossyScale.x;
                    for (int i = 0; i < rule.m_particleTypeParam.m_ribbon.m_HistoryPointNum; i++) {
                        unit.m_particleTypeParam.m_ribbon.m_pPos[i] = unit.m_pos;
                        unit.m_particleTypeParam.m_ribbon.m_pVelocity[i] = Vector3.zero;
                    }
                    break;
                }
            }
            m_AttachFuncList(unit);
        }

        private void AttachFunc_Move_Acceleration(ParticleUnit unit) {
            unit.m_AccelerationComponent.m_acceleration = rule.m_AccelerationComponent.m_accelerationRange.Random() * m_thisTransform.lossyScale.x;
            unit.m_AccelerationComponent.m_dragForce = rule.m_AccelerationComponent.m_dragForceRange.Random();
        }

        private void AttachFunc_Move_Rotation(ParticleUnit unit) {
            unit.m_RotationComponent.m_rot.Set(rule.m_RotationComponent.m_rotRange.Random(), rule.m_RotationComponent.m_rotRange.Random(), rule.m_RotationComponent.m_rotRange.Random());
            unit.m_RotationComponent.m_rotAnchorOffset.Set(Helper.GetFRandom(0f, 1f) * rule.m_RotationComponent.m_rotAnchorOffsetRange.Random(), Helper.GetFRandom(0f, 1f) * rule.m_RotationComponent.m_rotAnchorOffsetRange.Random(), Helper.GetFRandom(0f, 1f) * rule.m_RotationComponent.m_rotAnchorOffsetRange.Random());
            unit.m_RotationComponent.m_rotAnchorOffset *= m_thisTransform.lossyScale.x;
            unit.m_RotationComponent.m_rotVelocity.Set(rule.m_RotationComponent.m_rotSpeedRange.Random(), rule.m_RotationComponent.m_rotSpeedRange.Random(), rule.m_RotationComponent.m_rotSpeedRange.Random());
            unit.m_RotationComponent.m_rotAcceleration.Set(rule.m_RotationComponent.m_rotAccelerationRange.Random(), rule.m_RotationComponent.m_rotAccelerationRange.Random(), rule.m_RotationComponent.m_rotAccelerationRange.Random());
            unit.m_RotationComponent.m_rotDragForce = rule.m_RotationComponent.m_rotDragForceRange.Random();
        }

        private void AttachFunc_Move_Tail(ParticleUnit unit) {
            for (int i = 0; i < rule.m_TailComponent.m_tailJointNum + 2; i++) {
                unit.m_TailComponent.m_pTailPos[i] = unit.m_pos;
            }
            unit.m_TailComponent.m_tailUVRect = rule.m_TailComponent.m_tailUVRect;
        }

        private void AttachFunc_Move_PathMove(ParticleUnit unit) {
            Vector4 vect = Vector4.zero;
            Matrix4x4 mat = Matrix4x4.identity;
            Quaternion quat;
            vect.Set(0f, 0f, rule.m_PathMoveComponent.m_FocusRadiusRange[0].Random(), 1f);
            quat = Quaternion.Euler(GetRandomDegree(), GetRandomDegree(), 0f);
            mat.SetTRS(Vector3.zero, quat, Vector3.one);
            vect = mat.MultiplyPoint(vect);
            unit.m_PathMoveComponent.m_offsetPos[0] = vect;
            vect.x *= m_thisTransform.lossyScale.x; //looks like this is in the wrong order
            vect.y *= m_thisTransform.lossyScale.y;
            vect.z *= m_thisTransform.lossyScale.z;
            vect.Set(0f, 0f, rule.m_PathMoveComponent.m_FocusRadiusRange[1].Random(), 1f);
            quat = Quaternion.Euler(GetRandomDegree(), GetRandomDegree(), 0f);
            mat.SetTRS(Vector3.zero, quat, Vector3.one);
            vect = mat.MultiplyPoint(vect);
            vect.x *= m_thisTransform.lossyScale.x;
            vect.y *= m_thisTransform.lossyScale.y;
            vect.z *= m_thisTransform.lossyScale.z;
            unit.m_PathMoveComponent.m_offsetPos[1] = vect;
            vect.Set(0f, 0f, rule.m_PathMoveComponent.m_FocusRadiusRange[2].Random(), 1f);
            quat = Quaternion.Euler(GetRandomDegree(), GetRandomDegree(), 0f);
            mat.SetTRS(Vector3.zero, quat, Vector3.one);
            vect = mat.MultiplyPoint(vect);
            vect.x *= m_thisTransform.lossyScale.x;
            vect.y *= m_thisTransform.lossyScale.y;
            vect.z *= m_thisTransform.lossyScale.z;
            unit.m_PathMoveComponent.m_offsetPos[2] = vect;
            vect.Set(0f, 0f, rule.m_PathMoveComponent.m_FocusRadiusRange[3].Random(), 1f);
            quat = Quaternion.Euler(GetRandomDegree(), GetRandomDegree(), 0f);
            mat.SetTRS(Vector3.zero, quat, Vector3.one);
            vect = mat.MultiplyPoint(vect);
            vect.x *= m_thisTransform.lossyScale.x;
            vect.y *= m_thisTransform.lossyScale.y;
            vect.z *= m_thisTransform.lossyScale.z;
            unit.m_PathMoveComponent.m_offsetPos[3] = vect;
        }

        private void AttachFunc_ColorAnim_ColorCurve(ParticleUnit unit) {
            if (rule.m_ColorCurveComponent.m_pColorCurveArray == null || rule.m_ColorCurveComponent.m_pColorCurveArray.Length == 0) {
                unit.m_ColorCurveComponent.m_pColorCurve = null;
            } else {
                unit.m_ColorCurveComponent.m_pColorCurve = rule.m_ColorCurveComponent.m_pColorCurveArray[Helper.GetIRandom() % rule.m_ColorCurveComponent.m_pColorCurveArray.Length];
            }
        }

        private void AttachFunc_ColorAnim_Blink(ParticleUnit unit) {
            unit.m_BlinkComponent.m_blinkRate = Helper.GetFRandom(0f, 1f);
            unit.m_BlinkComponent.m_blinkSpeed = rule.m_BlinkComponent.m_blinkSpanSecRange.Random();
        }

        private void AttachFunc_UVAnim(ParticleUnit unit) {
            int steps = rule.m_uvBlockNum != 0 ? Helper.GetIRandom() % rule.m_uvBlockNum : 0;
            unit.m_uvRect = rule.m_uvRect_TopBlock;

            if (steps == 0) { return; }

            while (true) {
                if (unit.m_uvRect.x + unit.m_uvRect.width > 1f) {
                    unit.m_uvRect.x = 0f;
                    unit.m_uvRect.y -= unit.m_uvRect.height;
                    if (unit.m_uvRect.y - unit.m_uvRect.height < 0f) {
                        unit.m_uvRect.y = rule.m_uvRect_TopBlock.y;
                    }
                }
                if (steps == 0) { break; }
                unit.m_uvRect.x += unit.m_uvRect.width;
                steps--;
            }
        }

        private void AttachFunc_UVAnim_UVAnimation(ParticleUnit unit) {
            unit.m_UVAnimationComponent.m_nowBlock = rule.m_uvBlockNum != 0 && rule.m_UVAnimationComponent.m_randomStartBlockFlg ? Helper.GetIRandom() % rule.m_uvBlockNum : 0;
            unit.m_UVAnimationComponent.m_switchSec = rule.m_UVAnimationComponent.m_switchBlockSecRange.Random();
            unit.m_UVAnimationComponent.m_switchSecWork = unit.m_UVAnimationComponent.m_switchSec;
            unit.m_uvRect = rule.m_uvRect_TopBlock;

            if (unit.m_UVAnimationComponent.m_nowBlock == 0) { return; }

            int steps = unit.m_UVAnimationComponent.m_nowBlock;
            while (true) {
                if (unit.m_uvRect.x + unit.m_uvRect.width > 1f) {
                    unit.m_uvRect.x = 0f;
                    unit.m_uvRect.y -= unit.m_uvRect.height;
                    if (unit.m_uvRect.y - unit.m_uvRect.height < 0f) {
                        unit.m_uvRect.y = rule.m_uvRect_TopBlock.y;
                    }
                }
                if (steps == 0) { break; }
                unit.m_uvRect.x += unit.m_uvRect.width;
                steps--;
            }
        }

        private void CalcFunc_Move_Acceleration(ParticleUnit unit) {
            Vector3 velocity = unit.m_velocity;
            if (velocity.sqrMagnitude > 0f) {
                velocity.Normalize();
                unit.m_velocity += unit.m_AccelerationComponent.m_acceleration * time * velocity;
                unit.m_velocity *= Mathf.Pow(unit.m_AccelerationComponent.m_dragForce, framerate);
            }
        }

        private void CalcFunc_Move_Rotation(ParticleUnit unit) {
            Vector3 rotVelocity = unit.m_RotationComponent.m_rotVelocity;
            if (rotVelocity.sqrMagnitude > 0f) {
                float force = Mathf.Min(1f, unit.m_RotationComponent.m_rotDragForce);
                rotVelocity.Normalize();
                Vector3 addVelocity = unit.m_RotationComponent.m_rotAcceleration * time;
                addVelocity.Scale(rotVelocity);
                unit.m_RotationComponent.m_rot += unit.m_RotationComponent.m_rotVelocity * time;
                unit.m_RotationComponent.m_rotVelocity += addVelocity;
                unit.m_RotationComponent.m_rotVelocity *= Mathf.Pow(force, framerate);
            }
        }

        private void CalcFunc_Move_PathMove(ParticleUnit unit) {
            float rate;
            if (unit.m_lifeSpanRate < 0.33f) {
                rate = unit.m_lifeSpanRate;
                m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[0];
                m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[0];
                m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[1];
                m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[2];
            } else if (unit.m_lifeSpanRate < 0.66f) {
                rate = unit.m_lifeSpanRate - 0.33f;
                m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[0];
                m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[1];
                m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[2];
                m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[3];
            } else {
                rate = unit.m_lifeSpanRate - 0.66f;
                m_WorkPaths[0] = unit.m_PathMoveComponent.m_offsetPos[1];
                m_WorkPaths[1] = unit.m_PathMoveComponent.m_offsetPos[2];
                m_WorkPaths[2] = unit.m_PathMoveComponent.m_offsetPos[3];
                m_WorkPaths[3] = unit.m_PathMoveComponent.m_offsetPos[3];
            }
            MathFunc.CatmullRom(out unit.m_offset, m_WorkPaths, Mathf.Clamp01(rate / 0.33f));
        }

        private void CalcFunc_Move(ParticleUnit unit) {
            float factor;
            switch (rule.m_LifeScaleType) {
                case eParticleLifeScaleType.eParticleLifeScaleType_CurveAcceleration:
                    factor = unit.m_lifeSpanRate * unit.m_lifeSpanRate;
                    break;
                case eParticleLifeScaleType.eParticleLifeScaleType_CurveSlowdonw:
                    factor = 1f - unit.m_lifeSpanRate;
                    factor = 1f - factor * factor;
                    break;
                default:
                    factor = unit.m_lifeSpanRate;
                    break;
            }
            unit.m_scale = Mathf.Lerp(1f, unit.m_lifeScaleMax, factor);
            unit.m_velocity += unit.m_gravityForce * time;
        }

        private void CalcFunc_ColorAnim_NotColorCurve(ParticleUnit unit) {
            unit.m_color = Color.white;
            unit.m_color.a = unit.m_color.a * unit.m_lifeSpanFadeAlphaRate * rule.m_AlphaScale;
        }

        private void CalcFunc_ColorAnim_ColorCurve(ParticleUnit unit) {
            if (unit.m_ColorCurveComponent.m_pColorCurve != null) {
                unit.m_color = unit.m_ColorCurveComponent.m_pColorCurve.Lerp(unit.m_lifeSpanRate);
            } else {
                unit.m_color = Color.white;
            }
            unit.m_color.a = unit.m_color.a * unit.m_lifeSpanFadeAlphaRate * rule.m_AlphaScale;
        }

        private void CalcFunc_ColorAnim_Blink(ParticleUnit unit) {
            unit.m_BlinkComponent.m_blinkRate = Mathf.Repeat(unit.m_BlinkComponent.m_blinkRate + time / unit.m_BlinkComponent.m_blinkSpeed, 2f);
            float a = unit.m_BlinkComponent.m_blinkRate <= 1f ? unit.m_BlinkComponent.m_blinkRate : 2f - unit.m_BlinkComponent.m_blinkRate;
            unit.m_color.a *= 1f - a * a * a * a;
        }

        private void CalcFunc_UVAnim_UVAnimation(ParticleUnit unit) {
            unit.m_UVAnimationComponent.m_switchSecWork -= time;
            if (unit.m_UVAnimationComponent.m_switchSecWork <= 0f) {
                unit.m_UVAnimationComponent.m_switchSecWork = unit.m_UVAnimationComponent.m_switchSec;
                unit.m_UVAnimationComponent.m_nowBlock = rule.m_uvBlockNum != 0 ? (unit.m_UVAnimationComponent.m_nowBlock + 1) % rule.m_uvBlockNum : 0;
                unit.m_uvRect.x += unit.m_uvRect.width;
                if (unit.m_uvRect.x + unit.m_uvRect.width > 1f) {
                    unit.m_uvRect.x = 0f;
                    unit.m_uvRect.y -= unit.m_uvRect.height;
                    if (unit.m_uvRect.y - unit.m_uvRect.height < 0f) {
                        unit.m_uvRect.y = rule.m_uvRect_TopBlock.y;
                    }
                }
            }
        }

        private void CalcFunc_LifeSpan(ParticleUnit unit) {
            if (!unit.m_initializedFlg) { return; }

            float lifeRate = unit.m_lifeSpanRate;
            switch (rule.m_lifeSpanType) {
                case eParticleLifeSpanType.eParticleLifeSpanType_Time:
                    lifeRate = Mathf.Clamp(lifeRate + time / unit.m_lifeSpanParam[0], 0f, 1f);
                    break;
                case eParticleLifeSpanType.eParticleLifeSpanType_Distance:
                    lifeRate = Mathf.Clamp((unit.m_pos - unit.m_birthPos).magnitude / unit.m_lifeSpanParam[0], 0f, 1f);
                    break;
                case eParticleLifeSpanType.eParticleLifeSpanType_HeightRange:
                    float diff;
                    float offset;
                    if (unit.m_pos.y >= unit.m_birthPos.y) {
                        diff = unit.m_pos.y - unit.m_birthPos.y;
                        offset = unit.m_lifeSpanParam[1] - unit.m_birthPos.y;
                    } else {
                        diff = unit.m_birthPos.y - unit.m_pos.y;
                        offset = unit.m_birthPos.y - unit.m_lifeSpanParam[0];
                    }
                    lifeRate = offset == 0f ? 1f : Mathf.Clamp(diff / offset, 0f, 1f);
                    break;
                case eParticleLifeSpanType.eParticleLifeSpanType_AnimFrame: {
                    lifeRate = Mathf.Clamp(lifeRate + unit.m_lifeSpanParam[0] * framerate, 0f, 1f);
                    break;
                }
            }
            unit.m_lifeSpanRate = lifeRate;

            float fadeRate = unit.m_lifeSpanFadeAlphaRate;
            switch (rule.m_lifeSpanAlpha) {
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_None:
                    fadeRate = 1f;
                    break;
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeInOut:
                    if (lifeRate < 0.2f) {
                        fadeRate = 1f - lifeRate / 0.2f;
                        fadeRate = 1f - fadeRate * fadeRate;
                    } else if (lifeRate >= 0.8f) {
                        fadeRate = (lifeRate - 0.8f) / 0.2f;
                        fadeRate = 1f - fadeRate * fadeRate;
                    } else {
                        fadeRate = 1f;
                    }
                    break;
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeIn:
                    if (lifeRate < 0.2f) {
                        fadeRate = 1f - lifeRate / 0.2f;
                        fadeRate = 1f - fadeRate * fadeRate;
                    } else {
                        fadeRate = 1f;
                    }
                    break;
                case eParticleLifeSpanAlpha.eParticleLifeSpanAlpha_FadeOut:
                    if (lifeRate >= 0.8f) {
                        fadeRate = (lifeRate - 0.8f) / 0.2f;
                        fadeRate = 1f - fadeRate * fadeRate;
                    }
                    break;
            }
            unit.m_lifeSpanFadeAlphaRate = fadeRate;

            if (unit.m_lifeSpanRate >= 1f) {
                unit.m_activeFlg = false;
            }
        }

        private void CalcFuncMerge(ParticleUnit unit) {
            float originalY = unit.m_pos.y;
            if (rule.m_particleType != eParticleType.eParticleType_Ribbon) {
                unit.m_pos += unit.m_velocity * time;
            }

            if (rule.m_collisionType != eParticleCollisionType.eParticleCollisionType_Height) { return; }

            float height = rule.m_collisionParam.m_height.m_height;
            float add;
            if (rule.m_collisionParam.m_height.m_isLocalHeight) { //why are they both the same
                if (originalY >= height && unit.m_pos.y < height) {
                    add = Mathf.Max(height - unit.m_pos.y, 0.001f);
                } else if (originalY <= height && unit.m_pos.y > height) {
                    add = Mathf.Min(height - unit.m_pos.y, -0.001f);
                } else {
                    return;
                }
            } else {
                if (originalY >= height && unit.m_pos.y < height) {
                    add = Mathf.Max(height - unit.m_pos.y, 0.001f);
                } else if (originalY <= height && unit.m_pos.y > height) {
                    add = Mathf.Min(height - unit.m_pos.y, -0.001f);
                } else {
                    return;
                }
            }
            unit.m_pos.y = height + add;
            unit.m_velocity.x *= rule.m_collisionParam.m_height.m_frictionCoefficient;
            unit.m_velocity.y = -unit.m_velocity.y * rule.m_collisionParam.m_height.m_reflectionCoefficient;
            unit.m_velocity.z *= rule.m_collisionParam.m_height.m_frictionCoefficient;
        }

        private void CalcFuncMerge_NotRotation(ParticleUnit unit) {
            int index = unit.m_Index;
            switch (unit.m_particleType) {
                case eParticleType.eParticleType_Billboard:
                    if (!unit.m_activeFlg) {
                        m_BillboardBuffer.positions[index] = Vector3.zero;
                        m_BillboardBuffer.rots[index] = 0f;
                        m_BillboardBuffer.sizes[index].Set(0f, 0f);
                        m_BillboardBuffer.colors[index] = Color.clear;
                        return;
                    }
                    m_BillboardBuffer.positions[index] = unit.m_pos + unit.m_offset;
                    m_BillboardBuffer.sizes[index].Set(unit.m_particleTypeParam.m_billboard.m_width * unit.m_scale, unit.m_particleTypeParam.m_billboard.m_height * unit.m_scale);
                    m_BillboardBuffer.rots[index] = 0f;
                    m_BillboardBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
                    m_BillboardBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_BillboardBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_BillboardBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_BillboardBuffer.positions[index];
                        unit.m_TailComponent.m_width = m_BillboardBuffer.sizes[index].x;
                    }
                    break;
                case eParticleType.eParticleType_Point:
                    if (!unit.m_activeFlg) {
                        m_PointBuffer.positions[index] = Vector3.zero;
                        m_PointBuffer.sizes[index] = 0f;
                        m_PointBuffer.colors[index] = Color.clear;
                        return;
                    }
                    m_PointBuffer.positions[index] = unit.m_pos + unit.m_offset;
                    m_PointBuffer.sizes[index] = unit.m_particleTypeParam.m_point.m_size;
                    m_PointBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_PointBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_PointBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_PointBuffer.positions[index];
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_point.m_size;
                    }
                    break;
                case eParticleType.eParticleType_Line: {
                    PrimLineBuffer.LineBuffer.Line line = m_LineBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_line.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_line.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int i = 0; i < linePoints; i++) {
                            line.SetPosition(Vector3.zero, 0);
                            line.SetWidth(0f, i);
                            line.SetColor(Color.clear, i);
                        }
                        return;
                    }
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = unit.m_pos + unit.m_offset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_line.m_pPos[i] = pos;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_line.m_pPos[i] = unit.m_particleTypeParam.m_line.m_pPos[i - 1];
                        }
                    }
                    unit.m_particleTypeParam.m_line.m_pPos[0] = unit.m_pos + unit.m_offset;
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_line.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x, unit.m_uvRect.y + unit.m_uvRect.height * i / (linePoints - 1));
                            line.SetUV(uv, i);
                            line.SetWidth(1f, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_line.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 pos, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x, unit.m_uvRect.y + unit.m_uvRect.height * i / (linePoints - 1));
                            line.SetUV(uv, i);
                            line.SetWidth(1f, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_line.m_width;
                    }
                    break;
                }
                case eParticleType.eParticleType_PolyLine: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_PolylineBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_polyLine.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int i = 0; i < linePoints; i++) {
                            line.SetPosition(Vector3.zero, i);
                            line.SetWidth(0f, i);
                            line.SetColor(Color.clear, i);
                        }
                        return;
                    }
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = unit.m_pos + unit.m_offset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_polyLine.m_pPos[i] = pos;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_polyLine.m_pPos[i] = unit.m_particleTypeParam.m_polyLine.m_pPos[i - 1];
                        }
                    }
                    unit.m_particleTypeParam.m_polyLine.m_pPos[0] = unit.m_pos + unit.m_offset;
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_polyLine.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.PolyLine polyLine = unit.m_particleTypeParam.m_polyLine;
                            line.SetWidth(Mathf.Lerp(polyLine.m_topWidth, polyLine.m_endWidth, i / (float)(polyLine.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_polyLine.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 vector4, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                vector4 = m_Matrix.MultiplyPoint3x4(vector4);
                            }
                            line.SetPosition(vector4, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.PolyLine polyLine = unit.m_particleTypeParam.m_polyLine;
                            line.SetWidth(Mathf.Lerp(polyLine.m_topWidth, polyLine.m_endWidth, i / (float)(polyLine.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_polyLine.m_topWidth * unit.m_scale;
                    }
                    break;
                }
                case eParticleType.eParticleType_Confetti:
                    if (!unit.m_activeFlg) {
                        m_ConfettiBuffer.positions[index] = Vector3.zero;
                        m_ConfettiBuffer.sizes[index].Set(0f, 0f);
                        m_ConfettiBuffer.colors[index] = Color.clear;
                        return;
                    }
                    m_ConfettiBuffer.positions[index] = unit.m_pos + unit.m_offset;
                    m_ConfettiBuffer.sizes[index].Set(unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale, unit.m_particleTypeParam.m_confetti.m_height * unit.m_scale);
                    m_ConfettiBuffer.rots[index].Set(0f, 0f, 0f);
                    m_ConfettiBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
                    m_ConfettiBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_ConfettiBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_ConfettiBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_ConfettiBuffer.positions[index];
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale;
                    }
                    break;
                case eParticleType.eParticleType_Ribbon: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_RibbonBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_ribbon.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int i = 0; i < linePoints; i++) {
                            line.SetPosition(Vector3.zero, i);
                            line.SetWidth(0f, i);
                            line.SetColor(Color.clear, i);
                        }
                        return;
                    }
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = m_Matrix.GetColumn(3);
                        pos += unit.m_offset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = pos;
                            unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = Vector3.zero;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = unit.m_particleTypeParam.m_ribbon.m_pPos[i - 1];
                            unit.m_particleTypeParam.m_ribbon.m_pVelocity[i] = unit.m_particleTypeParam.m_ribbon.m_pVelocity[i - 1];
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = unit.m_particleTypeParam.m_ribbon.m_pPos[i] + unit.m_particleTypeParam.m_ribbon.m_pVelocity[i] * time;
                        }
                    }
                    if (m_isActive) {
                        unit.m_particleTypeParam.m_ribbon.m_pPos[0] = m_Matrix.GetColumn(3);
                        unit.m_particleTypeParam.m_ribbon.m_pPos[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[0] + unit.m_offset;
                        unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = unit.m_velocity;
                    }
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_ribbon.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.Ribbon ribbon = unit.m_particleTypeParam.m_ribbon;
                            line.SetWidth(Mathf.Lerp(ribbon.m_topWidth, ribbon.m_endWidth, i / (float)(ribbon.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_ribbon.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 pos, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.Ribbon ribbon = unit.m_particleTypeParam.m_ribbon;
                            line.SetWidth(Mathf.Lerp(ribbon.m_topWidth, ribbon.m_endWidth, i / (float)(ribbon.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_ribbon.m_topWidth * unit.m_scale;
                    }
                    break;
                }
            }
        }

        private void CalcFuncMerge_Rotation(ParticleUnit unit) {
            int index = unit.m_Index;
            switch (unit.m_particleType) {
                case eParticleType.eParticleType_Billboard: {
                    if (!unit.m_activeFlg) {
                        m_BillboardBuffer.positions[index] = Vector3.zero;
                        m_BillboardBuffer.rots[index] = 0f;
                        m_BillboardBuffer.sizes[index].Set(0f, 0f);
                        m_BillboardBuffer.colors[index] = Color.clear;
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    m_BillboardBuffer.positions[index] = unit.m_pos + unit.m_offset + rotatedOffset;
                    m_BillboardBuffer.sizes[index].Set(unit.m_particleTypeParam.m_billboard.m_width * unit.m_scale, unit.m_particleTypeParam.m_billboard.m_height * unit.m_scale);
                    m_BillboardBuffer.rots[index] = unit.m_RotationComponent.m_rot.z;
                    m_BillboardBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
                    m_BillboardBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_BillboardBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_BillboardBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_BillboardBuffer.positions[index];
                        unit.m_TailComponent.m_width = m_BillboardBuffer.sizes[index].x;
                    }
                    break;
                }
                case eParticleType.eParticleType_Point: {
                    if (!unit.m_activeFlg) {
                        m_PointBuffer.positions[index] = Vector3.zero;
                        m_PointBuffer.sizes[index] = 0f;
                        m_PointBuffer.colors[index] = Color.clear;
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    m_PointBuffer.positions[index] = unit.m_pos + unit.m_offset + rotatedOffset;
                    m_PointBuffer.sizes[index] = unit.m_particleTypeParam.m_point.m_size;
                    m_PointBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_PointBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_PointBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_PointBuffer.positions[index];
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_point.m_size;
                    }
                    break;
                }
                case eParticleType.eParticleType_Line: {
                    PrimLineBuffer.LineBuffer.Line line = m_LineBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_line.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_line.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int i = 0; i < linePoints; i++) {
                            line.SetPosition(Vector3.zero, i);
                            line.SetWidth(0f, i);
                            line.SetColor(Color.clear, i);
                        }
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = unit.m_pos + unit.m_offset + rotatedOffset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_line.m_pPos[i] = pos;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_line.m_pPos[i] = unit.m_particleTypeParam.m_line.m_pPos[i - 1];
                        }
                    }
                    unit.m_particleTypeParam.m_line.m_pPos[0] = unit.m_pos + unit.m_offset + rotatedOffset;
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_line.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos); //why?
                            }
                            line.SetPosition(unit.m_particleTypeParam.m_line.m_pPos[i], i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x, unit.m_uvRect.y + unit.m_uvRect.height * i / (linePoints - 1));
                            line.SetUV(uv, i);
                            line.SetWidth(1f, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_line.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_line.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 pos, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos); //why?
                            }
                            line.SetPosition(unit.m_particleTypeParam.m_line.m_pPos[i], i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x, unit.m_uvRect.y + unit.m_uvRect.height * i / (linePoints - 1));
                            line.SetUV(uv, i);
                            line.SetWidth(1f, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_line.m_width;
                    }
                    break;
                }
                case eParticleType.eParticleType_PolyLine: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_PolylineBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_polyLine.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_polyLine.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int n = 0; n < linePoints; n++) {
                            line.SetPosition(Vector3.zero, n);
                            line.SetWidth(0f, n);
                            line.SetColor(Color.clear, n);
                        }
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = unit.m_pos + unit.m_offset + rotatedOffset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_polyLine.m_pPos[i] = pos;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_polyLine.m_pPos[i] = unit.m_particleTypeParam.m_polyLine.m_pPos[i - 1];
                        }
                    }
                    unit.m_particleTypeParam.m_polyLine.m_pPos[0] = unit.m_pos + unit.m_offset + rotatedOffset;
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_polyLine.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.PolyLine polyLine = unit.m_particleTypeParam.m_polyLine;
                            line.SetWidth(Mathf.Lerp(polyLine.m_topWidth, polyLine.m_endWidth, i / (float)(polyLine.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_polyLine.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_polyLine.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 vector4, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                vector4 = m_Matrix.MultiplyPoint3x4(vector4);
                            }
                            line.SetPosition(vector4, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.PolyLine polyLine = unit.m_particleTypeParam.m_polyLine;
                            line.SetWidth(Mathf.Lerp(polyLine.m_topWidth, polyLine.m_endWidth, i / (float)(polyLine.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_polyLine.m_topWidth * unit.m_scale;
                    }
                    break;
                }
                case eParticleType.eParticleType_Confetti: {
                    if (!unit.m_activeFlg) {
                        m_ConfettiBuffer.rots[index] = Vector3.zero;
                        m_ConfettiBuffer.sizes[index].Set(0f, 0f);
                        m_ConfettiBuffer.colors[index] = Color.clear;
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    m_ConfettiBuffer.positions[index] = unit.m_pos + unit.m_offset + rotatedOffset;
                    m_ConfettiBuffer.sizes[index].Set(unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale, unit.m_particleTypeParam.m_confetti.m_height * unit.m_scale);
                    m_ConfettiBuffer.rots[index] = unit.m_RotationComponent.m_rot;
                    m_ConfettiBuffer.UVs[index].Set(unit.m_uvRect.x, unit.m_uvRect.y, unit.m_uvRect.width, unit.m_uvRect.height);
                    m_ConfettiBuffer.colors[index] = unit.m_color;
                    if (unit.m_isLocal) {
                        m_ConfettiBuffer.positions[index] = m_Matrix.MultiplyPoint3x4(m_ConfettiBuffer.positions[index]);
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = m_ConfettiBuffer.positions[index];
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_confetti.m_width * unit.m_scale;
                    }
                    break;
                }
                case eParticleType.eParticleType_Ribbon: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_RibbonBuffer.GetLine(index);
                    int linePoints = unit.m_particleTypeParam.m_ribbon.m_jointNum + 2;
                    int historyPoints = unit.m_particleTypeParam.m_ribbon.m_HistoryPointNum;
                    if (!unit.m_activeFlg) {
                        for (int i = 0; i < linePoints; i++) {
                            line.SetPosition(Vector3.zero, i);
                            line.SetWidth(0f, i);
                            line.SetColor(Color.clear, i);
                        }
                        return;
                    }
                    Vector3 rotatedOffset = Quaternion.Euler(unit.m_RotationComponent.m_rot) * unit.m_RotationComponent.m_rotAnchorOffset;
                    if (!unit.m_initializedFlg) {
                        Vector3 pos = m_Matrix.GetColumn(3);
                        pos += unit.m_offset + rotatedOffset;
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = pos;
                            unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = Vector3.zero;
                        }
                    } else {
                        for (int i = historyPoints - 1; i >= 1; i--) {
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = unit.m_particleTypeParam.m_ribbon.m_pPos[i - 1];
                            unit.m_particleTypeParam.m_ribbon.m_pVelocity[i] = unit.m_particleTypeParam.m_ribbon.m_pVelocity[i - 1];
                            unit.m_particleTypeParam.m_ribbon.m_pPos[i] = unit.m_particleTypeParam.m_ribbon.m_pPos[i] + unit.m_particleTypeParam.m_ribbon.m_pVelocity[i] * time;
                        }
                    }
                    if (m_isActive) {
                        unit.m_particleTypeParam.m_ribbon.m_pPos[0] = m_Matrix.GetColumn(3);
                        unit.m_particleTypeParam.m_ribbon.m_pPos[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[0] + unit.m_offset + rotatedOffset;
                        unit.m_particleTypeParam.m_ribbon.m_pVelocity[0] = unit.m_velocity;
                    }
                    if (historyPoints == linePoints) {
                        for (int i = 0; i < linePoints; i++) {
                            Vector3 pos = unit.m_particleTypeParam.m_ribbon.m_pPos[i];
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.Ribbon ribbon = unit.m_particleTypeParam.m_ribbon;
                            line.SetWidth(Mathf.Lerp(ribbon.m_topWidth, ribbon.m_endWidth, i / (float)(ribbon.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i + 2) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    } else {
                        float points = Mathf.Max(linePoints / (float)historyPoints, 1f);
                        for (int i = 0; i < linePoints; i++) {
                            float pointFraction = i / (float)(linePoints - 1) * (historyPoints - 1);
                            int point = (int)pointFraction;
                            m_WorkPaths[0] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Max(point - 1, 0)];
                            m_WorkPaths[1] = unit.m_particleTypeParam.m_ribbon.m_pPos[point];
                            m_WorkPaths[2] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Min(point + 1, historyPoints - 1)];
                            m_WorkPaths[3] = unit.m_particleTypeParam.m_ribbon.m_pPos[Mathf.Min(point + 2, historyPoints - 1)];
                            MathFunc.CatmullRom(out Vector3 pos, m_WorkPaths, pointFraction - point);
                            if (unit.m_isLocal) {
                                pos = m_Matrix.MultiplyPoint3x4(pos);
                            }
                            line.SetPosition(pos, i);
                            Vector2 uv = new Vector2(unit.m_uvRect.x + unit.m_uvRect.width * i / (linePoints - 1), unit.m_uvRect.y);
                            line.SetUV(uv, unit.m_uvRect.height, i);
                            ParticleUnit.ParticleTypeParam.Ribbon ribbon = unit.m_particleTypeParam.m_ribbon;
                            line.SetWidth(Mathf.Lerp(ribbon.m_topWidth, ribbon.m_endWidth, i / (float)(ribbon.m_jointNum + 1)) * unit.m_scale, i);
                            if (unit.m_aliveCount > i / points + 2f) {
                                line.SetColor(unit.m_color, i);
                            }
                        }
                    }
                    if (rule.m_UsingTailFlg) {
                        unit.m_TailComponent.m_TopPos = line.GetPosition(0);
                        unit.m_TailComponent.m_width = unit.m_particleTypeParam.m_ribbon.m_topWidth * unit.m_scale;
                    }
                    break;
                }
            }
        }

        private void CalcFuncMerge_Tail(ParticleUnit unit) {
            PrimPolyLineBuffer.PolyLineBuffer.Line line = m_TailBuffer.GetLine(unit.m_Index);
            int tailPoints = rule.m_TailComponent.m_tailJointNum + 2;
            if (!unit.m_activeFlg) {
                for (int i = 0; i < tailPoints; i++) {
                    line.SetWidth(0f, i);
                    line.SetColor(Color.clear, i);
                }
                return;
            }
            if (!unit.m_initializedFlg) {
                for (int i = tailPoints - 1; i >= 1; i--) {
                    unit.m_TailComponent.m_pTailPos[i] = unit.m_TailComponent.m_TopPos;
                }
            } else {
                for (int i = tailPoints - 1; i >= 1; i--) {
                    unit.m_TailComponent.m_pTailPos[i] = unit.m_TailComponent.m_pTailPos[i - 1];
                }
            }
            unit.m_TailComponent.m_pTailPos[0] = unit.m_TailComponent.m_TopPos;
            for (int i = 0; i < tailPoints; i++) {
                line.SetPosition(unit.m_TailComponent.m_pTailPos[i], i);
                Rect uvRect = unit.m_TailComponent.m_tailUVRect;
                Vector2 uv = new Vector2(uvRect.x, uvRect.y + uvRect.height * i / (tailPoints - 1));
                line.SetUV(uv, uvRect.width, i);
                line.SetWidth(unit.m_TailComponent.m_width * 0.75f, i);
                if (unit.m_aliveCount > i + 2) {
                    line.SetColor(unit.m_color, i);
                }
            }
        }

        private void UpdateDirtyMesh(bool active) {
            if (m_BillboardBuffer != null) {
                m_BillboardBuffer.UpdatePositions();
                m_BillboardBuffer.UpdateSizes();
                m_BillboardBuffer.UpdateRots();
                m_BillboardBuffer.UpdateUVs();
                m_BillboardBuffer.UpdateColors();
                m_BillboardBuffer.enabled = active;
            }
            if (m_PointBuffer != null) {
                m_PointBuffer.UpdatePositions();
                m_PointBuffer.UpdateSizes();
                m_PointBuffer.UpdateUVs();
                m_PointBuffer.UpdateColors();
                m_PointBuffer.enabled = active;
            }
            if (m_LineBuffer != null) {
                m_LineBuffer.UpdatePositions();
                m_LineBuffer.UpdateWidths();
                m_LineBuffer.UpdateUVs();
                m_LineBuffer.UpdateColors();
                m_LineBuffer.enabled = active;
            }
            if (m_PolylineBuffer != null) {
                m_PolylineBuffer.UpdatePositions();
                m_PolylineBuffer.UpdateWidths();
                m_PolylineBuffer.UpdateUVs();
                m_PolylineBuffer.UpdateColors();
                m_PolylineBuffer.enabled = active;
            }
            if (m_ConfettiBuffer != null) {
                m_ConfettiBuffer.UpdatePositions();
                m_ConfettiBuffer.UpdateSizes();
                m_ConfettiBuffer.UpdateRots();
                m_ConfettiBuffer.UpdateUVs();
                m_ConfettiBuffer.UpdateColors();
                m_ConfettiBuffer.enabled = active;
            }
            if (m_RibbonBuffer != null) {
                m_RibbonBuffer.UpdatePositions();
                m_RibbonBuffer.UpdateWidths();
                m_RibbonBuffer.UpdateUVs();
                m_RibbonBuffer.UpdateColors();
                m_RibbonBuffer.enabled = active;
            }
            if (m_TailBuffer != null) {
                m_TailBuffer.UpdatePositions();
                m_TailBuffer.UpdateWidths();
                m_TailBuffer.UpdateUVs();
                m_TailBuffer.UpdateColors();
                m_TailBuffer.enabled = active;
            }
            if ((m_RenderSettingsFixed == null || m_RenderSettingsFixed.m_EffectRenderer == null) && m_EffectRenderer != null) {
                m_EffectRenderer.enabled = active;
            }
            if (m_TailRenderer != null) {
                m_TailRenderer.enabled = active;
            }
        }

        private void ClearUnit(ParticleUnit unit) {
            int index = unit.m_Index;
            switch (unit.m_particleType) {
                case eParticleType.eParticleType_Billboard:
                    m_BillboardBuffer.positions[index] = Vector3.zero;
                    m_BillboardBuffer.rots[index] = 0f;
                    m_BillboardBuffer.sizes[index].Set(0f, 0f);
                    m_BillboardBuffer.colors[index] = Color.clear;
                    break;
                case eParticleType.eParticleType_Point:
                    m_PointBuffer.positions[index] = Vector3.zero;
                    m_PointBuffer.sizes[index] = 0f;
                    m_PointBuffer.colors[index] = Color.clear;
                    break;
                case eParticleType.eParticleType_Line: {
                    PrimLineBuffer.LineBuffer.Line line = m_LineBuffer.GetLine(index);
                    for (int i = 0; i < (unit.m_particleTypeParam.m_line.m_jointNum + 2); i++) {
                        line.SetPosition(Vector3.zero, i);
                        line.SetWidth(0f, i);
                        line.SetColor(Color.clear, i);
                    }
                    break;
                }
                case eParticleType.eParticleType_PolyLine: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_PolylineBuffer.GetLine(index);
                    for (int i = 0; i < (unit.m_particleTypeParam.m_polyLine.m_jointNum + 2); i++) {
                        line.SetPosition(Vector3.zero, i);
                        line.SetWidth(0f, i);
                        line.SetColor(Color.clear, i);
                    }
                    break;
                }
                case eParticleType.eParticleType_Confetti:
                    m_ConfettiBuffer.rots[index] = Vector3.zero;
                    m_ConfettiBuffer.sizes[index].Set(0f, 0f);
                    m_ConfettiBuffer.colors[index] = Color.clear;
                    break;
                case eParticleType.eParticleType_Ribbon: {
                    PrimPolyLineBuffer.PolyLineBuffer.Line line = m_RibbonBuffer.GetLine(index);
                    for (int i = 0; i < (unit.m_particleTypeParam.m_ribbon.m_jointNum + 2); i++) {
                        line.SetPosition(Vector3.zero, i);
                        line.SetWidth(0f, i);
                        line.SetColor(Color.clear, i);
                    }
                    break;
                }
            }
        }

        public override int GetPropertyNum() {
            return m_Rule.GetPropertyNum();
        }

        public override int GetArrayNum(int propertyIdx) {
            return m_Rule.GetArrayNum(propertyIdx);
        }

        public override object GetValue(int propertyIdx, int arrayIdx) {
            return m_Rule.GetValue(propertyIdx, arrayIdx);
        }

        public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value) {
            m_Rule.SetValue(propertyIdx, arrayIdx, tgtIdx, value);
        }

        public override void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int componentIdx, float value) {
            m_Rule.SetValue(propertyIdx, arrayIdx, tgtIdx+ componentIdx, value);
        }

        public override eEffectAnimTypeCode GetTypeCode(int propertyIdx) {
            return m_Rule.GetTypeCode(propertyIdx);
        }

        public override Type GetPropertType(int propertyIdx) {
            return m_Rule.GetType(propertyIdx);
        }

        public override string GetPropertyName(int propertyIdx) {
            return m_Rule.GetPropertyName(propertyIdx);
        }

        [Serializable]
        public class RenderSettings {
            public EffectMeshBuffer m_MeshBufferPrimBillboard;
            public EffectMeshBuffer m_MeshBufferPrimPoint;
            public EffectMeshBuffer m_MeshBufferPrimLine;
            public EffectMeshBuffer m_MeshBufferPrimPolyline;
            public EffectMeshBuffer m_MeshBufferPrimConfetti;
            public EffectMeshBuffer m_MeshBufferPrimRibbon;
            public EffectMeshBuffer m_MeshBufferPrimTail;
            public EffectRenderer m_EffectRenderer;
            public EffectRenderer m_TailRenderer;
        }

        private struct History {
            public eParticleType m_particleType;
            public int m_particleNum;
            public bool m_UsingAccelerationFlg;
            public bool m_UsingRotationFlg;
            public bool m_UsingUVAnimationFlg;
            public bool m_UsingColorCurveFlg;
            public bool m_UsingTailFlg;
            public bool m_UsingBlinkFlg;
            public bool m_UsingPathMoveFlg;

            public bool IsEqual(ParticleRule ptclRule) {
                return ptclRule.m_particleType == m_particleType
                    && ptclRule.m_particleNum == m_particleNum
                    && ptclRule.m_UsingAccelerationFlg == m_UsingAccelerationFlg
                    && ptclRule.m_UsingRotationFlg == m_UsingRotationFlg
                    && ptclRule.m_UsingUVAnimationFlg == m_UsingUVAnimationFlg
                    && ptclRule.m_UsingColorCurveFlg == m_UsingColorCurveFlg
                    && ptclRule.m_UsingTailFlg == m_UsingTailFlg
                    && ptclRule.m_UsingBlinkFlg == m_UsingBlinkFlg
                    && ptclRule.m_UsingPathMoveFlg == m_UsingPathMoveFlg;
            }
        }
    }
}
