﻿using Meige.AssetBundles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Meige {
    public class MeigeResource {
        [Flags]
        public enum Option {
            None,
            Scene,
            SceneAdditive,
            SceneActivationFalse = 4,
            DownloadOnly = 8,
            ResourceKeep = 16,
            ReferenceCounting = 32
        }

        public enum Error {
            None,
            ManifestUpdateError,
            Timeout,
            Unknown
        }

        public class Handler : IEnumerator {
            protected string m_resourceName;
            protected string m_assetName;
            protected string m_assetbundleName;
            protected int m_option;
            protected Action<Handler> m_callback;
            protected State m_state;
            protected Error m_error;
            protected UnityEngine.Object[] m_assets;
            protected AsyncOperation m_asyncOperation;
            protected bool m_isUnloadAssetbundle;
            protected int m_refCount;

            public string resourceName => m_resourceName;
            public string assetName => m_assetName;
            public string assetBundleName => m_assetbundleName;
            public int option => m_option;
            public bool IsDone => m_state == State.Done;
            public bool IsUnloadDone => m_state == State.UnloadDone;
            public bool IsWait => m_state == State.Wait;
            public bool IsLoading => m_state == State.Loading;
            public State state => m_state;
            public Error error => m_error;
            public bool IsError => m_error != Error.None;
            public AsyncOperation asyncOperation => m_asyncOperation;
            public UnityEngine.Object[] assets => m_assets;
            public bool IsReference => m_refCount > 0;
            public object Current => null;

            public Action<Handler> callback {
                get => m_callback;
                set {
                    m_callback = value;
                    if (value != null && m_state == State.Done) {
                        m_callback(this);
                    }
                }
            }

            public UnityEngine.Object asset {
                get {
                    if (m_assets != null && m_assets.Length > 0) {
                        return m_assets[0];
                    }
                    return null;
                }
            }

            public Handler(string resourceName, string assetName = null, Action<Handler> callback = null, params Option[] options) {
                Clear();
                m_resourceName = resourceName;
                m_assetName = assetName;
                m_callback = callback;
                foreach (Option option in options) {
                    m_option |= (int)option;
                }
                m_refCount = 1;
            }

            ~Handler() {
                Unload(false);
                Clear();
            }

            public void Reset() { }

            public bool MoveNext() {
                return m_state != State.Done;
            }

            public void AddReferenceCount() {
                m_refCount++;
            }

            public void Clear() {
                m_resourceName = string.Empty;
                m_option = 0;
                m_refCount = 0;
                m_assets = null;
                m_asyncOperation = null;
                m_state = State.Wait;
                m_error = Error.None;
                m_isUnloadAssetbundle = false;
            }

            public T GetAsset<T>() where T : UnityEngine.Object {
                if (m_assets != null && m_assets.Length > 0) {
                    return m_assets[0] as T;
                }
                return null;
            }

            public T[] GetAssets<T>() where T : UnityEngine.Object {
                if (m_assets == null) { return null; }

                T[] outAssets = new T[m_assets.Length];
                for (int i = 0; i < m_assets.Length; i++) {
                    outAssets[i] = m_assets[i] as T;
                }
                return outAssets;
            }

            public void Unload(bool isForce = false) {
                if (m_state == State.Unloading || m_state == State.UnloadDone) { return; }

                m_state = State.Unloading;
                if (_Unload(isForce)) {
                    _UnloadScene();
                }
                m_state = State.UnloadDone;
            }

            public IEnumerator UnloadAsync(bool isForce = false) {
                if (m_state == State.Unloading || m_state == State.UnloadDone) { yield break; }

                m_state = State.Unloading;
                if (_Unload(isForce)) {
                    yield return _UnloadSceneAsync();
                }
                m_state = State.UnloadDone;
            }

            private bool _Unload(bool isForce = false) {
                m_refCount--;
                if (m_refCount > 0 && IsReferenceCounting() && !isForce) {
                    return false;
                }
                m_refCount = 0;
                if (m_assets != null) {
                    for (int i = 0; i < m_assets.Length; i++) {
                        if (m_assets[i] != null) {
                            m_assets[i] = null;
                        }
                    }
                    m_assets = null;
                }
                UnloadAssetbundle();
                m_error = Error.None;
                return true;
            }

            public bool IsUnloadAsync() {
                return IsScene();
            }

            private void UnloadAssetbundle() {
                if (IsDownload() && !m_isUnloadAssetbundle && !string.IsNullOrEmpty(m_assetbundleName)) {
                    AssetBundleManager.UnloadAssetBundle(m_assetbundleName, IsDownloadOnly());
                    m_isUnloadAssetbundle = true;
                }
            }

            private void _UnloadScene() {
                if (m_state != State.Unloading) { return; }
                m_asyncOperation = null;

                if (!IsScene() || MeigeResourceManager.quit) { return; }

                string text = IsDownload() ? m_assetName : m_resourceName;
                if (SceneManager.GetSceneByName(text).isLoaded) {
                    m_asyncOperation = SceneManager.UnloadSceneAsync(text);
                }
            }

            private IEnumerator _UnloadSceneAsync() {
                _UnloadScene();
                if (m_asyncOperation != null) {
                    yield return m_asyncOperation;
                }
            }

            public IEnumerator LoadAsync() {
                while (!MeigeResourceManager.isReadyAssetbundle || MeigeResourceManager.isError) {
                    yield return null;
                }
                if (m_state != State.Wait) {
                    yield break;
                }
                m_state = State.Loading;
                int retry = 0;
                while (MeigeResourceManager.retryCount > retry) {
                    if (retry != 0) {
                        UnloadAssetbundle();
                    }
                    retry++;
                    if (IsScene()) {
                        bool isAdditive = IsSceneAdditive();
                        bool allowSceneActivation = !IsSceneActivationFalse();
                        if (IsDownload()) {
                            m_assetbundleName = m_resourceName.ToLower();
                            if (IsDownloadOnly()) {
                                m_isUnloadAssetbundle = true;
                                AssetBundleLoadOperation req = AssetBundleManager.DownloadOnlyAsync(m_assetbundleName);
                                yield return req;
                                if (req.GetError() == ErrorType.Timeout) {
                                    SetError(Error.Timeout);
                                    continue;
                                }
                                if (req.GetError() == ErrorType.Unknown) {
                                    SetError(Error.Unknown);
                                } else {
                                    SetError(Error.None);
                                }
                            } else {
                                m_isUnloadAssetbundle = false;
                                AssetBundleLoadOperation req = AssetBundleManager.LoadLevelAsync(m_assetbundleName, m_assetName, isAdditive, allowSceneActivation);
                                yield return req;
                                m_asyncOperation = req.asyncOperation;
                                if (req.GetError() == ErrorType.Timeout) {
                                    SetError(Error.Timeout);
                                    continue;
                                }
                                if (req.GetError() == ErrorType.Unknown) {
                                    SetError(Error.Unknown);
                                } else {
                                    SetError(Error.None);
                                    bool IsDependence = AssetBundleManager.IsDependence(m_assetbundleName);
                                    if (!IsOption(Option.ResourceKeep) && !IsDependence) {
                                        AssetBundleManager.UnloadAssetBundle(m_assetbundleName, IsDownloadOnly());
                                        m_isUnloadAssetbundle = true;
                                    }
                                }
                            }
                        } else {
                            m_asyncOperation = SceneManager.LoadSceneAsync(m_resourceName, isAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
                            m_asyncOperation.allowSceneActivation = allowSceneActivation;
                            yield return m_asyncOperation;
                        }
                    } else if (IsDownload()) {
                        m_assetbundleName = m_resourceName.ToLower();
                        if (IsDownloadOnly()) {
                            m_isUnloadAssetbundle = true;
                            AssetBundleLoadOperation req = AssetBundleManager.DownloadOnlyAsync(m_assetbundleName);
                            yield return req;
                            if (req.GetError() == ErrorType.Timeout) {
                                SetError(Error.Timeout);
                                continue;
                            }
                            if (req.GetError() == ErrorType.Unknown) {
                                SetError(Error.Unknown);
                            } else {
                                SetError(Error.None);
                            }
                        } else {
                            m_isUnloadAssetbundle = false;
                            AssetBundleLoadAssetOperation req = AssetBundleManager.LoadAssetAsync(m_assetbundleName, m_assetName, typeof(UnityEngine.Object));
                            yield return req;
                            if (req.GetError() == ErrorType.Timeout) {
                                SetError(Error.Timeout);
                                continue;
                            }
                            if (req.GetError() == ErrorType.Unknown) {
                                SetError(Error.Unknown);
                            } else {
                                m_assets = req.GetAllAsset<UnityEngine.Object>();
                                SetError(Error.None);
                                bool isDependence = AssetBundleManager.IsDependence(m_assetbundleName);
                                if (!IsOption(Option.ResourceKeep) && !isDependence) {
                                    AssetBundleManager.UnloadAssetBundle(m_assetbundleName, IsDownloadOnly());
                                    m_isUnloadAssetbundle = true;
                                }
                            }
                        }
                    } else {
                        m_assets = Resources.LoadAll<UnityEngine.Object>(m_resourceName);
                        if (m_assets == null) {
                            Debug.LogError("File does not exist : " + m_resourceName);
                            m_error = Error.Unknown;
                        }
                    }
                    break;
                }
                m_state = State.Done;
                if (m_error != Error.None) {
                    MeigeResourceManager.SetError(this);
                }
                m_callback?.Invoke(this);
            }

            public IEnumerator Retry() {
                yield return UnloadAsync(false);
                m_state = State.Wait;
                yield return LoadAsync();
            }

            public string GetVersion() {
                if (string.IsNullOrEmpty(m_assetbundleName)) { return string.Empty; }
                return AssetBundleManager.GetBundleVersion(m_assetbundleName);
            }

            private void SetError(Error error) {
                m_error = error;
            }

            private bool IsScene() {
                return IsOption(Option.Scene | Option.SceneAdditive);
            }

            private bool IsSceneAdditive() {
                return IsOption(Option.SceneAdditive);
            }

            private bool IsSceneActivationFalse() {
                return IsOption(Option.SceneActivationFalse);
            }

            private bool IsDownloadOnly() {
                return IsOption(Option.DownloadOnly);
            }

            private bool IsReferenceCounting() {
                return IsOption(Option.ReferenceCounting);
            }

            private bool IsOption(Option option) {
                return (m_option & (int)option) != 0;
            }

            private bool IsDownload() {
                return m_resourceName.Contains(MeigeResourceManager.AB_EXTENSION);
            }

            public enum State {
                Wait,
                Loading,
                Done,
                Unloading,
                UnloadDone
            }
        }

        public class DLSizeHandle : IEnumerator {
            private const int POOLING_JOB_NUM = 50;
            private List<string> m_FilePathList;
            private int m_CacheSaveNum;
            private object m_LockObj = new object();
            private List<string> m_ListToDelete = new List<string>();
            private State m_State;
            private long m_ResultCode;

            public DLSizeHandle(List<string> list) {
                m_FilePathList = list;
                m_State = State.Wait;
                m_ResultCode = 0;
            }

            public IEnumerator CheckAsync() {
                while (!MeigeResourceManager.isReadyAssetbundle || MeigeResourceManager.isError) {
                    yield return null;
                }
                if (m_State != State.Wait) {
                    yield break;
                }
                m_State = State.Checking;

                List<Job> unusedJobs = new List<Job>(POOLING_JOB_NUM);
                List<Job> usedJobs = new List<Job>(POOLING_JOB_NUM);
                List<Job> doneList = new List<Job>();
                for (int i = 0; i < POOLING_JOB_NUM; i++) {
                    unusedJobs.Add(new Job());
                }

                List<string> dependencyList = AssetBundleManager.Instance.RemakeListWithDependencies(m_FilePathList);
                long dlSize = 0;
                foreach (string filePath in dependencyList) {
                    while (unusedJobs.Count == 0) {
                        yield return null;
                        for (int i = 0; i < usedJobs.Count; i++) {
                            if (usedJobs[i].IsDone()) {
                                doneList.Add(usedJobs[i]);
                            }
                        }
                        if (doneList.Count > 0) {
                            for (int i = 0; i < doneList.Count; i++) {
                                Job doneJob = doneList[i];
                                usedJobs.Remove(doneJob);
                                dlSize += (doneJob.GetObject() as JobWorkObject).dlSize;
                                doneJob.Clear();
                                unusedJobs.Add(doneJob);
                            }
                            doneList.Clear();
                        }
                    }

                    JobWorkObject workObject = new JobWorkObject();
                    workObject.localFilePath = MeigeAssetBundlesUtilityExt.ToBundleHashName(filePath);
                    workObject.fullFilePath = Utility.GetFilePath(workObject.localFilePath);
                    workObject.streamingAssetsPath = Utility.GetStreamingPath(workObject.localFilePath);

                    Job job = unusedJobs[0];
                    unusedJobs.RemoveAt(0);
                    job.SetFunction(DoWork_Check, workObject);
                    JobQueue.Enqueue(job);
                    usedJobs.Add(job);
                }
                while (usedJobs.Count > 0) {
                    yield return null;
                    for (int i = 0; i < usedJobs.Count; i++) {
                        if (usedJobs[i].IsDone()) {
                            doneList.Add(usedJobs[i]);
                        }
                    }
                    if (doneList.Count > 0) {
                        for (int i = 0; i < doneList.Count; i++) {
                            Job doneJob = doneList[i];
                            usedJobs.Remove(doneJob);
                            dlSize += (doneJob.GetObject() as JobWorkObject).dlSize;
                        }
                        doneList.Clear();
                    }
                }
                m_ResultCode = dlSize;
                m_State = State.Done;
                if (m_CacheSaveNum > 0) {
                    AssetBundleManager.Instance.assetBundleVersionCache.SaveVersionCacheFile(true);
                }
            }

            private void DoWork_Check(object obj) {
                JobWorkObject workObject = obj as JobWorkObject;
                long size = AssetBundleManager.GetBundleSize(workObject.localFilePath);
                string localVersion = null;
                workObject.dlSize = 0;
                if (File.Exists(workObject.streamingAssetsPath)) {
                    localVersion = AssetBundleManager.GetBundleVersion(workObject.localFilePath);
                    string streamingAssetsVersion = AssetBundleManager.GetBundleVersion(workObject.localFilePath, true);
                    if (localVersion == streamingAssetsVersion) {
                        workObject.dlSize = 0;
                        return;
                    }
                }
                if (!File.Exists(workObject.fullFilePath)) {
                    workObject.dlSize = size;
                    return;
                }
                FileInfo file = new FileInfo(workObject.fullFilePath);
                if (!file.Exists || file.Length == size) {
                    localVersion ??= AssetBundleManager.GetBundleVersion(workObject.localFilePath);
                    if (AssetBundleManager.Instance.assetBundleVersionCache.GetVersion(workObject.localFilePath, out string version, out _, out _, out bool isEncrypt)) {
                        if (localVersion == version) {
                            return;
                        }
                    } else {
                        string md5 = new Utility().GetMD5(workObject.fullFilePath);
                        if (md5 == localVersion) {
                            AssetBundleManager.Instance.assetBundleVersionCache.SetVersion(workObject.localFilePath, localVersion, 0, size, false, isEncrypt);
                            Interlocked.Increment(ref m_CacheSaveNum);
                            return;
                        }
                    }
                }
                workObject.dlSize = size;
                AddListToDelete(workObject.fullFilePath);
            }

            private void AddListToDelete(string path) {
                lock (m_LockObj) {
                    if (!m_ListToDelete.Contains(path)) {
                        m_ListToDelete.Add(path);
                    }
                }
            }

            public object Current => null;

            public void Reset() { }

            public bool MoveNext() {
                return m_State != State.Done;
            }

            public bool IsDone() {
                return m_State == State.Done;
            }

            public long GetResultCode() {
                return m_ResultCode;
            }

            public List<string> GetListToDelete() {
                return m_ListToDelete;
            }

            public enum State {
                Wait,
                Checking,
                Done
            }

            private class JobWorkObject {
                public string localFilePath;
                public string fullFilePath;
                public string streamingAssetsPath;
                public long dlSize;
            }
        }
    }
}
