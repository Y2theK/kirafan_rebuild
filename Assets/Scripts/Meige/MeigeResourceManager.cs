﻿using Meige.AssetBundles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Meige {
    public class MeigeResourceManager : SingletonMonoBehaviour<MeigeResourceManager> {
        public const int DEFAULT_RETRY_COUNT = 5;
        public const string AB_EXTENSION = ".muast";

        private static bool m_quit;

        private int m_retryCount = DEFAULT_RETRY_COUNT;
        private Dictionary<string, MeigeResource.Handler> m_resourceHandlers = new Dictionary<string, MeigeResource.Handler>();
        private Dictionary<string, MeigeResource.Error> m_errorList = new Dictionary<string, MeigeResource.Error>();

        public static bool quit => m_quit;
        public static bool isReadyAssetbundle => AssetBundleManager.isReady;
        public static bool isError => AssetBundleManager.manifestUpdateError || Instance.m_errorList.Count > 0;

        public static int retryCount {
            get => Instance.m_retryCount;
            set => Instance.m_retryCount = value;
        }

        public static float timeoutSec {
            get => AssetBundleManager.timeoutSec;
            set => AssetBundleManager.timeoutSec = value;
        }

        private void OnApplicationQuit() {
            m_quit = true;
        }

        public static MeigeResource.Error GetError() {
            if (AssetBundleManager.manifestUpdateError) {
                return MeigeResource.Error.ManifestUpdateError;
            }
            return Instance.m_errorList.Values.FirstOrDefault();
        }

        public static void SetAssetbundleServer(string url) {
            AssetBundleManager.Initialize(url);
        }

        public static void AssetbundleCleanCache() {
            AssetbundleCache.CleanCache();
            AssetBundleManager.ClearFlags();
            AssetBundleManager.Instance.assetBundleVersionCache.Clear();
        }

        public static IEnumerator AssetbundleCleanCacheAsync() {
            yield return new AssetbundleCache(string.Empty).CleanCacheAsync();
            AssetBundleManager.ClearFlags();
            AssetBundleManager.Instance.assetBundleVersionCache.Clear();
            yield return null;
        }

        public static IEnumerator UpdateAssetbundleManifest() {
            yield return AssetBundleManager.Instance.UpdateManifest();
        }

        public static string GetVersion(string resName) {
            if (string.IsNullOrEmpty(resName)) {
                return string.Empty;
            }
            if (!resName.Contains(AB_EXTENSION)) {
                resName += AB_EXTENSION;
            }
            string assetBundleName = resName.ToLower();
            return AssetBundleManager.GetBundleVersion(assetBundleName);
        }

        public static Dictionary<string, MeigeResource.Error> GetErrors() {
            return new Dictionary<string, MeigeResource.Error>(Instance.m_errorList);
        }

        public static void SetError(MeigeResource.Handler handler) {
            Instance.m_errorList[handler.resourceName] = handler.error;
        }

        public static void ClearError() {
            AssetBundleManager.RestartManifestUpdate();
            Instance.m_errorList.Clear();
        }

        public static void ClearError(string resourceName) {
            if (Instance.m_errorList.ContainsKey(resourceName)) {
                Instance.m_errorList.Remove(resourceName);
            }
        }

        public static int GetLoadingCount() {
            return Instance._GetLoadingCount();
        }

        private int _GetLoadingCount() {
            return _LoadingList().Count;
        }

        public static List<string> GetLoadingList() {
            return Instance._LoadingList();
        }

        private List<string> _LoadingList() {
            List<string> loading = new List<string>();
            foreach (string resource in Instance.m_resourceHandlers.Keys) {
                MeigeResource.Handler handler = Instance.m_resourceHandlers[resource];
                if (!handler.IsDone || handler.IsError) {
                    loading.Add(resource);
                }
            }
            return loading;
        }

        public static MeigeResource.Handler LoadHandler(string resourceName, params MeigeResource.Option[] options) {
            return Instance._LoadHandler(resourceName, null, null, options);
        }

        public static MeigeResource.Handler LoadHandler(string resourceName, string assetName, params MeigeResource.Option[] options) {
            return Instance._LoadHandler(resourceName, assetName, null, options);
        }

        public static MeigeResource.Handler LoadHandler(string resourceName, Action<MeigeResource.Handler> callback, params MeigeResource.Option[] options) {
            return Instance._LoadHandler(resourceName, null, callback, options);
        }

        public static MeigeResource.Handler LoadHandler(string resourceName, string assetName, Action<MeigeResource.Handler> callback, params MeigeResource.Option[] options) {
            return Instance._LoadHandler(resourceName, assetName, callback, options);
        }

        private MeigeResource.Handler _LoadHandler(string resourceName, string assetName = null, Action<MeigeResource.Handler> callback = null, params MeigeResource.Option[] options) {
            if (Instance.m_resourceHandlers.TryGetValue(resourceName, out MeigeResource.Handler handler)) {
                if (handler.IsWait || handler.IsLoading || handler.IsError || handler.asset != null) {
                    handler.AddReferenceCount();
                    return handler;
                }
                _UnloadHandler(resourceName, false);
            } else {
                Debug.LogWarning("Handler does not exist : " + resourceName);
            }
            MeigeResource.Handler newHandler = new MeigeResource.Handler(resourceName, assetName, callback, options);
            m_resourceHandlers.Add(resourceName, newHandler);
            StartCoroutine(newHandler.LoadAsync());
            return newHandler;
        }

        public static void RetryHandler(MeigeResource.Handler handler) {
            Instance._RetryHandler(handler);
        }

        public void _RetryHandler(MeigeResource.Handler handler) {
            ClearError(handler.resourceName);
            StartCoroutine(handler.Retry());
        }

        public static void UnloadHandler(MeigeResource.Handler handler, bool isForce = false) {
            Instance._UnloadHandler(handler, isForce);
        }

        private void _UnloadHandler(MeigeResource.Handler handler, bool isForce = false) {
            Instance._UnloadHandler(handler.resourceName, isForce);
        }

        private void _UnloadHandler(string resourceName, bool isForce = false) {
            if (m_resourceHandlers.TryGetValue(resourceName, out MeigeResource.Handler handler)) {
                handler.Unload(isForce);
                if (!handler.IsReference) {
                    m_resourceHandlers.Remove(resourceName);
                }
            }
        }

        public static Dictionary<string, MeigeResource.Handler> GetHandlerAll() {
            return Instance._GetHandlerAll();
        }

        private Dictionary<string, MeigeResource.Handler> _GetHandlerAll() {
            return m_resourceHandlers;
        }

        public static IEnumerator UnloadHandlerAllAsync() {
            yield return Instance._UnloadHandlerAllAsync();
        }

        public static void UnloadHandlerAll() {
            Instance._UnloadHandlerAll();
        }

        private void _UnloadHandlerAll() {
            foreach (MeigeResource.Handler handler in m_resourceHandlers.Values) {
                StopCoroutine(handler.LoadAsync());
                handler.Unload(true);
            }
            m_resourceHandlers.Clear();
        }

        private IEnumerator _UnloadHandlerAllAsync() {
            foreach (MeigeResource.Handler handler in m_resourceHandlers.Values) {
                StopCoroutine(handler.LoadAsync());
                if (handler.IsUnloadAsync()) {
                    yield return handler.UnloadAsync(true);
                } else {
                    handler.Unload(false);
                }
            }
            m_resourceHandlers.Clear();
        }

        public static MeigeResource.DLSizeHandle CheckDLSize(List<string> resourceList) {
            return Instance._CheckDLSize(resourceList);
        }

        private MeigeResource.DLSizeHandle _CheckDLSize(List<string> resourceList) {
            MeigeResource.DLSizeHandle handle = new MeigeResource.DLSizeHandle(resourceList);
            StartCoroutine(handle.CheckAsync());
            return handle;
        }
    }
}
