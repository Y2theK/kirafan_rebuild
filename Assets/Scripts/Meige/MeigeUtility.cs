﻿using System.Xml;
using UnityEngine;

namespace Meige {
    public static class MeigeUtility {
        public static int MakeEnumFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeEnumFromXmlNode ... XmlNode is not existed.");
                return -1;
            }
            string[] array = xmlNode.InnerXml.Split(':');
            return int.Parse(array[0]);
        }

        public static bool MakeBoolFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeBoolFromXmlNode ... XmlNode is not existed.");
                return false;
            }
            return xmlNode.InnerXml.Equals("True");
        }

        public static int MakeIntFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeIntFromXmlNode ... XmlNode is not existed.");
                return 0;
            }
            return int.Parse(xmlNode.InnerXml);
        }

        public static float MakeFloatFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeFloatFromXmlNode ... XmlNode is not existed.");
                return 0f;
            }
            return float.Parse(xmlNode.InnerXml);
        }

        public static string MakeStringFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeStringFromXmlNode ... XmlNode is not existed.");
                return string.Empty;
            }
            return xmlNode.InnerXml;
        }

        public static Vector2 MakeFloat2FromXmlNode(XmlNode xmlNode) {
            string[] values = xmlNode.InnerXml.Split(':')[1].Split(',');
            Vector2 result;
            result.x = float.Parse(values[0]);
            result.y = float.Parse(values[1]);
            return result;
        }

        public static Vector4 MakeVectorFromXmlNode(XmlNode xmlNode) {
            string[] values = xmlNode.InnerXml.Split(':')[1].Split(',');
            Vector4 result;
            result.x = float.Parse(values[0]);
            result.y = float.Parse(values[1]);
            result.z = float.Parse(values[2]);
            result.w = float.Parse(values[3]);
            return result;
        }

        public static void MakeIntArrayFromXmlNode(out int[] iArray, XmlNode xmlNode) {
            string[] values = xmlNode.InnerXml.Split(',');
            iArray = new int[values.Length];
            for (int i = 0; i < values.Length; i++) {
                iArray[i] = int.Parse(values[i]);
            }
        }

        public static Color MakeColorFromXmlNode(XmlNode xmlNode) {
            if (xmlNode == null) {
                Helper.Break(false, "MakeColorFromXmlNode ... XmlNode is not existed.");
                return Color.black;
            }
            return MakeVectorFromXmlNode(xmlNode);
        }

        public static Rect MakeRectFromXmlNode(XmlNode xmlNode) {
            Vector4 vector = MakeVectorFromXmlNode(xmlNode);
            return new Rect(vector.x, vector.y, vector.z, vector.w);
        }

        public static Bounds MakeAABBFromXmlNode(XmlNode xmlNode) {
            return new Bounds {
                min = MakeVectorFromXmlNode(xmlNode.SelectSingleNode("AABB.Min")),
                max = MakeVectorFromXmlNode(xmlNode.SelectSingleNode("AABB.Max"))
            };
        }

        public static ProjDepend.eRenderQueue RenderStageToRenderQueue(eRenderStage renderStage, int renderOrder) {
            int order = renderOrder % MeigeDefs.STRIDE_FROM_RENDERSTAGE_TO_RENDERQUEUE;
            int stage = (int)renderStage * MeigeDefs.STRIDE_FROM_RENDERSTAGE_TO_RENDERQUEUE;
            if (renderStage >= eRenderStage.eRenderStage_Alpha) {
                stage += MeigeDefs.OFFSET_TO_ALPHA_RENDERQUEUE;
            }
            return (ProjDepend.eRenderQueue)(order + stage);
        }

        public static bool FloatEqual(float f0, float f1, float threshold = MeigeDefs.F_EPSILON6) {
            return Mathf.Abs(f0 - f1) <= threshold;
        }

        public static Vector3 CM2M(Vector3 v) {
            return v / 100f;
        }

        public static float CM2M(float v) {
            return v / 100f;
        }
    }
}
