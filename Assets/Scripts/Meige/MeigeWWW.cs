﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Meige {
    public class MeigeWWW : IDisposable {
        private const int DEFAULT_TIMEOUT_SEC = 20;
        private const string REGEX_SERVER_TIME = "\"serverTime\":\"([-+\\d:T]+?)\"";

        private static Dictionary<string, string> m_HttpHeaderCommon;
        private static string m_sessionID = "";
        private static string m_appversion = "0.0.0";
        private static bool m_isRelease;
        private static int m_timeoutSec = DEFAULT_TIMEOUT_SEC;

        private UnityWebRequest m_www;
        private MeigewwwParam m_param;

        public static string SessionId {
            get => m_sessionID;
            set => m_sessionID = value;
        }

        public static string AppVersion {
            get => m_appversion;
            set => m_appversion = value;
        }

        public static bool IsRelease {
            get => m_isRelease;
            set => m_isRelease = value;
        }

        public static int TimeoutSec {
            get => m_timeoutSec;
            set => m_timeoutSec = value;
        }

        public static Dictionary<string, string> HttpHeaderCommon {
            get => m_HttpHeaderCommon;
            set => m_HttpHeaderCommon = value;
        }

        public static void SetUp() {
            if (m_HttpHeaderCommon == null) {
                m_HttpHeaderCommon = new Dictionary<string, string>();
                string text = "app/" + m_appversion;
                text = text + "; " + SystemInfo.operatingSystem.Replace("(", string.Empty).Replace(")", string.Empty);
#if UNITY_IOS
				text = text + "; " + Device.generation.ToString().Replace("(", string.Empty).Replace(")", string.Empty);
#endif
                text = text + "; " + SystemInfo.deviceModel.Replace("(", string.Empty).Replace(")", string.Empty);
                m_HttpHeaderCommon.Add("Unity-User-Agent", text);
                m_HttpHeaderCommon.Add("Content-Type", "application/json; charset=UTF-8");
            }
        }

        public MeigeWWW(MeigewwwParam wwwParam) {
            m_www = null;
            m_param = wwwParam;
        }

        public void Dispose() {
            if (m_www != null) {
                m_www.Dispose();
                m_www = null;
            }
            if (m_param != null) {
                m_param = null;
            }
        }

        public IEnumerator Kick() {
            m_param.Setup();
            Dictionary<string, string> headers = new Dictionary<string, string>();
            string json = string.Empty;
            byte[] postData = null;
            if (m_param.GetMethod() == MeigewwwParam.eRequestMethod.Post) {
                json = m_param.GetRequestJson();
                postData = Encoding.UTF8.GetBytes(json);
                if (m_param.IsPostCrypt()) {
                    IEnumerator coroutine = Compress(postData);
                    yield return coroutine;
                    postData = (byte[])coroutine.Current;
                    coroutine = Encrypt(postData);
                    yield return coroutine;
                    postData = (byte[])coroutine.Current;
                    headers.Add(ProjDepend.WHCRY, "1");
                }
            }
            headers.Add(ProjDepend.WHRTID, m_param.GetGUID() + m_sessionID);
            string basestring = m_sessionID;
            if (basestring.Length > 0) {
                basestring += " ";
            }
            basestring = basestring + "/api/" + m_param.GetAPI();
            if (!string.IsNullOrEmpty(json)) {
                basestring = basestring + " " + json;
            }
            basestring = basestring + " " + (m_isRelease ? ProjDepend.REQUESTHASH_SECRET_APPLYING : ProjDepend.REQUESTHASH_SECRET);
            SHA256 crypto = new SHA256CryptoServiceProvider();
            byte[] requesthash = crypto.ComputeHash(Encoding.UTF8.GetBytes(basestring));
            crypto.Clear();
            StringBuilder requesthashText = new StringBuilder();
            foreach (byte b in requesthash) {
                requesthashText.Append(b.ToString("x2"));
            }
            headers[ProjDepend.WHERHK] = requesthashText.ToString();
            if (!string.IsNullOrEmpty(m_sessionID)) {
                headers[ProjDepend.WHSIK] = m_sessionID;
            }
            foreach (string key in m_HttpHeaderCommon.Keys) {
                if (!headers.ContainsKey(key)) {
                    headers.Add(key, m_HttpHeaderCommon[key]);
                }
            }
            string server_url = (m_isRelease ? ProjDepend.SERVER_APPLYING_URL : ProjDepend.SERVER_URL) + m_param.GetAPI();
            int tryCnt = m_param.GetRetryCount() + 1;
            bool success = true;
            while (true) {
                long startTicks = DateTime.Now.Ticks;
                m_www = new UnityWebRequest(server_url, m_param.GetMethod().ToString());
                if (m_param.GetMethod() == MeigewwwParam.eRequestMethod.Post) {
                    m_www.uploadHandler = new UploadHandlerRaw(postData);
                }
                m_www.downloadHandler = new DownloadHandlerBuffer();
                foreach (KeyValuePair<string, string> kvp in headers) {
                    m_www.SetRequestHeader(kvp.Key, kvp.Value);
                }
                m_www.timeout = TimeoutSec;
                yield return m_www.SendWebRequest();
                if (m_www != null && m_www.isDone && string.IsNullOrEmpty(m_www.error)) {
                    break;
                }
                if (--tryCnt <= 0) {
                    success = false;
                    break;
                }
                if (m_www != null) {
                    m_www.Dispose();
                    m_www = null;
                }
                yield return new WaitForSeconds(5f);
            }
            if (success) {
                DownloadHandlerBuffer handler = (DownloadHandlerBuffer)m_www.downloadHandler;
                string headerValue = m_www.GetResponseHeader(ProjDepend.WHRPK);
                string text = "";
                if (m_www.GetResponseHeader(ProjDepend.WHRCRY) == "1") {
                    byte[] bytes = handler.data;
                    if (bytes != null) {
                        IEnumerator coroutine = Decrypt(bytes);
                        yield return coroutine;
                        bytes = (byte[])coroutine.Current;
                        if (bytes != null) {
                            coroutine = Uncompress(bytes);
                            yield return coroutine;
                            bytes = (byte[])coroutine.Current;
                            text = Encoding.UTF8.GetString(bytes);
                        }
                    }
                } else {
                    text = handler.text;
                }
                if (!CheckResponseBody(text, headerValue)) {
                    m_param.SetSystemError(MeigewwwParam.eSystemError.Unknown);
                    m_param.SetResponseJson(string.Empty);
                } else {
                    m_param.SetSystemError(MeigewwwParam.eSystemError.None);
                    m_param.SetResponseJson(text);
                }
            }
            if (m_www != null && !string.IsNullOrEmpty(m_www.error)) {
                Debug.Log("www system error : " + m_www.error);
                m_param.SetError(m_www.error);
                m_param.SetSystemError(MeigewwwParam.eSystemError.Unknown);
            }
            if (m_www != null) {
                m_www.Dispose();
                m_www = null;
            }
            m_param.OnRecived();
            Dispose();
        }

        private bool CheckResponseBody(string body, string headerValue) {
            if (string.IsNullOrEmpty(headerValue)) {
                return false;
            }
            uint headCrc;
            if (ProjDepend.RH_CRC_IS_HEXADECIMAL) {
                uint.TryParse(headerValue, NumberStyles.HexNumber, null, out headCrc);
            } else {
                uint.TryParse(headerValue, out headCrc);
            }
            string value = string.Empty;
            Regex regex = new Regex(REGEX_SERVER_TIME, RegexOptions.None);
            Match match = regex.Match(body);
            if (match.Success) {
                value = match.Groups[1].ToString();
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(body);
            stringBuilder.Append(value);
            byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
            uint crc = CRC32.Compute(bytes);
            return headCrc == crc;
        }

        private IEnumerator Encrypt(byte[] src) {
            string text = "/api/" + m_param.GetAPI();
            text += ProjDepend.WHCRYSC;
            EncryptParam param = new EncryptParam() {
                src = src,
                ivStr = text
            };
            Job job = new Job();
            job.SetFunction(DoWork_Encrypt, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            yield return param.dst;
        }

        private void DoWork_Encrypt(object obj) {
            EncryptParam param = (EncryptParam)obj;
            byte[] inArray = new AesCryptUtil().encrypt(param.src, param.ivStr);
            string s = Convert.ToBase64String(inArray);
            param.dst = Encoding.UTF8.GetBytes(s);
        }

        private IEnumerator Compress(byte[] src) {
            CompressParam param = new CompressParam() {
                src = src,
            };
            Job job = new Job();
            job.SetFunction(DoWork_Compress, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            yield return param.dst;
        }

        private void DoWork_Compress(object obj) {
            CompressParam param = (CompressParam)obj;
            param.dst = new ZlibUtil().compressProcess(param.src);
        }

        private IEnumerator Decrypt(byte[] src) {
            string text = "/api/" + m_param.GetAPI();
            text += ProjDepend.WHCRYSC;
            DecryptParam param = new DecryptParam() {
                src = src,
                ivStr = text
            };
            Job job = new Job();
            job.SetFunction(DoWork_Decrypt, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            yield return param.dst;
        }

        private void DoWork_Decrypt(object obj) {
            DecryptParam param = (DecryptParam)obj;
            param.dst = new AesCryptUtil().decrypt(param.src, param.ivStr);
        }

        private IEnumerator Uncompress(byte[] src) {
            UncompressParam param = new UncompressParam() {
                src = src,
            };
            Job job = new Job();
            job.SetFunction(DoWork_Uncompress, param);
            JobQueue.Enqueue(job);
            while (!job.IsDone()) {
                yield return null;
            }
            yield return param.dst;
        }

        private void DoWork_Uncompress(object obj) {
            UncompressParam param = (UncompressParam)obj;
            param.dst = new ZlibUtil().uncompressProcess(param.src);
        }

        private class EncryptParam {
            public byte[] src;
            public string ivStr;
            public byte[] dst;
        }

        private class CompressParam {
            public byte[] src;
            public byte[] dst;
        }

        private class DecryptParam {
            public byte[] src;
            public string ivStr;
            public byte[] dst;
        }

        private class UncompressParam {
            public byte[] src;
            public byte[] dst;
        }
    }
}
