﻿using Newtonsoft.Json;
using System;

namespace Meige {
    public class MeigewwwParam {
        private string m_API;
        private string m_requestParameter;
        private eRequestMethod m_method;
        private string m_requestJson;
        private Callback onRecivedCallback;
        private bool m_postDataCrypt;
        private int m_retryCount;
        private eStatus m_status;
        private string m_responseJson;
        private string m_responseError;
        private eSystemError m_systemError;
        private int m_maskFlag;
        private int m_parameterCount;
        private Parameter[] m_parameters;
        private string m_guid;

        public MeigewwwParam() {
            Clear();
            m_guid = Guid.NewGuid().ToString();
        }

        public bool IsDone() {
            return m_status == eStatus.Done;
        }

        public eStatus GetStatus() {
            return m_status;
        }

        public void SetStatus(eStatus status) {
            m_status = status;
        }

        public string GetAPI() {
            return m_API + m_requestParameter;
        }

        public eRequestMethod GetMethod() {
            return m_method;
        }

        public string GetRequestJson() {
            return m_requestJson;
        }

        public void SetResponseJson(string json) {
            m_responseJson = json;
        }

        public string GetResponseJson() {
            return m_responseJson;
        }

        public bool IsSystemError() {
            return m_systemError != eSystemError.None;
        }

        public eSystemError GetSystemError() {
            return m_systemError;
        }

        public void SetSystemError(eSystemError systemError) {
            m_systemError = systemError;
        }

        public void SetError(string json) {
            m_responseError = json;
        }

        public string GetError() {
            return m_responseError;
        }

        public void SetMaskFlag(int maskFlag) {
            m_maskFlag = maskFlag;
        }

        public int GetMaskFlag() {
            return m_maskFlag;
        }

        public void SetRetryCount(int retry) {
            m_retryCount = (retry < 0) ? 0 : retry;
        }

        public int GetRetryCount() {
            return m_retryCount;
        }

        public string GetGUID() {
            return m_guid;
        }

        public void SetPostCrypt() {
            m_postDataCrypt = true;
        }

        public bool IsPostCrypt() {
            return m_postDataCrypt;
        }

        public void Clear() {
            m_API = string.Empty;
            m_requestParameter = string.Empty;
            m_method = eRequestMethod.None;
            m_requestJson = string.Empty;
            m_responseJson = string.Empty;
            m_responseError = null;
            onRecivedCallback = null;
            m_postDataCrypt = false;
            m_retryCount = 0;
            m_systemError = eSystemError.None;
            m_parameterCount = 0;
            m_parameters = new Parameter[100];
        }

        public void Init(string api, eRequestMethod method, Callback callback = null) {
            m_API = api;
            m_method = method;
            onRecivedCallback = callback;
        }

        public void Setup() {
            if (m_method == eRequestMethod.Get) {
                m_requestParameter = ParameterToHttpParam();
            } else if (m_method == eRequestMethod.Post) {
                m_requestJson = ParameterToJson();
            }
        }

        public void OnRecived() {
            NetworkQueueManager.Done(this);
            if (onRecivedCallback != null) {
                onRecivedCallback(this);
            }
        }

        private string ParameterToHttpParam() {
            if (m_parameterCount <= 0) {
                return string.Empty;
            }
            string text = "?";
            for (int i = 0; i < m_parameterCount; i++) {
                if (i != 0) {
                    text += "&";
                }
                text += m_parameters[i].paramName;
                text += "=";
                string text2 = m_parameters[i].value.ToString();
                text2 = NetworkQueueManager.escapeSpecialChars(text2);
                text += text2;
            }
            return text;
        }

        private string ParameterToJson() {
            string str = "{";
            if (m_parameterCount > 0) {
                for (int i = 0; i < m_parameterCount; i++) {
                    if (i != 0) {
                        str += ",";
                    }
                    str = str + "\"" + m_parameters[i].paramName + "\"";
                    str += ":";
                    str += JsonConvert.SerializeObject(m_parameters[i].value);
                }
            }
            return str + "}";
        }

        public void Add(string paramName, object value) {
            if (MeigeDefs.WWW_MAX_REQUEST_PARAM <= m_parameterCount) {
                throw new Exception("MeigewwwParam.Add Array outside the reference. [MeigeDefs.WWW_MAX_REQUEST_PARAM <= " + m_parameterCount + "]");
            }
            m_parameters[m_parameterCount].paramName = paramName;
            m_parameters[m_parameterCount].value = value;
            m_parameterCount++;
        }

        public int GetParameterNum() {
            return m_parameterCount;
        }

        public void GetParameter(out Parameter outparam, int index) {
            outparam = m_parameters[index];
        }

        public enum eStatus {
            Wait,
            Progress,
            Done
        }

        public enum eSystemError {
            None,
            Timeout,
            Unknown
        }

        public enum eRequestMethod {
            None,
            Get,
            Post
        }

        public struct Parameter {
            public string paramName;
            public object value;
        }

        public delegate void Callback(MeigewwwParam wwwParam);
    }
}
