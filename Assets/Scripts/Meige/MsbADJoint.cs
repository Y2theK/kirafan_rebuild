﻿using UnityEngine;

namespace Meige {
    public class MsbADJoint {
        private MsbArticulatedDynamicsHandler m_Owner;
        private MsbADJoint m_Parent;
        private MsbADJointParam m_Work;
        private AD_HIE_CALC m_ADHieCalc;
        private Vector3 m_WorldPosition;
        private Vector3 m_OldWorldPosition;
        private Vector3 m_Velocity;
        private Vector3 m_Gravity;
        private Vector3 m_KeepShapeAccel;
        private float m_Mass;
        private float m_AirFrictionCoef;
        private float m_AirFrictionCoefDivideByMass;
        private float m_fVelocity;
        private bool m_bCollided;
        private bool m_bAvailable;

        public void Init(MsbArticulatedDynamicsHandler owner, MsbADJointParam src) {
            m_Owner = owner;
            m_Work = new MsbADJointParam(src);
            m_Velocity = Vector3.zero;
            m_fVelocity = 0f;
            m_WorldPosition = Vector3.zero;
            m_OldWorldPosition = Vector3.zero;
            MsbHierarchyNode node = owner.GetOwner().GetMsbHierarchyNodeByName(GetHieName());
            m_ADHieCalc = new AD_HIE_CALC();
            m_ADHieCalc.Init(node);
            m_KeepShapeAccel = m_ADHieCalc.m_Position;
            float accelSqr = Vector3.Dot(m_KeepShapeAccel, m_KeepShapeAccel);
            if (accelSqr > MeigeDefs.F_EPSILON6) {
                m_KeepShapeAccel.Normalize();
                m_KeepShapeAccel *= MeigeDefs.MEIGE_GRAVITY;
            } else {
                m_KeepShapeAccel = Vector3.zero;
            }
            m_Gravity.Set(0f, -MeigeDefs.MEIGE_GRAVITY, 0f);
            m_Mass = 1f;
            m_AirFrictionCoef = m_Owner.GetAirFrictionCoef();
            m_AirFrictionCoefDivideByMass = m_AirFrictionCoef / m_Mass;
        }

        public void SetParent(MsbADJoint parent) {
            m_Parent = parent;
            m_ADHieCalc.m_HieCalcParent = m_Parent.GetADHieCalc();
        }

        public void StartFromScratch() {
            m_ADHieCalc.StartFromScratch();
        }

        public void CopyFromHierarchy() {
            MsbHierarchyNode hierarchyNode = m_ADHieCalc.m_HierarchyNode;
            m_ADHieCalc.m_Rotation = hierarchyNode.GetTransform().localRotation;
            m_ADHieCalc.m_ScaleOfDistance = hierarchyNode.GetScaleOfDistance();
            m_ADHieCalc.MakeMatrix();
            m_WorldPosition = m_ADHieCalc.m_Matrix.GetColumn(3);
            m_OldWorldPosition = m_WorldPosition;
        }

        public void Prepare(Vector3 translateToDiscard, bool bStartFromScratch) {
            m_ADHieCalc.m_Rotation = Quaternion.identity;
            m_ADHieCalc.m_ScaleOfDistance = 1f;
            m_ADHieCalc.MakeMatrix();
            if (bStartFromScratch) {
                m_Velocity = Vector3.zero;
                m_fVelocity = 0f;
                m_WorldPosition = m_ADHieCalc.m_Matrix.GetColumn(3);
                m_OldWorldPosition = m_WorldPosition;
            } else if (IsLock()) {
                m_Velocity = Vector3.zero;
                m_fVelocity = 0f;
                m_WorldPosition = GetHierarchyNode().GetTransform().localToWorldMatrix.GetColumn(3);
                m_OldWorldPosition = m_WorldPosition;
            } else {
                m_WorldPosition += translateToDiscard;
                m_OldWorldPosition += translateToDiscard;
            }
        }

        public void ExternalForce(float fDt) {
            Collided(false);

            if (IsLock()) { return; }

            Vector3 accel = Vector3.zero;
            float keepShapeCoef = m_Owner.GetKeepShapeCoef();
            if (!IsRoot() && keepShapeCoef > 0f) {
                accel += m_ADHieCalc.m_Matrix.MultiplyVector(m_KeepShapeAccel) * keepShapeCoef;
                if (IsHasGravity()) {
                    accel += m_Gravity * (1f - keepShapeCoef);
                }
            } else if (IsHasGravity()) {
                accel += m_Gravity;
            }
            if (IsHasAirFriction()) {
                accel -= m_Velocity * m_AirFrictionCoefDivideByMass;
            }
            m_Velocity += accel * fDt;
            float velocitySqr = Vector3.Dot(m_Velocity, m_Velocity);
            if (velocitySqr > MeigeDefs.F_EPSILON6) {
                m_fVelocity = Mathf.Sqrt(velocitySqr);
            } else {
                m_fVelocity = 0f;
                m_Velocity = Vector3.zero;
            }
            m_WorldPosition += m_Velocity * fDt;
        }

        public void Finalyze(float fDtRepci, bool bAfterIKSolve) {
            if (bAfterIKSolve) {
                m_ADHieCalc.MakeMatrix();
                m_WorldPosition = m_ADHieCalc.m_Matrix.GetColumn(3);
            }
            Vector3 dPos = (m_WorldPosition - m_OldWorldPosition) * fDtRepci;
            float dPosSqr = Vector3.Dot(dPos, dPos);
            float dPosMag = dPosSqr <= MeigeDefs.F_EPSILON6 ? 0f : Mathf.Sqrt(dPosSqr);
            if (dPosMag > MeigeDefs.F_EPSILON6) {
                m_fVelocity = Mathf.Min(m_fVelocity, dPosMag);
                m_Velocity = dPos * (m_fVelocity / dPosMag);
            } else {
                m_fVelocity = 0f;
                m_Velocity = Vector3.zero;
            }
            m_OldWorldPosition = m_WorldPosition;
        }

        public void Flush(bool bAnimBlend) {
            MsbHierarchyNode hierarchyNode = GetHierarchyNode();
            hierarchyNode.GetTransform().localRotation = m_ADHieCalc.m_Rotation;
            hierarchyNode.SetScaleOfDistance(m_ADHieCalc.m_ScaleOfDistance);
            if (bAnimBlend) {
                m_WorldPosition = m_ADHieCalc.m_Matrix.GetColumn(3);
                m_OldWorldPosition = m_WorldPosition;
            }
        }

        public string GetHieName() {
            return m_Work.m_HieName;
        }

        public bool IsLock() {
            return m_Work.m_bLock;
        }

        public bool IsRoot() {
            return m_Work.m_bRoot;
        }

        public bool IsHasGravity() {
            return m_Work.m_bGravity;
        }

        public bool IsHasAirFriction() {
            return m_Work.m_bAirFrictiion;
        }

        public bool IsIKLinkStretch() {
            return m_Work.m_bILKinkStretch;
        }

        public void SetLock(bool bLock) {
            m_Work.m_bLock = bLock;
        }

        public void EnableIKLinkStrecth(bool bStretch) {
            m_Work.m_bILKinkStretch = bStretch;
        }

        public Vector3 GetWorldPosition() {
            return m_WorldPosition;
        }

        public void SetWorldPosition(Vector3 vPos) {
            m_WorldPosition = vPos;
        }

        public Vector3 GetOldWorldPosition() {
            return m_OldWorldPosition;
        }

        public void SetOldWorldPosition(Vector3 vPos) {
            m_OldWorldPosition = vPos;
        }

        public Vector3 GetVelocity() {
            return m_Velocity;
        }

        public void SetVelocity(Vector3 vVelocity) {
            m_Velocity = vVelocity;
        }

        public bool IsStable() {
            return Mathf.Abs(m_Velocity.x) <= 0.05f && Mathf.Abs(m_Velocity.y) <= 0.05f && Mathf.Abs(m_Velocity.z) <= 0.05f;
        }

        public MsbArticulatedDynamicsHandler GetOwner() {
            return m_Owner;
        }

        public MsbHierarchyNode GetHierarchyNode() {
            return m_ADHieCalc.m_HierarchyNode;
        }

        public AD_HIE_CALC GetADHieCalc() {
            return m_ADHieCalc;
        }

        public Vector3 GetGravity() {
            return m_Gravity;
        }

        public void SetGravity(ref Vector3 gravity) {
            m_Gravity = gravity;
        }

        public float GetMass() {
            return m_Mass;
        }

        public float GetAirfrictionCoef() {
            return m_AirFrictionCoef;
        }

        public float GetAirfrictionCoefDivideByMass() {
            return m_AirFrictionCoefDivideByMass;
        }

        public int GetNumberOfIKGroup() {
            return m_Work.m_NumOfIKGroup;
        }

        public int GetParentIdxOfADHierarchy() {
            return m_Work.m_ParentIdxOfADHierarchy;
        }

        public void Collided(bool bCollided) {
            m_bCollided = bCollided;
        }

        public bool IsCollided() {
            return m_bCollided;
        }

        public class AD_HIE_CALC {
            public MsbHierarchyNode m_HierarchyNode;
            public AD_HIE_CALC m_HieCalcParent;
            public float m_ScaleOfDistance;
            public Vector3 m_Position;
            public Quaternion m_Rotation;
            public Vector3 m_Scale;
            public Matrix4x4 m_Matrix;

            public void Init(MsbHierarchyNode msbHieNode) {
                m_HierarchyNode = msbHieNode;
                StartFromScratch();
                m_Position = m_HierarchyNode.GetTransform().localPosition;
                m_Rotation = m_HierarchyNode.GetTransform().localRotation;
                m_Scale = m_HierarchyNode.GetTransform().localScale;
            }

            public void StartFromScratch() {
                m_HierarchyNode.Revert();
                m_ScaleOfDistance = 1f;
            }

            public void GetParentMatrix(out Matrix4x4 parentMtx) {
                if (m_HieCalcParent != null) {
                    parentMtx = m_HieCalcParent.GetMatrix();
                } else if (m_HierarchyNode != null) {
                    parentMtx = m_HierarchyNode.GetTransform().parent.localToWorldMatrix;
                } else {
                    parentMtx = Matrix4x4.identity;
                }
            }

            public void MakeMatrix() {
                GetParentMatrix(out Matrix4x4 parentMtx);
                MsbHierarchyNode.EzMatrixCalcFunction(out m_Matrix, ref m_Position, ref m_Rotation, ref m_Scale, m_ScaleOfDistance, parentMtx);
            }

            public Matrix4x4 GetMatrix() {
                return m_Matrix;
            }
        }
    }
}
