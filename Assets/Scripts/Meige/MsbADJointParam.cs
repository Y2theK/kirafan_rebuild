﻿using System;

namespace Meige {
    [Serializable]
    public class MsbADJointParam {
        public string m_HieName;
        public int m_ParentIdxOfADHierarchy;
        public int m_NumOfIKGroup;
        public bool m_bLock;
        public bool m_bRoot;
        public bool m_bGravity;
        public bool m_bAirFrictiion;
        public bool m_bILKinkStretch;

        public MsbADJointParam() {
            m_bILKinkStretch = false;
        }

        public MsbADJointParam(MsbADJointParam src) {
            m_HieName = src.m_HieName;
            m_ParentIdxOfADHierarchy = src.m_ParentIdxOfADHierarchy;
            m_NumOfIKGroup = src.m_NumOfIKGroup;
            m_bLock = src.m_bLock;
            m_bRoot = src.m_bRoot;
            m_bGravity = src.m_bGravity;
            m_bAirFrictiion = src.m_bAirFrictiion;
            m_bILKinkStretch = src.m_bILKinkStretch;
        }
    }
}
