﻿using UnityEngine;

namespace Meige {
    public class MsbADLink {
        private MsbArticulatedDynamicsHandler m_Owner;
        private MsbADLinkParam m_Work;
        private MsbADJoint m_SrcJoint;
        private MsbADJoint m_TgtJoint;
        private float m_InternalForceCoef;
        private float m_LengthForConstraint;

        public void Init(MsbArticulatedDynamicsHandler owner, MsbADLinkParam src) {
            m_Owner = owner;
            m_Work = new MsbADLinkParam(src);
            m_SrcJoint = m_Owner.GetJoint(GetSrcJointIndex());
            m_TgtJoint = m_Owner.GetJoint(GetTgtJointIndex());
            m_LengthForConstraint = GetLength();
            if (m_TgtJoint.GetADHieCalc().m_HieCalcParent == m_SrcJoint.GetADHieCalc()) {
                m_InternalForceCoef = 1f;
                if (GetLinkType() == eADLinkType.eADLinkType_Spring) {
                    m_TgtJoint.EnableIKLinkStrecth(true);
                }
            } else {
                m_InternalForceCoef = m_Owner.GetLinkConstraintForce();
            }
        }

        public void Prepare() {
            Vector3 one = Vector3.one;
            Vector3 trans = m_SrcJoint.GetADHieCalc().m_Matrix.MultiplyVector(one);
            m_LengthForConstraint = GetLength() * (trans.magnitude / one.magnitude);
        }

        public float ConstraintForce(float fDt) {
            eADLinkType linkType = GetLinkType();
            if (linkType == eADLinkType.eADLinkType_Standard) {
                return ConstraintForceStandard();
            }
            if (linkType == eADLinkType.eADLinkType_Spring) {
                return ConstraintForceSpring(fDt);
            }
            return 0f;
        }

        public eADLinkType GetLinkType() {
            return m_Work.m_LinkType;
        }

        public int GetSrcJointIndex() {
            return m_Work.m_SrcADJointIdx;
        }

        public int GetTgtJointIndex() {
            return m_Work.m_DstADJointIdx;
        }

        public float GetLength() {
            return m_Work.m_Length;
        }

        public float GetRadius() {
            return m_Work.m_Radius;
        }

        public MsbADJoint GetSrcJoint() {
            return m_SrcJoint;
        }

        public MsbADJoint GetTgtJoint() {
            return m_TgtJoint;
        }

        public float ConstraintForceStandard() {
            Vector3 srcPos = m_SrcJoint.GetWorldPosition();
            Vector3 tgtPos = m_TgtJoint.GetWorldPosition();
            float srcMass;
            float tgtMass;
            if (m_SrcJoint.IsLock()) {
                srcMass = 1f;
                tgtMass = 0f;
            } else if (m_TgtJoint.IsLock()) {
                srcMass = 0f;
                tgtMass = 1f;
            } else {
                srcMass = m_SrcJoint.GetMass();
                tgtMass = m_TgtJoint.GetMass();
            }

            Vector3 posDiff = tgtPos - srcPos;
            float dist = posDiff.magnitude;
            float force = (dist - m_LengthForConstraint) * m_InternalForceCoef;
            if (force <= 0f) { return 0f; }

            Vector3 dir = posDiff / dist;
            float forceCoeff = force / (srcMass + tgtMass);
            m_SrcJoint.SetWorldPosition(srcPos + (forceCoeff * tgtMass * dir));
            m_TgtJoint.SetWorldPosition(tgtPos - (forceCoeff * srcMass * dir));
            return force;
        }

        public float ConstraintForceSpring(float fDt) {
            Vector3 srcPos = m_SrcJoint.GetWorldPosition();
            Vector3 tgtPos = m_TgtJoint.GetWorldPosition();
            float srcMass;
            float tgtMass;
            if (m_SrcJoint.IsLock()) {
                srcMass = 1f;
                tgtMass = 0f;
            } else if (m_TgtJoint.IsLock()) {
                srcMass = 0f;
                tgtMass = 1f;
            } else {
                srcMass = m_SrcJoint.GetMass();
                tgtMass = m_TgtJoint.GetMass();
            }

            Vector3 posDiff = tgtPos - srcPos;
            float dist = posDiff.magnitude;
            float force = dist - m_LengthForConstraint;
            if (force <= MeigeDefs.F_EPSILON6) { return 0f; }

            Vector3 dir = posDiff / dist;
            float totalMass = srcMass + tgtMass;
            float instantForce = (force * m_InternalForceCoef / totalMass) * fDt;
            m_SrcJoint.SetWorldPosition(srcPos + (dir * (instantForce * tgtMass / totalMass)) * fDt);
            m_TgtJoint.SetWorldPosition(tgtPos - (dir * (instantForce * srcMass / totalMass)) * fDt);
            return instantForce;
        }
    }
}
