﻿using System;

namespace Meige {
    [Serializable]
    public class MsbADLinkParam {
        public int m_SrcADJointIdx;
        public int m_DstADJointIdx;
        public float m_Radius;
        public float m_Length;
        public eADLinkType m_LinkType;

        public MsbADLinkParam() { }

        public MsbADLinkParam(MsbADLinkParam src) {
            m_SrcADJointIdx = src.m_SrcADJointIdx;
            m_DstADJointIdx = src.m_DstADJointIdx;
            m_Radius = src.m_Radius;
            m_Length = src.m_Length;
            m_LinkType = src.m_LinkType;
        }
    }
}
