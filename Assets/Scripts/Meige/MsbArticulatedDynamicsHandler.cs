﻿using System;
using System.Linq;
using UnityEngine;

namespace Meige {
    [Serializable]
	public class MsbArticulatedDynamicsHandler {
        private MsbADJoint[] m_MsbADJointArray;
        private MsbADLink[] m_MsbADLinkArray;
        private MsbHandler m_Owner;
        private MsbADParam m_Work;
        private Vector3[] m_OldRootPosition;
        private int[] m_RootIndexArray;
        private int m_DiscardTranslateFlags;
        private bool m_bStartFromScratch;
        private eLimitationAngleMode m_LimitationAngleMode;

        public string m_Name;
        public MsbADParam m_Src;
        public MsbADJointParam[] m_SrcMsbADJointParamArray;
        public MsbADLinkParam[] m_SrcMsbADLinkParamArray;

        public void Init(MsbHandler msbHandler) {
            m_Owner = msbHandler;
            m_Work = new MsbADParam(m_Src);
            m_OldRootPosition = new Vector3[m_Work.m_RootJntNum];
            m_RootIndexArray = new int[m_Work.m_RootJntNum];
            m_DiscardTranslateFlags = 0;
            m_bStartFromScratch = true;
            m_LimitationAngleMode = eLimitationAngleMode.eLimitationAngleMode_Standard;
            m_MsbADJointArray = new MsbADJoint[m_SrcMsbADJointParamArray.Count()];
            s32 rootIdx = 0;
            for (int i = 0; i < m_SrcMsbADJointParamArray.Count(); i++) {
                MsbADJoint msbADJoint = new MsbADJoint();
                msbADJoint.Init(this, m_SrcMsbADJointParamArray[i]);
                if (msbADJoint.IsRoot()) {
                    m_RootIndexArray[rootIdx] = i;
                    m_OldRootPosition[rootIdx] = Vector3.zero;
                    rootIdx++;
                }
                m_MsbADJointArray[i] = msbADJoint;
            }
            foreach (MsbADJoint msbADJoint in m_MsbADJointArray) {
                MsbADJoint.AD_HIE_CALC adhieCalc = msbADJoint.GetADHieCalc();
                int parentIdxOfADHierarchy = msbADJoint.GetParentIdxOfADHierarchy();
                if (parentIdxOfADHierarchy != -1) {
                    msbADJoint.SetParent(m_MsbADJointArray[parentIdxOfADHierarchy]);
                }
                adhieCalc.MakeMatrix();
                msbADJoint.SetWorldPosition(adhieCalc.m_Matrix.GetColumn(3));
                msbADJoint.SetOldWorldPosition(adhieCalc.m_Matrix.GetColumn(3));
            }
            m_MsbADLinkArray = new MsbADLink[m_SrcMsbADLinkParamArray.Count()];
            for (int i = 0; i < m_SrcMsbADLinkParamArray.Count(); i++) {
                MsbADLink msbADLink = new MsbADLink();
                msbADLink.Init(this, m_SrcMsbADLinkParamArray[i]);
                m_MsbADLinkArray[i] = msbADLink;
            }
        }

		public MsbHandler GetOwner() {
            return m_Owner;
        }

		public int GetJointNum() {
            return m_MsbADJointArray != null ? m_MsbADJointArray.Count() : 0;
        }

		public MsbADJoint GetJoint(int index) {
            return m_MsbADJointArray[index];
        }

		public int GetLinkNum() {
            return m_MsbADLinkArray != null ? m_MsbADLinkArray.Count() : 0;
        }

		public MsbADLink GetLink(int index) {
            return m_MsbADLinkArray[index];
        }

		public int GetRootNum() {
            return m_RootIndexArray != null ? m_RootIndexArray.Count() : 0;
        }

		public MsbADJoint GetRoot(int index) {
            return GetJoint(m_RootIndexArray[index]);
        }

		public float GetAnimBlendRatio() {
            return m_Work.m_AnimBlendRatio;
        }

		public void SetAnimBlendRatio(float ratio) {
            m_Work.m_AnimBlendRatio = ratio;
        }

		public float GetKeepShapeCoef() {
            return m_Work.m_KeepShapeCoef;
        }

		public void SetKeepShapeCoef(float coef) {
            m_Work.m_KeepShapeCoef = coef;
        }

		public float GetLinkConstraintForce() {
            return m_Work.m_LinkConstraintForce;
        }

		public float GetLimitationAngle() {
            return m_Work.m_LimitationAngle;
        }

		public void SetDiscardTranslate(bool bX, bool bY, bool bZ) {
            m_DiscardTranslateFlags = (bX ? 1 : 0) | (bY ? 2 : 0) | (bZ ? 4 : 0);
        }

		public bool IsDiscardTranslateX() {
            return (m_DiscardTranslateFlags & 1) != 0;
        }

		public bool IsDiscardTranslateY() {
            return (m_DiscardTranslateFlags & 2) != 0;
        }

		public bool IsDiscardTranslateZ() {
            return (m_DiscardTranslateFlags & 4) != 0;
        }

		public void SetLimitationAngleMode(eLimitationAngleMode limitAngleMode) {
            m_LimitationAngleMode = limitAngleMode;
        }

		public eLimitationAngleMode GetLimitationAngleMode() {
            return m_LimitationAngleMode;
        }

		public float GetAirFrictionCoef() {
            return m_Work.m_AirFrictionCoef;
        }

		public string GetName() {
            return m_Name;
        }

		public int GetRefID() {
            return m_Work.m_RefID;
        }

		private bool IsAnimationBlend() {
            return GetAnimBlendRatio() > MeigeDefs.F_EPSILON6;
        }

		public void CalculateHierarchyMatrix() {
            for (int i = 0; i < GetJointNum(); i++) {
                MsbADJoint joint = GetJoint(i);
                if (m_bStartFromScratch) {
                    joint.StartFromScratch();
                }
            }
            if (m_bStartFromScratch) {
                for (int i = 0; i < GetRootNum(); i++) {
                    MsbADJoint root = GetRoot(i);
                    m_OldRootPosition[i] = root.GetHierarchyNode().transform.position;
                }
            }
        }

		public void DoWork()
		{
			//Why are these here?
            int ikSteps = 2;
            int jointSteps = 1;
            int linkConstraintForce = 1;
            float fDt = 0.016666668f;
            bool blend = false;

            for (int i = 0; i < ikSteps; i++) {
                Prepare();
                for (int j = 0; j < jointSteps; j++) {
                    for (int k = 0; k < GetJointNum(); k++) {
                        MsbADJoint joint = GetJoint(k);
                        joint.ExternalForce(fDt);
                    }
                    LinkConstraintForce(fDt, linkConstraintForce);
                }
                IKSolve(false, blend);
                Finalyze(1f / fDt, true);
                m_bStartFromScratch = false;
            }
            for (int i = 0; i < GetRootNum(); i++) {
                MsbADJoint root = GetRoot(i);
                m_OldRootPosition[i] = root.GetWorldPosition();
            }
        }

		private void Prepare() {
            for (int i = 0; i < GetRootNum(); i++) {
                MsbADJoint root = GetRoot(i);
                Vector3 discard = Vector3.zero;
                root.Prepare(discard, m_bStartFromScratch);
                discard = root.GetHierarchyNode().GetTransform().position - m_OldRootPosition[i];
                if (!IsDiscardTranslateX()) { discard.x = 0f; }
                if (!IsDiscardTranslateY()) { discard.y = 0f; }
                if (!IsDiscardTranslateZ()) { discard.z = 0f; }
                for (int j = 1; j < root.GetNumberOfIKGroup(); j++) {
                    MsbADJoint joint = GetJoint(m_RootIndexArray[i] + j);
                    joint.Prepare(discard, m_bStartFromScratch);
                }
            }
            for (int i = 0; i < GetLinkNum(); i++) {
                MsbADLink link = GetLink(i);
                link.Prepare();
            }
        }

		private void LinkConstraintForce(float fDt, int linkConstraintForceRep) {
            int rep = 0;
            do {
                bool changed = false;
                for (int i = 0; i < GetLinkNum(); i++) {
                    MsbADLink link = GetLink(i);
                    MsbADJoint srcJoint = link.GetSrcJoint();
                    MsbADJoint tgtJoint = link.GetTgtJoint();
                    if (!srcJoint.IsLock() || !tgtJoint.IsLock()) {
                        if (link.ConstraintForce(fDt) >= MeigeDefs.F_EPSILON6) {
                            changed = true;
                        }
                    }
                }
                if (linkConstraintForceRep <= 0) { break; }
                if (!changed) { break; }
                for (int j = GetLinkNum() - 2; j >= 0; j--) {
                    MsbADLink link = GetLink(j);
                    MsbADJoint srcJoint = link.GetSrcJoint();
                    MsbADJoint tgtJoint = link.GetTgtJoint();
                    if (!srcJoint.IsLock() || !tgtJoint.IsLock()) {
                        if (link.ConstraintForce(fDt) >= MeigeDefs.F_EPSILON6) {
                            changed = true;
                        }
                    }
                }
                if (!changed) { break; }
            }
            while (++rep < linkConstraintForceRep);
        }

		private void Finalyze(float fDtRepci, bool bAfterIKSolve) {
            for (int i = 0; i < GetJointNum(); i++) {
                MsbADJoint joint = GetJoint(i);
                joint.Finalyze(fDtRepci, bAfterIKSolve);
            }
        }

		private void IKSolve(bool bEnableIKLinkStretch, bool bAnimBlend) {
            for (int i = 0; i < GetRootNum(); i++) {
                MsbADJoint root = GetRoot(i);
                root.GetADHieCalc().MakeMatrix();
                for (int j = 1; j < root.GetNumberOfIKGroup(); j++) {
                    MsbADJoint joint = GetJoint(m_RootIndexArray[i] + j);
                    MsbADJoint parent = GetJoint(joint.GetParentIdxOfADHierarchy());
                    MsbADJoint.AD_HIE_CALC adhieCalc = joint.GetADHieCalc();
                    MsbADJoint.AD_HIE_CALC adhieCalcParent = parent.GetADHieCalc();
                    adhieCalc.MakeMatrix();
                    Vector3 jointPos = adhieCalc.m_Matrix.GetColumn(3);
                    Vector3 parentPos = adhieCalcParent.m_Matrix.GetColumn(3);
                    Vector3 worldPosition = joint.GetWorldPosition();
                    Vector3 toJointLocal = jointPos - parentPos;
                    Vector3 toJointWorld = worldPosition - parentPos;
                    float localMag = toJointLocal.magnitude;
                    float worldMag = toJointWorld.magnitude;
                    if (!float.IsNaN(localMag) && !float.IsNaN(worldMag)) {
                        if (localMag > 0f && worldMag > 0f) {
                            Vector3 localDir = toJointLocal / localMag;
                            Vector3 worldDir = toJointWorld / worldMag;
                            float angleF = Vector3.Dot(localDir, worldDir);
                            if (angleF >= 0.9999f) {
                                if (bEnableIKLinkStretch || joint.IsIKLinkStretch()) {
                                    adhieCalc.m_ScaleOfDistance = worldMag / localMag;
                                    adhieCalc.MakeMatrix();
                                }
                            } else {
                                Vector3 vector = Vector3.Cross(localDir, worldDir);
                                if (Mathf.Abs(vector.x) <= 0f && Mathf.Abs(vector.y) <= 0f && Mathf.Abs(vector.z) <= 0f) {
                                    if (bEnableIKLinkStretch || joint.IsIKLinkStretch()) {
                                        adhieCalc.m_ScaleOfDistance = worldMag / localMag;
                                        adhieCalc.MakeMatrix();
                                    }
                                } else {
                                    vector = Matrix4x4.Inverse(adhieCalcParent.m_Matrix).MultiplyVector(vector);
                                    vector.Normalize();
                                    float angle = Mathf.Acos(angleF);
                                    if (!joint.IsCollided()) {
                                        float angleAbs = Mathf.Abs(angle);
                                        if (angleAbs > GetLimitationAngle()) {
                                            float newAngle;
                                            switch (GetLimitationAngleMode()) {
                                                case eLimitationAngleMode.eLimitationAngleMode_Disable:
                                                    newAngle = Mathf.Lerp(GetLimitationAngle(), angleAbs, 0.5f);
                                                    break;
                                                case eLimitationAngleMode.eLimitationAngleMode_ExceptCollide:
                                                    newAngle = joint.IsCollided() ? Mathf.Min(angleAbs, 0.7853982f) : GetLimitationAngle();
                                                    break;
                                                case eLimitationAngleMode.eLimitationAngleMode_Standard:
                                                default:
                                                    newAngle = GetLimitationAngle();
                                                    break;
                                            }
                                            if (angle < 0f) {
                                                angle = -newAngle;
                                            } else {
                                                angle = newAngle;
                                            }
                                        }
                                    }
                                    Quaternion rotation = Quaternion.AngleAxis(Mathf.Rad2Deg * angle, vector);
                                    if (bAnimBlend) {
                                        rotation = Quaternion.Slerp(rotation, adhieCalcParent.m_HierarchyNode.GetTransform().localRotation, GetAnimBlendRatio());
                                    }
                                    adhieCalcParent.m_Rotation *= rotation;
                                    adhieCalcParent.MakeMatrix();
                                    if (bEnableIKLinkStretch || joint.IsIKLinkStretch()) {
                                        adhieCalc.m_ScaleOfDistance = worldMag / localMag;
                                    }
                                    adhieCalc.MakeMatrix();
                                }
                            }
                        }
                    }
                }
            }
        }

		public void Flush() {
            bool bAnimBlend = IsAnimationBlend();
            for (int i = 0; i < GetJointNum(); i++) {
                MsbADJoint joint = GetJoint(i);
                joint.Flush(bAnimBlend);
            }
            m_bStartFromScratch = false;
        }

        [Serializable]
		public class MsbADParam {
            public float m_AnimBlendRatio;
            public float m_KeepShapeCoef;
            public float m_LinkConstraintForce;
            public float m_LimitationAngle;
            public float m_AirFrictionCoef;
            public int m_JntNum;
            public int m_LinkNum;
            public int m_RootJntNum;
            public int m_HierachyDepth;
            public int m_RefID;
            public int m_CollisionNum;

            public MsbADParam() { }

			public MsbADParam(MsbADParam src) {
                m_AnimBlendRatio = src.m_AnimBlendRatio;
                m_KeepShapeCoef = src.m_KeepShapeCoef;
                m_LinkConstraintForce = src.m_LinkConstraintForce;
                m_LimitationAngle = src.m_LimitationAngle;
                m_AirFrictionCoef = src.m_AirFrictionCoef;
                m_JntNum = src.m_JntNum;
                m_LinkNum = src.m_LinkNum;
                m_RootJntNum = src.m_RootJntNum;
                m_HierachyDepth = src.m_HierachyDepth;
                m_RefID = src.m_RefID;
                m_CollisionNum = src.m_CollisionNum;
            }
		}

		public enum eLimitationAngleMode
		{
			eLimitationAngleMode_Disable,
			eLimitationAngleMode_Standard,
			eLimitationAngleMode_ExceptCollide
		}
	}
}
