﻿using System;
using UnityEngine;

namespace Meige {
    public class MsbHierarchyNode : MonoBehaviour {
        public Vector3 m_Distance;
        public int m_RefID;
        public MsbHierarchyNodeAttr m_Src;
        private Transform m_Transform;
        private float m_ScaleOfDistance = 1f;

        public Transform GetTransform() {
            return m_Transform;
        }

        public void SetScaleOfDistance(float scaleOfDist) {
            m_ScaleOfDistance = scaleOfDist;
        }

        public float GetScaleOfDistance() {
            return m_ScaleOfDistance;
        }

        public void Revert() {
            Vector3 distance = m_Distance;
            distance.x *= -1f;
            m_Transform.localPosition = distance;
            m_Transform.localRotation = Quaternion.identity;
            m_Transform.localScale = Vector3.one;
        }

        public void Init() {
            m_Transform = transform;
        }

        public static void EzMatrixCalcFunction(out Matrix4x4 outMtx, ref Vector3 position, ref Quaternion rot, ref Vector3 scale, float scaleOfDistance, Matrix4x4 parentMtx) {
            Vector3 pos = position * scaleOfDistance;
            Matrix4x4 rhs = Matrix4x4.TRS(pos, rot, scale);
            outMtx = parentMtx * rhs;
        }

        [Serializable]
        public class MsbHierarchyNodeAttr {
            public bool m_bSegmentScale;
            public bool m_bSkeleton;
            public bool m_bJustTransAffectedByParent;
            public bool m_bLinkConstraint;

            public MsbHierarchyNodeAttr() {
                m_bLinkConstraint = false;
            }

            public MsbHierarchyNodeAttr(MsbHierarchyNodeAttr src) {
                m_bSegmentScale = src.m_bSegmentScale;
                m_bSkeleton = src.m_bSkeleton;
                m_bJustTransAffectedByParent = src.m_bJustTransAffectedByParent;
                m_bLinkConstraint = src.m_bLinkConstraint;
            }
        }
    }
}
