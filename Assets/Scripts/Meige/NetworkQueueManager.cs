﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Meige {
    public class NetworkQueueManager : SingletonMonoBehaviour<NetworkQueueManager> {
        private List<MeigewwwParam> m_wwwParams;
        private int m_barrierJobFlagMask;
        private int[] m_doingJobCounter;
        private Action<MeigewwwParam> m_RecvSystemCallback;

        public static bool IsLogin => !string.IsNullOrEmpty(MeigeWWW.SessionId);
        public int totalJobCount => m_wwwParams.Where(p => p.GetStatus() == MeigewwwParam.eStatus.Progress).Count();

        public static void Request(MeigewwwParam wwwParam) {
            Instance.RequestParam(wwwParam);
        }

        public static void Done(MeigewwwParam wwwParam) {
            Instance.DoneJob(wwwParam);
        }

        public static void SetSessionId(string sessionId) {
            MeigeWWW.SessionId = sessionId;
        }

        public static string GetSessionId() {
            return MeigeWWW.SessionId;
        }

        public static void SetAppVersion(string version) {
            MeigeWWW.AppVersion = version;
        }

        public static void SetTimeoutSec(int timeoutSec) {
            MeigeWWW.TimeoutSec = timeoutSec;
        }

        public static void SetRelease(bool release) {
            MeigeWWW.IsRelease = release;
        }

        public static int TotalJobCounter() {
            return Instance.totalJobCount;
        }

        protected new void Awake() {
            m_wwwParams = new List<MeigewwwParam>();
            m_barrierJobFlagMask = 0;
            m_doingJobCounter = new int[16];
            MeigeWWW.SetUp();
        }

        public void ClearRequest() {
            m_wwwParams.Clear();
            m_barrierJobFlagMask = 0;
            for (int i = 0; i < MeigeDefs.WWW_QUEUE_MASK_BIT_FLAG_NUM; i++) {
                m_doingJobCounter[i] = 0;
            }
        }

        public void Update() {
            if (m_wwwParams == null) { return; }
            for (int i = m_wwwParams.Count - 1; i >= 0; i--) {
                if (m_wwwParams[i].IsDone()) {
                    m_wwwParams.RemoveAt(i);
                }
            }
            if (totalJobCount >= MeigeDefs.WWW_MAX_CONCURRENT_CONNECTION) { return; }
            MeigewwwParam param = null;
            int maskFlag = 0;
            foreach (MeigewwwParam tempParam in m_wwwParams) {
                if (tempParam.GetStatus() != MeigewwwParam.eStatus.Progress) {
                    maskFlag = tempParam.GetMaskFlag();
                    if (maskFlag == 0 || (m_barrierJobFlagMask & maskFlag) == 0) {
                        param = tempParam;
                        break;
                    }
                }
            }
            if (param == null) { return; }
            m_barrierJobFlagMask |= maskFlag;
            for (int i = 0; i < MeigeDefs.WWW_QUEUE_MASK_BIT_FLAG_NUM; i++) {
                if ((maskFlag & 1 << i) != 0) {
                    m_doingJobCounter[i]++;
                }
            }
            param.SetStatus(MeigewwwParam.eStatus.Progress);
            MeigeWWW meigeWWW = new MeigeWWW(param);
            StartCoroutine(meigeWWW.Kick());
        }

        private void DoneJob(MeigewwwParam wwwRequest) {
            wwwRequest.SetStatus(MeigewwwParam.eStatus.Done);
            int maskFlag = wwwRequest.GetMaskFlag();
            if (maskFlag != 0) {
                for (int i = 0; i < MeigeDefs.WWW_QUEUE_MASK_BIT_FLAG_NUM; i++) {
                    int flag = 1 << i;
                    if ((maskFlag & flag) != 0) {
                        m_doingJobCounter[i]--;
                        if (m_doingJobCounter[i] <= 0 && (m_barrierJobFlagMask & flag) != 0) {
                            m_barrierJobFlagMask &= ~flag;
                        }
                    }
                }
            }
            if (m_RecvSystemCallback != null) {
                m_RecvSystemCallback(wwwRequest);
            }
        }

        private void RequestParam(MeigewwwParam wwwParam) {
            wwwParam.SetStatus(MeigewwwParam.eStatus.Wait);
            m_wwwParams.Add(wwwParam);
        }

        public static string escapeSpecialChars(string text) {
            if (string.IsNullOrEmpty(text)) { return text; }
            return text.Replace("\\", "\\\\").Replace("\r\n", "\\n").Replace("\r", "\\n").Replace("\n", "\\n").Replace("\"", "\\\"").Replace("/", "\\/").Replace("\b", "\\b").Replace("\f", "\\f").Replace("\t", "\\t");
        }

        public static void SetSystemRecvCallback(Action<MeigewwwParam> callbacks) {
            Instance.m_RecvSystemCallback = callbacks;
        }
    }
}
