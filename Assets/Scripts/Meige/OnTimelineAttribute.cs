﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200125E RID: 4702
	[Token(Token = "0x2000C4C")]
	[Attribute(Name = "AttributeUsageAttribute", RVA = "0x10011D590", Offset = "0x11D590")]
	[StructLayout(3)]
	public class OnTimelineAttribute : Attribute
	{
		// Token: 0x06005F5A RID: 24410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005845")]
		[Address(RVA = "0x1014BD1BC", Offset = "0x14BD1BC", VA = "0x1014BD1BC")]
		public OnTimelineAttribute(string name, string affiliation)
		{
		}

		// Token: 0x04006F06 RID: 28422
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004EC3")]
		public string name;

		// Token: 0x04006F07 RID: 28423
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004EC4")]
		public string affiliation;

		// Token: 0x04006F08 RID: 28424
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004EC5")]
		public int id;
	}
}
