﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200130E RID: 4878
	[Token(Token = "0x2000CAC")]
	[StructLayout(3)]
	public class PDBUtility
	{
		// Token: 0x06006344 RID: 25412 RVA: 0x00020E68 File Offset: 0x0001F068
		[Token(Token = "0x6005B3F")]
		[Address(RVA = "0x1014BD1F4", Offset = "0x14BD1F4", VA = "0x1014BD1F4")]
		public static uint GetRand(uint s)
		{
			return 0U;
		}

		// Token: 0x06006345 RID: 25413 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005B40")]
		[Address(RVA = "0x1014BD20C", Offset = "0x14BD20C", VA = "0x1014BD20C")]
		public static string Serialize(byte[] binaryData)
		{
			return null;
		}

		// Token: 0x06006346 RID: 25414 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005B41")]
		[Address(RVA = "0x1014BD3E8", Offset = "0x14BD3E8", VA = "0x1014BD3E8")]
		public static byte[] SerializeBytes(byte[] binaryData)
		{
			return null;
		}

		// Token: 0x06006347 RID: 25415 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005B42")]
		[Address(RVA = "0x1014BD594", Offset = "0x14BD594", VA = "0x1014BD594")]
		public static byte[] Load(string path)
		{
			return null;
		}

		// Token: 0x06006348 RID: 25416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005B43")]
		[Address(RVA = "0x1014BD80C", Offset = "0x14BD80C", VA = "0x1014BD80C")]
		public PDBUtility()
		{
		}
	}
}
