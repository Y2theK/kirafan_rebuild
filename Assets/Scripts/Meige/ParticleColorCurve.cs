﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001271 RID: 4721
	[Token(Token = "0x2000C5F")]
	[Serializable]
	[StructLayout(3)]
	public class ParticleColorCurve
	{
		// Token: 0x06005FB4 RID: 24500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600589F")]
		[Address(RVA = "0x1014BD814", Offset = "0x14BD814", VA = "0x1014BD814")]
		public ParticleColorCurve()
		{
		}

		// Token: 0x04006F16 RID: 28438
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004ED3")]
		public Color[] m_Color;
	}
}
