﻿using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public class ParticleRule {
        public int m_particleNum = 1;
        public ParticleTypeParam m_particleTypeParam = new ParticleTypeParam();
        public eParticleType m_particleType;
        public EmitionParam m_emitionParam = new EmitionParam();
        public eParticleEmitionType m_emitionType;
        public Rect m_uvRect_TopBlock;
        public int m_uvBlockNum;
        public eParticleLifeScaleType m_LifeScaleType;
        public RangePareFloat m_lifeScaleRange = new RangePareFloat();
        public RangePareFloat m_speedRange = new RangePareFloat();
        public Vector3 m_gravityDir = new Vector3(0f, -1f, 0f);
        public RangePareFloat m_gravityForceRange = new RangePareFloat();
        public eParticleLifeSpanType m_lifeSpanType;
        public eParticleLifeSpanAlpha m_lifeSpanAlpha;
        public LifeSpanParam m_LifeSpanParam = new LifeSpanParam();
        public eParticleCollisionType m_collisionType;
        public CollisionTypeParam m_collisionParam = new CollisionTypeParam();
        public float m_incidentNumberPerSec;
        public float m_incidentRandomLevel;
        public float m_HDR_Factor = 1f;
        public float m_AlphaScale = 1f;
        public float m_frameSpeedScale = 1f;
        public bool m_usingLight;
        public bool m_isLocalTrans;
        public bool m_isRandomEmitDir;
        public bool m_UsingAccelerationFlg;
        public AccelerationComponent m_AccelerationComponent;
        public bool m_UsingRotationFlg;
        public RotationComponent m_RotationComponent;
        public bool m_UsingUVAnimationFlg;
        public UVAnimationComponent m_UVAnimationComponent;
        public bool m_UsingColorCurveFlg;
        public ColorCurveComponent m_ColorCurveComponent;
        public bool m_UsingTailFlg;
        public TailComponent m_TailComponent;
        public bool m_UsingBlinkFlg;
        public BlinkComponent m_BlinkComponent;
        public bool m_UsingPathMoveFlg;
        public PathMoveComponent m_PathMoveComponent;

        public int GetPropertyNum() {
            return (int)ePropertyID.Max;
        }

        public int GetArrayNum(int propertyIdx) {
            if (propertyIdx == (int)ePropertyID.Particle_ColorCurveColorCurve) {
                if (m_ColorCurveComponent != null && m_ColorCurveComponent.m_pColorCurveArray != null) {
                    return m_ColorCurveComponent.m_pColorCurveArray.Length;
                }
                return 0;
            }
            if (propertyIdx == (int)ePropertyID.Particle_PathMoveFocusRadiusRange) {
                if (m_PathMoveComponent != null && m_PathMoveComponent.m_FocusRadiusRange != null) {
                    return m_PathMoveComponent.m_FocusRadiusRange.Length;
                }
                return 0;
            }
            return 1;
        }

        public object GetValue(int propertyIdx, int arrayIdx) {
            return (ePropertyID)propertyIdx switch {
                ePropertyID.Particle_TypeParamBillboardWidthRange => m_particleTypeParam.m_billboard.m_widthRange,
                ePropertyID.Particle_TypeParamBillboardHeightRange => m_particleTypeParam.m_billboard.m_heightRange,
                ePropertyID.Particle_TypeParamPointSizeRange => m_particleTypeParam.m_point.m_sizeRange,
                ePropertyID.Particle_TypeParamLineWidth => m_particleTypeParam.m_line.m_width,
                ePropertyID.Particle_TypeParamPolyLineTopWidthRange => m_particleTypeParam.m_polyLine.m_topWidthRange,
                ePropertyID.Particle_TypeParamPolyLineEndWidthRange => m_particleTypeParam.m_polyLine.m_endWidthRange,
                ePropertyID.Particle_TypeParamConfettiWidthRange => m_particleTypeParam.m_confetti.m_widthRange,
                ePropertyID.Particle_TypeParamConfettiHeightRange => m_particleTypeParam.m_confetti.m_heightRange,
                ePropertyID.Particle_EmitParamPointAngleRange => m_emitionParam.m_point.m_angleRange,
                ePropertyID.Particle_EmitParamBoxWidthRange => m_emitionParam.m_box.m_widthRange,
                ePropertyID.Particle_EmitParamBoxHeightRange => m_emitionParam.m_box.m_heightRange,
                ePropertyID.Particle_EmitParamBoxDepthRange => m_emitionParam.m_box.m_depthRange,
                ePropertyID.Particle_EmitParamPlaneQuadWidthRange => m_emitionParam.m_planeQuad.m_widthRange,
                ePropertyID.Particle_EmitParamPlaneQuadHeightRange => m_emitionParam.m_planeQuad.m_heightRange,
                ePropertyID.Particle_EmitParamPlaneCircleRadiusRange => m_emitionParam.m_planeCircle.m_radiusRange,
                ePropertyID.Particle_EmitParamSphereAngleRange => m_emitionParam.m_sphere.m_angleRange,
                ePropertyID.Particle_EmitParamSphereRadiusRange => m_emitionParam.m_sphere.m_radiusRange,
                ePropertyID.Particle_EmitParamTorusAngleRange => m_emitionParam.m_torus.m_angleRange,
                ePropertyID.Particle_EmitParamTorusBigRadius => m_emitionParam.m_torus.m_bigRadiusRange,
                ePropertyID.Particle_EmitParamTorusSmallRadiusRange => m_emitionParam.m_torus.m_smallRadiusRange,
                ePropertyID.Particle_EmitParamCylinderRadiusRange => m_emitionParam.m_cylinder.m_RadiusRange,
                ePropertyID.Particle_EmitParamCylinderHeightRange => m_emitionParam.m_cylinder.m_HeightRange,
                ePropertyID.Particle_GravityDir => m_gravityDir,
                ePropertyID.Particle_GravityForceRange => m_gravityForceRange,
                ePropertyID.Particle_LifeSpanType => m_lifeSpanType,
                ePropertyID.Particle_LifeSpanAlpha => m_lifeSpanAlpha,
                ePropertyID.Particle_LifeSpanTimeRange => m_LifeSpanParam.m_Time.m_lifeSpanSecRange,
                ePropertyID.Particle_LifeSpanDistanceMaxRange => m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange,
                ePropertyID.Particle_LifeSpanHeightRange => m_LifeSpanParam.m_Height.m_lifeSpanHeightRange,
                ePropertyID.Particle_IncidentNumberPerSec => m_incidentNumberPerSec,
                ePropertyID.Particle_IncidentRandomLevel => m_incidentRandomLevel,
                ePropertyID.Particle_HDRFactor => m_HDR_Factor,
                ePropertyID.Particle_IsLocalTrans => m_isLocalTrans,
                ePropertyID.Particle_IsRandomEmitDir => m_isRandomEmitDir,
                ePropertyID.Particle_AccelerationAccelerationRange => m_AccelerationComponent.m_accelerationRange,
                ePropertyID.Particle_AccelerationDragForceRange => m_AccelerationComponent.m_dragForceRange,
                ePropertyID.Particle_RotationRotSpeedRange => m_RotationComponent.m_rotSpeedRange,
                ePropertyID.Particle_RotationRotAccelerationRange => m_RotationComponent.m_rotAccelerationRange,
                ePropertyID.Particle_RotationRotDragForceRange => m_RotationComponent.m_rotDragForceRange,
                ePropertyID.Particle_RotationRotAnchorOffsetRange => m_RotationComponent.m_rotAnchorOffsetRange,
                ePropertyID.Particle_RotationRotRange => m_RotationComponent.m_rotRange,
                ePropertyID.Particle_ColorCurveColorCurve => m_ColorCurveComponent.m_pColorCurveArray[arrayIdx],
                ePropertyID.Particle_BlinkBlinkSpanRange => m_BlinkComponent.m_blinkSpanSecRange,
                ePropertyID.Particle_PathMoveFocusRadiusRange => m_PathMoveComponent.m_FocusRadiusRange[arrayIdx],
                _ => null,
            };
        }

        public void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, object value) {
            switch ((ePropertyID)propertyIdx) {
                case ePropertyID.Particle_TypeParamBillboardWidthRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_billboard.m_widthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamBillboardHeightRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_billboard.m_heightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPointSizeRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_point.m_sizeRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamLineWidth:
                    EffectHelper.Copy(ref m_particleTypeParam.m_line.m_width, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPolyLineTopWidthRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_polyLine.m_topWidthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPolyLineEndWidthRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_polyLine.m_endWidthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamConfettiWidthRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_confetti.m_widthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamConfettiHeightRange:
                    EffectHelper.Copy(ref m_particleTypeParam.m_confetti.m_heightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPointAngleRange:
                    EffectHelper.Copy(ref m_emitionParam.m_point.m_angleRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxWidthRange:
                    EffectHelper.Copy(ref m_emitionParam.m_box.m_widthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxHeightRange:
                    EffectHelper.Copy(ref m_emitionParam.m_box.m_heightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxDepthRange:
                    EffectHelper.Copy(ref m_emitionParam.m_box.m_depthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneQuadWidthRange:
                    EffectHelper.Copy(ref m_emitionParam.m_planeQuad.m_widthRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneQuadHeightRange:
                    EffectHelper.Copy(ref m_emitionParam.m_planeQuad.m_heightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneCircleRadiusRange:
                    EffectHelper.Copy(ref m_emitionParam.m_planeCircle.m_radiusRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamSphereAngleRange:
                    EffectHelper.Copy(ref m_emitionParam.m_sphere.m_angleRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamSphereRadiusRange:
                    EffectHelper.Copy(ref m_emitionParam.m_sphere.m_radiusRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusAngleRange:
                    EffectHelper.Copy(ref m_emitionParam.m_torus.m_angleRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusBigRadius:
                    EffectHelper.Copy(ref m_emitionParam.m_torus.m_bigRadiusRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusSmallRadiusRange:
                    EffectHelper.Copy(ref m_emitionParam.m_torus.m_smallRadiusRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamCylinderRadiusRange:
                    EffectHelper.Copy(ref m_emitionParam.m_cylinder.m_RadiusRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamCylinderHeightRange:
                    EffectHelper.Copy(ref m_emitionParam.m_cylinder.m_HeightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_GravityDir:
                    EffectHelper.Copy(ref m_gravityDir, tgtIdx, value);
                    break;
                case ePropertyID.Particle_GravityForceRange:
                    EffectHelper.Copy(ref m_gravityForceRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanType:
                    int type = (int)m_lifeSpanType;
                    EffectHelper.Copy(ref type, tgtIdx, value);
                    m_lifeSpanType = (eParticleLifeSpanType)type;
                    break;
                case ePropertyID.Particle_LifeSpanAlpha:
                    int alpha = (int)m_lifeSpanAlpha;
                    EffectHelper.Copy(ref alpha, tgtIdx, value);
                    m_lifeSpanAlpha = (eParticleLifeSpanAlpha)alpha;
                    break;
                case ePropertyID.Particle_LifeSpanTimeRange:
                    EffectHelper.Copy(ref m_LifeSpanParam.m_Time.m_lifeSpanSecRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanDistanceMaxRange:
                    EffectHelper.Copy(ref m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanHeightRange:
                    EffectHelper.Copy(ref m_LifeSpanParam.m_Height.m_lifeSpanHeightRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_IncidentNumberPerSec:
                    EffectHelper.Copy(ref m_incidentNumberPerSec, tgtIdx, value);
                    break;
                case ePropertyID.Particle_IncidentRandomLevel:
                    EffectHelper.Copy(ref m_incidentRandomLevel, tgtIdx, value);
                    break;
                case ePropertyID.Particle_HDRFactor:
                    EffectHelper.Copy(ref m_HDR_Factor, tgtIdx, value);
                    break;
                case ePropertyID.Particle_IsLocalTrans:
                    EffectHelper.Copy(ref m_isLocalTrans, tgtIdx, value);
                    break;
                case ePropertyID.Particle_IsRandomEmitDir:
                    EffectHelper.Copy(ref m_isRandomEmitDir, tgtIdx, value);
                    break;
                case ePropertyID.Particle_AccelerationAccelerationRange:
                    EffectHelper.Copy(ref m_AccelerationComponent.m_accelerationRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_AccelerationDragForceRange:
                    EffectHelper.Copy(ref m_AccelerationComponent.m_dragForceRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotSpeedRange:
                    EffectHelper.Copy(ref m_RotationComponent.m_rotSpeedRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotAccelerationRange:
                    EffectHelper.Copy(ref m_RotationComponent.m_rotAccelerationRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotDragForceRange:
                    EffectHelper.Copy(ref m_RotationComponent.m_rotDragForceRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotAnchorOffsetRange:
                    EffectHelper.Copy(ref m_RotationComponent.m_rotAnchorOffsetRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotRange:
                    EffectHelper.Copy(ref m_RotationComponent.m_rotRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_ColorCurveColorCurve:
                    EffectHelper.Copy(ref m_ColorCurveComponent.m_pColorCurveArray[arrayIdx], tgtIdx, value);
                    break;
                case ePropertyID.Particle_BlinkBlinkSpanRange:
                    EffectHelper.Copy(ref m_BlinkComponent.m_blinkSpanSecRange, tgtIdx, value);
                    break;
                case ePropertyID.Particle_PathMoveFocusRadiusRange:
                    EffectHelper.Copy(ref m_PathMoveComponent.m_FocusRadiusRange[arrayIdx], tgtIdx, value);
                    break;
            }
        }

        public void SetValue(int propertyIdx, int arrayIdx, int tgtIdx, int compoentIdx, float value) {
            switch ((ePropertyID)propertyIdx) {
                case ePropertyID.Particle_TypeParamBillboardWidthRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_billboard.m_widthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamBillboardHeightRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_billboard.m_heightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPointSizeRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_point.m_sizeRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamLineWidth:
                    EffectHelper.Set(ref m_particleTypeParam.m_line.m_width, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPolyLineTopWidthRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_polyLine.m_topWidthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamPolyLineEndWidthRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_polyLine.m_endWidthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamConfettiWidthRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_confetti.m_widthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_TypeParamConfettiHeightRange:
                    EffectHelper.Set(ref m_particleTypeParam.m_confetti.m_heightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPointAngleRange:
                    EffectHelper.Set(ref m_emitionParam.m_point.m_angleRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxWidthRange:
                    EffectHelper.Set(ref m_emitionParam.m_box.m_widthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxHeightRange:
                    EffectHelper.Set(ref m_emitionParam.m_box.m_heightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamBoxDepthRange:
                    EffectHelper.Set(ref m_emitionParam.m_box.m_depthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneQuadWidthRange:
                    EffectHelper.Set(ref m_emitionParam.m_planeQuad.m_widthRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneQuadHeightRange:
                    EffectHelper.Set(ref m_emitionParam.m_planeQuad.m_heightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamPlaneCircleRadiusRange:
                    EffectHelper.Set(ref m_emitionParam.m_planeCircle.m_radiusRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamSphereAngleRange:
                    EffectHelper.Set(ref m_emitionParam.m_sphere.m_angleRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamSphereRadiusRange:
                    EffectHelper.Set(ref m_emitionParam.m_sphere.m_radiusRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusAngleRange:
                    EffectHelper.Set(ref m_emitionParam.m_torus.m_angleRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusBigRadius:
                    EffectHelper.Set(ref m_emitionParam.m_torus.m_bigRadiusRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamTorusSmallRadiusRange:
                    EffectHelper.Set(ref m_emitionParam.m_torus.m_smallRadiusRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamCylinderRadiusRange:
                    EffectHelper.Set(ref m_emitionParam.m_cylinder.m_RadiusRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_EmitParamCylinderHeightRange:
                    EffectHelper.Set(ref m_emitionParam.m_cylinder.m_HeightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_GravityDir:
                    EffectHelper.Set(ref m_gravityDir, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_GravityForceRange:
                    EffectHelper.Set(ref m_gravityForceRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanType:
                    int type = (int)m_lifeSpanType;
                    EffectHelper.Copy(ref type, tgtIdx, value);
                    m_lifeSpanType = (eParticleLifeSpanType)type;
                    break;
                case ePropertyID.Particle_LifeSpanAlpha:
                    int alpha = (int)m_lifeSpanAlpha;
                    EffectHelper.Copy(ref alpha, tgtIdx, value);
                    m_lifeSpanAlpha = (eParticleLifeSpanAlpha)alpha;
                    break;
                case ePropertyID.Particle_LifeSpanTimeRange:
                    EffectHelper.Set(ref m_LifeSpanParam.m_Time.m_lifeSpanSecRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanDistanceMaxRange:
                    EffectHelper.Set(ref m_LifeSpanParam.m_Distance.m_lifeSpanDistanceMaxRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_LifeSpanHeightRange:
                    EffectHelper.Set(ref m_LifeSpanParam.m_Height.m_lifeSpanHeightRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_IncidentNumberPerSec:
                    EffectHelper.Set(ref m_incidentNumberPerSec, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_IncidentRandomLevel:
                    EffectHelper.Set(ref m_incidentRandomLevel, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_HDRFactor:
                    EffectHelper.Set(ref m_HDR_Factor, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_IsLocalTrans:
                    EffectHelper.Set(ref m_isLocalTrans, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_IsRandomEmitDir:
                    EffectHelper.Set(ref m_isRandomEmitDir, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_AccelerationAccelerationRange:
                    EffectHelper.Set(ref m_AccelerationComponent.m_accelerationRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_AccelerationDragForceRange:
                    EffectHelper.Set(ref m_AccelerationComponent.m_dragForceRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotSpeedRange:
                    EffectHelper.Set(ref m_RotationComponent.m_rotSpeedRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotAccelerationRange:
                    EffectHelper.Set(ref m_RotationComponent.m_rotAccelerationRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotDragForceRange:
                    EffectHelper.Set(ref m_RotationComponent.m_rotDragForceRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotAnchorOffsetRange:
                    EffectHelper.Set(ref m_RotationComponent.m_rotAnchorOffsetRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_RotationRotRange:
                    EffectHelper.Set(ref m_RotationComponent.m_rotRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_ColorCurveColorCurve:
                    EffectHelper.Set(ref m_ColorCurveComponent.m_pColorCurveArray[arrayIdx], tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_BlinkBlinkSpanRange:
                    EffectHelper.Set(ref m_BlinkComponent.m_blinkSpanSecRange, tgtIdx, compoentIdx, value);
                    break;
                case ePropertyID.Particle_PathMoveFocusRadiusRange:
                    EffectHelper.Set(ref m_PathMoveComponent.m_FocusRadiusRange[arrayIdx], tgtIdx, compoentIdx, value);
                    break;
            }
        }

        public eEffectAnimTypeCode GetTypeCode(int propertyIdx) {
            return m_PropertyList[propertyIdx].m_TypeCode;
        }

        public Type GetType(int propertyIdx) {
            eEffectAnimTypeCode typeCode = GetTypeCode(propertyIdx);
            if (typeCode == eEffectAnimTypeCode.Enum) {
                if (propertyIdx == (int)ePropertyID.Particle_LifeSpanType) {
                    return typeof(eParticleLifeSpanType);
                }
                if (propertyIdx == (int)ePropertyID.Particle_LifeSpanAlpha) {
                    return typeof(eParticleLifeSpanAlpha);
                }
            }
            return EffectHelper.GetTypeFromTypeCode(typeCode);
        }

        public string GetPropertyName(int propertyIdx) {
            return m_PropertyList[propertyIdx].m_PropertyName;
        }

        private static Property[] m_PropertyList = new Property[(int)ePropertyID.Max] {
            new Property("TypeParamBillboardWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamBillboardHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamPointSizeRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamLineWidth", eEffectAnimTypeCode.Float),
            new Property("TypeParamPolyLineTopWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamPolyLineEndWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamConfettiWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("TypeParamConfettiHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamPointAngleRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamBoxWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamBoxHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamBoxDepthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamPlaneQuadWidthRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamPlaneQuadHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamPlaneCircleRadiusRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamSphereAngleRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamSphereRadiusRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamTorusAngleRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamTorusBigRadius", eEffectAnimTypeCode.Float),
            new Property("EmitParamTorusSmallRadiusRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamCylinderRadiusRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("EmitParamCylinderHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("GravityDir", eEffectAnimTypeCode.Vector3),
            new Property("GravityForceRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("LifeSpanType", eEffectAnimTypeCode.Enum),
            new Property("LifeSpanAlpha", eEffectAnimTypeCode.Enum),
            new Property("LifeSpanTimeRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("LifeSpanDistanceMaxRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("LifeSpanHeightRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("IncidentNumberPerSec", eEffectAnimTypeCode.Float),
            new Property("IncidentRandomLevel", eEffectAnimTypeCode.Float),
            new Property("HDRFactor", eEffectAnimTypeCode.Float),
            new Property("IsLocalTrans", eEffectAnimTypeCode.Bool),
            new Property("IsRandomEmitDir", eEffectAnimTypeCode.Bool),
            new Property("AccelerationAccelerationRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("AccelerationDragForceRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("RotationRotSpeedRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("RotationRotAccelerationRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("RotationRotDragForceRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("RotationRotAnchorOffsetRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("RotationRotRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("ColorCurveColorCurve", eEffectAnimTypeCode.RangeValueColor),
            new Property("BlinkBlinkSpanRange", eEffectAnimTypeCode.RangePareFloat),
            new Property("PathMoveFocusRadiusRange", eEffectAnimTypeCode.RangePareFloat)
        };

        [Serializable]
        public class ParticleTypeParam {
            public Billboard m_billboard;
            public Point m_point;
            public Line m_line;
            public PolyLine m_polyLine;
            public Confetti m_confetti;
            public Ribbon m_ribbon;

            [Serializable]
            public class Billboard {
                public RangePareFloat m_widthRange = new RangePareFloat();
                public RangePareFloat m_heightRange = new RangePareFloat();
                public PrimBillboardBuffer.eRotateType m_RotateType;
            }

            [Serializable]
            public class Point {
                public RangePareFloat m_sizeRange = new RangePareFloat();
            }

            [Serializable]
            public class Line {
                public float m_width = 1f;
                public int m_jointNum;
                public int m_HistoryPointNum;
            }

            [Serializable]
            public class PolyLine {
                public RangePareFloat m_topWidthRange = new RangePareFloat();
                public RangePareFloat m_endWidthRange = new RangePareFloat();
                public int m_jointNum;
                public int m_HistoryPointNum;
            }

            [Serializable]
            public class Confetti {
                public RangePareFloat m_widthRange = new RangePareFloat();
                public RangePareFloat m_heightRange = new RangePareFloat();
            }

            [Serializable]
            public class Ribbon {
                public RangePareFloat m_topWidthRange = new RangePareFloat();
                public RangePareFloat m_endWidthRange = new RangePareFloat();
                public int m_jointNum;
                public int m_HistoryPointNum;
            }
        }

        [Serializable]
        public class EmitionParam {
            public Point m_point;
            public Box m_box;
            public PlaneQuad m_planeQuad;
            public PlaneCircle m_planeCircle;
            public Sphere m_sphere;
            public Torus m_torus;
            public Cylinder m_cylinder;

            [Serializable]
            public class Point {
                public RangePareFloat m_angleRange = new RangePareFloat();
            }

            [Serializable]
            public class Box {
                public RangePareFloat m_widthRange = new RangePareFloat();
                public RangePareFloat m_heightRange = new RangePareFloat();
                public RangePareFloat m_depthRange = new RangePareFloat();
            }

            [Serializable]
            public class PlaneQuad {
                public RangePareFloat m_widthRange = new RangePareFloat();
                public RangePareFloat m_heightRange = new RangePareFloat();
            }

            [Serializable]
            public class PlaneCircle {
                public RangePareFloat m_radiusRange = new RangePareFloat();
            }

            [Serializable]
            public class Sphere {
                public RangePareFloat m_angleRange = new RangePareFloat();
                public RangePareFloat m_radiusRange = new RangePareFloat();
            }

            [Serializable]
            public class Torus {
                public RangePareFloat m_angleRange = new RangePareFloat();
                public float m_bigRadiusRange;
                public RangePareFloat m_smallRadiusRange = new RangePareFloat();
            }

            [Serializable]
            public class Cylinder {
                public RangePareFloat m_RadiusRange = new RangePareFloat();
                public RangePareFloat m_HeightRange = new RangePareFloat();
            }
        }

        [Serializable]
        public class LifeSpanParam {
            public Time m_Time;
            public Distance m_Distance;
            public Height m_Height;

            [Serializable]
            public class Time {
                public RangePareFloat m_lifeSpanSecRange = new RangePareFloat();
            }

            [Serializable]
            public class Distance {
                public RangePareFloat m_lifeSpanDistanceMaxRange = new RangePareFloat();
            }

            [Serializable]
            public class Height {
                public RangePareFloat m_lifeSpanHeightRange = new RangePareFloat();
            }
        }

        [Serializable]
        public class CollisionTypeParam {
            public Height m_height;
            public Collision m_collision;

            [Serializable]
            public struct Height {
                public float m_height;
                public float m_reflectionCoefficient;
                public float m_frictionCoefficient;
                public bool m_isLocalHeight;
            }

            [Serializable]
            public struct Collision {
                public float m_reflectionCoefficient;
            }
        }

        [Serializable]
        public class AccelerationComponent {
            public RangePareFloat m_accelerationRange = new RangePareFloat();
            public RangePareFloat m_dragForceRange = new RangePareFloat();
        }

        [Serializable]
        public class RotationComponent {
            public RangePareFloat m_rotSpeedRange = new RangePareFloat();
            public RangePareFloat m_rotAccelerationRange = new RangePareFloat();
            public RangePareFloat m_rotDragForceRange = new RangePareFloat();
            public RangePareFloat m_rotAnchorOffsetRange = new RangePareFloat();
            public RangePareFloat m_rotRange = new RangePareFloat();
        }

        [Serializable]
        public class UVAnimationComponent {
            public eParticleTextureAnim m_uvAnimeType;
            public RangePareFloat m_switchBlockSecRange = new RangePareFloat();
            public bool m_randomStartBlockFlg;
        }

        [Serializable]
        public class ColorCurveComponent {
            public RangeValueColor[] m_pColorCurveArray;
            public int m_colorCurveNum;
        }

        [Serializable]
        public struct TailComponent {
            public Rect m_tailUVRect;
            public int m_tailJointNum;
        }

        [Serializable]
        public class BlinkComponent {
            public RangePareFloat m_blinkSpanSecRange = new RangePareFloat();
        }

        [Serializable]
        public class PathMoveComponent {
            public RangePareFloat m_startFocusRadiusRange = new RangePareFloat();
            public RangePareFloat m_endFocusRadiusRange = new RangePareFloat();
            public RangePareFloat[] m_FocusRadiusRange = new RangePareFloat[4];
        }

        public enum ePropertyID {
            Particle_TypeParamBillboardWidthRange,
            Particle_TypeParamBillboardHeightRange,
            Particle_TypeParamPointSizeRange,
            Particle_TypeParamLineWidth,
            Particle_TypeParamPolyLineTopWidthRange,
            Particle_TypeParamPolyLineEndWidthRange,
            Particle_TypeParamConfettiWidthRange,
            Particle_TypeParamConfettiHeightRange,
            Particle_EmitParamPointAngleRange,
            Particle_EmitParamBoxWidthRange,
            Particle_EmitParamBoxHeightRange,
            Particle_EmitParamBoxDepthRange,
            Particle_EmitParamPlaneQuadWidthRange,
            Particle_EmitParamPlaneQuadHeightRange,
            Particle_EmitParamPlaneCircleRadiusRange,
            Particle_EmitParamSphereAngleRange,
            Particle_EmitParamSphereRadiusRange,
            Particle_EmitParamTorusAngleRange,
            Particle_EmitParamTorusBigRadius,
            Particle_EmitParamTorusSmallRadiusRange,
            Particle_EmitParamCylinderRadiusRange,
            Particle_EmitParamCylinderHeightRange,
            Particle_GravityDir,
            Particle_GravityForceRange,
            Particle_LifeSpanType,
            Particle_LifeSpanAlpha,
            Particle_LifeSpanTimeRange,
            Particle_LifeSpanDistanceMaxRange,
            Particle_LifeSpanHeightRange,
            Particle_IncidentNumberPerSec,
            Particle_IncidentRandomLevel,
            Particle_HDRFactor,
            Particle_IsLocalTrans,
            Particle_IsRandomEmitDir,
            Particle_AccelerationAccelerationRange,
            Particle_AccelerationDragForceRange,
            Particle_RotationRotSpeedRange,
            Particle_RotationRotAccelerationRange,
            Particle_RotationRotDragForceRange,
            Particle_RotationRotAnchorOffsetRange,
            Particle_RotationRotRange,
            Particle_ColorCurveColorCurve,
            Particle_BlinkBlinkSpanRange,
            Particle_PathMoveFocusRadiusRange,
            Max
        }

        public struct Property {
            public string m_PropertyName;
            public eEffectAnimTypeCode m_TypeCode;

            public Property(string str, eEffectAnimTypeCode typeCode) {
                m_PropertyName = str;
                m_TypeCode = typeCode;
            }
        }
    }
}
