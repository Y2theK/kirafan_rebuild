﻿namespace Meige {
    public static class ParticleTools {
        public const int PARTICLE_EMITTER_CHUNK_COLORCURVE_NUM = 4;
        public const int PARTICLE_EMITTER_CHUNK_COLORCURVE_POINT_NUM = 4;
    }
}
