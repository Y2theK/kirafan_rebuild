﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Meige {
    [Serializable]
    public class ParticleUnit {
        public ParticleTypeParam m_particleTypeParam;
        public eParticleType m_particleType;

        public Vector3 m_birthPos;
        public Vector3 m_pos;
        public Vector3 m_offset;
        public float m_scale;
        public float m_lifeScaleMax;
        public Vector3 m_velocity;
        public Vector3 m_gravityForce;
        public Rect m_uvRect;
        public Color m_color;
        public eParticleLifeSpanType m_lifeSpanType;
        public float m_lifeSpanRate;
        public float[] m_lifeSpanParam = new float[2];
        public float m_lifeSpanFadeAlphaRate;
        public int m_aliveCount;
        public bool m_initializedFlg;
        public bool m_activeFlg;
        public bool m_isDirty;
        public bool m_isLocal;
        public int m_Index;

        public AccelerationComponent m_AccelerationComponent;
        public RotationComponent m_RotationComponent;
        public UVAnimationComponent m_UVAnimationComponent;
        public ColorCurveComponent m_ColorCurveComponent;
        public TailComponent m_TailComponent;
        public BlinkComponent m_BlinkComponent;
        public PathMoveComponent m_PathMoveComponent;

        [Serializable]
        [StructLayout(LayoutKind.Explicit)]
        public struct ParticleTypeParam {
            [FieldOffset(0)] public Billboard m_billboard;
            [FieldOffset(0)] public Point m_point;
            [FieldOffset(0)] public Line m_line;
            [FieldOffset(0)] public PolyLine m_polyLine;
            [FieldOffset(0)] public Confetti m_confetti;
            [FieldOffset(0)] public Ribbon m_ribbon;

            [Serializable]
            public struct Billboard {
                public float m_width;
                public float m_height;
            }

            [Serializable]
            public struct Point {
                public float m_size;
            }

            [Serializable]
            public struct Line {
                public short m_jointNum;
                public short m_HistoryPointNum;
                public float m_width;
                public float m_width2;
                public Vector3[] m_pPos;
            }

            [Serializable]
            public struct PolyLine {
                public short m_jointNum;
                public short m_HistoryPointNum;
                public float m_topWidth;
                public float m_endWidth;
                public Vector3[] m_pPos;
            }

            [Serializable]
            public struct Confetti {
                public float m_width;
                public float m_height;
            }

            [Serializable]
            public struct Ribbon {
                public short m_jointNum;
                public short m_HistoryPointNum;
                public float m_topWidth;
                public float m_endWidth;
                public Vector3[] m_pPos;
                public Vector3[] m_pVelocity;
            }
        }

        [Serializable]
        public struct AccelerationComponent {
            public float m_acceleration;
            public float m_dragForce;
        }

        [Serializable]
        public struct RotationComponent {
            public Vector3 m_rot;
            public Vector3 m_rotAnchorOffset;
            public Vector3 m_rotVelocity;
            public Vector3 m_rotAcceleration;
            public float m_rotDragForce;
        }

        [Serializable]
        public struct UVAnimationComponent {
            public int m_nowBlock;
            public float m_switchSec;
            public float m_switchSecWork;
        }

        [Serializable]
        public struct ColorCurveComponent {
            public RangeValueColor m_pColorCurve;
        }

        [Serializable]
        public struct TailComponent {
            public Vector3[] m_pTailPos;
            public Vector3 m_TopPos;
            public Rect m_tailUVRect;
            public float m_width;
        }

        [Serializable]
        public struct BlinkComponent {
            public float m_blinkRate;
            public float m_blinkSpeed;
        }

        [Serializable]
        public struct PathMoveComponent {
            public Vector3[] m_offsetPos;
        }
    }
}
