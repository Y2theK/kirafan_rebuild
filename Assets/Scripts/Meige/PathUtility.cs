﻿using System.IO;
using System.Linq;

namespace Meige {
    public static class PathUtility {
        public static string ReplaceDelimiter(string targetPath) {
            return targetPath.Replace(@"\", "/");
        }

        public static string ChangeExtension(string path, string extension) {
            return ReplaceDelimiter(Path.ChangeExtension(path, extension));
        }

        public static string Combine(string path1, string path2) {
            return ReplaceDelimiter(Path.Combine(path1, path2));
        }

        public static string Combine(string path1, string path2, string path3) {
            return ReplaceDelimiter(Path.Combine(path1, path2, path3));
        }

        public static string Combine(string path1, string path2, string path3, string path4) {
            return ReplaceDelimiter(Path.Combine(path1, path2, path3, path4));
        }

        public static string Combine(params string[] paths) {
            return ReplaceDelimiter(Path.Combine(paths));
        }

        public static string GetDirectoryName(string path) {
            return ReplaceDelimiter(Path.GetDirectoryName(path));
        }

        public static string GetFileName(string path) {
            return ReplaceDelimiter(Path.GetFileName(path));
        }

        public static string GetFileNameWithoutExtension(string path) {
            return ReplaceDelimiter(Path.GetFileNameWithoutExtension(path));
        }

        public static string GetFullPath(string path) {
            return ReplaceDelimiter(Path.GetFullPath(path));
        }

        public static char[] GetInvalidFileNameChars() {
            return Path.GetInvalidFileNameChars();
        }

        public static char[] GetInvalidPathChars() {
            return Path.GetInvalidPathChars();
        }

        public static string GetPathRoot(string path) {
            return ReplaceDelimiter(Path.GetPathRoot(path));
        }

        public static string GetRandomFileName() {
            return ReplaceDelimiter(Path.GetRandomFileName());
        }

        public static string GetTempFileName() {
            return ReplaceDelimiter(Path.GetTempFileName());
        }

        public static string GetTempPath() {
            return ReplaceDelimiter(Path.GetTempPath());
        }

        public static bool HasExtension(string path) {
            return Path.HasExtension(path);
        }

        public static bool IsPathRooted(string path) {
            return Path.IsPathRooted(path);
        }

        public static string[] GetDirectories(string path) {
            return Directory.GetDirectories(path).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetDirectories(string path, string searchPattern) {
            return Directory.GetDirectories(path, searchPattern).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetDirectories(string path, string searchPattern, SearchOption searchOption) {
            return Directory.GetDirectories(path, searchPattern, searchOption).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFiles(string path) {
            return Directory.GetFiles(path).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFiles(string path, string searchPattern) {
            return Directory.GetFiles(path, searchPattern).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFiles(string path, string searchPattern, SearchOption searchOption) {
            return Directory.GetFiles(path, searchPattern, searchOption).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFileSystemEntries(string path) {
            return Directory.GetFileSystemEntries(path).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFileSystemEntries(string path, string searchPattern) {
            return Directory.GetFileSystemEntries(path, searchPattern).Select(ReplaceDelimiter).ToArray();
        }

        public static string[] GetFileSystemEntries(string path, string searchPattern, SearchOption searchOption) {
            return Directory.GetFileSystemEntries(path, searchPattern, searchOption).Select(ReplaceDelimiter).ToArray();
        }
    }
}
