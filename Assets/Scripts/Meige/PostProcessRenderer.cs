﻿using System;
using UnityEngine;

#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Meige {
    public class PostProcessRenderer : MonoBehaviour {
        private static string[] m_ShaderKeywordName_EnableToneCurve = new string[] {
            "_ENABLETONECURVE_DISABLE",
            "_ENABLETONECURVE_ENABLE"
        };

        private static string[] m_ShaderKeywordName_EnableContrast = new string[] {
            "_ENABLECONTRAST_DISABLE",
            "_ENABLECONTRAST_ENABLE"
        };

        private static string[] m_ShaderKeywordName_EnableBrightness = new string[] {
            "_ENABLEBRIGHTNESS_DISABLE",
            "_ENABLEBRIGHTNESS_ENABLE"
        };

        private static string[] m_ShaderKeywordName_EnableChroma = new string[] {
            "_ENABLECHROMA_DISABLE",
            "_ENABLECHROMA_ENABLE"
        };

        private static string[] m_ShaderKeywordName_EnableColorBlend = new string[] {
            "_ENABLECOLORBLEND_DISABLE",
            "_ENABLECOLORBLEND_ENABLE"
        };

        private static string[] m_ShaderKeywordName_EnableBloom = new string[] {
            "_ENABLEBLOOM_DISABLE",
            "_ENABLEBLOOM_ENABLE"
        };

        private static string[] m_ShaderKeywordName_GaussFilterType = new string[] {
            "_GAUSSFILTERTYPE_X5",
            "_GAUSSFILTERTYPE_X7"
        };

        [SerializeField] private bool m_enableCorrectToneCurve;
        [SerializeField] private bool m_enableCorrectContrast;
        [SerializeField] private bool m_enableCorrectBrightness;
        [SerializeField] private bool m_enableCorrectChroma;
        [SerializeField] private bool m_enableCorrectColorBlend;
        [SerializeField] private bool m_enableBloom;
        [SerializeField] private eToneMapProc m_ToneMapProc = eToneMapProc.eToneMapProc_Fix;
        [SerializeField] private eToneMapWhitePoint m_ToneMapWhitePoint = eToneMapWhitePoint.eToneMapWhitePoint_Fix;
        [SerializeField] private float m_ToneMapWhitePointScale = 1f;
        [SerializeField] private RangePareFloat m_ToneMapWhitePointRange = new RangePareFloat(0.2f, 5f);
        [SerializeField] private float m_ExposureCorrectionValue = 1f;
        [SerializeField] private float m_AdaptationRateSpeed = 0.2f;
        [SerializeField] private eBloomReferenceType m_BloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;
        [SerializeField] private float m_BloomBrightThresholdOffset = 1f;
        [SerializeField] private float m_BloomOffset = 5f;
        [SerializeField] private eGaussFilterType m_GaussFilterType = eGaussFilterType.eGaussFilterType_x5;
        [SerializeField, Range(1f, 4f)] private int m_BlurIterationNum = 2;
        [SerializeField] private float m_BloomIntensity = 1f;
        [SerializeField] private float m_CorrectContrastLevel;
        [SerializeField] private float m_CorrectBrightnessLevel;
        [SerializeField] private float m_CorrectChromaLevel;
        [SerializeField] private float m_CorrectBlendLevel;
        [SerializeField] private Color m_CorrectBlendColor = Color.white;

        private bool m_BackupEnableCorrectToneCurve;
        private bool m_BackupEnableCorrectContrast;
        private bool m_BackupEnableCorrectBrightness;
        private bool m_BackupEnableCorrectChroma;
        private bool m_BackupEnableCorrectColorBlend;
        private bool m_BackupEnableBloom;
        private Vector4 m_BackupCorrectionKeyValue = Vector4.zero;
        private float m_BackupCorrectContrastLevel = -9999f;
        private float m_BackupCorrectBrightnessLevel = -9999f;
        private float m_BackupCorrectChromaLevel = -9999f;
        private float m_BackupCorrectBlendLevel = -9999f;
        private Color m_BackupCorrectBlendColor = Color.black;
        private float m_BackupBloomBrightThresholdOffset;
        private float m_BackupBloomOffset;
        private float m_BackupBloomIntensity;
        private eGaussFilterType m_BackupGaussFilterType;

        private Material m_CorrectMaterial;
        private Material m_BrightPassFilterMaterial;
        private Material m_BloomMaterial;
        private Material m_LuminunceMaterial;
        private float m_AdaptedWhitePointScale = 1f;
        private RenderTexture[] m_AdaptRenderTex = new RenderTexture[2];
        private int m_CurAdaptIndex;
        private bool m_isAvailable;
        private int m_DivLevel = 1;

        public bool enableCorrectToneCurve {
            get => m_enableCorrectToneCurve;
            set => m_enableCorrectToneCurve = value;
        }

        public bool enableCorrectContrast {
            get => m_enableCorrectContrast;
            set => m_enableCorrectContrast = value;
        }

        public bool enableCorrectBrightness {
            get => m_enableCorrectBrightness;
            set => m_enableCorrectBrightness = value;
        }

        public bool enableCorrectChroma {
            get => m_enableCorrectChroma;
            set => m_enableCorrectChroma = value;
        }

        public bool enableCorrectColorBlend {
            get => m_enableCorrectColorBlend;
            set => m_enableCorrectColorBlend = value;
        }

        public bool enableBloom {
            get => m_enableBloom;
            set => m_enableBloom = value;
        }

        public eToneMapProc toneMapProc {
            get => m_ToneMapProc;
            set => m_ToneMapProc = value;
        }

        public eToneMapWhitePoint toneMapWhitePoint {
            get => m_ToneMapWhitePoint;
            set => m_ToneMapWhitePoint = value;
        }

        public float toneMapWhitePointScale {
            get => m_ToneMapWhitePointScale;
            set => m_ToneMapWhitePointScale = value;
        }

        public RangePareFloat toneMapWhitePointRange {
            get => m_ToneMapWhitePointRange;
            set => m_ToneMapWhitePointRange = value;
        }

        public float exposureCorrectionValue {
            get => m_ExposureCorrectionValue;
            set => m_ExposureCorrectionValue = value;
        }

        public float adaptationRateSpeed {
            get => m_AdaptationRateSpeed;
            set => m_AdaptationRateSpeed = value;
        }

        public eBloomReferenceType bloomReferenceType {
            get => m_BloomReferenceType;
            set => m_BloomReferenceType = value;
        }

        public float bloomBrightThresholdOffset {
            get => m_BloomBrightThresholdOffset;
            set => m_BloomBrightThresholdOffset = value;
        }

        public float bloomOffset {
            get => m_BloomOffset;
            set => m_BloomOffset = value;
        }

        public eGaussFilterType gaussFilterType {
            get => m_GaussFilterType;
            set => m_GaussFilterType = value;
        }

        public int blurIterationNum {
            get => m_BlurIterationNum;
            set => m_BlurIterationNum = value;
        }

        public float bloomIntensity {
            get => m_BloomIntensity;
            set => m_BloomIntensity = value;
        }

        public float correctContrastLevel {
            get => m_CorrectContrastLevel;
            set => m_CorrectContrastLevel = value;
        }

        public float correctBrightnessLevel {
            get => m_CorrectBrightnessLevel;
            set => m_CorrectBrightnessLevel = value;
        }

        public float correctChromaLevel {
            get => m_CorrectChromaLevel;
            set => m_CorrectChromaLevel = value;
        }

        public float correctBlendLevel {
            get => m_CorrectBlendLevel;
            set => m_CorrectBlendLevel = value;
        }

        public Color correctBlendColor {
            get => m_CorrectBlendColor;
            set => m_CorrectBlendColor = value;
        }

        private void Start() {
#if UNITY_ANDROID
			int androidVersion = new AndroidJavaClass("android.os.Build$VERSION").GetStatic<int>("SDK_INT");
			if (androidVersion < 21) {
				m_DivLevel = 2;
			}
#elif UNITY_IOS
			DeviceGeneration generation = Device.generation;
            if (generation == DeviceGeneration.iPadMini1Gen 
				|| generation == DeviceGeneration.iPadMini2Gen
				|| generation == DeviceGeneration.iPadMini3Gen
				|| generation == DeviceGeneration.iPadMini4Gen) {
                m_DivLevel = 2;
            }
#endif
            m_CorrectMaterial = new Material(Shader.Find("Hidden/Meige/MeigePostProcessShader"));
            m_BrightPassFilterMaterial = new Material(Shader.Find("Hidden/Meige/MeigeBrightPassFilterShader"));
            m_BloomMaterial = new Material(Shader.Find("Hidden/Meige/MeigeBloomShader"));
            m_LuminunceMaterial = new Material(Shader.Find("Hidden/Meige/MeigeLuminunceShader"));
            for (int i = 0; i < m_AdaptRenderTex.Length; i++) {
                m_AdaptRenderTex[i] = new RenderTexture(1, 1, 0, RenderTextureFormat.DefaultHDR);
            }
            m_AdaptedWhitePointScale = m_ToneMapWhitePointScale;
            m_isAvailable = true;
        }

        private void Update() { }

        private void CalcLuminunceValue(RenderTexture src) {
            int size = Mathf.Min(1024, 1024);
            RenderTexture renderTexture = RenderTexture.GetTemporary(size / 2, size / 2, 0, src.format, RenderTextureReadWrite.Default, 1);
            renderTexture.filterMode = FilterMode.Point;
            renderTexture.wrapMode = TextureWrapMode.Repeat;
            Graphics.Blit(src, renderTexture, m_LuminunceMaterial, 0);
            while (renderTexture.width > 1 || renderTexture.height > 1) {
                int width = renderTexture.width / 2;
                if (width < 1) {
                    width = 1;
                }
                int height = renderTexture.height / 2;
                if (height < 1) {
                    height = 1;
                }
                RenderTexture temporary = RenderTexture.GetTemporary(width, height, 0, src.format, RenderTextureReadWrite.Default, 1);
                renderTexture.filterMode = FilterMode.Point;
                renderTexture.wrapMode = TextureWrapMode.Repeat;
                temporary.filterMode = FilterMode.Point;
                temporary.wrapMode = TextureWrapMode.Repeat;
                Graphics.Blit(renderTexture, temporary, m_LuminunceMaterial, 1);
                RenderTexture.ReleaseTemporary(renderTexture);
                renderTexture = temporary;
            }
            int prevAdaptIndex = m_CurAdaptIndex;
            m_CurAdaptIndex = (m_CurAdaptIndex + 1) % 2;
            float adapt = 1f - Mathf.Pow(1f - m_AdaptationRateSpeed, 30f * Time.deltaTime);
            adapt = Mathf.Clamp(adapt, 0.01f, 1f);
            m_LuminunceMaterial.SetTexture("_CurTex", renderTexture);
            m_LuminunceMaterial.SetVector("_AdaptParams", new Vector4(adapt, m_ToneMapWhitePointRange.min, m_ToneMapWhitePointRange.max, 0f));
            Graphics.SetRenderTarget(m_AdaptRenderTex[m_CurAdaptIndex]);
            GL.Clear(false, true, Color.black);
            Graphics.Blit(m_AdaptRenderTex[prevAdaptIndex], m_AdaptRenderTex[m_CurAdaptIndex], m_LuminunceMaterial, 2);
        }

        private void OnRenderImage(RenderTexture src, RenderTexture dst) {
            if (!m_isAvailable) {
                Graphics.Blit(src, dst);
                return;
            }

            if ((m_enableCorrectToneCurve || m_enableBloom) && ((m_enableCorrectToneCurve && m_ToneMapWhitePoint != eToneMapWhitePoint.eToneMapWhitePoint_Fix) || (m_enableBloom && m_BloomReferenceType != eBloomReferenceType.eBloomReferenceType_Fix))) {
                CalcLuminunceValue(src);
            }
            if (m_BackupEnableCorrectToneCurve != m_enableCorrectToneCurve) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableToneCurve, m_enableCorrectToneCurve ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableToneCurve", m_enableCorrectToneCurve ? 1f : 0f);
                m_BackupEnableCorrectToneCurve = m_enableCorrectToneCurve;
            }
            if (m_BackupEnableCorrectContrast != m_enableCorrectContrast) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableContrast, m_enableCorrectContrast ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableContrast", m_enableCorrectContrast ? 1f : 0f);
                m_BackupEnableCorrectContrast = m_enableCorrectContrast;
            }
            if (m_BackupEnableCorrectBrightness != m_enableCorrectBrightness) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableBrightness, m_enableCorrectBrightness ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableBrightness", m_enableCorrectBrightness ? 1f : 0f);
                m_BackupEnableCorrectBrightness = m_enableCorrectBrightness;
            }
            if (m_BackupEnableCorrectChroma != m_enableCorrectChroma) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableChroma, m_enableCorrectChroma ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableChroma", m_enableCorrectChroma ? 1f : 0f);
                m_BackupEnableCorrectChroma = m_enableCorrectChroma;
            }
            if (m_BackupEnableCorrectColorBlend != m_enableCorrectColorBlend) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableColorBlend, m_enableCorrectColorBlend ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableColorBlend", m_enableCorrectColorBlend ? 1f : 0f);
                m_BackupEnableCorrectColorBlend = m_enableCorrectColorBlend;
            }
            if (m_BackupEnableBloom != m_enableBloom) {
                MeigeShaderUtility.EnableKeyword(m_CorrectMaterial, m_ShaderKeywordName_EnableBloom, m_enableBloom ? 1 : 0);
                m_CorrectMaterial.SetFloat("_EnableBloom", m_enableBloom ? 1f : 0f);
                m_BackupEnableBloom = m_enableBloom;
            }
            m_AdaptedWhitePointScale = Mathf.Lerp(m_AdaptedWhitePointScale, m_ToneMapWhitePointScale, m_AdaptationRateSpeed);
            Vector4 toneCorretion = new Vector4();
            toneCorretion.x = m_ExposureCorrectionValue;
            toneCorretion.y = m_ToneMapWhitePointScale;
            toneCorretion.w = toneCorretion.x / Mathf.Pow(2f, toneCorretion.y);
            toneCorretion.z = m_AdaptedWhitePointScale * toneCorretion.w;
            toneCorretion.z *= toneCorretion.z;
            if (m_BackupCorrectionKeyValue != toneCorretion) {
                m_CorrectMaterial.SetVector("_ToneCurve_Param", toneCorretion);
                m_BackupCorrectionKeyValue = toneCorretion;
            }
            if (m_BackupCorrectContrastLevel != m_CorrectContrastLevel) {
                m_CorrectMaterial.SetFloat("_CorrectContrastRate", m_CorrectContrastLevel);
                m_BackupCorrectContrastLevel = m_CorrectContrastLevel;
            }
            if (m_BackupCorrectBrightnessLevel != m_CorrectBrightnessLevel) {
                m_CorrectMaterial.SetFloat("_CorrectBrightnessRate", m_CorrectBrightnessLevel);
                m_BackupCorrectBrightnessLevel = m_CorrectBrightnessLevel;
            }
            if (m_BackupCorrectChromaLevel != m_CorrectChromaLevel) {
                m_CorrectMaterial.SetFloat("_CorrectChromaRate", m_CorrectChromaLevel);
                m_BackupCorrectChromaLevel = m_CorrectChromaLevel;
            }
            if (m_BackupCorrectBlendLevel != m_CorrectBlendLevel) {
                m_CorrectMaterial.SetFloat("_CorrectBlendRate", m_CorrectBlendLevel);
                m_BackupCorrectBlendLevel = m_CorrectBlendLevel;
            }
            if (m_BackupCorrectBlendColor != m_CorrectBlendColor) {
                m_CorrectMaterial.SetColor("_CorrectBlendColor", m_CorrectBlendColor);
                m_BackupCorrectBlendColor = m_CorrectBlendColor;
            }
            if (m_BackupBloomIntensity != m_BloomIntensity) {
                m_CorrectMaterial.SetFloat("_Bloomintensity", m_BloomIntensity);
                m_BackupBloomIntensity = m_BloomIntensity;
            }
            if (m_BackupBloomBrightThresholdOffset != m_BloomBrightThresholdOffset) {
                m_BrightPassFilterMaterial.SetFloat("_BloomKeyValue", m_BloomBrightThresholdOffset);
                m_BackupBloomBrightThresholdOffset = m_BloomBrightThresholdOffset;
            }
            if (m_BackupGaussFilterType != m_GaussFilterType) {
                MeigeShaderUtility.EnableKeyword(m_BloomMaterial, m_ShaderKeywordName_GaussFilterType, m_GaussFilterType == eGaussFilterType.eGaussFilterType_x7 ? 1 : 0);
                m_BloomMaterial.SetFloat("_GaussFilterType", (float)m_GaussFilterType - 1f);
                m_BackupGaussFilterType = m_GaussFilterType;
            }

            if (m_enableBloom) {
                int width = Mathf.Min(src.width, 1920);
                int height = Mathf.Min(src.height, 1080);
                int div2 = 2 * m_DivLevel;
                RenderTexture renderTexture = null;
                RenderTexture temporary = RenderTexture.GetTemporary(src.width / div2, src.height / div2, 0, src.format, RenderTextureReadWrite.Default, 1);
                if (temporary != null) {
                    temporary.filterMode = FilterMode.Bilinear;
                    if (m_BloomReferenceType == eBloomReferenceType.eBloomReferenceType_ToneLuminanceValue) {
                        m_BrightPassFilterMaterial.SetTexture("_LuminunceTex", m_AdaptRenderTex[m_CurAdaptIndex]);
                        Graphics.Blit(src, temporary, m_BrightPassFilterMaterial, 1);
                    } else if (m_BloomReferenceType == eBloomReferenceType.eBloomReferenceType_Fix) {
                        Graphics.Blit(src, temporary, m_BrightPassFilterMaterial, 0);
                    }
                    renderTexture = temporary;
                    int div4 = 4 * m_DivLevel;
                    for (int i = 0; i < m_BlurIterationNum; i++) {
                        if (m_BackupBloomOffset != m_BloomOffset) {
                            m_BloomMaterial.SetFloat("_BloomOffset", m_BloomOffset + i);
                            m_BackupBloomOffset = m_BloomOffset;
                        }
                        temporary = RenderTexture.GetTemporary(src.width / div4, src.height / div4, 0, src.format, RenderTextureReadWrite.Default, 1);
                        if (renderTexture != null) {
                            renderTexture.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(renderTexture, temporary, m_BloomMaterial, 0);
                            RenderTexture.ReleaseTemporary(renderTexture);
                        }
                        renderTexture = RenderTexture.GetTemporary(src.width / div4, src.height / div4, 0, src.format, RenderTextureReadWrite.Default, 1);
                        if (temporary != null) {
                            temporary.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(temporary, renderTexture, m_BloomMaterial, 1);
                            RenderTexture.ReleaseTemporary(temporary);
                        }
                    }
                }
                m_CorrectMaterial.SetTexture("_BloomTex", renderTexture);
                switch (m_ToneMapWhitePoint) {
                    case eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValue:
                        m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", m_AdaptRenderTex[m_CurAdaptIndex]);
                        Graphics.Blit(src, dst, m_CorrectMaterial, 1);
                        break;
                    case eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValueMax:
                        m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", m_AdaptRenderTex[m_CurAdaptIndex]);
                        Graphics.Blit(src, dst, m_CorrectMaterial, 2);
                        break;
                    case eToneMapWhitePoint.eToneMapWhitePoint_Fix:
                        Graphics.Blit(src, dst, m_CorrectMaterial, 0);
                        break;
                }
                if (renderTexture != null) {
                    RenderTexture.ReleaseTemporary(renderTexture);
                }
            } else if (m_enableCorrectToneCurve || m_enableCorrectContrast || m_enableCorrectBrightness || m_enableCorrectChroma || m_enableCorrectColorBlend) {
                switch (m_ToneMapWhitePoint) {
                    case eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValue:
                        m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", m_AdaptRenderTex[m_CurAdaptIndex]);
                        Graphics.Blit(src, dst, m_CorrectMaterial, 1);
                        break;
                    case eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValueMax:
                        m_CorrectMaterial.SetTexture("_AdaptedLuminunceTex", m_AdaptRenderTex[m_CurAdaptIndex]);
                        Graphics.Blit(src, dst, m_CorrectMaterial, 2);
                        break;
                    case eToneMapWhitePoint.eToneMapWhitePoint_Fix:
                        Graphics.Blit(src, dst, m_CorrectMaterial, 0);
                        break;
                }
            } else {
                Graphics.Blit(src, dst);
            }
        }
    }
}
