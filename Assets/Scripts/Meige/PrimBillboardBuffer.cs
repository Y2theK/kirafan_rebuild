﻿using System.Collections.Generic;
using UnityEngine;

namespace Meige {
    public class PrimBillboardBuffer : MonoBehaviour {
        private const int MAX_ACTIVE_MESHBUFFER = 32;

        private EffectMeshBuffer m_EffectMeshBuffer;

        protected bool m_MeshBufferIsSelf;
        protected bool m_SetupIsEnd;
        protected List<BillboardBuffer> m_BillboardBufferList = new List<BillboardBuffer>();
        protected List<BillboardBuffer> m_activeBillboardBufferList = new List<BillboardBuffer>(MAX_ACTIVE_MESHBUFFER);

        public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer) {
            if (m_EffectMeshBuffer == null && meshBuffer != null) {
                m_EffectMeshBuffer = meshBuffer;
            }
        }

        public void ClearExternalMeshBuffer() {
            if (m_EffectMeshBuffer != null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer.RemoveHook_OnWillRender(UpdateMesh);
                foreach (BillboardBuffer buffer in m_BillboardBufferList) {
                    m_EffectMeshBuffer.RemoveBuffer(buffer.meshBuffer);
                }
                m_EffectMeshBuffer = null;
            }
        }

        private void OnDestroy() {
            ClearExternalMeshBuffer();
        }

        internal void AddActiveBufferList(BillboardBuffer meshBuffer) {
            m_activeBillboardBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(BillboardBuffer meshBuffer) {
            m_activeBillboardBufferList.Remove(meshBuffer);
        }

        public bool m_maxTriangleBufferChanged { get; set; }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public bool bufferChangeFlg { get; set; }

        private void Start() {
            Setup();
        }

        private void Setup() {
            if (m_SetupIsEnd) { return; }

            if (m_EffectMeshBuffer == null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = gameObject.AddComponent<EffectMeshBuffer>();
                m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
                m_MeshBufferIsSelf = true;
            }
            if (m_EffectMeshBuffer != null) {
                m_EffectMeshBuffer.AddHook_OnWillRender(UpdateMesh);
            }
            m_SetupIsEnd = true;
        }

        public virtual BillboardBuffer AddBuffer(int billboardNum, bool isShareVetexColor = true) {
            Setup();
            BillboardBuffer buffer = new BillboardBuffer();
            buffer.positions = new Vector3[billboardNum];
            buffer.sizes = new Vector2[billboardNum];
            buffer.rots = new float[billboardNum];
            buffer.UVs = new Rect[billboardNum];
            buffer.colors = new Color[billboardNum];
            if (m_EffectMeshBuffer.isUsableSubUVsFix) {
                buffer.SubUVs = new Rect[billboardNum];
            }
            if (isShareVetexColor) {
                buffer.colors1 = buffer.colors;
                buffer.colors2 = buffer.colors;
                buffer.colors3 = buffer.colors;
            } else {
                buffer.colors1 = new Color[billboardNum];
                buffer.colors2 = new Color[billboardNum];
                buffer.colors3 = new Color[billboardNum];
            }
            buffer.billboardNum = billboardNum;
            buffer.parent = this;
            buffer.enabled = false;
            m_BillboardBufferList.Add(buffer);
            buffer.UpdatePositions();
            buffer.UpdateSizes();
            buffer.UpdateRots();
            buffer.UpdateUVs();
            buffer.UpdateColors();
            if (m_EffectMeshBuffer.isUsableSubUVsFix) {
                buffer.UpdateSubUVs();
            }
            bufferChangeFlg = true;
            buffer.meshBuffer = m_EffectMeshBuffer.AddBuffer(billboardNum * 4, billboardNum * 4);
            for (int i = 0; i < billboardNum; i += 4) {
                buffer.meshBuffer.indices[i] = i;
                buffer.meshBuffer.indices[i + 1] = i + 3;
                buffer.meshBuffer.indices[i + 2] = i + 2;
                buffer.meshBuffer.indices[i + 3] = i + 1;
            }
            buffer.meshBuffer.UpdateIndices();
            return buffer;
        }

        public virtual void RemoveBuffer(BillboardBuffer billboardBuffer) {
            if (m_EffectMeshBuffer == null) { return; }

            billboardBuffer.enabled = false;
            int idx = m_BillboardBufferList.IndexOf(billboardBuffer);
            if (idx >= 0) {
                m_EffectMeshBuffer.RemoveBuffer(billboardBuffer.meshBuffer);
                billboardBuffer.meshBuffer = null;
                m_BillboardBufferList.RemoveAt(idx);
            }
        }

        private void UpdateMesh() {
            if (m_BillboardBufferList.Count == 0) { return; }

            Camera camera = Camera.current;
            if (camera == null) { return; }

            Transform camTransform = camera.transform;
            for (int i = 0; i < m_BillboardBufferList.Count; i++) {
                BillboardBuffer buffer = m_BillboardBufferList[i];
                if (buffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer mesh = buffer.meshBuffer;
                if (mesh == null) { continue; }

                if (!buffer.enabled) {
                    mesh.enabled = false;
                    continue;
                }

                Quaternion lhs;
                switch (buffer.rotateType) {
                    case eRotateType.BillboardX: {
                        Vector3 forward = camTransform.forward;
                        forward.x = 0f;
                        lhs = Quaternion.LookRotation(forward.normalized);
                        break;
                    }
                    case eRotateType.BillboardY: {
                        Vector3 forward = camTransform.forward;
                        forward.y = 0f;
                        lhs = Quaternion.LookRotation(forward.normalized);
                        break;
                    }
                    case eRotateType.BillboardZ: {
                        Vector3 forward = camTransform.forward;
                        forward.z = 0f;
                        lhs = Quaternion.LookRotation(forward.normalized);
                        break;
                    }
                    case eRotateType.X:
                        lhs = Quaternion.Euler(0f, 90f, 0f);
                        break;
                    case eRotateType.Y:
                        lhs = Quaternion.Euler(90f, 0f, 0f);
                        break;
                    case eRotateType.Z:
                        lhs = Quaternion.Euler(0f, 0f, 0f);
                        break;
                    default:
                        lhs = camTransform.rotation;
                        break;
                }
                for (int j = 0; j < buffer.billboardNum; j++) {
                    int v = j * 4;
                    if (buffer.isDirtyPositions || buffer.isDirtySizes || buffer.isDirtyRots) {
                        mesh.vertices[v].x     = buffer.sizes[j].x * -0.5f;
                        mesh.vertices[v].y     = buffer.sizes[j].y * -0.5f;
                        mesh.vertices[v].z     = 0f;
                        mesh.vertices[v + 1].x = buffer.sizes[j].x * 0.5f;
                        mesh.vertices[v + 1].y = buffer.sizes[j].y * -0.5f;
                        mesh.vertices[v + 1].z = 0f;
                        mesh.vertices[v + 2].x = buffer.sizes[j].x * 0.5f;
                        mesh.vertices[v + 2].y = buffer.sizes[j].y * 0.5f;
                        mesh.vertices[v + 2].z = 0f;
                        mesh.vertices[v + 3].x = buffer.sizes[j].x * -0.5f;
                        mesh.vertices[v + 3].y = buffer.sizes[j].y * 0.5f;
                        mesh.vertices[v + 3].z = 0f;

                        Quaternion rhs = Quaternion.Euler(0f, 0f, buffer.rots[j]);
                        mesh.vertices[v]     = lhs * rhs * mesh.vertices[v] + buffer.positions[j];
                        mesh.vertices[v + 1] = lhs * rhs * mesh.vertices[v + 1] + buffer.positions[j];
                        mesh.vertices[v + 2] = lhs * rhs * mesh.vertices[v + 2] + buffer.positions[j];
                        mesh.vertices[v + 3] = lhs * rhs * mesh.vertices[v + 3] + buffer.positions[j];
                    }
                    if (buffer.isDirtyUVs) {
                        mesh.UVs[v].x     = buffer.UVs[j].x;
                        mesh.UVs[v].y     = buffer.UVs[j].y;
                        mesh.UVs[v + 1].x = buffer.UVs[j].x + buffer.UVs[j].width;
                        mesh.UVs[v + 1].y = buffer.UVs[j].y;
                        mesh.UVs[v + 2].x = buffer.UVs[j].x + buffer.UVs[j].width;
                        mesh.UVs[v + 2].y = buffer.UVs[j].y + buffer.UVs[j].height;
                        mesh.UVs[v + 3].x = buffer.UVs[j].x;
                        mesh.UVs[v + 3].y = buffer.UVs[j].y + buffer.UVs[j].height;
                    }
                    if (m_EffectMeshBuffer.isUsableSubUVsFix) {
                        mesh.SubUVs[v].x     = buffer.SubUVs[j].x;
                        mesh.SubUVs[v].y     = buffer.SubUVs[j].y;
                        mesh.SubUVs[v + 1].x = buffer.SubUVs[j].x + buffer.SubUVs[j].width;
                        mesh.SubUVs[v + 1].y = buffer.SubUVs[j].y;
                        mesh.SubUVs[v + 2].x = buffer.SubUVs[j].x + buffer.SubUVs[j].width;
                        mesh.SubUVs[v + 2].y = buffer.SubUVs[j].y + buffer.SubUVs[j].height;
                        mesh.SubUVs[v + 3].x = buffer.SubUVs[j].x;
                        mesh.SubUVs[v + 3].y = buffer.SubUVs[j].y + buffer.SubUVs[j].height;
                    }
                    if (buffer.isDirtyColors) {
                        mesh.colors[v]     = buffer.colors[j];
                        mesh.colors[v + 1] = buffer.colors1[j];
                        mesh.colors[v + 2] = buffer.colors2[j];
                        mesh.colors[v + 3] = buffer.colors3[j];
                    }
                }
                if (buffer.isDirtyPositions || buffer.isDirtySizes || buffer.isDirtyRots) {
                    mesh.UpdateVertices();
                    buffer.isDirtyPositions = false;
                    buffer.isDirtySizes = false;
                    buffer.isDirtyRots = false;
                }
                if (buffer.isDirtyUVs) {
                    mesh.UpdateUVs();
                    buffer.isDirtyUVs = false;
                }
                if (buffer.isDirtySubUVs) {
                    mesh.UpdateSubUVs();
                    buffer.isDirtyUVs = false;
                }
                if (buffer.isDirtyColors) {
                    mesh.UpdateColors();
                    buffer.isDirtyColors = false;
                }
                mesh.enabled = true;
            }
        }
        private void UpdateAlways() {
            int activeBuffers = 0;
            for (int i = 0; i < m_BillboardBufferList.Count; i++) {
                BillboardBuffer billboardBuffer = m_BillboardBufferList[i];
                if (billboardBuffer != null) {
                    EffectMeshBuffer.MeshBuffer meshBuffer = billboardBuffer.meshBuffer;
                    if (meshBuffer != null) {
                        if (billboardBuffer.enabled) {
                            meshBuffer.enabled = true;
                            activeBuffers++;
                        } else {
                            meshBuffer.enabled = false;
                        }
                    }
                }
            }
            if (activeBuffers == 0) {
                enabled = false;
            }
        }
        private void Update() {
            UpdateAlways();
        }

        public enum eRotateType : byte {
            BillboardXYZ,
            BillboardX,
            BillboardY,
            BillboardZ,
            X,
            Y,
            Z,
            Max
        }

        public class BillboardBuffer {
            internal PrimBillboardBuffer parent;
            internal EffectMeshBuffer.MeshBuffer meshBuffer;

            public Vector3[] positions;
            public Vector2[] sizes;
            public float[] rots;
            public Rect[] UVs;
            public Rect[] SubUVs;
            public Color[] colors;
            public Color[] colors1;
            public Color[] colors2;
            public Color[] colors3;
            public eRotateType rotateType;
            public int billboardNum;

            internal bool isDirtyPositions;
            internal bool isDirtySizes;
            internal bool isDirtyRots;
            internal bool isDirtyUVs;
            internal bool isDirtySubUVs;
            internal bool isDirtyColors;
            internal bool m_isShareVertexColor = true;

            private bool m_enabled;

            public bool isShareVertexColor => m_isShareVertexColor;

            public bool enabled {
                get => m_enabled;
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        parent.RenderFlg = true;
                        parent.AddActiveBufferList(this);
                    } else {
                        parent.RemoveActiveBufferList(this);
                    }
                    parent.m_maxTriangleBufferChanged = true;
                    UpdatePositions();
                    UpdateSizes();
                    UpdateRots();
                    UpdateColors();
                }
            }

            public void Remove() { }

            public void UpdatePositions() {
                isDirtyPositions = true;
            }

            public void UpdateSizes() {
                isDirtySizes = true;
            }

            public void UpdateRots() {
                isDirtyRots = true;
            }

            public void UpdateUVs() {
                isDirtyUVs = true;
            }

            public void UpdateSubUVs() {
                isDirtySubUVs = true;
            }

            public void UpdateColors() {
                isDirtyColors = true;
            }

            public void Minimize() { }
        }
    }
}
