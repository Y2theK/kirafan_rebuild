﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x020012B1 RID: 4785
	[Token(Token = "0x2000C6C")]
	[StructLayout(3)]
	public class PrimCircleBuffer : MonoBehaviour
	{
		// Token: 0x0600615D RID: 24925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A17")]
		[Address(RVA = "0x1014C5718", Offset = "0x14C5718", VA = "0x1014C5718")]
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
		}

		// Token: 0x0600615E RID: 24926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A18")]
		[Address(RVA = "0x1014C57CC", Offset = "0x14C57CC", VA = "0x1014C57CC")]
		public void ClearExternalMeshBuffer()
		{
		}

		// Token: 0x0600615F RID: 24927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A19")]
		[Address(RVA = "0x1014C5A40", Offset = "0x14C5A40", VA = "0x1014C5A40")]
		private void OnDestroy()
		{
		}

		// Token: 0x06006160 RID: 24928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A1A")]
		[Address(RVA = "0x1014C5A44", Offset = "0x14C5A44", VA = "0x1014C5A44")]
		internal void AddActiveBufferList(PrimCircleBuffer.CircleBuffer meshBuffer)
		{
		}

		// Token: 0x06006161 RID: 24929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A1B")]
		[Address(RVA = "0x1014C5A84", Offset = "0x14C5A84", VA = "0x1014C5A84")]
		internal void RemoveActiveBufferList(PrimCircleBuffer.CircleBuffer meshBuffer)
		{
		}

		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x06006163 RID: 24931 RVA: 0x00020268 File Offset: 0x0001E468
		// (set) Token: 0x06006162 RID: 24930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700061F")]
		public bool m_maxTriangleBufferChanged
		{
			[Token(Token = "0x6005A1D")]
			[Address(RVA = "0x1014C5ACC", Offset = "0x14C5ACC", VA = "0x1014C5ACC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A1C")]
			[Address(RVA = "0x1014C5AC4", Offset = "0x14C5AC4", VA = "0x1014C5AC4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x06006165 RID: 24933 RVA: 0x00020280 File Offset: 0x0001E480
		// (set) Token: 0x06006164 RID: 24932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000620")]
		public bool RenderFlg
		{
			[Token(Token = "0x6005A1F")]
			[Address(RVA = "0x1014C5ADC", Offset = "0x14C5ADC", VA = "0x1014C5ADC")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A1E")]
			[Address(RVA = "0x1014C5AD4", Offset = "0x14C5AD4", VA = "0x1014C5AD4")]
			set
			{
			}
		}

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x06006167 RID: 24935 RVA: 0x00020298 File Offset: 0x0001E498
		// (set) Token: 0x06006166 RID: 24934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000621")]
		public bool bufferChangeFlg
		{
			[Token(Token = "0x6005A21")]
			[Address(RVA = "0x1014C5AEC", Offset = "0x14C5AEC", VA = "0x1014C5AEC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A20")]
			[Address(RVA = "0x1014C5AE4", Offset = "0x14C5AE4", VA = "0x1014C5AE4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06006168 RID: 24936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A22")]
		[Address(RVA = "0x1014C5AF4", Offset = "0x14C5AF4", VA = "0x1014C5AF4")]
		private void Start()
		{
		}

		// Token: 0x06006169 RID: 24937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A23")]
		[Address(RVA = "0x1014C5AF8", Offset = "0x14C5AF8", VA = "0x1014C5AF8")]
		private void Setup()
		{
		}

		// Token: 0x0600616A RID: 24938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005A24")]
		[Address(RVA = "0x1014C5BC0", Offset = "0x14C5BC0", VA = "0x1014C5BC0", Slot = "4")]
		public virtual PrimCircleBuffer.CircleBuffer AddBuffer(int vtxNum)
		{
			return null;
		}

		// Token: 0x0600616B RID: 24939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A25")]
		[Address(RVA = "0x1014C5EC4", Offset = "0x14C5EC4", VA = "0x1014C5EC4", Slot = "5")]
		public virtual void RemoveBuffer(PrimCircleBuffer.CircleBuffer circleBuffer)
		{
		}

		// Token: 0x0600616C RID: 24940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A26")]
		[Address(RVA = "0x1014C600C", Offset = "0x14C600C", VA = "0x1014C600C")]
		private void UpdateMesh()
		{
		}

		// Token: 0x0600616D RID: 24941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A27")]
		[Address(RVA = "0x1014C6C94", Offset = "0x14C6C94", VA = "0x1014C6C94")]
		private void Update()
		{
		}

		// Token: 0x0600616E RID: 24942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A28")]
		[Address(RVA = "0x1014C6C98", Offset = "0x14C6C98", VA = "0x1014C6C98")]
		public PrimCircleBuffer()
		{
		}

		// Token: 0x0400706F RID: 28783
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004F54")]
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x04007070 RID: 28784
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004F55")]
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04007071 RID: 28785
		[Token(Token = "0x4004F56")]
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x04007072 RID: 28786
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004F57")]
		protected ArrayList m_CircleBufferList;

		// Token: 0x04007073 RID: 28787
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004F58")]
		protected ArrayList m_activeCircleBufferList;

		// Token: 0x020012B2 RID: 4786
		[Token(Token = "0x20012E9")]
		[Serializable]
		public class CircleBuffer
		{
			// Token: 0x0600616F RID: 24943 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063E4")]
			[Address(RVA = "0x1015E7D74", Offset = "0x15E7D74", VA = "0x1015E7D74")]
			public void UpdatePositions()
			{
			}

			// Token: 0x06006170 RID: 24944 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063E5")]
			[Address(RVA = "0x1015E7D80", Offset = "0x15E7D80", VA = "0x1015E7D80")]
			public void UpdateUVs()
			{
			}

			// Token: 0x06006171 RID: 24945 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063E6")]
			[Address(RVA = "0x1015E7D8C", Offset = "0x15E7D8C", VA = "0x1015E7D8C")]
			public void UpdateColors()
			{
			}

			// Token: 0x17000681 RID: 1665
			// (get) Token: 0x06006172 RID: 24946 RVA: 0x000202B0 File Offset: 0x0001E4B0
			// (set) Token: 0x06006173 RID: 24947 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006D5")]
			public bool enabled
			{
				[Token(Token = "0x60063E7")]
				[Address(RVA = "0x1015E7D98", Offset = "0x15E7D98", VA = "0x1015E7D98")]
				get
				{
					return default(bool);
				}
				[Token(Token = "0x60063E8")]
				[Address(RVA = "0x1015E7DA0", Offset = "0x15E7DA0", VA = "0x1015E7DA0")]
				set
				{
				}
			}

			// Token: 0x06006174 RID: 24948 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063E9")]
			[Address(RVA = "0x1015E7E6C", Offset = "0x15E7E6C", VA = "0x1015E7E6C")]
			public CircleBuffer()
			{
			}

			// Token: 0x04007076 RID: 28790
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007403")]
			internal PrimCircleBuffer parent;

			// Token: 0x04007077 RID: 28791
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007404")]
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04007078 RID: 28792
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007405")]
			internal int vtxNum;

			// Token: 0x04007079 RID: 28793
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4007406")]
			internal bool isDirtyPositions;

			// Token: 0x0400707A RID: 28794
			[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
			[Token(Token = "0x4007407")]
			internal bool isDirtyUVs;

			// Token: 0x0400707B RID: 28795
			[Cpp2IlInjected.FieldOffset(Offset = "0x26")]
			[Token(Token = "0x4007408")]
			internal bool isDirtyColors;

			// Token: 0x0400707C RID: 28796
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007409")]
			public Vector3 m_Position;

			// Token: 0x0400707D RID: 28797
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x400740A")]
			public Vector3 m_Rotation;

			// Token: 0x0400707E RID: 28798
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400740B")]
			public Vector3 m_Scale;

			// Token: 0x0400707F RID: 28799
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x400740C")]
			public float m_InStartAngle;

			// Token: 0x04007080 RID: 28800
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400740D")]
			public float m_InEndAngle;

			// Token: 0x04007081 RID: 28801
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x400740E")]
			public float m_OutStartAngle;

			// Token: 0x04007082 RID: 28802
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x400740F")]
			public float m_OutEndAngle;

			// Token: 0x04007083 RID: 28803
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x4007410")]
			public float m_InR;

			// Token: 0x04007084 RID: 28804
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4007411")]
			public float m_OutR;

			// Token: 0x04007085 RID: 28805
			[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
			[Token(Token = "0x4007412")]
			public float m_InH;

			// Token: 0x04007086 RID: 28806
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4007413")]
			public float m_OutH;

			// Token: 0x04007087 RID: 28807
			[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
			[Token(Token = "0x4007414")]
			public float m_OffsetStartInR;

			// Token: 0x04007088 RID: 28808
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4007415")]
			public float m_OffsetEndInR;

			// Token: 0x04007089 RID: 28809
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x4007416")]
			public float m_OffsetStartOutR;

			// Token: 0x0400708A RID: 28810
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x4007417")]
			public float m_OffsetEndOutR;

			// Token: 0x0400708B RID: 28811
			[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
			[Token(Token = "0x4007418")]
			public float m_OffsetStartInH;

			// Token: 0x0400708C RID: 28812
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x4007419")]
			public float m_OffsetEndInH;

			// Token: 0x0400708D RID: 28813
			[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
			[Token(Token = "0x400741A")]
			public float m_OffsetStartOutH;

			// Token: 0x0400708E RID: 28814
			[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
			[Token(Token = "0x400741B")]
			public float m_OffsetEndOutH;

			// Token: 0x0400708F RID: 28815
			[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
			[Token(Token = "0x400741C")]
			public float m_AddRandomOffsetStartInR;

			// Token: 0x04007090 RID: 28816
			[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
			[Token(Token = "0x400741D")]
			public float m_AddRandomOffsetEndInR;

			// Token: 0x04007091 RID: 28817
			[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
			[Token(Token = "0x400741E")]
			public float m_AddRandomOffsetStartOutR;

			// Token: 0x04007092 RID: 28818
			[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
			[Token(Token = "0x400741F")]
			public float m_AddRandomOffsetEndOutR;

			// Token: 0x04007093 RID: 28819
			[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
			[Token(Token = "0x4007420")]
			public float m_AddRandomOffsetStartInH;

			// Token: 0x04007094 RID: 28820
			[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
			[Token(Token = "0x4007421")]
			public float m_AddRandomOffsetEndInH;

			// Token: 0x04007095 RID: 28821
			[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
			[Token(Token = "0x4007422")]
			public float m_AddRandomOffsetStartOutH;

			// Token: 0x04007096 RID: 28822
			[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
			[Token(Token = "0x4007423")]
			public float m_AddRandomOffsetEndOutH;

			// Token: 0x04007097 RID: 28823
			[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
			[Token(Token = "0x4007424")]
			public Rect m_UVRect;

			// Token: 0x04007098 RID: 28824
			[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
			[Token(Token = "0x4007425")]
			public bool m_UVReverse;

			// Token: 0x04007099 RID: 28825
			[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
			[Token(Token = "0x4007426")]
			public Color m_ColorStartInR;

			// Token: 0x0400709A RID: 28826
			[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
			[Token(Token = "0x4007427")]
			public Color m_ColorEndInR;

			// Token: 0x0400709B RID: 28827
			[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
			[Token(Token = "0x4007428")]
			public Color m_ColorStartOutR;

			// Token: 0x0400709C RID: 28828
			[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
			[Token(Token = "0x4007429")]
			public Color m_ColorEndOutR;

			// Token: 0x0400709D RID: 28829
			[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
			[Token(Token = "0x400742A")]
			private bool m_enabled;
		}
	}
}
