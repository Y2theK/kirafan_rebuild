﻿using System.Collections;
using UnityEngine;

namespace Meige {
    public class PrimConfettiBuffer : MonoBehaviour {
        private const int MAX_ACTIVE_MESHBUFFER = 32;

        private EffectMeshBuffer m_EffectMeshBuffer;
        protected bool m_MeshBufferIsSelf;
        protected ArrayList m_ConfettiBufferList = new ArrayList();
        protected ArrayList m_activeConfettiBufferList = new ArrayList(MAX_ACTIVE_MESHBUFFER);

        public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer) {
            if (m_EffectMeshBuffer == null && meshBuffer != null) {
                m_EffectMeshBuffer = meshBuffer;
            }
        }

        public void ClearExternalMeshBuffer() {
            if (m_EffectMeshBuffer != null && !m_MeshBufferIsSelf) {
                foreach (ConfettiBuffer confettiBuffer in m_ConfettiBufferList) {
                    m_EffectMeshBuffer.RemoveBuffer(confettiBuffer.meshBuffer);
                }
                m_EffectMeshBuffer = null;
            }
        }

        private void OnDestroy() {
            ClearExternalMeshBuffer();
        }

        internal void AddActiveBufferList(ConfettiBuffer meshBuffer) {
            m_activeConfettiBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(ConfettiBuffer meshBuffer) {
            m_activeConfettiBufferList.Remove(meshBuffer);
        }

        public bool m_maxTriangleBufferChanged { get; set; }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public bool bufferChangeFlg { get; set; }

        private void Start() {
            Setup();
        }

        private void Setup() {
            if (m_EffectMeshBuffer == null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = gameObject.AddComponent<EffectMeshBuffer>();
                m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
                m_MeshBufferIsSelf = true;
            }
        }

        public virtual ConfettiBuffer AddBuffer(int confettiNum) {
            Setup();
            ConfettiBuffer confettiBuffer = new ConfettiBuffer();
            confettiBuffer.positions = new Vector3[confettiNum];
            confettiBuffer.sizes = new Vector2[confettiNum];
            confettiBuffer.rots = new Vector3[confettiNum];
            confettiBuffer.UVs = new Rect[confettiNum];
            confettiBuffer.colors = new Color[confettiNum];
            confettiBuffer.confettiNum = confettiNum;
            confettiBuffer.parent = this;
            confettiBuffer.enabled = false;
            m_ConfettiBufferList.Add(confettiBuffer);
            confettiBuffer.UpdatePositions();
            confettiBuffer.UpdateSizes();
            confettiBuffer.UpdateRots();
            confettiBuffer.UpdateUVs();
            confettiBuffer.UpdateColors();
            bufferChangeFlg = true;
            confettiBuffer.meshBuffer = m_EffectMeshBuffer.AddBuffer(confettiNum * 4, confettiNum * 4);
            for (int i = 0; i < confettiNum; i += 4) {
                confettiBuffer.meshBuffer.indices[i] = i;
                confettiBuffer.meshBuffer.indices[i + 1] = i + 3;
                confettiBuffer.meshBuffer.indices[i + 2] = i + 2;
                confettiBuffer.meshBuffer.indices[i + 3] = i + 1;
            }
            confettiBuffer.meshBuffer.UpdateIndices();
            return confettiBuffer;
        }

        public virtual void RemoveBuffer(ConfettiBuffer confettiBuffer) {
            if (m_EffectMeshBuffer == null) { return; }

            confettiBuffer.enabled = false;
            int idx = m_ConfettiBufferList.IndexOf(confettiBuffer);
            if (idx >= 0) {
                m_EffectMeshBuffer.RemoveBuffer(confettiBuffer.meshBuffer);
                confettiBuffer.meshBuffer = null;
                m_ConfettiBufferList.RemoveAt(idx);
            }
        }

        private void UpdateMesh() {
            if (m_ConfettiBufferList.Count == 0) { return; }

            int activeBuffers = 0;
            for (int i = 0; i < m_ConfettiBufferList.Count; i++) {
                ConfettiBuffer confettiBuffer = m_ConfettiBufferList[i] as ConfettiBuffer;
                if (confettiBuffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer meshBuffer = confettiBuffer.meshBuffer;
                if (meshBuffer == null) { continue; }

                if (confettiBuffer.enabled) {
                    for (int j = 0; j < confettiBuffer.confettiNum; j++) {
                        int pIdx = j * 4;
                        if (confettiBuffer.isDirtyPositions || confettiBuffer.isDirtySizes || confettiBuffer.isDirtyRots) {
                            meshBuffer.vertices[pIdx].x = confettiBuffer.sizes[j].x * -0.5f;
                            meshBuffer.vertices[pIdx].y = confettiBuffer.sizes[j].y * -0.5f;
                            meshBuffer.vertices[pIdx].z = 0f;
                            meshBuffer.vertices[pIdx + 1].x = confettiBuffer.sizes[j].x * 0.5f;
                            meshBuffer.vertices[pIdx + 1].y = confettiBuffer.sizes[j].y * -0.5f;
                            meshBuffer.vertices[pIdx + 1].z = 0f;
                            meshBuffer.vertices[pIdx + 2].x = confettiBuffer.sizes[j].x * 0.5f;
                            meshBuffer.vertices[pIdx + 2].y = confettiBuffer.sizes[j].y * 0.5f;
                            meshBuffer.vertices[pIdx + 2].z = 0f;
                            meshBuffer.vertices[pIdx + 3].x = confettiBuffer.sizes[j].x * -0.5f;
                            meshBuffer.vertices[pIdx + 3].y = confettiBuffer.sizes[j].y * 0.5f;
                            meshBuffer.vertices[pIdx + 3].z = 0f;
                            Quaternion rotation = Quaternion.Euler(confettiBuffer.rots[j]);
                            meshBuffer.vertices[pIdx] = rotation * meshBuffer.vertices[pIdx] + confettiBuffer.positions[j];
                            meshBuffer.vertices[pIdx + 1] = rotation * meshBuffer.vertices[pIdx + 1] + confettiBuffer.positions[j];
                            meshBuffer.vertices[pIdx + 2] = rotation * meshBuffer.vertices[pIdx + 2] + confettiBuffer.positions[j];
                            meshBuffer.vertices[pIdx + 3] = rotation * meshBuffer.vertices[pIdx + 3] + confettiBuffer.positions[j];
                        }
                        if (confettiBuffer.isDirtyUVs) {
                            meshBuffer.UVs[pIdx].x = confettiBuffer.UVs[j].x;
                            meshBuffer.UVs[pIdx].y = confettiBuffer.UVs[j].y;
                            meshBuffer.UVs[pIdx + 1].x = confettiBuffer.UVs[j].x + confettiBuffer.UVs[j].width;
                            meshBuffer.UVs[pIdx + 1].y = confettiBuffer.UVs[j].y;
                            meshBuffer.UVs[pIdx + 2].x = confettiBuffer.UVs[j].x + confettiBuffer.UVs[j].width;
                            meshBuffer.UVs[pIdx + 2].y = confettiBuffer.UVs[j].y + confettiBuffer.UVs[j].height;
                            meshBuffer.UVs[pIdx + 3].x = confettiBuffer.UVs[j].x;
                            meshBuffer.UVs[pIdx + 3].y = confettiBuffer.UVs[j].y + confettiBuffer.UVs[j].height;
                        }
                        if (confettiBuffer.isDirtyColors) {
                            meshBuffer.colors[pIdx] = confettiBuffer.colors[j];
                            meshBuffer.colors[pIdx + 1] = confettiBuffer.colors[j];
                            meshBuffer.colors[pIdx + 2] = confettiBuffer.colors[j];
                            meshBuffer.colors[pIdx + 3] = confettiBuffer.colors[j];
                        }
                    }
                    if (confettiBuffer.isDirtyPositions || confettiBuffer.isDirtySizes || confettiBuffer.isDirtyRots) {
                        meshBuffer.UpdateVertices();
                        confettiBuffer.isDirtyPositions = false;
                        confettiBuffer.isDirtySizes = false;
                        confettiBuffer.isDirtyRots = false;
                    }
                    if (confettiBuffer.isDirtyUVs) {
                        meshBuffer.UpdateUVs();
                        confettiBuffer.isDirtyUVs = false;
                    }
                    if (confettiBuffer.isDirtyColors) {
                        meshBuffer.UpdateColors();
                        confettiBuffer.isDirtyColors = false;
                    }
                    activeBuffers++;
                    meshBuffer.enabled = true;
                } else {
                    meshBuffer.enabled = false;
                }
            }

            if (activeBuffers == 0) {
                enabled = false;
            }
        }

        private void Update() {
            UpdateMesh();
        }

        private void OnWillRenderObject() { }

        public class ConfettiBuffer {
            internal PrimConfettiBuffer parent;
            internal EffectMeshBuffer.MeshBuffer meshBuffer;
            internal bool isDirtyPositions;
            internal bool isDirtySizes;
            internal bool isDirtyRots;
            internal bool isDirtyUVs;
            internal bool isDirtyColors;

            private bool m_enabled;

            public Vector3[] positions;
            public Vector2[] sizes;
            public Vector3[] rots;
            public Rect[] UVs;
            public Color[] colors;
            public int confettiNum;

            public bool enabled {
                get {
                    return m_enabled;
                }
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        parent.RenderFlg = true;
                        parent.AddActiveBufferList(this);
                    } else {
                        parent.RemoveActiveBufferList(this);
                    }
                    parent.m_maxTriangleBufferChanged = true;
                    UpdatePositions();
                    UpdateSizes();
                    UpdateRots();
                    UpdateColors();
                }
            }

            public void Remove() { }

            public void UpdatePositions() {
                isDirtyPositions = true;
            }

            public void UpdateSizes() {
                isDirtySizes = true;
            }

            public void UpdateRots() {
                isDirtyRots = true;
            }

            public void UpdateUVs() {
                isDirtyUVs = true;
            }

            public void UpdateColors() {
                isDirtyColors = true;
            }

            public void Minimize() { }
        }
    }
}
