﻿using System.Collections;
using UnityEngine;

namespace Meige {
    public class PrimLineBuffer : MonoBehaviour {
        private const int MAX_ACTIVE_MESHBUFFER = 32;

        private EffectMeshBuffer m_EffectMeshBuffer;
        protected bool m_MeshBufferIsSelf;
        protected ArrayList m_LineBufferList = new ArrayList();
        protected ArrayList m_activeLineBufferList = new ArrayList(MAX_ACTIVE_MESHBUFFER);

        public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer) {
            if (m_EffectMeshBuffer == null) {
                m_EffectMeshBuffer = meshBuffer;
            }
        }

        public void ClearExternalMeshBuffer() {
            if (m_EffectMeshBuffer != null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = null;
            }
        }

        private void OnDestroy() {
            ClearExternalMeshBuffer();
        }

        internal void AddActiveBufferList(LineBuffer meshBuffer) {
            m_activeLineBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(LineBuffer meshBuffer) {
            m_activeLineBufferList.Remove(meshBuffer);
        }

        public bool m_maxTriangleBufferChanged { get; set; }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public bool bufferChangeFlg { get; set; }

        private void Start() {
            Setup();
        }

        private void Setup() {
            if (m_EffectMeshBuffer == null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = gameObject.AddComponent<EffectMeshBuffer>();
                m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Lines;
                m_MeshBufferIsSelf = true;
            }
        }

        public virtual LineBuffer AddBuffer(int lineNum, int pointNum) {
            Setup();
            LineBuffer lineBuffer = new LineBuffer();
            lineBuffer.lines = new LineBuffer.Line[lineNum];
            lineBuffer.positions = new Vector3[lineNum * pointNum];
            lineBuffer.widths = new float[lineNum * pointNum];
            lineBuffer.UVs = new Vector2[lineNum * pointNum];
            lineBuffer.colors = new Color[lineNum * pointNum];
            lineBuffer.lineNum = lineNum;
            lineBuffer.pointNum = pointNum;
            lineBuffer.parent = this;
            lineBuffer.enabled = false;
            m_LineBufferList.Add(lineBuffer);
            bufferChangeFlg = true;
            lineBuffer.meshBuffer = m_EffectMeshBuffer.AddBuffer(lineNum * pointNum, lineNum * (pointNum - 1) * 2);
            int iIdx = 0;
            int vIdx = 0;
            for (int i = 0; i < lineNum; i++) {
                lineBuffer.lines[i] = new LineBuffer.Line();
                lineBuffer.lines[i].parent = lineBuffer;
                lineBuffer.lines[i].topPointIdx = vIdx;
                lineBuffer.meshBuffer.indices[iIdx++] = vIdx;
                vIdx++;
                for (int j = 0; j < pointNum - 2; j++) {
                    lineBuffer.meshBuffer.indices[iIdx++] = vIdx;
                    lineBuffer.meshBuffer.indices[iIdx++] = vIdx;
                    vIdx++;
                }
                lineBuffer.meshBuffer.indices[iIdx++] = vIdx;
                vIdx++;
            }
            lineBuffer.UpdatePositions();
            lineBuffer.UpdateWidths();
            lineBuffer.UpdateUVs();
            lineBuffer.UpdateColors();
            lineBuffer.meshBuffer.UpdateIndices();
            return lineBuffer;
        }

        public virtual void RemoveBuffer(LineBuffer lineBuffer) {
            if (m_EffectMeshBuffer == null) { return; }

            lineBuffer.enabled = false;
            int idx = m_LineBufferList.IndexOf(lineBuffer);
            if (idx >= 0) {
                m_EffectMeshBuffer.RemoveBuffer(lineBuffer.meshBuffer);
                lineBuffer.meshBuffer = null;
                m_LineBufferList.RemoveAt(idx);
            }
        }

        private void UpdateMesh() {
            if (m_LineBufferList.Count == 0) { return; }

            int activeBuffers = 0;
            for (int i = 0; i < m_LineBufferList.Count; i++) {
                LineBuffer lineBuffer = m_LineBufferList[i] as LineBuffer;
                if (lineBuffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer meshBuffer = lineBuffer.meshBuffer;
                if (meshBuffer == null) { continue; }

                if (lineBuffer.enabled) {
                    for (int j = 0; j < lineBuffer.lineNum; j++) {
                        LineBuffer.Line line = lineBuffer.GetLine(j);
                        if (line.isDirtyPositions) {
                            for (int k = 0; k < lineBuffer.pointNum; k++) {
                                meshBuffer.vertices[j * lineBuffer.pointNum + k] = line.GetPosition(k);
                            }
                            line.isDirtyPositions = false;
                            meshBuffer.UpdateVertices();
                        }
                        if (line.isDirtyUVs) {
                            for (int k = 0; k < lineBuffer.pointNum; k++) {
                                meshBuffer.UVs[j * lineBuffer.pointNum + k] = line.GetUV(k);
                            }
                            line.isDirtyUVs = false;
                            meshBuffer.UpdateUVs();
                        }
                        if (line.isDirtyColors) {
                            for (int k = 0; k < lineBuffer.pointNum; k++) {
                                meshBuffer.colors[j * lineBuffer.pointNum + k] = line.GetColor(k);
                            }
                            line.isDirtyColors = false;
                            meshBuffer.UpdateColors();
                        }
                    }
                    activeBuffers++;
                    meshBuffer.enabled = true;
                } else {
                    meshBuffer.enabled = false;
                }
            }

            if (activeBuffers == 0) {
                enabled = false;
            }
        }

        private void Update() {
            UpdateMesh();
        }

        private void OnWillRenderObject() { }

        public class LineBuffer {
            internal PrimLineBuffer parent;
            internal EffectMeshBuffer.MeshBuffer meshBuffer;
            internal Line[] lines;

            public Vector3[] positions;
            public float[] widths;
            public float[] rots;
            public Vector2[] UVs;
            public Color[] colors;

            public int lineNum;
            public int pointNum;
            private bool m_enabled;

            public bool enabled {
                get => m_enabled;
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        parent.RenderFlg = true;
                        parent.AddActiveBufferList(this);
                    } else {
                        parent.RemoveActiveBufferList(this);
                    }
                    parent.m_maxTriangleBufferChanged = true;
                    UpdatePositions();
                    UpdateWidths();
                    UpdateColors();
                }
            }

            public Line GetLine(int lineIdx) {
                return lines[lineIdx];
            }

            public void Remove() { }

            public void UpdatePositions() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyPositions = true;
                }
            }

            public void UpdateWidths() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyWidths = true;
                }
            }

            public void UpdateUVs() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyUVs = true;
                }
            }

            public void UpdateColors() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyColors = true;
                }
            }

            public void Minimize() { }

            public class Line {
                internal LineBuffer parent;
                internal bool isDirtyPositions;
                internal bool isDirtyWidths;
                internal bool isDirtyUVs;
                internal bool isDirtyColors;

                public int topPointIdx;

                public void SetPosition(Vector3 pos, int pointIdx) {
                    parent.positions[topPointIdx + pointIdx] = pos;
                    isDirtyPositions = true;
                }

                public Vector3 GetPosition(int pointIdx) {
                    return parent.positions[topPointIdx + pointIdx];
                }

                public void SetWidth(float width, int pointIdx) {
                    parent.widths[topPointIdx + pointIdx] = width;
                    isDirtyWidths = true;
                }

                public float GetWidth(int pointIdx) {
                    return parent.widths[topPointIdx + pointIdx];
                }

                public void SetUV(Vector2 uv, int pointIdx) {
                    parent.UVs[topPointIdx + pointIdx] = uv;
                    isDirtyUVs = true;
                }

                public Vector2 GetUV(int pointIdx) {
                    return parent.UVs[topPointIdx + pointIdx];
                }

                public void SetColor(Color color, int pointIdx) {
                    parent.colors[topPointIdx + pointIdx] = color;
                    isDirtyColors = true;
                }

                public Color GetColor(int pointIdx) {
                    return parent.colors[topPointIdx + pointIdx];
                }
            }
        }
    }
}
