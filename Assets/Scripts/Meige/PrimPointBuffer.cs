﻿using System.Collections;
using UnityEngine;

namespace Meige {
    public class PrimPointBuffer : MonoBehaviour {
        private const int MAX_ACTIVE_MESHBUFFER = 32;

        private EffectMeshBuffer m_EffectMeshBuffer;

        protected bool m_MeshBufferIsSelf;
        protected ArrayList m_PointBufferList = new ArrayList();
        protected ArrayList m_activePointBufferList = new ArrayList(MAX_ACTIVE_MESHBUFFER);

        public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer) {
            if (m_EffectMeshBuffer == null && meshBuffer != null) {
                m_EffectMeshBuffer = meshBuffer;
            }
        }

        public void ClearExternalMeshBuffer() {
            if (m_EffectMeshBuffer != null && !m_MeshBufferIsSelf) {
                foreach (PointBuffer pointBuffer in m_PointBufferList) {
                    m_EffectMeshBuffer.RemoveBuffer(pointBuffer.meshBuffer);
                }
                m_EffectMeshBuffer = null;
            }
        }

        private void OnDestroy() {
            ClearExternalMeshBuffer();
        }

        internal void AddActiveBufferList(PointBuffer meshBuffer) {
            m_activePointBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(PointBuffer meshBuffer) {
            m_activePointBufferList.Remove(meshBuffer);
        }

        public bool m_maxTriangleBufferChanged { get; set; }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public bool bufferChangeFlg { get; set; }

        private void Start() {
            Setup();
        }

        private void Setup() {
            if (m_EffectMeshBuffer == null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = gameObject.AddComponent<EffectMeshBuffer>();
                m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Points;
                m_MeshBufferIsSelf = true;
            }
        }

        public virtual PointBuffer AddBuffer(int pointNum) {
            Setup();
            PointBuffer pointBuffer = new PointBuffer();
            pointBuffer.positions = new Vector3[pointNum];
            pointBuffer.sizes = new float[pointNum];
            pointBuffer.UVs = new Rect[pointNum];
            pointBuffer.colors = new Color[pointNum];
            pointBuffer.pointNum = pointNum;
            pointBuffer.parent = this;
            pointBuffer.enabled = false;
            m_PointBufferList.Add(pointBuffer);
            pointBuffer.UpdatePositions();
            pointBuffer.UpdateSizes();
            pointBuffer.UpdateUVs();
            pointBuffer.UpdateColors();
            bufferChangeFlg = true;
            pointBuffer.meshBuffer = m_EffectMeshBuffer.AddBuffer(pointNum, pointNum);
            for (int i = 0; i < pointNum; i++) {
                pointBuffer.meshBuffer.indices[i] = i;
            }
            pointBuffer.meshBuffer.UpdateIndices();
            return pointBuffer;
        }

        public virtual void RemoveBuffer(PointBuffer billboardBuffer) {
            if (m_EffectMeshBuffer == null) { return; }

            billboardBuffer.enabled = false;
            int idx = m_PointBufferList.IndexOf(billboardBuffer);
            if (idx >= 0) {
                m_EffectMeshBuffer.RemoveBuffer(billboardBuffer.meshBuffer);
                billboardBuffer.meshBuffer = null;
                m_PointBufferList.RemoveAt(idx);
            }
        }

        private void UpdateMesh() {
            if (m_PointBufferList.Count == 0) { return; }

            int activeBuffers = 0;
            for (int i = 0; i < m_PointBufferList.Count; i++) {
                PointBuffer pointBuffer = m_PointBufferList[i] as PointBuffer;
                if (pointBuffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer meshBuffer = pointBuffer.meshBuffer;
                if (meshBuffer == null) { continue; }


                if (pointBuffer.enabled) {
                    for (int j = 0; j < pointBuffer.pointNum; j++) {
                        if (pointBuffer.isDirtyPositions || pointBuffer.isDirtySizes) {
                            meshBuffer.vertices[j] = pointBuffer.positions[j];
                        }
                        if (pointBuffer.isDirtyUVs) {
                            meshBuffer.UVs[j] = pointBuffer.UVs[j].position;
                        }
                        if (pointBuffer.isDirtyColors) {
                            meshBuffer.colors[j] = pointBuffer.colors[j];
                        }
                    }
                    if (pointBuffer.isDirtyPositions || pointBuffer.isDirtySizes) {
                        meshBuffer.UpdateVertices();
                        pointBuffer.isDirtyPositions = false;
                        pointBuffer.isDirtySizes = false;
                    }
                    if (pointBuffer.isDirtyUVs) {
                        meshBuffer.UpdateUVs();
                        pointBuffer.isDirtyUVs = false;
                    }
                    if (pointBuffer.isDirtyColors) {
                        meshBuffer.UpdateColors();
                        pointBuffer.isDirtyColors = false;
                    }
                    activeBuffers++;
                    meshBuffer.enabled = true;
                } else {
                    meshBuffer.enabled = false;
                }
            }

            if (activeBuffers == 0) {
                enabled = false;
            }
        }

        private void Update() {
            UpdateMesh();
        }

        private void OnWillRenderObject() { }
        
        public class PointBuffer {
            internal PrimPointBuffer parent;
            internal EffectMeshBuffer.MeshBuffer meshBuffer;
            internal bool isDirtyPositions;
            internal bool isDirtySizes;
            internal bool isDirtyUVs;
            internal bool isDirtyColors;
            private bool m_enabled;

            public Vector3[] positions;
            public float[] sizes;
            public Rect[] UVs;
            public Color[] colors;
            public int pointNum;

            public bool enabled {
                get => m_enabled;
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        parent.RenderFlg = true;
                        parent.AddActiveBufferList(this);
                    } else {
                        parent.RemoveActiveBufferList(this);
                    }
                    parent.m_maxTriangleBufferChanged = true;
                    UpdatePositions();
                    UpdateSizes();
                    UpdateColors();
                }
            }

            public void Remove() { }

            public void UpdatePositions() {
                isDirtyPositions = true;
            }

            public void UpdateSizes() {
                isDirtySizes = true;
            }

            public void UpdateUVs() {
                isDirtyUVs = true;
            }

            public void UpdateColors() {
                isDirtyColors = true;
            }

            public void Minimize() { }
        }
    }
}
