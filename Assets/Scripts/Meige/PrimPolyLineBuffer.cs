﻿using System.Collections;
using UnityEngine;

namespace Meige {
    public class PrimPolyLineBuffer : MonoBehaviour {
        private const int MAX_ACTIVE_MESHBUFFER = 32;

        private EffectMeshBuffer m_EffectMeshBuffer;
        protected bool m_MeshBufferIsSelf;
        protected bool m_SetupIsEnd;
        protected ArrayList m_PolyLineBufferList = new ArrayList();
        protected ArrayList m_activePolyLineBufferList = new ArrayList(MAX_ACTIVE_MESHBUFFER);

        public bool m_maxTriangleBufferChanged { get; set; }
        public bool bufferChangeFlg { get; set; }

        public bool RenderFlg {
            get => enabled;
            set => enabled = value;
        }

        public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer) {
            if (m_EffectMeshBuffer == null && meshBuffer != null) {
                m_EffectMeshBuffer = meshBuffer;
            }
        }

        public void ClearExternalMeshBuffer() {
            if (m_EffectMeshBuffer != null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer.RemoveHook_OnWillRender(UpdateMesh);
                foreach (PolyLineBuffer polyLineBuffer in m_PolyLineBufferList) {
                    m_EffectMeshBuffer.RemoveBuffer(polyLineBuffer.meshBuffer);
                }
                m_EffectMeshBuffer = null;
            }
        }

        private void OnDestroy() {
            ClearExternalMeshBuffer();
        }

        internal void AddActiveBufferList(PolyLineBuffer meshBuffer) {
            m_activePolyLineBufferList.Add(meshBuffer);
        }

        internal void RemoveActiveBufferList(PolyLineBuffer meshBuffer) {
            m_activePolyLineBufferList.Remove(meshBuffer);
        }

        private void Start() {
            Setup();
        }

        private void Setup() {
            if (m_SetupIsEnd) { return; }

            if (m_EffectMeshBuffer == null && !m_MeshBufferIsSelf) {
                m_EffectMeshBuffer = gameObject.AddComponent<EffectMeshBuffer>();
                m_EffectMeshBuffer.m_MeshTopology = EffectMeshBuffer.Topology.Quads;
                m_MeshBufferIsSelf = true;
            }
            if (m_EffectMeshBuffer != null) {
                m_EffectMeshBuffer.AddHook_OnWillRender(UpdateMesh);
            }
            m_SetupIsEnd = true;
        }

        public virtual PolyLineBuffer AddBuffer(int lineNum, int pointNum) {
            Setup();
            PolyLineBuffer polyLineBuffer = new PolyLineBuffer();
            polyLineBuffer.lines = new PolyLineBuffer.Line[lineNum];
            polyLineBuffer.positions = new Vector3[lineNum * pointNum];
            polyLineBuffer.widths = new float[lineNum * pointNum];
            polyLineBuffer.UVs = new Vector2[lineNum * pointNum];
            polyLineBuffer.UWidths = new float[lineNum * pointNum];
            polyLineBuffer.colors = new Color[lineNum * pointNum];
            polyLineBuffer.lineNum = lineNum;
            polyLineBuffer.pointNum = pointNum;
            polyLineBuffer.parent = this;
            polyLineBuffer.enabled = false;
            m_PolyLineBufferList.Add(polyLineBuffer);
            bufferChangeFlg = true;
            polyLineBuffer.meshBuffer = m_EffectMeshBuffer.AddBuffer(lineNum * pointNum * 2, lineNum * (pointNum - 1) * 4);
            int iIdx = 0;
            int vIdx = 0;
            for (int i = 0; i < lineNum; i++) {
                polyLineBuffer.lines[i] = new PolyLineBuffer.Line();
                polyLineBuffer.lines[i].parent = polyLineBuffer;
                polyLineBuffer.lines[i].topPointIdx = vIdx;
                for (int j = 0; j < pointNum - 1; j++) {
                    polyLineBuffer.meshBuffer.indices[iIdx++] = vIdx * 2;
                    polyLineBuffer.meshBuffer.indices[iIdx++] = vIdx * 2 + 1;
                    polyLineBuffer.meshBuffer.indices[iIdx++] = vIdx * 2 + 3;
                    polyLineBuffer.meshBuffer.indices[iIdx++] = vIdx * 2 + 2;
                    vIdx++;
                }
                vIdx++;
            }
            polyLineBuffer.UpdatePositions();
            polyLineBuffer.UpdateWidths();
            polyLineBuffer.UpdateUVs();
            polyLineBuffer.UpdateColors();
            polyLineBuffer.meshBuffer.UpdateIndices();
            return polyLineBuffer;
        }

        public virtual void RemoveBuffer(PolyLineBuffer lineBuffer) {
            if (m_EffectMeshBuffer == null || lineBuffer == null) { return; }

            lineBuffer.enabled = false;
            int idx = m_PolyLineBufferList.IndexOf(lineBuffer);
            if (idx >= 0) {
                m_EffectMeshBuffer.RemoveBuffer(lineBuffer.meshBuffer);
                lineBuffer.meshBuffer = null;
                m_PolyLineBufferList.RemoveAt(idx);
            }
        }

        private void UpdateMesh() {
            if (m_PolyLineBufferList.Count == 0) { return; }

            Camera current = Camera.current;
            if (current == null) { return; }

            for (int i = 0; i < m_PolyLineBufferList.Count; i++) {
                PolyLineBuffer polyLineBuffer = m_PolyLineBufferList[i] as PolyLineBuffer;
                if (polyLineBuffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer meshBuffer = polyLineBuffer.meshBuffer;
                if (meshBuffer == null) { continue; }

                if (polyLineBuffer.enabled) {
                    for (int j = 0; j < polyLineBuffer.lineNum; j++) {
                        PolyLineBuffer.Line line = polyLineBuffer.GetLine(j);
                        Vector3 rhs = line.outerElement switch {
                            eOuterElement.X => Vector3.right,
                            eOuterElement.Y => Vector3.up,
                            eOuterElement.Z => Vector3.forward,
                            _ => current.worldToCameraMatrix.GetRow(2),
                        };
                        if (line.isDirtyPositions) {
                            for (int k = 0; k < polyLineBuffer.pointNum; k++) {
                                Vector3 delta;
                                if (k < polyLineBuffer.pointNum - 1) {
                                    delta = line.GetPosition(k) - line.GetPosition(k + 1);
                                } else {
                                    delta = line.GetPosition(k - 1) - line.GetPosition(k);
                                }
                                delta = Vector3.Cross(delta, rhs);
                                delta = delta.normalized * line.GetWidth(k) / 2f;
                                meshBuffer.vertices[(j * polyLineBuffer.pointNum + k) * 2] = line.GetPosition(k) + delta;
                                meshBuffer.vertices[(j * polyLineBuffer.pointNum + k) * 2 + 1] = line.GetPosition(k) - delta;
                            }
                            line.isDirtyPositions = false;
                            meshBuffer.UpdateVertices();
                        }
                        if (line.isDirtyUVs) {
                            if (polyLineBuffer.m_UVReverse) {
                                for (int k = 0; k < polyLineBuffer.pointNum; k++) {
                                    meshBuffer.UVs[(j * polyLineBuffer.pointNum + k) * 2] = line.GetUV(k);
                                    meshBuffer.UVs[(j * polyLineBuffer.pointNum + k) * 2 + 1] = line.GetUV(k) + Vector2.right * line.GetUWidth(k);
                                }
                            } else {
                                for (int k = 0; k < polyLineBuffer.pointNum; k++) {
                                    meshBuffer.UVs[(j * polyLineBuffer.pointNum + k) * 2] = line.GetUV(k);
                                    meshBuffer.UVs[(j * polyLineBuffer.pointNum + k) * 2 + 1] = line.GetUV(k) + Vector2.up * line.GetUWidth(k);
                                }
                            }
                            line.isDirtyUVs = false;
                            meshBuffer.UpdateUVs();
                        }
                        if (line.isDirtyColors) {
                            for (int k = 0; k < polyLineBuffer.pointNum; k++) {
                                meshBuffer.colors[(j * polyLineBuffer.pointNum + k) * 2] = line.GetColor(k);
                                meshBuffer.colors[(j * polyLineBuffer.pointNum + k) * 2 + 1] = line.GetColor(k);
                            }
                            line.isDirtyColors = false;
                            meshBuffer.UpdateColors();
                        }
                    }
                    meshBuffer.enabled = true;
                } else {
                    meshBuffer.enabled = false;
                }
            }
        }

        private void UpdateAlways() {
            int activeBuffers = 0;
            for (int i = 0; i < m_PolyLineBufferList.Count; i++) {
                PolyLineBuffer polyLineBuffer = m_PolyLineBufferList[i] as PolyLineBuffer;
                if (polyLineBuffer == null) { continue; }

                EffectMeshBuffer.MeshBuffer meshBuffer = polyLineBuffer.meshBuffer;
                if (meshBuffer == null) { continue; }

                if (polyLineBuffer.enabled) {
                    activeBuffers++;
                    meshBuffer.enabled = true;
                } else {
                    meshBuffer.enabled = false;
                }
            }

            if (activeBuffers == 0) {
                enabled = false;
            }
        }

        private void Update() {
            UpdateAlways();
        }

        private void OnWillRenderObject() { }

        public enum eOuterElement {
            Eye,
            X,
            Y,
            Z
        }

        public class PolyLineBuffer {
            internal PrimPolyLineBuffer parent;
            internal EffectMeshBuffer.MeshBuffer meshBuffer;
            internal Line[] lines;
            internal Vector3[] positions;
            internal float[] widths;
            internal Vector2[] UVs;
            internal float[] UWidths;
            internal Color[] colors;
            internal int lineNum;
            internal int pointNum;

            private bool m_enabled;

            public bool m_UVReverse;

            public bool enabled {
                get => m_enabled;
                set {
                    if (m_enabled == value) { return; }

                    m_enabled = value;
                    if (m_enabled) {
                        parent.RenderFlg = true;
                        parent.AddActiveBufferList(this);
                    } else {
                        parent.RemoveActiveBufferList(this);
                    }
                    parent.m_maxTriangleBufferChanged = true;
                    UpdatePositions();
                    UpdateWidths();
                    UpdateColors();
                }
            }

            public Line GetLine(int lineIdx) {
                return lines[lineIdx];
            }

            public void Remove() { }

            public void UpdatePositions() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyPositions = true;
                }
            }

            public void UpdateWidths() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyWidths = true;
                }
            }

            public void UpdateUVs() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyUVs = true;
                }
            }

            public void UpdateColors() {
                for (int i = 0; i < lineNum; i++) {
                    lines[i].isDirtyColors = true;
                }
            }

            public void Minimize() { }

            public class Line {
                internal PolyLineBuffer parent;
                internal bool isDirtyPositions;
                internal bool isDirtyWidths;
                internal bool isDirtyUVs;
                internal bool isDirtyColors;

                public int topPointIdx;
                public eOuterElement outerElement;

                public void SetPosition(Vector3 pos, int pointIdx) {
                    parent.positions[topPointIdx + pointIdx] = pos;
                    isDirtyPositions = true;
                }

                public Vector3 GetPosition(int pointIdx) {
                    return parent.positions[topPointIdx + pointIdx];
                }

                public void SetWidth(float width, int pointIdx) {
                    parent.widths[topPointIdx + pointIdx] = width;
                    isDirtyWidths = true;
                }

                public float GetWidth(int pointIdx) {
                    return parent.widths[topPointIdx + pointIdx];
                }

                public void SetUV(Vector2 uv, float uWidth, int pointIdx) {
                    parent.UVs[topPointIdx + pointIdx] = uv;
                    parent.UWidths[topPointIdx + pointIdx] = uWidth;
                    isDirtyUVs = true;
                }

                public Vector2 GetUV(int pointIdx) {
                    return parent.UVs[topPointIdx + pointIdx];
                }

                public float GetUWidth(int pointIdx) {
                    return parent.UWidths[topPointIdx + pointIdx];
                }

                public void SetColor(Color color, int pointIdx) {
                    parent.colors[topPointIdx + pointIdx] = color;
                    isDirtyColors = true;
                }

                public Color GetColor(int pointIdx) {
                    return parent.colors[topPointIdx + pointIdx];
                }
            }
        }
    }
}
