﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Meige
{
	// Token: 0x020012BE RID: 4798
	[Token(Token = "0x2000C71")]
	[StructLayout(3)]
	public class PrimSphereBuffer : MonoBehaviour
	{
		// Token: 0x060061FC RID: 25084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A76")]
		[Address(RVA = "0x1015EE0D8", Offset = "0x15EE0D8", VA = "0x1015EE0D8")]
		public void SetExternalMeshBuffer(EffectMeshBuffer meshBuffer)
		{
		}

		// Token: 0x060061FD RID: 25085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A77")]
		[Address(RVA = "0x1015EE18C", Offset = "0x15EE18C", VA = "0x1015EE18C")]
		public void ClearExternalMeshBuffer()
		{
		}

		// Token: 0x060061FE RID: 25086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A78")]
		[Address(RVA = "0x1015EE400", Offset = "0x15EE400", VA = "0x1015EE400")]
		private void OnDestroy()
		{
		}

		// Token: 0x060061FF RID: 25087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A79")]
		[Address(RVA = "0x1015EE404", Offset = "0x15EE404", VA = "0x1015EE404")]
		internal void AddActiveBufferList(PrimSphereBuffer.SphereBuffer meshBuffer)
		{
		}

		// Token: 0x06006200 RID: 25088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A7A")]
		[Address(RVA = "0x1015EE444", Offset = "0x15EE444", VA = "0x1015EE444")]
		internal void RemoveActiveBufferList(PrimSphereBuffer.SphereBuffer meshBuffer)
		{
		}

		// Token: 0x17000692 RID: 1682
		// (get) Token: 0x06006202 RID: 25090 RVA: 0x00020520 File Offset: 0x0001E720
		// (set) Token: 0x06006201 RID: 25089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700062E")]
		public bool m_maxTriangleBufferChanged
		{
			[Token(Token = "0x6005A7C")]
			[Address(RVA = "0x1015EE48C", Offset = "0x15EE48C", VA = "0x1015EE48C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A7B")]
			[Address(RVA = "0x1015EE484", Offset = "0x15EE484", VA = "0x1015EE484")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000693 RID: 1683
		// (get) Token: 0x06006204 RID: 25092 RVA: 0x00020538 File Offset: 0x0001E738
		// (set) Token: 0x06006203 RID: 25091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700062F")]
		public bool RenderFlg
		{
			[Token(Token = "0x6005A7E")]
			[Address(RVA = "0x1015EE49C", Offset = "0x15EE49C", VA = "0x1015EE49C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A7D")]
			[Address(RVA = "0x1015EE494", Offset = "0x15EE494", VA = "0x1015EE494")]
			set
			{
			}
		}

		// Token: 0x17000694 RID: 1684
		// (get) Token: 0x06006206 RID: 25094 RVA: 0x00020550 File Offset: 0x0001E750
		// (set) Token: 0x06006205 RID: 25093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000630")]
		public bool bufferChangeFlg
		{
			[Token(Token = "0x6005A80")]
			[Address(RVA = "0x1015EE4AC", Offset = "0x15EE4AC", VA = "0x1015EE4AC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005A7F")]
			[Address(RVA = "0x1015EE4A4", Offset = "0x15EE4A4", VA = "0x1015EE4A4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06006207 RID: 25095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A81")]
		[Address(RVA = "0x1015EE4B4", Offset = "0x15EE4B4", VA = "0x1015EE4B4")]
		private void Start()
		{
		}

		// Token: 0x06006208 RID: 25096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A82")]
		[Address(RVA = "0x1015EE4B8", Offset = "0x15EE4B8", VA = "0x1015EE4B8")]
		private void Setup()
		{
		}

		// Token: 0x06006209 RID: 25097 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005A83")]
		[Address(RVA = "0x1015EE580", Offset = "0x15EE580", VA = "0x1015EE580", Slot = "4")]
		public virtual PrimSphereBuffer.SphereBuffer AddBuffer(int holyzonVtxNum, int verticalDivideNum)
		{
			return null;
		}

		// Token: 0x0600620A RID: 25098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A84")]
		[Address(RVA = "0x1015EEAF8", Offset = "0x15EEAF8", VA = "0x1015EEAF8", Slot = "5")]
		public virtual void RemoveBuffer(PrimSphereBuffer.SphereBuffer sphereBuffer)
		{
		}

		// Token: 0x0600620B RID: 25099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A85")]
		[Address(RVA = "0x1015EEC3C", Offset = "0x15EEC3C", VA = "0x1015EEC3C")]
		private void UpdateMesh()
		{
		}

		// Token: 0x0600620C RID: 25100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A86")]
		[Address(RVA = "0x1015EF588", Offset = "0x15EF588", VA = "0x1015EF588")]
		private void Update()
		{
		}

		// Token: 0x0600620D RID: 25101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005A87")]
		[Address(RVA = "0x1015EF58C", Offset = "0x15EF58C", VA = "0x1015EF58C")]
		public PrimSphereBuffer()
		{
		}

		// Token: 0x040070FE RID: 28926
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004F78")]
		private EffectMeshBuffer m_EffectMeshBuffer;

		// Token: 0x040070FF RID: 28927
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004F79")]
		protected bool m_MeshBufferIsSelf;

		// Token: 0x04007100 RID: 28928
		[Token(Token = "0x4004F7A")]
		private const int MAX_ACTIVE_MESHBUFFER = 32;

		// Token: 0x04007101 RID: 28929
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004F7B")]
		protected ArrayList m_SphereBufferList;

		// Token: 0x04007102 RID: 28930
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004F7C")]
		protected ArrayList m_activeSphereBufferList;

		// Token: 0x020012BF RID: 4799
		[Token(Token = "0x20012EF")]
		[Serializable]
		public class SphereBuffer
		{
			// Token: 0x0600620E RID: 25102 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006411")]
			[Address(RVA = "0x1015EEAD4", Offset = "0x15EEAD4", VA = "0x1015EEAD4")]
			public void UpdatePositions()
			{
			}

			// Token: 0x0600620F RID: 25103 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006412")]
			[Address(RVA = "0x1015EEAE0", Offset = "0x15EEAE0", VA = "0x1015EEAE0")]
			public void UpdateUVs()
			{
			}

			// Token: 0x06006210 RID: 25104 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006413")]
			[Address(RVA = "0x1015EEAEC", Offset = "0x15EEAEC", VA = "0x1015EEAEC")]
			public void UpdateColors()
			{
			}

			// Token: 0x17000695 RID: 1685
			// (get) Token: 0x06006211 RID: 25105 RVA: 0x00020568 File Offset: 0x0001E768
			// (set) Token: 0x06006212 RID: 25106 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006DA")]
			public bool enabled
			{
				[Token(Token = "0x6006414")]
				[Address(RVA = "0x1015EF580", Offset = "0x15EF580", VA = "0x1015EF580")]
				get
				{
					return default(bool);
				}
				[Token(Token = "0x6006415")]
				[Address(RVA = "0x1015EE9EC", Offset = "0x15EE9EC", VA = "0x1015EE9EC")]
				set
				{
				}
			}

			// Token: 0x06006213 RID: 25107 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006416")]
			[Address(RVA = "0x1015EE8F4", Offset = "0x15EE8F4", VA = "0x1015EE8F4")]
			public SphereBuffer()
			{
			}

			// Token: 0x04007105 RID: 28933
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007461")]
			internal PrimSphereBuffer parent;

			// Token: 0x04007106 RID: 28934
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007462")]
			internal EffectMeshBuffer.MeshBuffer meshBuffer;

			// Token: 0x04007107 RID: 28935
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007463")]
			internal int holyzonVtxNum;

			// Token: 0x04007108 RID: 28936
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4007464")]
			internal int verticalDivideNum;

			// Token: 0x04007109 RID: 28937
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007465")]
			internal bool isDirtyPositions;

			// Token: 0x0400710A RID: 28938
			[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
			[Token(Token = "0x4007466")]
			internal bool isDirtyUVs;

			// Token: 0x0400710B RID: 28939
			[Cpp2IlInjected.FieldOffset(Offset = "0x2A")]
			[Token(Token = "0x4007467")]
			internal bool isDirtyColors;

			// Token: 0x0400710C RID: 28940
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4007468")]
			public Vector3 m_Position;

			// Token: 0x0400710D RID: 28941
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4007469")]
			public Vector3 m_Rotation;

			// Token: 0x0400710E RID: 28942
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x400746A")]
			public Vector3 m_Scale;

			// Token: 0x0400710F RID: 28943
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400746B")]
			public float m_HorizonStartAngle;

			// Token: 0x04007110 RID: 28944
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x400746C")]
			public float m_HorizonEndAngle;

			// Token: 0x04007111 RID: 28945
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x400746D")]
			public float m_VerticalStartAngle;

			// Token: 0x04007112 RID: 28946
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x400746E")]
			public float m_VerticalEndAngle;

			// Token: 0x04007113 RID: 28947
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x400746F")]
			public Rect m_UVRect;

			// Token: 0x04007114 RID: 28948
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4007470")]
			public bool m_UVReverse;

			// Token: 0x04007115 RID: 28949
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x4007471")]
			public Color m_ColorStartHorizon;

			// Token: 0x04007116 RID: 28950
			[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
			[Token(Token = "0x4007472")]
			public Color m_ColorEndHorizon;

			// Token: 0x04007117 RID: 28951
			[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
			[Token(Token = "0x4007473")]
			public Color m_ColorStartVertical;

			// Token: 0x04007118 RID: 28952
			[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
			[Token(Token = "0x4007474")]
			public Color m_ColorEndVertical;

			// Token: 0x04007119 RID: 28953
			[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
			[Token(Token = "0x4007475")]
			private bool m_enabled;
		}
	}
}
