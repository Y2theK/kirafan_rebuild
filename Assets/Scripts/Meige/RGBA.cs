﻿using Meige.GenericOperator;
using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public struct RGBA : IComparable, IComparable<RGBA>, IOperatorFast<RGBA> {
        [SerializeField] public Color m_Value;

        public Color value {
            get => m_Value;
            set => m_Value = value;
        }

        public float r {
            get => m_Value.r;
            set => m_Value.r = value;
        }

        public float g {
            get => m_Value.g;
            set => m_Value.g = value;
        }

        public float b {
            get => m_Value.b;
            set => m_Value.b = value;
        }

        public float a {
            get => m_Value.a;
            set => m_Value.a = value;
        }

        public float this[int idx] {
            get => m_Value[idx];
            set => m_Value[idx] = value;
        }

        public RGBA(RGBA value) {
            m_Value = value.m_Value;
        }

        public RGBA(Color value) {
            m_Value = value;
        }

        public RGBA(float r, float g, float b, float a) {
            m_Value.r = r;
            m_Value.g = g;
            m_Value.b = b;
            m_Value.a = a;
        }

        public void Add(RGBA a, RGBA b) {
            this = a + b;
        }

        public void Sub(RGBA a, RGBA b) {
            this = a - b;
        }

        public void Mul(RGBA a, RGBA b) {
            this = a * b;
        }

        public void Mul(RGBA a, float b) {
            this = a * b;
        }

        public void Div(RGBA a, RGBA b) {
            this = a / b;
        }

        public void Add(RGBA b) {
            this += b;
        }

        public void Sub(RGBA b) {
            this -= b;
        }

        public void Mul(RGBA b) {
            this *= b;
        }

        public void Mul(float b) {
            this *= b;
        }

        public void Div(RGBA b) {
            this *= b; //what
        }

        public void SetRowValue(object o) {
            m_Value = (Color)o;
        }

        public void SetRowValue(float o, int componentIdx) {
            m_Value[componentIdx] = o;
        }

        public int CompareTo(object obj) {
            RGBA? rgba = obj as RGBA?;
            if (rgba == null) {
                return 1;
            }
            return CompareTo(rgba);
        }

        public int CompareTo(RGBA target) {
            if (target.m_Value.r == r &&
                target.m_Value.g == g &&
                target.m_Value.b == b &&
                target.m_Value.a == a) {
                return 0;
            }
            return 1;
        }

        public override bool Equals(object obj) {
            RGBA? rgba = obj as RGBA?;
            return rgba != null && Equals(rgba);
        }

        public bool Equals(RGBA obj) {
            return this == obj;
        }

        public override int GetHashCode() {
            return m_Value.GetHashCode();
        }

        public static implicit operator RGBA(Color v) {
            return new RGBA(v);
        }

        public static implicit operator Color(RGBA v) {
            return v.value;
        }

        public static RGBA operator +(RGBA v0) {
            return v0;
        }

        public static RGBA operator +(RGBA v0, RGBA v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r + v1.m_Value.r;
            result.m_Value.g = v0.m_Value.g + v1.m_Value.g;
            result.m_Value.b = v0.m_Value.b + v1.m_Value.b;
            result.m_Value.a = v0.m_Value.a + v1.m_Value.a;
            return result;
        }

        public static RGBA operator -(RGBA v0) {
            RGBA result = default;
            result.m_Value.r = -v0.m_Value.r;
            result.m_Value.g = -v0.m_Value.g;
            result.m_Value.b = -v0.m_Value.b;
            result.m_Value.a = -v0.m_Value.a;
            return result;
        }

        public static RGBA operator -(RGBA v0, RGBA v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r - v1.m_Value.r;
            result.m_Value.g = v0.m_Value.g - v1.m_Value.g;
            result.m_Value.b = v0.m_Value.b - v1.m_Value.b;
            result.m_Value.a = v0.m_Value.a - v1.m_Value.a;
            return result;
        }

        public static RGBA operator *(RGBA v0, RGBA v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r * v1.m_Value.r;
            result.m_Value.g = v0.m_Value.g * v1.m_Value.g;
            result.m_Value.b = v0.m_Value.b * v1.m_Value.b;
            result.m_Value.a = v0.m_Value.a * v1.m_Value.a;
            return result;
        }

        public static RGBA operator *(RGBA v0, float v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r * v1;
            result.m_Value.g = v0.m_Value.g * v1;
            result.m_Value.b = v0.m_Value.b * v1;
            result.m_Value.a = v0.m_Value.a * v1;
            return result;
        }

        public static RGBA operator /(RGBA v0, RGBA v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r / v1.m_Value.r;
            result.m_Value.g = v0.m_Value.g / v1.m_Value.g;
            result.m_Value.b = v0.m_Value.b / v1.m_Value.b;
            result.m_Value.a = v0.m_Value.a / v1.m_Value.a;
            return result;
        }

        public static RGBA operator /(RGBA v0, float v1) {
            RGBA result = default;
            result.m_Value.r = v0.m_Value.r / v1;
            result.m_Value.g = v0.m_Value.g / v1;
            result.m_Value.b = v0.m_Value.b / v1;
            result.m_Value.a = v0.m_Value.a / v1;
            return result;
        }

        public static bool operator ==(RGBA v0, RGBA v1) {
            return v0.value == v1.value;
        }

        public static bool operator !=(RGBA v0, RGBA v1) {
            return v0.value != v1.value;
        }
    }
}
