﻿using Meige.GenericOperator;
using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public class RangePare<Value> where Value : struct, IOperatorFast<Value>, IComparable<Value> {
        [SerializeField] public Value m_Min;
        [SerializeField] public Value m_Max;

        public Value this[int idx] {
            get {
                if (idx == 0) {
                    return m_Min;
                } else if (idx == 1) {
                    return m_Max;
                }
                return default;
            }
            set {
                if (idx == 0) {
                    m_Min = value;
                } else if (idx == 1) {
                    m_Max = value;
                }
            }
        }

        public Value min {
            get => m_Min;
            set => m_Min = value;
        }

        public Value max {
            get => m_Max;
            set => m_Max = value;
        }

        public Value ValueMin => m_Min.CompareTo(m_Max) < 0 ? m_Min : m_Max;
        public Value ValueMax => m_Min.CompareTo(m_Max) > 0 ? m_Min : m_Max;

        public RangePare() { }

        public RangePare(Value min, Value max) {
            m_Min = min;
            m_Max = max;
        }

        public RangePare(RangePare<Value> value) {
            m_Min = value.min;
            m_Max = value.max;
        }

        public void Set(RangePare<Value> v) {
            m_Min = v.min;
            m_Max = v.max;
        }

        public void SetValue(Value min, Value max) {
            m_Min = min;
            m_Max = max;
        }

        public void SetValueRow(object min, object max) {
            m_Min.SetRowValue(min);
            m_Max.SetRowValue(max);
        }

        public static RangePare<Value> Clone(RangePare<Value> target) {
            return new RangePare<Value> {
                m_Min = target.m_Min,
                m_Max = target.m_Max
            };
        }

        public virtual RangePare<Value> Clone() {
            return Clone(this);
        }

        public Value Random() {
            return Lerp(UnityEngine.Random.Range(0f, 1f));
        }

        public Value Lerp(float rate) {
            Value value = default;
            value.Sub(max, min);
            value.Mul(value, rate);
            value.Add(value, min);
            return value;
        }

        public Value ReverseLerp(float rate) {
            Value value = default;
            value.Sub(min, max);
            value.Mul(value, rate);
            value.Add(value, max);
            return value;
        }
    }
}
