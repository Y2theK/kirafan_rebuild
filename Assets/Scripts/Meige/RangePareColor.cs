﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001270 RID: 4720
	[Token(Token = "0x2000C5E")]
	[Serializable]
	[StructLayout(3)]
	public class RangePareColor : RangePare<RGBA>
	{
		// Token: 0x06005FB0 RID: 24496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600589B")]
		[Address(RVA = "0x1015EFAE0", Offset = "0x15EFAE0", VA = "0x1015EFAE0")]
		public RangePareColor()
		{
		}

		// Token: 0x06005FB1 RID: 24497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600589C")]
		[Address(RVA = "0x1015EFB30", Offset = "0x15EFB30", VA = "0x1015EFB30")]
		public RangePareColor(RGBA min, RGBA max)
		{
		}

		// Token: 0x06005FB2 RID: 24498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600589D")]
		[Address(RVA = "0x1015EFBE0", Offset = "0x15EFBE0", VA = "0x1015EFBE0")]
		public RangePareColor(RangePareColor v)
		{
		}

		// Token: 0x06005FB3 RID: 24499 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600589E")]
		[Address(RVA = "0x1015EFC40", Offset = "0x15EFC40", VA = "0x1015EFC40")]
		public new RangePareColor Clone()
		{
			return null;
		}
	}
}
