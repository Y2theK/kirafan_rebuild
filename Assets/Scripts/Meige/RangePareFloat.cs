﻿using System;

namespace Meige {
    [Serializable]
    public class RangePareFloat : RangePare<f32> {
        public RangePareFloat() { }

        public RangePareFloat(f32 min, f32 max) : base(min, max) { }

        public RangePareFloat(RangePareFloat v) : base(v) { }

        public new RangePareFloat Clone() {
            return new RangePareFloat(this);
        }
    }
}
