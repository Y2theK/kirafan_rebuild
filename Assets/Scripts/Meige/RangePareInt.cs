﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200126B RID: 4715
	[Token(Token = "0x2000C59")]
	[Serializable]
	[StructLayout(3)]
	public class RangePareInt : RangePare<s32>
	{
		// Token: 0x06005F9C RID: 24476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005887")]
		[Address(RVA = "0x1015EFE18", Offset = "0x15EFE18", VA = "0x1015EFE18")]
		public RangePareInt()
		{
		}

		// Token: 0x06005F9D RID: 24477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005888")]
		[Address(RVA = "0x1015EFE68", Offset = "0x15EFE68", VA = "0x1015EFE68")]
		public RangePareInt(s32 min, s32 max)
		{
		}

		// Token: 0x06005F9E RID: 24478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005889")]
		[Address(RVA = "0x1015EFED0", Offset = "0x15EFED0", VA = "0x1015EFED0")]
		public RangePareInt(RangePareInt v)
		{
		}

		// Token: 0x06005F9F RID: 24479 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600588A")]
		[Address(RVA = "0x1015EFF30", Offset = "0x15EFF30", VA = "0x1015EFF30")]
		public new RangePareInt Clone()
		{
			return null;
		}
	}
}
