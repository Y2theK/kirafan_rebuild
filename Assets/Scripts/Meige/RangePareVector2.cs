﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200126D RID: 4717
	[Token(Token = "0x2000C5B")]
	[Serializable]
	[StructLayout(3)]
	public class RangePareVector2 : RangePare<Float2>
	{
		// Token: 0x06005FA4 RID: 24484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600588F")]
		[Address(RVA = "0x1015EFF90", Offset = "0x15EFF90", VA = "0x1015EFF90")]
		public RangePareVector2()
		{
		}

		// Token: 0x06005FA5 RID: 24485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005890")]
		[Address(RVA = "0x1015EFFE0", Offset = "0x15EFFE0", VA = "0x1015EFFE0")]
		public RangePareVector2(Float2 min, Float2 max)
		{
		}

		// Token: 0x06005FA6 RID: 24486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005891")]
		[Address(RVA = "0x1015F0060", Offset = "0x15F0060", VA = "0x1015F0060")]
		public RangePareVector2(RangePareVector2 v)
		{
		}

		// Token: 0x06005FA7 RID: 24487 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005892")]
		[Address(RVA = "0x1015F00C0", Offset = "0x15F00C0", VA = "0x1015F00C0")]
		public new RangePareVector2 Clone()
		{
			return null;
		}
	}
}
