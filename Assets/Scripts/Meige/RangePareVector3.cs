﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200126E RID: 4718
	[Token(Token = "0x2000C5C")]
	[Serializable]
	[StructLayout(3)]
	public class RangePareVector3 : RangePare<Float3>
	{
		// Token: 0x06005FA8 RID: 24488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005893")]
		[Address(RVA = "0x1015F0120", Offset = "0x15F0120", VA = "0x1015F0120")]
		public RangePareVector3()
		{
		}

		// Token: 0x06005FA9 RID: 24489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005894")]
		[Address(RVA = "0x1015F0170", Offset = "0x15F0170", VA = "0x1015F0170")]
		public RangePareVector3(Float3 min, Float3 max)
		{
		}

		// Token: 0x06005FAA RID: 24490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005895")]
		[Address(RVA = "0x1015F0208", Offset = "0x15F0208", VA = "0x1015F0208")]
		public RangePareVector3(RangePareVector3 v)
		{
		}

		// Token: 0x06005FAB RID: 24491 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005896")]
		[Address(RVA = "0x1015F0268", Offset = "0x15F0268", VA = "0x1015F0268")]
		public new RangePareVector3 Clone()
		{
			return null;
		}
	}
}
