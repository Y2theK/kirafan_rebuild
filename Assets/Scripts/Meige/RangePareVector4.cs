﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200126F RID: 4719
	[Token(Token = "0x2000C5D")]
	[Serializable]
	[StructLayout(3)]
	public class RangePareVector4 : RangePare<Float4>
	{
		// Token: 0x06005FAC RID: 24492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005897")]
		[Address(RVA = "0x1015F02C8", Offset = "0x15F02C8", VA = "0x1015F02C8")]
		public RangePareVector4()
		{
		}

		// Token: 0x06005FAD RID: 24493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005898")]
		[Address(RVA = "0x1015F0318", Offset = "0x15F0318", VA = "0x1015F0318")]
		public RangePareVector4(Float4 min, Float4 max)
		{
		}

		// Token: 0x06005FAE RID: 24494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005899")]
		[Address(RVA = "0x1015F03C8", Offset = "0x15F03C8", VA = "0x1015F03C8")]
		public RangePareVector4(RangePareVector4 v)
		{
		}

		// Token: 0x06005FAF RID: 24495 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600589A")]
		[Address(RVA = "0x1015F0428", Offset = "0x15F0428", VA = "0x1015F0428")]
		public new RangePareVector4 Clone()
		{
			return null;
		}
	}
}
