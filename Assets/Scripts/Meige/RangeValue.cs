﻿using Meige.GenericOperator;
using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public class RangeValue<Value> where Value : struct, IOperatorFast<Value> {
        [SerializeField] public Value[] m_Value;

        public float[] m_Point;

        public Value this[int idx] {
            get => m_Value[idx];
            set => m_Value[idx] = value;
        }

        public RangeValue() {
            m_Value = new Value[2];
        }

        public RangeValue(int num) {
            m_Value = new Value[num];
        }

        public RangeValue(RangeValue<Value> value) {
            m_Value = new Value[value.GetLength()];
            for (int i = 0; i < value.GetLength(); i++) {
                m_Value[i] = value[i];
            }
        }

        public void Set(Value v, int idx) {
            m_Value[idx] = v;
        }

        public void SetValueRow(object v, int idx) {
            m_Value[idx].SetRowValue(v);
        }

        public Value Get(int idx) {
            return m_Value[idx];
        }

        public int GetLength() {
            return m_Value.Length;
        }

        public static RangeValue<Value> Clone(RangeValue<Value> target) {
            RangeValue<Value> rangeValue = new RangeValue<Value>(); //should pass in the target length
            for (int i = 0; i < target.GetLength(); i++) {
                rangeValue.m_Value[i] = target.m_Value[i];
            }
            return rangeValue;
        }

        public virtual RangeValue<Value> Clone() {
            return Clone(this);
        }

        private void CreateSelectionPoint() {
            m_Point = new float[GetLength()];
            for (int i = 0; i < m_Point.Length; i++) {
                m_Point[i] = i / (float)(m_Point.Length - 1);
            }
        }

        private void SetSectionPoint(int idx, float point) {
            if (m_Point != null) {
                m_Point[idx] = point;
            }
        }

        private float GetSectionPoint(int idx) {
            if (m_Point != null && m_Point.Length > 0) {
                return m_Point[idx];
            }
            return idx / (float)(m_Value.Length - 1);
        }

        public Value Random() {
            return Lerp(UnityEngine.Random.Range(0f, 1f));
        }

        public Value Lerp(float point) {
            if (m_Value.Length == 2) {
                Value result = m_Value[1];
                result.Sub(m_Value[0]);
                result.Mul(point);
                result.Add(m_Value[0]);
                return result;
            }
            if (point <= 0f) {
                return m_Value[0];
            }
            if (point >= 1f) {
                return m_Value[m_Value.Length - 1];
            }
            int idx = 0;
            if (m_Point != null && m_Point.Length > 0) {
                for (int i = 0; i < m_Value.Length; i++) {
                    float sectionPoint = GetSectionPoint(i);
                    if (point <= sectionPoint) {
                        idx = i;
                        break;
                    }
                }
            } else {
                idx = (int)(point / (1f / (m_Value.Length - 1)));
            }
            float point2 = GetSectionPoint(idx);
            float point3 = GetSectionPoint(idx + 1);
            float t = Mathf.Clamp((point - point2) / (point3 - point2), 0f, 1f);
            Value a = default;
            Value b = default;
            a.Mul(m_Value[idx], 1f - t);
            b.Mul(m_Value[idx + 1], t);
            b.Add(a, b);
            return b;
        }

        public Value ReverseLerp(float point) {
            return Lerp(1f - point);
        }
    }
}
