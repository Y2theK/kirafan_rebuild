﻿using System;

namespace Meige {
    [Serializable]
    public class RangeValueColor : RangeValue<RGBA> {
        public RangeValueColor() { }

        public RangeValueColor(int num) : base(num) { }

        public RangeValueColor(RangeValueColor v) : base(v) { }

        public new RangeValueColor Clone() {
            return new RangeValueColor(this);
        }
    }
}
