﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001266 RID: 4710
	[Token(Token = "0x2000C54")]
	[Serializable]
	[StructLayout(3)]
	public class RangeValueFloat : RangeValue<f32>
	{
		// Token: 0x06005F88 RID: 24456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005873")]
		[Address(RVA = "0x1015F05F8", Offset = "0x15F05F8", VA = "0x1015F05F8")]
		public RangeValueFloat()
		{
		}

		// Token: 0x06005F89 RID: 24457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005874")]
		[Address(RVA = "0x1015F0648", Offset = "0x15F0648", VA = "0x1015F0648")]
		public RangeValueFloat(int num)
		{
		}

		// Token: 0x06005F8A RID: 24458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005875")]
		[Address(RVA = "0x1015F06A8", Offset = "0x15F06A8", VA = "0x1015F06A8")]
		public RangeValueFloat(RangeValueFloat v)
		{
		}

		// Token: 0x06005F8B RID: 24459 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005876")]
		[Address(RVA = "0x1015F0708", Offset = "0x15F0708", VA = "0x1015F0708")]
		public new RangeValueFloat Clone()
		{
			return null;
		}
	}
}
