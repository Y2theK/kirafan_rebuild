﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001265 RID: 4709
	[Token(Token = "0x2000C53")]
	[Serializable]
	[StructLayout(3)]
	public class RangeValueInt : RangeValue<s32>
	{
		// Token: 0x06005F84 RID: 24452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600586F")]
		[Address(RVA = "0x1015F0768", Offset = "0x15F0768", VA = "0x1015F0768")]
		public RangeValueInt()
		{
		}

		// Token: 0x06005F85 RID: 24453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005870")]
		[Address(RVA = "0x1015F07B8", Offset = "0x15F07B8", VA = "0x1015F07B8")]
		public RangeValueInt(int num)
		{
		}

		// Token: 0x06005F86 RID: 24454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005871")]
		[Address(RVA = "0x1015F0818", Offset = "0x15F0818", VA = "0x1015F0818")]
		public RangeValueInt(RangeValueInt v)
		{
		}

		// Token: 0x06005F87 RID: 24455 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005872")]
		[Address(RVA = "0x1015F0878", Offset = "0x15F0878", VA = "0x1015F0878")]
		public new RangeValueInt Clone()
		{
			return null;
		}
	}
}
