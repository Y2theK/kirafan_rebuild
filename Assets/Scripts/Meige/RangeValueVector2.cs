﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001267 RID: 4711
	[Token(Token = "0x2000C55")]
	[Serializable]
	[StructLayout(3)]
	public class RangeValueVector2 : RangeValue<Float2>
	{
		// Token: 0x06005F8C RID: 24460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005877")]
		[Address(RVA = "0x1015F08D8", Offset = "0x15F08D8", VA = "0x1015F08D8")]
		public RangeValueVector2()
		{
		}

		// Token: 0x06005F8D RID: 24461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005878")]
		[Address(RVA = "0x1015F0928", Offset = "0x15F0928", VA = "0x1015F0928")]
		public RangeValueVector2(int num)
		{
		}

		// Token: 0x06005F8E RID: 24462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005879")]
		[Address(RVA = "0x1015F0988", Offset = "0x15F0988", VA = "0x1015F0988")]
		public RangeValueVector2(RangeValueVector2 v)
		{
		}

		// Token: 0x06005F8F RID: 24463 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600587A")]
		[Address(RVA = "0x1015F09E8", Offset = "0x15F09E8", VA = "0x1015F09E8")]
		public new RangeValueVector2 Clone()
		{
			return null;
		}
	}
}
