﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001268 RID: 4712
	[Token(Token = "0x2000C56")]
	[Serializable]
	[StructLayout(3)]
	public class RangeValueVector3 : RangeValue<Float3>
	{
		// Token: 0x06005F90 RID: 24464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600587B")]
		[Address(RVA = "0x1015F0A48", Offset = "0x15F0A48", VA = "0x1015F0A48")]
		public RangeValueVector3()
		{
		}

		// Token: 0x06005F91 RID: 24465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600587C")]
		[Address(RVA = "0x1015F0A98", Offset = "0x15F0A98", VA = "0x1015F0A98")]
		public RangeValueVector3(int num)
		{
		}

		// Token: 0x06005F92 RID: 24466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600587D")]
		[Address(RVA = "0x1015F0AF8", Offset = "0x15F0AF8", VA = "0x1015F0AF8")]
		public RangeValueVector3(RangeValueVector3 v)
		{
		}

		// Token: 0x06005F93 RID: 24467 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600587E")]
		[Address(RVA = "0x1015F0B58", Offset = "0x15F0B58", VA = "0x1015F0B58")]
		public new RangeValueVector3 Clone()
		{
			return null;
		}
	}
}
