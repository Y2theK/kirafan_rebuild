﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001269 RID: 4713
	[Token(Token = "0x2000C57")]
	[Serializable]
	[StructLayout(3)]
	public class RangeValueVector4 : RangeValue<Float4>
	{
		// Token: 0x06005F94 RID: 24468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600587F")]
		[Address(RVA = "0x1015F0BB8", Offset = "0x15F0BB8", VA = "0x1015F0BB8")]
		public RangeValueVector4()
		{
		}

		// Token: 0x06005F95 RID: 24469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005880")]
		[Address(RVA = "0x1015F0C08", Offset = "0x15F0C08", VA = "0x1015F0C08")]
		public RangeValueVector4(int num)
		{
		}

		// Token: 0x06005F96 RID: 24470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005881")]
		[Address(RVA = "0x1015F0C68", Offset = "0x15F0C68", VA = "0x1015F0C68")]
		public RangeValueVector4(RangeValueVector4 v)
		{
		}

		// Token: 0x06005F97 RID: 24471 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005882")]
		[Address(RVA = "0x1015F0CC8", Offset = "0x15F0CC8", VA = "0x1015F0CC8")]
		public new RangeValueVector4 Clone()
		{
			return null;
		}
	}
}
