﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200132E RID: 4910
	[Token(Token = "0x2000CBC")]
	[StructLayout(3)]
	public class TTrophyCusotmInfo
	{
		// Token: 0x06006411 RID: 25617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BE3")]
		[Address(RVA = "0x1015F0D28", Offset = "0x15F0D28", VA = "0x1015F0D28")]
		public TTrophyCusotmInfo()
		{
		}

		// Token: 0x0400751A RID: 29978
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40052D7")]
		public string m_TrophyApiName;

		// Token: 0x0400751B RID: 29979
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40052D8")]
		public int m_TrophyID;
	}
}
