﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x0200125F RID: 4703
	[Token(Token = "0x2000C4D")]
	[Attribute(Name = "AttributeUsageAttribute", RVA = "0x10011D5DC", Offset = "0x11D5DC")]
	[StructLayout(3)]
	public class TimelineFieldAttribute : Attribute
	{
		// Token: 0x06005F5B RID: 24411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005846")]
		[Address(RVA = "0x1015F0D30", Offset = "0x15F0D30", VA = "0x1015F0D30")]
		public TimelineFieldAttribute(string name, string affiliation)
		{
		}

		// Token: 0x04006F09 RID: 28425
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004EC6")]
		public string name;

		// Token: 0x04006F0A RID: 28426
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004EC7")]
		public string affiliation;

		// Token: 0x04006F0B RID: 28427
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004EC8")]
		public int id;
	}
}
