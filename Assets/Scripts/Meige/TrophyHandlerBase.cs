﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001336 RID: 4918
	[Token(Token = "0x2000CC2")]
	[StructLayout(3)]
	public class TrophyHandlerBase
	{
		// Token: 0x06006426 RID: 25638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF7")]
		[Address(RVA = "0x1015F0D68", Offset = "0x15F0D68", VA = "0x1015F0D68")]
		public TrophyHandlerBase()
		{
		}

		// Token: 0x04007537 RID: 30007
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40052EB")]
		public eTrophyProcessType m_processType;

		// Token: 0x04007538 RID: 30008
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40052EC")]
		public eTrophyResult m_result;

		// Token: 0x04007539 RID: 30009
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40052ED")]
		public bool m_isAvailable;
	}
}
