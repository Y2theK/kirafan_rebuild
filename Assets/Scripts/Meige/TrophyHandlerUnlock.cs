﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001337 RID: 4919
	[Token(Token = "0x2000CC3")]
	[StructLayout(3)]
	public class TrophyHandlerUnlock : TrophyHandlerBase
	{
		// Token: 0x06006427 RID: 25639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005BF8")]
		[Address(RVA = "0x1015F0D70", Offset = "0x15F0D70", VA = "0x1015F0D70")]
		public TrophyHandlerUnlock()
		{
		}

		// Token: 0x0400753A RID: 30010
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40052EE")]
		public int m_trophyID;
	}
}
