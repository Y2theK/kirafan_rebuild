﻿using System;
using System.Threading;

namespace Meige {
    public class WorkerThread {
        private Thread m_thread;
        private ManualResetEvent m_event;
        private bool m_exit;

        public WorkerThread() {
            Init();
        }

        private void Init() {
            m_exit = false;
            m_event = new ManualResetEvent(false);
            m_thread = new Thread(new ThreadStart(Exec));
            m_thread.IsBackground = true;
            m_thread.Priority = ThreadPriority.AboveNormal;
            m_thread.Start();
        }

        public void Release() {
            m_exit = true;
            if (m_thread != null) {
                Signal();
                m_thread.Join();
                m_thread = null;
            }
            if (m_event != null) {
                m_event.Close();
                m_event = null;
            }
        }

        public void Signal() {
            m_event.Set();
        }

        private void Exec() {
            while (!m_exit) {
                try {
                    m_event.WaitOne();
                    m_event.Reset();
                    Job job = JobQueue.Dequeue();
                    if (job != null) {
                        job.Invoke();
                        JobQueue.Done(job);
                        Signal();
                    }
                } catch (ThreadAbortException) {
                    m_exit = true;
                } catch (Exception) {
                    m_exit = true;
                    JobQueue.Release();
                }
            }
        }
    }
}
