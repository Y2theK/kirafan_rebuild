﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012CD RID: 4813
	[Token(Token = "0x2000C7F")]
	[StructLayout(3, Size = 4)]
	public enum eAI_ID
	{
		// Token: 0x0400725A RID: 29274
		[Token(Token = "0x40050BF")]
		eAI_ID_Invalid = -1
	}
}
