﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012CB RID: 4811
	[Token(Token = "0x2000C7D")]
	[StructLayout(3, Size = 4)]
	public enum eActTransitSystemCond
	{
		// Token: 0x04007251 RID: 29265
		[Token(Token = "0x40050B6")]
		eActTransitSystemCond_Invalid = -1,
		// Token: 0x04007252 RID: 29266
		[Token(Token = "0x40050B7")]
		eActTransitSystemCond_I_StateChange,
		// Token: 0x04007253 RID: 29267
		[Token(Token = "0x40050B8")]
		eActTransitSystemCond_B_NextFromEnd,
		// Token: 0x04007254 RID: 29268
		[Token(Token = "0x40050B9")]
		eActTransitSystemCond_B_NextByFlag,
		// Token: 0x04007255 RID: 29269
		[Token(Token = "0x40050BA")]
		eActTransitSystemCond_Max = 32
	}
}
