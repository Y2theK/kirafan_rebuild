﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012CC RID: 4812
	[Token(Token = "0x2000C7E")]
	[StructLayout(3, Size = 4)]
	public enum eActTransitUserCond
	{
		// Token: 0x04007257 RID: 29271
		[Token(Token = "0x40050BC")]
		eActTransitUserCond_Start = 32,
		// Token: 0x04007258 RID: 29272
		[Token(Token = "0x40050BD")]
		eActTransitUserCond_I_KeyAcceptLayer
	}
}
