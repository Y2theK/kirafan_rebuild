﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012F9 RID: 4857
	[Token(Token = "0x2000C9B")]
	[StructLayout(3, Size = 4)]
	public enum eAnimControlType
	{
		// Token: 0x040073C2 RID: 29634
		[Token(Token = "0x40051D6")]
		eAnimControl_Invalid = -1,
		// Token: 0x040073C3 RID: 29635
		[Token(Token = "0x40051D7")]
		eAnimControl_Linear,
		// Token: 0x040073C4 RID: 29636
		[Token(Token = "0x40051D8")]
		eAnimControl_Bool,
		// Token: 0x040073C5 RID: 29637
		[Token(Token = "0x40051D9")]
		eAnimControl_Constant,
		// Token: 0x040073C6 RID: 29638
		[Token(Token = "0x40051DA")]
		eAnimControl_Cubic
	}
}
