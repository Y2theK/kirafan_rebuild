﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C6 RID: 4806
	[Token(Token = "0x2000C78")]
	[StructLayout(3, Size = 4)]
	public enum eAreaFlagID
	{
		// Token: 0x04007244 RID: 29252
		[Token(Token = "0x40050A9")]
		eAreaFlagID_None,
		// Token: 0x04007245 RID: 29253
		[Token(Token = "0x40050AA")]
		eAreaFlagID_Max
	}
}
