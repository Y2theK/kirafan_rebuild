﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C2 RID: 4802
	[Token(Token = "0x2000C74")]
	[StructLayout(3, Size = 4)]
	public enum eAreaType
	{
		// Token: 0x04007236 RID: 29238
		[Token(Token = "0x400509B")]
		eAreaType_Circle,
		// Token: 0x04007237 RID: 29239
		[Token(Token = "0x400509C")]
		eAreaType_Square
	}
}
