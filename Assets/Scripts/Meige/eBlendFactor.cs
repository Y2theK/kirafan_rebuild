﻿namespace Meige {
    public enum eBlendFactor {
        Zero,
        One,
        DstColor,
        SrcColor,
        OneMinusDstColor,
        SrcAlpha,
        OneMinusSrcColor,
        DstAlpha,
        OneMinusDstAlpha,
        OneMinusSrcAlpha = 10
    }
}
