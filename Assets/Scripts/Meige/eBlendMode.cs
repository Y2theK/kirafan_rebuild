﻿namespace Meige {
    public enum eBlendMode {
        eBlendMode_Invalid = -1,
        eBlendMode_None,
        eBlendMode_Std,
        eBlendMode_Add,
        eBlendMode_Sub,
        eBlendMode_Negative,
        eBlendMode_SrcOne,
        eBlendMode_DstOne,
        eBlendMode_Mul,
        eBlendMode_Custom,
        eBlendMode_Max
    }
}
