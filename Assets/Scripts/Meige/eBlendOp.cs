﻿namespace Meige {
    public enum eBlendOp {
        Add,
        Sub,
        RevSub,
        Min,
        Max
    }
}
