﻿namespace Meige {
    public enum eCameraProjection {
        eCameraProjectionPerspective,
        eCameraProjectionOrthographic,
        eCameraProjectionMAX
    }
}
