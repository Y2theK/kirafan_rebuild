﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001314 RID: 4884
	[Token(Token = "0x2000CB2")]
	[StructLayout(3, Size = 4)]
	public enum eColorCorrectCombineID
	{
		// Token: 0x04007470 RID: 29808
		[Token(Token = "0x400526A")]
		eColorCorrectCombineID_ToneMap,
		// Token: 0x04007471 RID: 29809
		[Token(Token = "0x400526B")]
		eColorCorrectCombineID_Contrast,
		// Token: 0x04007472 RID: 29810
		[Token(Token = "0x400526C")]
		eColorCorrectCombineID_Brightness,
		// Token: 0x04007473 RID: 29811
		[Token(Token = "0x400526D")]
		eColorCorrectCombineID_Chroma,
		// Token: 0x04007474 RID: 29812
		[Token(Token = "0x400526E")]
		eColorCorrectCombineID_ColorBlend
	}
}
