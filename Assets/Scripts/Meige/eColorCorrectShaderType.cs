﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001313 RID: 4883
	[Token(Token = "0x2000CB1")]
	[StructLayout(3, Size = 4)]
	public enum eColorCorrectShaderType
	{
		// Token: 0x0400746B RID: 29803
		[Token(Token = "0x4005265")]
		eColorCorrectShaderType_Invalid,
		// Token: 0x0400746C RID: 29804
		[Token(Token = "0x4005266")]
		eColorCorrectShaderType_ToneMapParam,
		// Token: 0x0400746D RID: 29805
		[Token(Token = "0x4005267")]
		eColorCorrectShaderType_Reduction,
		// Token: 0x0400746E RID: 29806
		[Token(Token = "0x4005268")]
		eColorCorrectShaderType_Combiner
	}
}
