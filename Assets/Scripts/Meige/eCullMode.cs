﻿namespace Meige {
    public enum eCullMode {
        Off,
        Front,
        Back
    }
}
