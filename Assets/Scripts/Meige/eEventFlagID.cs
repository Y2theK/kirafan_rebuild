﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C8 RID: 4808
	[Token(Token = "0x2000C7A")]
	[StructLayout(3, Size = 4)]
	public enum eEventFlagID
	{
		// Token: 0x0400724A RID: 29258
		[Token(Token = "0x40050AF")]
		eEventFlagID_None,
		// Token: 0x0400724B RID: 29259
		[Token(Token = "0x40050B0")]
		eEventFlagID_Max
	}
}
