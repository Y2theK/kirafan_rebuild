﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C3 RID: 4803
	[Token(Token = "0x2000C75")]
	[StructLayout(3, Size = 4)]
	public enum eFieldSpaceType
	{
		// Token: 0x04007239 RID: 29241
		[Token(Token = "0x400509E")]
		eFieldSpaceType_3D,
		// Token: 0x0400723A RID: 29242
		[Token(Token = "0x400509F")]
		eFieldSpaceType_2D
	}
}
