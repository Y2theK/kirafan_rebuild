﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C5 RID: 4805
	[Token(Token = "0x2000C77")]
	[StructLayout(3, Size = 4)]
	public enum eFlagType
	{
		// Token: 0x0400723E RID: 29246
		[Token(Token = "0x40050A3")]
		eFlagType_None,
		// Token: 0x0400723F RID: 29247
		[Token(Token = "0x40050A4")]
		eFlagType_Area,
		// Token: 0x04007240 RID: 29248
		[Token(Token = "0x40050A5")]
		eFlagType_Chapter,
		// Token: 0x04007241 RID: 29249
		[Token(Token = "0x40050A6")]
		eFlagType_Permanent,
		// Token: 0x04007242 RID: 29250
		[Token(Token = "0x40050A7")]
		eFlagType_Max
	}
}
