﻿namespace Meige {
    public enum eLayerBlendMode {
        eLayerBlendMode_Invalid = -1,
        eLayerBlendMode_Default,
        eLayerBlendMode_Std,
        eLayerBlendMode_Add,
        eLayerBlendMode_Sub,
        eLayerBlendMode_Mul,
        eLayerBlendMode_SrcOne,
        eLayerBlendMode_DstOne,
        eLayerBlendMode_Max
    }
}
