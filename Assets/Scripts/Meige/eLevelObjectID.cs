﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C1 RID: 4801
	[Token(Token = "0x2000C73")]
	[StructLayout(3, Size = 4)]
	public enum eLevelObjectID
	{
		// Token: 0x04007233 RID: 29235
		[Token(Token = "0x4005098")]
		eLevelObjectID_Invalid = -1,
		// Token: 0x04007234 RID: 29236
		[Token(Token = "0x4005099")]
		eLevelObjectID_Max
	}
}
