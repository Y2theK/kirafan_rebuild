﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012FD RID: 4861
	[Token(Token = "0x2000C9F")]
	[StructLayout(3, Size = 4)]
	public enum eMFFChunkType
	{
		// Token: 0x040073E8 RID: 29672
		[Token(Token = "0x40051FC")]
		eMFFChunk_Object,
		// Token: 0x040073E9 RID: 29673
		[Token(Token = "0x40051FD")]
		eMFFChunk_Texture,
		// Token: 0x040073EA RID: 29674
		[Token(Token = "0x40051FE")]
		eMFFChunk_Animation,
		// Token: 0x040073EB RID: 29675
		[Token(Token = "0x40051FF")]
		eMFFChunk_Material,
		// Token: 0x040073EC RID: 29676
		[Token(Token = "0x4005200")]
		eMFFChunk_Hierarchies,
		// Token: 0x040073ED RID: 29677
		[Token(Token = "0x4005201")]
		eMFFChunk_ParticleEmitters,
		// Token: 0x040073EE RID: 29678
		[Token(Token = "0x4005202")]
		eMFFChunk_Collisions,
		// Token: 0x040073EF RID: 29679
		[Token(Token = "0x4005203")]
		eMFFChunk_SoundConfig,
		// Token: 0x040073F0 RID: 29680
		[Token(Token = "0x4005204")]
		eMFFChunk_SoundBank,
		// Token: 0x040073F1 RID: 29681
		[Token(Token = "0x4005205")]
		eMFFChunk_CatFileElement,
		// Token: 0x040073F2 RID: 29682
		[Token(Token = "0x4005206")]
		eMFFChunk_ShaderKey,
		// Token: 0x040073F3 RID: 29683
		[Token(Token = "0x4005207")]
		eMFFChunk_ShaderBinary,
		// Token: 0x040073F4 RID: 29684
		[Token(Token = "0x4005208")]
		eMFFChunk_SpectrumCtrls,
		// Token: 0x040073F5 RID: 29685
		[Token(Token = "0x4005209")]
		eMFFChunk_ADNode,
		// Token: 0x040073F6 RID: 29686
		[Token(Token = "0x400520A")]
		eMFFChunk_Cameras,
		// Token: 0x040073F7 RID: 29687
		[Token(Token = "0x400520B")]
		eMFFChunk_SignalCtrls,
		// Token: 0x040073F8 RID: 29688
		[Token(Token = "0x400520C")]
		eMFFChunk_FontParam,
		// Token: 0x040073F9 RID: 29689
		[Token(Token = "0x400520D")]
		eMFFChunk_RenderEnvironment,
		// Token: 0x040073FA RID: 29690
		[Token(Token = "0x400520E")]
		eMFFChunk_LevelData,
		// Token: 0x040073FB RID: 29691
		[Token(Token = "0x400520F")]
		eMFFChunk_ActData
	}
}
