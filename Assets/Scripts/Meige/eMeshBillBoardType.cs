﻿namespace Meige {
    public enum eMeshBillBoardType {
        eMeshBillBoardType_None,
        eMeshBillBoardType_XYZ,
        eMeshBillBoardType_X,
        eMeshBillBoardType_Y,
        eMeshBillBoardType_Z
    }
}
