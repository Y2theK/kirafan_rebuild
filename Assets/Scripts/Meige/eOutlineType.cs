﻿namespace Meige {
    public enum eOutlineType {
        eOutlineType_Invalid = -1,
        eOutlineType_Std,
        eOutlineType_MulBaseColor,
        eOutlineType_Max
    }
}
