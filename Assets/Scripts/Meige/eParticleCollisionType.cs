﻿namespace Meige {
    public enum eParticleCollisionType : byte {
        eParticleCollisionType_None,
        eParticleCollisionType_Height,
        eParticleCollisionType_Collision
    }
}
