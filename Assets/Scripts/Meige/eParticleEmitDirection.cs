﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012E7 RID: 4839
	[Token(Token = "0x2000C89")]
	[StructLayout(3, Size = 1)]
	public enum eParticleEmitDirection : byte
	{
		// Token: 0x04007309 RID: 29449
		[Token(Token = "0x400511D")]
		eParticleEmitDirection_ShapeAlong,
		// Token: 0x0400730A RID: 29450
		[Token(Token = "0x400511E")]
		eParticleEmitDirection_Random
	}
}
