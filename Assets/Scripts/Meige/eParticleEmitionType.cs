﻿namespace Meige {
    public enum eParticleEmitionType : byte {
        eParticleEmitionType_Point,
        eParticleEmitionType_Box,
        eParticleEmitionType_PlaneQuad,
        eParticleEmitionType_PlaneCircle,
        eParticleEmitionType_Sphere,
        eParticleEmitionType_Torus,
        eParticleEmitionType_MeshVertex,
        eParticleEmitionType_Cylinder
    }
}
