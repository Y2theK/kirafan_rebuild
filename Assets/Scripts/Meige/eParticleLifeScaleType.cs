﻿namespace Meige {
    public enum eParticleLifeScaleType : byte {
        eParticleLifeScaleType_Linear,
        eParticleLifeScaleType_CurveAcceleration,
        eParticleLifeScaleType_CurveSlowdonw
    }
}
