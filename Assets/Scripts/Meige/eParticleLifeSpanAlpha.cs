﻿namespace Meige {
    public enum eParticleLifeSpanAlpha : byte {
        eParticleLifeSpanAlpha_None,
        eParticleLifeSpanAlpha_FadeInOut,
        eParticleLifeSpanAlpha_FadeIn,
        eParticleLifeSpanAlpha_FadeOut
    }
}
