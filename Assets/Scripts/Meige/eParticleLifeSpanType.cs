﻿namespace Meige {
    public enum eParticleLifeSpanType : byte {
        eParticleLifeSpanType_Time,
        eParticleLifeSpanType_Distance,
        eParticleLifeSpanType_HeightRange,
        eParticleLifeSpanType_AnimFrame
    }
}
