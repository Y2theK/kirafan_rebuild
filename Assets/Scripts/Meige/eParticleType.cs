﻿namespace Meige {
    public enum eParticleType {
        eParticleType_Invalid = -1,
        eParticleType_Billboard,
        eParticleType_Point,
        eParticleType_Line,
        eParticleType_PolyLine,
        eParticleType_Confetti,
        eParticleType_Ribbon
    }
}
