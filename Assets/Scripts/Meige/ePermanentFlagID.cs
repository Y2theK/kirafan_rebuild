﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012C7 RID: 4807
	[Token(Token = "0x2000C79")]
	[StructLayout(3, Size = 4)]
	public enum ePermanentFlagID
	{
		// Token: 0x04007247 RID: 29255
		[Token(Token = "0x40050AC")]
		ePermanentFlagID_None,
		// Token: 0x04007248 RID: 29256
		[Token(Token = "0x40050AD")]
		ePermanentFlagID_Max
	}
}
