﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012FA RID: 4858
	[Token(Token = "0x2000C9C")]
	[StructLayout(3, Size = 4)]
	public enum ePixelFormat
	{
		// Token: 0x040073C8 RID: 29640
		[Token(Token = "0x40051DC")]
		ePixelFormat_Invalid = -1,
		// Token: 0x040073C9 RID: 29641
		[Token(Token = "0x40051DD")]
		ePixelFormat_R8G8B8A8,
		// Token: 0x040073CA RID: 29642
		[Token(Token = "0x40051DE")]
		ePixelFormat_R10G10B10A2,
		// Token: 0x040073CB RID: 29643
		[Token(Token = "0x40051DF")]
		ePixelFormat_R4G4B4A4,
		// Token: 0x040073CC RID: 29644
		[Token(Token = "0x40051E0")]
		ePixelFormat_R5G5B5A1,
		// Token: 0x040073CD RID: 29645
		[Token(Token = "0x40051E1")]
		ePixelFormat_L8,
		// Token: 0x040073CE RID: 29646
		[Token(Token = "0x40051E2")]
		ePixelFormat_A8,
		// Token: 0x040073CF RID: 29647
		[Token(Token = "0x40051E3")]
		ePixelFormat_P4,
		// Token: 0x040073D0 RID: 29648
		[Token(Token = "0x40051E4")]
		ePixelFormat_P8,
		// Token: 0x040073D1 RID: 29649
		[Token(Token = "0x40051E5")]
		ePixelFormat_DXT1,
		// Token: 0x040073D2 RID: 29650
		[Token(Token = "0x40051E6")]
		ePixelFormat_DXT3,
		// Token: 0x040073D3 RID: 29651
		[Token(Token = "0x40051E7")]
		ePixelFormat_DXT5,
		// Token: 0x040073D4 RID: 29652
		[Token(Token = "0x40051E8")]
		ePixelFormat_F32,
		// Token: 0x040073D5 RID: 29653
		[Token(Token = "0x40051E9")]
		ePixelFormat_S8D24,
		// Token: 0x040073D6 RID: 29654
		[Token(Token = "0x40051EA")]
		ePixelFormat_D32,
		// Token: 0x040073D7 RID: 29655
		[Token(Token = "0x40051EB")]
		ePixelFormat_YUV,
		// Token: 0x040073D8 RID: 29656
		[Token(Token = "0x40051EC")]
		ePixelFormat_R16G16B16A16,
		// Token: 0x040073D9 RID: 29657
		[Token(Token = "0x40051ED")]
		ePixelFormat_R8G8B8,
		// Token: 0x040073DA RID: 29658
		[Token(Token = "0x40051EE")]
		ePixelFormat_D16,
		// Token: 0x040073DB RID: 29659
		[Token(Token = "0x40051EF")]
		ePixelFormat_R5G6B5,
		// Token: 0x040073DC RID: 29660
		[Token(Token = "0x40051F0")]
		ePixelFormat_R32G32,
		// Token: 0x040073DD RID: 29661
		[Token(Token = "0x40051F1")]
		ePixelFormat_R16G16,
		// Token: 0x040073DE RID: 29662
		[Token(Token = "0x40051F2")]
		ePixelForma_Cnt
	}
}
