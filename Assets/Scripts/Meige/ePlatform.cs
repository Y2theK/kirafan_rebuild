﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012E0 RID: 4832
	[Token(Token = "0x2000C82")]
	[StructLayout(3, Size = 4)]
	public enum ePlatform
	{
		// Token: 0x040072B3 RID: 29363
		[Token(Token = "0x40050C7")]
		ePlatform_Invalid = -1,
		// Token: 0x040072B4 RID: 29364
		[Token(Token = "0x40050C8")]
		ePlatform_WIN,
		// Token: 0x040072B5 RID: 29365
		[Token(Token = "0x40050C9")]
		ePlatform_VITA,
		// Token: 0x040072B6 RID: 29366
		[Token(Token = "0x40050CA")]
		ePlatform_PSP,
		// Token: 0x040072B7 RID: 29367
		[Token(Token = "0x40050CB")]
		ePlatform_Cnt
	}
}
