﻿namespace Meige {
    public enum eShadingType {
        eShadingType_Invalid = -1,
        eShadingType_Unlight,
        eShadingType_BlinnPhong,
        eShadingType_Toon,
        eShadingType_BlinnToon,
        eShadingType_Outline,
        eShadingType_Regular_Max,
        eShadingType_User_Start = 256,
        eShadingType_User_Before = 255,
        eShadingType_PostProcess_Start = 1024,
        eShadingType_PostProcess_Before = 1023,
        eShadingType_ColorCorrection,
        eShadingType_BrightPassFilter,
        eShadingType_Gauss,
        eShadingType_Particle,
        eShadingType_PostProcessUser_Start = 1280,
        eShadingType_PostProcessUser_Before = 1279
    }
}
