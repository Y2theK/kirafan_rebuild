﻿namespace Meige {
    public enum eStencilOp {
        Keep,
        Zero,
        Replace,
        IncrementSaturate,
        DecrementSaturate,
        Invert,
        IncrementWrap,
        DecrementWrap
    }
}
