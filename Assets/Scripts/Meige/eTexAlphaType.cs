﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012F1 RID: 4849
	[Token(Token = "0x2000C93")]
	[StructLayout(3, Size = 4)]
	public enum eTexAlphaType
	{
		// Token: 0x04007359 RID: 29529
		[Token(Token = "0x400516D")]
		eTexAlpha_Invalid = -1,
		// Token: 0x0400735A RID: 29530
		[Token(Token = "0x400516E")]
		eTexAlpha_Opaque,
		// Token: 0x0400735B RID: 29531
		[Token(Token = "0x400516F")]
		eTexAlpha_PunchThrough,
		// Token: 0x0400735C RID: 29532
		[Token(Token = "0x4005170")]
		eTexAlpha_Alpha
	}
}
