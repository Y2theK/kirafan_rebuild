﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x020012F3 RID: 4851
	[Token(Token = "0x2000C95")]
	[StructLayout(3, Size = 4)]
	public enum eTextureMapType
	{
		// Token: 0x04007362 RID: 29538
		[Token(Token = "0x4005176")]
		eTextureMapType_Std,
		// Token: 0x04007363 RID: 29539
		[Token(Token = "0x4005177")]
		eTextureMapType_SSDistorsion,
		// Token: 0x04007364 RID: 29540
		[Token(Token = "0x4005178")]
		eTextureMapType_Max
	}
}
