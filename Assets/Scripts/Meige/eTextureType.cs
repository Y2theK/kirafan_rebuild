﻿namespace Meige {
    public enum eTextureType {
        eTextureType_Invalid = -1,
        eTextureType_Albedo,
        eTextureType_Normal,
        eTextureType_Specular,
        eTextureType_Additional_Start,
        eTextureType_Additional0 = 3,
        eTextureType_Max
    }
}
