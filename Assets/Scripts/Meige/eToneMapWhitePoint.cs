﻿namespace Meige {
    public enum eToneMapWhitePoint {
        eToneMapWhitePoint_ToneLuminanceValue,
        eToneMapWhitePoint_ToneLuminanceValueMax,
        eToneMapWhitePoint_Fix
    }
}
