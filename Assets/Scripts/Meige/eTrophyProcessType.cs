﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001334 RID: 4916
	[Token(Token = "0x2000CC0")]
	[StructLayout(3, Size = 4)]
	public enum eTrophyProcessType
	{
		// Token: 0x0400752E RID: 29998
		[Token(Token = "0x40052E2")]
		Unlock,
		// Token: 0x0400752F RID: 29999
		[Token(Token = "0x40052E3")]
		DetailsGroup,
		// Token: 0x04007530 RID: 30000
		[Token(Token = "0x40052E4")]
		DetailsAll,
		// Token: 0x04007531 RID: 30001
		[Token(Token = "0x40052E5")]
		UnlockedFlg
	}
}
