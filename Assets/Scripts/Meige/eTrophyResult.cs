﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Meige
{
	// Token: 0x02001335 RID: 4917
	[Token(Token = "0x2000CC1")]
	[StructLayout(3, Size = 4)]
	public enum eTrophyResult
	{
		// Token: 0x04007533 RID: 30003
		[Token(Token = "0x40052E7")]
		ErrorInvalidTrophyID = -1,
		// Token: 0x04007534 RID: 30004
		[Token(Token = "0x40052E8")]
		CanNotAccess,
		// Token: 0x04007535 RID: 30005
		[Token(Token = "0x40052E9")]
		Success,
		// Token: 0x04007536 RID: 30006
		[Token(Token = "0x40052EA")]
		Unlocked
	}
}
