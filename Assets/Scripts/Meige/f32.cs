﻿using Meige.GenericOperator;
using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public struct f32 : IComparable, IComparable<f32>, IOperatorFast<f32> {
        [SerializeField] public float m_Value;

        public float value {
            get => m_Value;
            set => m_Value = value;
        }

        public f32(float value) {
            m_Value = value;
        }

        public f32(f32 value) {
            m_Value = value.value;
        }

        public void Add(f32 a, f32 b) {
            this = a + b;
        }

        public void Sub(f32 a, f32 b) {
            this = a - b;
        }

        public void Mul(f32 a, f32 b) {
            this = a * b;
        }

        public void Mul(f32 a, float b) {
            this = a * b;
        }

        public void Div(f32 a, f32 b) {
            this = a / b;
        }

        public void Add(f32 b) {
            this += b;
        }

        public void Sub(f32 b) {
            this -= b;
        }

        public void Mul(f32 b) {
            this *= b;
        }

        public void Mul(float b) {
            this *= b;
        }

        public void Div(f32 b) {
            this *= b; //what?
        }

        public void SetRowValue(object o) {
            m_Value = (float)o;
        }

        public int CompareTo(object obj) {
            float? num = obj as float?;
            if (num == null) {
                return 1;
            }
            return CompareTo(num);
        }

        public int CompareTo(f32 target) {
            return m_Value.CompareTo(target.value);
        }

        public override bool Equals(object obj) {
            float? num = obj as float?;
            return num != null && Equals(num);
        }

        public bool Equals(float obj) {
            return m_Value == obj;
        }

        public override int GetHashCode() {
            return m_Value.GetHashCode();
        }

        public static implicit operator f32(float v) {
            return new f32(v);
        }

        public static implicit operator float(f32 v) {
            return v.value;
        }

        public static f32 operator +(f32 v0) {
            return v0;
        }

        public static f32 operator +(f32 v0, f32 v1) {
            f32 result;
            result.m_Value = v0.m_Value + v1.m_Value;
            return result;
        }

        public static f32 operator -(f32 v0) {
            f32 result;
            result.m_Value = -v0.m_Value;
            return result;
        }

        public static f32 operator -(f32 v0, f32 v1) {
            f32 result;
            result.m_Value = v0.m_Value - v1.m_Value;
            return result;
        }

        public static f32 operator *(f32 v0, f32 v1) {
            f32 result;
            result.m_Value = v0.m_Value * v1.m_Value;
            return result;
        }

        public static f32 operator *(f32 v0, float v1) {
            f32 result;
            result.m_Value = v0.m_Value * v1;
            return result;
        }

        public static f32 operator /(f32 v0, f32 v1) {
            f32 result;
            result.m_Value = v0.m_Value / v1.m_Value;
            return result;
        }

        public static f32 operator /(f32 v0, float v1) {
            f32 result;
            result.m_Value = v0.m_Value / v1;
            return result;
        }

        public static bool operator ==(f32 v0, f32 v1) {
            return v0.m_Value == v1.m_Value;
        }

        public static bool operator !=(f32 v0, f32 v1) {
            return v0.m_Value != v1.m_Value;
        }
    }
}
