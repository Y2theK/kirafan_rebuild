﻿using Meige.GenericOperator;
using System;
using UnityEngine;

namespace Meige {
    [Serializable]
    public struct s32 : IComparable, IComparable<s32>, IOperatorFast<s32> {
        [SerializeField] public int m_Value;

        public int value {
            get => m_Value;
            set => m_Value = value;
        }

        public s32(int value) {
            m_Value = value;
        }

        public s32(s32 value) {
            m_Value = value.value;
        }

        public void Add(s32 a, s32 b) {
            this = a + b;
        }

        public void Sub(s32 a, s32 b) {
            this = a - b;
        }

        public void Mul(s32 a, s32 b) {
            this = a * b;
        }

        public void Mul(s32 a, float b) {
            this = a * b;
        }

        public void Div(s32 a, s32 b) {
            this = a / b;
        }

        public void Add(s32 b) {
            this += b;
        }

        public void Sub(s32 b) {
            this -= b;
        }

        public void Mul(s32 b) {
            this *= b;
        }

        public void Mul(float b) {
            this *= b;
        }

        public void Div(s32 b) {
            this *= b;
        }

        public void SetRowValue(object o) {
            m_Value = (int)o;
        }

        public int CompareTo(object obj) {
            float? num = obj as float?;
            if (num == null) {
                return 1;
            }
            return CompareTo(num);
        }

        public int CompareTo(s32 target) {
            return m_Value.CompareTo(target.value);
        }

        public override bool Equals(object obj) {
            float? num = obj as float?;
            return num != null && Equals(num);
        }

        public bool Equals(float obj) {
            return m_Value == obj;
        }

        public override int GetHashCode() {
            return m_Value.GetHashCode();
        }

        public static implicit operator s32(int v) {
            return new s32(v);
        }

        public static implicit operator int(s32 v) {
            return v.value;
        }

        public static s32 operator +(s32 v0) {
            return v0;
        }

        public static s32 operator +(s32 v0, s32 v1) {
            s32 result;
            result.m_Value = v0.m_Value + v1.m_Value;
            return result;
        }

        public static s32 operator -(s32 v0) {
            s32 result;
            result.m_Value = -v0.m_Value;
            return result;
        }

        public static s32 operator -(s32 v0, s32 v1) {
            s32 result;
            result.m_Value = v0.m_Value - v1.m_Value;
            return result;
        }

        public static s32 operator *(s32 v0, s32 v1) {
            s32 result;
            result.m_Value = v0.m_Value * v1.m_Value;
            return result;
        }

        public static s32 operator *(s32 v0, float v1) {
            s32 result;
            result.m_Value = (int)(v0.m_Value * v1);
            return result;
        }

        public static s32 operator /(s32 v0, s32 v1) {
            s32 result;
            result.m_Value = v0.m_Value / v1.m_Value;
            return result;
        }

        public static s32 operator /(s32 v0, float v1) {
            s32 result;
            result.m_Value = (int)(v0.m_Value / v1);
            return result;
        }

        public static bool operator ==(s32 v0, s32 v1) {
            return v0.m_Value == v1.m_Value;
        }

        public static bool operator !=(s32 v0, s32 v1) {
            return v0.m_Value != v1.m_Value;
        }
    }
}
