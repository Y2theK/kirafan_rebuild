﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige.GenericOperator;
using UnityEngine;

namespace Meige
{
	// Token: 0x02001273 RID: 4723
	[Token(Token = "0x2000C61")]
	[Serializable]
	[StructLayout(0, Size = 4)]
	public struct u32 : IComparable, IComparable<u32>, IOperatorFast<u32>
	{
		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x06005FD6 RID: 24534 RVA: 0x0001F3F8 File Offset: 0x0001D5F8
		// (set) Token: 0x06005FD5 RID: 24533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000600")]
		public uint value
		{
			[Token(Token = "0x60058C1")]
			[Address(RVA = "0x100038EAC", Offset = "0x38EAC", VA = "0x100038EAC")]
			get
			{
				return 0U;
			}
			[Token(Token = "0x60058C0")]
			[Address(RVA = "0x100038EA4", Offset = "0x38EA4", VA = "0x100038EA4")]
			set
			{
			}
		}

		// Token: 0x06005FD7 RID: 24535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C2")]
		[Address(RVA = "0x100038EB4", Offset = "0x38EB4", VA = "0x100038EB4")]
		public u32(uint value)
		{
		}

		// Token: 0x06005FD8 RID: 24536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C3")]
		[Address(RVA = "0x100038EBC", Offset = "0x38EBC", VA = "0x100038EBC")]
		public u32(u32 value)
		{
		}

		// Token: 0x06005FD9 RID: 24537 RVA: 0x0001F410 File Offset: 0x0001D610
		[Token(Token = "0x60058C4")]
		[Address(RVA = "0x1015F18EC", Offset = "0x15F18EC", VA = "0x1015F18EC")]
		public static implicit operator u32(uint v)
		{
			return default(u32);
		}

		// Token: 0x06005FDA RID: 24538 RVA: 0x0001F428 File Offset: 0x0001D628
		[Token(Token = "0x60058C5")]
		[Address(RVA = "0x1015F18F4", Offset = "0x15F18F4", VA = "0x1015F18F4")]
		public static implicit operator uint(u32 v)
		{
			return 0U;
		}

		// Token: 0x06005FDB RID: 24539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C6")]
		[Address(RVA = "0x100038EC4", Offset = "0x38EC4", VA = "0x100038EC4", Slot = "6")]
		public void Add(u32 a, u32 b)
		{
		}

		// Token: 0x06005FDC RID: 24540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C7")]
		[Address(RVA = "0x100038ED0", Offset = "0x38ED0", VA = "0x100038ED0", Slot = "7")]
		public void Sub(u32 a, u32 b)
		{
		}

		// Token: 0x06005FDD RID: 24541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C8")]
		[Address(RVA = "0x100038EDC", Offset = "0x38EDC", VA = "0x100038EDC", Slot = "8")]
		public void Mul(u32 a, u32 b)
		{
		}

		// Token: 0x06005FDE RID: 24542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058C9")]
		[Address(RVA = "0x100038EE8", Offset = "0x38EE8", VA = "0x100038EE8", Slot = "9")]
		public void Mul(u32 a, float b)
		{
		}

		// Token: 0x06005FDF RID: 24543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CA")]
		[Address(RVA = "0x100038F08", Offset = "0x38F08", VA = "0x100038F08", Slot = "10")]
		public void Div(u32 a, u32 b)
		{
		}

		// Token: 0x06005FE0 RID: 24544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CB")]
		[Address(RVA = "0x100038F14", Offset = "0x38F14", VA = "0x100038F14", Slot = "11")]
		public void Add(u32 b)
		{
		}

		// Token: 0x06005FE1 RID: 24545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CC")]
		[Address(RVA = "0x100038F24", Offset = "0x38F24", VA = "0x100038F24", Slot = "12")]
		public void Sub(u32 b)
		{
		}

		// Token: 0x06005FE2 RID: 24546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CD")]
		[Address(RVA = "0x100038F34", Offset = "0x38F34", VA = "0x100038F34", Slot = "13")]
		public void Mul(u32 b)
		{
		}

		// Token: 0x06005FE3 RID: 24547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CE")]
		[Address(RVA = "0x100038F44", Offset = "0x38F44", VA = "0x100038F44", Slot = "14")]
		public void Mul(float b)
		{
		}

		// Token: 0x06005FE4 RID: 24548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058CF")]
		[Address(RVA = "0x100038F68", Offset = "0x38F68", VA = "0x100038F68", Slot = "15")]
		public void Div(u32 b)
		{
		}

		// Token: 0x06005FE5 RID: 24549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60058D0")]
		[Address(RVA = "0x100038F78", Offset = "0x38F78", VA = "0x100038F78", Slot = "16")]
		public void SetRowValue(object o)
		{
		}

		// Token: 0x06005FE6 RID: 24550 RVA: 0x0001F440 File Offset: 0x0001D640
		[Token(Token = "0x60058D1")]
		[Address(RVA = "0x100038F80", Offset = "0x38F80", VA = "0x100038F80", Slot = "4")]
		public int CompareTo(object obj)
		{
			return 0;
		}

		// Token: 0x06005FE7 RID: 24551 RVA: 0x0001F458 File Offset: 0x0001D658
		[Token(Token = "0x60058D2")]
		[Address(RVA = "0x100038F9C", Offset = "0x38F9C", VA = "0x100038F9C", Slot = "5")]
		public int CompareTo(u32 target)
		{
			return 0;
		}

		// Token: 0x06005FE8 RID: 24552 RVA: 0x0001F470 File Offset: 0x0001D670
		[Token(Token = "0x60058D3")]
		[Address(RVA = "0x100038FB4", Offset = "0x38FB4", VA = "0x100038FB4", Slot = "0")]
		public override bool Equals(object obj)
		{
			return default(bool);
		}

		// Token: 0x06005FE9 RID: 24553 RVA: 0x0001F488 File Offset: 0x0001D688
		[Token(Token = "0x60058D4")]
		[Address(RVA = "0x100038FD0", Offset = "0x38FD0", VA = "0x100038FD0")]
		public bool Equals(float obj)
		{
			return default(bool);
		}

		// Token: 0x06005FEA RID: 24554 RVA: 0x0001F4A0 File Offset: 0x0001D6A0
		[Token(Token = "0x60058D5")]
		[Address(RVA = "0x100038FE4", Offset = "0x38FE4", VA = "0x100038FE4", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06005FEB RID: 24555 RVA: 0x0001F4B8 File Offset: 0x0001D6B8
		[Token(Token = "0x60058D6")]
		[Address(RVA = "0x1015F1C24", Offset = "0x15F1C24", VA = "0x1015F1C24")]
		public static u32 operator +(u32 v0)
		{
			return default(u32);
		}

		// Token: 0x06005FEC RID: 24556 RVA: 0x0001F4D0 File Offset: 0x0001D6D0
		[Token(Token = "0x60058D7")]
		[Address(RVA = "0x1015F18F8", Offset = "0x15F18F8", VA = "0x1015F18F8")]
		public static u32 operator +(u32 v0, u32 v1)
		{
			return default(u32);
		}

		// Token: 0x06005FED RID: 24557 RVA: 0x0001F4E8 File Offset: 0x0001D6E8
		[Token(Token = "0x60058D8")]
		[Address(RVA = "0x1015F1C2C", Offset = "0x15F1C2C", VA = "0x1015F1C2C")]
		public static u32 operator -(u32 v0)
		{
			return default(u32);
		}

		// Token: 0x06005FEE RID: 24558 RVA: 0x0001F500 File Offset: 0x0001D700
		[Token(Token = "0x60058D9")]
		[Address(RVA = "0x1015F1900", Offset = "0x15F1900", VA = "0x1015F1900")]
		public static u32 operator -(u32 v0, u32 v1)
		{
			return default(u32);
		}

		// Token: 0x06005FEF RID: 24559 RVA: 0x0001F518 File Offset: 0x0001D718
		[Token(Token = "0x60058DA")]
		[Address(RVA = "0x1015F1908", Offset = "0x15F1908", VA = "0x1015F1908")]
		public static u32 operator *(u32 v0, u32 v1)
		{
			return default(u32);
		}

		// Token: 0x06005FF0 RID: 24560 RVA: 0x0001F530 File Offset: 0x0001D730
		[Token(Token = "0x60058DB")]
		[Address(RVA = "0x1015F1910", Offset = "0x15F1910", VA = "0x1015F1910")]
		public static u32 operator *(u32 v0, float v1)
		{
			return default(u32);
		}

		// Token: 0x06005FF1 RID: 24561 RVA: 0x0001F548 File Offset: 0x0001D748
		[Token(Token = "0x60058DC")]
		[Address(RVA = "0x1015F192C", Offset = "0x15F192C", VA = "0x1015F192C")]
		public static u32 operator /(u32 v0, u32 v1)
		{
			return default(u32);
		}

		// Token: 0x06005FF2 RID: 24562 RVA: 0x0001F560 File Offset: 0x0001D760
		[Token(Token = "0x60058DD")]
		[Address(RVA = "0x1015F1C34", Offset = "0x15F1C34", VA = "0x1015F1C34")]
		public static u32 operator /(u32 v0, float v1)
		{
			return default(u32);
		}

		// Token: 0x06005FF3 RID: 24563 RVA: 0x0001F578 File Offset: 0x0001D778
		[Token(Token = "0x60058DE")]
		[Address(RVA = "0x1015F1C50", Offset = "0x15F1C50", VA = "0x1015F1C50")]
		public static bool operator ==(u32 v0, u32 v1)
		{
			return default(bool);
		}

		// Token: 0x06005FF4 RID: 24564 RVA: 0x0001F590 File Offset: 0x0001D790
		[Token(Token = "0x60058DF")]
		[Address(RVA = "0x1015F1C5C", Offset = "0x15F1C5C", VA = "0x1015F1C5C")]
		public static bool operator !=(u32 v0, u32 v1)
		{
			return default(bool);
		}

		// Token: 0x04006F18 RID: 28440
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004ED5")]
		[SerializeField]
		public uint m_Value;
	}
}
