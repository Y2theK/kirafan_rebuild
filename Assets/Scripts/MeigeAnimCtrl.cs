﻿using Meige;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class MeigeAnimCtrl : MonoBehaviour {
    private float m_OldNormalizedPlayTime;
    private ePlayState m_PlayState = ePlayState.ePlayState_Invalid;
    private Dictionary<string, ReferenceAnimHandler> m_ReferenceAnimHandlerDic;
    private TargetInstance[] m_TargetInstanceCache;
    private ReferenceAnimHandler m_CurAnim;

    public bool m_bManualUpdate;
    public bool m_bFixedDeltaTime;
    public float m_AnimationPlaySpeed = 1f;
    public float m_AnimationPlayTime;
    public float m_NormalizedPlayTime;
    public WrapMode m_WrapMode = WrapMode.Loop;
    public List<ReferenceAnimHandler> m_ReferenceAnimHandlerList;

    public void Init() {
        m_AnimationPlayTime = 0f;
        m_PlayState = ePlayState.ePlayState_Invalid;
        m_TargetInstanceCache = null;
        m_CurAnim = null;
        m_ReferenceAnimHandlerDic = null;
    }

    public ReferenceAnimHandler GetReferenceAnimHandler(string clipName) {
        m_ReferenceAnimHandlerDic.TryGetValue(clipName, out ReferenceAnimHandler result);
        return result;
    }

    public void EnableManualUpdate(bool b) {
        m_bManualUpdate = b;
    }

    public void SetAnimationPlaySpeed(float spd) {
        m_AnimationPlaySpeed = spd;
    }

    public ePlayState GetPlayState() {
        if (m_bManualUpdate) {
            return ePlayState.ePlaySteta_Manual;
        }
        return m_PlayState;
    }

    public void Open() {
        m_ReferenceAnimHandlerList = new List<ReferenceAnimHandler>();
        m_ReferenceAnimHandlerDic = new Dictionary<string, ReferenceAnimHandler>();
        m_TargetInstanceCache = null;
        m_CurAnim = null;
        m_PlayState = ePlayState.ePlayState_Invalid;
    }

    public bool AddClip(MsbHandler targetMsb, MeigeAnimClipHolder clipHolder) {
        if (targetMsb == null || clipHolder == null) {
            return false;
        }

        string[] nameSplit;
        if (string.IsNullOrEmpty(clipHolder.m_MeigeAnimClip.m_Name)) {
            nameSplit = clipHolder.m_UnityAnimClip.name.Split('@');
        } else {
            nameSplit = clipHolder.m_MeigeAnimClip.m_Name.Split('@');
        }
        string name = (nameSplit == null || nameSplit.Length <= 1) ? string.Empty : nameSplit[1];
        if (!m_ReferenceAnimHandlerDic.TryGetValue(name, out ReferenceAnimHandler handler)) {
            handler = new ReferenceAnimHandler();
            handler.m_ClipName = name;
            handler.m_AnimationTime = 0f;
            m_ReferenceAnimHandlerList.Add(handler);
            m_ReferenceAnimHandlerDic.Add(name, handler);
            handler.m_ClipTargetHandlerList = new List<ClipTargetHandler>();
        }
        ClipTargetHandler clipTargetHandler = new ClipTargetHandler();
        clipTargetHandler.m_Target = targetMsb;
        clipTargetHandler.m_Clip = clipHolder;
        if (clipHolder.m_MeigeAnimClip != null) {
            handler.m_AnimationTime = Mathf.Max(handler.m_AnimationTime, clipHolder.m_MeigeAnimClip.m_AnimTimeBySec);
        }
        if (clipHolder.m_UnityAnimClip != null) {
            handler.m_AnimationTime = Mathf.Max(handler.m_AnimationTime, clipHolder.m_UnityAnimClip.length);
            Animation animTarget = targetMsb.GetAnimationTarget();
            if (animTarget != null) {
                animTarget.AddClip(clipHolder.m_UnityAnimClip, clipHolder.m_UnityAnimClip.name);
            }
        }
        handler.m_ClipTargetHandlerList.Add(clipTargetHandler);
        return true;
    }

    public void Close() {
        int cacheSize = 0;
        for (int i = 0; i < m_ReferenceAnimHandlerList.Count; i++) {
            cacheSize = Mathf.Max(cacheSize, m_ReferenceAnimHandlerList[i].CalcNumOfMabAnimNode());
        }
        m_TargetInstanceCache = new TargetInstance[cacheSize];
    }

    public void Play(string clipName, float blendSec, WrapMode wrap) {
        if (wrap == WrapMode.Default) {
            wrap = WrapMode.Loop;
        }
        m_CurAnim = m_ReferenceAnimHandlerDic[clipName];
        int cacheIdx = 0;
        for (int i = 0; i < m_CurAnim.m_ClipTargetHandlerList.Count; i++) {
            ClipTargetHandler clipHandler = m_CurAnim.m_ClipTargetHandlerList[i];
            Animation animationTarget = clipHandler.m_Target.GetAnimationTarget();
            if (clipHandler.m_Clip.m_UnityAnimClip) {
                Helper.InterpolateAnimation(animationTarget, clipHandler.m_Clip.m_UnityAnimClip.name, blendSec, wrap);
            } else {
                animationTarget.Stop();
            }
            MeigeAnimEventNotifier notifier = clipHandler.m_Target.GetAnimEventNotifier();
            if (notifier != null) {
                notifier.UpdateAnimClip(clipHandler.m_Clip.m_MeigeAnimClip);
            }
            for (int j = 0; j < clipHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length; j++) {
                MabAnimNodeHandler nodeHandler = clipHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray[j];
                nodeHandler.Setup(ref m_TargetInstanceCache[cacheIdx], clipHandler.m_Target);
                cacheIdx++;
            }
        }
        m_AnimationPlayTime = 0f;
        m_NormalizedPlayTime = -0.1f;
        m_OldNormalizedPlayTime = -0.1f;
        m_WrapMode = wrap;
        m_PlayState = ePlayState.ePlayState_Play;
    }

    public void Pause() {
        if (m_CurAnim != null) {
            m_PlayState = ePlayState.ePlayState_Pause;
        }
    }

    public void UpdateAnimation(float sec) {
        m_OldNormalizedPlayTime = m_NormalizedPlayTime;
        m_NormalizedPlayTime = sec / m_CurAnim.m_AnimationTime;
        switch (m_WrapMode) {
            case WrapMode.Once:
                m_NormalizedPlayTime = m_NormalizedPlayTime > 1f ? 0f : m_NormalizedPlayTime;
                break;
            case WrapMode.Loop:
                m_NormalizedPlayTime = Mathf.Repeat(m_NormalizedPlayTime, 1f);
                break;
            case WrapMode.PingPong:
                m_NormalizedPlayTime = Mathf.PingPong(m_NormalizedPlayTime, 1f);
                break;
            case WrapMode.ClampForever:
                m_NormalizedPlayTime = Mathf.Min(1f, m_NormalizedPlayTime);
                break;
        }
        if (m_OldNormalizedPlayTime > m_NormalizedPlayTime) {
            m_OldNormalizedPlayTime = -0.1f;
        }
        int cacheIdx = 0;
        float animTime = m_CurAnim.m_AnimationTime * m_NormalizedPlayTime;
        for (int i = 0; i < m_CurAnim.m_ClipTargetHandlerList.Count; i++) {
            ClipTargetHandler clipHandler = m_CurAnim.m_ClipTargetHandlerList[i];
            AnimationClip unityClip = clipHandler.m_Clip.m_UnityAnimClip;
            if (unityClip != null) {
                Animation animationTarget = clipHandler.m_Target.GetAnimationTarget();
                AnimationState state = animationTarget[unityClip.name];
                state.time = Mathf.Clamp(animTime, 0f, state.length);
                state.speed = 0f;
            }
            MabHandler meigeClip = clipHandler.m_Clip.m_MeigeAnimClip;
            if (meigeClip != null) {
                float clipTime = Mathf.Clamp(animTime / meigeClip.m_AnimTimeBySec, 0f, 1f);
                float frame = clipTime * (meigeClip.m_NumOfKeyframe - 1);
                for (int j = 0; j < clipHandler.m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length; j++) {
                    MabAnimNodeHandler nodeHandler = meigeClip.m_AnimNodeHandlerArray[j];
                    nodeHandler.Animate(ref m_TargetInstanceCache[cacheIdx], frame);
                    cacheIdx++;
                }
            }
        }
    }

    private void Update() {
        if (m_CurAnim == null || m_TargetInstanceCache == null) {
            return;
        }
        if (m_bManualUpdate || m_PlayState == ePlayState.ePlayState_Pause) {
            for (int i = 0; i < m_CurAnim.m_ClipTargetHandlerList.Count; i++) {
                ClipTargetHandler clipHandler = m_CurAnim.m_ClipTargetHandlerList[i];
                if (clipHandler.m_Target.m_AnimationTarget[clipHandler.m_Clip.m_UnityAnimClip.name] != null) {
                    clipHandler.m_Target.m_AnimationTarget[clipHandler.m_Clip.m_UnityAnimClip.name].speed = 0f;
                }
            }
        } else if (m_PlayState == ePlayState.ePlayState_Play) {
            UpdateAnimation(m_AnimationPlayTime);
            if (m_bFixedDeltaTime) {
                m_AnimationPlayTime += Time.fixedDeltaTime + MeigeDefs.F_EPSILON6;
            } else {
                m_AnimationPlayTime += Time.deltaTime * m_AnimationPlaySpeed + MeigeDefs.F_EPSILON6;
            }
        }
    }

    private void LateUpdate() {
        if (m_CurAnim == null || m_TargetInstanceCache == null) {
            return;
        }
        if (!m_bManualUpdate && m_PlayState != ePlayState.ePlayState_Play) {
            return;
        }
        for (int i = 0; i < m_CurAnim.m_ClipTargetHandlerList.Count; i++) {
            ClipTargetHandler clipHandler = m_CurAnim.m_ClipTargetHandlerList[i];
            MeigeAnimEventNotifier notifier = clipHandler.m_Target.GetAnimEventNotifier();
            if (notifier != null) {
                MabHandler meigeAnimClip = clipHandler.m_Clip.m_MeigeAnimClip;
                if (meigeAnimClip != null && meigeAnimClip.m_AnimEvArray != null) {
                    for (int j = 0; j < meigeAnimClip.m_AnimEvArray.Length; j++) {
                        MabHandler.MabAnimEvent mabAnimEvent = meigeAnimClip.m_AnimEvArray[j];
                        float normalizedTime = mabAnimEvent.m_Frame / (meigeAnimClip.m_NumOfKeyframe - 1);
                        if (m_OldNormalizedPlayTime < normalizedTime && m_NormalizedPlayTime >= normalizedTime) {
                            notifier.Notify_MeigeAnimEvent(j);
                        }
                    }
                }
            }
        }
    }

    [Serializable]
    public class ClipTargetHandler {
        public MeigeAnimClipHolder m_Clip;
        public MsbHandler m_Target;
    }

    [Serializable]
    public class ReferenceAnimHandler {
        public string m_ClipName;
        public float m_AnimationTime;
        public List<ClipTargetHandler> m_ClipTargetHandlerList;

        public int CalcNumOfMabAnimNode() {
            int num = 0;
            for (int i = 0; i < m_ClipTargetHandlerList.Count; i++) {
                num += m_ClipTargetHandlerList[i].m_Clip.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length;
            }
            return num;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct TargetInstance {
        [FieldOffset(0)] public UnityEngine.Object m_Reference;
        [FieldOffset(0)] public MsbObjectHandler.MsbObjectParam m_MsbObjWork;
        [FieldOffset(0)] public MsbMaterialHandler.MsbMaterialParam m_MsbMatWork;
        [FieldOffset(0)] public MsbMaterialHandler.MsbTextureParam m_MsbTextureWork;
        [FieldOffset(0)] public MeigeParticleEmitter m_MeigeParticleEmitterWork;
        [FieldOffset(0)] public MsbCameraHandler.MsbCameraParam m_MsbCameraWork;

        public void Init() {
            m_Reference = null;
        }
    }

    public enum ePlayState {
        ePlayState_Invalid = -1,
        ePlayState_Play,
        ePlayState_Pause,
        ePlaySteta_Manual
    }
}
