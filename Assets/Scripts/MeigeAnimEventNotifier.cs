﻿using UnityEngine;

public class MeigeAnimEventNotifier : MonoBehaviour {
    public const string NOTIFY_FUNCNAME = "Notify_MeigeAnimEvent";

    public delegate void OnEvent(MabHandler.MabAnimEvent ev);

    private OnEvent m_NotifyFuncList;
    private MabHandler m_MabHandler;

    public void Notify_MeigeAnimEvent(int ev_id) {
		if (m_NotifyFuncList != null && m_MabHandler != null && m_MabHandler.m_AnimEvArray != null) {
            if (ev_id < m_MabHandler.m_AnimEvArray.Length) {
                m_NotifyFuncList(m_MabHandler.m_AnimEvArray[ev_id]);
            }
        }
	}

	public void UpdateAnimClip(MabHandler mabHndl) {
        m_MabHandler = mabHndl;
    }

	public void ClearNotify() {
        m_NotifyFuncList = null;
    }

	public void AddNotify(OnEvent func) {
        m_NotifyFuncList += func;
    }

	public void RemoveNotify(OnEvent func) {
        m_NotifyFuncList -= func;
    }
}
