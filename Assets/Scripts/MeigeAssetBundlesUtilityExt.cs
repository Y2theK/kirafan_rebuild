﻿using Meige.AssetBundles;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

public static class MeigeAssetBundlesUtilityExt {
    public static string ToBundleHashName(this string assetBundlePath) {
        if (string.IsNullOrEmpty(assetBundlePath)) {
            return assetBundlePath;
        }
        string path = assetBundlePath.Replace("\\", "/");
        string fileName = Path.GetFileName(path);
        path = path.Replace(fileName, "");
        string ret = "";
        if (!string.IsNullOrEmpty(path)) {
            ret += GetStringHash(path) + "/";
        }
        ret += GetStringHash(fileName);
        return ret;
    }

    private static string GetStringHash(string src) {
        byte[] bytes = Encoding.UTF8.GetBytes(src).Concat(Utility.HashPathSrc.Take(src.Count())).ToArray();
        using (var hasher = new MD5CryptoServiceProvider()) {
            byte[] hash = hasher.ComputeHash(bytes);
            hasher.Clear();
            return BitConverter.ToString(hash).ToLower().Replace("-", "");
        }
    }
}
