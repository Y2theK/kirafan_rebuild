﻿using Meige;
using UnityEngine;

public class MeigeShaderUtility {
    private static string[] m_ShaderPropertyName = new string[] {
        "_ShadingType",
        "_BlendMode",
        "_BlendSrc",
        "_BlendDst",
        "_BlendOpColor",
        "_BlendOpAlpha",
        "_DepthTest",
        "_DepthWrite",
        "_DepthOffsetFactor",
        "_DepthOffsetUnits",
        "_Using_AlphaTest",
        "_CullMode",
        "_Lighting_Type",
        "_Using_Fog",
        "_MeshColor",
        "_HDRFactor",
        "_AlphaTestRefValue",
        "_Using_HalfAccuracy_Color",
        "_Texture_Albedo",
        "_Texture_AlbedoLayer",
        "_Texture_Normal",
        "_Texture_NormalLayer",
        "_Texture_Specular",
        "_Texture_SpecularLayer",
        "_Texture_Additional0",
        "_Texture_Additional0Layer",
        "_MatColor_Albedo",
        "_MatColor_AlbedoLayer",
        "_MatColor_Normal",
        "_MatColor_NormalLayer",
        "_MatColor_Specular",
        "_MatColor_SpecularLayer",
        "_MatColor_Additional0",
        "_MatColor_Additional0Layer",
        "_UVMatrix_Albedo",
        "_UVMatrix_AlbedoLayer",
        "_UVMatrix_Normal",
        "_UVMatrix_NormalLayer",
        "_UVMatrix_Specular",
        "_UVMatrix_SpecularLayer",
        "_UVMatrix_Additional0",
        "_UVMatrix_Additional0Layer",
        "_LayerColorBlendMode_Albedo",
        "_LayerAlphaBlendMode_Albedo",
        "_LayerColorBlendMode_Normal",
        "_LayerAlphaBlendMode_Normal",
        "_LayerColorBlendMode_Specular",
        "_LayerAlphaBlendMode_Specular",
        "_Using_Albedo_Texture",
        "_Using_Normal_Texture",
        "_Using_Specular_Texture",
        "_Using_Additional0_Texture",
        "_OutlineType",
        "_OutlineColor",
        "_OutlineWidth",
        "_OutlineOffset",
        "_OutlineBlendMode",
        "_OutlineBlendSrc",
        "_OutlineBlendDst",
        "_OutlineBlendOpColor",
        "_OutlineBlendOpAlpha",
        "_StencilComp",
        "_Stencil",
        "_StencilOp",
        "_StencilOpFail",
        "_StencilOpZFail",
        "_StencilWriteMask",
        "_StencilReadMask",
        "_OutlineStencilComp",
        "_OutlineStencil",
        "_OutlineStencilOp",
        "_OutlineStencilOpFail",
        "_OutlineStencilOpZFail",
        "_OutlineStencilWriteMask",
        "_OutlineStencilReadMask"
    };

    private static string[] m_ShaderKeywordName_ShaderType = new string[] {
        "_SHADINGTYPE_UNLIGHT",
        "_SHADINGTYPE_BLINNPHONG",
        "_SHADINGTYPE_TOON"
    };

    private static string[] m_ShaderKeywordName_AlphaTest = new string[] {
        "_USING_ALPHATEST_DISABLE",
        "_USING_ALPHATEST_ENABLE"
    };

    private static string[] m_ShaderKeywordName_Lighting = new string[] {
        "_LIGHTING_TYPE_VERTEX",
        "_LIGHTING_TYPE_PIXEL"
    };

    private static string[] m_ShaderKeywordName_Fog = new string[] {
        "_USING_FOG_DISABLE",
        "_USING_FOG_ENABLE"
    };

    private static string[] m_ShaderKeywordName_UsingTexture_Albedo = new string[] {
        "_USING_ALBEDO_TEXTURE_NONE",
        "_USING_ALBEDO_TEXTURE_SINGLE",
        "_USING_ALBEDO_TEXTURE_LAYER"
    };

    private static string[] m_ShaderKeywordName_UsingTexture_Normal = new string[] {
        "_USING_NORMAL_TEXTURE_NONE",
        "_USING_NORMAL_TEXTURE_SINGLE",
        "_USING_NORMAL_TEXTURE_LAYER"
    };

    private static string[] m_ShaderKeywordName_UsingTexture_Specular = new string[] {
        "_USING_SPECULAR_TEXTURE_NONE",
        "_USING_SPECULAR_TEXTURE_SINGLE",
        "_USING_SPECULAR_TEXTURE_LAYER"
    };

    private static string[] m_ShaderKeywordName_UsingTexture_Additional0 = new string[] {
        "_USING_ADDITIONAL0_TEXTURE_NONE",
        "_USING_ADDITIONAL0_TEXTURE_SINGLE",
        "_USING_ADDITIONAL0_TEXTURE_LAYER"
    };

    private static string[] m_ShaderKeywordName_HalfAccuracyColor = new string[] {
        "_USING_HALFACCURACY_COLOR_DISABLE",
        "_USING_HALFACCURACY_COLOR_ENABLE",
        "_USING_ADDITIONAL0_TEXTURE_LAYER"
    };

    private static string[] m_ShaderKeywordName_OutlineType = new string[] {
        "_OTLINETYPE_STD",
        "_OTLINETYPE_MULBASECOLOR"
    };

    private static BlendComponent[] m_blendComponent = new BlendComponent[] {
        new BlendComponent(eBlendOp.Add, eBlendFactor.One, eBlendFactor.Zero),
        new BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.OneMinusSrcAlpha),
        new BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.One),
        new BlendComponent(eBlendOp.RevSub, eBlendFactor.SrcAlpha, eBlendFactor.One),
        new BlendComponent(eBlendOp.Add, eBlendFactor.OneMinusDstColor, eBlendFactor.OneMinusSrcAlpha),
        new BlendComponent(eBlendOp.Add, eBlendFactor.One, eBlendFactor.Zero),
        new BlendComponent(eBlendOp.Add, eBlendFactor.Zero, eBlendFactor.One),
        new BlendComponent(eBlendOp.Add, eBlendFactor.DstColor, eBlendFactor.Zero),
        new BlendComponent(eBlendOp.Add, eBlendFactor.SrcAlpha, eBlendFactor.OneMinusSrcAlpha)
    };

    public static int GetPropertyID(string propertyName) {
        return Shader.PropertyToID(propertyName);
    }

	public static void MakeBlendingParam(Material material, eBlendMode blendMode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha) {
        if (blendMode == eBlendMode.eBlendMode_Invalid) {
            Helper.Break(false, "MakeBlendingParam .... BlendMode is Invalid.");
            src = eBlendFactor.SrcColor;
            dst = eBlendFactor.Zero;
            opColor = eBlendOp.Add;
            opAlpha = eBlendOp.Add;
            return;
        }
		BlendComponent component = m_blendComponent[(int)blendMode];
        src = component.m_BlendFactorSrc;
        dst = component.m_BlendFactorDst;
        opColor = component.m_BlendOp;
        opAlpha = eBlendOp.Add;
    }

	public static void GetBlendingParam(Material material, out eBlendMode mode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha) {
        src = (eBlendFactor)material.GetFloat(m_ShaderPropertyName[2]);
        dst = (eBlendFactor)material.GetFloat(m_ShaderPropertyName[3]);
        opColor = (eBlendOp)material.GetFloat(m_ShaderPropertyName[4]);
        opAlpha = (eBlendOp)material.GetFloat(m_ShaderPropertyName[5]);
        mode = (eBlendMode)material.GetFloat(m_ShaderPropertyName[1]);
    }

	public static eBlendMode ConvertLayerBlend(eLayerBlendMode mode) {
        eBlendMode[] convert = new eBlendMode[]
        {
            eBlendMode.eBlendMode_Std,
            eBlendMode.eBlendMode_Std,
            eBlendMode.eBlendMode_Add,
            eBlendMode.eBlendMode_Sub,
            eBlendMode.eBlendMode_Mul,
            eBlendMode.eBlendMode_SrcOne,
            eBlendMode.eBlendMode_DstOne
        };
        return convert[(int)mode];
	}

	public static eLayerBlendMode ConvertAlphaBlend(eBlendMode mode) {
        eLayerBlendMode[] convert = new eLayerBlendMode[]
        {
            eLayerBlendMode.eLayerBlendMode_Std,
            eLayerBlendMode.eLayerBlendMode_Std,
            eLayerBlendMode.eLayerBlendMode_Add,
            eLayerBlendMode.eLayerBlendMode_Sub,
            eLayerBlendMode.eLayerBlendMode_Std,
            eLayerBlendMode.eLayerBlendMode_SrcOne,
            eLayerBlendMode.eLayerBlendMode_DstOne,
            eLayerBlendMode.eLayerBlendMode_Mul,
            eLayerBlendMode.eLayerBlendMode_Std
        };
        return convert[(int)mode];
    }

	public static void SaveBlendingParam(Material material, eBlendMode mode) {
        if (mode != eBlendMode.eBlendMode_Custom) {
            MakeBlendingParam(material, mode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha);
            SaveShaderProperty(material, eShaderPropertyID.BlendMode, (float)mode);
            SaveBlendingParam(material, src, dst, opColor, opAlpha);
        }
    }

	public static void SaveBlendingParam(Material material, eBlendMode mode, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha) {
        if (mode != eBlendMode.eBlendMode_Custom) {
            MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
        }
        SaveShaderProperty(material, eShaderPropertyID.BlendMode, (float)mode);
        SaveBlendingParam(material, src, dst, opColor, opAlpha);
    }

	public static void SaveBlendingParam(Material material, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha) {
        SaveShaderProperty(material, eShaderPropertyID.BlendSrc, (float)src);
        SaveShaderProperty(material, eShaderPropertyID.BlendDst, (float)dst);
        SaveShaderProperty(material, eShaderPropertyID.BlendOpColor, (float)opColor);
        SaveShaderProperty(material, eShaderPropertyID.BlendOpAlpha, (float)opAlpha);
    }

	public static void SaveOutlineBlendingParam(Material material, eBlendMode mode) {
        if (mode != eBlendMode.eBlendMode_Custom) {
            MakeBlendingParam(material, mode, out eBlendFactor src, out eBlendFactor dst, out eBlendOp opColor, out eBlendOp opAlpha);
            SaveShaderProperty(material, eShaderPropertyID.OutlineBlendMode, (float)mode);
            SaveOutlineBlendingParam(material, src, dst, opColor, opAlpha);
        }
    }

	public static void SaveOutlineBlendingParam(Material material, eBlendMode mode, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha) {
        if (mode != eBlendMode.eBlendMode_Custom) {
            MakeBlendingParam(material, mode, out src, out dst, out opColor, out opAlpha);
        }
        SaveShaderProperty(material, eShaderPropertyID.OutlineBlendMode, (float)mode);
        SaveOutlineBlendingParam(material, src, dst, opColor, opAlpha);
    }

	public static void SaveOutlineBlendingParam(Material material, eBlendFactor src, eBlendFactor dst, eBlendOp opColor, eBlendOp opAlpha) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineBlendSrc, (float)src);
        SaveShaderProperty(material, eShaderPropertyID.OutlineBlendDst, (float)dst);
        SaveShaderProperty(material, eShaderPropertyID.OutlineBlendOpColor, (float)opColor);
        SaveShaderProperty(material, eShaderPropertyID.OutlineBlendOpAlpha, (float)opAlpha);
    }

	public static string GetShaderPropertyName(eShaderPropertyID propertyID) {
        return m_ShaderPropertyName[(int)propertyID];
    }

	public static void SaveShaderProperty(Material material, eShaderPropertyID propertyID, object value) {
        if (value == null) { return; }

		if (value is float f) {
            material.SetFloat(m_ShaderPropertyName[(int)propertyID], f);
        } else if (value is Texture || value is Texture2D || value is Texture3D || value is RenderTexture) {
            material.SetTexture(m_ShaderPropertyName[(int)propertyID], (Texture)value);
        } else if (value is Matrix4x4 matrix) {
            material.SetMatrix(m_ShaderPropertyName[(int)propertyID], matrix);
        } else if (value is Vector4 vector) {
            material.SetVector(m_ShaderPropertyName[(int)propertyID], vector);
        } else if (value is Color color) {
            material.SetColor(m_ShaderPropertyName[(int)propertyID], color);
        } else {
            material.SetInt(m_ShaderPropertyName[(int)propertyID], (int)value);
        }
	}

	public static void EnableKeyword(Material material, string[] keywords, int value) {
        for (int i = 0; i < keywords.Length; i++) {
            if (i == value) {
                material.EnableKeyword(keywords[i]);
            } else {
                material.DisableKeyword(keywords[i]);
            }
        }
    }

	public static void SetShadingType(Material material, eShadingType value) {
        EnableKeyword(material, m_ShaderKeywordName_ShaderType, (int)value);
        SaveShaderProperty(material, eShaderPropertyID.ShadingType, (float)value);
    }

	public static void SetAlphaBlendMode(Material material, eBlendMode value) {
        SaveBlendingParam(material, value);
    }

	public static void SetAlphaBlendSrcFactor(Material material, eBlendFactor value) {
        SaveShaderProperty(material, eShaderPropertyID.BlendSrc, (float)value);
    }

	public static void SetAlphaBlendDstFactor(Material material, eBlendFactor value) {
        SaveShaderProperty(material, eShaderPropertyID.BlendDst, (float)value);
    }

	public static void SetAlphaBlendOpColor(Material material, eBlendOp value) {
        SaveShaderProperty(material, eShaderPropertyID.BlendOpColor, (float)value);
    }

	public static void SetAlphaBlendOpAlpha(Material material, eBlendOp value) {
        SaveShaderProperty(material, eShaderPropertyID.BlendOpAlpha, (float)value);
    }

	public static void SetDepthTest(Material material, eCompareFunc value) {
        SaveShaderProperty(material, eShaderPropertyID.DepthTest, (float)value);
    }

	public static void SetEnableDepthWrite(Material material, bool value) {
        SaveShaderProperty(material, eShaderPropertyID.DepthWrite, value ? 1f : 0f);
    }

	public static void SetDepthOffsetFactor(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.DepthOffsetFactor, value);
    }

	public static void SetDepthOffsetUnits(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.DepthOffsetUnits, value);
    }

	public static void SetEnableAlphaTest(Material material, bool value) {
        EnableKeyword(material, m_ShaderKeywordName_AlphaTest, value ? 1 : 0);
        SaveShaderProperty(material, eShaderPropertyID.Using_AlphaTest, value ? 1f : 0f);
    }

	public static void SetCullMode(Material material, eCullMode value) {
        SaveShaderProperty(material, eShaderPropertyID.CullMode, (float)value);
    }

	public static void SetLightingType(Material material, int value) {
        EnableKeyword(material, m_ShaderKeywordName_Lighting, value);
        SaveShaderProperty(material, eShaderPropertyID.Lighting_Type, (float)value);
    }

	public static void SetUsingFog(Material material, bool value) {
        EnableKeyword(material, m_ShaderKeywordName_Fog, value ? 1 : 0);
        SaveShaderProperty(material, eShaderPropertyID.Using_Fog, value ? 1f : 0f);
    }

	public static void SetMeshColor(Material material, Color value) {
        SaveShaderProperty(material, eShaderPropertyID.MeshColor, value);
    }

	public static void SetHDRFactor(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.HDRFactor, value);
    }

	public static void SetAlphaTestRefValue(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.AlphaTest_RefValue, value);
    }

	public static void SetHalfAccuracyColor(Material material, bool value) {
        EnableKeyword(material, m_ShaderKeywordName_HalfAccuracyColor, value ? 1 : 0);
        SaveShaderProperty(material, eShaderPropertyID.UsingHalfAccuracyColor, value ? 1f : 0f);
    }

	public static void SetUsingTexture(Material material, eUsingTexture value, eTextureType textureID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                EnableKeyword(material, m_ShaderKeywordName_UsingTexture_Albedo, (int)value);
                SaveShaderProperty(material, eShaderPropertyID.Using_AlbedoTexture, (float)value);
                break;
            case eTextureType.eTextureType_Normal:
                EnableKeyword(material, m_ShaderKeywordName_UsingTexture_Normal, (int)value);
                SaveShaderProperty(material, eShaderPropertyID.Using_NormalTexture, (float)value);
                break;
            case eTextureType.eTextureType_Specular:
                EnableKeyword(material, m_ShaderKeywordName_UsingTexture_Specular, (int)value);
                SaveShaderProperty(material, eShaderPropertyID.Using_SpecularTexture, (float)value);
                break;
            case eTextureType.eTextureType_Additional_Start:
                EnableKeyword(material, m_ShaderKeywordName_UsingTexture_Additional0, (int)value);
                SaveShaderProperty(material, eShaderPropertyID.Using_Additional0Texture, (float)value);
                break;
        }
    }

	public static void SetTexture(Material material, Texture value, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_Albedo, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_AlbedoLayer, value);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_Normal, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_NormalLayer, value);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_Specular, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_SpecularLayer, value);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_Additional0, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.Texture_Additional0Layer, value);
                }
                break;
        }
    }

	public static void SetMaterialColor(Material material, Color value, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_Albedo, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_AlbedoLayer, value);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_Normal, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_NormalLayer, value);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_Specular, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_SpecularLayer, value);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_Additional0, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.MatColor_Additional0Layer, value);
                }
                break;
        }
    }

	public static void SetUVMatrix(Material material, Matrix4x4 value, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_Albedo, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_AlbedoLayer, value);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_Normal, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_NormalLayer, value);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_Specular, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_SpecularLayer, value);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_Additional0, value);
                } else if (layerID == 1) {
                    SaveShaderProperty(material, eShaderPropertyID.UVMatrix_Additional0Layer, value);
                }
                break;
        }
    }

	public static void SetLayerBlendMode(Material material, eLayerBlendMode colValue, eLayerBlendMode alpValue, eTextureType textureID) {
        eBlendMode colBlend = ConvertLayerBlend(colValue);
        eBlendMode alpBlend = ConvertLayerBlend(alpValue);
        if (textureID == eTextureType.eTextureType_Albedo) {
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_AlbedoColor, (float)colBlend);
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_AlbedoAlpha, (float)alpBlend);
        } else if (textureID == eTextureType.eTextureType_Normal) {
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_NormalColor, (float)colBlend);
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_NormalAlpha, (float)alpBlend);
        } else if (textureID == eTextureType.eTextureType_Specular) {
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_SpecularColor, (float)colBlend);
            SaveShaderProperty(material, eShaderPropertyID.LayerBlendMode_SpecularAlpha, (float)alpBlend);
        }
    }

	public static void SetOutlineType(Material material, eOutlineType value) {
        EnableKeyword(material, m_ShaderKeywordName_OutlineType, (int)value);
        SaveShaderProperty(material, eShaderPropertyID.OutlineType, (float)value);
    }

	public static void SetOutlineColor(Material material, Color value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineColor, value);
    }

	public static void SetOutlineWidth(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineWidth, value);
    }

	public static void SetOutlineDepthOffset(Material material, float value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineDepthOffset, value);
    }

	public static void SetOutlineAlphaBlendMode(Material material, eBlendMode value) {
        SaveOutlineBlendingParam(material, value);
    }

	public static eLayerBlendMode SetLayerBlendMode(Material material, eLayerBlendMode colValue, eBlendMode alpValue, eTextureType textureID) {
        eLayerBlendMode result = eLayerBlendMode.eLayerBlendMode_Std;
        if (textureID == eTextureType.eTextureType_Albedo) {
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[42]));
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[43]));
        } else if (textureID == eTextureType.eTextureType_Normal) {
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[44]));
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[45]));
        } else if (textureID == eTextureType.eTextureType_Specular) {
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[46]));
            result = ConvertAlphaBlend((eBlendMode)material.GetFloat(m_ShaderPropertyName[47]));
        }
        return result;
    }

	public static void SetStencilCompare(Material material, eCompareFunc value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilCompare, (float)value);
    }

	public static void SetStencilID(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilID, (float)value);
    }

	public static void SetStencilOp(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilOp, (float)value);
    }

	public static void SetStencilOpFail(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilOpFail, (float)value);
    }

	public static void SetStencilOpZFail(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilOpZFail, (float)value);
    }

	public static void SetStencilWriteMask(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.StencilWriteMask, (float)value);
    }

	public static void SetStencilReadMask(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.SteancilReadMask, (float)value);
    }

	public static void SetOutlineStencilCompare(Material material, eCompareFunc value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilCompare, (float)value);
    }

	public static void SetOutlineStencilID(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilID, (float)value);
    }

	public static void SetOutlineStencilOp(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilOp, (float)value);
    }

	public static void SetOutlineStencilOpFail(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilOpFail, (float)value);
    }

	public static void SetOutlineStencilOpZFail(Material material, eStencilOp value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilOpZFail, (float)value);
    }

	public static void SetOutlineStencilWriteMask(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.OutlineStencilWriteMask, (float)value);
    }

	public static void SetOutlineStencilReadMask(Material material, byte value) {
        SaveShaderProperty(material, eShaderPropertyID.SteancilReadMask, (float)value);
    }

	public static eBlendMode GetAlphaBlendMode(Material material) {
        return (eBlendMode)material.GetFloat(m_ShaderPropertyName[1]);
    }

	public static eBlendFactor GetAlphaBlendSrcFactor(Material material) {
        return (eBlendFactor)material.GetFloat(m_ShaderPropertyName[2]);
    }

	public static eBlendFactor GetAlphaBlendDstFactor(Material material) {
        return (eBlendFactor)material.GetFloat(m_ShaderPropertyName[3]);
    }

	public static eBlendOp GetAlphaBlendOpColor(Material material) {
        return (eBlendOp)material.GetFloat(m_ShaderPropertyName[4]);
    }

	public static eBlendOp GetAlphaBlendOpAlpha(Material material) {
        return (eBlendOp)material.GetFloat(m_ShaderPropertyName[5]);
    }

	public static eCompareFunc GetDepthTest(Material material) {
        return (eCompareFunc)material.GetFloat(m_ShaderPropertyName[6]);
    }

	public static bool GetEnableDepthWrite(Material material) {
        return material.GetFloat(m_ShaderPropertyName[7]) > 0f;
    }

	public static float GetDepthOffsetFactor(Material material) {
        return material.GetFloat(m_ShaderPropertyName[8]);
    }

	public static float GetDepthOffsetUnits(Material material) {
        return material.GetFloat(m_ShaderPropertyName[9]);
    }

	public static bool GetEnableAlphaTest(Material material) {
        return material.GetFloat(m_ShaderPropertyName[10]) > 0f;
    }

	public static eCullMode GetCullMode(Material material) {
        return (eCullMode)material.GetFloat(m_ShaderPropertyName[11]);
    }

	public static int GetLightingType(Material material) {
        return (int)material.GetFloat(m_ShaderPropertyName[12]);
    }

	public static bool GetUsingFog(Material material) {
        return material.GetFloat(m_ShaderPropertyName[13]) > 0f;
    }

	public static Color GetMeshColor(Material material) {
        return material.GetColor(m_ShaderPropertyName[14]);
    }

	public static float GetHDRFactor(Material material) {
        return material.GetFloat(m_ShaderPropertyName[15]);
    }

	public static float GetAlphaTestRefValue(Material material) {
        return material.GetFloat(m_ShaderPropertyName[16]);
    }

	public static bool GetHalfAccuracyColor(Material material) {
        return material.GetFloat(m_ShaderPropertyName[17]) > 0f;
    }

	public static eUsingTexture GetUsingTexture(Material material, int textureID) {
        return textureID switch {
            0 => (eUsingTexture)material.GetFloat(m_ShaderPropertyName[48]),
            1 => (eUsingTexture)material.GetFloat(m_ShaderPropertyName[49]),
            2 => (eUsingTexture)material.GetFloat(m_ShaderPropertyName[50]),
            3 => (eUsingTexture)material.GetFloat(m_ShaderPropertyName[51]),
            _ => eUsingTexture.None,
        };
    }

	public static Texture GetTexture(Material material, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    return material.GetTexture(m_ShaderPropertyName[18]);
                } else if (layerID == 1) {
                    return material.GetTexture(m_ShaderPropertyName[19]);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    return material.GetTexture(m_ShaderPropertyName[20]);
                } else if (layerID == 1) {
                    return material.GetTexture(m_ShaderPropertyName[21]);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    return material.GetTexture(m_ShaderPropertyName[22]);
                } else if (layerID == 1) {
                    return material.GetTexture(m_ShaderPropertyName[23]);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    return material.GetTexture(m_ShaderPropertyName[24]);
                } else if (layerID == 1) {
                    return material.GetTexture(m_ShaderPropertyName[25]);
                }
                break;
        }
        return null;
    }

	public static Color GetMaterialColor(Material material, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    return material.GetColor(m_ShaderPropertyName[26]);
                } else if (layerID == 1) {
                    return material.GetColor(m_ShaderPropertyName[27]);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    return material.GetColor(m_ShaderPropertyName[28]);
                } else if (layerID == 1) {
                    return material.GetColor(m_ShaderPropertyName[29]);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    return material.GetColor(m_ShaderPropertyName[30]);
                } else if (layerID == 1) {
                    return material.GetColor(m_ShaderPropertyName[31]);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    return material.GetColor(m_ShaderPropertyName[32]);
                } else if (layerID == 1) {
                    return material.GetColor(m_ShaderPropertyName[33]);
                }
                break;
        }
        return Color.white;
    }

	public static Matrix4x4 GetUVMatrix(Material material, Matrix4x4 value, eTextureType textureID, int layerID) {
        switch (textureID) {
            case eTextureType.eTextureType_Albedo:
                if (layerID == 0) {
                    return material.GetMatrix(m_ShaderPropertyName[34]);
                } else if (layerID == 1) {
                    return material.GetMatrix(m_ShaderPropertyName[35]);
                }
                break;
            case eTextureType.eTextureType_Normal:
                if (layerID == 0) {
                    return material.GetMatrix(m_ShaderPropertyName[36]);
                } else if (layerID == 1) {
                    return material.GetMatrix(m_ShaderPropertyName[37]);
                }
                break;
            case eTextureType.eTextureType_Specular:
                if (layerID == 0) {
                    return material.GetMatrix(m_ShaderPropertyName[38]);
                } else if (layerID == 1) {
                    return material.GetMatrix(m_ShaderPropertyName[39]);
                }
                break;
            case eTextureType.eTextureType_Additional_Start:
                if (layerID == 0) {
                    return material.GetMatrix(m_ShaderPropertyName[40]);
                } else if (layerID == 1) {
                    return material.GetMatrix(m_ShaderPropertyName[41]);
                }
                break;
        }
        return Matrix4x4.identity;
    }

	public static eCompareFunc GetStencilCompare(Material material, eCompareFunc value) {
        return (eCompareFunc)material.GetFloat(m_ShaderPropertyName[61]);
    }

	public static byte GetStencilID(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[62]);
    }

	public static eStencilOp GetStencilOp(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[63]);
    }

	public static eStencilOp GetStencilOpFail(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[64]);
    }

	public static eStencilOp GetStencilOpZFail(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[65]);
    }

	public static byte GetStencilWriteMask(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[66]);
    }

	public static byte GetStencilReadMask(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[67]);
    }

	public static eCompareFunc GetOutlineStencilCompare(Material material, eCompareFunc value) {
        return (eCompareFunc)material.GetFloat(m_ShaderPropertyName[68]);
    }

	public static byte GetOutlineStencilID(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[69]);
    }

	public static eStencilOp GetOutlineStencilOp(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[70]);
    }

	public static eStencilOp GetOutlineStencilOpFail(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[71]);
    }

	public static eStencilOp GetOutlineStencilOpZFail(Material material, eStencilOp value) {
        return (eStencilOp)material.GetFloat(m_ShaderPropertyName[72]);
    }

	public static byte GetOutlineStencilWriteMask(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[73]);
    }

	public static byte GetOutlineStencilReadMask(Material material, byte value) {
        return (byte)material.GetFloat(m_ShaderPropertyName[67]);
    }

    public enum eShaderPropertyID {
		ShadingType,
		BlendMode,
		BlendSrc,
		BlendDst,
		BlendOpColor,
		BlendOpAlpha,
		DepthTest,
		DepthWrite,
		DepthOffsetFactor,
		DepthOffsetUnits,
		Using_AlphaTest,
		CullMode,
		Lighting_Type,
		Using_Fog,
		MeshColor,
		HDRFactor,
		AlphaTest_RefValue,
		UsingHalfAccuracyColor,
		Texture_Albedo,
		Texture_AlbedoLayer,
		Texture_Normal,
		Texture_NormalLayer,
		Texture_Specular,
		Texture_SpecularLayer,
		Texture_Additional0,
		Texture_Additional0Layer,
		MatColor_Albedo,
		MatColor_AlbedoLayer,
		MatColor_Normal,
		MatColor_NormalLayer,
		MatColor_Specular,
		MatColor_SpecularLayer,
		MatColor_Additional0,
		MatColor_Additional0Layer,
		UVMatrix_Albedo,
		UVMatrix_AlbedoLayer,
		UVMatrix_Normal,
		UVMatrix_NormalLayer,
		UVMatrix_Specular,
		UVMatrix_SpecularLayer,
		UVMatrix_Additional0,
		UVMatrix_Additional0Layer,
		LayerBlendMode_AlbedoColor,
		LayerBlendMode_AlbedoAlpha,
		LayerBlendMode_NormalColor,
		LayerBlendMode_NormalAlpha,
		LayerBlendMode_SpecularColor,
		LayerBlendMode_SpecularAlpha,
		Using_AlbedoTexture,
		Using_NormalTexture,
		Using_SpecularTexture,
		Using_Additional0Texture,
		OutlineType,
		OutlineColor,
		OutlineWidth,
		OutlineDepthOffset,
		OutlineBlendMode,
		OutlineBlendSrc,
		OutlineBlendDst,
		OutlineBlendOpColor,
		OutlineBlendOpAlpha,
		StencilCompare,
		StencilID,
		StencilOp,
		StencilOpFail,
		StencilOpZFail,
		StencilWriteMask,
		SteancilReadMask,
		OutlineStencilCompare,
		OutlineStencilID,
		OutlineStencilOp,
		OutlineStencilOpFail,
		OutlineStencilOpZFail,
		OutlineStencilWriteMask,
		OutlineSteancilReadMask
	}

	private struct BlendComponent {
        public eBlendOp m_BlendOp;
        public eBlendFactor m_BlendFactorSrc;
        public eBlendFactor m_BlendFactorDst;

        public BlendComponent(eBlendOp op, eBlendFactor src, eBlendFactor dst) {
            m_BlendOp = op;
            m_BlendFactorSrc = src;
            m_BlendFactorDst = dst;
        }
	}
}
