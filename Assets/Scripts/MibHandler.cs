﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

// Token: 0x020000CE RID: 206
[Token(Token = "0x2000099")]
[StructLayout(3)]
public class MibHandler : MonoBehaviour
{
	// Token: 0x060004D2 RID: 1234 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000478")]
	[Address(RVA = "0x1015FC1B0", Offset = "0x15FC1B0", VA = "0x1015FC1B0")]
	public MibHandler()
	{
	}

	// Token: 0x040002E9 RID: 745
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40001C0")]
	public MibHandler.MibInfo[] m_MibInfoArray;

	// Token: 0x020000CF RID: 207
	[Token(Token = "0x2000D10")]
	[Serializable]
	public class MibInfo
	{
		// Token: 0x060004D3 RID: 1235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D27")]
		[Address(RVA = "0x1015FC1B8", Offset = "0x15FC1B8", VA = "0x1015FC1B8")]
		public MibInfo()
		{
		}

		// Token: 0x040002EA RID: 746
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40054C3")]
		public string m_Name;

		// Token: 0x040002EB RID: 747
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40054C4")]
		public ePixelFormat m_PixelFormat;

		// Token: 0x040002EC RID: 748
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40054C5")]
		public bool m_bRepeatU;

		// Token: 0x040002ED RID: 749
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
		[Token(Token = "0x40054C6")]
		public bool m_bRepeatV;
	}
}
