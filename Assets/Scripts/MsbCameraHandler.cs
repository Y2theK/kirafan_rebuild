﻿using Meige;
using System;
using UnityEngine;

[Serializable]
public class MsbCameraHandler {
    [SerializeField] private bool m_PostProcessEnabled = true;

    public string m_HierarchyName;
    public eCameraProjection m_ProjectionType;
    public MsbCameraParam m_Src;

    private MsbCameraParam m_Work;
    private MsbHandler m_Owner;
    private GameObject m_HierarchyGO;
    private Camera m_Camera;
    private Transform m_CameraTransform;
    private PostProcessRenderer m_PostProcessRenderer;
    private Camera m_PostProcessAttachedCamera;
    private const float m_BloomOffsetBaseSize = 960f;

    private void AttachPostProcessRenderer(Camera cam) {
        if (m_Src == null) { return; }

        bool flags = m_Src.m_bEnableCorrectToneMap
                  || m_Src.m_bEnableCorrectContrast
                  || m_Src.m_bEnableCorrectBrightness
                  || m_Src.m_bEnableCorrectChroma
                  || m_Src.m_bEnableCorrectColorBlend
                  || m_Src.m_bEnableBloom;
        if (!flags) { return; }

        if (m_PostProcessAttachedCamera != cam) {
            m_PostProcessAttachedCamera = cam;
            if (m_PostProcessRenderer != null) {
                m_PostProcessRenderer.enabled = false;
            }
            if (m_PostProcessAttachedCamera != null) {
                if (!m_PostProcessAttachedCamera.gameObject.TryGetComponent(out m_PostProcessRenderer)) {
                    m_PostProcessRenderer = m_PostProcessAttachedCamera.gameObject.AddComponent<PostProcessRenderer>();
                }
                m_PostProcessAttachedCamera.allowHDR = true;
            }
        }
    }

    public void UpdatePostProcessParameter(bool enabled) {
        if (m_PostProcessRenderer == null || m_Camera == null) { return; }

        m_PostProcessRenderer.enableCorrectToneCurve = m_Work.m_bEnableCorrectToneMap;
        m_PostProcessRenderer.enableCorrectContrast = m_Work.m_bEnableCorrectContrast;
        m_PostProcessRenderer.enableCorrectBrightness = m_Work.m_bEnableCorrectBrightness;
        m_PostProcessRenderer.enableCorrectChroma = m_Work.m_bEnableCorrectChroma;
        m_PostProcessRenderer.enableCorrectColorBlend = m_Work.m_bEnableCorrectColorBlend;
        m_PostProcessRenderer.enableBloom = m_Work.m_bEnableBloom;
        m_PostProcessRenderer.toneMapWhitePoint = eToneMapWhitePoint.eToneMapWhitePoint_Fix;
        m_PostProcessRenderer.toneMapWhitePointScale = m_Work.m_ToneMapWhitePointScale;
        m_PostProcessRenderer.toneMapWhitePointRange.SetValue(m_Work.m_ToneMapWhitePointRangeMin, m_Work.m_ToneMapWhitePointRangeMax);
        m_PostProcessRenderer.exposureCorrectionValue = m_Work.m_ExposureValue;
        m_PostProcessRenderer.adaptationRateSpeed = m_Work.m_AdaptationRateSpeed;
        m_PostProcessRenderer.bloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;
        m_PostProcessRenderer.bloomBrightThresholdOffset = m_Work.m_BloomReferenceThresholdOffset;
        m_PostProcessRenderer.bloomOffset = m_Work.m_BloomOffset * m_Camera.pixelWidth / m_BloomOffsetBaseSize;
        m_PostProcessRenderer.correctContrastLevel = m_Work.m_ContrastLevel;
        m_PostProcessRenderer.correctBrightnessLevel = m_Work.m_BrightnessLevel;
        m_PostProcessRenderer.correctChromaLevel = m_Work.m_ChromaLevel;
        m_PostProcessRenderer.correctBlendLevel = m_Work.m_ColorBlendLevel;
        m_PostProcessRenderer.correctBlendColor = m_Work.m_ColorBlendColor;
        bool flags = m_Work.m_bEnableCorrectToneMap
                  || m_Work.m_bEnableCorrectContrast
                  || m_Work.m_bEnableCorrectBrightness
                  || m_Work.m_bEnableCorrectChroma
                  || m_Work.m_bEnableCorrectColorBlend
                  || m_Work.m_bEnableBloom;
        m_PostProcessRenderer.enabled = flags && enabled && m_PostProcessEnabled;
    }

    public void SetCamera(Camera cam) {
        m_Camera = cam;
        m_CameraTransform = m_Camera != null ? m_Camera.transform : null;
        AttachPostProcessRenderer(m_Camera);
    }

    public MsbCameraParam GetWork() {
        return m_Work;
    }

    public void Init(MsbHandler owner) {
        m_Owner = owner;
        m_Camera = null;
        m_HierarchyGO = null;
        m_Work = null;
        if (m_HierarchyName != null) {
            foreach (Transform transform in m_Owner.GetComponentsInChildren<Transform>()) {
                if (transform.gameObject.name.Equals(m_HierarchyName)) {
                    m_HierarchyGO = transform.gameObject;
                    break;
                }
            }
            m_Work = new MsbCameraParam(m_Src);
        }
    }

    public void UpdateParam() {
        if (m_HierarchyGO == null || m_Camera == null) { return; }

        UpdatePostProcessParameter(true);
        m_Camera.orthographic = m_ProjectionType == eCameraProjection.eCameraProjectionOrthographic;
        m_CameraTransform.position = m_HierarchyGO.transform.position;
        if (m_Camera.orthographic) {
            m_CameraTransform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
            m_Camera.orthographicSize = m_Work.m_OrthographicsSize / 354f;
        } else {
            m_CameraTransform.rotation = m_HierarchyGO.transform.rotation * Quaternion.AngleAxis(-90f, Vector3.up);
            m_Camera.fieldOfView = CalcHorizontalFov(m_Work.m_FocalLength, m_Work.m_ApartureWidth);
        }
    }

    private float CalcVerticalFov(float focalLength, float apartureHeight) {
        return 180f * Mathf.Atan(apartureHeight * 0.5f / focalLength) / 3.141593f;
    }

    private float CalcHorizontalFov(float focalLength, float apartureWidth) {
        return 180f * Mathf.Atan(apartureWidth * 0.5f / (focalLength * 0.81f)) / 3.141593f;
    }

    [Serializable]
    public class MsbCameraParam {
        public float m_FocalLength;
        public float m_ApartureWidth;
        public float m_ApartureHeight;
        public float m_Znear;
        public float m_Zfar;
        public float m_OrthographicsSize;
        public float m_ExposureValue;
        public float m_ToneMapWhitePointScale;
        public float m_ToneMapWhitePointRangeMin;
        public float m_ToneMapWhitePointRangeMax;
        public float m_BloomReferenceThresholdOffset;
        public float m_BloomOffset;
        public float m_ContrastLevel;
        public float m_BrightnessLevel;
        public float m_ChromaLevel;
        public float m_ColorBlendLevel;
        public Color m_ColorBlendColor;
        public float m_AdaptationRateSpeed;
        public bool m_bEnableCorrectToneMap;
        public bool m_bEnableBloom;
        public bool m_bEnableCorrectContrast;
        public bool m_bEnableCorrectBrightness;
        public bool m_bEnableCorrectChroma;
        public bool m_bEnableCorrectColorBlend;
        public eToneMapWhitePoint m_ToneMapWhitePointType;
        public eBloomReferenceType m_BloomReferenceType;

        public MsbCameraParam() {
            m_FocalLength = 0f;
            m_ApartureWidth = 0f;
            m_ApartureHeight = 0f;
            m_Znear = 0f;
            m_Zfar = 0f;
            m_ExposureValue = 1f;
            m_ToneMapWhitePointScale = 1f;
            m_ToneMapWhitePointRangeMin = 0.2f;
            m_ToneMapWhitePointRangeMax = 5f;
            m_BloomReferenceThresholdOffset = 1f;
            m_BloomOffset = 5f;
            m_ContrastLevel = 0f;
            m_BrightnessLevel = 0f;
            m_ChromaLevel = 0f;
            m_ColorBlendLevel = 0f;
            m_ColorBlendColor = Color.white;
            m_AdaptationRateSpeed = 0.2f;
            m_bEnableCorrectToneMap = false;
            m_bEnableBloom = false;
            m_bEnableCorrectContrast = false;
            m_bEnableCorrectBrightness = false;
            m_bEnableCorrectChroma = false;
            m_bEnableCorrectColorBlend = false;
            m_ToneMapWhitePointType = eToneMapWhitePoint.eToneMapWhitePoint_Fix;
            m_BloomReferenceType = eBloomReferenceType.eBloomReferenceType_Fix;
        }

        public MsbCameraParam(MsbCameraParam src) {
            m_FocalLength = src.m_FocalLength;
            m_ApartureWidth = src.m_ApartureWidth;
            m_ApartureHeight = src.m_ApartureHeight;
            m_Znear = src.m_Znear;
            m_Zfar = src.m_Zfar;
            m_OrthographicsSize = src.m_OrthographicsSize;
            m_ExposureValue = src.m_ExposureValue;
            m_ToneMapWhitePointScale = src.m_ToneMapWhitePointScale;
            m_ToneMapWhitePointRangeMin = src.m_ToneMapWhitePointRangeMin;
            m_ToneMapWhitePointRangeMax = src.m_ToneMapWhitePointRangeMax;
            m_BloomReferenceThresholdOffset = src.m_BloomReferenceThresholdOffset;
            m_BloomOffset = src.m_BloomOffset;
            m_ContrastLevel = src.m_ContrastLevel;
            m_BrightnessLevel = src.m_BrightnessLevel;
            m_ChromaLevel = src.m_ChromaLevel;
            m_ColorBlendLevel = src.m_ColorBlendLevel;
            m_ColorBlendColor = src.m_ColorBlendColor;
            m_AdaptationRateSpeed = src.m_AdaptationRateSpeed;
            m_bEnableCorrectToneMap = src.m_bEnableCorrectToneMap;
            m_bEnableBloom = src.m_bEnableBloom;
            m_bEnableCorrectContrast = src.m_bEnableCorrectContrast;
            m_bEnableCorrectBrightness = src.m_bEnableCorrectBrightness;
            m_bEnableCorrectChroma = src.m_bEnableCorrectChroma;
            m_bEnableCorrectColorBlend = src.m_bEnableCorrectColorBlend;
            m_ToneMapWhitePointType = src.m_ToneMapWhitePointType;
            m_BloomReferenceType = src.m_BloomReferenceType;
        }
    }
}
