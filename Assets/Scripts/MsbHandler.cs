﻿using Meige;
using System;
using System.Collections.Generic;
using UnityEngine;

public class MsbHandler : MonoBehaviour {
    private Dictionary<string, MsbObjectHandler> m_DicMsbObj;
    private Dictionary<string, MsbMaterialHandler> m_DicMsbMat;
    private Dictionary<string, MsbHierarchyNode> m_DicMsbHie;
    private Transform m_Transform;

    protected Renderer[] m_rdArray;

    public MsbObjectHandler[] m_MsbObjectHandlerArray;
    public MsbMaterialHandler[] m_MsbMaterialHandlerArray;
    public MeigeParticleEmitter[] m_MeigeParticleEmitterArray;
    public MsbParticleEmitter[] m_MsbParticleEmitterArray;
    public MsbCameraHandler[] m_MsbCameraHandlerArray;
    public MsbArticulatedDynamicsHandler[] m_MsbADHandlerArray;
    public Animation m_AnimationTarget;
    public MeigeAnimEventNotifier m_AnimEvNotifier;

    public Transform GetTransform() {
        return m_Transform;
    }

    public Renderer[] GetRendererCache() {
        return m_rdArray;
    }

    public int GetMsbObjectHandlerNum() {
        return m_MsbObjectHandlerArray != null ? m_MsbObjectHandlerArray.Length : 0;
    }

    public int GetMsbMaterialHandlerNum() {
        return m_MsbMaterialHandlerArray != null ? m_MsbMaterialHandlerArray.Length : 0;
    }

    public int GetParticleEmitterNum() {
        if (m_MeigeParticleEmitterArray != null && m_MeigeParticleEmitterArray.Length > 0) {
            return m_MeigeParticleEmitterArray.Length;
        }
        return m_MsbParticleEmitterArray != null ? m_MsbParticleEmitterArray.Length : 0;
    }

    public int GetMsbCameraHandlerNum() {
        return m_MsbCameraHandlerArray != null ? m_MsbCameraHandlerArray.Length : 0;
    }

    public MsbObjectHandler GetMsbObjectHandler(int i) {
        Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
        return m_MsbObjectHandlerArray[i];
    }

    public MsbMaterialHandler GetMsbMaterialHandler(int i) {
        Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
        return m_MsbMaterialHandlerArray[i];
    }

    public MeigeParticleEmitter GetParticleEmitter(int i) {
        Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
        if (m_MeigeParticleEmitterArray != null && m_MeigeParticleEmitterArray.Length > 0) {
            return m_MeigeParticleEmitterArray[i];
        }
        if (m_MsbParticleEmitterArray != null && m_MsbParticleEmitterArray.Length > 0) {
            return m_MsbParticleEmitterArray[i].m_ParticleEmitter;
        }
        return null;
    }

    public MsbCameraHandler GetMsbCameraHandler(int i) {
        Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
        return m_MsbCameraHandlerArray[i];
    }

    public Animation GetAnimationTarget() {
        return m_AnimationTarget;
    }

    public MeigeAnimEventNotifier GetAnimEventNotifier() {
        return m_AnimEvNotifier;
    }

    public MsbObjectHandler GetMsbObjectHandlerByName(string name) {
        m_DicMsbObj.TryGetValue(name, out MsbObjectHandler result);
        return result;
    }

    public MsbMaterialHandler GetMsbMaterialHandlerByName(string name) {
        m_DicMsbMat.TryGetValue(name, out MsbMaterialHandler result);
        return result;
    }

    public MsbHierarchyNode GetMsbHierarchyNodeByName(string name) {
        m_DicMsbHie.TryGetValue(name, out MsbHierarchyNode result);
        return result;
    }

    public void AttachCamera(int idx, Camera cam) {
        if (m_MsbCameraHandlerArray.Length > idx) {
            m_MsbCameraHandlerArray[idx].SetCamera(cam);
        }
    }

    public void DetachCamera() {
        for (int i = 0; i < m_MsbCameraHandlerArray.Length; i++) {
            DetachCamera(i);
        }
    }

    public void DetachCamera(int idx) {
        m_MsbCameraHandlerArray[idx].SetCamera(null);
    }

    public void SetDefaultVisibility() {
        foreach (MsbObjectHandler objectHandler in m_MsbObjectHandlerArray) {
            objectHandler.GetWork().m_bVisibility = objectHandler.m_Src.m_bVisibility;
        }
    }

    private void OnDisable() {
        if (m_MsbCameraHandlerArray != null) {
            foreach (MsbCameraHandler cameraHandler in m_MsbCameraHandlerArray) {
                cameraHandler.UpdatePostProcessParameter(false);
            }
        }
    }

    private void Awake() {
        Init();
    }

    public void Init() {
        m_Transform = transform;
        m_DicMsbHie = new Dictionary<string, MsbHierarchyNode>();
        foreach (MsbHierarchyNode hierarchyNode in gameObject.GetComponentsInChildren<MsbHierarchyNode>()) {
            m_DicMsbHie.Add(hierarchyNode.name, hierarchyNode);
            hierarchyNode.Init();
        }
        m_rdArray = gameObject.GetComponentsInChildren<Renderer>();
        m_DicMsbMat = new Dictionary<string, MsbMaterialHandler>();
        foreach (MsbMaterialHandler materialHandler in m_MsbMaterialHandlerArray) {
            materialHandler.Init(this);
            m_DicMsbMat.Add(materialHandler.m_Name, materialHandler);
        }
        m_DicMsbObj = new Dictionary<string, MsbObjectHandler>();
        foreach (MsbObjectHandler objectHandler in m_MsbObjectHandlerArray) {
            objectHandler.Init(this);
            m_DicMsbObj.Add(objectHandler.m_Name, objectHandler);
        }
        foreach (MsbCameraHandler cameraHandler in m_MsbCameraHandlerArray) {
            cameraHandler.Init(this);
        }
        if (m_MsbParticleEmitterArray != null) {
            foreach (MsbParticleEmitter particleEmitter in m_MsbParticleEmitterArray) {
                if (particleEmitter.m_ParticleEmitter != null && particleEmitter.m_ParentRenderer != null) {
                    string value = particleEmitter.m_ParentRenderer.sharedMaterial.name.Replace(" (Instance)", string.Empty);
                    foreach (MsbMaterialHandler materialHandler in m_MsbMaterialHandlerArray) {
                        if (materialHandler.m_Name.Equals(value)) {
                            particleEmitter.m_MsbMaterialHandler = materialHandler;
                            break;
                        }
                    }
                    particleEmitter.m_ParticleEmitter.m_Material = particleEmitter.m_ParentRenderer.material;
                }
            }
        }
        if (m_MsbADHandlerArray != null) {
            foreach (MsbArticulatedDynamicsHandler msbArticulatedDynamicsHandler in m_MsbADHandlerArray) {
                msbArticulatedDynamicsHandler.Init(this);
            }
        }
    }

    private void OnDestroy() {
        foreach (MsbObjectHandler objectHandler in m_MsbObjectHandlerArray) {
            objectHandler.Destroy();
        }
    }

    private void LateUpdate() {
        foreach (MsbMaterialHandler materialHandler in m_MsbMaterialHandlerArray) {
            materialHandler.UpdateParam();
        }
        foreach (MsbObjectHandler objectHandler in m_MsbObjectHandlerArray) {
            objectHandler.UpdateParam();
        }
        foreach (MsbCameraHandler cameraHandler in m_MsbCameraHandlerArray) {
            cameraHandler.UpdateParam();
        }
        if (m_MsbParticleEmitterArray != null) {
            foreach (MsbParticleEmitter particleEmitter in m_MsbParticleEmitterArray) {
                if (particleEmitter.m_ParticleEmitter != null && particleEmitter.m_MsbMaterialHandler != null && particleEmitter.m_MsbMaterialHandler.GetWork() != null) {
                    MeigeShaderUtility.SetMeshColor(particleEmitter.m_ParticleEmitter.m_Material, particleEmitter.m_MsbMaterialHandler.GetWork().m_Diffuse);
                }
            }
        }
    }

    private void FixedUpdate() {
        if (m_MsbADHandlerArray != null) {
            foreach (MsbArticulatedDynamicsHandler articulatedDynamicsHandler in m_MsbADHandlerArray) {
                articulatedDynamicsHandler.CalculateHierarchyMatrix();
                articulatedDynamicsHandler.DoWork();
                articulatedDynamicsHandler.Flush();
            }
        }
    }

    [Serializable]
    public class MsbParticleEmitter {
        public MeigeParticleEmitter m_ParticleEmitter;
        public MsbMaterialHandler m_MsbMaterialHandler;
        public Renderer m_ParentRenderer;
    }
}
