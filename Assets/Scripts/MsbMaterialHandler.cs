﻿using Meige;
using System;
using UnityEngine;

[Serializable]
public class MsbMaterialHandler {
    public string m_Name;
    public int m_RefID;
    public MsbMaterialParam m_Src;

    private MsbMaterialParam m_Work;

    public void Init(MsbHandler owner) {
        m_Work = new MsbMaterialParam(m_Src);
    }

    public void UpdateParam() {
        m_Work.UpdateParam();
    }

    public MsbMaterialParam GetWork() {
        return m_Work;
    }

    [Serializable]
    public class MsbTextureParam {
        public string m_Name;
        public eTextureType m_eType;
        public int m_Layer;
        public Vector2 m_CoverageUV;
        public Vector2 m_TranslationUV;
        public Vector2 m_OffsetUV;
        public float m_RotateUV;
        public eLayerBlendMode m_LayerBlendMode;
        public eLayerBlendMode m_LayerBlendModeAlpha;

        protected Matrix4x4 m_UVMat;

        public MsbTextureParam() { }

        public MsbTextureParam(MsbTextureParam src) {
            m_eType = src.m_eType;
            m_Layer = src.m_Layer;
            m_CoverageUV = src.m_CoverageUV;
            m_TranslationUV = src.m_TranslationUV;
            m_OffsetUV = src.m_OffsetUV;
            m_RotateUV = src.m_RotateUV;
            m_LayerBlendMode = src.m_LayerBlendMode;
            m_LayerBlendModeAlpha = src.m_LayerBlendModeAlpha;
            m_Name = src.m_Name;
        }

        public void UpdateParam() {
            Quaternion q = Quaternion.Euler(0f, 0f, m_RotateUV);
            Matrix4x4 rhs = Matrix4x4.TRS(Vector3.zero, q, Vector3.one);
            float uvx = 1f / m_CoverageUV.x;
            float uvy = 1f / m_CoverageUV.y;
            m_UVMat = Matrix4x4.identity;
            m_UVMat *= Matrix4x4.Scale(new Vector3(uvx, uvy, 1f));
            m_UVMat *= Matrix4x4.TRS(new Vector3(0.5f * uvx, 0.5f * uvy, 0f), Quaternion.identity, Vector3.one);
            m_UVMat *= rhs;
            m_UVMat *= Matrix4x4.TRS(new Vector3(-0.5f * uvx, -0.5f * uvy, 0f), Quaternion.identity, Vector3.one);
            Vector3 offsetVect = new Vector3(m_OffsetUV.x, m_OffsetUV.y);
            offsetVect = rhs.MultiplyPoint3x4(offsetVect);
            Vector3 translateVect = new Vector3(-m_TranslationUV.x, -m_TranslationUV.y);
            translateVect = rhs.MultiplyPoint3x4(translateVect);
            m_UVMat *= Matrix4x4.TRS(offsetVect, Quaternion.identity, Vector3.one);
            m_UVMat *= Matrix4x4.TRS(translateVect, Quaternion.identity, Vector3.one);
        }

        public Matrix4x4 GetMatrix() {
            return m_UVMat;
        }
    }

    [Serializable]
    public class MsbMaterialParam {
        public Color m_Diffuse;
        public eBlendMode m_BlendMode;
        public MsbTextureParam[] m_Texture;
        public eBlendMode m_OutlineBlendMode;
        public float m_AlphaTestRefValue = 0.01f;

        public MsbMaterialParam() { }

        public MsbMaterialParam(MsbMaterialParam src) {
            m_Diffuse = src.m_Diffuse;
            m_BlendMode = src.m_BlendMode;
            m_Texture = new MsbTextureParam[src.m_Texture.Length];
            for (int i = 0; i < src.m_Texture.Length; i++) {
                m_Texture[i] = new MsbTextureParam(src.m_Texture[i]);
            }
        }

        public int SearchTexIndex(eTextureType texType, int layerID) {
            for (int i = 0; i < m_Texture.Length; i++) {
                if (m_Texture[i].m_eType == texType && m_Texture[i].m_Layer == layerID) {
                    return i;
                }
            }
            return -1;
        }

        public void UpdateParam() {
            foreach (MsbTextureParam msbTextureParam in m_Texture) {
                msbTextureParam.UpdateParam();
            }
        }
    }
}
