﻿using Meige;
using System;
using UnityEngine;

[Serializable]
public class MsbObjectHandler {
    public string m_Name;
    public int m_RefID;
    public int m_HieIndex;
    public int m_BoundingReferHieIndex;
    public MsbObjectParam m_Src;

    private MsbObjectParam m_Work;
    private MsbHandler m_Owner;
    private GameObject m_GO;
    private Transform m_Transfrom;
    private Renderer m_Renderer;
    private Material[] m_MatArray;
    private int[] m_MatIndex;

    public Transform transform => m_Transfrom;

    public void Init(MsbHandler owner) {
        m_Owner = owner;
        m_Work = new MsbObjectParam(m_Src);
        m_GO = null;
        m_Transfrom = null;
        foreach (Renderer renderer in m_Owner.GetComponentsInChildren<Renderer>(true)) {
            string text = Helper.GetMeshNameByRenderer(renderer);
            if (text != null) {
                text = text.Replace(" Instance", string.Empty);
                if (text.Equals(m_Name)) {
                    m_GO = renderer.gameObject;
                    m_Transfrom = m_GO.transform;
                    m_Renderer = renderer;
                    break;
                }
            }
        }
        if (m_Renderer != null && m_Renderer.materials != null) {
            m_MatArray = m_Renderer.materials;
            m_MatIndex = new int[m_Renderer.materials.Length];
            for (int i = 0; i < m_MatArray.Length; i++) {
                Material material = m_MatArray[i];
                string value = material.name.Replace(" (Instance)", string.Empty);
                m_MatIndex[i] = -1;
                for (int j = 0; j < m_Owner.GetMsbMaterialHandlerNum(); j++) {
                    MsbMaterialHandler msbMaterialHandler = m_Owner.GetMsbMaterialHandler(j);
                    if (msbMaterialHandler != null && msbMaterialHandler.m_Name.Equals(value)) {
                        m_MatIndex[i] = j;
                        break;
                    }
                }
                bool depthWrite = true;
                bool alphaTest = false;
                if (m_Src.m_eRenderStage >= eRenderStage.eRenderStage_PunchThrough_Higher && m_Src.m_eRenderStage <= eRenderStage.eRenderStage_PunchThrough_Lower) {
                    alphaTest = true;
                } else if (m_Src.m_eRenderStage >= eRenderStage.eRenderStage_Alpha_Higher && m_Src.m_eRenderStage <= eRenderStage.eRenderStage_Alpha_Lower) {
                    depthWrite = false;
                }
                MeigeShaderUtility.SetDepthTest(material, eCompareFunc.Less);
                MeigeShaderUtility.SetEnableDepthWrite(material, depthWrite);
                MeigeShaderUtility.SetEnableAlphaTest(material, alphaTest);
                MeigeShaderUtility.SetOutlineType(material, m_Src.m_OutlineType);
                MeigeShaderUtility.SetOutlineColor(material, m_Src.m_OutlineColor);
                MeigeShaderUtility.SetOutlineWidth(material, m_Src.m_OutlineWidth);
                MeigeShaderUtility.SetOutlineDepthOffset(material, m_Src.m_OutlineDepthOffset * m_Transfrom.lossyScale.x);
            }
        }
        if (m_Work != null) {
            m_Work.m_History.Init();
        }
    }

    public void Destroy() {
        if (m_MatArray != null) {
            foreach (Material material in m_MatArray) {
                if (material && material.name.Contains("(Instance)")) {
                    UnityEngine.Object.DestroyImmediate(material);
                }
            }
            m_MatArray = null;
        }
    }

    public void UpdateParam() {
        if (m_Renderer == null) { return; }

        if (m_Renderer.enabled != m_Work.m_bVisibility) {
            m_Renderer.enabled = m_Work.m_bVisibility;
        }

        if (!m_Work.m_bVisibility) { return; }

        for (int i = 0; i < m_MatArray.Length; i++) {
            if (m_MatIndex[i] == -1) { continue; }

            Material material = m_MatArray[i];
            MsbMaterialHandler.MsbMaterialParam work = m_Owner.m_MsbMaterialHandlerArray[m_MatIndex[i]].GetWork();
            material.renderQueue = (int)MeigeUtility.RenderStageToRenderQueue(m_Work.m_eRenderStage, m_Work.m_RenderOrder);
            Color color = work.m_Diffuse * m_Work.m_MeshColor;
            if (m_Work.m_bHalfVertexColor) {
                color.r *= 2f;
                color.g *= 2f;
                color.b *= 2f;
            }
            if (m_Work.m_History.m_MeshColor != color) {
                MeigeShaderUtility.SetMeshColor(material, color);
                m_Work.m_History.m_MeshColor = color;
            }
            Matrix4x4 matrix0 = work.m_Texture[0].GetMatrix();
            MeigeShaderUtility.SetUVMatrix(material, matrix0, eTextureType.eTextureType_Albedo, 0);
            if (work.m_Texture.Length >= 2) {
                Matrix4x4 matrix1 = work.m_Texture[1].GetMatrix();
                MeigeShaderUtility.SetUVMatrix(material, matrix1, eTextureType.eTextureType_Albedo, 1);
                if (m_Work.m_History.m_LayerBlendMode != work.m_Texture[1].m_LayerBlendMode || m_Work.m_History.m_LayerBlendModeAlpha != work.m_Texture[1].m_LayerBlendModeAlpha) {
                    MeigeShaderUtility.SetLayerBlendMode(material, work.m_Texture[1].m_LayerBlendMode, work.m_Texture[1].m_LayerBlendModeAlpha, eTextureType.eTextureType_Albedo);
                    m_Work.m_History.m_LayerBlendMode = work.m_Texture[1].m_LayerBlendMode;
                    m_Work.m_History.m_LayerBlendModeAlpha = work.m_Texture[1].m_LayerBlendModeAlpha;
                }
            }
            if (m_Work.m_History.m_BlendMode != work.m_BlendMode) {
                MeigeShaderUtility.SetAlphaBlendMode(material, work.m_BlendMode);
                m_Work.m_History.m_BlendMode = work.m_BlendMode;
            }
            if (m_Work.m_History.m_HDRFactor != m_Work.m_HDRFactor) {
                MeigeShaderUtility.SetHDRFactor(material, m_Work.m_HDRFactor);
                m_Work.m_History.m_HDRFactor = m_Work.m_HDRFactor;
            }
            if (m_Work.m_History.m_bFogEnable != m_Work.m_bFogEnable) {
                MeigeShaderUtility.SetUsingFog(material, m_Work.m_bFogEnable);
                m_Work.m_History.m_bFogEnable = m_Work.m_bFogEnable;
            }
            if (m_Work.m_History.m_OutlineType != m_Work.m_OutlineType) {
                MeigeShaderUtility.SetOutlineType(material, m_Work.m_OutlineType);
                m_Work.m_History.m_OutlineType = m_Work.m_OutlineType;
            }
            if (m_Work.m_History.m_OutlineColor != m_Work.m_OutlineColor) {
                MeigeShaderUtility.SetOutlineColor(material, m_Work.m_OutlineColor);
                m_Work.m_History.m_OutlineColor = m_Work.m_OutlineColor;
            }
            if (m_Work.m_History.m_OutlineWidth != m_Work.m_OutlineWidth) {
                MeigeShaderUtility.SetOutlineWidth(material, m_Work.m_OutlineWidth);
                m_Work.m_History.m_OutlineWidth = m_Work.m_OutlineWidth;
            }
            float offset = m_Work.m_OutlineDepthOffset * m_Transfrom.lossyScale.x;
            if (m_Work.m_History.m_OutlineDepthOffset != offset) {
                MeigeShaderUtility.SetOutlineDepthOffset(material, offset);
                m_Work.m_History.m_OutlineDepthOffset = offset;
            }
            if (m_Work.m_History.m_AlphaTestRefValue != work.m_AlphaTestRefValue) {
                MeigeShaderUtility.SetAlphaTestRefValue(material, work.m_AlphaTestRefValue);
                m_Work.m_History.m_AlphaTestRefValue = work.m_AlphaTestRefValue;
            }
        }

        if (m_Work.m_eBillboardType == eMeshBillBoardType.eMeshBillBoardType_None) {
            Camera main = Camera.main;
            if (main != null) {
                switch (m_Work.m_eBillboardType) {
                    case eMeshBillBoardType.eMeshBillBoardType_XYZ:
                        m_Transfrom.LookAt(main.transform);
                        break;
                    case eMeshBillBoardType.eMeshBillBoardType_X: {
                        Vector3 position = main.transform.position;
                        position.x = m_Transfrom.position.x;
                        m_Transfrom.LookAt(position);
                        break;
                    }
                    case eMeshBillBoardType.eMeshBillBoardType_Y: {
                        Vector3 position = main.transform.position;
                        position.y = m_Transfrom.position.y;
                        m_Transfrom.LookAt(position);
                        break;
                    }
                    case eMeshBillBoardType.eMeshBillBoardType_Z: {
                        Vector3 position = main.transform.position;
                        position.z = m_Transfrom.position.z;
                        m_Transfrom.LookAt(position);
                        break;
                    }
                }
            }
        }
    }

    public MsbObjectParam GetWork() {
        return m_Work;
    }

    public Renderer GetRenderer() {
        return m_Renderer;
    }

    [Serializable]
    public class MsbObjectParam {
        public Bounds m_AABB;
        public Color m_MeshColor;
        public float m_HDRFactor = 1f;
        public eRenderStage m_eRenderStage;
        public int m_RenderOrder;
        public eMeshBillBoardType m_eBillboardType;
        public bool m_bVisibility;
        public bool m_bHalfVertexColor;
        public bool m_bFogEnable;
        public float m_OutlineWidth = 0.2f;
        public float m_OutlineDepthOffset = 0.01f;
        public Color m_OutlineColor = Color.black;
        public eOutlineType m_OutlineType;
        public History m_History;

        public MsbObjectParam() { }

        public MsbObjectParam(MsbObjectParam src) {
            m_MeshColor = src.m_MeshColor;
            m_AABB = src.m_AABB;
            m_eRenderStage = src.m_eRenderStage;
            m_RenderOrder = src.m_RenderOrder;
            m_eBillboardType = src.m_eBillboardType;
            m_bVisibility = src.m_bVisibility;
            m_HDRFactor = src.m_HDRFactor;
            m_bHalfVertexColor = src.m_bHalfVertexColor;
            m_bFogEnable = src.m_bFogEnable;
            m_OutlineWidth = src.m_OutlineWidth;
            m_OutlineDepthOffset = src.m_OutlineDepthOffset;
            m_OutlineColor = src.m_OutlineColor;
            m_OutlineType = src.m_OutlineType;
            m_History.m_MeshColor = Color.black;
            m_History.m_HDRFactor = 0f;
            m_History.m_OutlineWidth = -1f;
            m_History.m_OutlineDepthOffset = -1f;
            m_History.m_OutlineColor = Color.white;
            m_History.m_OutlineType = eOutlineType.eOutlineType_Invalid;
            m_History.m_bFogEnable = !src.m_bFogEnable;
            m_History.m_LayerBlendMode = eLayerBlendMode.eLayerBlendMode_Invalid;
            m_History.m_LayerBlendModeAlpha = eLayerBlendMode.eLayerBlendMode_Invalid;
            m_History.m_BlendMode = eBlendMode.eBlendMode_Invalid;
            m_History.m_AlphaTestRefValue = -1f;
        }

        public struct History {
            public Color m_MeshColor;
            public float m_HDRFactor;
            public float m_OutlineWidth;
            public float m_OutlineDepthOffset;
            public Color m_OutlineColor;
            public eOutlineType m_OutlineType;
            public bool m_bFogEnable;
            public eLayerBlendMode m_LayerBlendMode;
            public eLayerBlendMode m_LayerBlendModeAlpha;
            public eBlendMode m_BlendMode;
            public float m_AlphaTestRefValue;

            public void Init() {
                m_MeshColor = Color.white * -1f;
                m_HDRFactor = 1f;
                m_OutlineWidth = -1f;
                m_OutlineDepthOffset = -1f;
                m_OutlineColor = Color.white * -1f;
                m_OutlineType = eOutlineType.eOutlineType_Invalid;
                m_bFogEnable = false;
                m_LayerBlendMode = eLayerBlendMode.eLayerBlendMode_Invalid;
                m_LayerBlendModeAlpha = eLayerBlendMode.eLayerBlendMode_Invalid;
                m_BlendMode = eBlendMode.eBlendMode_Invalid;
                m_AlphaTestRefValue = 0f;
            }
        }
    }
}
