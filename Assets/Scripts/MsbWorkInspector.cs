﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000E5 RID: 229
[Token(Token = "0x20000A8")]
[Serializable]
[StructLayout(3)]
public class MsbWorkInspector : MonoBehaviour
{
	// Token: 0x060005B4 RID: 1460 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600054B")]
	[Address(RVA = "0x101600A4C", Offset = "0x1600A4C", VA = "0x101600A4C")]
	private void Awake()
	{
	}

	// Token: 0x060005B5 RID: 1461 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600054C")]
	[Address(RVA = "0x101600A58", Offset = "0x1600A58", VA = "0x101600A58")]
	public void InitObjectWorkInfoArray(int num)
	{
	}

	// Token: 0x060005B6 RID: 1462 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600054D")]
	[Address(RVA = "0x101600BFC", Offset = "0x1600BFC", VA = "0x101600BFC")]
	public void InitMaterialWorkArray(int num)
	{
	}

	// Token: 0x060005B7 RID: 1463 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600054E")]
	[Address(RVA = "0x101600DA0", Offset = "0x1600DA0", VA = "0x101600DA0")]
	public void InitCameraWorkArray(int num)
	{
	}

	// Token: 0x060005B8 RID: 1464 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600054F")]
	[Address(RVA = "0x101600F44", Offset = "0x1600F44", VA = "0x101600F44")]
	public MsbWorkInspector()
	{
	}

	// Token: 0x0400037A RID: 890
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000203")]
	public MsbWorkInspector.MsbObjectWorkInfo[] m_ObjWorkInfoArray;

	// Token: 0x0400037B RID: 891
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000204")]
	public MsbWorkInspector.MsbMaterialWorkInfo[] m_MatWorkInfoArray;

	// Token: 0x0400037C RID: 892
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000205")]
	public MsbWorkInspector.MsbCameraWorkInfo[] m_CamWorkInfoArray;

	// Token: 0x020000E6 RID: 230
	[Token(Token = "0x2000D17")]
	[Serializable]
	public class MsbObjectWorkInfo
	{
		// Token: 0x060005B9 RID: 1465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D35")]
		[Address(RVA = "0x101600BF4", Offset = "0x1600BF4", VA = "0x101600BF4")]
		public MsbObjectWorkInfo()
		{
		}

		// Token: 0x0400037D RID: 893
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005506")]
		public string m_Name;

		// Token: 0x0400037E RID: 894
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4005507")]
		public MsbObjectHandler m_SrcHndl;

		// Token: 0x0400037F RID: 895
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4005508")]
		public MsbObjectHandler.MsbObjectParam m_Work;
	}

	// Token: 0x020000E7 RID: 231
	[Token(Token = "0x2000D18")]
	[Serializable]
	public class MsbMaterialWorkInfo
	{
		// Token: 0x060005BA RID: 1466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D36")]
		[Address(RVA = "0x101600D98", Offset = "0x1600D98", VA = "0x101600D98")]
		public MsbMaterialWorkInfo()
		{
		}

		// Token: 0x04000380 RID: 896
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005509")]
		public string m_Name;

		// Token: 0x04000381 RID: 897
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400550A")]
		public MsbMaterialHandler m_SrcHndl;

		// Token: 0x04000382 RID: 898
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400550B")]
		public MsbMaterialHandler.MsbMaterialParam m_Work;
	}

	// Token: 0x020000E8 RID: 232
	[Token(Token = "0x2000D19")]
	[Serializable]
	public class MsbCameraWorkInfo
	{
		// Token: 0x060005BB RID: 1467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D37")]
		[Address(RVA = "0x101600F3C", Offset = "0x1600F3C", VA = "0x101600F3C")]
		public MsbCameraWorkInfo()
		{
		}

		// Token: 0x04000383 RID: 899
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400550C")]
		public string m_Name;

		// Token: 0x04000384 RID: 900
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400550D")]
		public MsbCameraHandler m_SrcHndl;

		// Token: 0x04000385 RID: 901
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400550E")]
		public MsbCameraHandler.MsbCameraParam m_Work;
	}
}
