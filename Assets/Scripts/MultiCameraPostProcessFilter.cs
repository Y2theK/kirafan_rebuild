﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

// Token: 0x0200003A RID: 58
[Token(Token = "0x2000022")]
[StructLayout(3)]
public class MultiCameraPostProcessFilter : MonoBehaviour
{
	// Token: 0x17000028 RID: 40
	// (get) Token: 0x060000E7 RID: 231 RVA: 0x00002358 File Offset: 0x00000558
	// (set) Token: 0x060000E6 RID: 230 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001C")]
	public bool enableCorrectToneCurve
	{
		[Token(Token = "0x60000A6")]
		[Address(RVA = "0x101600F54", Offset = "0x1600F54", VA = "0x101600F54")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000A5")]
		[Address(RVA = "0x101600F4C", Offset = "0x1600F4C", VA = "0x101600F4C")]
		set
		{
		}
	}

	// Token: 0x17000029 RID: 41
	// (get) Token: 0x060000E9 RID: 233 RVA: 0x00002370 File Offset: 0x00000570
	// (set) Token: 0x060000E8 RID: 232 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001D")]
	public bool enableCorrectContrast
	{
		[Token(Token = "0x60000A8")]
		[Address(RVA = "0x101600F64", Offset = "0x1600F64", VA = "0x101600F64")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000A7")]
		[Address(RVA = "0x101600F5C", Offset = "0x1600F5C", VA = "0x101600F5C")]
		set
		{
		}
	}

	// Token: 0x1700002A RID: 42
	// (get) Token: 0x060000EB RID: 235 RVA: 0x00002388 File Offset: 0x00000588
	// (set) Token: 0x060000EA RID: 234 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001E")]
	public bool enableCorrectBrightness
	{
		[Token(Token = "0x60000AA")]
		[Address(RVA = "0x101600F74", Offset = "0x1600F74", VA = "0x101600F74")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000A9")]
		[Address(RVA = "0x101600F6C", Offset = "0x1600F6C", VA = "0x101600F6C")]
		set
		{
		}
	}

	// Token: 0x1700002B RID: 43
	// (get) Token: 0x060000ED RID: 237 RVA: 0x000023A0 File Offset: 0x000005A0
	// (set) Token: 0x060000EC RID: 236 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700001F")]
	public bool enableCorrectChroma
	{
		[Token(Token = "0x60000AC")]
		[Address(RVA = "0x101600F84", Offset = "0x1600F84", VA = "0x101600F84")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000AB")]
		[Address(RVA = "0x101600F7C", Offset = "0x1600F7C", VA = "0x101600F7C")]
		set
		{
		}
	}

	// Token: 0x1700002C RID: 44
	// (get) Token: 0x060000EF RID: 239 RVA: 0x000023B8 File Offset: 0x000005B8
	// (set) Token: 0x060000EE RID: 238 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000020")]
	public bool enableCorrectColorBlend
	{
		[Token(Token = "0x60000AE")]
		[Address(RVA = "0x101600F94", Offset = "0x1600F94", VA = "0x101600F94")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000AD")]
		[Address(RVA = "0x101600F8C", Offset = "0x1600F8C", VA = "0x101600F8C")]
		set
		{
		}
	}

	// Token: 0x1700002D RID: 45
	// (get) Token: 0x060000F1 RID: 241 RVA: 0x000023D0 File Offset: 0x000005D0
	// (set) Token: 0x060000F0 RID: 240 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000021")]
	public bool enableBloom
	{
		[Token(Token = "0x60000B0")]
		[Address(RVA = "0x101600FA4", Offset = "0x1600FA4", VA = "0x101600FA4")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x60000AF")]
		[Address(RVA = "0x101600F9C", Offset = "0x1600F9C", VA = "0x101600F9C")]
		set
		{
		}
	}

	// Token: 0x1700002E RID: 46
	// (get) Token: 0x060000F3 RID: 243 RVA: 0x000023E8 File Offset: 0x000005E8
	// (set) Token: 0x060000F2 RID: 242 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000022")]
	public MultiCameraPostProcessFilter.eToneMapProc toneMapProc
	{
		[Token(Token = "0x60000B2")]
		[Address(RVA = "0x101600FB4", Offset = "0x1600FB4", VA = "0x101600FB4")]
		get
		{
			return MultiCameraPostProcessFilter.eToneMapProc.eToneMapProc_Comp;
		}
		[Token(Token = "0x60000B1")]
		[Address(RVA = "0x101600FAC", Offset = "0x1600FAC", VA = "0x101600FAC")]
		set
		{
		}
	}

	// Token: 0x1700002F RID: 47
	// (get) Token: 0x060000F5 RID: 245 RVA: 0x00002400 File Offset: 0x00000600
	// (set) Token: 0x060000F4 RID: 244 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000023")]
	public MultiCameraPostProcessFilter.eToneMapWhitePoint toneMapWhitePoint
	{
		[Token(Token = "0x60000B4")]
		[Address(RVA = "0x101600FC4", Offset = "0x1600FC4", VA = "0x101600FC4")]
		get
		{
			return MultiCameraPostProcessFilter.eToneMapWhitePoint.eToneMapWhitePoint_ToneLuminanceValue;
		}
		[Token(Token = "0x60000B3")]
		[Address(RVA = "0x101600FBC", Offset = "0x1600FBC", VA = "0x101600FBC")]
		set
		{
		}
	}

	// Token: 0x17000030 RID: 48
	// (get) Token: 0x060000F7 RID: 247 RVA: 0x00002418 File Offset: 0x00000618
	// (set) Token: 0x060000F6 RID: 246 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000024")]
	public float toneMapWhitePointScale
	{
		[Token(Token = "0x60000B6")]
		[Address(RVA = "0x101600FD4", Offset = "0x1600FD4", VA = "0x101600FD4")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000B5")]
		[Address(RVA = "0x101600FCC", Offset = "0x1600FCC", VA = "0x101600FCC")]
		set
		{
		}
	}

	// Token: 0x17000031 RID: 49
	// (get) Token: 0x060000F9 RID: 249 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x060000F8 RID: 248 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000025")]
	public RangePareFloat toneMapWhitePointRange
	{
		[Token(Token = "0x60000B8")]
		[Address(RVA = "0x101600FE4", Offset = "0x1600FE4", VA = "0x101600FE4")]
		get
		{
			return null;
		}
		[Token(Token = "0x60000B7")]
		[Address(RVA = "0x101600FDC", Offset = "0x1600FDC", VA = "0x101600FDC")]
		set
		{
		}
	}

	// Token: 0x17000032 RID: 50
	// (get) Token: 0x060000FB RID: 251 RVA: 0x00002430 File Offset: 0x00000630
	// (set) Token: 0x060000FA RID: 250 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000026")]
	public float exposureCorrectionValue
	{
		[Token(Token = "0x60000BA")]
		[Address(RVA = "0x101600FF4", Offset = "0x1600FF4", VA = "0x101600FF4")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000B9")]
		[Address(RVA = "0x101600FEC", Offset = "0x1600FEC", VA = "0x101600FEC")]
		set
		{
		}
	}

	// Token: 0x17000033 RID: 51
	// (get) Token: 0x060000FD RID: 253 RVA: 0x00002448 File Offset: 0x00000648
	// (set) Token: 0x060000FC RID: 252 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000027")]
	public float adaptationRateSpeed
	{
		[Token(Token = "0x60000BC")]
		[Address(RVA = "0x101601004", Offset = "0x1601004", VA = "0x101601004")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000BB")]
		[Address(RVA = "0x101600FFC", Offset = "0x1600FFC", VA = "0x101600FFC")]
		set
		{
		}
	}

	// Token: 0x17000034 RID: 52
	// (get) Token: 0x060000FF RID: 255 RVA: 0x00002460 File Offset: 0x00000660
	// (set) Token: 0x060000FE RID: 254 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000028")]
	public MultiCameraPostProcessFilter.eBloomReferenceType bloomReferenceType
	{
		[Token(Token = "0x60000BE")]
		[Address(RVA = "0x101601014", Offset = "0x1601014", VA = "0x101601014")]
		get
		{
			return MultiCameraPostProcessFilter.eBloomReferenceType.eBloomReferenceType_ToneLuminanceValue;
		}
		[Token(Token = "0x60000BD")]
		[Address(RVA = "0x10160100C", Offset = "0x160100C", VA = "0x10160100C")]
		set
		{
		}
	}

	// Token: 0x17000035 RID: 53
	// (get) Token: 0x06000101 RID: 257 RVA: 0x00002478 File Offset: 0x00000678
	// (set) Token: 0x06000100 RID: 256 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000029")]
	public float bloomBrightThresholdOffset
	{
		[Token(Token = "0x60000C0")]
		[Address(RVA = "0x101601024", Offset = "0x1601024", VA = "0x101601024")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000BF")]
		[Address(RVA = "0x10160101C", Offset = "0x160101C", VA = "0x10160101C")]
		set
		{
		}
	}

	// Token: 0x17000036 RID: 54
	// (get) Token: 0x06000103 RID: 259 RVA: 0x00002490 File Offset: 0x00000690
	// (set) Token: 0x06000102 RID: 258 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002A")]
	public float bloomOffset
	{
		[Token(Token = "0x60000C2")]
		[Address(RVA = "0x101601034", Offset = "0x1601034", VA = "0x101601034")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000C1")]
		[Address(RVA = "0x10160102C", Offset = "0x160102C", VA = "0x10160102C")]
		set
		{
		}
	}

	// Token: 0x17000037 RID: 55
	// (get) Token: 0x06000105 RID: 261 RVA: 0x000024A8 File Offset: 0x000006A8
	// (set) Token: 0x06000104 RID: 260 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002B")]
	public MultiCameraPostProcessFilter.eGaussFilterType gaussFilterType
	{
		[Token(Token = "0x60000C4")]
		[Address(RVA = "0x101601044", Offset = "0x1601044", VA = "0x101601044")]
		get
		{
			return MultiCameraPostProcessFilter.eGaussFilterType.eGaussFilterType_None;
		}
		[Token(Token = "0x60000C3")]
		[Address(RVA = "0x10160103C", Offset = "0x160103C", VA = "0x10160103C")]
		set
		{
		}
	}

	// Token: 0x17000038 RID: 56
	// (get) Token: 0x06000107 RID: 263 RVA: 0x000024C0 File Offset: 0x000006C0
	// (set) Token: 0x06000106 RID: 262 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002C")]
	public int blurIterationNum
	{
		[Token(Token = "0x60000C6")]
		[Address(RVA = "0x101601054", Offset = "0x1601054", VA = "0x101601054")]
		get
		{
			return 0;
		}
		[Token(Token = "0x60000C5")]
		[Address(RVA = "0x10160104C", Offset = "0x160104C", VA = "0x10160104C")]
		set
		{
		}
	}

	// Token: 0x17000039 RID: 57
	// (get) Token: 0x06000109 RID: 265 RVA: 0x000024D8 File Offset: 0x000006D8
	// (set) Token: 0x06000108 RID: 264 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002D")]
	public float bloomIntensity
	{
		[Token(Token = "0x60000C8")]
		[Address(RVA = "0x101601064", Offset = "0x1601064", VA = "0x101601064")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000C7")]
		[Address(RVA = "0x10160105C", Offset = "0x160105C", VA = "0x10160105C")]
		set
		{
		}
	}

	// Token: 0x1700003A RID: 58
	// (get) Token: 0x0600010B RID: 267 RVA: 0x000024F0 File Offset: 0x000006F0
	// (set) Token: 0x0600010A RID: 266 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002E")]
	public float correctContrastLevel
	{
		[Token(Token = "0x60000CA")]
		[Address(RVA = "0x101601074", Offset = "0x1601074", VA = "0x101601074")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000C9")]
		[Address(RVA = "0x10160106C", Offset = "0x160106C", VA = "0x10160106C")]
		set
		{
		}
	}

	// Token: 0x1700003B RID: 59
	// (get) Token: 0x0600010D RID: 269 RVA: 0x00002508 File Offset: 0x00000708
	// (set) Token: 0x0600010C RID: 268 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700002F")]
	public float correctBrightnessLevel
	{
		[Token(Token = "0x60000CC")]
		[Address(RVA = "0x101601084", Offset = "0x1601084", VA = "0x101601084")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000CB")]
		[Address(RVA = "0x10160107C", Offset = "0x160107C", VA = "0x10160107C")]
		set
		{
		}
	}

	// Token: 0x1700003C RID: 60
	// (get) Token: 0x0600010F RID: 271 RVA: 0x00002520 File Offset: 0x00000720
	// (set) Token: 0x0600010E RID: 270 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000030")]
	public float correctChromaLevel
	{
		[Token(Token = "0x60000CE")]
		[Address(RVA = "0x101601094", Offset = "0x1601094", VA = "0x101601094")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000CD")]
		[Address(RVA = "0x10160108C", Offset = "0x160108C", VA = "0x10160108C")]
		set
		{
		}
	}

	// Token: 0x1700003D RID: 61
	// (get) Token: 0x06000111 RID: 273 RVA: 0x00002538 File Offset: 0x00000738
	// (set) Token: 0x06000110 RID: 272 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000031")]
	public float correctBlendLevel
	{
		[Token(Token = "0x60000D0")]
		[Address(RVA = "0x1016010A4", Offset = "0x16010A4", VA = "0x1016010A4")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x60000CF")]
		[Address(RVA = "0x10160109C", Offset = "0x160109C", VA = "0x10160109C")]
		set
		{
		}
	}

	// Token: 0x1700003E RID: 62
	// (get) Token: 0x06000113 RID: 275 RVA: 0x00002550 File Offset: 0x00000750
	// (set) Token: 0x06000112 RID: 274 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000032")]
	public Color correctBlendColor
	{
		[Token(Token = "0x60000D2")]
		[Address(RVA = "0x1016010C0", Offset = "0x16010C0", VA = "0x1016010C0")]
		get
		{
			return default(Color);
		}
		[Token(Token = "0x60000D1")]
		[Address(RVA = "0x1016010AC", Offset = "0x16010AC", VA = "0x1016010AC")]
		set
		{
		}
	}

	// Token: 0x06000114 RID: 276 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000D3")]
	[Address(RVA = "0x1016010CC", Offset = "0x16010CC", VA = "0x1016010CC")]
	private void Start()
	{
	}

	// Token: 0x06000115 RID: 277 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000D4")]
	[Address(RVA = "0x1016012F8", Offset = "0x16012F8", VA = "0x1016012F8")]
	private void CalcLuminunceValue(RenderTexture src)
	{
	}

	// Token: 0x06000116 RID: 278 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000D5")]
	[Address(RVA = "0x101601810", Offset = "0x1601810", VA = "0x101601810")]
	private void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
	}

	// Token: 0x06000117 RID: 279 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000D6")]
	[Address(RVA = "0x1016029E4", Offset = "0x16029E4", VA = "0x1016029E4")]
	public MultiCameraPostProcessFilter()
	{
	}

	// Token: 0x04000129 RID: 297
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40000D5")]
	[SerializeField]
	private bool m_enableCorrectToneCurve;

	// Token: 0x0400012A RID: 298
	[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
	[Token(Token = "0x40000D6")]
	[SerializeField]
	private bool m_enableCorrectContrast;

	// Token: 0x0400012B RID: 299
	[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
	[Token(Token = "0x40000D7")]
	[SerializeField]
	private bool m_enableCorrectBrightness;

	// Token: 0x0400012C RID: 300
	[Cpp2IlInjected.FieldOffset(Offset = "0x1B")]
	[Token(Token = "0x40000D8")]
	[SerializeField]
	private bool m_enableCorrectChroma;

	// Token: 0x0400012D RID: 301
	[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
	[Token(Token = "0x40000D9")]
	[SerializeField]
	private bool m_enableCorrectColorBlend;

	// Token: 0x0400012E RID: 302
	[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
	[Token(Token = "0x40000DA")]
	[SerializeField]
	private bool m_enableBloom;

	// Token: 0x0400012F RID: 303
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x40000DB")]
	[SerializeField]
	private MultiCameraPostProcessFilter.eToneMapProc m_ToneMapProc;

	// Token: 0x04000130 RID: 304
	[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
	[Token(Token = "0x40000DC")]
	[SerializeField]
	private MultiCameraPostProcessFilter.eToneMapWhitePoint m_ToneMapWhitePoint;

	// Token: 0x04000131 RID: 305
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x40000DD")]
	[SerializeField]
	private float m_ToneMapWhitePointScale;

	// Token: 0x04000132 RID: 306
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x40000DE")]
	[SerializeField]
	private RangePareFloat m_ToneMapWhitePointRange;

	// Token: 0x04000133 RID: 307
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x40000DF")]
	[SerializeField]
	private float m_ExposureCorrectionValue;

	// Token: 0x04000134 RID: 308
	[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
	[Token(Token = "0x40000E0")]
	[SerializeField]
	private float m_AdaptationRateSpeed;

	// Token: 0x04000135 RID: 309
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x40000E1")]
	[SerializeField]
	private MultiCameraPostProcessFilter.eBloomReferenceType m_BloomReferenceType;

	// Token: 0x04000136 RID: 310
	[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
	[Token(Token = "0x40000E2")]
	[SerializeField]
	private float m_BloomBrightThresholdOffset;

	// Token: 0x04000137 RID: 311
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x40000E3")]
	[SerializeField]
	private float m_BloomOffset;

	// Token: 0x04000138 RID: 312
	[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
	[Token(Token = "0x40000E4")]
	[SerializeField]
	private MultiCameraPostProcessFilter.eGaussFilterType m_GaussFilterType;

	// Token: 0x04000139 RID: 313
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x40000E5")]
	[SerializeField]
	[Attribute(Name = "RangeAttribute", RVA = "0x10011E4A0", Offset = "0x11E4A0")]
	private int m_BlurIterationNum;

	// Token: 0x0400013A RID: 314
	[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
	[Token(Token = "0x40000E6")]
	[SerializeField]
	private float m_BloomIntensity;

	// Token: 0x0400013B RID: 315
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x40000E7")]
	[SerializeField]
	private float m_CorrectContrastLevel;

	// Token: 0x0400013C RID: 316
	[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
	[Token(Token = "0x40000E8")]
	[SerializeField]
	private float m_CorrectBrightnessLevel;

	// Token: 0x0400013D RID: 317
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x40000E9")]
	[SerializeField]
	private float m_CorrectChromaLevel;

	// Token: 0x0400013E RID: 318
	[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
	[Token(Token = "0x40000EA")]
	[SerializeField]
	private float m_CorrectBlendLevel;

	// Token: 0x0400013F RID: 319
	[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
	[Token(Token = "0x40000EB")]
	[SerializeField]
	private Color m_CorrectBlendColor;

	// Token: 0x04000140 RID: 320
	[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
	[Token(Token = "0x40000EC")]
	private bool m_BackupEnableCorrectToneCurve;

	// Token: 0x04000141 RID: 321
	[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
	[Token(Token = "0x40000ED")]
	private bool m_BackupEnableCorrectContrast;

	// Token: 0x04000142 RID: 322
	[Cpp2IlInjected.FieldOffset(Offset = "0x7A")]
	[Token(Token = "0x40000EE")]
	private bool m_BackupEnableCorrectBrightness;

	// Token: 0x04000143 RID: 323
	[Cpp2IlInjected.FieldOffset(Offset = "0x7B")]
	[Token(Token = "0x40000EF")]
	private bool m_BackupEnableCorrectChroma;

	// Token: 0x04000144 RID: 324
	[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
	[Token(Token = "0x40000F0")]
	private bool m_BackupEnableCorrectColorBlend;

	// Token: 0x04000145 RID: 325
	[Cpp2IlInjected.FieldOffset(Offset = "0x7D")]
	[Token(Token = "0x40000F1")]
	private bool m_BackupEnableBloom;

	// Token: 0x04000146 RID: 326
	[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
	[Token(Token = "0x40000F2")]
	private Vector4 m_BackupCorrectionKeyValue;

	// Token: 0x04000147 RID: 327
	[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
	[Token(Token = "0x40000F3")]
	private float m_BackupCorrectContrastLevel;

	// Token: 0x04000148 RID: 328
	[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
	[Token(Token = "0x40000F4")]
	private float m_BackupCorrectBrightnessLevel;

	// Token: 0x04000149 RID: 329
	[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
	[Token(Token = "0x40000F5")]
	private float m_BackupCorrectChromaLevel;

	// Token: 0x0400014A RID: 330
	[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
	[Token(Token = "0x40000F6")]
	private float m_BackupCorrectBlendLevel;

	// Token: 0x0400014B RID: 331
	[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
	[Token(Token = "0x40000F7")]
	private Color m_BackupCorrectBlendColor;

	// Token: 0x0400014C RID: 332
	[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
	[Token(Token = "0x40000F8")]
	private float m_BackupBloomBrightThresholdOffset;

	// Token: 0x0400014D RID: 333
	[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
	[Token(Token = "0x40000F9")]
	private float m_BackupBloomOffset;

	// Token: 0x0400014E RID: 334
	[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
	[Token(Token = "0x40000FA")]
	private float m_BackupBloomIntensity;

	// Token: 0x0400014F RID: 335
	[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
	[Token(Token = "0x40000FB")]
	private MultiCameraPostProcessFilter.eGaussFilterType m_BackupGaussFilterType;

	// Token: 0x04000150 RID: 336
	[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
	[Token(Token = "0x40000FC")]
	private Material m_CorrectMaterial;

	// Token: 0x04000151 RID: 337
	[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
	[Token(Token = "0x40000FD")]
	private Material m_BrightPassFilterMaterial;

	// Token: 0x04000152 RID: 338
	[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
	[Token(Token = "0x40000FE")]
	private Material m_BloomMaterial;

	// Token: 0x04000153 RID: 339
	[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
	[Token(Token = "0x40000FF")]
	private Material m_LuminunceMaterial;

	// Token: 0x04000154 RID: 340
	[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
	[Token(Token = "0x4000100")]
	private float m_AdaptedWhitePointScale;

	// Token: 0x04000155 RID: 341
	[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
	[Token(Token = "0x4000101")]
	private RenderTexture[] m_AdaptRenderTex;

	// Token: 0x04000156 RID: 342
	[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
	[Token(Token = "0x4000102")]
	private int m_CurAdaptIndex;

	// Token: 0x04000157 RID: 343
	[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
	[Token(Token = "0x4000103")]
	private bool m_isAvailable;

	// Token: 0x04000158 RID: 344
	[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
	[Token(Token = "0x4000104")]
	private int m_DivLevel;

	// Token: 0x04000159 RID: 345
	[Token(Token = "0x4000105")]
	private static string[] m_ShaderKeywordName_EnableToneCurve;

	// Token: 0x0400015A RID: 346
	[Token(Token = "0x4000106")]
	private static string[] m_ShaderKeywordName_EnableContrast;

	// Token: 0x0400015B RID: 347
	[Token(Token = "0x4000107")]
	private static string[] m_ShaderKeywordName_EnableBrightness;

	// Token: 0x0400015C RID: 348
	[Token(Token = "0x4000108")]
	private static string[] m_ShaderKeywordName_EnableChroma;

	// Token: 0x0400015D RID: 349
	[Token(Token = "0x4000109")]
	private static string[] m_ShaderKeywordName_EnableColorBlend;

	// Token: 0x0400015E RID: 350
	[Token(Token = "0x400010A")]
	private static string[] m_ShaderKeywordName_EnableBloom;

	// Token: 0x0400015F RID: 351
	[Token(Token = "0x400010B")]
	private static string[] m_ShaderKeywordName_GaussFilterType;

	// Token: 0x0200003B RID: 59
	[Token(Token = "0x2000CF3")]
	public enum eToneMapType
	{
		// Token: 0x04000161 RID: 353
		[Token(Token = "0x40053EF")]
		eToneMapType_Normal
	}

	// Token: 0x0200003C RID: 60
	[Token(Token = "0x2000CF4")]
	public enum eToneMapProc
	{
		// Token: 0x04000163 RID: 355
		[Token(Token = "0x40053F1")]
		eToneMapProc_Comp,
		// Token: 0x04000164 RID: 356
		[Token(Token = "0x40053F2")]
		eToneMapProc_Step,
		// Token: 0x04000165 RID: 357
		[Token(Token = "0x40053F3")]
		eToneMapProc_Fix
	}

	// Token: 0x0200003D RID: 61
	[Token(Token = "0x2000CF5")]
	public enum eToneMapWhitePoint
	{
		// Token: 0x04000167 RID: 359
		[Token(Token = "0x40053F5")]
		eToneMapWhitePoint_ToneLuminanceValue,
		// Token: 0x04000168 RID: 360
		[Token(Token = "0x40053F6")]
		eToneMapWhitePoint_ToneLuminanceValueMax,
		// Token: 0x04000169 RID: 361
		[Token(Token = "0x40053F7")]
		eToneMapWhitePoint_Fix
	}

	// Token: 0x0200003E RID: 62
	[Token(Token = "0x2000CF6")]
	public enum eBloomReferenceType
	{
		// Token: 0x0400016B RID: 363
		[Token(Token = "0x40053F9")]
		eBloomReferenceType_ToneLuminanceValue,
		// Token: 0x0400016C RID: 364
		[Token(Token = "0x40053FA")]
		eBloomReferenceType_Fix
	}

	// Token: 0x0200003F RID: 63
	[Token(Token = "0x2000CF7")]
	public enum eColorCorrectShaderType
	{
		// Token: 0x0400016E RID: 366
		[Token(Token = "0x40053FC")]
		eColorCorrectShaderType_Invalid,
		// Token: 0x0400016F RID: 367
		[Token(Token = "0x40053FD")]
		eColorCorrectShaderType_ToneMapParam,
		// Token: 0x04000170 RID: 368
		[Token(Token = "0x40053FE")]
		eColorCorrectShaderType_Reduction,
		// Token: 0x04000171 RID: 369
		[Token(Token = "0x40053FF")]
		eColorCorrectShaderType_Combiner
	}

	// Token: 0x02000040 RID: 64
	[Token(Token = "0x2000CF8")]
	public enum eColorCorrectCombineID
	{
		// Token: 0x04000173 RID: 371
		[Token(Token = "0x4005401")]
		eColorCorrectCombineID_ToneMap,
		// Token: 0x04000174 RID: 372
		[Token(Token = "0x4005402")]
		eColorCorrectCombineID_Contrast,
		// Token: 0x04000175 RID: 373
		[Token(Token = "0x4005403")]
		eColorCorrectCombineID_Brightness,
		// Token: 0x04000176 RID: 374
		[Token(Token = "0x4005404")]
		eColorCorrectCombineID_Chroma,
		// Token: 0x04000177 RID: 375
		[Token(Token = "0x4005405")]
		eColorCorrectCombineID_ColorBlend
	}

	// Token: 0x02000041 RID: 65
	[Token(Token = "0x2000CF9")]
	public enum eGaussFilterType
	{
		// Token: 0x04000179 RID: 377
		[Token(Token = "0x4005407")]
		eGaussFilterType_None,
		// Token: 0x0400017A RID: 378
		[Token(Token = "0x4005408")]
		eGaussFilterType_x5,
		// Token: 0x0400017B RID: 379
		[Token(Token = "0x4005409")]
		eGaussFilterType_x7
	}
}
