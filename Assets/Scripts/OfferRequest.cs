﻿using Meige;
using OfferRequestTypes;

public static class OfferRequest {
    public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Order(Order param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/order", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("playerOfferId", param.playerOfferId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Order(long playerOfferId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/order", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("playerOfferId", playerOfferId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Complete(Complete param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("playerOfferId", param.playerOfferId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Complete(long playerOfferId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/offer/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("playerOfferId", playerOfferId);
        www.SetPostCrypt();
        return www;
    }
}
