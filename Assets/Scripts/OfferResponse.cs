﻿using Meige;
using OfferResponseTypes;
using System.Collections.Generic;
using WWWTypes;

public static class OfferResponse {
    public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
    }

    public static Order Order(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Order>(param, dialogType, acceptableResultCodes);
    }

    public static Complete Complete(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Complete>(param, dialogType, acceptableResultCodes);
    }
}
