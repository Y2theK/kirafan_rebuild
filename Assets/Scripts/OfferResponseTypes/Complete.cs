﻿using CommonResponseTypes;
using WWWTypes;

namespace OfferResponseTypes {
    public class Complete : CommonResponse {
        public PlayerOffer[] offers;
        public OfferTitleType[] offerTitleTypes;
        public Player player;
        public PlayerMasterOrb[] managedMasterOrbs;
        public PlayerItemSummary[] itemSummary;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (offers == null) ? string.Empty : offers.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            str += (managedMasterOrbs == null) ? string.Empty : managedMasterOrbs.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            return str;
        }
    }
}
