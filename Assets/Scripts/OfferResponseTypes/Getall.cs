﻿using CommonResponseTypes;
using WWWTypes;

namespace OfferResponseTypes {
    public class Getall : CommonResponse {
        public PlayerOffer[] offers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (offers == null) ? string.Empty : offers.ToString();
            return str;
        }
    }
}
