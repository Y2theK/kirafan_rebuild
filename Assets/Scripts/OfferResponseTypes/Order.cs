﻿using CommonResponseTypes;
using WWWTypes;

namespace OfferResponseTypes {
    public class Order : CommonResponse {
        public PlayerOffer[] offers;
        public OfferTitleType[] offerTitleTypes;
        public PlayerItemSummary[] itemSummary;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (offers == null) ? string.Empty : offers.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            return str;
        }
    }
}
