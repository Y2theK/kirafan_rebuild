﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000BB RID: 187
[Token(Token = "0x2000092")]
[StructLayout(3)]
public class ParticleManager : MonoBehaviour
{
	// Token: 0x06000442 RID: 1090 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F8")]
	[Address(RVA = "0x10162DAC0", Offset = "0x162DAC0", VA = "0x10162DAC0")]
	private void Start()
	{
	}

	// Token: 0x06000443 RID: 1091 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003F9")]
	[Address(RVA = "0x10162DAC4", Offset = "0x162DAC4", VA = "0x10162DAC4")]
	private void Update()
	{
	}

	// Token: 0x06000444 RID: 1092 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003FA")]
	[Address(RVA = "0x10162DAC8", Offset = "0x162DAC8", VA = "0x10162DAC8")]
	public ParticleManager()
	{
	}
}
