﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000042 RID: 66
[Token(Token = "0x2000023")]
[StructLayout(3)]
public class PixelCrashRenderer : MonoBehaviour
{
	// Token: 0x1700003F RID: 63
	// (get) Token: 0x06000119 RID: 281 RVA: 0x00002568 File Offset: 0x00000768
	[Token(Token = "0x17000033")]
	public float rate
	{
		[Token(Token = "0x60000D8")]
		[Address(RVA = "0x10162DAD0", Offset = "0x162DAD0", VA = "0x10162DAD0")]
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000040 RID: 64
	// (get) Token: 0x0600011A RID: 282 RVA: 0x00002580 File Offset: 0x00000780
	[Token(Token = "0x17000034")]
	public bool isAlive
	{
		[Token(Token = "0x60000D9")]
		[Address(RVA = "0x10162DAD8", Offset = "0x162DAD8", VA = "0x10162DAD8")]
		get
		{
			return default(bool);
		}
	}

	// Token: 0x0600011B RID: 283 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000DA")]
	[Address(RVA = "0x10162DAE0", Offset = "0x162DAE0", VA = "0x10162DAE0")]
	public void EnableRender(bool flg)
	{
	}

	// Token: 0x0600011C RID: 284 RVA: 0x00002598 File Offset: 0x00000798
	[Token(Token = "0x60000DB")]
	[Address(RVA = "0x10162DB0C", Offset = "0x162DB0C", VA = "0x10162DB0C")]
	public bool IsEnableRender()
	{
		return default(bool);
	}

	// Token: 0x0600011D RID: 285 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000DC")]
	[Address(RVA = "0x10162DB14", Offset = "0x162DB14", VA = "0x10162DB14")]
	private void OnDestroy()
	{
	}

	// Token: 0x0600011E RID: 286 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000DD")]
	[Address(RVA = "0x10162DB18", Offset = "0x162DB18", VA = "0x10162DB18")]
	private void SetProgressRate(float rate)
	{
	}

	// Token: 0x0600011F RID: 287 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000DE")]
	[Address(RVA = "0x10162DB8C", Offset = "0x162DB8C", VA = "0x10162DB8C")]
	public void SetGravity()
	{
	}

	// Token: 0x06000120 RID: 288 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000DF")]
	[Address(RVA = "0x10162DCF0", Offset = "0x162DCF0", VA = "0x10162DCF0")]
	public void SetNormalGravity()
	{
	}

	// Token: 0x06000121 RID: 289 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E0")]
	[Address(RVA = "0x10162DE54", Offset = "0x162DE54", VA = "0x10162DE54")]
	public void SetFirstCommonVelocity()
	{
	}

	// Token: 0x06000122 RID: 290 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E1")]
	[Address(RVA = "0x10162DFB8", Offset = "0x162DFB8", VA = "0x10162DFB8")]
	public void SetFirstNormalVelocity()
	{
	}

	// Token: 0x06000123 RID: 291 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E2")]
	[Address(RVA = "0x10162E11C", Offset = "0x162E11C", VA = "0x10162E11C")]
	public void SetType()
	{
	}

	// Token: 0x06000124 RID: 292 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E3")]
	[Address(RVA = "0x10162E2BC", Offset = "0x162E2BC", VA = "0x10162E2BC")]
	public void SetShapeArgment()
	{
	}

	// Token: 0x06000125 RID: 293 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E4")]
	[Address(RVA = "0x10162E420", Offset = "0x162E420", VA = "0x10162E420")]
	public void SetInvertMoveToNormalDir()
	{
	}

	// Token: 0x06000126 RID: 294 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E5")]
	[Address(RVA = "0x10162E5DC", Offset = "0x162E5DC", VA = "0x10162E5DC")]
	public void SetMulColor()
	{
	}

	// Token: 0x06000127 RID: 295 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E6")]
	[Address(RVA = "0x10162E700", Offset = "0x162E700", VA = "0x10162E700")]
	public void SetAddColor()
	{
	}

	// Token: 0x06000128 RID: 296 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E7")]
	[Address(RVA = "0x10162E824", Offset = "0x162E824", VA = "0x10162E824")]
	public void SetBaseAddColor()
	{
	}

	// Token: 0x06000129 RID: 297 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E8")]
	[Address(RVA = "0x10162E948", Offset = "0x162E948", VA = "0x10162E948")]
	public void SetHDRFactor()
	{
	}

	// Token: 0x0600012A RID: 298 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000E9")]
	[Address(RVA = "0x10162EA28", Offset = "0x162EA28", VA = "0x10162EA28")]
	public void SetChangeHDRFactor()
	{
	}

	// Token: 0x0600012B RID: 299 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000EA")]
	[Address(RVA = "0x10162EB08", Offset = "0x162EB08", VA = "0x10162EB08")]
	public void SetChangeThreshold()
	{
	}

	// Token: 0x0600012C RID: 300 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000EB")]
	[Address(RVA = "0x10162EBE8", Offset = "0x162EBE8", VA = "0x10162EBE8")]
	public void SetFadeThreshold()
	{
	}

	// Token: 0x0600012D RID: 301 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000EC")]
	[Address(RVA = "0x10162ECC8", Offset = "0x162ECC8", VA = "0x10162ECC8")]
	public void SetNoiseTextureParam(float addX = 0f, float addY = 0f)
	{
	}

	// Token: 0x0600012E RID: 302 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000ED")]
	[Address(RVA = "0x10162EE98", Offset = "0x162EE98", VA = "0x10162EE98")]
	private void Start()
	{
	}

	// Token: 0x0600012F RID: 303 RVA: 0x000025B0 File Offset: 0x000007B0
	[Token(Token = "0x60000EE")]
	[Address(RVA = "0x10162EE9C", Offset = "0x162EE9C", VA = "0x10162EE9C")]
	public bool Setup()
	{
		return default(bool);
	}

	// Token: 0x06000130 RID: 304 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000EF")]
	[Address(RVA = "0x10162EFA8", Offset = "0x162EFA8", VA = "0x10162EFA8")]
	private void CreateRenderObject()
	{
	}

	// Token: 0x06000131 RID: 305 RVA: 0x000025C8 File Offset: 0x000007C8
	[Token(Token = "0x60000F0")]
	[Address(RVA = "0x10162F258", Offset = "0x162F258", VA = "0x10162F258")]
	public bool Prepare(float startRate = 0f)
	{
		return default(bool);
	}

	// Token: 0x06000132 RID: 306 RVA: 0x000025E0 File Offset: 0x000007E0
	[Token(Token = "0x60000F1")]
	[Address(RVA = "0x10162F3B0", Offset = "0x162F3B0", VA = "0x10162F3B0")]
	public bool Play(float startRate = 0f)
	{
		return default(bool);
	}

	// Token: 0x06000133 RID: 307 RVA: 0x000025F8 File Offset: 0x000007F8
	[Token(Token = "0x60000F2")]
	[Address(RVA = "0x10162F51C", Offset = "0x162F51C", VA = "0x10162F51C")]
	public bool Kill()
	{
		return default(bool);
	}

	// Token: 0x06000134 RID: 308 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000F3")]
	[Address(RVA = "0x10162F550", Offset = "0x162F550", VA = "0x10162F550")]
	private void Update()
	{
	}

	// Token: 0x06000135 RID: 309 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000F4")]
	[Address(RVA = "0x10162F718", Offset = "0x162F718", VA = "0x10162F718")]
	private void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
	}

	// Token: 0x06000136 RID: 310 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000F5")]
	[Address(RVA = "0x10162F800", Offset = "0x162F800", VA = "0x10162F800")]
	public PixelCrashRenderer()
	{
	}

	// Token: 0x0400017C RID: 380
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x400010C")]
	public SaveAreaCamera m_SaveAreaCamera;

	// Token: 0x0400017D RID: 381
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x400010D")]
	public Texture m_NoiseTexture;

	// Token: 0x0400017E RID: 382
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x400010E")]
	public Material m_PixelCrashMaterial;

	// Token: 0x0400017F RID: 383
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x400010F")]
	public PixelCrashRenderer.Param m_Param;

	// Token: 0x04000180 RID: 384
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x4000110")]
	private PixelCrashRenderer.Param m_History;

	// Token: 0x04000181 RID: 385
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x4000111")]
	private GameObject m_GO;

	// Token: 0x04000182 RID: 386
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x4000112")]
	[SerializeField]
	private bool m_DebugStartFlg;

	// Token: 0x04000183 RID: 387
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x4000113")]
	[SerializeField]
	private Texture m_DebugTexture;

	// Token: 0x04000184 RID: 388
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x4000114")]
	private float m_Rate;

	// Token: 0x04000185 RID: 389
	[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
	[Token(Token = "0x4000115")]
	private bool m_isAlive;

	// Token: 0x04000186 RID: 390
	[Cpp2IlInjected.FieldOffset(Offset = "0x5D")]
	[Token(Token = "0x4000116")]
	private bool m_isEnableRender;

	// Token: 0x02000043 RID: 67
	[Token(Token = "0x2000CFA")]
	public enum eType
	{
		// Token: 0x04000188 RID: 392
		[Token(Token = "0x400540B")]
		Invalid = -1,
		// Token: 0x04000189 RID: 393
		[Token(Token = "0x400540C")]
		None,
		// Token: 0x0400018A RID: 394
		[Token(Token = "0x400540D")]
		Line,
		// Token: 0x0400018B RID: 395
		[Token(Token = "0x400540E")]
		Circle
	}

	// Token: 0x02000044 RID: 68
	[Token(Token = "0x2000CFB")]
	public enum eMove
	{
		// Token: 0x0400018D RID: 397
		[Token(Token = "0x4005410")]
		Slowdown_x2,
		// Token: 0x0400018E RID: 398
		[Token(Token = "0x4005411")]
		Slowdown,
		// Token: 0x0400018F RID: 399
		[Token(Token = "0x4005412")]
		Standard,
		// Token: 0x04000190 RID: 400
		[Token(Token = "0x4005413")]
		Acceleration,
		// Token: 0x04000191 RID: 401
		[Token(Token = "0x4005414")]
		Acceleration_x2
	}

	// Token: 0x02000045 RID: 69
	[Token(Token = "0x2000CFC")]
	[Serializable]
	public class Param
	{
		// Token: 0x06000137 RID: 311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0E")]
		[Address(RVA = "0x10162EF80", Offset = "0x162EF80", VA = "0x10162EF80")]
		public Param()
		{
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D0F")]
		[Address(RVA = "0x10162F810", Offset = "0x162F810", VA = "0x10162F810")]
		public void SetDefault()
		{
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D10")]
		[Address(RVA = "0x10162F09C", Offset = "0x162F09C", VA = "0x10162F09C")]
		public void SetDirty()
		{
		}

		// Token: 0x04000192 RID: 402
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005415")]
		public PixelCrashRenderer.eType m_Type;

		// Token: 0x04000193 RID: 403
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4005416")]
		public PixelCrashRenderer.eMove m_RateMove;

		// Token: 0x04000194 RID: 404
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4005417")]
		public float m_LifeSpanSec;

		// Token: 0x04000195 RID: 405
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4005418")]
		public Vector4 m_Gravity;

		// Token: 0x04000196 RID: 406
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4005419")]
		public Vector4 m_NormalGravity;

		// Token: 0x04000197 RID: 407
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x400541A")]
		public Vector4 m_FirstCommonVelocity;

		// Token: 0x04000198 RID: 408
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400541B")]
		public Vector4 m_FirstNormalVelocity;

		// Token: 0x04000199 RID: 409
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400541C")]
		public Vector4 m_ShapeArgment;

		// Token: 0x0400019A RID: 410
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400541D")]
		public bool m_InvertMoveToNormalDir;

		// Token: 0x0400019B RID: 411
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400541E")]
		public Color m_MulColor;

		// Token: 0x0400019C RID: 412
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400541F")]
		public Color m_AddColor;

		// Token: 0x0400019D RID: 413
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4005420")]
		public Color m_BaseAddColor;

		// Token: 0x0400019E RID: 414
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4005421")]
		public float m_HDRFactor;

		// Token: 0x0400019F RID: 415
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4005422")]
		public float m_ChangeHDRFactor;

		// Token: 0x040001A0 RID: 416
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4005423")]
		public float m_ChangeThreshold;

		// Token: 0x040001A1 RID: 417
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4005424")]
		public float m_FadeThreshold;

		// Token: 0x040001A2 RID: 418
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4005425")]
		public Vector4 m_NoiseTextureParam;
	}
}
