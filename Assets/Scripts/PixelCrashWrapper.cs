﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x02000046 RID: 70
[Token(Token = "0x2000024")]
[StructLayout(3)]
public class PixelCrashWrapper : MonoBehaviour
{
	// Token: 0x17000041 RID: 65
	// (get) Token: 0x0600013A RID: 314 RVA: 0x00002610 File Offset: 0x00000810
	[Token(Token = "0x17000035")]
	public float rate
	{
		[Token(Token = "0x60000F6")]
		[Address(RVA = "0x10162F9CC", Offset = "0x162F9CC", VA = "0x10162F9CC")]
		get
		{
			return 0f;
		}
	}

	// Token: 0x17000042 RID: 66
	// (get) Token: 0x0600013B RID: 315 RVA: 0x00002628 File Offset: 0x00000828
	[Token(Token = "0x17000036")]
	public bool isAlive
	{
		[Token(Token = "0x60000F7")]
		[Address(RVA = "0x10162F9D4", Offset = "0x162F9D4", VA = "0x10162F9D4")]
		get
		{
			return default(bool);
		}
	}

	// Token: 0x0600013C RID: 316 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000F8")]
	[Address(RVA = "0x10162F9DC", Offset = "0x162F9DC", VA = "0x10162F9DC")]
	private void OnDestroy()
	{
	}

	// Token: 0x0600013D RID: 317 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000F9")]
	[Address(RVA = "0x10162F9E0", Offset = "0x162F9E0", VA = "0x10162F9E0")]
	private void SetProgressRate(float rate)
	{
	}

	// Token: 0x0600013E RID: 318 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FA")]
	[Address(RVA = "0x10162FA54", Offset = "0x162FA54", VA = "0x10162FA54")]
	public void SetGravity()
	{
	}

	// Token: 0x0600013F RID: 319 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FB")]
	[Address(RVA = "0x10162FBB8", Offset = "0x162FBB8", VA = "0x10162FBB8")]
	public void SetNormalGravity()
	{
	}

	// Token: 0x06000140 RID: 320 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FC")]
	[Address(RVA = "0x10162FD1C", Offset = "0x162FD1C", VA = "0x10162FD1C")]
	public void SetFirstCommonVelocity()
	{
	}

	// Token: 0x06000141 RID: 321 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FD")]
	[Address(RVA = "0x10162FE80", Offset = "0x162FE80", VA = "0x10162FE80")]
	public void SetFirstNormalVelocity()
	{
	}

	// Token: 0x06000142 RID: 322 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FE")]
	[Address(RVA = "0x10162FFE4", Offset = "0x162FFE4", VA = "0x10162FFE4")]
	public void SetType()
	{
	}

	// Token: 0x06000143 RID: 323 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60000FF")]
	[Address(RVA = "0x101630184", Offset = "0x1630184", VA = "0x101630184")]
	public void SetShapeArgment()
	{
	}

	// Token: 0x06000144 RID: 324 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000100")]
	[Address(RVA = "0x1016302E8", Offset = "0x16302E8", VA = "0x1016302E8")]
	public void SetInvertMoveToNormalDir()
	{
	}

	// Token: 0x06000145 RID: 325 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000101")]
	[Address(RVA = "0x1016304A4", Offset = "0x16304A4", VA = "0x1016304A4")]
	public void SetMulColor()
	{
	}

	// Token: 0x06000146 RID: 326 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000102")]
	[Address(RVA = "0x1016305C8", Offset = "0x16305C8", VA = "0x1016305C8")]
	public void SetAddColor()
	{
	}

	// Token: 0x06000147 RID: 327 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000103")]
	[Address(RVA = "0x1016306EC", Offset = "0x16306EC", VA = "0x1016306EC")]
	public void SetBaseAddColor()
	{
	}

	// Token: 0x06000148 RID: 328 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000104")]
	[Address(RVA = "0x101630810", Offset = "0x1630810", VA = "0x101630810")]
	public void SetHDRFactor()
	{
	}

	// Token: 0x06000149 RID: 329 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000105")]
	[Address(RVA = "0x1016308F0", Offset = "0x16308F0", VA = "0x1016308F0")]
	public void SetChangeHDRFactor()
	{
	}

	// Token: 0x0600014A RID: 330 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000106")]
	[Address(RVA = "0x1016309D0", Offset = "0x16309D0", VA = "0x1016309D0")]
	public void SetChangeThreshold()
	{
	}

	// Token: 0x0600014B RID: 331 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000107")]
	[Address(RVA = "0x101630AB0", Offset = "0x1630AB0", VA = "0x101630AB0")]
	public void SetFadeThreshold()
	{
	}

	// Token: 0x0600014C RID: 332 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000108")]
	[Address(RVA = "0x101630B90", Offset = "0x1630B90", VA = "0x101630B90")]
	public void SetNoiseTextureParam(float addX = 0f, float addY = 0f)
	{
	}

	// Token: 0x0600014D RID: 333 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000109")]
	[Address(RVA = "0x101630D60", Offset = "0x1630D60", VA = "0x101630D60")]
	private void Awake()
	{
	}

	// Token: 0x0600014E RID: 334 RVA: 0x00002640 File Offset: 0x00000840
	[Token(Token = "0x600010A")]
	[Address(RVA = "0x101630D64", Offset = "0x1630D64", VA = "0x101630D64")]
	public bool Setup()
	{
		return default(bool);
	}

	// Token: 0x0600014F RID: 335 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600010B")]
	[Address(RVA = "0x101631594", Offset = "0x1631594", VA = "0x101631594")]
	private void AttachTargetCamera()
	{
	}

	// Token: 0x06000150 RID: 336 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600010C")]
	[Address(RVA = "0x101630F0C", Offset = "0x1630F0C", VA = "0x101630F0C")]
	private void CreateRenderObject()
	{
	}

	// Token: 0x06000151 RID: 337 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600010D")]
	[Address(RVA = "0x101631874", Offset = "0x1631874", VA = "0x101631874")]
	public void SetMeshScale(float scale = 1f)
	{
	}

	// Token: 0x06000152 RID: 338 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600010E")]
	[Address(RVA = "0x101631AA0", Offset = "0x1631AA0", VA = "0x101631AA0")]
	public void SetMeshColor(Color color)
	{
	}

	// Token: 0x06000153 RID: 339 RVA: 0x00002658 File Offset: 0x00000858
	[Token(Token = "0x600010F")]
	[Address(RVA = "0x101631BC8", Offset = "0x1631BC8", VA = "0x101631BC8")]
	public bool Prepare(float startRate = 0f)
	{
		return default(bool);
	}

	// Token: 0x06000154 RID: 340 RVA: 0x00002670 File Offset: 0x00000870
	[Token(Token = "0x6000110")]
	[Address(RVA = "0x101631DB0", Offset = "0x1631DB0", VA = "0x101631DB0")]
	public bool Play(float startRate = 0f)
	{
		return default(bool);
	}

	// Token: 0x06000155 RID: 341 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000111")]
	[Address(RVA = "0x101631FA8", Offset = "0x1631FA8", VA = "0x101631FA8")]
	public void EnableRender(bool flg)
	{
	}

	// Token: 0x06000156 RID: 342 RVA: 0x00002688 File Offset: 0x00000888
	[Token(Token = "0x6000112")]
	[Address(RVA = "0x101632050", Offset = "0x1632050", VA = "0x101632050")]
	public bool IsEnableRender()
	{
		return default(bool);
	}

	// Token: 0x06000157 RID: 343 RVA: 0x000026A0 File Offset: 0x000008A0
	[Token(Token = "0x6000113")]
	[Address(RVA = "0x101632058", Offset = "0x1632058", VA = "0x101632058")]
	public bool Kill()
	{
		return default(bool);
	}

	// Token: 0x06000158 RID: 344 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000114")]
	[Address(RVA = "0x10163208C", Offset = "0x163208C", VA = "0x10163208C")]
	private void Update()
	{
	}

	// Token: 0x06000159 RID: 345 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000115")]
	[Address(RVA = "0x10163225C", Offset = "0x163225C", VA = "0x10163225C")]
	public PixelCrashWrapper()
	{
	}

	// Token: 0x040001A3 RID: 419
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000117")]
	public Camera m_CrashTargetCamera;

	// Token: 0x040001A4 RID: 420
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000118")]
	public Camera m_RenderCamera;

	// Token: 0x040001A5 RID: 421
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000119")]
	public Material m_PixelCrashMaterialBase;

	// Token: 0x040001A6 RID: 422
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x400011A")]
	public Material m_PixelCrashMaterial;

	// Token: 0x040001A7 RID: 423
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x400011B")]
	public PixelCrashWrapper.Param m_Param;

	// Token: 0x040001A8 RID: 424
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x400011C")]
	private PixelCrashWrapper.Param m_History;

	// Token: 0x040001A9 RID: 425
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x400011D")]
	private RenderTexture m_Texture;

	// Token: 0x040001AA RID: 426
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x400011E")]
	private Renderer m_Renderer;

	// Token: 0x040001AB RID: 427
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x400011F")]
	private MeshFilter m_MeshFilter;

	// Token: 0x040001AC RID: 428
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x4000120")]
	private Mesh m_Mesh;

	// Token: 0x040001AD RID: 429
	[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
	[Token(Token = "0x4000121")]
	private GameObject m_GO;

	// Token: 0x040001AE RID: 430
	[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
	[Token(Token = "0x4000122")]
	[SerializeField]
	private bool m_DebugStartFlg;

	// Token: 0x040001AF RID: 431
	[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
	[Token(Token = "0x4000123")]
	[SerializeField]
	private Texture m_DebugTexture;

	// Token: 0x040001B0 RID: 432
	[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
	[Token(Token = "0x4000124")]
	private float m_Rate;

	// Token: 0x040001B1 RID: 433
	[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
	[Token(Token = "0x4000125")]
	private bool m_isAlive;

	// Token: 0x040001B2 RID: 434
	[Cpp2IlInjected.FieldOffset(Offset = "0x85")]
	[Token(Token = "0x4000126")]
	private bool m_isEnableRender;

	// Token: 0x02000047 RID: 71
	[Token(Token = "0x2000CFD")]
	public enum eType
	{
		// Token: 0x040001B4 RID: 436
		[Token(Token = "0x4005427")]
		Invalid = -1,
		// Token: 0x040001B5 RID: 437
		[Token(Token = "0x4005428")]
		None,
		// Token: 0x040001B6 RID: 438
		[Token(Token = "0x4005429")]
		Line,
		// Token: 0x040001B7 RID: 439
		[Token(Token = "0x400542A")]
		Circle
	}

	// Token: 0x02000048 RID: 72
	[Token(Token = "0x2000CFE")]
	public enum eMove
	{
		// Token: 0x040001B9 RID: 441
		[Token(Token = "0x400542C")]
		Slowdown_x2,
		// Token: 0x040001BA RID: 442
		[Token(Token = "0x400542D")]
		Slowdown,
		// Token: 0x040001BB RID: 443
		[Token(Token = "0x400542E")]
		Standard,
		// Token: 0x040001BC RID: 444
		[Token(Token = "0x400542F")]
		Acceleration,
		// Token: 0x040001BD RID: 445
		[Token(Token = "0x4005430")]
		Acceleration_x2
	}

	// Token: 0x02000049 RID: 73
	[Token(Token = "0x2000CFF")]
	[Serializable]
	public class Param
	{
		// Token: 0x0600015A RID: 346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D11")]
		[Address(RVA = "0x101630EE4", Offset = "0x1630EE4", VA = "0x101630EE4")]
		public Param()
		{
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D12")]
		[Address(RVA = "0x10163226C", Offset = "0x163226C", VA = "0x10163226C")]
		public void SetDefault()
		{
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D13")]
		[Address(RVA = "0x1016316B8", Offset = "0x16316B8", VA = "0x1016316B8")]
		public void SetDirty()
		{
		}

		// Token: 0x040001BE RID: 446
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005431")]
		public PixelCrashWrapper.eType m_Type;

		// Token: 0x040001BF RID: 447
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4005432")]
		public PixelCrashWrapper.eMove m_RateMove;

		// Token: 0x040001C0 RID: 448
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4005433")]
		public float m_LifeSpanSec;

		// Token: 0x040001C1 RID: 449
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4005434")]
		public Vector4 m_Gravity;

		// Token: 0x040001C2 RID: 450
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4005435")]
		public Vector4 m_NormalGravity;

		// Token: 0x040001C3 RID: 451
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4005436")]
		public Vector4 m_FirstCommonVelocity;

		// Token: 0x040001C4 RID: 452
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4005437")]
		public Vector4 m_FirstNormalVelocity;

		// Token: 0x040001C5 RID: 453
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4005438")]
		public Vector4 m_ShapeArgment;

		// Token: 0x040001C6 RID: 454
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4005439")]
		public bool m_InvertMoveToNormalDir;

		// Token: 0x040001C7 RID: 455
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400543A")]
		public Color m_MulColor;

		// Token: 0x040001C8 RID: 456
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400543B")]
		public Color m_AddColor;

		// Token: 0x040001C9 RID: 457
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400543C")]
		public Color m_BaseAddColor;

		// Token: 0x040001CA RID: 458
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400543D")]
		public float m_HDRFactor;

		// Token: 0x040001CB RID: 459
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x400543E")]
		public float m_ChangeHDRFactor;

		// Token: 0x040001CC RID: 460
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400543F")]
		public float m_ChangeThreshold;

		// Token: 0x040001CD RID: 461
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4005440")]
		public float m_FadeThreshold;

		// Token: 0x040001CE RID: 462
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4005441")]
		public Vector4 m_NoiseTextureParam;
	}
}
