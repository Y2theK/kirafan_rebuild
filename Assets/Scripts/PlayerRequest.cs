﻿using Meige;
using PlayerRequestTypes;
using WWWTypes;

public static class PlayerRequest {
    public static MeigewwwParam Signup(Signup param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/signup", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", param.uuid);
        www.Add("platform", param.platform);
        www.Add("name", param.name);
        www.Add("stepCode", param.stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Signup(string uuid, int platform, string name, int stepCode, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/signup", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", uuid);
        www.Add("platform", platform);
        www.Add("name", name);
        www.Add("stepCode", stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Login(Login param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/login", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", param.uuid);
        www.Add("accessToken", param.accessToken);
        www.Add("platform", param.platform);
        www.Add("appVersion", param.appVersion);
        www.Add("authVoided", param.authVoided);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Login(string uuid, string accessToken, int platform, string appVersion, int authVoided, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/login", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", uuid);
        www.Add("accessToken", accessToken);
        www.Add("platform", platform);
        www.Add("appVersion", appVersion);
        www.Add("authVoided", authVoided);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setpushtoken(Setpushtoken param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/push_token/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("pushToken", param.pushToken);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setpushtoken(string pushToken, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/push_token/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("pushToken", pushToken);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Moveget(Moveget param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/get", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("password", param.password);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Moveget(string password, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/get", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("password", password);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Moveset(Moveset param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("moveCode", param.moveCode);
        www.Add("movePassword", param.movePassword);
        www.Add("uuid", param.uuid);
        www.Add("platform", param.platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Moveset(string moveCode, string movePassword, string uuid, int platform, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("moveCode", moveCode);
        www.Add("movePassword", movePassword);
        www.Add("uuid", uuid);
        www.Add("platform", platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Linkmoveset(Linkmoveset param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/link/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("linkId", param.linkId);
        www.Add("uuid", param.uuid);
        www.Add("platform", param.platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Linkmoveset(string linkId, string uuid, int platform, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/move/link/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("linkId", linkId);
        www.Add("uuid", uuid);
        www.Add("platform", platform);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Reset(Reset param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/reset", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", param.uuid);
        www.Add("accessToken", param.accessToken);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Reset(string uuid, string accessToken, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/reset", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("uuid", uuid);
        www.Add("accessToken", accessToken);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam GeneralFlagSave(GeneralFlagSave param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/general_flag_save", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("type", param.type);
        www.Add("data", param.data);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam GeneralFlagSave(long type, string data, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/general_flag_save", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("type", type);
        www.Add("data", data);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getbadgecount(Getbadgecount param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/badge/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Getbadgecount(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/badge/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Resumeget(Resumeget param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/resume/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("playerId", param.playerId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Resumeget(long playerId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/resume/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("playerId", playerId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("name", param.name);
        www.Add("comment", param.comment);
        www.Add("linkId", param.linkId);
        www.Add("managedFavoriteCharacterIds", param.managedFavoriteCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Set(string name, string comment, string linkId, long[] managedFavoriteCharacterIds, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("name", name);
        www.Add("comment", comment);
        www.Add("linkId", linkId);
        www.Add("managedFavoriteCharacterIds", managedFavoriteCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setpushnotification(Setpushnotification param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/push_notification/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("flag1", param.flag1);
        www.Add("flag2", param.flag2);
        www.Add("flag3", param.flag3);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setpushnotification(int flag1, int flag2, int flag3, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/push_notification/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("flag1", flag1);
        www.Add("flag2", flag2);
        www.Add("flag3", flag3);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setage(Setage param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/age/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("age", param.age);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Setage(int age, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/age/set", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("age", age);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Loginbonus(Loginbonus param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/login_bonus/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Loginbonus(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/login_bonus/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Itemgetsummary(Itemgetsummary param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/item/get_summary", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Itemgetsummary(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/item/get_summary", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Itemsale(Itemsale param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/item/sale", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("itemId", param.itemId);
        www.Add("amount", param.amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Itemsale(string itemId, string amount, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/item/sale", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("itemId", itemId);
        www.Add("amount", amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weapongetall(Weapongetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weapongetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponmake(Weaponmake param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/make", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("recipeId", param.recipeId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponmake(int recipeId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/make", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("recipeId", recipeId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponupgrade(Weaponupgrade param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", param.managedWeaponId);
        www.Add("itemId", param.itemId);
        www.Add("amount", param.amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponupgrade(long managedWeaponId, string itemId, string amount, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", managedWeaponId);
        www.Add("itemId", itemId);
        www.Add("amount", amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponsale(Weaponsale param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/sale", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", param.managedWeaponId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponsale(string managedWeaponId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/sale", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", managedWeaponId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponreceive(Weaponreceive param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/receive", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterIds", param.managedCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponreceive(long[] managedCharacterIds, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/receive", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterIds", managedCharacterIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponevolution(Weaponevolution param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/evolution", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", param.managedWeaponId);
        www.Add("weaponEvolutionId", param.weaponEvolutionId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponevolution(long managedWeaponId, int weaponEvolutionId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/evolution", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", managedWeaponId);
        www.Add("weaponEvolutionId", weaponEvolutionId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponlimitadd(Weaponlimitadd param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("num", param.num);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponlimitadd(int num, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("num", num);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponskillup(Weaponskillup param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/skill/up", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", param.managedWeaponId);
        www.Add("itemIds", param.itemIds);
        www.Add("amounts", param.amounts);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Weaponskillup(long managedWeaponId, int[] itemIds, int[] amounts, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/weapon/skill/up", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedWeaponId", managedWeaponId);
        www.Add("itemIds", itemIds);
        www.Add("amounts", amounts);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactergetall(Charactergetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactergetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactersetshown(Charactersetshown param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", param.managedCharacterId);
        www.Add("shown", param.shown);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactersetshown(long managedCharacterId, int shown, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/set_shown", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", managedCharacterId);
        www.Add("shown", shown);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterlimitbreak(Characterlimitbreak param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/limitBreak", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", param.managedCharacterId);
        www.Add("itemIds", param.itemIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterlimitbreak(long managedCharacterId, int[] itemIds, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/limitBreak", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", managedCharacterId);
        www.Add("itemIds", itemIds);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterupgrade(Characterupgrade param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", param.managedCharacterId);
        www.Add("itemId", param.itemId);
        www.Add("amount", param.amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterupgrade(long managedCharacterId, string itemId, string amount, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/upgrade", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", managedCharacterId);
        www.Add("itemId", itemId);
        www.Add("amount", amount);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterevolution(Characterevolution param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/evolution", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", param.managedCharacterId);
        www.Add("recipeId", param.recipeId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterevolution(long managedCharacterId, int recipeId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/evolution", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterId", managedCharacterId);
        www.Add("recipeId", recipeId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactersetview(Charactersetview param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/set_view", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterIds", param.managedCharacterIds);
        www.Add("viewEvolutions", param.viewEvolutions);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Charactersetview(long[] managedCharacterIds, int[] viewEvolutions, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/set_view", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedCharacterIds", managedCharacterIds);
        www.Add("viewEvolutions", viewEvolutions);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterresetallview(Characterresetallview param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/reset_all_view", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Characterresetallview(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/character/reset_all_view", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartysetname(Battlepartysetname param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/set_name", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedBattlePartyId", param.managedBattlePartyId);
        www.Add("name", param.name);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartysetname(long managedBattlePartyId, string name, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/set_name", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("managedBattlePartyId", managedBattlePartyId);
        www.Add("name", name);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartysetall(Battlepartysetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/set_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("battlePartyMembers", param.battlePartyMembers);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartysetall(PlayerBattlePartyMember[] battlePartyMembers, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/set_all", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("battlePartyMembers", battlePartyMembers);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartygetall(Battlepartygetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Battlepartygetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/battle_party/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Masterorbgetall(Masterorbgetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Masterorbgetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questgetall(Questgetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questgetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questread(Questread param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest/read", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("readGroups", param.readGroups);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questread(PlayerReadGroup[] readGroups, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest/read", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("readGroups", readGroups);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogretry(Questlogretry param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/retry", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", param.orderReceiveId);
        www.Add("questData", param.questData);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogretry(long orderReceiveId, string questData, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/retry", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", orderReceiveId);
        www.Add("questData", questData);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogsave(Questlogsave param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/save", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", param.orderReceiveId);
        www.Add("questData", param.questData);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogsave(long orderReceiveId, string questData, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/save", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", orderReceiveId);
        www.Add("questData", questData);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogload(Questlogload param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/load", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogload(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/load", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogreset(Questlogreset param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/reset", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogreset(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/reset", MeigewwwParam.eRequestMethod.Post, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentgetall(Presentgetall param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentgetall(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get_all", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentreceived(Presentreceived param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get_received", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentreceived(MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get_received", MeigewwwParam.eRequestMethod.Get, callback);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentget(Presentget param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("managedPresentId", param.managedPresentId);
        www.Add("stepCode", param.stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Presentget(string managedPresentId, int stepCode, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/present/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("managedPresentId", managedPresentId);
        www.Add("stepCode", stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Staminaadd(Staminaadd param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/stamina/add", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("type", param.type);
        www.Add("num", param.num);
        www.Add("itemId", param.itemId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Staminaadd(int type, int num, int itemId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/stamina/add", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("type", type);
        www.Add("num", num);
        www.Add("itemId", itemId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogorder(Questlogorder param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/order", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("questId", param.questId);
        www.Add("managedBattlePartyId", param.managedBattlePartyId);
        www.Add("supportCharacterId", param.supportCharacterId);
        www.Add("questNpcId", param.questNpcId);
        www.Add("questData", param.questData);
        www.Add("questStamina", param.questStamina);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogorder(int questId, long managedBattlePartyId, long supportCharacterId, long questNpcId, string questData, int questStamina, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/order", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("questId", questId);
        www.Add("managedBattlePartyId", managedBattlePartyId);
        www.Add("supportCharacterId", supportCharacterId);
        www.Add("questNpcId", questNpcId);
        www.Add("questData", questData);
        www.Add("questStamina", questStamina);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogcomplete(Questlogcomplete param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", param.orderReceiveId);
        www.Add("state", param.state);
        www.Add("clearRank", param.clearRank);
        www.Add("skillExps", param.skillExps);
        www.Add("weaponSkillExps", param.weaponSkillExps);
        www.Add("friendUseNum", param.friendUseNum);
        www.Add("masterSkillUseNum", param.masterSkillUseNum);
        www.Add("uniqueSkillUseNum", param.uniqueSkillUseNum);
        www.Add("stepCode", param.stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Questlogcomplete(long orderReceiveId, int state, int clearRank, string skillExps, string weaponSkillExps, int friendUseNum, int masterSkillUseNum, int uniqueSkillUseNum, int stepCode, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/quest_log/complete", MeigewwwParam.eRequestMethod.Post, callback);
        www.Add("orderReceiveId", orderReceiveId);
        www.Add("state", state);
        www.Add("clearRank", clearRank);
        www.Add("skillExps", skillExps);
        www.Add("weaponSkillExps", weaponSkillExps);
        www.Add("friendUseNum", friendUseNum);
        www.Add("masterSkillUseNum", masterSkillUseNum);
        www.Add("uniqueSkillUseNum", uniqueSkillUseNum);
        www.Add("stepCode", stepCode);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Pointeventget(Pointeventget param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/point_event/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("pointEventId", param.pointEventId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Pointeventget(long pointEventId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/point_event/get", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("pointEventId", pointEventId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Pointeventreceivetotalreward(Pointeventreceivetotalreward param, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/point_event/receive_total_reward", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("pointEventId", param.pointEventId);
        www.SetPostCrypt();
        return www;
    }

    public static MeigewwwParam Pointeventreceivetotalreward(long pointEventId, MeigewwwParam.Callback callback = null) {
        MeigewwwParam www = new MeigewwwParam();
        www.Init("player/point_event/receive_total_reward", MeigewwwParam.eRequestMethod.Get, callback);
        www.Add("pointEventId", pointEventId);
        www.SetPostCrypt();
        return www;
    }
}
