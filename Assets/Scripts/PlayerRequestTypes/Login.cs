﻿namespace PlayerRequestTypes {
    public class Login {
        public string uuid;
        public string accessToken;
        public int platform;
        public string appVersion;
        public int authVoided;
    }
}
