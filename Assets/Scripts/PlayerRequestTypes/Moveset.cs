﻿namespace PlayerRequestTypes {
    public class Moveset {
        public string moveCode;
        public string movePassword;
        public string uuid;
        public int platform;
    }
}
