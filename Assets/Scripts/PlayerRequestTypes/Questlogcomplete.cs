﻿namespace PlayerRequestTypes {
    public class Questlogcomplete {
        public long orderReceiveId;
        public int state;
        public int clearRank;
        public string skillExps;
        public string weaponSkillExps;
        public int friendUseNum;
        public int masterSkillUseNum;
        public int uniqueSkillUseNum;
        public int stepCode;
    }
}
