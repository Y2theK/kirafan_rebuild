﻿namespace PlayerRequestTypes {
    public class Questlogorder {
        public int questId;
        public long managedBattlePartyId;
        public long supportCharacterId;
        public long questNpcId;
        public string questData;
        public int questStamina;
    }
}
