﻿namespace PlayerRequestTypes {
    public class Set {
        public string name;
        public string comment;
        public string linkId;
        public long[] managedFavoriteCharacterIds;
    }
}
