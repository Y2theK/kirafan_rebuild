﻿namespace PlayerRequestTypes {
    public class Weaponskillup {
        public long managedWeaponId;
        public int[] itemIds;
        public int[] amounts;
    }
}
