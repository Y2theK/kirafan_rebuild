﻿using Meige;
using PlayerResponseTypes;
using System.Collections.Generic;
using WWWTypes;

public static class PlayerResponse {
    public static Signup Signup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Signup>(param, dialogType, acceptableResultCodes);
    }

    public static Login Login(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Login>(param, dialogType, acceptableResultCodes);
    }

    public static Setpushtoken Setpushtoken(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Setpushtoken>(param, dialogType, acceptableResultCodes);
    }

    public static Moveget Moveget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Moveget>(param, dialogType, acceptableResultCodes);
    }

    public static Moveset Moveset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Moveset>(param, dialogType, acceptableResultCodes);
    }

    public static Linkmoveset Linkmoveset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Linkmoveset>(param, dialogType, acceptableResultCodes);
    }

    public static Reset Reset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Reset>(param, dialogType, acceptableResultCodes);
    }

    public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
    }

    public static GeneralFlagSave GeneralFlagSave(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<GeneralFlagSave>(param, dialogType, acceptableResultCodes);
    }

    public static Getbadgecount Getbadgecount(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Getbadgecount>(param, dialogType, acceptableResultCodes);
    }

    public static Resumeget Resumeget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Resumeget>(param, dialogType, acceptableResultCodes);
    }

    public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
    }

    public static Setpushnotification Setpushnotification(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Setpushnotification>(param, dialogType, acceptableResultCodes);
    }

    public static Setage Setage(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Setage>(param, dialogType, acceptableResultCodes);
    }

    public static Loginbonus Loginbonus(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Loginbonus>(param, dialogType, acceptableResultCodes);
    }

    public static Itemgetsummary Itemgetsummary(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Itemgetsummary>(param, dialogType, acceptableResultCodes);
    }

    public static Itemsale Itemsale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Itemsale>(param, dialogType, acceptableResultCodes);
    }

    public static Weapongetall Weapongetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weapongetall>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponmake Weaponmake(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponmake>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponupgrade Weaponupgrade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponupgrade>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponsale Weaponsale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponsale>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponreceive Weaponreceive(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponreceive>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponevolution Weaponevolution(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponevolution>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponlimitadd Weaponlimitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponlimitadd>(param, dialogType, acceptableResultCodes);
    }

    public static Weaponskillup Weaponskillup(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Weaponskillup>(param, dialogType, acceptableResultCodes);
    }

    public static Charactergetall Charactergetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Charactergetall>(param, dialogType, acceptableResultCodes);
    }

    public static Charactersetshown Charactersetshown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Charactersetshown>(param, dialogType, acceptableResultCodes);
    }

    public static Characterlimitbreak Characterlimitbreak(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Characterlimitbreak>(param, dialogType, acceptableResultCodes);
    }

    public static Characterupgrade Characterupgrade(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Characterupgrade>(param, dialogType, acceptableResultCodes);
    }

    public static Characterevolution Characterevolution(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Characterevolution>(param, dialogType, acceptableResultCodes);
    }

    public static Charactersetview Charactersetview(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Charactersetview>(param, dialogType, acceptableResultCodes);
    }

    public static Characterresetallview Characterresetallview(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Characterresetallview>(param, dialogType, acceptableResultCodes);
    }

    public static Battlepartysetname Battlepartysetname(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Battlepartysetname>(param, dialogType, acceptableResultCodes);
    }

    public static Battlepartysetall Battlepartysetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Battlepartysetall>(param, dialogType, acceptableResultCodes);
    }

    public static Battlepartygetall Battlepartygetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Battlepartygetall>(param, dialogType, acceptableResultCodes);
    }

    public static Masterorbgetall Masterorbgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Masterorbgetall>(param, dialogType, acceptableResultCodes);
    }

    public static Questgetall Questgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questgetall>(param, dialogType, acceptableResultCodes);
    }

    public static Questread Questread(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questread>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogretry Questlogretry(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogretry>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogsave Questlogsave(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogsave>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogload Questlogload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogload>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogreset Questlogreset(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogreset>(param, dialogType, acceptableResultCodes);
    }

    public static Presentgetall Presentgetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Presentgetall>(param, dialogType, acceptableResultCodes);
    }

    public static Presentreceived Presentreceived(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Presentreceived>(param, dialogType, acceptableResultCodes);
    }

    public static Presentget Presentget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Presentget>(param, dialogType, acceptableResultCodes);
    }

    public static Staminaadd Staminaadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Staminaadd>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogorder Questlogorder(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogorder>(param, dialogType, acceptableResultCodes);
    }

    public static Questlogcomplete Questlogcomplete(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Questlogcomplete>(param, dialogType, acceptableResultCodes);
    }

    public static Pointeventget Pointeventget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Pointeventget>(param, dialogType, acceptableResultCodes);
    }

    public static Pointeventreceivetotalreward Pointeventreceivetotalreward(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null) {
        return ResponseCommon.Func<Pointeventreceivetotalreward>(param, dialogType, acceptableResultCodes);
    }
}
