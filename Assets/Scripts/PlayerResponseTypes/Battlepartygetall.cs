﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Battlepartygetall : CommonResponse {
        public PlayerBattleParty[] managedBattleParties;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedBattleParties == null) ? string.Empty : managedBattleParties.ToString();
            return str;
        }
    }
}
