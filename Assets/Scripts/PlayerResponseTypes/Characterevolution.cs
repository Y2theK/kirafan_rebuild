﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Characterevolution : CommonResponse {
        public PlayerItemSummary[] itemSummary;
        public PlayerCharacter managedCharacter;
        public long gold;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedCharacter == null) ? string.Empty : managedCharacter.ToString();
            str += gold.ToString();
            return str;
        }
    }
}
