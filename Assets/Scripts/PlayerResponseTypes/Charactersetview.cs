﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Charactersetview : CommonResponse {
        public PlayerCharacter[] managedCharacters;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            return str;
        }
    }
}
