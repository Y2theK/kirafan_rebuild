﻿using CommonResponseTypes;
using System;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Getall : CommonResponse {
        public Player player;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon[] managedWeapons;
        public PlayerMasterOrb[] managedMasterOrbs;
        public PlayerBattleParty[] managedBattleParties;
        public PlayerFieldPartyMember[] managedFieldPartyMembers;
        public PlayerFavoriteMember[] favoriteMembers;
        public PlayerTown[] managedTowns;
        public PlayerTownFacility[] managedTownFacilities;
        public PlayerRoom[] managedRooms;
        public PlayerRoomObject[] managedRoomObjects;
        public PlayerSupport[] supportCharacters;
        public int[] advIds;
        public int[] tipIds;
        public int stepCode;
        public int friendProposedCount;
        public int presentCount;
        public int trainingCount;
        public long orderReceiveId;
        public string questData;
        public BattleDropItems[] dropItem;
        public BattleOccurEnemy occurEnemy;
        public FriendList supportFriend;
        public SupportCharacter supportCharacter;
        public int flagPush;
        public int flagStamina;
        public int flagUi;
        public DateTime lastMemberAdded;
        public Information[] featuredInformations;
        public int isCloseInfo;
        public int isNewProduct;
        public OfferTitleType[] offerTitleTypes;
        public PlayerAbilityBoard[] managedAbilityBoards;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += (managedMasterOrbs == null) ? string.Empty : managedMasterOrbs.ToString();
            str += (managedBattleParties == null) ? string.Empty : managedBattleParties.ToString();
            str += (managedFieldPartyMembers == null) ? string.Empty : managedFieldPartyMembers.ToString();
            str += (favoriteMembers == null) ? string.Empty : favoriteMembers.ToString();
            str += (managedTowns == null) ? string.Empty : managedTowns.ToString();
            str += (managedTownFacilities == null) ? string.Empty : managedTownFacilities.ToString();
            str += (managedRooms == null) ? string.Empty : managedRooms.ToString();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            str += (supportCharacters == null) ? string.Empty : supportCharacters.ToString();
            str += (advIds == null) ? string.Empty : advIds.ToString();
            str += (tipIds == null) ? string.Empty : tipIds.ToString();
            str += stepCode.ToString();
            str += friendProposedCount.ToString();
            str += presentCount.ToString();
            str += trainingCount.ToString();
            str += orderReceiveId.ToString();
            str += (questData == null) ? string.Empty : questData.ToString();
            str += (dropItem == null) ? string.Empty : dropItem.ToString();
            str += (occurEnemy == null) ? string.Empty : occurEnemy.ToString();
            str += (supportFriend == null) ? string.Empty : supportFriend.ToString();
            str += (supportCharacter == null) ? string.Empty : supportCharacter.ToString();
            str += flagPush.ToString();
            str += flagStamina.ToString();
            str += flagUi.ToString();
            str += lastMemberAdded.ToString();
            str += (featuredInformations == null) ? string.Empty : featuredInformations.ToString();
            str += isCloseInfo.ToString();
            str += isNewProduct.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (managedAbilityBoards == null) ? string.Empty : managedAbilityBoards.ToString();
            return str;
        }
    }
}
