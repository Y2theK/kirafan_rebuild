﻿using CommonResponseTypes;

namespace PlayerResponseTypes {
    public class Getbadgecount : CommonResponse {
        public int friendProposedCount;
        public int presentCount;
        public int trainingCount;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += friendProposedCount.ToString();
            str += presentCount.ToString();
            str += trainingCount.ToString();
            return str;
        }
    }
}
