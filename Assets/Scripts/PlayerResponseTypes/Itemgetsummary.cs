﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Itemgetsummary : CommonResponse {
        public PlayerItemSummary[] itemSummary;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            return str;
        }
    }
}
