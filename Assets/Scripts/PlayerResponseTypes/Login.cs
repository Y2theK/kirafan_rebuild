﻿using CommonResponseTypes;

namespace PlayerResponseTypes {
    public class Login : CommonResponse {
        public string sessionId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (sessionId == null) ? string.Empty : sessionId.ToString();
            return str;
        }
    }
}
