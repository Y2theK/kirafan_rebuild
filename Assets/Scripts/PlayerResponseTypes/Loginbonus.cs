﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Loginbonus : CommonResponse {
        public Bonus[] bonuses;
        public int missionChange;
        public long gainKirara;
        public long kirara;
        public RegularPremium[] regularPremium;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (bonuses == null) ? string.Empty : bonuses.ToString();
            str += missionChange.ToString();
            str += gainKirara.ToString();
            str += kirara.ToString();
            str += (regularPremium == null) ? string.Empty : regularPremium.ToString();
            return str;
        }
    }
}
