﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Masterorbgetall : CommonResponse {
        public PlayerMasterOrb[] managedMasterOrbs;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedMasterOrbs == null) ? string.Empty : managedMasterOrbs.ToString();
            return str;
        }
    }
}
