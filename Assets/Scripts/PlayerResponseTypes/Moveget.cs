﻿using CommonResponseTypes;
using System;

namespace PlayerResponseTypes {
    public class Moveget : CommonResponse {
        public string moveCode;
        public DateTime moveDeadline;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (moveCode == null) ? string.Empty : moveCode.ToString();
            str += moveDeadline.ToString();
            return str;
        }
    }
}
