﻿using CommonResponseTypes;

namespace PlayerResponseTypes {
    public class Moveset : CommonResponse {
        public long playerId;
        public string accessToken;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += playerId.ToString();
            str += (accessToken == null) ? string.Empty : accessToken.ToString();
            return str;
        }
    }
}
