﻿using CommonResponseTypes;
using System;

namespace PlayerResponseTypes {
    public class Pointeventget : CommonResponse {
        public long totalPoint;
        public long playerTotalPoint;
        public long nextRewardNeedTotalPoint;
        public bool canReceiveTotalReward;
        public DateTime expiredAt;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += totalPoint.ToString();
            str += playerTotalPoint.ToString();
            str += nextRewardNeedTotalPoint.ToString();
            str += canReceiveTotalReward.ToString();
            str += expiredAt.ToString();
            return str;
        }
    }
}
