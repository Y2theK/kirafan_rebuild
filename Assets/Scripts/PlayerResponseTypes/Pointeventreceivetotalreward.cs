﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Pointeventreceivetotalreward : CommonResponse {
        public PointEventTotalReward[] receivedTotalRewards;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (receivedTotalRewards == null) ? string.Empty : receivedTotalRewards.ToString();
            return str;
        }
    }
}
