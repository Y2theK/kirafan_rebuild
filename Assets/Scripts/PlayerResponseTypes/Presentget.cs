﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Presentget : CommonResponse {
        public PlayerPresent[] received;
        public Player player;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon[] managedWeapons;
        public PlayerMasterOrb[] managedMasterOrbs;
        public PlayerTownFacility[] managedTownFacilities;
        public PlayerRoomObject[] managedRoomObjects;
        public ArousalResult[] arousalResults;
        public OfferTitleType[] offerTitleTypes;
        public PlayerOffer[] offers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (received == null) ? string.Empty : received.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += (managedMasterOrbs == null) ? string.Empty : managedMasterOrbs.ToString();
            str += (managedTownFacilities == null) ? string.Empty : managedTownFacilities.ToString();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            str += (arousalResults == null) ? string.Empty : arousalResults.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += (offers == null) ? string.Empty : offers.ToString();
            return str;
        }
    }
}
