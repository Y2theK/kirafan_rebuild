﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Presentgetall : CommonResponse {
        public PlayerPresent[] presents;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (presents == null) ? string.Empty : presents.ToString();
            return str;
        }
    }
}
