﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Presentreceived : CommonResponse {
        public PlayerPresent[] received;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (received == null) ? string.Empty : received.ToString();
            return str;
        }
    }
}
