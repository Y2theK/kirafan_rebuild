﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Questgetall : CommonResponse {
        public PlayerQuest[] quests;
        public PlayerQuest[] questPart2s;
        public PlayerEventQuest[] eventQuests;
        public PlayerCharacterQuest[] characterQuests;
        public PlayerOfferQuest[] playerOfferQuests;
        public PlayerReadGroup[] readGroups;
        public EventQuestPeriod[] eventQuestPeriods;
        public QuestStaminaReduction[] questStaminaReductions;
        public int[] lastPlayedChapterQuestIds;
        public int[] playedOpenChapterIds;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (quests == null) ? string.Empty : quests.ToString();
            str += (questPart2s == null) ? string.Empty : questPart2s.ToString();
            str += (eventQuests == null) ? string.Empty : eventQuests.ToString();
            str += (characterQuests == null) ? string.Empty : characterQuests.ToString();
            str += (playerOfferQuests == null) ? string.Empty : playerOfferQuests.ToString();
            str += (readGroups == null) ? string.Empty : readGroups.ToString();
            str += (eventQuestPeriods == null) ? string.Empty : eventQuestPeriods.ToString();
            str += (questStaminaReductions == null) ? string.Empty : questStaminaReductions.ToString();
            str += (lastPlayedChapterQuestIds == null) ? string.Empty : lastPlayedChapterQuestIds.ToString();
            str += (playedOpenChapterIds == null) ? string.Empty : playedOpenChapterIds.ToString();
            return str;
        }
    }
}
