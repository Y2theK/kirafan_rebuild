﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Questlogcomplete : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerCharacter[] managedCharacters;
        public PlayerNamedType[] managedNamedTypes;
        public PlayerWeapon[] managedWeapons;
        public PlayerMasterOrb managedMasterOrb;
        public QuestReward rewardFirst;
        public int isFirstComplete;
        public string questNoticeTitle;
        public string questNoticeMessage;
        public PointEventResult pointEventResult;
        public QuestReward rewardGroup;
        public QuestReward rewardComp;
        public OfferTitleType[] offerTitleTypes;
        public int isContinuous;
        public PlayerOffer[] offers;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedCharacters == null) ? string.Empty : managedCharacters.ToString();
            str += (managedNamedTypes == null) ? string.Empty : managedNamedTypes.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += (managedMasterOrb == null) ? string.Empty : managedMasterOrb.ToString();
            str += (rewardFirst == null) ? string.Empty : rewardFirst.ToString();
            str += isFirstComplete.ToString();
            str += (questNoticeTitle == null) ? string.Empty : questNoticeTitle.ToString();
            str += (questNoticeMessage == null) ? string.Empty : questNoticeMessage.ToString();
            str += (pointEventResult == null) ? string.Empty : pointEventResult.ToString();
            str += (rewardGroup == null) ? string.Empty : rewardGroup.ToString();
            str += (rewardComp == null) ? string.Empty : rewardComp.ToString();
            str += (offerTitleTypes == null) ? string.Empty : offerTitleTypes.ToString();
            str += isContinuous.ToString();
            str += (offers == null) ? string.Empty : offers.ToString();
            return str;
        }
    }
}
