﻿using CommonResponseTypes;

namespace PlayerResponseTypes {
    public class Questlogload : CommonResponse {
        public long orderReceiveId;
        public string questData;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += orderReceiveId.ToString();
            str += (questData == null) ? string.Empty : questData.ToString();
            return str;
        }
    }
}
