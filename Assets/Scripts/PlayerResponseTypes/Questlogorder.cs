﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Questlogorder : CommonResponse {
        public long orderReceiveId;
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public BattleDropItems[] dropItem;
        public BattleOccurEnemy occurEnemy;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += orderReceiveId.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (dropItem == null) ? string.Empty : dropItem.ToString();
            str += (occurEnemy == null) ? string.Empty : occurEnemy.ToString();
            return str;
        }
    }
}
