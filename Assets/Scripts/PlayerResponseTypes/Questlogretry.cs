﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Questlogretry : CommonResponse {
        public long orderReceiveId;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += orderReceiveId.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
