﻿using CommonResponseTypes;

namespace PlayerResponseTypes {
    public class Resumeget : CommonResponse {
        public long stamina;
        public long staminaMax;
        public int recastTime;
        public int recastTimeMax;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += stamina.ToString();
            str += staminaMax.ToString();
            str += recastTime.ToString();
            str += recastTimeMax.ToString();
            return str;
        }
    }
}
