﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Staminaadd : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            return str;
        }
    }
}
