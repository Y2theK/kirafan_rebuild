﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weapongetall : CommonResponse {
        public PlayerWeapon[] managedWeapons;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            return str;
        }
    }
}
