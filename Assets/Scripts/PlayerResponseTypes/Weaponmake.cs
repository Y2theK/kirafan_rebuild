﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weaponmake : CommonResponse {
        public Player player;
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon[] managedWeapons;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            return str;
        }
    }
}
