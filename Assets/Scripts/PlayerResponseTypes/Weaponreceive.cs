﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weaponreceive : CommonResponse {
        public PlayerWeapon[] managedWeapons;
        public int weaponLimit;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            str += weaponLimit.ToString();
            return str;
        }
    }
}
