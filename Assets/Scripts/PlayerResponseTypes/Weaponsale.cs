﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weaponsale : CommonResponse {
        public Player player;
        public PlayerWeapon[] managedWeapons;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (managedWeapons == null) ? string.Empty : managedWeapons.ToString();
            return str;
        }
    }
}
