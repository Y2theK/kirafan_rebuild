﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weaponskillup : CommonResponse {
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon managedWeapon;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapon == null) ? string.Empty : managedWeapon.ToString();
            return str;
        }
    }
}
