﻿using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes {
    public class Weaponupgrade : CommonResponse {
        public PlayerItemSummary[] itemSummary;
        public PlayerWeapon managedWeapon;
        public long gold;
        public int successType;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            str += (managedWeapon == null) ? string.Empty : managedWeapon.ToString();
            str += gold.ToString();
            str += successType.ToString();
            return str;
        }
    }
}
