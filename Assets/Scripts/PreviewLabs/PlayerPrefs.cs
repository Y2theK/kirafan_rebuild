﻿using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace PreviewLabs {
    public static class PlayerPrefs {
        private const string PARAMETERS_SEPERATOR = ";";
        private const string KEY_VALUE_SEPERATOR = ":";

        private static readonly Hashtable playerPrefsHashtable = new Hashtable();
        private static bool hashTableChanged = false;
        private static string serializedOutput = string.Empty;
        private static string serializedInput = string.Empty;
        private static string[] seperators = new string[] {
            PARAMETERS_SEPERATOR,
            KEY_VALUE_SEPERATOR
        };
        private static readonly string fileName = Application.persistentDataPath + "/PlayerPrefs.txt";
        private static readonly string secureFileName = Application.persistentDataPath + "/BundleVersion.bin";
        private static byte[] bytes = Encoding.ASCII.GetBytes("iw3q" + SystemInfo.deviceUniqueIdentifier.Substring(0, 4));
        private static bool wasEncrypted = false;
        private static bool securityModeEnabled = true;

        static PlayerPrefs() {
            StreamReader reader = null;
            if (File.Exists(secureFileName)) {
                reader = new StreamReader(secureFileName);
                wasEncrypted = true;
                serializedInput = Decrypt(reader.ReadToEnd());
                if (!securityModeEnabled) {
                    hashTableChanged = true;
                }
            } else if (File.Exists(fileName)) {
                reader = new StreamReader(fileName);
                serializedInput = reader.ReadToEnd();
                if (securityModeEnabled) {
                    hashTableChanged = true;
                }
            }
            if (!string.IsNullOrEmpty(serializedInput)) {
                if (serializedInput.Length > 0 && serializedInput[serializedInput.Length - 1] == '\n') {
                    serializedInput = serializedInput.Substring(0, serializedInput.Length - 1);
                    if (serializedInput.Length > 0 && serializedInput[serializedInput.Length - 1] == '\r') {
                        serializedInput = serializedInput.Substring(0, serializedInput.Length - 1);
                    }
                }
                Deserialize();
            }
            if (reader != null) {
                reader.Close();
            }
            Flush();
        }

        public static void Setup() { }

        public static bool HasKey(string key) {
            return playerPrefsHashtable.ContainsKey(key);
        }

        public static void SetString(string key, string value) {
            if (!playerPrefsHashtable.ContainsKey(key)) {
                playerPrefsHashtable.Add(key, value);
            } else {
                playerPrefsHashtable[key] = value;
            }
            hashTableChanged = true;
        }

        public static void SetInt(string key, int value) {
            if (!playerPrefsHashtable.ContainsKey(key)) {
                playerPrefsHashtable.Add(key, value);
            } else {
                playerPrefsHashtable[key] = value;
            }
            hashTableChanged = true;
        }

        public static void SetFloat(string key, float value) {
            if (!playerPrefsHashtable.ContainsKey(key)) {
                playerPrefsHashtable.Add(key, value);
            } else {
                playerPrefsHashtable[key] = value;
            }
            hashTableChanged = true;
        }

        public static void SetBool(string key, bool value) {
            if (!playerPrefsHashtable.ContainsKey(key)) {
                playerPrefsHashtable.Add(key, value);
            } else {
                playerPrefsHashtable[key] = value;
            }
            hashTableChanged = true;
        }

        public static void SetLong(string key, long value) {
            if (!playerPrefsHashtable.ContainsKey(key)) {
                playerPrefsHashtable.Add(key, value);
            } else {
                playerPrefsHashtable[key] = value;
            }
            hashTableChanged = true;
        }

        public static string GetString(string key) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return playerPrefsHashtable[key].ToString();
            }
            return null;
        }

        public static string GetString(string key, string defaultValue) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return playerPrefsHashtable[key].ToString();
            }
            playerPrefsHashtable.Add(key, defaultValue);
            hashTableChanged = true;
            return defaultValue;
        }

        public static int GetInt(string key) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (int)playerPrefsHashtable[key];
            }
            return 0;
        }

        public static int GetInt(string key, int defaultValue) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (int)playerPrefsHashtable[key];
            }
            playerPrefsHashtable.Add(key, defaultValue);
            hashTableChanged = true;
            return defaultValue;
        }

        public static long GetLong(string key) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (long)playerPrefsHashtable[key];
            }
            return 0L;
        }

        public static long GetLong(string key, long defaultValue) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (long)playerPrefsHashtable[key];
            }
            playerPrefsHashtable.Add(key, defaultValue);
            hashTableChanged = true;
            return defaultValue;
        }

        public static float GetFloat(string key) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (float)playerPrefsHashtable[key];
            }
            return 0f;
        }

        public static float GetFloat(string key, float defaultValue) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (float)playerPrefsHashtable[key];
            }
            playerPrefsHashtable.Add(key, defaultValue);
            hashTableChanged = true;
            return defaultValue;
        }

        public static bool GetBool(string key) {
            return playerPrefsHashtable.ContainsKey(key) && (bool)playerPrefsHashtable[key];
        }

        public static bool GetBool(string key, bool defaultValue) {
            if (playerPrefsHashtable.ContainsKey(key)) {
                return (bool)playerPrefsHashtable[key];
            }
            playerPrefsHashtable.Add(key, defaultValue);
            hashTableChanged = true;
            return defaultValue;
        }

        public static void DeleteKey(string key) {
            playerPrefsHashtable.Remove(key);
        }

        public static void DeleteAll() {
            playerPrefsHashtable.Clear();
        }

        public static bool WasReadPlayerPrefsFileEncrypted() {
            return wasEncrypted;
        }

        public static void EnableEncryption(bool enabled) {
            securityModeEnabled = enabled;
        }

        public static void Flush() {
            if (hashTableChanged) {
                Serialize();
                string value = securityModeEnabled ? Encrypt(serializedOutput) : serializedOutput;
                StreamWriter writer = File.CreateText(securityModeEnabled ? secureFileName : fileName);
                File.Delete(securityModeEnabled ? fileName : secureFileName);
                if (writer == null) {
                    Debug.LogWarning("PlayerPrefs::Flush() opening file for writing failed: " + fileName);
                    return;
                }
                writer.Write(value);
                writer.Close();
                serializedOutput = string.Empty;
            }
        }

        private static void Serialize() {
            IDictionaryEnumerator enumerator = playerPrefsHashtable.GetEnumerator();
            StringBuilder sb = new StringBuilder();
            bool firstEntry = true;
            while (enumerator.MoveNext()) {
                if (!firstEntry) {
                    sb.Append(" ");
                    sb.Append(PARAMETERS_SEPERATOR);
                    sb.Append(" ");
                }
                sb.Append(EscapeNonSeperators(enumerator.Key.ToString(), seperators));
                sb.Append(" ");
                sb.Append(KEY_VALUE_SEPERATOR);
                sb.Append(" ");
                sb.Append(EscapeNonSeperators(enumerator.Value.ToString(), seperators));
                sb.Append(" ");
                sb.Append(KEY_VALUE_SEPERATOR);
                sb.Append(" ");
                sb.Append(enumerator.Value.GetType());
                firstEntry = false;
            }
            serializedOutput = sb.ToString();
        }

        private static void Deserialize() {
            string[] entries = serializedInput.Split($" {PARAMETERS_SEPERATOR} ", StringSplitOptions.RemoveEmptyEntries);
            foreach (string entry in entries) {
                string[] kvt = entry.Split($" {KEY_VALUE_SEPERATOR} ");
                string key = DeEscapeNonSeperators(kvt[0], seperators);
                object value = GetTypeValue(kvt[2], DeEscapeNonSeperators(kvt[1], seperators));
                playerPrefsHashtable.Add(key, value);
                if (kvt.Length > 3) {
                    Debug.LogWarning("PlayerPrefs::Deserialize() parameterContent has " + kvt.Length + " elements");
                }
            }
        }

        public static string EscapeNonSeperators(string inputToEscape, string[] seperators) {
            inputToEscape = inputToEscape.Replace(@"\", @"\\");
            for (int i = 0; i < seperators.Length; i++) {
                inputToEscape = inputToEscape.Replace(seperators[i], @"\" + seperators[i]);
            }
            return inputToEscape;
        }

        public static string DeEscapeNonSeperators(string inputToDeEscape, string[] seperators) {
            for (int i = 0; i < seperators.Length; i++) {
                inputToDeEscape = inputToDeEscape.Replace(@"\" + seperators[i], seperators[i]);
            }
            inputToDeEscape = inputToDeEscape.Replace(@"\\", @"\");
            return inputToDeEscape;
        }

        private static string Encrypt(string originalString) {
            if (string.IsNullOrEmpty(originalString)) {
                return string.Empty;
            }
            string result = string.Empty;
            using (var des = new DESCryptoServiceProvider())
            using (var stream = new MemoryStream())
            using (var transform = des.CreateEncryptor(bytes, bytes))
            using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            using (var writer = new StreamWriter(cryptoStream)) {
                writer.Write(originalString);
                writer.Flush();
                cryptoStream.FlushFinalBlock();
                writer.Flush();
                result = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length);
            }
            return result;
        }

        private static string Decrypt(string cryptedString) {
            if (string.IsNullOrEmpty(cryptedString)) {
                return string.Empty;
            }
            string result = string.Empty;
            using (var des = new DESCryptoServiceProvider())
            using (var stream = new MemoryStream(Convert.FromBase64String(cryptedString)))
            using (var transform = des.CreateDecryptor(bytes, bytes))
            using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read))
            using (var reader = new StreamReader(cryptoStream)) {
                result = reader.ReadToEnd();
            }
            return result;
        }

        private static object GetTypeValue(string typeName, string value) {
            if (typeName == "System.String") {
                return value.ToString();
            }
            if (typeName == "System.Int32") {
                return Convert.ToInt32(value);
            }
            if (typeName == "System.Boolean") {
                return Convert.ToBoolean(value);
            }
            if (typeName == "System.Single") {
                return Convert.ToSingle(value);
            }
            if (typeName == "System.Int64") {
                return Convert.ToInt64(value);
            }
            Debug.LogError("Unsupported type: " + typeName);
            return null;
        }
    }
}
