﻿using CommonLogic;
using UnityEngine;

public class ProjDepend {
    private const GameScreen.ASPECT_ADJUST_TYPE m_aspectAdjustType = GameScreen.ASPECT_ADJUST_TYPE.CINEMA_SCOPE;
    public const int AB_EncryptPasswordCount = 16;
    public const bool BUNDLEVERSION_ISENCRYPT = true;

    private static readonly Vector2 m_uiSpaceSize = new Vector2(320, 480);
    public static int ASSETBUNDLE_MAX_LOADING_COUNT = 200;
    public static int ASSETBUNDLE_MAX_DOWNLOADING_COUNT = 5;
    public static readonly byte[] AB_EK = new byte[32] {
        0xd2, 0x87, 0x4d, 0xb4, 0xb5, 0x64, 0x26, 0x77, 0x03, 0xf6, 0x29, 0x58, 0x45, 0x7d, 0x45, 0x55,
        0xb2, 0x25, 0xc4, 0x1f, 0xfc, 0x35, 0xec, 0x5e, 0x55, 0x07, 0x84, 0xf5, 0x58, 0x7f, 0x2d, 0xae
    };
    public static readonly int CRYPT_AES_CBC_LENGTH = 128;
    public static readonly bool RH_CRC_IS_HEXADECIMAL = true;
    private static readonly string m_RHKBinary = "WUwOQrIv+yR3bKUeV+0EcEA8AA/zQLRBFYLyn7+oKNo=";
    private static readonly string m_SIKBinary = "RSi4ahndfzAQAwrpkOo5DLqnzW3m3GqsVsu/5ptHjjw=";
    private static readonly string m_RKBinary = "hIzvMLz9oyLwiaARClFSnw==";
    private static readonly byte[] m_ABKBinary = new byte[16] { 0x2a, 0x9c, 0x5e, 0xa0, 0xb6, 0x05, 0x5c, 0x06, 0x5b, 0x39, 0x07, 0x12, 0x8f, 0xda, 0x60, 0x13 };
    private static readonly byte[] m_ACIVBinary = new byte[16] { 0x78, 0x58, 0x42, 0x38, 0x64, 0x7a, 0x68, 0x4a, 0x45, 0x63, 0x32, 0x53, 0x4c, 0x42, 0x45, 0x61 };
    private static readonly string m_WHCRYBinary = "UkD+2ctgdL7N6M7e23WqUdfi+eFhJp6RmVEq5wE9o7c=";
    private static readonly string m_WHRCRYBinary = "pmaA3Pvg0GT+ueUtIIio7nHJ76OXApKps6sABAFHOYs=";
    private static readonly string m_WHCRYSCBinary = "IwKFD9P6Q/37dyka+oBd4A==";
    private static readonly string m_WHRTIDCBinary = "tIi4LRlqBjGc3PIQ8DmS4zXhFb/ZfaKIYd2g+F0leDs=";
    private static readonly string m_CKHAKBinary = "awfQbH3PJnyEWUjx";
    private static readonly string m_CKHAIVBinary = "2ycRr4zbm3dDcAXr";
    private static readonly string m_RHS_APL_Binary = "uAD0Fj8LNzdFIr3ztNwv8DSfcnjBmJukA6H0mG6kKTyM0CmKPJTh52lFfpDyG1203BkQ8bHKOC+v2FTWy+u6iRQDnIy5FE9l6RrLl4wuvug=";
    public static string SERVER_APPLYING_URL = "";
    public static readonly string SERVER_URL = "https://krr-prd.star-api.com/api/";
    private static readonly string m_RHS_Binary = "uAD0Fj8LNzdFIr3ztNwv8DSfcnjBmJukA6H0mG6kKTyM0CmKPJTh52lFfpDyG1203BkQ8bHKOC+v2FTWy+u6iRQDnIy5FE9l6RrLl4wuvug=";

    public static GameScreen.ASPECT_ADJUST_TYPE aspectAdjustType => m_aspectAdjustType;
    public static Vector2 uiSpaceSize => m_uiSpaceSize;
    public static byte[] CRYPT_AES_CBC_KEY => m_ABKBinary;
    public static byte[] CACIV => m_ACIVBinary;
    public static string CKHAK => m_CKHAKBinary;
    public static string CKHAIV => m_CKHAIVBinary;
    public static string WHERHK => new LogicCalcKey().Calc(m_RHKBinary, CKHAK, CKHAIV);
    public static string WHSIK => new LogicCalcKey().Calc(m_SIKBinary, CKHAK, CKHAIV);
    public static string WHRPK => new LogicCalcKey().Calc(m_RKBinary, CKHAK, CKHAIV);
    public static string WHCRY => new LogicCalcKey().Calc(m_WHCRYBinary, CKHAK, CKHAIV);
    public static string WHRCRY => new LogicCalcKey().Calc(m_WHRCRYBinary, CKHAK, CKHAIV);
    public static string WHCRYSC => new LogicCalcKey().Calc(m_WHCRYSCBinary, CKHAK, CKHAIV);
    public static string WHRTID => new LogicCalcKey().Calc(m_WHRTIDCBinary, CKHAK, CKHAIV);
    public static string REQUESTHASH_SECRET_APPLYING => new LogicCalcKey().Calc(m_RHS_APL_Binary, CKHAK, CKHAIV);
    public static string REQUESTHASH_SECRET => new LogicCalcKey().Calc(m_RHS_Binary, CKHAK, CKHAIV);

    public static int GetMaskFromLayer(eLayer id) {
        return 1 << (int)id;
    }

    public enum eRenderQueue {
        eRenderQueue_BG_Higher,
        eRenderQueue_BG_High = 125,
        eRenderQueue_BG = 250,
        eRenderQueue_BG_Low = 375,
        eRenderQueue_BG_Lower = 500,
        eRenderQueue_Opaque_Higher = 625,
        eRenderQueue_Opaque_High = 750,
        eRenderQueue_Opaque = 875,
        eRenderQueue_Opaque_Low = 1000,
        eRenderQueue_Opaque_Lower = 1125,
        eRenderQueue_PunchThrough_Higher = 1250,
        eRenderQueue_PunchThrough_High = 1375,
        eRenderQueue_PunchThrough = 1500,
        eRenderQueue_PunchThrough_Low = 1625,
        eRenderQueue_PunchThrough_Lower = 1750,
        eRenderQueue_AfterPunchThrough_Higher = 1875,
        eRenderQueue_AfterPunchThrough_High = 2000,
        eRenderQueue_AfterPunchThrough = 2125,
        eRenderQueue_AfterPunchThrough_Low = 2250,
        eRenderQueue_AfterPunchThrough_Lower = 2375,
        eRenderQueue_Alpha_Higher = 2501,
        eRenderQueue_Alpha_High = 2626,
        eRenderQueue_Alpha = 2751,
        eRenderQueue_Alpha_Low = 2876,
        eRenderQueue_Alpha_Lower = 3001,
        eRenderQueue_AfterAlpha_Higher = 3126,
        eRenderQueue_AfterAlpha_High = 3251,
        eRenderQueue_AfterAlpha = 3376,
        eRenderQueue_AfterAlpha_Low = 3501,
        eRenderQueue_AfterAlpha_Lower = 3626,
        eRenderQueue_UI_Higher = 3751,
        eRenderQueue_UI_High = 3876,
        eRenderQueue_UI = 4001,
        eRenderQueue_UI_Low = 4126,
        eRenderQueue_UI_Lower = 4251,
        eRenderQueue_FinalPass_Higher = 4376,
        eRenderQueue_FinalPass_High = 4501,
        eRenderQueue_FinalPass = 4626,
        eRenderQueue_FinalPass_Low = 4751,
        eRenderQueue_FinalPass_Lower = 4876,
        eRenderQueue_SystemFont = 5001
    }

    public enum eLayer {
        eBuiltinLayerStart,
        eLayer_Default = 0,
        eLayer_TransparentFX,
        eLayer_IgnoreRaycast,
        eLayer_Blank0,
        eLayer_Water,
        eLayer_Blank1,
        eLayer_Blank2,
        eLayer_Blank3,
        eBuiltinLayer_End = 7,
        eUserLayerStart,
        eLayer_Graphic2D = 8,
        eLayer_Player,
        eLayer_Enemy,
        eLayer_Sample
    }
}
