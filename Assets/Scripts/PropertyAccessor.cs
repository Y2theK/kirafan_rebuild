﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000B4 RID: 180
[Token(Token = "0x200008B")]
[StructLayout(3)]
internal sealed class PropertyAccessor<TTarget, TProperty> : IPropertyAccessor
{
	// Token: 0x06000431 RID: 1073 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003E7")]
	[Address(RVA = "0x1016CB29C", Offset = "0x16CB29C", VA = "0x1016CB29C")]
	public PropertyAccessor(Func<TTarget, TProperty> getter, Action<TTarget, TProperty> setter)
	{
	}

	// Token: 0x06000432 RID: 1074 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003E8")]
	[Address(RVA = "0x1016CB2E0", Offset = "0x16CB2E0", VA = "0x1016CB2E0", Slot = "4")]
	public object GetValue(object target)
	{
		return null;
	}

	// Token: 0x06000433 RID: 1075 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003E9")]
	[Address(RVA = "0x1016CB3DC", Offset = "0x16CB3DC", VA = "0x1016CB3DC", Slot = "5")]
	public void SetValue(object target, object value)
	{
	}

	// Token: 0x06000434 RID: 1076 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60003EA")]
	[Address(RVA = "0x1016CB570", Offset = "0x16CB570", VA = "0x1016CB570")]
	public TProperty GetValue(TTarget target)
	{
		return null;
	}

	// Token: 0x06000435 RID: 1077 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003EB")]
	[Address(RVA = "0x1016CB5C0", Offset = "0x16CB5C0", VA = "0x1016CB5C0")]
	public void SetValue(TTarget target, TProperty value)
	{
	}

	// Token: 0x04000251 RID: 593
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x4000197")]
	private readonly Func<TTarget, TProperty> getter;

	// Token: 0x04000252 RID: 594
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x4000198")]
	private readonly Action<TTarget, TProperty> setter;
}
