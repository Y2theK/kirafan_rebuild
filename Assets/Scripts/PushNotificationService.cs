﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x02000051 RID: 81
[Token(Token = "0x200002C")]
[StructLayout(3)]
public class PushNotificationService : SingletonMonoBehaviour<PushNotificationService>
{
	// Token: 0x17000056 RID: 86
	// (get) Token: 0x060001CE RID: 462 RVA: 0x00002850 File Offset: 0x00000A50
	[Token(Token = "0x1700004A")]
	public bool isRegist
	{
		[Token(Token = "0x6000187")]
		[Address(RVA = "0x10164247C", Offset = "0x164247C", VA = "0x10164247C")]
		get
		{
			return default(bool);
		}
	}

	// Token: 0x060001CF RID: 463 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000188")]
	[Address(RVA = "0x101642484", Offset = "0x1642484", VA = "0x101642484")]
	public void RegisterDevice(Action onRegistered)
	{
	}

	// Token: 0x060001D0 RID: 464 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000189")]
	[Address(RVA = "0x101642524", Offset = "0x1642524", VA = "0x101642524")]
	private void CheckForDeviceToken()
	{
	}

	// Token: 0x060001D1 RID: 465 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600018A")]
	[Address(RVA = "0x101642640", Offset = "0x1642640", VA = "0x101642640")]
	private void PushRegister(string token)
	{
	}

	// Token: 0x060001D2 RID: 466 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600018B")]
	[Address(RVA = "0x1016426F4", Offset = "0x16426F4", VA = "0x1016426F4")]
	public PushNotificationService()
	{
	}

	// Token: 0x040001F2 RID: 498
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x400014A")]
	private bool m_isRegist;

	// Token: 0x040001F3 RID: 499
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x400014B")]
	private Action m_onRegistered;

	// Token: 0x040001F4 RID: 500
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x400014C")]
	private int count;
}
