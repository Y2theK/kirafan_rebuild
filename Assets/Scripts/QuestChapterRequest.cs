﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using QuestChapterRequestTypes;

// Token: 0x02000073 RID: 115
[Token(Token = "0x200004C")]
[StructLayout(3)]
public static class QuestChapterRequest
{
	// Token: 0x060002D3 RID: 723 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600028C")]
	[Address(RVA = "0x101642778", Offset = "0x1642778", VA = "0x101642778")]
	public static MeigewwwParam Get(Get param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002D4 RID: 724 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600028D")]
	[Address(RVA = "0x10164287C", Offset = "0x164287C", VA = "0x10164287C")]
	public static MeigewwwParam Get(string chapterId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002D5 RID: 725 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600028E")]
	[Address(RVA = "0x10164298C", Offset = "0x164298C", VA = "0x10164298C")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002D6 RID: 726 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600028F")]
	[Address(RVA = "0x101642A50", Offset = "0x1642A50", VA = "0x101642A50")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
