﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using QuestChapterResponseTypes;
using WWWTypes;

// Token: 0x02000097 RID: 151
[Token(Token = "0x2000070")]
[StructLayout(3)]
public static class QuestChapterResponse
{
	// Token: 0x060003BA RID: 954 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000373")]
	[Address(RVA = "0x101642B24", Offset = "0x1642B24", VA = "0x101642B24")]
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003BB RID: 955 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000374")]
	[Address(RVA = "0x101642B8C", Offset = "0x1642B8C", VA = "0x101642B8C")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
