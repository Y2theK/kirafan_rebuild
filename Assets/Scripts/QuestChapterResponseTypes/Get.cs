﻿using CommonResponseTypes;
using WWWTypes;

namespace QuestChapterResponseTypes {
    public class Get : CommonResponse {
        public QuestChapter[] chapters;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (chapters == null) ? string.Empty : chapters.ToString();
            return str;
        }
    }
}
