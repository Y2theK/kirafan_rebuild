﻿using CommonResponseTypes;
using System;
using WWWTypes;

namespace QuestChapterResponseTypes {
    public class GetAll : CommonResponse {
        public QuestChapterArray[] chapters;
        public DateTime? newDispEndAt;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (chapters == null) ? string.Empty : chapters.ToString();
            str += (newDispEndAt == null) ? string.Empty : newDispEndAt.ToString();
            return str;
        }
    }
}
