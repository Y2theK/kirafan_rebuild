﻿using CommonResponseTypes;
using WWWTypes;

namespace QuestResponseTypes {
    public class Getall : CommonResponse {
        public Quest[] quests;
        public EventQuest[] eventQuests;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (quests == null) ? string.Empty : quests.ToString();
            str += (eventQuests == null) ? string.Empty : eventQuests.ToString();
            return str;
        }
    }
}
