﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000D7 RID: 215
[Token(Token = "0x20000A0")]
[StructLayout(3)]
public sealed class RealTimeClock : MonoBehaviour
{
	// Token: 0x0600055B RID: 1371 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000500")]
	[Address(RVA = "0x101643060", Offset = "0x1643060", VA = "0x101643060")]
	private void Awake()
	{
	}

	// Token: 0x0600055C RID: 1372 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000501")]
	[Address(RVA = "0x101643194", Offset = "0x1643194", VA = "0x101643194")]
	private void Update()
	{
	}

	// Token: 0x0600055D RID: 1373 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000502")]
	[Address(RVA = "0x1016430F8", Offset = "0x16430F8", VA = "0x1016430F8")]
	private void SetupDate()
	{
	}

	// Token: 0x0600055E RID: 1374 RVA: 0x000034F8 File Offset: 0x000016F8
	[Token(Token = "0x6000503")]
	[Address(RVA = "0x101643198", Offset = "0x1643198", VA = "0x101643198")]
	public int GetYear()
	{
		return 0;
	}

	// Token: 0x0600055F RID: 1375 RVA: 0x00003510 File Offset: 0x00001710
	[Token(Token = "0x6000504")]
	[Address(RVA = "0x10164323C", Offset = "0x164323C", VA = "0x10164323C")]
	public int GetMonth()
	{
		return 0;
	}

	// Token: 0x06000560 RID: 1376 RVA: 0x00003528 File Offset: 0x00001728
	[Token(Token = "0x6000505")]
	[Address(RVA = "0x1016432E0", Offset = "0x16432E0", VA = "0x1016432E0")]
	public int GetDay()
	{
		return 0;
	}

	// Token: 0x06000561 RID: 1377 RVA: 0x00003540 File Offset: 0x00001740
	[Token(Token = "0x6000506")]
	[Address(RVA = "0x101643380", Offset = "0x1643380", VA = "0x101643380")]
	public int GetHour()
	{
		return 0;
	}

	// Token: 0x06000562 RID: 1378 RVA: 0x00003558 File Offset: 0x00001758
	[Token(Token = "0x6000507")]
	[Address(RVA = "0x101643424", Offset = "0x1643424", VA = "0x101643424")]
	public int GetMinuite()
	{
		return 0;
	}

	// Token: 0x06000563 RID: 1379 RVA: 0x00003570 File Offset: 0x00001770
	[Token(Token = "0x6000508")]
	[Address(RVA = "0x1016434C8", Offset = "0x16434C8", VA = "0x1016434C8")]
	public int GetSecond()
	{
		return 0;
	}

	// Token: 0x06000564 RID: 1380 RVA: 0x00003588 File Offset: 0x00001788
	[Token(Token = "0x6000509")]
	[Address(RVA = "0x10164356C", Offset = "0x164356C", VA = "0x10164356C")]
	public long GetUTCSecondFromEpoch()
	{
		return 0L;
	}

	// Token: 0x06000565 RID: 1381 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600050A")]
	[Address(RVA = "0x10164358C", Offset = "0x164358C", VA = "0x10164358C")]
	public RealTimeClock()
	{
	}

	// Token: 0x04000302 RID: 770
	[Token(Token = "0x40001D0")]
	private static DateTime UNIX_EPOCH;

	// Token: 0x04000303 RID: 771
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x40001D1")]
	private DateTime m_DateTime;

	// Token: 0x04000304 RID: 772
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x40001D2")]
	private DateTime m_UTC;

	// Token: 0x04000305 RID: 773
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x40001D3")]
	private TimeSpan m_UTC_FromEpoch;
}
