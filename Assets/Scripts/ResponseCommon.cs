﻿using CommonResponseTypes;
using Meige;
using Newtonsoft.Json;
using Star;
using System;
using System.Collections.Generic;
using System.Linq;
using WWWTypes;

public static class ResponseCommon {
    public static T Func<T>(MeigewwwParam wwwParam, DialogType dialogType = DialogType.Retry, List<ResultCode> acceptableResultCodes = null) where T : CommonResponse {
        T resp = null;
        if (!wwwParam.IsDone()) {
            return resp;
        }
        GameSystem game = GameSystem.Inst;
        if (game != null) {
            game.ConnectingIcon.Close();
        }
        if (wwwParam.IsSystemError()) {
            Debug.Log("www system error : " + wwwParam.GetSystemError());
            ShowDialog(wwwParam, null, DialogType.Retry);
            return null;
        }
        resp = Deserialize<T>(wwwParam.GetResponseJson());
        if (resp == null) {
            APIUtility.ShowErrorWindowTitle(wwwParam, "通信エラーが発生しました。");
            return null;
        }
        if (game != null) {
            game.ServerTime = resp.serverTime;
        }
        ResultCode result = resp.GetResult();
        switch (result) {
            case ResultCode.ERROR:
            case ResultCode.ACCESS_LIMITATION:
            case ResultCode.UNAVAILABLE:
            case ResultCode.INVALID_REQUEST_HASH:
            case ResultCode.INVALID_PARAMETERS:
            case ResultCode.INSUFFICIENT_PARAMETERS:
            case ResultCode.INVALID_JSON_SCHEMA:
            case ResultCode.PLAYER_STOPPED:
            case ResultCode.UNKNOWN_ERROR:
                ShowDialog(wwwParam, result.ToMessageString(), DialogType.Title);
                break;
            case ResultCode.SUCCESS:
                if (game != null && game.UserDataMng != null && resp.newAchievementCount > 0) {
                    game.UserDataMng.NoticeAchievementCount = resp.newAchievementCount;
                }
                return resp;
            case ResultCode.OUTDATED:
                APIUtility.ShowErrorWindowOk_NoClose("アプリの更新", result.ToMessageString(), APIUtility.OpenStore);
                break;
            case ResultCode.MAINTENANCE:
                if (game.CmnMsgWindow.WindowList.Any((p) => p.m_ResultCode.HasValue && p.m_ResultCode.GetValueOrDefault() == result)) {
                    return null;
                }
                game.CmnMsgWindow.ChangeOKButtonText("アプリを終了");
                string errorMessage = !string.IsNullOrEmpty(resp.message) ? resp.message : result.ToMessageString();
                APIUtility.ShowErrorWindowTitle(wwwParam, "メンテナンス中", errorMessage, false, result);
                break;
            case ResultCode.OUTDATED_AB_VERSION:
                if (game.CmnMsgWindow.WindowList.Any((p) => p.m_ResultCode.HasValue && p.m_ResultCode.GetValueOrDefault() == result)) {
                    return null;
                }
                APIUtility.ShowErrorWindowTitle(wwwParam, "データの更新", result.ToMessageString(), true, result);
                break;
            case ResultCode.DB_ERROR:
                ShowDialog(wwwParam, result.ToMessageString(), DialogType.Retry);
                break;
            case ResultCode.ALREADY_PROCESSED:
                APIUtility.ShowErrorWindowTitle(wwwParam, "通信エラー", result.ToMessageString(), false);
                break;
            case ResultCode.PLAYER_SESSION_EXPIRED:
                APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
                break;
            case ResultCode.PLAYER_SUSPENDED:
                APIUtility.ShowErrorWindowTitle(wwwParam, "確認", result.ToMessageString());
                break;
            case ResultCode.PLAYER_DELETED:
                if (AccessSaveData.Inst != null) {
                    AccessSaveData.Inst.Reset();
                    AccessSaveData.Inst.Save();
                }
                APIUtility.ShowErrorWindowTitle(wwwParam, "通信エラー", result.ToMessageString());
                break;
            default:
                if (dialogType == DialogType.None) {
                    return resp;
                }
                if (acceptableResultCodes != null && acceptableResultCodes.Contains(result)) {
                    return resp;
                }
                ShowDialog(wwwParam, resp.GetResult().ToMessageString(), dialogType);
                break;
        }
        return null;
    }

    public static void ShowDialog(MeigewwwParam wwwParam, string errMsg, DialogType dialogType) {
        switch (dialogType) {
            case DialogType.Ok:
                APIUtility.ShowErrorWindowOk(errMsg, null);
                break;
            case DialogType.Retry:
                APIUtility.ShowErrorWindowRetry(wwwParam, errMsg);
                break;
            case DialogType.Title:
                APIUtility.ShowErrorWindowTitle(wwwParam, errMsg);
                break;
        }
    }

    public static T Deserialize<T>(string json) where T : CommonResponse {
        T result = null;
        try {
            result = JsonConvert.DeserializeObject<T>(json);
        } catch (Exception ex) {
            Debug.Log("Deserialize json : " + json);
            Debug.Log("Deserialize Exception : " + ex.ToString());
        }
        return result;
    }

    public enum DialogType {
        None,
        Ok,
        Retry,
        Title
    }
}
