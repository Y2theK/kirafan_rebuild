﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomObjectRequestTypes;
using WWWTypes;

// Token: 0x02000075 RID: 117
[Token(Token = "0x200004E")]
[StructLayout(3)]
public static class RoomObjectRequest
{
	// Token: 0x060002D9 RID: 729 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000292")]
	[Address(RVA = "0x101643D58", Offset = "0x1643D58", VA = "0x101643D58")]
	public static MeigewwwParam Add(Add param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DA RID: 730 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000293")]
	[Address(RVA = "0x101643E88", Offset = "0x1643E88", VA = "0x101643E88")]
	public static MeigewwwParam Add(string roomObjectId, string amount, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DB RID: 731 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000294")]
	[Address(RVA = "0x101643FDC", Offset = "0x1643FDC", VA = "0x101643FDC")]
	public static MeigewwwParam BuySet(BuySet param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DC RID: 732 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000295")]
	[Address(RVA = "0x101644238", Offset = "0x1644238", VA = "0x101644238")]
	public static MeigewwwParam BuySet(int roomObjectId, int buyAmount, long managedRoomId, int floorId, int groupId, PlayerRoomArrangement[] arrangeData, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DD RID: 733 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000296")]
	[Address(RVA = "0x1016444AC", Offset = "0x16444AC", VA = "0x1016444AC")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DE RID: 734 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000297")]
	[Address(RVA = "0x101644570", Offset = "0x1644570", VA = "0x101644570")]
	public static MeigewwwParam GetAll([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002DF RID: 735 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000298")]
	[Address(RVA = "0x101644634", Offset = "0x1644634", VA = "0x101644634")]
	public static MeigewwwParam Remove(Remove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E0 RID: 736 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000299")]
	[Address(RVA = "0x10164475C", Offset = "0x164475C", VA = "0x10164475C")]
	public static MeigewwwParam Remove(long managedRoomObjectId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E1 RID: 737 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029A")]
	[Address(RVA = "0x101644874", Offset = "0x1644874", VA = "0x101644874")]
	public static MeigewwwParam Sale(Sale param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E2 RID: 738 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029B")]
	[Address(RVA = "0x101644978", Offset = "0x1644978", VA = "0x101644978")]
	public static MeigewwwParam Sale(string managedRoomObjectId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E3 RID: 739 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029C")]
	[Address(RVA = "0x101644A88", Offset = "0x1644A88", VA = "0x101644A88")]
	public static MeigewwwParam GetList(GetList param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E4 RID: 740 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029D")]
	[Address(RVA = "0x101644B4C", Offset = "0x1644B4C", VA = "0x101644B4C")]
	public static MeigewwwParam GetList([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E5 RID: 741 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029E")]
	[Address(RVA = "0x101644C10", Offset = "0x1644C10", VA = "0x101644C10")]
	public static MeigewwwParam Limitadd(Limitadd param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E6 RID: 742 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600029F")]
	[Address(RVA = "0x101644D38", Offset = "0x1644D38", VA = "0x101644D38")]
	public static MeigewwwParam Limitadd(int num, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
