﻿using WWWTypes;

namespace RoomObjectRequestTypes {
    public class BuySet {
        public int roomObjectId;
        public int buyAmount;
        public long managedRoomId;
        public int floorId;
        public int groupId;
        public PlayerRoomArrangement[] arrangeData;
    }
}
