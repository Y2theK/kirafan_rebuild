﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomObjectResponseTypes;
using WWWTypes;

// Token: 0x0200009C RID: 156
[Token(Token = "0x2000073")]
[StructLayout(3)]
public static class RoomObjectResponse
{
	// Token: 0x060003C3 RID: 963 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000379")]
	[Address(RVA = "0x101644E88", Offset = "0x1644E88", VA = "0x101644E88")]
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C4 RID: 964 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037A")]
	[Address(RVA = "0x101644EF0", Offset = "0x1644EF0", VA = "0x101644EF0")]
	public static BuySet BuySet(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C5 RID: 965 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037B")]
	[Address(RVA = "0x101644F58", Offset = "0x1644F58", VA = "0x101644F58")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C6 RID: 966 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037C")]
	[Address(RVA = "0x101644FC0", Offset = "0x1644FC0", VA = "0x101644FC0")]
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C7 RID: 967 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037D")]
	[Address(RVA = "0x101645028", Offset = "0x1645028", VA = "0x101645028")]
	public static Sale Sale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C8 RID: 968 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037E")]
	[Address(RVA = "0x101645090", Offset = "0x1645090", VA = "0x101645090")]
	public static GetList GetList(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003C9 RID: 969 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600037F")]
	[Address(RVA = "0x1016450F8", Offset = "0x16450F8", VA = "0x1016450F8")]
	public static Limitadd Limitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
