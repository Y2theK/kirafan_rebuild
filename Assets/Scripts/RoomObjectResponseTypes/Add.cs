﻿using CommonResponseTypes;

namespace RoomObjectResponseTypes {
    public class Add : CommonResponse {
        public string managedRoomObjectIds;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedRoomObjectIds == null) ? string.Empty : managedRoomObjectIds.ToString();
            return str;
        }
    }
}
