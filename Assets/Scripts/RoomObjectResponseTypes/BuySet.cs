﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes {
    public class BuySet : CommonResponse {
        public string managedRoomObjectIds;
        public PlayerRoomObject[] managedRoomObjects;
        public PlayerRoom[] managedRooms;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedRoomObjectIds == null) ? string.Empty : managedRoomObjectIds.ToString();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            str += (managedRooms == null) ? string.Empty : managedRooms.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
