﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerRoomObject[] managedRoomObjects;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            return str;
        }
    }
}
