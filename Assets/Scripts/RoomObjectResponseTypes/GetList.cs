﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes {
    public class GetList : CommonResponse {
        public RoomObject[] roomObjects;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (roomObjects == null) ? string.Empty : roomObjects.ToString();
            return str;
        }
    }
}
