﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes {
    public class Limitadd : CommonResponse {
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
