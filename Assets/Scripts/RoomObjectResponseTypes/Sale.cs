﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomObjectResponseTypes {
    public class Sale : CommonResponse {
        public PlayerRoomObject[] managedRoomObjects;
        public Player player;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedRoomObjects == null) ? string.Empty : managedRoomObjects.ToString();
            str += (player == null) ? string.Empty : player.ToString();
            return str;
        }
    }
}
