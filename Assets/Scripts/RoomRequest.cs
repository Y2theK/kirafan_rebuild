﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomRequestTypes;
using WWWTypes;

// Token: 0x02000076 RID: 118
[Token(Token = "0x200004F")]
[StructLayout(3)]
public static class RoomRequest
{
	// Token: 0x060002E7 RID: 743 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A0")]
	[Address(RVA = "0x101645580", Offset = "0x1645580", VA = "0x101645580")]
	public static MeigewwwParam Set(Set param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E8 RID: 744 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A1")]
	[Address(RVA = "0x10164575C", Offset = "0x164575C", VA = "0x10164575C")]
	public static MeigewwwParam Set(long managedRoomId, int floorId, int groupId, PlayerRoomArrangement[] arrangeData, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002E9 RID: 745 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A2")]
	[Address(RVA = "0x101645948", Offset = "0x1645948", VA = "0x101645948")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002EA RID: 746 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A3")]
	[Address(RVA = "0x101645AF8", Offset = "0x1645AF8", VA = "0x101645AF8")]
	public static MeigewwwParam GetAll(long playerId, int floorId, int groupId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002EB RID: 747 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A4")]
	[Address(RVA = "0x101645CA0", Offset = "0x1645CA0", VA = "0x101645CA0")]
	public static MeigewwwParam Remove(Remove param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002EC RID: 748 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A5")]
	[Address(RVA = "0x101645DC8", Offset = "0x1645DC8", VA = "0x101645DC8")]
	public static MeigewwwParam Remove(long managedRoomId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002ED RID: 749 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A6")]
	[Address(RVA = "0x101645EE0", Offset = "0x1645EE0", VA = "0x101645EE0")]
	public static MeigewwwParam Setactiveroom(Setactiveroom param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002EE RID: 750 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A7")]
	[Address(RVA = "0x101646008", Offset = "0x1646008", VA = "0x101646008")]
	public static MeigewwwParam Setactiveroom(long managedRoomId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002EF RID: 751 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A8")]
	[Address(RVA = "0x101646120", Offset = "0x1646120", VA = "0x101646120")]
	public static MeigewwwParam Getactiveroom(Getactiveroom param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F0 RID: 752 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002A9")]
	[Address(RVA = "0x1016461E4", Offset = "0x16461E4", VA = "0x1016461E4")]
	public static MeigewwwParam Getactiveroom([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
