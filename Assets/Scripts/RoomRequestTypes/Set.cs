﻿using WWWTypes;

namespace RoomRequestTypes {
    public class Set {
        public long managedRoomId;
        public int floorId;
        public int groupId;
        public PlayerRoomArrangement[] arrangeData;
    }
}
