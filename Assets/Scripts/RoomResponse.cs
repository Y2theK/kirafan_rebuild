﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomResponseTypes;
using WWWTypes;

// Token: 0x0200009D RID: 157
[Token(Token = "0x2000074")]
[StructLayout(3)]
public static class RoomResponse
{
	// Token: 0x060003CA RID: 970 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000380")]
	[Address(RVA = "0x1016462D0", Offset = "0x16462D0", VA = "0x1016462D0")]
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003CB RID: 971 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000381")]
	[Address(RVA = "0x101646338", Offset = "0x1646338", VA = "0x101646338")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003CC RID: 972 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000382")]
	[Address(RVA = "0x1016463A0", Offset = "0x16463A0", VA = "0x1016463A0")]
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003CD RID: 973 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000383")]
	[Address(RVA = "0x101646408", Offset = "0x1646408", VA = "0x101646408")]
	public static Setactiveroom Setactiveroom(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003CE RID: 974 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000384")]
	[Address(RVA = "0x101646470", Offset = "0x1646470", VA = "0x101646470")]
	public static Getactiveroom Getactiveroom(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
