﻿using CommonResponseTypes;
using WWWTypes;

namespace RoomResponseTypes {
    public class GetAll : CommonResponse {
        public PlayerRoom[] managedRooms;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (managedRooms == null) ? string.Empty : managedRooms.ToString();
            return str;
        }
    }
}
