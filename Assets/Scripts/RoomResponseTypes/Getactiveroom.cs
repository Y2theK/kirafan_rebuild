﻿using CommonResponseTypes;

namespace RoomResponseTypes {
    public class Getactiveroom : CommonResponse {
        public long managedRoomId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += managedRoomId.ToString();
            return str;
        }
    }
}
