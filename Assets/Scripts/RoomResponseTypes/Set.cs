﻿using CommonResponseTypes;

namespace RoomResponseTypes {
    public class Set : CommonResponse {
        public long managedRoomId;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += managedRoomId.ToString();
            return str;
        }
    }
}
