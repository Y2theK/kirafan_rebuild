﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using STORERequestTypes;
using WWWTypes;

// Token: 0x02000078 RID: 120
[Token(Token = "0x2000051")]
[StructLayout(3)]
public static class STORERequest
{
	// Token: 0x060002F7 RID: 759 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B0")]
	[Address(RVA = "0x101646610", Offset = "0x1646610", VA = "0x101646610")]
	public static MeigewwwParam GetAll(GetAll param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F8 RID: 760 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B1")]
	[Address(RVA = "0x101646778", Offset = "0x1646778", VA = "0x101646778")]
	public static MeigewwwParam GetAll(int platform, int env, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F9 RID: 761 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B2")]
	[Address(RVA = "0x1016468D0", Offset = "0x16468D0", VA = "0x1016468D0")]
	public static MeigewwwParam ProductSetShown(ProductSetShown param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FA RID: 762 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B3")]
	[Address(RVA = "0x1016469D4", Offset = "0x16469D4", VA = "0x1016469D4")]
	public static MeigewwwParam ProductSetShown(ShownProduct[] shown, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FB RID: 763 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B4")]
	[Address(RVA = "0x101646AE4", Offset = "0x1646AE4", VA = "0x101646AE4")]
	public static MeigewwwParam PurchaseAppStore(PurchaseAppStore param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FC RID: 764 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B5")]
	[Address(RVA = "0x101646BE8", Offset = "0x1646BE8", VA = "0x101646BE8")]
	public static MeigewwwParam PurchaseAppStore(string receipt, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FD RID: 765 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B6")]
	[Address(RVA = "0x101646CF8", Offset = "0x1646CF8", VA = "0x101646CF8")]
	public static MeigewwwParam PurchaseGooglePlay(PurchaseGooglePlay param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FE RID: 766 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B7")]
	[Address(RVA = "0x101646E28", Offset = "0x1646E28", VA = "0x101646E28")]
	public static MeigewwwParam PurchaseGooglePlay(string receipt, string signature, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002FF RID: 767 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B8")]
	[Address(RVA = "0x101646F7C", Offset = "0x1646F7C", VA = "0x101646F7C")]
	public static MeigewwwParam GetGooglePayload(GetGooglePayload param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000300 RID: 768 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002B9")]
	[Address(RVA = "0x1016470A4", Offset = "0x16470A4", VA = "0x1016470A4")]
	public static MeigewwwParam GetGooglePayload(int id, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000301 RID: 769 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BA")]
	[Address(RVA = "0x1016471BC", Offset = "0x16471BC", VA = "0x1016471BC")]
	public static MeigewwwParam GetApplePayload(GetApplePayload param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000302 RID: 770 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BB")]
	[Address(RVA = "0x1016472E4", Offset = "0x16472E4", VA = "0x1016472E4")]
	public static MeigewwwParam GetApplePayload(int id, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000303 RID: 771 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BC")]
	[Address(RVA = "0x1016473FC", Offset = "0x16473FC", VA = "0x1016473FC")]
	public static MeigewwwParam GetPurchaseLog(GetPurchaseLog param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000304 RID: 772 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BD")]
	[Address(RVA = "0x1016474C0", Offset = "0x16474C0", VA = "0x1016474C0")]
	public static MeigewwwParam GetPurchaseLog([Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000305 RID: 773 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BE")]
	[Address(RVA = "0x101647584", Offset = "0x1647584", VA = "0x101647584")]
	public static MeigewwwParam ProductRemindSetShown(ProductRemindSetShown param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000306 RID: 774 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002BF")]
	[Address(RVA = "0x101647688", Offset = "0x1647688", VA = "0x101647688")]
	public static MeigewwwParam ProductRemindSetShown(int[] premiumIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000307 RID: 775 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C0")]
	[Address(RVA = "0x101647798", Offset = "0x1647798", VA = "0x101647798")]
	public static MeigewwwParam ShownDirectSale(ShownDirectSale param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000308 RID: 776 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C1")]
	[Address(RVA = "0x10164792C", Offset = "0x164792C", VA = "0x10164792C")]
	public static MeigewwwParam ShownDirectSale(int platform, int env, int[] productIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x06000309 RID: 777 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C2")]
	[Address(RVA = "0x101647AD0", Offset = "0x1647AD0", VA = "0x101647AD0")]
	public static MeigewwwParam RegisterGooglePendingTransaction(RegisterGooglePendingTransaction param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x0600030A RID: 778 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002C3")]
	[Address(RVA = "0x101647C2C", Offset = "0x1647C2C", VA = "0x101647C2C")]
	public static MeigewwwParam RegisterGooglePendingTransaction(string packageName, string productId, string transactionId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
