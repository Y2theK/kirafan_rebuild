﻿namespace STORERequestTypes {
    public class RegisterGooglePendingTransaction {
        public string packageName;
        public string productId;
        public string transactionId;
    }
}
