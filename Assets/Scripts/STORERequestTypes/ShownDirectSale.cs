﻿namespace STORERequestTypes {
    public class ShownDirectSale {
        public int platform;
        public int env;
        public int[] productIds;
    }
}
