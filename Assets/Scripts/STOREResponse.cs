﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using STOREResponseTypes;
using WWWTypes;

// Token: 0x0200009F RID: 159
[Token(Token = "0x2000076")]
[StructLayout(3)]
public static class STOREResponse
{
	// Token: 0x060003D2 RID: 978 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000388")]
	[Address(RVA = "0x101647E1C", Offset = "0x1647E1C", VA = "0x101647E1C")]
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D3 RID: 979 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000389")]
	[Address(RVA = "0x101647E84", Offset = "0x1647E84", VA = "0x101647E84")]
	public static ProductSetShown ProductSetShown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D4 RID: 980 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038A")]
	[Address(RVA = "0x101647EEC", Offset = "0x1647EEC", VA = "0x101647EEC")]
	public static PurchaseAppStore PurchaseAppStore(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D5 RID: 981 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038B")]
	[Address(RVA = "0x101647F54", Offset = "0x1647F54", VA = "0x101647F54")]
	public static PurchaseGooglePlay PurchaseGooglePlay(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D6 RID: 982 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038C")]
	[Address(RVA = "0x101647FBC", Offset = "0x1647FBC", VA = "0x101647FBC")]
	public static GetGooglePayload GetGooglePayload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D7 RID: 983 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038D")]
	[Address(RVA = "0x101648024", Offset = "0x1648024", VA = "0x101648024")]
	public static GetApplePayload GetApplePayload(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D8 RID: 984 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038E")]
	[Address(RVA = "0x10164808C", Offset = "0x164808C", VA = "0x10164808C")]
	public static GetPurchaseLog GetPurchaseLog(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D9 RID: 985 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x600038F")]
	[Address(RVA = "0x1016480F4", Offset = "0x16480F4", VA = "0x1016480F4")]
	public static ProductRemindSetShown ProductRemindSetShown(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003DA RID: 986 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000390")]
	[Address(RVA = "0x10164815C", Offset = "0x164815C", VA = "0x10164815C")]
	public static ShownDirectSale ShownDirectSale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003DB RID: 987 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000391")]
	[Address(RVA = "0x1016481C4", Offset = "0x16481C4", VA = "0x1016481C4")]
	public static RegisterGooglePendingTransaction RegisterGooglePendingTransaction(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
