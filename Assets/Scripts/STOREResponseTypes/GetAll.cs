﻿using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes {
    public class GetAll : CommonResponse {
        public StoreProduct[] products;
        public ExchangeShop[] exchangeShops;
        public long monthlyAmount;
        public long availableBalance;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (products == null) ? string.Empty : products.ToString();
            str += (exchangeShops == null) ? string.Empty : exchangeShops.ToString();
            str += monthlyAmount.ToString();
            str += availableBalance.ToString();
            return str;
        }
    }
}
