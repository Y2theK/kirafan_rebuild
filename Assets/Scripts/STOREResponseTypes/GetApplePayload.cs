﻿using CommonResponseTypes;

namespace STOREResponseTypes {
    public class GetApplePayload : CommonResponse {
        public string payload;
        public long monthlyAmount;
        public long availableBalance;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (payload == null) ? string.Empty : payload.ToString();
            str += monthlyAmount.ToString();
            str += availableBalance.ToString();
            return str;
        }
    }
}
