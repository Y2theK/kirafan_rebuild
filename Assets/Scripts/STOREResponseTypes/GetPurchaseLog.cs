﻿using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes {
    public class GetPurchaseLog : CommonResponse {
        public PlayerPurchase[] purchases;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (purchases == null) ? string.Empty : purchases.ToString();
            return str;
        }
    }
}
