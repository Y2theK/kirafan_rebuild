﻿using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes {
    public class PurchaseGooglePlay : CommonResponse {
        public Player player;
        public StoreProduct[] purchased;
        public StoreProduct[] products;
        public PlayerItemSummary[] itemSummary;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (player == null) ? string.Empty : player.ToString();
            str += (purchased == null) ? string.Empty : purchased.ToString();
            str += (products == null) ? string.Empty : products.ToString();
            str += (itemSummary == null) ? string.Empty : itemSummary.ToString();
            return str;
        }
    }
}
