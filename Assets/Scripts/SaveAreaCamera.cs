﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200004A RID: 74
[Token(Token = "0x2000025")]
[StructLayout(3)]
public class SaveAreaCamera : MonoBehaviour
{
	// Token: 0x17000043 RID: 67
	// (get) Token: 0x0600015D RID: 349 RVA: 0x000026B8 File Offset: 0x000008B8
	// (set) Token: 0x0600015E RID: 350 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000037")]
	public bool isAvailable
	{
		[Token(Token = "0x6000116")]
		[Address(RVA = "0x10164885C", Offset = "0x164885C", VA = "0x10164885C")]
		[CompilerGenerated]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x6000117")]
		[Address(RVA = "0x101648864", Offset = "0x1648864", VA = "0x101648864")]
		[CompilerGenerated]
		private set
		{
		}
	}

	// Token: 0x0600015F RID: 351 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000118")]
	[Address(RVA = "0x10164886C", Offset = "0x164886C", VA = "0x10164886C")]
	public Camera GetCamera()
	{
		return null;
	}

	// Token: 0x06000160 RID: 352 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000119")]
	[Address(RVA = "0x101648874", Offset = "0x1648874", VA = "0x101648874")]
	public RenderTexture GetRenderTexture()
	{
		return null;
	}

	// Token: 0x06000161 RID: 353 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600011A")]
	[Address(RVA = "0x10164887C", Offset = "0x164887C", VA = "0x10164887C")]
	public void EnableCamera(bool flg)
	{
	}

	// Token: 0x06000162 RID: 354 RVA: 0x000026D0 File Offset: 0x000008D0
	[Token(Token = "0x600011B")]
	[Address(RVA = "0x1016488B4", Offset = "0x16488B4", VA = "0x1016488B4")]
	public bool IsEnableCamera()
	{
		return default(bool);
	}

	// Token: 0x06000163 RID: 355 RVA: 0x000026E8 File Offset: 0x000008E8
	[Token(Token = "0x600011C")]
	[Address(RVA = "0x1016488E4", Offset = "0x16488E4", VA = "0x1016488E4")]
	public bool Setup()
	{
		return default(bool);
	}

	// Token: 0x06000164 RID: 356 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600011D")]
	[Address(RVA = "0x101648A30", Offset = "0x1648A30", VA = "0x101648A30")]
	private void CreateRenderTarget()
	{
	}

	// Token: 0x06000165 RID: 357 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600011E")]
	[Address(RVA = "0x101648C30", Offset = "0x1648C30", VA = "0x101648C30")]
	private void Awake()
	{
	}

	// Token: 0x06000166 RID: 358 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600011F")]
	[Address(RVA = "0x101648C34", Offset = "0x1648C34", VA = "0x101648C34")]
	public SaveAreaCamera()
	{
	}

	// Token: 0x040001CF RID: 463
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000127")]
	private Camera m_TargetCamera;

	// Token: 0x040001D0 RID: 464
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000128")]
	private RenderTexture m_RenderTexture;

	// Token: 0x040001D1 RID: 465
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000129")]
	private GameObject m_GameObject;
}
