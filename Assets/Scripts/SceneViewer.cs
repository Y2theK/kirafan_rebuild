﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000E9 RID: 233
[Token(Token = "0x20000A9")]
[StructLayout(3)]
public class SceneViewer : MonoBehaviour
{
	// Token: 0x060005BC RID: 1468 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000550")]
	[Address(RVA = "0x101648C3C", Offset = "0x1648C3C", VA = "0x101648C3C")]
	private void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
	{
	}

	// Token: 0x060005BD RID: 1469 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000551")]
	[Address(RVA = "0x101648C40", Offset = "0x1648C40", VA = "0x101648C40")]
	private void Start()
	{
	}

	// Token: 0x060005BE RID: 1470 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000552")]
	[Address(RVA = "0x101648CD0", Offset = "0x1648CD0", VA = "0x101648CD0")]
	private void Update()
	{
	}

	// Token: 0x060005BF RID: 1471 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000553")]
	[Address(RVA = "0x10164965C", Offset = "0x164965C", VA = "0x10164965C")]
	public SceneViewer()
	{
	}

	// Token: 0x04000386 RID: 902
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000206")]
	public bool m_bInstantiate;

	// Token: 0x04000387 RID: 903
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000207")]
	public SceneViewer.ScenePrefab[] m_ScenePrefabArray;

	// Token: 0x04000388 RID: 904
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000208")]
	private SceneViewer.SceneInstance[] m_SceneInstanceArray;

	// Token: 0x04000389 RID: 905
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x4000209")]
	private string m_ClipName;

	// Token: 0x0400038A RID: 906
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x400020A")]
	private SceneViewer.ePhase m_Phase;

	// Token: 0x0400038B RID: 907
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x400020B")]
	private MeigeAnimCtrl m_AnimCtrl;

	// Token: 0x020000EA RID: 234
	[Token(Token = "0x2000D1A")]
	private enum ePhase
	{
		// Token: 0x0400038D RID: 909
		[Token(Token = "0x4005510")]
		ePhase_None,
		// Token: 0x0400038E RID: 910
		[Token(Token = "0x4005511")]
		ePhase_Instantiate,
		// Token: 0x0400038F RID: 911
		[Token(Token = "0x4005512")]
		ePhase_Setup,
		// Token: 0x04000390 RID: 912
		[Token(Token = "0x4005513")]
		ePhase_Play,
		// Token: 0x04000391 RID: 913
		[Token(Token = "0x4005514")]
		ePhase_Main
	}

	// Token: 0x020000EB RID: 235
	[Token(Token = "0x2000D1B")]
	[Serializable]
	public class ScenePrefab
	{
		// Token: 0x060005C0 RID: 1472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D38")]
		[Address(RVA = "0x101649664", Offset = "0x1649664", VA = "0x101649664")]
		public ScenePrefab()
		{
		}

		// Token: 0x04000392 RID: 914
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005515")]
		public UnityEngine.Object m_TargetPrefab;

		// Token: 0x04000393 RID: 915
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4005516")]
		public UnityEngine.Object m_AnimClipPrefab;

		// Token: 0x04000394 RID: 916
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4005517")]
		public bool m_bCameraAnim;
	}

	// Token: 0x020000EC RID: 236
	[Token(Token = "0x2000D1C")]
	public class SceneInstance
	{
		// Token: 0x060005C1 RID: 1473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005D39")]
		[Address(RVA = "0x101649630", Offset = "0x1649630", VA = "0x101649630")]
		public SceneInstance()
		{
		}

		// Token: 0x04000395 RID: 917
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4005518")]
		public GameObject m_TargetGO;

		// Token: 0x04000396 RID: 918
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4005519")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04000397 RID: 919
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400551A")]
		public MeigeAnimClipHolder m_ClipHolder;

		// Token: 0x04000398 RID: 920
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400551B")]
		public GameObject m_AnimClipGO;
	}
}
