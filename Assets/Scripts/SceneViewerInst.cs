﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000ED RID: 237
[Token(Token = "0x20000AA")]
[StructLayout(3)]
public class SceneViewerInst : MonoBehaviour
{
	// Token: 0x060005C2 RID: 1474 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000554")]
	[Address(RVA = "0x101649690", Offset = "0x1649690", VA = "0x101649690")]
	private void Start()
	{
	}

	// Token: 0x060005C3 RID: 1475 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000555")]
	[Address(RVA = "0x1016496F8", Offset = "0x16496F8", VA = "0x1016496F8")]
	private void Update()
	{
	}

	// Token: 0x060005C4 RID: 1476 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000556")]
	[Address(RVA = "0x1016496FC", Offset = "0x16496FC", VA = "0x1016496FC")]
	public SceneViewerInst()
	{
	}
}
