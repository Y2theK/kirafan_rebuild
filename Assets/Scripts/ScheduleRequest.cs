﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using ScheduleRequestTypes;
using WWWTypes;

// Token: 0x02000077 RID: 119
[Token(Token = "0x2000050")]
[StructLayout(3)]
public static class ScheduleRequest
{
	// Token: 0x060002F1 RID: 753 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AA")]
	[Address(RVA = "0x101649704", Offset = "0x1649704", VA = "0x101649704")]
	public static MeigewwwParam Schedulegetall(Schedulegetall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F2 RID: 754 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AB")]
	[Address(RVA = "0x101649808", Offset = "0x1649808", VA = "0x101649808")]
	public static MeigewwwParam Schedulegetall(long[] managedCharacterIds, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F3 RID: 755 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AC")]
	[Address(RVA = "0x101649918", Offset = "0x1649918", VA = "0x101649918")]
	public static MeigewwwParam Scheduleaddall(Scheduleaddall param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F4 RID: 756 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AD")]
	[Address(RVA = "0x101649A1C", Offset = "0x1649A1C", VA = "0x101649A1C")]
	public static MeigewwwParam Scheduleaddall(ScheduleMemberParam[] members, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F5 RID: 757 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AE")]
	[Address(RVA = "0x101649B2C", Offset = "0x1649B2C", VA = "0x101649B2C")]
	public static MeigewwwParam Schedulereapdrop(Schedulereapdrop param, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}

	// Token: 0x060002F6 RID: 758 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x60002AF")]
	[Address(RVA = "0x101649C54", Offset = "0x1649C54", VA = "0x101649C54")]
	public static MeigewwwParam Schedulereapdrop(long managedPartyMemberId, [Optional] MeigewwwParam.Callback callback)
	{
		return null;
	}
}
