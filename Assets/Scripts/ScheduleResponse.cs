﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using ScheduleResponseTypes;
using WWWTypes;

// Token: 0x0200009E RID: 158
[Token(Token = "0x2000075")]
[StructLayout(3)]
public static class ScheduleResponse
{
	// Token: 0x060003CF RID: 975 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000385")]
	[Address(RVA = "0x101649D84", Offset = "0x1649D84", VA = "0x101649D84")]
	public static Schedulegetall Schedulegetall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D0 RID: 976 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000386")]
	[Address(RVA = "0x101649DEC", Offset = "0x1649DEC", VA = "0x101649DEC")]
	public static Scheduleaddall Scheduleaddall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}

	// Token: 0x060003D1 RID: 977 RVA: 0x00002052 File Offset: 0x00000252
	[Token(Token = "0x6000387")]
	[Address(RVA = "0x101649E54", Offset = "0x1649E54", VA = "0x101649E54")]
	public static Schedulereapdrop Schedulereapdrop(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, [Optional] List<ResultCode> acceptableResultCodes)
	{
		return null;
	}
}
