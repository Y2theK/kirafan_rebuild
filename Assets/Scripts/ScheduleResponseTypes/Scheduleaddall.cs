﻿using CommonResponseTypes;
using WWWTypes;

namespace ScheduleResponseTypes {
    public class Scheduleaddall : CommonResponse {
        public ScheduleMember[] members;

        public override string ToMessage() {
            string str = base.ToMessage();
            str += (members == null) ? string.Empty : members.ToString();
            return str;
        }
    }
}
