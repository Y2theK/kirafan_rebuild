﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000AD RID: 173
[Token(Token = "0x2000084")]
[StructLayout(3)]
public class ScriptData : ScriptableObject
{
	// Token: 0x0600040A RID: 1034 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003C0")]
	[Address(RVA = "0x10164A0D4", Offset = "0x164A0D4", VA = "0x10164A0D4")]
	public ScriptData()
	{
	}

	// Token: 0x04000229 RID: 553
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x400016F")]
	public ScriptData_Param[] m_Params;
}
