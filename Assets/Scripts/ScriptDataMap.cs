﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000AF RID: 175
[Token(Token = "0x2000086")]
[StructLayout(3)]
public class ScriptDataMap : ScriptableObject
{
	// Token: 0x0600040B RID: 1035 RVA: 0x000028B0 File Offset: 0x00000AB0
	[Token(Token = "0x60003C1")]
	[Address(RVA = "0x10164A0DC", Offset = "0x164A0DC", VA = "0x10164A0DC")]
	public ScriptDataMap_Param GetParam(int ID)
	{
		return default(ScriptDataMap_Param);
	}

	// Token: 0x0600040C RID: 1036 RVA: 0x000028C8 File Offset: 0x00000AC8
	[Token(Token = "0x60003C2")]
	[Address(RVA = "0x10164A1B4", Offset = "0x164A1B4", VA = "0x10164A1B4")]
	public ScriptDataMap_Param GetParam(string part)
	{
		return default(ScriptDataMap_Param);
	}

	// Token: 0x0600040D RID: 1037 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x60003C3")]
	[Address(RVA = "0x10164A250", Offset = "0x164A250", VA = "0x10164A250")]
	public ScriptDataMap()
	{
	}

	// Token: 0x0400022C RID: 556
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000172")]
	public ScriptDataMap_Param[] m_Params;
}
