﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000AE RID: 174
[Token(Token = "0x2000085")]
[Serializable]
[StructLayout(0, Size = 16)]
public struct ScriptDataMap_Param
{
	// Token: 0x0400022A RID: 554
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x4000170")]
	public int[] IDs;

	// Token: 0x0400022B RID: 555
	[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
	[Token(Token = "0x4000171")]
	public string part;
}
