﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000AB RID: 171
[Token(Token = "0x2000082")]
[Serializable]
[StructLayout(0, Size = 104)]
public struct ScriptData_FuncParam
{
	// Token: 0x04000218 RID: 536
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x400015E")]
	public string funcName;

	// Token: 0x04000219 RID: 537
	[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
	[Token(Token = "0x400015F")]
	public int argNum;

	// Token: 0x0400021A RID: 538
	[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
	[Token(Token = "0x4000160")]
	public int argType1;

	// Token: 0x0400021B RID: 539
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x4000161")]
	public string m_value1;

	// Token: 0x0400021C RID: 540
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000162")]
	public int argType2;

	// Token: 0x0400021D RID: 541
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000163")]
	public string m_value2;

	// Token: 0x0400021E RID: 542
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000164")]
	public int argType3;

	// Token: 0x0400021F RID: 543
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x4000165")]
	public string m_value3;

	// Token: 0x04000220 RID: 544
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x4000166")]
	public int argType4;

	// Token: 0x04000221 RID: 545
	[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
	[Token(Token = "0x4000167")]
	public string m_value4;

	// Token: 0x04000222 RID: 546
	[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
	[Token(Token = "0x4000168")]
	public int argType5;

	// Token: 0x04000223 RID: 547
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x4000169")]
	public string m_value5;

	// Token: 0x04000224 RID: 548
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x400016A")]
	public int argType6;

	// Token: 0x04000225 RID: 549
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x400016B")]
	public string m_value6;
}
