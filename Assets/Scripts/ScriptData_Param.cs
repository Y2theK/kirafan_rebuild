﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

// Token: 0x020000AC RID: 172
[Token(Token = "0x2000083")]
[Serializable]
[StructLayout(0)]
public struct ScriptData_Param
{
	// Token: 0x04000226 RID: 550
	[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
	[Token(Token = "0x400016C")]
	public int ScriptID;

	// Token: 0x04000227 RID: 551
	[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
	[Token(Token = "0x400016D")]
	public string ScriptName;

	// Token: 0x04000228 RID: 552
	[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
	[Token(Token = "0x400016E")]
	public ScriptData_FuncParam[] FuncParam;
}
