﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200004B RID: 75
[Token(Token = "0x2000026")]
[StructLayout(3)]
public class ScrollDistortionRenderer : MonoBehaviour
{
	// Token: 0x17000044 RID: 68
	// (get) Token: 0x06000168 RID: 360 RVA: 0x00002700 File Offset: 0x00000900
	// (set) Token: 0x06000167 RID: 359 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000038")]
	public float level
	{
		[Token(Token = "0x6000121")]
		[Address(RVA = "0x10164A260", Offset = "0x164A260", VA = "0x10164A260")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x6000120")]
		[Address(RVA = "0x10164A258", Offset = "0x164A258", VA = "0x10164A258")]
		set
		{
		}
	}

	// Token: 0x17000045 RID: 69
	// (get) Token: 0x0600016A RID: 362 RVA: 0x00002718 File Offset: 0x00000918
	// (set) Token: 0x06000169 RID: 361 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000039")]
	public Vector4 noiseParam
	{
		[Token(Token = "0x6000123")]
		[Address(RVA = "0x10164A27C", Offset = "0x164A27C", VA = "0x10164A27C")]
		get
		{
			return default(Vector4);
		}
		[Token(Token = "0x6000122")]
		[Address(RVA = "0x10164A268", Offset = "0x164A268", VA = "0x10164A268")]
		set
		{
		}
	}

	// Token: 0x17000046 RID: 70
	// (get) Token: 0x0600016C RID: 364 RVA: 0x00002730 File Offset: 0x00000930
	// (set) Token: 0x0600016B RID: 363 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003A")]
	public Vector4 levelParam
	{
		[Token(Token = "0x6000125")]
		[Address(RVA = "0x10164A29C", Offset = "0x164A29C", VA = "0x10164A29C")]
		get
		{
			return default(Vector4);
		}
		[Token(Token = "0x6000124")]
		[Address(RVA = "0x10164A288", Offset = "0x164A288", VA = "0x10164A288")]
		set
		{
		}
	}

	// Token: 0x17000047 RID: 71
	// (get) Token: 0x0600016E RID: 366 RVA: 0x00002748 File Offset: 0x00000948
	// (set) Token: 0x0600016D RID: 365 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003B")]
	public Vector2 noiseSpeed
	{
		[Token(Token = "0x6000127")]
		[Address(RVA = "0x10164A2B4", Offset = "0x164A2B4", VA = "0x10164A2B4")]
		get
		{
			return default(Vector2);
		}
		[Token(Token = "0x6000126")]
		[Address(RVA = "0x10164A2A8", Offset = "0x164A2A8", VA = "0x10164A2A8")]
		set
		{
		}
	}

	// Token: 0x17000048 RID: 72
	// (get) Token: 0x06000170 RID: 368 RVA: 0x00002760 File Offset: 0x00000960
	// (set) Token: 0x0600016F RID: 367 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003C")]
	public Vector2 levelSpeed
	{
		[Token(Token = "0x6000129")]
		[Address(RVA = "0x10164A2C8", Offset = "0x164A2C8", VA = "0x10164A2C8")]
		get
		{
			return default(Vector2);
		}
		[Token(Token = "0x6000128")]
		[Address(RVA = "0x10164A2BC", Offset = "0x164A2BC", VA = "0x10164A2BC")]
		set
		{
		}
	}

	// Token: 0x17000049 RID: 73
	// (get) Token: 0x06000172 RID: 370 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x06000171 RID: 369 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003D")]
	public Texture2D noiseTexture
	{
		[Token(Token = "0x600012B")]
		[Address(RVA = "0x10164A2D8", Offset = "0x164A2D8", VA = "0x10164A2D8")]
		get
		{
			return null;
		}
		[Token(Token = "0x600012A")]
		[Address(RVA = "0x10164A2D0", Offset = "0x164A2D0", VA = "0x10164A2D0")]
		set
		{
		}
	}

	// Token: 0x1700004A RID: 74
	// (get) Token: 0x06000174 RID: 372 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x06000173 RID: 371 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003E")]
	public Texture2D levelTexture
	{
		[Token(Token = "0x600012D")]
		[Address(RVA = "0x10164A2E8", Offset = "0x164A2E8", VA = "0x10164A2E8")]
		get
		{
			return null;
		}
		[Token(Token = "0x600012C")]
		[Address(RVA = "0x10164A2E0", Offset = "0x164A2E0", VA = "0x10164A2E0")]
		set
		{
		}
	}

	// Token: 0x06000175 RID: 373 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600012E")]
	[Address(RVA = "0x10164A2F0", Offset = "0x164A2F0", VA = "0x10164A2F0")]
	public void Setup()
	{
	}

	// Token: 0x06000176 RID: 374 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600012F")]
	[Address(RVA = "0x10164A4F4", Offset = "0x164A4F4", VA = "0x10164A4F4")]
	private void Start()
	{
	}

	// Token: 0x06000177 RID: 375 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000130")]
	[Address(RVA = "0x10164A4F8", Offset = "0x164A4F8", VA = "0x10164A4F8")]
	private void Update()
	{
	}

	// Token: 0x06000178 RID: 376 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000131")]
	[Address(RVA = "0x10164A784", Offset = "0x164A784", VA = "0x10164A784")]
	private void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
	}

	// Token: 0x06000179 RID: 377 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000132")]
	[Address(RVA = "0x10164A95C", Offset = "0x164A95C", VA = "0x10164A95C")]
	public ScrollDistortionRenderer()
	{
	}

	// Token: 0x040001D3 RID: 467
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x400012B")]
	[SerializeField]
	private float m_Level;

	// Token: 0x040001D4 RID: 468
	[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
	[Token(Token = "0x400012C")]
	[SerializeField]
	private Vector4 m_NoiseParam;

	// Token: 0x040001D5 RID: 469
	[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
	[Token(Token = "0x400012D")]
	[SerializeField]
	private Vector4 m_LevelParam;

	// Token: 0x040001D6 RID: 470
	[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
	[Token(Token = "0x400012E")]
	[SerializeField]
	private Vector2 m_NoiseSpeed;

	// Token: 0x040001D7 RID: 471
	[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
	[Token(Token = "0x400012F")]
	[SerializeField]
	private Vector2 m_LevelSpeed;

	// Token: 0x040001D8 RID: 472
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x4000130")]
	[SerializeField]
	private Texture2D m_NoiseTexture;

	// Token: 0x040001D9 RID: 473
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x4000131")]
	[SerializeField]
	private Texture2D m_LevelTexture;

	// Token: 0x040001DA RID: 474
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x4000132")]
	private float m_BackupLevel;

	// Token: 0x040001DB RID: 475
	[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
	[Token(Token = "0x4000133")]
	private Vector4 m_BackupNoiseParam;

	// Token: 0x040001DC RID: 476
	[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
	[Token(Token = "0x4000134")]
	private Vector4 m_BackupLevelParam;

	// Token: 0x040001DD RID: 477
	[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
	[Token(Token = "0x4000135")]
	private Material m_Material;

	// Token: 0x040001DE RID: 478
	[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
	[Token(Token = "0x4000136")]
	public RenderTexture m_SaveAreaTexture;
}
