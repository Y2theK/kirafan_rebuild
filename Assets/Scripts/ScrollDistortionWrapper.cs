﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200004C RID: 76
[Token(Token = "0x2000027")]
[StructLayout(3)]
public class ScrollDistortionWrapper : MonoBehaviour
{
	// Token: 0x1700004B RID: 75
	// (get) Token: 0x0600017B RID: 379 RVA: 0x00002778 File Offset: 0x00000978
	// (set) Token: 0x0600017A RID: 378 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x1700003F")]
	public float level
	{
		[Token(Token = "0x6000134")]
		[Address(RVA = "0x10164AAB0", Offset = "0x164AAB0", VA = "0x10164AAB0")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x6000133")]
		[Address(RVA = "0x10164AAA8", Offset = "0x164AAA8", VA = "0x10164AAA8")]
		set
		{
		}
	}

	// Token: 0x1700004C RID: 76
	// (get) Token: 0x0600017D RID: 381 RVA: 0x00002790 File Offset: 0x00000990
	// (set) Token: 0x0600017C RID: 380 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000040")]
	public Vector4 noiseParam
	{
		[Token(Token = "0x6000136")]
		[Address(RVA = "0x10164AACC", Offset = "0x164AACC", VA = "0x10164AACC")]
		get
		{
			return default(Vector4);
		}
		[Token(Token = "0x6000135")]
		[Address(RVA = "0x10164AAB8", Offset = "0x164AAB8", VA = "0x10164AAB8")]
		set
		{
		}
	}

	// Token: 0x1700004D RID: 77
	// (get) Token: 0x0600017F RID: 383 RVA: 0x000027A8 File Offset: 0x000009A8
	// (set) Token: 0x0600017E RID: 382 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000041")]
	public Vector4 levelParam
	{
		[Token(Token = "0x6000138")]
		[Address(RVA = "0x10164AAEC", Offset = "0x164AAEC", VA = "0x10164AAEC")]
		get
		{
			return default(Vector4);
		}
		[Token(Token = "0x6000137")]
		[Address(RVA = "0x10164AAD8", Offset = "0x164AAD8", VA = "0x10164AAD8")]
		set
		{
		}
	}

	// Token: 0x1700004E RID: 78
	// (get) Token: 0x06000181 RID: 385 RVA: 0x000027C0 File Offset: 0x000009C0
	// (set) Token: 0x06000180 RID: 384 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000042")]
	public Vector2 noiseSpeed
	{
		[Token(Token = "0x600013A")]
		[Address(RVA = "0x10164AB04", Offset = "0x164AB04", VA = "0x10164AB04")]
		get
		{
			return default(Vector2);
		}
		[Token(Token = "0x6000139")]
		[Address(RVA = "0x10164AAF8", Offset = "0x164AAF8", VA = "0x10164AAF8")]
		set
		{
		}
	}

	// Token: 0x1700004F RID: 79
	// (get) Token: 0x06000183 RID: 387 RVA: 0x000027D8 File Offset: 0x000009D8
	// (set) Token: 0x06000182 RID: 386 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000043")]
	public Vector2 levelSpeed
	{
		[Token(Token = "0x600013C")]
		[Address(RVA = "0x10164AB18", Offset = "0x164AB18", VA = "0x10164AB18")]
		get
		{
			return default(Vector2);
		}
		[Token(Token = "0x600013B")]
		[Address(RVA = "0x10164AB0C", Offset = "0x164AB0C", VA = "0x10164AB0C")]
		set
		{
		}
	}

	// Token: 0x17000050 RID: 80
	// (get) Token: 0x06000185 RID: 389 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x06000184 RID: 388 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000044")]
	public Texture2D noiseTexture
	{
		[Token(Token = "0x600013E")]
		[Address(RVA = "0x10164AB28", Offset = "0x164AB28", VA = "0x10164AB28")]
		get
		{
			return null;
		}
		[Token(Token = "0x600013D")]
		[Address(RVA = "0x10164AB20", Offset = "0x164AB20", VA = "0x10164AB20")]
		set
		{
		}
	}

	// Token: 0x17000051 RID: 81
	// (get) Token: 0x06000187 RID: 391 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x06000186 RID: 390 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000045")]
	public Texture2D levelTexture
	{
		[Token(Token = "0x6000140")]
		[Address(RVA = "0x10164AB38", Offset = "0x164AB38", VA = "0x10164AB38")]
		get
		{
			return null;
		}
		[Token(Token = "0x600013F")]
		[Address(RVA = "0x10164AB30", Offset = "0x164AB30", VA = "0x10164AB30")]
		set
		{
		}
	}

	// Token: 0x17000052 RID: 82
	// (get) Token: 0x06000189 RID: 393 RVA: 0x000027F0 File Offset: 0x000009F0
	// (set) Token: 0x06000188 RID: 392 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000046")]
	public bool isChildrenAlsoAffect
	{
		[Token(Token = "0x6000142")]
		[Address(RVA = "0x10164AB48", Offset = "0x164AB48", VA = "0x10164AB48")]
		get
		{
			return default(bool);
		}
		[Token(Token = "0x6000141")]
		[Address(RVA = "0x10164AB40", Offset = "0x164AB40", VA = "0x10164AB40")]
		set
		{
		}
	}

	// Token: 0x17000053 RID: 83
	// (get) Token: 0x0600018B RID: 395 RVA: 0x00002808 File Offset: 0x00000A08
	// (set) Token: 0x0600018A RID: 394 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000047")]
	public int renderQueue
	{
		[Token(Token = "0x6000144")]
		[Address(RVA = "0x10164AB58", Offset = "0x164AB58", VA = "0x10164AB58")]
		get
		{
			return 0;
		}
		[Token(Token = "0x6000143")]
		[Address(RVA = "0x10164AB50", Offset = "0x164AB50", VA = "0x10164AB50")]
		set
		{
		}
	}

	// Token: 0x0600018C RID: 396 RVA: 0x00002820 File Offset: 0x00000A20
	[Token(Token = "0x6000145")]
	[Address(RVA = "0x10164AB60", Offset = "0x164AB60", VA = "0x10164AB60")]
	public bool IsAvailable()
	{
		return default(bool);
	}

	// Token: 0x0600018D RID: 397 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000146")]
	[Address(RVA = "0x10164AB80", Offset = "0x164AB80", VA = "0x10164AB80")]
	public void Setup()
	{
	}

	// Token: 0x0600018E RID: 398 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000147")]
	[Address(RVA = "0x10164B76C", Offset = "0x164B76C", VA = "0x10164B76C")]
	private void RevertMaterial()
	{
	}

	// Token: 0x0600018F RID: 399 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000148")]
	[Address(RVA = "0x10164B88C", Offset = "0x164B88C", VA = "0x10164B88C")]
	private void OnEnable()
	{
	}

	// Token: 0x06000190 RID: 400 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000149")]
	[Address(RVA = "0x10164B890", Offset = "0x164B890", VA = "0x10164B890")]
	private void OnDisable()
	{
	}

	// Token: 0x06000191 RID: 401 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600014A")]
	[Address(RVA = "0x10164B894", Offset = "0x164B894", VA = "0x10164B894")]
	private void OnDestroy()
	{
	}

	// Token: 0x06000192 RID: 402 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600014B")]
	[Address(RVA = "0x10164B8B8", Offset = "0x164B8B8", VA = "0x10164B8B8")]
	private void Start()
	{
	}

	// Token: 0x06000193 RID: 403 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600014C")]
	[Address(RVA = "0x10164B8BC", Offset = "0x164B8BC", VA = "0x10164B8BC")]
	private void Update()
	{
	}

	// Token: 0x06000194 RID: 404 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600014D")]
	[Address(RVA = "0x10164C0A4", Offset = "0x164C0A4", VA = "0x10164C0A4")]
	public ScrollDistortionWrapper()
	{
	}

	// Token: 0x040001DF RID: 479
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000137")]
	[SerializeField]
	private float m_Level;

	// Token: 0x040001E0 RID: 480
	[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
	[Token(Token = "0x4000138")]
	[SerializeField]
	private Vector4 m_NoiseParam;

	// Token: 0x040001E1 RID: 481
	[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
	[Token(Token = "0x4000139")]
	[SerializeField]
	private Vector4 m_LevelParam;

	// Token: 0x040001E2 RID: 482
	[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
	[Token(Token = "0x400013A")]
	[SerializeField]
	private Vector2 m_NoiseSpeed;

	// Token: 0x040001E3 RID: 483
	[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
	[Token(Token = "0x400013B")]
	[SerializeField]
	private Vector2 m_LevelSpeed;

	// Token: 0x040001E4 RID: 484
	[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
	[Token(Token = "0x400013C")]
	[SerializeField]
	private Texture2D m_NoiseTexture;

	// Token: 0x040001E5 RID: 485
	[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
	[Token(Token = "0x400013D")]
	[SerializeField]
	private Texture2D m_LevelTexture;

	// Token: 0x040001E6 RID: 486
	[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
	[Token(Token = "0x400013E")]
	[SerializeField]
	private bool m_isChildrenAlsoAffect;

	// Token: 0x040001E7 RID: 487
	[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
	[Token(Token = "0x400013F")]
	[SerializeField]
	private int m_RenderQueue;

	// Token: 0x040001E8 RID: 488
	[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
	[Token(Token = "0x4000140")]
	private Vector4 m_BackupNoiseParam;

	// Token: 0x040001E9 RID: 489
	[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
	[Token(Token = "0x4000141")]
	private Vector4 m_BackupLevelParam;

	// Token: 0x040001EA RID: 490
	[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
	[Token(Token = "0x4000142")]
	private bool m_BackupIsChildrenAlsoAffect;

	// Token: 0x040001EB RID: 491
	[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
	[Token(Token = "0x4000143")]
	private Material[] m_Materials;

	// Token: 0x040001EC RID: 492
	[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
	[Token(Token = "0x4000144")]
	private Material[] m_BackupMaterials;

	// Token: 0x040001ED RID: 493
	[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
	[Token(Token = "0x4000145")]
	private Image[] m_Images;
}
