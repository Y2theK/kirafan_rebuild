﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000054 RID: 84
[Token(Token = "0x200002F")]
[StructLayout(3)]
public class SelectedCharaInfoLevelNewLine : MonoBehaviour
{
	// Token: 0x060001DA RID: 474 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000193")]
	[Address(RVA = "0x10164C1EC", Offset = "0x164C1EC", VA = "0x10164C1EC")]
	private void Start()
	{
	}

	// Token: 0x060001DB RID: 475 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000194")]
	[Address(RVA = "0x10164C1F0", Offset = "0x164C1F0", VA = "0x10164C1F0")]
	private void Update()
	{
	}

	// Token: 0x060001DC RID: 476 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000195")]
	[Address(RVA = "0x10164C458", Offset = "0x164C458", VA = "0x10164C458")]
	public SelectedCharaInfoLevelNewLine()
	{
	}

	// Token: 0x040001F5 RID: 501
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x400014D")]
	[SerializeField]
	private Text m_Source;

	// Token: 0x040001F6 RID: 502
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x400014E")]
	[SerializeField]
	private Text m_BeforeDestination;

	// Token: 0x040001F7 RID: 503
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x400014F")]
	[SerializeField]
	private Text m_AfterDestination;

	// Token: 0x040001F8 RID: 504
	[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
	[Token(Token = "0x4000150")]
	[SerializeField]
	private SelectedCharaInfoLevelNewLine.eNewLineType m_NewLineType;

	// Token: 0x040001F9 RID: 505
	[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
	[Token(Token = "0x4000151")]
	private string m_PreSource;

	// Token: 0x02000055 RID: 85
	[Token(Token = "0x2000D00")]
	public enum eNewLineType
	{
		// Token: 0x040001FB RID: 507
		[Token(Token = "0x4005443")]
		BeforeSlash,
		// Token: 0x040001FC RID: 508
		[Token(Token = "0x4005444")]
		AfterSlash
	}
}
