﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x0200004D RID: 77
[Token(Token = "0x2000028")]
[StructLayout(3)]
public class ShadeOffRenderer : MonoBehaviour
{
	// Token: 0x17000054 RID: 84
	// (get) Token: 0x06000196 RID: 406 RVA: 0x00002838 File Offset: 0x00000A38
	// (set) Token: 0x06000195 RID: 405 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000048")]
	public float level
	{
		[Token(Token = "0x600014F")]
		[Address(RVA = "0x10164C468", Offset = "0x164C468", VA = "0x10164C468")]
		get
		{
			return 0f;
		}
		[Token(Token = "0x600014E")]
		[Address(RVA = "0x10164C460", Offset = "0x164C460", VA = "0x10164C460")]
		set
		{
		}
	}

	// Token: 0x17000055 RID: 85
	// (get) Token: 0x06000198 RID: 408 RVA: 0x00002052 File Offset: 0x00000252
	// (set) Token: 0x06000197 RID: 407 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x17000049")]
	public Vector4[] offsetArray
	{
		[Token(Token = "0x6000151")]
		[Address(RVA = "0x10164C478", Offset = "0x164C478", VA = "0x10164C478")]
		get
		{
			return null;
		}
		[Token(Token = "0x6000150")]
		[Address(RVA = "0x10164C470", Offset = "0x164C470", VA = "0x10164C470")]
		set
		{
		}
	}

	// Token: 0x06000199 RID: 409 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000152")]
	[Address(RVA = "0x10164C480", Offset = "0x164C480", VA = "0x10164C480")]
	public void Setup()
	{
	}

	// Token: 0x0600019A RID: 410 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000153")]
	[Address(RVA = "0x10164C5A8", Offset = "0x164C5A8", VA = "0x10164C5A8")]
	private void Start()
	{
	}

	// Token: 0x0600019B RID: 411 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000154")]
	[Address(RVA = "0x10164C5AC", Offset = "0x164C5AC", VA = "0x10164C5AC")]
	private void OnRenderImage(RenderTexture src, RenderTexture dst)
	{
	}

	// Token: 0x0600019C RID: 412 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x6000155")]
	[Address(RVA = "0x10164C928", Offset = "0x164C928", VA = "0x10164C928")]
	public ShadeOffRenderer()
	{
	}

	// Token: 0x040001EE RID: 494
	[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
	[Token(Token = "0x4000146")]
	[SerializeField]
	private float m_Level;

	// Token: 0x040001EF RID: 495
	[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
	[Token(Token = "0x4000147")]
	[SerializeField]
	private Vector4[] m_OffsetArray;

	// Token: 0x040001F0 RID: 496
	[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
	[Token(Token = "0x4000148")]
	private Material m_Material;
}
