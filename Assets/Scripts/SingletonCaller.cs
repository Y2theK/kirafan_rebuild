﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

// Token: 0x020000D8 RID: 216
[Token(Token = "0x20000A1")]
[StructLayout(3)]
public class SingletonCaller : MonoBehaviour
{
	// Token: 0x06000566 RID: 1382 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600050B")]
	[Address(RVA = "0x10164CAE4", Offset = "0x164CAE4", VA = "0x10164CAE4")]
	private void Awake()
	{
	}

	// Token: 0x06000567 RID: 1383 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600050C")]
	[Address(RVA = "0x10164CC50", Offset = "0x164CC50", VA = "0x10164CC50")]
	private void Update()
	{
	}

	// Token: 0x06000568 RID: 1384 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600050D")]
	[Address(RVA = "0x10164CCC8", Offset = "0x164CCC8", VA = "0x10164CCC8")]
	private void LateUpdate()
	{
	}

	// Token: 0x06000569 RID: 1385 RVA: 0x00002050 File Offset: 0x00000250
	[Token(Token = "0x600050E")]
	[Address(RVA = "0x10164CD40", Offset = "0x164CD40", VA = "0x10164CD40")]
	public SingletonCaller()
	{
	}

	// Token: 0x04000306 RID: 774
	[Token(Token = "0x40001D4")]
	private static SingletonCaller m_Instance;
}
