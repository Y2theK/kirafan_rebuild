﻿using Meige;
using System.Collections.Generic;

namespace Star {
    public class ABResourceLoader {
        private Dictionary<string, ABResourceObjectHandler> m_LoadingResources;
        private Dictionary<string, ABResourceObjectHandler> m_LoadedResources;
        private Dictionary<string, ABResourceObjectHandler> m_ReserveResources;
        private int m_SimultaneouslyNum = 32;
        private bool m_UnloadAllFlg;
        private bool m_IsUnloadIgnoreUnloadPathListOnUnloadAll;
        private List<string> m_IgnoreUnloadPathList;
        private List<string> m_RemoveKeys;

        public ABResourceLoader(int simultaneouslyNum = -1) {
            if (simultaneouslyNum > 0) {
                m_SimultaneouslyNum = simultaneouslyNum;
            }
            m_LoadedResources = new Dictionary<string, ABResourceObjectHandler>();
            m_LoadedResources = new Dictionary<string, ABResourceObjectHandler>();
            m_ReserveResources = new Dictionary<string, ABResourceObjectHandler>();
            m_RemoveKeys = new List<string>();
        }

        public void Update() {
            if (m_ReserveResources.Count > 0) {
                m_RemoveKeys.Clear();
                foreach (string path in m_ReserveResources.Keys) {
                    if (m_LoadingResources.Count >= m_SimultaneouslyNum) {
                        break;
                    }
                    ABResourceObjectHandler handler = m_ReserveResources[path];
                    handler.Hndl = MeigeResourceManager.LoadHandler(path, handler.Options);
                    m_LoadingResources.Add(handler.Path, handler);
                    m_RemoveKeys.Add(path);
                }
                for (int i = 0; i < m_RemoveKeys.Count; i++) {
                    m_ReserveResources.Remove(m_RemoveKeys[i]);
                }
            }
            if (m_LoadingResources.Count > 0) {
                m_RemoveKeys.Clear();
                foreach (string path in m_LoadingResources.Keys) {
                    ABResourceObjectHandler handler = m_LoadingResources[path];
                    if (handler.Update()) {
                        m_LoadedResources.Add(path, handler);
                        m_RemoveKeys.Add(path);
                    } else if (handler.IsError() && handler.RefCount <= 0) {
                        handler.Destroy();
                        m_RemoveKeys.Add(path);
                    }
                }
                for (int i = 0; i < m_RemoveKeys.Count; i++) {
                    m_LoadingResources.Remove(m_RemoveKeys[i]);
                }
            }
            if (m_LoadedResources.Count > 0) {
                m_RemoveKeys.Clear();
                foreach (string path in m_LoadedResources.Keys) {
                    ABResourceObjectHandler handler = m_LoadedResources[path];
                    if (handler.RefCount <= 0) {
                        if (!IsContainsIgnorePathList(handler.Path)) {
                            handler.Destroy();
                            m_RemoveKeys.Add(path);
                        }
                    }
                }
                for (int i = 0; i < m_RemoveKeys.Count; i++) {
                    m_LoadedResources.Remove(m_RemoveKeys[i]);
                }
            }
            if (m_UnloadAllFlg) {
                m_RemoveKeys.Clear();
                foreach (string path in m_LoadedResources.Keys) {
                    if (m_IsUnloadIgnoreUnloadPathListOnUnloadAll || !IsContainsIgnorePathList(path)) {
                        m_LoadedResources[path].Destroy();
                        m_RemoveKeys.Add(path);
                    }
                }
                for (int l = 0; l < m_RemoveKeys.Count; l++) {
                    m_LoadedResources.Remove(m_RemoveKeys[l]);
                }
                m_LoadingResources.Clear();
                m_ReserveResources.Clear();
                m_UnloadAllFlg = false;
                m_IsUnloadIgnoreUnloadPathListOnUnloadAll = false;
            }
        }

        public bool IsLoading() {
            return m_LoadingResources.Count > 0 || m_ReserveResources.Count > 0;
        }

        public bool IsExistLoadedObj() {
            return m_LoadedResources.Count > 0;
        }

        public bool IsError() {
            foreach (string key in m_LoadingResources.Keys) {
                if (m_LoadingResources[key].IsError()) {
                    return true;
                }
            }
            return false;
        }

        public ABResourceObjectHandler Load(string path, params MeigeResource.Option[] options) {
            if (m_UnloadAllFlg) {
                return null;
            }
            if (!m_LoadedResources.TryGetValue(path, out ABResourceObjectHandler handler)) {
                if (!m_LoadingResources.TryGetValue(path, out handler)) {
                    if (!m_ReserveResources.TryGetValue(path, out handler)) {
                        handler = new ABResourceObjectHandler(null, path, options);
                        m_ReserveResources.Add(path, handler);
                        return handler;
                    }
                }
            }
            handler.AddRef();
            return handler;
        }

        public void Unload(ABResourceObjectHandler unloadHndl) {
            if (unloadHndl == null) {
                return;
            }
            if (!m_LoadedResources.TryGetValue(unloadHndl.Path, out ABResourceObjectHandler handler)) {
                if (!m_LoadingResources.TryGetValue(unloadHndl.Path, out handler)) {
                    if (!m_ReserveResources.TryGetValue(unloadHndl.Path, out handler)) {
                        return;
                    }
                }
            }
            if (handler == unloadHndl) {
                handler.RemoveRef();
            }
        }

        public void Unload(string path) {
            if (!m_LoadedResources.TryGetValue(path, out ABResourceObjectHandler handler)) {
                if (!m_LoadingResources.TryGetValue(path, out handler)) {
                    if (!m_ReserveResources.TryGetValue(path, out handler)) {
                        return;
                    }
                }
            }
            handler.RemoveRef();
        }

        public void UnloadAll(bool isUnloadIgnoreUnloadPathListOnUnloadAll = false) {
            m_UnloadAllFlg = true;
            m_IsUnloadIgnoreUnloadPathListOnUnloadAll = isUnloadIgnoreUnloadPathListOnUnloadAll;
        }

        public bool IsCompleteUnloadAll() {
            return !m_UnloadAllFlg;
        }

        public void AddIgnoreUnloadPathList(string path) {
            if (m_IgnoreUnloadPathList == null) {
                m_IgnoreUnloadPathList = new List<string>();
            }
            if (!IsContainsIgnorePathList(path)) {
                m_IgnoreUnloadPathList.Add(path);
            }
        }

        public void RemoveIgnoreUnloadPathList(string path) {
            if (m_IgnoreUnloadPathList != null) {
                m_IgnoreUnloadPathList.Remove(path);
            }
        }

        private bool IsContainsIgnorePathList(string path) {
            if (m_IgnoreUnloadPathList != null) {
                for (int i = 0; i < m_IgnoreUnloadPathList.Count; i++) {
                    if (m_IgnoreUnloadPathList[i] == path) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
