﻿using Meige;
using Meige.AssetBundles;
using System;

namespace Star {
    public class ABResourceObjectHandler {
        private bool m_IsError;
        private bool m_IsDone;
        private bool m_DisableAutoRetry;

        public MeigeResource.Handler Hndl { get; set; }
        public MeigeResource.Option[] Options { get; set; }
        public string Path { get; private set; }
        public int RefCount { get; private set; }
        public UnityEngine.Object[] Objs { get; private set; }

        public ABResourceObjectHandler(MeigeResource.Handler hndl, string path, params MeigeResource.Option[] options) {
            Hndl = hndl;
            Path = path;
            Options = options;
            RefCount = 1;
            Objs = null;
            m_IsError = false;
            m_IsDone = false;
            m_DisableAutoRetry = false;
        }

        public int AddRef() {
            return ++RefCount;
        }

        public int RemoveRef() {
            return --RefCount;
        }

        public bool Update() {
            if (Hndl != null && Hndl.IsDone) {
                if (!Hndl.IsError) {
                    Objs = new UnityEngine.Object[Hndl.assets.Length];
                    for (int i = 0; i < Objs.Length; i++) {
                        Objs[i] = Hndl.assets[i];
                    }
                    MeigeResourceManager.UnloadHandler(Hndl);
                    Hndl = null;
                    m_IsDone = true;
                    return true;
                } else if (m_DisableAutoRetry) {
                    m_IsError = true;
                    m_IsDone = true;
                    MeigeResourceManager.ClearError(Hndl.resourceName);
                } else {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(Hndl);
                }
            }
            return false;
        }

        public bool IsDone() {
            return m_IsDone;
        }

        public bool IsError() {
            return m_IsError;
        }

        public void Retry() {
            if (m_IsError && !m_DisableAutoRetry) {
                MeigeResourceManager.RetryHandler(Hndl);
                m_IsError = false;
            }
        }

        public void SetDisableAutoRetry(bool disableAutoRetry) {
            m_DisableAutoRetry = disableAutoRetry;
        }

        public void Destroy() {
            if (Hndl != null) {
                if (Hndl.IsError) {
                    MeigeResourceManager.ClearError(Hndl.resourceName);
                }
                MeigeResourceManager.UnloadHandler(Hndl);
                Hndl = null;
            }
            if (Objs != null) {
                for (int i = 0; i < Objs.Length; i++) {
                    if (Objs[i] != null) {
                        Objs[i] = null;
                    }
                }
                Objs = null;
            }
            RefCount = 0;
        }

        public UnityEngine.Object FindObj(string findName, bool isIgnoreCase = true) {
            if (Objs != null) {
                for (int i = 0; i < Objs.Length; i++) {
                    try {
                        if (isIgnoreCase) {
                            if (Objs[i].name == findName) {
                                return Objs[i];
                            }
                        } else if (Objs[i].name.ToLower() == findName.ToLower()) {
                            return Objs[i];
                        }
                    } catch (Exception) { }
                }
            }
            return null;
        }

        public UnityEngine.Object FindObjContains(string findName) {
            if (Objs != null) {
                for (int i = 0; i < Objs.Length; i++) {
                    try {
                        if (Objs[i].name.Contains(findName)) {
                            return Objs[i];
                        }
                    } catch (Exception) { }
                }
            }
            return null;
        }
    }
}
