﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02001199 RID: 4505
	[Token(Token = "0x2000BB6")]
	[StructLayout(3)]
	public class ADVBackGround : MonoBehaviour
	{
		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x060059B9 RID: 22969 RVA: 0x0001D580 File Offset: 0x0001B780
		[Token(Token = "0x170005AB")]
		public bool IsEmpty
		{
			[Token(Token = "0x60052FD")]
			[Address(RVA = "0x10164EBDC", Offset = "0x164EBDC", VA = "0x10164EBDC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x060059BA RID: 22970 RVA: 0x0001D598 File Offset: 0x0001B798
		// (set) Token: 0x060059BB RID: 22971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005AC")]
		public uint ID
		{
			[Token(Token = "0x60052FE")]
			[Address(RVA = "0x10164EC68", Offset = "0x164EC68", VA = "0x10164EC68")]
			get
			{
				return 0U;
			}
			[Token(Token = "0x60052FF")]
			[Address(RVA = "0x10164EC70", Offset = "0x164EC70", VA = "0x10164EC70")]
			set
			{
			}
		}

		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x060059BC RID: 22972 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005AD")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005300")]
			[Address(RVA = "0x10164EC78", Offset = "0x164EC78", VA = "0x10164EC78")]
			get
			{
				return null;
			}
		}

		// Token: 0x060059BD RID: 22973 RVA: 0x0001D5B0 File Offset: 0x0001B7B0
		[Token(Token = "0x6005301")]
		[Address(RVA = "0x10164ED10", Offset = "0x164ED10", VA = "0x10164ED10")]
		public bool IsMoving()
		{
			return default(bool);
		}

		// Token: 0x060059BE RID: 22974 RVA: 0x0001D5C8 File Offset: 0x0001B7C8
		[Token(Token = "0x6005302")]
		[Address(RVA = "0x10164ED18", Offset = "0x164ED18", VA = "0x10164ED18")]
		public bool IsScale()
		{
			return default(bool);
		}

		// Token: 0x060059BF RID: 22975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005303")]
		[Address(RVA = "0x10164ED20", Offset = "0x164ED20", VA = "0x10164ED20")]
		public void Initialize()
		{
		}

		// Token: 0x060059C0 RID: 22976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005304")]
		[Address(RVA = "0x10164F148", Offset = "0x164F148", VA = "0x10164F148")]
		private void OnDestroy()
		{
		}

		// Token: 0x060059C1 RID: 22977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005305")]
		[Address(RVA = "0x10164F14C", Offset = "0x164F14C", VA = "0x10164F14C")]
		public void Destroy()
		{
		}

		// Token: 0x060059C2 RID: 22978 RVA: 0x0001D5E0 File Offset: 0x0001B7E0
		[Token(Token = "0x6005306")]
		[Address(RVA = "0x10164F180", Offset = "0x164F180", VA = "0x10164F180")]
		private Vector3 AnchorPosToLocalPos(Vector2 pos)
		{
			return default(Vector3);
		}

		// Token: 0x060059C3 RID: 22979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005307")]
		[Address(RVA = "0x10164F418", Offset = "0x164F418", VA = "0x10164F418")]
		public void MoveTo(Vector2 pos, float sec, eADVCurveType curveType)
		{
		}

		// Token: 0x060059C4 RID: 22980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005308")]
		[Address(RVA = "0x10164F7FC", Offset = "0x164F7FC", VA = "0x10164F7FC")]
		public void OnCompleteMove()
		{
		}

		// Token: 0x060059C5 RID: 22981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005309")]
		[Address(RVA = "0x10164F804", Offset = "0x164F804", VA = "0x10164F804")]
		public void ScaleTo(float scale, float sec, eADVCurveType curveType)
		{
		}

		// Token: 0x060059C6 RID: 22982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530A")]
		[Address(RVA = "0x10164FBB4", Offset = "0x164FBB4", VA = "0x10164FBB4")]
		public void PointScaleTo(float scale, float sec, float x, float y, eADVCurveType curveType)
		{
		}

		// Token: 0x060059C7 RID: 22983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530B")]
		[Address(RVA = "0x10164FF04", Offset = "0x164FF04", VA = "0x10164FF04")]
		public void OnCompleteScale()
		{
		}

		// Token: 0x060059C8 RID: 22984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530C")]
		[Address(RVA = "0x10164FF0C", Offset = "0x164FF0C", VA = "0x10164FF0C")]
		public void ChangeColor(Color startColor, Color endColor, float sec, eADVCurveType curveType)
		{
		}

		// Token: 0x060059C9 RID: 22985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530D")]
		[Address(RVA = "0x10165029C", Offset = "0x165029C", VA = "0x10165029C")]
		public void OnUpdateColorChange(float value)
		{
		}

		// Token: 0x060059CA RID: 22986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530E")]
		[Address(RVA = "0x10164EF4C", Offset = "0x164EF4C", VA = "0x10164EF4C")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x060059CB RID: 22987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600530F")]
		[Address(RVA = "0x101650378", Offset = "0x1650378", VA = "0x101650378")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x060059CC RID: 22988 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005310")]
		[Address(RVA = "0x1016503B0", Offset = "0x16503B0", VA = "0x1016503B0")]
		public Sprite GetSprite()
		{
			return null;
		}

		// Token: 0x060059CD RID: 22989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005311")]
		[Address(RVA = "0x10164EF88", Offset = "0x164EF88", VA = "0x10164EF88")]
		public void SetAnchor(eADVScreenAnchor anchor)
		{
		}

		// Token: 0x060059CE RID: 22990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005312")]
		[Address(RVA = "0x10164F100", Offset = "0x164F100", VA = "0x10164F100")]
		public void SetPivot(Vector2 pivot)
		{
		}

		// Token: 0x060059CF RID: 22991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005313")]
		[Address(RVA = "0x1016503E0", Offset = "0x16503E0", VA = "0x1016503E0")]
		public void SetDirty()
		{
		}

		// Token: 0x060059D0 RID: 22992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005314")]
		[Address(RVA = "0x101650418", Offset = "0x1650418", VA = "0x101650418")]
		public ADVBackGround()
		{
		}

		// Token: 0x04006C87 RID: 27783
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D0D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133EB8", Offset = "0x133EB8")]
		private Image m_Image;

		// Token: 0x04006C88 RID: 27784
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D0E")]
		private uint m_ID;

		// Token: 0x04006C89 RID: 27785
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004D0F")]
		private Color m_StartColor;

		// Token: 0x04006C8A RID: 27786
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4004D10")]
		private Color m_EndColor;

		// Token: 0x04006C8B RID: 27787
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004D11")]
		private bool m_IsMoving;

		// Token: 0x04006C8C RID: 27788
		[Cpp2IlInjected.FieldOffset(Offset = "0x45")]
		[Token(Token = "0x4004D12")]
		private bool m_IsScale;

		// Token: 0x04006C8D RID: 27789
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004D13")]
		private float m_FinalScale;

		// Token: 0x04006C8E RID: 27790
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004D14")]
		private GameObject m_GameObject;

		// Token: 0x04006C8F RID: 27791
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004D15")]
		private RectTransform m_RectTransform;
	}
}
