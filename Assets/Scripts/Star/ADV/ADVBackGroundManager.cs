﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200119A RID: 4506
	[Token(Token = "0x2000BB7")]
	[StructLayout(3)]
	public class ADVBackGroundManager : MonoBehaviour
	{
		// Token: 0x060059D1 RID: 22993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005315")]
		[Address(RVA = "0x101650428", Offset = "0x1650428", VA = "0x101650428")]
		private void Awake()
		{
		}

		// Token: 0x060059D2 RID: 22994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005316")]
		[Address(RVA = "0x10165042C", Offset = "0x165042C", VA = "0x10165042C")]
		public void Setup()
		{
		}

		// Token: 0x060059D3 RID: 22995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005317")]
		[Address(RVA = "0x1016505BC", Offset = "0x16505BC", VA = "0x1016505BC")]
		private void OnDestroy()
		{
		}

		// Token: 0x060059D4 RID: 22996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005318")]
		[Address(RVA = "0x1016505C0", Offset = "0x16505C0", VA = "0x1016505C0")]
		public void Destroy()
		{
		}

		// Token: 0x060059D5 RID: 22997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005319")]
		[Address(RVA = "0x1016508F8", Offset = "0x16508F8", VA = "0x1016508F8")]
		public void AddLoad(int useIdx, string fileName)
		{
		}

		// Token: 0x060059D6 RID: 22998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600531A")]
		[Address(RVA = "0x1016509CC", Offset = "0x16509CC", VA = "0x1016509CC")]
		public void ClearLoadParamList()
		{
		}

		// Token: 0x060059D7 RID: 22999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600531B")]
		[Address(RVA = "0x101650A2C", Offset = "0x1650A2C", VA = "0x101650A2C")]
		public void LoadFirst()
		{
		}

		// Token: 0x060059D8 RID: 23000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600531C")]
		[Address(RVA = "0x101650810", Offset = "0x1650810", VA = "0x101650810")]
		private void Unload(ADVBackGroundManager.SpriteHandlerFileNamePair pair)
		{
		}

		// Token: 0x060059D9 RID: 23001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600531D")]
		[Address(RVA = "0x101650AD0", Offset = "0x1650AD0", VA = "0x101650AD0")]
		private void LoadRecently()
		{
		}

		// Token: 0x060059DA RID: 23002 RVA: 0x0001D5F8 File Offset: 0x0001B7F8
		[Token(Token = "0x600531E")]
		[Address(RVA = "0x101650D78", Offset = "0x1650D78", VA = "0x101650D78")]
		public bool IsDoneLoaded(int cmdIdx)
		{
			return default(bool);
		}

		// Token: 0x060059DB RID: 23003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600531F")]
		[Address(RVA = "0x101650EB8", Offset = "0x1650EB8", VA = "0x101650EB8")]
		public void Visible(uint id, string fileName)
		{
		}

		// Token: 0x060059DC RID: 23004 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005320")]
		[Address(RVA = "0x1016513B0", Offset = "0x16513B0", VA = "0x1016513B0")]
		public SpriteHandler GetSpriteHandler(string fileName)
		{
			return null;
		}

		// Token: 0x060059DD RID: 23005 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005321")]
		[Address(RVA = "0x1016512DC", Offset = "0x16512DC", VA = "0x1016512DC")]
		public ADVBackGround GetEmptyInstance()
		{
			return null;
		}

		// Token: 0x060059DE RID: 23006 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005322")]
		[Address(RVA = "0x1016511B8", Offset = "0x16511B8", VA = "0x1016511B8")]
		public ADVBackGround GetActiveMatchIdInstance(uint id)
		{
			return null;
		}

		// Token: 0x060059DF RID: 23007 RVA: 0x0001D610 File Offset: 0x0001B810
		[Token(Token = "0x6005323")]
		[Address(RVA = "0x1016514BC", Offset = "0x16514BC", VA = "0x1016514BC")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x060059E0 RID: 23008 RVA: 0x0001D628 File Offset: 0x0001B828
		[Token(Token = "0x6005324")]
		[Address(RVA = "0x1016515FC", Offset = "0x16515FC", VA = "0x1016515FC")]
		public bool IsMoving(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x060059E1 RID: 23009 RVA: 0x0001D640 File Offset: 0x0001B840
		[Token(Token = "0x6005325")]
		[Address(RVA = "0x101651700", Offset = "0x1651700", VA = "0x101651700")]
		public bool IsScale(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x060059E2 RID: 23010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005326")]
		[Address(RVA = "0x101651804", Offset = "0x1651804", VA = "0x101651804")]
		public void SetDirty()
		{
		}

		// Token: 0x060059E3 RID: 23011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005327")]
		[Address(RVA = "0x1016518C0", Offset = "0x16518C0", VA = "0x1016518C0")]
		public ADVBackGroundManager()
		{
		}

		// Token: 0x04006C90 RID: 27792
		[Token(Token = "0x4004D16")]
		private const int FIRSTLOAD = 3;

		// Token: 0x04006C91 RID: 27793
		[Token(Token = "0x4004D17")]
		private const int LOAD_MAX = 4;

		// Token: 0x04006C92 RID: 27794
		[Token(Token = "0x4004D18")]
		private const int BG_MAX = 4;

		// Token: 0x04006C93 RID: 27795
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D19")]
		public List<ADVBackGroundManager.LoadParam> m_LoadParamList;

		// Token: 0x04006C94 RID: 27796
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D1A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133F04", Offset = "0x133F04")]
		private RectTransform m_ShakeBGParent;

		// Token: 0x04006C95 RID: 27797
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D1B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133F50", Offset = "0x133F50")]
		private ADVBackGround m_Prefab;

		// Token: 0x04006C96 RID: 27798
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004D1C")]
		private List<ADVBackGroundManager.SpriteHandlerFileNamePair> m_SpriteHandlerFileNamePairs;

		// Token: 0x04006C97 RID: 27799
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004D1D")]
		private ADVBackGround[] m_Insts;

		// Token: 0x0200119B RID: 4507
		[Token(Token = "0x20012A3")]
		public class LoadParam
		{
			// Token: 0x060059E4 RID: 23012 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636C")]
			[Address(RVA = "0x101651930", Offset = "0x1651930", VA = "0x101651930")]
			private LoadParam()
			{
			}

			// Token: 0x060059E5 RID: 23013 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636D")]
			[Address(RVA = "0x101650990", Offset = "0x1650990", VA = "0x101650990")]
			public LoadParam(int idx, string fileName)
			{
			}

			// Token: 0x04006C98 RID: 27800
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072A8")]
			public int m_UseIdx;

			// Token: 0x04006C99 RID: 27801
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072A9")]
			public string m_FileName;
		}

		// Token: 0x0200119C RID: 4508
		[Token(Token = "0x20012A4")]
		public class SpriteHandlerFileNamePair
		{
			// Token: 0x060059E6 RID: 23014 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636E")]
			[Address(RVA = "0x101650D38", Offset = "0x1650D38", VA = "0x101650D38")]
			public SpriteHandlerFileNamePair(SpriteHandler handler, string fileName, int useIdx)
			{
			}

			// Token: 0x04006C9A RID: 27802
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072AA")]
			public SpriteHandler m_Handler;

			// Token: 0x04006C9B RID: 27803
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072AB")]
			public string m_FileName;

			// Token: 0x04006C9C RID: 27804
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072AC")]
			public int m_UseIdx;
		}
	}
}
