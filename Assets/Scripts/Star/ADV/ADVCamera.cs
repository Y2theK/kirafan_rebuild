﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200119D RID: 4509
	[Token(Token = "0x2000BB8")]
	[StructLayout(3)]
	public class ADVCamera : MonoBehaviour
	{
		// Token: 0x060059E7 RID: 23015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005328")]
		[Address(RVA = "0x101651938", Offset = "0x1651938", VA = "0x101651938")]
		private void Start()
		{
		}

		// Token: 0x060059E8 RID: 23016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005329")]
		[Address(RVA = "0x10165193C", Offset = "0x165193C", VA = "0x10165193C")]
		private void Fix()
		{
		}

		// Token: 0x060059E9 RID: 23017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532A")]
		[Address(RVA = "0x101651A40", Offset = "0x1651A40", VA = "0x101651A40")]
		public ADVCamera()
		{
		}

		// Token: 0x04006C9D RID: 27805
		[Token(Token = "0x4004D1E")]
		private const float DEFAULT_ASPECT = 0.5622189f;

		// Token: 0x04006C9E RID: 27806
		[Token(Token = "0x4004D1F")]
		private const float DEFAULT_ORTHOGRAPHSIZE = 0.5f;
	}
}
