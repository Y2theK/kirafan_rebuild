﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x0200119E RID: 4510
	[Token(Token = "0x2000BB9")]
	[StructLayout(3)]
	public class ADVCaption : MonoBehaviour
	{
		// Token: 0x060059EA RID: 23018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532B")]
		[Address(RVA = "0x101651A48", Offset = "0x1651A48", VA = "0x101651A48")]
		public void Setup(ADVPlayer player)
		{
		}

		// Token: 0x060059EB RID: 23019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532C")]
		[Address(RVA = "0x101651E70", Offset = "0x1651E70", VA = "0x101651E70")]
		public void Destroy()
		{
		}

		// Token: 0x060059EC RID: 23020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532D")]
		[Address(RVA = "0x101651B4C", Offset = "0x1651B4C", VA = "0x101651B4C")]
		public void SetupDefault()
		{
		}

		// Token: 0x060059ED RID: 23021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532E")]
		[Address(RVA = "0x101652CE4", Offset = "0x1652CE4", VA = "0x101652CE4")]
		public void SetColor(float r, float g, float b)
		{
		}

		// Token: 0x060059EE RID: 23022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600532F")]
		[Address(RVA = "0x101651F28", Offset = "0x1651F28", VA = "0x101651F28")]
		public void SetCaptionBGStart(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059EF RID: 23023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005330")]
		[Address(RVA = "0x101652040", Offset = "0x1652040", VA = "0x101652040")]
		public void SetCaptionBGIdle(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059F0 RID: 23024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005331")]
		[Address(RVA = "0x10165215C", Offset = "0x165215C", VA = "0x10165215C")]
		public void SetCaptionBGEnd(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059F1 RID: 23025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005332")]
		[Address(RVA = "0x101652278", Offset = "0x1652278", VA = "0x101652278")]
		public void SetCaptionBGEaseIn(float sec, float delay, eADVCurveType curveType)
		{
		}

		// Token: 0x060059F2 RID: 23026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005333")]
		[Address(RVA = "0x101652354", Offset = "0x1652354", VA = "0x101652354")]
		public void SetCaptionBGEaseOut(float sec, float delay, eADVCurveType curveType)
		{
		}

		// Token: 0x060059F3 RID: 23027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005334")]
		[Address(RVA = "0x101652434", Offset = "0x1652434", VA = "0x101652434")]
		public void SetCaptionTextStart(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059F4 RID: 23028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005335")]
		[Address(RVA = "0x10165254C", Offset = "0x165254C", VA = "0x10165254C")]
		public void SetCaptionTextIdle(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059F5 RID: 23029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005336")]
		[Address(RVA = "0x101652668", Offset = "0x1652668", VA = "0x101652668")]
		public void SetCaptionTextEnd(Vector2 pos, Vector2 scl, Color rgba)
		{
		}

		// Token: 0x060059F6 RID: 23030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005337")]
		[Address(RVA = "0x101652784", Offset = "0x1652784", VA = "0x101652784")]
		public void SetCaptionTextEaseIn(float sec, float delay, eADVCurveType curveType)
		{
		}

		// Token: 0x060059F7 RID: 23031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005338")]
		[Address(RVA = "0x101652860", Offset = "0x1652860", VA = "0x101652860")]
		public void SetCaptionTextEaseOut(float sec, float delay, eADVCurveType curveType)
		{
		}

		// Token: 0x060059F8 RID: 23032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005339")]
		[Address(RVA = "0x101652940", Offset = "0x1652940", VA = "0x101652940")]
		public void SetCaptionBGSprite(string fileName, float width, float height)
		{
		}

		// Token: 0x060059F9 RID: 23033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533A")]
		[Address(RVA = "0x1016529A4", Offset = "0x16529A4", VA = "0x1016529A4")]
		public void SetCaptionTextAlignment(eADVAnchor anchor)
		{
		}

		// Token: 0x060059FA RID: 23034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533B")]
		[Address(RVA = "0x101652EC0", Offset = "0x1652EC0", VA = "0x101652EC0")]
		public void AddUseSprite(string fileName)
		{
		}

		// Token: 0x060059FB RID: 23035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533C")]
		[Address(RVA = "0x101651AEC", Offset = "0x1651AEC", VA = "0x101651AEC")]
		public void ClearUseSprite()
		{
		}

		// Token: 0x060059FC RID: 23036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533D")]
		[Address(RVA = "0x101653030", Offset = "0x1653030", VA = "0x101653030")]
		private void Start()
		{
		}

		// Token: 0x060059FD RID: 23037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533E")]
		[Address(RVA = "0x10165381C", Offset = "0x165381C", VA = "0x10165381C")]
		private void Update()
		{
		}

		// Token: 0x060059FE RID: 23038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600533F")]
		[Address(RVA = "0x10165397C", Offset = "0x165397C", VA = "0x10165397C")]
		public void Play(string text)
		{
		}

		// Token: 0x060059FF RID: 23039 RVA: 0x0001D658 File Offset: 0x0001B858
		[Token(Token = "0x6005340")]
		[Address(RVA = "0x101653B98", Offset = "0x1653B98", VA = "0x101653B98")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06005A00 RID: 23040 RVA: 0x0001D670 File Offset: 0x0001B870
		[Token(Token = "0x6005341")]
		[Address(RVA = "0x101653BB8", Offset = "0x1653BB8", VA = "0x101653BB8")]
		public float GetIdleTime()
		{
			return 0f;
		}

		// Token: 0x06005A01 RID: 23041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005342")]
		[Address(RVA = "0x101653BD0", Offset = "0x1653BD0", VA = "0x101653BD0")]
		public void Tap()
		{
		}

		// Token: 0x06005A02 RID: 23042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005343")]
		[Address(RVA = "0x101653038", Offset = "0x1653038", VA = "0x101653038")]
		private void ChangeState(ADVCaption.eState state)
		{
		}

		// Token: 0x06005A03 RID: 23043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005344")]
		[Address(RVA = "0x101653EEC", Offset = "0x1653EEC", VA = "0x101653EEC")]
		private void UpdateBG(ADVCaption.eState state, float rate)
		{
		}

		// Token: 0x06005A04 RID: 23044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005345")]
		[Address(RVA = "0x1016542AC", Offset = "0x16542AC", VA = "0x1016542AC")]
		private void UpdateText(ADVCaption.eState state, float rate)
		{
		}

		// Token: 0x06005A05 RID: 23045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005346")]
		[Address(RVA = "0x10165471C", Offset = "0x165471C", VA = "0x10165471C")]
		private void OnUpdateBGEase(float t)
		{
		}

		// Token: 0x06005A06 RID: 23046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005347")]
		[Address(RVA = "0x101654724", Offset = "0x1654724", VA = "0x101654724")]
		private void OnCompleteBGEase()
		{
		}

		// Token: 0x06005A07 RID: 23047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005348")]
		[Address(RVA = "0x101654770", Offset = "0x1654770", VA = "0x101654770")]
		private void OnUpdateTextEase(float t)
		{
		}

		// Token: 0x06005A08 RID: 23048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005349")]
		[Address(RVA = "0x101654778", Offset = "0x1654778", VA = "0x101654778")]
		private void OnCompleteTextEase()
		{
		}

		// Token: 0x06005A09 RID: 23049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600534A")]
		[Address(RVA = "0x10165472C", Offset = "0x165472C", VA = "0x10165472C")]
		private void UpdateState()
		{
		}

		// Token: 0x06005A0A RID: 23050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600534B")]
		[Address(RVA = "0x101653CE0", Offset = "0x1653CE0", VA = "0x101653CE0")]
		private void End()
		{
		}

		// Token: 0x06005A0B RID: 23051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600534C")]
		[Address(RVA = "0x101654780", Offset = "0x1654780", VA = "0x101654780")]
		public ADVCaption()
		{
		}

		// Token: 0x04006C9F RID: 27807
		[Token(Token = "0x4004D20")]
		public const float AUTO_SEC = 2f;

		// Token: 0x04006CA0 RID: 27808
		[Token(Token = "0x4004D21")]
		public const string INVALID_FILENAME = "�";

		// Token: 0x04006CA1 RID: 27809
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D22")]
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x04006CA2 RID: 27810
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D23")]
		[SerializeField]
		private Image m_BG;

		// Token: 0x04006CA3 RID: 27811
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D24")]
		private RectTransform m_BGRt;

		// Token: 0x04006CA4 RID: 27812
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004D25")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04006CA5 RID: 27813
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004D26")]
		private RectTransform m_TextRt;

		// Token: 0x04006CA6 RID: 27814
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004D27")]
		private CanvasGroup m_TextCanvasGroup;

		// Token: 0x04006CA7 RID: 27815
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004D28")]
		private ADVCaption.Pose[] m_BGPoses;

		// Token: 0x04006CA8 RID: 27816
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004D29")]
		private ADVCaption.Pose[] m_TextPoses;

		// Token: 0x04006CA9 RID: 27817
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004D2A")]
		private ADVCaption.Ease[] m_BGEases;

		// Token: 0x04006CAA RID: 27818
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004D2B")]
		private ADVCaption.Ease[] m_TextEases;

		// Token: 0x04006CAB RID: 27819
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004D2C")]
		private ADVCaption.eState m_State;

		// Token: 0x04006CAC RID: 27820
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004D2D")]
		private bool m_IsEasingBG;

		// Token: 0x04006CAD RID: 27821
		[Cpp2IlInjected.FieldOffset(Offset = "0x6D")]
		[Token(Token = "0x4004D2E")]
		private bool m_IsEasingText;

		// Token: 0x04006CAE RID: 27822
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004D2F")]
		private float m_IdleTime;

		// Token: 0x04006CAF RID: 27823
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004D30")]
		private List<string> m_SpriteNameList;

		// Token: 0x04006CB0 RID: 27824
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004D31")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04006CB1 RID: 27825
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004D32")]
		private string m_CurrentSprite;

		// Token: 0x04006CB2 RID: 27826
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004D33")]
		private string m_Reserve;

		// Token: 0x04006CB3 RID: 27827
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004D34")]
		private ADVPlayer m_Player;

		// Token: 0x0200119F RID: 4511
		[Token(Token = "0x20012A5")]
		private class Pose
		{
			// Token: 0x06005A0C RID: 23052 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636F")]
			[Address(RVA = "0x101652DFC", Offset = "0x1652DFC", VA = "0x101652DFC")]
			public Pose(Vector2 pos, Vector2 scl, Color col)
			{
			}

			// Token: 0x04006CB4 RID: 27828
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072AD")]
			public Vector2 m_Position;

			// Token: 0x04006CB5 RID: 27829
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072AE")]
			public Vector2 m_Scale;

			// Token: 0x04006CB6 RID: 27830
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072AF")]
			public Color m_Color;
		}

		// Token: 0x020011A0 RID: 4512
		[Token(Token = "0x20012A6")]
		private enum ePoseKey
		{
			// Token: 0x04006CB8 RID: 27832
			[Token(Token = "0x40072B1")]
			Start,
			// Token: 0x04006CB9 RID: 27833
			[Token(Token = "0x40072B2")]
			Idle,
			// Token: 0x04006CBA RID: 27834
			[Token(Token = "0x40072B3")]
			End,
			// Token: 0x04006CBB RID: 27835
			[Token(Token = "0x40072B4")]
			Num
		}

		// Token: 0x020011A1 RID: 4513
		[Token(Token = "0x20012A7")]
		private class Ease
		{
			// Token: 0x06005A0D RID: 23053 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006370")]
			[Address(RVA = "0x101652E80", Offset = "0x1652E80", VA = "0x101652E80")]
			public Ease(float sec, float delay, eADVCurveType curveType)
			{
			}

			// Token: 0x04006CBC RID: 27836
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072B5")]
			public float m_Sec;

			// Token: 0x04006CBD RID: 27837
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40072B6")]
			public float m_Delay;

			// Token: 0x04006CBE RID: 27838
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072B7")]
			public eADVCurveType m_CurveType;
		}

		// Token: 0x020011A2 RID: 4514
		[Token(Token = "0x20012A8")]
		private enum eEaseKey
		{
			// Token: 0x04006CC0 RID: 27840
			[Token(Token = "0x40072B9")]
			In,
			// Token: 0x04006CC1 RID: 27841
			[Token(Token = "0x40072BA")]
			Out,
			// Token: 0x04006CC2 RID: 27842
			[Token(Token = "0x40072BB")]
			Num
		}

		// Token: 0x020011A3 RID: 4515
		[Token(Token = "0x20012A9")]
		private enum eState
		{
			// Token: 0x04006CC4 RID: 27844
			[Token(Token = "0x40072BD")]
			Hide,
			// Token: 0x04006CC5 RID: 27845
			[Token(Token = "0x40072BE")]
			In,
			// Token: 0x04006CC6 RID: 27846
			[Token(Token = "0x40072BF")]
			Idle,
			// Token: 0x04006CC7 RID: 27847
			[Token(Token = "0x40072C0")]
			Out
		}
	}
}
