﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011A4 RID: 4516
	[Token(Token = "0x2000BBA")]
	[StructLayout(3)]
	public class ADVDataBase
	{
		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06005A0E RID: 23054 RVA: 0x0001D688 File Offset: 0x0001B888
		[Token(Token = "0x170005AE")]
		public bool IsDoneLoad
		{
			[Token(Token = "0x600534D")]
			[Address(RVA = "0x101654840", Offset = "0x1654840", VA = "0x101654840")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700060B RID: 1547
		// (get) Token: 0x06005A0F RID: 23055 RVA: 0x0001D6A0 File Offset: 0x0001B8A0
		[Token(Token = "0x170005AF")]
		public bool IsError
		{
			[Token(Token = "0x600534E")]
			[Address(RVA = "0x101654848", Offset = "0x1654848", VA = "0x101654848")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005A10 RID: 23056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600534F")]
		[Address(RVA = "0x101654850", Offset = "0x1654850", VA = "0x101654850")]
		public void Init()
		{
		}

		// Token: 0x06005A11 RID: 23057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005350")]
		[Address(RVA = "0x10165490C", Offset = "0x165490C", VA = "0x10165490C")]
		public void Prepare()
		{
		}

		// Token: 0x06005A12 RID: 23058 RVA: 0x0001D6B8 File Offset: 0x0001B8B8
		[Token(Token = "0x6005351")]
		[Address(RVA = "0x1016549B4", Offset = "0x16549B4", VA = "0x1016549B4")]
		public bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005A13 RID: 23059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005352")]
		[Address(RVA = "0x101655058", Offset = "0x1655058", VA = "0x101655058")]
		public void Destroy()
		{
		}

		// Token: 0x06005A14 RID: 23060 RVA: 0x0001D6D0 File Offset: 0x0001B8D0
		[Token(Token = "0x6005353")]
		[Address(RVA = "0x101655094", Offset = "0x1655094", VA = "0x101655094")]
		public bool UpdateDestroy()
		{
			return default(bool);
		}

		// Token: 0x06005A15 RID: 23061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005354")]
		[Address(RVA = "0x1016550E8", Offset = "0x16550E8", VA = "0x1016550E8")]
		public ADVDataBase()
		{
		}

		// Token: 0x04006CC8 RID: 27848
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004D35")]
		private ABResourceLoader m_Loader;

		// Token: 0x04006CC9 RID: 27849
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D36")]
		private ABResourceObjectHandler m_ResourceHandler;

		// Token: 0x04006CCA RID: 27850
		[Token(Token = "0x4004D37")]
		private const string RESOURCE_PATH = "adv/advdatabase/advdatabase.muast";

		// Token: 0x04006CCB RID: 27851
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D38")]
		private readonly string[] OBJ_NAME;

		// Token: 0x04006CCC RID: 27852
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D39")]
		public ADVCharacterListDB m_CharaList;

		// Token: 0x04006CCD RID: 27853
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004D3A")]
		public ADVEmotionListDB m_EmotionList;

		// Token: 0x04006CCE RID: 27854
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004D3B")]
		public ADVMotionListDB m_MotionList;

		// Token: 0x04006CCF RID: 27855
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004D3C")]
		public ADVTextTagArgDB m_TextTagArgList;

		// Token: 0x04006CD0 RID: 27856
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004D3D")]
		public Dictionary<string, int> m_CharaDBIndices;

		// Token: 0x04006CD1 RID: 27857
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004D3E")]
		public Dictionary<string, int> m_MotionDBIndices;

		// Token: 0x04006CD2 RID: 27858
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004D3F")]
		public Dictionary<string, int> m_EmotionDBIndices;

		// Token: 0x04006CD3 RID: 27859
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004D40")]
		public Dictionary<string, int> m_TextTagDBIndices;

		// Token: 0x04006CD4 RID: 27860
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004D41")]
		public bool m_IsDoneLoad;

		// Token: 0x04006CD5 RID: 27861
		[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
		[Token(Token = "0x4004D42")]
		private bool m_IsError;

		// Token: 0x020011A5 RID: 4517
		[Token(Token = "0x20012AA")]
		private enum eADVDatabase
		{
			// Token: 0x04006CD7 RID: 27863
			[Token(Token = "0x40072C2")]
			CharaList,
			// Token: 0x04006CD8 RID: 27864
			[Token(Token = "0x40072C3")]
			EmotionList,
			// Token: 0x04006CD9 RID: 27865
			[Token(Token = "0x40072C4")]
			MotionList,
			// Token: 0x04006CDA RID: 27866
			[Token(Token = "0x40072C5")]
			TextTagArg,
			// Token: 0x04006CDB RID: 27867
			[Token(Token = "0x40072C6")]
			Num
		}
	}
}
