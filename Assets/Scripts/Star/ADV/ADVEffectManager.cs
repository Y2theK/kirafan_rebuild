﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x020011A6 RID: 4518
	[Token(Token = "0x2000BBB")]
	[StructLayout(3)]
	public class ADVEffectManager : MonoBehaviour
	{
		// Token: 0x06005A16 RID: 23062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005355")]
		[Address(RVA = "0x1016552A8", Offset = "0x16552A8", VA = "0x1016552A8")]
		public void SetEnablePlaySE(bool flg)
		{
		}

		// Token: 0x06005A17 RID: 23063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005356")]
		[Address(RVA = "0x1016552B0", Offset = "0x16552B0", VA = "0x1016552B0")]
		public void Prepare(ADVPlayer player)
		{
		}

		// Token: 0x06005A18 RID: 23064 RVA: 0x0001D6E8 File Offset: 0x0001B8E8
		[Token(Token = "0x6005357")]
		[Address(RVA = "0x101655398", Offset = "0x1655398", VA = "0x101655398")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005A19 RID: 23065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005358")]
		[Address(RVA = "0x101655418", Offset = "0x1655418", VA = "0x101655418")]
		private void Update()
		{
		}

		// Token: 0x06005A1A RID: 23066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005359")]
		[Address(RVA = "0x10165633C", Offset = "0x165633C", VA = "0x10165633C")]
		private void LateUpdate()
		{
		}

		// Token: 0x06005A1B RID: 23067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535A")]
		[Address(RVA = "0x101656340", Offset = "0x1656340", VA = "0x101656340")]
		public void ClearNeedList()
		{
		}

		// Token: 0x06005A1C RID: 23068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535B")]
		[Address(RVA = "0x1016563A0", Offset = "0x16563A0", VA = "0x1016563A0")]
		public void AddNeedList(string effectID)
		{
		}

		// Token: 0x06005A1D RID: 23069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535C")]
		[Address(RVA = "0x101656410", Offset = "0x1656410", VA = "0x101656410")]
		public void AddNeedListByEmotionID(string emotionID)
		{
		}

		// Token: 0x06005A1E RID: 23070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535D")]
		[Address(RVA = "0x1016564D8", Offset = "0x16564D8", VA = "0x1016564D8")]
		public void UnloadAllNotNeedEffect()
		{
		}

		// Token: 0x06005A1F RID: 23071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535E")]
		[Address(RVA = "0x101656870", Offset = "0x1656870", VA = "0x101656870")]
		public void LoadAllNeedEffect()
		{
		}

		// Token: 0x06005A20 RID: 23072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600535F")]
		[Address(RVA = "0x10165692C", Offset = "0x165692C", VA = "0x10165692C")]
		public void LoadEffect(string effectID)
		{
		}

		// Token: 0x06005A21 RID: 23073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005360")]
		[Address(RVA = "0x101656A74", Offset = "0x1656A74", VA = "0x101656A74")]
		public void DestroyAllEffect()
		{
		}

		// Token: 0x06005A22 RID: 23074 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005361")]
		[Address(RVA = "0x101656490", Offset = "0x1656490", VA = "0x101656490")]
		private string GetEffectIDByEmotionID(string emotionID)
		{
			return null;
		}

		// Token: 0x06005A23 RID: 23075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005362")]
		[Address(RVA = "0x101656CD4", Offset = "0x1656CD4", VA = "0x101656CD4")]
		public void Play(string effectID, Vector3 position, Quaternion rotation, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005A24 RID: 23076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005363")]
		[Address(RVA = "0x101656CE8", Offset = "0x1656CE8", VA = "0x101656CE8")]
		public void PlayScreen(string effectID, Vector3 position, Quaternion rotation, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005A25 RID: 23077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005364")]
		[Address(RVA = "0x101656CFC", Offset = "0x1656CFC", VA = "0x101656CFC")]
		public void PlayEmo(string emotionID, Vector3 position, Quaternion rotation, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005A26 RID: 23078 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005365")]
		[Address(RVA = "0x10165631C", Offset = "0x165631C", VA = "0x10165631C")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, bool loop = false, bool isBehind = false)
		{
			return null;
		}

		// Token: 0x06005A27 RID: 23079 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005366")]
		[Address(RVA = "0x101656DAC", Offset = "0x1656DAC", VA = "0x101656DAC")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayNormalProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, bool isBehind = false)
		{
			return null;
		}

		// Token: 0x06005A28 RID: 23080 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005367")]
		[Address(RVA = "0x101656D98", Offset = "0x1656D98", VA = "0x101656D98")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayLoopProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, bool isBehind = false)
		{
			return null;
		}

		// Token: 0x06005A29 RID: 23081 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005368")]
		[Address(RVA = "0x101656DC0", Offset = "0x1656DC0", VA = "0x101656DC0")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever, bool isBehind = false)
		{
			return null;
		}

		// Token: 0x06005A2A RID: 23082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005369")]
		[Address(RVA = "0x101656DD4", Offset = "0x1656DD4", VA = "0x101656DD4")]
		public void PlayAndReserveNextEffect(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode, ADVEffectManager.ePlayType nextPlayType, string nextEffectID, WrapMode nextPlayMode, bool isBehind = false)
		{
		}

		// Token: 0x06005A2B RID: 23083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600536A")]
		[Address(RVA = "0x101656FE0", Offset = "0x1656FE0", VA = "0x101656FE0")]
		public void PlayAndReserveNextEffect(ADVEffectManager.PlayProcessArgument argument, ADVEffectManager.PlayProcessArgument nextEffectArgument)
		{
		}

		// Token: 0x06005A2C RID: 23084 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600536B")]
		[Address(RVA = "0x1016561F0", Offset = "0x16561F0", VA = "0x1016561F0")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever, bool isBehind = false)
		{
			return null;
		}

		// Token: 0x06005A2D RID: 23085 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600536C")]
		[Address(RVA = "0x10165706C", Offset = "0x165706C", VA = "0x10165706C")]
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.PlayProcessArgument argument)
		{
			return null;
		}

		// Token: 0x06005A2E RID: 23086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600536D")]
		[Address(RVA = "0x101657608", Offset = "0x1657608", VA = "0x101657608")]
		public void Stop(string effectID)
		{
		}

		// Token: 0x06005A2F RID: 23087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600536E")]
		[Address(RVA = "0x101657964", Offset = "0x1657964", VA = "0x101657964")]
		public void StopEmo(string emotionID)
		{
		}

		// Token: 0x06005A30 RID: 23088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600536F")]
		[Address(RVA = "0x10165798C", Offset = "0x165798C", VA = "0x10165798C")]
		public void LoopEnd(string effectID)
		{
		}

		// Token: 0x06005A31 RID: 23089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005370")]
		[Address(RVA = "0x1016567A4", Offset = "0x16567A4", VA = "0x1016567A4")]
		public void StopAllEffect()
		{
		}

		// Token: 0x06005A32 RID: 23090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005371")]
		[Address(RVA = "0x101657724", Offset = "0x1657724", VA = "0x101657724")]
		public void ReleaseRemovedEffect(int effectUniqueId)
		{
		}

		// Token: 0x06005A33 RID: 23091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005372")]
		[Address(RVA = "0x101657BB8", Offset = "0x1657BB8", VA = "0x101657BB8")]
		public void ReleaseRemovedEffectAll()
		{
		}

		// Token: 0x06005A34 RID: 23092 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005373")]
		[Address(RVA = "0x101657CAC", Offset = "0x1657CAC", VA = "0x101657CAC")]
		private List<ADVEffectManager.ADVEffectHandlerWrapper> FilterEffects(ADVEffectManager.eEffectPlayState playState, string effectID)
		{
			return null;
		}

		// Token: 0x06005A35 RID: 23093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005374")]
		[Address(RVA = "0x101657E40", Offset = "0x1657E40", VA = "0x101657E40")]
		public void AddWaitPrecedingEffectInformation(int precedingUniqueId, ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever, bool isBehind = false)
		{
		}

		// Token: 0x06005A36 RID: 23094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005375")]
		[Address(RVA = "0x101657490", Offset = "0x1657490", VA = "0x101657490")]
		protected void AddWaitPrecedingEffectInformation(ADVEffectManager.WaitPrecedingEffectInformation waitPrecedingEffectInfo)
		{
		}

		// Token: 0x06005A37 RID: 23095 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005376")]
		[Address(RVA = "0x101657508", Offset = "0x1657508", VA = "0x101657508")]
		public ADVEffectManager.ADVEffectHandlerWrapper GetEffect(int effectUniqueId)
		{
			return null;
		}

		// Token: 0x06005A38 RID: 23096 RVA: 0x0001D700 File Offset: 0x0001B900
		[Token(Token = "0x6005377")]
		[Address(RVA = "0x1016560AC", Offset = "0x16560AC", VA = "0x1016560AC")]
		public ADVEffectManager.eEffectPlayState GetEffectPlayState(int effectUniqueId)
		{
			return ADVEffectManager.eEffectPlayState.Error;
		}

		// Token: 0x06005A39 RID: 23097 RVA: 0x0001D718 File Offset: 0x0001B918
		[Token(Token = "0x6005378")]
		[Address(RVA = "0x101658000", Offset = "0x1658000", VA = "0x101658000")]
		public bool IsDonePlaying(int effectUniqueId)
		{
			return default(bool);
		}

		// Token: 0x06005A3A RID: 23098 RVA: 0x0001D730 File Offset: 0x0001B930
		[Token(Token = "0x6005379")]
		[Address(RVA = "0x10165801C", Offset = "0x165801C", VA = "0x10165801C")]
		public bool IsDonePlayingAllFromEffectIdIgnoreLoop(string effectID)
		{
			return default(bool);
		}

		// Token: 0x06005A3B RID: 23099 RVA: 0x0001D748 File Offset: 0x0001B948
		[Token(Token = "0x600537A")]
		[Address(RVA = "0x1016580D8", Offset = "0x16580D8", VA = "0x1016580D8")]
		public bool IsDonePlayingAllFromEmotionIdIgnoreLoop(string emotionID)
		{
			return default(bool);
		}

		// Token: 0x06005A3C RID: 23100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600537B")]
		[Address(RVA = "0x101658100", Offset = "0x1658100", VA = "0x101658100")]
		public void PresetEffectLoop(string st, string lp, string ed, bool ignoreParticle, bool isBehind = false)
		{
		}

		// Token: 0x06005A3D RID: 23101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600537C")]
		[Address(RVA = "0x10165820C", Offset = "0x165820C", VA = "0x10165820C")]
		public void PlayPreset(Vector3 position, Quaternion rotation, bool isBehind = false)
		{
		}

		// Token: 0x06005A3E RID: 23102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600537D")]
		[Address(RVA = "0x101658348", Offset = "0x1658348", VA = "0x101658348")]
		public void PlayPresetScreen(Vector3 position, Quaternion rotation, bool isBehind = false)
		{
		}

		// Token: 0x06005A3F RID: 23103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600537E")]
		[Address(RVA = "0x101658484", Offset = "0x1658484", VA = "0x101658484")]
		public ADVEffectManager()
		{
		}

		// Token: 0x04006CDC RID: 27868
		[Token(Token = "0x4004D43")]
		private const float SCREENEFFECT_SIZE = 1334f;

		// Token: 0x04006CDD RID: 27869
		[Token(Token = "0x4004D44")]
		private const float CAMERA_ORTHOGRAPHIC_SIZE = 0.5f;

		// Token: 0x04006CDE RID: 27870
		[Token(Token = "0x4004D45")]
		public const int DEFAULT_UNIQUE_EFFECT_ID = -1;

		// Token: 0x04006CDF RID: 27871
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D46")]
		private ADVPlayer m_Player;

		// Token: 0x04006CE0 RID: 27872
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D47")]
		private List<string> m_NeedList;

		// Token: 0x04006CE1 RID: 27873
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D48")]
		private List<string> m_LoadedList;

		// Token: 0x04006CE2 RID: 27874
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004D49")]
		private List<ADVEffectManager.ADVEffectHandlerWrapper> m_EffectHandlerList;

		// Token: 0x04006CE3 RID: 27875
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004D4A")]
		private List<ADVEffectManager.WaitPrecedingEffectInformation> m_WaitInfoList;

		// Token: 0x04006CE4 RID: 27876
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004D4B")]
		private bool m_IsPlaySE;

		// Token: 0x04006CE5 RID: 27877
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004D4C")]
		private ADVEffectManager.EffectPreset m_Preset;

		// Token: 0x020011A7 RID: 4519
		[Token(Token = "0x20012AB")]
		public enum ePlayType
		{
			// Token: 0x04006CE7 RID: 27879
			[Token(Token = "0x40072C8")]
			Error,
			// Token: 0x04006CE8 RID: 27880
			[Token(Token = "0x40072C9")]
			Normal,
			// Token: 0x04006CE9 RID: 27881
			[Token(Token = "0x40072CA")]
			Screen,
			// Token: 0x04006CEA RID: 27882
			[Token(Token = "0x40072CB")]
			Emo
		}

		// Token: 0x020011A8 RID: 4520
		[Token(Token = "0x20012AC")]
		public enum eEffectPlayState
		{
			// Token: 0x04006CEC RID: 27884
			[Token(Token = "0x40072CD")]
			Error,
			// Token: 0x04006CED RID: 27885
			[Token(Token = "0x40072CE")]
			None,
			// Token: 0x04006CEE RID: 27886
			[Token(Token = "0x40072CF")]
			Play,
			// Token: 0x04006CEF RID: 27887
			[Token(Token = "0x40072D0")]
			PlayLoop,
			// Token: 0x04006CF0 RID: 27888
			[Token(Token = "0x40072D1")]
			Done
		}

		// Token: 0x020011A9 RID: 4521
		[Token(Token = "0x20012AD")]
		public class ADVEffectHandlerWrapper
		{
			// Token: 0x06005A40 RID: 23104 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006371")]
			[Address(RVA = "0x10165788C", Offset = "0x165788C", VA = "0x10165788C")]
			public ADVEffectHandlerWrapper(ADVEffectManager.ePlayType playType, int uniqueId, EffectHandler effectHandler)
			{
			}

			// Token: 0x06005A41 RID: 23105 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006372")]
			[Address(RVA = "0x101656064", Offset = "0x1656064", VA = "0x101656064")]
			public void Remove()
			{
			}

			// Token: 0x06005A42 RID: 23106 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006373")]
			[Address(RVA = "0x101658494", Offset = "0x1658494", VA = "0x101658494")]
			public void DestroyEffectHandler()
			{
			}

			// Token: 0x06005A43 RID: 23107 RVA: 0x0001D760 File Offset: 0x0001B960
			[Token(Token = "0x6006374")]
			[Address(RVA = "0x101656314", Offset = "0x1656314", VA = "0x101656314")]
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return ADVEffectManager.ePlayType.Error;
			}

			// Token: 0x06005A44 RID: 23108 RVA: 0x0001D778 File Offset: 0x0001B978
			[Token(Token = "0x6006375")]
			[Address(RVA = "0x101657BB0", Offset = "0x1657BB0", VA = "0x101657BB0")]
			public int GetUniqueId()
			{
				return 0;
			}

			// Token: 0x06005A45 RID: 23109 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006376")]
			[Address(RVA = "0x1016562E4", Offset = "0x16562E4", VA = "0x1016562E4")]
			public string GetEffectId()
			{
				return null;
			}

			// Token: 0x06005A46 RID: 23110 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006377")]
			[Address(RVA = "0x1016562DC", Offset = "0x16562DC", VA = "0x1016562DC")]
			public EffectHandler GetEffectHandler()
			{
				return null;
			}

			// Token: 0x06005A47 RID: 23111 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006378")]
			[Address(RVA = "0x101657904", Offset = "0x1657904", VA = "0x101657904")]
			public Transform GetEffectTransform()
			{
				return null;
			}

			// Token: 0x06005A48 RID: 23112 RVA: 0x0001D790 File Offset: 0x0001B990
			[Token(Token = "0x6006379")]
			[Address(RVA = "0x101656034", Offset = "0x1656034", VA = "0x101656034")]
			public bool IsEffectPlaying()
			{
				return default(bool);
			}

			// Token: 0x06005A49 RID: 23113 RVA: 0x0001D7A8 File Offset: 0x0001B9A8
			[Token(Token = "0x600637A")]
			[Address(RVA = "0x10165602C", Offset = "0x165602C", VA = "0x10165602C")]
			public ADVEffectManager.eEffectPlayState GetPlayState()
			{
				return ADVEffectManager.eEffectPlayState.Error;
			}

			// Token: 0x06005A4A RID: 23114 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600637B")]
			[Address(RVA = "0x101657AC8", Offset = "0x1657AC8", VA = "0x101657AC8")]
			public void LoopEnd()
			{
			}

			// Token: 0x04006CF1 RID: 27889
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072D2")]
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x04006CF2 RID: 27890
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40072D3")]
			private int m_UniqueId;

			// Token: 0x04006CF3 RID: 27891
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072D4")]
			private ADVEffectManager.eEffectPlayState m_PlayState;

			// Token: 0x04006CF4 RID: 27892
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072D5")]
			private EffectHandler m_EffectHandler;
		}

		// Token: 0x020011AA RID: 4522
		[Token(Token = "0x20012AE")]
		public class WaitPrecedingEffectInformation
		{
			// Token: 0x06005A4B RID: 23115 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600637C")]
			[Address(RVA = "0x101657F38", Offset = "0x1657F38", VA = "0x101657F38")]
			public WaitPrecedingEffectInformation(ADVEffectManager.ADVEffectHandlerWrapper precedingInstance, int precedingUniqueId, ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever, bool isBehind = false)
			{
			}

			// Token: 0x06005A4C RID: 23116 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600637D")]
			[Address(RVA = "0x101657398", Offset = "0x1657398", VA = "0x101657398")]
			public WaitPrecedingEffectInformation(ADVEffectManager.ADVEffectHandlerWrapper precedingInstance, int precedingUniqueId, ADVEffectManager.PlayProcessArgument argument)
			{
			}

			// Token: 0x06005A4D RID: 23117 RVA: 0x0001D7C0 File Offset: 0x0001B9C0
			[Token(Token = "0x600637E")]
			[Address(RVA = "0x10165608C", Offset = "0x165608C", VA = "0x10165608C")]
			public ADVEffectManager.eEffectPlayState GetPrecedingEffectState()
			{
				return ADVEffectManager.eEffectPlayState.Error;
			}

			// Token: 0x06005A4E RID: 23118 RVA: 0x0001D7D8 File Offset: 0x0001B9D8
			[Token(Token = "0x600637F")]
			[Address(RVA = "0x1016560A4", Offset = "0x16560A4", VA = "0x1016560A4")]
			public int GetPrecedingUniqueId()
			{
				return 0;
			}

			// Token: 0x06005A4F RID: 23119 RVA: 0x0001D7F0 File Offset: 0x0001B9F0
			[Token(Token = "0x6006380")]
			[Address(RVA = "0x1016561B0", Offset = "0x16561B0", VA = "0x1016561B0")]
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return ADVEffectManager.ePlayType.Error;
			}

			// Token: 0x06005A50 RID: 23120 RVA: 0x0001D808 File Offset: 0x0001BA08
			[Token(Token = "0x6006381")]
			[Address(RVA = "0x1016561B8", Offset = "0x16561B8", VA = "0x1016561B8")]
			public int GetEffectUniqueId()
			{
				return 0;
			}

			// Token: 0x06005A51 RID: 23121 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006382")]
			[Address(RVA = "0x1016561C0", Offset = "0x16561C0", VA = "0x1016561C0")]
			public string GetEffectID()
			{
				return null;
			}

			// Token: 0x06005A52 RID: 23122 RVA: 0x0001D820 File Offset: 0x0001BA20
			[Token(Token = "0x6006383")]
			[Address(RVA = "0x1016561C8", Offset = "0x16561C8", VA = "0x1016561C8")]
			public Vector3 GetPosition()
			{
				return default(Vector3);
			}

			// Token: 0x06005A53 RID: 23123 RVA: 0x0001D838 File Offset: 0x0001BA38
			[Token(Token = "0x6006384")]
			[Address(RVA = "0x1016561D4", Offset = "0x16561D4", VA = "0x1016561D4")]
			public Quaternion GetRotation()
			{
				return default(Quaternion);
			}

			// Token: 0x06005A54 RID: 23124 RVA: 0x0001D850 File Offset: 0x0001BA50
			[Token(Token = "0x6006385")]
			[Address(RVA = "0x1016561E0", Offset = "0x16561E0", VA = "0x1016561E0")]
			public WrapMode GetPlayMode()
			{
				return WrapMode.Default;
			}

			// Token: 0x06005A55 RID: 23125 RVA: 0x0001D868 File Offset: 0x0001BA68
			[Token(Token = "0x6006386")]
			[Address(RVA = "0x1016561E8", Offset = "0x16561E8", VA = "0x1016561E8")]
			public bool IsBehind()
			{
				return default(bool);
			}

			// Token: 0x04006CF5 RID: 27893
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072D6")]
			private ADVEffectManager.ADVEffectHandlerWrapper m_PrecedingInstance;

			// Token: 0x04006CF6 RID: 27894
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072D7")]
			private int m_PrecedingUniqueId;

			// Token: 0x04006CF7 RID: 27895
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40072D8")]
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x04006CF8 RID: 27896
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072D9")]
			private int m_EffectUniqueId;

			// Token: 0x04006CF9 RID: 27897
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40072DA")]
			private string m_EffectID;

			// Token: 0x04006CFA RID: 27898
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40072DB")]
			private Vector3 m_Position;

			// Token: 0x04006CFB RID: 27899
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x40072DC")]
			private Quaternion m_Rotation;

			// Token: 0x04006CFC RID: 27900
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x40072DD")]
			private WrapMode m_PlayMode;

			// Token: 0x04006CFD RID: 27901
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x40072DE")]
			private bool m_isBehind;
		}

		// Token: 0x020011AB RID: 4523
		[Token(Token = "0x20012AF")]
		public class PlayProcessArgument
		{
			// Token: 0x06005A56 RID: 23126 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006387")]
			[Address(RVA = "0x101656F30", Offset = "0x1656F30", VA = "0x101656F30")]
			public PlayProcessArgument(ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever, bool isBehind = false)
			{
			}

			// Token: 0x06005A57 RID: 23127 RVA: 0x0001D880 File Offset: 0x0001BA80
			[Token(Token = "0x6006388")]
			[Address(RVA = "0x101657868", Offset = "0x1657868", VA = "0x101657868")]
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return ADVEffectManager.ePlayType.Error;
			}

			// Token: 0x06005A58 RID: 23128 RVA: 0x0001D898 File Offset: 0x0001BA98
			[Token(Token = "0x6006389")]
			[Address(RVA = "0x101657500", Offset = "0x1657500", VA = "0x101657500")]
			public int GetEffectUniqueId()
			{
				return 0;
			}

			// Token: 0x06005A59 RID: 23129 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600638A")]
			[Address(RVA = "0x101657870", Offset = "0x1657870", VA = "0x101657870")]
			public string GetEffectID()
			{
				return null;
			}

			// Token: 0x06005A5A RID: 23130 RVA: 0x0001D8B0 File Offset: 0x0001BAB0
			[Token(Token = "0x600638B")]
			[Address(RVA = "0x10165785C", Offset = "0x165785C", VA = "0x10165785C")]
			public Vector3 GetPosition()
			{
				return default(Vector3);
			}

			// Token: 0x06005A5B RID: 23131 RVA: 0x0001D8C8 File Offset: 0x0001BAC8
			[Token(Token = "0x600638C")]
			[Address(RVA = "0x101657878", Offset = "0x1657878", VA = "0x101657878")]
			public Quaternion GetRotation()
			{
				return default(Quaternion);
			}

			// Token: 0x06005A5C RID: 23132 RVA: 0x0001D8E0 File Offset: 0x0001BAE0
			[Token(Token = "0x600638D")]
			[Address(RVA = "0x101657884", Offset = "0x1657884", VA = "0x101657884")]
			public WrapMode GetPlayMode()
			{
				return WrapMode.Default;
			}

			// Token: 0x06005A5D RID: 23133 RVA: 0x0001D8F8 File Offset: 0x0001BAF8
			[Token(Token = "0x600638E")]
			[Address(RVA = "0x1016578FC", Offset = "0x16578FC", VA = "0x1016578FC")]
			public bool IsBehind()
			{
				return default(bool);
			}

			// Token: 0x04006CFE RID: 27902
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072DF")]
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x04006CFF RID: 27903
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40072E0")]
			private int m_EffectUniqueId;

			// Token: 0x04006D00 RID: 27904
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072E1")]
			private string m_EffectID;

			// Token: 0x04006D01 RID: 27905
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072E2")]
			private Vector3 m_Position;

			// Token: 0x04006D02 RID: 27906
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40072E3")]
			private Quaternion m_Rotation;

			// Token: 0x04006D03 RID: 27907
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x40072E4")]
			private WrapMode m_PlayMode;

			// Token: 0x04006D04 RID: 27908
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40072E5")]
			private bool m_isBehind;
		}

		// Token: 0x020011AC RID: 4524
		[Token(Token = "0x20012B0")]
		public class EffectPreset
		{
			// Token: 0x06005A5E RID: 23134 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600638F")]
			[Address(RVA = "0x101658204", Offset = "0x1658204", VA = "0x101658204")]
			public EffectPreset()
			{
			}

			// Token: 0x04006D05 RID: 27909
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40072E6")]
			public string st;

			// Token: 0x04006D06 RID: 27910
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40072E7")]
			public string lp;

			// Token: 0x04006D07 RID: 27911
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40072E8")]
			public string ed;

			// Token: 0x04006D08 RID: 27912
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40072E9")]
			public bool m_ignoreParticle;

			// Token: 0x04006D09 RID: 27913
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40072EA")]
			public Vector3 pos;

			// Token: 0x04006D0A RID: 27914
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40072EB")]
			public Quaternion rot;

			// Token: 0x04006D0B RID: 27915
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40072EC")]
			public bool isLoop;

			// Token: 0x04006D0C RID: 27916
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x40072ED")]
			public int m_Step;

			// Token: 0x04006D0D RID: 27917
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x40072EE")]
			public bool isBehind;
		}
	}
}
