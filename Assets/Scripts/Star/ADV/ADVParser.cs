﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x020011AD RID: 4525
	[Token(Token = "0x2000BBC")]
	[StructLayout(3)]
	public class ADVParser
	{
		// Token: 0x06005A5F RID: 23135 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600537F")]
		[Address(RVA = "0x101658518", Offset = "0x1658518", VA = "0x101658518")]
		public string GetTagCommand(ADVParser.EventType eventType)
		{
			return null;
		}

		// Token: 0x06005A60 RID: 23136 RVA: 0x0001D910 File Offset: 0x0001BB10
		[Token(Token = "0x6005380")]
		[Address(RVA = "0x101658568", Offset = "0x1658568", VA = "0x101658568")]
		public int GetCommandsCount()
		{
			return 0;
		}

		// Token: 0x06005A61 RID: 23137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005381")]
		[Address(RVA = "0x101658594", Offset = "0x1658594", VA = "0x101658594")]
		public void Setup(ADVPlayer player, ADVTalkCustomWindow talkCustomWindow)
		{
		}

		// Token: 0x06005A62 RID: 23138 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005382")]
		[Address(RVA = "0x1016585A0", Offset = "0x16585A0", VA = "0x1016585A0")]
		public string ParseAll(string text, bool execTag, bool execBreak, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return null;
		}

		// Token: 0x06005A63 RID: 23139 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005383")]
		[Address(RVA = "0x1016586B0", Offset = "0x16586B0", VA = "0x1016586B0")]
		public string Parse(ref string text, ref int idx, ref List<ADVParser.EventType> evList, bool execTag, bool execBreak, out bool breakFlag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return null;
		}

		// Token: 0x06005A64 RID: 23140 RVA: 0x0001D928 File Offset: 0x0001BB28
		[Token(Token = "0x6005384")]
		[Address(RVA = "0x101658B54", Offset = "0x1658B54", VA = "0x101658B54")]
		private bool AnalyzeTag(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005A65 RID: 23141 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005385")]
		[Address(RVA = "0x101658D28", Offset = "0x1658D28", VA = "0x101658D28")]
		private ADVTextTag ConvertTagStringToADVTextTag(string tagString)
		{
			return null;
		}

		// Token: 0x06005A66 RID: 23142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005386")]
		[Address(RVA = "0x101659398", Offset = "0x1659398", VA = "0x101659398")]
		private void DeleteTag(ref string text, ref int idx, int charNum)
		{
		}

		// Token: 0x06005A67 RID: 23143 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005387")]
		[Address(RVA = "0x101659444", Offset = "0x1659444", VA = "0x101659444")]
		public ADVPlayer GetADVPlayer()
		{
			return null;
		}

		// Token: 0x06005A68 RID: 23144 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005388")]
		[Address(RVA = "0x10165944C", Offset = "0x165944C", VA = "0x10165944C")]
		public ADVTalkCustomWindow GetTalkWindow()
		{
			return null;
		}

		// Token: 0x06005A69 RID: 23145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005389")]
		[Address(RVA = "0x101659454", Offset = "0x1659454", VA = "0x101659454")]
		public void SetTapWaitWithTapFlag(bool flag)
		{
		}

		// Token: 0x06005A6A RID: 23146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600538A")]
		[Address(RVA = "0x10165945C", Offset = "0x165945C", VA = "0x10165945C")]
		public ADVParser()
		{
		}

		// Token: 0x04006D0E RID: 27918
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004D4D")]
		private ADVPlayer m_Player;

		// Token: 0x04006D0F RID: 27919
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D4E")]
		private ADVTalkCustomWindow m_TalkCustomWindow;

		// Token: 0x04006D10 RID: 27920
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D4F")]
		private bool m_IsTapWaitWithTap;

		// Token: 0x04006D11 RID: 27921
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D50")]
		private string[] m_TagCommand;

		// Token: 0x020011AE RID: 4526
		[Token(Token = "0x20012B1")]
		public enum EventType
		{
			// Token: 0x04006D13 RID: 27923
			[Token(Token = "0x40072F0")]
			event_none,
			// Token: 0x04006D14 RID: 27924
			[Token(Token = "0x40072F1")]
			event_se,
			// Token: 0x04006D15 RID: 27925
			[Token(Token = "0x40072F2")]
			event_voice,
			// Token: 0x04006D16 RID: 27926
			[Token(Token = "0x40072F3")]
			event_fullvoice,
			// Token: 0x04006D17 RID: 27927
			[Token(Token = "0x40072F4")]
			event_motion,
			// Token: 0x04006D18 RID: 27928
			[Token(Token = "0x40072F5")]
			event_wait_motion,
			// Token: 0x04006D19 RID: 27929
			[Token(Token = "0x40072F6")]
			event_move,
			// Token: 0x04006D1A RID: 27930
			[Token(Token = "0x40072F7")]
			event_wait_move,
			// Token: 0x04006D1B RID: 27931
			[Token(Token = "0x40072F8")]
			event_emo,
			// Token: 0x04006D1C RID: 27932
			[Token(Token = "0x40072F9")]
			event_face,
			// Token: 0x04006D1D RID: 27933
			[Token(Token = "0x40072FA")]
			event_pose,
			// Token: 0x04006D1E RID: 27934
			[Token(Token = "0x40072FB")]
			event_shake,
			// Token: 0x04006D1F RID: 27935
			[Token(Token = "0x40072FC")]
			event_color,
			// Token: 0x04006D20 RID: 27936
			[Token(Token = "0x40072FD")]
			event_size,
			// Token: 0x04006D21 RID: 27937
			[Token(Token = "0x40072FE")]
			event_strong,
			// Token: 0x04006D22 RID: 27938
			[Token(Token = "0x40072FF")]
			event_ruby,
			// Token: 0x04006D23 RID: 27939
			[Token(Token = "0x4007300")]
			event_wait,
			// Token: 0x04006D24 RID: 27940
			[Token(Token = "0x4007301")]
			event_bold,
			// Token: 0x04006D25 RID: 27941
			[Token(Token = "0x4007302")]
			event_italic,
			// Token: 0x04006D26 RID: 27942
			[Token(Token = "0x4007303")]
			event_page,
			// Token: 0x04006D27 RID: 27943
			[Token(Token = "0x4007304")]
			event_shift,
			// Token: 0x04006D28 RID: 27944
			[Token(Token = "0x4007305")]
			event_Max
		}

		// Token: 0x020011AF RID: 4527
		[Token(Token = "0x20012B2")]
		public class TagInformation
		{
			// Token: 0x06005A6B RID: 23147 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006390")]
			[Address(RVA = "0x101659BD8", Offset = "0x1659BD8", VA = "0x101659BD8")]
			public TagInformation()
			{
			}
		}

		// Token: 0x020011B0 RID: 4528
		[Token(Token = "0x20012B3")]
		public class RubyData
		{
			// Token: 0x06005A6C RID: 23148 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006391")]
			[Address(RVA = "0x101659BB4", Offset = "0x1659BB4", VA = "0x101659BB4")]
			public void Set(string text, Vector2 start, Vector2 end, Text obj)
			{
			}

			// Token: 0x06005A6D RID: 23149 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006392")]
			[Address(RVA = "0x101659BD0", Offset = "0x1659BD0", VA = "0x101659BD0")]
			public RubyData()
			{
			}

			// Token: 0x04006D29 RID: 27945
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007306")]
			public string m_Text;

			// Token: 0x04006D2A RID: 27946
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007307")]
			public Vector2 m_StartPos;

			// Token: 0x04006D2B RID: 27947
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007308")]
			public Vector2 m_EndPos;

			// Token: 0x04006D2C RID: 27948
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007309")]
			public Text m_TextObj;
		}
	}
}
