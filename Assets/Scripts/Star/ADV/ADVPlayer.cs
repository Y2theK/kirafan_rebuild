﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x020011B1 RID: 4529
	[Token(Token = "0x2000BBD")]
	[StructLayout(3)]
	public class ADVPlayer : MonoBehaviour
	{
		// Token: 0x1700060C RID: 1548
		// (get) Token: 0x06005A6E RID: 23150 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005B0")]
		public PixelCrashWrapper pixelCrashWrapper
		{
			[Token(Token = "0x600538B")]
			[Address(RVA = "0x101659BE0", Offset = "0x1659BE0", VA = "0x101659BE0")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x06005A70 RID: 23152 RVA: 0x0001D940 File Offset: 0x0001BB40
		// (set) Token: 0x06005A6F RID: 23151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005B1")]
		private bool isWaitPixelCrashPrepare
		{
			[Token(Token = "0x600538D")]
			[Address(RVA = "0x101659BF0", Offset = "0x1659BF0", VA = "0x101659BF0")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600538C")]
			[Address(RVA = "0x101659BE8", Offset = "0x1659BE8", VA = "0x101659BE8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x06005A72 RID: 23154 RVA: 0x0001D958 File Offset: 0x0001BB58
		// (set) Token: 0x06005A71 RID: 23153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005B2")]
		private bool isWaitPixelCrash
		{
			[Token(Token = "0x600538F")]
			[Address(RVA = "0x101659C00", Offset = "0x1659C00", VA = "0x101659C00")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600538E")]
			[Address(RVA = "0x101659BF8", Offset = "0x1659BF8", VA = "0x101659BF8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x06005A74 RID: 23156 RVA: 0x0001D970 File Offset: 0x0001BB70
		// (set) Token: 0x06005A73 RID: 23155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005B3")]
		private bool isPlayingPixelCrash
		{
			[Token(Token = "0x6005391")]
			[Address(RVA = "0x101659C10", Offset = "0x1659C10", VA = "0x101659C10")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005390")]
			[Address(RVA = "0x101659C08", Offset = "0x1659C08", VA = "0x101659C08")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x06005A75 RID: 23157 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005B4")]
		public MultiCameraPostProcessFilter postProcessRenderer
		{
			[Token(Token = "0x6005392")]
			[Address(RVA = "0x101659C18", Offset = "0x1659C18", VA = "0x101659C18")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005A76 RID: 23158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005393")]
		[Address(RVA = "0x101659C20", Offset = "0x1659C20", VA = "0x101659C20")]
		public void WaitPixelCrashPrepare()
		{
		}

		// Token: 0x06005A77 RID: 23159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005394")]
		[Address(RVA = "0x101659CAC", Offset = "0x1659CAC", VA = "0x101659CAC")]
		public void WaitPixelCrash()
		{
		}

		// Token: 0x06005A78 RID: 23160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005395")]
		[Address(RVA = "0x101659D30", Offset = "0x1659D30", VA = "0x101659D30")]
		public void StartPixelCrashPrepare(float sec)
		{
		}

		// Token: 0x06005A79 RID: 23161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005396")]
		[Address(RVA = "0x101659E08", Offset = "0x1659E08", VA = "0x101659E08")]
		public void StartPixelCrash(float sec)
		{
		}

		// Token: 0x06005A7A RID: 23162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005397")]
		[Address(RVA = "0x101659F24", Offset = "0x1659F24", VA = "0x101659F24")]
		public void AddPicForPixelCrash(ADVStandPic pic)
		{
		}

		// Token: 0x06005A7B RID: 23163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005398")]
		[Address(RVA = "0x10165A178", Offset = "0x165A178", VA = "0x10165A178")]
		public void ClearPicForPixelCrash()
		{
		}

		// Token: 0x06005A7C RID: 23164 RVA: 0x0001D988 File Offset: 0x0001BB88
		[Token(Token = "0x6005399")]
		[Address(RVA = "0x10165A3AC", Offset = "0x165A3AC", VA = "0x10165A3AC")]
		public bool UpdatePixelCrashAndCheckProgress()
		{
			return default(bool);
		}

		// Token: 0x06005A7D RID: 23165 RVA: 0x0001D9A0 File Offset: 0x0001BBA0
		[Token(Token = "0x600539A")]
		[Address(RVA = "0x10165A81C", Offset = "0x165A81C", VA = "0x10165A81C")]
		private float Update_PP_Rate(float now, float tgt, ref float nowTime, ref float maxTime)
		{
			return 0f;
		}

		// Token: 0x06005A7E RID: 23166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600539B")]
		[Address(RVA = "0x10165A8EC", Offset = "0x165A8EC", VA = "0x10165A8EC")]
		public void Start_PP_Contrast(float sec, float level)
		{
		}

		// Token: 0x06005A7F RID: 23167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600539C")]
		[Address(RVA = "0x10165A9A0", Offset = "0x165A9A0", VA = "0x10165A9A0")]
		public void Start_PP_Brightness(float sec, float level)
		{
		}

		// Token: 0x06005A80 RID: 23168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600539D")]
		[Address(RVA = "0x10165AA54", Offset = "0x165AA54", VA = "0x10165AA54")]
		public void Start_PP_Chroma(float sec, float level)
		{
		}

		// Token: 0x06005A81 RID: 23169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600539E")]
		[Address(RVA = "0x10165AB08", Offset = "0x165AB08", VA = "0x10165AB08")]
		public void Start_PP_ColorBlend(float sec, float level)
		{
		}

		// Token: 0x06005A82 RID: 23170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600539F")]
		[Address(RVA = "0x10165ABBC", Offset = "0x165ABBC", VA = "0x10165ABBC")]
		public void Start_PP_BloomThreshold(float sec, float level)
		{
		}

		// Token: 0x06005A83 RID: 23171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A0")]
		[Address(RVA = "0x10165AC70", Offset = "0x165AC70", VA = "0x10165AC70")]
		private void Update_PP_Contrast()
		{
		}

		// Token: 0x06005A84 RID: 23172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A1")]
		[Address(RVA = "0x10165AD40", Offset = "0x165AD40", VA = "0x10165AD40")]
		private void Update_PP_Brightness()
		{
		}

		// Token: 0x06005A85 RID: 23173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A2")]
		[Address(RVA = "0x10165AE10", Offset = "0x165AE10", VA = "0x10165AE10")]
		private void Update_PP_Chroma()
		{
		}

		// Token: 0x06005A86 RID: 23174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A3")]
		[Address(RVA = "0x10165AEE0", Offset = "0x165AEE0", VA = "0x10165AEE0")]
		private void Update_PP_ColorBlend()
		{
		}

		// Token: 0x06005A87 RID: 23175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A4")]
		[Address(RVA = "0x10165AFB0", Offset = "0x165AFB0", VA = "0x10165AFB0")]
		private void Update_PP_BloomThreshold()
		{
		}

		// Token: 0x06005A88 RID: 23176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A5")]
		[Address(RVA = "0x10165B080", Offset = "0x165B080", VA = "0x10165B080")]
		public void Update_PP_Progress()
		{
		}

		// Token: 0x06005A89 RID: 23177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A6")]
		[Address(RVA = "0x10165B0BC", Offset = "0x165B0BC", VA = "0x10165B0BC")]
		public void AddPicForDistorsion(ADVStandPic pic)
		{
		}

		// Token: 0x06005A8A RID: 23178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A7")]
		[Address(RVA = "0x10165B208", Offset = "0x165B208", VA = "0x10165B208")]
		public void RemovePicForDistorsion(ADVStandPic pic)
		{
		}

		// Token: 0x06005A8B RID: 23179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A8")]
		[Address(RVA = "0x10165B464", Offset = "0x165B464", VA = "0x10165B464")]
		public void ClearPicForDistorsion()
		{
		}

		// Token: 0x06005A8C RID: 23180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053A9")]
		[Address(RVA = "0x10165B648", Offset = "0x165B648", VA = "0x10165B648")]
		public void EnableCharaDistorsion(bool flg)
		{
		}

		// Token: 0x06005A8D RID: 23181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053AA")]
		[Address(RVA = "0x10165B6AC", Offset = "0x165B6AC", VA = "0x10165B6AC")]
		public void SetCharaDistorsion(float sec, float level)
		{
		}

		// Token: 0x06005A8E RID: 23182 RVA: 0x0001D9B8 File Offset: 0x0001BBB8
		[Token(Token = "0x60053AB")]
		[Address(RVA = "0x10165B6F8", Offset = "0x165B6F8", VA = "0x10165B6F8")]
		public bool Update_CharaDistorsion()
		{
			return default(bool);
		}

		// Token: 0x06005A8F RID: 23183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053AC")]
		[Address(RVA = "0x10165B758", Offset = "0x165B758", VA = "0x10165B758")]
		public void EnableBGDistorsion(bool flg)
		{
		}

		// Token: 0x06005A90 RID: 23184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053AD")]
		[Address(RVA = "0x10165B790", Offset = "0x165B790", VA = "0x10165B790")]
		public void StartBGDistorsion(float sec, float level)
		{
		}

		// Token: 0x06005A91 RID: 23185 RVA: 0x0001D9D0 File Offset: 0x0001BBD0
		[Token(Token = "0x60053AE")]
		[Address(RVA = "0x10165B7DC", Offset = "0x165B7DC", VA = "0x10165B7DC")]
		public bool Update_BGDistorsion()
		{
			return default(bool);
		}

		// Token: 0x06005A92 RID: 23186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053AF")]
		[Address(RVA = "0x10165B83C", Offset = "0x165B83C", VA = "0x10165B83C")]
		public void EnableShadeOff(bool flg)
		{
		}

		// Token: 0x06005A93 RID: 23187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053B0")]
		[Address(RVA = "0x10165B8EC", Offset = "0x165B8EC", VA = "0x10165B8EC")]
		public void Start_ShadeOff_Level(float sec, float level)
		{
		}

		// Token: 0x06005A94 RID: 23188 RVA: 0x0001D9E8 File Offset: 0x0001BBE8
		[Token(Token = "0x60053B1")]
		[Address(RVA = "0x10165B998", Offset = "0x165B998", VA = "0x10165B998")]
		public bool Update_ShadeOff()
		{
			return default(bool);
		}

		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x06005A95 RID: 23189 RVA: 0x0001DA00 File Offset: 0x0001BC00
		[Token(Token = "0x170005B5")]
		public bool IsDonePrepare
		{
			[Token(Token = "0x60053B2")]
			[Address(RVA = "0x10165BA5C", Offset = "0x165BA5C", VA = "0x10165BA5C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x06005A96 RID: 23190 RVA: 0x0001DA18 File Offset: 0x0001BC18
		[Token(Token = "0x170005B6")]
		public bool IsPlay
		{
			[Token(Token = "0x60053B3")]
			[Address(RVA = "0x10165BA64", Offset = "0x165BA64", VA = "0x10165BA64")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x06005A97 RID: 23191 RVA: 0x0001DA30 File Offset: 0x0001BC30
		[Token(Token = "0x170005B7")]
		public bool IsEnd
		{
			[Token(Token = "0x60053B4")]
			[Address(RVA = "0x10165BA6C", Offset = "0x165BA6C", VA = "0x10165BA6C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x06005A98 RID: 23192 RVA: 0x0001DA48 File Offset: 0x0001BC48
		// (set) Token: 0x06005A99 RID: 23193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005B8")]
		public bool IsEnableAuto
		{
			[Token(Token = "0x60053B5")]
			[Address(RVA = "0x10165BA74", Offset = "0x165BA74", VA = "0x10165BA74")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60053B6")]
			[Address(RVA = "0x10165BA7C", Offset = "0x165BA7C", VA = "0x10165BA7C")]
			set
			{
			}
		}

		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06005A9A RID: 23194 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005B9")]
		public ADVUI UI
		{
			[Token(Token = "0x60053B7")]
			[Address(RVA = "0x10165BB44", Offset = "0x165BB44", VA = "0x10165BB44")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x06005A9B RID: 23195 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BA")]
		public StandPicManager StandPicManager
		{
			[Token(Token = "0x60053B8")]
			[Address(RVA = "0x10165BB4C", Offset = "0x165BB4C", VA = "0x10165BB4C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x06005A9C RID: 23196 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BB")]
		public ADVEffectManager ADVEffectManager
		{
			[Token(Token = "0x60053B9")]
			[Address(RVA = "0x10165BB54", Offset = "0x165BB54", VA = "0x10165BB54")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x06005A9D RID: 23197 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BC")]
		public ADVSpriteManager SpriteManager
		{
			[Token(Token = "0x60053BA")]
			[Address(RVA = "0x10165BB5C", Offset = "0x165BB5C", VA = "0x10165BB5C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06005A9E RID: 23198 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BD")]
		public ADVCaption Caption
		{
			[Token(Token = "0x60053BB")]
			[Address(RVA = "0x10165BB64", Offset = "0x165BB64", VA = "0x10165BB64")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x06005A9F RID: 23199 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BE")]
		public ADVBackGroundManager BackGroundManager
		{
			[Token(Token = "0x60053BC")]
			[Address(RVA = "0x10165BB6C", Offset = "0x165BB6C", VA = "0x10165BB6C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700061B RID: 1563
		// (get) Token: 0x06005AA0 RID: 23200 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005BF")]
		public ADVSoundManager SoundManager
		{
			[Token(Token = "0x60053BD")]
			[Address(RVA = "0x10165BB74", Offset = "0x165BB74", VA = "0x10165BB74")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700061C RID: 1564
		// (get) Token: 0x06005AA1 RID: 23201 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C0")]
		public ScriptPlayer ScriptPlayer
		{
			[Token(Token = "0x60053BE")]
			[Address(RVA = "0x10165BB7C", Offset = "0x165BB7C", VA = "0x10165BB7C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700061D RID: 1565
		// (set) Token: 0x06005AA2 RID: 23202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005C1")]
		public bool BreakFlag
		{
			[Token(Token = "0x60053BF")]
			[Address(RVA = "0x101659CA4", Offset = "0x1659CA4", VA = "0x101659CA4")]
			set
			{
			}
		}

		// Token: 0x06005AA3 RID: 23203 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60053C0")]
		[Address(RVA = "0x10165BB84", Offset = "0x165BB84", VA = "0x10165BB84")]
		public string GetTagCommand(ADVParser.EventType eventType)
		{
			return null;
		}

		// Token: 0x06005AA4 RID: 23204 RVA: 0x0001DA60 File Offset: 0x0001BC60
		[Token(Token = "0x60053C1")]
		[Address(RVA = "0x10165BB94", Offset = "0x165BB94", VA = "0x10165BB94")]
		public int GetCommandsCount()
		{
			return 0;
		}

		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x06005AA5 RID: 23205 RVA: 0x0001DA78 File Offset: 0x0001BC78
		[Token(Token = "0x170005C2")]
		public int AdvID
		{
			[Token(Token = "0x60053C2")]
			[Address(RVA = "0x10165BBD0", Offset = "0x165BBD0", VA = "0x10165BBD0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06005AA6 RID: 23206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C3")]
		[Address(RVA = "0x10165BBD8", Offset = "0x165BBD8", VA = "0x10165BBD8")]
		private void OnDestroy()
		{
		}

		// Token: 0x06005AA7 RID: 23207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C4")]
		[Address(RVA = "0x10165BE64", Offset = "0x165BE64", VA = "0x10165BE64")]
		private void ResetCamera()
		{
		}

		// Token: 0x06005AA8 RID: 23208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C5")]
		[Address(RVA = "0x10165C014", Offset = "0x165C014", VA = "0x10165C014")]
		public void PrepareDB()
		{
		}

		// Token: 0x06005AA9 RID: 23209 RVA: 0x0001DA90 File Offset: 0x0001BC90
		[Token(Token = "0x60053C6")]
		[Address(RVA = "0x10165C098", Offset = "0x165C098", VA = "0x10165C098")]
		public bool IsDonePrepareDB()
		{
			return default(bool);
		}

		// Token: 0x06005AAA RID: 23210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C7")]
		[Address(RVA = "0x10165C0C4", Offset = "0x165C0C4", VA = "0x10165C0C4")]
		public void UpdatePrepareDB()
		{
		}

		// Token: 0x06005AAB RID: 23211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C8")]
		[Address(RVA = "0x10165C118", Offset = "0x165C118", VA = "0x10165C118")]
		public void Prepare(int advID)
		{
		}

		// Token: 0x06005AAC RID: 23212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053C9")]
		[Address(RVA = "0x10165C244", Offset = "0x165C244", VA = "0x10165C244")]
		public void Prepare(eADVCategory category, string scriptName, string textName)
		{
		}

		// Token: 0x06005AAD RID: 23213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053CA")]
		[Address(RVA = "0x10165C308", Offset = "0x165C308", VA = "0x10165C308")]
		public void Prepare()
		{
		}

		// Token: 0x06005AAE RID: 23214 RVA: 0x0001DAA8 File Offset: 0x0001BCA8
		[Token(Token = "0x60053CB")]
		[Address(RVA = "0x10165C548", Offset = "0x165C548", VA = "0x10165C548")]
		public ADVCharacterListDB_Param GetADVCharacterParam(string ADVCharaID)
		{
			return default(ADVCharacterListDB_Param);
		}

		// Token: 0x06005AAF RID: 23215 RVA: 0x0001DAC0 File Offset: 0x0001BCC0
		[Token(Token = "0x60053CC")]
		[Address(RVA = "0x10165C688", Offset = "0x165C688", VA = "0x10165C688")]
		public ADVMotionListDB_Param GetADVMotionParam(string ADVMotionID)
		{
			return default(ADVMotionListDB_Param);
		}

		// Token: 0x06005AB0 RID: 23216 RVA: 0x0001DAD8 File Offset: 0x0001BCD8
		[Token(Token = "0x60053CD")]
		[Address(RVA = "0x101656B98", Offset = "0x1656B98", VA = "0x101656B98")]
		public ADVEmotionListDB_Param GetADVEmotionParam(string ADVEmotionID)
		{
			return default(ADVEmotionListDB_Param);
		}

		// Token: 0x06005AB1 RID: 23217 RVA: 0x0001DAF0 File Offset: 0x0001BCF0
		[Token(Token = "0x60053CE")]
		[Address(RVA = "0x10165C7B0", Offset = "0x165C7B0", VA = "0x10165C7B0")]
		public ADVTextTagArgDB_Param GetADVTextTagParam(string ADVTextTagArgRefName)
		{
			return default(ADVTextTagArgDB_Param);
		}

		// Token: 0x06005AB2 RID: 23218 RVA: 0x0001DB08 File Offset: 0x0001BD08
		[Token(Token = "0x60053CF")]
		[Address(RVA = "0x10165C8D8", Offset = "0x165C8D8", VA = "0x10165C8D8")]
		public ADVCharacterListDB_Data GetADVCharacterListDBData(string ADVCharaID, string poseName)
		{
			return default(ADVCharacterListDB_Data);
		}

		// Token: 0x06005AB3 RID: 23219 RVA: 0x0001DB20 File Offset: 0x0001BD20
		[Token(Token = "0x60053D0")]
		[Address(RVA = "0x10165C9C8", Offset = "0x165C9C8", VA = "0x10165C9C8")]
		public int GetCurrentCommandIdx()
		{
			return 0;
		}

		// Token: 0x06005AB4 RID: 23220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D1")]
		[Address(RVA = "0x10165C1E4", Offset = "0x165C1E4", VA = "0x10165C1E4")]
		private void ClearUseResourcePathList()
		{
		}

		// Token: 0x06005AB5 RID: 23221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D2")]
		[Address(RVA = "0x101652F70", Offset = "0x1652F70", VA = "0x101652F70")]
		public void AddUseResourcePathList(string resourcePath)
		{
		}

		// Token: 0x06005AB6 RID: 23222 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60053D3")]
		[Address(RVA = "0x10165C9E0", Offset = "0x165C9E0", VA = "0x10165C9E0")]
		public List<string> GetUseResourcePathList()
		{
			return null;
		}

		// Token: 0x06005AB7 RID: 23223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D4")]
		[Address(RVA = "0x10165C9E8", Offset = "0x165C9E8", VA = "0x10165C9E8")]
		public void ClearNeedStandPic()
		{
		}

		// Token: 0x06005AB8 RID: 23224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D5")]
		[Address(RVA = "0x10165CA18", Offset = "0x165CA18", VA = "0x10165CA18")]
		public void AddNeedStandPic(int idx, string charaID, [Optional] string poseName)
		{
		}

		// Token: 0x06005AB9 RID: 23225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D6")]
		[Address(RVA = "0x10165CE00", Offset = "0x165CE00", VA = "0x10165CE00")]
		public void AddNeedFace(string charaID, eADVFace face)
		{
		}

		// Token: 0x06005ABA RID: 23226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D7")]
		[Address(RVA = "0x10165CFF8", Offset = "0x165CFF8", VA = "0x10165CFF8")]
		public void DestroyAllNotNeedStandPics()
		{
		}

		// Token: 0x06005ABB RID: 23227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D8")]
		[Address(RVA = "0x10165D028", Offset = "0x165D028", VA = "0x10165D028")]
		public void CreateAllNeedStandPics()
		{
		}

		// Token: 0x06005ABC RID: 23228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053D9")]
		[Address(RVA = "0x10165D058", Offset = "0x165D058", VA = "0x10165D058")]
		public void LoadFirstNeedStandPics()
		{
		}

		// Token: 0x06005ABD RID: 23229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DA")]
		[Address(RVA = "0x10165D088", Offset = "0x165D088", VA = "0x10165D088")]
		public void ClearNeedEffect()
		{
		}

		// Token: 0x06005ABE RID: 23230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DB")]
		[Address(RVA = "0x10165D0B4", Offset = "0x165D0B4", VA = "0x10165D0B4")]
		public void AddNeedEffect(string effectID)
		{
		}

		// Token: 0x06005ABF RID: 23231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DC")]
		[Address(RVA = "0x10165D16C", Offset = "0x165D16C", VA = "0x10165D16C")]
		public void AddNeedEmo(string emotionID)
		{
		}

		// Token: 0x06005AC0 RID: 23232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DD")]
		[Address(RVA = "0x10165D1A4", Offset = "0x165D1A4", VA = "0x10165D1A4")]
		public void UnloadAllNotNeedEffect()
		{
		}

		// Token: 0x06005AC1 RID: 23233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DE")]
		[Address(RVA = "0x10165D1D0", Offset = "0x165D1D0", VA = "0x10165D1D0")]
		public void LoadAllNeedEffect()
		{
		}

		// Token: 0x06005AC2 RID: 23234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053DF")]
		[Address(RVA = "0x10165D1FC", Offset = "0x165D1FC", VA = "0x10165D1FC")]
		public void ClearNeedSprite()
		{
		}

		// Token: 0x06005AC3 RID: 23235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E0")]
		[Address(RVA = "0x10165D22C", Offset = "0x165D22C", VA = "0x10165D22C")]
		public void AddNeedSprite(int instId, string fileName)
		{
		}

		// Token: 0x06005AC4 RID: 23236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E1")]
		[Address(RVA = "0x10165D294", Offset = "0x165D294", VA = "0x10165D294")]
		public void LoadRecentNeedSprite()
		{
		}

		// Token: 0x06005AC5 RID: 23237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E2")]
		[Address(RVA = "0x10165D2C4", Offset = "0x165D2C4", VA = "0x10165D2C4")]
		public void LoadFirstNeedResource()
		{
		}

		// Token: 0x06005AC6 RID: 23238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E3")]
		[Address(RVA = "0x10165D3F0", Offset = "0x165D3F0", VA = "0x10165D3F0")]
		public void LoadFullVoiceCueSheet(string fullVoiceCueSheet)
		{
		}

		// Token: 0x06005AC7 RID: 23239 RVA: 0x0001DB38 File Offset: 0x0001BD38
		[Token(Token = "0x60053E4")]
		[Address(RVA = "0x10165D428", Offset = "0x165D428", VA = "0x10165D428")]
		public bool UpdatePrepareScript()
		{
			return default(bool);
		}

		// Token: 0x06005AC8 RID: 23240 RVA: 0x0001DB50 File Offset: 0x0001BD50
		[Token(Token = "0x60053E5")]
		[Address(RVA = "0x10165D474", Offset = "0x165D474", VA = "0x10165D474")]
		public bool IsDonePrepareScript()
		{
			return default(bool);
		}

		// Token: 0x06005AC9 RID: 23241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E6")]
		[Address(RVA = "0x10165D4D4", Offset = "0x165D4D4", VA = "0x10165D4D4")]
		public void UpdatePrepare()
		{
		}

		// Token: 0x06005ACA RID: 23242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E7")]
		[Address(RVA = "0x10165E3B4", Offset = "0x165E3B4", VA = "0x10165E3B4")]
		public void Play()
		{
		}

		// Token: 0x06005ACB RID: 23243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E8")]
		[Address(RVA = "0x10165E43C", Offset = "0x165E43C", VA = "0x10165E43C")]
		private void Update()
		{
		}

		// Token: 0x06005ACC RID: 23244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053E9")]
		[Address(RVA = "0x10165E674", Offset = "0x165E674", VA = "0x10165E674")]
		private void UpdateWork()
		{
		}

		// Token: 0x06005ACD RID: 23245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053EA")]
		[Address(RVA = "0x10165F118", Offset = "0x165F118", VA = "0x10165F118")]
		public void Destroy()
		{
		}

		// Token: 0x06005ACE RID: 23246 RVA: 0x0001DB68 File Offset: 0x0001BD68
		[Token(Token = "0x60053EB")]
		[Address(RVA = "0x10165F22C", Offset = "0x165F22C", VA = "0x10165F22C")]
		public bool UpdateDestroy()
		{
			return default(bool);
		}

		// Token: 0x06005ACF RID: 23247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053EC")]
		[Address(RVA = "0x10165F288", Offset = "0x165F288", VA = "0x10165F288")]
		public void Tap()
		{
		}

		// Token: 0x06005AD0 RID: 23248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053ED")]
		[Address(RVA = "0x10165F984", Offset = "0x165F984", VA = "0x10165F984")]
		public void ParseTalker(string charaName, out string talkerCharaID, out string talkerName)
		{
		}

		// Token: 0x06005AD1 RID: 23249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053EE")]
		[Address(RVA = "0x10165F008", Offset = "0x165F008", VA = "0x10165F008")]
		private void FlushText()
		{
		}

		// Token: 0x06005AD2 RID: 23250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053EF")]
		[Address(RVA = "0x10165FAA8", Offset = "0x165FAA8", VA = "0x10165FAA8")]
		public void AddBackLogText()
		{
		}

		// Token: 0x06005AD3 RID: 23251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F0")]
		[Address(RVA = "0x101653E8C", Offset = "0x1653E8C", VA = "0x101653E8C")]
		public void AddBackLogText(string charaID, string talker, string text, List<ADVParser.RubyData> rubyList)
		{
		}

		// Token: 0x06005AD4 RID: 23252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F1")]
		[Address(RVA = "0x10165FC68", Offset = "0x165FC68", VA = "0x10165FC68")]
		public void CreateSkipLogText()
		{
		}

		// Token: 0x06005AD5 RID: 23253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F2")]
		[Address(RVA = "0x10165FF9C", Offset = "0x165FF9C", VA = "0x10165FF9C")]
		public void SkipScript()
		{
		}

		// Token: 0x06005AD6 RID: 23254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F3")]
		[Address(RVA = "0x10165BA84", Offset = "0x165BA84", VA = "0x10165BA84")]
		public void EnableAuto(bool flag, bool anim = false)
		{
		}

		// Token: 0x06005AD7 RID: 23255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F4")]
		[Address(RVA = "0x10165F66C", Offset = "0x165F66C", VA = "0x10165F66C")]
		public void ToggleShowUI()
		{
		}

		// Token: 0x06005AD8 RID: 23256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F5")]
		[Address(RVA = "0x10165F0A8", Offset = "0x165F0A8", VA = "0x10165F0A8")]
		public void SetWait(float time)
		{
		}

		// Token: 0x06005AD9 RID: 23257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F6")]
		[Address(RVA = "0x1016600C8", Offset = "0x16600C8", VA = "0x1016600C8")]
		public void GoTo(int scriptId)
		{
		}

		// Token: 0x06005ADA RID: 23258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F7")]
		[Address(RVA = "0x101660100", Offset = "0x1660100", VA = "0x101660100")]
		public void FillScreen(Color rgba)
		{
		}

		// Token: 0x06005ADB RID: 23259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F8")]
		[Address(RVA = "0x101660178", Offset = "0x1660178", VA = "0x101660178")]
		public void Fade(Color rgba, float time)
		{
		}

		// Token: 0x06005ADC RID: 23260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053F9")]
		[Address(RVA = "0x101660200", Offset = "0x1660200", VA = "0x101660200")]
		public void WaitFade()
		{
		}

		// Token: 0x06005ADD RID: 23261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FA")]
		[Address(RVA = "0x10166020C", Offset = "0x166020C", VA = "0x10166020C")]
		public void WaitCharaFade()
		{
		}

		// Token: 0x06005ADE RID: 23262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FB")]
		[Address(RVA = "0x101660218", Offset = "0x1660218", VA = "0x101660218")]
		public void WaitMotion()
		{
		}

		// Token: 0x06005ADF RID: 23263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FC")]
		[Address(RVA = "0x101660224", Offset = "0x1660224", VA = "0x101660224")]
		public void WaitMove()
		{
		}

		// Token: 0x06005AE0 RID: 23264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FD")]
		[Address(RVA = "0x101660230", Offset = "0x1660230", VA = "0x101660230")]
		public void WaitRotate()
		{
		}

		// Token: 0x06005AE1 RID: 23265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FE")]
		[Address(RVA = "0x10166023C", Offset = "0x166023C", VA = "0x10166023C")]
		public void WaitSE()
		{
		}

		// Token: 0x06005AE2 RID: 23266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60053FF")]
		[Address(RVA = "0x101660248", Offset = "0x1660248", VA = "0x101660248")]
		public void WaitVOICE()
		{
		}

		// Token: 0x06005AE3 RID: 23267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005400")]
		[Address(RVA = "0x101660254", Offset = "0x1660254", VA = "0x101660254")]
		public void WaitEffect(string effectId)
		{
		}

		// Token: 0x06005AE4 RID: 23268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005401")]
		[Address(RVA = "0x10166025C", Offset = "0x166025C", VA = "0x10166025C")]
		public void WaitEmotion(string emotionId)
		{
		}

		// Token: 0x06005AE5 RID: 23269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005402")]
		[Address(RVA = "0x101660290", Offset = "0x1660290", VA = "0x101660290")]
		public void WaitCharaMove(string charaId)
		{
		}

		// Token: 0x06005AE6 RID: 23270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005403")]
		[Address(RVA = "0x101660298", Offset = "0x1660298", VA = "0x101660298")]
		public void WaitCharaRotate(string charaId)
		{
		}

		// Token: 0x06005AE7 RID: 23271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005404")]
		[Address(RVA = "0x1016602A0", Offset = "0x16602A0", VA = "0x1016602A0")]
		public void WaitSpriteFade(int id = -1)
		{
		}

		// Token: 0x06005AE8 RID: 23272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005405")]
		[Address(RVA = "0x1016602B0", Offset = "0x16602B0", VA = "0x1016602B0")]
		public void WaitSpritePos(int id = -1)
		{
		}

		// Token: 0x06005AE9 RID: 23273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005406")]
		[Address(RVA = "0x1016602C0", Offset = "0x16602C0", VA = "0x1016602C0")]
		public void WaitSpriteScale(int id = -1)
		{
		}

		// Token: 0x06005AEA RID: 23274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005407")]
		[Address(RVA = "0x1016602D0", Offset = "0x16602D0", VA = "0x1016602D0")]
		public void WaitSpriteColor(int id = -1)
		{
		}

		// Token: 0x06005AEB RID: 23275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005408")]
		[Address(RVA = "0x1016602E0", Offset = "0x16602E0", VA = "0x1016602E0")]
		public void WaitBGScroll(int id = -1)
		{
		}

		// Token: 0x06005AEC RID: 23276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005409")]
		[Address(RVA = "0x1016602F0", Offset = "0x16602F0", VA = "0x1016602F0")]
		public void WaitBGScale(int id = -1)
		{
		}

		// Token: 0x06005AED RID: 23277 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600540A")]
		[Address(RVA = "0x101660300", Offset = "0x1660300", VA = "0x101660300")]
		public string GetTalkerADVCharaID(uint m_TextID)
		{
			return null;
		}

		// Token: 0x06005AEE RID: 23278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600540B")]
		[Address(RVA = "0x1016604D4", Offset = "0x16604D4", VA = "0x1016604D4")]
		public void CharaHighlight(string m_ADVCharaID)
		{
		}

		// Token: 0x06005AEF RID: 23279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600540C")]
		[Address(RVA = "0x10166050C", Offset = "0x166050C", VA = "0x10166050C")]
		public void CharaHighlightAll()
		{
		}

		// Token: 0x06005AF0 RID: 23280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600540D")]
		[Address(RVA = "0x10166053C", Offset = "0x166053C", VA = "0x10166053C")]
		public void CharaHighlightDefault(string m_ADVCharaID)
		{
		}

		// Token: 0x06005AF1 RID: 23281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600540E")]
		[Address(RVA = "0x101660574", Offset = "0x1660574", VA = "0x101660574")]
		public void CharaHighlightDefaultAll()
		{
		}

		// Token: 0x06005AF2 RID: 23282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600540F")]
		[Address(RVA = "0x1016605A4", Offset = "0x16605A4", VA = "0x1016605A4")]
		public void CharaShading(string m_ADVCharaID)
		{
		}

		// Token: 0x06005AF3 RID: 23283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005410")]
		[Address(RVA = "0x1016605DC", Offset = "0x16605DC", VA = "0x1016605DC")]
		public void CharaShadingAll()
		{
		}

		// Token: 0x06005AF4 RID: 23284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005411")]
		[Address(RVA = "0x10166060C", Offset = "0x166060C", VA = "0x10166060C")]
		public void CharaHighlightTalker(string m_ADVCharaID)
		{
		}

		// Token: 0x06005AF5 RID: 23285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005412")]
		[Address(RVA = "0x101660644", Offset = "0x1660644", VA = "0x101660644")]
		public void CharaHighlightTalkerResetAll()
		{
		}

		// Token: 0x06005AF6 RID: 23286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005413")]
		[Address(RVA = "0x101660674", Offset = "0x1660674", VA = "0x101660674")]
		public void CharaTalk(uint m_TextID, bool fullVoice)
		{
		}

		// Token: 0x06005AF7 RID: 23287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005414")]
		[Address(RVA = "0x101660B30", Offset = "0x1660B30", VA = "0x101660B30")]
		public void OpenTalk()
		{
		}

		// Token: 0x06005AF8 RID: 23288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005415")]
		[Address(RVA = "0x101660B7C", Offset = "0x1660B7C", VA = "0x101660B7C")]
		public void CloseTalk()
		{
		}

		// Token: 0x06005AF9 RID: 23289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005416")]
		[Address(RVA = "0x101660BC8", Offset = "0x1660BC8", VA = "0x101660BC8")]
		public void AddNovelText(uint m_TextID)
		{
		}

		// Token: 0x06005AFA RID: 23290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005417")]
		[Address(RVA = "0x101660F74", Offset = "0x1660F74", VA = "0x101660F74")]
		public void NovelInsertLine()
		{
		}

		// Token: 0x06005AFB RID: 23291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005418")]
		[Address(RVA = "0x101660FBC", Offset = "0x1660FBC", VA = "0x101660FBC")]
		public void ClearNovelText()
		{
		}

		// Token: 0x06005AFC RID: 23292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005419")]
		[Address(RVA = "0x101661004", Offset = "0x1661004", VA = "0x101661004")]
		public void SetNovelAnchor(eADVAnchor anchor, Vector2 pos)
		{
		}

		// Token: 0x06005AFD RID: 23293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541A")]
		[Address(RVA = "0x10166106C", Offset = "0x166106C", VA = "0x10166106C")]
		public void SetNovelAnchorDefault()
		{
		}

		// Token: 0x06005AFE RID: 23294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541B")]
		[Address(RVA = "0x1016610B4", Offset = "0x16610B4", VA = "0x1016610B4")]
		public void CloseNovel()
		{
		}

		// Token: 0x06005AFF RID: 23295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541C")]
		[Address(RVA = "0x10165F6CC", Offset = "0x165F6CC", VA = "0x10165F6CC")]
		private void SkipTextProgress()
		{
		}

		// Token: 0x06005B00 RID: 23296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541D")]
		[Address(RVA = "0x101661100", Offset = "0x1661100", VA = "0x101661100")]
		public void SetRuby(ADVParser.RubyData rubyData)
		{
		}

		// Token: 0x06005B01 RID: 23297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541E")]
		[Address(RVA = "0x10165F0B0", Offset = "0x165F0B0", VA = "0x10165F0B0")]
		public void SetRubyNovel(ADVParser.RubyData rubyData)
		{
		}

		// Token: 0x06005B02 RID: 23298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600541F")]
		[Address(RVA = "0x101661150", Offset = "0x1661150", VA = "0x101661150")]
		public void PlayCaption(uint m_TextID)
		{
		}

		// Token: 0x06005B03 RID: 23299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005420")]
		[Address(RVA = "0x1016612C0", Offset = "0x16612C0", VA = "0x1016612C0")]
		public void PlayEffect(string effectID, eADVStandPosition standPosition, float offsetX, float offsetY, float rotate, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005B04 RID: 23300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005421")]
		[Address(RVA = "0x1016614D4", Offset = "0x16614D4", VA = "0x1016614D4")]
		public void EffectLoopTarget(string effectID, string afterEffectID, eADVStandPosition standPosition, float offsetX, float offsetY, float rotate, bool isBehind = false)
		{
		}

		// Token: 0x06005B05 RID: 23301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005422")]
		[Address(RVA = "0x101661748", Offset = "0x1661748", VA = "0x101661748")]
		public void PlayEffectPreset(eADVStandPosition standPosition, float offsetX, float offsetY, float rotate, bool isBehind = false)
		{
		}

		// Token: 0x06005B06 RID: 23302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005423")]
		[Address(RVA = "0x101661940", Offset = "0x1661940", VA = "0x101661940")]
		public void PlayEffect(string effectID, string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005B07 RID: 23303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005424")]
		[Address(RVA = "0x101661BD4", Offset = "0x1661BD4", VA = "0x101661BD4")]
		public void EffectCharaLoopTarget(string effectID, string afterEffectID, string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate, bool isBehind = false)
		{
		}

		// Token: 0x06005B08 RID: 23304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005425")]
		[Address(RVA = "0x101661EC8", Offset = "0x1661EC8", VA = "0x101661EC8")]
		public void PlayEffectPreset(string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate, bool isBehind = false)
		{
		}

		// Token: 0x06005B09 RID: 23305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005426")]
		[Address(RVA = "0x101662140", Offset = "0x1662140", VA = "0x101662140")]
		public void PlayEffectScreen(string effectID, eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005B0A RID: 23306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005427")]
		[Address(RVA = "0x101662378", Offset = "0x1662378", VA = "0x101662378")]
		public void EffectScreenLoopTarget(string effectID, string afterEffectID, eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate, bool loop = false, bool isBehind = false)
		{
		}

		// Token: 0x06005B0B RID: 23307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005428")]
		[Address(RVA = "0x10166260C", Offset = "0x166260C", VA = "0x10166260C")]
		public void PlayEffectScreenPreset(eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate, bool isBehind = false)
		{
		}

		// Token: 0x06005B0C RID: 23308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005429")]
		[Address(RVA = "0x101662828", Offset = "0x1662828", VA = "0x101662828")]
		public void PlayEmotion(string charaID, string emotionID, eADVEmotionPosition position, bool isBehind = false)
		{
		}

		// Token: 0x06005B0D RID: 23309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600542A")]
		[Address(RVA = "0x1016629E8", Offset = "0x16629E8", VA = "0x1016629E8")]
		public void SetEnableEmoOffsetRotate(bool flg)
		{
		}

		// Token: 0x06005B0E RID: 23310 RVA: 0x0001DB80 File Offset: 0x0001BD80
		[Token(Token = "0x600542B")]
		[Address(RVA = "0x1016629F0", Offset = "0x16629F0", VA = "0x1016629F0")]
		public bool GetEnableEmoOffsetRotate()
		{
			return default(bool);
		}

		// Token: 0x06005B0F RID: 23311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600542C")]
		[Address(RVA = "0x1016629F8", Offset = "0x16629F8", VA = "0x1016629F8")]
		public void StopShake()
		{
		}

		// Token: 0x06005B10 RID: 23312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600542D")]
		[Address(RVA = "0x101662B30", Offset = "0x1662B30", VA = "0x101662B30")]
		public void ShakeAll(eADVShakeType type, float time)
		{
		}

		// Token: 0x06005B11 RID: 23313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600542E")]
		[Address(RVA = "0x101662C04", Offset = "0x1662C04", VA = "0x101662C04")]
		public void ShakeChara(eADVShakeType type, float time, bool isAllShake = false)
		{
		}

		// Token: 0x06005B12 RID: 23314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600542F")]
		[Address(RVA = "0x10166303C", Offset = "0x166303C", VA = "0x10166303C")]
		public void ShakeOnlyChara(ADVShake target, eADVShakeType type, float time)
		{
		}

		// Token: 0x06005B13 RID: 23315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005430")]
		[Address(RVA = "0x101662D88", Offset = "0x1662D88", VA = "0x101662D88")]
		public void ShakeBG(eADVShakeType type, float time, bool isAllShake = false)
		{
		}

		// Token: 0x06005B14 RID: 23316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005431")]
		[Address(RVA = "0x101662E64", Offset = "0x1662E64", VA = "0x101662E64")]
		public void Shake(ADVShake target, bool isAllShake, bool isTargetCharaShake, eADVShakeType type, float time)
		{
		}

		// Token: 0x06005B15 RID: 23317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005432")]
		[Address(RVA = "0x101663158", Offset = "0x1663158", VA = "0x101663158")]
		public void MoveToNextPosition()
		{
		}

		// Token: 0x06005B16 RID: 23318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005433")]
		[Address(RVA = "0x101663268", Offset = "0x1663268", VA = "0x101663268")]
		public void MoveToNextPositionChara()
		{
		}

		// Token: 0x06005B17 RID: 23319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005434")]
		[Address(RVA = "0x101662BB4", Offset = "0x1662BB4", VA = "0x101662BB4")]
		private void UpdateShakeAllRandomParameter()
		{
		}

		// Token: 0x06005B18 RID: 23320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005435")]
		[Address(RVA = "0x10166333C", Offset = "0x166333C", VA = "0x10166333C")]
		private void UpdateShakeAllRandomParameterX()
		{
		}

		// Token: 0x06005B19 RID: 23321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005436")]
		[Address(RVA = "0x101663370", Offset = "0x1663370", VA = "0x101663370")]
		private void UpdateShakeAllRandomParameterY()
		{
		}

		// Token: 0x06005B1A RID: 23322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005437")]
		[Address(RVA = "0x101662E14", Offset = "0x1662E14", VA = "0x101662E14")]
		private void UpdateCharaRandomParameter()
		{
		}

		// Token: 0x06005B1B RID: 23323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005438")]
		[Address(RVA = "0x1016633A4", Offset = "0x16633A4", VA = "0x1016633A4")]
		private void UpdateCharaRandomParameterX()
		{
		}

		// Token: 0x06005B1C RID: 23324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005439")]
		[Address(RVA = "0x1016633D8", Offset = "0x16633D8", VA = "0x1016633D8")]
		private void UpdateCharaRandomParameterY()
		{
		}

		// Token: 0x06005B1D RID: 23325 RVA: 0x0001DB98 File Offset: 0x0001BD98
		[Token(Token = "0x600543A")]
		[Address(RVA = "0x10166340C", Offset = "0x166340C", VA = "0x10166340C")]
		public float GetShakeAllRandomParameter(int i)
		{
			return 0f;
		}

		// Token: 0x06005B1E RID: 23326 RVA: 0x0001DBB0 File Offset: 0x0001BDB0
		[Token(Token = "0x600543B")]
		[Address(RVA = "0x10166342C", Offset = "0x166342C", VA = "0x10166342C")]
		public float GetShakeAllRandomParameterX()
		{
			return 0f;
		}

		// Token: 0x06005B1F RID: 23327 RVA: 0x0001DBC8 File Offset: 0x0001BDC8
		[Token(Token = "0x600543C")]
		[Address(RVA = "0x101663434", Offset = "0x1663434", VA = "0x101663434")]
		public float GetShakeAllRandomParameterY()
		{
			return 0f;
		}

		// Token: 0x06005B20 RID: 23328 RVA: 0x0001DBE0 File Offset: 0x0001BDE0
		[Token(Token = "0x600543D")]
		[Address(RVA = "0x10166343C", Offset = "0x166343C", VA = "0x10166343C")]
		public float GetCharaRandomParameter(int i)
		{
			return 0f;
		}

		// Token: 0x06005B21 RID: 23329 RVA: 0x0001DBF8 File Offset: 0x0001BDF8
		[Token(Token = "0x600543E")]
		[Address(RVA = "0x10166345C", Offset = "0x166345C", VA = "0x10166345C")]
		public float GetCharaRandomParameterX()
		{
			return 0f;
		}

		// Token: 0x06005B22 RID: 23330 RVA: 0x0001DC10 File Offset: 0x0001BE10
		[Token(Token = "0x600543F")]
		[Address(RVA = "0x101663464", Offset = "0x1663464", VA = "0x101663464")]
		public float GetCharaRandomParameterY()
		{
			return 0f;
		}

		// Token: 0x06005B23 RID: 23331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005440")]
		[Address(RVA = "0x10166346C", Offset = "0x166346C", VA = "0x10166346C")]
		public ADVPlayer()
		{
		}

		// Token: 0x04006D2D RID: 27949
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004D51")]
		[SerializeField]
		private CanvasScaler m_CanvasScaler;

		// Token: 0x04006D2E RID: 27950
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004D52")]
		private float m_WaitTimer;

		// Token: 0x04006D2F RID: 27951
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004D53")]
		private bool m_WaitFade;

		// Token: 0x04006D30 RID: 27952
		[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
		[Token(Token = "0x4004D54")]
		private bool m_WaitCharaFade;

		// Token: 0x04006D31 RID: 27953
		[Cpp2IlInjected.FieldOffset(Offset = "0x26")]
		[Token(Token = "0x4004D55")]
		private bool m_WaitMotion;

		// Token: 0x04006D32 RID: 27954
		[Cpp2IlInjected.FieldOffset(Offset = "0x27")]
		[Token(Token = "0x4004D56")]
		private bool m_WaitMove;

		// Token: 0x04006D33 RID: 27955
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004D57")]
		private bool m_WaitSE;

		// Token: 0x04006D34 RID: 27956
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4004D58")]
		private bool m_WaitVOICE;

		// Token: 0x04006D35 RID: 27957
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004D59")]
		private string m_WaitEffectId;

		// Token: 0x04006D36 RID: 27958
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004D5A")]
		private string m_WaitMoveCharaId;

		// Token: 0x04006D37 RID: 27959
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004D5B")]
		private bool m_WaitRot;

		// Token: 0x04006D38 RID: 27960
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004D5C")]
		private string m_WaitRotCharaId;

		// Token: 0x04006D39 RID: 27961
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004D5D")]
		private bool m_WaitSpriteFade;

		// Token: 0x04006D3A RID: 27962
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004D5E")]
		private int m_WaitSpriteFadeTarget;

		// Token: 0x04006D3B RID: 27963
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004D5F")]
		private bool m_WaitSpritePos;

		// Token: 0x04006D3C RID: 27964
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4004D60")]
		private int m_WaitSpritePosTarget;

		// Token: 0x04006D3D RID: 27965
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004D61")]
		private bool m_WaitSpriteScale;

		// Token: 0x04006D3E RID: 27966
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004D62")]
		private int m_WaitSpriteScaleTarget;

		// Token: 0x04006D3F RID: 27967
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004D63")]
		private bool m_WaitSpriteColor;

		// Token: 0x04006D40 RID: 27968
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004D64")]
		private int m_WaitSpriteColorTarget;

		// Token: 0x04006D41 RID: 27969
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004D65")]
		private bool m_WaitBGScroll;

		// Token: 0x04006D42 RID: 27970
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4004D66")]
		private int m_WaitBGScrollTarget;

		// Token: 0x04006D43 RID: 27971
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004D67")]
		private bool m_WaitBGScale;

		// Token: 0x04006D44 RID: 27972
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4004D68")]
		private int m_WaitBGScaleTarget;

		// Token: 0x04006D45 RID: 27973
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004D69")]
		[SerializeField]
		private float m_TextProgressPerSec;

		// Token: 0x04006D46 RID: 27974
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4004D6A")]
		[SerializeField]
		private float m_AutoWait;

		// Token: 0x04006D47 RID: 27975
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004D6B")]
		private ADVPlayer.eNovelTextState m_NovelTextState;

		// Token: 0x04006D48 RID: 27976
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004D6C")]
		private string m_TalkerText;

		// Token: 0x04006D49 RID: 27977
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004D6D")]
		private string m_TalkText;

		// Token: 0x04006D4A RID: 27978
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004D6E")]
		private string m_TalkerCharaID;

		// Token: 0x04006D4B RID: 27979
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004D6F")]
		private int m_TextCount;

		// Token: 0x04006D4C RID: 27980
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004D70")]
		private float m_TextCountTimer;

		// Token: 0x04006D4D RID: 27981
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004D71")]
		private bool m_IsAuto;

		// Token: 0x04006D4E RID: 27982
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4004D72")]
		private float m_AutoWaitCount;

		// Token: 0x04006D4F RID: 27983
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004D73")]
		private bool m_ShowUI;

		// Token: 0x04006D50 RID: 27984
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004D74")]
		private List<ADVParser.EventType> m_msgEventList;

		// Token: 0x04006D51 RID: 27985
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004D75")]
		private ADVParser m_Parser;

		// Token: 0x04006D52 RID: 27986
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004D76")]
		[SerializeField]
		private StandPicManager m_StandPicManager;

		// Token: 0x04006D53 RID: 27987
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004D77")]
		[SerializeField]
		private ADVEffectManager m_ADVEffectManager;

		// Token: 0x04006D54 RID: 27988
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004D78")]
		[SerializeField]
		private ADVSpriteManager m_SpriteManager;

		// Token: 0x04006D55 RID: 27989
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004D79")]
		[SerializeField]
		private ADVCaption m_Caption;

		// Token: 0x04006D56 RID: 27990
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004D7A")]
		[SerializeField]
		private ADVBackGroundManager m_BackGroundManager;

		// Token: 0x04006D57 RID: 27991
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004D7B")]
		[SerializeField]
		private Camera m_BGCamera;

		// Token: 0x04006D58 RID: 27992
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004D7C")]
		[SerializeField]
		private Camera m_PostProcessCamera;

		// Token: 0x04006D59 RID: 27993
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004D7D")]
		private ADVSoundManager m_SoundManager;

		// Token: 0x04006D5A RID: 27994
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004D7E")]
		private ScriptPlayer m_ScriptPlayer;

		// Token: 0x04006D5B RID: 27995
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004D7F")]
		[SerializeField]
		private ADVShake m_ShakeEffect;

		// Token: 0x04006D5C RID: 27996
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004D80")]
		[SerializeField]
		private ADVShake m_ShakeChara;

		// Token: 0x04006D5D RID: 27997
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004D81")]
		[SerializeField]
		private ADVShake m_ShakeSpr;

		// Token: 0x04006D5E RID: 27998
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004D82")]
		[SerializeField]
		private ADVShake m_ShakeBG;

		// Token: 0x04006D5F RID: 27999
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004D83")]
		[SerializeField]
		private ADVUI m_UI;

		// Token: 0x04006D60 RID: 28000
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004D84")]
		[SerializeField]
		private GameObject m_SaveAreasParent;

		// Token: 0x04006D61 RID: 28001
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4004D85")]
		[SerializeField]
		private PixelCrashWrapper m_PixelCrashWrapperPrefab;

		// Token: 0x04006D62 RID: 28002
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4004D86")]
		private PixelCrashWrapper m_PixelCrashWrapper;

		// Token: 0x04006D63 RID: 28003
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4004D87")]
		private readonly Color PixelCrashBaseColor;

		// Token: 0x04006D64 RID: 28004
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4004D88")]
		private float m_PixelCrashPrepareTime;

		// Token: 0x04006D65 RID: 28005
		[Cpp2IlInjected.FieldOffset(Offset = "0x16C")]
		[Token(Token = "0x4004D89")]
		private float m_PixelCrashPrepareTimeMax;

		// Token: 0x04006D69 RID: 28009
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4004D8D")]
		private List<ADVStandPic> m_StandPicForPixelCrash;

		// Token: 0x04006D6A RID: 28010
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4004D8E")]
		private List<ADVStandPic> m_StandPicForDistorsion;

		// Token: 0x04006D6B RID: 28011
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4004D8F")]
		private MultiCameraPostProcessFilter m_PostProcessRenderer;

		// Token: 0x04006D6C RID: 28012
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x4004D90")]
		[SerializeField]
		private Texture2D m_NoiseTexture;

		// Token: 0x04006D6D RID: 28013
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x4004D91")]
		[SerializeField]
		private Texture2D m_NoiseLevelTexture;

		// Token: 0x04006D6E RID: 28014
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x4004D92")]
		private ScrollDistortionRenderer m_ScrollDistorsionRendererForChara;

		// Token: 0x04006D6F RID: 28015
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4004D93")]
		private ScrollDistortionRenderer m_ScrollDistorsionWrapperForBG;

		// Token: 0x04006D70 RID: 28016
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4004D94")]
		private ShadeOffRenderer m_ShadeOffRenderer;

		// Token: 0x04006D71 RID: 28017
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x4004D95")]
		[SerializeField]
		private FilterRendererBase m_FilterRenderer;

		// Token: 0x04006D72 RID: 28018
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C0")]
		[Token(Token = "0x4004D96")]
		private float m_PP_ContrastLevel;

		// Token: 0x04006D73 RID: 28019
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C4")]
		[Token(Token = "0x4004D97")]
		private float m_PP_ContrastLevelTgt;

		// Token: 0x04006D74 RID: 28020
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C8")]
		[Token(Token = "0x4004D98")]
		private float m_PP_ContrastLevelTime;

		// Token: 0x04006D75 RID: 28021
		[Cpp2IlInjected.FieldOffset(Offset = "0x1CC")]
		[Token(Token = "0x4004D99")]
		private float m_PP_ContrastLevelTimeMax;

		// Token: 0x04006D76 RID: 28022
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D0")]
		[Token(Token = "0x4004D9A")]
		private float m_PP_BrightnessLevel;

		// Token: 0x04006D77 RID: 28023
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D4")]
		[Token(Token = "0x4004D9B")]
		private float m_PP_BrightnessLevelTgt;

		// Token: 0x04006D78 RID: 28024
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D8")]
		[Token(Token = "0x4004D9C")]
		private float m_PP_BrightnessLevelTime;

		// Token: 0x04006D79 RID: 28025
		[Cpp2IlInjected.FieldOffset(Offset = "0x1DC")]
		[Token(Token = "0x4004D9D")]
		private float m_PP_BrightnessLevelTimeMax;

		// Token: 0x04006D7A RID: 28026
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E0")]
		[Token(Token = "0x4004D9E")]
		private float m_PP_ChromaLevel;

		// Token: 0x04006D7B RID: 28027
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E4")]
		[Token(Token = "0x4004D9F")]
		private float m_PP_ChromaLevelTgt;

		// Token: 0x04006D7C RID: 28028
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E8")]
		[Token(Token = "0x4004DA0")]
		private float m_PP_ChromaLevelTime;

		// Token: 0x04006D7D RID: 28029
		[Cpp2IlInjected.FieldOffset(Offset = "0x1EC")]
		[Token(Token = "0x4004DA1")]
		private float m_PP_ChromaLevelTimeMax;

		// Token: 0x04006D7E RID: 28030
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F0")]
		[Token(Token = "0x4004DA2")]
		private float m_PP_ColorBlendLevel;

		// Token: 0x04006D7F RID: 28031
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F4")]
		[Token(Token = "0x4004DA3")]
		private float m_PP_ColorBlendLevelTgt;

		// Token: 0x04006D80 RID: 28032
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F8")]
		[Token(Token = "0x4004DA4")]
		private float m_PP_ColorBlendLevelTime;

		// Token: 0x04006D81 RID: 28033
		[Cpp2IlInjected.FieldOffset(Offset = "0x1FC")]
		[Token(Token = "0x4004DA5")]
		private float m_PP_ColorBlendLevelTimeMax;

		// Token: 0x04006D82 RID: 28034
		[Cpp2IlInjected.FieldOffset(Offset = "0x200")]
		[Token(Token = "0x4004DA6")]
		private float m_PP_BloomThreshold;

		// Token: 0x04006D83 RID: 28035
		[Cpp2IlInjected.FieldOffset(Offset = "0x204")]
		[Token(Token = "0x4004DA7")]
		private float m_PP_BloomThresholdTgt;

		// Token: 0x04006D84 RID: 28036
		[Cpp2IlInjected.FieldOffset(Offset = "0x208")]
		[Token(Token = "0x4004DA8")]
		private float m_PP_BloomThresholdTime;

		// Token: 0x04006D85 RID: 28037
		[Cpp2IlInjected.FieldOffset(Offset = "0x20C")]
		[Token(Token = "0x4004DA9")]
		private float m_PP_BloomThresholdTimeMax;

		// Token: 0x04006D86 RID: 28038
		[Cpp2IlInjected.FieldOffset(Offset = "0x210")]
		[Token(Token = "0x4004DAA")]
		private float m_CharaDistorsionLevel;

		// Token: 0x04006D87 RID: 28039
		[Cpp2IlInjected.FieldOffset(Offset = "0x214")]
		[Token(Token = "0x4004DAB")]
		private float m_CharaDistorsionLevelTgt;

		// Token: 0x04006D88 RID: 28040
		[Cpp2IlInjected.FieldOffset(Offset = "0x218")]
		[Token(Token = "0x4004DAC")]
		private float m_CharaDistorsionTime;

		// Token: 0x04006D89 RID: 28041
		[Cpp2IlInjected.FieldOffset(Offset = "0x21C")]
		[Token(Token = "0x4004DAD")]
		private float m_CharaDistorsionTimeMax;

		// Token: 0x04006D8A RID: 28042
		[Cpp2IlInjected.FieldOffset(Offset = "0x220")]
		[Token(Token = "0x4004DAE")]
		private float m_BGDistorsionLevel;

		// Token: 0x04006D8B RID: 28043
		[Cpp2IlInjected.FieldOffset(Offset = "0x224")]
		[Token(Token = "0x4004DAF")]
		private float m_BGDistorsionLevelTgt;

		// Token: 0x04006D8C RID: 28044
		[Cpp2IlInjected.FieldOffset(Offset = "0x228")]
		[Token(Token = "0x4004DB0")]
		private float m_BGDistorsionTime;

		// Token: 0x04006D8D RID: 28045
		[Cpp2IlInjected.FieldOffset(Offset = "0x22C")]
		[Token(Token = "0x4004DB1")]
		private float m_BGDistorsionTimeMax;

		// Token: 0x04006D8E RID: 28046
		[Cpp2IlInjected.FieldOffset(Offset = "0x230")]
		[Token(Token = "0x4004DB2")]
		private float m_ShadeOffLevel;

		// Token: 0x04006D8F RID: 28047
		[Cpp2IlInjected.FieldOffset(Offset = "0x234")]
		[Token(Token = "0x4004DB3")]
		private float m_ShadeOffLevelTgt;

		// Token: 0x04006D90 RID: 28048
		[Cpp2IlInjected.FieldOffset(Offset = "0x238")]
		[Token(Token = "0x4004DB4")]
		private float m_ShadeOffTime;

		// Token: 0x04006D91 RID: 28049
		[Cpp2IlInjected.FieldOffset(Offset = "0x23C")]
		[Token(Token = "0x4004DB5")]
		private float m_ShadeOffTimeMax;

		// Token: 0x04006D92 RID: 28050
		[Cpp2IlInjected.FieldOffset(Offset = "0x240")]
		[Token(Token = "0x4004DB6")]
		private List<ADVParser.RubyData> m_RubyList;

		// Token: 0x04006D93 RID: 28051
		[Cpp2IlInjected.FieldOffset(Offset = "0x248")]
		[Token(Token = "0x4004DB7")]
		private bool m_IsDonePrepare;

		// Token: 0x04006D94 RID: 28052
		[Cpp2IlInjected.FieldOffset(Offset = "0x249")]
		[Token(Token = "0x4004DB8")]
		private bool m_IsPlay;

		// Token: 0x04006D95 RID: 28053
		[Cpp2IlInjected.FieldOffset(Offset = "0x24A")]
		[Token(Token = "0x4004DB9")]
		private bool m_IsEnd;

		// Token: 0x04006D96 RID: 28054
		[Cpp2IlInjected.FieldOffset(Offset = "0x24C")]
		[Token(Token = "0x4004DBA")]
		private float m_ShakeAllRandomParamX;

		// Token: 0x04006D97 RID: 28055
		[Cpp2IlInjected.FieldOffset(Offset = "0x250")]
		[Token(Token = "0x4004DBB")]
		private float m_ShakeAllRandomParamY;

		// Token: 0x04006D98 RID: 28056
		[Cpp2IlInjected.FieldOffset(Offset = "0x254")]
		[Token(Token = "0x4004DBC")]
		private float m_CharaRandomParamX;

		// Token: 0x04006D99 RID: 28057
		[Cpp2IlInjected.FieldOffset(Offset = "0x258")]
		[Token(Token = "0x4004DBD")]
		private float m_CharaRandomParamY;

		// Token: 0x04006D9A RID: 28058
		[Cpp2IlInjected.FieldOffset(Offset = "0x260")]
		[Token(Token = "0x4004DBE")]
		private List<ADVShake> m_OnlyCharaShakes;

		// Token: 0x04006D9B RID: 28059
		[Cpp2IlInjected.FieldOffset(Offset = "0x268")]
		[Token(Token = "0x4004DBF")]
		private ADVDataBase m_DB;

		// Token: 0x04006D9C RID: 28060
		[Cpp2IlInjected.FieldOffset(Offset = "0x270")]
		[Token(Token = "0x4004DC0")]
		private bool m_BreakFlag;

		// Token: 0x04006D9D RID: 28061
		[Cpp2IlInjected.FieldOffset(Offset = "0x271")]
		[Token(Token = "0x4004DC1")]
		private bool m_StrongTextFlag;

		// Token: 0x04006D9E RID: 28062
		[Cpp2IlInjected.FieldOffset(Offset = "0x272")]
		[Token(Token = "0x4004DC2")]
		private bool m_IsEmoOffsetRotate;

		// Token: 0x04006D9F RID: 28063
		[Cpp2IlInjected.FieldOffset(Offset = "0x274")]
		[Token(Token = "0x4004DC3")]
		private int m_AdvID;

		// Token: 0x04006DA0 RID: 28064
		[Cpp2IlInjected.FieldOffset(Offset = "0x278")]
		[Token(Token = "0x4004DC4")]
		private List<string> m_UseResourcePathList;

		// Token: 0x020011B2 RID: 4530
		[Token(Token = "0x20012B4")]
		private enum eNovelTextState
		{
			// Token: 0x04006DA2 RID: 28066
			[Token(Token = "0x400730B")]
			None,
			// Token: 0x04006DA3 RID: 28067
			[Token(Token = "0x400730C")]
			Progress,
			// Token: 0x04006DA4 RID: 28068
			[Token(Token = "0x400730D")]
			VoiceWait,
			// Token: 0x04006DA5 RID: 28069
			[Token(Token = "0x400730E")]
			TapWait
		}
	}
}
