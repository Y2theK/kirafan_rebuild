﻿using System;
using System.Runtime.InteropServices;
using ADV;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011B3 RID: 4531
	[Token(Token = "0x2000BBE")]
	[StructLayout(3)]
	public class ADVScriptCommandInherited : ADVScriptCommand
	{
		// Token: 0x06005B24 RID: 23332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005441")]
		[Address(RVA = "0x10166364C", Offset = "0x166364C", VA = "0x10166364C")]
		public void Setup(ADVPlayer player)
		{
		}

		// Token: 0x06005B25 RID: 23333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005442")]
		[Address(RVA = "0x1016636F4", Offset = "0x16636F4", VA = "0x1016636F4", Slot = "4")]
		public override void Wait(float m_Sec)
		{
		}

		// Token: 0x06005B26 RID: 23334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005443")]
		[Address(RVA = "0x101663750", Offset = "0x1663750", VA = "0x101663750", Slot = "5")]
		public override void GoTo(uint m_ScriptID)
		{
		}

		// Token: 0x06005B27 RID: 23335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005444")]
		[Address(RVA = "0x1016637AC", Offset = "0x16637AC", VA = "0x1016637AC", Slot = "6")]
		public override void PlayBGM(string m_BGMCueName, float m_FadeInSec)
		{
		}

		// Token: 0x06005B28 RID: 23336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005445")]
		[Address(RVA = "0x101663B34", Offset = "0x1663B34", VA = "0x101663B34", Slot = "7")]
		public override void PlayBGM(string m_BGMCueName)
		{
		}

		// Token: 0x06005B29 RID: 23337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005446")]
		[Address(RVA = "0x101663EA0", Offset = "0x1663EA0", VA = "0x101663EA0", Slot = "8")]
		public override void StopBGM(float m_FadeOutSec)
		{
		}

		// Token: 0x06005B2A RID: 23338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005447")]
		[Address(RVA = "0x101663F18", Offset = "0x1663F18", VA = "0x101663F18", Slot = "9")]
		public override void StopBGM()
		{
		}

		// Token: 0x06005B2B RID: 23339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005448")]
		[Address(RVA = "0x101663F90", Offset = "0x1663F90", VA = "0x101663F90", Slot = "10")]
		public override void PlayVOICE(string m_ADVCharaID, uint m_CueID)
		{
		}

		// Token: 0x06005B2C RID: 23340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005449")]
		[Address(RVA = "0x101664008", Offset = "0x1664008", VA = "0x101664008", Slot = "11")]
		public override void PlayVOICECueName(string m_ADVCharaID, string m_CueName)
		{
		}

		// Token: 0x06005B2D RID: 23341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544A")]
		[Address(RVA = "0x1016640EC", Offset = "0x16640EC", VA = "0x1016640EC", Slot = "12")]
		public override void PlayFULLVOICE(string m_CueName)
		{
		}

		// Token: 0x06005B2E RID: 23342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544B")]
		[Address(RVA = "0x101664128", Offset = "0x1664128", VA = "0x101664128", Slot = "13")]
		public override void StopVOICE()
		{
		}

		// Token: 0x06005B2F RID: 23343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544C")]
		[Address(RVA = "0x101664158", Offset = "0x1664158", VA = "0x101664158", Slot = "14")]
		public override void WaitVOICE()
		{
		}

		// Token: 0x06005B30 RID: 23344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544D")]
		[Address(RVA = "0x1016641B0", Offset = "0x16641B0", VA = "0x1016641B0", Slot = "15")]
		public override void PlaySE(string m_CueID)
		{
		}

		// Token: 0x06005B31 RID: 23345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544E")]
		[Address(RVA = "0x1016641F0", Offset = "0x16641F0", VA = "0x1016641F0", Slot = "16")]
		public override void PlaySE(string m_SeCueName, float m_FadeInSec, float m_FadeOutSec)
		{
		}

		// Token: 0x06005B32 RID: 23346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600544F")]
		[Address(RVA = "0x101664240", Offset = "0x1664240", VA = "0x101664240", Slot = "17")]
		public override void StopSE()
		{
		}

		// Token: 0x06005B33 RID: 23347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005450")]
		[Address(RVA = "0x101664270", Offset = "0x1664270", VA = "0x101664270", Slot = "18")]
		public override void WaitSE()
		{
		}

		// Token: 0x06005B34 RID: 23348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005451")]
		[Address(RVA = "0x1016642C8", Offset = "0x16642C8", VA = "0x1016642C8", Slot = "19")]
		public override void FadeIn(string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x06005B35 RID: 23349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005452")]
		[Address(RVA = "0x1016643A8", Offset = "0x16643A8", VA = "0x1016643A8", Slot = "20")]
		public override void FadeOut(string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x06005B36 RID: 23350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005453")]
		[Address(RVA = "0x1016644B0", Offset = "0x16644B0", VA = "0x1016644B0", Slot = "21")]
		public override void FillScreen(string m_RGBA)
		{
		}

		// Token: 0x06005B37 RID: 23351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005454")]
		[Address(RVA = "0x10166452C", Offset = "0x166452C", VA = "0x10166452C", Slot = "22")]
		public override void Fade(string m_RGBAStart, string m_RGBAEnd, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B38 RID: 23352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005455")]
		[Address(RVA = "0x101664600", Offset = "0x1664600", VA = "0x101664600", Slot = "23")]
		public override void WaitFade()
		{
		}

		// Token: 0x06005B39 RID: 23353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005456")]
		[Address(RVA = "0x101664658", Offset = "0x1664658", VA = "0x101664658", Slot = "24")]
		public override void BGVisible(uint m_ID, string m_FileNameWithoutExt)
		{
		}

		// Token: 0x06005B3A RID: 23354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005457")]
		[Address(RVA = "0x10166469C", Offset = "0x166469C", VA = "0x10166469C", Slot = "25")]
		public override void BGScroll(uint m_ID, float m_X, float m_Y, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B3B RID: 23355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005458")]
		[Address(RVA = "0x101664740", Offset = "0x1664740", VA = "0x101664740", Slot = "26")]
		public override void WaitBGScroll(uint m_ID)
		{
		}

		// Token: 0x06005B3C RID: 23356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005459")]
		[Address(RVA = "0x1016647AC", Offset = "0x16647AC", VA = "0x1016647AC", Slot = "27")]
		public override void WaitBGScroll()
		{
		}

		// Token: 0x06005B3D RID: 23357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545A")]
		[Address(RVA = "0x101664814", Offset = "0x1664814", VA = "0x101664814", Slot = "28")]
		public override void BGScrollReset(uint m_ID, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B3E RID: 23358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545B")]
		[Address(RVA = "0x1016648A8", Offset = "0x16648A8", VA = "0x1016648A8", Slot = "29")]
		public override void BGColor(uint m_ID, string m_StartColor, string m_EndColor, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B3F RID: 23359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545C")]
		[Address(RVA = "0x101664998", Offset = "0x1664998", VA = "0x101664998", Slot = "30")]
		public override void BGScale(uint m_ID, float m_Scale, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B40 RID: 23360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545D")]
		[Address(RVA = "0x101664A08", Offset = "0x1664A08", VA = "0x101664A08", Slot = "31")]
		public override void BGPointScale(uint m_ID, float m_Scale, float m_Sec, float m_X, float m_Y, int m_CurveType)
		{
		}

		// Token: 0x06005B41 RID: 23361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545E")]
		[Address(RVA = "0x101664A90", Offset = "0x1664A90", VA = "0x101664A90", Slot = "32")]
		public override void WaitBGScale(uint m_ID)
		{
		}

		// Token: 0x06005B42 RID: 23362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600545F")]
		[Address(RVA = "0x101664AFC", Offset = "0x1664AFC", VA = "0x101664AFC", Slot = "33")]
		public override void WaitBGScale()
		{
		}

		// Token: 0x06005B43 RID: 23363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005460")]
		[Address(RVA = "0x101664B64", Offset = "0x1664B64", VA = "0x101664B64", Slot = "34")]
		public override void SetBGAnchor(uint m_ID, int m_Anchor)
		{
		}

		// Token: 0x06005B44 RID: 23364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005461")]
		[Address(RVA = "0x101664CCC", Offset = "0x1664CCC", VA = "0x101664CCC", Slot = "35")]
		public override void SetBGPivot(uint m_ID, float m_X, float m_Y)
		{
		}

		// Token: 0x06005B45 RID: 23365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005462")]
		[Address(RVA = "0x101664F58", Offset = "0x1664F58", VA = "0x101664F58", Slot = "36")]
		public override void StopShake()
		{
		}

		// Token: 0x06005B46 RID: 23366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005463")]
		[Address(RVA = "0x101664F84", Offset = "0x1664F84", VA = "0x101664F84", Slot = "37")]
		public override void Shake(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x06005B47 RID: 23367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005464")]
		[Address(RVA = "0x101664FC8", Offset = "0x1664FC8", VA = "0x101664FC8", Slot = "38")]
		public override void ShakeChara(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x06005B48 RID: 23368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005465")]
		[Address(RVA = "0x101665010", Offset = "0x1665010", VA = "0x101665010", Slot = "39")]
		public override void ShakeBG(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x06005B49 RID: 23369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005466")]
		[Address(RVA = "0x101665058", Offset = "0x1665058", VA = "0x101665058", Slot = "40")]
		public override void ShakeChara(int m_ShakeType, float m_Time, string m_ADVCharaID)
		{
		}

		// Token: 0x06005B4A RID: 23370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005467")]
		[Address(RVA = "0x101665260", Offset = "0x1665260", VA = "0x101665260", Slot = "41")]
		public override void CharaShot(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B4B RID: 23371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005468")]
		[Address(RVA = "0x101665840", Offset = "0x1665840", VA = "0x101665840", Slot = "42")]
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2)
		{
		}

		// Token: 0x06005B4C RID: 23372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005469")]
		[Address(RVA = "0x101665970", Offset = "0x1665970", VA = "0x101665970", Slot = "43")]
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3)
		{
		}

		// Token: 0x06005B4D RID: 23373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546A")]
		[Address(RVA = "0x101665AF4", Offset = "0x1665AF4", VA = "0x101665AF4", Slot = "44")]
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4)
		{
		}

		// Token: 0x06005B4E RID: 23374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546B")]
		[Address(RVA = "0x101665CC4", Offset = "0x1665CC4", VA = "0x101665CC4", Slot = "45")]
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
		}

		// Token: 0x06005B4F RID: 23375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546C")]
		[Address(RVA = "0x101665EE8", Offset = "0x1665EE8", VA = "0x101665EE8", Slot = "46")]
		public override void CharaShotFade(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06005B50 RID: 23376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546D")]
		[Address(RVA = "0x101665FD8", Offset = "0x1665FD8", VA = "0x101665FD8", Slot = "47")]
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, float m_Sec)
		{
		}

		// Token: 0x06005B51 RID: 23377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546E")]
		[Address(RVA = "0x101666114", Offset = "0x1666114", VA = "0x101666114", Slot = "48")]
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, float m_Sec)
		{
		}

		// Token: 0x06005B52 RID: 23378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600546F")]
		[Address(RVA = "0x1016662A4", Offset = "0x16662A4", VA = "0x1016662A4", Slot = "49")]
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, float m_Sec)
		{
		}

		// Token: 0x06005B53 RID: 23379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005470")]
		[Address(RVA = "0x101666480", Offset = "0x1666480", VA = "0x101666480", Slot = "50")]
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5, float m_Sec)
		{
		}

		// Token: 0x06005B54 RID: 23380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005471")]
		[Address(RVA = "0x1016666B0", Offset = "0x16666B0", VA = "0x1016666B0", Slot = "51")]
		public override void SetCharaShotPosition(int m_ADVCharaPos)
		{
		}

		// Token: 0x06005B55 RID: 23381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005472")]
		[Address(RVA = "0x10166675C", Offset = "0x166675C", VA = "0x10166675C", Slot = "52")]
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2)
		{
		}

		// Token: 0x06005B56 RID: 23382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005473")]
		[Address(RVA = "0x10166682C", Offset = "0x166682C", VA = "0x10166682C", Slot = "53")]
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3)
		{
		}

		// Token: 0x06005B57 RID: 23383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005474")]
		[Address(RVA = "0x101666928", Offset = "0x1666928", VA = "0x101666928", Slot = "54")]
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4)
		{
		}

		// Token: 0x06005B58 RID: 23384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005475")]
		[Address(RVA = "0x101666A48", Offset = "0x1666A48", VA = "0x101666A48", Slot = "55")]
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4, int m_ADVCharaPos5)
		{
		}

		// Token: 0x06005B59 RID: 23385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005476")]
		[Address(RVA = "0x101665344", Offset = "0x1665344", VA = "0x101665344")]
		protected void CharaShotProcess(ADVScriptCommandInherited.CharaIDs charaId, float m_FadeSec = 0f)
		{
		}

		// Token: 0x06005B5A RID: 23386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005477")]
		[Address(RVA = "0x101666B94", Offset = "0x1666B94", VA = "0x101666B94", Slot = "56")]
		public override void CharaOutAll()
		{
		}

		// Token: 0x06005B5B RID: 23387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005478")]
		[Address(RVA = "0x101666C20", Offset = "0x1666C20", VA = "0x101666C20", Slot = "57")]
		public override void CharaOutAllFade(float m_Sec)
		{
		}

		// Token: 0x06005B5C RID: 23388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005479")]
		[Address(RVA = "0x101666CBC", Offset = "0x1666CBC", VA = "0x101666CBC", Slot = "58")]
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition)
		{
		}

		// Token: 0x06005B5D RID: 23389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547A")]
		[Address(RVA = "0x101666D3C", Offset = "0x1666D3C", VA = "0x101666D3C", Slot = "59")]
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B5E RID: 23390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547B")]
		[Address(RVA = "0x101666D50", Offset = "0x1666D50", VA = "0x101666D50", Slot = "60")]
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x06005B5F RID: 23391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547C")]
		[Address(RVA = "0x101666E3C", Offset = "0x1666E3C", VA = "0x101666E3C", Slot = "61")]
		public override void CharaOut(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B60 RID: 23392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547D")]
		[Address(RVA = "0x101666EDC", Offset = "0x1666EDC", VA = "0x101666EDC", Slot = "62")]
		public override void CharaOut(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B61 RID: 23393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547E")]
		[Address(RVA = "0x101666FD4", Offset = "0x1666FD4", VA = "0x101666FD4", Slot = "63")]
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, float m_Sec)
		{
		}

		// Token: 0x06005B62 RID: 23394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600547F")]
		[Address(RVA = "0x101667078", Offset = "0x1667078", VA = "0x101667078", Slot = "64")]
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B63 RID: 23395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005480")]
		[Address(RVA = "0x10166708C", Offset = "0x166708C", VA = "0x10166708C", Slot = "65")]
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x06005B64 RID: 23396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005481")]
		[Address(RVA = "0x1016671A0", Offset = "0x16671A0", VA = "0x1016671A0", Slot = "66")]
		public override void CharaOutFade(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06005B65 RID: 23397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005482")]
		[Address(RVA = "0x10166724C", Offset = "0x166724C", VA = "0x10166724C", Slot = "67")]
		public override void CharaOutFade(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B66 RID: 23398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005483")]
		[Address(RVA = "0x10166733C", Offset = "0x166733C", VA = "0x10166733C", Slot = "68")]
		public override void SetTarget(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
		}

		// Token: 0x06005B67 RID: 23399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005484")]
		[Address(RVA = "0x10166753C", Offset = "0x166753C", VA = "0x10166753C")]
		protected void SetTargetProcess(params string[] advCharaIds)
		{
		}

		// Token: 0x06005B68 RID: 23400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005485")]
		[Address(RVA = "0x10166769C", Offset = "0x166769C", VA = "0x10166769C", Slot = "69")]
		public override void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x06005B69 RID: 23401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005486")]
		[Address(RVA = "0x101667EBC", Offset = "0x1667EBC", VA = "0x101667EBC", Slot = "70")]
		public override void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B6A RID: 23402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005487")]
		[Address(RVA = "0x101667ED0", Offset = "0x1667ED0", VA = "0x101667ED0", Slot = "71")]
		public override void CharaOutTarget(int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B6B RID: 23403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005488")]
		[Address(RVA = "0x1016685E0", Offset = "0x16685E0", VA = "0x1016685E0", Slot = "73")]
		public override void CharaAlignment(float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B6C RID: 23404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005489")]
		[Address(RVA = "0x101668628", Offset = "0x1668628", VA = "0x101668628", Slot = "74")]
		public override void CharaSwap(string m_ADVCharaID1, string m_ADVCharaID2, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B6D RID: 23405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548A")]
		[Address(RVA = "0x1016687E4", Offset = "0x16687E4", VA = "0x1016687E4", Slot = "72")]
		public override void WaitCharaFade()
		{
		}

		// Token: 0x06005B6E RID: 23406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548B")]
		[Address(RVA = "0x10166883C", Offset = "0x166883C", VA = "0x10166883C", Slot = "75")]
		public override void CharaPosition(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Yoffset)
		{
		}

		// Token: 0x06005B6F RID: 23407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548C")]
		[Address(RVA = "0x1016689E8", Offset = "0x16689E8", VA = "0x1016689E8", Slot = "76")]
		public override void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_XOffset, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B70 RID: 23408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548D")]
		[Address(RVA = "0x101668BAC", Offset = "0x1668BAC", VA = "0x101668BAC", Slot = "77")]
		public override void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_XOffset, float m_YOffset, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B71 RID: 23409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548E")]
		[Address(RVA = "0x101668D74", Offset = "0x1668D74", VA = "0x101668D74", Slot = "78")]
		public override void WaitCharaMove()
		{
		}

		// Token: 0x06005B72 RID: 23410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600548F")]
		[Address(RVA = "0x101668DCC", Offset = "0x1668DCC", VA = "0x101668DCC", Slot = "79")]
		public override void CharaRotate(string m_ADVCharaID, float m_PivotX, float m_PivotY, float m_Angle, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B73 RID: 23411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005490")]
		[Address(RVA = "0x101668EDC", Offset = "0x1668EDC", VA = "0x101668EDC", Slot = "80")]
		public override void CharaRotate(string m_ADVCharaID, int m_CharaAnchor, float m_Angle, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B74 RID: 23412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005491")]
		[Address(RVA = "0x101668FDC", Offset = "0x1668FDC", VA = "0x101668FDC", Slot = "81")]
		public override void CharaRotate(string m_ADVCharaID, int m_CharaAnchor, float m_Angle, float m_Sec)
		{
		}

		// Token: 0x06005B75 RID: 23413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005492")]
		[Address(RVA = "0x101668FF0", Offset = "0x1668FF0", VA = "0x101668FF0", Slot = "82")]
		public override void CharaRotateReset(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06005B76 RID: 23414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005493")]
		[Address(RVA = "0x10166911C", Offset = "0x166911C", VA = "0x10166911C", Slot = "83")]
		public override void WaitCharaRotate(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B77 RID: 23415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005494")]
		[Address(RVA = "0x101669178", Offset = "0x1669178", VA = "0x101669178", Slot = "84")]
		public override void WaitCharaRotate()
		{
		}

		// Token: 0x06005B78 RID: 23416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005495")]
		[Address(RVA = "0x1016691D0", Offset = "0x16691D0", VA = "0x1016691D0", Slot = "85")]
		public override void CharaMot(string m_ADVCharaID, string m_MotID)
		{
		}

		// Token: 0x06005B79 RID: 23417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005496")]
		[Address(RVA = "0x101669230", Offset = "0x1669230", VA = "0x101669230", Slot = "86")]
		public override void WaitMotion()
		{
		}

		// Token: 0x06005B7A RID: 23418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005497")]
		[Address(RVA = "0x101669288", Offset = "0x1669288", VA = "0x101669288", Slot = "87")]
		public override void CharaFace(string m_ADVCharaID, int m_FaceID)
		{
		}

		// Token: 0x06005B7B RID: 23419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005498")]
		[Address(RVA = "0x1016692E8", Offset = "0x16692E8", VA = "0x1016692E8", Slot = "88")]
		public override void CharaPose(string m_ADVCharaID, string m_PoseName)
		{
		}

		// Token: 0x06005B7C RID: 23420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005499")]
		[Address(RVA = "0x101669348", Offset = "0x1669348", VA = "0x101669348", Slot = "89")]
		public override void CharaEmotion(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06005B7D RID: 23421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549A")]
		[Address(RVA = "0x101669394", Offset = "0x1669394", VA = "0x101669394", Slot = "90")]
		public override void CharaEmotion(string m_ADVCharaID, string m_EmotionID, int m_Position)
		{
		}

		// Token: 0x06005B7E RID: 23422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549B")]
		[Address(RVA = "0x1016693E4", Offset = "0x16693E4", VA = "0x1016693E4", Slot = "91")]
		public override void EmotionEnd(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06005B7F RID: 23423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549C")]
		[Address(RVA = "0x101669434", Offset = "0x1669434", VA = "0x101669434", Slot = "92")]
		public override void WaitEmotion(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06005B80 RID: 23424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549D")]
		[Address(RVA = "0x1016694A4", Offset = "0x16694A4", VA = "0x1016694A4", Slot = "93")]
		public override void EmotionOffsetRotateMode(uint m_Enable)
		{
		}

		// Token: 0x06005B81 RID: 23425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549E")]
		[Address(RVA = "0x1016694DC", Offset = "0x16694DC", VA = "0x1016694DC", Slot = "94")]
		public override void CharaHighlight(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B82 RID: 23426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600549F")]
		[Address(RVA = "0x101669510", Offset = "0x1669510", VA = "0x101669510", Slot = "95")]
		public override void CharaHighlightAll()
		{
		}

		// Token: 0x06005B83 RID: 23427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A0")]
		[Address(RVA = "0x10166953C", Offset = "0x166953C", VA = "0x10166953C", Slot = "96")]
		public override void CharaHighlightReset(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B84 RID: 23428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A1")]
		[Address(RVA = "0x101669570", Offset = "0x1669570", VA = "0x101669570", Slot = "97")]
		public override void CharaHighlightResetAll()
		{
		}

		// Token: 0x06005B85 RID: 23429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A2")]
		[Address(RVA = "0x10166959C", Offset = "0x166959C", VA = "0x10166959C", Slot = "98")]
		public override void CharaShading(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B86 RID: 23430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A3")]
		[Address(RVA = "0x1016695D0", Offset = "0x16695D0", VA = "0x1016695D0", Slot = "99")]
		public override void CharaShadingAll()
		{
		}

		// Token: 0x06005B87 RID: 23431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A4")]
		[Address(RVA = "0x1016695FC", Offset = "0x16695FC", VA = "0x1016695FC", Slot = "100")]
		public override void CharaHighlightTalker(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B88 RID: 23432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A5")]
		[Address(RVA = "0x101669630", Offset = "0x1669630", VA = "0x101669630", Slot = "101")]
		public override void CharaPriorityTop(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B89 RID: 23433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A6")]
		[Address(RVA = "0x101669668", Offset = "0x1669668", VA = "0x101669668", Slot = "102")]
		public override void CharaPriorityTopSet(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B8A RID: 23434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A7")]
		[Address(RVA = "0x1016696A0", Offset = "0x16696A0", VA = "0x1016696A0", Slot = "103")]
		public override void CharaPriorityBottom(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B8B RID: 23435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A8")]
		[Address(RVA = "0x1016696D8", Offset = "0x16696D8", VA = "0x1016696D8", Slot = "104")]
		public override void CharaPriorityBottomSet(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B8C RID: 23436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054A9")]
		[Address(RVA = "0x101669710", Offset = "0x1669710", VA = "0x101669710", Slot = "105")]
		public override void CharaPriorityReset(string m_ADVCharaID)
		{
		}

		// Token: 0x06005B8D RID: 23437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AA")]
		[Address(RVA = "0x101669748", Offset = "0x1669748", VA = "0x101669748", Slot = "106")]
		public override void CharaTransparency(string m_ADVCharaID, uint m_Alpha, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B8E RID: 23438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AB")]
		[Address(RVA = "0x101669840", Offset = "0x1669840", VA = "0x101669840", Slot = "107")]
		public override void CharaTransparencyAll(uint m_Alpha, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06005B8F RID: 23439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AC")]
		[Address(RVA = "0x101669898", Offset = "0x1669898", VA = "0x101669898", Slot = "108")]
		public override void CharaTransparencyResetAll()
		{
		}

		// Token: 0x06005B90 RID: 23440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AD")]
		[Address(RVA = "0x1016698B4", Offset = "0x16698B4", VA = "0x1016698B4", Slot = "109")]
		public override void Effect(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B91 RID: 23441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AE")]
		[Address(RVA = "0x101669928", Offset = "0x1669928", VA = "0x101669928", Slot = "110")]
		public override void Effect(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x06005B92 RID: 23442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054AF")]
		[Address(RVA = "0x101669944", Offset = "0x1669944", VA = "0x101669944", Slot = "111")]
		public override void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B93 RID: 23443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B0")]
		[Address(RVA = "0x1016699C0", Offset = "0x16699C0", VA = "0x1016699C0", Slot = "112")]
		public override void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005B94 RID: 23444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B1")]
		[Address(RVA = "0x1016699DC", Offset = "0x16699DC", VA = "0x1016699DC", Slot = "113")]
		public override void EffectScreen(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B95 RID: 23445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B2")]
		[Address(RVA = "0x101669A50", Offset = "0x1669A50", VA = "0x101669A50", Slot = "114")]
		public override void EffectScreen(string m_effectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005B96 RID: 23446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B3")]
		[Address(RVA = "0x101669A6C", Offset = "0x1669A6C", VA = "0x101669A6C", Slot = "115")]
		public override void EffectLoop(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B97 RID: 23447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B4")]
		[Address(RVA = "0x101669AE0", Offset = "0x1669AE0", VA = "0x101669AE0", Slot = "116")]
		public override void EffectLoop(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x06005B98 RID: 23448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B5")]
		[Address(RVA = "0x101669AFC", Offset = "0x1669AFC", VA = "0x101669AFC", Slot = "117")]
		public override void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B99 RID: 23449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B6")]
		[Address(RVA = "0x101669B78", Offset = "0x1669B78", VA = "0x101669B78", Slot = "118")]
		public override void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005B9A RID: 23450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B7")]
		[Address(RVA = "0x101669B94", Offset = "0x1669B94", VA = "0x101669B94", Slot = "119")]
		public override void EffectScreenLoop(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005B9B RID: 23451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B8")]
		[Address(RVA = "0x101669C08", Offset = "0x1669C08", VA = "0x101669C08", Slot = "120")]
		public override void EffectScreenLoop(string m_effectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005B9C RID: 23452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054B9")]
		[Address(RVA = "0x101669C24", Offset = "0x1669C24", VA = "0x101669C24", Slot = "121")]
		public override void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y)
		{
		}

		// Token: 0x06005B9D RID: 23453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BA")]
		[Address(RVA = "0x101669C90", Offset = "0x1669C90", VA = "0x101669C90", Slot = "122")]
		public override void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition)
		{
		}

		// Token: 0x06005B9E RID: 23454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BB")]
		[Address(RVA = "0x101669CA8", Offset = "0x1669CA8", VA = "0x101669CA8", Slot = "123")]
		public override void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06005B9F RID: 23455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BC")]
		[Address(RVA = "0x101669D24", Offset = "0x1669D24", VA = "0x101669D24", Slot = "124")]
		public override void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005BA0 RID: 23456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BD")]
		[Address(RVA = "0x101669D3C", Offset = "0x1669D3C", VA = "0x101669D3C", Slot = "125")]
		public override void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BA1 RID: 23457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BE")]
		[Address(RVA = "0x101669DAC", Offset = "0x1669DAC", VA = "0x101669DAC", Slot = "126")]
		public override void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005BA2 RID: 23458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054BF")]
		[Address(RVA = "0x101669DC4", Offset = "0x1669DC4", VA = "0x101669DC4", Slot = "140")]
		public override void Effect_CharaBehind(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BA3 RID: 23459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C0")]
		[Address(RVA = "0x101669E38", Offset = "0x1669E38", VA = "0x101669E38", Slot = "141")]
		public override void Effect_CharaBehind(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x06005BA4 RID: 23460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C1")]
		[Address(RVA = "0x101669E54", Offset = "0x1669E54", VA = "0x101669E54", Slot = "142")]
		public override void EffectChara_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BA5 RID: 23461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C2")]
		[Address(RVA = "0x101669ED0", Offset = "0x1669ED0", VA = "0x101669ED0", Slot = "143")]
		public override void EffectChara_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005BA6 RID: 23462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C3")]
		[Address(RVA = "0x101669EEC", Offset = "0x1669EEC", VA = "0x101669EEC", Slot = "144")]
		public override void EffectScreen_CharaBehind(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BA7 RID: 23463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C4")]
		[Address(RVA = "0x101669F60", Offset = "0x1669F60", VA = "0x101669F60", Slot = "145")]
		public override void EffectScreen_CharaBehind(string m_effectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005BA8 RID: 23464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C5")]
		[Address(RVA = "0x101669F7C", Offset = "0x1669F7C", VA = "0x101669F7C", Slot = "146")]
		public override void EffectLoop_CharaBehind(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BA9 RID: 23465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C6")]
		[Address(RVA = "0x101669FF0", Offset = "0x1669FF0", VA = "0x101669FF0", Slot = "147")]
		public override void EffectLoop_CharaBehind(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x06005BAA RID: 23466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C7")]
		[Address(RVA = "0x10166A00C", Offset = "0x166A00C", VA = "0x10166A00C", Slot = "148")]
		public override void EffectCharaLoop_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BAB RID: 23467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C8")]
		[Address(RVA = "0x10166A088", Offset = "0x166A088", VA = "0x10166A088", Slot = "149")]
		public override void EffectCharaLoop_CharaBehind(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005BAC RID: 23468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054C9")]
		[Address(RVA = "0x10166A0A4", Offset = "0x166A0A4", VA = "0x10166A0A4", Slot = "150")]
		public override void EffectScreenLoop_CharaBehind(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BAD RID: 23469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CA")]
		[Address(RVA = "0x10166A118", Offset = "0x166A118", VA = "0x10166A118", Slot = "151")]
		public override void EffectScreenLoop_CharaBehind(string m_effectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005BAE RID: 23470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CB")]
		[Address(RVA = "0x10166A134", Offset = "0x166A134", VA = "0x10166A134", Slot = "152")]
		public override void EffectLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BAF RID: 23471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CC")]
		[Address(RVA = "0x10166A1A0", Offset = "0x166A1A0", VA = "0x10166A1A0", Slot = "153")]
		public override void EffectLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_StandPosition)
		{
		}

		// Token: 0x06005BB0 RID: 23472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CD")]
		[Address(RVA = "0x10166A1B8", Offset = "0x166A1B8", VA = "0x10166A1B8", Slot = "154")]
		public override void EffectCharaLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BB1 RID: 23473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CE")]
		[Address(RVA = "0x10166A234", Offset = "0x166A234", VA = "0x10166A234", Slot = "155")]
		public override void EffectCharaLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005BB2 RID: 23474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054CF")]
		[Address(RVA = "0x10166A24C", Offset = "0x166A24C", VA = "0x10166A24C", Slot = "156")]
		public override void EffectScreenLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BB3 RID: 23475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D0")]
		[Address(RVA = "0x10166A2BC", Offset = "0x166A2BC", VA = "0x10166A2BC", Slot = "157")]
		public override void EffectScreenLoopTarget_CharaBehind(string m_effectID, string m_afterEffectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06005BB4 RID: 23476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D1")]
		[Address(RVA = "0x10166A2D4", Offset = "0x166A2D4", VA = "0x10166A2D4", Slot = "127")]
		public override void EffectLoopTargetEnd(string m_effectID)
		{
		}

		// Token: 0x06005BB5 RID: 23477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D2")]
		[Address(RVA = "0x10166A318", Offset = "0x166A318", VA = "0x10166A318", Slot = "128")]
		public override void EffectEnd(string m_effectID)
		{
		}

		// Token: 0x06005BB6 RID: 23478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D3")]
		[Address(RVA = "0x10166A35C", Offset = "0x166A35C", VA = "0x10166A35C", Slot = "129")]
		public override void WaitEffect(string m_effectID)
		{
		}

		// Token: 0x06005BB7 RID: 23479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D4")]
		[Address(RVA = "0x10166A3B8", Offset = "0x166A3B8", VA = "0x10166A3B8", Slot = "130")]
		public override void EffectMuteSE()
		{
		}

		// Token: 0x06005BB8 RID: 23480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D5")]
		[Address(RVA = "0x10166A3F4", Offset = "0x166A3F4", VA = "0x10166A3F4", Slot = "131")]
		public override void EffectMuteSEReset()
		{
		}

		// Token: 0x06005BB9 RID: 23481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D6")]
		[Address(RVA = "0x10166A434", Offset = "0x166A434", VA = "0x10166A434", Slot = "132")]
		public override void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID, sbyte m_IgnoreParticle)
		{
		}

		// Token: 0x06005BBA RID: 23482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D7")]
		[Address(RVA = "0x10166A4A8", Offset = "0x166A4A8", VA = "0x10166A4A8", Slot = "133")]
		public override void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID)
		{
		}

		// Token: 0x06005BBB RID: 23483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D8")]
		[Address(RVA = "0x10166A4BC", Offset = "0x166A4BC", VA = "0x10166A4BC", Slot = "134")]
		public override void EffectLoopWithPreset(int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BBC RID: 23484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054D9")]
		[Address(RVA = "0x10166A51C", Offset = "0x166A51C", VA = "0x10166A51C", Slot = "135")]
		public override void EffectLoopWithPreset(int m_StandPosition)
		{
		}

		// Token: 0x06005BBD RID: 23485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DA")]
		[Address(RVA = "0x10166A538", Offset = "0x166A538", VA = "0x10166A538", Slot = "136")]
		public override void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BBE RID: 23486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DB")]
		[Address(RVA = "0x10166A5A8", Offset = "0x166A5A8", VA = "0x10166A5A8", Slot = "137")]
		public override void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06005BBF RID: 23487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DC")]
		[Address(RVA = "0x10166A5B8", Offset = "0x166A5B8", VA = "0x10166A5B8", Slot = "138")]
		public override void EffectScreenLoopWithPreset(int m_Anchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06005BC0 RID: 23488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DD")]
		[Address(RVA = "0x10166A618", Offset = "0x166A618", VA = "0x10166A618", Slot = "139")]
		public override void EffectScreenLoopWithPreset(int m_Anchor)
		{
		}

		// Token: 0x06005BC1 RID: 23489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DE")]
		[Address(RVA = "0x10166A634", Offset = "0x166A634", VA = "0x10166A634", Slot = "158")]
		public override void SpriteSet(uint m_ID, string m_FileNameWithoutExt)
		{
		}

		// Token: 0x06005BC2 RID: 23490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054DF")]
		[Address(RVA = "0x10166A6A0", Offset = "0x166A6A0", VA = "0x10166A6A0", Slot = "159")]
		public override void SpriteVisible(uint m_ID, uint m_VisibleFlg)
		{
		}

		// Token: 0x06005BC3 RID: 23491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E0")]
		[Address(RVA = "0x10166A6EC", Offset = "0x166A6EC", VA = "0x10166A6EC", Slot = "160")]
		public override void SpritePos(uint m_ID, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BC4 RID: 23492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E1")]
		[Address(RVA = "0x10166A748", Offset = "0x166A748", VA = "0x10166A748", Slot = "161")]
		public override void SpritePos(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
		}

		// Token: 0x06005BC5 RID: 23493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E2")]
		[Address(RVA = "0x10166A7B0", Offset = "0x166A7B0", VA = "0x10166A7B0", Slot = "162")]
		public override void SpriteScale(uint m_ID, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BC6 RID: 23494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E3")]
		[Address(RVA = "0x10166A80C", Offset = "0x166A80C", VA = "0x10166A80C", Slot = "163")]
		public override void SpriteScale(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
		}

		// Token: 0x06005BC7 RID: 23495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E4")]
		[Address(RVA = "0x10166A874", Offset = "0x166A874", VA = "0x10166A874", Slot = "164")]
		public override void SpriteColor(uint m_ID, string m_RGBA)
		{
		}

		// Token: 0x06005BC8 RID: 23496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E5")]
		[Address(RVA = "0x10166A8FC", Offset = "0x166A8FC", VA = "0x10166A8FC", Slot = "165")]
		public override void SpriteColor(uint m_ID, string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x06005BC9 RID: 23497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E6")]
		[Address(RVA = "0x10166A990", Offset = "0x166A990", VA = "0x10166A990", Slot = "166")]
		public override void SpriteFadeIn(uint m_ID, float m_Sec)
		{
		}

		// Token: 0x06005BCA RID: 23498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E7")]
		[Address(RVA = "0x10166A9D8", Offset = "0x166A9D8", VA = "0x10166A9D8", Slot = "167")]
		public override void SpriteFadeIn(uint m_ID, float m_Sec, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BCB RID: 23499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E8")]
		[Address(RVA = "0x10166AA5C", Offset = "0x166AA5C", VA = "0x10166AA5C", Slot = "168")]
		public override void SpriteFadeOut(uint m_ID, float m_Sec)
		{
		}

		// Token: 0x06005BCC RID: 23500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054E9")]
		[Address(RVA = "0x10166AAA4", Offset = "0x166AAA4", VA = "0x10166AAA4", Slot = "169")]
		public override void WaitSpriteFade()
		{
		}

		// Token: 0x06005BCD RID: 23501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054EA")]
		[Address(RVA = "0x10166AB0C", Offset = "0x166AB0C", VA = "0x10166AB0C", Slot = "170")]
		public override void WaitSpriteFade(uint m_ID)
		{
		}

		// Token: 0x06005BCE RID: 23502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054EB")]
		[Address(RVA = "0x10166AB78", Offset = "0x166AB78", VA = "0x10166AB78", Slot = "171")]
		public override void WaitSpritePos()
		{
		}

		// Token: 0x06005BCF RID: 23503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054EC")]
		[Address(RVA = "0x10166ABE0", Offset = "0x166ABE0", VA = "0x10166ABE0", Slot = "172")]
		public override void WaitSpritePos(uint m_ID)
		{
		}

		// Token: 0x06005BD0 RID: 23504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054ED")]
		[Address(RVA = "0x10166AC4C", Offset = "0x166AC4C", VA = "0x10166AC4C", Slot = "173")]
		public override void WaitSpriteScale()
		{
		}

		// Token: 0x06005BD1 RID: 23505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054EE")]
		[Address(RVA = "0x10166ACB4", Offset = "0x166ACB4", VA = "0x10166ACB4", Slot = "174")]
		public override void WaitSpriteScale(uint m_ID)
		{
		}

		// Token: 0x06005BD2 RID: 23506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054EF")]
		[Address(RVA = "0x10166AD20", Offset = "0x166AD20", VA = "0x10166AD20", Slot = "175")]
		public override void WaitSpriteColor()
		{
		}

		// Token: 0x06005BD3 RID: 23507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F0")]
		[Address(RVA = "0x10166AD88", Offset = "0x166AD88", VA = "0x10166AD88", Slot = "176")]
		public override void WaitSpriteColor(uint m_ID)
		{
		}

		// Token: 0x06005BD4 RID: 23508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F1")]
		[Address(RVA = "0x10166ADF4", Offset = "0x166ADF4", VA = "0x10166ADF4", Slot = "177")]
		public override void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos)
		{
		}

		// Token: 0x06005BD5 RID: 23509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F2")]
		[Address(RVA = "0x10166AED4", Offset = "0x166AED4", VA = "0x10166AED4", Slot = "178")]
		public override void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID)
		{
		}

		// Token: 0x06005BD6 RID: 23510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F3")]
		[Address(RVA = "0x10166AEE8", Offset = "0x166AEE8", VA = "0x10166AEE8", Slot = "179")]
		public override void CharaTalk(uint m_TextID, int m_FaceID)
		{
		}

		// Token: 0x06005BD7 RID: 23511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F4")]
		[Address(RVA = "0x10166AF90", Offset = "0x166AF90", VA = "0x10166AF90", Slot = "180")]
		public override void CharaTalk(uint m_TextID)
		{
		}

		// Token: 0x06005BD8 RID: 23512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F5")]
		[Address(RVA = "0x10166AFF0", Offset = "0x166AFF0", VA = "0x10166AFF0", Slot = "181")]
		public override void CharaTalkFullVoice(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos)
		{
		}

		// Token: 0x06005BD9 RID: 23513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F6")]
		[Address(RVA = "0x10166B0D0", Offset = "0x166B0D0", VA = "0x10166B0D0", Slot = "182")]
		public override void CharaTalkFullVoice(uint m_TextID, int m_FaceID, string m_EmotionID)
		{
		}

		// Token: 0x06005BDA RID: 23514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F7")]
		[Address(RVA = "0x10166B0E4", Offset = "0x166B0E4", VA = "0x10166B0E4", Slot = "183")]
		public override void CharaTalkFullVoice(uint m_TextID, int m_FaceID)
		{
		}

		// Token: 0x06005BDB RID: 23515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F8")]
		[Address(RVA = "0x10166B18C", Offset = "0x166B18C", VA = "0x10166B18C", Slot = "184")]
		public override void CharaTalkFullVoice(uint m_TextID)
		{
		}

		// Token: 0x06005BDC RID: 23516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054F9")]
		[Address(RVA = "0x10166B1EC", Offset = "0x166B1EC", VA = "0x10166B1EC", Slot = "185")]
		public override void CloseTalk()
		{
		}

		// Token: 0x06005BDD RID: 23517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FA")]
		[Address(RVA = "0x10166B218", Offset = "0x166B218", VA = "0x10166B218", Slot = "186")]
		public override void ClearNovelText()
		{
		}

		// Token: 0x06005BDE RID: 23518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FB")]
		[Address(RVA = "0x10166B244", Offset = "0x166B244", VA = "0x10166B244", Slot = "187")]
		public override void NovelAnchorSetting(int m_AnchorID, float m_X, float m_Y)
		{
		}

		// Token: 0x06005BDF RID: 23519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FC")]
		[Address(RVA = "0x10166B298", Offset = "0x166B298", VA = "0x10166B298", Slot = "188")]
		public override void NovelAnchorSettingDefault()
		{
		}

		// Token: 0x06005BE0 RID: 23520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FD")]
		[Address(RVA = "0x10166B2C4", Offset = "0x166B2C4", VA = "0x10166B2C4", Slot = "189")]
		public override void AddNovelText(uint m_TextID)
		{
		}

		// Token: 0x06005BE1 RID: 23521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FE")]
		[Address(RVA = "0x10166B320", Offset = "0x166B320", VA = "0x10166B320", Slot = "190")]
		public override void NovelInsertLine()
		{
		}

		// Token: 0x06005BE2 RID: 23522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60054FF")]
		[Address(RVA = "0x10166B34C", Offset = "0x166B34C", VA = "0x10166B34C", Slot = "191")]
		public override void SetCaptionDefault()
		{
		}

		// Token: 0x06005BE3 RID: 23523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005500")]
		[Address(RVA = "0x10166B388", Offset = "0x166B388", VA = "0x10166B388", Slot = "192")]
		public override void SetCaptionBGStart(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BE4 RID: 23524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005501")]
		[Address(RVA = "0x10166B45C", Offset = "0x166B45C", VA = "0x10166B45C", Slot = "193")]
		public override void SetCaptionBGIdle(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BE5 RID: 23525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005502")]
		[Address(RVA = "0x10166B530", Offset = "0x166B530", VA = "0x10166B530", Slot = "194")]
		public override void SetCaptionBGEnd(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BE6 RID: 23526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005503")]
		[Address(RVA = "0x10166B604", Offset = "0x166B604", VA = "0x10166B604", Slot = "195")]
		public override void SetCaptionBGEaseIn(float sec, float delay, int curveType)
		{
		}

		// Token: 0x06005BE7 RID: 23527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005504")]
		[Address(RVA = "0x10166B660", Offset = "0x166B660", VA = "0x10166B660", Slot = "196")]
		public override void SetCaptionBGEaseOut(float sec, float delay, int curveType)
		{
		}

		// Token: 0x06005BE8 RID: 23528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005505")]
		[Address(RVA = "0x10166B6BC", Offset = "0x166B6BC", VA = "0x10166B6BC", Slot = "197")]
		public override void SetCaptionTextStart(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BE9 RID: 23529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005506")]
		[Address(RVA = "0x10166B790", Offset = "0x166B790", VA = "0x10166B790", Slot = "198")]
		public override void SetCaptionTextIdle(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BEA RID: 23530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005507")]
		[Address(RVA = "0x10166B864", Offset = "0x166B864", VA = "0x10166B864", Slot = "199")]
		public override void SetCaptionTextEnd(float posX, float posY, float sclX, float sclY, string rgba)
		{
		}

		// Token: 0x06005BEB RID: 23531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005508")]
		[Address(RVA = "0x10166B938", Offset = "0x166B938", VA = "0x10166B938", Slot = "200")]
		public override void SetCaptionTextEaseIn(float sec, float delay, int curveType)
		{
		}

		// Token: 0x06005BEC RID: 23532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005509")]
		[Address(RVA = "0x10166B994", Offset = "0x166B994", VA = "0x10166B994", Slot = "201")]
		public override void SetCaptionTextEaseOut(float sec, float delay, int curveType)
		{
		}

		// Token: 0x06005BED RID: 23533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550A")]
		[Address(RVA = "0x10166B9F0", Offset = "0x166B9F0", VA = "0x10166B9F0", Slot = "202")]
		public override void SetCaptionBGSprite(string fileName, float width, float height)
		{
		}

		// Token: 0x06005BEE RID: 23534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550B")]
		[Address(RVA = "0x10166BA4C", Offset = "0x166BA4C", VA = "0x10166BA4C", Slot = "203")]
		public override void SetCaptionTextAlignment(int anchor)
		{
		}

		// Token: 0x06005BEF RID: 23535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550C")]
		[Address(RVA = "0x10166BA90", Offset = "0x166BA90", VA = "0x10166BA90", Slot = "204")]
		public override void PlayCaption(uint textID)
		{
		}

		// Token: 0x06005BF0 RID: 23536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550D")]
		[Address(RVA = "0x10166BAEC", Offset = "0x166BAEC", VA = "0x10166BAEC", Slot = "205")]
		public override void WarpPrepare(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06005BF1 RID: 23537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550E")]
		[Address(RVA = "0x10166BB74", Offset = "0x166BB74", VA = "0x10166BB74", Slot = "206")]
		public override void WarpPrepareWait()
		{
		}

		// Token: 0x06005BF2 RID: 23538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600550F")]
		[Address(RVA = "0x10166BBA0", Offset = "0x166BBA0", VA = "0x10166BBA0", Slot = "207")]
		public override void WarpStart(float m_Sec)
		{
		}

		// Token: 0x06005BF3 RID: 23539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005510")]
		[Address(RVA = "0x10166BBDC", Offset = "0x166BBDC", VA = "0x10166BBDC", Slot = "208")]
		public override void WarpWait()
		{
		}

		// Token: 0x06005BF4 RID: 23540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005511")]
		[Address(RVA = "0x10166BC08", Offset = "0x166BC08", VA = "0x10166BC08", Slot = "209")]
		public override void SetHDRFactor(string m_ADVCharaID, float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005BF5 RID: 23541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005512")]
		[Address(RVA = "0x10166BC80", Offset = "0x166BC80", VA = "0x10166BC80", Slot = "210")]
		public override void Enable_PP_Contrast(uint m_Enable)
		{
		}

		// Token: 0x06005BF6 RID: 23542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005513")]
		[Address(RVA = "0x10166BD54", Offset = "0x166BD54", VA = "0x10166BD54", Slot = "211")]
		public override void SetPP_Contrast_Level(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005BF7 RID: 23543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005514")]
		[Address(RVA = "0x10166BE18", Offset = "0x166BE18", VA = "0x10166BE18", Slot = "212")]
		public override void Enable_PP_Brightness(uint m_Enable)
		{
		}

		// Token: 0x06005BF8 RID: 23544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005515")]
		[Address(RVA = "0x10166BEEC", Offset = "0x166BEEC", VA = "0x10166BEEC", Slot = "213")]
		public override void SetPP_BrightnessLevel(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005BF9 RID: 23545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005516")]
		[Address(RVA = "0x10166BFB0", Offset = "0x166BFB0", VA = "0x10166BFB0", Slot = "214")]
		public override void Enable_PP_Chroma(uint m_Enable)
		{
		}

		// Token: 0x06005BFA RID: 23546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005517")]
		[Address(RVA = "0x10166C084", Offset = "0x166C084", VA = "0x10166C084", Slot = "215")]
		public override void SetPP_ChromaLevel(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005BFB RID: 23547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005518")]
		[Address(RVA = "0x10166C148", Offset = "0x166C148", VA = "0x10166C148", Slot = "216")]
		public override void Enable_PP_ColorBlend(uint m_Enable)
		{
		}

		// Token: 0x06005BFC RID: 23548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005519")]
		[Address(RVA = "0x10166C21C", Offset = "0x166C21C", VA = "0x10166C21C", Slot = "217")]
		public override void SetPP_ColorBlendColor(uint m_R, uint m_G, uint m_B)
		{
		}

		// Token: 0x06005BFD RID: 23549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551A")]
		[Address(RVA = "0x10166C330", Offset = "0x166C330", VA = "0x10166C330", Slot = "218")]
		public override void SetPP_ColorBlendLevel(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005BFE RID: 23550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551B")]
		[Address(RVA = "0x10166C3F4", Offset = "0x166C3F4", VA = "0x10166C3F4", Slot = "219")]
		public override void EnableDistorsion_BG(uint m_Enable)
		{
		}

		// Token: 0x06005BFF RID: 23551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551C")]
		[Address(RVA = "0x10166C42C", Offset = "0x166C42C", VA = "0x10166C42C", Slot = "220")]
		public override void SetDistorsionLevel_BG(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005C00 RID: 23552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551D")]
		[Address(RVA = "0x10166C488", Offset = "0x166C488", VA = "0x10166C488", Slot = "221")]
		public override void Distorsion_CharaClear()
		{
		}

		// Token: 0x06005C01 RID: 23553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551E")]
		[Address(RVA = "0x10166C4B4", Offset = "0x166C4B4", VA = "0x10166C4B4", Slot = "222")]
		public override void Distorsion_CharaAdd(string m_ADVCharaID)
		{
		}

		// Token: 0x06005C02 RID: 23554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600551F")]
		[Address(RVA = "0x10166C510", Offset = "0x166C510", VA = "0x10166C510", Slot = "223")]
		public override void Distorsion_CharaRemove(string m_ADVCharaID)
		{
		}

		// Token: 0x06005C03 RID: 23555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005520")]
		[Address(RVA = "0x10166C56C", Offset = "0x166C56C", VA = "0x10166C56C", Slot = "224")]
		public override void EnableDistorsion_Chara(uint m_Enable)
		{
		}

		// Token: 0x06005C04 RID: 23556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005521")]
		[Address(RVA = "0x10166C5A4", Offset = "0x166C5A4", VA = "0x10166C5A4", Slot = "225")]
		public override void SetDistorsionLevel_Chara(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005C05 RID: 23557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005522")]
		[Address(RVA = "0x10166C600", Offset = "0x166C600", VA = "0x10166C600", Slot = "226")]
		public override void SetPP_BloomThshold(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005C06 RID: 23558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005523")]
		[Address(RVA = "0x10166C644", Offset = "0x166C644", VA = "0x10166C644", Slot = "227")]
		public override void Enable_ShadeOff(uint m_Enable)
		{
		}

		// Token: 0x06005C07 RID: 23559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005524")]
		[Address(RVA = "0x10166C67C", Offset = "0x166C67C", VA = "0x10166C67C", Slot = "228")]
		public override void Set_ShadeOffLevel(float m_Value, float m_Sec)
		{
		}

		// Token: 0x06005C08 RID: 23560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005525")]
		[Address(RVA = "0x10166C6C0", Offset = "0x166C6C0", VA = "0x10166C6C0")]
		public ADVScriptCommandInherited()
		{
		}

		// Token: 0x04006DA6 RID: 28070
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004DC5")]
		private ADVPlayer m_Player;

		// Token: 0x04006DA7 RID: 28071
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004DC6")]
		private StandPicManager m_StandPicManager;

		// Token: 0x04006DA8 RID: 28072
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004DC7")]
		private ADVSpriteManager m_SpriteManager;

		// Token: 0x04006DA9 RID: 28073
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004DC8")]
		private ADVBackGroundManager m_BackGroundManager;

		// Token: 0x04006DAA RID: 28074
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004DC9")]
		private ADVSoundManager m_SoundManager;

		// Token: 0x04006DAB RID: 28075
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004DCA")]
		private string[] m_TargetADVCharaIDs;

		// Token: 0x04006DAC RID: 28076
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004DCB")]
		protected ADVScriptCommandInherited.CharaPositions positions;

		// Token: 0x020011B4 RID: 4532
		[Token(Token = "0x20012B5")]
		public class VariableArrayBase<vType>
		{
			// Token: 0x06005C09 RID: 23561 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006393")]
			[Address(RVA = "0x1016CBC54", Offset = "0x16CBC54", VA = "0x1016CBC54")]
			public VariableArrayBase(params vType[] in_params)
			{
			}

			// Token: 0x06005C0A RID: 23562 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006394")]
			[Address(RVA = "0x1016CBD64", Offset = "0x16CBD64", VA = "0x1016CBD64", Slot = "4")]
			protected virtual vType GetDefaultValue(int index)
			{
				return null;
			}

			// Token: 0x04006DAD RID: 28077
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400730F")]
			public vType[] value;
		}

		// Token: 0x020011B5 RID: 4533
		[Token(Token = "0x20012B6")]
		public class CharaIDs : ADVScriptCommandInherited.VariableArrayBase<string>
		{
			// Token: 0x06005C0B RID: 23563 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006395")]
			[Address(RVA = "0x10166C720", Offset = "0x166C720", VA = "0x10166C720")]
			public CharaIDs(params string[] in_params)
			{
			}

			// Token: 0x06005C0C RID: 23564 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006396")]
			[Address(RVA = "0x10166C780", Offset = "0x166C780", VA = "0x10166C780", Slot = "4")]
			protected override string GetDefaultValue(int index)
			{
				return null;
			}
		}

		// Token: 0x020011B6 RID: 4534
		[Token(Token = "0x20012B7")]
		public class CharaPositions : ADVScriptCommandInherited.VariableArrayBase<eADVStandPosition>
		{
			// Token: 0x06005C0D RID: 23565 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006397")]
			[Address(RVA = "0x10166C7C8", Offset = "0x166C7C8", VA = "0x10166C7C8")]
			public CharaPositions(params eADVStandPosition[] in_params)
			{
			}

			// Token: 0x06005C0E RID: 23566 RVA: 0x0001DC28 File Offset: 0x0001BE28
			[Token(Token = "0x6006398")]
			[Address(RVA = "0x10166C828", Offset = "0x166C828", VA = "0x10166C828", Slot = "4")]
			protected override eADVStandPosition GetDefaultValue(int index)
			{
				return eADVStandPosition.Left;
			}
		}
	}
}
