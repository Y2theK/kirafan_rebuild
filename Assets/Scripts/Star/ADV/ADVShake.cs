﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x020011B7 RID: 4535
	[Token(Token = "0x2000BBF")]
	[StructLayout(3)]
	public class ADVShake : MonoBehaviour
	{
		// Token: 0x06005C0F RID: 23567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005526")]
		[Address(RVA = "0x10166C838", Offset = "0x166C838", VA = "0x10166C838")]
		private void Awake()
		{
		}

		// Token: 0x06005C10 RID: 23568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005527")]
		[Address(RVA = "0x10166C894", Offset = "0x166C894", VA = "0x10166C894")]
		private void Update()
		{
		}

		// Token: 0x06005C11 RID: 23569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005528")]
		[Address(RVA = "0x10166CD30", Offset = "0x166CD30", VA = "0x10166CD30")]
		public void Shake(ADVPlayer player, bool isAllShake, bool isThisCharaShake, ADVShake.eShakeType xType, float xRadius, ADVShake.eShakeType yType, float yRadius, float cycle, float time)
		{
		}

		// Token: 0x06005C12 RID: 23570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005529")]
		[Address(RVA = "0x10166D1F8", Offset = "0x166D1F8", VA = "0x10166D1F8")]
		public void RestartShake()
		{
		}

		// Token: 0x06005C13 RID: 23571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600552A")]
		[Address(RVA = "0x10166CE9C", Offset = "0x166CE9C", VA = "0x10166CE9C")]
		private void MoveToNextPosition()
		{
		}

		// Token: 0x06005C14 RID: 23572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600552B")]
		[Address(RVA = "0x10166D1FC", Offset = "0x166D1FC", VA = "0x10166D1FC")]
		private void MoveToNextPosition(Vector2 pos)
		{
		}

		// Token: 0x06005C15 RID: 23573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600552C")]
		[Address(RVA = "0x10166D480", Offset = "0x166D480", VA = "0x10166D480")]
		private void OnComplete()
		{
		}

		// Token: 0x06005C16 RID: 23574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600552D")]
		[Address(RVA = "0x10166C910", Offset = "0x166C910", VA = "0x10166C910")]
		private void ReturnToOrig()
		{
		}

		// Token: 0x06005C17 RID: 23575 RVA: 0x0001DC40 File Offset: 0x0001BE40
		[Token(Token = "0x600552E")]
		[Address(RVA = "0x10166D60C", Offset = "0x166D60C", VA = "0x10166D60C")]
		public ADVShake.eShakeType GetShakeTypeX()
		{
			return ADVShake.eShakeType.None;
		}

		// Token: 0x06005C18 RID: 23576 RVA: 0x0001DC58 File Offset: 0x0001BE58
		[Token(Token = "0x600552F")]
		[Address(RVA = "0x10166D650", Offset = "0x166D650", VA = "0x10166D650")]
		public ADVShake.eShakeType GetShakeTypeY()
		{
			return ADVShake.eShakeType.None;
		}

		// Token: 0x06005C19 RID: 23577 RVA: 0x0001DC70 File Offset: 0x0001BE70
		[Token(Token = "0x6005530")]
		[Address(RVA = "0x10166D698", Offset = "0x166D698", VA = "0x10166D698")]
		public float GetRadiusX()
		{
			return 0f;
		}

		// Token: 0x06005C1A RID: 23578 RVA: 0x0001DC88 File Offset: 0x0001BE88
		[Token(Token = "0x6005531")]
		[Address(RVA = "0x10166D6DC", Offset = "0x166D6DC", VA = "0x10166D6DC")]
		public float GetRadiusY()
		{
			return 0f;
		}

		// Token: 0x06005C1B RID: 23579 RVA: 0x0001DCA0 File Offset: 0x0001BEA0
		[Token(Token = "0x6005532")]
		[Address(RVA = "0x10166D724", Offset = "0x166D724", VA = "0x10166D724")]
		public bool IsNeedMoveToNext()
		{
			return default(bool);
		}

		// Token: 0x06005C1C RID: 23580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005533")]
		[Address(RVA = "0x10166D72C", Offset = "0x166D72C", VA = "0x10166D72C")]
		public void CancelSyncShakeAll()
		{
		}

		// Token: 0x06005C1D RID: 23581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005534")]
		[Address(RVA = "0x10166D734", Offset = "0x166D734", VA = "0x10166D734")]
		public void StopShake()
		{
		}

		// Token: 0x06005C1E RID: 23582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005535")]
		[Address(RVA = "0x10166C83C", Offset = "0x166C83C", VA = "0x10166C83C")]
		public void UpdateTransform()
		{
		}

		// Token: 0x06005C1F RID: 23583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005536")]
		[Address(RVA = "0x10166D2A4", Offset = "0x166D2A4", VA = "0x10166D2A4")]
		private void SetupShake(ADVShake.eEasingType easingType, Action onComplete, float x, float y)
		{
		}

		// Token: 0x06005C20 RID: 23584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005537")]
		[Address(RVA = "0x10166C920", Offset = "0x166C920", VA = "0x10166C920")]
		private void UpdateShake()
		{
		}

		// Token: 0x06005C21 RID: 23585 RVA: 0x0001DCB8 File Offset: 0x0001BEB8
		[Token(Token = "0x6005538")]
		[Address(RVA = "0x10166DB1C", Offset = "0x166DB1C", VA = "0x10166DB1C")]
		private float linear(float start, float end, float value)
		{
			return 0f;
		}

		// Token: 0x06005C22 RID: 23586 RVA: 0x0001DCD0 File Offset: 0x0001BED0
		[Token(Token = "0x6005539")]
		[Address(RVA = "0x10166DBA4", Offset = "0x166DBA4", VA = "0x10166DBA4")]
		private float easeInOutCubic(float start, float end, float value)
		{
			return 0f;
		}

		// Token: 0x06005C23 RID: 23587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600553A")]
		[Address(RVA = "0x10166DBF0", Offset = "0x166DBF0", VA = "0x10166DBF0")]
		public ADVShake()
		{
		}

		// Token: 0x04006DAE RID: 28078
		[Token(Token = "0x4004DCC")]
		public const float RADIUS_S = 2f;

		// Token: 0x04006DAF RID: 28079
		[Token(Token = "0x4004DCD")]
		public const float RADIUS_M = 7f;

		// Token: 0x04006DB0 RID: 28080
		[Token(Token = "0x4004DCE")]
		public const float RADIUS_L = 20f;

		// Token: 0x04006DB1 RID: 28081
		[Token(Token = "0x4004DCF")]
		public const float RADIUS_RANDOM_S = 1f;

		// Token: 0x04006DB2 RID: 28082
		[Token(Token = "0x4004DD0")]
		public const float RADIUS_RANDOM_M = 7f;

		// Token: 0x04006DB3 RID: 28083
		[Token(Token = "0x4004DD1")]
		public const float RADIUS_RANDOM_L = 20f;

		// Token: 0x04006DB4 RID: 28084
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004DD2")]
		private ADVShake.eShakePhase m_ShakePhase;

		// Token: 0x04006DB5 RID: 28085
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004DD3")]
		private Vector3 m_OrigPos;

		// Token: 0x04006DB6 RID: 28086
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004DD4")]
		private bool m_IsShake;

		// Token: 0x04006DB7 RID: 28087
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004DD5")]
		private ADVShake.ShakeData[] m_ShakeData;

		// Token: 0x04006DB8 RID: 28088
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004DD6")]
		private float m_ShakeTimeCount;

		// Token: 0x04006DB9 RID: 28089
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004DD7")]
		private float m_ShakeMaxTime;

		// Token: 0x04006DBA RID: 28090
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004DD8")]
		private float m_Cycle;

		// Token: 0x04006DBB RID: 28091
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004DD9")]
		private int m_MoveCount;

		// Token: 0x04006DBC RID: 28092
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004DDA")]
		private GameObject m_GameObject;

		// Token: 0x04006DBD RID: 28093
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004DDB")]
		private Transform m_Transform;

		// Token: 0x04006DBE RID: 28094
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004DDC")]
		private ADVPlayer m_Player;

		// Token: 0x04006DBF RID: 28095
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004DDD")]
		private bool m_IsAllShake;

		// Token: 0x04006DC0 RID: 28096
		[Cpp2IlInjected.FieldOffset(Offset = "0x61")]
		[Token(Token = "0x4004DDE")]
		private bool m_IsThisCharaShake;

		// Token: 0x04006DC1 RID: 28097
		[Cpp2IlInjected.FieldOffset(Offset = "0x62")]
		[Token(Token = "0x4004DDF")]
		private bool m_NeedMoveToNext;

		// Token: 0x04006DC2 RID: 28098
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004DE0")]
		private float m_RunningTime;

		// Token: 0x04006DC3 RID: 28099
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004DE1")]
		private ADVShake.eEasingType m_EasingType;

		// Token: 0x04006DC4 RID: 28100
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004DE2")]
		private ADVShake.EasingFunction m_EasingFunction;

		// Token: 0x04006DC5 RID: 28101
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004DE3")]
		private Vector3[] m_Vector3s;

		// Token: 0x04006DC6 RID: 28102
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004DE4")]
		private Action m_OnComplete;

		// Token: 0x04006DC7 RID: 28103
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004DE5")]
		private int m_CallOnCompleteCount;

		// Token: 0x020011B8 RID: 4536
		[Token(Token = "0x20012B8")]
		private struct ShakeData
		{
			// Token: 0x04006DC8 RID: 28104
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4007310")]
			public ADVShake.eShakeType m_Type;

			// Token: 0x04006DC9 RID: 28105
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4007311")]
			public float m_Radius;
		}

		// Token: 0x020011B9 RID: 4537
		[Token(Token = "0x20012B9")]
		public enum eShakeType
		{
			// Token: 0x04006DCB RID: 28107
			[Token(Token = "0x4007313")]
			None,
			// Token: 0x04006DCC RID: 28108
			[Token(Token = "0x4007314")]
			Round,
			// Token: 0x04006DCD RID: 28109
			[Token(Token = "0x4007315")]
			Random
		}

		// Token: 0x020011BA RID: 4538
		[Token(Token = "0x20012BA")]
		private enum eShakePhase
		{
			// Token: 0x04006DCF RID: 28111
			[Token(Token = "0x4007317")]
			Wait,
			// Token: 0x04006DD0 RID: 28112
			[Token(Token = "0x4007318")]
			Trigger,
			// Token: 0x04006DD1 RID: 28113
			[Token(Token = "0x4007319")]
			Setup,
			// Token: 0x04006DD2 RID: 28114
			[Token(Token = "0x400731A")]
			Update
		}

		// Token: 0x020011BB RID: 4539
		[Token(Token = "0x20012BB")]
		private enum eEasingType
		{
			// Token: 0x04006DD4 RID: 28116
			[Token(Token = "0x400731C")]
			Linear,
			// Token: 0x04006DD5 RID: 28117
			[Token(Token = "0x400731D")]
			EaseInOutCubic
		}

		// Token: 0x020011BC RID: 4540
		// (Invoke) Token: 0x06005C25 RID: 23589
		[Token(Token = "0x20012BC")]
		private delegate float EasingFunction(float start, float end, float Value);
	}
}
