﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011BD RID: 4541
	[Token(Token = "0x2000BC0")]
	[StructLayout(3)]
	public class ADVSoundManager
	{
		// Token: 0x06005C28 RID: 23592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600553B")]
		[Address(RVA = "0x10166DD9C", Offset = "0x166DD9C", VA = "0x10166DD9C")]
		public void Prepare(ADVPlayer player)
		{
		}

		// Token: 0x06005C29 RID: 23593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600553C")]
		[Address(RVA = "0x10166DF64", Offset = "0x166DF64", VA = "0x10166DF64")]
		public void LoadFullVoiceCueSheet(string fullVoiceCueSheet)
		{
		}

		// Token: 0x06005C2A RID: 23594 RVA: 0x0001DCE8 File Offset: 0x0001BEE8
		[Token(Token = "0x600553D")]
		[Address(RVA = "0x10166E010", Offset = "0x166E010", VA = "0x10166E010")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005C2B RID: 23595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600553E")]
		[Address(RVA = "0x10166E090", Offset = "0x166E090", VA = "0x10166E090")]
		public void Update()
		{
		}

		// Token: 0x06005C2C RID: 23596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600553F")]
		[Address(RVA = "0x10166E4BC", Offset = "0x166E4BC", VA = "0x10166E4BC")]
		public void UnloadCueSheet()
		{
		}

		// Token: 0x06005C2D RID: 23597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005540")]
		[Address(RVA = "0x10166E76C", Offset = "0x166E76C", VA = "0x10166E76C")]
		public void UnloadVoice()
		{
		}

		// Token: 0x06005C2E RID: 23598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005541")]
		[Address(RVA = "0x10166E928", Offset = "0x166E928", VA = "0x10166E928")]
		public void PlaySE(string cueName, float fadeInSec = -1f, float fadeOutSec = -1f)
		{
		}

		// Token: 0x06005C2F RID: 23599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005542")]
		[Address(RVA = "0x10166EB88", Offset = "0x166EB88", VA = "0x10166EB88")]
		public void StopAllSE()
		{
		}

		// Token: 0x06005C30 RID: 23600 RVA: 0x0001DD00 File Offset: 0x0001BF00
		[Token(Token = "0x6005543")]
		[Address(RVA = "0x10166EC50", Offset = "0x166EC50", VA = "0x10166EC50")]
		public bool IsPlayingSE()
		{
			return default(bool);
		}

		// Token: 0x06005C31 RID: 23601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005544")]
		[Address(RVA = "0x10166ECBC", Offset = "0x166ECBC", VA = "0x10166ECBC")]
		public void ClearNeedVoice()
		{
		}

		// Token: 0x06005C32 RID: 23602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005545")]
		[Address(RVA = "0x10166ED1C", Offset = "0x166ED1C", VA = "0x10166ED1C")]
		public void AddNeedVoice(string advCharaID, int commandIdx)
		{
		}

		// Token: 0x06005C33 RID: 23603 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005546")]
		[Address(RVA = "0x10166EDF0", Offset = "0x166EDF0", VA = "0x10166EDF0")]
		public List<string> GetRecentlyNeed(int currentIdx)
		{
			return null;
		}

		// Token: 0x06005C34 RID: 23604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005547")]
		[Address(RVA = "0x10166F0B4", Offset = "0x166F0B4", VA = "0x10166F0B4")]
		public void LoadRecentlyNeed(int currentIdx)
		{
		}

		// Token: 0x06005C35 RID: 23605 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005548")]
		[Address(RVA = "0x10166EF8C", Offset = "0x166EF8C", VA = "0x10166EF8C")]
		private string GetCueSheetByADVCharaID(string advCharaID)
		{
			return null;
		}

		// Token: 0x06005C36 RID: 23606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005549")]
		[Address(RVA = "0x10166F358", Offset = "0x166F358", VA = "0x10166F358")]
		public void LoadVOICECharaID(string advCharaID)
		{
		}

		// Token: 0x06005C37 RID: 23607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554A")]
		[Address(RVA = "0x10166F24C", Offset = "0x166F24C", VA = "0x10166F24C")]
		private void LoadVOICECueSheet(string cueSheet)
		{
		}

		// Token: 0x06005C38 RID: 23608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554B")]
		[Address(RVA = "0x10166F39C", Offset = "0x166F39C", VA = "0x10166F39C")]
		public void PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cue, bool isTalkText)
		{
		}

		// Token: 0x06005C39 RID: 23609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554C")]
		[Address(RVA = "0x10166F544", Offset = "0x166F544", VA = "0x10166F544")]
		public void PlayVOICE(eCharaNamedType namedType, string cue, bool isTalkText)
		{
		}

		// Token: 0x06005C3A RID: 23610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554D")]
		[Address(RVA = "0x10166F714", Offset = "0x166F714", VA = "0x10166F714")]
		public void PlayVOICE(string cueSheet, string cue, bool isTalkText)
		{
		}

		// Token: 0x06005C3B RID: 23611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554E")]
		[Address(RVA = "0x10166F8B0", Offset = "0x166F8B0", VA = "0x10166F8B0")]
		public void PlayVOICEFullVoice(string cue, bool isTalkText)
		{
		}

		// Token: 0x06005C3C RID: 23612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600554F")]
		[Address(RVA = "0x10166FA20", Offset = "0x166FA20", VA = "0x10166FA20")]
		public void StopAllVOICE()
		{
		}

		// Token: 0x06005C3D RID: 23613 RVA: 0x0001DD18 File Offset: 0x0001BF18
		[Token(Token = "0x6005550")]
		[Address(RVA = "0x10166FAE8", Offset = "0x166FAE8", VA = "0x10166FAE8")]
		public bool IsPlayingVOICE()
		{
			return default(bool);
		}

		// Token: 0x06005C3E RID: 23614 RVA: 0x0001DD30 File Offset: 0x0001BF30
		[Token(Token = "0x6005551")]
		[Address(RVA = "0x10166FB54", Offset = "0x166FB54", VA = "0x10166FB54")]
		public bool IsPlayingTalkVoice()
		{
			return default(bool);
		}

		// Token: 0x06005C3F RID: 23615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005552")]
		[Address(RVA = "0x10166F530", Offset = "0x166F530", VA = "0x10166F530")]
		public void StopTalkVoice()
		{
		}

		// Token: 0x06005C40 RID: 23616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005553")]
		[Address(RVA = "0x10166FB64", Offset = "0x166FB64", VA = "0x10166FB64")]
		public void ClearNeedSE()
		{
		}

		// Token: 0x06005C41 RID: 23617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005554")]
		[Address(RVA = "0x10166FBC4", Offset = "0x166FBC4", VA = "0x10166FBC4")]
		public void AddNeedSE(string cueName)
		{
		}

		// Token: 0x06005C42 RID: 23618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005555")]
		[Address(RVA = "0x10166FC34", Offset = "0x166FC34", VA = "0x10166FC34")]
		public void LoadNeedSE()
		{
		}

		// Token: 0x06005C43 RID: 23619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005556")]
		[Address(RVA = "0x10166FDA0", Offset = "0x166FDA0", VA = "0x10166FDA0")]
		private void LoadSeCueSheet(string cueSheet)
		{
		}

		// Token: 0x06005C44 RID: 23620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005557")]
		[Address(RVA = "0x10166FE90", Offset = "0x166FE90", VA = "0x10166FE90")]
		public ADVSoundManager()
		{
		}

		// Token: 0x04006DD6 RID: 28118
		[Token(Token = "0x4004DE6")]
		private const int VOICE_CUESHEET_MAX = 10;

		// Token: 0x04006DD7 RID: 28119
		[Token(Token = "0x4004DE7")]
		private const int VOICE_CUESHEET_RECENTLY_LOAD_NUM = 8;

		// Token: 0x04006DD8 RID: 28120
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004DE8")]
		private List<SoundHandler> m_VOICEList;

		// Token: 0x04006DD9 RID: 28121
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004DE9")]
		private List<SoundHandler> m_SEList;

		// Token: 0x04006DDA RID: 28122
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004DEA")]
		private List<string> m_LoadedVOICECueSheetList;

		// Token: 0x04006DDB RID: 28123
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004DEB")]
		private List<eSoundCueSheetDB> m_LoadedCueSheetList;

		// Token: 0x04006DDC RID: 28124
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004DEC")]
		private List<string> m_LoadedSeCueSheetList;

		// Token: 0x04006DDD RID: 28125
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004DED")]
		private string m_FullVoiceCueSheet;

		// Token: 0x04006DDE RID: 28126
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004DEE")]
		private SoundHandler m_TalkTextVoice;

		// Token: 0x04006DDF RID: 28127
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004DEF")]
		private ADVPlayer m_Player;

		// Token: 0x04006DE0 RID: 28128
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004DF0")]
		private List<ADVSoundManager.ADVVoiceLoadParam> m_VoiceNeedList;

		// Token: 0x04006DE1 RID: 28129
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004DF1")]
		private List<string> m_SENeedList;

		// Token: 0x020011BE RID: 4542
		[Token(Token = "0x20012BD")]
		private class ADVVoiceLoadParam
		{
			// Token: 0x06005C45 RID: 23621 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600639D")]
			[Address(RVA = "0x10166EDB4", Offset = "0x166EDB4", VA = "0x10166EDB4")]
			public ADVVoiceLoadParam(string cueSheet, int commandIdx)
			{
			}

			// Token: 0x04006DE2 RID: 28130
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400731E")]
			public string m_CueSheet;

			// Token: 0x04006DE3 RID: 28131
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400731F")]
			public int m_CommandIdx;
		}
	}
}
