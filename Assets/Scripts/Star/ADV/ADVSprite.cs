﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x020011BF RID: 4543
	[Token(Token = "0x2000BC1")]
	[StructLayout(3)]
	public class ADVSprite : MonoBehaviour
	{
		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06005C46 RID: 23622 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C3")]
		public GameObject GameObject
		{
			[Token(Token = "0x6005558")]
			[Address(RVA = "0x10166FFC8", Offset = "0x166FFC8", VA = "0x10166FFC8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000620 RID: 1568
		// (get) Token: 0x06005C47 RID: 23623 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C4")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005559")]
			[Address(RVA = "0x101670058", Offset = "0x1670058", VA = "0x101670058")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005C48 RID: 23624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600555A")]
		[Address(RVA = "0x1016700F0", Offset = "0x16700F0", VA = "0x1016700F0")]
		public ADVSprite()
		{
		}

		// Token: 0x06005C49 RID: 23625 RVA: 0x0001DD48 File Offset: 0x0001BF48
		[Token(Token = "0x600555B")]
		[Address(RVA = "0x10167013C", Offset = "0x167013C", VA = "0x10167013C")]
		public bool Initialize(int instId)
		{
			return default(bool);
		}

		// Token: 0x06005C4A RID: 23626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600555C")]
		[Address(RVA = "0x1016703E0", Offset = "0x16703E0", VA = "0x1016703E0")]
		public void Destroy()
		{
		}

		// Token: 0x06005C4B RID: 23627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600555D")]
		[Address(RVA = "0x101670474", Offset = "0x1670474", VA = "0x101670474")]
		public void SetUseFileName(string fileName)
		{
		}

		// Token: 0x06005C4C RID: 23628 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600555E")]
		[Address(RVA = "0x10167047C", Offset = "0x167047C", VA = "0x10167047C")]
		public string GetUseFileName()
		{
			return null;
		}

		// Token: 0x06005C4D RID: 23629 RVA: 0x0001DD60 File Offset: 0x0001BF60
		[Token(Token = "0x600555F")]
		[Address(RVA = "0x101670484", Offset = "0x1670484", VA = "0x101670484")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06005C4E RID: 23630 RVA: 0x0001DD78 File Offset: 0x0001BF78
		[Token(Token = "0x6005560")]
		[Address(RVA = "0x10167048C", Offset = "0x167048C", VA = "0x10167048C")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x06005C4F RID: 23631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005561")]
		[Address(RVA = "0x1016704CC", Offset = "0x16704CC", VA = "0x1016704CC")]
		private void Update()
		{
		}

		// Token: 0x06005C50 RID: 23632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005562")]
		[Address(RVA = "0x101670290", Offset = "0x1670290", VA = "0x101670290")]
		public void RemoveSprite()
		{
		}

		// Token: 0x06005C51 RID: 23633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005563")]
		[Address(RVA = "0x101671280", Offset = "0x1671280", VA = "0x101671280")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x06005C52 RID: 23634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005564")]
		[Address(RVA = "0x101670620", Offset = "0x1670620", VA = "0x101670620")]
		public void SetImageEnabled(bool enabled)
		{
		}

		// Token: 0x06005C53 RID: 23635 RVA: 0x0001DD90 File Offset: 0x0001BF90
		[Token(Token = "0x6005565")]
		[Address(RVA = "0x101671378", Offset = "0x1671378", VA = "0x101671378")]
		public bool IsMoving()
		{
			return default(bool);
		}

		// Token: 0x06005C54 RID: 23636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005566")]
		[Address(RVA = "0x1016706DC", Offset = "0x16706DC", VA = "0x1016706DC")]
		public void Move(Vector2 pos, float sec = 0f)
		{
		}

		// Token: 0x06005C55 RID: 23637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005567")]
		[Address(RVA = "0x101671380", Offset = "0x1671380", VA = "0x101671380")]
		private void OnCompleteMove()
		{
		}

		// Token: 0x06005C56 RID: 23638 RVA: 0x0001DDA8 File Offset: 0x0001BFA8
		[Token(Token = "0x6005568")]
		[Address(RVA = "0x101671388", Offset = "0x1671388", VA = "0x101671388")]
		public bool IsScaling()
		{
			return default(bool);
		}

		// Token: 0x06005C57 RID: 23639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005569")]
		[Address(RVA = "0x101670AB0", Offset = "0x1670AB0", VA = "0x101670AB0")]
		public void Scale(Vector2 scale, float sec = 0f)
		{
		}

		// Token: 0x06005C58 RID: 23640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600556A")]
		[Address(RVA = "0x101671390", Offset = "0x1671390", VA = "0x101671390")]
		private void OnCompleteScale()
		{
		}

		// Token: 0x06005C59 RID: 23641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600556B")]
		[Address(RVA = "0x10167059C", Offset = "0x167059C", VA = "0x10167059C")]
		private void ApplyColor(Color col)
		{
		}

		// Token: 0x06005C5A RID: 23642 RVA: 0x0001DDC0 File Offset: 0x0001BFC0
		[Token(Token = "0x600556C")]
		[Address(RVA = "0x101671398", Offset = "0x1671398", VA = "0x101671398")]
		public bool IsChangingColor()
		{
			return default(bool);
		}

		// Token: 0x06005C5B RID: 23643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600556D")]
		[Address(RVA = "0x101670E84", Offset = "0x1670E84", VA = "0x101670E84")]
		public void SetColor(Color color, float sec = 0f)
		{
		}

		// Token: 0x06005C5C RID: 23644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600556E")]
		[Address(RVA = "0x1016713A0", Offset = "0x16713A0", VA = "0x1016713A0")]
		public void OnUpdateColor(float value)
		{
		}

		// Token: 0x06005C5D RID: 23645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600556F")]
		[Address(RVA = "0x1016713E8", Offset = "0x16713E8", VA = "0x1016713E8")]
		private void OnCompleteColor()
		{
		}

		// Token: 0x06005C5E RID: 23646 RVA: 0x0001DDD8 File Offset: 0x0001BFD8
		[Token(Token = "0x6005570")]
		[Address(RVA = "0x1016713F0", Offset = "0x16713F0", VA = "0x1016713F0")]
		public bool IsFade()
		{
			return default(bool);
		}

		// Token: 0x06005C5F RID: 23647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005571")]
		[Address(RVA = "0x101671268", Offset = "0x1671268", VA = "0x101671268")]
		public void FadeInit()
		{
		}

		// Token: 0x06005C60 RID: 23648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005572")]
		[Address(RVA = "0x101671400", Offset = "0x1671400", VA = "0x101671400")]
		public void FadeIn(float sec)
		{
		}

		// Token: 0x06005C61 RID: 23649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005573")]
		[Address(RVA = "0x10167145C", Offset = "0x167145C", VA = "0x10167145C")]
		public void FadeOut(float sec)
		{
		}

		// Token: 0x04006DE4 RID: 28132
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004DF2")]
		public int m_InstID;

		// Token: 0x04006DE5 RID: 28133
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004DF3")]
		private Image m_Image;

		// Token: 0x04006DE6 RID: 28134
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004DF4")]
		private bool m_IsAvailable;

		// Token: 0x04006DE7 RID: 28135
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004DF5")]
		private string m_UseFileName;

		// Token: 0x04006DE8 RID: 28136
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004DF6")]
		public Color m_ColorTweenStart;

		// Token: 0x04006DE9 RID: 28137
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004DF7")]
		public Color m_ColorTweenEnd;

		// Token: 0x04006DEA RID: 28138
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004DF8")]
		private Color m_OrigColor;

		// Token: 0x04006DEB RID: 28139
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004DF9")]
		private float m_FadeRate;

		// Token: 0x04006DEC RID: 28140
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004DFA")]
		private float m_FadeDuration;

		// Token: 0x04006DED RID: 28141
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004DFB")]
		private ADVSprite.eFadeState m_FadeState;

		// Token: 0x04006DEE RID: 28142
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4004DFC")]
		private bool m_IsMoving;

		// Token: 0x04006DEF RID: 28143
		[Cpp2IlInjected.FieldOffset(Offset = "0x75")]
		[Token(Token = "0x4004DFD")]
		private bool m_IsScaling;

		// Token: 0x04006DF0 RID: 28144
		[Cpp2IlInjected.FieldOffset(Offset = "0x76")]
		[Token(Token = "0x4004DFE")]
		private bool m_IsChangingColor;

		// Token: 0x04006DF1 RID: 28145
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004DFF")]
		private GameObject m_GameObject;

		// Token: 0x04006DF2 RID: 28146
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004E00")]
		private RectTransform m_RectTransform;

		// Token: 0x020011C0 RID: 4544
		[Token(Token = "0x20012BE")]
		private enum eFadeState
		{
			// Token: 0x04006DF4 RID: 28148
			[Token(Token = "0x4007321")]
			None,
			// Token: 0x04006DF5 RID: 28149
			[Token(Token = "0x4007322")]
			Hide,
			// Token: 0x04006DF6 RID: 28150
			[Token(Token = "0x4007323")]
			Show
		}
	}
}
