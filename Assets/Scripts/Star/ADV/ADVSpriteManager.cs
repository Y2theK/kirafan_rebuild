﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x020011C1 RID: 4545
	[Token(Token = "0x2000BC2")]
	[StructLayout(3)]
	public class ADVSpriteManager : MonoBehaviour
	{
		// Token: 0x17000621 RID: 1569
		// (get) Token: 0x06005C62 RID: 23650 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C5")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005574")]
			[Address(RVA = "0x1016714B8", Offset = "0x16714B8", VA = "0x1016714B8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005C63 RID: 23651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005575")]
		[Address(RVA = "0x1016714C0", Offset = "0x16714C0", VA = "0x1016714C0")]
		public void Prepare()
		{
		}

		// Token: 0x06005C64 RID: 23652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005576")]
		[Address(RVA = "0x101671570", Offset = "0x1671570", VA = "0x101671570")]
		public void ClearNeedList()
		{
		}

		// Token: 0x06005C65 RID: 23653 RVA: 0x0001DDF0 File Offset: 0x0001BFF0
		[Token(Token = "0x6005577")]
		[Address(RVA = "0x1016716EC", Offset = "0x16716EC", VA = "0x1016716EC")]
		public int AddNeedList(int instId, string fileName)
		{
			return 0;
		}

		// Token: 0x06005C66 RID: 23654 RVA: 0x0001DE08 File Offset: 0x0001C008
		[Token(Token = "0x6005578")]
		[Address(RVA = "0x10167173C", Offset = "0x167173C", VA = "0x10167173C")]
		public int CreateADVSpriteInst(int instId)
		{
			return 0;
		}

		// Token: 0x06005C67 RID: 23655 RVA: 0x0001DE20 File Offset: 0x0001C020
		[Token(Token = "0x6005579")]
		[Address(RVA = "0x101671880", Offset = "0x1671880", VA = "0x101671880")]
		public int AddNeedSprite(int instId, string fileName)
		{
			return 0;
		}

		// Token: 0x06005C68 RID: 23656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600557A")]
		[Address(RVA = "0x101671A80", Offset = "0x1671A80", VA = "0x101671A80")]
		public void LoadRecentNeedSprite()
		{
		}

		// Token: 0x06005C69 RID: 23657 RVA: 0x0001DE38 File Offset: 0x0001C038
		[Token(Token = "0x600557B")]
		[Address(RVA = "0x101671C2C", Offset = "0x1671C2C", VA = "0x101671C2C")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x06005C6A RID: 23658 RVA: 0x0001DE50 File Offset: 0x0001C050
		[Token(Token = "0x600557C")]
		[Address(RVA = "0x101671D2C", Offset = "0x1671D2C", VA = "0x101671D2C")]
		public bool IsExistLoadingInst()
		{
			return default(bool);
		}

		// Token: 0x06005C6B RID: 23659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600557D")]
		[Address(RVA = "0x101671E0C", Offset = "0x1671E0C", VA = "0x101671E0C")]
		public void Destroy()
		{
		}

		// Token: 0x06005C6C RID: 23660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600557E")]
		[Address(RVA = "0x101671F20", Offset = "0x1671F20", VA = "0x101671F20")]
		private void Update()
		{
		}

		// Token: 0x06005C6D RID: 23661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600557F")]
		[Address(RVA = "0x101671F24", Offset = "0x1671F24", VA = "0x101671F24")]
		private void ApplySpriteAll()
		{
		}

		// Token: 0x06005C6E RID: 23662 RVA: 0x0001DE68 File Offset: 0x0001C068
		[Token(Token = "0x6005580")]
		[Address(RVA = "0x101671FE0", Offset = "0x1671FE0", VA = "0x101671FE0")]
		private bool ApplySprite(ADVSprite inst)
		{
			return default(bool);
		}

		// Token: 0x06005C6F RID: 23663 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005581")]
		[Address(RVA = "0x101671940", Offset = "0x1671940", VA = "0x101671940")]
		private ADVSprite GetADVSpriteFromInstID(int instId)
		{
			return null;
		}

		// Token: 0x06005C70 RID: 23664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005582")]
		[Address(RVA = "0x1016721B0", Offset = "0x16721B0", VA = "0x1016721B0")]
		public void UnuseAndLoadNext(int instId)
		{
		}

		// Token: 0x06005C71 RID: 23665 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005583")]
		[Address(RVA = "0x101672068", Offset = "0x1672068", VA = "0x101672068")]
		private ADVSpriteManager.ImageInfo GetAvailableImageInfo(string fileName)
		{
			return null;
		}

		// Token: 0x06005C72 RID: 23666 RVA: 0x0001DE80 File Offset: 0x0001C080
		[Token(Token = "0x6005584")]
		[Address(RVA = "0x1016723A8", Offset = "0x16723A8", VA = "0x1016723A8")]
		public bool SetUse(int id, string fileName)
		{
			return default(bool);
		}

		// Token: 0x06005C73 RID: 23667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005585")]
		[Address(RVA = "0x1016724B4", Offset = "0x16724B4", VA = "0x1016724B4")]
		public void EnableRender(uint id, bool flag)
		{
		}

		// Token: 0x06005C74 RID: 23668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005586")]
		[Address(RVA = "0x1016725A4", Offset = "0x16725A4", VA = "0x1016725A4")]
		public void Move(uint id, Vector2 pos, float sec = 0f)
		{
		}

		// Token: 0x06005C75 RID: 23669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005587")]
		[Address(RVA = "0x1016726BC", Offset = "0x16726BC", VA = "0x1016726BC")]
		public void Scale(uint id, Vector2 scale, float sec = 0f)
		{
		}

		// Token: 0x06005C76 RID: 23670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005588")]
		[Address(RVA = "0x1016727D4", Offset = "0x16727D4", VA = "0x1016727D4")]
		public void SetColor(uint id, Color color, float sec = 0f)
		{
		}

		// Token: 0x06005C77 RID: 23671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005589")]
		[Address(RVA = "0x101672908", Offset = "0x1672908", VA = "0x101672908")]
		public void SetFadeIn(uint id, float sec = 0f)
		{
		}

		// Token: 0x06005C78 RID: 23672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600558A")]
		[Address(RVA = "0x101672A04", Offset = "0x1672A04", VA = "0x101672A04")]
		public void SetFadeOut(uint id, float sec = 0f)
		{
		}

		// Token: 0x06005C79 RID: 23673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600558B")]
		[Address(RVA = "0x1016724B0", Offset = "0x16724B0", VA = "0x1016724B0")]
		protected void OutputNotFoundError(string functionName, int id)
		{
		}

		// Token: 0x06005C7A RID: 23674 RVA: 0x0001DE98 File Offset: 0x0001C098
		[Token(Token = "0x600558C")]
		[Address(RVA = "0x101672B00", Offset = "0x1672B00", VA = "0x101672B00")]
		public bool IsMoving(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x06005C7B RID: 23675 RVA: 0x0001DEB0 File Offset: 0x0001C0B0
		[Token(Token = "0x600558D")]
		[Address(RVA = "0x101672C50", Offset = "0x1672C50", VA = "0x101672C50")]
		public bool IsScaling(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x06005C7C RID: 23676 RVA: 0x0001DEC8 File Offset: 0x0001C0C8
		[Token(Token = "0x600558E")]
		[Address(RVA = "0x101672DA0", Offset = "0x1672DA0", VA = "0x101672DA0")]
		public bool IsChangingColor(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x06005C7D RID: 23677 RVA: 0x0001DEE0 File Offset: 0x0001C0E0
		[Token(Token = "0x600558F")]
		[Address(RVA = "0x101672EF0", Offset = "0x1672EF0", VA = "0x101672EF0")]
		public bool IsFade(int id = -1)
		{
			return default(bool);
		}

		// Token: 0x06005C7E RID: 23678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005590")]
		[Address(RVA = "0x101673040", Offset = "0x1673040", VA = "0x101673040")]
		public ADVSpriteManager()
		{
		}

		// Token: 0x04006DF7 RID: 28151
		[Token(Token = "0x4004E01")]
		private const int INSTMAX = 8;

		// Token: 0x04006DF8 RID: 28152
		[Token(Token = "0x4004E02")]
		private const int LOADMAX = 16;

		// Token: 0x04006DF9 RID: 28153
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E03")]
		private List<ADVSpriteManager.ImageInfo> m_ImageList;

		// Token: 0x04006DFA RID: 28154
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E04")]
		private List<ADVSprite> m_InstList;

		// Token: 0x04006DFB RID: 28155
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E05")]
		[SerializeField]
		private ADVSprite m_Prefab;

		// Token: 0x04006DFC RID: 28156
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E06")]
		private RectTransform m_RectTransform;

		// Token: 0x04006DFD RID: 28157
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E07")]
		private int m_ImageIdMax;

		// Token: 0x020011C2 RID: 4546
		[Token(Token = "0x20012BF")]
		private class ImageInfo
		{
			// Token: 0x06005C7F RID: 23679 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600639E")]
			[Address(RVA = "0x101671A40", Offset = "0x1671A40", VA = "0x101671A40")]
			public ImageInfo(int imageId, int instId, string fileName)
			{
			}

			// Token: 0x06005C80 RID: 23680 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600639F")]
			[Address(RVA = "0x101671B90", Offset = "0x1671B90", VA = "0x101671B90")]
			public void Load()
			{
			}

			// Token: 0x06005C81 RID: 23681 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063A0")]
			[Address(RVA = "0x101671650", Offset = "0x1671650", VA = "0x101671650")]
			public void Unload()
			{
			}

			// Token: 0x06005C82 RID: 23682 RVA: 0x0001DEF8 File Offset: 0x0001C0F8
			[Token(Token = "0x60063A1")]
			[Address(RVA = "0x101672394", Offset = "0x1672394", VA = "0x101672394")]
			public bool IsAvailable()
			{
				return default(bool);
			}

			// Token: 0x06005C83 RID: 23683 RVA: 0x0001DF10 File Offset: 0x0001C110
			[Token(Token = "0x60063A2")]
			[Address(RVA = "0x101671B80", Offset = "0x1671B80", VA = "0x101671B80")]
			public bool IsActive()
			{
				return default(bool);
			}

			// Token: 0x06005C84 RID: 23684 RVA: 0x0001DF28 File Offset: 0x0001C128
			[Token(Token = "0x60063A3")]
			[Address(RVA = "0x101671D08", Offset = "0x1671D08", VA = "0x101671D08")]
			public bool IsLoading()
			{
				return default(bool);
			}

			// Token: 0x04006DFE RID: 28158
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007324")]
			public int m_ImageId;

			// Token: 0x04006DFF RID: 28159
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007325")]
			public int m_InstId;

			// Token: 0x04006E00 RID: 28160
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007326")]
			public string m_FileName;

			// Token: 0x04006E01 RID: 28161
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007327")]
			public SpriteHandler m_SpriteHandler;
		}
	}
}
