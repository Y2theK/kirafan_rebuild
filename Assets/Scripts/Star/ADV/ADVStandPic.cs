﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x020011C3 RID: 4547
	[Token(Token = "0x2000BC3")]
	[StructLayout(3)]
	public class ADVStandPic : MonoBehaviour
	{
		// Token: 0x06005C85 RID: 23685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005591")]
		[Address(RVA = "0x1016730B8", Offset = "0x16730B8", VA = "0x1016730B8")]
		public void ChangeMaterial(Material mat)
		{
		}

		// Token: 0x06005C86 RID: 23686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005592")]
		[Address(RVA = "0x1016731C8", Offset = "0x16731C8", VA = "0x1016731C8")]
		public void SetHDRFactor(float hdrFactor, float sec)
		{
		}

		// Token: 0x06005C87 RID: 23687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005593")]
		[Address(RVA = "0x1016731EC", Offset = "0x16731EC", VA = "0x1016731EC")]
		public void SetHDRFactor(float hdrFactor)
		{
		}

		// Token: 0x17000622 RID: 1570
		// (get) Token: 0x06005C88 RID: 23688 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C6")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005594")]
			[Address(RVA = "0x1016733F8", Offset = "0x16733F8", VA = "0x1016733F8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000623 RID: 1571
		// (get) Token: 0x06005C89 RID: 23689 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005C7")]
		public GameObject CacheGameObject
		{
			[Token(Token = "0x6005595")]
			[Address(RVA = "0x101673400", Offset = "0x1673400", VA = "0x101673400")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06005C8A RID: 23690 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06005C8B RID: 23691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005C8")]
		public string ADVCharaID
		{
			[Token(Token = "0x6005596")]
			[Address(RVA = "0x101673408", Offset = "0x1673408", VA = "0x101673408")]
			get
			{
				return null;
			}
			[Token(Token = "0x6005597")]
			[Address(RVA = "0x101673410", Offset = "0x1673410", VA = "0x101673410")]
			set
			{
			}
		}

		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06005C8C RID: 23692 RVA: 0x0001DF40 File Offset: 0x0001C140
		[Token(Token = "0x170005C9")]
		public ADVStandPic.eState State
		{
			[Token(Token = "0x6005598")]
			[Address(RVA = "0x101673418", Offset = "0x1673418", VA = "0x101673418")]
			get
			{
				return ADVStandPic.eState.Hide;
			}
		}

		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06005C8D RID: 23693 RVA: 0x0001DF58 File Offset: 0x0001C158
		[Token(Token = "0x170005CA")]
		public int CurrentPoseIdx
		{
			[Token(Token = "0x6005599")]
			[Address(RVA = "0x101673420", Offset = "0x1673420", VA = "0x101673420")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06005C8E RID: 23694 RVA: 0x0001DF70 File Offset: 0x0001C170
		[Token(Token = "0x170005CB")]
		public bool IsPlayingMotion
		{
			[Token(Token = "0x600559A")]
			[Address(RVA = "0x101673428", Offset = "0x1673428", VA = "0x101673428")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x06005C8F RID: 23695 RVA: 0x0001DF88 File Offset: 0x0001C188
		[Token(Token = "0x170005CC")]
		public bool IsMoving
		{
			[Token(Token = "0x600559B")]
			[Address(RVA = "0x10167347C", Offset = "0x167347C", VA = "0x10167347C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x06005C90 RID: 23696 RVA: 0x0001DFA0 File Offset: 0x0001C1A0
		[Token(Token = "0x170005CD")]
		public bool IsRotate
		{
			[Token(Token = "0x600559C")]
			[Address(RVA = "0x101673484", Offset = "0x1673484", VA = "0x101673484")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005C91 RID: 23697 RVA: 0x0001DFB8 File Offset: 0x0001C1B8
		[Token(Token = "0x600559D")]
		[Address(RVA = "0x10167348C", Offset = "0x167348C", VA = "0x10167348C")]
		public float GetBodySpriteWidth()
		{
			return 0f;
		}

		// Token: 0x06005C92 RID: 23698 RVA: 0x0001DFD0 File Offset: 0x0001C1D0
		[Token(Token = "0x600559E")]
		[Address(RVA = "0x101673578", Offset = "0x1673578", VA = "0x101673578")]
		public float GetBodySpriteHeight()
		{
			return 0f;
		}

		// Token: 0x06005C93 RID: 23699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600559F")]
		[Address(RVA = "0x101673664", Offset = "0x1673664", VA = "0x101673664")]
		public void Setup(StandPicManager manager)
		{
		}

		// Token: 0x06005C94 RID: 23700 RVA: 0x0001DFE8 File Offset: 0x0001C1E8
		[Token(Token = "0x60055A0")]
		[Address(RVA = "0x101673824", Offset = "0x1673824", VA = "0x101673824")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06005C95 RID: 23701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A1")]
		[Address(RVA = "0x101673A84", Offset = "0x1673A84", VA = "0x101673A84")]
		public void UnloadSprites()
		{
		}

		// Token: 0x06005C96 RID: 23702 RVA: 0x0001E000 File Offset: 0x0001C200
		[Token(Token = "0x60055A2")]
		[Address(RVA = "0x101673E24", Offset = "0x1673E24", VA = "0x101673E24")]
		public int GetLoadedPoseNum()
		{
			return 0;
		}

		// Token: 0x06005C97 RID: 23703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A3")]
		[Address(RVA = "0x101673EB4", Offset = "0x1673EB4", VA = "0x101673EB4")]
		public void LoadPose(string poseName)
		{
		}

		// Token: 0x06005C98 RID: 23704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A4")]
		[Address(RVA = "0x101673FB0", Offset = "0x1673FB0", VA = "0x101673FB0")]
		public void LoadPose(int idx)
		{
		}

		// Token: 0x06005C99 RID: 23705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A5")]
		[Address(RVA = "0x10167477C", Offset = "0x167477C", VA = "0x10167477C")]
		public void UnloadPose(string poseName)
		{
		}

		// Token: 0x06005C9A RID: 23706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A6")]
		[Address(RVA = "0x101674B50", Offset = "0x1674B50", VA = "0x101674B50")]
		public void ApplyPose(string poseName)
		{
		}

		// Token: 0x06005C9B RID: 23707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A7")]
		[Address(RVA = "0x101674C3C", Offset = "0x1674C3C", VA = "0x101674C3C")]
		public void ApplyPose(int poseIdx)
		{
		}

		// Token: 0x06005C9C RID: 23708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055A8")]
		[Address(RVA = "0x10167581C", Offset = "0x167581C", VA = "0x10167581C")]
		public void ApplyFace(eADVFace face)
		{
		}

		// Token: 0x06005C9D RID: 23709 RVA: 0x0001E018 File Offset: 0x0001C218
		[Token(Token = "0x60055A9")]
		[Address(RVA = "0x101674C54", Offset = "0x1674C54", VA = "0x101674C54")]
		public bool UpdateApplyPose()
		{
			return default(bool);
		}

		// Token: 0x06005C9E RID: 23710 RVA: 0x0001E030 File Offset: 0x0001C230
		[Token(Token = "0x60055AA")]
		[Address(RVA = "0x101675D04", Offset = "0x1675D04", VA = "0x101675D04")]
		public bool IsDoneApplyPose()
		{
			return default(bool);
		}

		// Token: 0x06005C9F RID: 23711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055AB")]
		[Address(RVA = "0x101675D14", Offset = "0x1675D14", VA = "0x101675D14")]
		private void Update()
		{
		}

		// Token: 0x06005CA0 RID: 23712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055AC")]
		[Address(RVA = "0x101675ECC", Offset = "0x1675ECC", VA = "0x101675ECC")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06005CA1 RID: 23713 RVA: 0x0001E048 File Offset: 0x0001C248
		[Token(Token = "0x60055AD")]
		[Address(RVA = "0x101676090", Offset = "0x1676090", VA = "0x101676090")]
		public bool IsActive()
		{
			return default(bool);
		}

		// Token: 0x06005CA2 RID: 23714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055AE")]
		[Address(RVA = "0x1016760A4", Offset = "0x16760A4", VA = "0x1016760A4")]
		public void HighlightTalker()
		{
		}

		// Token: 0x06005CA3 RID: 23715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055AF")]
		[Address(RVA = "0x1016760BC", Offset = "0x16760BC", VA = "0x1016760BC")]
		public void HighlightTalkerReset()
		{
		}

		// Token: 0x06005CA4 RID: 23716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B0")]
		[Address(RVA = "0x1016760D4", Offset = "0x16760D4", VA = "0x1016760D4")]
		public void Highlight()
		{
		}

		// Token: 0x06005CA5 RID: 23717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B1")]
		[Address(RVA = "0x1016760E0", Offset = "0x16760E0", VA = "0x1016760E0")]
		public void HighlightDefault()
		{
		}

		// Token: 0x06005CA6 RID: 23718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B2")]
		[Address(RVA = "0x1016760F8", Offset = "0x16760F8", VA = "0x1016760F8")]
		public void Shading()
		{
		}

		// Token: 0x06005CA7 RID: 23719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B3")]
		[Address(RVA = "0x101675E24", Offset = "0x1675E24", VA = "0x101675E24")]
		public void UpdateFinalColor()
		{
		}

		// Token: 0x06005CA8 RID: 23720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B4")]
		[Address(RVA = "0x101676214", Offset = "0x1676214", VA = "0x101676214")]
		public void SetPriorityState(ADVStandPic.ePriorityState state)
		{
		}

		// Token: 0x06005CA9 RID: 23721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B5")]
		[Address(RVA = "0x101676104", Offset = "0x1676104", VA = "0x101676104")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06005CAA RID: 23722 RVA: 0x0001E060 File Offset: 0x0001C260
		[Token(Token = "0x60055B6")]
		[Address(RVA = "0x10167621C", Offset = "0x167621C", VA = "0x10167621C")]
		public Color GetColor()
		{
			return default(Color);
		}

		// Token: 0x06005CAB RID: 23723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B7")]
		[Address(RVA = "0x101676254", Offset = "0x1676254", VA = "0x101676254")]
		public void SetAnchoredPosition(Vector2 position)
		{
		}

		// Token: 0x06005CAC RID: 23724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055B8")]
		[Address(RVA = "0x1016764A4", Offset = "0x16764A4", VA = "0x1016764A4")]
		public void SetPositionZ(float z)
		{
		}

		// Token: 0x06005CAD RID: 23725 RVA: 0x0001E078 File Offset: 0x0001C278
		[Token(Token = "0x60055B9")]
		[Address(RVA = "0x101676550", Offset = "0x1676550", VA = "0x101676550")]
		public ADVStandPic.ePriorityState GetPriorityState()
		{
			return ADVStandPic.ePriorityState.Error;
		}

		// Token: 0x06005CAE RID: 23726 RVA: 0x0001E090 File Offset: 0x0001C290
		[Token(Token = "0x60055BA")]
		[Address(RVA = "0x101676558", Offset = "0x1676558", VA = "0x101676558")]
		public Vector2 CharaPositionToRotPosition(Vector2 pos)
		{
			return default(Vector2);
		}

		// Token: 0x06005CAF RID: 23727 RVA: 0x0001E0A8 File Offset: 0x0001C2A8
		[Token(Token = "0x60055BB")]
		[Address(RVA = "0x101676784", Offset = "0x1676784", VA = "0x101676784")]
		public Vector2 GetCharaPosition()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB0 RID: 23728 RVA: 0x0001E0C0 File Offset: 0x0001C2C0
		[Token(Token = "0x60055BC")]
		[Address(RVA = "0x1016767F4", Offset = "0x16767F4", VA = "0x1016767F4")]
		public Vector2 GetCharaPositionWithRot()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB1 RID: 23729 RVA: 0x0001E0D8 File Offset: 0x0001C2D8
		[Token(Token = "0x60055BD")]
		[Address(RVA = "0x101676818", Offset = "0x1676818", VA = "0x101676818")]
		public Vector2 GetCharaFacePosition()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB2 RID: 23730 RVA: 0x0001E0F0 File Offset: 0x0001C2F0
		[Token(Token = "0x60055BE")]
		[Address(RVA = "0x101676BD4", Offset = "0x1676BD4", VA = "0x101676BD4")]
		public Vector2 GetCharaFacePositionWithRot()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB3 RID: 23731 RVA: 0x0001E108 File Offset: 0x0001C308
		[Token(Token = "0x60055BF")]
		[Address(RVA = "0x101676BF8", Offset = "0x1676BF8", VA = "0x101676BF8")]
		public Vector2 GetCharaFootPosition()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB4 RID: 23732 RVA: 0x0001E120 File Offset: 0x0001C320
		[Token(Token = "0x60055C0")]
		[Address(RVA = "0x101676CE0", Offset = "0x1676CE0", VA = "0x101676CE0")]
		public Vector2 GetCharaFootPositionWithRot()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB5 RID: 23733 RVA: 0x0001E138 File Offset: 0x0001C338
		[Token(Token = "0x60055C1")]
		[Address(RVA = "0x101676D04", Offset = "0x1676D04", VA = "0x101676D04")]
		public Vector2 GetEmoOffset()
		{
			return default(Vector2);
		}

		// Token: 0x06005CB6 RID: 23734 RVA: 0x0001E150 File Offset: 0x0001C350
		[Token(Token = "0x60055C2")]
		[Address(RVA = "0x101676DF0", Offset = "0x1676DF0", VA = "0x101676DF0")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06005CB7 RID: 23735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C3")]
		[Address(RVA = "0x101676E04", Offset = "0x1676E04", VA = "0x101676E04")]
		public void FadeIn(float sec)
		{
		}

		// Token: 0x06005CB8 RID: 23736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C4")]
		[Address(RVA = "0x101676EA0", Offset = "0x1676EA0", VA = "0x101676EA0")]
		public void FadeOut(float sec)
		{
		}

		// Token: 0x06005CB9 RID: 23737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C5")]
		[Address(RVA = "0x101676ECC", Offset = "0x1676ECC", VA = "0x101676ECC")]
		public void PlayMotion(string motion)
		{
		}

		// Token: 0x06005CBA RID: 23738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C6")]
		[Address(RVA = "0x101676338", Offset = "0x1676338", VA = "0x101676338")]
		public void StopMotion()
		{
		}

		// Token: 0x06005CBB RID: 23739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C7")]
		[Address(RVA = "0x10167759C", Offset = "0x167759C", VA = "0x10167759C")]
		public void StopAlpha()
		{
		}

		// Token: 0x06005CBC RID: 23740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055C8")]
		[Address(RVA = "0x101676F10", Offset = "0x1676F10", VA = "0x101676F10")]
		public void DoMotAction()
		{
		}

		// Token: 0x06005CBD RID: 23741 RVA: 0x0001E168 File Offset: 0x0001C368
		[Token(Token = "0x60055C9")]
		[Address(RVA = "0x101677628", Offset = "0x1677628", VA = "0x101677628")]
		public iTween.EaseType GetEaseType(eADVCurveType easeType)
		{
			return iTween.EaseType.easeInQuad;
		}

		// Token: 0x06005CBE RID: 23742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CA")]
		[Address(RVA = "0x10167763C", Offset = "0x167763C", VA = "0x10167763C")]
		public void OnCompleteMotAction()
		{
		}

		// Token: 0x06005CBF RID: 23743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CB")]
		[Address(RVA = "0x1016776B0", Offset = "0x16776B0", VA = "0x1016776B0")]
		public void SetPosition(Vector2 pos)
		{
		}

		// Token: 0x06005CC0 RID: 23744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CC")]
		[Address(RVA = "0x1016776BC", Offset = "0x16776BC", VA = "0x1016776BC")]
		public void MoveTo(Vector2 pos, float duration, eADVCurveType easeType, bool isCharaOut)
		{
		}

		// Token: 0x06005CC1 RID: 23745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CD")]
		[Address(RVA = "0x1016779E8", Offset = "0x16779E8", VA = "0x1016779E8")]
		public void Move(Vector2 pos, float duration, eADVCurveType easeType, bool isCharaOut)
		{
		}

		// Token: 0x06005CC2 RID: 23746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CE")]
		[Address(RVA = "0x101677D54", Offset = "0x1677D54", VA = "0x101677D54")]
		public void AlphaTo(uint alpha, float duration, eADVCurveType easeType)
		{
		}

		// Token: 0x06005CC3 RID: 23747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055CF")]
		[Address(RVA = "0x1016780D4", Offset = "0x16780D4", VA = "0x1016780D4")]
		public void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x06005CC4 RID: 23748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D0")]
		[Address(RVA = "0x1016780DC", Offset = "0x16780DC", VA = "0x1016780DC")]
		public void OnCompleteMove()
		{
		}

		// Token: 0x06005CC5 RID: 23749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D1")]
		[Address(RVA = "0x1016780F4", Offset = "0x16780F4", VA = "0x1016780F4")]
		public void OnCompleteInAlpha()
		{
		}

		// Token: 0x06005CC6 RID: 23750 RVA: 0x0001E180 File Offset: 0x0001C380
		[Token(Token = "0x60055D2")]
		[Address(RVA = "0x1016780F8", Offset = "0x16780F8", VA = "0x1016780F8")]
		public Vector2 GetRotatePivot()
		{
			return default(Vector2);
		}

		// Token: 0x06005CC7 RID: 23751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D3")]
		[Address(RVA = "0x101678128", Offset = "0x1678128", VA = "0x101678128")]
		public void RotatePivot(Vector2 pivotPos, float angle, float duration, eADVCurveType easeType)
		{
		}

		// Token: 0x06005CC8 RID: 23752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D4")]
		[Address(RVA = "0x101678548", Offset = "0x1678548", VA = "0x101678548")]
		public void Rotate(eADVCharaAnchor pivotCharaAnchor, float angle, float duration, eADVCurveType easeType)
		{
		}

		// Token: 0x06005CC9 RID: 23753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D5")]
		[Address(RVA = "0x1016787B4", Offset = "0x16787B4", VA = "0x1016787B4")]
		public void OnUpdateRotate(float value)
		{
		}

		// Token: 0x06005CCA RID: 23754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D6")]
		[Address(RVA = "0x10167888C", Offset = "0x167888C", VA = "0x10167888C")]
		public void OnCompleteRotate()
		{
		}

		// Token: 0x06005CCB RID: 23755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D7")]
		[Address(RVA = "0x101678894", Offset = "0x1678894", VA = "0x101678894")]
		public ADVStandPic()
		{
		}

		// Token: 0x04006E02 RID: 28162
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E08")]
		private string m_ADVCharaID;

		// Token: 0x04006E03 RID: 28163
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E09")]
		[SerializeField]
		private Image m_BodyImage;

		// Token: 0x04006E04 RID: 28164
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E0A")]
		[SerializeField]
		private Image m_FaceImage;

		// Token: 0x04006E05 RID: 28165
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E0B")]
		[SerializeField]
		private RectTransform m_RotateRect;

		// Token: 0x04006E06 RID: 28166
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E0C")]
		private SpriteHandler[] m_PoseSpriteHandlers;

		// Token: 0x04006E07 RID: 28167
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004E0D")]
		private ADVStandPic.eHighlightState m_HighlightState;

		// Token: 0x04006E08 RID: 28168
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004E0E")]
		private ADVStandPic.ePriorityState m_PriorityStateState;

		// Token: 0x04006E09 RID: 28169
		[Token(Token = "0x4004E0F")]
		private const string SHADER_PROPERTY_NAME_HDRFACTOR = "_HDRFactor";

		// Token: 0x04006E0A RID: 28170
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004E10")]
		private float m_CurrentHDRFactor;

		// Token: 0x04006E0B RID: 28171
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004E11")]
		private float m_GoalHDRFactor;

		// Token: 0x04006E0C RID: 28172
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004E12")]
		private float m_HDRFactorSpeed;

		// Token: 0x04006E0D RID: 28173
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004E13")]
		private List<ADVStandPic.FacePatternData> m_FacePatternList;

		// Token: 0x04006E0E RID: 28174
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004E14")]
		private ADVStandPic.FacePatternData m_CurrentFacePatternData;

		// Token: 0x04006E0F RID: 28175
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004E15")]
		private ADVStandPic.eState m_State;

		// Token: 0x04006E10 RID: 28176
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004E16")]
		private int m_CurrentPoseIdx;

		// Token: 0x04006E11 RID: 28177
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004E17")]
		private int m_NextPoseIdx;

		// Token: 0x04006E12 RID: 28178
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004E18")]
		private string m_MotionID;

		// Token: 0x04006E13 RID: 28179
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004E19")]
		private int m_MotActionIdx;

		// Token: 0x04006E14 RID: 28180
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4004E1A")]
		private Vector2 m_OrigPosition;

		// Token: 0x04006E15 RID: 28181
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4004E1B")]
		private float m_OrigAlpha;

		// Token: 0x04006E16 RID: 28182
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004E1C")]
		private bool m_IsMoving;

		// Token: 0x04006E17 RID: 28183
		[Cpp2IlInjected.FieldOffset(Offset = "0x91")]
		[Token(Token = "0x4004E1D")]
		private bool m_IsRotate;

		// Token: 0x04006E18 RID: 28184
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4004E1E")]
		private float m_StartRotate;

		// Token: 0x04006E19 RID: 28185
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004E1F")]
		private float m_FinalRotate;

		// Token: 0x04006E1A RID: 28186
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4004E20")]
		private float m_FadeDuration;

		// Token: 0x04006E1B RID: 28187
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004E21")]
		private float m_FadeRate;

		// Token: 0x04006E1C RID: 28188
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4004E22")]
		private float m_AlphaRate;

		// Token: 0x04006E1D RID: 28189
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004E23")]
		private StandPicManager m_OwnerManager;

		// Token: 0x04006E1E RID: 28190
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004E24")]
		private RectTransform m_RectTransform;

		// Token: 0x04006E1F RID: 28191
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004E25")]
		private GameObject m_GameObject;

		// Token: 0x04006E20 RID: 28192
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004E26")]
		private bool m_isCharaOutMove;

		// Token: 0x020011C4 RID: 4548
		[Token(Token = "0x20012C0")]
		public enum eHighlightState
		{
			// Token: 0x04006E22 RID: 28194
			[Token(Token = "0x4007329")]
			None,
			// Token: 0x04006E23 RID: 28195
			[Token(Token = "0x400732A")]
			Talker,
			// Token: 0x04006E24 RID: 28196
			[Token(Token = "0x400732B")]
			Command,
			// Token: 0x04006E25 RID: 28197
			[Token(Token = "0x400732C")]
			Shader
		}

		// Token: 0x020011C5 RID: 4549
		[Token(Token = "0x20012C1")]
		public enum ePriorityState
		{
			// Token: 0x04006E27 RID: 28199
			[Token(Token = "0x400732E")]
			Error,
			// Token: 0x04006E28 RID: 28200
			[Token(Token = "0x400732F")]
			Normal,
			// Token: 0x04006E29 RID: 28201
			[Token(Token = "0x4007330")]
			TopSet,
			// Token: 0x04006E2A RID: 28202
			[Token(Token = "0x4007331")]
			BottomSet
		}

		// Token: 0x020011C6 RID: 4550
		[Token(Token = "0x20012C2")]
		private class FacePatternData
		{
			// Token: 0x06005CCC RID: 23756 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063A4")]
			[Address(RVA = "0x101674520", Offset = "0x1674520", VA = "0x101674520")]
			public void Load(string resourceBaseName, int patternID, List<eADVFace> faceList)
			{
			}

			// Token: 0x06005CCD RID: 23757 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063A5")]
			[Address(RVA = "0x101673D14", Offset = "0x1673D14", VA = "0x101673D14")]
			public void UnLoadAll()
			{
			}

			// Token: 0x06005CCE RID: 23758 RVA: 0x0001E198 File Offset: 0x0001C398
			[Token(Token = "0x60063A6")]
			[Address(RVA = "0x1016739A4", Offset = "0x16739A4", VA = "0x1016739A4")]
			public bool IsAvailable()
			{
				return default(bool);
			}

			// Token: 0x06005CCF RID: 23759 RVA: 0x0001E1B0 File Offset: 0x0001C3B0
			[Token(Token = "0x60063A7")]
			[Address(RVA = "0x1016743B0", Offset = "0x16743B0", VA = "0x1016743B0")]
			public int GetPatternID()
			{
				return 0;
			}

			// Token: 0x06005CD0 RID: 23760 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60063A8")]
			[Address(RVA = "0x1016758E0", Offset = "0x16758E0", VA = "0x1016758E0")]
			public Sprite GetFaceSprite(eADVFace face)
			{
				return null;
			}

			// Token: 0x06005CD1 RID: 23761 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063A9")]
			[Address(RVA = "0x1016743B8", Offset = "0x16743B8", VA = "0x1016743B8")]
			public FacePatternData()
			{
			}

			// Token: 0x04006E2B RID: 28203
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007332")]
			private int m_PatternID;

			// Token: 0x04006E2C RID: 28204
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007333")]
			private SpriteHandler[] m_SpriteHandlers;
		}

		// Token: 0x020011C7 RID: 4551
		[Token(Token = "0x20012C3")]
		public enum eState
		{
			// Token: 0x04006E2E RID: 28206
			[Token(Token = "0x4007335")]
			Hide,
			// Token: 0x04006E2F RID: 28207
			[Token(Token = "0x4007336")]
			In,
			// Token: 0x04006E30 RID: 28208
			[Token(Token = "0x4007337")]
			Wait,
			// Token: 0x04006E31 RID: 28209
			[Token(Token = "0x4007338")]
			Out
		}
	}
}
