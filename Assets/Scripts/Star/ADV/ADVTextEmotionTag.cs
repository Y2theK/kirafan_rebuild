﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E9 RID: 4585
	[Token(Token = "0x2000BE4")]
	[StructLayout(3)]
	public class ADVTextEmotionTag : ADVTextEmotionTagBase
	{
		// Token: 0x06005D11 RID: 23825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005617")]
		[Address(RVA = "0x10167896C", Offset = "0x167896C", VA = "0x10167896C")]
		public ADVTextEmotionTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D12 RID: 23826 RVA: 0x0001E408 File Offset: 0x0001C608
		[Token(Token = "0x6005618")]
		[Address(RVA = "0x101678984", Offset = "0x1678984", VA = "0x101678984", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
