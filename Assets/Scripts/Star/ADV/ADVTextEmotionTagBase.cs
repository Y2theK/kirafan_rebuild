﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011E8 RID: 4584
	[Token(Token = "0x2000BE3")]
	[StructLayout(3)]
	public class ADVTextEmotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D10 RID: 23824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005616")]
		[Address(RVA = "0x101678978", Offset = "0x1678978", VA = "0x101678978")]
		protected ADVTextEmotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
