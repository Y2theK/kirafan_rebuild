﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011EA RID: 4586
	[Token(Token = "0x2000BE5")]
	[StructLayout(3)]
	public class ADVTextEmotionTagEnd : ADVTextEmotionTagBase
	{
		// Token: 0x06005D13 RID: 23827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005619")]
		[Address(RVA = "0x101678D2C", Offset = "0x1678D2C", VA = "0x101678D2C")]
		public ADVTextEmotionTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D14 RID: 23828 RVA: 0x0001E420 File Offset: 0x0001C620
		[Token(Token = "0x600561A")]
		[Address(RVA = "0x101678D38", Offset = "0x1678D38", VA = "0x101678D38", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
