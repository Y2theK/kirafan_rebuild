﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011EF RID: 4591
	[Token(Token = "0x2000BEA")]
	[StructLayout(3)]
	public class ADVTextFaceTag : ADVTextFaceTagBase
	{
		// Token: 0x06005D1B RID: 23835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005621")]
		[Address(RVA = "0x101678D90", Offset = "0x1678D90", VA = "0x101678D90")]
		public ADVTextFaceTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D1C RID: 23836 RVA: 0x0001E468 File Offset: 0x0001C668
		[Token(Token = "0x6005622")]
		[Address(RVA = "0x101678DA8", Offset = "0x1678DA8", VA = "0x101678DA8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
