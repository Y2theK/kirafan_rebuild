﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011EE RID: 4590
	[Token(Token = "0x2000BE9")]
	[StructLayout(3)]
	public class ADVTextFaceTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D1A RID: 23834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005620")]
		[Address(RVA = "0x101678D9C", Offset = "0x1678D9C", VA = "0x101678D9C")]
		protected ADVTextFaceTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
