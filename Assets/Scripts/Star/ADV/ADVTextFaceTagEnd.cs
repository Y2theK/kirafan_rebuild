﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F0 RID: 4592
	[Token(Token = "0x2000BEB")]
	[StructLayout(3)]
	public class ADVTextFaceTagEnd : ADVTextFaceTagBase
	{
		// Token: 0x06005D1D RID: 23837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005623")]
		[Address(RVA = "0x101679188", Offset = "0x1679188", VA = "0x101679188")]
		public ADVTextFaceTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D1E RID: 23838 RVA: 0x0001E480 File Offset: 0x0001C680
		[Token(Token = "0x6005624")]
		[Address(RVA = "0x101679194", Offset = "0x1679194", VA = "0x101679194", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
