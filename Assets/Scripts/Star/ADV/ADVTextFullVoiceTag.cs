﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011DB RID: 4571
	[Token(Token = "0x2000BD6")]
	[StructLayout(3)]
	public class ADVTextFullVoiceTag : ADVTextFullVoiceTagBase
	{
		// Token: 0x06005CF9 RID: 23801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055FF")]
		[Address(RVA = "0x10167919C", Offset = "0x167919C", VA = "0x10167919C")]
		public ADVTextFullVoiceTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CFA RID: 23802 RVA: 0x0001E318 File Offset: 0x0001C518
		[Token(Token = "0x6005600")]
		[Address(RVA = "0x1016791B4", Offset = "0x16791B4", VA = "0x1016791B4", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
