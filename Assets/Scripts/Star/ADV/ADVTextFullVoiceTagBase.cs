﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011DA RID: 4570
	[Token(Token = "0x2000BD5")]
	[StructLayout(3)]
	public class ADVTextFullVoiceTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005CF8 RID: 23800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055FE")]
		[Address(RVA = "0x1016791A8", Offset = "0x16791A8", VA = "0x1016791A8")]
		protected ADVTextFullVoiceTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
