﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011DC RID: 4572
	[Token(Token = "0x2000BD7")]
	[StructLayout(3)]
	public class ADVTextFullVoiceTagEnd : ADVTextVoiceTagBase
	{
		// Token: 0x06005CFB RID: 23803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005601")]
		[Address(RVA = "0x101679334", Offset = "0x1679334", VA = "0x101679334")]
		public ADVTextFullVoiceTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CFC RID: 23804 RVA: 0x0001E330 File Offset: 0x0001C530
		[Token(Token = "0x6005602")]
		[Address(RVA = "0x10167934C", Offset = "0x167934C", VA = "0x10167934C", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
