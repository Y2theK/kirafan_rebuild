﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E3 RID: 4579
	[Token(Token = "0x2000BDE")]
	[StructLayout(3)]
	public class ADVTextMotionTag : ADVTextMotionTagBase
	{
		// Token: 0x06005D07 RID: 23815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600560D")]
		[Address(RVA = "0x1016793A4", Offset = "0x16793A4", VA = "0x1016793A4")]
		public ADVTextMotionTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D08 RID: 23816 RVA: 0x0001E3A8 File Offset: 0x0001C5A8
		[Token(Token = "0x600560E")]
		[Address(RVA = "0x1016793BC", Offset = "0x16793BC", VA = "0x1016793BC", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
