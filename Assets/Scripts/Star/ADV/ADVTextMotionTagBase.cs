﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011E2 RID: 4578
	[Token(Token = "0x2000BDD")]
	[StructLayout(3)]
	public class ADVTextMotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D06 RID: 23814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600560C")]
		[Address(RVA = "0x1016793B0", Offset = "0x16793B0", VA = "0x1016793B0")]
		protected ADVTextMotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
