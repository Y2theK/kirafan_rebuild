﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E4 RID: 4580
	[Token(Token = "0x2000BDF")]
	[StructLayout(3)]
	public class ADVTextMotionTagEnd : ADVTextMotionTagBase
	{
		// Token: 0x06005D09 RID: 23817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600560F")]
		[Address(RVA = "0x101679638", Offset = "0x1679638", VA = "0x101679638")]
		public ADVTextMotionTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D0A RID: 23818 RVA: 0x0001E3C0 File Offset: 0x0001C5C0
		[Token(Token = "0x6005610")]
		[Address(RVA = "0x101679644", Offset = "0x1679644", VA = "0x101679644", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
