﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E0 RID: 4576
	[Token(Token = "0x2000BDB")]
	[StructLayout(3)]
	public class ADVTextMoveTag : ADVTextRangeTagBase
	{
		// Token: 0x06005D02 RID: 23810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005608")]
		[Address(RVA = "0x10167969C", Offset = "0x167969C", VA = "0x10167969C")]
		public ADVTextMoveTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D03 RID: 23811 RVA: 0x0001E378 File Offset: 0x0001C578
		[Token(Token = "0x6005609")]
		[Address(RVA = "0x1016796A8", Offset = "0x16796A8", VA = "0x1016796A8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
