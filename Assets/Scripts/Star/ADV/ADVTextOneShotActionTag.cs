﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011CA RID: 4554
	[Token(Token = "0x2000BC5")]
	[StructLayout(3)]
	public class ADVTextOneShotActionTag : ADVTextTag
	{
		// Token: 0x06005CDC RID: 23772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E2")]
		[Address(RVA = "0x101679B94", Offset = "0x1679B94", VA = "0x101679B94")]
		protected ADVTextOneShotActionTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isActiveExecTab)
		{
		}
	}
}
