﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011CD RID: 4557
	[Token(Token = "0x2000BC8")]
	[StructLayout(3)]
	public class ADVTextOriginalRangeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005CE1 RID: 23777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E7")]
		[Address(RVA = "0x101679C2C", Offset = "0x1679C2C", VA = "0x101679C2C")]
		protected ADVTextOriginalRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag)
		{
		}
	}
}
