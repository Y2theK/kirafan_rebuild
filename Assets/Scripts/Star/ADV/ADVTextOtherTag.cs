﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F8 RID: 4600
	[Token(Token = "0x2000BF3")]
	[StructLayout(3)]
	public class ADVTextOtherTag : ADVTextStandardRangeTagBase
	{
		// Token: 0x06005D2A RID: 23850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005630")]
		[Address(RVA = "0x101679C30", Offset = "0x1679C30", VA = "0x101679C30")]
		public ADVTextOtherTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag)
		{
		}

		// Token: 0x06005D2B RID: 23851 RVA: 0x0001E4F8 File Offset: 0x0001C6F8
		[Token(Token = "0x6005631")]
		[Address(RVA = "0x101679CF8", Offset = "0x1679CF8", VA = "0x101679CF8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
