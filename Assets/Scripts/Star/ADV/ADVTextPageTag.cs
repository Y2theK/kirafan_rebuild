﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F5 RID: 4597
	[Token(Token = "0x2000BF0")]
	[StructLayout(3)]
	public class ADVTextPageTag : ADVTextWaitTagBase
	{
		// Token: 0x06005D25 RID: 23845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600562B")]
		[Address(RVA = "0x101679D7C", Offset = "0x1679D7C", VA = "0x101679D7C")]
		public ADVTextPageTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D26 RID: 23846 RVA: 0x0001E4C8 File Offset: 0x0001C6C8
		[Token(Token = "0x600562C")]
		[Address(RVA = "0x101679D94", Offset = "0x1679D94", VA = "0x101679D94", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
