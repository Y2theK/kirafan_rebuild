﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011F4 RID: 4596
	[Token(Token = "0x2000BEF")]
	[StructLayout(3)]
	public class ADVTextPageTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D24 RID: 23844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600562A")]
		[Address(RVA = "0x101679E84", Offset = "0x1679E84", VA = "0x101679E84")]
		protected ADVTextPageTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
