﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011EC RID: 4588
	[Token(Token = "0x2000BE7")]
	[StructLayout(3)]
	public class ADVTextPoseTag : ADVTextPoseTagBase
	{
		// Token: 0x06005D16 RID: 23830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600561C")]
		[Address(RVA = "0x101679E90", Offset = "0x1679E90", VA = "0x101679E90")]
		public ADVTextPoseTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D17 RID: 23831 RVA: 0x0001E438 File Offset: 0x0001C638
		[Token(Token = "0x600561D")]
		[Address(RVA = "0x101679EA8", Offset = "0x1679EA8", VA = "0x101679EA8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
