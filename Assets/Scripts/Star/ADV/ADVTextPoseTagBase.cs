﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011EB RID: 4587
	[Token(Token = "0x2000BE6")]
	[StructLayout(3)]
	public class ADVTextPoseTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D15 RID: 23829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600561B")]
		[Address(RVA = "0x101679E9C", Offset = "0x1679E9C", VA = "0x101679E9C")]
		protected ADVTextPoseTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
