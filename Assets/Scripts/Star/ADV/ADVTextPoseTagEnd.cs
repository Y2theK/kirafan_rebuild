﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011ED RID: 4589
	[Token(Token = "0x2000BE8")]
	[StructLayout(3)]
	public class ADVTextPoseTagEnd : ADVTextPoseTagBase
	{
		// Token: 0x06005D18 RID: 23832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600561E")]
		[Address(RVA = "0x10167A19C", Offset = "0x167A19C", VA = "0x10167A19C")]
		public ADVTextPoseTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D19 RID: 23833 RVA: 0x0001E450 File Offset: 0x0001C650
		[Token(Token = "0x600561F")]
		[Address(RVA = "0x10167A1A8", Offset = "0x167A1A8", VA = "0x10167A1A8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
