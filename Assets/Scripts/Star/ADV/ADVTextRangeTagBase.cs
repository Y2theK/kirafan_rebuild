﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011CB RID: 4555
	[Token(Token = "0x2000BC6")]
	[StructLayout(3)]
	public class ADVTextRangeTagBase : ADVTextTag
	{
		// Token: 0x06005CDD RID: 23773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E3")]
		[Address(RVA = "0x10167A200", Offset = "0x167A200", VA = "0x10167A200")]
		protected ADVTextRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType)
		{
		}

		// Token: 0x06005CDE RID: 23774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E4")]
		[Address(RVA = "0x101678CC0", Offset = "0x1678CC0", VA = "0x101678CC0")]
		protected ADVTextRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag)
		{
		}

		// Token: 0x04006E3C RID: 28220
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4004E2C")]
		protected bool m_IsEndTag;
	}
}
