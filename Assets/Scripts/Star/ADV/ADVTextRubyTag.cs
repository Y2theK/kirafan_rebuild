﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D2 RID: 4562
	[Token(Token = "0x2000BCD")]
	[StructLayout(3)]
	public class ADVTextRubyTag : ADVTextRubyTagBase
	{
		// Token: 0x06005CEA RID: 23786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055F0")]
		[Address(RVA = "0x10167A25C", Offset = "0x167A25C", VA = "0x10167A25C")]
		public ADVTextRubyTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CEB RID: 23787 RVA: 0x0001E288 File Offset: 0x0001C488
		[Token(Token = "0x60055F1")]
		[Address(RVA = "0x10167A274", Offset = "0x167A274", VA = "0x10167A274", Slot = "6")]
		public override bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005CEC RID: 23788 RVA: 0x0001E2A0 File Offset: 0x0001C4A0
		[Token(Token = "0x60055F2")]
		[Address(RVA = "0x10167A5F4", Offset = "0x167A5F4", VA = "0x10167A5F4", Slot = "5")]
		public override bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
