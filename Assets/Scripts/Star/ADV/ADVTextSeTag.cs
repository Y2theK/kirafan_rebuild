﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D5 RID: 4565
	[Token(Token = "0x2000BD0")]
	[StructLayout(3)]
	public class ADVTextSeTag : ADVTextSeTagBase
	{
		// Token: 0x06005CEF RID: 23791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055F5")]
		[Address(RVA = "0x10167A66C", Offset = "0x167A66C", VA = "0x10167A66C")]
		public ADVTextSeTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CF0 RID: 23792 RVA: 0x0001E2B8 File Offset: 0x0001C4B8
		[Token(Token = "0x60055F6")]
		[Address(RVA = "0x10167A684", Offset = "0x167A684", VA = "0x10167A684", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
