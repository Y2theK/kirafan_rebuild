﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D6 RID: 4566
	[Token(Token = "0x2000BD1")]
	[StructLayout(3)]
	public class ADVTextSeTagEnd : ADVTextSeTagBase
	{
		// Token: 0x06005CF1 RID: 23793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055F7")]
		[Address(RVA = "0x10167A808", Offset = "0x167A808", VA = "0x10167A808")]
		public ADVTextSeTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CF2 RID: 23794 RVA: 0x0001E2D0 File Offset: 0x0001C4D0
		[Token(Token = "0x60055F8")]
		[Address(RVA = "0x10167A814", Offset = "0x167A814", VA = "0x10167A814", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
