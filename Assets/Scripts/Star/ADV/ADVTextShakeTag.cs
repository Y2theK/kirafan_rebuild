﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F2 RID: 4594
	[Token(Token = "0x2000BED")]
	[StructLayout(3)]
	public class ADVTextShakeTag : ADVTextShakeTagBase
	{
		// Token: 0x06005D20 RID: 23840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005626")]
		[Address(RVA = "0x10167A86C", Offset = "0x167A86C", VA = "0x10167A86C")]
		public ADVTextShakeTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D21 RID: 23841 RVA: 0x0001E498 File Offset: 0x0001C698
		[Token(Token = "0x6005627")]
		[Address(RVA = "0x10167A884", Offset = "0x167A884", VA = "0x10167A884", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
