﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F3 RID: 4595
	[Token(Token = "0x2000BEE")]
	[StructLayout(3)]
	public class ADVTextShakeTagEnd : ADVTextShakeTagBase
	{
		// Token: 0x06005D22 RID: 23842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005628")]
		[Address(RVA = "0x10167AB44", Offset = "0x167AB44", VA = "0x10167AB44")]
		public ADVTextShakeTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D23 RID: 23843 RVA: 0x0001E4B0 File Offset: 0x0001C6B0
		[Token(Token = "0x6005629")]
		[Address(RVA = "0x10167AB50", Offset = "0x167AB50", VA = "0x10167AB50", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
