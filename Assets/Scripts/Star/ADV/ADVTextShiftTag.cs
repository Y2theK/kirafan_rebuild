﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011F7 RID: 4599
	[Token(Token = "0x2000BF2")]
	[StructLayout(3)]
	public class ADVTextShiftTag : ADVTextWaitTagBase
	{
		// Token: 0x06005D28 RID: 23848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600562E")]
		[Address(RVA = "0x10167ABA8", Offset = "0x167ABA8", VA = "0x10167ABA8")]
		public ADVTextShiftTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D29 RID: 23849 RVA: 0x0001E4E0 File Offset: 0x0001C6E0
		[Token(Token = "0x600562F")]
		[Address(RVA = "0x10167ABB4", Offset = "0x167ABB4", VA = "0x10167ABB4", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
