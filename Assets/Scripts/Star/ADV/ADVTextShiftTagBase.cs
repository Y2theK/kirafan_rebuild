﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011F6 RID: 4598
	[Token(Token = "0x2000BF1")]
	[StructLayout(3)]
	public class ADVTextShiftTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D27 RID: 23847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600562D")]
		[Address(RVA = "0x10167AD4C", Offset = "0x167AD4C", VA = "0x10167AD4C")]
		protected ADVTextShiftTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
