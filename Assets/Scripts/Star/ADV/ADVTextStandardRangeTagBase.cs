﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011CC RID: 4556
	[Token(Token = "0x2000BC7")]
	[StructLayout(3)]
	public class ADVTextStandardRangeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005CDF RID: 23775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E5")]
		[Address(RVA = "0x101679C9C", Offset = "0x1679C9C", VA = "0x101679C9C")]
		protected ADVTextStandardRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType)
		{
		}

		// Token: 0x06005CE0 RID: 23776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E6")]
		[Address(RVA = "0x10167AD58", Offset = "0x167AD58", VA = "0x10167AD58", Slot = "7")]
		public override void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
		}
	}
}
