﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011CF RID: 4559
	[Token(Token = "0x2000BCA")]
	[StructLayout(3)]
	public class ADVTextStrongTag : ADVTextStrongTagBase
	{
		// Token: 0x06005CE5 RID: 23781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055EB")]
		[Address(RVA = "0x10167AD5C", Offset = "0x167AD5C", VA = "0x10167AD5C")]
		public ADVTextStrongTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CE6 RID: 23782 RVA: 0x0001E258 File Offset: 0x0001C458
		[Token(Token = "0x60055EC")]
		[Address(RVA = "0x10167AD74", Offset = "0x167AD74", VA = "0x10167AD74", Slot = "6")]
		public override bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
