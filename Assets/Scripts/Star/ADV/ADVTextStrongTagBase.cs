﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011CE RID: 4558
	[Token(Token = "0x2000BC9")]
	[StructLayout(3)]
	public class ADVTextStrongTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005CE2 RID: 23778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055E8")]
		[Address(RVA = "0x10167AD68", Offset = "0x167AD68", VA = "0x10167AD68")]
		protected ADVTextStrongTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}

		// Token: 0x06005CE3 RID: 23779 RVA: 0x0001E240 File Offset: 0x0001C440
		[Token(Token = "0x60055E9")]
		[Address(RVA = "0x10167AE80", Offset = "0x167AE80", VA = "0x10167AE80", Slot = "5")]
		public override bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005CE4 RID: 23780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055EA")]
		[Address(RVA = "0x10167AE88", Offset = "0x167AE88", VA = "0x10167AE88", Slot = "7")]
		public override void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
		}
	}
}
