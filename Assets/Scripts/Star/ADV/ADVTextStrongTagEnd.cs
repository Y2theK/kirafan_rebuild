﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D0 RID: 4560
	[Token(Token = "0x2000BCB")]
	[StructLayout(3)]
	public class ADVTextStrongTagEnd : ADVTextStrongTagBase
	{
		// Token: 0x06005CE7 RID: 23783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055ED")]
		[Address(RVA = "0x10167AE8C", Offset = "0x167AE8C", VA = "0x10167AE8C")]
		public ADVTextStrongTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CE8 RID: 23784 RVA: 0x0001E270 File Offset: 0x0001C470
		[Token(Token = "0x60055EE")]
		[Address(RVA = "0x10167AE98", Offset = "0x167AE98", VA = "0x10167AE98", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
