﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011C8 RID: 4552
	[Token(Token = "0x2000BC4")]
	[StructLayout(3)]
	public class ADVTextTag
	{
		// Token: 0x06005CD2 RID: 23762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055D8")]
		[Address(RVA = "0x101679BDC", Offset = "0x1679BDC", VA = "0x101679BDC")]
		protected ADVTextTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType)
		{
		}

		// Token: 0x06005CD3 RID: 23763 RVA: 0x0001E1C8 File Offset: 0x0001C3C8
		[Token(Token = "0x60055D9")]
		[Address(RVA = "0x10167AF78", Offset = "0x167AF78", VA = "0x10167AF78", Slot = "4")]
		public virtual bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005CD4 RID: 23764 RVA: 0x0001E1E0 File Offset: 0x0001C3E0
		[Token(Token = "0x60055DA")]
		[Address(RVA = "0x10167B05C", Offset = "0x167B05C", VA = "0x10167B05C", Slot = "5")]
		public virtual bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005CD5 RID: 23765 RVA: 0x0001E1F8 File Offset: 0x0001C3F8
		[Token(Token = "0x60055DB")]
		[Address(RVA = "0x10167B064", Offset = "0x167B064", VA = "0x10167B064", Slot = "6")]
		public virtual bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}

		// Token: 0x06005CD6 RID: 23766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055DC")]
		[Address(RVA = "0x10167B06C", Offset = "0x167B06C", VA = "0x10167B06C", Slot = "7")]
		public virtual void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
		}

		// Token: 0x06005CD7 RID: 23767 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60055DD")]
		[Address(RVA = "0x101679A10", Offset = "0x1679A10", VA = "0x101679A10")]
		protected List<string> ParseTag(string tag)
		{
			return null;
		}

		// Token: 0x06005CD8 RID: 23768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055DE")]
		[Address(RVA = "0x101678C14", Offset = "0x1678C14", VA = "0x101678C14")]
		protected void DeleteTag(ref string text, ref int idx, int charNum)
		{
		}

		// Token: 0x06005CD9 RID: 23769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055DF")]
		[Address(RVA = "0x10167B0BC", Offset = "0x167B0BC", VA = "0x10167B0BC")]
		public void SetTapWaitWithTapFlag(bool flag)
		{
		}

		// Token: 0x06005CDA RID: 23770 RVA: 0x0001E210 File Offset: 0x0001C410
		[Token(Token = "0x60055E0")]
		[Address(RVA = "0x10167B0C4", Offset = "0x167B0C4", VA = "0x10167B0C4")]
		public ADVParser.EventType GetEventType()
		{
			return ADVParser.EventType.event_none;
		}

		// Token: 0x06005CDB RID: 23771 RVA: 0x0001E228 File Offset: 0x0001C428
		[Token(Token = "0x60055E1")]
		[Address(RVA = "0x10167B0CC", Offset = "0x167B0CC", VA = "0x10167B0CC")]
		public ADVTextTag.eTagType GetTagType()
		{
			return ADVTextTag.eTagType.Error;
		}

		// Token: 0x04006E32 RID: 28210
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004E27")]
		protected ADVPlayer m_Player;

		// Token: 0x04006E33 RID: 28211
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E28")]
		protected ADVParser m_ParentParser;

		// Token: 0x04006E34 RID: 28212
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E29")]
		protected ADVParser.EventType m_EventType;

		// Token: 0x04006E35 RID: 28213
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004E2A")]
		protected ADVTextTag.eTagType m_TagType;

		// Token: 0x04006E36 RID: 28214
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E2B")]
		protected bool m_IsTapWaitWithTap;

		// Token: 0x020011C9 RID: 4553
		[Token(Token = "0x20012C4")]
		public enum eTagType
		{
			// Token: 0x04006E38 RID: 28216
			[Token(Token = "0x400733A")]
			Error,
			// Token: 0x04006E39 RID: 28217
			[Token(Token = "0x400733B")]
			OneShotAction,
			// Token: 0x04006E3A RID: 28218
			[Token(Token = "0x400733C")]
			RangeBegin,
			// Token: 0x04006E3B RID: 28219
			[Token(Token = "0x400733D")]
			RangeEnd
		}
	}
}
