﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D8 RID: 4568
	[Token(Token = "0x2000BD3")]
	[StructLayout(3)]
	public class ADVTextVoiceTag : ADVTextVoiceTagBase
	{
		// Token: 0x06005CF4 RID: 23796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055FA")]
		[Address(RVA = "0x10167B0D4", Offset = "0x167B0D4", VA = "0x10167B0D4")]
		public ADVTextVoiceTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CF5 RID: 23797 RVA: 0x0001E2E8 File Offset: 0x0001C4E8
		[Token(Token = "0x60055FB")]
		[Address(RVA = "0x10167B0E0", Offset = "0x167B0E0", VA = "0x10167B0E0", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
