﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011D9 RID: 4569
	[Token(Token = "0x2000BD4")]
	[StructLayout(3)]
	public class ADVTextVoiceTagEnd : ADVTextVoiceTagBase
	{
		// Token: 0x06005CF6 RID: 23798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60055FC")]
		[Address(RVA = "0x10167B3CC", Offset = "0x167B3CC", VA = "0x10167B3CC")]
		public ADVTextVoiceTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CF7 RID: 23799 RVA: 0x0001E300 File Offset: 0x0001C500
		[Token(Token = "0x60055FD")]
		[Address(RVA = "0x10167B3D8", Offset = "0x167B3D8", VA = "0x10167B3D8", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
