﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E6 RID: 4582
	[Token(Token = "0x2000BE1")]
	[StructLayout(3)]
	public class ADVTextWaitMotionTag : ADVTextWaitMotionTagBase
	{
		// Token: 0x06005D0C RID: 23820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005612")]
		[Address(RVA = "0x10167B430", Offset = "0x167B430", VA = "0x10167B430")]
		public ADVTextWaitMotionTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D0D RID: 23821 RVA: 0x0001E3D8 File Offset: 0x0001C5D8
		[Token(Token = "0x6005613")]
		[Address(RVA = "0x10167B448", Offset = "0x167B448", VA = "0x10167B448", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
