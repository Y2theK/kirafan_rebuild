﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011E5 RID: 4581
	[Token(Token = "0x2000BE0")]
	[StructLayout(3)]
	public class ADVTextWaitMotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005D0B RID: 23819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005611")]
		[Address(RVA = "0x10167B43C", Offset = "0x167B43C", VA = "0x10167B43C")]
		protected ADVTextWaitMotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
