﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E7 RID: 4583
	[Token(Token = "0x2000BE2")]
	[StructLayout(3)]
	public class ADVTextWaitMotionTagEnd : ADVTextWaitMotionTagBase
	{
		// Token: 0x06005D0E RID: 23822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005614")]
		[Address(RVA = "0x10167B4D8", Offset = "0x167B4D8", VA = "0x10167B4D8")]
		public ADVTextWaitMotionTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D0F RID: 23823 RVA: 0x0001E3F0 File Offset: 0x0001C5F0
		[Token(Token = "0x6005615")]
		[Address(RVA = "0x10167B4E4", Offset = "0x167B4E4", VA = "0x10167B4E4", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
