﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011E1 RID: 4577
	[Token(Token = "0x2000BDC")]
	[StructLayout(3)]
	public class ADVTextWaitMoveTag : ADVTextRangeTagBase
	{
		// Token: 0x06005D04 RID: 23812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600560A")]
		[Address(RVA = "0x10167B53C", Offset = "0x167B53C", VA = "0x10167B53C")]
		public ADVTextWaitMoveTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D05 RID: 23813 RVA: 0x0001E390 File Offset: 0x0001C590
		[Token(Token = "0x600560B")]
		[Address(RVA = "0x10167B548", Offset = "0x167B548", VA = "0x10167B548", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
