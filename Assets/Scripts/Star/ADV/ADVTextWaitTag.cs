﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011DE RID: 4574
	[Token(Token = "0x2000BD9")]
	[StructLayout(3)]
	public class ADVTextWaitTag : ADVTextWaitTagBase
	{
		// Token: 0x06005CFE RID: 23806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005604")]
		[Address(RVA = "0x10167B640", Offset = "0x167B640", VA = "0x10167B640")]
		public ADVTextWaitTag(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005CFF RID: 23807 RVA: 0x0001E348 File Offset: 0x0001C548
		[Token(Token = "0x6005605")]
		[Address(RVA = "0x10167B64C", Offset = "0x167B64C", VA = "0x10167B64C", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
