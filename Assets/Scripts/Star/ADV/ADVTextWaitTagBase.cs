﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011DD RID: 4573
	[Token(Token = "0x2000BD8")]
	[StructLayout(3)]
	public class ADVTextWaitTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06005CFD RID: 23805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005603")]
		[Address(RVA = "0x101679D88", Offset = "0x1679D88", VA = "0x101679D88")]
		protected ADVTextWaitTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag)
		{
		}
	}
}
