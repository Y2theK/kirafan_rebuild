﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x020011DF RID: 4575
	[Token(Token = "0x2000BDA")]
	[StructLayout(3)]
	public class ADVTextWaitTagEnd : ADVTextWaitTagBase
	{
		// Token: 0x06005D00 RID: 23808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005606")]
		[Address(RVA = "0x10167B80C", Offset = "0x167B80C", VA = "0x10167B80C")]
		public ADVTextWaitTagEnd(ADVPlayer player, ADVParser parentParser)
		{
		}

		// Token: 0x06005D01 RID: 23809 RVA: 0x0001E360 File Offset: 0x0001C560
		[Token(Token = "0x6005607")]
		[Address(RVA = "0x10167B818", Offset = "0x167B818", VA = "0x10167B818", Slot = "4")]
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return default(bool);
		}
	}
}
