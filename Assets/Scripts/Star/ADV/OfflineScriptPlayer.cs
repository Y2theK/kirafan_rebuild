﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x02001200 RID: 4608
	[Token(Token = "0x2000BF6")]
	[StructLayout(3)]
	public class OfflineScriptPlayer
	{
		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06005D9B RID: 23963 RVA: 0x0001E798 File Offset: 0x0001C998
		[Token(Token = "0x170005DB")]
		public bool IsDonePrepare
		{
			[Token(Token = "0x6005699")]
			[Address(RVA = "0x10167B870", Offset = "0x167B870", VA = "0x10167B870")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06005D9C RID: 23964 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005DC")]
		public ADVTextDB TextDB
		{
			[Token(Token = "0x600569A")]
			[Address(RVA = "0x10167B878", Offset = "0x167B878", VA = "0x10167B878")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06005D9D RID: 23965 RVA: 0x0001E7B0 File Offset: 0x0001C9B0
		[Token(Token = "0x170005DD")]
		public bool IsError
		{
			[Token(Token = "0x600569B")]
			[Address(RVA = "0x10167B880", Offset = "0x167B880", VA = "0x10167B880")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005D9E RID: 23966 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600569C")]
		[Address(RVA = "0x10167B888", Offset = "0x167B888", VA = "0x10167B888")]
		public static string[] GetScriptPaths(eADVCategory category, string scriptName, string textDBName)
		{
			return null;
		}

		// Token: 0x06005D9F RID: 23967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600569D")]
		[Address(RVA = "0x10167BB88", Offset = "0x167BB88", VA = "0x10167BB88")]
		public void Prepare(eADVCategory category, string scriptName, string textDBName)
		{
		}

		// Token: 0x06005DA0 RID: 23968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600569E")]
		[Address(RVA = "0x10167BC0C", Offset = "0x167BC0C", VA = "0x10167BC0C")]
		private void PrepareWithPath(eADVCategory category, string scriptPath, string textDBPath)
		{
		}

		// Token: 0x06005DA1 RID: 23969 RVA: 0x0001E7C8 File Offset: 0x0001C9C8
		[Token(Token = "0x600569F")]
		[Address(RVA = "0x10167BD00", Offset = "0x167BD00", VA = "0x10167BD00")]
		public bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005DA2 RID: 23970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60056A0")]
		[Address(RVA = "0x10167C094", Offset = "0x167C094", VA = "0x10167C094")]
		public void Destroy()
		{
		}

		// Token: 0x06005DA3 RID: 23971 RVA: 0x0001E7E0 File Offset: 0x0001C9E0
		[Token(Token = "0x60056A1")]
		[Address(RVA = "0x10167C0AC", Offset = "0x167C0AC", VA = "0x10167C0AC")]
		public bool UpdateDestroy()
		{
			return default(bool);
		}

		// Token: 0x06005DA4 RID: 23972 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A2")]
		[Address(RVA = "0x10167C120", Offset = "0x167C120", VA = "0x10167C120")]
		public List<int> GetSkipLogTextList()
		{
			return null;
		}

		// Token: 0x06005DA5 RID: 23973 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A3")]
		[Address(RVA = "0x10167C360", Offset = "0x167C360", VA = "0x10167C360")]
		private List<string> GetTagSEList(string text)
		{
			return null;
		}

		// Token: 0x06005DA6 RID: 23974 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A4")]
		[Address(RVA = "0x10167C578", Offset = "0x167C578", VA = "0x10167C578")]
		private List<string> GetStandPicFacePath(string charaID, ADVDataBase DB, [Optional] string poseName)
		{
			return null;
		}

		// Token: 0x06005DA7 RID: 23975 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A5")]
		[Address(RVA = "0x10167C91C", Offset = "0x167C91C", VA = "0x10167C91C")]
		public List<string> GetFacePath(string charaID, eADVFace face, ADVDataBase DB)
		{
			return null;
		}

		// Token: 0x06005DA8 RID: 23976 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A6")]
		[Address(RVA = "0x10167CD0C", Offset = "0x167CD0C", VA = "0x10167CD0C")]
		public List<OfflineScriptPlayer.ADVResourceData> GetResourceList(ADVDataBase DB)
		{
			return null;
		}

		// Token: 0x06005DA9 RID: 23977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60056A7")]
		[Address(RVA = "0x10167CDF0", Offset = "0x167CDF0", VA = "0x10167CDF0")]
		private void SetResourceListFromCommand(ScriptData_Param script, ADVDataBase DB)
		{
		}

		// Token: 0x06005DAA RID: 23978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60056A8")]
		[Address(RVA = "0x10167F310", Offset = "0x167F310", VA = "0x10167F310")]
		private void ParseTalker(string charaName, ADVDataBase DB, out string talkerCharaID, out string talkerName)
		{
		}

		// Token: 0x06005DAB RID: 23979 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056A9")]
		[Address(RVA = "0x10167F434", Offset = "0x167F434", VA = "0x10167F434")]
		private string GetTalkerADVCharaID(uint m_TextID, ADVDataBase DB)
		{
			return null;
		}

		// Token: 0x06005DAC RID: 23980 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056AA")]
		[Address(RVA = "0x10167F214", Offset = "0x167F214", VA = "0x10167F214")]
		private string GetCueSheetByADVCharaID(string advCharaID, ADVDataBase DB)
		{
			return null;
		}

		// Token: 0x06005DAD RID: 23981 RVA: 0x0001E7F8 File Offset: 0x0001C9F8
		[Token(Token = "0x60056AB")]
		[Address(RVA = "0x10167CBF0", Offset = "0x167CBF0", VA = "0x10167CBF0")]
		private ADVCharacterListDB_Param GetADVCharacterParam(string ADVCharaID, ADVDataBase DB)
		{
			return default(ADVCharacterListDB_Param);
		}

		// Token: 0x06005DAE RID: 23982 RVA: 0x0001E810 File Offset: 0x0001CA10
		[Token(Token = "0x60056AC")]
		[Address(RVA = "0x10167F5F8", Offset = "0x167F5F8", VA = "0x10167F5F8")]
		private ADVTextTagArgDB_Param GetADVTextTagParam(string ADVTextTagArgRefName, ADVDataBase DB)
		{
			return default(ADVTextTagArgDB_Param);
		}

		// Token: 0x06005DAF RID: 23983 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60056AD")]
		[Address(RVA = "0x10167F11C", Offset = "0x167F11C", VA = "0x10167F11C")]
		private string GetSECueSheetName(string name)
		{
			return null;
		}

		// Token: 0x06005DB0 RID: 23984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60056AE")]
		[Address(RVA = "0x10167F6FC", Offset = "0x167F6FC", VA = "0x10167F6FC")]
		public OfflineScriptPlayer()
		{
		}

		// Token: 0x04006E68 RID: 28264
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004E49")]
		private string[] m_TagCommand;

		// Token: 0x04006E69 RID: 28265
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E4A")]
		private Dictionary<string, int> m_standPicPoseNum;

		// Token: 0x04006E6A RID: 28266
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E4B")]
		private string m_ScriptPath;

		// Token: 0x04006E6B RID: 28267
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E4C")]
		private string m_TextDBPath;

		// Token: 0x04006E6C RID: 28268
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E4D")]
		private bool m_IsDonePrepare;

		// Token: 0x04006E6D RID: 28269
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4004E4E")]
		private bool m_IsError;

		// Token: 0x04006E6E RID: 28270
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E4F")]
		private ABResourceLoader m_Loader;

		// Token: 0x04006E6F RID: 28271
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004E50")]
		private ScriptData m_ScriptDB;

		// Token: 0x04006E70 RID: 28272
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004E51")]
		private ABResourceObjectHandler m_HndlScript;

		// Token: 0x04006E71 RID: 28273
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004E52")]
		private ADVTextDB m_TextDB;

		// Token: 0x04006E72 RID: 28274
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004E53")]
		private ABResourceObjectHandler m_HndlTextDB;

		// Token: 0x04006E73 RID: 28275
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004E54")]
		private List<OfflineScriptPlayer.ADVResourceData> m_advResourceList;

		// Token: 0x02001201 RID: 4609
		[Token(Token = "0x20012C8")]
		public class ADVCommand
		{
			// Token: 0x06005DB1 RID: 23985 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063B1")]
			[Address(RVA = "0x10167F0AC", Offset = "0x167F0AC", VA = "0x10167F0AC")]
			public ADVCommand()
			{
			}

			// Token: 0x04006E74 RID: 28276
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007348")]
			public OfflineScriptPlayer.ADVCommand.Param param;

			// Token: 0x02001202 RID: 4610
			[Token(Token = "0x2001358")]
			public class Param
			{
				// Token: 0x06005DB2 RID: 23986 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600651F")]
				[Address(RVA = "0x10167FEA4", Offset = "0x167FEA4", VA = "0x10167FEA4")]
				public Param()
				{
				}

				// Token: 0x04006E75 RID: 28277
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x40075DB")]
				public string FuncName;

				// Token: 0x04006E76 RID: 28278
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x40075DC")]
				public int ArgNum;

				// Token: 0x04006E77 RID: 28279
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x40075DD")]
				public OfflineScriptPlayer.ADVCommand.ArgParam[] argParam;
			}

			// Token: 0x02001203 RID: 4611
			[Token(Token = "0x2001359")]
			public struct ArgParam
			{
				// Token: 0x04006E78 RID: 28280
				[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
				[Token(Token = "0x40075DE")]
				public int ArgType;

				// Token: 0x04006E79 RID: 28281
				[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
				[Token(Token = "0x40075DF")]
				public string value;
			}
		}

		// Token: 0x02001204 RID: 4612
		[Token(Token = "0x20012C9")]
		public enum eAdvResourceType
		{
			// Token: 0x04006E7B RID: 28283
			[Token(Token = "0x400734A")]
			AssetBundle,
			// Token: 0x04006E7C RID: 28284
			[Token(Token = "0x400734B")]
			Sound_acb,
			// Token: 0x04006E7D RID: 28285
			[Token(Token = "0x400734C")]
			Sound_both
		}

		// Token: 0x02001205 RID: 4613
		[Token(Token = "0x20012CA")]
		public class ADVResourceData
		{
			// Token: 0x06005DB3 RID: 23987 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063B2")]
			[Address(RVA = "0x10167F114", Offset = "0x167F114", VA = "0x10167F114")]
			public ADVResourceData()
			{
			}

			// Token: 0x04006E7E RID: 28286
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400734D")]
			public string path;

			// Token: 0x04006E7F RID: 28287
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400734E")]
			public OfflineScriptPlayer.eAdvResourceType type;
		}
	}
}
