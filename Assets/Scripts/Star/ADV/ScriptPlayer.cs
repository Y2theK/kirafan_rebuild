﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.ADV
{
	// Token: 0x020011F9 RID: 4601
	[Token(Token = "0x2000BF4")]
	[StructLayout(3)]
	public class ScriptPlayer
	{
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x06005D2C RID: 23852 RVA: 0x0001E510 File Offset: 0x0001C710
		[Token(Token = "0x170005CE")]
		public bool IsDonePrepare
		{
			[Token(Token = "0x6005632")]
			[Address(RVA = "0x10167FEAC", Offset = "0x167FEAC", VA = "0x10167FEAC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06005D2D RID: 23853 RVA: 0x0001E528 File Offset: 0x0001C728
		[Token(Token = "0x170005CF")]
		public bool IsEnd
		{
			[Token(Token = "0x6005633")]
			[Address(RVA = "0x10167FEB4", Offset = "0x167FEB4", VA = "0x10167FEB4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06005D2E RID: 23854 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005D0")]
		public ADVTextDB TextDB
		{
			[Token(Token = "0x6005634")]
			[Address(RVA = "0x10167FEBC", Offset = "0x167FEBC", VA = "0x10167FEBC")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700062D RID: 1581
		// (set) Token: 0x06005D2F RID: 23855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005D1")]
		public ADVPlayer Player
		{
			[Token(Token = "0x6005635")]
			[Address(RVA = "0x10167FEC4", Offset = "0x167FEC4", VA = "0x10167FEC4")]
			set
			{
			}
		}

		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06005D30 RID: 23856 RVA: 0x0001E540 File Offset: 0x0001C740
		[Token(Token = "0x170005D2")]
		public int NowCommandIdx
		{
			[Token(Token = "0x6005636")]
			[Address(RVA = "0x10167FECC", Offset = "0x167FECC", VA = "0x10167FECC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06005D31 RID: 23857 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005D3")]
		public List<ScriptPlayer.ADVCommand> CommandList
		{
			[Token(Token = "0x6005637")]
			[Address(RVA = "0x10167FED4", Offset = "0x167FED4", VA = "0x10167FED4")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06005D32 RID: 23858 RVA: 0x0001E558 File Offset: 0x0001C758
		[Token(Token = "0x170005D4")]
		public bool IsError
		{
			[Token(Token = "0x6005638")]
			[Address(RVA = "0x10167FEDC", Offset = "0x167FEDC", VA = "0x10167FEDC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005D33 RID: 23859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005639")]
		[Address(RVA = "0x10167FEE4", Offset = "0x167FEE4", VA = "0x10167FEE4")]
		public void Prepare(eADVCategory category, string scriptName, string textDBName)
		{
		}

		// Token: 0x06005D34 RID: 23860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600563A")]
		[Address(RVA = "0x1016801C8", Offset = "0x16801C8", VA = "0x1016801C8")]
		private void PrepareWithPath(eADVCategory category, string scriptPath, string textDBPath)
		{
		}

		// Token: 0x06005D35 RID: 23861 RVA: 0x0001E570 File Offset: 0x0001C770
		[Token(Token = "0x600563B")]
		[Address(RVA = "0x1016802FC", Offset = "0x16802FC", VA = "0x1016802FC")]
		public bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005D36 RID: 23862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600563C")]
		[Address(RVA = "0x10168069C", Offset = "0x168069C", VA = "0x10168069C")]
		public void ParseScript(int scriptId = -1)
		{
		}

		// Token: 0x06005D37 RID: 23863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600563D")]
		[Address(RVA = "0x1016807E0", Offset = "0x16807E0", VA = "0x1016807E0")]
		private void SetCommandList(ScriptData_Param script)
		{
		}

		// Token: 0x06005D38 RID: 23864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600563E")]
		[Address(RVA = "0x101682BA8", Offset = "0x1682BA8", VA = "0x101682BA8")]
		public void Destroy()
		{
		}

		// Token: 0x06005D39 RID: 23865 RVA: 0x0001E588 File Offset: 0x0001C788
		[Token(Token = "0x600563F")]
		[Address(RVA = "0x101682BC0", Offset = "0x1682BC0", VA = "0x101682BC0")]
		public bool UpdateDestroy()
		{
			return default(bool);
		}

		// Token: 0x06005D3A RID: 23866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005640")]
		[Address(RVA = "0x101682C34", Offset = "0x1682C34", VA = "0x101682C34")]
		public void Play(int startCommandIdx = 0)
		{
		}

		// Token: 0x06005D3B RID: 23867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005641")]
		[Address(RVA = "0x101682C40", Offset = "0x1682C40", VA = "0x101682C40")]
		public void DoCommand()
		{
		}

		// Token: 0x06005D3C RID: 23868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005642")]
		[Address(RVA = "0x101683C3C", Offset = "0x1683C3C", VA = "0x101683C3C")]
		public void Goto(int scriptId)
		{
		}

		// Token: 0x06005D3D RID: 23869 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005643")]
		[Address(RVA = "0x101683C40", Offset = "0x1683C40", VA = "0x101683C40")]
		public List<int> GetSkipLogTextList()
		{
			return null;
		}

		// Token: 0x06005D3E RID: 23870 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005644")]
		[Address(RVA = "0x10168299C", Offset = "0x168299C", VA = "0x10168299C")]
		private List<string> GetTagSEList(string text)
		{
			return null;
		}

		// Token: 0x06005D3F RID: 23871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005645")]
		[Address(RVA = "0x101683E80", Offset = "0x1683E80", VA = "0x101683E80")]
		public ScriptPlayer()
		{
		}

		// Token: 0x04006E3D RID: 28221
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004E2D")]
		private List<ScriptPlayer.ADVCommand> m_CommandList;

		// Token: 0x04006E3E RID: 28222
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E2E")]
		private int m_NowCommandIdx;

		// Token: 0x04006E3F RID: 28223
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E2F")]
		private ADVScriptCommandInherited m_ADVCommandInherited;

		// Token: 0x04006E40 RID: 28224
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E30")]
		private bool m_IsEnd;

		// Token: 0x04006E41 RID: 28225
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E31")]
		private string m_ScriptPath;

		// Token: 0x04006E42 RID: 28226
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E32")]
		private string m_TextDBPath;

		// Token: 0x04006E43 RID: 28227
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004E33")]
		private bool m_IsDonePrepare;

		// Token: 0x04006E44 RID: 28228
		[Cpp2IlInjected.FieldOffset(Offset = "0x41")]
		[Token(Token = "0x4004E34")]
		private bool m_IsError;

		// Token: 0x04006E45 RID: 28229
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004E35")]
		private ABResourceLoader m_Loader;

		// Token: 0x04006E46 RID: 28230
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004E36")]
		private ScriptData m_ScriptDB;

		// Token: 0x04006E47 RID: 28231
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004E37")]
		private ABResourceObjectHandler m_HndlScript;

		// Token: 0x04006E48 RID: 28232
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004E38")]
		private ADVTextDB m_TextDB;

		// Token: 0x04006E49 RID: 28233
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004E39")]
		private ABResourceObjectHandler m_HndlTextDB;

		// Token: 0x04006E4A RID: 28234
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004E3A")]
		private ADVPlayer m_Player;

		// Token: 0x020011FA RID: 4602
		[Token(Token = "0x20012C5")]
		public class ADVCommand
		{
			// Token: 0x06005D40 RID: 23872 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063AA")]
			[Address(RVA = "0x101682934", Offset = "0x1682934", VA = "0x101682934")]
			public ADVCommand()
			{
			}

			// Token: 0x04006E4B RID: 28235
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400733E")]
			public ScriptPlayer.ADVCommand.Param param;

			// Token: 0x020011FB RID: 4603
			[Token(Token = "0x2001356")]
			public class Param
			{
				// Token: 0x06005D41 RID: 23873 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600651E")]
				[Address(RVA = "0x101683E88", Offset = "0x1683E88", VA = "0x101683E88")]
				public Param()
				{
				}

				// Token: 0x04006E4C RID: 28236
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x40075D6")]
				public string FuncName;

				// Token: 0x04006E4D RID: 28237
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x40075D7")]
				public int ArgNum;

				// Token: 0x04006E4E RID: 28238
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x40075D8")]
				public ScriptPlayer.ADVCommand.ArgParam[] argParam;
			}

			// Token: 0x020011FC RID: 4604
			[Token(Token = "0x2001357")]
			public struct ArgParam
			{
				// Token: 0x04006E4F RID: 28239
				[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
				[Token(Token = "0x40075D9")]
				public int ArgType;

				// Token: 0x04006E50 RID: 28240
				[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
				[Token(Token = "0x40075DA")]
				public string value;
			}
		}
	}
}
