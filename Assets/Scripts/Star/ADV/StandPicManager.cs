﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x020011FD RID: 4605
	[Token(Token = "0x2000BF5")]
	[StructLayout(3)]
	public class StandPicManager : MonoBehaviour
	{
		// Token: 0x06005D42 RID: 23874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005646")]
		[Address(RVA = "0x101683E90", Offset = "0x1683E90", VA = "0x101683E90")]
		private void OnDestroy()
		{
		}

		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x06005D43 RID: 23875 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005D5")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005647")]
			[Address(RVA = "0x101683E98", Offset = "0x1683E98", VA = "0x101683E98")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x06005D44 RID: 23876 RVA: 0x0001E5A0 File Offset: 0x0001C7A0
		[Token(Token = "0x170005D6")]
		public bool IsPlayingMotion
		{
			[Token(Token = "0x6005648")]
			[Address(RVA = "0x101683EA0", Offset = "0x1683EA0", VA = "0x101683EA0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x06005D45 RID: 23877 RVA: 0x0001E5B8 File Offset: 0x0001C7B8
		[Token(Token = "0x170005D7")]
		public bool IsMoving
		{
			[Token(Token = "0x6005649")]
			[Address(RVA = "0x101683FD4", Offset = "0x1683FD4", VA = "0x101683FD4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x06005D46 RID: 23878 RVA: 0x0001E5D0 File Offset: 0x0001C7D0
		[Token(Token = "0x170005D8")]
		public bool IsRotate
		{
			[Token(Token = "0x600564A")]
			[Address(RVA = "0x101684104", Offset = "0x1684104", VA = "0x101684104")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06005D47 RID: 23879 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005D9")]
		public ADVPlayer Player
		{
			[Token(Token = "0x600564B")]
			[Address(RVA = "0x10167381C", Offset = "0x167381C", VA = "0x10167381C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06005D48 RID: 23880 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005DA")]
		public RectTransform CanvasRectTransform
		{
			[Token(Token = "0x600564C")]
			[Address(RVA = "0x101684234", Offset = "0x1684234", VA = "0x101684234")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005D49 RID: 23881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600564D")]
		[Address(RVA = "0x10168423C", Offset = "0x168423C", VA = "0x10168423C")]
		public void Prepare(ADVPlayer player)
		{
		}

		// Token: 0x06005D4A RID: 23882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600564E")]
		[Address(RVA = "0x101684334", Offset = "0x1684334", VA = "0x101684334")]
		public void ClearNeedList()
		{
		}

		// Token: 0x06005D4B RID: 23883 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600564F")]
		[Address(RVA = "0x1016843B8", Offset = "0x16843B8", VA = "0x1016843B8")]
		public List<string> GetNeedPoseList(string advCharaID)
		{
			return null;
		}

		// Token: 0x06005D4C RID: 23884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005650")]
		[Address(RVA = "0x1016845B4", Offset = "0x16845B4", VA = "0x1016845B4")]
		public void AddNeedList(int startIdx, string advCharaID, string poseName)
		{
		}

		// Token: 0x06005D4D RID: 23885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005651")]
		[Address(RVA = "0x10168478C", Offset = "0x168478C", VA = "0x10168478C")]
		public void AddNeedFaceList(string advCharaID, eADVFace face)
		{
		}

		// Token: 0x06005D4E RID: 23886 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005652")]
		[Address(RVA = "0x101674418", Offset = "0x1674418", VA = "0x101674418")]
		public StandPicManager.FaceLoadParam GetNeedFaceLoadParam(string advCharaID)
		{
			return null;
		}

		// Token: 0x06005D4F RID: 23887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005653")]
		[Address(RVA = "0x101684A1C", Offset = "0x1684A1C", VA = "0x101684A1C")]
		public void DestroyAllNotNeedStandPic()
		{
		}

		// Token: 0x06005D50 RID: 23888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005654")]
		[Address(RVA = "0x101684E28", Offset = "0x1684E28", VA = "0x101684E28")]
		private void CreateStandPic(string advCharaID)
		{
		}

		// Token: 0x06005D51 RID: 23889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005655")]
		[Address(RVA = "0x1016850F8", Offset = "0x16850F8", VA = "0x1016850F8")]
		public void CreateAllNeedStandPic()
		{
		}

		// Token: 0x06005D52 RID: 23890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005656")]
		[Address(RVA = "0x101675950", Offset = "0x1675950", VA = "0x101675950")]
		public void UnUse(string advCharaID, string poseName)
		{
		}

		// Token: 0x06005D53 RID: 23891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005657")]
		[Address(RVA = "0x101685208", Offset = "0x1685208", VA = "0x101685208")]
		public void UnloadUnusePose()
		{
		}

		// Token: 0x06005D54 RID: 23892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005658")]
		[Address(RVA = "0x1016853D8", Offset = "0x16853D8", VA = "0x1016853D8")]
		public void LoadRecentPicMax()
		{
		}

		// Token: 0x06005D55 RID: 23893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005659")]
		[Address(RVA = "0x1016851C0", Offset = "0x16851C0", VA = "0x1016851C0")]
		private void LoadRecentPic()
		{
		}

		// Token: 0x06005D56 RID: 23894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600565A")]
		[Address(RVA = "0x101675B60", Offset = "0x1675B60", VA = "0x101675B60")]
		public void ForceLoadPic(string advCharaID, string poseName)
		{
		}

		// Token: 0x06005D57 RID: 23895 RVA: 0x0001E5E8 File Offset: 0x0001C7E8
		[Token(Token = "0x600565B")]
		[Address(RVA = "0x101685484", Offset = "0x1685484", VA = "0x101685484")]
		private int UsingPicNum()
		{
			return 0;
		}

		// Token: 0x06005D58 RID: 23896 RVA: 0x0001E600 File Offset: 0x0001C800
		[Token(Token = "0x600565C")]
		[Address(RVA = "0x101685554", Offset = "0x1685554", VA = "0x101685554")]
		private int LoadedPicNum()
		{
			return 0;
		}

		// Token: 0x06005D59 RID: 23897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600565D")]
		[Address(RVA = "0x1016857F0", Offset = "0x16857F0", VA = "0x1016857F0")]
		public void LoadFirstNeedStandPics()
		{
		}

		// Token: 0x06005D5A RID: 23898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600565E")]
		[Address(RVA = "0x1016858E8", Offset = "0x16858E8", VA = "0x1016858E8")]
		public void LoadFirstLoadStandPics(int currentIdx)
		{
		}

		// Token: 0x06005D5B RID: 23899 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600565F")]
		[Address(RVA = "0x101685624", Offset = "0x1685624", VA = "0x101685624")]
		private StandPicManager.StandPicLoadParam GetRecentNeedStandPic()
		{
			return null;
		}

		// Token: 0x06005D5C RID: 23900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005660")]
		[Address(RVA = "0x101685784", Offset = "0x1685784", VA = "0x101685784")]
		private void LoadStandPic(StandPicManager.StandPicLoadParam param)
		{
		}

		// Token: 0x06005D5D RID: 23901 RVA: 0x0001E618 File Offset: 0x0001C818
		[Token(Token = "0x6005661")]
		[Address(RVA = "0x101685928", Offset = "0x1685928", VA = "0x101685928")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x06005D5E RID: 23902 RVA: 0x0001E630 File Offset: 0x0001C830
		[Token(Token = "0x6005662")]
		[Address(RVA = "0x1016859FC", Offset = "0x16859FC", VA = "0x1016859FC")]
		public bool IsDoneApplyPose()
		{
			return default(bool);
		}

		// Token: 0x06005D5F RID: 23903 RVA: 0x0001E648 File Offset: 0x0001C848
		[Token(Token = "0x6005663")]
		[Address(RVA = "0x101685AD0", Offset = "0x1685AD0", VA = "0x101685AD0")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06005D60 RID: 23904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005664")]
		[Address(RVA = "0x101685BA8", Offset = "0x1685BA8", VA = "0x101685BA8")]
		private void Update()
		{
		}

		// Token: 0x06005D61 RID: 23905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005665")]
		[Address(RVA = "0x101685C78", Offset = "0x1685C78", VA = "0x101685C78")]
		public void Destroy()
		{
		}

		// Token: 0x06005D62 RID: 23906 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005666")]
		[Address(RVA = "0x101685DE8", Offset = "0x1685DE8", VA = "0x101685DE8")]
		public List<ADVStandPic> GetStandPicList()
		{
			return null;
		}

		// Token: 0x06005D63 RID: 23907 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005667")]
		[Address(RVA = "0x101679080", Offset = "0x1679080", VA = "0x101679080")]
		public ADVStandPic GetStandPic(string advCharaID)
		{
			return null;
		}

		// Token: 0x06005D64 RID: 23908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005668")]
		[Address(RVA = "0x101685DF0", Offset = "0x1685DF0", VA = "0x101685DF0")]
		public void HideChara(string advCharaID)
		{
		}

		// Token: 0x06005D65 RID: 23909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005669")]
		[Address(RVA = "0x101684D5C", Offset = "0x1684D5C", VA = "0x101684D5C")]
		public void HideAllChara()
		{
		}

		// Token: 0x06005D66 RID: 23910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566A")]
		[Address(RVA = "0x101685ECC", Offset = "0x1685ECC", VA = "0x101685ECC")]
		public void HideAllCharaFade(float sec)
		{
		}

		// Token: 0x06005D67 RID: 23911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566B")]
		[Address(RVA = "0x101676020", Offset = "0x1676020", VA = "0x101676020")]
		public void RemovePicList(ADVStandPic standPic)
		{
		}

		// Token: 0x06005D68 RID: 23912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566C")]
		[Address(RVA = "0x101685FC8", Offset = "0x1685FC8", VA = "0x101685FC8")]
		public void SetCharaEnableRender(string advCharaID, bool flg, float fadesec = 0f)
		{
		}

		// Token: 0x06005D69 RID: 23913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566D")]
		[Address(RVA = "0x101686168", Offset = "0x1686168", VA = "0x101686168")]
		public void SetChara(string advCharaID, eADVStandPosition standPos, float xOffset, float sec, eADVCurveType curveType)
		{
		}

		// Token: 0x06005D6A RID: 23914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566E")]
		[Address(RVA = "0x1016863D8", Offset = "0x16863D8", VA = "0x1016863D8")]
		public void SetCharaScreenSide(string advCharaID, eADVSidePosition side, float xOffset, float sec, eADVCurveType curveType, bool charaOut = false)
		{
		}

		// Token: 0x06005D6B RID: 23915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600566F")]
		[Address(RVA = "0x1016865F8", Offset = "0x16865F8", VA = "0x1016865F8")]
		public void CharaAlignment(float m_Sec, eADVCurveType curveType)
		{
		}

		// Token: 0x06005D6C RID: 23916 RVA: 0x0001E660 File Offset: 0x0001C860
		[Token(Token = "0x6005670")]
		[Address(RVA = "0x1016869A8", Offset = "0x16869A8", VA = "0x1016869A8")]
		public Vector2 GetCharaPosition(string advCharaID)
		{
			return default(Vector2);
		}

		// Token: 0x06005D6D RID: 23917 RVA: 0x0001E678 File Offset: 0x0001C878
		[Token(Token = "0x6005671")]
		[Address(RVA = "0x101686BC8", Offset = "0x1686BC8", VA = "0x101686BC8")]
		public Vector2 GetCharaEmoPosition(string charaID, eADVEmotionPosition position, bool offsetRotateMode)
		{
			return default(Vector2);
		}

		// Token: 0x06005D6E RID: 23918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005672")]
		[Address(RVA = "0x101687038", Offset = "0x1687038", VA = "0x101687038")]
		public void SetAsLastSibling(string advCharaID)
		{
		}

		// Token: 0x06005D6F RID: 23919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005673")]
		[Address(RVA = "0x101687040", Offset = "0x1687040", VA = "0x101687040")]
		protected void SetAsLastSibling(string advCharaID, bool isExecNormalOnly)
		{
		}

		// Token: 0x06005D70 RID: 23920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005674")]
		[Address(RVA = "0x101687418", Offset = "0x1687418", VA = "0x101687418")]
		protected void SetAsFirstSibling(string advCharaID, bool isExecNormalOnly)
		{
		}

		// Token: 0x06005D71 RID: 23921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005675")]
		[Address(RVA = "0x1016877CC", Offset = "0x16877CC", VA = "0x1016877CC")]
		public void CharaPriorityTop(string advCharaID)
		{
		}

		// Token: 0x06005D72 RID: 23922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005676")]
		[Address(RVA = "0x1016877D4", Offset = "0x16877D4", VA = "0x1016877D4")]
		public void CharaPriorityBottom(string advCharaID)
		{
		}

		// Token: 0x06005D73 RID: 23923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005677")]
		[Address(RVA = "0x10168714C", Offset = "0x168714C", VA = "0x10168714C")]
		public void SetAsTopSetGroupLastSibling(string advCharaID)
		{
		}

		// Token: 0x06005D74 RID: 23924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005678")]
		[Address(RVA = "0x101687524", Offset = "0x1687524", VA = "0x101687524")]
		public void SetAsTopSetGroupFirstSibling(string advCharaID)
		{
		}

		// Token: 0x06005D75 RID: 23925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005679")]
		[Address(RVA = "0x101687238", Offset = "0x1687238", VA = "0x101687238")]
		public void SetAsNormalGroupLastSibling(string advCharaID)
		{
		}

		// Token: 0x06005D76 RID: 23926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567A")]
		[Address(RVA = "0x1016875F4", Offset = "0x16875F4", VA = "0x1016875F4")]
		public void SetAsNormalGroupFirstSibling(string advCharaID)
		{
		}

		// Token: 0x06005D77 RID: 23927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567B")]
		[Address(RVA = "0x101687348", Offset = "0x1687348", VA = "0x101687348")]
		public void SetAsBottomSetGroupLastSibling(string advCharaID)
		{
		}

		// Token: 0x06005D78 RID: 23928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567C")]
		[Address(RVA = "0x101687704", Offset = "0x1687704", VA = "0x101687704")]
		public void SetAsBottomSetGroupFirstSibling(string advCharaID)
		{
		}

		// Token: 0x06005D79 RID: 23929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567D")]
		[Address(RVA = "0x101687C94", Offset = "0x1687C94", VA = "0x101687C94")]
		public void SetHDRFactor(string advCharaID, float hdrFactor)
		{
		}

		// Token: 0x06005D7A RID: 23930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567E")]
		[Address(RVA = "0x1016877DC", Offset = "0x16877DC", VA = "0x1016877DC")]
		private void InsertSiblingIndex(int index, ADVStandPic pic)
		{
		}

		// Token: 0x06005D7B RID: 23931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600567F")]
		[Address(RVA = "0x101687D5C", Offset = "0x1687D5C", VA = "0x101687D5C")]
		private void InsertSiblingIndexFront(int index, ADVStandPic pic)
		{
		}

		// Token: 0x06005D7C RID: 23932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005680")]
		[Address(RVA = "0x101687E14", Offset = "0x1687E14", VA = "0x101687E14")]
		private void InsertSiblingIndexBack(int index, ADVStandPic pic)
		{
		}

		// Token: 0x06005D7D RID: 23933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005681")]
		[Address(RVA = "0x101687ECC", Offset = "0x1687ECC", VA = "0x101687ECC")]
		public void CharaPriorityReset(string advCharaID)
		{
		}

		// Token: 0x06005D7E RID: 23934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005682")]
		[Address(RVA = "0x1016878E0", Offset = "0x16878E0", VA = "0x1016878E0")]
		private void RefreshPositionZ()
		{
		}

		// Token: 0x06005D7F RID: 23935 RVA: 0x0001E690 File Offset: 0x0001C890
		[Token(Token = "0x6005683")]
		[Address(RVA = "0x101687A08", Offset = "0x1687A08", VA = "0x101687A08")]
		private int GetPriorityTopSetGroupTailIndex()
		{
			return 0;
		}

		// Token: 0x06005D80 RID: 23936 RVA: 0x0001E6A8 File Offset: 0x0001C8A8
		[Token(Token = "0x6005684")]
		[Address(RVA = "0x101687B44", Offset = "0x1687B44", VA = "0x101687B44")]
		private int GetPriorityNormalGroupHeadIndex()
		{
			return 0;
		}

		// Token: 0x06005D81 RID: 23937 RVA: 0x0001E6C0 File Offset: 0x0001C8C0
		[Token(Token = "0x6005685")]
		[Address(RVA = "0x101687B5C", Offset = "0x1687B5C", VA = "0x101687B5C")]
		private int GetPriorityNormalGroupTailIndex()
		{
			return 0;
		}

		// Token: 0x06005D82 RID: 23938 RVA: 0x0001E6D8 File Offset: 0x0001C8D8
		[Token(Token = "0x6005686")]
		[Address(RVA = "0x101687B74", Offset = "0x1687B74", VA = "0x101687B74")]
		private int GetPriorityBottomSetGroupHeadIndex()
		{
			return 0;
		}

		// Token: 0x06005D83 RID: 23939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005687")]
		[Address(RVA = "0x1016880D8", Offset = "0x16880D8", VA = "0x1016880D8")]
		public void SetPriorityState(string advCharaID, ADVStandPic.ePriorityState state)
		{
		}

		// Token: 0x06005D84 RID: 23940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005688")]
		[Address(RVA = "0x101688048", Offset = "0x1688048", VA = "0x101688048")]
		public void SetPriorityState(ADVStandPic pic, ADVStandPic.ePriorityState state)
		{
		}

		// Token: 0x06005D85 RID: 23941 RVA: 0x0001E6F0 File Offset: 0x0001C8F0
		[Token(Token = "0x6005689")]
		[Address(RVA = "0x101688100", Offset = "0x1688100", VA = "0x101688100")]
		public ADVStandPic.ePriorityState GetPriorityState(string advCharaID)
		{
			return ADVStandPic.ePriorityState.Error;
		}

		// Token: 0x06005D86 RID: 23942 RVA: 0x0001E708 File Offset: 0x0001C908
		[Token(Token = "0x600568A")]
		[Address(RVA = "0x101687FBC", Offset = "0x1687FBC", VA = "0x101687FBC")]
		public ADVStandPic.ePriorityState GetPriorityState(ADVStandPic pic)
		{
			return ADVStandPic.ePriorityState.Error;
		}

		// Token: 0x06005D87 RID: 23943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600568B")]
		[Address(RVA = "0x101688118", Offset = "0x1688118", VA = "0x101688118")]
		public void HighlightTalker([Optional] string advCharaID)
		{
		}

		// Token: 0x06005D88 RID: 23944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600568C")]
		[Address(RVA = "0x101688270", Offset = "0x1688270", VA = "0x101688270")]
		public void HighlightTalkerResetAll()
		{
		}

		// Token: 0x06005D89 RID: 23945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600568D")]
		[Address(RVA = "0x101688350", Offset = "0x1688350", VA = "0x101688350")]
		public void AddNextTalkTalkerHighlightCharacter(string advCharaID)
		{
		}

		// Token: 0x06005D8A RID: 23946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600568E")]
		[Address(RVA = "0x1016883C0", Offset = "0x16883C0", VA = "0x1016883C0")]
		public void Highlight(string advCharaID)
		{
		}

		// Token: 0x06005D8B RID: 23947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600568F")]
		[Address(RVA = "0x101688478", Offset = "0x1688478", VA = "0x101688478")]
		public void HighlightAll()
		{
		}

		// Token: 0x06005D8C RID: 23948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005690")]
		[Address(RVA = "0x10168854C", Offset = "0x168854C", VA = "0x10168854C")]
		public void HighlightDefault(string advCharaID)
		{
		}

		// Token: 0x06005D8D RID: 23949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005691")]
		[Address(RVA = "0x10168860C", Offset = "0x168860C", VA = "0x10168860C")]
		public void HighlightDefaultAll()
		{
		}

		// Token: 0x06005D8E RID: 23950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005692")]
		[Address(RVA = "0x1016886E0", Offset = "0x16886E0", VA = "0x1016886E0")]
		public void Shading(string advCharaID)
		{
		}

		// Token: 0x06005D8F RID: 23951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005693")]
		[Address(RVA = "0x101688798", Offset = "0x1688798", VA = "0x101688798")]
		public void ShadingAll()
		{
		}

		// Token: 0x06005D90 RID: 23952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005694")]
		[Address(RVA = "0x10168886C", Offset = "0x168886C", VA = "0x10168886C")]
		public void AlphaToAll(uint alpha, float duration, eADVCurveType easeType)
		{
		}

		// Token: 0x06005D91 RID: 23953 RVA: 0x0001E720 File Offset: 0x0001C920
		[Token(Token = "0x6005695")]
		[Address(RVA = "0x101686F88", Offset = "0x1686F88", VA = "0x101686F88")]
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			return default(Vector3);
		}

		// Token: 0x06005D92 RID: 23954 RVA: 0x0001E738 File Offset: 0x0001C938
		[Token(Token = "0x6005696")]
		[Address(RVA = "0x101688958", Offset = "0x1688958", VA = "0x101688958")]
		public bool IsMovingChara(string charaId)
		{
			return default(bool);
		}

		// Token: 0x06005D93 RID: 23955 RVA: 0x0001E750 File Offset: 0x0001C950
		[Token(Token = "0x6005697")]
		[Address(RVA = "0x101688ACC", Offset = "0x1688ACC", VA = "0x101688ACC")]
		public bool IsRotateChara(string charaId)
		{
			return default(bool);
		}

		// Token: 0x06005D94 RID: 23956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005698")]
		[Address(RVA = "0x101688C40", Offset = "0x1688C40", VA = "0x101688C40")]
		public StandPicManager()
		{
		}

		// Token: 0x04006E51 RID: 28241
		[Token(Token = "0x4004E3B")]
		public const int STAND_PIC_FIRST_NUM = 10;

		// Token: 0x04006E52 RID: 28242
		[Token(Token = "0x4004E3C")]
		public const int STAND_PIC_POSE_MAX = 15;

		// Token: 0x04006E53 RID: 28243
		[Token(Token = "0x4004E3D")]
		public const float ZSPACE = 100f;

		// Token: 0x04006E54 RID: 28244
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004E3E")]
		private List<StandPicManager.StandPicLoadParam> m_NeedList;

		// Token: 0x04006E55 RID: 28245
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004E3F")]
		private List<StandPicManager.FaceLoadParam> m_FaceNeedList;

		// Token: 0x04006E56 RID: 28246
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004E40")]
		[SerializeField]
		private ADVStandPic m_StandPicPrefab;

		// Token: 0x04006E57 RID: 28247
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004E41")]
		private List<ADVStandPic> m_InstList;

		// Token: 0x04006E58 RID: 28248
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004E42")]
		private List<ADVStandPic> m_PicList;

		// Token: 0x04006E59 RID: 28249
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004E43")]
		[SerializeField]
		private RectTransform m_CanvasRectTransform;

		// Token: 0x04006E5A RID: 28250
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004E44")]
		[SerializeField]
		private Material m_PicMaterialBase;

		// Token: 0x04006E5B RID: 28251
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004E45")]
		private int m_PosePicMax;

		// Token: 0x04006E5C RID: 28252
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004E46")]
		private RectTransform m_RectTransform;

		// Token: 0x04006E5D RID: 28253
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004E47")]
		private ADVPlayer m_Player;

		// Token: 0x04006E5E RID: 28254
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004E48")]
		private List<string> m_NextTalkTalkerHighlightCharacters;

		// Token: 0x020011FE RID: 4606
		[Token(Token = "0x20012C6")]
		private class StandPicLoadParam
		{
			// Token: 0x06005D95 RID: 23957 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063AB")]
			[Address(RVA = "0x101688CE0", Offset = "0x1688CE0", VA = "0x101688CE0")]
			public StandPicLoadParam(string advCharaId, string poseName)
			{
			}

			// Token: 0x06005D96 RID: 23958 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063AC")]
			[Address(RVA = "0x101688D88", Offset = "0x1688D88", VA = "0x101688D88")]
			public void AddUseIdx(int useStartIdx)
			{
			}

			// Token: 0x06005D97 RID: 23959 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063AD")]
			[Address(RVA = "0x101688E58", Offset = "0x1688E58", VA = "0x101688E58")]
			public void RemoveRecentUseIdx()
			{
			}

			// Token: 0x06005D98 RID: 23960 RVA: 0x0001E768 File Offset: 0x0001C968
			[Token(Token = "0x60063AE")]
			[Address(RVA = "0x101688EBC", Offset = "0x1688EBC", VA = "0x101688EBC")]
			public int GetUseIdxListNum()
			{
				return 0;
			}

			// Token: 0x06005D99 RID: 23961 RVA: 0x0001E780 File Offset: 0x0001C980
			[Token(Token = "0x60063AF")]
			[Address(RVA = "0x101688F1C", Offset = "0x1688F1C", VA = "0x101688F1C")]
			public int GetRecentIdx()
			{
				return 0;
			}

			// Token: 0x04006E5F RID: 28255
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400733F")]
			public string m_AdvCharaID;

			// Token: 0x04006E60 RID: 28256
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007340")]
			public string m_PoseName;

			// Token: 0x04006E61 RID: 28257
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007341")]
			private List<int> m_UseIdxList;

			// Token: 0x04006E62 RID: 28258
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007342")]
			public bool[] m_UseFaces;

			// Token: 0x04006E63 RID: 28259
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4007343")]
			public bool m_IsLoaded;

			// Token: 0x04006E64 RID: 28260
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4007344")]
			public int m_UseCount;

			// Token: 0x04006E65 RID: 28261
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4007345")]
			public int m_UnuseIdx;
		}

		// Token: 0x020011FF RID: 4607
		[Token(Token = "0x20012C7")]
		public class FaceLoadParam
		{
			// Token: 0x06005D9A RID: 23962 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60063B0")]
			[Address(RVA = "0x1016849BC", Offset = "0x16849BC", VA = "0x1016849BC")]
			public FaceLoadParam()
			{
			}

			// Token: 0x04006E66 RID: 28262
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007346")]
			public string m_AdvCharaID;

			// Token: 0x04006E67 RID: 28263
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007347")]
			public bool[] m_Faces;
		}
	}
}
