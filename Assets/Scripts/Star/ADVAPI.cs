﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;

namespace Star
{
	// Token: 0x02000355 RID: 853
	[Token(Token = "0x20002D2")]
	[StructLayout(3)]
	public class ADVAPI
	{
		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06000BA3 RID: 2979 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000BA4 RID: 2980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000008")]
		private event Action m_OnResponse_AdvAdd
		{
			[Token(Token = "0x6000AD1")]
			[Address(RVA = "0x101688FBC", Offset = "0x1688FBC", VA = "0x101688FBC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000AD2")]
			[Address(RVA = "0x1016890C8", Offset = "0x16890C8", VA = "0x1016890C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06000BA5 RID: 2981 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000BA6 RID: 2982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000009")]
		private event Action m_OnResponse_AdvQuestClear
		{
			[Token(Token = "0x6000AD3")]
			[Address(RVA = "0x1016891D4", Offset = "0x16891D4", VA = "0x1016891D4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000AD4")]
			[Address(RVA = "0x1016892E0", Offset = "0x16892E0", VA = "0x1016892E0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06000BA7 RID: 2983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AD5")]
		[Address(RVA = "0x1016893EC", Offset = "0x16893EC", VA = "0x1016893EC")]
		public void Request_AdvAdd(int advID, Action callback, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000BA8 RID: 2984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AD6")]
		[Address(RVA = "0x10168957C", Offset = "0x168957C", VA = "0x10168957C")]
		private void OnResponse_AdvAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000BA9 RID: 2985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AD7")]
		[Address(RVA = "0x1016897D0", Offset = "0x16897D0", VA = "0x1016897D0")]
		public void Request_QuestSuccessAdvOnly(Action callback)
		{
		}

		// Token: 0x06000BAA RID: 2986 RVA: 0x00004848 File Offset: 0x00002A48
		[Token(Token = "0x6000AD8")]
		[Address(RVA = "0x101689898", Offset = "0x1689898", VA = "0x101689898")]
		private bool OnResponse_QuestSuccessAdvOnly(MeigewwwParam wwwParam, Questlogcomplete param)
		{
			return default(bool);
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AD9")]
		[Address(RVA = "0x1016898FC", Offset = "0x16898FC", VA = "0x1016898FC")]
		public ADVAPI()
		{
		}

		// Token: 0x04000CA6 RID: 3238
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A06")]
		private int m_AdvID;
	}
}
