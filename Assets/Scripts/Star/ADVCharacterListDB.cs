﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000359 RID: 857
	[Token(Token = "0x20002D6")]
	[StructLayout(3)]
	public class ADVCharacterListDB : ScriptableObject
	{
		// Token: 0x06000BAC RID: 2988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ADA")]
		[Address(RVA = "0x10168990C", Offset = "0x168990C", VA = "0x10168990C")]
		public ADVCharacterListDB()
		{
		}

		// Token: 0x04000CC3 RID: 3267
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A23")]
		public ADVCharacterListDB_Param[] m_Params;
	}
}
