﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000357 RID: 855
	[Token(Token = "0x20002D4")]
	[Serializable]
	[StructLayout(0, Size = 80)]
	public struct ADVCharacterListDB_Data
	{
		// Token: 0x04000CAC RID: 3244
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A0C")]
		public int m_PoseID;

		// Token: 0x04000CAD RID: 3245
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A0D")]
		public string m_PoseName;

		// Token: 0x04000CAE RID: 3246
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A0E")]
		public float m_OffsetX;

		// Token: 0x04000CAF RID: 3247
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000A0F")]
		public float m_OffsetY;

		// Token: 0x04000CB0 RID: 3248
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A10")]
		public sbyte m_DummyStandPic;

		// Token: 0x04000CB1 RID: 3249
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000A11")]
		public int m_FacePattern;

		// Token: 0x04000CB2 RID: 3250
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A12")]
		public float m_FacePivotX;

		// Token: 0x04000CB3 RID: 3251
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000A13")]
		public float m_FacePivotY;

		// Token: 0x04000CB4 RID: 3252
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A14")]
		public float m_FacePicX;

		// Token: 0x04000CB5 RID: 3253
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000A15")]
		public float m_FacePicY;

		// Token: 0x04000CB6 RID: 3254
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A16")]
		public int m_FaceReferenceImageType;

		// Token: 0x04000CB7 RID: 3255
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000A17")]
		public float m_FaceX;

		// Token: 0x04000CB8 RID: 3256
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A18")]
		public float m_FaceY;

		// Token: 0x04000CB9 RID: 3257
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000A19")]
		public float m_EmoOffsetX;

		// Token: 0x04000CBA RID: 3258
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000A1A")]
		public float m_EmoOffsetY;

		// Token: 0x04000CBB RID: 3259
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000A1B")]
		public float m_FootOffsetX;

		// Token: 0x04000CBC RID: 3260
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000A1C")]
		public float m_FootOffsetY;
	}
}
