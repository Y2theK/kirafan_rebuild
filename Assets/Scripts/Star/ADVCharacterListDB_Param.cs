﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000358 RID: 856
	[Token(Token = "0x20002D5")]
	[Serializable]
	[StructLayout(0)]
	public struct ADVCharacterListDB_Param
	{
		// Token: 0x04000CBD RID: 3261
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A1D")]
		public string m_ADVCharaID;

		// Token: 0x04000CBE RID: 3262
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A1E")]
		public string m_ResourceBaseName;

		// Token: 0x04000CBF RID: 3263
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A1F")]
		public string m_DisplayName;

		// Token: 0x04000CC0 RID: 3264
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A20")]
		public int m_NamedType;

		// Token: 0x04000CC1 RID: 3265
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A21")]
		public string m_CueSheet;

		// Token: 0x04000CC2 RID: 3266
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A22")]
		public ADVCharacterListDB_Data[] m_Datas;
	}
}
