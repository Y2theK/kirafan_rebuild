﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200035A RID: 858
	[Token(Token = "0x20002D7")]
	[StructLayout(3)]
	public class ADVDefine : MonoBehaviour
	{
		// Token: 0x06000BAD RID: 2989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ADB")]
		[Address(RVA = "0x101689A48", Offset = "0x1689A48", VA = "0x101689A48")]
		public ADVDefine()
		{
		}

		// Token: 0x04000CC4 RID: 3268
		[Token(Token = "0x4000A24")]
		public const float DEFAULT_FADE_SEC = 1f;

		// Token: 0x04000CC5 RID: 3269
		[Token(Token = "0x4000A25")]
		public static readonly float[] CHARA_POSITION;
	}
}
