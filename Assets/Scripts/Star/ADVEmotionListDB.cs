﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200035C RID: 860
	[Token(Token = "0x20002D9")]
	[StructLayout(3)]
	public class ADVEmotionListDB : ScriptableObject
	{
		// Token: 0x06000BAF RID: 2991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ADD")]
		[Address(RVA = "0x101689ACC", Offset = "0x1689ACC", VA = "0x101689ACC")]
		public ADVEmotionListDB()
		{
		}

		// Token: 0x04000CCA RID: 3274
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A2A")]
		public ADVEmotionListDB_Param[] m_Params;
	}
}
