﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200035B RID: 859
	[Token(Token = "0x20002D8")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct ADVEmotionListDB_Param
	{
		// Token: 0x04000CC6 RID: 3270
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A26")]
		public string m_EmotionID;

		// Token: 0x04000CC7 RID: 3271
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A27")]
		public string m_EffectID;

		// Token: 0x04000CC8 RID: 3272
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A28")]
		public int m_DefaultPosition;

		// Token: 0x04000CC9 RID: 3273
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000A29")]
		public sbyte m_Loop;
	}
}
