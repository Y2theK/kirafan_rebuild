﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004A6 RID: 1190
	[Token(Token = "0x200039E")]
	[StructLayout(3)]
	public class ADVLibraryListDB : ScriptableObject
	{
		// Token: 0x060013F5 RID: 5109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AA")]
		[Address(RVA = "0x101689AD4", Offset = "0x1689AD4", VA = "0x101689AD4")]
		public ADVLibraryListDB()
		{
		}

		// Token: 0x04001629 RID: 5673
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400100F")]
		public ADVLibraryListDB_Param[] m_Params;
	}
}
