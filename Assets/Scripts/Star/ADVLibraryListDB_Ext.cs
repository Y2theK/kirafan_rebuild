﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004F7 RID: 1271
	[Token(Token = "0x20003ED")]
	[StructLayout(3)]
	public static class ADVLibraryListDB_Ext
	{
		// Token: 0x0600150E RID: 5390 RVA: 0x00008C70 File Offset: 0x00006E70
		[Token(Token = "0x60013C3")]
		[Address(RVA = "0x101689ADC", Offset = "0x1689ADC", VA = "0x101689ADC")]
		public static ADVLibraryListDB_Param GetParam(this ADVLibraryListDB self, int advLibID)
		{
			return default(ADVLibraryListDB_Param);
		}
	}
}
