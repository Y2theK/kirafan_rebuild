﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004A5 RID: 1189
	[Token(Token = "0x200039D")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct ADVLibraryListDB_Param
	{
		// Token: 0x04001625 RID: 5669
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400100B")]
		public int m_LibraryListID;

		// Token: 0x04001626 RID: 5670
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400100C")]
		public int m_Category;

		// Token: 0x04001627 RID: 5671
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400100D")]
		public int m_Part;

		// Token: 0x04001628 RID: 5672
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400100E")]
		public string m_ListName;
	}
}
