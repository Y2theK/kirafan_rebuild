﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004A9 RID: 1193
	[Token(Token = "0x20003A1")]
	[StructLayout(3)]
	public class ADVListDB : ScriptableObject
	{
		// Token: 0x060013F6 RID: 5110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AB")]
		[Address(RVA = "0x101689BE4", Offset = "0x1689BE4", VA = "0x101689BE4")]
		public ADVListDB()
		{
		}

		// Token: 0x04001645 RID: 5701
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400102B")]
		public ADVListDB_Param[] m_Params;
	}
}
