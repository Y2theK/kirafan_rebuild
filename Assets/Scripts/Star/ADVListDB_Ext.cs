﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004F8 RID: 1272
	[Token(Token = "0x20003EE")]
	[StructLayout(3)]
	public static class ADVListDB_Ext
	{
		// Token: 0x0600150F RID: 5391 RVA: 0x00008C88 File Offset: 0x00006E88
		[Token(Token = "0x60013C4")]
		[Address(RVA = "0x101689BEC", Offset = "0x1689BEC", VA = "0x101689BEC")]
		public static ADVListDB_Param GetParam(this ADVListDB self, int advID)
		{
			return default(ADVListDB_Param);
		}
	}
}
