﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004A8 RID: 1192
	[Token(Token = "0x20003A0")]
	[Serializable]
	[StructLayout(0, Size = 120)]
	public struct ADVListDB_Param
	{
		// Token: 0x04001634 RID: 5684
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400101A")]
		public int m_AdvID;

		// Token: 0x04001635 RID: 5685
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400101B")]
		public int m_Category;

		// Token: 0x04001636 RID: 5686
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400101C")]
		public string m_ScriptName;

		// Token: 0x04001637 RID: 5687
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400101D")]
		public string m_ScriptTextName;

		// Token: 0x04001638 RID: 5688
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400101E")]
		public string m_CueSheet;

		// Token: 0x04001639 RID: 5689
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400101F")]
		public uint m_CRIVersion;

		// Token: 0x0400163A RID: 5690
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001020")]
		public string m_MovieFileName;

		// Token: 0x0400163B RID: 5691
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001021")]
		public string m_ComicGroupName;

		// Token: 0x0400163C RID: 5692
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001022")]
		public string m_ComicName;

		// Token: 0x0400163D RID: 5693
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001023")]
		public string m_Title;

		// Token: 0x0400163E RID: 5694
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001024")]
		public int m_LibraryID;

		// Token: 0x0400163F RID: 5695
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4001025")]
		public int[] m_NamedType;

		// Token: 0x04001640 RID: 5696
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4001026")]
		public int m_CharaID;

		// Token: 0x04001641 RID: 5697
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4001027")]
		public int[] m_LimitNamedType;

		// Token: 0x04001642 RID: 5698
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4001028")]
		public int m_LimitFriendShipLv;

		// Token: 0x04001643 RID: 5699
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4001029")]
		public int m_PresentGemNum;

		// Token: 0x04001644 RID: 5700
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400102A")]
		public int m_PresentMessageID;
	}
}
