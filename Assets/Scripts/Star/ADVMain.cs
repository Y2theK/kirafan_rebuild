﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;

namespace Star
{
	// Token: 0x0200076B RID: 1899
	[Token(Token = "0x20005B6")]
	[StructLayout(3)]
	public class ADVMain : GameStateMain
	{
		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06001C65 RID: 7269 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000204")]
		public ADVPlayer Player
		{
			[Token(Token = "0x60019E3")]
			[Address(RVA = "0x101689D1C", Offset = "0x1689D1C", VA = "0x101689D1C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06001C66 RID: 7270 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000205")]
		public ADVVoiceDownloadWindow DownloadWindow
		{
			[Token(Token = "0x60019E4")]
			[Address(RVA = "0x101689D24", Offset = "0x1689D24", VA = "0x101689D24")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001C67 RID: 7271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019E5")]
		[Address(RVA = "0x101689D2C", Offset = "0x1689D2C", VA = "0x101689D2C")]
		private void Start()
		{
		}

		// Token: 0x06001C68 RID: 7272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019E6")]
		[Address(RVA = "0x101689D38", Offset = "0x1689D38", VA = "0x101689D38", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001C69 RID: 7273 RVA: 0x0000CBE8 File Offset: 0x0000ADE8
		[Token(Token = "0x60019E7")]
		[Address(RVA = "0x101689E18", Offset = "0x1689E18", VA = "0x101689E18", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001C6A RID: 7274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019E8")]
		[Address(RVA = "0x101689E68", Offset = "0x1689E68", VA = "0x101689E68", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001C6B RID: 7275 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60019E9")]
		[Address(RVA = "0x101689E70", Offset = "0x1689E70", VA = "0x101689E70", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001C6C RID: 7276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019EA")]
		[Address(RVA = "0x10168A004", Offset = "0x168A004", VA = "0x10168A004")]
		public ADVMain()
		{
		}

		// Token: 0x04002C9B RID: 11419
		[Token(Token = "0x4002359")]
		public const int STATE_INIT = 0;

		// Token: 0x04002C9C RID: 11420
		[Token(Token = "0x400235A")]
		public const int STATE_PLAY = 1;

		// Token: 0x04002C9D RID: 11421
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400235B")]
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04002C9E RID: 11422
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400235C")]
		[SerializeField]
		private ADVVoiceDownloadWindow m_DownloadWindow;
	}
}
