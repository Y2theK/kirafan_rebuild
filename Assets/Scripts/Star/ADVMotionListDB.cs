﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000360 RID: 864
	[Token(Token = "0x20002DD")]
	[StructLayout(3)]
	public class ADVMotionListDB : ScriptableObject
	{
		// Token: 0x06000BB0 RID: 2992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ADE")]
		[Address(RVA = "0x10168A00C", Offset = "0x168A00C", VA = "0x10168A00C")]
		public ADVMotionListDB()
		{
		}

		// Token: 0x04000CD7 RID: 3287
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A37")]
		public ADVMotionListDB_Param[] m_Params;
	}
}
