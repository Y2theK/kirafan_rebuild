﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200035E RID: 862
	[Token(Token = "0x20002DB")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct ADVMotionListDB_Data
	{
		// Token: 0x04000CCF RID: 3279
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A2F")]
		public int m_Action;

		// Token: 0x04000CD0 RID: 3280
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000A30")]
		public float m_X;

		// Token: 0x04000CD1 RID: 3281
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A31")]
		public float m_Y;

		// Token: 0x04000CD2 RID: 3282
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4000A32")]
		public float m_Duration;

		// Token: 0x04000CD3 RID: 3283
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A33")]
		public int m_EaseType;

		// Token: 0x04000CD4 RID: 3284
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000A34")]
		public sbyte m_WaitEnd;
	}
}
