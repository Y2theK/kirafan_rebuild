﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200035F RID: 863
	[Token(Token = "0x20002DC")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct ADVMotionListDB_Param
	{
		// Token: 0x04000CD5 RID: 3285
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A35")]
		public string m_MotionID;

		// Token: 0x04000CD6 RID: 3286
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A36")]
		public ADVMotionListDB_Data[] m_Datas;
	}
}
