﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200076C RID: 1900
	[Token(Token = "0x20005B7")]
	[StructLayout(3)]
	public class ADVState : GameStateBase
	{
		// Token: 0x06001C6D RID: 7277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019EB")]
		[Address(RVA = "0x10168A014", Offset = "0x168A014", VA = "0x10168A014")]
		public ADVState(ADVMain owner)
		{
		}

		// Token: 0x06001C6E RID: 7278 RVA: 0x0000CC00 File Offset: 0x0000AE00
		[Token(Token = "0x60019EC")]
		[Address(RVA = "0x10168A048", Offset = "0x168A048", VA = "0x10168A048", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001C6F RID: 7279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019ED")]
		[Address(RVA = "0x10168A050", Offset = "0x168A050", VA = "0x10168A050", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001C70 RID: 7280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019EE")]
		[Address(RVA = "0x10168A054", Offset = "0x168A054", VA = "0x10168A054", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001C71 RID: 7281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019EF")]
		[Address(RVA = "0x10168A058", Offset = "0x168A058", VA = "0x10168A058", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001C72 RID: 7282 RVA: 0x0000CC18 File Offset: 0x0000AE18
		[Token(Token = "0x60019F0")]
		[Address(RVA = "0x10168A05C", Offset = "0x168A05C", VA = "0x10168A05C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001C73 RID: 7283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F1")]
		[Address(RVA = "0x10168A064", Offset = "0x168A064", VA = "0x10168A064", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002C9F RID: 11423
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400235D")]
		protected ADVMain m_Owner;

		// Token: 0x04002CA0 RID: 11424
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400235E")]
		protected int m_NextState;
	}
}
