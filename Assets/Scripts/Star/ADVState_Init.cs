﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x0200076D RID: 1901
	[Token(Token = "0x20005B8")]
	[StructLayout(3)]
	public class ADVState_Init : ADVState
	{
		// Token: 0x06001C74 RID: 7284 RVA: 0x0000CC30 File Offset: 0x0000AE30
		[Token(Token = "0x60019F2")]
		[Address(RVA = "0x10168A068", Offset = "0x168A068", VA = "0x10168A068")]
		private bool ExecFullVoiceDL()
		{
			return default(bool);
		}

		// Token: 0x06001C75 RID: 7285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F3")]
		[Address(RVA = "0x101689F44", Offset = "0x1689F44", VA = "0x101689F44")]
		public ADVState_Init(ADVMain owner)
		{
		}

		// Token: 0x06001C76 RID: 7286 RVA: 0x0000CC48 File Offset: 0x0000AE48
		[Token(Token = "0x60019F4")]
		[Address(RVA = "0x10168A088", Offset = "0x168A088", VA = "0x10168A088", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001C77 RID: 7287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F5")]
		[Address(RVA = "0x10168A090", Offset = "0x168A090", VA = "0x10168A090", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001C78 RID: 7288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F6")]
		[Address(RVA = "0x10168A098", Offset = "0x168A098", VA = "0x10168A098", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001C79 RID: 7289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F7")]
		[Address(RVA = "0x10168A09C", Offset = "0x168A09C", VA = "0x10168A09C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001C7A RID: 7290 RVA: 0x0000CC60 File Offset: 0x0000AE60
		[Token(Token = "0x60019F8")]
		[Address(RVA = "0x10168A0A0", Offset = "0x168A0A0", VA = "0x10168A0A0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001C7B RID: 7291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019F9")]
		[Address(RVA = "0x10168B9B0", Offset = "0x168B9B0", VA = "0x10168B9B0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001C7C RID: 7292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FA")]
		[Address(RVA = "0x10168A1D4", Offset = "0x168A1D4", VA = "0x10168A1D4")]
		private void Step_PrepareDB()
		{
		}

		// Token: 0x06001C7D RID: 7293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FB")]
		[Address(RVA = "0x10168A32C", Offset = "0x168A32C", VA = "0x10168A32C")]
		private void Step_PrepareDBWait()
		{
		}

		// Token: 0x06001C7E RID: 7294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FC")]
		[Address(RVA = "0x10168A3AC", Offset = "0x168A3AC", VA = "0x10168A3AC")]
		private void Step_CheckFullVoiceDLFlg()
		{
		}

		// Token: 0x06001C7F RID: 7295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FD")]
		[Address(RVA = "0x10168A580", Offset = "0x168A580", VA = "0x10168A580")]
		private void Step_FullVoiceSizeCheck()
		{
		}

		// Token: 0x06001C80 RID: 7296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FE")]
		[Address(RVA = "0x10168A780", Offset = "0x168A780", VA = "0x10168A780")]
		private void Step_FullVoiceSizeCheckWait()
		{
		}

		// Token: 0x06001C81 RID: 7297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019FF")]
		[Address(RVA = "0x10168A964", Offset = "0x168A964", VA = "0x10168A964")]
		private void Step_FullVoiceConfirmWait()
		{
		}

		// Token: 0x06001C82 RID: 7298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A00")]
		[Address(RVA = "0x10168AAC0", Offset = "0x168AAC0", VA = "0x10168AAC0")]
		private void Step_Prepare()
		{
		}

		// Token: 0x06001C83 RID: 7299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A01")]
		[Address(RVA = "0x10168AB84", Offset = "0x168AB84", VA = "0x10168AB84")]
		private void Step_PrepareScriptWait()
		{
		}

		// Token: 0x06001C84 RID: 7300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A02")]
		[Address(RVA = "0x10168AC68", Offset = "0x168AC68", VA = "0x10168AC68")]
		private void Step_SizeCheck()
		{
		}

		// Token: 0x06001C85 RID: 7301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A03")]
		[Address(RVA = "0x10168AD38", Offset = "0x168AD38", VA = "0x10168AD38")]
		private void Step_SizeCheckWait()
		{
		}

		// Token: 0x06001C86 RID: 7302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A04")]
		[Address(RVA = "0x10168AD70", Offset = "0x168AD70", VA = "0x10168AD70")]
		private void Step_SizeConfirm()
		{
		}

		// Token: 0x06001C87 RID: 7303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A05")]
		[Address(RVA = "0x10168B25C", Offset = "0x168B25C", VA = "0x10168B25C")]
		private void Step_FullVoiceDLStart()
		{
		}

		// Token: 0x06001C88 RID: 7304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A06")]
		[Address(RVA = "0x10168B49C", Offset = "0x168B49C", VA = "0x10168B49C")]
		private void Step_FullVoiceDLWait()
		{
		}

		// Token: 0x06001C89 RID: 7305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A07")]
		[Address(RVA = "0x10168B58C", Offset = "0x168B58C", VA = "0x10168B58C")]
		private void Step_FullVoiceDLError()
		{
		}

		// Token: 0x06001C8A RID: 7306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A08")]
		[Address(RVA = "0x10168B680", Offset = "0x168B680", VA = "0x10168B680")]
		private void Step_LoadStart()
		{
		}

		// Token: 0x06001C8B RID: 7307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A09")]
		[Address(RVA = "0x10168B7D4", Offset = "0x168B7D4", VA = "0x10168B7D4")]
		private void Step_LoadWait()
		{
		}

		// Token: 0x06001C8C RID: 7308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A0A")]
		[Address(RVA = "0x10168B8F8", Offset = "0x168B8F8", VA = "0x10168B8F8")]
		private void Step_LoadCloseWait()
		{
		}

		// Token: 0x04002CA1 RID: 11425
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400235F")]
		private ADVState_Init.eStep m_Step;

		// Token: 0x04002CA2 RID: 11426
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002360")]
		private bool m_ExecFullVoice;

		// Token: 0x04002CA3 RID: 11427
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4002361")]
		private bool m_ExistFullVoice;

		// Token: 0x04002CA4 RID: 11428
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002362")]
		private MeigeResource.DLSizeHandle m_ABDLSizeHndl;

		// Token: 0x04002CA5 RID: 11429
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002363")]
		private SoundDLSizeCheck m_FullVoiceDLSizeHndl;

		// Token: 0x0200076E RID: 1902
		[Token(Token = "0x2000E86")]
		private enum eStep
		{
			// Token: 0x04002CA7 RID: 11431
			[Token(Token = "0x4005CB9")]
			None = -1,
			// Token: 0x04002CA8 RID: 11432
			[Token(Token = "0x4005CBA")]
			PrepareDB,
			// Token: 0x04002CA9 RID: 11433
			[Token(Token = "0x4005CBB")]
			PrepareDBWait,
			// Token: 0x04002CAA RID: 11434
			[Token(Token = "0x4005CBC")]
			CheckFullVoiceDLFlg,
			// Token: 0x04002CAB RID: 11435
			[Token(Token = "0x4005CBD")]
			FullVoiceSizeCheck,
			// Token: 0x04002CAC RID: 11436
			[Token(Token = "0x4005CBE")]
			FullVoiceSizeCheckWait,
			// Token: 0x04002CAD RID: 11437
			[Token(Token = "0x4005CBF")]
			FullVoiceConfirm,
			// Token: 0x04002CAE RID: 11438
			[Token(Token = "0x4005CC0")]
			FullVoiceConfirmWait,
			// Token: 0x04002CAF RID: 11439
			[Token(Token = "0x4005CC1")]
			Prepare,
			// Token: 0x04002CB0 RID: 11440
			[Token(Token = "0x4005CC2")]
			PrepareScriptWait,
			// Token: 0x04002CB1 RID: 11441
			[Token(Token = "0x4005CC3")]
			SizeCheck,
			// Token: 0x04002CB2 RID: 11442
			[Token(Token = "0x4005CC4")]
			SizeCheckWait,
			// Token: 0x04002CB3 RID: 11443
			[Token(Token = "0x4005CC5")]
			SizeConfirm,
			// Token: 0x04002CB4 RID: 11444
			[Token(Token = "0x4005CC6")]
			SizeConfirmWait,
			// Token: 0x04002CB5 RID: 11445
			[Token(Token = "0x4005CC7")]
			FullVoiceDLStart,
			// Token: 0x04002CB6 RID: 11446
			[Token(Token = "0x4005CC8")]
			FullVoiceDLWait,
			// Token: 0x04002CB7 RID: 11447
			[Token(Token = "0x4005CC9")]
			FullVoiceDLError,
			// Token: 0x04002CB8 RID: 11448
			[Token(Token = "0x4005CCA")]
			LoadStart,
			// Token: 0x04002CB9 RID: 11449
			[Token(Token = "0x4005CCB")]
			LoadWait,
			// Token: 0x04002CBA RID: 11450
			[Token(Token = "0x4005CCC")]
			LoadCloseWait,
			// Token: 0x04002CBB RID: 11451
			[Token(Token = "0x4005CCD")]
			End
		}
	}
}
