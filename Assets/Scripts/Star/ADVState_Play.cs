﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200076F RID: 1903
	[Token(Token = "0x20005B9")]
	[StructLayout(3)]
	public class ADVState_Play : ADVState
	{
		// Token: 0x06001C8E RID: 7310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A0C")]
		[Address(RVA = "0x101689F78", Offset = "0x1689F78", VA = "0x101689F78")]
		public ADVState_Play(ADVMain owner)
		{
		}

		// Token: 0x06001C8F RID: 7311 RVA: 0x0000CC78 File Offset: 0x0000AE78
		[Token(Token = "0x6001A0D")]
		[Address(RVA = "0x10168BC94", Offset = "0x168BC94", VA = "0x10168BC94", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001C90 RID: 7312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A0E")]
		[Address(RVA = "0x10168BC9C", Offset = "0x168BC9C", VA = "0x10168BC9C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001C91 RID: 7313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A0F")]
		[Address(RVA = "0x10168BCA4", Offset = "0x168BCA4", VA = "0x10168BCA4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001C92 RID: 7314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A10")]
		[Address(RVA = "0x10168BCA8", Offset = "0x168BCA8", VA = "0x10168BCA8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001C93 RID: 7315 RVA: 0x0000CC90 File Offset: 0x0000AE90
		[Token(Token = "0x6001A11")]
		[Address(RVA = "0x10168BCAC", Offset = "0x168BCAC", VA = "0x10168BCAC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001C94 RID: 7316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A12")]
		[Address(RVA = "0x10168C93C", Offset = "0x168C93C", VA = "0x10168C93C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001C95 RID: 7317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A13")]
		[Address(RVA = "0x10168C940", Offset = "0x168C940", VA = "0x10168C940")]
		private void OnResponse_AdvAdd()
		{
		}

		// Token: 0x06001C96 RID: 7318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A14")]
		[Address(RVA = "0x10168C94C", Offset = "0x168C94C", VA = "0x10168C94C")]
		private void OnCompleteTutorialQuest()
		{
		}

		// Token: 0x04002CBC RID: 11452
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002364")]
		private ADVState_Play.eStep m_Step;

		// Token: 0x04002CBD RID: 11453
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002365")]
		private ADVAPI m_AdvAPI;

		// Token: 0x04002CBE RID: 11454
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002366")]
		private RewardPlayer.ADVReward m_ADVReward;

		// Token: 0x04002CBF RID: 11455
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002367")]
		private bool m_IsFirstPlay;

		// Token: 0x02000770 RID: 1904
		[Token(Token = "0x2000E87")]
		private enum eStep
		{
			// Token: 0x04002CC1 RID: 11457
			[Token(Token = "0x4005CCF")]
			None = -1,
			// Token: 0x04002CC2 RID: 11458
			[Token(Token = "0x4005CD0")]
			Play,
			// Token: 0x04002CC3 RID: 11459
			[Token(Token = "0x4005CD1")]
			FadeOut_Wait,
			// Token: 0x04002CC4 RID: 11460
			[Token(Token = "0x4005CD2")]
			Request_Wait,
			// Token: 0x04002CC5 RID: 11461
			[Token(Token = "0x4005CD3")]
			OccuredRequest,
			// Token: 0x04002CC6 RID: 11462
			[Token(Token = "0x4005CD4")]
			OccuredRequest_Wait,
			// Token: 0x04002CC7 RID: 11463
			[Token(Token = "0x4005CD5")]
			Reward,
			// Token: 0x04002CC8 RID: 11464
			[Token(Token = "0x4005CD6")]
			Reward_Wait,
			// Token: 0x04002CC9 RID: 11465
			[Token(Token = "0x4005CD7")]
			ToBattleOnTutorial_0,
			// Token: 0x04002CCA RID: 11466
			[Token(Token = "0x4005CD8")]
			ToBattleOnTutorial_1,
			// Token: 0x04002CCB RID: 11467
			[Token(Token = "0x4005CD9")]
			ToBattleOnTutorial_2,
			// Token: 0x04002CCC RID: 11468
			[Token(Token = "0x4005CDA")]
			End
		}
	}
}
