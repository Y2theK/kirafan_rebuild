﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000362 RID: 866
	[Token(Token = "0x20002DF")]
	[StructLayout(3)]
	public class ADVTextTagArgDB : ScriptableObject
	{
		// Token: 0x06000BB1 RID: 2993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ADF")]
		[Address(RVA = "0x10168C964", Offset = "0x168C964", VA = "0x10168C964")]
		public ADVTextTagArgDB()
		{
		}

		// Token: 0x04000CDA RID: 3290
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A3A")]
		public ADVTextTagArgDB_Param[] m_Params;
	}
}
