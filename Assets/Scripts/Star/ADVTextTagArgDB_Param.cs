﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000361 RID: 865
	[Token(Token = "0x20002DE")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct ADVTextTagArgDB_Param
	{
		// Token: 0x04000CD8 RID: 3288
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000A38")]
		public string m_ReferenceName;

		// Token: 0x04000CD9 RID: 3289
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000A39")]
		public int m_EnumID;
	}
}
