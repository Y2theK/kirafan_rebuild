﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000363 RID: 867
	[Token(Token = "0x20002E0")]
	[StructLayout(3)]
	public static class ADVUtility
	{
		// Token: 0x06000BB2 RID: 2994 RVA: 0x00004860 File Offset: 0x00002A60
		[Token(Token = "0x6000AE0")]
		[Address(RVA = "0x10168C96C", Offset = "0x168C96C", VA = "0x10168C96C")]
		public static bool IsCrossScenario(int advID)
		{
			return default(bool);
		}

		// Token: 0x06000BB3 RID: 2995 RVA: 0x00004878 File Offset: 0x00002A78
		[Token(Token = "0x6000AE1")]
		[Address(RVA = "0x10168CA68", Offset = "0x168CA68", VA = "0x10168CA68")]
		public static Vector3 GetStandPosition(eADVStandPosition standPos)
		{
			return default(Vector3);
		}

		// Token: 0x06000BB4 RID: 2996 RVA: 0x00004890 File Offset: 0x00002A90
		[Token(Token = "0x6000AE2")]
		[Address(RVA = "0x10168CB58", Offset = "0x168CB58", VA = "0x10168CB58")]
		public static iTween.EaseType GetEaseType(eADVCurveType easeType)
		{
			return iTween.EaseType.easeInQuad;
		}

		// Token: 0x06000BB5 RID: 2997 RVA: 0x000048A8 File Offset: 0x00002AA8
		[Token(Token = "0x6000AE3")]
		[Address(RVA = "0x10168CB6C", Offset = "0x168CB6C", VA = "0x10168CB6C")]
		public static bool IsEmptyADVCharaID(string advCharaID)
		{
			return default(bool);
		}

		// Token: 0x06000BB6 RID: 2998 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000AE4")]
		[Address(RVA = "0x10168CBEC", Offset = "0x168CBEC", VA = "0x10168CBEC")]
		public static string GetADVStandPicResourcePath(string resourceBaseName, int poseId)
		{
			return null;
		}

		// Token: 0x06000BB7 RID: 2999 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000AE5")]
		[Address(RVA = "0x10168CE08", Offset = "0x168CE08", VA = "0x10168CE08")]
		public static string GetADVFaceResourcePath(string resourceBaseName, int facePatternID, string faceName)
		{
			return null;
		}

		// Token: 0x06000BB8 RID: 3000 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000AE6")]
		[Address(RVA = "0x10168D09C", Offset = "0x168D09C", VA = "0x10168D09C")]
		public static string GetADVBackGroundPath(string filename)
		{
			return null;
		}

		// Token: 0x06000BB9 RID: 3001 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000AE7")]
		[Address(RVA = "0x10168D1E0", Offset = "0x168D1E0", VA = "0x10168D1E0")]
		public static string GetADVSpritePath(string filename)
		{
			return null;
		}
	}
}
