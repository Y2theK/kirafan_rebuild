﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000364 RID: 868
	[Token(Token = "0x20002E1")]
	[StructLayout(3)]
	public class ADVVoiceDownloadWindow : MonoBehaviour
	{
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000BBA RID: 3002 RVA: 0x000048C0 File Offset: 0x00002AC0
		[Token(Token = "0x17000074")]
		public bool isOpened
		{
			[Token(Token = "0x6000AE8")]
			[Address(RVA = "0x10168BBBC", Offset = "0x168BBBC", VA = "0x10168BBBC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000BBB RID: 3003 RVA: 0x000048D8 File Offset: 0x00002AD8
		[Token(Token = "0x17000075")]
		public bool isErrorOpened
		{
			[Token(Token = "0x6000AE9")]
			[Address(RVA = "0x10168BC50", Offset = "0x168BC50", VA = "0x10168BC50")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000BBC RID: 3004 RVA: 0x000048F0 File Offset: 0x00002AF0
		[Token(Token = "0x17000076")]
		public bool errorWindowResult
		{
			[Token(Token = "0x6000AEA")]
			[Address(RVA = "0x10168BC80", Offset = "0x168BC80", VA = "0x10168BC80")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000BBE RID: 3006 RVA: 0x00004908 File Offset: 0x00002B08
		// (set) Token: 0x06000BBD RID: 3005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000077")]
		public eADVCategory category
		{
			[Token(Token = "0x6000AEC")]
			[Address(RVA = "0x10168D32C", Offset = "0x168D32C", VA = "0x10168D32C")]
			[CompilerGenerated]
			get
			{
				return eADVCategory.None;
			}
			[Token(Token = "0x6000AEB")]
			[Address(RVA = "0x10168D324", Offset = "0x168D324", VA = "0x10168D324")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000BC0 RID: 3008 RVA: 0x00004920 File Offset: 0x00002B20
		// (set) Token: 0x06000BBF RID: 3007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000078")]
		private bool isADVFullVoiceConfirm
		{
			[Token(Token = "0x6000AEE")]
			[Address(RVA = "0x10168D3C8", Offset = "0x168D3C8", VA = "0x10168D3C8")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000AED")]
			[Address(RVA = "0x10168D334", Offset = "0x168D334", VA = "0x10168D334")]
			set
			{
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000BC2 RID: 3010 RVA: 0x00004938 File Offset: 0x00002B38
		// (set) Token: 0x06000BC1 RID: 3009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000079")]
		private bool isADVFullVoice
		{
			[Token(Token = "0x6000AF0")]
			[Address(RVA = "0x10168D4E0", Offset = "0x168D4E0", VA = "0x10168D4E0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000AEF")]
			[Address(RVA = "0x10168D44C", Offset = "0x168D44C", VA = "0x10168D44C")]
			set
			{
			}
		}

		// Token: 0x06000BC3 RID: 3011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF1")]
		[Address(RVA = "0x10168D564", Offset = "0x168D564", VA = "0x10168D564")]
		private void SetConfirmToggle(bool isOn)
		{
		}

		// Token: 0x06000BC4 RID: 3012 RVA: 0x00004950 File Offset: 0x00002B50
		[Token(Token = "0x6000AF2")]
		[Address(RVA = "0x10168B9B4", Offset = "0x168B9B4", VA = "0x10168B9B4")]
		public bool Open(eADVCategory cate, double dlSize)
		{
			return default(bool);
		}

		// Token: 0x06000BC5 RID: 3013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF3")]
		[Address(RVA = "0x10168D5E0", Offset = "0x168D5E0", VA = "0x10168D5E0")]
		public void Close()
		{
		}

		// Token: 0x06000BC6 RID: 3014 RVA: 0x00004968 File Offset: 0x00002B68
		[Token(Token = "0x6000AF4")]
		[Address(RVA = "0x10168BBEC", Offset = "0x168BBEC", VA = "0x10168BBEC")]
		public bool ErrorOpen()
		{
			return default(bool);
		}

		// Token: 0x06000BC7 RID: 3015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF5")]
		[Address(RVA = "0x10168D71C", Offset = "0x168D71C", VA = "0x10168D71C")]
		public void ErrorClose()
		{
		}

		// Token: 0x06000BC8 RID: 3016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF6")]
		[Address(RVA = "0x10168D64C", Offset = "0x168D64C", VA = "0x10168D64C")]
		private void LocalSave()
		{
		}

		// Token: 0x06000BC9 RID: 3017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF7")]
		[Address(RVA = "0x10168D780", Offset = "0x168D780", VA = "0x10168D780")]
		public void Callback_YesButton()
		{
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF8")]
		[Address(RVA = "0x10168D7A8", Offset = "0x168D7A8", VA = "0x10168D7A8")]
		public void Callback_NoButton()
		{
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AF9")]
		[Address(RVA = "0x10168D7D0", Offset = "0x168D7D0", VA = "0x10168D7D0")]
		public void Callback_Toggle()
		{
		}

		// Token: 0x06000BCC RID: 3020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AFA")]
		[Address(RVA = "0x10168D7F8", Offset = "0x168D7F8", VA = "0x10168D7F8")]
		public void Callback_ErrorYesButton()
		{
		}

		// Token: 0x06000BCD RID: 3021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AFB")]
		[Address(RVA = "0x10168D804", Offset = "0x168D804", VA = "0x10168D804")]
		public void Callback_ErrorNoButton()
		{
		}

		// Token: 0x06000BCE RID: 3022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AFC")]
		[Address(RVA = "0x10168D80C", Offset = "0x168D80C", VA = "0x10168D80C")]
		public ADVVoiceDownloadWindow()
		{
		}

		// Token: 0x04000CDB RID: 3291
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A3B")]
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x04000CDC RID: 3292
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A3C")]
		[SerializeField]
		private UIGroup m_Window;

		// Token: 0x04000CDD RID: 3293
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A3D")]
		[SerializeField]
		private UIGroup m_ErrorWindow;

		// Token: 0x04000CDE RID: 3294
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A3E")]
		[SerializeField]
		private GameObject m_ToggleOn;

		// Token: 0x04000CDF RID: 3295
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A3F")]
		[SerializeField]
		private GameObject m_ToggleOff;

		// Token: 0x04000CE0 RID: 3296
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000A40")]
		[SerializeField]
		private Text m_DLSizeText;

		// Token: 0x04000CE1 RID: 3297
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000A41")]
		private bool m_FullVoiceLoadFlg;

		// Token: 0x04000CE2 RID: 3298
		[Cpp2IlInjected.FieldOffset(Offset = "0x49")]
		[Token(Token = "0x4000A42")]
		private bool m_FullVoiceLoadConfirmFlg;

		// Token: 0x04000CE3 RID: 3299
		[Cpp2IlInjected.FieldOffset(Offset = "0x4A")]
		[Token(Token = "0x4000A43")]
		private bool m_ErrorWindowResult;
	}
}
