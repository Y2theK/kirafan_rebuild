﻿using Meige;
using Star.UI;
using Star.UI.WeaponList;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using WWWTypes;

namespace Star {
    public static class APIUtility {
        public const string ERR_TITLE = "通信エラー";
        public const string ERR_TITLE_MAINTENANCE = "メンテナンス中";
        public const string ERR_TITLE_PLAYER_DELETED = "削除済みのユーザー";
        public const string ERR_TITLE_NOT_UUID = "引き継ぎが行われました。";
        public const string ERR_TITLE_OUTDATED = "アプリの更新";
        public const string ERR_TITLE_AB_OUTDATED = "データの更新";
        public const string ERR_TITLE_PLAYER_SESSION_EXPIRED = "日付の更新";
        public const string ERR_TITLE_CONFIRM = "確認";
        public const string ERR_MSG_SYSTEM_ERROR = "通信エラーが発生しました。";
        public const string ERR_MSG_RETURN_TITLE = "タイトルに戻ります。";
        public const string ERR_MSG_RETRY = "リトライします。";
        public const string ERR_BTN_APP_EXIT = "アプリを終了";

        public static void ShowErrorWindowNone(string errorMessage) {
            ShowErrorWindowNone(ERR_TITLE, errorMessage);
        }

        public static void ShowErrorWindowNone(string errorTitle, string errorMessage) {
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.None, errorTitle, errorMessage, null);
        }

        public static void ShowErrorWindowOk(string errorMessage, Action action) {
            ShowErrorWindowOk(ERR_TITLE, errorMessage, action);
        }

        public static void ShowErrorWindowOk(string errorTitle, string errorMessage, Action action) {
            if (GameSystem.Inst.GlobalUI != null) {
                GameSystem.Inst.GlobalUI.ResetSelectedShortCut();
            }
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, errorTitle, errorMessage, null, (_) => action.Call());
        }

        public static void ShowErrorWindowOk_NoClose(string errorTitle, string errorMessage, Action action) {
            if (GameSystem.Inst.GlobalUI != null) {
                GameSystem.Inst.GlobalUI.ResetSelectedShortCut();
            }
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK_NoClose, errorTitle, errorMessage, null, (_) => action.Call());
        }

        public static void ShowErrorWindowRetry(MeigewwwParam wwwParam, string errorMessage, bool isForceErrorMessage = false) {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(errorMessage) || (!isForceErrorMessage && wwwParam.IsSystemError())) {
                errorMessage = ERR_MSG_SYSTEM_ERROR;
            }
            sb.Append(errorMessage);
            sb.Append("\n");
            sb.Append(ERR_MSG_RETRY);
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, ERR_TITLE, sb.ToString(), null, (_) => RetryAPI(wwwParam));
        }

        public static void ShowErrorWindowTitle(MeigewwwParam wwwParam, string errorMessage) {
            ShowErrorWindowTitle(wwwParam, ERR_TITLE, errorMessage);
        }

        public static void ShowErrorWindowTitle(MeigewwwParam wwwParam, string errorTitle, string errorMessage, bool isAddMessageReturnTitle = true, ResultCode? resultCode = null) {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(errorMessage) || (wwwParam != null && wwwParam.IsSystemError())) {
                errorMessage = ERR_MSG_SYSTEM_ERROR;
            }
            sb.Append(errorMessage);
            if (isAddMessageReturnTitle) {
                sb.Append("\n");
                sb.Append(ERR_MSG_RETURN_TITLE);
            }
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, errorTitle, sb.ToString(), null, (_) => ReturnTitle(true), resultCode);
        }

        public static void RetryAPI(MeigewwwParam wwwParam) {
            GameSystem.Inst.ConnectingIcon.Open();
            NetworkQueueManager.Request(wwwParam);
        }

        public static void ReturnTitle(bool isForceTransitTitleScene = true) {
            GameSystem inst = GameSystem.Inst;
            inst.GlobalParam.Reset();
            inst.CharaMng.DestroyCharaAll();
            inst.CharaResMng.UnloadAll();
            inst.TownResMng.UnloadAllResources();
            inst.RoomResMng.UnloadAllResources();
            inst.EffectMng.ForceResetOnReturnTitle();
            inst.SpriteMng.ForceResetOnReturnTitle();
            inst.SoundMng.ForceResetOnReturnTitle();
            inst.StoreMng.ForceResetOnReturnTitle();
            inst.QuestMng.ForceResetOnReturnTitle();
            inst.OfferMng.ForceResetOnReturnTitle();
            inst.AchievementMng.ForceResetOnReturnTitle();
            inst.MissionMng.ForceResetOnReturnTitle();
            inst.FriendMng.ForceResetOnReturnTitle();
            inst.TrainingMng.ForceResetOnReturnTitle();
            inst.Chest.ForceResetOnReturnTitle();
            inst.EditMng.ForceResetOnReturnTitle();
            inst.EnhanceMng.ForceResetOnReturnTitle();
            inst.AbilityMng.ForceResetOnReturnTitle();
            inst.Unlock.ForceResetOnReturnTitle();
            inst.Gacha.ForceResetOnReturnTitle();
            inst.TutorialMng.ForceResetOnReturnTitle();
            inst.LoginManager.ForceResetOnReturnTitle();

            inst.FadeMng.SetFadeRatio(1);
            inst.FadeMng.SetColor(Color.white);

            if (inst.GlobalUI != null) {
                inst.GlobalUI.Close();
                inst.GlobalUI.ResetSelectedShortCut();
                inst.GlobalUI.SetEnableUpdateDisp(true);
                inst.GlobalUI.SetActiveGemAddButton(true);
            }

            inst.ConnectingIcon.Close();
            inst.LoadingUI.Abort();
            inst.TutorialTipsUI.Abort();
            inst.TutorialMessage.Clear();
            inst.BackGroundManager.Clear();
            inst.InputBlock.Clear();
            inst.GlobalParam.IsRequestedReturnTitle = true;

            GemShopPlayer.Inst.ForceHide();
            StaminaShopPlayer.Inst.ForceHide();
            CharaDetailPlayer.Inst.ForceHide();
            WeaponListPlayer.Inst.ForceHide();
            RewardPlayer.Inst.ForceHide();
            RewardPlayer.Inst.SetReserveOpen(false);
            SupportEditPlayer.Inst.ForceHide();

            inst.OverlayUIMng.Clear();
            inst.WebView.Hide();
            if (inst.AgeConfirmWindow != null) {
                inst.AgeConfirmWindow.ForceHide();
            }

            Screen.sleepTimeout = -2;
            inst.SoundVersionDictionary.Clear();
            inst.RoomObjListBackupParam.Clear();
            Time.timeScale = 1;
            Application.targetFrameRate = 30;
            if (isForceTransitTitleScene) {
                SceneLoader.Inst.ForceTransitTitleScene();
            }
        }

        public static void OpenStore() {
#if UNITY_ANDROID
            string text = WebDataListUtil.GetIDToAdress(1013);
            if (text == null) {
				text = "https://play.google.com/store/apps/details?id=com.aniplex.kirarafantasia";
            }
            Application.OpenURL(text);
#elif UNITY_IOS
			string text = WebDataListUtil.GetIDToAdress(1014);
            if (text == null) {
                text = "https://itunes.apple.com/jp/app/id1226256105";
            }
            Application.OpenURL(text);
#endif
        }

        public static void ApplyingOn(string applyUrl, string applyAssetUrl) {
            Version.IsApplying = true;
            ProjDepend.SERVER_APPLYING_URL = applyUrl;
            AssetServerSettings.m_assetServerReleaseURL = applyAssetUrl;
            NetworkQueueManager.SetRelease(true);
        }

        public static string GenerateUUID() {
            return Guid.NewGuid().ToString();
        }

        public static bool IsNeedSignup() {
            return string.IsNullOrEmpty(AccessSaveData.Inst.UUID);
        }

        public static string ArrayToStr<T>(T[] array) {
            if (array != null) {
                string text = string.Empty;
                for (int i = 0; i < array.Length; i++) {
                    text += ((i != 0) ? ("," + array[i].ToString()) : array[i].ToString());
                }
                return text;
            }
            return null;
        }

        public static void wwwToUserData(Player src, UserData dest) {
            dest.ID = src.id;
            dest.Name = src.name;
            dest.Comment = src.comment;
            dest.Age = (eAgeType)src.age;
            dest.MyCode = src.myCode;
            dest.AchievementID = src.currentAchievementId;
            dest.Lv = src.level;
            dest.LvExp = src.levelExp;
            dest.Exp = src.totalExp;
            dest.Gold = src.gold;
            dest.UnlimitedGem = src.unlimitedGem;
            dest.LimitedGem = src.limitedGem;
            dest.LotteryTicket = src.lotteryTicket;
            dest.PartyCost = src.partyCost;
            dest.KRRPoint.SetPoint(src.kirara);
            dest.KRRPoint.SetLimit(src.kiraraLimit);
            dest.WeaponLimit = src.weaponLimit;
            dest.WeaponLimitCount = src.weaponLimitCount;
            dest.FacilityLimit = src.facilityLimit;
            dest.FacilityLimitCount = src.facilityLimitCount;
            dest.RoomObjectLimit = src.roomObjectLimit;
            dest.RoomObjectLimitCount = src.roomObjectLimitCount;
            dest.CharacterLimit = src.characterLimit;
            dest.ItemLimit = src.itemLimit;
            dest.FriendLimit = src.friendLimit;
            dest.SupportLimit = src.supportLimit;
            dest.LoginCount = src.loginCount;
            dest.LastLoginAt = src.lastLoginAt;
            dest.LoginDays = src.loginDays;
            dest.ContinuousDays = src.continuousDays;
            dest.Stamina.SetValueMax(src.staminaMax);
            dest.Stamina.SetValue(src.stamina);
            dest.Stamina.SetRemainSecMax(src.recastTimeMax);
            dest.Stamina.SetRemainSec(src.recastTime);
            GameSystem.Inst.NotificationCtrl.LocalPush_StaminaFull();
        }

        public static void wwwToUserData(PlayerCharacter src, UserCharacterData dest) {
            dest.MngID = src.managedCharacterId;
            dest.IsNew = src.shown == 0;
            if (dest.Param == null) {
                dest.Param = new CharacterParam();
            }
            CharacterUtility.SetupCharacterParam(
                dest.Param,
                src.managedCharacterId,
                src.characterId,
                src.viewCharacterId,
                src.level,
                src.levelLimit,
                src.exp,
                src.levelBreak,
                src.arousalLevel,
                src.duplicatedCount
            );
            CharacterListDB_Param param = GameSystem.Inst.DbMng.CharaListDB.GetParam(dest.Param.CharaID);
            dest.Param.UniqueSkillLearnDatas = new List<SkillLearnData> {
                new SkillLearnData(param.m_CharaSkillID, src.skillLevel1, src.skillLevelLimit1, src.skillExp1, param.m_CharaSkillExpTableID)
            };
            if (dest.Param.ClassSkillLearnDatas != null) {
                dest.Param.ClassSkillLearnDatas = new List<SkillLearnData>();
            }
            dest.Param.ClassSkillLearnDatas.Clear();
            int num = 0;
            if (param.m_ClassSkillIDs.Length > num) {
                dest.Param.ClassSkillLearnDatas.Add(new SkillLearnData(param.m_ClassSkillIDs[num], src.skillLevel2, src.skillLevelLimit2, src.skillExp2, param.m_ClassSkillExpTableIDs[num], true));
                num = 1;
                if (param.m_ClassSkillIDs.Length > num) {
                    dest.Param.ClassSkillLearnDatas.Add(new SkillLearnData(param.m_ClassSkillIDs[num], src.skillLevel3, src.skillLevelLimit3, src.skillExp3, param.m_ClassSkillExpTableIDs[num], true));
                }
            }
        }

        public static void wwwToUserData(PlayerCharacter[] src, List<UserCharacterData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserCharacterData userCharacterData = new UserCharacterData();
                wwwToUserData(src[i], userCharacterData);
                dest.Add(userCharacterData);
            }
        }

        public static void wwwToUserData(SupportCharacter src, UserSupportData dest) {
            dest.MngID = src.managedCharacterId;
            dest.CharaID = src.characterId;
            dest.Lv = src.level;
            dest.MaxLv = EditUtility.CalcMaxLvFromLimitBreak(src.characterId, src.levelBreak);
            dest.Exp = src.exp;
            dest.LimitBreak = src.levelBreak;
            dest.UniqueSkillLv = src.skillLevel1;
            dest.ClassSkillLvs = new int[] {
                src.skillLevel2,
                src.skillLevel3,
            };
            dest.FriendShip = src.namedLevel;
            dest.FriendShipExp = src.namedExp;
            dest.WeaponID = src.weaponId;
            dest.WeaponLv = src.weaponLevel;
            dest.WeaponSkillLv = src.weaponSkillLevel;
            dest.WeaponSkillExp = src.weaponSkillExp;
            dest.ArousalLv = src.arousalLevel;
            dest.AbilityBoardID = src.abilityBoardId;
            dest.AbilityEquipItemIDs = src.equipItemIds;
            dest.DuplicatedCount = src.duplicatedCount;
            dest.Cost = EditUtility.CalcCharaCost(dest.CharaID, dest.ArousalLv);
        }

        public static void wwwToUserData(SupportCharacter[] src, List<UserSupportData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserSupportData userSupportData = new UserSupportData();
                wwwToUserData(src[i], userSupportData);
                dest.Add(userSupportData);
            }
        }

        public static void wwwToUserData(PlayerNamedType src, UserNamedData dest) {
            dest.MngID = src.managedNamedTypeId;
            dest.NamedType = (eCharaNamedType)src.namedType;
            dest.FriendShip = src.level;
            dest.FriendShipExp = src.exp;
            NamedListDB_Param param = GameSystem.Inst.DbMng.NamedListDB.GetParam(dest.NamedType);
            dest.FriendShipMax = GameSystem.Inst.DbMng.NamedFriendshipExpDB.GetFriendShipMax(param.m_FriendshipTableID);
        }

        public static void wwwToUserData(PlayerNamedType[] src, Dictionary<eCharaNamedType, UserNamedData> dest) {
            if (src == null || dest == null) { return; }
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserNamedData userNamedData = new UserNamedData();
                wwwToUserData(src[i], userNamedData);
                dest.Add((eCharaNamedType)src[i].namedType, userNamedData);
            }
        }

        public static void wwwToUserData(PlayerWeapon src, UserWeaponData dest) {
            dest.MngID = src.managedWeaponId;
            if (dest.Param == null) {
                dest.Param = new WeaponParam();
            }
            CharacterUtility.SetupWeaponParam(dest.Param, src.managedWeaponId, src.weaponId, src.level, src.exp, src.skillLevel, src.skillExp);
        }

        public static void wwwToUserData(PlayerWeapon[] src, List<UserWeaponData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserWeaponData userWeaponData = new UserWeaponData();
                wwwToUserData(src[i], userWeaponData);
                dest.Add(userWeaponData);
            }
        }

        public static void wwwToUserData(PlayerItemSummary src, UserItemData dest) {
            dest.ItemID = src.id;
            dest.ItemNum = src.amount;
            GameSystem.Inst.UserDataMng.UpdateSubOfferCount(GameSystem.Inst.OfferMng.GetTitles());
        }

        public static void wwwToUserData(PlayerItemSummary[] src, List<UserItemData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserItemData userItemData = new UserItemData();
                wwwToUserData(src[i], userItemData);
                dest.Add(userItemData);
            }
        }

        public static void wwwToUserData(PlayerMasterOrb src, UserMasterOrbData dest) {
            dest.MngID = src.managedMasterOrbId;
            dest.OrbID = src.masterOrbId;
            dest.Lv = src.level;
            dest.Exp = src.exp;
        }

        public static void wwwToUserData(PlayerMasterOrb[] src, List<UserMasterOrbData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserMasterOrbData userMasterOrbData = new UserMasterOrbData();
                wwwToUserData(src[i], userMasterOrbData);
                dest.Add(userMasterOrbData);
            }
        }

        public static void wwwToUserData(PlayerBattleParty src, UserBattlePartyData dest) {
            dest.MngID = src.managedBattlePartyId;
            dest.PartyName = src.name;
            dest.ClearSlotAll();
            for (int i = 0; i < src.managedCharacterIds.Length; i++) {
                dest.SetMemberAt(i, src.managedCharacterIds[i]);
            }
            for (int i = 0; i < src.managedWeaponIds.Length; i++) {
                dest.SetWeaponAt(i, src.managedWeaponIds[i]);
            }
            dest.OrbID = src.masterOrbId;
        }

        public static void wwwToUserData(PlayerAbilityBoard[] src, List<UserAbilityData> dest) {
            if (src == null || dest == null) { return; }
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserAbilityData userBattlePartyData = new UserAbilityData();
                wwwToUserData(src[i], userBattlePartyData);
                dest.Add(userBattlePartyData);
            }
        }

        public static void wwwToUserData(PlayerAbilityBoard src, UserAbilityData dest) {
            dest.MngID = src.managedAbilityBoardId;
            dest.CharaMngID = src.managedCharacterId;
            if (dest.Param == null) {
                dest.Param = new AbilityParam();
            }
            CharacterUtility.SetupAbilityParam(dest.Param, src.abilityBoardId, src.equipItemIds);
        }

        public static void wwwToUserData(PlayerBattleParty[] src, List<UserBattlePartyData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserBattlePartyData userBattlePartyData = new UserBattlePartyData();
                wwwToUserData(src[i], userBattlePartyData);
                dest.Add(userBattlePartyData);
            }
        }

        public static void wwwToUserData(PlayerSupport src, UserSupportPartyData dest) {
            dest.MngID = src.managedSupportId;
            dest.PartyName = src.name;
            dest.IsActive = (src.active == 1);
            dest.CharaMngIDs.Clear();
            for (int i = 0; i < src.managedCharacterIds.Length; i++) {
                dest.CharaMngIDs.Add(src.managedCharacterIds[i]);
            }
            dest.WeaponMngIDs.Clear();
            for (int i = 0; i < src.managedWeaponIds.Length; i++) {
                dest.WeaponMngIDs.Add(src.managedWeaponIds[i]);
            }
        }

        public static void wwwToUserData(PlayerSupport[] src, List<UserSupportPartyData> dest) {
            dest.Clear();
            for (int i = 0; i < src.Length; i++) {
                UserSupportPartyData userSupportPartyData = new UserSupportPartyData();
                wwwToUserData(src[i], userSupportPartyData);
                dest.Add(userSupportPartyData);
            }
        }

        public static void wwwToUserData(PlayerTown[] src, UserTownData dest) {
            WWWComUtil.SetTownBuildListKey(src, true);
        }

        public static void wwwToUserData(PlayerTownFacility[] src, List<UserTownObjectData> dest) {
            WWWComUtil.SetTownObjectListKey(src, true);
        }

        public static void wwwToUserData(PlayerRoom[] src, List<UserRoomData> dest) {
            WWWComUtil.SetRoomPlacementKey(src, true);
        }

        public static void wwwToUserData(PlayerRoomObject[] src, List<UserRoomObjectData> dest) {
            WWWComUtil.SetRoomObjectList(src, true, false);
        }

        public static void AddNewRoomObj(PlayerRoomObject[] src) {
            List<PlayerRoomObject> list = new List<PlayerRoomObject>();
            for (int i = 0; i < src.Length; i++) {
                long managedId = src[i].managedRoomObjectId;
                if (managedId != -1 && UserRoomUtil.GetManageIDToUserRoomData(managedId) == null) {
                    list.Add(src[i]);
                }
            }
            if (list.Count > 0) {
                WWWComUtil.SetRoomObjectList(list.ToArray(), false);
            }
        }

        public static void wwwToUserData(PlayerFieldPartyMember[] src, UserFieldCharaData dest) {
            WWWComUtil.SetFieldPartyListKey(src, true);
        }

        public static void wwwToUserData(ArousalResult[] arousalResult, List<UserArousalResultData> dest) {
            dest.Clear();
            if (arousalResult == null || arousalResult.Length == 0) { return; }
            for (int i = 0; i < arousalResult.Length; i++) {
                ArousalResult result = arousalResult[i];
                UserCharacterData charData = GameSystem.Inst.UserDataMng.GetUserCharaData(result.characterId);
                UserArousalResultData resultData = new UserArousalResultData();
                if (charData != null) {
                    resultData.CharaID = charData.Param.CharaID;
                    resultData.BeforeArousalLevel = charData.Param.ArousalLv;
                    resultData.BeforeDuplicatedCount = charData.Param.DuplicatedCount;
                } else {
                    resultData.CharaID = result.characterId;
                    resultData.BeforeArousalLevel = 0;
                    resultData.BeforeDuplicatedCount = 0;
                }
                resultData.AfterArousalLevel = result.arousalLevel;
                resultData.AfterDuplicatedCount = result.duplicatedCount;
                resultData.ArousalItemID = result.arousalItemId;
                resultData.ArousalItemAmount = result.arousalItemAmount;
                dest.Add(resultData);
            }
        }

        public static void wwwToUserData(PlayerFavoriteMember[] src, UserFavoriteMemberData dest) {
            dest.DataClear();
            for (int i = 0; i < src.Length; i++) {
                dest.SetData(src[i]);
            }
        }
    }
}
