﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200090D RID: 2317
	[Token(Token = "0x20006A0")]
	[StructLayout(3)]
	public class AStar : IPathFinding
	{
		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06002630 RID: 9776 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700026F")]
		public List<AStar.ANode> OpenList
		{
			[Token(Token = "0x6002340")]
			[Address(RVA = "0x101691B00", Offset = "0x1691B00", VA = "0x101691B00")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06002631 RID: 9777 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000270")]
		public List<AStar.ANode> CloseList
		{
			[Token(Token = "0x6002341")]
			[Address(RVA = "0x101691B08", Offset = "0x1691B08", VA = "0x101691B08")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06002632 RID: 9778 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000271")]
		public AStar.ANode CurrentNode
		{
			[Token(Token = "0x6002342")]
			[Address(RVA = "0x101691B10", Offset = "0x1691B10", VA = "0x101691B10")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06002633 RID: 9779 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000272")]
		public UnityEngine.Object LockObject
		{
			[Token(Token = "0x6002343")]
			[Address(RVA = "0x101691B18", Offset = "0x1691B18", VA = "0x101691B18")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002634 RID: 9780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002344")]
		[Address(RVA = "0x101691B20", Offset = "0x1691B20", VA = "0x101691B20")]
		public AStar(int w, int h)
		{
		}

		// Token: 0x06002635 RID: 9781 RVA: 0x00010338 File Offset: 0x0000E538
		[Token(Token = "0x6002345")]
		[Address(RVA = "0x101691C10", Offset = "0x1691C10", VA = "0x101691C10")]
		public bool IsPassable(Vector2 pt)
		{
			return default(bool);
		}

		// Token: 0x06002636 RID: 9782 RVA: 0x00010350 File Offset: 0x0000E550
		[Token(Token = "0x6002346")]
		[Address(RVA = "0x101691C2C", Offset = "0x1691C2C", VA = "0x101691C2C")]
		public int GetCost(Vector2 pt)
		{
			return 0;
		}

		// Token: 0x06002637 RID: 9783 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002347")]
		[Address(RVA = "0x101691CEC", Offset = "0x1691CEC", VA = "0x101691CEC", Slot = "8")]
		public virtual List<Vector2> GetPassableNeighbours(AStar.ANode node)
		{
			return null;
		}

		// Token: 0x06002638 RID: 9784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002348")]
		[Address(RVA = "0x1016920D4", Offset = "0x16920D4", VA = "0x1016920D4")]
		public void OpenNeighbours(AStar.ANode parent)
		{
		}

		// Token: 0x06002639 RID: 9785 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002349")]
		[Address(RVA = "0x101692300", Offset = "0x1692300", VA = "0x101692300")]
		public AStar.ANode OpenNode(Vector2 pos, AStar.ANode parent)
		{
			return null;
		}

		// Token: 0x0600263A RID: 9786 RVA: 0x00010368 File Offset: 0x0000E568
		[Token(Token = "0x600234A")]
		[Address(RVA = "0x101692764", Offset = "0x1692764", VA = "0x101692764", Slot = "9")]
		public virtual int CalcHeuristicCost(Vector2 start, Vector2 end)
		{
			return 0;
		}

		// Token: 0x0600263B RID: 9787 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600234B")]
		[Address(RVA = "0x101692878", Offset = "0x1692878", VA = "0x101692878")]
		public AStar.ANode GetMinCostNodeInOpenList()
		{
			return null;
		}

		// Token: 0x0600263C RID: 9788 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600234C")]
		[Address(RVA = "0x101692880", Offset = "0x1692880", VA = "0x101692880")]
		public AStar.ANode GetMinCostNode(List<AStar.ANode> list)
		{
			return null;
		}

		// Token: 0x0600263D RID: 9789 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600234D")]
		[Address(RVA = "0x1016929B8", Offset = "0x16929B8", VA = "0x1016929B8")]
		public AStar.ANode GetMinCostNodeInCloseList()
		{
			return null;
		}

		// Token: 0x0600263E RID: 9790 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600234E")]
		[Address(RVA = "0x101692A98", Offset = "0x1692A98", VA = "0x101692A98", Slot = "5")]
		public override List<Vector2> Find(Vector2 v1, Vector2 v2)
		{
			return null;
		}

		// Token: 0x0600263F RID: 9791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600234F")]
		[Address(RVA = "0x101692D78", Offset = "0x1692D78", VA = "0x101692D78")]
		protected void ClipPathInCost()
		{
		}

		// Token: 0x06002640 RID: 9792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002350")]
		[Address(RVA = "0x101692F1C", Offset = "0x1692F1C", VA = "0x101692F1C", Slot = "6")]
		public override void FindAsync(Vector2 v1, Vector2 v2)
		{
		}

		// Token: 0x06002641 RID: 9793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002351")]
		[Address(RVA = "0x101693004", Offset = "0x1693004", VA = "0x101693004", Slot = "10")]
		public virtual void FindThread()
		{
		}

		// Token: 0x06002642 RID: 9794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002352")]
		[Address(RVA = "0x10169322C", Offset = "0x169322C", VA = "0x10169322C", Slot = "7")]
		public override void Abort()
		{
		}

		// Token: 0x0400365F RID: 13919
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40027BF")]
		protected int m_Width;

		// Token: 0x04003660 RID: 13920
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40027C0")]
		protected int m_Height;

		// Token: 0x04003661 RID: 13921
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40027C1")]
		protected List<AStar.ANode> m_OpenList;

		// Token: 0x04003662 RID: 13922
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40027C2")]
		protected List<AStar.ANode> m_CloseList;

		// Token: 0x04003663 RID: 13923
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40027C3")]
		protected Dictionary<Vector2, AStar.ANode> m_Pool;

		// Token: 0x04003664 RID: 13924
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40027C4")]
		protected AStar.ANode m_CurrentNode;

		// Token: 0x04003665 RID: 13925
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40027C5")]
		protected Vector2 m_DestPosOriginal;

		// Token: 0x04003666 RID: 13926
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40027C6")]
		protected Vector2 m_DestPos;

		// Token: 0x04003667 RID: 13927
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40027C7")]
		protected Vector2 m_StartPos;

		// Token: 0x04003668 RID: 13928
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40027C8")]
		protected bool m_AllowDiagonal;

		// Token: 0x04003669 RID: 13929
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40027C9")]
		protected Thread m_Thread;

		// Token: 0x0400366A RID: 13930
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40027CA")]
		protected UnityEngine.Object m_LockObject;

		// Token: 0x0400366B RID: 13931
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40027CB")]
		protected bool m_AbortRequest;

		// Token: 0x0200090E RID: 2318
		[Token(Token = "0x2000F3D")]
		public class ANode
		{
			// Token: 0x1700029A RID: 666
			// (get) Token: 0x06002643 RID: 9795 RVA: 0x00002052 File Offset: 0x00000252
			// (set) Token: 0x06002644 RID: 9796 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x1700069D")]
			public AStar.ANode Parent
			{
				[Token(Token = "0x6005FAE")]
				[Address(RVA = "0x1016932E4", Offset = "0x16932E4", VA = "0x1016932E4")]
				[CompilerGenerated]
				get
				{
					return null;
				}
				[Token(Token = "0x6005FAF")]
				[Address(RVA = "0x1016932EC", Offset = "0x16932EC", VA = "0x1016932EC")]
				[CompilerGenerated]
				private set
				{
				}
			}

			// Token: 0x1700029B RID: 667
			// (get) Token: 0x06002645 RID: 9797 RVA: 0x00010380 File Offset: 0x0000E580
			// (set) Token: 0x06002646 RID: 9798 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x1700069E")]
			public int ID
			{
				[Token(Token = "0x6005FB0")]
				[Address(RVA = "0x1016932F4", Offset = "0x16932F4", VA = "0x1016932F4")]
				[CompilerGenerated]
				get
				{
					return 0;
				}
				[Token(Token = "0x6005FB1")]
				[Address(RVA = "0x1016932FC", Offset = "0x16932FC", VA = "0x1016932FC")]
				[CompilerGenerated]
				private set
				{
				}
			}

			// Token: 0x1700029C RID: 668
			// (get) Token: 0x06002647 RID: 9799 RVA: 0x00010398 File Offset: 0x0000E598
			// (set) Token: 0x06002648 RID: 9800 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x1700069F")]
			public Vector2 Pos
			{
				[Token(Token = "0x6005FB2")]
				[Address(RVA = "0x1016920CC", Offset = "0x16920CC", VA = "0x1016920CC")]
				[CompilerGenerated]
				get
				{
					return default(Vector2);
				}
				[Token(Token = "0x6005FB3")]
				[Address(RVA = "0x101693304", Offset = "0x1693304", VA = "0x101693304")]
				[CompilerGenerated]
				private set
				{
				}
			}

			// Token: 0x1700029D RID: 669
			// (get) Token: 0x06002649 RID: 9801 RVA: 0x000103B0 File Offset: 0x0000E5B0
			// (set) Token: 0x0600264A RID: 9802 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006A0")]
			public int G_cost
			{
				[Token(Token = "0x6005FB4")]
				[Address(RVA = "0x101692708", Offset = "0x1692708", VA = "0x101692708")]
				[CompilerGenerated]
				get
				{
					return 0;
				}
				[Token(Token = "0x6005FB5")]
				[Address(RVA = "0x101693310", Offset = "0x1693310", VA = "0x101693310")]
				[CompilerGenerated]
				private set
				{
				}
			}

			// Token: 0x1700029E RID: 670
			// (get) Token: 0x0600264B RID: 9803 RVA: 0x000103C8 File Offset: 0x0000E5C8
			// (set) Token: 0x0600264C RID: 9804 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006A1")]
			public int H_cost
			{
				[Token(Token = "0x6005FB6")]
				[Address(RVA = "0x1016929B0", Offset = "0x16929B0", VA = "0x1016929B0")]
				[CompilerGenerated]
				get
				{
					return 0;
				}
				[Token(Token = "0x6005FB7")]
				[Address(RVA = "0x101693318", Offset = "0x1693318", VA = "0x101693318")]
				[CompilerGenerated]
				private set
				{
				}
			}

			// Token: 0x1700029F RID: 671
			// (get) Token: 0x0600264D RID: 9805 RVA: 0x000103E0 File Offset: 0x0000E5E0
			[Token(Token = "0x170006A2")]
			public int F_cost
			{
				[Token(Token = "0x6005FB8")]
				[Address(RVA = "0x101692710", Offset = "0x1692710", VA = "0x101692710")]
				get
				{
					return 0;
				}
			}

			// Token: 0x0600264E RID: 9806 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FB9")]
			[Address(RVA = "0x10169271C", Offset = "0x169271C", VA = "0x10169271C")]
			public ANode(Vector2 pos)
			{
			}

			// Token: 0x0600264F RID: 9807 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FBA")]
			[Address(RVA = "0x101692758", Offset = "0x1692758", VA = "0x101692758")]
			public void Open(int G_cost, int H_cost, AStar.ANode parent)
			{
			}

			// Token: 0x06002650 RID: 9808 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005FBB")]
			[Address(RVA = "0x101692CC8", Offset = "0x1692CC8", VA = "0x101692CC8")]
			public List<Vector2> GetPath()
			{
				return null;
			}

			// Token: 0x06002651 RID: 9809 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FBC")]
			[Address(RVA = "0x101693320", Offset = "0x1693320", VA = "0x101693320")]
			private void GetPath(List<Vector2> list)
			{
			}
		}
	}
}
