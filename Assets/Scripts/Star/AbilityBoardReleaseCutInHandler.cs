﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000332 RID: 818
	[Token(Token = "0x20002BD")]
	[StructLayout(3)]
	public class AbilityBoardReleaseCutInHandler : MonoBehaviour
	{
		// Token: 0x06000AF1 RID: 2801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A25")]
		[Address(RVA = "0x1016933B8", Offset = "0x16933B8", VA = "0x1016933B8")]
		private void Awake()
		{
		}

		// Token: 0x06000AF2 RID: 2802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A26")]
		[Address(RVA = "0x1016935AC", Offset = "0x16935AC", VA = "0x1016935AC")]
		private void Update()
		{
		}

		// Token: 0x06000AF3 RID: 2803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A27")]
		[Address(RVA = "0x1016935DC", Offset = "0x16935DC", VA = "0x1016935DC")]
		public void Play()
		{
		}

		// Token: 0x06000AF4 RID: 2804 RVA: 0x000045D8 File Offset: 0x000027D8
		[Token(Token = "0x6000A28")]
		[Address(RVA = "0x10169369C", Offset = "0x169369C", VA = "0x10169369C")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06000AF5 RID: 2805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A29")]
		[Address(RVA = "0x101693740", Offset = "0x1693740", VA = "0x101693740")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x06000AF6 RID: 2806 RVA: 0x000045F0 File Offset: 0x000027F0
		[Token(Token = "0x6000A2A")]
		[Address(RVA = "0x101693798", Offset = "0x1693798", VA = "0x101693798")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06000AF7 RID: 2807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A2B")]
		[Address(RVA = "0x1016937C8", Offset = "0x16937C8", VA = "0x1016937C8")]
		public AbilityBoardReleaseCutInHandler()
		{
		}

		// Token: 0x04000C0B RID: 3083
		[Token(Token = "0x40009B4")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04000C0C RID: 3084
		[Token(Token = "0x40009B5")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000C0D RID: 3085
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009B6")]
		public Transform m_Transform;

		// Token: 0x04000C0E RID: 3086
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009B7")]
		public Animation m_Anim;

		// Token: 0x04000C0F RID: 3087
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009B8")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000C10 RID: 3088
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009B9")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x04000C11 RID: 3089
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40009BA")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04000C12 RID: 3090
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40009BB")]
		public MsbHndlWrapper m_MsbHndlWrapper;
	}
}
