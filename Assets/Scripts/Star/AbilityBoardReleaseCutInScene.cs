﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000333 RID: 819
	[Token(Token = "0x20002BE")]
	[StructLayout(3)]
	public class AbilityBoardReleaseCutInScene
	{
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000AF8 RID: 2808 RVA: 0x00004608 File Offset: 0x00002808
		// (set) Token: 0x06000AF9 RID: 2809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000072")]
		public bool Skipped
		{
			[Token(Token = "0x6000A2C")]
			[Address(RVA = "0x101693830", Offset = "0x1693830", VA = "0x101693830")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000A2D")]
			[Address(RVA = "0x101693838", Offset = "0x1693838", VA = "0x101693838")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x06000AFA RID: 2810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A2E")]
		[Address(RVA = "0x101693840", Offset = "0x1693840", VA = "0x101693840")]
		public AbilityBoardReleaseCutInScene()
		{
		}

		// Token: 0x06000AFB RID: 2811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A2F")]
		[Address(RVA = "0x101693910", Offset = "0x1693910", VA = "0x101693910")]
		public void Destroy()
		{
		}

		// Token: 0x06000AFC RID: 2812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A30")]
		[Address(RVA = "0x101693A78", Offset = "0x1693A78", VA = "0x101693A78")]
		public void Update()
		{
		}

		// Token: 0x06000AFD RID: 2813 RVA: 0x00004620 File Offset: 0x00002820
		[Token(Token = "0x6000A31")]
		[Address(RVA = "0x101693E50", Offset = "0x1693E50", VA = "0x101693E50")]
		public bool Prepare(Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x06000AFE RID: 2814 RVA: 0x00004638 File Offset: 0x00002838
		[Token(Token = "0x6000A32")]
		[Address(RVA = "0x101693F00", Offset = "0x1693F00", VA = "0x101693F00")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06000AFF RID: 2815 RVA: 0x00004650 File Offset: 0x00002850
		[Token(Token = "0x6000A33")]
		[Address(RVA = "0x101693F10", Offset = "0x1693F10", VA = "0x101693F10")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000B00 RID: 2816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A34")]
		[Address(RVA = "0x101693A94", Offset = "0x1693A94", VA = "0x101693A94")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06000B01 RID: 2817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A35")]
		[Address(RVA = "0x101693F20", Offset = "0x1693F20", VA = "0x101693F20")]
		public void Play()
		{
		}

		// Token: 0x06000B02 RID: 2818 RVA: 0x00004668 File Offset: 0x00002868
		[Token(Token = "0x6000A36")]
		[Address(RVA = "0x10169403C", Offset = "0x169403C", VA = "0x10169403C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000B03 RID: 2819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A37")]
		[Address(RVA = "0x101693D30", Offset = "0x1693D30", VA = "0x101693D30")]
		private void Update_Main()
		{
		}

		// Token: 0x06000B04 RID: 2820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A38")]
		[Address(RVA = "0x101694060", Offset = "0x1694060", VA = "0x101694060")]
		protected void OnSkip()
		{
		}

		// Token: 0x06000B05 RID: 2821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A39")]
		[Address(RVA = "0x10169408C", Offset = "0x169408C", VA = "0x10169408C")]
		private void Fadeout()
		{
		}

		// Token: 0x06000B06 RID: 2822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A3A")]
		[Address(RVA = "0x1016939D8", Offset = "0x16939D8", VA = "0x1016939D8")]
		private void DestroySound()
		{
		}

		// Token: 0x04000C13 RID: 3091
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40009BC")]
		private AbilityBoardReleaseCutInScene.eMode m_Mode;

		// Token: 0x04000C14 RID: 3092
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40009BD")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x04000C15 RID: 3093
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009BE")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000C16 RID: 3094
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009BF")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000C17 RID: 3095
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009C0")]
		private GameObject m_EffectGO;

		// Token: 0x04000C18 RID: 3096
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009C1")]
		private AbilityBoardReleaseCutInHandler m_EffectHndl;

		// Token: 0x04000C19 RID: 3097
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40009C2")]
		private Transform m_ParentTransform;

		// Token: 0x04000C1A RID: 3098
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40009C3")]
		private Vector3 m_Position;

		// Token: 0x04000C1C RID: 3100
		[Cpp2IlInjected.FieldOffset(Offset = "0x4D")]
		[Token(Token = "0x40009C5")]
		private bool m_IsStartedFadeout;

		// Token: 0x04000C1D RID: 3101
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40009C6")]
		private SoundHandler m_SoundHndl;

		// Token: 0x04000C1E RID: 3102
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40009C7")]
		private readonly string m_ResourcePath;

		// Token: 0x04000C1F RID: 3103
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40009C8")]
		private readonly string m_ObjectPath;

		// Token: 0x04000C20 RID: 3104
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40009C9")]
		private AbilityBoardReleaseCutInScene.ePrepareStep m_PrepareStep;

		// Token: 0x04000C21 RID: 3105
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40009CA")]
		private AbilityBoardReleaseCutInScene.eMainStep m_MainStep;

		// Token: 0x02000334 RID: 820
		[Token(Token = "0x2000D4B")]
		public enum eMode
		{
			// Token: 0x04000C23 RID: 3107
			[Token(Token = "0x40055DF")]
			None = -1,
			// Token: 0x04000C24 RID: 3108
			[Token(Token = "0x40055E0")]
			Prepare,
			// Token: 0x04000C25 RID: 3109
			[Token(Token = "0x40055E1")]
			UpdateMain,
			// Token: 0x04000C26 RID: 3110
			[Token(Token = "0x40055E2")]
			Destroy
		}

		// Token: 0x02000335 RID: 821
		[Token(Token = "0x2000D4C")]
		private enum ePrepareStep
		{
			// Token: 0x04000C28 RID: 3112
			[Token(Token = "0x40055E4")]
			None = -1,
			// Token: 0x04000C29 RID: 3113
			[Token(Token = "0x40055E5")]
			Prepare,
			// Token: 0x04000C2A RID: 3114
			[Token(Token = "0x40055E6")]
			Prepare_Wait,
			// Token: 0x04000C2B RID: 3115
			[Token(Token = "0x40055E7")]
			End,
			// Token: 0x04000C2C RID: 3116
			[Token(Token = "0x40055E8")]
			Prepare_Error
		}

		// Token: 0x02000336 RID: 822
		[Token(Token = "0x2000D4D")]
		private enum eMainStep
		{
			// Token: 0x04000C2E RID: 3118
			[Token(Token = "0x40055EA")]
			None = -1,
			// Token: 0x04000C2F RID: 3119
			[Token(Token = "0x40055EB")]
			Play,
			// Token: 0x04000C30 RID: 3120
			[Token(Token = "0x40055EC")]
			End
		}
	}
}
