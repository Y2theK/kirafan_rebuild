﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200049B RID: 1179
	[Token(Token = "0x2000393")]
	[StructLayout(3)]
	public class AbilityBoardsDB : ScriptableObject
	{
		// Token: 0x060013F0 RID: 5104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A5")]
		[Address(RVA = "0x1016940D0", Offset = "0x16940D0", VA = "0x1016940D0")]
		public AbilityBoardsDB()
		{
		}

		// Token: 0x04001606 RID: 5638
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FEC")]
		public AbilityBoardsDB_Param[] m_Params;
	}
}
