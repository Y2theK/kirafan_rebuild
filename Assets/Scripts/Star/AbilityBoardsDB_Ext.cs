﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004F1 RID: 1265
	[Token(Token = "0x20003E7")]
	[StructLayout(3)]
	public static class AbilityBoardsDB_Ext
	{
		// Token: 0x06001501 RID: 5377 RVA: 0x00008B80 File Offset: 0x00006D80
		[Token(Token = "0x60013B6")]
		[Address(RVA = "0x1016940D8", Offset = "0x16940D8", VA = "0x1016940D8")]
		public static AbilityBoardsDB_Param? GetParam(this AbilityBoardsDB self, int id)
		{
			return null;
		}

		// Token: 0x06001502 RID: 5378 RVA: 0x00008B98 File Offset: 0x00006D98
		[Token(Token = "0x60013B7")]
		[Address(RVA = "0x101694210", Offset = "0x1694210", VA = "0x101694210")]
		public static AbilityBoardsDB_Param? GetParamFromItemID(this AbilityBoardsDB self, int itemID)
		{
			return null;
		}

		// Token: 0x06001503 RID: 5379 RVA: 0x00008BB0 File Offset: 0x00006DB0
		[Token(Token = "0x60013B8")]
		[Address(RVA = "0x101694348", Offset = "0x1694348", VA = "0x101694348")]
		public static int BoardIDToItemID(this AbilityBoardsDB self, int id)
		{
			return 0;
		}
	}
}
