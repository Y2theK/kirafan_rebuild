﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200049A RID: 1178
	[Token(Token = "0x2000392")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct AbilityBoardsDB_Param
	{
		// Token: 0x04001604 RID: 5636
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FEA")]
		public int m_ID;

		// Token: 0x04001605 RID: 5637
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000FEB")]
		public int m_ItemID;
	}
}
