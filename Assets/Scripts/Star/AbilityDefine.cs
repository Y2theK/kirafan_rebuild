﻿using System.Collections.Generic;

namespace Star {
    public static class AbilityDefine {
        public const eRare CAN_BE_EQUIPE_RARITY = eRare.Star4;

        public enum eAbilitySlotReleaseType {
            None,
            Item
        }

        public enum eAbilitySphereType {
            Hp,
            Atk,
            Mgc,
            Def,
            MDef,
            Passive
        }

        public class AbilitySphereUpgradeRecipe {
            public bool HaveMaterials;
            public bool IsEnoughAmount;
            public bool IsEnoughMainRecipeAmount;
            public bool IsMainRecipe;
            public List<Material> Materials;
            public int Amount;

            public void AddMaterial(int itemID, int itemAmount) {
                if (itemAmount > 0 && Materials != null) {
                    for (int i = 0; i < Materials.Count; i++) {
                        if (Materials[i].m_ItemID == itemID) {
                            Materials[i].m_ItemAmount += itemAmount;
                            return;
                        }
                    }
                    Materials.Add(new Material(itemID, itemAmount));
                }
            }

            public class Material {
                public int m_ItemID = -1;
                public int m_ItemAmount;

                public Material(int itemID, int itemAmount) {
                    m_ItemID = itemID;
                    m_ItemAmount = itemAmount;
                }
            }
        }

        public class AbilityPassiveSet {
            public int m_ItemID;
            public int m_PassiveSkillID;

            public AbilityPassiveSet(int itemID, int passiveSkillID) {
                m_ItemID = itemID;
                m_PassiveSkillID = passiveSkillID;
            }
        }
    }
}
