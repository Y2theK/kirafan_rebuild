﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x0200033D RID: 829
	[Token(Token = "0x20002C0")]
	[StructLayout(3)]
	public sealed class AbilityManager
	{
		// Token: 0x06000B0B RID: 2827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A3B")]
		[Address(RVA = "0x101694640", Offset = "0x1694640", VA = "0x101694640")]
		public AbilityManager()
		{
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A3C")]
		[Address(RVA = "0x10168EC54", Offset = "0x168EC54", VA = "0x10168EC54")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000B0D RID: 2829 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B0E RID: 2830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000001")]
		private event Action m_OnResponse_ReleaseBoard
		{
			[Token(Token = "0x6000A3D")]
			[Address(RVA = "0x101694650", Offset = "0x1694650", VA = "0x101694650")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A3E")]
			[Address(RVA = "0x10169475C", Offset = "0x169475C", VA = "0x10169475C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000B0F RID: 2831 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B10 RID: 2832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000002")]
		private event Action<int> m_OnResponse_ReleaseSlot
		{
			[Token(Token = "0x6000A3F")]
			[Address(RVA = "0x101694868", Offset = "0x1694868", VA = "0x101694868")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A40")]
			[Address(RVA = "0x101694974", Offset = "0x1694974", VA = "0x101694974")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000B11 RID: 2833 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B12 RID: 2834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000003")]
		private event Action m_OnResponse_EquipSphere
		{
			[Token(Token = "0x6000A41")]
			[Address(RVA = "0x101694A80", Offset = "0x1694A80", VA = "0x101694A80")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A42")]
			[Address(RVA = "0x101694B8C", Offset = "0x1694B8C", VA = "0x101694B8C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000B13 RID: 2835 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B14 RID: 2836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000004")]
		private event Action m_OnResponse_UpgradeSphere
		{
			[Token(Token = "0x6000A43")]
			[Address(RVA = "0x101694C98", Offset = "0x1694C98", VA = "0x101694C98")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A44")]
			[Address(RVA = "0x101694DA4", Offset = "0x1694DA4", VA = "0x101694DA4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06000B15 RID: 2837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A45")]
		[Address(RVA = "0x101694EB0", Offset = "0x1694EB0", VA = "0x101694EB0")]
		public void Request_ReleaseBoard(long charaMngID, int itemID, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B16 RID: 2838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A46")]
		[Address(RVA = "0x101694FF0", Offset = "0x1694FF0", VA = "0x101694FF0")]
		private void OnResponse_ReleaseBoard(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B17 RID: 2839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A47")]
		[Address(RVA = "0x101695110", Offset = "0x1695110", VA = "0x101695110")]
		public void Request_ReleaseSlot(long boardMngID, int slotIndex, Action<int> onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B18 RID: 2840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A48")]
		[Address(RVA = "0x10169525C", Offset = "0x169525C", VA = "0x10169525C")]
		private void OnResponse_ReleaseSlot(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B19 RID: 2841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A49")]
		[Address(RVA = "0x1016953B8", Offset = "0x16953B8", VA = "0x1016953B8")]
		public void Request_EquipSphere(long boardMngID, int slotIndex, int itemID, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B1A RID: 2842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A4A")]
		[Address(RVA = "0x10169551C", Offset = "0x169551C", VA = "0x10169551C")]
		private void OnResponse_Equip(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B1B RID: 2843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A4B")]
		[Address(RVA = "0x10169563C", Offset = "0x169563C", VA = "0x10169563C")]
		public void Request_UpgradeSphere(long boardMngID, int slotIndex, int srcItemID, int[] materialItemIDs, int[] materialItemAmounts, int amount, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B1C RID: 2844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A4C")]
		[Address(RVA = "0x1016957E8", Offset = "0x16957E8", VA = "0x1016957E8")]
		private void OnResponse_UpgradeSphere(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x04000C46 RID: 3142
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40009CC")]
		private int m_RequestedReleaseSlotIndex;
	}
}
