﻿using System;
using System.Collections.Generic;

namespace Star {
    [Serializable]
    public class AbilityParam {
        public int AbilityBoardID { get; set; }
        public int[] EquipItemIDs { get; set; }

        public AbilityParam() {
            AbilityBoardID = -1;
            EquipItemIDs = new int[0];
        }

        public List<int> GetAvailableEquipItemIDs() {
            List<int> result = new List<int>();
            if (EquipItemIDs != null) {
                for (int i = 0; i < EquipItemIDs.Length; i++) {
                    if (EquipItemIDs[i] != -1) {
                        result.Add(EquipItemIDs[i]);
                    }
                }
            }
            return result;
        }
    }
}
