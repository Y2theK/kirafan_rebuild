﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200049D RID: 1181
	[Token(Token = "0x2000395")]
	[StructLayout(3)]
	public class AbilitySlotItemRecipesDB : ScriptableObject
	{
		// Token: 0x060013F1 RID: 5105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A6")]
		[Address(RVA = "0x101695A88", Offset = "0x1695A88", VA = "0x101695A88")]
		public AbilitySlotItemRecipesDB()
		{
		}

		// Token: 0x0400160B RID: 5643
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FF1")]
		public AbilitySlotItemRecipesDB_Param[] m_Params;
	}
}
