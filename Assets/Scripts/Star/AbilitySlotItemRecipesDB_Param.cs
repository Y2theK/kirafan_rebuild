﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200049C RID: 1180
	[Token(Token = "0x2000394")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct AbilitySlotItemRecipesDB_Param
	{
		// Token: 0x04001607 RID: 5639
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FED")]
		public int m_ID;

		// Token: 0x04001608 RID: 5640
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000FEE")]
		public int m_ItemID;

		// Token: 0x04001609 RID: 5641
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FEF")]
		public int m_ItemAmount;

		// Token: 0x0400160A RID: 5642
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4000FF0")]
		public int m_Amount;
	}
}
