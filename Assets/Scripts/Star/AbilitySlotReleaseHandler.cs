﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200033E RID: 830
	[Token(Token = "0x20002C1")]
	[StructLayout(3)]
	public class AbilitySlotReleaseHandler : MonoBehaviour
	{
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000B1D RID: 2845 RVA: 0x00004680 File Offset: 0x00002880
		[Token(Token = "0x17000073")]
		public AbilitySlotReleaseHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x6000A4D")]
			[Address(RVA = "0x101695BC8", Offset = "0x1695BC8", VA = "0x101695BC8")]
			get
			{
				return AbilitySlotReleaseHandler.eAnimType.Effect;
			}
		}

		// Token: 0x06000B1E RID: 2846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A4E")]
		[Address(RVA = "0x101695BD0", Offset = "0x1695BD0", VA = "0x101695BD0")]
		private void Awake()
		{
		}

		// Token: 0x06000B1F RID: 2847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A4F")]
		[Address(RVA = "0x101695DC8", Offset = "0x1695DC8", VA = "0x101695DC8")]
		private void OnDestroy()
		{
		}

		// Token: 0x06000B20 RID: 2848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A50")]
		[Address(RVA = "0x101695DCC", Offset = "0x1695DCC", VA = "0x101695DCC")]
		public void Destroy()
		{
		}

		// Token: 0x06000B21 RID: 2849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A51")]
		[Address(RVA = "0x101695E1C", Offset = "0x1695E1C", VA = "0x101695E1C")]
		private void Update()
		{
		}

		// Token: 0x06000B22 RID: 2850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A52")]
		[Address(RVA = "0x101695F3C", Offset = "0x1695F3C", VA = "0x101695F3C")]
		public void Move(Vector3 pos)
		{
		}

		// Token: 0x06000B23 RID: 2851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A53")]
		[Address(RVA = "0x101695F94", Offset = "0x1695F94", VA = "0x101695F94")]
		public void Play(AbilitySlotReleaseHandler.eAnimType animType)
		{
		}

		// Token: 0x06000B24 RID: 2852 RVA: 0x00004698 File Offset: 0x00002898
		[Token(Token = "0x6000A54")]
		[Address(RVA = "0x101695E60", Offset = "0x1695E60", VA = "0x101695E60")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06000B25 RID: 2853 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A55")]
		[Address(RVA = "0x101696054", Offset = "0x1696054", VA = "0x101696054")]
		private string GetAnimPlayKey(AbilitySlotReleaseHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x06000B26 RID: 2854 RVA: 0x000046B0 File Offset: 0x000028B0
		[Token(Token = "0x6000A56")]
		[Address(RVA = "0x1016960A4", Offset = "0x16960A4", VA = "0x1016960A4")]
		public float GetAnimationPlayTime()
		{
			return 0f;
		}

		// Token: 0x06000B27 RID: 2855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A57")]
		[Address(RVA = "0x1016960D0", Offset = "0x16960D0", VA = "0x1016960D0")]
		public AbilitySlotReleaseHandler()
		{
		}

		// Token: 0x04000C4B RID: 3147
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009D1")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x04000C4C RID: 3148
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009D2")]
		public Transform m_Transform;

		// Token: 0x04000C4D RID: 3149
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009D3")]
		public Animation m_Anim;

		// Token: 0x04000C4E RID: 3150
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009D4")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000C4F RID: 3151
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40009D5")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x04000C50 RID: 3152
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40009D6")]
		public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x04000C51 RID: 3153
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40009D7")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04000C52 RID: 3154
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40009D8")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x04000C53 RID: 3155
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40009D9")]
		private AbilitySlotReleaseHandler.eAnimType m_CurrentAnimType;

		// Token: 0x0200033F RID: 831
		[Token(Token = "0x2000D52")]
		public enum eAnimType
		{
			// Token: 0x04000C55 RID: 3157
			[Token(Token = "0x4005600")]
			None = -1,
			// Token: 0x04000C56 RID: 3158
			[Token(Token = "0x4005601")]
			Effect,
			// Token: 0x04000C57 RID: 3159
			[Token(Token = "0x4005602")]
			Num
		}
	}
}
