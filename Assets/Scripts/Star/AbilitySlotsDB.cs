﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200049F RID: 1183
	[Token(Token = "0x2000397")]
	[StructLayout(3)]
	public class AbilitySlotsDB : ScriptableObject
	{
		// Token: 0x060013F2 RID: 5106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A7")]
		[Address(RVA = "0x1016961BC", Offset = "0x16961BC", VA = "0x1016961BC")]
		public AbilitySlotsDB()
		{
		}

		// Token: 0x04001612 RID: 5650
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FF8")]
		public AbilitySlotsDB_Param[] m_Params;
	}
}
