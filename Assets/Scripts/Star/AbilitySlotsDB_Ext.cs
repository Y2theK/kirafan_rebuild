﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004F3 RID: 1267
	[Token(Token = "0x20003E9")]
	[StructLayout(3)]
	public static class AbilitySlotsDB_Ext
	{
		// Token: 0x06001505 RID: 5381 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013BA")]
		[Address(RVA = "0x1016961C4", Offset = "0x16961C4", VA = "0x1016961C4")]
		public static List<AbilitySlotsDB_Param> GetSlotParams(this AbilitySlotsDB self, int abilityBoardID)
		{
			return null;
		}

		// Token: 0x06001506 RID: 5382 RVA: 0x00008BE0 File Offset: 0x00006DE0
		[Token(Token = "0x60013BB")]
		[Address(RVA = "0x101696398", Offset = "0x1696398", VA = "0x101696398")]
		private static int CompareSlotParams(AbilitySlotsDB_Param A, AbilitySlotsDB_Param B)
		{
			return 0;
		}
	}
}
