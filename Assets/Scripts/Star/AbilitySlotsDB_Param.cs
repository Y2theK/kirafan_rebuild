﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200049E RID: 1182
	[Token(Token = "0x2000396")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct AbilitySlotsDB_Param
	{
		// Token: 0x0400160C RID: 5644
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FF2")]
		public int m_ID;

		// Token: 0x0400160D RID: 5645
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000FF3")]
		public int m_BoardID;

		// Token: 0x0400160E RID: 5646
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FF4")]
		public int m_SlotIndex;

		// Token: 0x0400160F RID: 5647
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4000FF5")]
		public int m_IsSp;

		// Token: 0x04001610 RID: 5648
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FF6")]
		public int m_ReleaseType;

		// Token: 0x04001611 RID: 5649
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000FF7")]
		public int m_ReleaseRecipeID;
	}
}
