﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004A1 RID: 1185
	[Token(Token = "0x2000399")]
	[StructLayout(3)]
	public class AbilitySphereRecipesDB : ScriptableObject
	{
		// Token: 0x060013F3 RID: 5107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A8")]
		[Address(RVA = "0x1016963B8", Offset = "0x16963B8", VA = "0x1016963B8")]
		public AbilitySphereRecipesDB()
		{
		}

		// Token: 0x04001619 RID: 5657
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FFF")]
		public AbilitySphereRecipesDB_Param[] m_Params;
	}
}
