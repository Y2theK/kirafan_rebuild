﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004F4 RID: 1268
	[Token(Token = "0x20003EA")]
	[StructLayout(3)]
	public static class AbilitySphereRecipesDB_Ext
	{
		// Token: 0x06001507 RID: 5383 RVA: 0x00008BF8 File Offset: 0x00006DF8
		[Token(Token = "0x60013BC")]
		[Address(RVA = "0x1016963C0", Offset = "0x16963C0", VA = "0x1016963C0")]
		public static AbilitySphereRecipesDB_Param? GetParamFromSrcItemID(this AbilitySphereRecipesDB self, int srcItemID)
		{
			return null;
		}

		// Token: 0x06001508 RID: 5384 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013BD")]
		[Address(RVA = "0x10169652C", Offset = "0x169652C", VA = "0x10169652C")]
		public static List<AbilitySphereRecipesDB_Param> GetRecipeHistory(this AbilitySphereRecipesDB self, int srcItemID)
		{
			return null;
		}
	}
}
