﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004A0 RID: 1184
	[Token(Token = "0x2000398")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct AbilitySphereRecipesDB_Param
	{
		// Token: 0x04001613 RID: 5651
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FF9")]
		public int m_ID;

		// Token: 0x04001614 RID: 5652
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000FFA")]
		public int m_DstItemID;

		// Token: 0x04001615 RID: 5653
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FFB")]
		public int m_BaseItemID;

		// Token: 0x04001616 RID: 5654
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4000FFC")]
		public int m_SrcItemID;

		// Token: 0x04001617 RID: 5655
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FFD")]
		public int m_SrcItemAmount;

		// Token: 0x04001618 RID: 5656
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000FFE")]
		public int m_Amount;
	}
}
