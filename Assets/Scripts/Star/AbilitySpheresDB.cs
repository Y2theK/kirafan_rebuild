﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004A3 RID: 1187
	[Token(Token = "0x200039B")]
	[StructLayout(3)]
	public class AbilitySpheresDB : ScriptableObject
	{
		// Token: 0x060013F4 RID: 5108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A9")]
		[Address(RVA = "0x101696864", Offset = "0x1696864", VA = "0x101696864")]
		public AbilitySpheresDB()
		{
		}

		// Token: 0x0400161E RID: 5662
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001004")]
		public AbilitySpheresDB_Param[] m_Params;
	}
}
