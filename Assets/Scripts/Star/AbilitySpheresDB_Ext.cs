﻿namespace Star {
    public static class AbilitySpheresDB_Ext {
        public static AbilitySpheresDB_Param? GetParam(this AbilitySpheresDB self, int id) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == id) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static AbilitySpheresDB_Param? GetParamFromItemID(this AbilitySpheresDB self, int itemID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ItemID == itemID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static void CalcStatus(this AbilitySpheresDB self, int[] itemIDs, out int out_hp, out int out_atk, out int out_mgc, out int out_def, out int out_mdef) {
            out_hp = 0;
            out_atk = 0;
            out_mgc = 0;
            out_def = 0;
            out_mdef = 0;
            if (itemIDs != null) {
                for (int i = 0; i < itemIDs.Length; i++) {
                    AbilitySpheresDB_Param? param = self.GetParamFromItemID(itemIDs[i]);
                    if (param.HasValue) {
                        switch ((AbilityDefine.eAbilitySphereType)param.Value.m_Type) {
                            case AbilityDefine.eAbilitySphereType.Hp:
                                out_hp += param.Value.m_Value;
                                break;
                            case AbilityDefine.eAbilitySphereType.Atk:
                                out_atk += param.Value.m_Value;
                                break;
                            case AbilityDefine.eAbilitySphereType.Mgc:
                                out_mgc += param.Value.m_Value;
                                break;
                            case AbilityDefine.eAbilitySphereType.Def:
                                out_def += param.Value.m_Value;
                                break;
                            case AbilityDefine.eAbilitySphereType.MDef:
                                out_mdef += param.Value.m_Value;
                                break;
                        }
                    }
                }
            }
        }

        public static int GetPassiveSkillID(this AbilitySpheresDB_Param self) {
            return self.m_Type == (int)AbilityDefine.eAbilitySphereType.Passive ? self.m_Value : -1;
        }
    }
}
