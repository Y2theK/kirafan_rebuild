﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004A2 RID: 1186
	[Token(Token = "0x200039A")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct AbilitySpheresDB_Param
	{
		// Token: 0x0400161A RID: 5658
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001000")]
		public int m_ID;

		// Token: 0x0400161B RID: 5659
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001001")]
		public int m_ItemID;

		// Token: 0x0400161C RID: 5660
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001002")]
		public int m_Type;

		// Token: 0x0400161D RID: 5661
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001003")]
		public int m_Value;
	}
}
