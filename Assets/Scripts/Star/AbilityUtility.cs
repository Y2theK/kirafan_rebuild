﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000340 RID: 832
	[Token(Token = "0x20002C2")]
	[StructLayout(3)]
	public static class AbilityUtility
	{
		// Token: 0x06000B28 RID: 2856 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A58")]
		[Address(RVA = "0x101696D14", Offset = "0x1696D14", VA = "0x101696D14")]
		public static AbilityDefine.AbilitySphereUpgradeRecipe CalcAbilitySphereUpgradeRecipe(int upgradeSrcItemID)
		{
			return null;
		}

		// Token: 0x06000B29 RID: 2857 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A59")]
		[Address(RVA = "0x101697584", Offset = "0x1697584", VA = "0x101697584")]
		public static List<AbilityDefine.AbilityPassiveSet> CreateSortedAbilityPassiveSets(AbilityParam abilityParam)
		{
			return null;
		}

		// Token: 0x06000B2A RID: 2858 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A5A")]
		[Address(RVA = "0x10169789C", Offset = "0x169789C", VA = "0x10169789C")]
		public static List<int> GetSortedAbilitySphereItemIdList()
		{
			return null;
		}

		// Token: 0x06000B2B RID: 2859 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A5B")]
		[Address(RVA = "0x101697AE0", Offset = "0x1697AE0", VA = "0x101697AE0")]
		public static string GetAbilitySphereDescription(int itemID)
		{
			return null;
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x000046C8 File Offset: 0x000028C8
		[Token(Token = "0x6000A5C")]
		[Address(RVA = "0x101697D38", Offset = "0x1697D38", VA = "0x101697D38")]
		public static int GetFirstLockSlotIndex(List<AbilitySlotsDB_Param> slotParam)
		{
			return 0;
		}
	}
}
