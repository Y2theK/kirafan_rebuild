﻿using MsgPack;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Star {
    public class AccessSaveData {
        private static readonly AccessSaveData instance = new AccessSaveData();
        private static readonly string EncryptKey = "7gyPmqc54dVNB3Te6pIpd2THj2y3hjOP";
        private static readonly int EncryptPasswordCount = 16;
        private static readonly string FilePrefix = string.Empty;
        private static readonly string SaveFile = Application.persistentDataPath + "/" + FilePrefix + "a.d";
        private static readonly string SaveFileBak = Application.persistentDataPath + "/" + FilePrefix + "a.d2";
        private static readonly string PasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private readonly SaveDataVersion m_LatestVersion;
        private SaveData m_SaveData;

        public static AccessSaveData Inst => instance;

        public string UUID {
            get => m_SaveData.m_UUID;
            set => m_SaveData.m_UUID = value;
        }

        public string AccessToken {
            get => m_SaveData.m_AccessToken;
            set => m_SaveData.m_AccessToken = value;
        }

        public int ConfirmedVer {
            get => m_SaveData.m_ConfirmedVer;
            set => m_SaveData.m_ConfirmedVer = value;
        }

        public string MyCode {
            get => m_SaveData.m_MyCode;
            set => m_SaveData.m_MyCode = value;
        }

        public bool SaveDataAutoDelete { get; set; }

        protected AccessSaveData() {
            m_LatestVersion = SaveDataVersion._171101;
            m_SaveData = new SaveData();
        }

        public void Reset() {
            if (m_SaveData != null) {
                m_SaveData.Reset();
            }
        }

        public void Save() {
            ObjectPacker objectPacker = new ObjectPacker();
            byte[] src = objectPacker.Pack(m_SaveData);
            if (File.Exists(SaveFile)) {
                File.Copy(SaveFile, SaveFileBak, true);
            }
            try {
                EncryptAes(src, EncryptKey, EncryptPasswordCount, out string pw, out byte[] dst);
                using (var fs = new FileStream(SaveFile, FileMode.Create, FileAccess.Write))
                using (var writer = new BinaryWriter(fs)) {
                    int header = Random.Range(-2147483647, int.MaxValue);
                    writer.Write((int)(header & 0xFFFF00FF) | (sbyte)m_LatestVersion << 8);
                    byte[] bytes = Encoding.UTF8.GetBytes(pw);
                    for (int i = 0; i < bytes.Length; ++i) {
                        bytes[i] += (byte)(0x60 + i);
                    }
                    writer.Write((byte)(bytes.Length + (header & 0xFF)));
                    writer.Write(bytes);
                    writer.Write(dst.Length);
                    writer.Write(dst);
                }
            } catch {
            }
        }

        public void Load() {
            Load(SaveFile);
        }

        public bool Load(string path) {
            if (!File.Exists(path)) {
                return false;
            }
            bool versionMismatch = false;
            try {
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read)) {
                    string pw = null;
                    byte[] array = null;
                    using (var reader = new System.IO.BinaryReader(fs)) {
                        int header = reader.ReadInt32();
                        int version = (header & 65280) >> 8;
                        if (version != (int)m_LatestVersion) {
                            versionMismatch = true;
                        } else {
                            int count = reader.ReadByte() - (header & 0x7F);
                            array = reader.ReadBytes(count);
                            for (int i = 0; i < array.Length; ++i) {
                                array[i] -= (byte)(0x60 + i);
                            }
                            pw = Encoding.UTF8.GetString(array);
                            count = reader.ReadInt32();
                            array = reader.ReadBytes(count);
                        }
                    }
                    if (!versionMismatch) {
                        ObjectPacker objectPacker = new ObjectPacker();
                        DecryptAes(array, EncryptKey, pw, out byte[] buf);
                        m_SaveData = objectPacker.Unpack<SaveData>(buf);
                    }
                }
            } catch {
                bool backupLoaded = false;
                if (SaveFileBak != path) {
                    backupLoaded = Load(SaveFileBak);
                }
                if (!backupLoaded) {
                    return false;
                }
            }
            if (versionMismatch) {
                DeleteAllSaveData();
                SaveDataAutoDelete = true;
                return true;
            }
            AfterLoad();
            return true;
        }

        public void DeleteAllSaveData() {
            DeleteSaveData(SaveFile);
            DeleteSaveData(SaveFileBak);
        }

        public void DeleteSaveData(string path) {
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }

        private void AfterLoad() { }

        private static void EncryptAes(byte[] src, string encryptKey, int pwSize, out string pw, out byte[] dst) {
            pw = CreatePassword(pwSize);
            dst = null;
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(encryptKey);
                byte[] iv = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateEncryptor(key, iv))
                using (var stream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(src, 0, src.Length);
                    cryptoStream.FlushFinalBlock();
                    dst = stream.ToArray();
                }
            }
        }

        private static void DecryptAes(byte[] src, string encryptKey, string pw, out byte[] dst) {
            dst = new byte[src.Length];
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(encryptKey);
                byte[] iv = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateDecryptor(key, iv))
                using (var stream = new MemoryStream(src))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read)) {
                    cryptoStream.Read(dst, 0, dst.Length);
                }
            }
        }

        private static string CreatePassword(int count) {
            StringBuilder stringBuilder = new StringBuilder(count);
            for (int i = count - 1; i >= 0; i--) {
                char value = PasswordChars[Random.Range(0, PasswordChars.Length)];
                stringBuilder.Append(value);
            }
            return stringBuilder.ToString();
        }

        public enum SaveDataVersion {
            _170404 = 17,
            _170713,
            _171101,
            _latest
        }

        public enum Amount {
            High,
            Middle,
            Low
        }

        private class SaveData {
            public int m_ConfirmedVer;
            public string m_UUID;
            public string m_AccessToken;
            public string m_MyCode;

            public SaveData() {
                Reset();
            }

            public void Reset() {
                m_ConfirmedVer = 0;
                m_AccessToken = null;
                m_MyCode = null;
                m_UUID = null;
            }
        }
    }
}
