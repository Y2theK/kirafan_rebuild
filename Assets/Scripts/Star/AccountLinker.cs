﻿using Meige;
using UnityEngine;
using WWWTypes;
using Req = PlayerRequestTypes;
using Resp = PlayerResponseTypes;

namespace Star {
    public class AccountLinker : MonoBehaviour {
        private eResult m_ActiveResult;
        private eStep m_Step;
        private string m_WorkUUID;

        public static void CheckCreate() {
            CGlobalInstance.CreateInstance();
            CGlobalInstance.EntryClass<AccountLinker>();
        }

        public static void SetAutoDestroy() {
            AccountLinker linker = CGlobalInstance.GetClass<AccountLinker>();
            linker.m_Step = eStep.AutoDestory;
        }

        private string CreateUserAccountID() {
            return string.Format("{0}_{1}", CRC32.Calc(IAcountController.GetUserName()), IAcountController.GetUserID());
        }

        public static void SetAcountLinkData() {
            AccountLinker linker = CGlobalInstance.GetClass<AccountLinker>();
            linker.m_ActiveResult = eResult.Access;
            linker.m_Step = eStep.CheckLogIn;
            linker.enabled = true;
        }

        public static void BuildUpAcountLinkPlayer() {
            AccountLinker linker = CGlobalInstance.GetClass<AccountLinker>();
            linker.m_ActiveResult = eResult.Access;
            linker.m_Step = eStep.MovePlayerLogIn;
            linker.enabled = true;
        }

        private void Update() {
            switch (m_Step) {
                case eStep.Non:
                    enabled = false;
                    break;
                case eStep.CheckLogIn:
                    if (!IAcountController.IsSetup()) {
                        IAcountController.Create();
                    }
                    m_Step = eStep.LinkSet;
                    IAcountController.ResetCallback();
                    break;
                case eStep.LinkSet: {
                    IAcountController.eState state = IAcountController.GetState();
                    Debug.Log("Link LogIn Check : " + state);
                    if (state == IAcountController.eState.Online) {
                        m_Step = eStep.ComCheck;
                        SendPlayerLinkCom();
                    } else if (state == IAcountController.eState.SignOut) {
                        m_Step = eStep.End;
                        m_ActiveResult = eResult.LogInError;
                    }
                    break;
                }
                case eStep.End:
                    m_Step = eStep.Non;
                    break;
                case eStep.MovePlayerLogIn:
                    if (!IAcountController.IsSetup()) {
                        IAcountController.Create();
                    }
                    m_Step = eStep.LinkMoveCom;
                    IAcountController.ResetCallback();
                    break;
                case eStep.LinkMoveCom: {
                    IAcountController.eState state = IAcountController.GetState();
                    Debug.Log("LinkMove LogIn Check : " + state);
                    if (state == IAcountController.eState.Online) {
                        m_Step = eStep.ComCheck;
                        SendLinkMoveCom();
                    } else if (state == IAcountController.eState.SignOut) {
                        m_Step = eStep.End;
                        m_ActiveResult = eResult.LogInError;
                    }
                    break;
                }
                case eStep.AutoDestory:
                    Destroy(this);
                    break;
            }
        }

        private void SendPlayerLinkCom() {
            Req.Set req = new Req.Set() {
                name = GameSystem.Inst.UserDataMng.UserData.Name,
                comment = GameSystem.Inst.UserDataMng.UserData.Comment,
                linkId = CreateUserAccountID(),
                managedFavoriteCharacterIds = GameSystem.Inst.UserDataMng.UserFavoriteData.GetManageedCharacterIDs()
            };
            MeigewwwParam param = PlayerRequest.Set(req, CallbackUserLinkSet);
            NetworkQueueManager.Request(param);
            Debug.Log("Link Set : " + req.linkId);
            GameSystem.Inst.ConnectingIcon.Open();
        }

        private void CallbackUserLinkSet(MeigewwwParam wwwParam) {
            Resp.Set resp = PlayerResponse.Set(wwwParam);
            if (resp != null) {
                m_Step = eStep.End;
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
                    m_ActiveResult = eResult.ComError;
                    Debug.Log("AccountLinker.CallbackUserLinkSet ComError");
                    return;
                }
                m_ActiveResult = eResult.Success;
                Debug.Log("AccountLinker.CallbackUserLinkSet Success");
            }
        }

        private void SendLinkMoveCom() {
            Req.Linkmoveset req = new Req.Linkmoveset() {
                linkId = CreateUserAccountID(),
                uuid = APIUtility.GenerateUUID(),
                platform = Platform.Get()
            };
            m_WorkUUID = req.uuid;
            MeigewwwParam param = PlayerRequest.Linkmoveset(req, CallbackUserLinkMove);
            NetworkQueueManager.Request(param);
            Debug.Log("Link Move " + req.linkId + " : " + req.uuid);
            GameSystem.Inst.ConnectingIcon.Open();
        }

        private void CallbackUserLinkMove(MeigewwwParam wwwParam) {
            Resp.Linkmoveset resp = PlayerResponse.Linkmoveset(wwwParam);
            if (resp != null) {
                m_Step = eStep.End;
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
                    m_ActiveResult = eResult.ComError;
                    Debug.Log("AccountLinker.CallbackUserLinkMove ComError");
                    return;
                }
                AccessSaveData.Inst.UUID = m_WorkUUID;
                AccessSaveData.Inst.AccessToken = resp.accessToken;
                AccessSaveData.Inst.Save();
                m_ActiveResult = eResult.Success;
                Debug.Log("AccountLinker.CallbackUserLinkMove Success");
            }
        }

        public static eResult GetState() {
            return CGlobalInstance.GetClass<AccountLinker>().m_ActiveResult;
        }

        public enum eResult {
            Access,
            Success,
            LogInError,
            ComError
        }

        private enum eStep {
            Non,
            CheckLogIn,
            LinkSet,
            ComCheck,
            End,
            MovePlayerLogIn,
            LinkMoveCom,
            AutoDestory
        }
    }
}
