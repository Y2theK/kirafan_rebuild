﻿using UnityEngine;

namespace Star {
    public static class AchievementDefine {
        public const int DEFAULT_ID = 0;

        public static Color32 SELECTED_COLOR = new Color32(0xFC, 0xFD, 0xC1, 0xFF);

        public enum eFilterType {
            None,
            Acquired,
            All,
            Unacquired
        }
    }
}
