﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x02000346 RID: 838
	[Token(Token = "0x20002C5")]
	[StructLayout(3)]
	public sealed class AchievementManager
	{
		// Token: 0x06000B3A RID: 2874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A6A")]
		[Address(RVA = "0x10169A0AC", Offset = "0x169A0AC", VA = "0x10169A0AC")]
		public AchievementManager()
		{
		}

		// Token: 0x06000B3B RID: 2875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A6B")]
		[Address(RVA = "0x10168EC44", Offset = "0x168EC44", VA = "0x10168EC44")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06000B3C RID: 2876 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A6C")]
		[Address(RVA = "0x10169A0B4", Offset = "0x169A0B4", VA = "0x10169A0B4")]
		public eTitleType[] GetTitles()
		{
			return null;
		}

		// Token: 0x06000B3D RID: 2877 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A6D")]
		[Address(RVA = "0x10169A0BC", Offset = "0x169A0BC", VA = "0x10169A0BC")]
		public AchievementManager.AchievementData[] GetAchievements(eTitleType titleType)
		{
			return null;
		}

		// Token: 0x06000B3E RID: 2878 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A6E")]
		[Address(RVA = "0x10169A2D8", Offset = "0x169A2D8", VA = "0x10169A2D8")]
		public AchievementManager.AchievementData[] GetAchievements(eTitleType titleType, AchievementDefine.eFilterType filterType)
		{
			return null;
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A6F")]
		[Address(RVA = "0x10169A630", Offset = "0x169A630", VA = "0x10169A630")]
		public AchievementManager.AchievementData GetAchievementData(int id)
		{
			return null;
		}

		// Token: 0x06000B40 RID: 2880 RVA: 0x000046F8 File Offset: 0x000028F8
		[Token(Token = "0x6000A70")]
		[Address(RVA = "0x10169A720", Offset = "0x169A720", VA = "0x10169A720")]
		public bool IsNew(eTitleType titleType)
		{
			return default(bool);
		}

		// Token: 0x06000B41 RID: 2881 RVA: 0x00004710 File Offset: 0x00002910
		[Token(Token = "0x6000A71")]
		[Address(RVA = "0x10169A818", Offset = "0x169A818", VA = "0x10169A818")]
		public bool IsAnyNew()
		{
			return default(bool);
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000B42 RID: 2882 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B43 RID: 2883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000005")]
		private event Action m_OnResponse_ShownAll
		{
			[Token(Token = "0x6000A72")]
			[Address(RVA = "0x10169A8C4", Offset = "0x169A8C4", VA = "0x10169A8C4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A73")]
			[Address(RVA = "0x10169A9D0", Offset = "0x169A9D0", VA = "0x10169A9D0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000B44 RID: 2884 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B45 RID: 2885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000006")]
		private event Action m_OnResponse_GetAll
		{
			[Token(Token = "0x6000A74")]
			[Address(RVA = "0x10169AADC", Offset = "0x169AADC", VA = "0x10169AADC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A75")]
			[Address(RVA = "0x10169ABE8", Offset = "0x169ABE8", VA = "0x10169ABE8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000B46 RID: 2886 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000B47 RID: 2887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000007")]
		private event Action m_OnResponse_Set
		{
			[Token(Token = "0x6000A76")]
			[Address(RVA = "0x10169ACF4", Offset = "0x169ACF4", VA = "0x10169ACF4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000A77")]
			[Address(RVA = "0x10169AE00", Offset = "0x169AE00", VA = "0x10169AE00")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06000B48 RID: 2888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A78")]
		[Address(RVA = "0x10169AF0C", Offset = "0x169AF0C", VA = "0x10169AF0C")]
		public void Request_Shown(HashSet<eTitleType> titleTypes, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B49 RID: 2889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A79")]
		[Address(RVA = "0x10169B274", Offset = "0x169B274", VA = "0x10169B274")]
		private void OnResponse_ShownAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B4A RID: 2890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7A")]
		[Address(RVA = "0x10169B408", Offset = "0x169B408", VA = "0x10169B408")]
		public void Request_GetAll(Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B4B RID: 2891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7B")]
		[Address(RVA = "0x10169B510", Offset = "0x169B510", VA = "0x10169B510")]
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B4C RID: 2892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7C")]
		[Address(RVA = "0x10169BA64", Offset = "0x169BA64", VA = "0x10169BA64")]
		public void Request_Set(int id, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06000B4D RID: 2893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7D")]
		[Address(RVA = "0x10169BB88", Offset = "0x169BB88", VA = "0x10169BB88")]
		private void OnResponse_Set(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06000B4E RID: 2894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7E")]
		[Address(RVA = "0x10169B5A0", Offset = "0x169B5A0", VA = "0x10169B5A0")]
		private void SetAchievementsFromWWWType(PlayerAchievement[] src)
		{
		}

		// Token: 0x04000C70 RID: 3184
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40009DF")]
		private AchievementManager.AchievementData[] m_Achievements;

		// Token: 0x04000C71 RID: 3185
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009E0")]
		private eTitleType[] m_SortedTitleTypes;

		// Token: 0x04000C75 RID: 3189
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40009E4")]
		private List<int> m_RequestedShownTitleTypes;

		// Token: 0x02000347 RID: 839
		[Token(Token = "0x2000D56")]
		public class AchievementData
		{
			// Token: 0x06000B4F RID: 2895 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D94")]
			[Address(RVA = "0x10169BC80", Offset = "0x169BC80", VA = "0x10169BC80")]
			public AchievementData()
			{
			}

			// Token: 0x04000C76 RID: 3190
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005616")]
			public int m_ID;

			// Token: 0x04000C77 RID: 3191
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005617")]
			public eTitleType m_TitleType;

			// Token: 0x04000C78 RID: 3192
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005618")]
			public bool m_IsEnable;

			// Token: 0x04000C79 RID: 3193
			[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
			[Token(Token = "0x4005619")]
			public bool m_IsNew;

			// Token: 0x04000C7A RID: 3194
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400561A")]
			public string m_Desc;
		}
	}
}
