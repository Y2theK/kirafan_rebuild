﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200097B RID: 2427
	[Token(Token = "0x20006DD")]
	[StructLayout(3)]
	public class ActXlsKeyAnimeWait : ActXlsKeyBase
	{
		// Token: 0x06002860 RID: 10336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002522")]
		[Address(RVA = "0x10169BC90", Offset = "0x169BC90", VA = "0x10169BC90", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002861 RID: 10337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002523")]
		[Address(RVA = "0x10169BE30", Offset = "0x169BE30", VA = "0x10169BE30")]
		public ActXlsKeyAnimeWait()
		{
		}

		// Token: 0x040038C9 RID: 14537
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002915")]
		public float m_OffsetTime;
	}
}
