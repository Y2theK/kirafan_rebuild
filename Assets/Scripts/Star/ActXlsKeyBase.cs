﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000978 RID: 2424
	[Token(Token = "0x20006DA")]
	[StructLayout(3)]
	public class ActXlsKeyBase
	{
		// Token: 0x06002859 RID: 10329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600251B")]
		[Address(RVA = "0x10169BD0C", Offset = "0x169BD0C", VA = "0x10169BD0C", Slot = "4")]
		public virtual void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600285A RID: 10330 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600251C")]
		[Address(RVA = "0x10169BE40", Offset = "0x169BE40", VA = "0x10169BE40")]
		public static ActXlsKeyBase CreateKey(int ftype)
		{
			return null;
		}

		// Token: 0x0600285B RID: 10331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600251D")]
		[Address(RVA = "0x10169BE38", Offset = "0x169BE38", VA = "0x10169BE38")]
		public ActXlsKeyBase()
		{
		}

		// Token: 0x040038C1 RID: 14529
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400290D")]
		public eRoomScriptCommand m_Type;

		// Token: 0x040038C2 RID: 14530
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400290E")]
		public int m_Time;
	}
}
