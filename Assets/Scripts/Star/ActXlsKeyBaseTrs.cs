﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000986 RID: 2438
	[Token(Token = "0x20006E8")]
	[StructLayout(3)]
	public class ActXlsKeyBaseTrs : ActXlsKeyBase
	{
		// Token: 0x06002876 RID: 10358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002538")]
		[Address(RVA = "0x10169C118", Offset = "0x169C118", VA = "0x10169C118", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002877 RID: 10359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002539")]
		[Address(RVA = "0x10169C268", Offset = "0x169C268", VA = "0x10169C268")]
		public ActXlsKeyBaseTrs()
		{
		}

		// Token: 0x040038E5 RID: 14565
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002931")]
		public string m_TargetName;

		// Token: 0x040038E6 RID: 14566
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002932")]
		public Vector3 m_Pos;

		// Token: 0x040038E7 RID: 14567
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002933")]
		public float m_Rot;
	}
}
