﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000994 RID: 2452
	[Token(Token = "0x20006F6")]
	[StructLayout(3)]
	public class ActXlsKeyBaseTrsNonRev : ActXlsKeyBase
	{
		// Token: 0x06002892 RID: 10386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002554")]
		[Address(RVA = "0x10169C270", Offset = "0x169C270", VA = "0x10169C270", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002893 RID: 10387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002555")]
		[Address(RVA = "0x10169C3C0", Offset = "0x169C3C0", VA = "0x10169C3C0")]
		public ActXlsKeyBaseTrsNonRev()
		{
		}

		// Token: 0x04003905 RID: 14597
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002951")]
		public string m_TargetName;

		// Token: 0x04003906 RID: 14598
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002952")]
		public Vector3 m_Pos;

		// Token: 0x04003907 RID: 14599
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002953")]
		public float m_Rot;
	}
}
