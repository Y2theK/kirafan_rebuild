﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200097C RID: 2428
	[Token(Token = "0x20006DE")]
	[StructLayout(3)]
	public class ActXlsKeyBind : ActXlsKeyBase
	{
		// Token: 0x06002862 RID: 10338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002524")]
		[Address(RVA = "0x10169C3C8", Offset = "0x169C3C8", VA = "0x10169C3C8", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002863 RID: 10339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002525")]
		[Address(RVA = "0x10169C5A8", Offset = "0x169C5A8", VA = "0x10169C5A8")]
		public ActXlsKeyBind()
		{
		}

		// Token: 0x040038CA RID: 14538
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002916")]
		public string m_BindHrcName;

		// Token: 0x040038CB RID: 14539
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002917")]
		public string m_TargetName;

		// Token: 0x040038CC RID: 14540
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002918")]
		public CActScriptKeyBind.eNameAttach m_Attach;

		// Token: 0x040038CD RID: 14541
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002919")]
		public int m_LinkTime;

		// Token: 0x040038CE RID: 14542
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400291A")]
		public CActScriptKeyBind.eCalc m_CalcType;
	}
}
