﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000989 RID: 2441
	[Token(Token = "0x20006EB")]
	[StructLayout(3)]
	public class ActXlsKeyBindRot : ActXlsKeyBase
	{
		// Token: 0x0600287C RID: 10364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253E")]
		[Address(RVA = "0x10169C5B0", Offset = "0x169C5B0", VA = "0x10169C5B0", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600287D RID: 10365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253F")]
		[Address(RVA = "0x10169C664", Offset = "0x169C664", VA = "0x10169C664")]
		public ActXlsKeyBindRot()
		{
		}

		// Token: 0x040038EE RID: 14574
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400293A")]
		public string m_TargetName;

		// Token: 0x040038EF RID: 14575
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400293B")]
		public bool m_OnOff;
	}
}
