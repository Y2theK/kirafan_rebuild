﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098D RID: 2445
	[Token(Token = "0x20006EF")]
	[StructLayout(3)]
	public class ActXlsKeyCharaCfg : ActXlsKeyBase
	{
		// Token: 0x06002884 RID: 10372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002546")]
		[Address(RVA = "0x10169C66C", Offset = "0x169C66C", VA = "0x10169C66C", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002885 RID: 10373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002547")]
		[Address(RVA = "0x10169C720", Offset = "0x169C720", VA = "0x10169C720")]
		public ActXlsKeyCharaCfg()
		{
		}

		// Token: 0x040038F8 RID: 14584
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002944")]
		public int m_OffsetLayer;

		// Token: 0x040038F9 RID: 14585
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002945")]
		public float m_OffsetZ;
	}
}
