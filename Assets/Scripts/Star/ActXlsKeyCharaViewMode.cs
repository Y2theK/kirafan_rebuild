﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098B RID: 2443
	[Token(Token = "0x20006ED")]
	[StructLayout(3)]
	public class ActXlsKeyCharaViewMode : ActXlsKeyBase
	{
		// Token: 0x06002880 RID: 10368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002542")]
		[Address(RVA = "0x10169C728", Offset = "0x169C728", VA = "0x10169C728", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002881 RID: 10369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002543")]
		[Address(RVA = "0x10169C810", Offset = "0x169C810", VA = "0x10169C810")]
		public ActXlsKeyCharaViewMode()
		{
		}

		// Token: 0x040038F1 RID: 14577
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400293D")]
		public string m_TargetName;

		// Token: 0x040038F2 RID: 14578
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400293E")]
		public bool m_View;

		// Token: 0x040038F3 RID: 14579
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400293F")]
		public int m_EntryTagID;
	}
}
