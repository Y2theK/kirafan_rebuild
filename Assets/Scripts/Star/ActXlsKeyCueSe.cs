﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098E RID: 2446
	[Token(Token = "0x20006F0")]
	[StructLayout(3)]
	public class ActXlsKeyCueSe : ActXlsKeyBase
	{
		// Token: 0x06002886 RID: 10374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002548")]
		[Address(RVA = "0x10169C818", Offset = "0x169C818", VA = "0x10169C818", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002887 RID: 10375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002549")]
		[Address(RVA = "0x10169C8CC", Offset = "0x169C8CC", VA = "0x10169C8CC")]
		public ActXlsKeyCueSe()
		{
		}

		// Token: 0x040038FA RID: 14586
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002946")]
		public string m_CueName;

		// Token: 0x040038FB RID: 14587
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002947")]
		public float m_Volume;
	}
}
