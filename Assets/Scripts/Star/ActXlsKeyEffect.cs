﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000985 RID: 2437
	[Token(Token = "0x20006E7")]
	[StructLayout(3)]
	public class ActXlsKeyEffect : ActXlsKeyBase
	{
		// Token: 0x06002874 RID: 10356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002536")]
		[Address(RVA = "0x10169C8D4", Offset = "0x169C8D4", VA = "0x10169C8D4", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002875 RID: 10357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002537")]
		[Address(RVA = "0x10169CA24", Offset = "0x169CA24", VA = "0x10169CA24")]
		public ActXlsKeyEffect()
		{
		}

		// Token: 0x040038E2 RID: 14562
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400292E")]
		public int m_EffectNo;

		// Token: 0x040038E3 RID: 14563
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400292F")]
		public Vector3 m_OffsetPos;

		// Token: 0x040038E4 RID: 14564
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002930")]
		public float m_Size;
	}
}
