﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000992 RID: 2450
	[Token(Token = "0x20006F4")]
	[StructLayout(3)]
	public class ActXlsKeyForceViewMode : ActXlsKeyBase
	{
		// Token: 0x0600288E RID: 10382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002550")]
		[Address(RVA = "0x10169CA2C", Offset = "0x169CA2C", VA = "0x10169CA2C", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600288F RID: 10383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002551")]
		[Address(RVA = "0x10169CB14", Offset = "0x169CB14", VA = "0x10169CB14")]
		public ActXlsKeyForceViewMode()
		{
		}

		// Token: 0x04003901 RID: 14593
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400294D")]
		public string m_TargetName;

		// Token: 0x04003902 RID: 14594
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400294E")]
		public bool m_View;

		// Token: 0x04003903 RID: 14595
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400294F")]
		public int m_EntryTagID;
	}
}
