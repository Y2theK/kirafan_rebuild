﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098A RID: 2442
	[Token(Token = "0x20006EC")]
	[StructLayout(3)]
	public class ActXlsKeyFrameChg : ActXlsKeyBase
	{
		// Token: 0x0600287E RID: 10366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002540")]
		[Address(RVA = "0x10169CB1C", Offset = "0x169CB1C", VA = "0x10169CB1C", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600287F RID: 10367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002541")]
		[Address(RVA = "0x10169CB98", Offset = "0x169CB98", VA = "0x10169CB98")]
		public ActXlsKeyFrameChg()
		{
		}

		// Token: 0x040038F0 RID: 14576
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400293C")]
		public int m_Frame;
	}
}
