﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000982 RID: 2434
	[Token(Token = "0x20006E4")]
	[StructLayout(3)]
	public class ActXlsKeyJump : ActXlsKeyBase
	{
		// Token: 0x0600286E RID: 10350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002530")]
		[Address(RVA = "0x10169CBA0", Offset = "0x169CBA0", VA = "0x10169CBA0", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600286F RID: 10351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002531")]
		[Address(RVA = "0x10169CC88", Offset = "0x169CC88", VA = "0x10169CC88")]
		public ActXlsKeyJump()
		{
		}

		// Token: 0x040038DB RID: 14555
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002927")]
		public int m_Randam;

		// Token: 0x040038DC RID: 14556
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002928")]
		public int m_Offset;

		// Token: 0x040038DD RID: 14557
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002929")]
		public bool m_FrameReset;
	}
}
