﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200097D RID: 2429
	[Token(Token = "0x20006DF")]
	[StructLayout(3)]
	public class ActXlsKeyLinkCut : ActXlsKeyBase
	{
		// Token: 0x06002864 RID: 10340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002526")]
		[Address(RVA = "0x10169CC90", Offset = "0x169CC90", VA = "0x10169CC90", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002865 RID: 10341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002527")]
		[Address(RVA = "0x10169CD44", Offset = "0x169CD44", VA = "0x10169CD44")]
		public ActXlsKeyLinkCut()
		{
		}

		// Token: 0x040038CF RID: 14543
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400291B")]
		public string m_BindHrcName;

		// Token: 0x040038D0 RID: 14544
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400291C")]
		public int m_LinkTime;
	}
}
