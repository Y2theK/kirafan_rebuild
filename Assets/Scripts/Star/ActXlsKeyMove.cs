﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200097F RID: 2431
	[Token(Token = "0x20006E1")]
	[StructLayout(3)]
	public class ActXlsKeyMove : ActXlsKeyBase
	{
		// Token: 0x06002868 RID: 10344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252A")]
		[Address(RVA = "0x10169CD4C", Offset = "0x169CD4C", VA = "0x10169CD4C", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002869 RID: 10345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252B")]
		[Address(RVA = "0x10169CECC", Offset = "0x169CECC", VA = "0x10169CECC")]
		public ActXlsKeyMove()
		{
		}

		// Token: 0x040038D4 RID: 14548
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002920")]
		public CActScriptKeyMove.eMoveType m_Func;

		// Token: 0x040038D5 RID: 14549
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002921")]
		public float m_Speed;

		// Token: 0x040038D6 RID: 14550
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002922")]
		public int m_MoveTime;

		// Token: 0x040038D7 RID: 14551
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002923")]
		public Vector3 m_Offset;
	}
}
