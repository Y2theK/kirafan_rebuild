﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098C RID: 2444
	[Token(Token = "0x20006EE")]
	[StructLayout(3)]
	public class ActXlsKeyObjPlayAnim : ActXlsKeyBase
	{
		// Token: 0x06002882 RID: 10370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002544")]
		[Address(RVA = "0x10169CED4", Offset = "0x169CED4", VA = "0x10169CED4", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002883 RID: 10371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002545")]
		[Address(RVA = "0x10169CFF0", Offset = "0x169CFF0", VA = "0x10169CFF0")]
		public ActXlsKeyObjPlayAnim()
		{
		}

		// Token: 0x040038F4 RID: 14580
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002940")]
		public int m_PlayAnimNo;

		// Token: 0x040038F5 RID: 14581
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002941")]
		public int m_AnimeKey;

		// Token: 0x040038F6 RID: 14582
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002942")]
		public bool m_Loop;

		// Token: 0x040038F7 RID: 14583
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002943")]
		public string m_AnmName;
	}
}
