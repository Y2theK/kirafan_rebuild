﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200098F RID: 2447
	[Token(Token = "0x20006F1")]
	[StructLayout(3)]
	public class ActXlsKeyObjectCfg : ActXlsKeyBase
	{
		// Token: 0x06002888 RID: 10376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254A")]
		[Address(RVA = "0x10169CFF8", Offset = "0x169CFF8", VA = "0x10169CFF8", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002889 RID: 10377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254B")]
		[Address(RVA = "0x10169D0E0", Offset = "0x169D0E0", VA = "0x10169D0E0")]
		public ActXlsKeyObjectCfg()
		{
		}

		// Token: 0x040038FC RID: 14588
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002948")]
		public int m_OffsetLayer;

		// Token: 0x040038FD RID: 14589
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002949")]
		public float m_OffsetZ;

		// Token: 0x040038FE RID: 14590
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400294A")]
		public string m_TargetName;
	}
}
