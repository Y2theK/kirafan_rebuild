﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000984 RID: 2436
	[Token(Token = "0x20006E6")]
	[StructLayout(3)]
	public class ActXlsKeyObjectDir : ActXlsKeyBase
	{
		// Token: 0x06002872 RID: 10354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002534")]
		[Address(RVA = "0x10169D0E8", Offset = "0x169D0E8", VA = "0x10169D0E8", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002873 RID: 10355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002535")]
		[Address(RVA = "0x10169D180", Offset = "0x169D180", VA = "0x10169D180")]
		public ActXlsKeyObjectDir()
		{
		}

		// Token: 0x040038E1 RID: 14561
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400292D")]
		public CActScriptKeyObjectDir.eDirFunc m_Func;
	}
}
