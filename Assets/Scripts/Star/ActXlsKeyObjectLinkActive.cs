﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000990 RID: 2448
	[Token(Token = "0x20006F2")]
	[StructLayout(3)]
	public class ActXlsKeyObjectLinkActive : ActXlsKeyBase
	{
		// Token: 0x0600288A RID: 10378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254C")]
		[Address(RVA = "0x10169D188", Offset = "0x169D188", VA = "0x10169D188", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600288B RID: 10379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254D")]
		[Address(RVA = "0x10169D204", Offset = "0x169D204", VA = "0x10169D204")]
		public ActXlsKeyObjectLinkActive()
		{
		}

		// Token: 0x040038FF RID: 14591
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400294B")]
		public bool m_flg;
	}
}
