﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000979 RID: 2425
	[Token(Token = "0x20006DB")]
	[StructLayout(3)]
	public class ActXlsKeyPlayAnim : ActXlsKeyBase
	{
		// Token: 0x0600285C RID: 10332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600251E")]
		[Address(RVA = "0x10169D20C", Offset = "0x169D20C", VA = "0x10169D20C", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600285D RID: 10333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600251F")]
		[Address(RVA = "0x10169D328", Offset = "0x169D328", VA = "0x10169D328")]
		public ActXlsKeyPlayAnim()
		{
		}

		// Token: 0x040038C3 RID: 14531
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400290F")]
		public int m_PlayAnimNo;

		// Token: 0x040038C4 RID: 14532
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002910")]
		public int m_SequenceNo;

		// Token: 0x040038C5 RID: 14533
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002911")]
		public bool m_Loop;

		// Token: 0x040038C6 RID: 14534
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002912")]
		public int m_AnmType;
	}
}
