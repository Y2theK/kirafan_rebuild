﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000980 RID: 2432
	[Token(Token = "0x20006E2")]
	[StructLayout(3)]
	public class ActXlsKeyPopUp : ActXlsKeyBase
	{
		// Token: 0x0600286A RID: 10346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252C")]
		[Address(RVA = "0x10169D330", Offset = "0x169D330", VA = "0x10169D330", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600286B RID: 10347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252D")]
		[Address(RVA = "0x10169D3E4", Offset = "0x169D3E4", VA = "0x10169D3E4")]
		public ActXlsKeyPopUp()
		{
		}

		// Token: 0x040038D8 RID: 14552
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002924")]
		public int m_PopUpID;

		// Token: 0x040038D9 RID: 14553
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002925")]
		public int m_PopUpTime;
	}
}
