﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000987 RID: 2439
	[Token(Token = "0x20006E9")]
	[StructLayout(3)]
	public class ActXlsKeyPosAnime : ActXlsKeyBase
	{
		// Token: 0x06002878 RID: 10360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253A")]
		[Address(RVA = "0x10169D3EC", Offset = "0x169D3EC", VA = "0x10169D3EC", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002879 RID: 10361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253B")]
		[Address(RVA = "0x10169D52C", Offset = "0x169D52C", VA = "0x10169D52C")]
		public ActXlsKeyPosAnime()
		{
		}

		// Token: 0x040038E8 RID: 14568
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002934")]
		public string m_TargetName;

		// Token: 0x040038E9 RID: 14569
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002935")]
		public int m_Times;

		// Token: 0x040038EA RID: 14570
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002936")]
		public Vector3 m_Pos;
	}
}
