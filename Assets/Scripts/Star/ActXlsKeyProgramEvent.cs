﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000983 RID: 2435
	[Token(Token = "0x20006E5")]
	[StructLayout(3)]
	public class ActXlsKeyProgramEvent : ActXlsKeyBase
	{
		// Token: 0x06002870 RID: 10352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002532")]
		[Address(RVA = "0x10169D534", Offset = "0x169D534", VA = "0x10169D534", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002871 RID: 10353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002533")]
		[Address(RVA = "0x10169D61C", Offset = "0x169D61C", VA = "0x10169D61C")]
		public ActXlsKeyProgramEvent()
		{
		}

		// Token: 0x040038DE RID: 14558
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400292A")]
		public int m_EventID;

		// Token: 0x040038DF RID: 14559
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400292B")]
		public int m_OptionKey;

		// Token: 0x040038E0 RID: 14560
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400292C")]
		public int m_EvtTime;
	}
}
