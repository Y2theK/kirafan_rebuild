﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000991 RID: 2449
	[Token(Token = "0x20006F3")]
	[StructLayout(3)]
	public class ActXlsKeyReflectMoveFlg : ActXlsKeyBase
	{
		// Token: 0x0600288C RID: 10380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254E")]
		[Address(RVA = "0x10169D624", Offset = "0x169D624", VA = "0x10169D624", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600288D RID: 10381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600254F")]
		[Address(RVA = "0x10169D6A0", Offset = "0x169D6A0", VA = "0x10169D6A0")]
		public ActXlsKeyReflectMoveFlg()
		{
		}

		// Token: 0x04003900 RID: 14592
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400294C")]
		public bool m_flg;
	}
}
