﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000995 RID: 2453
	[Token(Token = "0x20006F7")]
	[StructLayout(3)]
	public class ActXlsKeyResetAnime : ActXlsKeyBase
	{
		// Token: 0x06002894 RID: 10388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002556")]
		[Address(RVA = "0x10169D6A8", Offset = "0x169D6A8", VA = "0x10169D6A8", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002895 RID: 10389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002557")]
		[Address(RVA = "0x10169D75C", Offset = "0x169D75C", VA = "0x10169D75C")]
		public ActXlsKeyResetAnime()
		{
		}

		// Token: 0x04003908 RID: 14600
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002954")]
		public int m_PlayAnimNo;

		// Token: 0x04003909 RID: 14601
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002955")]
		public int m_AnmType;
	}
}
