﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000993 RID: 2451
	[Token(Token = "0x20006F5")]
	[StructLayout(3)]
	public class ActXlsKeyResetBaseRot : ActXlsKeyBase
	{
		// Token: 0x06002890 RID: 10384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002552")]
		[Address(RVA = "0x10169D764", Offset = "0x169D764", VA = "0x10169D764", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002891 RID: 10385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002553")]
		[Address(RVA = "0x10169D7E0", Offset = "0x169D7E0", VA = "0x10169D7E0")]
		public ActXlsKeyResetBaseRot()
		{
		}

		// Token: 0x04003904 RID: 14596
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002950")]
		public string m_TargetName;
	}
}
