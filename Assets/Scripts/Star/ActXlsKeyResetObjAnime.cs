﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000996 RID: 2454
	[Token(Token = "0x20006F8")]
	[StructLayout(3)]
	public class ActXlsKeyResetObjAnime : ActXlsKeyBase
	{
		// Token: 0x06002896 RID: 10390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002558")]
		[Address(RVA = "0x10169D7E8", Offset = "0x169D7E8", VA = "0x10169D7E8", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002897 RID: 10391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002559")]
		[Address(RVA = "0x10169D89C", Offset = "0x169D89C", VA = "0x10169D89C")]
		public ActXlsKeyResetObjAnime()
		{
		}

		// Token: 0x0400390A RID: 14602
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002956")]
		public int m_PlayAnimNo;

		// Token: 0x0400390B RID: 14603
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002957")]
		public string m_AnmName;
	}
}
