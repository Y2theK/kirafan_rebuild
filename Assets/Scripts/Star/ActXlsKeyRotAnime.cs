﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000988 RID: 2440
	[Token(Token = "0x20006EA")]
	[StructLayout(3)]
	public class ActXlsKeyRotAnime : ActXlsKeyBase
	{
		// Token: 0x0600287A RID: 10362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253C")]
		[Address(RVA = "0x10169D8A4", Offset = "0x169D8A4", VA = "0x10169D8A4", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600287B RID: 10363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600253D")]
		[Address(RVA = "0x10169D98C", Offset = "0x169D98C", VA = "0x10169D98C")]
		public ActXlsKeyRotAnime()
		{
		}

		// Token: 0x040038EB RID: 14571
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002937")]
		public string m_TargetName;

		// Token: 0x040038EC RID: 14572
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002938")]
		public int m_Times;

		// Token: 0x040038ED RID: 14573
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002939")]
		public float m_Rot;
	}
}
