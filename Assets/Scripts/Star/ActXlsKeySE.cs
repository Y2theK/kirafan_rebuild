﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000981 RID: 2433
	[Token(Token = "0x20006E3")]
	[StructLayout(3)]
	public class ActXlsKeySE : ActXlsKeyBase
	{
		// Token: 0x0600286C RID: 10348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252E")]
		[Address(RVA = "0x10169D994", Offset = "0x169D994", VA = "0x10169D994", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600286D RID: 10349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600252F")]
		[Address(RVA = "0x10169DA10", Offset = "0x169DA10", VA = "0x10169DA10")]
		public ActXlsKeySE()
		{
		}

		// Token: 0x040038DA RID: 14554
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002926")]
		public int m_SE;
	}
}
