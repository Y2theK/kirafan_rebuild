﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000997 RID: 2455
	[Token(Token = "0x20006F9")]
	[StructLayout(3)]
	public class ActXlsKeyTag
	{
		// Token: 0x06002898 RID: 10392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600255A")]
		[Address(RVA = "0x10169DA18", Offset = "0x169DA18", VA = "0x10169DA18")]
		public void WriteIO(BinaryIO pio)
		{
		}

		// Token: 0x06002899 RID: 10393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600255B")]
		[Address(RVA = "0x10169DC20", Offset = "0x169DC20", VA = "0x10169DC20")]
		public void ReadIO(BinaryIO pio)
		{
		}

		// Token: 0x0600289A RID: 10394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600255C")]
		[Address(RVA = "0x10169DE80", Offset = "0x169DE80", VA = "0x10169DE80")]
		public ActXlsKeyTag()
		{
		}

		// Token: 0x0400390C RID: 14604
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002958")]
		public string m_KeyName;

		// Token: 0x0400390D RID: 14605
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002959")]
		public int m_AccessKey;

		// Token: 0x0400390E RID: 14606
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400295A")]
		public int m_LinkKey;

		// Token: 0x0400390F RID: 14607
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400295B")]
		public ActXlsKeyBase[] m_List;
	}
}
