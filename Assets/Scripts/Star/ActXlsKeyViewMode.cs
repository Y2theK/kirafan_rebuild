﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200097E RID: 2430
	[Token(Token = "0x20006E0")]
	[StructLayout(3)]
	public class ActXlsKeyViewMode : ActXlsKeyBase
	{
		// Token: 0x06002866 RID: 10342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002528")]
		[Address(RVA = "0x10169DE88", Offset = "0x169DE88", VA = "0x10169DE88", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x06002867 RID: 10343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002529")]
		[Address(RVA = "0x10169DF70", Offset = "0x169DF70", VA = "0x10169DF70")]
		public ActXlsKeyViewMode()
		{
		}

		// Token: 0x040038D1 RID: 14545
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400291D")]
		public string m_TargetName;

		// Token: 0x040038D2 RID: 14546
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400291E")]
		public bool m_View;

		// Token: 0x040038D3 RID: 14547
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400291F")]
		public int m_EntryTagID;
	}
}
