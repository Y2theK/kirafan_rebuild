﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200097A RID: 2426
	[Token(Token = "0x20006DC")]
	[StructLayout(3)]
	public class ActXlsKeyWait : ActXlsKeyBase
	{
		// Token: 0x0600285E RID: 10334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002520")]
		[Address(RVA = "0x10169DF78", Offset = "0x169DF78", VA = "0x10169DF78", Slot = "4")]
		public override void SerializeCode(BinaryIO pio)
		{
		}

		// Token: 0x0600285F RID: 10335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002521")]
		[Address(RVA = "0x10169E090", Offset = "0x169E090", VA = "0x10169E090")]
		public ActXlsKeyWait()
		{
		}

		// Token: 0x040038C7 RID: 14535
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002913")]
		public int m_DummyNo;

		// Token: 0x040038C8 RID: 14536
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002914")]
		public CActScriptKeyWait.eWait m_WaitType;
	}
}
