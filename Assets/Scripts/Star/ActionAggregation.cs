﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200034A RID: 842
	[Token(Token = "0x20002C8")]
	[StructLayout(3)]
	public class ActionAggregation : MonoBehaviour
	{
		// Token: 0x06000B5F RID: 2911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A8E")]
		[Address(RVA = "0x10169E098", Offset = "0x169E098", VA = "0x10169E098", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06000B60 RID: 2912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A8F")]
		[Address(RVA = "0x10169E12C", Offset = "0x169E12C", VA = "0x10169E12C")]
		private void Update()
		{
		}

		// Token: 0x06000B61 RID: 2913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A90")]
		[Address(RVA = "0x10169E130", Offset = "0x169E130", VA = "0x10169E130")]
		protected void UpdateChildren()
		{
		}

		// Token: 0x06000B62 RID: 2914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A91")]
		[Address(RVA = "0x10169E304", Offset = "0x169E304", VA = "0x10169E304")]
		public void PlayStart()
		{
		}

		// Token: 0x06000B63 RID: 2915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A92")]
		[Address(RVA = "0x10169E368", Offset = "0x169E368", VA = "0x10169E368")]
		public void Skip()
		{
		}

		// Token: 0x06000B64 RID: 2916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A93")]
		[Address(RVA = "0x10169E5E0", Offset = "0x169E5E0", VA = "0x10169E5E0", Slot = "5")]
		public virtual void RecoveryChildFinishToWait()
		{
		}

		// Token: 0x06000B65 RID: 2917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A94")]
		[Address(RVA = "0x10169E6B8", Offset = "0x169E6B8", VA = "0x10169E6B8", Slot = "6")]
		public virtual void ForceRecoveryChildFinishToWait()
		{
		}

		// Token: 0x06000B66 RID: 2918 RVA: 0x000047A0 File Offset: 0x000029A0
		[Token(Token = "0x6000A95")]
		[Address(RVA = "0x10169E790", Offset = "0x169E790", VA = "0x10169E790")]
		protected eActionAggregationState GetState()
		{
			return eActionAggregationState.Wait;
		}

		// Token: 0x06000B67 RID: 2919 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000A96")]
		[Address(RVA = "0x10169E798", Offset = "0x169E798", VA = "0x10169E798")]
		protected ActionAggregationChildAction GetChildAction(int index)
		{
			return null;
		}

		// Token: 0x06000B68 RID: 2920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A97")]
		[Address(RVA = "0x10169E7F8", Offset = "0x169E7F8", VA = "0x10169E7F8")]
		public void SetFinishFrame(int finishFrame)
		{
		}

		// Token: 0x06000B69 RID: 2921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A98")]
		[Address(RVA = "0x10169E800", Offset = "0x169E800", VA = "0x10169E800")]
		public void SetPlayStartFrame(int index, int playStartFrame)
		{
		}

		// Token: 0x06000B6A RID: 2922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A99")]
		[Address(RVA = "0x10169E830", Offset = "0x169E830", VA = "0x10169E830")]
		public void SetIsExecuteEvenIfSkip(int index, bool isExecuteEvenIfSkip)
		{
		}

		// Token: 0x06000B6B RID: 2923 RVA: 0x000047B8 File Offset: 0x000029B8
		[Token(Token = "0x6000A9A")]
		[Address(RVA = "0x10169E860", Offset = "0x169E860", VA = "0x10169E860")]
		public bool IsFinished()
		{
			return default(bool);
		}

		// Token: 0x06000B6C RID: 2924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A9B")]
		[Address(RVA = "0x10169E870", Offset = "0x169E870", VA = "0x10169E870")]
		public ActionAggregation()
		{
		}

		// Token: 0x04000C83 RID: 3203
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009ED")]
		[SerializeField]
		private int m_FrameParSecond;

		// Token: 0x04000C84 RID: 3204
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40009EE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EA04", Offset = "0x11EA04")]
		private int m_FinishFrame;

		// Token: 0x04000C85 RID: 3205
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009EF")]
		[SerializeField]
		private ActionAggregationChildAction[] m_ChildActions;

		// Token: 0x04000C86 RID: 3206
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009F0")]
		private eActionAggregationState m_State;

		// Token: 0x04000C87 RID: 3207
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009F1")]
		private double m_PlayStartTime;

		// Token: 0x04000C88 RID: 3208
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40009F2")]
		private double m_RealtimeSinceStartup;
	}
}
