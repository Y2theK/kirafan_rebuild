﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000349 RID: 841
	[Token(Token = "0x20002C7")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildAction
	{
		// Token: 0x06000B50 RID: 2896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A7F")]
		[Address(RVA = "0x10169E880", Offset = "0x169E880", VA = "0x10169E880", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06000B51 RID: 2897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A80")]
		[Address(RVA = "0x10169E8B4", Offset = "0x169E8B4", VA = "0x10169E8B4", Slot = "5")]
		public virtual void Update()
		{
		}

		// Token: 0x06000B52 RID: 2898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A81")]
		[Address(RVA = "0x10169E8E8", Offset = "0x169E8E8", VA = "0x10169E8E8", Slot = "6")]
		public virtual void PlayStart()
		{
		}

		// Token: 0x06000B53 RID: 2899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A82")]
		[Address(RVA = "0x10169E91C", Offset = "0x169E91C", VA = "0x10169E91C", Slot = "7")]
		public virtual void Skip()
		{
		}

		// Token: 0x06000B54 RID: 2900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A83")]
		[Address(RVA = "0x10169E950", Offset = "0x169E950", VA = "0x10169E950", Slot = "8")]
		public virtual void RecoveryFinishToWait()
		{
		}

		// Token: 0x06000B55 RID: 2901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A84")]
		[Address(RVA = "0x10169E9F0", Offset = "0x169E9F0", VA = "0x10169E9F0", Slot = "9")]
		public virtual void ForceRecoveryFinishToWait()
		{
		}

		// Token: 0x06000B56 RID: 2902 RVA: 0x00004728 File Offset: 0x00002928
		[Token(Token = "0x6000A85")]
		[Address(RVA = "0x10169E5B4", Offset = "0x169E5B4", VA = "0x10169E5B4")]
		public eActionAggregationState GetState()
		{
			return eActionAggregationState.Wait;
		}

		// Token: 0x06000B57 RID: 2903 RVA: 0x00004740 File Offset: 0x00002940
		[Token(Token = "0x6000A86")]
		[Address(RVA = "0x10169E2F4", Offset = "0x169E2F4", VA = "0x10169E2F4")]
		public bool IsExecute()
		{
			return default(bool);
		}

		// Token: 0x06000B58 RID: 2904 RVA: 0x00004758 File Offset: 0x00002958
		[Token(Token = "0x6000A87")]
		[Address(RVA = "0x10169E2FC", Offset = "0x169E2FC", VA = "0x10169E2FC")]
		public int GetPlayStartFrame()
		{
			return 0;
		}

		// Token: 0x06000B59 RID: 2905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A88")]
		[Address(RVA = "0x10169EA2C", Offset = "0x169EA2C", VA = "0x10169EA2C")]
		public void SetIsExecute(bool isExecute)
		{
		}

		// Token: 0x06000B5A RID: 2906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A89")]
		[Address(RVA = "0x10169E828", Offset = "0x169E828", VA = "0x10169E828")]
		public void SetPlayStartFrame(int playStartFrame)
		{
		}

		// Token: 0x06000B5B RID: 2907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A8A")]
		[Address(RVA = "0x10169E858", Offset = "0x169E858", VA = "0x10169E858")]
		public void SetIsExecuteEvenIfSkip(bool isExecuteEvenIfSkip)
		{
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x00004770 File Offset: 0x00002970
		[Token(Token = "0x6000A8B")]
		[Address(RVA = "0x10169E5AC", Offset = "0x169E5AC", VA = "0x10169E5AC")]
		public bool IsExecuteEvenIfSkip()
		{
			return default(bool);
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x00004788 File Offset: 0x00002988
		[Token(Token = "0x6000A8C")]
		[Address(RVA = "0x10169EA34", Offset = "0x169EA34", VA = "0x10169EA34")]
		public bool IsFinished()
		{
			return default(bool);
		}

		// Token: 0x06000B5E RID: 2910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A8D")]
		[Address(RVA = "0x10169EA3C", Offset = "0x169EA3C", VA = "0x10169EA3C")]
		public ActionAggregationChildAction()
		{
		}

		// Token: 0x04000C7F RID: 3199
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40009E9")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011E900", Offset = "0x11E900")]
		private ActionAggregationChildActionAdapter m_ChildActionAdapter;

		// Token: 0x04000C80 RID: 3200
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009EA")]
		[SerializeField]
		private bool m_IsExecute;

		// Token: 0x04000C81 RID: 3201
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40009EB")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011E95C", Offset = "0x11E95C")]
		private int m_PlayStartFrame;

		// Token: 0x04000C82 RID: 3202
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009EC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011E9A8", Offset = "0x11E9A8")]
		private bool m_IsExecuteEvenIfSkip;
	}
}
