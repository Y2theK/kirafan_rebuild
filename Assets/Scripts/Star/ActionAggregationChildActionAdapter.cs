﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200034B RID: 843
	[Token(Token = "0x20002C9")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildActionAdapter : MonoBehaviour
	{
		// Token: 0x06000B6D RID: 2925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A9C")]
		[Address(RVA = "0x10169EA4C", Offset = "0x169EA4C", VA = "0x10169EA4C", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06000B6E RID: 2926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A9D")]
		[Address(RVA = "0x10169EA5C", Offset = "0x169EA5C", VA = "0x10169EA5C", Slot = "5")]
		public virtual void Update()
		{
		}

		// Token: 0x06000B6F RID: 2927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A9E")]
		[Address(RVA = "0x10169EA74", Offset = "0x169EA74", VA = "0x10169EA74", Slot = "6")]
		public virtual void PlayStart()
		{
		}

		// Token: 0x06000B70 RID: 2928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000A9F")]
		[Address(RVA = "0x10169EAAC", Offset = "0x169EAAC", VA = "0x10169EAAC", Slot = "7")]
		public virtual void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B71 RID: 2929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA0")]
		[Address(RVA = "0x10169EAB0", Offset = "0x169EAB0", VA = "0x10169EAB0", Slot = "8")]
		public virtual void Skip()
		{
		}

		// Token: 0x06000B72 RID: 2930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA1")]
		[Address(RVA = "0x10169EABC", Offset = "0x169EABC", VA = "0x10169EABC", Slot = "9")]
		public virtual void RecoveryFinishToWait()
		{
		}

		// Token: 0x06000B73 RID: 2931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA2")]
		[Address(RVA = "0x10169EAF4", Offset = "0x169EAF4", VA = "0x10169EAF4", Slot = "10")]
		public virtual void ForceRecoveryFinishToWait()
		{
		}

		// Token: 0x06000B74 RID: 2932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA3")]
		[Address(RVA = "0x10169EB20", Offset = "0x169EB20", VA = "0x10169EB20", Slot = "11")]
		public virtual void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B75 RID: 2933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA4")]
		[Address(RVA = "0x10169EB24", Offset = "0x169EB24", VA = "0x10169EB24", Slot = "12")]
		public virtual void ForceRecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B76 RID: 2934 RVA: 0x000047D0 File Offset: 0x000029D0
		[Token(Token = "0x6000AA5")]
		[Address(RVA = "0x10169EA24", Offset = "0x169EA24", VA = "0x10169EA24")]
		public eActionAggregationState GetState()
		{
			return eActionAggregationState.Wait;
		}

		// Token: 0x06000B77 RID: 2935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA6")]
		[Address(RVA = "0x10169EA54", Offset = "0x169EA54", VA = "0x10169EA54")]
		protected void SetState(eActionAggregationState state)
		{
		}

		// Token: 0x06000B78 RID: 2936 RVA: 0x000047E8 File Offset: 0x000029E8
		[Token(Token = "0x6000AA7")]
		[Address(RVA = "0x10169EB28", Offset = "0x169EB28", VA = "0x10169EB28", Slot = "13")]
		public virtual bool IsFinished()
		{
			return default(bool);
		}

		// Token: 0x06000B79 RID: 2937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA8")]
		[Address(RVA = "0x10169EB30", Offset = "0x169EB30", VA = "0x10169EB30")]
		public ActionAggregationChildActionAdapter()
		{
		}

		// Token: 0x04000C89 RID: 3209
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40009F3")]
		private eActionAggregationState m_State;
	}
}
