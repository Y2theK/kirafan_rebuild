﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200034C RID: 844
	[Token(Token = "0x20002CA")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildActionAdapterExGen<GenType> : ActionAggregationChildActionAdapter where GenType : UnityEngine.Object
	{
		// Token: 0x06000B7A RID: 2938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AA9")]
		[Address(RVA = "0x1016CBE84", Offset = "0x16CBE84", VA = "0x1016CBE84")]
		public ActionAggregationChildActionAdapterExGen()
		{
		}

		// Token: 0x04000C8A RID: 3210
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40009F4")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EA60", Offset = "0x11EA60")]
		protected GenType m_Action;
	}
}
