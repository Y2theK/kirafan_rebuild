﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200034D RID: 845
	[Token(Token = "0x20002CB")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011C770", Offset = "0x11C770")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C770", Offset = "0x11C770")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildActionAggregationActionAdapter : ActionAggregationChildActionAdapterExGen<ActionAggregation>
	{
		// Token: 0x06000B7B RID: 2939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAA")]
		[Address(RVA = "0x10169EB38", Offset = "0x169EB38", VA = "0x10169EB38", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06000B7C RID: 2940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAB")]
		[Address(RVA = "0x10169EB78", Offset = "0x169EB78", VA = "0x10169EB78", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B7D RID: 2941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAC")]
		[Address(RVA = "0x10169EBC0", Offset = "0x169EBC0", VA = "0x10169EBC0", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B7E RID: 2942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAD")]
		[Address(RVA = "0x10169EBEC", Offset = "0x169EBEC", VA = "0x10169EBEC", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B7F RID: 2943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAE")]
		[Address(RVA = "0x10169EC28", Offset = "0x169EC28", VA = "0x10169EC28", Slot = "11")]
		public override void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B80 RID: 2944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AAF")]
		[Address(RVA = "0x10169EC2C", Offset = "0x169EC2C", VA = "0x10169EC2C", Slot = "12")]
		public override void ForceRecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B81 RID: 2945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB0")]
		[Address(RVA = "0x10169EC60", Offset = "0x169EC60", VA = "0x10169EC60")]
		public ActionAggregationChildActionAggregationActionAdapter()
		{
		}
	}
}
