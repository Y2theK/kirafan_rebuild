﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200034E RID: 846
	[Token(Token = "0x20002CC")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011C7F8", Offset = "0x11C7F8")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C7F8", Offset = "0x11C7F8")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildAnimUIPlayerActionAdapter : ActionAggregationChildActionAdapterExGen<AnimUIPlayer>
	{
		// Token: 0x06000B82 RID: 2946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB1")]
		[Address(RVA = "0x10169ECB0", Offset = "0x169ECB0", VA = "0x10169ECB0", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B83 RID: 2947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB2")]
		[Address(RVA = "0x10169ED04", Offset = "0x169ED04", VA = "0x10169ED04", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B84 RID: 2948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB3")]
		[Address(RVA = "0x10169ED34", Offset = "0x169ED34", VA = "0x10169ED34", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B85 RID: 2949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB4")]
		[Address(RVA = "0x10169ED74", Offset = "0x169ED74", VA = "0x10169ED74", Slot = "11")]
		public override void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B86 RID: 2950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB5")]
		[Address(RVA = "0x10169EDA4", Offset = "0x169EDA4", VA = "0x10169EDA4")]
		public ActionAggregationChildAnimUIPlayerActionAdapter()
		{
		}
	}
}
