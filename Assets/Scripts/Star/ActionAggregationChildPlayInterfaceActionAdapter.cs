﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000350 RID: 848
	[Token(Token = "0x20002CE")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C880", Offset = "0x11C880")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildPlayInterfaceActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000B8A RID: 2954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB9")]
		[Address(RVA = "0x10169EDF4", Offset = "0x169EDF4", VA = "0x10169EDF4", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B8B RID: 2955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABA")]
		[Address(RVA = "0x10169EDF8", Offset = "0x169EDF8", VA = "0x10169EDF8", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B8C RID: 2956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABB")]
		[Address(RVA = "0x10169EE98", Offset = "0x169EE98", VA = "0x10169EE98", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B8D RID: 2957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABC")]
		[Address(RVA = "0x10169EF38", Offset = "0x169EF38", VA = "0x10169EF38", Slot = "11")]
		public override void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B8E RID: 2958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABD")]
		[Address(RVA = "0x10169EFD8", Offset = "0x169EFD8", VA = "0x10169EFD8")]
		public ActionAggregationChildPlayInterfaceActionAdapter()
		{
		}

		// Token: 0x04000C8B RID: 3211
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009F5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EAAC", Offset = "0x11EAAC")]
		private PlayInterfaceMonoBehaviour m_PlayInterface;
	}
}
