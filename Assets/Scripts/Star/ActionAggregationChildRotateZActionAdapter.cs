﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000351 RID: 849
	[Token(Token = "0x20002CF")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C8B8", Offset = "0x11C8B8")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildRotateZActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000B8F RID: 2959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABE")]
		[Address(RVA = "0x10169EFE0", Offset = "0x169EFE0", VA = "0x10169EFE0", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ABF")]
		[Address(RVA = "0x10169F12C", Offset = "0x169F12C", VA = "0x10169F12C", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B91 RID: 2961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC0")]
		[Address(RVA = "0x10169F1B0", Offset = "0x169F1B0", VA = "0x10169F1B0", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B92 RID: 2962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC1")]
		[Address(RVA = "0x10169F1F8", Offset = "0x169F1F8", VA = "0x10169F1F8")]
		public ActionAggregationChildRotateZActionAdapter()
		{
		}

		// Token: 0x04000C8C RID: 3212
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40009F6")]
		[SerializeField]
		private float m_PlaySecond;

		// Token: 0x04000C8D RID: 3213
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009F7")]
		[SerializeField]
		private float m_RotateZPerSecond;

		// Token: 0x04000C8E RID: 3214
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009F8")]
		private RectTransform m_RectTransform;

		// Token: 0x04000C8F RID: 3215
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009F9")]
		private float m_BaseRotateZ;

		// Token: 0x04000C90 RID: 3216
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40009FA")]
		private float m_ElapsedTime;
	}
}
