﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000352 RID: 850
	[Token(Token = "0x20002D0")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011C8F0", Offset = "0x11C8F0")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C8F0", Offset = "0x11C8F0")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildTranslateActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000B93 RID: 2963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC2")]
		[Address(RVA = "0x10169F200", Offset = "0x169F200", VA = "0x10169F200", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B94 RID: 2964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC3")]
		[Address(RVA = "0x10169F3CC", Offset = "0x169F3CC", VA = "0x10169F3CC")]
		private void ApplyParam(int idx, float t)
		{
		}

		// Token: 0x06000B95 RID: 2965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC4")]
		[Address(RVA = "0x10169F6F8", Offset = "0x169F6F8", VA = "0x10169F6F8")]
		private void CalcRandom()
		{
		}

		// Token: 0x06000B96 RID: 2966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC5")]
		[Address(RVA = "0x10169FB58", Offset = "0x169FB58", VA = "0x10169FB58", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B97 RID: 2967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC6")]
		[Address(RVA = "0x10169FC9C", Offset = "0x169FC9C", VA = "0x10169FC9C", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B98 RID: 2968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC7")]
		[Address(RVA = "0x10169FD18", Offset = "0x169FD18", VA = "0x10169FD18", Slot = "11")]
		public override void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000B99 RID: 2969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC8")]
		[Address(RVA = "0x10169FD2C", Offset = "0x169FD2C", VA = "0x10169FD2C")]
		public ActionAggregationChildTranslateActionAdapter()
		{
		}

		// Token: 0x04000C91 RID: 3217
		[Token(Token = "0x40009FB")]
		private const float FPS = 30f;

		// Token: 0x04000C92 RID: 3218
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40009FC")]
		public List<ActionAggregationChildTranslateActionAdapter.Param> m_ParamList;

		// Token: 0x04000C93 RID: 3219
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40009FD")]
		private List<ActionAggregationChildTranslateActionAdapter.Param> m_FixedParamList;

		// Token: 0x04000C94 RID: 3220
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40009FE")]
		private int m_Current;

		// Token: 0x04000C95 RID: 3221
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40009FF")]
		private float m_Time;

		// Token: 0x04000C96 RID: 3222
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A00")]
		private GameObject m_GameObject;

		// Token: 0x04000C97 RID: 3223
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000A01")]
		private RectTransform m_RectTransform;

		// Token: 0x04000C98 RID: 3224
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000A02")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x02000353 RID: 851
		[Token(Token = "0x2000D57")]
		[Serializable]
		public class Param
		{
			// Token: 0x06000B9A RID: 2970 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D95")]
			[Address(RVA = "0x10169FA80", Offset = "0x169FA80", VA = "0x10169FA80")]
			public Param()
			{
			}

			// Token: 0x04000C99 RID: 3225
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400561B")]
			public float m_Frame;

			// Token: 0x04000C9A RID: 3226
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400561C")]
			public float m_FrameRand;

			// Token: 0x04000C9B RID: 3227
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400561D")]
			public Vector2 m_Position;

			// Token: 0x04000C9C RID: 3228
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400561E")]
			public Vector2 m_PositionRand;

			// Token: 0x04000C9D RID: 3229
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400561F")]
			public float m_Rotate;

			// Token: 0x04000C9E RID: 3230
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4005620")]
			public float m_RotateRand;

			// Token: 0x04000C9F RID: 3231
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005621")]
			public Vector3 m_Scale;

			// Token: 0x04000CA0 RID: 3232
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4005622")]
			public Vector3 m_ScaleRand;

			// Token: 0x04000CA1 RID: 3233
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4005623")]
			public float m_Alpha;

			// Token: 0x04000CA2 RID: 3234
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x4005624")]
			public float m_AlphaRand;
		}
	}
}
