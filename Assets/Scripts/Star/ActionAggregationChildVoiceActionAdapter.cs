﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000354 RID: 852
	[Token(Token = "0x20002D1")]
	[Attribute(Name = "AddComponentMenu", RVA = "0x10011C978", Offset = "0x11C978")]
	[Serializable]
	[StructLayout(3)]
	public class ActionAggregationChildVoiceActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000B9B RID: 2971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AC9")]
		[Address(RVA = "0x10169FD9C", Offset = "0x169FD9C", VA = "0x10169FD9C", Slot = "5")]
		public override void Update()
		{
		}

		// Token: 0x06000B9C RID: 2972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ACA")]
		[Address(RVA = "0x10169FDA0", Offset = "0x169FDA0", VA = "0x10169FDA0", Slot = "7")]
		public override void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000B9D RID: 2973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ACB")]
		[Address(RVA = "0x10169FEF4", Offset = "0x169FEF4", VA = "0x10169FEF4", Slot = "8")]
		public override void Skip()
		{
		}

		// Token: 0x06000B9E RID: 2974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ACC")]
		[Address(RVA = "0x10169FF30", Offset = "0x169FF30", VA = "0x10169FF30")]
		public void SetRequestData(int requestCueSheetIndex, int requestNamedIndex, int requestCharaIdForSelector)
		{
		}

		// Token: 0x06000B9F RID: 2975 RVA: 0x00004800 File Offset: 0x00002A00
		[Token(Token = "0x6000ACD")]
		[Address(RVA = "0x10169FF3C", Offset = "0x169FF3C", VA = "0x10169FF3C")]
		public int GetRequestCueSheetIndex()
		{
			return 0;
		}

		// Token: 0x06000BA0 RID: 2976 RVA: 0x00004818 File Offset: 0x00002A18
		[Token(Token = "0x6000ACE")]
		[Address(RVA = "0x10169FF44", Offset = "0x169FF44", VA = "0x10169FF44")]
		public int GetRequestNamedIndex()
		{
			return 0;
		}

		// Token: 0x06000BA1 RID: 2977 RVA: 0x00004830 File Offset: 0x00002A30
		[Token(Token = "0x6000ACF")]
		[Address(RVA = "0x10169FF4C", Offset = "0x169FF4C", VA = "0x10169FF4C")]
		public int GetRequestCharaIdForSelector()
		{
			return 0;
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AD0")]
		[Address(RVA = "0x10169FF54", Offset = "0x169FF54", VA = "0x10169FF54")]
		public ActionAggregationChildVoiceActionAdapter()
		{
		}

		// Token: 0x04000CA3 RID: 3235
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000A03")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EB18", Offset = "0x11EB18")]
		private int m_RequestCueSheetIndex;

		// Token: 0x04000CA4 RID: 3236
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A04")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EB64", Offset = "0x11EB64")]
		private int m_RequestNamedIndex;

		// Token: 0x04000CA5 RID: 3237
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000A05")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011EBB0", Offset = "0x11EBB0")]
		private int m_RequestCharaIdForSelector;
	}
}
