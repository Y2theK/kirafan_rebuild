﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200044F RID: 1103
	[Token(Token = "0x200036A")]
	[StructLayout(3)]
	public class AdjustOrthographicCamera : MonoBehaviour
	{
		// Token: 0x060010D0 RID: 4304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F8E")]
		[Address(RVA = "0x10169FF6C", Offset = "0x169FF6C", VA = "0x10169FF6C")]
		private void Start()
		{
		}

		// Token: 0x060010D1 RID: 4305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F8F")]
		[Address(RVA = "0x1016A01E8", Offset = "0x16A01E8", VA = "0x1016A01E8")]
		private void Update()
		{
		}

		// Token: 0x060010D2 RID: 4306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F90")]
		[Address(RVA = "0x1016A00C4", Offset = "0x16A00C4", VA = "0x1016A00C4")]
		public void AdjustOrthographic()
		{
		}

		// Token: 0x060010D3 RID: 4307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F91")]
		[Address(RVA = "0x1016A01EC", Offset = "0x16A01EC", VA = "0x1016A01EC")]
		public AdjustOrthographicCamera()
		{
		}

		// Token: 0x040013B4 RID: 5044
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E57")]
		[SerializeField]
		private float m_BaseWidth;

		// Token: 0x040013B5 RID: 5045
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000E58")]
		[SerializeField]
		private float m_BaseHeight;

		// Token: 0x040013B6 RID: 5046
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E59")]
		[SerializeField]
		private float m_BasePixelPerUnit;

		// Token: 0x040013B7 RID: 5047
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000E5A")]
		[SerializeField]
		private bool m_IsDestroyOnStart;

		// Token: 0x040013B8 RID: 5048
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E5B")]
		private Camera m_Camera;
	}
}
