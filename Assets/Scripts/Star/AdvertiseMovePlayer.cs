﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AF7 RID: 2807
	[Token(Token = "0x20007AC")]
	[StructLayout(3)]
	public class AdvertiseMovePlayer : MonoBehaviour
	{
		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x060031A2 RID: 12706 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000393")]
		public MovieCanvas movieCanvas
		{
			[Token(Token = "0x6002D5D")]
			[Address(RVA = "0x1016A0210", Offset = "0x16A0210", VA = "0x1016A0210")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x060031A3 RID: 12707 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000394")]
		public MoviePlayUI moviePlayUI
		{
			[Token(Token = "0x6002D5E")]
			[Address(RVA = "0x1016A0218", Offset = "0x16A0218", VA = "0x1016A0218")]
			get
			{
				return null;
			}
		}

		// Token: 0x060031A4 RID: 12708 RVA: 0x00015378 File Offset: 0x00013578
		[Token(Token = "0x6002D5F")]
		[Address(RVA = "0x1016A0220", Offset = "0x16A0220", VA = "0x1016A0220")]
		public bool Setup(string moveName, bool isImmediatePlay)
		{
			return default(bool);
		}

		// Token: 0x060031A5 RID: 12709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D60")]
		[Address(RVA = "0x1016A060C", Offset = "0x16A060C", VA = "0x1016A060C")]
		public void Destroy()
		{
		}

		// Token: 0x060031A6 RID: 12710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D61")]
		[Address(RVA = "0x1016A0648", Offset = "0x16A0648", VA = "0x1016A0648")]
		private void OnDestroy()
		{
		}

		// Token: 0x060031A7 RID: 12711 RVA: 0x00015390 File Offset: 0x00013590
		[Token(Token = "0x6002D62")]
		[Address(RVA = "0x1016A064C", Offset = "0x16A064C", VA = "0x1016A064C")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x060031A8 RID: 12712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D63")]
		[Address(RVA = "0x1016A065C", Offset = "0x16A065C", VA = "0x1016A065C")]
		public void EnableRender(bool flg)
		{
		}

		// Token: 0x060031A9 RID: 12713 RVA: 0x000153A8 File Offset: 0x000135A8
		[Token(Token = "0x6002D64")]
		[Address(RVA = "0x1016A0444", Offset = "0x16A0444", VA = "0x1016A0444")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x060031AA RID: 12714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D65")]
		[Address(RVA = "0x1016A077C", Offset = "0x16A077C", VA = "0x1016A077C")]
		public void ForceStop()
		{
		}

		// Token: 0x060031AB RID: 12715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D66")]
		[Address(RVA = "0x1016A07DC", Offset = "0x16A07DC", VA = "0x1016A07DC")]
		public void SetPlayEndCallback(Action callback)
		{
		}

		// Token: 0x060031AC RID: 12716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D67")]
		[Address(RVA = "0x1016A07E4", Offset = "0x16A07E4", VA = "0x1016A07E4")]
		private void OnExecSkip()
		{
		}

		// Token: 0x060031AD RID: 12717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D68")]
		[Address(RVA = "0x1016A07F0", Offset = "0x16A07F0", VA = "0x1016A07F0")]
		private void Update()
		{
		}

		// Token: 0x060031AE RID: 12718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D69")]
		[Address(RVA = "0x1016A0938", Offset = "0x16A0938", VA = "0x1016A0938")]
		public AdvertiseMovePlayer()
		{
		}

		// Token: 0x04004117 RID: 16663
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E3B")]
		[SerializeField]
		private MovieCanvas m_MovieCanvas;

		// Token: 0x04004118 RID: 16664
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E3C")]
		[SerializeField]
		private MoviePlayUI m_MoviePlayUI;

		// Token: 0x04004119 RID: 16665
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E3D")]
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x0400411A RID: 16666
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002E3E")]
		private Action m_OnPlayEnd;

		// Token: 0x0400411B RID: 16667
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002E3F")]
		private string m_MovieName;

		// Token: 0x0400411C RID: 16668
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002E40")]
		private bool m_isImmediatePlay;

		// Token: 0x0400411D RID: 16669
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002E41")]
		private AdvertiseMovePlayer.eStep m_Step;

		// Token: 0x0400411E RID: 16670
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E42")]
		private bool m_SkipFlg;

		// Token: 0x02000AF8 RID: 2808
		[Token(Token = "0x2001018")]
		private enum eStep
		{
			// Token: 0x04004120 RID: 16672
			[Token(Token = "0x4006649")]
			Invalid = -1,
			// Token: 0x04004121 RID: 16673
			[Token(Token = "0x400664A")]
			Prepare,
			// Token: 0x04004122 RID: 16674
			[Token(Token = "0x400664B")]
			Wait,
			// Token: 0x04004123 RID: 16675
			[Token(Token = "0x400664C")]
			Playing
		}
	}
}
