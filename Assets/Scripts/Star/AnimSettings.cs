﻿using Meige;

namespace Star {
    public struct AnimSettings {
        public string m_ResoucePath;
        public string m_ObjectNameWithoutExt;
        public string m_ActionKey;
        public bool m_IsPack;

        public AnimSettings(string actionKey, string resoucePath) {
            m_ActionKey = actionKey;
            m_ResoucePath = resoucePath;
            if (!m_ResoucePath.Contains(MeigeResourceManager.AB_EXTENSION)) {
                m_ResoucePath += MeigeResourceManager.AB_EXTENSION;
            }
            m_ObjectNameWithoutExt = PathUtility.GetFileNameWithoutExtension(resoucePath);
            m_IsPack = false;
        }

        public AnimSettings(string actionKey, string resoucePath, string objectNameWithoutExt) {
            m_ActionKey = actionKey;
            m_ResoucePath = resoucePath;
            m_ObjectNameWithoutExt = objectNameWithoutExt;
            m_IsPack = true;
        }
    }
}
