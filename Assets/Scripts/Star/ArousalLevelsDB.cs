﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004AB RID: 1195
	[Token(Token = "0x20003A3")]
	[StructLayout(3)]
	public class ArousalLevelsDB : ScriptableObject
	{
		// Token: 0x060013F7 RID: 5111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AC")]
		[Address(RVA = "0x1016A0A9C", Offset = "0x16A0A9C", VA = "0x1016A0A9C")]
		public ArousalLevelsDB()
		{
		}

		// Token: 0x04001652 RID: 5714
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001038")]
		public ArousalLevelsDB_Param[] m_Params;
	}
}
