﻿namespace Star {
    public static class ArousalLevelsDB_Ext {
        public static bool IsFinalLv(this ArousalLevelsDB_Param self) {
            return self.m_DuplicatedCountForNextLv == 0;
        }

        public static ArousalLevelsDB_Param? GetParam(this ArousalLevelsDB self, eRare rare, int lv) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_Rare == (int)rare && self.m_Params[i].m_Lv == lv) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static int GetStatusParam(this ArousalLevelsDB_Param self, BattleDefine.eStatus st) {
            return st switch {
                BattleDefine.eStatus.Atk => self.m_Atk,
                BattleDefine.eStatus.Mgc => self.m_Mgc,
                BattleDefine.eStatus.Def => self.m_Def,
                BattleDefine.eStatus.MDef => self.m_MDef,
                _ => 0,
            };
        }

        public static int GetDuplicatedCountForNextLv(this ArousalLevelsDB self, eRare rare, int lv) {
            ArousalLevelsDB_Param? param = self.GetParam(rare, lv);
            if (param.HasValue) {
                return param.Value.m_DuplicatedCountForNextLv;
            }
            return 0;
        }

        public static int[] GetDuplicatedCountsForNextLv(this ArousalLevelsDB self, eRare rare, int lv_a, int lv_b) {
            int lvs = lv_b - lv_a + 1;
            if (lvs <= 0) {
                return null;
            }
            int[] dupes = new int[lvs];
            for (int i = 0; i < lvs; i++) {
                dupes[i] = self.GetDuplicatedCountForNextLv(rare, lv_a + i);
            }
            return dupes;
        }

        public static int GetDuplicatedCountForMaxLv(this ArousalLevelsDB self, eRare rare) {
            int lv = 0;
            int dupes = 0;
            while (!self.IsMax(rare, lv)) {
                dupes += self.GetDuplicatedCountForNextLv(rare, lv);
                lv++;
            }
            return dupes;
        }

        public static int GetCurrentDuplicatedCount(this ArousalLevelsDB self, eRare rare, int currentLv, int totalDuplicatedCount) {
            for (int i = 0; i < currentLv; i++) {
                totalDuplicatedCount -= self.GetDuplicatedCountForNextLv(rare, i);
            }
            return totalDuplicatedCount > 0 ? totalDuplicatedCount : 0;
        }

        public static bool IsMax(this ArousalLevelsDB self, eRare rare, int lv) {
            ArousalLevelsDB_Param? param = self.GetParam(rare, lv);
            return param.HasValue && param.Value.IsFinalLv();
        }
    }
}
