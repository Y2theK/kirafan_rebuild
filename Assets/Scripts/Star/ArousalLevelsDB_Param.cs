﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004AA RID: 1194
	[Token(Token = "0x20003A2")]
	[Serializable]
	[StructLayout(0, Size = 48)]
	public struct ArousalLevelsDB_Param
	{
		// Token: 0x04001646 RID: 5702
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400102C")]
		public int m_ID;

		// Token: 0x04001647 RID: 5703
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400102D")]
		public int m_Rare;

		// Token: 0x04001648 RID: 5704
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400102E")]
		public int m_Lv;

		// Token: 0x04001649 RID: 5705
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400102F")]
		public int m_DuplicatedCountForNextLv;

		// Token: 0x0400164A RID: 5706
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001030")]
		public int m_Hp;

		// Token: 0x0400164B RID: 5707
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001031")]
		public int m_Atk;

		// Token: 0x0400164C RID: 5708
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001032")]
		public int m_Mgc;

		// Token: 0x0400164D RID: 5709
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001033")]
		public int m_Def;

		// Token: 0x0400164E RID: 5710
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001034")]
		public int m_MDef;

		// Token: 0x0400164F RID: 5711
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001035")]
		public int m_SkillLvLimit;

		// Token: 0x04001650 RID: 5712
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001036")]
		public int m_SkillExp;

		// Token: 0x04001651 RID: 5713
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001037")]
		public uint m_MinusCost;
	}
}
