﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Arousal;

namespace Star
{
	// Token: 0x02000365 RID: 869
	[Token(Token = "0x20002E2")]
	[StructLayout(3)]
	public class ArousalLvUp
	{
		// Token: 0x06000BCF RID: 3023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AFD")]
		[Address(RVA = "0x1016A1034", Offset = "0x16A1034", VA = "0x1016A1034")]
		public ArousalLvUp(ArousalPopupUI uiArousalPopup, ArousalResultUI uiArousalResult)
		{
		}

		// Token: 0x06000BD0 RID: 3024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AFE")]
		[Address(RVA = "0x1016A1078", Offset = "0x16A1078", VA = "0x1016A1078")]
		private void NextPlayIndex()
		{
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x00004980 File Offset: 0x00002B80
		[Token(Token = "0x6000AFF")]
		[Address(RVA = "0x1016A117C", Offset = "0x16A117C", VA = "0x1016A117C")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x00004998 File Offset: 0x00002B98
		[Token(Token = "0x6000B00")]
		[Address(RVA = "0x1016A1240", Offset = "0x16A1240", VA = "0x1016A1240")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x06000BD3 RID: 3027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B01")]
		[Address(RVA = "0x1016A1610", Offset = "0x16A1610", VA = "0x1016A1610")]
		private void SubOfferWaitFinished(bool b)
		{
		}

		// Token: 0x06000BD4 RID: 3028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B02")]
		[Address(RVA = "0x1016A161C", Offset = "0x16A161C", VA = "0x1016A161C")]
		private void OnAllSkip()
		{
		}

		// Token: 0x04000CE5 RID: 3301
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A45")]
		private ArousalLvUp.eStep m_Step;

		// Token: 0x04000CE6 RID: 3302
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A46")]
		private ArousalPopupUI m_uiArousalPopup;

		// Token: 0x04000CE7 RID: 3303
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A47")]
		private ArousalResultUI m_uiArousalResult;

		// Token: 0x04000CE8 RID: 3304
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A48")]
		private List<UserArousalResultData> m_Datas;

		// Token: 0x04000CE9 RID: 3305
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A49")]
		private ArousalLvUpEffectScene m_EffectScene;

		// Token: 0x04000CEA RID: 3306
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A4A")]
		private int m_EffectPlayIndex;

		// Token: 0x04000CEB RID: 3307
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000A4B")]
		private bool m_AllSkipped;

		// Token: 0x02000366 RID: 870
		[Token(Token = "0x2000D58")]
		private enum eStep
		{
			// Token: 0x04000CED RID: 3309
			[Token(Token = "0x4005626")]
			None = -1,
			// Token: 0x04000CEE RID: 3310
			[Token(Token = "0x4005627")]
			PopUp,
			// Token: 0x04000CEF RID: 3311
			[Token(Token = "0x4005628")]
			PopUp_Wait,
			// Token: 0x04000CF0 RID: 3312
			[Token(Token = "0x4005629")]
			EffectPrepare,
			// Token: 0x04000CF1 RID: 3313
			[Token(Token = "0x400562A")]
			EffectPrepare_Wait,
			// Token: 0x04000CF2 RID: 3314
			[Token(Token = "0x400562B")]
			EffectPlay_Wait,
			// Token: 0x04000CF3 RID: 3315
			[Token(Token = "0x400562C")]
			PreEnd,
			// Token: 0x04000CF4 RID: 3316
			[Token(Token = "0x400562D")]
			SubOffer_Wait,
			// Token: 0x04000CF5 RID: 3317
			[Token(Token = "0x400562E")]
			End
		}
	}
}
