﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Arousal;
using UnityEngine;

namespace Star
{
	// Token: 0x02000367 RID: 871
	[Token(Token = "0x20002E3")]
	[StructLayout(3)]
	public class ArousalLvUpEffectHandler : MonoBehaviour
	{
		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06000BD5 RID: 3029 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06000BD6 RID: 3030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000A")]
		public event Action OnWhiteout
		{
			[Token(Token = "0x6000B03")]
			[Address(RVA = "0x101104A70", Offset = "0x1104A70", VA = "0x101104A70")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000B04")]
			[Address(RVA = "0x101104B7C", Offset = "0x1104B7C", VA = "0x101104B7C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000BD7 RID: 3031 RVA: 0x000049B0 File Offset: 0x00002BB0
		[Token(Token = "0x1700007A")]
		public ArousalLvUpEffectHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x6000B05")]
			[Address(RVA = "0x101104C88", Offset = "0x1104C88", VA = "0x101104C88")]
			get
			{
				return ArousalLvUpEffectHandler.eAnimType.Effect;
			}
		}

		// Token: 0x06000BD8 RID: 3032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B06")]
		[Address(RVA = "0x101104C90", Offset = "0x1104C90", VA = "0x101104C90")]
		private void Awake()
		{
		}

		// Token: 0x06000BD9 RID: 3033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B07")]
		[Address(RVA = "0x101104ED4", Offset = "0x1104ED4", VA = "0x101104ED4")]
		private void OnDestroy()
		{
		}

		// Token: 0x06000BDA RID: 3034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B08")]
		[Address(RVA = "0x101104ED8", Offset = "0x1104ED8", VA = "0x101104ED8")]
		public void Destroy()
		{
		}

		// Token: 0x06000BDB RID: 3035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B09")]
		[Address(RVA = "0x101104F3C", Offset = "0x1104F3C", VA = "0x101104F3C")]
		private void Update()
		{
		}

		// Token: 0x06000BDC RID: 3036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B0A")]
		[Address(RVA = "0x10110515C", Offset = "0x110515C", VA = "0x10110515C")]
		public void ApplyTexture(Texture tex_charaIllust)
		{
		}

		// Token: 0x06000BDD RID: 3037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B0B")]
		[Address(RVA = "0x1011051D0", Offset = "0x11051D0", VA = "0x1011051D0")]
		public void SetUI(ArousalResultUI ui)
		{
		}

		// Token: 0x06000BDE RID: 3038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B0C")]
		[Address(RVA = "0x10110506C", Offset = "0x110506C", VA = "0x10110506C")]
		public void Play(ArousalLvUpEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x000049C8 File Offset: 0x00002BC8
		[Token(Token = "0x6000B0D")]
		[Address(RVA = "0x101104F90", Offset = "0x1104F90", VA = "0x101104F90")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B0E")]
		[Address(RVA = "0x1011051D8", Offset = "0x11051D8", VA = "0x1011051D8")]
		private string GetAnimPlayKey(ArousalLvUpEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x06000BE1 RID: 3041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B0F")]
		[Address(RVA = "0x101105228", Offset = "0x1105228", VA = "0x101105228")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x06000BE2 RID: 3042 RVA: 0x000049E0 File Offset: 0x00002BE0
		[Token(Token = "0x6000B10")]
		[Address(RVA = "0x101105280", Offset = "0x1105280", VA = "0x101105280")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x06000BE3 RID: 3043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B11")]
		[Address(RVA = "0x1011052B0", Offset = "0x11052B0", VA = "0x1011052B0")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x06000BE4 RID: 3044 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B12")]
		[Address(RVA = "0x101105334", Offset = "0x1105334", VA = "0x101105334")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x100135AE4", Offset = "0x135AE4")]
		private IEnumerator Whiteout()
		{
			return null;
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B13")]
		[Address(RVA = "0x1011053D4", Offset = "0x11053D4", VA = "0x1011053D4")]
		public ArousalLvUpEffectHandler()
		{
		}

		// Token: 0x04000CF6 RID: 3318
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A4C")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x04000CF7 RID: 3319
		[Token(Token = "0x4000A4D")]
		private const string OBJ_CHARA_ILLUST = "CharaIllust";

		// Token: 0x04000CF8 RID: 3320
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A4E")]
		public Transform m_Transform;

		// Token: 0x04000CF9 RID: 3321
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A4F")]
		public Animation m_Anim;

		// Token: 0x04000CFA RID: 3322
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A50")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000CFB RID: 3323
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A51")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x04000CFC RID: 3324
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000A52")]
		public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x04000CFD RID: 3325
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000A53")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04000CFE RID: 3326
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000A54")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x04000CFF RID: 3327
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000A55")]
		private ArousalLvUpEffectHandler.eAnimType m_CurrentAnimType;

		// Token: 0x04000D00 RID: 3328
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000A56")]
		private ArousalResultUI m_UI;

		// Token: 0x04000D01 RID: 3329
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000A57")]
		private Coroutine m_WhiteoutCoroutine;

		// Token: 0x02000368 RID: 872
		[Token(Token = "0x2000D59")]
		public enum eAnimType
		{
			// Token: 0x04000D04 RID: 3332
			[Token(Token = "0x4005630")]
			None = -1,
			// Token: 0x04000D05 RID: 3333
			[Token(Token = "0x4005631")]
			Effect,
			// Token: 0x04000D06 RID: 3334
			[Token(Token = "0x4005632")]
			Loop,
			// Token: 0x04000D07 RID: 3335
			[Token(Token = "0x4005633")]
			Num
		}

		// Token: 0x02000369 RID: 873
		[Token(Token = "0x2000D5A")]
		public enum eAnimEvent
		{
			// Token: 0x04000D09 RID: 3337
			[Token(Token = "0x4005635")]
			Whiteout = 102
		}
	}
}
