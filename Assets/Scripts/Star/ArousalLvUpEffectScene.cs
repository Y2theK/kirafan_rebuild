﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Arousal;
using UnityEngine;

namespace Star
{
	// Token: 0x0200036B RID: 875
	[Token(Token = "0x20002E4")]
	[StructLayout(3)]
	public class ArousalLvUpEffectScene
	{
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000BEC RID: 3052 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700007B")]
		public ArousalLvUpEffectHandler EffectHndl
		{
			[Token(Token = "0x6000B14")]
			[Address(RVA = "0x101105770", Offset = "0x1105770", VA = "0x101105770")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000BED RID: 3053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B15")]
		[Address(RVA = "0x101105778", Offset = "0x1105778", VA = "0x101105778")]
		public ArousalLvUpEffectScene(ArousalResultUI uiArousalResult)
		{
		}

		// Token: 0x06000BEE RID: 3054 RVA: 0x00004A10 File Offset: 0x00002C10
		[Token(Token = "0x6000B16")]
		[Address(RVA = "0x101105864", Offset = "0x1105864", VA = "0x101105864")]
		public bool Destroy()
		{
			return default(bool);
		}

		// Token: 0x06000BEF RID: 3055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B17")]
		[Address(RVA = "0x101105A38", Offset = "0x1105A38", VA = "0x101105A38")]
		public void Update()
		{
		}

		// Token: 0x06000BF0 RID: 3056 RVA: 0x00004A28 File Offset: 0x00002C28
		[Token(Token = "0x6000B18")]
		[Address(RVA = "0x101105F2C", Offset = "0x1105F2C", VA = "0x101105F2C")]
		public bool Prepare(UserArousalResultData data, Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x06000BF1 RID: 3057 RVA: 0x00004A40 File Offset: 0x00002C40
		[Token(Token = "0x6000B19")]
		[Address(RVA = "0x10110602C", Offset = "0x110602C", VA = "0x10110602C")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06000BF2 RID: 3058 RVA: 0x00004A58 File Offset: 0x00002C58
		[Token(Token = "0x6000B1A")]
		[Address(RVA = "0x10110603C", Offset = "0x110603C", VA = "0x10110603C")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000BF3 RID: 3059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B1B")]
		[Address(RVA = "0x101105A54", Offset = "0x1105A54", VA = "0x101105A54")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06000BF4 RID: 3060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B1C")]
		[Address(RVA = "0x10110604C", Offset = "0x110604C", VA = "0x10110604C")]
		public void Play()
		{
		}

		// Token: 0x06000BF5 RID: 3061 RVA: 0x00004A70 File Offset: 0x00002C70
		[Token(Token = "0x6000B1D")]
		[Address(RVA = "0x10110610C", Offset = "0x110610C", VA = "0x10110610C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000BF6 RID: 3062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B1E")]
		[Address(RVA = "0x10110611C", Offset = "0x110611C", VA = "0x10110611C")]
		public void Fadeout()
		{
		}

		// Token: 0x06000BF7 RID: 3063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B1F")]
		[Address(RVA = "0x101105EC4", Offset = "0x1105EC4", VA = "0x101105EC4")]
		private void Update_Main()
		{
		}

		// Token: 0x06000BF8 RID: 3064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B20")]
		[Address(RVA = "0x101106204", Offset = "0x1106204", VA = "0x101106204")]
		private void OnWhiteout()
		{
		}

		// Token: 0x04000D0D RID: 3341
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000A59")]
		private ArousalLvUpEffectScene.eMode m_Mode;

		// Token: 0x04000D0E RID: 3342
		[Token(Token = "0x4000A5A")]
		private const string RESOURCE_PATH = "arousallvup.muast";

		// Token: 0x04000D0F RID: 3343
		[Token(Token = "0x4000A5B")]
		private const string OBJECT_PATH = "ArousalLvUp";

		// Token: 0x04000D10 RID: 3344
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000A5C")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x04000D11 RID: 3345
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A5D")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000D12 RID: 3346
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A5E")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000D13 RID: 3347
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A5F")]
		private SpriteHandler m_SpriteHndl_CharaIllust;

		// Token: 0x04000D14 RID: 3348
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A60")]
		private GameObject m_EffectGO;

		// Token: 0x04000D15 RID: 3349
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A61")]
		private ArousalLvUpEffectHandler m_EffectHndl;

		// Token: 0x04000D16 RID: 3350
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000A62")]
		private Transform m_ParentTransform;

		// Token: 0x04000D17 RID: 3351
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000A63")]
		private Vector3 m_Position;

		// Token: 0x04000D18 RID: 3352
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000A64")]
		private UserArousalResultData m_Data;

		// Token: 0x04000D19 RID: 3353
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000A65")]
		private ArousalResultUI m_uiArousalResult;

		// Token: 0x04000D1A RID: 3354
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000A66")]
		private SoundSeRequest m_SeRequest;

		// Token: 0x04000D1B RID: 3355
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000A67")]
		private ArousalLvUpEffectScene.ePrepareStep m_PrepareStep;

		// Token: 0x04000D1C RID: 3356
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4000A68")]
		private ArousalLvUpEffectScene.eMainStep m_MainStep;

		// Token: 0x0200036C RID: 876
		[Token(Token = "0x2000D5C")]
		public enum eMode
		{
			// Token: 0x04000D1E RID: 3358
			[Token(Token = "0x400563A")]
			None = -1,
			// Token: 0x04000D1F RID: 3359
			[Token(Token = "0x400563B")]
			Prepare,
			// Token: 0x04000D20 RID: 3360
			[Token(Token = "0x400563C")]
			UpdateMain,
			// Token: 0x04000D21 RID: 3361
			[Token(Token = "0x400563D")]
			Destroy
		}

		// Token: 0x0200036D RID: 877
		[Token(Token = "0x2000D5D")]
		private enum ePrepareStep
		{
			// Token: 0x04000D23 RID: 3363
			[Token(Token = "0x400563F")]
			None = -1,
			// Token: 0x04000D24 RID: 3364
			[Token(Token = "0x4005640")]
			Prepare,
			// Token: 0x04000D25 RID: 3365
			[Token(Token = "0x4005641")]
			Prepare_Wait,
			// Token: 0x04000D26 RID: 3366
			[Token(Token = "0x4005642")]
			End,
			// Token: 0x04000D27 RID: 3367
			[Token(Token = "0x4005643")]
			Prepare_Error
		}

		// Token: 0x0200036E RID: 878
		[Token(Token = "0x2000D5E")]
		private enum eMainStep
		{
			// Token: 0x04000D29 RID: 3369
			[Token(Token = "0x4005645")]
			None = -1,
			// Token: 0x04000D2A RID: 3370
			[Token(Token = "0x4005646")]
			Play,
			// Token: 0x04000D2B RID: 3371
			[Token(Token = "0x4005647")]
			Play_Wait,
			// Token: 0x04000D2C RID: 3372
			[Token(Token = "0x4005648")]
			End
		}
	}
}
