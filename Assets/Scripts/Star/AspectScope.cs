﻿using UnityEngine;

namespace Star {
    public class AspectScope : MonoBehaviour {
        private Camera m_Camera;

        private void CameraSetup() {
            if (m_Camera != null) { return; }

            m_Camera = GetComponent<Camera>();
            FixSafeArea();
        }

        private void Awake() {
            CameraSetup();
        }

        private void FixSafeArea() {
            Rect rect = new Rect(0f, 0f, 1f, 1f);
            float screenH = Screen.height;
            float screenW = Screen.width;
            if (screenH / screenW < UIDefine.IDEAL_ASPECT_RATIO) {
                rect.height = 1f;
                rect.width = (screenH / UIDefine.IDEAL_ASPECT_RATIO) / screenW;
                rect.y = 0f;
                rect.x = (1f - rect.width) * 0.5f;
            }
            FixSafeArea(rect);
        }

        private void FixSafeArea(Rect safeArea) {
            if (m_Camera != null) {
                m_Camera.rect = safeArea;
            }
        }

        public bool FixSafeAreaVertical() {
            float screenH = Screen.height;
            float screenW = Screen.width;
            bool fixArea = screenH / screenW > UIDefine.IDEAL_ASPECT_RATIO;
            if (fixArea) {
                Rect rect = new Rect(0f, 0f, 1f, 1f);
                rect.width = 1f;
                rect.height = (screenW * UIDefine.IDEAL_ASPECT_RATIO) / screenH;
                rect.x = 0f;
                rect.y = (1f - rect.height) * 0.5f;
                FixSafeArea(rect);
            }
            return fixArea;
        }

        public void RevertSafeAreaVertical() {
            FixSafeArea();
        }
    }
}
