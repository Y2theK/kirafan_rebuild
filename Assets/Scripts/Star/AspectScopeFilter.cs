﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000451 RID: 1105
	[Token(Token = "0x200036C")]
	[StructLayout(3)]
	public class AspectScopeFilter : MonoBehaviour
	{
		// Token: 0x060010DB RID: 4315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F99")]
		[Address(RVA = "0x10110663C", Offset = "0x110663C", VA = "0x10110663C")]
		private void Setup()
		{
		}

		// Token: 0x060010DC RID: 4316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F9A")]
		[Address(RVA = "0x1011069DC", Offset = "0x11069DC", VA = "0x1011069DC")]
		private void Awake()
		{
		}

		// Token: 0x060010DD RID: 4317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F9B")]
		[Address(RVA = "0x1011069E0", Offset = "0x11069E0", VA = "0x1011069E0")]
		private void Start()
		{
		}

		// Token: 0x060010DE RID: 4318 RVA: 0x00007170 File Offset: 0x00005370
		[Token(Token = "0x6000F9C")]
		[Address(RVA = "0x1011069E4", Offset = "0x11069E4", VA = "0x1011069E4")]
		public Vector2 GetCineScoImagesSizeDelta()
		{
			return default(Vector2);
		}

		// Token: 0x060010DF RID: 4319 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F9D")]
		[Address(RVA = "0x101106A14", Offset = "0x1106A14", VA = "0x101106A14")]
		public Image GetCineScoImages(int index)
		{
			return null;
		}

		// Token: 0x060010E0 RID: 4320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F9E")]
		[Address(RVA = "0x101106A64", Offset = "0x1106A64", VA = "0x101106A64")]
		public AspectScopeFilter()
		{
		}

		// Token: 0x040013BA RID: 5050
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E5D")]
		[SerializeField]
		private RectTransform m_CineScoImagesTransform0;

		// Token: 0x040013BB RID: 5051
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E5E")]
		[SerializeField]
		private RectTransform m_CineScoImagesTransform1;

		// Token: 0x040013BC RID: 5052
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E5F")]
		[SerializeField]
		private Image[] m_CineScoImagesImage;

		// Token: 0x040013BD RID: 5053
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E60")]
		private bool m_isAvailable;
	}
}
