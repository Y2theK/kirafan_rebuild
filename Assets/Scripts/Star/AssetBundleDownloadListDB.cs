﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004AD RID: 1197
	[Token(Token = "0x20003A5")]
	[StructLayout(3)]
	public class AssetBundleDownloadListDB : ScriptableObject
	{
		// Token: 0x060013F8 RID: 5112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AD")]
		[Address(RVA = "0x101106A6C", Offset = "0x1106A6C", VA = "0x101106A6C")]
		public AssetBundleDownloadListDB()
		{
		}

		// Token: 0x04001654 RID: 5716
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400103A")]
		public AssetBundleDownloadListDB_Param[] m_Params;
	}
}
