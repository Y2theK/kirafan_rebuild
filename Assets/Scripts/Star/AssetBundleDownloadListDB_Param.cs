﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004AC RID: 1196
	[Token(Token = "0x20003A4")]
	[Serializable]
	[StructLayout(0)]
	public struct AssetBundleDownloadListDB_Param
	{
		// Token: 0x04001653 RID: 5715
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001039")]
		public string[] m_Paths;
	}
}
