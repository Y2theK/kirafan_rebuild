﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200032D RID: 813
	[Token(Token = "0x20002BA")]
	[StructLayout(3)]
	public class AtlasSprite
	{
		// Token: 0x06000A91 RID: 2705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60009C6")]
		[Address(RVA = "0x101106D2C", Offset = "0x1106D2C", VA = "0x101106D2C")]
		public void LoadAtlas(string path)
		{
		}

		// Token: 0x06000A92 RID: 2706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60009C7")]
		[Address(RVA = "0x101106E60", Offset = "0x1106E60", VA = "0x101106E60")]
		public void Dispose()
		{
		}

		// Token: 0x06000A93 RID: 2707 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60009C8")]
		[Address(RVA = "0x101106FF0", Offset = "0x1106FF0", VA = "0x101106FF0")]
		public Sprite GetSprite(string imgName)
		{
			return null;
		}

		// Token: 0x06000A94 RID: 2708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60009C9")]
		[Address(RVA = "0x101107094", Offset = "0x1107094", VA = "0x101107094")]
		public AtlasSprite()
		{
		}

		// Token: 0x04000BBF RID: 3007
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400099B")]
		private Dictionary<string, Sprite> m_SpriteDict;
	}
}
