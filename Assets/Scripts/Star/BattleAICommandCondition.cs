﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200037A RID: 890
	[Token(Token = "0x20002F0")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAICommandCondition
	{
		// Token: 0x06000BFE RID: 3070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B26")]
		[Address(RVA = "0x101107260", Offset = "0x1107260", VA = "0x101107260")]
		public BattleAICommandCondition()
		{
		}

		// Token: 0x04000D99 RID: 3481
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000AD5")]
		public eBattleAICommandConditionType m_Type;

		// Token: 0x04000D9A RID: 3482
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AD6")]
		public float[] m_Args;
	}
}
