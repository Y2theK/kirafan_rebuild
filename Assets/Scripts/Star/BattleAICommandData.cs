﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000379 RID: 889
	[Token(Token = "0x20002EF")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAICommandData
	{
		// Token: 0x06000BFD RID: 3069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B25")]
		[Address(RVA = "0x101107268", Offset = "0x1107268", VA = "0x101107268")]
		public BattleAICommandData()
		{
		}

		// Token: 0x04000D96 RID: 3478
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000AD2")]
		public List<BattleAICommandCondition> m_Conditions;

		// Token: 0x04000D97 RID: 3479
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AD3")]
		public List<BattleAIExecData> m_ExecDatas;

		// Token: 0x04000D98 RID: 3480
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000AD4")]
		public int m_ExecNum;
	}
}
