﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200037D RID: 893
	[Token(Token = "0x20002F3")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAICommandTargetSingleCondition
	{
		// Token: 0x06000C01 RID: 3073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B29")]
		[Address(RVA = "0x101107300", Offset = "0x1107300", VA = "0x101107300")]
		public BattleAICommandTargetSingleCondition()
		{
		}

		// Token: 0x04000DA2 RID: 3490
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000ADE")]
		public eBattleAICommandTargetSingleConditionType m_Type;

		// Token: 0x04000DA3 RID: 3491
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000ADF")]
		public float[] m_Args;

		// Token: 0x04000DA4 RID: 3492
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000AE0")]
		public bool isFalse;
	}
}
