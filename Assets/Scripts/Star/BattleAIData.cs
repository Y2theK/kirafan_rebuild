﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200036F RID: 879
	[Token(Token = "0x20002E5")]
	[StructLayout(3)]
	public class BattleAIData : ScriptableObject
	{
		// Token: 0x06000BF9 RID: 3065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B21")]
		[Address(RVA = "0x101107308", Offset = "0x1107308", VA = "0x101107308")]
		public BattleAIData()
		{
		}

		// Token: 0x04000D2D RID: 3373
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000A69")]
		public int m_ID;

		// Token: 0x04000D2E RID: 3374
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000A6A")]
		public List<BattleAIPatternChangeData> m_PatternChangeDatas;

		// Token: 0x04000D2F RID: 3375
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000A6B")]
		public List<BattleAIPatternData> m_PatternDatas;

		// Token: 0x04000D30 RID: 3376
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000A6C")]
		public List<BattleAICommandData> m_ChargeCommandDatas;

		// Token: 0x04000D31 RID: 3377
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000A6D")]
		public eSingleTargetPriorityWhenHateSame m_SingleTargetPriorityWhenHateSame;
	}
}
