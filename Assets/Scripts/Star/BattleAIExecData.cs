﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200037B RID: 891
	[Token(Token = "0x20002F1")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAIExecData
	{
		// Token: 0x06000BFF RID: 3071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B27")]
		[Address(RVA = "0x101107378", Offset = "0x1107378", VA = "0x101107378")]
		public BattleAIExecData()
		{
		}

		// Token: 0x04000D9B RID: 3483
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000AD7")]
		public int m_CommandIndex;

		// Token: 0x04000D9C RID: 3484
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000AD8")]
		public bool m_IsCommandTargetSelectionInOrder;

		// Token: 0x04000D9D RID: 3485
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AD9")]
		public int m_Ratio;

		// Token: 0x04000D9E RID: 3486
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000ADA")]
		public List<BattleAIFlag> m_AIFlags;

		// Token: 0x04000D9F RID: 3487
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000ADB")]
		public List<BattleAICommandTargetSingleCondition> m_SingleConditions;
	}
}
