﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200037C RID: 892
	[Token(Token = "0x20002F2")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAIFlag
	{
		// Token: 0x06000C00 RID: 3072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B28")]
		[Address(RVA = "0x101107410", Offset = "0x1107410", VA = "0x101107410")]
		public BattleAIFlag()
		{
		}

		// Token: 0x04000DA0 RID: 3488
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000ADC")]
		public int m_ID;

		// Token: 0x04000DA1 RID: 3489
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000ADD")]
		public bool m_IsFlag;
	}
}
