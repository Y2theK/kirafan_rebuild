﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200037E RID: 894
	[Token(Token = "0x20002F4")]
	[StructLayout(3)]
	public static class BattleAIParser
	{
		// Token: 0x06000C02 RID: 3074 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B2A")]
		[Address(RVA = "0x101107418", Offset = "0x1107418", VA = "0x101107418")]
		public static BattleAISolveResult SolveBattleAI(CharacterHandler executor)
		{
			return null;
		}

		// Token: 0x06000C03 RID: 3075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B2B")]
		[Address(RVA = "0x101107500", Offset = "0x1107500", VA = "0x101107500")]
		private static void SolveBattleAI_ChargeSkill(CharacterHandler executor, BattleAISolveResult out_result)
		{
		}

		// Token: 0x06000C04 RID: 3076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B2C")]
		[Address(RVA = "0x1011075DC", Offset = "0x11075DC", VA = "0x1011075DC")]
		private static void SolveBattleAI_Skill(CharacterHandler executor, BattleAISolveResult out_result)
		{
		}

		// Token: 0x06000C05 RID: 3077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B2D")]
		[Address(RVA = "0x101107E60", Offset = "0x1107E60", VA = "0x101107E60")]
		private static void SolveBattleAI_ChangePattern(CharacterHandler executor)
		{
		}

		// Token: 0x06000C06 RID: 3078 RVA: 0x00004A88 File Offset: 0x00002C88
		[Token(Token = "0x6000B2E")]
		[Address(RVA = "0x10110779C", Offset = "0x110779C", VA = "0x10110779C")]
		private static bool SolveBattleAI_CommandDatas(CharacterHandler executor, List<BattleCommandData> commands, int patternIndex, List<BattleAICommandData> aiCommandDatas, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame, BattleAISolveResult out_result)
		{
			return default(bool);
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x00004AA0 File Offset: 0x00002CA0
		[Token(Token = "0x6000B2F")]
		[Address(RVA = "0x10110809C", Offset = "0x110809C", VA = "0x10110809C")]
		private static bool JudgeBattleAIPatternChangeConditions(CharacterHandler executor, List<BattleAIPatternChangeCondition> conditions)
		{
			return default(bool);
		}

		// Token: 0x06000C08 RID: 3080 RVA: 0x00004AB8 File Offset: 0x00002CB8
		[Token(Token = "0x6000B30")]
		[Address(RVA = "0x101108C04", Offset = "0x1108C04", VA = "0x101108C04")]
		private static bool JudgeBattleAIPatternChangeCondition(CharacterHandler executor, BattleAIPatternChangeCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C09 RID: 3081 RVA: 0x00004AD0 File Offset: 0x00002CD0
		[Token(Token = "0x6000B31")]
		[Address(RVA = "0x101108168", Offset = "0x1108168", VA = "0x101108168")]
		private static bool JudgeBattleAICommandConditions(CharacterHandler executor, List<BattleAICommandCondition> conditions)
		{
			return default(bool);
		}

		// Token: 0x06000C0A RID: 3082 RVA: 0x00004AE8 File Offset: 0x00002CE8
		[Token(Token = "0x6000B32")]
		[Address(RVA = "0x101108C24", Offset = "0x1108C24", VA = "0x101108C24")]
		private static bool JudgeBattleAICommandCondition(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C0B RID: 3083 RVA: 0x00004B00 File Offset: 0x00002D00
		[Token(Token = "0x6000B33")]
		[Address(RVA = "0x10110A45C", Offset = "0x110A45C", VA = "0x10110A45C")]
		private static bool Judge_IsEqualArg(float arg)
		{
			return default(bool);
		}

		// Token: 0x06000C0C RID: 3084 RVA: 0x00004B18 File Offset: 0x00002D18
		[Token(Token = "0x6000B34")]
		[Address(RVA = "0x10110A468", Offset = "0x110A468", VA = "0x10110A468")]
		private static bool Judge_IsRange(int val, int chk, BattleAIParser.eJudgeRangeCmp arg_cmp)
		{
			return default(bool);
		}

		// Token: 0x06000C0D RID: 3085 RVA: 0x00004B30 File Offset: 0x00002D30
		[Token(Token = "0x6000B35")]
		[Address(RVA = "0x10110A494", Offset = "0x110A494", VA = "0x10110A494")]
		private static bool Judge_IsStatusChange(CharacterHandler executor, int buffType, BattleAIParser.eJudgeIsCmp cmpType)
		{
			return default(bool);
		}

		// Token: 0x06000C0E RID: 3086 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B36")]
		[Address(RVA = "0x10110A7D0", Offset = "0x110A7D0", VA = "0x10110A7D0")]
		private static List<CharacterHandler> GetRangedJoinMember(CharacterHandler executor, BattleAICommandCondition condition, out int chkNum)
		{
			return null;
		}

		// Token: 0x06000C0F RID: 3087 RVA: 0x00004B48 File Offset: 0x00002D48
		[Token(Token = "0x6000B37")]
		[Address(RVA = "0x101108DE4", Offset = "0x1108DE4", VA = "0x101108DE4")]
		private static bool Judge_Self_HpRange(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C10 RID: 3088 RVA: 0x00004B60 File Offset: 0x00002D60
		[Token(Token = "0x6000B38")]
		[Address(RVA = "0x10110A97C", Offset = "0x110A97C", VA = "0x10110A97C")]
		private static bool JudgeSolo_Self_StateAbnormal(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C11 RID: 3089 RVA: 0x00004B78 File Offset: 0x00002D78
		[Token(Token = "0x6000B39")]
		[Address(RVA = "0x101108ED0", Offset = "0x1108ED0", VA = "0x101108ED0")]
		private static bool Judge_Self_StateAbnormal(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C12 RID: 3090 RVA: 0x00004B90 File Offset: 0x00002D90
		[Token(Token = "0x6000B3A")]
		[Address(RVA = "0x101109150", Offset = "0x1109150", VA = "0x101109150")]
		private static bool Judge_Self_StatusChange(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C13 RID: 3091 RVA: 0x00004BA8 File Offset: 0x00002DA8
		[Token(Token = "0x6000B3B")]
		[Address(RVA = "0x101109444", Offset = "0x1109444", VA = "0x101109444")]
		private static bool Judge_Self_ChargeCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C14 RID: 3092 RVA: 0x00004BC0 File Offset: 0x00002DC0
		[Token(Token = "0x6000B3C")]
		[Address(RVA = "0x101109554", Offset = "0x1109554", VA = "0x101109554")]
		private static bool Judge_MyParty_AliveNum(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C15 RID: 3093 RVA: 0x00004BD8 File Offset: 0x00002DD8
		[Token(Token = "0x6000B3D")]
		[Address(RVA = "0x101109668", Offset = "0x1109668", VA = "0x101109668")]
		private static bool Judge_TgtParty_AliveNum(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C16 RID: 3094 RVA: 0x00004BF0 File Offset: 0x00002DF0
		[Token(Token = "0x6000B3E")]
		[Address(RVA = "0x1011097DC", Offset = "0x11097DC", VA = "0x1011097DC")]
		private static bool Judge_TgtParty_DeadCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C17 RID: 3095 RVA: 0x00004C08 File Offset: 0x00002E08
		[Token(Token = "0x6000B3F")]
		[Address(RVA = "0x101109A1C", Offset = "0x1109A1C", VA = "0x101109A1C")]
		private static bool Judge_TgtParty_StateAbnormalCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C18 RID: 3096 RVA: 0x00004C20 File Offset: 0x00002E20
		[Token(Token = "0x6000B40")]
		[Address(RVA = "0x101109BC4", Offset = "0x1109BC4", VA = "0x101109BC4")]
		private static bool Judge_TgtParty_StatusChange(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C19 RID: 3097 RVA: 0x00004C38 File Offset: 0x00002E38
		[Token(Token = "0x6000B41")]
		[Address(RVA = "0x101109DF4", Offset = "0x1109DF4", VA = "0x101109DF4")]
		private static bool Judge_TgtParty_NormalAttackUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1A RID: 3098 RVA: 0x00004C50 File Offset: 0x00002E50
		[Token(Token = "0x6000B42")]
		[Address(RVA = "0x101109EAC", Offset = "0x1109EAC", VA = "0x101109EAC")]
		private static bool Judge_TgtParty_SkillUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1B RID: 3099 RVA: 0x00004C68 File Offset: 0x00002E68
		[Token(Token = "0x6000B43")]
		[Address(RVA = "0x101109F64", Offset = "0x1109F64", VA = "0x101109F64")]
		private static bool Judge_TgtParty_TogetherAttackUseCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1C RID: 3100 RVA: 0x00004C80 File Offset: 0x00002E80
		[Token(Token = "0x6000B44")]
		[Address(RVA = "0x10110A01C", Offset = "0x110A01C", VA = "0x10110A01C")]
		private static bool Judge_TgtParty_MemberChangeCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1D RID: 3101 RVA: 0x00004C98 File Offset: 0x00002E98
		[Token(Token = "0x6000B45")]
		[Address(RVA = "0x10110A0D4", Offset = "0x110A0D4", VA = "0x10110A0D4")]
		private static bool Judge_TgtParty_ClassCount(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1E RID: 3102 RVA: 0x00004CB0 File Offset: 0x00002EB0
		[Token(Token = "0x6000B46")]
		[Address(RVA = "0x10110A290", Offset = "0x110A290", VA = "0x10110A290")]
		private static bool Judge_Flag(CharacterHandler executor, BattleAICommandCondition condition)
		{
			return default(bool);
		}

		// Token: 0x06000C1F RID: 3103 RVA: 0x00004CC8 File Offset: 0x00002EC8
		[Token(Token = "0x6000B47")]
		[Address(RVA = "0x101108394", Offset = "0x1108394", VA = "0x101108394")]
		private static BattleDefine.eJoinMember SolveTargetJoin(CharacterHandler executor, BattleCommandData commandData, bool isCommandTargetSelectionInOrder, List<BattleAICommandTargetSingleCondition> singleConditions, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C20 RID: 3104 RVA: 0x00004CE0 File Offset: 0x00002EE0
		[Token(Token = "0x6000B48")]
		[Address(RVA = "0x10110AD28", Offset = "0x110AD28", VA = "0x10110AD28")]
		private static BattleDefine.eJoinMember SolveTargetJoinFromHateResults(List<BattleAIParser.BattleAIHateResult> hateResults, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C21 RID: 3105 RVA: 0x00004CF8 File Offset: 0x00002EF8
		[Token(Token = "0x6000B49")]
		[Address(RVA = "0x10110B154", Offset = "0x110B154", VA = "0x10110B154")]
		private static int CompareHateResult(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			return 0;
		}

		// Token: 0x06000C22 RID: 3106 RVA: 0x00004D10 File Offset: 0x00002F10
		[Token(Token = "0x6000B4A")]
		[Address(RVA = "0x10110B190", Offset = "0x110B190", VA = "0x10110B190")]
		private static int CompareHateResult_JoinABC(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			return 0;
		}

		// Token: 0x06000C23 RID: 3107 RVA: 0x00004D28 File Offset: 0x00002F28
		[Token(Token = "0x6000B4B")]
		[Address(RVA = "0x10110B1A8", Offset = "0x110B1A8", VA = "0x10110B1A8")]
		private static int CompareHateResult_JoinCBA(BattleAIParser.BattleAIHateResult A, BattleAIParser.BattleAIHateResult B)
		{
			return 0;
		}

		// Token: 0x06000C24 RID: 3108 RVA: 0x00004D40 File Offset: 0x00002F40
		[Token(Token = "0x6000B4C")]
		[Address(RVA = "0x10110B1C0", Offset = "0x110B1C0", VA = "0x10110B1C0")]
		public static BattleDefine.eJoinMember SolveSingleCardTargetJoin(CharacterHandler executor, BattleCommandData commandData, eSingleTargetPriorityWhenHateSame singleTargetPriorityWhenHateSame)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C25 RID: 3109 RVA: 0x00004D58 File Offset: 0x00002F58
		[Token(Token = "0x6000B4D")]
		[Address(RVA = "0x10110AB38", Offset = "0x110AB38", VA = "0x10110AB38")]
		private static bool GetIsValue(float inputAsBool)
		{
			return default(bool);
		}

		// Token: 0x06000C26 RID: 3110 RVA: 0x00004D70 File Offset: 0x00002F70
		[Token(Token = "0x6000B4E")]
		[Address(RVA = "0x10110AB44", Offset = "0x110AB44", VA = "0x10110AB44")]
		private static int CalcHitSingleConditionCount(CharacterHandler executor, CharacterHandler targetCharaHndl, List<BattleAICommandTargetSingleCondition> singleConditions)
		{
			return 0;
		}

		// Token: 0x06000C27 RID: 3111 RVA: 0x00004D88 File Offset: 0x00002F88
		[Token(Token = "0x6000B4F")]
		[Address(RVA = "0x10110B1D0", Offset = "0x110B1D0", VA = "0x10110B1D0")]
		private static bool Judge_Dying(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C28 RID: 3112 RVA: 0x00004DA0 File Offset: 0x00002FA0
		[Token(Token = "0x6000B50")]
		[Address(RVA = "0x10110B338", Offset = "0x110B338", VA = "0x10110B338")]
		private static bool Judge_Element(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C29 RID: 3113 RVA: 0x00004DB8 File Offset: 0x00002FB8
		[Token(Token = "0x6000B51")]
		[Address(RVA = "0x10110B3EC", Offset = "0x110B3EC", VA = "0x10110B3EC")]
		private static bool Judge_Class(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C2A RID: 3114 RVA: 0x00004DD0 File Offset: 0x00002FD0
		[Token(Token = "0x6000B52")]
		[Address(RVA = "0x10110B4A0", Offset = "0x110B4A0", VA = "0x10110B4A0")]
		private static bool Judge_StateAbnormal(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C2B RID: 3115 RVA: 0x00004DE8 File Offset: 0x00002FE8
		[Token(Token = "0x6000B53")]
		[Address(RVA = "0x10110B61C", Offset = "0x110B61C", VA = "0x10110B61C")]
		private static bool Judge_StatusChange(CharacterHandler charaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C2C RID: 3116 RVA: 0x00004E00 File Offset: 0x00003000
		[Token(Token = "0x6000B54")]
		[Address(RVA = "0x10110B688", Offset = "0x110B688", VA = "0x10110B688")]
		private static bool Judge_WeakElement(CharacterHandler executor, CharacterHandler targetCharaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C2D RID: 3117 RVA: 0x00004E18 File Offset: 0x00003018
		[Token(Token = "0x6000B55")]
		[Address(RVA = "0x10110B76C", Offset = "0x110B76C", VA = "0x10110B76C")]
		private static bool Judge_IsSelf(CharacterHandler executor, CharacterHandler targetCharaHndl, BattleAICommandTargetSingleCondition singleCondition)
		{
			return default(bool);
		}

		// Token: 0x06000C2E RID: 3118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B56")]
		[Address(RVA = "0x10110823C", Offset = "0x110823C", VA = "0x10110823C")]
		private static void RequestResetActionCount(BattlePartyData party, List<BattleAICommandCondition> conditions)
		{
		}

		// Token: 0x0200037F RID: 895
		[Token(Token = "0x2000D5F")]
		public enum eJudgeIsCmp
		{
			// Token: 0x04000DA6 RID: 3494
			[Token(Token = "0x400564A")]
			Equal,
			// Token: 0x04000DA7 RID: 3495
			[Token(Token = "0x400564B")]
			NotEqual
		}

		// Token: 0x02000380 RID: 896
		[Token(Token = "0x2000D60")]
		public enum eJudgeRangeCmp
		{
			// Token: 0x04000DA9 RID: 3497
			[Token(Token = "0x400564D")]
			GreaterEqual,
			// Token: 0x04000DAA RID: 3498
			[Token(Token = "0x400564E")]
			LessEqual,
			// Token: 0x04000DAB RID: 3499
			[Token(Token = "0x400564F")]
			Equal
		}

		// Token: 0x02000381 RID: 897
		[Token(Token = "0x2000D61")]
		private struct BattleAIHateResult
		{
			// Token: 0x04000DAC RID: 3500
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005650")]
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x04000DAD RID: 3501
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005651")]
			public float m_HateValue;

			// Token: 0x04000DAE RID: 3502
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005652")]
			public int m_HitSingleConditionCount;
		}
	}
}
