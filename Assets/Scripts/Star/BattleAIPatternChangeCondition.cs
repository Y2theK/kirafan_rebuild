﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000377 RID: 887
	[Token(Token = "0x20002ED")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAIPatternChangeCondition
	{
		// Token: 0x06000BFB RID: 3067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B23")]
		[Address(RVA = "0x10110B8F8", Offset = "0x110B8F8", VA = "0x10110B8F8")]
		public BattleAIPatternChangeCondition()
		{
		}

		// Token: 0x04000D93 RID: 3475
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000ACF")]
		public eBattleAIPatternChangeConditionType m_Type;

		// Token: 0x04000D94 RID: 3476
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AD0")]
		public float[] m_Args;
	}
}
