﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000376 RID: 886
	[Token(Token = "0x20002EC")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAIPatternChangeData
	{
		// Token: 0x06000BFA RID: 3066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B22")]
		[Address(RVA = "0x10110B900", Offset = "0x110B900", VA = "0x10110B900")]
		public BattleAIPatternChangeData()
		{
		}

		// Token: 0x04000D91 RID: 3473
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000ACD")]
		public List<BattleAIPatternChangeCondition> m_Conditions;

		// Token: 0x04000D92 RID: 3474
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000ACE")]
		public int m_ChangeToPatternIndex;
	}
}
