﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000378 RID: 888
	[Token(Token = "0x20002EE")]
	[Serializable]
	[StructLayout(3)]
	public class BattleAIPatternData
	{
		// Token: 0x06000BFC RID: 3068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B24")]
		[Address(RVA = "0x10110B970", Offset = "0x110B970", VA = "0x10110B970")]
		public BattleAIPatternData()
		{
		}

		// Token: 0x04000D95 RID: 3477
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000AD1")]
		public List<BattleAICommandData> m_CommandDatas;
	}
}
