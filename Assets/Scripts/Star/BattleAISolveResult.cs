﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000382 RID: 898
	[Token(Token = "0x20002F5")]
	[StructLayout(3)]
	public class BattleAISolveResult
	{
		// Token: 0x06000C2F RID: 3119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B57")]
		[Address(RVA = "0x1011074F0", Offset = "0x11074F0", VA = "0x1011074F0")]
		public BattleAISolveResult()
		{
		}

		// Token: 0x04000DAF RID: 3503
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000AE1")]
		public int m_CommandIndex;

		// Token: 0x04000DB0 RID: 3504
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000AE2")]
		public BattleDefine.eJoinMember m_TargetJoin;

		// Token: 0x04000DB1 RID: 3505
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AE3")]
		public bool m_IsChargeSkill;
	}
}
