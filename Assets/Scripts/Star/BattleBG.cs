﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000383 RID: 899
	[Token(Token = "0x20002F6")]
	[StructLayout(3)]
	public class BattleBG : MonoBehaviour
	{
		// Token: 0x06000C30 RID: 3120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B58")]
		[Address(RVA = "0x10110B9E0", Offset = "0x110B9E0", VA = "0x10110B9E0")]
		private void Awake()
		{
		}

		// Token: 0x06000C31 RID: 3121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B59")]
		[Address(RVA = "0x10110BC4C", Offset = "0x110BC4C", VA = "0x10110BC4C")]
		private void LateUpdate()
		{
		}

		// Token: 0x06000C32 RID: 3122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B5A")]
		[Address(RVA = "0x10110BCD8", Offset = "0x110BCD8", VA = "0x10110BCD8")]
		public void Play()
		{
		}

		// Token: 0x06000C33 RID: 3123 RVA: 0x00004E30 File Offset: 0x00003030
		[Token(Token = "0x6000B5B")]
		[Address(RVA = "0x10110C064", Offset = "0x110C064", VA = "0x10110C064")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06000C34 RID: 3124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B5C")]
		[Address(RVA = "0x10110C1A4", Offset = "0x110C1A4", VA = "0x10110C1A4")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06000C35 RID: 3125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B5D")]
		[Address(RVA = "0x10110C1B0", Offset = "0x110C1B0", VA = "0x10110C1B0")]
		public void EnableRender()
		{
		}

		// Token: 0x06000C36 RID: 3126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B5E")]
		[Address(RVA = "0x10110C1F4", Offset = "0x110C1F4", VA = "0x10110C1F4")]
		public void DisableRender()
		{
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B5F")]
		[Address(RVA = "0x10110C41C", Offset = "0x110C41C", VA = "0x10110C41C")]
		public void ChangeColor(Color startColor, Color endColor, float sec = 0.3f)
		{
		}

		// Token: 0x06000C38 RID: 3128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B60")]
		[Address(RVA = "0x10110C684", Offset = "0x110C684", VA = "0x10110C684")]
		public void ChangeColorFromCurrent(Color endColor, float sec = 0.3f)
		{
		}

		// Token: 0x06000C39 RID: 3129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B61")]
		[Address(RVA = "0x10110C6C0", Offset = "0x110C6C0", VA = "0x10110C6C0")]
		public void ResetColor()
		{
		}

		// Token: 0x06000C3A RID: 3130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B62")]
		[Address(RVA = "0x10110BC50", Offset = "0x110BC50", VA = "0x10110BC50")]
		private void UpdateColor()
		{
		}

		// Token: 0x06000C3B RID: 3131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B63")]
		[Address(RVA = "0x10110C47C", Offset = "0x110C47C", VA = "0x10110C47C")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B64")]
		[Address(RVA = "0x10110C6EC", Offset = "0x110C6EC", VA = "0x10110C6EC")]
		public BattleBG()
		{
		}

		// Token: 0x04000DB2 RID: 3506
		[Token(Token = "0x4000AE4")]
		private const string PLAY_KEY = "anim_{0}";

		// Token: 0x04000DB3 RID: 3507
		[Token(Token = "0x4000AE5")]
		private const string PLAY_KEY_FOR_ANIM = "BattleBG_{0}@anim_{1}";

		// Token: 0x04000DB4 RID: 3508
		[Token(Token = "0x4000AE6")]
		private const float CHANGE_COLOR_DEFAULT_TIME = 0.3f;

		// Token: 0x04000DB5 RID: 3509
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AE7")]
		public int m_ID;

		// Token: 0x04000DB6 RID: 3510
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000AE8")]
		public Transform m_Transform;

		// Token: 0x04000DB7 RID: 3511
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000AE9")]
		public Animation m_Anim;

		// Token: 0x04000DB8 RID: 3512
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000AEA")]
		public List<MeigeAnimCtrl> m_MeigeAnimCtrls;

		// Token: 0x04000DB9 RID: 3513
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000AEB")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x04000DBA RID: 3514
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000AEC")]
		public MsbHandler m_MsbHndl;

		// Token: 0x04000DBB RID: 3515
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000AED")]
		private Color m_StartColor;

		// Token: 0x04000DBC RID: 3516
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000AEE")]
		private Color m_EndColor;

		// Token: 0x04000DBD RID: 3517
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000AEF")]
		private Color m_CurrentColor;

		// Token: 0x04000DBE RID: 3518
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000AF0")]
		private float m_ColorTime;

		// Token: 0x04000DBF RID: 3519
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4000AF1")]
		private float m_ColorTimer;

		// Token: 0x04000DC0 RID: 3520
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000AF2")]
		private bool m_IsUpdateColor;
	}
}
