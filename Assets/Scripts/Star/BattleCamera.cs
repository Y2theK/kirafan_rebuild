﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000384 RID: 900
	[Token(Token = "0x20002F7")]
	[StructLayout(3)]
	public class BattleCamera : MonoBehaviour
	{
		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000C3D RID: 3133 RVA: 0x00004E48 File Offset: 0x00003048
		[Token(Token = "0x1700007C")]
		public bool IsMoving
		{
			[Token(Token = "0x6000B65")]
			[Address(RVA = "0x10110C728", Offset = "0x110C728", VA = "0x10110C728")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000C3E RID: 3134 RVA: 0x00004E60 File Offset: 0x00003060
		[Token(Token = "0x1700007D")]
		public bool IsZoomed
		{
			[Token(Token = "0x6000B66")]
			[Address(RVA = "0x10110C738", Offset = "0x110C738", VA = "0x10110C738")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06000C3F RID: 3135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B67")]
		[Address(RVA = "0x10110C740", Offset = "0x110C740", VA = "0x10110C740")]
		private void Awake()
		{
		}

		// Token: 0x06000C40 RID: 3136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B68")]
		[Address(RVA = "0x10110C854", Offset = "0x110C854", VA = "0x10110C854")]
		private void Update()
		{
		}

		// Token: 0x06000C41 RID: 3137 RVA: 0x00004E78 File Offset: 0x00003078
		[Token(Token = "0x6000B69")]
		[Address(RVA = "0x10110CA10", Offset = "0x110CA10", VA = "0x10110CA10")]
		private float easing(float currentTime, float startValue, float changeValue, float duration)
		{
			return 0f;
		}

		// Token: 0x06000C42 RID: 3138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6A")]
		[Address(RVA = "0x10110CAB4", Offset = "0x110CAB4", VA = "0x10110CAB4")]
		public void Pause()
		{
		}

		// Token: 0x06000C43 RID: 3139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6B")]
		[Address(RVA = "0x10110CAE8", Offset = "0x110CAE8", VA = "0x10110CAE8")]
		public void Resume()
		{
		}

		// Token: 0x06000C44 RID: 3140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6C")]
		[Address(RVA = "0x10110CAEC", Offset = "0x110CAEC", VA = "0x10110CAEC")]
		public void ResetDefault()
		{
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6D")]
		[Address(RVA = "0x10110CC50", Offset = "0x110CC50", VA = "0x10110CC50")]
		public void SetZoom(BattleCamera.eZoomType zoomType)
		{
		}

		// Token: 0x06000C46 RID: 3142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6E")]
		[Address(RVA = "0x10110CD54", Offset = "0x110CD54", VA = "0x10110CD54")]
		public void ToDefault(float zoomTime = 0f)
		{
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B6F")]
		[Address(RVA = "0x10110CE48", Offset = "0x110CE48", VA = "0x10110CE48")]
		public void ZoomStart(BattleCamera.eZoomType zoomType)
		{
		}

		// Token: 0x06000C48 RID: 3144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B70")]
		[Address(RVA = "0x10110CDC8", Offset = "0x110CDC8", VA = "0x10110CDC8")]
		private void ZoomStart(Vector3 targetPos, float targeOrthographicSize, float time)
		{
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B71")]
		[Address(RVA = "0x10110C7FC", Offset = "0x110C7FC", VA = "0x10110C7FC")]
		private void ApplyDefaultOrthographic()
		{
		}

		// Token: 0x06000C4A RID: 3146 RVA: 0x00004E90 File Offset: 0x00003090
		[Token(Token = "0x6000B72")]
		[Address(RVA = "0x10110CDB8", Offset = "0x110CDB8", VA = "0x10110CDB8")]
		private float CalcDefaultOrthographic()
		{
			return 0f;
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B73")]
		[Address(RVA = "0x10110CF40", Offset = "0x110CF40", VA = "0x10110CF40")]
		public void Shake(BattleCamera.eShakeType shakeType)
		{
		}

		// Token: 0x06000C4C RID: 3148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B74")]
		[Address(RVA = "0x10110DBF4", Offset = "0x110DBF4", VA = "0x10110DBF4")]
		private void OnShakeComplete(Vector3 pos)
		{
		}

		// Token: 0x06000C4D RID: 3149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B75")]
		[Address(RVA = "0x10110DC5C", Offset = "0x110DC5C", VA = "0x10110DC5C")]
		public void SetEnablePostProcess(bool flg)
		{
		}

		// Token: 0x06000C4E RID: 3150 RVA: 0x00004EA8 File Offset: 0x000030A8
		[Token(Token = "0x6000B76")]
		[Address(RVA = "0x10110DD0C", Offset = "0x110DD0C", VA = "0x10110DD0C")]
		public bool IsEnablePostProcess()
		{
			return default(bool);
		}

		// Token: 0x06000C4F RID: 3151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B77")]
		[Address(RVA = "0x10110DDA8", Offset = "0x110DDA8", VA = "0x10110DDA8")]
		public BattleCamera()
		{
		}

		// Token: 0x04000DC1 RID: 3521
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000AF3")]
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x04000DC2 RID: 3522
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000AF4")]
		[SerializeField]
		private PostProcessRenderer m_PostProcess;

		// Token: 0x04000DC3 RID: 3523
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000AF5")]
		[SerializeField]
		private float m_BaseWidth;

		// Token: 0x04000DC4 RID: 3524
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000AF6")]
		[SerializeField]
		private float m_BaseHeight;

		// Token: 0x04000DC5 RID: 3525
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000AF7")]
		[SerializeField]
		private float m_BasePixelPerUnit;

		// Token: 0x04000DC6 RID: 3526
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000AF8")]
		[SerializeField]
		private BattleCamera.ZoomParam[] m_ZoomParams;

		// Token: 0x04000DC7 RID: 3527
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000AF9")]
		[SerializeField]
		private float m_ZoomToDefaultTime;

		// Token: 0x04000DC8 RID: 3528
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000AFA")]
		[SerializeField]
		private BattleCamera.ShakeParam[] m_ShakeParams;

		// Token: 0x04000DC9 RID: 3529
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000AFB")]
		private Camera m_Camera;

		// Token: 0x04000DCA RID: 3530
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000AFC")]
		private Vector3 m_DefaultPos;

		// Token: 0x04000DCB RID: 3531
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4000AFD")]
		private float m_DefaultNear;

		// Token: 0x04000DCC RID: 3532
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000AFE")]
		private float m_DefaultFar;

		// Token: 0x04000DCD RID: 3533
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000AFF")]
		private float m_DefaultOrthographicSize;

		// Token: 0x04000DCE RID: 3534
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000B00")]
		private BattleCamera.eMode m_Mode;

		// Token: 0x04000DCF RID: 3535
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4000B01")]
		private Vector3 m_StartPos;

		// Token: 0x04000DD0 RID: 3536
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000B02")]
		private Vector3 m_TargetPos;

		// Token: 0x04000DD1 RID: 3537
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4000B03")]
		private float m_StartOrthographicSize;

		// Token: 0x04000DD2 RID: 3538
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000B04")]
		private float m_TargetOrthographicSize;

		// Token: 0x04000DD3 RID: 3539
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4000B05")]
		private float m_TargetTime;

		// Token: 0x04000DD4 RID: 3540
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000B06")]
		private float m_t;

		// Token: 0x04000DD5 RID: 3541
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4000B07")]
		private bool m_IsToDefault;

		// Token: 0x04000DD6 RID: 3542
		[Cpp2IlInjected.FieldOffset(Offset = "0x9D")]
		[Token(Token = "0x4000B08")]
		private bool m_IsZoomed;

		// Token: 0x02000385 RID: 901
		[Token(Token = "0x2000D62")]
		public enum eZoomType
		{
			// Token: 0x04000DD8 RID: 3544
			[Token(Token = "0x4005654")]
			ToPlayer,
			// Token: 0x04000DD9 RID: 3545
			[Token(Token = "0x4005655")]
			ToEnemy,
			// Token: 0x04000DDA RID: 3546
			[Token(Token = "0x4005656")]
			ToPlayerOnUniqueSkillResult,
			// Token: 0x04000DDB RID: 3547
			[Token(Token = "0x4005657")]
			ToEnemyOnUniqueSkillResult,
			// Token: 0x04000DDC RID: 3548
			[Token(Token = "0x4005658")]
			ToPlayerOnBattleEnd,
			// Token: 0x04000DDD RID: 3549
			[Token(Token = "0x4005659")]
			ToEnemyOnBattleEnd,
			// Token: 0x04000DDE RID: 3550
			[Token(Token = "0x400565A")]
			ToPlayerOnStun,
			// Token: 0x04000DDF RID: 3551
			[Token(Token = "0x400565B")]
			ToEnemyOnStun,
			// Token: 0x04000DE0 RID: 3552
			[Token(Token = "0x400565C")]
			QuestStart,
			// Token: 0x04000DE1 RID: 3553
			[Token(Token = "0x400565D")]
			WaveOut,
			// Token: 0x04000DE2 RID: 3554
			[Token(Token = "0x400565E")]
			Num
		}

		// Token: 0x02000386 RID: 902
		[Token(Token = "0x2000D63")]
		[Serializable]
		public class ZoomParam
		{
			// Token: 0x06000C50 RID: 3152 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D9C")]
			[Address(RVA = "0x10110DE64", Offset = "0x110DE64", VA = "0x10110DE64")]
			public ZoomParam()
			{
			}

			// Token: 0x04000DE3 RID: 3555
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400565F")]
			public Vector3 m_TargetPos;

			// Token: 0x04000DE4 RID: 3556
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005660")]
			public float m_TargetOrthographicSize;

			// Token: 0x04000DE5 RID: 3557
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005661")]
			public float m_TargetTime;
		}

		// Token: 0x02000387 RID: 903
		[Token(Token = "0x2000D64")]
		public enum eShakeType
		{
			// Token: 0x04000DE7 RID: 3559
			[Token(Token = "0x4005663")]
			LastBlow,
			// Token: 0x04000DE8 RID: 3560
			[Token(Token = "0x4005664")]
			Hit_Regist,
			// Token: 0x04000DE9 RID: 3561
			[Token(Token = "0x4005665")]
			Hit_Default,
			// Token: 0x04000DEA RID: 3562
			[Token(Token = "0x4005666")]
			Hit_Weak,
			// Token: 0x04000DEB RID: 3563
			[Token(Token = "0x4005667")]
			Stun
		}

		// Token: 0x02000388 RID: 904
		[Token(Token = "0x2000D65")]
		[Serializable]
		public class ShakeParam
		{
			// Token: 0x06000C51 RID: 3153 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D9D")]
			[Address(RVA = "0x10110DE5C", Offset = "0x110DE5C", VA = "0x10110DE5C")]
			public ShakeParam()
			{
			}

			// Token: 0x04000DEC RID: 3564
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005668")]
			public Vector3 m_Shake;

			// Token: 0x04000DED RID: 3565
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005669")]
			public iTween.EaseType m_EaseType;
		}

		// Token: 0x02000389 RID: 905
		[Token(Token = "0x2000D66")]
		public enum eMode
		{
			// Token: 0x04000DEF RID: 3567
			[Token(Token = "0x400566B")]
			Default,
			// Token: 0x04000DF0 RID: 3568
			[Token(Token = "0x400566C")]
			Zoom
		}
	}
}
