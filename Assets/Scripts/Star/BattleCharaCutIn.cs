﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200038A RID: 906
	[Token(Token = "0x20002F8")]
	[StructLayout(3)]
	public class BattleCharaCutIn : MonoBehaviour
	{
		// Token: 0x06000C52 RID: 3154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B78")]
		[Address(RVA = "0x10110DE6C", Offset = "0x110DE6C", VA = "0x10110DE6C")]
		private void Start()
		{
		}

		// Token: 0x06000C53 RID: 3155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B79")]
		[Address(RVA = "0x10110DEC4", Offset = "0x110DEC4", VA = "0x10110DEC4")]
		private void Update()
		{
		}

		// Token: 0x06000C54 RID: 3156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B7A")]
		[Address(RVA = "0x10110EA00", Offset = "0x110EA00", VA = "0x10110EA00")]
		public void Play(int viewCharaID, eElementType elementType)
		{
		}

		// Token: 0x06000C55 RID: 3157 RVA: 0x00004EC0 File Offset: 0x000030C0
		[Token(Token = "0x6000B7B")]
		[Address(RVA = "0x10110EA44", Offset = "0x110EA44", VA = "0x10110EA44")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06000C56 RID: 3158 RVA: 0x00004ED8 File Offset: 0x000030D8
		[Token(Token = "0x6000B7C")]
		[Address(RVA = "0x10110EA54", Offset = "0x110EA54", VA = "0x10110EA54")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000C57 RID: 3159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B7D")]
		[Address(RVA = "0x10110EA64", Offset = "0x110EA64", VA = "0x10110EA64")]
		public void Destroy()
		{
		}

		// Token: 0x06000C58 RID: 3160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B7E")]
		[Address(RVA = "0x10110E6B4", Offset = "0x110E6B4", VA = "0x10110E6B4")]
		private void SetStep(BattleCharaCutIn.eStep step)
		{
		}

		// Token: 0x06000C59 RID: 3161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B7F")]
		[Address(RVA = "0x10110E6BC", Offset = "0x110E6BC", VA = "0x10110E6BC")]
		private void ApplyCharaIllustTexture()
		{
		}

		// Token: 0x06000C5A RID: 3162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B80")]
		[Address(RVA = "0x10110E810", Offset = "0x110E810", VA = "0x10110E810")]
		private void ApplyElement()
		{
		}

		// Token: 0x06000C5B RID: 3163 RVA: 0x00004EF0 File Offset: 0x000030F0
		[Token(Token = "0x6000B81")]
		[Address(RVA = "0x10110E9C8", Offset = "0x110E9C8", VA = "0x10110E9C8")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000C5C RID: 3164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B82")]
		[Address(RVA = "0x10110EB24", Offset = "0x110EB24", VA = "0x10110EB24")]
		public BattleCharaCutIn()
		{
		}

		// Token: 0x04000DF1 RID: 3569
		[Token(Token = "0x4000B09")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04000DF2 RID: 3570
		[Token(Token = "0x4000B0A")]
		private static readonly string[] OBJ_CHARA_ILLUSTS;

		// Token: 0x04000DF3 RID: 3571
		[Token(Token = "0x4000B0B")]
		private static readonly string[] OBJ_ELEMENTS;

		// Token: 0x04000DF4 RID: 3572
		[Token(Token = "0x4000B0C")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000DF5 RID: 3573
		[Token(Token = "0x4000B0D")]
		private const string RESOURCE_PATH = "battle/characutin/characutin.muast";

		// Token: 0x04000DF6 RID: 3574
		[Token(Token = "0x4000B0E")]
		private const string OBJ_MODEL_PATH = "CharaCutIn";

		// Token: 0x04000DF7 RID: 3575
		[Token(Token = "0x4000B0F")]
		private const string OBJ_ANIM_PATH = "MeigeAC_CharaCutIn@Take 001";

		// Token: 0x04000DF8 RID: 3576
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000B10")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04000DF9 RID: 3577
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000B11")]
		[SerializeField]
		private AdjustOrthographicCamera m_AdjustOrthographicCamera;

		// Token: 0x04000DFA RID: 3578
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000B12")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000DFB RID: 3579
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000B13")]
		private BattleCharaCutIn.eStep m_Step;

		// Token: 0x04000DFC RID: 3580
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000B14")]
		private bool m_IsDoneFirstPrepare;

		// Token: 0x04000DFD RID: 3581
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000B15")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000DFE RID: 3582
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000B16")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000DFF RID: 3583
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000B17")]
		private SpriteHandler m_SpriteHndl;

		// Token: 0x04000E00 RID: 3584
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000B18")]
		private Transform m_BaseTransform;

		// Token: 0x04000E01 RID: 3585
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000B19")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04000E02 RID: 3586
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000B1A")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x04000E03 RID: 3587
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000B1B")]
		private GameObject m_BodyObj;

		// Token: 0x04000E04 RID: 3588
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000B1C")]
		private Texture m_Texture;

		// Token: 0x04000E05 RID: 3589
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000B1D")]
		private int m_ViewCharaID;

		// Token: 0x04000E06 RID: 3590
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4000B1E")]
		private eElementType m_ElementType;

		// Token: 0x0200038B RID: 907
		[Token(Token = "0x2000D67")]
		private enum eStep
		{
			// Token: 0x04000E08 RID: 3592
			[Token(Token = "0x400566E")]
			None = -1,
			// Token: 0x04000E09 RID: 3593
			[Token(Token = "0x400566F")]
			Prepare,
			// Token: 0x04000E0A RID: 3594
			[Token(Token = "0x4005670")]
			Prepare_Wait,
			// Token: 0x04000E0B RID: 3595
			[Token(Token = "0x4005671")]
			Prepare_Error,
			// Token: 0x04000E0C RID: 3596
			[Token(Token = "0x4005672")]
			Play,
			// Token: 0x04000E0D RID: 3597
			[Token(Token = "0x4005673")]
			Play_Wait
		}
	}
}
