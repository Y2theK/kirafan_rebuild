﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200038F RID: 911
	[Token(Token = "0x20002FA")]
	[Serializable]
	[StructLayout(3)]
	public class BattleCommandData
	{
		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000CBC RID: 3260 RVA: 0x000052F8 File Offset: 0x000034F8
		[Token(Token = "0x1700007E")]
		public BattleCommandData.eCommandType CommandType
		{
			[Token(Token = "0x6000BE2")]
			[Address(RVA = "0x10110EEF4", Offset = "0x110EEF4", VA = "0x10110EEF4")]
			get
			{
				return BattleCommandData.eCommandType.NormalAttack;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000CBD RID: 3261 RVA: 0x00005310 File Offset: 0x00003510
		[Token(Token = "0x1700007F")]
		public int CommandIndex
		{
			[Token(Token = "0x6000BE3")]
			[Address(RVA = "0x10110EEFC", Offset = "0x110EEFC", VA = "0x10110EEFC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000CBE RID: 3262 RVA: 0x00005328 File Offset: 0x00003528
		[Token(Token = "0x17000080")]
		public int SkillID
		{
			[Token(Token = "0x6000BE4")]
			[Address(RVA = "0x10110EF04", Offset = "0x110EF04", VA = "0x10110EF04")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000CBF RID: 3263 RVA: 0x00005340 File Offset: 0x00003540
		[Token(Token = "0x17000081")]
		public int SkillLv
		{
			[Token(Token = "0x6000BE5")]
			[Address(RVA = "0x10110EF0C", Offset = "0x110EF0C", VA = "0x10110EF0C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000CC0 RID: 3264 RVA: 0x00005358 File Offset: 0x00003558
		[Token(Token = "0x17000082")]
		public SkillListDB_Param SkillParam
		{
			[Token(Token = "0x6000BE6")]
			[Address(RVA = "0x10110EF14", Offset = "0x110EF14", VA = "0x10110EF14")]
			get
			{
				return default(SkillListDB_Param);
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000CC1 RID: 3265 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000083")]
		public List<BattleCommandData.SkillContentSet> SkillContentSets
		{
			[Token(Token = "0x6000BE7")]
			[Address(RVA = "0x10110EF48", Offset = "0x110EF48", VA = "0x10110EF48")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000CC2 RID: 3266 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000084")]
		public string UniqueSkillScene
		{
			[Token(Token = "0x6000BE8")]
			[Address(RVA = "0x10110EF50", Offset = "0x110EF50", VA = "0x10110EF50")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000CC3 RID: 3267 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000085")]
		public string UniqueSkillVoiceCueSheet
		{
			[Token(Token = "0x6000BE9")]
			[Address(RVA = "0x10110EF88", Offset = "0x110EF88", VA = "0x10110EF88")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000CC4 RID: 3268 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000086")]
		public string UniqueSkillSeCueSheet
		{
			[Token(Token = "0x6000BEA")]
			[Address(RVA = "0x10110EF90", Offset = "0x110EF90", VA = "0x10110EF90")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000CC5 RID: 3269 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000087")]
		public string UniqueSkillBgmCueName
		{
			[Token(Token = "0x6000BEB")]
			[Address(RVA = "0x10110EF98", Offset = "0x110EF98", VA = "0x10110EF98")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000CC6 RID: 3270 RVA: 0x00005370 File Offset: 0x00003570
		[Token(Token = "0x17000088")]
		public bool IsExistUniqueSkillScene
		{
			[Token(Token = "0x6000BEC")]
			[Address(RVA = "0x10110EFA0", Offset = "0x110EFA0", VA = "0x10110EFA0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000CC7 RID: 3271 RVA: 0x00005388 File Offset: 0x00003588
		[Token(Token = "0x17000089")]
		public bool IsCharaCutIn
		{
			[Token(Token = "0x6000BED")]
			[Address(RVA = "0x10110EFE4", Offset = "0x110EFE4", VA = "0x10110EFE4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000CC8 RID: 3272 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700008A")]
		public string SAP_ID
		{
			[Token(Token = "0x6000BEE")]
			[Address(RVA = "0x10110EFEC", Offset = "0x110EFEC", VA = "0x10110EFEC")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000CC9 RID: 3273 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700008B")]
		public string SAG_ID
		{
			[Token(Token = "0x6000BEF")]
			[Address(RVA = "0x10110EFF4", Offset = "0x110EFF4", VA = "0x10110EFF4")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000CCA RID: 3274 RVA: 0x000053A0 File Offset: 0x000035A0
		[Token(Token = "0x1700008C")]
		public eSkillTargetType MainSkillTargetType
		{
			[Token(Token = "0x6000BF0")]
			[Address(RVA = "0x101108BFC", Offset = "0x1108BFC", VA = "0x101108BFC")]
			get
			{
				return eSkillTargetType.Self;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000CCB RID: 3275 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700008D")]
		public string ActionKey
		{
			[Token(Token = "0x6000BF1")]
			[Address(RVA = "0x10110EFFC", Offset = "0x110EFFC", VA = "0x10110EFFC")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000CCC RID: 3276 RVA: 0x000053B8 File Offset: 0x000035B8
		[Token(Token = "0x1700008E")]
		public float Ratio
		{
			[Token(Token = "0x6000BF2")]
			[Address(RVA = "0x10110F004", Offset = "0x110F004", VA = "0x10110F004")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000CCD RID: 3277 RVA: 0x000053D0 File Offset: 0x000035D0
		[Token(Token = "0x1700008F")]
		public int RecastMax
		{
			[Token(Token = "0x6000BF3")]
			[Address(RVA = "0x10110F034", Offset = "0x110F034", VA = "0x10110F034")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000CCE RID: 3278 RVA: 0x000053E8 File Offset: 0x000035E8
		[Token(Token = "0x17000090")]
		public bool RecastRecoveredFlg
		{
			[Token(Token = "0x6000BF4")]
			[Address(RVA = "0x10110F03C", Offset = "0x110F03C", VA = "0x10110F03C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000CCF RID: 3279 RVA: 0x00005400 File Offset: 0x00003600
		[Token(Token = "0x17000091")]
		public float LoadFactor
		{
			[Token(Token = "0x6000BF5")]
			[Address(RVA = "0x10110F044", Offset = "0x110F044", VA = "0x10110F044")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000CD0 RID: 3280 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000092")]
		public CharacterHandler Owner
		{
			[Token(Token = "0x6000BF6")]
			[Address(RVA = "0x10110F04C", Offset = "0x110F04C", VA = "0x10110F04C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000CD1 RID: 3281 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000093")]
		public BattleCommandSolveResult SolveResult
		{
			[Token(Token = "0x6000BF7")]
			[Address(RVA = "0x10110F054", Offset = "0x110F054", VA = "0x10110F054")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000CD2 RID: 3282 RVA: 0x00005418 File Offset: 0x00003618
		[Token(Token = "0x17000094")]
		public bool CanUseOnConfusion
		{
			[Token(Token = "0x6000BF8")]
			[Address(RVA = "0x10110F05C", Offset = "0x110F05C", VA = "0x10110F05C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06000CD3 RID: 3283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BF9")]
		[Address(RVA = "0x10110F064", Offset = "0x110F064", VA = "0x10110F064")]
		public BattleCommandData()
		{
		}

		// Token: 0x06000CD4 RID: 3284 RVA: 0x00005430 File Offset: 0x00003630
		[Token(Token = "0x6000BFA")]
		[Address(RVA = "0x10110F0F0", Offset = "0x110F0F0", VA = "0x10110F0F0")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06000CD5 RID: 3285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BFB")]
		[Address(RVA = "0x10110F118", Offset = "0x110F118", VA = "0x10110F118")]
		public void Setup(BattleCommandData.eCommandType type, int commandIndex, CharacterHandler owner, int skillID, int skillLv, int skillMaxLv, long totalUseNum, sbyte expTableID, bool canUseOnConfusion, bool isCharaCutIn)
		{
		}

		// Token: 0x06000CD6 RID: 3286 RVA: 0x00005448 File Offset: 0x00003648
		[Token(Token = "0x6000BFC")]
		[Address(RVA = "0x10110FD64", Offset = "0x110FD64", VA = "0x10110FD64")]
		private bool UpdateSkillLvFromCurrentParam(bool isUpdateSkillLv, bool isUpdateParamAssociatedWithSkillLv)
		{
			return default(bool);
		}

		// Token: 0x06000CD7 RID: 3287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BFD")]
		[Address(RVA = "0x10110FF14", Offset = "0x110FF14", VA = "0x10110FF14")]
		public void Clear()
		{
		}

		// Token: 0x06000CD8 RID: 3288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BFE")]
		[Address(RVA = "0x10110FF7C", Offset = "0x110FF7C", VA = "0x10110FF7C")]
		public void SetOwner(CharacterHandler owner)
		{
		}

		// Token: 0x06000CD9 RID: 3289 RVA: 0x00005460 File Offset: 0x00003660
		[Token(Token = "0x6000BFF")]
		[Address(RVA = "0x10110FF84", Offset = "0x110FF84", VA = "0x10110FF84")]
		public bool IsCardTargetSame()
		{
			return default(bool);
		}

		// Token: 0x06000CDA RID: 3290 RVA: 0x00005478 File Offset: 0x00003678
		[Token(Token = "0x6000C00")]
		[Address(RVA = "0x101110184", Offset = "0x1110184", VA = "0x101110184")]
		public bool IsExistSkillContentType(eSkillContentType skillContentType)
		{
			return default(bool);
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C01")]
		[Address(RVA = "0x10111026C", Offset = "0x111026C", VA = "0x10111026C")]
		public List<eElementType> GetAttackSkillElementTypes()
		{
			return null;
		}

		// Token: 0x06000CDC RID: 3292 RVA: 0x00005490 File Offset: 0x00003690
		[Token(Token = "0x6000C02")]
		[Address(RVA = "0x101110624", Offset = "0x1110624", VA = "0x101110624")]
		public bool IncrementSkillUseNum()
		{
			return default(bool);
		}

		// Token: 0x06000CDD RID: 3293 RVA: 0x000054A8 File Offset: 0x000036A8
		[Token(Token = "0x6000C03")]
		[Address(RVA = "0x101110668", Offset = "0x1110668", VA = "0x101110668")]
		public int CalcRecast(int val)
		{
			return 0;
		}

		// Token: 0x06000CDE RID: 3294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C04")]
		[Address(RVA = "0x101110704", Offset = "0x1110704", VA = "0x101110704")]
		public void ResetRecastRecoverdFlg()
		{
		}

		// Token: 0x06000CDF RID: 3295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C05")]
		[Address(RVA = "0x10111070C", Offset = "0x111070C", VA = "0x10111070C")]
		public void RecastZero(bool recastRecoveredFlg = false)
		{
		}

		// Token: 0x06000CE0 RID: 3296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C06")]
		[Address(RVA = "0x101110718", Offset = "0x1110718", VA = "0x101110718")]
		public void RecastFull()
		{
		}

		// Token: 0x04000E34 RID: 3636
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000B20")]
		[NonSerialized]
		private BattleCommandData.eCommandType m_CommandType;

		// Token: 0x04000E35 RID: 3637
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000B21")]
		[NonSerialized]
		private int m_CommandIndex;

		// Token: 0x04000E36 RID: 3638
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000B22")]
		[NonSerialized]
		private int m_SkillID;

		// Token: 0x04000E37 RID: 3639
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000B23")]
		[NonSerialized]
		private int m_SkillLv;

		// Token: 0x04000E38 RID: 3640
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000B24")]
		[NonSerialized]
		private int m_SkillMaxLv;

		// Token: 0x04000E39 RID: 3641
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000B25")]
		[NonSerialized]
		private long m_TotalUseNum;

		// Token: 0x04000E3A RID: 3642
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000B26")]
		[SerializeField]
		public int UseNum;

		// Token: 0x04000E3B RID: 3643
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000B27")]
		[NonSerialized]
		private sbyte m_ExpTableID;

		// Token: 0x04000E3C RID: 3644
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000B28")]
		[NonSerialized]
		private SkillListDB_Param m_SkillParam;

		// Token: 0x04000E3D RID: 3645
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000B29")]
		[NonSerialized]
		private string m_UniqueSkillScene;

		// Token: 0x04000E3E RID: 3646
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000B2A")]
		[NonSerialized]
		private string m_UniqueSkillVoiceCueSheet;

		// Token: 0x04000E3F RID: 3647
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000B2B")]
		[NonSerialized]
		private string m_UniqueSkillSeCueSheet;

		// Token: 0x04000E40 RID: 3648
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000B2C")]
		[NonSerialized]
		private string m_UniqueSkillBgmCueName;

		// Token: 0x04000E41 RID: 3649
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000B2D")]
		[NonSerialized]
		private string m_SapID;

		// Token: 0x04000E42 RID: 3650
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000B2E")]
		[NonSerialized]
		private string m_SagID;

		// Token: 0x04000E43 RID: 3651
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4000B2F")]
		[NonSerialized]
		private bool m_CanUseOnConfusion;

		// Token: 0x04000E44 RID: 3652
		[Cpp2IlInjected.FieldOffset(Offset = "0xD1")]
		[Token(Token = "0x4000B30")]
		[NonSerialized]
		public bool m_IsCharaCutIn;

		// Token: 0x04000E45 RID: 3653
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4000B31")]
		[NonSerialized]
		private List<BattleCommandData.SkillContentSet> m_SkillContentSets;

		// Token: 0x04000E46 RID: 3654
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4000B32")]
		[NonSerialized]
		private eSkillTargetType m_MainSkillTargetType;

		// Token: 0x04000E47 RID: 3655
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4000B33")]
		[NonSerialized]
		private string m_ActKey;

		// Token: 0x04000E48 RID: 3656
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4000B34")]
		[SerializeField]
		public int RecastVal;

		// Token: 0x04000E49 RID: 3657
		[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
		[Token(Token = "0x4000B35")]
		[NonSerialized]
		private int m_RecastMax;

		// Token: 0x04000E4A RID: 3658
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4000B36")]
		[NonSerialized]
		private bool m_RecastRecoveredFlg;

		// Token: 0x04000E4B RID: 3659
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x4000B37")]
		[NonSerialized]
		private float m_LoadFactor;

		// Token: 0x04000E4C RID: 3660
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4000B38")]
		[NonSerialized]
		private CharacterHandler m_Owner;

		// Token: 0x04000E4D RID: 3661
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4000B39")]
		[NonSerialized]
		private BattleCommandSolveResult m_SolveResult;

		// Token: 0x04000E4E RID: 3662
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4000B3A")]
		[NonSerialized]
		public CharacterHandler solvedSingleCardTarget;

		// Token: 0x02000390 RID: 912
		[Token(Token = "0x2000D6A")]
		public enum eCommandType
		{
			// Token: 0x04000E50 RID: 3664
			[Token(Token = "0x400569A")]
			NormalAttack,
			// Token: 0x04000E51 RID: 3665
			[Token(Token = "0x400569B")]
			Guard,
			// Token: 0x04000E52 RID: 3666
			[Token(Token = "0x400569C")]
			Skill,
			// Token: 0x04000E53 RID: 3667
			[Token(Token = "0x400569D")]
			WeaponSkill,
			// Token: 0x04000E54 RID: 3668
			[Token(Token = "0x400569E")]
			UniqueSkill,
			// Token: 0x04000E55 RID: 3669
			[Token(Token = "0x400569F")]
			MasterSkill,
			// Token: 0x04000E56 RID: 3670
			[Token(Token = "0x40056A0")]
			Card,
			// Token: 0x04000E57 RID: 3671
			[Token(Token = "0x40056A1")]
			Num
		}

		// Token: 0x02000391 RID: 913
		[Token(Token = "0x2000D6B")]
		public class SkillContentSet
		{
			// Token: 0x06000CE1 RID: 3297 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005D9E")]
			[Address(RVA = "0x10110FECC", Offset = "0x110FECC", VA = "0x10110FECC")]
			public SkillContentSet(eSkillTargetType target, SkillContentListDB_Data data)
			{
			}

			// Token: 0x04000E58 RID: 3672
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40056A2")]
			public eSkillTargetType m_Target;

			// Token: 0x04000E59 RID: 3673
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40056A3")]
			public SkillContentListDB_Data m_Data;
		}
	}
}
