﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200038C RID: 908
	[Token(Token = "0x20002F9")]
	[StructLayout(3)]
	public static class BattleCommandParser
	{
		// Token: 0x06000C5E RID: 3166 RVA: 0x00004F08 File Offset: 0x00003108
		[Token(Token = "0x6000B84")]
		[Address(RVA = "0x101110728", Offset = "0x1110728", VA = "0x101110728")]
		private static BattleCommandParser.eAutoSkillTypeOrdinal GetAISkillOrdinal(eSkillContentType sct, int power, BattleDefine.eStatus buffCategory = BattleDefine.eStatus.Num)
		{
			return BattleCommandParser.eAutoSkillTypeOrdinal.Recover;
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x00004F20 File Offset: 0x00003120
		[Token(Token = "0x6000B85")]
		[Address(RVA = "0x1011107F8", Offset = "0x11107F8", VA = "0x1011107F8")]
		private static float GetAISkillContentPower(BattleCommandData.SkillContentSet contentSet, out int targetIndex)
		{
			return 0f;
		}

		// Token: 0x06000C60 RID: 3168 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B86")]
		[Address(RVA = "0x101110AF4", Offset = "0x1110AF4", VA = "0x101110AF4")]
		private static BattleCommandData.SkillContentSet GetSkillAutoTargetType(BattleCommandData bcd, out int skillPower, out int skillIndex)
		{
			return null;
		}

		// Token: 0x06000C61 RID: 3169 RVA: 0x00004F38 File Offset: 0x00003138
		[Token(Token = "0x6000B87")]
		[Address(RVA = "0x101110DC0", Offset = "0x1110DC0", VA = "0x101110DC0")]
		private static BattleDefine.eJoinMember GetBuffTarget(BattleCommandData.SkillContentSet scs, bool isPlayerExecute, List<CharacterHandler> chl)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C62 RID: 3170 RVA: 0x00004F50 File Offset: 0x00003150
		[Token(Token = "0x6000B88")]
		[Address(RVA = "0x1011112E4", Offset = "0x11112E4", VA = "0x1011112E4")]
		private static BattleDefine.eJoinMember GetElementTarget(BattleCommandData.SkillContentSet scs, List<CharacterHandler> chl)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x00004F68 File Offset: 0x00003168
		[Token(Token = "0x6000B89")]
		[Address(RVA = "0x1011119E0", Offset = "0x11119E0", VA = "0x1011119E0")]
		private static BattleDefine.eJoinMember GetHateTarget(BattleCommandData.SkillContentSet scs, List<CharacterHandler> chl)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000C64 RID: 3172 RVA: 0x00004F80 File Offset: 0x00003180
		[Token(Token = "0x6000B8A")]
		[Address(RVA = "0x101111E3C", Offset = "0x1111E3C", VA = "0x101111E3C")]
		private static int GetOrderFrameIndex(CharacterHandler charaHndl)
		{
			return 0;
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x00004F98 File Offset: 0x00003198
		[Token(Token = "0x6000B8B")]
		[Address(RVA = "0x101112014", Offset = "0x1112014", VA = "0x101112014")]
		private static bool IsSelfTargetSkill_OnOthers(CharacterBattle other, CharacterHandler myself, eSkillTargetType skillRange)
		{
			return default(bool);
		}

		// Token: 0x06000C66 RID: 3174 RVA: 0x00004FB0 File Offset: 0x000031B0
		[Token(Token = "0x6000B8C")]
		[Address(RVA = "0x101112098", Offset = "0x1112098", VA = "0x101112098")]
		public static int SolveAutoCommand(CharacterHandler charaHndl)
		{
			return 0;
		}

		// Token: 0x06000C67 RID: 3175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B8D")]
		[Address(RVA = "0x101114028", Offset = "0x1114028", VA = "0x101114028")]
		public static void OptimizeTargetIndex(CharacterHandler charaHndl, eSkillTargetType targetType)
		{
		}

		// Token: 0x06000C68 RID: 3176 RVA: 0x00004FC8 File Offset: 0x000031C8
		[Token(Token = "0x6000B8E")]
		[Address(RVA = "0x1011142EC", Offset = "0x11142EC", VA = "0x1011142EC")]
		public static int UpdateOrder(BattleOrder battleOrder, int commandIndex, bool isUseUniqueSkill, bool isRealMove)
		{
			return 0;
		}

		// Token: 0x06000C69 RID: 3177 RVA: 0x00004FE0 File Offset: 0x000031E0
		[Token(Token = "0x6000B8F")]
		[Address(RVA = "0x1011147D4", Offset = "0x11147D4", VA = "0x1011147D4")]
		public static int GetMoveToOrder(BattleOrder battleOrder, int commandIndex, bool isUseUniqueSkill)
		{
			return 0;
		}

		// Token: 0x06000C6A RID: 3178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B90")]
		[Address(RVA = "0x1011147DC", Offset = "0x11147DC", VA = "0x1011147DC")]
		public static void UpdateOrderAfterTogetherAttack(BattleOrder battleOrder, List<CharacterHandler> togetherAttackedCharaHndls)
		{
		}

		// Token: 0x06000C6B RID: 3179 RVA: 0x00004FF8 File Offset: 0x000031F8
		[Token(Token = "0x6000B91")]
		[Address(RVA = "0x101114C6C", Offset = "0x1114C6C", VA = "0x101114C6C")]
		public static float CalcOrderBaseCount(int spd)
		{
			return 0f;
		}

		// Token: 0x06000C6C RID: 3180 RVA: 0x00005010 File Offset: 0x00003210
		[Token(Token = "0x6000B92")]
		[Address(RVA = "0x1011146B4", Offset = "0x11146B4", VA = "0x1011146B4")]
		public static float CalcOrderValue(int spd, float loadFactor, float loadFactorCoef, float orderCoef)
		{
			return 0f;
		}

		// Token: 0x06000C6D RID: 3181 RVA: 0x00005028 File Offset: 0x00003228
		[Token(Token = "0x6000B93")]
		[Address(RVA = "0x101114608", Offset = "0x1114608", VA = "0x101114608")]
		public static float CalcOrderValue(CharacterHandler charaHndl, int commandIndex, bool isUseUniqueSkill)
		{
			return 0f;
		}

		// Token: 0x06000C6E RID: 3182 RVA: 0x00005040 File Offset: 0x00003240
		[Token(Token = "0x6000B94")]
		[Address(RVA = "0x101114D94", Offset = "0x1114D94", VA = "0x101114D94")]
		public static float CalcOrderValue(CharacterHandler charaHndl, BattleCommandData command)
		{
			return 0f;
		}

		// Token: 0x06000C6F RID: 3183 RVA: 0x00005058 File Offset: 0x00003258
		[Token(Token = "0x6000B95")]
		[Address(RVA = "0x101114F74", Offset = "0x1114F74", VA = "0x101114F74")]
		public static float CalcWaveStartOrderValue(CharacterHandler charaHndl)
		{
			return 0f;
		}

		// Token: 0x06000C70 RID: 3184 RVA: 0x00005070 File Offset: 0x00003270
		[Token(Token = "0x6000B96")]
		[Address(RVA = "0x101115040", Offset = "0x1115040", VA = "0x101115040")]
		private static float CalcStunValue(int damage, int maxHp, float stunCoef, float elementCoef)
		{
			return 0f;
		}

		// Token: 0x06000C71 RID: 3185 RVA: 0x00005088 File Offset: 0x00003288
		[Token(Token = "0x6000B97")]
		[Address(RVA = "0x101115058", Offset = "0x1115058", VA = "0x101115058")]
		private static float CalcStunerValue(float stuner, float stunMag, float ratio)
		{
			return 0f;
		}

		// Token: 0x06000C72 RID: 3186 RVA: 0x000050A0 File Offset: 0x000032A0
		[Token(Token = "0x6000B98")]
		[Address(RVA = "0x101115070", Offset = "0x1115070", VA = "0x101115070")]
		public static float CalcStunDecreaseValueOnTurnStart()
		{
			return 0f;
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x000050B8 File Offset: 0x000032B8
		[Token(Token = "0x6000B99")]
		[Address(RVA = "0x10111510C", Offset = "0x111510C", VA = "0x10111510C")]
		private static float CalcAdditiveOrderValueOnStun(float orderValue)
		{
			return 0f;
		}

		// Token: 0x06000C74 RID: 3188 RVA: 0x000050D0 File Offset: 0x000032D0
		[Token(Token = "0x6000B9A")]
		[Address(RVA = "0x101115190", Offset = "0x1115190", VA = "0x101115190")]
		private static float CalcAdditiveOrderValueOnStunWeakAttacked(float orderValue, float ratio)
		{
			return 0f;
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000B9B")]
		[Address(RVA = "0x10111521C", Offset = "0x111521C", VA = "0x10111521C")]
		public static List<CharacterHandler> UpdateOrderOnStun(BattleOrder battleOrder)
		{
			return null;
		}

		// Token: 0x06000C76 RID: 3190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000B9C")]
		[Address(RVA = "0x101115674", Offset = "0x1115674", VA = "0x101115674")]
		public static void UpdateOrderOnStunWeakAttacked(BattleOrder battleOrder, CharacterHandler receiver, float orderValue, float ratio)
		{
		}

		// Token: 0x06000C77 RID: 3191 RVA: 0x000050E8 File Offset: 0x000032E8
		[Token(Token = "0x6000B9D")]
		[Address(RVA = "0x101115830", Offset = "0x1115830", VA = "0x101115830")]
		public static int CalcDamage(int power, int atkOrMgc, float togetherAttackCoef, int recv_def, float recv_barrierCutRatio, float elementCoef, bool isGuard, float criticalCoef, float ratio, int opt_special_damage)
		{
			return 0;
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x00005100 File Offset: 0x00003300
		[Token(Token = "0x6000B9E")]
		[Address(RVA = "0x101115A04", Offset = "0x1115A04", VA = "0x101115A04")]
		public static int CalcSelfDamage(int power, float ratio)
		{
			return 0;
		}

		// Token: 0x06000C79 RID: 3193 RVA: 0x00005118 File Offset: 0x00003318
		[Token(Token = "0x6000B9F")]
		[Address(RVA = "0x101115A40", Offset = "0x1115A40", VA = "0x101115A40")]
		public static int CalcRecover(int power, int mgc, float elementCoef, int recv_MaxHp, float togetherAttackCoef, bool recv_isUnhappy)
		{
			return 0;
		}

		// Token: 0x06000C7A RID: 3194 RVA: 0x00005130 File Offset: 0x00003330
		[Token(Token = "0x6000BA0")]
		[Address(RVA = "0x101115BAC", Offset = "0x1115BAC", VA = "0x101115BAC")]
		public static float CalcElementCoef(bool[] atkElementFlgs, eElementType receiverElementType, float[] receiverFixedElementCoefs, out int out_RegistHit_Or_DefaultHit_Or_WeakHit)
		{
			return 0f;
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BA1")]
		[Address(RVA = "0x101115E3C", Offset = "0x1115E3C", VA = "0x101115E3C")]
		private static void DrainByAttackDamage(CharacterHandler executor, bool isCannotRecover, int damage, float divRatio, List<SkillActionEffectSet> effectSets, BattleCommandSolveResult out_result)
		{
		}

		// Token: 0x06000C7C RID: 3196 RVA: 0x00005148 File Offset: 0x00003348
		[Token(Token = "0x6000BA2")]
		[Address(RVA = "0x10111606C", Offset = "0x111606C", VA = "0x10111606C")]
		public static int JudgeElementCompatibility(List<eElementType> atkElementTypes, eElementType receiverElementType)
		{
			return 0;
		}

		// Token: 0x06000C7D RID: 3197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BA3")]
		[Address(RVA = "0x1011161C4", Offset = "0x11161C4", VA = "0x1011161C4")]
		public static void SetupDefaultElementCoef(eElementType elementType, ref float[] ref_results)
		{
		}

		// Token: 0x06000C7E RID: 3198 RVA: 0x00005160 File Offset: 0x00003360
		[Token(Token = "0x6000BA4")]
		[Address(RVA = "0x10110B8D8", Offset = "0x110B8D8", VA = "0x10110B8D8")]
		public static eElementType GetStrongElementType(eElementType elementType)
		{
			return eElementType.Fire;
		}

		// Token: 0x06000C7F RID: 3199 RVA: 0x00005178 File Offset: 0x00003378
		[Token(Token = "0x6000BA5")]
		[Address(RVA = "0x101114008", Offset = "0x1114008", VA = "0x101114008")]
		public static eElementType GetWeakElementType(eElementType elementType)
		{
			return eElementType.Fire;
		}

		// Token: 0x06000C80 RID: 3200 RVA: 0x00005190 File Offset: 0x00003390
		[Token(Token = "0x6000BA6")]
		[Address(RVA = "0x101116364", Offset = "0x1116364", VA = "0x101116364")]
		public static float CalcTogetherAttackChainCoef(CharacterHandler ch)
		{
			return 0f;
		}

		// Token: 0x06000C81 RID: 3201 RVA: 0x000051A8 File Offset: 0x000033A8
		[Token(Token = "0x6000BA7")]
		[Address(RVA = "0x101116510", Offset = "0x1116510", VA = "0x101116510")]
		public static bool CalcCritical(int luck, int recv_luck, int registHit_Or_DefaultHit_Or_WeakHit, bool isNextBuffCritical, bool isEnableStateAbnormalBearish, bool isStun)
		{
			return default(bool);
		}

		// Token: 0x06000C82 RID: 3202 RVA: 0x000051C0 File Offset: 0x000033C0
		[Token(Token = "0x6000BA8")]
		[Address(RVA = "0x1011166C4", Offset = "0x11166C4", VA = "0x1011166C4")]
		public static bool SolveCommandAt(BattleCommandData command, int contentIndex, float[] optionParams, CharacterHandler targetCharaHndl, out List<SkillActionEffectSet> out_effectSets)
		{
			return default(bool);
		}

		// Token: 0x06000C83 RID: 3203 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BA9")]
		[Address(RVA = "0x10111678C", Offset = "0x111678C", VA = "0x10111678C")]
		private static List<SkillActionEffectSet> SolveSkillContent(BattleCommandData command, int contentIndex, float[] optionParams, CharacterHandler targetCharaHndl)
		{
			return null;
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BAA")]
		[Address(RVA = "0x10111F650", Offset = "0x111F650", VA = "0x10111F650")]
		public static string SkillContentTypeToString(eSkillContentType skillContentType)
		{
			return null;
		}

		// Token: 0x06000C85 RID: 3205 RVA: 0x000051D8 File Offset: 0x000033D8
		[Token(Token = "0x6000BAB")]
		[Address(RVA = "0x10111F744", Offset = "0x111F744", VA = "0x10111F744")]
		private static int GetAttackOptionValue(BattleCommandData.SkillContentSet contentSet, int index = 0)
		{
			return 0;
		}

		// Token: 0x06000C86 RID: 3206 RVA: 0x000051F0 File Offset: 0x000033F0
		[Token(Token = "0x6000BAC")]
		[Address(RVA = "0x101113FFC", Offset = "0x1113FFC", VA = "0x101113FFC")]
		private static bool GetIsValue(float inputAsBool)
		{
			return default(bool);
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x00005208 File Offset: 0x00003408
		[Token(Token = "0x6000BAD")]
		[Address(RVA = "0x10111F7A8", Offset = "0x111F7A8", VA = "0x10111F7A8")]
		public static bool IsBuffParam(BattleDefine.eStatus status, float param)
		{
			return default(bool);
		}

		// Token: 0x06000C88 RID: 3208 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BAE")]
		[Address(RVA = "0x101117130", Offset = "0x1117130", VA = "0x101117130")]
		private static List<SkillActionEffectSet> SolveSkillContent_Attack(CharacterHandler executor, bool isMasterSkill, bool isCard, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, float[] optionParams, float orderValue, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C89 RID: 3209 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BAF")]
		[Address(RVA = "0x10111937C", Offset = "0x111937C", VA = "0x10111937C")]
		private static List<SkillActionEffectSet> SolveSkillContent_AttackSelf(CharacterHandler executor, bool isMasterSkill, bool isCard, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, float[] optionParams, float orderValue, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C8A RID: 3210 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BB0")]
		[Address(RVA = "0x101119BF0", Offset = "0x1119BF0", VA = "0x101119BF0")]
		private static List<SkillActionEffectSet> SolveSkillContent_Recover(CharacterHandler executor, bool isMasterSkill, bool isCard, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C8B RID: 3211 RVA: 0x00005220 File Offset: 0x00003420
		[Token(Token = "0x6000BB1")]
		[Address(RVA = "0x10111F81C", Offset = "0x111F81C", VA = "0x10111F81C")]
		private static int _SolveRecover(CharacterHandler executor, CharacterHandler receiver, bool isMasterSkill, bool isRegene, bool isCard, int power, float togetherAttackChainCoef, out float out_elementCoef, out bool out_recoveryValueIsZeroBecauseUnhappy)
		{
			return 0;
		}

		// Token: 0x06000C8C RID: 3212 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BB2")]
		[Address(RVA = "0x10111A018", Offset = "0x111A018", VA = "0x10111A018")]
		public static List<SkillActionEffectSet> SolveSkillContent_StatusChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C8D RID: 3213 RVA: 0x00005238 File Offset: 0x00003438
		[Token(Token = "0x6000BB3")]
		[Address(RVA = "0x10111FD58", Offset = "0x111FD58", VA = "0x10111FD58")]
		private static bool SetStatusChangeEffect(CharacterHandler receiver, float[] opt, bool isBuff, bool isMissed, List<SkillActionEffectSet> effectSets)
		{
			return default(bool);
		}

		// Token: 0x06000C8E RID: 3214 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BB4")]
		[Address(RVA = "0x10111EBB0", Offset = "0x111EBB0", VA = "0x10111EBB0")]
		public static List<SkillActionEffectSet> SolveSkillContent_RandomStatusChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C8F RID: 3215 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BB5")]
		[Address(RVA = "0x10111F39C", Offset = "0x111F39C", VA = "0x10111F39C")]
		private static List<SkillActionEffectSet> SolveSkillContent_StatusChangeDisable(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C90 RID: 3216 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BB6")]
		[Address(RVA = "0x10111A5A4", Offset = "0x111A5A4", VA = "0x10111A5A4")]
		public static List<SkillActionEffectSet> SolveSkillContent_StatusChangeReset(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C91 RID: 3217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BB7")]
		[Address(RVA = "0x10111FEB0", Offset = "0x111FEB0", VA = "0x10111FEB0")]
		private static void SetSkillMissEffect(CharacterHandler receiver, List<SkillActionEffectSet> effectSets, eSkillContentType skillType)
		{
		}

		// Token: 0x06000C92 RID: 3218 RVA: 0x00005250 File Offset: 0x00003450
		[Token(Token = "0x6000BB8")]
		[Address(RVA = "0x10111667C", Offset = "0x111667C", VA = "0x10111667C")]
		private static bool IsProbabilityPercent(float probability)
		{
			return default(bool);
		}

		// Token: 0x06000C93 RID: 3219 RVA: 0x00005268 File Offset: 0x00003468
		[Token(Token = "0x6000BB9")]
		[Address(RVA = "0x10111F7C8", Offset = "0x111F7C8", VA = "0x10111F7C8")]
		private static bool IsProbabilityPercent(float probability, out float out_rnd)
		{
			return default(bool);
		}

		// Token: 0x06000C94 RID: 3220 RVA: 0x00005280 File Offset: 0x00003480
		[Token(Token = "0x6000BBA")]
		[Address(RVA = "0x10111FF58", Offset = "0x111FF58", VA = "0x10111FF58")]
		private static int GetProbabilityIndexByWeighting(float[] probDat)
		{
			return 0;
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BBB")]
		[Address(RVA = "0x10111A8C4", Offset = "0x111A8C4", VA = "0x10111A8C4")]
		public static List<SkillActionEffectSet> SolveSkillContent_Abnormal(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C96 RID: 3222 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BBC")]
		[Address(RVA = "0x10111AEB4", Offset = "0x111AEB4", VA = "0x10111AEB4")]
		public static List<SkillActionEffectSet> SolveSkillContent_AbnormalRecover(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C97 RID: 3223 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BBD")]
		[Address(RVA = "0x10111B190", Offset = "0x111B190", VA = "0x10111B190")]
		private static List<SkillActionEffectSet> SolveSkillContent_AbnormalDisable(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BBE")]
		[Address(RVA = "0x10111B3EC", Offset = "0x111B3EC", VA = "0x10111B3EC")]
		private static List<SkillActionEffectSet> SolveSkillContent_AbnormalAdditionalProbability(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C99 RID: 3225 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BBF")]
		[Address(RVA = "0x10111B69C", Offset = "0x111B69C", VA = "0x10111B69C")]
		private static List<SkillActionEffectSet> SolveSkillContent_ElementResist(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9A RID: 3226 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC0")]
		[Address(RVA = "0x10111BBB4", Offset = "0x111BBB4", VA = "0x10111BBB4")]
		private static List<SkillActionEffectSet> SolveSkillContent_ElementChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC1")]
		[Address(RVA = "0x10111BF48", Offset = "0x111BF48", VA = "0x10111BF48")]
		private static List<SkillActionEffectSet> SolveSkillContent_WeakElementBonus(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9C RID: 3228 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC2")]
		[Address(RVA = "0x10111E778", Offset = "0x111E778", VA = "0x10111E778")]
		private static List<SkillActionEffectSet> SolveSkillContent_CriticalDamageChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9D RID: 3229 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC3")]
		[Address(RVA = "0x10111C2DC", Offset = "0x111C2DC", VA = "0x10111C2DC")]
		private static List<SkillActionEffectSet> SolveSkillContent_NextAttackUp(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC4")]
		[Address(RVA = "0x10111C650", Offset = "0x111C650", VA = "0x10111C650")]
		private static List<SkillActionEffectSet> SolveSkillContent_NextAttackCritical(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000C9F RID: 3231 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC5")]
		[Address(RVA = "0x10111C848", Offset = "0x111C848", VA = "0x10111C848")]
		private static List<SkillActionEffectSet> SolveSkillContent_Barrier(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA0 RID: 3232 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC6")]
		[Address(RVA = "0x10111EFF4", Offset = "0x111EFF4", VA = "0x10111EFF4")]
		public static List<SkillActionEffectSet> SolveSkillContent_KiraraJumpGaugeUpOnDamage(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC7")]
		[Address(RVA = "0x10111E3EC", Offset = "0x111E3EC", VA = "0x10111E3EC")]
		private static List<SkillActionEffectSet> SolveSkillContent_LoadFactorReduce(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA2 RID: 3234 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC8")]
		[Address(RVA = "0x10111CBAC", Offset = "0x111CBAC", VA = "0x10111CBAC")]
		private static List<SkillActionEffectSet> SolveSkillContent_RecastChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BC9")]
		[Address(RVA = "0x10111CDD4", Offset = "0x111CDD4", VA = "0x10111CDD4")]
		private static List<SkillActionEffectSet> SolveSkillContent_KiraraJumpGaugeChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCA")]
		[Address(RVA = "0x10111D0CC", Offset = "0x111D0CC", VA = "0x10111D0CC")]
		private static List<SkillActionEffectSet> SolveSkillContent_KiraraJumpGaugeCoef(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA5 RID: 3237 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCB")]
		[Address(RVA = "0x10111D0D4", Offset = "0x111D0D4", VA = "0x10111D0D4")]
		private static List<SkillActionEffectSet> SolveSkillContent_OrderChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA6 RID: 3238 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCC")]
		[Address(RVA = "0x10111D0DC", Offset = "0x111D0DC", VA = "0x10111D0DC")]
		private static List<SkillActionEffectSet> SolveSkillContent_HateChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA7 RID: 3239 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCD")]
		[Address(RVA = "0x10111D458", Offset = "0x111D458", VA = "0x10111D458")]
		private static List<SkillActionEffectSet> SolveSkillContent_ChargeChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA8 RID: 3240 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCE")]
		[Address(RVA = "0x10111D740", Offset = "0x111D740", VA = "0x10111D740")]
		private static List<SkillActionEffectSet> SolveSkillContent_ChainCoefChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CA9 RID: 3241 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BCF")]
		[Address(RVA = "0x10111D980", Offset = "0x111D980", VA = "0x10111D980")]
		private static List<SkillActionEffectSet> SolveSkillContent_Card(CharacterHandler executor, int commandIndex, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CAA RID: 3242 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BD0")]
		[Address(RVA = "0x10111DE08", Offset = "0x111DE08", VA = "0x10111DE08")]
		private static List<SkillActionEffectSet> SolveSkillContent_StunRecover(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CAB RID: 3243 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BD1")]
		[Address(RVA = "0x10111E01C", Offset = "0x111E01C", VA = "0x10111E01C")]
		public static List<SkillActionEffectSet> SolveSkillContent_Regene(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x06000CAC RID: 3244 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BD2")]
		[Address(RVA = "0x101116EC8", Offset = "0x1116EC8", VA = "0x101116EC8")]
		private static List<CharacterHandler> GetSkillTargets(eSkillTargetType skillTargetType, CharacterHandler executor)
		{
			return null;
		}

		// Token: 0x06000CAD RID: 3245 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BD3")]
		[Address(RVA = "0x101120150", Offset = "0x1120150", VA = "0x101120150")]
		public static List<eElementType> GetElementsOnAttack(BattleCommandData.SkillContentSet contentSet)
		{
			return null;
		}

		// Token: 0x06000CAE RID: 3246 RVA: 0x00005298 File Offset: 0x00003498
		[Token(Token = "0x6000BD4")]
		[Address(RVA = "0x1011203D4", Offset = "0x11203D4", VA = "0x1011203D4")]
		public static int SolveConfusionCommand(CharacterHandler targetCharaHndl)
		{
			return 0;
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x000052B0 File Offset: 0x000034B0
		[Token(Token = "0x6000BD5")]
		[Address(RVA = "0x101120714", Offset = "0x1120714", VA = "0x101120714")]
		public static bool SolveParalysisCancel(CharacterHandler targetCharaHndl)
		{
			return default(bool);
		}

		// Token: 0x06000CB0 RID: 3248 RVA: 0x000052C8 File Offset: 0x000034C8
		[Token(Token = "0x6000BD6")]
		[Address(RVA = "0x101120808", Offset = "0x1120808", VA = "0x101120808")]
		public static int SolvePoison(CharacterHandler targetCharaHndl)
		{
			return 0;
		}

		// Token: 0x06000CB1 RID: 3249 RVA: 0x000052E0 File Offset: 0x000034E0
		[Token(Token = "0x6000BD7")]
		[Address(RVA = "0x1011209E0", Offset = "0x11209E0", VA = "0x1011209E0")]
		public static int SolveRegene(CharacterHandler targetCharaHndl, out bool out_recoveryValueIsZeroBecauseUnhappy)
		{
			return 0;
		}

		// Token: 0x06000CB2 RID: 3250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BD8")]
		[Address(RVA = "0x101120BD4", Offset = "0x1120BD4", VA = "0x101120BD4")]
		public static void GetTakeCharaList(BattleCommandData command, out List<CharacterHandler> out_tgtCharaList, out List<CharacterHandler> out_myCharaList)
		{
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BD9")]
		[Address(RVA = "0x101121080", Offset = "0x1121080", VA = "0x101121080")]
		public static List<CharacterHandler> GetTgtAllCharaHndlsFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			return null;
		}

		// Token: 0x06000CB4 RID: 3252 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BDA")]
		[Address(RVA = "0x1011211B4", Offset = "0x11211B4", VA = "0x1011211B4")]
		public static CharacterHandler GetTgtSingleCharaHndlFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			return null;
		}

		// Token: 0x06000CB5 RID: 3253 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BDB")]
		[Address(RVA = "0x101121338", Offset = "0x1121338", VA = "0x101121338")]
		public static List<CharacterHandler> GetMyAllCharaHndlsFromCommand(CharacterHandler executor, BattleCommandData command, bool isIncludeExecutor)
		{
			return null;
		}

		// Token: 0x06000CB6 RID: 3254 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000BDC")]
		[Address(RVA = "0x10112149C", Offset = "0x112149C", VA = "0x10112149C")]
		public static CharacterHandler GetMySingleCharaHndlFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			return null;
		}

		// Token: 0x06000CB7 RID: 3255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BDD")]
		[Address(RVA = "0x101121620", Offset = "0x1121620", VA = "0x101121620")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135B48", Offset = "0x135B48")]
		private static void LOG(object o)
		{
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BDE")]
		[Address(RVA = "0x101121624", Offset = "0x1121624", VA = "0x101121624")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135B80", Offset = "0x135B80")]
		private static void LOGF(string format, params object[] args)
		{
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BDF")]
		[Address(RVA = "0x101121628", Offset = "0x1121628", VA = "0x101121628")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135BB8", Offset = "0x135BB8")]
		private static void LOG_DAMAGE(CharacterHandler executor, CharacterHandler receiver)
		{
		}

		// Token: 0x06000CBA RID: 3258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BE0")]
		[Address(RVA = "0x101121E20", Offset = "0x1121E20", VA = "0x101121E20")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135BF0", Offset = "0x135BF0")]
		private static void LOG_SKILLCONTENT_StatusChange(BattleCommandData.SkillContentSet contentSet, int skillLv)
		{
		}

		// Token: 0x06000CBB RID: 3259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000BE1")]
		[Address(RVA = "0x101123178", Offset = "0x1123178", VA = "0x101123178")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135C28", Offset = "0x135C28")]
		private static void LOG_SKILLCONTENT_ElementResist(BattleCommandData.SkillContentSet contentSet, int skillLv)
		{
		}

		// Token: 0x04000E0E RID: 3598
		[Token(Token = "0x4000B1F")]
		private const int ATTACKOPTION_ABNORMAL = 100;

		// Token: 0x0200038D RID: 909
		[Token(Token = "0x2000D68")]
		private enum eAutoSkillTypeOrdinal
		{
			// Token: 0x04000E10 RID: 3600
			[Token(Token = "0x4005675")]
			Recover,
			// Token: 0x04000E11 RID: 3601
			[Token(Token = "0x4005676")]
			Card,
			// Token: 0x04000E12 RID: 3602
			[Token(Token = "0x4005677")]
			Regene,
			// Token: 0x04000E13 RID: 3603
			[Token(Token = "0x4005678")]
			AbnormalRecover,
			// Token: 0x04000E14 RID: 3604
			[Token(Token = "0x4005679")]
			StatusChangeReset,
			// Token: 0x04000E15 RID: 3605
			[Token(Token = "0x400567A")]
			HateUp,
			// Token: 0x04000E16 RID: 3606
			[Token(Token = "0x400567B")]
			HateDown,
			// Token: 0x04000E17 RID: 3607
			[Token(Token = "0x400567C")]
			GaugeUpOnDamage,
			// Token: 0x04000E18 RID: 3608
			[Token(Token = "0x400567D")]
			RandomBuffDebuff,
			// Token: 0x04000E19 RID: 3609
			[Token(Token = "0x400567E")]
			AtkBuff,
			// Token: 0x04000E1A RID: 3610
			[Token(Token = "0x400567F")]
			DefDebuff,
			// Token: 0x04000E1B RID: 3611
			[Token(Token = "0x4005680")]
			ElemDebuff,
			// Token: 0x04000E1C RID: 3612
			[Token(Token = "0x4005681")]
			DefBuff,
			// Token: 0x04000E1D RID: 3613
			[Token(Token = "0x4005682")]
			ElemBuff,
			// Token: 0x04000E1E RID: 3614
			[Token(Token = "0x4005683")]
			AtkDebuff,
			// Token: 0x04000E1F RID: 3615
			[Token(Token = "0x4005684")]
			StatusChangeDisable,
			// Token: 0x04000E20 RID: 3616
			[Token(Token = "0x4005685")]
			Unknown,
			// Token: 0x04000E21 RID: 3617
			[Token(Token = "0x4005686")]
			Attack,
			// Token: 0x04000E22 RID: 3618
			[Token(Token = "0x4005687")]
			Num
		}

		// Token: 0x0200038E RID: 910
		[Token(Token = "0x2000D69")]
		private enum eSkillContentTypeAttackOption
		{
			// Token: 0x04000E24 RID: 3620
			[Token(Token = "0x4005689")]
			None,
			// Token: 0x04000E25 RID: 3621
			[Token(Token = "0x400568A")]
			Abnormal,
			// Token: 0x04000E26 RID: 3622
			[Token(Token = "0x400568B")]
			MaxHpRatio,
			// Token: 0x04000E27 RID: 3623
			[Token(Token = "0x400568C")]
			CurrentHpRatio,
			// Token: 0x04000E28 RID: 3624
			[Token(Token = "0x400568D")]
			FixValue,
			// Token: 0x04000E29 RID: 3625
			[Token(Token = "0x400568E")]
			DivideDamage,
			// Token: 0x04000E2A RID: 3626
			[Token(Token = "0x400568F")]
			Stun = 99,
			// Token: 0x04000E2B RID: 3627
			[Token(Token = "0x4005690")]
			Abnormal_Confusion,
			// Token: 0x04000E2C RID: 3628
			[Token(Token = "0x4005691")]
			Abnormal_Paralysis,
			// Token: 0x04000E2D RID: 3629
			[Token(Token = "0x4005692")]
			Abnormal_Poison,
			// Token: 0x04000E2E RID: 3630
			[Token(Token = "0x4005693")]
			Abnormal_Bearish,
			// Token: 0x04000E2F RID: 3631
			[Token(Token = "0x4005694")]
			Abnormal_Sleep,
			// Token: 0x04000E30 RID: 3632
			[Token(Token = "0x4005695")]
			Abnormal_Unhappy,
			// Token: 0x04000E31 RID: 3633
			[Token(Token = "0x4005696")]
			Abnormal_Silence,
			// Token: 0x04000E32 RID: 3634
			[Token(Token = "0x4005697")]
			Abnormal_Isolation,
			// Token: 0x04000E33 RID: 3635
			[Token(Token = "0x4005698")]
			Abnormal_Num
		}
	}
}
