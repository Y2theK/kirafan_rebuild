﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000392 RID: 914
	[Token(Token = "0x20002FB")]
	[StructLayout(3)]
	public static class BattleCommandSkillContentArgDefine
	{
		// Token: 0x02000393 RID: 915
		[Token(Token = "0x2000D6C")]
		public enum eLoadFactorArg
		{
			// Token: 0x04000E5B RID: 3675
			[Token(Token = "0x40056A5")]
			ElapsedType,
			// Token: 0x04000E5C RID: 3676
			[Token(Token = "0x40056A6")]
			Turn,
			// Token: 0x04000E5D RID: 3677
			[Token(Token = "0x40056A7")]
			Ratio
		}

		// Token: 0x02000394 RID: 916
		[Token(Token = "0x2000D6D")]
		public enum eCriticalDamageArg
		{
			// Token: 0x04000E5F RID: 3679
			[Token(Token = "0x40056A9")]
			ElapsedType,
			// Token: 0x04000E60 RID: 3680
			[Token(Token = "0x40056AA")]
			Turn,
			// Token: 0x04000E61 RID: 3681
			[Token(Token = "0x40056AB")]
			Ratio
		}
	}
}
