﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000395 RID: 917
	[Token(Token = "0x20002FC")]
	[StructLayout(3)]
	public class BattleCommandSolveResult
	{
		// Token: 0x06000CE2 RID: 3298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C07")]
		[Address(RVA = "0x101124478", Offset = "0x1124478", VA = "0x101124478")]
		public BattleCommandSolveResult()
		{
		}

		// Token: 0x06000CE3 RID: 3299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C08")]
		[Address(RVA = "0x10112454C", Offset = "0x112454C", VA = "0x10112454C")]
		public void Clear()
		{
		}

		// Token: 0x06000CE4 RID: 3300 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C09")]
		[Address(RVA = "0x101124618", Offset = "0x1124618", VA = "0x101124618")]
		public BattleCommandSolveResult.CharaResult AddCharaResult_Safe(CharacterHandler hndl)
		{
			return null;
		}

		// Token: 0x06000CE5 RID: 3301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C0A")]
		[Address(RVA = "0x1011247AC", Offset = "0x11247AC", VA = "0x1011247AC")]
		public void AddCharaDiedWithThisCommand_Safe(CharacterHandler dead, CharacterHandler killer)
		{
		}

		// Token: 0x06000CE6 RID: 3302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C0B")]
		[Address(RVA = "0x101124860", Offset = "0x1124860", VA = "0x101124860")]
		public void AddCharaRevivedWithThisCommand_Safe(CharacterHandler revived, CharacterHandler killer)
		{
		}

		// Token: 0x06000CE7 RID: 3303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C0C")]
		[Address(RVA = "0x101124914", Offset = "0x1124914", VA = "0x101124914")]
		public void AddCharaAttackedWithThisCommand(CharacterHandler damaged, CharacterHandler attacker)
		{
		}

		// Token: 0x06000CE8 RID: 3304 RVA: 0x000054C0 File Offset: 0x000036C0
		[Token(Token = "0x6000C0D")]
		[Address(RVA = "0x1011249C8", Offset = "0x11249C8", VA = "0x1011249C8")]
		public int GetTotalDamageHitNum()
		{
			return 0;
		}

		// Token: 0x06000CE9 RID: 3305 RVA: 0x000054D8 File Offset: 0x000036D8
		[Token(Token = "0x6000C0E")]
		[Address(RVA = "0x101124B80", Offset = "0x1124B80", VA = "0x101124B80")]
		public int GetTotalDamageHitValue()
		{
			return 0;
		}

		// Token: 0x06000CEA RID: 3306 RVA: 0x000054F0 File Offset: 0x000036F0
		[Token(Token = "0x6000C0F")]
		[Address(RVA = "0x101124DA4", Offset = "0x1124DA4", VA = "0x101124DA4")]
		public int GetTotalRecoverHitNum()
		{
			return 0;
		}

		// Token: 0x06000CEB RID: 3307 RVA: 0x00005508 File Offset: 0x00003708
		[Token(Token = "0x6000C10")]
		[Address(RVA = "0x101124F5C", Offset = "0x1124F5C", VA = "0x101124F5C")]
		public int GetTotalRecoverHitValue()
		{
			return 0;
		}

		// Token: 0x04000E62 RID: 3682
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000B3B")]
		public Dictionary<CharacterHandler, BattleCommandSolveResult.CharaResult> m_CharaResults;

		// Token: 0x04000E63 RID: 3683
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000B3C")]
		public Dictionary<CharacterHandler, CharacterHandler> m_CharaDiedWithThisCommand;

		// Token: 0x04000E64 RID: 3684
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000B3D")]
		public Dictionary<CharacterHandler, CharacterHandler> m_CharaRevivedWithThisCommand;

		// Token: 0x04000E65 RID: 3685
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000B3E")]
		public Dictionary<CharacterHandler, CharacterHandler> m_CharaAttackedWithThisCommand;

		// Token: 0x04000E66 RID: 3686
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000B3F")]
		public bool m_IsCreatedCard;

		// Token: 0x02000396 RID: 918
		[Token(Token = "0x2000D6E")]
		public enum eSpecialReason
		{
			// Token: 0x04000E68 RID: 3688
			[Token(Token = "0x40056AD")]
			BecameStateAbnormal,
			// Token: 0x04000E69 RID: 3689
			[Token(Token = "0x40056AE")]
			MissStateAbnormal,
			// Token: 0x04000E6A RID: 3690
			[Token(Token = "0x40056AF")]
			RecoverdStateAbnormal,
			// Token: 0x04000E6B RID: 3691
			[Token(Token = "0x40056B0")]
			RecoveryValueIsZeroBecauseUnhappy,
			// Token: 0x04000E6C RID: 3692
			[Token(Token = "0x40056B1")]
			Num
		}

		// Token: 0x02000397 RID: 919
		[Token(Token = "0x2000D6F")]
		public class HitData
		{
			// Token: 0x170000A6 RID: 166
			// (get) Token: 0x06000CEC RID: 3308 RVA: 0x00005520 File Offset: 0x00003720
			// (set) Token: 0x06000CED RID: 3309 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x17000687")]
			public BattleDefine.eFluctuation m_ElementFluctuation
			{
				[Token(Token = "0x6005D9F")]
				[Address(RVA = "0x101125608", Offset = "0x1125608", VA = "0x101125608")]
				[CompilerGenerated]
				get
				{
					return BattleDefine.eFluctuation.None;
				}
				[Token(Token = "0x6005DA0")]
				[Address(RVA = "0x101125610", Offset = "0x1125610", VA = "0x101125610")]
				[CompilerGenerated]
				set
				{
				}
			}

			// Token: 0x06000CEE RID: 3310 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DA1")]
			[Address(RVA = "0x101125618", Offset = "0x1125618", VA = "0x101125618")]
			public HitData(int value, bool isCritical, int registHit_Or_defaultHit_Or_weakHit, BattleDefine.eFluctuation elementFluctuation, bool isCard)
			{
			}

			// Token: 0x06000CEF RID: 3311 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DA2")]
			[Address(RVA = "0x101125684", Offset = "0x1125684", VA = "0x101125684")]
			public HitData(int value, bool isCritical, int registHit_Or_defaultHit_Or_weakHit, BattleDefine.eFluctuation elementFluctuation)
			{
			}

			// Token: 0x04000E6D RID: 3693
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40056B2")]
			public int m_Value;

			// Token: 0x04000E6E RID: 3694
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40056B3")]
			public bool m_IsCard;

			// Token: 0x04000E6F RID: 3695
			[Cpp2IlInjected.FieldOffset(Offset = "0x15")]
			[Token(Token = "0x40056B4")]
			public bool m_IsCritical;

			// Token: 0x04000E70 RID: 3696
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40056B5")]
			public int m_registHit_Or_defaultHit_Or_weakHit;

			// Token: 0x04000E72 RID: 3698
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40056B7")]
			public bool m_isNeedDamageReaction;
		}

		// Token: 0x02000398 RID: 920
		[Token(Token = "0x2000D70")]
		public class CharaResult
		{
			// Token: 0x06000CF0 RID: 3312 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DA3")]
			[Address(RVA = "0x1011246F8", Offset = "0x11246F8", VA = "0x1011246F8")]
			public CharaResult()
			{
			}

			// Token: 0x06000CF1 RID: 3313 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DA4")]
			[Address(RVA = "0x101125180", Offset = "0x1125180", VA = "0x101125180")]
			public void Clear()
			{
			}

			// Token: 0x06000CF2 RID: 3314 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DA5")]
			[Address(RVA = "0x101125224", Offset = "0x1125224", VA = "0x101125224")]
			public void AddDamageHitData(BattleCommandSolveResult.HitData hitData)
			{
			}

			// Token: 0x06000CF3 RID: 3315 RVA: 0x00005538 File Offset: 0x00003738
			[Token(Token = "0x6005DA6")]
			[Address(RVA = "0x101124B20", Offset = "0x1124B20", VA = "0x101124B20")]
			public int GetDamageHitDataNum()
			{
				return 0;
			}

			// Token: 0x06000CF4 RID: 3316 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005DA7")]
			[Address(RVA = "0x101125294", Offset = "0x1125294", VA = "0x101125294")]
			public BattleCommandSolveResult.HitData GetDamageHitDataAt(int index)
			{
				return null;
			}

			// Token: 0x06000CF5 RID: 3317 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005DA8")]
			[Address(RVA = "0x101125304", Offset = "0x1125304", VA = "0x101125304")]
			public BattleCommandSolveResult.HitData GetDamageHitDataNew()
			{
				return null;
			}

			// Token: 0x06000CF6 RID: 3318 RVA: 0x00005550 File Offset: 0x00003750
			[Token(Token = "0x6005DA9")]
			[Address(RVA = "0x101124CD8", Offset = "0x1124CD8", VA = "0x101124CD8")]
			public int GetTotalDamageValue()
			{
				return 0;
			}

			// Token: 0x06000CF7 RID: 3319 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DAA")]
			[Address(RVA = "0x1011253B8", Offset = "0x11253B8", VA = "0x1011253B8")]
			public void AddRecoverHitData(BattleCommandSolveResult.HitData hitData)
			{
			}

			// Token: 0x06000CF8 RID: 3320 RVA: 0x00005568 File Offset: 0x00003768
			[Token(Token = "0x6005DAB")]
			[Address(RVA = "0x101124EFC", Offset = "0x1124EFC", VA = "0x101124EFC")]
			public int GetRecoverHitDataNum()
			{
				return 0;
			}

			// Token: 0x06000CF9 RID: 3321 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005DAC")]
			[Address(RVA = "0x101125428", Offset = "0x1125428", VA = "0x101125428")]
			public BattleCommandSolveResult.HitData GetRecoverHitDataAt(int index)
			{
				return null;
			}

			// Token: 0x06000CFA RID: 3322 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005DAD")]
			[Address(RVA = "0x101125498", Offset = "0x1125498", VA = "0x101125498")]
			public BattleCommandSolveResult.HitData GetRecoverHitDataNew()
			{
				return null;
			}

			// Token: 0x06000CFB RID: 3323 RVA: 0x00005580 File Offset: 0x00003780
			[Token(Token = "0x6005DAE")]
			[Address(RVA = "0x1011250B4", Offset = "0x11250B4", VA = "0x1011250B4")]
			public int GetTotalRecoverValue()
			{
				return 0;
			}

			// Token: 0x06000CFC RID: 3324 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DAF")]
			[Address(RVA = "0x10112554C", Offset = "0x112554C", VA = "0x10112554C")]
			public void SetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason reason, bool flg)
			{
			}

			// Token: 0x06000CFD RID: 3325 RVA: 0x00005598 File Offset: 0x00003798
			[Token(Token = "0x6005DB0")]
			[Address(RVA = "0x1011255A8", Offset = "0x11255A8", VA = "0x1011255A8")]
			public bool GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason reason)
			{
				return default(bool);
			}

			// Token: 0x06000CFE RID: 3326 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DB1")]
			[Address(RVA = "0x1011255F8", Offset = "0x11255F8", VA = "0x1011255F8")]
			public void SetAutoFacialID(int facialID)
			{
			}

			// Token: 0x06000CFF RID: 3327 RVA: 0x000055B0 File Offset: 0x000037B0
			[Token(Token = "0x6005DB2")]
			[Address(RVA = "0x101125600", Offset = "0x1125600", VA = "0x101125600")]
			public int GetAutoFacialID()
			{
				return 0;
			}

			// Token: 0x04000E73 RID: 3699
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40056B8")]
			private List<BattleCommandSolveResult.HitData> m_DamageHitDatas;

			// Token: 0x04000E74 RID: 3700
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40056B9")]
			private List<BattleCommandSolveResult.HitData> m_RecoverHitDatas;

			// Token: 0x04000E75 RID: 3701
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40056BA")]
			private bool[] m_IsSpecialReasonFlg;

			// Token: 0x04000E76 RID: 3702
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40056BB")]
			private ushort m_ReadDamageHitDataIndex;

			// Token: 0x04000E77 RID: 3703
			[Cpp2IlInjected.FieldOffset(Offset = "0x2A")]
			[Token(Token = "0x40056BC")]
			private ushort m_ReadRecoverHitDataIndex;

			// Token: 0x04000E78 RID: 3704
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40056BD")]
			private int m_AutoFacialID;
		}
	}
}
