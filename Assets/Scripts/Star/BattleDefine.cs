﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star {
    public static class BattleDefine {
        public const int PARTY_NUM = 2;
        public const int PARTY_MEMBER_NUM = 5;
        public const int PARTY_MEMBER_NUM_INC_FRIEND = 6;
        public const int JOIN_NUM = 3;
        public const int BENCH_NUM = 2;
        public const int PL_SKILL_MAX = 4;
        public const string PLAYER_MEMBER_NAME_PREFIX = "P";
        public const string PLAYER_FRIEND_MEMBER_NAME = "PF";
        public const string ENEMY_MEMBER_NAME_PREFIX = "E";
        public const string BATTLE_ACT_KEY_IDLE = "idle";
        public const string BATTLE_ACT_KEY_ABNORMAL = "abnormal";
        public const string BATTLE_ACT_KEY_DAMAGE = "damage";
        public const string BATTLE_ACT_KEY_DEAD = "dead";
        public const string BATTLE_ACT_KEY_RUN = "battle_run";
        public const string BATTLE_ACT_KEY_IN = "battle_in";
        public const string BATTLE_ACT_KEY_OUT = "battle_out";
        public const string BATTLE_ACT_KEY_WALK = "walk";
        public const string BATTLE_ACT_KEY_GUARD = "";
        public const string BATTLE_ACT_KEY_ATK = "attack";
        public const string BATTLE_ACT_KEY_CLS_SKILL = "class_skill_";
        public const string BATTLE_ACT_KEY_CHARA_SKILL = "chara_skill_";
        public const string BATTLE_ACT_KEY_EN_SKILL = "skill_";
        public const string BATTLE_ACT_KEY_EN_UNQ_SKILL = "charge_skill";
        public const string BATTLE_ACT_KEY_WIN_ST = "win_st_";
        public const string BATTLE_ACT_KEY_WIN_LP = "win_lp_";
        public const string BATTLE_ACT_KEY_KIRARA_JUMP = "kirarajump_";
        public const int ACT_KEY_CLS_SKILL_RANGE_MIN = 1;
        public const int ACT_KEY_CLS_SKILL_RANGE_MAX = 3;
        public const int ACT_KEY_CHARA_SKILL_RANGE_MIN = 1;
        public const int ACT_KEY_CHARA_SKILL_RANGE_MAX = 1;
        public const int HIT_STOP_FRAME = 1;
        public const int HIT_STOP_FRAME_STUN = 3;
        public const float CHANGE_COLOR_SKILL_TIME = 0.18f;
        public static readonly Color BG_COLOR_SKILL = new Color(0.5f, 0.5f, 0.5f);
        public static readonly Color CHARA_COLOR_SKILL = new Color(0.5f, 0.5f, 0.5f);
        public static readonly Color BG_COLOR_UNIQUESKILL = new Color(0.25f, 0.25f, 0.25f);
        public static readonly Color CHARA_COLOR_UNIQUESKILL = new Color(0.25f, 0.25f, 0.25f);
        public const float LAST_BLOW_TIME_SCALE = 0.5f;
        public const float LAST_BLOW_TIME_SCALE_SEC = 2.4f;
        public const float LAST_BLOW_TIME_SCALE_SEC_REVERT_RATIO = 1f;
        public const string BGM_DEFAULT = "bgm_battle_1";
        public const string BGM_WARNING = "bgm_battle_2";
        public const string EFFECT_PACK_ALLWAYS = "btl_always";
        public const float CHARA_WAVE_IN_FRAME = 6f;
        public const float CHARA_WAVE_IN_DELAY_INTERVAL = 0.15f;
        public const float CHARA_IDLE_DELAY_INTERVAL = 0.2f;
        public static float[] IDLE_SHADOW_OFFSET_BY_CLASS = new float[] {
            0f,
            -0.2f,
            0.28f,
            0.1f,
            0.1f
        };
        public const float SHADOW_OFFSET_Z = 0.3f;
        public static readonly float[] SHADOW_SCALE_RANGE = new float[] {
            1.3f,
            1.75f
        };
        public static readonly Vector3 PL_STUN_OFFSET = new Vector3(-0.1f, 0.92f, 0f);
        public const float COLLISION_RADIUS = 0.36f;
        public const float TOGETHER_GAUGE_MAX = 3f;
        public const int FACIAL_ID_PLUSEVENT = 9;
        public const int FACIAL_ID_ABNORMALEVENT = 6;
        public const eSoundVoiceListDB VOICE_START = eSoundVoiceListDB.voice_401;
        public const eSoundVoiceListDB VOICE_START_WARNING = eSoundVoiceListDB.voice_401;
        public const eSoundVoiceListDB VOICE_KIRARA_JUMP = eSoundVoiceListDB.voice_412;
        public static readonly string[] VOICE_DAMAGES = new string[] {
            "voice_413",
            "voice_413",
            "voice_413"
        };
        public const eSoundVoiceListDB VOICE_ABNORMAL = eSoundVoiceListDB.voice_415;
        public const eSoundVoiceListDB VOICE_DEAD = eSoundVoiceListDB.voice_416;
        public const eSoundVoiceListDB VOICE_BATTLE_CLEAR = eSoundVoiceListDB.voice_417;
        public const eSoundVoiceListDB VOICE_GAME_OVER = eSoundVoiceListDB.voice_419;
        public const eSoundVoiceListDB VOICE_CHARA_TOUCH = eSoundVoiceListDB.voice_420;
        public static readonly eSoundVoiceListDB[] VOICE_SKILLS = new eSoundVoiceListDB[] {
            eSoundVoiceListDB.voice_403,
            eSoundVoiceListDB.voice_403,
            eSoundVoiceListDB.voice_409,
            eSoundVoiceListDB.voice_406,
            eSoundVoiceListDB.voice_406,
            eSoundVoiceListDB.voice_409,
            eSoundVoiceListDB.voice_411,
            eSoundVoiceListDB.voice_406,
            eSoundVoiceListDB.voice_411,
        };
        public const string EN_VOICE_START = "voice_400";
        public static readonly string[,] EN_VOICE_DAMAGES = new string[3, 3] {
            { "voice_413", "voice_413", "voice_413" },
            { "voice_413", "voice_414", "voice_414" },
            { "voice_413", "voice_414", "voice_416" }
        };
        public const string EN_VOICE_DEAD = "voice_418";
        public const string EN_VOICE_GAME_OVER = "voice_419";
        public static readonly float[] DAMAGE_VOICE_THRESHOLD = new float[]
        {
            1f,
            0.6f,
            0.2f
        };
        public const int HIT_REGIST = -1;
        public const int HIT_DEFAULT = 0;
        public const int HIT_WEAK = 1;
        public const int AI_FLAG_NUM = 5;
        public const int STATUSCHANGE_SPDBUFF_REF_SKILL_LV_TABLE_ID = 1;
        public const int STATUSCHANGE_SPDDEBUFF_REF_SKILL_LV_TABLE_ID = 2;
        public static readonly string[] SKILL_CONTENT_TYPE_STRINGS = new string[] {
            "攻撃",
            "回復",
            "ステータス変化",
            "ステータス変化リセット",
            "状態異常",
            "状態異常回復",
            "状態異常無効",
            "状態異常付与確率補正",
            "属性耐性",
            "属性変化",
            "有利属性ボーナス",
            "次回の攻撃威力アップ",
            "次回の攻撃絶対クリティカル",
            "バリア",
            "リキャスト変化",
            "きららジャンプゲージ量変化",
            "きららジャンプゲージ倍率変化",
            "行動順変化",
            "ヘイト変化",
            "チャージ変化",
            "チェイン倍率変化",
            "スキルカード",
            "スタン回復",
            "リジェネ",
            "何もしない",
            "スキル負荷軽減",
            "クリティカルダメージ量変化",
            "自分自身ダメージ",
            "ランダムステータス変化",
            "被ダメージ時きららジャンプゲージ量上昇",
            "ステータス変化無効"
        };
        public static readonly string[] PASSIVE_SKILL_CONTENT_TYPE_STRINGS = new string[0x12] {
            "ステータス変化",
            "ヘイト変化",
            "状態異常無効",
            "スタン無効",
            "スタン値変化",
            "きららジャンプゲージ量変化",
            "きららジャンプゲージ倍率変化",
            "クリティカルダメージ変化",
            "スキル変化",
            "超過回復化",
            "与ダメｎ％回復",
            "リヴァイブ",
            "スキルカード回数変化",
            "HP依存ステータス変化",
            "カウンターステータス変化",
            "状態異常付与確率補正",
            "属性耐性",
            "ターン増加"
        };
        public static readonly Vector3 BG_POS = new Vector3(0f, 0f, 70f);
        public const float DEFAULT_CHARA_POS_INTERVAL_Z = 6f;
        public const float SCENE_LINE_CHARA = 0f;
        public const float SCENE_LINE_EFFECT_DEFAULT_FROM_NEARCLIP = 20f;
        public const float SCENE_LINE_EFFECT_DAMAGE_FROM_NEARCLIP = 10f;
        public const int SORTING_ORDER_CHARA_BASE = 0;
        public const int SORTING_ORDER_CHARA_INTERVAL = 10;
        public const int SORTING_ORDER_EFFECT_DEFAULT = 1000;
        public const int SORTING_ORDER_EFFECT_DAMAGE = 1500;
        public const int SORTING_ORDER_BG = -32768;

        public enum ePartyType {
            None = -1,
            Player,
            Enemy,
            Num
        }

        public enum eBattlePartySlot {
            None = -1,
            Slot_1,
            Slot_2,
            Slot_3,
            Slot_4,
            Slot_5,
            Num,
            Slot_Friend = 5,
            Num_IncludeFriend
        }

        public enum eJoinMember {
            None = -1,
            Join_1,
            Join_2,
            Join_3,
            Bench_1,
            Bench_2,
            Bench_Friend
        }

        public enum eDeadDetail {
            Alive,
            Dead_Ready,
            Dead_Complete
        }

        public enum eFluctuation {
            Down = -1,
            None,
            Up
        }

        public enum eStatus {
            Atk,
            Mgc,
            Def,
            MDef,
            Spd,
            Luck,
            Num
        }

        public enum eForUIStatusIconFlag {
            StateAbnormal_Confusion,
            StateAbnormal_Paralysis,
            StateAbnormal_Poison,
            StateAbnormal_Bearish,
            StateAbnormal_Sleep,
            StateAbnormal_Unhappy,
            StateAbnormal_Silence,
            StateAbnormal_Isolation,
            StatusChange_Atk_Up,
            StatusChange_Mgc_Up,
            StatusChange_Def_Up,
            StatusChange_MDef_Up,
            StatusChange_Spd_Up,
            StatusChange_Luck_Up,
            CriticalDamageChange_Up,
            HateChange_Up,
            ElementResist_Fire_Up,
            ElementResist_Water_Up,
            ElementResist_Earth_Up,
            ElementResist_Wind_Up,
            ElementResist_Moon_Up,
            ElementResist_Sun_Up,
            StateAbnormalAdditionalProbability_Up,
            StatusChange_Atk_Down,
            StatusChange_Mgc_Down,
            StatusChange_Def_Down,
            StatusChange_MDef_Down,
            StatusChange_Spd_Down,
            StatusChange_Luck_Down,
            CriticalDamageChange_Down,
            HateChange_Down,
            ElementResist_Fire_Down,
            ElementResist_Water_Down,
            ElementResist_Earth_Down,
            ElementResist_Wind_Down,
            ElementResist_Moon_Down,
            ElementResist_Sun_Down,
            StateAbnormalAdditionalProbability_Down,
            Barrier,
            Regene,
            AbsorbAttack,
            StateAbnormalDisable,
            LoadFactorReduce,
            WeakElementBonus,
            NextAtkUp,
            NextMgcUp,
            NextCritical,
            TogetherAttackChainCoefChange,
            TogetherAttackGaugeUpOnDamage,
            StatusChangeDisable_Up,
            StatusChangeDisable_Down,
            Num
        }

        [Serializable]
        public class BuffData {
            public eSkillTurnConsume Type;
            public int Turn;
            public float Val;

            public BuffData(eSkillTurnConsume turnConsume, int turn, float value) {
                Type = turnConsume;
                Turn = turn;
                Val = value;
            }

            public void Reset() {
                Turn = 0;
                Val = 0f;
            }

            public bool IsBuff(eStatus st) {
                return st == eStatus.Spd ? Val < 1f : Val > 0f;
            }

            public bool IsDebuff(eStatus st) {
                return st == eStatus.Spd ? Val > 1f : Val < 0f;
            }
        }

        [Serializable]
        public class BuffStack {
            [SerializeField] private List<BuffData> Stack;

            public BuffStack() {
                Stack = new List<BuffData>();
            }

            public void Add(BuffData buff) {
                Stack.Add(buff);
            }

            public void Clear() {
                Stack.Clear();
            }

            public void RemoveAt(int index) {
                Stack.RemoveAt(index);
            }

            public bool IsEnable() {
                return Stack.Count > 0;
            }

            public float GetValue(bool isSpd = false) {
                float total = 0f;
                if (isSpd) {
                    if (Stack.Count > 0) {
                        float mod = 0f;
                        for (int i = 0; i < Stack.Count; i++) {
                            if (Stack[i].Turn > 0) {
                                mod += 1f - Stack[i].Val;
                            }
                        }
                        total = 1f - mod;
                    }
                } else {
                    for (int i = 0; i < Stack.Count; i++) {
                        if (Stack[i].Turn > 0) {
                            total += Stack[i].Val;
                        }
                    }
                }
                return total;
            }

            public bool IsExistUp(bool isSpd = false) {
                if (isSpd) {
                    for (int i = 0; i < Stack.Count; i++) {
                        if (Stack[i].Val < 1f) {
                            return true;
                        }
                    }
                } else {
                    for (int i = 0; i < Stack.Count; i++) {
                        if (Stack[i].Val > 0f) {
                            return true;
                        }
                    }
                }
                return false;
            }

            public bool IsExistDown(bool isSpd = false) {
                if (isSpd) {
                    for (int i = 0; i < Stack.Count; i++) {
                        if (Stack[i].Val > 1f) {
                            return true;
                        }
                    }
                } else {
                    for (int i = 0; i < Stack.Count; i++) {
                        if (Stack[i].Val < 0f) {
                            return true;
                        }
                    }
                }
                return false;
            }

            public void DecreaseTurn(bool isMyTurn) {
                for (int i = Stack.Count - 1; i >= 0; i--) {
                    if (Stack[i].Type != eSkillTurnConsume.Self || isMyTurn) {
                        Stack[i].Turn--;
                        if (Stack[i].Turn <= 0) {
                            Stack.RemoveAt(i);
                        }
                    }
                }
            }

            public int GetStackNum() {
                return Stack.Count;
            }

            public BuffData GetStackAt(int index) {
                return Stack[index];
            }
        }

        [Serializable]
        public class StateAbnormal {
            public bool IsRegist;
            public int Turn;

            public StateAbnormal() { }

            public StateAbnormal(bool isRegist, int turn) {
                IsRegist = isRegist;
                Turn = turn;
            }

            public void Reset() {
                Turn = 0;
            }

            public bool IsEnable() {
                return Turn > 0;
            }

            public void DecreaseTurn(bool isMyTurn) {
                if (Turn > 0) {
                    Turn--;
                }
            }
        }

        public enum eNextBuff {
            Attack,
            Magic,
            Critical,
            Num
        }

        [Serializable]
        public class NextBuff {
            public eStep Step;
            public float Val;

            public void Reset() {
                Step = eStep.None;
                Val = 0f;
            }

            public void SetUsedIfAvailable() {
                if (Step == eStep.Ready) {
                    Step = eStep.Used;
                }
            }

            public void ResetIfUsed() {
                if (Step == eStep.Used) {
                    Reset();
                }
            }

            public float GetCoef() {
                if (Step >= eStep.Ready && Step <= eStep.Used) {
                    return Val;
                }
                return 0f;
            }

            public enum eStep {
                None = -1,
                Ready,
                Used
            }
        }

        [Serializable]
        public class RegeneData {
            public string OwnerName;
            public int Turn;
            public int Pow;

            public RegeneData(string ownerName, int turn, int power) {
                OwnerName = ownerName;
                Turn = turn;
                Pow = power;
            }

            public void Reset() {
                OwnerName = null;
                Turn = 0;
                Pow = 0;
            }

            public bool IsEnable() {
                return Turn > 0;
            }

            public void DecreaseTurn(bool isMyTurn) {
                Turn -= isMyTurn ? 1 : 0;
                if (Turn <= 0) {
                    Reset();
                }
            }
        }

        public enum eReviveRecoverType {
            Ratio,
            Direct,
            Num
        }

        [Serializable]
        public class ReviveData {
            public eReviveRecoverType Type;
            public float Recover;

            public ReviveData(eReviveRecoverType type, float recover) {
                Type = type;
                Recover = recover;
            }

            public int CalcRecoverValue(int maxHp) {
                if (Type == eReviveRecoverType.Ratio) {
                    return (int)(Recover * maxHp * 0.01f);
                }
                return (int)Recover;
            }
        }

        [Serializable]
        public class CounterStatusChangeData {
            public bool IsAvailable;
            public eSkillTargetType Target;
            public float[] Param;

            public CounterStatusChangeData(eSkillTargetType target, float[] p) {
                IsAvailable = true;
                Target = target;
                Param = p;
            }

            public void Reset() {
                IsAvailable = false;
                Target = eSkillTargetType.Self;
                Param = null;
            }
        }

        [Serializable]
        public class TurnChecker {
            public eSkillTurnConsume Type;
            public int Turn;

            public TurnChecker(eSkillTurnConsume turnConsume, int turn) {
                Type = turnConsume;
                Turn = turn;
            }

            public void Init(eSkillTurnConsume turnConsume, int turn) {
                Type = turnConsume;
                Turn = turn;
            }

            public void Reset() {
                Turn = 0;
            }

            public bool IsEnable() {
                return Turn > 0;
            }

            public void DecreaseTurn(bool isMyTurn) {
                if (Type != eSkillTurnConsume.Self || isMyTurn) {
                    if (Turn > 0) {
                        Turn--;
                    }
                }
            }
        }

        [Serializable]
        public class GaugeUpOnDamage {
            public TurnChecker turnCh;
            public float value;

            public GaugeUpOnDamage(eSkillTurnConsume turnConsume, int turn, float v) {
                turnCh = new TurnChecker(turnConsume, turn);
                value = v;
            }

            public void Init(eSkillTurnConsume turnConsume, int turn, float v) {
                turnCh.Init(turnConsume, turn);
                value = v;
            }

            public void Reset() {
                turnCh.Reset();
                value = 0;
            }

            public bool IsEnable() {
                return turnCh.IsEnable();
            }

            public void DecreaseTurn(bool isMyTurn) {
                turnCh.DecreaseTurn(isMyTurn);
            }
        }

        public enum eTurnPreEndEvent {
            None = -1,
            CounterStatusChange,
            AwakeFromSleep,
            AbnormalPoison,
            Regene,
            Num
        }

        public class AttachEffect {
            public string EffectID;
            public EffectHandler EffectHndl;
            public Transform AttachTo;
            public Vector3 Offset = Vector3.zero;

            public void Destroy() {
                if (EffectHndl != null) {
                    GameSystem.Inst.EffectMng.DestroyToCache(EffectHndl);
                    EffectHndl = null;
                }
                AttachTo = null;
            }
        }
    }
}
