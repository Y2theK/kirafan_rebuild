﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004B0 RID: 1200
	[Token(Token = "0x20003A8")]
	[StructLayout(3)]
	public class BattleDefineDB : ScriptableObject
	{
		// Token: 0x060013F9 RID: 5113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AE")]
		[Address(RVA = "0x101127DB4", Offset = "0x1127DB4", VA = "0x101127DB4")]
		public BattleDefineDB()
		{
		}

		// Token: 0x040016DD RID: 5853
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010C3")]
		public BattleDefineDB_Param[] m_Params;
	}
}
