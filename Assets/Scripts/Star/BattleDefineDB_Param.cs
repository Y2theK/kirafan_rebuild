﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004AF RID: 1199
	[Token(Token = "0x20003A7")]
	[Serializable]
	[StructLayout(0, Size = 4)]
	public struct BattleDefineDB_Param
	{
		// Token: 0x040016DC RID: 5852
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010C2")]
		public float m_Value;
	}
}
