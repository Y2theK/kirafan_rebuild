﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003AF RID: 943
	[Token(Token = "0x20002FE")]
	[StructLayout(3)]
	public abstract class BattleDropObjectHandler
	{
		// Token: 0x06000D2E RID: 3374
		[Token(Token = "0x6000C12")]
		[Address(Slot = "4")]
		public abstract void Update();

		// Token: 0x06000D2F RID: 3375
		[Token(Token = "0x6000C13")]
		[Address(Slot = "5")]
		public abstract bool Play(Transform parent, int sortingOrder);

		// Token: 0x06000D30 RID: 3376
		[Token(Token = "0x6000C14")]
		[Address(Slot = "6")]
		public abstract void Stop();

		// Token: 0x06000D31 RID: 3377
		[Token(Token = "0x6000C15")]
		[Address(Slot = "7")]
		public abstract bool IsPlaying();

		// Token: 0x06000D32 RID: 3378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C16")]
		[Address(RVA = "0x101127DBC", Offset = "0x1127DBC", VA = "0x101127DBC")]
		protected BattleDropObjectHandler()
		{
		}
	}
}
