﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003B0 RID: 944
	[Token(Token = "0x20002FF")]
	[StructLayout(3)]
	public sealed class BattleDropObjectManager
	{
		// Token: 0x06000D33 RID: 3379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C17")]
		[Address(RVA = "0x101127DC4", Offset = "0x1127DC4", VA = "0x101127DC4")]
		public void Setup(Transform cacheRoot)
		{
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C18")]
		[Address(RVA = "0x101127F70", Offset = "0x1127F70", VA = "0x101127F70")]
		public void Destroy()
		{
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C19")]
		[Address(RVA = "0x1011280A8", Offset = "0x11280A8", VA = "0x1011280A8")]
		public void Update()
		{
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C1A")]
		[Address(RVA = "0x1011281FC", Offset = "0x11281FC", VA = "0x1011281FC")]
		private BattleDropObjectHandler ScoopCache(BattleDropObjectManager.eDropObjType type)
		{
			return null;
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C1B")]
		[Address(RVA = "0x101128324", Offset = "0x1128324", VA = "0x101128324")]
		public void PlayTreasureBox(BattleDropItem dropItem, Vector3 startPos, Vector3 targetPos, Transform parent, int sortingOrder, Action<BattleDropItem> OnCompleteFunc)
		{
		}

		// Token: 0x06000D38 RID: 3384 RVA: 0x00005700 File Offset: 0x00003900
		[Token(Token = "0x6000C1C")]
		[Address(RVA = "0x101128480", Offset = "0x1128480", VA = "0x101128480")]
		public bool IsPlayingTreasureBox()
		{
			return default(bool);
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C1D")]
		[Address(RVA = "0x101128588", Offset = "0x1128588", VA = "0x101128588")]
		public BattleDropObjectManager()
		{
		}

		// Token: 0x04000F58 RID: 3928
		[Token(Token = "0x4000B97")]
		private static readonly int[] CHACE_NUMS;

		// Token: 0x04000F59 RID: 3929
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000B98")]
		private Transform m_Root;

		// Token: 0x04000F5A RID: 3930
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000B99")]
		private List<BattleDropObjectHandler> m_ActiveHndls;

		// Token: 0x04000F5B RID: 3931
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000B9A")]
		private Dictionary<BattleDropObjectManager.eDropObjType, BattleDropObjectHandler[]> m_Chaces;

		// Token: 0x020003B1 RID: 945
		[Token(Token = "0x2000D85")]
		public enum eDropObjType
		{
			// Token: 0x04000F5D RID: 3933
			[Token(Token = "0x4005743")]
			None = -1,
			// Token: 0x04000F5E RID: 3934
			[Token(Token = "0x4005744")]
			TreasureBox,
			// Token: 0x04000F5F RID: 3935
			[Token(Token = "0x4005745")]
			Num
		}
	}
}
