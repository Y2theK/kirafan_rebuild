﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003B2 RID: 946
	[Token(Token = "0x2000300")]
	[StructLayout(3)]
	public class BattleFriendCutIn : MonoBehaviour
	{
		// Token: 0x06000D3B RID: 3387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C1F")]
		[Address(RVA = "0x1011286B0", Offset = "0x11286B0", VA = "0x1011286B0")]
		private void Start()
		{
		}

		// Token: 0x06000D3C RID: 3388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C20")]
		[Address(RVA = "0x101128708", Offset = "0x1128708", VA = "0x101128708")]
		private void Update()
		{
		}

		// Token: 0x06000D3D RID: 3389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C21")]
		[Address(RVA = "0x1011291BC", Offset = "0x11291BC", VA = "0x1011291BC")]
		public void Play(int charaID)
		{
		}

		// Token: 0x06000D3E RID: 3390 RVA: 0x00005718 File Offset: 0x00003918
		[Token(Token = "0x6000C22")]
		[Address(RVA = "0x101129214", Offset = "0x1129214", VA = "0x101129214")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06000D3F RID: 3391 RVA: 0x00005730 File Offset: 0x00003930
		[Token(Token = "0x6000C23")]
		[Address(RVA = "0x101129224", Offset = "0x1129224", VA = "0x101129224")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000D40 RID: 3392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C24")]
		[Address(RVA = "0x101129234", Offset = "0x1129234", VA = "0x101129234")]
		public void Destroy()
		{
		}

		// Token: 0x06000D41 RID: 3393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C25")]
		[Address(RVA = "0x101128FEC", Offset = "0x1128FEC", VA = "0x101128FEC")]
		private void SetStep(BattleFriendCutIn.eStep step)
		{
		}

		// Token: 0x06000D42 RID: 3394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C26")]
		[Address(RVA = "0x101128FF4", Offset = "0x1128FF4", VA = "0x101128FF4")]
		private void ApplyCharaIllustTexture()
		{
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x00005748 File Offset: 0x00003948
		[Token(Token = "0x6000C27")]
		[Address(RVA = "0x101129184", Offset = "0x1129184", VA = "0x101129184")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000D44 RID: 3396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C28")]
		[Address(RVA = "0x1011292DC", Offset = "0x11292DC", VA = "0x1011292DC")]
		public BattleFriendCutIn()
		{
		}

		// Token: 0x04000F60 RID: 3936
		[Token(Token = "0x4000B9B")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04000F61 RID: 3937
		[Token(Token = "0x4000B9C")]
		private const string OBJ_CHARA_ILLUST = "chara";

		// Token: 0x04000F62 RID: 3938
		[Token(Token = "0x4000B9D")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000F63 RID: 3939
		[Token(Token = "0x4000B9E")]
		private const string RESOURCE_PATH = "battle/friendcutin/friendcutin.muast";

		// Token: 0x04000F64 RID: 3940
		[Token(Token = "0x4000B9F")]
		private const string OBJ_MODEL_PATH = "FriendCutIn";

		// Token: 0x04000F65 RID: 3941
		[Token(Token = "0x4000BA0")]
		private const string OBJ_ANIM_PATH = "MeigeAC_FriendCutIn@Take 001";

		// Token: 0x04000F66 RID: 3942
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BA1")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04000F67 RID: 3943
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BA2")]
		[SerializeField]
		private AdjustOrthographicCamera m_AdjustOrthographicCamera;

		// Token: 0x04000F68 RID: 3944
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BA3")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000F69 RID: 3945
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000BA4")]
		private BattleFriendCutIn.eStep m_Step;

		// Token: 0x04000F6A RID: 3946
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000BA5")]
		private bool m_IsDonePrepare;

		// Token: 0x04000F6B RID: 3947
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000BA6")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000F6C RID: 3948
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000BA7")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000F6D RID: 3949
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000BA8")]
		private SpriteHandler m_SpriteHndl;

		// Token: 0x04000F6E RID: 3950
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000BA9")]
		private Transform m_BaseTransform;

		// Token: 0x04000F6F RID: 3951
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000BAA")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04000F70 RID: 3952
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000BAB")]
		private GameObject m_BodyObj;

		// Token: 0x04000F71 RID: 3953
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000BAC")]
		private Texture m_Texture;

		// Token: 0x04000F72 RID: 3954
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000BAD")]
		private int m_CharaID;

		// Token: 0x020003B3 RID: 947
		[Token(Token = "0x2000D86")]
		private enum eStep
		{
			// Token: 0x04000F74 RID: 3956
			[Token(Token = "0x4005747")]
			None = -1,
			// Token: 0x04000F75 RID: 3957
			[Token(Token = "0x4005748")]
			Prepare,
			// Token: 0x04000F76 RID: 3958
			[Token(Token = "0x4005749")]
			Prepare_Wait,
			// Token: 0x04000F77 RID: 3959
			[Token(Token = "0x400574A")]
			Prepare_Error,
			// Token: 0x04000F78 RID: 3960
			[Token(Token = "0x400574B")]
			Play,
			// Token: 0x04000F79 RID: 3961
			[Token(Token = "0x400574C")]
			Play_Wait
		}
	}
}
