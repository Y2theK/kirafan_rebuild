﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003B4 RID: 948
	[Token(Token = "0x2000301")]
	[StructLayout(3)]
	public class BattleInputData
	{
		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000D45 RID: 3397 RVA: 0x00005760 File Offset: 0x00003960
		// (set) Token: 0x06000D46 RID: 3398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000095")]
		public BattleInputData.eResult Result
		{
			[Token(Token = "0x6000C29")]
			[Address(RVA = "0x10112935C", Offset = "0x112935C", VA = "0x10112935C")]
			[CompilerGenerated]
			get
			{
				return BattleInputData.eResult.Command;
			}
			[Token(Token = "0x6000C2A")]
			[Address(RVA = "0x101129364", Offset = "0x1129364", VA = "0x101129364")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000D47 RID: 3399 RVA: 0x00005778 File Offset: 0x00003978
		// (set) Token: 0x06000D48 RID: 3400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000096")]
		public bool IsCancel
		{
			[Token(Token = "0x6000C2B")]
			[Address(RVA = "0x10112936C", Offset = "0x112936C", VA = "0x10112936C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000C2C")]
			[Address(RVA = "0x101129374", Offset = "0x1129374", VA = "0x101129374")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000D49 RID: 3401 RVA: 0x00005790 File Offset: 0x00003990
		// (set) Token: 0x06000D4A RID: 3402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000097")]
		public int SelectCommandIndex
		{
			[Token(Token = "0x6000C2D")]
			[Address(RVA = "0x10112937C", Offset = "0x112937C", VA = "0x10112937C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6000C2E")]
			[Address(RVA = "0x101129384", Offset = "0x1129384", VA = "0x101129384")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000D4B RID: 3403 RVA: 0x000057A8 File Offset: 0x000039A8
		// (set) Token: 0x06000D4C RID: 3404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000098")]
		public bool SelectIsUniqueSkill
		{
			[Token(Token = "0x6000C2F")]
			[Address(RVA = "0x10112938C", Offset = "0x112938C", VA = "0x10112938C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000C30")]
			[Address(RVA = "0x101129394", Offset = "0x1129394", VA = "0x101129394")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000D4D RID: 3405 RVA: 0x000057C0 File Offset: 0x000039C0
		// (set) Token: 0x06000D4E RID: 3406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000099")]
		public int SelectBenchIndex
		{
			[Token(Token = "0x6000C31")]
			[Address(RVA = "0x10112939C", Offset = "0x112939C", VA = "0x10112939C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6000C32")]
			[Address(RVA = "0x1011293A4", Offset = "0x11293A4", VA = "0x1011293A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000D4F RID: 3407 RVA: 0x000057D8 File Offset: 0x000039D8
		// (set) Token: 0x06000D50 RID: 3408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700009A")]
		public int SelectMasterSkillIndex
		{
			[Token(Token = "0x6000C33")]
			[Address(RVA = "0x1011293AC", Offset = "0x11293AC", VA = "0x1011293AC")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6000C34")]
			[Address(RVA = "0x1011293B4", Offset = "0x11293B4", VA = "0x1011293B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000D51 RID: 3409 RVA: 0x000057F0 File Offset: 0x000039F0
		// (set) Token: 0x06000D52 RID: 3410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700009B")]
		public BattleDefine.eJoinMember SelectSingleTargetIndex
		{
			[Token(Token = "0x6000C35")]
			[Address(RVA = "0x1011293BC", Offset = "0x11293BC", VA = "0x1011293BC")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.eJoinMember.Join_1;
			}
			[Token(Token = "0x6000C36")]
			[Address(RVA = "0x1011293C4", Offset = "0x11293C4", VA = "0x1011293C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000D53 RID: 3411 RVA: 0x00005808 File Offset: 0x00003A08
		// (set) Token: 0x06000D54 RID: 3412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700009C")]
		public BattleDefine.eJoinMember InterruptJoin
		{
			[Token(Token = "0x6000C37")]
			[Address(RVA = "0x1011293CC", Offset = "0x11293CC", VA = "0x1011293CC")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.eJoinMember.Join_1;
			}
			[Token(Token = "0x6000C38")]
			[Address(RVA = "0x1011293D4", Offset = "0x11293D4", VA = "0x1011293D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000D55 RID: 3413 RVA: 0x00005820 File Offset: 0x00003A20
		// (set) Token: 0x06000D56 RID: 3414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700009D")]
		public bool IsDecideInterruptJoin
		{
			[Token(Token = "0x6000C39")]
			[Address(RVA = "0x1011293DC", Offset = "0x11293DC", VA = "0x1011293DC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000C3A")]
			[Address(RVA = "0x1011293E4", Offset = "0x11293E4", VA = "0x1011293E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000D57 RID: 3415 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06000D58 RID: 3416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700009E")]
		public CharacterHandler ViewStatusWindowChara
		{
			[Token(Token = "0x6000C3B")]
			[Address(RVA = "0x1011293EC", Offset = "0x11293EC", VA = "0x1011293EC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6000C3C")]
			[Address(RVA = "0x1011293F4", Offset = "0x11293F4", VA = "0x1011293F4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x06000D59 RID: 3417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C3D")]
		[Address(RVA = "0x1011293FC", Offset = "0x11293FC", VA = "0x1011293FC")]
		public void Reset()
		{
		}

		// Token: 0x06000D5A RID: 3418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C3E")]
		[Address(RVA = "0x101129424", Offset = "0x1129424", VA = "0x101129424")]
		public void InputCommand_Decide(bool selectIsUniqueSkill = false)
		{
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C3F")]
		[Address(RVA = "0x101129430", Offset = "0x1129430", VA = "0x101129430")]
		public void InputTogetherAttack_Start()
		{
		}

		// Token: 0x06000D5C RID: 3420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C40")]
		[Address(RVA = "0x101129444", Offset = "0x1129444", VA = "0x101129444")]
		public void InputTogetherAttack_Decide()
		{
		}

		// Token: 0x06000D5D RID: 3421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C41")]
		[Address(RVA = "0x101129458", Offset = "0x1129458", VA = "0x101129458")]
		public void InputTogetherAttack_Cancel()
		{
		}

		// Token: 0x06000D5E RID: 3422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C42")]
		[Address(RVA = "0x10112946C", Offset = "0x112946C", VA = "0x10112946C")]
		public void InputMaster_Start()
		{
		}

		// Token: 0x06000D5F RID: 3423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C43")]
		[Address(RVA = "0x101129488", Offset = "0x1129488", VA = "0x101129488")]
		public void InputMemberChange_Decide(int benchIndex)
		{
		}

		// Token: 0x06000D60 RID: 3424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C44")]
		[Address(RVA = "0x101129498", Offset = "0x1129498", VA = "0x101129498")]
		public void InputMasterSkill_Decide(int selectMasterSkillIndex, BattleDefine.eJoinMember selectSingleTargetIndex)
		{
		}

		// Token: 0x06000D61 RID: 3425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C45")]
		[Address(RVA = "0x1011294A8", Offset = "0x11294A8", VA = "0x1011294A8")]
		public void InputMaster_Cancel()
		{
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C46")]
		[Address(RVA = "0x1011294B8", Offset = "0x11294B8", VA = "0x1011294B8")]
		public void InputInterruptFriend_Start()
		{
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C47")]
		[Address(RVA = "0x1011294D0", Offset = "0x11294D0", VA = "0x1011294D0")]
		public void InputInterruptFriend_Select(BattleDefine.eJoinMember interruptJoin)
		{
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C48")]
		[Address(RVA = "0x1011294E0", Offset = "0x11294E0", VA = "0x1011294E0")]
		public void InputInterruptFriend_Decide()
		{
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C49")]
		[Address(RVA = "0x1011294F4", Offset = "0x11294F4", VA = "0x1011294F4")]
		public void InputInterruptFriend_Cancel()
		{
		}

		// Token: 0x06000D66 RID: 3430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C4A")]
		[Address(RVA = "0x101129508", Offset = "0x1129508", VA = "0x101129508")]
		public void InputViewStatusWindow_Start(CharacterHandler viewCharaHndl)
		{
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C4B")]
		[Address(RVA = "0x101129518", Offset = "0x1129518", VA = "0x101129518")]
		public void InputViewStatusWindow_Select(CharacterHandler viewCharaHndl)
		{
		}

		// Token: 0x06000D68 RID: 3432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C4C")]
		[Address(RVA = "0x101129528", Offset = "0x1129528", VA = "0x101129528")]
		public void InputViewStatusWindow_Cancel()
		{
		}

		// Token: 0x06000D69 RID: 3433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C4D")]
		[Address(RVA = "0x101129540", Offset = "0x1129540", VA = "0x101129540")]
		public BattleInputData()
		{
		}

		// Token: 0x020003B5 RID: 949
		[Token(Token = "0x2000D87")]
		public enum eResult
		{
			// Token: 0x04000F85 RID: 3973
			[Token(Token = "0x400574E")]
			None = -1,
			// Token: 0x04000F86 RID: 3974
			[Token(Token = "0x400574F")]
			Command,
			// Token: 0x04000F87 RID: 3975
			[Token(Token = "0x4005750")]
			Master,
			// Token: 0x04000F88 RID: 3976
			[Token(Token = "0x4005751")]
			TogetherAttack,
			// Token: 0x04000F89 RID: 3977
			[Token(Token = "0x4005752")]
			InterruptFriend,
			// Token: 0x04000F8A RID: 3978
			[Token(Token = "0x4005753")]
			ViewStatusWindow
		}
	}
}
