﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003B6 RID: 950
	[Token(Token = "0x2000302")]
	[StructLayout(3)]
	public class BattleKiraraJumpObjectHandler : MonoBehaviour
	{
		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000D6A RID: 3434 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700009F")]
		public MsbHandler MsbHndl
		{
			[Token(Token = "0x6000C4E")]
			[Address(RVA = "0x101129548", Offset = "0x1129548", VA = "0x101129548")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000D6B RID: 3435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C4F")]
		[Address(RVA = "0x101129550", Offset = "0x1129550", VA = "0x101129550")]
		public void Setup(string resourceName, MeigeAnimClipHolder meigeAnimClipHolder, List<CharacterHandler> charaHndls)
		{
		}

		// Token: 0x06000D6C RID: 3436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C50")]
		[Address(RVA = "0x1011299EC", Offset = "0x11299EC", VA = "0x1011299EC")]
		public void Destroy()
		{
		}

		// Token: 0x06000D6D RID: 3437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C51")]
		[Address(RVA = "0x101129A1C", Offset = "0x1129A1C", VA = "0x101129A1C")]
		public void DetachAllCharas()
		{
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C52")]
		[Address(RVA = "0x101129800", Offset = "0x1129800", VA = "0x101129800")]
		private Transform GetLocator(int totalCharaNum, int index)
		{
			return null;
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C53")]
		[Address(RVA = "0x101129BB0", Offset = "0x1129BB0", VA = "0x101129BB0")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x06000D70 RID: 3440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C54")]
		[Address(RVA = "0x101129B14", Offset = "0x1129B14", VA = "0x101129B14")]
		public void DetachCamera()
		{
		}

		// Token: 0x06000D71 RID: 3441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C55")]
		[Address(RVA = "0x101129C64", Offset = "0x1129C64", VA = "0x101129C64")]
		public void Play()
		{
		}

		// Token: 0x06000D72 RID: 3442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C56")]
		[Address(RVA = "0x101129E58", Offset = "0x1129E58", VA = "0x101129E58")]
		public void Pause()
		{
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x00005838 File Offset: 0x00003A38
		[Token(Token = "0x6000C57")]
		[Address(RVA = "0x101129EF4", Offset = "0x1129EF4", VA = "0x101129EF4")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C58")]
		[Address(RVA = "0x1011298C4", Offset = "0x11298C4", VA = "0x1011298C4")]
		private void SetCharaParent(CharacterHandler charaHndl, Transform parent)
		{
		}

		// Token: 0x06000D75 RID: 3445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C59")]
		[Address(RVA = "0x101129F74", Offset = "0x1129F74", VA = "0x101129F74")]
		public BattleKiraraJumpObjectHandler()
		{
		}

		// Token: 0x04000F8B RID: 3979
		[Token(Token = "0x4000BB8")]
		private const string LOCATOR_KEY = "loc_{0}_{1}";

		// Token: 0x04000F8C RID: 3980
		[Token(Token = "0x4000BB9")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000F8D RID: 3981
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BBA")]
		private Transform m_BaseTransform;

		// Token: 0x04000F8E RID: 3982
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BBB")]
		private Animation m_Anim;

		// Token: 0x04000F8F RID: 3983
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BBC")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000F90 RID: 3984
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000BBD")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04000F91 RID: 3985
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000BBE")]
		private List<CharacterHandler> m_CharaHndls;
	}
}
