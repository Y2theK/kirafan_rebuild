﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003B7 RID: 951
	[Token(Token = "0x2000303")]
	[StructLayout(3)]
	public class BattleKiraraJumpScene
	{
		// Token: 0x06000D76 RID: 3446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C5A")]
		[Address(RVA = "0x101129F7C", Offset = "0x1129F7C", VA = "0x101129F7C")]
		public BattleKiraraJumpScene()
		{
		}

		// Token: 0x06000D77 RID: 3447 RVA: 0x00005850 File Offset: 0x00003A50
		[Token(Token = "0x6000C5B")]
		[Address(RVA = "0x10112A000", Offset = "0x112A000", VA = "0x10112A000")]
		public bool Prepare(BattleSystem system, Camera camera, List<CharacterHandler> charaHndls)
		{
			return default(bool);
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x00005868 File Offset: 0x00003A68
		[Token(Token = "0x6000C5C")]
		[Address(RVA = "0x10112A024", Offset = "0x112A024", VA = "0x10112A024")]
		public bool IsCompltePrepare()
		{
			return default(bool);
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x00005880 File Offset: 0x00003A80
		[Token(Token = "0x6000C5D")]
		[Address(RVA = "0x10112A044", Offset = "0x112A044", VA = "0x10112A044")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000D7A RID: 3450 RVA: 0x00005898 File Offset: 0x00003A98
		[Token(Token = "0x6000C5E")]
		[Address(RVA = "0x10112A064", Offset = "0x112A064", VA = "0x10112A064")]
		public bool Destory()
		{
			return default(bool);
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x000058B0 File Offset: 0x00003AB0
		[Token(Token = "0x6000C5F")]
		[Address(RVA = "0x10112A0B4", Offset = "0x112A0B4", VA = "0x10112A0B4")]
		public bool IsComplteDestory()
		{
			return default(bool);
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x000058C8 File Offset: 0x00003AC8
		[Token(Token = "0x6000C60")]
		[Address(RVA = "0x10112A0D8", Offset = "0x112A0D8", VA = "0x10112A0D8")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x000058E0 File Offset: 0x00003AE0
		[Token(Token = "0x6000C61")]
		[Address(RVA = "0x10112A090", Offset = "0x112A090", VA = "0x10112A090")]
		public bool IsCompltePlay()
		{
			return default(bool);
		}

		// Token: 0x06000D7E RID: 3454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C62")]
		[Address(RVA = "0x10112A104", Offset = "0x112A104", VA = "0x10112A104")]
		public void Update()
		{
		}

		// Token: 0x06000D7F RID: 3455 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C63")]
		[Address(RVA = "0x10112AE20", Offset = "0x112AE20", VA = "0x10112AE20")]
		public List<CharacterHandler> GetSortTargetCharaHndls()
		{
			return null;
		}

		// Token: 0x06000D80 RID: 3456 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C64")]
		[Address(RVA = "0x10112AE28", Offset = "0x112AE28", VA = "0x10112AE28")]
		public Renderer[] GetBaseObjectRenderers()
		{
			return null;
		}

		// Token: 0x06000D81 RID: 3457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C65")]
		[Address(RVA = "0x10112AF24", Offset = "0x112AF24", VA = "0x10112AF24")]
		public void SetBaseObjectSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C66")]
		[Address(RVA = "0x10112AE18", Offset = "0x112AE18", VA = "0x10112AE18")]
		private void SetDestroyStep(BattleKiraraJumpScene.eDestroyStep step)
		{
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x000058F8 File Offset: 0x00003AF8
		[Token(Token = "0x6000C67")]
		[Address(RVA = "0x10112AA00", Offset = "0x112AA00", VA = "0x10112AA00")]
		private BattleKiraraJumpScene.eMode Update_Destroy()
		{
			return BattleKiraraJumpScene.eMode.Prepare;
		}

		// Token: 0x06000D84 RID: 3460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C68")]
		[Address(RVA = "0x10112AE10", Offset = "0x112AE10", VA = "0x10112AE10")]
		private void SetMainStep(BattleKiraraJumpScene.eMainStep step)
		{
		}

		// Token: 0x06000D85 RID: 3461 RVA: 0x00005910 File Offset: 0x00003B10
		[Token(Token = "0x6000C69")]
		[Address(RVA = "0x10112A6C4", Offset = "0x112A6C4", VA = "0x10112A6C4")]
		private BattleKiraraJumpScene.eMode Update_Main()
		{
			return BattleKiraraJumpScene.eMode.Prepare;
		}

		// Token: 0x06000D86 RID: 3462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C6A")]
		[Address(RVA = "0x10112AE08", Offset = "0x112AE08", VA = "0x10112AE08")]
		private void SetPrepareStep(BattleKiraraJumpScene.ePrepareStep step)
		{
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x00005928 File Offset: 0x00003B28
		[Token(Token = "0x6000C6B")]
		[Address(RVA = "0x10112A198", Offset = "0x112A198", VA = "0x10112A198")]
		private BattleKiraraJumpScene.eMode Update_Prepare()
		{
			return BattleKiraraJumpScene.eMode.Prepare;
		}

		// Token: 0x04000F92 RID: 3986
		[Token(Token = "0x4000BBF")]
		private const string RESOURCE_PATH = "battle/kiraraJump/kiraraJump.muast";

		// Token: 0x04000F93 RID: 3987
		[Token(Token = "0x4000BC0")]
		private const string OBJ_MODEL_PATH = "KiraraJump";

		// Token: 0x04000F94 RID: 3988
		[Token(Token = "0x4000BC1")]
		private const string OBJ_ANIM_PATH = "MeigeAC_KiraraJump@Take 001";

		// Token: 0x04000F95 RID: 3989
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000BC2")]
		private BattleKiraraJumpScene.eMode m_Mode;

		// Token: 0x04000F96 RID: 3990
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000BC3")]
		private readonly Color FADE_COLOR;

		// Token: 0x04000F97 RID: 3991
		[Token(Token = "0x4000BC4")]
		private const float FADE_TIME = 0.2f;

		// Token: 0x04000F98 RID: 3992
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BC5")]
		private BattleSystem m_System;

		// Token: 0x04000F99 RID: 3993
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000BC6")]
		private Camera m_Camera;

		// Token: 0x04000F9A RID: 3994
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000BC7")]
		private List<CharacterHandler> m_CharaHndls;

		// Token: 0x04000F9B RID: 3995
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000BC8")]
		private GameObject m_KiraraJumpObject;

		// Token: 0x04000F9C RID: 3996
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000BC9")]
		private BattleKiraraJumpObjectHandler m_KiraraJumpObjectHandler;

		// Token: 0x04000F9D RID: 3997
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000BCA")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000F9E RID: 3998
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000BCB")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000F9F RID: 3999
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000BCC")]
		private BattleKiraraJumpScene.eDestroyStep m_DestroyStep;

		// Token: 0x04000FA0 RID: 4000
		[Token(Token = "0x4000BCD")]
		private const string ANIM_KEY = "kirarajump_0";

		// Token: 0x04000FA1 RID: 4001
		[Token(Token = "0x4000BCE")]
		private const float FADE_SEC = 0.2f;

		// Token: 0x04000FA2 RID: 4002
		[Token(Token = "0x4000BCF")]
		private const float LAST_FADEOUT_START_TIME = 0.2f;

		// Token: 0x04000FA3 RID: 4003
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4000BD0")]
		private bool m_WasStartedLastFadeOut;

		// Token: 0x04000FA4 RID: 4004
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000BD1")]
		private BattleKiraraJumpScene.eMainStep m_MainStep;

		// Token: 0x04000FA5 RID: 4005
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000BD2")]
		private BattleKiraraJumpScene.ePrepareStep m_PrepareStep;

		// Token: 0x020003B8 RID: 952
		[Token(Token = "0x2000D88")]
		public enum eMode
		{
			// Token: 0x04000FA7 RID: 4007
			[Token(Token = "0x4005755")]
			None = -1,
			// Token: 0x04000FA8 RID: 4008
			[Token(Token = "0x4005756")]
			Prepare,
			// Token: 0x04000FA9 RID: 4009
			[Token(Token = "0x4005757")]
			UpdateMain,
			// Token: 0x04000FAA RID: 4010
			[Token(Token = "0x4005758")]
			Destroy
		}

		// Token: 0x020003B9 RID: 953
		[Token(Token = "0x2000D89")]
		private enum eDestroyStep
		{
			// Token: 0x04000FAC RID: 4012
			[Token(Token = "0x400575A")]
			None = -1,
			// Token: 0x04000FAD RID: 4013
			[Token(Token = "0x400575B")]
			First,
			// Token: 0x04000FAE RID: 4014
			[Token(Token = "0x400575C")]
			Destroy_0,
			// Token: 0x04000FAF RID: 4015
			[Token(Token = "0x400575D")]
			Destroy_1,
			// Token: 0x04000FB0 RID: 4016
			[Token(Token = "0x400575E")]
			Destroy_2,
			// Token: 0x04000FB1 RID: 4017
			[Token(Token = "0x400575F")]
			End
		}

		// Token: 0x020003BA RID: 954
		[Token(Token = "0x2000D8A")]
		private enum eMainStep
		{
			// Token: 0x04000FB3 RID: 4019
			[Token(Token = "0x4005761")]
			None = -1,
			// Token: 0x04000FB4 RID: 4020
			[Token(Token = "0x4005762")]
			First,
			// Token: 0x04000FB5 RID: 4021
			[Token(Token = "0x4005763")]
			Playing,
			// Token: 0x04000FB6 RID: 4022
			[Token(Token = "0x4005764")]
			End
		}

		// Token: 0x020003BB RID: 955
		[Token(Token = "0x2000D8B")]
		private enum ePrepareStep
		{
			// Token: 0x04000FB8 RID: 4024
			[Token(Token = "0x4005766")]
			None = -1,
			// Token: 0x04000FB9 RID: 4025
			[Token(Token = "0x4005767")]
			First,
			// Token: 0x04000FBA RID: 4026
			[Token(Token = "0x4005768")]
			BattleSystem_PreProcess,
			// Token: 0x04000FBB RID: 4027
			[Token(Token = "0x4005769")]
			KiraraJumpPrepare,
			// Token: 0x04000FBC RID: 4028
			[Token(Token = "0x400576A")]
			KiraraJumpPrepare_Wait,
			// Token: 0x04000FBD RID: 4029
			[Token(Token = "0x400576B")]
			KiraraJumpPrepare_Error,
			// Token: 0x04000FBE RID: 4030
			[Token(Token = "0x400576C")]
			Setup,
			// Token: 0x04000FBF RID: 4031
			[Token(Token = "0x400576D")]
			End
		}
	}
}
