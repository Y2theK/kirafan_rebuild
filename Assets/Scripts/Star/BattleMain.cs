﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000771 RID: 1905
	[Token(Token = "0x20005BA")]
	[StructLayout(3)]
	public class BattleMain : GameStateMain
	{
		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06001C98 RID: 7320 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000206")]
		public BattleSystem System
		{
			[Token(Token = "0x6001A16")]
			[Address(RVA = "0x10112B14C", Offset = "0x112B14C", VA = "0x10112B14C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06001C99 RID: 7321 RVA: 0x0000CCA8 File Offset: 0x0000AEA8
		// (set) Token: 0x06001C9A RID: 7322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000207")]
		public bool IsReorder
		{
			[Token(Token = "0x6001A17")]
			[Address(RVA = "0x10112B154", Offset = "0x112B154", VA = "0x10112B154")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001A18")]
			[Address(RVA = "0x10112B15C", Offset = "0x112B15C", VA = "0x10112B15C")]
			set
			{
			}
		}

		// Token: 0x06001C9B RID: 7323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A19")]
		[Address(RVA = "0x10112B164", Offset = "0x112B164", VA = "0x10112B164")]
		private void Start()
		{
		}

		// Token: 0x06001C9C RID: 7324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A1A")]
		[Address(RVA = "0x10112B170", Offset = "0x112B170", VA = "0x10112B170", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001C9D RID: 7325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A1B")]
		[Address(RVA = "0x10112B2A8", Offset = "0x112B2A8", VA = "0x10112B2A8")]
		private void LateUpdate()
		{
		}

		// Token: 0x06001C9E RID: 7326 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A1C")]
		[Address(RVA = "0x10112B300", Offset = "0x112B300", VA = "0x10112B300", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001C9F RID: 7327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A1D")]
		[Address(RVA = "0x10112B4A8", Offset = "0x112B4A8", VA = "0x10112B4A8")]
		public BattleMain()
		{
		}

		// Token: 0x04002CCD RID: 11469
		[Token(Token = "0x4002368")]
		public const int STATE_MAIN = 1;

		// Token: 0x04002CCE RID: 11470
		[Token(Token = "0x4002369")]
		public const int STATE_RESULT = 2;

		// Token: 0x04002CCF RID: 11471
		[Token(Token = "0x400236A")]
		public const int STATE_FINAL = 3;

		// Token: 0x04002CD0 RID: 11472
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400236B")]
		[SerializeField]
		private BattleSystem m_System;

		// Token: 0x04002CD1 RID: 11473
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400236C")]
		private bool m_IsReorder;
	}
}
