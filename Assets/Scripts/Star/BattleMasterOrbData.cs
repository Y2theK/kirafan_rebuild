﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003BC RID: 956
	[Token(Token = "0x2000304")]
	[StructLayout(3)]
	public class BattleMasterOrbData
	{
		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000D88 RID: 3464 RVA: 0x00005940 File Offset: 0x00003B40
		[Token(Token = "0x170000A0")]
		public int OrbID
		{
			[Token(Token = "0x6000C6C")]
			[Address(RVA = "0x10112B4B0", Offset = "0x112B4B0", VA = "0x10112B4B0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000D89 RID: 3465 RVA: 0x00005958 File Offset: 0x00003B58
		[Token(Token = "0x170000A1")]
		public int OrbLv
		{
			[Token(Token = "0x6000C6D")]
			[Address(RVA = "0x10112B4B8", Offset = "0x112B4B8", VA = "0x10112B4B8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000D8A RID: 3466 RVA: 0x00005970 File Offset: 0x00003B70
		[Token(Token = "0x170000A2")]
		public MasterOrbListDB_Param MasterOrbParam
		{
			[Token(Token = "0x6000C6E")]
			[Address(RVA = "0x10112B4C0", Offset = "0x112B4C0", VA = "0x10112B4C0")]
			get
			{
				return default(MasterOrbListDB_Param);
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000D8B RID: 3467 RVA: 0x00005988 File Offset: 0x00003B88
		[Token(Token = "0x170000A3")]
		public int CommandNum
		{
			[Token(Token = "0x6000C6F")]
			[Address(RVA = "0x10112B4D4", Offset = "0x112B4D4", VA = "0x10112B4D4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06000D8C RID: 3468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C70")]
		[Address(RVA = "0x10112B534", Offset = "0x112B534", VA = "0x10112B534")]
		public BattleMasterOrbData()
		{
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x000059A0 File Offset: 0x00003BA0
		[Token(Token = "0x6000C71")]
		[Address(RVA = "0x10112B548", Offset = "0x112B548", VA = "0x10112B548")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C72")]
		[Address(RVA = "0x10112B558", Offset = "0x112B558", VA = "0x10112B558")]
		public void Setup(int orbID, int orbLv)
		{
		}

		// Token: 0x06000D8F RID: 3471 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C73")]
		[Address(RVA = "0x10112B7A0", Offset = "0x112B7A0", VA = "0x10112B7A0")]
		public BattleCommandData GetCommandDataAt(int commandIndex)
		{
			return null;
		}

		// Token: 0x04000FC0 RID: 4032
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000BD3")]
		private int m_OrbID;

		// Token: 0x04000FC1 RID: 4033
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000BD4")]
		private int m_OrbLv;

		// Token: 0x04000FC2 RID: 4034
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BD5")]
		private List<BattleCommandData> m_Commands;

		// Token: 0x04000FC3 RID: 4035
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BD6")]
		private MasterOrbListDB_Param m_MasterOrbParam;
	}
}
