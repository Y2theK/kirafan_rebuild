﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003BD RID: 957
	[Token(Token = "0x2000305")]
	[StructLayout(3)]
	public class BattleMasterSkillCutIn : MonoBehaviour
	{
		// Token: 0x06000D90 RID: 3472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C74")]
		[Address(RVA = "0x10112B814", Offset = "0x112B814", VA = "0x10112B814")]
		private void OnDestroy()
		{
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C75")]
		[Address(RVA = "0x10112B954", Offset = "0x112B954", VA = "0x10112B954")]
		private void Update()
		{
		}

		// Token: 0x06000D92 RID: 3474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C76")]
		[Address(RVA = "0x10112C0C4", Offset = "0x112C0C4", VA = "0x10112C0C4")]
		public void Prepare(int masterOrbID, int sortingOrder)
		{
		}

		// Token: 0x06000D93 RID: 3475 RVA: 0x000059B8 File Offset: 0x00003BB8
		[Token(Token = "0x6000C77")]
		[Address(RVA = "0x10112C1F0", Offset = "0x112C1F0", VA = "0x10112C1F0")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06000D94 RID: 3476 RVA: 0x000059D0 File Offset: 0x00003BD0
		[Token(Token = "0x6000C78")]
		[Address(RVA = "0x10112C1F8", Offset = "0x112C1F8", VA = "0x10112C1F8")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000D95 RID: 3477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C79")]
		[Address(RVA = "0x10112C208", Offset = "0x112C208", VA = "0x10112C208")]
		public void Play(int masterSkillIndex)
		{
		}

		// Token: 0x06000D96 RID: 3478 RVA: 0x000059E8 File Offset: 0x00003BE8
		[Token(Token = "0x6000C7A")]
		[Address(RVA = "0x10112C370", Offset = "0x112C370", VA = "0x10112C370")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06000D97 RID: 3479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C7B")]
		[Address(RVA = "0x10112B818", Offset = "0x112B818", VA = "0x10112B818")]
		public void Destroy()
		{
		}

		// Token: 0x06000D98 RID: 3480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C7C")]
		[Address(RVA = "0x10112C084", Offset = "0x112C084", VA = "0x10112C084")]
		private void SetStep(BattleMasterSkillCutIn.eStep step)
		{
		}

		// Token: 0x06000D99 RID: 3481 RVA: 0x00005A00 File Offset: 0x00003C00
		[Token(Token = "0x6000C7D")]
		[Address(RVA = "0x10112C08C", Offset = "0x112C08C", VA = "0x10112C08C")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000D9A RID: 3482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C7E")]
		[Address(RVA = "0x10112C380", Offset = "0x112C380", VA = "0x10112C380")]
		public BattleMasterSkillCutIn()
		{
		}

		// Token: 0x04000FC4 RID: 4036
		[Token(Token = "0x4000BD7")]
		private const string LAYER_MASK = "UI";

		// Token: 0x04000FC5 RID: 4037
		[Token(Token = "0x4000BD8")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000FC6 RID: 4038
		[Token(Token = "0x4000BD9")]
		private const string RESOURCE_PATH = "battle/masterskillcutin/masterskillcutin.muast";

		// Token: 0x04000FC7 RID: 4039
		[Token(Token = "0x4000BDA")]
		private const string OBJ_MODEL_PATH = "MasterSkillCutIn";

		// Token: 0x04000FC8 RID: 4040
		[Token(Token = "0x4000BDB")]
		private const string OBJ_ANIM_PATH = "MeigeAC_MasterSkillCutIn@Take 001";

		// Token: 0x04000FC9 RID: 4041
		[Token(Token = "0x4000BDC")]
		private const string OBJ_CHARA_ILLUST = "chara";

		// Token: 0x04000FCA RID: 4042
		[Token(Token = "0x4000BDD")]
		private const string OBJ_ORB_ILLUST = "orb2";

		// Token: 0x04000FCB RID: 4043
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BDE")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000FCC RID: 4044
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BDF")]
		private BattleMasterSkillCutIn.eStep m_Step;

		// Token: 0x04000FCD RID: 4045
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000BE0")]
		private bool m_IsDonePrepare;

		// Token: 0x04000FCE RID: 4046
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BE1")]
		private ABResourceLoader m_Loader;

		// Token: 0x04000FCF RID: 4047
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000BE2")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000FD0 RID: 4048
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000BE3")]
		private SpriteHandler m_MasterIllustSpriteHndl;

		// Token: 0x04000FD1 RID: 4049
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000BE4")]
		private SpriteHandler m_MasterOrbSpriteHndl;

		// Token: 0x04000FD2 RID: 4050
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000BE5")]
		private Transform m_BaseTransform;

		// Token: 0x04000FD3 RID: 4051
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000BE6")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04000FD4 RID: 4052
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000BE7")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x04000FD5 RID: 4053
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000BE8")]
		private GameObject m_BodyObj;

		// Token: 0x04000FD6 RID: 4054
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000BE9")]
		private int m_MasterOrbID;

		// Token: 0x04000FD7 RID: 4055
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000BEA")]
		private int m_MasterSkillIndex;

		// Token: 0x04000FD8 RID: 4056
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000BEB")]
		private bool m_IsRequestedVoice;

		// Token: 0x04000FD9 RID: 4057
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4000BEC")]
		private int m_SortingOrder;

		// Token: 0x020003BE RID: 958
		[Token(Token = "0x2000D8C")]
		private enum eStep
		{
			// Token: 0x04000FDB RID: 4059
			[Token(Token = "0x400576F")]
			None = -1,
			// Token: 0x04000FDC RID: 4060
			[Token(Token = "0x4005770")]
			Prepare_Wait,
			// Token: 0x04000FDD RID: 4061
			[Token(Token = "0x4005771")]
			Prepare_Error,
			// Token: 0x04000FDE RID: 4062
			[Token(Token = "0x4005772")]
			Wait,
			// Token: 0x04000FDF RID: 4063
			[Token(Token = "0x4005773")]
			Play_Wait
		}
	}
}
