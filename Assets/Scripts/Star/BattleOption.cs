﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003BF RID: 959
	[Token(Token = "0x2000306")]
	[StructLayout(3)]
	public class BattleOption
	{
		// Token: 0x06000D9B RID: 3483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C7F")]
		[Address(RVA = "0x10112C420", Offset = "0x112C420", VA = "0x10112C420")]
		public void Setup(BattleTimeScaler scaler, LocalSaveData localSaveData)
		{
		}

		// Token: 0x06000D9C RID: 3484 RVA: 0x00005A18 File Offset: 0x00003C18
		[Token(Token = "0x6000C80")]
		[Address(RVA = "0x10112C65C", Offset = "0x112C65C", VA = "0x10112C65C")]
		public bool GetIsAuto()
		{
			return default(bool);
		}

		// Token: 0x06000D9D RID: 3485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C81")]
		[Address(RVA = "0x10112C664", Offset = "0x112C664", VA = "0x10112C664")]
		public void SetIsAuto(bool flg)
		{
		}

		// Token: 0x06000D9E RID: 3486 RVA: 0x00005A30 File Offset: 0x00003C30
		[Token(Token = "0x6000C82")]
		[Address(RVA = "0x10112C6B4", Offset = "0x112C6B4", VA = "0x10112C6B4")]
		public bool ToggleAuto()
		{
			return default(bool);
		}

		// Token: 0x06000D9F RID: 3487 RVA: 0x00005A48 File Offset: 0x00003C48
		[Token(Token = "0x6000C83")]
		[Address(RVA = "0x10112C6E4", Offset = "0x112C6E4", VA = "0x10112C6E4")]
		public bool GetBattleSpeedLvMasterFlg()
		{
			return default(bool);
		}

		// Token: 0x06000DA0 RID: 3488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C84")]
		[Address(RVA = "0x10112C6F8", Offset = "0x112C6F8", VA = "0x10112C6F8")]
		public void SetBattleSpeedLvMasterFlg(bool flg)
		{
		}

		// Token: 0x06000DA1 RID: 3489 RVA: 0x00005A60 File Offset: 0x00003C60
		[Token(Token = "0x6000C85")]
		[Address(RVA = "0x10112C70C", Offset = "0x112C70C", VA = "0x10112C70C")]
		public bool ToggleBattleSpeedLvMasterFlg()
		{
			return default(bool);
		}

		// Token: 0x06000DA2 RID: 3490 RVA: 0x00005A78 File Offset: 0x00003C78
		[Token(Token = "0x6000C86")]
		[Address(RVA = "0x10112C50C", Offset = "0x112C50C", VA = "0x10112C50C")]
		public int GetSpeedLv(LocalSaveData.eBattleSpeedLv type)
		{
			return 0;
		}

		// Token: 0x06000DA3 RID: 3491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C87")]
		[Address(RVA = "0x10112C520", Offset = "0x112C520", VA = "0x10112C520")]
		public void SetSpeedLv(LocalSaveData.eBattleSpeedLv type, int lv, bool isDirtyUpdate = true)
		{
		}

		// Token: 0x06000DA4 RID: 3492 RVA: 0x00005A90 File Offset: 0x00003C90
		[Token(Token = "0x6000C88")]
		[Address(RVA = "0x10112C720", Offset = "0x112C720", VA = "0x10112C720")]
		public bool GetIsSkipTogetherAttackInput()
		{
			return default(bool);
		}

		// Token: 0x06000DA5 RID: 3493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C89")]
		[Address(RVA = "0x10112C728", Offset = "0x112C728", VA = "0x10112C728")]
		public void SetIsSkipTogetherAttackInput(bool flg)
		{
		}

		// Token: 0x06000DA6 RID: 3494 RVA: 0x00005AA8 File Offset: 0x00003CA8
		[Token(Token = "0x6000C8A")]
		[Address(RVA = "0x10112C740", Offset = "0x112C740", VA = "0x10112C740")]
		public bool ToggleIsSkipTogetherAttackInput()
		{
			return default(bool);
		}

		// Token: 0x06000DA7 RID: 3495 RVA: 0x00005AC0 File Offset: 0x00003CC0
		[Token(Token = "0x6000C8B")]
		[Address(RVA = "0x10112C78C", Offset = "0x112C78C", VA = "0x10112C78C")]
		public bool GetIsUseSkillInAuto()
		{
			return default(bool);
		}

		// Token: 0x06000DA8 RID: 3496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C8C")]
		[Address(RVA = "0x10112C794", Offset = "0x112C794", VA = "0x10112C794")]
		public void SetIsUseSkillInAuto(bool flg)
		{
		}

		// Token: 0x06000DA9 RID: 3497 RVA: 0x00005AD8 File Offset: 0x00003CD8
		[Token(Token = "0x6000C8D")]
		[Address(RVA = "0x10112C7AC", Offset = "0x112C7AC", VA = "0x10112C7AC")]
		public bool ToggleIsUseSkillInAuto()
		{
			return default(bool);
		}

		// Token: 0x06000DAA RID: 3498 RVA: 0x00005AF0 File Offset: 0x00003CF0
		[Token(Token = "0x6000C8E")]
		[Address(RVA = "0x10112C7F8", Offset = "0x112C7F8", VA = "0x10112C7F8")]
		public LocalSaveData.eBattleAutoCancelType GetAutoCancel()
		{
			return LocalSaveData.eBattleAutoCancelType.ButtonOnly;
		}

		// Token: 0x06000DAB RID: 3499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C8F")]
		[Address(RVA = "0x10112C800", Offset = "0x112C800", VA = "0x10112C800")]
		public void SetAutoCancel(LocalSaveData.eBattleAutoCancelType value)
		{
		}

		// Token: 0x06000DAC RID: 3500 RVA: 0x00005B08 File Offset: 0x00003D08
		[Token(Token = "0x6000C90")]
		[Address(RVA = "0x10112C818", Offset = "0x112C818", VA = "0x10112C818")]
		public bool GetIsSkipUniqueSkill()
		{
			return default(bool);
		}

		// Token: 0x06000DAD RID: 3501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C91")]
		[Address(RVA = "0x10112C858", Offset = "0x112C858", VA = "0x10112C858")]
		public void SetIsSkipUniqueSkill(bool flg)
		{
		}

		// Token: 0x06000DAE RID: 3502 RVA: 0x00005B20 File Offset: 0x00003D20
		[Token(Token = "0x6000C92")]
		[Address(RVA = "0x10112C870", Offset = "0x112C870", VA = "0x10112C870")]
		public bool ToggleIsSkipUniqueSkill()
		{
			return default(bool);
		}

		// Token: 0x06000DAF RID: 3503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C93")]
		[Address(RVA = "0x10112C8BC", Offset = "0x112C8BC", VA = "0x10112C8BC")]
		public BattleOption()
		{
		}

		// Token: 0x04000FE0 RID: 4064
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000BED")]
		private LocalSaveData m_LocalSaveData;

		// Token: 0x04000FE1 RID: 4065
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BEE")]
		private BattleTimeScaler m_Scaler;

		// Token: 0x04000FE2 RID: 4066
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BEF")]
		private bool m_IsAuto;

		// Token: 0x04000FE3 RID: 4067
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4000BF0")]
		private bool m_IsSkipTogetherAttackInput;

		// Token: 0x04000FE4 RID: 4068
		[Cpp2IlInjected.FieldOffset(Offset = "0x22")]
		[Token(Token = "0x4000BF1")]
		private bool m_IsUseSkillInAuto;

		// Token: 0x04000FE5 RID: 4069
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000BF2")]
		private LocalSaveData.eBattleAutoCancelType m_AutoCancel;

		// Token: 0x04000FE6 RID: 4070
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BF3")]
		private bool m_IsSkipUniqueSkill;
	}
}
