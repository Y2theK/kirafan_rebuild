﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003C0 RID: 960
	[Token(Token = "0x2000307")]
	[StructLayout(3)]
	public class BattleOrder
	{
		// Token: 0x06000DB0 RID: 3504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C94")]
		[Address(RVA = "0x10112C8D0", Offset = "0x112C8D0", VA = "0x10112C8D0")]
		public BattleOrder(BattleTogetherGauge gauge)
		{
		}

		// Token: 0x06000DB1 RID: 3505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C95")]
		[Address(RVA = "0x10112C9AC", Offset = "0x112C9AC", VA = "0x10112C9AC")]
		public void Reset()
		{
		}

		// Token: 0x06000DB2 RID: 3506 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C96")]
		[Address(RVA = "0x10112CA14", Offset = "0x112CA14", VA = "0x10112CA14")]
		public BattleOrder.TogetherData GetTogetherData()
		{
			return null;
		}

		// Token: 0x06000DB3 RID: 3507 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000C97")]
		[Address(RVA = "0x10112CA1C", Offset = "0x112CA1C", VA = "0x10112CA1C")]
		public BattleTogetherGauge GetGauge()
		{
			return null;
		}

		// Token: 0x06000DB4 RID: 3508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C98")]
		[Address(RVA = "0x10112CA24", Offset = "0x112CA24", VA = "0x10112CA24")]
		public void AddFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
		{
		}

		// Token: 0x06000DB5 RID: 3509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C99")]
		[Address(RVA = "0x10112CB30", Offset = "0x112CB30", VA = "0x10112CB30")]
		public void InsertFrameData(int index, BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
		{
		}

		// Token: 0x06000DB6 RID: 3510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C9A")]
		[Address(RVA = "0x10112CBF0", Offset = "0x112CBF0", VA = "0x10112CBF0")]
		public void RemoveFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, bool isRemoveIsFirstOnly = false)
		{
		}

		// Token: 0x06000DB7 RID: 3511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C9B")]
		[Address(RVA = "0x10112CE70", Offset = "0x112CE70", VA = "0x10112CE70")]
		public void RemoveFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, out int out_removeIndex, out float out_orderValue)
		{
		}

		// Token: 0x06000DB8 RID: 3512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C9C")]
		[Address(RVA = "0x10112D030", Offset = "0x112D030", VA = "0x10112D030")]
		public void RemoveFrameDataAt(int index)
		{
		}

		// Token: 0x06000DB9 RID: 3513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C9D")]
		[Address(RVA = "0x10112D0A0", Offset = "0x112D0A0", VA = "0x10112D0A0")]
		public void Shuffle()
		{
		}

		// Token: 0x06000DBA RID: 3514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000C9E")]
		[Address(RVA = "0x10112D218", Offset = "0x112D218", VA = "0x10112D218")]
		public void SortOrder()
		{
		}

		// Token: 0x06000DBB RID: 3515 RVA: 0x00005B38 File Offset: 0x00003D38
		[Token(Token = "0x6000C9F")]
		[Address(RVA = "0x10112D2A4", Offset = "0x112D2A4", VA = "0x10112D2A4")]
		private static int CompareFrame(BattleOrder.FrameData A, BattleOrder.FrameData B)
		{
			return 0;
		}

		// Token: 0x06000DBC RID: 3516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CA0")]
		[Address(RVA = "0x10112D308", Offset = "0x112D308", VA = "0x10112D308")]
		public void UpdateOrderValue()
		{
		}

		// Token: 0x06000DBD RID: 3517 RVA: 0x00005B50 File Offset: 0x00003D50
		[Token(Token = "0x6000CA1")]
		[Address(RVA = "0x10112D51C", Offset = "0x112D51C", VA = "0x10112D51C")]
		public int GetFrameNum()
		{
			return 0;
		}

		// Token: 0x06000DBE RID: 3518 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CA2")]
		[Address(RVA = "0x10112D468", Offset = "0x112D468", VA = "0x10112D468")]
		public BattleOrder.FrameData GetFrameDataAt(int index)
		{
			return null;
		}

		// Token: 0x06000DBF RID: 3519 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CA3")]
		[Address(RVA = "0x10112D57C", Offset = "0x112D57C", VA = "0x10112D57C")]
		public CharacterHandler GetFrameOwnerAt(int index)
		{
			return null;
		}

		// Token: 0x06000DC0 RID: 3520 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CA4")]
		[Address(RVA = "0x10112D638", Offset = "0x112D638", VA = "0x10112D638")]
		public CharacterHandler GetTurnOwner()
		{
			return null;
		}

		// Token: 0x06000DC1 RID: 3521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CA5")]
		[Address(RVA = "0x10112D640", Offset = "0x112D640", VA = "0x10112D640")]
		public BattleOrder.FrameData FindCardFrameData(CharacterHandler executor, int skillID, bool isMasterSkill)
		{
			return null;
		}

		// Token: 0x06000DC2 RID: 3522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CA6")]
		[Address(RVA = "0x10112D894", Offset = "0x112D894", VA = "0x10112D894")]
		public void SaveFrames()
		{
		}

		// Token: 0x06000DC3 RID: 3523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CA7")]
		[Address(RVA = "0x10112DA74", Offset = "0x112DA74", VA = "0x10112DA74")]
		public void RevertFrames()
		{
		}

		// Token: 0x06000DC4 RID: 3524 RVA: 0x00005B68 File Offset: 0x00003D68
		[Token(Token = "0x6000CA8")]
		[Address(RVA = "0x10112DC54", Offset = "0x112DC54", VA = "0x10112DC54")]
		public int MoveTopFrameWhileFixing(float newOrderValue, bool isRealMove)
		{
			return 0;
		}

		// Token: 0x06000DC5 RID: 3525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CA9")]
		[Address(RVA = "0x10112DDF4", Offset = "0x112DDF4", VA = "0x10112DDF4")]
		public void SlideFrames()
		{
		}

		// Token: 0x06000DC6 RID: 3526 RVA: 0x00005B80 File Offset: 0x00003D80
		[Token(Token = "0x6000CAA")]
		[Address(RVA = "0x10112DE58", Offset = "0x112DE58", VA = "0x10112DE58")]
		public bool CanBeTogehterAttack()
		{
			return default(bool);
		}

		// Token: 0x06000DC7 RID: 3527 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CAB")]
		[Address(RVA = "0x10112DEE0", Offset = "0x112DEE0", VA = "0x10112DEE0")]
		public List<BattleDefine.eJoinMember> CalcTogetherAttackDefaultOrders()
		{
			return null;
		}

		// Token: 0x06000DC8 RID: 3528 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CAC")]
		[Address(RVA = "0x10112DF10", Offset = "0x112DF10", VA = "0x10112DF10")]
		public List<BattleDefine.eJoinMember> CalcTogetherAttackDefaultOrders(out int out_togetherTopIndex, out int out_togetherContiguousNum, bool includeTop = true)
		{
			return null;
		}

		// Token: 0x06000DC9 RID: 3529 RVA: 0x00005B98 File Offset: 0x00003D98
		[Token(Token = "0x6000CAD")]
		[Address(RVA = "0x10112E314", Offset = "0x112E314", VA = "0x10112E314")]
		public bool GetTogetherAttackExecutable()
		{
			return default(bool);
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CAE")]
		[Address(RVA = "0x10112E31C", Offset = "0x112E31C", VA = "0x10112E31C")]
		public void SetTogetherAttackExecutable(bool isExecutableTogetherAttack)
		{
		}

		// Token: 0x04000FE7 RID: 4071
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000BF4")]
		private BattleOrder.TogetherData m_TogetherData;

		// Token: 0x04000FE8 RID: 4072
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BF5")]
		private BattleTogetherGauge m_Gauge;

		// Token: 0x04000FE9 RID: 4073
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000BF6")]
		private List<BattleOrder.FrameData> m_Frames;

		// Token: 0x04000FEA RID: 4074
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000BF7")]
		private List<BattleOrder.FrameData> m_SaveFrames;

		// Token: 0x04000FEB RID: 4075
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000BF8")]
		private bool m_IsExecutableTogetherAttack;

		// Token: 0x020003C1 RID: 961
		[Token(Token = "0x2000D8D")]
		public class TogetherData
		{
			// Token: 0x06000DCB RID: 3531 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DE0")]
			[Address(RVA = "0x10112C97C", Offset = "0x112C97C", VA = "0x10112C97C")]
			public TogetherData()
			{
			}

			// Token: 0x06000DCC RID: 3532 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DE1")]
			[Address(RVA = "0x10112E514", Offset = "0x112E514", VA = "0x10112E514")]
			public void SetupOnTurnStart(bool executable)
			{
			}

			// Token: 0x06000DCD RID: 3533 RVA: 0x00005BB0 File Offset: 0x00003DB0
			[Token(Token = "0x6005DE2")]
			[Address(RVA = "0x10112E530", Offset = "0x112E530", VA = "0x10112E530")]
			public bool IsAvailable()
			{
				return default(bool);
			}

			// Token: 0x06000DCE RID: 3534 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DE3")]
			[Address(RVA = "0x10112E538", Offset = "0x112E538", VA = "0x10112E538")]
			public void SetAvailable(bool flg)
			{
			}

			// Token: 0x06000DCF RID: 3535 RVA: 0x00005BC8 File Offset: 0x00003DC8
			[Token(Token = "0x6005DE4")]
			[Address(RVA = "0x10112E540", Offset = "0x112E540", VA = "0x10112E540")]
			public bool Executable()
			{
				return default(bool);
			}

			// Token: 0x06000DD0 RID: 3536 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DE5")]
			[Address(RVA = "0x10112E528", Offset = "0x112E528", VA = "0x10112E528")]
			public void SetExecutable(bool executable)
			{
			}

			// Token: 0x06000DD1 RID: 3537 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005DE6")]
			[Address(RVA = "0x10112E548", Offset = "0x112E548", VA = "0x10112E548")]
			public List<BattleDefine.eJoinMember> GetJoinOrder()
			{
				return null;
			}

			// Token: 0x06000DD2 RID: 3538 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DE7")]
			[Address(RVA = "0x10112E550", Offset = "0x112E550", VA = "0x10112E550")]
			public void SetJoinOrder(List<BattleDefine.eJoinMember> joinOrder)
			{
			}

			// Token: 0x06000DD3 RID: 3539 RVA: 0x00005BE0 File Offset: 0x00003DE0
			[Token(Token = "0x6005DE8")]
			[Address(RVA = "0x10112E558", Offset = "0x112E558", VA = "0x10112E558")]
			public int GetCount()
			{
				return 0;
			}

			// Token: 0x06000DD4 RID: 3540 RVA: 0x00005BF8 File Offset: 0x00003DF8
			[Token(Token = "0x6005DE9")]
			[Address(RVA = "0x10112E560", Offset = "0x112E560", VA = "0x10112E560")]
			public int IncreaseCount()
			{
				return 0;
			}

			// Token: 0x06000DD5 RID: 3541 RVA: 0x00005C10 File Offset: 0x00003E10
			[Token(Token = "0x6005DEA")]
			[Address(RVA = "0x10112E574", Offset = "0x112E574", VA = "0x10112E574")]
			public bool IsEnd()
			{
				return default(bool);
			}

			// Token: 0x04000FEC RID: 4076
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005774")]
			private bool m_IsAvailable;

			// Token: 0x04000FED RID: 4077
			[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
			[Token(Token = "0x4005775")]
			private bool m_Executable;

			// Token: 0x04000FEE RID: 4078
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005776")]
			private List<BattleDefine.eJoinMember> m_JoinOrder;

			// Token: 0x04000FEF RID: 4079
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005777")]
			private int m_Count;
		}

		// Token: 0x020003C2 RID: 962
		[Token(Token = "0x2000D8E")]
		public enum eFrameType
		{
			// Token: 0x04000FF1 RID: 4081
			[Token(Token = "0x4005779")]
			Chara,
			// Token: 0x04000FF2 RID: 4082
			[Token(Token = "0x400577A")]
			Card
		}

		// Token: 0x020003C3 RID: 963
		[Token(Token = "0x2000D8F")]
		public class CardArguments
		{
			// Token: 0x170000B6 RID: 182
			// (get) Token: 0x06000DD6 RID: 3542 RVA: 0x00005C28 File Offset: 0x00003E28
			[Token(Token = "0x17000688")]
			public eSkillContentType SkillContentType
			{
				[Token(Token = "0x6005DEB")]
				[Address(RVA = "0x10112E324", Offset = "0x112E324", VA = "0x10112E324")]
				get
				{
					return eSkillContentType.Attack;
				}
			}

			// Token: 0x06000DD7 RID: 3543 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DEC")]
			[Address(RVA = "0x10112E414", Offset = "0x112E414", VA = "0x10112E414")]
			public CardArguments(int aliveNum, int refID, int createdSkillId, BattleCommandData command, bool isMasterSkill)
			{
			}

			// Token: 0x04000FF3 RID: 4083
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400577B")]
			public int m_AliveNum;

			// Token: 0x04000FF4 RID: 4084
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400577C")]
			public int m_RefID;

			// Token: 0x04000FF5 RID: 4085
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400577D")]
			public BattleCommandData m_Command;

			// Token: 0x04000FF6 RID: 4086
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400577E")]
			public int m_CreatedSkillID;

			// Token: 0x04000FF7 RID: 4087
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400577F")]
			public bool m_isCreatedByMasterSkill;
		}

		// Token: 0x020003C4 RID: 964
		[Token(Token = "0x2000D90")]
		public class FrameData
		{
			// Token: 0x170000B7 RID: 183
			// (get) Token: 0x06000DD8 RID: 3544 RVA: 0x00005C40 File Offset: 0x00003E40
			[Token(Token = "0x17000689")]
			public bool IsCharaType
			{
				[Token(Token = "0x6005DED")]
				[Address(RVA = "0x10112E46C", Offset = "0x112E46C", VA = "0x10112E46C")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x170000B8 RID: 184
			// (get) Token: 0x06000DD9 RID: 3545 RVA: 0x00005C58 File Offset: 0x00003E58
			[Token(Token = "0x1700068A")]
			public bool IsCardType
			{
				[Token(Token = "0x6005DEE")]
				[Address(RVA = "0x10112D884", Offset = "0x112D884", VA = "0x10112D884")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x06000DDA RID: 3546 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DEF")]
			[Address(RVA = "0x10112CADC", Offset = "0x112CADC", VA = "0x10112CADC")]
			public FrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
			{
			}

			// Token: 0x06000DDB RID: 3547 RVA: 0x00005C70 File Offset: 0x00003E70
			[Token(Token = "0x6005DF0")]
			[Address(RVA = "0x10112E47C", Offset = "0x112E47C", VA = "0x10112E47C")]
			public bool CompareOwnerCharaOnly(CharacterHandler charaHndl)
			{
				return default(bool);
			}

			// Token: 0x04000FF8 RID: 4088
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005780")]
			public BattleOrder.eFrameType m_FrameType;

			// Token: 0x04000FF9 RID: 4089
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005781")]
			public CharacterHandler m_Owner;

			// Token: 0x04000FFA RID: 4090
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005782")]
			public float m_OrderValue;

			// Token: 0x04000FFB RID: 4091
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005783")]
			public BattleOrder.CardArguments m_CardArgs;
		}
	}
}
