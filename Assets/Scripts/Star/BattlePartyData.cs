﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003C5 RID: 965
	[Token(Token = "0x2000308")]
	[StructLayout(3)]
	public class BattlePartyData
	{
		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000DDC RID: 3548 RVA: 0x00005C88 File Offset: 0x00003E88
		// (set) Token: 0x06000DDD RID: 3549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000A4")]
		public BattleDefine.ePartyType PartyType
		{
			[Token(Token = "0x6000CAF")]
			[Address(RVA = "0x10112E5E4", Offset = "0x112E5E4", VA = "0x10112E5E4")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.ePartyType.Player;
			}
			[Token(Token = "0x6000CB0")]
			[Address(RVA = "0x10112E5EC", Offset = "0x112E5EC", VA = "0x10112E5EC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000DDE RID: 3550 RVA: 0x00005CA0 File Offset: 0x00003EA0
		// (set) Token: 0x06000DDF RID: 3551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000A5")]
		public BattleDefine.eJoinMember SingleAtkTargetIndex
		{
			[Token(Token = "0x6000CB1")]
			[Address(RVA = "0x10112E5F4", Offset = "0x112E5F4", VA = "0x10112E5F4")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.eJoinMember.Join_1;
			}
			[Token(Token = "0x6000CB2")]
			[Address(RVA = "0x10112E5FC", Offset = "0x112E5FC", VA = "0x10112E5FC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000DE0 RID: 3552 RVA: 0x00005CB8 File Offset: 0x00003EB8
		// (set) Token: 0x06000DE1 RID: 3553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000A6")]
		public BattleDefine.eJoinMember SingleDefTargetIndex
		{
			[Token(Token = "0x6000CB3")]
			[Address(RVA = "0x10112E604", Offset = "0x112E604", VA = "0x10112E604")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.eJoinMember.Join_1;
			}
			[Token(Token = "0x6000CB4")]
			[Address(RVA = "0x10112E60C", Offset = "0x112E60C", VA = "0x10112E60C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CB5")]
		[Address(RVA = "0x10112E614", Offset = "0x112E614", VA = "0x10112E614")]
		public void SetSingleTargetIndex(BattleDefine.eJoinMember index, eSkillTargetType skillType)
		{
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x00005CD0 File Offset: 0x00003ED0
		[Token(Token = "0x6000CB6")]
		[Address(RVA = "0x10112E640", Offset = "0x112E640", VA = "0x10112E640")]
		public BattleDefine.eJoinMember GetSingleTargetIndex(eSkillTargetType skillType)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000DE4 RID: 3556 RVA: 0x00005CE8 File Offset: 0x00003EE8
		// (set) Token: 0x06000DE5 RID: 3557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000A7")]
		public long PartyMngID
		{
			[Token(Token = "0x6000CB7")]
			[Address(RVA = "0x10112E670", Offset = "0x112E670", VA = "0x10112E670")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6000CB8")]
			[Address(RVA = "0x10112E678", Offset = "0x112E678", VA = "0x10112E678")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000DE6 RID: 3558 RVA: 0x00005D00 File Offset: 0x00003F00
		[Token(Token = "0x170000A8")]
		public bool IsPlayerParty
		{
			[Token(Token = "0x6000CB9")]
			[Address(RVA = "0x10112E2F4", Offset = "0x112E2F4", VA = "0x10112E2F4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000DE7 RID: 3559 RVA: 0x00005D18 File Offset: 0x00003F18
		[Token(Token = "0x170000A9")]
		public bool IsEnemyParty
		{
			[Token(Token = "0x6000CBA")]
			[Address(RVA = "0x10112E304", Offset = "0x112E304", VA = "0x10112E304")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CBB")]
		[Address(RVA = "0x10112E680", Offset = "0x112E680", VA = "0x10112E680")]
		public void SetBattlePartyParam(BattleSystemDataForPLPT param)
		{
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CBC")]
		[Address(RVA = "0x10112E6B4", Offset = "0x112E6B4", VA = "0x10112E6B4")]
		public void SetBattleEnemyPartyParam()
		{
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CBD")]
		[Address(RVA = "0x10112E740", Offset = "0x112E740", VA = "0x10112E740")]
		public BattlePartyData(BattleDefine.ePartyType partyType, int[] joinMemberIndices)
		{
		}

		// Token: 0x06000DEB RID: 3563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CBE")]
		[Address(RVA = "0x10112E8CC", Offset = "0x112E8CC", VA = "0x10112E8CC")]
		public void Destroy()
		{
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CBF")]
		[Address(RVA = "0x10112EAEC", Offset = "0x112EAEC", VA = "0x10112EAEC")]
		public void SetMemberAt(int slotIndex, CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000DED RID: 3565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CC0")]
		[Address(RVA = "0x10112EB78", Offset = "0x112EB78", VA = "0x10112EB78")]
		public void SetJoinMemberIndex(BattleDefine.eJoinMember join, int slotIndex)
		{
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x00005D30 File Offset: 0x00003F30
		[Token(Token = "0x6000CC1")]
		[Address(RVA = "0x10112ED4C", Offset = "0x112ED4C", VA = "0x10112ED4C")]
		public BattleDefine.eJoinMember SlotIndexToJoin(int slotIndex)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC2")]
		[Address(RVA = "0x10112EC90", Offset = "0x112EC90", VA = "0x10112EC90")]
		public CharacterHandler GetMember(BattleDefine.eJoinMember join)
		{
			return null;
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC3")]
		[Address(RVA = "0x10112EDE4", Offset = "0x112EDE4", VA = "0x10112EDE4")]
		public CharacterHandler GetMemberAt(int slotIndex)
		{
			return null;
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC4")]
		[Address(RVA = "0x10112EE34", Offset = "0x112EE34", VA = "0x10112EE34")]
		public List<CharacterHandler> GetAvailableMembers()
		{
			return null;
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC5")]
		[Address(RVA = "0x10112EFA0", Offset = "0x112EFA0", VA = "0x10112EFA0")]
		public List<int> GetAvailableCharaIDs()
		{
			return null;
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC6")]
		[Address(RVA = "0x10112F140", Offset = "0x112F140", VA = "0x10112F140")]
		public List<int> GetAvailableViewCharaIDs()
		{
			return null;
		}

		// Token: 0x06000DF4 RID: 3572 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CC7")]
		[Address(RVA = "0x10112F2E0", Offset = "0x112F2E0", VA = "0x10112F2E0")]
		public List<CharacterHandler> GetMembers(List<BattleDefine.eJoinMember> joins)
		{
			return null;
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x00005D48 File Offset: 0x00003F48
		[Token(Token = "0x6000CC8")]
		[Address(RVA = "0x10112F444", Offset = "0x112F444", VA = "0x10112F444")]
		public int GetJoinCharaNum(bool isAliveOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DF6 RID: 3574 RVA: 0x00005D60 File Offset: 0x00003F60
		[Token(Token = "0x6000CC9")]
		[Address(RVA = "0x10112F548", Offset = "0x112F548", VA = "0x10112F548")]
		public int GetAliveCount(bool isIncludeReserve)
		{
			return 0;
		}

		// Token: 0x06000DF7 RID: 3575 RVA: 0x00005D78 File Offset: 0x00003F78
		[Token(Token = "0x6000CCA")]
		[Address(RVA = "0x10112F658", Offset = "0x112F658", VA = "0x10112F658")]
		public int GetStateAbnormalCountAny(bool isJoinOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DF8 RID: 3576 RVA: 0x00005D90 File Offset: 0x00003F90
		[Token(Token = "0x6000CCB")]
		[Address(RVA = "0x10112F7B8", Offset = "0x112F7B8", VA = "0x10112F7B8")]
		public int GetStateAbnormalCountNone(bool isJoinOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DF9 RID: 3577 RVA: 0x00005DA8 File Offset: 0x00003FA8
		[Token(Token = "0x6000CCC")]
		[Address(RVA = "0x10112F91C", Offset = "0x112F91C", VA = "0x10112F91C")]
		public int GetStateAbnormalCountSpecific(eStateAbnormal stateAbnormal, bool isJoinOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x00005DC0 File Offset: 0x00003FC0
		[Token(Token = "0x6000CCD")]
		[Address(RVA = "0x10112FA84", Offset = "0x112FA84", VA = "0x10112FA84")]
		public int GetStateAbnormalCountSpecificNone(eStateAbnormal stateAbnormal, bool isJoinOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x00005DD8 File Offset: 0x00003FD8
		[Token(Token = "0x6000CCE")]
		[Address(RVA = "0x10112FBF0", Offset = "0x112FBF0", VA = "0x10112FBF0")]
		public int GetMemberClassCount(eClassType memberClass, eElementType elemType, bool isJoinOnly = false)
		{
			return 0;
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CCF")]
		[Address(RVA = "0x10112FD98", Offset = "0x112FD98", VA = "0x10112FD98")]
		public List<CharacterHandler> GetJoinMembers(bool isAliveOnly = false)
		{
			return null;
		}

		// Token: 0x06000DFD RID: 3581 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CD0")]
		[Address(RVA = "0x10112FEE4", Offset = "0x112FEE4", VA = "0x10112FEE4")]
		public List<CharacterHandler> GetBenchMembers(bool isAliveOnly = false)
		{
			return null;
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CD1")]
		[Address(RVA = "0x101130034", Offset = "0x1130034", VA = "0x101130034")]
		public List<CharacterHandler> GetJoinClassMembers(eClassType ct, bool isAliveOnly = false)
		{
			return null;
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x00005DF0 File Offset: 0x00003FF0
		[Token(Token = "0x6000CD2")]
		[Address(RVA = "0x1011301C4", Offset = "0x11301C4", VA = "0x1011301C4")]
		public bool IsExistCharaID(int charaID, bool isIncludeFriend = true)
		{
			return default(bool);
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x00005E08 File Offset: 0x00004008
		[Token(Token = "0x6000CD3")]
		[Address(RVA = "0x1011302CC", Offset = "0x11302CC", VA = "0x1011302CC")]
		public bool IsExistFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CD4")]
		[Address(RVA = "0x101130368", Offset = "0x1130368", VA = "0x101130368")]
		public CharacterHandler GetFriend()
		{
			return null;
		}

		// Token: 0x06000E02 RID: 3586 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CD5")]
		[Address(RVA = "0x1011303B0", Offset = "0x11303B0", VA = "0x1011303B0")]
		public CharacterHandler FindMemberByName(string gameObjectName)
		{
			return null;
		}

		// Token: 0x06000E03 RID: 3587 RVA: 0x00005E20 File Offset: 0x00004020
		[Token(Token = "0x6000CD6")]
		[Address(RVA = "0x1011304A0", Offset = "0x11304A0", VA = "0x1011304A0")]
		public bool CanChangeJoinMember()
		{
			return default(bool);
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x00005E38 File Offset: 0x00004038
		[Token(Token = "0x6000CD7")]
		[Address(RVA = "0x1011305A4", Offset = "0x11305A4", VA = "0x1011305A4")]
		public bool ChangeJoinMember(BattleDefine.eJoinMember join, BattleDefine.eJoinMember bench)
		{
			return default(bool);
		}

		// Token: 0x06000E05 RID: 3589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CD8")]
		[Address(RVA = "0x1011307D0", Offset = "0x11307D0", VA = "0x1011307D0")]
		public void WavePreProcess(int waveIndex)
		{
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x00005E50 File Offset: 0x00004050
		[Token(Token = "0x6000CD9")]
		[Address(RVA = "0x1011309C8", Offset = "0x11309C8", VA = "0x1011309C8")]
		public bool CheckDeadAll()
		{
			return default(bool);
		}

		// Token: 0x06000E07 RID: 3591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CDA")]
		[Address(RVA = "0x101130BFC", Offset = "0x1130BFC", VA = "0x101130BFC")]
		public void TurnStartProcess(CharacterHandler turnOwner)
		{
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CDB")]
		[Address(RVA = "0x101130D70", Offset = "0x1130D70", VA = "0x101130D70")]
		public void TurnEndProcess(CharacterHandler turnOwner)
		{
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CDC")]
		[Address(RVA = "0x101130F2C", Offset = "0x1130F2C", VA = "0x101130F2C")]
		public void ResetOnRetry()
		{
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CDD")]
		[Address(RVA = "0x101131038", Offset = "0x1131038", VA = "0x101131038")]
		public void SetTogetherAttackChainCoefChange(float togetherAttackChainCoefChange)
		{
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x00005E68 File Offset: 0x00004068
		[Token(Token = "0x6000CDE")]
		[Address(RVA = "0x101131098", Offset = "0x1131098", VA = "0x101131098")]
		public float GetTogetherAttackChainCoefChange()
		{
			return 0f;
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CDF")]
		[Address(RVA = "0x10113100C", Offset = "0x113100C", VA = "0x10113100C")]
		public void ResetTogetherAttackChainCoefChange()
		{
		}

		// Token: 0x06000E0D RID: 3597 RVA: 0x00005E80 File Offset: 0x00004080
		[Token(Token = "0x6000CE0")]
		[Address(RVA = "0x1011310C4", Offset = "0x11310C4", VA = "0x1011310C4")]
		public int GetActionCount(BattlePartyData.eActionCountType actionCountType)
		{
			return 0;
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CE1")]
		[Address(RVA = "0x101131114", Offset = "0x1131114", VA = "0x101131114")]
		public void IncrementActionCount(BattlePartyData.eActionCountType actionCountType)
		{
		}

		// Token: 0x06000E0F RID: 3599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CE2")]
		[Address(RVA = "0x10113116C", Offset = "0x113116C", VA = "0x10113116C")]
		public void RequestResetActionCount(BattlePartyData.eActionCountType actionCountType)
		{
		}

		// Token: 0x06000E10 RID: 3600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CE3")]
		[Address(RVA = "0x101130E50", Offset = "0x1130E50", VA = "0x101130E50")]
		private void ResetRequestedActionCounts()
		{
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CE4")]
		[Address(RVA = "0x101130924", Offset = "0x1130924", VA = "0x101130924")]
		public void ResetAllActionCountDatas()
		{
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x00005E98 File Offset: 0x00004098
		[Token(Token = "0x6000CE5")]
		[Address(RVA = "0x1011311C0", Offset = "0x11311C0", VA = "0x1011311C0")]
		public bool IsExistPassiveSkill()
		{
			return default(bool);
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CE6")]
		[Address(RVA = "0x1011312AC", Offset = "0x11312AC", VA = "0x1011312AC")]
		public Dictionary<CharacterHandler, List<BattlePassiveSkillSolve>> SolvePassiveSkills(ePassiveSkillTrigger trigger)
		{
			return null;
		}

		// Token: 0x06000E14 RID: 3604 RVA: 0x00005EB0 File Offset: 0x000040B0
		[Token(Token = "0x6000CE7")]
		[Address(RVA = "0x1011313D4", Offset = "0x11313D4", VA = "0x1011313D4")]
		public bool PlayVoice(string cueName)
		{
			return default(bool);
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x00005EC8 File Offset: 0x000040C8
		[Token(Token = "0x6000CE8")]
		[Address(RVA = "0x1011315E8", Offset = "0x11315E8", VA = "0x1011315E8")]
		public bool PlayVoice(eSoundVoiceListDB cueID)
		{
			return default(bool);
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x00005EE0 File Offset: 0x000040E0
		[Token(Token = "0x6000CE9")]
		[Address(RVA = "0x1011317FC", Offset = "0x11317FC", VA = "0x1011317FC")]
		public bool IsPlayingVoice()
		{
			return default(bool);
		}

		// Token: 0x06000E17 RID: 3607 RVA: 0x00005EF8 File Offset: 0x000040F8
		[Token(Token = "0x6000CEA")]
		[Address(RVA = "0x1011318D0", Offset = "0x11318D0", VA = "0x1011318D0")]
		public bool PrepareEffect()
		{
			return default(bool);
		}

		// Token: 0x06000E18 RID: 3608 RVA: 0x00005F10 File Offset: 0x00004110
		[Token(Token = "0x6000CEB")]
		[Address(RVA = "0x1011322DC", Offset = "0x11322DC", VA = "0x1011322DC")]
		public bool IsPreparingEffect()
		{
			return default(bool);
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CEC")]
		[Address(RVA = "0x101131A6C", Offset = "0x1131A6C", VA = "0x101131A6C")]
		private string[] GetEffectIDFromSkillAction()
		{
			return null;
		}

		// Token: 0x06000E1A RID: 3610 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CED")]
		[Address(RVA = "0x101131F6C", Offset = "0x1131F6C", VA = "0x101131F6C")]
		private string[] SetupAttachEffects()
		{
			return null;
		}

		// Token: 0x06000E1B RID: 3611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CEE")]
		[Address(RVA = "0x101132354", Offset = "0x1132354", VA = "0x101132354")]
		public void ShowAttachEffects()
		{
		}

		// Token: 0x04000FFC RID: 4092
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000BF9")]
		private CharacterHandler[] m_Members;

		// Token: 0x04000FFD RID: 4093
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000BFA")]
		private int[] m_JoinMemberIndices;

		// Token: 0x04001002 RID: 4098
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000BFF")]
		private BattlePartyDataParam m_Param;

		// Token: 0x04001003 RID: 4099
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000C00")]
		private BattlePartyData.ActionCountData[] m_ActionCountDatas;

		// Token: 0x020003C6 RID: 966
		[Token(Token = "0x2000D91")]
		public enum eActionCountType
		{
			// Token: 0x04001005 RID: 4101
			[Token(Token = "0x4005785")]
			NormalAttackUseCount,
			// Token: 0x04001006 RID: 4102
			[Token(Token = "0x4005786")]
			SkillUseCount,
			// Token: 0x04001007 RID: 4103
			[Token(Token = "0x4005787")]
			TogetherAttackUseCount,
			// Token: 0x04001008 RID: 4104
			[Token(Token = "0x4005788")]
			MemberChangeCount,
			// Token: 0x04001009 RID: 4105
			[Token(Token = "0x4005789")]
			Num
		}

		// Token: 0x020003C7 RID: 967
		[Token(Token = "0x2000D92")]
		public struct ActionCountData
		{
			// Token: 0x0400100A RID: 4106
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400578A")]
			public int m_Count;

			// Token: 0x0400100B RID: 4107
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x400578B")]
			public bool m_RequestResetFlg;
		}
	}
}
