﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003DA RID: 986
	[Token(Token = "0x200030E")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePartyDataParam
	{
		// Token: 0x06000F0F RID: 3855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE1")]
		[Address(RVA = "0x10112E718", Offset = "0x112E718", VA = "0x10112E718")]
		public BattlePartyDataParam()
		{
		}

		// Token: 0x04001163 RID: 4451
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CA6")]
		public float TogetherChainBoost;
	}
}
