﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003F4 RID: 1012
	[Token(Token = "0x200031B")]
	[Serializable]
	[StructLayout(3)]
	public sealed class BattlePassiveSkill
	{
		// Token: 0x06000FAB RID: 4011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E7B")]
		[Address(RVA = "0x101132460", Offset = "0x1132460", VA = "0x101132460")]
		public BattlePassiveSkill(List<BattlePassiveSkill.Source> sources, List<BattlePassiveSkillSolve> solves)
		{
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x00006AE0 File Offset: 0x00004CE0
		[Token(Token = "0x6000E7C")]
		[Address(RVA = "0x1011324E8", Offset = "0x11324E8", VA = "0x1011324E8")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E7D")]
		[Address(RVA = "0x101132554", Offset = "0x1132554", VA = "0x101132554")]
		public List<BattlePassiveSkill.Source> GetSources(BattlePassiveSkill.eSourceType sourceType)
		{
			return null;
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E7E")]
		[Address(RVA = "0x1011326B0", Offset = "0x11326B0", VA = "0x1011326B0")]
		public void ResetSuppress()
		{
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E7F")]
		[Address(RVA = "0x1011327C0", Offset = "0x11327C0", VA = "0x1011327C0")]
		public List<BattlePassiveSkillSolve> GetPassiveSkillSolves()
		{
			return null;
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E80")]
		[Address(RVA = "0x1011327C8", Offset = "0x11327C8", VA = "0x1011327C8")]
		public BattlePassiveSkillSolve FindPassiveSkillSolveFirst(ePassiveSkillContentType type)
		{
			return null;
		}

		// Token: 0x0400123C RID: 4668
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000D19")]
		[NonSerialized]
		private List<BattlePassiveSkill.Source> m_Sources;

		// Token: 0x0400123D RID: 4669
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D1A")]
		[SerializeField]
		public List<BattlePassiveSkillSolve> Solves;

		// Token: 0x020003F5 RID: 1013
		[Token(Token = "0x2000DAD")]
		public enum eSourceType
		{
			// Token: 0x0400123F RID: 4671
			[Token(Token = "0x40058A5")]
			Weapon,
			// Token: 0x04001240 RID: 4672
			[Token(Token = "0x40058A6")]
			Ability
		}

		// Token: 0x020003F6 RID: 1014
		[Token(Token = "0x2000DAE")]
		public class Source
		{
			// Token: 0x06000FB1 RID: 4017 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF4")]
			[Address(RVA = "0x1011328E0", Offset = "0x11328E0", VA = "0x1011328E0")]
			public Source()
			{
			}

			// Token: 0x04001241 RID: 4673
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058A7")]
			public BattlePassiveSkill.eSourceType m_SourceType;

			// Token: 0x04001242 RID: 4674
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40058A8")]
			public int m_SourceValue;

			// Token: 0x04001243 RID: 4675
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058A9")]
			public int m_SkillID;

			// Token: 0x04001244 RID: 4676
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40058AA")]
			public string m_SkillDetail;
		}
	}
}
