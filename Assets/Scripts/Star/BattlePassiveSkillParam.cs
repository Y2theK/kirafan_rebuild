﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003F7 RID: 1015
	[Token(Token = "0x200031C")]
	[Serializable]
	[StructLayout(3)]
	public abstract class BattlePassiveSkillParam
	{
		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000FB2 RID: 4018
		[Token(Token = "0x170000C9")]
		public abstract ePassiveSkillContentType PassiveSkillContentType { [Token(Token = "0x6000E81")] [Address(Slot = "4")] get; }

		// Token: 0x06000FB3 RID: 4019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E82")]
		[Address(RVA = "0x1011328F0", Offset = "0x11328F0", VA = "0x1011328F0")]
		public BattlePassiveSkillParam(bool isAvailable)
		{
		}

		// Token: 0x04001245 RID: 4677
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000D1B")]
		[SerializeField]
		public bool IsAvailable;
	}
}
