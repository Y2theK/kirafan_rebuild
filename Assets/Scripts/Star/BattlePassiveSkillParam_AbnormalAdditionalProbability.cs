﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003F8 RID: 1016
	[Token(Token = "0x200031D")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_AbnormalAdditionalProbability : BattlePassiveSkillParam
	{
		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000FB4 RID: 4020 RVA: 0x00006AF8 File Offset: 0x00004CF8
		[Token(Token = "0x170000CA")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E83")]
			[Address(RVA = "0x10113291C", Offset = "0x113291C", VA = "0x10113291C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E84")]
		[Address(RVA = "0x101132924", Offset = "0x1132924", VA = "0x101132924")]
		public BattlePassiveSkillParam_AbnormalAdditionalProbability(bool isAvailable, float[] abnmlAddProbs)
		{
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x00006B10 File Offset: 0x00004D10
		[Token(Token = "0x6000E85")]
		[Address(RVA = "0x101132960", Offset = "0x1132960", VA = "0x101132960")]
		public float GetAbnormalAdditionalProbability(eStateAbnormal stateAbnormal)
		{
			return 0f;
		}

		// Token: 0x04001246 RID: 4678
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D1C")]
		[SerializeField]
		public float[] AbnmlAddProbs;
	}
}
