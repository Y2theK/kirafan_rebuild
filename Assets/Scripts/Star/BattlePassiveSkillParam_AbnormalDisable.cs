﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003F9 RID: 1017
	[Token(Token = "0x200031E")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_AbnormalDisable : BattlePassiveSkillParam
	{
		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000FB7 RID: 4023 RVA: 0x00006B28 File Offset: 0x00004D28
		[Token(Token = "0x170000CB")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E86")]
			[Address(RVA = "0x1011329B4", Offset = "0x11329B4", VA = "0x1011329B4", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E87")]
		[Address(RVA = "0x1011329BC", Offset = "0x11329BC", VA = "0x1011329BC")]
		public BattlePassiveSkillParam_AbnormalDisable(bool isAvailable)
		{
		}
	}
}
