﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FA RID: 1018
	[Token(Token = "0x200031F")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_AbsorbAttack : BattlePassiveSkillParam
	{
		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000FB9 RID: 4025 RVA: 0x00006B40 File Offset: 0x00004D40
		[Token(Token = "0x170000CC")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E88")]
			[Address(RVA = "0x1011329E8", Offset = "0x11329E8", VA = "0x1011329E8", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E89")]
		[Address(RVA = "0x1011329F0", Offset = "0x11329F0", VA = "0x1011329F0")]
		public BattlePassiveSkillParam_AbsorbAttack(bool isAvailable, float normal, float skill, float weapon, float unique)
		{
		}

		// Token: 0x04001247 RID: 4679
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D1D")]
		[SerializeField]
		public float normalRatio;

		// Token: 0x04001248 RID: 4680
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D1E")]
		[SerializeField]
		public float classRatio;

		// Token: 0x04001249 RID: 4681
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000D1F")]
		[SerializeField]
		public float weaponRatio;

		// Token: 0x0400124A RID: 4682
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000D20")]
		[SerializeField]
		public float uniqueRatio;
	}
}
