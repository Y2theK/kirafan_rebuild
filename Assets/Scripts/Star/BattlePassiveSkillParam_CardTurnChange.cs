﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FB RID: 1019
	[Token(Token = "0x2000320")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_CardTurnChange : BattlePassiveSkillParam
	{
		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000FBB RID: 4027 RVA: 0x00006B58 File Offset: 0x00004D58
		[Token(Token = "0x170000CD")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E8A")]
			[Address(RVA = "0x101132A44", Offset = "0x1132A44", VA = "0x101132A44", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E8B")]
		[Address(RVA = "0x101132A4C", Offset = "0x1132A4C", VA = "0x101132A4C")]
		public BattlePassiveSkillParam_CardTurnChange(bool isAvailable, float val)
		{
		}

		// Token: 0x0400124B RID: 4683
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D21")]
		[SerializeField]
		public int addTurnCount;
	}
}
