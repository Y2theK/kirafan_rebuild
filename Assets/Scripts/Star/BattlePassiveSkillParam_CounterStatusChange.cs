﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FC RID: 1020
	[Token(Token = "0x2000321")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_CounterStatusChange : BattlePassiveSkillParam
	{
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000FBD RID: 4029 RVA: 0x00006B70 File Offset: 0x00004D70
		[Token(Token = "0x170000CE")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E8C")]
			[Address(RVA = "0x101132A8C", Offset = "0x1132A8C", VA = "0x101132A8C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FBE RID: 4030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E8D")]
		[Address(RVA = "0x101132A94", Offset = "0x1132A94", VA = "0x101132A94")]
		public BattlePassiveSkillParam_CounterStatusChange(bool isAvailable, eSkillTargetType skillTarget, eSkillTurnConsume turnConsume, int turnTerm, float[] paramArray)
		{
		}

		// Token: 0x0400124C RID: 4684
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D22")]
		[SerializeField]
		public eSkillTargetType target;

		// Token: 0x0400124D RID: 4685
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D23")]
		[SerializeField]
		public eSkillTurnConsume turnType;

		// Token: 0x0400124E RID: 4686
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000D24")]
		[SerializeField]
		public int turn;

		// Token: 0x0400124F RID: 4687
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000D25")]
		[SerializeField]
		public float[] param;
	}
}
