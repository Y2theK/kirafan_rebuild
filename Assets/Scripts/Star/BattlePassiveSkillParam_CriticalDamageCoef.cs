﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FD RID: 1021
	[Token(Token = "0x2000322")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_CriticalDamageCoef : BattlePassiveSkillParam
	{
		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000FBF RID: 4031 RVA: 0x00006B88 File Offset: 0x00004D88
		[Token(Token = "0x170000CF")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E8E")]
			[Address(RVA = "0x101132AF4", Offset = "0x1132AF4", VA = "0x101132AF4", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FC0 RID: 4032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E8F")]
		[Address(RVA = "0x101132AFC", Offset = "0x1132AFC", VA = "0x101132AFC")]
		public BattlePassiveSkillParam_CriticalDamageCoef(bool isAvailable, float val)
		{
		}

		// Token: 0x04001250 RID: 4688
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D26")]
		[SerializeField]
		public float Val;
	}
}
