﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FE RID: 1022
	[Token(Token = "0x2000323")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_ElementResist : BattlePassiveSkillParam
	{
		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000FC1 RID: 4033 RVA: 0x00006BA0 File Offset: 0x00004DA0
		[Token(Token = "0x170000D0")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E90")]
			[Address(RVA = "0x101132B38", Offset = "0x1132B38", VA = "0x101132B38", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FC2 RID: 4034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E91")]
		[Address(RVA = "0x101132B40", Offset = "0x1132B40", VA = "0x101132B40")]
		public BattlePassiveSkillParam_ElementResist(bool isAvailable, float[] elemResists)
		{
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x00006BB8 File Offset: 0x00004DB8
		[Token(Token = "0x6000E92")]
		[Address(RVA = "0x101132B7C", Offset = "0x1132B7C", VA = "0x101132B7C")]
		public float GetElementResist(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x04001251 RID: 4689
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D27")]
		[SerializeField]
		public float[] ElemResists;
	}
}
