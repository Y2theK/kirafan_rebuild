﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003FF RID: 1023
	[Token(Token = "0x2000324")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_HateChange : BattlePassiveSkillParam
	{
		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000FC4 RID: 4036 RVA: 0x00006BD0 File Offset: 0x00004DD0
		[Token(Token = "0x170000D1")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E93")]
			[Address(RVA = "0x101132BD0", Offset = "0x1132BD0", VA = "0x101132BD0", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FC5 RID: 4037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E94")]
		[Address(RVA = "0x101132BD8", Offset = "0x1132BD8", VA = "0x101132BD8")]
		public BattlePassiveSkillParam_HateChange(bool isAvailable, float val)
		{
		}

		// Token: 0x04001252 RID: 4690
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D28")]
		[SerializeField]
		public float Val;
	}
}
