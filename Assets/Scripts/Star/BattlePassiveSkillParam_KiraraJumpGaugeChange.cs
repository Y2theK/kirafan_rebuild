﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000400 RID: 1024
	[Token(Token = "0x2000325")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_KiraraJumpGaugeChange : BattlePassiveSkillParam
	{
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000FC6 RID: 4038 RVA: 0x00006BE8 File Offset: 0x00004DE8
		[Token(Token = "0x170000D2")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E95")]
			[Address(RVA = "0x101132C20", Offset = "0x1132C20", VA = "0x101132C20", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FC7 RID: 4039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E96")]
		[Address(RVA = "0x101132C28", Offset = "0x1132C28", VA = "0x101132C28")]
		public BattlePassiveSkillParam_KiraraJumpGaugeChange(bool isAvailable, float val)
		{
		}

		// Token: 0x04001253 RID: 4691
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D29")]
		[SerializeField]
		public float Val;
	}
}
