﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000401 RID: 1025
	[Token(Token = "0x2000326")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_KiraraJumpGaugeCoef : BattlePassiveSkillParam
	{
		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000FC8 RID: 4040 RVA: 0x00006C00 File Offset: 0x00004E00
		[Token(Token = "0x170000D3")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E97")]
			[Address(RVA = "0x101132C64", Offset = "0x1132C64", VA = "0x101132C64", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FC9 RID: 4041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E98")]
		[Address(RVA = "0x101132C6C", Offset = "0x1132C6C", VA = "0x101132C6C")]
		public BattlePassiveSkillParam_KiraraJumpGaugeCoef(bool isAvailable, float val)
		{
		}

		// Token: 0x04001254 RID: 4692
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D2A")]
		[SerializeField]
		public float Val;
	}
}
