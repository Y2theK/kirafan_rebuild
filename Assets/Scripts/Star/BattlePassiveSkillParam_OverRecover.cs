﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000402 RID: 1026
	[Token(Token = "0x2000327")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_OverRecover : BattlePassiveSkillParam
	{
		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000FCA RID: 4042 RVA: 0x00006C18 File Offset: 0x00004E18
		[Token(Token = "0x170000D4")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E99")]
			[Address(RVA = "0x101132CA8", Offset = "0x1132CA8", VA = "0x101132CA8", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E9A")]
		[Address(RVA = "0x101132CB0", Offset = "0x1132CB0", VA = "0x101132CB0")]
		public BattlePassiveSkillParam_OverRecover(bool isAvailable, float limit)
		{
		}

		// Token: 0x04001255 RID: 4693
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D2B")]
		[SerializeField]
		public float limitRatio;
	}
}
