﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000403 RID: 1027
	[Token(Token = "0x2000328")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_Revive : BattlePassiveSkillParam
	{
		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000FCC RID: 4044 RVA: 0x00006C30 File Offset: 0x00004E30
		[Token(Token = "0x170000D5")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E9B")]
			[Address(RVA = "0x101132CF8", Offset = "0x1132CF8", VA = "0x101132CF8", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FCD RID: 4045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E9C")]
		[Address(RVA = "0x101132D00", Offset = "0x1132D00", VA = "0x101132D00")]
		public BattlePassiveSkillParam_Revive(bool isAvailable, BattleDefine.eReviveRecoverType type, float recover)
		{
		}

		// Token: 0x04001256 RID: 4694
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D2C")]
		[SerializeField]
		public BattleDefine.eReviveRecoverType Type;

		// Token: 0x04001257 RID: 4695
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D2D")]
		[SerializeField]
		public float Recover;
	}
}
