﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000404 RID: 1028
	[Token(Token = "0x2000329")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_SkillChange : BattlePassiveSkillParam
	{
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000FCE RID: 4046 RVA: 0x00006C48 File Offset: 0x00004E48
		[Token(Token = "0x170000D6")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E9D")]
			[Address(RVA = "0x101132D4C", Offset = "0x1132D4C", VA = "0x101132D4C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FCF RID: 4047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E9E")]
		[Address(RVA = "0x101132D54", Offset = "0x1132D54", VA = "0x101132D54")]
		public BattlePassiveSkillParam_SkillChange(bool isAvailable, int normalAttack, int classSkill_1, int classSkill_2)
		{
		}

		// Token: 0x04001258 RID: 4696
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D2E")]
		[SerializeField]
		public int NormalAttack;

		// Token: 0x04001259 RID: 4697
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D2F")]
		[SerializeField]
		public int ClassSkill_1;

		// Token: 0x0400125A RID: 4698
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000D30")]
		[SerializeField]
		public int ClassSkill_2;
	}
}
