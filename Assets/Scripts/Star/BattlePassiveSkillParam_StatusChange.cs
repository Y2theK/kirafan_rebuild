﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000405 RID: 1029
	[Token(Token = "0x200032A")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_StatusChange : BattlePassiveSkillParam
	{
		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000FD0 RID: 4048 RVA: 0x00006C60 File Offset: 0x00004E60
		[Token(Token = "0x170000D7")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000E9F")]
			[Address(RVA = "0x101132D90", Offset = "0x1132D90", VA = "0x101132D90", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EA0")]
		[Address(RVA = "0x101132D98", Offset = "0x1132D98", VA = "0x101132D98")]
		public BattlePassiveSkillParam_StatusChange(bool isAvailable, float atk, float mgc, float def, float mdef, float spd, float luck)
		{
		}

		// Token: 0x0400125B RID: 4699
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D31")]
		[SerializeField]
		public float Atk;

		// Token: 0x0400125C RID: 4700
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D32")]
		[SerializeField]
		public float Mgc;

		// Token: 0x0400125D RID: 4701
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000D33")]
		[SerializeField]
		public float Def;

		// Token: 0x0400125E RID: 4702
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000D34")]
		[SerializeField]
		public float MDef;

		// Token: 0x0400125F RID: 4703
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000D35")]
		[SerializeField]
		public float Spd;

		// Token: 0x04001260 RID: 4704
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000D36")]
		[SerializeField]
		public float Luck;
	}
}
