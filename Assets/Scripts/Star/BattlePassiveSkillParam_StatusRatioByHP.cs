﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000406 RID: 1030
	[Token(Token = "0x200032B")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_StatusRatioByHP : BattlePassiveSkillParam
	{
		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000FD2 RID: 4050 RVA: 0x00006C78 File Offset: 0x00004E78
		[Token(Token = "0x170000D8")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EA1")]
			[Address(RVA = "0x101132E00", Offset = "0x1132E00", VA = "0x101132E00", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EA2")]
		[Address(RVA = "0x101132E08", Offset = "0x1132E08", VA = "0x101132E08")]
		public BattlePassiveSkillParam_StatusRatioByHP(bool isAvailable, BattleDefine.eStatus st, int tableID)
		{
		}

		// Token: 0x04001261 RID: 4705
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D37")]
		[SerializeField]
		public BattleDefine.eStatus Type;

		// Token: 0x04001262 RID: 4706
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D38")]
		[SerializeField]
		public float[] Thresholds;

		// Token: 0x04001263 RID: 4707
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000D39")]
		[SerializeField]
		public float[] Ratios;
	}
}
