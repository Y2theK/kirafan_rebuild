﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000407 RID: 1031
	[Token(Token = "0x200032C")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_StunCoef : BattlePassiveSkillParam
	{
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000FD4 RID: 4052 RVA: 0x00006C90 File Offset: 0x00004E90
		[Token(Token = "0x170000D9")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EA3")]
			[Address(RVA = "0x10113305C", Offset = "0x113305C", VA = "0x10113305C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FD5 RID: 4053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EA4")]
		[Address(RVA = "0x101133064", Offset = "0x1133064", VA = "0x101133064")]
		public BattlePassiveSkillParam_StunCoef(bool isAvailable, float val)
		{
		}

		// Token: 0x04001264 RID: 4708
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D3A")]
		[SerializeField]
		public float Val;
	}
}
