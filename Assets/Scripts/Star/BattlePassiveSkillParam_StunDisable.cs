﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000408 RID: 1032
	[Token(Token = "0x200032D")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_StunDisable : BattlePassiveSkillParam
	{
		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000FD6 RID: 4054 RVA: 0x00006CA8 File Offset: 0x00004EA8
		[Token(Token = "0x170000DA")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EA5")]
			[Address(RVA = "0x1011330A0", Offset = "0x11330A0", VA = "0x1011330A0", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FD7 RID: 4055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EA6")]
		[Address(RVA = "0x1011330A8", Offset = "0x11330A8", VA = "0x1011330A8")]
		public BattlePassiveSkillParam_StunDisable(bool isAvailable)
		{
		}
	}
}
