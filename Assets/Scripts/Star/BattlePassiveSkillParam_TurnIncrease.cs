﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000409 RID: 1033
	[Token(Token = "0x200032E")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillParam_TurnIncrease : BattlePassiveSkillParam
	{
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000FD8 RID: 4056 RVA: 0x00006CC0 File Offset: 0x00004EC0
		[Token(Token = "0x170000DB")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EA7")]
			[Address(RVA = "0x1011330D4", Offset = "0x11330D4", VA = "0x1011330D4", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FD9 RID: 4057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EA8")]
		[Address(RVA = "0x1011330DC", Offset = "0x11330DC", VA = "0x1011330DC")]
		public BattlePassiveSkillParam_TurnIncrease(bool isAvailable, BattlePassiveSkillParam_TurnIncrease.eTarget target, BattlePassiveSkillParam_TurnIncrease.eDirection direction, int increase)
		{
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x00006CD8 File Offset: 0x00004ED8
		[Token(Token = "0x6000EA9")]
		[Address(RVA = "0x10113312C", Offset = "0x113312C", VA = "0x10113312C")]
		public int GetTurnIncrease(BattlePassiveSkillParam_TurnIncrease.eTarget target, bool isUpValue)
		{
			return 0;
		}

		// Token: 0x04001265 RID: 4709
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000D3B")]
		[SerializeField]
		public BattlePassiveSkillParam_TurnIncrease.eTarget Target;

		// Token: 0x04001266 RID: 4710
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D3C")]
		[SerializeField]
		public BattlePassiveSkillParam_TurnIncrease.eDirection Direction;

		// Token: 0x04001267 RID: 4711
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000D3D")]
		[SerializeField]
		public int Increase;

		// Token: 0x0200040A RID: 1034
		[Token(Token = "0x2000DAF")]
		public enum eTarget
		{
			// Token: 0x04001269 RID: 4713
			[Token(Token = "0x40058AC")]
			StatusChange,
			// Token: 0x0400126A RID: 4714
			[Token(Token = "0x40058AD")]
			AbnormalDisable,
			// Token: 0x0400126B RID: 4715
			[Token(Token = "0x40058AE")]
			WeakElementBonus,
			// Token: 0x0400126C RID: 4716
			[Token(Token = "0x40058AF")]
			HateChange,
			// Token: 0x0400126D RID: 4717
			[Token(Token = "0x40058B0")]
			Regene,
			// Token: 0x0400126E RID: 4718
			[Token(Token = "0x40058B1")]
			LoadFactorReduce,
			// Token: 0x0400126F RID: 4719
			[Token(Token = "0x40058B2")]
			CriticalDamageChange,
			// Token: 0x04001270 RID: 4720
			[Token(Token = "0x40058B3")]
			KiraraJumpGaugeUpOnDamage
		}

		// Token: 0x0200040B RID: 1035
		[Token(Token = "0x2000DB0")]
		public enum eDirection
		{
			// Token: 0x04001272 RID: 4722
			[Token(Token = "0x40058B5")]
			UpDown,
			// Token: 0x04001273 RID: 4723
			[Token(Token = "0x40058B6")]
			Up,
			// Token: 0x04001274 RID: 4724
			[Token(Token = "0x40058B7")]
			Down
		}
	}
}
