﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200040D RID: 1037
	[Token(Token = "0x2000330")]
	[Serializable]
	[StructLayout(3)]
	public abstract class BattlePassiveSkillSolve
	{
		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000FDC RID: 4060 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000DC")]
		public CharacterHandler Owner
		{
			[Token(Token = "0x6000EAB")]
			[Address(RVA = "0x101133190", Offset = "0x1133190", VA = "0x101133190")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000FDD RID: 4061 RVA: 0x00006CF0 File Offset: 0x00004EF0
		[Token(Token = "0x170000DD")]
		public ePassiveSkillTrigger Trigger
		{
			[Token(Token = "0x6000EAC")]
			[Address(RVA = "0x101133198", Offset = "0x1133198", VA = "0x101133198")]
			get
			{
				return ePassiveSkillTrigger.OnStart;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000FDE RID: 4062
		[Token(Token = "0x170000DE")]
		public abstract ePassiveSkillContentType PassiveSkillContentType { [Token(Token = "0x6000EAD")] [Address(Slot = "4")] get; }

		// Token: 0x06000FDF RID: 4063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EAE")]
		[Address(RVA = "0x1011331A0", Offset = "0x11331A0", VA = "0x1011331A0")]
		public BattlePassiveSkillSolve(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x00006D08 File Offset: 0x00004F08
		[Token(Token = "0x6000EAF")]
		[Address(RVA = "0x10113323C", Offset = "0x113323C", VA = "0x10113323C")]
		protected double ArgsAt(int index)
		{
			return 0.0;
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x00006D20 File Offset: 0x00004F20
		[Token(Token = "0x6000EB0")]
		[Address(RVA = "0x101133268", Offset = "0x1133268", VA = "0x101133268")]
		public static bool ParseArgsAt(double[] args, int index, out double out_result)
		{
			return default(bool);
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EB1")]
		[Address(RVA = "0x101132780", Offset = "0x1132780", VA = "0x101132780")]
		public void ResetSuppress()
		{
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x00006D38 File Offset: 0x00004F38
		[Token(Token = "0x6000EB2")]
		[Address(RVA = "0x1011332DC", Offset = "0x11332DC", VA = "0x1011332DC", Slot = "5")]
		protected virtual bool CanResetSuppress()
		{
			return default(bool);
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x00006D50 File Offset: 0x00004F50
		[Token(Token = "0x6000EB3")]
		[Address(RVA = "0x1011332EC", Offset = "0x11332EC", VA = "0x1011332EC")]
		public bool Solve(ePassiveSkillTrigger trigger)
		{
			return default(bool);
		}

		// Token: 0x06000FE5 RID: 4069
		[Token(Token = "0x6000EB4")]
		[Address(Slot = "6")]
		public abstract void SolveContent();

		// Token: 0x04001276 RID: 4726
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000D3F")]
		[NonSerialized]
		protected CharacterHandler m_Owner;

		// Token: 0x04001277 RID: 4727
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000D40")]
		[NonSerialized]
		protected ePassiveSkillTrigger m_Trigger;

		// Token: 0x04001278 RID: 4728
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000D41")]
		[NonSerialized]
		protected double[] m_Args;

		// Token: 0x04001279 RID: 4729
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000D42")]
		[SerializeField]
		public BattlePassiveSkillSolveSerializeField Serialize;
	}
}
