﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200040C RID: 1036
	[Token(Token = "0x200032F")]
	[Serializable]
	[StructLayout(3)]
	public sealed class BattlePassiveSkillSolveSerializeField
	{
		// Token: 0x06000FDB RID: 4059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EAA")]
		[Address(RVA = "0x101133234", Offset = "0x1133234", VA = "0x101133234")]
		public BattlePassiveSkillSolveSerializeField()
		{
		}

		// Token: 0x04001275 RID: 4725
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000D3E")]
		[SerializeField]
		public bool Suppress;
	}
}
