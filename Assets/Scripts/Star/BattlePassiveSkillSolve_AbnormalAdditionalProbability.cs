﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200040E RID: 1038
	[Token(Token = "0x2000331")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_AbnormalAdditionalProbability : BattlePassiveSkillSolve
	{
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000FE6 RID: 4070 RVA: 0x00006D68 File Offset: 0x00004F68
		[Token(Token = "0x170000DF")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EB5")]
			[Address(RVA = "0x101133374", Offset = "0x1133374", VA = "0x101133374", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EB6")]
		[Address(RVA = "0x10113337C", Offset = "0x113337C", VA = "0x10113337C")]
		public BattlePassiveSkillSolve_AbnormalAdditionalProbability(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EB7")]
		[Address(RVA = "0x101133380", Offset = "0x1133380", VA = "0x101133380", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400127A RID: 4730
		[Token(Token = "0x4000D43")]
		private const int IDX_CONFUSION = 0;
	}
}
