﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200040F RID: 1039
	[Token(Token = "0x2000332")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_AbnormalDisable : BattlePassiveSkillSolve
	{
		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000FE9 RID: 4073 RVA: 0x00006D80 File Offset: 0x00004F80
		[Token(Token = "0x170000E0")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EB8")]
			[Address(RVA = "0x1011334CC", Offset = "0x11334CC", VA = "0x1011334CC", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FEA RID: 4074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EB9")]
		[Address(RVA = "0x1011334D4", Offset = "0x11334D4", VA = "0x1011334D4")]
		public BattlePassiveSkillSolve_AbnormalDisable(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FEB RID: 4075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EBA")]
		[Address(RVA = "0x1011334D8", Offset = "0x11334D8", VA = "0x1011334D8", Slot = "6")]
		public override void SolveContent()
		{
		}
	}
}
