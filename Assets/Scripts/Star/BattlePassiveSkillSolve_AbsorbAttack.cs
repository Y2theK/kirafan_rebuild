﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000410 RID: 1040
	[Token(Token = "0x2000333")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_AbsorbAttack : BattlePassiveSkillSolve
	{
		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000FEC RID: 4076 RVA: 0x00006D98 File Offset: 0x00004F98
		[Token(Token = "0x170000E1")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EBB")]
			[Address(RVA = "0x101133588", Offset = "0x1133588", VA = "0x101133588", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EBC")]
		[Address(RVA = "0x101133590", Offset = "0x1133590", VA = "0x101133590")]
		public BattlePassiveSkillSolve_AbsorbAttack(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EBD")]
		[Address(RVA = "0x101133594", Offset = "0x1133594", VA = "0x101133594", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400127B RID: 4731
		[Token(Token = "0x4000D44")]
		private const int IDX_VAL_NORMAL = 0;

		// Token: 0x0400127C RID: 4732
		[Token(Token = "0x4000D45")]
		private const int IDX_VAL_SKILL = 1;

		// Token: 0x0400127D RID: 4733
		[Token(Token = "0x4000D46")]
		private const int IDX_VAL_WEAPON = 2;

		// Token: 0x0400127E RID: 4734
		[Token(Token = "0x4000D47")]
		private const int IDX_VAL_UNIQUE = 3;
	}
}
