﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000411 RID: 1041
	[Token(Token = "0x2000334")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_CardTurnChange : BattlePassiveSkillSolve
	{
		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000FEF RID: 4079 RVA: 0x00006DB0 File Offset: 0x00004FB0
		[Token(Token = "0x170000E2")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EBE")]
			[Address(RVA = "0x10113377C", Offset = "0x113377C", VA = "0x10113377C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FF0 RID: 4080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EBF")]
		[Address(RVA = "0x101133784", Offset = "0x1133784", VA = "0x101133784")]
		public BattlePassiveSkillSolve_CardTurnChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC0")]
		[Address(RVA = "0x101133788", Offset = "0x1133788", VA = "0x101133788", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400127F RID: 4735
		[Token(Token = "0x4000D48")]
		private const int IDX_ADDTURNCOUNT = 0;
	}
}
