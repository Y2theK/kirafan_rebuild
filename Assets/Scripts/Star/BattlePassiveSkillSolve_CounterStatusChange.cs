﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000412 RID: 1042
	[Token(Token = "0x2000335")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_CounterStatusChange : BattlePassiveSkillSolve
	{
		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000FF2 RID: 4082 RVA: 0x00006DC8 File Offset: 0x00004FC8
		[Token(Token = "0x170000E3")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EC1")]
			[Address(RVA = "0x10113386C", Offset = "0x113386C", VA = "0x10113386C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC2")]
		[Address(RVA = "0x101133874", Offset = "0x1133874", VA = "0x101133874")]
		public BattlePassiveSkillSolve_CounterStatusChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC3")]
		[Address(RVA = "0x101133878", Offset = "0x1133878", VA = "0x101133878", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x04001280 RID: 4736
		[Token(Token = "0x4000D49")]
		private const int IDX_TARGETTYPE = 0;

		// Token: 0x04001281 RID: 4737
		[Token(Token = "0x4000D4A")]
		private const int IDX_TURNCONSUME = 1;

		// Token: 0x04001282 RID: 4738
		[Token(Token = "0x4000D4B")]
		private const int IDX_TURN = 2;

		// Token: 0x04001283 RID: 4739
		[Token(Token = "0x4000D4C")]
		private const int IDX_ATK = 3;

		// Token: 0x04001284 RID: 4740
		[Token(Token = "0x4000D4D")]
		private const int IDX_MGC = 4;

		// Token: 0x04001285 RID: 4741
		[Token(Token = "0x4000D4E")]
		private const int IDX_DEF = 5;

		// Token: 0x04001286 RID: 4742
		[Token(Token = "0x4000D4F")]
		private const int IDX_MDF = 6;

		// Token: 0x04001287 RID: 4743
		[Token(Token = "0x4000D50")]
		private const int IDX_SPD = 7;

		// Token: 0x04001288 RID: 4744
		[Token(Token = "0x4000D51")]
		private const int IDX_LUC = 8;
	}
}
