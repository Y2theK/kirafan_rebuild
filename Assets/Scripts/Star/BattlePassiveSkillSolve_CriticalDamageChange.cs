﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000413 RID: 1043
	[Token(Token = "0x2000336")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_CriticalDamageChange : BattlePassiveSkillSolve
	{
		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000FF5 RID: 4085 RVA: 0x00006DE0 File Offset: 0x00004FE0
		[Token(Token = "0x170000E4")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EC4")]
			[Address(RVA = "0x101133ACC", Offset = "0x1133ACC", VA = "0x101133ACC", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC5")]
		[Address(RVA = "0x101133AD4", Offset = "0x1133AD4", VA = "0x101133AD4")]
		public BattlePassiveSkillSolve_CriticalDamageChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC6")]
		[Address(RVA = "0x101133AD8", Offset = "0x1133AD8", VA = "0x101133AD8", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x04001289 RID: 4745
		[Token(Token = "0x4000D52")]
		private const int IDX_VAL = 0;
	}
}
