﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000414 RID: 1044
	[Token(Token = "0x2000337")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_ElementResist : BattlePassiveSkillSolve
	{
		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000FF8 RID: 4088 RVA: 0x00006DF8 File Offset: 0x00004FF8
		[Token(Token = "0x170000E5")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EC7")]
			[Address(RVA = "0x101133BB8", Offset = "0x1133BB8", VA = "0x101133BB8", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC8")]
		[Address(RVA = "0x101133BC0", Offset = "0x1133BC0", VA = "0x101133BC0")]
		public BattlePassiveSkillSolve_ElementResist(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EC9")]
		[Address(RVA = "0x101133BC4", Offset = "0x1133BC4", VA = "0x101133BC4", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128A RID: 4746
		[Token(Token = "0x4000D53")]
		private const int IDX_FIRE = 0;
	}
}
