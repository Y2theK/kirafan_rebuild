﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000415 RID: 1045
	[Token(Token = "0x2000338")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_HateChange : BattlePassiveSkillSolve
	{
		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000FFB RID: 4091 RVA: 0x00006E10 File Offset: 0x00005010
		[Token(Token = "0x170000E6")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ECA")]
			[Address(RVA = "0x101133D1C", Offset = "0x1133D1C", VA = "0x101133D1C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ECB")]
		[Address(RVA = "0x101133D24", Offset = "0x1133D24", VA = "0x101133D24")]
		public BattlePassiveSkillSolve_HateChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ECC")]
		[Address(RVA = "0x101133D28", Offset = "0x1133D28", VA = "0x101133D28", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128B RID: 4747
		[Token(Token = "0x4000D54")]
		private const int IDX_VAL = 0;
	}
}
