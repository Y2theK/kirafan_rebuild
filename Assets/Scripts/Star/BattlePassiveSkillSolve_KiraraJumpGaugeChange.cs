﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000416 RID: 1046
	[Token(Token = "0x2000339")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_KiraraJumpGaugeChange : BattlePassiveSkillSolve
	{
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000FFE RID: 4094 RVA: 0x00006E28 File Offset: 0x00005028
		[Token(Token = "0x170000E7")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ECD")]
			[Address(RVA = "0x101133E14", Offset = "0x1133E14", VA = "0x101133E14", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06000FFF RID: 4095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ECE")]
		[Address(RVA = "0x101133E1C", Offset = "0x1133E1C", VA = "0x101133E1C")]
		public BattlePassiveSkillSolve_KiraraJumpGaugeChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ECF")]
		[Address(RVA = "0x101133E20", Offset = "0x1133E20", VA = "0x101133E20", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128C RID: 4748
		[Token(Token = "0x4000D55")]
		private const int IDX_VAL = 0;
	}
}
