﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000417 RID: 1047
	[Token(Token = "0x200033A")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_KiraraJumpGaugeCoef : BattlePassiveSkillSolve
	{
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06001001 RID: 4097 RVA: 0x00006E40 File Offset: 0x00005040
		[Token(Token = "0x170000E8")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ED0")]
			[Address(RVA = "0x101133F84", Offset = "0x1133F84", VA = "0x101133F84", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001002 RID: 4098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED1")]
		[Address(RVA = "0x101133F8C", Offset = "0x1133F8C", VA = "0x101133F8C")]
		public BattlePassiveSkillSolve_KiraraJumpGaugeCoef(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001003 RID: 4099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED2")]
		[Address(RVA = "0x101133F90", Offset = "0x1133F90", VA = "0x101133F90", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128D RID: 4749
		[Token(Token = "0x4000D56")]
		private const int IDX_VAL = 0;
	}
}
