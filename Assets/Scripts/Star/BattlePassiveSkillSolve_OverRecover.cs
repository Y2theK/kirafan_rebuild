﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000418 RID: 1048
	[Token(Token = "0x200033B")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_OverRecover : BattlePassiveSkillSolve
	{
		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06001004 RID: 4100 RVA: 0x00006E58 File Offset: 0x00005058
		[Token(Token = "0x170000E9")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ED3")]
			[Address(RVA = "0x101134070", Offset = "0x1134070", VA = "0x101134070", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001005 RID: 4101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED4")]
		[Address(RVA = "0x101134078", Offset = "0x1134078", VA = "0x101134078")]
		public BattlePassiveSkillSolve_OverRecover(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001006 RID: 4102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED5")]
		[Address(RVA = "0x10113407C", Offset = "0x113407C", VA = "0x10113407C", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128E RID: 4750
		[Token(Token = "0x4000D57")]
		private const int IDX_VAL = 0;
	}
}
