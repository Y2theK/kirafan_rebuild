﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000419 RID: 1049
	[Token(Token = "0x200033C")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_Revive : BattlePassiveSkillSolve
	{
		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06001007 RID: 4103 RVA: 0x00006E70 File Offset: 0x00005070
		[Token(Token = "0x170000EA")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ED6")]
			[Address(RVA = "0x101134210", Offset = "0x1134210", VA = "0x101134210", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001008 RID: 4104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED7")]
		[Address(RVA = "0x101134218", Offset = "0x1134218", VA = "0x101134218")]
		public BattlePassiveSkillSolve_Revive(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001009 RID: 4105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000ED8")]
		[Address(RVA = "0x10113421C", Offset = "0x113421C", VA = "0x10113421C", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400128F RID: 4751
		[Token(Token = "0x4000D58")]
		private const int IDX_TYPE = 0;

		// Token: 0x04001290 RID: 4752
		[Token(Token = "0x4000D59")]
		private const int IDX_RECOVER = 1;
	}
}
