﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041A RID: 1050
	[Token(Token = "0x200033D")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_SkillChange : BattlePassiveSkillSolve
	{
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x0600100A RID: 4106 RVA: 0x00006E88 File Offset: 0x00005088
		[Token(Token = "0x170000EB")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000ED9")]
			[Address(RVA = "0x101134324", Offset = "0x1134324", VA = "0x101134324", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x0600100B RID: 4107 RVA: 0x00006EA0 File Offset: 0x000050A0
		[Token(Token = "0x170000EC")]
		public int NormalAttack
		{
			[Token(Token = "0x6000EDA")]
			[Address(RVA = "0x10113432C", Offset = "0x113432C", VA = "0x10113432C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x0600100C RID: 4108 RVA: 0x00006EB8 File Offset: 0x000050B8
		[Token(Token = "0x170000ED")]
		public int ClassSkill_1
		{
			[Token(Token = "0x6000EDB")]
			[Address(RVA = "0x1011343A0", Offset = "0x11343A0", VA = "0x1011343A0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x0600100D RID: 4109 RVA: 0x00006ED0 File Offset: 0x000050D0
		[Token(Token = "0x170000EE")]
		public int ClassSkill_2
		{
			[Token(Token = "0x6000EDC")]
			[Address(RVA = "0x101134414", Offset = "0x1134414", VA = "0x101134414")]
			get
			{
				return 0;
			}
		}

		// Token: 0x0600100E RID: 4110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EDD")]
		[Address(RVA = "0x101134488", Offset = "0x1134488", VA = "0x101134488")]
		public BattlePassiveSkillSolve_SkillChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x0600100F RID: 4111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EDE")]
		[Address(RVA = "0x10113448C", Offset = "0x113448C", VA = "0x10113448C", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x06001010 RID: 4112 RVA: 0x00006EE8 File Offset: 0x000050E8
		[Token(Token = "0x6000EDF")]
		[Address(RVA = "0x101134368", Offset = "0x1134368", VA = "0x101134368")]
		public static int GetNormalAttackSkillIDFromArgs(double[] args)
		{
			return 0;
		}

		// Token: 0x06001011 RID: 4113 RVA: 0x00006F00 File Offset: 0x00005100
		[Token(Token = "0x6000EE0")]
		[Address(RVA = "0x1011343DC", Offset = "0x11343DC", VA = "0x1011343DC")]
		public static int GetClassSkill1SkillIDFromArgs(double[] args)
		{
			return 0;
		}

		// Token: 0x06001012 RID: 4114 RVA: 0x00006F18 File Offset: 0x00005118
		[Token(Token = "0x6000EE1")]
		[Address(RVA = "0x101134450", Offset = "0x1134450", VA = "0x101134450")]
		public static int GetClassSkill2SkillIDFromArgs(double[] args)
		{
			return 0;
		}

		// Token: 0x06001013 RID: 4115 RVA: 0x00006F30 File Offset: 0x00005130
		[Token(Token = "0x6000EE2")]
		[Address(RVA = "0x101134490", Offset = "0x1134490", VA = "0x101134490")]
		public static int GetSkillIDFromArgs(double[] args, int index)
		{
			return 0;
		}

		// Token: 0x04001291 RID: 4753
		[Token(Token = "0x4000D5A")]
		public const int IDX_NORMAL_ATTACK = 0;

		// Token: 0x04001292 RID: 4754
		[Token(Token = "0x4000D5B")]
		public const int IDX_CLASS_SKILL_1 = 1;

		// Token: 0x04001293 RID: 4755
		[Token(Token = "0x4000D5C")]
		public const int IDX_CLASS_SKILL_2 = 2;
	}
}
