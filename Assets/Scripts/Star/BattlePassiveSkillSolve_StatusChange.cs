﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041B RID: 1051
	[Token(Token = "0x200033E")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_StatusChange : BattlePassiveSkillSolve
	{
		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06001014 RID: 4116 RVA: 0x00006F48 File Offset: 0x00005148
		[Token(Token = "0x170000EF")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EE3")]
			[Address(RVA = "0x1011344C4", Offset = "0x11344C4", VA = "0x1011344C4", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001015 RID: 4117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EE4")]
		[Address(RVA = "0x1011344CC", Offset = "0x11344CC", VA = "0x1011344CC")]
		public BattlePassiveSkillSolve_StatusChange(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001016 RID: 4118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EE5")]
		[Address(RVA = "0x1011344D0", Offset = "0x11344D0", VA = "0x1011344D0", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x04001294 RID: 4756
		[Token(Token = "0x4000D5D")]
		private const int IDX_VAL_ATK = 0;

		// Token: 0x04001295 RID: 4757
		[Token(Token = "0x4000D5E")]
		private const int IDX_VAL_MGC = 1;

		// Token: 0x04001296 RID: 4758
		[Token(Token = "0x4000D5F")]
		private const int IDX_VAL_DEF = 2;

		// Token: 0x04001297 RID: 4759
		[Token(Token = "0x4000D60")]
		private const int IDX_VAL_MDEF = 3;

		// Token: 0x04001298 RID: 4760
		[Token(Token = "0x4000D61")]
		private const int IDX_VAL_SPD = 4;

		// Token: 0x04001299 RID: 4761
		[Token(Token = "0x4000D62")]
		private const int IDX_VAL_LUCK = 5;
	}
}
