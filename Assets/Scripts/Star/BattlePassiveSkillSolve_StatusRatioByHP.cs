﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041C RID: 1052
	[Token(Token = "0x200033F")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_StatusRatioByHP : BattlePassiveSkillSolve
	{
		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06001017 RID: 4119 RVA: 0x00006F60 File Offset: 0x00005160
		[Token(Token = "0x170000F0")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EE6")]
			[Address(RVA = "0x10113468C", Offset = "0x113468C", VA = "0x10113468C", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001018 RID: 4120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EE7")]
		[Address(RVA = "0x101134694", Offset = "0x1134694", VA = "0x101134694")]
		public BattlePassiveSkillSolve_StatusRatioByHP(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001019 RID: 4121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EE8")]
		[Address(RVA = "0x101134698", Offset = "0x1134698", VA = "0x101134698", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400129A RID: 4762
		[Token(Token = "0x4000D63")]
		private const int IDX_STATUSTYPE = 0;

		// Token: 0x0400129B RID: 4763
		[Token(Token = "0x4000D64")]
		private const int IDX_TABLEID = 1;
	}
}
