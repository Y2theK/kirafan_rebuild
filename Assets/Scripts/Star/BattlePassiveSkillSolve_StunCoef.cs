﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041D RID: 1053
	[Token(Token = "0x2000340")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_StunCoef : BattlePassiveSkillSolve
	{
		// Token: 0x17000106 RID: 262
		// (get) Token: 0x0600101A RID: 4122 RVA: 0x00006F78 File Offset: 0x00005178
		[Token(Token = "0x170000F1")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EE9")]
			[Address(RVA = "0x101134788", Offset = "0x1134788", VA = "0x101134788", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x0600101B RID: 4123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EEA")]
		[Address(RVA = "0x101134790", Offset = "0x1134790", VA = "0x101134790")]
		public BattlePassiveSkillSolve_StunCoef(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x0600101C RID: 4124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EEB")]
		[Address(RVA = "0x101134794", Offset = "0x1134794", VA = "0x101134794", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400129C RID: 4764
		[Token(Token = "0x4000D65")]
		private const int IDX_VAL = 0;
	}
}
