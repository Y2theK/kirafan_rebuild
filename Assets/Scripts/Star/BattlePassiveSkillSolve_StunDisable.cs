﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041E RID: 1054
	[Token(Token = "0x2000341")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_StunDisable : BattlePassiveSkillSolve
	{
		// Token: 0x17000107 RID: 263
		// (get) Token: 0x0600101D RID: 4125 RVA: 0x00006F90 File Offset: 0x00005190
		[Token(Token = "0x170000F2")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EEC")]
			[Address(RVA = "0x101134874", Offset = "0x1134874", VA = "0x101134874", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x0600101E RID: 4126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EED")]
		[Address(RVA = "0x10113487C", Offset = "0x113487C", VA = "0x10113487C")]
		public BattlePassiveSkillSolve_StunDisable(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x0600101F RID: 4127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EEE")]
		[Address(RVA = "0x101134880", Offset = "0x1134880", VA = "0x101134880", Slot = "6")]
		public override void SolveContent()
		{
		}
	}
}
