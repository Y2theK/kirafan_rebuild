﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200041F RID: 1055
	[Token(Token = "0x2000342")]
	[Serializable]
	[StructLayout(3)]
	public class BattlePassiveSkillSolve_TurnIncrease : BattlePassiveSkillSolve
	{
		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06001020 RID: 4128 RVA: 0x00006FA8 File Offset: 0x000051A8
		[Token(Token = "0x170000F3")]
		public override ePassiveSkillContentType PassiveSkillContentType
		{
			[Token(Token = "0x6000EEF")]
			[Address(RVA = "0x101134930", Offset = "0x1134930", VA = "0x101134930", Slot = "4")]
			get
			{
				return ePassiveSkillContentType.StatusChange;
			}
		}

		// Token: 0x06001021 RID: 4129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EF0")]
		[Address(RVA = "0x101134938", Offset = "0x1134938", VA = "0x101134938")]
		public BattlePassiveSkillSolve_TurnIncrease(CharacterHandler owner, ePassiveSkillTrigger trigger, double[] args)
		{
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EF1")]
		[Address(RVA = "0x10113493C", Offset = "0x113493C", VA = "0x10113493C", Slot = "6")]
		public override void SolveContent()
		{
		}

		// Token: 0x0400129D RID: 4765
		[Token(Token = "0x4000D66")]
		private const int IDX_TARGET = 0;

		// Token: 0x0400129E RID: 4766
		[Token(Token = "0x4000D67")]
		private const int IDX_DIRECTION = 1;

		// Token: 0x0400129F RID: 4767
		[Token(Token = "0x4000D68")]
		private const int IDX_INCREASE = 2;
	}
}
