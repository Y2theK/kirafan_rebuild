﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000420 RID: 1056
	[Token(Token = "0x2000343")]
	[StructLayout(3)]
	public static class BattlePassiveSkillUtility
	{
		// Token: 0x06001023 RID: 4131 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000EF2")]
		[Address(RVA = "0x101134A98", Offset = "0x1134A98", VA = "0x101134A98")]
		public static BattlePassiveSkill CreateBattlePassiveSkill(List<BattlePassiveSkillSolveSerializeField> pool, CharacterHandler owner, List<BattlePassiveSkill.eSourceType> sourceTypes, List<int> sourceValues, List<int> passiveSkillIDs)
		{
			return null;
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000EF3")]
		[Address(RVA = "0x101134E24", Offset = "0x1134E24", VA = "0x101134E24")]
		private static List<BattlePassiveSkillSolve> CreateBattlePassiveSkillSolves(CharacterHandler owner, List<PassiveSkillListDB_Param> parameters, List<BattlePassiveSkillSolveSerializeField> pool)
		{
			return null;
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000EF4")]
		[Address(RVA = "0x101135248", Offset = "0x1135248", VA = "0x101135248")]
		private static BattlePassiveSkillSolve CreateBattlePassiveSkillSolve(CharacterHandler owner, ePassiveSkillTrigger trigger, ePassiveSkillContentType type, double[] args)
		{
			return null;
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x00006FC0 File Offset: 0x000051C0
		[Token(Token = "0x6000EF5")]
		[Address(RVA = "0x1011352E0", Offset = "0x11352E0", VA = "0x1011352E0")]
		public static bool IsAvailable(BattlePassiveSkillParam param)
		{
			return default(bool);
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000EF6")]
		[Address(RVA = "0x1011352FC", Offset = "0x11352FC", VA = "0x1011352FC")]
		public static double[] GetSkillChangeArgs(int dbIdx, int passiveSkillID)
		{
			return null;
		}

		// Token: 0x06001028 RID: 4136 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000EF7")]
		[Address(RVA = "0x1011353B8", Offset = "0x11353B8", VA = "0x1011353B8")]
		public static string PassiveSkillContentTypeToString(ePassiveSkillContentType skillContentType)
		{
			return null;
		}
	}
}
