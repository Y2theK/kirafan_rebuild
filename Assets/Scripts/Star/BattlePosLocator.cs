﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003C8 RID: 968
	[Token(Token = "0x2000309")]
	[StructLayout(3)]
	public class BattlePosLocator : MonoBehaviour
	{
		// Token: 0x06000E1C RID: 3612 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CEF")]
		[Address(RVA = "0x1011354AC", Offset = "0x11354AC", VA = "0x1011354AC")]
		public Transform GetLocator(int index)
		{
			return null;
		}

		// Token: 0x06000E1D RID: 3613 RVA: 0x00005F28 File Offset: 0x00004128
		[Token(Token = "0x6000CF0")]
		[Address(RVA = "0x10113550C", Offset = "0x113550C", VA = "0x10113550C")]
		public Vector3 GetLocatorPos(int index)
		{
			return default(Vector3);
		}

		// Token: 0x06000E1E RID: 3614 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CF1")]
		[Address(RVA = "0x1011357B4", Offset = "0x11357B4", VA = "0x1011357B4")]
		public Transform GetBackLocator(int index)
		{
			return null;
		}

		// Token: 0x06000E1F RID: 3615 RVA: 0x00005F40 File Offset: 0x00004140
		[Token(Token = "0x6000CF2")]
		[Address(RVA = "0x101135848", Offset = "0x1135848", VA = "0x101135848")]
		public Vector3 GetBackLocatorPos(int index)
		{
			return default(Vector3);
		}

		// Token: 0x06000E20 RID: 3616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CF3")]
		[Address(RVA = "0x101135B18", Offset = "0x1135B18", VA = "0x101135B18")]
		public BattlePosLocator()
		{
		}

		// Token: 0x0400100C RID: 4108
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000C01")]
		[SerializeField]
		private Transform[] m_Locators;

		// Token: 0x0400100D RID: 4109
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000C02")]
		[SerializeField]
		private Transform[] m_BackLocators;
	}
}
