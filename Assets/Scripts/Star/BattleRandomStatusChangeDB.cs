﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004B3 RID: 1203
	[Token(Token = "0x20003AB")]
	[StructLayout(3)]
	public class BattleRandomStatusChangeDB : ScriptableObject
	{
		// Token: 0x060013FA RID: 5114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012AF")]
		[Address(RVA = "0x101135B20", Offset = "0x1135B20", VA = "0x101135B20")]
		public BattleRandomStatusChangeDB()
		{
		}

		// Token: 0x040016E2 RID: 5858
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010C8")]
		public BattleRandomStatusChangeDB_Param[] m_Params;
	}
}
