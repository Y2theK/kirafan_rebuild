﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004B1 RID: 1201
	[Token(Token = "0x20003A9")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct BattleRandomStatusChangeDB_Data
	{
		// Token: 0x040016DE RID: 5854
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010C4")]
		public uint m_Prob;

		// Token: 0x040016DF RID: 5855
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010C5")]
		public float[] m_Args;
	}
}
