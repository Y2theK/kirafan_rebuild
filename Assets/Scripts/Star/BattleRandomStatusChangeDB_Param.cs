﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004B2 RID: 1202
	[Token(Token = "0x20003AA")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct BattleRandomStatusChangeDB_Param
	{
		// Token: 0x040016E0 RID: 5856
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010C6")]
		public int m_ID;

		// Token: 0x040016E1 RID: 5857
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010C7")]
		public BattleRandomStatusChangeDB_Data[] m_Datas;
	}
}
