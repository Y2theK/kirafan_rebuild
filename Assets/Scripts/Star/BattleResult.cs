﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020003C9 RID: 969
	[Token(Token = "0x200030A")]
	[StructLayout(3)]
	public class BattleResult
	{
		// Token: 0x06000E21 RID: 3617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CF4")]
		[Address(RVA = "0x101135C60", Offset = "0x1135C60", VA = "0x101135C60")]
		public BattleResult()
		{
		}

		// Token: 0x06000E22 RID: 3618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CF5")]
		[Address(RVA = "0x101135D0C", Offset = "0x1135D0C", VA = "0x101135D0C")]
		public void AddKilledEnemy(int enemyID)
		{
		}

		// Token: 0x06000E23 RID: 3619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CF6")]
		[Address(RVA = "0x101135E24", Offset = "0x1135E24", VA = "0x101135E24")]
		public void SetupDropItems(List<CommonLogic.BattleDropItems> scheduleDropItems, int waveIndex)
		{
		}

		// Token: 0x06000E24 RID: 3620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000CF7")]
		[Address(RVA = "0x101135FF4", Offset = "0x1135FF4", VA = "0x101135FF4")]
		public void AddItem(CommonLogic.BattleDropItem item)
		{
		}

		// Token: 0x06000E25 RID: 3621 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CF8")]
		[Address(RVA = "0x101136064", Offset = "0x1136064", VA = "0x101136064")]
		public List<int> GetBeforeLvs()
		{
			return null;
		}

		// Token: 0x06000E26 RID: 3622 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CF9")]
		[Address(RVA = "0x1011361B0", Offset = "0x11361B0", VA = "0x1011361B0")]
		public List<UserCharacterData> GetUserCharaDatas()
		{
			return null;
		}

		// Token: 0x06000E27 RID: 3623 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CFA")]
		[Address(RVA = "0x101136354", Offset = "0x1136354", VA = "0x101136354")]
		public OfferManager.TitleData GetBeforeTitleData(eTitleType titleType)
		{
			return null;
		}

		// Token: 0x06000E28 RID: 3624 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000CFB")]
		[Address(RVA = "0x101136444", Offset = "0x1136444", VA = "0x101136444")]
		public OfferManager.TitleData GetAfterTitleData(eTitleType titleType)
		{
			return null;
		}

		// Token: 0x0400100E RID: 4110
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000C03")]
		public int m_QuestID;

		// Token: 0x0400100F RID: 4111
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000C04")]
		public BattleResult.eStatus m_Status;

		// Token: 0x04001010 RID: 4112
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000C05")]
		public bool m_IsContinuous;

		// Token: 0x04001011 RID: 4113
		[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
		[Token(Token = "0x4000C06")]
		public bool m_IsFirstClear;

		// Token: 0x04001012 RID: 4114
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
		[Token(Token = "0x4000C07")]
		public bool m_IsLoserClear;

		// Token: 0x04001013 RID: 4115
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000C08")]
		public QuestDefine.eClearRank m_ClearRank;

		// Token: 0x04001014 RID: 4116
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000C09")]
		public int m_ContinueCount;

		// Token: 0x04001015 RID: 4117
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000C0A")]
		public int m_DeadCount;

		// Token: 0x04001016 RID: 4118
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000C0B")]
		public int m_InterruptFriendUseNum;

		// Token: 0x04001017 RID: 4119
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000C0C")]
		public int m_MasterSkillUseNum;

		// Token: 0x04001018 RID: 4120
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000C0D")]
		public Dictionary<int, int> m_KilledEnemies;

		// Token: 0x04001019 RID: 4121
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000C0E")]
		public List<CommonLogic.BattleDropItem> m_DropItems;

		// Token: 0x0400101A RID: 4122
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000C0F")]
		public long m_RewardUserExp;

		// Token: 0x0400101B RID: 4123
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000C10")]
		public long[] m_UserNextExps;

		// Token: 0x0400101C RID: 4124
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000C11")]
		public int m_BeforeUserLv;

		// Token: 0x0400101D RID: 4125
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000C12")]
		public long m_BeforeUserExp;

		// Token: 0x0400101E RID: 4126
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000C13")]
		public int m_AfterUserLv;

		// Token: 0x0400101F RID: 4127
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000C14")]
		public long m_AfterUserExp;

		// Token: 0x04001020 RID: 4128
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000C15")]
		public long m_RewardCharaExp;

		// Token: 0x04001021 RID: 4129
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000C16")]
		public long m_RewardFriendshipExp;

		// Token: 0x04001022 RID: 4130
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000C17")]
		public BattleResult.CharaData[] m_CharaDatas;

		// Token: 0x04001023 RID: 4131
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000C18")]
		public OfferManager.TitleData[] m_BeforeTitleDatas;

		// Token: 0x04001024 RID: 4132
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000C19")]
		public OfferManager.TitleData[] m_AfterTitleDatas;

		// Token: 0x04001025 RID: 4133
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000C1A")]
		public long m_RewardGold;

		// Token: 0x04001026 RID: 4134
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000C1B")]
		public long m_BeforeGold;

		// Token: 0x04001027 RID: 4135
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000C1C")]
		public long m_AfterGold;

		// Token: 0x04001028 RID: 4136
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000C1D")]
		public QuestReward m_RewardFirst;

		// Token: 0x04001029 RID: 4137
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000C1E")]
		public QuestReward m_RewardGroup;

		// Token: 0x0400102A RID: 4138
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000C1F")]
		public QuestReward m_RewardComp;

		// Token: 0x0400102B RID: 4139
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000C20")]
		public long m_GainPoint;

		// Token: 0x0400102C RID: 4140
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4000C21")]
		public long m_TotalPoint;

		// Token: 0x020003CA RID: 970
		[Token(Token = "0x2000D93")]
		public enum eStatus
		{
			// Token: 0x0400102E RID: 4142
			[Token(Token = "0x400578D")]
			None = -1,
			// Token: 0x0400102F RID: 4143
			[Token(Token = "0x400578E")]
			Retire,
			// Token: 0x04001030 RID: 4144
			[Token(Token = "0x400578F")]
			GameOver,
			// Token: 0x04001031 RID: 4145
			[Token(Token = "0x4005790")]
			Clear
		}

		// Token: 0x020003CB RID: 971
		[Token(Token = "0x2000D94")]
		public struct CharaData
		{
			// Token: 0x04001032 RID: 4146
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005791")]
			public bool m_IsAvailable;

			// Token: 0x04001033 RID: 4147
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005792")]
			public long m_MngID;

			// Token: 0x04001034 RID: 4148
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005793")]
			public long m_AddExp;

			// Token: 0x04001035 RID: 4149
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005794")]
			public long[] m_NextExps;

			// Token: 0x04001036 RID: 4150
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005795")]
			public int m_BeforeLv;

			// Token: 0x04001037 RID: 4151
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005796")]
			public long m_BeforeExp;

			// Token: 0x04001038 RID: 4152
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005797")]
			public int m_AfterLv;

			// Token: 0x04001039 RID: 4153
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005798")]
			public long m_AfterExp;

			// Token: 0x0400103A RID: 4154
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005799")]
			public long m_AddFriendshipExp;

			// Token: 0x0400103B RID: 4155
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x400579A")]
			public long[] m_FriendshipNextExps;

			// Token: 0x0400103C RID: 4156
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400579B")]
			public int m_BeforeFriendship;

			// Token: 0x0400103D RID: 4157
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x400579C")]
			public long m_BeforeFriendshipExp;

			// Token: 0x0400103E RID: 4158
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x400579D")]
			public int m_AfterFriendship;

			// Token: 0x0400103F RID: 4159
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x400579E")]
			public long m_AfterFriendshipExp;
		}
	}
}
