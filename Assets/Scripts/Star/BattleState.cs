﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000772 RID: 1906
	[Token(Token = "0x20005BB")]
	[StructLayout(3)]
	public class BattleState : GameStateBase
	{
		// Token: 0x06001CA0 RID: 7328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A1E")]
		[Address(RVA = "0x101136534", Offset = "0x1136534", VA = "0x101136534")]
		public BattleState(BattleMain owner)
		{
		}

		// Token: 0x06001CA1 RID: 7329 RVA: 0x0000CCC0 File Offset: 0x0000AEC0
		[Token(Token = "0x6001A1F")]
		[Address(RVA = "0x101136560", Offset = "0x1136560", VA = "0x101136560", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CA2 RID: 7330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A20")]
		[Address(RVA = "0x101136568", Offset = "0x1136568", VA = "0x101136568", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CA3 RID: 7331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A21")]
		[Address(RVA = "0x10113656C", Offset = "0x113656C", VA = "0x10113656C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CA4 RID: 7332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A22")]
		[Address(RVA = "0x101136570", Offset = "0x1136570", VA = "0x101136570", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CA5 RID: 7333 RVA: 0x0000CCD8 File Offset: 0x0000AED8
		[Token(Token = "0x6001A23")]
		[Address(RVA = "0x101136574", Offset = "0x1136574", VA = "0x101136574", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001CA6 RID: 7334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A24")]
		[Address(RVA = "0x10113657C", Offset = "0x113657C", VA = "0x10113657C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001CA7 RID: 7335 RVA: 0x0000CCF0 File Offset: 0x0000AEF0
		[Token(Token = "0x6001A25")]
		[Address(RVA = "0x101136580", Offset = "0x1136580", VA = "0x101136580")]
		protected int GetAfterADV()
		{
			return 0;
		}

		// Token: 0x04002CD2 RID: 11474
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400236D")]
		protected BattleMain m_Owner;
	}
}
