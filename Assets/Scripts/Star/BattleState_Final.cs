﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000773 RID: 1907
	[Token(Token = "0x20005BC")]
	[StructLayout(3)]
	public class BattleState_Final : BattleState
	{
		// Token: 0x06001CA8 RID: 7336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A26")]
		[Address(RVA = "0x10112B474", Offset = "0x112B474", VA = "0x10112B474")]
		public BattleState_Final(BattleMain owner)
		{
		}

		// Token: 0x06001CA9 RID: 7337 RVA: 0x0000CD08 File Offset: 0x0000AF08
		[Token(Token = "0x6001A27")]
		[Address(RVA = "0x101136768", Offset = "0x1136768", VA = "0x101136768", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CAA RID: 7338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A28")]
		[Address(RVA = "0x101136770", Offset = "0x1136770", VA = "0x101136770", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CAB RID: 7339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A29")]
		[Address(RVA = "0x101136778", Offset = "0x1136778", VA = "0x101136778", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CAC RID: 7340 RVA: 0x0000CD20 File Offset: 0x0000AF20
		[Token(Token = "0x6001A2A")]
		[Address(RVA = "0x10113677C", Offset = "0x113677C", VA = "0x10113677C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x04002CD3 RID: 11475
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400236E")]
		private BattleState_Final.eStep m_Step;

		// Token: 0x02000774 RID: 1908
		[Token(Token = "0x2000E88")]
		private enum eStep
		{
			// Token: 0x04002CD5 RID: 11477
			[Token(Token = "0x4005CDC")]
			None = -1,
			// Token: 0x04002CD6 RID: 11478
			[Token(Token = "0x4005CDD")]
			First,
			// Token: 0x04002CD7 RID: 11479
			[Token(Token = "0x4005CDE")]
			FadeOut,
			// Token: 0x04002CD8 RID: 11480
			[Token(Token = "0x4005CDF")]
			FadeOut_Wait,
			// Token: 0x04002CD9 RID: 11481
			[Token(Token = "0x4005CE0")]
			Destory,
			// Token: 0x04002CDA RID: 11482
			[Token(Token = "0x4005CE1")]
			Destory_Wait,
			// Token: 0x04002CDB RID: 11483
			[Token(Token = "0x4005CE2")]
			End
		}
	}
}
