﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000775 RID: 1909
	[Token(Token = "0x20005BD")]
	[StructLayout(3)]
	public class BattleState_Main : BattleState
	{
		// Token: 0x06001CAD RID: 7341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A2B")]
		[Address(RVA = "0x10112B3BC", Offset = "0x112B3BC", VA = "0x10112B3BC")]
		public BattleState_Main(BattleMain owner)
		{
		}

		// Token: 0x06001CAE RID: 7342 RVA: 0x0000CD38 File Offset: 0x0000AF38
		[Token(Token = "0x6001A2C")]
		[Address(RVA = "0x101136BB0", Offset = "0x1136BB0", VA = "0x101136BB0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CAF RID: 7343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A2D")]
		[Address(RVA = "0x101136BB8", Offset = "0x1136BB8", VA = "0x101136BB8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CB0 RID: 7344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A2E")]
		[Address(RVA = "0x101136BC0", Offset = "0x1136BC0", VA = "0x101136BC0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CB1 RID: 7345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A2F")]
		[Address(RVA = "0x101136BC4", Offset = "0x1136BC4", VA = "0x101136BC4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CB2 RID: 7346 RVA: 0x0000CD50 File Offset: 0x0000AF50
		[Token(Token = "0x6001A30")]
		[Address(RVA = "0x101136BC8", Offset = "0x1136BC8", VA = "0x101136BC8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x04002CDC RID: 11484
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400236F")]
		private BattleState_Main.eStep m_Step;

		// Token: 0x02000776 RID: 1910
		[Token(Token = "0x2000E89")]
		private enum eStep
		{
			// Token: 0x04002CDE RID: 11486
			[Token(Token = "0x4005CE4")]
			None = -1,
			// Token: 0x04002CDF RID: 11487
			[Token(Token = "0x4005CE5")]
			First,
			// Token: 0x04002CE0 RID: 11488
			[Token(Token = "0x4005CE6")]
			CacheClearWait,
			// Token: 0x04002CE1 RID: 11489
			[Token(Token = "0x4005CE7")]
			Prepare,
			// Token: 0x04002CE2 RID: 11490
			[Token(Token = "0x4005CE8")]
			Main
		}
	}
}
