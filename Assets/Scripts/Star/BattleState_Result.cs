﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.UI.Battle;
using UnityEngine;

namespace Star
{
	// Token: 0x02000777 RID: 1911
	[Token(Token = "0x20005BE")]
	[StructLayout(3)]
	public class BattleState_Result : BattleState
	{
		// Token: 0x06001CB3 RID: 7347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A31")]
		[Address(RVA = "0x10112B3F0", Offset = "0x112B3F0", VA = "0x10112B3F0")]
		public BattleState_Result(BattleMain owner)
		{
		}

		// Token: 0x06001CB4 RID: 7348 RVA: 0x0000CD68 File Offset: 0x0000AF68
		[Token(Token = "0x6001A32")]
		[Address(RVA = "0x101136F1C", Offset = "0x1136F1C", VA = "0x101136F1C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CB5 RID: 7349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A33")]
		[Address(RVA = "0x101136F24", Offset = "0x1136F24", VA = "0x101136F24", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CB6 RID: 7350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A34")]
		[Address(RVA = "0x101136F2C", Offset = "0x1136F2C", VA = "0x101136F2C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CB7 RID: 7351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A35")]
		[Address(RVA = "0x101136F30", Offset = "0x1136F30", VA = "0x101136F30", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CB8 RID: 7352 RVA: 0x0000CD80 File Offset: 0x0000AF80
		[Token(Token = "0x6001A36")]
		[Address(RVA = "0x101136F38", Offset = "0x1136F38", VA = "0x101136F38", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001CB9 RID: 7353 RVA: 0x0000CD98 File Offset: 0x0000AF98
		[Token(Token = "0x6001A37")]
		[Address(RVA = "0x1011380F8", Offset = "0x11380F8", VA = "0x1011380F8")]
		private bool SetupAndPlay_Result()
		{
			return default(bool);
		}

		// Token: 0x06001CBA RID: 7354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A38")]
		[Address(RVA = "0x101138438", Offset = "0x1138438", VA = "0x101138438")]
		private void SetResultUIReorderParam()
		{
		}

		// Token: 0x06001CBB RID: 7355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A39")]
		[Address(RVA = "0x1011385C8", Offset = "0x11385C8", VA = "0x1011385C8")]
		private void OnClickButton_Reorder()
		{
		}

		// Token: 0x06001CBC RID: 7356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A3A")]
		[Address(RVA = "0x10113883C", Offset = "0x113883C", VA = "0x10113883C")]
		private void OnOrderSuccess()
		{
		}

		// Token: 0x06001CBD RID: 7357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A3B")]
		[Address(RVA = "0x10113888C", Offset = "0x113888C", VA = "0x10113888C")]
		private void OnOrderFailed(QuestDefine.eReturnLogAdd result)
		{
		}

		// Token: 0x06001CBE RID: 7358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A3C")]
		[Address(RVA = "0x10113891C", Offset = "0x113891C", VA = "0x10113891C")]
		public void HideCharactersForResult()
		{
		}

		// Token: 0x06001CBF RID: 7359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A3D")]
		[Address(RVA = "0x101138C64", Offset = "0x1138C64", VA = "0x101138C64")]
		public void ShowCharactersForResult()
		{
		}

		// Token: 0x06001CC0 RID: 7360 RVA: 0x0000CDB0 File Offset: 0x0000AFB0
		[Token(Token = "0x6001A3E")]
		[Address(RVA = "0x101139024", Offset = "0x1139024", VA = "0x101139024")]
		private bool OnResponse_QuestSuccess(MeigewwwParam wwwParam, Questlogcomplete param)
		{
			return default(bool);
		}

		// Token: 0x06001CC1 RID: 7361 RVA: 0x0000CDC8 File Offset: 0x0000AFC8
		[Token(Token = "0x6001A3F")]
		[Address(RVA = "0x101139098", Offset = "0x1139098", VA = "0x101139098")]
		private bool OnResponse_QuestFailed(MeigewwwParam wwwParam, Questlogcomplete param)
		{
			return default(bool);
		}

		// Token: 0x04002CE3 RID: 11491
		[Token(Token = "0x4002370")]
		private static readonly Vector3[] CHARA_POS_IN_RESULT;

		// Token: 0x04002CE4 RID: 11492
		[Token(Token = "0x4002371")]
		private static readonly float[] CHARA_POS_OFFSET_EACH_WIN;

		// Token: 0x04002CE5 RID: 11493
		[Token(Token = "0x4002372")]
		private const float CHARA_SCALE_IN_RESULT = 6.5f;

		// Token: 0x04002CE6 RID: 11494
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002373")]
		private BattleResultUI m_ResultUI;

		// Token: 0x04002CE7 RID: 11495
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002374")]
		private bool m_IsContinuousOnStart;

		// Token: 0x04002CE8 RID: 11496
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4002375")]
		private bool m_IsFirstClearOnStart;

		// Token: 0x04002CE9 RID: 11497
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002376")]
		private QuestOrderProcess m_QuestOrderProcess;

		// Token: 0x04002CEA RID: 11498
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002377")]
		private BattleState_Result.eStep m_Step;

		// Token: 0x02000778 RID: 1912
		[Token(Token = "0x2000E8A")]
		private enum eStep
		{
			// Token: 0x04002CEC RID: 11500
			[Token(Token = "0x4005CEA")]
			None = -1,
			// Token: 0x04002CED RID: 11501
			[Token(Token = "0x4005CEB")]
			First,
			// Token: 0x04002CEE RID: 11502
			[Token(Token = "0x4005CEC")]
			Request_Wait,
			// Token: 0x04002CEF RID: 11503
			[Token(Token = "0x4005CED")]
			RequestPostProcess,
			// Token: 0x04002CF0 RID: 11504
			[Token(Token = "0x4005CEE")]
			RequestPostProcess_Wait,
			// Token: 0x04002CF1 RID: 11505
			[Token(Token = "0x4005CEF")]
			ResultLoad,
			// Token: 0x04002CF2 RID: 11506
			[Token(Token = "0x4005CF0")]
			ResultLoad_Wait,
			// Token: 0x04002CF3 RID: 11507
			[Token(Token = "0x4005CF1")]
			ResultPlayIn,
			// Token: 0x04002CF4 RID: 11508
			[Token(Token = "0x4005CF2")]
			Result_Wait,
			// Token: 0x04002CF5 RID: 11509
			[Token(Token = "0x4005CF3")]
			FriendPropose,
			// Token: 0x04002CF6 RID: 11510
			[Token(Token = "0x4005CF4")]
			FriendProposeWait,
			// Token: 0x04002CF7 RID: 11511
			[Token(Token = "0x4005CF5")]
			StoreReview,
			// Token: 0x04002CF8 RID: 11512
			[Token(Token = "0x4005CF6")]
			StoreReview_AppOpenWait,
			// Token: 0x04002CF9 RID: 11513
			[Token(Token = "0x4005CF7")]
			StoreReview_NativeOpenWait,
			// Token: 0x04002CFA RID: 11514
			[Token(Token = "0x4005CF8")]
			LoadOpen,
			// Token: 0x04002CFB RID: 11515
			[Token(Token = "0x4005CF9")]
			LoadOpenWait,
			// Token: 0x04002CFC RID: 11516
			[Token(Token = "0x4005CFA")]
			ResultUnload,
			// Token: 0x04002CFD RID: 11517
			[Token(Token = "0x4005CFB")]
			ResultUnload_Wait,
			// Token: 0x04002CFE RID: 11518
			[Token(Token = "0x4005CFC")]
			FailedTips,
			// Token: 0x04002CFF RID: 11519
			[Token(Token = "0x4005CFD")]
			FailedTipsWait,
			// Token: 0x04002D00 RID: 11520
			[Token(Token = "0x4005CFE")]
			Failed,
			// Token: 0x04002D01 RID: 11521
			[Token(Token = "0x4005CFF")]
			Failed_Wait,
			// Token: 0x04002D02 RID: 11522
			[Token(Token = "0x4005D00")]
			Transit
		}
	}
}
