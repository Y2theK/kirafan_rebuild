﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004B6 RID: 1206
	[Token(Token = "0x20003AE")]
	[StructLayout(3)]
	public class BattleStatusRatioByHpDB : ScriptableObject
	{
		// Token: 0x060013FB RID: 5115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B0")]
		[Address(RVA = "0x101139340", Offset = "0x1139340", VA = "0x101139340")]
		public BattleStatusRatioByHpDB()
		{
		}

		// Token: 0x040016E7 RID: 5863
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010CD")]
		public BattleStatusRatioByHpDB_Param[] m_Params;
	}
}
