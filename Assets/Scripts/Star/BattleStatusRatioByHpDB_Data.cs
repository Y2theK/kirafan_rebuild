﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004B4 RID: 1204
	[Token(Token = "0x20003AC")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct BattleStatusRatioByHpDB_Data
	{
		// Token: 0x040016E3 RID: 5859
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010C9")]
		public float m_HPThreshold;

		// Token: 0x040016E4 RID: 5860
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010CA")]
		public float m_Ratio;
	}
}
