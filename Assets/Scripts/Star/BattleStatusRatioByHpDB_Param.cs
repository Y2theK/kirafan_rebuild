﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004B5 RID: 1205
	[Token(Token = "0x20003AD")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct BattleStatusRatioByHpDB_Param
	{
		// Token: 0x040016E5 RID: 5861
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010CB")]
		public int m_ID;

		// Token: 0x040016E6 RID: 5862
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010CC")]
		public BattleStatusRatioByHpDB_Data[] m_Datas;
	}
}
