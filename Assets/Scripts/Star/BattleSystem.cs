﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.UI.Battle;
using UnityEngine;

namespace Star
{
	// Token: 0x020003CC RID: 972
	[Token(Token = "0x200030B")]
	[StructLayout(3)]
	public class BattleSystem : MonoBehaviour
	{
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000E29 RID: 3625 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AA")]
		public BattleSystemData BSD
		{
			[Token(Token = "0x6000CFC")]
			[Address(RVA = "0x101138834", Offset = "0x1138834", VA = "0x101138834")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000E2A RID: 3626 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AB")]
		public BattleBG BattleBG
		{
			[Token(Token = "0x6000CFD")]
			[Address(RVA = "0x101139348", Offset = "0x1139348", VA = "0x101139348")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000E2B RID: 3627 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AC")]
		public Camera MainCamera
		{
			[Token(Token = "0x6000CFE")]
			[Address(RVA = "0x10113901C", Offset = "0x113901C", VA = "0x10113901C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000E2C RID: 3628 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AD")]
		public Camera ResultCamera
		{
			[Token(Token = "0x6000CFF")]
			[Address(RVA = "0x101138428", Offset = "0x1138428", VA = "0x101138428")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000E2D RID: 3629 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AE")]
		public BattleCamera BattleCamera
		{
			[Token(Token = "0x6000D00")]
			[Address(RVA = "0x101138C5C", Offset = "0x1138C5C", VA = "0x101138C5C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000E2E RID: 3630 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000AF")]
		public Transform MainCameraTransform
		{
			[Token(Token = "0x6000D01")]
			[Address(RVA = "0x101139350", Offset = "0x1139350", VA = "0x101139350")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000E2F RID: 3631 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B0")]
		public SkillActionPlayer SkillActionPlayer
		{
			[Token(Token = "0x6000D02")]
			[Address(RVA = "0x101139358", Offset = "0x1139358", VA = "0x101139358")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000E30 RID: 3632 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B1")]
		public Transform CenterLocator_Player
		{
			[Token(Token = "0x6000D03")]
			[Address(RVA = "0x101139360", Offset = "0x1139360", VA = "0x101139360")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000E31 RID: 3633 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B2")]
		public Transform CenterLocator_Enemy
		{
			[Token(Token = "0x6000D04")]
			[Address(RVA = "0x101139368", Offset = "0x1139368", VA = "0x101139368")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000E32 RID: 3634 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B3")]
		public BattleUniqueSkillCutIn UniqueSkillCutIn
		{
			[Token(Token = "0x6000D05")]
			[Address(RVA = "0x101139370", Offset = "0x1139370", VA = "0x101139370")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000E33 RID: 3635 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B4")]
		public BattlePartyData[] Parties
		{
			[Token(Token = "0x6000D06")]
			[Address(RVA = "0x101139378", Offset = "0x1139378", VA = "0x101139378")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000E34 RID: 3636 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B5")]
		public BattlePartyData PlayerParty
		{
			[Token(Token = "0x6000D07")]
			[Address(RVA = "0x10113806C", Offset = "0x113806C", VA = "0x10113806C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000E35 RID: 3637 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B6")]
		public BattlePartyData EnemyParty
		{
			[Token(Token = "0x6000D08")]
			[Address(RVA = "0x101138C14", Offset = "0x1138C14", VA = "0x101138C14")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000E36 RID: 3638 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B7")]
		public BattleOrder Order
		{
			[Token(Token = "0x6000D09")]
			[Address(RVA = "0x101139380", Offset = "0x1139380", VA = "0x101139380")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000E37 RID: 3639 RVA: 0x00005F58 File Offset: 0x00004158
		[Token(Token = "0x170000B8")]
		public QuestListDB_Param QuestParam
		{
			[Token(Token = "0x6000D0A")]
			[Address(RVA = "0x101136728", Offset = "0x1136728", VA = "0x101136728")]
			get
			{
				return default(QuestListDB_Param);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000E38 RID: 3640 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000B9")]
		public BattleTutorial Tutorial
		{
			[Token(Token = "0x6000D0B")]
			[Address(RVA = "0x101139388", Offset = "0x1139388", VA = "0x101139388")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000E39 RID: 3641 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000BA")]
		public BattleOption Option
		{
			[Token(Token = "0x6000D0C")]
			[Address(RVA = "0x101139390", Offset = "0x1139390", VA = "0x101139390")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000E3A RID: 3642 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000BB")]
		public BattleTimeScaler TimeScaler
		{
			[Token(Token = "0x6000D0D")]
			[Address(RVA = "0x101139398", Offset = "0x1139398", VA = "0x101139398")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000E3B RID: 3643 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000BC")]
		public BattleResult QuestResult
		{
			[Token(Token = "0x6000D0E")]
			[Address(RVA = "0x101136760", Offset = "0x1136760", VA = "0x101136760")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x06000E3C RID: 3644 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000BD")]
		public BattleUniqueSkillScene UniqueSkillScene
		{
			[Token(Token = "0x6000D0F")]
			[Address(RVA = "0x1011393A0", Offset = "0x11393A0", VA = "0x1011393A0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x06000E3D RID: 3645 RVA: 0x00005F70 File Offset: 0x00004170
		[Token(Token = "0x170000BE")]
		public bool IsInUniqueSkillScene
		{
			[Token(Token = "0x6000D10")]
			[Address(RVA = "0x1011393A8", Offset = "0x11393A8", VA = "0x1011393A8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000E3E RID: 3646 RVA: 0x00005F88 File Offset: 0x00004188
		[Token(Token = "0x170000BF")]
		public bool IsRequestLastBlow
		{
			[Token(Token = "0x6000D11")]
			[Address(RVA = "0x1011393B8", Offset = "0x11393B8", VA = "0x1011393B8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000E3F RID: 3647 RVA: 0x00005FA0 File Offset: 0x000041A0
		// (set) Token: 0x06000E40 RID: 3648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000C0")]
		public bool IsEnableTouchChangeTarget
		{
			[Token(Token = "0x6000D12")]
			[Address(RVA = "0x1011393C0", Offset = "0x11393C0", VA = "0x1011393C0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000D13")]
			[Address(RVA = "0x101139454", Offset = "0x1139454", VA = "0x101139454")]
			set
			{
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000E41 RID: 3649 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C1")]
		public BattleMasterOrbData MasterOrbData
		{
			[Token(Token = "0x6000D14")]
			[Address(RVA = "0x101138430", Offset = "0x1138430", VA = "0x101138430")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000E42 RID: 3650 RVA: 0x00005FB8 File Offset: 0x000041B8
		[Token(Token = "0x170000C2")]
		public bool SelectIsUniqueSkill
		{
			[Token(Token = "0x6000D15")]
			[Address(RVA = "0x10113945C", Offset = "0x113945C", VA = "0x10113945C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D16")]
		[Address(RVA = "0x101136E54", Offset = "0x1136E54", VA = "0x101136E54")]
		public void SystemAwake()
		{
		}

		// Token: 0x06000E44 RID: 3652 RVA: 0x00005FD0 File Offset: 0x000041D0
		[Token(Token = "0x6000D17")]
		[Address(RVA = "0x101139488", Offset = "0x1139488", VA = "0x101139488")]
		private bool SystemAwakeCommonConstruct(long recvID, int questID)
		{
			return default(bool);
		}

		// Token: 0x06000E45 RID: 3653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D18")]
		[Address(RVA = "0x1011380B0", Offset = "0x11380B0", VA = "0x1011380B0")]
		public void SystemDestoryOnlyUI()
		{
		}

		// Token: 0x06000E46 RID: 3654 RVA: 0x00005FE8 File Offset: 0x000041E8
		[Token(Token = "0x6000D19")]
		[Address(RVA = "0x1011380D4", Offset = "0x11380D4", VA = "0x1011380D4")]
		public bool IsComplteDestoryOnlyUI()
		{
			return default(bool);
		}

		// Token: 0x06000E47 RID: 3655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D1A")]
		[Address(RVA = "0x101136B6C", Offset = "0x1136B6C", VA = "0x101136B6C")]
		public void SystemDestory()
		{
		}

		// Token: 0x06000E48 RID: 3656 RVA: 0x00006000 File Offset: 0x00004200
		[Token(Token = "0x6000D1B")]
		[Address(RVA = "0x101136B8C", Offset = "0x1136B8C", VA = "0x101136B8C")]
		public bool IsComplteDestory()
		{
			return default(bool);
		}

		// Token: 0x06000E49 RID: 3657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D1C")]
		[Address(RVA = "0x10112B1A8", Offset = "0x112B1A8", VA = "0x10112B1A8")]
		public void SystemUpdate()
		{
		}

		// Token: 0x06000E4A RID: 3658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D1D")]
		[Address(RVA = "0x10112B2EC", Offset = "0x112B2EC", VA = "0x10112B2EC")]
		public void SystemLateUpdate()
		{
		}

		// Token: 0x06000E4B RID: 3659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D1E")]
		[Address(RVA = "0x10113F600", Offset = "0x113F600", VA = "0x10113F600")]
		private void OnDestroy()
		{
		}

		// Token: 0x06000E4C RID: 3660 RVA: 0x00006018 File Offset: 0x00004218
		[Token(Token = "0x6000D1F")]
		[Address(RVA = "0x10113F6A4", Offset = "0x113F6A4", VA = "0x10113F6A4")]
		public bool IsPrepareing()
		{
			return default(bool);
		}

		// Token: 0x06000E4D RID: 3661 RVA: 0x00006030 File Offset: 0x00004230
		[Token(Token = "0x6000D20")]
		[Address(RVA = "0x101136F0C", Offset = "0x1136F0C", VA = "0x101136F0C")]
		public bool IsBattleFinished()
		{
			return default(bool);
		}

		// Token: 0x06000E4E RID: 3662 RVA: 0x00006048 File Offset: 0x00004248
		[Token(Token = "0x6000D21")]
		[Address(RVA = "0x10113F6B4", Offset = "0x113F6B4", VA = "0x10113F6B4")]
		private bool CheckClearAllWave()
		{
			return default(bool);
		}

		// Token: 0x06000E4F RID: 3663 RVA: 0x00006060 File Offset: 0x00004260
		[Token(Token = "0x6000D22")]
		[Address(RVA = "0x10113F72C", Offset = "0x113F72C", VA = "0x10113F72C")]
		private bool CheckClearCurrentWave()
		{
			return default(bool);
		}

		// Token: 0x06000E50 RID: 3664 RVA: 0x00006078 File Offset: 0x00004278
		[Token(Token = "0x6000D23")]
		[Address(RVA = "0x10113F758", Offset = "0x113F758", VA = "0x10113F758")]
		private bool CheckGameOver()
		{
			return default(bool);
		}

		// Token: 0x06000E51 RID: 3665 RVA: 0x00006090 File Offset: 0x00004290
		[Token(Token = "0x6000D24")]
		[Address(RVA = "0x10113F784", Offset = "0x113F784", VA = "0x10113F784")]
		private bool IsWarningWave()
		{
			return default(bool);
		}

		// Token: 0x06000E52 RID: 3666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D25")]
		[Address(RVA = "0x10113F7EC", Offset = "0x113F7EC", VA = "0x10113F7EC")]
		private void SetupWaveData(int waveIndex)
		{
		}

		// Token: 0x06000E53 RID: 3667 RVA: 0x000060A8 File Offset: 0x000042A8
		[Token(Token = "0x6000D26")]
		[Address(RVA = "0x10113FB3C", Offset = "0x113FB3C", VA = "0x10113FB3C")]
		private BattleOrder.eFrameType TurnStartProcess()
		{
			return BattleOrder.eFrameType.Chara;
		}

		// Token: 0x06000E54 RID: 3668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D27")]
		[Address(RVA = "0x10113FF80", Offset = "0x113FF80", VA = "0x10113FF80")]
		private void TurnEndProcess()
		{
		}

		// Token: 0x06000E55 RID: 3669 RVA: 0x000060C0 File Offset: 0x000042C0
		[Token(Token = "0x6000D28")]
		[Address(RVA = "0x101140C34", Offset = "0x1140C34", VA = "0x101140C34")]
		private bool TurnSkipProcess()
		{
			return default(bool);
		}

		// Token: 0x06000E56 RID: 3670 RVA: 0x000060D8 File Offset: 0x000042D8
		[Token(Token = "0x6000D29")]
		[Address(RVA = "0x101141070", Offset = "0x1141070", VA = "0x101141070")]
		private bool StunCheckProcess()
		{
			return default(bool);
		}

		// Token: 0x06000E57 RID: 3671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D2A")]
		[Address(RVA = "0x1011411B0", Offset = "0x11411B0", VA = "0x1011411B0")]
		private void StunOccurProcess()
		{
		}

		// Token: 0x06000E58 RID: 3672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D2B")]
		[Address(RVA = "0x1011413F0", Offset = "0x11413F0", VA = "0x1011413F0")]
		private void StunApplyProcess()
		{
		}

		// Token: 0x06000E59 RID: 3673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D2C")]
		[Address(RVA = "0x1011416D0", Offset = "0x11416D0", VA = "0x1011416D0")]
		private void StunApplyPostProcess()
		{
		}

		// Token: 0x06000E5A RID: 3674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D2D")]
		[Address(RVA = "0x10114172C", Offset = "0x114172C", VA = "0x10114172C")]
		private void TurnEndPreProcess_DisplayDamageAndUpdate(int damage, BattleUI.eDispDamageAbnormal abnormalFlag, CharacterHandler turnOwner)
		{
		}

		// Token: 0x06000E5B RID: 3675 RVA: 0x000060F0 File Offset: 0x000042F0
		[Token(Token = "0x6000D2E")]
		[Address(RVA = "0x1011418B4", Offset = "0x11418B4", VA = "0x1011418B4")]
		private bool TurnEndPreProcess_AwakeFromSleep()
		{
			return default(bool);
		}

		// Token: 0x06000E5C RID: 3676 RVA: 0x00006108 File Offset: 0x00004308
		[Token(Token = "0x6000D2F")]
		[Address(RVA = "0x101141CF8", Offset = "0x1141CF8", VA = "0x101141CF8")]
		private bool TurnEndPreProcess_AbnormalPoison(CharacterHandler topCharaHndl)
		{
			return default(bool);
		}

		// Token: 0x06000E5D RID: 3677 RVA: 0x00006120 File Offset: 0x00004320
		[Token(Token = "0x6000D30")]
		[Address(RVA = "0x101141E5C", Offset = "0x1141E5C", VA = "0x101141E5C")]
		private bool TurnEndPreProcess_RegeneRecover(CharacterHandler topCharaHndl)
		{
			return default(bool);
		}

		// Token: 0x06000E5E RID: 3678 RVA: 0x00006138 File Offset: 0x00004338
		[Token(Token = "0x6000D31")]
		[Address(RVA = "0x1011421E8", Offset = "0x11421E8", VA = "0x1011421E8")]
		private bool TurnEndPreProcess_StatusChange(CharacterHandler executor, CharacterHandler receiver)
		{
			return default(bool);
		}

		// Token: 0x06000E5F RID: 3679 RVA: 0x00006150 File Offset: 0x00004350
		[Token(Token = "0x6000D32")]
		[Address(RVA = "0x101142740", Offset = "0x1142740", VA = "0x101142740")]
		private bool TurnEndPreProcess_CounterStatusChange(CharacterHandler topCharaHndl)
		{
			return default(bool);
		}

		// Token: 0x06000E60 RID: 3680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D33")]
		[Address(RVA = "0x1011429B0", Offset = "0x11429B0", VA = "0x1011429B0")]
		private void TurnEndPreProcess(BattleDefine.eTurnPreEndEvent type)
		{
		}

		// Token: 0x06000E61 RID: 3681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D34")]
		[Address(RVA = "0x101142B20", Offset = "0x1142B20", VA = "0x101142B20")]
		private void OrderSlide()
		{
		}

		// Token: 0x06000E62 RID: 3682 RVA: 0x00006168 File Offset: 0x00004368
		[Token(Token = "0x6000D35")]
		[Address(RVA = "0x101143298", Offset = "0x1143298", VA = "0x101143298")]
		private bool CommandInputStart()
		{
			return default(bool);
		}

		// Token: 0x06000E63 RID: 3683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D36")]
		[Address(RVA = "0x101143468", Offset = "0x1143468", VA = "0x101143468")]
		private void CommandDecisonByConfusion()
		{
		}

		// Token: 0x06000E64 RID: 3684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D37")]
		[Address(RVA = "0x1011436AC", Offset = "0x11436AC", VA = "0x1011436AC")]
		private void CommandDecisonByAuto()
		{
		}

		// Token: 0x06000E65 RID: 3685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D38")]
		[Address(RVA = "0x101143B34", Offset = "0x1143B34", VA = "0x101143B34")]
		private void CommandDecisionByAI()
		{
		}

		// Token: 0x06000E66 RID: 3686 RVA: 0x00006180 File Offset: 0x00004380
		[Token(Token = "0x6000D39")]
		[Address(RVA = "0x101143D44", Offset = "0x1143D44", VA = "0x101143D44")]
		private bool ExecCharaCutInOfTurnOwner()
		{
			return default(bool);
		}

		// Token: 0x06000E67 RID: 3687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D3A")]
		[Address(RVA = "0x101143F60", Offset = "0x1143F60", VA = "0x101143F60")]
		private void ExecCommandOfTurnOwner()
		{
		}

		// Token: 0x06000E68 RID: 3688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D3B")]
		[Address(RVA = "0x1011440D0", Offset = "0x11440D0", VA = "0x1011440D0")]
		private void ExecSkillCard()
		{
		}

		// Token: 0x06000E69 RID: 3689 RVA: 0x00006198 File Offset: 0x00004398
		[Token(Token = "0x6000D3C")]
		[Address(RVA = "0x101144620", Offset = "0x1144620", VA = "0x101144620")]
		private bool IsCompleteExecCommandAnim()
		{
			return default(bool);
		}

		// Token: 0x06000E6A RID: 3690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D3D")]
		[Address(RVA = "0x10113FAEC", Offset = "0x113FAEC", VA = "0x10113FAEC")]
		private void ClearExecCommand()
		{
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D3E")]
		[Address(RVA = "0x10114470C", Offset = "0x114470C", VA = "0x10114470C")]
		private void PostAfterExecCommand(BattleCommandSolveResult solveResult)
		{
		}

		// Token: 0x06000E6C RID: 3692 RVA: 0x000061B0 File Offset: 0x000043B0
		[Token(Token = "0x6000D3F")]
		[Address(RVA = "0x101144EA4", Offset = "0x1144EA4", VA = "0x101144EA4")]
		private bool IsCompletePostAfterExecCommand()
		{
			return default(bool);
		}

		// Token: 0x06000E6D RID: 3693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D40")]
		[Address(RVA = "0x10114502C", Offset = "0x114502C", VA = "0x10114502C")]
		private void OnEndDeadAnimation(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000E6E RID: 3694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D41")]
		[Address(RVA = "0x1011453A8", Offset = "0x11453A8", VA = "0x1011453A8")]
		private void OnCompleteDropItem(BattleDropItem dropItem)
		{
		}

		// Token: 0x06000E6F RID: 3695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D42")]
		[Address(RVA = "0x1011454B8", Offset = "0x11454B8", VA = "0x1011454B8")]
		public void JudgeAndRequestLastBlow(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000E70 RID: 3696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D43")]
		[Address(RVA = "0x1011455A4", Offset = "0x11455A4", VA = "0x1011455A4")]
		public void CalcTogetherAttackGauge(float value)
		{
		}

		// Token: 0x06000E71 RID: 3697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D44")]
		[Address(RVA = "0x101133F5C", Offset = "0x1133F5C", VA = "0x101133F5C")]
		public void AddTogetherAttackGauge(float value)
		{
		}

		// Token: 0x06000E72 RID: 3698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D45")]
		[Address(RVA = "0x101140B38", Offset = "0x1140B38", VA = "0x101140B38")]
		public void CalcTogetherAttackGaugeBuffer()
		{
		}

		// Token: 0x06000E73 RID: 3699 RVA: 0x000061C8 File Offset: 0x000043C8
		[Token(Token = "0x6000D46")]
		[Address(RVA = "0x101145694", Offset = "0x1145694", VA = "0x101145694")]
		private bool CanBeContinueTogetherAttack()
		{
			return default(bool);
		}

		// Token: 0x06000E74 RID: 3700 RVA: 0x000061E0 File Offset: 0x000043E0
		[Token(Token = "0x6000D47")]
		[Address(RVA = "0x101145718", Offset = "0x1145718", VA = "0x101145718")]
		private bool IsExistUniqueSkillSceneOfTurnOwner()
		{
			return default(bool);
		}

		// Token: 0x06000E75 RID: 3701 RVA: 0x000061F8 File Offset: 0x000043F8
		[Token(Token = "0x6000D48")]
		[Address(RVA = "0x101145838", Offset = "0x1145838", VA = "0x101145838")]
		private bool ExecTogetherAttackOfTurnOwner()
		{
			return default(bool);
		}

		// Token: 0x06000E76 RID: 3702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D49")]
		[Address(RVA = "0x101145C58", Offset = "0x1145C58", VA = "0x101145C58")]
		private void ExecTogetherAttackOfTurnOwnerNotEffect()
		{
		}

		// Token: 0x06000E77 RID: 3703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D4A")]
		[Address(RVA = "0x101145DDC", Offset = "0x1145DDC", VA = "0x101145DDC")]
		private void DisplaySkillNameTogetherAttackOfTurnOwner()
		{
		}

		// Token: 0x06000E78 RID: 3704 RVA: 0x00006210 File Offset: 0x00004410
		[Token(Token = "0x6000D4B")]
		[Address(RVA = "0x101145F74", Offset = "0x1145F74", VA = "0x101145F74")]
		private bool PostTogetherAttackOfTurnOwner()
		{
			return default(bool);
		}

		// Token: 0x06000E79 RID: 3705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D4C")]
		[Address(RVA = "0x101146244", Offset = "0x1146244", VA = "0x101146244")]
		private void ReturnFromCancelTogetherAttack()
		{
		}

		// Token: 0x06000E7A RID: 3706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D4D")]
		[Address(RVA = "0x1011465A8", Offset = "0x11465A8", VA = "0x1011465A8")]
		private void SetupMasterOrb()
		{
		}

		// Token: 0x06000E7B RID: 3707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D4E")]
		[Address(RVA = "0x1011466C0", Offset = "0x11466C0", VA = "0x1011466C0")]
		private void ExecMasterSkill()
		{
		}

		// Token: 0x06000E7C RID: 3708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D4F")]
		[Address(RVA = "0x1011469DC", Offset = "0x11469DC", VA = "0x1011469DC")]
		private void ChangePlayerJoinMember_GotoBench()
		{
		}

		// Token: 0x06000E7D RID: 3709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D50")]
		[Address(RVA = "0x101146A90", Offset = "0x1146A90", VA = "0x101146A90")]
		private void ChangePlayerJoinMember_GotoBenchMove(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x00006228 File Offset: 0x00004428
		[Token(Token = "0x6000D51")]
		[Address(RVA = "0x101146B90", Offset = "0x1146B90", VA = "0x101146B90")]
		private bool IsCompleteGotoBench()
		{
			return default(bool);
		}

		// Token: 0x06000E7F RID: 3711 RVA: 0x00006240 File Offset: 0x00004440
		[Token(Token = "0x6000D52")]
		[Address(RVA = "0x101146C5C", Offset = "0x1146C5C", VA = "0x101146C5C")]
		private bool ChangePlayerJoinMember_GotoJoin()
		{
			return default(bool);
		}

		// Token: 0x06000E80 RID: 3712 RVA: 0x00006258 File Offset: 0x00004458
		[Token(Token = "0x6000D53")]
		[Address(RVA = "0x101146F24", Offset = "0x1146F24", VA = "0x101146F24")]
		private bool IsDonePrepareChangePlayerJoinMember()
		{
			return default(bool);
		}

		// Token: 0x06000E81 RID: 3713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D54")]
		[Address(RVA = "0x101146DD4", Offset = "0x1146DD4", VA = "0x101146DD4")]
		private void ChangePlayerJoinMember_GotoJoinMove(CharacterHandler charaHndl, int locatorIndex)
		{
		}

		// Token: 0x06000E82 RID: 3714 RVA: 0x00006270 File Offset: 0x00004470
		[Token(Token = "0x6000D55")]
		[Address(RVA = "0x101146FF8", Offset = "0x1146FF8", VA = "0x101146FF8")]
		private bool IsCompleteGotoJoin()
		{
			return default(bool);
		}

		// Token: 0x06000E83 RID: 3715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D56")]
		[Address(RVA = "0x1011470FC", Offset = "0x11470FC", VA = "0x1011470FC")]
		private void ChangePlayerJoinMember()
		{
		}

		// Token: 0x06000E84 RID: 3716 RVA: 0x00006288 File Offset: 0x00004488
		[Token(Token = "0x6000D57")]
		[Address(RVA = "0x101147938", Offset = "0x1147938", VA = "0x101147938")]
		private bool CalcAutoMemberChange()
		{
			return default(bool);
		}

		// Token: 0x06000E85 RID: 3717 RVA: 0x000062A0 File Offset: 0x000044A0
		[Token(Token = "0x6000D58")]
		[Address(RVA = "0x101147D94", Offset = "0x1147D94", VA = "0x101147D94")]
		private bool ChangePlayerJoinMember_GotoJoinOnPlayerDead()
		{
			return default(bool);
		}

		// Token: 0x06000E86 RID: 3718 RVA: 0x000062B8 File Offset: 0x000044B8
		[Token(Token = "0x6000D59")]
		[Address(RVA = "0x101147FDC", Offset = "0x1147FDC", VA = "0x101147FDC")]
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnPlayerDead()
		{
			return default(bool);
		}

		// Token: 0x06000E87 RID: 3719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D5A")]
		[Address(RVA = "0x10114811C", Offset = "0x114811C", VA = "0x10114811C")]
		private void ChangePlayerDeadMember()
		{
		}

		// Token: 0x06000E88 RID: 3720 RVA: 0x000062D0 File Offset: 0x000044D0
		[Token(Token = "0x6000D5B")]
		[Address(RVA = "0x1011483E0", Offset = "0x11483E0", VA = "0x1011483E0")]
		private bool InterruptFriendJoinSelect()
		{
			return default(bool);
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x000062E8 File Offset: 0x000044E8
		[Token(Token = "0x6000D5C")]
		[Address(RVA = "0x1011489F0", Offset = "0x11489F0", VA = "0x1011489F0")]
		private BattleSystem.eMainStep ReturnFromCancelInterruptFriend()
		{
			return BattleSystem.eMainStep.First;
		}

		// Token: 0x06000E8A RID: 3722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D5D")]
		[Address(RVA = "0x101148BE0", Offset = "0x1148BE0", VA = "0x101148BE0")]
		private void UpdateTouchChangeInterruptFriedJoin()
		{
		}

		// Token: 0x06000E8B RID: 3723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D5E")]
		[Address(RVA = "0x1011486B4", Offset = "0x11486B4", VA = "0x1011486B4")]
		private void ExecInterruptFriend()
		{
		}

		// Token: 0x06000E8C RID: 3724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D5F")]
		[Address(RVA = "0x101148F30", Offset = "0x1148F30", VA = "0x101148F30")]
		private void ChangePlayerJoinMember_GotoBenchOnInterruputFriend()
		{
		}

		// Token: 0x06000E8D RID: 3725 RVA: 0x00006300 File Offset: 0x00004500
		[Token(Token = "0x6000D60")]
		[Address(RVA = "0x10114904C", Offset = "0x114904C", VA = "0x10114904C")]
		private bool IsCompleteGotoBenchOnInterruputFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E8E RID: 3726 RVA: 0x00006318 File Offset: 0x00004518
		[Token(Token = "0x6000D61")]
		[Address(RVA = "0x101149184", Offset = "0x1149184", VA = "0x101149184")]
		private bool ChangePlayerJoinMember_GotoJoinOnInterruputFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E8F RID: 3727 RVA: 0x00006330 File Offset: 0x00004530
		[Token(Token = "0x6000D62")]
		[Address(RVA = "0x101149294", Offset = "0x1149294", VA = "0x101149294")]
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnInterruputFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E90 RID: 3728 RVA: 0x00006348 File Offset: 0x00004548
		[Token(Token = "0x6000D63")]
		[Address(RVA = "0x10114934C", Offset = "0x114934C", VA = "0x10114934C")]
		private bool IsCompleteGotoJoinOnInterruputFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E91 RID: 3729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D64")]
		[Address(RVA = "0x10114941C", Offset = "0x114941C", VA = "0x10114941C")]
		private void ChangePlayerJoinMemberOnInterruputFriend()
		{
		}

		// Token: 0x06000E92 RID: 3730 RVA: 0x00006360 File Offset: 0x00004560
		[Token(Token = "0x6000D65")]
		[Address(RVA = "0x1011497A4", Offset = "0x11497A4", VA = "0x1011497A4")]
		public bool IsAliveJoiningFriendChara()
		{
			return default(bool);
		}

		// Token: 0x06000E93 RID: 3731 RVA: 0x00006378 File Offset: 0x00004578
		[Token(Token = "0x6000D66")]
		[Address(RVA = "0x101149910", Offset = "0x1149910", VA = "0x101149910")]
		private bool CalcGoBackFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E94 RID: 3732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D67")]
		[Address(RVA = "0x101149A18", Offset = "0x1149A18", VA = "0x101149A18")]
		private void ChangePlayerJoinMember_GotoBenchOnGoBackFriend()
		{
		}

		// Token: 0x06000E95 RID: 3733 RVA: 0x00006390 File Offset: 0x00004590
		[Token(Token = "0x6000D68")]
		[Address(RVA = "0x101149B50", Offset = "0x1149B50", VA = "0x101149B50")]
		private bool IsCompleteGotoBenchOnGoBackFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x000063A8 File Offset: 0x000045A8
		[Token(Token = "0x6000D69")]
		[Address(RVA = "0x101149C6C", Offset = "0x1149C6C", VA = "0x101149C6C")]
		private bool ChangePlayerJoinMember_GotoJoinOnGoBackFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x000063C0 File Offset: 0x000045C0
		[Token(Token = "0x6000D6A")]
		[Address(RVA = "0x101149E3C", Offset = "0x1149E3C", VA = "0x101149E3C")]
		private bool IsDonePrepareChangePlayerJoinMember_GotoJoinOnGoBackFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x000063D8 File Offset: 0x000045D8
		[Token(Token = "0x6000D6B")]
		[Address(RVA = "0x101149EF8", Offset = "0x1149EF8", VA = "0x101149EF8")]
		private bool IsCompleteGotoJoinOnGoBackFriend()
		{
			return default(bool);
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D6C")]
		[Address(RVA = "0x101149FCC", Offset = "0x1149FCC", VA = "0x101149FCC")]
		private void ChangePlayerJoinMemberOnGoBackFriend()
		{
		}

		// Token: 0x06000E9A RID: 3738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D6D")]
		[Address(RVA = "0x10114A470", Offset = "0x114A470", VA = "0x10114A470")]
		private void UpdateTouchChangeTarget()
		{
		}

		// Token: 0x06000E9B RID: 3739 RVA: 0x000063F0 File Offset: 0x000045F0
		[Token(Token = "0x6000D6E")]
		[Address(RVA = "0x10114AC74", Offset = "0x114AC74", VA = "0x10114AC74")]
		private bool UpdateTouchChangeTarget_TutorialTgtSingle(CharacterHandler touchCharaHndl)
		{
			return default(bool);
		}

		// Token: 0x06000E9C RID: 3740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D6F")]
		[Address(RVA = "0x10114BD74", Offset = "0x114BD74", VA = "0x10114BD74")]
		private void UpdateTouchChangeTargetOnTogetherAttack()
		{
		}

		// Token: 0x06000E9D RID: 3741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D70")]
		[Address(RVA = "0x10114C138", Offset = "0x114C138", VA = "0x10114C138")]
		private void UpdateTouchChangeTargetOnViewStatusWindow()
		{
		}

		// Token: 0x06000E9E RID: 3742 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000D71")]
		[Address(RVA = "0x10114AAA8", Offset = "0x114AAA8", VA = "0x10114AAA8")]
		private CharacterHandler GetRayCastCharaHndl(Vector2 screenPoint)
		{
			return null;
		}

		// Token: 0x06000E9F RID: 3743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D72")]
		[Address(RVA = "0x10114C530", Offset = "0x114C530", VA = "0x10114C530")]
		private void InitPlacementParty(bool isBackStart = true)
		{
		}

		// Token: 0x06000EA0 RID: 3744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D73")]
		[Address(RVA = "0x10114CB54", Offset = "0x114CB54", VA = "0x10114CB54")]
		private void WaveInParty()
		{
		}

		// Token: 0x06000EA1 RID: 3745 RVA: 0x00006408 File Offset: 0x00004608
		[Token(Token = "0x6000D74")]
		[Address(RVA = "0x10114CF54", Offset = "0x114CF54", VA = "0x10114CF54")]
		private bool IsCompleteWaveInParty()
		{
			return default(bool);
		}

		// Token: 0x06000EA2 RID: 3746 RVA: 0x00006420 File Offset: 0x00004620
		[Token(Token = "0x6000D75")]
		[Address(RVA = "0x10114D094", Offset = "0x114D094", VA = "0x10114D094")]
		private bool CheckTransitWaveOut()
		{
			return default(bool);
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D76")]
		[Address(RVA = "0x10114D1DC", Offset = "0x114D1DC", VA = "0x10114D1DC")]
		private void WaveOutPartyJoinOnlyFromCurrentPos()
		{
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x00006438 File Offset: 0x00004638
		[Token(Token = "0x6000D77")]
		[Address(RVA = "0x10114D408", Offset = "0x114D408", VA = "0x10114D408")]
		private bool IsCompleteWaveOutParty()
		{
			return default(bool);
		}

		// Token: 0x06000EA5 RID: 3749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D78")]
		[Address(RVA = "0x10112B0EC", Offset = "0x112B0EC", VA = "0x10112B0EC")]
		public void RequestSort()
		{
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D79")]
		[Address(RVA = "0x10114D548", Offset = "0x114D548", VA = "0x10114D548")]
		private void UpdateSort()
		{
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x00006450 File Offset: 0x00004650
		[Token(Token = "0x6000D7A")]
		[Address(RVA = "0x10114E338", Offset = "0x114E338", VA = "0x10114E338")]
		private static int CompareSortData_CharaOnly(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			return 0;
		}

		// Token: 0x06000EA8 RID: 3752 RVA: 0x00006468 File Offset: 0x00004668
		[Token(Token = "0x6000D7B")]
		[Address(RVA = "0x10114E418", Offset = "0x114E418", VA = "0x10114E418")]
		private static int CompareSortData_KiraraJumpScene(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			return 0;
		}

		// Token: 0x06000EA9 RID: 3753 RVA: 0x00006480 File Offset: 0x00004680
		[Token(Token = "0x6000D7C")]
		[Address(RVA = "0x10114E4F8", Offset = "0x114E4F8", VA = "0x10114E4F8")]
		private static int CompareSortData_UniqueSkillScene(BattleSystem.SortData A, BattleSystem.SortData B)
		{
			return 0;
		}

		// Token: 0x06000EAA RID: 3754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D7D")]
		[Address(RVA = "0x10112B0F8", Offset = "0x112B0F8", VA = "0x10112B0F8")]
		public void KiraraJumpScenePreProcess()
		{
		}

		// Token: 0x06000EAB RID: 3755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D7E")]
		[Address(RVA = "0x10112B08C", Offset = "0x112B08C", VA = "0x10112B08C")]
		public void RevertKiraraJumpSceneToSystem()
		{
		}

		// Token: 0x06000EAC RID: 3756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D7F")]
		[Address(RVA = "0x10114E698", Offset = "0x114E698", VA = "0x10114E698")]
		public void RequestKiraraJumpVoice()
		{
		}

		// Token: 0x06000EAD RID: 3757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D80")]
		[Address(RVA = "0x10114E7D4", Offset = "0x114E7D4", VA = "0x10114E7D4")]
		public void UniqueSkillScenePreProcess()
		{
		}

		// Token: 0x06000EAE RID: 3758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D81")]
		[Address(RVA = "0x10114E828", Offset = "0x114E828", VA = "0x10114E828")]
		public void RevertUniqueSkillSceneToSystem()
		{
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D82")]
		[Address(RVA = "0x10114E888", Offset = "0x114E888", VA = "0x10114E888")]
		private void ExecRetry()
		{
		}

		// Token: 0x06000EB0 RID: 3760 RVA: 0x00006498 File Offset: 0x00004698
		[Token(Token = "0x6000D83")]
		[Address(RVA = "0x10114ED18", Offset = "0x114ED18", VA = "0x10114ED18")]
		private bool IsDonePrepareOnRetry()
		{
			return default(bool);
		}

		// Token: 0x06000EB1 RID: 3761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D84")]
		[Address(RVA = "0x101137BB0", Offset = "0x1137BB0", VA = "0x101137BB0")]
		public void CalcResultPreRequest()
		{
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000D85")]
		[Address(RVA = "0x101144D94", Offset = "0x1144D94", VA = "0x101144D94")]
		private BattleDropItem JudgeDropItem(int waveIndex, BattleDefine.eJoinMember join)
		{
			return null;
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D86")]
		[Address(RVA = "0x10114EE04", Offset = "0x114EE04", VA = "0x10114EE04")]
		private void RequestSaveOnWaveStart()
		{
		}

		// Token: 0x06000EB4 RID: 3764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D87")]
		[Address(RVA = "0x10113FF00", Offset = "0x113FF00", VA = "0x10113FF00")]
		private void RequestSaveIfFailed()
		{
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000EB5 RID: 3765 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C3")]
		public BattleUI BattleUI
		{
			[Token(Token = "0x6000D88")]
			[Address(RVA = "0x10112B084", Offset = "0x112B084", VA = "0x10112B084")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D89")]
		[Address(RVA = "0x10114EE8C", Offset = "0x114EE8C", VA = "0x10114EE8C")]
		private void PrecedeInitializeBattleUI()
		{
		}

		// Token: 0x06000EB7 RID: 3767 RVA: 0x000064B0 File Offset: 0x000046B0
		[Token(Token = "0x6000D8A")]
		[Address(RVA = "0x10114EFB0", Offset = "0x114EFB0", VA = "0x10114EFB0")]
		private bool IsDonePrecedeInitializeBattleUI()
		{
			return default(bool);
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x000064C8 File Offset: 0x000046C8
		[Token(Token = "0x6000D8B")]
		[Address(RVA = "0x10114F0B0", Offset = "0x114F0B0", VA = "0x10114F0B0")]
		private bool InitializeBattleUI()
		{
			return default(bool);
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D8C")]
		[Address(RVA = "0x10114FA10", Offset = "0x114FA10", VA = "0x10114FA10")]
		private void SetupBattleUIOnWaveStart()
		{
		}

		// Token: 0x06000EBA RID: 3770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D8D")]
		[Address(RVA = "0x10113FCE0", Offset = "0x113FCE0", VA = "0x10113FCE0")]
		private void SetupBattleUIOnTurnStart(CharacterHandler topCharaHndl)
		{
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D8E")]
		[Address(RVA = "0x10114FF4C", Offset = "0x114FF4C", VA = "0x10114FF4C")]
		private void CloseBattleUIOnPlayerTurn()
		{
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D8F")]
		[Address(RVA = "0x10114091C", Offset = "0x114091C", VA = "0x10114091C")]
		public void UpdateAllCharacterStatus(BattleSystem.eUIEnableRenderChange enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
		}

		// Token: 0x06000EBD RID: 3773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D90")]
		[Address(RVA = "0x101141888", Offset = "0x1141888", VA = "0x101141888")]
		public void UpdateUICharaStatus(BattleDefine.ePartyType partyType, BattleDefine.eJoinMember join, BattleSystem.eUIEnableRenderChange enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
		}

		// Token: 0x06000EBE RID: 3774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D91")]
		[Address(RVA = "0x1011474C0", Offset = "0x11474C0", VA = "0x1011474C0")]
		private void UpdateUIPlayerStatus(BattleDefine.eJoinMember join, BattleSystem.eUIEnableRenderChange enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
		}

		// Token: 0x06000EBF RID: 3775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D92")]
		[Address(RVA = "0x10114FF8C", Offset = "0x114FF8C", VA = "0x10114FF8C")]
		private void UpdateUIEnemyStatus(BattleDefine.eJoinMember join, BattleSystem.eUIEnableRenderChange enableRenderChange, bool isMoveHpBar = false, bool isUpdateIconOnly = false)
		{
		}

		// Token: 0x06000EC0 RID: 3776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D93")]
		[Address(RVA = "0x10114FD6C", Offset = "0x114FD6C", VA = "0x10114FD6C")]
		public void UpdateUIStun(CharacterHandler charaHndl, bool isMoveBar)
		{
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D94")]
		[Address(RVA = "0x1011463A8", Offset = "0x11463A8", VA = "0x1011463A8")]
		private void SetTurnOwnerMarker(bool flg)
		{
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D95")]
		[Address(RVA = "0x10114B06C", Offset = "0x114B06C", VA = "0x10114B06C")]
		private void UpdateTargetMarker(BattleDefine.eJoinMember join, eSkillTargetType skillType = eSkillTargetType.Self, bool isUseUniqueSkill = false)
		{
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D96")]
		[Address(RVA = "0x10114C3A4", Offset = "0x114C3A4", VA = "0x10114C3A4")]
		private void UpdateTargetMarkerOnViewStatusWindow(CharacterHandler touchCharaHndl, bool isIgnorePlaySE)
		{
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D97")]
		[Address(RVA = "0x101148814", Offset = "0x1148814", VA = "0x101148814")]
		private void UpdateTargetMarkerOnInterruptFriend(BattleDefine.eJoinMember join)
		{
		}

		// Token: 0x06000EC5 RID: 3781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D98")]
		[Address(RVA = "0x101142CF0", Offset = "0x1142CF0", VA = "0x101142CF0")]
		private void SetupCommandButton(bool isUnSelect, bool isUpdateSlideUIOnUnSelect = true)
		{
		}

		// Token: 0x06000EC6 RID: 3782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D99")]
		[Address(RVA = "0x10114379C", Offset = "0x114379C", VA = "0x10114379C")]
		private void SelectCommandButton(int commandIndex, bool isUpdateOrder = true)
		{
		}

		// Token: 0x06000EC7 RID: 3783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D9A")]
		[Address(RVA = "0x1011504C0", Offset = "0x11504C0", VA = "0x1011504C0")]
		private void OnChangeCommandButton(int commandIndex, bool isSilence)
		{
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D9B")]
		[Address(RVA = "0x1011508E8", Offset = "0x11508E8", VA = "0x1011508E8")]
		private void OnClickCommandButton(int newCommandIndex)
		{
		}

		// Token: 0x06000EC9 RID: 3785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D9C")]
		[Address(RVA = "0x101150A68", Offset = "0x1150A68", VA = "0x101150A68")]
		private void OnClickFriendButton()
		{
		}

		// Token: 0x06000ECA RID: 3786 RVA: 0x000064E0 File Offset: 0x000046E0
		[Token(Token = "0x6000D9D")]
		[Address(RVA = "0x101150AE4", Offset = "0x1150AE4", VA = "0x101150AE4")]
		private bool OnJudgeInterruptFriendJoinSelect()
		{
			return default(bool);
		}

		// Token: 0x06000ECB RID: 3787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D9E")]
		[Address(RVA = "0x101150CD4", Offset = "0x1150CD4", VA = "0x101150CD4")]
		private void OnDecideInterruptFriendJoinSelect()
		{
		}

		// Token: 0x06000ECC RID: 3788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000D9F")]
		[Address(RVA = "0x101150D0C", Offset = "0x1150D0C", VA = "0x101150D0C")]
		private void OnCancelInterruptFriendJoinSelect()
		{
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA0")]
		[Address(RVA = "0x101150D44", Offset = "0x1150D44", VA = "0x101150D44")]
		private void OnMemberChange()
		{
		}

		// Token: 0x06000ECE RID: 3790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA1")]
		[Address(RVA = "0x101150DC0", Offset = "0x1150DC0", VA = "0x101150DC0")]
		private void OnSelectMemberChange(int index)
		{
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA2")]
		[Address(RVA = "0x101150E70", Offset = "0x1150E70", VA = "0x101150E70")]
		private void OnCancelMemberChange()
		{
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA3")]
		[Address(RVA = "0x101150EA4", Offset = "0x1150EA4", VA = "0x101150EA4")]
		private void OnSelectMasterSkill(int masterSkillIndex, BattleDefine.eJoinMember singleTargetIndex)
		{
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA4")]
		[Address(RVA = "0x101150EE8", Offset = "0x1150EE8", VA = "0x101150EE8")]
		private void OnClickTogetherButton()
		{
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA5")]
		[Address(RVA = "0x101151110", Offset = "0x1151110", VA = "0x101151110")]
		private void OnDecideTogetherMember(BattleDefine.eJoinMember join)
		{
		}

		// Token: 0x06000ED3 RID: 3795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA6")]
		[Address(RVA = "0x101151114", Offset = "0x1151114", VA = "0x101151114")]
		private void OnDecideTogether(List<BattleDefine.eJoinMember> joins)
		{
		}

		// Token: 0x06000ED4 RID: 3796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA7")]
		[Address(RVA = "0x1011511C8", Offset = "0x11511C8", VA = "0x1011511C8")]
		private void OnCancelTogether()
		{
		}

		// Token: 0x06000ED5 RID: 3797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA8")]
		[Address(RVA = "0x101151280", Offset = "0x1151280", VA = "0x101151280")]
		private void OpenMasterMenu()
		{
		}

		// Token: 0x06000ED6 RID: 3798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DA9")]
		[Address(RVA = "0x101151984", Offset = "0x1151984", VA = "0x101151984")]
		private void CloseMasterMenu(bool isReturnToInputCommand)
		{
		}

		// Token: 0x06000ED7 RID: 3799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAA")]
		[Address(RVA = "0x101151B28", Offset = "0x1151B28", VA = "0x101151B28")]
		private void OpenTogetherAttackMenu()
		{
		}

		// Token: 0x06000ED8 RID: 3800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAB")]
		[Address(RVA = "0x10115214C", Offset = "0x115214C", VA = "0x10115214C")]
		private void OpenViewStatusWindow()
		{
		}

		// Token: 0x06000ED9 RID: 3801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAC")]
		[Address(RVA = "0x101152248", Offset = "0x1152248", VA = "0x101152248")]
		private void OnCancelStatusWindow()
		{
		}

		// Token: 0x06000EDA RID: 3802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAD")]
		[Address(RVA = "0x101152304", Offset = "0x1152304", VA = "0x101152304")]
		private void UpdateTouchCancleAuto()
		{
		}

		// Token: 0x06000EDB RID: 3803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAE")]
		[Address(RVA = "0x1011524EC", Offset = "0x11524EC", VA = "0x1011524EC")]
		private void OnClickAutoButton()
		{
		}

		// Token: 0x06000EDC RID: 3804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DAF")]
		[Address(RVA = "0x101152478", Offset = "0x1152478", VA = "0x101152478")]
		private void ToggleAuto()
		{
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB0")]
		[Address(RVA = "0x1011524F0", Offset = "0x11524F0", VA = "0x1011524F0")]
		private void OnClickSpeedButton()
		{
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB1")]
		[Address(RVA = "0x1011524F4", Offset = "0x11524F4", VA = "0x1011524F4")]
		private void ToggleBattleSpeedLvMasterFlg()
		{
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB2")]
		[Address(RVA = "0x1011525A4", Offset = "0x11525A4", VA = "0x1011525A4")]
		private void OnClickMenuButton()
		{
		}

		// Token: 0x06000EE0 RID: 3808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB3")]
		[Address(RVA = "0x1011525D8", Offset = "0x11525D8", VA = "0x1011525D8")]
		private void OnCancelBattleMenuWindow()
		{
		}

		// Token: 0x06000EE1 RID: 3809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB4")]
		[Address(RVA = "0x10115264C", Offset = "0x115264C", VA = "0x10115264C")]
		private void OnClickRetireButton()
		{
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB5")]
		[Address(RVA = "0x101152718", Offset = "0x1152718", VA = "0x101152718")]
		private void OnClosedWindow_BattleRetire(int answer)
		{
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB6")]
		[Address(RVA = "0x1011527E8", Offset = "0x11527E8", VA = "0x1011527E8")]
		private void OpenRetryWindow()
		{
		}

		// Token: 0x06000EE4 RID: 3812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB7")]
		[Address(RVA = "0x101152B94", Offset = "0x1152B94", VA = "0x101152B94")]
		private void OnClosedWindow_BattleRetry(int answer)
		{
		}

		// Token: 0x06000EE5 RID: 3813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DB8")]
		[Address(RVA = "0x10113A0E0", Offset = "0x113A0E0", VA = "0x10113A0E0")]
		private void SetDestroyStep(BattleSystem.eDestroyStep step)
		{
		}

		// Token: 0x06000EE6 RID: 3814 RVA: 0x000064F8 File Offset: 0x000046F8
		[Token(Token = "0x6000DB9")]
		[Address(RVA = "0x10113ED80", Offset = "0x113ED80", VA = "0x10113ED80")]
		private BattleSystem.eMode Update_Destroy()
		{
			return BattleSystem.eMode.Prepare;
		}

		// Token: 0x06000EE7 RID: 3815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DBA")]
		[Address(RVA = "0x10113F418", Offset = "0x113F418", VA = "0x10113F418")]
		private void SetMainStep(BattleSystem.eMainStep step)
		{
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x00006510 File Offset: 0x00004710
		[Token(Token = "0x6000DBB")]
		[Address(RVA = "0x101152C04", Offset = "0x1152C04", VA = "0x101152C04")]
		private bool CheckAndShiftBattlePhase(bool isWaveIn)
		{
			return default(bool);
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x00006528 File Offset: 0x00004728
		[Token(Token = "0x6000DBC")]
		[Address(RVA = "0x10113AC18", Offset = "0x113AC18", VA = "0x10113AC18")]
		private BattleSystem.eMode Update_Main()
		{
			return BattleSystem.eMode.Prepare;
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DBD")]
		[Address(RVA = "0x10113F5D0", Offset = "0x113F5D0", VA = "0x10113F5D0")]
		private void LateUpdate_Main()
		{
		}

		// Token: 0x06000EEB RID: 3819 RVA: 0x00006540 File Offset: 0x00004740
		[Token(Token = "0x6000DBE")]
		[Address(RVA = "0x101152D8C", Offset = "0x1152D8C", VA = "0x101152D8C")]
		private bool OnResponse_QuestRetry(MeigewwwParam wwwParam, Questlogretry param)
		{
			return default(bool);
		}

		// Token: 0x06000EEC RID: 3820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DBF")]
		[Address(RVA = "0x101152F9C", Offset = "0x1152F9C", VA = "0x101152F9C")]
		private void OnConfirmPurchaseGem(int answer)
		{
		}

		// Token: 0x06000EED RID: 3821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DC0")]
		[Address(RVA = "0x10115305C", Offset = "0x115305C", VA = "0x10115305C")]
		private void OnOpenedTutorialTipsWindow()
		{
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DC1")]
		[Address(RVA = "0x1011530C0", Offset = "0x11530C0", VA = "0x1011530C0")]
		private void OnCompleteTutorialTipsWindow()
		{
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DC2")]
		[Address(RVA = "0x10113A0D8", Offset = "0x113A0D8", VA = "0x10113A0D8")]
		private void SetPrepareStep(BattleSystem.ePrepareStep step)
		{
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x00006558 File Offset: 0x00004758
		[Token(Token = "0x6000DC3")]
		[Address(RVA = "0x10113A0E8", Offset = "0x113A0E8", VA = "0x10113A0E8")]
		private BattleSystem.eMode Update_Prepare()
		{
			return BattleSystem.eMode.Prepare;
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x00006570 File Offset: 0x00004770
		[Token(Token = "0x6000DC4")]
		[Address(RVA = "0x101153EB8", Offset = "0x1153EB8", VA = "0x101153EB8")]
		private bool StartPrepare_Sound()
		{
			return default(bool);
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x00006588 File Offset: 0x00004788
		[Token(Token = "0x6000DC5")]
		[Address(RVA = "0x101155188", Offset = "0x1155188", VA = "0x101155188")]
		private bool UpdatePrepare_Sound()
		{
			return default(bool);
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x000065A0 File Offset: 0x000047A0
		[Token(Token = "0x6000DC6")]
		[Address(RVA = "0x101153FBC", Offset = "0x1153FBC", VA = "0x101153FBC")]
		private bool StartPrepare_BG()
		{
			return default(bool);
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x000065B8 File Offset: 0x000047B8
		[Token(Token = "0x6000DC7")]
		[Address(RVA = "0x1011554A4", Offset = "0x11554A4", VA = "0x1011554A4")]
		private bool UpdatePrepare_BG()
		{
			return default(bool);
		}

		// Token: 0x06000EF5 RID: 3829 RVA: 0x000065D0 File Offset: 0x000047D0
		[Token(Token = "0x6000DC8")]
		[Address(RVA = "0x1011540D8", Offset = "0x11540D8", VA = "0x1011540D8")]
		private bool StartPrepare_AlwaysEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x000065E8 File Offset: 0x000047E8
		[Token(Token = "0x6000DC9")]
		[Address(RVA = "0x101155784", Offset = "0x1155784", VA = "0x101155784")]
		private bool UpdatePrepare_AlwaysEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EF7 RID: 3831 RVA: 0x00006600 File Offset: 0x00004800
		[Token(Token = "0x6000DCA")]
		[Address(RVA = "0x1011541E0", Offset = "0x11541E0", VA = "0x1011541E0")]
		private bool StartPrepare_Player()
		{
			return default(bool);
		}

		// Token: 0x06000EF8 RID: 3832 RVA: 0x00006618 File Offset: 0x00004818
		[Token(Token = "0x6000DCB")]
		[Address(RVA = "0x101155878", Offset = "0x1155878", VA = "0x101155878")]
		private bool UpdatePrepare_Player()
		{
			return default(bool);
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x00006630 File Offset: 0x00004830
		[Token(Token = "0x6000DCC")]
		[Address(RVA = "0x101156494", Offset = "0x1156494", VA = "0x101156494")]
		private bool StartPrepare_PlayerEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x00006648 File Offset: 0x00004848
		[Token(Token = "0x6000DCD")]
		[Address(RVA = "0x1011565C4", Offset = "0x11565C4", VA = "0x1011565C4")]
		private bool UpdatePrepare_PlayerEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x00006660 File Offset: 0x00004860
		[Token(Token = "0x6000DCE")]
		[Address(RVA = "0x101154C20", Offset = "0x1154C20", VA = "0x101154C20")]
		private bool StartPrepare_Enemy()
		{
			return default(bool);
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x00006678 File Offset: 0x00004878
		[Token(Token = "0x6000DCF")]
		[Address(RVA = "0x101155F80", Offset = "0x1155F80", VA = "0x101155F80")]
		private bool UpdatePrepare_Enemy()
		{
			return default(bool);
		}

		// Token: 0x06000EFD RID: 3837 RVA: 0x00006690 File Offset: 0x00004890
		[Token(Token = "0x6000DD0")]
		[Address(RVA = "0x101156508", Offset = "0x1156508", VA = "0x101156508")]
		private bool StartPrepare_EnemyEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EFE RID: 3838 RVA: 0x000066A8 File Offset: 0x000048A8
		[Token(Token = "0x6000DD1")]
		[Address(RVA = "0x101156668", Offset = "0x1156668", VA = "0x101156668")]
		private bool UpdatePrepare_EnemyEffect()
		{
			return default(bool);
		}

		// Token: 0x06000EFF RID: 3839 RVA: 0x000066C0 File Offset: 0x000048C0
		[Token(Token = "0x6000DD2")]
		[Address(RVA = "0x10115657C", Offset = "0x115657C", VA = "0x10115657C")]
		private bool StartPrepare_UI()
		{
			return default(bool);
		}

		// Token: 0x06000F00 RID: 3840 RVA: 0x000066D8 File Offset: 0x000048D8
		[Token(Token = "0x6000DD3")]
		[Address(RVA = "0x101156728", Offset = "0x1156728", VA = "0x101156728")]
		private bool UpdatePrepare_UI()
		{
			return default(bool);
		}

		// Token: 0x06000F01 RID: 3841 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000DD4")]
		[Address(RVA = "0x1011567C4", Offset = "0x11567C4", VA = "0x1011567C4")]
		private CharacterHandler InstantiateFriend(CharacterDefine.eFriendType friendType, int charaID, int charaLv, int limitBreak, int uniqueSkillLv, int[] classSkillLvs, int weaponID, int weaponLv, int weaponSkillLv, int friendShip, int arousalLv, int duplicatedCount, int abilityBoardID, int[] abilityEquipItemIDs)
		{
			return null;
		}

		// Token: 0x06000F02 RID: 3842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DD5")]
		[Address(RVA = "0x101156E44", Offset = "0x1156E44", VA = "0x101156E44")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135E20", Offset = "0x135E20")]
		private void StartStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DD6")]
		[Address(RVA = "0x101156E48", Offset = "0x1156E48", VA = "0x101156E48")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135E58", Offset = "0x135E58")]
		private void StartStopwatchForDebugPrepare(BattleSystem.ePrepareTarget prepareTarget)
		{
		}

		// Token: 0x06000F04 RID: 3844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DD7")]
		[Address(RVA = "0x101156E4C", Offset = "0x1156E4C", VA = "0x101156E4C")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135E90", Offset = "0x135E90")]
		private void StopStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DD8")]
		[Address(RVA = "0x101156E50", Offset = "0x1156E50", VA = "0x101156E50")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135EC8", Offset = "0x135EC8")]
		private void StopStopwatchForDebugPrepare(BattleSystem.ePrepareTarget prepareTarget)
		{
		}

		// Token: 0x06000F06 RID: 3846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DD9")]
		[Address(RVA = "0x101156E54", Offset = "0x1156E54", VA = "0x101156E54")]
		public BattleSystem()
		{
		}

		// Token: 0x04001040 RID: 4160
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000C22")]
		[SerializeField]
		private BattlePosLocator m_PosLocator_Player;

		// Token: 0x04001041 RID: 4161
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000C23")]
		[SerializeField]
		private BattlePosLocator[] m_PosLocator_Enemy;

		// Token: 0x04001042 RID: 4162
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000C24")]
		[SerializeField]
		private Transform m_CenterLocator_Player;

		// Token: 0x04001043 RID: 4163
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000C25")]
		[SerializeField]
		private Transform m_CenterLocator_Enemy;

		// Token: 0x04001044 RID: 4164
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000C26")]
		[SerializeField]
		private BattleFriendCutIn m_FriendCutIn;

		// Token: 0x04001045 RID: 4165
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000C27")]
		[SerializeField]
		private BattleCharaCutIn m_CharaCutIn;

		// Token: 0x04001046 RID: 4166
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000C28")]
		[SerializeField]
		private BattleTitleCutIn m_TitleCutIn;

		// Token: 0x04001047 RID: 4167
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000C29")]
		[SerializeField]
		private BattleUniqueSkillCutIn m_UniqueSkillCutIn;

		// Token: 0x04001048 RID: 4168
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000C2A")]
		[SerializeField]
		private Camera m_ResultCamera;

		// Token: 0x04001049 RID: 4169
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000C2B")]
		private BattleSystemData m_BSD;

		// Token: 0x0400104A RID: 4170
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000C2C")]
		private BattlePartyData[] m_Parties;

		// Token: 0x0400104B RID: 4171
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000C2D")]
		private bool m_IsAwaked;

		// Token: 0x0400104C RID: 4172
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000C2E")]
		private Transform m_ObjRoot;

		// Token: 0x0400104D RID: 4173
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000C2F")]
		private BattleBG m_BattleBG;

		// Token: 0x0400104E RID: 4174
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000C30")]
		private Camera m_MainCamera;

		// Token: 0x0400104F RID: 4175
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000C31")]
		private BattleCamera m_BattleCamera;

		// Token: 0x04001050 RID: 4176
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000C32")]
		private Transform m_MainCameraTransform;

		// Token: 0x04001051 RID: 4177
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000C33")]
		private SkillActionPlayer m_SkillActionPlayer;

		// Token: 0x04001052 RID: 4178
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000C34")]
		private SimpleTimer m_Timer;

		// Token: 0x04001053 RID: 4179
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000C35")]
		private string m_BgmCueName;

		// Token: 0x04001054 RID: 4180
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000C36")]
		private BattleOrder m_Order;

		// Token: 0x04001055 RID: 4181
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000C37")]
		private BattleResult m_Result;

		// Token: 0x04001056 RID: 4182
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000C38")]
		private QuestListDB_Param m_QuestParam;

		// Token: 0x04001057 RID: 4183
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4000C39")]
		private BattleInputData m_InputData;

		// Token: 0x04001058 RID: 4184
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4000C3A")]
		private BattleTutorial m_Tutorial;

		// Token: 0x04001059 RID: 4185
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4000C3B")]
		private BattleOption m_Option;

		// Token: 0x0400105A RID: 4186
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4000C3C")]
		private BattleTimeScaler m_Scaler;

		// Token: 0x0400105B RID: 4187
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4000C3D")]
		private BattleMasterOrbData m_MasterOrbData;

		// Token: 0x0400105C RID: 4188
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4000C3E")]
		private BattleCommandData m_ExecCommand;

		// Token: 0x0400105D RID: 4189
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4000C3F")]
		private BattleDropObjectManager m_DropObjectManager;

		// Token: 0x0400105E RID: 4190
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4000C40")]
		private Dictionary<CharacterHandler, BattleDropItem> m_DropItemsFromChara;

		// Token: 0x0400105F RID: 4191
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4000C41")]
		private BattleKiraraJumpScene m_KiraraJumpScene;

		// Token: 0x04001060 RID: 4192
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4000C42")]
		private BattleUniqueSkillScene m_UniqueSkillScene;

		// Token: 0x04001061 RID: 4193
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x4000C43")]
		private List<string> m_UniqueSkillSceneUnloadCueSheetNames;

		// Token: 0x04001062 RID: 4194
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x4000C44")]
		private CharacterHandler m_LastBlowCharaHndl;

		// Token: 0x04001063 RID: 4195
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x4000C45")]
		private bool m_IsRequestLastBlow;

		// Token: 0x04001064 RID: 4196
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A1")]
		[Token(Token = "0x4000C46")]
		private bool m_PlayedWarningVoice;

		// Token: 0x04001065 RID: 4197
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A2")]
		[Token(Token = "0x4000C47")]
		private bool m_ExecutedMemberChange;

		// Token: 0x04001066 RID: 4198
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A3")]
		[Token(Token = "0x4000C48")]
		private bool m_IsGoBackFriendCheck;

		// Token: 0x04001067 RID: 4199
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A4")]
		[Token(Token = "0x4000C49")]
		private bool m_IsEnableTouchChangeTarget;

		// Token: 0x04001068 RID: 4200
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A5")]
		[Token(Token = "0x4000C4A")]
		private bool m_IsAutoTogetherAttack;

		// Token: 0x04001069 RID: 4201
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A6")]
		[Token(Token = "0x4000C4B")]
		private bool m_IsAfterOrderSlide;

		// Token: 0x0400106A RID: 4202
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4000C4C")]
		private int m_InstaniatedFriendCharaID;

		// Token: 0x0400106B RID: 4203
		[Cpp2IlInjected.FieldOffset(Offset = "0x1AC")]
		[Token(Token = "0x4000C4D")]
		private float m_TogetherGaugeWillAdd;

		// Token: 0x0400106C RID: 4204
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4000C4E")]
		private float m_TogetherGaugeWillSub;

		// Token: 0x0400106D RID: 4205
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x4000C4F")]
		private List<BattleSystem.AutoMemberChangeData> m_AutoMemberChangeDatas;

		// Token: 0x0400106E RID: 4206
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C0")]
		[Token(Token = "0x4000C50")]
		private List<BattleSystem.DeadCharaData> m_DeadPlayerDatas;

		// Token: 0x0400106F RID: 4207
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C8")]
		[Token(Token = "0x4000C51")]
		private CharacterHandler m_CharaHndlForGameOver;

		// Token: 0x04001070 RID: 4208
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D0")]
		[Token(Token = "0x4000C52")]
		private List<CharacterHandler> m_StunCharaHndls;

		// Token: 0x04001071 RID: 4209
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D8")]
		[Token(Token = "0x4000C53")]
		private List<long> m_EnemyPoints;

		// Token: 0x04001072 RID: 4210
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E0")]
		[Token(Token = "0x4000C54")]
		private long m_TotalPoint;

		// Token: 0x04001073 RID: 4211
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E8")]
		[Token(Token = "0x4000C55")]
		private bool m_IsEnablePointEvent;

		// Token: 0x04001074 RID: 4212
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E9")]
		[Token(Token = "0x4000C56")]
		private bool m_IsPlayCharaTouchVoice;

		// Token: 0x04001075 RID: 4213
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F0")]
		[Token(Token = "0x4000C57")]
		private BattleTouchChara m_TouchChara;

		// Token: 0x04001076 RID: 4214
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F8")]
		[Token(Token = "0x4000C58")]
		private BattleSystem.eMode m_Mode;

		// Token: 0x04001077 RID: 4215
		[Cpp2IlInjected.FieldOffset(Offset = "0x1FC")]
		[Token(Token = "0x4000C59")]
		private BattleSystem.eInterruptFriendOccurredSituation m_InterruptFriendOccurredSituation;

		// Token: 0x04001078 RID: 4216
		[Cpp2IlInjected.FieldOffset(Offset = "0x200")]
		[Token(Token = "0x4000C5A")]
		private List<BattleSystem.SortData> m_SortDatas;

		// Token: 0x04001079 RID: 4217
		[Cpp2IlInjected.FieldOffset(Offset = "0x208")]
		[Token(Token = "0x4000C5B")]
		private bool m_IsRequestSort;

		// Token: 0x0400107A RID: 4218
		[Cpp2IlInjected.FieldOffset(Offset = "0x210")]
		[Token(Token = "0x4000C5C")]
		[SerializeField]
		private BattleUI m_BattleUI;

		// Token: 0x0400107B RID: 4219
		[Cpp2IlInjected.FieldOffset(Offset = "0x218")]
		[Token(Token = "0x4000C5D")]
		[SerializeField]
		private Vector3 uiTurnOwnerMarkerPosOffset;

		// Token: 0x0400107C RID: 4220
		[Cpp2IlInjected.FieldOffset(Offset = "0x224")]
		[Token(Token = "0x4000C5E")]
		private bool m_IsDestroyOnlyUI;

		// Token: 0x0400107D RID: 4221
		[Cpp2IlInjected.FieldOffset(Offset = "0x228")]
		[Token(Token = "0x4000C5F")]
		private BattleSystem.eDestroyStep m_DestroyStep;

		// Token: 0x0400107E RID: 4222
		[Token(Token = "0x4000C60")]
		private const float NOTICE_WAIT_TIME = 1.8f;

		// Token: 0x0400107F RID: 4223
		[Token(Token = "0x4000C61")]
		private const float AFTER_ORDER_SLIDE_WAIT_TIME = 0.3f;

		// Token: 0x04001080 RID: 4224
		[Token(Token = "0x4000C62")]
		private const float POST_SLILL_CARD_EXE_WAIT_TIME = 1.8f;

		// Token: 0x04001081 RID: 4225
		[Token(Token = "0x4000C63")]
		private const float POST_COMMAND_EXE_WAIT_TIME = 0.2f;

		// Token: 0x04001082 RID: 4226
		[Token(Token = "0x4000C64")]
		private const float POST_COMMAND_EXE_WAIT_TIME_TOGETHER_ATTACK_NOT_EFFECT = 0.5f;

		// Token: 0x04001083 RID: 4227
		[Token(Token = "0x4000C65")]
		private const float TOGETHER_ATTACK_NOT_EFFECT_WAIT_TIME = 1.5f;

		// Token: 0x04001084 RID: 4228
		[Token(Token = "0x4000C66")]
		private const float POST_TOGETHER_ATTACK_OF_TURN_OWNER_WAIT_TIME = 2f;

		// Token: 0x04001085 RID: 4229
		[Token(Token = "0x4000C67")]
		private const float POST_TOGETHER_ATTACK_OF_TURN_OWNER_WAIT_TIME_NOHIT = 0.8f;

		// Token: 0x04001086 RID: 4230
		[Token(Token = "0x4000C68")]
		private const float BATTLE_START_WAVEIN_WAIT_TIME = 0.65f;

		// Token: 0x04001087 RID: 4231
		[Token(Token = "0x4000C69")]
		private const float WAVEOUT_PRE_ZOOM_WAIT_TIME = 0.23f;

		// Token: 0x04001088 RID: 4232
		[Token(Token = "0x4000C6A")]
		private const float WAVEOUT_PRE_FADE_WAIT_TIME = 0.7f;

		// Token: 0x04001089 RID: 4233
		[Token(Token = "0x4000C6B")]
		private const float BATTLE_CLEAR_ZOOM_WAIT_TIME = 1.1f;

		// Token: 0x0400108A RID: 4234
		[Token(Token = "0x4000C6C")]
		private const float BATTLE_CLEAR_MINIMUM_WAIT_TIME = 3.5f;

		// Token: 0x0400108B RID: 4235
		[Token(Token = "0x4000C6D")]
		private const float STUN_OCCUR_WAIT_TIME = 0.10000001f;

		// Token: 0x0400108C RID: 4236
		[Token(Token = "0x4000C6E")]
		private const float STUN_APPLY_WAIT_TIME = 1.5f;

		// Token: 0x0400108D RID: 4237
		[Cpp2IlInjected.FieldOffset(Offset = "0x230")]
		[Token(Token = "0x4000C6F")]
		private readonly float[] TURN_PRE_END_EVENT_WAIT_TIMES;

		// Token: 0x0400108E RID: 4238
		[Token(Token = "0x4000C70")]
		private const float GAME_OVER_MINIMUM_WAIT_TIME = 3f;

		// Token: 0x0400108F RID: 4239
		[Cpp2IlInjected.FieldOffset(Offset = "0x238")]
		[Token(Token = "0x4000C71")]
		private int m_TurnPreEndStep;

		// Token: 0x04001090 RID: 4240
		[Cpp2IlInjected.FieldOffset(Offset = "0x23C")]
		[Token(Token = "0x4000C72")]
		private int m_TurnPreEndLoopCount;

		// Token: 0x04001091 RID: 4241
		[Cpp2IlInjected.FieldOffset(Offset = "0x240")]
		[Token(Token = "0x4000C73")]
		private BattleSystem.eMainStep m_MainStep;

		// Token: 0x04001092 RID: 4242
		[Cpp2IlInjected.FieldOffset(Offset = "0x248")]
		[Token(Token = "0x4000C74")]
		private ABResourceLoader m_BgLoader;

		// Token: 0x04001093 RID: 4243
		[Cpp2IlInjected.FieldOffset(Offset = "0x250")]
		[Token(Token = "0x4000C75")]
		private ABResourceObjectHandler m_BgResourceObjHndl;

		// Token: 0x04001094 RID: 4244
		[Cpp2IlInjected.FieldOffset(Offset = "0x258")]
		[Token(Token = "0x4000C76")]
		private List<CharacterHandler>[] m_PrepareCharaHndls;

		// Token: 0x04001095 RID: 4245
		[Cpp2IlInjected.FieldOffset(Offset = "0x260")]
		[Token(Token = "0x4000C77")]
		private bool m_IsFirstPrepared;

		// Token: 0x04001096 RID: 4246
		[Cpp2IlInjected.FieldOffset(Offset = "0x264")]
		[Token(Token = "0x4000C78")]
		private BattleSystem.ePrepareStep m_PrepareStep;

		// Token: 0x04001097 RID: 4247
		[Cpp2IlInjected.FieldOffset(Offset = "0x268")]
		[Token(Token = "0x4000C79")]
		private bool[] m_IsNeedPrepares;

		// Token: 0x04001098 RID: 4248
		[Cpp2IlInjected.FieldOffset(Offset = "0x270")]
		[Token(Token = "0x4000C7A")]
		private int m_PrepareStatus_Sound;

		// Token: 0x020003CD RID: 973
		[Token(Token = "0x2000D95")]
		private struct AutoMemberChangeData
		{
			// Token: 0x04001099 RID: 4249
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400579F")]
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x0400109A RID: 4250
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40057A0")]
			public BattleDefine.eJoinMember m_Bench;

			// Token: 0x0400109B RID: 4251
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40057A1")]
			public int m_OrderIndex;

			// Token: 0x0400109C RID: 4252
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x40057A2")]
			public float m_OrderValue;
		}

		// Token: 0x020003CE RID: 974
		[Token(Token = "0x2000D96")]
		private struct DeadCharaData
		{
			// Token: 0x0400109D RID: 4253
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40057A3")]
			public BattleDefine.eJoinMember m_Join;

			// Token: 0x0400109E RID: 4254
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40057A4")]
			public int m_OrderIndex;

			// Token: 0x0400109F RID: 4255
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40057A5")]
			public float m_OrderValue;
		}

		// Token: 0x020003CF RID: 975
		[Token(Token = "0x2000D97")]
		public enum eMode
		{
			// Token: 0x040010A1 RID: 4257
			[Token(Token = "0x40057A7")]
			None = -1,
			// Token: 0x040010A2 RID: 4258
			[Token(Token = "0x40057A8")]
			Prepare,
			// Token: 0x040010A3 RID: 4259
			[Token(Token = "0x40057A9")]
			BattleMain,
			// Token: 0x040010A4 RID: 4260
			[Token(Token = "0x40057AA")]
			BattleFinished,
			// Token: 0x040010A5 RID: 4261
			[Token(Token = "0x40057AB")]
			Destroy
		}

		// Token: 0x020003D0 RID: 976
		[Token(Token = "0x2000D98")]
		private enum eInterruptFriendOccurredSituation
		{
			// Token: 0x040010A7 RID: 4263
			[Token(Token = "0x40057AD")]
			None = -1,
			// Token: 0x040010A8 RID: 4264
			[Token(Token = "0x40057AE")]
			OrderSliding,
			// Token: 0x040010A9 RID: 4265
			[Token(Token = "0x40057AF")]
			CommandInput
		}

		// Token: 0x020003D1 RID: 977
		[Token(Token = "0x2000D99")]
		public enum eSortMode
		{
			// Token: 0x040010AB RID: 4267
			[Token(Token = "0x40057B1")]
			Default,
			// Token: 0x040010AC RID: 4268
			[Token(Token = "0x40057B2")]
			KiraraJumpScene,
			// Token: 0x040010AD RID: 4269
			[Token(Token = "0x40057B3")]
			UniqueSkillScene
		}

		// Token: 0x020003D2 RID: 978
		[Token(Token = "0x2000D9A")]
		public class SortData
		{
			// Token: 0x06000F07 RID: 3847 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF1")]
			[Address(RVA = "0x101157200", Offset = "0x1157200", VA = "0x101157200")]
			public SortData(Transform transform, CharacterHandler charaHndl, Renderer render, BattleUniqueSkillBaseObjectHandler.CharaRender US_CharaRender)
			{
			}

			// Token: 0x040010AE RID: 4270
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40057B4")]
			public Transform m_Transform;

			// Token: 0x040010AF RID: 4271
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40057B5")]
			public CharacterHandler m_CharaHndl;

			// Token: 0x040010B0 RID: 4272
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40057B6")]
			public BattleUniqueSkillBaseObjectHandler.CharaRender m_US_CharaRender;

			// Token: 0x040010B1 RID: 4273
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40057B7")]
			public Renderer m_Render;
		}

		// Token: 0x020003D3 RID: 979
		[Token(Token = "0x2000D9B")]
		public enum eUIEnableRenderChange
		{
			// Token: 0x040010B3 RID: 4275
			[Token(Token = "0x40057B9")]
			Invisible,
			// Token: 0x040010B4 RID: 4276
			[Token(Token = "0x40057BA")]
			Visible,
			// Token: 0x040010B5 RID: 4277
			[Token(Token = "0x40057BB")]
			Keep
		}

		// Token: 0x020003D4 RID: 980
		[Token(Token = "0x2000D9C")]
		private enum eDestroyStep
		{
			// Token: 0x040010B7 RID: 4279
			[Token(Token = "0x40057BD")]
			None = -1,
			// Token: 0x040010B8 RID: 4280
			[Token(Token = "0x40057BE")]
			First,
			// Token: 0x040010B9 RID: 4281
			[Token(Token = "0x40057BF")]
			DestroyOnlyUI_0,
			// Token: 0x040010BA RID: 4282
			[Token(Token = "0x40057C0")]
			Destroy_0,
			// Token: 0x040010BB RID: 4283
			[Token(Token = "0x40057C1")]
			Destroy_1,
			// Token: 0x040010BC RID: 4284
			[Token(Token = "0x40057C2")]
			Destroy_2,
			// Token: 0x040010BD RID: 4285
			[Token(Token = "0x40057C3")]
			Destroy_3,
			// Token: 0x040010BE RID: 4286
			[Token(Token = "0x40057C4")]
			UnloadUnusedAssets,
			// Token: 0x040010BF RID: 4287
			[Token(Token = "0x40057C5")]
			UnloadUnusedAssets_Wait,
			// Token: 0x040010C0 RID: 4288
			[Token(Token = "0x40057C6")]
			End
		}

		// Token: 0x020003D5 RID: 981
		[Token(Token = "0x2000D9D")]
		private enum eMainStep
		{
			// Token: 0x040010C2 RID: 4290
			[Token(Token = "0x40057C8")]
			None = -1,
			// Token: 0x040010C3 RID: 4291
			[Token(Token = "0x40057C9")]
			First,
			// Token: 0x040010C4 RID: 4292
			[Token(Token = "0x40057CA")]
			WarningStart,
			// Token: 0x040010C5 RID: 4293
			[Token(Token = "0x40057CB")]
			WarningStart_Wait,
			// Token: 0x040010C6 RID: 4294
			[Token(Token = "0x40057CC")]
			WaveInCharacter,
			// Token: 0x040010C7 RID: 4295
			[Token(Token = "0x40057CD")]
			WaveInCharacter_Wait,
			// Token: 0x040010C8 RID: 4296
			[Token(Token = "0x40057CE")]
			BattleStart,
			// Token: 0x040010C9 RID: 4297
			[Token(Token = "0x40057CF")]
			BattleStart_Wait_0,
			// Token: 0x040010CA RID: 4298
			[Token(Token = "0x40057D0")]
			BattleStart_Wait_1,
			// Token: 0x040010CB RID: 4299
			[Token(Token = "0x40057D1")]
			BattleStart_Wait_2,
			// Token: 0x040010CC RID: 4300
			[Token(Token = "0x40057D2")]
			OrderSlide,
			// Token: 0x040010CD RID: 4301
			[Token(Token = "0x40057D3")]
			OrderSlide_Wait,
			// Token: 0x040010CE RID: 4302
			[Token(Token = "0x40057D4")]
			OrderSlide_AfterWait,
			// Token: 0x040010CF RID: 4303
			[Token(Token = "0x40057D5")]
			OrderCheck,
			// Token: 0x040010D0 RID: 4304
			[Token(Token = "0x40057D6")]
			TurnSkipCheck,
			// Token: 0x040010D1 RID: 4305
			[Token(Token = "0x40057D7")]
			TurnSkip_Wait,
			// Token: 0x040010D2 RID: 4306
			[Token(Token = "0x40057D8")]
			CommandInputStartCheck,
			// Token: 0x040010D3 RID: 4307
			[Token(Token = "0x40057D9")]
			CommandInput_Wait,
			// Token: 0x040010D4 RID: 4308
			[Token(Token = "0x40057DA")]
			ViewStatusWindowInput_Wait,
			// Token: 0x040010D5 RID: 4309
			[Token(Token = "0x40057DB")]
			MasterUIInput_Wait,
			// Token: 0x040010D6 RID: 4310
			[Token(Token = "0x40057DC")]
			MasterSkillExecute_0,
			// Token: 0x040010D7 RID: 4311
			[Token(Token = "0x40057DD")]
			MasterSkillExecute_1,
			// Token: 0x040010D8 RID: 4312
			[Token(Token = "0x40057DE")]
			MemberChangeExecute_0,
			// Token: 0x040010D9 RID: 4313
			[Token(Token = "0x40057DF")]
			MemberChangeExecute_1,
			// Token: 0x040010DA RID: 4314
			[Token(Token = "0x40057E0")]
			MemberChangeExecute_2,
			// Token: 0x040010DB RID: 4315
			[Token(Token = "0x40057E1")]
			MemberChangeExecute_3,
			// Token: 0x040010DC RID: 4316
			[Token(Token = "0x40057E2")]
			MemberChangeExecute_4,
			// Token: 0x040010DD RID: 4317
			[Token(Token = "0x40057E3")]
			AutoMemberChangeExecute_0,
			// Token: 0x040010DE RID: 4318
			[Token(Token = "0x40057E4")]
			AutoMemberChangeExecute_1,
			// Token: 0x040010DF RID: 4319
			[Token(Token = "0x40057E5")]
			AutoMemberChangeExecute_2,
			// Token: 0x040010E0 RID: 4320
			[Token(Token = "0x40057E6")]
			AutoMemberChangeExecute_3,
			// Token: 0x040010E1 RID: 4321
			[Token(Token = "0x40057E7")]
			AutoMemberChangeExecute_4,
			// Token: 0x040010E2 RID: 4322
			[Token(Token = "0x40057E8")]
			GoBackFriendExecute_0,
			// Token: 0x040010E3 RID: 4323
			[Token(Token = "0x40057E9")]
			GoBackFriendExecute_1,
			// Token: 0x040010E4 RID: 4324
			[Token(Token = "0x40057EA")]
			GoBackFriendExecute_2,
			// Token: 0x040010E5 RID: 4325
			[Token(Token = "0x40057EB")]
			GoBackFriendExecute_3,
			// Token: 0x040010E6 RID: 4326
			[Token(Token = "0x40057EC")]
			GoBackFriendExecute_4,
			// Token: 0x040010E7 RID: 4327
			[Token(Token = "0x40057ED")]
			InterruptFriendJoinSelect_Wait,
			// Token: 0x040010E8 RID: 4328
			[Token(Token = "0x40057EE")]
			InterruptFriendExecute_0,
			// Token: 0x040010E9 RID: 4329
			[Token(Token = "0x40057EF")]
			InterruptFriendExecute_1,
			// Token: 0x040010EA RID: 4330
			[Token(Token = "0x40057F0")]
			InterruptFriendExecute_2,
			// Token: 0x040010EB RID: 4331
			[Token(Token = "0x40057F1")]
			InterruptFriendExecute_3,
			// Token: 0x040010EC RID: 4332
			[Token(Token = "0x40057F2")]
			InterruptFriendExecute_4,
			// Token: 0x040010ED RID: 4333
			[Token(Token = "0x40057F3")]
			InterruptFriendExecute_5,
			// Token: 0x040010EE RID: 4334
			[Token(Token = "0x40057F4")]
			InterruptFriendExecute_6,
			// Token: 0x040010EF RID: 4335
			[Token(Token = "0x40057F5")]
			InterruptFriendExecute_7,
			// Token: 0x040010F0 RID: 4336
			[Token(Token = "0x40057F6")]
			InterruptFriendExecute_8,
			// Token: 0x040010F1 RID: 4337
			[Token(Token = "0x40057F7")]
			TogetherAttackInput_Wait,
			// Token: 0x040010F2 RID: 4338
			[Token(Token = "0x40057F8")]
			TogetherAttackExecute_0,
			// Token: 0x040010F3 RID: 4339
			[Token(Token = "0x40057F9")]
			KiraraJumpScenePrepare,
			// Token: 0x040010F4 RID: 4340
			[Token(Token = "0x40057FA")]
			KiraraJumpScenePrepare_Wait,
			// Token: 0x040010F5 RID: 4341
			[Token(Token = "0x40057FB")]
			KiraraJumpScenePlaying,
			// Token: 0x040010F6 RID: 4342
			[Token(Token = "0x40057FC")]
			KiraraJumpSceneDestroy_Wait_0,
			// Token: 0x040010F7 RID: 4343
			[Token(Token = "0x40057FD")]
			KiraraJumpSceneDestroy_Wait_1,
			// Token: 0x040010F8 RID: 4344
			[Token(Token = "0x40057FE")]
			UniqueSkillScenePrepare_Wait,
			// Token: 0x040010F9 RID: 4345
			[Token(Token = "0x40057FF")]
			UniqueSkillScenePrepare_WaitOnNotEffect_0,
			// Token: 0x040010FA RID: 4346
			[Token(Token = "0x4005800")]
			UniqueSkillScenePrepare_WaitOnNotEffect_1,
			// Token: 0x040010FB RID: 4347
			[Token(Token = "0x4005801")]
			UniqueSkillScenePlaying,
			// Token: 0x040010FC RID: 4348
			[Token(Token = "0x4005802")]
			UniqueSkillSceneDestroy_Wait_0,
			// Token: 0x040010FD RID: 4349
			[Token(Token = "0x4005803")]
			UniqueSkillSceneDestroy_Wait_1,
			// Token: 0x040010FE RID: 4350
			[Token(Token = "0x4005804")]
			UniqueSkillSceneDestroy_Wait_2,
			// Token: 0x040010FF RID: 4351
			[Token(Token = "0x4005805")]
			UniqueSkillSceneDestroy_Wait_3,
			// Token: 0x04001100 RID: 4352
			[Token(Token = "0x4005806")]
			CommandExecute,
			// Token: 0x04001101 RID: 4353
			[Token(Token = "0x4005807")]
			CommandExecute_CharaCutInWait,
			// Token: 0x04001102 RID: 4354
			[Token(Token = "0x4005808")]
			CommandExecute_Wait_0,
			// Token: 0x04001103 RID: 4355
			[Token(Token = "0x4005809")]
			CommandExecute_Wait_1,
			// Token: 0x04001104 RID: 4356
			[Token(Token = "0x400580A")]
			CommandExecute_Wait_2,
			// Token: 0x04001105 RID: 4357
			[Token(Token = "0x400580B")]
			PostCommandExecute,
			// Token: 0x04001106 RID: 4358
			[Token(Token = "0x400580C")]
			StunExecute,
			// Token: 0x04001107 RID: 4359
			[Token(Token = "0x400580D")]
			StunExecute_Wait_0,
			// Token: 0x04001108 RID: 4360
			[Token(Token = "0x400580E")]
			StunExecute_Wait_1,
			// Token: 0x04001109 RID: 4361
			[Token(Token = "0x400580F")]
			StunExecute_Wait_2,
			// Token: 0x0400110A RID: 4362
			[Token(Token = "0x4005810")]
			StunExecute_Wait_3,
			// Token: 0x0400110B RID: 4363
			[Token(Token = "0x4005811")]
			TurnPreEnd,
			// Token: 0x0400110C RID: 4364
			[Token(Token = "0x4005812")]
			TurnPreEnd_Wait,
			// Token: 0x0400110D RID: 4365
			[Token(Token = "0x4005813")]
			TurnPreEnd_Wait_2,
			// Token: 0x0400110E RID: 4366
			[Token(Token = "0x4005814")]
			TurnEnd,
			// Token: 0x0400110F RID: 4367
			[Token(Token = "0x4005815")]
			WaveOut,
			// Token: 0x04001110 RID: 4368
			[Token(Token = "0x4005816")]
			WaveOut_Wait_0,
			// Token: 0x04001111 RID: 4369
			[Token(Token = "0x4005817")]
			WaveOut_Wait_1,
			// Token: 0x04001112 RID: 4370
			[Token(Token = "0x4005818")]
			WaveOut_Wait_2,
			// Token: 0x04001113 RID: 4371
			[Token(Token = "0x4005819")]
			WaveOut_Wait_3,
			// Token: 0x04001114 RID: 4372
			[Token(Token = "0x400581A")]
			BattleClear,
			// Token: 0x04001115 RID: 4373
			[Token(Token = "0x400581B")]
			BattleClear_Wait_0,
			// Token: 0x04001116 RID: 4374
			[Token(Token = "0x400581C")]
			BattleClear_Wait_1,
			// Token: 0x04001117 RID: 4375
			[Token(Token = "0x400581D")]
			GameOver,
			// Token: 0x04001118 RID: 4376
			[Token(Token = "0x400581E")]
			GameOver_Wait_0,
			// Token: 0x04001119 RID: 4377
			[Token(Token = "0x400581F")]
			GameOver_Wait_1,
			// Token: 0x0400111A RID: 4378
			[Token(Token = "0x4005820")]
			RetrySelect,
			// Token: 0x0400111B RID: 4379
			[Token(Token = "0x4005821")]
			RetrySelect_Wait,
			// Token: 0x0400111C RID: 4380
			[Token(Token = "0x4005822")]
			RetryCancel,
			// Token: 0x0400111D RID: 4381
			[Token(Token = "0x4005823")]
			RetryRequest,
			// Token: 0x0400111E RID: 4382
			[Token(Token = "0x4005824")]
			RetryRequest_Wait,
			// Token: 0x0400111F RID: 4383
			[Token(Token = "0x4005825")]
			Retry,
			// Token: 0x04001120 RID: 4384
			[Token(Token = "0x4005826")]
			Retry_Wait,
			// Token: 0x04001121 RID: 4385
			[Token(Token = "0x4005827")]
			ToResult,
			// Token: 0x04001122 RID: 4386
			[Token(Token = "0x4005828")]
			ToResult_Wait,
			// Token: 0x04001123 RID: 4387
			[Token(Token = "0x4005829")]
			Retire,
			// Token: 0x04001124 RID: 4388
			[Token(Token = "0x400582A")]
			PrepareError,
			// Token: 0x04001125 RID: 4389
			[Token(Token = "0x400582B")]
			PrepareError_Wait
		}

		// Token: 0x020003D6 RID: 982
		[Token(Token = "0x2000D9E")]
		private enum ePrepareStep
		{
			// Token: 0x04001127 RID: 4391
			[Token(Token = "0x400582D")]
			None = -1,
			// Token: 0x04001128 RID: 4392
			[Token(Token = "0x400582E")]
			First,
			// Token: 0x04001129 RID: 4393
			[Token(Token = "0x400582F")]
			Prepare_0,
			// Token: 0x0400112A RID: 4394
			[Token(Token = "0x4005830")]
			Prepare_1,
			// Token: 0x0400112B RID: 4395
			[Token(Token = "0x4005831")]
			End,
			// Token: 0x0400112C RID: 4396
			[Token(Token = "0x4005832")]
			PrepareError,
			// Token: 0x0400112D RID: 4397
			[Token(Token = "0x4005833")]
			PrepareError_Wait
		}

		// Token: 0x020003D7 RID: 983
		[Token(Token = "0x2000D9F")]
		private enum ePrepareTarget
		{
			// Token: 0x0400112F RID: 4399
			[Token(Token = "0x4005835")]
			Sound,
			// Token: 0x04001130 RID: 4400
			[Token(Token = "0x4005836")]
			BG,
			// Token: 0x04001131 RID: 4401
			[Token(Token = "0x4005837")]
			AlwaysEffect,
			// Token: 0x04001132 RID: 4402
			[Token(Token = "0x4005838")]
			Player,
			// Token: 0x04001133 RID: 4403
			[Token(Token = "0x4005839")]
			PlayerEffect,
			// Token: 0x04001134 RID: 4404
			[Token(Token = "0x400583A")]
			Enemy,
			// Token: 0x04001135 RID: 4405
			[Token(Token = "0x400583B")]
			EnemyEffect,
			// Token: 0x04001136 RID: 4406
			[Token(Token = "0x400583C")]
			UI,
			// Token: 0x04001137 RID: 4407
			[Token(Token = "0x400583D")]
			Num
		}
	}
}
