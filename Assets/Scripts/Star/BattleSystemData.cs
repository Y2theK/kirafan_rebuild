﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using Star.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x020003D8 RID: 984
	[Token(Token = "0x200030C")]
	[Serializable]
	[StructLayout(3)]
	public class BattleSystemData
	{
		// Token: 0x06000F08 RID: 3848 RVA: 0x000066F0 File Offset: 0x000048F0
		[Token(Token = "0x6000DDA")]
		[Address(RVA = "0x101157258", Offset = "0x1157258", VA = "0x101157258")]
		public bool VersionCheck(string version)
		{
			return default(bool);
		}

		// Token: 0x06000F09 RID: 3849 RVA: 0x00006708 File Offset: 0x00004908
		[Token(Token = "0x6000DDB")]
		[Address(RVA = "0x1011572A8", Offset = "0x11572A8", VA = "0x1011572A8")]
		public bool WasMasterSkillUsed()
		{
			return default(bool);
		}

		// Token: 0x06000F0A RID: 3850 RVA: 0x00006720 File Offset: 0x00004920
		[Token(Token = "0x6000DDC")]
		[Address(RVA = "0x1011572B8", Offset = "0x11572B8", VA = "0x1011572B8")]
		public bool GetMasterSkillFlg(int skillIndex)
		{
			return default(bool);
		}

		// Token: 0x06000F0B RID: 3851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DDD")]
		[Address(RVA = "0x1011572D0", Offset = "0x11572D0", VA = "0x1011572D0")]
		public void SetMasterSkillFlg(int skillIndex, bool used)
		{
		}

		// Token: 0x06000F0C RID: 3852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DDE")]
		[Address(RVA = "0x1011572F4", Offset = "0x11572F4", VA = "0x1011572F4")]
		public void ResetMasterSkillFlg()
		{
		}

		// Token: 0x06000F0D RID: 3853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DDF")]
		[Address(RVA = "0x1011572FC", Offset = "0x11572FC", VA = "0x1011572FC")]
		public BattleSystemData()
		{
		}

		// Token: 0x04001138 RID: 4408
		[Token(Token = "0x4000C7B")]
		public const string SUBVERSION = "5";

		// Token: 0x04001139 RID: 4409
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000C7C")]
		public string Ver;

		// Token: 0x0400113A RID: 4410
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000C7D")]
		public string SubVer;

		// Token: 0x0400113B RID: 4411
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000C7E")]
		public int Phase;

		// Token: 0x0400113C RID: 4412
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000C7F")]
		public long RecvID;

		// Token: 0x0400113D RID: 4413
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000C80")]
		public int QuestID;

		// Token: 0x0400113E RID: 4414
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000C81")]
		public long PtyMngID;

		// Token: 0x0400113F RID: 4415
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000C82")]
		public long QuestNpcID;

		// Token: 0x04001140 RID: 4416
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000C83")]
		public int ContCount;

		// Token: 0x04001141 RID: 4417
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4000C84")]
		public int WaveIdx;

		// Token: 0x04001142 RID: 4418
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000C85")]
		[NonSerialized]
		public int WaveNum;

		// Token: 0x04001143 RID: 4419
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4000C86")]
		public int OrbID;

		// Token: 0x04001144 RID: 4420
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000C87")]
		public int OrbLv;

		// Token: 0x04001145 RID: 4421
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4000C88")]
		public int MstSkillCount;

		// Token: 0x04001146 RID: 4422
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000C89")]
		[SerializeField]
		private int MstSkillFlg;

		// Token: 0x04001147 RID: 4423
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4000C8A")]
		public int FrdType;

		// Token: 0x04001148 RID: 4424
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000C8B")]
		public int FrdCharaID;

		// Token: 0x04001149 RID: 4425
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000C8C")]
		public int FrdCharaLv;

		// Token: 0x0400114A RID: 4426
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000C8D")]
		public int FrdLB;

		// Token: 0x0400114B RID: 4427
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4000C8E")]
		public int FrdUSLv;

		// Token: 0x0400114C RID: 4428
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000C8F")]
		public int[] FrdCSLvs;

		// Token: 0x0400114D RID: 4429
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000C90")]
		public int FrdWpID;

		// Token: 0x0400114E RID: 4430
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4000C91")]
		public int FrdWpLv;

		// Token: 0x0400114F RID: 4431
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000C92")]
		public int FrdWpSLv;

		// Token: 0x04001150 RID: 4432
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4000C93")]
		public int Frdship;

		// Token: 0x04001151 RID: 4433
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000C94")]
		public int FrdArousalLv;

		// Token: 0x04001152 RID: 4434
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4000C95")]
		public int FrdDuplicatedCount;

		// Token: 0x04001153 RID: 4435
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000C96")]
		public int FrdAblID;

		// Token: 0x04001154 RID: 4436
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000C97")]
		public int[] FrdAblItemIDs;

		// Token: 0x04001155 RID: 4437
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000C98")]
		public int FrdRemainTurn;

		// Token: 0x04001156 RID: 4438
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4000C99")]
		public int FrdUseCount;

		// Token: 0x04001157 RID: 4439
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000C9A")]
		public bool FrdUsed;

		// Token: 0x04001158 RID: 4440
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000C9B")]
		public BattleTogetherGauge Gauge;

		// Token: 0x04001159 RID: 4441
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000C9C")]
		public float TogetherChainBoost;

		// Token: 0x0400115A RID: 4442
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000C9D")]
		public TownBuff TBuff;

		// Token: 0x0400115B RID: 4443
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4000C9E")]
		public BattleOccurEnemy Enemies;

		// Token: 0x0400115C RID: 4444
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4000C9F")]
		public List<BattleDropItems> ScheduleItems;

		// Token: 0x0400115D RID: 4445
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4000CA0")]
		public BattleSystemDataForPL[] PLs;

		// Token: 0x0400115E RID: 4446
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4000CA1")]
		public BattleSystemDataForPLPT PLPT;

		// Token: 0x0400115F RID: 4447
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4000CA2")]
		public int[] PLJoinIdxs;
	}
}
