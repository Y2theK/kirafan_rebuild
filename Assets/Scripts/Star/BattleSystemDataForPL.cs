﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003D9 RID: 985
	[Token(Token = "0x200030D")]
	[Serializable]
	[StructLayout(3)]
	public class BattleSystemDataForPL
	{
		// Token: 0x06000F0E RID: 3854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE0")]
		[Address(RVA = "0x101157394", Offset = "0x1157394", VA = "0x101157394")]
		public BattleSystemDataForPL()
		{
		}

		// Token: 0x04001160 RID: 4448
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CA3")]
		public CharacterBattleParam Param;

		// Token: 0x04001161 RID: 4449
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CA4")]
		public List<BattlePassiveSkillSolveSerializeField> Solves;

		// Token: 0x04001162 RID: 4450
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CA5")]
		public BattleCommandData[] Cmds;
	}
}
