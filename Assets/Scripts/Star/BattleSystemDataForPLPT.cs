﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003DB RID: 987
	[Token(Token = "0x200030F")]
	[Serializable]
	[StructLayout(3)]
	public class BattleSystemDataForPLPT
	{
		// Token: 0x06000F10 RID: 3856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE2")]
		[Address(RVA = "0x10115739C", Offset = "0x115739C", VA = "0x10115739C")]
		public BattleSystemDataForPLPT()
		{
		}

		// Token: 0x04001164 RID: 4452
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CA7")]
		public BattlePartyDataParam Param;
	}
}
