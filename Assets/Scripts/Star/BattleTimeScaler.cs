﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003DC RID: 988
	[Token(Token = "0x2000310")]
	[StructLayout(3)]
	public class BattleTimeScaler
	{
		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x06000F11 RID: 3857 RVA: 0x00006738 File Offset: 0x00004938
		// (set) Token: 0x06000F12 RID: 3858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170000C4")]
		public bool IsDirty
		{
			[Token(Token = "0x6000DE3")]
			[Address(RVA = "0x1011573A4", Offset = "0x11573A4", VA = "0x1011573A4")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000DE4")]
			[Address(RVA = "0x1011573AC", Offset = "0x11573AC", VA = "0x1011573AC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE5")]
		[Address(RVA = "0x1011573B4", Offset = "0x11573B4", VA = "0x1011573B4")]
		public void Update()
		{
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE6")]
		[Address(RVA = "0x1011574D8", Offset = "0x11574D8", VA = "0x1011574D8")]
		public void SetBaseTimeScale(float baseTimeScale)
		{
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE7")]
		[Address(RVA = "0x1011574E0", Offset = "0x11574E0", VA = "0x1011574E0")]
		public void SetChangeTimeScale(float changeTimeScale, float changeTimeScaleSec, float changeTimeScaleSecRevertRatio = 1f)
		{
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE8")]
		[Address(RVA = "0x1011574FC", Offset = "0x11574FC", VA = "0x1011574FC")]
		public BattleTimeScaler()
		{
		}

		// Token: 0x04001165 RID: 4453
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CA8")]
		private float m_BeforeTime;

		// Token: 0x04001166 RID: 4454
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000CA9")]
		private float m_BaseTimeScale;

		// Token: 0x04001167 RID: 4455
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CAA")]
		private float m_ChangeTimeScale;

		// Token: 0x04001168 RID: 4456
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000CAB")]
		private float m_ChangeTimeScaleInit;

		// Token: 0x04001169 RID: 4457
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CAC")]
		private float m_ChangeTimeScaleSec;

		// Token: 0x0400116A RID: 4458
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000CAD")]
		private float m_ChangeTimeScaleSecRevert;

		// Token: 0x0400116B RID: 4459
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CAE")]
		private bool m_IsChangeTimeScale;
	}
}
