﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003DD RID: 989
	[Token(Token = "0x2000311")]
	[StructLayout(3)]
	public class BattleTitleCutIn : MonoBehaviour
	{
		// Token: 0x06000F17 RID: 3863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DE9")]
		[Address(RVA = "0x101157510", Offset = "0x1157510", VA = "0x101157510")]
		private void Start()
		{
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DEA")]
		[Address(RVA = "0x101157568", Offset = "0x1157568", VA = "0x101157568")]
		private void Update()
		{
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DEB")]
		[Address(RVA = "0x1011579F0", Offset = "0x11579F0", VA = "0x1011579F0")]
		public void Play(BattleTitleCutIn.eType type)
		{
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x00006750 File Offset: 0x00004950
		[Token(Token = "0x6000DEC")]
		[Address(RVA = "0x101157A34", Offset = "0x1157A34", VA = "0x101157A34")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DED")]
		[Address(RVA = "0x101157924", Offset = "0x1157924", VA = "0x101157924")]
		public void Destroy()
		{
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DEE")]
		[Address(RVA = "0x10115791C", Offset = "0x115791C", VA = "0x10115791C")]
		private void SetStep(BattleTitleCutIn.eStep step)
		{
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DEF")]
		[Address(RVA = "0x101157A44", Offset = "0x1157A44", VA = "0x101157A44")]
		public BattleTitleCutIn()
		{
		}

		// Token: 0x0400116D RID: 4461
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CB0")]
		private BattleTitleCutIn.eType m_Type;

		// Token: 0x0400116E RID: 4462
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CB1")]
		private readonly string[] EFFECT_IDS;

		// Token: 0x0400116F RID: 4463
		[Token(Token = "0x4000CB2")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04001170 RID: 4464
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CB3")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04001171 RID: 4465
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000CB4")]
		[SerializeField]
		private AdjustOrthographicCamera m_AdjustOrthographicCamera;

		// Token: 0x04001172 RID: 4466
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000CB5")]
		[SerializeField]
		private float[] m_Scales;

		// Token: 0x04001173 RID: 4467
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000CB6")]
		[SerializeField]
		private Vector3[] m_Offsets;

		// Token: 0x04001174 RID: 4468
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000CB7")]
		private BattleTitleCutIn.eStep m_Step;

		// Token: 0x04001175 RID: 4469
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000CB8")]
		private EffectHandler m_EffectHndl;

		// Token: 0x020003DE RID: 990
		[Token(Token = "0x2000DA0")]
		public enum eType
		{
			// Token: 0x04001177 RID: 4471
			[Token(Token = "0x400583F")]
			Start,
			// Token: 0x04001178 RID: 4472
			[Token(Token = "0x4005840")]
			Clear,
			// Token: 0x04001179 RID: 4473
			[Token(Token = "0x4005841")]
			Failed,
			// Token: 0x0400117A RID: 4474
			[Token(Token = "0x4005842")]
			Num
		}

		// Token: 0x020003DF RID: 991
		[Token(Token = "0x2000DA1")]
		private enum eStep
		{
			// Token: 0x0400117C RID: 4476
			[Token(Token = "0x4005844")]
			None = -1,
			// Token: 0x0400117D RID: 4477
			[Token(Token = "0x4005845")]
			Play,
			// Token: 0x0400117E RID: 4478
			[Token(Token = "0x4005846")]
			Play_Wait
		}
	}
}
