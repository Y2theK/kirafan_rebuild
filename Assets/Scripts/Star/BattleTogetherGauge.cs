﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003E0 RID: 992
	[Token(Token = "0x2000312")]
	[Serializable]
	[StructLayout(3)]
	public class BattleTogetherGauge
	{
		// Token: 0x06000F1E RID: 3870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF0")]
		[Address(RVA = "0x101157D14", Offset = "0x1157D14", VA = "0x101157D14")]
		public BattleTogetherGauge()
		{
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x00006768 File Offset: 0x00004968
		[Token(Token = "0x6000DF1")]
		[Address(RVA = "0x101157D3C", Offset = "0x1157D3C", VA = "0x101157D3C")]
		public float GetValue()
		{
			return 0f;
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x00006780 File Offset: 0x00004980
		[Token(Token = "0x6000DF2")]
		[Address(RVA = "0x101157D44", Offset = "0x1157D44", VA = "0x101157D44")]
		public float CalcGauge(float value)
		{
			return 0f;
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF3")]
		[Address(RVA = "0x101157DD8", Offset = "0x1157DD8", VA = "0x101157DD8")]
		public void ResetGauge()
		{
		}

		// Token: 0x0400117F RID: 4479
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CB9")]
		[SerializeField]
		private float Val;
	}
}
