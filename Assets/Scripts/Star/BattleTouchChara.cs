﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003E1 RID: 993
	[Token(Token = "0x2000313")]
	[StructLayout(3)]
	public class BattleTouchChara
	{
		// Token: 0x06000F22 RID: 3874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF4")]
		[Address(RVA = "0x101157DE0", Offset = "0x1157DE0", VA = "0x101157DE0")]
		public void Reset()
		{
		}

		// Token: 0x06000F23 RID: 3875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF5")]
		[Address(RVA = "0x101157DEC", Offset = "0x1157DEC", VA = "0x101157DEC")]
		public void SetTochingCharaHndl(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000F24 RID: 3876 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000DF6")]
		[Address(RVA = "0x101157DF4", Offset = "0x1157DF4", VA = "0x101157DF4")]
		public CharacterHandler GetTochingCharaHndl()
		{
			return null;
		}

		// Token: 0x06000F25 RID: 3877 RVA: 0x00006798 File Offset: 0x00004998
		[Token(Token = "0x6000DF7")]
		[Address(RVA = "0x101157DFC", Offset = "0x1157DFC", VA = "0x101157DFC")]
		public bool UpdateHold(float deltaTime)
		{
			return default(bool);
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF8")]
		[Address(RVA = "0x101157E18", Offset = "0x1157E18", VA = "0x101157E18")]
		public BattleTouchChara()
		{
		}

		// Token: 0x04001180 RID: 4480
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CBA")]
		private CharacterHandler m_TouchingCharaHndl;

		// Token: 0x04001181 RID: 4481
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CBB")]
		private float m_TouchHoldCount;
	}
}
