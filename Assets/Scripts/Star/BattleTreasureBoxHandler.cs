﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003E2 RID: 994
	[Token(Token = "0x2000314")]
	[StructLayout(3)]
	public class BattleTreasureBoxHandler : BattleDropObjectHandler
	{
		// Token: 0x06000F27 RID: 3879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DF9")]
		[Address(RVA = "0x101157E20", Offset = "0x1157E20", VA = "0x101157E20", Slot = "4")]
		public override void Update()
		{
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DFA")]
		[Address(RVA = "0x10115837C", Offset = "0x115837C", VA = "0x10115837C")]
		public void SetParameters(BattleDropItem dropItem, Vector3 startPos, Vector3 targetPos, Action<BattleDropItem> OnCompleteFunc)
		{
		}

		// Token: 0x06000F29 RID: 3881 RVA: 0x000067B0 File Offset: 0x000049B0
		[Token(Token = "0x6000DFB")]
		[Address(RVA = "0x1011583A0", Offset = "0x11583A0", VA = "0x1011583A0", Slot = "5")]
		public override bool Play(Transform parent, int sortingOrder)
		{
			return default(bool);
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DFC")]
		[Address(RVA = "0x10115861C", Offset = "0x115861C", VA = "0x10115861C", Slot = "6")]
		public override void Stop()
		{
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x000067C8 File Offset: 0x000049C8
		[Token(Token = "0x6000DFD")]
		[Address(RVA = "0x1011586F0", Offset = "0x11586F0", VA = "0x1011586F0", Slot = "7")]
		public override bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x000067E0 File Offset: 0x000049E0
		[Token(Token = "0x6000DFE")]
		[Address(RVA = "0x1011582D8", Offset = "0x11582D8", VA = "0x1011582D8")]
		private bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000DFF")]
		[Address(RVA = "0x1011580E8", Offset = "0x11580E8", VA = "0x1011580E8")]
		private void UpdateVisiblity()
		{
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E00")]
		[Address(RVA = "0x101158808", Offset = "0x1158808", VA = "0x101158808")]
		public BattleTreasureBoxHandler()
		{
		}

		// Token: 0x04001182 RID: 4482
		[Token(Token = "0x4000CBC")]
		private const string EFFECT_ID = "TreasureBox";

		// Token: 0x04001183 RID: 4483
		[Token(Token = "0x4000CBD")]
		private static readonly string[] TARGET_NAMES;

		// Token: 0x04001184 RID: 4484
		[Token(Token = "0x4000CBE")]
		private const float POS_OFFSET_Y = 0.35f;

		// Token: 0x04001185 RID: 4485
		[Token(Token = "0x4000CBF")]
		private const float WAIT_SEC_AFTER_DROP = 0.033333335f;

		// Token: 0x04001186 RID: 4486
		[Token(Token = "0x4000CC0")]
		private const float GO_TO_UI_SEC = 0.20000002f;

		// Token: 0x04001187 RID: 4487
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CC1")]
		private BattleTreasureBoxHandler.eStep m_Step;

		// Token: 0x04001188 RID: 4488
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CC2")]
		private SimpleTimer m_Timer;

		// Token: 0x04001189 RID: 4489
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CC3")]
		private BattleDropItem m_DropItem;

		// Token: 0x0400118A RID: 4490
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CC4")]
		private Vector3 m_StartPos;

		// Token: 0x0400118B RID: 4491
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000CC5")]
		private Vector3 m_TargetPos;

		// Token: 0x0400118C RID: 4492
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000CC6")]
		private EffectHandler m_EffectHndl;

		// Token: 0x0400118D RID: 4493
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000CC7")]
		public Action<BattleDropItem> OnComplete;

		// Token: 0x020003E3 RID: 995
		[Token(Token = "0x2000DA2")]
		private enum eStep
		{
			// Token: 0x0400118F RID: 4495
			[Token(Token = "0x4005848")]
			None = -1,
			// Token: 0x04001190 RID: 4496
			[Token(Token = "0x4005849")]
			Drop_Wait_0,
			// Token: 0x04001191 RID: 4497
			[Token(Token = "0x400584A")]
			Drop_Wait_1,
			// Token: 0x04001192 RID: 4498
			[Token(Token = "0x400584B")]
			GoToUI,
			// Token: 0x04001193 RID: 4499
			[Token(Token = "0x400584C")]
			GoToUI_Wait,
			// Token: 0x04001194 RID: 4500
			[Token(Token = "0x400584D")]
			End
		}
	}
}
