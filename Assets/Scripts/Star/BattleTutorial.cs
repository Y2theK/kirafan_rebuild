﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003E4 RID: 996
	[Token(Token = "0x2000315")]
	[StructLayout(3)]
	public class BattleTutorial
	{
		// Token: 0x06000F30 RID: 3888 RVA: 0x000067F8 File Offset: 0x000049F8
		[Token(Token = "0x6000E02")]
		[Address(RVA = "0x101158A2C", Offset = "0x1158A2C", VA = "0x101158A2C")]
		public bool IsSeq(BattleTutorial.eSeq seq)
		{
			return default(bool);
		}

		// Token: 0x06000F31 RID: 3889 RVA: 0x00006810 File Offset: 0x00004A10
		[Token(Token = "0x6000E03")]
		[Address(RVA = "0x101158A3C", Offset = "0x1158A3C", VA = "0x101158A3C")]
		public BattleTutorial.eSeq GetSeq()
		{
			return BattleTutorial.eSeq._00_WAVE_IDX_0_TIPS_START;
		}

		// Token: 0x06000F32 RID: 3890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E04")]
		[Address(RVA = "0x101158A44", Offset = "0x1158A44", VA = "0x101158A44")]
		public void SetSeq(BattleTutorial.eSeq seq)
		{
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E05")]
		[Address(RVA = "0x101158A4C", Offset = "0x1158A4C", VA = "0x101158A4C")]
		public void SetSeq(int seq)
		{
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x00006828 File Offset: 0x00004A28
		[Token(Token = "0x6000E06")]
		[Address(RVA = "0x101158A54", Offset = "0x1158A54", VA = "0x101158A54")]
		public BattleTutorial.eSeq ApplyNextSeq()
		{
			return BattleTutorial.eSeq._00_WAVE_IDX_0_TIPS_START;
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E07")]
		[Address(RVA = "0x101158A68", Offset = "0x1158A68", VA = "0x101158A68")]
		public void AddCommandBtnIndexHistory(int index)
		{
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E08")]
		[Address(RVA = "0x101158B04", Offset = "0x1158B04", VA = "0x101158B04")]
		public void ClearCommandBtnIndexHistory()
		{
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x00006840 File Offset: 0x00004A40
		[Token(Token = "0x6000E09")]
		[Address(RVA = "0x101158B64", Offset = "0x1158B64", VA = "0x101158B64")]
		public bool JudgeCommandBtnIndexHistory()
		{
			return default(bool);
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x00006858 File Offset: 0x00004A58
		[Token(Token = "0x6000E0A")]
		[Address(RVA = "0x101158CD8", Offset = "0x1158CD8", VA = "0x101158CD8")]
		public BattleDefine.eJoinMember GetSingleTargetIndex()
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E0B")]
		[Address(RVA = "0x101158CE0", Offset = "0x1158CE0", VA = "0x101158CE0")]
		public void SetSingleTargetIndex(BattleDefine.eJoinMember join)
		{
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E0C")]
		[Address(RVA = "0x101158CE8", Offset = "0x1158CE8", VA = "0x101158CE8")]
		public BattleTutorial()
		{
		}

		// Token: 0x04001195 RID: 4501
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CC8")]
		private BattleTutorial.eSeq m_Seq;

		// Token: 0x04001196 RID: 4502
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CC9")]
		private List<int> m_CommandBtnIndexHistory;

		// Token: 0x04001197 RID: 4503
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CCA")]
		public int[] NeedCommandBtnIndexHistory;

		// Token: 0x04001198 RID: 4504
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CCB")]
		private BattleDefine.eJoinMember m_SingleTargetIndex;

		// Token: 0x020003E5 RID: 997
		[Token(Token = "0x2000DA3")]
		public enum eSeq
		{
			// Token: 0x0400119A RID: 4506
			[Token(Token = "0x400584F")]
			None = -1,
			// Token: 0x0400119B RID: 4507
			[Token(Token = "0x4005850")]
			_00_WAVE_IDX_0_TIPS_START,
			// Token: 0x0400119C RID: 4508
			[Token(Token = "0x4005851")]
			_01_WAVE_IDX_0_ATK,
			// Token: 0x0400119D RID: 4509
			[Token(Token = "0x4005852")]
			_02_WAVE_IDX_0_TIPS_COMMAND,
			// Token: 0x0400119E RID: 4510
			[Token(Token = "0x4005853")]
			_03_WAVE_IDX_0_SWIPE,
			// Token: 0x0400119F RID: 4511
			[Token(Token = "0x4005854")]
			_04_WAVE_IDX_0_TIPS_ELEMENT,
			// Token: 0x040011A0 RID: 4512
			[Token(Token = "0x4005855")]
			_05_WAVE_IDX_0_TARGET,
			// Token: 0x040011A1 RID: 4513
			[Token(Token = "0x4005856")]
			_06_WAVE_IDX_0_ATK_AFTER_TARGET,
			// Token: 0x040011A2 RID: 4514
			[Token(Token = "0x4005857")]
			_07_WAVE_IDX_1_TIPS_START,
			// Token: 0x040011A3 RID: 4515
			[Token(Token = "0x4005858")]
			_08_WAVE_IDX_1_OWNER_IMAGE_SLIDE,
			// Token: 0x040011A4 RID: 4516
			[Token(Token = "0x4005859")]
			_09_WAVE_IDX_1_TIPS_CHARGESTUN,
			// Token: 0x040011A5 RID: 4517
			[Token(Token = "0x400585A")]
			_10_WAVE_IDX_1_ATK_AFTER_TIPS_STUN,
			// Token: 0x040011A6 RID: 4518
			[Token(Token = "0x400585B")]
			_11_WAVE_IDX_1_TIPS_UNIQUE_SKILL,
			// Token: 0x040011A7 RID: 4519
			[Token(Token = "0x400585C")]
			_12_WAVE_IDX_1_UNIQUE_SKILL,
			// Token: 0x040011A8 RID: 4520
			[Token(Token = "0x400585D")]
			Max
		}
	}
}
