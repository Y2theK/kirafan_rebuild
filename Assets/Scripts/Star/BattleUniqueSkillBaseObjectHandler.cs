﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020003E6 RID: 998
	[Token(Token = "0x2000316")]
	[StructLayout(3)]
	public class BattleUniqueSkillBaseObjectHandler : MonoBehaviour
	{
		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000F3B RID: 3899 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C5")]
		public MsbHandler MsbHndl
		{
			[Token(Token = "0x6000E0D")]
			[Address(RVA = "0x101158DA8", Offset = "0x1158DA8", VA = "0x101158DA8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000F3C RID: 3900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E0E")]
		[Address(RVA = "0x101158DB0", Offset = "0x1158DB0", VA = "0x101158DB0")]
		public void Setup(string resourceName, MeigeAnimClipHolder meigeAnimClipHolder, CharacterHandler owner, List<CharacterHandler> tgtCharaHndls, CharacterHandler tgtSingleCharaHndl, List<CharacterHandler> myCharaHndls, CharacterHandler mySingleCharaHndl)
		{
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E0F")]
		[Address(RVA = "0x101159DBC", Offset = "0x1159DBC", VA = "0x101159DBC")]
		public void Destroy()
		{
		}

		// Token: 0x06000F3E RID: 3902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E10")]
		[Address(RVA = "0x10115A13C", Offset = "0x115A13C", VA = "0x10115A13C")]
		public void Update()
		{
		}

		// Token: 0x06000F3F RID: 3903 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E11")]
		[Address(RVA = "0x10115A3FC", Offset = "0x115A3FC", VA = "0x10115A3FC")]
		public List<BattleUniqueSkillBaseObjectHandler.CharaRender> GetCharaRenderCache()
		{
			return null;
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E12")]
		[Address(RVA = "0x10115A404", Offset = "0x115A404", VA = "0x10115A404")]
		public List<Renderer> GetRenderCache()
		{
			return null;
		}

		// Token: 0x06000F41 RID: 3905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E13")]
		[Address(RVA = "0x101159E98", Offset = "0x1159E98", VA = "0x101159E98")]
		public void DetachAllCharas()
		{
		}

		// Token: 0x06000F42 RID: 3906 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E14")]
		[Address(RVA = "0x10115A40C", Offset = "0x115A40C", VA = "0x10115A40C")]
		public Transform GetLocator(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			return null;
		}

		// Token: 0x06000F43 RID: 3907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E15")]
		[Address(RVA = "0x10115A540", Offset = "0x115A540", VA = "0x10115A540")]
		private void SetLocatorStatus(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType, bool isOn, CharacterHandler charaHndl)
		{
		}

		// Token: 0x06000F44 RID: 3908 RVA: 0x00006870 File Offset: 0x00004A70
		[Token(Token = "0x6000E16")]
		[Address(RVA = "0x10115A604", Offset = "0x115A604", VA = "0x10115A604")]
		private bool IsLocatorStatus(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			return default(bool);
		}

		// Token: 0x06000F45 RID: 3909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E17")]
		[Address(RVA = "0x10115A688", Offset = "0x115A688", VA = "0x10115A688")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x06000F46 RID: 3910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E18")]
		[Address(RVA = "0x10115A0A0", Offset = "0x115A0A0", VA = "0x10115A0A0")]
		public void DetachCamera()
		{
		}

		// Token: 0x06000F47 RID: 3911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E19")]
		[Address(RVA = "0x10115A790", Offset = "0x115A790", VA = "0x10115A790")]
		public void Play()
		{
		}

		// Token: 0x06000F48 RID: 3912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E1A")]
		[Address(RVA = "0x10115A8F4", Offset = "0x115A8F4", VA = "0x10115A8F4")]
		public void Pause()
		{
		}

		// Token: 0x06000F49 RID: 3913 RVA: 0x00006888 File Offset: 0x00004A88
		[Token(Token = "0x6000E1B")]
		[Address(RVA = "0x10115A990", Offset = "0x115A990", VA = "0x10115A990")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000F4A RID: 3914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E1C")]
		[Address(RVA = "0x10115A140", Offset = "0x115A140", VA = "0x10115A140")]
		private void UpdateVisiblity()
		{
		}

		// Token: 0x06000F4B RID: 3915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E1D")]
		[Address(RVA = "0x10115AA10", Offset = "0x115AA10", VA = "0x10115AA10")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x06000F4C RID: 3916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E1E")]
		[Address(RVA = "0x10115B3D8", Offset = "0x115B3D8", VA = "0x10115B3D8")]
		private void OnMeigeAnimEvent_FadeIn(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E1F")]
		[Address(RVA = "0x10115B4C4", Offset = "0x115B4C4", VA = "0x10115B4C4")]
		private void OnMeigeAnimEvent_FadeOut(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F4E RID: 3918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E20")]
		[Address(RVA = "0x10115B5B0", Offset = "0x115B5B0", VA = "0x10115B5B0")]
		private void OnMeigeAnimEvent_TgtSingle_OnOff(int onOff)
		{
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E21")]
		[Address(RVA = "0x10115B828", Offset = "0x115B828", VA = "0x10115B828")]
		private void OnMeigeAnimEvent_TgtAll_OnOff(int onOff)
		{
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E22")]
		[Address(RVA = "0x10115BA98", Offset = "0x115BA98", VA = "0x10115BA98")]
		private void OnMeigeAnimEvent_Tgt_OnOff(int targetLocator, int onOff)
		{
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E23")]
		[Address(RVA = "0x10115BD78", Offset = "0x115BD78", VA = "0x10115BD78")]
		private void OnMeigeAnimEvent_MySingle_OnOff(int onOff, int fixOwnerRootBoneTransform)
		{
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E24")]
		[Address(RVA = "0x10115C080", Offset = "0x115C080", VA = "0x10115C080")]
		private void OnMeigeAnimEvent_MyAll_OnOff(int onOff, int fixOwnerRootBoneTransform)
		{
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E25")]
		[Address(RVA = "0x10115C590", Offset = "0x115C590", VA = "0x10115C590")]
		private void OnMeigeAnimEvent_My_OnOff(int targetLocator, int onOff)
		{
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E26")]
		[Address(RVA = "0x10115C874", Offset = "0x115C874", VA = "0x10115C874")]
		private void OnMeigeAnimEvent_DamageAnim(int targetLocator)
		{
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E27")]
		[Address(RVA = "0x10115CC14", Offset = "0x115CC14", VA = "0x10115CC14")]
		private void OnMeigeAnimEvent_WeaponVisible(int onOff, int option0, int option1)
		{
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E28")]
		[Address(RVA = "0x10115CDF0", Offset = "0x115CDF0", VA = "0x10115CDF0")]
		private void OnMeigeAnimEvent_MeshColor_TgtSingle_Change(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E29")]
		[Address(RVA = "0x10115CF54", Offset = "0x115CF54", VA = "0x10115CF54")]
		private void OnMeigeAnimEvent_MeshColor_TgtSingle_Revert(int miliSec)
		{
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2A")]
		[Address(RVA = "0x10115D0B8", Offset = "0x115D0B8", VA = "0x10115D0B8")]
		private void OnMeigeAnimEvent_MeshColor_TgtAll_Change(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2B")]
		[Address(RVA = "0x10115D258", Offset = "0x115D258", VA = "0x10115D258")]
		private void OnMeigeAnimEvent_MeshColor_TgtAll_Revert(int miliSec)
		{
		}

		// Token: 0x06000F5A RID: 3930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2C")]
		[Address(RVA = "0x10115D3F8", Offset = "0x115D3F8", VA = "0x10115D3F8")]
		private void OnMeigeAnimEvent_MeshColor_MySingle_Change(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F5B RID: 3931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2D")]
		[Address(RVA = "0x10115D55C", Offset = "0x115D55C", VA = "0x10115D55C")]
		private void OnMeigeAnimEvent_MeshColor_MySingle_Revert(int miliSec)
		{
		}

		// Token: 0x06000F5C RID: 3932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2E")]
		[Address(RVA = "0x10115D6C0", Offset = "0x115D6C0", VA = "0x10115D6C0")]
		private void OnMeigeAnimEvent_MeshColor_MyAll_Change(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E2F")]
		[Address(RVA = "0x10115D860", Offset = "0x115D860", VA = "0x10115D860")]
		private void OnMeigeAnimEvent_MeshColor_MyAll_Revert(int miliSec)
		{
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E30")]
		[Address(RVA = "0x10115DA00", Offset = "0x115DA00", VA = "0x10115DA00")]
		private void OnMeigeAnimEvent_MeshColor_Self_Change(int miliSec, int colorType)
		{
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E31")]
		[Address(RVA = "0x10115DB0C", Offset = "0x115DB0C", VA = "0x10115DB0C")]
		private void OnMeigeAnimEvent_MeshColor_Self_Revert(int miliSec)
		{
		}

		// Token: 0x06000F60 RID: 3936 RVA: 0x000068A0 File Offset: 0x00004AA0
		[Token(Token = "0x6000E32")]
		[Address(RVA = "0x10115DDFC", Offset = "0x115DDFC", VA = "0x10115DDFC")]
		private Color GetMeshColorChangeType(int colorType)
		{
			return default(Color);
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E33")]
		[Address(RVA = "0x10115DC18", Offset = "0x115DC18", VA = "0x10115DC18")]
		private void OnMeigeAnimEvent_ShadowVisible_Owner(int onOff)
		{
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E34")]
		[Address(RVA = "0x10115DC54", Offset = "0x115DC54", VA = "0x10115DC54")]
		private void OnMeigeAnimEvent_ShadowVisible_TgtAll(int onOff)
		{
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E35")]
		[Address(RVA = "0x10115DD28", Offset = "0x115DD28", VA = "0x10115DD28")]
		private void OnMeigeAnimEvent_ShadowVisible_MyAll(int onOff)
		{
		}

		// Token: 0x06000F64 RID: 3940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E36")]
		[Address(RVA = "0x101159B54", Offset = "0x1159B54", VA = "0x101159B54")]
		private void SetCharaParent(CharacterHandler charaHndl, Transform parent)
		{
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E37")]
		[Address(RVA = "0x10115DE04", Offset = "0x115DE04", VA = "0x10115DE04")]
		public BattleUniqueSkillBaseObjectHandler()
		{
		}

		// Token: 0x040011A9 RID: 4521
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CCC")]
		private readonly string[] ASSOCIATED_LOCATOR_MESH_NAMES;

		// Token: 0x040011AA RID: 4522
		[Token(Token = "0x4000CCD")]
		private const string LOCATOR_TGT_0 = "loc_TGT_0";

		// Token: 0x040011AB RID: 4523
		[Token(Token = "0x4000CCE")]
		private const string LOCATOR_TGT_1 = "loc_TGT_1";

		// Token: 0x040011AC RID: 4524
		[Token(Token = "0x4000CCF")]
		private const string LOCATOR_TGT_2 = "loc_TGT_2";

		// Token: 0x040011AD RID: 4525
		[Token(Token = "0x4000CD0")]
		private const string LOCATOR_MY_0 = "loc_MY_0";

		// Token: 0x040011AE RID: 4526
		[Token(Token = "0x4000CD1")]
		private const string LOCATOR_MY_1 = "loc_MY_1";

		// Token: 0x040011AF RID: 4527
		[Token(Token = "0x4000CD2")]
		private const string LOCATOR_MY_2 = "loc_MY_2";

		// Token: 0x040011B0 RID: 4528
		[Token(Token = "0x4000CD3")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x040011B1 RID: 4529
		[Token(Token = "0x4000CD4")]
		private const float IDLE_BLEND_SEC = 0.3f;

		// Token: 0x040011B2 RID: 4530
		[Token(Token = "0x4000CD5")]
		private const float NEAR_CLIP = 0.1f;

		// Token: 0x040011B3 RID: 4531
		[Token(Token = "0x4000CD6")]
		private const float FAR_CLIP = 200f;

		// Token: 0x040011B4 RID: 4532
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CD7")]
		private Transform m_BaseTransform;

		// Token: 0x040011B5 RID: 4533
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CD8")]
		private Animation m_Anim;

		// Token: 0x040011B6 RID: 4534
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000CD9")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040011B7 RID: 4535
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000CDA")]
		private MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x040011B8 RID: 4536
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000CDB")]
		private MsbHandler m_MsbHndl;

		// Token: 0x040011B9 RID: 4537
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000CDC")]
		private CharacterHandler m_Owner;

		// Token: 0x040011BA RID: 4538
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000CDD")]
		private List<CharacterHandler> m_TgtCharaHndls;

		// Token: 0x040011BB RID: 4539
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000CDE")]
		private CharacterHandler m_TgtSingleCharaHndl;

		// Token: 0x040011BC RID: 4540
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000CDF")]
		private List<CharacterHandler> m_MyCharaHndls;

		// Token: 0x040011BD RID: 4541
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000CE0")]
		private CharacterHandler m_MySingleCharaHndl;

		// Token: 0x040011BE RID: 4542
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000CE1")]
		private Dictionary<BattleUniqueSkillBaseObjectHandler.eLocatorType, BattleUniqueSkillBaseObjectHandler.LocatorStatus> m_CharaHndlsInLocator;

		// Token: 0x040011BF RID: 4543
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000CE2")]
		private List<MsbObjectHandler>[] m_AssociatedLocatorObjs;

		// Token: 0x040011C0 RID: 4544
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000CE3")]
		private List<MeigeParticleEmitter>[] m_AssociatedEmmiters;

		// Token: 0x040011C1 RID: 4545
		[Token(Token = "0x4000CE4")]
		private const int CHECK_CHARA_MESH_NUM = 3;

		// Token: 0x040011C2 RID: 4546
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000CE5")]
		private List<BattleUniqueSkillBaseObjectHandler.CharaRender> m_CharaRenderCache;

		// Token: 0x040011C3 RID: 4547
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000CE6")]
		private List<Renderer> m_RenderCache;

		// Token: 0x020003E7 RID: 999
		[Token(Token = "0x2000DA4")]
		public enum eLocatorType
		{
			// Token: 0x040011C5 RID: 4549
			[Token(Token = "0x400585F")]
			None = -1,
			// Token: 0x040011C6 RID: 4550
			[Token(Token = "0x4005860")]
			LOC_TGT_0,
			// Token: 0x040011C7 RID: 4551
			[Token(Token = "0x4005861")]
			LOC_TGT_1,
			// Token: 0x040011C8 RID: 4552
			[Token(Token = "0x4005862")]
			LOC_TGT_2,
			// Token: 0x040011C9 RID: 4553
			[Token(Token = "0x4005863")]
			LOC_MY_0,
			// Token: 0x040011CA RID: 4554
			[Token(Token = "0x4005864")]
			LOC_MY_1,
			// Token: 0x040011CB RID: 4555
			[Token(Token = "0x4005865")]
			LOC_MY_2,
			// Token: 0x040011CC RID: 4556
			[Token(Token = "0x4005866")]
			Num
		}

		// Token: 0x020003E8 RID: 1000
		[Token(Token = "0x2000DA5")]
		public enum eAnimEvent
		{
			// Token: 0x040011CE RID: 4558
			[Token(Token = "0x4005868")]
			FadeIn = 101,
			// Token: 0x040011CF RID: 4559
			[Token(Token = "0x4005869")]
			FadeOut,
			// Token: 0x040011D0 RID: 4560
			[Token(Token = "0x400586A")]
			TgtSingle_OnOff = 110,
			// Token: 0x040011D1 RID: 4561
			[Token(Token = "0x400586B")]
			TgtAll_OnOff,
			// Token: 0x040011D2 RID: 4562
			[Token(Token = "0x400586C")]
			Tgt_OnOff,
			// Token: 0x040011D3 RID: 4563
			[Token(Token = "0x400586D")]
			MySingle_OnOff = 120,
			// Token: 0x040011D4 RID: 4564
			[Token(Token = "0x400586E")]
			MyAll_OnOff,
			// Token: 0x040011D5 RID: 4565
			[Token(Token = "0x400586F")]
			My_OnOff,
			// Token: 0x040011D6 RID: 4566
			[Token(Token = "0x4005870")]
			DamageAnim = 130,
			// Token: 0x040011D7 RID: 4567
			[Token(Token = "0x4005871")]
			WeaponVisible = 140,
			// Token: 0x040011D8 RID: 4568
			[Token(Token = "0x4005872")]
			MeshColor_TgtSingle_Change = 150,
			// Token: 0x040011D9 RID: 4569
			[Token(Token = "0x4005873")]
			MeshColor_TgtSingle_Revert,
			// Token: 0x040011DA RID: 4570
			[Token(Token = "0x4005874")]
			MeshColor_TgtAll_Change,
			// Token: 0x040011DB RID: 4571
			[Token(Token = "0x4005875")]
			MeshColor_TgtAll_Revert,
			// Token: 0x040011DC RID: 4572
			[Token(Token = "0x4005876")]
			MeshColor_MySingle_Change = 160,
			// Token: 0x040011DD RID: 4573
			[Token(Token = "0x4005877")]
			MeshColor_MySingle_Revert,
			// Token: 0x040011DE RID: 4574
			[Token(Token = "0x4005878")]
			MeshColor_MyAll_Change,
			// Token: 0x040011DF RID: 4575
			[Token(Token = "0x4005879")]
			MeshColor_MyAll_Revert,
			// Token: 0x040011E0 RID: 4576
			[Token(Token = "0x400587A")]
			MeshColor_Self_Change = 170,
			// Token: 0x040011E1 RID: 4577
			[Token(Token = "0x400587B")]
			MeshColor_Self_Revert,
			// Token: 0x040011E2 RID: 4578
			[Token(Token = "0x400587C")]
			ShadowVisible_Owner = 180,
			// Token: 0x040011E3 RID: 4579
			[Token(Token = "0x400587D")]
			ShadowVisible_TgtAll,
			// Token: 0x040011E4 RID: 4580
			[Token(Token = "0x400587E")]
			ShadowVisible_MyAll
		}

		// Token: 0x020003E9 RID: 1001
		[Token(Token = "0x2000DA6")]
		private class LocatorStatus
		{
			// Token: 0x06000F66 RID: 3942 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF2")]
			[Address(RVA = "0x101159B44", Offset = "0x1159B44", VA = "0x101159B44")]
			public LocatorStatus()
			{
			}

			// Token: 0x040011E5 RID: 4581
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400587F")]
			public bool m_IsOn;

			// Token: 0x040011E6 RID: 4582
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005880")]
			public CharacterHandler m_CharaHndl;
		}

		// Token: 0x020003EA RID: 1002
		[Token(Token = "0x2000DA7")]
		public class CharaRender
		{
			// Token: 0x06000F67 RID: 3943 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF3")]
			[Address(RVA = "0x101159B4C", Offset = "0x1159B4C", VA = "0x101159B4C")]
			public CharaRender()
			{
			}

			// Token: 0x040011E7 RID: 4583
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005881")]
			public List<Renderer> m_RenderCache;

			// Token: 0x040011E8 RID: 4584
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005882")]
			public Transform m_Transform;
		}
	}
}
