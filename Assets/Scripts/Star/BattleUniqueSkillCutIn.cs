﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003EB RID: 1003
	[Token(Token = "0x2000317")]
	[StructLayout(3)]
	public class BattleUniqueSkillCutIn : MonoBehaviour
	{
		// Token: 0x06000F68 RID: 3944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E38")]
		[Address(RVA = "0x10115E094", Offset = "0x115E094", VA = "0x10115E094")]
		private void OnDestroy()
		{
		}

		// Token: 0x06000F69 RID: 3945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E39")]
		[Address(RVA = "0x10115E0A0", Offset = "0x115E0A0", VA = "0x10115E0A0")]
		public void Play()
		{
		}

		// Token: 0x06000F6A RID: 3946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3A")]
		[Address(RVA = "0x10115E658", Offset = "0x115E658", VA = "0x10115E658")]
		public void Stop()
		{
		}

		// Token: 0x06000F6B RID: 3947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3B")]
		[Address(RVA = "0x10115E540", Offset = "0x115E540", VA = "0x10115E540")]
		private void AdjustScale()
		{
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3C")]
		[Address(RVA = "0x10115E6F8", Offset = "0x115E6F8", VA = "0x10115E6F8")]
		public BattleUniqueSkillCutIn()
		{
		}

		// Token: 0x040011E9 RID: 4585
		[Token(Token = "0x4000CE7")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x040011EA RID: 4586
		[Token(Token = "0x4000CE8")]
		private const string ANIM_KEY = "Take 001";

		// Token: 0x040011EB RID: 4587
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CE9")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040011EC RID: 4588
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CEA")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040011ED RID: 4589
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CEB")]
		[SerializeField]
		private GameObject m_ModelPrefab;

		// Token: 0x040011EE RID: 4590
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000CEC")]
		[SerializeField]
		private MeigeAnimClipHolder m_MeigeAnimClipHolder;

		// Token: 0x040011EF RID: 4591
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000CED")]
		[SerializeField]
		private int m_BaseWidth;

		// Token: 0x040011F0 RID: 4592
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000CEE")]
		[SerializeField]
		private int m_BaseHeight;

		// Token: 0x040011F1 RID: 4593
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000CEF")]
		private GameObject m_BodyObj;

		// Token: 0x040011F2 RID: 4594
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000CF0")]
		private Transform m_BaseTransform;

		// Token: 0x040011F3 RID: 4595
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000CF1")]
		private MsbHandler m_MsbHndl;

		// Token: 0x040011F4 RID: 4596
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000CF2")]
		private float m_SaveCameraDepth;
	}
}
