﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003EC RID: 1004
	[Token(Token = "0x2000318")]
	[StructLayout(3)]
	public class BattleUniqueSkillOwnerControllHandler : MonoBehaviour
	{
		// Token: 0x06000F6D RID: 3949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3D")]
		[Address(RVA = "0x10115E70C", Offset = "0x115E70C", VA = "0x10115E70C")]
		public void Setup(CharacterHandler owner, List<MeigeAnimClipHolder> meigeAnimClipHolders)
		{
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3E")]
		[Address(RVA = "0x10115EA74", Offset = "0x115EA74", VA = "0x10115EA74")]
		public void Destroy()
		{
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E3F")]
		[Address(RVA = "0x10115EDC0", Offset = "0x115EDC0", VA = "0x10115EDC0")]
		public void Play()
		{
		}

		// Token: 0x06000F70 RID: 3952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E40")]
		[Address(RVA = "0x10115EE70", Offset = "0x115EE70", VA = "0x10115EE70")]
		public void Pause()
		{
		}

		// Token: 0x06000F71 RID: 3953 RVA: 0x000068B8 File Offset: 0x00004AB8
		[Token(Token = "0x6000E41")]
		[Address(RVA = "0x10115EF0C", Offset = "0x115EF0C", VA = "0x10115EF0C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06000F72 RID: 3954 RVA: 0x000068D0 File Offset: 0x00004AD0
		[Token(Token = "0x6000E42")]
		[Address(RVA = "0x10115EF8C", Offset = "0x115EF8C", VA = "0x10115EF8C")]
		public float GetPlayingSec()
		{
			return 0f;
		}

		// Token: 0x06000F73 RID: 3955 RVA: 0x000068E8 File Offset: 0x00004AE8
		[Token(Token = "0x6000E43")]
		[Address(RVA = "0x10115F024", Offset = "0x115F024", VA = "0x10115F024")]
		public float GetMaxSec()
		{
			return 0f;
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x00006900 File Offset: 0x00004B00
		[Token(Token = "0x6000E44")]
		[Address(RVA = "0x10115F09C", Offset = "0x115F09C", VA = "0x10115F09C")]
		public WrapMode GetWrapMode()
		{
			return WrapMode.Default;
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E45")]
		[Address(RVA = "0x10115F128", Offset = "0x115F128", VA = "0x10115F128")]
		public BattleUniqueSkillOwnerControllHandler()
		{
		}

		// Token: 0x040011F5 RID: 4597
		[Token(Token = "0x4000CF3")]
		private const string PLAY_KEY = "skill";

		// Token: 0x040011F6 RID: 4598
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CF4")]
		private CharacterHandler m_Owner;

		// Token: 0x040011F7 RID: 4599
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CF5")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040011F8 RID: 4600
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CF6")]
		private List<string> m_ClipNames;
	}
}
