﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020003ED RID: 1005
	[Token(Token = "0x2000319")]
	[StructLayout(3)]
	public class BattleUniqueSkillScene
	{
		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000F76 RID: 3958 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C6")]
		public List<string> UnloadCueSheetNames
		{
			[Token(Token = "0x6000E46")]
			[Address(RVA = "0x10115F130", Offset = "0x115F130", VA = "0x10115F130")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E47")]
		[Address(RVA = "0x10115F138", Offset = "0x115F138", VA = "0x10115F138")]
		public BattleUniqueSkillScene()
		{
		}

		// Token: 0x06000F78 RID: 3960 RVA: 0x00006918 File Offset: 0x00004B18
		[Token(Token = "0x6000E48")]
		[Address(RVA = "0x10115F200", Offset = "0x115F200", VA = "0x10115F200")]
		public bool Prepare(string resourceName, BattleSystem system, Camera camera, CharacterHandler owner, List<CharacterHandler> tgtCharaHndls, CharacterHandler tgtSingleCharaHndl, List<CharacterHandler> myCharaHndls, CharacterHandler mySingleCharaHndl, bool isFadeOutStart, int togetherAttackChainCount)
		{
			return default(bool);
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x00006930 File Offset: 0x00004B30
		[Token(Token = "0x6000E49")]
		[Address(RVA = "0x10115F240", Offset = "0x115F240", VA = "0x10115F240")]
		public bool IsCompltePrepare()
		{
			return default(bool);
		}

		// Token: 0x06000F7A RID: 3962 RVA: 0x00006948 File Offset: 0x00004B48
		[Token(Token = "0x6000E4A")]
		[Address(RVA = "0x10115F260", Offset = "0x115F260", VA = "0x10115F260")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06000F7B RID: 3963 RVA: 0x00006960 File Offset: 0x00004B60
		[Token(Token = "0x6000E4B")]
		[Address(RVA = "0x10115F280", Offset = "0x115F280", VA = "0x10115F280")]
		public bool Destory()
		{
			return default(bool);
		}

		// Token: 0x06000F7C RID: 3964 RVA: 0x00006978 File Offset: 0x00004B78
		[Token(Token = "0x6000E4C")]
		[Address(RVA = "0x10115F2D4", Offset = "0x115F2D4", VA = "0x10115F2D4")]
		public bool IsComplteDestory()
		{
			return default(bool);
		}

		// Token: 0x06000F7D RID: 3965 RVA: 0x00006990 File Offset: 0x00004B90
		[Token(Token = "0x6000E4D")]
		[Address(RVA = "0x10115F2F8", Offset = "0x115F2F8", VA = "0x10115F2F8")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x000069A8 File Offset: 0x00004BA8
		[Token(Token = "0x6000E4E")]
		[Address(RVA = "0x10115F2B0", Offset = "0x115F2B0", VA = "0x10115F2B0")]
		public bool IsCompltePlay()
		{
			return default(bool);
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E4F")]
		[Address(RVA = "0x10115F324", Offset = "0x115F324", VA = "0x10115F324")]
		public void Update()
		{
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E50")]
		[Address(RVA = "0x101161834", Offset = "0x1161834", VA = "0x101161834")]
		public void LateUpdate()
		{
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x000069C0 File Offset: 0x00004BC0
		[Token(Token = "0x6000E51")]
		[Address(RVA = "0x1011619A4", Offset = "0x11619A4", VA = "0x1011619A4")]
		private SkillActionPlayer.RefAnimTime SkillActionPlayerGetRefAnimTime()
		{
			return default(SkillActionPlayer.RefAnimTime);
		}

		// Token: 0x06000F82 RID: 3970 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E52")]
		[Address(RVA = "0x101161A2C", Offset = "0x1161A2C", VA = "0x101161A2C")]
		public static string GetResourcePath(string resourceName)
		{
			return null;
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E53")]
		[Address(RVA = "0x101161B3C", Offset = "0x1161B3C", VA = "0x101161B3C")]
		public static string GetOwnerAnimObjectPath(CharacterDefine.ePlayerModelParts parts)
		{
			return null;
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E54")]
		[Address(RVA = "0x101161BA8", Offset = "0x1161BA8", VA = "0x101161BA8")]
		public static string GetBaseModelObjectPath()
		{
			return null;
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E55")]
		[Address(RVA = "0x101161BF0", Offset = "0x1161BF0", VA = "0x101161BF0")]
		public static string GetBaseAnimObjectPath()
		{
			return null;
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x000069D8 File Offset: 0x00004BD8
		[Token(Token = "0x6000E56")]
		[Address(RVA = "0x101161C38", Offset = "0x1161C38", VA = "0x101161C38")]
		private static eSoundBgmListDB GetBgmCueID(eRare rare, eClassType classType)
		{
			return eSoundBgmListDB.BGM_TITLE;
		}

		// Token: 0x06000F87 RID: 3975 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E57")]
		[Address(RVA = "0x101161C70", Offset = "0x1161C70", VA = "0x101161C70")]
		public Transform GetLocator(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			return null;
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E58")]
		[Address(RVA = "0x101161D20", Offset = "0x1161D20", VA = "0x101161D20")]
		public List<CharacterHandler> GetSortTargetCharaHndls()
		{
			return null;
		}

		// Token: 0x06000F89 RID: 3977 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E59")]
		[Address(RVA = "0x101161F08", Offset = "0x1161F08", VA = "0x101161F08")]
		public List<BattleUniqueSkillBaseObjectHandler.CharaRender> GetCharaRenderCache()
		{
			return null;
		}

		// Token: 0x06000F8A RID: 3978 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E5A")]
		[Address(RVA = "0x101161F9C", Offset = "0x1161F9C", VA = "0x101161F9C")]
		public List<Renderer> GetRenderCache()
		{
			return null;
		}

		// Token: 0x06000F8B RID: 3979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E5B")]
		[Address(RVA = "0x101162030", Offset = "0x1162030", VA = "0x101162030")]
		public void SetBaseObjectParticleSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x06000F8C RID: 3980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E5C")]
		[Address(RVA = "0x10116182C", Offset = "0x116182C", VA = "0x10116182C")]
		private void SetDestroyStep(BattleUniqueSkillScene.eDestroyStep step)
		{
		}

		// Token: 0x06000F8D RID: 3981 RVA: 0x000069F0 File Offset: 0x00004BF0
		[Token(Token = "0x6000E5D")]
		[Address(RVA = "0x101161280", Offset = "0x1161280", VA = "0x101161280")]
		private BattleUniqueSkillScene.eMode Update_Destroy()
		{
			return BattleUniqueSkillScene.eMode.Prepare;
		}

		// Token: 0x06000F8E RID: 3982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E5E")]
		[Address(RVA = "0x101161824", Offset = "0x1161824", VA = "0x101161824")]
		private void SetMainStep(BattleUniqueSkillScene.eMainStep step)
		{
		}

		// Token: 0x06000F8F RID: 3983 RVA: 0x00006A08 File Offset: 0x00004C08
		[Token(Token = "0x6000E5F")]
		[Address(RVA = "0x101160780", Offset = "0x1160780", VA = "0x101160780")]
		private BattleUniqueSkillScene.eMode Update_Main()
		{
			return BattleUniqueSkillScene.eMode.Prepare;
		}

		// Token: 0x06000F90 RID: 3984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E60")]
		[Address(RVA = "0x10116181C", Offset = "0x116181C", VA = "0x10116181C")]
		private void SetPrepareStep(BattleUniqueSkillScene.ePrepareStep step)
		{
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x00006A20 File Offset: 0x00004C20
		[Token(Token = "0x6000E61")]
		[Address(RVA = "0x10115F42C", Offset = "0x115F42C", VA = "0x10115F42C")]
		private BattleUniqueSkillScene.eMode Update_Prepare()
		{
			return BattleUniqueSkillScene.eMode.Prepare;
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E62")]
		[Address(RVA = "0x101162234", Offset = "0x1162234", VA = "0x101162234")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135F20", Offset = "0x135F20")]
		private void StartStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E63")]
		[Address(RVA = "0x101162238", Offset = "0x1162238", VA = "0x101162238")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100135F58", Offset = "0x135F58")]
		private void StopStopwatchForDebugPrepare(int prepareTarget, string opt)
		{
		}

		// Token: 0x040011F9 RID: 4601
		[Token(Token = "0x4000CF7")]
		public static readonly Color FADE_COLOR;

		// Token: 0x040011FA RID: 4602
		[Token(Token = "0x4000CF8")]
		public const float FADE_TIME = 0.12f;

		// Token: 0x040011FB RID: 4603
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000CF9")]
		private BattleUniqueSkillScene.eMode m_Mode;

		// Token: 0x040011FC RID: 4604
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000CFA")]
		private string m_ResourceName;

		// Token: 0x040011FD RID: 4605
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000CFB")]
		private BattleSystem m_System;

		// Token: 0x040011FE RID: 4606
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000CFC")]
		private Camera m_Camera;

		// Token: 0x040011FF RID: 4607
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000CFD")]
		private CharacterHandler m_Owner;

		// Token: 0x04001200 RID: 4608
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000CFE")]
		private List<CharacterHandler> m_TgtCharaHndls;

		// Token: 0x04001201 RID: 4609
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000CFF")]
		private CharacterHandler m_TgtSingleCharaHndl;

		// Token: 0x04001202 RID: 4610
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000D00")]
		private List<CharacterHandler> m_MyCharaHndls;

		// Token: 0x04001203 RID: 4611
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000D01")]
		private CharacterHandler m_MySingleCharaHndl;

		// Token: 0x04001204 RID: 4612
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000D02")]
		private bool m_IsFadeOutStart;

		// Token: 0x04001205 RID: 4613
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4000D03")]
		private int m_TogetherAttackChainCount;

		// Token: 0x04001206 RID: 4614
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000D04")]
		private GameObject m_OwnerControllObject;

		// Token: 0x04001207 RID: 4615
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000D05")]
		private BattleUniqueSkillOwnerControllHandler m_OwnerControllHandler;

		// Token: 0x04001208 RID: 4616
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000D06")]
		private GameObject m_BaseObject;

		// Token: 0x04001209 RID: 4617
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000D07")]
		private BattleUniqueSkillBaseObjectHandler m_BaseObjectHandler;

		// Token: 0x0400120A RID: 4618
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000D08")]
		private SoundFrameController[] m_SoundFrameControllers;

		// Token: 0x0400120B RID: 4619
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000D09")]
		private eSoundBgmListDB m_BgmCueID;

		// Token: 0x0400120C RID: 4620
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4000D0A")]
		private bool m_IsResumeBgm;

		// Token: 0x0400120D RID: 4621
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000D0B")]
		private List<string> m_UnloadCueSheetNames;

		// Token: 0x0400120E RID: 4622
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000D0C")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400120F RID: 4623
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000D0D")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04001210 RID: 4624
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000D0E")]
		private BattleUniqueSkillScene.eDestroyStep m_DestroyStep;

		// Token: 0x04001211 RID: 4625
		[Token(Token = "0x4000D0F")]
		public const float SKIPPABLE_TIME_FROM_START = 1f;

		// Token: 0x04001212 RID: 4626
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4000D10")]
		private bool m_IsSkippable;

		// Token: 0x04001213 RID: 4627
		[Cpp2IlInjected.FieldOffset(Offset = "0xAD")]
		[Token(Token = "0x4000D11")]
		private bool m_ExecutedSkip;

		// Token: 0x04001214 RID: 4628
		[Cpp2IlInjected.FieldOffset(Offset = "0xAE")]
		[Token(Token = "0x4000D12")]
		private bool m_WasStartedLastFadeOut;

		// Token: 0x04001215 RID: 4629
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000D13")]
		private BattleUniqueSkillScene.eMainStep m_MainStep;

		// Token: 0x04001216 RID: 4630
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4000D14")]
		private BattleUniqueSkillScene.ePrepareStep m_PrepareStep;

		// Token: 0x04001217 RID: 4631
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000D15")]
		private float m_NowLoadingDisplayTimer;

		// Token: 0x04001218 RID: 4632
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4000D16")]
		private bool m_DisplayedNowLoading;

		// Token: 0x020003EE RID: 1006
		[Token(Token = "0x2000DA8")]
		public enum eMode
		{
			// Token: 0x0400121A RID: 4634
			[Token(Token = "0x4005884")]
			None = -1,
			// Token: 0x0400121B RID: 4635
			[Token(Token = "0x4005885")]
			Prepare,
			// Token: 0x0400121C RID: 4636
			[Token(Token = "0x4005886")]
			UpdateMain,
			// Token: 0x0400121D RID: 4637
			[Token(Token = "0x4005887")]
			Destroy
		}

		// Token: 0x020003EF RID: 1007
		[Token(Token = "0x2000DA9")]
		private enum eSoundFrameControll
		{
			// Token: 0x0400121F RID: 4639
			[Token(Token = "0x4005889")]
			SE,
			// Token: 0x04001220 RID: 4640
			[Token(Token = "0x400588A")]
			VOICE,
			// Token: 0x04001221 RID: 4641
			[Token(Token = "0x400588B")]
			Num
		}

		// Token: 0x020003F0 RID: 1008
		[Token(Token = "0x2000DAA")]
		private enum eDestroyStep
		{
			// Token: 0x04001223 RID: 4643
			[Token(Token = "0x400588D")]
			None = -1,
			// Token: 0x04001224 RID: 4644
			[Token(Token = "0x400588E")]
			First,
			// Token: 0x04001225 RID: 4645
			[Token(Token = "0x400588F")]
			Transit_Wait,
			// Token: 0x04001226 RID: 4646
			[Token(Token = "0x4005890")]
			Destroy_0,
			// Token: 0x04001227 RID: 4647
			[Token(Token = "0x4005891")]
			Destroy_1,
			// Token: 0x04001228 RID: 4648
			[Token(Token = "0x4005892")]
			Destroy_2,
			// Token: 0x04001229 RID: 4649
			[Token(Token = "0x4005893")]
			Destroy_3,
			// Token: 0x0400122A RID: 4650
			[Token(Token = "0x4005894")]
			End
		}

		// Token: 0x020003F1 RID: 1009
		[Token(Token = "0x2000DAB")]
		private enum eMainStep
		{
			// Token: 0x0400122C RID: 4652
			[Token(Token = "0x4005896")]
			None = -1,
			// Token: 0x0400122D RID: 4653
			[Token(Token = "0x4005897")]
			First,
			// Token: 0x0400122E RID: 4654
			[Token(Token = "0x4005898")]
			Playing,
			// Token: 0x0400122F RID: 4655
			[Token(Token = "0x4005899")]
			SkipWait,
			// Token: 0x04001230 RID: 4656
			[Token(Token = "0x400589A")]
			End
		}

		// Token: 0x020003F2 RID: 1010
		[Token(Token = "0x2000DAC")]
		private enum ePrepareStep
		{
			// Token: 0x04001232 RID: 4658
			[Token(Token = "0x400589C")]
			None = -1,
			// Token: 0x04001233 RID: 4659
			[Token(Token = "0x400589D")]
			First,
			// Token: 0x04001234 RID: 4660
			[Token(Token = "0x400589E")]
			BattleSystem_PreProcess,
			// Token: 0x04001235 RID: 4661
			[Token(Token = "0x400589F")]
			Prepare,
			// Token: 0x04001236 RID: 4662
			[Token(Token = "0x40058A0")]
			Prepare_Wait,
			// Token: 0x04001237 RID: 4663
			[Token(Token = "0x40058A1")]
			Prepare_Error,
			// Token: 0x04001238 RID: 4664
			[Token(Token = "0x40058A2")]
			Setup,
			// Token: 0x04001239 RID: 4665
			[Token(Token = "0x40058A3")]
			End
		}
	}
}
