﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020003F3 RID: 1011
	[Token(Token = "0x200031A")]
	[StructLayout(3)]
	public static class BattleUtility
	{
		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000F95 RID: 3989 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C7")]
		private static BattleDefineDB CacheBtlDefDB
		{
			[Token(Token = "0x6000E65")]
			[Address(RVA = "0x1011622A0", Offset = "0x11622A0", VA = "0x1011622A0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000F96 RID: 3990 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000C8")]
		private static TogetherChargeDefineDB CacheTogeChgDefDB
		{
			[Token(Token = "0x6000E66")]
			[Address(RVA = "0x1011623C8", Offset = "0x11623C8", VA = "0x1011623C8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x00006A38 File Offset: 0x00004C38
		[Token(Token = "0x6000E67")]
		[Address(RVA = "0x101162190", Offset = "0x1162190", VA = "0x101162190")]
		public static float DefVal(eBattleDefineDB def)
		{
			return 0f;
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000E68")]
		[Address(RVA = "0x1011624F4", Offset = "0x11624F4", VA = "0x1011624F4")]
		public static void SetupCharaNameOnBattle(List<CharacterHandler> charas)
		{
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E69")]
		[Address(RVA = "0x10116272C", Offset = "0x116272C", VA = "0x10116272C")]
		public static string GetBattleBgResourcePath(int bgID)
		{
			return null;
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E6A")]
		[Address(RVA = "0x10116283C", Offset = "0x116283C", VA = "0x10116283C")]
		public static int[] GetBorderFromSkillLvs()
		{
			return null;
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x00006A50 File Offset: 0x00004C50
		[Token(Token = "0x6000E6B")]
		[Address(RVA = "0x1011629F0", Offset = "0x11629F0", VA = "0x1011629F0")]
		public static float TogetherChargeDefVal(eSkillContentType def)
		{
			return 0f;
		}

		// Token: 0x06000F9C RID: 3996 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E6C")]
		[Address(RVA = "0x101162A94", Offset = "0x1162A94", VA = "0x101162A94")]
		public static string BSDToSaveResponse(BattleSystemData bsd)
		{
			return null;
		}

		// Token: 0x06000F9D RID: 3997 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E6D")]
		[Address(RVA = "0x101162BE4", Offset = "0x1162BE4", VA = "0x101162BE4")]
		public static BattleSystemData SaveResponseToBSD(string save)
		{
			return null;
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x00006A68 File Offset: 0x00004C68
		[Token(Token = "0x6000E6E")]
		[Address(RVA = "0x101162CFC", Offset = "0x1162CFC", VA = "0x101162CFC")]
		public static int GetQuestIDFromSaveResponse(string save)
		{
			return 0;
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x00006A80 File Offset: 0x00004C80
		[Token(Token = "0x6000E6F")]
		[Address(RVA = "0x101158700", Offset = "0x1158700", VA = "0x101158700")]
		public static int GetTreasureBoxIndex(BattleDropItem battleDropItem)
		{
			return 0;
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x00006A98 File Offset: 0x00004C98
		[Token(Token = "0x6000E70")]
		[Address(RVA = "0x101162D74", Offset = "0x1162D74", VA = "0x101162D74")]
		public static int ConvertItemIdToTreasureBoxIndex(int itemID)
		{
			return 0;
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x00006AB0 File Offset: 0x00004CB0
		[Token(Token = "0x6000E71")]
		[Address(RVA = "0x101162E54", Offset = "0x1162E54", VA = "0x101162E54")]
		public static int ConvertRareToTreasureBoxIndex(eRare rare)
		{
			return 0;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E72")]
		[Address(RVA = "0x101162E74", Offset = "0x1162E74", VA = "0x101162E74")]
		public static List<QuestWaveListData> Convert(QuestWaveListDB db)
		{
			return null;
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E73")]
		[Address(RVA = "0x1011630DC", Offset = "0x11630DC", VA = "0x1011630DC")]
		public static List<QuestWaveRandomListData> Convert(QuestWaveRandomListDB db)
		{
			return null;
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E74")]
		[Address(RVA = "0x1011633B4", Offset = "0x11633B4", VA = "0x1011633B4")]
		public static List<QuestWaveDropItemData> Convert(QuestWaveDropsDB db)
		{
			return null;
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E75")]
		[Address(RVA = "0x1011635A4", Offset = "0x11635A4", VA = "0x1011635A4")]
		private static string TurnEndPreProcess_StatusChangeBuffString(float[] setParam)
		{
			return null;
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E76")]
		[Address(RVA = "0x101163778", Offset = "0x1163778", VA = "0x101163778")]
		private static string TurnEndPreProcess_StatusChangeStatusString(float[] setParam)
		{
			return null;
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E77")]
		[Address(RVA = "0x10116399C", Offset = "0x116399C", VA = "0x10116399C")]
		private static string TurnEndPreProcess_StatusChangeTargetString(eSkillTargetType targetType)
		{
			return null;
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000E78")]
		[Address(RVA = "0x101163AE0", Offset = "0x1163AE0", VA = "0x101163AE0")]
		public static string TurnEndPreProcess_GetCounterStatusMessage(CharacterHandler executor, eSkillTargetType targetType, float[] setParam)
		{
			return null;
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x00006AC8 File Offset: 0x00004CC8
		[Token(Token = "0x6000E79")]
		[Address(RVA = "0x101163D28", Offset = "0x1163D28", VA = "0x101163D28")]
		public static bool IsJoinSlot(BattleDefine.eJoinMember join)
		{
			return default(bool);
		}

		// Token: 0x0400123A RID: 4666
		[Token(Token = "0x4000D17")]
		private static BattleDefineDB m_CacheBtlDefDB;

		// Token: 0x0400123B RID: 4667
		[Token(Token = "0x4000D18")]
		private static TogetherChargeDefineDB m_CacheTogeChgDefDB;
	}
}
