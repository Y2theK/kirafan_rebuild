﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B2 RID: 1714
	[Token(Token = "0x2000574")]
	[StructLayout(3)]
	public class BinaryFileReader : BinaryReader
	{
		// Token: 0x06001901 RID: 6401 RVA: 0x0000B568 File Offset: 0x00009768
		[Token(Token = "0x6001785")]
		[Address(RVA = "0x101163D38", Offset = "0x1163D38", VA = "0x101163D38")]
		public bool OpenFile(string filename)
		{
			return default(bool);
		}

		// Token: 0x06001902 RID: 6402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001786")]
		[Address(RVA = "0x101163F20", Offset = "0x1163F20", VA = "0x101163F20")]
		public void ReadFile()
		{
		}

		// Token: 0x06001903 RID: 6403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001787")]
		[Address(RVA = "0x101163F24", Offset = "0x1163F24", VA = "0x101163F24")]
		public void CloseFile()
		{
		}

		// Token: 0x06001904 RID: 6404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001788")]
		[Address(RVA = "0x101163F2C", Offset = "0x1163F2C", VA = "0x101163F2C")]
		public BinaryFileReader()
		{
		}
	}
}
