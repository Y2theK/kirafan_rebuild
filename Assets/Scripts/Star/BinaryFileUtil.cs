﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B3 RID: 1715
	[Token(Token = "0x2000575")]
	[StructLayout(3)]
	public class BinaryFileUtil
	{
		// Token: 0x06001905 RID: 6405 RVA: 0x0000B580 File Offset: 0x00009780
		[Token(Token = "0x6001789")]
		[Address(RVA = "0x101163F3C", Offset = "0x1163F3C", VA = "0x101163F3C")]
		public static ushort MakeCipherCode()
		{
			return 0;
		}

		// Token: 0x06001906 RID: 6406 RVA: 0x0000B598 File Offset: 0x00009798
		[Token(Token = "0x600178A")]
		[Address(RVA = "0x101163FE8", Offset = "0x1163FE8", VA = "0x101163FE8")]
		public static ushort DecodeCode(byte[] pchk, int fsize, ushort fcode, int foffset)
		{
			return 0;
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x0000B5B0 File Offset: 0x000097B0
		[Token(Token = "0x600178B")]
		[Address(RVA = "0x101164114", Offset = "0x1164114", VA = "0x101164114")]
		public static ushort CipherCode(byte[] pchk, int fsize, ushort fcode, int foffset)
		{
			return 0;
		}

		// Token: 0x06001908 RID: 6408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600178C")]
		[Address(RVA = "0x101164238", Offset = "0x1164238", VA = "0x101164238")]
		public BinaryFileUtil()
		{
		}

		// Token: 0x020006B4 RID: 1716
		[Token(Token = "0x2000E12")]
		[StructLayout(2)]
		public struct TCipherKey
		{
			// Token: 0x04002993 RID: 10643
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005AC4")]
			public ushort m_code;

			// Token: 0x04002994 RID: 10644
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005AC5")]
			public byte m_key0;

			// Token: 0x04002995 RID: 10645
			[Cpp2IlInjected.FieldOffset(Offset = "0x1")]
			[Token(Token = "0x4005AC6")]
			public byte m_key1;
		}
	}
}
