﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B5 RID: 1717
	[Token(Token = "0x2000576")]
	[StructLayout(3)]
	public class BinaryFileWrite : BinaryWrite
	{
		// Token: 0x06001909 RID: 6409 RVA: 0x0000B5C8 File Offset: 0x000097C8
		[Token(Token = "0x600178D")]
		[Address(RVA = "0x101164240", Offset = "0x1164240", VA = "0x101164240")]
		public bool CreateFile(string filename)
		{
			return default(bool);
		}

		// Token: 0x0600190A RID: 6410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600178E")]
		[Address(RVA = "0x1011642C0", Offset = "0x11642C0", VA = "0x1011642C0")]
		public void WriteFile()
		{
		}

		// Token: 0x0600190B RID: 6411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600178F")]
		[Address(RVA = "0x10116490C", Offset = "0x116490C", VA = "0x10116490C")]
		public void CloseFile()
		{
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001790")]
		[Address(RVA = "0x101164950", Offset = "0x1164950", VA = "0x101164950")]
		public BinaryFileWrite()
		{
		}

		// Token: 0x04002996 RID: 10646
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002250")]
		private FileStream m_File;
	}
}
