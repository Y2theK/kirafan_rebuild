﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B6 RID: 1718
	[Token(Token = "0x2000577")]
	[StructLayout(3)]
	public interface BinaryIO
	{
		// Token: 0x0600190D RID: 6413
		[Token(Token = "0x6001791")]
		[Address(Slot = "0")]
		int Enum(Enum fkey, Type ftype);

		// Token: 0x0600190E RID: 6414
		[Token(Token = "0x6001792")]
		[Address(Slot = "1")]
		void Bool(ref bool fkey);

		// Token: 0x0600190F RID: 6415
		[Token(Token = "0x6001793")]
		[Address(Slot = "2")]
		void Byte(ref byte fkey);

		// Token: 0x06001910 RID: 6416
		[Token(Token = "0x6001794")]
		[Address(Slot = "3")]
		void Short(ref short fkey);

		// Token: 0x06001911 RID: 6417
		[Token(Token = "0x6001795")]
		[Address(Slot = "4")]
		void Int(ref int fkey);

		// Token: 0x06001912 RID: 6418
		[Token(Token = "0x6001796")]
		[Address(Slot = "5")]
		void Long(ref long fkey);

		// Token: 0x06001913 RID: 6419
		[Token(Token = "0x6001797")]
		[Address(Slot = "6")]
		void Float(ref float fkey);

		// Token: 0x06001914 RID: 6420
		[Token(Token = "0x6001798")]
		[Address(Slot = "7")]
		void String(ref string fkey);

		// Token: 0x06001915 RID: 6421
		[Token(Token = "0x6001799")]
		[Address(Slot = "8")]
		void ByteTable(ref byte[] fkey);

		// Token: 0x06001916 RID: 6422
		[Token(Token = "0x600179A")]
		[Address(Slot = "9")]
		void ShortTable(ref short[] fkey);

		// Token: 0x06001917 RID: 6423
		[Token(Token = "0x600179B")]
		[Address(Slot = "10")]
		void IntTable(ref int[] fkey);
	}
}
