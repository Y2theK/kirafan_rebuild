﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B7 RID: 1719
	[Token(Token = "0x2000578")]
	[StructLayout(3)]
	public class BinaryReader : BinaryIO
	{
		// Token: 0x06001918 RID: 6424 RVA: 0x0000B5E0 File Offset: 0x000097E0
		[Token(Token = "0x600179C")]
		[Address(RVA = "0x1011649DC", Offset = "0x11649DC", VA = "0x1011649DC", Slot = "4")]
		public int Enum(Enum fkey, Type ftype)
		{
			return 0;
		}

		// Token: 0x06001919 RID: 6425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600179D")]
		[Address(RVA = "0x101164A68", Offset = "0x1164A68", VA = "0x101164A68", Slot = "5")]
		public void Bool(ref bool fkey)
		{
		}

		// Token: 0x0600191A RID: 6426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600179E")]
		[Address(RVA = "0x101164AE0", Offset = "0x1164AE0", VA = "0x101164AE0", Slot = "6")]
		public void Byte(ref byte fkey)
		{
		}

		// Token: 0x0600191B RID: 6427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600179F")]
		[Address(RVA = "0x101164B50", Offset = "0x1164B50", VA = "0x101164B50", Slot = "7")]
		public void Short(ref short fkey)
		{
		}

		// Token: 0x0600191C RID: 6428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A0")]
		[Address(RVA = "0x101164BE4", Offset = "0x1164BE4", VA = "0x101164BE4", Slot = "8")]
		public void Int(ref int fkey)
		{
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A1")]
		[Address(RVA = "0x101164C78", Offset = "0x1164C78", VA = "0x101164C78", Slot = "9")]
		public void Long(ref long fkey)
		{
		}

		// Token: 0x0600191E RID: 6430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A2")]
		[Address(RVA = "0x101164D0C", Offset = "0x1164D0C", VA = "0x101164D0C", Slot = "10")]
		public void Float(ref float fkey)
		{
		}

		// Token: 0x0600191F RID: 6431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A3")]
		[Address(RVA = "0x101164DA0", Offset = "0x1164DA0", VA = "0x101164DA0", Slot = "11")]
		public void String(ref string fkey)
		{
		}

		// Token: 0x06001920 RID: 6432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A4")]
		[Address(RVA = "0x101164EB0", Offset = "0x1164EB0", VA = "0x101164EB0", Slot = "12")]
		public void ByteTable(ref byte[] fkey)
		{
		}

		// Token: 0x06001921 RID: 6433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A5")]
		[Address(RVA = "0x101164F84", Offset = "0x1164F84", VA = "0x101164F84", Slot = "13")]
		public void ShortTable(ref short[] fkey)
		{
		}

		// Token: 0x06001922 RID: 6434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A6")]
		[Address(RVA = "0x10116509C", Offset = "0x116509C", VA = "0x10116509C", Slot = "14")]
		public void IntTable(ref int[] fkey)
		{
		}

		// Token: 0x06001923 RID: 6435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A7")]
		[Address(RVA = "0x1011651B4", Offset = "0x11651B4", VA = "0x1011651B4")]
		public void SetBinary(byte[] pdat, int fsize)
		{
		}

		// Token: 0x06001924 RID: 6436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017A8")]
		[Address(RVA = "0x101163E9C", Offset = "0x1163E9C", VA = "0x101163E9C")]
		public void CreateBuffer(int fsize)
		{
		}

		// Token: 0x06001925 RID: 6437 RVA: 0x0000B5F8 File Offset: 0x000097F8
		[Token(Token = "0x60017A9")]
		[Address(RVA = "0x101165228", Offset = "0x1165228", VA = "0x101165228")]
		public byte GetByte()
		{
			return 0;
		}

		// Token: 0x06001926 RID: 6438 RVA: 0x0000B610 File Offset: 0x00009810
		[Token(Token = "0x60017AA")]
		[Address(RVA = "0x101165290", Offset = "0x1165290", VA = "0x101165290")]
		public short GetShort()
		{
			return 0;
		}

		// Token: 0x06001927 RID: 6439 RVA: 0x0000B628 File Offset: 0x00009828
		[Token(Token = "0x60017AB")]
		[Address(RVA = "0x10116531C", Offset = "0x116531C", VA = "0x10116531C")]
		public int GetInt()
		{
			return 0;
		}

		// Token: 0x06001928 RID: 6440 RVA: 0x0000B640 File Offset: 0x00009840
		[Token(Token = "0x60017AC")]
		[Address(RVA = "0x1011653A8", Offset = "0x11653A8", VA = "0x1011653A8")]
		public long GetLong()
		{
			return 0L;
		}

		// Token: 0x06001929 RID: 6441 RVA: 0x0000B658 File Offset: 0x00009858
		[Token(Token = "0x60017AD")]
		[Address(RVA = "0x101165434", Offset = "0x1165434", VA = "0x101165434")]
		public float GetFloat()
		{
			return 0f;
		}

		// Token: 0x0600192A RID: 6442 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017AE")]
		[Address(RVA = "0x1011654C0", Offset = "0x11654C0", VA = "0x1011654C0")]
		public string GetString()
		{
			return null;
		}

		// Token: 0x0600192B RID: 6443 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017AF")]
		[Address(RVA = "0x1011655C8", Offset = "0x11655C8", VA = "0x1011655C8")]
		public byte[] GetByteTable(int flen)
		{
			return null;
		}

		// Token: 0x0600192C RID: 6444 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017B0")]
		[Address(RVA = "0x10116565C", Offset = "0x116565C", VA = "0x10116565C")]
		public short[] GetShortTable(int flen)
		{
			return null;
		}

		// Token: 0x0600192D RID: 6445 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017B1")]
		[Address(RVA = "0x1011656F0", Offset = "0x11656F0", VA = "0x1011656F0")]
		public int[] GetIntTable(int flen)
		{
			return null;
		}

		// Token: 0x0600192E RID: 6446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017B2")]
		[Address(RVA = "0x101165784", Offset = "0x1165784", VA = "0x101165784")]
		public void Offset(int fslide)
		{
		}

		// Token: 0x0600192F RID: 6447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017B3")]
		[Address(RVA = "0x101165794", Offset = "0x1165794", VA = "0x101165794")]
		public void PushPoint(int foffset)
		{
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017B4")]
		[Address(RVA = "0x101165810", Offset = "0x1165810", VA = "0x101165810")]
		public void PopPoint()
		{
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017B5")]
		[Address(RVA = "0x101165880", Offset = "0x1165880", VA = "0x101165880")]
		public void SetMarking()
		{
		}

		// Token: 0x06001932 RID: 6450 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017B6")]
		[Address(RVA = "0x10116588C", Offset = "0x116588C", VA = "0x10116588C")]
		public string GetCPString()
		{
			return null;
		}

		// Token: 0x06001933 RID: 6451 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017B7")]
		[Address(RVA = "0x1011659A8", Offset = "0x11659A8", VA = "0x1011659A8")]
		public string GetMsgName()
		{
			return null;
		}

		// Token: 0x06001934 RID: 6452 RVA: 0x0000B670 File Offset: 0x00009870
		[Token(Token = "0x60017B8")]
		[Address(RVA = "0x101165B10", Offset = "0x1165B10", VA = "0x101165B10")]
		public bool IsEOF()
		{
			return default(bool);
		}

		// Token: 0x06001935 RID: 6453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017B9")]
		[Address(RVA = "0x101163F34", Offset = "0x1163F34", VA = "0x101163F34")]
		public BinaryReader()
		{
		}

		// Token: 0x04002997 RID: 10647
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002251")]
		public byte[] m_Buffer;

		// Token: 0x04002998 RID: 10648
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002252")]
		public int m_FilePoint;

		// Token: 0x04002999 RID: 10649
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002253")]
		public int m_FileSize;

		// Token: 0x0400299A RID: 10650
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002254")]
		public int[] m_StackPoint;

		// Token: 0x0400299B RID: 10651
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002255")]
		public int m_StackNum;

		// Token: 0x0400299C RID: 10652
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002256")]
		public int m_MarkingPoint;

		// Token: 0x020006B8 RID: 1720
		[Token(Token = "0x2000E13")]
		[StructLayout(2)]
		private struct ItoF
		{
			// Token: 0x0400299D RID: 10653
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005AC7")]
			public int i;

			// Token: 0x0400299E RID: 10654
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005AC8")]
			public float f;
		}
	}
}
