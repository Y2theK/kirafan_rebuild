﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B9 RID: 1721
	[Token(Token = "0x2000579")]
	[StructLayout(3)]
	public class BinaryWrite : BinaryIO
	{
		// Token: 0x06001936 RID: 6454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017BA")]
		[Address(RVA = "0x101165B20", Offset = "0x1165B20", VA = "0x101165B20")]
		public void PushPoint()
		{
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017BB")]
		[Address(RVA = "0x101165B90", Offset = "0x1165B90", VA = "0x101165B90")]
		public void PopPoint()
		{
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0000B688 File Offset: 0x00009888
		[Token(Token = "0x60017BC")]
		[Address(RVA = "0x101165C00", Offset = "0x1165C00", VA = "0x101165C00")]
		public int OffsetPoint()
		{
			return 0;
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x0000B6A0 File Offset: 0x000098A0
		[Token(Token = "0x60017BD")]
		[Address(RVA = "0x101165C10", Offset = "0x1165C10", VA = "0x101165C10")]
		public int FilePoint()
		{
			return 0;
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x0000B6B8 File Offset: 0x000098B8
		[Token(Token = "0x60017BE")]
		[Address(RVA = "0x101165C18", Offset = "0x1165C18", VA = "0x101165C18", Slot = "4")]
		public int Enum(Enum fkey, Type ftype)
		{
			return 0;
		}

		// Token: 0x0600193B RID: 6459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017BF")]
		[Address(RVA = "0x101165ECC", Offset = "0x1165ECC", VA = "0x101165ECC", Slot = "5")]
		public void Bool(ref bool fkey)
		{
		}

		// Token: 0x0600193C RID: 6460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C0")]
		[Address(RVA = "0x101166098", Offset = "0x1166098", VA = "0x101166098", Slot = "6")]
		public void Byte(ref byte fkey)
		{
		}

		// Token: 0x0600193D RID: 6461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C1")]
		[Address(RVA = "0x1011660A4", Offset = "0x11660A4", VA = "0x1011660A4", Slot = "7")]
		public void Short(ref short fkey)
		{
		}

		// Token: 0x0600193E RID: 6462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C2")]
		[Address(RVA = "0x101166270", Offset = "0x1166270", VA = "0x101166270", Slot = "8")]
		public void Int(ref int fkey)
		{
		}

		// Token: 0x0600193F RID: 6463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C3")]
		[Address(RVA = "0x10116627C", Offset = "0x116627C", VA = "0x10116627C", Slot = "9")]
		public void Long(ref long fkey)
		{
		}

		// Token: 0x06001940 RID: 6464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C4")]
		[Address(RVA = "0x101166448", Offset = "0x1166448", VA = "0x101166448", Slot = "10")]
		public void Float(ref float fkey)
		{
		}

		// Token: 0x06001941 RID: 6465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C5")]
		[Address(RVA = "0x101166614", Offset = "0x1166614", VA = "0x101166614", Slot = "11")]
		public void String(ref string fkey)
		{
		}

		// Token: 0x06001942 RID: 6466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C6")]
		[Address(RVA = "0x1011668B4", Offset = "0x11668B4", VA = "0x1011668B4", Slot = "12")]
		public void ByteTable(ref byte[] fkey)
		{
		}

		// Token: 0x06001943 RID: 6467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C7")]
		[Address(RVA = "0x101166B20", Offset = "0x1166B20", VA = "0x101166B20", Slot = "13")]
		public void ShortTable(ref short[] fkey)
		{
		}

		// Token: 0x06001944 RID: 6468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017C8")]
		[Address(RVA = "0x101166D94", Offset = "0x1166D94", VA = "0x101166D94", Slot = "14")]
		public void IntTable(ref int[] fkey)
		{
		}

		// Token: 0x06001945 RID: 6469 RVA: 0x0000B6D0 File Offset: 0x000098D0
		[Token(Token = "0x60017C9")]
		[Address(RVA = "0x101165EE0", Offset = "0x1165EE0", VA = "0x101165EE0")]
		public int SetByte(byte fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x06001946 RID: 6470 RVA: 0x0000B6E8 File Offset: 0x000098E8
		[Token(Token = "0x60017CA")]
		[Address(RVA = "0x1011660B0", Offset = "0x11660B0", VA = "0x1011660B0")]
		public int SetShort(short fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x06001947 RID: 6471 RVA: 0x0000B700 File Offset: 0x00009900
		[Token(Token = "0x60017CB")]
		[Address(RVA = "0x101165D10", Offset = "0x1165D10", VA = "0x101165D10")]
		public int SetInt(int fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x06001948 RID: 6472 RVA: 0x0000B718 File Offset: 0x00009918
		[Token(Token = "0x60017CC")]
		[Address(RVA = "0x101166288", Offset = "0x1166288", VA = "0x101166288")]
		public int SetLong(long fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x06001949 RID: 6473 RVA: 0x0000B730 File Offset: 0x00009930
		[Token(Token = "0x60017CD")]
		[Address(RVA = "0x101166454", Offset = "0x1166454", VA = "0x101166454")]
		public int SetFloat(float fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x0600194A RID: 6474 RVA: 0x0000B748 File Offset: 0x00009948
		[Token(Token = "0x60017CE")]
		[Address(RVA = "0x10116661C", Offset = "0x116661C", VA = "0x10116661C")]
		public int SetString(string fname)
		{
			return 0;
		}

		// Token: 0x0600194B RID: 6475 RVA: 0x0000B760 File Offset: 0x00009960
		[Token(Token = "0x60017CF")]
		[Address(RVA = "0x101166908", Offset = "0x1166908", VA = "0x101166908")]
		public int SetByteTable(byte[] fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x0600194C RID: 6476 RVA: 0x0000B778 File Offset: 0x00009978
		[Token(Token = "0x60017D0")]
		[Address(RVA = "0x101166B74", Offset = "0x1166B74", VA = "0x101166B74")]
		public int SetShortTable(short[] fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x0600194D RID: 6477 RVA: 0x0000B790 File Offset: 0x00009990
		[Token(Token = "0x60017D1")]
		[Address(RVA = "0x101166DE8", Offset = "0x1166DE8", VA = "0x101166DE8")]
		public int SetIntTable(int[] fdat, int faccessid = -1)
		{
			return 0;
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017D2")]
		[Address(RVA = "0x101167144", Offset = "0x1167144", VA = "0x101167144")]
		public void Offset(int fslide)
		{
		}

		// Token: 0x0600194F RID: 6479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017D3")]
		[Address(RVA = "0x101167250", Offset = "0x1167250", VA = "0x101167250")]
		public void Align(int falign)
		{
		}

		// Token: 0x06001950 RID: 6480 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017D4")]
		[Address(RVA = "0x10116701C", Offset = "0x116701C", VA = "0x10116701C")]
		private BinaryWrite.IWriteData FindData(int fkey)
		{
			return null;
		}

		// Token: 0x06001951 RID: 6481 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017D5")]
		[Address(RVA = "0x101167368", Offset = "0x1167368", VA = "0x101167368")]
		public byte[] PackToByte()
		{
			return null;
		}

		// Token: 0x06001952 RID: 6482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017D6")]
		[Address(RVA = "0x101164954", Offset = "0x1164954", VA = "0x101164954")]
		public BinaryWrite()
		{
		}

		// Token: 0x0400299F RID: 10655
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002257")]
		public List<BinaryWrite.IWriteData> m_Table;

		// Token: 0x040029A0 RID: 10656
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002258")]
		public int m_FileSize;

		// Token: 0x040029A1 RID: 10657
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002259")]
		public int[] m_StackPoint;

		// Token: 0x040029A2 RID: 10658
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400225A")]
		public int m_StackNum;

		// Token: 0x040029A3 RID: 10659
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400225B")]
		public int m_MarkPoint;

		// Token: 0x020006BA RID: 1722
		[Token(Token = "0x2000E14")]
		public enum eData
		{
			// Token: 0x040029A5 RID: 10661
			[Token(Token = "0x4005ACA")]
			Byte,
			// Token: 0x040029A6 RID: 10662
			[Token(Token = "0x4005ACB")]
			Short,
			// Token: 0x040029A7 RID: 10663
			[Token(Token = "0x4005ACC")]
			Int,
			// Token: 0x040029A8 RID: 10664
			[Token(Token = "0x4005ACD")]
			Long,
			// Token: 0x040029A9 RID: 10665
			[Token(Token = "0x4005ACE")]
			Float,
			// Token: 0x040029AA RID: 10666
			[Token(Token = "0x4005ACF")]
			String,
			// Token: 0x040029AB RID: 10667
			[Token(Token = "0x4005AD0")]
			Table
		}

		// Token: 0x020006BB RID: 1723
		[Token(Token = "0x2000E15")]
		public class IWriteData
		{
			// Token: 0x06001953 RID: 6483 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E40")]
			[Address(RVA = "0x101167CDC", Offset = "0x1167CDC", VA = "0x101167CDC")]
			public IWriteData()
			{
			}

			// Token: 0x040029AC RID: 10668
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AD1")]
			public BinaryWrite.eData m_Type;

			// Token: 0x040029AD RID: 10669
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005AD2")]
			public int m_FilePoint;
		}

		// Token: 0x020006BC RID: 1724
		[Token(Token = "0x2000E16")]
		public class TableWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001954 RID: 6484 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E41")]
			[Address(RVA = "0x10116713C", Offset = "0x116713C", VA = "0x10116713C")]
			public TableWriteData()
			{
			}

			// Token: 0x040029AE RID: 10670
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD3")]
			public byte[] m_Data;
		}

		// Token: 0x020006BD RID: 1725
		[Token(Token = "0x2000E17")]
		public class ByteWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001955 RID: 6485 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E42")]
			[Address(RVA = "0x101167014", Offset = "0x1167014", VA = "0x101167014")]
			public ByteWriteData()
			{
			}

			// Token: 0x040029AF RID: 10671
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD4")]
			public byte m_Data;
		}

		// Token: 0x020006BE RID: 1726
		[Token(Token = "0x2000E18")]
		public class ShortWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001956 RID: 6486 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E43")]
			[Address(RVA = "0x10116711C", Offset = "0x116711C", VA = "0x10116711C")]
			public ShortWriteData()
			{
			}

			// Token: 0x040029B0 RID: 10672
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD5")]
			public short m_Data;
		}

		// Token: 0x020006BF RID: 1727
		[Token(Token = "0x2000E19")]
		public class IntWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001957 RID: 6487 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E44")]
			[Address(RVA = "0x101167124", Offset = "0x1167124", VA = "0x101167124")]
			public IntWriteData()
			{
			}

			// Token: 0x040029B1 RID: 10673
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD6")]
			public int m_Data;
		}

		// Token: 0x020006C0 RID: 1728
		[Token(Token = "0x2000E1A")]
		public class LongWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001958 RID: 6488 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E45")]
			[Address(RVA = "0x10116712C", Offset = "0x116712C", VA = "0x10116712C")]
			public LongWriteData()
			{
			}

			// Token: 0x040029B2 RID: 10674
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD7")]
			public long m_Data;
		}

		// Token: 0x020006C1 RID: 1729
		[Token(Token = "0x2000E1B")]
		public class FloatWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x06001959 RID: 6489 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E46")]
			[Address(RVA = "0x101167134", Offset = "0x1167134", VA = "0x101167134")]
			public FloatWriteData()
			{
			}

			// Token: 0x040029B3 RID: 10675
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD8")]
			public float m_Data;
		}

		// Token: 0x020006C2 RID: 1730
		[Token(Token = "0x2000E1C")]
		public class StringWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x0600195A RID: 6490 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E47")]
			[Address(RVA = "0x101167CE4", Offset = "0x1167CE4", VA = "0x101167CE4")]
			public StringWriteData()
			{
			}

			// Token: 0x040029B4 RID: 10676
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AD9")]
			public string m_Data;
		}
	}
}
