﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B2A RID: 2858
	[Token(Token = "0x20007D1")]
	[StructLayout(3)]
	public class BindTownCharaMap : ITownEventCommand
	{
		// Token: 0x06003244 RID: 12868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF7")]
		[Address(RVA = "0x101167CEC", Offset = "0x1167CEC", VA = "0x101167CEC")]
		public BindTownCharaMap(long fmngid, TownBuilder pbuiler)
		{
		}

		// Token: 0x06003245 RID: 12869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF8")]
		[Address(RVA = "0x101167D30", Offset = "0x1167D30", VA = "0x101167D30")]
		public void SetLinkBindKey(Transform parent, int flayerid, float foffset)
		{
		}

		// Token: 0x06003246 RID: 12870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF9")]
		[Address(RVA = "0x101167D90", Offset = "0x1167D90", VA = "0x101167D90")]
		public void ChangeManageID(long fmngid)
		{
		}

		// Token: 0x06003247 RID: 12871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DFA")]
		[Address(RVA = "0x101167DA0", Offset = "0x1167DA0", VA = "0x101167DA0")]
		public void CalcCharaPopIconPos(long fmanageid, int flayerid, float foffset)
		{
		}

		// Token: 0x06003248 RID: 12872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DFB")]
		[Address(RVA = "0x1011681D0", Offset = "0x11681D0", VA = "0x1011681D0")]
		public void SetViewActive(bool factive)
		{
		}

		// Token: 0x06003249 RID: 12873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DFC")]
		[Address(RVA = "0x101168304", Offset = "0x1168304", VA = "0x101168304")]
		public void BindGameObject(ITownObjectHandler pobj)
		{
		}

		// Token: 0x0600324A RID: 12874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DFD")]
		[Address(RVA = "0x101168584", Offset = "0x1168584", VA = "0x101168584")]
		public void ReBindGameObject(ITownObjectHandler pobj)
		{
		}

		// Token: 0x0600324B RID: 12875 RVA: 0x000156C0 File Offset: 0x000138C0
		[Token(Token = "0x6002DFE")]
		[Address(RVA = "0x10116884C", Offset = "0x116884C", VA = "0x10116884C")]
		public int BackLinkCut(TownBuilder pbuilder, long fmanageid, int flayerid, int fsynckey = -1)
		{
			return 0;
		}

		// Token: 0x0600324C RID: 12876 RVA: 0x000156D8 File Offset: 0x000138D8
		[Token(Token = "0x6002DFF")]
		[Address(RVA = "0x101168A48", Offset = "0x1168A48", VA = "0x101168A48", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x040041E1 RID: 16865
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EC8")]
		private ITownObjectHandler[] m_BindObject;

		// Token: 0x040041E2 RID: 16866
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EC9")]
		private int m_BindObjNum;

		// Token: 0x040041E3 RID: 16867
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002ECA")]
		private int m_BindObjMax;

		// Token: 0x040041E4 RID: 16868
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002ECB")]
		private bool m_ViewAct;

		// Token: 0x040041E5 RID: 16869
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002ECC")]
		private TownBuilder m_Builder;

		// Token: 0x040041E6 RID: 16870
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002ECD")]
		private Vector3 m_BasePos;

		// Token: 0x040041E7 RID: 16871
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002ECE")]
		private int m_LayerID;

		// Token: 0x040041E8 RID: 16872
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002ECF")]
		private float m_OffsetPos;

		// Token: 0x040041E9 RID: 16873
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002ED0")]
		public long m_ManageID;
	}
}
