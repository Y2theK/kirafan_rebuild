﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006A3 RID: 1699
	// (Invoke) Token: 0x0600187E RID: 6270
	[Token(Token = "0x200056B")]
	[StructLayout(3, Size = 8)]
	public delegate void BuildUpCallback(int buildpoint, int objID, long fmanageid);
}
