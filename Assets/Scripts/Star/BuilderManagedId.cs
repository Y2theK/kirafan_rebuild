﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A3F RID: 2623
	[Token(Token = "0x200074C")]
	[StructLayout(0, Size = 8)]
	public struct BuilderManagedId
	{
		// Token: 0x06002D46 RID: 11590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029A3")]
		[Address(RVA = "0x10002F3C0", Offset = "0x2F3C0", VA = "0x10002F3C0")]
		public BuilderManagedId(long id)
		{
		}

		// Token: 0x06002D47 RID: 11591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029A4")]
		[Address(RVA = "0x10002F3C8", Offset = "0x2F3C8", VA = "0x10002F3C8")]
		public BuilderManagedId(BuilderManagedId builderManagedId)
		{
		}

		// Token: 0x06002D48 RID: 11592 RVA: 0x00013578 File Offset: 0x00011778
		[Token(Token = "0x60029A5")]
		[Address(RVA = "0x101168F1C", Offset = "0x1168F1C", VA = "0x101168F1C")]
		public static implicit operator BuilderManagedId(long value)
		{
			return default(BuilderManagedId);
		}

		// Token: 0x06002D49 RID: 11593 RVA: 0x00013590 File Offset: 0x00011790
		[Token(Token = "0x60029A6")]
		[Address(RVA = "0x101168F20", Offset = "0x1168F20", VA = "0x101168F20")]
		public static implicit operator long(BuilderManagedId value)
		{
			return 0L;
		}

		// Token: 0x04003CAC RID: 15532
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002BA9")]
		public long id;
	}
}
