﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000951 RID: 2385
	[Token(Token = "0x20006BC")]
	[StructLayout(3)]
	public class CActScriptKeyAnimeWait : IRoomScriptData
	{
		// Token: 0x06002802 RID: 10242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CC")]
		[Address(RVA = "0x101168F24", Offset = "0x1168F24", VA = "0x101168F24", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002803 RID: 10243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CD")]
		[Address(RVA = "0x101169030", Offset = "0x1169030", VA = "0x101169030")]
		public CActScriptKeyAnimeWait()
		{
		}

		// Token: 0x04003831 RID: 14385
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028A3")]
		public float m_OffsetTime;
	}
}
