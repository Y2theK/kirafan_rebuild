﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000960 RID: 2400
	[Token(Token = "0x20006C7")]
	[StructLayout(3)]
	public class CActScriptKeyBaseTrs : IRoomScriptData
	{
		// Token: 0x06002818 RID: 10264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E2")]
		[Address(RVA = "0x101169098", Offset = "0x1169098", VA = "0x101169098", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002819 RID: 10265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E3")]
		[Address(RVA = "0x101169364", Offset = "0x1169364", VA = "0x101169364")]
		public CActScriptKeyBaseTrs()
		{
		}

		// Token: 0x04003867 RID: 14439
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028C1")]
		public string m_TargetName;

		// Token: 0x04003868 RID: 14440
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028C2")]
		public uint m_CRCKey;

		// Token: 0x04003869 RID: 14441
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028C3")]
		public Vector3 m_Pos;

		// Token: 0x0400386A RID: 14442
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40028C4")]
		public float m_Rot;
	}
}
