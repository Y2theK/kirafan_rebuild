﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200096E RID: 2414
	[Token(Token = "0x20006D5")]
	[StructLayout(3)]
	public class CActScriptKeyBaseTrsNonRev : IRoomScriptData
	{
		// Token: 0x06002834 RID: 10292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FE")]
		[Address(RVA = "0x1011693CC", Offset = "0x11693CC", VA = "0x1011693CC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002835 RID: 10293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FF")]
		[Address(RVA = "0x101169518", Offset = "0x1169518", VA = "0x101169518")]
		public CActScriptKeyBaseTrsNonRev()
		{
		}

		// Token: 0x0400388D RID: 14477
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028E7")]
		public string m_TargetName;

		// Token: 0x0400388E RID: 14478
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028E8")]
		public uint m_CRCKey;

		// Token: 0x0400388F RID: 14479
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028E9")]
		public Vector3 m_Pos;

		// Token: 0x04003890 RID: 14480
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40028EA")]
		public float m_Rot;
	}
}
