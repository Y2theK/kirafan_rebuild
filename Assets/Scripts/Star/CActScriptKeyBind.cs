﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000952 RID: 2386
	[Token(Token = "0x20006BD")]
	[StructLayout(3)]
	public class CActScriptKeyBind : IRoomScriptData
	{
		// Token: 0x06002804 RID: 10244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CE")]
		[Address(RVA = "0x101169580", Offset = "0x1169580", VA = "0x101169580", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002805 RID: 10245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CF")]
		[Address(RVA = "0x1011696E0", Offset = "0x11696E0", VA = "0x1011696E0")]
		public CActScriptKeyBind()
		{
		}

		// Token: 0x04003832 RID: 14386
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028A4")]
		public string m_BindHrcName;

		// Token: 0x04003833 RID: 14387
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028A5")]
		public string m_TargetName;

		// Token: 0x04003834 RID: 14388
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028A6")]
		public CActScriptKeyBind.eNameAttach m_Attach;

		// Token: 0x04003835 RID: 14389
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40028A7")]
		public uint m_CRCKey;

		// Token: 0x04003836 RID: 14390
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40028A8")]
		public float m_LinkTime;

		// Token: 0x04003837 RID: 14391
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40028A9")]
		public CActScriptKeyBind.eCalc m_CalcType;

		// Token: 0x02000953 RID: 2387
		[Token(Token = "0x2000F65")]
		public enum eNameAttach
		{
			// Token: 0x04003839 RID: 14393
			[Token(Token = "0x4006302")]
			Non,
			// Token: 0x0400383A RID: 14394
			[Token(Token = "0x4006303")]
			R,
			// Token: 0x0400383B RID: 14395
			[Token(Token = "0x4006304")]
			L
		}

		// Token: 0x02000954 RID: 2388
		[Token(Token = "0x2000F66")]
		public enum eCalc
		{
			// Token: 0x0400383D RID: 14397
			[Token(Token = "0x4006306")]
			Chara,
			// Token: 0x0400383E RID: 14398
			[Token(Token = "0x4006307")]
			Non,
			// Token: 0x0400383F RID: 14399
			[Token(Token = "0x4006308")]
			LinkOffset,
			// Token: 0x04003840 RID: 14400
			[Token(Token = "0x4006309")]
			ChrTrs,
			// Token: 0x04003841 RID: 14401
			[Token(Token = "0x400630A")]
			LinkOffTrs,
			// Token: 0x04003842 RID: 14402
			[Token(Token = "0x400630B")]
			ChrTrsNonReverse,
			// Token: 0x04003843 RID: 14403
			[Token(Token = "0x400630C")]
			CharaNonReverse
		}
	}
}
