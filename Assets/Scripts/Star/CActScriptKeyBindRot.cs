﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000963 RID: 2403
	[Token(Token = "0x20006CA")]
	[StructLayout(3)]
	public class CActScriptKeyBindRot : IRoomScriptData
	{
		// Token: 0x0600281E RID: 10270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E8")]
		[Address(RVA = "0x101169748", Offset = "0x1169748", VA = "0x101169748", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600281F RID: 10271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E9")]
		[Address(RVA = "0x101169884", Offset = "0x1169884", VA = "0x101169884")]
		public CActScriptKeyBindRot()
		{
		}

		// Token: 0x04003873 RID: 14451
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028CD")]
		public string m_TargetName;

		// Token: 0x04003874 RID: 14452
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028CE")]
		public uint m_CRCKey;

		// Token: 0x04003875 RID: 14453
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028CF")]
		public bool m_OnOff;
	}
}
