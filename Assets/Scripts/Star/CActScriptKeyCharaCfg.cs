﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000967 RID: 2407
	[Token(Token = "0x20006CE")]
	[StructLayout(3)]
	public class CActScriptKeyCharaCfg : IRoomScriptData
	{
		// Token: 0x06002826 RID: 10278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F0")]
		[Address(RVA = "0x1011698EC", Offset = "0x11698EC", VA = "0x1011698EC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002827 RID: 10279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F1")]
		[Address(RVA = "0x1011699FC", Offset = "0x11699FC", VA = "0x1011699FC")]
		public CActScriptKeyCharaCfg()
		{
		}

		// Token: 0x0400387E RID: 14462
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028D8")]
		public int m_OffsetLayer;

		// Token: 0x0400387F RID: 14463
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028D9")]
		public float m_OffsetZ;
	}
}
