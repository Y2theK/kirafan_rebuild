﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000965 RID: 2405
	[Token(Token = "0x20006CC")]
	[StructLayout(3)]
	public class CActScriptKeyCharaViewMode : IRoomScriptData
	{
		// Token: 0x06002822 RID: 10274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024EC")]
		[Address(RVA = "0x101169A64", Offset = "0x1169A64", VA = "0x101169A64", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002823 RID: 10275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024ED")]
		[Address(RVA = "0x101169BA0", Offset = "0x1169BA0", VA = "0x101169BA0")]
		public CActScriptKeyCharaViewMode()
		{
		}

		// Token: 0x04003877 RID: 14455
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028D1")]
		public string m_TargetName;

		// Token: 0x04003878 RID: 14456
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028D2")]
		public bool m_View;

		// Token: 0x04003879 RID: 14457
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028D3")]
		public uint m_CRCKey;
	}
}
