﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000968 RID: 2408
	[Token(Token = "0x20006CF")]
	[StructLayout(3)]
	public class CActScriptKeyCueSe : IRoomScriptData
	{
		// Token: 0x06002828 RID: 10280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F2")]
		[Address(RVA = "0x101169C08", Offset = "0x1169C08", VA = "0x101169C08", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002829 RID: 10281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F3")]
		[Address(RVA = "0x101169D18", Offset = "0x1169D18", VA = "0x101169D18")]
		public CActScriptKeyCueSe()
		{
		}

		// Token: 0x04003880 RID: 14464
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028DA")]
		public string m_CueName;

		// Token: 0x04003881 RID: 14465
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028DB")]
		public float m_Volume;
	}
}
