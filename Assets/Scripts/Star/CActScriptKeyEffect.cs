﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200095F RID: 2399
	[Token(Token = "0x20006C6")]
	[StructLayout(3)]
	public class CActScriptKeyEffect : IRoomScriptData
	{
		// Token: 0x06002816 RID: 10262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E0")]
		[Address(RVA = "0x101169D80", Offset = "0x1169D80", VA = "0x101169D80", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002817 RID: 10263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E1")]
		[Address(RVA = "0x101169EA0", Offset = "0x1169EA0", VA = "0x101169EA0")]
		public CActScriptKeyEffect()
		{
		}

		// Token: 0x04003864 RID: 14436
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028BE")]
		public int m_EffectNo;

		// Token: 0x04003865 RID: 14437
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028BF")]
		public Vector3 m_OffsetPos;

		// Token: 0x04003866 RID: 14438
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028C0")]
		public float m_Size;
	}
}
