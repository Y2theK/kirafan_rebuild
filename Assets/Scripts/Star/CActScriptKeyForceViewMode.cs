﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200096C RID: 2412
	[Token(Token = "0x20006D3")]
	[StructLayout(3)]
	public class CActScriptKeyForceViewMode : IRoomScriptData
	{
		// Token: 0x06002830 RID: 10288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FA")]
		[Address(RVA = "0x101169F08", Offset = "0x1169F08", VA = "0x101169F08", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002831 RID: 10289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FB")]
		[Address(RVA = "0x10116A044", Offset = "0x116A044", VA = "0x10116A044")]
		public CActScriptKeyForceViewMode()
		{
		}

		// Token: 0x04003888 RID: 14472
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028E2")]
		public string m_TargetName;

		// Token: 0x04003889 RID: 14473
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028E3")]
		public bool m_View;

		// Token: 0x0400388A RID: 14474
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028E4")]
		public uint m_CRCKey;
	}
}
