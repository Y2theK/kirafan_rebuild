﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000964 RID: 2404
	[Token(Token = "0x20006CB")]
	[StructLayout(3)]
	public class CActScriptKeyFrameChg : IRoomScriptData
	{
		// Token: 0x06002820 RID: 10272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024EA")]
		[Address(RVA = "0x10116A0AC", Offset = "0x116A0AC", VA = "0x10116A0AC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002821 RID: 10273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024EB")]
		[Address(RVA = "0x10116A1BC", Offset = "0x116A1BC", VA = "0x10116A1BC")]
		public CActScriptKeyFrameChg()
		{
		}

		// Token: 0x04003876 RID: 14454
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028D0")]
		public float m_Frame;
	}
}
