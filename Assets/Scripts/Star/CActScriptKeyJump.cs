﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200095B RID: 2395
	[Token(Token = "0x20006C3")]
	[StructLayout(3)]
	public class CActScriptKeyJump : IRoomScriptData
	{
		// Token: 0x06002810 RID: 10256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DA")]
		[Address(RVA = "0x10116A224", Offset = "0x116A224", VA = "0x10116A224", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002811 RID: 10257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DB")]
		[Address(RVA = "0x10116A334", Offset = "0x116A334", VA = "0x10116A334")]
		public CActScriptKeyJump()
		{
		}

		// Token: 0x04003858 RID: 14424
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028B7")]
		public int m_Randam;

		// Token: 0x04003859 RID: 14425
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028B8")]
		public int m_Offset;

		// Token: 0x0400385A RID: 14426
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028B9")]
		public bool m_FrameReset;
	}
}
