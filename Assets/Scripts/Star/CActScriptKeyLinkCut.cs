﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000955 RID: 2389
	[Token(Token = "0x20006BE")]
	[StructLayout(3)]
	public class CActScriptKeyLinkCut : IRoomScriptData
	{
		// Token: 0x06002806 RID: 10246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D0")]
		[Address(RVA = "0x10116A39C", Offset = "0x116A39C", VA = "0x10116A39C", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002807 RID: 10247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D1")]
		[Address(RVA = "0x10116A4E8", Offset = "0x116A4E8", VA = "0x10116A4E8")]
		public CActScriptKeyLinkCut()
		{
		}

		// Token: 0x04003844 RID: 14404
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028AA")]
		public string m_BindHrcName;

		// Token: 0x04003845 RID: 14405
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028AB")]
		public uint m_CRCKey;

		// Token: 0x04003846 RID: 14406
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028AC")]
		public float m_LinkTime;
	}
}
