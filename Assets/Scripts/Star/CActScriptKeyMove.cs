﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000957 RID: 2391
	[Token(Token = "0x20006C0")]
	[StructLayout(3)]
	public class CActScriptKeyMove : IRoomScriptData
	{
		// Token: 0x0600280A RID: 10250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D4")]
		[Address(RVA = "0x10116A550", Offset = "0x116A550", VA = "0x10116A550", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600280B RID: 10251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D5")]
		[Address(RVA = "0x10116A684", Offset = "0x116A684", VA = "0x10116A684")]
		public CActScriptKeyMove()
		{
		}

		// Token: 0x0400384A RID: 14410
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028B0")]
		public CActScriptKeyMove.eMoveType m_Func;

		// Token: 0x0400384B RID: 14411
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028B1")]
		public float m_Speed;

		// Token: 0x0400384C RID: 14412
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028B2")]
		public float m_MoveTime;

		// Token: 0x0400384D RID: 14413
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028B3")]
		public Vector3 m_Offset;

		// Token: 0x02000958 RID: 2392
		[Token(Token = "0x2000F67")]
		public enum eMoveType
		{
			// Token: 0x0400384F RID: 14415
			[Token(Token = "0x400630E")]
			TargetTime,
			// Token: 0x04003850 RID: 14416
			[Token(Token = "0x400630F")]
			TargetSpeed,
			// Token: 0x04003851 RID: 14417
			[Token(Token = "0x4006310")]
			BaseTime,
			// Token: 0x04003852 RID: 14418
			[Token(Token = "0x4006311")]
			BaseSpeed,
			// Token: 0x04003853 RID: 14419
			[Token(Token = "0x4006312")]
			BasePos,
			// Token: 0x04003854 RID: 14420
			[Token(Token = "0x4006313")]
			LocTarget
		}
	}
}
