﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000969 RID: 2409
	[Token(Token = "0x20006D0")]
	[StructLayout(3)]
	public class CActScriptKeyObjCfg : IRoomScriptData
	{
		// Token: 0x0600282A RID: 10282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F4")]
		[Address(RVA = "0x10116A6EC", Offset = "0x116A6EC", VA = "0x10116A6EC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600282B RID: 10283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F5")]
		[Address(RVA = "0x10116A874", Offset = "0x116A874", VA = "0x10116A874")]
		public CActScriptKeyObjCfg()
		{
		}

		// Token: 0x04003882 RID: 14466
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028DC")]
		public int m_OffsetLayer;

		// Token: 0x04003883 RID: 14467
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028DD")]
		public float m_OffsetZ;

		// Token: 0x04003884 RID: 14468
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028DE")]
		public string m_TargetName;

		// Token: 0x04003885 RID: 14469
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028DF")]
		public uint m_CRCKey;
	}
}
