﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200096A RID: 2410
	[Token(Token = "0x20006D1")]
	[StructLayout(3)]
	public class CActScriptKeyObjLinkActive : IRoomScriptData
	{
		// Token: 0x0600282C RID: 10284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F6")]
		[Address(RVA = "0x10116A8DC", Offset = "0x116A8DC", VA = "0x10116A8DC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600282D RID: 10285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F7")]
		[Address(RVA = "0x10116A9E0", Offset = "0x116A9E0", VA = "0x10116A9E0")]
		public CActScriptKeyObjLinkActive()
		{
		}

		// Token: 0x04003886 RID: 14470
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028E0")]
		public bool m_active;
	}
}
