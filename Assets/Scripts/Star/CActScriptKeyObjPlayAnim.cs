﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000966 RID: 2406
	[Token(Token = "0x20006CD")]
	[StructLayout(3)]
	public class CActScriptKeyObjPlayAnim : IRoomScriptData
	{
		// Token: 0x06002824 RID: 10276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024EE")]
		[Address(RVA = "0x10116AA48", Offset = "0x116AA48", VA = "0x10116AA48", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002825 RID: 10277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024EF")]
		[Address(RVA = "0x10116AB70", Offset = "0x116AB70", VA = "0x10116AB70")]
		public CActScriptKeyObjPlayAnim()
		{
		}

		// Token: 0x0400387A RID: 14458
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028D4")]
		public int m_PlayAnimNo;

		// Token: 0x0400387B RID: 14459
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028D5")]
		public int m_AnimeKey;

		// Token: 0x0400387C RID: 14460
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028D6")]
		public string m_AnimeName;

		// Token: 0x0400387D RID: 14461
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028D7")]
		public WrapMode m_Loop;
	}
}
