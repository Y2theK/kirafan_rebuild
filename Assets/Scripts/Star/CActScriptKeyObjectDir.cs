﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200095D RID: 2397
	[Token(Token = "0x20006C5")]
	[StructLayout(3)]
	public class CActScriptKeyObjectDir : IRoomScriptData
	{
		// Token: 0x06002814 RID: 10260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DE")]
		[Address(RVA = "0x10116ABD8", Offset = "0x116ABD8", VA = "0x10116ABD8", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002815 RID: 10261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DF")]
		[Address(RVA = "0x10116ACDC", Offset = "0x116ACDC", VA = "0x10116ACDC")]
		public CActScriptKeyObjectDir()
		{
		}

		// Token: 0x0400385E RID: 14430
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028BD")]
		public CActScriptKeyObjectDir.eDirFunc m_Func;

		// Token: 0x0200095E RID: 2398
		[Token(Token = "0x2000F68")]
		public enum eDirFunc
		{
			// Token: 0x04003860 RID: 14432
			[Token(Token = "0x4006315")]
			TargetDir,
			// Token: 0x04003861 RID: 14433
			[Token(Token = "0x4006316")]
			TargetRevDir,
			// Token: 0x04003862 RID: 14434
			[Token(Token = "0x4006317")]
			Left,
			// Token: 0x04003863 RID: 14435
			[Token(Token = "0x4006318")]
			Right
		}
	}
}
