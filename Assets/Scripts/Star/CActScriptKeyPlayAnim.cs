﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200094D RID: 2381
	[Token(Token = "0x20006BA")]
	[StructLayout(3)]
	public class CActScriptKeyPlayAnim : IRoomScriptData
	{
		// Token: 0x060027FE RID: 10238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C8")]
		[Address(RVA = "0x10116AD44", Offset = "0x116AD44", VA = "0x10116AD44", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x060027FF RID: 10239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C9")]
		[Address(RVA = "0x10116AE6C", Offset = "0x116AE6C", VA = "0x10116AE6C")]
		public CActScriptKeyPlayAnim()
		{
		}

		// Token: 0x04003820 RID: 14368
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400289D")]
		public int m_PlayAnimNo;

		// Token: 0x04003821 RID: 14369
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400289E")]
		public int m_SequenceNo;

		// Token: 0x04003822 RID: 14370
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400289F")]
		public uint m_Category;

		// Token: 0x04003823 RID: 14371
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028A0")]
		public WrapMode m_Loop;

		// Token: 0x0200094E RID: 2382
		[Token(Token = "0x2000F63")]
		public enum eAnimType
		{
			// Token: 0x04003825 RID: 14373
			[Token(Token = "0x40062F7")]
			Idle,
			// Token: 0x04003826 RID: 14374
			[Token(Token = "0x40062F8")]
			Walk,
			// Token: 0x04003827 RID: 14375
			[Token(Token = "0x40062F9")]
			Enjoy,
			// Token: 0x04003828 RID: 14376
			[Token(Token = "0x40062FA")]
			Rejoice,
			// Token: 0x04003829 RID: 14377
			[Token(Token = "0x40062FB")]
			Lift,
			// Token: 0x0400382A RID: 14378
			[Token(Token = "0x40062FC")]
			Event
		}
	}
}
