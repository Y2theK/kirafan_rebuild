﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000959 RID: 2393
	[Token(Token = "0x20006C1")]
	[StructLayout(3)]
	public class CActScriptKeyPopUp : IRoomScriptData
	{
		// Token: 0x0600280C RID: 10252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D6")]
		[Address(RVA = "0x10116AED4", Offset = "0x116AED4", VA = "0x10116AED4", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600280D RID: 10253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D7")]
		[Address(RVA = "0x10116AFF0", Offset = "0x116AFF0", VA = "0x10116AFF0")]
		public CActScriptKeyPopUp()
		{
		}

		// Token: 0x04003855 RID: 14421
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028B4")]
		public int m_PopUpID;

		// Token: 0x04003856 RID: 14422
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028B5")]
		public float m_PopUpTime;
	}
}
