﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000961 RID: 2401
	[Token(Token = "0x20006C8")]
	[StructLayout(3)]
	public class CActScriptKeyPosAnime : IRoomScriptData
	{
		// Token: 0x0600281A RID: 10266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E4")]
		[Address(RVA = "0x10116B058", Offset = "0x116B058", VA = "0x10116B058", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600281B RID: 10267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E5")]
		[Address(RVA = "0x10116B1B0", Offset = "0x116B1B0", VA = "0x10116B1B0")]
		public CActScriptKeyPosAnime()
		{
		}

		// Token: 0x0400386B RID: 14443
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028C5")]
		public string m_TargetName;

		// Token: 0x0400386C RID: 14444
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028C6")]
		public uint m_CRCKey;

		// Token: 0x0400386D RID: 14445
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028C7")]
		public Vector3 m_Pos;

		// Token: 0x0400386E RID: 14446
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40028C8")]
		public float m_Frame;
	}
}
