﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200095C RID: 2396
	[Token(Token = "0x20006C4")]
	[StructLayout(3)]
	public class CActScriptKeyProgramEvent : IRoomScriptData
	{
		// Token: 0x06002812 RID: 10258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DC")]
		[Address(RVA = "0x10116B218", Offset = "0x116B218", VA = "0x10116B218", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002813 RID: 10259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024DD")]
		[Address(RVA = "0x10116B334", Offset = "0x116B334", VA = "0x10116B334")]
		public CActScriptKeyProgramEvent()
		{
		}

		// Token: 0x0400385B RID: 14427
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028BA")]
		public int m_EventID;

		// Token: 0x0400385C RID: 14428
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028BB")]
		public int m_OptionKey;

		// Token: 0x0400385D RID: 14429
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028BC")]
		public float m_EvtTime;
	}
}
