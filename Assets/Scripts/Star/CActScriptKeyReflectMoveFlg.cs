﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200096B RID: 2411
	[Token(Token = "0x20006D2")]
	[StructLayout(3)]
	public class CActScriptKeyReflectMoveFlg : IRoomScriptData
	{
		// Token: 0x0600282E RID: 10286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F8")]
		[Address(RVA = "0x10116B39C", Offset = "0x116B39C", VA = "0x10116B39C", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600282F RID: 10287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024F9")]
		[Address(RVA = "0x10116B4A0", Offset = "0x116B4A0", VA = "0x10116B4A0")]
		public CActScriptKeyReflectMoveFlg()
		{
		}

		// Token: 0x04003887 RID: 14471
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028E1")]
		public bool m_active;
	}
}
