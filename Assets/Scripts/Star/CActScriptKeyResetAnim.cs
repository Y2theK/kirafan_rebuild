﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200096F RID: 2415
	[Token(Token = "0x20006D6")]
	[StructLayout(3)]
	public class CActScriptKeyResetAnim : IRoomScriptData
	{
		// Token: 0x06002836 RID: 10294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002500")]
		[Address(RVA = "0x10116B508", Offset = "0x116B508", VA = "0x10116B508", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002837 RID: 10295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002501")]
		[Address(RVA = "0x10116B610", Offset = "0x116B610", VA = "0x10116B610")]
		public CActScriptKeyResetAnim()
		{
		}

		// Token: 0x04003891 RID: 14481
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028EB")]
		public int m_PlayAnimNo;

		// Token: 0x04003892 RID: 14482
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028EC")]
		public int m_AnmType;
	}
}
