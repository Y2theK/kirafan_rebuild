﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200096D RID: 2413
	[Token(Token = "0x20006D4")]
	[StructLayout(3)]
	public class CActScriptKeyResetBaseRot : IRoomScriptData
	{
		// Token: 0x06002832 RID: 10290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FC")]
		[Address(RVA = "0x10116B678", Offset = "0x116B678", VA = "0x10116B678", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002833 RID: 10291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024FD")]
		[Address(RVA = "0x10116B7A8", Offset = "0x116B7A8", VA = "0x10116B7A8")]
		public CActScriptKeyResetBaseRot()
		{
		}

		// Token: 0x0400388B RID: 14475
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028E5")]
		public string m_TargetName;

		// Token: 0x0400388C RID: 14476
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028E6")]
		public uint m_CRCKey;
	}
}
