﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000970 RID: 2416
	[Token(Token = "0x20006D7")]
	[StructLayout(3)]
	public class CActScriptKeyResetObjAnime : IRoomScriptData
	{
		// Token: 0x06002838 RID: 10296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002502")]
		[Address(RVA = "0x10116B810", Offset = "0x116B810", VA = "0x10116B810", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002839 RID: 10297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002503")]
		[Address(RVA = "0x10116B920", Offset = "0x116B920", VA = "0x10116B920")]
		public CActScriptKeyResetObjAnime()
		{
		}

		// Token: 0x04003893 RID: 14483
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028ED")]
		public int m_PlayAnimNo;

		// Token: 0x04003894 RID: 14484
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028EE")]
		public string m_AnmName;
	}
}
