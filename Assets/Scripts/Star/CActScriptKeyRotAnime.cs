﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000962 RID: 2402
	[Token(Token = "0x20006C9")]
	[StructLayout(3)]
	public class CActScriptKeyRotAnime : IRoomScriptData
	{
		// Token: 0x0600281C RID: 10268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E6")]
		[Address(RVA = "0x10116B988", Offset = "0x116B988", VA = "0x10116B988", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600281D RID: 10269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024E7")]
		[Address(RVA = "0x10116BAD8", Offset = "0x116BAD8", VA = "0x10116BAD8")]
		public CActScriptKeyRotAnime()
		{
		}

		// Token: 0x0400386F RID: 14447
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028C9")]
		public string m_TargetName;

		// Token: 0x04003870 RID: 14448
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028CA")]
		public uint m_CRCKey;

		// Token: 0x04003871 RID: 14449
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028CB")]
		public float m_Rot;

		// Token: 0x04003872 RID: 14450
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028CC")]
		public float m_Frame;
	}
}
