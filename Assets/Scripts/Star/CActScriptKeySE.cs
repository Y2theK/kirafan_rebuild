﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200095A RID: 2394
	[Token(Token = "0x20006C2")]
	[StructLayout(3)]
	public class CActScriptKeySE : IRoomScriptData
	{
		// Token: 0x0600280E RID: 10254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D8")]
		[Address(RVA = "0x10116BB40", Offset = "0x116BB40", VA = "0x10116BB40", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x0600280F RID: 10255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D9")]
		[Address(RVA = "0x10116BC44", Offset = "0x116BC44", VA = "0x10116BC44")]
		public CActScriptKeySE()
		{
		}

		// Token: 0x04003857 RID: 14423
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028B6")]
		public int m_SE;
	}
}
