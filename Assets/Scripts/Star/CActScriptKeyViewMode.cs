﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000956 RID: 2390
	[Token(Token = "0x20006BF")]
	[StructLayout(3)]
	public class CActScriptKeyViewMode : IRoomScriptData
	{
		// Token: 0x06002808 RID: 10248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D2")]
		[Address(RVA = "0x10116BCAC", Offset = "0x116BCAC", VA = "0x10116BCAC", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002809 RID: 10249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024D3")]
		[Address(RVA = "0x10116BDE8", Offset = "0x116BDE8", VA = "0x10116BDE8")]
		public CActScriptKeyViewMode()
		{
		}

		// Token: 0x04003847 RID: 14407
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028AD")]
		public string m_TargetName;

		// Token: 0x04003848 RID: 14408
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028AE")]
		public bool m_View;

		// Token: 0x04003849 RID: 14409
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40028AF")]
		public uint m_CRCKey;
	}
}
