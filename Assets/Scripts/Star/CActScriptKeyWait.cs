﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200094F RID: 2383
	[Token(Token = "0x20006BB")]
	[StructLayout(3)]
	public class CActScriptKeyWait : IRoomScriptData
	{
		// Token: 0x06002800 RID: 10240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CA")]
		[Address(RVA = "0x10116BE50", Offset = "0x116BE50", VA = "0x10116BE50", Slot = "4")]
		public override void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x06002801 RID: 10241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024CB")]
		[Address(RVA = "0x10116BF58", Offset = "0x116BF58", VA = "0x10116BF58")]
		public CActScriptKeyWait()
		{
		}

		// Token: 0x0400382B RID: 14379
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028A1")]
		public int m_DummyNo;

		// Token: 0x0400382C RID: 14380
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028A2")]
		public CActScriptKeyWait.eWait m_WaitType;

		// Token: 0x02000950 RID: 2384
		[Token(Token = "0x2000F64")]
		public enum eWait
		{
			// Token: 0x0400382E RID: 14382
			[Token(Token = "0x40062FE")]
			Non,
			// Token: 0x0400382F RID: 14383
			[Token(Token = "0x40062FF")]
			Move,
			// Token: 0x04003830 RID: 14384
			[Token(Token = "0x4006300")]
			Popup
		}
	}
}
