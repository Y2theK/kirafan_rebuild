﻿using System.Text;

namespace Star {
    public static class CRC32 {
        private const int TABLE_LENGTH = 256;

        private static uint[] crcTable;

        private static void BuildCRC32Table() {
            crcTable = new uint[TABLE_LENGTH];
            for (int i = 0; i < TABLE_LENGTH; i++) {
                uint val = (uint)i;
                for (int j = 0; j < 8; j++) {
                    val = ((val & 1) != 0) ? 0xEDB88320 ^ (val >> 1) : (val >> 1);
                }
                crcTable[i] = val;
            }
        }

        public static uint Calc(byte[] buf) {
            if (crcTable == null) {
                BuildCRC32Table();
            }
            uint hash = 0xFFFFFFFF;
            for (int i = 0; i < buf.Length; i++) {
                hash = crcTable[buf[i] ^ hash & 0xFF] ^ (hash >> 8);
            }
            return ~hash;
        }

        public static uint Calc(string fname) {
            return Calc(Encoding.ASCII.GetBytes(fname));
        }
    }
}
