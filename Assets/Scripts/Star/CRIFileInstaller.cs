﻿using Meige;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Star {
    public class CRIFileInstaller {
        private const string PREFS_FORMAT = "cri_{0}";
        private const string SAVE_DIR_NAME = "c.r";

        public static string InstallPath;

        private string m_URL;
        private bool m_IsInstalling;
        private bool m_IsError;
        private int m_FileCount;
        private int m_FileCountMax;
        private uint m_SimultaneouslyNum = 2;
        private Dictionary<string, uint> m_VersionDict;
        private List<string> m_ErrorFileNames = new List<string>();
        private List<string> m_InstallFileNames;
        private List<InstallData> m_InstallingList;
        private List<int> m_RemoveKeys = new List<int>();

        public bool IsInstalling => m_IsInstalling;
        public bool IsError => m_IsError;
        public float FileCountMax => m_FileCountMax;
        public float Ratio => m_FileCountMax > 0 ? FinishedFileCount / m_FileCountMax : 1f;

        public float FinishedFileCount {
            get {
                float total = m_FileCount;
                if (m_InstallingList != null) {
                    for (int i = 0; i < m_InstallingList.Count; i++) {
                        total += m_InstallingList[i].GetProgress();
                    }
                }
                return total;
            }
        }

        public void Update() {
            if (!m_IsInstalling) { return; }

            if (!m_IsError && m_InstallFileNames.Count > 0) {
                m_RemoveKeys.Clear();
                for (int i = 0; i < m_InstallFileNames.Count; i++) {
                    if (m_InstallingList.Count >= m_SimultaneouslyNum) { break; }

                    CriFsInstallRequest request = InstallProcess(m_InstallFileNames[i], out uint idealVer);
                    if (request != null) {
                        InstallData installData = new InstallData();
                        installData.m_FileName = m_InstallFileNames[i];
                        installData.m_IdealVer = idealVer;
                        installData.m_Request = request;
                        m_InstallingList.Add(installData);
                    } else {
                        m_FileCount++;
                    }
                    m_RemoveKeys.Add(i);
                }
                for (int i = m_RemoveKeys.Count - 1; i >= 0; i--) {
                    m_InstallFileNames.RemoveAt(m_RemoveKeys[i]);
                }
            }
            for (int i = m_InstallingList.Count - 1; i >= 0; i--) {
                InstallData data = m_InstallingList[i];
                if (data.m_Request.isDone) {
                    if (string.IsNullOrEmpty(data.m_Request.error)) {
                        SetVersion(data.m_FileName, data.m_IdealVer.ToString());
                        m_FileCount++;
                    } else {
                        m_IsError = true;
                        m_ErrorFileNames.Add(data.m_FileName);
                    }
                    data.m_Request.Dispose();
                    m_InstallingList.RemoveAt(i);
                }
            }
            if (m_IsError) {
                if (m_InstallingList.Count == 0) {
                    m_IsInstalling = false;
                }
            } else if (m_InstallFileNames.Count == 0 && m_InstallingList.Count == 0) {
                m_IsInstalling = false;
            }
        }

        public void Setup(string url) {
            m_URL = url;
            InstallPath = PathUtility.Combine(CriWare.installTargetPath, SAVE_DIR_NAME);
            if (!CriFsWebInstaller.isInitialized) {
                CriFsWebInstaller.ModuleConfig config = CriFsWebInstaller.defaultModuleConfig;
                config.numInstallers = m_SimultaneouslyNum;
                config.inactiveTimeoutSec = 180U;
                CriFsWebInstaller.InitializeModule(config);
            }
            if (!Directory.Exists(InstallPath)) {
                Directory.CreateDirectory(InstallPath);
            }
            m_IsInstalling = false;
            m_IsError = false;
            m_FileCountMax = 0;
            Debug.Log($"CRIFileInstaller.Setup()  >>> URL:{m_URL}, InstallPath:{InstallPath}");
        }

        public void Destroy() {
            if (CriFsWebInstaller.isInitialized) {
                CriFsWebInstaller.FinalizeModule();
            }
        }

        public static int ParseCRIFileVersionDB(CRIFileVersionDB criFileVersionDB, GameDefine.eDLScene dlScene, out Dictionary<string, uint> out_versionDict, out List<string> out_installFileNames, string specificFileNameWithoutExt = null) {
            out_versionDict = new Dictionary<string, uint>();
            out_installFileNames = new List<string>();
            for (int i = 0; i < criFileVersionDB.m_Params.Length; i++) {
                CRIFileVersionDB_Param param = criFileVersionDB.m_Params[i];
                if (!IsSkipDL(dlScene, param.m_DLType)) {
                    if (string.IsNullOrEmpty(specificFileNameWithoutExt) || param.m_FileName == specificFileNameWithoutExt) {
                        List<string> paths = criFileVersionDB.GetPaths(i, out string key, out uint value);
                        if (!paths.Contains(SoundDefine.ACF_FILE_NAME)) {
                            if (!out_versionDict.ContainsKey(key)) {
                                out_versionDict.Add(key, value);
                            }
                            out_installFileNames.AddRange(paths);
                        }
                    }
                }
            }
            return out_installFileNames.Count;
        }

        public static bool IsSkipDL(GameDefine.eDLScene dlScene, int dlType) {
            GameDefine.eDLType type = (GameDefine.eDLType)dlType;
            return (dlScene != GameDefine.eDLScene.NotDLScene && type == GameDefine.eDLType.NotDLInDLScene)
                || (dlScene == GameDefine.eDLScene.DL_First && type != GameDefine.eDLType.FirstAndAlways);
        }

        public void Install(CRIFileVersionDB criFileVersionDB, GameDefine.eDLScene dlScene, string specificFileNameWithoutExt) {
            ParseCRIFileVersionDB(criFileVersionDB, dlScene, out Dictionary<string, uint> versionDict, out List<string> fileNames, specificFileNameWithoutExt);
            Install(versionDict, fileNames);
        }

        public void Install(CRIFileVersionDB criFileVersionDB, GameDefine.eDLScene dlScene) {
            ParseCRIFileVersionDB(criFileVersionDB, dlScene, out Dictionary<string, uint> versionDict, out List<string> fileNames);
            Install(versionDict, fileNames);
        }

        public void Install(Dictionary<string, uint> versionDict, List<string> installFileNames) {
            if (m_IsInstalling) { return; }

            m_IsInstalling = true;
            m_IsError = false;
            if (versionDict != null && installFileNames != null) {
                m_VersionDict = versionDict;
                m_InstallFileNames = installFileNames;
                m_FileCount = 0;
                m_FileCountMax = installFileNames.Count;
                m_InstallingList = new List<InstallData>();
            }
        }

        public void RetryInstall() {
            if (m_IsError) {
                for (int i = 0; i < m_ErrorFileNames.Count; i++) {
                    Debug.Log($"CRIFileInstaller.RetryInstall() >>> [{i}]{m_ErrorFileNames[i]}");
                    m_InstallFileNames.Add(m_ErrorFileNames[i]);
                }
                m_ErrorFileNames.Clear();
                m_IsInstalling = true;
                m_IsError = false;
            }
        }

        public string GetStreamingFullPath(string name) {
            string path = Application.streamingAssetsPath;
#if UNITY_ANDROID
            path = PathUtility.Combine(path, "Android");
#elif UNITY_IOS
            path = PathUtility.Combine(path, "iOS");
#else
            path = PathUtility.Combine(path, "Windows");
#endif
            return PathUtility.Combine(path, "CRI", name);
        }

        public string GetStreamingRelativePath(string name) {
#if UNITY_ANDROID
            return PathUtility.Combine("Android", "CRI", name);
#elif UNITY_IOS
            return PathUtility.Combine("iOS", "CRI", name);
#else
            return PathUtility.Combine("Windows", "CRI", name);
#endif
        }

        private CriFsInstallRequest InstallProcess(string fileName, out uint out_idealVer) {
            out_idealVer = 0U;
            string name = PathUtility.GetFileNameWithoutExtension(fileName);
            if (m_VersionDict.TryGetValue(name, out uint num)) {
                out_idealVer = num;
                if (!IsExist(fileName, num)) {
                    string srcPath = GetSrcPath(fileName);
                    string destPath = GetDestPath(fileName);
                    return CriFsUtility.WebInstall(srcPath, destPath, null);
                }
            }
            return null;
        }

        public void InstallAcf(CRIFileVersionDB criFileVersionDB) {
            string acf = criFileVersionDB.GetAcf(out string key, out uint value);
            if (!string.IsNullOrEmpty(acf)) {
                Install(new Dictionary<string, uint> { { key, value } }, new List<string> { acf });
            }
        }

        public bool CleanCache(string fileName) {
            if (!string.IsNullOrEmpty(fileName)) {
                try {
                    DeleteVersion(fileName);
                    new FileInfo(GetDestPath(fileName)).Delete();
                    return true;
                } catch (Exception e) {
                    Debug.LogException(e);
                }
            }
            return false;
        }

        public bool CleanCache() {
            try {
                DirectoryInfo dirInfo = new DirectoryInfo(InstallPath);
                CleanCacheProcess(dirInfo, string.Empty);
            } catch (Exception) {
                return false;
            }
            return true;
        }

        private void CleanCacheProcess(DirectoryInfo dirInfo, string path = "") {
            foreach (FileInfo fileInfo in dirInfo.GetFiles()) {
                string fileName = PathUtility.Combine(path, fileInfo.Name);
                DeleteVersion(fileName);
                fileInfo.Delete();
            }
            foreach (DirectoryInfo directoryInfo in dirInfo.GetDirectories()) {
                CleanCacheProcess(directoryInfo, PathUtility.Combine(path, directoryInfo.Name));
            }
        }

        public string GetSrcPath(string fileName) {
            return m_URL + fileName;
        }

        public string GetDestPath(string fileName) {
            return PathUtility.Combine(InstallPath, fileName);
        }

        private void SetVersion(string fileName, string version) {
            string key = GetFileVersionKey(fileName);
            PreviewLabs.PlayerPrefs.SetString(key, version);
            PreviewLabs.PlayerPrefs.Flush();
        }

        private void DeleteVersion(string fileName) {
            string key = GetFileVersionKey(fileName);
            PreviewLabs.PlayerPrefs.DeleteKey(key);
            PreviewLabs.PlayerPrefs.Flush();
        }

        public bool IsExist(string fileName) {
            if (!File.Exists(GetStreamingFullPath(fileName))) {
                return File.Exists(GetDestPath(fileName));
            }
            return false;
        }

        public bool IsExist(string fileName, uint version) {
            if (IsStreamAssetsFromToLoadNeeds(fileName, out _)) { return true; }

            string key = GetFileVersionKey(fileName);
            string versionStr = PreviewLabs.PlayerPrefs.GetString(key, "0");
            if (versionStr == version.ToString()) {
                return File.Exists(GetDestPath(fileName));
            }
            return false;
        }

        public bool IsStreamAssetsFromToLoadNeeds(string fileName, out bool existStreamingAssets) {
            string path = GetStreamingFullPath(fileName);
            existStreamingAssets = File.Exists(path);
            if (existStreamingAssets) {
                SoundVersionDictionary.FileInformation file1 = GameSystem.Inst.SoundVersionDictionary.Search(fileName, true);
                if (file1 != null) {
                    SoundVersionDictionary.FileInformation file2 = GameSystem.Inst.SoundVersionDictionary.Search(fileName);
                    if (file2 != null) {
                        return file1.version == file2.version;
                    }
                    return true;
                }
            }
            return false;
        }

        public bool IsExistStreamAssets(string fileName) {
            return File.Exists(GetStreamingFullPath(fileName));
        }

        private string GetFileVersionKey(string fileName) {
            return string.Format(PREFS_FORMAT, fileName);
        }

        public class InstallData {
            public string m_FileName;
            public uint m_IdealVer;
            public CriFsInstallRequest m_Request;

            public float GetProgress() {
                if (m_Request != null && m_Request.progress >= 0f) {
                    return m_Request.progress;
                }
                return 0f;
            }
        }
    }
}
