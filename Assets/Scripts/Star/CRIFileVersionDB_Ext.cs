﻿using System.Collections.Generic;
using System.Text;

namespace Star {
    public static class CRIFileVersionDB_Ext {
        public static List<string> GetPaths(this CRIFileVersionDB self, int index, out string out_fileNameKeyWithoutExt, out uint out_version) {
            StringBuilder sb = new StringBuilder();
            List<string> paths = new List<string>();
            CRIFileVersionDB_Param param = self.m_Params[index];
            if (param.m_IsAcb != 0) {
                sb.Append(param.m_FileName);
                sb.Append(SoundDefine.EXT_ACB);
                paths.Add(sb.ToString());
            }
            if (param.m_IsAwb != 0) {
                sb.Length = 0;
                sb.Append(param.m_FileName);
                sb.Append(SoundDefine.EXT_AWB);
                paths.Add(sb.ToString());
            }
            if (param.m_IsAcf != 0) {
                sb.Length = 0;
                sb.Append(param.m_FileName);
                sb.Append(SoundDefine.EXT_ACF);
                paths.Add(sb.ToString());
            }
            if (param.m_IsUsm != 0) {
                sb.Length = 0;
                sb.Append(param.m_FileName);
                sb.Append(MovieDefine.EXT_USM);
                paths.Add(sb.ToString());
            }
            out_fileNameKeyWithoutExt = param.m_FileName;
            out_version = param.m_Version;
            return paths;
        }

        public static string GetAcf(this CRIFileVersionDB self, out string out_fileNameKeyWithoutExt, out uint out_version) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                CRIFileVersionDB_Param param = self.m_Params[i];
                if (param.m_IsAcf != 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(param.m_FileName);
                    sb.Append(SoundDefine.EXT_ACF);
                    out_fileNameKeyWithoutExt = param.m_FileName;
                    out_version = param.m_Version;
                    return sb.ToString();
                }
            }
            out_fileNameKeyWithoutExt = null;
            out_version = 0U;
            return null;
        }

        public static uint GetVersion(this CRIFileVersionDB self, string fileNameWithoutExt) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                CRIFileVersionDB_Param param = self.m_Params[i];
                if (param.m_FileName == fileNameWithoutExt) {
                    return param.m_Version;
                }
            }
            return 0;
        }
    }
}
