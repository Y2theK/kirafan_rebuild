﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004DC RID: 1244
	[Token(Token = "0x20003D4")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct CRIFileVersionDB_Param
	{
		// Token: 0x04001779 RID: 6009
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400115F")]
		public string m_FileName;

		// Token: 0x0400177A RID: 6010
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001160")]
		public int m_DLType;

		// Token: 0x0400177B RID: 6011
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001161")]
		public uint m_Version;

		// Token: 0x0400177C RID: 6012
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001162")]
		public byte m_IsAcb;

		// Token: 0x0400177D RID: 6013
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4001163")]
		public byte m_IsAwb;

		// Token: 0x0400177E RID: 6014
		[Cpp2IlInjected.FieldOffset(Offset = "0x12")]
		[Token(Token = "0x4001164")]
		public byte m_IsAcf;

		// Token: 0x0400177F RID: 6015
		[Cpp2IlInjected.FieldOffset(Offset = "0x13")]
		[Token(Token = "0x4001165")]
		public byte m_IsUsm;
	}
}
