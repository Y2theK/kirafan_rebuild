﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004B8 RID: 1208
	[Token(Token = "0x20003B0")]
	[StructLayout(3)]
	public class CacheCleanTargetsDB : ScriptableObject
	{
		// Token: 0x060013FC RID: 5116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B1")]
		[Address(RVA = "0x10116E714", Offset = "0x116E714", VA = "0x10116E714")]
		public CacheCleanTargetsDB()
		{
		}

		// Token: 0x040016EF RID: 5871
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010D5")]
		public CacheCleanTargetsDB_Param[] m_Params;
	}
}
