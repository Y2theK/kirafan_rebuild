﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004B7 RID: 1207
	[Token(Token = "0x20003AF")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct CacheCleanTargetsDB_Param
	{
		// Token: 0x040016E8 RID: 5864
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010CE")]
		public int m_ID;

		// Token: 0x040016E9 RID: 5865
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010CF")]
		public int m_Type;

		// Token: 0x040016EA RID: 5866
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010D0")]
		public int m_IsDir;

		// Token: 0x040016EB RID: 5867
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40010D1")]
		public string m_TargetName;

		// Token: 0x040016EC RID: 5868
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010D2")]
		public int m_Version;

		// Token: 0x040016ED RID: 5869
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40010D3")]
		public int m_Platform_android;

		// Token: 0x040016EE RID: 5870
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40010D4")]
		public int m_Platform_ios;
	}
}
