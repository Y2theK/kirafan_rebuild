﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008BB RID: 2235
	[Token(Token = "0x2000665")]
	[StructLayout(3)]
	public static class CacheCleaner
	{
		// Token: 0x06002498 RID: 9368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021BC")]
		[Address(RVA = "0x10116E71C", Offset = "0x116E71C", VA = "0x10116E71C")]
		public static void ClearAll()
		{
		}

		// Token: 0x06002499 RID: 9369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021BD")]
		[Address(RVA = "0x10116E8C8", Offset = "0x116E8C8", VA = "0x10116E8C8")]
		public static void ClearFromTargetsDB()
		{
		}

		// Token: 0x0600249A RID: 9370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021BE")]
		[Address(RVA = "0x10116ED24", Offset = "0x116ED24", VA = "0x10116ED24")]
		private static void ClearFromTargetsDB_AssetBundle(int isDir, string targetName)
		{
		}

		// Token: 0x0600249B RID: 9371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021BF")]
		[Address(RVA = "0x10116EE70", Offset = "0x116EE70", VA = "0x10116EE70")]
		private static void ClearFromTargetsDB_CRI(string targetName)
		{
		}
	}
}
