﻿using System;
using UnityEngine;

namespace Star {
    [Serializable]
    public class CacheObjectData {
        [SerializeField] private GameObject m_Prefab;
        [SerializeField] private int m_CacheSize = 1;

        private GameObject[] m_Objects;
        private int m_CacheIndex;

        public event Action<GameObject> OnFindActiveObject;
        public event Action<GameObject> OnInstantiateObject;

        public GameObject Prefab => m_Prefab;
        public int CacheSize => m_CacheSize;
        public GameObject[] CacheObjects => m_Objects;

        public void Initialize(GameObject prefab, int cacheSize, Transform parent) {
            m_Prefab = prefab;
            m_CacheSize = cacheSize;
            Initialize(parent);
        }

        public void Initialize(Transform parent) {
            m_Objects = new GameObject[m_CacheSize];
            for (int i = 0; i < m_CacheSize; i++) {
                m_Objects[i] = UnityEngine.Object.Instantiate(m_Prefab, parent, false);
                m_Objects[i].SetActive(false);
                m_Objects[i].name = m_Objects[i].name + i;
                if (OnInstantiateObject != null) {
                    OnInstantiateObject(m_Objects[i]);
                }
            }
        }

        public void Destroy() {
            if (m_Objects != null) {
                for (int i = 0; i < m_Objects.Length; i++) {
                    UnityEngine.Object.Destroy(m_Objects[i]);
                    m_Objects[i] = null;
                }
                m_Objects = null;
            }
            m_Prefab = null;
            m_CacheSize = 0;
            m_CacheIndex = 0;
        }

        public GameObject GetNextObjectInCache() {
            GameObject obj = null;
            for (int i = 0; i < m_CacheSize; i++) {
                obj = m_Objects[i];
                if (obj != null && !obj.activeSelf) {
                    break;
                }
                m_CacheIndex = (m_CacheIndex + 1) % m_CacheSize;
            }
            if (obj != null && obj.activeSelf && OnFindActiveObject != null) {
                OnFindActiveObject(obj);
            }
            m_CacheIndex = (m_CacheIndex + 1) % m_CacheSize;
            return obj;
        }
    }
}
