﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004BB RID: 1211
	[Token(Token = "0x20003B3")]
	[StructLayout(3)]
	public class ChapterUISettingDB : ScriptableObject
	{
		// Token: 0x060013FD RID: 5117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B2")]
		[Address(RVA = "0x10116F9D4", Offset = "0x116F9D4", VA = "0x10116F9D4")]
		public ChapterUISettingDB()
		{
		}

		// Token: 0x040016F8 RID: 5880
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010DE")]
		public ChapterUISettingDB_Param[] m_Params;
	}
}
