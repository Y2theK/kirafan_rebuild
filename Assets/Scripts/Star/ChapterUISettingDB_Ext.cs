﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004FC RID: 1276
	[Token(Token = "0x20003F2")]
	[StructLayout(3)]
	public static class ChapterUISettingDB_Ext
	{
		// Token: 0x0600151A RID: 5402 RVA: 0x00008D60 File Offset: 0x00006F60
		[Token(Token = "0x60013CF")]
		[Address(RVA = "0x10116F9DC", Offset = "0x116F9DC", VA = "0x10116F9DC")]
		public static ChapterUISettingDB_Param? GetParam(this ChapterUISettingDB self, int chapterId)
		{
			return null;
		}

		// Token: 0x0600151B RID: 5403 RVA: 0x00008D78 File Offset: 0x00006F78
		[Token(Token = "0x60013D0")]
		[Address(RVA = "0x10116FB48", Offset = "0x116FB48", VA = "0x10116FB48")]
		public static bool TryGetVoice(this ChapterUISettingDB self, int chapterId, out string out_cueSheet, out string out_cueName)
		{
			return default(bool);
		}
	}
}
