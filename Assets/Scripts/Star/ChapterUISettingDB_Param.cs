﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004BA RID: 1210
	[Token(Token = "0x20003B2")]
	[Serializable]
	[StructLayout(0)]
	public struct ChapterUISettingDB_Param
	{
		// Token: 0x040016F3 RID: 5875
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010D9")]
		public int m_ChapterID;

		// Token: 0x040016F4 RID: 5876
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010DA")]
		public string m_BackGroundName;

		// Token: 0x040016F5 RID: 5877
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40010DB")]
		public int m_IsDisplayKirara;

		// Token: 0x040016F6 RID: 5878
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010DC")]
		public string[] m_CueSheetsOrCharaIDs;

		// Token: 0x040016F7 RID: 5879
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40010DD")]
		public string[] m_CueNames;
	}
}
