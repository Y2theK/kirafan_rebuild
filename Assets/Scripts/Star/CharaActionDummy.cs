﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200093A RID: 2362
	[Token(Token = "0x20006AC")]
	[StructLayout(3)]
	public class CharaActionDummy : ICharaRoomAction
	{
		// Token: 0x060027C3 RID: 10179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600248D")]
		[Address(RVA = "0x10116FF0C", Offset = "0x116FF0C", VA = "0x10116FF0C", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027C4 RID: 10180 RVA: 0x00010F08 File Offset: 0x0000F108
		[Token(Token = "0x600248E")]
		[Address(RVA = "0x10116FF10", Offset = "0x116FF10", VA = "0x10116FF10", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027C5 RID: 10181 RVA: 0x00010F20 File Offset: 0x0000F120
		[Token(Token = "0x600248F")]
		[Address(RVA = "0x10116FF18", Offset = "0x116FF18", VA = "0x10116FF18", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027C6 RID: 10182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002490")]
		[Address(RVA = "0x10116FF20", Offset = "0x116FF20", VA = "0x10116FF20", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027C7 RID: 10183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002491")]
		[Address(RVA = "0x10116FF24", Offset = "0x116FF24", VA = "0x10116FF24")]
		public CharaActionDummy()
		{
		}
	}
}
