﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200093B RID: 2363
	[Token(Token = "0x20006AD")]
	[StructLayout(3)]
	public class CharaActionLift : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x060027C8 RID: 10184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002492")]
		[Address(RVA = "0x10116FF2C", Offset = "0x116FF2C", VA = "0x10116FF2C", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027C9 RID: 10185 RVA: 0x00010F38 File Offset: 0x0000F138
		[Token(Token = "0x6002493")]
		[Address(RVA = "0x101170018", Offset = "0x1170018", VA = "0x101170018", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027CA RID: 10186 RVA: 0x00010F50 File Offset: 0x0000F150
		[Token(Token = "0x6002494")]
		[Address(RVA = "0x1011700A0", Offset = "0x11700A0", VA = "0x1011700A0")]
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return default(bool);
		}

		// Token: 0x060027CB RID: 10187 RVA: 0x00010F68 File Offset: 0x0000F168
		[Token(Token = "0x6002495")]
		[Address(RVA = "0x1011700A8", Offset = "0x11700A8", VA = "0x1011700A8", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027CC RID: 10188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002496")]
		[Address(RVA = "0x101170104", Offset = "0x1170104", VA = "0x101170104", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027CD RID: 10189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002497")]
		[Address(RVA = "0x10117012C", Offset = "0x117012C", VA = "0x10117012C")]
		public CharaActionLift()
		{
		}
	}
}
