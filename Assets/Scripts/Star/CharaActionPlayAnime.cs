﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200093C RID: 2364
	[Token(Token = "0x20006AE")]
	[StructLayout(3)]
	public class CharaActionPlayAnime : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x060027CE RID: 10190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002498")]
		[Address(RVA = "0x101170134", Offset = "0x1170134", VA = "0x101170134", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027CF RID: 10191 RVA: 0x00010F80 File Offset: 0x0000F180
		[Token(Token = "0x6002499")]
		[Address(RVA = "0x1011702F4", Offset = "0x11702F4", VA = "0x1011702F4", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027D0 RID: 10192 RVA: 0x00010F98 File Offset: 0x0000F198
		[Token(Token = "0x600249A")]
		[Address(RVA = "0x1011703A8", Offset = "0x11703A8", VA = "0x1011703A8")]
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return default(bool);
		}

		// Token: 0x060027D1 RID: 10193 RVA: 0x00010FB0 File Offset: 0x0000F1B0
		[Token(Token = "0x600249B")]
		[Address(RVA = "0x1011706F8", Offset = "0x11706F8", VA = "0x1011706F8", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027D2 RID: 10194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600249C")]
		[Address(RVA = "0x1011707B0", Offset = "0x11707B0", VA = "0x1011707B0", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027D3 RID: 10195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600249D")]
		[Address(RVA = "0x1011707DC", Offset = "0x11707DC", VA = "0x1011707DC")]
		public CharaActionPlayAnime()
		{
		}

		// Token: 0x040037BA RID: 14266
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002857")]
		private Transform m_TargetTrs;

		// Token: 0x040037BB RID: 14267
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002858")]
		private bool m_MarkPoint;

		// Token: 0x040037BC RID: 14268
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x4002859")]
		private Vector3 m_BackTagMark;
	}
}
