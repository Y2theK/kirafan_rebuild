﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200093D RID: 2365
	[Token(Token = "0x20006AF")]
	[StructLayout(3)]
	public class CharaActionSit : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x060027D4 RID: 10196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600249E")]
		[Address(RVA = "0x1011707E4", Offset = "0x11707E4", VA = "0x1011707E4", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027D5 RID: 10197 RVA: 0x00010FC8 File Offset: 0x0000F1C8
		[Token(Token = "0x600249F")]
		[Address(RVA = "0x101170938", Offset = "0x1170938", VA = "0x101170938", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027D6 RID: 10198 RVA: 0x00010FE0 File Offset: 0x0000F1E0
		[Token(Token = "0x60024A0")]
		[Address(RVA = "0x1011709C0", Offset = "0x11709C0", VA = "0x1011709C0", Slot = "8")]
		protected virtual bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return default(bool);
		}

		// Token: 0x060027D7 RID: 10199 RVA: 0x00010FF8 File Offset: 0x0000F1F8
		[Token(Token = "0x60024A1")]
		[Address(RVA = "0x101170CD4", Offset = "0x1170CD4", VA = "0x101170CD4", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027D8 RID: 10200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024A2")]
		[Address(RVA = "0x101170D48", Offset = "0x1170D48", VA = "0x101170D48", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027D9 RID: 10201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024A3")]
		[Address(RVA = "0x101170D74", Offset = "0x1170D74", VA = "0x101170D74")]
		public CharaActionSit()
		{
		}

		// Token: 0x040037BD RID: 14269
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400285A")]
		protected Transform m_TargetTrs;
	}
}
