﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200093F RID: 2367
	[Token(Token = "0x20006B1")]
	[StructLayout(3)]
	public class CharaActionSitBook : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x060027DC RID: 10204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024A6")]
		[Address(RVA = "0x101170D7C", Offset = "0x1170D7C", VA = "0x101170D7C", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027DD RID: 10205 RVA: 0x00011028 File Offset: 0x0000F228
		[Token(Token = "0x60024A7")]
		[Address(RVA = "0x101170ED4", Offset = "0x1170ED4", VA = "0x101170ED4", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027DE RID: 10206 RVA: 0x00011040 File Offset: 0x0000F240
		[Token(Token = "0x60024A8")]
		[Address(RVA = "0x101170F68", Offset = "0x1170F68", VA = "0x101170F68")]
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return default(bool);
		}

		// Token: 0x060027DF RID: 10207 RVA: 0x00011058 File Offset: 0x0000F258
		[Token(Token = "0x60024A9")]
		[Address(RVA = "0x1011712DC", Offset = "0x11712DC", VA = "0x1011712DC", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027E0 RID: 10208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024AA")]
		[Address(RVA = "0x101171350", Offset = "0x1171350", VA = "0x101171350", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027E1 RID: 10209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024AB")]
		[Address(RVA = "0x10117137C", Offset = "0x117137C", VA = "0x10117137C")]
		public CharaActionSitBook()
		{
		}

		// Token: 0x040037BE RID: 14270
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400285B")]
		private Transform m_TargetTrs;

		// Token: 0x040037BF RID: 14271
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400285C")]
		private bool m_MarkPoint;
	}
}
