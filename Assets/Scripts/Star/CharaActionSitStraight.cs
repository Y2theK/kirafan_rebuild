﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200093E RID: 2366
	[Token(Token = "0x20006B0")]
	[StructLayout(3)]
	public class CharaActionSitStraight : CharaActionSit
	{
		// Token: 0x060027DA RID: 10202 RVA: 0x00011010 File Offset: 0x0000F210
		[Token(Token = "0x60024A4")]
		[Address(RVA = "0x101171384", Offset = "0x1171384", VA = "0x101171384", Slot = "8")]
		protected override bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return default(bool);
		}

		// Token: 0x060027DB RID: 10203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024A5")]
		[Address(RVA = "0x10117165C", Offset = "0x117165C", VA = "0x10117165C")]
		public CharaActionSitStraight()
		{
		}
	}
}
