﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000941 RID: 2369
	[Token(Token = "0x20006B3")]
	[StructLayout(3)]
	public class CharaActionSleep : ICharaRoomAction
	{
		// Token: 0x060027E2 RID: 10210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024AC")]
		[Address(RVA = "0x101171664", Offset = "0x1171664", VA = "0x101171664", Slot = "4")]
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x060027E3 RID: 10211 RVA: 0x00011070 File Offset: 0x0000F270
		[Token(Token = "0x60024AD")]
		[Address(RVA = "0x101171C28", Offset = "0x1171C28", VA = "0x101171C28", Slot = "5")]
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x060027E4 RID: 10212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024AE")]
		[Address(RVA = "0x1011727F0", Offset = "0x11727F0", VA = "0x1011727F0")]
		private void SetUpHudPopUp()
		{
		}

		// Token: 0x060027E5 RID: 10213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024AF")]
		[Address(RVA = "0x1011719CC", Offset = "0x11719CC", VA = "0x1011719CC")]
		public void ChangeSleepFloor()
		{
		}

		// Token: 0x060027E6 RID: 10214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024B0")]
		[Address(RVA = "0x1011718D4", Offset = "0x11718D4", VA = "0x1011718D4")]
		public void CalcTargetTrs()
		{
		}

		// Token: 0x060027E7 RID: 10215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024B1")]
		[Address(RVA = "0x1011726F8", Offset = "0x11726F8", VA = "0x1011726F8")]
		private void SetBedComforterView(bool fview)
		{
		}

		// Token: 0x060027E8 RID: 10216 RVA: 0x00011088 File Offset: 0x0000F288
		[Token(Token = "0x60024B2")]
		[Address(RVA = "0x101172AA0", Offset = "0x1172AA0", VA = "0x101172AA0", Slot = "6")]
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return default(bool);
		}

		// Token: 0x060027E9 RID: 10217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024B3")]
		[Address(RVA = "0x101172C78", Offset = "0x1172C78", VA = "0x101172C78", Slot = "7")]
		public void Release()
		{
		}

		// Token: 0x060027EA RID: 10218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024B4")]
		[Address(RVA = "0x101172C7C", Offset = "0x1172C7C", VA = "0x101172C7C")]
		public CharaActionSleep()
		{
		}

		// Token: 0x040037C4 RID: 14276
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002861")]
		private CharaActionSleep.eSleepStep m_Step;

		// Token: 0x040037C5 RID: 14277
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002862")]
		private CharacterHandler m_Owner;

		// Token: 0x040037C6 RID: 14278
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002863")]
		private RoomObjectCtrlChara m_Base;

		// Token: 0x040037C7 RID: 14279
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002864")]
		private RoomAnimAccessMap m_AnimeMap;

		// Token: 0x040037C8 RID: 14280
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002865")]
		private RoomPartsActionPakage m_PartsAction;

		// Token: 0x040037C9 RID: 14281
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002866")]
		private RoomBuilderBase m_Builder;

		// Token: 0x040037CA RID: 14282
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002867")]
		private Transform m_BindTrs;

		// Token: 0x040037CB RID: 14283
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002868")]
		private IRoomObjectControll m_Target;

		// Token: 0x040037CC RID: 14284
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002869")]
		private Vector2 m_MainOwnerPos;

		// Token: 0x040037CD RID: 14285
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400286A")]
		private Vector3 m_BackUpPos;

		// Token: 0x040037CE RID: 14286
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400286B")]
		private bool m_EventQue;

		// Token: 0x040037CF RID: 14287
		[Cpp2IlInjected.FieldOffset(Offset = "0x65")]
		[Token(Token = "0x400286C")]
		private bool m_FloorChg;

		// Token: 0x040037D0 RID: 14288
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400286D")]
		private float m_TimeChk;

		// Token: 0x040037D1 RID: 14289
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400286E")]
		private int m_SleepFloor;

		// Token: 0x040037D2 RID: 14290
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400286F")]
		private int m_OverlapMessgeID;

		// Token: 0x02000942 RID: 2370
		[Token(Token = "0x2000F5E")]
		public enum eSleepStep
		{
			// Token: 0x040037D4 RID: 14292
			[Token(Token = "0x40062D7")]
			FloorUp,
			// Token: 0x040037D5 RID: 14293
			[Token(Token = "0x40062D8")]
			Init,
			// Token: 0x040037D6 RID: 14294
			[Token(Token = "0x40062D9")]
			HudAction,
			// Token: 0x040037D7 RID: 14295
			[Token(Token = "0x40062DA")]
			SleepIn,
			// Token: 0x040037D8 RID: 14296
			[Token(Token = "0x40062DB")]
			Sleep,
			// Token: 0x040037D9 RID: 14297
			[Token(Token = "0x40062DC")]
			SleepMoveOut,
			// Token: 0x040037DA RID: 14298
			[Token(Token = "0x40062DD")]
			SleepOut,
			// Token: 0x040037DB RID: 14299
			[Token(Token = "0x40062DE")]
			MainMove,
			// Token: 0x040037DC RID: 14300
			[Token(Token = "0x40062DF")]
			Wake,
			// Token: 0x040037DD RID: 14301
			[Token(Token = "0x40062E0")]
			End
		}
	}
}
