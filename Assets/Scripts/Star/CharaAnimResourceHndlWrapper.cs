﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000481 RID: 1153
	[Token(Token = "0x2000381")]
	[StructLayout(3)]
	public class CharaAnimResourceHndlWrapper
	{
		// Token: 0x06001394 RID: 5012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600124F")]
		[Address(RVA = "0x101172C84", Offset = "0x1172C84", VA = "0x101172C84")]
		public CharaAnimResourceHndlWrapper()
		{
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001250")]
		[Address(RVA = "0x101172C8C", Offset = "0x1172C8C", VA = "0x101172C8C")]
		public CharaAnimResourceHndlWrapper(ABResourceObjectHandler abResObjHndl, string actKey, string path)
		{
		}

		// Token: 0x040015A3 RID: 5539
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FBB")]
		public ABResourceObjectHandler m_ABResObjHndl;

		// Token: 0x040015A4 RID: 5540
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FBC")]
		public List<string> m_ActKeys;

		// Token: 0x040015A5 RID: 5541
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000FBD")]
		public List<string> m_Paths;
	}
}
