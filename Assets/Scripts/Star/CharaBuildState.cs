﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006A2 RID: 1698
	// (Invoke) Token: 0x0600187A RID: 6266
	[Token(Token = "0x200056A")]
	[StructLayout(3, Size = 8)]
	public delegate void CharaBuildState(eCharaBuildUp ftype, long fmanageid, int froomid);
}
