﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BF8 RID: 3064
	[Token(Token = "0x200083F")]
	[StructLayout(3)]
	public class CharaQuestUnlock
	{
		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x060035FA RID: 13818 RVA: 0x00016BA8 File Offset: 0x00014DA8
		[Token(Token = "0x170003AB")]
		public bool IsExistUnlockData
		{
			[Token(Token = "0x6003114")]
			[Address(RVA = "0x10117322C", Offset = "0x117322C", VA = "0x10117322C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x060035FB RID: 13819 RVA: 0x00016BC0 File Offset: 0x00014DC0
		[Token(Token = "0x170003AC")]
		public bool IsGotoQuest
		{
			[Token(Token = "0x6003115")]
			[Address(RVA = "0x101173298", Offset = "0x1173298", VA = "0x101173298")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060035FC RID: 13820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003116")]
		[Address(RVA = "0x1011732A0", Offset = "0x11732A0", VA = "0x1011732A0")]
		public CharaQuestUnlock()
		{
		}

		// Token: 0x060035FD RID: 13821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003117")]
		[Address(RVA = "0x101173318", Offset = "0x1173318", VA = "0x101173318")]
		public void Reset()
		{
		}

		// Token: 0x060035FE RID: 13822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003118")]
		[Address(RVA = "0x101173380", Offset = "0x1173380", VA = "0x101173380")]
		private void AddUnlockData(long charaMngID, int charaID)
		{
		}

		// Token: 0x060035FF RID: 13823 RVA: 0x00016BD8 File Offset: 0x00014DD8
		[Token(Token = "0x6003119")]
		[Address(RVA = "0x1011734B0", Offset = "0x11734B0", VA = "0x1011734B0")]
		public static bool CheckUnlockedCharaQuest(ref CharacterQuestListDB_Param charaQuestListParam)
		{
			return default(bool);
		}

		// Token: 0x06003600 RID: 13824 RVA: 0x00016BF0 File Offset: 0x00014DF0
		[Token(Token = "0x600311A")]
		[Address(RVA = "0x1011734DC", Offset = "0x11734DC", VA = "0x1011734DC")]
		private static bool _CheckUnlockedCharaQuest_CharacterEvolve(ref CharacterQuestListDB_Param charaQuestListParam)
		{
			return default(bool);
		}

		// Token: 0x06003601 RID: 13825 RVA: 0x00016C08 File Offset: 0x00014E08
		[Token(Token = "0x600311B")]
		[Address(RVA = "0x101173558", Offset = "0x1173558", VA = "0x101173558")]
		private static bool _CheckUnlockedCharaQuest_CharacterLvUp(ref CharacterQuestListDB_Param charaQuestListParam)
		{
			return default(bool);
		}

		// Token: 0x06003602 RID: 13826 RVA: 0x00016C20 File Offset: 0x00014E20
		[Token(Token = "0x600311C")]
		[Address(RVA = "0x101173608", Offset = "0x1173608", VA = "0x101173608")]
		private static bool _CheckUnlockedCharaQuest_WeaponEvolve(ref CharacterQuestListDB_Param charaQuestListParam)
		{
			return default(bool);
		}

		// Token: 0x06003603 RID: 13827 RVA: 0x00016C38 File Offset: 0x00014E38
		[Token(Token = "0x600311D")]
		[Address(RVA = "0x101173788", Offset = "0x1173788", VA = "0x101173788")]
		public bool CheckAndStackUnlock_CharaEvolution(UserCharacterData after)
		{
			return default(bool);
		}

		// Token: 0x06003604 RID: 13828 RVA: 0x00016C50 File Offset: 0x00014E50
		[Token(Token = "0x600311E")]
		[Address(RVA = "0x101173940", Offset = "0x1173940", VA = "0x101173940")]
		public bool CheckAndStackUnlock_CharaLvUp(int beforeLv, UserCharacterData after)
		{
			return default(bool);
		}

		// Token: 0x06003605 RID: 13829 RVA: 0x00016C68 File Offset: 0x00014E68
		[Token(Token = "0x600311F")]
		[Address(RVA = "0x101173B80", Offset = "0x1173B80", VA = "0x101173B80")]
		public bool CheckAndStackUnlock_CharaLvUp(List<int> beforeLvs, List<UserCharacterData> afters)
		{
			return default(bool);
		}

		// Token: 0x06003606 RID: 13830 RVA: 0x00016C80 File Offset: 0x00014E80
		[Token(Token = "0x6003120")]
		[Address(RVA = "0x101173C9C", Offset = "0x1173C9C", VA = "0x101173C9C")]
		public bool CheckAndStackUnlock_WeaponEvolution(long afterWeaponMngID)
		{
			return default(bool);
		}

		// Token: 0x06003607 RID: 13831 RVA: 0x00016C98 File Offset: 0x00014E98
		[Token(Token = "0x6003121")]
		[Address(RVA = "0x101173D48", Offset = "0x1173D48", VA = "0x101173D48")]
		public bool CheckAndStackUnlock_WeaponEvolution(UserWeaponData after)
		{
			return default(bool);
		}

		// Token: 0x06003608 RID: 13832 RVA: 0x00016CB0 File Offset: 0x00014EB0
		[Token(Token = "0x6003122")]
		[Address(RVA = "0x101174070", Offset = "0x1174070", VA = "0x101174070")]
		public bool Play(bool disableGotoCharaQuest)
		{
			return default(bool);
		}

		// Token: 0x06003609 RID: 13833 RVA: 0x00016CC8 File Offset: 0x00014EC8
		[Token(Token = "0x6003123")]
		[Address(RVA = "0x10117428C", Offset = "0x117428C", VA = "0x10117428C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600360A RID: 13834 RVA: 0x00016CE0 File Offset: 0x00014EE0
		[Token(Token = "0x6003124")]
		[Address(RVA = "0x1011742A8", Offset = "0x11742A8", VA = "0x1011742A8")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x0600360B RID: 13835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003125")]
		[Address(RVA = "0x1011742B8", Offset = "0x11742B8", VA = "0x1011742B8")]
		private void OnConfirmGotoQuest(bool isYes)
		{
		}

		// Token: 0x0600360C RID: 13836 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003126")]
		[Address(RVA = "0x101174178", Offset = "0x1174178", VA = "0x101174178")]
		public List<int> GetUnlockCharaIDs()
		{
			return null;
		}

		// Token: 0x0400461E RID: 17950
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400313C")]
		private CharaQuestUnlock.eStep m_Step;

		// Token: 0x0400461F RID: 17951
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400313D")]
		private List<CharaQuestUnlock.UnlockData> m_UnlockDatas;

		// Token: 0x04004620 RID: 17952
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400313E")]
		private bool m_IsGotoQuest;

		// Token: 0x02000BF9 RID: 3065
		[Token(Token = "0x2001082")]
		private enum eStep
		{
			// Token: 0x04004622 RID: 17954
			[Token(Token = "0x4006837")]
			None = -1,
			// Token: 0x04004623 RID: 17955
			[Token(Token = "0x4006838")]
			GotoQuestConfirm,
			// Token: 0x04004624 RID: 17956
			[Token(Token = "0x4006839")]
			GotoQuestConfirm_Wait,
			// Token: 0x04004625 RID: 17957
			[Token(Token = "0x400683A")]
			End
		}

		// Token: 0x02000BFA RID: 3066
		[Token(Token = "0x2001083")]
		public class UnlockData
		{
			// Token: 0x0600360D RID: 13837 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600619C")]
			[Address(RVA = "0x101174358", Offset = "0x1174358", VA = "0x101174358")]
			public UnlockData(long charaMngID, int charaID)
			{
			}

			// Token: 0x04004626 RID: 17958
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400683B")]
			public long m_CharaMngID;

			// Token: 0x04004627 RID: 17959
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400683C")]
			public int m_CharaID;
		}
	}
}
