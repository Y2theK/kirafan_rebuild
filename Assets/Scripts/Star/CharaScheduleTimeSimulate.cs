﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000678 RID: 1656
	[Token(Token = "0x2000549")]
	[StructLayout(3)]
	public class CharaScheduleTimeSimulate
	{
		// Token: 0x06001832 RID: 6194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D0")]
		[Address(RVA = "0x1011743A4", Offset = "0x11743A4", VA = "0x1011743A4")]
		public void CreateTable(int fnum)
		{
		}

		// Token: 0x06001833 RID: 6195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D1")]
		[Address(RVA = "0x10117440C", Offset = "0x117440C", VA = "0x10117440C")]
		public void AddSumilateState(FieldObjHandleChara pstate)
		{
		}

		// Token: 0x06001834 RID: 6196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D2")]
		[Address(RVA = "0x1011744A8", Offset = "0x11744A8", VA = "0x1011744A8")]
		public void CalcSimulate(long fstarttime, long fendtime, FieldBuildMap pbuildmap, bool frefresh)
		{
		}

		// Token: 0x06001835 RID: 6197 RVA: 0x0000B190 File Offset: 0x00009390
		[Token(Token = "0x60016D3")]
		[Address(RVA = "0x101174B68", Offset = "0x1174B68", VA = "0x101174B68")]
		public bool CalcSimulate2(long nowTimeBase, long nextTimeBase, FieldBuildMap pbuildmap, bool frefresh, bool isAllSimulate = false)
		{
			return default(bool);
		}

		// Token: 0x06001836 RID: 6198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D4")]
		[Address(RVA = "0x101175884", Offset = "0x1175884", VA = "0x101175884")]
		public void SetUpPartyCom(FldComPartyScript pcomparty)
		{
		}

		// Token: 0x06001837 RID: 6199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D5")]
		[Address(RVA = "0x101175934", Offset = "0x1175934", VA = "0x101175934")]
		public CharaScheduleTimeSimulate()
		{
		}

		// Token: 0x04002789 RID: 10121
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400206C")]
		public FieldObjHandleChara[] m_EntryCharaTable;

		// Token: 0x0400278A RID: 10122
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400206D")]
		public int m_EntryCharaNum;

		// Token: 0x0400278B RID: 10123
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400206E")]
		public int m_EntryCharaMax;

		// Token: 0x02000679 RID: 1657
		[Token(Token = "0x2000E03")]
		public struct ScheduleTimeKey
		{
			// Token: 0x0400278C RID: 10124
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005A9E")]
			public int m_CharaIndex;

			// Token: 0x0400278D RID: 10125
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005A9F")]
			public int m_TimeIndex;

			// Token: 0x0400278E RID: 10126
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005AA0")]
			public int m_TimeKey;
		}

		// Token: 0x0200067A RID: 1658
		[Token(Token = "0x2000E04")]
		public class ScheduleTimePack
		{
			// Token: 0x06001838 RID: 6200 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E26")]
			[Address(RVA = "0x101174A50", Offset = "0x1174A50", VA = "0x101174A50")]
			public ScheduleTimePack(int fkeynum)
			{
			}

			// Token: 0x06001839 RID: 6201 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E27")]
			[Address(RVA = "0x10117593C", Offset = "0x117593C", VA = "0x10117593C")]
			private void InsertTimeKey(int findex, int fsubindex, int ftime)
			{
			}

			// Token: 0x0600183A RID: 6202 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E28")]
			[Address(RVA = "0x101174AC4", Offset = "0x1174AC4", VA = "0x101174AC4")]
			public void InsertSchedule(UserScheduleData.ListPack pschedule, int findex)
			{
			}

			// Token: 0x0400278F RID: 10127
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AA1")]
			public CharaScheduleTimeSimulate.ScheduleTimeKey[] m_Table;

			// Token: 0x04002790 RID: 10128
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AA2")]
			public int m_EntryNum;

			// Token: 0x04002791 RID: 10129
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005AA3")]
			public int m_Max;
		}
	}
}
