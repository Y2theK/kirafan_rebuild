﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000454 RID: 1108
	[Token(Token = "0x200036F")]
	[StructLayout(3)]
	public class CharacterAnim
	{
		// Token: 0x17000114 RID: 276
		// (get) Token: 0x060010E7 RID: 4327 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000FD")]
		public CharacterAnim.ModelSet[] ModelSets
		{
			[Token(Token = "0x6000FA5")]
			[Address(RVA = "0x101175C88", Offset = "0x1175C88", VA = "0x101175C88")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x060010E8 RID: 4328 RVA: 0x000071D0 File Offset: 0x000053D0
		[Token(Token = "0x170000FE")]
		public int ModelSetsNum
		{
			[Token(Token = "0x6000FA6")]
			[Address(RVA = "0x101175C90", Offset = "0x1175C90", VA = "0x101175C90")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x060010E9 RID: 4329 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000FF")]
		public CharacterAnim.ModelSet[] WeaponModelSets
		{
			[Token(Token = "0x6000FA7")]
			[Address(RVA = "0x101175CA8", Offset = "0x1175CA8", VA = "0x101175CA8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x060010EA RID: 4330 RVA: 0x000071E8 File Offset: 0x000053E8
		[Token(Token = "0x17000100")]
		public int WeaponModelSetsNum
		{
			[Token(Token = "0x6000FA8")]
			[Address(RVA = "0x101175CB0", Offset = "0x1175CB0", VA = "0x101175CB0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x060010EB RID: 4331 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000101")]
		public GameObject Shadow
		{
			[Token(Token = "0x6000FA9")]
			[Address(RVA = "0x101175CEC", Offset = "0x1175CEC", VA = "0x101175CEC")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060010EC RID: 4332 RVA: 0x00007200 File Offset: 0x00005400
		[Token(Token = "0x17000102")]
		public CharacterDefine.eDir Dir
		{
			[Token(Token = "0x6000FAA")]
			[Address(RVA = "0x101175CF4", Offset = "0x1175CF4", VA = "0x101175CF4")]
			get
			{
				return CharacterDefine.eDir.L;
			}
		}

		// Token: 0x060010ED RID: 4333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FAB")]
		[Address(RVA = "0x101175CFC", Offset = "0x1175CFC", VA = "0x101175CFC")]
		public CharacterAnim(CharacterHandler charaHndl)
		{
		}

		// Token: 0x060010EE RID: 4334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FAC")]
		[Address(RVA = "0x101175E94", Offset = "0x1175E94", VA = "0x101175E94")]
		public void Update()
		{
		}

		// Token: 0x060010EF RID: 4335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FAD")]
		[Address(RVA = "0x1011763A8", Offset = "0x11763A8", VA = "0x1011763A8")]
		public void LateUpdate()
		{
		}

		// Token: 0x060010F0 RID: 4336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FAE")]
		[Address(RVA = "0x1011765B0", Offset = "0x11765B0", VA = "0x1011765B0")]
		public void Setup()
		{
		}

		// Token: 0x060010F1 RID: 4337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FAF")]
		[Address(RVA = "0x1011765B4", Offset = "0x11765B4", VA = "0x1011765B4")]
		public void AttachCharaModel(GameObject[] modelObjs)
		{
		}

		// Token: 0x060010F2 RID: 4338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB0")]
		[Address(RVA = "0x10117727C", Offset = "0x117727C", VA = "0x10117727C")]
		public void AttachWeaponModel(GameObject[] modelObjs)
		{
		}

		// Token: 0x060010F3 RID: 4339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB1")]
		[Address(RVA = "0x101177A84", Offset = "0x1177A84", VA = "0x101177A84")]
		public void AttachShadow(GameObject modelObj)
		{
		}

		// Token: 0x060010F4 RID: 4340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB2")]
		[Address(RVA = "0x101177BE4", Offset = "0x1177BE4", VA = "0x101177BE4")]
		public void SetupShadowTransform(bool isReverse)
		{
		}

		// Token: 0x060010F5 RID: 4341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB3")]
		[Address(RVA = "0x101177DD0", Offset = "0x1177DD0", VA = "0x101177DD0")]
		public void Destroy()
		{
		}

		// Token: 0x060010F6 RID: 4342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB4")]
		[Address(RVA = "0x101178184", Offset = "0x1178184", VA = "0x101178184")]
		public void ReAttachWeapon(CharacterDefine.eWeaponIndex wpnIndex)
		{
		}

		// Token: 0x060010F7 RID: 4343 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FB5")]
		[Address(RVA = "0x1011778D4", Offset = "0x11778D4", VA = "0x1011778D4")]
		public Transform GetHandTransform(bool isLeft)
		{
			return null;
		}

		// Token: 0x060010F8 RID: 4344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB6")]
		[Address(RVA = "0x101178360", Offset = "0x1178360", VA = "0x101178360")]
		public void AnimCtrlOpen(int partsIndex)
		{
		}

		// Token: 0x060010F9 RID: 4345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB7")]
		[Address(RVA = "0x1011784C0", Offset = "0x11784C0", VA = "0x1011784C0")]
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder, int partsIndex)
		{
		}

		// Token: 0x060010FA RID: 4346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB8")]
		[Address(RVA = "0x1011786A8", Offset = "0x11786A8", VA = "0x1011786A8")]
		public void AnimCtrlClose(int partsIndex)
		{
		}

		// Token: 0x060010FB RID: 4347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FB9")]
		[Address(RVA = "0x101178808", Offset = "0x1178808", VA = "0x101178808")]
		public void PlayAnim(string actionKey, WrapMode wrapMode, float blendSec = 0f, float offsetTime = 0f, float timeScale = 1f)
		{
		}

		// Token: 0x060010FC RID: 4348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FBA")]
		[Address(RVA = "0x101178A88", Offset = "0x1178A88", VA = "0x101178A88")]
		public void SetAnimationPlaySpeed(float timeScale)
		{
		}

		// Token: 0x060010FD RID: 4349 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FBB")]
		[Address(RVA = "0x101178C1C", Offset = "0x1178C1C", VA = "0x101178C1C")]
		public string GetLastActionKey()
		{
			return null;
		}

		// Token: 0x060010FE RID: 4350 RVA: 0x00007218 File Offset: 0x00005418
		[Token(Token = "0x6000FBC")]
		[Address(RVA = "0x101178C24", Offset = "0x1178C24", VA = "0x101178C24")]
		public int GetPlayingFrame(string actionKey, int partsIndex = 0)
		{
			return 0;
		}

		// Token: 0x060010FF RID: 4351 RVA: 0x00007230 File Offset: 0x00005430
		[Token(Token = "0x6000FBD")]
		[Address(RVA = "0x101178EB0", Offset = "0x1178EB0", VA = "0x101178EB0")]
		public float GetPlayingSec(string actionKey, int partsIndex = 0)
		{
			return 0f;
		}

		// Token: 0x06001100 RID: 4352 RVA: 0x00007248 File Offset: 0x00005448
		[Token(Token = "0x6000FBE")]
		[Address(RVA = "0x101179084", Offset = "0x1179084", VA = "0x101179084")]
		public float GetMaxSec(string actionKey, int partsIndex = 0)
		{
			return 0f;
		}

		// Token: 0x06001101 RID: 4353 RVA: 0x00007260 File Offset: 0x00005460
		[Token(Token = "0x6000FBF")]
		[Address(RVA = "0x101179200", Offset = "0x1179200", VA = "0x101179200")]
		public WrapMode GetWrapMode(int partsIndex = 0)
		{
			return WrapMode.Default;
		}

		// Token: 0x06001102 RID: 4354 RVA: 0x00007278 File Offset: 0x00005478
		[Token(Token = "0x6000FC0")]
		[Address(RVA = "0x101179354", Offset = "0x1179354", VA = "0x101179354")]
		public bool IsPlayingAnim(string actionKey)
		{
			return default(bool);
		}

		// Token: 0x06001103 RID: 4355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC1")]
		[Address(RVA = "0x1011795BC", Offset = "0x11795BC", VA = "0x1011795BC")]
		public void Pause()
		{
		}

		// Token: 0x06001104 RID: 4356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC2")]
		[Address(RVA = "0x101179740", Offset = "0x1179740", VA = "0x101179740")]
		public void FrameReset()
		{
		}

		// Token: 0x06001105 RID: 4357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC3")]
		[Address(RVA = "0x1011798C8", Offset = "0x11798C8", VA = "0x1011798C8")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x06001106 RID: 4358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC4")]
		[Address(RVA = "0x101179AA4", Offset = "0x1179AA4", VA = "0x101179AA4")]
		public void AddFacialAnim(MeigeAnimClipHolder meigeAnimClipHolder)
		{
		}

		// Token: 0x06001107 RID: 4359 RVA: 0x00007290 File Offset: 0x00005490
		[Token(Token = "0x6000FC5")]
		[Address(RVA = "0x101179AAC", Offset = "0x1179AAC", VA = "0x101179AAC")]
		public int GetFacialID()
		{
			return 0;
		}

		// Token: 0x06001108 RID: 4360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC6")]
		[Address(RVA = "0x1011799A8", Offset = "0x11799A8", VA = "0x1011799A8")]
		public void SetFacial(int facialID, int overrideSetID)
		{
		}

		// Token: 0x06001109 RID: 4361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC7")]
		[Address(RVA = "0x101179AB4", Offset = "0x1179AB4", VA = "0x101179AB4")]
		public void SetFacial(int facialID)
		{
		}

		// Token: 0x0600110A RID: 4362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FC8")]
		[Address(RVA = "0x101179FA8", Offset = "0x1179FA8", VA = "0x101179FA8")]
		public void ResetFacial()
		{
		}

		// Token: 0x0600110B RID: 4363 RVA: 0x000072A8 File Offset: 0x000054A8
		[Token(Token = "0x6000FC9")]
		[Address(RVA = "0x10117A074", Offset = "0x117A074", VA = "0x10117A074")]
		public int GetBattleDefaultFacial()
		{
			return 0;
		}

		// Token: 0x0600110C RID: 4364 RVA: 0x000072C0 File Offset: 0x000054C0
		[Token(Token = "0x6000FCA")]
		[Address(RVA = "0x10117A158", Offset = "0x117A158", VA = "0x10117A158")]
		public bool IsEnableBlink()
		{
			return default(bool);
		}

		// Token: 0x0600110D RID: 4365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FCB")]
		[Address(RVA = "0x10117A160", Offset = "0x117A160", VA = "0x10117A160")]
		public void SetBlink(bool isEnableBlink)
		{
		}

		// Token: 0x0600110E RID: 4366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FCC")]
		[Address(RVA = "0x101175EB8", Offset = "0x1175EB8", VA = "0x101175EB8")]
		private void UpdateBlink()
		{
		}

		// Token: 0x0600110F RID: 4367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FCD")]
		[Address(RVA = "0x101176E38", Offset = "0x1176E38", VA = "0x101176E38")]
		private void SetupFloatingObj()
		{
		}

		// Token: 0x06001110 RID: 4368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FCE")]
		[Address(RVA = "0x10117A488", Offset = "0x117A488", VA = "0x10117A488")]
		public void SetEnableFloatingObj(bool flg)
		{
		}

		// Token: 0x06001111 RID: 4369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FCF")]
		[Address(RVA = "0x1011763AC", Offset = "0x11763AC", VA = "0x1011763AC")]
		private void UpdateFloatingObj()
		{
		}

		// Token: 0x06001112 RID: 4370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD0")]
		[Address(RVA = "0x10117A58C", Offset = "0x117A58C", VA = "0x10117A58C")]
		public void ChangeMeshColor(Color startColor, Color endColor, float sec)
		{
		}

		// Token: 0x06001113 RID: 4371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD1")]
		[Address(RVA = "0x10117AB7C", Offset = "0x117AB7C", VA = "0x10117AB7C")]
		public void ResetMeshColor()
		{
		}

		// Token: 0x06001114 RID: 4372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD2")]
		[Address(RVA = "0x101176320", Offset = "0x1176320", VA = "0x101176320")]
		private void UpdateMeshColor()
		{
		}

		// Token: 0x06001115 RID: 4373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD3")]
		[Address(RVA = "0x10117A5EC", Offset = "0x117A5EC", VA = "0x10117A5EC")]
		private void SetMeshColor(Color color)
		{
		}

		// Token: 0x06001116 RID: 4374 RVA: 0x000072D8 File Offset: 0x000054D8
		[Token(Token = "0x6000FD4")]
		[Address(RVA = "0x10117ABA8", Offset = "0x117ABA8", VA = "0x10117ABA8")]
		public Color GetCurrentMeshColor()
		{
			return default(Color);
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD5")]
		[Address(RVA = "0x10117ABB4", Offset = "0x117ABB4", VA = "0x10117ABB4")]
		public void ChangeDir(CharacterDefine.eDir dir, bool isAnimChange = false)
		{
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FD6")]
		[Address(RVA = "0x10117AD1C", Offset = "0x117AD1C", VA = "0x10117AD1C")]
		public void SetSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x000072F0 File Offset: 0x000054F0
		[Token(Token = "0x6000FD7")]
		[Address(RVA = "0x10117B0D0", Offset = "0x117B0D0", VA = "0x10117B0D0")]
		public int GetSortingOrder()
		{
			return 0;
		}

		// Token: 0x0600111A RID: 4378 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FD8")]
		[Address(RVA = "0x10117B208", Offset = "0x117B208", VA = "0x10117B208")]
		public Transform GetLocator(CharacterDefine.eLocatorIndex locatorIndex)
		{
			return null;
		}

		// Token: 0x0600111B RID: 4379 RVA: 0x00007308 File Offset: 0x00005508
		[Token(Token = "0x6000FD9")]
		[Address(RVA = "0x10117B5E0", Offset = "0x117B5E0", VA = "0x10117B5E0")]
		public Vector3 GetLocatorPos(CharacterDefine.eLocatorIndex locatorIndex, bool isLocalPosition = false)
		{
			return default(Vector3);
		}

		// Token: 0x0600111C RID: 4380 RVA: 0x00007320 File Offset: 0x00005520
		[Token(Token = "0x6000FDA")]
		[Address(RVA = "0x10117B60C", Offset = "0x117B60C", VA = "0x10117B60C")]
		public Vector3 GetLocatorPos(CharacterDefine.eLocatorIndex locatorIndex, bool isLocalPosition, out Transform out_locator, out float out_offsetYfromLocator)
		{
			return default(Vector3);
		}

		// Token: 0x0600111D RID: 4381 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FDB")]
		[Address(RVA = "0x10117BB20", Offset = "0x117BB20", VA = "0x10117BB20")]
		public Transform GetWeaponLocator(CharacterDefine.eWeaponIndex weaponIndex, int locatorIndex)
		{
			return null;
		}

		// Token: 0x0600111E RID: 4382 RVA: 0x00007338 File Offset: 0x00005538
		[Token(Token = "0x6000FDC")]
		[Address(RVA = "0x10117BCD4", Offset = "0x117BCD4", VA = "0x10117BCD4")]
		public Vector3 GetWeaponLocatorPos(CharacterDefine.eWeaponIndex weaponIndex, int locatorIndex, bool isLocalPosition = false)
		{
			return default(Vector3);
		}

		// Token: 0x0600111F RID: 4383 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FDD")]
		[Address(RVA = "0x10117BE2C", Offset = "0x117BE2C", VA = "0x10117BE2C")]
		public List<Material> GetMaterials()
		{
			return null;
		}

		// Token: 0x040013C2 RID: 5058
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000E65")]
		private CharacterHandler m_Owner;

		// Token: 0x040013C3 RID: 5059
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E66")]
		private CharacterAnim.ModelSet[] m_ModelSets;

		// Token: 0x040013C4 RID: 5060
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E67")]
		private CharacterAnim.ModelSet[] m_WeaponModelSets;

		// Token: 0x040013C5 RID: 5061
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E68")]
		public MeigeAnimClipHolder m_FacialClipHolder;

		// Token: 0x040013C6 RID: 5062
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E69")]
		private GameObject m_Shadow;

		// Token: 0x040013C7 RID: 5063
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E6A")]
		public MsbHandler m_ShadowMsbHndl;

		// Token: 0x040013C8 RID: 5064
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E6B")]
		private CharacterDefine.eDir m_Dir;

		// Token: 0x040013C9 RID: 5065
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000E6C")]
		private string m_LastActionKey;

		// Token: 0x040013CA RID: 5066
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E6D")]
		private WrapMode m_LastWrapMode;

		// Token: 0x040013CB RID: 5067
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4000E6E")]
		private int m_CurrentFacialID;

		// Token: 0x040013CC RID: 5068
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000E6F")]
		private bool m_IsEnableBlink;

		// Token: 0x040013CD RID: 5069
		[Cpp2IlInjected.FieldOffset(Offset = "0x59")]
		[Token(Token = "0x4000E70")]
		private bool m_BlinkOpen;

		// Token: 0x040013CE RID: 5070
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4000E71")]
		private int m_BlinkCount;

		// Token: 0x040013CF RID: 5071
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000E72")]
		private float m_BlinkTimer;

		// Token: 0x040013D0 RID: 5072
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4000E73")]
		private CharacterDefine.eDir m_BlinkDir;

		// Token: 0x040013D1 RID: 5073
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000E74")]
		private CharacterAnim.eBlinkType m_BlinkType;

		// Token: 0x040013D2 RID: 5074
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000E75")]
		private readonly float[,] BLINK_TYPE_PROBABLITY;

		// Token: 0x040013D3 RID: 5075
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000E76")]
		private readonly float[] BLINK_CLOSE_TIME;

		// Token: 0x040013D4 RID: 5076
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000E77")]
		private readonly float[] BLINK_OPEN_TIME_RANGE;

		// Token: 0x040013D5 RID: 5077
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000E78")]
		private List<CharacterAnim.FloatingSet> m_FloatingSets;

		// Token: 0x040013D6 RID: 5078
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000E79")]
		private Color m_StartColor;

		// Token: 0x040013D7 RID: 5079
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000E7A")]
		private Color m_EndColor;

		// Token: 0x040013D8 RID: 5080
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000E7B")]
		private Color m_CurrentColor;

		// Token: 0x040013D9 RID: 5081
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000E7C")]
		private float m_MeshColorTime;

		// Token: 0x040013DA RID: 5082
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4000E7D")]
		private float m_MeshColorTimer;

		// Token: 0x040013DB RID: 5083
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000E7E")]
		private bool m_IsUpdateMeshColor;

		// Token: 0x02000455 RID: 1109
		[Token(Token = "0x2000DB9")]
		public class ModelSet
		{
			// Token: 0x06001120 RID: 4384 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E06")]
			[Address(RVA = "0x101178070", Offset = "0x1178070", VA = "0x101178070")]
			public void Destroy()
			{
			}

			// Token: 0x06001121 RID: 4385 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E07")]
			[Address(RVA = "0x101176E30", Offset = "0x1176E30", VA = "0x101176E30")]
			public ModelSet()
			{
			}

			// Token: 0x040013DC RID: 5084
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058DE")]
			public GameObject m_Obj;

			// Token: 0x040013DD RID: 5085
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058DF")]
			public Transform m_Transform;

			// Token: 0x040013DE RID: 5086
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40058E0")]
			public Transform m_RootTransform;

			// Token: 0x040013DF RID: 5087
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40058E1")]
			public Animation m_Anim;

			// Token: 0x040013E0 RID: 5088
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40058E2")]
			public MeigeAnimCtrl m_MeigeAnimCtrl;

			// Token: 0x040013E1 RID: 5089
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40058E3")]
			public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

			// Token: 0x040013E2 RID: 5090
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40058E4")]
			public MsbHandler m_MsbHndl;
		}

		// Token: 0x02000456 RID: 1110
		[Token(Token = "0x2000DBA")]
		private enum eBlinkType
		{
			// Token: 0x040013E4 RID: 5092
			[Token(Token = "0x40058E6")]
			SLOW,
			// Token: 0x040013E5 RID: 5093
			[Token(Token = "0x40058E7")]
			FAST,
			// Token: 0x040013E6 RID: 5094
			[Token(Token = "0x40058E8")]
			TWO,
			// Token: 0x040013E7 RID: 5095
			[Token(Token = "0x40058E9")]
			Num
		}

		// Token: 0x02000457 RID: 1111
		[Token(Token = "0x2000DBB")]
		public class FloatingSet
		{
			// Token: 0x06001122 RID: 4386 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E08")]
			[Address(RVA = "0x10117A410", Offset = "0x117A410", VA = "0x10117A410")]
			public FloatingSet()
			{
			}

			// Token: 0x040013E8 RID: 5096
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058EA")]
			public Transform m_Transform;

			// Token: 0x040013E9 RID: 5097
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058EB")]
			public Transform m_RefTransform;

			// Token: 0x040013EA RID: 5098
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40058EC")]
			public Vector3 m_BaseOffset;

			// Token: 0x040013EB RID: 5099
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40058ED")]
			public float m_Float;
		}
	}
}
