﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000458 RID: 1112
	[Token(Token = "0x2000370")]
	[StructLayout(3)]
	public class CharacterBattle
	{
		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06001123 RID: 4387 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001124 RID: 4388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000103")]
		public BattleSystem System
		{
			[Token(Token = "0x6000FDE")]
			[Address(RVA = "0x10117C100", Offset = "0x117C100", VA = "0x10117C100")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6000FDF")]
			[Address(RVA = "0x10117C108", Offset = "0x117C108", VA = "0x10117C108")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06001125 RID: 4389 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001126 RID: 4390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000104")]
		public BattlePartyData MyParty
		{
			[Token(Token = "0x6000FE0")]
			[Address(RVA = "0x10117C110", Offset = "0x117C110", VA = "0x10117C110")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6000FE1")]
			[Address(RVA = "0x10117C118", Offset = "0x117C118", VA = "0x10117C118")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06001127 RID: 4391 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001128 RID: 4392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000105")]
		public BattlePartyData TgtParty
		{
			[Token(Token = "0x6000FE2")]
			[Address(RVA = "0x10117C120", Offset = "0x117C120", VA = "0x10117C120")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6000FE3")]
			[Address(RVA = "0x10117C128", Offset = "0x117C128", VA = "0x10117C128")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06001129 RID: 4393 RVA: 0x00007350 File Offset: 0x00005550
		// (set) Token: 0x0600112A RID: 4394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000106")]
		public BattleDefine.eJoinMember Join
		{
			[Token(Token = "0x6000FE4")]
			[Address(RVA = "0x10117C130", Offset = "0x117C130", VA = "0x10117C130")]
			[CompilerGenerated]
			get
			{
				return BattleDefine.eJoinMember.Join_1;
			}
			[Token(Token = "0x6000FE5")]
			[Address(RVA = "0x10117C138", Offset = "0x117C138", VA = "0x10117C138")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x0600112B RID: 4395 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600112C RID: 4396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000107")]
		public string Name
		{
			[Token(Token = "0x6000FE6")]
			[Address(RVA = "0x10117C140", Offset = "0x117C140", VA = "0x10117C140")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6000FE7")]
			[Address(RVA = "0x10117C148", Offset = "0x117C148", VA = "0x10117C148")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x0600112D RID: 4397 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600112E RID: 4398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000C")]
		public event Action<CharacterHandler> OnEndDeadAnimation
		{
			[Token(Token = "0x6000FE8")]
			[Address(RVA = "0x10117C150", Offset = "0x117C150", VA = "0x10117C150")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000FE9")]
			[Address(RVA = "0x10117C25C", Offset = "0x117C25C", VA = "0x10117C25C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600112F RID: 4399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FEA")]
		[Address(RVA = "0x10117C368", Offset = "0x117C368", VA = "0x10117C368")]
		public CharacterBattle()
		{
		}

		// Token: 0x06001130 RID: 4400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FEB")]
		[Address(RVA = "0x10117C6A0", Offset = "0x117C6A0", VA = "0x10117C6A0")]
		public void Update()
		{
		}

		// Token: 0x06001131 RID: 4401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FEC")]
		[Address(RVA = "0x10117F17C", Offset = "0x117F17C", VA = "0x10117F17C")]
		public void LateUpdate()
		{
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FED")]
		[Address(RVA = "0x10117FC08", Offset = "0x117FC08", VA = "0x10117FC08")]
		public void Setup(CharacterHandler charaHndl, bool setupParameterOnly)
		{
		}

		// Token: 0x06001133 RID: 4403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FEE")]
		[Address(RVA = "0x101180E98", Offset = "0x1180E98", VA = "0x101180E98")]
		public void Destroy()
		{
		}

		// Token: 0x06001134 RID: 4404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FEF")]
		[Address(RVA = "0x101180B08", Offset = "0x1180B08", VA = "0x101180B08")]
		public void SetupCollision()
		{
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06001135 RID: 4405 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000108")]
		public Dictionary<string, SkillActionPlan> SAPs
		{
			[Token(Token = "0x6000FF0")]
			[Address(RVA = "0x1011813E0", Offset = "0x11813E0", VA = "0x1011813E0")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06001136 RID: 4406 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000109")]
		public Dictionary<string, SkillActionGraphics> SAGs
		{
			[Token(Token = "0x6000FF1")]
			[Address(RVA = "0x1011813E8", Offset = "0x11813E8", VA = "0x1011813E8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06001137 RID: 4407 RVA: 0x00007368 File Offset: 0x00005568
		// (set) Token: 0x06001138 RID: 4408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700010A")]
		public bool PreparedEffectWasFromSkillAction
		{
			[Token(Token = "0x6000FF2")]
			[Address(RVA = "0x1011813F0", Offset = "0x11813F0", VA = "0x1011813F0")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6000FF3")]
			[Address(RVA = "0x1011813F8", Offset = "0x11813F8", VA = "0x1011813F8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06001139 RID: 4409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FF4")]
		[Address(RVA = "0x10118022C", Offset = "0x118022C", VA = "0x10118022C")]
		private void SetupSkillActionData()
		{
		}

		// Token: 0x0600113A RID: 4410 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FF5")]
		[Address(RVA = "0x101181400", Offset = "0x1181400", VA = "0x101181400")]
		public Dictionary<string, int> GetEffectIDFromSkillAction()
		{
			return null;
		}

		// Token: 0x0600113B RID: 4411 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000FF6")]
		[Address(RVA = "0x101181DA4", Offset = "0x1181DA4", VA = "0x101181DA4")]
		public string[] SetupAttachEffects()
		{
			return null;
		}

		// Token: 0x0600113C RID: 4412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FF7")]
		[Address(RVA = "0x10118211C", Offset = "0x118211C", VA = "0x10118211C")]
		private void ApplyAttachEffect(string effectID, string parentName, Vector3 offset)
		{
		}

		// Token: 0x0600113D RID: 4413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FF8")]
		[Address(RVA = "0x1011812AC", Offset = "0x11812AC", VA = "0x1011812AC")]
		private void FreeAttachEffects()
		{
		}

		// Token: 0x0600113E RID: 4414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FF9")]
		[Address(RVA = "0x101182348", Offset = "0x1182348", VA = "0x101182348")]
		public void ShowAttachEffects(int sortingOrder)
		{
		}

		// Token: 0x0600113F RID: 4415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FFA")]
		[Address(RVA = "0x101182624", Offset = "0x1182624", VA = "0x101182624")]
		public void HideAttachEffects()
		{
		}

		// Token: 0x06001140 RID: 4416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FFB")]
		[Address(RVA = "0x10117AFCC", Offset = "0x117AFCC", VA = "0x10117AFCC")]
		public void SetAttachEffectSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06001141 RID: 4417 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700010B")]
		public CharacterBattleParam Param
		{
			[Token(Token = "0x6000FFC")]
			[Address(RVA = "0x101182784", Offset = "0x1182784", VA = "0x101182784")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001142 RID: 4418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FFD")]
		[Address(RVA = "0x10118278C", Offset = "0x118278C", VA = "0x10118278C")]
		public void SetupParams()
		{
		}

		// Token: 0x06001143 RID: 4419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FFE")]
		[Address(RVA = "0x101184F6C", Offset = "0x1184F6C", VA = "0x101184F6C")]
		public void SetupParams(BattleSystemDataForPL bsdForPL, float[] townBuffParams, int orbID, int orbLv)
		{
		}

		// Token: 0x06001144 RID: 4420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000FFF")]
		[Address(RVA = "0x101185008", Offset = "0x1185008", VA = "0x101185008")]
		public void ResetAllParameter()
		{
		}

		// Token: 0x06001145 RID: 4421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001000")]
		[Address(RVA = "0x1011857DC", Offset = "0x11857DC", VA = "0x1011857DC")]
		private void ResetOnRevive()
		{
		}

		// Token: 0x06001146 RID: 4422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001001")]
		[Address(RVA = "0x101185858", Offset = "0x1185858", VA = "0x101185858")]
		private void ResetOnDead()
		{
		}

		// Token: 0x06001147 RID: 4423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001002")]
		[Address(RVA = "0x101185ABC", Offset = "0x1185ABC", VA = "0x101185ABC")]
		public void ResetOnRetry()
		{
		}

		// Token: 0x06001148 RID: 4424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001003")]
		[Address(RVA = "0x101185AC0", Offset = "0x1185AC0", VA = "0x101185AC0")]
		public void WavePreProcess(int waveIndex)
		{
		}

		// Token: 0x06001149 RID: 4425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001004")]
		[Address(RVA = "0x101185B00", Offset = "0x1185B00", VA = "0x101185B00")]
		public void TurnStartProcess(CharacterHandler turnOwner)
		{
		}

		// Token: 0x0600114A RID: 4426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001005")]
		[Address(RVA = "0x101185CBC", Offset = "0x1185CBC", VA = "0x101185CBC")]
		public void TurnEndProcess(CharacterHandler turnOwner)
		{
		}

		// Token: 0x0600114B RID: 4427 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001006")]
		[Address(RVA = "0x10118715C", Offset = "0x118715C", VA = "0x10118715C")]
		public bool[] GetForUIStatusIconFlags()
		{
			return null;
		}

		// Token: 0x0600114C RID: 4428 RVA: 0x00007380 File Offset: 0x00005580
		[Token(Token = "0x6001007")]
		[Address(RVA = "0x101187C54", Offset = "0x1187C54", VA = "0x101187C54")]
		public bool CanBeTogetherAttack()
		{
			return default(bool);
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x0600114D RID: 4429 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700010C")]
		private BattleCommandData m_UniqueSkillCommand
		{
			[Token(Token = "0x6001008")]
			[Address(RVA = "0x101187E4C", Offset = "0x1187E4C", VA = "0x101187E4C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600114E RID: 4430 RVA: 0x00007398 File Offset: 0x00005598
		[Token(Token = "0x1700010D")]
		public int NormalAttackSkillID
		{
			[Token(Token = "0x6001009")]
			[Address(RVA = "0x101187EB0", Offset = "0x1187EB0", VA = "0x101187EB0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600114F RID: 4431 RVA: 0x000073B0 File Offset: 0x000055B0
		[Token(Token = "0x1700010E")]
		public int CommandNum
		{
			[Token(Token = "0x600100A")]
			[Address(RVA = "0x101187EB8", Offset = "0x1187EB8", VA = "0x101187EB8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06001150 RID: 4432 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700010F")]
		public BattleCommandData ExecCommand
		{
			[Token(Token = "0x600100B")]
			[Address(RVA = "0x101187F18", Offset = "0x1187F18", VA = "0x101187F18")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06001151 RID: 4433 RVA: 0x000073C8 File Offset: 0x000055C8
		[Token(Token = "0x17000110")]
		public int TogetherAttackChainCount
		{
			[Token(Token = "0x600100C")]
			[Address(RVA = "0x101187F20", Offset = "0x1187F20", VA = "0x101187F20")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600100D")]
		[Address(RVA = "0x10117FD4C", Offset = "0x117FD4C", VA = "0x10117FD4C")]
		private void SetupSkillID()
		{
		}

		// Token: 0x06001153 RID: 4435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600100E")]
		[Address(RVA = "0x101183C44", Offset = "0x1183C44", VA = "0x101183C44")]
		private void SetupCommandDatas(BattleCommandData[] pool)
		{
		}

		// Token: 0x06001154 RID: 4436 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600100F")]
		[Address(RVA = "0x101187F28", Offset = "0x1187F28", VA = "0x101187F28")]
		public List<BattleCommandData> GetCommandDatas()
		{
			return null;
		}

		// Token: 0x06001155 RID: 4437 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001010")]
		[Address(RVA = "0x101187F30", Offset = "0x1187F30", VA = "0x101187F30")]
		public BattleCommandData GetCommandDataAt(int commandIndex)
		{
			return null;
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001011")]
		[Address(RVA = "0x101187FA0", Offset = "0x1187FA0", VA = "0x101187FA0")]
		public List<BattleCommandData> GetUniqueSkillCommandDatas()
		{
			return null;
		}

		// Token: 0x06001157 RID: 4439 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001012")]
		[Address(RVA = "0x101186B18", Offset = "0x1186B18", VA = "0x101186B18")]
		public BattleCommandData GetUniqueSkillCommandData(int selectedIndex = 0)
		{
			return null;
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x000073E0 File Offset: 0x000055E0
		[Token(Token = "0x6001013")]
		[Address(RVA = "0x101187FA8", Offset = "0x1187FA8", VA = "0x101187FA8")]
		public bool CanBeExecCommand(int commandIndex)
		{
			return default(bool);
		}

		// Token: 0x06001159 RID: 4441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001014")]
		[Address(RVA = "0x101188080", Offset = "0x1188080", VA = "0x101188080")]
		public void RecastFull()
		{
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001015")]
		[Address(RVA = "0x1011856E4", Offset = "0x11856E4", VA = "0x1011856E4")]
		public void RecastZero(bool recastRecoveredFlg = false)
		{
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001016")]
		[Address(RVA = "0x101186A2C", Offset = "0x1186A2C", VA = "0x101186A2C")]
		public void RecastDecrement()
		{
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001017")]
		[Address(RVA = "0x101188148", Offset = "0x1188148", VA = "0x101188148")]
		public void RecastChange(float ratio)
		{
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x0600115D RID: 4445 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000111")]
		public BattlePassiveSkill PassiveSkill
		{
			[Token(Token = "0x6001018")]
			[Address(RVA = "0x101188268", Offset = "0x1188268", VA = "0x101188268")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600115E RID: 4446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001019")]
		[Address(RVA = "0x1011838D0", Offset = "0x11838D0", VA = "0x1011838D0")]
		private void SetupPassiveSkill(List<BattlePassiveSkillSolveSerializeField> pool)
		{
		}

		// Token: 0x0600115F RID: 4447 RVA: 0x000073F8 File Offset: 0x000055F8
		[Token(Token = "0x600101A")]
		[Address(RVA = "0x101188270", Offset = "0x1188270", VA = "0x101188270")]
		public bool IsExistAbilityPassiveSkill()
		{
			return default(bool);
		}

		// Token: 0x06001160 RID: 4448 RVA: 0x00007410 File Offset: 0x00005610
		[Token(Token = "0x600101B")]
		[Address(RVA = "0x1011882E0", Offset = "0x11882E0", VA = "0x1011882E0")]
		public bool IsExistWeaponPassiveSkill()
		{
			return default(bool);
		}

		// Token: 0x06001161 RID: 4449 RVA: 0x00007428 File Offset: 0x00005628
		[Token(Token = "0x600101C")]
		[Address(RVA = "0x101187148", Offset = "0x1187148", VA = "0x101187148")]
		public bool IsExistPassiveSkill()
		{
			return default(bool);
		}

		// Token: 0x06001162 RID: 4450 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600101D")]
		[Address(RVA = "0x101188350", Offset = "0x1188350", VA = "0x101188350")]
		public BattlePassiveSkill GetPassiveSkill()
		{
			return null;
		}

		// Token: 0x06001163 RID: 4451 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600101E")]
		[Address(RVA = "0x101188358", Offset = "0x1188358", VA = "0x101188358")]
		public List<BattlePassiveSkillSolve> SolvePassiveSkills(ePassiveSkillTrigger trigger)
		{
			return null;
		}

		// Token: 0x06001164 RID: 4452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600101F")]
		[Address(RVA = "0x1011884CC", Offset = "0x11884CC", VA = "0x1011884CC")]
		private void UpdateUIOnSolvePassiveSkills(List<BattlePassiveSkillSolve> solvedList)
		{
		}

		// Token: 0x06001165 RID: 4453 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001020")]
		[Address(RVA = "0x10118852C", Offset = "0x118852C", VA = "0x10118852C")]
		public List<BattlePassiveSkillSolve> OnPassiveTrigger_OnDamage()
		{
			return null;
		}

		// Token: 0x06001166 RID: 4454 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001021")]
		[Address(RVA = "0x101188534", Offset = "0x1188534", VA = "0x101188534")]
		public List<BattlePassiveSkillSolve> OnPassiveTrigger_OnKilledEnemy()
		{
			return null;
		}

		// Token: 0x06001167 RID: 4455 RVA: 0x00007440 File Offset: 0x00005640
		[Token(Token = "0x6001022")]
		[Address(RVA = "0x101187140", Offset = "0x1187140", VA = "0x101187140")]
		public bool IsStun()
		{
			return default(bool);
		}

		// Token: 0x06001168 RID: 4456 RVA: 0x00007458 File Offset: 0x00005658
		[Token(Token = "0x6001023")]
		[Address(RVA = "0x10118853C", Offset = "0x118853C", VA = "0x10118853C")]
		public float GetStunRatio()
		{
			return 0f;
		}

		// Token: 0x06001169 RID: 4457 RVA: 0x00007470 File Offset: 0x00005670
		[Token(Token = "0x6001024")]
		[Address(RVA = "0x1011885B8", Offset = "0x11885B8", VA = "0x1011885B8")]
		public float GetStunValue()
		{
			return 0f;
		}

		// Token: 0x0600116A RID: 4458 RVA: 0x00007488 File Offset: 0x00005688
		[Token(Token = "0x6001025")]
		[Address(RVA = "0x101185BFC", Offset = "0x1185BFC", VA = "0x101185BFC")]
		public float CalcStunValue(float value)
		{
			return 0f;
		}

		// Token: 0x0600116B RID: 4459 RVA: 0x000074A0 File Offset: 0x000056A0
		[Token(Token = "0x6001026")]
		[Address(RVA = "0x1011885C0", Offset = "0x11885C0", VA = "0x1011885C0")]
		public bool JudgeAndApplyStun()
		{
			return default(bool);
		}

		// Token: 0x0600116C RID: 4460 RVA: 0x000074B8 File Offset: 0x000056B8
		[Token(Token = "0x6001027")]
		[Address(RVA = "0x101186AF8", Offset = "0x1186AF8", VA = "0x101186AF8")]
		public bool ResetStunIfStun()
		{
			return default(bool);
		}

		// Token: 0x0600116D RID: 4461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001028")]
		[Address(RVA = "0x1011857B4", Offset = "0x11857B4", VA = "0x1011857B4")]
		public void ResetStun()
		{
		}

		// Token: 0x0600116E RID: 4462 RVA: 0x000074D0 File Offset: 0x000056D0
		[Token(Token = "0x6001029")]
		[Address(RVA = "0x101188604", Offset = "0x1188604", VA = "0x101188604")]
		public bool IsStunerJudged()
		{
			return default(bool);
		}

		// Token: 0x0600116F RID: 4463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600102A")]
		[Address(RVA = "0x10118860C", Offset = "0x118860C", VA = "0x10118860C")]
		public void SetIsStunerJudged(bool isStunerJudged)
		{
		}

		// Token: 0x06001170 RID: 4464 RVA: 0x000074E8 File Offset: 0x000056E8
		[Token(Token = "0x600102B")]
		[Address(RVA = "0x101188614", Offset = "0x1188614", VA = "0x101188614")]
		public bool IsStunerApply()
		{
			return default(bool);
		}

		// Token: 0x06001171 RID: 4465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600102C")]
		[Address(RVA = "0x10118861C", Offset = "0x118861C", VA = "0x10118861C")]
		public void SetIsStunerApply(bool isStunerApply)
		{
		}

		// Token: 0x06001172 RID: 4466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600102D")]
		[Address(RVA = "0x1011857C4", Offset = "0x11857C4", VA = "0x1011857C4")]
		public void ResetStuner()
		{
		}

		// Token: 0x06001173 RID: 4467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600102E")]
		[Address(RVA = "0x101188624", Offset = "0x1188624", VA = "0x101188624")]
		public void AttachStunOccurEffect()
		{
		}

		// Token: 0x06001174 RID: 4468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600102F")]
		[Address(RVA = "0x10117F0B0", Offset = "0x117F0B0", VA = "0x10117F0B0")]
		private void DetachStunOccurEffect()
		{
		}

		// Token: 0x06001175 RID: 4469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001030")]
		[Address(RVA = "0x101188BD4", Offset = "0x1188BD4", VA = "0x101188BD4")]
		private void AttachStunEffect()
		{
		}

		// Token: 0x06001176 RID: 4470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001031")]
		[Address(RVA = "0x1011811E0", Offset = "0x11811E0", VA = "0x1011811E0")]
		private void DetachStunEffect()
		{
		}

		// Token: 0x06001177 RID: 4471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001032")]
		[Address(RVA = "0x101188FAC", Offset = "0x1188FAC", VA = "0x101188FAC")]
		public void SetStunEffectSortingOrder(int ord)
		{
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06001178 RID: 4472 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000112")]
		public BattleAIData BattleAIData
		{
			[Token(Token = "0x6001033")]
			[Address(RVA = "0x101188FB4", Offset = "0x1188FB4", VA = "0x101188FB4")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001179 RID: 4473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001034")]
		[Address(RVA = "0x101180160", Offset = "0x1180160", VA = "0x101180160")]
		private void SetupAI()
		{
		}

		// Token: 0x0600117A RID: 4474 RVA: 0x00007500 File Offset: 0x00005700
		[Token(Token = "0x6001035")]
		[Address(RVA = "0x101186C6C", Offset = "0x1186C6C", VA = "0x101186C6C")]
		public bool IsEnableAI()
		{
			return default(bool);
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001036")]
		[Address(RVA = "0x101188FBC", Offset = "0x1188FBC", VA = "0x101188FBC")]
		public void AddAIPatternChangeHitIndex(int patternChangeHitIndex)
		{
		}

		// Token: 0x0600117C RID: 4476 RVA: 0x00007518 File Offset: 0x00005718
		[Token(Token = "0x6001037")]
		[Address(RVA = "0x10118906C", Offset = "0x118906C", VA = "0x10118906C")]
		public bool IsAIPatternChangeHitIndex(int patternChangeHitIndex)
		{
			return default(bool);
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001038")]
		[Address(RVA = "0x1011890DC", Offset = "0x11890DC", VA = "0x1011890DC")]
		public void SetAIPatternIndex(int patternIndex)
		{
		}

		// Token: 0x0600117E RID: 4478 RVA: 0x00007530 File Offset: 0x00005730
		[Token(Token = "0x6001039")]
		[Address(RVA = "0x1011890E4", Offset = "0x11890E4", VA = "0x1011890E4")]
		public int GetAIPatternIndex()
		{
			return 0;
		}

		// Token: 0x0600117F RID: 4479 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600103A")]
		[Address(RVA = "0x1011890EC", Offset = "0x11890EC", VA = "0x1011890EC")]
		public List<BattleAIFlag> GetAIFlags()
		{
			return null;
		}

		// Token: 0x06001180 RID: 4480 RVA: 0x00007548 File Offset: 0x00005748
		[Token(Token = "0x600103B")]
		[Address(RVA = "0x1011890F4", Offset = "0x11890F4", VA = "0x1011890F4")]
		public bool GetAIFlag(int flagID)
		{
			return default(bool);
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x00007560 File Offset: 0x00005760
		[Token(Token = "0x600103C")]
		[Address(RVA = "0x101189200", Offset = "0x1189200", VA = "0x101189200")]
		public bool JudgeAIFlags(List<BattleAIFlag> condFlags)
		{
			return default(bool);
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600103D")]
		[Address(RVA = "0x101189328", Offset = "0x1189328", VA = "0x101189328")]
		public void SetAIWriteFlags(List<BattleAIFlag> aiWriteFlags)
		{
		}

		// Token: 0x06001183 RID: 4483 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600103E")]
		[Address(RVA = "0x101189330", Offset = "0x1189330", VA = "0x101189330")]
		public List<BattleAIFlag> GetAIWriteFlags()
		{
			return null;
		}

		// Token: 0x06001184 RID: 4484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600103F")]
		[Address(RVA = "0x101186CC0", Offset = "0x1186CC0", VA = "0x101186CC0")]
		public void ApplyAIWriteFlag()
		{
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001040")]
		[Address(RVA = "0x101189338", Offset = "0x1189338", VA = "0x101189338")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x1001360F4", Offset = "0x1360F4")]
		public void LogAIFlags()
		{
		}

		// Token: 0x06001186 RID: 4486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001041")]
		[Address(RVA = "0x101189528", Offset = "0x1189528", VA = "0x101189528")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x10013612C", Offset = "0x13612C")]
		public void LogAIWriteFlags()
		{
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x00007578 File Offset: 0x00005778
		[Token(Token = "0x6001042")]
		[Address(RVA = "0x101189720", Offset = "0x1189720", VA = "0x101189720")]
		public int GetAIExecedNum(int patternIndex, int conditionIndex)
		{
			return 0;
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001043")]
		[Address(RVA = "0x10118987C", Offset = "0x118987C", VA = "0x10118987C")]
		public void SetAIExecedNum(int patternIndex, int conditionIndex, int num)
		{
		}

		// Token: 0x06001189 RID: 4489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001044")]
		[Address(RVA = "0x1011898FC", Offset = "0x11898FC", VA = "0x1011898FC")]
		public void IncrementAIExecedNum(int patternIndex, int conditionIndex)
		{
		}

		// Token: 0x0600118A RID: 4490 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001045")]
		[Address(RVA = "0x1011897E0", Offset = "0x11897E0", VA = "0x1011897E0")]
		private static string GetAIExecedKey(int patternIndex, int condtionIndex)
		{
			return null;
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x0600118B RID: 4491 RVA: 0x00007590 File Offset: 0x00005790
		// (set) Token: 0x0600118C RID: 4492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000113")]
		public bool IsMoveTo
		{
			[Token(Token = "0x6001046")]
			[Address(RVA = "0x101189A28", Offset = "0x1189A28", VA = "0x101189A28")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001047")]
			[Address(RVA = "0x101189A30", Offset = "0x1189A30", VA = "0x101189A30")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x000075A8 File Offset: 0x000057A8
		[Token(Token = "0x6001048")]
		[Address(RVA = "0x101189A38", Offset = "0x1189A38", VA = "0x101189A38")]
		public Vector3 GetMoveTargetPosOnAttack()
		{
			return default(Vector3);
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001049")]
		[Address(RVA = "0x101189BC8", Offset = "0x1189BC8", VA = "0x101189BC8")]
		public void MoveToPosition(Vector3 startPos, Vector3 endPos, float speed)
		{
		}

		// Token: 0x0600118F RID: 4495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600104A")]
		[Address(RVA = "0x10118A2C0", Offset = "0x118A2C0", VA = "0x10118A2C0")]
		public void MoveToPositionTime(Vector3 startPos, Vector3 endPos, float time, float delay)
		{
		}

		// Token: 0x06001190 RID: 4496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600104B")]
		[Address(RVA = "0x10118AA6C", Offset = "0x118AA6C", VA = "0x10118AA6C")]
		public void MoveToPositionFromCurrent(Vector3 endPos, float speed)
		{
		}

		// Token: 0x06001191 RID: 4497 RVA: 0x000075C0 File Offset: 0x000057C0
		[Token(Token = "0x600104C")]
		[Address(RVA = "0x10118B108", Offset = "0x118B108", VA = "0x10118B108")]
		public BattleDefine.eJoinMember GetOldSingleTargetIndex(eSkillTargetType skillTargetType)
		{
			return BattleDefine.eJoinMember.Join_1;
		}

		// Token: 0x06001192 RID: 4498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600104D")]
		[Address(RVA = "0x101184EE8", Offset = "0x1184EE8", VA = "0x101184EE8")]
		private void ResetOldSingleTargetIndices()
		{
		}

		// Token: 0x06001193 RID: 4499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600104E")]
		[Address(RVA = "0x10118B19C", Offset = "0x118B19C", VA = "0x10118B19C")]
		public void ChangeMeshColor(Color startColor, Color endColor, float sec)
		{
		}

		// Token: 0x06001194 RID: 4500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600104F")]
		[Address(RVA = "0x10118B3D4", Offset = "0x118B3D4", VA = "0x10118B3D4")]
		public void ResetMeshColor()
		{
		}

		// Token: 0x06001195 RID: 4501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001050")]
		[Address(RVA = "0x10118B4A0", Offset = "0x118B4A0", VA = "0x10118B4A0")]
		public void SetFixRootBoneTransform(bool flg)
		{
		}

		// Token: 0x06001196 RID: 4502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001051")]
		[Address(RVA = "0x10117F208", Offset = "0x117F208", VA = "0x10117F208")]
		private void UpdateFixRootBoneTransform()
		{
		}

		// Token: 0x06001197 RID: 4503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001052")]
		[Address(RVA = "0x10118BA78", Offset = "0x118BA78", VA = "0x10118BA78")]
		public void PlayCharaTouchVoice()
		{
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06001198 RID: 4504 RVA: 0x000075D8 File Offset: 0x000057D8
		[Token(Token = "0x17000114")]
		public CharacterBattle.eMode Mode
		{
			[Token(Token = "0x6001053")]
			[Address(RVA = "0x10118BAF8", Offset = "0x118BAF8", VA = "0x10118BAF8")]
			get
			{
				return CharacterBattle.eMode.Idle;
			}
		}

		// Token: 0x06001199 RID: 4505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001054")]
		[Address(RVA = "0x101186F0C", Offset = "0x1186F0C", VA = "0x101186F0C")]
		public void RequestIdleMode(float blendSec = 0f, float delaySec = 0f, bool isStunScaleAnim = false)
		{
		}

		// Token: 0x0600119A RID: 4506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001055")]
		[Address(RVA = "0x10117C800", Offset = "0x117C800", VA = "0x10117C800")]
		private void UpdateIdleMode()
		{
		}

		// Token: 0x0600119B RID: 4507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001056")]
		[Address(RVA = "0x10118BB00", Offset = "0x118BB00", VA = "0x10118BB00")]
		public void RequestDamageMode()
		{
		}

		// Token: 0x0600119C RID: 4508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001057")]
		[Address(RVA = "0x10117D014", Offset = "0x117D014", VA = "0x10117D014")]
		private void UpdateDamageMode()
		{
		}

		// Token: 0x0600119D RID: 4509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001058")]
		[Address(RVA = "0x10118BB8C", Offset = "0x118BB8C", VA = "0x10118BB8C")]
		public void RequestReviveMode()
		{
		}

		// Token: 0x0600119E RID: 4510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001059")]
		[Address(RVA = "0x10117D510", Offset = "0x117D510", VA = "0x10117D510")]
		private void UpdateReviveMode()
		{
		}

		// Token: 0x0600119F RID: 4511 RVA: 0x000075F0 File Offset: 0x000057F0
		[Token(Token = "0x600105A")]
		[Address(RVA = "0x10118BC54", Offset = "0x118BC54", VA = "0x10118BC54")]
		public bool IsPlayingReviveAnim()
		{
			return default(bool);
		}

		// Token: 0x060011A0 RID: 4512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600105B")]
		[Address(RVA = "0x101181048", Offset = "0x1181048", VA = "0x101181048")]
		private void DetachReviveEffectHandler()
		{
		}

		// Token: 0x060011A1 RID: 4513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600105C")]
		[Address(RVA = "0x10118BC78", Offset = "0x118BC78", VA = "0x10118BC78")]
		public void RequestDeadMode()
		{
		}

		// Token: 0x060011A2 RID: 4514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600105D")]
		[Address(RVA = "0x10117DA08", Offset = "0x117DA08", VA = "0x10117DA08")]
		private void UpdateDeadMode()
		{
		}

		// Token: 0x060011A3 RID: 4515 RVA: 0x00007608 File Offset: 0x00005808
		[Token(Token = "0x600105E")]
		[Address(RVA = "0x10118BD04", Offset = "0x118BD04", VA = "0x10118BD04")]
		public bool IsPlayingDeadAnim()
		{
			return default(bool);
		}

		// Token: 0x060011A4 RID: 4516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600105F")]
		[Address(RVA = "0x101181114", Offset = "0x1181114", VA = "0x101181114")]
		private void DetachDeadEffectHandler()
		{
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x060011A5 RID: 4517 RVA: 0x00007620 File Offset: 0x00005820
		[Token(Token = "0x17000115")]
		public int BattleWinID
		{
			[Token(Token = "0x6001060")]
			[Address(RVA = "0x10118BD28", Offset = "0x118BD28", VA = "0x10118BD28")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060011A6 RID: 4518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001061")]
		[Address(RVA = "0x10118BD30", Offset = "0x118BD30", VA = "0x10118BD30")]
		public void RequestWinMode(bool isStartFromLoop = false)
		{
		}

		// Token: 0x060011A7 RID: 4519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001062")]
		[Address(RVA = "0x10117E8F4", Offset = "0x117E8F4", VA = "0x10117E8F4")]
		private void UpdateWinMode()
		{
		}

		// Token: 0x060011A8 RID: 4520 RVA: 0x00007638 File Offset: 0x00005838
		[Token(Token = "0x6001063")]
		[Address(RVA = "0x10118BDB8", Offset = "0x118BDB8", VA = "0x10118BDB8")]
		public bool IsPlayingWinAnim()
		{
			return default(bool);
		}

		// Token: 0x060011A9 RID: 4521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001064")]
		[Address(RVA = "0x10118BDDC", Offset = "0x118BDDC", VA = "0x10118BDDC")]
		public BattleCommandData RequestCommandMode(SkillActionPlayer skillActionPlayer, int commandIndex, bool isUseUniqueSkill, int togetherAttackChainCount)
		{
			return null;
		}

		// Token: 0x060011AA RID: 4522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001065")]
		[Address(RVA = "0x10117E4D8", Offset = "0x117E4D8", VA = "0x10117E4D8")]
		private void UpdateCommandMode()
		{
		}

		// Token: 0x060011AB RID: 4523 RVA: 0x00007650 File Offset: 0x00005850
		[Token(Token = "0x6001066")]
		[Address(RVA = "0x10118BF38", Offset = "0x118BF38", VA = "0x10118BF38")]
		private SkillActionPlayer.RefAnimTime SkillActionPlayerGetRefAnimTime()
		{
			return default(SkillActionPlayer.RefAnimTime);
		}

		// Token: 0x060011AC RID: 4524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001067")]
		[Address(RVA = "0x10118C02C", Offset = "0x118C02C", VA = "0x10118C02C")]
		public void RequestInMode(float delaySec = 0f)
		{
		}

		// Token: 0x060011AD RID: 4525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001068")]
		[Address(RVA = "0x10117EAFC", Offset = "0x117EAFC", VA = "0x10117EAFC")]
		private void UpdateInMode()
		{
		}

		// Token: 0x060011AE RID: 4526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001069")]
		[Address(RVA = "0x10118C1D0", Offset = "0x118C1D0", VA = "0x10118C1D0")]
		public void RequestOutMode(Vector3 toPos, float delaySec = 0f, bool isToFront = false)
		{
		}

		// Token: 0x060011AF RID: 4527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600106A")]
		[Address(RVA = "0x10118C0B4", Offset = "0x118C0B4", VA = "0x10118C0B4")]
		private void SetOutFaceDir(bool isPartyFrontDir)
		{
		}

		// Token: 0x060011B0 RID: 4528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600106B")]
		[Address(RVA = "0x10117ECC4", Offset = "0x117ECC4", VA = "0x10117ECC4")]
		private void UpdateOutMode()
		{
		}

		// Token: 0x060011B1 RID: 4529 RVA: 0x00007668 File Offset: 0x00005868
		[Token(Token = "0x600106C")]
		[Address(RVA = "0x10118C324", Offset = "0x118C324", VA = "0x10118C324")]
		public bool IsArrivalOutToPos()
		{
			return default(bool);
		}

		// Token: 0x060011B2 RID: 4530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600106D")]
		[Address(RVA = "0x101186FEC", Offset = "0x1186FEC", VA = "0x101186FEC")]
		public void RequestAutoAndBaseFacial(int facialID, float time, int baseFacialID, bool isBaseBlink)
		{
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600106E")]
		[Address(RVA = "0x1011870A4", Offset = "0x11870A4", VA = "0x1011870A4")]
		public void RequestAutoFacial(int facialID, float time = 1f)
		{
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600106F")]
		[Address(RVA = "0x10117F1A8", Offset = "0x117F1A8", VA = "0x10117F1A8")]
		private void UpdateAutoFacial()
		{
		}

		// Token: 0x060011B5 RID: 4533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001070")]
		[Address(RVA = "0x10118C268", Offset = "0x118C268", VA = "0x10118C268")]
		private void ResetAutoFacial()
		{
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x00007680 File Offset: 0x00005880
		[Token(Token = "0x6001071")]
		[Address(RVA = "0x10118C348", Offset = "0x118C348", VA = "0x10118C348")]
		public bool IsAutoFacialing()
		{
			return default(bool);
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001072")]
		[Address(RVA = "0x10118C358", Offset = "0x118C358", VA = "0x10118C358")]
		public void SetupForKiraraJumpScene()
		{
		}

		// Token: 0x060011B8 RID: 4536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001073")]
		[Address(RVA = "0x10118C708", Offset = "0x118C708", VA = "0x10118C708")]
		public void RevertFromKiraraJumpScene()
		{
		}

		// Token: 0x060011B9 RID: 4537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001074")]
		[Address(RVA = "0x10118CB14", Offset = "0x118CB14", VA = "0x10118CB14")]
		public void RequestKiraraJump(int index)
		{
		}

		// Token: 0x060011BA RID: 4538 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001075")]
		[Address(RVA = "0x10118CBE4", Offset = "0x118CBE4", VA = "0x10118CBE4")]
		public BattleCommandData SetTogetherAttackCommand(int togetherAttackChainCount)
		{
			return null;
		}

		// Token: 0x060011BB RID: 4539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001076")]
		[Address(RVA = "0x10118CC10", Offset = "0x118CC10", VA = "0x10118CC10")]
		public void SetupForUniqueSkillScene()
		{
		}

		// Token: 0x060011BC RID: 4540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001077")]
		[Address(RVA = "0x10118D02C", Offset = "0x118D02C", VA = "0x10118D02C")]
		public void RevertFromUniqueSkillScene()
		{
		}

		// Token: 0x060011BD RID: 4541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001078")]
		[Address(RVA = "0x101180588", Offset = "0x1180588", VA = "0x101180588")]
		private void SetupShadow()
		{
		}

		// Token: 0x060011BE RID: 4542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001079")]
		[Address(RVA = "0x10117F7D8", Offset = "0x117F7D8", VA = "0x10117F7D8")]
		private void UpdateShadow()
		{
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600107A")]
		[Address(RVA = "0x10118D46C", Offset = "0x118D46C", VA = "0x10118D46C")]
		public void SetupForResult(bool enableRender, Vector3 pos, float scale)
		{
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600107B")]
		[Address(RVA = "0x10118D6BC", Offset = "0x118D6BC", VA = "0x10118D6BC")]
		public void SetStencilParamForResult(int idx)
		{
		}

		// Token: 0x040013EC RID: 5100
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000E7F")]
		private bool m_SetupedParameter;

		// Token: 0x040013ED RID: 5101
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4000E80")]
		private bool m_SetupedResource;

		// Token: 0x040013EE RID: 5102
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E81")]
		private CharacterHandler m_Owner;

		// Token: 0x040013EF RID: 5103
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E82")]
		private CharacterBattleParam m_Param;

		// Token: 0x040013F0 RID: 5104
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E83")]
		private BattleAIData m_BattleAIData;

		// Token: 0x040013F1 RID: 5105
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E84")]
		private Dictionary<string, SkillActionPlan> m_SAPs;

		// Token: 0x040013F2 RID: 5106
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E85")]
		private Dictionary<string, SkillActionGraphics> m_SAGs;

		// Token: 0x040013F8 RID: 5112
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000E8B")]
		private BattleDefine.eJoinMember[] m_OldSingleTargetIndices;

		// Token: 0x040013F9 RID: 5113
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000E8C")]
		private SkillActionPlayer m_SkillActionPlayer;

		// Token: 0x040013FA RID: 5114
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000E8D")]
		private string m_ActionKey;

		// Token: 0x040013FB RID: 5115
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000E8E")]
		private bool m_IsInUniqueSkillScene;

		// Token: 0x040013FC RID: 5116
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4000E8F")]
		private float m_DelaySec;

		// Token: 0x040013FD RID: 5117
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000E90")]
		private float m_BlendSec;

		// Token: 0x040013FE RID: 5118
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4000E91")]
		private bool m_IsFixRootBoneTransform;

		// Token: 0x040013FF RID: 5119
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000E92")]
		private SimpleTimer m_Timer;

		// Token: 0x04001402 RID: 5122
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000E95")]
		private List<BattleDefine.AttachEffect> m_AttachEffects;

		// Token: 0x04001403 RID: 5123
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000E96")]
		private int m_NormalAttackSkillID;

		// Token: 0x04001404 RID: 5124
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000E97")]
		private List<BattleCommandData> m_Commands;

		// Token: 0x04001405 RID: 5125
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000E98")]
		private List<BattleCommandData> m_UniqueSkillCommands;

		// Token: 0x04001406 RID: 5126
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000E99")]
		private BattleCommandData m_ExecCommand;

		// Token: 0x04001407 RID: 5127
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4000E9A")]
		private int m_TogetherAttackChainCount;

		// Token: 0x04001408 RID: 5128
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4000E9B")]
		private BattlePassiveSkill m_PassiveSkill;

		// Token: 0x04001409 RID: 5129
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4000E9C")]
		private bool m_IsStun;

		// Token: 0x0400140A RID: 5130
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4000E9D")]
		private float m_StunValue;

		// Token: 0x0400140B RID: 5131
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4000E9E")]
		private bool m_IsStunerJudged;

		// Token: 0x0400140C RID: 5132
		[Cpp2IlInjected.FieldOffset(Offset = "0xE9")]
		[Token(Token = "0x4000E9F")]
		private bool m_IsStunerApply;

		// Token: 0x0400140D RID: 5133
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4000EA0")]
		private EffectHandler m_StunOccurEffectHandler;

		// Token: 0x0400140E RID: 5134
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4000EA1")]
		private EffectHandler m_StunEffectHandler;

		// Token: 0x0400140F RID: 5135
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4000EA2")]
		private int stunEffectSortingOrder;

		// Token: 0x04001410 RID: 5136
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4000EA3")]
		private int m_AIPatternIndex;

		// Token: 0x04001411 RID: 5137
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4000EA4")]
		private Dictionary<int, bool> m_AIPatternChangeHitIndices;

		// Token: 0x04001412 RID: 5138
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4000EA5")]
		private List<BattleAIFlag> m_AIFlags;

		// Token: 0x04001413 RID: 5139
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4000EA6")]
		private List<BattleAIFlag> m_AIWriteFlags;

		// Token: 0x04001414 RID: 5140
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4000EA7")]
		private Dictionary<string, int> m_AIExecedNums;

		// Token: 0x04001416 RID: 5142
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4000EA9")]
		private CharacterBattle.eMode m_Mode;

		// Token: 0x04001417 RID: 5143
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4000EAA")]
		private CharacterBattle.eUpdateIdleStep m_UpdateIdleStep;

		// Token: 0x04001418 RID: 5144
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4000EAB")]
		private bool m_IdleIsStunScaleAnim;

		// Token: 0x04001419 RID: 5145
		[Cpp2IlInjected.FieldOffset(Offset = "0x135")]
		[Token(Token = "0x4000EAC")]
		public bool m_IsNeedStunRedraw;

		// Token: 0x0400141A RID: 5146
		[Token(Token = "0x4000EAD")]
		public const float DEFAULT_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x0400141B RID: 5147
		[Token(Token = "0x4000EAE")]
		public const int FACIAL_ID_DEFAULT = 1;

		// Token: 0x0400141C RID: 5148
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4000EAF")]
		private CharacterBattle.eUpdateDamageStep m_UpdateDamageStep;

		// Token: 0x0400141D RID: 5149
		[Token(Token = "0x4000EB0")]
		private const float DAMAGE_TO_IDLE_BLEND_SEC = 0.2f;

		// Token: 0x0400141E RID: 5150
		[Cpp2IlInjected.FieldOffset(Offset = "0x13C")]
		[Token(Token = "0x4000EB1")]
		private CharacterBattle.eUpdateReviveStep m_UpdateReviveStep;

		// Token: 0x0400141F RID: 5151
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4000EB2")]
		private EffectHandler m_ReviveEffectHandler;

		// Token: 0x04001420 RID: 5152
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4000EB3")]
		private CharacterBattle.eUpdateDeadStep m_UpdateDeadStep;

		// Token: 0x04001421 RID: 5153
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4000EB4")]
		private EffectHandler m_DeadEffectHandler;

		// Token: 0x04001422 RID: 5154
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4000EB5")]
		private readonly float[] DEAD_ANIM_TIMESCALES;

		// Token: 0x04001423 RID: 5155
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4000EB6")]
		private readonly int[] DEAD_SCALE_TIMING_FRAMES;

		// Token: 0x04001424 RID: 5156
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4000EB7")]
		private readonly int[] DEAD_SCALE_FRAMES;

		// Token: 0x04001425 RID: 5157
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4000EB8")]
		private float m_DeadScaleRatio;

		// Token: 0x04001426 RID: 5158
		[Cpp2IlInjected.FieldOffset(Offset = "0x174")]
		[Token(Token = "0x4000EB9")]
		private CharacterBattle.eUpdateWinStep m_UpdateWinStep;

		// Token: 0x04001427 RID: 5159
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4000EBA")]
		private bool m_BattleWinIsStartFromLoop;

		// Token: 0x04001428 RID: 5160
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4000EBB")]
		private string m_BattleWinActKey;

		// Token: 0x04001429 RID: 5161
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4000EBC")]
		private int m_BattleWinID;

		// Token: 0x0400142A RID: 5162
		[Cpp2IlInjected.FieldOffset(Offset = "0x18C")]
		[Token(Token = "0x4000EBD")]
		private CharacterBattle.eUpdateCommandStep m_UpdateCommandStep;

		// Token: 0x0400142B RID: 5163
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x4000EBE")]
		private bool m_IsEndCommandPlayingAnim;

		// Token: 0x0400142C RID: 5164
		[Cpp2IlInjected.FieldOffset(Offset = "0x194")]
		[Token(Token = "0x4000EBF")]
		private CharacterBattle.eUpdateInStep m_UpdateInStep;

		// Token: 0x0400142D RID: 5165
		[Token(Token = "0x4000EC0")]
		private const float IN_TO_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x0400142E RID: 5166
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x4000EC1")]
		private CharacterBattle.eUpdateOutStep m_UpdateOutStep;

		// Token: 0x0400142F RID: 5167
		[Cpp2IlInjected.FieldOffset(Offset = "0x19C")]
		[Token(Token = "0x4000EC2")]
		private Vector3 m_OutToPos;

		// Token: 0x04001430 RID: 5168
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4000EC3")]
		private EffectHandler m_SmokeEffectHandler;

		// Token: 0x04001431 RID: 5169
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4000EC4")]
		private bool m_IsToFront;

		// Token: 0x04001432 RID: 5170
		[Token(Token = "0x4000EC5")]
		private const float OUT_SPEED = 4.2f;

		// Token: 0x04001433 RID: 5171
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B4")]
		[Token(Token = "0x4000EC6")]
		private int m_AutoFacialID;

		// Token: 0x04001434 RID: 5172
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x4000EC7")]
		private float m_AutoFacialTimer;

		// Token: 0x04001435 RID: 5173
		[Cpp2IlInjected.FieldOffset(Offset = "0x1BC")]
		[Token(Token = "0x4000EC8")]
		private int m_AutoFacialSaveFacialID;

		// Token: 0x04001436 RID: 5174
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C0")]
		[Token(Token = "0x4000EC9")]
		private bool m_AutoFacialSaveIsEnableBlink;

		// Token: 0x04001437 RID: 5175
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C8")]
		[Token(Token = "0x4000ECA")]
		private Transform m_ShadowTransform;

		// Token: 0x04001438 RID: 5176
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D0")]
		[Token(Token = "0x4000ECB")]
		private Transform m_ShadowRefTransform;

		// Token: 0x04001439 RID: 5177
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D8")]
		[Token(Token = "0x4000ECC")]
		private float m_ShadowOffsetX;

		// Token: 0x0400143A RID: 5178
		[Cpp2IlInjected.FieldOffset(Offset = "0x1DC")]
		[Token(Token = "0x4000ECD")]
		private float m_ShadowBaseOffsetX;

		// Token: 0x0400143B RID: 5179
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E0")]
		[Token(Token = "0x4000ECE")]
		private float m_ShadowOffsetXInIdle;

		// Token: 0x0400143C RID: 5180
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E4")]
		[Token(Token = "0x4000ECF")]
		private float m_ShadowScaleBaseHeight;

		// Token: 0x0400143D RID: 5181
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E8")]
		[Token(Token = "0x4000ED0")]
		private float m_ShadowScaleMax;

		// Token: 0x0400143E RID: 5182
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F0")]
		[Token(Token = "0x4000ED1")]
		private List<Material> m_MaterialList;

		// Token: 0x02000459 RID: 1113
		[Token(Token = "0x2000DBC")]
		public enum eMode
		{
			// Token: 0x04001440 RID: 5184
			[Token(Token = "0x40058EF")]
			None = -1,
			// Token: 0x04001441 RID: 5185
			[Token(Token = "0x40058F0")]
			Idle,
			// Token: 0x04001442 RID: 5186
			[Token(Token = "0x40058F1")]
			Damage,
			// Token: 0x04001443 RID: 5187
			[Token(Token = "0x40058F2")]
			Revive,
			// Token: 0x04001444 RID: 5188
			[Token(Token = "0x40058F3")]
			Dead,
			// Token: 0x04001445 RID: 5189
			[Token(Token = "0x40058F4")]
			Command,
			// Token: 0x04001446 RID: 5190
			[Token(Token = "0x40058F5")]
			Win,
			// Token: 0x04001447 RID: 5191
			[Token(Token = "0x40058F6")]
			In,
			// Token: 0x04001448 RID: 5192
			[Token(Token = "0x40058F7")]
			Out
		}

		// Token: 0x0200045A RID: 1114
		[Token(Token = "0x2000DBD")]
		private enum eUpdateIdleStep
		{
			// Token: 0x0400144A RID: 5194
			[Token(Token = "0x40058F9")]
			None = -1,
			// Token: 0x0400144B RID: 5195
			[Token(Token = "0x40058FA")]
			ExecAnimation,
			// Token: 0x0400144C RID: 5196
			[Token(Token = "0x40058FB")]
			ExecAnimation_Wait,
			// Token: 0x0400144D RID: 5197
			[Token(Token = "0x40058FC")]
			End
		}

		// Token: 0x0200045B RID: 1115
		[Token(Token = "0x2000DBE")]
		private enum eUpdateDamageStep
		{
			// Token: 0x0400144F RID: 5199
			[Token(Token = "0x40058FE")]
			None = -1,
			// Token: 0x04001450 RID: 5200
			[Token(Token = "0x40058FF")]
			ExecAnimation,
			// Token: 0x04001451 RID: 5201
			[Token(Token = "0x4005900")]
			ExecAnimation_Wait,
			// Token: 0x04001452 RID: 5202
			[Token(Token = "0x4005901")]
			End
		}

		// Token: 0x0200045C RID: 1116
		[Token(Token = "0x2000DBF")]
		private enum eUpdateReviveStep
		{
			// Token: 0x04001454 RID: 5204
			[Token(Token = "0x4005903")]
			None = -1,
			// Token: 0x04001455 RID: 5205
			[Token(Token = "0x4005904")]
			ExecAnimation,
			// Token: 0x04001456 RID: 5206
			[Token(Token = "0x4005905")]
			ExecAnimation_Wait,
			// Token: 0x04001457 RID: 5207
			[Token(Token = "0x4005906")]
			End
		}

		// Token: 0x0200045D RID: 1117
		[Token(Token = "0x2000DC0")]
		private enum eUpdateDeadStep
		{
			// Token: 0x04001459 RID: 5209
			[Token(Token = "0x4005908")]
			None = -1,
			// Token: 0x0400145A RID: 5210
			[Token(Token = "0x4005909")]
			ExecAnimation,
			// Token: 0x0400145B RID: 5211
			[Token(Token = "0x400590A")]
			ExecAnimation_Wait,
			// Token: 0x0400145C RID: 5212
			[Token(Token = "0x400590B")]
			End
		}

		// Token: 0x0200045E RID: 1118
		[Token(Token = "0x2000DC1")]
		private enum eUpdateWinStep
		{
			// Token: 0x0400145E RID: 5214
			[Token(Token = "0x400590D")]
			None = -1,
			// Token: 0x0400145F RID: 5215
			[Token(Token = "0x400590E")]
			ExecAnimation,
			// Token: 0x04001460 RID: 5216
			[Token(Token = "0x400590F")]
			ExecAnimation_Wait,
			// Token: 0x04001461 RID: 5217
			[Token(Token = "0x4005910")]
			End
		}

		// Token: 0x0200045F RID: 1119
		[Token(Token = "0x2000DC2")]
		private enum eUpdateCommandStep
		{
			// Token: 0x04001463 RID: 5219
			[Token(Token = "0x4005912")]
			None = -1,
			// Token: 0x04001464 RID: 5220
			[Token(Token = "0x4005913")]
			ExecAnimation,
			// Token: 0x04001465 RID: 5221
			[Token(Token = "0x4005914")]
			ExecAnimation_Wait,
			// Token: 0x04001466 RID: 5222
			[Token(Token = "0x4005915")]
			End
		}

		// Token: 0x02000460 RID: 1120
		[Token(Token = "0x2000DC3")]
		private enum eUpdateInStep
		{
			// Token: 0x04001468 RID: 5224
			[Token(Token = "0x4005917")]
			None = -1,
			// Token: 0x04001469 RID: 5225
			[Token(Token = "0x4005918")]
			ExecAnimation,
			// Token: 0x0400146A RID: 5226
			[Token(Token = "0x4005919")]
			ExecAnimation_Wait,
			// Token: 0x0400146B RID: 5227
			[Token(Token = "0x400591A")]
			End
		}

		// Token: 0x02000461 RID: 1121
		[Token(Token = "0x2000DC4")]
		private enum eUpdateOutStep
		{
			// Token: 0x0400146D RID: 5229
			[Token(Token = "0x400591C")]
			None = -1,
			// Token: 0x0400146E RID: 5230
			[Token(Token = "0x400591D")]
			ExecAnimation,
			// Token: 0x0400146F RID: 5231
			[Token(Token = "0x400591E")]
			ExecAnimation_Wait,
			// Token: 0x04001470 RID: 5232
			[Token(Token = "0x400591F")]
			End
		}
	}
}
