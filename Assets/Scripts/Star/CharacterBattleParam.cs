﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000462 RID: 1122
	[Token(Token = "0x2000371")]
	[Serializable]
	[StructLayout(3)]
	public class CharacterBattleParam
	{
		// Token: 0x060011C1 RID: 4545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600107C")]
		[Address(RVA = "0x101182834", Offset = "0x1182834", VA = "0x101182834")]
		public CharacterBattleParam()
		{
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600107D")]
		[Address(RVA = "0x101182924", Offset = "0x1182924", VA = "0x101182924")]
		public void SetupParams(CharacterHandler owner, float[] townBuffParams, int orbID, int orbLv)
		{
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600107E")]
		[Address(RVA = "0x10118D900", Offset = "0x118D900", VA = "0x10118D900")]
		public void SetPoint(long point)
		{
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x00007698 File Offset: 0x00005898
		[Token(Token = "0x600107F")]
		[Address(RVA = "0x10118D908", Offset = "0x118D908", VA = "0x10118D908")]
		public long GetPoint()
		{
			return 0L;
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x000076B0 File Offset: 0x000058B0
		[Token(Token = "0x6001080")]
		[Address(RVA = "0x10118D910", Offset = "0x118D910", VA = "0x10118D910")]
		public int FixedMaxHp()
		{
			return 0;
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x000076C8 File Offset: 0x000058C8
		[Token(Token = "0x6001081")]
		[Address(RVA = "0x10118D918", Offset = "0x118D918", VA = "0x10118D918")]
		public int FixedAtk(bool isPlayer)
		{
			return 0;
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x000076E0 File Offset: 0x000058E0
		[Token(Token = "0x6001082")]
		[Address(RVA = "0x10118DD9C", Offset = "0x118DD9C", VA = "0x10118DD9C")]
		public int FixedMgc(bool isPlayer)
		{
			return 0;
		}

		// Token: 0x060011C8 RID: 4552 RVA: 0x000076F8 File Offset: 0x000058F8
		[Token(Token = "0x6001083")]
		[Address(RVA = "0x10118E0B0", Offset = "0x118E0B0", VA = "0x10118E0B0")]
		public int FixedDef(bool isPlayer)
		{
			return 0;
		}

		// Token: 0x060011C9 RID: 4553 RVA: 0x00007710 File Offset: 0x00005910
		[Token(Token = "0x6001084")]
		[Address(RVA = "0x10118E3A8", Offset = "0x118E3A8", VA = "0x10118E3A8")]
		public int FixedMDef(bool isPlayer)
		{
			return 0;
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x00007728 File Offset: 0x00005928
		[Token(Token = "0x6001085")]
		[Address(RVA = "0x10118E6A0", Offset = "0x118E6A0", VA = "0x10118E6A0")]
		public int FixedSpd()
		{
			return 0;
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x00007740 File Offset: 0x00005940
		[Token(Token = "0x6001086")]
		[Address(RVA = "0x10118E6A8", Offset = "0x118E6A8", VA = "0x10118E6A8")]
		public int FixedLuck(bool isPlayer, bool ignoreCoef)
		{
			return 0;
		}

		// Token: 0x060011CC RID: 4556 RVA: 0x00007758 File Offset: 0x00005958
		[Token(Token = "0x6001087")]
		[Address(RVA = "0x10118E9AC", Offset = "0x118E9AC", VA = "0x10118E9AC")]
		public float GetOrderCoef()
		{
			return 0f;
		}

		// Token: 0x060011CD RID: 4557 RVA: 0x00007770 File Offset: 0x00005970
		[Token(Token = "0x6001088")]
		[Address(RVA = "0x10118EBF4", Offset = "0x118EBF4", VA = "0x10118EBF4")]
		public int GetFixedStatus(BattleDefine.eStatus st, bool isPlayer)
		{
			return 0;
		}

		// Token: 0x060011CE RID: 4558 RVA: 0x00007788 File Offset: 0x00005988
		[Token(Token = "0x6001089")]
		[Address(RVA = "0x10118EC54", Offset = "0x118EC54", VA = "0x10118EC54")]
		public int GetHp()
		{
			return 0;
		}

		// Token: 0x060011CF RID: 4559 RVA: 0x000077A0 File Offset: 0x000059A0
		[Token(Token = "0x600108A")]
		[Address(RVA = "0x10118EC5C", Offset = "0x118EC5C", VA = "0x10118EC5C")]
		public int GetOverRecoveredMaxHp()
		{
			return 0;
		}

		// Token: 0x060011D0 RID: 4560 RVA: 0x000077B8 File Offset: 0x000059B8
		[Token(Token = "0x600108B")]
		[Address(RVA = "0x10118EC70", Offset = "0x118EC70", VA = "0x10118EC70")]
		public int GetMaxOverLimitHp(CharacterBattleParam executorParam, BattleCommandData.eCommandType cType)
		{
			return 0;
		}

		// Token: 0x060011D1 RID: 4561 RVA: 0x000077D0 File Offset: 0x000059D0
		[Token(Token = "0x600108C")]
		[Address(RVA = "0x10118ED64", Offset = "0x118ED64", VA = "0x10118ED64")]
		public bool IsGreaterEqualHpThreshold(float threshold)
		{
			return default(bool);
		}

		// Token: 0x060011D2 RID: 4562 RVA: 0x000077E8 File Offset: 0x000059E8
		[Token(Token = "0x600108D")]
		[Address(RVA = "0x10118BB74", Offset = "0x118BB74", VA = "0x10118BB74")]
		public float GetHpRatio()
		{
			return 0f;
		}

		// Token: 0x060011D3 RID: 4563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600108E")]
		[Address(RVA = "0x10118ED8C", Offset = "0x118ED8C", VA = "0x10118ED8C")]
		public void SetHp(int hp)
		{
		}

		// Token: 0x060011D4 RID: 4564 RVA: 0x00007800 File Offset: 0x00005A00
		[Token(Token = "0x600108F")]
		[Address(RVA = "0x10118ED94", Offset = "0x118ED94", VA = "0x10118ED94")]
		public int CalcHp(int val, bool isOverRecover, float overRecoverLimit)
		{
			return 0;
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x00007818 File Offset: 0x00005A18
		[Token(Token = "0x6001090")]
		[Address(RVA = "0x1011852B8", Offset = "0x11852B8", VA = "0x1011852B8")]
		public int RecoveryFullHp()
		{
			return 0;
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x00007830 File Offset: 0x00005A30
		[Token(Token = "0x6001091")]
		[Address(RVA = "0x10118EE74", Offset = "0x118EE74", VA = "0x10118EE74")]
		public bool IsHpRange(float minRatio, float maxRatio)
		{
			return default(bool);
		}

		// Token: 0x060011D7 RID: 4567 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001092")]
		[Address(RVA = "0x10118EEB4", Offset = "0x118EEB4", VA = "0x10118EEB4")]
		public float[] GetTownBuffParams()
		{
			return null;
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x00007848 File Offset: 0x00005A48
		[Token(Token = "0x6001093")]
		[Address(RVA = "0x10118EEBC", Offset = "0x118EEBC", VA = "0x10118EEBC")]
		public int GetChargeCount()
		{
			return 0;
		}

		// Token: 0x060011D9 RID: 4569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001094")]
		[Address(RVA = "0x101186BD0", Offset = "0x1186BD0", VA = "0x101186BD0")]
		public void SetChargeCount(int chargeCount)
		{
		}

		// Token: 0x060011DA RID: 4570 RVA: 0x00007860 File Offset: 0x00005A60
		[Token(Token = "0x6001095")]
		[Address(RVA = "0x10118EEC4", Offset = "0x118EEC4", VA = "0x10118EEC4")]
		public int GetChargeCountMax()
		{
			return 0;
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x00007878 File Offset: 0x00005A78
		[Token(Token = "0x6001096")]
		[Address(RVA = "0x101186BD8", Offset = "0x1186BD8", VA = "0x101186BD8")]
		public int CalcChargeCount(int val)
		{
			return 0;
		}

		// Token: 0x060011DC RID: 4572 RVA: 0x00007890 File Offset: 0x00005A90
		[Token(Token = "0x6001097")]
		[Address(RVA = "0x10118EF0C", Offset = "0x118EF0C", VA = "0x10118EF0C")]
		public bool IsChargeCountMax()
		{
			return default(bool);
		}

		// Token: 0x060011DD RID: 4573 RVA: 0x000078A8 File Offset: 0x00005AA8
		[Token(Token = "0x6001098")]
		[Address(RVA = "0x101185BD8", Offset = "0x1185BD8", VA = "0x101185BD8")]
		public bool IsDead()
		{
			return default(bool);
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x000078C0 File Offset: 0x00005AC0
		[Token(Token = "0x6001099")]
		[Address(RVA = "0x10118EF34", Offset = "0x118EF34", VA = "0x10118EF34")]
		public BattleDefine.eDeadDetail GetDeadDetail()
		{
			return BattleDefine.eDeadDetail.Alive;
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600109A")]
		[Address(RVA = "0x1011852B0", Offset = "0x11852B0", VA = "0x1011852B0")]
		public void SetDeadDetail(BattleDefine.eDeadDetail deadDetail)
		{
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600109B")]
		[Address(RVA = "0x1011864EC", Offset = "0x11864EC", VA = "0x1011864EC")]
		public void UpdateDeadDetailIfReady()
		{
		}

		// Token: 0x060011E1 RID: 4577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600109C")]
		[Address(RVA = "0x1011857CC", Offset = "0x11857CC", VA = "0x1011857CC")]
		public void SetWasAttacked(bool flg)
		{
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x000078D8 File Offset: 0x00005AD8
		[Token(Token = "0x600109D")]
		[Address(RVA = "0x101186910", Offset = "0x1186910", VA = "0x101186910")]
		public bool WasAttacked()
		{
			return default(bool);
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600109E")]
		[Address(RVA = "0x1011857D4", Offset = "0x11857D4", VA = "0x1011857D4")]
		public void SetWasAttackedOnSleeping(bool flg)
		{
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x000078F0 File Offset: 0x00005AF0
		[Token(Token = "0x600109F")]
		[Address(RVA = "0x10118EF3C", Offset = "0x118EF3C", VA = "0x10118EF3C")]
		public bool WasAttackedOnSleeping()
		{
			return default(bool);
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010A0")]
		[Address(RVA = "0x10118EF44", Offset = "0x118EF44", VA = "0x10118EF44")]
		public void SetElementResist(eElementType elementType, eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010A1")]
		[Address(RVA = "0x101187AA4", Offset = "0x1187AA4", VA = "0x101187AA4")]
		public BattleDefine.BuffStack GetElementResist(eElementType elementType)
		{
			return null;
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x00007908 File Offset: 0x00005B08
		[Token(Token = "0x60010A2")]
		[Address(RVA = "0x10118F024", Offset = "0x118F024", VA = "0x10118F024")]
		public float GetElementResistValue(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x00007920 File Offset: 0x00005B20
		[Token(Token = "0x60010A3")]
		[Address(RVA = "0x10118F098", Offset = "0x118F098", VA = "0x10118F098")]
		public float GetDefaultEmentCoef(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x00007938 File Offset: 0x00005B38
		[Token(Token = "0x60010A4")]
		[Address(RVA = "0x10118F0E8", Offset = "0x118F0E8", VA = "0x10118F0E8")]
		public float GetElementResistTownBuffValue(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x060011EA RID: 4586 RVA: 0x00007950 File Offset: 0x00005B50
		[Token(Token = "0x60010A5")]
		[Address(RVA = "0x10118F148", Offset = "0x118F148", VA = "0x10118F148")]
		public float GetFixedElementCoef(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010A6")]
		[Address(RVA = "0x10118F304", Offset = "0x118F304", VA = "0x10118F304")]
		public float[] GetFixedElementCoefs()
		{
			return null;
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010A7")]
		[Address(RVA = "0x10118F3CC", Offset = "0x118F3CC", VA = "0x10118F3CC")]
		public void ResetElementResist(eElementType elementType)
		{
		}

		// Token: 0x060011ED RID: 4589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010A8")]
		[Address(RVA = "0x1011852C8", Offset = "0x11852C8", VA = "0x1011852C8")]
		public void ResetElementResistAll()
		{
		}

		// Token: 0x060011EE RID: 4590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010A9")]
		[Address(RVA = "0x10118659C", Offset = "0x118659C", VA = "0x10118659C")]
		public void DecreaseTurnElementResistAll(bool isMyTurn)
		{
		}

		// Token: 0x060011EF RID: 4591 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010AA")]
		[Address(RVA = "0x10118F43C", Offset = "0x118F43C", VA = "0x10118F43C")]
		public int[] GetElementResistFlags()
		{
			return null;
		}

		// Token: 0x060011F0 RID: 4592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010AB")]
		[Address(RVA = "0x10118F54C", Offset = "0x118F54C", VA = "0x10118F54C")]
		public void ElementChange(eElementType elementType)
		{
		}

		// Token: 0x060011F1 RID: 4593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010AC")]
		[Address(RVA = "0x10118F5E8", Offset = "0x118F5E8", VA = "0x10118F5E8")]
		public void SetStatusBuff(BattleDefine.eStatus status, eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x060011F2 RID: 4594 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010AD")]
		[Address(RVA = "0x101187A54", Offset = "0x1187A54", VA = "0x101187A54")]
		public BattleDefine.BuffStack GetStatusBuff(BattleDefine.eStatus status)
		{
			return null;
		}

		// Token: 0x060011F3 RID: 4595 RVA: 0x00007968 File Offset: 0x00005B68
		[Token(Token = "0x60010AE")]
		[Address(RVA = "0x10118EA18", Offset = "0x118EA18", VA = "0x10118EA18")]
		public float GetStatusBuffValue(BattleDefine.eStatus status)
		{
			return 0f;
		}

		// Token: 0x060011F4 RID: 4596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010AF")]
		[Address(RVA = "0x10118F6C8", Offset = "0x118F6C8", VA = "0x10118F6C8")]
		public void ResetStatusBuff(BattleDefine.eStatus status)
		{
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010B0")]
		[Address(RVA = "0x10118F738", Offset = "0x118F738", VA = "0x10118F738")]
		public void ResetStatusBuff(BattleDefine.eStatus status, bool isBuff)
		{
		}

		// Token: 0x060011F6 RID: 4598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010B1")]
		[Address(RVA = "0x101185360", Offset = "0x1185360", VA = "0x101185360")]
		public void ResetStatusBuffAll()
		{
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010B2")]
		[Address(RVA = "0x10118663C", Offset = "0x118663C", VA = "0x10118663C")]
		public void DecreaseTurnStatusBuffAll(bool isMyTurn)
		{
		}

		// Token: 0x060011F8 RID: 4600 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010B3")]
		[Address(RVA = "0x10118F84C", Offset = "0x118F84C", VA = "0x10118F84C")]
		public int[] GetStatusBuffFlags()
		{
			return null;
		}

		// Token: 0x060011F9 RID: 4601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010B4")]
		[Address(RVA = "0x10118F9F4", Offset = "0x118F9F4", VA = "0x10118F9F4")]
		public void SetStateAbnormal(eStateAbnormal stateAbnormal, int turn)
		{
		}

		// Token: 0x060011FA RID: 4602 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010B5")]
		[Address(RVA = "0x10118FB00", Offset = "0x118FB00", VA = "0x10118FB00")]
		public BattleDefine.StateAbnormal GetStateAbnormal(eStateAbnormal stateAbnormal)
		{
			return null;
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x00007980 File Offset: 0x00005B80
		[Token(Token = "0x60010B6")]
		[Address(RVA = "0x101186504", Offset = "0x1186504", VA = "0x101186504")]
		public bool IsEnableStateAbnormalAny()
		{
			return default(bool);
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x00007998 File Offset: 0x00005B98
		[Token(Token = "0x60010B7")]
		[Address(RVA = "0x101187DDC", Offset = "0x1187DDC", VA = "0x101187DDC")]
		public bool IsEnableStateAbnormal(eStateAbnormal stateAbnormal)
		{
			return default(bool);
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x000079B0 File Offset: 0x00005BB0
		[Token(Token = "0x60010B8")]
		[Address(RVA = "0x10118FB50", Offset = "0x118FB50", VA = "0x10118FB50")]
		public bool IsEnableStateAbnormalCannotAct()
		{
			return default(bool);
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010B9")]
		[Address(RVA = "0x1011867D4", Offset = "0x11867D4", VA = "0x1011867D4")]
		public void ResetStateAbnormal(eStateAbnormal stateAbnormal)
		{
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010BA")]
		[Address(RVA = "0x1011854D0", Offset = "0x11854D0", VA = "0x1011854D0")]
		public void ResetStateAbnormalAll()
		{
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010BB")]
		[Address(RVA = "0x1011866DC", Offset = "0x11866DC", VA = "0x1011866DC")]
		public void DecreaseTurnStateAbnormalAll(bool isMyTurn)
		{
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x000079C8 File Offset: 0x00005BC8
		[Token(Token = "0x60010BC")]
		[Address(RVA = "0x1011867A4", Offset = "0x11867A4", VA = "0x1011867A4")]
		public bool CanPossibleCancelSleep()
		{
			return default(bool);
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010BD")]
		[Address(RVA = "0x10118FC54", Offset = "0x118FC54", VA = "0x10118FC54")]
		public void SetStateAbnormalDisableBuff(eSkillTurnConsume turnConsume, int turn)
		{
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010BE")]
		[Address(RVA = "0x10118677C", Offset = "0x118677C", VA = "0x10118677C")]
		public BattleDefine.BuffStack GetStateAbnormalDisableBuff()
		{
			return null;
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x000079E0 File Offset: 0x00005BE0
		[Token(Token = "0x60010BF")]
		[Address(RVA = "0x101187B10", Offset = "0x1187B10", VA = "0x101187B10")]
		public bool IsStateAbnormalDisableBuff()
		{
			return default(bool);
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010C0")]
		[Address(RVA = "0x101185568", Offset = "0x1185568", VA = "0x101185568")]
		public void ResetStateAbnormalDisableBuff()
		{
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x000079F8 File Offset: 0x00005BF8
		[Token(Token = "0x60010C1")]
		[Address(RVA = "0x10118FCE8", Offset = "0x118FCE8", VA = "0x10118FCE8")]
		public bool IsStateAbnormalDisable()
		{
			return default(bool);
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010C2")]
		[Address(RVA = "0x10118798C", Offset = "0x118798C", VA = "0x10118798C")]
		public bool[] GetStateAbnormalFlags()
		{
			return null;
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010C3")]
		[Address(RVA = "0x10118FD30", Offset = "0x118FD30", VA = "0x10118FD30")]
		public void SetStsBuffDisableBuff(eSkillTurnConsume turnConsume, int turn, bool isBuff)
		{
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010C4")]
		[Address(RVA = "0x101186784", Offset = "0x1186784", VA = "0x101186784")]
		public BattleDefine.BuffStack GetStsBuffDisableBuff(bool isBuff)
		{
			return null;
		}

		// Token: 0x0600120A RID: 4618 RVA: 0x00007A10 File Offset: 0x00005C10
		[Token(Token = "0x60010C5")]
		[Address(RVA = "0x10118FDE0", Offset = "0x118FDE0", VA = "0x10118FDE0")]
		public bool IsEnableStsBuffDisableBuff(bool isBuff)
		{
			return default(bool);
		}

		// Token: 0x0600120B RID: 4619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010C6")]
		[Address(RVA = "0x1011853F8", Offset = "0x11853F8", VA = "0x1011853F8")]
		public void ResetStsBuffDisableBuff(bool isBuff)
		{
		}

		// Token: 0x0600120C RID: 4620 RVA: 0x00007A28 File Offset: 0x00005C28
		[Token(Token = "0x60010C7")]
		[Address(RVA = "0x101187B50", Offset = "0x1187B50", VA = "0x101187B50")]
		public bool IsStsBuffDisable(bool isBuff)
		{
			return default(bool);
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x00007A40 File Offset: 0x00005C40
		[Token(Token = "0x60010C8")]
		[Address(RVA = "0x10118FE30", Offset = "0x118FE30", VA = "0x10118FE30")]
		public bool IsStateStunDisable()
		{
			return default(bool);
		}

		// Token: 0x0600120E RID: 4622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010C9")]
		[Address(RVA = "0x10118FE48", Offset = "0x118FE48", VA = "0x10118FE48")]
		public void SetStateAbnormalAddProbabilityBuff(eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x0600120F RID: 4623 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010CA")]
		[Address(RVA = "0x10118679C", Offset = "0x118679C", VA = "0x10118679C")]
		public BattleDefine.BuffStack GetStateAbnormalAddProbabilityBuff()
		{
			return null;
		}

		// Token: 0x06001210 RID: 4624 RVA: 0x00007A58 File Offset: 0x00005C58
		[Token(Token = "0x60010CB")]
		[Address(RVA = "0x10118FEE8", Offset = "0x118FEE8", VA = "0x10118FEE8")]
		public float GetStateAbnormalAddProbabilityBuffValue()
		{
			return 0f;
		}

		// Token: 0x06001211 RID: 4625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010CC")]
		[Address(RVA = "0x101185598", Offset = "0x1185598", VA = "0x101185598")]
		public void ResetStateAbnormalAddProbabilityBuff()
		{
		}

		// Token: 0x06001212 RID: 4626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010CD")]
		[Address(RVA = "0x10118FF1C", Offset = "0x118FF1C", VA = "0x10118FF1C")]
		public void SetNextBuff(BattleDefine.eNextBuff nextBuff, float value)
		{
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010CE")]
		[Address(RVA = "0x10118FFDC", Offset = "0x118FFDC", VA = "0x10118FFDC")]
		public BattleDefine.NextBuff GetNextBuff(BattleDefine.eNextBuff nextBuff)
		{
			return null;
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x00007A70 File Offset: 0x00005C70
		[Token(Token = "0x60010CF")]
		[Address(RVA = "0x101187BE4", Offset = "0x1187BE4", VA = "0x101187BE4")]
		public float GetNextBuffCoef(BattleDefine.eNextBuff nextBuff)
		{
			return 0f;
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D0")]
		[Address(RVA = "0x10119002C", Offset = "0x119002C", VA = "0x10119002C")]
		public void ResetNextBuff(BattleDefine.eNextBuff nextBuff)
		{
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D1")]
		[Address(RVA = "0x101185438", Offset = "0x1185438", VA = "0x101185438")]
		public void ResetNextBuffAll()
		{
		}

		// Token: 0x06001217 RID: 4631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D2")]
		[Address(RVA = "0x10119009C", Offset = "0x119009C", VA = "0x10119009C")]
		public void SetUsedNextBuffIfAvailable(BattleDefine.eNextBuff nextBuff)
		{
		}

		// Token: 0x06001218 RID: 4632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D3")]
		[Address(RVA = "0x101186940", Offset = "0x1186940", VA = "0x101186940")]
		public void ResetNextBuffIfUsed()
		{
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D4")]
		[Address(RVA = "0x10119010C", Offset = "0x119010C", VA = "0x10119010C")]
		public void SetWeakElementBonusBuff(eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x0600121A RID: 4634 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010D5")]
		[Address(RVA = "0x1011869D8", Offset = "0x11869D8", VA = "0x1011869D8")]
		public BattleDefine.BuffStack GetWeakElementBonusBuff()
		{
			return null;
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x00007A88 File Offset: 0x00005C88
		[Token(Token = "0x60010D6")]
		[Address(RVA = "0x101187B5C", Offset = "0x1187B5C", VA = "0x101187B5C")]
		public float GetWeakElementBonusBuffValue()
		{
			return 0f;
		}

		// Token: 0x0600121C RID: 4636 RVA: 0x00007AA0 File Offset: 0x00005CA0
		[Token(Token = "0x60010D7")]
		[Address(RVA = "0x1011901AC", Offset = "0x11901AC", VA = "0x1011901AC")]
		public float GetFixedWeakElementBonus()
		{
			return 0f;
		}

		// Token: 0x0600121D RID: 4637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D8")]
		[Address(RVA = "0x1011855C8", Offset = "0x11855C8", VA = "0x1011855C8")]
		public void ResetWeakElementBonusBuff()
		{
		}

		// Token: 0x0600121E RID: 4638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010D9")]
		[Address(RVA = "0x1011901B0", Offset = "0x11901B0", VA = "0x1011901B0")]
		public void SetCriticalDamageBuff(eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x0600121F RID: 4639 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010DA")]
		[Address(RVA = "0x1011869E0", Offset = "0x11869E0", VA = "0x1011869E0")]
		public BattleDefine.BuffStack GetCriticalDamageBuff()
		{
			return null;
		}

		// Token: 0x06001220 RID: 4640 RVA: 0x00007AB8 File Offset: 0x00005CB8
		[Token(Token = "0x60010DB")]
		[Address(RVA = "0x101190250", Offset = "0x1190250", VA = "0x101190250")]
		public float GetCriticalDamageBuffValue()
		{
			return 0f;
		}

		// Token: 0x06001221 RID: 4641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010DC")]
		[Address(RVA = "0x1011855F8", Offset = "0x11855F8", VA = "0x1011855F8")]
		public void ResetCriticalDamageBuff()
		{
		}

		// Token: 0x06001222 RID: 4642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010DD")]
		[Address(RVA = "0x1011902E8", Offset = "0x11902E8", VA = "0x1011902E8")]
		public void SetHateChange(eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x06001223 RID: 4643 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010DE")]
		[Address(RVA = "0x1011869E8", Offset = "0x11869E8", VA = "0x1011869E8")]
		public BattleDefine.BuffStack GetHateChange()
		{
			return null;
		}

		// Token: 0x06001224 RID: 4644 RVA: 0x00007AD0 File Offset: 0x00005CD0
		[Token(Token = "0x60010DF")]
		[Address(RVA = "0x101190388", Offset = "0x1190388", VA = "0x101190388")]
		public float GetHateChangeValue()
		{
			return 0f;
		}

		// Token: 0x06001225 RID: 4645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010E0")]
		[Address(RVA = "0x101185628", Offset = "0x1185628", VA = "0x101185628")]
		public void ResetHateChange()
		{
		}

		// Token: 0x06001226 RID: 4646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010E1")]
		[Address(RVA = "0x1011904EC", Offset = "0x11904EC", VA = "0x1011904EC")]
		public void SetRegene(string ownerName, int turn, int power)
		{
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010E2")]
		[Address(RVA = "0x1011869F0", Offset = "0x11869F0", VA = "0x1011869F0")]
		public BattleDefine.RegeneData GetRegene()
		{
			return null;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x00007AE8 File Offset: 0x00005CE8
		[Token(Token = "0x60010E3")]
		[Address(RVA = "0x10119056C", Offset = "0x119056C", VA = "0x10119056C")]
		public float GetRegenePower()
		{
			return 0f;
		}

		// Token: 0x06001229 RID: 4649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010E4")]
		[Address(RVA = "0x101185658", Offset = "0x1185658", VA = "0x101185658")]
		public void ResetRegene()
		{
		}

		// Token: 0x0600122A RID: 4650 RVA: 0x00007B00 File Offset: 0x00005D00
		[Token(Token = "0x60010E5")]
		[Address(RVA = "0x10119059C", Offset = "0x119059C", VA = "0x10119059C")]
		public bool IsExistRevive()
		{
			return default(bool);
		}

		// Token: 0x0600122B RID: 4651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010E6")]
		[Address(RVA = "0x101190638", Offset = "0x1190638", VA = "0x101190638")]
		public void AddRevive(BattleDefine.eReviveRecoverType type, float recover)
		{
		}

		// Token: 0x0600122C RID: 4652 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010E7")]
		[Address(RVA = "0x1011906E4", Offset = "0x11906E4", VA = "0x1011906E4")]
		public BattleDefine.ReviveData GetRevive()
		{
			return null;
		}

		// Token: 0x0600122D RID: 4653 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60010E8")]
		[Address(RVA = "0x101190768", Offset = "0x1190768", VA = "0x101190768")]
		public List<BattleDefine.ReviveData> GetRevives()
		{
			return null;
		}

		// Token: 0x0600122E RID: 4654 RVA: 0x00007B18 File Offset: 0x00005D18
		[Token(Token = "0x60010E9")]
		[Address(RVA = "0x10118BB9C", Offset = "0x118BB9C", VA = "0x10118BB9C")]
		public int ApplyRevive()
		{
			return 0;
		}

		// Token: 0x0600122F RID: 4655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010EA")]
		[Address(RVA = "0x101185688", Offset = "0x1185688", VA = "0x101185688")]
		public void ResetRevive()
		{
		}

		// Token: 0x06001230 RID: 4656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010EB")]
		[Address(RVA = "0x101190770", Offset = "0x1190770", VA = "0x101190770")]
		public void SetBarrier(float cutRatio, int count)
		{
		}

		// Token: 0x06001231 RID: 4657 RVA: 0x00007B30 File Offset: 0x00005D30
		[Token(Token = "0x60010EC")]
		[Address(RVA = "0x101187AF4", Offset = "0x1187AF4", VA = "0x101187AF4")]
		public float GetBarrierCutRatio()
		{
			return 0f;
		}

		// Token: 0x06001232 RID: 4658 RVA: 0x00007B48 File Offset: 0x00005D48
		[Token(Token = "0x60010ED")]
		[Address(RVA = "0x10119077C", Offset = "0x119077C", VA = "0x10119077C")]
		public int GetBarrierCount()
		{
			return 0;
		}

		// Token: 0x06001233 RID: 4659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010EE")]
		[Address(RVA = "0x101186918", Offset = "0x1186918", VA = "0x101186918")]
		public void DecreaseBarrier()
		{
		}

		// Token: 0x06001234 RID: 4660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010EF")]
		[Address(RVA = "0x101185690", Offset = "0x1185690", VA = "0x101185690")]
		public void ResetBarrier()
		{
		}

		// Token: 0x06001235 RID: 4661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F0")]
		[Address(RVA = "0x101190784", Offset = "0x1190784", VA = "0x101190784")]
		public void SetLoadFactorReduceRate(eSkillTurnConsume turnConsume, float reduceRatio, int turn)
		{
		}

		// Token: 0x06001236 RID: 4662 RVA: 0x00007B60 File Offset: 0x00005D60
		[Token(Token = "0x60010F1")]
		[Address(RVA = "0x101190790", Offset = "0x1190790", VA = "0x101190790")]
		public float GetLoadFactorReduceRate()
		{
			return 0f;
		}

		// Token: 0x06001237 RID: 4663 RVA: 0x00007B78 File Offset: 0x00005D78
		[Token(Token = "0x60010F2")]
		[Address(RVA = "0x101187B54", Offset = "0x1187B54", VA = "0x101187B54")]
		public int GetLoadFactorReduceCount()
		{
			return 0;
		}

		// Token: 0x06001238 RID: 4664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F3")]
		[Address(RVA = "0x1011869F8", Offset = "0x11869F8", VA = "0x1011869F8")]
		public void DecreaseLoadFactorReduce(bool isMyTurn)
		{
		}

		// Token: 0x06001239 RID: 4665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F4")]
		[Address(RVA = "0x101185698", Offset = "0x1185698", VA = "0x101185698")]
		public void ResetLoadFactorReduce()
		{
		}

		// Token: 0x0600123A RID: 4666 RVA: 0x00007B90 File Offset: 0x00005D90
		[Token(Token = "0x60010F5")]
		[Address(RVA = "0x101190798", Offset = "0x1190798", VA = "0x101190798")]
		public bool IsGuard()
		{
			return default(bool);
		}

		// Token: 0x0600123B RID: 4667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F6")]
		[Address(RVA = "0x1011907A0", Offset = "0x11907A0", VA = "0x1011907A0")]
		public void GuardOn()
		{
		}

		// Token: 0x0600123C RID: 4668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F7")]
		[Address(RVA = "0x1011856D4", Offset = "0x11856D4", VA = "0x1011856D4")]
		public void GuardOff()
		{
		}

		// Token: 0x0600123D RID: 4669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F8")]
		[Address(RVA = "0x1011907AC", Offset = "0x11907AC", VA = "0x1011907AC")]
		public void MemberChangeRecastFull()
		{
		}

		// Token: 0x0600123E RID: 4670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010F9")]
		[Address(RVA = "0x1011856DC", Offset = "0x11856DC", VA = "0x1011856DC")]
		public void MemberChangeRecastZero()
		{
		}

		// Token: 0x0600123F RID: 4671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60010FA")]
		[Address(RVA = "0x101190820", Offset = "0x1190820", VA = "0x101190820")]
		public void MemberChangeRecastDecrement()
		{
		}

		// Token: 0x06001240 RID: 4672 RVA: 0x00007BA8 File Offset: 0x00005DA8
		[Token(Token = "0x60010FB")]
		[Address(RVA = "0x101190898", Offset = "0x1190898", VA = "0x101190898")]
		public int GetMemberChangeRecast()
		{
			return 0;
		}

		// Token: 0x06001241 RID: 4673 RVA: 0x00007BC0 File Offset: 0x00005DC0
		[Token(Token = "0x60010FC")]
		[Address(RVA = "0x1011908A0", Offset = "0x11908A0", VA = "0x1011908A0")]
		public int GetMemberChangeRecastMax()
		{
			return 0;
		}

		// Token: 0x06001242 RID: 4674 RVA: 0x00007BD8 File Offset: 0x00005DD8
		[Token(Token = "0x60010FD")]
		[Address(RVA = "0x10119090C", Offset = "0x119090C", VA = "0x10119090C")]
		public float GetStunCoef()
		{
			return 0f;
		}

		// Token: 0x06001243 RID: 4675 RVA: 0x00007BF0 File Offset: 0x00005DF0
		[Token(Token = "0x60010FE")]
		[Address(RVA = "0x101190974", Offset = "0x1190974", VA = "0x101190974")]
		public float GetKiraraJumpGaugeCoef(BattleCommandData.eCommandType commandType)
		{
			return 0f;
		}

		// Token: 0x06001244 RID: 4676 RVA: 0x00007C08 File Offset: 0x00005E08
		[Token(Token = "0x60010FF")]
		[Address(RVA = "0x101187AFC", Offset = "0x1187AFC", VA = "0x101187AFC")]
		public bool IsStateAbsorbAttack()
		{
			return default(bool);
		}

		// Token: 0x06001245 RID: 4677 RVA: 0x00007C20 File Offset: 0x00005E20
		[Token(Token = "0x6001100")]
		[Address(RVA = "0x101190AAC", Offset = "0x1190AAC", VA = "0x101190AAC")]
		public float GetAbsorbCoef(BattleCommandData.eCommandType skillType)
		{
			return 0f;
		}

		// Token: 0x06001246 RID: 4678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001101")]
		[Address(RVA = "0x101190B18", Offset = "0x1190B18", VA = "0x101190B18")]
		public void SetAbsorbCoef(float normal, float skill, float weapon, float unique)
		{
		}

		// Token: 0x06001247 RID: 4679 RVA: 0x00007C38 File Offset: 0x00005E38
		[Token(Token = "0x6001102")]
		[Address(RVA = "0x101190C18", Offset = "0x1190C18", VA = "0x101190C18")]
		public bool IsGaugeUpOnDamage()
		{
			return default(bool);
		}

		// Token: 0x06001248 RID: 4680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001103")]
		[Address(RVA = "0x101190C48", Offset = "0x1190C48", VA = "0x101190C48")]
		public void SetGaugeUpOnDamage(eSkillTurnConsume turnConsume, int turn, float value)
		{
		}

		// Token: 0x06001249 RID: 4681 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001104")]
		[Address(RVA = "0x101186938", Offset = "0x1186938", VA = "0x101186938")]
		public BattleDefine.GaugeUpOnDamage GetGaugeUpOnDamage()
		{
			return null;
		}

		// Token: 0x0600124A RID: 4682 RVA: 0x00007C50 File Offset: 0x00005E50
		[Token(Token = "0x6001105")]
		[Address(RVA = "0x101187B90", Offset = "0x1187B90", VA = "0x101187B90")]
		public float GetGaugeUpOnDamageValue()
		{
			return 0f;
		}

		// Token: 0x0600124B RID: 4683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001106")]
		[Address(RVA = "0x1011856A4", Offset = "0x11856A4", VA = "0x1011856A4")]
		public void ResetGaugeUpOnDamage()
		{
		}

		// Token: 0x0600124C RID: 4684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001107")]
		[Address(RVA = "0x101190CA0", Offset = "0x1190CA0", VA = "0x101190CA0")]
		public void AddGaugeUpOnDamage()
		{
		}

		// Token: 0x0600124D RID: 4685 RVA: 0x00007C68 File Offset: 0x00005E68
		[Token(Token = "0x6001108")]
		[Address(RVA = "0x101190D14", Offset = "0x1190D14", VA = "0x101190D14")]
		public int GetCardTurnChange()
		{
			return 0;
		}

		// Token: 0x0600124E RID: 4686 RVA: 0x00007C80 File Offset: 0x00005E80
		[Token(Token = "0x6001109")]
		[Address(RVA = "0x10118ECF0", Offset = "0x118ECF0", VA = "0x10118ECF0")]
		public bool IsStateOverRecover()
		{
			return default(bool);
		}

		// Token: 0x0600124F RID: 4687 RVA: 0x00007C98 File Offset: 0x00005E98
		[Token(Token = "0x600110A")]
		[Address(RVA = "0x10118ECF8", Offset = "0x118ECF8", VA = "0x10118ECF8")]
		public float GetOverRecoverLimitRatio(BattleCommandData.eCommandType commandType)
		{
			return 0f;
		}

		// Token: 0x06001250 RID: 4688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600110B")]
		[Address(RVA = "0x101190D58", Offset = "0x1190D58", VA = "0x101190D58")]
		public void SetOverRecover(float skill, float weapon, float unique)
		{
		}

		// Token: 0x06001251 RID: 4689 RVA: 0x00007CB0 File Offset: 0x00005EB0
		[Token(Token = "0x600110C")]
		[Address(RVA = "0x101190EB4", Offset = "0x1190EB4", VA = "0x101190EB4")]
		private float CalcStatusRatioByHp(BattleDefine.eStatus targetStatus, BattlePassiveSkillParam_StatusRatioByHP stByHP)
		{
			return 0f;
		}

		// Token: 0x06001252 RID: 4690 RVA: 0x00007CC8 File Offset: 0x00005EC8
		[Token(Token = "0x600110D")]
		[Address(RVA = "0x10118DC28", Offset = "0x118DC28", VA = "0x10118DC28")]
		public float GetPassiveStatusChangeByHp(BattleDefine.eStatus targetStatus)
		{
			return 0f;
		}

		// Token: 0x06001253 RID: 4691 RVA: 0x00007CE0 File Offset: 0x00005EE0
		[Token(Token = "0x600110E")]
		[Address(RVA = "0x101190FD8", Offset = "0x1190FD8", VA = "0x101190FD8")]
		public bool IsStateCounterStatusChange()
		{
			return default(bool);
		}

		// Token: 0x06001254 RID: 4692 RVA: 0x00007CF8 File Offset: 0x00005EF8
		[Token(Token = "0x600110F")]
		[Address(RVA = "0x101190FF8", Offset = "0x1190FF8", VA = "0x101190FF8")]
		public eSkillTargetType GetCounterStatusChangeTarget()
		{
			return eSkillTargetType.Self;
		}

		// Token: 0x06001255 RID: 4693 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001110")]
		[Address(RVA = "0x101191010", Offset = "0x1191010", VA = "0x101191010")]
		public float[] GetCounterStatusChangeParam()
		{
			return null;
		}

		// Token: 0x06001256 RID: 4694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001111")]
		[Address(RVA = "0x101191028", Offset = "0x1191028", VA = "0x101191028")]
		public void SetCounterStatusChangeParam(eSkillTargetType target, float[] param)
		{
		}

		// Token: 0x06001257 RID: 4695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001112")]
		[Address(RVA = "0x1011910A0", Offset = "0x11910A0", VA = "0x1011910A0")]
		public void ResetCounterStatusChangeParam()
		{
		}

		// Token: 0x06001258 RID: 4696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001113")]
		[Address(RVA = "0x1011910D0", Offset = "0x11910D0", VA = "0x1011910D0")]
		public void GivePassiveSkillParam(BattlePassiveSkillParam param)
		{
		}

		// Token: 0x06001259 RID: 4697 RVA: 0x00007D10 File Offset: 0x00005F10
		[Token(Token = "0x6001114")]
		[Address(RVA = "0x10118DA9C", Offset = "0x118DA9C", VA = "0x10118DA9C")]
		public float GetPassiveStatusChange_Atk()
		{
			return 0f;
		}

		// Token: 0x0600125A RID: 4698 RVA: 0x00007D28 File Offset: 0x00005F28
		[Token(Token = "0x6001115")]
		[Address(RVA = "0x10118DF24", Offset = "0x118DF24", VA = "0x10118DF24")]
		public float GetPassiveStatusChange_Mgc()
		{
			return 0f;
		}

		// Token: 0x0600125B RID: 4699 RVA: 0x00007D40 File Offset: 0x00005F40
		[Token(Token = "0x6001116")]
		[Address(RVA = "0x10118E21C", Offset = "0x118E21C", VA = "0x10118E21C")]
		public float GetPassiveStatusChange_Def()
		{
			return 0f;
		}

		// Token: 0x0600125C RID: 4700 RVA: 0x00007D58 File Offset: 0x00005F58
		[Token(Token = "0x6001117")]
		[Address(RVA = "0x10118E514", Offset = "0x118E514", VA = "0x10118E514")]
		public float GetPassiveStatusChange_MDef()
		{
			return 0f;
		}

		// Token: 0x0600125D RID: 4701 RVA: 0x00007D70 File Offset: 0x00005F70
		[Token(Token = "0x6001118")]
		[Address(RVA = "0x10118EA90", Offset = "0x118EA90", VA = "0x10118EA90")]
		public float GetPassiveStatusChange_Spd()
		{
			return 0f;
		}

		// Token: 0x0600125E RID: 4702 RVA: 0x00007D88 File Offset: 0x00005F88
		[Token(Token = "0x6001119")]
		[Address(RVA = "0x10118E820", Offset = "0x118E820", VA = "0x10118E820")]
		public float GetPassiveStatusChange_Luck()
		{
			return 0f;
		}

		// Token: 0x0600125F RID: 4703 RVA: 0x00007DA0 File Offset: 0x00005FA0
		[Token(Token = "0x600111A")]
		[Address(RVA = "0x101190930", Offset = "0x1190930", VA = "0x101190930")]
		public float GetPassiveStunCoef()
		{
			return 0f;
		}

		// Token: 0x06001260 RID: 4704 RVA: 0x00007DB8 File Offset: 0x00005FB8
		[Token(Token = "0x600111B")]
		[Address(RVA = "0x10119099C", Offset = "0x119099C", VA = "0x10119099C")]
		public float GetPassiveKiraraJumpGaugeCoef()
		{
			return 0f;
		}

		// Token: 0x06001261 RID: 4705 RVA: 0x00007DD0 File Offset: 0x00005FD0
		[Token(Token = "0x600111C")]
		[Address(RVA = "0x1011902A4", Offset = "0x11902A4", VA = "0x1011902A4")]
		public float GetPassiveCriticalDamageCoef()
		{
			return 0f;
		}

		// Token: 0x06001262 RID: 4706 RVA: 0x00007DE8 File Offset: 0x00005FE8
		[Token(Token = "0x600111D")]
		[Address(RVA = "0x1011903DC", Offset = "0x11903DC", VA = "0x1011903DC")]
		public float GetPassiveHateCoef()
		{
			return 0f;
		}

		// Token: 0x06001263 RID: 4707 RVA: 0x00007E00 File Offset: 0x00006000
		[Token(Token = "0x600111E")]
		[Address(RVA = "0x101191760", Offset = "0x1191760", VA = "0x101191760")]
		public float GetPassiveAbsorbCoef(BattleCommandData.eCommandType skillType)
		{
			return 0f;
		}

		// Token: 0x06001264 RID: 4708 RVA: 0x00007E18 File Offset: 0x00006018
		[Token(Token = "0x600111F")]
		[Address(RVA = "0x101190D18", Offset = "0x1190D18", VA = "0x101190D18")]
		public int GetPassiveCardTurnChange()
		{
			return 0;
		}

		// Token: 0x06001265 RID: 4709 RVA: 0x00007E30 File Offset: 0x00006030
		[Token(Token = "0x6001120")]
		[Address(RVA = "0x10118FD24", Offset = "0x118FD24", VA = "0x10118FD24")]
		public bool IsPassiveStateAbnormalDisable()
		{
			return default(bool);
		}

		// Token: 0x06001266 RID: 4710 RVA: 0x00007E48 File Offset: 0x00006048
		[Token(Token = "0x6001121")]
		[Address(RVA = "0x10118FE3C", Offset = "0x118FE3C", VA = "0x10118FE3C")]
		public bool IsPassiveStateStunDisable()
		{
			return default(bool);
		}

		// Token: 0x06001267 RID: 4711 RVA: 0x00007E60 File Offset: 0x00006060
		[Token(Token = "0x6001122")]
		[Address(RVA = "0x101187B04", Offset = "0x1187B04", VA = "0x101187B04")]
		public bool IsPassiveStateAbsorbAttack()
		{
			return default(bool);
		}

		// Token: 0x06001268 RID: 4712 RVA: 0x00007E78 File Offset: 0x00006078
		[Token(Token = "0x6001123")]
		[Address(RVA = "0x101191814", Offset = "0x1191814", VA = "0x101191814")]
		public bool IsPassiveStateOverRecover()
		{
			return default(bool);
		}

		// Token: 0x06001269 RID: 4713 RVA: 0x00007E90 File Offset: 0x00006090
		[Token(Token = "0x6001124")]
		[Address(RVA = "0x101191820", Offset = "0x1191820", VA = "0x101191820")]
		public float GetPassiveAbnormalAdditionalProbability(eStateAbnormal stateAbnormal)
		{
			return 0f;
		}

		// Token: 0x0600126A RID: 4714 RVA: 0x00007EA8 File Offset: 0x000060A8
		[Token(Token = "0x6001125")]
		[Address(RVA = "0x10118F1E4", Offset = "0x118F1E4", VA = "0x10118F1E4")]
		public float GetPassiveElementResist(eElementType elementType)
		{
			return 0f;
		}

		// Token: 0x0600126B RID: 4715 RVA: 0x00007EC0 File Offset: 0x000060C0
		[Token(Token = "0x6001126")]
		[Address(RVA = "0x101191940", Offset = "0x1191940", VA = "0x101191940")]
		public int GetPassiveTurnIncrease(BattlePassiveSkillParam_TurnIncrease.eTarget target, bool isUpValue = true)
		{
			return 0;
		}

		// Token: 0x04001471 RID: 5233
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000ED2")]
		[NonSerialized]
		private CharacterHandler m_Owner;

		// Token: 0x04001472 RID: 5234
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000ED3")]
		[NonSerialized]
		private long m_Point;

		// Token: 0x04001473 RID: 5235
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000ED4")]
		[NonSerialized]
		private float[] m_TBuff;

		// Token: 0x04001474 RID: 5236
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000ED5")]
		[NonSerialized]
		private int m_FixedBaseHp;

		// Token: 0x04001475 RID: 5237
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000ED6")]
		[NonSerialized]
		private int m_FixedBaseAtk;

		// Token: 0x04001476 RID: 5238
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000ED7")]
		[NonSerialized]
		private int m_FixedBaseMgc;

		// Token: 0x04001477 RID: 5239
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000ED8")]
		[NonSerialized]
		private int m_FixedBaseDef;

		// Token: 0x04001478 RID: 5240
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000ED9")]
		[NonSerialized]
		private int m_FixedBaseMDef;

		// Token: 0x04001479 RID: 5241
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000EDA")]
		[NonSerialized]
		private int m_FixedBaseSpd;

		// Token: 0x0400147A RID: 5242
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000EDB")]
		[NonSerialized]
		private int m_FixedBaseLuck;

		// Token: 0x0400147B RID: 5243
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000EDC")]
		[NonSerialized]
		private float[] m_DefaultElementCoef;

		// Token: 0x0400147C RID: 5244
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000EDD")]
		[SerializeField]
		private BattleDefine.BuffStack[] ElemResists;

		// Token: 0x0400147D RID: 5245
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000EDE")]
		[SerializeField]
		private BattleDefine.BuffStack[] StsBuffs;

		// Token: 0x0400147E RID: 5246
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000EDF")]
		[SerializeField]
		private BattleDefine.StateAbnormal[] Abnmls;

		// Token: 0x0400147F RID: 5247
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000EE0")]
		[SerializeField]
		private BattleDefine.BuffStack AbnmlDisableBuff;

		// Token: 0x04001480 RID: 5248
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000EE1")]
		[SerializeField]
		private BattleDefine.BuffStack StsBuffDisableBuff;

		// Token: 0x04001481 RID: 5249
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000EE2")]
		[SerializeField]
		private BattleDefine.BuffStack StsDebuffDisableBuff;

		// Token: 0x04001482 RID: 5250
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000EE3")]
		[SerializeField]
		private BattleDefine.BuffStack AbnmlAddProbBuff;

		// Token: 0x04001483 RID: 5251
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000EE4")]
		[SerializeField]
		private BattleDefine.NextBuff[] NextBuffs;

		// Token: 0x04001484 RID: 5252
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000EE5")]
		[SerializeField]
		private BattleDefine.BuffStack WeakElemBonusBuff;

		// Token: 0x04001485 RID: 5253
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000EE6")]
		[SerializeField]
		private BattleDefine.BuffStack HateChange;

		// Token: 0x04001486 RID: 5254
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000EE7")]
		[SerializeField]
		private BattleDefine.RegeneData Regene;

		// Token: 0x04001487 RID: 5255
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000EE8")]
		[SerializeField]
		private List<BattleDefine.ReviveData> Revives;

		// Token: 0x04001488 RID: 5256
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000EE9")]
		[SerializeField]
		private int RevivedCount;

		// Token: 0x04001489 RID: 5257
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4000EEA")]
		[SerializeField]
		private float Barrier;

		// Token: 0x0400148A RID: 5258
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000EEB")]
		[SerializeField]
		private int BarrierCount;

		// Token: 0x0400148B RID: 5259
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4000EEC")]
		[SerializeField]
		private float LoadFactorReduceRate;

		// Token: 0x0400148C RID: 5260
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000EED")]
		[SerializeField]
		private eSkillTurnConsume LoadFactorTurnConsume;

		// Token: 0x0400148D RID: 5261
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4000EEE")]
		[SerializeField]
		private int LoadFactorReduceCount;

		// Token: 0x0400148E RID: 5262
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4000EEF")]
		[SerializeField]
		private BattleDefine.BuffStack CriticalDamageBuff;

		// Token: 0x0400148F RID: 5263
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4000EF0")]
		[SerializeField]
		private int Hp;

		// Token: 0x04001490 RID: 5264
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x4000EF1")]
		[NonSerialized]
		private int m_ChargeCount;

		// Token: 0x04001491 RID: 5265
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4000EF2")]
		[SerializeField]
		private BattleDefine.eDeadDetail DeadDetail;

		// Token: 0x04001492 RID: 5266
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x4000EF3")]
		[NonSerialized]
		private bool m_IsGuard;

		// Token: 0x04001493 RID: 5267
		[Cpp2IlInjected.FieldOffset(Offset = "0xDD")]
		[Token(Token = "0x4000EF4")]
		[NonSerialized]
		private bool m_WasAttacked;

		// Token: 0x04001494 RID: 5268
		[Cpp2IlInjected.FieldOffset(Offset = "0xDE")]
		[Token(Token = "0x4000EF5")]
		[NonSerialized]
		private bool m_WasAttackedOnSleeping;

		// Token: 0x04001495 RID: 5269
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4000EF6")]
		[SerializeField]
		private int MCRecast;

		// Token: 0x04001496 RID: 5270
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4000EF7")]
		[SerializeField]
		private bool IsAbsorbAttack;

		// Token: 0x04001497 RID: 5271
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4000EF8")]
		[SerializeField]
		private float[] AbsorbRatio;

		// Token: 0x04001498 RID: 5272
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4000EF9")]
		[SerializeField]
		private bool IsOverRecover;

		// Token: 0x04001499 RID: 5273
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4000EFA")]
		[SerializeField]
		private float[] OverRecoverLimitRatio;

		// Token: 0x0400149A RID: 5274
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4000EFB")]
		[SerializeField]
		private BattleDefine.GaugeUpOnDamage GaugeUpOnDamage;

		// Token: 0x0400149B RID: 5275
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4000EFC")]
		[SerializeField]
		private BattleDefine.CounterStatusChangeData CounterStatusChangeParam;

		// Token: 0x0400149C RID: 5276
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4000EFD")]
		[NonSerialized]
		public CharacterBattleParam.eRecoveredLog recoveried;

		// Token: 0x0400149D RID: 5277
		[Token(Token = "0x4000EFE")]
		[NonSerialized]
		public const CharacterBattleParam.eRecoveredLog RecoveryAndAbnormal = CharacterBattleParam.eRecoveredLog.Recovery | CharacterBattleParam.eRecoveredLog.Abnormal;

		// Token: 0x0400149E RID: 5278
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4000EFF")]
		[SerializeField]
		private List<BattlePassiveSkillParam_StatusChange> Passive_StatusChanges;

		// Token: 0x0400149F RID: 5279
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4000F00")]
		[SerializeField]
		private BattlePassiveSkillParam_AbnormalDisable Passive_AbnmlDisable;

		// Token: 0x040014A0 RID: 5280
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4000F01")]
		[SerializeField]
		private BattlePassiveSkillParam_StunDisable Passive_StunDisable;

		// Token: 0x040014A1 RID: 5281
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4000F02")]
		[SerializeField]
		private BattlePassiveSkillParam_StunCoef Passive_StunCoef;

		// Token: 0x040014A2 RID: 5282
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4000F03")]
		[SerializeField]
		private List<BattlePassiveSkillParam_KiraraJumpGaugeCoef> Passive_KiraraJumpGaugeCoefs;

		// Token: 0x040014A3 RID: 5283
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4000F04")]
		[SerializeField]
		private BattlePassiveSkillParam_CriticalDamageCoef Passive_CriticalDamageCoef;

		// Token: 0x040014A4 RID: 5284
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4000F05")]
		[SerializeField]
		private List<BattlePassiveSkillParam_HateChange> Passive_HateChanges;

		// Token: 0x040014A5 RID: 5285
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4000F06")]
		[SerializeField]
		private BattlePassiveSkillParam_AbsorbAttack Passive_AbsorbAttack;

		// Token: 0x040014A6 RID: 5286
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4000F07")]
		[SerializeField]
		private BattlePassiveSkillParam_OverRecover Passive_OverRecover;

		// Token: 0x040014A7 RID: 5287
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4000F08")]
		[SerializeField]
		private BattlePassiveSkillParam_CardTurnChange Passive_CardTurnChange;

		// Token: 0x040014A8 RID: 5288
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4000F09")]
		[SerializeField]
		private BattlePassiveSkillParam_StatusRatioByHP[] Passive_StatusRatioByHP;

		// Token: 0x040014A9 RID: 5289
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4000F0A")]
		[SerializeField]
		private List<BattlePassiveSkillParam_AbnormalAdditionalProbability> Passive_AbnmlAddProbs;

		// Token: 0x040014AA RID: 5290
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4000F0B")]
		[SerializeField]
		private List<BattlePassiveSkillParam_ElementResist> Passive_ElementResists;

		// Token: 0x040014AB RID: 5291
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4000F0C")]
		[SerializeField]
		private List<BattlePassiveSkillParam_TurnIncrease> Passive_TurnIncreases;

		// Token: 0x02000463 RID: 1123
		[Token(Token = "0x2000DC5")]
		[Flags]
		public enum eRecoveredLog
		{
			// Token: 0x040014AD RID: 5293
			[Token(Token = "0x4005921")]
			None = 0,
			// Token: 0x040014AE RID: 5294
			[Token(Token = "0x4005922")]
			Recovery = 1,
			// Token: 0x040014AF RID: 5295
			[Token(Token = "0x4005923")]
			Abnormal = 2,
			// Token: 0x040014B0 RID: 5296
			[Token(Token = "0x4005924")]
			AbnormalRecovery = 4,
			// Token: 0x040014B1 RID: 5297
			[Token(Token = "0x4005925")]
			Num = 5
		}
	}
}
