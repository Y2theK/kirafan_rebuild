﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000636 RID: 1590
	[Token(Token = "0x2000522")]
	[StructLayout(3)]
	public abstract class CharacterDataController
	{
		// Token: 0x060016EF RID: 5871 RVA: 0x0000A758 File Offset: 0x00008958
		[Token(Token = "0x6001596")]
		[Address(RVA = "0x101191A68", Offset = "0x1191A68", VA = "0x101191A68")]
		public static bool IsExistCharacterStatic(eCharacterDataType characterDataType)
		{
			return default(bool);
		}

		// Token: 0x060016F0 RID: 5872 RVA: 0x0000A770 File Offset: 0x00008970
		[Token(Token = "0x6001597")]
		[Address(RVA = "0x101191A78", Offset = "0x1191A78", VA = "0x101191A78")]
		public eCharacterDataType GetCharacterDataType()
		{
			return eCharacterDataType.Error;
		}

		// Token: 0x060016F1 RID: 5873 RVA: 0x0000A788 File Offset: 0x00008988
		[Token(Token = "0x6001598")]
		[Address(RVA = "0x101191AB4", Offset = "0x1191AB4", VA = "0x101191AB4")]
		public bool IsExistCharacter()
		{
			return default(bool);
		}

		// Token: 0x060016F2 RID: 5874
		[Token(Token = "0x6001599")]
		[Address(Slot = "4")]
		protected abstract CharacterDataWrapperBase GetDataBaseInternal();

		// Token: 0x060016F3 RID: 5875 RVA: 0x0000A7A0 File Offset: 0x000089A0
		[Token(Token = "0x600159A")]
		[Address(RVA = "0x101191AF4", Offset = "0x1191AF4", VA = "0x101191AF4")]
		public long GetCharaMngId()
		{
			return 0L;
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x0000A7B8 File Offset: 0x000089B8
		[Token(Token = "0x600159B")]
		[Address(RVA = "0x101191B30", Offset = "0x1191B30", VA = "0x101191B30")]
		public int GetCharaId()
		{
			return 0;
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x0000A7D0 File Offset: 0x000089D0
		[Token(Token = "0x600159C")]
		[Address(RVA = "0x101191B6C", Offset = "0x1191B6C", VA = "0x101191B6C")]
		public eEventQuestDropExtPlusType GetCharaEventQuestDropExtPlusType()
		{
			return eEventQuestDropExtPlusType.Self;
		}

		// Token: 0x060016F6 RID: 5878 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600159D")]
		[Address(RVA = "0x101191BA8", Offset = "0x1191BA8", VA = "0x101191BA8")]
		public Dictionary<int, int> GetCharaEventQuestDropExtTable()
		{
			return null;
		}

		// Token: 0x060016F7 RID: 5879 RVA: 0x0000A7E8 File Offset: 0x000089E8
		[Token(Token = "0x600159E")]
		[Address(RVA = "0x101191D28", Offset = "0x1191D28", VA = "0x101191D28")]
		public eClassType GetClassType()
		{
			return eClassType.Fighter;
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x0000A800 File Offset: 0x00008A00
		[Token(Token = "0x600159F")]
		[Address(RVA = "0x101191D64", Offset = "0x1191D64", VA = "0x101191D64")]
		public eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x060016F9 RID: 5881 RVA: 0x0000A818 File Offset: 0x00008A18
		[Token(Token = "0x60015A0")]
		[Address(RVA = "0x101191DA0", Offset = "0x1191DA0", VA = "0x101191DA0")]
		public int GetCharaCost()
		{
			return 0;
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x0000A830 File Offset: 0x00008A30
		[Token(Token = "0x60015A1")]
		[Address(RVA = "0x101191DDC", Offset = "0x1191DDC", VA = "0x101191DDC")]
		public int GetCharaLv()
		{
			return 0;
		}

		// Token: 0x060016FB RID: 5883 RVA: 0x0000A848 File Offset: 0x00008A48
		[Token(Token = "0x60015A2")]
		[Address(RVA = "0x101191E18", Offset = "0x1191E18", VA = "0x101191E18")]
		public int GetCharaMaxLv()
		{
			return 0;
		}

		// Token: 0x060016FC RID: 5884 RVA: 0x0000A860 File Offset: 0x00008A60
		[Token(Token = "0x60015A3")]
		[Address(RVA = "0x101191E54", Offset = "0x1191E54", VA = "0x101191E54")]
		public int GetCharaLimitBreak()
		{
			return 0;
		}

		// Token: 0x060016FD RID: 5885 RVA: 0x0000A878 File Offset: 0x00008A78
		[Token(Token = "0x60015A4")]
		[Address(RVA = "0x101191E94", Offset = "0x1191E94", VA = "0x101191E94")]
		public int GetCharaArousal()
		{
			return 0;
		}

		// Token: 0x060016FE RID: 5886 RVA: 0x0000A890 File Offset: 0x00008A90
		[Token(Token = "0x60015A5")]
		[Address(RVA = "0x101191ED4", Offset = "0x1191ED4", VA = "0x101191ED4")]
		public long GetCharaExp()
		{
			return 0L;
		}

		// Token: 0x060016FF RID: 5887 RVA: 0x0000A8A8 File Offset: 0x00008AA8
		[Token(Token = "0x60015A6")]
		[Address(RVA = "0x101191F14", Offset = "0x1191F14", VA = "0x101191F14")]
		public int GetFriendship()
		{
			return 0;
		}

		// Token: 0x06001700 RID: 5888 RVA: 0x0000A8C0 File Offset: 0x00008AC0
		[Token(Token = "0x60015A7")]
		[Address(RVA = "0x101191F54", Offset = "0x1191F54", VA = "0x101191F54", Slot = "5")]
		public virtual int GetCost()
		{
			return 0;
		}

		// Token: 0x06001701 RID: 5889 RVA: 0x0000A8D8 File Offset: 0x00008AD8
		[Token(Token = "0x60015A8")]
		[Address(RVA = "0x101191F58", Offset = "0x1191F58", VA = "0x101191F58")]
		public int GetArousalLv()
		{
			return 0;
		}

		// Token: 0x06001702 RID: 5890 RVA: 0x0000A8F0 File Offset: 0x00008AF0
		[Token(Token = "0x60015A9")]
		[Address(RVA = "0x101191F98", Offset = "0x1191F98", VA = "0x101191F98")]
		public int GetDuplicatedCount()
		{
			return 0;
		}

		// Token: 0x06001703 RID: 5891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015AA")]
		[Address(RVA = "0x101191FD8", Offset = "0x1191FD8", VA = "0x101191FD8")]
		protected CharacterDataController()
		{
		}
	}
}
