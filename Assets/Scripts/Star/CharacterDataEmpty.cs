﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000628 RID: 1576
	[Token(Token = "0x200051B")]
	[StructLayout(3)]
	public class CharacterDataEmpty : CharacterDataWrapperNoExist
	{
		// Token: 0x06001672 RID: 5746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001522")]
		[Address(RVA = "0x101191FE0", Offset = "0x1191FE0", VA = "0x101191FE0")]
		public CharacterDataEmpty()
		{
		}
	}
}
