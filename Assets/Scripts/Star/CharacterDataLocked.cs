﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000629 RID: 1577
	[Token(Token = "0x200051C")]
	[StructLayout(3)]
	public class CharacterDataLocked : CharacterDataWrapperNoExist
	{
		// Token: 0x06001673 RID: 5747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001523")]
		[Address(RVA = "0x101192040", Offset = "0x1192040", VA = "0x101192040")]
		public CharacterDataLocked()
		{
		}
	}
}
