﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000622 RID: 1570
	[Token(Token = "0x2000515")]
	[StructLayout(3)]
	public abstract class CharacterDataWrapperBase : EnumerationObjectData
	{
		// Token: 0x06001620 RID: 5664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014D0")]
		[Address(RVA = "0x101192070", Offset = "0x1192070", VA = "0x101192070")]
		protected CharacterDataWrapperBase(eCharacterDataType characterDataType)
		{
		}

		// Token: 0x06001621 RID: 5665 RVA: 0x00009C30 File Offset: 0x00007E30
		[Token(Token = "0x60014D1")]
		[Address(RVA = "0x101191AAC", Offset = "0x1191AAC", VA = "0x101191AAC")]
		public eCharacterDataType GetCharacterDataType()
		{
			return eCharacterDataType.Error;
		}

		// Token: 0x06001622 RID: 5666
		[Token(Token = "0x60014D2")]
		[Address(Slot = "4")]
		public abstract long GetMngId();

		// Token: 0x06001623 RID: 5667
		[Token(Token = "0x60014D3")]
		[Address(Slot = "5")]
		public abstract int GetCharaId();

		// Token: 0x06001624 RID: 5668 RVA: 0x00009C48 File Offset: 0x00007E48
		[Token(Token = "0x60014D4")]
		[Address(RVA = "0x1011920A0", Offset = "0x11920A0", VA = "0x1011920A0", Slot = "6")]
		public virtual eEventQuestDropExtPlusType GetCharaEventQuestDropExtPlusType()
		{
			return eEventQuestDropExtPlusType.Self;
		}

		// Token: 0x06001625 RID: 5669 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60014D5")]
		[Address(RVA = "0x101191BDC", Offset = "0x1191BDC", VA = "0x101191BDC")]
		public Dictionary<int, int> GetCharaEventQuestDropExtTable()
		{
			return null;
		}

		// Token: 0x06001626 RID: 5670
		[Token(Token = "0x60014D6")]
		[Address(Slot = "7")]
		public abstract eCharaNamedType GetCharaNamedType();

		// Token: 0x06001627 RID: 5671
		[Token(Token = "0x60014D7")]
		[Address(Slot = "8")]
		public abstract eClassType GetClassType();

		// Token: 0x06001628 RID: 5672
		[Token(Token = "0x60014D8")]
		[Address(Slot = "9")]
		public abstract eElementType GetElementType();

		// Token: 0x06001629 RID: 5673
		[Token(Token = "0x60014D9")]
		[Address(Slot = "10")]
		public abstract eRare GetRarity();

		// Token: 0x0600162A RID: 5674
		[Token(Token = "0x60014DA")]
		[Address(Slot = "11")]
		public abstract int GetCost();

		// Token: 0x0600162B RID: 5675
		[Token(Token = "0x60014DB")]
		[Address(Slot = "12")]
		public abstract int GetLv();

		// Token: 0x0600162C RID: 5676
		[Token(Token = "0x60014DC")]
		[Address(Slot = "13")]
		public abstract int GetMaxLv();

		// Token: 0x0600162D RID: 5677
		[Token(Token = "0x60014DD")]
		[Address(Slot = "14")]
		public abstract long GetExp();

		// Token: 0x0600162E RID: 5678
		[Token(Token = "0x60014DE")]
		[Address(Slot = "15")]
		public abstract int GetLimitBreak();

		// Token: 0x0600162F RID: 5679
		[Token(Token = "0x60014DF")]
		[Address(Slot = "16")]
		public abstract int GetArousal();

		// Token: 0x06001630 RID: 5680
		[Token(Token = "0x60014E0")]
		[Address(Slot = "17")]
		public abstract int GetFriendship();

		// Token: 0x06001631 RID: 5681
		[Token(Token = "0x60014E1")]
		[Address(Slot = "18")]
		public abstract int GetAroualLevel();

		// Token: 0x06001632 RID: 5682
		[Token(Token = "0x60014E2")]
		[Address(Slot = "19")]
		public abstract int GetDuplicatedCount();

		// Token: 0x04002624 RID: 9764
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001F8E")]
		protected eCharacterDataType m_CharacterDataType;
	}
}
