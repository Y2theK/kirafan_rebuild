﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000623 RID: 1571
	[Token(Token = "0x2000516")]
	[StructLayout(3)]
	public abstract class CharacterDataWrapperBaseExGen<DataType> : CharacterDataWrapperBase
	{
		// Token: 0x06001633 RID: 5683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014E3")]
		[Address(RVA = "0x1016CBEB0", Offset = "0x16CBEB0", VA = "0x1016CBEB0")]
		protected CharacterDataWrapperBaseExGen(eCharacterDataType characterDataType, DataType in_data)
		{
		}

		// Token: 0x04002625 RID: 9765
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F8F")]
		protected DataType data;
	}
}
