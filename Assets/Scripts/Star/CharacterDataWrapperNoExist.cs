﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000627 RID: 1575
	[Token(Token = "0x200051A")]
	[StructLayout(3)]
	public abstract class CharacterDataWrapperNoExist : CharacterDataWrapperBase
	{
		// Token: 0x06001662 RID: 5730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001512")]
		[Address(RVA = "0x101192010", Offset = "0x1192010", VA = "0x101192010")]
		protected CharacterDataWrapperNoExist(eCharacterDataType characterDataType)
		{
		}

		// Token: 0x06001663 RID: 5731 RVA: 0x0000A050 File Offset: 0x00008250
		[Token(Token = "0x6001513")]
		[Address(RVA = "0x1011920A8", Offset = "0x11920A8", VA = "0x1011920A8", Slot = "4")]
		public override long GetMngId()
		{
			return 0L;
		}

		// Token: 0x06001664 RID: 5732 RVA: 0x0000A068 File Offset: 0x00008268
		[Token(Token = "0x6001514")]
		[Address(RVA = "0x1011920B0", Offset = "0x11920B0", VA = "0x1011920B0", Slot = "5")]
		public override int GetCharaId()
		{
			return 0;
		}

		// Token: 0x06001665 RID: 5733 RVA: 0x0000A080 File Offset: 0x00008280
		[Token(Token = "0x6001515")]
		[Address(RVA = "0x1011920B8", Offset = "0x11920B8", VA = "0x1011920B8", Slot = "7")]
		public override eCharaNamedType GetCharaNamedType()
		{
			return eCharaNamedType.Named_0000;
		}

		// Token: 0x06001666 RID: 5734 RVA: 0x0000A098 File Offset: 0x00008298
		[Token(Token = "0x6001516")]
		[Address(RVA = "0x1011920C0", Offset = "0x11920C0", VA = "0x1011920C0", Slot = "8")]
		public override eClassType GetClassType()
		{
			return eClassType.Fighter;
		}

		// Token: 0x06001667 RID: 5735 RVA: 0x0000A0B0 File Offset: 0x000082B0
		[Token(Token = "0x6001517")]
		[Address(RVA = "0x1011920C8", Offset = "0x11920C8", VA = "0x1011920C8", Slot = "9")]
		public override eElementType GetElementType()
		{
			return eElementType.Fire;
		}

		// Token: 0x06001668 RID: 5736 RVA: 0x0000A0C8 File Offset: 0x000082C8
		[Token(Token = "0x6001518")]
		[Address(RVA = "0x1011920D0", Offset = "0x11920D0", VA = "0x1011920D0", Slot = "10")]
		public override eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x06001669 RID: 5737 RVA: 0x0000A0E0 File Offset: 0x000082E0
		[Token(Token = "0x6001519")]
		[Address(RVA = "0x1011920D8", Offset = "0x11920D8", VA = "0x1011920D8", Slot = "11")]
		public override int GetCost()
		{
			return 0;
		}

		// Token: 0x0600166A RID: 5738 RVA: 0x0000A0F8 File Offset: 0x000082F8
		[Token(Token = "0x600151A")]
		[Address(RVA = "0x1011920E0", Offset = "0x11920E0", VA = "0x1011920E0", Slot = "12")]
		public override int GetLv()
		{
			return 0;
		}

		// Token: 0x0600166B RID: 5739 RVA: 0x0000A110 File Offset: 0x00008310
		[Token(Token = "0x600151B")]
		[Address(RVA = "0x1011920E8", Offset = "0x11920E8", VA = "0x1011920E8", Slot = "13")]
		public override int GetMaxLv()
		{
			return 0;
		}

		// Token: 0x0600166C RID: 5740 RVA: 0x0000A128 File Offset: 0x00008328
		[Token(Token = "0x600151C")]
		[Address(RVA = "0x1011920F0", Offset = "0x11920F0", VA = "0x1011920F0", Slot = "14")]
		public override long GetExp()
		{
			return 0L;
		}

		// Token: 0x0600166D RID: 5741 RVA: 0x0000A140 File Offset: 0x00008340
		[Token(Token = "0x600151D")]
		[Address(RVA = "0x1011920F8", Offset = "0x11920F8", VA = "0x1011920F8", Slot = "15")]
		public override int GetLimitBreak()
		{
			return 0;
		}

		// Token: 0x0600166E RID: 5742 RVA: 0x0000A158 File Offset: 0x00008358
		[Token(Token = "0x600151E")]
		[Address(RVA = "0x101192100", Offset = "0x1192100", VA = "0x101192100", Slot = "16")]
		public override int GetArousal()
		{
			return 0;
		}

		// Token: 0x0600166F RID: 5743 RVA: 0x0000A170 File Offset: 0x00008370
		[Token(Token = "0x600151F")]
		[Address(RVA = "0x101192108", Offset = "0x1192108", VA = "0x101192108", Slot = "17")]
		public override int GetFriendship()
		{
			return 0;
		}

		// Token: 0x06001670 RID: 5744 RVA: 0x0000A188 File Offset: 0x00008388
		[Token(Token = "0x6001520")]
		[Address(RVA = "0x101192110", Offset = "0x1192110", VA = "0x101192110", Slot = "18")]
		public override int GetAroualLevel()
		{
			return 0;
		}

		// Token: 0x06001671 RID: 5745 RVA: 0x0000A1A0 File Offset: 0x000083A0
		[Token(Token = "0x6001521")]
		[Address(RVA = "0x101192118", Offset = "0x1192118", VA = "0x101192118", Slot = "19")]
		public override int GetDuplicatedCount()
		{
			return 0;
		}
	}
}
