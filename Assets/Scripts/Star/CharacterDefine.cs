﻿using UnityEngine;

namespace Star {
    public static class CharacterDefine {
        public static readonly string[] EN_LOCATOR_NAMES = new string[] {
            "LOC_body",
            "LOC_overhead"
        };
        public static readonly string[] WEAPON_ATTACH_NAMES = new string[] {
            "root/Hips/Spine1/Spine2/Shoulder_L/Arm_L/Fore_arm_L/Hand_L/Weapon_L/Loc_L",
            "root/Hips/Spine1/Spine2/Shoulder_R/Arm_R/Fore_arm_R/Hand_R/Weapon_R/Loc_R"
        };
        public const string PL_HEAD_ROOT_NAME = "Head_root";
        public const string PL_BODY_ROOT_NAME = "root";
        public const string HIPS_NAME = "Hips";
        public const string EN_ROOT_NAME = "root";
        public const float PL_BODY_LOCATOR_OFFSET_Y = 0.5f;
        public static readonly Vector3 PL_OVERHEAD_OFFSET_FROM_HEAD = new Vector3(-0.1f, 0.36f, 0f);
        public const int BODY_ID_DEFAULT = 0;
        public const int BODY_ID_SKIRT = 1;
        public const int BODY_ID_TIGHT_SKIRT = 2;
        public static readonly string[] BODY_ANIM_PREFIXS = new string[] {
            "common_body",
            "common_body_skirt",
            "common_body_tight"
        };
        public const int CLASS_ANIM_TYPE_DEFAULT = 0;
        public static readonly string[] MENU_MODE_ACT_KEYS = new string[] {
            MENU_MODE_IDLE_L_ACT_KEY,
            MENU_MODE_IDLE_R_ACT_KEY,
            MENU_MODE_IDLE_SKIRT_L_ACT_KEY,
            MENU_MODE_IDLE_SKIRT_R_ACT_KEY
        };
        public const string MENU_MODE_IDLE_L_ACT_KEY = "room_idle_L";
        public const string MENU_MODE_IDLE_R_ACT_KEY = "room_idle_R";
        public const string MENU_MODE_IDLE_SKIRT_L_ACT_KEY = "room_idle_skirt_L";
        public const string MENU_MODE_IDLE_SKIRT_R_ACT_KEY = "room_idle_skirt_R";
        public static readonly int[] FACIAL_ID_DEFAULT = new int[] {
            0,
            45
        };
        public static readonly int[,] FACIAL_ID_BLINK = new int[,] {
            { 0, 18 },
            { 45, 54 }
        };
        public static readonly int[,] FACIAL_ID_BLINK_BATTLE = new int[,] {
            { 1, 18 },
            { 45, 54 }
        };
        public static readonly int[] FACIAL_ID_ABNORMAL_BATTLE = new int[] {
            6,
            48
        };

        public static readonly int[] FACIAL_ID_SLEEP_BATTLE = new int[] {
            18,
            54
        };
        public const int FACIAL_NUM = 100;
        public const string FLOATING_OBJ_NAME = "Ex_Float";
        public const float FLOATIONG_FREQUENCY = 1.9f;
        public const float FLOATIONG_AMPLITUDE = 0.065f;
        public const string ABPATH_ANIM_PL_CMN_BTL_BODY = "anim/player/common_battle_body.muast";

        public enum eMode {
            None = -1,
            Battle,
            Room,
            Menu,
            Viewer,
            Num
        }

        public enum eDir {
            L,
            R,
            FRONT,
            Num
        }

        public enum ePlayerModelParts {
            Head,
            Body,
            Num
        }

        public enum eEnemyModelParts {
            Body,
            Num
        }

        public enum eLocatorIndex {
            Body,
            OverHead,
            Num
        }

        public enum eWeaponIndex {
            L,
            R,
            Num
        }

        public enum eFriendType {
            None = -1,
            Registered,
            Unregistered,
            NPC
        }

        public enum eAnimEvent {
            Facial = 1,
            Num
        }

        public enum eDedicatedAnimType {
            Default,
            DedicatedCharaSkill,
            DedicatedForChara
        }
    }
}
