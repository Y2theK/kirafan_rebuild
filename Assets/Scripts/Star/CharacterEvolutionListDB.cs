﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004BD RID: 1213
	[Token(Token = "0x20003B5")]
	[StructLayout(3)]
	public class CharacterEvolutionListDB : ScriptableObject
	{
		// Token: 0x060013FE RID: 5118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B3")]
		[Address(RVA = "0x1011927B0", Offset = "0x11927B0", VA = "0x1011927B0")]
		public CharacterEvolutionListDB()
		{
		}

		// Token: 0x04001700 RID: 5888
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010E6")]
		public CharacterEvolutionListDB_Param[] m_Params;
	}
}
