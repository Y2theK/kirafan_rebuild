﻿using System.Collections.Generic;

namespace Star {
    public static class CharacterEvolutionListDB_Ext {
        public static bool GetParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcCharaID == srcCharaID) {
                    result = self.m_Params[i];
                    return true;
                }
            }
            result = default;
            return false;
        }

        public static bool GetParamByDestCharaID(this CharacterEvolutionListDB self, int destCharaID, out CharacterEvolutionListDB_Param result) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_DestCharaID == destCharaID) {
                    result = self.m_Params[i];
                    return true;
                }
            }
            result = default;
            return false;
        }

        public static bool GetNormalMaterialParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcCharaID == srcCharaID && self.m_Params[i].m_RecipeType == (int)eCharaEvoRecipeType.Default) {
                    result = self.m_Params[i];
                    return true;
                }
            }
            result = default;
            return false;
        }

        public static bool GetSpecifyMaterialParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcCharaID == srcCharaID && self.m_Params[i].m_RecipeType == (int)eCharaEvoRecipeType.Only) {
                    result = self.m_Params[i];
                    return true;
                }
            }
            result = default;
            return false;
        }

        public static bool IsExistParam(this CharacterEvolutionListDB self, int srcCharaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcCharaID == srcCharaID) {
                    return true;
                }
            }
            return false;
        }

        public static int FindPairCharaID(this CharacterEvolutionListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SrcCharaID == charaID) {
                    return self.m_Params[i].m_DestCharaID;
                }
                if (self.m_Params[i].m_DestCharaID == charaID) {
                    return self.m_Params[i].m_SrcCharaID;
                }
            }
            return charaID;
        }

        public static bool GetBeforeEvolutionCharacterIDs(this CharacterEvolutionListDB self, int afterEvolutionCharaID, out List<int> result) {
            List<int> ids = new List<int>();
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_DestCharaID == afterEvolutionCharaID) {
                    ids.Add(self.m_Params[i].m_SrcCharaID);
                }
            }
            result = new List<int>();
            for (int i = 0; i < ids.Count; i++) {
                int id = ids[i];
                if (!result.Contains(id)) {
                    result.Add(id);
                }
            }
            return result.Count > 0;
        }

        public static bool GetBeforeEvolutionCharacterID(this CharacterEvolutionListDB self, int afterEvolutionCharaID, out int result) {
            self.GetBeforeEvolutionCharacterIDs(afterEvolutionCharaID, out List<int> ids);
            if (ids != null && ids.Count > 0) {
                result = ids[0];
                return ids.Count == 1;
            }
            result = -1;
            return false;
        }
    }
}
