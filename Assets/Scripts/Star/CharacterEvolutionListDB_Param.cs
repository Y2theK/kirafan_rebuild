﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004BC RID: 1212
	[Token(Token = "0x20003B4")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct CharacterEvolutionListDB_Param
	{
		// Token: 0x040016F9 RID: 5881
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010DF")]
		public int m_RecipeID;

		// Token: 0x040016FA RID: 5882
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010E0")]
		public int m_RecipeType;

		// Token: 0x040016FB RID: 5883
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010E1")]
		public int m_SrcCharaID;

		// Token: 0x040016FC RID: 5884
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40010E2")]
		public int m_DestCharaID;

		// Token: 0x040016FD RID: 5885
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40010E3")]
		public int m_Amount;

		// Token: 0x040016FE RID: 5886
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010E4")]
		public int[] m_ItemIDs;

		// Token: 0x040016FF RID: 5887
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40010E5")]
		public int[] m_ItemNums;
	}
}
