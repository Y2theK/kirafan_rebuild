﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004C0 RID: 1216
	[Token(Token = "0x20003B8")]
	[StructLayout(3)]
	public class CharacterExpDB : ScriptableObject
	{
		// Token: 0x060013FF RID: 5119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B4")]
		[Address(RVA = "0x1011931E4", Offset = "0x11931E4", VA = "0x1011931E4")]
		public CharacterExpDB()
		{
		}

		// Token: 0x04001706 RID: 5894
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010EC")]
		public CharacterExpDB_Param[] m_Params;
	}
}
