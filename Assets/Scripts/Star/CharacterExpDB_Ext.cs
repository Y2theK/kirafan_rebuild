﻿namespace Star {
    public static class CharacterExpDB_Ext {
        public static int GetNextExp(this CharacterExpDB self, int currentLv) {
            if (currentLv > self.m_Params.Length) {
                return 0;
            }
            return self.m_Params[currentLv - 1].m_NextExp;
        }

        public static long[] GetNextMaxExps(this CharacterExpDB self, int lv_a, int lv_b) {
            int diff = lv_b - lv_a + 1;
            long[] exps = null;
            if (diff > 0) {
                exps = new long[diff];
                for (int i = 0; i < diff; i++) {
                    exps[i] = self.GetNextExp(lv_a + i);
                }
            }
            return exps;
        }

        public static long GetTotalMaxExp(this CharacterExpDB self, int lv_a, int lv_b) {
            long[] exps = self.GetNextMaxExps(lv_a, lv_b);
            long total = 0L;
            if (exps != null) {
                for (int i = 0; i <= exps.Length; i++) {
                    total += exps[i];
                }
            }
            return total;
        }

        public static int GetUpgradeAmount(this CharacterExpDB self, int currentLv) {
            if (currentLv > self.m_Params.Length) {
                return 0;
            }
            return self.m_Params[currentLv - 1].m_UpgradeAmount;
        }
    }
}
