﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004BF RID: 1215
	[Token(Token = "0x20003B7")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct CharacterExpDB_Param
	{
		// Token: 0x04001704 RID: 5892
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010EA")]
		public int m_NextExp;

		// Token: 0x04001705 RID: 5893
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010EB")]
		public int m_UpgradeAmount;
	}
}
