﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004C3 RID: 1219
	[Token(Token = "0x20003BB")]
	[StructLayout(3)]
	public class CharacterFacialDB : ScriptableObject
	{
		// Token: 0x06001400 RID: 5120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B5")]
		[Address(RVA = "0x1011934F8", Offset = "0x11934F8", VA = "0x1011934F8")]
		public CharacterFacialDB()
		{
		}

		// Token: 0x0400170B RID: 5899
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010F1")]
		public CharacterFacialDB_Param[] m_Params;
	}
}
