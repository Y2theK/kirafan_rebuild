﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004C1 RID: 1217
	[Token(Token = "0x20003B9")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct CharacterFacialDB_Data
	{
		// Token: 0x04001707 RID: 5895
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010ED")]
		public int m_TargetNamed;

		// Token: 0x04001708 RID: 5896
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010EE")]
		public int m_FacialID;
	}
}
