﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004C2 RID: 1218
	[Token(Token = "0x20003BA")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct CharacterFacialDB_Param
	{
		// Token: 0x04001709 RID: 5897
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010EF")]
		public int m_ID;

		// Token: 0x0400170A RID: 5898
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010F0")]
		public CharacterFacialDB_Data[] m_Datas;
	}
}
