﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004C5 RID: 1221
	[Token(Token = "0x20003BD")]
	[StructLayout(3)]
	public class CharacterFlavorTextDB : ScriptableObject
	{
		// Token: 0x06001401 RID: 5121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B6")]
		[Address(RVA = "0x1011936E0", Offset = "0x11936E0", VA = "0x1011936E0")]
		public CharacterFlavorTextDB()
		{
		}

		// Token: 0x0400170E RID: 5902
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010F4")]
		public CharacterFlavorTextDB_Param[] m_Params;
	}
}
