﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004C4 RID: 1220
	[Token(Token = "0x20003BC")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct CharacterFlavorTextDB_Param
	{
		// Token: 0x0400170C RID: 5900
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010F2")]
		public int m_CharaID;

		// Token: 0x0400170D RID: 5901
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010F3")]
		public string m_FlavorText;
	}
}
