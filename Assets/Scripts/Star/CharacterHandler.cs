﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200046E RID: 1134
	[Token(Token = "0x2000373")]
	[StructLayout(3)]
	public class CharacterHandler : MonoBehaviour
	{
		// Token: 0x1700012D RID: 301
		// (get) Token: 0x0600126D RID: 4717 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000116")]
		public Transform CacheTransform
		{
			[Token(Token = "0x6001128")]
			[Address(RVA = "0x1011937F4", Offset = "0x11937F4", VA = "0x1011937F4")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x0600126E RID: 4718 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600126F RID: 4719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000117")]
		public Collider TouchCollider
		{
			[Token(Token = "0x6001129")]
			[Address(RVA = "0x1011937FC", Offset = "0x11937FC", VA = "0x1011937FC")]
			get
			{
				return null;
			}
			[Token(Token = "0x600112A")]
			[Address(RVA = "0x101193804", Offset = "0x1193804", VA = "0x101193804")]
			set
			{
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06001270 RID: 4720 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000118")]
		public CharacterResource CharaResource
		{
			[Token(Token = "0x600112B")]
			[Address(RVA = "0x10119380C", Offset = "0x119380C", VA = "0x10119380C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06001271 RID: 4721 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000119")]
		public CharacterBattle CharaBattle
		{
			[Token(Token = "0x600112C")]
			[Address(RVA = "0x101193814", Offset = "0x1193814", VA = "0x101193814")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06001272 RID: 4722 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700011A")]
		public CharacterMenu CharaMenu
		{
			[Token(Token = "0x600112D")]
			[Address(RVA = "0x10119381C", Offset = "0x119381C", VA = "0x10119381C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06001273 RID: 4723 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700011B")]
		public CharacterAnim CharaAnim
		{
			[Token(Token = "0x600112E")]
			[Address(RVA = "0x101193824", Offset = "0x1193824", VA = "0x101193824")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06001274 RID: 4724 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700011C")]
		public CharacterParam CharaParam
		{
			[Token(Token = "0x600112F")]
			[Address(RVA = "0x10119382C", Offset = "0x119382C", VA = "0x10119382C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06001275 RID: 4725 RVA: 0x00007ED8 File Offset: 0x000060D8
		[Token(Token = "0x1700011D")]
		public int CharaID
		{
			[Token(Token = "0x6001130")]
			[Address(RVA = "0x101193834", Offset = "0x1193834", VA = "0x101193834")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06001276 RID: 4726 RVA: 0x00007EF0 File Offset: 0x000060F0
		[Token(Token = "0x1700011E")]
		public uint UniqueID
		{
			[Token(Token = "0x6001131")]
			[Address(RVA = "0x101193868", Offset = "0x1193868", VA = "0x101193868")]
			get
			{
				return 0U;
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06001277 RID: 4727 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700011F")]
		public BattleAIData BattleAIData
		{
			[Token(Token = "0x6001132")]
			[Address(RVA = "0x10119389C", Offset = "0x119389C", VA = "0x10119389C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06001278 RID: 4728 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000120")]
		public NamedParam NamedParam
		{
			[Token(Token = "0x6001133")]
			[Address(RVA = "0x1011938CC", Offset = "0x11938CC", VA = "0x1011938CC")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06001279 RID: 4729 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600127A RID: 4730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000121")]
		public WeaponParam WeaponParam
		{
			[Token(Token = "0x6001134")]
			[Address(RVA = "0x1011938D4", Offset = "0x11938D4", VA = "0x1011938D4")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001135")]
			[Address(RVA = "0x1011938DC", Offset = "0x11938DC", VA = "0x1011938DC")]
			set
			{
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600127B RID: 4731 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000122")]
		public AbilityParam AbilityParam
		{
			[Token(Token = "0x6001136")]
			[Address(RVA = "0x1011938E4", Offset = "0x11938E4", VA = "0x1011938E4")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600127C RID: 4732 RVA: 0x00007F08 File Offset: 0x00006108
		[Token(Token = "0x17000123")]
		public CharacterDefine.eMode Mode
		{
			[Token(Token = "0x6001137")]
			[Address(RVA = "0x1011938EC", Offset = "0x11938EC", VA = "0x1011938EC")]
			get
			{
				return CharacterDefine.eMode.Battle;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x0600127D RID: 4733 RVA: 0x00007F20 File Offset: 0x00006120
		[Token(Token = "0x17000124")]
		public bool IsUserControll
		{
			[Token(Token = "0x6001138")]
			[Address(RVA = "0x1011938F4", Offset = "0x11938F4", VA = "0x1011938F4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x0600127E RID: 4734 RVA: 0x00007F38 File Offset: 0x00006138
		[Token(Token = "0x17000125")]
		public CharacterDefine.eFriendType FriendType
		{
			[Token(Token = "0x6001139")]
			[Address(RVA = "0x1011938FC", Offset = "0x11938FC", VA = "0x1011938FC")]
			get
			{
				return CharacterDefine.eFriendType.Registered;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x0600127F RID: 4735 RVA: 0x00007F50 File Offset: 0x00006150
		[Token(Token = "0x17000126")]
		public bool IsFriendChara
		{
			[Token(Token = "0x600113A")]
			[Address(RVA = "0x101193904", Offset = "0x1193904", VA = "0x101193904")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06001280 RID: 4736 RVA: 0x00007F68 File Offset: 0x00006168
		[Token(Token = "0x17000127")]
		public bool IsEnableRender
		{
			[Token(Token = "0x600113B")]
			[Address(RVA = "0x101193914", Offset = "0x1193914", VA = "0x101193914")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06001281 RID: 4737 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000128")]
		public bool[] IsEnableRenderWeapon
		{
			[Token(Token = "0x600113C")]
			[Address(RVA = "0x10119391C", Offset = "0x119391C", VA = "0x10119391C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x06001282 RID: 4738 RVA: 0x00007F80 File Offset: 0x00006180
		[Token(Token = "0x17000129")]
		public bool IsEnableRenderShadow
		{
			[Token(Token = "0x600113D")]
			[Address(RVA = "0x101193924", Offset = "0x1193924", VA = "0x101193924")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x06001283 RID: 4739 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700012A")]
		public SoundHandler VoiceSoundHndl
		{
			[Token(Token = "0x600113E")]
			[Address(RVA = "0x10119392C", Offset = "0x119392C", VA = "0x10119392C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06001284 RID: 4740 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700012B")]
		private string SoundSelector
		{
			[Token(Token = "0x600113F")]
			[Address(RVA = "0x101193934", Offset = "0x1193934", VA = "0x101193934")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06001285 RID: 4741 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700012C")]
		private string SoundLabel
		{
			[Token(Token = "0x6001140")]
			[Address(RVA = "0x10119394C", Offset = "0x119394C", VA = "0x10119394C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06001286 RID: 4742 RVA: 0x00007F98 File Offset: 0x00006198
		[Token(Token = "0x1700012D")]
		public float DispScale
		{
			[Token(Token = "0x6001141")]
			[Address(RVA = "0x101193964", Offset = "0x1193964", VA = "0x101193964")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x06001287 RID: 4743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001142")]
		[Address(RVA = "0x10119396C", Offset = "0x119396C", VA = "0x10119396C")]
		private void Awake()
		{
		}

		// Token: 0x06001288 RID: 4744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001143")]
		[Address(RVA = "0x101193B3C", Offset = "0x1193B3C", VA = "0x101193B3C")]
		private void Update()
		{
		}

		// Token: 0x06001289 RID: 4745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001144")]
		[Address(RVA = "0x101193CC8", Offset = "0x1193CC8", VA = "0x101193CC8")]
		private void LateUpdate()
		{
		}

		// Token: 0x0600128A RID: 4746 RVA: 0x00007FB0 File Offset: 0x000061B0
		[Token(Token = "0x6001145")]
		[Address(RVA = "0x101193D14", Offset = "0x1193D14", VA = "0x101193D14")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600128B RID: 4747 RVA: 0x00007FC8 File Offset: 0x000061C8
		[Token(Token = "0x6001146")]
		[Address(RVA = "0x101193D1C", Offset = "0x1193D1C", VA = "0x101193D1C")]
		public bool IsDonePreparePerfect()
		{
			return default(bool);
		}

		// Token: 0x0600128C RID: 4748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001147")]
		[Address(RVA = "0x101193D58", Offset = "0x1193D58", VA = "0x101193D58")]
		public void SetupInInstantiate(CharacterParam srcCharaParam, NamedParam srcNamedParam, WeaponParam srcWeaponParam, AbilityParam srcAbilityParam, eCharaResourceType resourceType, int resurceID, uint uniqueID, CharacterDefine.eMode mode, bool isUserControll, CharacterDefine.eFriendType friendType, float scale)
		{
		}

		// Token: 0x0600128D RID: 4749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001148")]
		[Address(RVA = "0x1011941D0", Offset = "0x11941D0", VA = "0x1011941D0")]
		public void Destroy()
		{
		}

		// Token: 0x0600128E RID: 4750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001149")]
		[Address(RVA = "0x101194690", Offset = "0x1194690", VA = "0x101194690")]
		public void Prepare()
		{
		}

		// Token: 0x0600128F RID: 4751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114A")]
		[Address(RVA = "0x1011947B8", Offset = "0x11947B8", VA = "0x1011947B8")]
		public void PrepareWithoutResourceSetup(bool isFirstPrepare)
		{
		}

		// Token: 0x06001290 RID: 4752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114B")]
		[Address(RVA = "0x101193BC4", Offset = "0x1193BC4", VA = "0x101193BC4")]
		private void PrepareMain()
		{
		}

		// Token: 0x06001291 RID: 4753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114C")]
		[Address(RVA = "0x101194AA4", Offset = "0x1194AA4", VA = "0x101194AA4")]
		public void SetupCharaModel(GameObject[] modelObjs)
		{
		}

		// Token: 0x06001292 RID: 4754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114D")]
		[Address(RVA = "0x101194DF8", Offset = "0x1194DF8", VA = "0x101194DF8")]
		public void SetupWeaponModel(GameObject[] modelObjs)
		{
		}

		// Token: 0x06001293 RID: 4755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114E")]
		[Address(RVA = "0x101195264", Offset = "0x1195264", VA = "0x101195264")]
		public void SetupAnimClip_Open(int partsIndex)
		{
		}

		// Token: 0x06001294 RID: 4756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600114F")]
		[Address(RVA = "0x101195278", Offset = "0x1195278", VA = "0x101195278")]
		public void SetupAnimClip(string actionKey, GameObject animPrefab, int partsIndex)
		{
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001150")]
		[Address(RVA = "0x10119536C", Offset = "0x119536C", VA = "0x10119536C")]
		public void SetupAnimClip_Close(int partsIndex)
		{
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001151")]
		[Address(RVA = "0x101195380", Offset = "0x1195380", VA = "0x101195380")]
		public void SetupFacialAnimClip(GameObject animPrefab)
		{
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001152")]
		[Address(RVA = "0x101195458", Offset = "0x1195458", VA = "0x101195458")]
		public void SetupShadow(GameObject modelObj)
		{
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001153")]
		[Address(RVA = "0x101194B5C", Offset = "0x1194B5C", VA = "0x101194B5C")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06001299 RID: 4761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001154")]
		[Address(RVA = "0x10119550C", Offset = "0x119550C", VA = "0x10119550C")]
		public void SetEnableRenderWeapon(bool flg)
		{
		}

		// Token: 0x0600129A RID: 4762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001155")]
		[Address(RVA = "0x101194F58", Offset = "0x1194F58", VA = "0x101194F58")]
		public void SetEnableRenderWeapon(CharacterDefine.eWeaponIndex wpnIndex, bool flg, bool isIgnoreMasterEnable = false)
		{
		}

		// Token: 0x0600129B RID: 4763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001156")]
		[Address(RVA = "0x101195738", Offset = "0x1195738", VA = "0x101195738")]
		public void RevertDefaultEnableRenderWeapon()
		{
		}

		// Token: 0x0600129C RID: 4764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001157")]
		[Address(RVA = "0x1011957B8", Offset = "0x11957B8", VA = "0x1011957B8")]
		public void SetEnableRenderShadow(bool flg)
		{
		}

		// Token: 0x0600129D RID: 4765 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001158")]
		[Address(RVA = "0x10119589C", Offset = "0x119589C", VA = "0x10119589C")]
		public string GetNickName()
		{
			return null;
		}

		// Token: 0x0600129E RID: 4766 RVA: 0x00007FE0 File Offset: 0x000061E0
		[Token(Token = "0x6001159")]
		[Address(RVA = "0x101195998", Offset = "0x1195998", VA = "0x101195998")]
		public int GetWeaponClassAnimType()
		{
			return 0;
		}

		// Token: 0x0600129F RID: 4767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600115A")]
		[Address(RVA = "0x101194138", Offset = "0x1194138", VA = "0x101194138")]
		public void ResetScale()
		{
		}

		// Token: 0x060012A0 RID: 4768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600115B")]
		[Address(RVA = "0x1011959B4", Offset = "0x11959B4", VA = "0x1011959B4")]
		public void PlayVoice(string cueName, float volume = 1f)
		{
		}

		// Token: 0x060012A1 RID: 4769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600115C")]
		[Address(RVA = "0x101195B1C", Offset = "0x1195B1C", VA = "0x101195B1C")]
		public void PlayVoice(eSoundVoiceListDB cueID, float volume = 1f)
		{
		}

		// Token: 0x060012A2 RID: 4770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600115D")]
		[Address(RVA = "0x101195C74", Offset = "0x1195C74", VA = "0x101195C74")]
		public void PlayVoiceRandom(eSoundVoiceListDB[] cueIDs, float volume = 1f)
		{
		}

		// Token: 0x060012A3 RID: 4771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600115E")]
		[Address(RVA = "0x101195D10", Offset = "0x1195D10", VA = "0x101195D10")]
		public void PlayVoiceRandom(string[] cueNames, float volume = 1f)
		{
		}

		// Token: 0x060012A4 RID: 4772 RVA: 0x00007FF8 File Offset: 0x000061F8
		[Token(Token = "0x600115F")]
		[Address(RVA = "0x101195DAC", Offset = "0x1195DAC", VA = "0x101195DAC")]
		public bool IsPlayingVoice()
		{
			return default(bool);
		}

		// Token: 0x060012A5 RID: 4773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001160")]
		[Address(RVA = "0x101195DE0", Offset = "0x1195DE0", VA = "0x101195DE0")]
		private void OnCompleteMoveTo()
		{
		}

		// Token: 0x060012A6 RID: 4774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001161")]
		[Address(RVA = "0x101195DF8", Offset = "0x1195DF8", VA = "0x101195DF8")]
		private void OnCompleteStunScale()
		{
		}

		// Token: 0x060012A7 RID: 4775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001162")]
		[Address(RVA = "0x101196034", Offset = "0x1196034", VA = "0x101196034")]
		public CharacterHandler()
		{
		}

		// Token: 0x040014F5 RID: 5365
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000F29")]
		private Transform m_Transform;

		// Token: 0x040014F6 RID: 5366
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000F2A")]
		private Collider m_TouchCollider;

		// Token: 0x040014F7 RID: 5367
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000F2B")]
		private CharacterParam m_CharaParam;

		// Token: 0x040014F8 RID: 5368
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000F2C")]
		private NamedParam m_NamedParam;

		// Token: 0x040014F9 RID: 5369
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000F2D")]
		private WeaponParam m_WeaponParam;

		// Token: 0x040014FA RID: 5370
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000F2E")]
		private AbilityParam m_AbilityParam;

		// Token: 0x040014FB RID: 5371
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000F2F")]
		private CharacterResource m_CharaResource;

		// Token: 0x040014FC RID: 5372
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000F30")]
		private CharacterBattle m_CharaBattle;

		// Token: 0x040014FD RID: 5373
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000F31")]
		private CharacterMenu m_CharaMenu;

		// Token: 0x040014FE RID: 5374
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000F32")]
		private CharacterAnim m_CharaAnim;

		// Token: 0x040014FF RID: 5375
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000F33")]
		private CharacterDefine.eMode m_Mode;

		// Token: 0x04001500 RID: 5376
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4000F34")]
		private bool m_IsUserControll;

		// Token: 0x04001501 RID: 5377
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000F35")]
		public CharacterDefine.eFriendType m_FriendType;

		// Token: 0x04001502 RID: 5378
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4000F36")]
		private CharacterHandler.ePrepareWithoutResourceSetup m_PrepareWithoutResourceSetup;

		// Token: 0x04001503 RID: 5379
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000F37")]
		private bool m_IsPreparing;

		// Token: 0x04001504 RID: 5380
		[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
		[Token(Token = "0x4000F38")]
		private bool m_IsEnableRender;

		// Token: 0x04001505 RID: 5381
		[Cpp2IlInjected.FieldOffset(Offset = "0x7A")]
		[Token(Token = "0x4000F39")]
		private bool m_IsEnableRenderWeapon;

		// Token: 0x04001506 RID: 5382
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000F3A")]
		private bool[] m_IsEnableRenderWeapons;

		// Token: 0x04001507 RID: 5383
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000F3B")]
		private bool m_IsEnableRenderShadow;

		// Token: 0x04001508 RID: 5384
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000F3C")]
		private SoundHandler m_VoiceSoundHndl;

		// Token: 0x04001509 RID: 5385
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000F3D")]
		private SoundDefine.Selector m_Selector;

		// Token: 0x0400150A RID: 5386
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000F3E")]
		private float m_DispScale;

		// Token: 0x0200046F RID: 1135
		[Token(Token = "0x2000DCF")]
		public enum ePrepareWithoutResourceSetup
		{
			// Token: 0x0400150C RID: 5388
			[Token(Token = "0x400594E")]
			None = -1,
			// Token: 0x0400150D RID: 5389
			[Token(Token = "0x400594F")]
			First,
			// Token: 0x0400150E RID: 5390
			[Token(Token = "0x4005950")]
			Second,
			// Token: 0x0400150F RID: 5391
			[Token(Token = "0x4005951")]
			Done
		}
	}
}
