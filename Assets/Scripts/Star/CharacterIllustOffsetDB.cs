﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004C7 RID: 1223
	[Token(Token = "0x20003BF")]
	[StructLayout(3)]
	public class CharacterIllustOffsetDB : ScriptableObject
	{
		// Token: 0x06001402 RID: 5122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B7")]
		[Address(RVA = "0x10119612C", Offset = "0x119612C", VA = "0x10119612C")]
		public CharacterIllustOffsetDB()
		{
		}

		// Token: 0x04001717 RID: 5911
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010FD")]
		public CharacterIllustOffsetDB_Param[] m_Params;
	}
}
