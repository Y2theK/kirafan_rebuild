﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000501 RID: 1281
	[Token(Token = "0x20003F7")]
	[StructLayout(3)]
	public static class CharacterIllustOffsetDB_Ext
	{
		// Token: 0x0600152A RID: 5418 RVA: 0x00008EB0 File Offset: 0x000070B0
		[Token(Token = "0x60013DF")]
		[Address(RVA = "0x101196134", Offset = "0x1196134", VA = "0x101196134")]
		public static CharacterIllustOffsetDB_Param GetParam(this CharacterIllustOffsetDB self, int charaID)
		{
			return default(CharacterIllustOffsetDB_Param);
		}
	}
}
