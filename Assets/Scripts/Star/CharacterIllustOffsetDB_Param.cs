﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004C6 RID: 1222
	[Token(Token = "0x20003BE")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct CharacterIllustOffsetDB_Param
	{
		// Token: 0x0400170F RID: 5903
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010F5")]
		public int m_CharaID;

		// Token: 0x04001710 RID: 5904
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010F6")]
		public float m_BustEyeOffsetY;

		// Token: 0x04001711 RID: 5905
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40010F7")]
		public float m_BustSupportX;

		// Token: 0x04001712 RID: 5906
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40010F8")]
		public float m_BustSupportY;

		// Token: 0x04001713 RID: 5907
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40010F9")]
		public float m_BustSupportScl;

		// Token: 0x04001714 RID: 5908
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40010FA")]
		public float[] m_GachaCutInPivotYs;

		// Token: 0x04001715 RID: 5909
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40010FB")]
		public float m_SelectionX;

		// Token: 0x04001716 RID: 5910
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40010FC")]
		public float m_SelectionY;
	}
}
