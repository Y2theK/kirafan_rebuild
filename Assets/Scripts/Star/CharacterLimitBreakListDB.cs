﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004C9 RID: 1225
	[Token(Token = "0x20003C1")]
	[StructLayout(3)]
	public class CharacterLimitBreakListDB : ScriptableObject
	{
		// Token: 0x06001403 RID: 5123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B8")]
		[Address(RVA = "0x101196244", Offset = "0x1196244", VA = "0x101196244")]
		public CharacterLimitBreakListDB()
		{
		}

		// Token: 0x04001723 RID: 5923
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001109")]
		public CharacterLimitBreakListDB_Param[] m_Params;
	}
}
