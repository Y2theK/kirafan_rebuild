﻿namespace Star {
    public static class CharacterLimitBreakListDB_Ext {
        public static CharacterLimitBreakListDB_Param GetParam(this CharacterLimitBreakListDB self, int recipeID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_RecipeID == recipeID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }
    }
}
