﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004C8 RID: 1224
	[Token(Token = "0x20003C0")]
	[Serializable]
	[StructLayout(0, Size = 56)]
	public struct CharacterLimitBreakListDB_Param
	{
		// Token: 0x04001718 RID: 5912
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40010FE")]
		public int m_RecipeID;

		// Token: 0x04001719 RID: 5913
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40010FF")]
		public int m_Rare;

		// Token: 0x0400171A RID: 5914
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001100")]
		public int m_Class;

		// Token: 0x0400171B RID: 5915
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001101")]
		public int m_Amount;

		// Token: 0x0400171C RID: 5916
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001102")]
		public int m_ItemID;

		// Token: 0x0400171D RID: 5917
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001103")]
		public int m_ItemNum;

		// Token: 0x0400171E RID: 5918
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001104")]
		public int m_AllClassItemID;

		// Token: 0x0400171F RID: 5919
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001105")]
		public int m_AllClassItemNum;

		// Token: 0x04001720 RID: 5920
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001106")]
		public int m_TitleItemNum;

		// Token: 0x04001721 RID: 5921
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001107")]
		public int[] m_CharaMaxLvUps;

		// Token: 0x04001722 RID: 5922
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001108")]
		public int[] m_SkillMaxLvUps;
	}
}
