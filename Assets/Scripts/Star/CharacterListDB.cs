﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004CB RID: 1227
	[Token(Token = "0x20003C3")]
	[StructLayout(3)]
	public class CharacterListDB : ScriptableObject
	{
		// Token: 0x06001404 RID: 5124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012B9")]
		[Address(RVA = "0x101196360", Offset = "0x1196360", VA = "0x101196360")]
		public CharacterListDB()
		{
		}

		// Token: 0x04001745 RID: 5957
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400112B")]
		public CharacterListDB_Param[] m_Params;
	}
}
