﻿using System.Collections.Generic;

namespace Star {
    public static class CharacterListDB_Ext {
        public static CharacterListDB_Param GetParam(this CharacterListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CharaID == charaID) {
                    return self.m_Params[i];
                }
            }
            return self.m_Params[0];
        }

        public static CharacterListDB_Param? GetParamNullable(this CharacterListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CharaID == charaID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static bool IsExistParam(this CharacterListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CharaID == charaID) {
                    return true;
                }
            }
            return false;
        }

        public static List<CharacterListDB_Param> GetParamsByNamedType(this CharacterListDB self, eCharaNamedType namedType) {
            List<CharacterListDB_Param> dbParams = new List<CharacterListDB_Param>();
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_NamedType == (int)namedType) {
                    dbParams.Add(self.m_Params[i]);
                }
            }
            return dbParams;
        }

        public static bool IsSameNamedType(this CharacterListDB self, int charaID, List<int> charaIDs) {
            int namedType = self.GetParam(charaID).m_NamedType;
            for (int i = 0; i < charaIDs.Count; i++) {
                if (namedType == self.GetParam(charaIDs[i]).m_NamedType) {
                    return true;
                }
            }
            return false;
        }

        public static eCharaNamedType GetNamedType(this CharacterListDB self, int charaID) {
            return (eCharaNamedType)self.GetNamedTypeInt(charaID);
        }

        public static int GetNamedTypeInt(this CharacterListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CharaID == charaID) {
                    return self.m_Params[i].m_NamedType;
                }
            }
            return -1;
        }

        public static bool IsDedicatedAnimType_CharaSkill(this CharacterListDB_Param self) {
            return self.m_DedicatedAnimType == (int)CharacterDefine.eDedicatedAnimType.DedicatedCharaSkill;
        }

        public static bool IsDedicatedAnimType_ForChara(this CharacterListDB_Param self) {
            return self.m_DedicatedAnimType == (int)CharacterDefine.eDedicatedAnimType.DedicatedForChara;
        }
    }
}
