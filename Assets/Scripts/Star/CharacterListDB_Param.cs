﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004CA RID: 1226
	[Token(Token = "0x20003C2")]
	[Serializable]
	[StructLayout(0, Size = 152)]
	public struct CharacterListDB_Param
	{
		// Token: 0x04001724 RID: 5924
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400110A")]
		public int m_CharaID;

		// Token: 0x04001725 RID: 5925
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400110B")]
		public int m_ResourceID;

		// Token: 0x04001726 RID: 5926
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400110C")]
		public int m_DedicatedAnimType;

		// Token: 0x04001727 RID: 5927
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400110D")]
		public int m_HeadID;

		// Token: 0x04001728 RID: 5928
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400110E")]
		public int m_BodyID;

		// Token: 0x04001729 RID: 5929
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400110F")]
		public float m_DispScale;

		// Token: 0x0400172A RID: 5930
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001110")]
		public string m_CRILabel;

		// Token: 0x0400172B RID: 5931
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001111")]
		public string m_Name;

		// Token: 0x0400172C RID: 5932
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001112")]
		public int m_NamedType;

		// Token: 0x0400172D RID: 5933
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001113")]
		public int m_Rare;

		// Token: 0x0400172E RID: 5934
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001114")]
		public int m_Class;

		// Token: 0x0400172F RID: 5935
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001115")]
		public int m_Element;

		// Token: 0x04001730 RID: 5936
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001116")]
		public int m_Cost;

		// Token: 0x04001731 RID: 5937
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001117")]
		public sbyte m_GrowthTableID;

		// Token: 0x04001732 RID: 5938
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001118")]
		public int m_InitLv;

		// Token: 0x04001733 RID: 5939
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001119")]
		public int m_InitLimitLv;

		// Token: 0x04001734 RID: 5940
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400111A")]
		public int m_InitHp;

		// Token: 0x04001735 RID: 5941
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400111B")]
		public int m_InitAtk;

		// Token: 0x04001736 RID: 5942
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400111C")]
		public int m_InitMgc;

		// Token: 0x04001737 RID: 5943
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400111D")]
		public int m_InitDef;

		// Token: 0x04001738 RID: 5944
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400111E")]
		public int m_InitMDef;

		// Token: 0x04001739 RID: 5945
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400111F")]
		public int m_InitSpd;

		// Token: 0x0400173A RID: 5946
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4001120")]
		public int m_InitLuck;

		// Token: 0x0400173B RID: 5947
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4001121")]
		public int m_SkillLimitLv;

		// Token: 0x0400173C RID: 5948
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4001122")]
		public int m_CharaSkillID;

		// Token: 0x0400173D RID: 5949
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4001123")]
		public sbyte m_CharaSkillExpTableID;

		// Token: 0x0400173E RID: 5950
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4001124")]
		public int[] m_ClassSkillIDs;

		// Token: 0x0400173F RID: 5951
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4001125")]
		public sbyte[] m_ClassSkillExpTableIDs;

		// Token: 0x04001740 RID: 5952
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4001126")]
		public float m_StunCoef;

		// Token: 0x04001741 RID: 5953
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4001127")]
		public int m_AltItemID;

		// Token: 0x04001742 RID: 5954
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4001128")]
		public int m_AltItemAmount;

		// Token: 0x04001743 RID: 5955
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4001129")]
		public byte m_FULLOPEN;

		// Token: 0x04001744 RID: 5956
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400112A")]
		public int m_LimitBreakRecipeID;
	}
}
