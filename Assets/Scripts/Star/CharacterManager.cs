﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000470 RID: 1136
	[Token(Token = "0x2000374")]
	[StructLayout(3)]
	public sealed class CharacterManager
	{
		// Token: 0x060012A8 RID: 4776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001163")]
		[Address(RVA = "0x101196AF8", Offset = "0x1196AF8", VA = "0x101196AF8")]
		public CharacterManager()
		{
		}

		// Token: 0x060012A9 RID: 4777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001164")]
		[Address(RVA = "0x101196B6C", Offset = "0x1196B6C", VA = "0x101196B6C")]
		public void Update()
		{
		}

		// Token: 0x060012AA RID: 4778 RVA: 0x00008010 File Offset: 0x00006210
		[Token(Token = "0x6001165")]
		[Address(RVA = "0x101196B70", Offset = "0x1196B70", VA = "0x101196B70")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x060012AB RID: 4779 RVA: 0x00008028 File Offset: 0x00006228
		[Token(Token = "0x6001166")]
		[Address(RVA = "0x101196B80", Offset = "0x1196B80", VA = "0x101196B80")]
		public int GetCharaNum()
		{
			return 0;
		}

		// Token: 0x060012AC RID: 4780 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001167")]
		[Address(RVA = "0x101196BE0", Offset = "0x1196BE0", VA = "0x101196BE0")]
		public CharacterHandler GetCharaAt(int index)
		{
			return null;
		}

		// Token: 0x060012AD RID: 4781 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001168")]
		[Address(RVA = "0x101196C94", Offset = "0x1196C94", VA = "0x101196C94")]
		public CharacterHandler FindChara(uint uniqueID)
		{
			return null;
		}

		// Token: 0x060012AE RID: 4782 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001169")]
		[Address(RVA = "0x101196DA4", Offset = "0x1196DA4", VA = "0x101196DA4")]
		public List<CharacterHandler> GetCharas()
		{
			return null;
		}

		// Token: 0x060012AF RID: 4783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600116A")]
		[Address(RVA = "0x101196DAC", Offset = "0x1196DAC", VA = "0x101196DAC")]
		public void DestroyCharaAt(int index)
		{
		}

		// Token: 0x060012B0 RID: 4784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600116B")]
		[Address(RVA = "0x101196FC4", Offset = "0x1196FC4", VA = "0x101196FC4")]
		public void DestroyChara(CharacterHandler charaHndl)
		{
		}

		// Token: 0x060012B1 RID: 4785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600116C")]
		[Address(RVA = "0x1011970D4", Offset = "0x11970D4", VA = "0x1011970D4")]
		public void DestroyCharaAll()
		{
		}

		// Token: 0x060012B2 RID: 4786 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600116D")]
		[Address(RVA = "0x101197150", Offset = "0x1197150", VA = "0x101197150")]
		public CharacterHandler InstantiateCharacter(CharacterParam srcCharaParam, NamedParam srcNamedParam, WeaponParam srcWeaponParam, AbilityParam srcAbilityParam, eCharaResourceType resourceType, int resourceID, CharacterDefine.eMode mode, bool isUserControll, CharacterDefine.eFriendType friendType, string name = "", [Optional] Transform parent, float dispScale = 0f)
		{
			return null;
		}

		// Token: 0x060012B3 RID: 4787 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600116E")]
		[Address(RVA = "0x101197320", Offset = "0x1197320", VA = "0x101197320")]
		private CharacterHandler _InstantiateCharacter(string name, Transform parent)
		{
			return null;
		}

		// Token: 0x060012B4 RID: 4788 RVA: 0x00008040 File Offset: 0x00006240
		[Token(Token = "0x600116F")]
		[Address(RVA = "0x1011974A8", Offset = "0x11974A8", VA = "0x1011974A8")]
		private uint GetNewUniqueID()
		{
			return 0U;
		}

		// Token: 0x04001510 RID: 5392
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000F3F")]
		private List<CharacterHandler> m_CharaList;

		// Token: 0x04001511 RID: 5393
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000F40")]
		private uint m_UniqueIDGenerator;
	}
}
