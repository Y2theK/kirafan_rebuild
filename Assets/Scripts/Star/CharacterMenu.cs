﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000471 RID: 1137
	[Token(Token = "0x2000375")]
	[StructLayout(3)]
	public class CharacterMenu
	{
		// Token: 0x060012B5 RID: 4789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001170")]
		[Address(RVA = "0x1011948C4", Offset = "0x11948C4", VA = "0x1011948C4")]
		public CharacterMenu()
		{
		}

		// Token: 0x060012B6 RID: 4790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001171")]
		[Address(RVA = "0x101193CC4", Offset = "0x1193CC4", VA = "0x101193CC4")]
		public void Update()
		{
		}

		// Token: 0x060012B7 RID: 4791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001172")]
		[Address(RVA = "0x1011948D4", Offset = "0x11948D4", VA = "0x1011948D4")]
		public void Setup(CharacterHandler charaHndl)
		{
		}

		// Token: 0x060012B8 RID: 4792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001173")]
		[Address(RVA = "0x1011942A8", Offset = "0x11942A8", VA = "0x1011942A8")]
		public void Destroy()
		{
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x060012B9 RID: 4793 RVA: 0x00008058 File Offset: 0x00006258
		[Token(Token = "0x1700012E")]
		public CharacterMenu.eMode Mode
		{
			[Token(Token = "0x6001174")]
			[Address(RVA = "0x101197714", Offset = "0x1197714", VA = "0x101197714")]
			get
			{
				return CharacterMenu.eMode.Idle;
			}
		}

		// Token: 0x060012BA RID: 4794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001175")]
		[Address(RVA = "0x1011974C0", Offset = "0x11974C0", VA = "0x1011974C0")]
		public void RequestIdleMode(float blendSec = 0f, float delaySec = 0f)
		{
		}

		// Token: 0x060012BB RID: 4795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001176")]
		[Address(RVA = "0x1011974BC", Offset = "0x11974BC", VA = "0x1011974BC")]
		private void UpdateIdleMode()
		{
		}

		// Token: 0x04001512 RID: 5394
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000F41")]
		private CharacterHandler m_Owner;

		// Token: 0x04001513 RID: 5395
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000F42")]
		private float m_BlendSec;

		// Token: 0x04001514 RID: 5396
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000F43")]
		private int m_BodyID;

		// Token: 0x04001515 RID: 5397
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000F44")]
		private CharacterMenu.eMode m_Mode;

		// Token: 0x04001516 RID: 5398
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000F45")]
		private CharacterMenu.eUpdateIdleStep m_UpdateIdleStep;

		// Token: 0x04001517 RID: 5399
		[Token(Token = "0x4000F46")]
		public const float DEFAULT_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x02000472 RID: 1138
		[Token(Token = "0x2000DD0")]
		public enum eMode
		{
			// Token: 0x04001519 RID: 5401
			[Token(Token = "0x4005953")]
			None = -1,
			// Token: 0x0400151A RID: 5402
			[Token(Token = "0x4005954")]
			Idle
		}

		// Token: 0x02000473 RID: 1139
		[Token(Token = "0x2000DD1")]
		private enum eUpdateIdleStep
		{
			// Token: 0x0400151C RID: 5404
			[Token(Token = "0x4005956")]
			None = -1,
			// Token: 0x0400151D RID: 5405
			[Token(Token = "0x4005957")]
			ExecAnimation_Wait,
			// Token: 0x0400151E RID: 5406
			[Token(Token = "0x4005958")]
			End
		}
	}
}
