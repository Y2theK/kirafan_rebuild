﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004CE RID: 1230
	[Token(Token = "0x20003C6")]
	[StructLayout(3)]
	public class CharacterOverrideDB : ScriptableObject
	{
		// Token: 0x06001405 RID: 5125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BA")]
		[Address(RVA = "0x10119771C", Offset = "0x119771C", VA = "0x10119771C")]
		public CharacterOverrideDB()
		{
		}

		// Token: 0x04001750 RID: 5968
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001136")]
		public CharacterOverrideDB_Param[] m_Params;
	}
}
