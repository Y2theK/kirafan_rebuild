﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004CC RID: 1228
	[Token(Token = "0x20003C4")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct CharacterOverrideDB_Data
	{
		// Token: 0x04001746 RID: 5958
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400112C")]
		public string m_SapSrcSkillActionID;

		// Token: 0x04001747 RID: 5959
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400112D")]
		public string m_SapDstSkillActionID;

		// Token: 0x04001748 RID: 5960
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400112E")]
		public string m_SagSrcSkillActionID;

		// Token: 0x04001749 RID: 5961
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400112F")]
		public string m_SagDstSkillActionID;
	}
}
