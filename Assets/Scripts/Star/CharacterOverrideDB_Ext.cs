﻿namespace Star {
    public static class CharacterOverrideDB_Ext {
        public static CharacterOverrideDB_Param? GetParam(this CharacterOverrideDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                for (int j = 0; j < self.m_Params[i].m_CharaIDs.Length; j++) {
                    if (self.m_Params[i].m_CharaIDs[j] == charaID) {
                        return self.m_Params[i];
                    }
                }
            }
            return null;
        }

        public static string SAPConvert(this CharacterOverrideDB self, int charaID, string srcSkillActionID) {
            CharacterOverrideDB_Param? param = self.GetParam(charaID);
            if (param.HasValue) {
                for (int i = 0; i < param.Value.m_Datas.Length; i++) {
                    if (param.Value.m_Datas[i].m_SapSrcSkillActionID == srcSkillActionID) {
                        return param.Value.m_Datas[i].m_SapDstSkillActionID;
                    }
                }
            }
            return srcSkillActionID;
        }

        public static string SAGConvert(this CharacterOverrideDB self, int charaID, string srcSkillActionID) {
            CharacterOverrideDB_Param? param = self.GetParam(charaID);
            if (param.HasValue) {
                for (int i = 0; i < param.Value.m_Datas.Length; i++) {
                    if (param.Value.m_Datas[i].m_SagSrcSkillActionID == srcSkillActionID) {
                        return param.Value.m_Datas[i].m_SagDstSkillActionID;
                    }
                }
            }
            return srcSkillActionID;
        }
    }
}
