﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004CD RID: 1229
	[Token(Token = "0x20003C5")]
	[Serializable]
	[StructLayout(0)]
	public struct CharacterOverrideDB_Param
	{
		// Token: 0x0400174A RID: 5962
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001130")]
		public int[] m_CharaIDs;

		// Token: 0x0400174B RID: 5963
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001131")]
		public int m_NormalAttackSkillID;

		// Token: 0x0400174C RID: 5964
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001132")]
		public int m_DefaultWeaponID;

		// Token: 0x0400174D RID: 5965
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001133")]
		public int m_GeneralWeaponID;

		// Token: 0x0400174E RID: 5966
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001134")]
		public float m_ShadowOffsetXInIdle;

		// Token: 0x0400174F RID: 5967
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001135")]
		public CharacterOverrideDB_Data[] m_Datas;
	}
}
