﻿using System;
using System.Collections.Generic;

namespace Star {
    [Serializable]
    public class CharacterParam {
        public long MngID { get; set; }
        public uint UniqueID { get; set; }
        public int CharaID { get; set; }
        public int ViewCharaID { get; set; }
        public int Lv { get; set; }
        public int MaxLv { get; set; }
        public long Exp { get; set; }
        public int LimitBreak { get; set; }
        public int ArousalLv { get; set; }
        public int DuplicatedCount { get; set; }
        public List<SkillLearnData> UniqueSkillLearnDatas { get; set; }
        public SkillLearnData UniqueSkillLearnData => UniqueSkillLearnDatas[0];
        public List<SkillLearnData> ClassSkillLearnDatas { get; set; }
        public eCharaNamedType NamedType { get; set; }
        public eRare RareType { get; set; }
        public eClassType ClassType { get; set; }
        public eElementType ElementType { get; set; }
        public int Cost { get; set; }
        public int Hp { get; set; }
        public int Atk { get; set; }
        public int Mgc { get; set; }
        public int Def { get; set; }
        public int MDef { get; set; }
        public int Spd { get; set; }
        public int Luck { get; set; }
        public bool[] IsRegistAbnormals { get; set; }
        public int AIID { get; set; }
        public int ChargeCountMax { get; set; }
        public float StunCoef { get; set; }
        public float StunerMag { get; set; }
        public float StunerAvoid { get; set; }

        public CharacterParam() {
            MngID = -1L;
            AIID = -1;
        }

        public int GetParam(BattleDefine.eStatus st) {
            return st switch {
                BattleDefine.eStatus.Atk => Atk,
                BattleDefine.eStatus.Mgc => Mgc,
                BattleDefine.eStatus.Def => Def,
                BattleDefine.eStatus.MDef => MDef,
                BattleDefine.eStatus.Spd => Spd,
                BattleDefine.eStatus.Luck => Luck,
                _ => 0,
            };
        }
    }
}
