﻿using Star.Town;
using UnityEngine;

namespace Star {
    public class CharacterParamCalculator {
        private int m_srcCharaID;
        private int m_srcCharaLv;
        private long m_srcCharaExp;
        private int m_srcFriendship;
        private int m_srcArousalLv;
        private int m_srcWeaponID;
        private int m_srcWeaponLv;
        private int m_srcOrbID;
        private int m_srcOrbLv;
        private int[] m_srcAbilityEquipItemIDs;

        public CharacterParamCalculator() {
            Clear();
        }

        public void Clear() {
            m_srcCharaID = -1;
            m_srcCharaLv = -1;
            m_srcCharaExp = 0;
            m_srcFriendship = 1;
            m_srcArousalLv = 0;
            m_srcWeaponID = -1;
            m_srcWeaponLv = -1;
            m_srcOrbID = -1;
            m_srcOrbLv = -1;
            m_srcAbilityEquipItemIDs = null;
        }

        public CharacterParamCalculator SetCharaID(int value) {
            m_srcCharaID = value;
            return this;
        }

        public CharacterParamCalculator SetCharaLv(int value) {
            m_srcCharaLv = value;
            return this;
        }

        public CharacterParamCalculator SetCharaExp(long value) {
            m_srcCharaExp = value;
            return this;
        }

        public CharacterParamCalculator SetFriendship(int value) {
            m_srcFriendship = value;
            return this;
        }

        public CharacterParamCalculator SetArousalLv(int value) {
            m_srcArousalLv = value;
            return this;
        }

        public CharacterParamCalculator SetWeaponID(int value) {
            m_srcWeaponID = value;
            return this;
        }

        public CharacterParamCalculator SetWeaponLv(int value) {
            m_srcWeaponLv = value;
            return this;
        }

        public CharacterParamCalculator SetOrbID(int value) {
            m_srcOrbID = value;
            return this;
        }

        public CharacterParamCalculator SetOrbLv(int value) {
            m_srcOrbLv = value;
            return this;
        }

        public CharacterParamCalculator SetAbilityEquipItemIDs(int[] value) {
            m_srcAbilityEquipItemIDs = value;
            return this;
        }

        public OutputCharaParam Calc(eCharaParamCalc flg) {
            OutputCharaParam lvParam = default;
            if (flg.HasFlag(eCharaParamCalc.Lv)) {
                lvParam = CalcCharaParamLevelOnly(m_srcCharaID, m_srcCharaLv);
            }
            OutputCharaParam weaponParam = default;
            if (flg.HasFlag(eCharaParamCalc.Weapon)) {
                weaponParam = CalcOutputWeaponParam(m_srcWeaponID, m_srcWeaponLv);
            }
            OutputCharaParam arousalParam = default;
            if (flg.HasFlag(eCharaParamCalc.Arousal)) {
                CharacterListDB_Param param = GameSystem.Inst.DbMng.CharaListDB.GetParam(m_srcCharaID);
                arousalParam = CalcArousalParam((eRare)param.m_Rare, m_srcArousalLv);
            }
            OutputCharaParam abilityParam = default;
            if (flg.HasFlag(eCharaParamCalc.Ability)) {
                abilityParam = CalcAbilityParam(m_srcAbilityEquipItemIDs);
            }
            OutputCharaPercentParam friendshipParam = default;
            if (flg.HasFlag(eCharaParamCalc.Friendship)) {
                friendshipParam = CalcFriendshipRate(m_srcCharaID, m_srcFriendship);
            }
            OutputCharaPercentParam townParam = default;
            if (flg.HasFlag(eCharaParamCalc.Town)) {
                townParam = CalcTownBuffRate(m_srcCharaID);
            }
            OutputCharaPercentParam orbParam = default;
            if (flg.HasFlag(eCharaParamCalc.Orb)) {
                orbParam = CalcOrbBuffRate(m_srcOrbID, m_srcOrbLv, m_srcCharaID);
            }
            return CalcCharaParam(m_srcCharaLv, m_srcCharaExp, ref lvParam, ref weaponParam, ref arousalParam, ref abilityParam, ref friendshipParam, ref townParam, ref orbParam);
        }

        public static OutputCharaParam CalcCharaParam(int currentLv, long exp, ref OutputCharaParam lvBaseCharaParam, ref OutputCharaParam weaponParam, ref OutputCharaParam arousalParam, ref OutputCharaParam abilityParam, ref OutputCharaPercentParam friendshipPercentParam, ref OutputCharaPercentParam townBuffPercentParam, ref OutputCharaPercentParam orbBuffPercentParam) {
            OutputCharaParam res = new OutputCharaParam();
            res.Hp = lvBaseCharaParam.Hp;
            res.Atk = lvBaseCharaParam.Atk;
            res.Mgc = lvBaseCharaParam.Mgc;
            res.Def = lvBaseCharaParam.Def;
            res.MDef = lvBaseCharaParam.MDef;
            res.Spd = lvBaseCharaParam.Spd;
            res.Luck = lvBaseCharaParam.Luck;
            res.Hp = CalcParamCorrect(res.Hp, friendshipPercentParam.Hp);
            res.Atk = CalcParamCorrect(res.Atk, friendshipPercentParam.Atk);
            res.Mgc = CalcParamCorrect(res.Mgc, friendshipPercentParam.Mgc);
            res.Def = CalcParamCorrect(res.Def, friendshipPercentParam.Def);
            res.MDef = CalcParamCorrect(res.MDef, friendshipPercentParam.MDef);
            res.Spd = CalcParamCorrect(res.Spd, friendshipPercentParam.Spd);
            res.Luck = CalcParamCorrect(res.Luck, friendshipPercentParam.Luck);
            res.Hp = CalcParamCorrect(res.Hp, townBuffPercentParam.Hp);
            res.Atk = CalcParamCorrect(res.Atk, townBuffPercentParam.Atk);
            res.Mgc = CalcParamCorrect(res.Mgc, townBuffPercentParam.Mgc);
            res.Def = CalcParamCorrect(res.Def, townBuffPercentParam.Def);
            res.MDef = CalcParamCorrect(res.MDef, townBuffPercentParam.MDef);
            res.Spd = CalcParamCorrect(res.Spd, townBuffPercentParam.Spd);
            res.Luck = CalcParamCorrect(res.Luck, townBuffPercentParam.Luck);
            res.Hp = CalcParamCorrect(res.Hp, orbBuffPercentParam.Hp);
            res.Atk = CalcParamCorrect(res.Atk, orbBuffPercentParam.Atk);
            res.Mgc = CalcParamCorrect(res.Mgc, orbBuffPercentParam.Mgc);
            res.Def = CalcParamCorrect(res.Def, orbBuffPercentParam.Def);
            res.MDef = CalcParamCorrect(res.MDef, orbBuffPercentParam.MDef);
            res.Spd = CalcParamCorrect(res.Spd, orbBuffPercentParam.Spd);
            res.Luck = CalcParamCorrect(res.Luck, orbBuffPercentParam.Luck);
            res.Hp += arousalParam.Hp;
            res.Atk += arousalParam.Atk;
            res.Mgc += arousalParam.Mgc;
            res.Def += arousalParam.Def;
            res.MDef += arousalParam.MDef;
            res.Hp += abilityParam.Hp;
            res.Atk += abilityParam.Atk;
            res.Mgc += abilityParam.Mgc;
            res.Def += abilityParam.Def;
            res.MDef += abilityParam.MDef;
            res.Atk += weaponParam.Atk;
            res.Mgc += weaponParam.Mgc;
            res.Def += weaponParam.Def;
            res.MDef += weaponParam.MDef;
            res.Lv = currentLv;
            res.NowExp = EditUtility.GetCharaNowExp(currentLv, exp);
            res.NextMaxExp = EditUtility.GetCharaNextMaxExp(currentLv);
            return res;
        }

        public static OutputCharaParam CalcCharaParamLevelOnly(int charaID, int currentLv) {
            OutputCharaParam res = new OutputCharaParam();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
            res.Lv = currentLv;
            res.Hp = dbMng.CharaParamGrowthListDB.CalcHp(param.m_InitHp, currentLv, param.m_GrowthTableID);
            res.Atk = dbMng.CharaParamGrowthListDB.CalcAtk(param.m_InitAtk, currentLv, param.m_GrowthTableID);
            res.Mgc = dbMng.CharaParamGrowthListDB.CalcMgc(param.m_InitMgc, currentLv, param.m_GrowthTableID);
            res.Def = dbMng.CharaParamGrowthListDB.CalcDef(param.m_InitDef, currentLv, param.m_GrowthTableID);
            res.MDef = dbMng.CharaParamGrowthListDB.CalcMDef(param.m_InitMDef, currentLv, param.m_GrowthTableID);
            res.Spd = dbMng.CharaParamGrowthListDB.CalcSpd(param.m_InitSpd, currentLv, param.m_GrowthTableID);
            res.Luck = dbMng.CharaParamGrowthListDB.CalcLuck(param.m_InitLuck, currentLv, param.m_GrowthTableID);
            return res;
        }

        public static OutputCharaParam CalcOutputWeaponParam(int weaponID, int weaponLv) {
            OutputCharaParam res = new OutputCharaParam();
            if (weaponID != -1 && !EditUtility.CheckDefaultWeapon(weaponID)) {
                WeaponListDB_Param param = GameSystem.Inst.DbMng.WeaponListDB.GetParam(weaponID);
                res.Atk += CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, param.m_InitLv, param.m_LimitLv, weaponLv);
                res.Mgc += CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, param.m_InitLv, param.m_LimitLv, weaponLv);
                res.Def += CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, param.m_InitLv, param.m_LimitLv, weaponLv);
                res.MDef += CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, param.m_InitLv, param.m_LimitLv, weaponLv);
            }
            return res;
        }

        public static OutputCharaParam CalcArousalParam(eRare rare, int arousalLv) {
            OutputCharaParam res = new OutputCharaParam();
            ArousalLevelsDB_Param? param = GameSystem.Inst.DbMng.ArousalLevelsDB.GetParam(rare, arousalLv);
            if (param.HasValue) {
                res.Hp = param.Value.m_Hp;
                res.Atk = param.Value.m_Atk;
                res.Mgc = param.Value.m_Mgc;
                res.Def = param.Value.m_Def;
                res.MDef = param.Value.m_MDef;
            }
            res.Spd = 0;
            res.Luck = 0;
            return res;
        }

        public static OutputCharaParam CalcAbilityParam(int[] abilityEquipItemIDs) {
            OutputCharaParam res = new OutputCharaParam();
            GameSystem.Inst.DbMng.AbilitySpheresDB.CalcStatus(abilityEquipItemIDs, out int hp, out int atk, out int mgc, out int def, out int mdef);
            res.Hp = hp;
            res.Atk = atk;
            res.Mgc = mgc;
            res.Def = def;
            res.MDef = mdef;
            res.Spd = 0;
            res.Luck = 0;
            return res;
        }

        public static OutputCharaPercentParam CalcFriendshipRate(int charaID, int friendship) {
            OutputCharaPercentParam res = new OutputCharaPercentParam();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            NamedListDB_Param namedParam = dbMng.NamedListDB.GetParam((eCharaNamedType)charaParam.m_NamedType);
            NamedFriendshipExpDB_Param friendParam = dbMng.NamedFriendshipExpDB.GetParam(friendship, namedParam.m_FriendshipTableID);
            res.Hp = friendParam.m_CorrectHp;
            res.Atk = friendParam.m_CorrectAtk;
            res.Mgc = friendParam.m_CorrectMgc;
            res.Def = friendParam.m_CorrectDef;
            res.MDef = friendParam.m_CorrectMDef;
            res.Spd = friendParam.m_CorrectSpd;
            res.Luck = friendParam.m_CorrectLuck;
            return res;
        }

        public static OutputCharaPercentParam CalcTownBuffRate(int charaID) {
            OutputCharaPercentParam res = new OutputCharaPercentParam();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            eTitleType titleType = dbMng.NamedListDB.GetTitleType((eCharaNamedType)charaParam.m_NamedType);
            float[] buffs = TownBuff.CalcTownBuff().GetStatusBuff(titleType, (eClassType)charaParam.m_Class, (eElementType)charaParam.m_Element);
            res.Hp = buffs[(int)eCharaStatus.Hp];
            res.Atk = buffs[(int)eCharaStatus.Atk];
            res.Mgc = buffs[(int)eCharaStatus.Mgc];
            res.Def = buffs[(int)eCharaStatus.Def];
            res.MDef = buffs[(int)eCharaStatus.MDef];
            res.Spd = buffs[(int)eCharaStatus.Spd];
            return res;
        }

        public static OutputCharaPercentParam CalcOrbBuffRate(int orbID, int orbLv, int targetCharaID) {
            OutputCharaPercentParam res = new OutputCharaPercentParam();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            MasterOrbListDB_Param? orbParam = dbMng.MasterOrbListDB.GetParamNullable(orbID);
            if (orbParam.HasValue && orbParam.Value.m_OrbBuffID != -1) {
                int namedType = dbMng.CharaListDB.GetNamedTypeInt(targetCharaID);
                int titleType = dbMng.NamedListDB.GetTitleTypeInt(namedType);
                if (orbParam.Value.m_TitleType == titleType) {
                    MasterOrbBuffsDB_Param? buffParam = dbMng.MasterOrbBuffsDB.GetParam(orbParam.Value.m_OrbBuffID, orbLv);
                    if (buffParam.HasValue) {
                        res.Hp = buffParam.Value.m_CorrectHp;
                        res.Atk = buffParam.Value.m_CorrectAtk;
                        res.Mgc = buffParam.Value.m_CorrectMgc;
                        res.Def = buffParam.Value.m_CorrectDef;
                        res.MDef = buffParam.Value.m_CorrectMDef;
                        res.Spd = buffParam.Value.m_CorrectSpd;
                        res.Luck = buffParam.Value.m_CorrectLuck;
                    }
                }
            }
            return res;
        }

        public static int CalcParamCorrect(int val, float correct) {
            return Mathf.CeilToInt((1f + correct * 0.01f) * val);
        }

        public static int CalcParamCorrectLv(int initVal, int maxVal, int initLv, int maxLv, int currentLv) {
            float factor = 1f;
            if (maxLv > 1) {
                factor -= (maxLv - currentLv) / (maxLv - initLv);
            }
            return Mathf.CeilToInt(initVal + factor * (maxVal - initVal));
        }
    }
}
