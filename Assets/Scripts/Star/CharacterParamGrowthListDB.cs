﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D0 RID: 1232
	[Token(Token = "0x20003C8")]
	[StructLayout(3)]
	public class CharacterParamGrowthListDB : ScriptableObject
	{
		// Token: 0x06001406 RID: 5126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BB")]
		[Address(RVA = "0x10119A388", Offset = "0x119A388", VA = "0x10119A388")]
		public CharacterParamGrowthListDB()
		{
		}

		// Token: 0x04001759 RID: 5977
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400113F")]
		public CharacterParamGrowthListDB_Param[] m_Params;
	}
}
