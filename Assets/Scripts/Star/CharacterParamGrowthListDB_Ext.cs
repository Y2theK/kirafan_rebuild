﻿using UnityEngine;

namespace Star {
    public static class CharacterParamGrowthListDB_Ext {
        public static int CalcHp(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthHp[tableID]);
            }
            return result;
        }

        public static int CalcAtk(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthAtk[tableID]);
            }
            return result;
        }

        public static int CalcMgc(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthMgc[tableID]);
            }
            return result;
        }

        public static int CalcDef(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthDef[tableID]);
            }
            return result;
        }

        public static int CalcMDef(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthMDef[tableID]);
            }
            return result;
        }

        public static int CalcSpd(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthSpd[tableID]);
            }
            return result;
        }

        public static int CalcLuck(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID) {
            int result = initValue;
            int idx = lv - 1;
            if (idx >= 0 && idx < self.m_Params.Length) {
                result = Mathf.CeilToInt(result * self.m_Params[idx].m_GrowthLuck[tableID]);
            }
            return result;
        }
    }
}
