﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004CF RID: 1231
	[Token(Token = "0x20003C7")]
	[Serializable]
	[StructLayout(0, Size = 64)]
	public struct CharacterParamGrowthListDB_Param
	{
		// Token: 0x04001751 RID: 5969
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001137")]
		public int m_Lv;

		// Token: 0x04001752 RID: 5970
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001138")]
		public float[] m_GrowthHp;

		// Token: 0x04001753 RID: 5971
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001139")]
		public float[] m_GrowthAtk;

		// Token: 0x04001754 RID: 5972
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400113A")]
		public float[] m_GrowthMgc;

		// Token: 0x04001755 RID: 5973
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400113B")]
		public float[] m_GrowthDef;

		// Token: 0x04001756 RID: 5974
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400113C")]
		public float[] m_GrowthMDef;

		// Token: 0x04001757 RID: 5975
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400113D")]
		public float[] m_GrowthSpd;

		// Token: 0x04001758 RID: 5976
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400113E")]
		public float[] m_GrowthLuck;
	}
}
