﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D3 RID: 1235
	[Token(Token = "0x20003CB")]
	[StructLayout(3)]
	public class CharacterQuestListDB : ScriptableObject
	{
		// Token: 0x06001407 RID: 5127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BC")]
		[Address(RVA = "0x10119A390", Offset = "0x119A390", VA = "0x10119A390")]
		public CharacterQuestListDB()
		{
		}

		// Token: 0x04001763 RID: 5987
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001149")]
		public CharacterQuestListDB_Param[] m_Params;
	}
}
