﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000506 RID: 1286
	[Token(Token = "0x20003FC")]
	[StructLayout(3)]
	public static class CharacterQuestListDB_Ext
	{
		// Token: 0x0600153F RID: 5439 RVA: 0x00009060 File Offset: 0x00007260
		[Token(Token = "0x60013F4")]
		[Address(RVA = "0x10119A398", Offset = "0x119A398", VA = "0x10119A398")]
		public static CharacterQuestListDB_Param? GetParamByQuestID(this CharacterQuestListDB self, int questID)
		{
			return null;
		}

		// Token: 0x06001540 RID: 5440 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013F5")]
		[Address(RVA = "0x10119A4FC", Offset = "0x119A4FC", VA = "0x10119A4FC")]
		public static List<CharacterQuestListDB_Param> GetParamsByCharaID(this CharacterQuestListDB self, int evolutionAfterCharaID)
		{
			return null;
		}

		// Token: 0x06001541 RID: 5441 RVA: 0x00009078 File Offset: 0x00007278
		[Token(Token = "0x60013F6")]
		[Address(RVA = "0x10119A6D0", Offset = "0x119A6D0", VA = "0x10119A6D0")]
		private static int CompareCharacterQuest(CharacterQuestListDB_Param A, CharacterQuestListDB_Param B)
		{
			return 0;
		}
	}
}
