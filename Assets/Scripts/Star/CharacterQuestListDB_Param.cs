﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004D2 RID: 1234
	[Token(Token = "0x20003CA")]
	[Serializable]
	[StructLayout(0, Size = 20)]
	public struct CharacterQuestListDB_Param
	{
		// Token: 0x0400175E RID: 5982
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001144")]
		public int m_QuestID;

		// Token: 0x0400175F RID: 5983
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001145")]
		public int m_EvolutionAfterCharaID;

		// Token: 0x04001760 RID: 5984
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001146")]
		public int m_OrderID;

		// Token: 0x04001761 RID: 5985
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001147")]
		public int m_OpenCondition;

		// Token: 0x04001762 RID: 5986
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001148")]
		public int m_OpenConditionValue;
	}
}
