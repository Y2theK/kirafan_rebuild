﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200047E RID: 1150
	[Token(Token = "0x2000380")]
	[StructLayout(3)]
	public class CharacterResource
	{
		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06001371 RID: 4977 RVA: 0x00008820 File Offset: 0x00006A20
		[Token(Token = "0x1700017A")]
		public eCharaResourceType ResourceType
		{
			[Token(Token = "0x600122C")]
			[Address(RVA = "0x1011959AC", Offset = "0x11959AC", VA = "0x1011959AC")]
			get
			{
				return eCharaResourceType.Player;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06001372 RID: 4978 RVA: 0x00008838 File Offset: 0x00006A38
		[Token(Token = "0x1700017B")]
		public int ResourceID
		{
			[Token(Token = "0x600122D")]
			[Address(RVA = "0x10119A6F0", Offset = "0x119A6F0", VA = "0x10119A6F0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06001373 RID: 4979 RVA: 0x00008850 File Offset: 0x00006A50
		[Token(Token = "0x1700017C")]
		public bool IsBosssResource
		{
			[Token(Token = "0x600122E")]
			[Address(RVA = "0x10119A6F8", Offset = "0x119A6F8", VA = "0x10119A6F8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06001374 RID: 4980 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700017D")]
		public string VoiceCueSheetName
		{
			[Token(Token = "0x600122F")]
			[Address(RVA = "0x101195B14", Offset = "0x1195B14", VA = "0x101195B14")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06001375 RID: 4981 RVA: 0x00008868 File Offset: 0x00006A68
		[Token(Token = "0x1700017E")]
		public bool IsAvailableVoice
		{
			[Token(Token = "0x6001230")]
			[Address(RVA = "0x101195B0C", Offset = "0x1195B0C", VA = "0x101195B0C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06001376 RID: 4982 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700017F")]
		public TweetListDB TweetListDB
		{
			[Token(Token = "0x6001231")]
			[Address(RVA = "0x10119A700", Offset = "0x119A700", VA = "0x10119A700")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06001377 RID: 4983 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001378 RID: 4984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000180")]
		public string[] AnimKeyForViewer
		{
			[Token(Token = "0x6001232")]
			[Address(RVA = "0x10119A708", Offset = "0x119A708", VA = "0x10119A708")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001233")]
			[Address(RVA = "0x10119A768", Offset = "0x119A768", VA = "0x10119A768")]
			set
			{
			}
		}

		// Token: 0x06001379 RID: 4985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001234")]
		[Address(RVA = "0x101193BB4", Offset = "0x1193BB4", VA = "0x101193BB4")]
		public void Update()
		{
		}

		// Token: 0x0600137A RID: 4986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001235")]
		[Address(RVA = "0x10119412C", Offset = "0x119412C", VA = "0x10119412C")]
		public void Setup(CharacterHandler charaHndl, eCharaResourceType resourceType, int resourceID)
		{
		}

		// Token: 0x0600137B RID: 4987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001236")]
		[Address(RVA = "0x1011942B0", Offset = "0x11942B0", VA = "0x1011942B0")]
		public void Destroy()
		{
		}

		// Token: 0x0600137C RID: 4988 RVA: 0x00008880 File Offset: 0x00006A80
		[Token(Token = "0x6001237")]
		[Address(RVA = "0x1011948BC", Offset = "0x11948BC", VA = "0x1011948BC")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600137D RID: 4989 RVA: 0x00008898 File Offset: 0x00006A98
		[Token(Token = "0x6001238")]
		[Address(RVA = "0x101193D48", Offset = "0x1193D48", VA = "0x101193D48")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x0600137E RID: 4990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001239")]
		[Address(RVA = "0x1011946CC", Offset = "0x11946CC", VA = "0x1011946CC")]
		public void Prepare()
		{
		}

		// Token: 0x0600137F RID: 4991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600123A")]
		[Address(RVA = "0x10119A7E0", Offset = "0x119A7E0", VA = "0x10119A7E0")]
		private void UpdatePrepare()
		{
		}

		// Token: 0x06001380 RID: 4992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600123B")]
		[Address(RVA = "0x10119A8EC", Offset = "0x119A8EC", VA = "0x10119A8EC")]
		private void PrepareCharaModel()
		{
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x000088B0 File Offset: 0x00006AB0
		[Token(Token = "0x600123C")]
		[Address(RVA = "0x10119AEF4", Offset = "0x119AEF4", VA = "0x10119AEF4")]
		private bool IsDonePrepareCharaModel()
		{
			return default(bool);
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600123D")]
		[Address(RVA = "0x10119B03C", Offset = "0x119B03C", VA = "0x10119B03C")]
		private void PostPrepareCharaModel()
		{
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600123E")]
		[Address(RVA = "0x10119A9E4", Offset = "0x119A9E4", VA = "0x10119A9E4")]
		private void PrepareWeaponModel()
		{
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x000088C8 File Offset: 0x00006AC8
		[Token(Token = "0x600123F")]
		[Address(RVA = "0x10119AF50", Offset = "0x119AF50", VA = "0x10119AF50")]
		private bool IsDonePrepareWeaponModel()
		{
			return default(bool);
		}

		// Token: 0x06001385 RID: 4997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001240")]
		[Address(RVA = "0x10119B48C", Offset = "0x119B48C", VA = "0x10119B48C")]
		private void PostPrepareWeaponModel()
		{
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001241")]
		[Address(RVA = "0x10119AC7C", Offset = "0x119AC7C", VA = "0x10119AC7C")]
		private void PrepareVoice()
		{
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x000088E0 File Offset: 0x00006AE0
		[Token(Token = "0x6001242")]
		[Address(RVA = "0x10119AFAC", Offset = "0x119AFAC", VA = "0x10119AFAC")]
		private bool IsDonePrepareVoice()
		{
			return default(bool);
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001243")]
		[Address(RVA = "0x10119B7EC", Offset = "0x119B7EC", VA = "0x10119B7EC")]
		private void PostPrepareVoice()
		{
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001244")]
		[Address(RVA = "0x10119B8BC", Offset = "0x119B8BC", VA = "0x10119B8BC")]
		private void PrepareAnimation()
		{
		}

		// Token: 0x0600138A RID: 5002 RVA: 0x000088F8 File Offset: 0x00006AF8
		[Token(Token = "0x6001245")]
		[Address(RVA = "0x10119C05C", Offset = "0x119C05C", VA = "0x10119C05C")]
		private bool IsDonePrepareAnimation()
		{
			return default(bool);
		}

		// Token: 0x0600138B RID: 5003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001246")]
		[Address(RVA = "0x10119C570", Offset = "0x119C570", VA = "0x10119C570")]
		private void PostPrepareAnimation()
		{
		}

		// Token: 0x0600138C RID: 5004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001247")]
		[Address(RVA = "0x10119BE14", Offset = "0x119BE14", VA = "0x10119BE14")]
		private void PrepareFacial()
		{
		}

		// Token: 0x0600138D RID: 5005 RVA: 0x00008910 File Offset: 0x00006B10
		[Token(Token = "0x6001248")]
		[Address(RVA = "0x10119C4B8", Offset = "0x119C4B8", VA = "0x10119C4B8")]
		private bool IsDonePrepareFacial()
		{
			return default(bool);
		}

		// Token: 0x0600138E RID: 5006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001249")]
		[Address(RVA = "0x10119C980", Offset = "0x119C980", VA = "0x10119C980")]
		private void PostPrepareFacial()
		{
		}

		// Token: 0x0600138F RID: 5007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600124A")]
		[Address(RVA = "0x10119BF30", Offset = "0x119BF30", VA = "0x10119BF30")]
		private void PrepareShadow()
		{
		}

		// Token: 0x06001390 RID: 5008 RVA: 0x00008928 File Offset: 0x00006B28
		[Token(Token = "0x600124B")]
		[Address(RVA = "0x10119C514", Offset = "0x119C514", VA = "0x10119C514")]
		private bool IsDonePrepareShadow()
		{
			return default(bool);
		}

		// Token: 0x06001391 RID: 5009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600124C")]
		[Address(RVA = "0x10119CA80", Offset = "0x119CA80", VA = "0x10119CA80")]
		private void PostPrepareShadow()
		{
		}

		// Token: 0x06001392 RID: 5010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600124D")]
		[Address(RVA = "0x10119AE20", Offset = "0x119AE20", VA = "0x10119AE20")]
		private void PostPrepareTweetListDB()
		{
		}

		// Token: 0x06001393 RID: 5011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600124E")]
		[Address(RVA = "0x101194118", Offset = "0x1194118", VA = "0x101194118")]
		public CharacterResource()
		{
		}

		// Token: 0x04001583 RID: 5507
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FAB")]
		private CharacterHandler m_Owner;

		// Token: 0x04001584 RID: 5508
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FAC")]
		private ABResourceObjectHandler m_CharaModelHndl;

		// Token: 0x04001585 RID: 5509
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000FAD")]
		private ABResourceObjectHandler m_WeaponModelHndl;

		// Token: 0x04001586 RID: 5510
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000FAE")]
		private Dictionary<string, CharaAnimResourceHndlWrapper>[] m_AnimResHndlWrappers;

		// Token: 0x04001587 RID: 5511
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000FAF")]
		private ABResourceObjectHandler m_FacialHndl;

		// Token: 0x04001588 RID: 5512
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000FB0")]
		private ABResourceObjectHandler m_ShadowHndl;

		// Token: 0x04001589 RID: 5513
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000FB1")]
		private eCharaResourceType m_ResourceType;

		// Token: 0x0400158A RID: 5514
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000FB2")]
		private int m_ResourceID;

		// Token: 0x0400158B RID: 5515
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000FB3")]
		private bool m_IsBossResource;

		// Token: 0x0400158C RID: 5516
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000FB4")]
		private string m_VoiceCueSheetName;

		// Token: 0x0400158D RID: 5517
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000FB5")]
		private bool m_IsAvailableVoice;

		// Token: 0x0400158E RID: 5518
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000FB6")]
		private TweetListDB m_TweetListDB;

		// Token: 0x0400158F RID: 5519
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000FB7")]
		private List<string> m_AnimKeyForViewer;

		// Token: 0x04001590 RID: 5520
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000FB8")]
		private bool m_IsPreparing;

		// Token: 0x04001591 RID: 5521
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000FB9")]
		private bool[] m_NeedPrepareFlgs;

		// Token: 0x04001592 RID: 5522
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000FBA")]
		private CharacterResource.ePrepareStep m_PrepareStep;

		// Token: 0x0200047F RID: 1151
		[Token(Token = "0x2000DD2")]
		private enum ePrepareFlg
		{
			// Token: 0x04001594 RID: 5524
			[Token(Token = "0x400595A")]
			None = -1,
			// Token: 0x04001595 RID: 5525
			[Token(Token = "0x400595B")]
			CharaModel,
			// Token: 0x04001596 RID: 5526
			[Token(Token = "0x400595C")]
			WeaponModel,
			// Token: 0x04001597 RID: 5527
			[Token(Token = "0x400595D")]
			Voice,
			// Token: 0x04001598 RID: 5528
			[Token(Token = "0x400595E")]
			Animation,
			// Token: 0x04001599 RID: 5529
			[Token(Token = "0x400595F")]
			Facial,
			// Token: 0x0400159A RID: 5530
			[Token(Token = "0x4005960")]
			Shadow,
			// Token: 0x0400159B RID: 5531
			[Token(Token = "0x4005961")]
			Num
		}

		// Token: 0x02000480 RID: 1152
		[Token(Token = "0x2000DD3")]
		private enum ePrepareStep
		{
			// Token: 0x0400159D RID: 5533
			[Token(Token = "0x4005963")]
			None = -1,
			// Token: 0x0400159E RID: 5534
			[Token(Token = "0x4005964")]
			Step_0,
			// Token: 0x0400159F RID: 5535
			[Token(Token = "0x4005965")]
			Step_1,
			// Token: 0x040015A0 RID: 5536
			[Token(Token = "0x4005966")]
			Step_2,
			// Token: 0x040015A1 RID: 5537
			[Token(Token = "0x4005967")]
			Done,
			// Token: 0x040015A2 RID: 5538
			[Token(Token = "0x4005968")]
			Num
		}
	}
}
