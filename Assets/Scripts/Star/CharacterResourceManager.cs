﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000482 RID: 1154
	[Token(Token = "0x2000382")]
	[StructLayout(3)]
	public sealed class CharacterResourceManager
	{
		// Token: 0x06001396 RID: 5014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001251")]
		[Address(RVA = "0x10119D778", Offset = "0x119D778", VA = "0x10119D778")]
		public CharacterResourceManager()
		{
		}

		// Token: 0x06001397 RID: 5015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001252")]
		[Address(RVA = "0x10119D8DC", Offset = "0x119D8DC", VA = "0x10119D8DC")]
		public void Update()
		{
		}

		// Token: 0x06001398 RID: 5016 RVA: 0x00008940 File Offset: 0x00006B40
		[Token(Token = "0x6001253")]
		[Address(RVA = "0x10119D90C", Offset = "0x119D90C", VA = "0x10119D90C")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06001399 RID: 5017 RVA: 0x00008958 File Offset: 0x00006B58
		[Token(Token = "0x6001254")]
		[Address(RVA = "0x10119D91C", Offset = "0x119D91C", VA = "0x10119D91C")]
		public bool IsLoadingAny()
		{
			return default(bool);
		}

		// Token: 0x0600139A RID: 5018 RVA: 0x00008970 File Offset: 0x00006B70
		[Token(Token = "0x6001255")]
		[Address(RVA = "0x10119D94C", Offset = "0x119D94C", VA = "0x10119D94C")]
		public bool IsError()
		{
			return default(bool);
		}

		// Token: 0x0600139B RID: 5019 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001256")]
		[Address(RVA = "0x10119CCFC", Offset = "0x119CCFC", VA = "0x10119CCFC")]
		public ABResourceObjectHandler Load(string path, params MeigeResource.Option[] options)
		{
			return null;
		}

		// Token: 0x0600139C RID: 5020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001257")]
		[Address(RVA = "0x10119D97C", Offset = "0x119D97C", VA = "0x10119D97C")]
		public void Unload(string path)
		{
		}

		// Token: 0x0600139D RID: 5021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001258")]
		[Address(RVA = "0x10119A8B4", Offset = "0x119A8B4", VA = "0x10119A8B4")]
		public void Unload(ABResourceObjectHandler unloadHndl)
		{
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001259")]
		[Address(RVA = "0x10119D9B4", Offset = "0x119D9B4", VA = "0x10119D9B4")]
		public void UnloadAll(bool isUnloadResident = false)
		{
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x00008988 File Offset: 0x00006B88
		[Token(Token = "0x600125A")]
		[Address(RVA = "0x10119D9EC", Offset = "0x119D9EC", VA = "0x10119D9EC")]
		public bool IsCompleteUnloadAll()
		{
			return default(bool);
		}

		// Token: 0x040015A6 RID: 5542
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FBE")]
		private readonly string[] RESIDENT_PATH_LIST;

		// Token: 0x040015A7 RID: 5543
		[Token(Token = "0x4000FBF")]
		private const int SIMULTANEOUSLY_LOAD_NUM = 64;

		// Token: 0x040015A8 RID: 5544
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FC0")]
		public ABResourceLoader m_ABResLoader;
	}
}
