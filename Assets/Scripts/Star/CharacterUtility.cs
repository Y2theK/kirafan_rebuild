﻿using Meige;
using Star.Town;
using System.Collections.Generic;
using System.Text;

namespace Star {
    public static class CharacterUtility {
        public static void SetupCharacterParam(CharacterParam destParam, long mngID, int charaID, int viewCharaID, int currentLv, int maxLv, long exp, int limitBreak, int arousalLv, int duplicatedCount) {
            destParam.MngID = mngID;
            destParam.CharaID = charaID;
            destParam.ViewCharaID = viewCharaID;
            destParam.Lv = currentLv;
            destParam.MaxLv = maxLv;
            destParam.Exp = exp;
            destParam.LimitBreak = limitBreak;
            destParam.ArousalLv = arousalLv;
            destParam.DuplicatedCount = duplicatedCount;
            if (destParam.ClassSkillLearnDatas == null) {
                destParam.ClassSkillLearnDatas = new List<SkillLearnData>();
            }
            if (destParam.UniqueSkillLearnDatas == null) {
                destParam.UniqueSkillLearnDatas = new List<SkillLearnData>();
            }
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param param = dbMng.CharaListDB.GetParam(destParam.CharaID);
            destParam.NamedType = (eCharaNamedType)param.m_NamedType;
            destParam.RareType = (eRare)param.m_Rare;
            destParam.ClassType = (eClassType)param.m_Class;
            destParam.ElementType = (eElementType)param.m_Element;
            destParam.Cost = EditUtility.CalcCharaCost(charaID, arousalLv);
            destParam.Hp = dbMng.CharaParamGrowthListDB.CalcHp(param.m_InitHp, currentLv, param.m_GrowthTableID);
            destParam.Atk = dbMng.CharaParamGrowthListDB.CalcAtk(param.m_InitAtk, currentLv, param.m_GrowthTableID);
            destParam.Mgc = dbMng.CharaParamGrowthListDB.CalcMgc(param.m_InitMgc, currentLv, param.m_GrowthTableID);
            destParam.Def = dbMng.CharaParamGrowthListDB.CalcDef(param.m_InitDef, currentLv, param.m_GrowthTableID);
            destParam.MDef = dbMng.CharaParamGrowthListDB.CalcMDef(param.m_InitMDef, currentLv, param.m_GrowthTableID);
            destParam.Spd = dbMng.CharaParamGrowthListDB.CalcSpd(param.m_InitSpd, currentLv, param.m_GrowthTableID);
            destParam.Luck = dbMng.CharaParamGrowthListDB.CalcLuck(param.m_InitLuck, currentLv, param.m_GrowthTableID);
            destParam.IsRegistAbnormals = new bool[(int)eStateAbnormal.Num];
            destParam.AIID = -1;
            destParam.ChargeCountMax = 1;
            destParam.StunCoef = param.m_StunCoef;
            destParam.StunerMag = 1f;
            destParam.StunerAvoid = 0f;
        }

        public static void SetupCharacterParamFromQuestEnemyParam(CharacterParam destParam, ref QuestEnemyListDB_Param questEnemyParam, int currentLv) {
            destParam.CharaID = questEnemyParam.m_ID;
            destParam.ViewCharaID = questEnemyParam.m_ID;
            destParam.Lv = currentLv;
            destParam.MaxLv = questEnemyParam.m_MaxLv;
            destParam.Exp = 0L;
            destParam.LimitBreak = 0;
            if (destParam.ClassSkillLearnDatas == null) {
                destParam.ClassSkillLearnDatas = new List<SkillLearnData>();
            }
            if (destParam.UniqueSkillLearnDatas == null) {
                destParam.UniqueSkillLearnDatas = new List<SkillLearnData>();
            }
            destParam.NamedType = eCharaNamedType.None;
            destParam.RareType = eRare.Star1;
            destParam.ClassType = eClassType.None;
            destParam.ElementType = (eElementType)questEnemyParam.m_Element;
            destParam.Cost = 0;
            destParam.Hp = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitHp, questEnemyParam.m_MaxHp, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.Atk = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitAtk, questEnemyParam.m_MaxAtk, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.Mgc = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitMgc, questEnemyParam.m_MaxMgc, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.Def = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitDef, questEnemyParam.m_MaxDef, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.MDef = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitMDef, questEnemyParam.m_MaxMDef, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.Spd = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitSpd, questEnemyParam.m_MaxSpd, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.Luck = CharacterParamCalculator.CalcParamCorrectLv(questEnemyParam.m_InitLuck, questEnemyParam.m_MaxLuck, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
            destParam.IsRegistAbnormals = new bool[(int)eStateAbnormal.Num];
            for (int i = 0; i < destParam.IsRegistAbnormals.Length; i++) {
                if (questEnemyParam.m_IsRegistAbnormals.Length > i) {
                    destParam.IsRegistAbnormals[i] = questEnemyParam.m_IsRegistAbnormals[i] == 1;
                }
            }
            int skillIDNum = QuestEnemyListDB_Ext.GetSkillIDNum(ref questEnemyParam);
            for (int j = 0; j < skillIDNum; j++) {
                destParam.ClassSkillLearnDatas.Add(new SkillLearnData(questEnemyParam.m_SkillIDs[j], 1, 1, 0L, -1, questEnemyParam.m_IsConfusionSkillIDs[j] == 1));
            }
            skillIDNum = QuestEnemyListDB_Ext.GetChargeSkillIDNum(ref questEnemyParam);
            for (int j = 0; j < skillIDNum; j++) {
                destParam.UniqueSkillLearnDatas.Add(new SkillLearnData(questEnemyParam.m_CharageSkillIDs[j], 1, 1, 0L, -1, false));
            }
            destParam.AIID = questEnemyParam.m_AIID;
            destParam.ChargeCountMax = questEnemyParam.m_CharageMax;
            destParam.StunCoef = questEnemyParam.m_StunCoef;
            destParam.StunerMag = questEnemyParam.m_StunerMag;
            destParam.StunerAvoid = questEnemyParam.m_StunerAvoid;
        }

        public static void SetupWeaponParam(WeaponParam destParam, long mngID, int weaponID, int currentLv, long exp, int weaponSkillLv, long weaponSkillExp) {
            destParam.MngID = mngID;
            destParam.WeaponID = weaponID;
            WeaponListDB_Param param = GameSystem.Inst.DbMng.WeaponListDB.GetParam(destParam.WeaponID);
            destParam.ResourceID_L = param.m_ResourceID_L;
            destParam.ResourceID_R = param.m_ResourceID_R;
            destParam.ControllType = (eWeaponControllType)param.m_ControllType;
            destParam.ClassAnimType = param.m_ClassAnimType;
            destParam.Lv = currentLv;
            destParam.MaxLv = param.m_LimitLv;
            destParam.Exp = exp;
            destParam.Cost = param.m_Cost;
            destParam.Atk = CharacterParamCalculator.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, param.m_InitLv, param.m_LimitLv, currentLv);
            destParam.Mgc = CharacterParamCalculator.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, param.m_InitLv, param.m_LimitLv, currentLv);
            destParam.Def = CharacterParamCalculator.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, param.m_InitLv, param.m_LimitLv, currentLv);
            destParam.MDef = CharacterParamCalculator.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, param.m_InitLv, param.m_LimitLv, currentLv);
            destParam.SkillLearnData = new SkillLearnData(param.m_SkillID, weaponSkillLv, param.m_SkillLimitLv, weaponSkillExp, param.m_SkillExpTableID);
            destParam.PassiveSkillID = param.m_PassiveSkillID;
        }

        public static void SetupDefaultWeaponParam(WeaponParam destParam, int charaID) {
            destParam.MngID = -1L;
            destParam.WeaponID = EditUtility.CalcDefaultWeaponIDFromCharaID(charaID);
            WeaponListDB_Param param = GameSystem.Inst.DbMng.WeaponListDB.GetParam(destParam.WeaponID);
            destParam.ResourceID_L = param.m_ResourceID_L;
            destParam.ResourceID_R = param.m_ResourceID_R;
            destParam.ControllType = (eWeaponControllType)param.m_ControllType;
            destParam.ClassAnimType = param.m_ClassAnimType;
            destParam.Lv = 1;
            destParam.MaxLv = 1;
            destParam.Exp = 0L;
            destParam.Cost = param.m_Cost;
            destParam.Atk = param.m_InitAtk;
            destParam.Mgc = param.m_InitMgc;
            destParam.Def = param.m_InitDef;
            destParam.MDef = param.m_InitMDef;
            destParam.SkillLearnData = new SkillLearnData(param.m_SkillID, 1, 1, 0L, -1, true);
            destParam.PassiveSkillID = param.m_PassiveSkillID;
        }

        public static void ChangeWeaponResourceIfDedicaedAnimCharaEquipGeneralWeapon(WeaponParam destParam, int charaID) {
            if (CheckDedicatedAnimCharaAndDefaultWeaponCombi(charaID, destParam.WeaponID)) {
                DatabaseManager dbMng = GameSystem.Inst.DbMng;
                CharacterOverrideDB_Param? overrideParam = dbMng.CharaOverrideDB.GetParam(charaID);
                if (overrideParam.HasValue) {
                    WeaponListDB_Param param = dbMng.WeaponListDB.GetParam(overrideParam.Value.m_GeneralWeaponID);
                    destParam.ResourceID_L = param.m_ResourceID_L;
                    destParam.ResourceID_R = param.m_ResourceID_R;
                    destParam.ControllType = (eWeaponControllType)param.m_ControllType;
                }
            }
        }

        public static bool CheckDedicatedAnimCharaAndDefaultWeaponCombi(int charaID, int weaponID) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            if (charaParam.m_DedicatedAnimType == (int)CharacterDefine.eDedicatedAnimType.DedicatedForChara) {
                WeaponListDB_Param param = dbMng.WeaponListDB.GetParam(weaponID);
                if (!param.IsCharacterWeapon()) {
                    CharacterOverrideDB_Param? overrideParam = dbMng.CharaOverrideDB.GetParam(charaID);
                    if (overrideParam.HasValue) {
                        return param.m_ID != overrideParam.Value.m_DefaultWeaponID;
                    }
                }
            }
            return false;
        }

        public static void SetupNamedParam(NamedParam destParam, int friendShip, eCharaNamedType namedType) {
            sbyte friendshipTableID = GameSystem.Inst.DbMng.NamedListDB.GetFriendshipTableID(namedType);
            SetupNamedParam(destParam, friendShip, friendshipTableID);
        }

        public static void SetupNamedParam(NamedParam destParam, int friendShip, sbyte friendShipTableID) {
            destParam.FriendShip = friendShip;
            destParam.FriendShipMax = GameSystem.Inst.DbMng.NamedFriendshipExpDB.GetFriendShipMax(friendShipTableID);
        }

        public static eTitleType GetTitleType(eCharaNamedType namedType) {
            return GameSystem.Inst.DbMng.NamedListDB.GetTitleType(namedType);
        }

        public static void SetupAbilityParam(AbilityParam destParam, int abilityBoardID, int[] equipItemIDs) {
            destParam.AbilityBoardID = abilityBoardID;
            destParam.EquipItemIDs = equipItemIDs;
        }

        public static string GetModelResourcePath(eCharaResourceType resourceType, int resourceID) {
            StringBuilder stringBuilder = new StringBuilder();
            if (resourceType == eCharaResourceType.Player) {
                stringBuilder.Append("model/player/model_pl_");
            } else {
                stringBuilder.Append("model/enemy/model_en_");
            }
            stringBuilder.Append(resourceID);
            stringBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            return stringBuilder.ToString();
        }

        public static string GetWeaponModelResourcePath(int resourceID_L, int resourceID_R) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("model/weapon/wpn_");
            stringBuilder.Append(resourceID_L != -1 ? resourceID_L : resourceID_R);
            stringBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            return stringBuilder.ToString();
        }

        public static string[] GetWeaponModelObjectPaths(int resourceID_L, int resourceID_R) {
            string[] array = new string[(int)CharacterDefine.eWeaponIndex.Num];
            StringBuilder stringBuilder = new StringBuilder();
            if (resourceID_L != -1) {
                stringBuilder.Append("WPN_");
                stringBuilder.Append(resourceID_L);
                stringBuilder.Append("_L");
                array[(int)CharacterDefine.eWeaponIndex.L] = stringBuilder.ToString();
            } else {
                array[(int)CharacterDefine.eWeaponIndex.L] = string.Empty;
            }
            if (resourceID_R != -1) {
                stringBuilder.Length = 0;
                stringBuilder.Append("WPN_");
                stringBuilder.Append(resourceID_R);
                stringBuilder.Append("_R");
                array[(int)CharacterDefine.eWeaponIndex.R] = stringBuilder.ToString();
            } else {
                array[(int)CharacterDefine.eWeaponIndex.R] = string.Empty;
            }
            return array;
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterDefine.eMode mode, CharacterHandler charaHndl, bool isViewerSetting = false) {
            return mode switch {
                CharacterDefine.eMode.Battle => GetCharacterAnimSettingsFromCurrentState_Battle(resourceType, resourceID, partsIndex, charaHndl, isViewerSetting),
                CharacterDefine.eMode.Room => GetCharacterAnimSettingsFromCurrentState_Room(resourceType, resourceID, partsIndex, charaHndl),
                CharacterDefine.eMode.Menu => GetCharacterAnimSettingsFromCurrentState_Menu(resourceType, resourceID, partsIndex, charaHndl),
                _ => GetCharacterAnimSettingsFromCurrentState_Viewer(resourceType, resourceID, partsIndex, charaHndl),
            };
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_Battle(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) {
            if (resourceType == eCharaResourceType.Player) {
                return GetCharacterAnimSettingsFromCurrentState_BattlePlayer(0, partsIndex, charaHndl, isViewerSetting);
            }
            return GetCharacterAnimSettingsFromCurrentState_BattleEnemy(resourceID, partsIndex, charaHndl, false); ;
        }

        private static AnimSettings ConstructAnimSettings(StringBuilder ref_sb, string actionKey, string resourcePath, string preAtName) {
            ref_sb.Length = 0;
            ref_sb.Append(preAtName);
            ref_sb.Append("@");
            ref_sb.Append(actionKey);
            return new AnimSettings(actionKey, resourcePath, ref_sb.ToString());
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_BattlePlayer(int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) { //TODO: not 100% sure (like 95%)
            List<AnimSettings> list = new List<AnimSettings>();
            StringBuilder animBuilder = new StringBuilder();
            StringBuilder pathBuilder = new StringBuilder();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterParam charaParam = charaHndl.CharaParam;
            CharacterListDB_Param dbParam = dbMng.CharaListDB.GetParam(charaParam.CharaID);
            CharacterListDB_Param dbViewParam = dbMng.CharaListDB.GetParam(charaParam.ViewCharaID);
            ClassListDB_Param classParam = dbMng.ClassListDB.GetParam(charaParam.ClassType);
            NamedListDB_Param namedParam = dbMng.NamedListDB.GetParam(charaParam.NamedType);
            CharacterEvolutionListDB_Param evoParam;
            int classAnim = charaHndl.WeaponParam != null ? charaHndl.WeaponParam.ClassAnimType : CharacterDefine.CLASS_ANIM_TYPE_DEFAULT;
            string part1;
            string part2;
            if (partsIndex == (int)CharacterDefine.ePlayerModelParts.Body) {
                part1 = "_body";
                part2 = "_body";
            } else {
                part1 = $"_head_{dbViewParam.m_HeadID}";
                part2 = "_head";
            }
            pathBuilder.Length = 0;
            pathBuilder.Append("meigeac_");
            pathBuilder.Append(classParam.m_ResouceBaseName.ToLower());
            if (classAnim != CharacterDefine.CLASS_ANIM_TYPE_DEFAULT) {
                pathBuilder.Append("_");
                pathBuilder.Append(classAnim);
            }
            pathBuilder.Append(part1);
            string classPrefix = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("meigeac_common");
            pathBuilder.Append(part1);
            string commonPrefix = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("meigeac_chara_battle");
            pathBuilder.Append(part2);
            string dedicatedPrefix = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("anim/player/class_");
            pathBuilder.Append(classParam.m_ResouceBaseName.ToLower());
            if (classAnim != CharacterDefine.CLASS_ANIM_TYPE_DEFAULT) {
                pathBuilder.Append("_");
                pathBuilder.Append(classAnim);
            }
            pathBuilder.Append(part1);
            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            string classResource = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("anim/player/common_battle");
            pathBuilder.Append(part1);
            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            string commonResource = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("anim/player/chara_battle_");
            if (dbMng.CharaEvolutionListDB.GetParamBySrcCharaID(charaParam.CharaID, out evoParam)) {
                pathBuilder.Append(dbMng.CharaListDB.GetParam(evoParam.m_DestCharaID).m_ResourceID);
            } else {
                pathBuilder.Append(dbParam.m_ResourceID);
            }
            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            string dedicatedResource = pathBuilder.ToString();
            if (dbParam.IsDedicatedAnimType_ForChara()) {
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_IDLE, dedicatedResource, dedicatedPrefix));
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ATK, dedicatedResource, dedicatedPrefix));
            } else {
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_IDLE, classResource, classPrefix));
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ATK, classResource, classPrefix));
            }
            for (int i = BattleDefine.ACT_KEY_CLS_SKILL_RANGE_MIN; i <= BattleDefine.ACT_KEY_CLS_SKILL_RANGE_MAX; i++) {
                string action = $"{BattleDefine.BATTLE_ACT_KEY_CLS_SKILL}{i}";
                if (dbParam.IsDedicatedAnimType_ForChara()) {
                    list.Add(ConstructAnimSettings(animBuilder, action, dedicatedResource, dedicatedPrefix));
                } else {
                    list.Add(ConstructAnimSettings(animBuilder, action, classResource, classPrefix));
                }
            }
            if (dbParam.IsDedicatedAnimType_CharaSkill() || dbParam.IsDedicatedAnimType_ForChara()) {
                string charaSkillResource = dedicatedResource;
                if (charaParam.CharaID != charaParam.ViewCharaID) {
                    if (dbMng.CharaEvolutionListDB.GetParamBySrcCharaID(charaParam.CharaID, out evoParam)) {
                        if (dbMng.IsExistCharacterWeaponOverrideAnim(dbMng.CharaListDB.GetParam(evoParam.m_DestCharaID).m_CharaID)) {
                            pathBuilder.Length = 0;
                            pathBuilder.Append("anim/player/chara_battle_");
                            pathBuilder.Append(dbViewParam.m_ResourceID);
                            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
                            charaSkillResource = pathBuilder.ToString();
                        }
                    } else if (dbMng.IsExistCharacterWeaponOverrideAnim(charaParam.CharaID)) {
                        pathBuilder.Length = 0;
                        pathBuilder.Append("anim/player/chara_battle_");
                        pathBuilder.Append(dbViewParam.m_ResourceID);
                        pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
                        charaSkillResource = pathBuilder.ToString();
                    }
                }
                for (int i = BattleDefine.ACT_KEY_CHARA_SKILL_RANGE_MIN; i <= BattleDefine.ACT_KEY_CHARA_SKILL_RANGE_MAX; i++) {
                    string action = $"{BattleDefine.BATTLE_ACT_KEY_CHARA_SKILL}{i}";
                    list.Add(ConstructAnimSettings(animBuilder, action, charaSkillResource, dedicatedPrefix));
                }
            }
            if (dbParam.IsDedicatedAnimType_ForChara()) {
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ABNORMAL, dedicatedResource, dedicatedPrefix));
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DAMAGE, dedicatedResource, dedicatedPrefix));
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DEAD, dedicatedResource, dedicatedPrefix));
            } else {
                if (charaParam.ClassType != eClassType.Magician || classAnim != CharacterDefine.CLASS_ANIM_TYPE_DEFAULT) {
                    list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ABNORMAL, commonResource, commonPrefix));
                    list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DAMAGE, commonResource, commonPrefix));
                } else {
                    list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ABNORMAL, classResource, classPrefix));
                    list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DAMAGE, classResource, classPrefix));
                }
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DEAD, commonResource, commonPrefix));
            }
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_IN, commonResource, commonPrefix));
            string outPrefix = commonPrefix;
            if (partsIndex == (int)CharacterDefine.ePlayerModelParts.Body && dbViewParam.m_BodyID == CharacterDefine.BODY_ID_TIGHT_SKIRT) {
                outPrefix = $"meigeac_{CharacterDefine.BODY_ANIM_PREFIXS[2].ToLower()}";
            }
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_OUT, commonResource, outPrefix));
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_RUN, commonResource, outPrefix));
            int[] array;
            if (isViewerSetting) {
                array = new int[] { 0, 1, 2, 3, 4 };
            } else {
                array = new int[] { namedParam.m_BattleWinID };
            }
            for (int i = 0; i < array.Length; i++) {
                list.Add(ConstructAnimSettings(animBuilder, $"{BattleDefine.BATTLE_ACT_KEY_WIN_ST}{array[i]}", commonResource, commonPrefix));
                list.Add(ConstructAnimSettings(animBuilder, $"{BattleDefine.BATTLE_ACT_KEY_WIN_LP}{array[i]}", commonResource, commonPrefix));
            }
            for (int i = 0; i < 3; i++) {
                list.Add(ConstructAnimSettings(animBuilder, $"{BattleDefine.BATTLE_ACT_KEY_KIRARA_JUMP}{i}", commonResource, commonPrefix));
            }
            return list;
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_BattleEnemy(int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) {
            List<AnimSettings> list = new List<AnimSettings>();
            StringBuilder animBuilder = new StringBuilder();
            StringBuilder pathBuilder = new StringBuilder();
            EnemyResourceListDB_Param param = GameSystem.Inst.DbMng.EnemyResourceListDB.GetParam(resourceID);
            pathBuilder.Length = 0;
            pathBuilder.Append("anim/enemy/common_en_");
            pathBuilder.Append(param.m_AnimID);
            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            string resource = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("meigeac_common_en_");
            pathBuilder.Append(param.m_AnimID);
            string prefix = pathBuilder.ToString();
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_IDLE, resource, prefix));
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_ABNORMAL, resource, prefix));
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DAMAGE, resource, prefix));
            list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_DEAD, resource, prefix));
            for (int i = 0; i <= 1; i++) {
                list.Add(ConstructAnimSettings(animBuilder, $"{BattleDefine.BATTLE_ACT_KEY_EN_SKILL}{i}", resource, prefix));
            }
            if (param.m_IsBoss == 1) {
                list.Add(ConstructAnimSettings(animBuilder, BattleDefine.BATTLE_ACT_KEY_EN_UNQ_SKILL, resource, prefix));
            }
            return list;
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_Room(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) {
            List<AnimSettings> list = new List<AnimSettings>();
            RoomUtility.GetRoomCharacterAnimSettings(list, resourceType, resourceID, partsIndex, charaHndl.CharaParam);
            return list;
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_Menu(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) {
            List<AnimSettings> list = new List<AnimSettings>();
            StringBuilder animBuilder = new StringBuilder();
            StringBuilder pathBuilder = new StringBuilder();
            CharacterListDB_Param param = GameSystem.Inst.DbMng.CharaListDB.GetParam(charaHndl.CharaParam.ViewCharaID);
            string partName;
            if (partsIndex == (int)CharacterDefine.ePlayerModelParts.Body) {
                partName = "_body";
            } else {
                partName = $"_head_{param.m_HeadID}";
            }
            pathBuilder.Length = 0;
            pathBuilder.Append("anim/player/common_menu");
            pathBuilder.Append(partName);
            pathBuilder.Append(MeigeResourceManager.AB_EXTENSION);
            string resouce = pathBuilder.ToString();
            pathBuilder.Length = 0;
            pathBuilder.Append("meigeac_common");
            pathBuilder.Append(partName);
            string prefix = pathBuilder.ToString();
            for (int i = 0; i < CharacterDefine.MENU_MODE_ACT_KEYS.Length; i++) {
                list.Add(ConstructAnimSettings(animBuilder, CharacterDefine.MENU_MODE_ACT_KEYS[i], resouce, prefix));
            }
            return list;
        }

        public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState_Viewer(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterHandler charaHndl, bool isViewerSetting = false) {
            List<AnimSettings> list = new List<AnimSettings>();
            list.AddRange(GetCharacterAnimSettingsFromCurrentState_Battle(resourceType, resourceID, partsIndex, charaHndl, true));
            list.AddRange(GetCharacterAnimSettingsFromCurrentState_Menu(resourceType, resourceID, partsIndex, charaHndl));
            return list;
        }

        public static string GetCharacterFacialAnimSetting(eCharaNamedType namedType) {
            NamedListDB_Param param = GameSystem.Inst.DbMng.NamedListDB.GetParam(namedType);
            StringBuilder sb = new StringBuilder();
            sb.Append("anim/player/meigeac_");
            sb.Append(param.m_ResouceBaseName);
            sb.Append("@facial");
            sb.Append(MeigeResourceManager.AB_EXTENSION);
            return sb.ToString();
        }

        public static string ConvertMeigeFbx2PrefabObjPath(string prefabRootName, bool isIncludePrefabRootName = false) {
            StringBuilder stringBuilder = new StringBuilder();
            string value = prefabRootName.Replace("(Clone)", string.Empty);
            if (isIncludePrefabRootName) {
                stringBuilder.Append(value);
                stringBuilder.Append("/MeshRoot_");
            } else {
                stringBuilder.Append("MeshRoot_");
            }
            stringBuilder.Append(value);
            stringBuilder.Append("/");
            stringBuilder.Append(value);
            stringBuilder.Append("(Clone)");
            return stringBuilder.ToString();
        }

        public static string GetBattleShadowResourcePath() {
            return "model/shadow/shadow_battle" + MeigeResourceManager.AB_EXTENSION;
        }

        public static string GetRoomShadowResourcePath() {
            return "model/shadow/shadow_room" + MeigeResourceManager.AB_EXTENSION;
        }

        public static string[] GetSAPIDs(CharacterParam charaParam, WeaponParam wpnParam, int normalAttackSkillID, bool isUserControll) {
            List<string> list = new List<string>();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            SkillListDB skillListDB_CARD = dbMng.SkillListDB_CARD;
            if (isUserControll) {
                SkillListDB skillListDB_PL = dbMng.SkillListDB_PL;
                SkillListDB skillListDB_WPN = dbMng.SkillListDB_WPN;
                SkillContentListDB skillContentListDB_PL = dbMng.SkillContentListDB_PL;
                SkillContentListDB skillContentListDB_WPN = dbMng.SkillContentListDB_WPN;
                CharacterListDB_Param dbParam = dbMng.CharaListDB.GetParam(charaParam.CharaID);
                int wpnAnim = 0;
                bool defaultWpnAnim = true;
                bool defaultAtk = true;
                string actionPlanId = skillListDB_PL.GetSkillActionPlanID(normalAttackSkillID);
                if (wpnParam != null) {
                    wpnAnim = wpnParam.ClassAnimType;
                    defaultWpnAnim = wpnAnim == 0;
                    if (!dbParam.IsDedicatedAnimType_ForChara() && !defaultWpnAnim) {
                        actionPlanId = dbMng.SkillActionConvertsDB_SAP.Convert(actionPlanId, wpnAnim);
                        defaultAtk = false;
                    }
                }
                if (!string.IsNullOrEmpty(actionPlanId)) {
                    list.Add(actionPlanId);
                }
                for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    actionPlanId = skillListDB_PL.GetSkillActionPlanID(skillId);
                    if (!defaultAtk) {
                        actionPlanId = dbMng.SkillActionConvertsDB_SAP.Convert(actionPlanId, wpnAnim);
                    }
                    if (!string.IsNullOrEmpty(actionPlanId)) {
                        list.Add(actionPlanId);
                    }
                    List<int> refIds = skillContentListDB_PL.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionPlanId = skillListDB_CARD.GetSkillActionPlanID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionPlanId)) {
                            list.Add(actionPlanId);
                        }
                    }
                }
                for (int i = 0; i < charaParam.UniqueSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    if (charaParam.CharaID != charaParam.ViewCharaID) {
                        CharacterListDB_Param dbViewParam = dbMng.CharaListDB.GetParam(charaParam.ViewCharaID);
                        skillId = skillListDB_PL.GetParam(dbViewParam.m_CharaSkillID).m_ID;
                    }
                    actionPlanId = skillListDB_PL.GetSkillActionPlanID(skillId);
                    if (!string.IsNullOrEmpty(actionPlanId)) {
                        list.Add(actionPlanId);
                    }
                    List<int> refIds = skillContentListDB_PL.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionPlanId = skillListDB_CARD.GetSkillActionPlanID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionPlanId)) {
                            list.Add(actionPlanId);
                        }
                    }
                }
                if (wpnParam != null && wpnParam.WeaponID != -1) {
                    int skillId = dbMng.WeaponListDB.GetParam(wpnParam.WeaponID).m_SkillID;
                    actionPlanId = skillListDB_WPN.GetSkillActionPlanID(skillId);
                    if (dbParam.IsDedicatedAnimType_ForChara()) {
                        actionPlanId = dbMng.CharaOverrideDB.SAPConvert(charaParam.CharaID, actionPlanId);
                    } else if (!defaultWpnAnim) {
                        actionPlanId = dbMng.SkillActionConvertsDB_SAP.Convert(actionPlanId, wpnAnim);
                    }
                    if (!string.IsNullOrEmpty(actionPlanId)) {
                        list.Add(actionPlanId);
                    }
                    List<int> refIds = skillContentListDB_WPN.GetRefIdsOnCardType(skillId);
                    for (int i = 0; i < refIds.Count; i++) {
                        actionPlanId = skillListDB_CARD.GetSkillActionPlanID(refIds[i]);
                        if (!string.IsNullOrEmpty(actionPlanId)) {
                            list.Add(actionPlanId);
                        }
                    }
                }
            } else {
                SkillListDB skillList = dbMng.SkillListDB_EN;
                SkillContentListDB skillContentList = dbMng.SkillContentListDB_EN;
                for (int i = 0; i < charaParam.UniqueSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.UniqueSkillLearnDatas[i].SkillID;
                    string actionPlanId = skillList.GetSkillActionPlanID(skillId);
                    if (!string.IsNullOrEmpty(actionPlanId)) {
                        list.Add(actionPlanId);
                    }
                    List<int> refIds = skillContentList.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionPlanId = skillListDB_CARD.GetSkillActionPlanID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionPlanId)) {
                            list.Add(actionPlanId);
                        }
                    }
                }
                for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    string actionPlanId = skillList.GetSkillActionPlanID(skillId);
                    if (!string.IsNullOrEmpty(actionPlanId)) {
                        list.Add(actionPlanId);
                    }
                    List<int> refIds = skillContentList.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionPlanId = skillListDB_CARD.GetSkillActionPlanID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionPlanId)) {
                            list.Add(actionPlanId);
                        }
                    }
                }
            }
            HashSet<string> hashSet = new HashSet<string>(list);
            string[] array = new string[hashSet.Count];
            hashSet.CopyTo(array, 0);
            return array;
        }

        public static string[] GetSAGIDs(CharacterParam charaParam, WeaponParam wpnParam, int normalAttackSkillID, bool isUserControll) {
            List<string> list = new List<string>();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            SkillListDB skillListDB_CARD = dbMng.SkillListDB_CARD;
            if (isUserControll) {
                SkillListDB skillListDB_PL = dbMng.SkillListDB_PL;
                SkillListDB skillListDB_WPN = dbMng.SkillListDB_WPN;
                SkillContentListDB skillContentListDB_PL = dbMng.SkillContentListDB_PL;
                SkillContentListDB skillContentListDB_WPN = dbMng.SkillContentListDB_WPN;
                CharacterListDB_Param dbParam = dbMng.CharaListDB.GetParam(charaParam.CharaID);
                int wpnAnim = 0;
                bool defaultWpnAnim = true;
                bool defaultAtk = true;
                string actionGraphicsId = skillListDB_PL.GetSkillActionGraphicsID(normalAttackSkillID);
                if (wpnParam != null) {
                    wpnAnim = wpnParam.ClassAnimType;
                    defaultWpnAnim = wpnAnim == 0;
                    if (!dbParam.IsDedicatedAnimType_ForChara() && !defaultWpnAnim) {
                        actionGraphicsId = dbMng.SkillActionConvertsDB_SAG.Convert(actionGraphicsId, wpnAnim);
                        defaultAtk = false;
                    }
                }
                if (!string.IsNullOrEmpty(actionGraphicsId)) {
                    list.Add(actionGraphicsId);
                }
                for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    actionGraphicsId = skillListDB_PL.GetSkillActionGraphicsID(skillId);
                    if (!defaultAtk) {
                        actionGraphicsId = dbMng.SkillActionConvertsDB_SAG.Convert(actionGraphicsId, wpnAnim);
                    }
                    if (!string.IsNullOrEmpty(actionGraphicsId)) {
                        list.Add(actionGraphicsId);
                    }
                    List<int> refIds = skillContentListDB_PL.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionGraphicsId = skillListDB_CARD.GetSkillActionGraphicsID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionGraphicsId)) {
                            list.Add(actionGraphicsId);
                        }
                    }
                }
                for (int i = 0; i < charaParam.UniqueSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    if (charaParam.CharaID != charaParam.ViewCharaID) {
                        CharacterListDB_Param dbViewParam = dbMng.CharaListDB.GetParam(charaParam.ViewCharaID);
                        skillId = skillListDB_PL.GetParam(dbViewParam.m_CharaSkillID).m_ID;
                    }
                    actionGraphicsId = skillListDB_PL.GetSkillActionGraphicsID(skillId);
                    if (!string.IsNullOrEmpty(actionGraphicsId)) {
                        list.Add(actionGraphicsId);
                    }
                    List<int> refIds = skillContentListDB_PL.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionGraphicsId = skillListDB_CARD.GetSkillActionGraphicsID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionGraphicsId)) {
                            list.Add(actionGraphicsId);
                        }
                    }
                }
                if (wpnParam != null && wpnParam.WeaponID != -1) {
                    int skillId = dbMng.WeaponListDB.GetParam(wpnParam.WeaponID).m_SkillID;
                    actionGraphicsId = skillListDB_WPN.GetSkillActionGraphicsID(skillId);
                    if (dbParam.IsDedicatedAnimType_ForChara()) {
                        actionGraphicsId = dbMng.CharaOverrideDB.SAGConvert(charaParam.CharaID, actionGraphicsId);
                    } else if (!defaultWpnAnim) {
                        actionGraphicsId = dbMng.SkillActionConvertsDB_SAG.Convert(actionGraphicsId, wpnAnim);
                    }
                    if (!string.IsNullOrEmpty(actionGraphicsId)) {
                        list.Add(actionGraphicsId);
                    }
                    List<int> refIds = skillContentListDB_WPN.GetRefIdsOnCardType(skillId);
                    for (int i = 0; i < refIds.Count; i++) {
                        actionGraphicsId = skillListDB_CARD.GetSkillActionGraphicsID(refIds[i]);
                        if (!string.IsNullOrEmpty(actionGraphicsId)) {
                            list.Add(actionGraphicsId);
                        }
                    }
                }
            } else {
                SkillListDB skillList = dbMng.SkillListDB_EN;
                SkillContentListDB skillContentList = dbMng.SkillContentListDB_EN;
                for (int i = 0; i < charaParam.UniqueSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.UniqueSkillLearnDatas[i].SkillID;
                    string actionGraphicsId = skillList.GetSkillActionGraphicsID(skillId);
                    if (!string.IsNullOrEmpty(actionGraphicsId)) {
                        list.Add(actionGraphicsId);
                    }
                    List<int> refIds = skillContentList.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionGraphicsId = skillListDB_CARD.GetSkillActionGraphicsID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionGraphicsId)) {
                            list.Add(actionGraphicsId);
                        }
                    }
                }
                for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++) {
                    int skillId = charaParam.ClassSkillLearnDatas[i].SkillID;
                    string actionGraphicsId = skillList.GetSkillActionGraphicsID(skillId);
                    if (!string.IsNullOrEmpty(actionGraphicsId)) {
                        list.Add(actionGraphicsId);
                    }
                    List<int> refIds = skillContentList.GetRefIdsOnCardType(skillId);
                    for (int j = 0; j < refIds.Count; j++) {
                        actionGraphicsId = skillListDB_CARD.GetSkillActionGraphicsID(refIds[j]);
                        if (!string.IsNullOrEmpty(actionGraphicsId)) {
                            list.Add(actionGraphicsId);
                        }
                    }
                }
            }
            HashSet<string> hashSet = new HashSet<string>(list);
            string[] array = new string[hashSet.Count];
            hashSet.CopyTo(array, 0);
            return array;
        }

        public static string GetSkillActionPlanResourcePath() {
            return "battle/sap/sap" + MeigeResourceManager.AB_EXTENSION;
        }

        public static string GetSkillActionPlanObjectPath(string sapID) {
            return "SAP_" + sapID;
        }

        public static string GetSkillActionGraphicsResourcePath() {
            return "battle/sag/sag" + MeigeResourceManager.AB_EXTENSION;
        }

        public static string GetSkillActionGraphicsObjectPath(string sagID) {
            return "SAG_" + sagID;
        }

        public static string GetBattleAIDataResourcePath() {
            return "database/battleai/battleai" + MeigeResourceManager.AB_EXTENSION;
        }

        public static string GetBattleAIDataObjectPath(int AIID) {
            return "BattleAIData_" + AIID;
        }

        public static string GetTweetListDBResourcePath(eCharaNamedType namedType) {
            return "Data/Database/TweetList_" + GameSystem.Inst.DbMng.NamedListDB.GetResourceBaseName(namedType);
        }

        public static float[] CalcCharaParamApplyTownBuff(TownBuff srcTownBuff, CharacterParam srcCharaParam) {
            float[] buffs = new float[(int)eTownObjectBuffType.Num];
            List<TownBuff.BuffParam> buffParam = srcTownBuff.GetBuffParam(GetTitleType(srcCharaParam.NamedType), srcCharaParam.ClassType, srcCharaParam.ElementType);
            if (buffParam != null) {
                for (int i = 0; i < buffParam.Count; i++) {
                    buffs[(int)buffParam[i].m_Type] += CalcTownBuff(buffParam[i].m_Type, buffParam[i].m_Val, srcCharaParam);
                }
            }
            return buffs;
        }

        public static float[] CalcCharaParamApplyTownBuff(float[] srcTownBuffValues, CharacterParam srcCharaParam) {
            float[] buffs = new float[(int)eTownObjectBuffType.Num];
            for (int i = 0; i < srcTownBuffValues.Length; i++) {
                buffs[i] = CalcTownBuff((eTownObjectBuffType)i, srcTownBuffValues[i], srcCharaParam);
            }
            return null;
        }

        public static float CalcTownBuff(eTownObjectBuffType type, float value, CharacterParam srcCharaParam) {
            float result = 0f;
            float buffFactor = value * 0.01f;
            switch (type) {
                case eTownObjectBuffType.Hp:
                    result = srcCharaParam.Hp * buffFactor;
                    break;
                case eTownObjectBuffType.Atk:
                    result = srcCharaParam.Atk * buffFactor;
                    break;
                case eTownObjectBuffType.Mgc:
                    result = srcCharaParam.Mgc * buffFactor;
                    break;
                case eTownObjectBuffType.Def:
                    result = srcCharaParam.Def * buffFactor;
                    break;
                case eTownObjectBuffType.MDef:
                    result = srcCharaParam.MDef * buffFactor;
                    break;
                case eTownObjectBuffType.Spd:
                    result = srcCharaParam.Spd * buffFactor;
                    break;
                case eTownObjectBuffType.Luck:
                    result = srcCharaParam.Luck * buffFactor;
                    break;
                case eTownObjectBuffType.ResistFire:
                case eTownObjectBuffType.ResistWater:
                case eTownObjectBuffType.ResistEarth:
                case eTownObjectBuffType.ResistWind:
                case eTownObjectBuffType.ResistMoon:
                case eTownObjectBuffType.ResistSun:
                    result = buffFactor;
                    break;
            }
            return result;
        }
    }
}
