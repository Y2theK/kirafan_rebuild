﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D5 RID: 1237
	[Token(Token = "0x20003CD")]
	[StructLayout(3)]
	public class CharacterWeaponListDB : ScriptableObject
	{
		// Token: 0x06001408 RID: 5128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BD")]
		[Address(RVA = "0x1011A31D8", Offset = "0x11A31D8", VA = "0x1011A31D8")]
		public CharacterWeaponListDB()
		{
		}

		// Token: 0x04001768 RID: 5992
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400114E")]
		public CharacterWeaponListDB_Param[] m_Params;
	}
}
