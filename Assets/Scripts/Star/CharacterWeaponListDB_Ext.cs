﻿namespace Star {
    public static class CharacterWeaponListDB_Ext {
        public static CharacterWeaponListDB_Param? GetParamByCharaID(this CharacterWeaponListDB self, int charaID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CharaID == charaID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static CharacterWeaponListDB_Param? GetParamByWeaponID(this CharacterWeaponListDB self, int weaponID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_WeaponID == weaponID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }
    }
}
