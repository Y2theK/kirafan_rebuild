﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004D4 RID: 1236
	[Token(Token = "0x20003CC")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct CharacterWeaponListDB_Param
	{
		// Token: 0x04001764 RID: 5988
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400114A")]
		public int m_ID;

		// Token: 0x04001765 RID: 5989
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400114B")]
		public int m_CharaID;

		// Token: 0x04001766 RID: 5990
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400114C")]
		public int m_WeaponID;

		// Token: 0x04001767 RID: 5991
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400114D")]
		public int m_CondLv;
	}
}
