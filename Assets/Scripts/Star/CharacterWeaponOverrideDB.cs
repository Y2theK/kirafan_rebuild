﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D7 RID: 1239
	[Token(Token = "0x20003CF")]
	[StructLayout(3)]
	public class CharacterWeaponOverrideDB : ScriptableObject
	{
		// Token: 0x06001409 RID: 5129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BE")]
		[Address(RVA = "0x1011A3450", Offset = "0x11A3450", VA = "0x1011A3450")]
		public CharacterWeaponOverrideDB()
		{
		}

		// Token: 0x0400176C RID: 5996
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001152")]
		public CharacterWeaponOverrideDB_Param[] m_Params;
	}
}
