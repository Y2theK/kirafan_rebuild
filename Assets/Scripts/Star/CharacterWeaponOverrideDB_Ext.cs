﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000508 RID: 1288
	[Token(Token = "0x20003FE")]
	[StructLayout(3)]
	public static class CharacterWeaponOverrideDB_Ext
	{
		// Token: 0x06001544 RID: 5444 RVA: 0x000090C0 File Offset: 0x000072C0
		[Token(Token = "0x60013F9")]
		[Address(RVA = "0x1011A3458", Offset = "0x11A3458", VA = "0x1011A3458")]
		public static CharacterWeaponOverrideDB_Param? GetParam(this CharacterWeaponOverrideDB self, int charaID)
		{
			return null;
		}

		// Token: 0x06001545 RID: 5445 RVA: 0x000090D8 File Offset: 0x000072D8
		[Token(Token = "0x60013FA")]
		[Address(RVA = "0x1011A35BC", Offset = "0x11A35BC", VA = "0x1011A35BC")]
		public static CharacterWeaponOverrideDB_Param? GetParamIfExistBeforeEvolAnim(this CharacterWeaponOverrideDB self, int charaID)
		{
			return null;
		}

		// Token: 0x06001546 RID: 5446 RVA: 0x000090F0 File Offset: 0x000072F0
		[Token(Token = "0x60013FB")]
		[Address(RVA = "0x1011A3664", Offset = "0x11A3664", VA = "0x1011A3664")]
		public static CharacterWeaponOverrideDB_Param? GetParamIfExistBeforeEvolEffect(this CharacterWeaponOverrideDB self, int afterEvolCharaID)
		{
			return null;
		}
	}
}
