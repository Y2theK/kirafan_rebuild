﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004D6 RID: 1238
	[Token(Token = "0x20003CE")]
	[Serializable]
	[StructLayout(0, Size = 12)]
	public struct CharacterWeaponOverrideDB_Param
	{
		// Token: 0x04001769 RID: 5993
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400114F")]
		public int m_CharaID;

		// Token: 0x0400176A RID: 5994
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001150")]
		public int m_IsExistBeforeEvolAnim;

		// Token: 0x0400176B RID: 5995
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001151")]
		public int m_IsExistBeforeEvolEffect;
	}
}
