﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x02000484 RID: 1156
	[Token(Token = "0x2000384")]
	[StructLayout(3)]
	public sealed class Chest
	{
		// Token: 0x060013C5 RID: 5061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001280")]
		[Address(RVA = "0x1011A3708", Offset = "0x11A3708", VA = "0x1011A3708")]
		public Chest()
		{
		}

		// Token: 0x060013C6 RID: 5062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001281")]
		[Address(RVA = "0x1011A37C8", Offset = "0x11A37C8", VA = "0x1011A37C8")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x060013C7 RID: 5063 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001282")]
		[Address(RVA = "0x1011A385C", Offset = "0x11A385C", VA = "0x1011A385C")]
		public List<Chest.ChestData> GetChestList()
		{
			return null;
		}

		// Token: 0x060013C8 RID: 5064 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001283")]
		[Address(RVA = "0x1011A3864", Offset = "0x11A3864", VA = "0x1011A3864")]
		public List<Chest.ChestContent> GetCurrentStepContentList()
		{
			return null;
		}

		// Token: 0x060013C9 RID: 5065 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001284")]
		[Address(RVA = "0x1011A386C", Offset = "0x11A386C", VA = "0x1011A386C")]
		public Chest.ChestData GetChestData(int chestID)
		{
			return null;
		}

		// Token: 0x060013CA RID: 5066 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001285")]
		[Address(RVA = "0x1011A396C", Offset = "0x11A396C", VA = "0x1011A396C")]
		public Chest.ChestData GetChestDataTradeGroupID(int tradeGroupID)
		{
			return null;
		}

		// Token: 0x060013CB RID: 5067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001286")]
		[Address(RVA = "0x1011A3A6C", Offset = "0x11A3A6C", VA = "0x1011A3A6C")]
		private void ApplyChestData(ChestInfo newChestList)
		{
		}

		// Token: 0x060013CC RID: 5068 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001287")]
		[Address(RVA = "0x1011A3EA8", Offset = "0x11A3EA8", VA = "0x1011A3EA8")]
		public List<Chest.Result> GetResults()
		{
			return null;
		}

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x060013CD RID: 5069 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060013CE RID: 5070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000D")]
		private event Action<ChestDefine.eReturnChestGetList, string> m_OnResponse_ChestGetAll
		{
			[Token(Token = "0x6001288")]
			[Address(RVA = "0x1011A3EB0", Offset = "0x11A3EB0", VA = "0x1011A3EB0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6001289")]
			[Address(RVA = "0x1011A3FBC", Offset = "0x11A3FBC", VA = "0x1011A3FBC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x060013CF RID: 5071 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060013D0 RID: 5072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000E")]
		private event Action<ChestDefine.eReturnChestGetProgress, string> m_OnResponse_ChestGetProgress
		{
			[Token(Token = "0x600128A")]
			[Address(RVA = "0x1011A40C8", Offset = "0x11A40C8", VA = "0x1011A40C8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600128B")]
			[Address(RVA = "0x1011A41D4", Offset = "0x11A41D4", VA = "0x1011A41D4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x060013D1 RID: 5073 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060013D2 RID: 5074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000F")]
		private event Action<ChestDefine.eReturnChestDraw, int, string> m_OnResponse_ChestDraw
		{
			[Token(Token = "0x600128C")]
			[Address(RVA = "0x1011A42E0", Offset = "0x11A42E0", VA = "0x1011A42E0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600128D")]
			[Address(RVA = "0x1011A43EC", Offset = "0x11A43EC", VA = "0x1011A43EC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x060013D3 RID: 5075 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060013D4 RID: 5076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000010")]
		private event Action<ChestDefine.eReturnChestReset, string> m_OnResponse_ChestReset
		{
			[Token(Token = "0x600128E")]
			[Address(RVA = "0x1011A44F8", Offset = "0x11A44F8", VA = "0x1011A44F8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600128F")]
			[Address(RVA = "0x1011A4604", Offset = "0x11A4604", VA = "0x1011A4604")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x00008A00 File Offset: 0x00006C00
		[Token(Token = "0x6001290")]
		[Address(RVA = "0x1011A4710", Offset = "0x11A4710", VA = "0x1011A4710")]
		public bool Request_ChestGetAll(Action<ChestDefine.eReturnChestGetList, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001291")]
		[Address(RVA = "0x1011A4804", Offset = "0x11A4804", VA = "0x1011A4804")]
		private void OnResponse_ChestGetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060013D7 RID: 5079 RVA: 0x00008A18 File Offset: 0x00006C18
		[Token(Token = "0x6001292")]
		[Address(RVA = "0x1011A49D8", Offset = "0x11A49D8", VA = "0x1011A49D8")]
		public bool Request_ChestGetProgress(int chestId, int step, Action<ChestDefine.eReturnChestGetProgress, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060013D8 RID: 5080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001293")]
		[Address(RVA = "0x1011A4B04", Offset = "0x11A4B04", VA = "0x1011A4B04")]
		private void OnResponse_ChestGetProgress(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060013D9 RID: 5081 RVA: 0x00008A30 File Offset: 0x00006C30
		[Token(Token = "0x6001294")]
		[Address(RVA = "0x1011A4DA0", Offset = "0x11A4DA0", VA = "0x1011A4DA0")]
		public bool Request_ChestDraw(int chestID, int drowNum, Action<ChestDefine.eReturnChestDraw, int, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060013DA RID: 5082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001295")]
		[Address(RVA = "0x1011A4ECC", Offset = "0x11A4ECC", VA = "0x1011A4ECC")]
		private void OnResponse_ChestPlay(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x00008A48 File Offset: 0x00006C48
		[Token(Token = "0x6001296")]
		[Address(RVA = "0x1011A52D4", Offset = "0x11A52D4", VA = "0x1011A52D4")]
		public bool Request_ChestReset(int chestID, Action<ChestDefine.eReturnChestReset, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060013DC RID: 5084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001297")]
		[Address(RVA = "0x1011A53DC", Offset = "0x11A53DC", VA = "0x1011A53DC")]
		private void OnResponse_ChestReset(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x040015A9 RID: 5545
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FC1")]
		private List<Chest.ChestData> m_ChestList;

		// Token: 0x040015AA RID: 5546
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FC2")]
		private List<Chest.ChestContent> m_ChestContents;

		// Token: 0x040015AB RID: 5547
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000FC3")]
		private List<Chest.Result> m_Results;

		// Token: 0x02000485 RID: 1157
		[Token(Token = "0x2000DD4")]
		public class ChestData
		{
			// Token: 0x060013DD RID: 5085 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E09")]
			[Address(RVA = "0x1011A5550", Offset = "0x11A5550", VA = "0x1011A5550")]
			public ChestData()
			{
			}

			// Token: 0x060013DE RID: 5086 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E0A")]
			[Address(RVA = "0x1011A3B9C", Offset = "0x11A3B9C", VA = "0x1011A3B9C")]
			public ChestData(ChestInfo wwwParams)
			{
			}

			// Token: 0x040015B0 RID: 5552
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005969")]
			public int m_ID;

			// Token: 0x040015B1 RID: 5553
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400596A")]
			public string m_Name;

			// Token: 0x040015B2 RID: 5554
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400596B")]
			public string m_BannerName;

			// Token: 0x040015B3 RID: 5555
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400596C")]
			public string m_BGBaseName;

			// Token: 0x040015B4 RID: 5556
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400596D")]
			public int m_EventType;

			// Token: 0x040015B5 RID: 5557
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x400596E")]
			public int m_TradeGroupID;

			// Token: 0x040015B6 RID: 5558
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400596F")]
			public DateTime m_StartAt;

			// Token: 0x040015B7 RID: 5559
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005970")]
			public DateTime m_EndAt;

			// Token: 0x040015B8 RID: 5560
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4005971")]
			public DateTime m_DispStartAt;

			// Token: 0x040015B9 RID: 5561
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4005972")]
			public DateTime m_DispEndAt;

			// Token: 0x040015BA RID: 5562
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4005973")]
			public int m_CostItemId;

			// Token: 0x040015BB RID: 5563
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x4005974")]
			public int m_CostItemAmount;

			// Token: 0x040015BC RID: 5564
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4005975")]
			public int m_TutorialTipsId;

			// Token: 0x040015BD RID: 5565
			[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
			[Token(Token = "0x4005976")]
			public bool m_EnableReset;

			// Token: 0x040015BE RID: 5566
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4005977")]
			public int m_CurrentStock;

			// Token: 0x040015BF RID: 5567
			[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
			[Token(Token = "0x4005978")]
			public int m_TotalStock;

			// Token: 0x040015C0 RID: 5568
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4005979")]
			public int m_CurrentChest;

			// Token: 0x040015C1 RID: 5569
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x400597A")]
			public int m_MaxChest;

			// Token: 0x040015C2 RID: 5570
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x400597B")]
			public List<Chest.ChestContent> m_ChestContents;
		}

		// Token: 0x02000486 RID: 1158
		[Token(Token = "0x2000DD5")]
		public class ChestContent
		{
			// Token: 0x060013DF RID: 5087 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E0B")]
			[Address(RVA = "0x1011A5548", Offset = "0x11A5548", VA = "0x1011A5548")]
			public ChestContent()
			{
			}

			// Token: 0x060013E0 RID: 5088 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E0C")]
			[Address(RVA = "0x1011A4CCC", Offset = "0x11A4CCC", VA = "0x1011A4CCC")]
			public ChestContent(ChestPrize wwwParam)
			{
			}

			// Token: 0x040015C3 RID: 5571
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400597C")]
			public ePresentType m_Type;

			// Token: 0x040015C4 RID: 5572
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400597D")]
			public int m_ID;

			// Token: 0x040015C5 RID: 5573
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400597E")]
			public int m_Amount;

			// Token: 0x040015C6 RID: 5574
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x400597F")]
			public bool m_Flg;

			// Token: 0x040015C7 RID: 5575
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005980")]
			public int m_Remain;

			// Token: 0x040015C8 RID: 5576
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4005981")]
			public int m_Max;
		}

		// Token: 0x02000487 RID: 1159
		[Token(Token = "0x2000DD6")]
		public class Result
		{
			// Token: 0x060013E1 RID: 5089 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E0D")]
			[Address(RVA = "0x1011A5230", Offset = "0x11A5230", VA = "0x1011A5230")]
			public Result(ChestPrizeResult wwwParams)
			{
			}

			// Token: 0x060013E2 RID: 5090 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E0E")]
			[Address(RVA = "0x1011A55C0", Offset = "0x11A55C0", VA = "0x1011A55C0")]
			public Result(ePresentType presentType, int itemID, int itemNum, bool isResetTarget)
			{
			}

			// Token: 0x040015C9 RID: 5577
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005982")]
			public ePresentType m_PresentType;

			// Token: 0x040015CA RID: 5578
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005983")]
			public int m_ItemID;

			// Token: 0x040015CB RID: 5579
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005984")]
			public int m_ItemNum;

			// Token: 0x040015CC RID: 5580
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005985")]
			public bool m_IsResetTarget;
		}
	}
}
