﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000488 RID: 1160
	[Token(Token = "0x2000385")]
	[StructLayout(3)]
	public static class ChestDefine
	{
		// Token: 0x02000489 RID: 1161
		[Token(Token = "0x2000DD7")]
		public enum eCheckDraw
		{
			// Token: 0x040015CE RID: 5582
			[Token(Token = "0x4005987")]
			Ok,
			// Token: 0x040015CF RID: 5583
			[Token(Token = "0x4005988")]
			ItemIsShort
		}

		// Token: 0x0200048A RID: 1162
		[Token(Token = "0x2000DD8")]
		public enum eReturnChestGetList
		{
			// Token: 0x040015D1 RID: 5585
			[Token(Token = "0x400598A")]
			Success,
			// Token: 0x040015D2 RID: 5586
			[Token(Token = "0x400598B")]
			Unknown
		}

		// Token: 0x0200048B RID: 1163
		[Token(Token = "0x2000DD9")]
		public enum eReturnChestGetProgress
		{
			// Token: 0x040015D4 RID: 5588
			[Token(Token = "0x400598D")]
			Success,
			// Token: 0x040015D5 RID: 5589
			[Token(Token = "0x400598E")]
			OutOfPeriod,
			// Token: 0x040015D6 RID: 5590
			[Token(Token = "0x400598F")]
			Unknown
		}

		// Token: 0x0200048C RID: 1164
		[Token(Token = "0x2000DDA")]
		public enum eReturnChestDraw
		{
			// Token: 0x040015D8 RID: 5592
			[Token(Token = "0x4005991")]
			Success,
			// Token: 0x040015D9 RID: 5593
			[Token(Token = "0x4005992")]
			ChestOutOfPeriod,
			// Token: 0x040015DA RID: 5594
			[Token(Token = "0x4005993")]
			ChestEmptyReward,
			// Token: 0x040015DB RID: 5595
			[Token(Token = "0x4005994")]
			ItemIsShort,
			// Token: 0x040015DC RID: 5596
			[Token(Token = "0x4005995")]
			Unknown
		}

		// Token: 0x0200048D RID: 1165
		[Token(Token = "0x2000DDB")]
		public enum eReturnChestReset
		{
			// Token: 0x040015DE RID: 5598
			[Token(Token = "0x4005997")]
			Success,
			// Token: 0x040015DF RID: 5599
			[Token(Token = "0x4005998")]
			OutOfPeriod,
			// Token: 0x040015E0 RID: 5600
			[Token(Token = "0x4005999")]
			CanNotReset,
			// Token: 0x040015E1 RID: 5601
			[Token(Token = "0x400599A")]
			Unknown
		}
	}
}
