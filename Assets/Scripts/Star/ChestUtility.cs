﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200048E RID: 1166
	[Token(Token = "0x2000386")]
	[StructLayout(3)]
	public static class ChestUtility
	{
		// Token: 0x060013E3 RID: 5091 RVA: 0x00008A60 File Offset: 0x00006C60
		[Token(Token = "0x6001298")]
		[Address(RVA = "0x1011A5618", Offset = "0x11A5618", VA = "0x1011A5618")]
		public static int GetDrawableCount(Chest.ChestData chestData, bool isClampSystemLimit = false, bool isClampChestStock = false)
		{
			return 0;
		}

		// Token: 0x060013E4 RID: 5092 RVA: 0x00008A78 File Offset: 0x00006C78
		[Token(Token = "0x6001299")]
		[Address(RVA = "0x1011A5668", Offset = "0x11A5668", VA = "0x1011A5668")]
		public static int GetDrawableCount(Chest.ChestData chestData)
		{
			return 0;
		}

		// Token: 0x060013E5 RID: 5093 RVA: 0x00008A90 File Offset: 0x00006C90
		[Token(Token = "0x600129A")]
		[Address(RVA = "0x1011A5800", Offset = "0x11A5800", VA = "0x1011A5800")]
		public static int GetDrawableCount(int itemID, int costAmount)
		{
			return 0;
		}

		// Token: 0x060013E6 RID: 5094 RVA: 0x00008AA8 File Offset: 0x00006CA8
		[Token(Token = "0x600129B")]
		[Address(RVA = "0x1011A56AC", Offset = "0x11A56AC", VA = "0x1011A56AC")]
		public static int ClampDrawableCountSystemLimit(int playCount)
		{
			return 0;
		}

		// Token: 0x060013E7 RID: 5095 RVA: 0x00008AC0 File Offset: 0x00006CC0
		[Token(Token = "0x600129C")]
		[Address(RVA = "0x1011A5774", Offset = "0x11A5774", VA = "0x1011A5774")]
		public static int ClampDrawableCountChestStock(int playCount, Chest.ChestData chestData)
		{
			return 0;
		}

		// Token: 0x060013E8 RID: 5096 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600129D")]
		[Address(RVA = "0x1011A58A8", Offset = "0x11A58A8", VA = "0x1011A58A8")]
		public static List<Chest.ChestContent> GetMainPrizeList(Chest.ChestData chestData)
		{
			return null;
		}

		// Token: 0x060013E9 RID: 5097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600129E")]
		[Address(RVA = "0x1011A59EC", Offset = "0x11A59EC", VA = "0x1011A59EC")]
		public static void GetPrizeCount(List<Chest.ChestContent> list, ref int remainCount, ref int maxCount)
		{
		}

		// Token: 0x060013EA RID: 5098 RVA: 0x00008AD8 File Offset: 0x00006CD8
		[Token(Token = "0x600129F")]
		[Address(RVA = "0x1011A5AF4", Offset = "0x11A5AF4", VA = "0x1011A5AF4")]
		public static bool IsExistMainPrize(Chest.ChestData chestData)
		{
			return default(bool);
		}

		// Token: 0x060013EB RID: 5099 RVA: 0x00008AF0 File Offset: 0x00006CF0
		[Token(Token = "0x60012A0")]
		[Address(RVA = "0x1011A5B58", Offset = "0x11A5B58", VA = "0x1011A5B58")]
		public static bool IsRemainMainPrize(Chest.ChestData chestData)
		{
			return default(bool);
		}

		// Token: 0x060013EC RID: 5100 RVA: 0x00008B08 File Offset: 0x00006D08
		[Token(Token = "0x60012A1")]
		[Address(RVA = "0x1011A5B94", Offset = "0x11A5B94", VA = "0x1011A5B94")]
		public static bool IsRareChestResult()
		{
			return default(bool);
		}
	}
}
