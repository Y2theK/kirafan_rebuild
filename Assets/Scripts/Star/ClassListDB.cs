﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D9 RID: 1241
	[Token(Token = "0x20003D1")]
	[StructLayout(3)]
	public class ClassListDB : ScriptableObject
	{
		// Token: 0x0600140A RID: 5130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012BF")]
		[Address(RVA = "0x1011A5C98", Offset = "0x11A5C98", VA = "0x1011A5C98")]
		public ClassListDB()
		{
		}

		// Token: 0x04001770 RID: 6000
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001156")]
		public ClassListDB_Param[] m_Params;
	}
}
