﻿namespace Star {
    public static class ClassListDB_Ext {
        public static ClassListDB_Param GetParam(this ClassListDB self, eClassType classType) {
            return self.m_Params[(int)classType];
        }
    }
}
