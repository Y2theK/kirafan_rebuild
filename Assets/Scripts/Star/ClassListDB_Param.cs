﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004D8 RID: 1240
	[Token(Token = "0x20003D0")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct ClassListDB_Param
	{
		// Token: 0x0400176D RID: 5997
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001153")]
		public string m_ResouceBaseName;

		// Token: 0x0400176E RID: 5998
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001154")]
		public int m_NormalAttackSkillID;

		// Token: 0x0400176F RID: 5999
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001155")]
		public int m_DefaultWeaponID;
	}
}
