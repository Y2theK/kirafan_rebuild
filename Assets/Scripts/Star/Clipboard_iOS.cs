﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000ABC RID: 2748
	[Token(Token = "0x200078F")]
	[StructLayout(3)]
	public static class Clipboard_iOS
	{
		// Token: 0x06002F67 RID: 12135
		[Token(Token = "0x6002B45")]
		[Address(RVA = "0x1011A5D58", Offset = "0x11A5D58", VA = "0x1011A5D58")]
		private static extern void _SetClipboard(string value);

		// Token: 0x06002F68 RID: 12136
		[Token(Token = "0x6002B46")]
		[Address(RVA = "0x1011A5D80", Offset = "0x11A5D80", VA = "0x1011A5D80")]
		private static extern string _GetClipboard();

		// Token: 0x06002F69 RID: 12137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B47")]
		[Address(RVA = "0x1011A5CC8", Offset = "0x11A5CC8", VA = "0x1011A5CC8")]
		public static void SetClipboard(string value)
		{
		}

		// Token: 0x06002F6A RID: 12138 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002B48")]
		[Address(RVA = "0x1011A5D24", Offset = "0x11A5D24", VA = "0x1011A5D24")]
		public static string GetClipboard()
		{
			return null;
		}
	}
}
