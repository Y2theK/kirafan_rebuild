﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000714 RID: 1812
	[Token(Token = "0x2000599")]
	[StructLayout(3)]
	public class ComItemUpState
	{
		// Token: 0x06001A7F RID: 6783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001870")]
		[Address(RVA = "0x1011A5DB4", Offset = "0x11A5DB4", VA = "0x1011A5DB4")]
		public ComItemUpState()
		{
		}

		// Token: 0x06001A80 RID: 6784 RVA: 0x0000BDF0 File Offset: 0x00009FF0
		[Token(Token = "0x6001871")]
		[Address(RVA = "0x1011A5E28", Offset = "0x11A5E28", VA = "0x1011A5E28")]
		public int GetNewListNum()
		{
			return 0;
		}

		// Token: 0x06001A81 RID: 6785 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001872")]
		[Address(RVA = "0x1011A5E88", Offset = "0x11A5E88", VA = "0x1011A5E88")]
		public ComItemUpState.UpCategory GetNewListAt(int findex)
		{
			return null;
		}

		// Token: 0x06001A82 RID: 6786 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001873")]
		[Address(RVA = "0x1011A5EF8", Offset = "0x11A5EF8", VA = "0x1011A5EF8")]
		private ComItemUpState.UpCategory SearchCategory(ComItemUpState.eUpCategory fcategory, int fitemno)
		{
			return null;
		}

		// Token: 0x06001A83 RID: 6787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001874")]
		[Address(RVA = "0x1011A6040", Offset = "0x11A6040", VA = "0x1011A6040")]
		public void AddCategory(ComItemUpState.eUpCategory fcategory, int fitemno, long fnum)
		{
		}

		// Token: 0x06001A84 RID: 6788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001875")]
		[Address(RVA = "0x1011A6150", Offset = "0x11A6150", VA = "0x1011A6150")]
		public void CalcPlayerDataNewParam(Player pcompdata)
		{
		}

		// Token: 0x06001A85 RID: 6789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001876")]
		[Address(RVA = "0x1011A635C", Offset = "0x11A635C", VA = "0x1011A635C")]
		public void CalcPlayerItemNewParam(PlayerItemSummary[] pcompdata)
		{
		}

		// Token: 0x06001A86 RID: 6790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001877")]
		[Address(RVA = "0x1011A6758", Offset = "0x11A6758", VA = "0x1011A6758")]
		public void CalcNamedFriendShipNewParam(PlayerNamedType[] pcompdata)
		{
		}

		// Token: 0x04002AA6 RID: 10918
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022BA")]
		public List<ComItemUpState.UpCategory> m_List;

		// Token: 0x02000715 RID: 1813
		[Token(Token = "0x2000E4D")]
		public enum eUpCategory
		{
			// Token: 0x04002AA8 RID: 10920
			[Token(Token = "0x4005B66")]
			Item,
			// Token: 0x04002AA9 RID: 10921
			[Token(Token = "0x4005B67")]
			Money,
			// Token: 0x04002AAA RID: 10922
			[Token(Token = "0x4005B68")]
			Point,
			// Token: 0x04002AAB RID: 10923
			[Token(Token = "0x4005B69")]
			Stamina,
			// Token: 0x04002AAC RID: 10924
			[Token(Token = "0x4005B6A")]
			FriendShip
		}

		// Token: 0x02000716 RID: 1814
		[Token(Token = "0x2000E4E")]
		public class UpCategory
		{
			// Token: 0x06001A87 RID: 6791 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED1")]
			[Address(RVA = "0x1011A6148", Offset = "0x11A6148", VA = "0x1011A6148")]
			public UpCategory()
			{
			}

			// Token: 0x04002AAD RID: 10925
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B6B")]
			public ComItemUpState.eUpCategory m_Category;

			// Token: 0x04002AAE RID: 10926
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B6C")]
			public long m_Num;

			// Token: 0x04002AAF RID: 10927
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B6D")]
			public int m_No;
		}
	}
}
