﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.Comic
{
	// Token: 0x02000C31 RID: 3121
	[Token(Token = "0x200085A")]
	[StructLayout(3)]
	public class ComicAction
	{
		// Token: 0x060037E5 RID: 14309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032DF")]
		[Address(RVA = "0x1011A6C64", Offset = "0x11A6C64", VA = "0x1011A6C64")]
		public void Clear()
		{
		}

		// Token: 0x060037E6 RID: 14310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032E0")]
		[Address(RVA = "0x1011A6CC4", Offset = "0x11A6CC4", VA = "0x1011A6CC4")]
		public void AddKey(float sec, eComicActionCurveType curveType, ComicAction.ActionElement state)
		{
		}

		// Token: 0x060037E7 RID: 14311 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60032E1")]
		[Address(RVA = "0x1011A6EA8", Offset = "0x11A6EA8", VA = "0x1011A6EA8")]
		public ComicAction.ActionElement GetCalcedElement(eComicActionElementType actionType, float sec)
		{
			return null;
		}

		// Token: 0x060037E8 RID: 14312 RVA: 0x00017970 File Offset: 0x00015B70
		[Token(Token = "0x60032E2")]
		[Address(RVA = "0x1011A74D0", Offset = "0x11A74D0", VA = "0x1011A74D0")]
		public float GetMaxSec()
		{
			return 0f;
		}

		// Token: 0x060037E9 RID: 14313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032E3")]
		[Address(RVA = "0x1011A75B0", Offset = "0x11A75B0", VA = "0x1011A75B0")]
		public void ApplyToRectTransform(RectTransform rt, float sec)
		{
		}

		// Token: 0x060037EA RID: 14314 RVA: 0x00017988 File Offset: 0x00015B88
		[Token(Token = "0x60032E4")]
		[Address(RVA = "0x1011A7B28", Offset = "0x11A7B28", VA = "0x1011A7B28")]
		private Vector3 GetShakeOffset(eComicActionElementType actionType, float sec)
		{
			return default(Vector3);
		}

		// Token: 0x060037EB RID: 14315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032E5")]
		[Address(RVA = "0x1011A7D54", Offset = "0x11A7D54", VA = "0x1011A7D54")]
		public ComicAction()
		{
		}

		// Token: 0x0400475D RID: 18269
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40031FE")]
		private List<ComicAction.ActionKey> m_KeyList;

		// Token: 0x02000C32 RID: 3122
		[Token(Token = "0x200109F")]
		public class ActionElement
		{
			// Token: 0x060037EC RID: 14316 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061BA")]
			[Address(RVA = "0x1011A7304", Offset = "0x11A7304", VA = "0x1011A7304")]
			public static ComicAction.ActionElement Lerp(ComicAction.ActionElement a, ComicAction.ActionElement b, float t)
			{
				return null;
			}

			// Token: 0x060037ED RID: 14317 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061BB")]
			[Address(RVA = "0x1011A7DC4", Offset = "0x11A7DC4", VA = "0x1011A7DC4")]
			public void SetupPosition(Vector2 position)
			{
			}

			// Token: 0x060037EE RID: 14318 RVA: 0x000179A0 File Offset: 0x00015BA0
			[Token(Token = "0x60061BC")]
			[Address(RVA = "0x1011A7998", Offset = "0x11A7998", VA = "0x1011A7998")]
			public Vector2 ToPosition()
			{
				return default(Vector2);
			}

			// Token: 0x060037EF RID: 14319 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061BD")]
			[Address(RVA = "0x1011A7E88", Offset = "0x11A7E88", VA = "0x1011A7E88")]
			public void SetupRotate(float rotate)
			{
			}

			// Token: 0x060037F0 RID: 14320 RVA: 0x000179B8 File Offset: 0x00015BB8
			[Token(Token = "0x60061BE")]
			[Address(RVA = "0x1011A7A38", Offset = "0x11A7A38", VA = "0x1011A7A38")]
			public float ToRotate()
			{
				return 0f;
			}

			// Token: 0x060037F1 RID: 14321 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061BF")]
			[Address(RVA = "0x1011A7F1C", Offset = "0x11A7F1C", VA = "0x1011A7F1C")]
			public void SetupScale(Vector2 scale)
			{
			}

			// Token: 0x060037F2 RID: 14322 RVA: 0x000179D0 File Offset: 0x00015BD0
			[Token(Token = "0x60061C0")]
			[Address(RVA = "0x1011A7A7C", Offset = "0x11A7A7C", VA = "0x1011A7A7C")]
			public Vector3 ToScale()
			{
				return default(Vector3);
			}

			// Token: 0x060037F3 RID: 14323 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061C1")]
			[Address(RVA = "0x1011A7FE4", Offset = "0x11A7FE4", VA = "0x1011A7FE4")]
			public void SetupColor(Color color)
			{
			}

			// Token: 0x060037F4 RID: 14324 RVA: 0x000179E8 File Offset: 0x00015BE8
			[Token(Token = "0x60061C2")]
			[Address(RVA = "0x1011A814C", Offset = "0x11A814C", VA = "0x1011A814C")]
			public Color ToColor()
			{
				return default(Color);
			}

			// Token: 0x060037F5 RID: 14325 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061C3")]
			[Address(RVA = "0x1011A826C", Offset = "0x11A826C", VA = "0x1011A826C")]
			public void SetupShake(eComicActionElementType type, float amplitude, float cycleSec)
			{
			}

			// Token: 0x060037F6 RID: 14326 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061C4")]
			[Address(RVA = "0x1011A7128", Offset = "0x11A7128", VA = "0x1011A7128")]
			public void SetupDefault(eComicActionElementType actionType)
			{
			}

			// Token: 0x060037F7 RID: 14327 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061C5")]
			[Address(RVA = "0x1011A7120", Offset = "0x11A7120", VA = "0x1011A7120")]
			public ActionElement()
			{
			}

			// Token: 0x0400475E RID: 18270
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068B1")]
			public eComicActionElementType m_ActionElementType;

			// Token: 0x0400475F RID: 18271
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068B2")]
			public float[] m_Args;
		}

		// Token: 0x02000C33 RID: 3123
		[Token(Token = "0x20010A0")]
		public class ActionKey
		{
			// Token: 0x060037F8 RID: 14328 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061C6")]
			[Address(RVA = "0x1011A6E5C", Offset = "0x11A6E5C", VA = "0x1011A6E5C")]
			public ActionKey(float sec, eComicActionCurveType curveType, ComicAction.ActionElement element)
			{
			}

			// Token: 0x04004760 RID: 18272
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068B3")]
			public float m_Sec;

			// Token: 0x04004761 RID: 18273
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40068B4")]
			public eComicActionCurveType m_CurveType;

			// Token: 0x04004762 RID: 18274
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068B5")]
			public ComicAction.ActionElement m_Element;
		}
	}
}
