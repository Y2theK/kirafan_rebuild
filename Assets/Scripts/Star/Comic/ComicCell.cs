﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.Comic
{
	// Token: 0x02000C34 RID: 3124
	[Token(Token = "0x200085B")]
	[StructLayout(3)]
	public class ComicCell : MonoBehaviour
	{
		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x060037F9 RID: 14329 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000421")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60032E6")]
			[Address(RVA = "0x1011A833C", Offset = "0x11A833C", VA = "0x1011A833C")]
			get
			{
				return null;
			}
		}

		// Token: 0x060037FA RID: 14330 RVA: 0x00017A00 File Offset: 0x00015C00
		[Token(Token = "0x60032E7")]
		[Address(RVA = "0x1011A83D4", Offset = "0x11A83D4", VA = "0x1011A83D4")]
		public ComicCell.eState GetState()
		{
			return ComicCell.eState.Hide;
		}

		// Token: 0x060037FB RID: 14331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032E8")]
		[Address(RVA = "0x1011A83DC", Offset = "0x11A83DC", VA = "0x1011A83DC")]
		public void Destroy()
		{
		}

		// Token: 0x060037FC RID: 14332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032E9")]
		[Address(RVA = "0x1011A83E0", Offset = "0x11A83E0", VA = "0x1011A83E0")]
		public void Clear()
		{
		}

		// Token: 0x060037FD RID: 14333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032EA")]
		[Address(RVA = "0x1011A856C", Offset = "0x11A856C", VA = "0x1011A856C")]
		public void SetBGSprite(Sprite sprite)
		{
		}

		// Token: 0x060037FE RID: 14334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032EB")]
		[Address(RVA = "0x1011A85A4", Offset = "0x11A85A4", VA = "0x1011A85A4")]
		public void SetupInOutAction(ComicAction inAct, ComicAction outAct, float fadeSec)
		{
		}

		// Token: 0x060037FF RID: 14335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032EC")]
		[Address(RVA = "0x1011A85B0", Offset = "0x11A85B0", VA = "0x1011A85B0")]
		public void PlayIn()
		{
		}

		// Token: 0x06003800 RID: 14336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032ED")]
		[Address(RVA = "0x1011A8764", Offset = "0x11A8764", VA = "0x1011A8764")]
		public void PlayOut()
		{
		}

		// Token: 0x06003801 RID: 14337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032EE")]
		[Address(RVA = "0x1011A8770", Offset = "0x11A8770", VA = "0x1011A8770")]
		private void Update()
		{
		}

		// Token: 0x06003802 RID: 14338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032EF")]
		[Address(RVA = "0x1011A85BC", Offset = "0x11A85BC", VA = "0x1011A85BC")]
		private void ApplyState(ComicCell.eState state, float stateTime)
		{
		}

		// Token: 0x06003803 RID: 14339 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60032F0")]
		[Address(RVA = "0x1011A8B44", Offset = "0x11A8B44", VA = "0x1011A8B44")]
		public RectTransform GetBody()
		{
			return null;
		}

		// Token: 0x06003804 RID: 14340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F1")]
		[Address(RVA = "0x1011A8B4C", Offset = "0x11A8B4C", VA = "0x1011A8B4C")]
		public void PlayAction(ComicAction act)
		{
		}

		// Token: 0x06003805 RID: 14341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F2")]
		[Address(RVA = "0x1011A8918", Offset = "0x11A8918", VA = "0x1011A8918")]
		private void ApplyAction(ComicAction act, float sec)
		{
		}

		// Token: 0x06003806 RID: 14342 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60032F3")]
		[Address(RVA = "0x1011A8B58", Offset = "0x11A8B58", VA = "0x1011A8B58")]
		private ComicAction GetCurrentAction()
		{
			return null;
		}

		// Token: 0x06003807 RID: 14343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F4")]
		[Address(RVA = "0x1011A8B98", Offset = "0x11A8B98", VA = "0x1011A8B98")]
		public void SkipAction()
		{
		}

		// Token: 0x06003808 RID: 14344 RVA: 0x00017A18 File Offset: 0x00015C18
		[Token(Token = "0x60032F5")]
		[Address(RVA = "0x1011A8AC4", Offset = "0x11A8AC4", VA = "0x1011A8AC4")]
		public bool IsExecutingAction()
		{
			return default(bool);
		}

		// Token: 0x06003809 RID: 14345 RVA: 0x00017A30 File Offset: 0x00015C30
		[Token(Token = "0x60032F6")]
		[Address(RVA = "0x1011A8C2C", Offset = "0x11A8C2C", VA = "0x1011A8C2C")]
		public float GetAlpha()
		{
			return 0f;
		}

		// Token: 0x0600380A RID: 14346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F7")]
		[Address(RVA = "0x1011A8C34", Offset = "0x11A8C34", VA = "0x1011A8C34")]
		public void SetActionSpeed(float speed)
		{
		}

		// Token: 0x0600380B RID: 14347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F8")]
		[Address(RVA = "0x1011A8C3C", Offset = "0x11A8C3C", VA = "0x1011A8C3C")]
		public void PlayZoom(float sec, Vector2 position, Vector2 scale)
		{
		}

		// Token: 0x0600380C RID: 14348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032F9")]
		[Address(RVA = "0x1011A8C20", Offset = "0x11A8C20", VA = "0x1011A8C20")]
		private void SkipZoom()
		{
		}

		// Token: 0x0600380D RID: 14349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032FA")]
		[Address(RVA = "0x1011A87BC", Offset = "0x11A87BC", VA = "0x1011A87BC")]
		private void UpdateZoom()
		{
		}

		// Token: 0x0600380E RID: 14350 RVA: 0x00017A48 File Offset: 0x00015C48
		[Token(Token = "0x60032FB")]
		[Address(RVA = "0x1011A8D88", Offset = "0x11A8D88", VA = "0x1011A8D88")]
		public bool IsZoomMoving()
		{
			return default(bool);
		}

		// Token: 0x0600380F RID: 14351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032FC")]
		[Address(RVA = "0x1011A8DAC", Offset = "0x11A8DAC", VA = "0x1011A8DAC")]
		public ComicCell()
		{
		}

		// Token: 0x04004763 RID: 18275
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40031FF")]
		[SerializeField]
		private Image m_BGImage;

		// Token: 0x04004764 RID: 18276
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003200")]
		[SerializeField]
		private RectTransform m_Body;

		// Token: 0x04004765 RID: 18277
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003201")]
		[SerializeField]
		private RectTransform m_ZoomTarget;

		// Token: 0x04004766 RID: 18278
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003202")]
		[SerializeField]
		private RectTransform m_OffsetTarget;

		// Token: 0x04004767 RID: 18279
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003203")]
		private ComicAction m_CellInAction;

		// Token: 0x04004768 RID: 18280
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003204")]
		private ComicAction m_CellOutAction;

		// Token: 0x04004769 RID: 18281
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003205")]
		private ComicAction m_Action;

		// Token: 0x0400476A RID: 18282
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003206")]
		private float m_ActionSpeed;

		// Token: 0x0400476B RID: 18283
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003207")]
		private float m_CurrentAlpha;

		// Token: 0x0400476C RID: 18284
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003208")]
		private ComicCell.eState m_State;

		// Token: 0x0400476D RID: 18285
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003209")]
		private float m_StateTime;

		// Token: 0x0400476E RID: 18286
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400320A")]
		private float m_FadeSec;

		// Token: 0x0400476F RID: 18287
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400320B")]
		private float m_ZoomSecCount;

		// Token: 0x04004770 RID: 18288
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400320C")]
		private float m_ZoomSec;

		// Token: 0x04004771 RID: 18289
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400320D")]
		private Vector2 m_ZoomStartOffset;

		// Token: 0x04004772 RID: 18290
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x400320E")]
		private Vector2 m_ZoomEndOffset;

		// Token: 0x04004773 RID: 18291
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x400320F")]
		private Vector2 m_ZoomStartScale;

		// Token: 0x04004774 RID: 18292
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003210")]
		private Vector2 m_ZoomEndScale;

		// Token: 0x04004775 RID: 18293
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003211")]
		private RectTransform m_RectTransform;

		// Token: 0x02000C35 RID: 3125
		[Token(Token = "0x20010A1")]
		public enum eState
		{
			// Token: 0x04004777 RID: 18295
			[Token(Token = "0x40068B7")]
			Hide,
			// Token: 0x04004778 RID: 18296
			[Token(Token = "0x40068B8")]
			In,
			// Token: 0x04004779 RID: 18297
			[Token(Token = "0x40068B9")]
			Idle,
			// Token: 0x0400477A RID: 18298
			[Token(Token = "0x40068BA")]
			Out
		}
	}
}
