﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.Comic
{
	// Token: 0x02000C36 RID: 3126
	[Token(Token = "0x200085C")]
	[StructLayout(3)]
	public class ComicCommand
	{
		// Token: 0x06003810 RID: 14352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60032FD")]
		[Address(RVA = "0x1011A8DBC", Offset = "0x11A8DBC", VA = "0x1011A8DBC")]
		public ComicCommand(string name, string[] args)
		{
		}

		// Token: 0x06003811 RID: 14353 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60032FE")]
		[Address(RVA = "0x1011A8DF4", Offset = "0x11A8DF4", VA = "0x1011A8DF4")]
		public string GetArg(int idx)
		{
			return null;
		}

		// Token: 0x06003812 RID: 14354 RVA: 0x00017A60 File Offset: 0x00015C60
		[Token(Token = "0x60032FF")]
		[Address(RVA = "0x1011A8E7C", Offset = "0x11A8E7C", VA = "0x1011A8E7C")]
		public int GetArgNum()
		{
			return 0;
		}

		// Token: 0x0400477B RID: 18299
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003212")]
		public string m_Name;

		// Token: 0x0400477C RID: 18300
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003213")]
		private string[] m_Args;
	}
}
