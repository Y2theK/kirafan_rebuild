﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.Comic
{
	// Token: 0x02000C37 RID: 3127
	[Token(Token = "0x200085D")]
	[StructLayout(3)]
	public class ComicCommandParser
	{
		// Token: 0x06003813 RID: 14355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003300")]
		[Address(RVA = "0x1011A8EA8", Offset = "0x11A8EA8", VA = "0x1011A8EA8")]
		public void Exec(ComicCommand cmd, ComicPlayer player)
		{
		}

		// Token: 0x06003814 RID: 14356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003301")]
		[Address(RVA = "0x1011A91C0", Offset = "0x11A91C0", VA = "0x1011A91C0")]
		public void BG(string name, string inAction, string outAction, float fadeSec)
		{
		}

		// Token: 0x06003815 RID: 14357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003302")]
		[Address(RVA = "0x1011A948C", Offset = "0x11A948C", VA = "0x1011A948C")]
		public void Sprite(string refName, string actionName, float fadeSec)
		{
		}

		// Token: 0x06003816 RID: 14358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003303")]
		[Address(RVA = "0x1011A9A04", Offset = "0x11A9A04", VA = "0x1011A9A04")]
		public void BGAction(string actionName)
		{
		}

		// Token: 0x06003817 RID: 14359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003304")]
		[Address(RVA = "0x1011A9D58", Offset = "0x11A9D58", VA = "0x1011A9D58")]
		public void SpriteAction(string spriteName, string actionName)
		{
		}

		// Token: 0x06003818 RID: 14360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003305")]
		[Address(RVA = "0x1011A9F68", Offset = "0x11A9F68", VA = "0x1011A9F68")]
		public void BGActionSpeed(float speed = 1f)
		{
		}

		// Token: 0x06003819 RID: 14361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003306")]
		[Address(RVA = "0x1011AA00C", Offset = "0x11AA00C", VA = "0x1011AA00C")]
		public void SpriteActionSpeed(string spriteName, float speed = 1f)
		{
		}

		// Token: 0x0600381A RID: 14362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003307")]
		[Address(RVA = "0x1011AA0E4", Offset = "0x11AA0E4", VA = "0x1011AA0E4")]
		public void SpritePivot(string spriteName, float x = 0.5f, float y = 0.5f)
		{
		}

		// Token: 0x0600381B RID: 14363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003308")]
		[Address(RVA = "0x1011AA1D0", Offset = "0x11AA1D0", VA = "0x1011AA1D0")]
		public void BGZoom(float sec, float x, float y, float scale = 1f)
		{
		}

		// Token: 0x0600381C RID: 14364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003309")]
		[Address(RVA = "0x1011AA2D0", Offset = "0x11AA2D0", VA = "0x1011AA2D0")]
		public void SpritePosition(string spriteName, float sec, float x, float y, string curveType)
		{
		}

		// Token: 0x0600381D RID: 14365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330A")]
		[Address(RVA = "0x1011AA50C", Offset = "0x11AA50C", VA = "0x1011AA50C")]
		public void SpriteRotate(string spriteName, float sec, float r, string curveType)
		{
		}

		// Token: 0x0600381E RID: 14366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330B")]
		[Address(RVA = "0x1011AA610", Offset = "0x11AA610", VA = "0x1011AA610")]
		public void SpriteScale(string spriteName, float sec, float x, float y, string curveType)
		{
		}

		// Token: 0x0600381F RID: 14367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330C")]
		[Address(RVA = "0x1011AA730", Offset = "0x11AA730", VA = "0x1011AA730")]
		public void SpritePriorityTop(string spriteName)
		{
		}

		// Token: 0x06003820 RID: 14368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330D")]
		[Address(RVA = "0x1011AA830", Offset = "0x11AA830", VA = "0x1011AA830")]
		public void FadeOut(float sec, string color)
		{
		}

		// Token: 0x06003821 RID: 14369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330E")]
		[Address(RVA = "0x1011AA968", Offset = "0x11AA968", VA = "0x1011AA968")]
		public void FadeIn(float sec, string color)
		{
		}

		// Token: 0x06003822 RID: 14370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600330F")]
		[Address(RVA = "0x1011AAA04", Offset = "0x11AAA04", VA = "0x1011AAA04")]
		public void Effect(string effectId, float x, float y, float rot, float scale = 1f)
		{
		}

		// Token: 0x06003823 RID: 14371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003310")]
		[Address(RVA = "0x1011AAB28", Offset = "0x11AAB28", VA = "0x1011AAB28")]
		public void EffectLoop(string effectId, float x, float y, float rot, float scale = 1f)
		{
		}

		// Token: 0x06003824 RID: 14372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003311")]
		[Address(RVA = "0x1011AABA0", Offset = "0x11AABA0", VA = "0x1011AABA0")]
		public void EffectScreen(string effectId, float x, float y, float rot, float scale = 1f)
		{
		}

		// Token: 0x06003825 RID: 14373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003312")]
		[Address(RVA = "0x1011AAC18", Offset = "0x11AAC18", VA = "0x1011AAC18")]
		public void EffectScreenLoop(string effectId, float x, float y, float rot, float scale = 1f)
		{
		}

		// Token: 0x06003826 RID: 14374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003313")]
		[Address(RVA = "0x1011AAC90", Offset = "0x11AAC90", VA = "0x1011AAC90")]
		public void StopEffect(string effectId)
		{
		}

		// Token: 0x06003827 RID: 14375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003314")]
		[Address(RVA = "0x1011AACF8", Offset = "0x11AACF8", VA = "0x1011AACF8")]
		public void WaitTap()
		{
		}

		// Token: 0x06003828 RID: 14376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003315")]
		[Address(RVA = "0x1011AAD88", Offset = "0x11AAD88", VA = "0x1011AAD88")]
		public void Wait(float sec)
		{
		}

		// Token: 0x06003829 RID: 14377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003316")]
		[Address(RVA = "0x1011AADC8", Offset = "0x11AADC8", VA = "0x1011AADC8")]
		public void WaitAction()
		{
		}

		// Token: 0x0600382A RID: 14378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003317")]
		[Address(RVA = "0x1011AADFC", Offset = "0x11AADFC", VA = "0x1011AADFC")]
		public void WaitFade()
		{
		}

		// Token: 0x0600382B RID: 14379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003318")]
		[Address(RVA = "0x1011AAE30", Offset = "0x11AAE30", VA = "0x1011AAE30")]
		public void WaitEffect()
		{
		}

		// Token: 0x0600382C RID: 14380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003319")]
		[Address(RVA = "0x1011AAE64", Offset = "0x11AAE64", VA = "0x1011AAE64")]
		public void WaitSE()
		{
		}

		// Token: 0x0600382D RID: 14381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331A")]
		[Address(RVA = "0x1011AAE98", Offset = "0x11AAE98", VA = "0x1011AAE98")]
		public void WaitVoice()
		{
		}

		// Token: 0x0600382E RID: 14382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331B")]
		[Address(RVA = "0x1011AAECC", Offset = "0x11AAECC", VA = "0x1011AAECC")]
		public void PlayBGM(string cueName, float inSec, float outSec)
		{
		}

		// Token: 0x0600382F RID: 14383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331C")]
		[Address(RVA = "0x1011AAF80", Offset = "0x11AAF80", VA = "0x1011AAF80")]
		public void StopBGM()
		{
		}

		// Token: 0x06003830 RID: 14384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331D")]
		[Address(RVA = "0x1011AAFDC", Offset = "0x11AAFDC", VA = "0x1011AAFDC")]
		public void PlaySE(string cueName)
		{
		}

		// Token: 0x06003831 RID: 14385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331E")]
		[Address(RVA = "0x1011AB024", Offset = "0x11AB024", VA = "0x1011AB024")]
		public void StopSE()
		{
		}

		// Token: 0x06003832 RID: 14386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600331F")]
		[Address(RVA = "0x1011AB064", Offset = "0x11AB064", VA = "0x1011AB064")]
		public void PlayVoice(string cueSheet, string cueName)
		{
		}

		// Token: 0x06003833 RID: 14387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003320")]
		[Address(RVA = "0x1011AB0BC", Offset = "0x11AB0BC", VA = "0x1011AB0BC")]
		public void StopVoice()
		{
		}

		// Token: 0x06003834 RID: 14388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003321")]
		[Address(RVA = "0x1011AB0FC", Offset = "0x11AB0FC", VA = "0x1011AB0FC")]
		public ComicCommandParser()
		{
		}

		// Token: 0x0400477D RID: 18301
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003214")]
		private ComicPlayer m_Player;
	}
}
