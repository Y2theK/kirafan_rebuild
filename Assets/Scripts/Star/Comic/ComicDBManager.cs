﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.Comic
{
	// Token: 0x02000C38 RID: 3128
	[Token(Token = "0x200085E")]
	[StructLayout(3)]
	public class ComicDBManager
	{
		// Token: 0x06003835 RID: 14389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003322")]
		[Address(RVA = "0x1011AB104", Offset = "0x11AB104", VA = "0x1011AB104")]
		public void Prepare(ABResourceLoader loader, string groupName, string comicName)
		{
		}

		// Token: 0x06003836 RID: 14390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003323")]
		[Address(RVA = "0x1011AB264", Offset = "0x11AB264", VA = "0x1011AB264")]
		public void UpdatePrepare()
		{
		}

		// Token: 0x06003837 RID: 14391 RVA: 0x00017A78 File Offset: 0x00015C78
		[Token(Token = "0x6003324")]
		[Address(RVA = "0x1011AB64C", Offset = "0x11AB64C", VA = "0x1011AB64C")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06003838 RID: 14392 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003325")]
		[Address(RVA = "0x1011A9AEC", Offset = "0x11A9AEC", VA = "0x1011A9AEC")]
		public ComicAction GetComicAction(string actionName)
		{
			return null;
		}

		// Token: 0x06003839 RID: 14393 RVA: 0x00017A90 File Offset: 0x00015C90
		[Token(Token = "0x6003326")]
		[Address(RVA = "0x1011AB654", Offset = "0x11AB654", VA = "0x1011AB654")]
		public int GetSpriteDataIdx(string refName)
		{
			return 0;
		}

		// Token: 0x0600383A RID: 14394 RVA: 0x00017AA8 File Offset: 0x00015CA8
		[Token(Token = "0x6003327")]
		[Address(RVA = "0x1011AB6FC", Offset = "0x11AB6FC", VA = "0x1011AB6FC")]
		public int GetSpriteDefaultPriority(string refName)
		{
			return 0;
		}

		// Token: 0x0600383B RID: 14395 RVA: 0x00017AC0 File Offset: 0x00015CC0
		[Token(Token = "0x6003328")]
		[Address(RVA = "0x1011AB700", Offset = "0x11AB700", VA = "0x1011AB700")]
		public ComicSpriteListDB_Data GetSpriteDBData(string refName)
		{
			return default(ComicSpriteListDB_Data);
		}

		// Token: 0x0600383C RID: 14396 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003329")]
		[Address(RVA = "0x1011AB780", Offset = "0x11AB780", VA = "0x1011AB780")]
		public List<ComicCommand> GetScriptCommandList()
		{
			return null;
		}

		// Token: 0x0600383D RID: 14397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600332A")]
		[Address(RVA = "0x1011AB9DC", Offset = "0x11AB9DC", VA = "0x1011AB9DC")]
		public ComicDBManager()
		{
		}

		// Token: 0x0400477E RID: 18302
		[Token(Token = "0x4003215")]
		private const string ACTIONDB_PATH = "comic/comicactionlist.muast";

		// Token: 0x0400477F RID: 18303
		[Token(Token = "0x4003216")]
		private const string SCRIPT_PATH_BASE = "comic/script/comicscript_";

		// Token: 0x04004780 RID: 18304
		[Token(Token = "0x4003217")]
		private const string SPRITELIST_PATH_BASE = "comic/spritelist/comicspritelist_";

		// Token: 0x04004781 RID: 18305
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003218")]
		private ABResourceLoader m_Loader;

		// Token: 0x04004782 RID: 18306
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003219")]
		private ABResourceObjectHandler m_ActionDBResObjHandler;

		// Token: 0x04004783 RID: 18307
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400321A")]
		private ComicActionListDB m_ActionDB;

		// Token: 0x04004784 RID: 18308
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400321B")]
		private ABResourceObjectHandler m_SpriteListDBResObjHandler;

		// Token: 0x04004785 RID: 18309
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400321C")]
		private ComicSpriteListDB m_SpriteListDB;

		// Token: 0x04004786 RID: 18310
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400321D")]
		private ABResourceObjectHandler m_ScriptResObjHandler;

		// Token: 0x04004787 RID: 18311
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400321E")]
		private ComicScriptDB m_Script;

		// Token: 0x04004788 RID: 18312
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400321F")]
		private ComicSpriteListDB_Param m_SpriteListParam;

		// Token: 0x04004789 RID: 18313
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003220")]
		private string m_ComicName;

		// Token: 0x0400478A RID: 18314
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003221")]
		private bool m_IsDonePrepare;
	}
}
