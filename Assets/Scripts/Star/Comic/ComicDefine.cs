﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.Comic
{
	// Token: 0x02000C39 RID: 3129
	[Token(Token = "0x200085F")]
	[StructLayout(3)]
	public class ComicDefine
	{
		// Token: 0x0600383E RID: 14398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600332B")]
		[Address(RVA = "0x1011AB9E4", Offset = "0x11AB9E4", VA = "0x1011AB9E4")]
		public ComicDefine()
		{
		}

		// Token: 0x0400478B RID: 18315
		[Token(Token = "0x4003222")]
		public static string COMIC_RESOURCE_PATH_BASE;

		// Token: 0x0400478C RID: 18316
		[Token(Token = "0x4003223")]
		public static string INVALID_ACTION_NAME;

		// Token: 0x0400478D RID: 18317
		[Token(Token = "0x4003224")]
		public static string COMMAND_CELLCHANGE;

		// Token: 0x0400478E RID: 18318
		[Token(Token = "0x4003225")]
		public static string COMMAND_SPRITE;

		// Token: 0x0400478F RID: 18319
		[Token(Token = "0x4003226")]
		public static string COMMAND_WAITTAP;

		// Token: 0x04004790 RID: 18320
		[Token(Token = "0x4003227")]
		public static readonly string[] CURVE_NAME;

		// Token: 0x04004791 RID: 18321
		[Token(Token = "0x4003228")]
		public static readonly string[] COMMAND_EFFECTS;

		// Token: 0x04004792 RID: 18322
		[Token(Token = "0x4003229")]
		public static string COMMAND_VOICE;

		// Token: 0x04004793 RID: 18323
		[Token(Token = "0x400322A")]
		public static string COMMAND_SE;
	}
}
