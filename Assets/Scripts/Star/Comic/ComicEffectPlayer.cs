﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.Comic
{
	// Token: 0x02000C3A RID: 3130
	[Token(Token = "0x2000860")]
	[StructLayout(3)]
	public class ComicEffectPlayer
	{
		// Token: 0x06003840 RID: 14400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600332D")]
		[Address(RVA = "0x1011ABDF0", Offset = "0x11ABDF0", VA = "0x1011ABDF0")]
		public void Destroy()
		{
		}

		// Token: 0x06003841 RID: 14401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600332E")]
		[Address(RVA = "0x1011ABF18", Offset = "0x11ABF18", VA = "0x1011ABF18")]
		public void Update()
		{
		}

		// Token: 0x06003842 RID: 14402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600332F")]
		[Address(RVA = "0x1011AC084", Offset = "0x11AC084", VA = "0x1011AC084")]
		public void Play(string effectId, Transform parent, Vector2 pos, float rot, float scale, bool adjustScreenEffect, bool isLoop)
		{
		}

		// Token: 0x06003843 RID: 14403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003330")]
		[Address(RVA = "0x1011AC3AC", Offset = "0x11AC3AC", VA = "0x1011AC3AC")]
		public void StopEffect(string effectId)
		{
		}

		// Token: 0x06003844 RID: 14404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003331")]
		[Address(RVA = "0x1011ABDF4", Offset = "0x11ABDF4", VA = "0x1011ABDF4")]
		public void StopAllEffect()
		{
		}

		// Token: 0x06003845 RID: 14405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003332")]
		[Address(RVA = "0x1011AC52C", Offset = "0x11AC52C", VA = "0x1011AC52C")]
		public void SkipShotEffect()
		{
		}

		// Token: 0x06003846 RID: 14406 RVA: 0x00017AD8 File Offset: 0x00015CD8
		[Token(Token = "0x6003333")]
		[Address(RVA = "0x1011AC6A4", Offset = "0x11AC6A4", VA = "0x1011AC6A4")]
		public bool IsPlayingShotEffect()
		{
			return default(bool);
		}

		// Token: 0x06003847 RID: 14407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003334")]
		[Address(RVA = "0x1011AC7C4", Offset = "0x11AC7C4", VA = "0x1011AC7C4")]
		public ComicEffectPlayer()
		{
		}

		// Token: 0x04004794 RID: 18324
		[Token(Token = "0x400322B")]
		private const float SCREENEFFECT_SIZE = 1334f;

		// Token: 0x04004795 RID: 18325
		[Token(Token = "0x400322C")]
		private const float CAMERA_ORTHOGRAPHIC_SIZE = 0.5f;

		// Token: 0x04004796 RID: 18326
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400322D")]
		private List<EffectHandler> m_EffectHandlerList;
	}
}
