﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.Comic
{
	// Token: 0x02000C3B RID: 3131
	[Token(Token = "0x2000861")]
	[StructLayout(3)]
	public class ComicPlayer : MonoBehaviour
	{
		// Token: 0x06003848 RID: 14408 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003335")]
		[Address(RVA = "0x1011AAF70", Offset = "0x11AAF70", VA = "0x1011AAF70")]
		public ComicSoundPlayer GetComicSoundPlayer()
		{
			return null;
		}

		// Token: 0x06003849 RID: 14409 RVA: 0x00017AF0 File Offset: 0x00015CF0
		[Token(Token = "0x6003336")]
		[Address(RVA = "0x1011AAF78", Offset = "0x11AAF78", VA = "0x1011AAF78")]
		public int GetCurrentCommandIdx()
		{
			return 0;
		}

		// Token: 0x0600384A RID: 14410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003337")]
		[Address(RVA = "0x1011AC834", Offset = "0x11AC834", VA = "0x1011AC834")]
		public void Play(int advId)
		{
		}

		// Token: 0x0600384B RID: 14411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003338")]
		[Address(RVA = "0x1011AC8E8", Offset = "0x11AC8E8", VA = "0x1011AC8E8")]
		public void Play(string groupName, string comicName)
		{
		}

		// Token: 0x0600384C RID: 14412 RVA: 0x00017B08 File Offset: 0x00015D08
		[Token(Token = "0x6003339")]
		[Address(RVA = "0x1011ACCE0", Offset = "0x11ACCE0", VA = "0x1011ACCE0")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600384D RID: 14413 RVA: 0x00017B20 File Offset: 0x00015D20
		[Token(Token = "0x600333A")]
		[Address(RVA = "0x1011ACCF0", Offset = "0x11ACCF0", VA = "0x1011ACCF0")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x0600384E RID: 14414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600333B")]
		[Address(RVA = "0x1011ACD00", Offset = "0x11ACD00", VA = "0x1011ACD00")]
		private void Update()
		{
		}

		// Token: 0x0600384F RID: 14415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600333C")]
		[Address(RVA = "0x1011AC984", Offset = "0x11AC984", VA = "0x1011AC984")]
		private void Prepare(string groupName, string comicName)
		{
		}

		// Token: 0x06003850 RID: 14416 RVA: 0x00017B38 File Offset: 0x00015D38
		[Token(Token = "0x600333D")]
		[Address(RVA = "0x1011AD3C8", Offset = "0x11AD3C8", VA = "0x1011AD3C8")]
		private bool IsDonePrepareDB()
		{
			return default(bool);
		}

		// Token: 0x06003851 RID: 14417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600333E")]
		[Address(RVA = "0x1011AD3F4", Offset = "0x11AD3F4", VA = "0x1011AD3F4")]
		private void PrepareResource()
		{
		}

		// Token: 0x06003852 RID: 14418 RVA: 0x00017B50 File Offset: 0x00015D50
		[Token(Token = "0x600333F")]
		[Address(RVA = "0x1011AD8B0", Offset = "0x11AD8B0", VA = "0x1011AD8B0")]
		private bool IsDonePrepareResource()
		{
			return default(bool);
		}

		// Token: 0x06003853 RID: 14419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003340")]
		[Address(RVA = "0x1011ADC1C", Offset = "0x11ADC1C", VA = "0x1011ADC1C")]
		private void Destroy()
		{
		}

		// Token: 0x06003854 RID: 14420 RVA: 0x00017B68 File Offset: 0x00015D68
		[Token(Token = "0x6003341")]
		[Address(RVA = "0x1011AE480", Offset = "0x11AE480", VA = "0x1011AE480")]
		public bool IsDoneDestroy()
		{
			return default(bool);
		}

		// Token: 0x06003855 RID: 14421 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003342")]
		[Address(RVA = "0x1011A9AE4", Offset = "0x11A9AE4", VA = "0x1011A9AE4")]
		public ComicDBManager GetDBMng()
		{
			return null;
		}

		// Token: 0x06003856 RID: 14422 RVA: 0x00017B80 File Offset: 0x00015D80
		[Token(Token = "0x6003343")]
		[Address(RVA = "0x1011AE27C", Offset = "0x11AE27C", VA = "0x1011AE27C")]
		private bool IsWaiting(ComicPlayer.eWaitFlagType type)
		{
			return default(bool);
		}

		// Token: 0x06003857 RID: 14423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003344")]
		[Address(RVA = "0x1011AE714", Offset = "0x11AE714", VA = "0x1011AE714")]
		private void ClearWait()
		{
		}

		// Token: 0x06003858 RID: 14424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003345")]
		[Address(RVA = "0x1011AE13C", Offset = "0x11AE13C", VA = "0x1011AE13C")]
		private void RefreshWaitFlags()
		{
		}

		// Token: 0x06003859 RID: 14425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003346")]
		[Address(RVA = "0x1011AAD2C", Offset = "0x11AAD2C", VA = "0x1011AAD2C")]
		public void SetWaitFlag(ComicPlayer.eWaitFlagType type, bool flg)
		{
		}

		// Token: 0x0600385A RID: 14426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003347")]
		[Address(RVA = "0x1011AADC0", Offset = "0x11AADC0", VA = "0x1011AADC0")]
		public void SetWaitSec(float sec)
		{
		}

		// Token: 0x0600385B RID: 14427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003348")]
		[Address(RVA = "0x1011AD970", Offset = "0x11AD970", VA = "0x1011AD970")]
		private void StartCommand()
		{
		}

		// Token: 0x0600385C RID: 14428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003349")]
		[Address(RVA = "0x1011ADCF4", Offset = "0x11ADCF4", VA = "0x1011ADCF4")]
		private void ExecCommand(int idx)
		{
		}

		// Token: 0x0600385D RID: 14429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600334A")]
		[Address(RVA = "0x1011A93E4", Offset = "0x11A93E4", VA = "0x1011A93E4")]
		public void ChangeBG(ComicCommand command)
		{
		}

		// Token: 0x0600385E RID: 14430 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600334B")]
		[Address(RVA = "0x1011A9ADC", Offset = "0x11A9ADC", VA = "0x1011A9ADC")]
		public ComicCell GetCell()
		{
			return null;
		}

		// Token: 0x0600385F RID: 14431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600334C")]
		[Address(RVA = "0x1011AE940", Offset = "0x11AE940", VA = "0x1011AE940")]
		private void SkipAny()
		{
		}

		// Token: 0x06003860 RID: 14432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600334D")]
		[Address(RVA = "0x1011AE97C", Offset = "0x11AE97C", VA = "0x1011AE97C")]
		private void SkipAction()
		{
		}

		// Token: 0x06003861 RID: 14433 RVA: 0x00017B98 File Offset: 0x00015D98
		[Token(Token = "0x600334E")]
		[Address(RVA = "0x1011AE798", Offset = "0x11AE798", VA = "0x1011AE798")]
		private bool IsPlayingAction()
		{
			return default(bool);
		}

		// Token: 0x06003862 RID: 14434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600334F")]
		[Address(RVA = "0x1011AEA5C", Offset = "0x11AEA5C", VA = "0x1011AEA5C")]
		private void SkipFade()
		{
		}

		// Token: 0x06003863 RID: 14435 RVA: 0x00017BB0 File Offset: 0x00015DB0
		[Token(Token = "0x6003350")]
		[Address(RVA = "0x1011AE8B4", Offset = "0x11AE8B4", VA = "0x1011AE8B4")]
		private bool IsPlayingFade()
		{
			return default(bool);
		}

		// Token: 0x06003864 RID: 14436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003351")]
		[Address(RVA = "0x1011AEA68", Offset = "0x11AEA68", VA = "0x1011AEA68")]
		private void SkipEffect()
		{
		}

		// Token: 0x06003865 RID: 14437 RVA: 0x00017BC8 File Offset: 0x00015DC8
		[Token(Token = "0x6003352")]
		[Address(RVA = "0x1011AE8C4", Offset = "0x11AE8C4", VA = "0x1011AE8C4")]
		private bool IsPlayingEffect()
		{
			return default(bool);
		}

		// Token: 0x06003866 RID: 14438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003353")]
		[Address(RVA = "0x1011AEA94", Offset = "0x11AEA94", VA = "0x1011AEA94")]
		private void SkipSE()
		{
		}

		// Token: 0x06003867 RID: 14439 RVA: 0x00017BE0 File Offset: 0x00015DE0
		[Token(Token = "0x6003354")]
		[Address(RVA = "0x1011AE2CC", Offset = "0x11AE2CC", VA = "0x1011AE2CC")]
		private bool IsPlayingSE()
		{
			return default(bool);
		}

		// Token: 0x06003868 RID: 14440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003355")]
		[Address(RVA = "0x1011AEAC4", Offset = "0x11AEAC4", VA = "0x1011AEAC4")]
		private void SkipVoice()
		{
		}

		// Token: 0x06003869 RID: 14441 RVA: 0x00017BF8 File Offset: 0x00015DF8
		[Token(Token = "0x6003356")]
		[Address(RVA = "0x1011AE2FC", Offset = "0x11AE2FC", VA = "0x1011AE2FC")]
		private bool IsPlayingVoice()
		{
			return default(bool);
		}

		// Token: 0x0600386A RID: 14442 RVA: 0x00017C10 File Offset: 0x00015E10
		[Token(Token = "0x6003357")]
		[Address(RVA = "0x1011AE238", Offset = "0x11AE238", VA = "0x1011AE238")]
		private bool IsPlayingAnyWithoutSound()
		{
			return default(bool);
		}

		// Token: 0x0600386B RID: 14443 RVA: 0x00017C28 File Offset: 0x00015E28
		[Token(Token = "0x6003358")]
		[Address(RVA = "0x1011ADB78", Offset = "0x11ADB78", VA = "0x1011ADB78")]
		private bool IsWaitingCommand()
		{
			return default(bool);
		}

		// Token: 0x0600386C RID: 14444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003359")]
		[Address(RVA = "0x1011AE4A8", Offset = "0x11AE4A8", VA = "0x1011AE4A8")]
		private void ClearCell()
		{
		}

		// Token: 0x0600386D RID: 14445 RVA: 0x00017C40 File Offset: 0x00015E40
		[Token(Token = "0x600335A")]
		[Address(RVA = "0x1011AEAF4", Offset = "0x11AEAF4", VA = "0x1011AEAF4")]
		private int GetCurrentCellIdx()
		{
			return 0;
		}

		// Token: 0x0600386E RID: 14446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600335B")]
		[Address(RVA = "0x1011AE8F0", Offset = "0x11AE8F0", VA = "0x1011AE8F0")]
		private void UpdateCurrentCellText()
		{
		}

		// Token: 0x0600386F RID: 14447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600335C")]
		[Address(RVA = "0x1011ADD94", Offset = "0x11ADD94", VA = "0x1011ADD94")]
		private void PlayInNextCell()
		{
		}

		// Token: 0x06003870 RID: 14448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600335D")]
		[Address(RVA = "0x1011AEC0C", Offset = "0x11AEC0C", VA = "0x1011AEC0C")]
		private void RefreshSpritePriority()
		{
		}

		// Token: 0x06003871 RID: 14449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600335E")]
		[Address(RVA = "0x1011A94E0", Offset = "0x11A94E0", VA = "0x1011A94E0")]
		public void PlaySprite(string refName, string action, float fadeSec)
		{
		}

		// Token: 0x06003872 RID: 14450 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600335F")]
		[Address(RVA = "0x1011A9E58", Offset = "0x11A9E58", VA = "0x1011A9E58")]
		public ComicSprite GetActiveSprite(string refName)
		{
			return null;
		}

		// Token: 0x06003873 RID: 14451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003360")]
		[Address(RVA = "0x1011AA764", Offset = "0x11AA764", VA = "0x1011AA764")]
		public void SetSpritePriorityTop(string spriteName)
		{
		}

		// Token: 0x06003874 RID: 14452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003361")]
		[Address(RVA = "0x1011AA8C8", Offset = "0x11AA8C8", VA = "0x1011AA8C8")]
		public void SetFade(Color startColor, Color endColor, float sec)
		{
		}

		// Token: 0x06003875 RID: 14453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003362")]
		[Address(RVA = "0x1011AAA7C", Offset = "0x11AAA7C", VA = "0x1011AAA7C")]
		public void PlayEffect(string effectId, bool isScreenEffect, Vector2 pos, float rot, float scale, bool adjustScreenEffect, bool isLoop)
		{
		}

		// Token: 0x06003876 RID: 14454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003363")]
		[Address(RVA = "0x1011AACC4", Offset = "0x11AACC4", VA = "0x1011AACC4")]
		public void StopEffect(string effectId)
		{
		}

		// Token: 0x06003877 RID: 14455 RVA: 0x00017C58 File Offset: 0x00015E58
		[Token(Token = "0x6003364")]
		[Address(RVA = "0x1011AA3F0", Offset = "0x11AA3F0", VA = "0x1011AA3F0")]
		public eComicActionCurveType CurveNameStringToEnum(string curveType)
		{
			return eComicActionCurveType.None;
		}

		// Token: 0x06003878 RID: 14456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003365")]
		[Address(RVA = "0x1011AE32C", Offset = "0x11AE32C", VA = "0x1011AE32C")]
		private void TapNext()
		{
		}

		// Token: 0x06003879 RID: 14457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003366")]
		[Address(RVA = "0x1011AEFE4", Offset = "0x11AEFE4", VA = "0x1011AEFE4")]
		private void ForceJumpScript(int idx)
		{
		}

		// Token: 0x0600387A RID: 14458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003367")]
		[Address(RVA = "0x1011AF128", Offset = "0x11AF128", VA = "0x1011AF128")]
		public void NextCell()
		{
		}

		// Token: 0x0600387B RID: 14459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003368")]
		[Address(RVA = "0x1011AF3D4", Offset = "0x11AF3D4", VA = "0x1011AF3D4")]
		public void PrevCell()
		{
		}

		// Token: 0x0600387C RID: 14460 RVA: 0x00017C70 File Offset: 0x00015E70
		[Token(Token = "0x6003369")]
		[Address(RVA = "0x1011AF1C0", Offset = "0x11AF1C0", VA = "0x1011AF1C0")]
		private int GetNextBGIdx()
		{
			return 0;
		}

		// Token: 0x0600387D RID: 14461 RVA: 0x00017C88 File Offset: 0x00015E88
		[Token(Token = "0x600336A")]
		[Address(RVA = "0x1011AF2C4", Offset = "0x11AF2C4", VA = "0x1011AF2C4")]
		private int GetLastWaitTapIdx()
		{
			return 0;
		}

		// Token: 0x0600387E RID: 14462 RVA: 0x00017CA0 File Offset: 0x00015EA0
		[Token(Token = "0x600336B")]
		[Address(RVA = "0x1011AF438", Offset = "0x11AF438", VA = "0x1011AF438")]
		private int GetPrevBGIdx()
		{
			return 0;
		}

		// Token: 0x0600387F RID: 14463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600336C")]
		[Address(RVA = "0x1011AE6D8", Offset = "0x11AE6D8", VA = "0x1011AE6D8")]
		public void SetAuto(bool flg)
		{
		}

		// Token: 0x06003880 RID: 14464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600336D")]
		[Address(RVA = "0x1011AF540", Offset = "0x11AF540", VA = "0x1011AF540")]
		public void Skip()
		{
		}

		// Token: 0x06003881 RID: 14465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600336E")]
		[Address(RVA = "0x1011AF554", Offset = "0x11AF554", VA = "0x1011AF554")]
		public void OnClickTapButtonCallBack()
		{
		}

		// Token: 0x06003882 RID: 14466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600336F")]
		[Address(RVA = "0x1011AF578", Offset = "0x11AF578", VA = "0x1011AF578")]
		public ComicPlayer()
		{
		}

		// Token: 0x04004797 RID: 18327
		[Token(Token = "0x400322E")]
		private const int DEFAULT_SPRITE_INST_NUM = 16;

		// Token: 0x04004798 RID: 18328
		[Token(Token = "0x400322F")]
		private const float AUTO_WAIT_SEC = 1.5f;

		// Token: 0x04004799 RID: 18329
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003230")]
		private int m_AdvId;

		// Token: 0x0400479A RID: 18330
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003231")]
		private ComicPlayer.eState m_State;

		// Token: 0x0400479B RID: 18331
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003232")]
		private string m_ComicName;

		// Token: 0x0400479C RID: 18332
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003233")]
		private ComicSizeCheck m_SizeCheck;

		// Token: 0x0400479D RID: 18333
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003234")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400479E RID: 18334
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003235")]
		private ABResourceObjectHandler m_ABHandler;

		// Token: 0x0400479F RID: 18335
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003236")]
		private ComicDBManager m_DBMng;

		// Token: 0x040047A0 RID: 18336
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003237")]
		private ComicEffectPlayer m_EffectPlayer;

		// Token: 0x040047A1 RID: 18337
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003238")]
		private ComicSoundPlayer m_SoundPlayer;

		// Token: 0x040047A2 RID: 18338
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003239")]
		[SerializeField]
		private ComicCell m_Cell;

		// Token: 0x040047A3 RID: 18339
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400323A")]
		[SerializeField]
		private CanvasGroup m_CellFade;

		// Token: 0x040047A4 RID: 18340
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400323B")]
		[SerializeField]
		private ComicSprite m_SpriteOrig;

		// Token: 0x040047A5 RID: 18341
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400323C")]
		[SerializeField]
		private Image m_FadeImage;

		// Token: 0x040047A6 RID: 18342
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400323D")]
		[SerializeField]
		private Transform m_ScreenEffectParent;

		// Token: 0x040047A7 RID: 18343
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400323E")]
		[SerializeField]
		private ComicUI m_UI;

		// Token: 0x040047A8 RID: 18344
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400323F")]
		private List<ComicSprite> m_ActiveSpriteList;

		// Token: 0x040047A9 RID: 18345
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003240")]
		private List<ComicSprite> m_InactiveSpriteList;

		// Token: 0x040047AA RID: 18346
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003241")]
		private List<string> m_PriorityChangeList;

		// Token: 0x040047AB RID: 18347
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003242")]
		private ComicCommandParser m_CommandParser;

		// Token: 0x040047AC RID: 18348
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003243")]
		private List<ComicCommand> m_CommandList;

		// Token: 0x040047AD RID: 18349
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003244")]
		private int m_CurrentCommandIdx;

		// Token: 0x040047AE RID: 18350
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4003245")]
		private int m_CellMaxNum;

		// Token: 0x040047AF RID: 18351
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003246")]
		private ComicCommand m_NextCellCommand;

		// Token: 0x040047B0 RID: 18352
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003247")]
		private float m_WaitSec;

		// Token: 0x040047B1 RID: 18353
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003248")]
		private bool[] m_WaitFlags;

		// Token: 0x040047B2 RID: 18354
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003249")]
		private bool m_IsAuto;

		// Token: 0x040047B3 RID: 18355
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x400324A")]
		private float m_AutoWaitCount;

		// Token: 0x040047B4 RID: 18356
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400324B")]
		private Color m_FadeStartColor;

		// Token: 0x040047B5 RID: 18357
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400324C")]
		private Color m_FadeEndColor;

		// Token: 0x040047B6 RID: 18358
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400324D")]
		private float m_FadeSec;

		// Token: 0x040047B7 RID: 18359
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x400324E")]
		private float m_FadeSecCount;

		// Token: 0x02000C3C RID: 3132
		[Token(Token = "0x20010A2")]
		private enum eState
		{
			// Token: 0x040047B9 RID: 18361
			[Token(Token = "0x40068BC")]
			None,
			// Token: 0x040047BA RID: 18362
			[Token(Token = "0x40068BD")]
			Prepare,
			// Token: 0x040047BB RID: 18363
			[Token(Token = "0x40068BE")]
			SizeCheck,
			// Token: 0x040047BC RID: 18364
			[Token(Token = "0x40068BF")]
			PrepareResource,
			// Token: 0x040047BD RID: 18365
			[Token(Token = "0x40068C0")]
			Start,
			// Token: 0x040047BE RID: 18366
			[Token(Token = "0x40068C1")]
			Idle,
			// Token: 0x040047BF RID: 18367
			[Token(Token = "0x40068C2")]
			Destroy,
			// Token: 0x040047C0 RID: 18368
			[Token(Token = "0x40068C3")]
			RequestOccurred,
			// Token: 0x040047C1 RID: 18369
			[Token(Token = "0x40068C4")]
			RequestOccurredWait
		}

		// Token: 0x02000C3D RID: 3133
		[Token(Token = "0x20010A3")]
		public enum eWaitFlagType
		{
			// Token: 0x040047C3 RID: 18371
			[Token(Token = "0x40068C6")]
			Tap,
			// Token: 0x040047C4 RID: 18372
			[Token(Token = "0x40068C7")]
			Action,
			// Token: 0x040047C5 RID: 18373
			[Token(Token = "0x40068C8")]
			Fade,
			// Token: 0x040047C6 RID: 18374
			[Token(Token = "0x40068C9")]
			Effect,
			// Token: 0x040047C7 RID: 18375
			[Token(Token = "0x40068CA")]
			SE,
			// Token: 0x040047C8 RID: 18376
			[Token(Token = "0x40068CB")]
			Voice,
			// Token: 0x040047C9 RID: 18377
			[Token(Token = "0x40068CC")]
			Num
		}
	}
}
