﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star.Comic
{
	// Token: 0x02000C3F RID: 3135
	[Token(Token = "0x2000862")]
	[StructLayout(3)]
	public class ComicSizeCheck
	{
		// Token: 0x06003887 RID: 14471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003371")]
		[Address(RVA = "0x1011AF7BC", Offset = "0x11AF7BC", VA = "0x1011AF7BC")]
		public void Check(string comicName, List<ComicCommand> commandList)
		{
		}

		// Token: 0x06003888 RID: 14472 RVA: 0x00017CD0 File Offset: 0x00015ED0
		[Token(Token = "0x6003372")]
		[Address(RVA = "0x1011AFB6C", Offset = "0x11AFB6C", VA = "0x1011AFB6C")]
		public bool IsChecking()
		{
			return default(bool);
		}

		// Token: 0x06003889 RID: 14473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003373")]
		[Address(RVA = "0x1011AFB7C", Offset = "0x11AFB7C", VA = "0x1011AFB7C")]
		public void Update()
		{
		}

		// Token: 0x0600388A RID: 14474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003374")]
		[Address(RVA = "0x1011AFBC8", Offset = "0x11AFBC8", VA = "0x1011AFBC8")]
		private void Confirm()
		{
		}

		// Token: 0x0600388B RID: 14475 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003375")]
		[Address(RVA = "0x1011AF944", Offset = "0x11AF944", VA = "0x1011AF944")]
		private List<string> GetUseResourcePath(List<ComicCommand> commandList)
		{
			return null;
		}

		// Token: 0x0600388C RID: 14476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003376")]
		[Address(RVA = "0x1011B00B4", Offset = "0x11B00B4", VA = "0x1011B00B4")]
		public ComicSizeCheck()
		{
		}

		// Token: 0x040047CC RID: 18380
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400324F")]
		private MeigeResource.DLSizeHandle m_ABDLSizeHndl;

		// Token: 0x040047CD RID: 18381
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003250")]
		private ComicSizeCheck.eState m_State;

		// Token: 0x02000C40 RID: 3136
		[Token(Token = "0x20010A5")]
		private enum eState
		{
			// Token: 0x040047CF RID: 18383
			[Token(Token = "0x40068D0")]
			None,
			// Token: 0x040047D0 RID: 18384
			[Token(Token = "0x40068D1")]
			Check,
			// Token: 0x040047D1 RID: 18385
			[Token(Token = "0x40068D2")]
			Confirm
		}
	}
}
