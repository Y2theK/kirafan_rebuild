﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.Comic
{
	// Token: 0x02000C41 RID: 3137
	[Token(Token = "0x2000863")]
	[StructLayout(3)]
	public class ComicSoundPlayer
	{
		// Token: 0x0600388E RID: 14478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003378")]
		[Address(RVA = "0x1011B00C4", Offset = "0x11B00C4", VA = "0x1011B00C4")]
		public void Prepare()
		{
		}

		// Token: 0x0600388F RID: 14479 RVA: 0x00017CE8 File Offset: 0x00015EE8
		[Token(Token = "0x6003379")]
		[Address(RVA = "0x1011B0258", Offset = "0x11B0258", VA = "0x1011B0258")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06003890 RID: 14480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337A")]
		[Address(RVA = "0x1011B0448", Offset = "0x11B0448", VA = "0x1011B0448")]
		public void Destroy()
		{
		}

		// Token: 0x06003891 RID: 14481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337B")]
		[Address(RVA = "0x1011B0AC4", Offset = "0x11B0AC4", VA = "0x1011B0AC4")]
		public void Update()
		{
		}

		// Token: 0x06003892 RID: 14482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337C")]
		[Address(RVA = "0x1011B082C", Offset = "0x11B082C", VA = "0x1011B082C")]
		private void RemoveFinishedSoundHandler(bool forceStop)
		{
		}

		// Token: 0x06003893 RID: 14483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337D")]
		[Address(RVA = "0x1011B0C40", Offset = "0x11B0C40", VA = "0x1011B0C40")]
		public void LoadVoiceCueSheet(string cueSheet)
		{
		}

		// Token: 0x06003894 RID: 14484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337E")]
		[Address(RVA = "0x1011B0D30", Offset = "0x11B0D30", VA = "0x1011B0D30")]
		public void LoadSeCueSheet(string cueSheet)
		{
		}

		// Token: 0x06003895 RID: 14485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600337F")]
		[Address(RVA = "0x1011B0E48", Offset = "0x11B0E48", VA = "0x1011B0E48")]
		public void SetEnableSoundPlay(bool flg)
		{
		}

		// Token: 0x06003896 RID: 14486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003380")]
		[Address(RVA = "0x1011B0E50", Offset = "0x11B0E50", VA = "0x1011B0E50")]
		public void PlayBGM(int commandIdx, string cueName, int inMiliSec, int outMiliSec)
		{
		}

		// Token: 0x06003897 RID: 14487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003381")]
		[Address(RVA = "0x1011B0E60", Offset = "0x11B0E60", VA = "0x1011B0E60")]
		public void StopBGM(int commandIdx)
		{
		}

		// Token: 0x06003898 RID: 14488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003382")]
		[Address(RVA = "0x1011B0E6C", Offset = "0x11B0E6C", VA = "0x1011B0E6C")]
		public void PlaySE(string cueName)
		{
		}

		// Token: 0x06003899 RID: 14489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003383")]
		[Address(RVA = "0x1011B068C", Offset = "0x11B068C", VA = "0x1011B068C")]
		public void StopSE()
		{
		}

		// Token: 0x0600389A RID: 14490 RVA: 0x00017D00 File Offset: 0x00015F00
		[Token(Token = "0x6003384")]
		[Address(RVA = "0x1011B108C", Offset = "0x11B108C", VA = "0x1011B108C")]
		public bool IsPlayingSE()
		{
			return default(bool);
		}

		// Token: 0x0600389B RID: 14491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003385")]
		[Address(RVA = "0x1011B10F8", Offset = "0x11B10F8", VA = "0x1011B10F8")]
		public void PlayVoice(string cueSheet, string cueName)
		{
		}

		// Token: 0x0600389C RID: 14492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003386")]
		[Address(RVA = "0x1011B075C", Offset = "0x11B075C", VA = "0x1011B075C")]
		public void StopVoice()
		{
		}

		// Token: 0x0600389D RID: 14493 RVA: 0x00017D18 File Offset: 0x00015F18
		[Token(Token = "0x6003387")]
		[Address(RVA = "0x1011B1264", Offset = "0x11B1264", VA = "0x1011B1264")]
		public bool IsPlayingVoice()
		{
			return default(bool);
		}

		// Token: 0x0600389E RID: 14494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003388")]
		[Address(RVA = "0x1011B12D0", Offset = "0x11B12D0", VA = "0x1011B12D0")]
		public ComicSoundPlayer()
		{
		}

		// Token: 0x040047D2 RID: 18386
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003251")]
		private List<SoundHandler> m_SEList;

		// Token: 0x040047D3 RID: 18387
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003252")]
		private List<SoundHandler> m_VoiceList;

		// Token: 0x040047D4 RID: 18388
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003253")]
		private bool m_EnableSoundPlay;

		// Token: 0x040047D5 RID: 18389
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003254")]
		private int m_CurrentBGMIdx;

		// Token: 0x040047D6 RID: 18390
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003255")]
		private int m_ReserveBGMIdx;

		// Token: 0x040047D7 RID: 18391
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003256")]
		private string m_ReserveBGMCueName;

		// Token: 0x040047D8 RID: 18392
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003257")]
		private int m_ReserveBGMInMiliSec;

		// Token: 0x040047D9 RID: 18393
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003258")]
		private int m_ReserveBGMOutMiliSec;

		// Token: 0x040047DA RID: 18394
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003259")]
		private List<string> m_LoadedVoiceCueSheetList;

		// Token: 0x040047DB RID: 18395
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400325A")]
		private List<string> m_LoadedSeCueSheetList;
	}
}
