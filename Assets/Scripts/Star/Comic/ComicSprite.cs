﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.Comic
{
	// Token: 0x02000C42 RID: 3138
	[Token(Token = "0x2000864")]
	[StructLayout(3)]
	public class ComicSprite : MonoBehaviour
	{
		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600389F RID: 14495 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000422")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6003389")]
			[Address(RVA = "0x1011B13B4", Offset = "0x11B13B4", VA = "0x1011B13B4")]
			get
			{
				return null;
			}
		}

		// Token: 0x060038A0 RID: 14496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600338A")]
		[Address(RVA = "0x1011B144C", Offset = "0x11B144C", VA = "0x1011B144C")]
		public void Clear()
		{
		}

		// Token: 0x060038A1 RID: 14497 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600338B")]
		[Address(RVA = "0x1011B17CC", Offset = "0x11B17CC", VA = "0x1011B17CC")]
		public string GetName()
		{
			return null;
		}

		// Token: 0x060038A2 RID: 14498 RVA: 0x00017D30 File Offset: 0x00015F30
		[Token(Token = "0x600338C")]
		[Address(RVA = "0x1011AF7B4", Offset = "0x11AF7B4", VA = "0x1011AF7B4")]
		public int GetPriority()
		{
			return 0;
		}

		// Token: 0x060038A3 RID: 14499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600338D")]
		[Address(RVA = "0x1011B17D4", Offset = "0x11B17D4", VA = "0x1011B17D4")]
		public void Setup(string refName, Sprite sprite, int priority)
		{
		}

		// Token: 0x060038A4 RID: 14500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600338E")]
		[Address(RVA = "0x1011B18D0", Offset = "0x11B18D0", VA = "0x1011B18D0")]
		public void PlayAction(ComicAction action, float fadeSec)
		{
		}

		// Token: 0x060038A5 RID: 14501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600338F")]
		[Address(RVA = "0x1011B1C64", Offset = "0x11B1C64", VA = "0x1011B1C64")]
		public void SetActionSpeed(float speed)
		{
		}

		// Token: 0x060038A6 RID: 14502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003390")]
		[Address(RVA = "0x1011B1C6C", Offset = "0x11B1C6C", VA = "0x1011B1C6C")]
		private void Update()
		{
		}

		// Token: 0x060038A7 RID: 14503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003391")]
		[Address(RVA = "0x1011B18E4", Offset = "0x11B18E4", VA = "0x1011B18E4")]
		private void ApplyActionAndFade(ComicAction act, float sec)
		{
		}

		// Token: 0x060038A8 RID: 14504 RVA: 0x00017D48 File Offset: 0x00015F48
		[Token(Token = "0x6003392")]
		[Address(RVA = "0x1011B2070", Offset = "0x11B2070", VA = "0x1011B2070")]
		private float ApplyCurve(eComicActionCurveType curve, float rate)
		{
			return 0f;
		}

		// Token: 0x060038A9 RID: 14505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003393")]
		[Address(RVA = "0x1011B1D20", Offset = "0x11B1D20", VA = "0x1011B1D20")]
		private void UpdateLerp()
		{
		}

		// Token: 0x060038AA RID: 14506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003394")]
		[Address(RVA = "0x1011B20D4", Offset = "0x11B20D4", VA = "0x1011B20D4")]
		public void SkipAction()
		{
		}

		// Token: 0x060038AB RID: 14507 RVA: 0x00017D60 File Offset: 0x00015F60
		[Token(Token = "0x6003395")]
		[Address(RVA = "0x1011B2148", Offset = "0x11B2148", VA = "0x1011B2148")]
		public bool IsExecutingAction()
		{
			return default(bool);
		}

		// Token: 0x060038AC RID: 14508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003396")]
		[Address(RVA = "0x1011B21E8", Offset = "0x11B21E8", VA = "0x1011B21E8")]
		public void SetLerpPosition(Vector2 pos, float sec, eComicActionCurveType curveType)
		{
		}

		// Token: 0x060038AD RID: 14509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003397")]
		[Address(RVA = "0x1011B2204", Offset = "0x11B2204", VA = "0x1011B2204")]
		public void SetLerpRotate(float r, float sec, eComicActionCurveType curveType)
		{
		}

		// Token: 0x060038AE RID: 14510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003398")]
		[Address(RVA = "0x1011B2220", Offset = "0x11B2220", VA = "0x1011B2220")]
		public void SetLerpScale(Vector2 scale, float sec, eComicActionCurveType curveType)
		{
		}

		// Token: 0x060038AF RID: 14511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003399")]
		[Address(RVA = "0x1011B223C", Offset = "0x11B223C", VA = "0x1011B223C")]
		public void SetActionPivot(Vector2 pivot)
		{
		}

		// Token: 0x060038B0 RID: 14512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600339A")]
		[Address(RVA = "0x1011B2284", Offset = "0x11B2284", VA = "0x1011B2284")]
		public ComicSprite()
		{
		}

		// Token: 0x040047DC RID: 18396
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400325B")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x040047DD RID: 18397
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400325C")]
		[SerializeField]
		private RectTransform m_OffsetTarget;

		// Token: 0x040047DE RID: 18398
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400325D")]
		[SerializeField]
		private RectTransform m_ActTarget;

		// Token: 0x040047DF RID: 18399
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400325E")]
		[SerializeField]
		private CanvasGroup m_FadeTarget;

		// Token: 0x040047E0 RID: 18400
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400325F")]
		private string m_RefName;

		// Token: 0x040047E1 RID: 18401
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003260")]
		private int m_Priority;

		// Token: 0x040047E2 RID: 18402
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003261")]
		private ComicAction m_CurrentAciton;

		// Token: 0x040047E3 RID: 18403
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003262")]
		private float m_FadeSec;

		// Token: 0x040047E4 RID: 18404
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003263")]
		private float m_ActionTimeCount;

		// Token: 0x040047E5 RID: 18405
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003264")]
		private float m_ActionSpeed;

		// Token: 0x040047E6 RID: 18406
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003265")]
		private float m_LerpPosCount;

		// Token: 0x040047E7 RID: 18407
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003266")]
		private float m_LerpPosTime;

		// Token: 0x040047E8 RID: 18408
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4003267")]
		private Vector2 m_LerpStartPos;

		// Token: 0x040047E9 RID: 18409
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4003268")]
		private Vector2 m_LerpFinalPos;

		// Token: 0x040047EA RID: 18410
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003269")]
		private eComicActionCurveType m_LerpCurvePos;

		// Token: 0x040047EB RID: 18411
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400326A")]
		private float m_LerpRotateCount;

		// Token: 0x040047EC RID: 18412
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x400326B")]
		private float m_LerpRotateTime;

		// Token: 0x040047ED RID: 18413
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400326C")]
		private float m_LerpStartRotate;

		// Token: 0x040047EE RID: 18414
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x400326D")]
		private float m_LerpFinalRotate;

		// Token: 0x040047EF RID: 18415
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400326E")]
		private eComicActionCurveType m_LerpCurveRotate;

		// Token: 0x040047F0 RID: 18416
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x400326F")]
		private float m_LerpScaleCount;

		// Token: 0x040047F1 RID: 18417
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003270")]
		private float m_LerpScaleTime;

		// Token: 0x040047F2 RID: 18418
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003271")]
		private Vector2 m_LerpStartScale;

		// Token: 0x040047F3 RID: 18419
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4003272")]
		private Vector2 m_LerpFinalScale;

		// Token: 0x040047F4 RID: 18420
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4003273")]
		private eComicActionCurveType m_LerpCurveScale;

		// Token: 0x040047F5 RID: 18421
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003274")]
		private RectTransform m_RectTransform;
	}
}
