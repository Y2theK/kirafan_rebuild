﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star.Comic
{
	// Token: 0x02000C43 RID: 3139
	[Token(Token = "0x2000865")]
	[StructLayout(3)]
	public class ComicUI : UIGroup
	{
		// Token: 0x060038B1 RID: 14513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600339B")]
		[Address(RVA = "0x1011B2294", Offset = "0x11B2294", VA = "0x1011B2294")]
		public void Setup(ComicPlayer player)
		{
		}

		// Token: 0x060038B2 RID: 14514 RVA: 0x00017D78 File Offset: 0x00015F78
		[Token(Token = "0x600339C")]
		[Address(RVA = "0x1011B238C", Offset = "0x11B238C", VA = "0x1011B238C")]
		private bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x060038B3 RID: 14515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600339D")]
		[Address(RVA = "0x1011B242C", Offset = "0x11B242C", VA = "0x1011B242C", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060038B4 RID: 14516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600339E")]
		[Address(RVA = "0x1011B245C", Offset = "0x11B245C", VA = "0x1011B245C")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x060038B5 RID: 14517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600339F")]
		[Address(RVA = "0x1011B2574", Offset = "0x11B2574", VA = "0x1011B2574")]
		private void UpdateUIAnim()
		{
		}

		// Token: 0x060038B6 RID: 14518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A0")]
		[Address(RVA = "0x1011B2840", Offset = "0x11B2840", VA = "0x1011B2840")]
		public void SetCellCount(int current, int max)
		{
		}

		// Token: 0x060038B7 RID: 14519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A1")]
		[Address(RVA = "0x1011B28F0", Offset = "0x11B28F0", VA = "0x1011B28F0")]
		public void SetEnableAutoUI(bool flg)
		{
		}

		// Token: 0x060038B8 RID: 14520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A2")]
		[Address(RVA = "0x1011B299C", Offset = "0x11B299C", VA = "0x1011B299C")]
		public void SetEnableWaitTapUI(bool flg)
		{
		}

		// Token: 0x060038B9 RID: 14521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A3")]
		[Address(RVA = "0x1011B22D4", Offset = "0x11B22D4", VA = "0x1011B22D4")]
		public void SetSlideMenu(bool open)
		{
		}

		// Token: 0x060038BA RID: 14522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A4")]
		[Address(RVA = "0x1011B26A0", Offset = "0x11B26A0", VA = "0x1011B26A0")]
		public void OnClickSkipButtonCallBack()
		{
		}

		// Token: 0x060038BB RID: 14523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A5")]
		[Address(RVA = "0x1011B2B3C", Offset = "0x11B2B3C", VA = "0x1011B2B3C")]
		public void OnClickAutoButtonCallBack()
		{
		}

		// Token: 0x060038BC RID: 14524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A6")]
		[Address(RVA = "0x1011B2B94", Offset = "0x11B2B94", VA = "0x1011B2B94")]
		public void OnClickPrevButtonCallBack()
		{
		}

		// Token: 0x060038BD RID: 14525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A7")]
		[Address(RVA = "0x1011B2BDC", Offset = "0x11B2BDC", VA = "0x1011B2BDC")]
		public void OnClickNextButtonCallBack()
		{
		}

		// Token: 0x060038BE RID: 14526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A8")]
		[Address(RVA = "0x1011B2C24", Offset = "0x11B2C24", VA = "0x1011B2C24")]
		public void OnClickToggleSlideMenuButtonCallBack()
		{
		}

		// Token: 0x060038BF RID: 14527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033A9")]
		[Address(RVA = "0x1011B2C9C", Offset = "0x11B2C9C", VA = "0x1011B2C9C")]
		public void OnClickAutoReleaseButtonCallBack()
		{
		}

		// Token: 0x060038C0 RID: 14528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033AA")]
		[Address(RVA = "0x1011B2CE8", Offset = "0x11B2CE8", VA = "0x1011B2CE8")]
		public ComicUI()
		{
		}

		// Token: 0x040047F6 RID: 18422
		[Token(Token = "0x4003275")]
		private const float AUTO_ICON_ROTATE_PER_SEC = -180f;

		// Token: 0x040047F7 RID: 18423
		[Token(Token = "0x4003276")]
		private const float WAITTAP_ICON_MOVE_Y = 16f;

		// Token: 0x040047F8 RID: 18424
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003277")]
		[SerializeField]
		private RectTransform m_ToggleIcon;

		// Token: 0x040047F9 RID: 18425
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003278")]
		[SerializeField]
		private AnimUIPlayer m_SlideMenuAnim;

		// Token: 0x040047FA RID: 18426
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003279")]
		[SerializeField]
		private Text m_CellCountText;

		// Token: 0x040047FB RID: 18427
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400327A")]
		[SerializeField]
		private GameObject m_AutoUIObj;

		// Token: 0x040047FC RID: 18428
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400327B")]
		[SerializeField]
		private AnimUIPlayer m_WaitTapUIObj;

		// Token: 0x040047FD RID: 18429
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400327C")]
		[SerializeField]
		private RectTransform m_AutoUIAnimTransform;

		// Token: 0x040047FE RID: 18430
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400327D")]
		[SerializeField]
		private AnimUIPlayer m_SlideMenuAutoMoveAnim;

		// Token: 0x040047FF RID: 18431
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400327E")]
		[SerializeField]
		private RectTransform m_WaitTapUIAnimTransform;

		// Token: 0x04004800 RID: 18432
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400327F")]
		private ComicPlayer m_Player;

		// Token: 0x04004801 RID: 18433
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003280")]
		private float m_UIAnimCount;
	}
}
