﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000493 RID: 1171
	[Token(Token = "0x200038B")]
	[StructLayout(3)]
	public class ComicActionListDB : ScriptableObject
	{
		// Token: 0x060013ED RID: 5101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A2")]
		[Address(RVA = "0x1011B2D28", Offset = "0x11B2D28", VA = "0x1011B2D28")]
		public ComicActionListDB()
		{
		}

		// Token: 0x040015F6 RID: 5622
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FDC")]
		public ComicActionListDB_Param[] m_Params;
	}
}
