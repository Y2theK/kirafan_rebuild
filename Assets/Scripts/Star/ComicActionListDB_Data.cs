﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000491 RID: 1169
	[Token(Token = "0x2000389")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct ComicActionListDB_Data
	{
		// Token: 0x040015F0 RID: 5616
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FD6")]
		public float m_Sec;

		// Token: 0x040015F1 RID: 5617
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4000FD7")]
		public int m_CurveType;

		// Token: 0x040015F2 RID: 5618
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FD8")]
		public int m_ElementType;

		// Token: 0x040015F3 RID: 5619
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FD9")]
		public float[] m_Args;
	}
}
