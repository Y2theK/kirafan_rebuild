﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000492 RID: 1170
	[Token(Token = "0x200038A")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct ComicActionListDB_Param
	{
		// Token: 0x040015F4 RID: 5620
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FDA")]
		public string m_ActionName;

		// Token: 0x040015F5 RID: 5621
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FDB")]
		public ComicActionListDB_Data[] m_Datas;
	}
}
