﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.Comic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000779 RID: 1913
	[Token(Token = "0x20005BF")]
	[StructLayout(3)]
	public class ComicMain : GameStateMain
	{
		// Token: 0x06001CC3 RID: 7363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A41")]
		[Address(RVA = "0x1011B2D30", Offset = "0x11B2D30", VA = "0x1011B2D30")]
		private void Start()
		{
		}

		// Token: 0x06001CC4 RID: 7364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A42")]
		[Address(RVA = "0x1011B2D3C", Offset = "0x11B2D3C", VA = "0x1011B2D3C", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001CC5 RID: 7365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A43")]
		[Address(RVA = "0x1011B3054", Offset = "0x11B3054", VA = "0x1011B3054")]
		public void Play()
		{
		}

		// Token: 0x06001CC6 RID: 7366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A44")]
		[Address(RVA = "0x1011B3108", Offset = "0x11B3108", VA = "0x1011B3108", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001CC7 RID: 7367 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A45")]
		[Address(RVA = "0x1011B3110", Offset = "0x11B3110", VA = "0x1011B3110", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001CC8 RID: 7368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A46")]
		[Address(RVA = "0x1011B31C8", Offset = "0x11B31C8", VA = "0x1011B31C8")]
		public ComicMain()
		{
		}

		// Token: 0x04002D03 RID: 11523
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002378")]
		[SerializeField]
		private ComicPlayer m_Player;

		// Token: 0x04002D04 RID: 11524
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002379")]
		private ComicMain.eStep m_Step;

		// Token: 0x0200077A RID: 1914
		[Token(Token = "0x2000E8B")]
		private enum eStep
		{
			// Token: 0x04002D06 RID: 11526
			[Token(Token = "0x4005D02")]
			None,
			// Token: 0x04002D07 RID: 11527
			[Token(Token = "0x4005D03")]
			Init,
			// Token: 0x04002D08 RID: 11528
			[Token(Token = "0x4005D04")]
			Play,
			// Token: 0x04002D09 RID: 11529
			[Token(Token = "0x4005D05")]
			RequestQuestClear,
			// Token: 0x04002D0A RID: 11530
			[Token(Token = "0x4005D06")]
			RequestQuestClearWait,
			// Token: 0x04002D0B RID: 11531
			[Token(Token = "0x4005D07")]
			Reward,
			// Token: 0x04002D0C RID: 11532
			[Token(Token = "0x4005D08")]
			RewardWait,
			// Token: 0x04002D0D RID: 11533
			[Token(Token = "0x4005D09")]
			End
		}
	}
}
