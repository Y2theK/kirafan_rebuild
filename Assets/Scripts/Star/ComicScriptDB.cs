﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000496 RID: 1174
	[Token(Token = "0x200038E")]
	[StructLayout(3)]
	public class ComicScriptDB : ScriptableObject
	{
		// Token: 0x060013EE RID: 5102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A3")]
		[Address(RVA = "0x1011B31E4", Offset = "0x11B31E4", VA = "0x1011B31E4")]
		public ComicScriptDB()
		{
		}

		// Token: 0x040015FB RID: 5627
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FE1")]
		public ComicScriptDB_Param[] m_Params;
	}
}
