﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000494 RID: 1172
	[Token(Token = "0x200038C")]
	[Serializable]
	[StructLayout(0)]
	public struct ComicScriptDB_Data
	{
		// Token: 0x040015F7 RID: 5623
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FDD")]
		public string m_CommandName;

		// Token: 0x040015F8 RID: 5624
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FDE")]
		public string[] m_Args;
	}
}
