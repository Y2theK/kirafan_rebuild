﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000495 RID: 1173
	[Token(Token = "0x200038D")]
	[Serializable]
	[StructLayout(0)]
	public struct ComicScriptDB_Param
	{
		// Token: 0x040015F9 RID: 5625
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FDF")]
		public string m_ComicName;

		// Token: 0x040015FA RID: 5626
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FE0")]
		public ComicScriptDB_Data[] m_Datas;
	}
}
