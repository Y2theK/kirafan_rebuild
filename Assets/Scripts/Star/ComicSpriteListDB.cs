﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000499 RID: 1177
	[Token(Token = "0x2000391")]
	[StructLayout(3)]
	public class ComicSpriteListDB : ScriptableObject
	{
		// Token: 0x060013EF RID: 5103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012A4")]
		[Address(RVA = "0x1011B31EC", Offset = "0x11B31EC", VA = "0x1011B31EC")]
		public ComicSpriteListDB()
		{
		}

		// Token: 0x04001603 RID: 5635
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FE9")]
		public ComicSpriteListDB_Param[] m_Params;
	}
}
