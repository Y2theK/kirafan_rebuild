﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000497 RID: 1175
	[Token(Token = "0x200038F")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct ComicSpriteListDB_Data
	{
		// Token: 0x040015FC RID: 5628
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FE2")]
		public string m_RefName;

		// Token: 0x040015FD RID: 5629
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FE3")]
		public string m_ResourceName;

		// Token: 0x040015FE RID: 5630
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000FE4")]
		public float m_X;

		// Token: 0x040015FF RID: 5631
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000FE5")]
		public float m_Y;

		// Token: 0x04001600 RID: 5632
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000FE6")]
		public int m_CellNo;
	}
}
