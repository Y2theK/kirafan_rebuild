﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000498 RID: 1176
	[Token(Token = "0x2000390")]
	[Serializable]
	[StructLayout(0)]
	public struct ComicSpriteListDB_Param
	{
		// Token: 0x04001601 RID: 5633
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4000FE7")]
		public string m_ComicName;

		// Token: 0x04001602 RID: 5634
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4000FE8")]
		public ComicSpriteListDB_Data[] m_Datas;
	}
}
