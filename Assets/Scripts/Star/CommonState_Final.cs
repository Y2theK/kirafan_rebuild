﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200077B RID: 1915
	[Token(Token = "0x20005C0")]
	[StructLayout(3)]
	public class CommonState_Final : GameStateBase
	{
		// Token: 0x06001CCA RID: 7370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A48")]
		[Address(RVA = "0x1011B3194", Offset = "0x11B3194", VA = "0x1011B3194")]
		public CommonState_Final(GameStateMain owner)
		{
		}

		// Token: 0x06001CCB RID: 7371 RVA: 0x0000CDE0 File Offset: 0x0000AFE0
		[Token(Token = "0x6001A49")]
		[Address(RVA = "0x1011B31F4", Offset = "0x11B31F4", VA = "0x1011B31F4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CCC RID: 7372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A4A")]
		[Address(RVA = "0x1011B31FC", Offset = "0x11B31FC", VA = "0x1011B31FC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CCD RID: 7373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A4B")]
		[Address(RVA = "0x1011B3204", Offset = "0x11B3204", VA = "0x1011B3204", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CCE RID: 7374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A4C")]
		[Address(RVA = "0x1011B3208", Offset = "0x11B3208", VA = "0x1011B3208", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CCF RID: 7375 RVA: 0x0000CDF8 File Offset: 0x0000AFF8
		[Token(Token = "0x6001A4D")]
		[Address(RVA = "0x1011B320C", Offset = "0x11B320C", VA = "0x1011B320C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001CD0 RID: 7376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A4E")]
		[Address(RVA = "0x1011B359C", Offset = "0x11B359C", VA = "0x1011B359C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002D0E RID: 11534
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400237A")]
		private CommonState_Final.eStep m_Step;

		// Token: 0x04002D0F RID: 11535
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400237B")]
		private GameStateMain m_Owner;

		// Token: 0x0200077C RID: 1916
		[Token(Token = "0x2000E8C")]
		private enum eStep
		{
			// Token: 0x04002D11 RID: 11537
			[Token(Token = "0x4005D0B")]
			None = -1,
			// Token: 0x04002D12 RID: 11538
			[Token(Token = "0x4005D0C")]
			First,
			// Token: 0x04002D13 RID: 11539
			[Token(Token = "0x4005D0D")]
			Step_0,
			// Token: 0x04002D14 RID: 11540
			[Token(Token = "0x4005D0E")]
			Step_1,
			// Token: 0x04002D15 RID: 11541
			[Token(Token = "0x4005D0F")]
			Step_2
		}
	}
}
