﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009DC RID: 2524
	[Token(Token = "0x200071D")]
	[StructLayout(3)]
	public class ContentRoomController : IUnitRoomController
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x060029F6 RID: 10742 RVA: 0x00011C10 File Offset: 0x0000FE10
		[Token(Token = "0x1700029D")]
		public ContentRoomController.ScheduleTime wakeupTime
		{
			[Token(Token = "0x60026A1")]
			[Address(RVA = "0x1011B35A0", Offset = "0x11B35A0", VA = "0x1011B35A0")]
			get
			{
				return default(ContentRoomController.ScheduleTime);
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x060029F7 RID: 10743 RVA: 0x00011C28 File Offset: 0x0000FE28
		[Token(Token = "0x1700029E")]
		public ContentRoomController.ScheduleTime sleepTime
		{
			[Token(Token = "0x60026A2")]
			[Address(RVA = "0x1011B35A8", Offset = "0x11B35A8", VA = "0x1011B35A8")]
			get
			{
				return default(ContentRoomController.ScheduleTime);
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x060029F8 RID: 10744 RVA: 0x00011C40 File Offset: 0x0000FE40
		[Token(Token = "0x1700029F")]
		private int dummyObjResId
		{
			[Token(Token = "0x60026A3")]
			[Address(RVA = "0x1011B35B0", Offset = "0x11B35B0", VA = "0x1011B35B0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x060029F9 RID: 10745 RVA: 0x00011C58 File Offset: 0x0000FE58
		[Token(Token = "0x170002A0")]
		private int dummyWallObjId
		{
			[Token(Token = "0x60026A4")]
			[Address(RVA = "0x1011B35B8", Offset = "0x11B35B8", VA = "0x1011B35B8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x060029FA RID: 10746 RVA: 0x00011C70 File Offset: 0x0000FE70
		[Token(Token = "0x170002A1")]
		private int dummyFloorObjId
		{
			[Token(Token = "0x60026A5")]
			[Address(RVA = "0x1011B35C0", Offset = "0x11B35C0", VA = "0x1011B35C0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x060029FB RID: 10747 RVA: 0x00011C88 File Offset: 0x0000FE88
		[Token(Token = "0x170002A2")]
		public int roomCharaSlotMax
		{
			[Token(Token = "0x60026A6")]
			[Address(RVA = "0x1011B35C8", Offset = "0x11B35C8", VA = "0x1011B35C8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002D9 RID: 729
		// (set) Token: 0x060029FC RID: 10748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002A3")]
		public Action callback_HideUI
		{
			[Token(Token = "0x60026A7")]
			[Address(RVA = "0x1011B35D0", Offset = "0x11B35D0", VA = "0x1011B35D0")]
			set
			{
			}
		}

		// Token: 0x170002DA RID: 730
		// (set) Token: 0x060029FD RID: 10749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002A4")]
		public Action callback_ShowUI
		{
			[Token(Token = "0x60026A8")]
			[Address(RVA = "0x1011B35D8", Offset = "0x11B35D8", VA = "0x1011B35D8")]
			set
			{
			}
		}

		// Token: 0x060029FE RID: 10750 RVA: 0x00011CA0 File Offset: 0x0000FEA0
		[Token(Token = "0x60026A9")]
		[Address(RVA = "0x1011B35E0", Offset = "0x11B35E0", VA = "0x1011B35E0")]
		private ContentRoomController.eTimeZone GetNowTimeZone()
		{
			return ContentRoomController.eTimeZone.Morning;
		}

		// Token: 0x060029FF RID: 10751 RVA: 0x00011CB8 File Offset: 0x0000FEB8
		[Token(Token = "0x60026AA")]
		[Address(RVA = "0x1011B36F4", Offset = "0x11B36F4", VA = "0x1011B36F4")]
		public bool IsNowWakeupTime()
		{
			return default(bool);
		}

		// Token: 0x06002A00 RID: 10752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026AB")]
		[Address(RVA = "0x1011B37E0", Offset = "0x11B37E0", VA = "0x1011B37E0")]
		private void CheckBGM()
		{
		}

		// Token: 0x06002A01 RID: 10753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026AC")]
		[Address(RVA = "0x1011B38D4", Offset = "0x11B38D4", VA = "0x1011B38D4", Slot = "4")]
		protected override void OnStart()
		{
		}

		// Token: 0x06002A02 RID: 10754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026AD")]
		[Address(RVA = "0x1011B3964", Offset = "0x11B3964", VA = "0x1011B3964", Slot = "5")]
		protected override void OnStartPreparation()
		{
		}

		// Token: 0x06002A03 RID: 10755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026AE")]
		[Address(RVA = "0x1011B3968", Offset = "0x11B3968", VA = "0x1011B3968", Slot = "6")]
		protected override void OnDonePreparation()
		{
		}

		// Token: 0x06002A04 RID: 10756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026AF")]
		[Address(RVA = "0x1011B39FC", Offset = "0x11B39FC", VA = "0x1011B39FC", Slot = "7")]
		protected override void OnUpdate()
		{
		}

		// Token: 0x06002A05 RID: 10757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026B0")]
		[Address(RVA = "0x1011B44C4", Offset = "0x11B44C4", VA = "0x1011B44C4", Slot = "8")]
		public override void OnEventSend(IMainComCommand cmd)
		{
		}

		// Token: 0x06002A06 RID: 10758 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026B1")]
		[Address(RVA = "0x1011B4530", Offset = "0x11B4530", VA = "0x1011B4530")]
		private ContentRoomController.ControlledRoomObj SearchControlledRoomObjFromParamIdx(int idx)
		{
			return null;
		}

		// Token: 0x06002A07 RID: 10759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026B2")]
		[Address(RVA = "0x1011B4630", Offset = "0x11B4630", VA = "0x1011B4630")]
		private void SetEventSealed(int eventSealed)
		{
		}

		// Token: 0x06002A08 RID: 10760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026B3")]
		[Address(RVA = "0x1011B480C", Offset = "0x11B480C", VA = "0x1011B480C")]
		private void CallbackEventEndTime(BuilderManagedId builderManagedId, UnitRoomBuilder.eCallbackType type, int value)
		{
		}

		// Token: 0x06002A09 RID: 10761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026B4")]
		[Address(RVA = "0x1011B494C", Offset = "0x11B494C", VA = "0x1011B494C")]
		private void DeleteRoomObjAnotherTitle(eTitleType titleType)
		{
		}

		// Token: 0x06002A0A RID: 10762 RVA: 0x00011CD0 File Offset: 0x0000FED0
		[Token(Token = "0x60026B5")]
		[Address(RVA = "0x1011B4B6C", Offset = "0x11B4B6C", VA = "0x1011B4B6C")]
		private bool SearchRoomObjFromAccessKey(int accessKey, ref RoomObjectListDB_Param refParam)
		{
			return default(bool);
		}

		// Token: 0x06002A0B RID: 10763 RVA: 0x00011CE8 File Offset: 0x0000FEE8
		[Token(Token = "0x60026B6")]
		[Address(RVA = "0x1011B4CD0", Offset = "0x11B4CD0", VA = "0x1011B4CD0")]
		private bool IsChangeSleepRoom(ContentRoomPresetDB db, int roomLevel)
		{
			return default(bool);
		}

		// Token: 0x06002A0C RID: 10764 RVA: 0x00011D00 File Offset: 0x0000FF00
		[Token(Token = "0x60026B7")]
		[Address(RVA = "0x1011B5414", Offset = "0x11B5414", VA = "0x1011B5414")]
		public bool SetRoomLevel(eTitleType titleType, ContentRoomPresetDB db, int roomLevel, bool isReady, bool isSkip = false, [Optional] Action<int> endCallback, int callbackId = 0)
		{
			return default(bool);
		}

		// Token: 0x06002A0D RID: 10765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026B8")]
		[Address(RVA = "0x1011B5908", Offset = "0x11B5908", VA = "0x1011B5908")]
		private void PlacementStdObj(eTitleType titleType, ContentRoomPresetDB db, int roomLevel, bool isReady, bool isSkip, List<ContentRoomController.ControlledRoomObj> newControlledRoomObjList)
		{
		}

		// Token: 0x06002A0E RID: 10766 RVA: 0x00011D18 File Offset: 0x0000FF18
		[Token(Token = "0x60026B9")]
		[Address(RVA = "0x1011B61BC", Offset = "0x11B61BC", VA = "0x1011B61BC")]
		private bool PlacementExChangeObj(eTitleType titleType, ContentRoomPresetDB db, int roomLevel, bool isReady, bool isSkip, List<ContentRoomController.ControlledRoomObj> newControlledRoomObjList)
		{
			return default(bool);
		}

		// Token: 0x06002A0F RID: 10767 RVA: 0x00011D30 File Offset: 0x0000FF30
		[Token(Token = "0x60026BA")]
		[Address(RVA = "0x1011B7868", Offset = "0x11B7868", VA = "0x1011B7868")]
		public bool SetRoomChara(ContentRoomController.RoomChara[] roomCharas, [Optional] Action<int> endCallback, int callbackId = 0)
		{
			return default(bool);
		}

		// Token: 0x06002A10 RID: 10768 RVA: 0x00011D48 File Offset: 0x0000FF48
		[Token(Token = "0x60026BB")]
		[Address(RVA = "0x1011B7878", Offset = "0x11B7878", VA = "0x1011B7878")]
		public bool SetRoomChara(ContentRoomController.RoomChara[] roomCharas, bool randPosFlg, [Optional] Action<int> endCallback, int callbackId = 0)
		{
			return default(bool);
		}

		// Token: 0x06002A11 RID: 10769 RVA: 0x00011D60 File Offset: 0x0000FF60
		[Token(Token = "0x60026BC")]
		[Address(RVA = "0x1011B837C", Offset = "0x11B837C", VA = "0x1011B837C")]
		private IVector2 SearchEmptyGrid(int startX, int startY, List<IVector2> usedGrid)
		{
			return default(IVector2);
		}

		// Token: 0x06002A12 RID: 10770 RVA: 0x00011D78 File Offset: 0x0000FF78
		[Token(Token = "0x60026BD")]
		[Address(RVA = "0x1011B8438", Offset = "0x11B8438", VA = "0x1011B8438")]
		private bool CheckGridFromDistance(int startX, int startY, int distance, RoomGridState gridState, List<IVector2> usedGrid, ref IVector2 refGridPos)
		{
			return default(bool);
		}

		// Token: 0x06002A13 RID: 10771 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026BE")]
		[Address(RVA = "0x1011B817C", Offset = "0x11B817C", VA = "0x1011B817C")]
		private List<IVector2> GetEmptyGridList(int startX, int startY)
		{
			return null;
		}

		// Token: 0x06002A14 RID: 10772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026BF")]
		[Address(RVA = "0x1011B8714", Offset = "0x11B8714", VA = "0x1011B8714")]
		private void GetEmptyGridListSub(int checkX, int checkY, RoomGridState gridState, bool[,] usedGrid, List<IVector2> emptyGridList)
		{
		}

		// Token: 0x06002A15 RID: 10773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026C0")]
		[Address(RVA = "0x1011B3BEC", Offset = "0x11B3BEC", VA = "0x1011B3BEC")]
		private void Update_ObjectTouch()
		{
		}

		// Token: 0x06002A16 RID: 10774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026C1")]
		[Address(RVA = "0x1011B89A4", Offset = "0x11B89A4", VA = "0x1011B89A4")]
		public ContentRoomController()
		{
		}

		// Token: 0x04003A50 RID: 14928
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A0A")]
		[SerializeField]
		private ContentRoomController.ScheduleTime m_MorningTime;

		// Token: 0x04003A51 RID: 14929
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A0B")]
		[SerializeField]
		private ContentRoomController.ScheduleTime m_AfterNoonTime;

		// Token: 0x04003A52 RID: 14930
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002A0C")]
		[SerializeField]
		private ContentRoomController.ScheduleTime m_EveningTime;

		// Token: 0x04003A53 RID: 14931
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002A0D")]
		[SerializeField]
		private ContentRoomController.ScheduleTime m_WakeupTime;

		// Token: 0x04003A54 RID: 14932
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002A0E")]
		[SerializeField]
		private ContentRoomController.ScheduleTime m_SleepTime;

		// Token: 0x04003A55 RID: 14933
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002A0F")]
		private ContentRoomController.eTimeZone m_TimeZoneHistory;

		// Token: 0x04003A56 RID: 14934
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002A10")]
		private RoomEventCmd_ChangeObj.ChangeEffect m_SmokeEffectData;

		// Token: 0x04003A57 RID: 14935
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002A11")]
		[SerializeField]
		private RoomObjChangeEffect[] m_ChangeEffects;

		// Token: 0x04003A58 RID: 14936
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002A12")]
		[SerializeField]
		private bool m_isDummyBoxFillGrid;

		// Token: 0x04003A59 RID: 14937
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002A13")]
		private IRoomObjectControll m_SelectingObj;

		// Token: 0x04003A5A RID: 14938
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002A14")]
		private RoomBuilder.EditObjectState m_EditObjState;

		// Token: 0x04003A5B RID: 14939
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002A15")]
		private int m_MainRayLayerMask;

		// Token: 0x04003A5C RID: 14940
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002A16")]
		private IRoomObjectControll.ObjTouchState m_ObjTouch;

		// Token: 0x04003A5D RID: 14941
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002A17")]
		private Vector2 m_TouchStart;

		// Token: 0x04003A5E RID: 14942
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002A18")]
		private bool m_CharaEventSealedFlg;

		// Token: 0x04003A5F RID: 14943
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4002A19")]
		private int m_DummyObjResId;

		// Token: 0x04003A60 RID: 14944
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002A1A")]
		private int m_DummyWallObjId;

		// Token: 0x04003A61 RID: 14945
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4002A1B")]
		private int m_DummyFloorObjId;

		// Token: 0x04003A62 RID: 14946
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002A1C")]
		[SerializeField]
		private ContentRoomController.FocusPoint m_MainRoomFocusPoint;

		// Token: 0x04003A63 RID: 14947
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4002A1D")]
		[SerializeField]
		private ContentRoomController.FocusPoint m_SleepRoomFocusPoint;

		// Token: 0x04003A64 RID: 14948
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002A1E")]
		private Action m_Callback_HideUI;

		// Token: 0x04003A65 RID: 14949
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002A1F")]
		private Action m_Callback_ShowUI;

		// Token: 0x04003A66 RID: 14950
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002A20")]
		private List<ContentRoomController.ControlledRoomObj> m_ControlledRoomObjList;

		// Token: 0x04003A67 RID: 14951
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002A21")]
		private readonly IVector2[] gridOffsets;

		// Token: 0x020009DD RID: 2525
		[Token(Token = "0x2000F8F")]
		private enum eTimeZone
		{
			// Token: 0x04003A69 RID: 14953
			[Token(Token = "0x40063BA")]
			Morning,
			// Token: 0x04003A6A RID: 14954
			[Token(Token = "0x40063BB")]
			AfterNoon,
			// Token: 0x04003A6B RID: 14955
			[Token(Token = "0x40063BC")]
			Evening,
			// Token: 0x04003A6C RID: 14956
			[Token(Token = "0x40063BD")]
			Max
		}

		// Token: 0x020009DE RID: 2526
		[Token(Token = "0x2000F90")]
		[Serializable]
		public struct ScheduleTime
		{
			// Token: 0x170002DB RID: 731
			// (get) Token: 0x06002A18 RID: 10776 RVA: 0x00011D90 File Offset: 0x0000FF90
			// (set) Token: 0x06002A17 RID: 10775 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006AC")]
			public int hour
			{
				[Token(Token = "0x6006014")]
				[Address(RVA = "0x1000300CC", Offset = "0x300CC", VA = "0x1000300CC")]
				get
				{
					return 0;
				}
				[Token(Token = "0x6006013")]
				[Address(RVA = "0x1000300C4", Offset = "0x300C4", VA = "0x1000300C4")]
				set
				{
				}
			}

			// Token: 0x170002DC RID: 732
			// (get) Token: 0x06002A1A RID: 10778 RVA: 0x00011DA8 File Offset: 0x0000FFA8
			// (set) Token: 0x06002A19 RID: 10777 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006AD")]
			public int minute
			{
				[Token(Token = "0x6006016")]
				[Address(RVA = "0x1000300DC", Offset = "0x300DC", VA = "0x1000300DC")]
				get
				{
					return 0;
				}
				[Token(Token = "0x6006015")]
				[Address(RVA = "0x1000300D4", Offset = "0x300D4", VA = "0x1000300D4")]
				set
				{
				}
			}

			// Token: 0x06002A1B RID: 10779 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006017")]
			[Address(RVA = "0x1000300E4", Offset = "0x300E4", VA = "0x1000300E4")]
			public ScheduleTime(int hour, int minute)
			{
			}

			// Token: 0x06002A1C RID: 10780 RVA: 0x00011DC0 File Offset: 0x0000FFC0
			[Token(Token = "0x6006018")]
			[Address(RVA = "0x1000300EC", Offset = "0x300EC", VA = "0x1000300EC")]
			public int GetTotalMinute()
			{
				return 0;
			}

			// Token: 0x06002A1D RID: 10781 RVA: 0x00011DD8 File Offset: 0x0000FFD8
			[Token(Token = "0x6006019")]
			[Address(RVA = "0x1011B924C", Offset = "0x11B924C", VA = "0x1011B924C")]
			public static int GetTotalMinute(int hour, int minute)
			{
				return 0;
			}

			// Token: 0x06002A1E RID: 10782 RVA: 0x00011DF0 File Offset: 0x0000FFF0
			[Token(Token = "0x600601A")]
			[Address(RVA = "0x1011B9258", Offset = "0x11B9258", VA = "0x1011B9258")]
			public static int GetTotalMinuteMin()
			{
				return 0;
			}

			// Token: 0x06002A1F RID: 10783 RVA: 0x00011E08 File Offset: 0x00010008
			[Token(Token = "0x600601B")]
			[Address(RVA = "0x1011B9260", Offset = "0x11B9260", VA = "0x1011B9260")]
			public static int GetTotalMinuteMax()
			{
				return 0;
			}

			// Token: 0x06002A20 RID: 10784 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600601C")]
			[Address(RVA = "0x1000300FC", Offset = "0x300FC", VA = "0x1000300FC")]
			public void SetTotalMinute(int totalMinute)
			{
			}

			// Token: 0x04003A6D RID: 14957
			[Token(Token = "0x40063BE")]
			public const int MaxHour = 24;

			// Token: 0x04003A6E RID: 14958
			[Token(Token = "0x40063BF")]
			public const int MaxMinute = 60;

			// Token: 0x04003A6F RID: 14959
			[Token(Token = "0x40063C0")]
			public const int MaxHourValue = 23;

			// Token: 0x04003A70 RID: 14960
			[Token(Token = "0x40063C1")]
			public const int MaxMinuteValue = 59;

			// Token: 0x04003A71 RID: 14961
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063C2")]
			[SerializeField]
			[Attribute(Name = "RangeAttribute", RVA = "0x1001346DC", Offset = "0x1346DC")]
			private int m_Hour;

			// Token: 0x04003A72 RID: 14962
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40063C3")]
			[SerializeField]
			[Attribute(Name = "RangeAttribute", RVA = "0x10013471C", Offset = "0x13471C")]
			private int m_Minute;
		}

		// Token: 0x020009DF RID: 2527
		[Token(Token = "0x2000F91")]
		[Serializable]
		public struct FocusPoint
		{
			// Token: 0x06002A21 RID: 10785 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600601D")]
			[Address(RVA = "0x1000300B8", Offset = "0x300B8", VA = "0x1000300B8")]
			public FocusPoint(float tgtPosX, float tgtPosY, float tgtSize)
			{
			}

			// Token: 0x04003A73 RID: 14963
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063C4")]
			public float tgtPosX;

			// Token: 0x04003A74 RID: 14964
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40063C5")]
			public float tgtPosY;

			// Token: 0x04003A75 RID: 14965
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40063C6")]
			public float tgtSize;
		}

		// Token: 0x020009E0 RID: 2528
		[Token(Token = "0x2000F92")]
		private class ControlledRoomObj
		{
			// Token: 0x06002A22 RID: 10786 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600601E")]
			[Address(RVA = "0x1011B7860", Offset = "0x11B7860", VA = "0x1011B7860")]
			public ControlledRoomObj()
			{
			}

			// Token: 0x04003A76 RID: 14966
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063C7")]
			internal eTitleType titleType;

			// Token: 0x04003A77 RID: 14967
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40063C8")]
			internal int roomParamIdx;

			// Token: 0x04003A78 RID: 14968
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40063C9")]
			internal BuilderManagedId[] builderManagedIds;

			// Token: 0x04003A79 RID: 14969
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40063CA")]
			internal bool isDummy;
		}

		// Token: 0x020009E1 RID: 2529
		[Token(Token = "0x2000F93")]
		public struct RoomChara
		{
			// Token: 0x04003A7A RID: 14970
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063CB")]
			public long managedId;

			// Token: 0x04003A7B RID: 14971
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40063CC")]
			public int slotId;
		}
	}
}
