﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200077D RID: 1917
	[Token(Token = "0x20005C1")]
	[StructLayout(3)]
	public class ContentRoomMain : GameStateMain
	{
		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06001CD1 RID: 7377 RVA: 0x0000CE10 File Offset: 0x0000B010
		[Token(Token = "0x17000208")]
		public eTitleType TitleType
		{
			[Token(Token = "0x6001A4F")]
			[Address(RVA = "0x1011B9268", Offset = "0x11B9268", VA = "0x1011B9268")]
			get
			{
				return eTitleType.Title_0000;
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06001CD2 RID: 7378 RVA: 0x0000CE28 File Offset: 0x0000B028
		[Token(Token = "0x17000209")]
		public bool IsHaveItem
		{
			[Token(Token = "0x6001A50")]
			[Address(RVA = "0x1011B9270", Offset = "0x11B9270", VA = "0x1011B9270")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06001CD3 RID: 7379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A51")]
		[Address(RVA = "0x1011B92F8", Offset = "0x11B92F8", VA = "0x1011B92F8")]
		private void Start()
		{
		}

		// Token: 0x06001CD4 RID: 7380 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A52")]
		[Address(RVA = "0x1011B9528", Offset = "0x11B9528", VA = "0x1011B9528", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001CD5 RID: 7381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A53")]
		[Address(RVA = "0x1011B969C", Offset = "0x11B969C", VA = "0x1011B969C", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001CD6 RID: 7382 RVA: 0x0000CE40 File Offset: 0x0000B040
		[Token(Token = "0x6001A54")]
		[Address(RVA = "0x1011B9858", Offset = "0x11B9858", VA = "0x1011B9858", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001CD7 RID: 7383 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A55")]
		[Address(RVA = "0x1011B9860", Offset = "0x11B9860", VA = "0x1011B9860")]
		public ContentRoomMembers GetContentRoomMembers()
		{
			return null;
		}

		// Token: 0x06001CD8 RID: 7384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A56")]
		[Address(RVA = "0x1011B9868", Offset = "0x11B9868", VA = "0x1011B9868")]
		public void CallSetRoomLevel(eTitleType titleType, ContentRoomPresetDB db, int roomLevel, bool isReady, bool isSkip, Action<int> endCallback)
		{
		}

		// Token: 0x06001CD9 RID: 7385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A57")]
		[Address(RVA = "0x1011B9914", Offset = "0x11B9914", VA = "0x1011B9914")]
		private void CallSetRoomChara(ContentRoomController.RoomChara[] roomCharas, Action<int> endCallback, bool isRandom)
		{
		}

		// Token: 0x06001CDA RID: 7386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A58")]
		[Address(RVA = "0x1011B97F8", Offset = "0x11B97F8", VA = "0x1011B97F8")]
		public void SetCharaHoldCallback(Action callback)
		{
		}

		// Token: 0x06001CDB RID: 7387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A59")]
		[Address(RVA = "0x1011B9828", Offset = "0x11B9828", VA = "0x1011B9828")]
		public void SetCharaReleaseCallback(Action callback)
		{
		}

		// Token: 0x06001CDC RID: 7388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5A")]
		[Address(RVA = "0x1011B99D0", Offset = "0x11B99D0", VA = "0x1011B99D0")]
		public void SetFlgPlayScriptSoundForUI(bool flag)
		{
		}

		// Token: 0x06001CDD RID: 7389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5B")]
		[Address(RVA = "0x1011B9A20", Offset = "0x11B9A20", VA = "0x1011B9A20")]
		public void InitializeContentRoom(Action finalCallback)
		{
		}

		// Token: 0x06001CDE RID: 7390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5C")]
		[Address(RVA = "0x1011B9B94", Offset = "0x11B9B94", VA = "0x1011B9B94")]
		private void SetRoomLevelCallback(int callbackId)
		{
		}

		// Token: 0x06001CDF RID: 7391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5D")]
		[Address(RVA = "0x1011B9DE4", Offset = "0x11B9DE4", VA = "0x1011B9DE4")]
		private void SetRoomCharaCallback(int callbackId)
		{
		}

		// Token: 0x06001CE0 RID: 7392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5E")]
		[Address(RVA = "0x1011B9E10", Offset = "0x11B9E10", VA = "0x1011B9E10")]
		public void ReplaceRoomInterior(bool isReady, bool isSkip, Action<int> callback)
		{
		}

		// Token: 0x06001CE1 RID: 7393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A5F")]
		[Address(RVA = "0x1011B9EDC", Offset = "0x11B9EDC", VA = "0x1011B9EDC")]
		public void ReplaceRoomMember(long[] charaMngId, Action finalCallback)
		{
		}

		// Token: 0x06001CE2 RID: 7394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A60")]
		[Address(RVA = "0x1011BA0CC", Offset = "0x11BA0CC", VA = "0x1011BA0CC")]
		private void OnResponse_SetAll()
		{
		}

		// Token: 0x06001CE3 RID: 7395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A61")]
		[Address(RVA = "0x1011BA2C8", Offset = "0x11BA2C8", VA = "0x1011BA2C8")]
		private void OnComplete_SetRoomChara(int callbackId)
		{
		}

		// Token: 0x06001CE4 RID: 7396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A62")]
		[Address(RVA = "0x1011BA2F4", Offset = "0x11BA2F4", VA = "0x1011BA2F4")]
		public void CreateSubOfferDataList()
		{
		}

		// Token: 0x06001CE5 RID: 7397 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A63")]
		[Address(RVA = "0x1011BA8B4", Offset = "0x11BA8B4", VA = "0x1011BA8B4")]
		public List<int> GetNamedList()
		{
			return null;
		}

		// Token: 0x06001CE6 RID: 7398 RVA: 0x0000CE58 File Offset: 0x0000B058
		[Token(Token = "0x6001A64")]
		[Address(RVA = "0x1011BA8BC", Offset = "0x11BA8BC", VA = "0x1011BA8BC")]
		public int GetNamedIndex(eCharaNamedType namedType)
		{
			return 0;
		}

		// Token: 0x06001CE7 RID: 7399 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A65")]
		[Address(RVA = "0x1011BA98C", Offset = "0x11BA98C", VA = "0x1011BA98C")]
		public List<int> GetCharacterIDList(int namedType)
		{
			return null;
		}

		// Token: 0x06001CE8 RID: 7400 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A66")]
		[Address(RVA = "0x1011BA9FC", Offset = "0x11BA9FC", VA = "0x1011BA9FC")]
		public List<OfferManager.OfferData> GetSubOfferDataList(int charaID)
		{
			return null;
		}

		// Token: 0x06001CE9 RID: 7401 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A67")]
		[Address(RVA = "0x1011BAA6C", Offset = "0x11BAA6C", VA = "0x1011BAA6C")]
		public OfferManager.OfferData GetSubOfferData(long mngID)
		{
			return null;
		}

		// Token: 0x06001CEA RID: 7402 RVA: 0x0000CE70 File Offset: 0x0000B070
		[Token(Token = "0x6001A68")]
		[Address(RVA = "0x1011BADB8", Offset = "0x11BADB8", VA = "0x1011BADB8")]
		public int GetBadgeCountNamed(int namedType)
		{
			return 0;
		}

		// Token: 0x06001CEB RID: 7403 RVA: 0x0000CE88 File Offset: 0x0000B088
		[Token(Token = "0x6001A69")]
		[Address(RVA = "0x1011BAF60", Offset = "0x11BAF60", VA = "0x1011BAF60")]
		public int GetBadgeCountCharacter(int characterID, bool haveItem = true)
		{
			return 0;
		}

		// Token: 0x06001CEC RID: 7404 RVA: 0x0000CEA0 File Offset: 0x0000B0A0
		[Token(Token = "0x6001A6A")]
		[Address(RVA = "0x1011BB0EC", Offset = "0x11BB0EC", VA = "0x1011BB0EC")]
		public bool IsHaveCharacter(int charaID)
		{
			return default(bool);
		}

		// Token: 0x06001CED RID: 7405 RVA: 0x0000CEB8 File Offset: 0x0000B0B8
		[Token(Token = "0x6001A6B")]
		[Address(RVA = "0x1011BB15C", Offset = "0x11BB15C", VA = "0x1011BB15C")]
		private int CompareNamed(int namedA, int namedB)
		{
			return 0;
		}

		// Token: 0x06001CEE RID: 7406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A6C")]
		[Address(RVA = "0x1011BB408", Offset = "0x11BB408", VA = "0x1011BB408")]
		public ContentRoomMain()
		{
		}

		// Token: 0x04002D16 RID: 11542
		[Token(Token = "0x400237C")]
		public const int STATE_INIT = 0;

		// Token: 0x04002D17 RID: 11543
		[Token(Token = "0x400237D")]
		public const int STATE_MAIN = 1;

		// Token: 0x04002D18 RID: 11544
		[Token(Token = "0x400237E")]
		public const int CALLBACK_ID_SET_ROOM_LEVEL = 1;

		// Token: 0x04002D19 RID: 11545
		[Token(Token = "0x400237F")]
		public const int CALLBACK_ID_SET_ROOM_CHARA = 2;

		// Token: 0x04002D1A RID: 11546
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002380")]
		[SerializeField]
		private ContentRoomController m_ContentRoomController;

		// Token: 0x04002D1B RID: 11547
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002381")]
		private ContentRoomMembers m_ContentRoomMembers;

		// Token: 0x04002D1C RID: 11548
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002382")]
		private ContentRoomPresetDB m_ContentRoomDB;

		// Token: 0x04002D1D RID: 11549
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002383")]
		private eTitleType m_TitleType;

		// Token: 0x04002D1E RID: 11550
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002384")]
		private long[] m_RoomMember;

		// Token: 0x04002D1F RID: 11551
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002385")]
		private Dictionary<int, List<int>> m_CharacterIDTable;

		// Token: 0x04002D20 RID: 11552
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002386")]
		private Dictionary<int, List<OfferManager.OfferData>> m_SubOfferDataTable;

		// Token: 0x04002D21 RID: 11553
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002387")]
		private List<int> m_NamedList;

		// Token: 0x04002D22 RID: 11554
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002388")]
		private List<int> m_HaveCharaList;

		// Token: 0x04002D23 RID: 11555
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002389")]
		private Action m_FinalCallback;
	}
}
