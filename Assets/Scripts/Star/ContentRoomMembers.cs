﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x020008F4 RID: 2292
	[Token(Token = "0x2000695")]
	[StructLayout(3)]
	public class ContentRoomMembers
	{
		// Token: 0x06002591 RID: 9617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022AD")]
		[Address(RVA = "0x1011B9470", Offset = "0x11B9470", VA = "0x1011B9470")]
		public ContentRoomMembers()
		{
		}

		// Token: 0x06002592 RID: 9618 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60022AE")]
		[Address(RVA = "0x1011B9B8C", Offset = "0x11B9B8C", VA = "0x1011B9B8C")]
		public long[] GetCharaMngIDs()
		{
			return null;
		}

		// Token: 0x06002593 RID: 9619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022AF")]
		[Address(RVA = "0x1011BB4C0", Offset = "0x11BB4C0", VA = "0x1011BB4C0")]
		public void SetCharaMngIDs(long[] charaMngIDs)
		{
		}

		// Token: 0x1400002A RID: 42
		// (add) Token: 0x06002594 RID: 9620 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002595 RID: 9621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400002A")]
		private event Action m_OnResponse_GetAll
		{
			[Token(Token = "0x60022B0")]
			[Address(RVA = "0x1011BB4C8", Offset = "0x11BB4C8", VA = "0x1011BB4C8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60022B1")]
			[Address(RVA = "0x1011BB5D4", Offset = "0x11BB5D4", VA = "0x1011BB5D4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400002B RID: 43
		// (add) Token: 0x06002596 RID: 9622 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002597 RID: 9623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400002B")]
		private event Action m_OnResponse_SetAll
		{
			[Token(Token = "0x60022B2")]
			[Address(RVA = "0x1011BB6E0", Offset = "0x11BB6E0", VA = "0x1011BB6E0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60022B3")]
			[Address(RVA = "0x1011BB7EC", Offset = "0x11BB7EC", VA = "0x1011BB7EC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002598 RID: 9624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B4")]
		[Address(RVA = "0x1011BB8F8", Offset = "0x11BB8F8", VA = "0x1011BB8F8")]
		public void Request_GetAll(eTitleType titleType, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x06002599 RID: 9625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B5")]
		[Address(RVA = "0x1011BBA1C", Offset = "0x11BBA1C", VA = "0x1011BBA1C")]
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600259A RID: 9626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B6")]
		[Address(RVA = "0x1011B9F8C", Offset = "0x11B9F8C", VA = "0x1011B9F8C")]
		public void Request_SetAll(eTitleType titleType, long[] charaMngIDs, Action onResponse, bool isOpenConnectingIcon = true)
		{
		}

		// Token: 0x0600259B RID: 9627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B7")]
		[Address(RVA = "0x1011BBAAC", Offset = "0x11BBAAC", VA = "0x1011BBAAC")]
		private void OnResponse_SetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x040035CE RID: 13774
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400277C")]
		private long[] m_CharaMngIDs;
	}
}
