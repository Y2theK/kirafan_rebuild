﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004DB RID: 1243
	[Token(Token = "0x20003D3")]
	[StructLayout(3)]
	public class ContentRoomPresetDB : ScriptableObject
	{
		// Token: 0x0600140B RID: 5131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60012C0")]
		[Address(RVA = "0x1011BBB3C", Offset = "0x11BBB3C", VA = "0x1011BBB3C")]
		public ContentRoomPresetDB()
		{
		}

		// Token: 0x04001778 RID: 6008
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400115E")]
		public ContentRoomPresetDB_Param[] m_Params;
	}
}
