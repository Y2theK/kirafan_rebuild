﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004DA RID: 1242
	[Token(Token = "0x20003D2")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct ContentRoomPresetDB_Param
	{
		// Token: 0x04001771 RID: 6001
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001157")]
		public int m_RoomLv;

		// Token: 0x04001772 RID: 6002
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001158")]
		public int m_FloorID;

		// Token: 0x04001773 RID: 6003
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001159")]
		public int m_Category;

		// Token: 0x04001774 RID: 6004
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400115A")]
		public int m_ObjID;

		// Token: 0x04001775 RID: 6005
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400115B")]
		public int m_PosX;

		// Token: 0x04001776 RID: 6006
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400115C")]
		public int m_PosY;

		// Token: 0x04001777 RID: 6007
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400115D")]
		public string m_Dir;
	}
}
