﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200077E RID: 1918
	[Token(Token = "0x20005C2")]
	[StructLayout(3)]
	public class ContentRoomState : GameStateBase
	{
		// Token: 0x06001CEF RID: 7407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A6D")]
		[Address(RVA = "0x1011BBB44", Offset = "0x11BBB44", VA = "0x1011BBB44")]
		public ContentRoomState(ContentRoomMain owner)
		{
		}

		// Token: 0x06001CF0 RID: 7408 RVA: 0x0000CED0 File Offset: 0x0000B0D0
		[Token(Token = "0x6001A6E")]
		[Address(RVA = "0x1011BBB78", Offset = "0x11BBB78", VA = "0x1011BBB78", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CF1 RID: 7409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A6F")]
		[Address(RVA = "0x1011BBB80", Offset = "0x11BBB80", VA = "0x1011BBB80", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CF2 RID: 7410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A70")]
		[Address(RVA = "0x1011BBB84", Offset = "0x11BBB84", VA = "0x1011BBB84", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CF3 RID: 7411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A71")]
		[Address(RVA = "0x1011BBB88", Offset = "0x11BBB88", VA = "0x1011BBB88", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CF4 RID: 7412 RVA: 0x0000CEE8 File Offset: 0x0000B0E8
		[Token(Token = "0x6001A72")]
		[Address(RVA = "0x1011BBB8C", Offset = "0x11BBB8C", VA = "0x1011BBB8C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001CF5 RID: 7413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A73")]
		[Address(RVA = "0x1011BBB94", Offset = "0x11BBB94", VA = "0x1011BBB94", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002D24 RID: 11556
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400238A")]
		protected ContentRoomMain m_Owner;

		// Token: 0x04002D25 RID: 11557
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400238B")]
		protected int m_NextState;
	}
}
