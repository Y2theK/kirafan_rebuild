﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200077F RID: 1919
	[Token(Token = "0x20005C3")]
	[StructLayout(3)]
	public class ContentRoomState_Init : ContentRoomState
	{
		// Token: 0x06001CF6 RID: 7414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A74")]
		[Address(RVA = "0x1011B961C", Offset = "0x11B961C", VA = "0x1011B961C")]
		public ContentRoomState_Init(ContentRoomMain owner)
		{
		}

		// Token: 0x06001CF7 RID: 7415 RVA: 0x0000CF00 File Offset: 0x0000B100
		[Token(Token = "0x6001A75")]
		[Address(RVA = "0x1011BBB98", Offset = "0x11BBB98", VA = "0x1011BBB98", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001CF8 RID: 7416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A76")]
		[Address(RVA = "0x1011BBBA0", Offset = "0x11BBBA0", VA = "0x1011BBBA0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001CF9 RID: 7417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A77")]
		[Address(RVA = "0x1011BBBAC", Offset = "0x11BBBAC", VA = "0x1011BBBAC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001CFA RID: 7418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A78")]
		[Address(RVA = "0x1011BBBB0", Offset = "0x11BBBB0", VA = "0x1011BBBB0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001CFB RID: 7419 RVA: 0x0000CF18 File Offset: 0x0000B118
		[Token(Token = "0x6001A79")]
		[Address(RVA = "0x1011BBBB4", Offset = "0x11BBBB4", VA = "0x1011BBBB4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001CFC RID: 7420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A7A")]
		[Address(RVA = "0x1011BC020", Offset = "0x11BC020", VA = "0x1011BC020")]
		private void OnResponse_SetShown()
		{
		}

		// Token: 0x06001CFD RID: 7421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A7B")]
		[Address(RVA = "0x1011BC0DC", Offset = "0x11BC0DC", VA = "0x1011BC0DC")]
		private void OnResponse_GetAll()
		{
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A7C")]
		[Address(RVA = "0x1011BC194", Offset = "0x11BC194", VA = "0x1011BC194")]
		private void OnResponse_ContentRoom_GetAll()
		{
		}

		// Token: 0x06001CFF RID: 7423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A7D")]
		[Address(RVA = "0x1011BC1A0", Offset = "0x11BC1A0", VA = "0x1011BC1A0")]
		private void BuildRoomCallback()
		{
		}

		// Token: 0x04002D26 RID: 11558
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400238C")]
		private ContentRoomState_Init.eStep m_Step;

		// Token: 0x04002D27 RID: 11559
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400238D")]
		private bool m_IsResponse;

		// Token: 0x04002D28 RID: 11560
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x400238E")]
		private bool m_BuildRoom;

		// Token: 0x02000780 RID: 1920
		[Token(Token = "0x2000E8D")]
		private enum eStep
		{
			// Token: 0x04002D2A RID: 11562
			[Token(Token = "0x4005D11")]
			None,
			// Token: 0x04002D2B RID: 11563
			[Token(Token = "0x4005D12")]
			First,
			// Token: 0x04002D2C RID: 11564
			[Token(Token = "0x4005D13")]
			LoadWait,
			// Token: 0x04002D2D RID: 11565
			[Token(Token = "0x4005D14")]
			BuildRoom,
			// Token: 0x04002D2E RID: 11566
			[Token(Token = "0x4005D15")]
			BuildRoomWait,
			// Token: 0x04002D2F RID: 11567
			[Token(Token = "0x4005D16")]
			End
		}
	}
}
