﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using WWWTypes;

namespace Star
{
	// Token: 0x02000781 RID: 1921
	[Token(Token = "0x20005C4")]
	[StructLayout(3)]
	public class ContentRoomState_Main : ContentRoomState
	{
		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06001D00 RID: 7424 RVA: 0x0000CF30 File Offset: 0x0000B130
		// (set) Token: 0x06001D01 RID: 7425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700020A")]
		public bool TransitHideUIMode
		{
			[Token(Token = "0x6001A7E")]
			[Address(RVA = "0x1011BC1AC", Offset = "0x11BC1AC", VA = "0x1011BC1AC")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001A7F")]
			[Address(RVA = "0x1011BC1B4", Offset = "0x11BC1B4", VA = "0x1011BC1B4")]
			set
			{
			}
		}

		// Token: 0x06001D02 RID: 7426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A80")]
		[Address(RVA = "0x1011B9650", Offset = "0x11B9650", VA = "0x1011B9650")]
		public ContentRoomState_Main(ContentRoomMain owner)
		{
		}

		// Token: 0x06001D03 RID: 7427 RVA: 0x0000CF48 File Offset: 0x0000B148
		[Token(Token = "0x6001A81")]
		[Address(RVA = "0x1011BC1BC", Offset = "0x11BC1BC", VA = "0x1011BC1BC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D04 RID: 7428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A82")]
		[Address(RVA = "0x1011BC1C4", Offset = "0x11BC1C4", VA = "0x1011BC1C4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D05 RID: 7429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A83")]
		[Address(RVA = "0x1011BC1D0", Offset = "0x11BC1D0", VA = "0x1011BC1D0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D06 RID: 7430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A84")]
		[Address(RVA = "0x1011BC278", Offset = "0x11BC278", VA = "0x1011BC278", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D07 RID: 7431 RVA: 0x0000CF60 File Offset: 0x0000B160
		[Token(Token = "0x6001A85")]
		[Address(RVA = "0x1011BC330", Offset = "0x11BC330", VA = "0x1011BC330", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D08 RID: 7432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A86")]
		[Address(RVA = "0x1011BDA78", Offset = "0x11BDA78", VA = "0x1011BDA78")]
		public void SetOccurredOfferEffect(ContentRoomState_Main.eOccurredEffectType occurredEffectType, long occurredCharaOfferMngID)
		{
		}

		// Token: 0x06001D09 RID: 7433 RVA: 0x0000CF78 File Offset: 0x0000B178
		[Token(Token = "0x6001A87")]
		[Address(RVA = "0x1011BD1EC", Offset = "0x11BD1EC", VA = "0x1011BD1EC")]
		private bool CheckEffect(eTitleType titleType, ref ContentRoomState_Main.eOfferEffectType offerEffectType, ref long managedId)
		{
			return default(bool);
		}

		// Token: 0x06001D0A RID: 7434 RVA: 0x0000CF90 File Offset: 0x0000B190
		[Token(Token = "0x6001A88")]
		[Address(RVA = "0x1011BD430", Offset = "0x11BD430", VA = "0x1011BD430")]
		private bool CheckEffectSubOffer(long offerMngID, ref ContentRoomState_Main.eOfferEffectType offerEffectType)
		{
			return default(bool);
		}

		// Token: 0x06001D0B RID: 7435 RVA: 0x0000CFA8 File Offset: 0x0000B1A8
		[Token(Token = "0x6001A89")]
		[Address(RVA = "0x1011BD4A8", Offset = "0x11BD4A8", VA = "0x1011BD4A8")]
		private bool UpdateEffect()
		{
			return default(bool);
		}

		// Token: 0x06001D0C RID: 7436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A8A")]
		[Address(RVA = "0x1011BDA84", Offset = "0x11BDA84", VA = "0x1011BDA84", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001D0D RID: 7437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A8B")]
		[Address(RVA = "0x1011BDA88", Offset = "0x11BDA88", VA = "0x1011BDA88")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x06001D0E RID: 7438 RVA: 0x0000CFC0 File Offset: 0x0000B1C0
		[Token(Token = "0x6001A8C")]
		[Address(RVA = "0x1011BDB24", Offset = "0x11BDB24", VA = "0x1011BDB24")]
		public bool OpenTipsWindow(Action onComplete)
		{
			return default(bool);
		}

		// Token: 0x06001D0F RID: 7439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A8D")]
		[Address(RVA = "0x1011BCFE8", Offset = "0x11BCFE8", VA = "0x1011BCFE8")]
		private void UpdateAttentionBadge()
		{
		}

		// Token: 0x06001D10 RID: 7440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A8E")]
		[Address(RVA = "0x1011BDC28", Offset = "0x11BDC28", VA = "0x1011BDC28")]
		private void StartShowUI()
		{
		}

		// Token: 0x06001D11 RID: 7441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A8F")]
		[Address(RVA = "0x1011BDC6C", Offset = "0x11BDC6C", VA = "0x1011BDC6C")]
		private void StartHideUI()
		{
		}

		// Token: 0x06001D12 RID: 7442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A90")]
		[Address(RVA = "0x1011BDCB0", Offset = "0x11BDCB0", VA = "0x1011BDCB0")]
		private void OnResponseOfferOrderAPI()
		{
		}

		// Token: 0x06001D13 RID: 7443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A91")]
		[Address(RVA = "0x1011BDD58", Offset = "0x11BDD58", VA = "0x1011BDD58")]
		private void OnResponseOfferCompleteAPI()
		{
		}

		// Token: 0x06001D14 RID: 7444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A92")]
		[Address(RVA = "0x1011BDD98", Offset = "0x11BDD98", VA = "0x1011BDD98")]
		private void OnResponseSubOfferAPI()
		{
		}

		// Token: 0x06001D15 RID: 7445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A93")]
		[Address(RVA = "0x1011BDDD8", Offset = "0x11BDDD8", VA = "0x1011BDDD8")]
		private void OnResponseOfferAPI_Failed(ResultCode resultCode)
		{
		}

		// Token: 0x06001D16 RID: 7446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A94")]
		[Address(RVA = "0x1011BDDF0", Offset = "0x11BDDF0", VA = "0x1011BDDF0")]
		public void ReplaceRoomInterior(bool isReady, bool isSkip, Action<int> callback)
		{
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A95")]
		[Address(RVA = "0x1011BDE3C", Offset = "0x11BDE3C", VA = "0x1011BDE3C")]
		public void ReplaceRoomMember(long[] charaMngId, Action finalCallback)
		{
		}

		// Token: 0x06001D18 RID: 7448 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A96")]
		[Address(RVA = "0x1011BDE80", Offset = "0x11BDE80", VA = "0x1011BDE80")]
		public long[] GetRoomMember()
		{
			return null;
		}

		// Token: 0x06001D19 RID: 7449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001A97")]
		[Address(RVA = "0x1011BDEBC", Offset = "0x11BDEBC", VA = "0x1011BDEBC")]
		public void SetFlgPlayScriptSoundForUI(bool flag)
		{
		}

		// Token: 0x06001D1A RID: 7450 RVA: 0x0000CFD8 File Offset: 0x0000B1D8
		[Token(Token = "0x6001A98")]
		[Address(RVA = "0x1011BDEF0", Offset = "0x11BDEF0", VA = "0x1011BDEF0")]
		public bool IsHaveItem()
		{
			return default(bool);
		}

		// Token: 0x06001D1B RID: 7451 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A99")]
		[Address(RVA = "0x1011BDF10", Offset = "0x11BDF10", VA = "0x1011BDF10")]
		public List<int> GetNamedList()
		{
			return null;
		}

		// Token: 0x06001D1C RID: 7452 RVA: 0x0000CFF0 File Offset: 0x0000B1F0
		[Token(Token = "0x6001A9A")]
		[Address(RVA = "0x1011BDF3C", Offset = "0x11BDF3C", VA = "0x1011BDF3C")]
		public int GetNamedIndex(eCharaNamedType namedType)
		{
			return 0;
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A9B")]
		[Address(RVA = "0x1011BDF70", Offset = "0x11BDF70", VA = "0x1011BDF70")]
		public List<int> GetCharacterIDList(int namedType)
		{
			return null;
		}

		// Token: 0x06001D1E RID: 7454 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A9C")]
		[Address(RVA = "0x1011BDFA4", Offset = "0x11BDFA4", VA = "0x1011BDFA4")]
		public List<OfferManager.OfferData> GetSubOfferDataList(int charaID)
		{
			return null;
		}

		// Token: 0x06001D1F RID: 7455 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001A9D")]
		[Address(RVA = "0x1011BDFD8", Offset = "0x11BDFD8", VA = "0x1011BDFD8")]
		public OfferManager.OfferData GetSubOfferData(long mngID)
		{
			return null;
		}

		// Token: 0x06001D20 RID: 7456 RVA: 0x0000D008 File Offset: 0x0000B208
		[Token(Token = "0x6001A9E")]
		[Address(RVA = "0x1011BE00C", Offset = "0x11BE00C", VA = "0x1011BE00C")]
		public int GetBadgeCountNamed(int namedType)
		{
			return 0;
		}

		// Token: 0x06001D21 RID: 7457 RVA: 0x0000D020 File Offset: 0x0000B220
		[Token(Token = "0x6001A9F")]
		[Address(RVA = "0x1011BE040", Offset = "0x11BE040", VA = "0x1011BE040")]
		public int GetBadgeCountCharacter(int characterID, bool isHaveItem = true)
		{
			return 0;
		}

		// Token: 0x06001D22 RID: 7458 RVA: 0x0000D038 File Offset: 0x0000B238
		[Token(Token = "0x6001AA0")]
		[Address(RVA = "0x1011BE084", Offset = "0x11BE084", VA = "0x1011BE084")]
		public bool IsHaveCharacter(int charaID)
		{
			return default(bool);
		}

		// Token: 0x04002D30 RID: 11568
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400238F")]
		private ContentRoomState_Main.eStep m_Step;

		// Token: 0x04002D31 RID: 11569
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002390")]
		private ContentRoomState_Main.eOfferEffectType m_OfferEffectType;

		// Token: 0x04002D32 RID: 11570
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002391")]
		private long m_OfferEffectMngId;

		// Token: 0x04002D33 RID: 11571
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002392")]
		private bool m_ResponseOfferAPI;

		// Token: 0x04002D34 RID: 11572
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002393")]
		private int m_TransitQuestID;

		// Token: 0x04002D35 RID: 11573
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002394")]
		private bool m_TransitHideUIMode;

		// Token: 0x04002D36 RID: 11574
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002395")]
		private ContentRoomState_Main.eOccurredEffectType m_IsOccurredOfferEffect;

		// Token: 0x04002D37 RID: 11575
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002396")]
		private long m_OccurredCharaOfferMngID;

		// Token: 0x04002D38 RID: 11576
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002397")]
		private ContentRoomUI m_UI;

		// Token: 0x04002D39 RID: 11577
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002398")]
		private OfferEffectSequencer m_OfferEffectSequencer;

		// Token: 0x04002D3A RID: 11578
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002399")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x02000782 RID: 1922
		[Token(Token = "0x2000E8E")]
		private enum eStep
		{
			// Token: 0x04002D3C RID: 11580
			[Token(Token = "0x4005D18")]
			None,
			// Token: 0x04002D3D RID: 11581
			[Token(Token = "0x4005D19")]
			First,
			// Token: 0x04002D3E RID: 11582
			[Token(Token = "0x4005D1A")]
			LoadWait,
			// Token: 0x04002D3F RID: 11583
			[Token(Token = "0x4005D1B")]
			Setup,
			// Token: 0x04002D40 RID: 11584
			[Token(Token = "0x4005D1C")]
			Main,
			// Token: 0x04002D41 RID: 11585
			[Token(Token = "0x4005D1D")]
			WaitAPI,
			// Token: 0x04002D42 RID: 11586
			[Token(Token = "0x4005D1E")]
			Effect,
			// Token: 0x04002D43 RID: 11587
			[Token(Token = "0x4005D1F")]
			ClosingUI,
			// Token: 0x04002D44 RID: 11588
			[Token(Token = "0x4005D20")]
			HideUI,
			// Token: 0x04002D45 RID: 11589
			[Token(Token = "0x4005D21")]
			OpeningUI,
			// Token: 0x04002D46 RID: 11590
			[Token(Token = "0x4005D22")]
			UnloadResource,
			// Token: 0x04002D47 RID: 11591
			[Token(Token = "0x4005D23")]
			UnloadResourceWait,
			// Token: 0x04002D48 RID: 11592
			[Token(Token = "0x4005D24")]
			End
		}

		// Token: 0x02000783 RID: 1923
		[Token(Token = "0x2000E8F")]
		private enum eOfferEffectType
		{
			// Token: 0x04002D4A RID: 11594
			[Token(Token = "0x4005D26")]
			None,
			// Token: 0x04002D4B RID: 11595
			[Token(Token = "0x4005D27")]
			Accept,
			// Token: 0x04002D4C RID: 11596
			[Token(Token = "0x4005D28")]
			Complete,
			// Token: 0x04002D4D RID: 11597
			[Token(Token = "0x4005D29")]
			AcceptSub,
			// Token: 0x04002D4E RID: 11598
			[Token(Token = "0x4005D2A")]
			CompleteSub
		}

		// Token: 0x02000784 RID: 1924
		[Token(Token = "0x2000E90")]
		public enum eOccurredEffectType
		{
			// Token: 0x04002D50 RID: 11600
			[Token(Token = "0x4005D2C")]
			None,
			// Token: 0x04002D51 RID: 11601
			[Token(Token = "0x4005D2D")]
			MainOffer,
			// Token: 0x04002D52 RID: 11602
			[Token(Token = "0x4005D2E")]
			SubOffer
		}
	}
}
