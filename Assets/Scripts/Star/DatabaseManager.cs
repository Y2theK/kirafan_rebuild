﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004DE RID: 1246
	[Token(Token = "0x20003D6")]
	[StructLayout(3)]
	public sealed class DatabaseManager
	{
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x0600140D RID: 5133 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600140E RID: 5134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000181")]
		public AssetBundleDownloadListDB ABDLDB_First
		{
			[Token(Token = "0x60012C2")]
			[Address(RVA = "0x1011BE20C", Offset = "0x11BE20C", VA = "0x1011BE20C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012C3")]
			[Address(RVA = "0x1011BE214", Offset = "0x11BE214", VA = "0x1011BE214")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x0600140F RID: 5135 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001410 RID: 5136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000182")]
		public AssetBundleDownloadListDB ABDLDB_Always
		{
			[Token(Token = "0x60012C4")]
			[Address(RVA = "0x1011BE21C", Offset = "0x11BE21C", VA = "0x1011BE21C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012C5")]
			[Address(RVA = "0x1011BE224", Offset = "0x11BE224", VA = "0x1011BE224")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06001411 RID: 5137 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001412 RID: 5138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000183")]
		public CRIFileVersionDB CRIFileVersionDB
		{
			[Token(Token = "0x60012C6")]
			[Address(RVA = "0x1011BE22C", Offset = "0x11BE22C", VA = "0x1011BE22C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012C7")]
			[Address(RVA = "0x1011BE234", Offset = "0x11BE234", VA = "0x1011BE234")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06001413 RID: 5139 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001414 RID: 5140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000184")]
		public CacheCleanTargetsDB CacheCleanTargetsDB
		{
			[Token(Token = "0x60012C8")]
			[Address(RVA = "0x1011BE23C", Offset = "0x11BE23C", VA = "0x1011BE23C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012C9")]
			[Address(RVA = "0x1011BE244", Offset = "0x11BE244", VA = "0x1011BE244")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06001415 RID: 5141 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001416 RID: 5142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000185")]
		public StarDefineDB StarDefineDB
		{
			[Token(Token = "0x60012CA")]
			[Address(RVA = "0x1011AF93C", Offset = "0x11AF93C", VA = "0x1011AF93C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012CB")]
			[Address(RVA = "0x1011BE24C", Offset = "0x11BE24C", VA = "0x1011BE24C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06001417 RID: 5143 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001418 RID: 5144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000186")]
		public Text_CommonDB TextCmnDB
		{
			[Token(Token = "0x60012CC")]
			[Address(RVA = "0x1011BE254", Offset = "0x11BE254", VA = "0x1011BE254")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012CD")]
			[Address(RVA = "0x1011BE25C", Offset = "0x11BE25C", VA = "0x1011BE25C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06001419 RID: 5145 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600141A RID: 5146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000187")]
		public Text_MessageDB TextMessageDB
		{
			[Token(Token = "0x60012CE")]
			[Address(RVA = "0x1011BE264", Offset = "0x11BE264", VA = "0x1011BE264")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012CF")]
			[Address(RVA = "0x1011BE26C", Offset = "0x11BE26C", VA = "0x1011BE26C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x0600141B RID: 5147 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600141C RID: 5148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000188")]
		public TipsDB TipsLoadingDB
		{
			[Token(Token = "0x60012D0")]
			[Address(RVA = "0x1011BE274", Offset = "0x11BE274", VA = "0x1011BE274")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012D1")]
			[Address(RVA = "0x1011BE27C", Offset = "0x11BE27C", VA = "0x1011BE27C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x0600141D RID: 5149 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600141E RID: 5150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000189")]
		public MoviePlayListDB MoviePlayListDB
		{
			[Token(Token = "0x60012D2")]
			[Address(RVA = "0x1011BE284", Offset = "0x11BE284", VA = "0x1011BE284")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012D3")]
			[Address(RVA = "0x1011BE28C", Offset = "0x11BE28C", VA = "0x1011BE28C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x0600141F RID: 5151 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001420 RID: 5152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018A")]
		public SoundCueListDB SoundCueListDB
		{
			[Token(Token = "0x60012D4")]
			[Address(RVA = "0x1011B1084", Offset = "0x11B1084", VA = "0x1011B1084")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012D5")]
			[Address(RVA = "0x1011BE294", Offset = "0x11BE294", VA = "0x1011BE294")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06001421 RID: 5153 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001422 RID: 5154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018B")]
		public SoundCueSheetDB SoundCueSheetDB
		{
			[Token(Token = "0x60012D6")]
			[Address(RVA = "0x1011B0C38", Offset = "0x11B0C38", VA = "0x1011B0C38")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012D7")]
			[Address(RVA = "0x1011BE29C", Offset = "0x11BE29C", VA = "0x1011BE29C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06001423 RID: 5155 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001424 RID: 5156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018C")]
		public SoundSeListDB SoundSeListDB
		{
			[Token(Token = "0x60012D8")]
			[Address(RVA = "0x1011BE2A4", Offset = "0x11BE2A4", VA = "0x1011BE2A4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012D9")]
			[Address(RVA = "0x1011BE2AC", Offset = "0x11BE2AC", VA = "0x1011BE2AC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06001425 RID: 5157 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001426 RID: 5158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018D")]
		public SoundBgmListDB SoundBgmListDB
		{
			[Token(Token = "0x60012DA")]
			[Address(RVA = "0x1011BE2B4", Offset = "0x11BE2B4", VA = "0x1011BE2B4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012DB")]
			[Address(RVA = "0x1011BE2BC", Offset = "0x11BE2BC", VA = "0x1011BE2BC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06001427 RID: 5159 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001428 RID: 5160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018E")]
		public SoundVoiceListDB SoundVoiceListDB
		{
			[Token(Token = "0x60012DC")]
			[Address(RVA = "0x1011BE2C4", Offset = "0x11BE2C4", VA = "0x1011BE2C4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012DD")]
			[Address(RVA = "0x1011BE2CC", Offset = "0x11BE2CC", VA = "0x1011BE2CC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x06001429 RID: 5161 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600142A RID: 5162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700018F")]
		public SoundVoiceControllListDB SoundVoiceControllListDB
		{
			[Token(Token = "0x60012DE")]
			[Address(RVA = "0x1011BE2D4", Offset = "0x11BE2D4", VA = "0x1011BE2D4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012DF")]
			[Address(RVA = "0x1011BE2DC", Offset = "0x11BE2DC", VA = "0x1011BE2DC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x0600142B RID: 5163 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600142C RID: 5164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000190")]
		public SoundHomeBgmListDB SoundHomeBgmListDB
		{
			[Token(Token = "0x60012E0")]
			[Address(RVA = "0x1011BE2E4", Offset = "0x11BE2E4", VA = "0x1011BE2E4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012E1")]
			[Address(RVA = "0x1011BE2EC", Offset = "0x11BE2EC", VA = "0x1011BE2EC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x0600142D RID: 5165 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600142E RID: 5166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000191")]
		public CharacterListDB CharaListDB
		{
			[Token(Token = "0x60012E2")]
			[Address(RVA = "0x1011BE2F4", Offset = "0x11BE2F4", VA = "0x1011BE2F4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012E3")]
			[Address(RVA = "0x1011BE2FC", Offset = "0x11BE2FC", VA = "0x1011BE2FC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x0600142F RID: 5167 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001430 RID: 5168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000192")]
		public CharacterOverrideDB CharaOverrideDB
		{
			[Token(Token = "0x60012E4")]
			[Address(RVA = "0x1011BE304", Offset = "0x11BE304", VA = "0x1011BE304")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012E5")]
			[Address(RVA = "0x1011BE30C", Offset = "0x11BE30C", VA = "0x1011BE30C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x06001431 RID: 5169 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001432 RID: 5170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000193")]
		public CharacterWeaponOverrideDB CharaWeaponOverrideDB
		{
			[Token(Token = "0x60012E6")]
			[Address(RVA = "0x1011BE314", Offset = "0x11BE314", VA = "0x1011BE314")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012E7")]
			[Address(RVA = "0x1011BE31C", Offset = "0x11BE31C", VA = "0x1011BE31C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06001433 RID: 5171 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001434 RID: 5172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000194")]
		public ClassListDB ClassListDB
		{
			[Token(Token = "0x60012E8")]
			[Address(RVA = "0x1011BE324", Offset = "0x11BE324", VA = "0x1011BE324")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012E9")]
			[Address(RVA = "0x1011BE32C", Offset = "0x11BE32C", VA = "0x1011BE32C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06001435 RID: 5173 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001436 RID: 5174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000195")]
		public NamedListDB NamedListDB
		{
			[Token(Token = "0x60012EA")]
			[Address(RVA = "0x1011BE334", Offset = "0x11BE334", VA = "0x1011BE334")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012EB")]
			[Address(RVA = "0x1011BE33C", Offset = "0x11BE33C", VA = "0x1011BE33C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06001437 RID: 5175 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001438 RID: 5176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000196")]
		public TitleListDB TitleListDB
		{
			[Token(Token = "0x60012EC")]
			[Address(RVA = "0x1011BCFE0", Offset = "0x11BCFE0", VA = "0x1011BCFE0")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012ED")]
			[Address(RVA = "0x1011BE344", Offset = "0x11BE344", VA = "0x1011BE344")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06001439 RID: 5177 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600143A RID: 5178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000197")]
		public WeaponListDB WeaponListDB
		{
			[Token(Token = "0x60012EE")]
			[Address(RVA = "0x1011BE34C", Offset = "0x11BE34C", VA = "0x1011BE34C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012EF")]
			[Address(RVA = "0x1011BE354", Offset = "0x11BE354", VA = "0x1011BE354")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x0600143B RID: 5179 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600143C RID: 5180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000198")]
		public WeaponRecipeListDB WeaponRecipeListDB
		{
			[Token(Token = "0x60012F0")]
			[Address(RVA = "0x1011BE35C", Offset = "0x11BE35C", VA = "0x1011BE35C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012F1")]
			[Address(RVA = "0x1011BE364", Offset = "0x11BE364", VA = "0x1011BE364")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x0600143D RID: 5181 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600143E RID: 5182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000199")]
		public WeaponEvolutionListDB WeaponEvolutionListDB
		{
			[Token(Token = "0x60012F2")]
			[Address(RVA = "0x1011BE36C", Offset = "0x11BE36C", VA = "0x1011BE36C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012F3")]
			[Address(RVA = "0x1011BE374", Offset = "0x11BE374", VA = "0x1011BE374")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x0600143F RID: 5183 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001440 RID: 5184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019A")]
		public CharacterWeaponListDB CharacterWeaponListDB
		{
			[Token(Token = "0x60012F4")]
			[Address(RVA = "0x1011BE37C", Offset = "0x11BE37C", VA = "0x1011BE37C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012F5")]
			[Address(RVA = "0x1011BE384", Offset = "0x11BE384", VA = "0x1011BE384")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06001441 RID: 5185 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001442 RID: 5186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019B")]
		public CharacterLimitBreakListDB CharaLimitBreakListDB
		{
			[Token(Token = "0x60012F6")]
			[Address(RVA = "0x1011BE38C", Offset = "0x11BE38C", VA = "0x1011BE38C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012F7")]
			[Address(RVA = "0x1011BE394", Offset = "0x11BE394", VA = "0x1011BE394")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06001443 RID: 5187 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001444 RID: 5188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019C")]
		public CharacterEvolutionListDB CharaEvolutionListDB
		{
			[Token(Token = "0x60012F8")]
			[Address(RVA = "0x1011BE39C", Offset = "0x11BE39C", VA = "0x1011BE39C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012F9")]
			[Address(RVA = "0x1011BE3A4", Offset = "0x11BE3A4", VA = "0x1011BE3A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06001445 RID: 5189 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001446 RID: 5190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019D")]
		public ArousalLevelsDB ArousalLevelsDB
		{
			[Token(Token = "0x60012FA")]
			[Address(RVA = "0x1011BE3AC", Offset = "0x11BE3AC", VA = "0x1011BE3AC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012FB")]
			[Address(RVA = "0x1011BE3B4", Offset = "0x11BE3B4", VA = "0x1011BE3B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06001447 RID: 5191 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001448 RID: 5192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019E")]
		public SkillActionConvertsDB SkillActionConvertsDB_SAP
		{
			[Token(Token = "0x60012FC")]
			[Address(RVA = "0x1011BE3BC", Offset = "0x11BE3BC", VA = "0x1011BE3BC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012FD")]
			[Address(RVA = "0x1011BE3C4", Offset = "0x11BE3C4", VA = "0x1011BE3C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06001449 RID: 5193 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600144A RID: 5194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700019F")]
		public SkillActionConvertsDB SkillActionConvertsDB_SAG
		{
			[Token(Token = "0x60012FE")]
			[Address(RVA = "0x1011BE3CC", Offset = "0x11BE3CC", VA = "0x1011BE3CC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x60012FF")]
			[Address(RVA = "0x1011BE3D4", Offset = "0x11BE3D4", VA = "0x1011BE3D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x0600144B RID: 5195 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600144C RID: 5196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A0")]
		public SkillLvCoefDB SkillLvCoefDB
		{
			[Token(Token = "0x6001300")]
			[Address(RVA = "0x1011BE3DC", Offset = "0x11BE3DC", VA = "0x1011BE3DC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001301")]
			[Address(RVA = "0x1011BE3E4", Offset = "0x11BE3E4", VA = "0x1011BE3E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x0600144D RID: 5197 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600144E RID: 5198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A1")]
		public SkillContentAndEffectCombiDB SkillContentAndEffectCombiDB
		{
			[Token(Token = "0x6001302")]
			[Address(RVA = "0x1011BE3EC", Offset = "0x11BE3EC", VA = "0x1011BE3EC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001303")]
			[Address(RVA = "0x1011BE3F4", Offset = "0x11BE3F4", VA = "0x1011BE3F4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x0600144F RID: 5199 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001450 RID: 5200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A2")]
		public UniqueSkillSoundDB UniqueSkillSoundDB
		{
			[Token(Token = "0x6001304")]
			[Address(RVA = "0x1011BE3FC", Offset = "0x11BE3FC", VA = "0x1011BE3FC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001305")]
			[Address(RVA = "0x1011BE404", Offset = "0x11BE404", VA = "0x1011BE404")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06001451 RID: 5201 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001452 RID: 5202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A3")]
		public BattleDefineDB BattleDefineDB
		{
			[Token(Token = "0x6001306")]
			[Address(RVA = "0x1011BE40C", Offset = "0x11BE40C", VA = "0x1011BE40C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001307")]
			[Address(RVA = "0x1011BE414", Offset = "0x11BE414", VA = "0x1011BE414")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06001453 RID: 5203 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001454 RID: 5204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A4")]
		public TogetherChargeDefineDB TogetherChargeDefineDB
		{
			[Token(Token = "0x6001308")]
			[Address(RVA = "0x1011BE41C", Offset = "0x11BE41C", VA = "0x1011BE41C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001309")]
			[Address(RVA = "0x1011BE424", Offset = "0x11BE424", VA = "0x1011BE424")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06001455 RID: 5205 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001456 RID: 5206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A5")]
		public BattleStatusRatioByHpDB BattleStatusRatioByHpDB
		{
			[Token(Token = "0x600130A")]
			[Address(RVA = "0x1011BE42C", Offset = "0x11BE42C", VA = "0x1011BE42C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600130B")]
			[Address(RVA = "0x1011BE434", Offset = "0x11BE434", VA = "0x1011BE434")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06001457 RID: 5207 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001458 RID: 5208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A6")]
		public BattleRandomStatusChangeDB BattleRandomStatusChangeDB
		{
			[Token(Token = "0x600130C")]
			[Address(RVA = "0x1011BE43C", Offset = "0x11BE43C", VA = "0x1011BE43C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600130D")]
			[Address(RVA = "0x1011BE444", Offset = "0x11BE444", VA = "0x1011BE444")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06001459 RID: 5209 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600145A RID: 5210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A7")]
		public GachaCutInListDB GachaCutInListDB
		{
			[Token(Token = "0x600130E")]
			[Address(RVA = "0x1011BE44C", Offset = "0x11BE44C", VA = "0x1011BE44C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600130F")]
			[Address(RVA = "0x1011BE454", Offset = "0x11BE454", VA = "0x1011BE454")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x0600145B RID: 5211 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600145C RID: 5212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A8")]
		public GachaItemLabelListDB GachaItemLabelListDB
		{
			[Token(Token = "0x6001310")]
			[Address(RVA = "0x1011BE45C", Offset = "0x11BE45C", VA = "0x1011BE45C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001311")]
			[Address(RVA = "0x1011BE464", Offset = "0x11BE464", VA = "0x1011BE464")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x0600145D RID: 5213 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600145E RID: 5214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001A9")]
		public CharacterFacialDB CharaFacialDB
		{
			[Token(Token = "0x6001312")]
			[Address(RVA = "0x1011BE46C", Offset = "0x11BE46C", VA = "0x1011BE46C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001313")]
			[Address(RVA = "0x1011BE474", Offset = "0x11BE474", VA = "0x1011BE474")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600145F RID: 5215 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001460 RID: 5216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AA")]
		public CharacterFlavorTextDB CharaFlavorTextDB
		{
			[Token(Token = "0x6001314")]
			[Address(RVA = "0x1011BE47C", Offset = "0x11BE47C", VA = "0x1011BE47C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001315")]
			[Address(RVA = "0x1011BE484", Offset = "0x11BE484", VA = "0x1011BE484")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06001461 RID: 5217 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001462 RID: 5218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AB")]
		public ItemListDB ItemListDB
		{
			[Token(Token = "0x6001316")]
			[Address(RVA = "0x1011BE48C", Offset = "0x11BE48C", VA = "0x1011BE48C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001317")]
			[Address(RVA = "0x1011BE494", Offset = "0x11BE494", VA = "0x1011BE494")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06001463 RID: 5219 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001464 RID: 5220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AC")]
		public MasterOrbListDB MasterOrbListDB
		{
			[Token(Token = "0x6001318")]
			[Address(RVA = "0x1011BE49C", Offset = "0x11BE49C", VA = "0x1011BE49C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001319")]
			[Address(RVA = "0x1011BE4A4", Offset = "0x11BE4A4", VA = "0x1011BE4A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06001465 RID: 5221 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001466 RID: 5222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AD")]
		public MasterOrbBuffsDB MasterOrbBuffsDB
		{
			[Token(Token = "0x600131A")]
			[Address(RVA = "0x1011BE4AC", Offset = "0x11BE4AC", VA = "0x1011BE4AC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600131B")]
			[Address(RVA = "0x1011BE4B4", Offset = "0x11BE4B4", VA = "0x1011BE4B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06001467 RID: 5223 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001468 RID: 5224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AE")]
		public AbilityBoardsDB AbilityBoardsDB
		{
			[Token(Token = "0x600131C")]
			[Address(RVA = "0x1011BE4BC", Offset = "0x11BE4BC", VA = "0x1011BE4BC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600131D")]
			[Address(RVA = "0x1011BE4C4", Offset = "0x11BE4C4", VA = "0x1011BE4C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06001469 RID: 5225 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600146A RID: 5226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001AF")]
		public AbilitySlotsDB AbilitySlotsDB
		{
			[Token(Token = "0x600131E")]
			[Address(RVA = "0x1011BE4CC", Offset = "0x11BE4CC", VA = "0x1011BE4CC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600131F")]
			[Address(RVA = "0x1011BE4D4", Offset = "0x11BE4D4", VA = "0x1011BE4D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x0600146B RID: 5227 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600146C RID: 5228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B0")]
		public AbilitySpheresDB AbilitySpheresDB
		{
			[Token(Token = "0x6001320")]
			[Address(RVA = "0x1011BE4DC", Offset = "0x11BE4DC", VA = "0x1011BE4DC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001321")]
			[Address(RVA = "0x1011BE4E4", Offset = "0x11BE4E4", VA = "0x1011BE4E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x0600146D RID: 5229 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600146E RID: 5230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B1")]
		public AbilitySlotItemRecipesDB AbilitySlotItemRecipesDB
		{
			[Token(Token = "0x6001322")]
			[Address(RVA = "0x1011BE4EC", Offset = "0x11BE4EC", VA = "0x1011BE4EC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001323")]
			[Address(RVA = "0x1011BE4F4", Offset = "0x11BE4F4", VA = "0x1011BE4F4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600146F RID: 5231 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001470 RID: 5232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B2")]
		public AbilitySphereRecipesDB AbilitySphereRecipesDB
		{
			[Token(Token = "0x6001324")]
			[Address(RVA = "0x1011BE4FC", Offset = "0x11BE4FC", VA = "0x1011BE4FC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001325")]
			[Address(RVA = "0x1011BE504", Offset = "0x11BE504", VA = "0x1011BE504")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06001471 RID: 5233 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001472 RID: 5234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B3")]
		public CharacterQuestListDB CharaQuestListDB
		{
			[Token(Token = "0x6001326")]
			[Address(RVA = "0x1011BE50C", Offset = "0x11BE50C", VA = "0x1011BE50C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001327")]
			[Address(RVA = "0x1011BE514", Offset = "0x11BE514", VA = "0x1011BE514")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06001473 RID: 5235 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001474 RID: 5236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B4")]
		public EventsDB EventsDB
		{
			[Token(Token = "0x6001328")]
			[Address(RVA = "0x1011BE51C", Offset = "0x11BE51C", VA = "0x1011BE51C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001329")]
			[Address(RVA = "0x1011BE524", Offset = "0x11BE524", VA = "0x1011BE524")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06001475 RID: 5237 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001476 RID: 5238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B5")]
		public EventGroupsDB EventGroupsDB
		{
			[Token(Token = "0x600132A")]
			[Address(RVA = "0x1011BE52C", Offset = "0x11BE52C", VA = "0x1011BE52C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600132B")]
			[Address(RVA = "0x1011BE534", Offset = "0x11BE534", VA = "0x1011BE534")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06001477 RID: 5239 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001478 RID: 5240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B6")]
		public EventQuestDropExtDB EventQuestDropExtDB
		{
			[Token(Token = "0x600132C")]
			[Address(RVA = "0x1011BE53C", Offset = "0x11BE53C", VA = "0x1011BE53C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600132D")]
			[Address(RVA = "0x1011BE544", Offset = "0x11BE544", VA = "0x1011BE544")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06001479 RID: 5241 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600147A RID: 5242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B7")]
		public EventQuestUISettingDB EventQuestUISettingDB
		{
			[Token(Token = "0x600132E")]
			[Address(RVA = "0x1011BE54C", Offset = "0x11BE54C", VA = "0x1011BE54C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600132F")]
			[Address(RVA = "0x1011BE554", Offset = "0x11BE554", VA = "0x1011BE554")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x0600147B RID: 5243 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600147C RID: 5244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B8")]
		public ChapterUISettingDB ChapterUISettingDB
		{
			[Token(Token = "0x6001330")]
			[Address(RVA = "0x1011BE55C", Offset = "0x11BE55C", VA = "0x1011BE55C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001331")]
			[Address(RVA = "0x1011BE564", Offset = "0x11BE564", VA = "0x1011BE564")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x0600147D RID: 5245 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600147E RID: 5246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001B9")]
		public QuestWaveListDB QuestWaveListDB
		{
			[Token(Token = "0x6001332")]
			[Address(RVA = "0x1011BE56C", Offset = "0x11BE56C", VA = "0x1011BE56C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001333")]
			[Address(RVA = "0x1011BE574", Offset = "0x11BE574", VA = "0x1011BE574")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x0600147F RID: 5247 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001480 RID: 5248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BA")]
		public QuestWaveRandomListDB QuestWaveRandomListDB
		{
			[Token(Token = "0x6001334")]
			[Address(RVA = "0x1011BE57C", Offset = "0x11BE57C", VA = "0x1011BE57C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001335")]
			[Address(RVA = "0x1011BE584", Offset = "0x11BE584", VA = "0x1011BE584")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06001481 RID: 5249 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001482 RID: 5250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BB")]
		public QuestWaveDropsDB QuestWaveDropsDB
		{
			[Token(Token = "0x6001336")]
			[Address(RVA = "0x1011BE58C", Offset = "0x11BE58C", VA = "0x1011BE58C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001337")]
			[Address(RVA = "0x1011BE594", Offset = "0x11BE594", VA = "0x1011BE594")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06001483 RID: 5251 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001484 RID: 5252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BC")]
		public QuestEnemyListDB QuestEnemyListDB
		{
			[Token(Token = "0x6001338")]
			[Address(RVA = "0x1011BE59C", Offset = "0x11BE59C", VA = "0x1011BE59C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001339")]
			[Address(RVA = "0x1011BE5A4", Offset = "0x11BE5A4", VA = "0x1011BE5A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06001485 RID: 5253 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001486 RID: 5254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BD")]
		public QuestADVTriggerDB QuestADVTriggerDB
		{
			[Token(Token = "0x600133A")]
			[Address(RVA = "0x1011BE5AC", Offset = "0x11BE5AC", VA = "0x1011BE5AC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600133B")]
			[Address(RVA = "0x1011BE5B4", Offset = "0x11BE5B4", VA = "0x1011BE5B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06001487 RID: 5255 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001488 RID: 5256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BE")]
		public QuestMapSDPositionDB QuestMapSDPositionDB
		{
			[Token(Token = "0x600133C")]
			[Address(RVA = "0x1011BE5BC", Offset = "0x11BE5BC", VA = "0x1011BE5BC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600133D")]
			[Address(RVA = "0x1011BE5C4", Offset = "0x11BE5C4", VA = "0x1011BE5C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06001489 RID: 5257 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600148A RID: 5258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001BF")]
		public QuestMapResourceListDB QuestMapResourceListDB
		{
			[Token(Token = "0x600133E")]
			[Address(RVA = "0x1011BE5CC", Offset = "0x11BE5CC", VA = "0x1011BE5CC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600133F")]
			[Address(RVA = "0x1011BE5D4", Offset = "0x11BE5D4", VA = "0x1011BE5D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x0600148B RID: 5259 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600148C RID: 5260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C0")]
		public EnemyResourceListDB EnemyResourceListDB
		{
			[Token(Token = "0x6001340")]
			[Address(RVA = "0x1011BE5DC", Offset = "0x11BE5DC", VA = "0x1011BE5DC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001341")]
			[Address(RVA = "0x1011BE5E4", Offset = "0x11BE5E4", VA = "0x1011BE5E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x0600148D RID: 5261 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600148E RID: 5262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C1")]
		public TownObjectListDB TownObjListDB
		{
			[Token(Token = "0x6001342")]
			[Address(RVA = "0x1011BCFD8", Offset = "0x11BCFD8", VA = "0x1011BCFD8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001343")]
			[Address(RVA = "0x1011BE5EC", Offset = "0x11BE5EC", VA = "0x1011BE5EC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x0600148F RID: 5263 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001490 RID: 5264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C2")]
		public TownShopListDB TownShopListDB
		{
			[Token(Token = "0x6001344")]
			[Address(RVA = "0x1011BE5F4", Offset = "0x11BE5F4", VA = "0x1011BE5F4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001345")]
			[Address(RVA = "0x1011BE5FC", Offset = "0x11BE5FC", VA = "0x1011BE5FC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06001491 RID: 5265 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001492 RID: 5266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C3")]
		public TownObjectLevelUpDB TownObjLvUpDB
		{
			[Token(Token = "0x6001346")]
			[Address(RVA = "0x1011BE604", Offset = "0x11BE604", VA = "0x1011BE604")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001347")]
			[Address(RVA = "0x1011BE60C", Offset = "0x11BE60C", VA = "0x1011BE60C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06001493 RID: 5267 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001494 RID: 5268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C4")]
		public TownObjectBuildSubCodeDB TownObjBuildSubDB
		{
			[Token(Token = "0x6001348")]
			[Address(RVA = "0x1011BE614", Offset = "0x11BE614", VA = "0x1011BE614")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001349")]
			[Address(RVA = "0x1011BE61C", Offset = "0x11BE61C", VA = "0x1011BE61C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x06001495 RID: 5269 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001496 RID: 5270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C5")]
		public TownObjectBuffDB TownObjBuffDB
		{
			[Token(Token = "0x600134A")]
			[Address(RVA = "0x1011BE624", Offset = "0x11BE624", VA = "0x1011BE624")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600134B")]
			[Address(RVA = "0x1011BE62C", Offset = "0x11BE62C", VA = "0x1011BE62C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06001497 RID: 5271 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001498 RID: 5272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C6")]
		public RoomListDB RoomListDB
		{
			[Token(Token = "0x600134C")]
			[Address(RVA = "0x1011BE634", Offset = "0x11BE634", VA = "0x1011BE634")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600134D")]
			[Address(RVA = "0x1011BE63C", Offset = "0x11BE63C", VA = "0x1011BE63C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06001499 RID: 5273 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600149A RID: 5274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C7")]
		public RoomObjectFilterCategoryDB RoomObjFilterCategoryDB
		{
			[Token(Token = "0x600134E")]
			[Address(RVA = "0x1011BE644", Offset = "0x11BE644", VA = "0x1011BE644")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600134F")]
			[Address(RVA = "0x1011BE64C", Offset = "0x11BE64C", VA = "0x1011BE64C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x0600149B RID: 5275 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600149C RID: 5276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C8")]
		public RoomObjectListDB RoomObjectListDB
		{
			[Token(Token = "0x6001350")]
			[Address(RVA = "0x1011B4CC8", Offset = "0x11B4CC8", VA = "0x1011B4CC8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001351")]
			[Address(RVA = "0x1011BE654", Offset = "0x11BE654", VA = "0x1011BE654")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x0600149D RID: 5277 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600149E RID: 5278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001C9")]
		public RoomShopListDB RoomShopListDB
		{
			[Token(Token = "0x6001352")]
			[Address(RVA = "0x1011BE65C", Offset = "0x11BE65C", VA = "0x1011BE65C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001353")]
			[Address(RVA = "0x1011BE664", Offset = "0x11BE664", VA = "0x1011BE664")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x0600149F RID: 5279 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014A0 RID: 5280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CA")]
		public RoomEnvEffectDB RoomEnvEffectDB
		{
			[Token(Token = "0x6001354")]
			[Address(RVA = "0x1011BE66C", Offset = "0x11BE66C", VA = "0x1011BE66C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001355")]
			[Address(RVA = "0x1011BE674", Offset = "0x11BE674", VA = "0x1011BE674")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x060014A1 RID: 5281 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014A2 RID: 5282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CB")]
		public EffectListDB EffectListDB
		{
			[Token(Token = "0x6001356")]
			[Address(RVA = "0x1011BE67C", Offset = "0x11BE67C", VA = "0x1011BE67C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001357")]
			[Address(RVA = "0x1011BE684", Offset = "0x11BE684", VA = "0x1011BE684")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x060014A3 RID: 5283 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014A4 RID: 5284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CC")]
		public MasterRankDB MasterRankDB
		{
			[Token(Token = "0x6001358")]
			[Address(RVA = "0x1011BE68C", Offset = "0x11BE68C", VA = "0x1011BE68C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001359")]
			[Address(RVA = "0x1011BE694", Offset = "0x11BE694", VA = "0x1011BE694")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x060014A5 RID: 5285 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014A6 RID: 5286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CD")]
		public CharacterExpDB CharaExpDB
		{
			[Token(Token = "0x600135A")]
			[Address(RVA = "0x1011BE69C", Offset = "0x11BE69C", VA = "0x1011BE69C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600135B")]
			[Address(RVA = "0x1011BE6A4", Offset = "0x11BE6A4", VA = "0x1011BE6A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x060014A7 RID: 5287 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014A8 RID: 5288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CE")]
		public NamedFriendshipExpDB NamedFriendshipExpDB
		{
			[Token(Token = "0x600135C")]
			[Address(RVA = "0x1011BE6AC", Offset = "0x11BE6AC", VA = "0x1011BE6AC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600135D")]
			[Address(RVA = "0x1011BE6B4", Offset = "0x11BE6B4", VA = "0x1011BE6B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x060014A9 RID: 5289 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014AA RID: 5290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001CF")]
		public WeaponExpDB WeaponExpDB
		{
			[Token(Token = "0x600135E")]
			[Address(RVA = "0x1011BE6BC", Offset = "0x11BE6BC", VA = "0x1011BE6BC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600135F")]
			[Address(RVA = "0x1011BE6C4", Offset = "0x11BE6C4", VA = "0x1011BE6C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x060014AB RID: 5291 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014AC RID: 5292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D0")]
		public SkillExpDB SkillExpDB
		{
			[Token(Token = "0x6001360")]
			[Address(RVA = "0x1011BE6CC", Offset = "0x11BE6CC", VA = "0x1011BE6CC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001361")]
			[Address(RVA = "0x1011BE6D4", Offset = "0x11BE6D4", VA = "0x1011BE6D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x060014AD RID: 5293 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014AE RID: 5294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D1")]
		public CharacterParamGrowthListDB CharaParamGrowthListDB
		{
			[Token(Token = "0x6001362")]
			[Address(RVA = "0x1011BE6DC", Offset = "0x11BE6DC", VA = "0x1011BE6DC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001363")]
			[Address(RVA = "0x1011BE6E4", Offset = "0x11BE6E4", VA = "0x1011BE6E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x060014AF RID: 5295 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014B0 RID: 5296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D2")]
		public ProfileVoiceListDB ProfileVoiceListDB
		{
			[Token(Token = "0x6001364")]
			[Address(RVA = "0x1011BE6EC", Offset = "0x11BE6EC", VA = "0x1011BE6EC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001365")]
			[Address(RVA = "0x1011BE6F4", Offset = "0x11BE6F4", VA = "0x1011BE6F4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x060014B1 RID: 5297 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014B2 RID: 5298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D3")]
		public FieldItemDropListDB FieldItemDropListDB
		{
			[Token(Token = "0x6001366")]
			[Address(RVA = "0x1011BE6FC", Offset = "0x11BE6FC", VA = "0x1011BE6FC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001367")]
			[Address(RVA = "0x1011BE704", Offset = "0x11BE704", VA = "0x1011BE704")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x060014B3 RID: 5299 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014B4 RID: 5300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D4")]
		public PopUpStateIconListDB PopUpStateIconListDB
		{
			[Token(Token = "0x6001368")]
			[Address(RVA = "0x1011BE70C", Offset = "0x11BE70C", VA = "0x1011BE70C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001369")]
			[Address(RVA = "0x1011BE714", Offset = "0x11BE714", VA = "0x1011BE714")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x060014B5 RID: 5301 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014B6 RID: 5302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D5")]
		public StateIconListDB StateIconListDB
		{
			[Token(Token = "0x600136A")]
			[Address(RVA = "0x1011BE71C", Offset = "0x11BE71C", VA = "0x1011BE71C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600136B")]
			[Address(RVA = "0x1011BE724", Offset = "0x11BE724", VA = "0x1011BE724")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x060014B7 RID: 5303 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014B8 RID: 5304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D6")]
		public CharacterIllustOffsetDB CharaIllustOffsetDB
		{
			[Token(Token = "0x600136C")]
			[Address(RVA = "0x1011BE72C", Offset = "0x11BE72C", VA = "0x1011BE72C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600136D")]
			[Address(RVA = "0x1011BE734", Offset = "0x11BE734", VA = "0x1011BE734")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x060014B9 RID: 5305 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014BA RID: 5306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D7")]
		public ADVListDB ADVListDB
		{
			[Token(Token = "0x600136E")]
			[Address(RVA = "0x1011BE73C", Offset = "0x11BE73C", VA = "0x1011BE73C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600136F")]
			[Address(RVA = "0x1011BE744", Offset = "0x11BE744", VA = "0x1011BE744")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x060014BB RID: 5307 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014BC RID: 5308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D8")]
		public ADVLibraryListDB ADVLibraryListDB
		{
			[Token(Token = "0x6001370")]
			[Address(RVA = "0x1011BE74C", Offset = "0x11BE74C", VA = "0x1011BE74C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001371")]
			[Address(RVA = "0x1011BE754", Offset = "0x11BE754", VA = "0x1011BE754")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x060014BD RID: 5309 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014BE RID: 5310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001D9")]
		public WordLibraryListDB WordLibraryListDB
		{
			[Token(Token = "0x6001372")]
			[Address(RVA = "0x1011BE75C", Offset = "0x11BE75C", VA = "0x1011BE75C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001373")]
			[Address(RVA = "0x1011BE764", Offset = "0x11BE764", VA = "0x1011BE764")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x060014BF RID: 5311 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014C0 RID: 5312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DA")]
		public OriginalCharaLibraryListDB OriginalCharaLibraryListDB
		{
			[Token(Token = "0x6001374")]
			[Address(RVA = "0x1011BE76C", Offset = "0x11BE76C", VA = "0x1011BE76C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001375")]
			[Address(RVA = "0x1011BE774", Offset = "0x11BE774", VA = "0x1011BE774")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x060014C1 RID: 5313 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014C2 RID: 5314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DB")]
		public SceneInfoListDB SceneInfoListDB
		{
			[Token(Token = "0x6001376")]
			[Address(RVA = "0x1011BE77C", Offset = "0x11BE77C", VA = "0x1011BE77C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001377")]
			[Address(RVA = "0x1011BE784", Offset = "0x11BE784", VA = "0x1011BE784")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060014C3 RID: 5315 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014C4 RID: 5316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DC")]
		public TutorialTipsListDB TutorialTipsListDB
		{
			[Token(Token = "0x6001378")]
			[Address(RVA = "0x1011BE78C", Offset = "0x11BE78C", VA = "0x1011BE78C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001379")]
			[Address(RVA = "0x1011BE794", Offset = "0x11BE794", VA = "0x1011BE794")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060014C5 RID: 5317 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014C6 RID: 5318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DD")]
		public RetireTipsListDB RetireTipsListDB
		{
			[Token(Token = "0x600137A")]
			[Address(RVA = "0x1011BE79C", Offset = "0x11BE79C", VA = "0x1011BE79C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600137B")]
			[Address(RVA = "0x1011BE7A4", Offset = "0x11BE7A4", VA = "0x1011BE7A4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060014C7 RID: 5319 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014C8 RID: 5320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DE")]
		public TutorialMessageListDB TutorialMessageListDB
		{
			[Token(Token = "0x600137C")]
			[Address(RVA = "0x1011BE7AC", Offset = "0x11BE7AC", VA = "0x1011BE7AC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600137D")]
			[Address(RVA = "0x1011BE7B4", Offset = "0x11BE7B4", VA = "0x1011BE7B4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060014C9 RID: 5321 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014CA RID: 5322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001DF")]
		public WebDataListDB WebDataListDB
		{
			[Token(Token = "0x600137E")]
			[Address(RVA = "0x1011BE7BC", Offset = "0x11BE7BC", VA = "0x1011BE7BC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600137F")]
			[Address(RVA = "0x1011BE7C4", Offset = "0x11BE7C4", VA = "0x1011BE7C4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060014CB RID: 5323 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014CC RID: 5324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E0")]
		public PackageItemListDB PackageItemListDB
		{
			[Token(Token = "0x6001380")]
			[Address(RVA = "0x1011BE7CC", Offset = "0x11BE7CC", VA = "0x1011BE7CC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001381")]
			[Address(RVA = "0x1011BE7D4", Offset = "0x11BE7D4", VA = "0x1011BE7D4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060014CD RID: 5325 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014CE RID: 5326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E1")]
		public PackageItemContentsDB PackageItemContentsDB
		{
			[Token(Token = "0x6001382")]
			[Address(RVA = "0x1011BE7DC", Offset = "0x11BE7DC", VA = "0x1011BE7DC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001383")]
			[Address(RVA = "0x1011BE7E4", Offset = "0x11BE7E4", VA = "0x1011BE7E4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060014CF RID: 5327 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014D0 RID: 5328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E2")]
		public WeaponSkillUpItemsDB WeaponSkillUpItemsDB
		{
			[Token(Token = "0x6001384")]
			[Address(RVA = "0x1011BE7EC", Offset = "0x11BE7EC", VA = "0x1011BE7EC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001385")]
			[Address(RVA = "0x1011BE7F4", Offset = "0x11BE7F4", VA = "0x1011BE7F4")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060014D1 RID: 5329 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014D2 RID: 5330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E3")]
		public FlavorsDB FlavorsDB
		{
			[Token(Token = "0x6001386")]
			[Address(RVA = "0x1011BE7FC", Offset = "0x11BE7FC", VA = "0x1011BE7FC")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001387")]
			[Address(RVA = "0x1011BE804", Offset = "0x11BE804", VA = "0x1011BE804")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060014D3 RID: 5331 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014D4 RID: 5332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E4")]
		public Dictionary<int, BattleAIData> BattleAIDatas
		{
			[Token(Token = "0x6001388")]
			[Address(RVA = "0x1011BE80C", Offset = "0x11BE80C", VA = "0x1011BE80C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001389")]
			[Address(RVA = "0x1011BE814", Offset = "0x11BE814", VA = "0x1011BE814")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x060014D5 RID: 5333 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014D6 RID: 5334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E5")]
		public Dictionary<string, SkillActionGraphics> SAGs
		{
			[Token(Token = "0x600138A")]
			[Address(RVA = "0x1011BE81C", Offset = "0x11BE81C", VA = "0x1011BE81C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600138B")]
			[Address(RVA = "0x1011BE824", Offset = "0x11BE824", VA = "0x1011BE824")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x060014D7 RID: 5335 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060014D8 RID: 5336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170001E6")]
		public Dictionary<string, SkillActionPlan> SAPs
		{
			[Token(Token = "0x600138C")]
			[Address(RVA = "0x1011BE82C", Offset = "0x11BE82C", VA = "0x1011BE82C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600138D")]
			[Address(RVA = "0x1011BE834", Offset = "0x11BE834", VA = "0x1011BE834")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x060014D9 RID: 5337 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001E7")]
		public PassiveSkillListDB PassiveSkillListDB_PL
		{
			[Token(Token = "0x600138E")]
			[Address(RVA = "0x1011BE83C", Offset = "0x11BE83C", VA = "0x1011BE83C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x060014DA RID: 5338 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001E8")]
		public PassiveSkillListDB PassiveSkillListDB_EN
		{
			[Token(Token = "0x600138F")]
			[Address(RVA = "0x1011BE880", Offset = "0x11BE880", VA = "0x1011BE880")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x060014DB RID: 5339 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001E9")]
		public PassiveSkillListDB PassiveSkillListDB_WPN
		{
			[Token(Token = "0x6001390")]
			[Address(RVA = "0x1011BE8C8", Offset = "0x11BE8C8", VA = "0x1011BE8C8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x060014DC RID: 5340 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001EA")]
		public PassiveSkillListDB PassiveSkillListDB_ABL
		{
			[Token(Token = "0x6001391")]
			[Address(RVA = "0x1011BE910", Offset = "0x11BE910", VA = "0x1011BE910")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x060014DD RID: 5341 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001EB")]
		public SkillListDB SkillListDB_PL
		{
			[Token(Token = "0x6001392")]
			[Address(RVA = "0x1011BE958", Offset = "0x11BE958", VA = "0x1011BE958")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x060014DE RID: 5342 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001EC")]
		public SkillListDB SkillListDB_EN
		{
			[Token(Token = "0x6001393")]
			[Address(RVA = "0x1011BE99C", Offset = "0x11BE99C", VA = "0x1011BE99C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x060014DF RID: 5343 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001ED")]
		public SkillListDB SkillListDB_WPN
		{
			[Token(Token = "0x6001394")]
			[Address(RVA = "0x1011BE9E4", Offset = "0x11BE9E4", VA = "0x1011BE9E4")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x060014E0 RID: 5344 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001EE")]
		public SkillListDB SkillListDB_MST
		{
			[Token(Token = "0x6001395")]
			[Address(RVA = "0x1011BEA2C", Offset = "0x11BEA2C", VA = "0x1011BEA2C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x060014E1 RID: 5345 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001EF")]
		public SkillListDB SkillListDB_CARD
		{
			[Token(Token = "0x6001396")]
			[Address(RVA = "0x1011BEA74", Offset = "0x11BEA74", VA = "0x1011BEA74")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x060014E2 RID: 5346 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001F0")]
		public SkillContentListDB SkillContentListDB_PL
		{
			[Token(Token = "0x6001397")]
			[Address(RVA = "0x1011BEABC", Offset = "0x11BEABC", VA = "0x1011BEABC")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x060014E3 RID: 5347 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001F1")]
		public SkillContentListDB SkillContentListDB_EN
		{
			[Token(Token = "0x6001398")]
			[Address(RVA = "0x1011BEB00", Offset = "0x11BEB00", VA = "0x1011BEB00")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x060014E4 RID: 5348 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001F2")]
		public SkillContentListDB SkillContentListDB_WPN
		{
			[Token(Token = "0x6001399")]
			[Address(RVA = "0x1011BEB48", Offset = "0x11BEB48", VA = "0x1011BEB48")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x060014E5 RID: 5349 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001F3")]
		public SkillContentListDB SkillContentListDB_MST
		{
			[Token(Token = "0x600139A")]
			[Address(RVA = "0x1011BEB90", Offset = "0x11BEB90", VA = "0x1011BEB90")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x060014E6 RID: 5350 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170001F4")]
		public SkillContentListDB SkillContentListDB_CARD
		{
			[Token(Token = "0x600139B")]
			[Address(RVA = "0x1011BEBD8", Offset = "0x11BEBD8", VA = "0x1011BEBD8")]
			get
			{
				return null;
			}
		}

		// Token: 0x060014E7 RID: 5351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600139C")]
		[Address(RVA = "0x1011BEC20", Offset = "0x11BEC20", VA = "0x1011BEC20")]
		public DatabaseManager()
		{
		}

		// Token: 0x060014E8 RID: 5352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600139D")]
		[Address(RVA = "0x1011BECB8", Offset = "0x11BECB8", VA = "0x1011BECB8")]
		public void Update()
		{
		}

		// Token: 0x060014E9 RID: 5353 RVA: 0x00008B20 File Offset: 0x00006D20
		[Token(Token = "0x600139E")]
		[Address(RVA = "0x1011C16F4", Offset = "0x11C16F4", VA = "0x1011C16F4")]
		public bool Prepare()
		{
			return default(bool);
		}

		// Token: 0x060014EA RID: 5354 RVA: 0x00008B38 File Offset: 0x00006D38
		[Token(Token = "0x600139F")]
		[Address(RVA = "0x1011C17DC", Offset = "0x11C17DC", VA = "0x1011C17DC")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x060014EB RID: 5355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013A0")]
		[Address(RVA = "0x1011BECFC", Offset = "0x11BECFC", VA = "0x1011BECFC")]
		private void Preparing()
		{
		}

		// Token: 0x060014EC RID: 5356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013A1")]
		[Address(RVA = "0x1011C17EC", Offset = "0x11C17EC", VA = "0x1011C17EC")]
		private void ParseTextCommonDB()
		{
		}

		// Token: 0x060014ED RID: 5357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013A2")]
		[Address(RVA = "0x1011C1960", Offset = "0x11C1960", VA = "0x1011C1960")]
		private void ParseTextMessageDB()
		{
		}

		// Token: 0x060014EE RID: 5358 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013A3")]
		[Address(RVA = "0x1011B2AB0", Offset = "0x11B2AB0", VA = "0x1011B2AB0")]
		public string GetTextCommon(eText_CommonDB id)
		{
			return null;
		}

		// Token: 0x060014EF RID: 5359 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013A4")]
		[Address(RVA = "0x1011C1CEC", Offset = "0x11C1CEC", VA = "0x1011C1CEC")]
		public string GetTextCommon(eText_CommonDB id, params object[] args)
		{
			return null;
		}

		// Token: 0x060014F0 RID: 5360 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013A5")]
		[Address(RVA = "0x1011AFF88", Offset = "0x11AFF88", VA = "0x1011AFF88")]
		public string GetTextMessage(eText_MessageDB id)
		{
			return null;
		}

		// Token: 0x060014F1 RID: 5361 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013A6")]
		[Address(RVA = "0x1011B0014", Offset = "0x11B0014", VA = "0x1011B0014")]
		public string GetTextMessage(eText_MessageDB id, params object[] args)
		{
			return null;
		}

		// Token: 0x060014F2 RID: 5362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013A7")]
		[Address(RVA = "0x1011C1AD4", Offset = "0x11C1AD4", VA = "0x1011C1AD4")]
		private void ParseCharaWeaponOverrideDB()
		{
		}

		// Token: 0x060014F3 RID: 5363 RVA: 0x00008B50 File Offset: 0x00006D50
		[Token(Token = "0x60013A8")]
		[Address(RVA = "0x1011C1D8C", Offset = "0x11C1D8C", VA = "0x1011C1D8C")]
		public bool IsExistCharacterWeaponOverrideAnim(int charaID)
		{
			return default(bool);
		}

		// Token: 0x060014F4 RID: 5364 RVA: 0x00008B68 File Offset: 0x00006D68
		[Token(Token = "0x60013A9")]
		[Address(RVA = "0x1011C1E04", Offset = "0x11C1E04", VA = "0x1011C1E04")]
		public bool IsExistCharacterWeaponOverrideEffect(int charaID)
		{
			return default(bool);
		}

		// Token: 0x060014F5 RID: 5365 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013AA")]
		[Address(RVA = "0x1011C1E7C", Offset = "0x11C1E7C", VA = "0x1011C1E7C")]
		public TweetListDB GetTweetListDB(eCharaNamedType namedType)
		{
			return null;
		}

		// Token: 0x060014F6 RID: 5366 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013AB")]
		[Address(RVA = "0x1011C1EEC", Offset = "0x11C1EEC", VA = "0x1011C1EEC")]
		public BattleAIData FindBattleAIData(int id)
		{
			return null;
		}

		// Token: 0x060014F7 RID: 5367 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013AC")]
		[Address(RVA = "0x1011C1F90", Offset = "0x11C1F90", VA = "0x1011C1F90")]
		public SkillActionGraphics FindSAG(string sagID)
		{
			return null;
		}

		// Token: 0x060014F8 RID: 5368 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013AD")]
		[Address(RVA = "0x1011C2034", Offset = "0x11C2034", VA = "0x1011C2034")]
		public SkillActionPlan FindSAP(string sapID)
		{
			return null;
		}

		// Token: 0x060014F9 RID: 5369 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60013AE")]
		[Address(RVA = "0x1011B9478", Offset = "0x11B9478", VA = "0x1011B9478")]
		public ContentRoomPresetDB GetContentRoomPresetDB(eTitleType titleType)
		{
			return null;
		}

		// Token: 0x04001781 RID: 6017
		[Token(Token = "0x4001167")]
		public const int IDX_SKILL_PL = 0;

		// Token: 0x04001782 RID: 6018
		[Token(Token = "0x4001168")]
		public const int IDX_SKILL_EN = 1;

		// Token: 0x04001783 RID: 6019
		[Token(Token = "0x4001169")]
		public const int IDX_SKILL_WPN = 2;

		// Token: 0x04001784 RID: 6020
		[Token(Token = "0x400116A")]
		public const int IDX_SKILL_MST = 3;

		// Token: 0x04001785 RID: 6021
		[Token(Token = "0x400116B")]
		public const int IDX_SKILL_CARD = 4;

		// Token: 0x04001786 RID: 6022
		[Token(Token = "0x400116C")]
		public const int IDX_SKILL_NUM = 5;

		// Token: 0x04001787 RID: 6023
		[Token(Token = "0x400116D")]
		public const int IDX_PASSIVE_SKILL_PL = 0;

		// Token: 0x04001788 RID: 6024
		[Token(Token = "0x400116E")]
		public const int IDX_PASSIVE_SKILL_EN = 1;

		// Token: 0x04001789 RID: 6025
		[Token(Token = "0x400116F")]
		public const int IDX_PASSIVE_SKILL_WPN = 2;

		// Token: 0x0400178A RID: 6026
		[Token(Token = "0x4001170")]
		public const int IDX_PASSIVE_SKILL_ABL = 3;

		// Token: 0x0400178B RID: 6027
		[Token(Token = "0x4001171")]
		public const int IDX_PASSIVE_SKILL_NUM = 4;

		// Token: 0x040017AB RID: 6059
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4001191")]
		public PassiveSkillListDB[] PassiveSkillListDB;

		// Token: 0x040017AC RID: 6060
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4001192")]
		public SkillListDB[] SkillListDB;

		// Token: 0x040017AD RID: 6061
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4001193")]
		public SkillContentListDB[] SkillContentListDB;

		// Token: 0x040017F2 RID: 6130
		[Cpp2IlInjected.FieldOffset(Offset = "0x340")]
		[Token(Token = "0x40011D8")]
		private Dictionary<int, TweetListDB> TweetListDBs;

		// Token: 0x040017F6 RID: 6134
		[Cpp2IlInjected.FieldOffset(Offset = "0x360")]
		[Token(Token = "0x40011DC")]
		private Dictionary<int, string> m_TextCommon;

		// Token: 0x040017F7 RID: 6135
		[Cpp2IlInjected.FieldOffset(Offset = "0x368")]
		[Token(Token = "0x40011DD")]
		private Dictionary<int, string> m_TextMessage;

		// Token: 0x040017F8 RID: 6136
		[Cpp2IlInjected.FieldOffset(Offset = "0x370")]
		[Token(Token = "0x40011DE")]
		private Dictionary<int, bool> m_CharaWeaponOverrideAnim;

		// Token: 0x040017F9 RID: 6137
		[Cpp2IlInjected.FieldOffset(Offset = "0x378")]
		[Token(Token = "0x40011DF")]
		private Dictionary<int, bool> m_CharaWeaponOverrideEffect;

		// Token: 0x040017FA RID: 6138
		[Cpp2IlInjected.FieldOffset(Offset = "0x380")]
		[Token(Token = "0x40011E0")]
		private Dictionary<int, ContentRoomPresetDB> ContentRoomPresetDBs;

		// Token: 0x040017FB RID: 6139
		[Token(Token = "0x40011E1")]
		private static string[] DB_OBJ_PATHS;

		// Token: 0x040017FC RID: 6140
		[Token(Token = "0x40011E2")]
		private const string RESOURCE_PATH = "database/database.muast";

		// Token: 0x040017FD RID: 6141
		[Cpp2IlInjected.FieldOffset(Offset = "0x388")]
		[Token(Token = "0x40011E3")]
		private DatabaseManager.ePrepareStep m_PrepareStep;

		// Token: 0x040017FE RID: 6142
		[Cpp2IlInjected.FieldOffset(Offset = "0x390")]
		[Token(Token = "0x40011E4")]
		private ABResourceLoader m_Loader;

		// Token: 0x040017FF RID: 6143
		[Cpp2IlInjected.FieldOffset(Offset = "0x398")]
		[Token(Token = "0x40011E5")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x020004DF RID: 1247
		[Token(Token = "0x2000DDC")]
		private enum eDatabase
		{
			// Token: 0x04001801 RID: 6145
			[Token(Token = "0x400599C")]
			ABDLDB_First,
			// Token: 0x04001802 RID: 6146
			[Token(Token = "0x400599D")]
			ABDLDB_Always,
			// Token: 0x04001803 RID: 6147
			[Token(Token = "0x400599E")]
			CRIFileVersionDB,
			// Token: 0x04001804 RID: 6148
			[Token(Token = "0x400599F")]
			CacheCleanTargetsDB,
			// Token: 0x04001805 RID: 6149
			[Token(Token = "0x40059A0")]
			StarDefineDB,
			// Token: 0x04001806 RID: 6150
			[Token(Token = "0x40059A1")]
			TextCommonDB,
			// Token: 0x04001807 RID: 6151
			[Token(Token = "0x40059A2")]
			TextMessageDB,
			// Token: 0x04001808 RID: 6152
			[Token(Token = "0x40059A3")]
			TipsLoadingDB,
			// Token: 0x04001809 RID: 6153
			[Token(Token = "0x40059A4")]
			MoviePlayListDB,
			// Token: 0x0400180A RID: 6154
			[Token(Token = "0x40059A5")]
			SoundCueListDB,
			// Token: 0x0400180B RID: 6155
			[Token(Token = "0x40059A6")]
			SoundCueSheetDB,
			// Token: 0x0400180C RID: 6156
			[Token(Token = "0x40059A7")]
			SoundSeListDB,
			// Token: 0x0400180D RID: 6157
			[Token(Token = "0x40059A8")]
			SoundBgmListDB,
			// Token: 0x0400180E RID: 6158
			[Token(Token = "0x40059A9")]
			SoundVoiceListDB,
			// Token: 0x0400180F RID: 6159
			[Token(Token = "0x40059AA")]
			SoundVoiceControllListDB,
			// Token: 0x04001810 RID: 6160
			[Token(Token = "0x40059AB")]
			SoundHomeBgmListDB,
			// Token: 0x04001811 RID: 6161
			[Token(Token = "0x40059AC")]
			CharaListDB,
			// Token: 0x04001812 RID: 6162
			[Token(Token = "0x40059AD")]
			CharaOverrideDB,
			// Token: 0x04001813 RID: 6163
			[Token(Token = "0x40059AE")]
			CharaWeaponOverrideDB,
			// Token: 0x04001814 RID: 6164
			[Token(Token = "0x40059AF")]
			ClassListDB,
			// Token: 0x04001815 RID: 6165
			[Token(Token = "0x40059B0")]
			NamedListDB,
			// Token: 0x04001816 RID: 6166
			[Token(Token = "0x40059B1")]
			TitleListDB,
			// Token: 0x04001817 RID: 6167
			[Token(Token = "0x40059B2")]
			WeaponListDB,
			// Token: 0x04001818 RID: 6168
			[Token(Token = "0x40059B3")]
			WeaponRecipeListDB,
			// Token: 0x04001819 RID: 6169
			[Token(Token = "0x40059B4")]
			WeaponEvolutionListDB,
			// Token: 0x0400181A RID: 6170
			[Token(Token = "0x40059B5")]
			CharacterWeaponListDB,
			// Token: 0x0400181B RID: 6171
			[Token(Token = "0x40059B6")]
			CharaLimitBreakListDB,
			// Token: 0x0400181C RID: 6172
			[Token(Token = "0x40059B7")]
			CharaEvolutionListDB,
			// Token: 0x0400181D RID: 6173
			[Token(Token = "0x40059B8")]
			ArousalLevelsDB,
			// Token: 0x0400181E RID: 6174
			[Token(Token = "0x40059B9")]
			SkillActionConvertsDB_SAP,
			// Token: 0x0400181F RID: 6175
			[Token(Token = "0x40059BA")]
			SkillActionConvertsDB_SAG,
			// Token: 0x04001820 RID: 6176
			[Token(Token = "0x40059BB")]
			PassiveSkillListDB_PL,
			// Token: 0x04001821 RID: 6177
			[Token(Token = "0x40059BC")]
			PassiveSkillListDB_EN,
			// Token: 0x04001822 RID: 6178
			[Token(Token = "0x40059BD")]
			PassiveSkillListDB_WPN,
			// Token: 0x04001823 RID: 6179
			[Token(Token = "0x40059BE")]
			PassiveSkillListDB_ABL,
			// Token: 0x04001824 RID: 6180
			[Token(Token = "0x40059BF")]
			SkillListDB_PL,
			// Token: 0x04001825 RID: 6181
			[Token(Token = "0x40059C0")]
			SkillListDB_EN,
			// Token: 0x04001826 RID: 6182
			[Token(Token = "0x40059C1")]
			SkillListDB_WPN,
			// Token: 0x04001827 RID: 6183
			[Token(Token = "0x40059C2")]
			SkillListDB_MST,
			// Token: 0x04001828 RID: 6184
			[Token(Token = "0x40059C3")]
			SkillListDB_CARD,
			// Token: 0x04001829 RID: 6185
			[Token(Token = "0x40059C4")]
			SkillContentListDB_PL,
			// Token: 0x0400182A RID: 6186
			[Token(Token = "0x40059C5")]
			SkillContentListDB_EN,
			// Token: 0x0400182B RID: 6187
			[Token(Token = "0x40059C6")]
			SkillContentListDB_WPN,
			// Token: 0x0400182C RID: 6188
			[Token(Token = "0x40059C7")]
			SkillContentListDB_MST,
			// Token: 0x0400182D RID: 6189
			[Token(Token = "0x40059C8")]
			SkillContentListDB_CARD,
			// Token: 0x0400182E RID: 6190
			[Token(Token = "0x40059C9")]
			SkillLvCoefDB,
			// Token: 0x0400182F RID: 6191
			[Token(Token = "0x40059CA")]
			BattleDefineDB,
			// Token: 0x04001830 RID: 6192
			[Token(Token = "0x40059CB")]
			SkillContentAndEffectCombiDB,
			// Token: 0x04001831 RID: 6193
			[Token(Token = "0x40059CC")]
			UniqueSkillSoundDB,
			// Token: 0x04001832 RID: 6194
			[Token(Token = "0x40059CD")]
			GachaCutInListDB,
			// Token: 0x04001833 RID: 6195
			[Token(Token = "0x40059CE")]
			GachaItemLabelListDB,
			// Token: 0x04001834 RID: 6196
			[Token(Token = "0x40059CF")]
			CharacterFacialDB,
			// Token: 0x04001835 RID: 6197
			[Token(Token = "0x40059D0")]
			CharacterFlavorTextDB,
			// Token: 0x04001836 RID: 6198
			[Token(Token = "0x40059D1")]
			ItemListDB,
			// Token: 0x04001837 RID: 6199
			[Token(Token = "0x40059D2")]
			MasterOrbListDB,
			// Token: 0x04001838 RID: 6200
			[Token(Token = "0x40059D3")]
			MasterOrbBuffsDB,
			// Token: 0x04001839 RID: 6201
			[Token(Token = "0x40059D4")]
			AbilityBoardsDB,
			// Token: 0x0400183A RID: 6202
			[Token(Token = "0x40059D5")]
			AbilitySlotsDB,
			// Token: 0x0400183B RID: 6203
			[Token(Token = "0x40059D6")]
			AbilitySpheresDB,
			// Token: 0x0400183C RID: 6204
			[Token(Token = "0x40059D7")]
			AbilitySlotItemRecipesDB,
			// Token: 0x0400183D RID: 6205
			[Token(Token = "0x40059D8")]
			AbilitySphereRecipesDB,
			// Token: 0x0400183E RID: 6206
			[Token(Token = "0x40059D9")]
			CharaQuestListDB,
			// Token: 0x0400183F RID: 6207
			[Token(Token = "0x40059DA")]
			EventsDB,
			// Token: 0x04001840 RID: 6208
			[Token(Token = "0x40059DB")]
			EventGroupsDB,
			// Token: 0x04001841 RID: 6209
			[Token(Token = "0x40059DC")]
			EventQuestDropExtDB,
			// Token: 0x04001842 RID: 6210
			[Token(Token = "0x40059DD")]
			EventQuestUISettingDB,
			// Token: 0x04001843 RID: 6211
			[Token(Token = "0x40059DE")]
			ChapterUISettingDB,
			// Token: 0x04001844 RID: 6212
			[Token(Token = "0x40059DF")]
			QuestWaveListDB,
			// Token: 0x04001845 RID: 6213
			[Token(Token = "0x40059E0")]
			QuestWaveRandomListDB,
			// Token: 0x04001846 RID: 6214
			[Token(Token = "0x40059E1")]
			QuestWaveDropsDB,
			// Token: 0x04001847 RID: 6215
			[Token(Token = "0x40059E2")]
			QuestEnemyListDB,
			// Token: 0x04001848 RID: 6216
			[Token(Token = "0x40059E3")]
			QuestADVTriggerDB,
			// Token: 0x04001849 RID: 6217
			[Token(Token = "0x40059E4")]
			QuestMapSDPositionDB,
			// Token: 0x0400184A RID: 6218
			[Token(Token = "0x40059E5")]
			QuestMapResourceListDB,
			// Token: 0x0400184B RID: 6219
			[Token(Token = "0x40059E6")]
			EnemyResourceListDB,
			// Token: 0x0400184C RID: 6220
			[Token(Token = "0x40059E7")]
			TownObjectListDB,
			// Token: 0x0400184D RID: 6221
			[Token(Token = "0x40059E8")]
			TownShopListDB,
			// Token: 0x0400184E RID: 6222
			[Token(Token = "0x40059E9")]
			TownObjLvUpDB,
			// Token: 0x0400184F RID: 6223
			[Token(Token = "0x40059EA")]
			TownObjBuffDB,
			// Token: 0x04001850 RID: 6224
			[Token(Token = "0x40059EB")]
			RoomListDB,
			// Token: 0x04001851 RID: 6225
			[Token(Token = "0x40059EC")]
			RoomObjFilterCategoryDB,
			// Token: 0x04001852 RID: 6226
			[Token(Token = "0x40059ED")]
			RoomObjectListDB,
			// Token: 0x04001853 RID: 6227
			[Token(Token = "0x40059EE")]
			RoomShopListDB,
			// Token: 0x04001854 RID: 6228
			[Token(Token = "0x40059EF")]
			RoomEnvEffectDB,
			// Token: 0x04001855 RID: 6229
			[Token(Token = "0x40059F0")]
			EffectListDB,
			// Token: 0x04001856 RID: 6230
			[Token(Token = "0x40059F1")]
			MasterRankDB,
			// Token: 0x04001857 RID: 6231
			[Token(Token = "0x40059F2")]
			CharacterExpDB,
			// Token: 0x04001858 RID: 6232
			[Token(Token = "0x40059F3")]
			NamedFriendshipExpDB,
			// Token: 0x04001859 RID: 6233
			[Token(Token = "0x40059F4")]
			WeaponExpDB,
			// Token: 0x0400185A RID: 6234
			[Token(Token = "0x40059F5")]
			SkillExpDB,
			// Token: 0x0400185B RID: 6235
			[Token(Token = "0x40059F6")]
			CharacterParamGrowthListDB,
			// Token: 0x0400185C RID: 6236
			[Token(Token = "0x40059F7")]
			ProfileVoiceListDB,
			// Token: 0x0400185D RID: 6237
			[Token(Token = "0x40059F8")]
			FieldItemDropListDB,
			// Token: 0x0400185E RID: 6238
			[Token(Token = "0x40059F9")]
			PopUpStateIconListDB,
			// Token: 0x0400185F RID: 6239
			[Token(Token = "0x40059FA")]
			StateIconListDB,
			// Token: 0x04001860 RID: 6240
			[Token(Token = "0x40059FB")]
			CharaIllustOffsetDB,
			// Token: 0x04001861 RID: 6241
			[Token(Token = "0x40059FC")]
			ADVListDB,
			// Token: 0x04001862 RID: 6242
			[Token(Token = "0x40059FD")]
			ADVLibraryListDB,
			// Token: 0x04001863 RID: 6243
			[Token(Token = "0x40059FE")]
			WordLibraryListDB,
			// Token: 0x04001864 RID: 6244
			[Token(Token = "0x40059FF")]
			OriginalCharaLibraryListDB,
			// Token: 0x04001865 RID: 6245
			[Token(Token = "0x4005A00")]
			SceneInfoListDB,
			// Token: 0x04001866 RID: 6246
			[Token(Token = "0x4005A01")]
			TutorialTipsListDB,
			// Token: 0x04001867 RID: 6247
			[Token(Token = "0x4005A02")]
			RetireTipsListDB,
			// Token: 0x04001868 RID: 6248
			[Token(Token = "0x4005A03")]
			TutorialMessageListDB,
			// Token: 0x04001869 RID: 6249
			[Token(Token = "0x4005A04")]
			WebDataListDB,
			// Token: 0x0400186A RID: 6250
			[Token(Token = "0x4005A05")]
			TownObjBuildSubDB,
			// Token: 0x0400186B RID: 6251
			[Token(Token = "0x4005A06")]
			TogtherChargeDefine,
			// Token: 0x0400186C RID: 6252
			[Token(Token = "0x4005A07")]
			BattleStatusRatioByHp,
			// Token: 0x0400186D RID: 6253
			[Token(Token = "0x4005A08")]
			PackageItemListDB,
			// Token: 0x0400186E RID: 6254
			[Token(Token = "0x4005A09")]
			PackageItemContentsDB,
			// Token: 0x0400186F RID: 6255
			[Token(Token = "0x4005A0A")]
			BattleRandomStatusChange,
			// Token: 0x04001870 RID: 6256
			[Token(Token = "0x4005A0B")]
			WeaponSkillUpItemsDB,
			// Token: 0x04001871 RID: 6257
			[Token(Token = "0x4005A0C")]
			FlavorsDB,
			// Token: 0x04001872 RID: 6258
			[Token(Token = "0x4005A0D")]
			Num
		}

		// Token: 0x020004E0 RID: 1248
		[Token(Token = "0x2000DDD")]
		private enum ePrepareStep
		{
			// Token: 0x04001874 RID: 6260
			[Token(Token = "0x4005A0F")]
			None = -1,
			// Token: 0x04001875 RID: 6261
			[Token(Token = "0x4005A10")]
			Preparing,
			// Token: 0x04001876 RID: 6262
			[Token(Token = "0x4005A11")]
			DonePrepare
		}
	}
}
