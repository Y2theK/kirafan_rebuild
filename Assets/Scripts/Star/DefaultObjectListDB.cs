﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200067D RID: 1661
	[Token(Token = "0x200054C")]
	[StructLayout(3)]
	public class DefaultObjectListDB : ScriptableObject
	{
		// Token: 0x0600183B RID: 6203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D6")]
		[Address(RVA = "0x1011C4664", Offset = "0x11C4664", VA = "0x1011C4664")]
		public DefaultObjectListDB()
		{
		}

		// Token: 0x040027A1 RID: 10145
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400207E")]
		public DefaultObjectListDB_Param[] m_Params;
	}
}
