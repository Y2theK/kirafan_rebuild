﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200067C RID: 1660
	[Token(Token = "0x200054B")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct DefaultObjectListDB_Param
	{
		// Token: 0x0400279B RID: 10139
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002078")]
		public int m_Category;

		// Token: 0x0400279C RID: 10140
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002079")]
		public int m_Type;

		// Token: 0x0400279D RID: 10141
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400207A")]
		public int m_ObjectNo;

		// Token: 0x0400279E RID: 10142
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400207B")]
		public int m_Num;

		// Token: 0x0400279F RID: 10143
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400207C")]
		public int[] m_ObjEventArgs;

		// Token: 0x040027A0 RID: 10144
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400207D")]
		public int m_WakeUp;
	}
}
