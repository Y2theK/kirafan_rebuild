﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Download;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x0200078E RID: 1934
	[Token(Token = "0x20005CA")]
	[StructLayout(3)]
	public class DownloadMain : GameStateMain
	{
		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06001D3C RID: 7484 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700020B")]
		public DownloadUI DownloadUI
		{
			[Token(Token = "0x6001ABA")]
			[Address(RVA = "0x1011C466C", Offset = "0x11C466C", VA = "0x1011C466C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06001D3D RID: 7485 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001D3E RID: 7486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700020C")]
		public MovieCanvas MovieCanvas
		{
			[Token(Token = "0x6001ABB")]
			[Address(RVA = "0x1011C4674", Offset = "0x11C4674", VA = "0x1011C4674")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001ABC")]
			[Address(RVA = "0x1011C467C", Offset = "0x11C467C", VA = "0x1011C467C")]
			set
			{
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06001D3F RID: 7487 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700020D")]
		public MoviePlayUI MoviePlayUI
		{
			[Token(Token = "0x6001ABD")]
			[Address(RVA = "0x1011C4684", Offset = "0x11C4684", VA = "0x1011C4684")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001D40 RID: 7488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ABE")]
		[Address(RVA = "0x1011C468C", Offset = "0x11C468C", VA = "0x1011C468C")]
		private void Start()
		{
		}

		// Token: 0x06001D41 RID: 7489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ABF")]
		[Address(RVA = "0x1011C4698", Offset = "0x11C4698", VA = "0x1011C4698")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001D42 RID: 7490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC0")]
		[Address(RVA = "0x1011C46A0", Offset = "0x11C46A0", VA = "0x1011C46A0", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001D43 RID: 7491 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001AC1")]
		[Address(RVA = "0x1011C46A8", Offset = "0x11C46A8", VA = "0x1011C46A8", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001D44 RID: 7492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC2")]
		[Address(RVA = "0x1011C47B4", Offset = "0x11C47B4", VA = "0x1011C47B4")]
		public void TouchDisplayUIOnPlayingMovie()
		{
		}

		// Token: 0x06001D45 RID: 7493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC3")]
		[Address(RVA = "0x1011C48AC", Offset = "0x11C48AC", VA = "0x1011C48AC")]
		public DownloadMain()
		{
		}

		// Token: 0x04002D9E RID: 11678
		[Token(Token = "0x40023B1")]
		public const int STATE_MAIN = 0;

		// Token: 0x04002D9F RID: 11679
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023B2")]
		[SerializeField]
		private DownloadUI m_DownloadUI;

		// Token: 0x04002DA0 RID: 11680
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023B3")]
		[SerializeField]
		private MoviePlayUI m_MoviePlayUI;

		// Token: 0x04002DA1 RID: 11681
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40023B4")]
		[SerializeField]
		private MovieCanvas m_MovieCanvas;
	}
}
