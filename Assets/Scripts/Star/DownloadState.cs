﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200078F RID: 1935
	[Token(Token = "0x20005CB")]
	[StructLayout(3)]
	public class DownloadState : GameStateBase
	{
		// Token: 0x06001D46 RID: 7494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC4")]
		[Address(RVA = "0x1011C48B4", Offset = "0x11C48B4", VA = "0x1011C48B4")]
		public DownloadState(DownloadMain owner)
		{
		}

		// Token: 0x06001D47 RID: 7495 RVA: 0x0000D0C8 File Offset: 0x0000B2C8
		[Token(Token = "0x6001AC5")]
		[Address(RVA = "0x1011C48E0", Offset = "0x11C48E0", VA = "0x1011C48E0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D48 RID: 7496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC6")]
		[Address(RVA = "0x1011C48E8", Offset = "0x11C48E8", VA = "0x1011C48E8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D49 RID: 7497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC7")]
		[Address(RVA = "0x1011C48EC", Offset = "0x11C48EC", VA = "0x1011C48EC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D4A RID: 7498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AC8")]
		[Address(RVA = "0x1011C48F0", Offset = "0x11C48F0", VA = "0x1011C48F0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D4B RID: 7499 RVA: 0x0000D0E0 File Offset: 0x0000B2E0
		[Token(Token = "0x6001AC9")]
		[Address(RVA = "0x1011C48F4", Offset = "0x11C48F4", VA = "0x1011C48F4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D4C RID: 7500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ACA")]
		[Address(RVA = "0x1011C48FC", Offset = "0x11C48FC", VA = "0x1011C48FC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002DA2 RID: 11682
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40023B5")]
		protected DownloadMain m_Owner;
	}
}
