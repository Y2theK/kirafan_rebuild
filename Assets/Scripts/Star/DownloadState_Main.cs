﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000790 RID: 1936
	[Token(Token = "0x20005CC")]
	[StructLayout(3)]
	public class DownloadState_Main : DownloadState
	{
		// Token: 0x06001D4D RID: 7501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ACB")]
		[Address(RVA = "0x1011C4718", Offset = "0x11C4718", VA = "0x1011C4718")]
		public DownloadState_Main(DownloadMain owner)
		{
		}

		// Token: 0x06001D4E RID: 7502 RVA: 0x0000D0F8 File Offset: 0x0000B2F8
		[Token(Token = "0x6001ACC")]
		[Address(RVA = "0x1011C4900", Offset = "0x11C4900", VA = "0x1011C4900", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D4F RID: 7503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ACD")]
		[Address(RVA = "0x1011C4908", Offset = "0x11C4908", VA = "0x1011C4908", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D50 RID: 7504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ACE")]
		[Address(RVA = "0x1011C4910", Offset = "0x11C4910", VA = "0x1011C4910", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D51 RID: 7505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ACF")]
		[Address(RVA = "0x1011C4914", Offset = "0x11C4914", VA = "0x1011C4914", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D52 RID: 7506 RVA: 0x0000D110 File Offset: 0x0000B310
		[Token(Token = "0x6001AD0")]
		[Address(RVA = "0x1011C4918", Offset = "0x11C4918", VA = "0x1011C4918", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D53 RID: 7507 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001AD1")]
		[Address(RVA = "0x1011C64D4", Offset = "0x11C64D4", VA = "0x1011C64D4")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x1001386DC", Offset = "0x1386DC")]
		private IEnumerator RemoveOldCache()
		{
			return null;
		}

		// Token: 0x06001D54 RID: 7508 RVA: 0x0000D128 File Offset: 0x0000B328
		[Token(Token = "0x6001AD2")]
		[Address(RVA = "0x1011C695C", Offset = "0x11C695C", VA = "0x1011C695C")]
		private float CalcProgress(DownloadState_Main.eDownloadPart part, float progressPerPart)
		{
			return 0f;
		}

		// Token: 0x06001D55 RID: 7509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AD3")]
		[Address(RVA = "0x1011C6548", Offset = "0x11C6548", VA = "0x1011C6548")]
		private void PrepareDownloadFiles()
		{
		}

		// Token: 0x06001D56 RID: 7510 RVA: 0x0000D140 File Offset: 0x0000B340
		[Token(Token = "0x6001AD4")]
		[Address(RVA = "0x1011C6818", Offset = "0x11C6818", VA = "0x1011C6818")]
		private bool IsCompletePrepareDownloadFiles()
		{
			return default(bool);
		}

		// Token: 0x06001D57 RID: 7511 RVA: 0x0000D158 File Offset: 0x0000B358
		[Token(Token = "0x6001AD5")]
		[Address(RVA = "0x1011C68A0", Offset = "0x11C68A0", VA = "0x1011C68A0")]
		private bool EnableDownloadSizeCheckPopup(double downloadSize)
		{
			return default(bool);
		}

		// Token: 0x06001D58 RID: 7512 RVA: 0x0000D170 File Offset: 0x0000B370
		[Token(Token = "0x6001AD6")]
		[Address(RVA = "0x1011C6954", Offset = "0x11C6954", VA = "0x1011C6954")]
		private bool IsFirstDownloadCaution()
		{
			return default(bool);
		}

		// Token: 0x06001D59 RID: 7513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AD7")]
		[Address(RVA = "0x1011C6A60", Offset = "0x11C6A60", VA = "0x1011C6A60", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001D5A RID: 7514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AD8")]
		[Address(RVA = "0x1011C6A64", Offset = "0x11C6A64", VA = "0x1011C6A64")]
		private void OnConfirmAssetBundleDownloadError(int answer)
		{
		}

		// Token: 0x06001D5B RID: 7515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AD9")]
		[Address(RVA = "0x1011C6AEC", Offset = "0x11C6AEC", VA = "0x1011C6AEC")]
		private void RetryDownloadAssetBundle()
		{
		}

		// Token: 0x06001D5C RID: 7516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADA")]
		[Address(RVA = "0x1011C6DB0", Offset = "0x11C6DB0", VA = "0x1011C6DB0")]
		private void OnConfirmCRIDownloadError(int answer)
		{
		}

		// Token: 0x06001D5D RID: 7517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADB")]
		[Address(RVA = "0x1011C6E98", Offset = "0x11C6E98", VA = "0x1011C6E98")]
		private void OnCompleteTutorialAdvAdd()
		{
		}

		// Token: 0x06001D5E RID: 7518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADC")]
		[Address(RVA = "0x1011C6EA4", Offset = "0x11C6EA4", VA = "0x1011C6EA4")]
		private void OnCompleteTutorialQuest()
		{
		}

		// Token: 0x06001D5F RID: 7519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADD")]
		[Address(RVA = "0x1011C6EB0", Offset = "0x11C6EB0", VA = "0x1011C6EB0")]
		private void OnExecSkipMovie()
		{
		}

		// Token: 0x06001D60 RID: 7520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADE")]
		[Address(RVA = "0x1011C6EBC", Offset = "0x11C6EBC", VA = "0x1011C6EBC")]
		private void OnCofirmDownloadEnd(int answer)
		{
		}

		// Token: 0x06001D61 RID: 7521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ADF")]
		[Address(RVA = "0x1011C6EC8", Offset = "0x11C6EC8", VA = "0x1011C6EC8")]
		private void OnConfirmDownloadSize(int answer)
		{
		}

		// Token: 0x06001D62 RID: 7522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AE0")]
		[Address(RVA = "0x1011C6EE4", Offset = "0x11C6EE4", VA = "0x1011C6EE4")]
		private void OnConfirmFirstDownloadCaution(int answer)
		{
		}

		// Token: 0x04002DA3 RID: 11683
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40023B6")]
		private DownloadState_Main.eStep m_Step;

		// Token: 0x04002DA4 RID: 11684
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023B7")]
		private bool m_IsDoneRemoveOldCache;

		// Token: 0x04002DA5 RID: 11685
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023B8")]
		private AssetBundleDownloadListDB m_ABDLDB;

		// Token: 0x04002DA6 RID: 11686
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023B9")]
		private int m_AssetBundleDownloadCountMax;

		// Token: 0x04002DA7 RID: 11687
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40023BA")]
		private int m_CriFileDownloadCountMax;

		// Token: 0x04002DA8 RID: 11688
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023BB")]
		private double m_DownloadSize;

		// Token: 0x04002DA9 RID: 11689
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023BC")]
		private MeigeResource.DLSizeHandle m_ABDLSizeHndl;

		// Token: 0x04002DAA RID: 11690
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40023BD")]
		private SoundDLSizeCheck m_SoundDLSizeHndl;

		// Token: 0x04002DAB RID: 11691
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40023BE")]
		private GameDefine.eDLScene m_DLScene;

		// Token: 0x04002DAC RID: 11692
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40023BF")]
		private bool m_IsMovie;

		// Token: 0x04002DAD RID: 11693
		[Cpp2IlInjected.FieldOffset(Offset = "0x4D")]
		[Token(Token = "0x40023C0")]
		private bool m_RequestedMovieSkip;

		// Token: 0x04002DAE RID: 11694
		[Cpp2IlInjected.FieldOffset(Offset = "0x4E")]
		[Token(Token = "0x40023C1")]
		private bool m_IsErrorWindow;

		// Token: 0x04002DAF RID: 11695
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40023C2")]
		private ADVAPI m_AdvAPI;

		// Token: 0x04002DB0 RID: 11696
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40023C3")]
		private float[] DOWNLOAD_THRESHOLDS;

		// Token: 0x02000791 RID: 1937
		[Token(Token = "0x2000E95")]
		private enum eStep
		{
			// Token: 0x04002DB2 RID: 11698
			[Token(Token = "0x4005D64")]
			None = -1,
			// Token: 0x04002DB3 RID: 11699
			[Token(Token = "0x4005D65")]
			PreProcess_0,
			// Token: 0x04002DB4 RID: 11700
			[Token(Token = "0x4005D66")]
			PreProcess_1,
			// Token: 0x04002DB5 RID: 11701
			[Token(Token = "0x4005D67")]
			PreProcess_2,
			// Token: 0x04002DB6 RID: 11702
			[Token(Token = "0x4005D68")]
			PreProcess_3,
			// Token: 0x04002DB7 RID: 11703
			[Token(Token = "0x4005D69")]
			PreProcess_4,
			// Token: 0x04002DB8 RID: 11704
			[Token(Token = "0x4005D6A")]
			PreProcess_End,
			// Token: 0x04002DB9 RID: 11705
			[Token(Token = "0x4005D6B")]
			DownloadAssetBundle,
			// Token: 0x04002DBA RID: 11706
			[Token(Token = "0x4005D6C")]
			DownloadAssetBundle_Wait,
			// Token: 0x04002DBB RID: 11707
			[Token(Token = "0x4005D6D")]
			DownloadCRI,
			// Token: 0x04002DBC RID: 11708
			[Token(Token = "0x4005D6E")]
			DownloadCRI_Wait,
			// Token: 0x04002DBD RID: 11709
			[Token(Token = "0x4005D6F")]
			PostProcess,
			// Token: 0x04002DBE RID: 11710
			[Token(Token = "0x4005D70")]
			PostProcess_Wait,
			// Token: 0x04002DBF RID: 11711
			[Token(Token = "0x4005D71")]
			Movie_Wait,
			// Token: 0x04002DC0 RID: 11712
			[Token(Token = "0x4005D72")]
			TransitNextScene,
			// Token: 0x04002DC1 RID: 11713
			[Token(Token = "0x4005D73")]
			TransitNextScene_Wait,
			// Token: 0x04002DC2 RID: 11714
			[Token(Token = "0x4005D74")]
			ToADVPrologueIntro2OnTutorial_0,
			// Token: 0x04002DC3 RID: 11715
			[Token(Token = "0x4005D75")]
			ToADVPrologueIntro2OnTutorial_1,
			// Token: 0x04002DC4 RID: 11716
			[Token(Token = "0x4005D76")]
			ToADVPrologueIntro2OnTutorial_2,
			// Token: 0x04002DC5 RID: 11717
			[Token(Token = "0x4005D77")]
			ToBattleOnTutorial_0,
			// Token: 0x04002DC6 RID: 11718
			[Token(Token = "0x4005D78")]
			ToBattleOnTutorial_1,
			// Token: 0x04002DC7 RID: 11719
			[Token(Token = "0x4005D79")]
			ToBattleOnTutorial_2
		}

		// Token: 0x02000792 RID: 1938
		[Token(Token = "0x2000E96")]
		public enum eDownloadPart
		{
			// Token: 0x04002DC9 RID: 11721
			[Token(Token = "0x4005D7B")]
			AB,
			// Token: 0x04002DCA RID: 11722
			[Token(Token = "0x4005D7C")]
			CRI,
			// Token: 0x04002DCB RID: 11723
			[Token(Token = "0x4005D7D")]
			PostProcess,
			// Token: 0x04002DCC RID: 11724
			[Token(Token = "0x4005D7E")]
			Num
		}
	}
}
