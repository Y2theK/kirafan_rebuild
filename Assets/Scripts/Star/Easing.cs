﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008BE RID: 2238
	[Token(Token = "0x2000668")]
	[StructLayout(3)]
	public static class Easing
	{
		// Token: 0x060024AA RID: 9386 RVA: 0x0000FAC8 File Offset: 0x0000DCC8
		[Token(Token = "0x60021CE")]
		[Address(RVA = "0x1011C7108", Offset = "0x11C7108", VA = "0x1011C7108")]
		public static float Linear(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024AB RID: 9387 RVA: 0x0000FAE0 File Offset: 0x0000DCE0
		[Token(Token = "0x60021CF")]
		[Address(RVA = "0x1011C7118", Offset = "0x11C7118", VA = "0x1011C7118")]
		public static float EaseInQuad(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024AC RID: 9388 RVA: 0x0000FAF8 File Offset: 0x0000DCF8
		[Token(Token = "0x60021D0")]
		[Address(RVA = "0x1011C712C", Offset = "0x11C712C", VA = "0x1011C712C")]
		public static float EaseOutQuad(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024AD RID: 9389 RVA: 0x0000FB10 File Offset: 0x0000DD10
		[Token(Token = "0x60021D1")]
		[Address(RVA = "0x1011C7148", Offset = "0x11C7148", VA = "0x1011C7148")]
		public static float EaseInOutQuad(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024AE RID: 9390 RVA: 0x0000FB28 File Offset: 0x0000DD28
		[Token(Token = "0x60021D2")]
		[Address(RVA = "0x1011C7198", Offset = "0x11C7198", VA = "0x1011C7198")]
		public static float EaseInCubic(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024AF RID: 9391 RVA: 0x0000FB40 File Offset: 0x0000DD40
		[Token(Token = "0x60021D3")]
		[Address(RVA = "0x1011C71B0", Offset = "0x11C71B0", VA = "0x1011C71B0")]
		public static float EaseOutCubic(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B0 RID: 9392 RVA: 0x0000FB58 File Offset: 0x0000DD58
		[Token(Token = "0x60021D4")]
		[Address(RVA = "0x1011C71D8", Offset = "0x11C71D8", VA = "0x1011C71D8")]
		public static float EaseInOutCubic(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B1 RID: 9393 RVA: 0x0000FB70 File Offset: 0x0000DD70
		[Token(Token = "0x60021D5")]
		[Address(RVA = "0x1011C7224", Offset = "0x11C7224", VA = "0x1011C7224")]
		public static float EaseInQuart(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B2 RID: 9394 RVA: 0x0000FB88 File Offset: 0x0000DD88
		[Token(Token = "0x60021D6")]
		[Address(RVA = "0x1011C7240", Offset = "0x11C7240", VA = "0x1011C7240")]
		public static float EaseOutQuart(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B3 RID: 9395 RVA: 0x0000FBA0 File Offset: 0x0000DDA0
		[Token(Token = "0x60021D7")]
		[Address(RVA = "0x1011C7268", Offset = "0x11C7268", VA = "0x1011C7268")]
		public static float EaseInOutQuart(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B4 RID: 9396 RVA: 0x0000FBB8 File Offset: 0x0000DDB8
		[Token(Token = "0x60021D8")]
		[Address(RVA = "0x1011C72C0", Offset = "0x11C72C0", VA = "0x1011C72C0")]
		public static float EaseInQuint(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B5 RID: 9397 RVA: 0x0000FBD0 File Offset: 0x0000DDD0
		[Token(Token = "0x60021D9")]
		[Address(RVA = "0x1011C72E0", Offset = "0x11C72E0", VA = "0x1011C72E0")]
		public static float EaseOutQuint(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B6 RID: 9398 RVA: 0x0000FBE8 File Offset: 0x0000DDE8
		[Token(Token = "0x60021DA")]
		[Address(RVA = "0x1011C7310", Offset = "0x11C7310", VA = "0x1011C7310")]
		public static float EaseInOutQuint(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B7 RID: 9399 RVA: 0x0000FC00 File Offset: 0x0000DE00
		[Token(Token = "0x60021DB")]
		[Address(RVA = "0x1011C736C", Offset = "0x11C736C", VA = "0x1011C736C")]
		public static float EaseInSine(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B8 RID: 9400 RVA: 0x0000FC18 File Offset: 0x0000DE18
		[Token(Token = "0x60021DC")]
		[Address(RVA = "0x1011C7408", Offset = "0x11C7408", VA = "0x1011C7408")]
		public static float EaseOutSine(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024B9 RID: 9401 RVA: 0x0000FC30 File Offset: 0x0000DE30
		[Token(Token = "0x60021DD")]
		[Address(RVA = "0x1011C74A0", Offset = "0x11C74A0", VA = "0x1011C74A0")]
		public static float EaseInOutSine(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BA RID: 9402 RVA: 0x0000FC48 File Offset: 0x0000DE48
		[Token(Token = "0x60021DE")]
		[Address(RVA = "0x1011C7548", Offset = "0x11C7548", VA = "0x1011C7548")]
		public static float EaseInExpo(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BB RID: 9403 RVA: 0x0000FC60 File Offset: 0x0000DE60
		[Token(Token = "0x60021DF")]
		[Address(RVA = "0x1011C75E4", Offset = "0x11C75E4", VA = "0x1011C75E4")]
		public static float EaseOutExpo(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BC RID: 9404 RVA: 0x0000FC78 File Offset: 0x0000DE78
		[Token(Token = "0x60021E0")]
		[Address(RVA = "0x1011C7680", Offset = "0x11C7680", VA = "0x1011C7680")]
		public static float EaseInOutExpo(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BD RID: 9405 RVA: 0x0000FC90 File Offset: 0x0000DE90
		[Token(Token = "0x60021E1")]
		[Address(RVA = "0x1011C7780", Offset = "0x11C7780", VA = "0x1011C7780")]
		public static float EaseInCirc(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BE RID: 9406 RVA: 0x0000FCA8 File Offset: 0x0000DEA8
		[Token(Token = "0x60021E2")]
		[Address(RVA = "0x1011C7820", Offset = "0x11C7820", VA = "0x1011C7820")]
		public static float EaseOutCirc(float t, float b, float c, float d)
		{
			return 0f;
		}

		// Token: 0x060024BF RID: 9407 RVA: 0x0000FCC0 File Offset: 0x0000DEC0
		[Token(Token = "0x60021E3")]
		[Address(RVA = "0x1011C78C0", Offset = "0x11C78C0", VA = "0x1011C78C0")]
		public static float EaseInOutCirc(float t, float b, float c, float d)
		{
			return 0f;
		}
	}
}
