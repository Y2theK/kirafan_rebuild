﻿namespace Star {
    public static class EditDefine {
        public const int SUPPORTPARTY_MEMBER_MAX = 8;

        public enum eEvolutionFailureReason {
            RecipeNotFound,
            LimitBreakFail,
            CharaLvFail,
            MaterialFail,
            AmountFail
        }
    }
}
