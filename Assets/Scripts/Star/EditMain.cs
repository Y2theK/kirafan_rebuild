﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000794 RID: 1940
	[Token(Token = "0x20005CD")]
	[StructLayout(3)]
	public class EditMain : GameStateMain
	{
		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06001D69 RID: 7529 RVA: 0x0000D1A0 File Offset: 0x0000B3A0
		// (set) Token: 0x06001D6A RID: 7530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700020E")]
		public EditMain.eTopType TopType
		{
			[Token(Token = "0x6001AE1")]
			[Address(RVA = "0x1011C79B4", Offset = "0x11C79B4", VA = "0x1011C79B4")]
			[CompilerGenerated]
			get
			{
				return EditMain.eTopType.EditTop;
			}
			[Token(Token = "0x6001AE2")]
			[Address(RVA = "0x1011C79BC", Offset = "0x11C79BC", VA = "0x1011C79BC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06001D6B RID: 7531 RVA: 0x0000D1B8 File Offset: 0x0000B3B8
		// (set) Token: 0x06001D6C RID: 7532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700020F")]
		public EditMain.eCharaListType CharaListType
		{
			[Token(Token = "0x6001AE3")]
			[Address(RVA = "0x1011C79C4", Offset = "0x11C79C4", VA = "0x1011C79C4")]
			[CompilerGenerated]
			get
			{
				return EditMain.eCharaListType.Upgrade;
			}
			[Token(Token = "0x6001AE4")]
			[Address(RVA = "0x1011C79CC", Offset = "0x11C79CC", VA = "0x1011C79CC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06001D6D RID: 7533 RVA: 0x0000D1D0 File Offset: 0x0000B3D0
		// (set) Token: 0x06001D6E RID: 7534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000210")]
		public EditMain.eEnhanceType EnhanceType
		{
			[Token(Token = "0x6001AE5")]
			[Address(RVA = "0x1011C79D4", Offset = "0x11C79D4", VA = "0x1011C79D4")]
			[CompilerGenerated]
			get
			{
				return EditMain.eEnhanceType.Status;
			}
			[Token(Token = "0x6001AE6")]
			[Address(RVA = "0x1011C79DC", Offset = "0x11C79DC", VA = "0x1011C79DC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06001D6F RID: 7535 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001D70 RID: 7536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000211")]
		public CharaListUI CharaListUI
		{
			[Token(Token = "0x6001AE7")]
			[Address(RVA = "0x1011C79E4", Offset = "0x11C79E4", VA = "0x1011C79E4")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001AE8")]
			[Address(RVA = "0x1011C79EC", Offset = "0x11C79EC", VA = "0x1011C79EC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06001D71 RID: 7537 RVA: 0x0000D1E8 File Offset: 0x0000B3E8
		// (set) Token: 0x06001D72 RID: 7538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000212")]
		public float ScrollValue
		{
			[Token(Token = "0x6001AE9")]
			[Address(RVA = "0x1011C79F4", Offset = "0x11C79F4", VA = "0x1011C79F4")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6001AEA")]
			[Address(RVA = "0x1011C79FC", Offset = "0x11C79FC", VA = "0x1011C79FC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06001D73 RID: 7539 RVA: 0x0000D200 File Offset: 0x0000B400
		// (set) Token: 0x06001D74 RID: 7540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000213")]
		public int AbilityBoardItemID
		{
			[Token(Token = "0x6001AEB")]
			[Address(RVA = "0x1011C7A04", Offset = "0x11C7A04", VA = "0x1011C7A04")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6001AEC")]
			[Address(RVA = "0x1011C7A0C", Offset = "0x11C7A0C", VA = "0x1011C7A0C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06001D75 RID: 7541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AED")]
		[Address(RVA = "0x1011C7A14", Offset = "0x11C7A14", VA = "0x1011C7A14")]
		private void Start()
		{
		}

		// Token: 0x06001D76 RID: 7542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AEE")]
		[Address(RVA = "0x1011C7A50", Offset = "0x11C7A50", VA = "0x1011C7A50")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001D77 RID: 7543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AEF")]
		[Address(RVA = "0x1011C7A58", Offset = "0x11C7A58", VA = "0x1011C7A58", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001D78 RID: 7544 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001AF0")]
		[Address(RVA = "0x1011C7A60", Offset = "0x11C7A60", VA = "0x1011C7A60", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001D79 RID: 7545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF1")]
		[Address(RVA = "0x1011C7CD4", Offset = "0x11C7CD4", VA = "0x1011C7CD4", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001D7A RID: 7546 RVA: 0x0000D218 File Offset: 0x0000B418
		[Token(Token = "0x6001AF2")]
		[Address(RVA = "0x1011C7D8C", Offset = "0x11C7D8C", VA = "0x1011C7D8C", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001D7B RID: 7547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF3")]
		[Address(RVA = "0x1011C7D94", Offset = "0x11C7D94", VA = "0x1011C7D94")]
		public EditMain()
		{
		}

		// Token: 0x04002DD0 RID: 11728
		[Token(Token = "0x40023C4")]
		public const int STATE_INIT = 0;

		// Token: 0x04002DD1 RID: 11729
		[Token(Token = "0x40023C5")]
		public const int STATE_EDIT_TOP = 1;

		// Token: 0x04002DD2 RID: 11730
		[Token(Token = "0x40023C6")]
		public const int STATE_PARTY_EDIT = 2;

		// Token: 0x04002DD3 RID: 11731
		[Token(Token = "0x40023C7")]
		public const int STATE_CHARALIST_PARTY_EDIT = 3;

		// Token: 0x04002DD4 RID: 11732
		[Token(Token = "0x40023C8")]
		public const int STATE_SUPPORT_EDIT = 4;

		// Token: 0x04002DD5 RID: 11733
		[Token(Token = "0x40023C9")]
		public const int STATE_CHARALIST_SUPPORT_EDIT = 5;

		// Token: 0x04002DD6 RID: 11734
		[Token(Token = "0x40023CA")]
		public const int STATE_WEAPONLIST_EQUIP = 6;

		// Token: 0x04002DD7 RID: 11735
		[Token(Token = "0x40023CB")]
		public const int STATE_CHARALIST_UPGRADE = 8;

		// Token: 0x04002DD8 RID: 11736
		[Token(Token = "0x40023CC")]
		public const int STATE_UPGRADE = 9;

		// Token: 0x04002DD9 RID: 11737
		[Token(Token = "0x40023CD")]
		public const int STATE_UPGRADE_RESULT = 10;

		// Token: 0x04002DDA RID: 11738
		[Token(Token = "0x40023CE")]
		public const int STATE_CHARALIST_LIMITBREAK = 11;

		// Token: 0x04002DDB RID: 11739
		[Token(Token = "0x40023CF")]
		public const int STATE_LIMITBREAK = 12;

		// Token: 0x04002DDC RID: 11740
		[Token(Token = "0x40023D0")]
		public const int STATE_LIMITBREAK_RESULT = 13;

		// Token: 0x04002DDD RID: 11741
		[Token(Token = "0x40023D1")]
		public const int STATE_CHARALIST_EVOLUTION = 14;

		// Token: 0x04002DDE RID: 11742
		[Token(Token = "0x40023D2")]
		public const int STATE_EVOLUTION = 15;

		// Token: 0x04002DDF RID: 11743
		[Token(Token = "0x40023D3")]
		public const int STATE_EVOLUTION_RESULT = 16;

		// Token: 0x04002DE0 RID: 11744
		[Token(Token = "0x40023D4")]
		public const int STATE_CHARALIST_VIEW = 17;

		// Token: 0x04002DE1 RID: 11745
		[Token(Token = "0x40023D5")]
		public const int STATE_ENHANCE = 18;

		// Token: 0x04002DE2 RID: 11746
		[Token(Token = "0x40023D6")]
		public const int STATE_CHARALIST_ENHANCE = 19;

		// Token: 0x04002DE3 RID: 11747
		[Token(Token = "0x40023D7")]
		public const int STATE_VIEWCHANGE = 20;

		// Token: 0x04002DE4 RID: 11748
		[Token(Token = "0x40023D8")]
		public const int STATE_CHARALIST_VIEWCHANGE = 21;

		// Token: 0x04002DE5 RID: 11749
		[Token(Token = "0x40023D9")]
		public const int STATE_ABILITY = 22;

		// Token: 0x04002DE6 RID: 11750
		[Token(Token = "0x40023DA")]
		public const int STATE_CHARALIST_ABILITY = 23;

		// Token: 0x02000795 RID: 1941
		[Token(Token = "0x2000E98")]
		public enum eTopType
		{
			// Token: 0x04002DEE RID: 11758
			[Token(Token = "0x4005D83")]
			EditTop,
			// Token: 0x04002DEF RID: 11759
			[Token(Token = "0x4005D84")]
			EnhanceTop
		}

		// Token: 0x02000796 RID: 1942
		[Token(Token = "0x2000E99")]
		public enum eCharaListType
		{
			// Token: 0x04002DF1 RID: 11761
			[Token(Token = "0x4005D86")]
			Upgrade,
			// Token: 0x04002DF2 RID: 11762
			[Token(Token = "0x4005D87")]
			LimitBreak,
			// Token: 0x04002DF3 RID: 11763
			[Token(Token = "0x4005D88")]
			Evolution
		}

		// Token: 0x02000797 RID: 1943
		[Token(Token = "0x2000E9A")]
		public enum eEnhanceType
		{
			// Token: 0x04002DF5 RID: 11765
			[Token(Token = "0x4005D8A")]
			Status,
			// Token: 0x04002DF6 RID: 11766
			[Token(Token = "0x4005D8B")]
			Upgrade,
			// Token: 0x04002DF7 RID: 11767
			[Token(Token = "0x4005D8C")]
			LimitBreak,
			// Token: 0x04002DF8 RID: 11768
			[Token(Token = "0x4005D8D")]
			Evolution
		}
	}
}
