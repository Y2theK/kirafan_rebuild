﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x0200062C RID: 1580
	[Token(Token = "0x200051E")]
	[StructLayout(3)]
	public sealed class EditManager
	{
		// Token: 0x06001674 RID: 5748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001524")]
		[Address(RVA = "0x1011C7D9C", Offset = "0x11C7D9C", VA = "0x1011C7D9C")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06001675 RID: 5749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001525")]
		[Address(RVA = "0x1011C7DB0", Offset = "0x11C7DB0", VA = "0x1011C7DB0")]
		private void EnableDirtyBattleParty()
		{
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x0000A1B8 File Offset: 0x000083B8
		[Token(Token = "0x6001526")]
		[Address(RVA = "0x1011C7DBC", Offset = "0x11C7DBC", VA = "0x1011C7DBC")]
		public bool IsDirtyBattleParty()
		{
			return default(bool);
		}

		// Token: 0x06001677 RID: 5751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001527")]
		[Address(RVA = "0x1011C7DC4", Offset = "0x11C7DC4", VA = "0x1011C7DC4")]
		public void SavePartyDataIfNotExist()
		{
		}

		// Token: 0x06001678 RID: 5752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001528")]
		[Address(RVA = "0x1011C7FFC", Offset = "0x11C7FFC", VA = "0x1011C7FFC")]
		public void AssignPartyMember(UserBattlePartyData partyData, int memberIndex, long assignCharaMngID)
		{
		}

		// Token: 0x06001679 RID: 5753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001529")]
		[Address(RVA = "0x1011C8230", Offset = "0x11C8230", VA = "0x1011C8230")]
		public void RemovePartyMember(UserBattlePartyData partyData, int memberIndex)
		{
		}

		// Token: 0x0600167A RID: 5754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600152A")]
		[Address(RVA = "0x1011C82AC", Offset = "0x11C82AC", VA = "0x1011C82AC")]
		public void AssignPartyWeapon(UserBattlePartyData partyData, int memberIndex, long assignWeaponMngID)
		{
		}

		// Token: 0x0600167B RID: 5755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600152B")]
		[Address(RVA = "0x1011C8304", Offset = "0x11C8304", VA = "0x1011C8304")]
		public void RemovePartyWeapon(UserBattlePartyData partyData, int memberIndex)
		{
		}

		// Token: 0x0600167C RID: 5756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600152C")]
		[Address(RVA = "0x1011C8354", Offset = "0x11C8354", VA = "0x1011C8354")]
		public void AssignPartyOrb(UserBattlePartyData partyData, int orbID)
		{
		}

		// Token: 0x0600167D RID: 5757 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600152D")]
		[Address(RVA = "0x1011C83A4", Offset = "0x11C83A4", VA = "0x1011C83A4")]
		private List<UserBattlePartyData> GetCostOverPartyDataList()
		{
			return null;
		}

		// Token: 0x0600167E RID: 5758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600152E")]
		[Address(RVA = "0x1011C8564", Offset = "0x11C8564", VA = "0x1011C8564")]
		private void RevertCostOverPartyData()
		{
		}

		// Token: 0x0600167F RID: 5759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600152F")]
		[Address(RVA = "0x1011C8638", Offset = "0x11C8638", VA = "0x1011C8638")]
		private void RevertPartyData(UserBattlePartyData partyData)
		{
		}

		// Token: 0x06001680 RID: 5760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001530")]
		[Address(RVA = "0x1011C8858", Offset = "0x11C8858", VA = "0x1011C8858")]
		public void Request_BattlePartySetAll(Action callback, Action callBackCanceled)
		{
		}

		// Token: 0x06001681 RID: 5761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001531")]
		[Address(RVA = "0x1011C907C", Offset = "0x11C907C", VA = "0x1011C907C")]
		private void OnConfirmCostOver(int btn)
		{
		}

		// Token: 0x06001682 RID: 5762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001532")]
		[Address(RVA = "0x1011C90D4", Offset = "0x11C90D4", VA = "0x1011C90D4")]
		private void OnResponse_BattlePartySetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001683 RID: 5763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001533")]
		[Address(RVA = "0x1011C91D0", Offset = "0x11C91D0", VA = "0x1011C91D0")]
		private void EnableDirtySupportParty()
		{
		}

		// Token: 0x06001684 RID: 5764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001534")]
		[Address(RVA = "0x1011C91DC", Offset = "0x11C91DC", VA = "0x1011C91DC")]
		public void AssignSupportPartyMember(UserSupportPartyData supportPartyData, int memberIndex, long assignCharaMngID)
		{
		}

		// Token: 0x06001685 RID: 5765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001535")]
		[Address(RVA = "0x1011C94B0", Offset = "0x11C94B0", VA = "0x1011C94B0")]
		public void RemoveSupportPartyMember(UserSupportPartyData supportPartyData, int memberIndex)
		{
		}

		// Token: 0x06001686 RID: 5766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001536")]
		[Address(RVA = "0x1011C952C", Offset = "0x11C952C", VA = "0x1011C952C")]
		public void AssignSupportPartyWeapon(UserSupportPartyData supportPartyData, int memberIndex, long assignWeaponMngID)
		{
		}

		// Token: 0x06001687 RID: 5767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001537")]
		[Address(RVA = "0x1011C9584", Offset = "0x11C9584", VA = "0x1011C9584")]
		public void RemoveSupportPartyWeapon(UserSupportPartyData supportPartyData, int memberIndex)
		{
		}

		// Token: 0x06001688 RID: 5768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001538")]
		[Address(RVA = "0x1011C95D4", Offset = "0x11C95D4", VA = "0x1011C95D4")]
		public void Request_SupportPartySetAll(Action callback)
		{
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001539")]
		[Address(RVA = "0x1011C9D7C", Offset = "0x11C9D7C", VA = "0x1011C9D7C")]
		private void OnResponse_SupportPartySetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600168A RID: 5770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600153A")]
		[Address(RVA = "0x1011C9E80", Offset = "0x11C9E80", VA = "0x1011C9E80")]
		public EditManager()
		{
		}

		// Token: 0x04002630 RID: 9776
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F94")]
		private Action m_OnResultPartySetAllCallBack;

		// Token: 0x04002631 RID: 9777
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F95")]
		private Action m_OnCanceledPartySetAllCallBack;

		// Token: 0x04002632 RID: 9778
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001F96")]
		private bool m_IsDirtyBattleParty;

		// Token: 0x04002633 RID: 9779
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001F97")]
		private List<UserBattlePartyData> m_SavedPartyData;

		// Token: 0x04002634 RID: 9780
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001F98")]
		private Action m_OnResultSupportPartySetAllCallBack;

		// Token: 0x04002635 RID: 9781
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001F99")]
		private bool m_IsDirtySupportParty;
	}
}
