﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200062D RID: 1581
	[Token(Token = "0x200051F")]
	[StructLayout(3)]
	public class EditRecommendParty
	{
		// Token: 0x0600168B RID: 5771 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600153B")]
		[Address(RVA = "0x1011C9E88", Offset = "0x11C9E88", VA = "0x1011C9E88")]
		public List<EditRecommendParty.ResultData> Recommend(bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles, bool[] bPriorityStates)
		{
			return null;
		}

		// Token: 0x0600168C RID: 5772 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600153C")]
		[Address(RVA = "0x1011C9EC0", Offset = "0x11C9EC0", VA = "0x1011C9EC0")]
		public static List<EditRecommendParty.ResultData> GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles, bool[] bPriorityStates)
		{
			return null;
		}

		// Token: 0x0600168D RID: 5773 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600153D")]
		[Address(RVA = "0x1011CB550", Offset = "0x11CB550", VA = "0x1011CB550")]
		private static List<UserCharacterData>[] SplitUserCharacterDataForClass(List<UserCharacterData> userCharacterData)
		{
			return null;
		}

		// Token: 0x0600168E RID: 5774 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600153E")]
		[Address(RVA = "0x1011CB0FC", Offset = "0x11CB0FC", VA = "0x1011CB0FC")]
		private static List<UserCharacterData> FilterUserCharacterDatas(List<UserCharacterData> userCharaDatas, bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles)
		{
			return null;
		}

		// Token: 0x0600168F RID: 5775 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600153F")]
		[Address(RVA = "0x1011CB7B0", Offset = "0x11CB7B0", VA = "0x1011CB7B0")]
		private static List<KeyValuePair<UserCharacterData, float>> SortUserCharacterData(List<UserCharacterData> ucds, eCharaStatus priorityState)
		{
			return null;
		}

		// Token: 0x06001690 RID: 5776 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001540")]
		[Address(RVA = "0x1011CBD98", Offset = "0x11CBD98", VA = "0x1011CBD98")]
		private static List<KeyValuePair<UserWeaponData, float>> SortUserWeaponData(List<UserWeaponData> uwds, eCharaStatus priorityState)
		{
			return null;
		}

		// Token: 0x06001691 RID: 5777 RVA: 0x0000A1D0 File Offset: 0x000083D0
		[Token(Token = "0x6001541")]
		[Address(RVA = "0x1011CC470", Offset = "0x11CC470", VA = "0x1011CC470")]
		private static int GetUserCharacterDataStateParameter(UserCharacterData userCharaData, eCharaStatus state)
		{
			return 0;
		}

		// Token: 0x06001692 RID: 5778 RVA: 0x0000A1E8 File Offset: 0x000083E8
		[Token(Token = "0x6001542")]
		[Address(RVA = "0x1011CC880", Offset = "0x11CC880", VA = "0x1011CC880")]
		private static int GetUserWeaponDataStateParameter(UserWeaponData userWeaponData, eCharaStatus state)
		{
			return 0;
		}

		// Token: 0x06001693 RID: 5779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001543")]
		[Address(RVA = "0x1011CC9D8", Offset = "0x11CC9D8", VA = "0x1011CC9D8")]
		public EditRecommendParty()
		{
		}

		// Token: 0x04002636 RID: 9782
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F9A")]
		private List<EditRecommendParty.ResultData> m_Results;

		// Token: 0x0200062E RID: 1582
		[Token(Token = "0x2000DE2")]
		public class ResultData
		{
			// Token: 0x06001694 RID: 5780 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E14")]
			[Address(RVA = "0x1011CC2BC", Offset = "0x11CC2BC", VA = "0x1011CC2BC")]
			public ResultData()
			{
			}

			// Token: 0x04002637 RID: 9783
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005A1D")]
			public long m_CharaMngId;

			// Token: 0x04002638 RID: 9784
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005A1E")]
			public long m_WeaponMngId;
		}
	}
}
