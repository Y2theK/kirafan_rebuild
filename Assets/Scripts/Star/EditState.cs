﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000798 RID: 1944
	[Token(Token = "0x20005CE")]
	[StructLayout(3)]
	public class EditState : GameStateBase
	{
		// Token: 0x06001D7C RID: 7548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF4")]
		[Address(RVA = "0x1011CCB18", Offset = "0x11CCB18", VA = "0x1011CCB18")]
		public EditState(EditMain owner)
		{
		}

		// Token: 0x06001D7D RID: 7549 RVA: 0x0000D230 File Offset: 0x0000B430
		[Token(Token = "0x6001AF5")]
		[Address(RVA = "0x1011CCB4C", Offset = "0x11CCB4C", VA = "0x1011CCB4C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D7E RID: 7550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF6")]
		[Address(RVA = "0x1011CCB54", Offset = "0x11CCB54", VA = "0x1011CCB54", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D7F RID: 7551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF7")]
		[Address(RVA = "0x1011CCB58", Offset = "0x11CCB58", VA = "0x1011CCB58", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D80 RID: 7552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AF8")]
		[Address(RVA = "0x1011CCB5C", Offset = "0x11CCB5C", VA = "0x1011CCB5C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D81 RID: 7553 RVA: 0x0000D248 File Offset: 0x0000B448
		[Token(Token = "0x6001AF9")]
		[Address(RVA = "0x1011CCB60", Offset = "0x11CCB60", VA = "0x1011CCB60", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D82 RID: 7554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AFA")]
		[Address(RVA = "0x1011CCB68", Offset = "0x11CCB68", VA = "0x1011CCB68", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001D83 RID: 7555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AFB")]
		[Address(RVA = "0x1011CCB6C", Offset = "0x11CCB6C", VA = "0x1011CCB6C")]
		protected void OnCanceledPartyEditSave()
		{
		}

		// Token: 0x04002DF9 RID: 11769
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40023E1")]
		protected EditMain m_Owner;

		// Token: 0x04002DFA RID: 11770
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40023E2")]
		protected int m_NextState;
	}
}
