﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Ability;

namespace Star
{
	// Token: 0x02000799 RID: 1945
	[Token(Token = "0x20005CF")]
	[StructLayout(3)]
	public class EditState_Ability : EditState
	{
		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06001D84 RID: 7556 RVA: 0x0000D260 File Offset: 0x0000B460
		[Token(Token = "0x17000214")]
		public long CharacterManagedId
		{
			[Token(Token = "0x6001AFC")]
			[Address(RVA = "0x1011CCBE4", Offset = "0x11CCBE4", VA = "0x1011CCBE4")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06001D85 RID: 7557 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000215")]
		public AbilityUI UI
		{
			[Token(Token = "0x6001AFD")]
			[Address(RVA = "0x1011CCBEC", Offset = "0x11CCBEC", VA = "0x1011CCBEC")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001D86 RID: 7558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AFE")]
		[Address(RVA = "0x1011CCBF4", Offset = "0x11CCBF4", VA = "0x1011CCBF4")]
		public EditState_Ability(EditMain owner)
		{
		}

		// Token: 0x06001D87 RID: 7559 RVA: 0x0000D278 File Offset: 0x0000B478
		[Token(Token = "0x6001AFF")]
		[Address(RVA = "0x1011CCC38", Offset = "0x11CCC38", VA = "0x1011CCC38", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D88 RID: 7560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B00")]
		[Address(RVA = "0x1011CCC40", Offset = "0x11CCC40", VA = "0x1011CCC40", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D89 RID: 7561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B01")]
		[Address(RVA = "0x1011CCCC8", Offset = "0x11CCCC8", VA = "0x1011CCCC8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D8A RID: 7562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B02")]
		[Address(RVA = "0x1011CCCCC", Offset = "0x11CCCCC", VA = "0x1011CCCCC", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D8B RID: 7563 RVA: 0x0000D290 File Offset: 0x0000B490
		[Token(Token = "0x6001B03")]
		[Address(RVA = "0x1011CCD68", Offset = "0x11CCD68", VA = "0x1011CCD68", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D8C RID: 7564 RVA: 0x0000D2A8 File Offset: 0x0000B4A8
		[Token(Token = "0x6001B04")]
		[Address(RVA = "0x1011CD15C", Offset = "0x11CD15C", VA = "0x1011CD15C")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001D8D RID: 7565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B05")]
		[Address(RVA = "0x1011CD37C", Offset = "0x11CD37C", VA = "0x1011CD37C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001D8E RID: 7566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B06")]
		[Address(RVA = "0x1011CD380", Offset = "0x11CD380", VA = "0x1011CD380")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04002DFB RID: 11771
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023E3")]
		private EditState_Ability.eStep m_Step;

		// Token: 0x04002DFC RID: 11772
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023E4")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002DFD RID: 11773
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023E5")]
		private AbilityUI m_UI;

		// Token: 0x04002DFE RID: 11774
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023E6")]
		private AbilityBoardReleaseCutInScene m_CutInScene;

		// Token: 0x04002DFF RID: 11775
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023E7")]
		private long m_CharacterManagedId;

		// Token: 0x0200079A RID: 1946
		[Token(Token = "0x2000E9B")]
		private enum eStep
		{
			// Token: 0x04002E01 RID: 11777
			[Token(Token = "0x4005D8F")]
			None = -1,
			// Token: 0x04002E02 RID: 11778
			[Token(Token = "0x4005D90")]
			First,
			// Token: 0x04002E03 RID: 11779
			[Token(Token = "0x4005D91")]
			PrepareCutInScene,
			// Token: 0x04002E04 RID: 11780
			[Token(Token = "0x4005D92")]
			PlayCutInScene,
			// Token: 0x04002E05 RID: 11781
			[Token(Token = "0x4005D93")]
			WaitCutInScene,
			// Token: 0x04002E06 RID: 11782
			[Token(Token = "0x4005D94")]
			Load,
			// Token: 0x04002E07 RID: 11783
			[Token(Token = "0x4005D95")]
			LoadWait,
			// Token: 0x04002E08 RID: 11784
			[Token(Token = "0x4005D96")]
			PlayIn,
			// Token: 0x04002E09 RID: 11785
			[Token(Token = "0x4005D97")]
			Main,
			// Token: 0x04002E0A RID: 11786
			[Token(Token = "0x4005D98")]
			Unload
		}
	}
}
