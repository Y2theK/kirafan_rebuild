﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Ability;

namespace Star
{
	// Token: 0x0200079B RID: 1947
	[Token(Token = "0x20005D0")]
	[StructLayout(3)]
	public class EditState_CharaListAbility : EditState
	{
		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06001D8F RID: 7567 RVA: 0x0000D2C0 File Offset: 0x0000B4C0
		// (set) Token: 0x06001D90 RID: 7568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000216")]
		public int UseAbilityBoardItemID
		{
			[Token(Token = "0x6001B07")]
			[Address(RVA = "0x1011CD41C", Offset = "0x11CD41C", VA = "0x1011CD41C")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6001B08")]
			[Address(RVA = "0x1011CD424", Offset = "0x11CD424", VA = "0x1011CD424")]
			set
			{
			}
		}

		// Token: 0x06001D91 RID: 7569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B09")]
		[Address(RVA = "0x1011CD42C", Offset = "0x11CD42C", VA = "0x1011CD42C")]
		public EditState_CharaListAbility(EditMain owner)
		{
		}

		// Token: 0x06001D92 RID: 7570 RVA: 0x0000D2D8 File Offset: 0x0000B4D8
		[Token(Token = "0x6001B0A")]
		[Address(RVA = "0x1011CD474", Offset = "0x11CD474", VA = "0x1011CD474", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001D93 RID: 7571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B0B")]
		[Address(RVA = "0x1011CD47C", Offset = "0x11CD47C", VA = "0x1011CD47C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001D94 RID: 7572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B0C")]
		[Address(RVA = "0x1011CD4B4", Offset = "0x11CD4B4", VA = "0x1011CD4B4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001D95 RID: 7573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B0D")]
		[Address(RVA = "0x1011CD4B8", Offset = "0x11CD4B8", VA = "0x1011CD4B8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001D96 RID: 7574 RVA: 0x0000D2F0 File Offset: 0x0000B4F0
		[Token(Token = "0x6001B0E")]
		[Address(RVA = "0x1011CD554", Offset = "0x11CD554", VA = "0x1011CD554", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001D97 RID: 7575 RVA: 0x0000D308 File Offset: 0x0000B508
		[Token(Token = "0x6001B0F")]
		[Address(RVA = "0x1011CD968", Offset = "0x11CD968", VA = "0x1011CD968")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001D98 RID: 7576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B10")]
		[Address(RVA = "0x1011CDD18", Offset = "0x11CDD18", VA = "0x1011CDD18")]
		private void OnClickCharacterIcon(long characterMngId)
		{
		}

		// Token: 0x06001D99 RID: 7577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B11")]
		[Address(RVA = "0x1011CE064", Offset = "0x11CE064", VA = "0x1011CE064", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001D9A RID: 7578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B12")]
		[Address(RVA = "0x1011CE018", Offset = "0x11CE018", VA = "0x1011CE018")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001D9B RID: 7579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B13")]
		[Address(RVA = "0x1011CE070", Offset = "0x11CE070", VA = "0x1011CE070")]
		private void OnEndSelectorUI()
		{
		}

		// Token: 0x06001D9C RID: 7580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B14")]
		[Address(RVA = "0x1011CE144", Offset = "0x11CE144", VA = "0x1011CE144")]
		private void OnCloseConfirmWindowCallback(int buttonIndex)
		{
		}

		// Token: 0x06001D9D RID: 7581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B15")]
		[Address(RVA = "0x1011CE23C", Offset = "0x11CE23C", VA = "0x1011CE23C")]
		private void OnResponseReleaseBoard()
		{
		}

		// Token: 0x04002E0B RID: 11787
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023E8")]
		private EditState_CharaListAbility.eStep m_Step;

		// Token: 0x04002E0C RID: 11788
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023E9")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002E0D RID: 11789
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40023EA")]
		private SceneDefine.eChildSceneID m_UISceneID;

		// Token: 0x04002E0E RID: 11790
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023EB")]
		private AbilityBoardSelectorUI m_SelectorUI;

		// Token: 0x04002E0F RID: 11791
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023EC")]
		private long m_SelectedCharaMngId;

		// Token: 0x04002E10 RID: 11792
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023ED")]
		private int m_UseAbilityBoardItemID;

		// Token: 0x0200079C RID: 1948
		[Token(Token = "0x2000E9C")]
		private enum eStep
		{
			// Token: 0x04002E12 RID: 11794
			[Token(Token = "0x4005D9A")]
			None = -1,
			// Token: 0x04002E13 RID: 11795
			[Token(Token = "0x4005D9B")]
			First,
			// Token: 0x04002E14 RID: 11796
			[Token(Token = "0x4005D9C")]
			LoadWait,
			// Token: 0x04002E15 RID: 11797
			[Token(Token = "0x4005D9D")]
			LoadAbilityBoardSelectorUI,
			// Token: 0x04002E16 RID: 11798
			[Token(Token = "0x4005D9E")]
			WaitAbilityBoardSelectorUI,
			// Token: 0x04002E17 RID: 11799
			[Token(Token = "0x4005D9F")]
			PlayIn,
			// Token: 0x04002E18 RID: 11800
			[Token(Token = "0x4005DA0")]
			Main,
			// Token: 0x04002E19 RID: 11801
			[Token(Token = "0x4005DA1")]
			UnloadResourceWait
		}
	}
}
