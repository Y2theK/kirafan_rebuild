﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200079D RID: 1949
	[Token(Token = "0x20005D1")]
	[StructLayout(3)]
	public class EditState_CharaListEnhance : EditState
	{
		// Token: 0x06001D9E RID: 7582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B16")]
		[Address(RVA = "0x1011CE2C0", Offset = "0x11CE2C0", VA = "0x1011CE2C0")]
		public EditState_CharaListEnhance(EditMain owner)
		{
		}

		// Token: 0x06001D9F RID: 7583 RVA: 0x0000D320 File Offset: 0x0000B520
		[Token(Token = "0x6001B17")]
		[Address(RVA = "0x1011CE2FC", Offset = "0x11CE2FC", VA = "0x1011CE2FC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DA0 RID: 7584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B18")]
		[Address(RVA = "0x1011CE304", Offset = "0x11CE304", VA = "0x1011CE304", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DA1 RID: 7585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B19")]
		[Address(RVA = "0x1011CE30C", Offset = "0x11CE30C", VA = "0x1011CE30C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DA2 RID: 7586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B1A")]
		[Address(RVA = "0x1011CE310", Offset = "0x11CE310", VA = "0x1011CE310", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DA3 RID: 7587 RVA: 0x0000D338 File Offset: 0x0000B538
		[Token(Token = "0x6001B1B")]
		[Address(RVA = "0x1011CE314", Offset = "0x11CE314", VA = "0x1011CE314", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DA4 RID: 7588 RVA: 0x0000D350 File Offset: 0x0000B550
		[Token(Token = "0x6001B1C")]
		[Address(RVA = "0x1011CE784", Offset = "0x11CE784", VA = "0x1011CE784")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DA5 RID: 7589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B1D")]
		[Address(RVA = "0x1011CEA68", Offset = "0x11CEA68", VA = "0x1011CEA68", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DA6 RID: 7590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B1E")]
		[Address(RVA = "0x1011CEA6C", Offset = "0x11CEA6C", VA = "0x1011CEA6C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002E1A RID: 11802
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023EE")]
		private EditState_CharaListEnhance.eStep m_Step;

		// Token: 0x04002E1B RID: 11803
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023EF")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0200079E RID: 1950
		[Token(Token = "0x2000E9D")]
		private enum eStep
		{
			// Token: 0x04002E1D RID: 11805
			[Token(Token = "0x4005DA3")]
			None = -1,
			// Token: 0x04002E1E RID: 11806
			[Token(Token = "0x4005DA4")]
			First,
			// Token: 0x04002E1F RID: 11807
			[Token(Token = "0x4005DA5")]
			LoadWait,
			// Token: 0x04002E20 RID: 11808
			[Token(Token = "0x4005DA6")]
			PlayIn,
			// Token: 0x04002E21 RID: 11809
			[Token(Token = "0x4005DA7")]
			Main
		}
	}
}
