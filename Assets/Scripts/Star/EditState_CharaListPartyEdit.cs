﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200079F RID: 1951
	[Token(Token = "0x20005D2")]
	[StructLayout(3)]
	public class EditState_CharaListPartyEdit : EditState_CharaListPartyEditBase
	{
		// Token: 0x06001DA7 RID: 7591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B1F")]
		[Address(RVA = "0x1011CEAB8", Offset = "0x11CEAB8", VA = "0x1011CEAB8")]
		public EditState_CharaListPartyEdit(EditMain owner)
		{
		}

		// Token: 0x06001DA8 RID: 7592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B20")]
		[Address(RVA = "0x1011CEB30", Offset = "0x11CEB30", VA = "0x1011CEB30", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DA9 RID: 7593 RVA: 0x0000D368 File Offset: 0x0000B568
		[Token(Token = "0x6001B21")]
		[Address(RVA = "0x1011CECE4", Offset = "0x11CECE4", VA = "0x1011CECE4", Slot = "10")]
		public override int GetDefaultNextSceneState()
		{
			return 0;
		}

		// Token: 0x06001DAA RID: 7594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B22")]
		[Address(RVA = "0x1011CECEC", Offset = "0x11CECEC", VA = "0x1011CECEC", Slot = "11")]
		protected override void OnStateUpdateMainOnClickButton()
		{
		}

		// Token: 0x06001DAB RID: 7595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B23")]
		[Address(RVA = "0x1011CF028", Offset = "0x11CF028", VA = "0x1011CF028", Slot = "12")]
		protected override void SetListEditMode()
		{
		}
	}
}
