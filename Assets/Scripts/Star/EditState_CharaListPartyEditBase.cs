﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007A0 RID: 1952
	[Token(Token = "0x20005D3")]
	[StructLayout(3)]
	public abstract class EditState_CharaListPartyEditBase : EditState
	{
		// Token: 0x06001DAC RID: 7596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B24")]
		[Address(RVA = "0x1011CEAF4", Offset = "0x11CEAF4", VA = "0x1011CEAF4")]
		public EditState_CharaListPartyEditBase(EditMain owner)
		{
		}

		// Token: 0x06001DAD RID: 7597 RVA: 0x0000D380 File Offset: 0x0000B580
		[Token(Token = "0x6001B25")]
		[Address(RVA = "0x1011CF134", Offset = "0x11CF134", VA = "0x1011CF134", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DAE RID: 7598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B26")]
		[Address(RVA = "0x1011CF13C", Offset = "0x11CF13C", VA = "0x1011CF13C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DAF RID: 7599 RVA: 0x0000D398 File Offset: 0x0000B598
		[Token(Token = "0x6001B27")]
		[Address(RVA = "0x1011CF144", Offset = "0x11CF144", VA = "0x1011CF144", Slot = "10")]
		public virtual int GetDefaultNextSceneState()
		{
			return 0;
		}

		// Token: 0x06001DB0 RID: 7600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B28")]
		[Address(RVA = "0x1011CF14C", Offset = "0x11CF14C", VA = "0x1011CF14C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DB1 RID: 7601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B29")]
		[Address(RVA = "0x1011CF150", Offset = "0x11CF150", VA = "0x1011CF150", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DB2 RID: 7602 RVA: 0x0000D3B0 File Offset: 0x0000B5B0
		[Token(Token = "0x6001B2A")]
		[Address(RVA = "0x1011CF154", Offset = "0x11CF154", VA = "0x1011CF154", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DB3 RID: 7603
		[Token(Token = "0x6001B2B")]
		[Address(Slot = "11")]
		protected abstract void OnStateUpdateMainOnClickButton();

		// Token: 0x06001DB4 RID: 7604 RVA: 0x0000D3C8 File Offset: 0x0000B5C8
		[Token(Token = "0x6001B2C")]
		[Address(RVA = "0x1011CF388", Offset = "0x11CF388", VA = "0x1011CF388")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DB5 RID: 7605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B2D")]
		[Address(RVA = "0x1011CF5F4", Offset = "0x11CF5F4", VA = "0x1011CF5F4", Slot = "12")]
		protected virtual void SetListEditMode()
		{
		}

		// Token: 0x06001DB6 RID: 7606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B2E")]
		[Address(RVA = "0x1011CEC30", Offset = "0x11CEC30", VA = "0x1011CEC30")]
		protected void GoToMenuEnd()
		{
		}

		// Token: 0x04002E22 RID: 11810
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023F0")]
		private EditState_CharaListPartyEditBase.eStep m_Step;

		// Token: 0x04002E23 RID: 11811
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023F1")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x020007A1 RID: 1953
		[Token(Token = "0x2000E9E")]
		private enum eStep
		{
			// Token: 0x04002E25 RID: 11813
			[Token(Token = "0x4005DA9")]
			None = -1,
			// Token: 0x04002E26 RID: 11814
			[Token(Token = "0x4005DAA")]
			First,
			// Token: 0x04002E27 RID: 11815
			[Token(Token = "0x4005DAB")]
			LoadWait,
			// Token: 0x04002E28 RID: 11816
			[Token(Token = "0x4005DAC")]
			PlayIn,
			// Token: 0x04002E29 RID: 11817
			[Token(Token = "0x4005DAD")]
			Main
		}
	}
}
