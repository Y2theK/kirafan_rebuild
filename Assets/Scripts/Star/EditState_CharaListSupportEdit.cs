﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007A2 RID: 1954
	[Token(Token = "0x20005D4")]
	[StructLayout(3)]
	public class EditState_CharaListSupportEdit : EditState_CharaListPartyEditBase
	{
		// Token: 0x06001DB7 RID: 7607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B2F")]
		[Address(RVA = "0x1011CF700", Offset = "0x11CF700", VA = "0x1011CF700")]
		public EditState_CharaListSupportEdit(EditMain owner)
		{
		}

		// Token: 0x06001DB8 RID: 7608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B30")]
		[Address(RVA = "0x1011CF73C", Offset = "0x11CF73C", VA = "0x1011CF73C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DB9 RID: 7609 RVA: 0x0000D3E0 File Offset: 0x0000B5E0
		[Token(Token = "0x6001B31")]
		[Address(RVA = "0x1011CF814", Offset = "0x11CF814", VA = "0x1011CF814", Slot = "10")]
		public override int GetDefaultNextSceneState()
		{
			return 0;
		}

		// Token: 0x06001DBA RID: 7610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B32")]
		[Address(RVA = "0x1011CF81C", Offset = "0x11CF81C", VA = "0x1011CF81C", Slot = "11")]
		protected override void OnStateUpdateMainOnClickButton()
		{
		}

		// Token: 0x06001DBB RID: 7611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B33")]
		[Address(RVA = "0x1011CFB48", Offset = "0x11CFB48", VA = "0x1011CFB48", Slot = "12")]
		protected override void SetListEditMode()
		{
		}
	}
}
