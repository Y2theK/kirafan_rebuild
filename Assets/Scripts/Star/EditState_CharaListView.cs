﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007A3 RID: 1955
	[Token(Token = "0x20005D5")]
	[StructLayout(3)]
	public class EditState_CharaListView : EditState
	{
		// Token: 0x06001DBC RID: 7612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B34")]
		[Address(RVA = "0x1011CFC4C", Offset = "0x11CFC4C", VA = "0x1011CFC4C")]
		public EditState_CharaListView(EditMain owner)
		{
		}

		// Token: 0x06001DBD RID: 7613 RVA: 0x0000D3F8 File Offset: 0x0000B5F8
		[Token(Token = "0x6001B35")]
		[Address(RVA = "0x1011CFC88", Offset = "0x11CFC88", VA = "0x1011CFC88", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DBE RID: 7614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B36")]
		[Address(RVA = "0x1011CFC90", Offset = "0x11CFC90", VA = "0x1011CFC90", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DBF RID: 7615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B37")]
		[Address(RVA = "0x1011CFC98", Offset = "0x11CFC98", VA = "0x1011CFC98", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DC0 RID: 7616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B38")]
		[Address(RVA = "0x1011CFC9C", Offset = "0x11CFC9C", VA = "0x1011CFC9C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DC1 RID: 7617 RVA: 0x0000D410 File Offset: 0x0000B610
		[Token(Token = "0x6001B39")]
		[Address(RVA = "0x1011CFCA0", Offset = "0x11CFCA0", VA = "0x1011CFCA0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DC2 RID: 7618 RVA: 0x0000D428 File Offset: 0x0000B628
		[Token(Token = "0x6001B3A")]
		[Address(RVA = "0x1011CFEB4", Offset = "0x11CFEB4", VA = "0x1011CFEB4")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DC3 RID: 7619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B3B")]
		[Address(RVA = "0x1011D00FC", Offset = "0x11D00FC", VA = "0x1011D00FC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DC4 RID: 7620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B3C")]
		[Address(RVA = "0x1011D0100", Offset = "0x11D0100", VA = "0x1011D0100")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002E2A RID: 11818
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023F2")]
		private EditState_CharaListView.eStep m_Step;

		// Token: 0x04002E2B RID: 11819
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023F3")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x020007A4 RID: 1956
		[Token(Token = "0x2000E9F")]
		private enum eStep
		{
			// Token: 0x04002E2D RID: 11821
			[Token(Token = "0x4005DAF")]
			None = -1,
			// Token: 0x04002E2E RID: 11822
			[Token(Token = "0x4005DB0")]
			First,
			// Token: 0x04002E2F RID: 11823
			[Token(Token = "0x4005DB1")]
			LoadWait,
			// Token: 0x04002E30 RID: 11824
			[Token(Token = "0x4005DB2")]
			PlayIn,
			// Token: 0x04002E31 RID: 11825
			[Token(Token = "0x4005DB3")]
			Main
		}
	}
}
