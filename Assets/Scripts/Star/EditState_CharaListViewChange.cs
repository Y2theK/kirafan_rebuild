﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007A5 RID: 1957
	[Token(Token = "0x20005D6")]
	[StructLayout(3)]
	public class EditState_CharaListViewChange : EditState
	{
		// Token: 0x06001DC5 RID: 7621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B3D")]
		[Address(RVA = "0x1011D014C", Offset = "0x11D014C", VA = "0x1011D014C")]
		public EditState_CharaListViewChange(EditMain owner)
		{
		}

		// Token: 0x06001DC6 RID: 7622 RVA: 0x0000D440 File Offset: 0x0000B640
		[Token(Token = "0x6001B3E")]
		[Address(RVA = "0x1011D0188", Offset = "0x11D0188", VA = "0x1011D0188", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DC7 RID: 7623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B3F")]
		[Address(RVA = "0x1011D0190", Offset = "0x11D0190", VA = "0x1011D0190", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DC8 RID: 7624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B40")]
		[Address(RVA = "0x1011D0198", Offset = "0x11D0198", VA = "0x1011D0198", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DC9 RID: 7625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B41")]
		[Address(RVA = "0x1011D019C", Offset = "0x11D019C", VA = "0x1011D019C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DCA RID: 7626 RVA: 0x0000D458 File Offset: 0x0000B658
		[Token(Token = "0x6001B42")]
		[Address(RVA = "0x1011D01A0", Offset = "0x11D01A0", VA = "0x1011D01A0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DCB RID: 7627 RVA: 0x0000D470 File Offset: 0x0000B670
		[Token(Token = "0x6001B43")]
		[Address(RVA = "0x1011D04BC", Offset = "0x11D04BC", VA = "0x1011D04BC")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DCC RID: 7628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B44")]
		[Address(RVA = "0x1011D0704", Offset = "0x11D0704", VA = "0x1011D0704", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DCD RID: 7629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B45")]
		[Address(RVA = "0x1011D0708", Offset = "0x11D0708", VA = "0x1011D0708")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002E32 RID: 11826
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023F4")]
		private EditState_CharaListViewChange.eStep m_Step;

		// Token: 0x04002E33 RID: 11827
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023F5")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x020007A6 RID: 1958
		[Token(Token = "0x2000EA0")]
		private enum eStep
		{
			// Token: 0x04002E35 RID: 11829
			[Token(Token = "0x4005DB5")]
			None = -1,
			// Token: 0x04002E36 RID: 11830
			[Token(Token = "0x4005DB6")]
			First,
			// Token: 0x04002E37 RID: 11831
			[Token(Token = "0x4005DB7")]
			LoadWait,
			// Token: 0x04002E38 RID: 11832
			[Token(Token = "0x4005DB8")]
			PlayIn,
			// Token: 0x04002E39 RID: 11833
			[Token(Token = "0x4005DB9")]
			Main
		}
	}
}
