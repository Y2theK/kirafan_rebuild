﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020007A7 RID: 1959
	[Token(Token = "0x20005D7")]
	[StructLayout(3)]
	public class EditState_EditTop : EditState
	{
		// Token: 0x06001DCE RID: 7630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B46")]
		[Address(RVA = "0x1011D0754", Offset = "0x11D0754", VA = "0x1011D0754")]
		public EditState_EditTop(EditMain owner)
		{
		}

		// Token: 0x06001DCF RID: 7631 RVA: 0x0000D488 File Offset: 0x0000B688
		[Token(Token = "0x6001B47")]
		[Address(RVA = "0x1011D0790", Offset = "0x11D0790", VA = "0x1011D0790", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DD0 RID: 7632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B48")]
		[Address(RVA = "0x1011D0798", Offset = "0x11D0798", VA = "0x1011D0798", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DD1 RID: 7633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B49")]
		[Address(RVA = "0x1011D07F4", Offset = "0x11D07F4", VA = "0x1011D07F4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DD2 RID: 7634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B4A")]
		[Address(RVA = "0x1011D07F8", Offset = "0x11D07F8", VA = "0x1011D07F8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DD3 RID: 7635 RVA: 0x0000D4A0 File Offset: 0x0000B6A0
		[Token(Token = "0x6001B4B")]
		[Address(RVA = "0x1011D0800", Offset = "0x11D0800", VA = "0x1011D0800", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DD4 RID: 7636 RVA: 0x0000D4B8 File Offset: 0x0000B6B8
		[Token(Token = "0x6001B4C")]
		[Address(RVA = "0x1011D0DBC", Offset = "0x11D0DBC", VA = "0x1011D0DBC")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DD5 RID: 7637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B4D")]
		[Address(RVA = "0x1011D1090", Offset = "0x11D1090", VA = "0x1011D1090", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DD6 RID: 7638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B4E")]
		[Address(RVA = "0x1011D12CC", Offset = "0x11D12CC", VA = "0x1011D12CC")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001DD7 RID: 7639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B4F")]
		[Address(RVA = "0x1011D1298", Offset = "0x11D1298", VA = "0x1011D1298")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002E3A RID: 11834
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023F6")]
		private EditState_EditTop.eStep m_Step;

		// Token: 0x04002E3B RID: 11835
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023F7")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002E3C RID: 11836
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023F8")]
		private EditTopUI m_UI;

		// Token: 0x020007A8 RID: 1960
		[Token(Token = "0x2000EA1")]
		private enum eStep
		{
			// Token: 0x04002E3E RID: 11838
			[Token(Token = "0x4005DBB")]
			None = -1,
			// Token: 0x04002E3F RID: 11839
			[Token(Token = "0x4005DBC")]
			First,
			// Token: 0x04002E40 RID: 11840
			[Token(Token = "0x4005DBD")]
			LoadWait,
			// Token: 0x04002E41 RID: 11841
			[Token(Token = "0x4005DBE")]
			PlayIn,
			// Token: 0x04002E42 RID: 11842
			[Token(Token = "0x4005DBF")]
			Main,
			// Token: 0x04002E43 RID: 11843
			[Token(Token = "0x4005DC0")]
			UnloadChildSceneWait
		}
	}
}
