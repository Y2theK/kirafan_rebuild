﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007A9 RID: 1961
	[Token(Token = "0x20005D8")]
	[StructLayout(3)]
	public class EditState_Enhance : EditState
	{
		// Token: 0x06001DD8 RID: 7640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B50")]
		[Address(RVA = "0x1011D12D0", Offset = "0x11D12D0", VA = "0x1011D12D0")]
		public EditState_Enhance(EditMain owner)
		{
		}

		// Token: 0x06001DD9 RID: 7641 RVA: 0x0000D4D0 File Offset: 0x0000B6D0
		[Token(Token = "0x6001B51")]
		[Address(RVA = "0x1011D1304", Offset = "0x11D1304", VA = "0x1011D1304", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DDA RID: 7642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B52")]
		[Address(RVA = "0x1011D130C", Offset = "0x11D130C", VA = "0x1011D130C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DDB RID: 7643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B53")]
		[Address(RVA = "0x1011D1314", Offset = "0x11D1314", VA = "0x1011D1314", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DDC RID: 7644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B54")]
		[Address(RVA = "0x1011D1318", Offset = "0x11D1318", VA = "0x1011D1318", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DDD RID: 7645 RVA: 0x0000D4E8 File Offset: 0x0000B6E8
		[Token(Token = "0x6001B55")]
		[Address(RVA = "0x1011D131C", Offset = "0x11D131C", VA = "0x1011D131C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DDE RID: 7646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B56")]
		[Address(RVA = "0x1011D180C", Offset = "0x11D180C", VA = "0x1011D180C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DDF RID: 7647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B57")]
		[Address(RVA = "0x1011D1810", Offset = "0x11D1810", VA = "0x1011D1810")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x06001DE0 RID: 7648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B58")]
		[Address(RVA = "0x1011D1814", Offset = "0x11D1814", VA = "0x1011D1814")]
		private void OnCloseCharaDrtailPlayer()
		{
		}

		// Token: 0x04002E44 RID: 11844
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023F9")]
		private EditState_Enhance.eStep m_Step;

		// Token: 0x020007AA RID: 1962
		[Token(Token = "0x2000EA2")]
		private enum eStep
		{
			// Token: 0x04002E46 RID: 11846
			[Token(Token = "0x4005DC2")]
			None = -1,
			// Token: 0x04002E47 RID: 11847
			[Token(Token = "0x4005DC3")]
			First,
			// Token: 0x04002E48 RID: 11848
			[Token(Token = "0x4005DC4")]
			Main
		}
	}
}
