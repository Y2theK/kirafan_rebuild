﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007AB RID: 1963
	[Token(Token = "0x20005D9")]
	[StructLayout(3)]
	public class EditState_Init : EditState
	{
		// Token: 0x06001DE1 RID: 7649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B59")]
		[Address(RVA = "0x1011D188C", Offset = "0x11D188C", VA = "0x1011D188C")]
		public EditState_Init(EditMain owner)
		{
		}

		// Token: 0x06001DE2 RID: 7650 RVA: 0x0000D500 File Offset: 0x0000B700
		[Token(Token = "0x6001B5A")]
		[Address(RVA = "0x1011D18C0", Offset = "0x11D18C0", VA = "0x1011D18C0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DE3 RID: 7651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B5B")]
		[Address(RVA = "0x1011D18C8", Offset = "0x11D18C8", VA = "0x1011D18C8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B5C")]
		[Address(RVA = "0x1011D18D0", Offset = "0x11D18D0", VA = "0x1011D18D0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B5D")]
		[Address(RVA = "0x1011D18D4", Offset = "0x11D18D4", VA = "0x1011D18D4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DE6 RID: 7654 RVA: 0x0000D518 File Offset: 0x0000B718
		[Token(Token = "0x6001B5E")]
		[Address(RVA = "0x1011D18D8", Offset = "0x11D18D8", VA = "0x1011D18D8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B5F")]
		[Address(RVA = "0x1011D1CA4", Offset = "0x11D1CA4", VA = "0x1011D1CA4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002E49 RID: 11849
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023FA")]
		private EditState_Init.eStep m_Step;

		// Token: 0x020007AC RID: 1964
		[Token(Token = "0x2000EA3")]
		private enum eStep
		{
			// Token: 0x04002E4B RID: 11851
			[Token(Token = "0x4005DC6")]
			None = -1,
			// Token: 0x04002E4C RID: 11852
			[Token(Token = "0x4005DC7")]
			First,
			// Token: 0x04002E4D RID: 11853
			[Token(Token = "0x4005DC8")]
			Load_Wait,
			// Token: 0x04002E4E RID: 11854
			[Token(Token = "0x4005DC9")]
			End
		}
	}
}
