﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020007AD RID: 1965
	[Token(Token = "0x20005DA")]
	[StructLayout(3)]
	public class EditState_PartyEdit : EditState_PartyEditBase<PartyEditUI>
	{
		// Token: 0x06001DE8 RID: 7656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B60")]
		[Address(RVA = "0x1011D1CA8", Offset = "0x11D1CA8", VA = "0x1011D1CA8")]
		public EditState_PartyEdit(EditMain owner)
		{
		}

		// Token: 0x06001DE9 RID: 7657 RVA: 0x0000D530 File Offset: 0x0000B730
		[Token(Token = "0x6001B61")]
		[Address(RVA = "0x1011D1D08", Offset = "0x11D1D08", VA = "0x1011D1D08", Slot = "10")]
		public override SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06001DEA RID: 7658 RVA: 0x0000D548 File Offset: 0x0000B748
		[Token(Token = "0x6001B62")]
		[Address(RVA = "0x1011D1D10", Offset = "0x11D1D10", VA = "0x1011D1D10", Slot = "11")]
		public override int GetCharaListSceneId()
		{
			return 0;
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B63")]
		[Address(RVA = "0x1011D1D18", Offset = "0x11D1D18", VA = "0x1011D1D18", Slot = "13")]
		public override void SetSelectedPartyIndex(int index)
		{
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B64")]
		[Address(RVA = "0x1011D1DB8", Offset = "0x11D1DB8", VA = "0x1011D1DB8", Slot = "14")]
		public override void SetSelectedPartyMemberIndex(int index)
		{
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B65")]
		[Address(RVA = "0x1011D1E38", Offset = "0x11D1E38", VA = "0x1011D1E38", Slot = "15")]
		public override void SetSelectedCharaMngId(long charaMngId)
		{
		}

		// Token: 0x06001DEE RID: 7662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B66")]
		[Address(RVA = "0x1011D1EB8", Offset = "0x11D1EB8", VA = "0x1011D1EB8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001DEF RID: 7663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B67")]
		[Address(RVA = "0x1011D1FB4", Offset = "0x11D1FB4", VA = "0x1011D1FB4", Slot = "12")]
		protected override void SetSceneInfo()
		{
		}

		// Token: 0x06001DF0 RID: 7664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B68")]
		[Address(RVA = "0x1011D204C", Offset = "0x11D204C", VA = "0x1011D204C", Slot = "16")]
		protected override void RequestSetPartyName(string partyName)
		{
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B69")]
		[Address(RVA = "0x1011D226C", Offset = "0x11D226C", VA = "0x1011D226C", Slot = "17")]
		protected override void OpenTutorialTipsWindow()
		{
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B6A")]
		[Address(RVA = "0x1011D20F4", Offset = "0x11D20F4", VA = "0x1011D20F4")]
		public void Request_BattlePartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x06001DF3 RID: 7667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B6B")]
		[Address(RVA = "0x1011D22F0", Offset = "0x11D22F0", VA = "0x1011D22F0")]
		protected void OnResponse_BattlePartySetName(MeigewwwParam wwwParam)
		{
		}
	}
}
