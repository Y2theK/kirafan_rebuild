﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020007AE RID: 1966
	[Token(Token = "0x20005DB")]
	[StructLayout(3)]
	public class EditState_PartyEditBase<UIType> : EditState where UIType : PartyEditUIBase
	{
		// Token: 0x06001DF4 RID: 7668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B6C")]
		[Address(RVA = "0x1016CBEF8", Offset = "0x16CBEF8", VA = "0x1016CBEF8")]
		public EditState_PartyEditBase(EditMain owner)
		{
		}

		// Token: 0x06001DF5 RID: 7669 RVA: 0x0000D560 File Offset: 0x0000B760
		[Token(Token = "0x6001B6D")]
		[Address(RVA = "0x1016CBF0C", Offset = "0x16CBF0C", VA = "0x1016CBF0C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001DF6 RID: 7670 RVA: 0x0000D578 File Offset: 0x0000B778
		[Token(Token = "0x6001B6E")]
		[Address(RVA = "0x1016CBF14", Offset = "0x16CBF14", VA = "0x1016CBF14", Slot = "10")]
		public virtual SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06001DF7 RID: 7671 RVA: 0x0000D590 File Offset: 0x0000B790
		[Token(Token = "0x6001B6F")]
		[Address(RVA = "0x1016CBF1C", Offset = "0x16CBF1C", VA = "0x1016CBF1C", Slot = "11")]
		public virtual int GetCharaListSceneId()
		{
			return 0;
		}

		// Token: 0x06001DF8 RID: 7672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B70")]
		[Address(RVA = "0x1016CBF24", Offset = "0x16CBF24", VA = "0x1016CBF24", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001DF9 RID: 7673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B71")]
		[Address(RVA = "0x1016CBF2C", Offset = "0x16CBF2C", VA = "0x1016CBF2C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001DFA RID: 7674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B72")]
		[Address(RVA = "0x1016CBF30", Offset = "0x16CBF30", VA = "0x1016CBF30", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001DFB RID: 7675 RVA: 0x0000D5A8 File Offset: 0x0000B7A8
		[Token(Token = "0x6001B73")]
		[Address(RVA = "0x1016CBF38", Offset = "0x16CBF38", VA = "0x1016CBF38", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001DFC RID: 7676 RVA: 0x0000D5C0 File Offset: 0x0000B7C0
		[Token(Token = "0x6001B74")]
		[Address(RVA = "0x1016CC264", Offset = "0x16CC264", VA = "0x1016CC264")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B75")]
		[Address(RVA = "0x1016CC4D8", Offset = "0x16CC4D8", VA = "0x1016CC4D8", Slot = "12")]
		protected virtual void SetSceneInfo()
		{
		}

		// Token: 0x06001DFE RID: 7678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B76")]
		[Address(RVA = "0x1016CC558", Offset = "0x16CC558", VA = "0x1016CC558")]
		protected void GoToMenuEnd()
		{
		}

		// Token: 0x06001DFF RID: 7679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B77")]
		[Address(RVA = "0x1016CC5F4", Offset = "0x16CC5F4", VA = "0x1016CC5F4", Slot = "13")]
		public virtual void SetSelectedPartyIndex(int index)
		{
		}

		// Token: 0x06001E00 RID: 7680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B78")]
		[Address(RVA = "0x1016CC5F8", Offset = "0x16CC5F8", VA = "0x1016CC5F8", Slot = "14")]
		public virtual void SetSelectedPartyMemberIndex(int index)
		{
		}

		// Token: 0x06001E01 RID: 7681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B79")]
		[Address(RVA = "0x1016CC5FC", Offset = "0x16CC5FC", VA = "0x1016CC5FC", Slot = "15")]
		public virtual void SetSelectedCharaMngId(long charaMngId)
		{
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7A")]
		[Address(RVA = "0x1016CC600", Offset = "0x16CC600", VA = "0x1016CC600", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7B")]
		[Address(RVA = "0x1016CC654", Offset = "0x16CC654", VA = "0x1016CC654")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001E04 RID: 7684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7C")]
		[Address(RVA = "0x1016CC690", Offset = "0x16CC690", VA = "0x1016CC690")]
		protected void OnDecideNameChange(string partyName)
		{
		}

		// Token: 0x06001E05 RID: 7685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7D")]
		[Address(RVA = "0x1016CC6CC", Offset = "0x16CC6CC", VA = "0x1016CC6CC", Slot = "16")]
		protected virtual void RequestSetPartyName(string partyName)
		{
		}

		// Token: 0x06001E06 RID: 7686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7E")]
		[Address(RVA = "0x1016CC6D0", Offset = "0x16CC6D0", VA = "0x1016CC6D0", Slot = "17")]
		protected virtual void OpenTutorialTipsWindow()
		{
		}

		// Token: 0x04002E4F RID: 11855
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40023FB")]
		private EditState_PartyEditBase<UIType>.eStep m_Step;

		// Token: 0x04002E50 RID: 11856
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40023FC")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002E51 RID: 11857
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40023FD")]
		public UIType m_UI;

		// Token: 0x04002E52 RID: 11858
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40023FE")]
		protected string m_RequestedPartyName;

		// Token: 0x020007AF RID: 1967
		[Token(Token = "0x2000EA4")]
		private enum eStep
		{
			// Token: 0x04002E54 RID: 11860
			[Token(Token = "0x4005DCB")]
			None = -1,
			// Token: 0x04002E55 RID: 11861
			[Token(Token = "0x4005DCC")]
			First,
			// Token: 0x04002E56 RID: 11862
			[Token(Token = "0x4005DCD")]
			LoadWait,
			// Token: 0x04002E57 RID: 11863
			[Token(Token = "0x4005DCE")]
			PlayIn,
			// Token: 0x04002E58 RID: 11864
			[Token(Token = "0x4005DCF")]
			Main,
			// Token: 0x04002E59 RID: 11865
			[Token(Token = "0x4005DD0")]
			UnloadChildSceneWait
		}
	}
}
