﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020007B0 RID: 1968
	[Token(Token = "0x20005DC")]
	[StructLayout(3)]
	public class EditState_SupportEdit : EditState_PartyEditBase<SupportEditUI>
	{
		// Token: 0x06001E07 RID: 7687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B7F")]
		[Address(RVA = "0x1011D24D0", Offset = "0x11D24D0", VA = "0x1011D24D0")]
		public EditState_SupportEdit(EditMain owner)
		{
		}

		// Token: 0x06001E08 RID: 7688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B80")]
		[Address(RVA = "0x1011D2530", Offset = "0x11D2530", VA = "0x1011D2530", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E09 RID: 7689 RVA: 0x0000D5D8 File Offset: 0x0000B7D8
		[Token(Token = "0x6001B81")]
		[Address(RVA = "0x1011D25C4", Offset = "0x11D25C4", VA = "0x1011D25C4", Slot = "10")]
		public override SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06001E0A RID: 7690 RVA: 0x0000D5F0 File Offset: 0x0000B7F0
		[Token(Token = "0x6001B82")]
		[Address(RVA = "0x1011D25CC", Offset = "0x11D25CC", VA = "0x1011D25CC", Slot = "11")]
		public override int GetCharaListSceneId()
		{
			return 0;
		}

		// Token: 0x06001E0B RID: 7691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B83")]
		[Address(RVA = "0x1011D25D4", Offset = "0x11D25D4", VA = "0x1011D25D4", Slot = "13")]
		public override void SetSelectedPartyIndex(int index)
		{
		}

		// Token: 0x06001E0C RID: 7692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B84")]
		[Address(RVA = "0x1011D2654", Offset = "0x11D2654", VA = "0x1011D2654", Slot = "14")]
		public override void SetSelectedPartyMemberIndex(int index)
		{
		}

		// Token: 0x06001E0D RID: 7693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B85")]
		[Address(RVA = "0x1011D26D4", Offset = "0x11D26D4", VA = "0x1011D26D4", Slot = "15")]
		public override void SetSelectedCharaMngId(long charaMngId)
		{
		}

		// Token: 0x06001E0E RID: 7694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B86")]
		[Address(RVA = "0x1011D2754", Offset = "0x11D2754", VA = "0x1011D2754", Slot = "17")]
		protected override void OpenTutorialTipsWindow()
		{
		}

		// Token: 0x06001E0F RID: 7695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B87")]
		[Address(RVA = "0x1011D27D8", Offset = "0x11D27D8", VA = "0x1011D27D8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001E10 RID: 7696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B88")]
		[Address(RVA = "0x1011D290C", Offset = "0x11D290C", VA = "0x1011D290C", Slot = "12")]
		protected override void SetSceneInfo()
		{
		}

		// Token: 0x06001E11 RID: 7697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B89")]
		[Address(RVA = "0x1011D29A4", Offset = "0x11D29A4", VA = "0x1011D29A4", Slot = "16")]
		protected override void RequestSetPartyName(string partyName)
		{
		}

		// Token: 0x06001E12 RID: 7698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B8A")]
		[Address(RVA = "0x1011D2A4C", Offset = "0x11D2A4C", VA = "0x1011D2A4C")]
		public void Request_SupportPartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x06001E13 RID: 7699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B8B")]
		[Address(RVA = "0x1011D2BC4", Offset = "0x11D2BC4", VA = "0x1011D2BC4")]
		protected void OnResponse_SupportPartySetName(MeigewwwParam wwwParam)
		{
		}
	}
}
