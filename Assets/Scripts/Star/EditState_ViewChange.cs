﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020007B1 RID: 1969
	[Token(Token = "0x20005DD")]
	[StructLayout(3)]
	public class EditState_ViewChange : EditState
	{
		// Token: 0x06001E14 RID: 7700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B8C")]
		[Address(RVA = "0x1011D2DA4", Offset = "0x11D2DA4", VA = "0x1011D2DA4")]
		public EditState_ViewChange(EditMain owner)
		{
		}

		// Token: 0x06001E15 RID: 7701 RVA: 0x0000D608 File Offset: 0x0000B808
		[Token(Token = "0x6001B8D")]
		[Address(RVA = "0x1011D2DE0", Offset = "0x11D2DE0", VA = "0x1011D2DE0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E16 RID: 7702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B8E")]
		[Address(RVA = "0x1011D2DE8", Offset = "0x11D2DE8", VA = "0x1011D2DE8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E17 RID: 7703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B8F")]
		[Address(RVA = "0x1011D2DF0", Offset = "0x11D2DF0", VA = "0x1011D2DF0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E18 RID: 7704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B90")]
		[Address(RVA = "0x1011D2DF4", Offset = "0x11D2DF4", VA = "0x1011D2DF4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E19 RID: 7705 RVA: 0x0000D620 File Offset: 0x0000B820
		[Token(Token = "0x6001B91")]
		[Address(RVA = "0x1011D2DF8", Offset = "0x11D2DF8", VA = "0x1011D2DF8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001E1A RID: 7706 RVA: 0x0000D638 File Offset: 0x0000B838
		[Token(Token = "0x6001B92")]
		[Address(RVA = "0x1011D2FA4", Offset = "0x11D2FA4", VA = "0x1011D2FA4")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001E1B RID: 7707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B93")]
		[Address(RVA = "0x1011D3190", Offset = "0x11D3190", VA = "0x1011D3190", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001E1C RID: 7708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B94")]
		[Address(RVA = "0x1011D31C4", Offset = "0x11D31C4", VA = "0x1011D31C4")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002E5A RID: 11866
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40023FF")]
		private EditState_ViewChange.eStep m_Step;

		// Token: 0x04002E5B RID: 11867
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002400")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002E5C RID: 11868
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002401")]
		private EditViewChangeUI m_UI;

		// Token: 0x020007B2 RID: 1970
		[Token(Token = "0x2000EA5")]
		private enum eStep
		{
			// Token: 0x04002E5E RID: 11870
			[Token(Token = "0x4005DD2")]
			None = -1,
			// Token: 0x04002E5F RID: 11871
			[Token(Token = "0x4005DD3")]
			First,
			// Token: 0x04002E60 RID: 11872
			[Token(Token = "0x4005DD4")]
			LoadWait,
			// Token: 0x04002E61 RID: 11873
			[Token(Token = "0x4005DD5")]
			PlayIn,
			// Token: 0x04002E62 RID: 11874
			[Token(Token = "0x4005DD6")]
			Main
		}
	}
}
