﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Star {
    public static class EditUtility {
        public static void SimulateCharaLv(int charaID, int maxLv, long currentExp, long addExp, out int out_afterLv, out long out_afterExp, out long out_getExp) {
            CharacterExpDB charaExpDB = GameSystem.Inst.DbMng.CharaExpDB;
            out_afterLv = 1;
            out_afterExp = 0L;
            long simExp = addExp + currentExp;
            while (simExp > 0L && out_afterLv < maxLv) {
                int nextExp = charaExpDB.GetNextExp(out_afterLv);
                if (nextExp <= 0) {
                    break;
                }
                if (nextExp > simExp) {
                    out_afterExp += simExp;
                    break;
                }
                out_afterLv++;
                out_afterExp += nextExp;
                simExp -= nextExp;
            }
            out_getExp = out_afterExp - currentExp;
        }

        public static long GetCharaNowExp(int currentLv, long exp) {
            CharacterExpDB charaExpDB = GameSystem.Inst.DbMng.CharaExpDB;
            int preExp = 0;
            for (int i = 1; i < currentLv; i++) {
                preExp += charaExpDB.GetNextExp(i);
            }
            if (exp >= preExp) {
                return exp - preExp;
            }
            Debug.Log($"GetCharaNowExp() is error.  exp: {exp}, pre: {preExp}");
            return 0L;
        }

        public static long GetCharaNextMaxExp(int lv) {
            return GameSystem.Inst.DbMng.CharaExpDB.GetNextExp(lv);
        }

        public static int CalcCharaCost(int charaID, int arousalLv) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            eRare rare = (eRare)charaParam.m_Rare;
            int cost = charaParam.m_Cost;
            ArousalLevelsDB_Param? arousalParam = dbMng.ArousalLevelsDB.GetParam(rare, arousalLv);
            if (arousalParam.HasValue) {
                cost -= (int)arousalParam.Value.m_MinusCost;
                cost = cost > 0 ? cost : 0;
            }
            return cost;
        }

        public static int CalcPartyTotalCost(UserBattlePartyData userBattlePartyData) {
            if (userBattlePartyData == null) {
                return 0;
            }
            int cost = 0;
            UserDataManager usrData = GameSystem.Inst.UserDataMng;
            int slotNum = userBattlePartyData.GetSlotNum();
            for (int i = 0; i < slotNum; i++) {
                long member = userBattlePartyData.GetMemberAt(i);
                UserCharacterData charaData = usrData.GetUserCharaData(member);
                if (charaData != null) {
                    cost += charaData.Param.Cost;
                }
                long weapon = userBattlePartyData.GetWeaponAt(i);
                UserWeaponData wpnData = usrData.GetUserWpnData(weapon);
                if (wpnData != null) {
                    cost += wpnData.Param.Cost;
                }
            }
            return cost;
        }

        public static int CalcMaxLv(int charaID) {
            CharacterListDB_Param param = GameSystem.Inst.DbMng.CharaListDB.GetParam(charaID);
            return CalcMaxLv(ref param);
        }

        public static int CalcMaxLv(ref CharacterListDB_Param charaListParam) {
            int add = CalcTotalLvUpValuByLimitBreak(charaListParam.m_CharaID);
            return charaListParam.m_InitLimitLv + add;
        }

        public static int CalcMaxLvFromLimitBreak(int charaID, int limitBreak) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            CharacterLimitBreakListDB_Param lbParam = dbMng.CharaLimitBreakListDB.GetParam(charaParam.m_LimitBreakRecipeID);
            if (limitBreak >= lbParam.m_CharaMaxLvUps.Length) {
                limitBreak = lbParam.m_CharaMaxLvUps.Length;
            }
            int lvs = 0;
            for (int i = 0; i < limitBreak; i++) {
                lvs += lbParam.m_CharaMaxLvUps[i];
            }
            return charaParam.m_InitLimitLv + lvs;
        }

        public static bool IsArousalMax(UserCharacterData userCharaData) {
            return GameSystem.Inst.DbMng.ArousalLevelsDB.IsMax(userCharaData.Param.RareType, userCharaData.Param.ArousalLv);
        }

        public static int GetArousalDuplicatedCountRemainToMax(UserCharacterData userCharaData) {
            if (IsArousalMax(userCharaData)) {
                return 0;
            }
            int dupes = GameSystem.Inst.DbMng.ArousalLevelsDB.GetDuplicatedCountForMaxLv(userCharaData.Param.RareType);
            return Mathf.Max(dupes - userCharaData.Param.DuplicatedCount, 0);
        }

        public static bool IsFriendShipMax(int charaID, int friendShip) {
            eCharaNamedType namedType = (eCharaNamedType)GameSystem.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
            return IsFriendShipMax(namedType, friendShip);
        }

        public static bool IsFriendShipMax(eCharaNamedType namedType, int friendShip) {
            sbyte tableId = GameSystem.Inst.DbMng.NamedListDB.GetFriendshipTableID(namedType);
            return IsFriendshipMax(tableId, friendShip);
        }

        public static bool IsFriendshipMax(sbyte friendShipTableID, int friendShip) {
            int max = GameSystem.Inst.DbMng.NamedFriendshipExpDB.GetFriendShipMax(friendShipTableID);
            return friendShip >= max;
        }

        public static int CalcSkillLvFromTotalUseNum(int maxLv, long totalUseNum, sbyte expTableID) {
            SkillExpDB skillExpDB = GameSystem.Inst.DbMng.SkillExpDB;
            int lv = 1;
            while (totalUseNum > 0L) {
                int nextExp = skillExpDB.GetNextExp(lv, expTableID);
                if (totalUseNum < nextExp) {
                    break;
                }
                totalUseNum -= nextExp;
                lv++;
            }
            if (lv > maxLv) {
                lv = maxLv;
            }
            return lv;
        }

        public static long GetSkillNowExp(int currentLv, long exp, sbyte tableID) {
            SkillExpDB skillExpDB = GameSystem.Inst.DbMng.SkillExpDB;
            int preExp = 0;
            for (int i = 1; i < currentLv; i++) {
                preExp += skillExpDB.GetNextExp(i, tableID);
            }
            if (exp >= preExp) {
                return exp - preExp;
            }
            Debug.Log($"GetSkillNowExp() is error.  exp: {exp}, pre: {preExp}");
            return 0L;
        }

        public static int CalcFriendshipLv(long totalExp, sbyte tableID) {
            int lv = 1;
            while (true) {
                int exp = GameSystem.Inst.DbMng.NamedFriendshipExpDB.GetNextExp(lv, tableID);
                if (exp <= 0 || totalExp < exp) {
                    break;
                }
                lv++;
                totalExp -= exp;
            }
            return lv;
        }

        public static List<long> GetHaveCharaList(eCharaNamedType namedType) {
            List<long> list = new List<long>();
            List<UserCharacterData> charas = GameSystem.Inst.UserDataMng.UserCharaDatas;
            for (int i = 0; i < charas.Count; i++) {
                CharacterParam param = charas[i].Param;
                if (param.NamedType == namedType) {
                    list.Add(param.MngID);
                }
            }
            return list;
        }

        public static int GetHaveNamedToCharaID(eCharaNamedType namedType) {
            int id = -1;
            eRare rare = eRare.Star1;
            List<UserCharacterData> charas = GameSystem.Inst.UserDataMng.UserCharaDatas;
            for (int i = 0; i < charas.Count; i++) {
                CharacterParam p = charas[i].Param;
                if (p.NamedType == namedType && (p.RareType > rare || (p.RareType == rare && (id == -1 || id > p.CharaID)))) {
                    id = p.CharaID;
                    rare = p.RareType;
                }
            }
            return id;
        }

        public static List<int> GetHaveNamedToCharaIDList(eCharaNamedType namedType) {
            List<long> charas = GetHaveCharaList(namedType);
            List<int> list = new List<int>();
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            for (int i = 0; i < charas.Count; i++) {
                list.Add(userData.GetUserCharaData(charas[i]).Param.CharaID);
            }
            list.Sort();
            return list;
        }

        public static UserBattlePartyData GetBattlePartyUserBattlePartyData(int partyIndex) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            if (userData == null) {
                Debug.LogError("Invalid UserDataMng");
                return null;
            }
            if (userData.UserBattlePartyDatas == null) {
                Debug.LogError("Invalid UserDataMng");
                return null;
            }
            if (partyIndex < 0) {
                Debug.LogError("Invalid PartyIndex");
                return null;
            }
            if (userData.UserBattlePartyDatas.Count <= partyIndex) {
                Debug.LogError("Invalid PartyIndex");
                return null;
            }
            return userData.UserBattlePartyDatas[partyIndex];
        }

        public static bool IsEmptyBattleParty(int partyIndex) {
            List<UserBattlePartyData> parties = GameSystem.Inst.UserDataMng.UserBattlePartyDatas;
            return parties[partyIndex].GetAvailableMemberMngIDs().Count == 0;
        }

        public static bool IsAvailableParty(int partyIndex) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserBattlePartyData party = userData.UserBattlePartyDatas[partyIndex];
            if (party.GetMemberAt(0) == -1L && party.GetMemberAt(1) == -1L && party.GetMemberAt(2) == -1L) {
                return false;
            }
            int cost = CalcPartyTotalCost(party);
            return cost <= userData.UserData.PartyCost;
        }

        public static bool CanBeRemoveFromParty(int partyIndex, int memberIndex) {
            List<UserBattlePartyData> parties = GameSystem.Inst.UserDataMng.UserBattlePartyDatas;
            return parties[partyIndex].GetMemberAt(memberIndex) != -1L;
        }

        public static List<int> GetCharaIDListEvolutionBeforeAfterIncluding(int charaID) {
            List<int> list = new List<int>();
            if (charaID > 0) {
                list.Add(charaID);
                int id = charaID;
                CharacterEvolutionListDB_Param param;
                while (GameSystem.Inst.DbMng.CharaEvolutionListDB.GetParamByDestCharaID(id, out param)) {
                    list.Add(param.m_SrcCharaID);
                    id = param.m_SrcCharaID;
                }
                id = charaID;
                while (GameSystem.Inst.DbMng.CharaEvolutionListDB.GetParamBySrcCharaID(id, out param)) {
                    list.Add(param.m_DestCharaID);
                    id = param.m_DestCharaID;
                }
            }
            return list;
        }

        public static bool IsSameCharacterEvolutionBeforeAfterIncluding(int charaA, int charaB) {
            if (charaA == charaB) {
                return true;
            }
            return GetCharaIDListEvolutionBeforeAfterIncluding(charaA).Any(c => c == charaB);
        }

        public static bool IsSameCharaOnChange(int partyIndex, int memberIndex, long changeCharaMngID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserCharacterData chara = userData.GetUserCharaData(changeCharaMngID);
            if (chara == null) {
                return false;
            }
            UserBattlePartyData party = userData.UserBattlePartyDatas[partyIndex];
            int slotNum = party.GetSlotNum();
            for (int i = 0; i < slotNum; i++) {
                if (i != memberIndex) {
                    long memberId = party.GetMemberAt(i);
                    if (memberId != -1L) {
                        UserCharacterData member = userData.GetUserCharaData(memberId);
                        if (IsSameCharacterEvolutionBeforeAfterIncluding(chara.Param.CharaID, member.Param.CharaID)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static int CheckPartyChara(int partyIndex, long charaMngID) {
            UserBattlePartyData party = GameSystem.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
            for (int i = 0; i < party.CharaMngIDs.Length; i++) {
                if (party.CharaMngIDs[i] == charaMngID) {
                    return i;
                }
            }
            return -1;
        }

        public static int CheckSupportPartyChara(int partyIndex, long charaMngID) {
            UserSupportPartyData party = GameSystem.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
            for (int i = 0; i < party.CharaMngIDs.Count; i++) {
                if (party.CharaMngIDs[i] == charaMngID) {
                    return i;
                }
            }
            return -1;
        }

        public static UserCharacterData GetSupportPartyUserCharacterData(int partyIndex, int partySlotIndex) {
            UserSupportPartyData party = GetSupportPartyUserSupportPartyData(partyIndex);
            if (party == null) {
                Debug.LogError("Invalid SupportPartyData");
                return null;
            }
            long memberAt = party.GetMemberAt(partySlotIndex);
            return GameSystem.Inst.UserDataMng.GetUserCharaData(memberAt);
        }

        public static UserWeaponData GetSupportPartyUserWeaponData(int partyIndex, int partySlotIndex) {
            UserSupportPartyData party = GetSupportPartyUserSupportPartyData(partyIndex);
            if (party == null) {
                Debug.LogError("Invalid SupportPartyData");
                return null;
            }
            long memberAt = party.GetMemberAt(partySlotIndex);
            return GameSystem.Inst.UserDataMng.GetUserWpnData(memberAt);
        }

        public static UserSupportPartyData GetSupportPartyUserSupportPartyData(int partyIndex) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            if (userData == null) {
                Debug.LogError("Invalid UserDataMng");
                return null;
            }
            if (userData.UserSupportPartyDatas == null) {
                Debug.LogError("Invalid UserDataMng");
                return null;
            }
            if (partyIndex < 0) {
                Debug.LogError("Invalid PartyIndex");
                return null;
            }
            if (userData.UserSupportPartyDatas.Count <= partyIndex) {
                Debug.LogError("Invalid PartyIndex");
                return null;
            }
            return userData.UserSupportPartyDatas[partyIndex];
        }

        public static bool IsEmptySupportParty(int partyIndex) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserSupportPartyData party = userData.UserSupportPartyDatas[partyIndex];
            for (int i = 0; i < userData.UserData.SupportLimit; i++) {
                if (party.GetMemberAt(i) != -1L) {
                    return false;
                }
            }
            return true;
        }

        public static bool CanBeAssignToSupportParty(int partyIndex, int memberIndex, long assignCharaMngID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserSupportPartyData party = userData.UserSupportPartyDatas[partyIndex];
            if (party == null) {
                return false;
            }
            UserCharacterData userCharaData = userData.GetUserCharaData(assignCharaMngID);
            return userCharaData != null;
        }

        public static bool CanUpgrade(long charaMngID) {
            UserCharacterData chara = GameSystem.Inst.UserDataMng.GetUserCharaData(charaMngID);
            return chara.Param.Lv < chara.Param.MaxLv;
        }

        public static List<ItemListDB_Param> GetUpgradeItemParams(eElementType elementType) {
            List<ItemListDB_Param> list = new List<ItemListDB_Param>();
            ItemListDB itemListDB = GameSystem.Inst.DbMng.ItemListDB;
            for (int i = 0; i < itemListDB.m_Params.Length; i++) {
                ItemListDB_Param item = itemListDB.m_Params[i];
                if (item.m_Type == (int)eItemType.Upgrade && item.m_TypeArgs[0] == (int)elementType) {
                    list.Add(item);
                }
            }
            return list;
        }

        public static void SimulateUpgradeCost(eElementType bonusElementType, int upgradeAmount, List<KeyValuePair<int, int>> materialList, out long out_totalAddExp, out int out_totalAmount) {
            out_totalAddExp = 0;
            out_totalAmount = 0;
            foreach (KeyValuePair<int, int> pair in materialList) {
                SimulateUpgradeCost(bonusElementType, upgradeAmount, pair.Key, pair.Value, out long totalAddExp, out int totalAmount);
                out_totalAddExp += totalAddExp;
                out_totalAmount += totalAmount;
            }
        }

        public static void SimulateUpgradeCost(eElementType bonusElementType, int upgradeAmount, int itemID, int itemNum, out long out_totalAddExp, out int out_totalAmount) {
            float bonus = GameSystem.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.UpgradeSameElementBonus);
            ItemListDB_Param itemParam = GameSystem.Inst.DbMng.ItemListDB.GetParam(itemID);
            int factor = 100;
            if (itemParam.m_TypeArgs[0] == (int)bonusElementType) {
                factor = Mathf.RoundToInt(factor * bonus);
            }
            out_totalAddExp = itemParam.m_TypeArgs[1] * factor / 100 * itemNum;
            out_totalAmount = upgradeAmount * itemNum;
        }

        public static bool IsMaxLimitBreak(long charaMngID) {
            return IsMaxLimitBreakLevel(GameSystem.Inst.UserDataMng.GetUserCharaData(charaMngID).Param.LimitBreak);
        }

        public static bool IsMaxLimitBreakLevel(int level) {
            return level >= (int)GameSystem.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.CharacterLimitBreakCountMax);
        }

        public static bool CanLimitBreak(long charaMngID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            UserCharacterData charaData = userData.GetUserCharaData(charaMngID);
            int recipe = dbMng.CharaListDB.GetParam(charaData.Param.CharaID).m_LimitBreakRecipeID;
            CharacterLimitBreakListDB_Param lbParam = dbMng.CharaLimitBreakListDB.GetParam(recipe);
            if (charaData.Param.LimitBreak >= lbParam.m_CharaMaxLvUps.Length) {
                return false;
            }
            if (userData.UserData.IsShortOfGold(lbParam.m_Amount)) {
                return false;
            }
            if (lbParam.m_ItemID != -1 && lbParam.m_ItemNum > 0) {
                if (userData.GetItemNum(lbParam.m_ItemID) >= lbParam.m_ItemNum) {
                    return true;
                }
            }
            eTitleType title = dbMng.NamedListDB.GetTitleType(charaData.Param.NamedType);
            TitleListDB_Param titleParam = dbMng.TitleListDB.GetParam(title);
            if (lbParam.m_TitleItemNum > 0 && titleParam.m_LimitBreakItemID != -1) {
                if (userData.GetItemNum(titleParam.m_LimitBreakItemID) >= lbParam.m_TitleItemNum) {
                    return true;
                }
            }
            if (lbParam.m_AllClassItemID != -1 && lbParam.m_AllClassItemNum > 0) {
                if (userData.GetItemNum(lbParam.m_AllClassItemID) >= lbParam.m_AllClassItemNum) {
                    return true;
                }
            }
            return false;
        }

        public static int CalcTotalLvUpValuByLimitBreak(int charaID) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            int recipe = dbMng.CharaListDB.GetParam(charaID).m_LimitBreakRecipeID;
            CharacterLimitBreakListDB_Param lbParam = dbMng.CharaLimitBreakListDB.GetParam(recipe);
            int lvs = 0;
            for (int i = 0; i < lbParam.m_CharaMaxLvUps.Length; i++) {
                lvs += lbParam.m_CharaMaxLvUps[i];
            }
            return lvs;
        }

        public static int GetUniqueSkillMaxLv(int charaID, int lb, eRare rarity, int arousalLevel) {
            int lv = GetSkillMaxLv(charaID, lb);
            ArousalLevelsDB_Param? param = GameSystem.Inst.DbMng.ArousalLevelsDB.GetParam(rarity, arousalLevel);
            if (param.HasValue) {
                lv += param.Value.m_SkillLvLimit;
            }
            return lv;
        }

        public static int GetSkillMaxLv(int charaID, int lb) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            CharacterLimitBreakListDB_Param lbParam = dbMng.CharaLimitBreakListDB.GetParam(charaParam.m_LimitBreakRecipeID);
            int num = 0;
            for (int i = 0; i < lb; i++) {
                num += lbParam.m_SkillMaxLvUps[i];
            }
            return charaParam.m_SkillLimitLv + num;
        }

        public static UserCharacterData GetHaveChara(int charaID) {
            List<UserCharacterData> charas = GameSystem.Inst.UserDataMng.UserCharaDatas;
            for (int i = 0; i < charas.Count; i++) {
                if (charas[i].Param.CharaID == charaID) {
                    return charas[i];
                }
            }
            return null;
        }

        public static bool IsHaveCharaOrEvolutionChara(int charaID) {
            if (GetHaveChara(charaID) != null) {
                return true;
            }
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            List<UserCharacterData> charas = GameSystem.Inst.UserDataMng.UserCharaDatas;
            bool exist = dbMng.CharaEvolutionListDB.GetParamBySrcCharaID(charaID, out CharacterEvolutionListDB_Param evoParam);
            if (exist) {
                for (int i = 0; i < charas.Count; i++) {
                    if (charas[i].Param.CharaID == evoParam.m_DestCharaID) {
                        return true;
                    }
                }
            }
            exist = dbMng.CharaEvolutionListDB.GetBeforeEvolutionCharacterIDs(charaID, out List<int> ids);
            if (exist) {
                for (int i = 0; i < charas.Count; i++) {
                    if (ids.Contains(charas[i].Param.CharaID)) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsExistEvolution(long charaMngID) {
            UserCharacterData chara = GameSystem.Inst.UserDataMng.GetUserCharaData(charaMngID);
            return chara != null && GameSystem.Inst.DbMng.CharaEvolutionListDB.IsExistParam(chara.Param.CharaID);
        }

        public static bool IsHaveEvolutionChara(long charaMngID) {
            UserCharacterData chara = GameSystem.Inst.UserDataMng.GetUserCharaData(charaMngID);
            if (chara == null) {
                return false;
            }
            if (!GameSystem.Inst.DbMng.CharaEvolutionListDB.GetParamBySrcCharaID(chara.Param.CharaID, out CharacterEvolutionListDB_Param evoParam)) {
                return false;
            }
            List<UserCharacterData> charas = GameSystem.Inst.UserDataMng.UserCharaDatas;
            for (int i = 0; i < charas.Count; i++) {
                if (charas[i].Param.CharaID == evoParam.m_DestCharaID) {
                    return true;
                }
            }
            return false;
        }

        public static bool CanEvolution(long charaMngID, bool isSpecify) {
            List<EditDefine.eEvolutionFailureReason> out_reasons = new List<EditDefine.eEvolutionFailureReason>();
            return CanEvolution(charaMngID, isSpecify, out_reasons);
        }

        public static bool CanEvolution(long charaMngID, bool isSpecify, List<EditDefine.eEvolutionFailureReason> out_reasons) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserCharacterData chara = userData.GetUserCharaData(charaMngID);
            if (chara == null) {
                return false;
            }
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            if (!dbMng.CharaEvolutionListDB.IsExistParam(chara.Param.CharaID)) {
                out_reasons.Add(EditDefine.eEvolutionFailureReason.RecipeNotFound);
            }
            int recipe = dbMng.CharaListDB.GetParam(chara.Param.CharaID).m_LimitBreakRecipeID;
            int lbs = dbMng.CharaLimitBreakListDB.GetParam(recipe).m_CharaMaxLvUps.Length;
            if (chara.Param.LimitBreak < lbs) {
                out_reasons.Add(EditDefine.eEvolutionFailureReason.LimitBreakFail);
            }
            if (chara.Param.Lv < chara.Param.MaxLv) {
                out_reasons.Add(EditDefine.eEvolutionFailureReason.CharaLvFail);
            }
            CharacterEvolutionListDB_Param evoParam;
            bool existEvo;
            if (isSpecify) {
                existEvo = dbMng.CharaEvolutionListDB.GetSpecifyMaterialParamBySrcCharaID(chara.Param.CharaID, out evoParam);
            } else {
                existEvo = dbMng.CharaEvolutionListDB.GetNormalMaterialParamBySrcCharaID(chara.Param.CharaID, out evoParam);
            }
            if (existEvo) {
                for (int i = 0; i < evoParam.m_ItemIDs.Length; i++) {
                    if (evoParam.m_ItemIDs[i] != -1 && userData.GetItemNum(evoParam.m_ItemIDs[i]) < evoParam.m_ItemNums[i]) {
                        out_reasons.Add(EditDefine.eEvolutionFailureReason.MaterialFail);
                        break;
                    }
                }
                if (userData.UserData.IsShortOfGold(evoParam.m_Amount)) {
                    out_reasons.Add(EditDefine.eEvolutionFailureReason.AmountFail);
                }
            }
            return existEvo && out_reasons.Count <= 0;
        }

        public static string GetEvolutionFailString(List<EditDefine.eEvolutionFailureReason> reasons) {
            StringBuilder stringBuilder = new StringBuilder();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            for (int i = 0; i < reasons.Count; i++) {
                if (i > 0) {
                    stringBuilder.Append("\n");
                }
                switch (reasons[i]) {
                    case EditDefine.eEvolutionFailureReason.RecipeNotFound:
                        stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedRecipeNotFound));
                        break;
                    case EditDefine.eEvolutionFailureReason.LimitBreakFail:
                        stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedLimitBreak));
                        break;
                    case EditDefine.eEvolutionFailureReason.CharaLvFail:
                        stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedCharaLv));
                        break;
                    case EditDefine.eEvolutionFailureReason.MaterialFail:
                        stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedMaterial));
                        break;
                    case EditDefine.eEvolutionFailureReason.AmountFail:
                        stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedAmount));
                        break;
                }
            }
            return stringBuilder.ToString();
        }

        public static List<UserWeaponData>[] SplitUserWeaponDataForClass(List<UserWeaponData> userWeaponData) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            List<UserWeaponData>[] weapons = new List<UserWeaponData>[5];
            for (int i = 0; i < weapons.Length; i++) {
                weapons[i] = new List<UserWeaponData>();
            }
            for (int j = 0; j < userWeaponData.Count; j++) {
                int classType = dbMng.WeaponListDB.GetParam(userWeaponData[j].Param.WeaponID).m_ClassType;
                weapons[classType].Add(userWeaponData[j]);
            }
            return weapons;
        }

        public static List<UserWeaponData> GetUserWeaponDataRefinePersonal(List<UserWeaponData> userWeaponDatas, eClassType classType, int characterID) {
            List<UserWeaponData> classWeapons = SplitUserWeaponDataForClass(userWeaponDatas)[(int)classType];
            classWeapons.FindAll(data => {
                WeaponListDB_Param weapon = GameSystem.Inst.DbMng.WeaponListDB.GetParam(data.Param.WeaponID);
                return !weapon.IsCharacterWeapon() || weapon.IsCharacterWeapon(characterID);
            });
            return null;
        }

        public static List<ItemListDB_Param> GetWeaponUpgradeItemParams() {
            List<ItemListDB_Param> items = new List<ItemListDB_Param>();
            ItemListDB itemListDB = GameSystem.Inst.DbMng.ItemListDB;
            for (int i = 0; i < itemListDB.m_Params.Length; i++) {
                ItemListDB_Param item = itemListDB.m_Params[i];
                if (item.m_Type == (int)eItemType.Weapon) {
                    items.Add(item);
                }
            }
            return items;
        }

        public static int CalcWeaponNum(int weaponID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            int copies = 0;
            for (int i = 0; i < userData.UserWeaponDatas.Count; i++) {
                if (userData.UserWeaponDatas[i].Param.WeaponID == weaponID) {
                    copies++;
                }
            }
            return copies;
        }

        public static bool CanEquipWeapon(long weaponMngID, long charaMngID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            UserWeaponData weapon = userData.GetUserWpnData(weaponMngID);
            if (weapon == null) {
                return false;
            }
            UserCharacterData chara = userData.GetUserCharaData(charaMngID);
            if (chara == null) {
                return false;
            }
            WeaponListDB_Param wpnParam = GameSystem.Inst.DbMng.WeaponListDB.GetParam(weapon.Param.WeaponID);
            return wpnParam.m_ClassType == (int)chara.Param.ClassType && (wpnParam.m_EquipableCharaID == -1 || wpnParam.m_EquipableCharaID == chara.Param.CharaID);
        }

        public static bool CheckDefaultWeapon(int weaponID) {
            ClassListDB classListDB = GameSystem.Inst.DbMng.ClassListDB;
            for (int i = 0; i < classListDB.m_Params.Length; i++) {
                if (classListDB.m_Params[i].m_DefaultWeaponID == weaponID) {
                    return true;
                }
            }
            CharacterOverrideDB overrideDB = GameSystem.Inst.DbMng.CharaOverrideDB;
            for (int i = 0; i < overrideDB.m_Params.Length; i++) {
                if (overrideDB.m_Params[i].m_DefaultWeaponID == weaponID) {
                    return true;
                }
            }
            return false;
        }

        public static int CalcDefaultWeaponIDFromCharaID(int charaID) {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            CharacterListDB_Param charaParam = dbMng.CharaListDB.GetParam(charaID);
            CharacterOverrideDB_Param? overrideParam = dbMng.CharaOverrideDB.GetParam(charaID);
            if (charaParam.IsDedicatedAnimType_ForChara()) {
                if (overrideParam.HasValue && overrideParam.Value.m_DefaultWeaponID != -1) {
                    return overrideParam.Value.m_DefaultWeaponID;
                }
            }
            return dbMng.ClassListDB.GetParam((eClassType)charaParam.m_Class).m_DefaultWeaponID;
        }

        public static bool SimulateWeaponUpgrade(long weaponMngID, List<KeyValuePair<int, int>> selectItem, out int out_amount, out long out_exp, out OutputCharaParam out_beforeParam, out OutputCharaParam out_afterParam, out long out_rest_exp) {
            out_exp = 0L;
            out_amount = 0;
            out_beforeParam = default;
            out_afterParam = default;
            out_rest_exp = 0;
            UserWeaponData wpn = GameSystem.Inst.UserDataMng.GetUserWpnData(weaponMngID);
            if (wpn == null) {
                return false;
            }
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            WeaponExpDB weaponExpDB = dbMng.WeaponExpDB;
            WeaponListDB_Param wpnParam = dbMng.WeaponListDB.GetParam(wpn.Param.WeaponID);
            if (selectItem != null) {
                ItemListDB itemListDB = dbMng.ItemListDB;
                float bonusMul = dbMng.StarDefineDB.GetValue(eStarDefineDB.UpgradeWeaponMaterialBonus);
                for (int i = 0; i < selectItem.Count; i++) {
                    int id = selectItem[i].Key;
                    if (id != -1) {
                        int amount = selectItem[i].Value;
                        ItemListDB_Param param2 = itemListDB.GetParam(id);
                        bool bonus = dbMng.WeaponListDB.IsBonusItem(wpnParam.m_ID, id);
                        float mul = bonus ? bonusMul : 1f;
                        out_exp += (long)(param2.m_TypeArgs[0] * amount * mul);
                        int upgradeAmount = weaponExpDB.GetUpgradeAmount(wpn.Param.Lv, wpnParam.m_ExpTableID);
                        out_amount += upgradeAmount * amount;
                    }
                }
            }
            out_beforeParam.Lv = wpn.Param.Lv;
            out_beforeParam.NowExp = GetWeaponNowExp(wpn.Param.WeaponID, out_beforeParam.Lv, wpn.Param.Exp);
            out_beforeParam.NextMaxExp = weaponExpDB.GetNextExp(out_beforeParam.Lv, wpnParam.m_ExpTableID);
            out_beforeParam.Hp = 0;
            out_beforeParam.Atk = wpn.Param.Atk;
            out_beforeParam.Mgc = wpn.Param.Mgc;
            out_beforeParam.Def = wpn.Param.Def;
            out_beforeParam.MDef = wpn.Param.MDef;
            out_beforeParam.Spd = 0;
            out_beforeParam.Luck = 0;
            SimulateWeaponLv(wpn.Param.WeaponID, wpn.Param.MaxLv, wpn.Param.Exp, out_exp, out int lv, out long afterExp, out long getExp);
            long rest = out_exp - getExp;
            out_rest_exp = rest > 0 ? rest : 0;
            out_exp = getExp;
            out_afterParam.Lv = lv;
            out_afterParam.NowExp = GetWeaponNowExp(wpn.Param.WeaponID, lv, afterExp);
            out_afterParam.NextMaxExp = weaponExpDB.GetNextExp(lv, wpnParam.m_ExpTableID);
            out_afterParam.Hp = 0;
            out_afterParam.Atk = CharacterParamCalculator.CalcParamCorrectLv(wpnParam.m_InitAtk, wpnParam.m_MaxAtk, wpnParam.m_InitLv, wpnParam.m_LimitLv, lv);
            out_afterParam.Mgc = CharacterParamCalculator.CalcParamCorrectLv(wpnParam.m_InitMgc, wpnParam.m_MaxMgc, wpnParam.m_InitLv, wpnParam.m_LimitLv, lv);
            out_afterParam.Def = CharacterParamCalculator.CalcParamCorrectLv(wpnParam.m_InitDef, wpnParam.m_MaxDef, wpnParam.m_InitLv, wpnParam.m_LimitLv, lv);
            out_afterParam.MDef = CharacterParamCalculator.CalcParamCorrectLv(wpnParam.m_InitMDef, wpnParam.m_MaxMDef, wpnParam.m_InitLv, wpnParam.m_LimitLv, lv);
            out_afterParam.Spd = 0;
            out_afterParam.Luck = 0;
            return true;
        }

        public static void SimulateWeaponLv(int weaponID, int maxLv, long currentExp, long addExp, out int out_afterLv, out long out_afterExp, out long out_getExp) {
            sbyte table = GameSystem.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_ExpTableID;
            WeaponExpDB weaponExpDB = GameSystem.Inst.DbMng.WeaponExpDB;
            out_afterLv = 1;
            out_afterExp = 0L;
            long simExp = addExp + currentExp;
            while (simExp > 0L && out_afterLv < maxLv) {
                int nextExp = weaponExpDB.GetNextExp(out_afterLv, table);
                if (nextExp <= 0) {
                    break;
                }
                if (nextExp > simExp) {
                    out_afterExp += simExp;
                    break;
                }
                out_afterLv++;
                out_afterExp += nextExp;
                simExp -= nextExp;
            }
            out_getExp = out_afterExp - currentExp;
        }

        public static long GetWeaponNowExp(int weaponID, int currentLv, long exp) {
            sbyte table = GameSystem.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_ExpTableID;
            WeaponExpDB weaponExpDB = GameSystem.Inst.DbMng.WeaponExpDB;
            int preExp = 0;
            for (int i = 1; i < currentLv; i++) {
                preExp += weaponExpDB.GetNextExp(i, table);
            }
            if (exp >= preExp) {
                return exp - preExp;
            }
            Debug.Log($"GetWeaponNowExp() is error.  exp: {exp}, pre: {preExp}");
            return 0L;
        }

        public static int GetReceiveCharacterWeaponID(long charaMngID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            UserCharacterData chara = userData.GetUserCharaData(charaMngID);
            if (chara == null) {
                return -1;
            }
            CharacterWeaponListDB_Param? charaWpnParam = dbMng.CharacterWeaponListDB.GetParamByCharaID(chara.Param.CharaID);
            if (!charaWpnParam.HasValue || chara.Param.Lv < charaWpnParam.Value.m_CondLv) {
                return -1;
            }
            List<int> ids = new List<int> { charaWpnParam.Value.m_WeaponID };
            WeaponListDB_Param wpnParam = dbMng.WeaponListDB.GetParam(charaWpnParam.Value.m_WeaponID);
            if (wpnParam.m_ID != charaWpnParam.Value.m_WeaponID || wpnParam.m_EvolvedCount != 0) {
                return -1;
            }
            WeaponEvolutionListDB_Param? wpnEvoParam;
            int wpnId = charaWpnParam.Value.m_WeaponID;
            do {
                wpnEvoParam = dbMng.WeaponEvolutionListDB.GetParamBySrcWeaponID(wpnId);
                if (!wpnEvoParam.HasValue) {
                    break;
                }
                wpnId = wpnEvoParam.Value.m_DestWeaponID;
                ids.Add(wpnId);
            } while (wpnId != -1);
            for (int i = 0; i < ids.Count; i++) {
                if (IsHaveWeapon(ids[i])) {
                    return -1;
                }
            }
            return charaWpnParam.Value.m_WeaponID;
        }

        public static bool GetCanReceiveCharacterWeapons(out List<long> out_charaMngIDs, out List<int> out_charaIDs, out List<int> out_weaponIDs) {
            out_charaMngIDs = null;
            out_charaIDs = null;
            out_weaponIDs = null;
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            for (int i = 0; i < userData.UserCharaDatas.Count; i++) {
                UserCharacterData chara = userData.UserCharaDatas[i];
                int wpnId = GetReceiveCharacterWeaponID(chara.MngID);
                if (wpnId != -1) {
                    if (out_charaMngIDs == null) {
                        out_charaMngIDs = new List<long>();
                        out_charaIDs = new List<int>();
                        out_weaponIDs = new List<int>();
                    }
                    out_charaMngIDs.Add(chara.MngID);
                    out_charaIDs.Add(chara.Param.CharaID);
                    out_weaponIDs.Add(wpnId);
                }
            }
            return out_charaMngIDs != null && out_charaMngIDs.Count > 0;
        }

        public static bool IsHaveCharacterWeaponAny() {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            WeaponListDB weaponListDB = GameSystem.Inst.DbMng.WeaponListDB;
            for (int i = 0; i < userData.UserWeaponDatas.Count; i++) {
                if (weaponListDB.GetParam(userData.UserWeaponDatas[i].Param.WeaponID).IsCharacterWeapon()) {
                    return true;
                }
            }
            return false;
        }

        public static bool IsHaveWeapon(int weaponID) {
            UserDataManager userData = GameSystem.Inst.UserDataMng;
            for (int i = 0; i < userData.UserWeaponDatas.Count; i++) {
                if (userData.UserWeaponDatas[i].Param.WeaponID == weaponID) {
                    return true;
                }
            }
            return false;
        }

        public static bool IsWeaponEvolvable(WeaponParam weaponParam) {
            return GameSystem.Inst.DbMng.WeaponEvolutionListDB.IsExistParam(weaponParam.WeaponID);
        }

        public static bool IsWeaponEvolutionEnoughGold(WeaponParam weaponParam) {
            WeaponEvolutionListDB_Param? param = GameSystem.Inst.DbMng.WeaponEvolutionListDB.GetParamBySrcWeaponID(weaponParam.WeaponID);
            if (param.HasValue) {
                return GameSystem.Inst.UserDataMng.UserData.IsShortOfGold(param.Value.m_AmountGold);
            }
            return false;
        }

        public static bool IsWeaponEvolutionEnoughMaterial(WeaponParam weaponParam) {
            WeaponEvolutionListDB_Param? param = GameSystem.Inst.DbMng.WeaponEvolutionListDB.GetParamBySrcWeaponID(weaponParam.WeaponID);
            if (param.HasValue) {
                UserDataManager userData = GameSystem.Inst.UserDataMng;
                for (int i = 0; i < param.Value.m_ItemIDs.Length; i++) {
                    if (userData.GetItemNum(param.Value.m_ItemIDs[i]) < param.Value.m_ItemNums[i]) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static bool IsMaxLevelWeapon(long weaponMngID) {
            UserWeaponData userWpnData = GameSystem.Inst.UserDataMng.GetUserWpnData(weaponMngID);
            return userWpnData != null && userWpnData.Param.Lv >= userWpnData.Param.MaxLv;
        }

        public static bool IsMaxLevelWeapon(WeaponParam weaponParam) {
            return IsMaxLevelWeapon(weaponParam.MngID);
        }

        public static bool IsMaxSkillLevelWeapon(WeaponParam weaponParam) {
            return weaponParam.SkillLearnData.SkillLv >= weaponParam.SkillLearnData.SkillMaxLv;
        }

        public static List<int> GetEvolutionWeaponIDs(int weaponID) {
            WeaponListDB weaponListDB = GameSystem.Inst.DbMng.WeaponListDB;
            WeaponEvolutionListDB evolutionListDB = GameSystem.Inst.DbMng.WeaponEvolutionListDB;
            WeaponEvolutionListDB_Param? evoParam = evolutionListDB.GetParamByDestWeaponID(weaponID);
            while (evoParam.HasValue) {
                weaponID = evoParam.Value.m_SrcWeaponID;
                evolutionListDB.GetParamByDestWeaponID(weaponID);
            }
            evoParam = evolutionListDB.GetParamBySrcWeaponID(weaponID);
            WeaponListDB_Param? wpnParam;
            if (evoParam.HasValue) {
                wpnParam = weaponListDB.GetParamNullable(weaponID);
                if (wpnParam.HasValue && wpnParam.Value.m_EvolvedCount == 0) {
                    weaponID = wpnParam.Value.m_ID;
                }
            } else {
                weaponID = -1;
            }
            List<int> weapons = new List<int>();
            wpnParam = weaponListDB.GetParamNullable(weaponID);
            while (wpnParam.HasValue) {
                weapons.Add(weaponID);
                evoParam = evolutionListDB.GetParamBySrcWeaponID(weaponID);
                if (!evoParam.HasValue) {
                    break;
                }
                weaponID = evoParam.Value.m_DestWeaponID;
                wpnParam = weaponListDB.GetParamNullable(weaponID);
            }
            return weapons;
        }

        public static int CalcRestPassiveSkillLearn(int beginWeaponID) {
            WeaponListDB weaponListDB = GameSystem.Inst.DbMng.WeaponListDB;
            WeaponListDB_Param wpnParam = weaponListDB.GetParam(beginWeaponID);
            if (wpnParam.m_PassiveSkillID != -1) {
                return 0;
            }
            WeaponEvolutionListDB evolutionListDB = GameSystem.Inst.DbMng.WeaponEvolutionListDB;
            WeaponEvolutionListDB_Param? evoParam = evolutionListDB.GetParamBySrcWeaponID(beginWeaponID);
            int count = 1;
            while (evoParam.HasValue && evoParam.Value.m_DestWeaponID != -1) {
                WeaponListDB_Param? wpnParamN = weaponListDB.GetParamNullable(evoParam.Value.m_DestWeaponID);
                if (!wpnParamN.HasValue) {
                    break;
                }
                if (wpnParamN.Value.m_PassiveSkillID != -1) {
                    return count;
                }
                count++;
                evoParam = evolutionListDB.GetParamBySrcWeaponID(wpnParamN.Value.m_ID);
            }
            return -1;
        }

        public static SkillLearnData SimulateWeaponSkillLv(SkillLearnData baseSkillData, int addExp) {
            SkillLearnData tmp = new SkillLearnData(baseSkillData.SkillID, baseSkillData.SkillLv, baseSkillData.SkillMaxLv, baseSkillData.TotalUseNum, baseSkillData.ExpTableID, baseSkillData.IsConfusionSkillID);
            tmp.TotalUseNum += addExp;
            int lv = GameSystem.Inst.DbMng.SkillExpDB.GetSimulateLv(tmp.ExpTableID, tmp.TotalUseNum);
            tmp.SkillLv = lv;
            return tmp;
        }
    }
}
