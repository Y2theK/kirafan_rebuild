﻿using System;

namespace Star {
    [Serializable]
    public abstract class EffectControllParam {
        public string[] m_TargetGoPaths;

        public abstract void Apply(int index, EffectHandler effHndl);
    }
}
