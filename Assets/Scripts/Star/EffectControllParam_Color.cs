﻿using System;
using UnityEngine;

namespace Star {
    [Serializable]
    public class EffectControllParam_Color : EffectControllParam {
        public Arg[] m_Args;

        public override void Apply(int index, EffectHandler effHndl) {
            if (index < 0 || index >= m_Args.Length) {
                return;
            }
            Arg arg = m_Args[index];
            if (effHndl.m_MsbHndl != null) {
                for (int i = 0; i < arg.m_Indices.Length; i++) {
                    int num = arg.m_Indices[i];
                    MsbObjectHandler handler = effHndl.m_MsbHndl.GetMsbObjectHandlerByName(m_TargetGoPaths[num]);
                    MsbObjectHandler.MsbObjectParam work = handler.GetWork();
                    if (work != null) {
                        work.m_MeshColor = new Color(arg.m_Color.r, arg.m_Color.g, arg.m_Color.b, work.m_MeshColor.a);
                    }
                }
            }
        }

        [Serializable]
        public class Arg {
            public int[] m_Indices;
            public Color m_Color;
        }
    }
}
