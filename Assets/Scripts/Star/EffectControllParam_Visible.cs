﻿using System;
using UnityEngine;

namespace Star {
    [Serializable]
    public class EffectControllParam_Visible : EffectControllParam {
        public Arg[] m_Args;
        public GameObject[] m_TargetGos;

        public override void Apply(int index, EffectHandler effHndl) {
            if (index < 0 || index >= m_Args.Length) {
                return;
            }
            Arg arg = m_Args[index];
            for (int i = 0; i < m_TargetGos.Length; i++) {
                bool active = false;
                for (int j = 0; j < arg.m_Indices.Length; j++) {
                    if (arg.m_Indices[j] == i) {
                        active = true;
                        break;
                    }
                }
                m_TargetGos[i].SetActive(active);
            }
        }

        [Serializable]
        public class Arg {
            public int[] m_Indices;
        }
    }
}
