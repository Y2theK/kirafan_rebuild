﻿using UnityEngine;

namespace Star {
    public class EffectHandler : EffectHandlerBase {
        private const string PLAY_KEY = "Take 001";

        public Animation m_Anim;
        public MeigeAnimCtrl m_MeigeAnimCtrl;
        public MeigeAnimClipHolder m_MeigeAnimClipHolder;
        public MsbHandler m_MsbHndl;
        public EffectControllParam_Visible m_ControllParam_Visible;
        public EffectControllParam_Color m_ControllParam_Color;

        private Color m_StartColor;
        private Color m_EndColor;
        private float m_MeshColorTime;
        private float m_MeshColorTimer;
        private float m_CurrentAlpha;
        private bool m_IsUpdateMeshColor;
        private bool m_IsCancelOnFadeComplete;
        private int m_FadeType;
        private float m_dispRate = 1f;

        private void LateUpdate() {
            UpdateMeshColor();
        }

        public override void OnDestroyToCache() {
            EmitterKill();
            Stop();
        }

        public bool IsPlaying() {
            if (m_MeigeAnimCtrl.m_WrapMode != WrapMode.ClampForever) {
                if (m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play) {
                    return true;
                }
            } else if (m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f) {
                return true;
            }

            int particleEmitterNum = m_MsbHndl.GetParticleEmitterNum();
            for (int i = 0; i < particleEmitterNum; i++) {
                if (m_MsbHndl.GetParticleEmitter(i).isAlive) {
                    return true;
                }
            }
            return false;
        }

        public void Setup() {
            if (m_MeigeAnimCtrl != null && m_MeigeAnimClipHolder != null && m_MsbHndl != null) {
                m_MeigeAnimCtrl.Open();
                m_MeigeAnimCtrl.AddClip(m_MsbHndl, m_MeigeAnimClipHolder);
                m_MeigeAnimCtrl.Close();
            }
            Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
            if (renderers != null) {
                for (int i = 0; i < renderers.Length; i++) {
                    renderers[i].enabled = false;
                }
            }
        }

        public void Play(Vector3 position, Quaternion rotation, Vector3 localScale, Transform parent, bool isLocalPosition, WrapMode wrapMode, float animationPlaySpeed) {
            if (parent != null) {
                m_Transform.SetParent(parent, false);
            }
            if (isLocalPosition) {
                m_Transform.localPosition = position;
                m_Transform.localRotation = rotation;
            } else {
                m_Transform.SetPositionAndRotation(position, rotation);
            }
            m_Transform.localScale = localScale * m_dispRate;
            m_MsbHndl.SetDefaultVisibility();
            int emitterNum = m_MsbHndl.GetParticleEmitterNum();
            for (int i = 0; i < emitterNum; i++) {
                m_MsbHndl.GetParticleEmitter(i).Kill();
            }
            ResetMeshColor();
            m_MeigeAnimCtrl.Play(PLAY_KEY, 0f, wrapMode);
            m_MeigeAnimCtrl.SetAnimationPlaySpeed(animationPlaySpeed);
        }

        public void SetScale(float scale) {
            SetScale(new Vector3(scale, scale, scale));
        }

        public void SetScale(Vector3 scale) {
            m_Transform.localScale = scale;
        }

        public void SetDispScale(float scale) {
            m_dispRate = scale;
        }

        public void SetSortingOrder(int sortingOrder) {
            if (m_MsbHndl != null) {
                Renderer[] cache = m_MsbHndl.GetRendererCache();
                for (int i = 0; i < cache.Length; i++) {
                    cache[i].sortingOrder = sortingOrder;
                }
                int emitterNum = m_MsbHndl.GetParticleEmitterNum();
                for (int j = 0; j < emitterNum; j++) {
                    m_MsbHndl.GetParticleEmitter(j).SetSortingOrder(sortingOrder);
                }
            }
        }

        public void SetLayer(int layerNo, bool needSetChildrens = true) {
            m_Transform.gameObject.SetLayer(layerNo, needSetChildrens);
            int emitterNum = m_MsbHndl.GetParticleEmitterNum();
            for (int i = 0; i < emitterNum; i++) {
                m_MsbHndl.GetParticleEmitter(i).SetLayer(layerNo);
            }
        }

        public void Stop() {
            m_MeigeAnimCtrl.Pause();
            int emitterNum = m_MsbHndl.GetParticleEmitterNum();
            for (int i = 0; i < emitterNum; i++) {
                m_MsbHndl.GetParticleEmitter(i).Activate(false);
            }
        }

        public float GetPlayingSec() {
            MeigeAnimCtrl.ReferenceAnimHandler refHandler = m_MeigeAnimCtrl.GetReferenceAnimHandler(PLAY_KEY);
            if (refHandler != null) {
                return refHandler.m_AnimationTime * m_MeigeAnimCtrl.m_NormalizedPlayTime;
            }
            return 0f;
        }

        public float GetMaxSec() {
            MeigeAnimCtrl.ReferenceAnimHandler refHandler = m_MeigeAnimCtrl.GetReferenceAnimHandler(PLAY_KEY);
            if (refHandler != null) {
                return refHandler.m_AnimationTime;
            }
            return 0f;
        }

        public void DisableOtherThanParticles(float fadeSec) {
            m_MeigeAnimCtrl.Pause();
            int particleEmitterNum = m_MsbHndl.GetParticleEmitterNum();
            for (int i = 0; i < particleEmitterNum; i++) {
                m_MsbHndl.GetParticleEmitter(i).Activate(false);
            }
            FadeIn(fadeSec, false);
        }

        public void EmitterKill() {
            if (m_MsbHndl != null) {
                int particleEmitterNum = m_MsbHndl.GetParticleEmitterNum();
                for (int i = 0; i < particleEmitterNum; i++) {
                    m_MsbHndl.GetParticleEmitter(i).Kill();
                }
            }
        }

        public void ApplyControllParam_Visible(int index) {
            if (m_ControllParam_Visible != null) {
                m_ControllParam_Visible.Apply(index, this);
            }
        }

        public void ApplyControllParam_Color(int index) {
            if (m_ControllParam_Color != null) {
                m_ControllParam_Color.Apply(index, this);
            }
        }

        public void ChangeMeshColor(Color startColor, Color endColor, float sec) {
            m_FadeType = -1;
            m_StartColor = startColor;
            m_EndColor = endColor;
            m_MeshColorTime = sec;
            m_MeshColorTimer = 0f;
            SetMeshColor(sec > 0f ? startColor : endColor);
            m_IsUpdateMeshColor = m_MeshColorTime > 0f;
        }

        public void FadeIn(float sec, bool isCancelOnFadeComplete) {
            m_FadeType = 0;
            m_MeshColorTime = sec;
            m_MeshColorTimer = 0f;
            m_IsUpdateMeshColor = true;
            m_IsCancelOnFadeComplete = isCancelOnFadeComplete;
        }

        public void FadeOut(float sec, bool isCancelOnFadeComplete) {
            m_FadeType = 1;
            m_MeshColorTime = sec;
            m_MeshColorTimer = 0f;
            m_IsUpdateMeshColor = true;
            m_IsCancelOnFadeComplete = isCancelOnFadeComplete;
        }

        private void UpdateMeshColor() {
            if (!m_IsUpdateMeshColor) {
                return;
            }

            m_MeshColorTimer += Time.deltaTime;
            float t = m_MeshColorTimer / m_MeshColorTime;
            if (t > 1f) {
                t = 1f;
            }
            if (m_FadeType == -1) {
                Color meshColor = Color.Lerp(m_StartColor, m_EndColor, t);
                SetMeshColor(meshColor);
                if (t >= 1f) {
                    m_IsUpdateMeshColor = false;
                }
            } else {
                float alpha = (m_FadeType == 0) ? 1 - t : t;
                SetMeshColorA(alpha);
                if (t >= 1f && m_IsCancelOnFadeComplete) {
                    m_IsUpdateMeshColor = false;
                }
            }
            if (t >= 1f && m_IsCancelOnFadeComplete) {
                m_IsUpdateMeshColor = false;
            }
        }

        public void ResetMeshColor() {
            m_FadeType = -1;
            m_IsUpdateMeshColor = false;
            m_IsCancelOnFadeComplete = false;
            SetMeshColor(Color.white);
        }

        private void SetMeshColor(Color color) {
            int handlerNum = m_MsbHndl.GetMsbObjectHandlerNum();
            for (int i = 0; i < handlerNum; i++) {
                MsbObjectHandler.MsbObjectParam work = m_MsbHndl.GetMsbObjectHandler(i).GetWork();
                if (work != null) {
                    work.m_MeshColor = color;
                }
            }
            if (m_MsbHndl.m_MsbParticleEmitterArray != null) {
                for (int i = 0; i < m_MsbHndl.m_MsbParticleEmitterArray.Length; i++) {
                    MsbMaterialHandler.MsbMaterialParam work = m_MsbHndl.m_MsbParticleEmitterArray[i].m_MsbMaterialHandler.GetWork();
                    if (work != null) {
                        work.m_Diffuse.r = color.r;
                        work.m_Diffuse.g = color.g;
                        work.m_Diffuse.b = color.b;
                    }
                }
            }
        }

        private void SetMeshColorA(float a) {
            m_CurrentAlpha = a;
            int handlerNum = m_MsbHndl.GetMsbObjectHandlerNum();
            for (int i = 0; i < handlerNum; i++) {
                MsbObjectHandler.MsbObjectParam work = m_MsbHndl.GetMsbObjectHandler(i).GetWork();
                if (work != null) {
                    work.m_MeshColor.a = m_CurrentAlpha;
                }
            }
        }
    }
}
