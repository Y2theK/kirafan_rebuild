﻿using UnityEngine;

namespace Star {
    public abstract class EffectHandlerBase : MonoBehaviour {
        public string m_EffectID;
        public Transform m_Transform;

        public string EffectID => m_EffectID;
        public Transform CacheTransform => m_Transform;

        public abstract void OnDestroyToCache();
    }
}
