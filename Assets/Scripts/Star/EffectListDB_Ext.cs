﻿namespace Star {
    public static class EffectListDB_Ext {
        public static bool IsExistSoundSetting(this EffectListDB self, int index) {
            return self.m_Params[index].m_PlayFrames != null
                && self.m_Params[index].m_PlayFrames.Length > 0
                && self.m_Params[index].m_CueIDs != null
                && self.m_Params[index].m_CueIDs.Length > 0;
        }

        public static void SetupSoundFrameController(this EffectListDB self, int index, SoundFrameController sfc) {
            EffectListDB_Param param = self.m_Params[index];
            for (int i = 0; i < param.m_PlayFrames.Length; i++) {
                sfc.AddRequestParam(param.m_PlayFrames[i], (eSoundSeListDB)param.m_CueIDs[i], true);
            }
        }
    }
}
