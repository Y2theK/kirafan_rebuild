﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004E1 RID: 1249
	[Token(Token = "0x20003D7")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct EffectListDB_Param
	{
		// Token: 0x04001877 RID: 6263
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40011E6")]
		public string m_PackName;

		// Token: 0x04001878 RID: 6264
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40011E7")]
		public string m_EffectID;

		// Token: 0x04001879 RID: 6265
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40011E8")]
		public int m_InstanceMax;

		// Token: 0x0400187A RID: 6266
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40011E9")]
		public int[] m_PlayFrames;

		// Token: 0x0400187B RID: 6267
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40011EA")]
		public int[] m_CueIDs;
	}
}
