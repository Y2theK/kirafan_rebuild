﻿using Meige;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;

namespace Star {
    public sealed class EffectManager : SingletonMonoBehaviour<EffectManager> {
        private const int SIMULTANEOUSLY_LOAD_NUM = 100;

        [SerializeField] private Transform m_CacheRoot;

        private Dictionary<string, bool> m_LoadedPacks = new Dictionary<string, bool>();
        private Dictionary<string, CacheObjectData> m_Caches = new Dictionary<string, CacheObjectData>();
        private Dictionary<GameObject, bool> m_ActiveCachedInstances = new Dictionary<GameObject, bool>();
        private List<EffectHandler> m_ActiveEffectList = new List<EffectHandler>();
        private EffectListDB m_EffectListDB;
        private Dictionary<string, PackEffectData> m_PackDatas = new Dictionary<string, PackEffectData>();
        private Dictionary<string, int> m_DBIndices = new Dictionary<string, int>();
        private SoundManager m_SoundManager;
        private Dictionary<EffectHandler, SoundFrameController> m_SfcDict = new Dictionary<EffectHandler, SoundFrameController>();
        private List<EffectHandler> m_RemoveSfcKeys = new List<EffectHandler>();
        private ABResourceLoader m_Loader;
        private Dictionary<string, ABResourceObjectHandler> m_LoadingHndls_Single = new Dictionary<string, ABResourceObjectHandler>();
        private Dictionary<string, ABResourceObjectHandler> m_LoadingHndls_Pack = new Dictionary<string, ABResourceObjectHandler>();
        private List<string> m_RemoveKeys = new List<string>();
        private StringBuilder m_sb = new StringBuilder();

        protected override void Awake() {
            base.Awake();
            m_Loader = new ABResourceLoader(SIMULTANEOUSLY_LOAD_NUM);
        }

        private void Update() {
            UpdateLoadEffect();
            for (int i = m_ActiveEffectList.Count - 1; i >= 0; i--) {
                EffectHandler handler = m_ActiveEffectList[i];
                if (!handler.IsPlaying()) {
                    DestroyToCache(handler);
                    m_ActiveEffectList.RemoveAt(i);
                }
            }
        }

        private void LateUpdate() {
            UpdateSfc();
        }

        public bool IsAvailable() {
            return m_EffectListDB != null;
        }

        public void ForceResetOnReturnTitle() {
            foreach (EffectHandler effectHandler in m_SfcDict.Keys) {
                if (effectHandler != null) {
                    m_SfcDict[effectHandler].Destroy(true, true);
                }
            }
            m_SfcDict.Clear();
            UnloadEffectAll(null);
        }

        public void SetupFromDB(EffectListDB effectListDB) {
            m_EffectListDB = effectListDB;
            m_PackDatas.Clear();
            m_DBIndices.Clear();
            int paramNum = m_EffectListDB.m_Params.Length;
            for (int i = 0; i < paramNum; i++) {
                string packName = m_EffectListDB.m_Params[i].m_PackName;
                if (!string.IsNullOrEmpty(packName)) {
                    if (!m_PackDatas.ContainsKey(packName)) {
                        m_PackDatas.Add(packName, new PackEffectData());
                    }
                    m_PackDatas[packName].m_EffectIDs.Add(m_EffectListDB.m_Params[i].m_EffectID);
                }
                m_DBIndices.Add(m_EffectListDB.m_Params[i].m_EffectID, i);
            }
        }

        public bool GetEffectListDBParam(string effectID, out EffectListDB_Param out_param) {
            if (m_DBIndices.TryGetValue(effectID, out int index)) {
                out_param = m_EffectListDB.m_Params[index];
                return true;
            }
            out_param = default;
            return false;
        }

        public string CheckPackEffect(string effectID) {
            if (m_DBIndices.TryGetValue(effectID, out int idx)) {
                string packName = m_EffectListDB.m_Params[idx].m_PackName;
                return string.IsNullOrEmpty(packName) ? null : packName;
            }
            return null;
        }

        public void SetupSound(SoundManager soundMng) {
            m_SoundManager = soundMng;
        }

        private void PlaySfc(EffectHandler effect) {
            int index = m_DBIndices[effect.EffectID];
            if (m_EffectListDB.IsExistSoundSetting(index) && m_SoundManager != null) {
                SoundFrameController controller = new SoundFrameController(m_SoundManager, null, null, null);
                m_EffectListDB.SetupSoundFrameController(index, controller);
                controller.PrepareQuick();
                controller.Play();
                m_SfcDict.AddOrReplace(effect, controller);
            }
        }

        private void UpdateSfc() {
            m_RemoveSfcKeys.Clear();
            foreach (EffectHandler handler in m_SfcDict.Keys) {
                if (handler == null) { continue; }

                if (handler.IsPlaying()) {
                    int currentFrame = (int)(handler.GetPlayingSec() / GameDefine.APP_SEC_PER_ONEFRAME);
                    m_SfcDict[handler].Update(currentFrame);
                } else {
                    m_SfcDict[handler].Destroy();
                    m_RemoveSfcKeys.Add(handler);
                }
            }
            for (int i = 0; i < m_RemoveSfcKeys.Count; i++) {
                m_SfcDict.Remove(m_RemoveSfcKeys[i]);
            }
        }

        private void UpdateLoadEffect() {
            m_Loader.Update();
            foreach (string id in m_LoadingHndls_Single.Keys) {
                ABResourceObjectHandler handler = m_LoadingHndls_Single[id];
                if (handler != null) {
                    if (handler.IsDone()) {
                        if (handler.IsError()) {
                            UnityEngine.Debug.LogFormat("abHndl is error :{0}", id);
                        } else {
                            CreateCacheFromAB(handler, id);
                        }
                        m_Loader.Unload(handler);
                        m_RemoveKeys.Add(id);
                    }
                } else {
                    m_RemoveKeys.Add(id);
                }
            }
            if (m_RemoveKeys.Count > 0) {
                for (int i = 0; i < m_RemoveKeys.Count; i++) {
                    m_LoadingHndls_Single.Remove(m_RemoveKeys[i]);
                }
                m_RemoveKeys.Clear();
            }
            m_RemoveKeys.Clear();
            foreach (string id in m_LoadingHndls_Pack.Keys) {
                ABResourceObjectHandler handler = m_LoadingHndls_Pack[id];
                if (handler != null) {
                    if (handler.IsDone()) {
                        if (handler.IsError()) {
                            UnityEngine.Debug.LogFormat("abHndl is error :{0}", id);
                        } else {
                            PackEffectData packData = m_PackDatas[id];
                            for (int i = 0; i < packData.m_EffectIDs.Count; i++) {
                                CreateCacheFromAB(handler, packData.m_EffectIDs[i]);
                            }
                            m_LoadedPacks.Add(id, true);
                        }
                        m_Loader.Unload(handler);
                        m_RemoveKeys.Add(id);
                    }
                } else {
                    m_RemoveKeys.Add(id);
                }
            }
            if (m_RemoveKeys.Count > 0) {
                for (int i = 0; i < m_RemoveKeys.Count; i++) {
                    m_LoadingHndls_Pack.Remove(m_RemoveKeys[i]);
                }
                m_RemoveKeys.Clear();
            }
        }

        private void CreateCacheFromAB(ABResourceObjectHandler hndl, string key) {
            GameObject gameObject = hndl.FindObj(key) as GameObject;
            if (gameObject != null) {
                CreateCache(key, gameObject);
            } else {
                UnityEngine.Debug.LogFormat("cache object not found :{0}", key);
            }
        }

        public string GetSingleEffectResourcePath(string effectID) {
            m_sb.Length = 0;
            m_sb.Append("effect/");
            m_sb.Append(effectID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return m_sb.ToString();
        }

        private string GetPackEffectResourcePath(string packName) {
            m_sb.Length = 0;
            m_sb.Append("effect/");
            m_sb.Append(packName);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return m_sb.ToString();
        }

        public bool IsLoadingAny() {
            return m_LoadingHndls_Single.Count > 0 || m_LoadingHndls_Pack.Count > 0;
        }

        public bool LoadPack(string packName) {
            if (m_LoadingHndls_Pack.ContainsKey(packName) || m_LoadedPacks.ContainsKey(packName)) {
                return false;
            }
            if (!m_PackDatas.ContainsKey(packName)) {
                return false;
            }
            ABResourceObjectHandler handler = m_Loader.Load(GetPackEffectResourcePath(packName));
            if (handler != null) {
                m_LoadingHndls_Pack.Add(packName, handler);
                return true;
            }
            return false;
        }

        public bool UnloadPack(string packName) {
            if (!m_PackDatas.ContainsKey(packName)) {
                return false;
            }
            PackEffectData packData = m_PackDatas[packName];
            for (int i = 0; i < packData.m_EffectIDs.Count; i++) {
                string id = packData.m_EffectIDs[i];
                ActiveToCache(id);
                if (m_Caches.ContainsKey(id)) {
                    m_Caches[id].Destroy();
                    m_Caches.Remove(id);
                }
                m_Loader.Unload(GetPackEffectResourcePath(packName));
            }
            m_LoadedPacks.Remove(packName);
            return true;
        }

        public bool LoadSingleEffect(string effectID, bool isPackLoadIfBelongingToPack = false) {
            if (isPackLoadIfBelongingToPack) {
                string pack = CheckPackEffect(effectID);
                if (!string.IsNullOrEmpty(pack)) {
                    return LoadPack(pack);
                }
            }
            if (m_LoadingHndls_Single.ContainsKey(effectID) || m_Caches.ContainsKey(effectID)) {
                return false;
            }
            if (!m_DBIndices.ContainsKey(effectID)) {
                return false;
            }
            ABResourceObjectHandler handler = m_Loader.Load(GetSingleEffectResourcePath(effectID));
            if (handler != null) {
                m_LoadingHndls_Single.Add(effectID, handler);
                return true;
            }
            return false;
        }

        public void UnloadSingleEffect(string effectID) {
            string pack = CheckPackEffect(effectID);
            if (string.IsNullOrEmpty(pack)) {
                ActiveToCache(effectID);
                if (m_Caches.ContainsKey(effectID)) {
                    m_Caches[effectID].Destroy();
                    m_Caches.Remove(effectID);
                    m_Loader.Unload(GetSingleEffectResourcePath(effectID));
                }
            }
        }

        private void ActiveToCache(string effectID) {
            for (int i = m_ActiveEffectList.Count - 1; i >= 0; i--) {
                EffectHandler effectHandler = m_ActiveEffectList[i];
                if (effectHandler.EffectID == effectID) {
                    DestroyToCache(effectHandler);
                    m_ActiveEffectList.RemoveAt(i);
                }
            }
        }

        public void UnloadEffectAll(string[] ignorePackNames) {
            List<string> list = new List<string>();
            foreach (string id in m_LoadedPacks.Keys) {
                bool unload = true;
                if (ignorePackNames != null) {
                    for (int i = 0; i < ignorePackNames.Length; i++) {
                        if (id == ignorePackNames[i]) {
                            unload = false;
                            break;
                        }
                    }
                }
                if (unload) {
                    list.Add(id);
                }
            }
            for (int i = 0; i < list.Count; i++) {
                UnloadPack(list[i]);
            }
            list.Clear();
            foreach (string id in m_Caches.Keys) {
                bool unload = true;
                if (ignorePackNames != null) {
                    string pack = CheckPackEffect(id);
                    if (!string.IsNullOrEmpty(pack)) {
                        for (int i = 0; i < ignorePackNames.Length; i++) {
                            if (pack == ignorePackNames[i]) {
                                unload = false;
                                break;
                            }
                        }
                    }
                }
                if (unload) {
                    list.Add(id);
                }
            }
            for (int i = 0; i < list.Count; i++) {
                UnloadSingleEffect(list[i]);
            }
        }

        private bool CreateCache(string effectID, GameObject modelPrefab) {
            if (modelPrefab == null || m_Caches.ContainsKey(effectID)) {
                return false;
            }
            int idx = m_DBIndices[effectID];
            int instanceMax = m_EffectListDB.m_Params[idx].m_InstanceMax;
            GameObject obj = Instantiate(modelPrefab);
            CacheObjectData cacheData = new CacheObjectData();
            cacheData.OnInstantiateObject += OnInitializeObj;
            cacheData.Initialize(obj, instanceMax, m_CacheRoot);
            cacheData.OnInstantiateObject -= OnInitializeObj;
            m_Caches.Add(effectID, cacheData);
            for (int i = 0; i < cacheData.CacheObjects.Length; i++) {
                m_ActiveCachedInstances.Add(cacheData.CacheObjects[i], true);
            }
            Destroy(obj);
            return true;
        }

        private void OnInitializeObj(GameObject obj) {
            if (obj.TryGetComponent(out EffectHandler effectHandler)) {
                effectHandler.Setup();
            } else if (obj.TryGetComponent(out TrailHandler trailHandler)) { } //unfinished code
        }

        public void DestroyToCache(EffectHandlerBase objToDestroy) {
            if (objToDestroy == null) {
                return;
            }
            objToDestroy.OnDestroyToCache();
            GameObject obj = objToDestroy.gameObject;
            if (m_ActiveCachedInstances.ContainsKey(obj)) {
                obj.SetActive(false);
                obj.transform.SetParent(m_CacheRoot, false);
                m_ActiveCachedInstances[obj] = false;
            } else {
                Destroy(obj);
            }
        }

        public void Play(string effectID, Vector3 position, Quaternion rotation, Vector3 localScale, Transform parent, bool isLocalPosition, int sortingOrder = 0, float animationPlaySpeed = 1f, bool isSoundPlay = false) {
            if (!m_Caches.ContainsKey(effectID)) {
                return;
            }
            GameObject obj = m_Caches[effectID].GetNextObjectInCache();
            if (obj == null) {
                return;
            }
            obj.SetActive(true);
            m_ActiveCachedInstances[obj] = true;
            if (obj.TryGetComponent(out EffectHandler handler)) {
                handler.Play(position, rotation, localScale, parent, isLocalPosition, WrapMode.ClampForever, animationPlaySpeed);
                handler.SetSortingOrder(sortingOrder);
                m_ActiveEffectList.Add(handler);
                if (isSoundPlay) {
                    PlaySfc(handler);
                }
            }
        }

        public EffectHandler PlayUnManaged(string effectID, Vector3 position, Quaternion rotation, Transform parent, bool isLocalPosition, WrapMode wrapMode = WrapMode.ClampForever, int sortingOrder = 0, float animationPlaySpeed = 1f, bool isSoundPlay = false, float dispScale = 1f) {
            if (!m_Caches.ContainsKey(effectID)) {
                return null;
            }
            GameObject obj = m_Caches[effectID].GetNextObjectInCache();
            if (obj == null) {
                return null;
            }
            obj.SetActive(true);
            m_ActiveCachedInstances[obj] = true;
            if (obj.TryGetComponent(out EffectHandler handler)) {
                handler.Play(position, rotation, Vector3.one, parent, isLocalPosition, wrapMode, animationPlaySpeed);
                handler.SetSortingOrder(sortingOrder);
                handler.CacheTransform.localScale *= dispScale;
                if (isSoundPlay) {
                    PlaySfc(handler);
                }
            }
            return handler;
        }

        public TrailHandler BorrowTrail(string effectID, Vector3 position, Quaternion rotation, Transform parent, bool isLocalPosition, int sortingOrder = 0) {
            if (!m_Caches.ContainsKey(effectID)) {
                return null;
            }
            CacheObjectData cacheData = m_Caches[effectID];
            if (cacheData == null) {
                return null;
            }
            GameObject obj = cacheData.GetNextObjectInCache();
            if (obj == null) {
                return null;
            }
            obj.SetActive(true);
            m_ActiveCachedInstances[obj] = true;
            if (obj.TryGetComponent(out TrailHandler handler)) {
                if (parent != null) {
                    handler.CacheTransform.SetParent(parent, false);
                }
                if (isLocalPosition) {
                    handler.CacheTransform.localPosition = position;
                } else {
                    handler.CacheTransform.position = position;
                }
                handler.CacheTransform.localRotation = rotation;
                handler.SetSortingOrder(sortingOrder);
            }
            return handler;
        }

        [Conditional("EFFECT_MNG_DEBUG")]
        private void StartStopwatchForDebugPrepare(string key, bool isPack) { }

        [Conditional("EFFECT_MNG_DEBUG")]
        private void StopStopwatchForDebugPrepare(string key, string header_msg, bool isPack) { }

        public class PackEffectData {
            public List<string> m_EffectIDs = new List<string>();
        }
    }
}
