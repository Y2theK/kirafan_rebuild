﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E4 RID: 1252
	[Token(Token = "0x20003DA")]
	[StructLayout(3)]
	public class EnemyResourceListDB : ScriptableObject
	{
		// Token: 0x060014FC RID: 5372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013B1")]
		[Address(RVA = "0x1011DEFD4", Offset = "0x11DEFD4", VA = "0x1011DEFD4")]
		public EnemyResourceListDB()
		{
		}

		// Token: 0x04001887 RID: 6279
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40011F6")]
		public EnemyResourceListDB_Param[] m_Params;
	}
}
