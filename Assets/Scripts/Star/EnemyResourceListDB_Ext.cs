﻿namespace Star {
    public static class EnemyResourceListDB_Ext {
        public static EnemyResourceListDB_Param GetParam(this EnemyResourceListDB self, int resourceID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ResourceID == resourceID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static bool IsExistParam(this EnemyResourceListDB self, int resourceID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ResourceID == resourceID) {
                    return true;
                }
            }
            return false;
        }
    }
}
