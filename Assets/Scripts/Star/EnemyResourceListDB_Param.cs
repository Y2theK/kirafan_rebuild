﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004E3 RID: 1251
	[Token(Token = "0x20003D9")]
	[Serializable]
	[StructLayout(0)]
	public struct EnemyResourceListDB_Param
	{
		// Token: 0x0400187D RID: 6269
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40011EC")]
		public int m_ResourceID;

		// Token: 0x0400187E RID: 6270
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40011ED")]
		public int m_AnimID;

		// Token: 0x0400187F RID: 6271
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40011EE")]
		public byte m_IsBoss;

		// Token: 0x04001880 RID: 6272
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40011EF")]
		public string m_VoiceCueSheetName;

		// Token: 0x04001881 RID: 6273
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40011F0")]
		public float m_ShadowScale;

		// Token: 0x04001882 RID: 6274
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40011F1")]
		public float m_ShadowOffsetX;

		// Token: 0x04001883 RID: 6275
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40011F2")]
		public string[] m_AttachEffectIDs;

		// Token: 0x04001884 RID: 6276
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40011F3")]
		public string[] m_AttachEffectParents;

		// Token: 0x04001885 RID: 6277
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40011F4")]
		public float[] m_AttachEffectOffsetXs;

		// Token: 0x04001886 RID: 6278
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40011F5")]
		public float[] m_AttachEffectOffsetYs;
	}
}
