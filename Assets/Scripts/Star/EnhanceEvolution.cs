﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000655 RID: 1621
	[Token(Token = "0x200053D")]
	[StructLayout(3)]
	public class EnhanceEvolution
	{
		// Token: 0x060017C1 RID: 6081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001664")]
		[Address(RVA = "0x1011DF188", Offset = "0x11DF188", VA = "0x1011DF188")]
		public EnhanceEvolution(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x060017C2 RID: 6082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001665")]
		[Address(RVA = "0x1011DF1BC", Offset = "0x11DF1BC", VA = "0x1011DF1BC")]
		public void Destroy()
		{
		}

		// Token: 0x060017C3 RID: 6083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001666")]
		[Address(RVA = "0x1011DF204", Offset = "0x11DF204", VA = "0x1011DF204")]
		public void Play(UserCharacterData userCharacterData, int charaIDBefore, int charaIDAfter)
		{
		}

		// Token: 0x060017C4 RID: 6084 RVA: 0x0000AE30 File Offset: 0x00009030
		[Token(Token = "0x6001667")]
		[Address(RVA = "0x1011DF214", Offset = "0x11DF214", VA = "0x1011DF214")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x04002684 RID: 9860
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001FD9")]
		private EnhanceEvolution.eStep m_Step;

		// Token: 0x04002685 RID: 9861
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FDA")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04002686 RID: 9862
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001FDB")]
		private EvolutionEffectScene m_EffectScene;

		// Token: 0x04002687 RID: 9863
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FDC")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x04002688 RID: 9864
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FDD")]
		private int m_CharacterIDBefore;

		// Token: 0x04002689 RID: 9865
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001FDE")]
		private int m_CharacterIDAfter;

		// Token: 0x02000656 RID: 1622
		[Token(Token = "0x2000DEC")]
		private enum eStep
		{
			// Token: 0x0400268B RID: 9867
			[Token(Token = "0x4005A2D")]
			None = -1,
			// Token: 0x0400268C RID: 9868
			[Token(Token = "0x4005A2E")]
			EffectPrepare,
			// Token: 0x0400268D RID: 9869
			[Token(Token = "0x4005A2F")]
			EffectPrepare_Wait,
			// Token: 0x0400268E RID: 9870
			[Token(Token = "0x4005A30")]
			EffectPlay_Wait,
			// Token: 0x0400268F RID: 9871
			[Token(Token = "0x4005A31")]
			PreEnd,
			// Token: 0x04002690 RID: 9872
			[Token(Token = "0x4005A32")]
			End
		}
	}
}
