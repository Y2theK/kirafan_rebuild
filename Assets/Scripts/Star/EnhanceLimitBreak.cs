﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000657 RID: 1623
	[Token(Token = "0x200053E")]
	[StructLayout(3)]
	public class EnhanceLimitBreak
	{
		// Token: 0x060017C5 RID: 6085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001668")]
		[Address(RVA = "0x1011DF450", Offset = "0x11DF450", VA = "0x1011DF450")]
		public EnhanceLimitBreak(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x060017C6 RID: 6086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001669")]
		[Address(RVA = "0x1011DF484", Offset = "0x11DF484", VA = "0x1011DF484")]
		public void Destroy()
		{
		}

		// Token: 0x060017C7 RID: 6087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600166A")]
		[Address(RVA = "0x1011DF4CC", Offset = "0x11DF4CC", VA = "0x1011DF4CC")]
		public void Play(UserCharacterData userCharacterData, int beforeLevel, int afterLevel)
		{
		}

		// Token: 0x060017C8 RID: 6088 RVA: 0x0000AE48 File Offset: 0x00009048
		[Token(Token = "0x600166B")]
		[Address(RVA = "0x1011DF4DC", Offset = "0x11DF4DC", VA = "0x1011DF4DC")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x04002691 RID: 9873
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001FDF")]
		private EnhanceLimitBreak.eStep m_Step;

		// Token: 0x04002692 RID: 9874
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FE0")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04002693 RID: 9875
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001FE1")]
		private int m_BeforeLevel;

		// Token: 0x04002694 RID: 9876
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001FE2")]
		private int m_AfterLevel;

		// Token: 0x04002695 RID: 9877
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FE3")]
		private LimitBreakEffectScene m_EffectScene;

		// Token: 0x04002696 RID: 9878
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FE4")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x02000658 RID: 1624
		[Token(Token = "0x2000DED")]
		private enum eStep
		{
			// Token: 0x04002698 RID: 9880
			[Token(Token = "0x4005A34")]
			None = -1,
			// Token: 0x04002699 RID: 9881
			[Token(Token = "0x4005A35")]
			EffectPrepare,
			// Token: 0x0400269A RID: 9882
			[Token(Token = "0x4005A36")]
			EffectPrepare_Wait,
			// Token: 0x0400269B RID: 9883
			[Token(Token = "0x4005A37")]
			EffectPlay_Wait,
			// Token: 0x0400269C RID: 9884
			[Token(Token = "0x4005A38")]
			PreEnd,
			// Token: 0x0400269D RID: 9885
			[Token(Token = "0x4005A39")]
			End
		}
	}
}
