﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Edit;
using WWWTypes;

namespace Star
{
	// Token: 0x02000635 RID: 1589
	[Token(Token = "0x2000521")]
	[StructLayout(3)]
	public sealed class EnhanceManager
	{
		// Token: 0x060016E6 RID: 5862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600158D")]
		[Address(RVA = "0x1011DF700", Offset = "0x11DF700", VA = "0x1011DF700")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x060016E7 RID: 5863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600158E")]
		[Address(RVA = "0x1011DF774", Offset = "0x11DF774", VA = "0x1011DF774")]
		public void Request_Upgrade(long charaMngID, List<KeyValuePair<int, int>> list, Action<ResultCode, int> callback)
		{
		}

		// Token: 0x060016E8 RID: 5864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600158F")]
		[Address(RVA = "0x1011DFAD8", Offset = "0x11DFAD8", VA = "0x1011DFAD8")]
		private static void OnResponse_Upgrade(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060016E9 RID: 5865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001590")]
		[Address(RVA = "0x1011DFDE4", Offset = "0x11DFDE4", VA = "0x1011DFDE4")]
		public void Request_LimitBreak(long charaMngID, int[] itemList, Action<ResultCode, int, int> callback)
		{
		}

		// Token: 0x060016EA RID: 5866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001591")]
		[Address(RVA = "0x1011DFF30", Offset = "0x11DFF30", VA = "0x1011DFF30")]
		private void OnResponse_LimitBreak(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001592")]
		[Address(RVA = "0x1011E0200", Offset = "0x11E0200", VA = "0x1011E0200")]
		public void Request_Evolution(long charaMngID, int itemIndex, Action<ResultCode, CharaDetailUI.LevelInfo, CharaDetailUI.LevelInfo> callback)
		{
		}

		// Token: 0x060016EC RID: 5868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001593")]
		[Address(RVA = "0x1011E04B8", Offset = "0x11E04B8", VA = "0x1011E04B8")]
		private void OnResponse_Evolution(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060016ED RID: 5869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001594")]
		[Address(RVA = "0x1011E0908", Offset = "0x11E0908", VA = "0x1011E0908")]
		public EnhanceManager()
		{
		}

		// Token: 0x04002640 RID: 9792
		[Token(Token = "0x4001F9B")]
		private static Action<ResultCode, int> m_OnResponse_Upgrade;

		// Token: 0x04002641 RID: 9793
		[Token(Token = "0x4001F9C")]
		private static Action<ResultCode, int, int> m_OnResponse_LimitBreak;

		// Token: 0x04002642 RID: 9794
		[Token(Token = "0x4001F9D")]
		private static Action<ResultCode, CharaDetailUI.LevelInfo, CharaDetailUI.LevelInfo> m_OnResponse_Evolution;
	}
}
