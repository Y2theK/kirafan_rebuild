﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000659 RID: 1625
	[Token(Token = "0x200053F")]
	[StructLayout(3)]
	public class EnhanceUpgrade
	{
		// Token: 0x060017C9 RID: 6089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600166C")]
		[Address(RVA = "0x1011E0914", Offset = "0x11E0914", VA = "0x1011E0914")]
		public EnhanceUpgrade(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x060017CA RID: 6090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600166D")]
		[Address(RVA = "0x1011E0948", Offset = "0x11E0948", VA = "0x1011E0948")]
		public void Destroy()
		{
		}

		// Token: 0x060017CB RID: 6091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600166E")]
		[Address(RVA = "0x1011E0994", Offset = "0x11E0994", VA = "0x1011E0994")]
		public void Play(UserCharacterData userCharacterData, int successLevel, Action playExpGaugeCallback)
		{
		}

		// Token: 0x060017CC RID: 6092 RVA: 0x0000AE60 File Offset: 0x00009060
		[Token(Token = "0x600166F")]
		[Address(RVA = "0x1011E09A8", Offset = "0x11E09A8", VA = "0x1011E09A8")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x0400269E RID: 9886
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001FE5")]
		private EnhanceUpgrade.eStep m_Step;

		// Token: 0x0400269F RID: 9887
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FE6")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x040026A0 RID: 9888
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001FE7")]
		private int m_SuccessLevel;

		// Token: 0x040026A1 RID: 9889
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FE8")]
		private UpgradeEffectScene m_EffectScene;

		// Token: 0x040026A2 RID: 9890
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FE9")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x040026A3 RID: 9891
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001FEA")]
		private Action m_PlayExpGaugeCallback;

		// Token: 0x0200065A RID: 1626
		[Token(Token = "0x2000DEE")]
		private enum eStep
		{
			// Token: 0x040026A5 RID: 9893
			[Token(Token = "0x4005A3B")]
			None = -1,
			// Token: 0x040026A6 RID: 9894
			[Token(Token = "0x4005A3C")]
			EffectPrepare,
			// Token: 0x040026A7 RID: 9895
			[Token(Token = "0x4005A3D")]
			EffectPrepare_Wait,
			// Token: 0x040026A8 RID: 9896
			[Token(Token = "0x4005A3E")]
			EffectPlay_Wait,
			// Token: 0x040026A9 RID: 9897
			[Token(Token = "0x4005A3F")]
			PreEnd,
			// Token: 0x040026AA RID: 9898
			[Token(Token = "0x4005A40")]
			End
		}
	}
}
