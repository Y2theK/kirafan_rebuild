﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000674 RID: 1652
	[Token(Token = "0x2000547")]
	[StructLayout(3)]
	public abstract class EnumerationObjectData
	{
		// Token: 0x06001829 RID: 6185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016CB")]
		[Address(RVA = "0x1011E0BEC", Offset = "0x11E0BEC", VA = "0x1011E0BEC")]
		protected EnumerationObjectData(eEnumerationObjectDataType enumerationObjectType)
		{
		}

		// Token: 0x0600182A RID: 6186 RVA: 0x0000B118 File Offset: 0x00009318
		[Token(Token = "0x60016CC")]
		[Address(RVA = "0x1011E0C18", Offset = "0x11E0C18", VA = "0x1011E0C18")]
		public eEnumerationObjectDataType GetEnumerationObjectType()
		{
			return eEnumerationObjectDataType.Error;
		}

		// Token: 0x0400277D RID: 10109
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400206B")]
		protected eEnumerationObjectDataType m_EnumerationObjectType;
	}
}
