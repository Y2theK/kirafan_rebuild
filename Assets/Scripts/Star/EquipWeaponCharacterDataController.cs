﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000637 RID: 1591
	[Token(Token = "0x2000523")]
	[StructLayout(3)]
	public abstract class EquipWeaponCharacterDataController : CharacterDataController
	{
		// Token: 0x06001704 RID: 5892 RVA: 0x0000A908 File Offset: 0x00008B08
		[Token(Token = "0x60015AB")]
		[Address(RVA = "0x1011E0C20", Offset = "0x11E0C20", VA = "0x1011E0C20", Slot = "6")]
		public virtual int GetWeaponId()
		{
			return 0;
		}

		// Token: 0x06001705 RID: 5893
		[Token(Token = "0x60015AC")]
		[Address(Slot = "7")]
		public abstract int GetWeaponIdNaked();

		// Token: 0x06001706 RID: 5894 RVA: 0x0000A920 File Offset: 0x00008B20
		[Token(Token = "0x60015AD")]
		[Address(RVA = "0x1011E0C68", Offset = "0x11E0C68", VA = "0x1011E0C68")]
		public int GetWeaponCost()
		{
			return 0;
		}

		// Token: 0x06001707 RID: 5895
		[Token(Token = "0x60015AE")]
		[Address(Slot = "8")]
		public abstract int GetWeaponSkillID();

		// Token: 0x06001708 RID: 5896
		[Token(Token = "0x60015AF")]
		[Address(Slot = "9")]
		public abstract sbyte GetWeaponSkillExpTableID();

		// Token: 0x06001709 RID: 5897
		[Token(Token = "0x60015B0")]
		[Address(Slot = "10")]
		public abstract int GetWeaponLv();

		// Token: 0x0600170A RID: 5898
		[Token(Token = "0x60015B1")]
		[Address(Slot = "11")]
		public abstract int GetWeaponMaxLv();

		// Token: 0x0600170B RID: 5899
		[Token(Token = "0x60015B2")]
		[Address(Slot = "12")]
		public abstract long GetWeaponExp();

		// Token: 0x0600170C RID: 5900
		[Token(Token = "0x60015B3")]
		[Address(Slot = "13")]
		public abstract int GetWeaponSkillLv();

		// Token: 0x0600170D RID: 5901
		[Token(Token = "0x60015B4")]
		[Address(Slot = "14")]
		public abstract int GetWeaponSkillMaxLv();

		// Token: 0x0600170E RID: 5902
		[Token(Token = "0x60015B5")]
		[Address(Slot = "15")]
		public abstract long GetWeaponSkillTotalUseNum();

		// Token: 0x0600170F RID: 5903 RVA: 0x0000A938 File Offset: 0x00008B38
		[Token(Token = "0x60015B6")]
		[Address(RVA = "0x1011E0D28", Offset = "0x11E0D28", VA = "0x1011E0D28", Slot = "16")]
		public virtual long GetWeaponSkillNowRemainExp()
		{
			return 0L;
		}

		// Token: 0x06001710 RID: 5904
		[Token(Token = "0x60015B7")]
		[Address(Slot = "17")]
		public abstract int GetWeaponPassiveSkillID();

		// Token: 0x06001711 RID: 5905 RVA: 0x0000A950 File Offset: 0x00008B50
		[Token(Token = "0x60015B8")]
		[Address(RVA = "0x1011E0E28", Offset = "0x11E0E28", VA = "0x1011E0E28", Slot = "5")]
		public override int GetCost()
		{
			return 0;
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x0000A968 File Offset: 0x00008B68
		[Token(Token = "0x60015B9")]
		[Address(RVA = "0x1011E0E5C", Offset = "0x11E0E5C", VA = "0x1011E0E5C", Slot = "18")]
		public virtual OutputCharaParam GetWeaponParam()
		{
			return default(OutputCharaParam);
		}

		// Token: 0x06001713 RID: 5907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015BA")]
		[Address(RVA = "0x1011E0EB4", Offset = "0x11E0EB4", VA = "0x1011E0EB4")]
		protected EquipWeaponCharacterDataController()
		{
		}
	}
}
