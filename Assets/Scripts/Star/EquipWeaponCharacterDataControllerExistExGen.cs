﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000639 RID: 1593
	[Token(Token = "0x2000525")]
	[StructLayout(3)]
	public abstract class EquipWeaponCharacterDataControllerExistExGen<ThisType, WrapperType> : EquipWeaponPartyMemberController where ThisType : EquipWeaponCharacterDataControllerExistExGen<ThisType, WrapperType> where WrapperType : CharacterDataWrapperBase
	{
		// Token: 0x06001718 RID: 5912 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015BF")]
		[Address(RVA = "0x1016CC6D4", Offset = "0x16CC6D4", VA = "0x1016CC6D4")]
		protected ThisType ApplyCharacterDataEx(CharacterDataWrapperBase in_memberDataEx)
		{
			return null;
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015C0")]
		[Address(RVA = "0x1016CC858", Offset = "0x16CC858", VA = "0x1016CC858", Slot = "4")]
		protected override CharacterDataWrapperBase GetDataBaseInternal()
		{
			return null;
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015C1")]
		[Address(RVA = "0x1016CC860", Offset = "0x16CC860", VA = "0x1016CC860")]
		protected WrapperType GetDataInternal()
		{
			return null;
		}

		// Token: 0x0600171B RID: 5915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015C2")]
		[Address(RVA = "0x1016CC868", Offset = "0x16CC868", VA = "0x1016CC868")]
		protected EquipWeaponCharacterDataControllerExistExGen()
		{
		}

		// Token: 0x04002645 RID: 9797
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001FA0")]
		private CharacterDataWrapperBase memberData;

		// Token: 0x04002646 RID: 9798
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001FA1")]
		private WrapperType memberData_Ex;
	}
}
