﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000647 RID: 1607
	[Token(Token = "0x2000533")]
	[StructLayout(3)]
	public class EquipWeaponManagedBattlePartiesController : EquipWeaponManagedPartiesControllerExGen<EquipWeaponManagedBattlePartiesController, EquipWeaponManagedBattlePartyController>
	{
		// Token: 0x06001769 RID: 5993 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001610")]
		[Address(RVA = "0x1011E0EBC", Offset = "0x11E0EBC", VA = "0x1011E0EBC")]
		public EquipWeaponManagedBattlePartiesController SetupEx([Optional] CharacterDataWrapperBase supportMember)
		{
			return null;
		}

		// Token: 0x0600176A RID: 5994 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001611")]
		[Address(RVA = "0x1011E1064", Offset = "0x11E1064", VA = "0x1011E1064", Slot = "8")]
		public override EquipWeaponManagedBattlePartiesController SetupEx()
		{
			return null;
		}

		// Token: 0x0600176B RID: 5995 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001612")]
		[Address(RVA = "0x1011E0EC0", Offset = "0x11E0EC0", VA = "0x1011E0EC0")]
		protected EquipWeaponManagedBattlePartiesController SetupProcess([Optional] CharacterDataWrapperBase supportMember)
		{
			return null;
		}

		// Token: 0x0600176C RID: 5996 RVA: 0x0000ACE0 File Offset: 0x00008EE0
		[Token(Token = "0x6001613")]
		[Address(RVA = "0x1011E1134", Offset = "0x11E1134", VA = "0x1011E1134", Slot = "6")]
		public override int HowManyPartyMemberAll()
		{
			return 0;
		}

		// Token: 0x0600176D RID: 5997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001614")]
		[Address(RVA = "0x1011E11A0", Offset = "0x11E11A0", VA = "0x1011E11A0")]
		public EquipWeaponManagedBattlePartiesController()
		{
		}

		// Token: 0x04002654 RID: 9812
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FAF")]
		private CharacterDataWrapperBase m_SupportMember;
	}
}
