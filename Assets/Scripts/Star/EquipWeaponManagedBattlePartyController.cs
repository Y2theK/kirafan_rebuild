﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000642 RID: 1602
	[Token(Token = "0x200052E")]
	[StructLayout(3)]
	public class EquipWeaponManagedBattlePartyController : EquipWeaponManagedPartyControllerExGen<EquipWeaponManagedBattlePartyController, EquipWeaponManagedBattlePartyMemberController>
	{
		// Token: 0x06001750 RID: 5968 RVA: 0x0000AC20 File Offset: 0x00008E20
		[Token(Token = "0x60015F7")]
		[Address(RVA = "0x1011E113C", Offset = "0x11E113C", VA = "0x1011E113C")]
		public static int SupportMemberToPartyMemberAllsNum(CharacterDataWrapperBase supportMember)
		{
			return 0;
		}

		// Token: 0x06001751 RID: 5969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015F8")]
		[Address(RVA = "0x1011E1070", Offset = "0x11E1070", VA = "0x1011E1070")]
		public EquipWeaponManagedBattlePartyController()
		{
		}

		// Token: 0x06001752 RID: 5970 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015F9")]
		[Address(RVA = "0x1011E11F0", Offset = "0x11E11F0", VA = "0x1011E11F0", Slot = "7")]
		public override EquipWeaponManagedBattlePartyController SetupEx(int partyIndex)
		{
			return null;
		}

		// Token: 0x06001753 RID: 5971 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015FA")]
		[Address(RVA = "0x1011E10C0", Offset = "0x11E10C0", VA = "0x1011E10C0")]
		public EquipWeaponManagedBattlePartyController SetupEx(int partyIndex, CharacterDataWrapperBase supportData)
		{
			return null;
		}

		// Token: 0x06001754 RID: 5972 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015FB")]
		[Address(RVA = "0x1011E11F8", Offset = "0x11E11F8", VA = "0x1011E11F8")]
		protected EquipWeaponManagedBattlePartyController SetupProcess(int partyIndex, [Optional] CharacterDataWrapperBase supportMember)
		{
			return null;
		}

		// Token: 0x06001755 RID: 5973 RVA: 0x0000AC38 File Offset: 0x00008E38
		[Token(Token = "0x60015FC")]
		[Address(RVA = "0x1011E1678", Offset = "0x11E1678", VA = "0x1011E1678", Slot = "6")]
		public override int GetPartyCost()
		{
			return 0;
		}

		// Token: 0x06001756 RID: 5974 RVA: 0x0000AC50 File Offset: 0x00008E50
		[Token(Token = "0x60015FD")]
		[Address(RVA = "0x1011E172C", Offset = "0x11E172C", VA = "0x1011E172C", Slot = "5")]
		public override int HowManyPartyMemberEnable()
		{
			return 0;
		}

		// Token: 0x0400264C RID: 9804
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FA7")]
		protected EquipWeaponManagedBattlePartyMemberController[] enablePartyMembers;

		// Token: 0x0400264D RID: 9805
		[Token(Token = "0x4001FA8")]
		public const int PARTYPANEL_CHARA_ALL = 6;

		// Token: 0x0400264E RID: 9806
		[Token(Token = "0x4001FA9")]
		public const int PARTYPANEL_CHARA_ENABLES = 5;
	}
}
