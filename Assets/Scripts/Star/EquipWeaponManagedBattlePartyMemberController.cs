﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063C RID: 1596
	[Token(Token = "0x2000528")]
	[StructLayout(3)]
	public class EquipWeaponManagedBattlePartyMemberController : EquipWeaponManagedPartyMemberControllerExGen<EquipWeaponManagedBattlePartyMemberController>
	{
		// Token: 0x0600172D RID: 5933 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015D4")]
		[Address(RVA = "0x1011E1758", Offset = "0x11E1758", VA = "0x1011E1758", Slot = "19")]
		protected override CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600172E RID: 5934 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015D5")]
		[Address(RVA = "0x1011E185C", Offset = "0x11E185C", VA = "0x1011E185C", Slot = "20")]
		protected override UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600172F RID: 5935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015D6")]
		[Address(RVA = "0x1011E1558", Offset = "0x11E1558", VA = "0x1011E1558")]
		public EquipWeaponManagedBattlePartyMemberController()
		{
		}
	}
}
