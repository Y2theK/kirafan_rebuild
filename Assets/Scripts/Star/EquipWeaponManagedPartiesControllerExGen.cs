﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000646 RID: 1606
	[Token(Token = "0x2000532")]
	[StructLayout(3)]
	public class EquipWeaponManagedPartiesControllerExGen<ThisType, PartyType> : EquipWeaponPartiesController where ThisType : EquipWeaponManagedPartiesControllerExGen<ThisType, PartyType> where PartyType : EquipWeaponPartyController
	{
		// Token: 0x06001765 RID: 5989 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600160C")]
		[Address(RVA = "0x1016CC894", Offset = "0x16CC894", VA = "0x1016CC894", Slot = "8")]
		public virtual ThisType SetupEx()
		{
			return null;
		}

		// Token: 0x06001766 RID: 5990 RVA: 0x0000ACC8 File Offset: 0x00008EC8
		[Token(Token = "0x600160D")]
		[Address(RVA = "0x1016CC950", Offset = "0x16CC950", VA = "0x1016CC950", Slot = "5")]
		public override int HowManyParties()
		{
			return 0;
		}

		// Token: 0x06001767 RID: 5991 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600160E")]
		[Address(RVA = "0x1016CC97C", Offset = "0x16CC97C", VA = "0x1016CC97C", Slot = "7")]
		public override EquipWeaponPartyController GetParty(int index)
		{
			return null;
		}

		// Token: 0x06001768 RID: 5992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600160F")]
		[Address(RVA = "0x1016CC9CC", Offset = "0x16CC9CC", VA = "0x1016CC9CC")]
		public EquipWeaponManagedPartiesControllerExGen()
		{
		}

		// Token: 0x04002653 RID: 9811
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001FAE")]
		protected PartyType[] parties;
	}
}
