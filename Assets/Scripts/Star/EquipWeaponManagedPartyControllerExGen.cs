﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000641 RID: 1601
	[Token(Token = "0x200052D")]
	[StructLayout(3)]
	public abstract class EquipWeaponManagedPartyControllerExGen<ThisType, EnablePartyMemberType> : EquipWeaponPartyController where ThisType : EquipWeaponManagedPartyControllerExGen<ThisType, EnablePartyMemberType> where EnablePartyMemberType : EquipWeaponManagedPartyMemberController
	{
		// Token: 0x0600174E RID: 5966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015F5")]
		[Address(RVA = "0x1016CC9F8", Offset = "0x16CC9F8", VA = "0x1016CC9F8")]
		public EquipWeaponManagedPartyControllerExGen()
		{
		}

		// Token: 0x0600174F RID: 5967 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015F6")]
		[Address(RVA = "0x1016CCA24", Offset = "0x16CCA24", VA = "0x1016CCA24", Slot = "7")]
		public virtual ThisType SetupEx(int partyIndex)
		{
			return null;
		}
	}
}
