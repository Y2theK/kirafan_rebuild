﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063A RID: 1594
	[Token(Token = "0x2000526")]
	[StructLayout(3)]
	public abstract class EquipWeaponManagedPartyMemberController : EquipWeaponCharacterDataControllerExistExGen<EquipWeaponManagedPartyMemberController, UserCharacterDataWrapper>
	{
		// Token: 0x0600171C RID: 5916 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015C3")]
		[Address(RVA = "0x1011E1920", Offset = "0x11E1920", VA = "0x1011E1920")]
		public EquipWeaponManagedPartyMemberController Setup(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600171D RID: 5917 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015C4")]
		[Address(RVA = "0x1011E19E0", Offset = "0x11E19E0", VA = "0x1011E19E0", Slot = "19")]
		protected virtual CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600171E RID: 5918 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015C5")]
		[Address(RVA = "0x1011E19E8", Offset = "0x11E19E8", VA = "0x1011E19E8", Slot = "20")]
		protected virtual UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600171F RID: 5919 RVA: 0x0000A9B0 File Offset: 0x00008BB0
		[Token(Token = "0x60015C6")]
		[Address(RVA = "0x1011E19F0", Offset = "0x11E19F0", VA = "0x1011E19F0", Slot = "7")]
		public override int GetWeaponIdNaked()
		{
			return 0;
		}

		// Token: 0x06001720 RID: 5920 RVA: 0x0000A9C8 File Offset: 0x00008BC8
		[Token(Token = "0x60015C7")]
		[Address(RVA = "0x1011E1A3C", Offset = "0x11E1A3C", VA = "0x1011E1A3C", Slot = "8")]
		public override int GetWeaponSkillID()
		{
			return 0;
		}

		// Token: 0x06001721 RID: 5921 RVA: 0x0000A9E0 File Offset: 0x00008BE0
		[Token(Token = "0x60015C8")]
		[Address(RVA = "0x1011E1AA0", Offset = "0x11E1AA0", VA = "0x1011E1AA0", Slot = "9")]
		public override sbyte GetWeaponSkillExpTableID()
		{
			return 0;
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x0000A9F8 File Offset: 0x00008BF8
		[Token(Token = "0x60015C9")]
		[Address(RVA = "0x1011E1B04", Offset = "0x11E1B04", VA = "0x1011E1B04", Slot = "10")]
		public override int GetWeaponLv()
		{
			return 0;
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x0000AA10 File Offset: 0x00008C10
		[Token(Token = "0x60015CA")]
		[Address(RVA = "0x1011E1B50", Offset = "0x11E1B50", VA = "0x1011E1B50", Slot = "11")]
		public override int GetWeaponMaxLv()
		{
			return 0;
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x0000AA28 File Offset: 0x00008C28
		[Token(Token = "0x60015CB")]
		[Address(RVA = "0x1011E1B9C", Offset = "0x11E1B9C", VA = "0x1011E1B9C", Slot = "12")]
		public override long GetWeaponExp()
		{
			return 0L;
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x0000AA40 File Offset: 0x00008C40
		[Token(Token = "0x60015CC")]
		[Address(RVA = "0x1011E1BE4", Offset = "0x11E1BE4", VA = "0x1011E1BE4", Slot = "13")]
		public override int GetWeaponSkillLv()
		{
			return 0;
		}

		// Token: 0x06001726 RID: 5926 RVA: 0x0000AA58 File Offset: 0x00008C58
		[Token(Token = "0x60015CD")]
		[Address(RVA = "0x1011E1C44", Offset = "0x11E1C44", VA = "0x1011E1C44", Slot = "14")]
		public override int GetWeaponSkillMaxLv()
		{
			return 0;
		}

		// Token: 0x06001727 RID: 5927 RVA: 0x0000AA70 File Offset: 0x00008C70
		[Token(Token = "0x60015CE")]
		[Address(RVA = "0x1011E1CA4", Offset = "0x11E1CA4", VA = "0x1011E1CA4", Slot = "15")]
		public override long GetWeaponSkillTotalUseNum()
		{
			return 0L;
		}

		// Token: 0x06001728 RID: 5928 RVA: 0x0000AA88 File Offset: 0x00008C88
		[Token(Token = "0x60015CF")]
		[Address(RVA = "0x1011E1D04", Offset = "0x11E1D04", VA = "0x1011E1D04", Slot = "18")]
		public override OutputCharaParam GetWeaponParam()
		{
			return default(OutputCharaParam);
		}

		// Token: 0x06001729 RID: 5929 RVA: 0x0000AAA0 File Offset: 0x00008CA0
		[Token(Token = "0x60015D0")]
		[Address(RVA = "0x1011E1DBC", Offset = "0x11E1DBC", VA = "0x1011E1DBC", Slot = "17")]
		public override int GetWeaponPassiveSkillID()
		{
			return 0;
		}

		// Token: 0x0600172A RID: 5930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015D1")]
		[Address(RVA = "0x1011E1E04", Offset = "0x11E1E04", VA = "0x1011E1E04")]
		protected EquipWeaponManagedPartyMemberController()
		{
		}

		// Token: 0x04002647 RID: 9799
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FA2")]
		protected UserWeaponData weaponData;
	}
}
