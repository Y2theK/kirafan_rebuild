﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063B RID: 1595
	[Token(Token = "0x2000527")]
	[StructLayout(3)]
	public abstract class EquipWeaponManagedPartyMemberControllerExGen<ThisType> : EquipWeaponManagedPartyMemberController where ThisType : EquipWeaponManagedPartyMemberControllerExGen<ThisType>
	{
		// Token: 0x0600172B RID: 5931 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015D2")]
		[Address(RVA = "0x1016CCB0C", Offset = "0x16CCB0C", VA = "0x1016CCB0C")]
		public ThisType SetupEx(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x0600172C RID: 5932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015D3")]
		[Address(RVA = "0x1016CCBF8", Offset = "0x16CCBF8", VA = "0x1016CCBF8")]
		protected EquipWeaponManagedPartyMemberControllerExGen()
		{
		}
	}
}
