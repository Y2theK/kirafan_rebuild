﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000648 RID: 1608
	[Token(Token = "0x2000534")]
	[StructLayout(3)]
	public class EquipWeaponManagedSupportPartiesController : EquipWeaponManagedPartiesControllerExGen<EquipWeaponManagedSupportPartiesController, EquipWeaponManagedSupportPartyController>
	{
		// Token: 0x0600176E RID: 5998 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001615")]
		[Address(RVA = "0x1011E1E54", Offset = "0x11E1E54", VA = "0x1011E1E54", Slot = "8")]
		public override EquipWeaponManagedSupportPartiesController SetupEx()
		{
			return null;
		}

		// Token: 0x0600176F RID: 5999 RVA: 0x0000ACF8 File Offset: 0x00008EF8
		[Token(Token = "0x6001616")]
		[Address(RVA = "0x1011E2044", Offset = "0x11E2044", VA = "0x1011E2044", Slot = "6")]
		public override int HowManyPartyMemberAll()
		{
			return 0;
		}

		// Token: 0x06001770 RID: 6000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001617")]
		[Address(RVA = "0x1011E204C", Offset = "0x11E204C", VA = "0x1011E204C")]
		public EquipWeaponManagedSupportPartiesController()
		{
		}
	}
}
