﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000643 RID: 1603
	[Token(Token = "0x200052F")]
	[StructLayout(3)]
	public class EquipWeaponManagedSupportPartyController : EquipWeaponManagedPartyControllerExGen<EquipWeaponManagedSupportPartyController, EquipWeaponManagedSupportPartyMemberController>
	{
		// Token: 0x06001757 RID: 5975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015FE")]
		[Address(RVA = "0x1011E1FF4", Offset = "0x11E1FF4", VA = "0x1011E1FF4")]
		public EquipWeaponManagedSupportPartyController()
		{
		}

		// Token: 0x06001758 RID: 5976 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015FF")]
		[Address(RVA = "0x1011E209C", Offset = "0x11E209C", VA = "0x1011E209C", Slot = "7")]
		public override EquipWeaponManagedSupportPartyController SetupEx(int partyIndex)
		{
			return null;
		}

		// Token: 0x06001759 RID: 5977 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001600")]
		[Address(RVA = "0x1011E2108", Offset = "0x11E2108", VA = "0x1011E2108")]
		protected EquipWeaponManagedSupportPartyController SetupProcess(int partyIndex)
		{
			return null;
		}

		// Token: 0x0600175A RID: 5978 RVA: 0x0000AC68 File Offset: 0x00008E68
		[Token(Token = "0x6001601")]
		[Address(RVA = "0x1011E2500", Offset = "0x11E2500", VA = "0x1011E2500", Slot = "5")]
		public override int HowManyPartyMemberEnable()
		{
			return 0;
		}

		// Token: 0x0400264F RID: 9807
		[Token(Token = "0x4001FAA")]
		public const int PARTYPANEL_CHARA_ALL = 8;

		// Token: 0x04002650 RID: 9808
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FAB")]
		protected EquipWeaponManagedSupportPartyMemberController[] enablePartyMembers;
	}
}
