﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063D RID: 1597
	[Token(Token = "0x2000529")]
	[StructLayout(3)]
	public class EquipWeaponManagedSupportPartyMemberController : EquipWeaponManagedPartyMemberControllerExGen<EquipWeaponManagedSupportPartyMemberController>
	{
		// Token: 0x06001730 RID: 5936 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015D7")]
		[Address(RVA = "0x1011E252C", Offset = "0x11E252C", VA = "0x1011E252C", Slot = "19")]
		protected override CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015D8")]
		[Address(RVA = "0x1011E2650", Offset = "0x11E2650", VA = "0x1011E2650", Slot = "20")]
		protected override UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015D9")]
		[Address(RVA = "0x1011E24B0", Offset = "0x11E24B0", VA = "0x1011E24B0")]
		public EquipWeaponManagedSupportPartyMemberController()
		{
		}
	}
}
