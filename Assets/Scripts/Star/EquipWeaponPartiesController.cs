﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000645 RID: 1605
	[Token(Token = "0x2000531")]
	[StructLayout(3)]
	public class EquipWeaponPartiesController
	{
		// Token: 0x06001760 RID: 5984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001607")]
		[Address(RVA = "0x1011E265C", Offset = "0x11E265C", VA = "0x1011E265C")]
		public EquipWeaponPartiesController()
		{
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001608")]
		[Address(RVA = "0x1011E106C", Offset = "0x11E106C", VA = "0x1011E106C", Slot = "4")]
		public virtual EquipWeaponPartiesController Setup()
		{
			return null;
		}

		// Token: 0x06001762 RID: 5986 RVA: 0x0000AC98 File Offset: 0x00008E98
		[Token(Token = "0x6001609")]
		[Address(RVA = "0x1011E2664", Offset = "0x11E2664", VA = "0x1011E2664", Slot = "5")]
		public virtual int HowManyParties()
		{
			return 0;
		}

		// Token: 0x06001763 RID: 5987 RVA: 0x0000ACB0 File Offset: 0x00008EB0
		[Token(Token = "0x600160A")]
		[Address(RVA = "0x1011E266C", Offset = "0x11E266C", VA = "0x1011E266C", Slot = "6")]
		public virtual int HowManyPartyMemberAll()
		{
			return 0;
		}

		// Token: 0x06001764 RID: 5988 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600160B")]
		[Address(RVA = "0x1011E2674", Offset = "0x11E2674", VA = "0x1011E2674", Slot = "7")]
		public virtual EquipWeaponPartyController GetParty(int index)
		{
			return null;
		}
	}
}
