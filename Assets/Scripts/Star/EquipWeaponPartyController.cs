﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000640 RID: 1600
	[Token(Token = "0x200052C")]
	[StructLayout(3)]
	public abstract class EquipWeaponPartyController
	{
		// Token: 0x06001743 RID: 5955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015EA")]
		[Address(RVA = "0x1011E267C", Offset = "0x11E267C", VA = "0x1011E267C")]
		public EquipWeaponPartyController()
		{
		}

		// Token: 0x06001744 RID: 5956 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015EB")]
		[Address(RVA = "0x1011E2684", Offset = "0x11E2684", VA = "0x1011E2684", Slot = "4")]
		protected virtual EquipWeaponPartyController SetupBase(int partyIndex)
		{
			return null;
		}

		// Token: 0x06001745 RID: 5957 RVA: 0x0000ABC0 File Offset: 0x00008DC0
		[Token(Token = "0x60015EC")]
		[Address(RVA = "0x1011E268C", Offset = "0x11E268C", VA = "0x1011E268C")]
		public int HowManyMembers()
		{
			return 0;
		}

		// Token: 0x06001746 RID: 5958
		[Token(Token = "0x60015ED")]
		[Address(Slot = "5")]
		public abstract int HowManyPartyMemberEnable();

		// Token: 0x06001747 RID: 5959 RVA: 0x0000ABD8 File Offset: 0x00008DD8
		[Token(Token = "0x60015EE")]
		[Address(RVA = "0x1011E26B8", Offset = "0x11E26B8", VA = "0x1011E26B8")]
		public long GetMngId()
		{
			return 0L;
		}

		// Token: 0x06001748 RID: 5960 RVA: 0x0000ABF0 File Offset: 0x00008DF0
		[Token(Token = "0x60015EF")]
		[Address(RVA = "0x1011E26C0", Offset = "0x11E26C0", VA = "0x1011E26C0")]
		public int GetPartyIndex()
		{
			return 0;
		}

		// Token: 0x06001749 RID: 5961 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015F0")]
		[Address(RVA = "0x1011E26C8", Offset = "0x11E26C8", VA = "0x1011E26C8")]
		public string GetPartyName()
		{
			return null;
		}

		// Token: 0x0600174A RID: 5962 RVA: 0x0000AC08 File Offset: 0x00008E08
		[Token(Token = "0x60015F1")]
		[Address(RVA = "0x1011E26D0", Offset = "0x11E26D0", VA = "0x1011E26D0", Slot = "6")]
		public virtual int GetPartyCost()
		{
			return 0;
		}

		// Token: 0x0600174B RID: 5963 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015F2")]
		[Address(RVA = "0x1011E276C", Offset = "0x11E276C", VA = "0x1011E276C")]
		public Dictionary<int, int> GetPartyEventQuestDropExtTable()
		{
			return null;
		}

		// Token: 0x0600174C RID: 5964 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015F3")]
		[Address(RVA = "0x1011E2A7C", Offset = "0x11E2A7C", VA = "0x1011E2A7C")]
		public EquipWeaponPartyMemberController GetMember(int index)
		{
			return null;
		}

		// Token: 0x0600174D RID: 5965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015F4")]
		[Address(RVA = "0x1011E2AE4", Offset = "0x11E2AE4", VA = "0x1011E2AE4")]
		public void SetPartyName(string partyName)
		{
		}

		// Token: 0x04002648 RID: 9800
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001FA3")]
		protected EquipWeaponPartyMemberController[] partyMembers;

		// Token: 0x04002649 RID: 9801
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FA4")]
		protected int m_PartyIndex;

		// Token: 0x0400264A RID: 9802
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001FA5")]
		protected long m_MngID;

		// Token: 0x0400264B RID: 9803
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FA6")]
		protected string m_Name;
	}
}
