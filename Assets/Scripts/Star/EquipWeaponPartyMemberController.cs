﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000638 RID: 1592
	[Token(Token = "0x2000524")]
	[StructLayout(3)]
	public abstract class EquipWeaponPartyMemberController : EquipWeaponCharacterDataController
	{
		// Token: 0x06001714 RID: 5908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015BB")]
		[Address(RVA = "0x1011E19C8", Offset = "0x11E19C8", VA = "0x1011E19C8")]
		protected void SetIndices(int in_partyIndex, int in_partySlotIndex)
		{
		}

		// Token: 0x06001715 RID: 5909 RVA: 0x0000A980 File Offset: 0x00008B80
		[Token(Token = "0x60015BC")]
		[Address(RVA = "0x1011E19D0", Offset = "0x11E19D0", VA = "0x1011E19D0")]
		public int GetPartyIndex()
		{
			return 0;
		}

		// Token: 0x06001716 RID: 5910 RVA: 0x0000A998 File Offset: 0x00008B98
		[Token(Token = "0x60015BD")]
		[Address(RVA = "0x1011E19D8", Offset = "0x11E19D8", VA = "0x1011E19D8")]
		public int GetPartySlotIndex()
		{
			return 0;
		}

		// Token: 0x06001717 RID: 5911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015BE")]
		[Address(RVA = "0x1011E2AEC", Offset = "0x11E2AEC", VA = "0x1011E2AEC")]
		protected EquipWeaponPartyMemberController()
		{
		}

		// Token: 0x04002643 RID: 9795
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F9E")]
		private int partyIndex;

		// Token: 0x04002644 RID: 9796
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001F9F")]
		private int partySlotIndex;
	}
}
