﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063E RID: 1598
	[Token(Token = "0x200052A")]
	[StructLayout(3)]
	public abstract class EquipWeaponUnmanagedPartyMemberController<ThisType, WrapperType> : EquipWeaponCharacterDataControllerExistExGen<ThisType, WrapperType> where ThisType : EquipWeaponUnmanagedPartyMemberController<ThisType, WrapperType> where WrapperType : CharacterDataWrapperBase
	{
		// Token: 0x06001733 RID: 5939 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015DA")]
		[Address(RVA = "0x1016CCC24", Offset = "0x16CCC24", VA = "0x1016CCC24")]
		public ThisType Setup(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015DB")]
		[Address(RVA = "0x1016CCD10", Offset = "0x16CCD10", VA = "0x1016CCD10")]
		protected EquipWeaponUnmanagedPartyMemberController()
		{
		}
	}
}
