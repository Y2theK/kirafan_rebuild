﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000649 RID: 1609
	[Token(Token = "0x2000535")]
	[StructLayout(3)]
	public class EquipWeaponUnmanagedSupportPartiesController : EquipWeaponPartiesController
	{
		// Token: 0x06001771 RID: 6001 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001618")]
		[Address(RVA = "0x1011E2AF4", Offset = "0x11E2AF4", VA = "0x1011E2AF4")]
		public EquipWeaponUnmanagedSupportPartiesController SetupEx(List<EquipWeaponUnmanagedSupportPartiesController.UnmanagedSupportPartyInformation> partyInfos, int supportLimit)
		{
			return null;
		}

		// Token: 0x06001772 RID: 6002 RVA: 0x0000AD10 File Offset: 0x00008F10
		[Token(Token = "0x6001619")]
		[Address(RVA = "0x1011E2D20", Offset = "0x11E2D20", VA = "0x1011E2D20", Slot = "5")]
		public override int HowManyParties()
		{
			return 0;
		}

		// Token: 0x06001773 RID: 6003 RVA: 0x0000AD28 File Offset: 0x00008F28
		[Token(Token = "0x600161A")]
		[Address(RVA = "0x1011E2D4C", Offset = "0x11E2D4C", VA = "0x1011E2D4C", Slot = "6")]
		public override int HowManyPartyMemberAll()
		{
			return 0;
		}

		// Token: 0x06001774 RID: 6004 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600161B")]
		[Address(RVA = "0x1011E2D54", Offset = "0x11E2D54", VA = "0x1011E2D54", Slot = "7")]
		public override EquipWeaponPartyController GetParty(int index)
		{
			return null;
		}

		// Token: 0x06001775 RID: 6005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600161C")]
		[Address(RVA = "0x1011E2DA4", Offset = "0x11E2DA4", VA = "0x1011E2DA4")]
		public EquipWeaponUnmanagedSupportPartiesController()
		{
		}

		// Token: 0x04002655 RID: 9813
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001FB0")]
		protected EquipWeaponUnmanagedSupportPartyController[] parties;

		// Token: 0x0200064A RID: 1610
		[Token(Token = "0x2000DE8")]
		public class UnmanagedSupportPartyInformation
		{
			// Token: 0x06001776 RID: 6006 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E1D")]
			[Address(RVA = "0x1011E2DAC", Offset = "0x11E2DAC", VA = "0x1011E2DAC")]
			public UnmanagedSupportPartyInformation()
			{
			}

			// Token: 0x04002656 RID: 9814
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005A26")]
			public List<UserSupportData> supportDatas;

			// Token: 0x04002657 RID: 9815
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005A27")]
			public string partyName;
		}
	}
}
