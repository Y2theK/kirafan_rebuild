﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000644 RID: 1604
	[Token(Token = "0x2000530")]
	[StructLayout(3)]
	public class EquipWeaponUnmanagedSupportPartyController : EquipWeaponPartyController
	{
		// Token: 0x0600175B RID: 5979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001602")]
		[Address(RVA = "0x1011E2CBC", Offset = "0x11E2CBC", VA = "0x1011E2CBC")]
		public EquipWeaponUnmanagedSupportPartyController()
		{
		}

		// Token: 0x0600175C RID: 5980 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001603")]
		[Address(RVA = "0x1011E2CC4", Offset = "0x11E2CC4", VA = "0x1011E2CC4")]
		public EquipWeaponUnmanagedSupportPartyController Setup(int partyIndex, List<UserSupportData> supportDatas, string partyName, int supportLimit)
		{
			return null;
		}

		// Token: 0x0600175D RID: 5981 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001604")]
		[Address(RVA = "0x1011E2DB4", Offset = "0x11E2DB4", VA = "0x1011E2DB4")]
		protected EquipWeaponUnmanagedSupportPartyController SetupProcess(int partyIndex, List<UserSupportData> supportDatas, string partyName, int supportLimit)
		{
			return null;
		}

		// Token: 0x0600175E RID: 5982 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001605")]
		[Address(RVA = "0x1011E2F38", Offset = "0x11E2F38", VA = "0x1011E2F38")]
		protected EquipWeaponUnmanagedSupportPartyController SetupProcess(int partyIndex, List<CharacterDataWrapperBase> supportDatas, string partyName, int PartyPanelCharaAll, int PartyPanelCharaEnables)
		{
			return null;
		}

		// Token: 0x0600175F RID: 5983 RVA: 0x0000AC80 File Offset: 0x00008E80
		[Token(Token = "0x6001606")]
		[Address(RVA = "0x1011E32AC", Offset = "0x11E32AC", VA = "0x1011E32AC", Slot = "5")]
		public override int HowManyPartyMemberEnable()
		{
			return 0;
		}

		// Token: 0x04002651 RID: 9809
		[Token(Token = "0x4001FAC")]
		public const int PARTYPANEL_CHARA_ALL = 8;

		// Token: 0x04002652 RID: 9810
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FAD")]
		protected EquipWeaponUnmanagedSupportPartyMemberController[] enablePartyMembers;
	}
}
