﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200063F RID: 1599
	[Token(Token = "0x200052B")]
	[StructLayout(3)]
	public class EquipWeaponUnmanagedSupportPartyMemberController : EquipWeaponUnmanagedPartyMemberController<EquipWeaponUnmanagedSupportPartyMemberController, UserSupportDataWrapper>
	{
		// Token: 0x06001735 RID: 5941 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015DC")]
		[Address(RVA = "0x1011E15F8", Offset = "0x11E15F8", VA = "0x1011E15F8")]
		public EquipWeaponUnmanagedSupportPartyMemberController Setup(CharacterDataWrapperBase characterData, int in_partyIndex = -1, int in_partySlotIndex = -1)
		{
			return null;
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60015DD")]
		[Address(RVA = "0x1011E32D8", Offset = "0x11E32D8", VA = "0x1011E32D8")]
		public EquipWeaponUnmanagedSupportPartyMemberController SetCharacterDataWrapperBase(CharacterDataWrapperBase supportData)
		{
			return null;
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x0000AAB8 File Offset: 0x00008CB8
		[Token(Token = "0x60015DE")]
		[Address(RVA = "0x1011E3338", Offset = "0x11E3338", VA = "0x1011E3338", Slot = "7")]
		public override int GetWeaponIdNaked()
		{
			return 0;
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x0000AAD0 File Offset: 0x00008CD0
		[Token(Token = "0x60015DF")]
		[Address(RVA = "0x1011E33C0", Offset = "0x11E33C0", VA = "0x1011E33C0", Slot = "8")]
		public override int GetWeaponSkillID()
		{
			return 0;
		}

		// Token: 0x06001739 RID: 5945 RVA: 0x0000AAE8 File Offset: 0x00008CE8
		[Token(Token = "0x60015E0")]
		[Address(RVA = "0x1011E3448", Offset = "0x11E3448", VA = "0x1011E3448", Slot = "9")]
		public override sbyte GetWeaponSkillExpTableID()
		{
			return 0;
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x0000AB00 File Offset: 0x00008D00
		[Token(Token = "0x60015E1")]
		[Address(RVA = "0x1011E34D0", Offset = "0x11E34D0", VA = "0x1011E34D0", Slot = "10")]
		public override int GetWeaponLv()
		{
			return 0;
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x0000AB18 File Offset: 0x00008D18
		[Token(Token = "0x60015E2")]
		[Address(RVA = "0x1011E3558", Offset = "0x11E3558", VA = "0x1011E3558", Slot = "11")]
		public override int GetWeaponMaxLv()
		{
			return 0;
		}

		// Token: 0x0600173C RID: 5948 RVA: 0x0000AB30 File Offset: 0x00008D30
		[Token(Token = "0x60015E3")]
		[Address(RVA = "0x1011E35E0", Offset = "0x11E35E0", VA = "0x1011E35E0", Slot = "12")]
		public override long GetWeaponExp()
		{
			return 0L;
		}

		// Token: 0x0600173D RID: 5949 RVA: 0x0000AB48 File Offset: 0x00008D48
		[Token(Token = "0x60015E4")]
		[Address(RVA = "0x1011E3668", Offset = "0x11E3668", VA = "0x1011E3668", Slot = "13")]
		public override int GetWeaponSkillLv()
		{
			return 0;
		}

		// Token: 0x0600173E RID: 5950 RVA: 0x0000AB60 File Offset: 0x00008D60
		[Token(Token = "0x60015E5")]
		[Address(RVA = "0x1011E36F0", Offset = "0x11E36F0", VA = "0x1011E36F0", Slot = "14")]
		public override int GetWeaponSkillMaxLv()
		{
			return 0;
		}

		// Token: 0x0600173F RID: 5951 RVA: 0x0000AB78 File Offset: 0x00008D78
		[Token(Token = "0x60015E6")]
		[Address(RVA = "0x1011E3778", Offset = "0x11E3778", VA = "0x1011E3778", Slot = "15")]
		public override long GetWeaponSkillTotalUseNum()
		{
			return 0L;
		}

		// Token: 0x06001740 RID: 5952 RVA: 0x0000AB90 File Offset: 0x00008D90
		[Token(Token = "0x60015E7")]
		[Address(RVA = "0x1011E3780", Offset = "0x11E3780", VA = "0x1011E3780", Slot = "16")]
		public override long GetWeaponSkillNowRemainExp()
		{
			return 0L;
		}

		// Token: 0x06001741 RID: 5953 RVA: 0x0000ABA8 File Offset: 0x00008DA8
		[Token(Token = "0x60015E8")]
		[Address(RVA = "0x1011E3788", Offset = "0x11E3788", VA = "0x1011E3788", Slot = "17")]
		public override int GetWeaponPassiveSkillID()
		{
			return 0;
		}

		// Token: 0x06001742 RID: 5954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60015E9")]
		[Address(RVA = "0x1011E15A8", Offset = "0x11E15A8", VA = "0x1011E15A8")]
		public EquipWeaponUnmanagedSupportPartyMemberController()
		{
		}
	}
}
