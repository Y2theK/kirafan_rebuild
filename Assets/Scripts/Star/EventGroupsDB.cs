﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E6 RID: 1254
	[Token(Token = "0x20003DC")]
	[StructLayout(3)]
	public class EventGroupsDB : ScriptableObject
	{
		// Token: 0x060014FD RID: 5373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013B2")]
		[Address(RVA = "0x1011E3810", Offset = "0x11E3810", VA = "0x1011E3810")]
		public EventGroupsDB()
		{
		}

		// Token: 0x0400188C RID: 6284
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40011FB")]
		public EventGroupsDB_Param[] m_Params;
	}
}
