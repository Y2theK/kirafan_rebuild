﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004E5 RID: 1253
	[Token(Token = "0x20003DB")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct EventGroupsDB_Param
	{
		// Token: 0x04001888 RID: 6280
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40011F7")]
		public int m_ID;

		// Token: 0x04001889 RID: 6281
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40011F8")]
		public int m_BannerID;

		// Token: 0x0400188A RID: 6282
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40011F9")]
		public string m_LockedText;

		// Token: 0x0400188B RID: 6283
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40011FA")]
		public string m_Url;
	}
}
