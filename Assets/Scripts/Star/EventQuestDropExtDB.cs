﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E8 RID: 1256
	[Token(Token = "0x20003DE")]
	[StructLayout(3)]
	public class EventQuestDropExtDB : ScriptableObject
	{
		// Token: 0x060014FE RID: 5374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013B3")]
		[Address(RVA = "0x1011E397C", Offset = "0x11E397C", VA = "0x1011E397C")]
		public EventQuestDropExtDB()
		{
		}

		// Token: 0x04001891 RID: 6289
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001200")]
		public EventQuestDropExtDB_Param[] m_Params;
	}
}
