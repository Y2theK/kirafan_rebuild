﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200050E RID: 1294
	[Token(Token = "0x2000404")]
	[StructLayout(3)]
	public static class EventQuestDropExtDB_Ext
	{
		// Token: 0x06001550 RID: 5456 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001405")]
		[Address(RVA = "0x1011E3984", Offset = "0x11E3984", VA = "0x1011E3984")]
		public static Dictionary<int, int> GetPlusNum(this EventQuestDropExtDB self, int eventType, int charaID, eEventQuestDropExtPlusType plusType)
		{
			return null;
		}

		// Token: 0x06001551 RID: 5457 RVA: 0x00009198 File Offset: 0x00007398
		[Token(Token = "0x6001406")]
		[Address(RVA = "0x1011E3D8C", Offset = "0x11E3D8C", VA = "0x1011E3D8C")]
		public static int GetPlusCharaNum(this EventQuestDropExtDB self, int eventType)
		{
			return 0;
		}
	}
}
