﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004E7 RID: 1255
	[Token(Token = "0x20003DD")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct EventQuestDropExtDB_Param
	{
		// Token: 0x0400188D RID: 6285
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40011FC")]
		public int m_EventType;

		// Token: 0x0400188E RID: 6286
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40011FD")]
		public int m_CharaID;

		// Token: 0x0400188F RID: 6287
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40011FE")]
		public int[] m_ItemIDs;

		// Token: 0x04001890 RID: 6288
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40011FF")]
		public int[] m_Pluses;
	}
}
