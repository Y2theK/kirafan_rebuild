﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004EE RID: 1262
	[Token(Token = "0x20003E4")]
	[StructLayout(3)]
	public class EventQuestUISettingDB : ScriptableObject
	{
		// Token: 0x060014FF RID: 5375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013B4")]
		[Address(RVA = "0x1011E3E3C", Offset = "0x11E3E3C", VA = "0x1011E3E3C")]
		public EventQuestUISettingDB()
		{
		}

		// Token: 0x040018AE RID: 6318
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400121D")]
		public EventQuestUISettingDB_Param[] m_Params;
	}
}
