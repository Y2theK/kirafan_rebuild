﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200050F RID: 1295
	[Token(Token = "0x2000405")]
	[StructLayout(3)]
	public static class EventQuestUISettingDB_Ext
	{
		// Token: 0x06001552 RID: 5458 RVA: 0x000091B0 File Offset: 0x000073B0
		[Token(Token = "0x6001407")]
		[Address(RVA = "0x1011E3E44", Offset = "0x11E3E44", VA = "0x1011E3E44")]
		public static EventQuestUISettingDB_Param? GetParamByEventType(this EventQuestUISettingDB self, int eventType)
		{
			return null;
		}

		// Token: 0x06001553 RID: 5459 RVA: 0x000091C8 File Offset: 0x000073C8
		[Token(Token = "0x6001408")]
		[Address(RVA = "0x1011E3FF8", Offset = "0x11E3FF8", VA = "0x1011E3FF8")]
		public static EventQuestUISettingDB_Param? GetParamByGroupID(this EventQuestUISettingDB self, int eventType)
		{
			return null;
		}

		// Token: 0x06001554 RID: 5460 RVA: 0x000091E0 File Offset: 0x000073E0
		[Token(Token = "0x6001409")]
		[Address(RVA = "0x1011E41B0", Offset = "0x11E41B0", VA = "0x1011E41B0")]
		public static EventQuestUISettingDB_Param? GetParamByCharaID(this EventQuestUISettingDB self, int charaID)
		{
			return null;
		}
	}
}
