﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004ED RID: 1261
	[Token(Token = "0x20003E3")]
	[Serializable]
	[StructLayout(0)]
	public struct EventQuestUISettingDB_Param
	{
		// Token: 0x040018A1 RID: 6305
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001210")]
		public int m_KindOfID;

		// Token: 0x040018A2 RID: 6306
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001211")]
		public int m_ID;

		// Token: 0x040018A3 RID: 6307
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001212")]
		public int m_CharaDispMode;

		// Token: 0x040018A4 RID: 6308
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001213")]
		public int m_CharaID;

		// Token: 0x040018A5 RID: 6309
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001214")]
		public int m_CharaIllustType;

		// Token: 0x040018A6 RID: 6310
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001215")]
		public int m_CharaAnchor;

		// Token: 0x040018A7 RID: 6311
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001216")]
		public float m_CharaOffsetX;

		// Token: 0x040018A8 RID: 6312
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001217")]
		public float m_CharaOffsetY;

		// Token: 0x040018A9 RID: 6313
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001218")]
		public float m_CharaScl;

		// Token: 0x040018AA RID: 6314
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001219")]
		public string m_BackGroundName;

		// Token: 0x040018AB RID: 6315
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400121A")]
		public string[] m_VoiceCueNames;

		// Token: 0x040018AC RID: 6316
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400121B")]
		public int[] m_HiddenVoiceCharaIDs;

		// Token: 0x040018AD RID: 6317
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400121C")]
		public string[] m_HiddenVoiceCues;
	}
}
