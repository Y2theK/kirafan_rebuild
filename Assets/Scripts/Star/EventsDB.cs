﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020004F0 RID: 1264
	[Token(Token = "0x20003E6")]
	[StructLayout(3)]
	public class EventsDB : ScriptableObject
	{
		// Token: 0x06001500 RID: 5376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60013B5")]
		[Address(RVA = "0x1011E4368", Offset = "0x11E4368", VA = "0x1011E4368")]
		public EventsDB()
		{
		}

		// Token: 0x040018B4 RID: 6324
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001223")]
		public EventsDB_Param[] m_Params;
	}
}
