﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020004EF RID: 1263
	[Token(Token = "0x20003E5")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct EventsDB_Param
	{
		// Token: 0x040018AF RID: 6319
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400121E")]
		public int m_EventType;

		// Token: 0x040018B0 RID: 6320
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400121F")]
		public string m_EventName;

		// Token: 0x040018B1 RID: 6321
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001220")]
		public string m_Url;

		// Token: 0x040018B2 RID: 6322
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001221")]
		public string m_MovieName;

		// Token: 0x040018B3 RID: 6323
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001222")]
		public long m_PointEventID;
	}
}
