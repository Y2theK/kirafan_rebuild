﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200065B RID: 1627
	[Token(Token = "0x2000540")]
	[StructLayout(3)]
	public class EvolutionEffectHandler : MonoBehaviour
	{
		// Token: 0x1700020E RID: 526
		// (get) Token: 0x060017CD RID: 6093 RVA: 0x0000AE78 File Offset: 0x00009078
		[Token(Token = "0x170001F7")]
		public EvolutionEffectHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x6001670")]
			[Address(RVA = "0x1011E44DC", Offset = "0x11E44DC", VA = "0x1011E44DC")]
			get
			{
				return EvolutionEffectHandler.eAnimType.Effect;
			}
		}

		// Token: 0x060017CE RID: 6094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001671")]
		[Address(RVA = "0x1011E44E4", Offset = "0x11E44E4", VA = "0x1011E44E4")]
		private void Awake()
		{
		}

		// Token: 0x060017CF RID: 6095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001672")]
		[Address(RVA = "0x1011E4718", Offset = "0x11E4718", VA = "0x1011E4718")]
		private void OnDestroy()
		{
		}

		// Token: 0x060017D0 RID: 6096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001673")]
		[Address(RVA = "0x1011E471C", Offset = "0x11E471C", VA = "0x1011E471C")]
		public void Destroy()
		{
		}

		// Token: 0x060017D1 RID: 6097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001674")]
		[Address(RVA = "0x1011E47B0", Offset = "0x11E47B0", VA = "0x1011E47B0")]
		private void Update()
		{
		}

		// Token: 0x060017D2 RID: 6098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001675")]
		[Address(RVA = "0x1011E4AC0", Offset = "0x11E4AC0", VA = "0x1011E4AC0")]
		public void ApplyTexture(Texture beforeCharaIllust, Texture afterCharaIllust)
		{
		}

		// Token: 0x060017D3 RID: 6099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001676")]
		[Address(RVA = "0x1011E496C", Offset = "0x11E496C", VA = "0x1011E496C")]
		public void Play(EvolutionEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x060017D4 RID: 6100 RVA: 0x0000AE90 File Offset: 0x00009090
		[Token(Token = "0x6001677")]
		[Address(RVA = "0x1011E4890", Offset = "0x11E4890", VA = "0x1011E4890")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x060017D5 RID: 6101 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001678")]
		[Address(RVA = "0x1011E4B90", Offset = "0x11E4B90", VA = "0x1011E4B90")]
		private string GetAnimPlayKey(EvolutionEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x060017D6 RID: 6102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001679")]
		[Address(RVA = "0x1011E4BE0", Offset = "0x11E4BE0", VA = "0x1011E4BE0")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x060017D7 RID: 6103 RVA: 0x0000AEA8 File Offset: 0x000090A8
		[Token(Token = "0x600167A")]
		[Address(RVA = "0x1011E4C8C", Offset = "0x11E4C8C", VA = "0x1011E4C8C")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x060017D8 RID: 6104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600167B")]
		[Address(RVA = "0x1011E4D10", Offset = "0x11E4D10", VA = "0x1011E4D10")]
		private void OnReplaceCard()
		{
		}

		// Token: 0x060017D9 RID: 6105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600167C")]
		[Address(RVA = "0x1011E4D30", Offset = "0x11E4D30", VA = "0x1011E4D30")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x060017DA RID: 6106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600167D")]
		[Address(RVA = "0x1011E4DB4", Offset = "0x11E4DB4", VA = "0x1011E4DB4")]
		public EvolutionEffectHandler()
		{
		}

		// Token: 0x040026AB RID: 9899
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001FEB")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x040026AC RID: 9900
		[Token(Token = "0x4001FEC")]
		private const string OBJ_CHARA_ILLUST_BEFORE = "before";

		// Token: 0x040026AD RID: 9901
		[Token(Token = "0x4001FED")]
		private const string OBJ_CHARA_ILLUST_AFTER = "after";

		// Token: 0x040026AE RID: 9902
		[Token(Token = "0x4001FEE")]
		private const string OBJ_CHARA_ILLUST_AFTER_BLUR = "after_blur";

		// Token: 0x040026AF RID: 9903
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001FEF")]
		private readonly float UI_START_FRAME;

		// Token: 0x040026B0 RID: 9904
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001FF0")]
		public Transform m_Transform;

		// Token: 0x040026B1 RID: 9905
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001FF1")]
		public Animation m_Anim;

		// Token: 0x040026B2 RID: 9906
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001FF2")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040026B3 RID: 9907
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001FF3")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x040026B4 RID: 9908
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001FF4")]
		public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x040026B5 RID: 9909
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4001FF5")]
		public MsbHandler m_MsbHndl;

		// Token: 0x040026B6 RID: 9910
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4001FF6")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x040026B7 RID: 9911
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4001FF7")]
		private EvolutionEffectHandler.eAnimType m_CurrentAnimType;

		// Token: 0x040026B8 RID: 9912
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4001FF8")]
		private float m_UIStart;

		// Token: 0x040026B9 RID: 9913
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4001FF9")]
		private bool m_IsUIFade;

		// Token: 0x040026BA RID: 9914
		[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
		[Token(Token = "0x4001FFA")]
		private bool m_IsDoneReplaceCard;

		// Token: 0x040026BB RID: 9915
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4001FFB")]
		public Action<bool> DispUICallback;

		// Token: 0x040026BC RID: 9916
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4001FFC")]
		public Action<float, float, float> UIFadeOutCallback;

		// Token: 0x040026BD RID: 9917
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4001FFD")]
		public Action UIFadeOutUpdateCallback;

		// Token: 0x040026BE RID: 9918
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4001FFE")]
		public Func<bool> UIFadeOutIsFinish;

		// Token: 0x040026BF RID: 9919
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4001FFF")]
		public Action OnReplaceCardCallback;

		// Token: 0x040026C0 RID: 9920
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002000")]
		public Action PlayVoiceCallback;

		// Token: 0x0200065C RID: 1628
		[Token(Token = "0x2000DEF")]
		public enum eAnimType
		{
			// Token: 0x040026C2 RID: 9922
			[Token(Token = "0x4005A42")]
			None = -1,
			// Token: 0x040026C3 RID: 9923
			[Token(Token = "0x4005A43")]
			Effect,
			// Token: 0x040026C4 RID: 9924
			[Token(Token = "0x4005A44")]
			Loop,
			// Token: 0x040026C5 RID: 9925
			[Token(Token = "0x4005A45")]
			Num
		}

		// Token: 0x0200065D RID: 1629
		[Token(Token = "0x2000DF0")]
		public enum eAnimEvent
		{
			// Token: 0x040026C7 RID: 9927
			[Token(Token = "0x4005A47")]
			ReplaceCard = 103
		}
	}
}
