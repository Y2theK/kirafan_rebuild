﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x0200065E RID: 1630
	[Token(Token = "0x2000541")]
	[StructLayout(3)]
	public class EvolutionEffectScene
	{
		// Token: 0x060017DB RID: 6107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600167E")]
		[Address(RVA = "0x1011E4EFC", Offset = "0x11E4EFC", VA = "0x1011E4EFC")]
		public EvolutionEffectScene(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x060017DC RID: 6108 RVA: 0x0000AEC0 File Offset: 0x000090C0
		[Token(Token = "0x600167F")]
		[Address(RVA = "0x1011E4FF0", Offset = "0x11E4FF0", VA = "0x1011E4FF0")]
		public bool Destroy()
		{
			return default(bool);
		}

		// Token: 0x060017DD RID: 6109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001680")]
		[Address(RVA = "0x1011E51C0", Offset = "0x11E51C0", VA = "0x1011E51C0")]
		public void Update()
		{
		}

		// Token: 0x060017DE RID: 6110 RVA: 0x0000AED8 File Offset: 0x000090D8
		[Token(Token = "0x6001681")]
		[Address(RVA = "0x1011E58A0", Offset = "0x11E58A0", VA = "0x1011E58A0")]
		public bool Prepare(Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x060017DF RID: 6111 RVA: 0x0000AEF0 File Offset: 0x000090F0
		[Token(Token = "0x6001682")]
		[Address(RVA = "0x1011E59DC", Offset = "0x11E59DC", VA = "0x1011E59DC")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x060017E0 RID: 6112 RVA: 0x0000AF08 File Offset: 0x00009108
		[Token(Token = "0x6001683")]
		[Address(RVA = "0x1011E59EC", Offset = "0x11E59EC", VA = "0x1011E59EC")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x060017E1 RID: 6113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001684")]
		[Address(RVA = "0x1011E59FC", Offset = "0x11E59FC", VA = "0x1011E59FC")]
		public void SetCharacterID(int before, int after)
		{
		}

		// Token: 0x060017E2 RID: 6114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001685")]
		[Address(RVA = "0x1011E51DC", Offset = "0x11E51DC", VA = "0x1011E51DC")]
		private void Update_Prepare()
		{
		}

		// Token: 0x060017E3 RID: 6115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001686")]
		[Address(RVA = "0x1011E5A04", Offset = "0x11E5A04", VA = "0x1011E5A04")]
		public void Play(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x060017E4 RID: 6116 RVA: 0x0000AF20 File Offset: 0x00009120
		[Token(Token = "0x6001687")]
		[Address(RVA = "0x1011E5AE0", Offset = "0x11E5AE0", VA = "0x1011E5AE0")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x060017E5 RID: 6117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001688")]
		[Address(RVA = "0x1011E5808", Offset = "0x11E5808", VA = "0x1011E5808")]
		private void Update_Main()
		{
		}

		// Token: 0x060017E6 RID: 6118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001689")]
		[Address(RVA = "0x1011E5AF0", Offset = "0x11E5AF0", VA = "0x1011E5AF0")]
		private void Skip()
		{
		}

		// Token: 0x060017E7 RID: 6119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600168A")]
		[Address(RVA = "0x1011E5BC4", Offset = "0x11E5BC4", VA = "0x1011E5BC4")]
		private void PlayVoice()
		{
		}

		// Token: 0x060017E8 RID: 6120 RVA: 0x0000AF38 File Offset: 0x00009138
		[Token(Token = "0x600168B")]
		[Address(RVA = "0x1011E5D64", Offset = "0x11E5D64", VA = "0x1011E5D64")]
		private eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_000;
		}

		// Token: 0x060017E9 RID: 6121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600168C")]
		[Address(RVA = "0x1011E5D6C", Offset = "0x11E5D6C", VA = "0x1011E5D6C")]
		public void DetectTouch()
		{
		}

		// Token: 0x040026C8 RID: 9928
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002001")]
		private EvolutionEffectScene.eMode m_Mode;

		// Token: 0x040026C9 RID: 9929
		[Token(Token = "0x4002002")]
		private const string RESOURCE_PATH = "mix/mix_evolution.muast";

		// Token: 0x040026CA RID: 9930
		[Token(Token = "0x4002003")]
		private const string OBJECT_PATH = "Mix_Evolution";

		// Token: 0x040026CB RID: 9931
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002004")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x040026CC RID: 9932
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002005")]
		private ABResourceLoader m_Loader;

		// Token: 0x040026CD RID: 9933
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002006")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x040026CE RID: 9934
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002007")]
		private SpriteHandler m_CharacterIllustBefore;

		// Token: 0x040026CF RID: 9935
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002008")]
		private SpriteHandler m_CharacterIllustAfter;

		// Token: 0x040026D0 RID: 9936
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002009")]
		private GameObject m_EffectGO;

		// Token: 0x040026D1 RID: 9937
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400200A")]
		private EvolutionEffectHandler m_EffectHndl;

		// Token: 0x040026D2 RID: 9938
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400200B")]
		private Transform m_ParentTransform;

		// Token: 0x040026D3 RID: 9939
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400200C")]
		private Vector3 m_Position;

		// Token: 0x040026D4 RID: 9940
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400200D")]
		private SoundSeRequest m_SeRequest;

		// Token: 0x040026D5 RID: 9941
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400200E")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x040026D6 RID: 9942
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400200F")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x040026D7 RID: 9943
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002010")]
		private EvolutionEffectScene.ePrepareStep m_PrepareStep;

		// Token: 0x040026D8 RID: 9944
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4002011")]
		private int m_CharacterIDBefore;

		// Token: 0x040026D9 RID: 9945
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002012")]
		private int m_CharacterIDAfter;

		// Token: 0x040026DA RID: 9946
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4002013")]
		private EvolutionEffectScene.eMainStep m_MainStep;

		// Token: 0x040026DB RID: 9947
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002014")]
		private bool m_DetectTouch;

		// Token: 0x040026DC RID: 9948
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002015")]
		private int m_RequestVoiceID;

		// Token: 0x0200065F RID: 1631
		[Token(Token = "0x2000DF1")]
		public enum eMode
		{
			// Token: 0x040026DE RID: 9950
			[Token(Token = "0x4005A49")]
			None = -1,
			// Token: 0x040026DF RID: 9951
			[Token(Token = "0x4005A4A")]
			Prepare,
			// Token: 0x040026E0 RID: 9952
			[Token(Token = "0x4005A4B")]
			UpdateMain,
			// Token: 0x040026E1 RID: 9953
			[Token(Token = "0x4005A4C")]
			Destroy
		}

		// Token: 0x02000660 RID: 1632
		[Token(Token = "0x2000DF2")]
		private enum ePrepareStep
		{
			// Token: 0x040026E3 RID: 9955
			[Token(Token = "0x4005A4E")]
			None = -1,
			// Token: 0x040026E4 RID: 9956
			[Token(Token = "0x4005A4F")]
			Prepare,
			// Token: 0x040026E5 RID: 9957
			[Token(Token = "0x4005A50")]
			Prepare_Wait,
			// Token: 0x040026E6 RID: 9958
			[Token(Token = "0x4005A51")]
			End,
			// Token: 0x040026E7 RID: 9959
			[Token(Token = "0x4005A52")]
			Prepare_Error
		}

		// Token: 0x02000661 RID: 1633
		[Token(Token = "0x2000DF3")]
		private enum eMainStep
		{
			// Token: 0x040026E9 RID: 9961
			[Token(Token = "0x4005A54")]
			None = -1,
			// Token: 0x040026EA RID: 9962
			[Token(Token = "0x4005A55")]
			Play,
			// Token: 0x040026EB RID: 9963
			[Token(Token = "0x4005A56")]
			Play_Wait,
			// Token: 0x040026EC RID: 9964
			[Token(Token = "0x4005A57")]
			End
		}
	}
}
