﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star {
    public class Fade : MonoBehaviour {
        [SerializeField, Range(0f, 1f)] private float m_Ratio;

        private Image m_Fade;
        private bool m_IsFading;
        private bool m_IsDoneInit;

        private void Start() {
            Init();
        }

        private void Init() {
            if (!m_IsDoneInit) {
                if (m_Fade == null) {
                    m_Fade = GetComponent<Image>();
                }
                m_IsFading = false;
                SetAlphaToObj(0f);
                m_IsDoneInit = true;
            }
        }

        private void SetAlphaToObj(float a) {
            SetColor(new Color(m_Fade.color.r, m_Fade.color.g, m_Fade.color.b, a));
        }

        public bool IsEnableRender() {
            return m_Fade.enabled;
        }

        public bool IsComplete() {
            return !m_IsFading;
        }

        public void SetFadeRatio(float ratio) {
            if (!m_IsDoneInit) {
                Init();
            }
            m_Ratio = ratio;
            SetAlphaToObj(m_Ratio);
        }

        public void SetColor(Color color) {
            m_Fade.color = color;
            m_Fade.enabled = color.a > 0f;
        }

        public Color GetColor() {
            return m_Fade.color;
        }

        public void CancelFade(float ratio) {
            StopAllCoroutines();
            SetFadeRatio(ratio);
            m_IsFading = false;
        }

        public Coroutine FadeOut(float time, Action action) {
            StopAllCoroutines();
            return StartCoroutine(FadeoutCoroutine(time, action));
        }

        public Coroutine FadeOut(float time) {
            return FadeOut(time, null);
        }

        public Coroutine FadeIn(float time, Action action) {
            StopAllCoroutines();
            return StartCoroutine(FadeinCoroutine(time, action));
        }

        public Coroutine FadeIn(float time) {
            return FadeIn(time, null);
        }

        private IEnumerator FadeinCoroutine(float time, Action action) {
            m_IsFading = true;
            float endTime = Time.timeSinceLevelLoad + time * m_Ratio;
            WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
            while (Time.timeSinceLevelLoad <= endTime) {
                if (time != 0f) {
                    m_Ratio = (endTime - Time.timeSinceLevelLoad) / time;
                } else {
                    m_Ratio = 0f;
                }
                SetAlphaToObj(m_Ratio);
                yield return endFrame;
            }
            m_IsFading = false;
            m_Ratio = 0f;
            SetAlphaToObj(m_Ratio);
            if (action != null) {
                action();
            }
        }

        private IEnumerator FadeoutCoroutine(float time, Action action) {
            m_IsFading = true;
            float endTime = Time.timeSinceLevelLoad + time * (1f - m_Ratio);
            WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
            while (Time.timeSinceLevelLoad <= endTime) {
                if (time != 0f) {
                    m_Ratio = 1f - (endTime - Time.timeSinceLevelLoad) / time;
                } else {
                    m_Ratio = 1f;
                }
                SetAlphaToObj(m_Ratio);
                yield return endFrame;
            }
            m_IsFading = false;
            m_Ratio = 1f;
            SetAlphaToObj(m_Ratio);
            if (action != null) {
                action();
            }
        }
    }
}
