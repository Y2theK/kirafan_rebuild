﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000BE0 RID: 3040
	[Token(Token = "0x2000835")]
	[StructLayout(3)]
	public class FadeCircle : MonoBehaviour
	{
		// Token: 0x0600354E RID: 13646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003084")]
		[Address(RVA = "0x1011E67C0", Offset = "0x11E67C0", VA = "0x1011E67C0")]
		private void Start()
		{
		}

		// Token: 0x0600354F RID: 13647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003085")]
		[Address(RVA = "0x1011E67C4", Offset = "0x11E67C4", VA = "0x1011E67C4")]
		private void Init()
		{
		}

		// Token: 0x06003550 RID: 13648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003086")]
		[Address(RVA = "0x1011E685C", Offset = "0x11E685C", VA = "0x1011E685C")]
		private void SetAlphaToObj(float a)
		{
		}

		// Token: 0x06003551 RID: 13649 RVA: 0x00016878 File Offset: 0x00014A78
		[Token(Token = "0x6003087")]
		[Address(RVA = "0x1011E69C8", Offset = "0x11E69C8", VA = "0x1011E69C8")]
		public bool IsEnableRender()
		{
			return default(bool);
		}

		// Token: 0x06003552 RID: 13650 RVA: 0x00016890 File Offset: 0x00014A90
		[Token(Token = "0x6003088")]
		[Address(RVA = "0x1011E69F8", Offset = "0x11E69F8", VA = "0x1011E69F8")]
		public bool IsComplete()
		{
			return default(bool);
		}

		// Token: 0x06003553 RID: 13651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003089")]
		[Address(RVA = "0x1011E6A08", Offset = "0x11E6A08", VA = "0x1011E6A08")]
		public void SetFadeRatio(float ratio)
		{
		}

		// Token: 0x06003554 RID: 13652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600308A")]
		[Address(RVA = "0x1011E6960", Offset = "0x11E6960", VA = "0x1011E6960")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06003555 RID: 13653 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600308B")]
		[Address(RVA = "0x1011E6B1C", Offset = "0x11E6B1C", VA = "0x1011E6B1C")]
		public Coroutine FadeOut(float time, Action action)
		{
			return null;
		}

		// Token: 0x06003556 RID: 13654 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600308C")]
		[Address(RVA = "0x1011E6C24", Offset = "0x11E6C24", VA = "0x1011E6C24")]
		public Coroutine FadeOut(float time)
		{
			return null;
		}

		// Token: 0x06003557 RID: 13655 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600308D")]
		[Address(RVA = "0x1011E6C2C", Offset = "0x11E6C2C", VA = "0x1011E6C2C")]
		public Coroutine FadeIn(float time, Action action)
		{
			return null;
		}

		// Token: 0x06003558 RID: 13656 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600308E")]
		[Address(RVA = "0x1011E6D34", Offset = "0x11E6D34", VA = "0x1011E6D34")]
		public Coroutine FadeIn(float time)
		{
			return null;
		}

		// Token: 0x06003559 RID: 13657 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600308F")]
		[Address(RVA = "0x1011E6C7C", Offset = "0x11E6C7C", VA = "0x1011E6C7C")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x10013A494", Offset = "0x13A494")]
		private IEnumerator FadeinCoroutine(float time, Action action)
		{
			return null;
		}

		// Token: 0x0600355A RID: 13658 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003090")]
		[Address(RVA = "0x1011E6B6C", Offset = "0x11E6B6C", VA = "0x1011E6B6C")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x10013A4F8", Offset = "0x13A4F8")]
		private IEnumerator FadeoutCoroutine(float time, Action action)
		{
			return null;
		}

		// Token: 0x0600355B RID: 13659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003091")]
		[Address(RVA = "0x1011E6D94", Offset = "0x11E6D94", VA = "0x1011E6D94")]
		public FadeCircle()
		{
		}

		// Token: 0x0400457E RID: 17790
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030F1")]
		[SerializeField]
		[Attribute(Name = "RangeAttribute", RVA = "0x1001217CC", Offset = "0x1217CC")]
		private float m_Ratio;

		// Token: 0x0400457F RID: 17791
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030F2")]
		[SerializeField]
		private Image m_FadeCircle;

		// Token: 0x04004580 RID: 17792
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40030F3")]
		private RectTransform m_FadeCircleRectTransform;

		// Token: 0x04004581 RID: 17793
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40030F4")]
		private GameObject m_FadeCircleGameObject;

		// Token: 0x04004582 RID: 17794
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40030F5")]
		[SerializeField]
		private float m_ScaleMax;

		// Token: 0x04004583 RID: 17795
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40030F6")]
		private bool m_IsFading;

		// Token: 0x04004584 RID: 17796
		[Cpp2IlInjected.FieldOffset(Offset = "0x3D")]
		[Token(Token = "0x40030F7")]
		private bool m_IsDoneInit;
	}
}
