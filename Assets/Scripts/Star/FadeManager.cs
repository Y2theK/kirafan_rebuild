﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000ABF RID: 2751
	[Token(Token = "0x2000791")]
	[StructLayout(3)]
	public class FadeManager : SingletonMonoBehaviour<FadeManager>
	{
		// Token: 0x06002F8D RID: 12173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B69")]
		[Address(RVA = "0x1011E7254", Offset = "0x11E7254", VA = "0x1011E7254")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06002F8E RID: 12174 RVA: 0x00014718 File Offset: 0x00012918
		[Token(Token = "0x6002B6A")]
		[Address(RVA = "0x1011E72B8", Offset = "0x11E72B8", VA = "0x1011E72B8")]
		public Color GetColor()
		{
			return default(Color);
		}

		// Token: 0x06002F8F RID: 12175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B6B")]
		[Address(RVA = "0x1011E7300", Offset = "0x11E7300", VA = "0x1011E7300")]
		public void CancelFade(float ratio)
		{
		}

		// Token: 0x06002F90 RID: 12176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B6C")]
		[Address(RVA = "0x1011E7350", Offset = "0x11E7350", VA = "0x1011E7350")]
		public void FadeIn(float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F91 RID: 12177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B6D")]
		[Address(RVA = "0x1011E7414", Offset = "0x11E7414", VA = "0x1011E7414")]
		public void FadeInFromCurrentColor(float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F92 RID: 12178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B6E")]
		[Address(RVA = "0x1011E7390", Offset = "0x11E7390", VA = "0x1011E7390")]
		public void FadeIn(Color color, float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F93 RID: 12179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B6F")]
		[Address(RVA = "0x1011E7424", Offset = "0x11E7424", VA = "0x1011E7424")]
		public void FadeOut(float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F94 RID: 12180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B70")]
		[Address(RVA = "0x1011E74E8", Offset = "0x11E74E8", VA = "0x1011E74E8")]
		public void FadeOutFromCurrentColor(float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F95 RID: 12181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B71")]
		[Address(RVA = "0x1011E7464", Offset = "0x11E7464", VA = "0x1011E7464")]
		public void FadeOut(Color color, float time = -1f, [Optional] Action onCompleteFunc)
		{
		}

		// Token: 0x06002F96 RID: 12182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B72")]
		[Address(RVA = "0x1011E74F8", Offset = "0x11E74F8", VA = "0x1011E74F8")]
		private void Update()
		{
		}

		// Token: 0x06002F97 RID: 12183 RVA: 0x00014730 File Offset: 0x00012930
		[Token(Token = "0x6002B73")]
		[Address(RVA = "0x1011E7544", Offset = "0x11E7544", VA = "0x1011E7544")]
		public bool IsEnableRender()
		{
			return default(bool);
		}

		// Token: 0x06002F98 RID: 12184 RVA: 0x00014748 File Offset: 0x00012948
		[Token(Token = "0x6002B74")]
		[Address(RVA = "0x1011E7570", Offset = "0x11E7570", VA = "0x1011E7570")]
		public bool IsComplete()
		{
			return default(bool);
		}

		// Token: 0x06002F99 RID: 12185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B75")]
		[Address(RVA = "0x1011E75A4", Offset = "0x11E75A4", VA = "0x1011E75A4")]
		public void SetFadeRatio(float ratio)
		{
		}

		// Token: 0x06002F9A RID: 12186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B76")]
		[Address(RVA = "0x1011E75E0", Offset = "0x11E75E0", VA = "0x1011E75E0")]
		public FadeManager()
		{
		}

		// Token: 0x04003F18 RID: 16152
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002D43")]
		[SerializeField]
		private Fade m_Fade;

		// Token: 0x04003F19 RID: 16153
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002D44")]
		[SerializeField]
		private float m_DefaultFadeOutTime;

		// Token: 0x04003F1A RID: 16154
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002D45")]
		[SerializeField]
		private float m_DefaultFadeInTime;

		// Token: 0x04003F1B RID: 16155
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002D46")]
		[SerializeField]
		private GameObject m_BackGround;

		// Token: 0x04003F1C RID: 16156
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002D47")]
		private Color m_CurrentColor;
	}
}
