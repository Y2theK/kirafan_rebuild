﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006AB RID: 1707
	// (Invoke) Token: 0x060018B5 RID: 6325
	[Token(Token = "0x200056F")]
	[StructLayout(3, Size = 8)]
	public delegate void FeedBackCharaCallBack(eCallBackType ftype, FieldObjHandleChara pbase);
}
