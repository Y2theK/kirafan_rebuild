﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006AC RID: 1708
	// (Invoke) Token: 0x060018B9 RID: 6329
	[Token(Token = "0x2000570")]
	[StructLayout(3, Size = 8)]
	public delegate void FeedBackResultCallback(eCallBackType ftype, FieldObjHandleBuild pthis, bool fup);
}
