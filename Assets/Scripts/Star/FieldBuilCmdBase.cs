﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000691 RID: 1681
	[Token(Token = "0x2000560")]
	[StructLayout(3)]
	public class FieldBuilCmdBase
	{
		// Token: 0x06001840 RID: 6208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DB")]
		[Address(RVA = "0x1011E7F3C", Offset = "0x11E7F3C", VA = "0x1011E7F3C")]
		public void CheckCallback(bool fsuccess)
		{
		}

		// Token: 0x06001841 RID: 6209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DC")]
		[Address(RVA = "0x1011E7FB0", Offset = "0x11E7FB0", VA = "0x1011E7FB0")]
		public FieldBuilCmdBase()
		{
		}

		// Token: 0x04002916 RID: 10518
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40021F3")]
		public FieldBuilCmdBase.eCmd m_Cmd;

		// Token: 0x04002917 RID: 10519
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40021F4")]
		public uint m_AccessKey;

		// Token: 0x04002918 RID: 10520
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40021F5")]
		public Action<bool> m_Callback;

		// Token: 0x02000692 RID: 1682
		[Token(Token = "0x2000E05")]
		public enum eCmd
		{
			// Token: 0x0400291A RID: 10522
			[Token(Token = "0x4005AA5")]
			Add,
			// Token: 0x0400291B RID: 10523
			[Token(Token = "0x4005AA6")]
			Move,
			// Token: 0x0400291C RID: 10524
			[Token(Token = "0x4005AA7")]
			Swap,
			// Token: 0x0400291D RID: 10525
			[Token(Token = "0x4005AA8")]
			Remove,
			// Token: 0x0400291E RID: 10526
			[Token(Token = "0x4005AA9")]
			LevelUp
		}
	}
}
