﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000693 RID: 1683
	[Token(Token = "0x2000561")]
	[StructLayout(3)]
	public class FieldBuildCmdAdd : FieldBuilCmdBase
	{
		// Token: 0x06001842 RID: 6210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DD")]
		[Address(RVA = "0x1011E7FB8", Offset = "0x11E7FB8", VA = "0x1011E7FB8")]
		public FieldBuildCmdAdd(long fmanageid, int fpoint, int fobjid)
		{
		}

		// Token: 0x0400291F RID: 10527
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40021F6")]
		public int m_BuildPoint;

		// Token: 0x04002920 RID: 10528
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40021F7")]
		public long m_ManageID;

		// Token: 0x04002921 RID: 10529
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40021F8")]
		public int m_ObjID;
	}
}
