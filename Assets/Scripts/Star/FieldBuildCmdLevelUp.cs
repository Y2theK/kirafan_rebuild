﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000695 RID: 1685
	[Token(Token = "0x2000563")]
	[StructLayout(3)]
	public class FieldBuildCmdLevelUp : FieldBuilCmdBase
	{
		// Token: 0x06001844 RID: 6212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DF")]
		[Address(RVA = "0x1011E8000", Offset = "0x11E8000", VA = "0x1011E8000")]
		public FieldBuildCmdLevelUp(long fmanageid)
		{
		}

		// Token: 0x04002924 RID: 10532
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40021FB")]
		public long m_ManageID;
	}
}
