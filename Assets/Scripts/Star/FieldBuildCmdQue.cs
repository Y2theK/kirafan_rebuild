﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000696 RID: 1686
	[Token(Token = "0x2000564")]
	[StructLayout(3)]
	public class FieldBuildCmdQue
	{
		// Token: 0x06001845 RID: 6213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E0")]
		[Address(RVA = "0x1011E8034", Offset = "0x11E8034", VA = "0x1011E8034")]
		public void AddComQue(FieldBuilCmdBase pque, [Optional] Action<bool> pcallback)
		{
		}

		// Token: 0x06001846 RID: 6214 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016E1")]
		[Address(RVA = "0x1011E80B8", Offset = "0x11E80B8", VA = "0x1011E80B8")]
		public FieldBuilCmdBase GetQue(Type ftype)
		{
			return null;
		}

		// Token: 0x06001847 RID: 6215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E2")]
		[Address(RVA = "0x1011E8204", Offset = "0x11E8204", VA = "0x1011E8204")]
		public void RemoveQue(Type ftype)
		{
		}

		// Token: 0x06001848 RID: 6216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E3")]
		[Address(RVA = "0x1011E8354", Offset = "0x11E8354", VA = "0x1011E8354")]
		public FieldBuildCmdQue()
		{
		}

		// Token: 0x04002925 RID: 10533
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40021FC")]
		private List<FieldBuilCmdBase> m_List;
	}
}
