﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000694 RID: 1684
	[Token(Token = "0x2000562")]
	[StructLayout(3)]
	public class FieldBuildCmdRemove : FieldBuilCmdBase
	{
		// Token: 0x06001843 RID: 6211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DE")]
		[Address(RVA = "0x1011E83C4", Offset = "0x11E83C4", VA = "0x1011E83C4")]
		public FieldBuildCmdRemove(long fmanageid, int fpoint)
		{
		}

		// Token: 0x04002922 RID: 10530
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40021F9")]
		public int m_BuildPoint;

		// Token: 0x04002923 RID: 10531
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40021FA")]
		public long m_ManageID;
	}
}
