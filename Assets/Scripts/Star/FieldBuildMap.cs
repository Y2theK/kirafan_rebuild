﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000697 RID: 1687
	[Token(Token = "0x2000565")]
	[StructLayout(3)]
	public class FieldBuildMap
	{
		// Token: 0x06001849 RID: 6217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E4")]
		[Address(RVA = "0x1011E8408", Offset = "0x11E8408", VA = "0x1011E8408")]
		public FieldBuildMap(FieldBuildMapReq prequest, FieldBuildCmdQue pcmdque)
		{
		}

		// Token: 0x0600184A RID: 6218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E5")]
		[Address(RVA = "0x1011E8490", Offset = "0x11E8490", VA = "0x1011E8490")]
		public void EntryCallbackBuildUp(BuildUpCallback pbuildup, BuildUpCallback pchangeup)
		{
		}

		// Token: 0x0600184B RID: 6219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E6")]
		[Address(RVA = "0x1011E8498", Offset = "0x11E8498", VA = "0x1011E8498")]
		public void PlayRequest(FieldBuildMapReq prequest, long ftimekey)
		{
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E7")]
		[Address(RVA = "0x1011E96CC", Offset = "0x11E96CC", VA = "0x1011E96CC")]
		public void PlayRequest2(FieldBuildMapReq prequest, long ftimekey)
		{
		}

		// Token: 0x0600184D RID: 6221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016E8")]
		[Address(RVA = "0x1011E9AD8", Offset = "0x11E9AD8", VA = "0x1011E9AD8")]
		public void SetUpTownData()
		{
		}

		// Token: 0x0600184E RID: 6222 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016E9")]
		[Address(RVA = "0x1011E9D14", Offset = "0x11E9D14", VA = "0x1011E9D14")]
		public static UserTownData.BuildObjectData CreateTownBuildData(int fbuildpoint, int fobjid, long fsetmngid, bool fnewup, long fsettime)
		{
			return null;
		}

		// Token: 0x0600184F RID: 6223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016EA")]
		[Address(RVA = "0x1011E9D78", Offset = "0x11E9D78", VA = "0x1011E9D78")]
		private void CallbackCompleteAddTownBuild(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016EB")]
		[Address(RVA = "0x1011E9EA0", Offset = "0x11E9EA0", VA = "0x1011E9EA0")]
		public void AddTownBuildInMakeID(int fbuildpoint, int fobjid, long fsetmngid, bool fnewup)
		{
		}

		// Token: 0x06001851 RID: 6225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016EC")]
		[Address(RVA = "0x1011EA440", Offset = "0x11EA440", VA = "0x1011EA440")]
		private void CallbackAddTownBuildIn(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016ED")]
		[Address(RVA = "0x1011EA78C", Offset = "0x11EA78C", VA = "0x1011EA78C")]
		public void ChangeTownBuildObject(int fbuildpoint, int fobjid, long fmanageid, [Optional] Action<bool> pcallback)
		{
		}

		// Token: 0x06001853 RID: 6227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016EE")]
		[Address(RVA = "0x1011EAF0C", Offset = "0x11EAF0C", VA = "0x1011EAF0C")]
		private void CallbackBuildMake(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016EF")]
		[Address(RVA = "0x1011EB0BC", Offset = "0x11EB0BC", VA = "0x1011EB0BC")]
		private void CallbackBuildRemoveChara(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001855 RID: 6229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F0")]
		[Address(RVA = "0x1011EB120", Offset = "0x11EB120", VA = "0x1011EB120")]
		private void CallbackBuildStateChg(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001856 RID: 6230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F1")]
		[Address(RVA = "0x1011EB410", Offset = "0x11EB410", VA = "0x1011EB410")]
		public void ChangeTownBuildInMakeID(int fbuildpoint, int fobjid, long fsetmngid)
		{
		}

		// Token: 0x06001857 RID: 6231 RVA: 0x0000B1A8 File Offset: 0x000093A8
		[Token(Token = "0x60016F2")]
		[Address(RVA = "0x1011EAAAC", Offset = "0x11EAAAC", VA = "0x1011EAAAC")]
		public long RemoveTownBuildObject(long fmanageid, int fbuildpoint, [Optional] Action<bool> pcallback)
		{
			return 0L;
		}

		// Token: 0x06001858 RID: 6232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F3")]
		[Address(RVA = "0x1011EB698", Offset = "0x11EB698", VA = "0x1011EB698")]
		private void CallbackBuildRemove(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001859 RID: 6233 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016F4")]
		[Address(RVA = "0x1011E8974", Offset = "0x11E8974", VA = "0x1011E8974")]
		public FieldObjHandleBuild GetBuildPointState(long fbuildmngid)
		{
			return null;
		}

		// Token: 0x0600185A RID: 6234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F5")]
		[Address(RVA = "0x1011EB8EC", Offset = "0x11EB8EC", VA = "0x1011EB8EC")]
		public void AreaSwapBuildTable(int fpointindex1, int fpointindex2, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600185B RID: 6235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F6")]
		[Address(RVA = "0x1011EC010", Offset = "0x11EC010", VA = "0x1011EC010")]
		public void AreaMoveBuild(int fpointindex1, int fpointindex2)
		{
		}

		// Token: 0x0600185C RID: 6236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F7")]
		[Address(RVA = "0x1011EC33C", Offset = "0x11EC33C", VA = "0x1011EC33C")]
		public void BuildSwapPointTable(int fpoint1, int fpoint2, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600185D RID: 6237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F8")]
		[Address(RVA = "0x1011EC824", Offset = "0x11EC824", VA = "0x1011EC824")]
		public void BuildMovePoint(int fnowpoint, int fnextpoint, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600185E RID: 6238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016F9")]
		[Address(RVA = "0x1011ECAE4", Offset = "0x11ECAE4", VA = "0x1011ECAE4")]
		public void ChangeBuildLevelUp(int fbuildpoint, long fmanageid, bool fopen, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600185F RID: 6239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016FA")]
		[Address(RVA = "0x1011ECD54", Offset = "0x11ECD54", VA = "0x1011ECD54")]
		private void CallbackLevelUpBuild(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001860 RID: 6240 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016FB")]
		[Address(RVA = "0x1011ECE8C", Offset = "0x11ECE8C", VA = "0x1011ECE8C")]
		public FieldObjHandleBuild GetFieldBuildState(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001861 RID: 6241 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60016FC")]
		[Address(RVA = "0x1011EA278", Offset = "0x11EA278", VA = "0x1011EA278")]
		public FieldObjHandleBuild GetFieldBuildPointState(int fbuildpoint)
		{
			return null;
		}

		// Token: 0x06001862 RID: 6242 RVA: 0x0000B1C0 File Offset: 0x000093C0
		[Token(Token = "0x60016FD")]
		[Address(RVA = "0x1011ECFB8", Offset = "0x11ECFB8", VA = "0x1011ECFB8")]
		public bool GetCheckBuildAreaMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x0000B1D8 File Offset: 0x000093D8
		[Token(Token = "0x60016FE")]
		[Address(RVA = "0x1011ED20C", Offset = "0x11ED20C", VA = "0x1011ED20C")]
		public bool GetFreeBufAccessMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x0000B1F0 File Offset: 0x000093F0
		[Token(Token = "0x60016FF")]
		[Address(RVA = "0x1011ED564", Offset = "0x11ED564", VA = "0x1011ED564")]
		public bool GetCheckBuildMenuMngID(long fchrmngid, int fscheduletag, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x04002926 RID: 10534
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40021FD")]
		public List<FieldObjHandleBuild> m_BuildList;

		// Token: 0x04002927 RID: 10535
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40021FE")]
		private BuildUpCallback m_BuildUpCallback;

		// Token: 0x04002928 RID: 10536
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40021FF")]
		private BuildUpCallback m_ChangeCallback;

		// Token: 0x04002929 RID: 10537
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002200")]
		private FieldBuildMapReq m_ReqManage;

		// Token: 0x0400292A RID: 10538
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002201")]
		private FieldBuildCmdQue m_ComQue;

		// Token: 0x0400292B RID: 10539
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002202")]
		private FldComPartyScript m_ReserveComScript;

		// Token: 0x02000698 RID: 1688
		[Token(Token = "0x2000E06")]
		public class CBuildStateEvent
		{
			// Token: 0x06001865 RID: 6245 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E29")]
			[Address(RVA = "0x1011EBFD4", Offset = "0x11EBFD4", VA = "0x1011EBFD4")]
			public CBuildStateEvent(FieldObjHandleBuild pbase, eCallBackType ftype)
			{
			}

			// Token: 0x0400292C RID: 10540
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AAA")]
			public FieldObjHandleBuild m_Base;

			// Token: 0x0400292D RID: 10541
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AAB")]
			public eCallBackType m_Type;
		}

		// Token: 0x02000699 RID: 1689
		[Token(Token = "0x2000E07")]
		public class ListUpCheckState
		{
			// Token: 0x06001866 RID: 6246 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E2A")]
			[Address(RVA = "0x1011ED7F0", Offset = "0x11ED7F0", VA = "0x1011ED7F0")]
			public ListUpCheckState()
			{
			}

			// Token: 0x0400292E RID: 10542
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AAC")]
			public int m_PointIndex;

			// Token: 0x0400292F RID: 10543
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005AAD")]
			public float m_Per;

			// Token: 0x04002930 RID: 10544
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AAE")]
			public bool m_FitMove;
		}
	}
}
