﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200069D RID: 1693
	[Token(Token = "0x2000568")]
	[StructLayout(3)]
	public class FieldBuildMapReq
	{
		// Token: 0x0600186A RID: 6250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001701")]
		[Address(RVA = "0x1011ED7F8", Offset = "0x11ED7F8", VA = "0x1011ED7F8")]
		public void CharaMoveBuildPoint(long fcharamngid, long foldpointmngid, FieldBuildMapReq.MoveBeforeCallback pbfcallback, FieldBuildMapReq.MoveResultCallback pafcallback)
		{
		}

		// Token: 0x0600186B RID: 6251 RVA: 0x0000B208 File Offset: 0x00009408
		[Token(Token = "0x6001702")]
		[Address(RVA = "0x1011ED8EC", Offset = "0x11ED8EC", VA = "0x1011ED8EC")]
		public bool IsRequest()
		{
			return default(bool);
		}

		// Token: 0x0600186C RID: 6252 RVA: 0x0000B220 File Offset: 0x00009420
		[Token(Token = "0x6001703")]
		[Address(RVA = "0x1011E88A4", Offset = "0x11E88A4", VA = "0x1011E88A4")]
		public int GetRequestNum()
		{
			return 0;
		}

		// Token: 0x0600186D RID: 6253 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001704")]
		[Address(RVA = "0x1011E8904", Offset = "0x11E8904", VA = "0x1011E8904")]
		public FieldBuildMapReq.CCharaMoveState GetRequest(int findex)
		{
			return null;
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001705")]
		[Address(RVA = "0x1011ED958", Offset = "0x11ED958", VA = "0x1011ED958")]
		public void ClearRequest()
		{
		}

		// Token: 0x0600186F RID: 6255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001706")]
		[Address(RVA = "0x1011ED9B8", Offset = "0x11ED9B8", VA = "0x1011ED9B8")]
		public FieldBuildMapReq()
		{
		}

		// Token: 0x0400293B RID: 10555
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400220B")]
		public List<FieldBuildMapReq.CCharaMoveState> m_StateList;

		// Token: 0x0200069E RID: 1694
		// (Invoke) Token: 0x06001871 RID: 6257
		[Token(Token = "0x2000E09")]
		public delegate void MoveResultCallback(eTownMoveState fup, long fnextpoint, long ftimekey);

		// Token: 0x0200069F RID: 1695
		// (Invoke) Token: 0x06001875 RID: 6261
		[Token(Token = "0x2000E0A")]
		public delegate bool MoveBeforeCallback(FieldBuildMap pbuild, ref TownSearchResult presult);

		// Token: 0x020006A0 RID: 1696
		[Token(Token = "0x2000E0B")]
		public class CCharaMoveState
		{
			// Token: 0x06001878 RID: 6264 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E35")]
			[Address(RVA = "0x1011ED8E4", Offset = "0x11ED8E4", VA = "0x1011ED8E4")]
			public CCharaMoveState()
			{
			}

			// Token: 0x0400293C RID: 10556
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AB1")]
			public long m_ManageID;

			// Token: 0x0400293D RID: 10557
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AB2")]
			public long m_OldPointMngID;

			// Token: 0x0400293E RID: 10558
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005AB3")]
			public FieldBuildMapReq.MoveBeforeCallback m_BfCallback;

			// Token: 0x0400293F RID: 10559
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005AB4")]
			public FieldBuildMapReq.MoveResultCallback m_AfCallBack;
		}
	}
}
