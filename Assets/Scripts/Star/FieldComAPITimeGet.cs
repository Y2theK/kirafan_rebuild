﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006CF RID: 1743
	[Token(Token = "0x200057E")]
	[StructLayout(3)]
	public class FieldComAPITimeGet : INetComHandle
	{
		// Token: 0x0600196C RID: 6508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E2")]
		[Address(RVA = "0x1011EDC74", Offset = "0x11EDC74", VA = "0x1011EDC74")]
		public FieldComAPITimeGet()
		{
		}
	}
}
