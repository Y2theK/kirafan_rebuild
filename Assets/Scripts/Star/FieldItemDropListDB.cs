﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200054B RID: 1355
	[Token(Token = "0x200043E")]
	[StructLayout(3)]
	public class FieldItemDropListDB : ScriptableObject
	{
		// Token: 0x060015D3 RID: 5587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001483")]
		[Address(RVA = "0x1011EDD10", Offset = "0x11EDD10", VA = "0x1011EDD10")]
		public FieldItemDropListDB()
		{
		}

		// Token: 0x040018BE RID: 6334
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001228")]
		public FieldItemDropListDB_Param[] m_Params;
	}
}
