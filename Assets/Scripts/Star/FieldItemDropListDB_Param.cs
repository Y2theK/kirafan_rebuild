﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200054A RID: 1354
	[Token(Token = "0x200043D")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct FieldItemDropListDB_Param
	{
		// Token: 0x040018BA RID: 6330
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001224")]
		public int m_ID;

		// Token: 0x040018BB RID: 6331
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001225")]
		public int[] m_Num;

		// Token: 0x040018BC RID: 6332
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001226")]
		public byte[] m_Category;

		// Token: 0x040018BD RID: 6333
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001227")]
		public int[] m_ObjectID;
	}
}
