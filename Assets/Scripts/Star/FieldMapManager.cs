﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020006A4 RID: 1700
	[Token(Token = "0x200056C")]
	[StructLayout(3)]
	public class FieldMapManager : MonoBehaviour
	{
		// Token: 0x06001881 RID: 6273 RVA: 0x0000B238 File Offset: 0x00009438
		[Token(Token = "0x600170F")]
		[Address(RVA = "0x1011EDD18", Offset = "0x11EDD18", VA = "0x1011EDD18")]
		public int GetCharaNum()
		{
			return 0;
		}

		// Token: 0x06001882 RID: 6274 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001710")]
		[Address(RVA = "0x1011EDD30", Offset = "0x11EDD30", VA = "0x1011EDD30")]
		public FieldObjHandleChara GetCharaAt(int idx)
		{
			return null;
		}

		// Token: 0x06001883 RID: 6275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001711")]
		[Address(RVA = "0x1011EDD88", Offset = "0x11EDD88", VA = "0x1011EDD88")]
		public static void CheckWakeUpObject(GameObject plink, [Optional] TownBuilder townbuilder)
		{
		}

		// Token: 0x06001884 RID: 6276 RVA: 0x0000B250 File Offset: 0x00009450
		[Token(Token = "0x6001712")]
		[Address(RVA = "0x1011EDF50", Offset = "0x11EDF50", VA = "0x1011EDF50")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06001885 RID: 6277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001713")]
		[Address(RVA = "0x1011EE510", Offset = "0x11EE510", VA = "0x1011EE510")]
		public static void StoreRelease()
		{
		}

		// Token: 0x06001886 RID: 6278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001714")]
		[Address(RVA = "0x1011EE5D8", Offset = "0x11EE5D8", VA = "0x1011EE5D8")]
		public static void EntryBuildCallback(BuildUpCallback pbuildup, BuildUpCallback pchangeup)
		{
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001715")]
		[Address(RVA = "0x1011EE658", Offset = "0x11EE658", VA = "0x1011EE658")]
		public static void EntryBuildState(long fmanageid, FeedBackResultCallback pcallback)
		{
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001716")]
		[Address(RVA = "0x1011EE6E8", Offset = "0x11EE6E8", VA = "0x1011EE6E8")]
		public static FieldObjHandleBuild GetBuildState(long fmanageid)
		{
			return null;
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001717")]
		[Address(RVA = "0x1011EE760", Offset = "0x11EE760", VA = "0x1011EE760")]
		public static FieldObjHandleChara GetCharaState(long fmanageid)
		{
			return null;
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001718")]
		[Address(RVA = "0x1011EE8E8", Offset = "0x11EE8E8", VA = "0x1011EE8E8")]
		public FieldBuildMap GetBuildMap()
		{
			return null;
		}

		// Token: 0x0600188B RID: 6283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001719")]
		[Address(RVA = "0x1011EDEF0", Offset = "0x11EDEF0", VA = "0x1011EDEF0")]
		private void SetUpUserDataToManageData()
		{
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600171A")]
		[Address(RVA = "0x1011EE010", Offset = "0x11EE010", VA = "0x1011EE010")]
		private void SetResToBuildData()
		{
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600171B")]
		[Address(RVA = "0x1011EE8F0", Offset = "0x11EE8F0", VA = "0x1011EE8F0")]
		public void ChkRoomInCharaManage(CharaBuildState pcallback)
		{
		}

		// Token: 0x0600188E RID: 6286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600171C")]
		[Address(RVA = "0x1011EEA0C", Offset = "0x11EEA0C", VA = "0x1011EEA0C")]
		public void InitRoomInCharaToBuild()
		{
		}

		// Token: 0x0600188F RID: 6287 RVA: 0x0000B268 File Offset: 0x00009468
		[Token(Token = "0x600171D")]
		[Address(RVA = "0x1011EFAFC", Offset = "0x11EFAFC", VA = "0x1011EFAFC")]
		private bool IsPossibleGetSchedule()
		{
			return default(bool);
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600171E")]
		[Address(RVA = "0x1011EFDB4", Offset = "0x11EFDB4", VA = "0x1011EFDB4")]
		private void UpdateSchedule()
		{
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600171F")]
		[Address(RVA = "0x1011F0340", Offset = "0x11F0340", VA = "0x1011F0340")]
		private void Update()
		{
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001720")]
		[Address(RVA = "0x1011F03AC", Offset = "0x11F03AC", VA = "0x1011F03AC")]
		public static void AddTownBuildObject(int fbuildpoint, int fobjid, long fmanageid)
		{
		}

		// Token: 0x06001893 RID: 6291 RVA: 0x0000B280 File Offset: 0x00009480
		[Token(Token = "0x6001721")]
		[Address(RVA = "0x1011F0514", Offset = "0x11F0514", VA = "0x1011F0514")]
		public static bool IsPointToBuilding(int fbuildpoint)
		{
			return default(bool);
		}

		// Token: 0x06001894 RID: 6292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001722")]
		[Address(RVA = "0x1011F05EC", Offset = "0x11F05EC", VA = "0x1011F05EC")]
		public static void ChangeTownBuildObject(int fbuildpoint, int fobjid, long fmanageid)
		{
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001723")]
		[Address(RVA = "0x1011F0728", Offset = "0x11F0728", VA = "0x1011F0728")]
		public static void ChangeTownBuildLevelWait(int fbuildpoint, long fmanageid, bool fopen, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x06001896 RID: 6294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001724")]
		[Address(RVA = "0x1011F096C", Offset = "0x11F096C", VA = "0x1011F096C")]
		public static void RemoveTownBuildObject(long fmanageid, int fbuildpoint, Action<bool> callback)
		{
		}

		// Token: 0x06001897 RID: 6295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001725")]
		[Address(RVA = "0x1011F0A90", Offset = "0x11F0A90", VA = "0x1011F0A90")]
		public static void AreaSwapBuild(int fpoint1, int fpoint2, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x06001898 RID: 6296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001726")]
		[Address(RVA = "0x1011F0B6C", Offset = "0x11F0B6C", VA = "0x1011F0B6C")]
		public static void AreaMoveBuild(int fpoint1, int fpoint2)
		{
		}

		// Token: 0x06001899 RID: 6297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001727")]
		[Address(RVA = "0x1011F0C38", Offset = "0x11F0C38", VA = "0x1011F0C38")]
		public static void BuildSwapPoint(int fpoint1, int fpoint2, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001728")]
		[Address(RVA = "0x1011F0CF4", Offset = "0x11F0CF4", VA = "0x1011F0CF4")]
		public static void BuildMovePoint(int fnowpoint, int fnextpoint, IFldNetComModule.CallBack callback)
		{
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001729")]
		[Address(RVA = "0x1011F0DB0", Offset = "0x11F0DB0", VA = "0x1011F0DB0")]
		public void SetInitBuildPoint(long fcharamngid, long fbuildmngid)
		{
		}

		// Token: 0x0600189C RID: 6300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600172A")]
		[Address(RVA = "0x1011F0E0C", Offset = "0x11F0E0C", VA = "0x1011F0E0C")]
		public static void RefreshManageChara(CharaBuildState pcallback)
		{
		}

		// Token: 0x0600189D RID: 6301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600172B")]
		[Address(RVA = "0x1011F0E74", Offset = "0x11F0E74", VA = "0x1011F0E74")]
		public static void ActiveBuildObject(long fmanageid, bool fgemup = false, [Optional] Action<bool> pcallback)
		{
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600172C")]
		[Address(RVA = "0x1011F1058", Offset = "0x11F1058", VA = "0x1011F1058")]
		private void CallbackLevelUpCom(IFldNetComModule pmodule)
		{
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600172D")]
		[Address(RVA = "0x1011EE7C8", Offset = "0x11EE7C8", VA = "0x1011EE7C8")]
		public FieldObjHandleChara GetFieldCharaState(long fmanageid)
		{
			return null;
		}

		// Token: 0x060018A0 RID: 6304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600172E")]
		[Address(RVA = "0x1011EF398", Offset = "0x11EF398", VA = "0x1011EF398")]
		public void AddFldCharacter(long fmanageid, int froomid, CharaBuildState pcallback, bool fdebug = false)
		{
		}

		// Token: 0x060018A1 RID: 6305 RVA: 0x0000B298 File Offset: 0x00009498
		[Token(Token = "0x600172F")]
		[Address(RVA = "0x1011EF680", Offset = "0x11EF680", VA = "0x1011EF680")]
		public bool CheckChangeRoomChara(long fmanageid, int froomid, CharaBuildState pcallback, bool fdebug = false)
		{
			return default(bool);
		}

		// Token: 0x060018A2 RID: 6306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001730")]
		[Address(RVA = "0x1011EF1B0", Offset = "0x11EF1B0", VA = "0x1011EF1B0")]
		private void ClearSearchFlag()
		{
		}

		// Token: 0x060018A3 RID: 6307 RVA: 0x0000B2B0 File Offset: 0x000094B0
		[Token(Token = "0x6001731")]
		[Address(RVA = "0x1011EF274", Offset = "0x11EF274", VA = "0x1011EF274")]
		private bool SearchChara(long fid)
		{
			return default(bool);
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x0000B2C8 File Offset: 0x000094C8
		[Token(Token = "0x6001732")]
		[Address(RVA = "0x1011EF818", Offset = "0x11EF818", VA = "0x1011EF818")]
		private bool DeleteCharaCheck(CharaBuildState pcallback)
		{
			return default(bool);
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001733")]
		[Address(RVA = "0x1011F1320", Offset = "0x11F1320", VA = "0x1011F1320")]
		public static ScheduleUIDataPack GetCharaManageIDToScheduleUIData(long fmanageid)
		{
			return null;
		}

		// Token: 0x060018A6 RID: 6310 RVA: 0x0000B2E0 File Offset: 0x000094E0
		[Token(Token = "0x6001734")]
		[Address(RVA = "0x1011F18C4", Offset = "0x11F18C4", VA = "0x1011F18C4")]
		public static int GetFieldCharaChangeRemainTimeSec()
		{
			return 0;
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001735")]
		[Address(RVA = "0x1011F19F8", Offset = "0x11F19F8", VA = "0x1011F19F8")]
		public static void BuildUpOptionCode(int fobjid, int flevel)
		{
		}

		// Token: 0x060018A8 RID: 6312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001736")]
		[Address(RVA = "0x1011F07EC", Offset = "0x11F07EC", VA = "0x1011F07EC")]
		private void CheckBuildToCharaSchedule(bool fsuccess = true)
		{
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001737")]
		[Address(RVA = "0x1011F1BE0", Offset = "0x11F1BE0", VA = "0x1011F1BE0")]
		public void SetEnableInputTownUI(bool flg)
		{
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001738")]
		[Address(RVA = "0x1011F1CD4", Offset = "0x11F1CD4", VA = "0x1011F1CD4")]
		public FieldMapManager()
		{
		}

		// Token: 0x04002944 RID: 10564
		[Token(Token = "0x4002210")]
		public static FieldMapManager Inst;

		// Token: 0x04002945 RID: 10565
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002211")]
		private FieldBuildMapReq m_ReqManage;

		// Token: 0x04002946 RID: 10566
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002212")]
		private FieldObjHandleChara[] m_CharaTable;

		// Token: 0x04002947 RID: 10567
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002213")]
		private FieldBuildMap m_BuildMap;

		// Token: 0x04002948 RID: 10568
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002214")]
		private FieldBuildCmdQue m_ComQue;

		// Token: 0x04002949 RID: 10569
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002215")]
		private bool m_InitSetUp;

		// Token: 0x0400294A RID: 10570
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002216")]
		private long m_StartTime;

		// Token: 0x0400294B RID: 10571
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002217")]
		private long m_UpdateTime;

		// Token: 0x0400294C RID: 10572
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002218")]
		private TownBuilder m_TownBuilder;

		// Token: 0x0400294D RID: 10573
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002219")]
		private FldComPartyScript m_ReserveComScript;

		// Token: 0x020006A5 RID: 1701
		[Token(Token = "0x2000E0C")]
		public class MemberComCheck
		{
			// Token: 0x060018AB RID: 6315 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E36")]
			[Address(RVA = "0x1011F1F40", Offset = "0x11F1F40", VA = "0x1011F1F40")]
			public MemberComCheck()
			{
			}

			// Token: 0x060018AC RID: 6316 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E37")]
			[Address(RVA = "0x1011F1FAC", Offset = "0x11F1FAC", VA = "0x1011F1FAC")]
			public void CallbackEvent(UserFieldCharaData.eUpState finout, UserFieldCharaData.RoomInCharaData pchara)
			{
			}

			// Token: 0x060018AD RID: 6317 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E38")]
			[Address(RVA = "0x1011F2154", Offset = "0x11F2154", VA = "0x1011F2154")]
			public void SendComCheck()
			{
			}

			// Token: 0x0400294E RID: 10574
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AB5")]
			private bool m_Send;

			// Token: 0x0400294F RID: 10575
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AB6")]
			private FldComPartyScript m_ComScript;
		}

		// Token: 0x020006A6 RID: 1702
		[Token(Token = "0x2000E0D")]
		public class CBuildStateEvent
		{
			// Token: 0x060018AE RID: 6318 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E39")]
			[Address(RVA = "0x1011F1F04", Offset = "0x11F1F04", VA = "0x1011F1F04")]
			public CBuildStateEvent(FieldObjHandleBuild pbase, eCallBackType ftype)
			{
			}

			// Token: 0x04002950 RID: 10576
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AB7")]
			public FieldObjHandleBuild m_Base;

			// Token: 0x04002951 RID: 10577
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AB8")]
			public eCallBackType m_Type;
		}
	}
}
