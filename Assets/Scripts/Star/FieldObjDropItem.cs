﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006AD RID: 1709
	[Token(Token = "0x2000571")]
	[StructLayout(3)]
	public class FieldObjDropItem
	{
		// Token: 0x060018BC RID: 6332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001741")]
		[Address(RVA = "0x1011F2194", Offset = "0x11F2194", VA = "0x1011F2194")]
		public void CheckDropItem(int fresno, int fadd)
		{
		}

		// Token: 0x060018BD RID: 6333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001742")]
		[Address(RVA = "0x1011F21A0", Offset = "0x11F21A0", VA = "0x1011F21A0")]
		public FieldObjDropItem()
		{
		}

		// Token: 0x04002964 RID: 10596
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002229")]
		public int m_OptionKey;

		// Token: 0x04002965 RID: 10597
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400222A")]
		public DropItemInfo m_Info;

		// Token: 0x04002966 RID: 10598
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400222B")]
		public DropItemState[] m_List;
	}
}
