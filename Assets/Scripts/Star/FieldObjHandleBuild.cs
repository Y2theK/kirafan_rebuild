﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006AE RID: 1710
	[Token(Token = "0x2000572")]
	[StructLayout(3)]
	public class FieldObjHandleBuild
	{
		// Token: 0x060018BE RID: 6334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001743")]
		[Address(RVA = "0x1011E9C24", Offset = "0x11E9C24", VA = "0x1011E9C24")]
		public FieldObjHandleBuild()
		{
		}

		// Token: 0x060018BF RID: 6335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001744")]
		[Address(RVA = "0x1011E91D0", Offset = "0x11E91D0", VA = "0x1011E91D0")]
		public void AddCharaManageID(long fcharamngid)
		{
		}

		// Token: 0x060018C0 RID: 6336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001745")]
		[Address(RVA = "0x1011E8A7C", Offset = "0x11E8A7C", VA = "0x1011E8A7C")]
		public void DelCharaManageID(long fcharamngid)
		{
		}

		// Token: 0x060018C1 RID: 6337 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001746")]
		[Address(RVA = "0x1011EB558", Offset = "0x11EB558", VA = "0x1011EB558")]
		public FldComPartyScript BackBuildToRemove(FieldBuildMapReq prequest, bool fcallevt = false)
		{
			return null;
		}

		// Token: 0x060018C2 RID: 6338 RVA: 0x0000B2F8 File Offset: 0x000094F8
		[Token(Token = "0x6001747")]
		[Address(RVA = "0x1011E9138", Offset = "0x11E9138", VA = "0x1011E9138")]
		public bool CheckBuildInChara(long fmanageid)
		{
			return default(bool);
		}

		// Token: 0x060018C3 RID: 6339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001748")]
		[Address(RVA = "0x1011E9CBC", Offset = "0x11E9CBC", VA = "0x1011E9CBC")]
		public void SetUpBuild(long fmngid, int fbuildpoint)
		{
		}

		// Token: 0x060018C4 RID: 6340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001749")]
		[Address(RVA = "0x1011F22C4", Offset = "0x11F22C4", VA = "0x1011F22C4")]
		public void SetUpSystemBuild(long fmngid, int fbuildpoint, int fresid)
		{
		}

		// Token: 0x060018C5 RID: 6341 RVA: 0x0000B310 File Offset: 0x00009510
		[Token(Token = "0x600174A")]
		[Address(RVA = "0x1011F2308", Offset = "0x11F2308", VA = "0x1011F2308")]
		public bool CalcDropItemListNum(ref DropItemInfo pstate)
		{
			return default(bool);
		}

		// Token: 0x060018C6 RID: 6342 RVA: 0x0000B328 File Offset: 0x00009528
		[Token(Token = "0x600174B")]
		[Address(RVA = "0x1011F23FC", Offset = "0x11F23FC", VA = "0x1011F23FC")]
		private bool CalcDropItemList(FieldObjDropItem plist)
		{
			return default(bool);
		}

		// Token: 0x060018C7 RID: 6343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600174C")]
		[Address(RVA = "0x1011F25E0", Offset = "0x11F25E0", VA = "0x1011F25E0")]
		public void LinkFeedBackCall(FeedBackResultCallback pback)
		{
		}

		// Token: 0x060018C8 RID: 6344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600174D")]
		[Address(RVA = "0x1011F25E8", Offset = "0x11F25E8", VA = "0x1011F25E8")]
		public void Destroy()
		{
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600174E")]
		[Address(RVA = "0x1011F25EC", Offset = "0x11F25EC", VA = "0x1011F25EC")]
		public void UpdateHandle()
		{
		}

		// Token: 0x060018CA RID: 6346 RVA: 0x0000B340 File Offset: 0x00009540
		[Token(Token = "0x600174F")]
		[Address(RVA = "0x1011F25F0", Offset = "0x11F25F0", VA = "0x1011F25F0")]
		public int GetStackQueNum()
		{
			return 0;
		}

		// Token: 0x060018CB RID: 6347 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001750")]
		[Address(RVA = "0x1011F25F8", Offset = "0x11F25F8", VA = "0x1011F25F8")]
		public FieldObjHandleBuild.StackEventQue GetIndexToQue(int findex)
		{
			return null;
		}

		// Token: 0x060018CC RID: 6348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001751")]
		[Address(RVA = "0x1011F2668", Offset = "0x11F2668", VA = "0x1011F2668")]
		public void ClearQue()
		{
		}

		// Token: 0x060018CD RID: 6349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001752")]
		[Address(RVA = "0x1011F279C", Offset = "0x11F279C", VA = "0x1011F279C")]
		private void StackQue(FieldObjHandleBuild.eQueType ftype, int fkeyid, int fnum)
		{
		}

		// Token: 0x060018CE RID: 6350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001753")]
		[Address(RVA = "0x1011F288C", Offset = "0x11F288C", VA = "0x1011F288C")]
		public void SendDropItemCom(IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x04002967 RID: 10599
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400222C")]
		public int m_BuildPoint;

		// Token: 0x04002968 RID: 10600
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400222D")]
		public int m_TownObjectID;

		// Token: 0x04002969 RID: 10601
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400222E")]
		public long[] m_StoreCharaMngID;

		// Token: 0x0400296A RID: 10602
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400222F")]
		public int m_StoreNum;

		// Token: 0x0400296B RID: 10603
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002230")]
		public int m_StoreMax;

		// Token: 0x0400296C RID: 10604
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002231")]
		public long m_UserMngID;

		// Token: 0x0400296D RID: 10605
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002232")]
		public int m_BuildInEntryMax;

		// Token: 0x0400296E RID: 10606
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002233")]
		public FeedBackResultCallback m_Callback;

		// Token: 0x0400296F RID: 10607
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002234")]
		public bool m_StateUp;

		// Token: 0x04002970 RID: 10608
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002235")]
		public List<FieldObjHandleBuild.StackEventQue> m_StackQue;

		// Token: 0x04002971 RID: 10609
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002236")]
		public short m_StackQueNum;

		// Token: 0x020006AF RID: 1711
		[Token(Token = "0x2000E10")]
		public enum eQueType
		{
			// Token: 0x04002973 RID: 10611
			[Token(Token = "0x4005ABD")]
			Event,
			// Token: 0x04002974 RID: 10612
			[Token(Token = "0x4005ABE")]
			Item,
			// Token: 0x04002975 RID: 10613
			[Token(Token = "0x4005ABF")]
			Money
		}

		// Token: 0x020006B0 RID: 1712
		[Token(Token = "0x2000E11")]
		public class StackEventQue
		{
			// Token: 0x060018CF RID: 6351 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E3F")]
			[Address(RVA = "0x1011F2884", Offset = "0x11F2884", VA = "0x1011F2884")]
			public StackEventQue()
			{
			}

			// Token: 0x04002976 RID: 10614
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AC0")]
			public FieldObjHandleBuild.eQueType m_Type;

			// Token: 0x04002977 RID: 10615
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005AC1")]
			public bool m_Erase;

			// Token: 0x04002978 RID: 10616
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AC2")]
			public int m_KeyID;

			// Token: 0x04002979 RID: 10617
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005AC3")]
			public int m_Num;
		}
	}
}
