﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006B1 RID: 1713
	[Token(Token = "0x2000573")]
	[StructLayout(3)]
	public class FieldObjHandleChara
	{
		// Token: 0x060018D0 RID: 6352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001754")]
		[Address(RVA = "0x1011F11FC", Offset = "0x11F11FC", VA = "0x1011F11FC")]
		public FieldObjHandleChara(long fmngid, int froomid)
		{
		}

		// Token: 0x060018D1 RID: 6353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001755")]
		[Address(RVA = "0x1011F1280", Offset = "0x11F1280", VA = "0x1011F1280")]
		public void SetUpChara(int faccesskey, long fmngid, long fsettimesec)
		{
		}

		// Token: 0x060018D2 RID: 6354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001756")]
		[Address(RVA = "0x1011F2DB8", Offset = "0x11F2DB8", VA = "0x1011F2DB8")]
		public void LinkFeedBackCall(FeedBackCharaCallBack pback)
		{
		}

		// Token: 0x060018D3 RID: 6355 RVA: 0x0000B358 File Offset: 0x00009558
		[Token(Token = "0x6001757")]
		[Address(RVA = "0x1011F2DC0", Offset = "0x11F2DC0", VA = "0x1011F2DC0")]
		public long GetStayBuildMngID()
		{
			return 0L;
		}

		// Token: 0x060018D4 RID: 6356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001758")]
		[Address(RVA = "0x1011F131C", Offset = "0x11F131C", VA = "0x1011F131C")]
		public void ReleaseParam()
		{
		}

		// Token: 0x060018D5 RID: 6357 RVA: 0x0000B370 File Offset: 0x00009570
		[Token(Token = "0x6001759")]
		[Address(RVA = "0x1011F2DC8", Offset = "0x11F2DC8", VA = "0x1011F2DC8")]
		public eCharaStay GetStayScene()
		{
			return eCharaStay.Room;
		}

		// Token: 0x060018D6 RID: 6358 RVA: 0x0000B388 File Offset: 0x00009588
		[Token(Token = "0x600175A")]
		[Address(RVA = "0x1011F2C68", Offset = "0x11F2C68", VA = "0x1011F2C68")]
		public bool InitBuildUpParam()
		{
			return default(bool);
		}

		// Token: 0x060018D7 RID: 6359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600175B")]
		[Address(RVA = "0x1011F2DD0", Offset = "0x11F2DD0", VA = "0x1011F2DD0")]
		public void Destroy()
		{
		}

		// Token: 0x060018D8 RID: 6360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600175C")]
		[Address(RVA = "0x1011F21A8", Offset = "0x11F21A8", VA = "0x1011F21A8")]
		public void BackRoomMove(FieldBuildMapReq preqmng)
		{
		}

		// Token: 0x060018D9 RID: 6361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600175D")]
		[Address(RVA = "0x1011F2DD4", Offset = "0x11F2DD4", VA = "0x1011F2DD4")]
		public void UpdateHandle(FieldBuildMapReq preqmng)
		{
		}

		// Token: 0x060018DA RID: 6362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600175E")]
		[Address(RVA = "0x1011F2FE0", Offset = "0x11F2FE0", VA = "0x1011F2FE0")]
		public void UpdateHandle2(FieldBuildMapReq preqmng)
		{
		}

		// Token: 0x060018DB RID: 6363 RVA: 0x0000B3A0 File Offset: 0x000095A0
		[Token(Token = "0x600175F")]
		[Address(RVA = "0x1011F31EC", Offset = "0x11F31EC", VA = "0x1011F31EC")]
		private bool CallbackRoomMove(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x060018DC RID: 6364 RVA: 0x0000B3B8 File Offset: 0x000095B8
		[Token(Token = "0x6001760")]
		[Address(RVA = "0x1011F3268", Offset = "0x11F3268", VA = "0x1011F3268")]
		private bool CalcBackContentSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x060018DD RID: 6365 RVA: 0x0000B3D0 File Offset: 0x000095D0
		[Token(Token = "0x6001761")]
		[Address(RVA = "0x1011F32B4", Offset = "0x11F32B4", VA = "0x1011F32B4")]
		private bool CalcBackTownSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x060018DE RID: 6366 RVA: 0x0000B3E8 File Offset: 0x000095E8
		[Token(Token = "0x6001762")]
		[Address(RVA = "0x1011F3300", Offset = "0x11F3300", VA = "0x1011F3300")]
		private bool CalcBackMenuSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return default(bool);
		}

		// Token: 0x060018DF RID: 6367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001763")]
		[Address(RVA = "0x1011F334C", Offset = "0x11F334C", VA = "0x1011F334C")]
		private void CallBackTownMove(eTownMoveState fupmove, long fnextpointmngid, long ftimekey)
		{
		}

		// Token: 0x060018E0 RID: 6368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001764")]
		[Address(RVA = "0x1011F34F4", Offset = "0x11F34F4", VA = "0x1011F34F4")]
		private void CallBackTownMove2(eTownMoveState fupmove, long fnextpointmngid, long ftimekey)
		{
		}

		// Token: 0x060018E1 RID: 6369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001765")]
		[Address(RVA = "0x1011F369C", Offset = "0x11F369C", VA = "0x1011F369C")]
		public void ScheduleEventFunc(UserScheduleData.eType ftype, UserScheduleData.Seg pdata)
		{
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001766")]
		[Address(RVA = "0x1011F3800", Offset = "0x11F3800", VA = "0x1011F3800")]
		private void ScheduleQueFunc(UserScheduleData.Seg pdata)
		{
		}

		// Token: 0x060018E3 RID: 6371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001767")]
		[Address(RVA = "0x1011EFA14", Offset = "0x11EFA14", VA = "0x1011EFA14")]
		public void UpScheduleParamerter()
		{
		}

		// Token: 0x060018E4 RID: 6372 RVA: 0x0000B400 File Offset: 0x00009600
		[Token(Token = "0x6001768")]
		[Address(RVA = "0x1011F3834", Offset = "0x11F3834", VA = "0x1011F3834")]
		public bool IsScheduleChangeTag()
		{
			return default(bool);
		}

		// Token: 0x060018E5 RID: 6373 RVA: 0x0000B418 File Offset: 0x00009618
		[Token(Token = "0x6001769")]
		[Address(RVA = "0x1011F3868", Offset = "0x11F3868", VA = "0x1011F3868")]
		public int GetScheduleTagItemLevel()
		{
			return 0;
		}

		// Token: 0x060018E6 RID: 6374 RVA: 0x0000B430 File Offset: 0x00009630
		[Token(Token = "0x600176A")]
		[Address(RVA = "0x1011F389C", Offset = "0x11F389C", VA = "0x1011F389C")]
		public bool IsTouchItemNew()
		{
			return default(bool);
		}

		// Token: 0x060018E7 RID: 6375 RVA: 0x0000B448 File Offset: 0x00009648
		[Token(Token = "0x600176B")]
		[Address(RVA = "0x1011F38D0", Offset = "0x11F38D0", VA = "0x1011F38D0")]
		public int GetTouchItemNewLevel()
		{
			return 0;
		}

		// Token: 0x060018E8 RID: 6376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600176C")]
		[Address(RVA = "0x1011F3904", Offset = "0x11F3904", VA = "0x1011F3904")]
		public void SetScheduleChangeTagUp()
		{
		}

		// Token: 0x060018E9 RID: 6377 RVA: 0x0000B460 File Offset: 0x00009660
		[Token(Token = "0x600176D")]
		[Address(RVA = "0x1011F3938", Offset = "0x11F3938", VA = "0x1011F3938")]
		public int GetStayToTownObjectID()
		{
			return 0;
		}

		// Token: 0x060018EA RID: 6378 RVA: 0x0000B478 File Offset: 0x00009678
		[Token(Token = "0x600176E")]
		[Address(RVA = "0x1011F3A74", Offset = "0x11F3A74", VA = "0x1011F3A74")]
		public bool CalcScheduleChangeTagDrop(IFldNetComModule.CallBack pcallback, TownBuilder builder)
		{
			return default(bool);
		}

		// Token: 0x060018EB RID: 6379 RVA: 0x0000B490 File Offset: 0x00009690
		[Token(Token = "0x600176F")]
		[Address(RVA = "0x1011F3C10", Offset = "0x11F3C10", VA = "0x1011F3C10")]
		public bool IsPopUpKeySpMode()
		{
			return default(bool);
		}

		// Token: 0x060018EC RID: 6380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001770")]
		[Address(RVA = "0x1011F3C18", Offset = "0x11F3C18", VA = "0x1011F3C18")]
		public void ClearDropItemState()
		{
		}

		// Token: 0x060018ED RID: 6381 RVA: 0x0000B4A8 File Offset: 0x000096A8
		[Token(Token = "0x6001771")]
		[Address(RVA = "0x1011F3C5C", Offset = "0x11F3C5C", VA = "0x1011F3C5C")]
		public int GetScheduleToPopMessageID()
		{
			return 0;
		}

		// Token: 0x060018EE RID: 6382 RVA: 0x0000B4C0 File Offset: 0x000096C0
		[Token(Token = "0x6001772")]
		[Address(RVA = "0x1011F3D84", Offset = "0x11F3D84", VA = "0x1011F3D84")]
		public int GetTagDropMessageID()
		{
			return 0;
		}

		// Token: 0x060018EF RID: 6383 RVA: 0x0000B4D8 File Offset: 0x000096D8
		[Token(Token = "0x6001773")]
		[Address(RVA = "0x1011F3D8C", Offset = "0x11F3D8C", VA = "0x1011F3D8C")]
		public long GetLastScheduleSettingDay()
		{
			return 0L;
		}

		// Token: 0x060018F0 RID: 6384 RVA: 0x0000B4F0 File Offset: 0x000096F0
		[Token(Token = "0x6001774")]
		[Address(RVA = "0x1011F3E2C", Offset = "0x11F3E2C", VA = "0x1011F3E2C")]
		public long GetLastPlayScheduleTagTime()
		{
			return 0L;
		}

		// Token: 0x060018F1 RID: 6385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001775")]
		[Address(RVA = "0x1011F3E44", Offset = "0x11F3E44", VA = "0x1011F3E44")]
		public void SetLastPlayScheduleTagTime(long time)
		{
		}

		// Token: 0x060018F2 RID: 6386 RVA: 0x0000B508 File Offset: 0x00009708
		[Token(Token = "0x6001776")]
		[Address(RVA = "0x1011F3E54", Offset = "0x11F3E54", VA = "0x1011F3E54")]
		public bool IsMatchIdealSchedule()
		{
			return default(bool);
		}

		// Token: 0x060018F3 RID: 6387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001777")]
		[Address(RVA = "0x1011F3F94", Offset = "0x11F3F94", VA = "0x1011F3F94")]
		public void ForceChangePlayIdx(int idx)
		{
		}

		// Token: 0x060018F4 RID: 6388 RVA: 0x0000B520 File Offset: 0x00009720
		[Token(Token = "0x6001778")]
		[Address(RVA = "0x1011F4064", Offset = "0x11F4064", VA = "0x1011F4064")]
		public bool UpdateTimeChk(long checkTimeSec)
		{
			return default(bool);
		}

		// Token: 0x060018F5 RID: 6389 RVA: 0x0000B538 File Offset: 0x00009738
		[Token(Token = "0x6001779")]
		[Address(RVA = "0x1011F0088", Offset = "0x11F0088", VA = "0x1011F0088")]
		public LogicSchedule.eType CheckSchedule(long checkTimeSec)
		{
			return LogicSchedule.eType.None;
		}

		// Token: 0x060018F6 RID: 6390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177A")]
		[Address(RVA = "0x1011F2A74", Offset = "0x11F2A74", VA = "0x1011F2A74")]
		public void InitEventTime(long fsettingtime, int fnamekey)
		{
		}

		// Token: 0x060018F7 RID: 6391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177B")]
		[Address(RVA = "0x1011F4228", Offset = "0x11F4228", VA = "0x1011F4228")]
		public void FitScheduleTime(long ftimesec)
		{
		}

		// Token: 0x060018F8 RID: 6392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177C")]
		[Address(RVA = "0x1011F43A0", Offset = "0x11F43A0", VA = "0x1011F43A0")]
		public void RefleshSchedule(long fsystemtime)
		{
		}

		// Token: 0x060018F9 RID: 6393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177D")]
		[Address(RVA = "0x1011F4428", Offset = "0x11F4428", VA = "0x1011F4428")]
		public void SetStateChangeTime(long fsystemtime)
		{
		}

		// Token: 0x060018FA RID: 6394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177E")]
		[Address(RVA = "0x1011F44C4", Offset = "0x11F44C4", VA = "0x1011F44C4")]
		public void SetScheduleTime(long fuptime)
		{
		}

		// Token: 0x060018FB RID: 6395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600177F")]
		[Address(RVA = "0x1011F4514", Offset = "0x11F4514", VA = "0x1011F4514")]
		public void PlaySceheduleTag(int ftagno, FieldBuildMapReq preqmng)
		{
		}

		// Token: 0x060018FC RID: 6396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001780")]
		[Address(RVA = "0x1011EF9C4", Offset = "0x11EF9C4", VA = "0x1011EF9C4")]
		public void PlaySceheduleTag2(FieldBuildMapReq preqmng, UserScheduleData.Seg seg)
		{
		}

		// Token: 0x060018FD RID: 6397 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001781")]
		[Address(RVA = "0x1011EF9BC", Offset = "0x11EF9BC", VA = "0x1011EF9BC")]
		public UserScheduleData.ListPack GetScheduleList()
		{
			return null;
		}

		// Token: 0x060018FE RID: 6398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001782")]
		[Address(RVA = "0x1011F4588", Offset = "0x11F4588", VA = "0x1011F4588")]
		public void SetScheduleList(UserScheduleData.Play schedule)
		{
		}

		// Token: 0x060018FF RID: 6399 RVA: 0x0000B550 File Offset: 0x00009750
		[Token(Token = "0x6001783")]
		[Address(RVA = "0x1011F1B30", Offset = "0x11F1B30", VA = "0x1011F1B30")]
		public bool IsBuildToScheduleChange()
		{
			return default(bool);
		}

		// Token: 0x06001900 RID: 6400 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001784")]
		[Address(RVA = "0x1011F14E8", Offset = "0x11F14E8", VA = "0x1011F14E8")]
		public ScheduleUIDataPack CreateScheduleUIData()
		{
			return null;
		}

		// Token: 0x0400297A RID: 10618
		[Token(Token = "0x4002237")]
		private const int POP_STACK_MAX = 6;

		// Token: 0x0400297B RID: 10619
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002238")]
		public long m_ManageID;

		// Token: 0x0400297C RID: 10620
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002239")]
		public int m_RoomID;

		// Token: 0x0400297D RID: 10621
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400223A")]
		public int m_CharaID;

		// Token: 0x0400297E RID: 10622
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400223B")]
		public long m_LinkBuildMngID;

		// Token: 0x0400297F RID: 10623
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400223C")]
		public eCharaStay m_StayType;

		// Token: 0x04002980 RID: 10624
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400223D")]
		public eCharaStay m_NextStay;

		// Token: 0x04002981 RID: 10625
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400223E")]
		public bool m_ChangeCheckPoint;

		// Token: 0x04002982 RID: 10626
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400223F")]
		public int[] m_PopUpMessageID;

		// Token: 0x04002983 RID: 10627
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002240")]
		public byte m_PopUpMsgMax;

		// Token: 0x04002984 RID: 10628
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002241")]
		public int m_SpPopUpMessageID;

		// Token: 0x04002985 RID: 10629
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002242")]
		public int m_ScheduleTagID;

		// Token: 0x04002986 RID: 10630
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002243")]
		public int m_PlayScheduleTag;

		// Token: 0x04002987 RID: 10631
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002244")]
		public int m_ScheduleTagTime;

		// Token: 0x04002988 RID: 10632
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002245")]
		public bool m_SearchActive;

		// Token: 0x04002989 RID: 10633
		[Cpp2IlInjected.FieldOffset(Offset = "0x55")]
		[Token(Token = "0x4002246")]
		public bool m_IsSimulateMode;

		// Token: 0x0400298A RID: 10634
		[Cpp2IlInjected.FieldOffset(Offset = "0x56")]
		[Token(Token = "0x4002247")]
		private bool m_SpPopMode;

		// Token: 0x0400298B RID: 10635
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002248")]
		public int m_EntryPoint;

		// Token: 0x0400298C RID: 10636
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002249")]
		public FeedBackCharaCallBack m_Callback;

		// Token: 0x0400298D RID: 10637
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400224A")]
		private UserScheduleData.ListPack m_Schedule;

		// Token: 0x0400298E RID: 10638
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400224B")]
		private int m_PlayIndex;

		// Token: 0x0400298F RID: 10639
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400224C")]
		private long m_PlayTimeSec;

		// Token: 0x04002990 RID: 10640
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400224D")]
		private long m_PlayTagTime;

		// Token: 0x04002991 RID: 10641
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400224E")]
		private long m_LastChangePlayTagTime;

		// Token: 0x04002992 RID: 10642
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400224F")]
		private long m_TestChangeStateTime;
	}
}
