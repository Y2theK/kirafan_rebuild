﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020006D0 RID: 1744
	[Token(Token = "0x200057F")]
	[StructLayout(3)]
	public class FieldPartyAPIAddAll : INetComHandle
	{
		// Token: 0x0600196D RID: 6509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E3")]
		[Address(RVA = "0x1011F45E4", Offset = "0x11F45E4", VA = "0x1011F45E4")]
		public FieldPartyAPIAddAll()
		{
		}

		// Token: 0x040029CD RID: 10701
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400225C")]
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
