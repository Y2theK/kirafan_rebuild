﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020006D8 RID: 1752
	[Token(Token = "0x2000587")]
	[StructLayout(3)]
	public class FieldPartyAPIAddAllNew : INetComHandle
	{
		// Token: 0x06001975 RID: 6517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017EB")]
		[Address(RVA = "0x1011F4684", Offset = "0x11F4684", VA = "0x1011F4684")]
		public FieldPartyAPIAddAllNew()
		{
		}

		// Token: 0x040029D7 RID: 10711
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002266")]
		public ScheduleMemberParam[] members;
	}
}
