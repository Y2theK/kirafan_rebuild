﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020006D4 RID: 1748
	[Token(Token = "0x2000583")]
	[StructLayout(3)]
	public class FieldPartyAPIChangeScheduleAll : INetComHandle
	{
		// Token: 0x06001971 RID: 6513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E7")]
		[Address(RVA = "0x1011F4724", Offset = "0x11F4724", VA = "0x1011F4724")]
		public FieldPartyAPIChangeScheduleAll()
		{
		}

		// Token: 0x040029D0 RID: 10704
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400225F")]
		public ChangeScheduleMember[] changeScheduleMembers;
	}
}
