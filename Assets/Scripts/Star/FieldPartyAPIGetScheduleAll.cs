﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006D6 RID: 1750
	[Token(Token = "0x2000585")]
	[StructLayout(3)]
	public class FieldPartyAPIGetScheduleAll : INetComHandle
	{
		// Token: 0x06001973 RID: 6515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E9")]
		[Address(RVA = "0x1011F4860", Offset = "0x11F4860", VA = "0x1011F4860")]
		public FieldPartyAPIGetScheduleAll()
		{
		}

		// Token: 0x040029D5 RID: 10709
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002264")]
		public long[] managedCharacterIds;
	}
}
