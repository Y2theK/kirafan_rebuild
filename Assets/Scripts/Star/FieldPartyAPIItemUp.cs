﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006D5 RID: 1749
	[Token(Token = "0x2000584")]
	[StructLayout(3)]
	public class FieldPartyAPIItemUp : INetComHandle
	{
		// Token: 0x06001972 RID: 6514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E8")]
		[Address(RVA = "0x1011F4900", Offset = "0x11F4900", VA = "0x1011F4900")]
		public FieldPartyAPIItemUp()
		{
		}

		// Token: 0x040029D1 RID: 10705
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002260")]
		public long managedPartyMemberId;

		// Token: 0x040029D2 RID: 10706
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002261")]
		public int touchItemResultNo;

		// Token: 0x040029D3 RID: 10707
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002262")]
		public int amount;

		// Token: 0x040029D4 RID: 10708
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002263")]
		public int flag;
	}
}
