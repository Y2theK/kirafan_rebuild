﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006D7 RID: 1751
	[Token(Token = "0x2000586")]
	[StructLayout(3)]
	public class FieldPartyAPIItemUpNew : INetComHandle
	{
		// Token: 0x06001974 RID: 6516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017EA")]
		[Address(RVA = "0x1011F49A0", Offset = "0x11F49A0", VA = "0x1011F49A0")]
		public FieldPartyAPIItemUpNew()
		{
		}

		// Token: 0x040029D6 RID: 10710
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002265")]
		public long managedPartyMemberId;
	}
}
