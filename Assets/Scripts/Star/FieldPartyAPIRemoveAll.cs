﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006D3 RID: 1747
	[Token(Token = "0x2000582")]
	[StructLayout(3)]
	public class FieldPartyAPIRemoveAll : INetComHandle
	{
		// Token: 0x06001970 RID: 6512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E6")]
		[Address(RVA = "0x1011F4A40", Offset = "0x11F4A40", VA = "0x1011F4A40")]
		public FieldPartyAPIRemoveAll()
		{
		}

		// Token: 0x040029CF RID: 10703
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400225E")]
		public long[] managedPartyMemberIds;
	}
}
