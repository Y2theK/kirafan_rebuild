﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020006D1 RID: 1745
	[Token(Token = "0x2000580")]
	[StructLayout(3)]
	public class FieldPartyAPISetAll : INetComHandle
	{
		// Token: 0x0600196E RID: 6510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E4")]
		[Address(RVA = "0x1011F4AE0", Offset = "0x11F4AE0", VA = "0x1011F4AE0")]
		public FieldPartyAPISetAll()
		{
		}

		// Token: 0x040029CE RID: 10702
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400225D")]
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
