﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200073D RID: 1853
	[Token(Token = "0x20005AC")]
	[StructLayout(3)]
	public class FlavorController
	{
		// Token: 0x06001B81 RID: 7041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001926")]
		[Address(RVA = "0x1011F4B80", Offset = "0x11F4B80", VA = "0x1011F4B80")]
		public void SetParent(Transform parent)
		{
		}

		// Token: 0x06001B82 RID: 7042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001927")]
		[Address(RVA = "0x1011F4B88", Offset = "0x11F4B88", VA = "0x1011F4B88")]
		public void Play(eFlavorConditionType type, int conditionId, Action callback, [Optional] Transform parent)
		{
		}

		// Token: 0x06001B83 RID: 7043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001928")]
		[Address(RVA = "0x1011F4CE0", Offset = "0x11F4CE0", VA = "0x1011F4CE0")]
		public void Update()
		{
		}

		// Token: 0x06001B84 RID: 7044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001929")]
		[Address(RVA = "0x1011F4D6C", Offset = "0x11F4D6C", VA = "0x1011F4D6C")]
		private void UpdatePreare()
		{
		}

		// Token: 0x06001B85 RID: 7045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600192A")]
		[Address(RVA = "0x1011F5068", Offset = "0x11F5068", VA = "0x1011F5068")]
		private void UpdatePlayFlavor()
		{
		}

		// Token: 0x06001B86 RID: 7046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600192B")]
		[Address(RVA = "0x1011F50A8", Offset = "0x11F50A8", VA = "0x1011F50A8")]
		private void UpdateFinished()
		{
		}

		// Token: 0x06001B87 RID: 7047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600192C")]
		[Address(RVA = "0x1011F559C", Offset = "0x11F559C", VA = "0x1011F559C")]
		public FlavorController()
		{
		}

		// Token: 0x04002B30 RID: 11056
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022E8")]
		private readonly string PREFAB_PATH;

		// Token: 0x04002B31 RID: 11057
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022E9")]
		private FlavorController.eState m_state;

		// Token: 0x04002B32 RID: 11058
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40022EA")]
		private FlavorController.ePrepareStep m_prepareStep;

		// Token: 0x04002B33 RID: 11059
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40022EB")]
		private ABResourceLoader m_Loader;

		// Token: 0x04002B34 RID: 11060
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40022EC")]
		private ABResourceObjectHandler m_ResObjHandler;

		// Token: 0x04002B35 RID: 11061
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40022ED")]
		private Transform m_Parent;

		// Token: 0x04002B36 RID: 11062
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40022EE")]
		private FlavorPlayer m_Player;

		// Token: 0x04002B37 RID: 11063
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40022EF")]
		private eFlavorConditionType m_Type;

		// Token: 0x04002B38 RID: 11064
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40022F0")]
		private int m_ConditionId;

		// Token: 0x04002B39 RID: 11065
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40022F1")]
		public Action m_LoadedCallback;

		// Token: 0x04002B3A RID: 11066
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40022F2")]
		public Action m_EndCallback;

		// Token: 0x0200073E RID: 1854
		[Token(Token = "0x2000E62")]
		private enum eState
		{
			// Token: 0x04002B3C RID: 11068
			[Token(Token = "0x4005BBF")]
			Stay,
			// Token: 0x04002B3D RID: 11069
			[Token(Token = "0x4005BC0")]
			Prepare,
			// Token: 0x04002B3E RID: 11070
			[Token(Token = "0x4005BC1")]
			PlayFlavor,
			// Token: 0x04002B3F RID: 11071
			[Token(Token = "0x4005BC2")]
			Finished
		}

		// Token: 0x0200073F RID: 1855
		[Token(Token = "0x2000E63")]
		private enum ePrepareStep
		{
			// Token: 0x04002B41 RID: 11073
			[Token(Token = "0x4005BC4")]
			Stay,
			// Token: 0x04002B42 RID: 11074
			[Token(Token = "0x4005BC5")]
			LoadPreafab,
			// Token: 0x04002B43 RID: 11075
			[Token(Token = "0x4005BC6")]
			LoadFlavorResource,
			// Token: 0x04002B44 RID: 11076
			[Token(Token = "0x4005BC7")]
			Loaded
		}
	}
}
