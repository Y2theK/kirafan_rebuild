﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000740 RID: 1856
	[Token(Token = "0x20005AD")]
	[StructLayout(3)]
	public class FlavorPlayer : MonoBehaviour
	{
		// Token: 0x06001B88 RID: 7048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600192D")]
		[Address(RVA = "0x1011F5618", Offset = "0x11F5618", VA = "0x1011F5618")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001B89 RID: 7049 RVA: 0x0000C498 File Offset: 0x0000A698
		[Token(Token = "0x600192E")]
		[Address(RVA = "0x1011F5504", Offset = "0x11F5504", VA = "0x1011F5504")]
		public bool IsPrepare()
		{
			return default(bool);
		}

		// Token: 0x06001B8A RID: 7050 RVA: 0x0000C4B0 File Offset: 0x0000A6B0
		[Token(Token = "0x600192F")]
		[Address(RVA = "0x1011F5514", Offset = "0x11F5514", VA = "0x1011F5514")]
		public bool IsReady2Play()
		{
			return default(bool);
		}

		// Token: 0x06001B8B RID: 7051 RVA: 0x0000C4C8 File Offset: 0x0000A6C8
		[Token(Token = "0x6001930")]
		[Address(RVA = "0x1011F558C", Offset = "0x11F558C", VA = "0x1011F558C")]
		public bool IsFinished()
		{
			return default(bool);
		}

		// Token: 0x06001B8C RID: 7052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001931")]
		[Address(RVA = "0x1011F5150", Offset = "0x11F5150", VA = "0x1011F5150")]
		public void Setup(eFlavorConditionType type, int conditionId)
		{
		}

		// Token: 0x06001B8D RID: 7053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001932")]
		[Address(RVA = "0x1011F5524", Offset = "0x11F5524", VA = "0x1011F5524")]
		public void Play()
		{
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001933")]
		[Address(RVA = "0x1011F5878", Offset = "0x11F5878", VA = "0x1011F5878")]
		private void Update()
		{
		}

		// Token: 0x06001B8F RID: 7055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001934")]
		[Address(RVA = "0x1011F5E34", Offset = "0x11F5E34", VA = "0x1011F5E34")]
		private void UpdateTouchState()
		{
		}

		// Token: 0x06001B90 RID: 7056 RVA: 0x0000C4E0 File Offset: 0x0000A6E0
		[Token(Token = "0x6001935")]
		[Address(RVA = "0x1011F606C", Offset = "0x11F606C", VA = "0x1011F606C")]
		private bool checkTouchScreen()
		{
			return default(bool);
		}

		// Token: 0x06001B91 RID: 7057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001936")]
		[Address(RVA = "0x1011F6150", Offset = "0x11F6150", VA = "0x1011F6150")]
		public FlavorPlayer()
		{
		}

		// Token: 0x04002B45 RID: 11077
		[Token(Token = "0x40022F3")]
		private const float CHARA_FADE_TIME = 0.2f;

		// Token: 0x04002B46 RID: 11078
		[Token(Token = "0x40022F4")]
		private const float HOLD_THRESHOLD = 0.5f;

		// Token: 0x04002B47 RID: 11079
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022F5")]
		[SerializeField]
		private CharaIllust m_charaIllust;

		// Token: 0x04002B48 RID: 11080
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40022F6")]
		[SerializeField]
		private Text m_txtCharaName;

		// Token: 0x04002B49 RID: 11081
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40022F7")]
		[SerializeField]
		private Text m_txtMessage;

		// Token: 0x04002B4A RID: 11082
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40022F8")]
		[SerializeField]
		private GameObject m_goNextPen;

		// Token: 0x04002B4B RID: 11083
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40022F9")]
		[SerializeField]
		private UIGroup m_FlavorTxtWindow;

		// Token: 0x04002B4C RID: 11084
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40022FA")]
		private FlavorPlayer.SimpleTextPlayer m_textPlayer;

		// Token: 0x04002B4D RID: 11085
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40022FB")]
		private int m_CharaID;

		// Token: 0x04002B4E RID: 11086
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40022FC")]
		private float m_CharaFadeTimer;

		// Token: 0x04002B4F RID: 11087
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40022FD")]
		private int m_stateTouch;

		// Token: 0x04002B50 RID: 11088
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40022FE")]
		private float m_holdTimer;

		// Token: 0x04002B51 RID: 11089
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40022FF")]
		private FlavorPlayer.eState m_state;

		// Token: 0x02000741 RID: 1857
		[Token(Token = "0x2000E64")]
		public enum eState
		{
			// Token: 0x04002B53 RID: 11091
			[Token(Token = "0x4005BC9")]
			Stay,
			// Token: 0x04002B54 RID: 11092
			[Token(Token = "0x4005BCA")]
			BGPrepare,
			// Token: 0x04002B55 RID: 11093
			[Token(Token = "0x4005BCB")]
			BGLoaded,
			// Token: 0x04002B56 RID: 11094
			[Token(Token = "0x4005BCC")]
			CharaPrepare,
			// Token: 0x04002B57 RID: 11095
			[Token(Token = "0x4005BCD")]
			Loaded,
			// Token: 0x04002B58 RID: 11096
			[Token(Token = "0x4005BCE")]
			Ready2Play,
			// Token: 0x04002B59 RID: 11097
			[Token(Token = "0x4005BCF")]
			PlayDisplayChar,
			// Token: 0x04002B5A RID: 11098
			[Token(Token = "0x4005BD0")]
			WaitFinish,
			// Token: 0x04002B5B RID: 11099
			[Token(Token = "0x4005BD1")]
			FadeOut,
			// Token: 0x04002B5C RID: 11100
			[Token(Token = "0x4005BD2")]
			Finish
		}

		// Token: 0x02000742 RID: 1858
		[Token(Token = "0x2000E65")]
		private class SimpleTextPlayer
		{
			// Token: 0x06001B92 RID: 7058 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F1C")]
			[Address(RVA = "0x1011F578C", Offset = "0x11F578C", VA = "0x1011F578C")]
			public void Setup(string message, string voice, eCharaNamedType namedType)
			{
			}

			// Token: 0x06001B93 RID: 7059 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F1D")]
			[Address(RVA = "0x1011F5798", Offset = "0x11F5798", VA = "0x1011F5798")]
			public void Play()
			{
			}

			// Token: 0x06001B94 RID: 7060 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005F1E")]
			[Address(RVA = "0x1011F5F44", Offset = "0x11F5F44", VA = "0x1011F5F44")]
			public string Update()
			{
				return null;
			}

			// Token: 0x06001B95 RID: 7061 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F1F")]
			[Address(RVA = "0x1011F607C", Offset = "0x11F607C", VA = "0x1011F607C")]
			public void ForceFinish()
			{
			}

			// Token: 0x06001B96 RID: 7062 RVA: 0x0000C4F8 File Offset: 0x0000A6F8
			[Token(Token = "0x6005F20")]
			[Address(RVA = "0x1011F605C", Offset = "0x11F605C", VA = "0x1011F605C")]
			public bool IsFinished()
			{
				return default(bool);
			}

			// Token: 0x06001B97 RID: 7063 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F21")]
			[Address(RVA = "0x1011F60B0", Offset = "0x11F60B0", VA = "0x1011F60B0")]
			public void StopVoice()
			{
			}

			// Token: 0x06001B98 RID: 7064 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F22")]
			[Address(RVA = "0x1011F61CC", Offset = "0x11F61CC", VA = "0x1011F61CC")]
			public SimpleTextPlayer()
			{
			}

			// Token: 0x04002B5D RID: 11101
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BD3")]
			public float m_TextSpd;

			// Token: 0x04002B5E RID: 11102
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005BD4")]
			private float m_Timer;

			// Token: 0x04002B5F RID: 11103
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005BD5")]
			private string m_Message;

			// Token: 0x04002B60 RID: 11104
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005BD6")]
			private string m_VoiceCueName;

			// Token: 0x04002B61 RID: 11105
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005BD7")]
			private eCharaNamedType m_namedType;

			// Token: 0x04002B62 RID: 11106
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4005BD8")]
			private int m_MsgCharNum;

			// Token: 0x04002B63 RID: 11107
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005BD9")]
			private int m_TotalCharNum;

			// Token: 0x04002B64 RID: 11108
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4005BDA")]
			private int m_voiveReqID;
		}
	}
}
