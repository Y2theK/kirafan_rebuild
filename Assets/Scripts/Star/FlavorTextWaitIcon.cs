﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000743 RID: 1859
	[Token(Token = "0x20005AE")]
	[StructLayout(3)]
	public class FlavorTextWaitIcon : MonoBehaviour
	{
		// Token: 0x06001B99 RID: 7065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001937")]
		[Address(RVA = "0x1011F61E8", Offset = "0x11F61E8", VA = "0x1011F61E8")]
		private void Update()
		{
		}

		// Token: 0x06001B9A RID: 7066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001938")]
		[Address(RVA = "0x1011F6088", Offset = "0x11F6088", VA = "0x1011F6088")]
		public void FadeIn()
		{
		}

		// Token: 0x06001B9B RID: 7067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001939")]
		[Address(RVA = "0x1011F62B0", Offset = "0x11F62B0", VA = "0x1011F62B0")]
		private void UpdateAnim()
		{
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600193A")]
		[Address(RVA = "0x1011F6410", Offset = "0x11F6410", VA = "0x1011F6410")]
		private void UpdateFade()
		{
		}

		// Token: 0x06001B9D RID: 7069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600193B")]
		[Address(RVA = "0x1011F64E8", Offset = "0x11F64E8", VA = "0x1011F64E8")]
		public FlavorTextWaitIcon()
		{
		}

		// Token: 0x04002B65 RID: 11109
		[Token(Token = "0x4002300")]
		private const float FADE_TIME = 0.1f;

		// Token: 0x04002B66 RID: 11110
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002301")]
		[SerializeField]
		private Image m_imgPen;

		// Token: 0x04002B67 RID: 11111
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002302")]
		[SerializeField]
		private Image m_imgShadow;

		// Token: 0x04002B68 RID: 11112
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002303")]
		[SerializeField]
		private AnimationCurve m_CurvePenMoveY;

		// Token: 0x04002B69 RID: 11113
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002304")]
		[SerializeField]
		private AnimationCurve m_CurveShadowScale;

		// Token: 0x04002B6A RID: 11114
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002305")]
		private float m_curveTimer;

		// Token: 0x04002B6B RID: 11115
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002306")]
		private float m_fadeTimer;
	}
}
