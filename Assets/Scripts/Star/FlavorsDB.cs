﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200054D RID: 1357
	[Token(Token = "0x2000440")]
	[StructLayout(3)]
	public class FlavorsDB : ScriptableObject
	{
		// Token: 0x060015D4 RID: 5588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001484")]
		[Address(RVA = "0x1011F6808", Offset = "0x11F6808", VA = "0x1011F6808")]
		public FlavorsDB()
		{
		}

		// Token: 0x040018C5 RID: 6341
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400122F")]
		public FlavorsDB_Param[] m_Params;
	}
}
