﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000511 RID: 1297
	[Token(Token = "0x2000407")]
	[StructLayout(3)]
	public static class FlavorsDB_Ext
	{
		// Token: 0x06001556 RID: 5462 RVA: 0x00009210 File Offset: 0x00007410
		[Token(Token = "0x600140B")]
		[Address(RVA = "0x1011F6810", Offset = "0x11F6810", VA = "0x1011F6810")]
		public static FlavorsDB_Param GetParam(this FlavorsDB self, int id)
		{
			return default(FlavorsDB_Param);
		}

		// Token: 0x06001557 RID: 5463 RVA: 0x00009228 File Offset: 0x00007428
		[Token(Token = "0x600140C")]
		[Address(RVA = "0x1011F564C", Offset = "0x11F564C", VA = "0x1011F564C")]
		public static FlavorsDB_Param GetParamFromCondition(this FlavorsDB self, eFlavorConditionType type, int conditionId)
		{
			return default(FlavorsDB_Param);
		}

		// Token: 0x06001558 RID: 5464 RVA: 0x00009240 File Offset: 0x00007440
		[Token(Token = "0x600140D")]
		[Address(RVA = "0x1011F6904", Offset = "0x11F6904", VA = "0x1011F6904")]
		public static bool Exist(this FlavorsDB self, eFlavorConditionType type, int conditionId)
		{
			return default(bool);
		}
	}
}
