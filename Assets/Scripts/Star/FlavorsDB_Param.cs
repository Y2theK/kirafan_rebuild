﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200054C RID: 1356
	[Token(Token = "0x200043F")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct FlavorsDB_Param
	{
		// Token: 0x040018BF RID: 6335
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001229")]
		public int m_Id;

		// Token: 0x040018C0 RID: 6336
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400122A")]
		public int m_ConditionType;

		// Token: 0x040018C1 RID: 6337
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400122B")]
		public int m_ConditionId;

		// Token: 0x040018C2 RID: 6338
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400122C")]
		public int m_CharaID;

		// Token: 0x040018C3 RID: 6339
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400122D")]
		public string m_VoiceCueName;

		// Token: 0x040018C4 RID: 6340
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400122E")]
		public string m_Body;
	}
}
