﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006DC RID: 1756
	[Token(Token = "0x200058A")]
	[StructLayout(3)]
	public class FldComBuildScript : IFldNetComModule
	{
		// Token: 0x0600198A RID: 6538 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017FC")]
		[Address(RVA = "0x1011F6A00", Offset = "0x11F6A00", VA = "0x1011F6A00")]
		public ComItemUpState GetUpItemState()
		{
			return null;
		}

		// Token: 0x0600198B RID: 6539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017FD")]
		[Address(RVA = "0x1011EA270", Offset = "0x11EA270", VA = "0x1011EA270")]
		public void SetMarkTime(long fbuildtime)
		{
		}

		// Token: 0x0600198C RID: 6540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017FE")]
		[Address(RVA = "0x1011EA1BC", Offset = "0x11EA1BC", VA = "0x1011EA1BC")]
		public FldComBuildScript()
		{
		}

		// Token: 0x0600198D RID: 6541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017FF")]
		[Address(RVA = "0x1011F6A08", Offset = "0x11F6A08", VA = "0x1011F6A08")]
		public void AddBuildUpParam(long fmanageid, int fobjid, int fbuildpoint)
		{
		}

		// Token: 0x0600198E RID: 6542 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001800")]
		[Address(RVA = "0x1011F6B88", Offset = "0x11F6B88", VA = "0x1011F6B88")]
		public FldComBuildScript.BuildUpParam SearchBuildUpParam(int fobjid, int fbuildpoint)
		{
			return null;
		}

		// Token: 0x0600198F RID: 6543 RVA: 0x0000B850 File Offset: 0x00009A50
		[Token(Token = "0x6001801")]
		[Address(RVA = "0x1011F6CD8", Offset = "0x11F6CD8", VA = "0x1011F6CD8")]
		public int GetBuildUpNum()
		{
			return 0;
		}

		// Token: 0x06001990 RID: 6544 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001802")]
		[Address(RVA = "0x1011F6D38", Offset = "0x11F6D38", VA = "0x1011F6D38")]
		public FldComBuildScript.BuildUpParam GetIndexToBuildUpParam(int findex)
		{
			return null;
		}

		// Token: 0x06001991 RID: 6545 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001803")]
		[Address(RVA = "0x1011F6DA8", Offset = "0x11F6DA8", VA = "0x1011F6DA8")]
		private FldComBuildScript.StackScriptCommand CreateBuildCommand(FldComBuildScript.eCmd fcalc)
		{
			return null;
		}

		// Token: 0x06001992 RID: 6546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001804")]
		[Address(RVA = "0x1011EA3A4", Offset = "0x11EA3A4", VA = "0x1011EA3A4")]
		public void StackSendCmd(FldComBuildScript.eCmd fcalc, long fkeycode, int foption, int fsubkey, [Optional] IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x06001993 RID: 6547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001805")]
		[Address(RVA = "0x1011EA408", Offset = "0x11EA408", VA = "0x1011EA408")]
		public void PlaySend()
		{
		}

		// Token: 0x06001994 RID: 6548 RVA: 0x0000B868 File Offset: 0x00009A68
		[Token(Token = "0x6001806")]
		[Address(RVA = "0x1011F725C", Offset = "0x11F725C", VA = "0x1011F725C", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x040029E2 RID: 10722
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002271")]
		public List<FldComBuildScript.BuildUpParam> m_BuildUp;

		// Token: 0x040029E3 RID: 10723
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002272")]
		public long m_TimeUp;

		// Token: 0x040029E4 RID: 10724
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002273")]
		public bool m_CreateObj;

		// Token: 0x040029E5 RID: 10725
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002274")]
		private FldComBuildScript.eStep m_Step;

		// Token: 0x040029E6 RID: 10726
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002275")]
		public List<FldComBuildScript.StackScriptCommand> m_Table;

		// Token: 0x040029E7 RID: 10727
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002276")]
		public ComItemUpState m_UpUserData;

		// Token: 0x020006DD RID: 1757
		[Token(Token = "0x2000E26")]
		public enum eCmd
		{
			// Token: 0x040029E9 RID: 10729
			[Token(Token = "0x4005AF3")]
			AddObj,
			// Token: 0x040029EA RID: 10730
			[Token(Token = "0x4005AF4")]
			AddBuildIn,
			// Token: 0x040029EB RID: 10731
			[Token(Token = "0x4005AF5")]
			ChangeBuild,
			// Token: 0x040029EC RID: 10732
			[Token(Token = "0x4005AF6")]
			CheckBuildState,
			// Token: 0x040029ED RID: 10733
			[Token(Token = "0x4005AF7")]
			LevelUpBuild,
			// Token: 0x040029EE RID: 10734
			[Token(Token = "0x4005AF8")]
			GemLevelUpBuild,
			// Token: 0x040029EF RID: 10735
			[Token(Token = "0x4005AF9")]
			LevelUpWait,
			// Token: 0x040029F0 RID: 10736
			[Token(Token = "0x4005AFA")]
			DelBuild,
			// Token: 0x040029F1 RID: 10737
			[Token(Token = "0x4005AFB")]
			ObjStateChg,
			// Token: 0x040029F2 RID: 10738
			[Token(Token = "0x4005AFC")]
			StoreObjState,
			// Token: 0x040029F3 RID: 10739
			[Token(Token = "0x4005AFD")]
			ItemUp,
			// Token: 0x040029F4 RID: 10740
			[Token(Token = "0x4005AFE")]
			CallEvt
		}

		// Token: 0x020006DE RID: 1758
		[Token(Token = "0x2000E27")]
		public enum eStep
		{
			// Token: 0x040029F6 RID: 10742
			[Token(Token = "0x4005B00")]
			Check,
			// Token: 0x040029F7 RID: 10743
			[Token(Token = "0x4005B01")]
			ComCheck,
			// Token: 0x040029F8 RID: 10744
			[Token(Token = "0x4005B02")]
			End
		}

		// Token: 0x020006DF RID: 1759
		[Token(Token = "0x2000E28")]
		public class BuildUpParam
		{
			// Token: 0x06001995 RID: 6549 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E52")]
			[Address(RVA = "0x1011F6B80", Offset = "0x11F6B80", VA = "0x1011F6B80")]
			public BuildUpParam()
			{
			}

			// Token: 0x040029F9 RID: 10745
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B03")]
			public int m_ObjID;

			// Token: 0x040029FA RID: 10746
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B04")]
			public int m_BuildPoint;

			// Token: 0x040029FB RID: 10747
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B05")]
			public long m_ManageID;
		}

		// Token: 0x020006E0 RID: 1760
		[Token(Token = "0x2000E29")]
		public class StackScriptCommand
		{
			// Token: 0x06001996 RID: 6550 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E53")]
			[Address(RVA = "0x1011FB180", Offset = "0x11FB180", VA = "0x1011FB180", Slot = "4")]
			public virtual void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x06001997 RID: 6551 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E54")]
			[Address(RVA = "0x1011FB184", Offset = "0x11FB184", VA = "0x1011FB184", Slot = "5")]
			public virtual void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x06001998 RID: 6552 RVA: 0x0000B880 File Offset: 0x00009A80
			[Token(Token = "0x6005E55")]
			[Address(RVA = "0x1011FB188", Offset = "0x11FB188", VA = "0x1011FB188", Slot = "6")]
			public virtual bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x06001999 RID: 6553 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E56")]
			[Address(RVA = "0x1011F7254", Offset = "0x11F7254", VA = "0x1011F7254")]
			public StackScriptCommand()
			{
			}

			// Token: 0x040029FC RID: 10748
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B06")]
			public FldComBuildScript.eCmd m_NextStep;

			// Token: 0x040029FD RID: 10749
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B07")]
			public IFldNetComModule.CallBack m_Callback;

			// Token: 0x040029FE RID: 10750
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B08")]
			public bool m_Active;

			// Token: 0x040029FF RID: 10751
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B09")]
			public FldComBuildScript m_IO;
		}

		// Token: 0x020006E1 RID: 1761
		[Token(Token = "0x2000E2A")]
		public class AddBuildObject : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600199A RID: 6554 RVA: 0x0000B898 File Offset: 0x00009A98
			[Token(Token = "0x6005E57")]
			[Address(RVA = "0x1011F7408", Offset = "0x11F7408", VA = "0x1011F7408", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x0600199B RID: 6555 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E58")]
			[Address(RVA = "0x1011F7410", Offset = "0x11F7410", VA = "0x1011F7410", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x0600199C RID: 6556 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E59")]
			[Address(RVA = "0x1011F74D8", Offset = "0x11F74D8", VA = "0x1011F74D8", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x0600199D RID: 6557 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5A")]
			[Address(RVA = "0x1011F7924", Offset = "0x11F7924", VA = "0x1011F7924")]
			private void CallbackBuildSet(INetComHandle phandle)
			{
			}

			// Token: 0x0600199E RID: 6558 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5B")]
			[Address(RVA = "0x1011F704C", Offset = "0x11F704C", VA = "0x1011F704C")]
			public AddBuildObject()
			{
			}

			// Token: 0x04002A00 RID: 10752
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B0A")]
			public List<long> m_ManageID;

			// Token: 0x04002A01 RID: 10753
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B0B")]
			public List<int> m_ObjID;

			// Token: 0x04002A02 RID: 10754
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B0C")]
			public List<int> m_BuildPoint;
		}

		// Token: 0x020006E2 RID: 1762
		[Token(Token = "0x2000E2B")]
		public class BuildStateChange : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x0600199F RID: 6559 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5C")]
			[Address(RVA = "0x1011FA884", Offset = "0x11FA884", VA = "0x1011FA884", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019A0 RID: 6560 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5D")]
			[Address(RVA = "0x1011FAA38", Offset = "0x11FAA38", VA = "0x1011FAA38")]
			private void CallbackBuildSet(INetComHandle phandle)
			{
			}

			// Token: 0x060019A1 RID: 6561 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5E")]
			[Address(RVA = "0x1011F7104", Offset = "0x11F7104", VA = "0x1011F7104")]
			public BuildStateChange()
			{
			}
		}

		// Token: 0x020006E3 RID: 1763
		[Token(Token = "0x2000E2C")]
		public class BuildStateCheck : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019A2 RID: 6562 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E5F")]
			[Address(RVA = "0x1011FAA40", Offset = "0x11FAA40", VA = "0x1011FAA40", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019A3 RID: 6563 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E60")]
			[Address(RVA = "0x1011FAFCC", Offset = "0x11FAFCC", VA = "0x1011FAFCC")]
			private void CallbackBuildPointSet(INetComHandle phandle)
			{
			}

			// Token: 0x060019A4 RID: 6564 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E61")]
			[Address(RVA = "0x1011FB178", Offset = "0x11FB178", VA = "0x1011FB178")]
			private void CallbackBuildSet(INetComHandle phandle)
			{
			}

			// Token: 0x060019A5 RID: 6565 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E62")]
			[Address(RVA = "0x1011F710C", Offset = "0x11F710C", VA = "0x1011F710C")]
			public BuildStateCheck()
			{
			}
		}

		// Token: 0x020006E4 RID: 1764
		[Token(Token = "0x2000E2D")]
		public class BuildPointSet : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019A6 RID: 6566 RVA: 0x0000B8B0 File Offset: 0x00009AB0
			[Token(Token = "0x6005E63")]
			[Address(RVA = "0x1011FA14C", Offset = "0x11FA14C", VA = "0x1011FA14C", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019A7 RID: 6567 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E64")]
			[Address(RVA = "0x1011FA154", Offset = "0x11FA154", VA = "0x1011FA154", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019A8 RID: 6568 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E65")]
			[Address(RVA = "0x1011FA21C", Offset = "0x11FA21C", VA = "0x1011FA21C", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019A9 RID: 6569 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E66")]
			[Address(RVA = "0x1011FA780", Offset = "0x11FA780", VA = "0x1011FA780")]
			private void CallbackObjectStateChg(INetComHandle phandle)
			{
			}

			// Token: 0x060019AA RID: 6570 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E67")]
			[Address(RVA = "0x1011F712C", Offset = "0x11F712C", VA = "0x1011F712C")]
			public BuildPointSet()
			{
			}

			// Token: 0x04002A03 RID: 10755
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B0D")]
			public List<long> m_ManageID;

			// Token: 0x04002A04 RID: 10756
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B0E")]
			public List<int> m_ObjID;

			// Token: 0x04002A05 RID: 10757
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B0F")]
			public List<int> m_BuildPoint;
		}

		// Token: 0x020006E5 RID: 1765
		[Token(Token = "0x2000E2E")]
		public class BuildLevelUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019AB RID: 6571 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E68")]
			[Address(RVA = "0x1011F8B88", Offset = "0x11F8B88", VA = "0x1011F8B88", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019AC RID: 6572 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E69")]
			[Address(RVA = "0x1011F8B94", Offset = "0x11F8B94", VA = "0x1011F8B94", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019AD RID: 6573 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6A")]
			[Address(RVA = "0x1011F8E38", Offset = "0x11F8E38", VA = "0x1011F8E38")]
			private void CallbackBuildLevelUp(INetComHandle phandle)
			{
			}

			// Token: 0x060019AE RID: 6574 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6B")]
			[Address(RVA = "0x1011F8E80", Offset = "0x11F8E80", VA = "0x1011F8E80")]
			private void CallbackBuildLevelWait(INetComHandle phandle)
			{
			}

			// Token: 0x060019AF RID: 6575 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6C")]
			[Address(RVA = "0x1011F7114", Offset = "0x11F7114", VA = "0x1011F7114")]
			public BuildLevelUp()
			{
			}

			// Token: 0x04002A06 RID: 10758
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B10")]
			public long m_ManageID;

			// Token: 0x04002A07 RID: 10759
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B11")]
			public int m_OptionCode;

			// Token: 0x04002A08 RID: 10760
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4005B12")]
			private int m_UpObjID;

			// Token: 0x04002A09 RID: 10761
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B13")]
			private int m_UpBuildPoint;
		}

		// Token: 0x020006E6 RID: 1766
		[Token(Token = "0x2000E2F")]
		public class BuildGemLevelUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019B0 RID: 6576 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6D")]
			[Address(RVA = "0x1011F83E4", Offset = "0x11F83E4", VA = "0x1011F83E4", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019B1 RID: 6577 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6E")]
			[Address(RVA = "0x1011F83F0", Offset = "0x11F83F0", VA = "0x1011F83F0", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019B2 RID: 6578 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E6F")]
			[Address(RVA = "0x1011F85B4", Offset = "0x11F85B4", VA = "0x1011F85B4")]
			private void CallbackBuildGemLevelUp(INetComHandle phandle)
			{
			}

			// Token: 0x060019B3 RID: 6579 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E70")]
			[Address(RVA = "0x1011F711C", Offset = "0x11F711C", VA = "0x1011F711C")]
			public BuildGemLevelUp()
			{
			}

			// Token: 0x04002A0A RID: 10762
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B14")]
			public long m_ManageID;

			// Token: 0x04002A0B RID: 10763
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B15")]
			public int m_OptionCode;
		}

		// Token: 0x020006E7 RID: 1767
		[Token(Token = "0x2000E30")]
		public class BuildLevelUpWait : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019B4 RID: 6580 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E71")]
			[Address(RVA = "0x1011F975C", Offset = "0x11F975C", VA = "0x1011F975C", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019B5 RID: 6581 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E72")]
			[Address(RVA = "0x1011F9768", Offset = "0x11F9768", VA = "0x1011F9768", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019B6 RID: 6582 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E73")]
			[Address(RVA = "0x1011F990C", Offset = "0x11F990C", VA = "0x1011F990C")]
			private void CallbackBuildLevelWait(INetComHandle phandle)
			{
			}

			// Token: 0x060019B7 RID: 6583 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E74")]
			[Address(RVA = "0x1011F7124", Offset = "0x11F7124", VA = "0x1011F7124")]
			public BuildLevelUpWait()
			{
			}

			// Token: 0x04002A0C RID: 10764
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B16")]
			public long m_ManageID;

			// Token: 0x04002A0D RID: 10765
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B17")]
			public int m_OptionCode;
		}

		// Token: 0x020006E8 RID: 1768
		[Token(Token = "0x2000E31")]
		public class BuildItemUp : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019B8 RID: 6584 RVA: 0x0000B8C8 File Offset: 0x00009AC8
			[Token(Token = "0x6005E75")]
			[Address(RVA = "0x1011F86D4", Offset = "0x11F86D4", VA = "0x1011F86D4", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019B9 RID: 6585 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E76")]
			[Address(RVA = "0x1011F86DC", Offset = "0x11F86DC", VA = "0x1011F86DC", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019BA RID: 6586 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E77")]
			[Address(RVA = "0x1011F87C4", Offset = "0x11F87C4", VA = "0x1011F87C4", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019BB RID: 6587 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E78")]
			[Address(RVA = "0x1011F8978", Offset = "0x11F8978", VA = "0x1011F8978")]
			private void CallbackTownObjItemUp(INetComHandle phandle)
			{
			}

			// Token: 0x060019BC RID: 6588 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E79")]
			[Address(RVA = "0x1011F71E4", Offset = "0x11F71E4", VA = "0x1011F71E4")]
			public BuildItemUp()
			{
			}

			// Token: 0x04002A0E RID: 10766
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B18")]
			private List<FldComBuildScript.BuildItemUp.UpItemKey> m_List;

			// Token: 0x04002A0F RID: 10767
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005B19")]
			private int m_SendNo;

			// Token: 0x020006E9 RID: 1769
			[Token(Token = "0x2001342")]
			public class UpItemKey
			{
				// Token: 0x060019BD RID: 6589 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600650A")]
				[Address(RVA = "0x1011F87BC", Offset = "0x11F87BC", VA = "0x1011F87BC")]
				public UpItemKey()
				{
				}

				// Token: 0x04002A10 RID: 10768
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x4007583")]
				public long m_ManageID;

				// Token: 0x04002A11 RID: 10769
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x4007584")]
				public int m_ItemNo;

				// Token: 0x04002A12 RID: 10770
				[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
				[Token(Token = "0x4007585")]
				public int m_ItemNum;
			}
		}

		// Token: 0x020006EA RID: 1770
		[Token(Token = "0x2000E32")]
		public class AddObjectCode : FldComBuildScript.StackScriptCommand
		{
			// Token: 0x060019BE RID: 6590 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E7A")]
			[Address(RVA = "0x1011F792C", Offset = "0x11F792C", VA = "0x1011F792C", Slot = "5")]
			public override void AddCode(long fkeycode, int fsuboption, int fsubkey)
			{
			}

			// Token: 0x060019BF RID: 6591 RVA: 0x0000B8E0 File Offset: 0x00009AE0
			[Token(Token = "0x6005E7B")]
			[Address(RVA = "0x1011F7A0C", Offset = "0x11F7A0C", VA = "0x1011F7A0C", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019C0 RID: 6592 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E7C")]
			[Address(RVA = "0x1011F7A14", Offset = "0x11F7A14", VA = "0x1011F7A14", Slot = "4")]
			public override void MakeSendCom(FldComBuildScript pbase)
			{
			}

			// Token: 0x060019C1 RID: 6593 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E7D")]
			[Address(RVA = "0x1011F8030", Offset = "0x11F8030", VA = "0x1011F8030")]
			private void CallbackObjectAdd(INetComHandle phandle)
			{
			}

			// Token: 0x060019C2 RID: 6594 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E7E")]
			[Address(RVA = "0x1011F6FDC", Offset = "0x11F6FDC", VA = "0x1011F6FDC")]
			public AddObjectCode()
			{
			}

			// Token: 0x04002A13 RID: 10771
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B1A")]
			public List<FldComBuildScript.AddObjectCode.AddObjState> m_List;

			// Token: 0x020006EB RID: 1771
			[Token(Token = "0x2001343")]
			public class AddObjState
			{
				// Token: 0x060019C3 RID: 6595 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600650B")]
				[Address(RVA = "0x1011F7A04", Offset = "0x11F7A04", VA = "0x1011F7A04")]
				public AddObjState()
				{
				}

				// Token: 0x04002A14 RID: 10772
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x4007586")]
				public int m_ObjID;

				// Token: 0x04002A15 RID: 10773
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x4007587")]
				public int m_OptionCode;

				// Token: 0x04002A16 RID: 10774
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x4007588")]
				public int m_BuildPoint;

				// Token: 0x04002A17 RID: 10775
				[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
				[Token(Token = "0x4007589")]
				public int m_OpenState;

				// Token: 0x04002A18 RID: 10776
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x400758A")]
				public int m_Level;
			}
		}
	}
}
