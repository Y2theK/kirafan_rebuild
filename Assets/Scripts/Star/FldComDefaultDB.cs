﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006EC RID: 1772
	[Token(Token = "0x200058B")]
	[StructLayout(3)]
	public class FldComDefaultDB : IFldNetComModule
	{
		// Token: 0x060019C4 RID: 6596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001807")]
		[Address(RVA = "0x1011FB190", Offset = "0x11FB190", VA = "0x1011FB190")]
		public FldComDefaultDB()
		{
		}

		// Token: 0x060019C5 RID: 6597 RVA: 0x0000B8F8 File Offset: 0x00009AF8
		[Token(Token = "0x6001808")]
		[Address(RVA = "0x1011FB1F4", Offset = "0x11FB1F4", VA = "0x1011FB1F4", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x0000B910 File Offset: 0x00009B10
		[Token(Token = "0x6001809")]
		[Address(RVA = "0x1011FB274", Offset = "0x11FB274", VA = "0x1011FB274")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x04002A19 RID: 10777
		[Token(Token = "0x4002277")]
		private static bool m_SetUpChk;
	}
}
