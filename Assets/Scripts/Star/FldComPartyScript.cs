﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006ED RID: 1773
	[Token(Token = "0x200058C")]
	[StructLayout(3)]
	public class FldComPartyScript : IFldNetComModule
	{
		// Token: 0x060019C7 RID: 6599 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600180A")]
		[Address(RVA = "0x1011FB2C4", Offset = "0x11FB2C4", VA = "0x1011FB2C4")]
		public ComItemUpState GetUpItemState()
		{
			return null;
		}

		// Token: 0x060019C8 RID: 6600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600180B")]
		[Address(RVA = "0x1011EEE8C", Offset = "0x11EEE8C", VA = "0x1011EEE8C")]
		public FldComPartyScript()
		{
		}

		// Token: 0x060019C9 RID: 6601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600180C")]
		[Address(RVA = "0x1011EEF18", Offset = "0x11EEF18", VA = "0x1011EEF18")]
		public void StackSendCmd(FldComPartyScript.eCmd fcalc, long fkeycode, [Optional] IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x060019CA RID: 6602 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600180D")]
		[Address(RVA = "0x1011FB2CC", Offset = "0x11FB2CC", VA = "0x1011FB2CC")]
		private FldComPartyScript.StackScriptCommand FindComCode(FldComPartyScript.eCmd fcalc, long fkeycode)
		{
			return null;
		}

		// Token: 0x060019CB RID: 6603 RVA: 0x0000B928 File Offset: 0x00009B28
		[Token(Token = "0x600180E")]
		[Address(RVA = "0x1011EFC80", Offset = "0x11EFC80", VA = "0x1011EFC80")]
		public bool IsExistComCode(FldComPartyScript.eCmd fcalc)
		{
			return default(bool);
		}

		// Token: 0x060019CC RID: 6604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600180F")]
		[Address(RVA = "0x1011EB0E8", Offset = "0x11EB0E8", VA = "0x1011EB0E8")]
		public void PlaySend()
		{
		}

		// Token: 0x060019CD RID: 6605 RVA: 0x0000B940 File Offset: 0x00009B40
		[Token(Token = "0x6001810")]
		[Address(RVA = "0x1011FB70C", Offset = "0x11FB70C", VA = "0x1011FB70C", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x04002A1A RID: 10778
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002278")]
		public List<FldComPartyScript.StackScriptCommand> m_Table;

		// Token: 0x04002A1B RID: 10779
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002279")]
		public FldComPartyScript.eStep m_Step;

		// Token: 0x04002A1C RID: 10780
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400227A")]
		public long m_SetUpManageID;

		// Token: 0x04002A1D RID: 10781
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400227B")]
		public ComItemUpState m_UpUserData;

		// Token: 0x020006EE RID: 1774
		[Token(Token = "0x2000E33")]
		public enum eCmd
		{
			// Token: 0x04002A1F RID: 10783
			[Token(Token = "0x4005B1C")]
			ChangeState,
			// Token: 0x04002A20 RID: 10784
			[Token(Token = "0x4005B1D")]
			ChangeSchedule,
			// Token: 0x04002A21 RID: 10785
			[Token(Token = "0x4005B1E")]
			AddMember,
			// Token: 0x04002A22 RID: 10786
			[Token(Token = "0x4005B1F")]
			RemoveMember,
			// Token: 0x04002A23 RID: 10787
			[Token(Token = "0x4005B20")]
			TouchItem,
			// Token: 0x04002A24 RID: 10788
			[Token(Token = "0x4005B21")]
			GetSchedule,
			// Token: 0x04002A25 RID: 10789
			[Token(Token = "0x4005B22")]
			TouchItemNew,
			// Token: 0x04002A26 RID: 10790
			[Token(Token = "0x4005B23")]
			AddMemberNew,
			// Token: 0x04002A27 RID: 10791
			[Token(Token = "0x4005B24")]
			CallEvt,
			// Token: 0x04002A28 RID: 10792
			[Token(Token = "0x4005B25")]
			Max
		}

		// Token: 0x020006EF RID: 1775
		[Token(Token = "0x2000E34")]
		public enum eStep
		{
			// Token: 0x04002A2A RID: 10794
			[Token(Token = "0x4005B27")]
			Check,
			// Token: 0x04002A2B RID: 10795
			[Token(Token = "0x4005B28")]
			ComEnd,
			// Token: 0x04002A2C RID: 10796
			[Token(Token = "0x4005B29")]
			ComCheck,
			// Token: 0x04002A2D RID: 10797
			[Token(Token = "0x4005B2A")]
			End
		}

		// Token: 0x020006F0 RID: 1776
		[Token(Token = "0x2000E35")]
		public class StackScriptCommand
		{
			// Token: 0x060019CE RID: 6606 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E7F")]
			[Address(RVA = "0x1011FDB24", Offset = "0x11FDB24", VA = "0x1011FDB24", Slot = "4")]
			public virtual void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019CF RID: 6607 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E80")]
			[Address(RVA = "0x1011FDB2C", Offset = "0x11FDB2C", VA = "0x1011FDB2C", Slot = "5")]
			public virtual void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019D0 RID: 6608 RVA: 0x0000B958 File Offset: 0x00009B58
			[Token(Token = "0x6005E81")]
			[Address(RVA = "0x1011FDB30", Offset = "0x11FDB30", VA = "0x1011FDB30", Slot = "6")]
			public virtual bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019D1 RID: 6609 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E82")]
			[Address(RVA = "0x1011FDB38", Offset = "0x11FDB38", VA = "0x1011FDB38")]
			public StackScriptCommand()
			{
			}

			// Token: 0x04002A2E RID: 10798
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B2B")]
			public FldComPartyScript.eCmd m_NextStep;

			// Token: 0x04002A2F RID: 10799
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B2C")]
			public bool m_Active;

			// Token: 0x04002A30 RID: 10800
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B2D")]
			public IFldNetComModule.CallBack m_Callback;
		}

		// Token: 0x020006F1 RID: 1777
		[Token(Token = "0x2000E36")]
		public class ChangeStateCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019D2 RID: 6610 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E83")]
			[Address(RVA = "0x1011FCB1C", Offset = "0x11FCB1C", VA = "0x1011FCB1C", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019D3 RID: 6611 RVA: 0x0000B970 File Offset: 0x00009B70
			[Token(Token = "0x6005E84")]
			[Address(RVA = "0x1011FCB8C", Offset = "0x11FCB8C", VA = "0x1011FCB8C", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019D4 RID: 6612 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E85")]
			[Address(RVA = "0x1011FCB94", Offset = "0x11FCB94", VA = "0x1011FCB94", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019D5 RID: 6613 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E86")]
			[Address(RVA = "0x1011FCDB8", Offset = "0x11FCDB8", VA = "0x1011FCDB8")]
			private void CallbackStateChg(INetComHandle phandle)
			{
			}

			// Token: 0x060019D6 RID: 6614 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E87")]
			[Address(RVA = "0x1011FB464", Offset = "0x11FB464", VA = "0x1011FB464")]
			public ChangeStateCom()
			{
			}

			// Token: 0x04002A31 RID: 10801
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B2E")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F2 RID: 1778
		[Token(Token = "0x2000E37")]
		public class AddMemberCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019D7 RID: 6615 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E88")]
			[Address(RVA = "0x1011FB8B8", Offset = "0x11FB8B8", VA = "0x1011FB8B8", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019D8 RID: 6616 RVA: 0x0000B988 File Offset: 0x00009B88
			[Token(Token = "0x6005E89")]
			[Address(RVA = "0x1011FB928", Offset = "0x11FB928", VA = "0x1011FB928", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019D9 RID: 6617 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E8A")]
			[Address(RVA = "0x1011FB930", Offset = "0x11FB930", VA = "0x1011FB930", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019DA RID: 6618 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E8B")]
			[Address(RVA = "0x1011FBB94", Offset = "0x11FBB94", VA = "0x1011FBB94")]
			private void CallbackPartyAdd(INetComHandle phandle)
			{
			}

			// Token: 0x060019DB RID: 6619 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E8C")]
			[Address(RVA = "0x1011FB544", Offset = "0x11FB544", VA = "0x1011FB544")]
			public AddMemberCom()
			{
			}

			// Token: 0x04002A32 RID: 10802
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B2F")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F3 RID: 1779
		[Token(Token = "0x2000E38")]
		public class RemoveMemberCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019DC RID: 6620 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E8D")]
			[Address(RVA = "0x1011FD834", Offset = "0x11FD834", VA = "0x1011FD834", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019DD RID: 6621 RVA: 0x0000B9A0 File Offset: 0x00009BA0
			[Token(Token = "0x6005E8E")]
			[Address(RVA = "0x1011FD8A4", Offset = "0x11FD8A4", VA = "0x1011FD8A4", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019DE RID: 6622 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E8F")]
			[Address(RVA = "0x1011FD8AC", Offset = "0x11FD8AC", VA = "0x1011FD8AC", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019DF RID: 6623 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E90")]
			[Address(RVA = "0x1011FDAA4", Offset = "0x11FDAA4", VA = "0x1011FDAA4")]
			private void CallbackPartyRemove(INetComHandle phandle)
			{
			}

			// Token: 0x060019E0 RID: 6624 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E91")]
			[Address(RVA = "0x1011FB5B4", Offset = "0x11FB5B4", VA = "0x1011FB5B4")]
			public RemoveMemberCom()
			{
			}

			// Token: 0x04002A33 RID: 10803
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B30")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F4 RID: 1780
		[Token(Token = "0x2000E39")]
		public class ChangeScheduleCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019E1 RID: 6625 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E92")]
			[Address(RVA = "0x1011FC878", Offset = "0x11FC878", VA = "0x1011FC878", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019E2 RID: 6626 RVA: 0x0000B9B8 File Offset: 0x00009BB8
			[Token(Token = "0x6005E93")]
			[Address(RVA = "0x1011FC8E8", Offset = "0x11FC8E8", VA = "0x1011FC8E8", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019E3 RID: 6627 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E94")]
			[Address(RVA = "0x1011FC8F0", Offset = "0x11FC8F0", VA = "0x1011FC8F0", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019E4 RID: 6628 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E95")]
			[Address(RVA = "0x1011FCB14", Offset = "0x11FCB14", VA = "0x1011FCB14")]
			private void CallbackScheduleChg(INetComHandle phandle)
			{
			}

			// Token: 0x060019E5 RID: 6629 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E96")]
			[Address(RVA = "0x1011FB4D4", Offset = "0x11FB4D4", VA = "0x1011FB4D4")]
			public ChangeScheduleCom()
			{
			}

			// Token: 0x04002A34 RID: 10804
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B31")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F5 RID: 1781
		[Token(Token = "0x2000E3A")]
		public class TouchItemCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019E6 RID: 6630 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E97")]
			[Address(RVA = "0x1011FDB40", Offset = "0x11FDB40", VA = "0x1011FDB40", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019E7 RID: 6631 RVA: 0x0000B9D0 File Offset: 0x00009BD0
			[Token(Token = "0x6005E98")]
			[Address(RVA = "0x1011FDB48", Offset = "0x11FDB48", VA = "0x1011FDB48", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019E8 RID: 6632 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E99")]
			[Address(RVA = "0x1011FDB50", Offset = "0x11FDB50", VA = "0x1011FDB50", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019E9 RID: 6633 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E9A")]
			[Address(RVA = "0x1011FDCD0", Offset = "0x11FDCD0", VA = "0x1011FDCD0")]
			private void CallbackPartyItemUp(INetComHandle phandle)
			{
			}

			// Token: 0x060019EA RID: 6634 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E9B")]
			[Address(RVA = "0x1011FDEA4", Offset = "0x11FDEA4", VA = "0x1011FDEA4")]
			public TouchItemCom()
			{
			}

			// Token: 0x04002A35 RID: 10805
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B32")]
			private FldComPartyScript m_parent;

			// Token: 0x04002A36 RID: 10806
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B33")]
			private long m_KeyCode;
		}

		// Token: 0x020006F6 RID: 1782
		[Token(Token = "0x2000E3B")]
		public class GetScheduleCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019EB RID: 6635 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E9C")]
			[Address(RVA = "0x1011FCDC8", Offset = "0x11FCDC8", VA = "0x1011FCDC8", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019EC RID: 6636 RVA: 0x0000B9E8 File Offset: 0x00009BE8
			[Token(Token = "0x6005E9D")]
			[Address(RVA = "0x1011FCE38", Offset = "0x11FCE38", VA = "0x1011FCE38", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019ED RID: 6637 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E9E")]
			[Address(RVA = "0x1011FCE40", Offset = "0x11FCE40", VA = "0x1011FCE40", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019EE RID: 6638 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E9F")]
			[Address(RVA = "0x1011FCFF8", Offset = "0x11FCFF8", VA = "0x1011FCFF8")]
			private void CallbackGetSchedule(INetComHandle phandle)
			{
			}

			// Token: 0x060019EF RID: 6639 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA0")]
			[Address(RVA = "0x1011FB624", Offset = "0x11FB624", VA = "0x1011FB624")]
			public GetScheduleCom()
			{
			}

			// Token: 0x04002A37 RID: 10807
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B34")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F7 RID: 1783
		[Token(Token = "0x2000E3C")]
		public class TouchItemNewCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019F0 RID: 6640 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA1")]
			[Address(RVA = "0x1011FDEAC", Offset = "0x11FDEAC", VA = "0x1011FDEAC", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019F1 RID: 6641 RVA: 0x0000BA00 File Offset: 0x00009C00
			[Token(Token = "0x6005EA2")]
			[Address(RVA = "0x1011FDEB4", Offset = "0x11FDEB4", VA = "0x1011FDEB4", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019F2 RID: 6642 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA3")]
			[Address(RVA = "0x1011FDEBC", Offset = "0x11FDEBC", VA = "0x1011FDEBC", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019F3 RID: 6643 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA4")]
			[Address(RVA = "0x1011FE064", Offset = "0x11FE064", VA = "0x1011FE064")]
			private void CallbackPartyItemUp(INetComHandle phandle)
			{
			}

			// Token: 0x060019F4 RID: 6644 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA5")]
			[Address(RVA = "0x1011FE260", Offset = "0x11FE260", VA = "0x1011FE260")]
			public TouchItemNewCom()
			{
			}

			// Token: 0x04002A38 RID: 10808
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B35")]
			private FldComPartyScript m_parent;

			// Token: 0x04002A39 RID: 10809
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B36")]
			private long m_KeyCode;
		}

		// Token: 0x020006F8 RID: 1784
		[Token(Token = "0x2000E3D")]
		public class AddMemberNewCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019F5 RID: 6645 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA6")]
			[Address(RVA = "0x1011FBD70", Offset = "0x11FBD70", VA = "0x1011FBD70", Slot = "5")]
			public override void AddCode(long fkeycode)
			{
			}

			// Token: 0x060019F6 RID: 6646 RVA: 0x0000BA18 File Offset: 0x00009C18
			[Token(Token = "0x6005EA7")]
			[Address(RVA = "0x1011FBDE0", Offset = "0x11FBDE0", VA = "0x1011FBDE0", Slot = "6")]
			public override bool IsAddCode()
			{
				return default(bool);
			}

			// Token: 0x060019F7 RID: 6647 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA8")]
			[Address(RVA = "0x1011FBDE8", Offset = "0x11FBDE8", VA = "0x1011FBDE8", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019F8 RID: 6648 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EA9")]
			[Address(RVA = "0x1011FC04C", Offset = "0x11FC04C", VA = "0x1011FC04C")]
			private void CallbackPartyAdd(INetComHandle phandle)
			{
			}

			// Token: 0x060019F9 RID: 6649 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EAA")]
			[Address(RVA = "0x1011FB694", Offset = "0x11FB694", VA = "0x1011FB694")]
			public AddMemberNewCom()
			{
			}

			// Token: 0x04002A3A RID: 10810
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B37")]
			private List<long> m_KeyCode;
		}

		// Token: 0x020006F9 RID: 1785
		[Token(Token = "0x2000E3E")]
		public class DummyEventCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060019FA RID: 6650 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EAB")]
			[Address(RVA = "0x1011FCDC0", Offset = "0x11FCDC0", VA = "0x1011FCDC0", Slot = "4")]
			public override void MakeSendCom(FldComPartyScript pbase)
			{
			}

			// Token: 0x060019FB RID: 6651 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EAC")]
			[Address(RVA = "0x1011FB704", Offset = "0x11FB704", VA = "0x1011FB704")]
			public DummyEventCom()
			{
			}
		}
	}
}
