﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000717 RID: 1815
	[Token(Token = "0x200059A")]
	[StructLayout(3)]
	public class FldDefaultObjectUtil : IDataBaseResource
	{
		// Token: 0x06001A88 RID: 6792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001878")]
		[Address(RVA = "0x1011FE268", Offset = "0x11FE268", VA = "0x1011FE268", Slot = "4")]
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
		}

		// Token: 0x06001A89 RID: 6793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001879")]
		[Address(RVA = "0x1011FE2F8", Offset = "0x11FE2F8", VA = "0x1011FE2F8")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06001A8A RID: 6794 RVA: 0x0000BE08 File Offset: 0x0000A008
		[Token(Token = "0x600187A")]
		[Address(RVA = "0x1011FE3B4", Offset = "0x11FE3B4", VA = "0x1011FE3B4")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06001A8B RID: 6795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600187B")]
		[Address(RVA = "0x1011FE414", Offset = "0x11FE414", VA = "0x1011FE414")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x06001A8C RID: 6796 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600187C")]
		[Address(RVA = "0x1011FE47C", Offset = "0x11FE47C", VA = "0x1011FE47C")]
		public static DefaultObjectListDB GetDB()
		{
			return null;
		}

		// Token: 0x06001A8D RID: 6797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600187D")]
		[Address(RVA = "0x1011FE3AC", Offset = "0x11FE3AC", VA = "0x1011FE3AC")]
		public FldDefaultObjectUtil()
		{
		}

		// Token: 0x04002AB0 RID: 10928
		[Token(Token = "0x40022BB")]
		public static FldDefaultObjectUtil ms_FldDefaultObjDB;

		// Token: 0x04002AB1 RID: 10929
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40022BC")]
		private DefaultObjectListDB m_DB;
	}
}
