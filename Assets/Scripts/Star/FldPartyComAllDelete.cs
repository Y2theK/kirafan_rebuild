﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006FA RID: 1786
	[Token(Token = "0x200058D")]
	[StructLayout(3)]
	public class FldPartyComAllDelete : IFldNetComModule
	{
		// Token: 0x060019FC RID: 6652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001811")]
		[Address(RVA = "0x1011FE568", Offset = "0x11FE568", VA = "0x1011FE568")]
		public FldPartyComAllDelete()
		{
		}

		// Token: 0x060019FD RID: 6653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001812")]
		[Address(RVA = "0x1011FE5C4", Offset = "0x11FE5C4", VA = "0x1011FE5C4")]
		public void PlaySend()
		{
		}

		// Token: 0x060019FE RID: 6654 RVA: 0x0000BA30 File Offset: 0x00009C30
		[Token(Token = "0x6001813")]
		[Address(RVA = "0x1011FE5FC", Offset = "0x11FE5FC", VA = "0x1011FE5FC")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060019FF RID: 6655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001814")]
		[Address(RVA = "0x1011FE6B8", Offset = "0x11FE6B8", VA = "0x1011FE6B8")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x06001A00 RID: 6656 RVA: 0x0000BA48 File Offset: 0x00009C48
		[Token(Token = "0x6001815")]
		[Address(RVA = "0x1011FE838", Offset = "0x11FE838", VA = "0x1011FE838")]
		public bool SetUpObjList()
		{
			return default(bool);
		}

		// Token: 0x06001A01 RID: 6657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001816")]
		[Address(RVA = "0x1011FE8F4", Offset = "0x11FE8F4", VA = "0x1011FE8F4")]
		private void CallbackObjSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x06001A02 RID: 6658 RVA: 0x0000BA60 File Offset: 0x00009C60
		[Token(Token = "0x6001817")]
		[Address(RVA = "0x1011FEA74", Offset = "0x11FEA74", VA = "0x1011FEA74", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x06001A03 RID: 6659 RVA: 0x0000BA78 File Offset: 0x00009C78
		[Token(Token = "0x6001818")]
		[Address(RVA = "0x1011FEAD8", Offset = "0x11FEAD8", VA = "0x1011FEAD8")]
		public bool DefaultDeleteManage()
		{
			return default(bool);
		}

		// Token: 0x06001A04 RID: 6660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001819")]
		[Address(RVA = "0x1011FEC6C", Offset = "0x11FEC6C", VA = "0x1011FEC6C")]
		private void CallbackDeleteUp(INetComHandle phandle)
		{
		}

		// Token: 0x04002A3B RID: 10811
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400227C")]
		private FldPartyComAllDelete.eStep m_Step;

		// Token: 0x04002A3C RID: 10812
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400227D")]
		private long[] m_Table;

		// Token: 0x04002A3D RID: 10813
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400227E")]
		private int m_DelStep;

		// Token: 0x020006FB RID: 1787
		[Token(Token = "0x2000E3F")]
		public enum eStep
		{
			// Token: 0x04002A3F RID: 10815
			[Token(Token = "0x4005B39")]
			GetState,
			// Token: 0x04002A40 RID: 10816
			[Token(Token = "0x4005B3A")]
			WaitCheck,
			// Token: 0x04002A41 RID: 10817
			[Token(Token = "0x4005B3B")]
			DeleteUp,
			// Token: 0x04002A42 RID: 10818
			[Token(Token = "0x4005B3C")]
			SetUpCheck,
			// Token: 0x04002A43 RID: 10819
			[Token(Token = "0x4005B3D")]
			GetObjState,
			// Token: 0x04002A44 RID: 10820
			[Token(Token = "0x4005B3E")]
			DeleteObjUp,
			// Token: 0x04002A45 RID: 10821
			[Token(Token = "0x4005B3F")]
			End
		}
	}
}
