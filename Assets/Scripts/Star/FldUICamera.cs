﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000719 RID: 1817
	[Token(Token = "0x200059C")]
	[StructLayout(3)]
	public class FldUICamera : MonoBehaviour
	{
		// Token: 0x06001A90 RID: 6800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001880")]
		[Address(RVA = "0x1011FEC78", Offset = "0x11FEC78", VA = "0x1011FEC78")]
		public static void CreateUICamera()
		{
		}

		// Token: 0x06001A91 RID: 6801 RVA: 0x0000BE20 File Offset: 0x0000A020
		[Token(Token = "0x6001881")]
		[Address(RVA = "0x1011FEEDC", Offset = "0x11FEEDC", VA = "0x1011FEEDC")]
		public static Vector3 CalcCanvasPos(Camera pcamera, Vector3 pworldpos)
		{
			return default(Vector3);
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x0000BE38 File Offset: 0x0000A038
		[Token(Token = "0x6001882")]
		[Address(RVA = "0x1011FEFF4", Offset = "0x11FEFF4", VA = "0x1011FEFF4")]
		public static Vector3 CalcScreenCanvasPos(Vector2 pscreen)
		{
			return default(Vector3);
		}

		// Token: 0x06001A93 RID: 6803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001883")]
		[Address(RVA = "0x1011FF120", Offset = "0x11FF120", VA = "0x1011FF120")]
		public FldUICamera()
		{
		}

		// Token: 0x04002AB2 RID: 10930
		[Token(Token = "0x40022BD")]
		private const float WD_KEY = 1334f;

		// Token: 0x04002AB3 RID: 10931
		[Token(Token = "0x40022BE")]
		private const float HI_KEY = 750f;
	}
}
