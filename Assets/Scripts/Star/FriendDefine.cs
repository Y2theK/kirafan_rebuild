﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000744 RID: 1860
	[Token(Token = "0x20005AF")]
	[StructLayout(3)]
	public static class FriendDefine
	{
		// Token: 0x02000745 RID: 1861
		[Token(Token = "0x2000E66")]
		public enum eType
		{
			// Token: 0x04002B6D RID: 11117
			[Token(Token = "0x4005BDC")]
			Friend,
			// Token: 0x04002B6E RID: 11118
			[Token(Token = "0x4005BDD")]
			OpponentPropose,
			// Token: 0x04002B6F RID: 11119
			[Token(Token = "0x4005BDE")]
			SelfPropose,
			// Token: 0x04002B70 RID: 11120
			[Token(Token = "0x4005BDF")]
			Guest
		}

		// Token: 0x02000746 RID: 1862
		[Token(Token = "0x2000E67")]
		public enum eGetAllType
		{
			// Token: 0x04002B72 RID: 11122
			[Token(Token = "0x4005BE1")]
			Friend = 1,
			// Token: 0x04002B73 RID: 11123
			[Token(Token = "0x4005BE2")]
			Guest,
			// Token: 0x04002B74 RID: 11124
			[Token(Token = "0x4005BE3")]
			Both
		}
	}
}
