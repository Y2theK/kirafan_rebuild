﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x02000747 RID: 1863
	[Token(Token = "0x20005B0")]
	[StructLayout(3)]
	public sealed class FriendManager
	{
		// Token: 0x06001B9E RID: 7070 RVA: 0x0000C510 File Offset: 0x0000A710
		[Token(Token = "0x600193C")]
		[Address(RVA = "0x1011FF128", Offset = "0x11FF128", VA = "0x1011FF128")]
		public int GetFriendListNum(FriendDefine.eType type)
		{
			return 0;
		}

		// Token: 0x06001B9F RID: 7071 RVA: 0x0000C528 File Offset: 0x0000A728
		[Token(Token = "0x600193D")]
		[Address(RVA = "0x1011FF204", Offset = "0x11FF204", VA = "0x1011FF204")]
		public int GetFriendListNumAvailableUserSupportData()
		{
			return 0;
		}

		// Token: 0x06001BA0 RID: 7072 RVA: 0x0000C540 File Offset: 0x0000A740
		[Token(Token = "0x600193E")]
		[Address(RVA = "0x1011FF330", Offset = "0x11FF330", VA = "0x1011FF330")]
		public int GetFriendListNum()
		{
			return 0;
		}

		// Token: 0x06001BA1 RID: 7073 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600193F")]
		[Address(RVA = "0x1011FF390", Offset = "0x11FF390", VA = "0x1011FF390")]
		public FriendManager.FriendData GetFriendDataAt(int index)
		{
			return null;
		}

		// Token: 0x06001BA2 RID: 7074 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001940")]
		[Address(RVA = "0x1011FF444", Offset = "0x11FF444", VA = "0x1011FF444")]
		public FriendManager.FriendData GetFriendData(long id)
		{
			return null;
		}

		// Token: 0x06001BA3 RID: 7075 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001941")]
		[Address(RVA = "0x1011FF544", Offset = "0x11FF544", VA = "0x1011FF544")]
		public FriendManager.FriendData GetFriendDataByMngFriendID(long mngFriendID)
		{
			return null;
		}

		// Token: 0x06001BA4 RID: 7076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001942")]
		[Address(RVA = "0x1011FF644", Offset = "0x11FF644", VA = "0x1011FF644")]
		public void ClearFriendList()
		{
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001943")]
		[Address(RVA = "0x1011FF6AC", Offset = "0x11FF6AC", VA = "0x1011FF6AC")]
		private void FriendListSetAll(List<FriendManager.FriendData> listFriend, List<FriendManager.FriendData> listGuest)
		{
		}

		// Token: 0x06001BA6 RID: 7078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001944")]
		[Address(RVA = "0x1011FF798", Offset = "0x11FF798", VA = "0x1011FF798")]
		private void FriendListUpsert(long friendPlayerId, long managedFriendId, FriendDefine.eType friendType)
		{
		}

		// Token: 0x06001BA7 RID: 7079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001945")]
		[Address(RVA = "0x1011FF858", Offset = "0x11FF858", VA = "0x1011FF858")]
		private void FriendListUpsert(FriendManager.FriendData friendData)
		{
		}

		// Token: 0x06001BA8 RID: 7080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001946")]
		[Address(RVA = "0x1011FFBA4", Offset = "0x11FFBA4", VA = "0x1011FFBA4")]
		private void FriendListAdd(FriendManager.FriendData data)
		{
		}

		// Token: 0x06001BA9 RID: 7081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001947")]
		[Address(RVA = "0x1011FFC40", Offset = "0x11FFC40", VA = "0x1011FFC40")]
		private void FriendListRemoveAt(int index)
		{
		}

		// Token: 0x06001BAA RID: 7082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001948")]
		[Address(RVA = "0x1011FFD3C", Offset = "0x11FFD3C", VA = "0x1011FFD3C")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06001BAB RID: 7083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001949")]
		[Address(RVA = "0x1011FFD40", Offset = "0x11FFD40", VA = "0x1011FFD40")]
		public void RefreshFriendSupportList()
		{
		}

		// Token: 0x06001BAC RID: 7084 RVA: 0x0000C558 File Offset: 0x0000A758
		[Token(Token = "0x600194A")]
		[Address(RVA = "0x1011FFD48", Offset = "0x11FFD48", VA = "0x1011FFD48")]
		public bool FilterFriendList(long partyMngID, bool bonusFilter, ref List<FriendManager.FriendData> friendDatas, ref List<UserSupportData> userSupportDatas)
		{
			return default(bool);
		}

		// Token: 0x06001BAD RID: 7085 RVA: 0x0000C570 File Offset: 0x0000A770
		[Token(Token = "0x600194B")]
		[Address(RVA = "0x101200714", Offset = "0x1200714", VA = "0x101200714")]
		private int SortCompare(FriendManager.FriendSupportData r, FriendManager.FriendSupportData l)
		{
			return 0;
		}

		// Token: 0x06001BAE RID: 7086 RVA: 0x0000C588 File Offset: 0x0000A788
		[Token(Token = "0x600194C")]
		[Address(RVA = "0x101200800", Offset = "0x1200800", VA = "0x101200800")]
		private long GetSortKey(FriendManager.FriendSupportData data)
		{
			return 0L;
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x06001BAF RID: 7087 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BB0 RID: 7088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000011")]
		private event Action<bool> m_OnResponse
		{
			[Token(Token = "0x600194D")]
			[Address(RVA = "0x101200CF8", Offset = "0x1200CF8", VA = "0x101200CF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600194E")]
			[Address(RVA = "0x101200E04", Offset = "0x1200E04", VA = "0x101200E04")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06001BB1 RID: 7089 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000200")]
		public FriendManager.FriendData SearchedFriendData
		{
			[Token(Token = "0x600194F")]
			[Address(RVA = "0x101200F10", Offset = "0x1200F10", VA = "0x101200F10")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001BB2 RID: 7090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001950")]
		[Address(RVA = "0x101200F18", Offset = "0x1200F18", VA = "0x101200F18")]
		private void OnConfirmRequestError()
		{
		}

		// Token: 0x06001BB3 RID: 7091 RVA: 0x0000C5A0 File Offset: 0x0000A7A0
		[Token(Token = "0x6001951")]
		[Address(RVA = "0x101200F74", Offset = "0x1200F74", VA = "0x101200F74")]
		public bool Request_GetAll(FriendDefine.eGetAllType type, Action<bool> onResponse, long opt_battlePartyMngID = -1L, bool opt_ignoreSupportParty = false, bool opt_reload = false)
		{
			return default(bool);
		}

		// Token: 0x06001BB4 RID: 7092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001952")]
		[Address(RVA = "0x101201108", Offset = "0x1201108", VA = "0x101201108")]
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BB5 RID: 7093 RVA: 0x0000C5B8 File Offset: 0x0000A7B8
		[Token(Token = "0x6001953")]
		[Address(RVA = "0x101201450", Offset = "0x1201450", VA = "0x101201450")]
		public bool Request_Propose(long playerID, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BB6 RID: 7094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001954")]
		[Address(RVA = "0x101201590", Offset = "0x1201590", VA = "0x101201590")]
		private void OnResponse_Propose(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BB7 RID: 7095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001955")]
		[Address(RVA = "0x10120170C", Offset = "0x120170C", VA = "0x10120170C")]
		private void ErrorLimitTarget()
		{
		}

		// Token: 0x06001BB8 RID: 7096 RVA: 0x0000C5D0 File Offset: 0x0000A7D0
		[Token(Token = "0x6001956")]
		[Address(RVA = "0x1012017E0", Offset = "0x12017E0", VA = "0x1012017E0")]
		public bool Request_Accept(long mngFriendID, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BB9 RID: 7097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001957")]
		[Address(RVA = "0x101201924", Offset = "0x1201924", VA = "0x101201924")]
		private void OnResponse_Accept(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BBA RID: 7098 RVA: 0x0000C5E8 File Offset: 0x0000A7E8
		[Token(Token = "0x6001958")]
		[Address(RVA = "0x101201A68", Offset = "0x1201A68", VA = "0x101201A68")]
		public bool Request_Refuse(long mngFriendID, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BBB RID: 7099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001959")]
		[Address(RVA = "0x101201BAC", Offset = "0x1201BAC", VA = "0x101201BAC")]
		private void OnResponse_Refuse(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BBC RID: 7100 RVA: 0x0000C600 File Offset: 0x0000A800
		[Token(Token = "0x600195A")]
		[Address(RVA = "0x101201CE4", Offset = "0x1201CE4", VA = "0x101201CE4")]
		public bool Request_Cancel(long mngFriendID, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BBD RID: 7101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600195B")]
		[Address(RVA = "0x101201E28", Offset = "0x1201E28", VA = "0x101201E28")]
		private void OnResponse_Cancel(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BBE RID: 7102 RVA: 0x0000C618 File Offset: 0x0000A818
		[Token(Token = "0x600195C")]
		[Address(RVA = "0x101201F7C", Offset = "0x1201F7C", VA = "0x101201F7C")]
		public bool Request_Terminate(long mngFriendID, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BBF RID: 7103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600195D")]
		[Address(RVA = "0x1012020C0", Offset = "0x12020C0", VA = "0x1012020C0")]
		private void OnResponse_Terminate(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BC0 RID: 7104 RVA: 0x0000C630 File Offset: 0x0000A830
		[Token(Token = "0x600195E")]
		[Address(RVA = "0x1012021F4", Offset = "0x12021F4", VA = "0x1012021F4")]
		public bool Request_Search(string myCode, Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BC1 RID: 7105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600195F")]
		[Address(RVA = "0x101202334", Offset = "0x1202334", VA = "0x101202334")]
		private void OnResponse_Search(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BC2 RID: 7106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001960")]
		[Address(RVA = "0x101201260", Offset = "0x1201260", VA = "0x101201260")]
		private static void wwwConvert(FriendList[] src, List<FriendManager.FriendData> dest)
		{
		}

		// Token: 0x06001BC3 RID: 7107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001961")]
		[Address(RVA = "0x1012024B8", Offset = "0x12024B8", VA = "0x1012024B8")]
		public static void wwwConvert(FriendList src, FriendManager.FriendData dest)
		{
		}

		// Token: 0x06001BC4 RID: 7108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001962")]
		[Address(RVA = "0x101201370", Offset = "0x1201370", VA = "0x101201370")]
		private void UpdateNoticeFriendCount()
		{
		}

		// Token: 0x06001BC5 RID: 7109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001963")]
		[Address(RVA = "0x1012026F8", Offset = "0x12026F8", VA = "0x1012026F8")]
		public FriendManager()
		{
		}

		// Token: 0x04002B75 RID: 11125
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002307")]
		private List<FriendManager.FriendData> m_FriendList;

		// Token: 0x04002B76 RID: 11126
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002308")]
		private List<FriendManager.FriendSupportData> m_FriendSupportDataList;

		// Token: 0x04002B77 RID: 11127
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002309")]
		private CharacterParamCalculator m_Calc;

		// Token: 0x04002B79 RID: 11129
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400230B")]
		private long m_ProposePlayerID;

		// Token: 0x04002B7A RID: 11130
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400230C")]
		private long m_AcceptMngFriendID;

		// Token: 0x04002B7B RID: 11131
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400230D")]
		private long m_RefuseMngFriendID;

		// Token: 0x04002B7C RID: 11132
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400230E")]
		private long m_CancelMngFriendID;

		// Token: 0x04002B7D RID: 11133
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400230F")]
		private long m_TerminateMngFriendID;

		// Token: 0x04002B7E RID: 11134
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002310")]
		private FriendManager.FriendData m_SearchedFriendData;

		// Token: 0x02000748 RID: 1864
		[Token(Token = "0x2000E68")]
		public class FriendData
		{
			// Token: 0x1700021C RID: 540
			// (get) Token: 0x06001BC7 RID: 7111 RVA: 0x0000C648 File Offset: 0x0000A848
			// (set) Token: 0x06001BC8 RID: 7112 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x17000691")]
			public int m_AchievementID
			{
				[Token(Token = "0x6005F23")]
				[Address(RVA = "0x1011FFB94", Offset = "0x11FFB94", VA = "0x1011FFB94")]
				[CompilerGenerated]
				get
				{
					return 0;
				}
				[Token(Token = "0x6005F24")]
				[Address(RVA = "0x1011FFB9C", Offset = "0x11FFB9C", VA = "0x1011FFB9C")]
				[CompilerGenerated]
				set
				{
				}
			}

			// Token: 0x06001BC9 RID: 7113 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F25")]
			[Address(RVA = "0x1011FF850", Offset = "0x11FF850", VA = "0x1011FF850")]
			public FriendData()
			{
			}

			// Token: 0x04002B7F RID: 11135
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BE4")]
			public FriendDefine.eType m_Type;

			// Token: 0x04002B80 RID: 11136
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005BE5")]
			public long m_MngFriendID;

			// Token: 0x04002B81 RID: 11137
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005BE6")]
			public long m_ID;

			// Token: 0x04002B82 RID: 11138
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005BE7")]
			public string m_Name;

			// Token: 0x04002B83 RID: 11139
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005BE8")]
			public string m_Comment;

			// Token: 0x04002B84 RID: 11140
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005BE9")]
			public int m_Lv;

			// Token: 0x04002B85 RID: 11141
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005BEA")]
			public string m_MyCode;

			// Token: 0x04002B87 RID: 11143
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4005BEC")]
			public DateTime m_LastLoginAt;

			// Token: 0x04002B88 RID: 11144
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4005BED")]
			public UserSupportData m_LeaderCharaData;

			// Token: 0x04002B89 RID: 11145
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4005BEE")]
			public List<UserSupportData> m_UserSupportData;

			// Token: 0x04002B8A RID: 11146
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4005BEF")]
			public string m_SupportPartyName;

			// Token: 0x04002B8B RID: 11147
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4005BF0")]
			public int m_SupportLimit;
		}

		// Token: 0x02000749 RID: 1865
		[Token(Token = "0x2000E69")]
		public class FriendSupportData
		{
			// Token: 0x06001BCA RID: 7114 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F26")]
			[Address(RVA = "0x1012006CC", Offset = "0x12006CC", VA = "0x1012006CC")]
			public FriendSupportData(FriendManager.FriendData friend, UserSupportData support, int supportIdx)
			{
			}

			// Token: 0x04002B8C RID: 11148
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BF1")]
			public FriendManager.FriendData m_FriendData;

			// Token: 0x04002B8D RID: 11149
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005BF2")]
			public UserSupportData m_UserSupportData;

			// Token: 0x04002B8E RID: 11150
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005BF3")]
			public int m_SupportIdx;
		}
	}
}
