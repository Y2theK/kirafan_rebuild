﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using GachaResponseTypes;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x0200074C RID: 1868
	[Token(Token = "0x20005B1")]
	[StructLayout(3)]
	public sealed class Gacha
	{
		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06001BCE RID: 7118 RVA: 0x0000C678 File Offset: 0x0000A878
		[Token(Token = "0x17000201")]
		public DateTime FreeGachaRefreshAt
		{
			[Token(Token = "0x6001965")]
			[Address(RVA = "0x101202840", Offset = "0x1202840", VA = "0x101202840")]
			get
			{
				return default(DateTime);
			}
		}

		// Token: 0x06001BCF RID: 7119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001966")]
		[Address(RVA = "0x101202848", Offset = "0x1202848", VA = "0x101202848")]
		public Gacha()
		{
		}

		// Token: 0x06001BD0 RID: 7120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001967")]
		[Address(RVA = "0x1012029A8", Offset = "0x12029A8", VA = "0x1012029A8")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06001BD1 RID: 7121 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001968")]
		[Address(RVA = "0x101202A08", Offset = "0x1202A08", VA = "0x101202A08")]
		public List<Gacha.GachaData> GetGachaList()
		{
			return null;
		}

		// Token: 0x06001BD2 RID: 7122 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001969")]
		[Address(RVA = "0x101202A10", Offset = "0x1202A10", VA = "0x101202A10")]
		public Dictionary<int, List<Gacha.GachaDailyFrees>> GetGachaFreeSchedule()
		{
			return null;
		}

		// Token: 0x06001BD3 RID: 7123 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600196A")]
		[Address(RVA = "0x101202A18", Offset = "0x1202A18", VA = "0x101202A18")]
		public Gacha.GachaData GetGachaData(int gachaID)
		{
			return null;
		}

		// Token: 0x06001BD4 RID: 7124 RVA: 0x0000C690 File Offset: 0x0000A890
		[Token(Token = "0x600196B")]
		[Address(RVA = "0x101202B18", Offset = "0x1202B18", VA = "0x101202B18")]
		public bool GetDrawPointDelta(int gachaId, out int beforeDrawPoint, out int afterDrawPoint)
		{
			return default(bool);
		}

		// Token: 0x06001BD5 RID: 7125 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600196C")]
		[Address(RVA = "0x101202C2C", Offset = "0x1202C2C", VA = "0x101202C2C")]
		public List<Gacha.FinishedGacha> GetFinishedGachaList()
		{
			return null;
		}

		// Token: 0x06001BD6 RID: 7126 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600196D")]
		[Address(RVA = "0x101202C34", Offset = "0x1202C34", VA = "0x101202C34")]
		public List<Gacha.Result> GetResults()
		{
			return null;
		}

		// Token: 0x06001BD7 RID: 7127 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600196E")]
		[Address(RVA = "0x101202C3C", Offset = "0x1202C3C", VA = "0x101202C3C")]
		public List<Gacha.BonusItem> GetBonusItems()
		{
			return null;
		}

		// Token: 0x06001BD8 RID: 7128 RVA: 0x0000C6A8 File Offset: 0x0000A8A8
		[Token(Token = "0x600196F")]
		[Address(RVA = "0x101202C44", Offset = "0x1202C44", VA = "0x101202C44")]
		public bool IsExistBonusItems()
		{
			return default(bool);
		}

		// Token: 0x06001BD9 RID: 7129 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001970")]
		[Address(RVA = "0x101202CB0", Offset = "0x1202CB0", VA = "0x101202CB0")]
		public List<int> GetBoxCharaIDs()
		{
			return null;
		}

		// Token: 0x06001BDA RID: 7130 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001971")]
		[Address(RVA = "0x101202CB8", Offset = "0x1202CB8", VA = "0x101202CB8")]
		public List<int> GetBoxPickupCharaIDs()
		{
			return null;
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06001BDB RID: 7131 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BDC RID: 7132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000012")]
		private event Action<MeigewwwParam, GetAll> m_OnResponse_GachaGetAll
		{
			[Token(Token = "0x6001972")]
			[Address(RVA = "0x101202CC0", Offset = "0x1202CC0", VA = "0x101202CC0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6001973")]
			[Address(RVA = "0x101202DCC", Offset = "0x1202DCC", VA = "0x101202DCC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06001BDD RID: 7133 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BDE RID: 7134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000013")]
		private event Action<GachaDefine.eReturnGachaPlay, string> m_OnResponse_GachaPlay
		{
			[Token(Token = "0x6001974")]
			[Address(RVA = "0x101202ED8", Offset = "0x1202ED8", VA = "0x101202ED8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6001975")]
			[Address(RVA = "0x101202FE4", Offset = "0x1202FE4", VA = "0x101202FE4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x06001BDF RID: 7135 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BE0 RID: 7136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000014")]
		private event Action<GachaDefine.eReturnGachaBox, string> m_OnResponse_GachaBox
		{
			[Token(Token = "0x6001976")]
			[Address(RVA = "0x1012030F0", Offset = "0x12030F0", VA = "0x1012030F0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6001977")]
			[Address(RVA = "0x1012031FC", Offset = "0x12031FC", VA = "0x1012031FC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06001BE1 RID: 7137 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BE2 RID: 7138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000015")]
		private event Action<ResultCode> m_OnResponse_GachaReDraw
		{
			[Token(Token = "0x6001978")]
			[Address(RVA = "0x101203308", Offset = "0x1203308", VA = "0x101203308")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6001979")]
			[Address(RVA = "0x101203414", Offset = "0x1203414", VA = "0x101203414")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06001BE3 RID: 7139 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BE4 RID: 7140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000016")]
		private event Action<ResultCode> m_OnResponse_GachaFix
		{
			[Token(Token = "0x600197A")]
			[Address(RVA = "0x101203520", Offset = "0x1203520", VA = "0x101203520")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600197B")]
			[Address(RVA = "0x10120362C", Offset = "0x120362C", VA = "0x10120362C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x06001BE5 RID: 7141 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BE6 RID: 7142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000017")]
		private event Action<GachaDefine.eReturnGachaPlay, string> m_OnResponse_PointToCharacter
		{
			[Token(Token = "0x600197C")]
			[Address(RVA = "0x101203738", Offset = "0x1203738", VA = "0x101203738")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600197D")]
			[Address(RVA = "0x101203844", Offset = "0x1203844", VA = "0x101203844")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x06001BE7 RID: 7143 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06001BE8 RID: 7144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000018")]
		private event Action<ResultCode> m_OnResponse_PointToItem
		{
			[Token(Token = "0x600197E")]
			[Address(RVA = "0x101203950", Offset = "0x1203950", VA = "0x101203950")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600197F")]
			[Address(RVA = "0x101203A5C", Offset = "0x1203A5C", VA = "0x101203A5C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06001BE9 RID: 7145 RVA: 0x0000C6C0 File Offset: 0x0000A8C0
		[Token(Token = "0x6001980")]
		[Address(RVA = "0x101203B68", Offset = "0x1203B68", VA = "0x101203B68")]
		public int GetPlayedGachaID()
		{
			return 0;
		}

		// Token: 0x06001BEA RID: 7146 RVA: 0x0000C6D8 File Offset: 0x0000A8D8
		[Token(Token = "0x6001981")]
		[Address(RVA = "0x101203B70", Offset = "0x1203B70", VA = "0x101203B70")]
		public int GetPlayedSelectionCharaID()
		{
			return 0;
		}

		// Token: 0x06001BEB RID: 7147 RVA: 0x0000C6F0 File Offset: 0x0000A8F0
		[Token(Token = "0x6001982")]
		[Address(RVA = "0x101203B78", Offset = "0x1203B78", VA = "0x101203B78")]
		public bool GetPlayedStepGacha()
		{
			return default(bool);
		}

		// Token: 0x06001BEC RID: 7148 RVA: 0x0000C708 File Offset: 0x0000A908
		[Token(Token = "0x6001983")]
		[Address(RVA = "0x101203B80", Offset = "0x1203B80", VA = "0x101203B80")]
		public int GetPlayedStepNum()
		{
			return 0;
		}

		// Token: 0x06001BED RID: 7149 RVA: 0x0000C720 File Offset: 0x0000A920
		[Token(Token = "0x6001984")]
		[Address(RVA = "0x101203B88", Offset = "0x1203B88", VA = "0x101203B88")]
		public bool GetPlayedDrawPointGacha()
		{
			return default(bool);
		}

		// Token: 0x06001BEE RID: 7150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001985")]
		[Address(RVA = "0x101203B90", Offset = "0x1203B90", VA = "0x101203B90")]
		public void ClearPlayedGacha()
		{
		}

		// Token: 0x06001BEF RID: 7151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001986")]
		[Address(RVA = "0x101203BA8", Offset = "0x1203BA8", VA = "0x101203BA8")]
		public void ClearFinishedGacha()
		{
		}

		// Token: 0x06001BF0 RID: 7152 RVA: 0x0000C738 File Offset: 0x0000A938
		[Token(Token = "0x6001987")]
		[Address(RVA = "0x101203C08", Offset = "0x1203C08", VA = "0x101203C08")]
		public bool Request_GachaGetAll(Action<MeigewwwParam, GetAll> onResponse, bool isOpenConnectionIcon = true)
		{
			return default(bool);
		}

		// Token: 0x06001BF1 RID: 7153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001988")]
		[Address(RVA = "0x101203D94", Offset = "0x1203D94", VA = "0x101203D94")]
		private void OnResponse_GachaGetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BF2 RID: 7154 RVA: 0x0000C750 File Offset: 0x0000A950
		[Token(Token = "0x6001989")]
		[Address(RVA = "0x101205184", Offset = "0x1205184", VA = "0x101205184")]
		public bool Request_GachaPlay(int gachaID, GachaDefine.ePlayType playType, int selectionCharaID, bool isStepup, int stepupNum, int totalNum, bool isFree, bool isRateUp, Action<GachaDefine.eReturnGachaPlay, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BF3 RID: 7155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600198A")]
		[Address(RVA = "0x10120535C", Offset = "0x120535C", VA = "0x10120535C")]
		private void OnResponse_GachaPlay(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BF4 RID: 7156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600198B")]
		[Address(RVA = "0x101205990", Offset = "0x1205990", VA = "0x101205990")]
		private void ParseGachaResult(GachaResult[] gachaResults, GachaBonus[] gachaBonuses)
		{
		}

		// Token: 0x06001BF5 RID: 7157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600198C")]
		[Address(RVA = "0x101205C24", Offset = "0x1205C24", VA = "0x101205C24")]
		private void ParseDrawPoint(int gachaId, int beforeDrawPoint, int afterDrawPoint)
		{
		}

		// Token: 0x06001BF6 RID: 7158 RVA: 0x0000C768 File Offset: 0x0000A968
		[Token(Token = "0x600198D")]
		[Address(RVA = "0x101205DF4", Offset = "0x1205DF4", VA = "0x101205DF4")]
		public bool Request_GachaReDraw(int gachaId, Action<ResultCode> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BF7 RID: 7159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600198E")]
		[Address(RVA = "0x101205EEC", Offset = "0x1205EEC", VA = "0x101205EEC")]
		private void OnResponse_GachaReDraw(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BF8 RID: 7160 RVA: 0x0000C780 File Offset: 0x0000A980
		[Token(Token = "0x600198F")]
		[Address(RVA = "0x101206138", Offset = "0x1206138", VA = "0x101206138")]
		public bool Request_GachaFix(Action<ResultCode> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BF9 RID: 7161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001990")]
		[Address(RVA = "0x101206224", Offset = "0x1206224", VA = "0x101206224")]
		private void OnResponse_GachaFix(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BFA RID: 7162 RVA: 0x0000C798 File Offset: 0x0000A998
		[Token(Token = "0x6001991")]
		[Address(RVA = "0x101206510", Offset = "0x1206510", VA = "0x101206510")]
		public bool Request_GachaBox(int gachaID, Action<GachaDefine.eReturnGachaBox, string> onResponse, bool isOpenConnectionIcon = true)
		{
			return default(bool);
		}

		// Token: 0x06001BFB RID: 7163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001992")]
		[Address(RVA = "0x101206620", Offset = "0x1206620", VA = "0x101206620")]
		private void OnResponse_GachaBox(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BFC RID: 7164 RVA: 0x0000C7B0 File Offset: 0x0000A9B0
		[Token(Token = "0x6001993")]
		[Address(RVA = "0x101206920", Offset = "0x1206920", VA = "0x101206920")]
		public bool Request_DrawPointToCharacter(int gachaId, int characterId, Action<GachaDefine.eReturnGachaPlay, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BFD RID: 7165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001994")]
		[Address(RVA = "0x101206AB4", Offset = "0x1206AB4", VA = "0x101206AB4")]
		private void OnResponse_DrawPointToCharacter(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001BFE RID: 7166 RVA: 0x0000C7C8 File Offset: 0x0000A9C8
		[Token(Token = "0x6001995")]
		[Address(RVA = "0x101206D10", Offset = "0x1206D10", VA = "0x101206D10")]
		public bool Request_DrawPointToItem(int[] gachaIDs, Action<ResultCode> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06001BFF RID: 7167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001996")]
		[Address(RVA = "0x101206E10", Offset = "0x1206E10", VA = "0x101206E10")]
		private void OnResponse_DrawPointToItem(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06001C00 RID: 7168 RVA: 0x0000C7E0 File Offset: 0x0000A9E0
		[Token(Token = "0x6001997")]
		[Address(RVA = "0x101206EC4", Offset = "0x1206EC4", VA = "0x101206EC4")]
		private static int SortComp(int x, int y)
		{
			return 0;
		}

		// Token: 0x04002B92 RID: 11154
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002311")]
		private List<Gacha.GachaData> m_GachaList;

		// Token: 0x04002B93 RID: 11155
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002312")]
		private Dictionary<int, List<Gacha.GachaDailyFrees>> m_GachaFreeSchedule;

		// Token: 0x04002B94 RID: 11156
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002313")]
		private DateTime m_FreeGachaRefreshAt;

		// Token: 0x04002B95 RID: 11157
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002314")]
		private Dictionary<int, Gacha.DrawPointDelta> m_DrawPointDeltaList;

		// Token: 0x04002B96 RID: 11158
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002315")]
		private List<Gacha.FinishedGacha> m_FinishedGachaList;

		// Token: 0x04002B97 RID: 11159
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002316")]
		private List<Gacha.Result> m_Results;

		// Token: 0x04002B98 RID: 11160
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002317")]
		private List<Gacha.BonusItem> m_BonusItems;

		// Token: 0x04002B99 RID: 11161
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002318")]
		private List<int> m_BoxCharaIDs;

		// Token: 0x04002B9A RID: 11162
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002319")]
		private List<int> m_BoxPickupCharaIDs;

		// Token: 0x04002BA2 RID: 11170
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002321")]
		private int m_PlayedGachaID;

		// Token: 0x04002BA3 RID: 11171
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4002322")]
		private int m_PlayedSelectionCharaID;

		// Token: 0x04002BA4 RID: 11172
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002323")]
		private bool m_PlayedStepGacha;

		// Token: 0x04002BA5 RID: 11173
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4002324")]
		private int m_PlayedStepNum;

		// Token: 0x04002BA6 RID: 11174
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002325")]
		private bool m_PlayedDrawPointGacha;

		// Token: 0x0200074D RID: 1869
		[Token(Token = "0x2000E6C")]
		public class GachaDataBase
		{
			// Token: 0x06001C01 RID: 7169 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F2A")]
			[Address(RVA = "0x1012070B8", Offset = "0x12070B8", VA = "0x1012070B8")]
			public GachaDataBase()
			{
			}

			// Token: 0x04002BA7 RID: 11175
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005BF7")]
			public int m_ID;

			// Token: 0x04002BA8 RID: 11176
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005BF8")]
			public string m_Name;

			// Token: 0x04002BA9 RID: 11177
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005BF9")]
			public string m_BannerID;

			// Token: 0x04002BAA RID: 11178
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005BFA")]
			public string m_WebViewUrl;

			// Token: 0x04002BAB RID: 11179
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005BFB")]
			public DateTime m_StartAt;

			// Token: 0x04002BAC RID: 11180
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4005BFC")]
			public DateTime m_EndAt;

			// Token: 0x04002BAD RID: 11181
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005BFD")]
			public DateTime m_DispStartAt;

			// Token: 0x04002BAE RID: 11182
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4005BFE")]
			public DateTime m_DispEndAt;

			// Token: 0x04002BAF RID: 11183
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4005BFF")]
			public int m_Sort;

			// Token: 0x04002BB0 RID: 11184
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x4005C00")]
			public int m_FreeType;

			// Token: 0x04002BB1 RID: 11185
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4005C01")]
			public int m_PermitPlayType;
		}

		// Token: 0x0200074E RID: 1870
		[Token(Token = "0x2000E6D")]
		public class GachaData : Gacha.GachaDataBase
		{
			// Token: 0x1700021E RID: 542
			// (get) Token: 0x06001C02 RID: 7170 RVA: 0x0000C7F8 File Offset: 0x0000A9F8
			[Token(Token = "0x17000692")]
			public bool IsStepup
			{
				[Token(Token = "0x6005F2B")]
				[Address(RVA = "0x101206AA4", Offset = "0x1206AA4", VA = "0x101206AA4")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x1700021F RID: 543
			// (get) Token: 0x06001C03 RID: 7171 RVA: 0x0000C810 File Offset: 0x0000AA10
			[Token(Token = "0x17000693")]
			public bool IsFreeDraw
			{
				[Token(Token = "0x6005F2C")]
				[Address(RVA = "0x101206FD8", Offset = "0x1206FD8", VA = "0x101206FD8")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x17000220 RID: 544
			// (get) Token: 0x06001C04 RID: 7172 RVA: 0x0000C828 File Offset: 0x0000AA28
			[Token(Token = "0x17000694")]
			public int GachaRateTicketID
			{
				[Token(Token = "0x6005F2D")]
				[Address(RVA = "0x101206FE8", Offset = "0x1206FE8", VA = "0x101206FE8")]
				get
				{
					return 0;
				}
			}

			// Token: 0x17000221 RID: 545
			// (get) Token: 0x06001C05 RID: 7173 RVA: 0x0000C840 File Offset: 0x0000AA40
			[Token(Token = "0x17000695")]
			public int GachaReDrawItemID
			{
				[Token(Token = "0x6005F2E")]
				[Address(RVA = "0x101206FF0", Offset = "0x1206FF0", VA = "0x101206FF0")]
				get
				{
					return 0;
				}
			}

			// Token: 0x17000222 RID: 546
			// (get) Token: 0x06001C06 RID: 7174 RVA: 0x0000C858 File Offset: 0x0000AA58
			[Token(Token = "0x17000696")]
			public int PlayerDrawPoint
			{
				[Token(Token = "0x6005F2F")]
				[Address(RVA = "0x101206FF8", Offset = "0x1206FF8", VA = "0x101206FF8")]
				get
				{
					return 0;
				}
			}

			// Token: 0x06001C07 RID: 7175 RVA: 0x0000C870 File Offset: 0x0000AA70
			[Token(Token = "0x6005F30")]
			[Address(RVA = "0x101207000", Offset = "0x1207000", VA = "0x101207000")]
			public bool CheckFreeDraw(GachaDefine.ePlayType playType)
			{
				return default(bool);
			}

			// Token: 0x06001C08 RID: 7176 RVA: 0x0000C888 File Offset: 0x0000AA88
			[Token(Token = "0x6005F31")]
			[Address(RVA = "0x101207024", Offset = "0x1207024", VA = "0x101207024")]
			public bool CanUseGachaRateTicket()
			{
				return default(bool);
			}

			// Token: 0x06001C09 RID: 7177 RVA: 0x0000C8A0 File Offset: 0x0000AAA0
			[Token(Token = "0x6005F32")]
			[Address(RVA = "0x101207034", Offset = "0x1207034", VA = "0x101207034")]
			public bool CanUseReDrawItem()
			{
				return default(bool);
			}

			// Token: 0x06001C0A RID: 7178 RVA: 0x0000C8B8 File Offset: 0x0000AAB8
			[Token(Token = "0x6005F33")]
			[Address(RVA = "0x101207044", Offset = "0x1207044", VA = "0x101207044")]
			public int GetDrawPointOneTime()
			{
				return 0;
			}

			// Token: 0x06001C0B RID: 7179 RVA: 0x0000C8D0 File Offset: 0x0000AAD0
			[Token(Token = "0x6005F34")]
			[Address(RVA = "0x10120709C", Offset = "0x120709C", VA = "0x10120709C")]
			public bool CanUseDrawPoint()
			{
				return default(bool);
			}

			// Token: 0x06001C0C RID: 7180 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F35")]
			[Address(RVA = "0x101205098", Offset = "0x1205098", VA = "0x101205098")]
			public GachaData()
			{
			}

			// Token: 0x04002BB2 RID: 11186
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x4005C02")]
			public eGachaType m_Type;

			// Token: 0x04002BB3 RID: 11187
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4005C03")]
			public int m_Cost_Gem1;

			// Token: 0x04002BB4 RID: 11188
			[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
			[Token(Token = "0x4005C04")]
			public bool m_Cost_UnlimitedGem1Flag;

			// Token: 0x04002BB5 RID: 11189
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4005C05")]
			public int m_Cost_Gem10;

			// Token: 0x04002BB6 RID: 11190
			[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
			[Token(Token = "0x4005C06")]
			public int m_Cost_First10;

			// Token: 0x04002BB7 RID: 11191
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4005C07")]
			public bool m_Cost_UnlimitedGem10Flag;

			// Token: 0x04002BB8 RID: 11192
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x4005C08")]
			public int m_Cost_UnlimitedGem1;

			// Token: 0x04002BB9 RID: 11193
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x4005C09")]
			public int m_Cost_ItemID;

			// Token: 0x04002BBA RID: 11194
			[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
			[Token(Token = "0x4005C0A")]
			public int m_Cost_ItemAmount;

			// Token: 0x04002BBB RID: 11195
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x4005C0B")]
			public int m_PlayNum_Gem1Total;

			// Token: 0x04002BBC RID: 11196
			[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
			[Token(Token = "0x4005C0C")]
			public int m_PlayNum_Gem1Daily;

			// Token: 0x04002BBD RID: 11197
			[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
			[Token(Token = "0x4005C0D")]
			public int m_PlayNum_Gem10Total;

			// Token: 0x04002BBE RID: 11198
			[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
			[Token(Token = "0x4005C0E")]
			public int m_PlayNum_Gem10Daily;

			// Token: 0x04002BBF RID: 11199
			[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
			[Token(Token = "0x4005C0F")]
			public int m_PlayNum_UnlimitedGem1Total;

			// Token: 0x04002BC0 RID: 11200
			[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
			[Token(Token = "0x4005C10")]
			public int m_PlayNum_UnlimitedGem1Daily;

			// Token: 0x04002BC1 RID: 11201
			[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
			[Token(Token = "0x4005C11")]
			public int m_PlayNum_Item1Total;

			// Token: 0x04002BC2 RID: 11202
			[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
			[Token(Token = "0x4005C12")]
			public int m_PlayNum_Item1Daily;

			// Token: 0x04002BC3 RID: 11203
			[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
			[Token(Token = "0x4005C13")]
			public int m_DrawNum_UnlimitedGem1;

			// Token: 0x04002BC4 RID: 11204
			[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
			[Token(Token = "0x4005C14")]
			public int[] m_SelectionCharaIDs;

			// Token: 0x04002BC5 RID: 11205
			[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
			[Token(Token = "0x4005C15")]
			public bool m_HasPeriod;

			// Token: 0x04002BC6 RID: 11206
			[Cpp2IlInjected.FieldOffset(Offset = "0xB1")]
			[Token(Token = "0x4005C16")]
			public bool m_HighRarity;

			// Token: 0x04002BC7 RID: 11207
			[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
			[Token(Token = "0x4005C17")]
			public int m_StepCurrent;

			// Token: 0x04002BC8 RID: 11208
			[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
			[Token(Token = "0x4005C18")]
			public int m_StepMax;

			// Token: 0x04002BC9 RID: 11209
			[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
			[Token(Token = "0x4005C19")]
			public Gacha.GachaStepData[] m_Steps;

			// Token: 0x04002BCA RID: 11210
			[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
			[Token(Token = "0x4005C1A")]
			public int m_GachaRateTicketID;

			// Token: 0x04002BCB RID: 11211
			[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
			[Token(Token = "0x4005C1B")]
			public int m_GachaReDrawItemID;

			// Token: 0x04002BCC RID: 11212
			[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
			[Token(Token = "0x4005C1C")]
			public int m_PlayerDrawPoint;

			// Token: 0x04002BCD RID: 11213
			[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
			[Token(Token = "0x4005C1D")]
			public Gacha.DrawPointReward[] m_DrawPointReward;
		}

		// Token: 0x0200074F RID: 1871
		[Token(Token = "0x2000E6E")]
		public class GachaStepData
		{
			// Token: 0x06001C0D RID: 7181 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F36")]
			[Address(RVA = "0x10120516C", Offset = "0x120516C", VA = "0x10120516C")]
			public GachaStepData()
			{
			}

			// Token: 0x04002BCE RID: 11214
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C1E")]
			public int m_Step;

			// Token: 0x04002BCF RID: 11215
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005C1F")]
			public List<UserItemData> m_BonusList;
		}

		// Token: 0x02000750 RID: 1872
		[Token(Token = "0x2000E6F")]
		public class GachaDailyFrees
		{
			// Token: 0x06001C0E RID: 7182 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F37")]
			[Address(RVA = "0x101205058", Offset = "0x1205058", VA = "0x101205058")]
			public GachaDailyFrees()
			{
			}

			// Token: 0x04002BD0 RID: 11216
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C20")]
			public int m_DrawType;

			// Token: 0x04002BD1 RID: 11217
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005C21")]
			public Gacha.GachaFreePeriod m_Period;

			// Token: 0x04002BD2 RID: 11218
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005C22")]
			public int m_DrawRemain;
		}

		// Token: 0x02000751 RID: 1873
		[Token(Token = "0x2000E70")]
		public class GachaFreePeriod
		{
			// Token: 0x06001C0F RID: 7183 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F38")]
			[Address(RVA = "0x1012070C0", Offset = "0x12070C0", VA = "0x1012070C0")]
			private GachaFreePeriod()
			{
			}

			// Token: 0x06001C10 RID: 7184 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F39")]
			[Address(RVA = "0x101205060", Offset = "0x1205060", VA = "0x101205060")]
			public GachaFreePeriod(DateTime startAt, DateTime endAt)
			{
			}

			// Token: 0x04002BD3 RID: 11219
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C23")]
			public DateTime m_StartAt;

			// Token: 0x04002BD4 RID: 11220
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005C24")]
			public DateTime m_EndAt;
		}

		// Token: 0x02000752 RID: 1874
		[Token(Token = "0x2000E71")]
		private class DrawPointDelta
		{
			// Token: 0x06001C11 RID: 7185 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3A")]
			[Address(RVA = "0x101205DEC", Offset = "0x1205DEC", VA = "0x101205DEC")]
			public DrawPointDelta()
			{
			}

			// Token: 0x04002BD5 RID: 11221
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C25")]
			public int m_Before;

			// Token: 0x04002BD6 RID: 11222
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C26")]
			public int m_After;
		}

		// Token: 0x02000753 RID: 1875
		[Token(Token = "0x2000E72")]
		public class DrawPointReward
		{
			// Token: 0x06001C12 RID: 7186 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3B")]
			[Address(RVA = "0x101205174", Offset = "0x1205174", VA = "0x101205174")]
			public DrawPointReward()
			{
			}

			// Token: 0x04002BD7 RID: 11223
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C27")]
			public int m_Point;

			// Token: 0x04002BD8 RID: 11224
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C28")]
			public int m_CharacterId;
		}

		// Token: 0x02000754 RID: 1876
		[Token(Token = "0x2000E73")]
		public class FinishedGacha
		{
			// Token: 0x06001C13 RID: 7187 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3C")]
			[Address(RVA = "0x10120517C", Offset = "0x120517C", VA = "0x10120517C")]
			public FinishedGacha()
			{
			}

			// Token: 0x04002BD9 RID: 11225
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C29")]
			public int m_GachaId;

			// Token: 0x04002BDA RID: 11226
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C2A")]
			public int m_Point;
		}

		// Token: 0x02000755 RID: 1877
		[Token(Token = "0x2000E74")]
		public class Result
		{
			// Token: 0x06001C14 RID: 7188 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3D")]
			[Address(RVA = "0x101205D64", Offset = "0x1205D64", VA = "0x101205D64")]
			public Result(int charaID, int itemID, int itemNum)
			{
			}

			// Token: 0x04002BDB RID: 11227
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C2B")]
			public int m_CharaID;

			// Token: 0x04002BDC RID: 11228
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C2C")]
			public int m_ItemID;

			// Token: 0x04002BDD RID: 11229
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005C2D")]
			public int m_ItemNum;
		}

		// Token: 0x02000756 RID: 1878
		[Token(Token = "0x2000E75")]
		public class BonusItem
		{
			// Token: 0x06001C15 RID: 7189 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3E")]
			[Address(RVA = "0x101205DAC", Offset = "0x1205DAC", VA = "0x101205DAC")]
			public BonusItem(int itemID, int itemNum)
			{
			}

			// Token: 0x04002BDE RID: 11230
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C2E")]
			public int m_ID;

			// Token: 0x04002BDF RID: 11231
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C2F")]
			public int m_Num;
		}
	}
}
