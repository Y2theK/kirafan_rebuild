﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000550 RID: 1360
	[Token(Token = "0x2000443")]
	[StructLayout(3)]
	public class GachaCutInListDB : ScriptableObject
	{
		// Token: 0x060015D5 RID: 5589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001485")]
		[Address(RVA = "0x1012070C8", Offset = "0x12070C8", VA = "0x1012070C8")]
		public GachaCutInListDB()
		{
		}

		// Token: 0x040018CE RID: 6350
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001238")]
		public GachaCutInListDB_Param[] m_Params;
	}
}
