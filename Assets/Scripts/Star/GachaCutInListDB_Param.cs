﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200054F RID: 1359
	[Token(Token = "0x2000442")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct GachaCutInListDB_Param
	{
		// Token: 0x040018CA RID: 6346
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001234")]
		public int m_TitleType;

		// Token: 0x040018CB RID: 6347
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001235")]
		public string m_CharaID;

		// Token: 0x040018CC RID: 6348
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001236")]
		public string m_CutInText;

		// Token: 0x040018CD RID: 6349
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001237")]
		public string m_CutInVoiceCueName;
	}
}
