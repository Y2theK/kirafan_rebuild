﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000757 RID: 1879
	[Token(Token = "0x20005B2")]
	[StructLayout(3)]
	public static class GachaDefine
	{
		// Token: 0x04002BE0 RID: 11232
		[Token(Token = "0x4002326")]
		public static int DRAW_POINT_CONVERT_ITEM_ID;

		// Token: 0x02000758 RID: 1880
		[Token(Token = "0x2000E76")]
		public enum eCostType
		{
			// Token: 0x04002BE2 RID: 11234
			[Token(Token = "0x4005C31")]
			Invalid = -1,
			// Token: 0x04002BE3 RID: 11235
			[Token(Token = "0x4005C32")]
			Free,
			// Token: 0x04002BE4 RID: 11236
			[Token(Token = "0x4005C33")]
			Consume = 2
		}

		// Token: 0x02000759 RID: 1881
		[Token(Token = "0x2000E77")]
		public enum ePlayType
		{
			// Token: 0x04002BE6 RID: 11238
			[Token(Token = "0x4005C35")]
			Unlimited = 1,
			// Token: 0x04002BE7 RID: 11239
			[Token(Token = "0x4005C36")]
			Gem1,
			// Token: 0x04002BE8 RID: 11240
			[Token(Token = "0x4005C37")]
			Gem10,
			// Token: 0x04002BE9 RID: 11241
			[Token(Token = "0x4005C38")]
			Item,
			// Token: 0x04002BEA RID: 11242
			[Token(Token = "0x4005C39")]
			DrawPoint
		}

		// Token: 0x0200075A RID: 1882
		[Token(Token = "0x2000E78")]
		public enum eFreeDrawType
		{
			// Token: 0x04002BEC RID: 11244
			[Token(Token = "0x4005C3B")]
			None,
			// Token: 0x04002BED RID: 11245
			[Token(Token = "0x4005C3C")]
			Free1,
			// Token: 0x04002BEE RID: 11246
			[Token(Token = "0x4005C3D")]
			Free10
		}

		// Token: 0x0200075B RID: 1883
		[Token(Token = "0x2000E79")]
		public enum eReturnGachaPlay
		{
			// Token: 0x04002BF0 RID: 11248
			[Token(Token = "0x4005C3F")]
			Success,
			// Token: 0x04002BF1 RID: 11249
			[Token(Token = "0x4005C40")]
			GachaOutOfPeriod,
			// Token: 0x04002BF2 RID: 11250
			[Token(Token = "0x4005C41")]
			GachaDrawLimit,
			// Token: 0x04002BF3 RID: 11251
			[Token(Token = "0x4005C42")]
			GemIsShort,
			// Token: 0x04002BF4 RID: 11252
			[Token(Token = "0x4005C43")]
			UnlimitedGemIsShort,
			// Token: 0x04002BF5 RID: 11253
			[Token(Token = "0x4005C44")]
			ItemIsShort,
			// Token: 0x04002BF6 RID: 11254
			[Token(Token = "0x4005C45")]
			ItemIsExpired,
			// Token: 0x04002BF7 RID: 11255
			[Token(Token = "0x4005C46")]
			GachaStepDrawLimit,
			// Token: 0x04002BF8 RID: 11256
			[Token(Token = "0x4005C47")]
			NotFreeDraw,
			// Token: 0x04002BF9 RID: 11257
			[Token(Token = "0x4005C48")]
			PossibleFreeDraw,
			// Token: 0x04002BFA RID: 11258
			[Token(Token = "0x4005C49")]
			Unknown
		}

		// Token: 0x0200075C RID: 1884
		[Token(Token = "0x2000E7A")]
		public enum eCheckPlay
		{
			// Token: 0x04002BFC RID: 11260
			[Token(Token = "0x4005C4B")]
			Ok,
			// Token: 0x04002BFD RID: 11261
			[Token(Token = "0x4005C4C")]
			GemIsShort,
			// Token: 0x04002BFE RID: 11262
			[Token(Token = "0x4005C4D")]
			ItemIsShort
		}

		// Token: 0x0200075D RID: 1885
		[Token(Token = "0x2000E7B")]
		public enum eReturnGachaBox
		{
			// Token: 0x04002C00 RID: 11264
			[Token(Token = "0x4005C4F")]
			Success,
			// Token: 0x04002C01 RID: 11265
			[Token(Token = "0x4005C50")]
			GachaOutOfPeriod,
			// Token: 0x04002C02 RID: 11266
			[Token(Token = "0x4005C51")]
			Unknown
		}
	}
}
