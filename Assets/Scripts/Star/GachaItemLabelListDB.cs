﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000552 RID: 1362
	[Token(Token = "0x2000445")]
	[StructLayout(3)]
	public class GachaItemLabelListDB : ScriptableObject
	{
		// Token: 0x060015D6 RID: 5590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001486")]
		[Address(RVA = "0x101207124", Offset = "0x1207124", VA = "0x101207124")]
		public GachaItemLabelListDB()
		{
		}

		// Token: 0x040018D2 RID: 6354
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400123C")]
		public GachaItemLabelListDB_Param[] m_Params;
	}
}
