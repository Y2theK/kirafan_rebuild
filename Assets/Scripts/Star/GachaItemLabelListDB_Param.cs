﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000551 RID: 1361
	[Token(Token = "0x2000444")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct GachaItemLabelListDB_Param
	{
		// Token: 0x040018CF RID: 6351
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001239")]
		public int m_ItemID;

		// Token: 0x040018D0 RID: 6352
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400123A")]
		public string m_ResourcePath0;

		// Token: 0x040018D1 RID: 6353
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400123B")]
		public string m_ResourcePath1;
	}
}
