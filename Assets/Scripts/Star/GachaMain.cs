﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007B7 RID: 1975
	[Token(Token = "0x20005DF")]
	[StructLayout(3)]
	public class GachaMain : GameStateMain
	{
		// Token: 0x06001E30 RID: 7728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA8")]
		[Address(RVA = "0x10120712C", Offset = "0x120712C", VA = "0x10120712C")]
		private void Start()
		{
		}

		// Token: 0x06001E31 RID: 7729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA9")]
		[Address(RVA = "0x10120713C", Offset = "0x120713C", VA = "0x10120713C", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001E32 RID: 7730 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001BAA")]
		[Address(RVA = "0x10120720C", Offset = "0x120720C", VA = "0x10120720C", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001E33 RID: 7731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BAB")]
		[Address(RVA = "0x101207388", Offset = "0x1207388", VA = "0x101207388", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001E34 RID: 7732 RVA: 0x0000D6E0 File Offset: 0x0000B8E0
		[Token(Token = "0x6001BAC")]
		[Address(RVA = "0x101207404", Offset = "0x1207404", VA = "0x101207404", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001E35 RID: 7733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BAD")]
		[Address(RVA = "0x10120740C", Offset = "0x120740C", VA = "0x10120740C")]
		public GachaMain()
		{
		}

		// Token: 0x04002E8A RID: 11914
		[Token(Token = "0x4002417")]
		public const int STATE_INIT = 0;

		// Token: 0x04002E8B RID: 11915
		[Token(Token = "0x4002418")]
		public const int STATE_MAIN = 1;
	}
}
