﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200075E RID: 1886
	[Token(Token = "0x20005B3")]
	[StructLayout(3)]
	public class GachaObjectHandler : MonoBehaviour
	{
		// Token: 0x06001C17 RID: 7191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001999")]
		[Address(RVA = "0x10120743C", Offset = "0x120743C", VA = "0x10120743C")]
		private void Update()
		{
		}

		// Token: 0x06001C18 RID: 7192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600199A")]
		[Address(RVA = "0x10120750C", Offset = "0x120750C", VA = "0x10120750C")]
		public void Setup(string resourceName, MeigeAnimClipHolder[] meigeAnimClipHolders)
		{
		}

		// Token: 0x06001C19 RID: 7193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600199B")]
		[Address(RVA = "0x101207834", Offset = "0x1207834", VA = "0x101207834")]
		public void Destroy()
		{
		}

		// Token: 0x06001C1A RID: 7194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600199C")]
		[Address(RVA = "0x10120797C", Offset = "0x120797C", VA = "0x10120797C")]
		public void SetMagicCircle(eClassType classType)
		{
		}

		// Token: 0x06001C1B RID: 7195 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600199D")]
		[Address(RVA = "0x101207A9C", Offset = "0x1207A9C", VA = "0x101207A9C")]
		private GameObject[] GetMagicCircleObject(eClassType classType)
		{
			return null;
		}

		// Token: 0x06001C1C RID: 7196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600199E")]
		[Address(RVA = "0x101207CF0", Offset = "0x1207CF0", VA = "0x101207CF0")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x06001C1D RID: 7197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600199F")]
		[Address(RVA = "0x1012078E0", Offset = "0x12078E0", VA = "0x1012078E0")]
		public void DetachCamera()
		{
		}

		// Token: 0x06001C1E RID: 7198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A0")]
		[Address(RVA = "0x101207DA4", Offset = "0x1207DA4", VA = "0x101207DA4")]
		public void ResetCamera()
		{
		}

		// Token: 0x06001C1F RID: 7199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A1")]
		[Address(RVA = "0x101207E1C", Offset = "0x1207E1C", VA = "0x101207E1C")]
		public void Play(GachaObjectHandler.eAnimType animType, float blendSec = 0f)
		{
		}

		// Token: 0x06001C20 RID: 7200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A2")]
		[Address(RVA = "0x101207FA0", Offset = "0x1207FA0", VA = "0x101207FA0")]
		public void Pause()
		{
		}

		// Token: 0x06001C21 RID: 7201 RVA: 0x0000C8E8 File Offset: 0x0000AAE8
		[Token(Token = "0x60019A3")]
		[Address(RVA = "0x10120803C", Offset = "0x120803C", VA = "0x10120803C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06001C22 RID: 7202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A4")]
		[Address(RVA = "0x1012080BC", Offset = "0x12080BC", VA = "0x1012080BC")]
		public void ResetMeshVisibility()
		{
		}

		// Token: 0x06001C23 RID: 7203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A5")]
		[Address(RVA = "0x1012080EC", Offset = "0x12080EC", VA = "0x1012080EC")]
		public void KillParticle()
		{
		}

		// Token: 0x06001C24 RID: 7204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A6")]
		[Address(RVA = "0x101208188", Offset = "0x1208188", VA = "0x101208188")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x06001C25 RID: 7205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A7")]
		[Address(RVA = "0x101207440", Offset = "0x1207440", VA = "0x101207440")]
		private void UpdateFade()
		{
		}

		// Token: 0x06001C26 RID: 7206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A8")]
		[Address(RVA = "0x101208328", Offset = "0x1208328", VA = "0x101208328")]
		public void ResetMeshColor()
		{
		}

		// Token: 0x06001C27 RID: 7207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019A9")]
		[Address(RVA = "0x101208354", Offset = "0x1208354", VA = "0x101208354")]
		private void SetMeshColor(Color color)
		{
		}

		// Token: 0x06001C28 RID: 7208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019AA")]
		[Address(RVA = "0x1012081C8", Offset = "0x12081C8", VA = "0x1012081C8")]
		private void SetMeshAlpha(float a)
		{
		}

		// Token: 0x06001C29 RID: 7209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019AB")]
		[Address(RVA = "0x10120849C", Offset = "0x120849C", VA = "0x10120849C")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x06001C2A RID: 7210 RVA: 0x0000C900 File Offset: 0x0000AB00
		[Token(Token = "0x60019AC")]
		[Address(RVA = "0x101208604", Offset = "0x1208604", VA = "0x101208604")]
		public float GetCurrentAnimationEndTime(GachaObjectHandler.eAnimType animType)
		{
			return 0f;
		}

		// Token: 0x06001C2B RID: 7211 RVA: 0x0000C918 File Offset: 0x0000AB18
		[Token(Token = "0x60019AD")]
		[Address(RVA = "0x1012086E0", Offset = "0x12086E0", VA = "0x1012086E0")]
		public float GetCurrentAnimationNowTime()
		{
			return 0f;
		}

		// Token: 0x06001C2C RID: 7212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019AE")]
		[Address(RVA = "0x10120870C", Offset = "0x120870C", VA = "0x10120870C")]
		public GachaObjectHandler()
		{
		}

		// Token: 0x04002C03 RID: 11267
		[Token(Token = "0x4002327")]
		public const int LV_NUM = 3;

		// Token: 0x04002C04 RID: 11268
		[Token(Token = "0x4002328")]
		private static readonly string[,] MAGIC_CIRCLE_PATHS;

		// Token: 0x04002C05 RID: 11269
		[Token(Token = "0x4002329")]
		private const string BG_OBJ_NAME = "gacha_com_bg_rgb";

		// Token: 0x04002C06 RID: 11270
		[Token(Token = "0x400232A")]
		private static readonly string[] ANIM_KEYS;

		// Token: 0x04002C07 RID: 11271
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400232B")]
		private Transform m_BaseTransform;

		// Token: 0x04002C08 RID: 11272
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400232C")]
		private Animation m_Anim;

		// Token: 0x04002C09 RID: 11273
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400232D")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04002C0A RID: 11274
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400232E")]
		private MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x04002C0B RID: 11275
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400232F")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04002C0C RID: 11276
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002330")]
		private float m_StartAlpha;

		// Token: 0x04002C0D RID: 11277
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002331")]
		private float m_EndAlpha;

		// Token: 0x04002C0E RID: 11278
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002332")]
		private float m_FadeTime;

		// Token: 0x04002C0F RID: 11279
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002333")]
		private float m_FadeTimer;

		// Token: 0x04002C10 RID: 11280
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002334")]
		private bool m_IsUpdateMeshColor;

		// Token: 0x0200075F RID: 1887
		[Token(Token = "0x2000E7C")]
		public enum eAnimType
		{
			// Token: 0x04002C12 RID: 11282
			[Token(Token = "0x4005C53")]
			Start,
			// Token: 0x04002C13 RID: 11283
			[Token(Token = "0x4005C54")]
			Branch_lv1,
			// Token: 0x04002C14 RID: 11284
			[Token(Token = "0x4005C55")]
			Branch_lv1_loop,
			// Token: 0x04002C15 RID: 11285
			[Token(Token = "0x4005C56")]
			Branch_lv2,
			// Token: 0x04002C16 RID: 11286
			[Token(Token = "0x4005C57")]
			Branch_lv2_loop,
			// Token: 0x04002C17 RID: 11287
			[Token(Token = "0x4005C58")]
			Branch_lv3,
			// Token: 0x04002C18 RID: 11288
			[Token(Token = "0x4005C59")]
			Branch_lv3_cutIn_loop,
			// Token: 0x04002C19 RID: 11289
			[Token(Token = "0x4005C5A")]
			Branch_lv3_loop,
			// Token: 0x04002C1A RID: 11290
			[Token(Token = "0x4005C5B")]
			Num
		}

		// Token: 0x02000760 RID: 1888
		[Token(Token = "0x2000E7D")]
		public enum eAnimEvent
		{
			// Token: 0x04002C1C RID: 11292
			[Token(Token = "0x4005C5D")]
			FadeOut = 1
		}
	}
}
