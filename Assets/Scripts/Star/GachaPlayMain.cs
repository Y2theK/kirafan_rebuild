﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007BD RID: 1981
	[Token(Token = "0x20005E3")]
	[StructLayout(3)]
	public class GachaPlayMain : GameStateMain
	{
		// Token: 0x1700023D RID: 573
		// (get) Token: 0x06001E66 RID: 7782 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001E67 RID: 7783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700021A")]
		public GachaPlayScene GachaPlayScene
		{
			[Token(Token = "0x6001BDE")]
			[Address(RVA = "0x101208F84", Offset = "0x1208F84", VA = "0x101208F84")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001BDF")]
			[Address(RVA = "0x101208F8C", Offset = "0x1208F8C", VA = "0x101208F8C")]
			set
			{
			}
		}

		// Token: 0x06001E68 RID: 7784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE0")]
		[Address(RVA = "0x101208F94", Offset = "0x1208F94", VA = "0x101208F94")]
		private void Start()
		{
		}

		// Token: 0x06001E69 RID: 7785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE1")]
		[Address(RVA = "0x101208FA0", Offset = "0x1208FA0", VA = "0x101208FA0")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001E6A RID: 7786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE2")]
		[Address(RVA = "0x101208FA8", Offset = "0x1208FA8", VA = "0x101208FA8", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001E6B RID: 7787 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001BE3")]
		[Address(RVA = "0x10120909C", Offset = "0x120909C", VA = "0x10120909C", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001E6C RID: 7788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE4")]
		[Address(RVA = "0x101209180", Offset = "0x1209180", VA = "0x101209180", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001E6D RID: 7789 RVA: 0x0000D818 File Offset: 0x0000BA18
		[Token(Token = "0x6001BE5")]
		[Address(RVA = "0x1012091DC", Offset = "0x12091DC", VA = "0x1012091DC", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001E6E RID: 7790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE6")]
		[Address(RVA = "0x101209230", Offset = "0x1209230", VA = "0x101209230")]
		public GachaPlayMain()
		{
		}

		// Token: 0x04002EBA RID: 11962
		[Token(Token = "0x4002435")]
		public const int STATE_MAIN = 1;

		// Token: 0x04002EBB RID: 11963
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002436")]
		private GachaPlayScene m_GachaPlayScene;
	}
}
