﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaPlay;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000761 RID: 1889
	[Token(Token = "0x20005B4")]
	[StructLayout(3)]
	public class GachaPlayScene
	{
		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06001C2E RID: 7214 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000202")]
		private Gacha.Result CurrentGachaResult
		{
			[Token(Token = "0x60019B0")]
			[Address(RVA = "0x101209248", Offset = "0x1209248", VA = "0x101209248")]
			get
			{
				return null;
			}
		}

		// Token: 0x06001C2F RID: 7215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019B1")]
		[Address(RVA = "0x1012092FC", Offset = "0x12092FC", VA = "0x1012092FC")]
		public GachaPlayScene()
		{
		}

		// Token: 0x06001C30 RID: 7216 RVA: 0x0000C930 File Offset: 0x0000AB30
		[Token(Token = "0x60019B2")]
		[Address(RVA = "0x101209B7C", Offset = "0x1209B7C", VA = "0x101209B7C")]
		public bool Prepare(List<Gacha.Result> gachaResults, List<Gacha.BonusItem> gachaBonusItems, Gacha.GachaData gachaData, Camera camera)
		{
			return default(bool);
		}

		// Token: 0x06001C31 RID: 7217 RVA: 0x0000C948 File Offset: 0x0000AB48
		[Token(Token = "0x60019B3")]
		[Address(RVA = "0x101209BA0", Offset = "0x1209BA0", VA = "0x101209BA0")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06001C32 RID: 7218 RVA: 0x0000C960 File Offset: 0x0000AB60
		[Token(Token = "0x60019B4")]
		[Address(RVA = "0x1012091AC", Offset = "0x12091AC", VA = "0x1012091AC")]
		public bool Destory()
		{
			return default(bool);
		}

		// Token: 0x06001C33 RID: 7219 RVA: 0x0000C978 File Offset: 0x0000AB78
		[Token(Token = "0x60019B5")]
		[Address(RVA = "0x10120920C", Offset = "0x120920C", VA = "0x10120920C")]
		public bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06001C34 RID: 7220 RVA: 0x0000C990 File Offset: 0x0000AB90
		[Token(Token = "0x60019B6")]
		[Address(RVA = "0x101209BE4", Offset = "0x1209BE4", VA = "0x101209BE4")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06001C35 RID: 7221 RVA: 0x0000C9A8 File Offset: 0x0000ABA8
		[Token(Token = "0x60019B7")]
		[Address(RVA = "0x101209BC0", Offset = "0x1209BC0", VA = "0x101209BC0")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06001C36 RID: 7222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019B8")]
		[Address(RVA = "0x101208FD4", Offset = "0x1208FD4", VA = "0x101208FD4")]
		public void Update()
		{
		}

		// Token: 0x06001C37 RID: 7223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019B9")]
		[Address(RVA = "0x10120C08C", Offset = "0x120C08C", VA = "0x10120C08C")]
		private void SetDestroyStep(GachaPlayScene.eDestroyStep step)
		{
		}

		// Token: 0x06001C38 RID: 7224 RVA: 0x0000C9C0 File Offset: 0x0000ABC0
		[Token(Token = "0x60019BA")]
		[Address(RVA = "0x10120BD88", Offset = "0x120BD88", VA = "0x10120BD88")]
		private GachaPlayScene.eMode Update_Destroy()
		{
			return GachaPlayScene.eMode.Prepare;
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06001C39 RID: 7225 RVA: 0x0000C9D8 File Offset: 0x0000ABD8
		[Token(Token = "0x17000203")]
		public bool IsRePlayed
		{
			[Token(Token = "0x60019BB")]
			[Address(RVA = "0x10120C1D0", Offset = "0x120C1D0", VA = "0x10120C1D0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06001C3A RID: 7226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019BC")]
		[Address(RVA = "0x10120C008", Offset = "0x120C008", VA = "0x10120C008")]
		private void SetMainStep(GachaPlayScene.eMainStep step)
		{
		}

		// Token: 0x06001C3B RID: 7227 RVA: 0x0000C9F0 File Offset: 0x0000ABF0
		[Token(Token = "0x60019BD")]
		[Address(RVA = "0x10120A634", Offset = "0x120A634", VA = "0x10120A634")]
		private GachaPlayScene.eMode Update_Main()
		{
			return GachaPlayScene.eMode.Prepare;
		}

		// Token: 0x06001C3C RID: 7228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019BE")]
		[Address(RVA = "0x10120C968", Offset = "0x120C968", VA = "0x10120C968")]
		private void OnClick()
		{
		}

		// Token: 0x06001C3D RID: 7229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019BF")]
		[Address(RVA = "0x10120CB90", Offset = "0x120CB90", VA = "0x10120CB90")]
		private void OnAllSkip()
		{
		}

		// Token: 0x06001C3E RID: 7230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C0")]
		[Address(RVA = "0x10120CC48", Offset = "0x120CC48", VA = "0x10120CC48")]
		private void OnCloseCharaFlavors()
		{
		}

		// Token: 0x06001C3F RID: 7231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C1")]
		[Address(RVA = "0x10120CD00", Offset = "0x120CD00", VA = "0x10120CD00")]
		private void OnClickCloseResult()
		{
		}

		// Token: 0x06001C40 RID: 7232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C2")]
		[Address(RVA = "0x10120CEA8", Offset = "0x120CEA8", VA = "0x10120CEA8")]
		private void OnResponseGachaFix(ResultCode resultCode)
		{
		}

		// Token: 0x06001C41 RID: 7233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C3")]
		[Address(RVA = "0x10120CEE0", Offset = "0x120CEE0", VA = "0x10120CEE0")]
		private void OnResponse_GachaRePlay(GachaDefine.eReturnGachaPlay ret, string errorMessage)
		{
		}

		// Token: 0x06001C42 RID: 7234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C4")]
		[Address(RVA = "0x10120CF94", Offset = "0x120CF94", VA = "0x10120CF94")]
		private void OnResponse_UpdateAfterTutorialGacha()
		{
		}

		// Token: 0x06001C43 RID: 7235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C5")]
		[Address(RVA = "0x10120CFC4", Offset = "0x120CFC4", VA = "0x10120CFC4")]
		private void OnClickRedraw()
		{
		}

		// Token: 0x06001C44 RID: 7236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C6")]
		[Address(RVA = "0x10120D4C4", Offset = "0x120D4C4", VA = "0x10120D4C4")]
		private void OnClickDecide_RedrawConfirmGroup()
		{
		}

		// Token: 0x06001C45 RID: 7237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C7")]
		[Address(RVA = "0x10120D5A0", Offset = "0x120D5A0", VA = "0x10120D5A0")]
		private void OnResponse_GachaReDraw(ResultCode resultCode)
		{
		}

		// Token: 0x06001C46 RID: 7238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C8")]
		[Address(RVA = "0x10120D61C", Offset = "0x120D61C", VA = "0x10120D61C")]
		private void OnCloseResult()
		{
		}

		// Token: 0x06001C47 RID: 7239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019C9")]
		[Address(RVA = "0x10120C55C", Offset = "0x120C55C", VA = "0x10120C55C")]
		private void OnCutIn()
		{
		}

		// Token: 0x06001C48 RID: 7240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019CA")]
		[Address(RVA = "0x10120D79C", Offset = "0x120D79C", VA = "0x10120D79C")]
		private void OnCharaIllust()
		{
		}

		// Token: 0x06001C49 RID: 7241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019CB")]
		[Address(RVA = "0x10120C1D8", Offset = "0x120C1D8", VA = "0x10120C1D8")]
		private void UpdateSound()
		{
		}

		// Token: 0x06001C4A RID: 7242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019CC")]
		[Address(RVA = "0x10120C094", Offset = "0x120C094", VA = "0x10120C094")]
		private void StopSound()
		{
		}

		// Token: 0x06001C4B RID: 7243 RVA: 0x0000CA08 File Offset: 0x0000AC08
		[Token(Token = "0x60019CD")]
		[Address(RVA = "0x10120C26C", Offset = "0x120C26C", VA = "0x10120C26C")]
		private GachaObjectHandler.eAnimType PlayBranchLoop()
		{
			return GachaObjectHandler.eAnimType.Start;
		}

		// Token: 0x06001C4C RID: 7244 RVA: 0x0000CA20 File Offset: 0x0000AC20
		[Token(Token = "0x60019CE")]
		[Address(RVA = "0x10120C424", Offset = "0x120C424", VA = "0x10120C424")]
		private GachaObjectHandler.eAnimType RareToAnimType(eRare rareType, bool isLoop)
		{
			return GachaObjectHandler.eAnimType.Start;
		}

		// Token: 0x06001C4D RID: 7245 RVA: 0x0000CA38 File Offset: 0x0000AC38
		[Token(Token = "0x60019CF")]
		[Address(RVA = "0x10120C458", Offset = "0x120C458", VA = "0x10120C458")]
		private int RareToLv(eRare rareType)
		{
			return 0;
		}

		// Token: 0x06001C4E RID: 7246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019D0")]
		[Address(RVA = "0x10120C000", Offset = "0x120C000", VA = "0x10120C000")]
		private void SetPrepareStep(GachaPlayScene.ePrepareStep step)
		{
		}

		// Token: 0x06001C4F RID: 7247 RVA: 0x0000CA50 File Offset: 0x0000AC50
		[Token(Token = "0x60019D1")]
		[Address(RVA = "0x101209C10", Offset = "0x1209C10", VA = "0x101209C10")]
		private GachaPlayScene.eMode Update_Prepare()
		{
			return GachaPlayScene.eMode.Prepare;
		}

		// Token: 0x04002C1D RID: 11293
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002335")]
		private GachaPlayScene.eMode m_Mode;

		// Token: 0x04002C1E RID: 11294
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002336")]
		private List<Gacha.Result> m_GachaResults;

		// Token: 0x04002C1F RID: 11295
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002337")]
		private List<Gacha.BonusItem> m_GachaBonusItems;

		// Token: 0x04002C20 RID: 11296
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002338")]
		private Gacha.GachaData m_GachaData;

		// Token: 0x04002C21 RID: 11297
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002339")]
		private Camera m_Camera;

		// Token: 0x04002C22 RID: 11298
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400233A")]
		private GameObject m_GachaObject;

		// Token: 0x04002C23 RID: 11299
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400233B")]
		private GachaObjectHandler m_GachaObjectHandler;

		// Token: 0x04002C24 RID: 11300
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400233C")]
		private GachaPlayUI m_UI;

		// Token: 0x04002C25 RID: 11301
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400233D")]
		private SimpleTimer m_Timer;

		// Token: 0x04002C26 RID: 11302
		[Token(Token = "0x400233E")]
		public const string VOICE_CLAIRE_CUE_SHEET = "Voice_Original_006";

		// Token: 0x04002C27 RID: 11303
		[Token(Token = "0x400233F")]
		private const string RESOURCE_PATH = "gacha/gacha.muast";

		// Token: 0x04002C28 RID: 11304
		[Token(Token = "0x4002340")]
		private const string MODEL_OBJECT_PATH = "Gacha";

		// Token: 0x04002C29 RID: 11305
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002341")]
		private readonly string[] ANIM_OBJECT_PATHS;

		// Token: 0x04002C2A RID: 11306
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002342")]
		private ABResourceLoader m_Loader;

		// Token: 0x04002C2B RID: 11307
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002343")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04002C2C RID: 11308
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002344")]
		private GachaPlayScene.eDestroyStep m_DestroyStep;

		// Token: 0x04002C2D RID: 11309
		[Token(Token = "0x4002345")]
		private const float GACHA_FADE_IN_SEC_ON_BRANCH_LOOP = 0.37f;

		// Token: 0x04002C2E RID: 11310
		[Token(Token = "0x4002346")]
		private const float GACHA_FADE_SEC = 0.4f;

		// Token: 0x04002C2F RID: 11311
		[Token(Token = "0x4002347")]
		private const float GACHA_FADE_WAIT_SEC = 0.2f;

		// Token: 0x04002C30 RID: 11312
		[Token(Token = "0x4002348")]
		private const float GACHA_FADE_OUT_TO_IN_SEC = 0.6f;

		// Token: 0x04002C31 RID: 11313
		[Token(Token = "0x4002349")]
		private const float GACHA_END_WAIT_SEC = 0.4f;

		// Token: 0x04002C32 RID: 11314
		[Token(Token = "0x400234A")]
		private const float SKIP_FADE_SEC = 0.3f;

		// Token: 0x04002C33 RID: 11315
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x400234B")]
		private int m_PlayIndex;

		// Token: 0x04002C34 RID: 11316
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400234C")]
		private CharacterListDB_Param m_CharaListParam;

		// Token: 0x04002C35 RID: 11317
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x400234D")]
		private GachaPlayScene.ePlayLoopStep m_PlayLoopStep;

		// Token: 0x04002C36 RID: 11318
		[Cpp2IlInjected.FieldOffset(Offset = "0x114")]
		[Token(Token = "0x400234E")]
		private bool m_IsOpenedCutIn;

		// Token: 0x04002C37 RID: 11319
		[Cpp2IlInjected.FieldOffset(Offset = "0x115")]
		[Token(Token = "0x400234F")]
		private bool m_IsPlayedCutInLoop;

		// Token: 0x04002C38 RID: 11320
		[Cpp2IlInjected.FieldOffset(Offset = "0x116")]
		[Token(Token = "0x4002350")]
		private bool m_SingleGachaPlaySkipped;

		// Token: 0x04002C39 RID: 11321
		[Cpp2IlInjected.FieldOffset(Offset = "0x117")]
		[Token(Token = "0x4002351")]
		private bool m_AllGachaPlaySkipped;

		// Token: 0x04002C3A RID: 11322
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4002352")]
		private bool m_SingleGachaPlaySkippExecuted;

		// Token: 0x04002C3B RID: 11323
		[Cpp2IlInjected.FieldOffset(Offset = "0x119")]
		[Token(Token = "0x4002353")]
		private bool m_AllGachaPlaySkippExecuted;

		// Token: 0x04002C3C RID: 11324
		[Cpp2IlInjected.FieldOffset(Offset = "0x11A")]
		[Token(Token = "0x4002354")]
		private bool m_IsRePlayed;

		// Token: 0x04002C3D RID: 11325
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4002355")]
		private ArousalLvUp m_ArousalLvUp;

		// Token: 0x04002C3E RID: 11326
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4002356")]
		private GachaPlayScene.GachaSound[] m_GachaSounds;

		// Token: 0x04002C3F RID: 11327
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002357")]
		private GachaPlayScene.eMainStep m_MainStep;

		// Token: 0x04002C40 RID: 11328
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4002358")]
		private GachaPlayScene.ePrepareStep m_PrepareStep;

		// Token: 0x02000762 RID: 1890
		[Token(Token = "0x2000E7E")]
		public enum eMode
		{
			// Token: 0x04002C42 RID: 11330
			[Token(Token = "0x4005C5F")]
			None = -1,
			// Token: 0x04002C43 RID: 11331
			[Token(Token = "0x4005C60")]
			Prepare,
			// Token: 0x04002C44 RID: 11332
			[Token(Token = "0x4005C61")]
			UpdateMain,
			// Token: 0x04002C45 RID: 11333
			[Token(Token = "0x4005C62")]
			Destroy
		}

		// Token: 0x02000763 RID: 1891
		[Token(Token = "0x2000E7F")]
		public enum eGachaBranch
		{
			// Token: 0x04002C47 RID: 11335
			[Token(Token = "0x4005C64")]
			Lv1,
			// Token: 0x04002C48 RID: 11336
			[Token(Token = "0x4005C65")]
			Lv2,
			// Token: 0x04002C49 RID: 11337
			[Token(Token = "0x4005C66")]
			Lv3,
			// Token: 0x04002C4A RID: 11338
			[Token(Token = "0x4005C67")]
			Num
		}

		// Token: 0x02000764 RID: 1892
		[Token(Token = "0x2000E80")]
		private enum eDestroyStep
		{
			// Token: 0x04002C4C RID: 11340
			[Token(Token = "0x4005C69")]
			None = -1,
			// Token: 0x04002C4D RID: 11341
			[Token(Token = "0x4005C6A")]
			First,
			// Token: 0x04002C4E RID: 11342
			[Token(Token = "0x4005C6B")]
			Transit_Wait,
			// Token: 0x04002C4F RID: 11343
			[Token(Token = "0x4005C6C")]
			Destroy_0,
			// Token: 0x04002C50 RID: 11344
			[Token(Token = "0x4005C6D")]
			Destroy_1,
			// Token: 0x04002C51 RID: 11345
			[Token(Token = "0x4005C6E")]
			Destroy_2,
			// Token: 0x04002C52 RID: 11346
			[Token(Token = "0x4005C6F")]
			Destroy_3,
			// Token: 0x04002C53 RID: 11347
			[Token(Token = "0x4005C70")]
			Destroy_4,
			// Token: 0x04002C54 RID: 11348
			[Token(Token = "0x4005C71")]
			Destroy_5,
			// Token: 0x04002C55 RID: 11349
			[Token(Token = "0x4005C72")]
			End
		}

		// Token: 0x02000765 RID: 1893
		[Token(Token = "0x2000E81")]
		private enum ePlayLoopStep
		{
			// Token: 0x04002C57 RID: 11351
			[Token(Token = "0x4005C74")]
			Chara,
			// Token: 0x04002C58 RID: 11352
			[Token(Token = "0x4005C75")]
			Item
		}

		// Token: 0x02000766 RID: 1894
		[Token(Token = "0x2000E82")]
		public class GachaSound
		{
			// Token: 0x06001C50 RID: 7248 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F3F")]
			[Address(RVA = "0x101209B40", Offset = "0x1209B40", VA = "0x101209B40")]
			public GachaSound(eSoundSeListDB cueID, float delay)
			{
			}

			// Token: 0x06001C51 RID: 7249 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F40")]
			[Address(RVA = "0x10120C8CC", Offset = "0x120C8CC", VA = "0x10120C8CC")]
			public void Stop()
			{
			}

			// Token: 0x06001C52 RID: 7250 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F41")]
			[Address(RVA = "0x10120C474", Offset = "0x120C474", VA = "0x10120C474")]
			public void Play()
			{
			}

			// Token: 0x06001C53 RID: 7251 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F42")]
			[Address(RVA = "0x10120D7D8", Offset = "0x120D7D8", VA = "0x10120D7D8")]
			public void Update()
			{
			}

			// Token: 0x04002C59 RID: 11353
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005C76")]
			public eSoundSeListDB m_CueID;

			// Token: 0x04002C5A RID: 11354
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005C77")]
			public float m_Delay;

			// Token: 0x04002C5B RID: 11355
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005C78")]
			public float m_Timer;

			// Token: 0x04002C5C RID: 11356
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005C79")]
			public int m_RequestPlay;

			// Token: 0x04002C5D RID: 11357
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005C7A")]
			public SoundHandler m_SoundHndl;
		}

		// Token: 0x02000767 RID: 1895
		[Token(Token = "0x2000E83")]
		private enum eGachaSound
		{
			// Token: 0x04002C5F RID: 11359
			[Token(Token = "0x4005C7C")]
			PLAY_START,
			// Token: 0x04002C60 RID: 11360
			[Token(Token = "0x4005C7D")]
			PLAY_START_RIPPLE,
			// Token: 0x04002C61 RID: 11361
			[Token(Token = "0x4005C7E")]
			BRANCH_LV1,
			// Token: 0x04002C62 RID: 11362
			[Token(Token = "0x4005C7F")]
			FINISH_LV1,
			// Token: 0x04002C63 RID: 11363
			[Token(Token = "0x4005C80")]
			BRANCH_LV2,
			// Token: 0x04002C64 RID: 11364
			[Token(Token = "0x4005C81")]
			FINISH_LV2,
			// Token: 0x04002C65 RID: 11365
			[Token(Token = "0x4005C82")]
			BRANCH_LV3,
			// Token: 0x04002C66 RID: 11366
			[Token(Token = "0x4005C83")]
			BRANCH_LOOP_LV3,
			// Token: 0x04002C67 RID: 11367
			[Token(Token = "0x4005C84")]
			APPEAR_CUTIN_PREV,
			// Token: 0x04002C68 RID: 11368
			[Token(Token = "0x4005C85")]
			APPEAR_CUTIN,
			// Token: 0x04002C69 RID: 11369
			[Token(Token = "0x4005C86")]
			FINISH_LV3,
			// Token: 0x04002C6A RID: 11370
			[Token(Token = "0x4005C87")]
			FINISH_LOOP,
			// Token: 0x04002C6B RID: 11371
			[Token(Token = "0x4005C88")]
			Num
		}

		// Token: 0x02000768 RID: 1896
		[Token(Token = "0x2000E84")]
		private enum eMainStep
		{
			// Token: 0x04002C6D RID: 11373
			[Token(Token = "0x4005C8A")]
			None = -1,
			// Token: 0x04002C6E RID: 11374
			[Token(Token = "0x4005C8B")]
			First,
			// Token: 0x04002C6F RID: 11375
			[Token(Token = "0x4005C8C")]
			LoadCharaIllust,
			// Token: 0x04002C70 RID: 11376
			[Token(Token = "0x4005C8D")]
			LoadCharaIllust_Wait,
			// Token: 0x04002C71 RID: 11377
			[Token(Token = "0x4005C8E")]
			FirstFadeIn_Wait,
			// Token: 0x04002C72 RID: 11378
			[Token(Token = "0x4005C8F")]
			TapToStart,
			// Token: 0x04002C73 RID: 11379
			[Token(Token = "0x4005C90")]
			PlayStart,
			// Token: 0x04002C74 RID: 11380
			[Token(Token = "0x4005C91")]
			PlayStart_Wait,
			// Token: 0x04002C75 RID: 11381
			[Token(Token = "0x4005C92")]
			PlayBranch_Wait,
			// Token: 0x04002C76 RID: 11382
			[Token(Token = "0x4005C93")]
			PlayBranch_lv3_Wait_0,
			// Token: 0x04002C77 RID: 11383
			[Token(Token = "0x4005C94")]
			PlayBranch_lv3_Wait_1,
			// Token: 0x04002C78 RID: 11384
			[Token(Token = "0x4005C95")]
			PlayBranch_lv3_Wait_2,
			// Token: 0x04002C79 RID: 11385
			[Token(Token = "0x4005C96")]
			PlayBranch_lv3_Wait_3,
			// Token: 0x04002C7A RID: 11386
			[Token(Token = "0x4005C97")]
			PlayBranch_lv3_Wait_4,
			// Token: 0x04002C7B RID: 11387
			[Token(Token = "0x4005C98")]
			PlayLoop_Wait,
			// Token: 0x04002C7C RID: 11388
			[Token(Token = "0x4005C99")]
			CurrentEnd,
			// Token: 0x04002C7D RID: 11389
			[Token(Token = "0x4005C9A")]
			CurrentEnd_Wait_0,
			// Token: 0x04002C7E RID: 11390
			[Token(Token = "0x4005C9B")]
			CurrentEnd_Wait_1,
			// Token: 0x04002C7F RID: 11391
			[Token(Token = "0x4005C9C")]
			Flavor,
			// Token: 0x04002C80 RID: 11392
			[Token(Token = "0x4005C9D")]
			Flavor_Wait,
			// Token: 0x04002C81 RID: 11393
			[Token(Token = "0x4005C9E")]
			Result,
			// Token: 0x04002C82 RID: 11394
			[Token(Token = "0x4005C9F")]
			Result_Wait,
			// Token: 0x04002C83 RID: 11395
			[Token(Token = "0x4005CA0")]
			Arousal,
			// Token: 0x04002C84 RID: 11396
			[Token(Token = "0x4005CA1")]
			RePlayTransit_0,
			// Token: 0x04002C85 RID: 11397
			[Token(Token = "0x4005CA2")]
			RePlayTransit_1,
			// Token: 0x04002C86 RID: 11398
			[Token(Token = "0x4005CA3")]
			RePlayTransit_2,
			// Token: 0x04002C87 RID: 11399
			[Token(Token = "0x4005CA4")]
			AllSkip_0,
			// Token: 0x04002C88 RID: 11400
			[Token(Token = "0x4005CA5")]
			AllSkip_1,
			// Token: 0x04002C89 RID: 11401
			[Token(Token = "0x4005CA6")]
			AllSkip_2,
			// Token: 0x04002C8A RID: 11402
			[Token(Token = "0x4005CA7")]
			SingleSkip_0,
			// Token: 0x04002C8B RID: 11403
			[Token(Token = "0x4005CA8")]
			SingleSkip_1,
			// Token: 0x04002C8C RID: 11404
			[Token(Token = "0x4005CA9")]
			SingleSkip_2,
			// Token: 0x04002C8D RID: 11405
			[Token(Token = "0x4005CAA")]
			End
		}

		// Token: 0x02000769 RID: 1897
		[Token(Token = "0x2000E85")]
		private enum ePrepareStep
		{
			// Token: 0x04002C8F RID: 11407
			[Token(Token = "0x4005CAC")]
			None = -1,
			// Token: 0x04002C90 RID: 11408
			[Token(Token = "0x4005CAD")]
			First,
			// Token: 0x04002C91 RID: 11409
			[Token(Token = "0x4005CAE")]
			Prepare,
			// Token: 0x04002C92 RID: 11410
			[Token(Token = "0x4005CAF")]
			Prepare_Wait,
			// Token: 0x04002C93 RID: 11411
			[Token(Token = "0x4005CB0")]
			UIPrepare,
			// Token: 0x04002C94 RID: 11412
			[Token(Token = "0x4005CB1")]
			UIPrepare_Wait,
			// Token: 0x04002C95 RID: 11413
			[Token(Token = "0x4005CB2")]
			Setup,
			// Token: 0x04002C96 RID: 11414
			[Token(Token = "0x4005CB3")]
			CharaIllustDownload,
			// Token: 0x04002C97 RID: 11415
			[Token(Token = "0x4005CB4")]
			CharaIllustDownloadWait,
			// Token: 0x04002C98 RID: 11416
			[Token(Token = "0x4005CB5")]
			End,
			// Token: 0x04002C99 RID: 11417
			[Token(Token = "0x4005CB6")]
			PrepareError,
			// Token: 0x04002C9A RID: 11418
			[Token(Token = "0x4005CB7")]
			PrepareError_Wait
		}
	}
}
