﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007BE RID: 1982
	[Token(Token = "0x20005E4")]
	[StructLayout(3)]
	public class GachaPlayState : GameStateBase
	{
		// Token: 0x06001E6F RID: 7791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE7")]
		[Address(RVA = "0x10120DA94", Offset = "0x120DA94", VA = "0x10120DA94")]
		public GachaPlayState(GachaPlayMain owner)
		{
		}

		// Token: 0x06001E70 RID: 7792 RVA: 0x0000D830 File Offset: 0x0000BA30
		[Token(Token = "0x6001BE8")]
		[Address(RVA = "0x10120DAC8", Offset = "0x120DAC8", VA = "0x10120DAC8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E71 RID: 7793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BE9")]
		[Address(RVA = "0x10120DAD0", Offset = "0x120DAD0", VA = "0x10120DAD0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E72 RID: 7794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BEA")]
		[Address(RVA = "0x10120DAD4", Offset = "0x120DAD4", VA = "0x10120DAD4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E73 RID: 7795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BEB")]
		[Address(RVA = "0x10120DAD8", Offset = "0x120DAD8", VA = "0x10120DAD8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E74 RID: 7796 RVA: 0x0000D848 File Offset: 0x0000BA48
		[Token(Token = "0x6001BEC")]
		[Address(RVA = "0x10120DADC", Offset = "0x120DADC", VA = "0x10120DADC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001E75 RID: 7797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BED")]
		[Address(RVA = "0x10120DAE4", Offset = "0x120DAE4", VA = "0x10120DAE4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002EBC RID: 11964
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002437")]
		protected GachaPlayMain m_Owner;
	}
}
