﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007BF RID: 1983
	[Token(Token = "0x20005E5")]
	[StructLayout(3)]
	public class GachaPlayState_Main : GachaPlayState
	{
		// Token: 0x06001E76 RID: 7798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BEE")]
		[Address(RVA = "0x10120914C", Offset = "0x120914C", VA = "0x10120914C")]
		public GachaPlayState_Main(GachaPlayMain owner)
		{
		}

		// Token: 0x06001E77 RID: 7799 RVA: 0x0000D860 File Offset: 0x0000BA60
		[Token(Token = "0x6001BEF")]
		[Address(RVA = "0x10120DAE8", Offset = "0x120DAE8", VA = "0x10120DAE8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E78 RID: 7800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BF0")]
		[Address(RVA = "0x10120DAF0", Offset = "0x120DAF0", VA = "0x10120DAF0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E79 RID: 7801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BF1")]
		[Address(RVA = "0x10120DAF8", Offset = "0x120DAF8", VA = "0x10120DAF8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E7A RID: 7802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BF2")]
		[Address(RVA = "0x10120DAFC", Offset = "0x120DAFC", VA = "0x10120DAFC", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E7B RID: 7803 RVA: 0x0000D878 File Offset: 0x0000BA78
		[Token(Token = "0x6001BF3")]
		[Address(RVA = "0x10120DB00", Offset = "0x120DB00", VA = "0x10120DB00", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x04002EBD RID: 11965
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002438")]
		private GachaPlayState_Main.eStep m_Step;

		// Token: 0x020007C0 RID: 1984
		[Token(Token = "0x2000EAB")]
		private enum eStep
		{
			// Token: 0x04002EBF RID: 11967
			[Token(Token = "0x4005DFC")]
			None = -1,
			// Token: 0x04002EC0 RID: 11968
			[Token(Token = "0x4005DFD")]
			First,
			// Token: 0x04002EC1 RID: 11969
			[Token(Token = "0x4005DFE")]
			Prepare,
			// Token: 0x04002EC2 RID: 11970
			[Token(Token = "0x4005DFF")]
			Prepare_Wait,
			// Token: 0x04002EC3 RID: 11971
			[Token(Token = "0x4005E00")]
			Play,
			// Token: 0x04002EC4 RID: 11972
			[Token(Token = "0x4005E01")]
			Main,
			// Token: 0x04002EC5 RID: 11973
			[Token(Token = "0x4005E02")]
			Destory,
			// Token: 0x04002EC6 RID: 11974
			[Token(Token = "0x4005E03")]
			Destory_Wait,
			// Token: 0x04002EC7 RID: 11975
			[Token(Token = "0x4005E04")]
			End
		}
	}
}
