﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007B8 RID: 1976
	[Token(Token = "0x20005E0")]
	[StructLayout(3)]
	public class GachaState : GameStateBase
	{
		// Token: 0x06001E36 RID: 7734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BAE")]
		[Address(RVA = "0x10120DF48", Offset = "0x120DF48", VA = "0x10120DF48")]
		public GachaState(GachaMain owner)
		{
		}

		// Token: 0x06001E37 RID: 7735 RVA: 0x0000D6F8 File Offset: 0x0000B8F8
		[Token(Token = "0x6001BAF")]
		[Address(RVA = "0x10120DF7C", Offset = "0x120DF7C", VA = "0x10120DF7C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E38 RID: 7736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB0")]
		[Address(RVA = "0x10120DF84", Offset = "0x120DF84", VA = "0x10120DF84", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E39 RID: 7737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB1")]
		[Address(RVA = "0x10120DF88", Offset = "0x120DF88", VA = "0x10120DF88", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E3A RID: 7738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB2")]
		[Address(RVA = "0x10120DF8C", Offset = "0x120DF8C", VA = "0x10120DF8C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E3B RID: 7739 RVA: 0x0000D710 File Offset: 0x0000B910
		[Token(Token = "0x6001BB3")]
		[Address(RVA = "0x10120DF90", Offset = "0x120DF90", VA = "0x10120DF90", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001E3C RID: 7740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB4")]
		[Address(RVA = "0x10120DF98", Offset = "0x120DF98", VA = "0x10120DF98", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002E8C RID: 11916
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002419")]
		protected GachaMain m_Owner;

		// Token: 0x04002E8D RID: 11917
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400241A")]
		protected int m_NextState;
	}
}
