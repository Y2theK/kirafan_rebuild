﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using GachaResponseTypes;
using Meige;

namespace Star
{
	// Token: 0x020007B9 RID: 1977
	[Token(Token = "0x20005E1")]
	[StructLayout(3)]
	public class GachaState_Init : GachaState
	{
		// Token: 0x06001E3D RID: 7741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB5")]
		[Address(RVA = "0x101207300", Offset = "0x1207300", VA = "0x101207300")]
		public GachaState_Init(GachaMain owner)
		{
		}

		// Token: 0x06001E3E RID: 7742 RVA: 0x0000D728 File Offset: 0x0000B928
		[Token(Token = "0x6001BB6")]
		[Address(RVA = "0x10120DF9C", Offset = "0x120DF9C", VA = "0x10120DF9C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E3F RID: 7743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB7")]
		[Address(RVA = "0x10120DFA4", Offset = "0x120DFA4", VA = "0x10120DFA4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E40 RID: 7744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB8")]
		[Address(RVA = "0x10120DFAC", Offset = "0x120DFAC", VA = "0x10120DFAC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E41 RID: 7745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BB9")]
		[Address(RVA = "0x10120DFB0", Offset = "0x120DFB0", VA = "0x10120DFB0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E42 RID: 7746 RVA: 0x0000D740 File Offset: 0x0000B940
		[Token(Token = "0x6001BBA")]
		[Address(RVA = "0x10120DFB4", Offset = "0x120DFB4", VA = "0x10120DFB4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001E43 RID: 7747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BBB")]
		[Address(RVA = "0x10120E26C", Offset = "0x120E26C", VA = "0x10120E26C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001E44 RID: 7748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BBC")]
		[Address(RVA = "0x10120E270", Offset = "0x120E270", VA = "0x10120E270")]
		private void OnResponse_GachaGetAll(MeigewwwParam wwwParam, GetAll param)
		{
		}

		// Token: 0x04002E8E RID: 11918
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400241B")]
		private GachaState_Init.eStep m_Step;

		// Token: 0x020007BA RID: 1978
		[Token(Token = "0x2000EA9")]
		private enum eStep
		{
			// Token: 0x04002E90 RID: 11920
			[Token(Token = "0x4005DEA")]
			None = -1,
			// Token: 0x04002E91 RID: 11921
			[Token(Token = "0x4005DEB")]
			First,
			// Token: 0x04002E92 RID: 11922
			[Token(Token = "0x4005DEC")]
			Request_Wait,
			// Token: 0x04002E93 RID: 11923
			[Token(Token = "0x4005DED")]
			LoadWait,
			// Token: 0x04002E94 RID: 11924
			[Token(Token = "0x4005DEE")]
			End
		}
	}
}
