﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using Star.UI.GachaSelect;
using WWWTypes;

namespace Star
{
	// Token: 0x020007BB RID: 1979
	[Token(Token = "0x20005E2")]
	[StructLayout(3)]
	public class GachaState_Main : GachaState
	{
		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06001E45 RID: 7749 RVA: 0x0000D758 File Offset: 0x0000B958
		[Token(Token = "0x17000217")]
		public bool ExistCommonWindow
		{
			[Token(Token = "0x6001BBD")]
			[Address(RVA = "0x10120E2C4", Offset = "0x120E2C4", VA = "0x10120E2C4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06001E46 RID: 7750 RVA: 0x0000D770 File Offset: 0x0000B970
		[Token(Token = "0x17000218")]
		public bool FromRateTicketWindow
		{
			[Token(Token = "0x6001BBE")]
			[Address(RVA = "0x10120E2CC", Offset = "0x120E2CC", VA = "0x10120E2CC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06001E47 RID: 7751 RVA: 0x0000D788 File Offset: 0x0000B988
		[Token(Token = "0x17000219")]
		public int SelectionCurrentCharacterID
		{
			[Token(Token = "0x6001BBF")]
			[Address(RVA = "0x10120E2D4", Offset = "0x120E2D4", VA = "0x10120E2D4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06001E48 RID: 7752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC0")]
		[Address(RVA = "0x10120E2DC", Offset = "0x120E2DC", VA = "0x10120E2DC")]
		public void SetFromPointSelectWindowFlag(bool flg)
		{
		}

		// Token: 0x06001E49 RID: 7753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC1")]
		[Address(RVA = "0x101207334", Offset = "0x1207334", VA = "0x101207334")]
		public GachaState_Main(GachaMain owner)
		{
		}

		// Token: 0x06001E4A RID: 7754 RVA: 0x0000D7A0 File Offset: 0x0000B9A0
		[Token(Token = "0x6001BC2")]
		[Address(RVA = "0x10120E2E4", Offset = "0x120E2E4", VA = "0x10120E2E4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E4B RID: 7755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC3")]
		[Address(RVA = "0x10120E2EC", Offset = "0x120E2EC", VA = "0x10120E2EC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E4C RID: 7756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC4")]
		[Address(RVA = "0x10120E2F4", Offset = "0x120E2F4", VA = "0x10120E2F4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E4D RID: 7757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC5")]
		[Address(RVA = "0x10120E2F8", Offset = "0x120E2F8", VA = "0x10120E2F8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001E4E RID: 7758 RVA: 0x0000D7B8 File Offset: 0x0000B9B8
		[Token(Token = "0x6001BC6")]
		[Address(RVA = "0x10120E304", Offset = "0x120E304", VA = "0x10120E304", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001E4F RID: 7759 RVA: 0x0000D7D0 File Offset: 0x0000B9D0
		[Token(Token = "0x6001BC7")]
		[Address(RVA = "0x10120EBE8", Offset = "0x120EBE8", VA = "0x10120EBE8")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001E50 RID: 7760 RVA: 0x0000D7E8 File Offset: 0x0000B9E8
		[Token(Token = "0x6001BC8")]
		[Address(RVA = "0x10120F088", Offset = "0x120F088", VA = "0x10120F088")]
		private bool SetupAndPlaySelectionUI()
		{
			return default(bool);
		}

		// Token: 0x06001E51 RID: 7761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BC9")]
		[Address(RVA = "0x10120F504", Offset = "0x120F504", VA = "0x10120F504")]
		private void OnClickRateTicketDecideCallback(bool isUse)
		{
		}

		// Token: 0x06001E52 RID: 7762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BCA")]
		[Address(RVA = "0x10120F774", Offset = "0x120F774", VA = "0x10120F774", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001E53 RID: 7763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BCB")]
		[Address(RVA = "0x10120FA48", Offset = "0x120FA48", VA = "0x10120FA48")]
		public void OnClickDecideButtonCallBack(int gachaID, GachaDefine.ePlayType playType)
		{
		}

		// Token: 0x06001E54 RID: 7764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BCC")]
		[Address(RVA = "0x101210420", Offset = "0x1210420", VA = "0x101210420")]
		public void OnClickSelectionCallBack(int charaID)
		{
		}

		// Token: 0x06001E55 RID: 7765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BCD")]
		[Address(RVA = "0x101210428", Offset = "0x1210428", VA = "0x101210428")]
		public void OnClickSelectionDecideButtonCallBack()
		{
		}

		// Token: 0x06001E56 RID: 7766 RVA: 0x0000D800 File Offset: 0x0000BA00
		[Token(Token = "0x6001BCE")]
		[Address(RVA = "0x10120F51C", Offset = "0x120F51C", VA = "0x10120F51C")]
		private bool OpenCheckMessage(bool selection, bool checkOnly = false)
		{
			return default(bool);
		}

		// Token: 0x06001E57 RID: 7767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BCF")]
		[Address(RVA = "0x101210AA4", Offset = "0x1210AA4", VA = "0x101210AA4")]
		public void OnClickDecideGachaBoxButtonCallBack(int gachaID)
		{
		}

		// Token: 0x06001E58 RID: 7768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD0")]
		[Address(RVA = "0x101210B60", Offset = "0x1210B60", VA = "0x101210B60")]
		private void OnConfirmPurchaseGem(int answer)
		{
		}

		// Token: 0x06001E59 RID: 7769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD1")]
		[Address(RVA = "0x101210C14", Offset = "0x1210C14", VA = "0x101210C14")]
		private void OnShortageItem(int buttonIndex)
		{
		}

		// Token: 0x06001E5A RID: 7770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD2")]
		[Address(RVA = "0x101210C1C", Offset = "0x1210C1C", VA = "0x101210C1C")]
		private void OnUpdateFreeGacha(int buttonIndex)
		{
		}

		// Token: 0x06001E5B RID: 7771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD3")]
		[Address(RVA = "0x101210C48", Offset = "0x1210C48", VA = "0x101210C48")]
		private void OnConfirmGachaPlay(int answer)
		{
		}

		// Token: 0x06001E5C RID: 7772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD4")]
		[Address(RVA = "0x101210D68", Offset = "0x1210D68", VA = "0x101210D68")]
		private void RefreshUI()
		{
		}

		// Token: 0x06001E5D RID: 7773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD5")]
		[Address(RVA = "0x10120F8FC", Offset = "0x120F8FC", VA = "0x10120F8FC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001E5E RID: 7774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD6")]
		[Address(RVA = "0x101210D98", Offset = "0x1210D98", VA = "0x101210D98")]
		private void OnClickDecidePointGachaCallBack(int gachaID, int charaID)
		{
		}

		// Token: 0x06001E5F RID: 7775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD7")]
		[Address(RVA = "0x101210E64", Offset = "0x1210E64", VA = "0x101210E64")]
		public void OnClickPointToItemCloseCallBack()
		{
		}

		// Token: 0x06001E60 RID: 7776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD8")]
		[Address(RVA = "0x10121109C", Offset = "0x121109C", VA = "0x10121109C")]
		private void OnResponse_PointToItem(ResultCode ret)
		{
		}

		// Token: 0x06001E61 RID: 7777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BD9")]
		[Address(RVA = "0x101211128", Offset = "0x1211128", VA = "0x101211128")]
		private void OnResponse_GachaPlay(GachaDefine.eReturnGachaPlay ret, string errorMessage)
		{
		}

		// Token: 0x06001E62 RID: 7778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BDA")]
		[Address(RVA = "0x101211220", Offset = "0x1211220", VA = "0x101211220")]
		private void OnResponse_PointGachaPlay(GachaDefine.eReturnGachaPlay ret, string errorMessage)
		{
		}

		// Token: 0x06001E63 RID: 7779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BDB")]
		[Address(RVA = "0x1012112E4", Offset = "0x12112E4", VA = "0x1012112E4")]
		private void OnResponse_GachaBox(GachaDefine.eReturnGachaBox ret, string errorMessage)
		{
		}

		// Token: 0x06001E64 RID: 7780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BDC")]
		[Address(RVA = "0x1012113D4", Offset = "0x12113D4", VA = "0x1012113D4")]
		private void OnConfirmOutOfPeriod()
		{
		}

		// Token: 0x06001E65 RID: 7781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BDD")]
		[Address(RVA = "0x1012113D8", Offset = "0x12113D8", VA = "0x1012113D8")]
		private void OnConfirmUpdateFreeGacha()
		{
		}

		// Token: 0x04002E95 RID: 11925
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400241C")]
		private GachaState_Main.eStep m_Step;

		// Token: 0x04002E96 RID: 11926
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400241D")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002E97 RID: 11927
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400241E")]
		private SceneDefine.eChildSceneID m_NpcSceneID;

		// Token: 0x04002E98 RID: 11928
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400241F")]
		private SceneDefine.eChildSceneID m_SelectionSceneID;

		// Token: 0x04002E99 RID: 11929
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002420")]
		private GachaSelectUI m_UI;

		// Token: 0x04002E9A RID: 11930
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002421")]
		private GachaSelectionUI m_SelectionUI;

		// Token: 0x04002E9B RID: 11931
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002422")]
		private NPCCharaDisplayUI m_NpcUI;

		// Token: 0x04002E9C RID: 11932
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002423")]
		private bool m_IsOutOfPeriod;

		// Token: 0x04002E9D RID: 11933
		[Cpp2IlInjected.FieldOffset(Offset = "0x49")]
		[Token(Token = "0x4002424")]
		private bool m_IsSuccessGachaPlay;

		// Token: 0x04002E9E RID: 11934
		[Cpp2IlInjected.FieldOffset(Offset = "0x4A")]
		[Token(Token = "0x4002425")]
		private bool m_IsSelectionStart;

		// Token: 0x04002E9F RID: 11935
		[Cpp2IlInjected.FieldOffset(Offset = "0x4B")]
		[Token(Token = "0x4002426")]
		private bool m_IsUpdateFreeGacha;

		// Token: 0x04002EA0 RID: 11936
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002427")]
		private bool m_IsFromPointSelectWindow;

		// Token: 0x04002EA1 RID: 11937
		[Cpp2IlInjected.FieldOffset(Offset = "0x4D")]
		[Token(Token = "0x4002428")]
		private bool m_IsFromRateTicketWindow;

		// Token: 0x04002EA2 RID: 11938
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002429")]
		private int m_WorkGachaID;

		// Token: 0x04002EA3 RID: 11939
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400242A")]
		private int m_WorkGachaSelectionCharaID;

		// Token: 0x04002EA4 RID: 11940
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400242B")]
		private bool m_WorkIsStepup;

		// Token: 0x04002EA5 RID: 11941
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400242C")]
		private int m_WorkStepupNum;

		// Token: 0x04002EA6 RID: 11942
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400242D")]
		private int m_WorkTotalNum;

		// Token: 0x04002EA7 RID: 11943
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400242E")]
		private bool m_WorkIsFree;

		// Token: 0x04002EA8 RID: 11944
		[Cpp2IlInjected.FieldOffset(Offset = "0x65")]
		[Token(Token = "0x400242F")]
		private bool m_WorkUseGachaRateTicket;

		// Token: 0x04002EA9 RID: 11945
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002430")]
		private int m_SelectionCurrentCharacterID;

		// Token: 0x04002EAA RID: 11946
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002431")]
		private GachaDefine.ePlayType m_WorkGachaPlayType;

		// Token: 0x04002EAB RID: 11947
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002432")]
		private bool m_ExistCommonWindow;

		// Token: 0x04002EAC RID: 11948
		[Cpp2IlInjected.FieldOffset(Offset = "0x71")]
		[Token(Token = "0x4002433")]
		private bool m_TutorialSelection;

		// Token: 0x04002EAD RID: 11949
		[Cpp2IlInjected.FieldOffset(Offset = "0x72")]
		[Token(Token = "0x4002434")]
		private bool m_TutorialSelectionEnd;

		// Token: 0x020007BC RID: 1980
		[Token(Token = "0x2000EAA")]
		private enum eStep
		{
			// Token: 0x04002EAF RID: 11951
			[Token(Token = "0x4005DF0")]
			None = -1,
			// Token: 0x04002EB0 RID: 11952
			[Token(Token = "0x4005DF1")]
			First,
			// Token: 0x04002EB1 RID: 11953
			[Token(Token = "0x4005DF2")]
			LoadWait,
			// Token: 0x04002EB2 RID: 11954
			[Token(Token = "0x4005DF3")]
			PlayIn,
			// Token: 0x04002EB3 RID: 11955
			[Token(Token = "0x4005DF4")]
			Main,
			// Token: 0x04002EB4 RID: 11956
			[Token(Token = "0x4005DF5")]
			SelectionStart,
			// Token: 0x04002EB5 RID: 11957
			[Token(Token = "0x4005DF6")]
			SelectionLoadWait,
			// Token: 0x04002EB6 RID: 11958
			[Token(Token = "0x4005DF7")]
			SelectionPlayIn,
			// Token: 0x04002EB7 RID: 11959
			[Token(Token = "0x4005DF8")]
			Selection,
			// Token: 0x04002EB8 RID: 11960
			[Token(Token = "0x4005DF9")]
			SelectionEnd,
			// Token: 0x04002EB9 RID: 11961
			[Token(Token = "0x4005DFA")]
			UnloadChildSceneWait
		}
	}
}
