﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200076A RID: 1898
	[Token(Token = "0x20005B5")]
	[StructLayout(3)]
	public static class GachaUtility
	{
		// Token: 0x06001C54 RID: 7252 RVA: 0x0000CA68 File Offset: 0x0000AC68
		[Token(Token = "0x60019D2")]
		[Address(RVA = "0x1012113DC", Offset = "0x12113DC", VA = "0x1012113DC")]
		public static bool IsCostInvalid(int amount)
		{
			return default(bool);
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x0000CA80 File Offset: 0x0000AC80
		[Token(Token = "0x60019D3")]
		[Address(RVA = "0x1012113E8", Offset = "0x12113E8", VA = "0x1012113E8")]
		public static bool IsCostFree(int amount)
		{
			return default(bool);
		}

		// Token: 0x06001C56 RID: 7254 RVA: 0x0000CA98 File Offset: 0x0000AC98
		[Token(Token = "0x60019D4")]
		[Address(RVA = "0x1012113F4", Offset = "0x12113F4", VA = "0x1012113F4")]
		public static bool IsCostConsume(int amount)
		{
			return default(bool);
		}

		// Token: 0x06001C57 RID: 7255 RVA: 0x0000CAB0 File Offset: 0x0000ACB0
		[Token(Token = "0x60019D5")]
		[Address(RVA = "0x101211400", Offset = "0x1211400", VA = "0x101211400")]
		public static bool IsOnlyPlayItem(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C58 RID: 7256 RVA: 0x0000CAC8 File Offset: 0x0000ACC8
		[Token(Token = "0x60019D6")]
		[Address(RVA = "0x10121146C", Offset = "0x121146C", VA = "0x10121146C")]
		public static bool IsExistPlayItem(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C59 RID: 7257 RVA: 0x0000CAE0 File Offset: 0x0000ACE0
		[Token(Token = "0x60019D7")]
		[Address(RVA = "0x1012114B4", Offset = "0x12114B4", VA = "0x1012114B4")]
		public static bool IsEnablePlayItem(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5A RID: 7258 RVA: 0x0000CAF8 File Offset: 0x0000ACF8
		[Token(Token = "0x60019D8")]
		[Address(RVA = "0x101211578", Offset = "0x1211578", VA = "0x101211578")]
		public static bool IsEnablePlayGem1(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5B RID: 7259 RVA: 0x0000CB10 File Offset: 0x0000AD10
		[Token(Token = "0x60019D9")]
		[Address(RVA = "0x1012115A8", Offset = "0x12115A8", VA = "0x1012115A8")]
		public static bool IsEnablePlayFirst10(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5C RID: 7260 RVA: 0x0000CB28 File Offset: 0x0000AD28
		[Token(Token = "0x60019DA")]
		[Address(RVA = "0x1012115EC", Offset = "0x12115EC", VA = "0x1012115EC")]
		public static bool IsEnablePlayGem10(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5D RID: 7261 RVA: 0x0000CB40 File Offset: 0x0000AD40
		[Token(Token = "0x60019DB")]
		[Address(RVA = "0x10121161C", Offset = "0x121161C", VA = "0x10121161C")]
		public static bool IsExistPlayUnlimitedGem(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5E RID: 7262 RVA: 0x0000CB58 File Offset: 0x0000AD58
		[Token(Token = "0x60019DC")]
		[Address(RVA = "0x10121164C", Offset = "0x121164C", VA = "0x10121164C")]
		public static bool IsEnablePlayUnlimitedGem(Gacha.GachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06001C5F RID: 7263 RVA: 0x0000CB70 File Offset: 0x0000AD70
		[Token(Token = "0x60019DD")]
		[Address(RVA = "0x101211690", Offset = "0x1211690", VA = "0x101211690")]
		public static int GetPlayNum(Gacha.GachaData gachaData)
		{
			return 0;
		}

		// Token: 0x06001C60 RID: 7264 RVA: 0x0000CB88 File Offset: 0x0000AD88
		[Token(Token = "0x60019DE")]
		[Address(RVA = "0x10121172C", Offset = "0x121172C", VA = "0x10121172C")]
		public static int GetCurrentStep(Gacha.GachaData gachaData)
		{
			return 0;
		}

		// Token: 0x06001C61 RID: 7265 RVA: 0x0000CBA0 File Offset: 0x0000ADA0
		[Token(Token = "0x60019DF")]
		[Address(RVA = "0x101211754", Offset = "0x1211754", VA = "0x101211754")]
		public static int GetMaxStep(Gacha.GachaData gachaData)
		{
			return 0;
		}

		// Token: 0x06001C62 RID: 7266 RVA: 0x0000CBB8 File Offset: 0x0000ADB8
		[Token(Token = "0x60019E0")]
		[Address(RVA = "0x1012106FC", Offset = "0x12106FC", VA = "0x1012106FC")]
		public static GachaDefine.eCheckPlay CheckPlay(Gacha.GachaData gachaData, GachaDefine.ePlayType playType, bool isFree, int amount, out CommonMessageWindow.eType out_windowType, out string out_messageTitle, out string out_message)
		{
			return GachaDefine.eCheckPlay.Ok;
		}

		// Token: 0x06001C63 RID: 7267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60019E1")]
		[Address(RVA = "0x101210274", Offset = "0x1210274", VA = "0x101210274")]
		public static void GetGachaMessageData(Gacha.GachaData gachaData, GachaDefine.ePlayType playType, bool isFree, out string itemName, out int playNum, out int amount)
		{
		}

		// Token: 0x06001C64 RID: 7268 RVA: 0x0000CBD0 File Offset: 0x0000ADD0
		[Token(Token = "0x60019E2")]
		[Address(RVA = "0x10121177C", Offset = "0x121177C", VA = "0x10121177C")]
		public static bool IsExistFreeDraw()
		{
			return default(bool);
		}
	}
}
