﻿namespace Star {
    public static class GameDefine {
        public const int INVALID_MNG_ID = -1;
        public const int INVALID_ID = -1;
        public const uint U_INVALID_ID = 0U;
        public const int TIMEOUT_SEC = 30;
        public const int AB_TIMEOUT_SEC = 180;
        public const int APP_TARGET_FRAMERATE = 30;
        public const float APP_SEC_PER_ONEFRAME = 0.033333335f;
        public const string FULLOPEN_NAME = "FULLOPEN";
        public const string TXT_DLERR_TITLE = "ダウンロードエラー";
        public const string TXT_DLERR_MSG = "ダウンロード中にエラーが発生しました。\nリトライします。";

        public static bool IsDownloadConfirm => true;

        public enum eDLType {
            AlwaysOnly,
            FirstAndAlways,
            NotDLInDLScene
        }

        public enum eDLScene {
            DL_First,
            DL_Always,
            NotDLScene
        }
    }
}
