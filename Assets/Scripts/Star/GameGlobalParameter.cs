﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AC6 RID: 2758
	[Token(Token = "0x2000796")]
	[StructLayout(3)]
	public class GameGlobalParameter
	{
		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06002FA0 RID: 12192 RVA: 0x00014778 File Offset: 0x00012978
		// (set) Token: 0x06002FA1 RID: 12193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002FC")]
		public bool IsRequestedReturnTitle
		{
			[Token(Token = "0x6002B7C")]
			[Address(RVA = "0x101211AB8", Offset = "0x1211AB8", VA = "0x101211AB8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002B7D")]
			[Address(RVA = "0x101211AC0", Offset = "0x1211AC0", VA = "0x101211AC0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06002FA2 RID: 12194 RVA: 0x00014790 File Offset: 0x00012990
		// (set) Token: 0x06002FA3 RID: 12195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002FD")]
		public int SelectedPartyMemberIndex
		{
			[Token(Token = "0x6002B7E")]
			[Address(RVA = "0x101211AC8", Offset = "0x1211AC8", VA = "0x101211AC8")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002B7F")]
			[Address(RVA = "0x101211AD0", Offset = "0x1211AD0", VA = "0x101211AD0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06002FA4 RID: 12196 RVA: 0x000147A8 File Offset: 0x000129A8
		// (set) Token: 0x06002FA5 RID: 12197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002FE")]
		public long SelectedCharaMngID
		{
			[Token(Token = "0x6002B80")]
			[Address(RVA = "0x101211AD8", Offset = "0x1211AD8", VA = "0x101211AD8")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6002B81")]
			[Address(RVA = "0x101211AE0", Offset = "0x1211AE0", VA = "0x101211AE0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06002FA6 RID: 12198 RVA: 0x000147C0 File Offset: 0x000129C0
		// (set) Token: 0x06002FA7 RID: 12199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002FF")]
		public int SelectedSupportPartyIndex
		{
			[Token(Token = "0x6002B82")]
			[Address(RVA = "0x101211AE8", Offset = "0x1211AE8", VA = "0x101211AE8")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002B83")]
			[Address(RVA = "0x101211AF0", Offset = "0x1211AF0", VA = "0x101211AF0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x06002FA8 RID: 12200 RVA: 0x000147D8 File Offset: 0x000129D8
		// (set) Token: 0x06002FA9 RID: 12201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000300")]
		public int SelectedSupportPartyMemberIndex
		{
			[Token(Token = "0x6002B84")]
			[Address(RVA = "0x101211AF8", Offset = "0x1211AF8", VA = "0x101211AF8")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002B85")]
			[Address(RVA = "0x101211B00", Offset = "0x1211B00", VA = "0x101211B00")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06002FAA RID: 12202 RVA: 0x000147F0 File Offset: 0x000129F0
		// (set) Token: 0x06002FAB RID: 12203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000301")]
		public long SelectedSupportCharaMngID
		{
			[Token(Token = "0x6002B86")]
			[Address(RVA = "0x101211B08", Offset = "0x1211B08", VA = "0x101211B08")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6002B87")]
			[Address(RVA = "0x101211B10", Offset = "0x1211B10", VA = "0x101211B10")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06002FAC RID: 12204 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FAD RID: 12205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000302")]
		public FriendManager.FriendData QuestFriendData
		{
			[Token(Token = "0x6002B88")]
			[Address(RVA = "0x101211B18", Offset = "0x1211B18", VA = "0x101211B18")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002B89")]
			[Address(RVA = "0x101211B20", Offset = "0x1211B20", VA = "0x101211B20")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06002FAE RID: 12206 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FAF RID: 12207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000303")]
		public UserSupportData QuestUserSupportData
		{
			[Token(Token = "0x6002B8A")]
			[Address(RVA = "0x101211B28", Offset = "0x1211B28", VA = "0x101211B28")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002B8B")]
			[Address(RVA = "0x101211B30", Offset = "0x1211B30", VA = "0x101211B30")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06002FB0 RID: 12208 RVA: 0x00014808 File Offset: 0x00012A08
		// (set) Token: 0x06002FB1 RID: 12209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000304")]
		public long QuestNpcID
		{
			[Token(Token = "0x6002B8C")]
			[Address(RVA = "0x101211B38", Offset = "0x1211B38", VA = "0x101211B38")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6002B8D")]
			[Address(RVA = "0x101211B40", Offset = "0x1211B40", VA = "0x101211B40")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06002FB2 RID: 12210 RVA: 0x00014820 File Offset: 0x00012A20
		// (set) Token: 0x06002FB3 RID: 12211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000305")]
		public bool IsRequestAPIWhenEnterQuestScene
		{
			[Token(Token = "0x6002B8E")]
			[Address(RVA = "0x101211B48", Offset = "0x1211B48", VA = "0x101211B48")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002B8F")]
			[Address(RVA = "0x101211B50", Offset = "0x1211B50", VA = "0x101211B50")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06002FB4 RID: 12212 RVA: 0x00014838 File Offset: 0x00012A38
		// (set) Token: 0x06002FB5 RID: 12213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000306")]
		public bool IsHomeModeStart
		{
			[Token(Token = "0x6002B90")]
			[Address(RVA = "0x101211B58", Offset = "0x1211B58", VA = "0x101211B58")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002B91")]
			[Address(RVA = "0x101211B60", Offset = "0x1211B60", VA = "0x101211B60")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06002FB6 RID: 12214 RVA: 0x00014850 File Offset: 0x00012A50
		// (set) Token: 0x06002FB7 RID: 12215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000307")]
		public long RoomPlayerID
		{
			[Token(Token = "0x6002B92")]
			[Address(RVA = "0x101211B68", Offset = "0x1211B68", VA = "0x101211B68")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6002B93")]
			[Address(RVA = "0x101211B70", Offset = "0x1211B70", VA = "0x101211B70")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06002FB8 RID: 12216 RVA: 0x00014868 File Offset: 0x00012A68
		// (set) Token: 0x06002FB9 RID: 12217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000308")]
		public int AdvID
		{
			[Token(Token = "0x6002B94")]
			[Address(RVA = "0x101211B78", Offset = "0x1211B78", VA = "0x101211B78")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002B95")]
			[Address(RVA = "0x101211B80", Offset = "0x1211B80", VA = "0x101211B80")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06002FBA RID: 12218 RVA: 0x00014880 File Offset: 0x00012A80
		// (set) Token: 0x06002FBB RID: 12219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000309")]
		public SceneDefine.eSceneID AdvToNextSceneID
		{
			[Token(Token = "0x6002B96")]
			[Address(RVA = "0x101211B88", Offset = "0x1211B88", VA = "0x101211B88")]
			[CompilerGenerated]
			get
			{
				return SceneDefine.eSceneID.Title;
			}
			[Token(Token = "0x6002B97")]
			[Address(RVA = "0x101211B90", Offset = "0x1211B90", VA = "0x101211B90")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06002FBC RID: 12220 RVA: 0x00014898 File Offset: 0x00012A98
		// (set) Token: 0x06002FBD RID: 12221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030A")]
		public bool AdvOnlyQuest
		{
			[Token(Token = "0x6002B98")]
			[Address(RVA = "0x101211B98", Offset = "0x1211B98", VA = "0x101211B98")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002B99")]
			[Address(RVA = "0x101211BA0", Offset = "0x1211BA0", VA = "0x101211BA0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06002FBE RID: 12222 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FBF RID: 12223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030B")]
		public string MoviePlayFileNameWithoutExt
		{
			[Token(Token = "0x6002B9A")]
			[Address(RVA = "0x101211BA8", Offset = "0x1211BA8", VA = "0x101211BA8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002B9B")]
			[Address(RVA = "0x101211BB0", Offset = "0x1211BB0", VA = "0x101211BB0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06002FC0 RID: 12224 RVA: 0x000148B0 File Offset: 0x00012AB0
		// (set) Token: 0x06002FC1 RID: 12225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030C")]
		public SceneDefine.eSceneID MoviePlayToNextSceneID
		{
			[Token(Token = "0x6002B9C")]
			[Address(RVA = "0x101211BB8", Offset = "0x1211BB8", VA = "0x101211BB8")]
			[CompilerGenerated]
			get
			{
				return SceneDefine.eSceneID.Title;
			}
			[Token(Token = "0x6002B9D")]
			[Address(RVA = "0x101211BC0", Offset = "0x1211BC0", VA = "0x101211BC0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06002FC2 RID: 12226 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FC3 RID: 12227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030D")]
		public Action<string[]> MoviePlayEndCallback
		{
			[Token(Token = "0x6002B9E")]
			[Address(RVA = "0x101211BC8", Offset = "0x1211BC8", VA = "0x101211BC8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002B9F")]
			[Address(RVA = "0x101211BD0", Offset = "0x1211BD0", VA = "0x101211BD0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06002FC4 RID: 12228 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FC5 RID: 12229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030E")]
		public string[] MoviePlayEndCallbackParams
		{
			[Token(Token = "0x6002BA0")]
			[Address(RVA = "0x101211BD8", Offset = "0x1211BD8", VA = "0x101211BD8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002BA1")]
			[Address(RVA = "0x101211BE0", Offset = "0x1211BE0", VA = "0x101211BE0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06002FC6 RID: 12230 RVA: 0x000148C8 File Offset: 0x00012AC8
		// (set) Token: 0x06002FC7 RID: 12231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700030F")]
		public bool IsMenuUserInfoStart
		{
			[Token(Token = "0x6002BA2")]
			[Address(RVA = "0x101211BE8", Offset = "0x1211BE8", VA = "0x101211BE8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BA3")]
			[Address(RVA = "0x101211BF0", Offset = "0x1211BF0", VA = "0x101211BF0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06002FC8 RID: 12232 RVA: 0x000148E0 File Offset: 0x00012AE0
		// (set) Token: 0x06002FC9 RID: 12233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000310")]
		public bool IsMenuFriendStart
		{
			[Token(Token = "0x6002BA4")]
			[Address(RVA = "0x101211BF8", Offset = "0x1211BF8", VA = "0x101211BF8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BA5")]
			[Address(RVA = "0x101211C00", Offset = "0x1211C00", VA = "0x101211C00")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06002FCA RID: 12234 RVA: 0x000148F8 File Offset: 0x00012AF8
		// (set) Token: 0x06002FCB RID: 12235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000311")]
		public bool IsMenuLibraryStart
		{
			[Token(Token = "0x6002BA6")]
			[Address(RVA = "0x101211C08", Offset = "0x1211C08", VA = "0x101211C08")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BA7")]
			[Address(RVA = "0x101211C10", Offset = "0x1211C10", VA = "0x101211C10")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06002FCC RID: 12236 RVA: 0x00014910 File Offset: 0x00012B10
		// (set) Token: 0x06002FCD RID: 12237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000312")]
		public bool IsMenuLibraryOPMovieStart
		{
			[Token(Token = "0x6002BA8")]
			[Address(RVA = "0x101211C18", Offset = "0x1211C18", VA = "0x101211C18")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BA9")]
			[Address(RVA = "0x101211C20", Offset = "0x1211C20", VA = "0x101211C20")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06002FCE RID: 12238 RVA: 0x00014928 File Offset: 0x00012B28
		// (set) Token: 0x06002FCF RID: 12239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000313")]
		public bool IsMenuUpgradeStart
		{
			[Token(Token = "0x6002BAA")]
			[Address(RVA = "0x101211C28", Offset = "0x1211C28", VA = "0x101211C28")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BAB")]
			[Address(RVA = "0x101211C30", Offset = "0x1211C30", VA = "0x101211C30")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06002FD0 RID: 12240 RVA: 0x00014940 File Offset: 0x00012B40
		// (set) Token: 0x06002FD1 RID: 12241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000314")]
		public bool IsMenuBackToHome
		{
			[Token(Token = "0x6002BAC")]
			[Address(RVA = "0x101211C38", Offset = "0x1211C38", VA = "0x101211C38")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BAD")]
			[Address(RVA = "0x101211C40", Offset = "0x1211C40", VA = "0x101211C40")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06002FD2 RID: 12242 RVA: 0x00014958 File Offset: 0x00012B58
		// (set) Token: 0x06002FD3 RID: 12243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000315")]
		public bool IsOccuredOpenInfo
		{
			[Token(Token = "0x6002BAE")]
			[Address(RVA = "0x101211C48", Offset = "0x1211C48", VA = "0x101211C48")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BAF")]
			[Address(RVA = "0x101211C50", Offset = "0x1211C50", VA = "0x101211C50")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06002FD4 RID: 12244 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FD5 RID: 12245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000316")]
		public InformationHomeOpen InfoHomeOpen
		{
			[Token(Token = "0x6002BB0")]
			[Address(RVA = "0x101211C58", Offset = "0x1211C58", VA = "0x101211C58")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002BB1")]
			[Address(RVA = "0x101211C60", Offset = "0x1211C60", VA = "0x101211C60")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06002FD6 RID: 12246 RVA: 0x00014970 File Offset: 0x00012B70
		// (set) Token: 0x06002FD7 RID: 12247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000317")]
		public float RefreshBadgeTimeSec
		{
			[Token(Token = "0x6002BB2")]
			[Address(RVA = "0x101211C68", Offset = "0x1211C68", VA = "0x101211C68")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6002BB3")]
			[Address(RVA = "0x101211C70", Offset = "0x1211C70", VA = "0x101211C70")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06002FD8 RID: 12248 RVA: 0x00014988 File Offset: 0x00012B88
		// (set) Token: 0x06002FD9 RID: 12249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000318")]
		public DateTime LastSupportRequest
		{
			[Token(Token = "0x6002BB4")]
			[Address(RVA = "0x101211C78", Offset = "0x1211C78", VA = "0x101211C78")]
			[CompilerGenerated]
			get
			{
				return default(DateTime);
			}
			[Token(Token = "0x6002BB5")]
			[Address(RVA = "0x101211C80", Offset = "0x1211C80", VA = "0x101211C80")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06002FDA RID: 12250 RVA: 0x000149A0 File Offset: 0x00012BA0
		// (set) Token: 0x06002FDB RID: 12251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000319")]
		public bool ComingSoonTrigger
		{
			[Token(Token = "0x6002BB6")]
			[Address(RVA = "0x101211C88", Offset = "0x1211C88", VA = "0x101211C88")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BB7")]
			[Address(RVA = "0x101211C90", Offset = "0x1211C90", VA = "0x101211C90")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06002FDC RID: 12252 RVA: 0x000149B8 File Offset: 0x00012BB8
		// (set) Token: 0x06002FDD RID: 12253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031A")]
		public int ClearAnimChapterId
		{
			[Token(Token = "0x6002BB8")]
			[Address(RVA = "0x101211C98", Offset = "0x1211C98", VA = "0x101211C98")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002BB9")]
			[Address(RVA = "0x101211CA0", Offset = "0x1211CA0", VA = "0x101211CA0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06002FDE RID: 12254 RVA: 0x000149D0 File Offset: 0x00012BD0
		// (set) Token: 0x06002FDF RID: 12255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031B")]
		public GameGlobalParameter.eQuestStartMode QuestStartMode
		{
			[Token(Token = "0x6002BBA")]
			[Address(RVA = "0x101211CA8", Offset = "0x1211CA8", VA = "0x101211CA8")]
			[CompilerGenerated]
			get
			{
				return GameGlobalParameter.eQuestStartMode.NotTop;
			}
			[Token(Token = "0x6002BBB")]
			[Address(RVA = "0x101211CB0", Offset = "0x1211CB0", VA = "0x101211CB0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06002FE0 RID: 12256 RVA: 0x000149E8 File Offset: 0x00012BE8
		// (set) Token: 0x06002FE1 RID: 12257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031C")]
		public bool IsNextQuestStart
		{
			[Token(Token = "0x6002BBC")]
			[Address(RVA = "0x101211CB8", Offset = "0x1211CB8", VA = "0x101211CB8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BBD")]
			[Address(RVA = "0x101211CC0", Offset = "0x1211CC0", VA = "0x101211CC0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06002FE2 RID: 12258 RVA: 0x00014A00 File Offset: 0x00012C00
		// (set) Token: 0x06002FE3 RID: 12259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031D")]
		public bool IsChapterSelectStart
		{
			[Token(Token = "0x6002BBE")]
			[Address(RVA = "0x101211CC8", Offset = "0x1211CC8", VA = "0x101211CC8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002BBF")]
			[Address(RVA = "0x101211CD0", Offset = "0x1211CD0", VA = "0x101211CD0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06002FE4 RID: 12260 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FE5 RID: 12261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031E")]
		public GameGlobalParameter.MenuLibraryADVParam MenuLibADVParam
		{
			[Token(Token = "0x6002BC0")]
			[Address(RVA = "0x101211CD8", Offset = "0x1211CD8", VA = "0x101211CD8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002BC1")]
			[Address(RVA = "0x101211CE0", Offset = "0x1211CE0", VA = "0x101211CE0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06002FE6 RID: 12262 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FE7 RID: 12263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700031F")]
		public GameGlobalParameter.RoomShopRequestParam RoomShopReqParam
		{
			[Token(Token = "0x6002BC2")]
			[Address(RVA = "0x101211CE8", Offset = "0x1211CE8", VA = "0x101211CE8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002BC3")]
			[Address(RVA = "0x101211CF0", Offset = "0x1211CF0", VA = "0x101211CF0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06002FE8 RID: 12264 RVA: 0x00014A18 File Offset: 0x00012C18
		// (set) Token: 0x06002FE9 RID: 12265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000320")]
		public GameGlobalParameter.eShopStartType ShopStartType
		{
			[Token(Token = "0x6002BC4")]
			[Address(RVA = "0x101211CF8", Offset = "0x1211CF8", VA = "0x101211CF8")]
			[CompilerGenerated]
			get
			{
				return GameGlobalParameter.eShopStartType.Top;
			}
			[Token(Token = "0x6002BC5")]
			[Address(RVA = "0x101211D00", Offset = "0x1211D00", VA = "0x101211D00")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06002FEA RID: 12266 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002FEB RID: 12267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000321")]
		public List<GameGlobalParameter.RemindPremium> RemindPremiumList
		{
			[Token(Token = "0x6002BC6")]
			[Address(RVA = "0x101211D08", Offset = "0x1211D08", VA = "0x101211D08")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002BC7")]
			[Address(RVA = "0x101211D10", Offset = "0x1211D10", VA = "0x101211D10")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06002FEC RID: 12268 RVA: 0x00014A30 File Offset: 0x00012C30
		// (set) Token: 0x06002FED RID: 12269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000322")]
		public eTitleType NextContentRoomTitleType
		{
			[Token(Token = "0x6002BC8")]
			[Address(RVA = "0x101211D18", Offset = "0x1211D18", VA = "0x101211D18")]
			[CompilerGenerated]
			get
			{
				return eTitleType.Title_0000;
			}
			[Token(Token = "0x6002BC9")]
			[Address(RVA = "0x101211D20", Offset = "0x1211D20", VA = "0x101211D20")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06002FEE RID: 12270 RVA: 0x00014A48 File Offset: 0x00012C48
		// (set) Token: 0x06002FEF RID: 12271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000323")]
		public eTitleType CurrentContentRoomTitleType
		{
			[Token(Token = "0x6002BCA")]
			[Address(RVA = "0x101211D28", Offset = "0x1211D28", VA = "0x101211D28")]
			[CompilerGenerated]
			get
			{
				return eTitleType.Title_0000;
			}
			[Token(Token = "0x6002BCB")]
			[Address(RVA = "0x101211D30", Offset = "0x1211D30", VA = "0x101211D30")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06002FF0 RID: 12272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002BCC")]
		[Address(RVA = "0x101211D38", Offset = "0x1211D38", VA = "0x101211D38")]
		public GameGlobalParameter()
		{
		}

		// Token: 0x06002FF1 RID: 12273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002BCD")]
		[Address(RVA = "0x101211D60", Offset = "0x1211D60", VA = "0x101211D60")]
		public void Reset()
		{
		}

		// Token: 0x06002FF2 RID: 12274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002BCE")]
		[Address(RVA = "0x101211E78", Offset = "0x1211E78", VA = "0x101211E78")]
		public void ResetSelectionQuestParam()
		{
		}

		// Token: 0x06002FF3 RID: 12275 RVA: 0x00014A60 File Offset: 0x00012C60
		[Token(Token = "0x6002BCF")]
		[Address(RVA = "0x101211E90", Offset = "0x1211E90", VA = "0x101211E90")]
		public eEventQuestDropExtPlusType GetSelectSupportDropExtType()
		{
			return eEventQuestDropExtPlusType.Self;
		}

		// Token: 0x06002FF4 RID: 12276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002BD0")]
		[Address(RVA = "0x10120DF3C", Offset = "0x120DF3C", VA = "0x10120DF3C")]
		public void SetAdvParam(int advID, SceneDefine.eSceneID advToNextSceneID, bool advOnlyQuest = false)
		{
		}

		// Token: 0x06002FF5 RID: 12277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002BD1")]
		[Address(RVA = "0x101211EB4", Offset = "0x1211EB4", VA = "0x101211EB4")]
		public void SetMoviePlayParam(string moviePlayFileNameWithoutExt, SceneDefine.eSceneID moviePlayToNextSceneID, [Optional] Action<string[]> endCallback, [Optional] string[] endCallbackParams)
		{
		}

		// Token: 0x02000AC7 RID: 2759
		[Token(Token = "0x2000FFD")]
		public enum eQuestStartMode
		{
			// Token: 0x04003F65 RID: 16229
			[Token(Token = "0x400654A")]
			Top = -1,
			// Token: 0x04003F66 RID: 16230
			[Token(Token = "0x400654B")]
			NotTop
		}

		// Token: 0x02000AC8 RID: 2760
		[Token(Token = "0x2000FFE")]
		public enum eCharaADVType
		{
			// Token: 0x04003F68 RID: 16232
			[Token(Token = "0x400654D")]
			Single,
			// Token: 0x04003F69 RID: 16233
			[Token(Token = "0x400654E")]
			Cross,
			// Token: 0x04003F6A RID: 16234
			[Token(Token = "0x400654F")]
			Weapon
		}

		// Token: 0x02000AC9 RID: 2761
		[Token(Token = "0x2000FFF")]
		public class MenuLibraryADVParam
		{
			// Token: 0x06002FF6 RID: 12278 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060DD")]
			[Address(RVA = "0x101211EC4", Offset = "0x1211EC4", VA = "0x101211EC4")]
			public MenuLibraryADVParam()
			{
			}

			// Token: 0x04003F6B RID: 16235
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006550")]
			public int m_AdvId;

			// Token: 0x04003F6C RID: 16236
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006551")]
			public int m_AdvLibID;

			// Token: 0x04003F6D RID: 16237
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006552")]
			public GameGlobalParameter.eCharaADVType m_CharaADVType;

			// Token: 0x04003F6E RID: 16238
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006553")]
			public eCharaNamedType m_Named;
		}

		// Token: 0x02000ACA RID: 2762
		[Token(Token = "0x2001000")]
		public class RoomShopRequestParam
		{
			// Token: 0x06002FF7 RID: 12279 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060DE")]
			[Address(RVA = "0x101211F28", Offset = "0x1211F28", VA = "0x101211F28")]
			public RoomShopRequestParam(bool isBuy, eRoomObjectCategory category, int id, int num)
			{
			}

			// Token: 0x04003F6F RID: 16239
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006554")]
			public bool m_IsBuy;

			// Token: 0x04003F70 RID: 16240
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006555")]
			public eRoomObjectCategory m_Category;

			// Token: 0x04003F71 RID: 16241
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006556")]
			public int m_RoomObjID;

			// Token: 0x04003F72 RID: 16242
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006557")]
			public int m_PlacementNum;
		}

		// Token: 0x02000ACB RID: 2763
		[Token(Token = "0x2001001")]
		public enum eShopStartType
		{
			// Token: 0x04003F74 RID: 16244
			[Token(Token = "0x4006559")]
			Top,
			// Token: 0x04003F75 RID: 16245
			[Token(Token = "0x400655A")]
			Trade,
			// Token: 0x04003F76 RID: 16246
			[Token(Token = "0x400655B")]
			Weapon,
			// Token: 0x04003F77 RID: 16247
			[Token(Token = "0x400655C")]
			Build,
			// Token: 0x04003F78 RID: 16248
			[Token(Token = "0x400655D")]
			Market
		}

		// Token: 0x02000ACC RID: 2764
		[Token(Token = "0x2001002")]
		public class RemindPremium
		{
			// Token: 0x06002FF8 RID: 12280 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060DF")]
			[Address(RVA = "0x101211ECC", Offset = "0x1211ECC", VA = "0x101211ECC")]
			public RemindPremium(int premiumId, StoreDefine.eRemindType remindType, string name, DateTime? expirationDate)
			{
			}

			// Token: 0x04003F79 RID: 16249
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400655E")]
			public int m_PremiumId;

			// Token: 0x04003F7A RID: 16250
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400655F")]
			public StoreDefine.eRemindType m_RemindType;

			// Token: 0x04003F7B RID: 16251
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006560")]
			public string m_Name;

			// Token: 0x04003F7C RID: 16252
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006561")]
			public DateTime? m_ExpirationDate;
		}
	}
}
