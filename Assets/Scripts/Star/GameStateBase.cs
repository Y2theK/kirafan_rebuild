﻿namespace Star {
    public abstract class GameStateBase {
        public abstract int GetStateID();

        public abstract void OnStateEnter();

        public abstract void OnStateExit();

        public abstract int OnStateUpdate();

        public abstract void OnDispose();

        protected abstract void OnClickBackButton(bool isCallFromShortCut);
    }
}
