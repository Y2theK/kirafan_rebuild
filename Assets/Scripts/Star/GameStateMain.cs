﻿using UnityEngine;

namespace Star {
    public class GameStateMain : MonoBehaviour {
        public const int COMMON_STATE_FINAL = 2147483646;
        protected int m_PrevStateID = -1;
        protected int m_NextStateID = -1;
        private SceneDefine.eSceneID m_NextTransitSceneID = SceneDefine.eSceneID.None;
        protected GameStateBase m_CurrentState;

        public int PrevStateID => m_PrevStateID;
        public int NextStateID => m_NextStateID;
        public GameStateBase CurrentState => m_CurrentState;

        public SceneDefine.eSceneID NextTransitSceneID {
            get => m_NextTransitSceneID;
            set => m_NextTransitSceneID = value;
        }

        protected virtual void Update() {
            if (m_NextStateID != -1) {
                if (m_CurrentState != null) {
                    m_PrevStateID = m_CurrentState.GetStateID();
                    m_CurrentState.OnStateExit();
                    m_CurrentState.OnDispose();
                    m_CurrentState = null;
                }
                m_CurrentState = ChangeState(m_NextStateID);
                if (m_CurrentState != null) {
                    m_CurrentState.OnStateEnter();
                }
                m_NextStateID = -1;
            }
            if (m_CurrentState != null) {
                m_NextStateID = m_CurrentState.OnStateUpdate();
            }
        }

        private void OnDestroy() {
            if (m_CurrentState != null) {
                m_CurrentState.OnDispose();
                m_CurrentState = null;
            }
        }

        public void SetNextState(int nextStateID) {
            m_NextStateID = nextStateID;
        }

        public virtual GameStateBase ChangeState(int stateID) {
            return null;
        }

        public virtual void Destroy() { }

        public virtual bool IsCompleteDestroy() {
            return true;
        }
    }
}
