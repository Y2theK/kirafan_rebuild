﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007C3 RID: 1987
	[Token(Token = "0x20005E8")]
	[StructLayout(3)]
	public static class GameStateUtility
	{
		// Token: 0x06001E8F RID: 7823 RVA: 0x0000D8F0 File Offset: 0x0000BAF0
		[Token(Token = "0x6001C07")]
		[Address(RVA = "0x101211FE0", Offset = "0x1211FE0", VA = "0x101211FE0")]
		public static SceneDefine.eSceneID SetupQuestTransitFromEventType(QuestManager.SelectQuest selectQuest, int eventType)
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E90 RID: 7824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C08")]
		[Address(RVA = "0x101212388", Offset = "0x1212388", VA = "0x101212388")]
		private static void MoviePlayEndCallback(string[] pms)
		{
		}

		// Token: 0x06001E91 RID: 7825 RVA: 0x0000D908 File Offset: 0x0000BB08
		[Token(Token = "0x6001C09")]
		[Address(RVA = "0x101212108", Offset = "0x1212108", VA = "0x101212108")]
		public static SceneDefine.eSceneID SetupQuestTransit(QuestManager.SelectQuest selectQuest)
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E92 RID: 7826 RVA: 0x0000D920 File Offset: 0x0000BB20
		[Token(Token = "0x6001C0A")]
		[Address(RVA = "0x1012125B4", Offset = "0x12125B4", VA = "0x1012125B4")]
		public static SceneDefine.eSceneID SetupCharaQuestTransit()
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E93 RID: 7827 RVA: 0x0000D938 File Offset: 0x0000BB38
		[Token(Token = "0x6001C0B")]
		[Address(RVA = "0x1012126BC", Offset = "0x12126BC", VA = "0x1012126BC")]
		public static SceneDefine.eSceneID SetupOfferQuestTransit(eTitleType titleType, int questId)
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E94 RID: 7828 RVA: 0x0000D950 File Offset: 0x0000BB50
		[Token(Token = "0x6001C0C")]
		[Address(RVA = "0x1012127FC", Offset = "0x12127FC", VA = "0x1012127FC")]
		public static SceneDefine.eSceneID SetupChapterQuestTransit(int chapterID)
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E95 RID: 7829 RVA: 0x0000D968 File Offset: 0x0000BB68
		[Token(Token = "0x6001C0D")]
		[Address(RVA = "0x101212914", Offset = "0x1212914", VA = "0x101212914")]
		public static SceneDefine.eSceneID SetupQuestToAdvTransit(int advID = -1, bool advOnlyQuest = false)
		{
			return SceneDefine.eSceneID.Title;
		}

		// Token: 0x06001E96 RID: 7830 RVA: 0x0000D980 File Offset: 0x0000BB80
		[Token(Token = "0x6001C0E")]
		[Address(RVA = "0x1012129AC", Offset = "0x12129AC", VA = "0x1012129AC")]
		public static SceneDefine.eSceneID GetSceneFromADVID(int advID)
		{
			return SceneDefine.eSceneID.Title;
		}
	}
}
