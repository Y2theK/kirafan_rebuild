﻿using Meige;
using Meige.AssetBundles;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.BackGround;
using Star.UI.Global;
using Star.UI.Room;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using WWWTypes;
using Random = UnityEngine.Random;
using ResumegetReq = PlayerRequestTypes.Resumeget;

namespace Star {
    public sealed class GameSystem : SingletonMonoBehaviour<GameSystem> {
        [SerializeField] private EffectManager m_EffectManager;
        [SerializeField] private GlobalUI m_GlobalUI;
        [SerializeField] private FadeManager m_FadeManager;
        [SerializeField] private LoadingUI m_LoadingUI;
        [SerializeField] private TutorialTipsUI m_TutorialTipsUI;
        [SerializeField] private CommonMessageWindow m_CommonMessageWindow;
        [SerializeField] private ItemDetailWindow m_ItemDetailWindow;
        [SerializeField] private CharaUnlockWindow m_CharaUnlockWindow;
        [SerializeField] private CharaQuestUnlockWindow m_CharaQuestUnlockWindow;
        [SerializeField] private AgeConfirmWindow m_AgeConfirmWindow;
        [SerializeField] private ConnectingUI m_ConnectingUI;
        [SerializeField] private WebViewWindow m_WebViewWindow;
        [SerializeField] private UIBackGroundManager m_BGManager;
        [SerializeField] private TutorialMessage m_TutorialMessage;
        [SerializeField] private UIInputBlock m_UIInputBlock;
        [SerializeField] private OverlayUIPlayerManager m_OverlayUIManager;
        [SerializeField] private SceneLoader m_SceneLoader;
        [SerializeField] private Camera m_UICamera;
        [SerializeField] private Camera m_FullUICamera;
        [SerializeField] private Camera m_SystemUICamera;
        [SerializeField] private Camera m_SaveAreaCamera;
        [SerializeField] private SaveAreaCamera m_SaveAreaCameraCtrl;
        [SerializeField] private PushNotificationService m_PushNotificationService;
        [SerializeField] private AspectScopeFilter m_AspectScopeFilter;
        [SerializeField] private TouchEffectManager m_TouchEffectManager;
        [SerializeField] private FakeReflectionCtrlBank m_FakeReflectionCtrlBank;

        private DateTime m_ServerTime;
        private DateTime m_LastServerTime;
        private DateTime m_LastAddMemberServerTime;

        private LoginManager m_LoginManager;
        private GameGlobalParameter m_GlobalParameter;
        private DatabaseManager m_DatabaseManager;
        private UserDataManager m_UserDataManager;
        private TutorialManager m_TutorialManager;
        private StoreManager m_StoreManager;
        private QuestManager m_QuestManager;
        private PresentManager m_PresentManager;
        private FriendManager m_FriendManager;
        private ShopManager m_ShopManager;
        private OfferManager m_OfferManager;
        private AchievementManager m_AchievementManager;
        private MissionManager m_MissionManager;
        private TrainingManager m_TrainingManager;
        private Gacha m_Gacha;
        private Chest m_Chest;
        private EditManager m_EditManager;
        private EnhanceManager m_EnhanceManager;
        private AbilityManager m_AbilityManager;
        private CharacterManager m_CharacterManager;
        private CharacterResourceManager m_CharacterResourceManager;
        private TownResourceManager m_TownResourceManager;
        private RoomObjectResourceManager m_RoomResourceManager;
        private SpriteManager m_SpriteManager;
        private SoundManager m_SoundManager;
        private SoundVersionDictionary m_SoundVersionDictionary;
        private VoiceController m_VoiceController;
        private CRIFileInstaller m_CRIFileInstaller;

        private Unlock m_Unlock;
        private UISettings m_UISettings;
        private NotificationController m_NotificationCtrl = new NotificationController();
        private bool m_IsRegisteredPushNotificationService;
        private List<AsyncOperation> m_UnloadUnusedAsyncOps = new List<AsyncOperation>();
        private bool m_IsRequestedUnloadUnusedAsync;
        private MeigeResource.Handler m_ResidentShaderPackResourceHandler;
        private EventBannerLoader m_EventBannerLoader = new EventBannerLoader();
        private List<UnplayedGachaAdvertise> m_UnplayedGachaAdvertiseList = new List<UnplayedGachaAdvertise>();
        private RoomObjListWindow.RoomObjListBackupParam m_RoomObjListBackupParam = new RoomObjListWindow.RoomObjListBackupParam();

        private MeigeResource.Handler m_ResourceHandlerGlobalUI;
        private MeigeResource.Handler m_ResourceHandlerItemDetailWindow;
        private MeigeResource.Handler m_ResourceHandlerCharaUnlockWindow;
        private MeigeResource.Handler m_ResourceHandlerCharaQuestUnlockWindow;
        private MeigeResource.Handler m_ResourceHandlerAgeConfirmWindow;

        private readonly string GlobalUIPrefabPath = "Prefab/UI/GlobalUIPrefab" + MeigeResourceManager.AB_EXTENSION;
        private readonly string ItemDetailWindowPrefabPath = "Prefab/UI/ItemDetailWindowPrefab" + MeigeResourceManager.AB_EXTENSION;
        private readonly string CharaUnlockWindowPrefabPath = "Prefab/UI/CharaUnlockWindowPrefab" + MeigeResourceManager.AB_EXTENSION;
        private readonly string CharaQuestUnlockWindowPrefabPath = "Prefab/UI/CharaQuestUnlockWindowPrefab" + MeigeResourceManager.AB_EXTENSION;
        private readonly string AgeConfirmWindowPrefabPath = "Prefab/UI/AgeConfirmWindowPrefab" + MeigeResourceManager.AB_EXTENSION;

        private VersionGetAPI m_VersionGetAPI;
        private const string STARTUP_SCENE = "Title";

        private bool m_IsAwakeFromStartup;
        private bool m_IsSuspended;
        private bool m_IsCompletePrepareVersionGet;
        private bool m_IsCompletePrepareNetwork;
        private bool m_IsCompletePrepareAssetBundles;
        private bool m_IsCompletePrepareDatabase;
        private bool m_IsCompletePrepareSound;
        private bool m_IsCompletePrepareDeferredSetupObject;

        private ePrepareStep m_PrepareStep = ePrepareStep.None;
        private readonly TimeSpan m_LastCheckedTimeWithThreshold = new TimeSpan(1, 0, 0);

        private CharacterHandler m_CharaHndlForMenu;

        public DateTime ServerTime {
            get => m_ServerTime;
            set {
                m_ServerTime = value;
                m_LastServerTime = value;
            }
        }

        public DateTime LastAddMemberServerTime {
            get => m_LastAddMemberServerTime;
            set => m_LastAddMemberServerTime = value;
        }

        public LoginManager LoginManager => m_LoginManager;
        public GameGlobalParameter GlobalParam => m_GlobalParameter;
        public DatabaseManager DbMng => m_DatabaseManager;
        public UserDataManager UserDataMng => m_UserDataManager;
        public TutorialManager TutorialMng => m_TutorialManager;
        public StoreManager StoreMng => m_StoreManager;
        public QuestManager QuestMng => m_QuestManager;
        public PresentManager PresentMng => m_PresentManager;
        public FriendManager FriendMng => m_FriendManager;
        public ShopManager ShopMng => m_ShopManager;
        public OfferManager OfferMng => m_OfferManager;
        public AchievementManager AchievementMng => m_AchievementManager;
        public MissionManager MissionMng => m_MissionManager;
        public TrainingManager TrainingMng => m_TrainingManager;
        public Gacha Gacha => m_Gacha;
        public Chest Chest => m_Chest;
        public EditManager EditMng => m_EditManager;
        public EnhanceManager EnhanceMng => m_EnhanceManager;
        public AbilityManager AbilityMng => m_AbilityManager;
        public CharacterManager CharaMng => m_CharacterManager;
        public CharacterResourceManager CharaResMng => m_CharacterResourceManager;
        public TownResourceManager TownResMng => m_TownResourceManager;
        public RoomObjectResourceManager RoomResMng => m_RoomResourceManager;
        public SpriteManager SpriteMng => m_SpriteManager;
        public SoundManager SoundMng => m_SoundManager;
        public SoundVersionDictionary SoundVersionDictionary => m_SoundVersionDictionary;
        public VoiceController VoiceCtrl => m_VoiceController;
        public CRIFileInstaller CRIFileInstaller => m_CRIFileInstaller;
        public UISettings UISettings => m_UISettings;
        public EffectManager EffectMng => m_EffectManager;
        public Unlock Unlock => m_Unlock;
        public WeaponUnlock WeaponUnlock => m_Unlock?.WeaponUnlock;
        public CharaQuestUnlock CharaQuestUnlock => m_Unlock?.CharaQuestUnlock;
        public GlobalUI GlobalUI => m_GlobalUI;
        public FadeManager FadeMng => m_FadeManager;
        public LoadingUI LoadingUI => m_LoadingUI;
        public TutorialTipsUI TutorialTipsUI => m_TutorialTipsUI;
        public CommonMessageWindow CmnMsgWindow => m_CommonMessageWindow;
        public ItemDetailWindow ItemDetailWindow => m_ItemDetailWindow;
        public CharaUnlockWindow CharaUnlockWindow => m_CharaUnlockWindow;
        public CharaQuestUnlockWindow CharaQuestUnlockWindow => m_CharaQuestUnlockWindow;
        public AgeConfirmWindow AgeConfirmWindow => m_AgeConfirmWindow;
        public ConnectingUI ConnectingIcon => m_ConnectingUI;
        public WebViewWindow WebView => m_WebViewWindow;
        public UIBackGroundManager BackGroundManager => m_BGManager;
        public TutorialMessage TutorialMessage => m_TutorialMessage;
        public UIInputBlock InputBlock => m_UIInputBlock;
        public OverlayUIPlayerManager OverlayUIMng => m_OverlayUIManager;
        public SceneLoader SceneLoader => m_SceneLoader;
        public Camera UICamera => m_UICamera;
        public Camera FullUICamera => m_FullUICamera;
        public Camera SystemUICamera => m_SystemUICamera;
        public Camera SaveAreaCamera => m_SaveAreaCamera;
        public SaveAreaCamera SaveAreaCameraCtrl => m_SaveAreaCameraCtrl;
        public NotificationController NotificationCtrl => m_NotificationCtrl;
        public FakeReflectionCtrlBank FakeReflectionCtrlBank => m_FakeReflectionCtrlBank;

        public CharacterHandler CharaHndlForMenu => m_CharaHndlForMenu;
        public EventBannerLoader EventBannerLoad => m_EventBannerLoader;

        public AspectScopeFilter AspectScopeFilter {
            get => m_AspectScopeFilter;
            set => m_AspectScopeFilter = value;
        }

        public TouchEffectManager TouchEffectMng {
            get => m_TouchEffectManager;
            set => m_TouchEffectManager = value;
        }

        public int GetUnplayedGachaAdvertiseNum() {
            return m_UnplayedGachaAdvertiseList.Count;
        }

        public UnplayedGachaAdvertise GetUnplayedGachaAdvertiseAt(int idx) {
            if (idx < GetUnplayedGachaAdvertiseNum()) {
                return m_UnplayedGachaAdvertiseList[idx];
            }
            return null;
        }

        public void AddUnplayedGachaAdvertise(UnplayedGachaAdvertise value) {
            for (int i = 0; i < m_UnplayedGachaAdvertiseList.Count; i++) {
                if (m_UnplayedGachaAdvertiseList[i].m_ID == value.m_ID) {
                    return;
                }
            }
            m_UnplayedGachaAdvertiseList.Add(value);
        }

        public void RemoveUnplayedGachaAdvertise(int idx) {
            if (idx < GetUnplayedGachaAdvertiseNum()) {
                m_UnplayedGachaAdvertiseList.RemoveAt(idx);
            }
        }

        public void ClearUnplayedGachaAdvertise() {
            m_UnplayedGachaAdvertiseList.Clear();
        }

        public RoomObjListWindow.RoomObjListBackupParam RoomObjListBackupParam => m_RoomObjListBackupParam;

        protected override void Awake() {
            if (CheckInstance()) {
                UnityEngine.Debug.unityLogger.logEnabled = false;
                Random.InitState(Environment.TickCount);
                Application.targetFrameRate = GameDefine.APP_TARGET_FRAMERATE;
                InputTouch.Create();
                Frame.Create();
                JobQueue.Create();
                Meige.AssetBundles.Utility.Initialize();
                m_IsAwakeFromStartup = SceneManager.GetActiveScene().name == STARTUP_SCENE;
                m_NotificationCtrl.OnAwake();
            }
        }

        private void Start() {
            LocalSaveData.Inst.Load();
            AccessSaveData.Inst.Load();
            PreviewLabs.PlayerPrefs.Setup();
            m_LoginManager = new LoginManager();
            m_SoundManager = new SoundManager();
            m_SoundVersionDictionary = new SoundVersionDictionary();
            m_VoiceController = new VoiceController();
            m_CRIFileInstaller = new CRIFileInstaller();
            m_UISettings = new UISettings();
            m_GlobalParameter = new GameGlobalParameter();
            m_DatabaseManager = new DatabaseManager();
            m_UserDataManager = new UserDataManager();
            m_TutorialManager = new TutorialManager();
            m_StoreManager = new StoreManager();
            m_QuestManager = new QuestManager();
            m_PresentManager = new PresentManager();
            m_FriendManager = new FriendManager();
            m_ShopManager = new ShopManager();
            m_OfferManager = new OfferManager();
            m_AchievementManager = new AchievementManager();
            m_MissionManager = new MissionManager();
            m_TrainingManager = new TrainingManager();
            m_Gacha = new Gacha();
            m_Chest = new Chest();
            m_EditManager = new EditManager();
            m_EnhanceManager = new EnhanceManager();
            m_AbilityManager = new AbilityManager();
            m_CharacterManager = new CharacterManager();
            m_CharacterResourceManager = new CharacterResourceManager();
            m_TownResourceManager = new TownResourceManager();
            m_RoomResourceManager = new RoomObjectResourceManager();
            m_SpriteManager = new SpriteManager();
            m_CommonMessageWindow.Setup();
            m_BGManager.Setup();
            m_Unlock = new Unlock();
            gameObject.RemoveComponent<CriWareErrorHandler>();
            m_PrepareStep = ePrepareStep.Start;
            if (Helper.IsEmulator()) {
                Application.Quit();
            }
        }

        private void Update() {
            m_ServerTime = m_ServerTime.AddSeconds((double)Time.unscaledDeltaTime);
            if (LoginManager != null && LoginManager.IsLogined && m_ServerTime - m_LastServerTime > m_LastCheckedTimeWithThreshold) {
                Request_ResumeGet();
                m_LastServerTime = m_ServerTime;
            }
            Frame.Instance.Update();
            InputTouch.Inst.Update();
            m_CRIFileInstaller.Update();
            m_SoundManager.Update();
            m_VoiceController.Update();
            m_DatabaseManager.Update();
            m_UserDataManager.Update();
            m_StoreManager.Update();
            m_CharacterResourceManager.Update();
            m_TownResourceManager.Update();
            m_RoomResourceManager.Update();
            m_SpriteManager.Update();
            m_TrainingManager.Update();
            m_MissionManager.Update();
            m_CharacterManager.Update();
            m_Unlock.Update();
            UpdatePrepare();
            for (int i = m_UnloadUnusedAsyncOps.Count - 1; i >= 0; i--) {
                if (m_UnloadUnusedAsyncOps[i].isDone) {
                    m_UnloadUnusedAsyncOps.RemoveAt(i);
                    if (m_UnloadUnusedAsyncOps.Count <= 0) {
                        GC.Collect();
                    }
                }
            }
        }

        public void PrepareDeferredSetupObject() {
            if (m_ResourceHandlerGlobalUI == null) {
                m_ResourceHandlerGlobalUI = MeigeResourceManager.LoadHandler(GlobalUIPrefabPath, MeigeResource.Option.ResourceKeep);
            }
            if (m_ResourceHandlerItemDetailWindow == null) {
                m_ResourceHandlerItemDetailWindow = MeigeResourceManager.LoadHandler(ItemDetailWindowPrefabPath, MeigeResource.Option.ResourceKeep);
            }
            if (m_ResourceHandlerCharaUnlockWindow == null) {
                m_ResourceHandlerCharaUnlockWindow = MeigeResourceManager.LoadHandler(CharaUnlockWindowPrefabPath, MeigeResource.Option.ResourceKeep);
            }
            if (m_ResourceHandlerCharaQuestUnlockWindow == null) {
                m_ResourceHandlerCharaQuestUnlockWindow = MeigeResourceManager.LoadHandler(CharaQuestUnlockWindowPrefabPath, MeigeResource.Option.ResourceKeep);
            }
            if (m_ResourceHandlerAgeConfirmWindow == null) {
                m_ResourceHandlerAgeConfirmWindow = MeigeResourceManager.LoadHandler(AgeConfirmWindowPrefabPath, MeigeResource.Option.ResourceKeep);
            }

            if (!m_ResourceHandlerGlobalUI.IsDone) {
                return;
            } else {
                if (m_ResourceHandlerGlobalUI.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(m_ResourceHandlerGlobalUI);
                    return;
                }
            }
            if (!m_ResourceHandlerItemDetailWindow.IsDone) {
                return;
            } else {
                if (m_ResourceHandlerItemDetailWindow.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(m_ResourceHandlerItemDetailWindow);
                    return;
                }
            }
            if (!m_ResourceHandlerCharaUnlockWindow.IsDone) {
                return;
            } else {
                if (m_ResourceHandlerCharaUnlockWindow.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(m_ResourceHandlerCharaUnlockWindow);
                    return;
                }
            }
            if (!m_ResourceHandlerCharaQuestUnlockWindow.IsDone) {
                return;
            } else {
                if (m_ResourceHandlerCharaQuestUnlockWindow.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(m_ResourceHandlerCharaQuestUnlockWindow);
                    return;
                }
            }
            if (!m_ResourceHandlerAgeConfirmWindow.IsDone) {
                return;
            } else {
                if (m_ResourceHandlerAgeConfirmWindow.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(m_ResourceHandlerAgeConfirmWindow);
                    return;
                }
            }

            if (m_GlobalUI == null) {
                GameObject go = m_ResourceHandlerGlobalUI.GetAsset<GameObject>();
                GlobalUI prefab = go.GetComponentInChildren<GlobalUI>();
                m_GlobalUI = Instantiate(prefab);
                m_GlobalUI.Setup();
                m_GlobalUI.transform.SetParent(transform);
            }
            if (m_ItemDetailWindow == null) {
                GameObject go = m_ResourceHandlerItemDetailWindow.GetAsset<GameObject>();
                ItemDetailWindow prefab = go.GetComponentInChildren<ItemDetailWindow>();
                m_ItemDetailWindow = Instantiate(prefab);
                m_ItemDetailWindow.transform.SetParent(transform);
            }
            if (m_CharaUnlockWindow == null) {
                GameObject go = m_ResourceHandlerCharaUnlockWindow.GetAsset<GameObject>();
                CharaUnlockWindow prefab = go.GetComponentInChildren<CharaUnlockWindow>();
                m_CharaUnlockWindow = Instantiate(prefab);
                m_CharaUnlockWindow.transform.SetParent(transform);
            }
            if (m_CharaQuestUnlockWindow == null) {
                GameObject go = m_ResourceHandlerCharaQuestUnlockWindow.GetAsset<GameObject>();
                CharaQuestUnlockWindow prefab = go.GetComponentInChildren<CharaQuestUnlockWindow>();
                m_CharaQuestUnlockWindow = Instantiate(prefab);
                m_CharaQuestUnlockWindow.transform.SetParent(transform);
            }
            if (m_AgeConfirmWindow == null) {
                GameObject go = m_ResourceHandlerAgeConfirmWindow.GetAsset<GameObject>();
                AgeConfirmWindow prefab = go.GetComponentInChildren<AgeConfirmWindow>();
                m_AgeConfirmWindow = Instantiate(prefab);
                m_AgeConfirmWindow.transform.SetParent(transform);
            }
            m_IsCompletePrepareDeferredSetupObject = true;
        }

        public bool IsCompletePrepareDeferredSetupObject() {
            return m_IsCompletePrepareDeferredSetupObject;
        }

        public void PrepareVersionGet() {
            m_IsCompletePrepareVersionGet = false;
            m_VersionGetAPI = new VersionGetAPI();
            m_VersionGetAPI.Request_VersionGet(OnResponse_VersionGet);
        }

        private void OnResponse_VersionGet() {
            m_IsCompletePrepareVersionGet = true;
            m_VersionGetAPI = null;
        }

        public bool IsCompletePrepareVersionGet() {
            return m_IsCompletePrepareVersionGet;
        }

        public void PrepareNetwork(bool isRetry = false) {
            m_IsCompletePrepareNetwork = false;
            if (isRetry) {
                MeigeResourceManager.ClearError();
                return;
            }
            m_CRIFileInstaller.Setup(AssetServerSettings.assetServerURL + "CRI/");
            NetworkQueueManager.SetTimeoutSec(GameDefine.TIMEOUT_SEC);
            MeigeResourceManager.timeoutSec = GameDefine.AB_TIMEOUT_SEC;
            AssetBundleManager.SetIsReady(false);
            MeigeResourceManager.SetAssetbundleServer(AssetServerSettings.assetServerURL);
        }

        public bool IsCompletePrepareNetowrk(out bool out_isError) {
            out_isError = false;
            if (MeigeResourceManager.isError) {
                out_isError = true;
                return false;
            }
            if (!MeigeResourceManager.isReadyAssetbundle) {
                return false;
            }
            if (!m_IsCompletePrepareNetwork) {
                m_IsCompletePrepareNetwork = true;
            }
            return true;
        }

        public void PrepareAssetBundles() {
            m_IsCompletePrepareAssetBundles = false;
            m_ResidentShaderPackResourceHandler = MeigeResourceManager.LoadHandler("residentshaderpack/residentshaderpack" + MeigeResourceManager.AB_EXTENSION);
        }

        public bool IsCompletePrepareAssetBundles() {
            if (m_ResidentShaderPackResourceHandler.IsDone) {
                if (m_ResidentShaderPackResourceHandler.IsError) {
                    MeigeResourceManager.RetryHandler(m_ResidentShaderPackResourceHandler);
                } else {
                    if (!m_IsCompletePrepareAssetBundles) {
                        for (int i = 0; i < m_ResidentShaderPackResourceHandler.assets.Length; i++) {
                            ShaderVariantCollection shaderVariantCollection = m_ResidentShaderPackResourceHandler.assets[i] as ShaderVariantCollection;
                            if (shaderVariantCollection != null) {
                                shaderVariantCollection.WarmUp();
                                break;
                            }
                        }
                    }
                    m_IsCompletePrepareAssetBundles = true;
                }
            }
            return m_IsCompletePrepareAssetBundles;
        }

        public void PrepareDatabase() {
            m_IsCompletePrepareDatabase = false;
            m_DatabaseManager.Prepare();
            StartCoroutine(m_SoundVersionDictionary.CreateVersionDictionary());
        }

        public bool IsCompletePrepareDatabase() {
            if (m_SoundVersionDictionary.IsError()) {
                m_SoundVersionDictionary.Restart();
                return false;
            }
            if (!m_SoundVersionDictionary.IsDone()) {
                return false;
            }
            if (!m_DatabaseManager.IsPreparing() && !m_IsCompletePrepareDatabase) {
                m_IsCompletePrepareDatabase = true;
                m_EffectManager.SetupFromDB(DbMng.EffectListDB);
                m_EffectManager.SetupSound(m_SoundManager);
                m_SoundManager.Setup(DbMng.SoundCueSheetDB, DbMng.SoundSeListDB, DbMng.SoundBgmListDB, DbMng.SoundVoiceListDB, DbMng.NamedListDB);
                m_VoiceController.Setup(m_SoundManager);
                m_UISettings.Setup(LocalSaveData.Inst);
            }
            return m_IsCompletePrepareDatabase;
        }

        public void PrepareSound(bool isRegisterInstalledAcf) {
            m_IsCompletePrepareSound = false;
            if (isRegisterInstalledAcf) {
                m_SoundManager.RegisterInstalledAcf();
            }
            int length = SoundDefine.BUILTIN_DATAS.GetLength(0);
            for (int i = 0; i < length; i++) {
                m_SoundManager.LoadCueSheet(SoundDefine.BUILTIN_DATAS[i, 0], SoundDefine.BUILTIN_DATAS[i, 1], SoundDefine.BUILTIN_DATAS[i, 2]);
            }
        }

        public bool IsCompletePrepareSound() {
            if (!m_SoundManager.IsLoadingCueSheets() && !m_IsCompletePrepareSound) {
                m_SoundManager.ApplySaveData(LocalSaveData.Inst);
                m_IsCompletePrepareSound = true;
            }
            return m_IsCompletePrepareSound;
        }

        private void UpdatePrepare() {
            switch (m_PrepareStep) {
                case ePrepareStep.Start:
                    if (m_IsAwakeFromStartup) {
                        m_PrepareStep = ePrepareStep.End;
                    } else {
                        m_PrepareStep = ePrepareStep.PreLogin_NonStartup_VersionGet;
                    }
                    break;
                case ePrepareStep.PreLogin_NonStartup_VersionGet:
                    PrepareVersionGet();
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_VersionGetWait;
                    break;
                case ePrepareStep.PreLogin_NonStartup_VersionGetWait:
                    if (m_IsCompletePrepareVersionGet) {
                        m_PrepareStep = ePrepareStep.PreLogin_NonStartup_PrepareNetwork;
                    }
                    break;
                case ePrepareStep.PreLogin_NonStartup_PrepareNetwork:
                    PrepareNetwork();
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_PrepareNetworkWait;
                    break;
                case ePrepareStep.PreLogin_NonStartup_PrepareNetworkWait:
                    if (!IsCompletePrepareNetowrk(out bool error)) {
                        if (error) {
                            PrepareNetwork(true);
                        }
                        break;
                    }
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_AB;
                    break;
                case ePrepareStep.PreLogin_NonStartup_AB:
                    PrepareAssetBundles();
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_ABWait;
                    break;
                case ePrepareStep.PreLogin_NonStartup_ABWait:
                    if (IsCompletePrepareAssetBundles()) {
                        m_PrepareStep = ePrepareStep.PreLogin_NonStartup_Database;
                    }
                    break;
                case ePrepareStep.PreLogin_NonStartup_Database:
                    PrepareDatabase();
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_DatabaseWait;
                    break;
                case ePrepareStep.PreLogin_NonStartup_DatabaseWait:
                    if (IsCompletePrepareDatabase()) {
                        m_PrepareStep = ePrepareStep.PreLogin_NonStartup_Sound;
                    }
                    break;
                case ePrepareStep.PreLogin_NonStartup_PrepareDeferredObject:
                    if (!m_IsCompletePrepareDeferredSetupObject) {
                        PrepareDeferredSetupObject();
                        if (!m_IsCompletePrepareDeferredSetupObject) {
                            return;
                        }
                    }
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_Sound;
                    break;
                case ePrepareStep.PreLogin_NonStartup_Sound:
                    PrepareSound(false);
                    m_SoundManager.LoadCueSheet("BGM", "BGM.acb", "BGM.awb");
                    m_PrepareStep = ePrepareStep.PreLogin_NonStartup_SoundWait;
                    break;
                case ePrepareStep.PreLogin_NonStartup_SoundWait:
                    if (IsCompletePrepareSound()) {
                        m_PrepareStep = ePrepareStep.AutoLogin;
                    }
                    break;
                case ePrepareStep.AutoLogin:
                    if (LoginManager.Request_Login(false, OnResponse_Login, OnResponse_GetAll)) {
                        m_PrepareStep = ePrepareStep.AutoLogin_Wait;
                    } else {
                        m_PrepareStep = ePrepareStep.AutoSignup;
                    }
                    break;
                case ePrepareStep.PostAutoLogin_QuestGetAll:
                    Inst.QuestMng.Request_QuestGetAll(OnResponse_QuestGetAll);
                    m_PrepareStep = ePrepareStep.PostAutoLogin_QuestGetAllWait;
                    break;
                case ePrepareStep.PostAutoLogin_Mission:
                    m_MissionManager.Prepare();
                    m_PrepareStep = ePrepareStep.PostAutoLogin_MissionWait;
                    break;
                case ePrepareStep.PostAutoLogin_MissionWait:
                    if (m_MissionManager.IsDonePrepare()) {
                        m_PrepareStep = ePrepareStep.PostAutoLogin_End;
                    }
                    break;
                case ePrepareStep.PostAutoLogin_End:
                    m_PrepareStep = ePrepareStep.End;
                    break;
                case ePrepareStep.AutoSignup:
                    LoginManager.Request_Signup(GameDefine.FULLOPEN_NAME, "", OnResponse_Signup);
                    m_PrepareStep = ePrepareStep.AutoSignup_Wait;
                    break;
            }
        }

        private void OnResponse_Login(MeigewwwParam wwwParam, Login param) {
            if (param == null) {
                m_PrepareStep = ePrepareStep.None;
            } else if (param.GetResult() != ResultCode.SUCCESS) {
                m_PrepareStep = ePrepareStep.AutoSignup;
            }
        }

        private void OnResponse_GetAll(MeigewwwParam wwwParam, Getall param) {
            if (param == null) {
                m_PrepareStep = ePrepareStep.None;
            } else {
                ResultCode result = param.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowNone(result.ToMessageString());
                } else {
                    m_PrepareStep = ePrepareStep.PostAutoLogin_QuestGetAll;
                }
            }
        }

        private void OnResponse_QuestGetAll() {
            Inst.QuestMng.Request_QuestChapterGetAll(OnResponse_QuestChapterGetAll);
        }

        private void OnResponse_QuestChapterGetAll() {
            m_PrepareStep = ePrepareStep.PostAutoLogin_Mission;
        }

        private void OnResponse_Signup(MeigewwwParam wwwParam, Signup param) {
            if (param == null) {
                m_PrepareStep = ePrepareStep.None;
            } else {
                ResultCode result = param.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowNone(result.ToMessageString());
                } else {
                    m_PrepareStep = ePrepareStep.AutoLogin;
                }
            }
        }

        private void LateUpdate() {
            Frame.Instance.LateUpdate();
            NetworkReachability network = Application.internetReachability;
            if (network == NetworkReachability.ReachableViaLocalAreaNetwork) {
                ProjDepend.ASSETBUNDLE_MAX_DOWNLOADING_COUNT = 5;
            } else if (network == NetworkReachability.ReachableViaCarrierDataNetwork) {
                ProjDepend.ASSETBUNDLE_MAX_DOWNLOADING_COUNT = 3;
            }
            if (m_IsRequestedUnloadUnusedAsync) {
                m_IsRequestedUnloadUnusedAsync = false;
                m_UnloadUnusedAsyncOps.Add(Resources.UnloadUnusedAssets());
            }
        }

        protected override void OnDestroy() {
            if (this == Inst) {
                if (m_SoundManager != null) {
                    m_SoundManager.Destroy();
                }
                if (m_CRIFileInstaller != null) {
                    m_CRIFileInstaller.Destroy();
                }
                if (m_TutorialTipsUI != null) {
                    m_TutorialTipsUI.Destroy();
                }
                JobQueue.Release();
                MeigeResourceManager.UnloadHandlerAll();
            }
            base.OnDestroy();
        }

        private void OnApplicationQuit() {
            PreviewLabs.PlayerPrefs.Flush();
        }

        private void OnApplicationPause(bool isPause) {
            if (isPause) {
                OnSuspend();
            } else {
                OnResume();
            }
        }

        private void OnSuspend() {
            if (!m_IsSuspended) {
                m_IsSuspended = true;
            }
        }

        private void OnResume() {
            if (m_IsSuspended) {
                m_IsSuspended = false;
                Request_ResumeGet();
            }
        }

        public void Request_ResumeGet() {
            if (IsAvailable()) {
                ResumegetReq param = new ResumegetReq { playerId = Inst.UserDataMng.UserData.ID };
                MeigewwwParam wwwParam = PlayerRequest.Resumeget(param, OnResponse_ResumeGet);
                NetworkQueueManager.Request(wwwParam);
            }
        }

        private void OnResponse_ResumeGet(MeigewwwParam wwwParam) {
            if (!wwwParam.IsDone() || wwwParam.IsSystemError()) { return; }

            string responseJson = wwwParam.GetResponseJson();
            Resumeget resumeget = ResponseCommon.Deserialize<Resumeget>(responseJson);
            if (resumeget != null) {
                ResultCode result = resumeget.GetResult();
                if (result == ResultCode.SUCCESS) {
                    m_ServerTime = resumeget.serverTime;
                    m_LastServerTime = resumeget.serverTime;
                    UserData userData = Inst.UserDataMng.UserData;
                    userData.Stamina.SetValueMax(resumeget.staminaMax);
                    userData.Stamina.SetValue(resumeget.stamina);
                    userData.Stamina.SetRemainSecMax(resumeget.recastTimeMax);
                    userData.Stamina.SetRemainSec(resumeget.recastTime);
                }
            }
        }

        private bool IsPreparing() {
            return m_PrepareStep != ePrepareStep.End;
        }

        public bool IsAvailable() {
            return !IsPreparing()
                && m_GlobalParameter != null
                && m_DatabaseManager != null
                && m_QuestManager != null
                && m_FriendManager != null
                && m_ShopManager != null
                && m_OfferManager != null
                && m_AchievementManager != null
                && m_Gacha != null
                && m_EnhanceManager != null
                && m_AbilityManager != null
                && m_UserDataManager != null
                && m_UserDataManager.IsAvailable()
                && m_CharacterManager != null
                && m_CharacterManager.IsAvailable()
                && m_CharacterResourceManager != null
                && m_CharacterResourceManager.IsAvailable()
                && m_TownResourceManager != null
                && m_TownResourceManager.IsAvailable()
                && m_RoomResourceManager != null
                && m_RoomResourceManager.IsAvailable()
                && m_SpriteManager != null
                && m_EffectManager != null;
        }

        public void RequesetUnloadUnusedAssets(bool isGCCollect = true) {
            if (!m_IsRequestedUnloadUnusedAsync) {
                m_IsRequestedUnloadUnusedAsync = true;
            }
        }

        public bool IsCompleteUnloadUnusedAssets() {
            return !m_IsRequestedUnloadUnusedAsync && m_UnloadUnusedAsyncOps.Count <= 0;
        }

        public void RegisterPushNotificationService(Action onRegistered) {
            if (!m_IsRegisteredPushNotificationService && m_PushNotificationService != null) {
                m_PushNotificationService.RegisterDevice(onRegistered);
                m_IsRegisteredPushNotificationService = true;
            }
        }

        public CharacterHandler PrepareCharaHndlForMenu(int viewCharaID, Transform parent = null) {
            DestroyCharaHndlForMenu();
            CharacterParam characterParam = new CharacterParam();
            CharacterUtility.SetupCharacterParam(characterParam, -1L, viewCharaID, viewCharaID, 1, 99, 0L, 0, 0, 0);
            CharacterListDB_Param param = DbMng.CharaListDB.GetParam(characterParam.ViewCharaID);
            m_CharaHndlForMenu = CharaMng.InstantiateCharacter(characterParam, null, null, null, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Menu, true, CharacterDefine.eFriendType.None, "CHARA_MENU", parent);
            if (m_CharaHndlForMenu != null) {
                m_CharaHndlForMenu.Prepare();
                m_CharaHndlForMenu.CacheTransform.position = new Vector3(0f, 10000f, 0f);
            }
            return m_CharaHndlForMenu;
        }

        public void DestroyCharaHndlForMenu() {
            if (m_CharaHndlForMenu != null) {
                CharaMng.DestroyChara(m_CharaHndlForMenu);
                m_CharaHndlForMenu = null;
            }
        }

        public void SuspendCharaHndlForMenu() {
            if (m_CharaHndlForMenu != null) {
                m_CharaHndlForMenu.SetEnableRender(false);
            }
        }

        public void ResumeCharaHndlForMenu() {
            if (m_CharaHndlForMenu != null) {
                m_CharaHndlForMenu.SetEnableRender(true);
            }
        }

        [Conditional("APP_DEBUG")]
        public void DebugLogCrashReport() {
            CrashReport[] reports = CrashReport.reports;
            if (reports != null) {
                UnityEngine.Debug.Log("Crash reports (" + reports.Length + ")");
                foreach (CrashReport crashReport in reports) {
                    UnityEngine.Debug.Log(string.Concat(new object[]
                    {
                        " Crash: ",
                        crashReport.time,
                        " >> ",
                        crashReport.text
                    }));
                }
            } else {
                UnityEngine.Debug.Log("Crash reports is not found.");
            }
        }

        public void SetCineScoImagesm(bool flg) {
            int cineScoCount = m_AspectScopeFilter.transform.childCount;
            for (int i = 0; i < cineScoCount; i++) {
                m_AspectScopeFilter.GetCineScoImages(i).enabled = flg;
            }
        }

        public bool IsInRangeServerTime(DateTime startAt, DateTime endAt) {
            return startAt <= m_ServerTime && m_ServerTime <= endAt;
        }

        public void SetRoomActionScriptPlaySoundFlg(RoomBuilderBase.ePlayScriptSoundId id, bool flg) {
            Scene room = SceneLoader.Inst.GetScene(SceneDefine.eSceneID.Room);
            if (room.path == null) {
                room = SceneLoader.Inst.GetScene(SceneDefine.eSceneID.ContentRoom);
                if (room.path == null) { return; }
            }
            foreach (GameObject obj in room.GetRootGameObjects()) {
                if (obj.TryGetComponent(out ContentRoomController controller)) {
                    controller.builder.SetFlgPlayScriptSound(id, flg);
                }
            }
        }

        public SoundDLSizeCheck CheckSoundDLSize(GameDefine.eDLScene dlScene) {
            SoundDLSizeCheck check = new SoundDLSizeCheck();
            StartCoroutine(check.CheckAsync_First(dlScene));
            return check;
        }

        public SoundDLSizeCheck CheckSoundDLSizeForCueSheet(List<string> list) {
            SoundDLSizeCheck check = new SoundDLSizeCheck();
            StartCoroutine(check.CheckAsync_CueSheet(list));
            return check;
        }

        public SoundDLSizeCheck CheckSoundDLSizeForFileList(List<SoundDLSizeCheck.VersionSet> list) {
            SoundDLSizeCheck check = new SoundDLSizeCheck();
            StartCoroutine(check.CheckAsync_FileList(list));
            return check;
        }

        public int GetTitleCallCharacterCueSheetIndex() {
            SoundVoiceControllListDB_Param param = DbMng.SoundVoiceControllListDB.GetParam(eSoundVoiceControllListDB.TitleCall);
            if (param.m_Datas != null && param.m_Datas.Length > 0) {
                return Random.Range(0, param.m_Datas[0].m_CueSheetNames.Length);
            }
            return -1;
        }

        public int GetTitleCallCharacterIdForSelector(eCharaNamedType namedType) {
            List<UserCharacterData> charas = m_UserDataManager.UserCharaDatas.FindAll(v => v.Param.NamedType == namedType);
            if (charas.Count > 0) {
                return charas[Random.Range(0, charas.Count)].Param.CharaID;
            }
            return -1;
        }

        public int GetNamedListIndex() {
            if (m_UserDataManager.UserNamedDatas.Count > 0) {
                int idx = Random.Range(0, m_UserDataManager.UserNamedDatas.Count);
                (eCharaNamedType type, _) = m_UserDataManager.UserNamedDatas.ElementAt(idx);
                for (int i = 0; i < DbMng.NamedListDB.m_Params.Length; i++) {
                    if (DbMng.NamedListDB.m_Params[i].m_NamedType == (int)type) {
                        return i;
                    }
                }
            }
            return -1;
        }

        public class UnplayedGachaAdvertise {
            public int m_ID;
            public string m_MovieName;
            public long m_StartAt_Tick;
            public long m_EndAt_Tick;

            public UnplayedGachaAdvertise(int id, string name, DateTime startAt, DateTime endAt) {
                m_ID = id;
                m_MovieName = name;
                m_StartAt_Tick = startAt.Ticks;
                m_EndAt_Tick = endAt.Ticks;
            }
        }

        private enum ePrepareStep {
            None = -1,
            Start,
            PreLogin_NonStartup_VersionGet,
            PreLogin_NonStartup_VersionGetWait,
            PreLogin_NonStartup_PrepareNetwork,
            PreLogin_NonStartup_PrepareNetworkWait,
            PreLogin_NonStartup_AB,
            PreLogin_NonStartup_ABWait,
            PreLogin_NonStartup_Database,
            PreLogin_NonStartup_DatabaseWait,
            PreLogin_NonStartup_PrepareDeferredObject,
            PreLogin_NonStartup_Sound,
            PreLogin_NonStartup_SoundWait,
            AutoLogin,
            AutoLogin_Wait,
            PostAutoLogin_QuestGetAll,
            PostAutoLogin_QuestGetAllWait,
            PostAutoLogin_Mission,
            PostAutoLogin_MissionWait,
            PostAutoLogin_End,
            AutoSignup,
            AutoSignup_Wait,
            End
        }
    }
}
