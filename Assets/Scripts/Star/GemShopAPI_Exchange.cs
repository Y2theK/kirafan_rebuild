﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000890 RID: 2192
	[Token(Token = "0x2000659")]
	[StructLayout(3)]
	public class GemShopAPI_Exchange
	{
		// Token: 0x0600237E RID: 9086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CA")]
		[Address(RVA = "0x101216C4C", Offset = "0x1216C4C", VA = "0x101216C4C")]
		public GemShopAPI_Exchange()
		{
		}

		// Token: 0x0600237F RID: 9087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CB")]
		[Address(RVA = "0x101216C54", Offset = "0x1216C54", VA = "0x101216C54")]
		public void Clear()
		{
		}

		// Token: 0x06002380 RID: 9088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CC")]
		[Address(RVA = "0x101216C64", Offset = "0x1216C64", VA = "0x101216C64")]
		public void Exchange(long exchangeShopId, int buyAmount, Action<int> OnEndExpiredWindowCallback)
		{
		}

		// Token: 0x06002381 RID: 9089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CD")]
		[Address(RVA = "0x101216F20", Offset = "0x1216F20", VA = "0x101216F20")]
		private void OnCompleteExchangeShopBuy_Success(ResultCode resultCode)
		{
		}

		// Token: 0x06002382 RID: 9090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CE")]
		[Address(RVA = "0x1012171D4", Offset = "0x12171D4", VA = "0x1012171D4")]
		private void OnCompleteExchangeShopBuy_Failure(ResultCode resultCode)
		{
		}

		// Token: 0x040033BB RID: 13243
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002631")]
		private StoreManager.Product m_Product;

		// Token: 0x040033BC RID: 13244
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002632")]
		private int m_BuyAmount;

		// Token: 0x040033BD RID: 13245
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002633")]
		public Action RefreshUI;
	}
}
