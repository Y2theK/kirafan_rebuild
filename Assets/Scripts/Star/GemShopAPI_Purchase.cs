﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000891 RID: 2193
	[Token(Token = "0x200065A")]
	[StructLayout(3)]
	public class GemShopAPI_Purchase
	{
		// Token: 0x06002383 RID: 9091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020CF")]
		[Address(RVA = "0x101217230", Offset = "0x1217230", VA = "0x101217230")]
		public GemShopAPI_Purchase()
		{
		}

		// Token: 0x06002384 RID: 9092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D0")]
		[Address(RVA = "0x101217238", Offset = "0x1217238", VA = "0x101217238")]
		public void Clear()
		{
		}

		// Token: 0x06002385 RID: 9093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D1")]
		[Address(RVA = "0x101217240", Offset = "0x1217240", VA = "0x101217240")]
		public void Purchase(int id, Action<int> OnEndExpiredWindowCallback)
		{
		}

		// Token: 0x06002386 RID: 9094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D2")]
		[Address(RVA = "0x101217588", Offset = "0x1217588", VA = "0x101217588")]
		private void OnCompletePurchase(StoreManager.Product product)
		{
		}

		// Token: 0x06002387 RID: 9095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D3")]
		[Address(RVA = "0x1012181FC", Offset = "0x12181FC", VA = "0x1012181FC")]
		private void OnPendingPurchase()
		{
		}

		// Token: 0x06002388 RID: 9096 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020D4")]
		[Address(RVA = "0x101217ED4", Offset = "0x1217ED4", VA = "0x101217ED4")]
		private string CreateProductString(string form, string formFirstDay, StoreManager.PremiumReward[] storeRewards)
		{
			return null;
		}

		// Token: 0x040033BE RID: 13246
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002634")]
		public Action RefreshUI;

		// Token: 0x040033BF RID: 13247
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002635")]
		public Action<string, string, string> OpenScrollListWindow;
	}
}
