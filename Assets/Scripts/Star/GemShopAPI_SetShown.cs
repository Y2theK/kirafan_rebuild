﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000892 RID: 2194
	[Token(Token = "0x200065B")]
	[StructLayout(3)]
	public class GemShopAPI_SetShown
	{
		// Token: 0x06002389 RID: 9097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D5")]
		[Address(RVA = "0x101218208", Offset = "0x1218208", VA = "0x101218208")]
		public GemShopAPI_SetShown()
		{
		}

		// Token: 0x0600238A RID: 9098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D6")]
		[Address(RVA = "0x1012182C8", Offset = "0x12182C8", VA = "0x1012182C8")]
		public void Clear()
		{
		}

		// Token: 0x0600238B RID: 9099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D7")]
		[Address(RVA = "0x101218380", Offset = "0x1218380", VA = "0x101218380")]
		public void AddShownList(List<StoreManager.Product> productList)
		{
		}

		// Token: 0x0600238C RID: 9100 RVA: 0x0000F330 File Offset: 0x0000D530
		[Token(Token = "0x60020D8")]
		[Address(RVA = "0x101218A74", Offset = "0x1218A74", VA = "0x101218A74")]
		public bool ExistAnyProduct()
		{
			return default(bool);
		}

		// Token: 0x0600238D RID: 9101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020D9")]
		[Address(RVA = "0x101218BD8", Offset = "0x1218BD8", VA = "0x101218BD8")]
		public void SetShownFlow(Action callback)
		{
		}

		// Token: 0x0600238E RID: 9102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020DA")]
		[Address(RVA = "0x101218BE0", Offset = "0x1218BE0", VA = "0x101218BE0")]
		private void SetShownFlowImpl()
		{
		}

		// Token: 0x0600238F RID: 9103 RVA: 0x0000F348 File Offset: 0x0000D548
		[Token(Token = "0x60020DB")]
		[Address(RVA = "0x101218AB8", Offset = "0x1218AB8", VA = "0x101218AB8")]
		public bool ExistSetShown()
		{
			return default(bool);
		}

		// Token: 0x06002390 RID: 9104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020DC")]
		[Address(RVA = "0x10121888C", Offset = "0x121888C", VA = "0x10121888C")]
		public void AddSetShown(StoreManager.Product product)
		{
		}

		// Token: 0x06002391 RID: 9105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020DD")]
		[Address(RVA = "0x101218D0C", Offset = "0x1218D0C", VA = "0x101218D0C")]
		public void SetShown(Action onResponseCallback)
		{
		}

		// Token: 0x06002392 RID: 9106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020DE")]
		[Address(RVA = "0x101218FD0", Offset = "0x1218FD0", VA = "0x101218FD0")]
		private void OnResponseSetShown()
		{
		}

		// Token: 0x06002393 RID: 9107 RVA: 0x0000F360 File Offset: 0x0000D560
		[Token(Token = "0x60020DF")]
		[Address(RVA = "0x101218B18", Offset = "0x1218B18", VA = "0x101218B18")]
		private bool ExistSetShownDirectSale()
		{
			return default(bool);
		}

		// Token: 0x06002394 RID: 9108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E0")]
		[Address(RVA = "0x101218540", Offset = "0x1218540", VA = "0x101218540")]
		private void AddSetShownDirectSale(StoreManager.Product product)
		{
		}

		// Token: 0x06002395 RID: 9109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E1")]
		[Address(RVA = "0x101218DF8", Offset = "0x1218DF8", VA = "0x101218DF8")]
		private void SetShownDirectSale(Action onResponseCallback)
		{
		}

		// Token: 0x06002396 RID: 9110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E2")]
		[Address(RVA = "0x101218FDC", Offset = "0x1218FDC", VA = "0x101218FDC")]
		private void OnResponseSetShownDirectSale()
		{
		}

		// Token: 0x06002397 RID: 9111 RVA: 0x0000F378 File Offset: 0x0000D578
		[Token(Token = "0x60020E3")]
		[Address(RVA = "0x101218B78", Offset = "0x1218B78", VA = "0x101218B78")]
		private bool ExistSetShownExchangeShop()
		{
			return default(bool);
		}

		// Token: 0x06002398 RID: 9112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E4")]
		[Address(RVA = "0x1012186D4", Offset = "0x12186D4", VA = "0x1012186D4")]
		private void AddSetShownExchangeShop(StoreManager.Product product)
		{
		}

		// Token: 0x06002399 RID: 9113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E5")]
		[Address(RVA = "0x101218EE4", Offset = "0x1218EE4", VA = "0x101218EE4")]
		private void SetShownExchangeShop(Action onResponseCallback)
		{
		}

		// Token: 0x0600239A RID: 9114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020E6")]
		[Address(RVA = "0x101218FE8", Offset = "0x1218FE8", VA = "0x101218FE8")]
		private void OnResponseSetShownExchangeShop()
		{
		}

		// Token: 0x040033C0 RID: 13248
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002636")]
		private List<ShownProduct> m_ShownList;

		// Token: 0x040033C1 RID: 13249
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002637")]
		private List<int> m_ShownListDirectSale;

		// Token: 0x040033C2 RID: 13250
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002638")]
		private List<long> m_ShownListExchangeShop;

		// Token: 0x040033C3 RID: 13251
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002639")]
		private Action m_OnResponseCallback;

		// Token: 0x040033C4 RID: 13252
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400263A")]
		private Action m_OnResponseDirectSaleCallback;

		// Token: 0x040033C5 RID: 13253
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400263B")]
		private Action m_OnResponseExchangeShopCallback;

		// Token: 0x040033C6 RID: 13254
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400263C")]
		private Action m_OnResponseSetShownFlowCallback;
	}
}
