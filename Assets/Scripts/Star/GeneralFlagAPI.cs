﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000AD1 RID: 2769
	[Token(Token = "0x2000798")]
	[StructLayout(3)]
	public class GeneralFlagAPI
	{
		// Token: 0x14000050 RID: 80
		// (add) Token: 0x06003076 RID: 12406 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003077 RID: 12407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000050")]
		private event Action m_OnResponse_GeneralFlagSave
		{
			[Token(Token = "0x6002C4C")]
			[Address(RVA = "0x101218FF4", Offset = "0x1218FF4", VA = "0x101218FF4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002C4D")]
			[Address(RVA = "0x101219100", Offset = "0x1219100", VA = "0x101219100")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003078 RID: 12408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C4E")]
		[Address(RVA = "0x10121920C", Offset = "0x121920C", VA = "0x10121920C")]
		public void Request_GeneralFlagSave(eGeneralFlagType type, string data, Action callback)
		{
		}

		// Token: 0x06003079 RID: 12409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C4F")]
		[Address(RVA = "0x101219338", Offset = "0x1219338", VA = "0x101219338")]
		private void OnResponse_GeneralFlagSave(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600307A RID: 12410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C50")]
		[Address(RVA = "0x101219384", Offset = "0x1219384", VA = "0x101219384")]
		public GeneralFlagAPI()
		{
		}
	}
}
