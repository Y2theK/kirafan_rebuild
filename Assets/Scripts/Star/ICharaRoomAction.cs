﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000943 RID: 2371
	[Token(Token = "0x20006B4")]
	[StructLayout(3)]
	public interface ICharaRoomAction
	{
		// Token: 0x060027EB RID: 10219
		[Token(Token = "0x60024B5")]
		[Address(Slot = "0")]
		void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup);

		// Token: 0x060027EC RID: 10220
		[Token(Token = "0x60024B6")]
		[Address(Slot = "1")]
		bool UpdateObjEventMode(RoomObjectCtrlChara hbase);

		// Token: 0x060027ED RID: 10221
		[Token(Token = "0x60024B7")]
		[Address(Slot = "2")]
		bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel);

		// Token: 0x060027EE RID: 10222
		[Token(Token = "0x60024B8")]
		[Address(Slot = "3")]
		void Release();
	}
}
