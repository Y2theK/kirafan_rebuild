﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200071A RID: 1818
	[Token(Token = "0x200059D")]
	[StructLayout(3)]
	public class IDataBaseResource : IObjectResourceHandle
	{
		// Token: 0x06001A94 RID: 6804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001884")]
		[Address(RVA = "0x10121938C", Offset = "0x121938C", VA = "0x10121938C")]
		public void LoadResource(string filename, string extname)
		{
		}

		// Token: 0x06001A95 RID: 6805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001885")]
		[Address(RVA = "0x101219394", Offset = "0x1219394", VA = "0x101219394")]
		public void LoadResInManager(string filename, string extname)
		{
		}

		// Token: 0x06001A96 RID: 6806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001886")]
		[Address(RVA = "0x10121939C", Offset = "0x121939C", VA = "0x10121939C")]
		public IDataBaseResource()
		{
		}
	}
}
