﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020006D9 RID: 1753
	[Token(Token = "0x2000588")]
	[StructLayout(3)]
	public class IFldNetComManager : MonoBehaviour
	{
		// Token: 0x06001976 RID: 6518 RVA: 0x0000B7C0 File Offset: 0x000099C0
		[Token(Token = "0x60017EC")]
		[Address(RVA = "0x1012193FC", Offset = "0x12193FC", VA = "0x1012193FC")]
		public bool IsWorking()
		{
			return default(bool);
		}

		// Token: 0x06001977 RID: 6519 RVA: 0x0000B7D8 File Offset: 0x000099D8
		[Token(Token = "0x60017ED")]
		[Address(RVA = "0x10121941C", Offset = "0x121941C", VA = "0x10121941C")]
		public int GetNetComHandle()
		{
			return 0;
		}

		// Token: 0x06001978 RID: 6520 RVA: 0x0000B7F0 File Offset: 0x000099F0
		[Token(Token = "0x60017EE")]
		[Address(RVA = "0x1012194AC", Offset = "0x12194AC", VA = "0x1012194AC")]
		public int GetNetComModule()
		{
			return 0;
		}

		// Token: 0x06001979 RID: 6521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017EF")]
		[Address(RVA = "0x10121953C", Offset = "0x121953C", VA = "0x10121953C")]
		public INetComHandle AtNetComHandle(int idx)
		{
			return null;
		}

		// Token: 0x0600197A RID: 6522 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60017F0")]
		[Address(RVA = "0x10121960C", Offset = "0x121960C", VA = "0x10121960C")]
		public IFldNetComModule AtNetComModule(int idx)
		{
			return null;
		}

		// Token: 0x0600197B RID: 6523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F1")]
		[Address(RVA = "0x1012196DC", Offset = "0x12196DC", VA = "0x1012196DC")]
		public static void SetDebugMode(bool fset)
		{
		}

		// Token: 0x0600197C RID: 6524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F2")]
		[Address(RVA = "0x1012196E0", Offset = "0x12196E0", VA = "0x1012196E0")]
		protected void AwakeCom()
		{
		}

		// Token: 0x0600197D RID: 6525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F3")]
		[Address(RVA = "0x101219770", Offset = "0x1219770", VA = "0x101219770")]
		protected void DestroyCom()
		{
		}

		// Token: 0x0600197E RID: 6526 RVA: 0x0000B808 File Offset: 0x00009A08
		[Token(Token = "0x60017F4")]
		[Address(RVA = "0x1012197C0", Offset = "0x12197C0", VA = "0x1012197C0")]
		protected bool UpCom()
		{
			return default(bool);
		}

		// Token: 0x0600197F RID: 6527 RVA: 0x0000B820 File Offset: 0x00009A20
		[Token(Token = "0x60017F5")]
		[Address(RVA = "0x101219A20", Offset = "0x1219A20", VA = "0x101219A20")]
		protected bool UpModule()
		{
			return default(bool);
		}

		// Token: 0x06001980 RID: 6528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F6")]
		[Address(RVA = "0x101219C60", Offset = "0x1219C60", VA = "0x101219C60")]
		public void SendCom(INetComHandle phandle, INetComHandle.ResponseCallbak pcallback, bool fopenui = false)
		{
		}

		// Token: 0x06001981 RID: 6529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F7")]
		[Address(RVA = "0x101219E9C", Offset = "0x1219E9C", VA = "0x101219E9C")]
		public void AddModule(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06001982 RID: 6530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F8")]
		[Address(RVA = "0x10121A048", Offset = "0x121A048", VA = "0x10121A048")]
		public IFldNetComManager()
		{
		}

		// Token: 0x040029D8 RID: 10712
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002267")]
		private INetComHandle[] m_ComTable;

		// Token: 0x040029D9 RID: 10713
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002268")]
		protected int m_ComNum;

		// Token: 0x040029DA RID: 10714
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002269")]
		private int m_ComMax;

		// Token: 0x040029DB RID: 10715
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400226A")]
		private IFldNetComModule[] m_ModuleTable;

		// Token: 0x040029DC RID: 10716
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400226B")]
		protected int m_ModNum;

		// Token: 0x040029DD RID: 10717
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400226C")]
		private int m_ModMax;

		// Token: 0x040029DE RID: 10718
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400226D")]
		private bool m_OpenUI;

		// Token: 0x040029DF RID: 10719
		[Token(Token = "0x400226E")]
		public static IFldNetComManager Instance;
	}
}
