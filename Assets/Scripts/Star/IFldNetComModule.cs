﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006DA RID: 1754
	[Token(Token = "0x2000589")]
	[StructLayout(3)]
	public class IFldNetComModule
	{
		// Token: 0x06001983 RID: 6531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017F9")]
		[Address(RVA = "0x10121A050", Offset = "0x121A050", VA = "0x10121A050")]
		public IFldNetComModule(IFldNetComManager pmanager)
		{
		}

		// Token: 0x06001984 RID: 6532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017FA")]
		[Address(RVA = "0x10121A07C", Offset = "0x121A07C", VA = "0x10121A07C")]
		public void EntryCallback(IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x06001985 RID: 6533 RVA: 0x0000B838 File Offset: 0x00009A38
		[Token(Token = "0x60017FB")]
		[Address(RVA = "0x10121A084", Offset = "0x121A084", VA = "0x10121A084", Slot = "4")]
		public virtual bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x040029E0 RID: 10720
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400226F")]
		protected IFldNetComManager m_NetMng;

		// Token: 0x040029E1 RID: 10721
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002270")]
		public IFldNetComModule.CallBack m_Callback;

		// Token: 0x020006DB RID: 1755
		// (Invoke) Token: 0x06001987 RID: 6535
		[Token(Token = "0x2000E25")]
		public delegate void CallBack(IFldNetComModule pmodule);
	}
}
