﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public interface IMissionFunction {
        bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition);
        void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition);
    }
}
