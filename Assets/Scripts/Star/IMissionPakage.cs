﻿using System;
using System.Collections.Generic;

namespace Star {
    public class IMissionPakage {
        public long m_MissionID;
        public eMissionCategory m_Type;
        public eXlsMissionSeg m_ActionCategory;
        public int m_ExtensionScriptID;
        public string m_TargetMessage;
        public long m_KeyManageID;
        public long m_SubCodeID;
        public eMissionState m_State;
        public List<IMissionReward> m_RewardList = new List<IMissionReward>();
        public int m_Rate;
        public int m_RateBase;
        public DateTime m_LimitTime;
        public ushort m_PopupTimingNo;
        public eMissionTransit m_TransitScene;
        public int m_TransitSceneParam;
        public int m_uiPriority;

        public virtual string GetTargetMessage() {
            if (!string.IsNullOrEmpty(m_TargetMessage) && m_TargetMessage.Contains("{0}")) {
                return string.Format(m_TargetMessage, m_RateBase);
            }
            return m_TargetMessage;
        }

        public class IMissionReward {
            public eMissionRewardCategory m_Category;
            public int m_ItemNo;
            public int m_Num;
        }
    }
}
