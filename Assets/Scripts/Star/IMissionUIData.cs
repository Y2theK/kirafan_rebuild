﻿using System;
using System.Collections.Generic;

namespace Star {
    public class IMissionUIData {
        public eMissionCategory m_Type;
        public string m_TargetMessage;
        public long m_ManageID;
        public long m_DatabaseID;
        public int m_Rate;
        public int m_RateMax;
        public eMissionState m_State;
        public DateTime m_LimitTime;
        public int m_Num;
        public int m_uiPriority;
        public List<IMissionPakage.IMissionReward> m_RewardList = new List<IMissionPakage.IMissionReward>();
        public bool m_Completed;
        public int m_SortedIdx;
    }
}
