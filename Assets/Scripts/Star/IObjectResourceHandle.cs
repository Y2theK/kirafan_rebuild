﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020006FC RID: 1788
	[Token(Token = "0x200058E")]
	[StructLayout(3)]
	public class IObjectResourceHandle
	{
		// Token: 0x06001A05 RID: 6661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600181A")]
		[Address(RVA = "0x1012193CC", Offset = "0x12193CC", VA = "0x1012193CC")]
		public IObjectResourceHandle()
		{
		}

		// Token: 0x06001A06 RID: 6662 RVA: 0x0000BA90 File Offset: 0x00009C90
		[Token(Token = "0x600181B")]
		[Address(RVA = "0x10121A78C", Offset = "0x121A78C", VA = "0x10121A78C")]
		public bool IsDone()
		{
			return default(bool);
		}

		// Token: 0x06001A07 RID: 6663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600181C")]
		[Address(RVA = "0x10121A794", Offset = "0x121A794", VA = "0x10121A794")]
		public void Release()
		{
		}

		// Token: 0x06001A08 RID: 6664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600181D")]
		[Address(RVA = "0x10121A7CC", Offset = "0x121A7CC", VA = "0x10121A7CC", Slot = "4")]
		public virtual void SetUpResource(UnityEngine.Object pres)
		{
		}

		// Token: 0x04002A46 RID: 10822
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400227F")]
		public bool m_Active;

		// Token: 0x04002A47 RID: 10823
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4002280")]
		public bool m_ReadWait;

		// Token: 0x04002A48 RID: 10824
		[Cpp2IlInjected.FieldOffset(Offset = "0x12")]
		[Token(Token = "0x4002281")]
		public bool m_Web;

		// Token: 0x04002A49 RID: 10825
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002282")]
		public int m_MappingNo;

		// Token: 0x04002A4A RID: 10826
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002283")]
		public int m_UseNum;

		// Token: 0x04002A4B RID: 10827
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002284")]
		public uint m_UID;

		// Token: 0x04002A4C RID: 10828
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002285")]
		public string m_FileName;

		// Token: 0x04002A4D RID: 10829
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002286")]
		public string m_FileExt;

		// Token: 0x04002A4E RID: 10830
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002287")]
		public UnityEngine.Object Obj;

		// Token: 0x04002A4F RID: 10831
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002288")]
		public MeigeResource.Handler m_Handle;
	}
}
