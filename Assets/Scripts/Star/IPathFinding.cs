﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200090F RID: 2319
	[Token(Token = "0x20006A1")]
	[StructLayout(3)]
	public abstract class IPathFinding
	{
		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06002652 RID: 9810 RVA: 0x000103F8 File Offset: 0x0000E5F8
		// (set) Token: 0x06002653 RID: 9811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000273")]
		public bool Finding
		{
			[Token(Token = "0x6002353")]
			[Address(RVA = "0x10121A7D0", Offset = "0x121A7D0", VA = "0x10121A7D0")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002354")]
			[Address(RVA = "0x10121A7D8", Offset = "0x121A7D8", VA = "0x10121A7D8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06002654 RID: 9812 RVA: 0x00010410 File Offset: 0x0000E610
		// (set) Token: 0x06002655 RID: 9813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000274")]
		public int SleepTime
		{
			[Token(Token = "0x6002355")]
			[Address(RVA = "0x10121A7E0", Offset = "0x121A7E0", VA = "0x10121A7E0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002356")]
			[Address(RVA = "0x10121A7E8", Offset = "0x121A7E8", VA = "0x10121A7E8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06002656 RID: 9814 RVA: 0x00010428 File Offset: 0x0000E628
		// (set) Token: 0x06002657 RID: 9815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000275")]
		public int FindingLimitCost
		{
			[Token(Token = "0x6002357")]
			[Address(RVA = "0x10121A7F0", Offset = "0x121A7F0", VA = "0x10121A7F0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002358")]
			[Address(RVA = "0x10121A7F8", Offset = "0x121A7F8", VA = "0x10121A7F8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06002658 RID: 9816 RVA: 0x00010440 File Offset: 0x0000E640
		// (set) Token: 0x06002659 RID: 9817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000276")]
		public int UsableCost
		{
			[Token(Token = "0x6002359")]
			[Address(RVA = "0x10121A800", Offset = "0x121A800", VA = "0x10121A800")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600235A")]
			[Address(RVA = "0x10121A808", Offset = "0x121A808", VA = "0x10121A808")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x0600265A RID: 9818 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000277")]
		public List<Vector2> Path
		{
			[Token(Token = "0x600235B")]
			[Address(RVA = "0x10121A810", Offset = "0x121A810", VA = "0x10121A810")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600235C")]
		[Address(RVA = "0x10121A818", Offset = "0x121A818", VA = "0x10121A818", Slot = "4")]
		public virtual void SetData(int[,] costData)
		{
		}

		// Token: 0x0600265C RID: 9820
		[Token(Token = "0x600235D")]
		[Address(Slot = "5")]
		public abstract List<Vector2> Find(Vector2 v1, Vector2 v2);

		// Token: 0x0600265D RID: 9821
		[Token(Token = "0x600235E")]
		[Address(Slot = "6")]
		public abstract void FindAsync(Vector2 v1, Vector2 v2);

		// Token: 0x0600265E RID: 9822
		[Token(Token = "0x600235F")]
		[Address(Slot = "7")]
		public abstract void Abort();

		// Token: 0x0600265F RID: 9823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002360")]
		[Address(RVA = "0x10121A820", Offset = "0x121A820", VA = "0x10121A820")]
		protected IPathFinding()
		{
		}

		// Token: 0x04003671 RID: 13937
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40027CC")]
		protected List<Vector2> m_Path;

		// Token: 0x04003672 RID: 13938
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40027CD")]
		protected int[,] m_CostData;
	}
}
