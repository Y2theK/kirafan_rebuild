﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009F0 RID: 2544
	[Token(Token = "0x2000722")]
	[StructLayout(3)]
	public class IRoomCharaAct
	{
		// Token: 0x06002A89 RID: 10889 RVA: 0x000120A8 File Offset: 0x000102A8
		[Token(Token = "0x6002718")]
		[Address(RVA = "0x10121A828", Offset = "0x121A828", VA = "0x10121A828", Slot = "4")]
		public virtual bool UpdateMode(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x06002A8A RID: 10890 RVA: 0x000120C0 File Offset: 0x000102C0
		[Token(Token = "0x6002719")]
		[Address(RVA = "0x10121AA18", Offset = "0x121AA18", VA = "0x10121AA18", Slot = "5")]
		public virtual bool IsMoveCost(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x06002A8B RID: 10891 RVA: 0x000120D8 File Offset: 0x000102D8
		[Token(Token = "0x600271A")]
		[Address(RVA = "0x10121AAC8", Offset = "0x121AAC8", VA = "0x10121AAC8", Slot = "6")]
		public virtual bool IsMoveCost(RoomObjectCtrlChara hbase, Vector2 pos)
		{
			return default(bool);
		}

		// Token: 0x06002A8C RID: 10892 RVA: 0x000120F0 File Offset: 0x000102F0
		[Token(Token = "0x600271B")]
		[Address(RVA = "0x10121AB34", Offset = "0x121AB34", VA = "0x10121AB34", Slot = "7")]
		public virtual bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return default(bool);
		}

		// Token: 0x06002A8D RID: 10893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600271C")]
		[Address(RVA = "0x10121AB3C", Offset = "0x121AB3C", VA = "0x10121AB3C", Slot = "8")]
		public virtual void TweetForceOff()
		{
		}

		// Token: 0x06002A8E RID: 10894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600271D")]
		[Address(RVA = "0x10121AC24", Offset = "0x121AC24", VA = "0x10121AC24")]
		public IRoomCharaAct()
		{
		}

		// Token: 0x04003AC2 RID: 15042
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A50")]
		public float m_tweetTimer;

		// Token: 0x04003AC3 RID: 15043
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002A51")]
		public int m_VoiceReqID;
	}
}
