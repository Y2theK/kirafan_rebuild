﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x0200099E RID: 2462
	[Token(Token = "0x20006FE")]
	[StructLayout(3)]
	public class IRoomCharaManager : MonoBehaviour
	{
		// Token: 0x060028C4 RID: 10436 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002586")]
		[Address(RVA = "0x10121AC34", Offset = "0x121AC34", VA = "0x10121AC34")]
		public static IRoomCharaManager LinkUpManager(GameObject plink, RoomBuilderBase pbuilder, long fplayerid, bool fmode)
		{
			return null;
		}

		// Token: 0x060028C5 RID: 10437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002587")]
		[Address(RVA = "0x10121ACE8", Offset = "0x121ACE8", VA = "0x10121ACE8", Slot = "4")]
		public virtual void SetLinkManager(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x060028C6 RID: 10438 RVA: 0x000112C8 File Offset: 0x0000F4C8
		[Token(Token = "0x6002588")]
		[Address(RVA = "0x10121AD24", Offset = "0x121AD24", VA = "0x10121AD24")]
		public bool IsAcitveResource()
		{
			return default(bool);
		}

		// Token: 0x060028C7 RID: 10439 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002589")]
		[Address(RVA = "0x10121AD54", Offset = "0x121AD54", VA = "0x10121AD54")]
		public MeigeResource.Handler GetShadowResource()
		{
			return null;
		}

		// Token: 0x060028C8 RID: 10440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600258A")]
		[Address(RVA = "0x10121AD5C", Offset = "0x121AD5C", VA = "0x10121AD5C", Slot = "5")]
		public virtual void BackLinkManager()
		{
		}

		// Token: 0x060028C9 RID: 10441 RVA: 0x000112E0 File Offset: 0x0000F4E0
		[Token(Token = "0x600258B")]
		[Address(RVA = "0x10121AD8C", Offset = "0x121AD8C", VA = "0x10121AD8C", Slot = "6")]
		public virtual bool IsBuild()
		{
			return default(bool);
		}

		// Token: 0x060028CA RID: 10442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600258C")]
		[Address(RVA = "0x10121AD94", Offset = "0x121AD94", VA = "0x10121AD94", Slot = "7")]
		public virtual void ResetState()
		{
		}

		// Token: 0x060028CB RID: 10443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600258D")]
		[Address(RVA = "0x10121AD98", Offset = "0x121AD98", VA = "0x10121AD98", Slot = "8")]
		public virtual void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
		}

		// Token: 0x060028CC RID: 10444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600258E")]
		[Address(RVA = "0x10121AD9C", Offset = "0x121AD9C", VA = "0x10121AD9C", Slot = "9")]
		public virtual void SetUpRoomCharacter()
		{
		}

		// Token: 0x060028CD RID: 10445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600258F")]
		[Address(RVA = "0x10121ADA0", Offset = "0x121ADA0", VA = "0x10121ADA0", Slot = "10")]
		public virtual void DeleteChara()
		{
		}

		// Token: 0x060028CE RID: 10446 RVA: 0x000112F8 File Offset: 0x0000F4F8
		[Token(Token = "0x6002590")]
		[Address(RVA = "0x10121ADA4", Offset = "0x121ADA4", VA = "0x10121ADA4", Slot = "11")]
		public virtual bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x060028CF RID: 10447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002591")]
		[Address(RVA = "0x10121ADAC", Offset = "0x121ADAC", VA = "0x10121ADAC", Slot = "12")]
		public virtual void DeleteCharacter(long fmanageid, bool fcutout = false)
		{
		}

		// Token: 0x060028D0 RID: 10448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002592")]
		[Address(RVA = "0x10121ADB0", Offset = "0x121ADB0", VA = "0x10121ADB0", Slot = "13")]
		public virtual void RefreshCharaCall()
		{
		}

		// Token: 0x060028D1 RID: 10449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002593")]
		[Address(RVA = "0x10121ADBC", Offset = "0x121ADBC", VA = "0x10121ADBC")]
		public void StopAction(bool fstop)
		{
		}

		// Token: 0x060028D2 RID: 10450 RVA: 0x00011310 File Offset: 0x0000F510
		[Token(Token = "0x6002594")]
		[Address(RVA = "0x10121ADC0", Offset = "0x121ADC0", VA = "0x10121ADC0")]
		public bool CheckAction(RoomGreet.eCategory fcategory, long fmanageid)
		{
			return default(bool);
		}

		// Token: 0x060028D3 RID: 10451 RVA: 0x00011328 File Offset: 0x0000F528
		[Token(Token = "0x6002595")]
		[Address(RVA = "0x10121AE7C", Offset = "0x121AE7C", VA = "0x10121AE7C", Slot = "14")]
		public virtual int GetCharaPakageMax()
		{
			return 0;
		}

		// Token: 0x060028D4 RID: 10452 RVA: 0x00011340 File Offset: 0x0000F540
		[Token(Token = "0x6002596")]
		[Address(RVA = "0x10121AE84", Offset = "0x121AE84", VA = "0x10121AE84", Slot = "15")]
		public virtual bool IsCharaExist(long managedID)
		{
			return default(bool);
		}

		// Token: 0x060028D5 RID: 10453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002597")]
		[Address(RVA = "0x10121AE8C", Offset = "0x121AE8C", VA = "0x10121AE8C")]
		public IRoomCharaManager()
		{
		}

		// Token: 0x0400393B RID: 14651
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002980")]
		public RoomBuilderBase m_Builder;

		// Token: 0x0400393C RID: 14652
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002981")]
		protected MeigeResource.Handler m_ShadowHndl;
	}
}
