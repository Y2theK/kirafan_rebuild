﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A48 RID: 2632
	[Token(Token = "0x2000755")]
	[StructLayout(3)]
	public class IRoomEventCommand
	{
		// Token: 0x06002D65 RID: 11621 RVA: 0x00013668 File Offset: 0x00011868
		[Token(Token = "0x60029C2")]
		[Address(RVA = "0x10121AE94", Offset = "0x121AE94", VA = "0x10121AE94", Slot = "4")]
		public virtual bool CalcRequest(RoomBuilder pbuild)
		{
			return default(bool);
		}

		// Token: 0x06002D66 RID: 11622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029C3")]
		[Address(RVA = "0x10121AE9C", Offset = "0x121AE9C", VA = "0x10121AE9C")]
		public IRoomEventCommand()
		{
		}

		// Token: 0x04003CEE RID: 15598
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BEB")]
		public eRoomRequest m_Type;

		// Token: 0x04003CEF RID: 15599
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002BEC")]
		public bool m_Enable;
	}
}
