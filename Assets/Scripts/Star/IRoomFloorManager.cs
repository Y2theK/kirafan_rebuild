﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009EC RID: 2540
	[Token(Token = "0x2000720")]
	[StructLayout(3)]
	public class IRoomFloorManager
	{
		// Token: 0x06002A4E RID: 10830 RVA: 0x00011FA0 File Offset: 0x000101A0
		[Token(Token = "0x60026DF")]
		[Address(RVA = "0x10121AEA4", Offset = "0x121AEA4", VA = "0x10121AEA4")]
		public Color GetBaseColor()
		{
			return default(Color);
		}

		// Token: 0x06002A4F RID: 10831 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026E0")]
		[Address(RVA = "0x10121AEB0", Offset = "0x121AEB0", VA = "0x10121AEB0")]
		public static IRoomFloorManager CreateFloorManager(RoomFloorCateory pcategory)
		{
			return null;
		}

		// Token: 0x06002A50 RID: 10832 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026E1")]
		[Address(RVA = "0x10121B0E8", Offset = "0x121B0E8", VA = "0x10121B0E8")]
		public GameObject GetRoot()
		{
			return null;
		}

		// Token: 0x06002A51 RID: 10833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026E2")]
		[Address(RVA = "0x10121B0F0", Offset = "0x121B0F0", VA = "0x10121B0F0")]
		public void CreateObject(Transform pparent, bool fmakewall)
		{
		}

		// Token: 0x06002A52 RID: 10834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026E3")]
		[Address(RVA = "0x10121B23C", Offset = "0x121B23C", VA = "0x10121B23C")]
		public void GetMoveArea(out IVector4 prect, out IVector2 psize, bool fcategory)
		{
		}

		// Token: 0x06002A53 RID: 10835 RVA: 0x00011FB8 File Offset: 0x000101B8
		[Token(Token = "0x60026E4")]
		[Address(RVA = "0x10121B280", Offset = "0x121B280", VA = "0x10121B280")]
		public IVector3 GetMoveSize()
		{
			return default(IVector3);
		}

		// Token: 0x06002A54 RID: 10836 RVA: 0x00011FD0 File Offset: 0x000101D0
		[Token(Token = "0x60026E5")]
		[Address(RVA = "0x10121B28C", Offset = "0x121B28C", VA = "0x10121B28C")]
		public Vector2 GetOffsetPos()
		{
			return default(Vector2);
		}

		// Token: 0x06002A55 RID: 10837 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026E6")]
		[Address(RVA = "0x10121B294", Offset = "0x121B294", VA = "0x10121B294")]
		public GameObject CreateFloorNode()
		{
			return null;
		}

		// Token: 0x06002A56 RID: 10838 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026E7")]
		[Address(RVA = "0x10121B39C", Offset = "0x121B39C", VA = "0x10121B39C")]
		public GameObject CreateFloorNode2()
		{
			return null;
		}

		// Token: 0x06002A57 RID: 10839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026E8")]
		[Address(RVA = "0x10121B508", Offset = "0x121B508", VA = "0x10121B508")]
		public void SetFloor(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002A58 RID: 10840 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026E9")]
		[Address(RVA = "0x10121B5DC", Offset = "0x121B5DC", VA = "0x10121B5DC")]
		public IRoomObjectControll GetFloor()
		{
			return null;
		}

		// Token: 0x06002A59 RID: 10841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026EA")]
		[Address(RVA = "0x10121B5E4", Offset = "0x121B5E4", VA = "0x10121B5E4")]
		public void ChangeFloor(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002A5A RID: 10842 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026EB")]
		[Address(RVA = "0x10121B7E4", Offset = "0x121B7E4", VA = "0x10121B7E4")]
		public GameObject CreateWallNode()
		{
			return null;
		}

		// Token: 0x06002A5B RID: 10843 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026EC")]
		[Address(RVA = "0x10121B8EC", Offset = "0x121B8EC", VA = "0x10121B8EC")]
		public GameObject CreateWallNode2()
		{
			return null;
		}

		// Token: 0x06002A5C RID: 10844 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026ED")]
		[Address(RVA = "0x10121BA58", Offset = "0x121BA58", VA = "0x10121BA58")]
		public GameObject CreateObjectsNode()
		{
			return null;
		}

		// Token: 0x06002A5D RID: 10845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026EE")]
		[Address(RVA = "0x10121BD38", Offset = "0x121BD38", VA = "0x10121BD38")]
		public void ArrangementObjectsNode()
		{
		}

		// Token: 0x06002A5E RID: 10846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026EF")]
		[Address(RVA = "0x10121BF30", Offset = "0x121BF30", VA = "0x10121BF30")]
		public void SetWall(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002A5F RID: 10847 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026F0")]
		[Address(RVA = "0x10121C004", Offset = "0x121C004", VA = "0x10121C004")]
		public IRoomObjectControll GetWall()
		{
			return null;
		}

		// Token: 0x06002A60 RID: 10848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F1")]
		[Address(RVA = "0x10121C00C", Offset = "0x121C00C", VA = "0x10121C00C")]
		public void ChangeWall(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002A61 RID: 10849 RVA: 0x00011FE8 File Offset: 0x000101E8
		[Token(Token = "0x60026F2")]
		[Address(RVA = "0x10121C208", Offset = "0x121C208", VA = "0x10121C208")]
		public bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06002A62 RID: 10850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F3")]
		[Address(RVA = "0x10121C304", Offset = "0x121C304", VA = "0x10121C304")]
		public void BuildUp(int fgroupno)
		{
		}

		// Token: 0x06002A63 RID: 10851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F4")]
		[Address(RVA = "0x10121D8D0", Offset = "0x121D8D0", VA = "0x10121D8D0")]
		public void SetSortOrder(float forderz)
		{
		}

		// Token: 0x06002A64 RID: 10852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F5")]
		[Address(RVA = "0x10121DAA4", Offset = "0x121DAA4", VA = "0x10121DAA4")]
		public void Destroy()
		{
		}

		// Token: 0x06002A65 RID: 10853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F6")]
		[Address(RVA = "0x10121DBCC", Offset = "0x121DBCC", VA = "0x10121DBCC")]
		public void SetGridView(RoomBuilderBase.eEditObject fedit, bool fview)
		{
		}

		// Token: 0x06002A66 RID: 10854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F7")]
		[Address(RVA = "0x10121CA9C", Offset = "0x121CA9C", VA = "0x10121CA9C")]
		public void TilingFloor(Vector2 foffset)
		{
		}

		// Token: 0x06002A67 RID: 10855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F8")]
		[Address(RVA = "0x10121DDB4", Offset = "0x121DDB4", VA = "0x10121DDB4")]
		private void BuildFloorCollider(GameObject pobj, Vector2 foffset, int fpointx, int fpointy)
		{
		}

		// Token: 0x06002A68 RID: 10856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026F9")]
		[Address(RVA = "0x10121E640", Offset = "0x121E640", VA = "0x10121E640")]
		private void BuildWallCollider(GameObject pobj, bool fside, Vector2 foffset, int fpointx, int fpointy)
		{
		}

		// Token: 0x06002A69 RID: 10857 RVA: 0x00012000 File Offset: 0x00010200
		[Token(Token = "0x60026FA")]
		[Address(RVA = "0x10121F128", Offset = "0x121F128", VA = "0x10121F128")]
		public bool CheckLineCross(Vector3 fstpt1, Vector3 fedpt1, Vector3 fstpt2, Vector3 fedpt2)
		{
			return default(bool);
		}

		// Token: 0x06002A6A RID: 10858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026FB")]
		[Address(RVA = "0x10121F1B0", Offset = "0x121F1B0", VA = "0x10121F1B0")]
		public void CalcLineToPerpendicular(ref Vector3 pans, Vector3 fpoint, Vector3 fstpt, Vector3 fedpt)
		{
		}

		// Token: 0x06002A6B RID: 10859 RVA: 0x00012018 File Offset: 0x00010218
		[Token(Token = "0x60026FC")]
		[Address(RVA = "0x10121F2D4", Offset = "0x121F2D4", VA = "0x10121F2D4")]
		public Vector2 CalcGridPos(float fpoint, Vector2 fst, Vector2 fend)
		{
			return default(Vector2);
		}

		// Token: 0x06002A6C RID: 10860 RVA: 0x00012030 File Offset: 0x00010230
		[Token(Token = "0x60026FD")]
		[Address(RVA = "0x10121F37C", Offset = "0x121F37C", VA = "0x10121F37C")]
		public bool CheckCrossLine(ref IVector3 pout, Vector3 fstartpt, Vector3 fendpt, bool fcategory)
		{
			return default(bool);
		}

		// Token: 0x06002A6D RID: 10861 RVA: 0x00012048 File Offset: 0x00010248
		[Token(Token = "0x60026FE")]
		[Address(RVA = "0x10121FA40", Offset = "0x121FA40", VA = "0x10121FA40")]
		public int GetObjectListNum()
		{
			return 0;
		}

		// Token: 0x06002A6E RID: 10862 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026FF")]
		[Address(RVA = "0x10121FA48", Offset = "0x121FA48", VA = "0x10121FA48")]
		public IRoomObjectControll GetIndexToHandle(int findex)
		{
			return null;
		}

		// Token: 0x06002A6F RID: 10863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002700")]
		[Address(RVA = "0x10121B1DC", Offset = "0x121B1DC", VA = "0x10121B1DC")]
		public void SetUpList()
		{
		}

		// Token: 0x06002A70 RID: 10864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002701")]
		[Address(RVA = "0x10121FAB4", Offset = "0x121FAB4", VA = "0x10121FAB4")]
		public void AttachSortObject(IRoomObjectControll pobj, int fcompkey, bool isChara = false)
		{
		}

		// Token: 0x06002A71 RID: 10865 RVA: 0x00012060 File Offset: 0x00010260
		[Token(Token = "0x6002702")]
		[Address(RVA = "0x10121FE34", Offset = "0x121FE34", VA = "0x10121FE34")]
		private int AssignCompKeyIndex(int baseIndex)
		{
			return 0;
		}

		// Token: 0x06002A72 RID: 10866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002703")]
		[Address(RVA = "0x10121FEE4", Offset = "0x121FEE4", VA = "0x10121FEE4")]
		public void DetachObject(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002A73 RID: 10867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002704")]
		[Address(RVA = "0x101220134", Offset = "0x1220134", VA = "0x101220134")]
		private void ListSort()
		{
		}

		// Token: 0x06002A74 RID: 10868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002705")]
		[Address(RVA = "0x101220970", Offset = "0x1220970", VA = "0x101220970")]
		public void BuildSortingOrder(int flayergroup, int forder)
		{
		}

		// Token: 0x06002A75 RID: 10869 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002706")]
		[Address(RVA = "0x101220B84", Offset = "0x1220B84", VA = "0x101220B84")]
		public IRoomObjectControll GetObject(int findex)
		{
			return null;
		}

		// Token: 0x06002A76 RID: 10870 RVA: 0x00012078 File Offset: 0x00010278
		[Token(Token = "0x6002707")]
		[Address(RVA = "0x101220BF0", Offset = "0x1220BF0", VA = "0x101220BF0")]
		public int GetFreeNum()
		{
			return 0;
		}

		// Token: 0x06002A77 RID: 10871 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002708")]
		[Address(RVA = "0x101220CA8", Offset = "0x1220CA8", VA = "0x101220CA8")]
		public IRoomObjectControll GetFreeObject(RoomObjectCtrlChara pchara, int fkeyindex = -1)
		{
			return null;
		}

		// Token: 0x06002A78 RID: 10872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002709")]
		[Address(RVA = "0x10121B078", Offset = "0x121B078", VA = "0x10121B078")]
		public IRoomFloorManager()
		{
		}

		// Token: 0x04003A95 RID: 14997
		[Token(Token = "0x4002A30")]
		private const int FLOOR_WALL_BLOCK_X = 0;

		// Token: 0x04003A96 RID: 14998
		[Token(Token = "0x4002A31")]
		private const int FLOOR_WALL_BLOCK_Y = 0;

		// Token: 0x04003A97 RID: 14999
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A32")]
		public RoomFloorCateory.eCategory m_Type;

		// Token: 0x04003A98 RID: 15000
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002A33")]
		public float m_PosX;

		// Token: 0x04003A99 RID: 15001
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A34")]
		public float m_PosY;

		// Token: 0x04003A9A RID: 15002
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002A35")]
		public int m_SizeX;

		// Token: 0x04003A9B RID: 15003
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A36")]
		public int m_SizeY;

		// Token: 0x04003A9C RID: 15004
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002A37")]
		public int m_SizeZ;

		// Token: 0x04003A9D RID: 15005
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A38")]
		public Vector2 m_RoomOffset;

		// Token: 0x04003A9E RID: 15006
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A39")]
		public Vector2 m_WallOffset;

		// Token: 0x04003A9F RID: 15007
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A3A")]
		public Vector3 m_WallLocalPos;

		// Token: 0x04003AA0 RID: 15008
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002A3B")]
		public Vector3 m_FloorLocalPos;

		// Token: 0x04003AA1 RID: 15009
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002A3C")]
		private GameObject m_Root;

		// Token: 0x04003AA2 RID: 15010
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002A3D")]
		private Vector2 m_BuildPosOffset;

		// Token: 0x04003AA3 RID: 15011
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002A3E")]
		private RoomObjectMdlSprite m_Floor;

		// Token: 0x04003AA4 RID: 15012
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002A3F")]
		private RoomObjectMdlSprite m_Wall;

		// Token: 0x04003AA5 RID: 15013
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002A40")]
		private Material m_RenderMaterial;

		// Token: 0x04003AA6 RID: 15014
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002A41")]
		private GameObject m_HitTileGroup;

		// Token: 0x04003AA7 RID: 15015
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002A42")]
		private GameObject m_HitWallGroup;

		// Token: 0x04003AA8 RID: 15016
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002A43")]
		private Vector3[] m_HitTileLine;

		// Token: 0x04003AA9 RID: 15017
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002A44")]
		private Vector2[] m_HitTileGrid;

		// Token: 0x04003AAA RID: 15018
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002A45")]
		private Vector3[] m_HitWallLine;

		// Token: 0x04003AAB RID: 15019
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002A46")]
		private Vector2[] m_HitWallGrid;

		// Token: 0x04003AAC RID: 15020
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002A47")]
		private List<GameObject> m_TilingFloors;

		// Token: 0x04003AAD RID: 15021
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002A48")]
		private Color m_BasePer;

		// Token: 0x04003AAE RID: 15022
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002A49")]
		public IRoomFloorManager.RoomSortObject[] m_List;

		// Token: 0x04003AAF RID: 15023
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002A4A")]
		public int m_MaxList;

		// Token: 0x04003AB0 RID: 15024
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4002A4B")]
		public int m_EntryNum;

		// Token: 0x020009ED RID: 2541
		[Token(Token = "0x2000F9A")]
		public class RoomSortObject
		{
			// Token: 0x06002A79 RID: 10873 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006029")]
			[Address(RVA = "0x10121FD80", Offset = "0x121FD80", VA = "0x10121FD80")]
			public RoomSortObject(IRoomObjectControll pobj)
			{
			}

			// Token: 0x06002A7A RID: 10874 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600602A")]
			[Address(RVA = "0x101220780", Offset = "0x1220780", VA = "0x101220780")]
			public void UpdateIsoBounds()
			{
			}

			// Token: 0x04003AB1 RID: 15025
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063D6")]
			public RoomBlockData m_Block;

			// Token: 0x04003AB2 RID: 15026
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40063D7")]
			public RoomIso.IsoBounds m_Box;

			// Token: 0x04003AB3 RID: 15027
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40063D8")]
			public RoomIso.IsoNamedVerts m_NameVert;

			// Token: 0x04003AB4 RID: 15028
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x40063D9")]
			public RoomIso.IsoVerts m_Vert;

			// Token: 0x04003AB5 RID: 15029
			[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
			[Token(Token = "0x40063DA")]
			public float m_Key;

			// Token: 0x04003AB6 RID: 15030
			[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
			[Token(Token = "0x40063DB")]
			public int m_CompKey;

			// Token: 0x04003AB7 RID: 15031
			[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
			[Token(Token = "0x40063DC")]
			public IRoomObjectControll m_Object;
		}
	}
}
