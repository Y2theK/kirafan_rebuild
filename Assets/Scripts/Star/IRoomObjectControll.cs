﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009FD RID: 2557
	[Token(Token = "0x200072E")]
	[StructLayout(3)]
	public class IRoomObjectControll : MonoBehaviour
	{
		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06002AB5 RID: 10933 RVA: 0x000122E8 File Offset: 0x000104E8
		[Token(Token = "0x170002AE")]
		public int baseSortingOrder
		{
			[Token(Token = "0x6002744")]
			[Address(RVA = "0x101220E48", Offset = "0x1220E48", VA = "0x101220E48")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06002AB6 RID: 10934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002745")]
		[Address(RVA = "0x101220E50", Offset = "0x1220E50", VA = "0x101220E50", Slot = "4")]
		protected virtual void StartPreparing()
		{
		}

		// Token: 0x06002AB7 RID: 10935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002746")]
		[Address(RVA = "0x101220E5C", Offset = "0x1220E5C", VA = "0x101220E5C", Slot = "5")]
		protected virtual void DonePreparing()
		{
		}

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06002AB8 RID: 10936 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002AF")]
		public Transform trs
		{
			[Token(Token = "0x6002747")]
			[Address(RVA = "0x101220FE4", Offset = "0x1220FE4", VA = "0x101220FE4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06002AB9 RID: 10937 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002B0")]
		public Transform CacheTransform
		{
			[Token(Token = "0x6002748")]
			[Address(RVA = "0x101220FEC", Offset = "0x1220FEC", VA = "0x101220FEC")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06002ABA RID: 10938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002B1")]
		public RoomBlockData BlockData
		{
			[Token(Token = "0x6002749")]
			[Address(RVA = "0x10121AAC0", Offset = "0x121AAC0", VA = "0x10121AAC0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06002ABB RID: 10939 RVA: 0x00012300 File Offset: 0x00010500
		[Token(Token = "0x170002B2")]
		public int FloorID
		{
			[Token(Token = "0x600274A")]
			[Address(RVA = "0x101220FF4", Offset = "0x1220FF4", VA = "0x101220FF4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06002ABC RID: 10940 RVA: 0x00012318 File Offset: 0x00010518
		[Token(Token = "0x170002B3")]
		public eRoomObjectCategory ObjCategory
		{
			[Token(Token = "0x600274B")]
			[Address(RVA = "0x101220FFC", Offset = "0x1220FFC", VA = "0x101220FFC")]
			get
			{
				return eRoomObjectCategory.Desk;
			}
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06002ABD RID: 10941 RVA: 0x00012330 File Offset: 0x00010530
		[Token(Token = "0x170002B4")]
		public byte ObjGroupKey
		{
			[Token(Token = "0x600274C")]
			[Address(RVA = "0x101221004", Offset = "0x1221004", VA = "0x101221004")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06002ABE RID: 10942 RVA: 0x00012348 File Offset: 0x00010548
		[Token(Token = "0x170002B5")]
		public int ObjID
		{
			[Token(Token = "0x600274D")]
			[Address(RVA = "0x10122100C", Offset = "0x122100C", VA = "0x10122100C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06002ABF RID: 10943 RVA: 0x00012360 File Offset: 0x00010560
		[Token(Token = "0x170002B6")]
		public long ManageID
		{
			[Token(Token = "0x600274E")]
			[Address(RVA = "0x101221014", Offset = "0x1221014", VA = "0x101221014")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06002AC0 RID: 10944 RVA: 0x00012378 File Offset: 0x00010578
		[Token(Token = "0x600274F")]
		[Address(RVA = "0x10122101C", Offset = "0x122101C", VA = "0x10122101C")]
		public bool IsActive()
		{
			return default(bool);
		}

		// Token: 0x06002AC1 RID: 10945 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002750")]
		[Address(RVA = "0x101221024", Offset = "0x1221024", VA = "0x101221024")]
		public RoomBuilderBase GetBuilder()
		{
			return null;
		}

		// Token: 0x06002AC2 RID: 10946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002751")]
		[Address(RVA = "0x10122102C", Offset = "0x122102C", VA = "0x10122102C")]
		public void SetManageID(long fmanageid)
		{
		}

		// Token: 0x06002AC3 RID: 10947 RVA: 0x00012390 File Offset: 0x00010590
		[Token(Token = "0x6002752")]
		[Address(RVA = "0x101221034", Offset = "0x1221034", VA = "0x101221034")]
		public int GetSortCompKey()
		{
			return 0;
		}

		// Token: 0x06002AC4 RID: 10948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002753")]
		[Address(RVA = "0x10122103C", Offset = "0x122103C", VA = "0x10122103C")]
		public void LinkObjectHandle(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002AC5 RID: 10949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002754")]
		[Address(RVA = "0x101221044", Offset = "0x1221044", VA = "0x101221044", Slot = "6")]
		public virtual void ReleaseObj()
		{
		}

		// Token: 0x06002AC6 RID: 10950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002755")]
		[Address(RVA = "0x10122104C", Offset = "0x122104C", VA = "0x10122104C")]
		protected void Awake()
		{
		}

		// Token: 0x06002AC7 RID: 10951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002756")]
		[Address(RVA = "0x10122115C", Offset = "0x122115C", VA = "0x10122115C")]
		protected void Update()
		{
		}

		// Token: 0x06002AC8 RID: 10952 RVA: 0x000123A8 File Offset: 0x000105A8
		[Token(Token = "0x6002757")]
		[Address(RVA = "0x10121C2FC", Offset = "0x121C2FC", VA = "0x10121C2FC")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06002AC9 RID: 10953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002758")]
		[Address(RVA = "0x1012211E8", Offset = "0x12211E8", VA = "0x1012211E8")]
		public void Prepare()
		{
		}

		// Token: 0x06002ACA RID: 10954 RVA: 0x000123C0 File Offset: 0x000105C0
		[Token(Token = "0x6002759")]
		[Address(RVA = "0x101221240", Offset = "0x1221240", VA = "0x101221240")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06002ACB RID: 10955 RVA: 0x000123D8 File Offset: 0x000105D8
		[Token(Token = "0x600275A")]
		[Address(RVA = "0x101221248", Offset = "0x1221248", VA = "0x101221248")]
		public bool IsDestoryProc()
		{
			return default(bool);
		}

		// Token: 0x06002ACC RID: 10956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600275B")]
		[Address(RVA = "0x101221250", Offset = "0x1221250", VA = "0x101221250", Slot = "7")]
		public virtual void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06002ACD RID: 10957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600275C")]
		[Address(RVA = "0x101221254", Offset = "0x1221254", VA = "0x101221254")]
		public void SetLayerPos(float fposz)
		{
		}

		// Token: 0x06002ACE RID: 10958 RVA: 0x000123F0 File Offset: 0x000105F0
		[Token(Token = "0x600275D")]
		[Address(RVA = "0x1012212CC", Offset = "0x12212CC", VA = "0x1012212CC", Slot = "8")]
		public virtual bool IsAction()
		{
			return default(bool);
		}

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06002ACF RID: 10959 RVA: 0x00012408 File Offset: 0x00010608
		// (set) Token: 0x06002AD0 RID: 10960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002B7")]
		public bool enableAction
		{
			[Token(Token = "0x600275E")]
			[Address(RVA = "0x1012212D4", Offset = "0x12212D4", VA = "0x1012212D4")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600275F")]
			[Address(RVA = "0x1012212DC", Offset = "0x12212DC", VA = "0x1012212DC")]
			set
			{
			}
		}

		// Token: 0x06002AD1 RID: 10961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002760")]
		[Address(RVA = "0x1012212E4", Offset = "0x12212E4", VA = "0x1012212E4", Slot = "9")]
		public virtual void LinkKeyLock(bool flock)
		{
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002761")]
		[Address(RVA = "0x1012212E8", Offset = "0x12212E8", VA = "0x1012212E8", Slot = "10")]
		public virtual void SetAttachLink(bool flock)
		{
		}

		// Token: 0x06002AD3 RID: 10963 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002762")]
		[Address(RVA = "0x1012212EC", Offset = "0x12212EC", VA = "0x1012212EC", Slot = "11")]
		public virtual Transform GetAttachTransform(Vector2 flinkpos)
		{
			return null;
		}

		// Token: 0x06002AD4 RID: 10964 RVA: 0x00012420 File Offset: 0x00010620
		[Token(Token = "0x6002763")]
		[Address(RVA = "0x1012212F4", Offset = "0x12212F4", VA = "0x1012212F4", Slot = "12")]
		public virtual bool IsInRange(int gridX, int gridY, int fdir)
		{
			return default(bool);
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002764")]
		[Address(RVA = "0x1012212FC", Offset = "0x12212FC", VA = "0x1012212FC", Slot = "13")]
		public virtual void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06002AD6 RID: 10966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002765")]
		[Address(RVA = "0x101221300", Offset = "0x1221300", VA = "0x101221300", Slot = "14")]
		public virtual void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002AD7 RID: 10967 RVA: 0x00012438 File Offset: 0x00010638
		[Token(Token = "0x6002766")]
		[Address(RVA = "0x101221308", Offset = "0x1221308", VA = "0x101221308", Slot = "15")]
		public virtual bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002AD8 RID: 10968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002767")]
		[Address(RVA = "0x101221310", Offset = "0x1221310", VA = "0x101221310", Slot = "16")]
		public virtual void SetActionSendObj(IRoomObjectControll plink)
		{
		}

		// Token: 0x06002AD9 RID: 10969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002768")]
		[Address(RVA = "0x101221314", Offset = "0x1221314", VA = "0x101221314", Slot = "17")]
		public virtual void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06002ADA RID: 10970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002769")]
		[Address(RVA = "0x101221318", Offset = "0x1221318", VA = "0x101221318", Slot = "18")]
		public virtual void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600276A")]
		[Address(RVA = "0x101221484", Offset = "0x1221484", VA = "0x101221484", Slot = "19")]
		public virtual void SetGridPosition(int fgridx, int fgridy, IRoomFloorManager floorManager)
		{
		}

		// Token: 0x06002ADC RID: 10972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600276B")]
		[Address(RVA = "0x1012215D0", Offset = "0x12215D0", VA = "0x1012215D0", Slot = "20")]
		public virtual void UpdateObj()
		{
		}

		// Token: 0x06002ADD RID: 10973 RVA: 0x00012450 File Offset: 0x00010650
		[Token(Token = "0x600276C")]
		[Address(RVA = "0x1012215D4", Offset = "0x12215D4", VA = "0x1012215D4")]
		public eRoomObjectEventType GetEventType(RoomObjectCtrlChara pbase)
		{
			return eRoomObjectEventType.Sit;
		}

		// Token: 0x06002ADE RID: 10974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600276D")]
		[Address(RVA = "0x101221938", Offset = "0x1221938", VA = "0x101221938")]
		public void GetEventParameter(RoomEventParameter pparam, RoomObjectCtrlChara pbase)
		{
		}

		// Token: 0x06002ADF RID: 10975 RVA: 0x00012468 File Offset: 0x00010668
		[Token(Token = "0x600276E")]
		[Address(RVA = "0x101221AE0", Offset = "0x1221AE0", VA = "0x101221AE0")]
		public CharacterDefine.eDir GetDir()
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002AE0 RID: 10976 RVA: 0x00012480 File Offset: 0x00010680
		[Token(Token = "0x600276F")]
		[Address(RVA = "0x101221AE8", Offset = "0x1221AE8", VA = "0x101221AE8")]
		public CharacterDefine.eDir FlipDir()
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002AE1 RID: 10977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002770")]
		[Address(RVA = "0x101221B18", Offset = "0x1221B18", VA = "0x101221B18")]
		public void SetDir(CharacterDefine.eDir dir)
		{
		}

		// Token: 0x06002AE2 RID: 10978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002771")]
		[Address(RVA = "0x101221EB0", Offset = "0x1221EB0", VA = "0x101221EB0")]
		private void BaseUpDirFunc()
		{
		}

		// Token: 0x06002AE3 RID: 10979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002772")]
		[Address(RVA = "0x101221EB4", Offset = "0x1221EB4", VA = "0x101221EB4")]
		public void UpdateDir()
		{
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x00012498 File Offset: 0x00010698
		[Token(Token = "0x6002773")]
		[Address(RVA = "0x101221EE0", Offset = "0x1221EE0", VA = "0x101221EE0", Slot = "21")]
		public virtual IVector2 GetPosToSize(int fchkposx, int fchkposy)
		{
			return default(IVector2);
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x000124B0 File Offset: 0x000106B0
		[Token(Token = "0x6002774")]
		[Address(RVA = "0x101221F04", Offset = "0x1221F04", VA = "0x101221F04")]
		public bool IsHasDepth()
		{
			return default(bool);
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x000124C8 File Offset: 0x000106C8
		[Token(Token = "0x6002775")]
		[Address(RVA = "0x101221F28", Offset = "0x1221F28", VA = "0x101221F28")]
		public int GetDepth()
		{
			return 0;
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002776")]
		[Address(RVA = "0x101221F48", Offset = "0x1221F48", VA = "0x101221F48", Slot = "22")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06002AE8 RID: 10984 RVA: 0x000124E0 File Offset: 0x000106E0
		[Token(Token = "0x6002777")]
		[Address(RVA = "0x101221F84", Offset = "0x1221F84", VA = "0x101221F84", Slot = "23")]
		public virtual bool CheckWithProcDestory()
		{
			return default(bool);
		}

		// Token: 0x06002AE9 RID: 10985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002778")]
		[Address(RVA = "0x101222010", Offset = "0x1222010", VA = "0x101222010", Slot = "24")]
		public virtual void SetBaseColor(Color fcolor)
		{
		}

		// Token: 0x06002AEA RID: 10986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002779")]
		[Address(RVA = "0x101222014", Offset = "0x1222014", VA = "0x101222014", Slot = "25")]
		public virtual void SetAlphaScale(float scale)
		{
		}

		// Token: 0x06002AEB RID: 10987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277A")]
		[Address(RVA = "0x101222030", Offset = "0x1222030", VA = "0x101222030", Slot = "26")]
		public virtual void SetSeleting(bool flg, bool isDisable = false)
		{
		}

		// Token: 0x06002AEC RID: 10988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277B")]
		[Address(RVA = "0x101222034", Offset = "0x1222034", VA = "0x101222034")]
		public void SetLayer(int layerNo)
		{
		}

		// Token: 0x06002AED RID: 10989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277C")]
		[Address(RVA = "0x101222134", Offset = "0x1222134", VA = "0x101222134", Slot = "27")]
		public virtual void PlayMotion(int fmotionid, WrapMode fmode, float fspeed)
		{
		}

		// Token: 0x06002AEE RID: 10990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277D")]
		[Address(RVA = "0x101222138", Offset = "0x1222138", VA = "0x101222138", Slot = "28")]
		public virtual void ResetAnim()
		{
		}

		// Token: 0x06002AEF RID: 10991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277E")]
		[Address(RVA = "0x10122213C", Offset = "0x122213C", VA = "0x10122213C", Slot = "29")]
		public virtual void PauseAnime()
		{
		}

		// Token: 0x06002AF0 RID: 10992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600277F")]
		[Address(RVA = "0x101222140", Offset = "0x1222140", VA = "0x101222140", Slot = "30")]
		public virtual void ResetAnimeFrame()
		{
		}

		// Token: 0x06002AF1 RID: 10993 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002780")]
		[Address(RVA = "0x101222144", Offset = "0x1222144", VA = "0x101222144", Slot = "31")]
		public virtual string GetPlayingAnim()
		{
			return null;
		}

		// Token: 0x06002AF2 RID: 10994 RVA: 0x000124F8 File Offset: 0x000106F8
		[Token(Token = "0x6002781")]
		[Address(RVA = "0x10122214C", Offset = "0x122214C", VA = "0x10122214C", Slot = "32")]
		public virtual bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return default(bool);
		}

		// Token: 0x06002AF3 RID: 10995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002782")]
		[Address(RVA = "0x101222154", Offset = "0x1222154", VA = "0x101222154")]
		public void GridStateBase(RoomGridState pgrid, int fupstate)
		{
		}

		// Token: 0x06002AF4 RID: 10996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002783")]
		[Address(RVA = "0x101222250", Offset = "0x1222250", VA = "0x101222250")]
		public void SortingOrderBase(float flayerpos, int sortingOrder, bool isLinked)
		{
		}

		// Token: 0x06002AF5 RID: 10997 RVA: 0x00012510 File Offset: 0x00010710
		[Token(Token = "0x6002784")]
		[Address(RVA = "0x1012222DC", Offset = "0x12222DC", VA = "0x1012222DC", Slot = "33")]
		public virtual bool IsCharaCheck(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AF6 RID: 10998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002785")]
		[Address(RVA = "0x1012222E4", Offset = "0x12222E4", VA = "0x1012222E4", Slot = "34")]
		public virtual void CreateHitAreaModel(bool fhitcolor)
		{
		}

		// Token: 0x06002AF7 RID: 10999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002786")]
		[Address(RVA = "0x1012222E8", Offset = "0x12222E8", VA = "0x1012222E8", Slot = "35")]
		public virtual void DestroyHitAreaModel()
		{
		}

		// Token: 0x06002AF8 RID: 11000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002787")]
		[Address(RVA = "0x1012222EC", Offset = "0x12222EC", VA = "0x1012222EC", Slot = "36")]
		public virtual void AbortLinkEventObj()
		{
		}

		// Token: 0x06002AF9 RID: 11001 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002788")]
		[Address(RVA = "0x1012222F0", Offset = "0x12222F0", VA = "0x1012222F0", Slot = "37")]
		public virtual IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			return null;
		}

		// Token: 0x06002AFA RID: 11002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002789")]
		[Address(RVA = "0x1012222F8", Offset = "0x12222F8", VA = "0x1012222F8", Slot = "38")]
		public virtual void SetSortKey(string ptargetname, int fsortkey, int fsortkeyOffset = 0)
		{
		}

		// Token: 0x06002AFB RID: 11003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600278A")]
		[Address(RVA = "0x1012222FC", Offset = "0x12222FC", VA = "0x1012222FC", Slot = "39")]
		public virtual void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
		}

		// Token: 0x06002AFC RID: 11004 RVA: 0x00012528 File Offset: 0x00010728
		[Token(Token = "0x600278B")]
		[Address(RVA = "0x10122169C", Offset = "0x122169C", VA = "0x10122169C")]
		protected bool CheckEventBrunchCondition(RoomObjectCtrlChara pbase, RoomObjectListDB_Param pObjParam)
		{
			return default(bool);
		}

		// Token: 0x06002AFD RID: 11005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600278C")]
		[Address(RVA = "0x101222300", Offset = "0x1222300", VA = "0x101222300")]
		public IRoomObjectControll()
		{
		}

		// Token: 0x04003AD9 RID: 15065
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A63")]
		protected Transform m_OwnerTrs;

		// Token: 0x04003ADA RID: 15066
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A64")]
		protected RoomBlockData m_BlockData;

		// Token: 0x04003ADB RID: 15067
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A65")]
		protected RoomBuilderBase m_Builder;

		// Token: 0x04003ADC RID: 15068
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A66")]
		protected int m_FloorID;

		// Token: 0x04003ADD RID: 15069
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A67")]
		protected long m_ManageID;

		// Token: 0x04003ADE RID: 15070
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002A68")]
		protected int m_SortCompKey;

		// Token: 0x04003ADF RID: 15071
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002A69")]
		protected int m_BaseSortingOrder;

		// Token: 0x04003AE0 RID: 15072
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002A6A")]
		protected Color m_BaseColor;

		// Token: 0x04003AE1 RID: 15073
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002A6B")]
		protected float m_AlphaScale;

		// Token: 0x04003AE2 RID: 15074
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002A6C")]
		protected IRoomObjectResource m_RoomObjResource;

		// Token: 0x04003AE3 RID: 15075
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002A6D")]
		protected bool m_IsAvailable;

		// Token: 0x04003AE4 RID: 15076
		[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
		[Token(Token = "0x4002A6E")]
		private bool m_IsPreparing;

		// Token: 0x04003AE5 RID: 15077
		[Cpp2IlInjected.FieldOffset(Offset = "0x6A")]
		[Token(Token = "0x4002A6F")]
		private bool m_IsDestoryProc;

		// Token: 0x04003AE6 RID: 15078
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002A70")]
		protected List<Transform> m_SubObjectListForOffset;

		// Token: 0x04003AE7 RID: 15079
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002A71")]
		protected eRoomObjectCategory m_Category;

		// Token: 0x04003AE8 RID: 15080
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4002A72")]
		protected byte m_ObjGroupKey;

		// Token: 0x04003AE9 RID: 15081
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002A73")]
		protected int m_ObjID;

		// Token: 0x04003AEA RID: 15082
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4002A74")]
		protected CharacterDefine.eDir m_Dir;

		// Token: 0x04003AEB RID: 15083
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002A75")]
		protected int m_DefaultSizeX;

		// Token: 0x04003AEC RID: 15084
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002A76")]
		protected int m_DefaultSizeY;

		// Token: 0x04003AED RID: 15085
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002A77")]
		protected int m_DefaultDepth;

		// Token: 0x04003AEE RID: 15086
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4002A78")]
		protected bool m_Active;

		// Token: 0x04003AEF RID: 15087
		[Cpp2IlInjected.FieldOffset(Offset = "0x95")]
		[Token(Token = "0x4002A79")]
		protected bool m_isActiveLinkObject;

		// Token: 0x04003AF0 RID: 15088
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002A7A")]
		protected IRoomObjectControll.UpdateDirFunc m_UpDirFunc;

		// Token: 0x04003AF1 RID: 15089
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002A7B")]
		public IRoomObjectControll.GridStateFunc UpGridState;

		// Token: 0x04003AF2 RID: 15090
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002A7C")]
		public IRoomObjectControll.SortingOrderFunc SetSortingOrder;

		// Token: 0x04003AF3 RID: 15091
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002A7D")]
		private bool m_EnableAction;

		// Token: 0x020009FE RID: 2558
		[Token(Token = "0x2000F9D")]
		public class ObjTouchState
		{
			// Token: 0x06002AFE RID: 11006 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600602B")]
			[Address(RVA = "0x1012229CC", Offset = "0x12229CC", VA = "0x1012229CC")]
			public ObjTouchState(RoomGameCamera pcamera)
			{
			}

			// Token: 0x06002AFF RID: 11007 RVA: 0x00012540 File Offset: 0x00010740
			[Token(Token = "0x600602C")]
			[Address(RVA = "0x1012229F8", Offset = "0x12229F8", VA = "0x1012229F8")]
			public bool IsMoveSlide()
			{
				return default(bool);
			}

			// Token: 0x04003AF4 RID: 15092
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063E7")]
			public IVector3 m_Grid;

			// Token: 0x04003AF5 RID: 15093
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40063E8")]
			public Vector2 m_TouchStart;

			// Token: 0x04003AF6 RID: 15094
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40063E9")]
			public Vector2 m_NowTouch;

			// Token: 0x04003AF7 RID: 15095
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40063EA")]
			public RoomGameCamera m_Camera;
		}

		// Token: 0x020009FF RID: 2559
		[Token(Token = "0x2000F9E")]
		public struct ModelRenderConfig
		{
			// Token: 0x04003AF8 RID: 15096
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063EB")]
			public short m_RenderSort;

			// Token: 0x04003AF9 RID: 15097
			[Cpp2IlInjected.FieldOffset(Offset = "0x2")]
			[Token(Token = "0x40063EC")]
			public short m_RenderSortOffset;

			// Token: 0x04003AFA RID: 15098
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40063ED")]
			public float m_OffsetZ;

			// Token: 0x04003AFB RID: 15099
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40063EE")]
			public Renderer m_RenderObject;
		}

		// Token: 0x02000A00 RID: 2560
		// (Invoke) Token: 0x06002B01 RID: 11009
		[Token(Token = "0x2000F9F")]
		protected delegate void UpdateDirFunc();

		// Token: 0x02000A01 RID: 2561
		// (Invoke) Token: 0x06002B05 RID: 11013
		[Token(Token = "0x2000FA0")]
		public delegate void GridStateFunc(RoomGridState pgrid, int fupstate);

		// Token: 0x02000A02 RID: 2562
		// (Invoke) Token: 0x06002B09 RID: 11017
		[Token(Token = "0x2000FA1")]
		public delegate void SortingOrderFunc(float flayerpos, int sortid, bool isLinked);
	}
}
