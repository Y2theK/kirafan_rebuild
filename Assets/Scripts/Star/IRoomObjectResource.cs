﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A1C RID: 2588
	[Token(Token = "0x200073C")]
	[StructLayout(3)]
	public class IRoomObjectResource
	{
		// Token: 0x06002C14 RID: 11284 RVA: 0x00012C18 File Offset: 0x00010E18
		[Token(Token = "0x6002883")]
		[Address(RVA = "0x1012211E0", Offset = "0x12211E0", VA = "0x1012211E0")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06002C15 RID: 11285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002884")]
		[Address(RVA = "0x101222BB0", Offset = "0x1222BB0", VA = "0x101222BB0")]
		public void SetDisableAutoRetry(bool flg)
		{
		}

		// Token: 0x06002C16 RID: 11286 RVA: 0x00012C30 File Offset: 0x00010E30
		[Token(Token = "0x6002885")]
		[Address(RVA = "0x101222BB8", Offset = "0x1222BB8", VA = "0x101222BB8")]
		public bool IsDisableAutoRetry()
		{
			return default(bool);
		}

		// Token: 0x06002C17 RID: 11287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002886")]
		[Address(RVA = "0x101222BC0", Offset = "0x1222BC0", VA = "0x101222BC0", Slot = "4")]
		public virtual void Setup(IRoomObjectControll hndl)
		{
		}

		// Token: 0x06002C18 RID: 11288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002887")]
		[Address(RVA = "0x101222BC4", Offset = "0x1222BC4", VA = "0x101222BC4", Slot = "5")]
		public virtual void Prepare()
		{
		}

		// Token: 0x06002C19 RID: 11289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002888")]
		[Address(RVA = "0x101222BD0", Offset = "0x1222BD0", VA = "0x101222BD0", Slot = "6")]
		public virtual void UpdateRes()
		{
		}

		// Token: 0x06002C1A RID: 11290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002889")]
		[Address(RVA = "0x101222BD4", Offset = "0x1222BD4", VA = "0x101222BD4", Slot = "7")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06002C1B RID: 11291 RVA: 0x00012C48 File Offset: 0x00010E48
		[Token(Token = "0x600288A")]
		[Address(RVA = "0x101222BD8", Offset = "0x1222BD8", VA = "0x101222BD8", Slot = "8")]
		public virtual bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06002C1C RID: 11292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600288B")]
		[Address(RVA = "0x101222BE0", Offset = "0x1222BE0", VA = "0x101222BE0")]
		public IRoomObjectResource()
		{
		}

		// Token: 0x04003BAC RID: 15276
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B09")]
		protected bool m_IsPreparing;

		// Token: 0x04003BAD RID: 15277
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4002B0A")]
		protected bool m_DisableAutoRetry;
	}
}
