﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A4A RID: 2634
	[Token(Token = "0x2000757")]
	[StructLayout(3)]
	public class IRoomPartsAction
	{
		// Token: 0x06002D67 RID: 11623 RVA: 0x00013680 File Offset: 0x00011880
		[Token(Token = "0x60029C4")]
		[Address(RVA = "0x101222BE8", Offset = "0x1222BE8", VA = "0x101222BE8", Slot = "4")]
		public virtual bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x06002D68 RID: 11624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029C5")]
		[Address(RVA = "0x101222BF0", Offset = "0x1222BF0", VA = "0x101222BF0", Slot = "5")]
		public virtual void PartsCancel()
		{
		}

		// Token: 0x06002D69 RID: 11625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029C6")]
		[Address(RVA = "0x101222BF4", Offset = "0x1222BF4", VA = "0x101222BF4")]
		public IRoomPartsAction()
		{
		}

		// Token: 0x04003CF3 RID: 15603
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BF0")]
		public eRoomPartsAction m_Type;

		// Token: 0x04003CF4 RID: 15604
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002BF1")]
		public uint m_UID;

		// Token: 0x04003CF5 RID: 15605
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BF2")]
		public bool m_Active;
	}
}
