﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200094C RID: 2380
	[Token(Token = "0x20006B9")]
	[StructLayout(3)]
	public class IRoomScriptData
	{
		// Token: 0x060027FA RID: 10234 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60024C4")]
		[Address(RVA = "0x101222BFC", Offset = "0x1222BFC", VA = "0x101222BFC")]
		public static IRoomScriptData CreateScriptData(eRoomScriptCommand ftype)
		{
			return null;
		}

		// Token: 0x060027FB RID: 10235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C5")]
		[Address(RVA = "0x101222D48", Offset = "0x1222D48", VA = "0x101222D48", Slot = "4")]
		public virtual void CopyDat(ActXlsKeyBase pbase)
		{
		}

		// Token: 0x060027FC RID: 10236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C6")]
		[Address(RVA = "0x101222D88", Offset = "0x1222D88", VA = "0x101222D88")]
		public IRoomScriptData()
		{
		}

		// Token: 0x0400381D RID: 14365
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400289A")]
		public eRoomScriptCommand m_Command;

		// Token: 0x0400381E RID: 14366
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400289B")]
		public float m_Time;

		// Token: 0x0400381F RID: 14367
		[Token(Token = "0x400289C")]
		public static Type[] ms_SciptUpTable;
	}
}
