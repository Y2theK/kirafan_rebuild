﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AFF RID: 2815
	[Token(Token = "0x20007B1")]
	[StructLayout(3)]
	public class ITownCameraKey
	{
		// Token: 0x060031C0 RID: 12736 RVA: 0x000153D8 File Offset: 0x000135D8
		[Token(Token = "0x6002D79")]
		[Address(RVA = "0x1012239D8", Offset = "0x12239D8", VA = "0x1012239D8", Slot = "4")]
		public virtual bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x060031C1 RID: 12737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7A")]
		[Address(RVA = "0x1012239E0", Offset = "0x12239E0", VA = "0x1012239E0")]
		public ITownCameraKey()
		{
		}

		// Token: 0x04004134 RID: 16692
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E4F")]
		public eTownCameraAction m_Type;

		// Token: 0x04004135 RID: 16693
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002E50")]
		public int m_UID;

		// Token: 0x04004136 RID: 16694
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E51")]
		public bool m_Active;

		// Token: 0x04004137 RID: 16695
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E52")]
		public TownCamera m_Camera;
	}
}
