﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BA2 RID: 2978
	[Token(Token = "0x2000818")]
	[StructLayout(3)]
	public class ITownEventCommand
	{
		// Token: 0x0600344E RID: 13390 RVA: 0x00016338 File Offset: 0x00014538
		[Token(Token = "0x6002FB7")]
		[Address(RVA = "0x1012239E8", Offset = "0x12239E8", VA = "0x1012239E8", Slot = "4")]
		public virtual bool CalcRequest(TownBuilder pbuild)
		{
			return default(bool);
		}

		// Token: 0x0600344F RID: 13391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FB8")]
		[Address(RVA = "0x1012239F0", Offset = "0x12239F0", VA = "0x1012239F0")]
		public ITownEventCommand()
		{
		}

		// Token: 0x0400445E RID: 17502
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400306B")]
		public eTownRequest m_Type;

		// Token: 0x0400445F RID: 17503
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400306C")]
		public bool m_Enable;
	}
}
