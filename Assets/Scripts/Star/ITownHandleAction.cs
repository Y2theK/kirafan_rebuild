﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B2C RID: 2860
	[Token(Token = "0x20007D3")]
	[StructLayout(3)]
	public class ITownHandleAction
	{
		// Token: 0x0600324D RID: 12877 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E00")]
		[Address(RVA = "0x1012239F8", Offset = "0x12239F8", VA = "0x1012239F8")]
		public static ITownHandleAction CreateHandleAction(ITownObjectHandler phandle, eTownEventAct faction)
		{
			return null;
		}

		// Token: 0x0600324E RID: 12878 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E01")]
		[Address(RVA = "0x101223A94", Offset = "0x1223A94", VA = "0x101223A94")]
		public static ITownHandleAction CreateAction(eTownEventAct faction)
		{
			return null;
		}

		// Token: 0x0600324F RID: 12879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E02")]
		[Address(RVA = "0x101223B04", Offset = "0x1223B04", VA = "0x101223B04")]
		public ITownHandleAction()
		{
		}

		// Token: 0x040041FD RID: 16893
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002EE4")]
		public eTownEventAct m_Type;

		// Token: 0x040041FE RID: 16894
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002EE5")]
		public int m_AccessID;
	}
}
