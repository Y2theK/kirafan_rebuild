﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B36 RID: 2870
	[Token(Token = "0x20007DD")]
	[StructLayout(3)]
	public interface ITownMenuAct
	{
		// Token: 0x06003259 RID: 12889
		[Token(Token = "0x6002E0C")]
		[Address(Slot = "0")]
		void SetUp(TownBuilder pbuild);

		// Token: 0x0600325A RID: 12890
		[Token(Token = "0x6002E0D")]
		[Address(Slot = "1")]
		void UpdateMenu(TownObjHandleMenu pmenu);
	}
}
