﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B79 RID: 2937
	[Token(Token = "0x20007FF")]
	[StructLayout(3)]
	public class ITownObjectHandler : MonoBehaviour
	{
		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06003363 RID: 13155 RVA: 0x00015C00 File Offset: 0x00013E00
		// (set) Token: 0x06003362 RID: 13154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700039F")]
		public virtual bool isUnderConstruction
		{
			[Token(Token = "0x6002EE1")]
			[Address(RVA = "0x101223B10", Offset = "0x1223B10", VA = "0x101223B10", Slot = "5")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002EE0")]
			[Address(RVA = "0x101223B0C", Offset = "0x1223B0C", VA = "0x101223B0C", Slot = "4")]
			set
			{
			}
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x06003364 RID: 13156 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170003A0")]
		public Transform CacheTransform
		{
			[Token(Token = "0x6002EE2")]
			[Address(RVA = "0x101223B18", Offset = "0x1223B18", VA = "0x101223B18")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x06003365 RID: 13157 RVA: 0x00015C18 File Offset: 0x00013E18
		[Token(Token = "0x170003A1")]
		public bool IsEnableRender
		{
			[Token(Token = "0x6002EE3")]
			[Address(RVA = "0x101223B20", Offset = "0x1223B20", VA = "0x101223B20")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x06003366 RID: 13158 RVA: 0x00015C30 File Offset: 0x00013E30
		[Token(Token = "0x170003A2")]
		public TownUtility.eResCategory HandleCategory
		{
			[Token(Token = "0x6002EE4")]
			[Address(RVA = "0x101223B28", Offset = "0x1223B28", VA = "0x101223B28")]
			get
			{
				return TownUtility.eResCategory.Field;
			}
		}

		// Token: 0x06003367 RID: 13159 RVA: 0x00015C48 File Offset: 0x00013E48
		[Token(Token = "0x6002EE5")]
		[Address(RVA = "0x101223A8C", Offset = "0x1223A8C", VA = "0x101223A8C")]
		public int GetBuildAccessID()
		{
			return 0;
		}

		// Token: 0x06003368 RID: 13160 RVA: 0x00015C60 File Offset: 0x00013E60
		[Token(Token = "0x6002EE6")]
		[Address(RVA = "0x101223B30", Offset = "0x1223B30", VA = "0x101223B30")]
		public long GetManageID()
		{
			return 0L;
		}

		// Token: 0x06003369 RID: 13161 RVA: 0x00015C78 File Offset: 0x00013E78
		[Token(Token = "0x6002EE7")]
		[Address(RVA = "0x101223B38", Offset = "0x1223B38", VA = "0x101223B38")]
		public int GetObjID()
		{
			return 0;
		}

		// Token: 0x0600336A RID: 13162 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002EE8")]
		[Address(RVA = "0x101223B40", Offset = "0x1223B40", VA = "0x101223B40")]
		public static ITownObjectHandler CreateHandler(GameObject pobj, TownUtility.eResCategory fcategory, TownBuilder pbuilder)
		{
			return null;
		}

		// Token: 0x0600336B RID: 13163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EE9")]
		[Address(RVA = "0x101223D74", Offset = "0x1223D74", VA = "0x101223D74")]
		public static void CopyHandle(GameObject pobj, ITownObjectHandler phandle)
		{
		}

		// Token: 0x0600336C RID: 13164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EEA")]
		[Address(RVA = "0x101223EB8", Offset = "0x1223EB8", VA = "0x101223EB8")]
		public void SetCallback(TownBuildUpBufParam.BuildUpEvent pcallback)
		{
		}

		// Token: 0x0600336D RID: 13165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EEB")]
		[Address(RVA = "0x101223EC0", Offset = "0x1223EC0", VA = "0x101223EC0")]
		private void Awake()
		{
		}

		// Token: 0x0600336E RID: 13166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EEC")]
		[Address(RVA = "0x101223F18", Offset = "0x1223F18", VA = "0x101223F18")]
		private void Update()
		{
		}

		// Token: 0x0600336F RID: 13167 RVA: 0x00015C90 File Offset: 0x00013E90
		[Token(Token = "0x6002EED")]
		[Address(RVA = "0x101223F64", Offset = "0x1223F64", VA = "0x101223F64")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06003370 RID: 13168 RVA: 0x00015CA8 File Offset: 0x00013EA8
		[Token(Token = "0x6002EEE")]
		[Address(RVA = "0x101223F6C", Offset = "0x1223F6C", VA = "0x101223F6C")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06003371 RID: 13169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EEF")]
		[Address(RVA = "0x101223F74", Offset = "0x1223F74", VA = "0x101223F74", Slot = "6")]
		public virtual void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x06003372 RID: 13170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF0")]
		[Address(RVA = "0x101223F8C", Offset = "0x1223F8C", VA = "0x101223F8C", Slot = "7")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06003373 RID: 13171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF1")]
		[Address(RVA = "0x101223FFC", Offset = "0x1223FFC", VA = "0x101223FFC", Slot = "8")]
		public virtual void Prepare()
		{
		}

		// Token: 0x06003374 RID: 13172 RVA: 0x00015CC0 File Offset: 0x00013EC0
		[Token(Token = "0x6002EF2")]
		[Address(RVA = "0x101224014", Offset = "0x1224014", VA = "0x101224014", Slot = "9")]
		protected virtual bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x06003375 RID: 13173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF3")]
		[Address(RVA = "0x101224024", Offset = "0x1224024", VA = "0x101224024", Slot = "10")]
		public virtual void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06003376 RID: 13174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF4")]
		[Address(RVA = "0x10122402C", Offset = "0x122402C", VA = "0x10122402C")]
		public void SetLayer(int layerNo)
		{
		}

		// Token: 0x06003377 RID: 13175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF5")]
		[Address(RVA = "0x10122412C", Offset = "0x122412C", VA = "0x10122412C", Slot = "11")]
		public virtual void DestroyRequest()
		{
		}

		// Token: 0x06003378 RID: 13176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EF6")]
		[Address(RVA = "0x101224130", Offset = "0x1224130", VA = "0x101224130")]
		public void MakeColliderLink(GameObject ptarget)
		{
		}

		// Token: 0x06003379 RID: 13177 RVA: 0x00015CD8 File Offset: 0x00013ED8
		[Token(Token = "0x6002EF7")]
		[Address(RVA = "0x101224270", Offset = "0x1224270", VA = "0x101224270")]
		public byte GetTimeKey()
		{
			return 0;
		}

		// Token: 0x0600337A RID: 13178 RVA: 0x00015CF0 File Offset: 0x00013EF0
		[Token(Token = "0x6002EF8")]
		[Address(RVA = "0x101224278", Offset = "0x1224278", VA = "0x101224278")]
		public int GetResourceKey()
		{
			return 0;
		}

		// Token: 0x0600337B RID: 13179 RVA: 0x00015D08 File Offset: 0x00013F08
		[Token(Token = "0x6002EF9")]
		[Address(RVA = "0x101224280", Offset = "0x1224280", VA = "0x101224280", Slot = "12")]
		public virtual bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x0600337C RID: 13180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EFA")]
		[Address(RVA = "0x101224288", Offset = "0x1224288", VA = "0x101224288")]
		public void LinkHitToNode(GameObject ptarget)
		{
		}

		// Token: 0x0600337D RID: 13181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EFB")]
		[Address(RVA = "0x1012243BC", Offset = "0x12243BC", VA = "0x1012243BC")]
		public static void LinkHitMesh(Transform ptrs)
		{
		}

		// Token: 0x0600337E RID: 13182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EFC")]
		[Address(RVA = "0x101224544", Offset = "0x1224544", VA = "0x101224544")]
		public void SetColliderActive(GameObject ptarget, bool factive)
		{
		}

		// Token: 0x0600337F RID: 13183 RVA: 0x00015D20 File Offset: 0x00013F20
		[Token(Token = "0x6002EFD")]
		[Address(RVA = "0x101224620", Offset = "0x1224620", VA = "0x101224620", Slot = "13")]
		public virtual bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003380 RID: 13184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EFE")]
		[Address(RVA = "0x101224628", Offset = "0x1224628", VA = "0x101224628")]
		public ITownObjectHandler()
		{
		}

		// Token: 0x04004368 RID: 17256
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002FBF")]
		protected Transform m_Transform;

		// Token: 0x04004369 RID: 17257
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002FC0")]
		protected TownBuilder m_Builder;

		// Token: 0x0400436A RID: 17258
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002FC1")]
		protected bool m_IsAvailable;

		// Token: 0x0400436B RID: 17259
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4002FC2")]
		protected bool m_IsPreparing;

		// Token: 0x0400436C RID: 17260
		[Cpp2IlInjected.FieldOffset(Offset = "0x2A")]
		[Token(Token = "0x4002FC3")]
		protected bool m_IsEnableRender;

		// Token: 0x0400436D RID: 17261
		[Cpp2IlInjected.FieldOffset(Offset = "0x2B")]
		[Token(Token = "0x4002FC4")]
		protected bool m_InitUp;

		// Token: 0x0400436E RID: 17262
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002FC5")]
		protected TownUtility.eResCategory m_HandleCategory;

		// Token: 0x0400436F RID: 17263
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002FC6")]
		protected int m_ResourceID;

		// Token: 0x04004370 RID: 17264
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002FC7")]
		protected int m_BuildAccessID;

		// Token: 0x04004371 RID: 17265
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002FC8")]
		protected int m_ObjID;

		// Token: 0x04004372 RID: 17266
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002FC9")]
		protected int m_LayerID;

		// Token: 0x04004373 RID: 17267
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002FCA")]
		protected byte m_TimeKey;

		// Token: 0x04004374 RID: 17268
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002FCB")]
		protected TownPartsActionPakage m_Action;

		// Token: 0x04004375 RID: 17269
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002FCC")]
		protected TownBuildUpBufParam.BuildUpEvent m_Callback;

		// Token: 0x04004376 RID: 17270
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002FCD")]
		protected long m_ManageID;

		// Token: 0x02000B7A RID: 2938
		[Token(Token = "0x2001047")]
		public enum eTouchState
		{
			// Token: 0x04004378 RID: 17272
			[Token(Token = "0x4006716")]
			Begin,
			// Token: 0x04004379 RID: 17273
			[Token(Token = "0x4006717")]
			Move,
			// Token: 0x0400437A RID: 17274
			[Token(Token = "0x4006718")]
			End,
			// Token: 0x0400437B RID: 17275
			[Token(Token = "0x4006719")]
			Cancel
		}
	}
}
