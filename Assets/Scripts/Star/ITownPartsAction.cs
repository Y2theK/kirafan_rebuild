﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B3D RID: 2877
	[Token(Token = "0x20007E2")]
	[StructLayout(3)]
	public class ITownPartsAction
	{
		// Token: 0x06003273 RID: 12915 RVA: 0x00015720 File Offset: 0x00013920
		[Token(Token = "0x6002E1C")]
		[Address(RVA = "0x101224638", Offset = "0x1224638", VA = "0x101224638", Slot = "4")]
		public virtual bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x06003274 RID: 12916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E1D")]
		[Address(RVA = "0x101224640", Offset = "0x1224640", VA = "0x101224640", Slot = "5")]
		public virtual void Destory()
		{
		}

		// Token: 0x06003275 RID: 12917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E1E")]
		[Address(RVA = "0x101224644", Offset = "0x1224644", VA = "0x101224644")]
		public ITownPartsAction()
		{
		}

		// Token: 0x04004221 RID: 16929
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002EFF")]
		public int m_UID;

		// Token: 0x04004222 RID: 16930
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002F00")]
		public bool m_Active;
	}
}
