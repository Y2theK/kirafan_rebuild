﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009EE RID: 2542
	[Token(Token = "0x2000721")]
	[StructLayout(3)]
	public abstract class IUnitRoomController : MonoBehaviour
	{
		// Token: 0x06002A7B RID: 10875
		[Token(Token = "0x600270A")]
		[Address(Slot = "4")]
		protected abstract void OnStart();

		// Token: 0x06002A7C RID: 10876
		[Token(Token = "0x600270B")]
		[Address(Slot = "5")]
		protected abstract void OnStartPreparation();

		// Token: 0x06002A7D RID: 10877
		[Token(Token = "0x600270C")]
		[Address(Slot = "6")]
		protected abstract void OnDonePreparation();

		// Token: 0x06002A7E RID: 10878
		[Token(Token = "0x600270D")]
		[Address(Slot = "7")]
		protected abstract void OnUpdate();

		// Token: 0x06002A7F RID: 10879
		[Token(Token = "0x600270E")]
		[Address(Slot = "8")]
		public abstract void OnEventSend(IMainComCommand cmd);

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06002A80 RID: 10880 RVA: 0x00012090 File Offset: 0x00010290
		[Token(Token = "0x170002AC")]
		public bool isAvailable
		{
			[Token(Token = "0x600270F")]
			[Address(RVA = "0x10122464C", Offset = "0x122464C", VA = "0x10122464C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06002A81 RID: 10881 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002AD")]
		public UnitRoomBuilder builder
		{
			[Token(Token = "0x6002710")]
			[Address(RVA = "0x101224654", Offset = "0x1224654", VA = "0x101224654")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002A82 RID: 10882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002711")]
		[Address(RVA = "0x10122465C", Offset = "0x122465C", VA = "0x10122465C")]
		public void Setup()
		{
		}

		// Token: 0x06002A83 RID: 10883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002712")]
		[Address(RVA = "0x101224668", Offset = "0x1224668", VA = "0x101224668")]
		public void Destroy()
		{
		}

		// Token: 0x06002A84 RID: 10884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002713")]
		[Address(RVA = "0x101224728", Offset = "0x1224728", VA = "0x101224728")]
		private void Start()
		{
		}

		// Token: 0x06002A85 RID: 10885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002714")]
		[Address(RVA = "0x101224758", Offset = "0x1224758", VA = "0x101224758")]
		private void Update()
		{
		}

		// Token: 0x06002A86 RID: 10886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002715")]
		[Address(RVA = "0x10122478C", Offset = "0x122478C", VA = "0x10122478C")]
		private void Update_WaitSystemSetup()
		{
		}

		// Token: 0x06002A87 RID: 10887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002716")]
		[Address(RVA = "0x101224874", Offset = "0x1224874", VA = "0x101224874")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06002A88 RID: 10888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002717")]
		[Address(RVA = "0x101224994", Offset = "0x1224994", VA = "0x101224994")]
		protected IUnitRoomController()
		{
		}

		// Token: 0x04003AB8 RID: 15032
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A4C")]
		private IUnitRoomController.eStep m_Step;

		// Token: 0x04003AB9 RID: 15033
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A4D")]
		[SerializeField]
		private UnitRoomBuilder m_RoomBuilder;

		// Token: 0x04003ABA RID: 15034
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A4E")]
		[SerializeField]
		private int m_CharaSlotLimit;

		// Token: 0x04003ABB RID: 15035
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002A4F")]
		private bool m_isAvailable;

		// Token: 0x020009EF RID: 2543
		[Token(Token = "0x2000F9B")]
		private enum eStep
		{
			// Token: 0x04003ABD RID: 15037
			[Token(Token = "0x40063DE")]
			Invalid,
			// Token: 0x04003ABE RID: 15038
			[Token(Token = "0x40063DF")]
			WaitSetup,
			// Token: 0x04003ABF RID: 15039
			[Token(Token = "0x40063E0")]
			WaitSystemSetup,
			// Token: 0x04003AC0 RID: 15040
			[Token(Token = "0x40063E1")]
			Prepare,
			// Token: 0x04003AC1 RID: 15041
			[Token(Token = "0x40063E2")]
			Update
		}
	}
}
