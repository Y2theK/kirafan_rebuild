﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A55 RID: 2645
	[Token(Token = "0x200075E")]
	[StructLayout(0, Size = 8)]
	public struct IVector2
	{
		// Token: 0x06002D8A RID: 11658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029DE")]
		[Address(RVA = "0x1000316A0", Offset = "0x316A0", VA = "0x1000316A0")]
		public IVector2(int fx, int fy)
		{
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x06002D8B RID: 11659 RVA: 0x000136E0 File Offset: 0x000118E0
		[Token(Token = "0x170002DA")]
		public static IVector2 zero
		{
			[Token(Token = "0x60029DF")]
			[Address(RVA = "0x1012249A4", Offset = "0x12249A4", VA = "0x1012249A4")]
			get
			{
				return default(IVector2);
			}
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06002D8C RID: 11660 RVA: 0x000136F8 File Offset: 0x000118F8
		[Token(Token = "0x170002DB")]
		public static IVector2 one
		{
			[Token(Token = "0x60029E0")]
			[Address(RVA = "0x1012249AC", Offset = "0x12249AC", VA = "0x1012249AC")]
			get
			{
				return default(IVector2);
			}
		}

		// Token: 0x04003D0E RID: 15630
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002C00")]
		public int x;

		// Token: 0x04003D0F RID: 15631
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002C01")]
		public int y;
	}
}
