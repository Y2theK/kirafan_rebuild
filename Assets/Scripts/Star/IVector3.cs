﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A56 RID: 2646
	[Token(Token = "0x200075F")]
	[StructLayout(0, Size = 12)]
	public struct IVector3
	{
		// Token: 0x06002D8D RID: 11661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029E1")]
		[Address(RVA = "0x1000316A8", Offset = "0x316A8", VA = "0x1000316A8")]
		public IVector3(int fx, int fy, int fz)
		{
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06002D8E RID: 11662 RVA: 0x00013710 File Offset: 0x00011910
		[Token(Token = "0x170002DC")]
		public static IVector3 zero
		{
			[Token(Token = "0x60029E2")]
			[Address(RVA = "0x1012249C0", Offset = "0x12249C0", VA = "0x1012249C0")]
			get
			{
				return default(IVector3);
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06002D8F RID: 11663 RVA: 0x00013728 File Offset: 0x00011928
		[Token(Token = "0x170002DD")]
		public static IVector3 one
		{
			[Token(Token = "0x60029E3")]
			[Address(RVA = "0x1012249CC", Offset = "0x12249CC", VA = "0x1012249CC")]
			get
			{
				return default(IVector3);
			}
		}

		// Token: 0x04003D10 RID: 15632
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002C02")]
		public int x;

		// Token: 0x04003D11 RID: 15633
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002C03")]
		public int y;

		// Token: 0x04003D12 RID: 15634
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002C04")]
		public int z;
	}
}
