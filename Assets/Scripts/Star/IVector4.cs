﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A57 RID: 2647
	[Token(Token = "0x2000760")]
	[StructLayout(0, Size = 16)]
	public struct IVector4
	{
		// Token: 0x06002D90 RID: 11664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029E4")]
		[Address(RVA = "0x1000316B4", Offset = "0x316B4", VA = "0x1000316B4")]
		public IVector4(int fx, int fy, int fz, int fw)
		{
		}

		// Token: 0x04003D13 RID: 15635
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002C05")]
		public int x;

		// Token: 0x04003D14 RID: 15636
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002C06")]
		public int y;

		// Token: 0x04003D15 RID: 15637
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002C07")]
		public int z;

		// Token: 0x04003D16 RID: 15638
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4002C08")]
		public int w;
	}
}
