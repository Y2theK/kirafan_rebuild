﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006C4 RID: 1732
	[Token(Token = "0x200057B")]
	[StructLayout(3)]
	public class IXlsBinForm
	{
		// Token: 0x0600195F RID: 6495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DB")]
		[Address(RVA = "0x1012249D8", Offset = "0x12249D8", VA = "0x1012249D8")]
		public void RealBinFile(BinaryReader pfiles)
		{
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DC")]
		[Address(RVA = "0x101224B88", Offset = "0x1224B88", VA = "0x101224B88", Slot = "4")]
		public virtual void SheetHeaderData(BinaryReader pfiles)
		{
		}

		// Token: 0x06001961 RID: 6497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DD")]
		[Address(RVA = "0x101224D9C", Offset = "0x1224D9C", VA = "0x101224D9C", Slot = "5")]
		public virtual void SheetDataTag(string psheetname, int ftablenums, BinaryReader pfiles)
		{
		}

		// Token: 0x06001962 RID: 6498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DE")]
		[Address(RVA = "0x101224DA0", Offset = "0x1224DA0", VA = "0x101224DA0", Slot = "6")]
		public virtual void SheetDataTag(string psheetname, string strTableData)
		{
		}

		// Token: 0x06001963 RID: 6499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DF")]
		[Address(RVA = "0x101224DA4", Offset = "0x1224DA4", VA = "0x101224DA4")]
		public IXlsBinForm()
		{
		}
	}
}
