﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine.Purchasing;

namespace Star
{
	// Token: 0x02000893 RID: 2195
	[Token(Token = "0x200065C")]
	[StructLayout(3)]
	public class InAppPurchaser : SingletonMonoBehaviour<InAppPurchaser>, IStoreListener
	{
		// Token: 0x1700026D RID: 621
		// (get) Token: 0x0600239B RID: 9115 RVA: 0x0000F390 File Offset: 0x0000D590
		[Token(Token = "0x17000248")]
		public InAppPurchaser.eInitializeResult InitializeResult
		{
			[Token(Token = "0x60020E7")]
			[Address(RVA = "0x101224DAC", Offset = "0x1224DAC", VA = "0x101224DAC")]
			get
			{
				return InAppPurchaser.eInitializeResult.NOTYET;
			}
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x0600239C RID: 9116 RVA: 0x0000F3A8 File Offset: 0x0000D5A8
		[Token(Token = "0x17000249")]
		public bool InitializeSuccess
		{
			[Token(Token = "0x60020E8")]
			[Address(RVA = "0x101224DC8", Offset = "0x1224DC8", VA = "0x101224DC8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x0600239D RID: 9117 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020E9")]
		[Address(RVA = "0x101224DE4", Offset = "0x1224DE4", VA = "0x101224DE4")]
		public string GetErrorTitle()
		{
			return null;
		}

		// Token: 0x0600239E RID: 9118 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020EA")]
		[Address(RVA = "0x101224DEC", Offset = "0x1224DEC", VA = "0x101224DEC")]
		public string GetErrorMessage()
		{
			return null;
		}

		// Token: 0x0600239F RID: 9119 RVA: 0x0000F3C0 File Offset: 0x0000D5C0
		[Token(Token = "0x60020EB")]
		[Address(RVA = "0x101224DF4", Offset = "0x1224DF4", VA = "0x101224DF4")]
		public PurchaseFailureReason GetErrorReason()
		{
			return PurchaseFailureReason.PurchasingUnavailable;
		}

		// Token: 0x060023A0 RID: 9120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020EC")]
		[Address(RVA = "0x101224DFC", Offset = "0x1224DFC", VA = "0x101224DFC")]
		public void AddProducts(List<string> productIDs)
		{
		}

		// Token: 0x060023A1 RID: 9121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020ED")]
		[Address(RVA = "0x10122500C", Offset = "0x122500C", VA = "0x10122500C")]
		public void Purchase(string productID, string payload)
		{
		}

		// Token: 0x060023A2 RID: 9122 RVA: 0x0000F3D8 File Offset: 0x0000D5D8
		[Token(Token = "0x60020EE")]
		[Address(RVA = "0x101225278", Offset = "0x1225278", VA = "0x101225278")]
		public InAppPurchaser.PurchaseState GetPurchaseState()
		{
			return InAppPurchaser.PurchaseState.None;
		}

		// Token: 0x060023A3 RID: 9123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020EF")]
		[Address(RVA = "0x1012252A0", Offset = "0x12252A0", VA = "0x1012252A0")]
		public void ConfirmPurchase()
		{
		}

		// Token: 0x060023A4 RID: 9124 RVA: 0x0000F3F0 File Offset: 0x0000D5F0
		[Token(Token = "0x60020F0")]
		[Address(RVA = "0x101225338", Offset = "0x1225338", VA = "0x101225338")]
		public bool IsRestrictedPurchase()
		{
			return default(bool);
		}

		// Token: 0x060023A5 RID: 9125 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F1")]
		[Address(RVA = "0x1012253BC", Offset = "0x12253BC", VA = "0x1012253BC")]
		public string GetPayload()
		{
			return null;
		}

		// Token: 0x060023A6 RID: 9126 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F2")]
		[Address(RVA = "0x10122545C", Offset = "0x122545C", VA = "0x10122545C")]
		public string GetReceiptAndroid()
		{
			return null;
		}

		// Token: 0x060023A7 RID: 9127 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F3")]
		[Address(RVA = "0x1012256D4", Offset = "0x12256D4", VA = "0x1012256D4")]
		public string GetSignature()
		{
			return null;
		}

		// Token: 0x060023A8 RID: 9128 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F4")]
		[Address(RVA = "0x101225794", Offset = "0x1225794", VA = "0x101225794")]
		public string GetRestorePayload(int index)
		{
			return null;
		}

		// Token: 0x060023A9 RID: 9129 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F5")]
		[Address(RVA = "0x10122589C", Offset = "0x122589C", VA = "0x10122589C")]
		public string GetRestoreReceiptAndroid(int index)
		{
			return null;
		}

		// Token: 0x060023AA RID: 9130 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F6")]
		[Address(RVA = "0x101225A00", Offset = "0x1225A00", VA = "0x101225A00")]
		public string GetRestoreSignatureAndroid(int index)
		{
			return null;
		}

		// Token: 0x060023AB RID: 9131 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020F7")]
		[Address(RVA = "0x101225B28", Offset = "0x1225B28", VA = "0x101225B28")]
		public InAppPurchaser.PendingProduct GetRecentPendingPurchase()
		{
			return null;
		}

		// Token: 0x060023AC RID: 9132 RVA: 0x0000F408 File Offset: 0x0000D608
		[Token(Token = "0x60020F8")]
		[Address(RVA = "0x101225C00", Offset = "0x1225C00", VA = "0x101225C00")]
		public bool IsNeedRestoreProduct()
		{
			return default(bool);
		}

		// Token: 0x060023AD RID: 9133 RVA: 0x0000F420 File Offset: 0x0000D620
		[Token(Token = "0x60020F9")]
		[Address(RVA = "0x101225C90", Offset = "0x1225C90", VA = "0x101225C90")]
		public int GetRestoreReceiptCount()
		{
			return 0;
		}

		// Token: 0x060023AE RID: 9134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020FA")]
		[Address(RVA = "0x101225D18", Offset = "0x1225D18", VA = "0x101225D18")]
		public void RestoreProduct(int index)
		{
		}

		// Token: 0x060023AF RID: 9135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020FB")]
		[Address(RVA = "0x101225ED4", Offset = "0x1225ED4", VA = "0x101225ED4")]
		public void ClearRestoreProduct()
		{
		}

		// Token: 0x060023B0 RID: 9136 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020FC")]
		[Address(RVA = "0x1012254E0", Offset = "0x12254E0", VA = "0x1012254E0")]
		private string GetPayloadReceiptToBaseString(Product product)
		{
			return null;
		}

		// Token: 0x060023B1 RID: 9137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020FD")]
		[Address(RVA = "0x101226550", Offset = "0x1226550", VA = "0x101226550", Slot = "9")]
		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
		}

		// Token: 0x060023B2 RID: 9138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020FE")]
		[Address(RVA = "0x101226680", Offset = "0x1226680", VA = "0x101226680", Slot = "6")]
		public void OnInitializeFailed(InitializationFailureReason error)
		{
		}

		// Token: 0x060023B3 RID: 9139 RVA: 0x0000F438 File Offset: 0x0000D638
		[Token(Token = "0x60020FF")]
		[Address(RVA = "0x101226768", Offset = "0x1226768", VA = "0x101226768", Slot = "7")]
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			return PurchaseProcessingResult.Complete;
		}

		// Token: 0x060023B4 RID: 9140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002100")]
		[Address(RVA = "0x101226B6C", Offset = "0x1226B6C", VA = "0x101226B6C", Slot = "8")]
		public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
		{
		}

		// Token: 0x060023B5 RID: 9141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002101")]
		[Address(RVA = "0x1012272F0", Offset = "0x12272F0", VA = "0x1012272F0")]
		private void OnPendingPurchaseToAndroidEvent(Product product)
		{
		}

		// Token: 0x060023B6 RID: 9142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002102")]
		[Address(RVA = "0x1012254DC", Offset = "0x12254DC", VA = "0x1012254DC")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100138BA4", Offset = "0x138BA4")]
		private static void LOG(object o)
		{
		}

		// Token: 0x060023B7 RID: 9143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002103")]
		[Address(RVA = "0x101225008", Offset = "0x1225008", VA = "0x101225008")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100138BDC", Offset = "0x138BDC")]
		private static void LOGF(string format, params object[] args)
		{
		}

		// Token: 0x060023B8 RID: 9144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002104")]
		[Address(RVA = "0x101227690", Offset = "0x1227690", VA = "0x101227690")]
		public InAppPurchaser()
		{
		}

		// Token: 0x040033C7 RID: 13255
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400263D")]
		private IStoreController m_Controller;

		// Token: 0x040033C8 RID: 13256
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400263E")]
		private IExtensionProvider m_Extensions;

		// Token: 0x040033C9 RID: 13257
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400263F")]
		private ConfigurationBuilder m_Builder;

		// Token: 0x040033CA RID: 13258
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002640")]
		private InAppPurchaser.eInitializeResult m_InitializeStatus;

		// Token: 0x040033CB RID: 13259
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002641")]
		private InAppPurchaser.PurchaseState m_PurchaseState;

		// Token: 0x040033CC RID: 13260
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002642")]
		private PurchaseFailureReason m_ErrorReason;

		// Token: 0x040033CD RID: 13261
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002643")]
		private string m_ErrorTitle;

		// Token: 0x040033CE RID: 13262
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002644")]
		private string m_ErrorMessage;

		// Token: 0x040033CF RID: 13263
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002645")]
		private Product m_PendingProduct;

		// Token: 0x040033D0 RID: 13264
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002646")]
		private List<Product> m_RestoreProducts;

		// Token: 0x040033D1 RID: 13265
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002647")]
		private List<string> m_RestoredProductIDs;

		// Token: 0x040033D2 RID: 13266
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002648")]
		private List<InAppPurchaser.PendingProduct> m_PendingPurchaseList;

		// Token: 0x040033D3 RID: 13267
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002649")]
		private bool m_isRestoreCall;

		// Token: 0x040033D4 RID: 13268
		[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
		[Token(Token = "0x400264A")]
		private bool m_isRestoreFinish;

		// Token: 0x02000894 RID: 2196
		[Token(Token = "0x2000F07")]
		private class Receipt
		{
			// Token: 0x060023BA RID: 9146 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F72")]
			[Address(RVA = "0x101227B34", Offset = "0x1227B34", VA = "0x101227B34")]
			public Receipt()
			{
			}

			// Token: 0x040033D5 RID: 13269
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40060FD")]
			public string Store;

			// Token: 0x040033D6 RID: 13270
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40060FE")]
			public string TransactionID;

			// Token: 0x040033D7 RID: 13271
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40060FF")]
			public string Payload;
		}

		// Token: 0x02000895 RID: 2197
		[Token(Token = "0x2000F08")]
		private class Payload
		{
			// Token: 0x060023BB RID: 9147 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F73")]
			[Address(RVA = "0x101227B2C", Offset = "0x1227B2C", VA = "0x101227B2C")]
			public Payload()
			{
			}

			// Token: 0x040033D8 RID: 13272
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006100")]
			public string json;

			// Token: 0x040033D9 RID: 13273
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006101")]
			public string signature;
		}

		// Token: 0x02000896 RID: 2198
		[Token(Token = "0x2000F09")]
		public class PendingProduct
		{
			// Token: 0x060023BC RID: 9148 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F74")]
			[Address(RVA = "0x1012264BC", Offset = "0x12264BC", VA = "0x1012264BC")]
			public PendingProduct(Product p)
			{
			}

			// Token: 0x040033DA RID: 13274
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006102")]
			public string id;

			// Token: 0x040033DB RID: 13275
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006103")]
			public string storeSpecificId;

			// Token: 0x040033DC RID: 13276
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006104")]
			public string transactionID;
		}

		// Token: 0x02000897 RID: 2199
		[Token(Token = "0x2000F0A")]
		public enum eInitializeResult
		{
			// Token: 0x040033DE RID: 13278
			[Token(Token = "0x4006106")]
			NOTYET,
			// Token: 0x040033DF RID: 13279
			[Token(Token = "0x4006107")]
			SUCCESS,
			// Token: 0x040033E0 RID: 13280
			[Token(Token = "0x4006108")]
			FAILURE,
			// Token: 0x040033E1 RID: 13281
			[Token(Token = "0x4006109")]
			NO_CONNECTION
		}

		// Token: 0x02000898 RID: 2200
		[Token(Token = "0x2000F0B")]
		public enum PurchaseState
		{
			// Token: 0x040033E3 RID: 13283
			[Token(Token = "0x400610B")]
			None,
			// Token: 0x040033E4 RID: 13284
			[Token(Token = "0x400610C")]
			Processing,
			// Token: 0x040033E5 RID: 13285
			[Token(Token = "0x400610D")]
			Pending,
			// Token: 0x040033E6 RID: 13286
			[Token(Token = "0x400610E")]
			Success,
			// Token: 0x040033E7 RID: 13287
			[Token(Token = "0x400610F")]
			Failure
		}
	}
}
