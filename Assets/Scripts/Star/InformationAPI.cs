﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BDB RID: 3035
	[Token(Token = "0x2000832")]
	[StructLayout(3)]
	public class InformationAPI
	{
		// Token: 0x0600352F RID: 13615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003071")]
		[Address(RVA = "0x101227B3C", Offset = "0x1227B3C", VA = "0x101227B3C")]
		public void Request_Read(int id)
		{
		}

		// Token: 0x06003530 RID: 13616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003072")]
		[Address(RVA = "0x101227C10", Offset = "0x1227C10", VA = "0x101227C10")]
		public InformationAPI()
		{
		}
	}
}
