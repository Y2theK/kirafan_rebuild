﻿using System;

namespace Star {
    public class InformationData {
        public const string URL_COMMAND_TRANSIT_TOP = "Transit:Top";
        public const string URL_COMMAND_TRANSIT_WEEKLY_QUEST = "Transit:WeeklyQuest";
        public const string URL_COMMAND_TRANSIT_CRAFT = "Transit:Craft";
        public const string URL_COMMAND_TRANSIT_ABILITYTREE = "Transit:AbilityTree";

        public int m_ID;
        public string m_ImgID;
        public string m_Url;
        public bool m_IsFeatured;
        public DateTime m_StartTime;
        public DateTime m_EndTime;
        public DateTime m_DispStartTime;
        public DateTime m_DispEndTime;
        public int m_Sort;

        public InformationData(int id, string imgID, string url, bool isFeatured, DateTime startTime, DateTime endTime, DateTime dispStartTime, DateTime dispEndTime, int sort) {
            m_ID = id;
            m_ImgID = imgID;
            m_Url = url;
            m_IsFeatured = isFeatured;
            m_StartTime = startTime;
            m_EndTime = endTime;
            m_DispStartTime = dispStartTime;
            m_DispEndTime = dispEndTime;
            m_Sort = sort;
        }
    }
}
