﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BDA RID: 3034
	[Token(Token = "0x2000831")]
	[StructLayout(3)]
	public class InformationHomeOpen
	{
		// Token: 0x0600352D RID: 13613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600306F")]
		[Address(RVA = "0x101227C98", Offset = "0x1227C98", VA = "0x101227C98")]
		public InformationHomeOpen()
		{
		}

		// Token: 0x0600352E RID: 13614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003070")]
		[Address(RVA = "0x101227CA8", Offset = "0x1227CA8", VA = "0x101227CA8")]
		public InformationHomeOpen(int id, string url)
		{
		}

		// Token: 0x04004569 RID: 17769
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40030EA")]
		public int m_ID;

		// Token: 0x0400456A RID: 17770
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030EB")]
		public string m_URL;
	}
}
