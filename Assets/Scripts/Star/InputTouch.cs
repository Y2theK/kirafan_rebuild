﻿using UnityEngine;

namespace Star {
    public sealed class InputTouch {
        private const int INVALIDATE_IDX = -1;
        public const int TOUCH_MAX = 3;
        private const float THRESHOLD_SAMETOUCH_RATIO = 0.001f;
        private const float THRESHOLD_SAMEDBLTOUCH_RATIO = 0.001f;
        private const float THRESHOLD_FLICK_RATIO = 0.0005f;
        private const float THRESHOLD_DBLTOUCH_FRAME = 10f;
        private const float THRESHOLD_SWITCH_HAND_COEFF = 0.25f;

        public float THRESHOLD_SAMETOUCH_PIX;
        public float THRESHOLD_SAMEDBLTOUCH_PIX;
        private float THRESHOLD_SWITCH_HAND;
        public float THRESHOLD_FLICK;

        private TOUCH_INFO[] m_TouchInfo = new TOUCH_INFO[TOUCH_MAX];
        private DOUBLE_TOUCH_INFO[] m_DblTouchInfo = new DOUBLE_TOUCH_INFO[TOUCH_MAX];
        private TOUCH_INFO[] m_HandTouchInfo = new TOUCH_INFO[(int)eHandID.eMax];
        private int[] m_HandIdToIdx = new int[(int)eHandID.eMax];
        private int m_AvailableTouchCnt;

        private static InputTouch m_Instance;
        public static InputTouch Inst => m_Instance;

        private InputTouch() {
            THRESHOLD_SAMETOUCH_PIX = (Screen.height < Screen.width ? Screen.height : Screen.width) * THRESHOLD_SAMETOUCH_RATIO;
            THRESHOLD_SAMEDBLTOUCH_PIX = (Screen.height < Screen.width ? Screen.height : Screen.width) * THRESHOLD_SAMEDBLTOUCH_RATIO;
            THRESHOLD_FLICK = (Screen.height < Screen.width ? Screen.height : Screen.width) * THRESHOLD_FLICK_RATIO;
            THRESHOLD_SWITCH_HAND = Screen.width * THRESHOLD_SWITCH_HAND_COEFF;
        }

        public static void Create() {
            if (m_Instance == null) {
                m_Instance = new InputTouch();
            }
            m_Instance.Init();
        }

        public void Init() {
            for (int i = 0; i < TOUCH_MAX; i++) {
                m_TouchInfo[i].m_bAvailable = false;
            }
        }

        public void Update() {
            for (int i = 0; i < TOUCH_MAX; i++) {
                if (m_DblTouchInfo[i].IsAvailable()) {
                    m_DblTouchInfo[i].m_Time = m_DblTouchInfo[i].m_Time + Time.deltaTime;
                    if (m_DblTouchInfo[i].m_Time >= THRESHOLD_DBLTOUCH_FRAME || m_DblTouchInfo[i].IsTrigger()) {
                        m_DblTouchInfo[i].Clear();
                    }
                }
            }
            int touchCount = Mathf.Min(Input.touchCount, TOUCH_MAX);
            int availableTouches = 0;
            for (int i = 0; i < touchCount; i++) {
                Touch touch = Input.GetTouch(i);
                if (touch.phase != TouchPhase.Canceled) {
                    Vector2 position = touch.position;
                    int singleIdx = SearchTouchInfo(touch.fingerId);
                    int doubleIdx = SearchDoubleTouchInfo(touch.fingerId);
                    switch (touch.phase) {
                        case TouchPhase.Began:
                            singleIdx = GetEmptyTouchInfo();
                            if (singleIdx >= 0) {
                                m_TouchInfo[singleIdx].m_FingerID = touch.fingerId;
                                m_TouchInfo[singleIdx].m_StartPos = position;
                                m_TouchInfo[singleIdx].m_CurrentPos = position;
                                m_TouchInfo[singleIdx].m_OldPos = position;
                                m_TouchInfo[singleIdx].m_DiffFromStart = Vector3.zero;
                                m_TouchInfo[singleIdx].m_DiffFromPrev = Vector3.zero;
                                if (m_HandIdToIdx[(int)eHandID.eLeft] == INVALIDATE_IDX && IsThisTouchOnHand(position, eHandID.eLeft)) {
                                    m_HandIdToIdx[(int)eHandID.eLeft] = singleIdx;
                                    MakeHandTouchInfo(eHandID.eLeft, m_TouchInfo[singleIdx], position);
                                } else if (m_HandIdToIdx[(int)eHandID.eRight] == INVALIDATE_IDX) {
                                    m_HandIdToIdx[(int)eHandID.eRight] = singleIdx;
                                    MakeHandTouchInfo(eHandID.eRight, m_TouchInfo[singleIdx], position);
                                }
                            }
                            doubleIdx = GetEmptyDoubleTouchInfo();
                            if (doubleIdx >= 0) {
                                m_DblTouchInfo[doubleIdx].Start(touch.fingerId, position);
                            }
                            break;
                        case TouchPhase.Moved:
                        case TouchPhase.Stationary:
                            if (singleIdx < 0) { break; }
                            UpdateTouchInfo(ref m_TouchInfo[singleIdx], position, false);
                            if (!IsThisTouchOnHand(position, eHandID.eLeft) && m_HandIdToIdx[(int)eHandID.eLeft] == singleIdx) {
                                if (Mathf.Abs(position.x - m_HandTouchInfo[(int)eHandID.eLeft].m_CurrentPos.x) >= THRESHOLD_SWITCH_HAND) {
                                    if (IsThisTouchOnHand(position, eHandID.eRight) && m_HandIdToIdx[(int)eHandID.eRight] == INVALIDATE_IDX) {
                                        m_HandIdToIdx[(int)eHandID.eRight] = singleIdx;
                                        MakeHandTouchInfo(eHandID.eRight, m_TouchInfo[singleIdx], position);
                                    }
                                    m_HandIdToIdx[(int)eHandID.eLeft] = INVALIDATE_IDX;
                                }
                            } else if (!IsThisTouchOnHand(position, eHandID.eRight) && m_HandIdToIdx[(int)eHandID.eRight] == singleIdx) {
                                if (Mathf.Abs(position.x - m_HandTouchInfo[(int)eHandID.eRight].m_CurrentPos.x) >= THRESHOLD_SWITCH_HAND) {
                                    if (IsThisTouchOnHand(position, eHandID.eLeft) && m_HandIdToIdx[(int)eHandID.eLeft] == INVALIDATE_IDX) {
                                        m_HandIdToIdx[(int)eHandID.eLeft] = singleIdx;
                                        MakeHandTouchInfo(eHandID.eLeft, m_TouchInfo[singleIdx], position);
                                    }
                                    m_HandIdToIdx[(int)eHandID.eRight] = INVALIDATE_IDX;
                                }
                            }
                            if (m_HandIdToIdx[(int)eHandID.eLeft] == singleIdx) {
                                UpdateTouchInfo(ref m_HandTouchInfo[(int)eHandID.eLeft], position, false);
                            } else if (m_HandIdToIdx[(int)eHandID.eRight] == singleIdx) {
                                UpdateTouchInfo(ref m_HandTouchInfo[(int)eHandID.eRight], position, false);
                            }
                            break;
                        case TouchPhase.Ended:
                            if (singleIdx >= 0) {
                                UpdateTouchInfo(ref m_TouchInfo[singleIdx], position, true);
                                if (m_HandIdToIdx[(int)eHandID.eLeft] == singleIdx) {
                                    UpdateTouchInfo(ref m_HandTouchInfo[(int)eHandID.eLeft], position, true);
                                } else if (m_HandIdToIdx[(int)eHandID.eRight] == singleIdx) {
                                    UpdateTouchInfo(ref m_HandTouchInfo[(int)eHandID.eRight], position, true);
                                }
                            }
                            if (doubleIdx >= 0) {
                                UpdateTouchCnt(doubleIdx, position);
                            }
                            break;
                    }
                    if (singleIdx >= 0) {
                        m_TouchInfo[singleIdx].m_bAvailable = true;
                        m_TouchInfo[singleIdx].m_RawPos = position;
                        m_TouchInfo[singleIdx].m_TouchPhase = touch.phase;
                        availableTouches |= 1 << singleIdx;
                    }
                }
            }
            m_AvailableTouchCnt = 0;
            for (int i = 0; i < TOUCH_MAX; i++) {
                if (m_TouchInfo[i].m_bAvailable) {
                    if ((availableTouches & (1 << i)) == 0) {
                        m_TouchInfo[i].m_bAvailable = false;
                    }
                    m_AvailableTouchCnt++;
                }
            }
            if (m_HandIdToIdx[(int)eHandID.eLeft] != INVALIDATE_IDX && !m_TouchInfo[m_HandIdToIdx[(int)eHandID.eLeft]].m_bAvailable) {
                m_HandIdToIdx[(int)eHandID.eLeft] = INVALIDATE_IDX;
                m_HandTouchInfo[(int)eHandID.eLeft].m_bAvailable = false;
            }
            if (m_HandIdToIdx[(int)eHandID.eRight] != INVALIDATE_IDX && !m_TouchInfo[m_HandIdToIdx[(int)eHandID.eRight]].m_bAvailable) {
                m_HandIdToIdx[(int)eHandID.eRight] = INVALIDATE_IDX;
                m_HandTouchInfo[(int)eHandID.eRight].m_bAvailable = false;
            }
        }

        private int SearchTouchInfo(int fingerID) {
            for (int i = 0; i < m_TouchInfo.Length; i++) {
                if (m_TouchInfo[i].m_FingerID == fingerID) {
                    return i;
                }
            }
            return INVALIDATE_IDX;
        }

        private int GetEmptyTouchInfo() {
            for (int i = 0; i < m_TouchInfo.Length; i++) {
                if (!m_TouchInfo[i].m_bAvailable) {
                    return i;
                }
            }
            return INVALIDATE_IDX;
        }

        private int SearchDoubleTouchInfo(int fingerID) {
            for (int i = 0; i < m_DblTouchInfo.Length; i++) {
                if (m_DblTouchInfo[i].m_FingerID == fingerID) {
                    return i;
                }
            }
            return INVALIDATE_IDX;
        }

        private int GetEmptyDoubleTouchInfo() {
            for (int i = 0; i < m_DblTouchInfo.Length; i++) {
                if (!m_DblTouchInfo[i].IsAvailable()) {
                    return i;
                }
            }
            return INVALIDATE_IDX;
        }

        private void UpdateTouchCnt(int idx, Vector2 pos) {
            float num = Vector2.Distance(pos, m_DblTouchInfo[idx].m_StartPos);
            if (num <= THRESHOLD_SAMEDBLTOUCH_PIX) {
                DOUBLE_TOUCH_INFO[] dblTouchInfo = m_DblTouchInfo;
                dblTouchInfo[idx].m_TouchCnt = dblTouchInfo[idx].m_TouchCnt + 1;
            }
        }

        private bool IsThisTouchOnHand(Vector2 position, eHandID hand) {
            float num = (float)Screen.width / 2f;
            if (hand == eHandID.eLeft) {
                return position.x <= num;
            }
            return position.x > num;
        }

        private void MakeHandTouchInfo(eHandID hand, TOUCH_INFO ti, Vector2 curPos) {
            m_HandTouchInfo[(int)hand].m_bAvailable = ti.m_bAvailable;
            m_HandTouchInfo[(int)hand].m_StartPos = curPos;
            m_HandTouchInfo[(int)hand].m_CurrentPos = curPos;
            m_HandTouchInfo[(int)hand].m_OldPos = curPos;
            m_HandTouchInfo[(int)hand].m_DiffFromStart = Vector3.zero;
            m_HandTouchInfo[(int)hand].m_DiffFromPrev = Vector3.zero;
            m_HandTouchInfo[(int)hand].m_RawPos = ti.m_RawPos;
            m_HandTouchInfo[(int)hand].m_TouchPhase = TouchPhase.Began;
            m_HandTouchInfo[(int)hand].m_bTrigger = false;
        }

        private void UpdateTouchInfo(ref TOUCH_INFO ti, Vector2 curPos, bool bPhaseEnd) {
            ti.m_OldPos = ti.m_CurrentPos;
            Vector2 diff = curPos - ti.m_StartPos;
            float diffMag = diff.magnitude;
            if (diffMag >= THRESHOLD_SAMETOUCH_PIX) {
                ti.m_DiffFromStart.z = diffMag;
                ti.m_DiffFromStart.x = diff.x / diffMag;
                ti.m_DiffFromStart.y = diff.y / diffMag;
            } else {
                ti.m_DiffFromStart = Vector3.zero;
                if (bPhaseEnd) {
                    ti.m_bTrigger = true;
                }
            }
            diff = curPos - ti.m_OldPos;
            diffMag = diff.magnitude;
            if (diffMag >= THRESHOLD_SAMETOUCH_PIX) {
                ti.m_DiffFromPrev.z = diffMag;
                ti.m_DiffFromPrev.x = diff.x / diffMag;
                ti.m_DiffFromPrev.y = diff.y / diffMag;
            } else {
                ti.m_DiffFromPrev = Vector3.zero;
            }
            ti.m_CurrentPos = ti.m_DiffFromStart;
            ti.m_CurrentPos = ti.m_CurrentPos * ti.m_DiffFromStart.z + ti.m_StartPos;
            ti.m_RawPos = curPos;
        }

        public bool IsAvailable(int idx) {
            return m_TouchInfo[idx].m_bAvailable;
        }

        public bool IsAvailable(eHandID tid) {
            return m_HandIdToIdx[(int)tid] != INVALIDATE_IDX;
        }

        public TOUCH_INFO GetInfo(eHandID tid) {
            return m_HandTouchInfo[(int)tid];
        }

        public TOUCH_INFO GetInfo(int idx) {
            return m_TouchInfo[idx];
        }

        public int GetAvailableTouchCnt() {
            return m_AvailableTouchCnt;
        }

        public bool DetectTriggerOfSingleTouch(out Vector2 pos, int idx) {
            if (IsAvailable(idx)) {
                TOUCH_INFO info = GetInfo(idx);
                if (info.m_TouchPhase == TouchPhase.Ended) {
                    float magnitude = (info.m_RawPos - info.m_StartPos).magnitude;
                    if (magnitude < THRESHOLD_SAMETOUCH_PIX * Screen.dpi) {
                        pos = info.m_RawPos;
                        Vector2 sizeDelta = GameSystem.Inst.AspectScopeFilter.GetCineScoImagesSizeDelta();
                        if (pos.x >= sizeDelta.x && pos.x <= Screen.width - sizeDelta.x && pos.y >= 0f && pos.y <= Screen.height) {
                            return true;
                        }
                    }
                }
            }
            pos = Vector2.zero;
            return false;
        }

        public bool IsTriggerOfDoubleTouch(int idx) {
            return m_DblTouchInfo[idx].IsAvailable() && m_DblTouchInfo[idx].IsTrigger();
        }

        public bool DectectTriggerOfDoubleTouch(out Vector2 pos, int idx) {
            bool trigger = IsTriggerOfDoubleTouch(idx);
            pos = trigger ? m_DblTouchInfo[idx].m_StartPos : Vector2.zero;
            return trigger;
        }

        public bool GetFlick(out Vector3 dir, out Vector3 start, out Vector3 end, int idx) {
            if (IsAvailable(idx)) {
                TOUCH_INFO info = GetInfo(idx);
                Vector2 diff = info.m_CurrentPos - info.m_OldPos;
                float magnitude = diff.magnitude;
                if (magnitude >= THRESHOLD_FLICK) {
                    dir.x = diff.x;
                    dir.y = diff.y;
                    dir.z = magnitude;
                    start = info.m_OldPos;
                    end = info.m_CurrentPos;
                    return true;
                }
            }
            start = Vector3.zero;
            end = Vector3.zero;
            dir = Vector3.zero;
            return false;
        }

        public bool GetFlick(out Vector3 dir, int idx) {
            return GetFlick(out dir, out _, out _, idx);
        }

        public enum eHandID {
            eRight,
            eLeft,
            eMax
        }

        public struct TOUCH_INFO {
            public Vector2 m_StartPos;
            public Vector2 m_CurrentPos;
            public Vector2 m_OldPos;
            public Vector3 m_DiffFromStart;
            public Vector3 m_DiffFromPrev;
            public Vector2 m_RawPos;
            public TouchPhase m_TouchPhase;
            public int m_FingerID;
            public bool m_bAvailable;
            public bool m_bTrigger;
        }

        private struct DOUBLE_TOUCH_INFO {
            public Vector2 m_StartPos;
            public int m_FingerID;
            public float m_Time;
            public int m_TouchCnt;

            public void Clear() {
                m_FingerID = INVALIDATE_IDX;
            }

            public void Start(int fingerID, Vector2 startPos) {
                m_StartPos = startPos;
                m_FingerID = fingerID;
                m_Time = 0f;
                m_TouchCnt = 0;
            }

            public bool IsAvailable() {
                return m_FingerID != INVALIDATE_IDX;
            }

            public bool IsTrigger() {
                return m_TouchCnt >= 2;
            }
        }
    }
}
