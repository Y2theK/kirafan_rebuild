﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000554 RID: 1364
	[Token(Token = "0x2000447")]
	[StructLayout(3)]
	public class ItemListDB : ScriptableObject
	{
		// Token: 0x060015D7 RID: 5591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001487")]
		[Address(RVA = "0x101229FB8", Offset = "0x1229FB8", VA = "0x101229FB8")]
		public ItemListDB()
		{
		}

		// Token: 0x040018DC RID: 6364
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001246")]
		public ItemListDB_Param[] m_Params;
	}
}
