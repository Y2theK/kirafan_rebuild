﻿namespace Star {
    public static class ItemListDB_Ext {
        public static ItemListDB_Param GetParam(this ItemListDB self, int itemID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == itemID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }
    }
}
