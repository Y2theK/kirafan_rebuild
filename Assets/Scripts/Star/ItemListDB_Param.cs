﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000553 RID: 1363
	[Token(Token = "0x2000446")]
	[Serializable]
	[StructLayout(0, Size = 56)]
	public struct ItemListDB_Param
	{
		// Token: 0x040018D3 RID: 6355
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400123D")]
		public int m_ID;

		// Token: 0x040018D4 RID: 6356
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400123E")]
		public int m_sortID;

		// Token: 0x040018D5 RID: 6357
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400123F")]
		public string m_Name;

		// Token: 0x040018D6 RID: 6358
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001240")]
		public int m_Rare;

		// Token: 0x040018D7 RID: 6359
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001241")]
		public int m_SaleAmount;

		// Token: 0x040018D8 RID: 6360
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001242")]
		public int m_Type;

		// Token: 0x040018D9 RID: 6361
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001243")]
		public int[] m_TypeArgs;

		// Token: 0x040018DA RID: 6362
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001244")]
		public string m_DetailText;

		// Token: 0x040018DB RID: 6363
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001245")]
		public int m_Appeal;
	}
}
