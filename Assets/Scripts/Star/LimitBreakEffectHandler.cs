﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000662 RID: 1634
	[Token(Token = "0x2000542")]
	[StructLayout(3)]
	public class LimitBreakEffectHandler : MonoBehaviour
	{
		// Token: 0x1700020F RID: 527
		// (get) Token: 0x060017EA RID: 6122 RVA: 0x0000AF50 File Offset: 0x00009150
		[Token(Token = "0x170001F8")]
		public LimitBreakEffectHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x600168D")]
			[Address(RVA = "0x10122A0D4", Offset = "0x122A0D4", VA = "0x10122A0D4")]
			get
			{
				return LimitBreakEffectHandler.eAnimType.Effect;
			}
		}

		// Token: 0x060017EB RID: 6123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600168E")]
		[Address(RVA = "0x10122A0DC", Offset = "0x122A0DC", VA = "0x10122A0DC")]
		private void Awake()
		{
		}

		// Token: 0x060017EC RID: 6124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600168F")]
		[Address(RVA = "0x10122A4A0", Offset = "0x122A4A0", VA = "0x10122A4A0")]
		private void OnDestroy()
		{
		}

		// Token: 0x060017ED RID: 6125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001690")]
		[Address(RVA = "0x10122A4A4", Offset = "0x122A4A4", VA = "0x10122A4A4")]
		public void Destroy()
		{
		}

		// Token: 0x060017EE RID: 6126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001691")]
		[Address(RVA = "0x10122A4F0", Offset = "0x122A4F0", VA = "0x10122A4F0")]
		private void Update()
		{
		}

		// Token: 0x060017EF RID: 6127 RVA: 0x0000AF68 File Offset: 0x00009168
		[Token(Token = "0x6001692")]
		[Address(RVA = "0x10122AAE0", Offset = "0x122AAE0", VA = "0x10122AAE0")]
		private LimitBreakEffectHandler.eAnimType GetProgressAnimType()
		{
			return LimitBreakEffectHandler.eAnimType.Effect;
		}

		// Token: 0x060017F0 RID: 6128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001693")]
		[Address(RVA = "0x10122ABE8", Offset = "0x122ABE8", VA = "0x10122ABE8")]
		public void SetLimitBreakLevel(int startLevel, int endLevel)
		{
		}

		// Token: 0x060017F1 RID: 6129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001694")]
		[Address(RVA = "0x10122ABF0", Offset = "0x122ABF0", VA = "0x10122ABF0")]
		public void Play()
		{
		}

		// Token: 0x060017F2 RID: 6130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001695")]
		[Address(RVA = "0x10122AF64", Offset = "0x122AF64", VA = "0x10122AF64")]
		public void Skip()
		{
		}

		// Token: 0x060017F3 RID: 6131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001696")]
		[Address(RVA = "0x10122A8A8", Offset = "0x122A8A8", VA = "0x10122A8A8")]
		private void PlayIndex(LimitBreakEffectHandler.eAnimIndex animIndex, LimitBreakEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x060017F4 RID: 6132 RVA: 0x0000AF80 File Offset: 0x00009180
		[Token(Token = "0x6001697")]
		[Address(RVA = "0x10122A7A8", Offset = "0x122A7A8", VA = "0x10122A7A8")]
		private bool IsPlayingAnim(LimitBreakEffectHandler.eAnimIndex animIndex)
		{
			return default(bool);
		}

		// Token: 0x060017F5 RID: 6133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001698")]
		[Address(RVA = "0x10122B158", Offset = "0x122B158", VA = "0x10122B158")]
		private void SetLayer(LimitBreakEffectHandler.eAnimIndex animIndex, LimitBreakEffectHandler.eAnimType animType)
		{
		}

		// Token: 0x060017F6 RID: 6134 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001699")]
		[Address(RVA = "0x10122B23C", Offset = "0x122B23C", VA = "0x10122B23C")]
		private string GetAnimPlayKey(LimitBreakEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x060017F7 RID: 6135 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600169A")]
		[Address(RVA = "0x10122B074", Offset = "0x122B074", VA = "0x10122B074")]
		private string GetClipName(LimitBreakEffectHandler.eAnimIndex animIndex, LimitBreakEffectHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x060017F8 RID: 6136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600169B")]
		[Address(RVA = "0x10122B28C", Offset = "0x122B28C", VA = "0x10122B28C")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x060017F9 RID: 6137 RVA: 0x0000AF98 File Offset: 0x00009198
		[Token(Token = "0x600169C")]
		[Address(RVA = "0x10122B2E4", Offset = "0x122B2E4", VA = "0x10122B2E4")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x060017FA RID: 6138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600169D")]
		[Address(RVA = "0x10122B314", Offset = "0x122B314", VA = "0x10122B314")]
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
		}

		// Token: 0x060017FB RID: 6139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600169E")]
		[Address(RVA = "0x10122B36C", Offset = "0x122B36C", VA = "0x10122B36C")]
		public LimitBreakEffectHandler()
		{
		}

		// Token: 0x040026ED RID: 9965
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002016")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x040026EE RID: 9966
		[Token(Token = "0x4002017")]
		private const string CLIP_NAME_FORMAT = "{0}_{1}";

		// Token: 0x040026EF RID: 9967
		[Token(Token = "0x4002018")]
		private const string CLIP_NAME_FORMAT_LETTER = "{0}_letter";

		// Token: 0x040026F0 RID: 9968
		[Token(Token = "0x4002019")]
		private const string FULL_CLIP_NAME_FORMAT = "Mix_LimitBreak@{0}";

		// Token: 0x040026F1 RID: 9969
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400201A")]
		private readonly float CLOVER_START_FRAME;

		// Token: 0x040026F2 RID: 9970
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400201B")]
		private readonly float CLOVER_INTERVAL_FRAME;

		// Token: 0x040026F3 RID: 9971
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400201C")]
		private readonly float[] LETTER_START_FRAME;

		// Token: 0x040026F4 RID: 9972
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400201D")]
		private readonly int LAYER_INDEX_LABEL;

		// Token: 0x040026F5 RID: 9973
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400201E")]
		private readonly int LAYER_INDEX_LETTER;

		// Token: 0x040026F6 RID: 9974
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400201F")]
		private readonly int LAYER_INDEX_CLOVER;

		// Token: 0x040026F7 RID: 9975
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002020")]
		public Transform m_Transform;

		// Token: 0x040026F8 RID: 9976
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002021")]
		public Animation m_Anim;

		// Token: 0x040026F9 RID: 9977
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002022")]
		public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x040026FA RID: 9978
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002023")]
		public MsbHandler m_MsbHndl;

		// Token: 0x040026FB RID: 9979
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002024")]
		public MsbHndlWrapper m_MsbHndlWrapper;

		// Token: 0x040026FC RID: 9980
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002025")]
		public List<MeigeAnimCtrl> m_MeigeAnimCtrlsList;

		// Token: 0x040026FD RID: 9981
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002026")]
		public List<LimitBreakEffectHandler.AnimClipHolderList> m_MeigeAnimClipHoldersList;

		// Token: 0x040026FE RID: 9982
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002027")]
		public GameObject[] m_Clover;

		// Token: 0x040026FF RID: 9983
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002028")]
		private LimitBreakEffectHandler.eAnimType m_CurrentAnimType;

		// Token: 0x04002700 RID: 9984
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002029")]
		private LimitBreakEffectHandler.eAnimType[] m_AnimTypeArray;

		// Token: 0x04002701 RID: 9985
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400202A")]
		private int m_StartLevel;

		// Token: 0x04002702 RID: 9986
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x400202B")]
		private int m_EndLevel;

		// Token: 0x04002703 RID: 9987
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400202C")]
		private int m_NextCloverIndex;

		// Token: 0x04002704 RID: 9988
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x400202D")]
		private float m_Elapsed;

		// Token: 0x04002705 RID: 9989
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400202E")]
		private float m_ElapsedClover;

		// Token: 0x04002706 RID: 9990
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x400202F")]
		private float m_CloverStart;

		// Token: 0x04002707 RID: 9991
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002030")]
		private float m_CloverInterval;

		// Token: 0x04002708 RID: 9992
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4002031")]
		private float m_LetterStart;

		// Token: 0x04002709 RID: 9993
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002032")]
		private bool m_CloverFlashSound;

		// Token: 0x0400270A RID: 9994
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002033")]
		private float m_CloverFlashSec;

		// Token: 0x0400270B RID: 9995
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002034")]
		public Action<LimitBreakEffectScene.eSEIndex> PlaySoundCallback;

		// Token: 0x0400270C RID: 9996
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002035")]
		public Action PlayVoiceCallback;

		// Token: 0x02000663 RID: 1635
		[Token(Token = "0x2000DF4")]
		private enum eAnimIndex
		{
			// Token: 0x0400270E RID: 9998
			[Token(Token = "0x4005A59")]
			Label,
			// Token: 0x0400270F RID: 9999
			[Token(Token = "0x4005A5A")]
			Letter,
			// Token: 0x04002710 RID: 10000
			[Token(Token = "0x4005A5B")]
			Leaf_0,
			// Token: 0x04002711 RID: 10001
			[Token(Token = "0x4005A5C")]
			Leaf_1,
			// Token: 0x04002712 RID: 10002
			[Token(Token = "0x4005A5D")]
			Leaf_2,
			// Token: 0x04002713 RID: 10003
			[Token(Token = "0x4005A5E")]
			Leaf_3
		}

		// Token: 0x02000664 RID: 1636
		[Token(Token = "0x2000DF5")]
		[Serializable]
		public class AnimClipHolderList
		{
			// Token: 0x060017FC RID: 6140 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E21")]
			[Address(RVA = "0x10122B4FC", Offset = "0x122B4FC", VA = "0x10122B4FC")]
			public AnimClipHolderList()
			{
			}

			// Token: 0x04002714 RID: 10004
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005A5F")]
			public List<MeigeAnimClipHolder> m_List;
		}

		// Token: 0x02000665 RID: 1637
		[Token(Token = "0x2000DF6")]
		public enum eAnimType
		{
			// Token: 0x04002716 RID: 10006
			[Token(Token = "0x4005A61")]
			None = -1,
			// Token: 0x04002717 RID: 10007
			[Token(Token = "0x4005A62")]
			Effect,
			// Token: 0x04002718 RID: 10008
			[Token(Token = "0x4005A63")]
			Loop,
			// Token: 0x04002719 RID: 10009
			[Token(Token = "0x4005A64")]
			Num
		}

		// Token: 0x02000666 RID: 1638
		[Token(Token = "0x2000DF7")]
		public enum eAnimEvent
		{
			// Token: 0x0400271B RID: 10011
			[Token(Token = "0x4005A66")]
			Whiteout = 102
		}
	}
}
