﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000667 RID: 1639
	[Token(Token = "0x2000543")]
	[StructLayout(3)]
	public class LimitBreakEffectScene
	{
		// Token: 0x060017FD RID: 6141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600169F")]
		[Address(RVA = "0x10122B504", Offset = "0x122B504", VA = "0x10122B504")]
		public LimitBreakEffectScene()
		{
		}

		// Token: 0x060017FE RID: 6142 RVA: 0x0000AFB0 File Offset: 0x000091B0
		[Token(Token = "0x60016A0")]
		[Address(RVA = "0x10122B7A4", Offset = "0x122B7A4", VA = "0x10122B7A4")]
		public bool Destroy()
		{
			return default(bool);
		}

		// Token: 0x060017FF RID: 6143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016A1")]
		[Address(RVA = "0x10122B924", Offset = "0x122B924", VA = "0x10122B924")]
		public void Update()
		{
		}

		// Token: 0x06001800 RID: 6144 RVA: 0x0000AFC8 File Offset: 0x000091C8
		[Token(Token = "0x60016A2")]
		[Address(RVA = "0x10122BDF8", Offset = "0x122BDF8", VA = "0x10122BDF8")]
		public bool Prepare(Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x06001801 RID: 6145 RVA: 0x0000AFE0 File Offset: 0x000091E0
		[Token(Token = "0x60016A3")]
		[Address(RVA = "0x10122BEA8", Offset = "0x122BEA8", VA = "0x10122BEA8")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06001802 RID: 6146 RVA: 0x0000AFF8 File Offset: 0x000091F8
		[Token(Token = "0x60016A4")]
		[Address(RVA = "0x10122BEB8", Offset = "0x122BEB8", VA = "0x10122BEB8")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06001803 RID: 6147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016A5")]
		[Address(RVA = "0x10122B940", Offset = "0x122B940", VA = "0x10122B940")]
		private void Update_Prepare()
		{
		}

		// Token: 0x06001804 RID: 6148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016A6")]
		[Address(RVA = "0x10122BEC8", Offset = "0x122BEC8", VA = "0x10122BEC8")]
		public void Play(UserCharacterData userCharacterData, int beforeLevel, int afterLevel)
		{
		}

		// Token: 0x06001805 RID: 6149 RVA: 0x0000B010 File Offset: 0x00009210
		[Token(Token = "0x60016A7")]
		[Address(RVA = "0x10122BFB0", Offset = "0x122BFB0", VA = "0x10122BFB0")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06001806 RID: 6150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016A8")]
		[Address(RVA = "0x10122BCC0", Offset = "0x122BCC0", VA = "0x10122BCC0")]
		private void Update_Main()
		{
		}

		// Token: 0x06001807 RID: 6151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016A9")]
		[Address(RVA = "0x10122BFC0", Offset = "0x122BFC0", VA = "0x10122BFC0")]
		private void Skip()
		{
		}

		// Token: 0x06001808 RID: 6152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016AA")]
		[Address(RVA = "0x10122C290", Offset = "0x122C290", VA = "0x10122C290")]
		public void DetectTouch()
		{
		}

		// Token: 0x06001809 RID: 6153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016AB")]
		[Address(RVA = "0x10122C29C", Offset = "0x122C29C", VA = "0x10122C29C")]
		private void PlaySound(LimitBreakEffectScene.eSEIndex seIndex)
		{
		}

		// Token: 0x0600180A RID: 6154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016AC")]
		[Address(RVA = "0x10122C0F0", Offset = "0x122C0F0", VA = "0x10122C0F0")]
		private void PlayVoice()
		{
		}

		// Token: 0x0600180B RID: 6155 RVA: 0x0000B028 File Offset: 0x00009228
		[Token(Token = "0x60016AD")]
		[Address(RVA = "0x10122C310", Offset = "0x122C310", VA = "0x10122C310")]
		private eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_000;
		}

		// Token: 0x0400271C RID: 10012
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002036")]
		private LimitBreakEffectScene.eMode m_Mode;

		// Token: 0x0400271D RID: 10013
		[Token(Token = "0x4002037")]
		private const string RESOURCE_PATH = "mix/mix_limitbreak.muast";

		// Token: 0x0400271E RID: 10014
		[Token(Token = "0x4002038")]
		private const string OBJECT_PATH = "Mix_LimitBreak";

		// Token: 0x0400271F RID: 10015
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002039")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x04002720 RID: 10016
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400203A")]
		private ABResourceLoader m_Loader;

		// Token: 0x04002721 RID: 10017
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400203B")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04002722 RID: 10018
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400203C")]
		private GameObject m_EffectGO;

		// Token: 0x04002723 RID: 10019
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400203D")]
		private LimitBreakEffectHandler m_EffectHndl;

		// Token: 0x04002724 RID: 10020
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400203E")]
		private Transform m_ParentTransform;

		// Token: 0x04002725 RID: 10021
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400203F")]
		private Vector3 m_Position;

		// Token: 0x04002726 RID: 10022
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002040")]
		private SoundSeRequest[] m_SeRequest;

		// Token: 0x04002727 RID: 10023
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002041")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04002728 RID: 10024
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002042")]
		private LimitBreakEffectScene.ePrepareStep m_PrepareStep;

		// Token: 0x04002729 RID: 10025
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002043")]
		private LimitBreakEffectScene.eMainStep m_MainStep;

		// Token: 0x0400272A RID: 10026
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002044")]
		private bool m_DetectTouch;

		// Token: 0x0400272B RID: 10027
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002045")]
		private int m_RequestVoiceID;

		// Token: 0x02000668 RID: 1640
		[Token(Token = "0x2000DF8")]
		public enum eSEIndex
		{
			// Token: 0x0400272D RID: 10029
			[Token(Token = "0x4005A68")]
			BaseSound,
			// Token: 0x0400272E RID: 10030
			[Token(Token = "0x4005A69")]
			Letter,
			// Token: 0x0400272F RID: 10031
			[Token(Token = "0x4005A6A")]
			Clover,
			// Token: 0x04002730 RID: 10032
			[Token(Token = "0x4005A6B")]
			CloverFlash
		}

		// Token: 0x02000669 RID: 1641
		[Token(Token = "0x2000DF9")]
		public enum eMode
		{
			// Token: 0x04002732 RID: 10034
			[Token(Token = "0x4005A6D")]
			None = -1,
			// Token: 0x04002733 RID: 10035
			[Token(Token = "0x4005A6E")]
			Prepare,
			// Token: 0x04002734 RID: 10036
			[Token(Token = "0x4005A6F")]
			UpdateMain,
			// Token: 0x04002735 RID: 10037
			[Token(Token = "0x4005A70")]
			Destroy
		}

		// Token: 0x0200066A RID: 1642
		[Token(Token = "0x2000DFA")]
		private enum ePrepareStep
		{
			// Token: 0x04002737 RID: 10039
			[Token(Token = "0x4005A72")]
			None = -1,
			// Token: 0x04002738 RID: 10040
			[Token(Token = "0x4005A73")]
			Prepare,
			// Token: 0x04002739 RID: 10041
			[Token(Token = "0x4005A74")]
			Prepare_Wait,
			// Token: 0x0400273A RID: 10042
			[Token(Token = "0x4005A75")]
			End,
			// Token: 0x0400273B RID: 10043
			[Token(Token = "0x4005A76")]
			Prepare_Error
		}

		// Token: 0x0200066B RID: 1643
		[Token(Token = "0x2000DFB")]
		private enum eMainStep
		{
			// Token: 0x0400273D RID: 10045
			[Token(Token = "0x4005A78")]
			None = -1,
			// Token: 0x0400273E RID: 10046
			[Token(Token = "0x4005A79")]
			Play,
			// Token: 0x0400273F RID: 10047
			[Token(Token = "0x4005A7A")]
			Play_Wait,
			// Token: 0x04002740 RID: 10048
			[Token(Token = "0x4005A7B")]
			End
		}
	}
}
