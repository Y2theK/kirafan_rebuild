﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AD2 RID: 2770
	[Token(Token = "0x2000799")]
	[StructLayout(3)]
	public class LocalNotificationController : MonoBehaviour
	{
		// Token: 0x0600307B RID: 12411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C51")]
		[Address(RVA = "0x10122C318", Offset = "0x122C318", VA = "0x10122C318")]
		private void Awake()
		{
		}

		// Token: 0x0600307C RID: 12412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C52")]
		[Address(RVA = "0x10122C31C", Offset = "0x122C31C", VA = "0x10122C31C")]
		public static void RegisterForNotifications()
		{
		}

		// Token: 0x0600307D RID: 12413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C53")]
		[Address(RVA = "0x10122C328", Offset = "0x122C328", VA = "0x10122C328")]
		public static void SimpleNotification(int id, TimeSpan delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "", [Optional] Color color)
		{
		}

		// Token: 0x0600307E RID: 12414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C54")]
		[Address(RVA = "0x10122C3BC", Offset = "0x122C3BC", VA = "0x10122C3BC")]
		public static void SimpleNotification(int id, int delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "", [Optional] Color color)
		{
		}

		// Token: 0x0600307F RID: 12415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C55")]
		[Address(RVA = "0x10122C410", Offset = "0x122C410", VA = "0x10122C410")]
		public static void SendNotification(int id, long delay, string title, string message, string smallIcon, string bigIcon = "", [Optional] Color32 bgColor, bool sound = true, bool vibrate = true, bool lights = true)
		{
		}

		// Token: 0x06003080 RID: 12416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C56")]
		[Address(RVA = "0x10122C61C", Offset = "0x122C61C", VA = "0x10122C61C")]
		public static void UnregisterNotification(int id)
		{
		}

		// Token: 0x06003081 RID: 12417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C57")]
		[Address(RVA = "0x10122C9BC", Offset = "0x122C9BC", VA = "0x10122C9BC")]
		public static void UnregisterNotificationAll()
		{
		}

		// Token: 0x06003082 RID: 12418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C58")]
		[Address(RVA = "0x10122CA58", Offset = "0x122CA58", VA = "0x10122CA58")]
		private void OnApplicationPause(bool isPause)
		{
		}

		// Token: 0x06003083 RID: 12419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002C59")]
		[Address(RVA = "0x10122CAEC", Offset = "0x122CAEC", VA = "0x10122CAEC")]
		public LocalNotificationController()
		{
		}
	}
}
