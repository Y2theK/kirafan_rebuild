﻿using MsgPack;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Star {
    public class LocalSaveData {
        public const int BATTLE_SPEED_LV_NUM = 2;

        private static readonly LocalSaveData instance = new LocalSaveData();
        private static readonly string EncryptKey = "3xgLmcc62dNLJ3Te6phdzf2TFj85yhxR";
        private static readonly int EncryptPasswordCount = 16;
        private static readonly string FilePrefix = string.Empty;
        private static readonly string SaveFile = Application.persistentDataPath + "/" + FilePrefix + "s.d";
        private static readonly string SaveFileBak = Application.persistentDataPath + "/" + FilePrefix + "s.d2";
        private static readonly string PasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public static float[] BATTLE_SPEED_LV_SCALE = new float[] {
            1f,
            2.5f
        };

        public static float[] BATTLE_TOGETHER_SPEED_LV_SCALE = new float[] {
            1f,
            1.8f
        };

        private readonly SaveDataVersion m_LatestVersion = SaveDataVersion._171130;

        private SaveData m_SaveData = new SaveData();

        public static LocalSaveData Inst => instance;
        public bool SaveDataAutoDelete { get; set; }

        public float BgmVolume {
            get => m_SaveData.m_BgmVolume;
            set => m_SaveData.m_BgmVolume = value;
        }

        public float SeVolume {
            get => m_SaveData.m_SeVolume;
            set => m_SaveData.m_SeVolume = value;
        }

        public float VoiceVolume {
            get => m_SaveData.m_VoiceVolume;
            set => m_SaveData.m_VoiceVolume = value;
        }

        public float MovieVolume {
            get => m_SaveData.m_MovieVolume;
            set => m_SaveData.m_MovieVolume = value;
        }

        public bool MoveInitFlag {
            get => m_SaveData.m_MoveInitFlag;
            set => m_SaveData.m_MoveInitFlag = value;
        }

        public eAmount BattleEffect {
            get => m_SaveData.m_BattleEffect;
            set => m_SaveData.m_BattleEffect = value;
        }

        public bool BattleAutoMode {
            get => m_SaveData.m_BattleAutoMode;
            set => m_SaveData.m_BattleAutoMode = value;
        }

        public bool BattleUseSkillInAuto {
            get => m_SaveData.m_BattleUseSkillInAuto;
            set => m_SaveData.m_BattleUseSkillInAuto = value;
        }

        public eBattleAutoCancelType BattleAutoCancel {
            get => m_SaveData.m_BattleAutoCancel;
            set => m_SaveData.m_BattleAutoCancel = value;
        }

        public bool BattleSkipTogetherAttackInput {
            get => m_SaveData.m_BattleSkipTogetherAttackInput;
            set => m_SaveData.m_BattleSkipTogetherAttackInput = value;
        }

        public bool BattleIsSkipUniqueSkill {
            get => m_SaveData.m_BattleIsSkipUniqueSkill;
            set => m_SaveData.m_BattleIsSkipUniqueSkill = value;
        }

        public bool ADVSkipOnBattle {
            get => m_SaveData.m_ADVSkipOnBattle;
            set => m_SaveData.m_ADVSkipOnBattle = value;
        }

        public bool TownSkipGreet {
            get => m_SaveData.m_RoomSkipGreet;
            set => m_SaveData.m_RoomSkipGreet = value;
        }

        public string StoreReviewAppVersion {
            get => m_SaveData.m_StoreReviewAppVersion;
            set => m_SaveData.m_StoreReviewAppVersion = value;
        }

        public int SelectedPartyIndex {
            get => m_SaveData.m_SelectedPartyIndex;
            set => m_SaveData.m_SelectedPartyIndex = value;
        }

        public int RetireTipsIdx {
            get => m_SaveData.m_RetireTipsIdx;
            set => m_SaveData.m_RetireTipsIdx = value;
        }

        public bool OccurredTrainingPrepare {
            get => m_SaveData.m_OccurredTrainingPrepare;
            set => m_SaveData.m_OccurredTrainingPrepare = value;
        }

        public bool Agreement {
            get => m_SaveData.m_Agreement;
            set => m_SaveData.m_Agreement = value;
        }

        public bool IsConfirmedDownloadCaution {
            get => m_SaveData.m_IsConfirmedDownloadCaution;
            set => m_SaveData.m_IsConfirmedDownloadCaution = value;
        }

        public List<UISortParam> UISortParams {
            get => m_SaveData.m_UISortParams;
            set => m_SaveData.m_UISortParams = value;
        }

        public List<UIFilterParam> UIFilterParams {
            get => m_SaveData.m_UIFilterParams;
            set => m_SaveData.m_UIFilterParams = value;
        }

        public List<int> playedGachaAdvertiseList {
            get => m_SaveData.m_PlayedGachaAdvertiseList;
            set => m_SaveData.m_PlayedGachaAdvertiseList = value;
        }

        public List<int> playedEventOpList {
            get => m_SaveData.m_PlayedEventOpList;
            set => m_SaveData.m_PlayedEventOpList = value;
        }

        public bool EventBonusFilter {
            get => m_SaveData.m_EventBonusFilter;
            set => m_SaveData.m_EventBonusFilter = value;
        }

        public bool EventBonusFilterSupport {
            get => m_SaveData.m_EventBonusFilterSupport;
            set => m_SaveData.m_EventBonusFilterSupport = value;
        }

        public bool isADV_Story_FullVoice {
            get => m_SaveData.m_isADV_Story_FullVoice_Ver1;
            set => m_SaveData.m_isADV_Story_FullVoice_Ver1 = value;
        }

        public bool isADV_Story_FullVoiceConfirm {
            get => m_SaveData.m_isADV_Story_FullVoiceConfirm_Ver1;
            set => m_SaveData.m_isADV_Story_FullVoiceConfirm_Ver1 = value;
        }

        public bool isADV_Event_FullVoice {
            get => m_SaveData.m_isADV_Event_FullVoice_Ver1;
            set => m_SaveData.m_isADV_Event_FullVoice_Ver1 = value;
        }

        public bool isADV_Event_FullVoiceConfirm {
            get => m_SaveData.m_isADV_Event_FullVoiceConfirm_Ver1;
            set => m_SaveData.m_isADV_Event_FullVoiceConfirm_Ver1 = value;
        }

        public bool isADV_Memorial_FullVoice {
            get => m_SaveData.m_isADV_Memorial_FullVoice_Ver1;
            set => m_SaveData.m_isADV_Memorial_FullVoice_Ver1 = value;
        }

        public bool isADV_Memorial_FullVoiceConfirm {
            get => m_SaveData.m_isADV_Memorial_FullVoiceConfirm_Ver1;
            set => m_SaveData.m_isADV_Memorial_FullVoiceConfirm_Ver1 = value;
        }

        public eTownBuildLvInfoState TownUIShowBuildLvInfo {
            get => m_SaveData.m_TownUIShowBuildLvInfo;
            set => m_SaveData.m_TownUIShowBuildLvInfo = value;
        }

        public AchievementDefine.eFilterType achvFilterType {
            get => m_SaveData.m_achvFilterType;
            set => m_SaveData.m_achvFilterType = value;
        }

        public void SetADVFullVoiceConfirm(eADVCategory category, bool value) {
            switch (category) {
                case eADVCategory.Story:
                    Inst.m_SaveData.m_isADV_Story_FullVoiceConfirm_Ver1 = value;
                    break;
                case eADVCategory.Event:
                    Inst.m_SaveData.m_isADV_Event_FullVoiceConfirm_Ver1 = value;
                    break;
                case eADVCategory.Weapon:
                    Inst.m_SaveData.m_isADV_Memorial_FullVoiceConfirm_Ver1 = value;
                    break;
            }
        }

        public bool IsADVFullVoiceConfirm(eADVCategory category) {
            return category switch {
                eADVCategory.Story => Inst.m_SaveData.m_isADV_Story_FullVoiceConfirm_Ver1,
                eADVCategory.Event => Inst.m_SaveData.m_isADV_Event_FullVoiceConfirm_Ver1,
                eADVCategory.Weapon => Inst.m_SaveData.m_isADV_Memorial_FullVoiceConfirm_Ver1,
                _ => true,
            };
        }

        public void SetADVFullVoice(eADVCategory category, bool value) {
            switch (category) {
                case eADVCategory.Story:
                    Inst.m_SaveData.m_isADV_Story_FullVoice_Ver1 = value;
                    break;
                case eADVCategory.Event:
                    Inst.m_SaveData.m_isADV_Event_FullVoice_Ver1 = value;
                    break;
                case eADVCategory.Weapon:
                    Inst.m_SaveData.m_isADV_Memorial_FullVoice_Ver1 = value;
                    break;
            }
        }

        public bool IsADVFullVoice(eADVCategory category) {
            return category switch {
                eADVCategory.Story => Inst.m_SaveData.m_isADV_Story_FullVoice_Ver1,
                eADVCategory.Event => Inst.m_SaveData.m_isADV_Event_FullVoice_Ver1,
                eADVCategory.Weapon => Inst.m_SaveData.m_isADV_Memorial_FullVoice_Ver1,
                _ => true,
            };
        }

        public void SetBattleSpeedLvMasterFlg(bool flg) {
            m_SaveData.m_BattleSpeedLvMasterFlg = flg;
            for (int i = 0; i < m_SaveData.m_BattleSpeedLvs.Length; i++) {
                m_SaveData.m_BattleSpeedLvs[i] = 1;
            }
        }

        public bool GetBattleSpeedLvMasterFlg() {
            return m_SaveData.m_BattleSpeedLvMasterFlg;
        }

        public bool ToggleBattleSpeedLvMasterFlg() {
            SetBattleSpeedLvMasterFlg(!GetBattleSpeedLvMasterFlg());
            return m_SaveData.m_BattleSpeedLvMasterFlg;
        }

        public float GetBattleSpeedScale(eBattleSpeedLv type) {
            if (m_SaveData.m_BattleSpeedLvMasterFlg) {
                return BATTLE_SPEED_LV_SCALE[GetBattleSpeedLv(type)];
            }
            return 1f;
        }

        public float GetBattleTogetherSpeedScale(eBattleSpeedLv type) {
            if (m_SaveData.m_BattleSpeedLvMasterFlg) {
                return BATTLE_TOGETHER_SPEED_LV_SCALE[GetBattleSpeedLv(type)];
            }
            return 1f;
        }

        public int GetBattleSpeedLv(eBattleSpeedLv type) {
            return m_SaveData.m_BattleSpeedLvs[(int)type];
        }

        public void SetBattleSpeedLv(eBattleSpeedLv type, int lv) {
            m_SaveData.m_BattleSpeedLvs[(int)type] = lv % BATTLE_SPEED_LV_NUM;
        }

        public int ToggleBattleSpeedLv(eBattleSpeedLv type) {
            int lv = (m_SaveData.m_BattleSpeedLvs[(int)type] + 1) % BATTLE_SPEED_LV_NUM;
            SetBattleSpeedLv(type, lv);
            return m_SaveData.m_BattleSpeedLvs[(int)type];
        }

        public void AddOccurredEventType(int eventType) {
            if (m_SaveData.m_OccurredEventTypes != null && !m_SaveData.m_OccurredEventTypes.Contains(eventType)) {
                m_SaveData.m_OccurredEventTypes.Add(eventType);
            }
        }

        public void AddOccurredEventGroup(int groupID) {
            if (m_SaveData.m_OccurredEventGroups != null && !m_SaveData.m_OccurredEventGroups.Contains(groupID)) {
                m_SaveData.m_OccurredEventGroups.Add(groupID);
            }
        }

        public bool IsOccurredEventType(int eventType) {
            return m_SaveData.m_OccurredEventTypes == null || m_SaveData.m_OccurredEventTypes.Contains(eventType);
        }

        public bool IsOccurredEventGroup(int groupID) {
            return m_SaveData.m_OccurredEventGroups == null || m_SaveData.m_OccurredEventGroups.Contains(groupID);
        }

        public int GetTitleCallCharacterCueSheetIndex() {
            return m_SaveData.m_TitleCallCueSheetIndex - 1;
        }

        public void SetTitleCallCharacterCueSheetIndex(int cueSheetIndex) {
            m_SaveData.m_TitleCallCueSheetIndex = cueSheetIndex + 1;
        }

        public int GetTitleCallCharacterNamedListIndex() {
            return m_SaveData.m_TitleCallNamedIndex - 1;
        }

        public void SetTitleCallCharacterNamedListIndex(int namedIndex) {
            m_SaveData.m_TitleCallNamedIndex = namedIndex + 1;
        }

        public int GetTitleCallCharacterIdForSelector() {
            return m_SaveData.m_TitleCallCharaIDForSelector - 1;
        }

        public void SetTitleCallCharacterIdForSelector(int charaID) {
            m_SaveData.m_TitleCallCharaIDForSelector = charaID + 1;
        }

        public bool CheckCacheCleanVersion(int id, int version) {
            if (version != -1 && m_SaveData.m_CacheClearParams != null) {
                List<CacheClearParam> cParams = m_SaveData.m_CacheClearParams;
                for (int i = 0; i < m_SaveData.m_CacheClearParams.Count; i++) {
                    if (cParams[i].m_ID == id) {
                        return cParams[i].m_Version != version;
                    }
                }
            }
            return true;
        }

        public bool UpdateCacheCleanVersion(int id, int version) {
            if (version != -1 && m_SaveData.m_CacheClearParams != null) {
                return false;
            }

            List<CacheClearParam> cParams = m_SaveData.m_CacheClearParams;
            for (int i = 0; i < m_SaveData.m_CacheClearParams.Count; i++) {
                if (cParams[i].m_ID == id) {
                    cParams[i].m_Version = version;
                    return true;
                }
            }
            cParams.Add(new CacheClearParam(id, version));
            return true;
        }

        public void Save() {
            ObjectPacker objectPacker = new ObjectPacker();
            byte[] src = objectPacker.Pack(m_SaveData);
            if (File.Exists(SaveFile)) {
                File.Copy(SaveFile, SaveFileBak, true);
            }
            try {
                EncryptAes(src, EncryptKey, EncryptPasswordCount, out string s, out byte[] array);
                using (var stream = new FileStream(SaveFile, FileMode.Create, FileAccess.Write))
                using (var writer = new BinaryWriter(stream)) {
                    int header = Random.Range(-2147483647, int.MaxValue);
                    byte mask = (byte)(header & 0xFF);
                    writer.Write((int)(header & 0xFFFF00FF) | (0xFF00 & ((short)m_LatestVersion << 8)));
                    byte[] bytes = Encoding.UTF8.GetBytes(s);
                    for (int i = 0; i < bytes.Length; i++) {
                        bytes[i] += (byte)(0x60 + i);
                    }
                    writer.Write((byte)(bytes.Length + mask));
                    writer.Write(bytes);
                    writer.Write(array.Length);
                    writer.Write(array);
                }
            } catch {
            }
        }

        public void Load() {
            Load(SaveFile);
        }

        public bool Load(string path) {
            if (!File.Exists(path)) {
                return false;
            }
            bool versionMismatch = false;
            try {
                using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read)) {
                    string pw = null;
                    byte[] array = null;
                    using (var binaryReader = new System.IO.BinaryReader(fileStream)) {
                        int header = binaryReader.ReadInt32();
                        byte mask = (byte)(header & 0xFF);
                        int version = (header & 0xFF00) >> 8;
                        if (version != (int)m_LatestVersion) {
                            versionMismatch = true;
                        } else {
                            int count = binaryReader.ReadByte() - mask;
                            array = binaryReader.ReadBytes(count);
                            for (int i = 0; i < array.Length; i++) {
                                array[i] -= (byte)(0x60 + i);
                            }
                            pw = Encoding.UTF8.GetString(array);
                            count = binaryReader.ReadInt32();
                            array = binaryReader.ReadBytes(count);
                        }
                    }
                    if (!versionMismatch) {
                        ObjectPacker objectPacker = new ObjectPacker();
                        DecryptAes(array, EncryptKey, pw, out byte[] buf);
                        m_SaveData = objectPacker.Unpack<SaveData>(buf);
                        if (m_SaveData != null) {
                            if (!m_SaveData.m_MoveInitFlag) {
                                m_SaveData.m_MovieVolume = 1;
                                m_SaveData.m_MoveInitFlag = true;
                            }
                            if (m_SaveData.m_OccurredEventTypes == null) {
                                m_SaveData.m_OccurredEventTypes = new List<int>();
                            }
                            if (m_SaveData.m_OccurredEventGroups == null) {
                                m_SaveData.m_OccurredEventGroups = new List<int>();
                            }
                            if (m_SaveData.m_CacheClearParams == null) {
                                m_SaveData.m_CacheClearParams = new List<CacheClearParam>();
                            }
                        }
                    }
                }
            } catch {
                bool backupLoaded = false;
                if (SaveFileBak != path) {
                    backupLoaded = Load(SaveFileBak);
                }
                if (!backupLoaded) {
                    return false;
                }
            }
            if (versionMismatch) {
                DeleteAllSaveData();
                SaveDataAutoDelete = true;
                return true;
            }
            AfterLoad();
            return true;
        }

        public void DeleteAllSaveData() {
            DeleteSaveData(SaveFile);
            DeleteSaveData(SaveFileBak);
        }

        public void DeleteSaveData(string path) {
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }

        public void ResetSave() {
            m_SaveData.Reset();
            GameSystem.Inst.UISettings.Clear();
            Save();
        }

        private void AfterLoad() { }

        private static void EncryptAes(byte[] src, string encryptKey, int pwSize, out string pw, out byte[] dst) {
            pw = CreatePassword(pwSize);
            dst = null;
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(encryptKey);
                byte[] iv = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateEncryptor(key, iv))
                using (var stream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(src, 0, src.Length);
                    cryptoStream.FlushFinalBlock();
                    dst = stream.ToArray();
                }
            }
        }

        private static void DecryptAes(byte[] src, string encryptKey, string pw, out byte[] dst) {
            dst = new byte[src.Length];
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                byte[] key = Encoding.UTF8.GetBytes(encryptKey);
                byte[] iv = Encoding.UTF8.GetBytes(pw);

                using (var transform = rijndael.CreateDecryptor(key, iv))
                using (var stream = new MemoryStream(src))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read)) {
                    cryptoStream.Read(dst, 0, dst.Length);
                }
            }
        }

        private static string CreatePassword(int count) {
            StringBuilder stringBuilder = new StringBuilder(count);
            for (int i = count - 1; i >= 0; i--) {
                char value = PasswordChars[Random.Range(0, PasswordChars.Length)];
                stringBuilder.Append(value);
            }
            return stringBuilder.ToString();
        }

        public enum SaveDataVersion {
            _170203 = 17,
            _170209,
            _170307,
            _170311,
            _170331,
            _170404,
            _170420,
            _170608,
            _170805,
            _170905,
            _170906,
            _170914,
            _171027,
            _171028,
            _171111,
            _171122,
            _171130,
            _latest
        }

        public enum eAmount {
            High,
            Middle,
            Low
        }

        public enum eBattleSpeedLv {
            PL,
            EN,
            UniqueSkill,
            Num
        }

        public enum eBattleAutoCancelType {
            ButtonOnly,
            AllScreen,
            Num
        }

        public enum eTownBuildLvInfoState {
            Show,
            Hide
        }

        public class UISortParam {
            public ushort m_Value;
            public ushort m_OrderType;

            public UISortParam() {
                m_Value = 0;
                m_OrderType = 0;
            }

            public void Clear() {
                m_Value = 0;
                m_OrderType = 0;
            }
        }

        public class UIFilterParam {
            public const int FILTER_TYPE_MAX = 12;

            private long[] m_BitFlgs;

            public UIFilterParam(long flg) {
                m_BitFlgs = new long[FILTER_TYPE_MAX];
                for (int i = 0; i < m_BitFlgs.Length; i++) {
                    m_BitFlgs[i] = flg;
                }
            }

            public UIFilterParam() {
                m_BitFlgs = new long[FILTER_TYPE_MAX];
                for (int i = 0; i < m_BitFlgs.Length; i++) {
                    m_BitFlgs[i] = 0L;
                }
            }

            public void Clear() {
                for (int i = 0; i < m_BitFlgs.Length; i++) {
                    m_BitFlgs[i] = 0L;
                }
            }

            public bool HasFlag(int typeIndex, int bitIndex) {
                return typeIndex < 0 || typeIndex >= m_BitFlgs.Length || ((m_BitFlgs[typeIndex] >> (bitIndex & 31)) & 1L) != 0L;
            }

            public void SetFlag(int typeIndex, int bitIndex, bool isOn) {
                if (typeIndex >= 0 && typeIndex < m_BitFlgs.Length) {
                    if (isOn) {
                        m_BitFlgs[typeIndex] |= (long)(1UL << (bitIndex & 31));
                    } else {
                        m_BitFlgs[typeIndex] &= ~(1L << (bitIndex & 31));
                    }
                }
            }

            public bool ToggleFlg(int typeIndex, int bitIndex) {
                SetFlag(typeIndex, bitIndex, !HasFlag(typeIndex, bitIndex));
                return HasFlag(typeIndex, bitIndex);
            }

            public int GetTypeNum() {
                return m_BitFlgs.Length;
            }
        }

        public class CacheClearParam {
            public int m_ID;
            public int m_Version;

            public CacheClearParam() {
                m_ID = 0;
                m_Version = 0;
            }

            public CacheClearParam(int id, int version) {
                m_ID = id;
                m_Version = version;
            }
        }

        public class SaveData {
            public float m_BgmVolume = 1f;
            public float m_SeVolume = 1f;
            public float m_VoiceVolume = 1f;
            public float m_MovieVolume = 1f;
            public bool m_MoveInitFlag;
            public eAmount m_BattleEffect;
            public int[] m_BattleSpeedLvs = new int[3];
            public bool m_BattleAutoMode;
            public bool m_BattleUseSkillInAuto;
            public eBattleAutoCancelType m_BattleAutoCancel;
            public bool m_BattleSkipTogetherAttackInput;
            public bool m_BattleSkipUniqueSkill;
            public bool m_ADVSkipOnBattle;
            public bool m_RoomSkipGreet;
            public string m_StoreReviewAppVersion = "0.0.0";
            public int m_PlayedOpenChapterId;
            public int m_SelectedPartyIndex;
            public int m_RetireTipsIdx = -1;
            public bool m_OccurredTrainingPrepare;
            public bool m_Agreement;
            public bool m_IsConfirmedDownloadCaution;
            public bool m_BattleSpeedLvMasterFlg;
            public byte[] reserve = new byte[96];
            public List<UISortParam> m_UISortParams;
            public List<UIFilterParam> m_UIFilterParams;
            public List<int> m_PlayedGachaAdvertiseList = new List<int>();
            public bool m_EventBonusFilter;
            public bool m_EventBonusFilterSupport;
            public bool m_isADVFullVoice;
            public bool m_isADVFullVoiceConfirm;
            public bool m_isADVFullVoice_Ver1;
            public bool m_isADVFullVoiceConfirm_Ver1;
            public bool m_isADV_Story_FullVoice_Ver1;
            public bool m_isADV_Story_FullVoiceConfirm_Ver1;
            public bool m_isADV_Event_FullVoice_Ver1;
            public bool m_isADV_Event_FullVoiceConfirm_Ver1;
            public bool m_isADV_Memorial_FullVoice_Ver1;
            public bool m_isADV_Memorial_FullVoiceConfirm_Ver1;
            public List<int> m_OccurredEventTypes = new List<int>();
            public List<int> m_OccurredEventGroups = new List <int>();
            public List<int> m_PlayedEventOpList = new List<int>();
            public bool m_BattleIsSkipUniqueSkill;
            public int m_LastPlayedChapterQuestID = -1;
            public int m_TitleCallCueSheetIndex;
            public int m_TitleCallNamedIndex;
            public int m_TitleCallCharaIDForSelector;
            public List<CacheClearParam> m_CacheClearParams = new List<CacheClearParam>();
            public eTownBuildLvInfoState m_TownUIShowBuildLvInfo;
            public AchievementDefine.eFilterType m_achvFilterType;

            public SaveData() {
                Reset();
            }

            public void Reset() {
                m_BgmVolume = 1f;
                m_SeVolume = 1f;
                m_VoiceVolume = 1f;
                m_MovieVolume = 1f;
                m_MoveInitFlag = false;
                m_BattleEffect = eAmount.High;
                m_BattleAutoMode = false;
                m_BattleUseSkillInAuto = true;
                m_BattleAutoCancel = eBattleAutoCancelType.ButtonOnly;
                for (int i = 0; i < m_BattleSpeedLvs.Length; i++) {
                    m_BattleSpeedLvs[i] = 0;
                }
                m_BattleSkipTogetherAttackInput = true;
                m_BattleSkipUniqueSkill = false;
                m_ADVSkipOnBattle = true;
                m_RoomSkipGreet = false;
                m_BattleIsSkipUniqueSkill = false;
                m_BattleSpeedLvMasterFlg = false;
                m_PlayedOpenChapterId = 0;
                m_SelectedPartyIndex = 0;
                m_RetireTipsIdx = -1;
                m_OccurredTrainingPrepare = false;
                m_Agreement = false;
                m_PlayedGachaAdvertiseList.Clear();
                m_PlayedEventOpList.Clear();
                m_EventBonusFilter = false;
                m_EventBonusFilterSupport = false;
                m_isADV_Story_FullVoice_Ver1 = false;
                m_isADV_Story_FullVoiceConfirm_Ver1 = false;
                m_isADV_Event_FullVoice_Ver1 = false;
                m_isADV_Event_FullVoiceConfirm_Ver1 = false;
                m_isADV_Memorial_FullVoice_Ver1 = false;
                m_isADV_Memorial_FullVoiceConfirm_Ver1 = false;
                m_OccurredEventTypes.Clear();
                m_OccurredEventGroups.Clear();
                m_LastPlayedChapterQuestID = -1;
                m_TitleCallCueSheetIndex = 0;
                m_TitleCallNamedIndex = 0;
                m_TitleCallCharaIDForSelector = 0;
                m_CacheClearParams.Clear();
                m_TownUIShowBuildLvInfo = eTownBuildLvInfoState.Show;
                m_achvFilterType = AchievementDefine.eFilterType.All;
            }
        }
    }
}
