﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000ADD RID: 2781
	[Token(Token = "0x200079B")]
	[StructLayout(3)]
	public class LoginBonus
	{
		// Token: 0x060030FA RID: 12538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CC3")]
		[Address(RVA = "0x101230A2C", Offset = "0x1230A2C", VA = "0x101230A2C")]
		public LoginBonus()
		{
		}

		// Token: 0x0400405C RID: 16476
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002DEF")]
		public int m_ID;

		// Token: 0x0400405D RID: 16477
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002DF0")]
		public int m_ImageID;

		// Token: 0x0400405E RID: 16478
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002DF1")]
		public int m_DayIndex;

		// Token: 0x0400405F RID: 16479
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002DF2")]
		public LoginBonusDay[] m_Days;
	}
}
