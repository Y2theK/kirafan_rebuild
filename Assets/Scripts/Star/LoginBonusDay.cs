﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000ADE RID: 2782
	[Token(Token = "0x200079C")]
	[StructLayout(3)]
	public class LoginBonusDay
	{
		// Token: 0x060030FB RID: 12539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CC4")]
		[Address(RVA = "0x101230A34", Offset = "0x1230A34", VA = "0x101230A34")]
		public LoginBonusDay()
		{
		}

		// Token: 0x04004060 RID: 16480
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002DF3")]
		public int m_IconID;

		// Token: 0x04004061 RID: 16481
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002DF4")]
		public LoginBonusPresent[] m_Presents;
	}
}
