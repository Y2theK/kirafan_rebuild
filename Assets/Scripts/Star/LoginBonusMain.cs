﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007C4 RID: 1988
	[Token(Token = "0x20005E9")]
	[StructLayout(3)]
	public class LoginBonusMain : GameStateMain
	{
		// Token: 0x06001E97 RID: 7831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C0F")]
		[Address(RVA = "0x101230A3C", Offset = "0x1230A3C", VA = "0x101230A3C")]
		private void Start()
		{
		}

		// Token: 0x06001E98 RID: 7832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C10")]
		[Address(RVA = "0x101230A48", Offset = "0x1230A48", VA = "0x101230A48", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001E99 RID: 7833 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001C11")]
		[Address(RVA = "0x101230A50", Offset = "0x1230A50", VA = "0x101230A50", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001E9A RID: 7834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C12")]
		[Address(RVA = "0x101230B08", Offset = "0x1230B08", VA = "0x101230B08")]
		public LoginBonusMain()
		{
		}

		// Token: 0x04002ECD RID: 11981
		[Token(Token = "0x400243E")]
		public const int STATE_MAIN = 0;
	}
}
