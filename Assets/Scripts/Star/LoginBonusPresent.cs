﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AE0 RID: 2784
	[Token(Token = "0x200079E")]
	[StructLayout(3)]
	public class LoginBonusPresent
	{
		// Token: 0x060030FD RID: 12541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CC6")]
		[Address(RVA = "0x101230B10", Offset = "0x1230B10", VA = "0x101230B10")]
		public LoginBonusPresent()
		{
		}

		// Token: 0x04004064 RID: 16484
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002DF7")]
		public ePresentType m_PresentType;

		// Token: 0x04004065 RID: 16485
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002DF8")]
		public int m_PresentID;

		// Token: 0x04004066 RID: 16486
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002DF9")]
		public int m_Amount;
	}
}
