﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007C5 RID: 1989
	[Token(Token = "0x20005EA")]
	[StructLayout(3)]
	public class LoginBonusState : GameStateBase
	{
		// Token: 0x06001E9B RID: 7835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C13")]
		[Address(RVA = "0x101230B18", Offset = "0x1230B18", VA = "0x101230B18")]
		public LoginBonusState(LoginBonusMain owner)
		{
		}

		// Token: 0x06001E9C RID: 7836 RVA: 0x0000D998 File Offset: 0x0000BB98
		[Token(Token = "0x6001C14")]
		[Address(RVA = "0x101230B44", Offset = "0x1230B44", VA = "0x101230B44", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001E9D RID: 7837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C15")]
		[Address(RVA = "0x101230B4C", Offset = "0x1230B4C", VA = "0x101230B4C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001E9E RID: 7838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C16")]
		[Address(RVA = "0x101230B50", Offset = "0x1230B50", VA = "0x101230B50", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001E9F RID: 7839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C17")]
		[Address(RVA = "0x101230B54", Offset = "0x1230B54", VA = "0x101230B54", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EA0 RID: 7840 RVA: 0x0000D9B0 File Offset: 0x0000BBB0
		[Token(Token = "0x6001C18")]
		[Address(RVA = "0x101230B58", Offset = "0x1230B58", VA = "0x101230B58", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EA1 RID: 7841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C19")]
		[Address(RVA = "0x101230B60", Offset = "0x1230B60", VA = "0x101230B60", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002ECE RID: 11982
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400243F")]
		protected LoginBonusMain m_Owner;
	}
}
