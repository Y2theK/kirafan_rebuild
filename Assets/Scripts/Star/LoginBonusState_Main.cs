﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.LoginBonus;

namespace Star
{
	// Token: 0x020007C6 RID: 1990
	[Token(Token = "0x20005EB")]
	[StructLayout(3)]
	public class LoginBonusState_Main : LoginBonusState
	{
		// Token: 0x06001EA2 RID: 7842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C1A")]
		[Address(RVA = "0x101230AD0", Offset = "0x1230AD0", VA = "0x101230AD0")]
		public LoginBonusState_Main(LoginBonusMain owner)
		{
		}

		// Token: 0x06001EA3 RID: 7843 RVA: 0x0000D9C8 File Offset: 0x0000BBC8
		[Token(Token = "0x6001C1B")]
		[Address(RVA = "0x101230B64", Offset = "0x1230B64", VA = "0x101230B64", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EA4 RID: 7844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C1C")]
		[Address(RVA = "0x101230B6C", Offset = "0x1230B6C", VA = "0x101230B6C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EA5 RID: 7845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C1D")]
		[Address(RVA = "0x101230B74", Offset = "0x1230B74", VA = "0x101230B74", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EA6 RID: 7846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C1E")]
		[Address(RVA = "0x101230B78", Offset = "0x1230B78", VA = "0x101230B78", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EA7 RID: 7847 RVA: 0x0000D9E0 File Offset: 0x0000BBE0
		[Token(Token = "0x6001C1F")]
		[Address(RVA = "0x101230B80", Offset = "0x1230B80", VA = "0x101230B80", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EA8 RID: 7848 RVA: 0x0000D9F8 File Offset: 0x0000BBF8
		[Token(Token = "0x6001C20")]
		[Address(RVA = "0x101230E08", Offset = "0x1230E08", VA = "0x101230E08")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x04002ECF RID: 11983
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002440")]
		private LoginBonusState_Main.eStep m_Step;

		// Token: 0x04002ED0 RID: 11984
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002441")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002ED1 RID: 11985
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002442")]
		private LoginBonusUI m_UI;

		// Token: 0x020007C7 RID: 1991
		[Token(Token = "0x2000EAC")]
		private enum eStep
		{
			// Token: 0x04002ED3 RID: 11987
			[Token(Token = "0x4005E06")]
			None = -1,
			// Token: 0x04002ED4 RID: 11988
			[Token(Token = "0x4005E07")]
			First,
			// Token: 0x04002ED5 RID: 11989
			[Token(Token = "0x4005E08")]
			LoadWait,
			// Token: 0x04002ED6 RID: 11990
			[Token(Token = "0x4005E09")]
			PlayIn,
			// Token: 0x04002ED7 RID: 11991
			[Token(Token = "0x4005E0A")]
			Main,
			// Token: 0x04002ED8 RID: 11992
			[Token(Token = "0x4005E0B")]
			UnloadChildScene,
			// Token: 0x04002ED9 RID: 11993
			[Token(Token = "0x4005E0C")]
			UnloadChildSceneWait
		}
	}
}
