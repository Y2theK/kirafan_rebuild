﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000ADF RID: 2783
	[Token(Token = "0x200079D")]
	[StructLayout(3)]
	public class LoginBonusTotal
	{
		// Token: 0x060030FC RID: 12540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CC5")]
		[Address(RVA = "0x101230EF0", Offset = "0x1230EF0", VA = "0x101230EF0")]
		public LoginBonusTotal()
		{
		}

		// Token: 0x04004062 RID: 16482
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002DF5")]
		public int m_Day;

		// Token: 0x04004063 RID: 16483
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002DF6")]
		public LoginBonusPresent[] m_Presents;
	}
}
