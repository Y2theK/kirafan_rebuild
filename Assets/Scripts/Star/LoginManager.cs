﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000AE2 RID: 2786
	[Token(Token = "0x20007A0")]
	[StructLayout(3)]
	public sealed class LoginManager
	{
		// Token: 0x14000051 RID: 81
		// (add) Token: 0x060030FF RID: 12543 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003100 RID: 12544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000051")]
		private event Action<MeigewwwParam, Login> m_OnResponse_Login
		{
			[Token(Token = "0x6002CC8")]
			[Address(RVA = "0x101230EF8", Offset = "0x1230EF8", VA = "0x101230EF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CC9")]
			[Address(RVA = "0x101231004", Offset = "0x1231004", VA = "0x101231004")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000052 RID: 82
		// (add) Token: 0x06003101 RID: 12545 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003102 RID: 12546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000052")]
		private event Action<MeigewwwParam, Getall> m_OnResponse_GetAll
		{
			[Token(Token = "0x6002CCA")]
			[Address(RVA = "0x101231110", Offset = "0x1231110", VA = "0x101231110")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CCB")]
			[Address(RVA = "0x10123121C", Offset = "0x123121C", VA = "0x10123121C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000053 RID: 83
		// (add) Token: 0x06003103 RID: 12547 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003104 RID: 12548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000053")]
		private event Action<MeigewwwParam, Signup> m_OnResponse_Signup
		{
			[Token(Token = "0x6002CCC")]
			[Address(RVA = "0x101231328", Offset = "0x1231328", VA = "0x101231328")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CCD")]
			[Address(RVA = "0x101231434", Offset = "0x1231434", VA = "0x101231434")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000054 RID: 84
		// (add) Token: 0x06003105 RID: 12549 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003106 RID: 12550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000054")]
		private event Action<bool> m_OnResponse_PlayerMoveGet
		{
			[Token(Token = "0x6002CCE")]
			[Address(RVA = "0x101231540", Offset = "0x1231540", VA = "0x101231540")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CCF")]
			[Address(RVA = "0x10123164C", Offset = "0x123164C", VA = "0x10123164C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000055 RID: 85
		// (add) Token: 0x06003107 RID: 12551 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003108 RID: 12552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000055")]
		private event Action<string> m_OnResponse_PlayerMoveSet
		{
			[Token(Token = "0x6002CD0")]
			[Address(RVA = "0x101231758", Offset = "0x1231758", VA = "0x101231758")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CD1")]
			[Address(RVA = "0x101231864", Offset = "0x1231864", VA = "0x101231864")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000056 RID: 86
		// (add) Token: 0x06003109 RID: 12553 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600310A RID: 12554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000056")]
		private event Action<MeigewwwParam, Reset> m_OnResponse_PlayerReset
		{
			[Token(Token = "0x6002CD2")]
			[Address(RVA = "0x101231970", Offset = "0x1231970", VA = "0x101231970")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CD3")]
			[Address(RVA = "0x101231A7C", Offset = "0x1231A7C", VA = "0x101231A7C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000057 RID: 87
		// (add) Token: 0x0600310B RID: 12555 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600310C RID: 12556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000057")]
		private event Action m_OnResponse_LoginBonus
		{
			[Token(Token = "0x6002CD4")]
			[Address(RVA = "0x101231B88", Offset = "0x1231B88", VA = "0x101231B88")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CD5")]
			[Address(RVA = "0x101231C94", Offset = "0x1231C94", VA = "0x101231C94")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x0600310D RID: 12557 RVA: 0x00015090 File Offset: 0x00013290
		[Token(Token = "0x17000386")]
		public bool IsLogined
		{
			[Token(Token = "0x6002CD6")]
			[Address(RVA = "0x101231DA0", Offset = "0x1231DA0", VA = "0x101231DA0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x0600310E RID: 12558 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000387")]
		public string MoveCode
		{
			[Token(Token = "0x6002CD7")]
			[Address(RVA = "0x101231DA8", Offset = "0x1231DA8", VA = "0x101231DA8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x0600310F RID: 12559 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000388")]
		public string MovePassward
		{
			[Token(Token = "0x6002CD8")]
			[Address(RVA = "0x101231DB0", Offset = "0x1231DB0", VA = "0x101231DB0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06003110 RID: 12560 RVA: 0x000150A8 File Offset: 0x000132A8
		[Token(Token = "0x17000389")]
		public DateTime MoveDeadLine
		{
			[Token(Token = "0x6002CD9")]
			[Address(RVA = "0x101231DB8", Offset = "0x1231DB8", VA = "0x101231DB8")]
			get
			{
				return default(DateTime);
			}
		}

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06003111 RID: 12561 RVA: 0x000150C0 File Offset: 0x000132C0
		[Token(Token = "0x1700038A")]
		public long AcquiredSupportKRRPoint
		{
			[Token(Token = "0x6002CDA")]
			[Address(RVA = "0x101231DC0", Offset = "0x1231DC0", VA = "0x101231DC0")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06003112 RID: 12562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CDB")]
		[Address(RVA = "0x101231DC8", Offset = "0x1231DC8", VA = "0x101231DC8")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06003113 RID: 12563 RVA: 0x000150D8 File Offset: 0x000132D8
		[Token(Token = "0x6002CDC")]
		[Address(RVA = "0x101231DF4", Offset = "0x1231DF4", VA = "0x101231DF4")]
		public bool IsAvailableAcquiredSupportKRRPoint()
		{
			return default(bool);
		}

		// Token: 0x06003114 RID: 12564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CDD")]
		[Address(RVA = "0x101231DE4", Offset = "0x1231DE4", VA = "0x101231DE4")]
		public void ResetAcquiredSupportKRRPoint()
		{
		}

		// Token: 0x06003115 RID: 12565 RVA: 0x000150F0 File Offset: 0x000132F0
		[Token(Token = "0x6002CDE")]
		[Address(RVA = "0x101231E04", Offset = "0x1231E04", VA = "0x101231E04")]
		public bool Request_Login(bool authVoided, Action<MeigewwwParam, Login> onResponse_Login, Action<MeigewwwParam, Getall> onResponse_GetAll)
		{
			return default(bool);
		}

		// Token: 0x06003116 RID: 12566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CDF")]
		[Address(RVA = "0x101232030", Offset = "0x1232030", VA = "0x101232030")]
		private void OnResponse_Login(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003117 RID: 12567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE0")]
		[Address(RVA = "0x101232180", Offset = "0x1232180", VA = "0x101232180")]
		public void Request_GetAll()
		{
		}

		// Token: 0x06003118 RID: 12568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE1")]
		[Address(RVA = "0x101232264", Offset = "0x1232264", VA = "0x101232264")]
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003119 RID: 12569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE2")]
		[Address(RVA = "0x101232FB0", Offset = "0x1232FB0", VA = "0x101232FB0")]
		public void Request_Signup(string userName, string preCode, Action<MeigewwwParam, Signup> onResponse)
		{
		}

		// Token: 0x0600311A RID: 12570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE3")]
		[Address(RVA = "0x101233188", Offset = "0x1233188", VA = "0x101233188")]
		private void OnResponse_Signup(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600311B RID: 12571 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CE4")]
		[Address(RVA = "0x101233304", Offset = "0x1233304", VA = "0x101233304")]
		public FriendManager.FriendData GetQuestFriendDataForRestart()
		{
			return null;
		}

		// Token: 0x0600311C RID: 12572 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CE5")]
		[Address(RVA = "0x10123330C", Offset = "0x123330C", VA = "0x10123330C")]
		public UserSupportData GetQuestUserSupportDataForRestart()
		{
			return null;
		}

		// Token: 0x0600311D RID: 12573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE6")]
		[Address(RVA = "0x101233314", Offset = "0x1233314", VA = "0x101233314")]
		public void Request_PlayerMoveGet(string movePassward, Action<bool> onResponse)
		{
		}

		// Token: 0x0600311E RID: 12574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE7")]
		[Address(RVA = "0x101233418", Offset = "0x1233418", VA = "0x101233418")]
		private void OnResponse_PlayerMoveGet(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600311F RID: 12575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE8")]
		[Address(RVA = "0x1012334E0", Offset = "0x12334E0", VA = "0x1012334E0")]
		public void Request_PlayerMoveSet(string moveCode, string movePassward, Action<string> onResponse)
		{
		}

		// Token: 0x06003120 RID: 12576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CE9")]
		[Address(RVA = "0x101233654", Offset = "0x1233654", VA = "0x101233654")]
		private void OnResponse_PlayerMoveSet(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003121 RID: 12577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CEA")]
		[Address(RVA = "0x1012338D4", Offset = "0x12338D4", VA = "0x1012338D4")]
		public void Request_PlayerReset(Action<MeigewwwParam, Reset> onResponse)
		{
		}

		// Token: 0x06003122 RID: 12578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CEB")]
		[Address(RVA = "0x101233A38", Offset = "0x1233A38", VA = "0x101233A38")]
		private void OnResponse_PlayerReset(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003123 RID: 12579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CEC")]
		[Address(RVA = "0x101233BD4", Offset = "0x1233BD4", VA = "0x101233BD4")]
		public void Request_LoginBonus(Action onResponse, bool isConnectingIcon = true)
		{
		}

		// Token: 0x06003124 RID: 12580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CED")]
		[Address(RVA = "0x101233CD8", Offset = "0x1233CD8", VA = "0x101233CD8")]
		private void OnResponse_LoginBonus(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003125 RID: 12581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CEE")]
		[Address(RVA = "0x10123465C", Offset = "0x123465C", VA = "0x10123465C")]
		private static void wwwConvert(Bonus src, LoginBonus dest)
		{
		}

		// Token: 0x06003126 RID: 12582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CEF")]
		[Address(RVA = "0x101234ADC", Offset = "0x1234ADC", VA = "0x101234ADC")]
		private static void wwwConvert(Bonus src, LoginBonusTotal dest)
		{
		}

		// Token: 0x06003127 RID: 12583 RVA: 0x00015108 File Offset: 0x00013308
		[Token(Token = "0x6002CF0")]
		[Address(RVA = "0x101234E00", Offset = "0x1234E00", VA = "0x101234E00")]
		public bool IsLoginBonusCall()
		{
			return default(bool);
		}

		// Token: 0x06003128 RID: 12584 RVA: 0x00015120 File Offset: 0x00013320
		[Token(Token = "0x6002CF1")]
		[Address(RVA = "0x101234F04", Offset = "0x1234F04", VA = "0x101234F04")]
		public bool IsAvailableLoginBonus()
		{
			return default(bool);
		}

		// Token: 0x06003129 RID: 12585 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CF2")]
		[Address(RVA = "0x101234F2C", Offset = "0x1234F2C", VA = "0x101234F2C")]
		public LoginBonus GetLoginBonusNormal()
		{
			return null;
		}

		// Token: 0x0600312A RID: 12586 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CF3")]
		[Address(RVA = "0x101234F34", Offset = "0x1234F34", VA = "0x101234F34")]
		public LoginBonus[] GetLoginBonusEvents()
		{
			return null;
		}

		// Token: 0x0600312B RID: 12587 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CF4")]
		[Address(RVA = "0x101234F3C", Offset = "0x1234F3C", VA = "0x101234F3C")]
		public LoginBonusTotal GetLoginBonusTotal()
		{
			return null;
		}

		// Token: 0x0600312C RID: 12588 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002CF5")]
		[Address(RVA = "0x101234F44", Offset = "0x1234F44", VA = "0x101234F44")]
		public LoginPremiumPresent[] GetLoginPremiumPresents()
		{
			return null;
		}

		// Token: 0x0600312D RID: 12589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CF6")]
		[Address(RVA = "0x101231DEC", Offset = "0x1231DEC", VA = "0x101231DEC")]
		public void ResetLoginPremiumPresents()
		{
		}

		// Token: 0x0600312E RID: 12590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CF7")]
		[Address(RVA = "0x101234F4C", Offset = "0x1234F4C", VA = "0x101234F4C")]
		public LoginManager()
		{
		}

		// Token: 0x04004070 RID: 16496
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E03")]
		private bool m_IsLogined;

		// Token: 0x04004071 RID: 16497
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E04")]
		private string m_WorkUUID;

		// Token: 0x04004072 RID: 16498
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E05")]
		private string m_MoveCode;

		// Token: 0x04004073 RID: 16499
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002E06")]
		private string m_MovePassward;

		// Token: 0x04004074 RID: 16500
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002E07")]
		private DateTime m_MoveDeadLine;

		// Token: 0x04004075 RID: 16501
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002E08")]
		private DateTime m_LoginBonusSuccessTime;

		// Token: 0x04004076 RID: 16502
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002E09")]
		private int m_LoginBonusSuccessCount;

		// Token: 0x04004077 RID: 16503
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002E0A")]
		private LoginBonus m_LoginBonusNormal;

		// Token: 0x04004078 RID: 16504
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002E0B")]
		private LoginBonus[] m_LoginBonusEvents;

		// Token: 0x04004079 RID: 16505
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002E0C")]
		private LoginBonusTotal m_LoginBonusTotal;

		// Token: 0x0400407A RID: 16506
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002E0D")]
		private LoginPremiumPresent[] m_LoginPremiumPresents;

		// Token: 0x0400407B RID: 16507
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002E0E")]
		private long m_AcquiredSupportKRRPoint;

		// Token: 0x0400407C RID: 16508
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002E0F")]
		private FriendManager.FriendData m_QuestFriendDataForRestart;

		// Token: 0x0400407D RID: 16509
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002E10")]
		private UserSupportData m_QuestUserSupportDataForRestart;
	}
}
