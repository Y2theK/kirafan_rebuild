﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AE1 RID: 2785
	[Token(Token = "0x200079F")]
	[StructLayout(3)]
	public class LoginPremiumPresent
	{
		// Token: 0x060030FE RID: 12542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CC7")]
		[Address(RVA = "0x101250458", Offset = "0x1250458", VA = "0x101250458")]
		public LoginPremiumPresent()
		{
		}

		// Token: 0x04004067 RID: 16487
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002DFA")]
		public string m_Name;

		// Token: 0x04004068 RID: 16488
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002DFB")]
		public RewardSet[] m_RewardSets;
	}
}
