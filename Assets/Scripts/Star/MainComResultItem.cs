﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200086B RID: 2155
	[Token(Token = "0x2000645")]
	[StructLayout(3)]
	public class MainComResultItem : IMainComCommand
	{
		// Token: 0x0600229A RID: 8858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FFA")]
		[Address(RVA = "0x101250460", Offset = "0x1250460", VA = "0x101250460")]
		public MainComResultItem(int fresultno, int faddnum)
		{
		}

		// Token: 0x0600229B RID: 8859 RVA: 0x0000EFD0 File Offset: 0x0000D1D0
		[Token(Token = "0x6001FFB")]
		[Address(RVA = "0x101250498", Offset = "0x1250498", VA = "0x101250498", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x040032DB RID: 13019
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025D0")]
		public int m_ResultNo;

		// Token: 0x040032DC RID: 13020
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40025D1")]
		public int m_AddNum;

		// Token: 0x0200086C RID: 2156
		// (Invoke) Token: 0x0600229D RID: 8861
		[Token(Token = "0x2000EF6")]
		private delegate void CallbackResult(MainComResultItem pup);
	}
}
