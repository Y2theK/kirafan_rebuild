﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200086D RID: 2157
	[Token(Token = "0x2000646")]
	[StructLayout(3)]
	public class MainComResultPlayerItem : IMainComCommand
	{
		// Token: 0x060022A0 RID: 8864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FFC")]
		[Address(RVA = "0x1012509F4", Offset = "0x12509F4", VA = "0x1012509F4")]
		public MainComResultPlayerItem(MainComResultPlayerItem.eCategory fcategory, long faddnum)
		{
		}

		// Token: 0x060022A1 RID: 8865 RVA: 0x0000EFE8 File Offset: 0x0000D1E8
		[Token(Token = "0x6001FFD")]
		[Address(RVA = "0x101250A30", Offset = "0x1250A30", VA = "0x101250A30", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x040032DD RID: 13021
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025D2")]
		public MainComResultPlayerItem.eCategory m_Category;

		// Token: 0x040032DE RID: 13022
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025D3")]
		public long m_AddNum;

		// Token: 0x0200086E RID: 2158
		[Token(Token = "0x2000EF7")]
		public enum eCategory
		{
			// Token: 0x040032E0 RID: 13024
			[Token(Token = "0x400607F")]
			Money,
			// Token: 0x040032E1 RID: 13025
			[Token(Token = "0x4006080")]
			KRRPoint,
			// Token: 0x040032E2 RID: 13026
			[Token(Token = "0x4006081")]
			Stamina
		}
	}
}
