﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x02000785 RID: 1925
	[Token(Token = "0x20005C5")]
	[StructLayout(3)]
	public class MainOfferAcceptEffectSequencer : OfferEffectSequencer
	{
		// Token: 0x06001D23 RID: 7459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA1")]
		[Address(RVA = "0x101250A38", Offset = "0x1250A38", VA = "0x101250A38")]
		public void Setup(ContentRoomState_Main ownerState, ContentRoomUI ui)
		{
		}

		// Token: 0x06001D24 RID: 7460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA2")]
		[Address(RVA = "0x101250A48", Offset = "0x1250A48", VA = "0x101250A48", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x06001D25 RID: 7461 RVA: 0x0000D050 File Offset: 0x0000B250
		[Token(Token = "0x6001AA3")]
		[Address(RVA = "0x101250A84", Offset = "0x1250A84", VA = "0x101250A84", Slot = "5")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x06001D26 RID: 7462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA4")]
		[Address(RVA = "0x101250DD0", Offset = "0x1250DD0", VA = "0x101250DD0")]
		private void OnCloseRewardDetailWindow()
		{
		}

		// Token: 0x06001D27 RID: 7463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA5")]
		[Address(RVA = "0x101250DDC", Offset = "0x1250DDC", VA = "0x101250DDC")]
		private void OnCompleteReplace(int id)
		{
		}

		// Token: 0x06001D28 RID: 7464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA6")]
		[Address(RVA = "0x101250DE8", Offset = "0x1250DE8", VA = "0x101250DE8")]
		public MainOfferAcceptEffectSequencer()
		{
		}

		// Token: 0x04002D53 RID: 11603
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400239A")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04002D54 RID: 11604
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400239B")]
		private ContentRoomUI m_UI;

		// Token: 0x04002D55 RID: 11605
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400239C")]
		private MainOfferAcceptEffectSequencer.eStep m_Step;

		// Token: 0x04002D56 RID: 11606
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400239D")]
		private bool m_ReplacedInterior;

		// Token: 0x04002D57 RID: 11607
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400239E")]
		private MainOfferStartCutInScene m_CutInScene;

		// Token: 0x02000786 RID: 1926
		[Token(Token = "0x2000E91")]
		private enum eStep
		{
			// Token: 0x04002D59 RID: 11609
			[Token(Token = "0x4005D30")]
			None,
			// Token: 0x04002D5A RID: 11610
			[Token(Token = "0x4005D31")]
			StartCloseUI,
			// Token: 0x04002D5B RID: 11611
			[Token(Token = "0x4005D32")]
			WaitCloseUI,
			// Token: 0x04002D5C RID: 11612
			[Token(Token = "0x4005D33")]
			PlayCutIn,
			// Token: 0x04002D5D RID: 11613
			[Token(Token = "0x4005D34")]
			WaitCutIn,
			// Token: 0x04002D5E RID: 11614
			[Token(Token = "0x4005D35")]
			ConvertRoomObject,
			// Token: 0x04002D5F RID: 11615
			[Token(Token = "0x4005D36")]
			StartOpenUI,
			// Token: 0x04002D60 RID: 11616
			[Token(Token = "0x4005D37")]
			WaitOpenUI,
			// Token: 0x04002D61 RID: 11617
			[Token(Token = "0x4005D38")]
			End
		}
	}
}
