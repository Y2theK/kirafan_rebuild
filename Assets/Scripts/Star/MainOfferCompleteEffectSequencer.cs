﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x02000787 RID: 1927
	[Token(Token = "0x20005C6")]
	[StructLayout(3)]
	public class MainOfferCompleteEffectSequencer : OfferEffectSequencer
	{
		// Token: 0x06001D29 RID: 7465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA7")]
		[Address(RVA = "0x101250DF0", Offset = "0x1250DF0", VA = "0x101250DF0")]
		public void Setup(ContentRoomState_Main ownerState, ContentRoomUI ui, long offerMngId)
		{
		}

		// Token: 0x06001D2A RID: 7466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AA8")]
		[Address(RVA = "0x101250E04", Offset = "0x1250E04", VA = "0x101250E04", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x06001D2B RID: 7467 RVA: 0x0000D068 File Offset: 0x0000B268
		[Token(Token = "0x6001AA9")]
		[Address(RVA = "0x101250E40", Offset = "0x1250E40", VA = "0x101250E40", Slot = "5")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AAA")]
		[Address(RVA = "0x101251384", Offset = "0x1251384", VA = "0x101251384")]
		private void OnCompleteReplace(int id)
		{
		}

		// Token: 0x06001D2D RID: 7469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AAB")]
		[Address(RVA = "0x101251390", Offset = "0x1251390", VA = "0x101251390")]
		private void OnCloseNoticeBonusCraft()
		{
		}

		// Token: 0x06001D2E RID: 7470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AAC")]
		[Address(RVA = "0x10125139C", Offset = "0x125139C", VA = "0x10125139C")]
		public MainOfferCompleteEffectSequencer()
		{
		}

		// Token: 0x04002D62 RID: 11618
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400239F")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04002D63 RID: 11619
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40023A0")]
		private ContentRoomUI m_UI;

		// Token: 0x04002D64 RID: 11620
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023A1")]
		private MainOfferCompleteEffectSequencer.eStep m_Step;

		// Token: 0x04002D65 RID: 11621
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023A2")]
		private long m_OfferMngId;

		// Token: 0x04002D66 RID: 11622
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023A3")]
		private bool m_ReplacedInterior;

		// Token: 0x04002D67 RID: 11623
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023A4")]
		private OfferCutInScene m_CutInScene;

		// Token: 0x02000788 RID: 1928
		[Token(Token = "0x2000E92")]
		private enum eStep
		{
			// Token: 0x04002D69 RID: 11625
			[Token(Token = "0x4005D3A")]
			None,
			// Token: 0x04002D6A RID: 11626
			[Token(Token = "0x4005D3B")]
			StartCloseUI,
			// Token: 0x04002D6B RID: 11627
			[Token(Token = "0x4005D3C")]
			WaitCloseUI,
			// Token: 0x04002D6C RID: 11628
			[Token(Token = "0x4005D3D")]
			InitCutIn,
			// Token: 0x04002D6D RID: 11629
			[Token(Token = "0x4005D3E")]
			PlayCutIn,
			// Token: 0x04002D6E RID: 11630
			[Token(Token = "0x4005D3F")]
			WaitCutIn,
			// Token: 0x04002D6F RID: 11631
			[Token(Token = "0x4005D40")]
			OpenRewardDetailWindow,
			// Token: 0x04002D70 RID: 11632
			[Token(Token = "0x4005D41")]
			WaitRewardDetailWindow,
			// Token: 0x04002D71 RID: 11633
			[Token(Token = "0x4005D42")]
			PlayRoomObjectCutIn,
			// Token: 0x04002D72 RID: 11634
			[Token(Token = "0x4005D43")]
			WaitRoomObjectCutIn,
			// Token: 0x04002D73 RID: 11635
			[Token(Token = "0x4005D44")]
			ConvertRoomObject,
			// Token: 0x04002D74 RID: 11636
			[Token(Token = "0x4005D45")]
			OpenNoticeBonusCraft,
			// Token: 0x04002D75 RID: 11637
			[Token(Token = "0x4005D46")]
			WaitNoticeBonusCraft,
			// Token: 0x04002D76 RID: 11638
			[Token(Token = "0x4005D47")]
			StartOpenUI,
			// Token: 0x04002D77 RID: 11639
			[Token(Token = "0x4005D48")]
			WaitOpenUI,
			// Token: 0x04002D78 RID: 11640
			[Token(Token = "0x4005D49")]
			PreEnd,
			// Token: 0x04002D79 RID: 11641
			[Token(Token = "0x4005D4A")]
			End
		}
	}
}
