﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008F5 RID: 2293
	[Token(Token = "0x2000696")]
	[StructLayout(3)]
	public sealed class MainOfferEndCutInScene : OfferCutInScene
	{
		// Token: 0x0600259C RID: 9628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B8")]
		[Address(RVA = "0x101251324", Offset = "0x1251324", VA = "0x101251324")]
		public MainOfferEndCutInScene()
		{
		}

		// Token: 0x0600259D RID: 9629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022B9")]
		[Address(RVA = "0x1012513AC", Offset = "0x12513AC", VA = "0x1012513AC", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x0600259E RID: 9630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BA")]
		[Address(RVA = "0x101251474", Offset = "0x1251474", VA = "0x101251474", Slot = "5")]
		public override void Play()
		{
		}

		// Token: 0x0600259F RID: 9631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BB")]
		[Address(RVA = "0x10125153C", Offset = "0x125153C", VA = "0x10125153C", Slot = "6")]
		protected override void OnSkip()
		{
		}

		// Token: 0x060025A0 RID: 9632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BC")]
		[Address(RVA = "0x1012513D4", Offset = "0x12513D4", VA = "0x1012513D4")]
		private void DestroySound()
		{
		}

		// Token: 0x040035D1 RID: 13777
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400277F")]
		private SoundHandler m_SoundHndl;
	}
}
