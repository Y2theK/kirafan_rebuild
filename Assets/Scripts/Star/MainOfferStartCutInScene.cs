﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008F6 RID: 2294
	[Token(Token = "0x2000697")]
	[StructLayout(3)]
	public sealed class MainOfferStartCutInScene : OfferCutInScene
	{
		// Token: 0x060025A1 RID: 9633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BD")]
		[Address(RVA = "0x101250D70", Offset = "0x1250D70", VA = "0x101250D70")]
		public MainOfferStartCutInScene()
		{
		}

		// Token: 0x060025A2 RID: 9634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BE")]
		[Address(RVA = "0x101251564", Offset = "0x1251564", VA = "0x101251564", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x060025A3 RID: 9635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022BF")]
		[Address(RVA = "0x10125162C", Offset = "0x125162C", VA = "0x10125162C", Slot = "5")]
		public override void Play()
		{
		}

		// Token: 0x060025A4 RID: 9636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C0")]
		[Address(RVA = "0x1012516F4", Offset = "0x12516F4", VA = "0x1012516F4", Slot = "6")]
		protected override void OnSkip()
		{
		}

		// Token: 0x060025A5 RID: 9637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C1")]
		[Address(RVA = "0x10125158C", Offset = "0x125158C", VA = "0x10125158C")]
		private void DestroySound()
		{
		}

		// Token: 0x040035D2 RID: 13778
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002780")]
		private SoundHandler m_SoundHndl;
	}
}
