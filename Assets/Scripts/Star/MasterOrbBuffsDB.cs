﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000557 RID: 1367
	[Token(Token = "0x200044A")]
	[StructLayout(3)]
	public class MasterOrbBuffsDB : ScriptableObject
	{
		// Token: 0x060015D8 RID: 5592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001488")]
		[Address(RVA = "0x10125171C", Offset = "0x125171C", VA = "0x10125171C")]
		public MasterOrbBuffsDB()
		{
		}

		// Token: 0x040018EA RID: 6378
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001254")]
		public MasterOrbBuffsDB_Param[] m_Params;
	}
}
