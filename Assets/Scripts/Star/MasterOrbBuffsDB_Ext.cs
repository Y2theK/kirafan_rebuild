﻿namespace Star {
    public static class MasterOrbBuffsDB_Ext {
        public static MasterOrbBuffsDB_Param? GetParam(this MasterOrbBuffsDB self, int id, int lv) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == id && self.m_Params[i].m_Lv == lv) {
                    return self.m_Params[i];
                }
            }
            return null;
        }
    }
}
