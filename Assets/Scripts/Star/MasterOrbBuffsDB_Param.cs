﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000556 RID: 1366
	[Token(Token = "0x2000449")]
	[Serializable]
	[StructLayout(0, Size = 36)]
	public struct MasterOrbBuffsDB_Param
	{
		// Token: 0x040018E1 RID: 6369
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400124B")]
		public int m_ID;

		// Token: 0x040018E2 RID: 6370
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400124C")]
		public int m_Lv;

		// Token: 0x040018E3 RID: 6371
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400124D")]
		public float m_CorrectHp;

		// Token: 0x040018E4 RID: 6372
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400124E")]
		public float m_CorrectAtk;

		// Token: 0x040018E5 RID: 6373
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400124F")]
		public float m_CorrectMgc;

		// Token: 0x040018E6 RID: 6374
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001250")]
		public float m_CorrectDef;

		// Token: 0x040018E7 RID: 6375
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001251")]
		public float m_CorrectMDef;

		// Token: 0x040018E8 RID: 6376
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001252")]
		public float m_CorrectSpd;

		// Token: 0x040018E9 RID: 6377
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001253")]
		public float m_CorrectLuck;
	}
}
