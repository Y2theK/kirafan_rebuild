﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000559 RID: 1369
	[Token(Token = "0x200044C")]
	[StructLayout(3)]
	public class MasterOrbListDB : ScriptableObject
	{
		// Token: 0x060015D9 RID: 5593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001489")]
		[Address(RVA = "0x1012518CC", Offset = "0x12518CC", VA = "0x1012518CC")]
		public MasterOrbListDB()
		{
		}

		// Token: 0x040018F4 RID: 6388
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400125E")]
		public MasterOrbListDB_Param[] m_Params;
	}
}
