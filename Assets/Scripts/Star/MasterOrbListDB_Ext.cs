﻿namespace Star {
    public static class MasterOrbListDB_Ext {
        public static eMasterOrbType GetMasterOrbType(this MasterOrbListDB_Param self) {
            return self.m_TitleType != -1 ? eMasterOrbType.ContentOrb : eMasterOrbType.ClassOrb;
        }

        public static eMasterOrbSkillUseType GetMasterOrbSkillUseType(this MasterOrbListDB_Param self) {
            return self.m_TitleType != -1 ? eMasterOrbSkillUseType.OnceForEach : eMasterOrbSkillUseType.OnceInTotal;
        }

        public static MasterOrbListDB_Param GetParam(this MasterOrbListDB self, int orbID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == orbID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static MasterOrbListDB_Param? GetParamNullable(this MasterOrbListDB self, int orbID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == orbID) {
                    return self.m_Params[i];
                }
            }
            return null;
        }

        public static int GetAvailableSkillIndex(this MasterOrbListDB self, int orbID, int orbLv) {
            MasterOrbListDB_Param? param = self.GetParamNullable(orbID);
            if (param.HasValue) {
                for (int i = 0; i < param.Value.m_SkillAvailableLvs.Length; i++) {
                    if (param.Value.m_SkillAvailableLvs[i] == orbLv) {
                        return i;
                    }
                }
            }
            return -1;
        }
    }
}
