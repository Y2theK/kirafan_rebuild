﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000558 RID: 1368
	[Token(Token = "0x200044B")]
	[Serializable]
	[StructLayout(0)]
	public struct MasterOrbListDB_Param
	{
		// Token: 0x040018EB RID: 6379
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001255")]
		public int m_ID;

		// Token: 0x040018EC RID: 6380
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001256")]
		public string m_Name;

		// Token: 0x040018ED RID: 6381
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001257")]
		public int m_Class;

		// Token: 0x040018EE RID: 6382
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001258")]
		public int m_TitleType;

		// Token: 0x040018EF RID: 6383
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001259")]
		public int m_LimitLv;

		// Token: 0x040018F0 RID: 6384
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400125A")]
		public int[] m_SkillIDs;

		// Token: 0x040018F1 RID: 6385
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400125B")]
		public int[] m_SkillAvailableLvs;

		// Token: 0x040018F2 RID: 6386
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400125C")]
		public string[] m_CueNames;

		// Token: 0x040018F3 RID: 6387
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400125D")]
		public int m_OrbBuffID;
	}
}
