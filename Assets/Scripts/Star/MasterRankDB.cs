﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200055D RID: 1373
	[Token(Token = "0x2000450")]
	[StructLayout(3)]
	public class MasterRankDB : ScriptableObject
	{
		// Token: 0x060015DA RID: 5594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148A")]
		[Address(RVA = "0x101251CA0", Offset = "0x1251CA0", VA = "0x101251CA0")]
		public MasterRankDB()
		{
		}

		// Token: 0x04001904 RID: 6404
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400126E")]
		public MasterRankDB_Param[] m_Params;
	}
}
