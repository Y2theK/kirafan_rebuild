﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000515 RID: 1301
	[Token(Token = "0x200040B")]
	[StructLayout(3)]
	public static class MasterRankDB_Ext
	{
		// Token: 0x06001560 RID: 5472 RVA: 0x00009300 File Offset: 0x00007500
		[Token(Token = "0x6001415")]
		[Address(RVA = "0x101251CA8", Offset = "0x1251CA8", VA = "0x101251CA8")]
		public static MasterRankDB_Param GetParam(this MasterRankDB self, int currentRank)
		{
			return default(MasterRankDB_Param);
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x00009318 File Offset: 0x00007518
		[Token(Token = "0x6001416")]
		[Address(RVA = "0x101251D64", Offset = "0x1251D64", VA = "0x101251D64")]
		public static int GetNextExp(this MasterRankDB self, int currentRank)
		{
			return 0;
		}

		// Token: 0x06001562 RID: 5474 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001417")]
		[Address(RVA = "0x101251E04", Offset = "0x1251E04", VA = "0x101251E04")]
		public static long[] GetNextMaxExps(this MasterRankDB self, int rank_a, int rank_b)
		{
			return null;
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x00009330 File Offset: 0x00007530
		[Token(Token = "0x6001418")]
		[Address(RVA = "0x101251EF4", Offset = "0x1251EF4", VA = "0x101251EF4")]
		public static long GetCurrentExp(this MasterRankDB self, int currentLv, long exp)
		{
			return 0L;
		}
	}
}
