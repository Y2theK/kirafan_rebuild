﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200055C RID: 1372
	[Token(Token = "0x200044F")]
	[Serializable]
	[StructLayout(0, Size = 36)]
	public struct MasterRankDB_Param
	{
		// Token: 0x040018FB RID: 6395
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001265")]
		public int m_Rank;

		// Token: 0x040018FC RID: 6396
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001266")]
		public int m_NextExp;

		// Token: 0x040018FD RID: 6397
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001267")]
		public int m_Stamina;

		// Token: 0x040018FE RID: 6398
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001268")]
		public int m_FriendLimit;

		// Token: 0x040018FF RID: 6399
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001269")]
		public int m_SupportLimit;

		// Token: 0x04001900 RID: 6400
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400126A")]
		public int m_BattlePartyCost;

		// Token: 0x04001901 RID: 6401
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400126B")]
		public int m_WeaponLimit;

		// Token: 0x04001902 RID: 6402
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400126C")]
		public int m_TrainingSlotNum;

		// Token: 0x04001903 RID: 6403
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400126D")]
		public int m_StoreReview;
	}
}
