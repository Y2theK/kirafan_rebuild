﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200071B RID: 1819
	[Token(Token = "0x200059E")]
	[StructLayout(3)]
	public class McatFileHelper : MonoBehaviour
	{
		// Token: 0x06001A97 RID: 6807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001887")]
		[Address(RVA = "0x101251F64", Offset = "0x1251F64", VA = "0x101251F64")]
		public McatFileHelper()
		{
		}

		// Token: 0x04002AB4 RID: 10932
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022BF")]
		[SerializeField]
		public McatFileHelper.MabFileState[] m_Table;

		// Token: 0x0200071C RID: 1820
		[Token(Token = "0x2000E4F")]
		[Serializable]
		public struct MabFileState
		{
			// Token: 0x04002AB5 RID: 10933
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005B6E")]
			public GameObject m_MabFile;

			// Token: 0x04002AB6 RID: 10934
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005B6F")]
			public int m_AccessKey;

			// Token: 0x04002AB7 RID: 10935
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B70")]
			public string m_AnimeName;
		}
	}
}
