﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007C8 RID: 1992
	[Token(Token = "0x20005EC")]
	[StructLayout(3)]
	public class MenuMain : GameStateMain
	{
		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06001EA9 RID: 7849 RVA: 0x0000DA10 File Offset: 0x0000BC10
		// (set) Token: 0x06001EAA RID: 7850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700021F")]
		public MenuLibraryWordUI.eMode LibraryStartMode
		{
			[Token(Token = "0x6001C21")]
			[Address(RVA = "0x101251F6C", Offset = "0x1251F6C", VA = "0x101251F6C")]
			[CompilerGenerated]
			get
			{
				return MenuLibraryWordUI.eMode.ContentTitle;
			}
			[Token(Token = "0x6001C22")]
			[Address(RVA = "0x101251F74", Offset = "0x1251F74", VA = "0x101251F74")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06001EAB RID: 7851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C23")]
		[Address(RVA = "0x101251F7C", Offset = "0x1251F7C", VA = "0x101251F7C")]
		private void Start()
		{
		}

		// Token: 0x06001EAC RID: 7852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C24")]
		[Address(RVA = "0x101251FB4", Offset = "0x1251FB4", VA = "0x101251FB4")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001EAD RID: 7853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C25")]
		[Address(RVA = "0x101251FC4", Offset = "0x1251FC4", VA = "0x101251FC4", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001EAE RID: 7854 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001C26")]
		[Address(RVA = "0x101251FCC", Offset = "0x1251FCC", VA = "0x101251FCC", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001EAF RID: 7855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C27")]
		[Address(RVA = "0x101252650", Offset = "0x1252650", VA = "0x101252650", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001EB0 RID: 7856 RVA: 0x0000DA28 File Offset: 0x0000BC28
		[Token(Token = "0x6001C28")]
		[Address(RVA = "0x1012526F4", Offset = "0x12526F4", VA = "0x1012526F4")]
		public MenuMain.eMenuMainInstanceState GetInstState()
		{
			return MenuMain.eMenuMainInstanceState.Invalid;
		}

		// Token: 0x06001EB1 RID: 7857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C29")]
		[Address(RVA = "0x1012526FC", Offset = "0x12526FC", VA = "0x1012526FC")]
		public void SetLaunchChildScene(MenuMain.eLaunchChildScene launchScene)
		{
		}

		// Token: 0x06001EB2 RID: 7858 RVA: 0x0000DA40 File Offset: 0x0000BC40
		[Token(Token = "0x6001C2A")]
		[Address(RVA = "0x101252828", Offset = "0x1252828", VA = "0x101252828")]
		public MenuMain.eLaunchChildScene GetLaunchChildScene()
		{
			return MenuMain.eLaunchChildScene.Invalid;
		}

		// Token: 0x06001EB3 RID: 7859 RVA: 0x0000DA58 File Offset: 0x0000BC58
		[Token(Token = "0x6001C2B")]
		[Address(RVA = "0x1012528D8", Offset = "0x12528D8", VA = "0x1012528D8", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001EB4 RID: 7860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C2C")]
		[Address(RVA = "0x1012528E0", Offset = "0x12528E0", VA = "0x1012528E0")]
		public MenuMain()
		{
		}

		// Token: 0x04002EDA RID: 11994
		[Token(Token = "0x4002443")]
		public const int STATE_INIT = 0;

		// Token: 0x04002EDB RID: 11995
		[Token(Token = "0x4002444")]
		public const int STATE_MENU_TOP = 1;

		// Token: 0x04002EDC RID: 11996
		[Token(Token = "0x4002445")]
		public const int STATE_MENU_USERINFO = 2;

		// Token: 0x04002EDD RID: 11997
		[Token(Token = "0x4002446")]
		public const int STATE_MENU_FRIEND = 3;

		// Token: 0x04002EDE RID: 11998
		[Token(Token = "0x4002447")]
		public const int STATE_MENU_LIBRARY = 4;

		// Token: 0x04002EDF RID: 11999
		[Token(Token = "0x4002448")]
		public const int STATE_MENU_HELP = 5;

		// Token: 0x04002EE0 RID: 12000
		[Token(Token = "0x4002449")]
		public const int STATE_MENU_OPTION = 6;

		// Token: 0x04002EE1 RID: 12001
		[Token(Token = "0x400244A")]
		public const int STATE_MENU_NOTIFICATION = 7;

		// Token: 0x04002EE2 RID: 12002
		[Token(Token = "0x400244B")]
		public const int STATE_MENU_OFFICIALSITE = 8;

		// Token: 0x04002EE3 RID: 12003
		[Token(Token = "0x400244C")]
		public const int STATE_MENU_TWITTER = 9;

		// Token: 0x04002EE4 RID: 12004
		[Token(Token = "0x400244D")]
		public const int STATE_MENU_TITLE = 11;

		// Token: 0x04002EE5 RID: 12005
		[Token(Token = "0x400244E")]
		public const int STATE_MENU_LIBRARY_TITLE = 12;

		// Token: 0x04002EE6 RID: 12006
		[Token(Token = "0x400244F")]
		public const int STATE_MENU_LIBRARY_EVENT = 13;

		// Token: 0x04002EE7 RID: 12007
		[Token(Token = "0x4002450")]
		public const int STATE_MENU_LIBRARY_ORIGINALCHARA = 14;

		// Token: 0x04002EE8 RID: 12008
		[Token(Token = "0x4002451")]
		public const int STATE_MENU_PLAYER_MOVE_CONFIGURATION = 15;

		// Token: 0x04002EE9 RID: 12009
		[Token(Token = "0x4002452")]
		public const int STATE_MENU_PURCHASE_HISTORY = 16;

		// Token: 0x04002EEA RID: 12010
		[Token(Token = "0x4002453")]
		public const int STATE_MENU_LIBRARY_OP_MOVIE = 17;

		// Token: 0x04002EEB RID: 12011
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002454")]
		public NPCCharaDisplayUI m_NpcUI;

		// Token: 0x04002EED RID: 12013
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002456")]
		private MenuMain.eMenuMainInstanceState m_InstState;

		// Token: 0x04002EEE RID: 12014
		[Token(Token = "0x4002457")]
		private static MenuMain.eLaunchChildScene? launchChildScene;

		// Token: 0x020007C9 RID: 1993
		[Token(Token = "0x2000EAD")]
		public enum eLaunchChildScene
		{
			// Token: 0x04002EF0 RID: 12016
			[Token(Token = "0x4005E0E")]
			Invalid,
			// Token: 0x04002EF1 RID: 12017
			[Token(Token = "0x4005E0F")]
			AllowChange,
			// Token: 0x04002EF2 RID: 12018
			[Token(Token = "0x4005E10")]
			Top,
			// Token: 0x04002EF3 RID: 12019
			[Token(Token = "0x4005E11")]
			Friend,
			// Token: 0x04002EF4 RID: 12020
			[Token(Token = "0x4005E12")]
			Library,
			// Token: 0x04002EF5 RID: 12021
			[Token(Token = "0x4005E13")]
			LibraryEvent
		}

		// Token: 0x020007CA RID: 1994
		[Token(Token = "0x2000EAE")]
		public enum eMenuMainInstanceState
		{
			// Token: 0x04002EF7 RID: 12023
			[Token(Token = "0x4005E15")]
			Invalid,
			// Token: 0x04002EF8 RID: 12024
			[Token(Token = "0x4005E16")]
			StartFunc,
			// Token: 0x04002EF9 RID: 12025
			[Token(Token = "0x4005E17")]
			EndStartFunc,
			// Token: 0x04002EFA RID: 12026
			[Token(Token = "0x4005E18")]
			FirstInitState,
			// Token: 0x04002EFB RID: 12027
			[Token(Token = "0x4005E19")]
			Idle,
			// Token: 0x04002EFC RID: 12028
			[Token(Token = "0x4005E1A")]
			CommonStateFinal,
			// Token: 0x04002EFD RID: 12029
			[Token(Token = "0x4005E1B")]
			EndCommonStateFinal,
			// Token: 0x04002EFE RID: 12030
			[Token(Token = "0x4005E1C")]
			DestroyFunc,
			// Token: 0x04002EFF RID: 12031
			[Token(Token = "0x4005E1D")]
			EndDestroyFunc
		}
	}
}
