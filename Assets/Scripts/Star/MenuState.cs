﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007CB RID: 1995
	[Token(Token = "0x20005ED")]
	[StructLayout(3)]
	public class MenuState : GameStateBase
	{
		// Token: 0x06001EB6 RID: 7862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C2E")]
		[Address(RVA = "0x1012528EC", Offset = "0x12528EC", VA = "0x1012528EC")]
		public MenuState(MenuMain owner)
		{
		}

		// Token: 0x06001EB7 RID: 7863 RVA: 0x0000DA70 File Offset: 0x0000BC70
		[Token(Token = "0x6001C2F")]
		[Address(RVA = "0x101252920", Offset = "0x1252920", VA = "0x101252920", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EB8 RID: 7864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C30")]
		[Address(RVA = "0x101252928", Offset = "0x1252928", VA = "0x101252928", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EB9 RID: 7865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C31")]
		[Address(RVA = "0x10125292C", Offset = "0x125292C", VA = "0x10125292C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EBA RID: 7866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C32")]
		[Address(RVA = "0x101252930", Offset = "0x1252930", VA = "0x101252930", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EBB RID: 7867 RVA: 0x0000DA88 File Offset: 0x0000BC88
		[Token(Token = "0x6001C33")]
		[Address(RVA = "0x101252934", Offset = "0x1252934", VA = "0x101252934", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EBC RID: 7868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C34")]
		[Address(RVA = "0x10125293C", Offset = "0x125293C", VA = "0x10125293C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002F00 RID: 12032
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002458")]
		protected MenuMain m_Owner;

		// Token: 0x04002F01 RID: 12033
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002459")]
		protected int m_NextState;
	}
}
