﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007CC RID: 1996
	[Token(Token = "0x20005EE")]
	[StructLayout(3)]
	public class MenuState_Friend : MenuState
	{
		// Token: 0x06001EBD RID: 7869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C35")]
		[Address(RVA = "0x1012523BC", Offset = "0x12523BC", VA = "0x1012523BC")]
		public MenuState_Friend(MenuMain owner)
		{
		}

		// Token: 0x06001EBE RID: 7870 RVA: 0x0000DAA0 File Offset: 0x0000BCA0
		[Token(Token = "0x6001C36")]
		[Address(RVA = "0x101252940", Offset = "0x1252940", VA = "0x101252940", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EBF RID: 7871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C37")]
		[Address(RVA = "0x101252948", Offset = "0x1252948", VA = "0x101252948", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EC0 RID: 7872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C38")]
		[Address(RVA = "0x101252950", Offset = "0x1252950", VA = "0x101252950", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EC1 RID: 7873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C39")]
		[Address(RVA = "0x101252954", Offset = "0x1252954", VA = "0x101252954", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EC2 RID: 7874 RVA: 0x0000DAB8 File Offset: 0x0000BCB8
		[Token(Token = "0x6001C3A")]
		[Address(RVA = "0x1012529DC", Offset = "0x12529DC", VA = "0x1012529DC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EC3 RID: 7875 RVA: 0x0000DAD0 File Offset: 0x0000BCD0
		[Token(Token = "0x6001C3B")]
		[Address(RVA = "0x101252CC4", Offset = "0x1252CC4", VA = "0x101252CC4")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001EC4 RID: 7876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C3C")]
		[Address(RVA = "0x101252F88", Offset = "0x1252F88", VA = "0x101252F88", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001EC5 RID: 7877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C3D")]
		[Address(RVA = "0x101253058", Offset = "0x1253058", VA = "0x101253058")]
		private void OnFriendRoomCallBack()
		{
		}

		// Token: 0x06001EC6 RID: 7878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C3E")]
		[Address(RVA = "0x101253094", Offset = "0x1253094", VA = "0x101253094")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001EC7 RID: 7879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C3F")]
		[Address(RVA = "0x101253024", Offset = "0x1253024", VA = "0x101253024")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F02 RID: 12034
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400245A")]
		private MenuState_Friend.eStep m_Step;

		// Token: 0x04002F03 RID: 12035
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400245B")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F04 RID: 12036
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400245C")]
		private MenuFriendUI m_UI;

		// Token: 0x04002F05 RID: 12037
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400245D")]
		private bool m_IsRoomInMode;

		// Token: 0x020007CD RID: 1997
		[Token(Token = "0x2000EAF")]
		private enum eStep
		{
			// Token: 0x04002F07 RID: 12039
			[Token(Token = "0x4005E1F")]
			None = -1,
			// Token: 0x04002F08 RID: 12040
			[Token(Token = "0x4005E20")]
			First,
			// Token: 0x04002F09 RID: 12041
			[Token(Token = "0x4005E21")]
			LoadWait,
			// Token: 0x04002F0A RID: 12042
			[Token(Token = "0x4005E22")]
			PlayIn,
			// Token: 0x04002F0B RID: 12043
			[Token(Token = "0x4005E23")]
			Main,
			// Token: 0x04002F0C RID: 12044
			[Token(Token = "0x4005E24")]
			UnloadChildSceneWait
		}
	}
}
