﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007CE RID: 1998
	[Token(Token = "0x20005EF")]
	[StructLayout(3)]
	public class MenuState_Help : MenuState
	{
		// Token: 0x06001EC8 RID: 7880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C40")]
		[Address(RVA = "0x1012524AC", Offset = "0x12524AC", VA = "0x1012524AC")]
		public MenuState_Help(MenuMain owner)
		{
		}

		// Token: 0x06001EC9 RID: 7881 RVA: 0x0000DAE8 File Offset: 0x0000BCE8
		[Token(Token = "0x6001C41")]
		[Address(RVA = "0x101253098", Offset = "0x1253098", VA = "0x101253098", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001ECA RID: 7882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C42")]
		[Address(RVA = "0x1012530A0", Offset = "0x12530A0", VA = "0x1012530A0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001ECB RID: 7883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C43")]
		[Address(RVA = "0x1012530A8", Offset = "0x12530A8", VA = "0x1012530A8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C44")]
		[Address(RVA = "0x1012530AC", Offset = "0x12530AC", VA = "0x1012530AC", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x0000DB00 File Offset: 0x0000BD00
		[Token(Token = "0x6001C45")]
		[Address(RVA = "0x1012530B4", Offset = "0x12530B4", VA = "0x1012530B4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001ECE RID: 7886 RVA: 0x0000DB18 File Offset: 0x0000BD18
		[Token(Token = "0x6001C46")]
		[Address(RVA = "0x1012532D0", Offset = "0x12532D0", VA = "0x1012532D0")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001ECF RID: 7887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C47")]
		[Address(RVA = "0x1012533FC", Offset = "0x12533FC", VA = "0x1012533FC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001ED0 RID: 7888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C48")]
		[Address(RVA = "0x101253464", Offset = "0x1253464", VA = "0x101253464")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001ED1 RID: 7889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C49")]
		[Address(RVA = "0x101253430", Offset = "0x1253430", VA = "0x101253430")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F0D RID: 12045
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400245E")]
		private MenuState_Help.eStep m_Step;

		// Token: 0x04002F0E RID: 12046
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400245F")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F0F RID: 12047
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002460")]
		private MenuHelpUI m_UI;

		// Token: 0x020007CF RID: 1999
		[Token(Token = "0x2000EB0")]
		private enum eStep
		{
			// Token: 0x04002F11 RID: 12049
			[Token(Token = "0x4005E26")]
			None = -1,
			// Token: 0x04002F12 RID: 12050
			[Token(Token = "0x4005E27")]
			First,
			// Token: 0x04002F13 RID: 12051
			[Token(Token = "0x4005E28")]
			LoadWait,
			// Token: 0x04002F14 RID: 12052
			[Token(Token = "0x4005E29")]
			PlayIn,
			// Token: 0x04002F15 RID: 12053
			[Token(Token = "0x4005E2A")]
			Main,
			// Token: 0x04002F16 RID: 12054
			[Token(Token = "0x4005E2B")]
			UnloadChildSceneWait
		}
	}
}
