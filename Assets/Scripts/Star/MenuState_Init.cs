﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007D0 RID: 2000
	[Token(Token = "0x20005F0")]
	[StructLayout(3)]
	public class MenuState_Init : MenuState
	{
		// Token: 0x06001ED2 RID: 7890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C4A")]
		[Address(RVA = "0x101252308", Offset = "0x1252308", VA = "0x101252308")]
		public MenuState_Init(MenuMain owner)
		{
		}

		// Token: 0x06001ED3 RID: 7891 RVA: 0x0000DB30 File Offset: 0x0000BD30
		[Token(Token = "0x6001C4B")]
		[Address(RVA = "0x101253468", Offset = "0x1253468", VA = "0x101253468", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001ED4 RID: 7892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C4C")]
		[Address(RVA = "0x101253470", Offset = "0x1253470", VA = "0x101253470", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001ED5 RID: 7893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C4D")]
		[Address(RVA = "0x101253478", Offset = "0x1253478", VA = "0x101253478", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001ED6 RID: 7894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C4E")]
		[Address(RVA = "0x10125347C", Offset = "0x125347C", VA = "0x10125347C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001ED7 RID: 7895 RVA: 0x0000DB48 File Offset: 0x0000BD48
		[Token(Token = "0x6001C4F")]
		[Address(RVA = "0x101253480", Offset = "0x1253480", VA = "0x101253480", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001ED8 RID: 7896 RVA: 0x0000DB60 File Offset: 0x0000BD60
		[Token(Token = "0x6001C50")]
		[Address(RVA = "0x101253C70", Offset = "0x1253C70", VA = "0x101253C70")]
		private bool SetupUI()
		{
			return default(bool);
		}

		// Token: 0x06001ED9 RID: 7897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C51")]
		[Address(RVA = "0x101253DB0", Offset = "0x1253DB0", VA = "0x101253DB0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002F17 RID: 12055
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002461")]
		private MenuState_Init.eStep m_Step;

		// Token: 0x04002F18 RID: 12056
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002462")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x020007D1 RID: 2001
		[Token(Token = "0x2000EB1")]
		private enum eStep
		{
			// Token: 0x04002F1A RID: 12058
			[Token(Token = "0x4005E2D")]
			None = -1,
			// Token: 0x04002F1B RID: 12059
			[Token(Token = "0x4005E2E")]
			First,
			// Token: 0x04002F1C RID: 12060
			[Token(Token = "0x4005E2F")]
			Load_Wait,
			// Token: 0x04002F1D RID: 12061
			[Token(Token = "0x4005E30")]
			Load_WaitBG,
			// Token: 0x04002F1E RID: 12062
			[Token(Token = "0x4005E31")]
			SetupUI,
			// Token: 0x04002F1F RID: 12063
			[Token(Token = "0x4005E32")]
			End
		}
	}
}
