﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007D2 RID: 2002
	[Token(Token = "0x20005F1")]
	[StructLayout(3)]
	public class MenuState_Library : MenuState
	{
		// Token: 0x06001EDA RID: 7898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C52")]
		[Address(RVA = "0x1012523F8", Offset = "0x12523F8", VA = "0x1012523F8")]
		public MenuState_Library(MenuMain owner)
		{
		}

		// Token: 0x06001EDB RID: 7899 RVA: 0x0000DB78 File Offset: 0x0000BD78
		[Token(Token = "0x6001C53")]
		[Address(RVA = "0x101253DB4", Offset = "0x1253DB4", VA = "0x101253DB4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EDC RID: 7900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C54")]
		[Address(RVA = "0x101253DBC", Offset = "0x1253DBC", VA = "0x101253DBC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EDD RID: 7901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C55")]
		[Address(RVA = "0x101253DC4", Offset = "0x1253DC4", VA = "0x101253DC4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EDE RID: 7902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C56")]
		[Address(RVA = "0x101253DC8", Offset = "0x1253DC8", VA = "0x101253DC8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EDF RID: 7903 RVA: 0x0000DB90 File Offset: 0x0000BD90
		[Token(Token = "0x6001C57")]
		[Address(RVA = "0x101253DD0", Offset = "0x1253DD0", VA = "0x101253DD0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EE0 RID: 7904 RVA: 0x0000DBA8 File Offset: 0x0000BDA8
		[Token(Token = "0x6001C58")]
		[Address(RVA = "0x101254208", Offset = "0x1254208", VA = "0x101254208")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C59")]
		[Address(RVA = "0x1012544EC", Offset = "0x12544EC", VA = "0x1012544EC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C5A")]
		[Address(RVA = "0x101254704", Offset = "0x1254704", VA = "0x101254704")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C5B")]
		[Address(RVA = "0x101254668", Offset = "0x1254668", VA = "0x101254668")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F20 RID: 12064
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002463")]
		private MenuState_Library.eStep m_Step;

		// Token: 0x04002F21 RID: 12065
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002464")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F22 RID: 12066
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002465")]
		private MenuLibraryUI m_UI;

		// Token: 0x020007D3 RID: 2003
		[Token(Token = "0x2000EB2")]
		private enum eStep
		{
			// Token: 0x04002F24 RID: 12068
			[Token(Token = "0x4005E34")]
			None = -1,
			// Token: 0x04002F25 RID: 12069
			[Token(Token = "0x4005E35")]
			First,
			// Token: 0x04002F26 RID: 12070
			[Token(Token = "0x4005E36")]
			LoadWait,
			// Token: 0x04002F27 RID: 12071
			[Token(Token = "0x4005E37")]
			PlayIn,
			// Token: 0x04002F28 RID: 12072
			[Token(Token = "0x4005E38")]
			Main,
			// Token: 0x04002F29 RID: 12073
			[Token(Token = "0x4005E39")]
			UnloadChildSceneWait
		}
	}
}
