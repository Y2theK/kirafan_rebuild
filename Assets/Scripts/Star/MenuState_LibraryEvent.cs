﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007D4 RID: 2004
	[Token(Token = "0x20005F2")]
	[StructLayout(3)]
	public class MenuState_LibraryEvent : MenuState
	{
		// Token: 0x06001EE4 RID: 7908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C5C")]
		[Address(RVA = "0x101252524", Offset = "0x1252524", VA = "0x101252524")]
		public MenuState_LibraryEvent(MenuMain owner)
		{
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x0000DBC0 File Offset: 0x0000BDC0
		[Token(Token = "0x6001C5D")]
		[Address(RVA = "0x101254708", Offset = "0x1254708", VA = "0x101254708", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C5E")]
		[Address(RVA = "0x101254710", Offset = "0x1254710", VA = "0x101254710", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C5F")]
		[Address(RVA = "0x101254718", Offset = "0x1254718", VA = "0x101254718", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C60")]
		[Address(RVA = "0x10125471C", Offset = "0x125471C", VA = "0x10125471C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EE9 RID: 7913 RVA: 0x0000DBD8 File Offset: 0x0000BDD8
		[Token(Token = "0x6001C61")]
		[Address(RVA = "0x101254724", Offset = "0x1254724", VA = "0x101254724", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EEA RID: 7914 RVA: 0x0000DBF0 File Offset: 0x0000BDF0
		[Token(Token = "0x6001C62")]
		[Address(RVA = "0x101254C64", Offset = "0x1254C64", VA = "0x101254C64")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001EEB RID: 7915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C63")]
		[Address(RVA = "0x101254EE4", Offset = "0x1254EE4", VA = "0x101254EE4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001EEC RID: 7916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C64")]
		[Address(RVA = "0x101255010", Offset = "0x1255010", VA = "0x101255010")]
		private void OnTransit()
		{
		}

		// Token: 0x06001EED RID: 7917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C65")]
		[Address(RVA = "0x101254FDC", Offset = "0x1254FDC", VA = "0x101254FDC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F2A RID: 12074
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002466")]
		private MenuState_LibraryEvent.eStep m_Step;

		// Token: 0x04002F2B RID: 12075
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002467")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F2C RID: 12076
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002468")]
		private MenuLibraryEventUI m_UI;

		// Token: 0x020007D5 RID: 2005
		[Token(Token = "0x2000EB3")]
		private enum eStep
		{
			// Token: 0x04002F2E RID: 12078
			[Token(Token = "0x4005E3B")]
			None = -1,
			// Token: 0x04002F2F RID: 12079
			[Token(Token = "0x4005E3C")]
			First,
			// Token: 0x04002F30 RID: 12080
			[Token(Token = "0x4005E3D")]
			LoadWait,
			// Token: 0x04002F31 RID: 12081
			[Token(Token = "0x4005E3E")]
			PlayIn,
			// Token: 0x04002F32 RID: 12082
			[Token(Token = "0x4005E3F")]
			Main,
			// Token: 0x04002F33 RID: 12083
			[Token(Token = "0x4005E40")]
			UnloadChildSceneWait
		}
	}
}
