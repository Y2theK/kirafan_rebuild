﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007D6 RID: 2006
	[Token(Token = "0x20005F3")]
	[StructLayout(3)]
	public class MenuState_LibraryOPMovie : MenuState
	{
		// Token: 0x06001EEE RID: 7918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C66")]
		[Address(RVA = "0x101252614", Offset = "0x1252614", VA = "0x101252614")]
		public MenuState_LibraryOPMovie(MenuMain owner)
		{
		}

		// Token: 0x06001EEF RID: 7919 RVA: 0x0000DC08 File Offset: 0x0000BE08
		[Token(Token = "0x6001C67")]
		[Address(RVA = "0x101255044", Offset = "0x1255044", VA = "0x101255044", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EF0 RID: 7920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C68")]
		[Address(RVA = "0x10125504C", Offset = "0x125504C", VA = "0x10125504C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EF1 RID: 7921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C69")]
		[Address(RVA = "0x101255054", Offset = "0x1255054", VA = "0x101255054", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EF2 RID: 7922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C6A")]
		[Address(RVA = "0x101255058", Offset = "0x1255058", VA = "0x101255058", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EF3 RID: 7923 RVA: 0x0000DC20 File Offset: 0x0000BE20
		[Token(Token = "0x6001C6B")]
		[Address(RVA = "0x101255060", Offset = "0x1255060", VA = "0x101255060", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EF4 RID: 7924 RVA: 0x0000DC38 File Offset: 0x0000BE38
		[Token(Token = "0x6001C6C")]
		[Address(RVA = "0x101255388", Offset = "0x1255388", VA = "0x101255388")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001EF5 RID: 7925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C6D")]
		[Address(RVA = "0x1012555F0", Offset = "0x12555F0", VA = "0x1012555F0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001EF6 RID: 7926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C6E")]
		[Address(RVA = "0x101255710", Offset = "0x1255710", VA = "0x101255710")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001EF7 RID: 7927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C6F")]
		[Address(RVA = "0x101255674", Offset = "0x1255674", VA = "0x101255674")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F34 RID: 12084
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002469")]
		private MenuState_LibraryOPMovie.eStep m_Step;

		// Token: 0x04002F35 RID: 12085
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400246A")]
		private bool m_playMovie;

		// Token: 0x04002F36 RID: 12086
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400246B")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F37 RID: 12087
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400246C")]
		private MenuLibraryOPMovieUI m_UI;

		// Token: 0x020007D7 RID: 2007
		[Token(Token = "0x2000EB4")]
		private enum eStep
		{
			// Token: 0x04002F39 RID: 12089
			[Token(Token = "0x4005E42")]
			None = -1,
			// Token: 0x04002F3A RID: 12090
			[Token(Token = "0x4005E43")]
			First,
			// Token: 0x04002F3B RID: 12091
			[Token(Token = "0x4005E44")]
			LoadWait,
			// Token: 0x04002F3C RID: 12092
			[Token(Token = "0x4005E45")]
			PlayIn,
			// Token: 0x04002F3D RID: 12093
			[Token(Token = "0x4005E46")]
			Main,
			// Token: 0x04002F3E RID: 12094
			[Token(Token = "0x4005E47")]
			UnloadChildSceneWait
		}
	}
}
