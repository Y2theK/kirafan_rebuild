﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007D8 RID: 2008
	[Token(Token = "0x20005F4")]
	[StructLayout(3)]
	public class MenuState_LibraryOriginalChara : MenuState
	{
		// Token: 0x06001EF8 RID: 7928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C70")]
		[Address(RVA = "0x101252560", Offset = "0x1252560", VA = "0x101252560")]
		public MenuState_LibraryOriginalChara(MenuMain owner)
		{
		}

		// Token: 0x06001EF9 RID: 7929 RVA: 0x0000DC50 File Offset: 0x0000BE50
		[Token(Token = "0x6001C71")]
		[Address(RVA = "0x10125571C", Offset = "0x125571C", VA = "0x10125571C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001EFA RID: 7930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C72")]
		[Address(RVA = "0x101255724", Offset = "0x1255724", VA = "0x101255724", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001EFB RID: 7931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C73")]
		[Address(RVA = "0x10125572C", Offset = "0x125572C", VA = "0x10125572C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001EFC RID: 7932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C74")]
		[Address(RVA = "0x101255730", Offset = "0x1255730", VA = "0x101255730", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001EFD RID: 7933 RVA: 0x0000DC68 File Offset: 0x0000BE68
		[Token(Token = "0x6001C75")]
		[Address(RVA = "0x101255738", Offset = "0x1255738", VA = "0x101255738", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001EFE RID: 7934 RVA: 0x0000DC80 File Offset: 0x0000BE80
		[Token(Token = "0x6001C76")]
		[Address(RVA = "0x101255974", Offset = "0x1255974", VA = "0x101255974")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001EFF RID: 7935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C77")]
		[Address(RVA = "0x101255B90", Offset = "0x1255B90", VA = "0x101255B90", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F00 RID: 7936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C78")]
		[Address(RVA = "0x101255C60", Offset = "0x1255C60", VA = "0x101255C60")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F01 RID: 7937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C79")]
		[Address(RVA = "0x101255C2C", Offset = "0x1255C2C", VA = "0x101255C2C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F3F RID: 12095
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400246D")]
		private MenuState_LibraryOriginalChara.eStep m_Step;

		// Token: 0x04002F40 RID: 12096
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400246E")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F41 RID: 12097
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400246F")]
		private MenuLibraryOriginalCharaUI m_UI;

		// Token: 0x020007D9 RID: 2009
		[Token(Token = "0x2000EB5")]
		private enum eStep
		{
			// Token: 0x04002F43 RID: 12099
			[Token(Token = "0x4005E49")]
			None = -1,
			// Token: 0x04002F44 RID: 12100
			[Token(Token = "0x4005E4A")]
			First,
			// Token: 0x04002F45 RID: 12101
			[Token(Token = "0x4005E4B")]
			LoadWait,
			// Token: 0x04002F46 RID: 12102
			[Token(Token = "0x4005E4C")]
			PlayIn,
			// Token: 0x04002F47 RID: 12103
			[Token(Token = "0x4005E4D")]
			Main,
			// Token: 0x04002F48 RID: 12104
			[Token(Token = "0x4005E4E")]
			UnloadChildSceneWait
		}
	}
}
