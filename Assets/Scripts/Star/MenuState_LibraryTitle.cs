﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007DA RID: 2010
	[Token(Token = "0x20005F5")]
	[StructLayout(3)]
	public class MenuState_LibraryTitle : MenuState
	{
		// Token: 0x06001F02 RID: 7938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C7A")]
		[Address(RVA = "0x1012524E8", Offset = "0x12524E8", VA = "0x1012524E8")]
		public MenuState_LibraryTitle(MenuMain owner)
		{
		}

		// Token: 0x06001F03 RID: 7939 RVA: 0x0000DC98 File Offset: 0x0000BE98
		[Token(Token = "0x6001C7B")]
		[Address(RVA = "0x101255C64", Offset = "0x1255C64", VA = "0x101255C64", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F04 RID: 7940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C7C")]
		[Address(RVA = "0x101255C6C", Offset = "0x1255C6C", VA = "0x101255C6C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F05 RID: 7941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C7D")]
		[Address(RVA = "0x101255C74", Offset = "0x1255C74", VA = "0x101255C74", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F06 RID: 7942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C7E")]
		[Address(RVA = "0x101255C78", Offset = "0x1255C78", VA = "0x101255C78", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F07 RID: 7943 RVA: 0x0000DCB0 File Offset: 0x0000BEB0
		[Token(Token = "0x6001C7F")]
		[Address(RVA = "0x101255C80", Offset = "0x1255C80", VA = "0x101255C80", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F08 RID: 7944 RVA: 0x0000DCC8 File Offset: 0x0000BEC8
		[Token(Token = "0x6001C80")]
		[Address(RVA = "0x101255E9C", Offset = "0x1255E9C", VA = "0x101255E9C")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F09 RID: 7945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C81")]
		[Address(RVA = "0x101256128", Offset = "0x1256128", VA = "0x101256128", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F0A RID: 7946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C82")]
		[Address(RVA = "0x1012561F8", Offset = "0x12561F8", VA = "0x1012561F8")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F0B RID: 7947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C83")]
		[Address(RVA = "0x1012561C4", Offset = "0x12561C4", VA = "0x1012561C4")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F49 RID: 12105
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002470")]
		private MenuState_LibraryTitle.eStep m_Step;

		// Token: 0x04002F4A RID: 12106
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002471")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F4B RID: 12107
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002472")]
		private MenuLibraryWordUI m_UI;

		// Token: 0x020007DB RID: 2011
		[Token(Token = "0x2000EB6")]
		private enum eStep
		{
			// Token: 0x04002F4D RID: 12109
			[Token(Token = "0x4005E50")]
			None = -1,
			// Token: 0x04002F4E RID: 12110
			[Token(Token = "0x4005E51")]
			First,
			// Token: 0x04002F4F RID: 12111
			[Token(Token = "0x4005E52")]
			LoadWait,
			// Token: 0x04002F50 RID: 12112
			[Token(Token = "0x4005E53")]
			PlayIn,
			// Token: 0x04002F51 RID: 12113
			[Token(Token = "0x4005E54")]
			Main,
			// Token: 0x04002F52 RID: 12114
			[Token(Token = "0x4005E55")]
			UnloadChildSceneWait
		}
	}
}
