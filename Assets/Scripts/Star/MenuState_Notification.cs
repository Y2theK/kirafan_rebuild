﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007DC RID: 2012
	[Token(Token = "0x20005F6")]
	[StructLayout(3)]
	public class MenuState_Notification : MenuState
	{
		// Token: 0x06001F0C RID: 7948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C84")]
		[Address(RVA = "0x101252470", Offset = "0x1252470", VA = "0x101252470")]
		public MenuState_Notification(MenuMain owner)
		{
		}

		// Token: 0x06001F0D RID: 7949 RVA: 0x0000DCE0 File Offset: 0x0000BEE0
		[Token(Token = "0x6001C85")]
		[Address(RVA = "0x1012561FC", Offset = "0x12561FC", VA = "0x1012561FC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F0E RID: 7950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C86")]
		[Address(RVA = "0x101256204", Offset = "0x1256204", VA = "0x101256204", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F0F RID: 7951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C87")]
		[Address(RVA = "0x10125620C", Offset = "0x125620C", VA = "0x10125620C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F10 RID: 7952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C88")]
		[Address(RVA = "0x101256210", Offset = "0x1256210", VA = "0x101256210", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F11 RID: 7953 RVA: 0x0000DCF8 File Offset: 0x0000BEF8
		[Token(Token = "0x6001C89")]
		[Address(RVA = "0x101256218", Offset = "0x1256218", VA = "0x101256218", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F12 RID: 7954 RVA: 0x0000DD10 File Offset: 0x0000BF10
		[Token(Token = "0x6001C8A")]
		[Address(RVA = "0x101256434", Offset = "0x1256434", VA = "0x101256434")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F13 RID: 7955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C8B")]
		[Address(RVA = "0x1012565EC", Offset = "0x12565EC", VA = "0x1012565EC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F14 RID: 7956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C8C")]
		[Address(RVA = "0x1012569D0", Offset = "0x12569D0", VA = "0x1012569D0")]
		private void OnClickDecideButton()
		{
		}

		// Token: 0x06001F15 RID: 7957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C8D")]
		[Address(RVA = "0x101256AD0", Offset = "0x1256AD0", VA = "0x101256AD0")]
		private void OnResponse_SetPushNotification()
		{
		}

		// Token: 0x06001F16 RID: 7958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C8E")]
		[Address(RVA = "0x101256934", Offset = "0x1256934", VA = "0x101256934")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F53 RID: 12115
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002473")]
		private MenuState_Notification.eStep m_Step;

		// Token: 0x04002F54 RID: 12116
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002474")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F55 RID: 12117
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002475")]
		private MenuNotificationUI m_UI;

		// Token: 0x020007DD RID: 2013
		[Token(Token = "0x2000EB7")]
		private enum eStep
		{
			// Token: 0x04002F57 RID: 12119
			[Token(Token = "0x4005E57")]
			None = -1,
			// Token: 0x04002F58 RID: 12120
			[Token(Token = "0x4005E58")]
			First,
			// Token: 0x04002F59 RID: 12121
			[Token(Token = "0x4005E59")]
			LoadWait,
			// Token: 0x04002F5A RID: 12122
			[Token(Token = "0x4005E5A")]
			PlayIn,
			// Token: 0x04002F5B RID: 12123
			[Token(Token = "0x4005E5B")]
			Main,
			// Token: 0x04002F5C RID: 12124
			[Token(Token = "0x4005E5C")]
			UnloadChildSceneWait
		}
	}
}
