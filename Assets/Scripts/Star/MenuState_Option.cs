﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007DE RID: 2014
	[Token(Token = "0x20005F7")]
	[StructLayout(3)]
	public class MenuState_Option : MenuState
	{
		// Token: 0x06001F17 RID: 7959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C8F")]
		[Address(RVA = "0x101252434", Offset = "0x1252434", VA = "0x101252434")]
		public MenuState_Option(MenuMain owner)
		{
		}

		// Token: 0x06001F18 RID: 7960 RVA: 0x0000DD28 File Offset: 0x0000BF28
		[Token(Token = "0x6001C90")]
		[Address(RVA = "0x101256AD4", Offset = "0x1256AD4", VA = "0x101256AD4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C91")]
		[Address(RVA = "0x101256ADC", Offset = "0x1256ADC", VA = "0x101256ADC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C92")]
		[Address(RVA = "0x101256AE4", Offset = "0x1256AE4", VA = "0x101256AE4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C93")]
		[Address(RVA = "0x101256AE8", Offset = "0x1256AE8", VA = "0x101256AE8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x0000DD40 File Offset: 0x0000BF40
		[Token(Token = "0x6001C94")]
		[Address(RVA = "0x101256AF0", Offset = "0x1256AF0", VA = "0x101256AF0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F1D RID: 7965 RVA: 0x0000DD58 File Offset: 0x0000BF58
		[Token(Token = "0x6001C95")]
		[Address(RVA = "0x101256D0C", Offset = "0x1256D0C", VA = "0x101256D0C")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F1E RID: 7966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C96")]
		[Address(RVA = "0x101256E78", Offset = "0x1256E78", VA = "0x101256E78", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F1F RID: 7967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C97")]
		[Address(RVA = "0x101256F48", Offset = "0x1256F48", VA = "0x101256F48")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F20 RID: 7968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C98")]
		[Address(RVA = "0x101256F14", Offset = "0x1256F14", VA = "0x101256F14")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F5D RID: 12125
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002476")]
		private MenuState_Option.eStep m_Step;

		// Token: 0x04002F5E RID: 12126
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002477")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F5F RID: 12127
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002478")]
		private MenuOptionUI m_UI;

		// Token: 0x020007DF RID: 2015
		[Token(Token = "0x2000EB8")]
		private enum eStep
		{
			// Token: 0x04002F61 RID: 12129
			[Token(Token = "0x4005E5E")]
			None = -1,
			// Token: 0x04002F62 RID: 12130
			[Token(Token = "0x4005E5F")]
			First,
			// Token: 0x04002F63 RID: 12131
			[Token(Token = "0x4005E60")]
			LoadWait,
			// Token: 0x04002F64 RID: 12132
			[Token(Token = "0x4005E61")]
			PlayIn,
			// Token: 0x04002F65 RID: 12133
			[Token(Token = "0x4005E62")]
			Main,
			// Token: 0x04002F66 RID: 12134
			[Token(Token = "0x4005E63")]
			UnloadChildSceneWait
		}
	}
}
