﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007E0 RID: 2016
	[Token(Token = "0x20005F8")]
	[StructLayout(3)]
	public class MenuState_PlayerMoveConfiguration : MenuState
	{
		// Token: 0x06001F21 RID: 7969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C99")]
		[Address(RVA = "0x10125259C", Offset = "0x125259C", VA = "0x10125259C")]
		public MenuState_PlayerMoveConfiguration(MenuMain owner)
		{
		}

		// Token: 0x06001F22 RID: 7970 RVA: 0x0000DD70 File Offset: 0x0000BF70
		[Token(Token = "0x6001C9A")]
		[Address(RVA = "0x101256F4C", Offset = "0x1256F4C", VA = "0x101256F4C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F23 RID: 7971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C9B")]
		[Address(RVA = "0x101256F54", Offset = "0x1256F54", VA = "0x101256F54", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F24 RID: 7972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C9C")]
		[Address(RVA = "0x101256F5C", Offset = "0x1256F5C", VA = "0x101256F5C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F25 RID: 7973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C9D")]
		[Address(RVA = "0x101256F60", Offset = "0x1256F60", VA = "0x101256F60", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F26 RID: 7974 RVA: 0x0000DD88 File Offset: 0x0000BF88
		[Token(Token = "0x6001C9E")]
		[Address(RVA = "0x101256F6C", Offset = "0x1256F6C", VA = "0x101256F6C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F27 RID: 7975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001C9F")]
		[Address(RVA = "0x101257670", Offset = "0x1257670", VA = "0x101257670")]
		private void ChangeStep(MenuState_PlayerMoveConfiguration.eStep step)
		{
		}

		// Token: 0x06001F28 RID: 7976 RVA: 0x0000DDA0 File Offset: 0x0000BFA0
		[Token(Token = "0x6001CA0")]
		[Address(RVA = "0x1012572F8", Offset = "0x12572F8", VA = "0x1012572F8")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA1")]
		[Address(RVA = "0x101257A00", Offset = "0x1257A00", VA = "0x101257A00", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F2A RID: 7978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA2")]
		[Address(RVA = "0x101257A68", Offset = "0x1257A68", VA = "0x101257A68")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F2B RID: 7979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA3")]
		[Address(RVA = "0x101257A34", Offset = "0x1257A34", VA = "0x101257A34")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA4")]
		[Address(RVA = "0x101257A6C", Offset = "0x1257A6C", VA = "0x101257A6C")]
		private void OnClickInputPasswordDecideButton(string moveCodePassward)
		{
		}

		// Token: 0x06001F2D RID: 7981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA5")]
		[Address(RVA = "0x101257B34", Offset = "0x1257B34", VA = "0x101257B34")]
		private void OnResponse_PlayerMoveGet(bool isError)
		{
		}

		// Token: 0x06001F2E RID: 7982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA6")]
		[Address(RVA = "0x101257C94", Offset = "0x1257C94", VA = "0x101257C94")]
		private void OnClickPlayerMoveDataCloseButton()
		{
		}

		// Token: 0x06001F2F RID: 7983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA7")]
		[Address(RVA = "0x101257D30", Offset = "0x1257D30", VA = "0x101257D30")]
		public void OnClickALButtonPlatform()
		{
		}

		// Token: 0x06001F30 RID: 7984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA8")]
		[Address(RVA = "0x101257D48", Offset = "0x1257D48", VA = "0x101257D48")]
		public void OnClickALButtonUseIDAndPassword()
		{
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CA9")]
		[Address(RVA = "0x101257CF4", Offset = "0x1257CF4", VA = "0x101257CF4")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x06001F32 RID: 7986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CAA")]
		[Address(RVA = "0x101257D60", Offset = "0x1257D60", VA = "0x101257D60")]
		public void OnClickNotifyConfirm(int idx)
		{
		}

		// Token: 0x06001F33 RID: 7987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CAB")]
		[Address(RVA = "0x101257DC0", Offset = "0x1257DC0", VA = "0x101257DC0")]
		public void OnClickResultSuccessConfirm(int idx)
		{
		}

		// Token: 0x06001F34 RID: 7988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CAC")]
		[Address(RVA = "0x101257E08", Offset = "0x1257E08", VA = "0x101257E08")]
		public void OnClickResultErrorConfirm()
		{
		}

		// Token: 0x04002F67 RID: 12135
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002479")]
		private MenuState_PlayerMoveConfiguration.eStep m_Step;

		// Token: 0x04002F68 RID: 12136
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400247A")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F69 RID: 12137
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400247B")]
		private MenuPlayerMoveConfigurationUI m_UI;

		// Token: 0x04002F6A RID: 12138
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400247C")]
		private UIGroup m_SelectGroup;

		// Token: 0x04002F6B RID: 12139
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400247D")]
		private DataInheritenceSelectContnt m_DataInheritenceSelectWindow;

		// Token: 0x020007E1 RID: 2017
		[Token(Token = "0x2000EB9")]
		private enum eStep
		{
			// Token: 0x04002F6D RID: 12141
			[Token(Token = "0x4005E65")]
			None = -1,
			// Token: 0x04002F6E RID: 12142
			[Token(Token = "0x4005E66")]
			First,
			// Token: 0x04002F6F RID: 12143
			[Token(Token = "0x4005E67")]
			LoadWait,
			// Token: 0x04002F70 RID: 12144
			[Token(Token = "0x4005E68")]
			PlayIn,
			// Token: 0x04002F71 RID: 12145
			[Token(Token = "0x4005E69")]
			PlayInWait,
			// Token: 0x04002F72 RID: 12146
			[Token(Token = "0x4005E6A")]
			MainSelect,
			// Token: 0x04002F73 RID: 12147
			[Token(Token = "0x4005E6B")]
			AccountLinkNotifyDialog,
			// Token: 0x04002F74 RID: 12148
			[Token(Token = "0x4005E6C")]
			AccountLinkWait,
			// Token: 0x04002F75 RID: 12149
			[Token(Token = "0x4005E6D")]
			AccountLinkEndDialog,
			// Token: 0x04002F76 RID: 12150
			[Token(Token = "0x4005E6E")]
			MainIDAndPass,
			// Token: 0x04002F77 RID: 12151
			[Token(Token = "0x4005E6F")]
			UnloadChildSceneWait,
			// Token: 0x04002F78 RID: 12152
			[Token(Token = "0x4005E70")]
			Close
		}
	}
}
