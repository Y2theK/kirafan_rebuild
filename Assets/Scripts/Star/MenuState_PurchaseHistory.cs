﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007E2 RID: 2018
	[Token(Token = "0x20005F9")]
	[StructLayout(3)]
	public class MenuState_PurchaseHistory : MenuState
	{
		// Token: 0x06001F35 RID: 7989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CAD")]
		[Address(RVA = "0x1012525D8", Offset = "0x12525D8", VA = "0x1012525D8")]
		public MenuState_PurchaseHistory(MenuMain owner)
		{
		}

		// Token: 0x06001F36 RID: 7990 RVA: 0x0000DDB8 File Offset: 0x0000BFB8
		[Token(Token = "0x6001CAE")]
		[Address(RVA = "0x101257E50", Offset = "0x1257E50", VA = "0x101257E50", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F37 RID: 7991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CAF")]
		[Address(RVA = "0x101257E58", Offset = "0x1257E58", VA = "0x101257E58", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F38 RID: 7992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB0")]
		[Address(RVA = "0x101257E60", Offset = "0x1257E60", VA = "0x101257E60", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F39 RID: 7993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB1")]
		[Address(RVA = "0x101257E64", Offset = "0x1257E64", VA = "0x101257E64", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F3A RID: 7994 RVA: 0x0000DDD0 File Offset: 0x0000BFD0
		[Token(Token = "0x6001CB2")]
		[Address(RVA = "0x101257E6C", Offset = "0x1257E6C", VA = "0x101257E6C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F3B RID: 7995 RVA: 0x0000DDE8 File Offset: 0x0000BFE8
		[Token(Token = "0x6001CB3")]
		[Address(RVA = "0x101258104", Offset = "0x1258104", VA = "0x101258104")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F3C RID: 7996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB4")]
		[Address(RVA = "0x101258320", Offset = "0x1258320", VA = "0x101258320", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F3D RID: 7997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB5")]
		[Address(RVA = "0x1012583F0", Offset = "0x12583F0", VA = "0x1012583F0")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB6")]
		[Address(RVA = "0x1012583BC", Offset = "0x12583BC", VA = "0x1012583BC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001F3F RID: 7999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB7")]
		[Address(RVA = "0x1012583F4", Offset = "0x12583F4", VA = "0x1012583F4")]
		private void OnResponse()
		{
		}

		// Token: 0x04002F79 RID: 12153
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400247E")]
		private MenuState_PurchaseHistory.eStep m_Step;

		// Token: 0x04002F7A RID: 12154
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400247F")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F7B RID: 12155
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002480")]
		private MenuPurchaseHistoryUI m_UI;

		// Token: 0x020007E3 RID: 2019
		[Token(Token = "0x2000EBA")]
		private enum eStep
		{
			// Token: 0x04002F7D RID: 12157
			[Token(Token = "0x4005E72")]
			None = -1,
			// Token: 0x04002F7E RID: 12158
			[Token(Token = "0x4005E73")]
			First,
			// Token: 0x04002F7F RID: 12159
			[Token(Token = "0x4005E74")]
			LoadWait,
			// Token: 0x04002F80 RID: 12160
			[Token(Token = "0x4005E75")]
			RequestWait,
			// Token: 0x04002F81 RID: 12161
			[Token(Token = "0x4005E76")]
			PlayIn,
			// Token: 0x04002F82 RID: 12162
			[Token(Token = "0x4005E77")]
			Main,
			// Token: 0x04002F83 RID: 12163
			[Token(Token = "0x4005E78")]
			UnloadChildSceneWait
		}
	}
}
