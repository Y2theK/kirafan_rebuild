﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007E4 RID: 2020
	[Token(Token = "0x20005FA")]
	[StructLayout(3)]
	public class MenuState_Support : MenuState
	{
		// Token: 0x06001F40 RID: 8000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CB8")]
		[Address(RVA = "0x101258400", Offset = "0x1258400", VA = "0x101258400")]
		public MenuState_Support(MenuMain owner)
		{
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x0000DE00 File Offset: 0x0000C000
		[Token(Token = "0x6001CB9")]
		[Address(RVA = "0x10125843C", Offset = "0x125843C", VA = "0x10125843C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CBA")]
		[Address(RVA = "0x101258444", Offset = "0x1258444", VA = "0x101258444", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CBB")]
		[Address(RVA = "0x10125844C", Offset = "0x125844C", VA = "0x10125844C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F44 RID: 8004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CBC")]
		[Address(RVA = "0x101258450", Offset = "0x1258450", VA = "0x101258450", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F45 RID: 8005 RVA: 0x0000DE18 File Offset: 0x0000C018
		[Token(Token = "0x6001CBD")]
		[Address(RVA = "0x101258458", Offset = "0x1258458", VA = "0x101258458", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F46 RID: 8006 RVA: 0x0000DE30 File Offset: 0x0000C030
		[Token(Token = "0x6001CBE")]
		[Address(RVA = "0x101258688", Offset = "0x1258688", VA = "0x101258688")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CBF")]
		[Address(RVA = "0x1012588E8", Offset = "0x12588E8", VA = "0x1012588E8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F48 RID: 8008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC0")]
		[Address(RVA = "0x1012589B8", Offset = "0x12589B8", VA = "0x1012589B8")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F49 RID: 8009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC1")]
		[Address(RVA = "0x1012589BC", Offset = "0x12589BC", VA = "0x1012589BC")]
		protected void OnClickPurchaseHistory()
		{
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC2")]
		[Address(RVA = "0x101258984", Offset = "0x1258984", VA = "0x101258984")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F84 RID: 12164
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002481")]
		private MenuState_Support.eStep m_Step;

		// Token: 0x04002F85 RID: 12165
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002482")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F86 RID: 12166
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002483")]
		private MenuSupportUI m_UI;

		// Token: 0x04002F87 RID: 12167
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002484")]
		private bool m_IsOnClickPurchaseHistory;

		// Token: 0x020007E5 RID: 2021
		[Token(Token = "0x2000EBB")]
		private enum eStep
		{
			// Token: 0x04002F89 RID: 12169
			[Token(Token = "0x4005E7A")]
			None = -1,
			// Token: 0x04002F8A RID: 12170
			[Token(Token = "0x4005E7B")]
			First,
			// Token: 0x04002F8B RID: 12171
			[Token(Token = "0x4005E7C")]
			LoadWait,
			// Token: 0x04002F8C RID: 12172
			[Token(Token = "0x4005E7D")]
			PlayIn,
			// Token: 0x04002F8D RID: 12173
			[Token(Token = "0x4005E7E")]
			Main,
			// Token: 0x04002F8E RID: 12174
			[Token(Token = "0x4005E7F")]
			UnloadChildSceneWait
		}
	}
}
