﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007E6 RID: 2022
	[Token(Token = "0x20005FB")]
	[StructLayout(3)]
	public class MenuState_Top : MenuState
	{
		// Token: 0x06001F4B RID: 8011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC3")]
		[Address(RVA = "0x101252344", Offset = "0x1252344", VA = "0x101252344")]
		public MenuState_Top(MenuMain owner)
		{
		}

		// Token: 0x06001F4C RID: 8012 RVA: 0x0000DE48 File Offset: 0x0000C048
		[Token(Token = "0x6001CC4")]
		[Address(RVA = "0x101258A64", Offset = "0x1258A64", VA = "0x101258A64", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F4D RID: 8013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC5")]
		[Address(RVA = "0x101258A6C", Offset = "0x1258A6C", VA = "0x101258A6C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F4E RID: 8014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC6")]
		[Address(RVA = "0x101258A74", Offset = "0x1258A74", VA = "0x101258A74", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F4F RID: 8015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CC7")]
		[Address(RVA = "0x101258A78", Offset = "0x1258A78", VA = "0x101258A78", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F50 RID: 8016 RVA: 0x0000DE60 File Offset: 0x0000C060
		[Token(Token = "0x6001CC8")]
		[Address(RVA = "0x101258A80", Offset = "0x1258A80", VA = "0x101258A80", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F51 RID: 8017 RVA: 0x0000DE78 File Offset: 0x0000C078
		[Token(Token = "0x6001CC9")]
		[Address(RVA = "0x101258D54", Offset = "0x1258D54", VA = "0x101258D54")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F52 RID: 8018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CCA")]
		[Address(RVA = "0x101258F70", Offset = "0x1258F70", VA = "0x101258F70", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F53 RID: 8019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CCB")]
		[Address(RVA = "0x1012590D4", Offset = "0x12590D4", VA = "0x1012590D4")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F54 RID: 8020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CCC")]
		[Address(RVA = "0x1012590A0", Offset = "0x12590A0", VA = "0x1012590A0")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002F8F RID: 12175
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002485")]
		private MenuState_Top.eStep m_Step;

		// Token: 0x04002F90 RID: 12176
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002486")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F91 RID: 12177
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002487")]
		private MenuTopUI m_UI;

		// Token: 0x020007E7 RID: 2023
		[Token(Token = "0x2000EBC")]
		private enum eStep
		{
			// Token: 0x04002F93 RID: 12179
			[Token(Token = "0x4005E81")]
			None = -1,
			// Token: 0x04002F94 RID: 12180
			[Token(Token = "0x4005E82")]
			First,
			// Token: 0x04002F95 RID: 12181
			[Token(Token = "0x4005E83")]
			LoadWait,
			// Token: 0x04002F96 RID: 12182
			[Token(Token = "0x4005E84")]
			PlayIn,
			// Token: 0x04002F97 RID: 12183
			[Token(Token = "0x4005E85")]
			Main,
			// Token: 0x04002F98 RID: 12184
			[Token(Token = "0x4005E86")]
			UnloadChildSceneWait
		}
	}
}
