﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x020007E8 RID: 2024
	[Token(Token = "0x20005FC")]
	[StructLayout(3)]
	public class MenuState_UserInfomation : MenuState
	{
		// Token: 0x06001F55 RID: 8021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CCD")]
		[Address(RVA = "0x101252380", Offset = "0x1252380", VA = "0x101252380")]
		public MenuState_UserInfomation(MenuMain owner)
		{
		}

		// Token: 0x06001F56 RID: 8022 RVA: 0x0000DE90 File Offset: 0x0000C090
		[Token(Token = "0x6001CCE")]
		[Address(RVA = "0x1012590D8", Offset = "0x12590D8", VA = "0x1012590D8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F57 RID: 8023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CCF")]
		[Address(RVA = "0x1012590E0", Offset = "0x12590E0", VA = "0x1012590E0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F58 RID: 8024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD0")]
		[Address(RVA = "0x1012590E8", Offset = "0x12590E8", VA = "0x1012590E8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F59 RID: 8025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD1")]
		[Address(RVA = "0x1012590EC", Offset = "0x12590EC", VA = "0x1012590EC", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F5A RID: 8026 RVA: 0x0000DEA8 File Offset: 0x0000C0A8
		[Token(Token = "0x6001CD2")]
		[Address(RVA = "0x101259174", Offset = "0x1259174", VA = "0x101259174", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F5B RID: 8027 RVA: 0x0000DEC0 File Offset: 0x0000C0C0
		[Token(Token = "0x6001CD3")]
		[Address(RVA = "0x10125943C", Offset = "0x125943C", VA = "0x10125943C")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001F5C RID: 8028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD4")]
		[Address(RVA = "0x1012596D4", Offset = "0x12596D4", VA = "0x1012596D4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F5D RID: 8029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD5")]
		[Address(RVA = "0x101259914", Offset = "0x1259914", VA = "0x101259914")]
		private void OnClickButton()
		{
		}

		// Token: 0x06001F5E RID: 8030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD6")]
		[Address(RVA = "0x101259878", Offset = "0x1259878", VA = "0x101259878")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001F5F RID: 8031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD7")]
		[Address(RVA = "0x101259724", Offset = "0x1259724", VA = "0x101259724")]
		private void SendPlayerNameSet()
		{
		}

		// Token: 0x06001F60 RID: 8032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CD8")]
		[Address(RVA = "0x101259918", Offset = "0x1259918", VA = "0x101259918")]
		public void OnResponsePlayerRequest(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x04002F99 RID: 12185
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002488")]
		private MenuState_UserInfomation.eStep m_Step;

		// Token: 0x04002F9A RID: 12186
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002489")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002F9B RID: 12187
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400248A")]
		private MenuUserInfomationUI m_UI;

		// Token: 0x020007E9 RID: 2025
		[Token(Token = "0x2000EBD")]
		private enum eStep
		{
			// Token: 0x04002F9D RID: 12189
			[Token(Token = "0x4005E88")]
			None = -1,
			// Token: 0x04002F9E RID: 12190
			[Token(Token = "0x4005E89")]
			First,
			// Token: 0x04002F9F RID: 12191
			[Token(Token = "0x4005E8A")]
			LoadWait,
			// Token: 0x04002FA0 RID: 12192
			[Token(Token = "0x4005E8B")]
			PlayIn,
			// Token: 0x04002FA1 RID: 12193
			[Token(Token = "0x4005E8C")]
			Main,
			// Token: 0x04002FA2 RID: 12194
			[Token(Token = "0x4005E8D")]
			UnloadChildSceneWait
		}
	}
}
