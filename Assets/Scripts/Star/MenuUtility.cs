﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008B7 RID: 2231
	[Token(Token = "0x2000661")]
	[StructLayout(3)]
	public static class MenuUtility
	{
		// Token: 0x06002467 RID: 9319 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600218B")]
		[Address(RVA = "0x101259BC8", Offset = "0x1259BC8", VA = "0x101259BC8")]
		public static List<int> GetAvailableADVLibrary(eADVLibraryCategory category)
		{
			return null;
		}

		// Token: 0x06002468 RID: 9320 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600218C")]
		[Address(RVA = "0x10125A014", Offset = "0x125A014", VA = "0x10125A014")]
		public static List<int> GetAvailableADVLibrary(eADVLibraryCategory category, int part)
		{
			return null;
		}

		// Token: 0x06002469 RID: 9321 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600218D")]
		[Address(RVA = "0x10125A210", Offset = "0x125A210", VA = "0x10125A210")]
		public static List<int> GetAvailableADVPartLibrary(eADVLibraryCategory category)
		{
			return null;
		}

		// Token: 0x0600246A RID: 9322 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600218E")]
		[Address(RVA = "0x101259E18", Offset = "0x1259E18", VA = "0x101259E18")]
		public static List<int> GetAvailableADVList(int libraryID)
		{
			return null;
		}

		// Token: 0x0600246B RID: 9323 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600218F")]
		[Address(RVA = "0x10125A430", Offset = "0x125A430", VA = "0x10125A430")]
		public static List<int> GetAvailableSingleADVList(eCharaNamedType namedType)
		{
			return null;
		}

		// Token: 0x0600246C RID: 9324 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002190")]
		[Address(RVA = "0x10125A614", Offset = "0x125A614", VA = "0x10125A614")]
		public static List<int> GetAvailableCrossADVList(eCharaNamedType namedType)
		{
			return null;
		}

		// Token: 0x0600246D RID: 9325 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002191")]
		[Address(RVA = "0x10125A7EC", Offset = "0x125A7EC", VA = "0x10125A7EC")]
		public static List<int> GetAvailableWeaponADVList(int charaID)
		{
			return null;
		}

		// Token: 0x0600246E RID: 9326 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002192")]
		[Address(RVA = "0x10125A960", Offset = "0x125A960", VA = "0x10125A960")]
		public static List<int> GetAvailableWeaponADVList(eCharaNamedType named)
		{
			return null;
		}

		// Token: 0x0600246F RID: 9327 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002193")]
		[Address(RVA = "0x10125ABB0", Offset = "0x125ABB0", VA = "0x10125ABB0")]
		public static List<int> GetOccurredWeaponAdv(int charaID)
		{
			return null;
		}

		// Token: 0x06002470 RID: 9328 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002194")]
		[Address(RVA = "0x10125AD4C", Offset = "0x125AD4C", VA = "0x10125AD4C")]
		public static List<int> GetOccurredWeaponAdv(eCharaNamedType named)
		{
			return null;
		}

		// Token: 0x06002471 RID: 9329 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002195")]
		[Address(RVA = "0x10125AF9C", Offset = "0x125AF9C", VA = "0x10125AF9C")]
		public static string GetLockTextCharaADV(int advid)
		{
			return null;
		}

		// Token: 0x06002472 RID: 9330 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002196")]
		[Address(RVA = "0x10125B5D4", Offset = "0x125B5D4", VA = "0x10125B5D4")]
		public static List<int> GetNewADVIDList()
		{
			return null;
		}

		// Token: 0x06002473 RID: 9331 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002197")]
		[Address(RVA = "0x10125B7A8", Offset = "0x125B7A8", VA = "0x10125B7A8")]
		public static List<FlavorsDB_Param> GetLibraryFlavorList(OfferManager.SubOffer subOffer, eCharaNamedType named, bool checkState)
		{
			return null;
		}
	}
}
