﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008D9 RID: 2265
	[Token(Token = "0x2000681")]
	[StructLayout(3)]
	public class MissionDataDB : ScriptableObject
	{
		// Token: 0x06002516 RID: 9494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002238")]
		[Address(RVA = "0x10125BAEC", Offset = "0x125BAEC", VA = "0x10125BAEC")]
		public MissionDataDB()
		{
		}

		// Token: 0x04003558 RID: 13656
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002724")]
		public MissionDataDB_Param[] m_Params;
	}
}
