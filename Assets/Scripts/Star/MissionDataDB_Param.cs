﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008D8 RID: 2264
	[Token(Token = "0x2000680")]
	[Serializable]
	[StructLayout(0, Size = 72)]
	public struct MissionDataDB_Param
	{
		// Token: 0x04003546 RID: 13638
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002712")]
		public int m_ID;

		// Token: 0x04003547 RID: 13639
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002713")]
		public ushort m_Category;

		// Token: 0x04003548 RID: 13640
		[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
		[Token(Token = "0x4002714")]
		public ushort m_ActionNo;

		// Token: 0x04003549 RID: 13641
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002715")]
		public int m_ExtentionScriptID;

		// Token: 0x0400354A RID: 13642
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4002716")]
		public int m_ExtentionFunKey;

		// Token: 0x0400354B RID: 13643
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002717")]
		public string m_TargetMessage;

		// Token: 0x0400354C RID: 13644
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002718")]
		public int m_RewardType;

		// Token: 0x0400354D RID: 13645
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002719")]
		public int m_RewardID;

		// Token: 0x0400354E RID: 13646
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400271A")]
		public int m_RewardNum;

		// Token: 0x0400354F RID: 13647
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400271B")]
		public int m_LimitTime;

		// Token: 0x04003550 RID: 13648
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400271C")]
		public int m_StartYear;

		// Token: 0x04003551 RID: 13649
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400271D")]
		public short m_StartMonth;

		// Token: 0x04003552 RID: 13650
		[Cpp2IlInjected.FieldOffset(Offset = "0x2E")]
		[Token(Token = "0x400271E")]
		public short m_StartDay;

		// Token: 0x04003553 RID: 13651
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400271F")]
		public int m_EndDays;

		// Token: 0x04003554 RID: 13652
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002720")]
		public int m_ListUpNo;

		// Token: 0x04003555 RID: 13653
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002721")]
		public int m_MakeUpType;

		// Token: 0x04003556 RID: 13654
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002722")]
		public int m_SubCode;

		// Token: 0x04003557 RID: 13655
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002723")]
		public ushort m_PopupTiming;
	}
}
