﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class MissionFuncBattle : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            switch ((eXlsMissionBattleFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionBattleFuncType.QuestClear:
                    MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    result = LogicMission.CheckBattleQuestClear(value);
                    break;
                case eXlsMissionBattleFuncType.QuestClearEventType:
                    break;
                case eXlsMissionBattleFuncType.SpecificQuestIdClearCount:
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            switch ((eXlsMissionBattleFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionBattleFuncType.QuestClear:
                    MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    MissionBattleQuestClearCondition clearCondition = new MissionBattleQuestClearCondition(true, true);
                    pparam.m_Rate = LogicMission.UpdateBattleQuestClear(value, clearCondition);
                    break;
                case eXlsMissionBattleFuncType.QuestClearEventType:
                    break;
                case eXlsMissionBattleFuncType.SpecificQuestIdClearCount:
                    break;
            }
        }
    }
}
