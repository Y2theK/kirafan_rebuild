﻿using CommonLogic;
using System.Collections.Generic;
using System.Linq;

namespace Star {
    public class MissionFuncChara : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            switch ((eXlsMissionCharaFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionCharaFuncType.FriendShip:
                    MissionCharaFriendShipValue friendValue = new MissionCharaFriendShipValue(pparam.m_Rate, pparam.m_RateBase, (int)pparam.m_SubCodeID);
                    result = LogicMission.CheckCharaFriendShip(friendValue, (MissionCharaFriendShipCondition)condition);
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            switch ((eXlsMissionCharaFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionCharaFuncType.FriendShip:
                    UserDataManager userData = GameSystem.Inst.UserDataMng;
                    List<eCharaNamedType> namedTypes = userData.UserNamedDatas.Keys.ToList();
                    int[] ids = new int[namedTypes.Count];
                    int[] lvs = new int[namedTypes.Count];
                    for (int i = 0; i < namedTypes.Count; i++) {
                        ids[i] = (int)namedTypes[i];
                        lvs[i] = userData.GetUserNamedData(namedTypes[i]).FriendShip;
                    }
                    //TODO: doesn't use the calculated values for some reason
                    MissionCharaFriendShipValue friendValue = new MissionCharaFriendShipValue(pparam.m_Rate, pparam.m_RateBase, (int)pparam.m_SubCodeID);
                    pparam.m_Rate = LogicMission.UpdateCharaFriendShip(friendValue, (MissionCharaFriendShipCondition)condition);
                    break;
            }
        }
    }
}
