﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class MissionFuncEdit : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionEditFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionEditFuncType.BattlePartyEdit:
                    result = LogicMission.CheckEditBattlePartyEdit(value);
                    break;
                case eXlsMissionEditFuncType.SupportPartyEdit:
                    result = LogicMission.CheckEditSupportPartyEdit(value);
                    break;
                case eXlsMissionEditFuncType.CharaStorength:
                    result = LogicMission.CheckEditCharaStrength(value);
                    break;
                case eXlsMissionEditFuncType.CharaLimitBreak:
                    result = LogicMission.CheckEditCharaLimitBreak(value);
                    break;
                case eXlsMissionEditFuncType.CharaEvelution:
                    break;
                case eXlsMissionEditFuncType.WeaponMake:
                    result = LogicMission.CheckEditWeaponMake(value);
                    break;
                case eXlsMissionEditFuncType.WeaponStorength:
                    result = LogicMission.CheckEditWeaponStrength(value);
                    break;
                case eXlsMissionEditFuncType.WeaponEquipment:
                    result = LogicMission.CheckEditWeaponEquipment(value);
                    break;
                case eXlsMissionEditFuncType.SpecificCharaRank:
                    break;
                case eXlsMissionEditFuncType.SpecificCharaLimitBreak:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaRank:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaLimitBreak:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaEvelution:
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionEditFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionEditFuncType.BattlePartyEdit:
                    pparam.m_Rate = LogicMission.UpdateEditBattlePartyEdit(value);
                    break;
                case eXlsMissionEditFuncType.SupportPartyEdit:
                    pparam.m_Rate = LogicMission.UpdateEditSupportPartyEdit(value);
                    break;
                case eXlsMissionEditFuncType.CharaStorength:
                    pparam.m_Rate = LogicMission.UpdateEditCharaStrength(value);
                    break;
                case eXlsMissionEditFuncType.CharaLimitBreak:
                    pparam.m_Rate = LogicMission.UpdateEditCharaLimitBreak(value);
                    break;
                case eXlsMissionEditFuncType.CharaEvelution:
                    break;
                case eXlsMissionEditFuncType.WeaponMake:
                    pparam.m_Rate = LogicMission.UpdateEditWeaponMake(value);
                    break;
                case eXlsMissionEditFuncType.WeaponStorength:
                    pparam.m_Rate = LogicMission.UpdateEditWeaponStrength(value);
                    break;
                case eXlsMissionEditFuncType.WeaponEquipment:
                    pparam.m_Rate = LogicMission.UpdateEditWeaponEquipment(value, (MissionEditWeaponEquipCondition)condition);
                    break;
                case eXlsMissionEditFuncType.SpecificCharaRank:
                    break;
                case eXlsMissionEditFuncType.SpecificCharaLimitBreak:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaRank:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaLimitBreak:
                    break;
                case eXlsMissionEditFuncType.SpecificRarityCharaEvelution:
                    break;
            }
        }
    }
}
