﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class MissionFuncRoom : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionRoomFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionRoomFuncType.ObjectSet:
                    result = LogicMission.CheckRoomObjectSet(value);
                    break;
                case eXlsMissionRoomFuncType.ObjectRemove:
                    result = LogicMission.CheckRoomObjectRemove(value);
                    break;
                case eXlsMissionRoomFuncType.CharaTouch:
                    result = LogicMission.CheckRoomCharaTouch(value);
                    break;
                case eXlsMissionRoomFuncType.Visit:
                    result = LogicMission.CheckRoomVisit(value);
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionRoomFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionRoomFuncType.ObjectSet:
                    pparam.m_Rate = LogicMission.UpdateRoomObjectSet(value);
                    break;
                case eXlsMissionRoomFuncType.ObjectRemove:
                    pparam.m_Rate = LogicMission.UpdateRoomObjectRemove(value);
                    break;
                case eXlsMissionRoomFuncType.CharaTouch:
                    pparam.m_Rate = LogicMission.UpdateRoomCharaTouch(value);
                    break;
                case eXlsMissionRoomFuncType.Visit:
                    pparam.m_Rate = LogicMission.UpdateRoomVisit(value);
                    break;
            }
        }
    }
}
