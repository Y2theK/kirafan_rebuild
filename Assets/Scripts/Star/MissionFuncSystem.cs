﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class MissionFuncSystem : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            switch ((eXlsMissionSystemFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionSystemFuncType.Login:
                    MissionValue loginValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    result = LogicMission.CheckSystemLogin(loginValue, (MissionSystemCondition)condition);
                    break;
                case eXlsMissionSystemFuncType.ContinutiyLogin:
                    MissionValue contLoginValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    result = LogicMission.CheckSystemContinutiyLogin(contLoginValue, (MissionSystemCondition)condition);
                    break;
                case eXlsMissionSystemFuncType.MissionAllComplete:
                    List<MissionAllCompleteCondition> conditions = new List<MissionAllCompleteCondition>();
                    for (int i = 0; i < plist.Count; i++) {
                        conditions.Add(new MissionAllCompleteCondition((int)plist[i].m_Type, (int)plist[i].m_State));
                    }
                    MissionAllCompleteValue acValue = new MissionAllCompleteValue(pparam.m_Rate, pparam.m_RateBase, (int)pparam.m_Type);
                    result = LogicMission.CheckSystemMissionAllComplete(acValue, conditions);
                    break;
                case eXlsMissionSystemFuncType.UserRank:
                    MissionValue rankValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    result = LogicMission.CheckSystemUserRank(rankValue, (MissionSystemCondition)condition);
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            switch ((eXlsMissionSystemFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionSystemFuncType.Login:
                    MissionValue loginValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    pparam.m_Rate = LogicMission.UpdateSystemLogin(loginValue, (MissionSystemCondition)condition);
                    break;
                case eXlsMissionSystemFuncType.ContinutiyLogin:
                    MissionValue contLoginValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    pparam.m_Rate = LogicMission.UpdateSystemContinutiyLogin(contLoginValue, (MissionSystemCondition)condition);
                    break;
                case eXlsMissionSystemFuncType.MissionAllComplete:
                    List<MissionAllCompleteCondition> conditions = new List<MissionAllCompleteCondition>();
                    for (int i = 0; i < plist.Count; i++) {
                        conditions.Add(new MissionAllCompleteCondition((int)plist[i].m_Type, (int)plist[i].m_State));
                    }
                    MissionAllCompleteValue acValue = new MissionAllCompleteValue(pparam.m_Rate, pparam.m_RateBase, (int)pparam.m_Type);
                    pparam.m_Rate = LogicMission.UpdateSystemMissionAllComplete(acValue, conditions);
                    break;
                case eXlsMissionSystemFuncType.UserRank:
                    MissionValue rankValue = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
                    pparam.m_Rate = LogicMission.UpdateSystemUserRank(rankValue, (MissionSystemCondition)condition);
                    break;
            }
        }
    }
}
