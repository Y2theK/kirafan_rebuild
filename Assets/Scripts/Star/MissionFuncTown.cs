﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class MissionFuncTown : IMissionFunction {
        public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            bool result = false;
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionTownFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionTownFuncType.Building:
                    break;
                case eXlsMissionTownFuncType.Move:
                    break;
                case eXlsMissionTownFuncType.Remove:
                    break;
                case eXlsMissionTownFuncType.CharaTouchItem:
                    result = LogicMission.CheckTownCharaTouchItem(value);
                    break;
                case eXlsMissionTownFuncType.ContentBuild:
                    result = LogicMission.CheckTownContentBuild(value);
                    break;
                case eXlsMissionTownFuncType.BufBuild:
                    result = LogicMission.CheckTownBufBuild(value);
                    break;
                case eXlsMissionTownFuncType.BuildTouchItem:
                    break;
                case eXlsMissionTownFuncType.IconTap:
                    result = LogicMission.CheckTownIconTap(value);
                    break;
                case eXlsMissionTownFuncType.ProductBuildCount:
                    break;
                case eXlsMissionTownFuncType.StrengBuildCount:
                    break;
            }
            return result;
        }

        public void Update(IMissionPakage pparam, List<IMissionPakage> plist, MissionConditionBase condition) {
            MissionValue value = new MissionValue(pparam.m_Rate, pparam.m_RateBase);
            switch ((eXlsMissionTownFuncType)pparam.m_ExtensionScriptID) {
                case eXlsMissionTownFuncType.Building:
                    break;
                case eXlsMissionTownFuncType.Move:
                    break;
                case eXlsMissionTownFuncType.Remove:
                    break;
                case eXlsMissionTownFuncType.CharaTouchItem:
                    pparam.m_Rate = LogicMission.UpdateTownCharaTouchItem(value);
                    break;
                case eXlsMissionTownFuncType.ContentBuild:
                    pparam.m_Rate = LogicMission.UpdateTownContentBuild(value);
                    break;
                case eXlsMissionTownFuncType.BufBuild:
                    pparam.m_Rate = LogicMission.UpdateTownBufBuild(value, (MissionTownBuildCondition)condition);
                    break;
                case eXlsMissionTownFuncType.BuildTouchItem:
                    break;
                case eXlsMissionTownFuncType.IconTap:
                    pparam.m_Rate = LogicMission.UpdateTownIconTap(value);
                    break;
                case eXlsMissionTownFuncType.ProductBuildCount:
                    break;
                case eXlsMissionTownFuncType.StrengBuildCount:
                    break;
            }
        }
    }
}
