﻿using CommonLogic;
using Meige;
using Star.UI.Mission;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star {
    public class MissionManager {
        public static MissionManager Inst;

        private readonly string PanelMissionUIPrefabPath = "Prefab/UI/PanelMissionUIPrefab" + MeigeResourceManager.AB_EXTENSION;

        private MeigeResource.Handler m_ResourceHandlerPanelMissionUI;
        private eStep m_Step = eStep.None;
        private ePrepareStep m_PrepareStep = ePrepareStep.None;
        private MissionUI m_MissionUI;
        private bool m_IsOpenUI;
        private MissionServerApi m_MissionApi;
        private GameObject m_PopUpModel;
        private List<IMissionPakage> m_PopupList = new List<IMissionPakage>();
        private MissionPopupControll m_PopUpWindow;
        private ABResourceLoader m_Loader;
        private ABResourceObjectHandler m_LoadingHndl;
        private bool m_IsReserveGetAll;
        private bool m_IsReserveGetAllOnLoadOpen;

        public void Prepare() {
            if (Inst == null) {
                Inst = this;
            }
            if (IsDonePrepare()) {
                return;
            }
            m_MissionApi = MissionServerApi.CreateServer();
            if (m_Loader == null) {
                m_Loader = new ABResourceLoader();
            }
            m_LoadingHndl = m_Loader.Load("prefab/ui/missionprecomp" + MeigeResourceManager.AB_EXTENSION);
            m_PopupList = new List<IMissionPakage>();
            m_PrepareStep = ePrepareStep.Preparing;
            m_IsReserveGetAll = false;
            m_IsReserveGetAllOnLoadOpen = false;
            ReleaseRefreshCallback();
        }

        public bool IsDonePrepare() {
            return m_MissionApi != null && m_MissionApi.IsAvailable() && m_PopUpModel != null && m_PrepareStep == ePrepareStep.DonePrepare;
        }

        public void ForceResetOnReturnTitle() {
            if (m_MissionApi != null) {
                MissionServerApi.DestroyServer();
            }
            if (m_LoadingHndl != null) {
                if (m_Loader == null) { //TODO: should definitely be !=
                    m_Loader.Unload(m_LoadingHndl);
                }
                m_LoadingHndl = null;
            }
            m_PopupList.Clear();
            m_PrepareStep = ePrepareStep.None;
            m_MissionUI = null;
            m_IsOpenUI = false;
            UnloadBeginnersMissionUI();
            m_IsReserveGetAll = false;
            m_IsReserveGetAllOnLoadOpen = false;
            m_Step = eStep.None;
        }

        private void Preparing() {
            if (m_LoadingHndl != null && m_LoadingHndl.IsDone()) {
                if (m_LoadingHndl.IsError()) {
                    m_LoadingHndl.Retry();
                } else {
                    m_PopUpModel = m_LoadingHndl.FindObj("MissionPreComp") as GameObject;
                    m_Loader.Unload(m_LoadingHndl);
                    m_LoadingHndl = null;
                    m_PrepareStep = ePrepareStep.DonePrepare;
                }
            }
        }

        public void UnlockAction(eXlsMissionSeg fcategory, object factionno, MissionConditionBase condition) {
            m_MissionApi.UnlockAction(fcategory, (int)factionno, condition);
        }

        public void CompleteMission(long fcompmangeid) {
            m_MissionApi.CompleteMission(fcompmangeid);
        }

        public void EntryRefreshCallback(Action pcallback) {
            m_MissionApi.EntryRefreshCallback(pcallback);
        }

        public void ReleaseRefreshCallback() {
            m_MissionApi.ReleaseRefreshCallback();
        }

        public int GetNewListUpNum() {
            return m_MissionApi.GetPreCompNum();
        }

        public int GetNewListUpNum(eMissionCategory category) {
            return m_MissionApi.GetPreCompNum(category);
        }

        public MissionUIDataPack GetCategoryToMissionUIData(eMissionCategory Category) {
            List<IMissionPakage> mList = m_MissionApi.GetMissionList();
            MissionUIDataPack pack = new MissionUIDataPack();
            List<IMissionPakage> catList = new List<IMissionPakage>();
            for (int i = 0; i < mList.Count; i++) {
                if (mList[i].m_Type == Category) {
                    catList.Add(mList[i]);
                }
            }
            pack.m_Table = new IMissionUIData[catList.Count];
            for (int i = 0; i < catList.Count; i++) {
                pack.m_Table[i] = new IMissionUIData();
                pack.m_Table[i].m_Type = catList[i].m_Type;
                pack.m_Table[i].m_TargetMessage = catList[i].GetTargetMessage();
                pack.m_Table[i].m_RewardList = catList[i].m_RewardList;
                pack.m_Table[i].m_DatabaseID = catList[i].m_MissionID;
                pack.m_Table[i].m_ManageID = catList[i].m_KeyManageID;
                pack.m_Table[i].m_Rate = catList[i].m_Rate;
                pack.m_Table[i].m_RateMax = catList[i].m_RateBase;
                pack.m_Table[i].m_State = catList[i].m_State;
                pack.m_Table[i].m_LimitTime = catList[i].m_LimitTime;
                pack.m_Table[i].m_uiPriority = catList[i].m_uiPriority;
            }
            return pack;
        }

        public void OpenUI() {
            if (!m_IsOpenUI) {
                GameSystem.Inst.InputBlock.SetBlockObj(GameSystem.Inst.gameObject, true);
                m_IsOpenUI = true;
                m_Step = eStep.First;
            }
        }

        public void CloseUI() {
            if (m_MissionUI != null) {
                m_MissionUI.PlayOut();
            }
        }

        public bool IsOpenUI() {
            return m_IsOpenUI;
        }

        public void Update() {
            if (m_Loader != null) {
                m_Loader.Update();
            }
            if (m_PrepareStep == ePrepareStep.Preparing) {
                Preparing();
                return;
            }
            switch (m_Step) {
                case eStep.First:
                    SceneLoader.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.MissionUI);
                    if (!m_MissionApi.IsSending()) {
                        RequestAdditionalGetAll();
                        m_Step = eStep.LoadWait;
                    }
                    break;
                case eStep.LoadWait:
                    if (m_MissionApi.IsListUp()) {
                        if (SceneLoader.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.MissionUI)) {
                            SceneLoader.Inst.ActivationChildScene(SceneDefine.eChildSceneID.MissionUI);
                            m_Step = eStep.PlayIn;
                        }
                    }
                    break;
                case eStep.PlayIn:
                    string name = SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.MissionUI].m_SceneName;
                    m_MissionUI = UIUtility.GetMenuComponent<MissionUI>(name);
                    if (m_MissionUI != null) {
                        m_MissionUI.Setup();
                        m_MissionUI.PlayIn();
                        GameSystem.Inst.InputBlock.SetBlockObj(GameSystem.Inst.gameObject, false);
                        m_Step = eStep.Main;
                    }
                    break;
                case eStep.Main:
                    if (m_MissionUI == null) {
                        m_IsOpenUI = false;
                        m_Step = eStep.None;
                    } else if (m_MissionUI.IsMenuEnd()) {
                        Resources.UnloadUnusedAssets();
                        m_MissionUI = null;
                        GameSystem.Inst.SceneLoader.UnloadChildSceneAsync(SceneDefine.eChildSceneID.MissionUI);
                        m_Step = eStep.UnloadChildSceneWait;
                    }
                    break;
                case eStep.UnloadChildSceneWait:
                    if (GameSystem.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.MissionUI)) {
                        m_Step = eStep.End;
                    }
                    break;
                case eStep.End:
                    if (GameSystem.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.MissionUI)) {
                        m_IsOpenUI = false;
                        m_Step = eStep.None;
                    }
                    break;
            }
            if (GameSystem.Inst != null && GameSystem.Inst.IsAvailable()) {
                if (!GameSystem.Inst.LoadingUI.IsOverlay()) {
                    if (m_PopUpModel != null) {
                        if (m_PopUpWindow == null) {
                            if (m_PopupList.Count > 0) {
                                IMissionPakage pack = m_PopupList[0];
                                if (pack != null) {
                                    GameObject gameObject = UnityEngine.Object.Instantiate(m_PopUpModel);
                                    UnityEngine.Object.DontDestroyOnLoad(gameObject);
                                    m_PopUpWindow = gameObject.AddComponent<MissionPopupControll>();
                                    m_PopUpWindow.SetUp(pack);
                                    m_PopupList.RemoveAt(0);
                                }
                            }
                        } else if (m_PopUpWindow.IsEnd()) {
                            m_PopUpWindow.SetAutoDestroy();
                            m_PopUpWindow = null;
                        }
                    }
                }
            }
        }

        public void ReserveGetAll(bool includeLoadOpen = true) {
            m_IsReserveGetAll = true;
            m_IsReserveGetAllOnLoadOpen = includeLoadOpen;
        }

        public void ExecuteReserveGetAll() {
            if (m_IsReserveGetAll) {
                RequestAdditionalGetAll();
            }
        }

        public void ExecuteReserveGetAllOnLoadOpen() {
            if (m_IsReserveGetAllOnLoadOpen) {
                RequestAdditionalGetAll();
            }
        }

        public void RequestAdditionalGetAll() {
            m_MissionApi.RefreshMissionLogList();
            m_IsReserveGetAll = false;
            m_IsReserveGetAllOnLoadOpen = false;
        }

        public void ExecutePopup() {
            if (m_MissionApi != null) {
                while (m_MissionApi.IsExistNewComp(true)) {
                    IMissionPakage pack = m_MissionApi.GetNewCompMission(true);
                    if (pack.m_Type == eMissionCategory.Beginner) {
                        int idx = 0;
                        for (int i = 0; i < m_PopupList.Count; i++) {
                            if (m_PopupList[i].m_Type != eMissionCategory.Beginner) {
                                idx = i;
                                break;
                            }
                        }
                        m_PopupList.Insert(idx, pack);
                    } else {
                        m_PopupList.Add(pack);
                    }
                    pack.m_State = eMissionState.PreComp;
                    m_MissionApi.RemoveNewCompMission(pack.m_Type, pack.m_KeyManageID);
                }
            }
        }

        public IMissionPakage GetMissionPackage(long missionMngID) {
            foreach (IMissionPakage pack in m_MissionApi.GetMissionList()) {
                if (pack.m_KeyManageID == missionMngID) {
                    return pack;
                }
            }
            return null;
        }

        public void LoadBeginnersMissionUIPrefab() {
            if (m_ResourceHandlerPanelMissionUI == null) {
                MeigeResourceManager.LoadHandler(PanelMissionUIPrefabPath, CallbackLoadBeginnersMissionUIPrefab, MeigeResource.Option.ResourceKeep);
            }
        }

        private void CallbackLoadBeginnersMissionUIPrefab(MeigeResource.Handler phandle) {
            m_ResourceHandlerPanelMissionUI = phandle;
        }

        public bool IsLoadedBeginnersMissionUIPrefab() {
            return m_ResourceHandlerPanelMissionUI != null;
        }

        public void UnloadBeginnersMissionUI() {
            if (m_ResourceHandlerPanelMissionUI != null) {
                m_ResourceHandlerPanelMissionUI.Unload();
                m_ResourceHandlerPanelMissionUI = null;
            }
        }

        public GameObject GetBeginnersMissionUIPrefab() {
            if (m_ResourceHandlerPanelMissionUI != null) {
                return m_ResourceHandlerPanelMissionUI.GetAsset<GameObject>();
            }
            return null;
        }

        public bool CheckBeginnersMissionExist() {
            foreach (IMissionPakage pack in m_MissionApi.GetMissionList()) {
                if (pack.m_Type == eMissionCategory.Beginner && pack.m_State != eMissionState.Comp) {
                    return true;
                }
            }
            return false;
        }

        public bool IsFinishedRequestCompAll() {
            return m_MissionApi.IsFinishedRequestCompAll();
        }

        private enum eStep {
            None = -1,
            First,
            LoadWait,
            PlayIn,
            Main,
            PlayOut,
            UnloadChildSceneWait,
            End
        }

        private enum ePrepareStep {
            None = -1,
            Preparing,
            DonePrepare
        }
    }
}
