﻿namespace Star {
    public class MissionPakageChara : IMissionPakage {
        public override string GetTargetMessage() {
            NamedListDB namedListDB = GameSystem.Inst.DbMng.NamedListDB;
            if (namedListDB.IsExist((eCharaNamedType)m_SubCodeID)) {
                NamedListDB_Param param = namedListDB.GetParam((eCharaNamedType)m_SubCodeID);
                return string.Format(m_TargetMessage, m_RateBase, param.m_NickName);
            }
            return string.Empty;
        }
    }
}
