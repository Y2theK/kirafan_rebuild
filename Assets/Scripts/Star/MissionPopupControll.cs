﻿using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star {
    public class MissionPopupControll : MonoBehaviour {
        private const float LISTUP_HI = 104f;
        private const float LISTUP_POINT = 60f;

        private float m_TimeUp;
        private eStep m_State;
        private Transform m_SetNode;

        public void SetUp(IMissionPakage ppakage) {
            GameObject camera = GameObject.Find("GameSystem/UICamera");
            if (camera != null) {
                Canvas canvas = camera.GetComponent<Canvas>();
                canvas.worldCamera = camera.GetComponent<Camera>();
            }
            Text text = UIUtility.FindHrcObject(transform, "TargetText", typeof(Text)) as Text;
            if (text != null) {
                text.text = ppakage.GetTargetMessage();
            }
            text = UIUtility.FindHrcObject(transform, "CategoryText", typeof(Text)) as Text;
            if (text != null) {
                switch (ppakage.m_Type) {
                    case eMissionCategory.Day:
                        text.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabNormal);
                        break;
                    case eMissionCategory.Week:
                        text.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabLimit);
                        break;
                    case eMissionCategory.Unlock:
                        text.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDay);
                        break;
                    case eMissionCategory.Event:
                        text.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDrop);
                        break;
                    case eMissionCategory.Beginner:
                        text.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabBeginners);
                        break;
                }
            }
            m_TimeUp = 0.5f;
            m_SetNode = UIUtility.FindHrcTransform(transform, "Base");
            m_SetNode.localPosition = Vector3.up * LISTUP_HI;
            m_SetNode.GetComponent<CustomButton>().AddListerner(OnClickCallBack);
            m_State = eStep.FadeIn;
        }

        public void Update() {
            m_TimeUp -= Time.deltaTime;
            if (m_TimeUp <= 0f) {
                m_TimeUp = 0f;
            }
            switch (m_State) {
                case eStep.FadeIn:
                    float inHeight = LISTUP_HI * 2f * m_TimeUp - LISTUP_POINT;
                    m_SetNode.localPosition = inHeight * Vector3.up;
                    if (m_TimeUp <= 0f) {
                        m_TimeUp = 2.5f;
                        m_State = eStep.Loop;
                    }
                    break;
                case eStep.FadeOut:
                    float outHeight = (0.5f - m_TimeUp) * LISTUP_HI * 4f - LISTUP_POINT;
                    m_SetNode.localPosition = outHeight * Vector3.up;
                    if (m_TimeUp <= 0f) {
                        m_State = eStep.End;
                    }
                    break;
                case eStep.End:
                    Destroy(gameObject);
                    break;
            }
        }

        public bool IsEnd() {
            return m_TimeUp <= 0f && m_State <= eStep.Loop;
        }

        public void SetAutoDestroy() {
            m_TimeUp = 0.5f;
            m_State = eStep.FadeOut;
        }

        private void OnClickCallBack() {
            if (m_State <= eStep.Loop) {
                SetAutoDestroy();
            }
        }

        public enum eStep {
            FadeIn,
            Loop,
            FadeOut,
            End
        }
    }
}
