﻿using CommonLogic;
using CommonResponseTypes;
using Meige;
using MISSIONResponseTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WWWTypes;

namespace Star {
    public class MissionServerApi : MonoBehaviour {
        private List<IMissionPakage> m_List = new List<IMissionPakage>();
        private Dictionary<eMissionCategory, HashSet<long>> m_NewCompList = new Dictionary<eMissionCategory, HashSet<long>>();
        private Dictionary<eMissionCategory, HashSet<long>> m_PreCompList = new Dictionary<eMissionCategory, HashSet<long>>();
        private List<long> m_SendSetList = new List<long>();
        private List<long> m_SendCompList = new List<long>();
        private bool m_SendComFlagSet;
        private bool m_SendComFlagComp;
        private Action m_Callback;
        private bool m_Refresh;
        private bool m_NewListUpKey;
        private eStep m_Step = eStep.None;

        public static MissionServerApi CreateServer() {
            if (!GameSystem.Inst.TryGetComponent(out MissionServerApi serverApi)) {
                serverApi = GameSystem.Inst.gameObject.AddComponent<MissionServerApi>();
            }
            serverApi._CreateServer();
            return serverApi;
        }

        private void _CreateServer() {
            m_Step = eStep.Init;
        }

        public static void DestroyServer() {
            if (GameSystem.Inst.TryGetComponent(out MissionServerApi serverApi)) {
                serverApi._DestroyServer();
            }
        }

        private void _DestroyServer() {
            NetworkQueueManager.SetSystemRecvCallback(null);
            m_List.Clear();
            m_NewCompList.Clear();
            m_PreCompList.Clear();
            m_SendSetList.Clear();
            m_SendCompList.Clear();
            m_SendComFlagSet = false;
            m_SendComFlagComp = false;
            m_Refresh = false;
            m_NewListUpKey = false;
            m_Step = eStep.None;
        }

        public bool IsAvailable() {
            return m_Step >= eStep.Calc;
        }

        public bool IsSending() {
            return m_SendComFlagSet || m_SendComFlagComp;
        }

        public List<IMissionPakage> GetMissionList() {
            return m_List;
        }

        private Dictionary<eMissionCategory, HashSet<long>> MakeCompatibleDictionary(Dictionary<eMissionCategory, HashSet<long>> srcDict, bool isFull) {
            Dictionary<eMissionCategory, HashSet<long>> newDict = new Dictionary<eMissionCategory, HashSet<long>>(5);
            foreach (KeyValuePair<eMissionCategory, HashSet<long>> kvp in srcDict) {
                if (isFull || kvp.Key < eMissionCategory.Beginner) {
                    newDict[kvp.Key] = new HashSet<long>(kvp.Value);
                }
            }
            return newDict;
        }

        public bool IsExistNewComp(bool isFull = false) {
            Dictionary<eMissionCategory, HashSet<long>> dict = MakeCompatibleDictionary(m_NewCompList, isFull);
            foreach (KeyValuePair<eMissionCategory, HashSet<long>> kvp in dict) {
                if (kvp.Value != null && kvp.Value.Count > 0) {
                    return true;
                }
            }
            return false;
        }

        public int GetPreCompNum(bool isFull = false) {
            int num = 0;
            Dictionary<eMissionCategory, HashSet<long>> dict = MakeCompatibleDictionary(m_NewCompList, isFull);
            foreach (KeyValuePair<eMissionCategory, HashSet<long>> kvp in dict) {
                if (kvp.Value != null) {
                    num += kvp.Value.Count;
                }
            }
            return num;
        }

        public int GetPreCompNum(eMissionCategory category) {
            if (m_PreCompList.ContainsKey(category) && m_PreCompList[category] != null) {
                return m_PreCompList[category].Count;
            }
            return 0;
        }

        private void AddNewCompList(eMissionCategory category, long managedID) {
            AddCompList(m_NewCompList, category, managedID);
        }

        private void RemoveNewCompList(eMissionCategory category, long managedID) {
            RemoveCompList(m_NewCompList, category, managedID);
        }

        private void AddPreCompList(eMissionCategory category, long managedID) {
            AddCompList(m_PreCompList, category, managedID);
        }

        private void RemovePreCompList(eMissionCategory category, long managedID) {
            RemoveCompList(m_PreCompList, category, managedID);
        }

        private void AddCompList(Dictionary<eMissionCategory, HashSet<long>> targetDict, eMissionCategory category, long managedID) {
            if (!targetDict.ContainsKey(category)) {
                targetDict[category] = new HashSet<long>();
            }
            targetDict[category].Add(managedID);
        }

        private void RemoveCompList(Dictionary<eMissionCategory, HashSet<long>> targetDict, eMissionCategory category, long managedID) {
            if (targetDict.ContainsKey(category)) {
                targetDict[category].Remove(managedID);
            }
        }

        public bool IsListUp() {
            return m_Step >= eStep.Calc;
        }

        private void CheckSendStateEntry(long fmanageid) {
            for (int i = 0; i < m_SendSetList.Count; i++) {
                if (m_SendSetList[i] == fmanageid) {
                    return;
                }
            }
            m_SendSetList.Add(fmanageid);
        }

        public IMissionPakage GetNewCompMission(bool isFull = false) {
            Dictionary<eMissionCategory, HashSet<long>> dict = MakeCompatibleDictionary(m_NewCompList, isFull);
            return dict.SelectMany(kvp => kvp.Value).Select(id => m_List.Find(mp => mp.m_KeyManageID == id)).FirstOrDefault(); ;
        }

        public void RemoveNewCompMission(eMissionCategory category, long missionManageID) {
            RemoveCompList(m_NewCompList, category, missionManageID);
        }

        public void UnlockAction(eXlsMissionSeg fcategory, int factionno, MissionConditionBase condition) {
            IMissionFunction missionFunction = null;
            switch (fcategory) { //TODO: other categories are unused. unfinished code?
                case eXlsMissionSeg.Room:
                    missionFunction = new MissionFuncRoom();
                    break;
            }
            if (missionFunction != null) {
                int count = m_List.Count;
                for (int i = 0; i < count; i++) {
                    IMissionPakage package = m_List[i];
                    if (package.m_ActionCategory == fcategory && package.m_ExtensionScriptID == factionno && package.m_State == eMissionState.Non) {
                        missionFunction.Update(package, m_List, condition);
                        if (missionFunction.CheckUnlock(package, m_List, condition)) {
                            package.m_State = eMissionState.PreComp;
                            AddNewCompList(package.m_Type, package.m_KeyManageID);
                            AddPreCompList(package.m_Type, package.m_KeyManageID);
                        }
                        CheckSendStateEntry(package.m_KeyManageID);
                    }
                }
                if (MissionManager.Inst != null) {
                    MissionManager.Inst.ExecutePopup();
                }
            }
        }

        public void CompleteMission(long fcompmanageid) {
            for (int i = 0; i < m_List.Count; i++) {
                if (m_List[i].m_KeyManageID == fcompmanageid) {
                    m_List[i].m_State = eMissionState.Comp;
                    RemoveNewCompList(m_List[i].m_Type, m_List[i].m_KeyManageID);
                    m_SendCompList.Add(m_List[i].m_KeyManageID);
                    break;
                }
            }
        }

        private void UpMissionLogElement(IMissionPakage pelement, PlayerMissionLog plog) {
            pelement.m_MissionID = plog.missionID;
            pelement.m_KeyManageID = plog.managedMissionId;
            pelement.m_SubCodeID = plog.subCode;
            pelement.m_uiPriority = plog.uiPriority;
            pelement.m_State = plog.state switch {
                0 => eMissionState.Non,
                1 => eMissionState.PrePop,
                2 => eMissionState.PreComp,
                _ => eMissionState.Comp,
            };
            switch (plog.category) {
                case 0:
                    pelement.m_Type = eMissionCategory.Day;
                    break;
                case 1:
                    pelement.m_Type = eMissionCategory.Week;
                    break;
                case 2:
                case 4:
                    pelement.m_Type = eMissionCategory.Unlock;
                    break;
                case 3:
                    pelement.m_Type = eMissionCategory.Event;
                    break;
                case 5:
                    pelement.m_Type = eMissionCategory.Beginner;
                    break;
            }
            pelement.m_ActionCategory = (eXlsMissionSeg)plog.missionSegType;
            pelement.m_ExtensionScriptID = plog.missionFuncType;
            if (plog.reward != null && plog.reward.Length > 0) {
                pelement.m_RewardList.Clear();
                for (int i = 0; i < plog.reward.Length; i++) {
                    IMissionPakage.IMissionReward reward = new IMissionPakage.IMissionReward();
                    switch (plog.reward[i].RewardType) {
                        case 0:
                            reward.m_Category = eMissionRewardCategory.Non;
                            break;
                        case 1:
                            reward.m_Category = eMissionRewardCategory.Money;
                            break;
                        case 2:
                            reward.m_Category = eMissionRewardCategory.Item;
                            break;
                        case 3:
                            reward.m_Category = eMissionRewardCategory.KRRPoint;
                            break;
                        case 4:
                            reward.m_Category = eMissionRewardCategory.Stamina;
                            break;
                        case 5:
                            reward.m_Category = eMissionRewardCategory.Friendship;
                            break;
                        case 6:
                            reward.m_Category = eMissionRewardCategory.Gem;
                            break;
                    }
                    reward.m_ItemNo = plog.reward[i].RewardNo;
                    reward.m_Num = plog.reward[i].RewardNum;
                    pelement.m_RewardList.Add(reward);
                }
            }
            pelement.m_Rate = plog.rate;
            pelement.m_RateBase = plog.rateMax;
            if (pelement.m_State != eMissionState.Comp) {
                pelement.m_State = pelement.m_Rate >= pelement.m_RateBase ? eMissionState.PrePop : eMissionState.Non;
            }
            pelement.m_LimitTime = plog.limitTime;
            pelement.m_TransitScene = (eMissionTransit)plog.transitScene;
            pelement.m_TransitSceneParam = plog.transitParam;
        }

        private IMissionPakage CreateMissionPakage(eXlsMissionCategory type) {
            if (type == eXlsMissionCategory.CharaUp) {
                return new MissionPakageChara();
            }
            return new IMissionPakage();
        }

        private void GetAllToList(PlayerMissionLog[] ptbl) {
            m_List.Clear();
            m_PreCompList.Clear();
            m_NewCompList.Clear();
            for (int i = 0; i < ptbl.Length; i++) {
                IMissionPakage pack = CreateMissionPakage((eXlsMissionCategory)ptbl[i].category);
                UpMissionLogElement(pack, ptbl[i]);
                if (pack.m_State == eMissionState.PrePop) {
                    pack.m_State = eMissionState.PreComp;
                }
                if (pack.m_State == eMissionState.PreComp) {
                    AddPreCompList(pack.m_Type, pack.m_KeyManageID);
                }
                m_List.Add(pack);
            }
        }

        private void RefreshAllToList(PlayerMissionLog[] ptbl) {
            m_NewListUpKey = false;
            for (int i = 0; i < m_List.Count; i++) {
                m_List[i].m_MissionID = -1;
            }
            m_PreCompList.Clear();
            for (int i = 0; i < ptbl.Length; i++) {
                IMissionPakage pack = null;
                for (int j = 0; j < m_List.Count; j++) {
                    pack = m_List[j];
                    if (pack.m_KeyManageID == ptbl[i].managedMissionId) {
                        UpMissionLogElement(pack, ptbl[i]);
                        if (pack.m_State == eMissionState.PrePop) {
                            pack.m_State = eMissionState.PreComp;
                        }
                        break;
                    }
                }
                if (pack == null) {
                    pack = CreateMissionPakage((eXlsMissionCategory)ptbl[i].category);
                    UpMissionLogElement(pack, ptbl[i]);
                    m_List.Add(pack);
                }
                if (pack.m_State == eMissionState.PreComp) {
                    AddPreCompList(pack.m_Type, pack.m_KeyManageID);
                } else if (pack.m_State == eMissionState.PrePop) {
                    AddNewCompList(pack.m_Type, pack.m_KeyManageID);
                    AddPreCompList(pack.m_Type, pack.m_KeyManageID);
                    pack.m_State = eMissionState.PreComp;
                }
            }
            for (int i = 0; i < m_List.Count;) {
                if (m_List[i].m_MissionID < 0) {
                    m_List.RemoveAt(i);
                    continue;
                }
                i++;
            }
            m_NewListUpKey = true;
        }

        public void GetMissionLogList() {
            m_Step = eStep.ComUp;
            MeigewwwParam param = MISSIONRequest.GetAll(CommonGetAllCallback);
            NetworkQueueManager.Request(param);
        }

        public void CommonGetAllCallback(MeigewwwParam pparam) {
            CommonResponse resp = ResponseCommon.Func<CommonResponse>(pparam);
            if (resp != null) {
                string json = pparam.GetResponseJson();
                GetAll getAll = JsonConvert.DeserializeObject<GetAll>(json);
                GetAllToList(getAll.missionLogs);
                m_Step++;
            }
        }

        public void RefreshMissionLogList() {
            m_Step = eStep.ComUp;
            MeigewwwParam param = MISSIONRequest.GetAll(CommonRefreshCallback);
            NetworkQueueManager.Request(param);
        }

        public void CommonRefreshCallback(MeigewwwParam pparam) {
            CommonResponse resp = ResponseCommon.Func<CommonResponse>(pparam);
            if (resp != null) {
                string json = pparam.GetResponseJson();
                GetAll getAll = JsonConvert.DeserializeObject<GetAll>(json);
                RefreshAllToList(getAll.missionLogs);
                m_NewListUpKey = true;
                if (MissionManager.Inst != null) {
                    MissionManager.Inst.ExecutePopup();
                }
                m_Step++;
            }
        }

        public void SendMissionSetList() {
            m_SendComFlagSet = true;
            PlayerMissionLog[] logs = new PlayerMissionLog[m_SendSetList.Count];
            for (int i = 0; i < m_SendSetList.Count; i++) {
                for (int j = 0; j < m_List.Count; j++) {
                    if (m_List[j].m_KeyManageID == m_SendSetList[i]) {
                        logs[i] = new PlayerMissionLog();
                        logs[i].managedMissionId = m_List[j].m_KeyManageID;
                        logs[i].missionID = (int)m_List[j].m_MissionID;
                        logs[i].rate = m_List[j].m_Rate;
                        logs[i].rateMax = m_List[j].m_RateBase;
                        logs[i].state = (int)m_List[j].m_State;
                        logs[i].reward = null;
                        logs[i].limitTime = m_List[j].m_LimitTime;
                        logs[i].uiPriority = m_List[j].m_uiPriority;
                        break;
                    }
                }
            }
            m_SendSetList.Clear();
            MeigewwwParam param = MISSIONRequest.Set(logs, CommonSetCallback);
            NetworkQueueManager.Request(param);
        }

        public void CommonSetCallback(MeigewwwParam pparam) {
            CommonResponse resp = ResponseCommon.Func<CommonResponse>(pparam);
            if (resp != null) {
                m_SendComFlagSet = false;
                if (resp.GetResult() == ResultCode.SUCCESS) {
                    m_Refresh = true;
                }
            }
        }

        public void SendMissionCompList() {
            m_SendComFlagComp = true;
            GameSystem.Inst.ConnectingIcon.Open();
            MeigewwwParam param = MISSIONRequest.Complete(m_SendCompList[0], CommonCompCallback);
            m_SendCompList.RemoveAt(0);
            NetworkQueueManager.Request(param);
        }

        public void CommonCompCallback(MeigewwwParam pparam) {
            GameSystem.Inst.ConnectingIcon.Close();
            CommonResponse resp = ResponseCommon.Func<CommonResponse>(pparam);
            m_SendComFlagComp = false;
            if (resp != null) {
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
                    return;
                }
                string json = pparam.GetResponseJson();
                Complete complete = JsonConvert.DeserializeObject<Complete>(json);
                APIUtility.wwwToUserData(complete.player, GameSystem.Inst.UserDataMng.UserData);
                GameSystem.Inst.UserDataMng.NoticePresentCount = complete.presents.Length;
                m_Refresh = true;
            }
        }

        public void Update() {
            switch (m_Step) {
                case eStep.Init:
                    if (GameSystem.Inst.UserDataMng.UserData.ID != -1L) {
                        m_Step++;
                        GetMissionLogList();
                    }
                    break;
                case eStep.Calc:
                    if (m_Refresh) {
                        m_Refresh = false;
                        RefreshMissionLogList();
                    }
                    if (m_NewListUpKey) {
                        if (m_Callback != null) {
                            m_Callback();
                        }
                        m_Callback = null;
                        m_NewListUpKey = false;
                    }
                    if (m_SendSetList.Count != 0 && !m_SendComFlagSet) {
                        SendMissionSetList();
                    }
                    if (m_SendCompList.Count != 0 && !m_SendComFlagComp) {
                        SendMissionCompList();
                    }
                    break;
            }
        }

        public void EntryRefreshCallback(Action pcallback) {
            m_Callback = pcallback;
        }

        public void ReleaseRefreshCallback() {
            m_Callback = null;
        }

        public bool IsFinishedRequestCompAll() {
            return !m_SendComFlagSet && !m_SendComFlagComp && m_SendCompList.Count == 0;
        }

        public enum eStep {
            None = -1,
            Init,
            ComUp,
            Calc,
            End
        }
    }
}
