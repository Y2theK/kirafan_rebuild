﻿namespace Star {
    public class MissionUIDataPack {
        public IMissionUIData[] m_Table;

        public int GetListNum() {
            return m_Table.Length;
        }

        public IMissionUIData GetTable(int findex) {
            return m_Table[findex];
        }
    }
}
