﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020007B3 RID: 1971
	[Token(Token = "0x20005DE")]
	[StructLayout(3)]
	public class MixEffectScene : MonoBehaviour
	{
		// Token: 0x06001E1D RID: 7709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B95")]
		[Address(RVA = "0x1012629F0", Offset = "0x12629F0", VA = "0x1012629F0")]
		private void Start()
		{
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B96")]
		[Address(RVA = "0x101262A24", Offset = "0x1262A24", VA = "0x101262A24")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001E1F RID: 7711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B97")]
		[Address(RVA = "0x101262BD8", Offset = "0x1262BD8", VA = "0x101262BD8")]
		private void Update()
		{
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B98")]
		[Address(RVA = "0x101263874", Offset = "0x1263874", VA = "0x101263874")]
		public void Prepare(int charaID)
		{
		}

		// Token: 0x06001E21 RID: 7713 RVA: 0x0000D650 File Offset: 0x0000B850
		[Token(Token = "0x6001B99")]
		[Address(RVA = "0x1012639F4", Offset = "0x12639F4", VA = "0x1012639F4")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06001E22 RID: 7714 RVA: 0x0000D668 File Offset: 0x0000B868
		[Token(Token = "0x6001B9A")]
		[Address(RVA = "0x101263A04", Offset = "0x1263A04", VA = "0x101263A04")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06001E23 RID: 7715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B9B")]
		[Address(RVA = "0x101263A14", Offset = "0x1263A14", VA = "0x101263A14")]
		public void Play(MixEffectScene.eAnimType animType)
		{
		}

		// Token: 0x06001E24 RID: 7716 RVA: 0x0000D680 File Offset: 0x0000B880
		[Token(Token = "0x6001B9C")]
		[Address(RVA = "0x101263D10", Offset = "0x1263D10", VA = "0x101263D10")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06001E25 RID: 7717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001B9D")]
		[Address(RVA = "0x101263D30", Offset = "0x1263D30", VA = "0x101263D30")]
		public void OnSkip()
		{
		}

		// Token: 0x06001E26 RID: 7718 RVA: 0x0000D698 File Offset: 0x0000B898
		[Token(Token = "0x6001B9E")]
		[Address(RVA = "0x101263D44", Offset = "0x1263D44", VA = "0x101263D44")]
		public float GetPlayingSec(MixEffectScene.eAnimType animType)
		{
			return 0f;
		}

		// Token: 0x06001E27 RID: 7719 RVA: 0x0000D6B0 File Offset: 0x0000B8B0
		[Token(Token = "0x6001B9F")]
		[Address(RVA = "0x101263E48", Offset = "0x1263E48", VA = "0x101263E48")]
		public float GetMaxSec(MixEffectScene.eAnimType animType)
		{
			return 0f;
		}

		// Token: 0x06001E28 RID: 7720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA0")]
		[Address(RVA = "0x101262A28", Offset = "0x1262A28", VA = "0x101262A28")]
		public void Destroy()
		{
		}

		// Token: 0x06001E29 RID: 7721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA1")]
		[Address(RVA = "0x101263F24", Offset = "0x1263F24", VA = "0x101263F24")]
		public void Suspend()
		{
		}

		// Token: 0x06001E2A RID: 7722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA2")]
		[Address(RVA = "0x101263834", Offset = "0x1263834", VA = "0x101263834")]
		private void SetStep(MixEffectScene.eStep step)
		{
		}

		// Token: 0x06001E2B RID: 7723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA3")]
		[Address(RVA = "0x1012633B0", Offset = "0x12633B0", VA = "0x1012633B0")]
		private void SetLayerRecursively(GameObject self, int layer)
		{
		}

		// Token: 0x06001E2C RID: 7724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA4")]
		[Address(RVA = "0x101263604", Offset = "0x1263604", VA = "0x101263604")]
		private void ApplyCharaIllustTexture()
		{
		}

		// Token: 0x06001E2D RID: 7725 RVA: 0x0000D6C8 File Offset: 0x0000B8C8
		[Token(Token = "0x6001BA5")]
		[Address(RVA = "0x10126383C", Offset = "0x126383C", VA = "0x10126383C")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06001E2E RID: 7726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001BA6")]
		[Address(RVA = "0x101264088", Offset = "0x1264088", VA = "0x101264088")]
		public MixEffectScene()
		{
		}

		// Token: 0x04002E63 RID: 11875
		[Token(Token = "0x4002402")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04002E64 RID: 11876
		[Token(Token = "0x4002403")]
		private static readonly string[] OBJ_CHARA_ILLUSTS;

		// Token: 0x04002E65 RID: 11877
		[Token(Token = "0x4002404")]
		private static readonly string[] ANIM_KEYS;

		// Token: 0x04002E66 RID: 11878
		[Token(Token = "0x4002405")]
		private static readonly string[] RESOURCE_PATHS;

		// Token: 0x04002E67 RID: 11879
		[Token(Token = "0x4002406")]
		private static readonly string[] MODEL_OBJECT_PATHS;

		// Token: 0x04002E68 RID: 11880
		[Token(Token = "0x4002407")]
		private static readonly string[,] ANIM_OBJECT_PATHS;

		// Token: 0x04002E69 RID: 11881
		[Token(Token = "0x4002408")]
		private static readonly eSoundSeListDB[] CUE_IDS;

		// Token: 0x04002E6A RID: 11882
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002409")]
		[SerializeField]
		private MixEffectScene.eMixType m_MixType;

		// Token: 0x04002E6B RID: 11883
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400240A")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04002E6C RID: 11884
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400240B")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04002E6D RID: 11885
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400240C")]
		private MixEffectScene.eStep m_Step;

		// Token: 0x04002E6E RID: 11886
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400240D")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x04002E6F RID: 11887
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400240E")]
		private ABResourceLoader m_Loader;

		// Token: 0x04002E70 RID: 11888
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400240F")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04002E71 RID: 11889
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002410")]
		private SpriteHandler m_SpriteHndl;

		// Token: 0x04002E72 RID: 11890
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002411")]
		private Transform m_BaseTransform;

		// Token: 0x04002E73 RID: 11891
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002412")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04002E74 RID: 11892
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002413")]
		private GameObject m_BodyObj;

		// Token: 0x04002E75 RID: 11893
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002414")]
		private Texture m_Texture;

		// Token: 0x04002E76 RID: 11894
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002415")]
		private int m_CharaID;

		// Token: 0x04002E77 RID: 11895
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002416")]
		private SoundHandler m_SoundHndl;

		// Token: 0x020007B4 RID: 1972
		[Token(Token = "0x2000EA6")]
		public enum eMixType
		{
			// Token: 0x04002E79 RID: 11897
			[Token(Token = "0x4005DD8")]
			Upgrade,
			// Token: 0x04002E7A RID: 11898
			[Token(Token = "0x4005DD9")]
			LimitBreak,
			// Token: 0x04002E7B RID: 11899
			[Token(Token = "0x4005DDA")]
			Evolution,
			// Token: 0x04002E7C RID: 11900
			[Token(Token = "0x4005DDB")]
			Num
		}

		// Token: 0x020007B5 RID: 1973
		[Token(Token = "0x2000EA7")]
		public enum eAnimType
		{
			// Token: 0x04002E7E RID: 11902
			[Token(Token = "0x4005DDD")]
			Effect,
			// Token: 0x04002E7F RID: 11903
			[Token(Token = "0x4005DDE")]
			Loop,
			// Token: 0x04002E80 RID: 11904
			[Token(Token = "0x4005DDF")]
			Num
		}

		// Token: 0x020007B6 RID: 1974
		[Token(Token = "0x2000EA8")]
		private enum eStep
		{
			// Token: 0x04002E82 RID: 11906
			[Token(Token = "0x4005DE1")]
			None = -1,
			// Token: 0x04002E83 RID: 11907
			[Token(Token = "0x4005DE2")]
			Prepare,
			// Token: 0x04002E84 RID: 11908
			[Token(Token = "0x4005DE3")]
			Prepare_Wait,
			// Token: 0x04002E85 RID: 11909
			[Token(Token = "0x4005DE4")]
			Prepare_Error,
			// Token: 0x04002E86 RID: 11910
			[Token(Token = "0x4005DE5")]
			PlayEffect,
			// Token: 0x04002E87 RID: 11911
			[Token(Token = "0x4005DE6")]
			PlayEffect_Wait,
			// Token: 0x04002E88 RID: 11912
			[Token(Token = "0x4005DE7")]
			PlayLoop,
			// Token: 0x04002E89 RID: 11913
			[Token(Token = "0x4005DE8")]
			PlayLoop_Wait
		}
	}
}
