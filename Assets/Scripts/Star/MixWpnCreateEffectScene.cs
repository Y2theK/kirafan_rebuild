﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000826 RID: 2086
	[Token(Token = "0x2000621")]
	[StructLayout(3)]
	public class MixWpnCreateEffectScene : MonoBehaviour
	{
		// Token: 0x06002106 RID: 8454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E6C")]
		[Address(RVA = "0x1012647B4", Offset = "0x12647B4", VA = "0x1012647B4")]
		private void Update()
		{
		}

		// Token: 0x06002107 RID: 8455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E6D")]
		[Address(RVA = "0x1012650D4", Offset = "0x12650D4", VA = "0x1012650D4")]
		public void Prepare(int sortingOrder)
		{
		}

		// Token: 0x06002108 RID: 8456 RVA: 0x0000E6D0 File Offset: 0x0000C8D0
		[Token(Token = "0x6001E6E")]
		[Address(RVA = "0x101265174", Offset = "0x1265174", VA = "0x101265174")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06002109 RID: 8457 RVA: 0x0000E6E8 File Offset: 0x0000C8E8
		[Token(Token = "0x6001E6F")]
		[Address(RVA = "0x10126517C", Offset = "0x126517C", VA = "0x10126517C")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x0600210A RID: 8458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E70")]
		[Address(RVA = "0x10126518C", Offset = "0x126518C", VA = "0x10126518C")]
		public void Play()
		{
		}

		// Token: 0x0600210B RID: 8459 RVA: 0x0000E700 File Offset: 0x0000C900
		[Token(Token = "0x6001E71")]
		[Address(RVA = "0x1012653E0", Offset = "0x12653E0", VA = "0x1012653E0")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x0600210C RID: 8460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E72")]
		[Address(RVA = "0x1012653F0", Offset = "0x12653F0", VA = "0x1012653F0")]
		public void Destroy()
		{
		}

		// Token: 0x0600210D RID: 8461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E73")]
		[Address(RVA = "0x101264E40", Offset = "0x1264E40", VA = "0x101264E40")]
		private void SetStep(MixWpnCreateEffectScene.eStep step)
		{
		}

		// Token: 0x0600210E RID: 8462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E74")]
		[Address(RVA = "0x101264E48", Offset = "0x1264E48", VA = "0x101264E48")]
		private void SetLayerRecursively(GameObject self, int layer)
		{
		}

		// Token: 0x0600210F RID: 8463 RVA: 0x0000E718 File Offset: 0x0000C918
		[Token(Token = "0x6001E75")]
		[Address(RVA = "0x10126509C", Offset = "0x126509C", VA = "0x10126509C")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06002110 RID: 8464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E76")]
		[Address(RVA = "0x101265404", Offset = "0x1265404", VA = "0x101265404")]
		public MixWpnCreateEffectScene()
		{
		}

		// Token: 0x04003106 RID: 12550
		[Token(Token = "0x4002519")]
		private const string LAYER_MASK = "UI";

		// Token: 0x04003107 RID: 12551
		[Token(Token = "0x400251A")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04003108 RID: 12552
		[Token(Token = "0x400251B")]
		private const string RESOURCE_PATH = "mix/mix_wpncreate.muast";

		// Token: 0x04003109 RID: 12553
		[Token(Token = "0x400251C")]
		private const string OBJ_MODEL_PATH = "Mix_WpnCreate";

		// Token: 0x0400310A RID: 12554
		[Token(Token = "0x400251D")]
		private const string OBJ_ANIM_PATH = "MeigeAC_Mix_WpnCreate@Take 001";

		// Token: 0x0400310B RID: 12555
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400251E")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400310C RID: 12556
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400251F")]
		private MixWpnCreateEffectScene.eStep m_Step;

		// Token: 0x0400310D RID: 12557
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002520")]
		private bool m_IsDonePrepare;

		// Token: 0x0400310E RID: 12558
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002521")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400310F RID: 12559
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002522")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04003110 RID: 12560
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002523")]
		private Transform m_BaseTransform;

		// Token: 0x04003111 RID: 12561
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002524")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04003112 RID: 12562
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002525")]
		private GameObject m_BodyObj;

		// Token: 0x04003113 RID: 12563
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002526")]
		private int m_SortingOrder;

		// Token: 0x02000827 RID: 2087
		[Token(Token = "0x2000ED5")]
		private enum eStep
		{
			// Token: 0x04003115 RID: 12565
			[Token(Token = "0x4005F61")]
			None = -1,
			// Token: 0x04003116 RID: 12566
			[Token(Token = "0x4005F62")]
			Prepare_Wait,
			// Token: 0x04003117 RID: 12567
			[Token(Token = "0x4005F63")]
			Prepare_Error,
			// Token: 0x04003118 RID: 12568
			[Token(Token = "0x4005F64")]
			Wait,
			// Token: 0x04003119 RID: 12569
			[Token(Token = "0x4005F65")]
			Play_Wait
		}
	}
}
