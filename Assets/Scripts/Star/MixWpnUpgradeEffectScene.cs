﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000828 RID: 2088
	[Token(Token = "0x2000622")]
	[StructLayout(3)]
	public class MixWpnUpgradeEffectScene : MonoBehaviour
	{
		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06002111 RID: 8465 RVA: 0x0000E730 File Offset: 0x0000C930
		// (set) Token: 0x06002112 RID: 8466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000236")]
		public Vector3 Scale
		{
			[Token(Token = "0x6001E77")]
			[Address(RVA = "0x101265478", Offset = "0x1265478", VA = "0x101265478")]
			get
			{
				return default(Vector3);
			}
			[Token(Token = "0x6001E78")]
			[Address(RVA = "0x101265484", Offset = "0x1265484", VA = "0x101265484")]
			set
			{
			}
		}

		// Token: 0x06002113 RID: 8467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E79")]
		[Address(RVA = "0x101265490", Offset = "0x1265490", VA = "0x101265490")]
		private void Update()
		{
		}

		// Token: 0x06002114 RID: 8468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E7A")]
		[Address(RVA = "0x101265DB0", Offset = "0x1265DB0", VA = "0x101265DB0")]
		public void Prepare(int sortingOrder)
		{
		}

		// Token: 0x06002115 RID: 8469 RVA: 0x0000E748 File Offset: 0x0000C948
		[Token(Token = "0x6001E7B")]
		[Address(RVA = "0x101265E50", Offset = "0x1265E50", VA = "0x101265E50")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06002116 RID: 8470 RVA: 0x0000E760 File Offset: 0x0000C960
		[Token(Token = "0x6001E7C")]
		[Address(RVA = "0x101265E58", Offset = "0x1265E58", VA = "0x101265E58")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06002117 RID: 8471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E7D")]
		[Address(RVA = "0x101265E68", Offset = "0x1265E68", VA = "0x101265E68")]
		public void Play()
		{
		}

		// Token: 0x06002118 RID: 8472 RVA: 0x0000E778 File Offset: 0x0000C978
		[Token(Token = "0x6001E7E")]
		[Address(RVA = "0x1012660BC", Offset = "0x12660BC", VA = "0x1012660BC")]
		public bool IsCompletePlay()
		{
			return default(bool);
		}

		// Token: 0x06002119 RID: 8473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E7F")]
		[Address(RVA = "0x1012660CC", Offset = "0x12660CC", VA = "0x1012660CC")]
		public void Destroy()
		{
		}

		// Token: 0x0600211A RID: 8474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E80")]
		[Address(RVA = "0x101265B1C", Offset = "0x1265B1C", VA = "0x101265B1C")]
		private void SetStep(MixWpnUpgradeEffectScene.eStep step)
		{
		}

		// Token: 0x0600211B RID: 8475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E81")]
		[Address(RVA = "0x101265B24", Offset = "0x1265B24", VA = "0x101265B24")]
		private void SetLayerRecursively(GameObject self, int layer)
		{
		}

		// Token: 0x0600211C RID: 8476 RVA: 0x0000E790 File Offset: 0x0000C990
		[Token(Token = "0x6001E82")]
		[Address(RVA = "0x101265D78", Offset = "0x1265D78", VA = "0x101265D78")]
		private bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600211D RID: 8477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E83")]
		[Address(RVA = "0x1012660E0", Offset = "0x12660E0", VA = "0x1012660E0")]
		public MixWpnUpgradeEffectScene()
		{
		}

		// Token: 0x0400311A RID: 12570
		[Token(Token = "0x4002527")]
		private const string LAYER_MASK = "UI";

		// Token: 0x0400311B RID: 12571
		[Token(Token = "0x4002528")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x0400311C RID: 12572
		[Token(Token = "0x4002529")]
		private const string RESOURCE_PATH = "mix/mix_wpnupgrade.muast";

		// Token: 0x0400311D RID: 12573
		[Token(Token = "0x400252A")]
		private const string OBJ_MODEL_PATH = "Mix_WpnUpgrade";

		// Token: 0x0400311E RID: 12574
		[Token(Token = "0x400252B")]
		private const string OBJ_ANIM_PATH = "MeigeAC_Mix_WpnUpgrade@Take 001";

		// Token: 0x0400311F RID: 12575
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400252C")]
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04003120 RID: 12576
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400252D")]
		private MixWpnUpgradeEffectScene.eStep m_Step;

		// Token: 0x04003121 RID: 12577
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400252E")]
		private bool m_IsDonePrepare;

		// Token: 0x04003122 RID: 12578
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400252F")]
		private ABResourceLoader m_Loader;

		// Token: 0x04003123 RID: 12579
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002530")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04003124 RID: 12580
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002531")]
		private Transform m_BaseTransform;

		// Token: 0x04003125 RID: 12581
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002532")]
		private MsbHandler m_MsbHndl;

		// Token: 0x04003126 RID: 12582
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002533")]
		private GameObject m_BodyObj;

		// Token: 0x04003127 RID: 12583
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002534")]
		private int m_SortingOrder;

		// Token: 0x04003128 RID: 12584
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002535")]
		private Vector3 m_Scale;

		// Token: 0x02000829 RID: 2089
		[Token(Token = "0x2000ED6")]
		private enum eStep
		{
			// Token: 0x0400312A RID: 12586
			[Token(Token = "0x4005F67")]
			None = -1,
			// Token: 0x0400312B RID: 12587
			[Token(Token = "0x4005F68")]
			Prepare_Wait,
			// Token: 0x0400312C RID: 12588
			[Token(Token = "0x4005F69")]
			Prepare_Error,
			// Token: 0x0400312D RID: 12589
			[Token(Token = "0x4005F6A")]
			Wait,
			// Token: 0x0400312E RID: 12590
			[Token(Token = "0x4005F6B")]
			Play_Wait
		}
	}
}
