﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008F2 RID: 2290
	[Token(Token = "0x2000693")]
	[StructLayout(3)]
	public class MovieCanvas : MonoBehaviour
	{
		// Token: 0x06002586 RID: 9606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A2")]
		[Address(RVA = "0x101266184", Offset = "0x1266184", VA = "0x101266184")]
		public void OnDestroy()
		{
		}

		// Token: 0x06002587 RID: 9607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A3")]
		[Address(RVA = "0x101266200", Offset = "0x1266200", VA = "0x101266200")]
		public void SetFile(string movieFileNameWithoutExt)
		{
		}

		// Token: 0x06002588 RID: 9608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A4")]
		[Address(RVA = "0x101266404", Offset = "0x1266404", VA = "0x101266404")]
		public void SetLoop(bool isLoop)
		{
		}

		// Token: 0x06002589 RID: 9609 RVA: 0x00010098 File Offset: 0x0000E298
		[Token(Token = "0x60022A5")]
		[Address(RVA = "0x1012664CC", Offset = "0x12664CC", VA = "0x1012664CC")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600258A RID: 9610 RVA: 0x000100B0 File Offset: 0x0000E2B0
		[Token(Token = "0x60022A6")]
		[Address(RVA = "0x10126667C", Offset = "0x126667C", VA = "0x10126667C")]
		public bool IsPaused()
		{
			return default(bool);
		}

		// Token: 0x0600258B RID: 9611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A7")]
		[Address(RVA = "0x101266738", Offset = "0x1266738", VA = "0x101266738")]
		public void Prepare()
		{
		}

		// Token: 0x0600258C RID: 9612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A8")]
		[Address(RVA = "0x1012667EC", Offset = "0x12667EC", VA = "0x1012667EC")]
		public void Play()
		{
		}

		// Token: 0x0600258D RID: 9613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022A9")]
		[Address(RVA = "0x101266888", Offset = "0x1266888", VA = "0x101266888")]
		public void Stop()
		{
		}

		// Token: 0x0600258E RID: 9614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022AA")]
		[Address(RVA = "0x101266924", Offset = "0x1266924", VA = "0x101266924")]
		public void Pause()
		{
		}

		// Token: 0x0600258F RID: 9615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022AB")]
		[Address(RVA = "0x1012669C4", Offset = "0x12669C4", VA = "0x1012669C4")]
		public void Resume()
		{
		}

		// Token: 0x06002590 RID: 9616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022AC")]
		[Address(RVA = "0x101266A64", Offset = "0x1266A64", VA = "0x101266A64")]
		public MovieCanvas()
		{
		}

		// Token: 0x040035CA RID: 13770
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002778")]
		[SerializeField]
		private CriManaMovieControllerForUI m_MovieController;
	}
}
