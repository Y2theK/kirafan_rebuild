﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200055F RID: 1375
	[Token(Token = "0x2000452")]
	[StructLayout(3)]
	public class MoviePlayListDB : ScriptableObject
	{
		// Token: 0x060015DB RID: 5595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148B")]
		[Address(RVA = "0x101266A6C", Offset = "0x1266A6C", VA = "0x101266A6C")]
		public MoviePlayListDB()
		{
		}

		// Token: 0x04001908 RID: 6408
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001272")]
		public MoviePlayListDB_Param[] m_Params;
	}
}
