﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200055E RID: 1374
	[Token(Token = "0x2000451")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct MoviePlayListDB_Param
	{
		// Token: 0x04001905 RID: 6405
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400126F")]
		public string m_FileName;

		// Token: 0x04001906 RID: 6406
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001270")]
		public int m_ConditionID;

		// Token: 0x04001907 RID: 6407
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001271")]
		public int m_BannerID;
	}
}
