﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x020007EA RID: 2026
	[Token(Token = "0x20005FD")]
	[StructLayout(3)]
	public class MoviePlayMain : GameStateMain
	{
		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06001F61 RID: 8033 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001F62 RID: 8034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000220")]
		public MovieCanvas MovieCanvas
		{
			[Token(Token = "0x6001CD9")]
			[Address(RVA = "0x101266B54", Offset = "0x1266B54", VA = "0x101266B54")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001CDA")]
			[Address(RVA = "0x101266B5C", Offset = "0x1266B5C", VA = "0x101266B5C")]
			set
			{
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06001F63 RID: 8035 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000221")]
		public MoviePlayUI MoviePlayUI
		{
			[Token(Token = "0x6001CDB")]
			[Address(RVA = "0x101266B64", Offset = "0x1266B64", VA = "0x101266B64")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06001F64 RID: 8036 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001F65 RID: 8037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000222")]
		public string MoviePlayFileName
		{
			[Token(Token = "0x6001CDC")]
			[Address(RVA = "0x101266B6C", Offset = "0x1266B6C", VA = "0x101266B6C")]
			get
			{
				return null;
			}
			[Token(Token = "0x6001CDD")]
			[Address(RVA = "0x101266B74", Offset = "0x1266B74", VA = "0x101266B74")]
			set
			{
			}
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06001F66 RID: 8038 RVA: 0x0000DED8 File Offset: 0x0000C0D8
		// (set) Token: 0x06001F67 RID: 8039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000223")]
		public int AdvID
		{
			[Token(Token = "0x6001CDE")]
			[Address(RVA = "0x101266B7C", Offset = "0x1266B7C", VA = "0x101266B7C")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6001CDF")]
			[Address(RVA = "0x101266B84", Offset = "0x1266B84", VA = "0x101266B84")]
			set
			{
			}
		}

		// Token: 0x06001F68 RID: 8040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE0")]
		[Address(RVA = "0x101266B8C", Offset = "0x1266B8C", VA = "0x101266B8C")]
		private void Start()
		{
		}

		// Token: 0x06001F69 RID: 8041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE1")]
		[Address(RVA = "0x101266B98", Offset = "0x1266B98", VA = "0x101266B98")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001F6A RID: 8042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE2")]
		[Address(RVA = "0x101266BA0", Offset = "0x1266BA0", VA = "0x101266BA0", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001F6B RID: 8043 RVA: 0x0000DEF0 File Offset: 0x0000C0F0
		[Token(Token = "0x6001CE3")]
		[Address(RVA = "0x101266BA8", Offset = "0x1266BA8", VA = "0x101266BA8", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001F6C RID: 8044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE4")]
		[Address(RVA = "0x101266BB0", Offset = "0x1266BB0", VA = "0x101266BB0", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001CE5")]
		[Address(RVA = "0x101266BB8", Offset = "0x1266BB8", VA = "0x101266BB8", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001F6E RID: 8046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE6")]
		[Address(RVA = "0x101266D48", Offset = "0x1266D48", VA = "0x101266D48")]
		public MoviePlayMain()
		{
		}

		// Token: 0x04002FA3 RID: 12195
		[Token(Token = "0x400248B")]
		public const int STATE_INIT = 0;

		// Token: 0x04002FA4 RID: 12196
		[Token(Token = "0x400248C")]
		public const int STATE_PLAY = 1;

		// Token: 0x04002FA5 RID: 12197
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400248D")]
		[SerializeField]
		private MovieCanvas m_MovieCanvas;

		// Token: 0x04002FA6 RID: 12198
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400248E")]
		[SerializeField]
		private MoviePlayUI m_UI;

		// Token: 0x04002FA7 RID: 12199
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400248F")]
		private string m_MoviePlayFileName;

		// Token: 0x04002FA8 RID: 12200
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002490")]
		private int m_AdvID;
	}
}
