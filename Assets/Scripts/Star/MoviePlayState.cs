﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007EB RID: 2027
	[Token(Token = "0x20005FE")]
	[StructLayout(3)]
	public class MoviePlayState : GameStateBase
	{
		// Token: 0x06001F6F RID: 8047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE7")]
		[Address(RVA = "0x101266D58", Offset = "0x1266D58", VA = "0x101266D58")]
		public MoviePlayState(MoviePlayMain owner)
		{
		}

		// Token: 0x06001F70 RID: 8048 RVA: 0x0000DF08 File Offset: 0x0000C108
		[Token(Token = "0x6001CE8")]
		[Address(RVA = "0x101266D8C", Offset = "0x1266D8C", VA = "0x101266D8C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F71 RID: 8049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CE9")]
		[Address(RVA = "0x101266D94", Offset = "0x1266D94", VA = "0x101266D94", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F72 RID: 8050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CEA")]
		[Address(RVA = "0x101266D98", Offset = "0x1266D98", VA = "0x101266D98", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F73 RID: 8051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CEB")]
		[Address(RVA = "0x101266D9C", Offset = "0x1266D9C", VA = "0x101266D9C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F74 RID: 8052 RVA: 0x0000DF20 File Offset: 0x0000C120
		[Token(Token = "0x6001CEC")]
		[Address(RVA = "0x101266DA0", Offset = "0x1266DA0", VA = "0x101266DA0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F75 RID: 8053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CED")]
		[Address(RVA = "0x101266DA8", Offset = "0x1266DA8", VA = "0x101266DA8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002FA9 RID: 12201
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002491")]
		protected MoviePlayMain m_Owner;

		// Token: 0x04002FAA RID: 12202
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002492")]
		protected int m_NextState;
	}
}
