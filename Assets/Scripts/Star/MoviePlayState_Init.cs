﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007EC RID: 2028
	[Token(Token = "0x20005FF")]
	[StructLayout(3)]
	public class MoviePlayState_Init : MoviePlayState
	{
		// Token: 0x06001F76 RID: 8054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CEE")]
		[Address(RVA = "0x101266C8C", Offset = "0x1266C8C", VA = "0x101266C8C")]
		public MoviePlayState_Init(MoviePlayMain owner)
		{
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x0000DF38 File Offset: 0x0000C138
		[Token(Token = "0x6001CEF")]
		[Address(RVA = "0x101266DAC", Offset = "0x1266DAC", VA = "0x101266DAC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F78 RID: 8056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF0")]
		[Address(RVA = "0x101266DB4", Offset = "0x1266DB4", VA = "0x101266DB4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F79 RID: 8057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF1")]
		[Address(RVA = "0x101266DBC", Offset = "0x1266DBC", VA = "0x101266DBC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF2")]
		[Address(RVA = "0x101266DC0", Offset = "0x1266DC0", VA = "0x101266DC0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F7B RID: 8059 RVA: 0x0000DF50 File Offset: 0x0000C150
		[Token(Token = "0x6001CF3")]
		[Address(RVA = "0x101266DC4", Offset = "0x1266DC4", VA = "0x101266DC4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F7C RID: 8060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF4")]
		[Address(RVA = "0x1012675F0", Offset = "0x12675F0", VA = "0x1012675F0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F7D RID: 8061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF5")]
		[Address(RVA = "0x1012675F4", Offset = "0x12675F4", VA = "0x1012675F4")]
		private void OnConfirmRetry(int answer)
		{
		}

		// Token: 0x04002FAB RID: 12203
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002493")]
		private MoviePlayState_Init.eStep m_Step;

		// Token: 0x04002FAC RID: 12204
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002494")]
		private SoundDLSizeCheck m_DLSizeHndl;

		// Token: 0x04002FAD RID: 12205
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002495")]
		private bool m_IsOpenedErrorWindow;

		// Token: 0x020007ED RID: 2029
		[Token(Token = "0x2000EBE")]
		private enum eStep
		{
			// Token: 0x04002FAF RID: 12207
			[Token(Token = "0x4005E8F")]
			None = -1,
			// Token: 0x04002FB0 RID: 12208
			[Token(Token = "0x4005E90")]
			First,
			// Token: 0x04002FB1 RID: 12209
			[Token(Token = "0x4005E91")]
			CheckDLSize_Wait,
			// Token: 0x04002FB2 RID: 12210
			[Token(Token = "0x4005E92")]
			DownloadConfirm_Wait,
			// Token: 0x04002FB3 RID: 12211
			[Token(Token = "0x4005E93")]
			Download,
			// Token: 0x04002FB4 RID: 12212
			[Token(Token = "0x4005E94")]
			Download_Wait,
			// Token: 0x04002FB5 RID: 12213
			[Token(Token = "0x4005E95")]
			Transit,
			// Token: 0x04002FB6 RID: 12214
			[Token(Token = "0x4005E96")]
			Transit_Wait,
			// Token: 0x04002FB7 RID: 12215
			[Token(Token = "0x4005E97")]
			End
		}
	}
}
