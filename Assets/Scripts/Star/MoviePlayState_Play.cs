﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using GachaResponseTypes;
using Meige;

namespace Star
{
	// Token: 0x020007EE RID: 2030
	[Token(Token = "0x2000600")]
	[StructLayout(3)]
	public class MoviePlayState_Play : MoviePlayState
	{
		// Token: 0x06001F7F RID: 8063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF7")]
		[Address(RVA = "0x101266CC0", Offset = "0x1266CC0", VA = "0x101266CC0")]
		public MoviePlayState_Play(MoviePlayMain owner)
		{
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x0000DF68 File Offset: 0x0000C168
		[Token(Token = "0x6001CF8")]
		[Address(RVA = "0x101267680", Offset = "0x1267680", VA = "0x101267680", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CF9")]
		[Address(RVA = "0x101267688", Offset = "0x1267688", VA = "0x101267688", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CFA")]
		[Address(RVA = "0x101267690", Offset = "0x1267690", VA = "0x101267690", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CFB")]
		[Address(RVA = "0x101267694", Offset = "0x1267694", VA = "0x101267694", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F84 RID: 8068 RVA: 0x0000DF80 File Offset: 0x0000C180
		[Token(Token = "0x6001CFC")]
		[Address(RVA = "0x101267698", Offset = "0x1267698", VA = "0x101267698", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F85 RID: 8069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CFD")]
		[Address(RVA = "0x101267E10", Offset = "0x1267E10", VA = "0x101267E10")]
		public void OnResponse_GachaGetAll(MeigewwwParam wwwParam, GetAll param)
		{
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CFE")]
		[Address(RVA = "0x101267E58", Offset = "0x1267E58", VA = "0x101267E58")]
		private void OnResponse_EventBannerGetAll()
		{
		}

		// Token: 0x06001F87 RID: 8071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001CFF")]
		[Address(RVA = "0x101267E64", Offset = "0x1267E64", VA = "0x101267E64", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F88 RID: 8072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D00")]
		[Address(RVA = "0x101267E68", Offset = "0x1267E68", VA = "0x101267E68")]
		private void OnExecSkipMovie()
		{
		}

		// Token: 0x06001F89 RID: 8073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D01")]
		[Address(RVA = "0x101267E74", Offset = "0x1267E74", VA = "0x101267E74")]
		private void OnResponse_AdvAdd()
		{
		}

		// Token: 0x04002FB8 RID: 12216
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002496")]
		private MoviePlayState_Play.eStep m_Step;

		// Token: 0x04002FB9 RID: 12217
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002497")]
		private bool m_RequestedMovieSkip;

		// Token: 0x04002FBA RID: 12218
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002498")]
		private ADVAPI m_AdvAPI;

		// Token: 0x020007EF RID: 2031
		[Token(Token = "0x2000EBF")]
		private enum eStep
		{
			// Token: 0x04002FBC RID: 12220
			[Token(Token = "0x4005E99")]
			None = -1,
			// Token: 0x04002FBD RID: 12221
			[Token(Token = "0x4005E9A")]
			Play,
			// Token: 0x04002FBE RID: 12222
			[Token(Token = "0x4005E9B")]
			Playing,
			// Token: 0x04002FBF RID: 12223
			[Token(Token = "0x4005E9C")]
			Request_Wait,
			// Token: 0x04002FC0 RID: 12224
			[Token(Token = "0x4005E9D")]
			End,
			// Token: 0x04002FC1 RID: 12225
			[Token(Token = "0x4005E9E")]
			GetGachaState,
			// Token: 0x04002FC2 RID: 12226
			[Token(Token = "0x4005E9F")]
			GetGachaStateWait,
			// Token: 0x04002FC3 RID: 12227
			[Token(Token = "0x4005EA0")]
			EventBannerState,
			// Token: 0x04002FC4 RID: 12228
			[Token(Token = "0x4005EA1")]
			EventBannerStateWait,
			// Token: 0x04002FC5 RID: 12229
			[Token(Token = "0x4005EA2")]
			EventBannerStateEnd
		}
	}
}
