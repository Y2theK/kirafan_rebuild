﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008C0 RID: 2240
	[Token(Token = "0x200066A")]
	[StructLayout(3)]
	public class MsbHndlWrapper
	{
		// Token: 0x17000283 RID: 643
		// (get) Token: 0x060024CC RID: 9420 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700025C")]
		public MsbHandler MsbHndl
		{
			[Token(Token = "0x60021F0")]
			[Address(RVA = "0x101267E80", Offset = "0x1267E80", VA = "0x101267E80")]
			get
			{
				return null;
			}
		}

		// Token: 0x060024CD RID: 9421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F1")]
		[Address(RVA = "0x101267E88", Offset = "0x1267E88", VA = "0x101267E88")]
		public MsbHndlWrapper()
		{
		}

		// Token: 0x060024CE RID: 9422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F2")]
		[Address(RVA = "0x101267E90", Offset = "0x1267E90", VA = "0x101267E90")]
		public void Setup(MsbHandler msbHndl)
		{
		}

		// Token: 0x060024CF RID: 9423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F3")]
		[Address(RVA = "0x101267E98", Offset = "0x1267E98", VA = "0x101267E98")]
		public void Destroy()
		{
		}

		// Token: 0x060024D0 RID: 9424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F4")]
		[Address(RVA = "0x101267F58", Offset = "0x1267F58", VA = "0x101267F58")]
		public void Update()
		{
		}

		// Token: 0x060024D1 RID: 9425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F5")]
		[Address(RVA = "0x101267FE0", Offset = "0x1267FE0", VA = "0x101267FE0")]
		private void UpdateFade()
		{
		}

		// Token: 0x060024D2 RID: 9426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F6")]
		[Address(RVA = "0x101268278", Offset = "0x1268278", VA = "0x101268278")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x060024D3 RID: 9427 RVA: 0x0000FD20 File Offset: 0x0000DF20
		[Token(Token = "0x60021F7")]
		[Address(RVA = "0x1012682B8", Offset = "0x12682B8", VA = "0x1012682B8")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x060024D4 RID: 9428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F8")]
		[Address(RVA = "0x1012682E4", Offset = "0x12682E4", VA = "0x1012682E4")]
		public void SetMeshColor(Color color)
		{
		}

		// Token: 0x060024D5 RID: 9429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021F9")]
		[Address(RVA = "0x1012680AC", Offset = "0x12680AC", VA = "0x1012680AC")]
		private void SetMeshAlpha(float a)
		{
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FA")]
		[Address(RVA = "0x1012684BC", Offset = "0x12684BC", VA = "0x1012684BC")]
		public void ResetMeshColor()
		{
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FB")]
		[Address(RVA = "0x1012684E8", Offset = "0x12684E8", VA = "0x1012684E8")]
		public void ResetMeshVisibility()
		{
		}

		// Token: 0x060024D8 RID: 9432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FC")]
		[Address(RVA = "0x101268584", Offset = "0x1268584", VA = "0x101268584")]
		public void KillParticle()
		{
		}

		// Token: 0x060024D9 RID: 9433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FD")]
		[Address(RVA = "0x1012686B4", Offset = "0x12686B4", VA = "0x1012686B4")]
		public void SetTexture(string objName, Texture texture)
		{
		}

		// Token: 0x060024DA RID: 9434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FE")]
		[Address(RVA = "0x101268878", Offset = "0x1268878", VA = "0x101268878")]
		public void SetLayer(GameObject go, string layerName)
		{
		}

		// Token: 0x060024DB RID: 9435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60021FF")]
		[Address(RVA = "0x101268888", Offset = "0x1268888", VA = "0x101268888")]
		public static void SetLayer(GameObject go, MsbHandler msbHndl, string layerName)
		{
		}

		// Token: 0x060024DC RID: 9436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002200")]
		[Address(RVA = "0x1012689FC", Offset = "0x12689FC", VA = "0x1012689FC")]
		public void SetSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x060024DD RID: 9437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002201")]
		[Address(RVA = "0x101268A04", Offset = "0x1268A04", VA = "0x101268A04")]
		public static void SetSortingOrder(MsbHandler msbHndl, int sortingOrder)
		{
		}

		// Token: 0x060024DE RID: 9438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002202")]
		[Address(RVA = "0x101268BFC", Offset = "0x1268BFC", VA = "0x101268BFC")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x060024DF RID: 9439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002203")]
		[Address(RVA = "0x101267EBC", Offset = "0x1267EBC", VA = "0x101267EBC")]
		public void DetachCamera()
		{
		}

		// Token: 0x060024E0 RID: 9440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002204")]
		[Address(RVA = "0x101268CB0", Offset = "0x1268CB0", VA = "0x101268CB0")]
		public void ResetCamera()
		{
		}

		// Token: 0x040034C9 RID: 13513
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400269A")]
		private MsbHandler m_MsbHndl;

		// Token: 0x040034CA RID: 13514
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400269B")]
		private float m_StartAlpha;

		// Token: 0x040034CB RID: 13515
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400269C")]
		private float m_EndAlpha;

		// Token: 0x040034CC RID: 13516
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400269D")]
		private float m_FadeTime;

		// Token: 0x040034CD RID: 13517
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400269E")]
		private float m_FadeTimer;

		// Token: 0x040034CE RID: 13518
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400269F")]
		private bool m_IsUpdateMeshColor;
	}
}
