﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000561 RID: 1377
	[Token(Token = "0x2000454")]
	[StructLayout(3)]
	public class NamedFriendshipExpDB : ScriptableObject
	{
		// Token: 0x060015DC RID: 5596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148C")]
		[Address(RVA = "0x101268D88", Offset = "0x1268D88", VA = "0x101268D88")]
		public NamedFriendshipExpDB()
		{
		}

		// Token: 0x04001913 RID: 6419
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400127D")]
		public NamedFriendshipExpDB_Param[] m_Params;
	}
}
