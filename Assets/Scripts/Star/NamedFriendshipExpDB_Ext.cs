﻿namespace Star {
    public static class NamedFriendshipExpDB_Ext {
        public static NamedFriendshipExpDB_Param GetParam(this NamedFriendshipExpDB self, int friendship, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                idx = idx + friendship - 1;
                if (idx >= 0 && idx < self.m_Params.Length) {
                    return self.m_Params[idx];
                }
            }
            return default;
        }

        public static float GetStatusParam(this NamedFriendshipExpDB self, BattleDefine.eStatus st, int friendship, sbyte expTableID) {
            NamedFriendshipExpDB_Param param = self.GetParam(friendship, expTableID);
            return st switch {
                BattleDefine.eStatus.Atk => param.m_CorrectAtk,
                BattleDefine.eStatus.Mgc => param.m_CorrectMgc,
                BattleDefine.eStatus.Def => param.m_CorrectDef,
                BattleDefine.eStatus.MDef => param.m_CorrectMDef,
                BattleDefine.eStatus.Spd => param.m_CorrectSpd,
                BattleDefine.eStatus.Luck => param.m_CorrectLuck,
                _ => 0f,
            };
        }

        public static int GetNextExp(this NamedFriendshipExpDB self, int friendship, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                idx = idx + friendship - 1;
                if (idx >= 0 && idx < self.m_Params.Length) {
                    return self.m_Params[idx].m_NextExp;
                }
            }
            return 0;
        }

        public static long[] GetNextMaxExps(this NamedFriendshipExpDB self, int lv_a, int lv_b, sbyte expTableID) {
            int lvs = lv_b - lv_a + 1;
            if (lvs <= 0) {
                return null;
            }
            long[] exps = new long[lvs];
            for (int i = 0; i < lvs; i++) {
                exps[i] = self.GetNextExp(lv_a + i, expTableID);
            }
            return exps;
        }

        public static long GetCurrentExp(this NamedFriendshipExpDB self, int currentLv, long exp, sbyte expTableID) {
            long total = 0L;
            for (int i = 1; i < currentLv; i++) {
                total += self.GetNextExp(i, expTableID);
            }
            if (exp >= total) {
                return exp - total;
            }
            return 0L;
        }

        public static int GetFriendShipMax(this NamedFriendshipExpDB self, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            int result = 0;
            if (idx >= 0) {
                for (int j = idx; j < self.m_Params.Length; j++) {
                    if (self.m_Params[j].m_ID != expTableID) {
                        break;
                    }
                    result = self.m_Params[j].m_Lv;
                }
            }
            return result;
        }
    }
}
