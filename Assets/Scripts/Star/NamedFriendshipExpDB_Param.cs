﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000560 RID: 1376
	[Token(Token = "0x2000453")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct NamedFriendshipExpDB_Param
	{
		// Token: 0x04001909 RID: 6409
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001273")]
		public int m_ID;

		// Token: 0x0400190A RID: 6410
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001274")]
		public int m_Lv;

		// Token: 0x0400190B RID: 6411
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001275")]
		public int m_NextExp;

		// Token: 0x0400190C RID: 6412
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001276")]
		public float m_CorrectHp;

		// Token: 0x0400190D RID: 6413
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001277")]
		public float m_CorrectAtk;

		// Token: 0x0400190E RID: 6414
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001278")]
		public float m_CorrectMgc;

		// Token: 0x0400190F RID: 6415
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001279")]
		public float m_CorrectDef;

		// Token: 0x04001910 RID: 6416
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400127A")]
		public float m_CorrectMDef;

		// Token: 0x04001911 RID: 6417
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400127B")]
		public float m_CorrectSpd;

		// Token: 0x04001912 RID: 6418
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400127C")]
		public float m_CorrectLuck;
	}
}
