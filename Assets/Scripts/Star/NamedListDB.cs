﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000563 RID: 1379
	[Token(Token = "0x2000456")]
	[StructLayout(3)]
	public class NamedListDB : ScriptableObject
	{
		// Token: 0x060015DD RID: 5597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148D")]
		[Address(RVA = "0x101269350", Offset = "0x1269350", VA = "0x101269350")]
		public NamedListDB()
		{
		}

		// Token: 0x0400191F RID: 6431
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001289")]
		public NamedListDB_Param[] m_Params;
	}
}
