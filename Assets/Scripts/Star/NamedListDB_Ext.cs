﻿namespace Star {
    public static class NamedListDB_Ext {
        public static bool IsExist(this NamedListDB self, eCharaNamedType namedType) {
            if (namedType != eCharaNamedType.None) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_NamedType == (int)namedType) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static NamedListDB_Param GetParam(this NamedListDB self, eCharaNamedType namedType) {
            if (namedType != eCharaNamedType.None) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_NamedType == (int)namedType) {
                        return self.m_Params[i];
                    }
                }
            }
            return default;
        }

        public static eTitleType GetTitleType(this NamedListDB self, eCharaNamedType namedType) {
            return (eTitleType)self.GetTitleTypeInt((int)namedType);
        }

        public static int GetTitleTypeInt(this NamedListDB self, int namedType) {
            if (namedType != (int)eCharaNamedType.None) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_NamedType == namedType) {
                        return self.m_Params[i].m_TitleType;
                    }
                }
            }
            return (int)eCharaNamedType.None;
        }

        public static sbyte GetFriendshipTableID(this NamedListDB self, eCharaNamedType namedType) {
            if (namedType != eCharaNamedType.None) {
                return self.GetParam(namedType).m_FriendshipTableID;
            }
            return 0;
        }

        public static string GetResourceBaseName(this NamedListDB self, eCharaNamedType namedType) {
            if (namedType != eCharaNamedType.None) {
                return self.GetParam(namedType).m_ResouceBaseName;
            }
            return string.Empty;
        }
    }
}
