﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000562 RID: 1378
	[Token(Token = "0x2000455")]
	[Serializable]
	[StructLayout(0, Size = 64)]
	public struct NamedListDB_Param
	{
		// Token: 0x04001914 RID: 6420
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400127E")]
		public int m_NamedType;

		// Token: 0x04001915 RID: 6421
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400127F")]
		public int m_TitleType;

		// Token: 0x04001916 RID: 6422
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001280")]
		public string m_ResouceBaseName;

		// Token: 0x04001917 RID: 6423
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001281")]
		public string m_NickName;

		// Token: 0x04001918 RID: 6424
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001282")]
		public string m_FullName;

		// Token: 0x04001919 RID: 6425
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001283")]
		public int m_BattleWinID;

		// Token: 0x0400191A RID: 6426
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001284")]
		public sbyte m_FriendshipTableID;

		// Token: 0x0400191B RID: 6427
		[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
		[Token(Token = "0x4001285")]
		public sbyte m_PersonalityType;

		// Token: 0x0400191C RID: 6428
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001286")]
		public string m_ProfileText;

		// Token: 0x0400191D RID: 6429
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001287")]
		public string m_CVText;

		// Token: 0x0400191E RID: 6430
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001288")]
		public int m_DropItemKey;
	}
}
