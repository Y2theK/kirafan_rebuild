﻿using System;

namespace Star {
    [Serializable]
    public class NamedParam {
        public int FriendShip { get; set; }
        public int FriendShipMax { get; set; }
        public bool IsFriendShipMax => FriendShip >= FriendShipMax;
    }
}
