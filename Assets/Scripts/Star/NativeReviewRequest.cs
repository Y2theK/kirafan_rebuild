﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AEF RID: 2799
	[Token(Token = "0x20007A6")]
	[StructLayout(3)]
	public class NativeReviewRequest
	{
		// Token: 0x0600316E RID: 12654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D2F")]
		[Address(RVA = "0x101269538", Offset = "0x1269538", VA = "0x101269538")]
		public static void RequestReview()
		{
		}

		// Token: 0x0600316F RID: 12655
		[Token(Token = "0x6002D30")]
		[Address(RVA = "0x10126953C", Offset = "0x126953C", VA = "0x10126953C")]
		private static extern void requestReview();

		// Token: 0x06003170 RID: 12656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D31")]
		[Address(RVA = "0x101269540", Offset = "0x1269540", VA = "0x101269540")]
		public NativeReviewRequest()
		{
		}
	}
}
