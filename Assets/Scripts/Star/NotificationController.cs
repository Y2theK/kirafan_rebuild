﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AE3 RID: 2787
	[Token(Token = "0x20007A1")]
	[StructLayout(3)]
	public class NotificationController
	{
		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x0600312F RID: 12591 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06003130 RID: 12592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700038B")]
		public bool[] PushFlags
		{
			[Token(Token = "0x6002CF8")]
			[Address(RVA = "0x101269548", Offset = "0x1269548", VA = "0x101269548")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002CF9")]
			[Address(RVA = "0x101269550", Offset = "0x1269550", VA = "0x101269550")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x14000058 RID: 88
		// (add) Token: 0x06003131 RID: 12593 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003132 RID: 12594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000058")]
		private event Action m_OnResponse_SetPushNotification
		{
			[Token(Token = "0x6002CFA")]
			[Address(RVA = "0x101269558", Offset = "0x1269558", VA = "0x101269558")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002CFB")]
			[Address(RVA = "0x101269664", Offset = "0x1269664", VA = "0x101269664")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003133 RID: 12595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CFC")]
		[Address(RVA = "0x101269770", Offset = "0x1269770", VA = "0x101269770")]
		public NotificationController()
		{
		}

		// Token: 0x06003134 RID: 12596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CFD")]
		[Address(RVA = "0x1012697D4", Offset = "0x12697D4", VA = "0x1012697D4")]
		public void Save()
		{
		}

		// Token: 0x06003135 RID: 12597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002CFE")]
		[Address(RVA = "0x1012698D8", Offset = "0x12698D8", VA = "0x1012698D8")]
		public void RestoreFromSave()
		{
		}

		// Token: 0x06003136 RID: 12598 RVA: 0x00015138 File Offset: 0x00013338
		[Token(Token = "0x6002CFF")]
		[Address(RVA = "0x1012566EC", Offset = "0x12566EC", VA = "0x1012566EC")]
		public bool IsDirty()
		{
			return default(bool);
		}

		// Token: 0x06003137 RID: 12599 RVA: 0x00015150 File Offset: 0x00013350
		[Token(Token = "0x6002D00")]
		[Address(RVA = "0x1012699D4", Offset = "0x12699D4", VA = "0x1012699D4")]
		public bool GetPushFlag(NotificationController.ePush type)
		{
			return default(bool);
		}

		// Token: 0x06003138 RID: 12600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D01")]
		[Address(RVA = "0x101269A34", Offset = "0x1269A34", VA = "0x101269A34")]
		public void SetPushFlag(NotificationController.ePush type, bool isOn)
		{
		}

		// Token: 0x06003139 RID: 12601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D02")]
		[Address(RVA = "0x101269A90", Offset = "0x1269A90", VA = "0x101269A90")]
		public void SetPushFlagsAll(bool isOn)
		{
		}

		// Token: 0x0600313A RID: 12602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D03")]
		[Address(RVA = "0x1012567E8", Offset = "0x12567E8", VA = "0x1012567E8")]
		public void Request_SetPushNotification(Action response)
		{
		}

		// Token: 0x0600313B RID: 12603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D04")]
		[Address(RVA = "0x101269B20", Offset = "0x1269B20", VA = "0x101269B20")]
		public void OnResponse_SetPushNotification(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600313C RID: 12604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D05")]
		[Address(RVA = "0x101269CDC", Offset = "0x1269CDC", VA = "0x101269CDC")]
		public void OnAwake()
		{
		}

		// Token: 0x0600313D RID: 12605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D06")]
		[Address(RVA = "0x101269CE0", Offset = "0x1269CE0", VA = "0x101269CE0")]
		public void OnQuit()
		{
		}

		// Token: 0x0600313E RID: 12606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D07")]
		[Address(RVA = "0x101269CE4", Offset = "0x1269CE4", VA = "0x101269CE4")]
		public void LocalPush_StaminaFull()
		{
		}

		// Token: 0x0600313F RID: 12607 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D08")]
		[Address(RVA = "0x101269DE0", Offset = "0x1269DE0", VA = "0x101269DE0")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x10013A0A8", Offset = "0x13A0A8")]
		private IEnumerator StaminaFullProc()
		{
			return null;
		}

		// Token: 0x06003140 RID: 12608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D09")]
		[Address(RVA = "0x101269E54", Offset = "0x1269E54", VA = "0x101269E54")]
		public void CancelAll()
		{
		}

		// Token: 0x06003141 RID: 12609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D0A")]
		[Address(RVA = "0x101269E5C", Offset = "0x1269E5C", VA = "0x101269E5C")]
		public void Push_StaminaFull()
		{
		}

		// Token: 0x06003142 RID: 12610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D0B")]
		[Address(RVA = "0x101269C20", Offset = "0x1269C20", VA = "0x101269C20")]
		public void Cancel_StaminaFull()
		{
		}

		// Token: 0x0400407E RID: 16510
		[Token(Token = "0x4002E11")]
		public const int ID_StaminaFull = 100;

		// Token: 0x04004080 RID: 16512
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E13")]
		private bool[] m_SavedPushFlags;

		// Token: 0x04004082 RID: 16514
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E15")]
		private Coroutine m_Coroutine_StaminaFull;

		// Token: 0x02000AE4 RID: 2788
		[Token(Token = "0x200100F")]
		public enum ePush
		{
			// Token: 0x04004084 RID: 16516
			[Token(Token = "0x40065DA")]
			ForUIFlag,
			// Token: 0x04004085 RID: 16517
			[Token(Token = "0x40065DB")]
			InfoFromMng,
			// Token: 0x04004086 RID: 16518
			[Token(Token = "0x40065DC")]
			StaminaFull,
			// Token: 0x04004087 RID: 16519
			[Token(Token = "0x40065DD")]
			Num
		}
	}
}
