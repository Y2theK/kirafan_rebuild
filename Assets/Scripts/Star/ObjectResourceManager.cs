﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020006FD RID: 1789
	[Token(Token = "0x200058F")]
	[StructLayout(3)]
	public class ObjectResourceManager : MonoBehaviour
	{
		// Token: 0x06001A09 RID: 6665 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600181E")]
		[Address(RVA = "0x10126A228", Offset = "0x126A228", VA = "0x10126A228")]
		public static MeigeResource.Handler CreateHandle(string pfilename)
		{
			return null;
		}

		// Token: 0x06001A0A RID: 6666 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600181F")]
		[Address(RVA = "0x10126A644", Offset = "0x126A644", VA = "0x10126A644")]
		public static MeigeResource.Handler CreateHandle(string pfilename, Action<MeigeResource.Handler> pcallback)
		{
			return null;
		}

		// Token: 0x06001A0B RID: 6667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001820")]
		[Address(RVA = "0x10126A7AC", Offset = "0x126A7AC", VA = "0x10126A7AC")]
		public static void PreloadHandle(string pfilename)
		{
		}

		// Token: 0x06001A0C RID: 6668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001821")]
		[Address(RVA = "0x10126A8E8", Offset = "0x126A8E8", VA = "0x10126A8E8")]
		public static void RetryHandle(MeigeResource.Handler hndl)
		{
		}

		// Token: 0x06001A0D RID: 6669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001822")]
		[Address(RVA = "0x10126A978", Offset = "0x126A978", VA = "0x10126A978")]
		public static void ClearError(MeigeResource.Handler hndl)
		{
		}

		// Token: 0x06001A0E RID: 6670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001823")]
		[Address(RVA = "0x10126A9FC", Offset = "0x126A9FC", VA = "0x10126A9FC")]
		public static void ReleaseHandle(MeigeResource.Handler phandle)
		{
		}

		// Token: 0x06001A0F RID: 6671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001824")]
		[Address(RVA = "0x10126ADE4", Offset = "0x126ADE4", VA = "0x10126ADE4")]
		public static void ObjectDestroy(GameObject pobj, bool funloader = false)
		{
		}

		// Token: 0x06001A10 RID: 6672 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001825")]
		[Address(RVA = "0x10126A360", Offset = "0x126A360", VA = "0x10126A360")]
		private MeigeResource.Handler ChkReshandleRefCnt(string presname)
		{
			return null;
		}

		// Token: 0x06001A11 RID: 6673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001826")]
		[Address(RVA = "0x10126A4C0", Offset = "0x126A4C0", VA = "0x10126A4C0")]
		private void AddReshandleRefCnt(MeigeResource.Handler phandle, string fchkname, int fstep)
		{
		}

		// Token: 0x06001A12 RID: 6674 RVA: 0x0000BAA8 File Offset: 0x00009CA8
		[Token(Token = "0x6001827")]
		[Address(RVA = "0x10126AA64", Offset = "0x126AA64", VA = "0x10126AA64")]
		private bool CheckReshandleRefCnt(MeigeResource.Handler phandle)
		{
			return default(bool);
		}

		// Token: 0x06001A13 RID: 6675 RVA: 0x0000BAC0 File Offset: 0x00009CC0
		[Token(Token = "0x6001828")]
		[Address(RVA = "0x10126AEE0", Offset = "0x126AEE0", VA = "0x10126AEE0")]
		public static bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x06001A14 RID: 6676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001829")]
		[Address(RVA = "0x10126AF70", Offset = "0x126AF70", VA = "0x10126AF70")]
		public static void SetUpManager(GameObject ptarget)
		{
		}

		// Token: 0x06001A15 RID: 6677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600182A")]
		[Address(RVA = "0x10126B068", Offset = "0x126B068", VA = "0x10126B068")]
		public static void ReleaseCheck()
		{
		}

		// Token: 0x06001A16 RID: 6678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600182B")]
		[Address(RVA = "0x10126B438", Offset = "0x126B438", VA = "0x10126B438")]
		private void Awake()
		{
		}

		// Token: 0x06001A17 RID: 6679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600182C")]
		[Address(RVA = "0x10126B534", Offset = "0x126B534", VA = "0x10126B534")]
		public void Update()
		{
		}

		// Token: 0x06001A18 RID: 6680 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600182D")]
		[Address(RVA = "0x10126BC20", Offset = "0x126BC20", VA = "0x10126BC20")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x100138474", Offset = "0x138474")]
		private IEnumerator UnloadObjectCash()
		{
			return null;
		}

		// Token: 0x06001A19 RID: 6681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600182E")]
		[Address(RVA = "0x10126B160", Offset = "0x126B160", VA = "0x10126B160")]
		private void AllResourceRelease()
		{
		}

		// Token: 0x06001A1A RID: 6682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600182F")]
		[Address(RVA = "0x10126BCC0", Offset = "0x126BCC0", VA = "0x10126BCC0")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001A1B RID: 6683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001830")]
		[Address(RVA = "0x10126BD68", Offset = "0x126BD68", VA = "0x10126BD68")]
		public static void EntryResMngQue(IObjectResourceHandle pque, string pfilename, string pext)
		{
		}

		// Token: 0x06001A1C RID: 6684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001831")]
		[Address(RVA = "0x10126C374", Offset = "0x126C374", VA = "0x10126C374")]
		public static void EntryResQue(IObjectResourceHandle pque, string pfilename, string pext)
		{
		}

		// Token: 0x06001A1D RID: 6685 RVA: 0x0000BAD8 File Offset: 0x00009CD8
		[Token(Token = "0x6001832")]
		[Address(RVA = "0x10126C47C", Offset = "0x126C47C", VA = "0x10126C47C")]
		private int EntryResource(IObjectResourceHandle phandle, string filepath, string fileext, bool fweb)
		{
			return 0;
		}

		// Token: 0x06001A1E RID: 6686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001833")]
		[Address(RVA = "0x10126C77C", Offset = "0x126C77C", VA = "0x10126C77C")]
		private void LoadQueCheck(int fresno)
		{
		}

		// Token: 0x06001A1F RID: 6687 RVA: 0x0000BAF0 File Offset: 0x00009CF0
		[Token(Token = "0x6001834")]
		[Address(RVA = "0x10126BE70", Offset = "0x126BE70", VA = "0x10126BE70")]
		private int EntryResManager(IObjectResourceHandle phandle, string filepath, string fileext)
		{
			return 0;
		}

		// Token: 0x06001A20 RID: 6688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001835")]
		[Address(RVA = "0x10126C220", Offset = "0x126C220", VA = "0x10126C220")]
		private void LoadQueMngCheck(int fresno, bool fwww = false)
		{
		}

		// Token: 0x06001A21 RID: 6689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001836")]
		[Address(RVA = "0x10126CA60", Offset = "0x126CA60", VA = "0x10126CA60")]
		private void ReleaseRes(IObjectResourceHandle phandle)
		{
		}

		// Token: 0x06001A22 RID: 6690 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001837")]
		[Address(RVA = "0x10126CBDC", Offset = "0x126CBDC", VA = "0x10126CBDC")]
		public static string GetPlatformAssetPath()
		{
			return null;
		}

		// Token: 0x06001A23 RID: 6691 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001838")]
		[Address(RVA = "0x10126CC78", Offset = "0x126CC78", VA = "0x10126CC78")]
		public static string GetPlatformExt()
		{
			return null;
		}

		// Token: 0x06001A24 RID: 6692 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001839")]
		[Address(RVA = "0x10126BB88", Offset = "0x126BB88", VA = "0x10126BB88")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x1001384D8", Offset = "0x1384D8")]
		private IEnumerator ResourceLoader(IObjectResourceHandle pstate)
		{
			return null;
		}

		// Token: 0x06001A25 RID: 6693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600183A")]
		[Address(RVA = "0x10126CCEC", Offset = "0x126CCEC", VA = "0x10126CCEC")]
		public ObjectResourceManager()
		{
		}

		// Token: 0x04002A50 RID: 10832
		[Token(Token = "0x4002289")]
		private static ObjectResourceManager Inst;

		// Token: 0x04002A51 RID: 10833
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400228A")]
		private IObjectResourceHandle[] m_ResTable;

		// Token: 0x04002A52 RID: 10834
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400228B")]
		private int m_ResMax;

		// Token: 0x04002A53 RID: 10835
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400228C")]
		private int[] m_WaitResTable;

		// Token: 0x04002A54 RID: 10836
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400228D")]
		private int m_WaitResNum;

		// Token: 0x04002A55 RID: 10837
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400228E")]
		private int m_WaitResMax;

		// Token: 0x04002A56 RID: 10838
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400228F")]
		private byte m_ResUp;

		// Token: 0x04002A57 RID: 10839
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002290")]
		private IObjectResourceHandle[] m_ResMngTable;

		// Token: 0x04002A58 RID: 10840
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002291")]
		private int m_ResMngMax;

		// Token: 0x04002A59 RID: 10841
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002292")]
		private int[] m_WaitResMngTable;

		// Token: 0x04002A5A RID: 10842
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002293")]
		private int m_WaitResMngNum;

		// Token: 0x04002A5B RID: 10843
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002294")]
		private int m_WaitResMngMax;

		// Token: 0x04002A5C RID: 10844
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002295")]
		private int m_ResourceRefresh;

		// Token: 0x04002A5D RID: 10845
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002296")]
		private bool m_UncashPlay;

		// Token: 0x04002A5E RID: 10846
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002297")]
		private List<ObjectResourceManager.MeigeHandleRefState> m_MeigeHandleTable;

		// Token: 0x020006FE RID: 1790
		[Token(Token = "0x2000E40")]
		public class MeigeHandleRefState
		{
			// Token: 0x06001A26 RID: 6694 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EAD")]
			[Address(RVA = "0x10126AED8", Offset = "0x126AED8", VA = "0x10126AED8")]
			public MeigeHandleRefState()
			{
			}

			// Token: 0x04002A5F RID: 10847
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B40")]
			public int m_RefCnt;

			// Token: 0x04002A60 RID: 10848
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B41")]
			public int m_Step;

			// Token: 0x04002A61 RID: 10849
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B42")]
			public string m_Name;

			// Token: 0x04002A62 RID: 10850
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B43")]
			public MeigeResource.Handler m_Handle;
		}
	}
}
