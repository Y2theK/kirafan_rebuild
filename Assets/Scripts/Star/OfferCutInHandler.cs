﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008F7 RID: 2295
	[Token(Token = "0x2000698")]
	[StructLayout(3)]
	public class OfferCutInHandler : MonoBehaviour
	{
		// Token: 0x060025A6 RID: 9638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C2")]
		[Address(RVA = "0x10126CFD0", Offset = "0x126CFD0", VA = "0x10126CFD0")]
		private void Awake()
		{
		}

		// Token: 0x060025A7 RID: 9639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C3")]
		[Address(RVA = "0x10126D1C4", Offset = "0x126D1C4", VA = "0x10126D1C4")]
		private void Update()
		{
		}

		// Token: 0x060025A8 RID: 9640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C4")]
		[Address(RVA = "0x10126D1F4", Offset = "0x126D1F4", VA = "0x10126D1F4")]
		public void Play()
		{
		}

		// Token: 0x060025A9 RID: 9641 RVA: 0x000100C8 File Offset: 0x0000E2C8
		[Token(Token = "0x60022C5")]
		[Address(RVA = "0x10126D2B4", Offset = "0x126D2B4", VA = "0x10126D2B4")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x060025AA RID: 9642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C6")]
		[Address(RVA = "0x10126D358", Offset = "0x126D358", VA = "0x10126D358")]
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x060025AB RID: 9643 RVA: 0x000100E0 File Offset: 0x0000E2E0
		[Token(Token = "0x60022C7")]
		[Address(RVA = "0x10126D3B0", Offset = "0x126D3B0", VA = "0x10126D3B0")]
		public bool IsFading()
		{
			return default(bool);
		}

		// Token: 0x060025AC RID: 9644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022C8")]
		[Address(RVA = "0x10126D3E0", Offset = "0x126D3E0", VA = "0x10126D3E0")]
		public OfferCutInHandler()
		{
		}

		// Token: 0x040035D3 RID: 13779
		[Token(Token = "0x4002781")]
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x040035D4 RID: 13780
		[Token(Token = "0x4002782")]
		private const string PLAY_KEY = "Take 001";

		// Token: 0x040035D5 RID: 13781
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002783")]
		public Transform m_Transform;

		// Token: 0x040035D6 RID: 13782
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002784")]
		public Animation m_Anim;

		// Token: 0x040035D7 RID: 13783
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002785")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040035D8 RID: 13784
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002786")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x040035D9 RID: 13785
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002787")]
		public MsbHandler m_MsbHndl;

		// Token: 0x040035DA RID: 13786
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002788")]
		public MsbHndlWrapper m_MsbHndlWrapper;
	}
}
