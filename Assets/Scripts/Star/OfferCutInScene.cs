﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008F8 RID: 2296
	[Token(Token = "0x2000699")]
	[StructLayout(3)]
	public class OfferCutInScene
	{
		// Token: 0x1700028B RID: 651
		// (get) Token: 0x060025AD RID: 9645 RVA: 0x000100F8 File Offset: 0x0000E2F8
		// (set) Token: 0x060025AE RID: 9646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000264")]
		public bool Skipped
		{
			[Token(Token = "0x60022C9")]
			[Address(RVA = "0x10126D448", Offset = "0x126D448", VA = "0x10126D448")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60022CA")]
			[Address(RVA = "0x10126D450", Offset = "0x126D450", VA = "0x10126D450")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x060025AF RID: 9647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022CB")]
		[Address(RVA = "0x10126D458", Offset = "0x126D458", VA = "0x10126D458")]
		public OfferCutInScene(string resourcePath, string objectPath)
		{
		}

		// Token: 0x060025B0 RID: 9648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022CC")]
		[Address(RVA = "0x10126D520", Offset = "0x126D520", VA = "0x10126D520", Slot = "4")]
		public virtual void Destroy()
		{
		}

		// Token: 0x060025B1 RID: 9649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022CD")]
		[Address(RVA = "0x10126D5E0", Offset = "0x126D5E0", VA = "0x10126D5E0")]
		public void Update()
		{
		}

		// Token: 0x060025B2 RID: 9650 RVA: 0x00010110 File Offset: 0x0000E310
		[Token(Token = "0x60022CE")]
		[Address(RVA = "0x10126D998", Offset = "0x126D998", VA = "0x10126D998")]
		public bool Prepare(Transform parentTransform, Vector3 position)
		{
			return default(bool);
		}

		// Token: 0x060025B3 RID: 9651 RVA: 0x00010128 File Offset: 0x0000E328
		[Token(Token = "0x60022CF")]
		[Address(RVA = "0x10126DA48", Offset = "0x126DA48", VA = "0x10126DA48")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x060025B4 RID: 9652 RVA: 0x00010140 File Offset: 0x0000E340
		[Token(Token = "0x60022D0")]
		[Address(RVA = "0x10126DA58", Offset = "0x126DA58", VA = "0x10126DA58")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x060025B5 RID: 9653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022D1")]
		[Address(RVA = "0x10126D5FC", Offset = "0x126D5FC", VA = "0x10126D5FC")]
		private void Update_Prepare()
		{
		}

		// Token: 0x060025B6 RID: 9654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022D2")]
		[Address(RVA = "0x10126DA68", Offset = "0x126DA68", VA = "0x10126DA68", Slot = "5")]
		public virtual void Play()
		{
		}

		// Token: 0x060025B7 RID: 9655 RVA: 0x00010158 File Offset: 0x0000E358
		[Token(Token = "0x60022D3")]
		[Address(RVA = "0x10126DB08", Offset = "0x126DB08", VA = "0x10126DB08")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x060025B8 RID: 9656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022D4")]
		[Address(RVA = "0x10126D898", Offset = "0x126D898", VA = "0x10126D898")]
		private void Update_Main()
		{
		}

		// Token: 0x060025B9 RID: 9657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60022D5")]
		[Address(RVA = "0x10126DB2C", Offset = "0x126DB2C", VA = "0x10126DB2C", Slot = "6")]
		protected virtual void OnSkip()
		{
		}

		// Token: 0x040035DB RID: 13787
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002789")]
		private OfferCutInScene.eMode m_Mode;

		// Token: 0x040035DC RID: 13788
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400278A")]
		private bool m_IsDonePrepareFirst;

		// Token: 0x040035DD RID: 13789
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400278B")]
		private ABResourceLoader m_Loader;

		// Token: 0x040035DE RID: 13790
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400278C")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x040035DF RID: 13791
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400278D")]
		private GameObject m_EffectGO;

		// Token: 0x040035E0 RID: 13792
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400278E")]
		private OfferCutInHandler m_EffectHndl;

		// Token: 0x040035E1 RID: 13793
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400278F")]
		private Transform m_ParentTransform;

		// Token: 0x040035E2 RID: 13794
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002790")]
		private Vector3 m_Position;

		// Token: 0x040035E4 RID: 13796
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002792")]
		private readonly string m_ResourcePath;

		// Token: 0x040035E5 RID: 13797
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002793")]
		private readonly string m_ObjectPath;

		// Token: 0x040035E6 RID: 13798
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002794")]
		private OfferCutInScene.ePrepareStep m_PrepareStep;

		// Token: 0x040035E7 RID: 13799
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002795")]
		private OfferCutInScene.eMainStep m_MainStep;

		// Token: 0x020008F9 RID: 2297
		[Token(Token = "0x2000F2F")]
		public enum eMode
		{
			// Token: 0x040035E9 RID: 13801
			[Token(Token = "0x40061C6")]
			None = -1,
			// Token: 0x040035EA RID: 13802
			[Token(Token = "0x40061C7")]
			Prepare,
			// Token: 0x040035EB RID: 13803
			[Token(Token = "0x40061C8")]
			UpdateMain,
			// Token: 0x040035EC RID: 13804
			[Token(Token = "0x40061C9")]
			Destroy
		}

		// Token: 0x020008FA RID: 2298
		[Token(Token = "0x2000F30")]
		private enum ePrepareStep
		{
			// Token: 0x040035EE RID: 13806
			[Token(Token = "0x40061CB")]
			None = -1,
			// Token: 0x040035EF RID: 13807
			[Token(Token = "0x40061CC")]
			Prepare,
			// Token: 0x040035F0 RID: 13808
			[Token(Token = "0x40061CD")]
			Prepare_Wait,
			// Token: 0x040035F1 RID: 13809
			[Token(Token = "0x40061CE")]
			End,
			// Token: 0x040035F2 RID: 13810
			[Token(Token = "0x40061CF")]
			Prepare_Error
		}

		// Token: 0x020008FB RID: 2299
		[Token(Token = "0x2000F31")]
		private enum eMainStep
		{
			// Token: 0x040035F4 RID: 13812
			[Token(Token = "0x40061D1")]
			None = -1,
			// Token: 0x040035F5 RID: 13813
			[Token(Token = "0x40061D2")]
			Play,
			// Token: 0x040035F6 RID: 13814
			[Token(Token = "0x40061D3")]
			End
		}
	}
}
