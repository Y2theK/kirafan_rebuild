﻿namespace Star {
    public static class OfferDefine {
        public const int SUBOFFER_UNLOCK_ITEM_ID = 50000;
        public const int SUBOFFER_UNLOCK_ITEM_USE_NUM = 1;

        public enum eState {
            Nothing,
            Occurred,
            WIP,
            WIP_Reach,
            Completed
        }

        public enum eCategory {
            None = -1,
            Main = 1,
            MainRepeat,
            Sub
        }

        public enum eFuncType {
            QuestClear = 1,
            PartyEditQuestClear,
            UniqueSkillLv
        }
    }
}
