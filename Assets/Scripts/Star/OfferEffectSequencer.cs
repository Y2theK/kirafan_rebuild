﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000789 RID: 1929
	[Token(Token = "0x20005C7")]
	[StructLayout(3)]
	public class OfferEffectSequencer
	{
		// Token: 0x06001D2F RID: 7471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AAD")]
		[Address(RVA = "0x10126DB70", Offset = "0x126DB70", VA = "0x10126DB70", Slot = "4")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x0000D080 File Offset: 0x0000B280
		[Token(Token = "0x6001AAE")]
		[Address(RVA = "0x10126DB74", Offset = "0x126DB74", VA = "0x10126DB74", Slot = "5")]
		public virtual bool Update()
		{
			return default(bool);
		}

		// Token: 0x06001D31 RID: 7473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AAF")]
		[Address(RVA = "0x10126DB7C", Offset = "0x126DB7C", VA = "0x10126DB7C")]
		public OfferEffectSequencer()
		{
		}
	}
}
