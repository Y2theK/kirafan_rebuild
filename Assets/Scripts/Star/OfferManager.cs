﻿using Meige;
using System;
using System.Collections.Generic;
using WWWTypes;
using Req = OfferRequestTypes;
using ReqSetshown = ContentroomRequestTypes.Setshown;
using Resp = OfferResponseTypes;
using RespSetshown = ContentroomResponseTypes.Setshown;

namespace Star {
    public sealed class OfferManager {
        private TitleData[] m_Titles;
        private MainOffer[] m_MainOffers;
        private SubOffer[] m_SubOffers;
        private List<int> m_ChangedToWipReachCharaIDsOnSubOffer;

        private event Action m_OnResponse_SetShown;
        private event Action m_OnResponse_GetAll;
        private event Action m_OnResponse_Order;
        private event Action<ResultCode> m_OnResponse_Order_Failed;
        private event Action m_OnResponse_Complete;

        public void ForceResetOnReturnTitle() {
            m_SubOffers = null;
            m_OnResponse_GetAll = null;
            m_OnResponse_SetShown = null;
            m_OnResponse_Order_Failed = null;
            m_OnResponse_Order = null;
            m_OnResponse_Complete = null;
        }

        public TitleData[] GetTitles() {
            return m_Titles;
        }

        public TitleData GetTitle(eTitleType titleType) {
            return GetTitle((int)titleType);
        }

        public TitleData GetTitle(int titleType) {
            return m_Titles != null ? m_Titles[titleType] : null;
        }

        public MainOffer GetMainOffer(eTitleType titleType) {
            return GetMainOffer((int)titleType);
        }

        public MainOffer GetMainOffer(int titleType) {
            return m_MainOffers != null ? m_MainOffers[titleType] : null;
        }

        public OfferData GetMainOfferData(eTitleType titleType, long mngID) {
            return GetMainOfferData((int)titleType, mngID);
        }

        public OfferData GetMainOfferData(int titleType, long mngID) {
            MainOffer offer = GetMainOffer(titleType);
            if (offer != null) {
                for (int i = 0; i < offer.m_Datas.Count; i++) {
                    if (offer.m_Datas[i].m_MngID == mngID) {
                        return offer.m_Datas[i];
                    }
                }
            }
            return null;
        }

        public eTitleType[] GetUnlockNoticeTitleTypes() {
            if (m_Titles != null) {
                int count = 0;
                for (int i = 0; i < m_Titles.Length; i++) {
                    if (m_Titles[i].m_IsUnlockNotice) {
                        count++;
                    }
                }
                if (count > 0) {
                    eTitleType[] res = new eTitleType[count];
                    int idx = 0;
                    for (int i = 0; i < count; i++) {
                        if (m_Titles[i].m_IsUnlockNotice) {
                            res[idx] = m_Titles[i].m_TitleType;
                            idx++;
                        }
                    }
                }
            }
            return null;
        }

        public bool CheckMainOfferStateAll(eTitleType titleType, OfferDefine.eCategory category, OfferDefine.eState state) {
            MainOffer offer = GetMainOffer(titleType);
            if (offer != null && offer.m_Datas != null) {
                for (int i = 0; i < offer.m_Datas.Count; i++) {
                    if (offer.m_Datas[i].m_Category == category) {
                        if (offer.m_Datas[i].m_State != state) {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public SubOffer[] GetSubOffer() {
            return m_SubOffers;
        }

        public SubOffer GetSubOffer(eTitleType titleType) {
            return GetSubOffer((int)titleType);
        }

        public SubOffer GetSubOffer(int titleType) {
            return ExistSubOffer() ? m_SubOffers[titleType] : null;
        }

        public bool ExistSubOffer() {
            return m_SubOffers != null;
        }

        public List<int> GetChangedToWipReachCharaIDsOnSubOffer() {
            return m_ChangedToWipReachCharaIDsOnSubOffer;
        }

        public void ClearChangedToWipReachCharaIDsOnSubOffer() {
            m_ChangedToWipReachCharaIDsOnSubOffer = null;
        }

        public void Request_SetShown(eTitleType[] titleTypes, Action onResponse, bool isOpenConnectingIcon = true) {
            m_OnResponse_SetShown = onResponse;
            ReqSetshown req = new ReqSetshown() {
                titleTypes = new int[titleTypes.Length]
            };
            for (int i = 0; i < titleTypes.Length; i++) {
                req.titleTypes[i] = (int)titleTypes[i];
            }
            MeigewwwParam param = ContentroomRequest.Setshown(req, OnResponse_SetShown);
            NetworkQueueManager.Request(param);
            if (isOpenConnectingIcon) {
                GameSystem.Inst.ConnectingIcon.Open();
            }
        }

        private void OnResponse_SetShown(MeigewwwParam wwwParam) {
            RespSetshown resp = ContentroomResponse.Setshown(wwwParam);
            if (resp != null) {
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
                    return;
                }
                SetTitlesFromWWWType(resp.offerTitleTypes);
                m_OnResponse_SetShown.Call();
                m_OnResponse_SetShown = null;
            }
        }

        public void Request_GetAll(Action onResponse, bool isOpenConnectingIcon = true) {
            m_MainOffers = null;
            m_SubOffers = null;
            m_OnResponse_GetAll = onResponse;
            Req.Getall req = new Req.Getall();
            MeigewwwParam param = OfferRequest.Getall(req, OnResponse_GetAll);
            NetworkQueueManager.Request(param);
            if (isOpenConnectingIcon) {
                GameSystem.Inst.ConnectingIcon.Open();
            }
        }

        private void OnResponse_GetAll(MeigewwwParam wwwParam) {
            Resp.Getall resp = OfferResponse.Getall(wwwParam);
            if (resp != null) {
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
                    return;
                }
                SetOffersFromWWWType(resp.offers);
                m_OnResponse_GetAll.Call();
                m_OnResponse_GetAll = null;
            }
        }

        public void Request_Order(long mngID, Action onResponse, Action<ResultCode> onResponseFailed, bool isOpenConnectingIcon = true) {
            m_OnResponse_Order = onResponse;
            m_OnResponse_Order_Failed = onResponseFailed;
            Req.Order req = new Req.Order() {
                playerOfferId = mngID,
            };
            MeigewwwParam param = OfferRequest.Order(req, OnResponse_Order);
            NetworkQueueManager.Request(param);
            if (isOpenConnectingIcon) {
                GameSystem.Inst.ConnectingIcon.Open();
            }
        }

        private void OnResponse_Order(MeigewwwParam wwwParam) {
            Resp.Order resp = OfferResponse.Order(wwwParam);
            if (resp != null) {
                ResultCode result = resp.GetResult();
                if (result == ResultCode.ITEM_IS_SHORT) {
                    APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
                    m_OnResponse_Order_Failed.Call(result);
                    return;
                } else if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
                    return;
                }
                APIUtility.wwwToUserData(resp.itemSummary, GameSystem.Inst.UserDataMng.UserItemDatas);
                SetChangedToWipReachCharaIDsOnSubOffer(resp.offers, true);
                SetOffersFromWWWType(resp.offers);
                SetTitlesFromWWWType(resp.offerTitleTypes);
                m_OnResponse_Order.Call();
                m_OnResponse_Order = null;
            }
        }

        public void Request_Complete(long mngID, Action onResponse, bool isOpenConnectingIcon = true) {
            m_OnResponse_Complete = onResponse;
            Req.Complete req = new Req.Complete() {
                playerOfferId = mngID,
            };
            MeigewwwParam param = OfferRequest.Complete(req, OnResponse_Complete);
            NetworkQueueManager.Request(param);
            if (isOpenConnectingIcon) {
                GameSystem.Inst.ConnectingIcon.Open();
            }
        }

        private void OnResponse_Complete(MeigewwwParam wwwParam) {
            Resp.Complete resp = OfferResponse.Complete(wwwParam);
            if (resp != null) {
                ResultCode result = resp.GetResult();
                if (result != ResultCode.SUCCESS) {
                    APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
                    return;
                }
                SetOffersFromWWWType(resp.offers);
                SetTitlesFromWWWType(resp.offerTitleTypes);
                UserDataManager dataMng = GameSystem.Inst.UserDataMng;
                APIUtility.wwwToUserData(resp.player, dataMng.UserData);
                APIUtility.wwwToUserData(resp.managedMasterOrbs, dataMng.UserMasterOrbDatas);
                APIUtility.wwwToUserData(resp.itemSummary, dataMng.UserItemDatas);
                m_OnResponse_Complete.Call();
                m_OnResponse_Complete = null;
            }
        }

        public void SetTitlesFromWWWType(OfferTitleType[] src) {
            int count = 0;
            if (src != null) {
                m_Titles = new TitleData[src.Length];
                for (int i = 0; i < src.Length; i++) {
                    TitleData data = new TitleData() {
                        m_TitleType = (eTitleType)src[i].titleType,
                        m_IsReleased = src[i].contentRoomFlg != 0,
                        m_State = (OfferDefine.eState)src[i].state,
                        m_Category = (OfferDefine.eCategory)src[i].category,
                        m_Lv = src[i].offerIndex,
                        m_Point = src[i].offerPoint,
                        m_MaxPoint = src[i].offerMaxPoint,
                        m_IsUnlocked = src[i].offerPoint >= 0,
                        m_IsUnlockNotice = src[i].shown == 0 && src[i].offerPoint >= 0,
                        m_IsExistSubOffer = src[i].subState != 0,
                        m_SubOfferOccurredCount = src[i].subState1,
                        m_SubOfferWipCount = src[i].subState2,
                        m_SubOfferWipReachCount = src[i].subState3,
                    };
                    m_Titles[i] = data;
                    if (data.m_IsUnlocked) {
                        if (data.m_State != OfferDefine.eState.Nothing && data.m_State != OfferDefine.eState.WIP) {
                            count++;
                        }
                    }
                }
                GameSystem.Inst.UserDataMng.UpdateSubOfferCount(m_Titles);
            }
            GameSystem.Inst.UserDataMng.NoticeMainOfferCount = count;
        }

        private void SetOffersFromWWWType(PlayerOffer[] src) {
            if (src == null || src.Length == 0) {
                return;
            }

            int titles = GameSystem.Inst.DbMng.TitleListDB.GetTitleNum();
            m_MainOffers = new MainOffer[titles];
            m_SubOffers = new SubOffer[titles];
            for (int i = 0; i < src.Length; i++) {
                PlayerOffer offer = src[i];
                OfferData data = new OfferData() {
                    m_MngID = offer.id,
                    m_State = (OfferDefine.eState)offer.state,
                    m_ID = offer.offerId,
                    m_TitleType = (eTitleType)offer.titleType,
                    m_Category = (OfferDefine.eCategory)offer.category,
                    m_Lv = offer.offerIndex,
                    m_Point = offer.offerPoint,
                    m_MaxPoint = offer.offerMaxPoint,
                    m_Progress = offer.progress,
                    m_MaxProgress = offer.maxProgress,
                    m_TransitQuestID = offer.transitQuestId,
                    m_TransitQuestName = offer.transitQuestName,
                    m_Title = offer.title,
                    m_ClientName = offer.clientName,
                    m_Body = offer.body,
                    m_FuncType = (OfferDefine.eFuncType)offer.funcType,
                    m_FuncID = offer.funcId
                };
                if (offer.offerRewards != null && offer.offerRewards.Length > 0) {
                    data.m_Rewards = new RewardSet[offer.offerRewards.Length];
                    for (int j = 0; j < offer.offerRewards.Length; j++) {
                        OfferReward reward = offer.offerRewards[j];
                        data.m_Rewards[j] = new RewardSet((ePresentType)reward.contentType, reward.contentId, reward.contentAmount, reward.contentName);
                    }
                }

                int title = (int)data.m_TitleType;
                if (data.m_Category == OfferDefine.eCategory.Sub && title < m_SubOffers.Length) {
                    if (m_SubOffers[title] == null) {
                        m_SubOffers[title] = new SubOffer();
                    }
                    m_SubOffers[title].m_Datas.Add(data);
                } else {
                    if (m_MainOffers[title] == null) {
                        m_MainOffers[title] = new MainOffer();
                    }
                    m_MainOffers[title].m_Datas.Add(data);
                }
            }
        }

        public void SetChangedToWipReachCharaIDsOnSubOffer(PlayerOffer[] src, bool isDiffCheck) {
            if (src != null && src.Length > 0) {
                m_ChangedToWipReachCharaIDsOnSubOffer = new List<int>();
                for (int i = 0; i < src.Length; i++) {
                    PlayerOffer offer = src[i];
                    if (offer.category != (int)OfferDefine.eCategory.Sub) { continue; }
                    if (offer.state != (int)OfferDefine.eState.WIP_Reach) { continue; }

                    if (isDiffCheck) {
                        SubOffer subOffer = GetSubOffer(offer.titleType);
                        if (subOffer == null) { continue; }
                        OfferData data = subOffer.GetOfferData(offer.offerId);
                        if (data == null || data.m_State == OfferDefine.eState.WIP_Reach) { continue; }
                    }
                    m_ChangedToWipReachCharaIDsOnSubOffer.Add(offer.funcId);
                }
            }
        }

        [Serializable]
        public class TitleData {
            public eTitleType m_TitleType = eTitleType.None;
            public bool m_IsReleased;
            public bool m_IsUnlocked;
            public bool m_IsUnlockNotice;
            public OfferDefine.eState m_State;
            public OfferDefine.eCategory m_Category = OfferDefine.eCategory.Main;
            public int m_Lv = -1;
            public int m_Point;
            public int m_MaxPoint;
            public bool m_IsExistSubOffer;
            public int m_SubOfferOccurredCount;
            public int m_SubOfferWipCount;
            public int m_SubOfferWipReachCount;

            public bool IsAttention() {
                if (m_IsUnlocked) {
                    return m_State == OfferDefine.eState.Occurred || m_State == OfferDefine.eState.WIP_Reach;
                }
                return false;
            }
        }

        public class MainOffer {
            public List<OfferData> m_Datas = new List<OfferData>();
        }

        public class SubOffer {
            public List<OfferData> m_Datas = new List<OfferData>();

            public OfferData GetOfferData(int offerId) {
                for (int i = 0; i < m_Datas.Count; i++) {
                    if (m_Datas[i].m_ID == offerId) {
                        return m_Datas[i];
                    }
                }
                return m_Datas[0];
            }
        }

        public class OfferData {
            public long m_MngID = -1;
            public OfferDefine.eState m_State;
            public int m_ID = -1;
            public eTitleType m_TitleType = eTitleType.None;
            public OfferDefine.eCategory m_Category = OfferDefine.eCategory.Main;
            public int m_Lv = -1;
            public int m_Point;
            public int m_MaxPoint;
            public int m_Progress;
            public int m_MaxProgress;
            public int m_TransitQuestID = -1;
            public string m_TransitQuestName;
            public RewardSet[] m_Rewards;
            public string m_Title;
            public string m_ClientName;
            public string m_Body;
            public OfferDefine.eFuncType m_FuncType;
            public int m_FuncID = -1;

            public int GetCharaID() {
                if (m_FuncType == OfferDefine.eFuncType.PartyEditQuestClear || m_FuncType == OfferDefine.eFuncType.UniqueSkillLv) {
                    return m_FuncID;
                }
                return -1;
            }
        }
    }
}
