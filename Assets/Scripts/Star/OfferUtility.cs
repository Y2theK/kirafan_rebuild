﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000905 RID: 2309
	[Token(Token = "0x200069C")]
	[StructLayout(3)]
	public static class OfferUtility
	{
		// Token: 0x060025E7 RID: 9703 RVA: 0x000101D0 File Offset: 0x0000E3D0
		[Token(Token = "0x60022FC")]
		[Address(RVA = "0x10127069C", Offset = "0x127069C", VA = "0x10127069C")]
		public static int GetRewardGold(RewardSet[] rewards)
		{
			return 0;
		}

		// Token: 0x060025E8 RID: 9704 RVA: 0x000101E8 File Offset: 0x0000E3E8
		[Token(Token = "0x60022FD")]
		[Address(RVA = "0x101270758", Offset = "0x1270758", VA = "0x101270758")]
		public static int GetRewardMasterOrb(RewardSet[] rewards, out int out_orbLv)
		{
			return 0;
		}

		// Token: 0x060025E9 RID: 9705 RVA: 0x00010200 File Offset: 0x0000E400
		[Token(Token = "0x60022FE")]
		[Address(RVA = "0x1012708C4", Offset = "0x12708C4", VA = "0x1012708C4")]
		public static int GetRewardAllowRoomObject(RewardSet[] rewards)
		{
			return 0;
		}

		// Token: 0x060025EA RID: 9706 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60022FF")]
		[Address(RVA = "0x101270980", Offset = "0x1270980", VA = "0x101270980")]
		public static RewardSet[] GetRewardItems(RewardSet[] rewards)
		{
			return null;
		}

		// Token: 0x060025EB RID: 9707 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002300")]
		[Address(RVA = "0x101270B4C", Offset = "0x1270B4C", VA = "0x101270B4C")]
		public static Dictionary<int, List<int>> ParseSubOffer(OfferManager.SubOffer subOffer, bool ignoreInvalidManageID = true)
		{
			return null;
		}

		// Token: 0x060025EC RID: 9708 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002301")]
		[Address(RVA = "0x101270ED4", Offset = "0x1270ED4", VA = "0x101270ED4")]
		public static List<OfferManager.OfferData> CreateSpecifiedCharaIdOfferDatas(OfferManager.SubOffer subOffer, int charaID, bool ignoreInvalidManageID = true)
		{
			return null;
		}

		// Token: 0x060025ED RID: 9709 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002302")]
		[Address(RVA = "0x10127104C", Offset = "0x127104C", VA = "0x10127104C")]
		public static List<int> PopSubOfferNoticeCharaIDs()
		{
			return null;
		}

		// Token: 0x060025EE RID: 9710 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002303")]
		[Address(RVA = "0x101271234", Offset = "0x1271234", VA = "0x101271234")]
		public static string SubOfferCreateString_CharacterName(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025EF RID: 9711 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002304")]
		[Address(RVA = "0x101271384", Offset = "0x1271384", VA = "0x101271384")]
		public static string SubOfferCreateString_FullTitle(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025F0 RID: 9712 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002305")]
		[Address(RVA = "0x1012714E8", Offset = "0x12714E8", VA = "0x1012714E8")]
		public static string SubOfferCreateString_Index(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025F1 RID: 9713 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002306")]
		[Address(RVA = "0x101271624", Offset = "0x1271624", VA = "0x101271624")]
		public static string SubOfferCreateString_Title(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025F2 RID: 9714 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002307")]
		[Address(RVA = "0x101271734", Offset = "0x1271734", VA = "0x101271734")]
		public static string SubOfferCreateString_ClearCondition(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025F3 RID: 9715 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002308")]
		[Address(RVA = "0x1012719F4", Offset = "0x12719F4", VA = "0x1012719F4")]
		public static string SubOfferCreateString_WIP(OfferManager.OfferData offerData)
		{
			return null;
		}

		// Token: 0x060025F4 RID: 9716 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002309")]
		[Address(RVA = "0x101271CFC", Offset = "0x1271CFC", VA = "0x101271CFC")]
		public static string SubOfferCreateString_LockText(OfferManager.OfferData offerData)
		{
			return null;
		}
	}
}
