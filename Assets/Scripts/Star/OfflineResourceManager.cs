﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Cpp2IlInjected;
using Meige;
using Star.ADV;
using Star.Comic;

namespace Star
{
	// Token: 0x02000907 RID: 2311
	[Token(Token = "0x200069E")]
	[StructLayout(3)]
	public class OfflineResourceManager
	{
		// Token: 0x060025FA RID: 9722 RVA: 0x00010218 File Offset: 0x0000E418
		[Token(Token = "0x600230F")]
		[Address(RVA = "0x101271E8C", Offset = "0x1271E8C", VA = "0x101271E8C")]
		private bool ContainsCueSheetList(string name)
		{
			return default(bool);
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x060025FB RID: 9723 RVA: 0x00010230 File Offset: 0x0000E430
		[Token(Token = "0x17000265")]
		public int DownloadedNum
		{
			[Token(Token = "0x6002310")]
			[Address(RVA = "0x101271F74", Offset = "0x1271F74", VA = "0x101271F74")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x060025FC RID: 9724 RVA: 0x00010248 File Offset: 0x0000E448
		[Token(Token = "0x17000266")]
		public int DownloadMax
		{
			[Token(Token = "0x6002311")]
			[Address(RVA = "0x101271F84", Offset = "0x1271F84", VA = "0x101271F84")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x060025FD RID: 9725 RVA: 0x00010260 File Offset: 0x0000E460
		[Token(Token = "0x17000267")]
		public int ScriptNum
		{
			[Token(Token = "0x6002312")]
			[Address(RVA = "0x101272048", Offset = "0x1272048", VA = "0x101272048")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x060025FE RID: 9726 RVA: 0x00010278 File Offset: 0x0000E478
		[Token(Token = "0x17000268")]
		public int ScriptMax
		{
			[Token(Token = "0x6002313")]
			[Address(RVA = "0x101272050", Offset = "0x1272050", VA = "0x101272050")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060025FF RID: 9727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002314")]
		[Address(RVA = "0x1012720B0", Offset = "0x12720B0", VA = "0x1012720B0")]
		public void Dispose()
		{
		}

		// Token: 0x06002600 RID: 9728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002315")]
		[Address(RVA = "0x101272200", Offset = "0x1272200", VA = "0x101272200")]
		private void AddDownLoadPath(StringBuilder sb, string[] str)
		{
		}

		// Token: 0x06002601 RID: 9729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002316")]
		[Address(RVA = "0x101272398", Offset = "0x1272398", VA = "0x101272398")]
		public void SetDownloadOfflineAssetbundles()
		{
		}

		// Token: 0x06002602 RID: 9730 RVA: 0x00010290 File Offset: 0x0000E490
		[Token(Token = "0x6002317")]
		[Address(RVA = "0x101274C18", Offset = "0x1274C18", VA = "0x101274C18")]
		private bool LoadADV()
		{
			return default(bool);
		}

		// Token: 0x06002603 RID: 9731 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002318")]
		[Address(RVA = "0x101274EEC", Offset = "0x1274EEC", VA = "0x101274EEC")]
		private List<int> GetCharaAdvList()
		{
			return null;
		}

		// Token: 0x06002604 RID: 9732 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002319")]
		[Address(RVA = "0x1012754A8", Offset = "0x12754A8", VA = "0x1012754A8")]
		private List<int> GetAdvList()
		{
			return null;
		}

		// Token: 0x06002605 RID: 9733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600231A")]
		[Address(RVA = "0x1012755B8", Offset = "0x12755B8", VA = "0x1012755B8")]
		public void LoadFirstADV()
		{
		}

		// Token: 0x06002606 RID: 9734 RVA: 0x000102A8 File Offset: 0x0000E4A8
		[Token(Token = "0x600231B")]
		[Address(RVA = "0x1012756C0", Offset = "0x12756C0", VA = "0x1012756C0")]
		public bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06002607 RID: 9735 RVA: 0x000102C0 File Offset: 0x0000E4C0
		[Token(Token = "0x600231C")]
		[Address(RVA = "0x1012761C8", Offset = "0x12761C8", VA = "0x1012761C8")]
		public bool Update()
		{
			return default(bool);
		}

		// Token: 0x06002608 RID: 9736 RVA: 0x000102D8 File Offset: 0x0000E4D8
		[Token(Token = "0x600231D")]
		[Address(RVA = "0x1012758CC", Offset = "0x12758CC", VA = "0x1012758CC")]
		private bool PrepareADV()
		{
			return default(bool);
		}

		// Token: 0x06002609 RID: 9737 RVA: 0x000102F0 File Offset: 0x0000E4F0
		[Token(Token = "0x600231E")]
		[Address(RVA = "0x101276560", Offset = "0x1276560", VA = "0x101276560")]
		private int UpdateDownLoadAssetBundle(out bool NoResHandler)
		{
			return 0;
		}

		// Token: 0x0600260A RID: 9738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600231F")]
		[Address(RVA = "0x1012769CC", Offset = "0x12769CC", VA = "0x1012769CC")]
		private void DownloadAssetbundle()
		{
		}

		// Token: 0x0600260B RID: 9739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002320")]
		[Address(RVA = "0x1012762E8", Offset = "0x12762E8", VA = "0x1012762E8")]
		private void UpdateCRIFileDownload()
		{
		}

		// Token: 0x0600260C RID: 9740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002321")]
		[Address(RVA = "0x101276B58", Offset = "0x1276B58", VA = "0x101276B58")]
		private void DownloadCRIFile()
		{
		}

		// Token: 0x0600260D RID: 9741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002322")]
		[Address(RVA = "0x101276CD8", Offset = "0x1276CD8", VA = "0x101276CD8")]
		private void RetryDownloadAssetBundle()
		{
		}

		// Token: 0x0600260E RID: 9742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002323")]
		[Address(RVA = "0x101276F9C", Offset = "0x1276F9C", VA = "0x101276F9C")]
		private void OnConfirmAssetBundleDownloadError(int answer)
		{
		}

		// Token: 0x0600260F RID: 9743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002324")]
		[Address(RVA = "0x101276FA4", Offset = "0x1276FA4", VA = "0x101276FA4")]
		private void OnConfirmCRIDownloadError(int answer)
		{
		}

		// Token: 0x06002610 RID: 9744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002325")]
		[Address(RVA = "0x101276FB8", Offset = "0x1276FB8", VA = "0x101276FB8")]
		public OfflineResourceManager()
		{
		}

		// Token: 0x04003634 RID: 13876
		[Token(Token = "0x40027A3")]
		private const int DOWNLOAD_MAX_IN_PROC = 3;

		// Token: 0x04003635 RID: 13877
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40027A4")]
		private MeigeResource.DLSizeHandle m_ABDLSizeHndl;

		// Token: 0x04003636 RID: 13878
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40027A5")]
		private List<int> m_AdvIdList;

		// Token: 0x04003637 RID: 13879
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40027A6")]
		private List<MeigeResource.Handler> m_DownloadResHandler;

		// Token: 0x04003638 RID: 13880
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40027A7")]
		private List<string> m_DownloadPaths;

		// Token: 0x04003639 RID: 13881
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40027A8")]
		private List<OfflineResourceManager.CueSheet> m_LoadCueSheetList;

		// Token: 0x0400363A RID: 13882
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40027A9")]
		private int m_LoadCueSheetIndex;

		// Token: 0x0400363B RID: 13883
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40027AA")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400363C RID: 13884
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40027AB")]
		private string m_ComicName;

		// Token: 0x0400363D RID: 13885
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40027AC")]
		private ComicDBManager m_ComicDBMng;

		// Token: 0x0400363E RID: 13886
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40027AD")]
		private OfflineResourceManager.eADVPattern m_ADVPattern;

		// Token: 0x0400363F RID: 13887
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40027AE")]
		private OfflineResourceManager.eUpdateState m_UpdateState;

		// Token: 0x04003640 RID: 13888
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40027AF")]
		private int m_DownloadedNum;

		// Token: 0x04003641 RID: 13889
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40027B0")]
		private bool m_IsErrorWindow;

		// Token: 0x04003642 RID: 13890
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40027B1")]
		private OfflineScriptPlayer m_ScriptPlayer;

		// Token: 0x04003643 RID: 13891
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40027B2")]
		private ADVDataBase m_DB;

		// Token: 0x04003644 RID: 13892
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40027B3")]
		private List<string> m_ADVResourcePathList;

		// Token: 0x04003645 RID: 13893
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40027B4")]
		private bool m_PrepareADV;

		// Token: 0x04003646 RID: 13894
		[Cpp2IlInjected.FieldOffset(Offset = "0x81")]
		[Token(Token = "0x40027B5")]
		private bool m_waitUnloadScript;

		// Token: 0x04003647 RID: 13895
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x40027B6")]
		private int m_ScriptIndex;

		// Token: 0x02000908 RID: 2312
		[Token(Token = "0x2000F39")]
		private enum eADVPattern
		{
			// Token: 0x04003649 RID: 13897
			[Token(Token = "0x4006205")]
			ADV,
			// Token: 0x0400364A RID: 13898
			[Token(Token = "0x4006206")]
			Comic,
			// Token: 0x0400364B RID: 13899
			[Token(Token = "0x4006207")]
			Movie
		}

		// Token: 0x02000909 RID: 2313
		[Token(Token = "0x2000F3A")]
		private enum eUpdateState
		{
			// Token: 0x0400364D RID: 13901
			[Token(Token = "0x4006209")]
			CRI,
			// Token: 0x0400364E RID: 13902
			[Token(Token = "0x400620A")]
			AssetBundle
		}

		// Token: 0x0200090A RID: 2314
		[Token(Token = "0x2000F3B")]
		private class CueSheet
		{
			// Token: 0x06002611 RID: 9745 RVA: 0x00010308 File Offset: 0x0000E508
			[Token(Token = "0x6005FA9")]
			[Address(RVA = "0x101276CC8", Offset = "0x1276CC8", VA = "0x101276CC8")]
			public bool IsFullVoice()
			{
				return default(bool);
			}

			// Token: 0x06002612 RID: 9746 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FAA")]
			[Address(RVA = "0x101274BA8", Offset = "0x1274BA8", VA = "0x101274BA8")]
			public CueSheet(string name)
			{
			}

			// Token: 0x06002613 RID: 9747 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FAB")]
			[Address(RVA = "0x101274BD8", Offset = "0x1274BD8", VA = "0x101274BD8")]
			public CueSheet(string name, Dictionary<string, uint> versionDic, List<string> paths)
			{
			}

			// Token: 0x0400364F RID: 13903
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400620B")]
			public string name;

			// Token: 0x04003650 RID: 13904
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400620C")]
			public Dictionary<string, uint> versionDic;

			// Token: 0x04003651 RID: 13905
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400620D")]
			public List<string> paths;
		}
	}
}
