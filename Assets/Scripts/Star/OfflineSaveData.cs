﻿using MsgPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Star {
    public class OfflineSaveData {
        private static readonly OfflineSaveData instance = new OfflineSaveData();
        private static readonly string EncryptKey = "Xds2ygFLS9kC9z7huWJQDVMP2T7E8Qti";
        private static readonly int EncryptPasswordCount = 16;
        private static readonly string FilePrefix = string.Empty;
        private static readonly string SaveFile = Application.persistentDataPath + "/" + FilePrefix + "o.d";
        private static readonly string SaveFileBak = Application.persistentDataPath + "/" + FilePrefix + "o.d2";
        private static readonly string PasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private SaveData m_SaveData = new SaveData();

        public static OfflineSaveData Inst => instance;

        public UserData UserData {
            get => m_SaveData.UserData;
            set => m_SaveData.UserData = value;
        }

        public List<UserCharacterData> UserCharaDatas {
            get => m_SaveData.UserCharaDatas;
            set => m_SaveData.UserCharaDatas = value;
        }

        public UserAdvData UserAdvData {
            get => m_SaveData.UserAdvData;
            set => m_SaveData.UserAdvData = value;
        }

        public OfferManager.SubOffer[] SubOffers {
            get => m_SaveData.m_SubOffers;
            set => m_SaveData.m_SubOffers = value;
        }

        public Dictionary<eCharaNamedType, UserNamedData> UserNamedDatas {
            get => m_SaveData.m_UserNamedDatas;
            set => m_SaveData.m_UserNamedDatas = value;
        }

        //TODO: external implementation, needs DLLImport >.>
        private static extern int addKey(byte[] key, int keyLength);
        private static extern int readKey(out IntPtr arrPtr);

        private static byte[] getKey() {
            byte[] key;
            int len = readKey(out IntPtr src);
            if (len == 0) {
                Aes aes = Aes.Create();
                aes.GenerateKey();
                key = aes.Key;
                addKey(key, key.Length);
            } else {
                key = new byte[len];
                Marshal.Copy(src, key, 0, len);
                Marshal.FreeHGlobal(src);
            }
            return key;
        }

        public void Reset() {
            if (m_SaveData != null) {
                m_SaveData.Reset();
            }
        }

        public void Save() {
            ObjectPacker objectPacker = new ObjectPacker();
            byte[] src = objectPacker.Pack(m_SaveData);
            if (File.Exists(SaveFile)) {
                File.Copy(SaveFile, SaveFileBak, true);
            }
            try {
                byte[] pw = getKey();
                byte[] iv = Aes.Create().IV;
                byte[] enc = EncryptAesCore(src, pw, iv);
                using (var stream = new FileStream(SaveFile, FileMode.Create, FileAccess.Write))
                using (var writer = new BinaryWriter(stream)) {
                    writer.Write(iv);
                    writer.Write(enc);
                }
            } catch {
            }
        }

        public void Load() {
            Load(SaveFile);
        }

        public bool Load(string path) {
            if (!File.Exists(path)) {
                return false;
            }
            ObjectPacker packer = new ObjectPacker();
            try {
                byte[] iv;
                byte[] array;
                using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read)) {
                    using (var binaryReader = new System.IO.BinaryReader(fileStream)) {
                        array = new byte[binaryReader.BaseStream.Length - EncryptPasswordCount];
                        iv = binaryReader.ReadBytes(EncryptPasswordCount);
                        binaryReader.Read(array, 0, array.Length);
                    }
                }
                byte[] pw = getKey();
                byte[] res = DecryptAesCore(array, pw, iv);
                m_SaveData = packer.Unpack<SaveData>(res);
            } catch {
                bool backupLoaded = false;
                if (SaveFileBak != path) {
                    backupLoaded = Load(SaveFileBak);
                }
                if (!backupLoaded) {
                    return false;
                }
            }
            return true;
        }

        private void AfterLoad() { }

        private static void EncryptAes(byte[] src, string pw, int ivSize, out string iv, out byte[] dst) {
            iv = CreatePassword(ivSize);
            dst = EncryptAesCore(src, Encoding.UTF8.GetBytes(pw), Encoding.UTF8.GetBytes(iv));
        }

        private static byte[] EncryptAesCore(byte[] src, byte[] pw, byte[] iv) {
            byte[] dst;
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                using (var transform = rijndael.CreateEncryptor(pw, iv))
                using (var stream = new MemoryStream())
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(src, 0, src.Length);
                    cryptoStream.FlushFinalBlock();
                    dst = stream.ToArray();
                }
            }
            return dst;
        }

        private static void DecryptAes(byte[] src, string pw, string iv, out byte[] dst) {
            dst = DecryptAesCore(src, Encoding.UTF8.GetBytes(pw), Encoding.UTF8.GetBytes(iv));
        }

        private static byte[] DecryptAesCore(byte[] src, byte[] pw, byte[] iv) {
            byte[] dst = new byte[src.Length];
            using (var rijndael = new RijndaelManaged()) {
                rijndael.Padding = PaddingMode.PKCS7;
                rijndael.Mode = CipherMode.CBC;
                rijndael.KeySize = 256;
                rijndael.BlockSize = 128;

                using (var transform = rijndael.CreateDecryptor(pw, iv))
                using (var stream = new MemoryStream(src))
                using (var cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read)) {
                    cryptoStream.Read(dst, 0, dst.Length);
                }
            }
            return dst;
        }

        private static string CreatePassword(int count) {
            StringBuilder stringBuilder = new StringBuilder(count);
            for (int i = count - 1; i >= 0; i--) {
                char value = PasswordChars[Random.Range(0, PasswordChars.Length)];
                stringBuilder.Append(value);
            }
            return stringBuilder.ToString();
        }

        private class SaveData {
            public UserData UserData = new UserData();
            public List<UserCharacterData> UserCharaDatas = new List<UserCharacterData>();
            public UserAdvData UserAdvData = new UserAdvData();
            public OfferManager.SubOffer[] m_SubOffers;
            public Dictionary<eCharaNamedType, UserNamedData> m_UserNamedDatas = new Dictionary<eCharaNamedType, UserNamedData>();

            public SaveData() {
                Reset();
            }

            public void Reset() {
                UserData = new UserData();
                UserCharaDatas = new List<UserCharacterData>();
                UserAdvData = new UserAdvData();
                m_SubOffers = null;
                m_UserNamedDatas = new Dictionary<eCharaNamedType, UserNamedData>();
            }
        }
    }
}
