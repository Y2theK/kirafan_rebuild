﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000565 RID: 1381
	[Token(Token = "0x2000458")]
	[StructLayout(3)]
	public class OriginalCharaLibraryListDB : ScriptableObject
	{
		// Token: 0x060015DE RID: 5598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148E")]
		[Address(RVA = "0x101278C40", Offset = "0x1278C40", VA = "0x101278C40")]
		public OriginalCharaLibraryListDB()
		{
		}

		// Token: 0x04001927 RID: 6439
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001291")]
		public OriginalCharaLibraryListDB_Param[] m_Params;
	}
}
