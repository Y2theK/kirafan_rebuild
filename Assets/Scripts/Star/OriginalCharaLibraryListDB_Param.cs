﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000564 RID: 1380
	[Token(Token = "0x2000457")]
	[Serializable]
	[StructLayout(0, Size = 48)]
	public struct OriginalCharaLibraryListDB_Param
	{
		// Token: 0x04001920 RID: 6432
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400128A")]
		public int m_ID;

		// Token: 0x04001921 RID: 6433
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400128B")]
		public string m_Title;

		// Token: 0x04001922 RID: 6434
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400128C")]
		public string m_Illustrator;

		// Token: 0x04001923 RID: 6435
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400128D")]
		public string m_CV;

		// Token: 0x04001924 RID: 6436
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400128E")]
		public string m_Descript;

		// Token: 0x04001925 RID: 6437
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400128F")]
		public int m_CondQuestID;

		// Token: 0x04001926 RID: 6438
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001290")]
		public int m_CondAdvID;
	}
}
