﻿namespace Star {
    public struct OutputCharaParam {
        public int Lv { get; set; }
        public long NowExp { get; set; }
        public long NextMaxExp { get; set; }
        public int Hp { get; set; }
        public int Atk { get; set; }
        public int Mgc { get; set; }
        public int Def { get; set; }
        public int MDef { get; set; }
        public int Spd { get; set; }
        public int Luck { get; set; }

        public int[] ToIntsForCharaStatus() {
            return new int[] {
                Hp,
                Atk,
                Mgc,
                Def,
                MDef,
                Spd
            };
        }
    }
}
