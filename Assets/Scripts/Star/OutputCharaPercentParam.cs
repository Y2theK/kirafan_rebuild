﻿namespace Star {
    public struct OutputCharaPercentParam {
        public float Hp { get; set; }
        public float Atk { get; set; }
        public float Mgc { get; set; }
        public float Def { get; set; }
        public float MDef { get; set; }
        public float Spd { get; set; }
        public float Luck { get; set; }

        public float[] ToFloatsForCharaStatusPercent() {
            return new float[] {
                Hp,
                Atk,
                Mgc,
                Def,
                MDef,
                Spd
            };
        }
    }
}
