﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000567 RID: 1383
	[Token(Token = "0x200045A")]
	[StructLayout(3)]
	public class PackageItemContentsDB : ScriptableObject
	{
		// Token: 0x060015DF RID: 5599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600148F")]
		[Address(RVA = "0x101278FD8", Offset = "0x1278FD8", VA = "0x101278FD8")]
		public PackageItemContentsDB()
		{
		}

		// Token: 0x0400192D RID: 6445
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001297")]
		public PackageItemContentsDB_Param[] m_Params;
	}
}
