﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000519 RID: 1305
	[Token(Token = "0x200040F")]
	[StructLayout(3)]
	public static class PackageItemContentsDB_Ext
	{
		// Token: 0x06001571 RID: 5489 RVA: 0x00009450 File Offset: 0x00007650
		[Token(Token = "0x6001426")]
		[Address(RVA = "0x101278FE0", Offset = "0x1278FE0", VA = "0x101278FE0")]
		public static PackageItemContentsDB_Param GetParam(this PackageItemContentsDB self, int id)
		{
			return default(PackageItemContentsDB_Param);
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001427")]
		[Address(RVA = "0x1012790E8", Offset = "0x12790E8", VA = "0x1012790E8")]
		public static PackageItemContentsDB_Param[] GetPackageContents(this PackageItemContentsDB self, int packid)
		{
			return null;
		}
	}
}
