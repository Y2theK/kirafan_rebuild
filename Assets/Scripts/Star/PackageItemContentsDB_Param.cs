﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000566 RID: 1382
	[Token(Token = "0x2000459")]
	[Serializable]
	[StructLayout(0, Size = 20)]
	public struct PackageItemContentsDB_Param
	{
		// Token: 0x04001928 RID: 6440
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001292")]
		public int id;

		// Token: 0x04001929 RID: 6441
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001293")]
		public int packageId;

		// Token: 0x0400192A RID: 6442
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001294")]
		public int itemType;

		// Token: 0x0400192B RID: 6443
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001295")]
		public int itemId;

		// Token: 0x0400192C RID: 6444
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001296")]
		public int itemAmount;
	}
}
