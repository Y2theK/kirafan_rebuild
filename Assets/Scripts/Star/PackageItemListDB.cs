﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000569 RID: 1385
	[Token(Token = "0x200045C")]
	[StructLayout(3)]
	public class PackageItemListDB : ScriptableObject
	{
		// Token: 0x060015E0 RID: 5600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001490")]
		[Address(RVA = "0x101279280", Offset = "0x1279280", VA = "0x101279280")]
		public PackageItemListDB()
		{
		}

		// Token: 0x04001933 RID: 6451
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400129D")]
		public PackageItemListDB_Param[] m_Params;
	}
}
