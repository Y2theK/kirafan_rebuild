﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200051A RID: 1306
	[Token(Token = "0x2000410")]
	[StructLayout(3)]
	public static class PackageItemListDB_Ext
	{
		// Token: 0x06001573 RID: 5491 RVA: 0x00009468 File Offset: 0x00007668
		[Token(Token = "0x6001428")]
		[Address(RVA = "0x101279288", Offset = "0x1279288", VA = "0x101279288")]
		public static PackageItemListDB_Param GetParam(this PackageItemListDB self, int id)
		{
			return default(PackageItemListDB_Param);
		}
	}
}
