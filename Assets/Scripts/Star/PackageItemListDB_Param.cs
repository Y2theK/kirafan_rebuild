﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000568 RID: 1384
	[Token(Token = "0x200045B")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct PackageItemListDB_Param
	{
		// Token: 0x0400192E RID: 6446
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001298")]
		public int id;

		// Token: 0x0400192F RID: 6447
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001299")]
		public string name;

		// Token: 0x04001930 RID: 6448
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400129A")]
		public int packageID;

		// Token: 0x04001931 RID: 6449
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400129B")]
		public int iconID;

		// Token: 0x04001932 RID: 6450
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400129C")]
		public string detail;
	}
}
