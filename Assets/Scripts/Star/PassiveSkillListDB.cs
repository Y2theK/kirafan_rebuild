﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200056C RID: 1388
	[Token(Token = "0x200045F")]
	[StructLayout(3)]
	public class PassiveSkillListDB : ScriptableObject
	{
		// Token: 0x060015E1 RID: 5601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001491")]
		[Address(RVA = "0x10127937C", Offset = "0x127937C", VA = "0x10127937C")]
		public PassiveSkillListDB()
		{
		}

		// Token: 0x0400193B RID: 6459
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40012A5")]
		public PassiveSkillListDB_Param[] m_Params;
	}
}
