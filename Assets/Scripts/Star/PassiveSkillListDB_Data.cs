﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200056A RID: 1386
	[Token(Token = "0x200045D")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct PassiveSkillListDB_Data
	{
		// Token: 0x04001934 RID: 6452
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400129E")]
		public int m_Trigger;

		// Token: 0x04001935 RID: 6453
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400129F")]
		public int m_Type;

		// Token: 0x04001936 RID: 6454
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40012A0")]
		public double[] m_Args;
	}
}
