﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200051B RID: 1307
	[Token(Token = "0x2000411")]
	[StructLayout(3)]
	public static class PassiveSkillListDB_Ext
	{
		// Token: 0x06001574 RID: 5492 RVA: 0x00009480 File Offset: 0x00007680
		[Token(Token = "0x6001429")]
		[Address(RVA = "0x101279384", Offset = "0x1279384", VA = "0x101279384")]
		public static PassiveSkillListDB_Param? GetParam(this PassiveSkillListDB self, int passiveSkillID)
		{
			return null;
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600142A")]
		[Address(RVA = "0x1012794D8", Offset = "0x12794D8", VA = "0x1012794D8")]
		public static double[] GetSkillChangeArgs(this PassiveSkillListDB self, int passiveSkillID)
		{
			return null;
		}
	}
}
