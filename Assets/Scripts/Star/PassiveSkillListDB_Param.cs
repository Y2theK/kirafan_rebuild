﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200056B RID: 1387
	[Token(Token = "0x200045E")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct PassiveSkillListDB_Param
	{
		// Token: 0x04001937 RID: 6455
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40012A1")]
		public int m_ID;

		// Token: 0x04001938 RID: 6456
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40012A2")]
		public string m_SkillName;

		// Token: 0x04001939 RID: 6457
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40012A3")]
		public string m_SkillDetail;

		// Token: 0x0400193A RID: 6458
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40012A4")]
		public PassiveSkillListDB_Data[] m_Datas;
	}
}
