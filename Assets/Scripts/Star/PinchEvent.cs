﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008B5 RID: 2229
	[Token(Token = "0x2000660")]
	[StructLayout(3)]
	public class PinchEvent
	{
		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06002456 RID: 9302 RVA: 0x0000F8E8 File Offset: 0x0000DAE8
		// (set) Token: 0x06002457 RID: 9303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700024D")]
		public float InitialPinchDist
		{
			[Token(Token = "0x600217A")]
			[Address(RVA = "0x101279670", Offset = "0x1279670", VA = "0x101279670")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600217B")]
			[Address(RVA = "0x101279678", Offset = "0x1279678", VA = "0x101279678")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06002458 RID: 9304 RVA: 0x0000F900 File Offset: 0x0000DB00
		// (set) Token: 0x06002459 RID: 9305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700024E")]
		public bool IsPinching
		{
			[Token(Token = "0x600217C")]
			[Address(RVA = "0x101279680", Offset = "0x1279680", VA = "0x101279680")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600217D")]
			[Address(RVA = "0x101279688", Offset = "0x1279688", VA = "0x101279688")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x0600245A RID: 9306 RVA: 0x0000F918 File Offset: 0x0000DB18
		// (set) Token: 0x0600245B RID: 9307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700024F")]
		public Vector2 FocusedPos
		{
			[Token(Token = "0x600217E")]
			[Address(RVA = "0x101279690", Offset = "0x1279690", VA = "0x101279690")]
			[CompilerGenerated]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x600217F")]
			[Address(RVA = "0x101279698", Offset = "0x1279698", VA = "0x101279698")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x0600245C RID: 9308 RVA: 0x0000F930 File Offset: 0x0000DB30
		// (set) Token: 0x0600245D RID: 9309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000250")]
		public float PinchRatio
		{
			[Token(Token = "0x6002180")]
			[Address(RVA = "0x1012796A4", Offset = "0x12796A4", VA = "0x1012796A4")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6002181")]
			[Address(RVA = "0x1012796AC", Offset = "0x12796AC", VA = "0x1012796AC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x0600245E RID: 9310 RVA: 0x0000F948 File Offset: 0x0000DB48
		// (set) Token: 0x0600245F RID: 9311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000251")]
		public Vector2 vDraggingMove
		{
			[Token(Token = "0x6002182")]
			[Address(RVA = "0x1012796B4", Offset = "0x12796B4", VA = "0x1012796B4")]
			[CompilerGenerated]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x6002183")]
			[Address(RVA = "0x1012796BC", Offset = "0x12796BC", VA = "0x1012796BC")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06002460 RID: 9312 RVA: 0x0000F960 File Offset: 0x0000DB60
		// (set) Token: 0x06002461 RID: 9313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000252")]
		public bool IsDragging
		{
			[Token(Token = "0x6002184")]
			[Address(RVA = "0x1012796C8", Offset = "0x12796C8", VA = "0x1012796C8")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002185")]
			[Address(RVA = "0x1012796D8", Offset = "0x12796D8", VA = "0x1012796D8")]
			set
			{
			}
		}

		// Token: 0x1700027A RID: 634
		// (set) Token: 0x06002462 RID: 9314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000253")]
		public int IgnoreLayerOnDragReady
		{
			[Token(Token = "0x6002186")]
			[Address(RVA = "0x1012796EC", Offset = "0x12796EC", VA = "0x1012796EC")]
			set
			{
			}
		}

		// Token: 0x06002463 RID: 9315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002187")]
		[Address(RVA = "0x1012796F4", Offset = "0x12796F4", VA = "0x1012796F4")]
		public PinchEvent()
		{
		}

		// Token: 0x06002464 RID: 9316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002188")]
		[Address(RVA = "0x101279770", Offset = "0x1279770", VA = "0x101279770")]
		public void Update()
		{
		}

		// Token: 0x06002465 RID: 9317 RVA: 0x0000F978 File Offset: 0x0000DB78
		[Token(Token = "0x6002189")]
		[Address(RVA = "0x101279BAC", Offset = "0x1279BAC", VA = "0x101279BAC")]
		private Vector2 CalcFocusPos()
		{
			return default(Vector2);
		}

		// Token: 0x06002466 RID: 9318 RVA: 0x0000F990 File Offset: 0x0000DB90
		[Token(Token = "0x600218A")]
		[Address(RVA = "0x101279A74", Offset = "0x1279A74", VA = "0x101279A74")]
		private float CalcPinchDist()
		{
			return 0f;
		}

		// Token: 0x040034A3 RID: 13475
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002678")]
		private float m_CurrentPinchDist;

		// Token: 0x040034A8 RID: 13480
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400267D")]
		private float m_ThresholdMoveRatio;

		// Token: 0x040034A9 RID: 13481
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400267E")]
		private float m_ThresholdMovePixel;

		// Token: 0x040034AB RID: 13483
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002680")]
		private PinchEvent.eDragState m_DragState;

		// Token: 0x040034AC RID: 13484
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002681")]
		private int m_IgnoreLayerOnDragReady;

		// Token: 0x020008B6 RID: 2230
		[Token(Token = "0x2000F25")]
		private enum eDragState
		{
			// Token: 0x040034AE RID: 13486
			[Token(Token = "0x400619F")]
			None = -1,
			// Token: 0x040034AF RID: 13487
			[Token(Token = "0x40061A0")]
			Ready,
			// Token: 0x040034B0 RID: 13488
			[Token(Token = "0x40061A1")]
			Dragging
		}
	}
}
