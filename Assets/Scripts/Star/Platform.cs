﻿namespace Star {
    public class Platform {
        public static Type GetEnum() {
#if UNITY_IOS
			return Type.iOS;
#elif UNITY_ANDROID
			return Type.Android;
#else
            return Type.Other;
#endif
        }

        public static int Get() {
            return (int)GetEnum();
        }

        public enum Type {
            Other,
            iOS,
            Android
        }
    }
}
