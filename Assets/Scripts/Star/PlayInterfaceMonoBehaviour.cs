﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200034F RID: 847
	[Token(Token = "0x20002CD")]
	[StructLayout(3)]
	public abstract class PlayInterfaceMonoBehaviour : MonoBehaviour
	{
		// Token: 0x06000B87 RID: 2951
		[Token(Token = "0x6000AB6")]
		[Address(Slot = "4")]
		public abstract void Play();

		// Token: 0x06000B88 RID: 2952
		[Token(Token = "0x6000AB7")]
		[Address(Slot = "5")]
		public abstract void Stop();

		// Token: 0x06000B89 RID: 2953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000AB8")]
		[Address(RVA = "0x101279D3C", Offset = "0x1279D3C", VA = "0x101279D3C")]
		protected PlayInterfaceMonoBehaviour()
		{
		}
	}
}
