﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000571 RID: 1393
	[Token(Token = "0x2000464")]
	[StructLayout(3)]
	public class PopUpStateIconListDB : ScriptableObject
	{
		// Token: 0x060015E2 RID: 5602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001492")]
		[Address(RVA = "0x101279D44", Offset = "0x1279D44", VA = "0x101279D44")]
		public PopUpStateIconListDB()
		{
		}

		// Token: 0x0400199E RID: 6558
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001308")]
		public PopUpStateIconListDB_Param[] m_Params;
	}
}
