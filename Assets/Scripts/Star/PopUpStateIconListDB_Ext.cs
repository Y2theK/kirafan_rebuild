﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200051C RID: 1308
	[Token(Token = "0x2000412")]
	[StructLayout(3)]
	public static class PopUpStateIconListDB_Ext
	{
		// Token: 0x06001576 RID: 5494 RVA: 0x00009498 File Offset: 0x00007698
		[Token(Token = "0x600142B")]
		[Address(RVA = "0x101279D4C", Offset = "0x1279D4C", VA = "0x101279D4C")]
		public static PopUpStateIconListDB_Param GetParam(this PopUpStateIconListDB self, ePopUpStateIconType popupType)
		{
			return default(PopUpStateIconListDB_Param);
		}
	}
}
