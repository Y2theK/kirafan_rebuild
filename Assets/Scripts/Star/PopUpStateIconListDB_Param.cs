﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000570 RID: 1392
	[Token(Token = "0x2000463")]
	[Serializable]
	[StructLayout(0)]
	public struct PopUpStateIconListDB_Param
	{
		// Token: 0x0400199B RID: 6555
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001305")]
		public int m_PopUpType;

		// Token: 0x0400199C RID: 6556
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001306")]
		public int m_StateIconType;

		// Token: 0x0400199D RID: 6557
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001307")]
		public string[] m_Text;
	}
}
