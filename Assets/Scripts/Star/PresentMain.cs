﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007F0 RID: 2032
	[Token(Token = "0x2000601")]
	[StructLayout(3)]
	public class PresentMain : GameStateMain
	{
		// Token: 0x06001F8A RID: 8074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D02")]
		[Address(RVA = "0x101279E64", Offset = "0x1279E64", VA = "0x101279E64")]
		private void Start()
		{
		}

		// Token: 0x06001F8B RID: 8075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D03")]
		[Address(RVA = "0x101279E70", Offset = "0x1279E70", VA = "0x101279E70", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001F8C RID: 8076 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001D04")]
		[Address(RVA = "0x101279E78", Offset = "0x1279E78", VA = "0x101279E78", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001F8D RID: 8077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D05")]
		[Address(RVA = "0x10127A018", Offset = "0x127A018", VA = "0x10127A018", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001F8E RID: 8078 RVA: 0x0000DF98 File Offset: 0x0000C198
		[Token(Token = "0x6001D06")]
		[Address(RVA = "0x10127A094", Offset = "0x127A094", VA = "0x10127A094", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001F8F RID: 8079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D07")]
		[Address(RVA = "0x10127A09C", Offset = "0x127A09C", VA = "0x10127A09C")]
		public PresentMain()
		{
		}

		// Token: 0x04002FC6 RID: 12230
		[Token(Token = "0x4002499")]
		public const int STATE_INIT = 0;

		// Token: 0x04002FC7 RID: 12231
		[Token(Token = "0x400249A")]
		public const int STATE_MAIN = 1;
	}
}
