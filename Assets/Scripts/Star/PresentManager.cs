﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x02000910 RID: 2320
	[Token(Token = "0x20006A2")]
	[StructLayout(3)]
	public sealed class PresentManager
	{
		// Token: 0x06002660 RID: 9824 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002361")]
		[Address(RVA = "0x10127A0A4", Offset = "0x127A0A4", VA = "0x10127A0A4")]
		public List<PresentManager.PresentData> GetPresentList(ePresentType presentType = ePresentType.None)
		{
			return null;
		}

		// Token: 0x06002661 RID: 9825 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002362")]
		[Address(RVA = "0x10127A1F0", Offset = "0x127A1F0", VA = "0x10127A1F0")]
		public PresentManager.PresentData GetPresentData(long mngID)
		{
			return null;
		}

		// Token: 0x06002662 RID: 9826 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002363")]
		[Address(RVA = "0x10127A2F0", Offset = "0x127A2F0", VA = "0x10127A2F0")]
		public List<PresentManager.PresentData> GetHistoryList()
		{
			return null;
		}

		// Token: 0x06002663 RID: 9827 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002364")]
		[Address(RVA = "0x10127A2F8", Offset = "0x127A2F8", VA = "0x10127A2F8")]
		public PresentManager.PresentData GetHistoryData(long mngID)
		{
			return null;
		}

		// Token: 0x14000031 RID: 49
		// (add) Token: 0x06002664 RID: 9828 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002665 RID: 9829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000031")]
		private event Action<bool> m_OnResponse_GetPresentList
		{
			[Token(Token = "0x6002365")]
			[Address(RVA = "0x10127A3F8", Offset = "0x127A3F8", VA = "0x10127A3F8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002366")]
			[Address(RVA = "0x10127A504", Offset = "0x127A504", VA = "0x10127A504")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000032 RID: 50
		// (add) Token: 0x06002666 RID: 9830 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002667 RID: 9831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000032")]
		private event Action<bool> m_OnResponce_GetPresentReceived
		{
			[Token(Token = "0x6002367")]
			[Address(RVA = "0x10127A610", Offset = "0x127A610", VA = "0x10127A610")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002368")]
			[Address(RVA = "0x10127A71C", Offset = "0x127A71C", VA = "0x10127A71C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000033 RID: 51
		// (add) Token: 0x06002668 RID: 9832 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002669 RID: 9833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000033")]
		private event Action<PresentManager.eGetResult> m_OnResponse_Get
		{
			[Token(Token = "0x6002369")]
			[Address(RVA = "0x10127A828", Offset = "0x127A828", VA = "0x10127A828")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600236A")]
			[Address(RVA = "0x10127A934", Offset = "0x127A934", VA = "0x10127A934")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600266A RID: 9834 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600236B")]
		[Address(RVA = "0x10127AA40", Offset = "0x127AA40", VA = "0x10127AA40")]
		public List<PresentManager.PresentData> GetGotList()
		{
			return null;
		}

		// Token: 0x0600266B RID: 9835 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600236C")]
		[Address(RVA = "0x10127AA48", Offset = "0x127AA48", VA = "0x10127AA48")]
		public List<PresentManager.ReceiveError> GetReceiveErrorList()
		{
			return null;
		}

		// Token: 0x0600266C RID: 9836 RVA: 0x00010458 File Offset: 0x0000E658
		[Token(Token = "0x600236D")]
		[Address(RVA = "0x10127AA50", Offset = "0x127AA50", VA = "0x10127AA50")]
		public bool Request_GetPresentList(Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x0600266D RID: 9837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600236E")]
		[Address(RVA = "0x10127AB84", Offset = "0x127AB84", VA = "0x10127AB84")]
		private void OnResponse_GetPresentList(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600266E RID: 9838 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600236F")]
		[Address(RVA = "0x10127B0EC", Offset = "0x127B0EC", VA = "0x10127B0EC")]
		private List<PresentManager.ReceiveError> RemoveReceiveError(ref List<long> presentMngIDs)
		{
			return null;
		}

		// Token: 0x0600266F RID: 9839 RVA: 0x00010470 File Offset: 0x0000E670
		[Token(Token = "0x6002370")]
		[Address(RVA = "0x10127B8E0", Offset = "0x127B8E0", VA = "0x10127B8E0")]
		public bool Request_Get(List<long> presentMngIDs, Action<PresentManager.eGetResult> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002670 RID: 9840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002371")]
		[Address(RVA = "0x10127BB98", Offset = "0x127BB98", VA = "0x10127BB98")]
		private void OnResponse_Get(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002671 RID: 9841 RVA: 0x00010488 File Offset: 0x0000E688
		[Token(Token = "0x6002372")]
		[Address(RVA = "0x10127C238", Offset = "0x127C238", VA = "0x10127C238")]
		public bool Request_GetPresentReceived(Action<bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002672 RID: 9842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002373")]
		[Address(RVA = "0x10127C36C", Offset = "0x127C36C", VA = "0x10127C36C")]
		private void OnResponse_GetPresentReceived(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002374")]
		[Address(RVA = "0x10127AD54", Offset = "0x127AD54", VA = "0x10127AD54")]
		private static void wwwConvert(PlayerPresent src, PresentManager.PresentData dest, bool isHistory)
		{
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002375")]
		[Address(RVA = "0x10127AFC4", Offset = "0x127AFC4", VA = "0x10127AFC4")]
		private void UpdateNoticePresentCount()
		{
		}

		// Token: 0x06002675 RID: 9845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002376")]
		[Address(RVA = "0x10127C140", Offset = "0x127C140", VA = "0x10127C140")]
		private void AddNoticePresentCount(int add)
		{
		}

		// Token: 0x06002676 RID: 9846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002377")]
		[Address(RVA = "0x10127C524", Offset = "0x127C524", VA = "0x10127C524")]
		public PresentManager()
		{
		}

		// Token: 0x04003677 RID: 13943
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40027D2")]
		private List<PresentManager.PresentData> m_PresentList;

		// Token: 0x04003678 RID: 13944
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40027D3")]
		private List<PresentManager.PresentData> m_HistoryList;

		// Token: 0x0400367C RID: 13948
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40027D7")]
		private List<PresentManager.PresentData> m_GotPresentList;

		// Token: 0x0400367D RID: 13949
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40027D8")]
		private List<PresentManager.ReceiveError> m_ReceiveErrorList;

		// Token: 0x02000911 RID: 2321
		[Token(Token = "0x2000F3E")]
		public class PresentData : IComparable<PresentManager.PresentData>
		{
			// Token: 0x06002677 RID: 9847 RVA: 0x000104A0 File Offset: 0x0000E6A0
			[Token(Token = "0x6005FBD")]
			[Address(RVA = "0x10127C5F4", Offset = "0x127C5F4", VA = "0x10127C5F4", Slot = "4")]
			public int CompareTo(PresentManager.PresentData comparePart)
			{
				return 0;
			}

			// Token: 0x06002678 RID: 9848 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FBE")]
			[Address(RVA = "0x10127AD44", Offset = "0x127AD44", VA = "0x10127AD44")]
			public PresentData()
			{
			}

			// Token: 0x0400367E RID: 13950
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006218")]
			public long m_MngID;

			// Token: 0x0400367F RID: 13951
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006219")]
			public string m_Title;

			// Token: 0x04003680 RID: 13952
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400621A")]
			public string m_Message;

			// Token: 0x04003681 RID: 13953
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400621B")]
			public DateTime m_Date;

			// Token: 0x04003682 RID: 13954
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400621C")]
			public DateTime m_CreatedDate;

			// Token: 0x04003683 RID: 13955
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400621D")]
			public DateTime m_ReceivedDate;

			// Token: 0x04003684 RID: 13956
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400621E")]
			public ePresentType m_Type;

			// Token: 0x04003685 RID: 13957
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x400621F")]
			public int m_ID;

			// Token: 0x04003686 RID: 13958
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006220")]
			public int m_Amount;

			// Token: 0x04003687 RID: 13959
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x4006221")]
			public int m_AltItemID;

			// Token: 0x04003688 RID: 13960
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006222")]
			public int m_AltItemAmount;

			// Token: 0x04003689 RID: 13961
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x4006223")]
			public bool m_IsIndefinitePeriod;
		}

		// Token: 0x02000912 RID: 2322
		[Token(Token = "0x2000F3F")]
		public enum eGetResult
		{
			// Token: 0x0400368B RID: 13963
			[Token(Token = "0x4006225")]
			Err = -1,
			// Token: 0x0400368C RID: 13964
			[Token(Token = "0x4006226")]
			Success,
			// Token: 0x0400368D RID: 13965
			[Token(Token = "0x4006227")]
			OutOfPeriod
		}

		// Token: 0x02000913 RID: 2323
		[Token(Token = "0x2000F40")]
		public enum eErrorReason
		{
			// Token: 0x0400368F RID: 13967
			[Token(Token = "0x4006229")]
			None,
			// Token: 0x04003690 RID: 13968
			[Token(Token = "0x400622A")]
			KPOver,
			// Token: 0x04003691 RID: 13969
			[Token(Token = "0x400622B")]
			RoomObjOver,
			// Token: 0x04003692 RID: 13970
			[Token(Token = "0x400622C")]
			RoomObjRemainOver,
			// Token: 0x04003693 RID: 13971
			[Token(Token = "0x400622D")]
			WeaponOver
		}

		// Token: 0x02000914 RID: 2324
		[Token(Token = "0x2000F41")]
		public class ReceiveError
		{
			// Token: 0x06002679 RID: 9849 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FBF")]
			[Address(RVA = "0x10127B8A4", Offset = "0x127B8A4", VA = "0x10127B8A4")]
			public ReceiveError(PresentManager.PresentData presentData, PresentManager.eErrorReason reason)
			{
			}

			// Token: 0x04003694 RID: 13972
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400622E")]
			public PresentManager.PresentData m_PresentData;

			// Token: 0x04003695 RID: 13973
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400622F")]
			public PresentManager.eErrorReason m_Reason;
		}
	}
}
