﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007F1 RID: 2033
	[Token(Token = "0x2000602")]
	[StructLayout(3)]
	public class PresentState : GameStateBase
	{
		// Token: 0x06001F90 RID: 8080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D08")]
		[Address(RVA = "0x10127C6E0", Offset = "0x127C6E0", VA = "0x10127C6E0")]
		public PresentState(PresentMain owner)
		{
		}

		// Token: 0x06001F91 RID: 8081 RVA: 0x0000DFB0 File Offset: 0x0000C1B0
		[Token(Token = "0x6001D09")]
		[Address(RVA = "0x10127C714", Offset = "0x127C714", VA = "0x10127C714", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F92 RID: 8082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D0A")]
		[Address(RVA = "0x10127C71C", Offset = "0x127C71C", VA = "0x10127C71C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F93 RID: 8083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D0B")]
		[Address(RVA = "0x10127C720", Offset = "0x127C720", VA = "0x10127C720", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F94 RID: 8084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D0C")]
		[Address(RVA = "0x10127C724", Offset = "0x127C724", VA = "0x10127C724", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F95 RID: 8085 RVA: 0x0000DFC8 File Offset: 0x0000C1C8
		[Token(Token = "0x6001D0D")]
		[Address(RVA = "0x10127C728", Offset = "0x127C728", VA = "0x10127C728", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F96 RID: 8086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D0E")]
		[Address(RVA = "0x10127C730", Offset = "0x127C730", VA = "0x10127C730", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04002FC8 RID: 12232
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400249B")]
		protected PresentMain m_Owner;

		// Token: 0x04002FC9 RID: 12233
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400249C")]
		protected int m_NextState;
	}
}
