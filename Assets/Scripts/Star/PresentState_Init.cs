﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007F2 RID: 2034
	[Token(Token = "0x2000603")]
	[StructLayout(3)]
	public class PresentState_Init : PresentState
	{
		// Token: 0x06001F97 RID: 8087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D0F")]
		[Address(RVA = "0x101279F4C", Offset = "0x1279F4C", VA = "0x101279F4C")]
		public PresentState_Init(PresentMain owner)
		{
		}

		// Token: 0x06001F98 RID: 8088 RVA: 0x0000DFE0 File Offset: 0x0000C1E0
		[Token(Token = "0x6001D10")]
		[Address(RVA = "0x10127C734", Offset = "0x127C734", VA = "0x10127C734", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001F99 RID: 8089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D11")]
		[Address(RVA = "0x10127C73C", Offset = "0x127C73C", VA = "0x10127C73C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001F9A RID: 8090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D12")]
		[Address(RVA = "0x10127C744", Offset = "0x127C744", VA = "0x10127C744", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001F9B RID: 8091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D13")]
		[Address(RVA = "0x10127C748", Offset = "0x127C748", VA = "0x10127C748", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001F9C RID: 8092 RVA: 0x0000DFF8 File Offset: 0x0000C1F8
		[Token(Token = "0x6001D14")]
		[Address(RVA = "0x10127C74C", Offset = "0x127C74C", VA = "0x10127C74C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001F9D RID: 8093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D15")]
		[Address(RVA = "0x10127CA50", Offset = "0x127CA50", VA = "0x10127CA50", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001F9E RID: 8094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D16")]
		[Address(RVA = "0x10127CA54", Offset = "0x127CA54", VA = "0x10127CA54")]
		private void OnResponse_GetPresentList(bool isError)
		{
		}

		// Token: 0x04002FCA RID: 12234
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400249D")]
		private PresentState_Init.eStep m_Step;

		// Token: 0x020007F3 RID: 2035
		[Token(Token = "0x2000EC0")]
		private enum eStep
		{
			// Token: 0x04002FCC RID: 12236
			[Token(Token = "0x4005EA4")]
			None = -1,
			// Token: 0x04002FCD RID: 12237
			[Token(Token = "0x4005EA5")]
			First,
			// Token: 0x04002FCE RID: 12238
			[Token(Token = "0x4005EA6")]
			Request_Wait,
			// Token: 0x04002FCF RID: 12239
			[Token(Token = "0x4005EA7")]
			Load_Wait,
			// Token: 0x04002FD0 RID: 12240
			[Token(Token = "0x4005EA8")]
			End
		}
	}
}
