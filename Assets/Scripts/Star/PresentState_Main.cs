﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Present;

namespace Star
{
	// Token: 0x020007F4 RID: 2036
	[Token(Token = "0x2000604")]
	[StructLayout(3)]
	public class PresentState_Main : PresentState
	{
		// Token: 0x06001F9F RID: 8095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D17")]
		[Address(RVA = "0x101279F80", Offset = "0x1279F80", VA = "0x101279F80")]
		public PresentState_Main(PresentMain owner)
		{
		}

		// Token: 0x06001FA0 RID: 8096 RVA: 0x0000E010 File Offset: 0x0000C210
		[Token(Token = "0x6001D18")]
		[Address(RVA = "0x10127CA64", Offset = "0x127CA64", VA = "0x10127CA64", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001FA1 RID: 8097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D19")]
		[Address(RVA = "0x10127CA6C", Offset = "0x127CA6C", VA = "0x10127CA6C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001FA2 RID: 8098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D1A")]
		[Address(RVA = "0x10127CA74", Offset = "0x127CA74", VA = "0x10127CA74", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001FA3 RID: 8099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D1B")]
		[Address(RVA = "0x10127CA78", Offset = "0x127CA78", VA = "0x10127CA78", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001FA4 RID: 8100 RVA: 0x0000E028 File Offset: 0x0000C228
		[Token(Token = "0x6001D1C")]
		[Address(RVA = "0x10127CA80", Offset = "0x127CA80", VA = "0x10127CA80", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001FA5 RID: 8101 RVA: 0x0000E040 File Offset: 0x0000C240
		[Token(Token = "0x6001D1D")]
		[Address(RVA = "0x10127CDFC", Offset = "0x127CDFC", VA = "0x10127CDFC")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001FA6 RID: 8102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D1E")]
		[Address(RVA = "0x10127D2F8", Offset = "0x127D2F8", VA = "0x10127D2F8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001FA7 RID: 8103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D1F")]
		[Address(RVA = "0x10127D428", Offset = "0x127D428", VA = "0x10127D428")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06001FA8 RID: 8104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D20")]
		[Address(RVA = "0x10127D45C", Offset = "0x127D45C", VA = "0x10127D45C")]
		public void ExecuteGet(long presentMngID)
		{
		}

		// Token: 0x06001FA9 RID: 8105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D21")]
		[Address(RVA = "0x10127D570", Offset = "0x127D570", VA = "0x10127D570")]
		public void ExecuteGetMulti(List<long> presentMngIDs)
		{
		}

		// Token: 0x06001FAA RID: 8106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D22")]
		[Address(RVA = "0x10127D638", Offset = "0x127D638", VA = "0x10127D638")]
		public void OnResponse_Get(PresentManager.eGetResult result)
		{
		}

		// Token: 0x06001FAB RID: 8107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D23")]
		[Address(RVA = "0x10127D80C", Offset = "0x127D80C", VA = "0x10127D80C")]
		private void ShowReceiveError()
		{
		}

		// Token: 0x06001FAC RID: 8108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D24")]
		[Address(RVA = "0x10127E0CC", Offset = "0x127E0CC", VA = "0x10127E0CC")]
		private void ShowResult()
		{
		}

		// Token: 0x06001FAD RID: 8109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D25")]
		[Address(RVA = "0x10127E594", Offset = "0x127E594", VA = "0x10127E594")]
		private void OnConfirmConvert(int btn)
		{
		}

		// Token: 0x06001FAE RID: 8110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D26")]
		[Address(RVA = "0x10127E770", Offset = "0x127E770", VA = "0x10127E770")]
		public void OnEndResultWindow()
		{
		}

		// Token: 0x06001FAF RID: 8111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D27")]
		[Address(RVA = "0x10127E87C", Offset = "0x127E87C", VA = "0x10127E87C")]
		private void OnConfirmOutOfPeriod(int btn)
		{
		}

		// Token: 0x06001FB0 RID: 8112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D28")]
		[Address(RVA = "0x10127E628", Offset = "0x127E628", VA = "0x10127E628")]
		private void StartArousal()
		{
		}

		// Token: 0x06001FB1 RID: 8113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D29")]
		[Address(RVA = "0x10127D250", Offset = "0x127D250", VA = "0x10127D250")]
		private void BackFromArousal()
		{
		}

		// Token: 0x06001FB2 RID: 8114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D2A")]
		[Address(RVA = "0x10127E8B8", Offset = "0x127E8B8", VA = "0x10127E8B8")]
		public void ExecteGetHistory()
		{
		}

		// Token: 0x06001FB3 RID: 8115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D2B")]
		[Address(RVA = "0x10127E974", Offset = "0x127E974", VA = "0x10127E974")]
		public void OnResponce_GetHistory(bool isError)
		{
		}

		// Token: 0x06001FB4 RID: 8116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D2C")]
		[Address(RVA = "0x10127E9FC", Offset = "0x127E9FC", VA = "0x10127E9FC")]
		private void ExcuteGetPresentList()
		{
		}

		// Token: 0x06001FB5 RID: 8117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D2D")]
		[Address(RVA = "0x10127EB2C", Offset = "0x127EB2C", VA = "0x10127EB2C")]
		private void OnResponse_GetPresentList(bool isError)
		{
		}

		// Token: 0x04002FD1 RID: 12241
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400249E")]
		private PresentState_Main.eStep m_Step;

		// Token: 0x04002FD2 RID: 12242
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400249F")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04002FD3 RID: 12243
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024A0")]
		private PresentUI m_UI;

		// Token: 0x04002FD4 RID: 12244
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024A1")]
		private List<long> m_RequestMngIDList;

		// Token: 0x04002FD5 RID: 12245
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40024A2")]
		private bool m_Restart;

		// Token: 0x04002FD6 RID: 12246
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40024A3")]
		private ArousalLvUp m_ArousalLvUp;

		// Token: 0x04002FD7 RID: 12247
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40024A4")]
		private float m_ReloadTimer;

		// Token: 0x020007F5 RID: 2037
		[Token(Token = "0x2000EC1")]
		private enum eStep
		{
			// Token: 0x04002FD9 RID: 12249
			[Token(Token = "0x4005EAA")]
			None = -1,
			// Token: 0x04002FDA RID: 12250
			[Token(Token = "0x4005EAB")]
			First,
			// Token: 0x04002FDB RID: 12251
			[Token(Token = "0x4005EAC")]
			LoadWait,
			// Token: 0x04002FDC RID: 12252
			[Token(Token = "0x4005EAD")]
			PlayIn,
			// Token: 0x04002FDD RID: 12253
			[Token(Token = "0x4005EAE")]
			Main,
			// Token: 0x04002FDE RID: 12254
			[Token(Token = "0x4005EAF")]
			Arousal,
			// Token: 0x04002FDF RID: 12255
			[Token(Token = "0x4005EB0")]
			UnloadChildSceneWait
		}
	}
}
