﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000573 RID: 1395
	[Token(Token = "0x2000466")]
	[StructLayout(3)]
	public class ProfileVoiceListDB : ScriptableObject
	{
		// Token: 0x060015E3 RID: 5603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001493")]
		[Address(RVA = "0x10127EC14", Offset = "0x127EC14", VA = "0x10127EC14")]
		public ProfileVoiceListDB()
		{
		}

		// Token: 0x040019A5 RID: 6565
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400130F")]
		public ProfileVoiceListDB_Param[] m_Params;
	}
}
