﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000572 RID: 1394
	[Token(Token = "0x2000465")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct ProfileVoiceListDB_Param
	{
		// Token: 0x0400199F RID: 6559
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001309")]
		public string m_CueName;

		// Token: 0x040019A0 RID: 6560
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400130A")]
		public string m_Category;

		// Token: 0x040019A1 RID: 6561
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400130B")]
		public string m_Title;

		// Token: 0x040019A2 RID: 6562
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400130C")]
		public int m_Friendship;

		// Token: 0x040019A3 RID: 6563
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400130D")]
		public int m_SPCategory;

		// Token: 0x040019A4 RID: 6564
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400130E")]
		public int[] m_SPParam;
	}
}
