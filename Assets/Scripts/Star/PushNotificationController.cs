﻿using System;
using System.Runtime.InteropServices;
using Amazon;
using Amazon.Runtime;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AE8 RID: 2792
	[Token(Token = "0x20007A3")]
	[StructLayout(3)]
	public class PushNotificationController : MonoBehaviour
	{
		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x0600314C RID: 12620 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700038C")]
		public string EndpointArn
		{
			[Token(Token = "0x6002D0F")]
			[Address(RVA = "0x10127EC1C", Offset = "0x127EC1C", VA = "0x10127EC1C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x0600314D RID: 12621 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700038D")]
		private AWSCredentials Credentials
		{
			[Token(Token = "0x6002D10")]
			[Address(RVA = "0x10127EC24", Offset = "0x127EC24", VA = "0x10127EC24")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x0600314E RID: 12622 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700038E")]
		private IAmazonSimpleNotificationService SnsClient
		{
			[Token(Token = "0x6002D11")]
			[Address(RVA = "0x10127ED1C", Offset = "0x127ED1C", VA = "0x10127ED1C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x0600314F RID: 12623 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700038F")]
		private RegionEndpoint _CognitoIdentityRegion
		{
			[Token(Token = "0x6002D12")]
			[Address(RVA = "0x10127ECB0", Offset = "0x127ECB0", VA = "0x10127ECB0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x06003150 RID: 12624 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000390")]
		private RegionEndpoint _SNSRegion
		{
			[Token(Token = "0x6002D13")]
			[Address(RVA = "0x10127EDB0", Offset = "0x127EDB0", VA = "0x10127EDB0")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003151 RID: 12625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D14")]
		[Address(RVA = "0x10127EE1C", Offset = "0x127EE1C", VA = "0x10127EE1C")]
		private void Start()
		{
		}

		// Token: 0x06003152 RID: 12626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D15")]
		[Address(RVA = "0x10127EE94", Offset = "0x127EE94", VA = "0x10127EE94")]
		public void RegisterDevice()
		{
		}

		// Token: 0x06003153 RID: 12627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D16")]
		[Address(RVA = "0x10127EF0C", Offset = "0x127EF0C", VA = "0x10127EF0C")]
		private void RegisterAndroidDeviceCallback(string regId)
		{
		}

		// Token: 0x06003154 RID: 12628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D17")]
		[Address(RVA = "0x10127EF10", Offset = "0x127EF10", VA = "0x10127EF10")]
		private void CheckForDeviceToken()
		{
		}

		// Token: 0x06003155 RID: 12629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D18")]
		[Address(RVA = "0x10127F150", Offset = "0x127F150", VA = "0x10127F150")]
		private void CreatePlatformEndpointAsynciOSCallback(AmazonServiceResult<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse> resultObject)
		{
		}

		// Token: 0x06003156 RID: 12630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D19")]
		[Address(RVA = "0x10127F258", Offset = "0x127F258", VA = "0x10127F258")]
		public void UnregisterDevice()
		{
		}

		// Token: 0x06003157 RID: 12631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D1A")]
		[Address(RVA = "0x10127F368", Offset = "0x127F368", VA = "0x10127F368")]
		private void UnregisterDeviceCallback(AmazonServiceResult<DeleteEndpointRequest, DeleteEndpointResponse> resultObject)
		{
		}

		// Token: 0x06003158 RID: 12632 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D1B")]
		[Address(RVA = "0x10127F3E8", Offset = "0x127F3E8", VA = "0x10127F3E8")]
		public string GetLog()
		{
			return null;
		}

		// Token: 0x06003159 RID: 12633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D1C")]
		[Address(RVA = "0x10127F3F0", Offset = "0x127F3F0", VA = "0x10127F3F0")]
		public PushNotificationController()
		{
		}

		// Token: 0x0400408F RID: 16527
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E16")]
		private string IdentityPoolId;

		// Token: 0x04004090 RID: 16528
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E17")]
		private string iOSPlatformApplicationArn;

		// Token: 0x04004091 RID: 16529
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E18")]
		private string CognitoIdentityRegion;

		// Token: 0x04004092 RID: 16530
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002E19")]
		private string SNSRegion;

		// Token: 0x04004093 RID: 16531
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002E1A")]
		private string _endpointArn;

		// Token: 0x04004094 RID: 16532
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002E1B")]
		private AWSCredentials _credentials;

		// Token: 0x04004095 RID: 16533
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E1C")]
		private IAmazonSimpleNotificationService _snsClient;

		// Token: 0x04004096 RID: 16534
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E1D")]
		private string deviceToken;

		// Token: 0x04004097 RID: 16535
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E1E")]
		private int count;

		// Token: 0x04004098 RID: 16536
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002E1F")]
		private string m_Log;
	}
}
