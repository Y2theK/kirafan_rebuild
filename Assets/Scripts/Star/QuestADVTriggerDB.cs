﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000576 RID: 1398
	[Token(Token = "0x2000469")]
	[StructLayout(3)]
	public class QuestADVTriggerDB : ScriptableObject
	{
		// Token: 0x060015E4 RID: 5604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001494")]
		[Address(RVA = "0x10127F480", Offset = "0x127F480", VA = "0x10127F480")]
		public QuestADVTriggerDB()
		{
		}

		// Token: 0x040019AC RID: 6572
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001316")]
		public QuestADVTriggerDB_Param[] m_Params;
	}
}
