﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000575 RID: 1397
	[Token(Token = "0x2000468")]
	[Serializable]
	[StructLayout(0, Size = 12)]
	public struct QuestADVTriggerDB_Param
	{
		// Token: 0x040019A9 RID: 6569
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001313")]
		public int m_TriggerType;

		// Token: 0x040019AA RID: 6570
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001314")]
		public int m_TriggerID;

		// Token: 0x040019AB RID: 6571
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001315")]
		public int m_AdvID;
	}
}
