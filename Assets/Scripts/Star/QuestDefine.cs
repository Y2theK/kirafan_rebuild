﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000915 RID: 2325
	[Token(Token = "0x20006A3")]
	[StructLayout(3)]
	public static class QuestDefine
	{
		// Token: 0x04003696 RID: 13974
		[Token(Token = "0x40027D9")]
		public const int DIFFICULTY_NUM = 3;

		// Token: 0x04003697 RID: 13975
		[Token(Token = "0x40027DA")]
		public const int NOT_EVENT_TYPE = -1;

		// Token: 0x04003698 RID: 13976
		[Token(Token = "0x40027DB")]
		public const int DAYOFWEEK_EVENT_TYPE = 0;

		// Token: 0x04003699 RID: 13977
		[Token(Token = "0x40027DC")]
		public const int CHALLENGE_EVENT_TYPE = 1;

		// Token: 0x0400369A RID: 13978
		[Token(Token = "0x40027DD")]
		public const int WRITER_EVENT_TYPE = 2;

		// Token: 0x0400369B RID: 13979
		[Token(Token = "0x40027DE")]
		public const int PART_1 = 0;

		// Token: 0x0400369C RID: 13980
		[Token(Token = "0x40027DF")]
		public const int PART_2 = 1;

		// Token: 0x02000916 RID: 2326
		[Token(Token = "0x2000F42")]
		public enum eClearRank
		{
			// Token: 0x0400369E RID: 13982
			[Token(Token = "0x4006231")]
			None,
			// Token: 0x0400369F RID: 13983
			[Token(Token = "0x4006232")]
			Star1,
			// Token: 0x040036A0 RID: 13984
			[Token(Token = "0x4006233")]
			Star2,
			// Token: 0x040036A1 RID: 13985
			[Token(Token = "0x4006234")]
			StarMax,
			// Token: 0x040036A2 RID: 13986
			[Token(Token = "0x4006235")]
			Num
		}

		// Token: 0x02000917 RID: 2327
		[Token(Token = "0x2000F43")]
		public enum eDifficulty
		{
			// Token: 0x040036A4 RID: 13988
			[Token(Token = "0x4006237")]
			Normal,
			// Token: 0x040036A5 RID: 13989
			[Token(Token = "0x4006238")]
			Hard,
			// Token: 0x040036A6 RID: 13990
			[Token(Token = "0x4006239")]
			Star,
			// Token: 0x040036A7 RID: 13991
			[Token(Token = "0x400623A")]
			Num
		}

		// Token: 0x02000918 RID: 2328
		[Token(Token = "0x2000F44")]
		public enum eGroupClearRank
		{
			// Token: 0x040036A9 RID: 13993
			[Token(Token = "0x400623C")]
			None,
			// Token: 0x040036AA RID: 13994
			[Token(Token = "0x400623D")]
			Clear,
			// Token: 0x040036AB RID: 13995
			[Token(Token = "0x400623E")]
			Complete
		}

		// Token: 0x02000919 RID: 2329
		[Token(Token = "0x2000F45")]
		public enum eQuestLogAddResult
		{
			// Token: 0x040036AD RID: 13997
			[Token(Token = "0x4006240")]
			Success,
			// Token: 0x040036AE RID: 13998
			[Token(Token = "0x4006241")]
			StaminaIsShort,
			// Token: 0x040036AF RID: 13999
			[Token(Token = "0x4006242")]
			KeyItemIsShort,
			// Token: 0x040036B0 RID: 14000
			[Token(Token = "0x4006243")]
			AnyExConditionError
		}

		// Token: 0x0200091A RID: 2330
		[Token(Token = "0x2000F46")]
		public enum eReturnLogAdd
		{
			// Token: 0x040036B2 RID: 14002
			[Token(Token = "0x4006245")]
			Success,
			// Token: 0x040036B3 RID: 14003
			[Token(Token = "0x4006246")]
			StaminaIsShort,
			// Token: 0x040036B4 RID: 14004
			[Token(Token = "0x4006247")]
			QuestOutOfPerod,
			// Token: 0x040036B5 RID: 14005
			[Token(Token = "0x4006248")]
			QuestOrderLimit,
			// Token: 0x040036B6 RID: 14006
			[Token(Token = "0x4006249")]
			ItemIsShort,
			// Token: 0x040036B7 RID: 14007
			[Token(Token = "0x400624A")]
			MoreStaminaNeededThanExpected,
			// Token: 0x040036B8 RID: 14008
			[Token(Token = "0x400624B")]
			Unknown
		}

		// Token: 0x0200091B RID: 2331
		[Token(Token = "0x2000F47")]
		public enum eUICategory
		{
			// Token: 0x040036BA RID: 14010
			[Token(Token = "0x400624D")]
			None,
			// Token: 0x040036BB RID: 14011
			[Token(Token = "0x400624E")]
			Chapter,
			// Token: 0x040036BC RID: 14012
			[Token(Token = "0x400624F")]
			Event,
			// Token: 0x040036BD RID: 14013
			[Token(Token = "0x4006250")]
			Chara,
			// Token: 0x040036BE RID: 14014
			[Token(Token = "0x4006251")]
			Offer
		}
	}
}
