﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000579 RID: 1401
	[Token(Token = "0x200046C")]
	[StructLayout(3)]
	public class QuestEnemyListDB : ScriptableObject
	{
		// Token: 0x060015E5 RID: 5605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001495")]
		[Address(RVA = "0x10127F5B0", Offset = "0x127F5B0", VA = "0x10127F5B0")]
		public QuestEnemyListDB()
		{
		}

		// Token: 0x040019CC RID: 6604
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001336")]
		public QuestEnemyListDB_Param[] m_Params;
	}
}
