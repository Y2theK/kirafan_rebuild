﻿namespace Star {
    public static class QuestEnemyListDB_Ext {
        public static QuestEnemyListDB_Param GetParam(this QuestEnemyListDB self, int questEnemyID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == questEnemyID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static int GetSkillIDNum(ref QuestEnemyListDB_Param questEnemyParam) {
            int num = 0;
            for (; num < questEnemyParam.m_SkillIDs.Length; num++) {
                if (questEnemyParam.m_SkillIDs[num] < 0) {
                    break;
                }
            }
            return num;
        }

        public static int GetChargeSkillIDNum(ref QuestEnemyListDB_Param questEnemyParam) {
            int num = 0;
            for (; num < questEnemyParam.m_CharageSkillIDs.Length; num++) {
                if (questEnemyParam.m_CharageSkillIDs[num] < 0) {
                    break;
                }
            }
            return num;
        }
    }
}
