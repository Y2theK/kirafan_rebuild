﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000578 RID: 1400
	[Token(Token = "0x200046B")]
	[Serializable]
	[StructLayout(0, Size = 144)]
	public struct QuestEnemyListDB_Param
	{
		// Token: 0x040019AF RID: 6575
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001319")]
		public int m_ID;

		// Token: 0x040019B0 RID: 6576
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400131A")]
		public string m_CharaName;

		// Token: 0x040019B1 RID: 6577
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400131B")]
		public int m_ResourceID;

		// Token: 0x040019B2 RID: 6578
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400131C")]
		public int m_Element;

		// Token: 0x040019B3 RID: 6579
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400131D")]
		public int m_InitLv;

		// Token: 0x040019B4 RID: 6580
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400131E")]
		public int m_MaxLv;

		// Token: 0x040019B5 RID: 6581
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400131F")]
		public int m_InitHp;

		// Token: 0x040019B6 RID: 6582
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001320")]
		public int m_InitAtk;

		// Token: 0x040019B7 RID: 6583
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001321")]
		public int m_InitMgc;

		// Token: 0x040019B8 RID: 6584
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001322")]
		public int m_InitDef;

		// Token: 0x040019B9 RID: 6585
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001323")]
		public int m_InitMDef;

		// Token: 0x040019BA RID: 6586
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001324")]
		public int m_InitSpd;

		// Token: 0x040019BB RID: 6587
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001325")]
		public int m_InitLuck;

		// Token: 0x040019BC RID: 6588
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001326")]
		public int m_MaxHp;

		// Token: 0x040019BD RID: 6589
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001327")]
		public int m_MaxAtk;

		// Token: 0x040019BE RID: 6590
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001328")]
		public int m_MaxMgc;

		// Token: 0x040019BF RID: 6591
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001329")]
		public int m_MaxDef;

		// Token: 0x040019C0 RID: 6592
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400132A")]
		public int m_MaxMDef;

		// Token: 0x040019C1 RID: 6593
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400132B")]
		public int m_MaxSpd;

		// Token: 0x040019C2 RID: 6594
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400132C")]
		public int m_MaxLuck;

		// Token: 0x040019C3 RID: 6595
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400132D")]
		public int m_AIID;

		// Token: 0x040019C4 RID: 6596
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400132E")]
		public int[] m_SkillIDs;

		// Token: 0x040019C5 RID: 6597
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400132F")]
		public int[] m_IsConfusionSkillIDs;

		// Token: 0x040019C6 RID: 6598
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4001330")]
		public int[] m_CharageSkillIDs;

		// Token: 0x040019C7 RID: 6599
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4001331")]
		public int m_CharageMax;

		// Token: 0x040019C8 RID: 6600
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4001332")]
		public float m_StunCoef;

		// Token: 0x040019C9 RID: 6601
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4001333")]
		public float m_StunerMag;

		// Token: 0x040019CA RID: 6602
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4001334")]
		public float m_StunerAvoid;

		// Token: 0x040019CB RID: 6603
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4001335")]
		public int[] m_IsRegistAbnormals;
	}
}
