﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200091C RID: 2332
	[Token(Token = "0x20006A4")]
	[StructLayout(3)]
	public class QuestEventAdditiveUIScene
	{
		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x0600267A RID: 9850 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000278")]
		public QuestEventAdditiveUI UI
		{
			[Token(Token = "0x6002378")]
			[Address(RVA = "0x10127F800", Offset = "0x127F800", VA = "0x10127F800")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600267B RID: 9851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002379")]
		[Address(RVA = "0x10127F808", Offset = "0x127F808", VA = "0x10127F808")]
		public void Update()
		{
		}

		// Token: 0x0600267C RID: 9852 RVA: 0x000104B8 File Offset: 0x0000E6B8
		[Token(Token = "0x600237A")]
		[Address(RVA = "0x10127F9E4", Offset = "0x127F9E4", VA = "0x10127F9E4")]
		public bool LoadIfExistUISetting(bool isOpenLoadingUI = true)
		{
			return default(bool);
		}

		// Token: 0x0600267D RID: 9853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600237B")]
		[Address(RVA = "0x10127FB24", Offset = "0x127FB24", VA = "0x10127FB24")]
		public void Unload()
		{
		}

		// Token: 0x0600267E RID: 9854 RVA: 0x000104D0 File Offset: 0x0000E6D0
		[Token(Token = "0x600237C")]
		[Address(RVA = "0x10127FBF0", Offset = "0x127FBF0", VA = "0x10127FBF0")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x0600267F RID: 9855 RVA: 0x000104E8 File Offset: 0x0000E6E8
		[Token(Token = "0x600237D")]
		[Address(RVA = "0x10127FC04", Offset = "0x127FC04", VA = "0x10127FC04")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06002680 RID: 9856 RVA: 0x00010500 File Offset: 0x0000E700
		[Token(Token = "0x600237E")]
		[Address(RVA = "0x10127FCC8", Offset = "0x127FCC8", VA = "0x10127FCC8")]
		public bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06002681 RID: 9857 RVA: 0x00010518 File Offset: 0x0000E718
		[Token(Token = "0x600237F")]
		[Address(RVA = "0x10127FD6C", Offset = "0x127FD6C", VA = "0x10127FD6C")]
		public int GetDisplayCharaID()
		{
			return 0;
		}

		// Token: 0x06002682 RID: 9858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002380")]
		[Address(RVA = "0x10127FE0C", Offset = "0x127FE0C", VA = "0x10127FE0C")]
		public void PlayIn(EventQuestUISettingDB_Param? param)
		{
		}

		// Token: 0x06002683 RID: 9859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002381")]
		[Address(RVA = "0x10127FF20", Offset = "0x127FF20", VA = "0x10127FF20")]
		public void PlayOut()
		{
		}

		// Token: 0x06002684 RID: 9860 RVA: 0x00010530 File Offset: 0x0000E730
		[Token(Token = "0x6002382")]
		[Address(RVA = "0x10127FFC0", Offset = "0x127FFC0", VA = "0x10127FFC0")]
		public bool PlayOutOnCondition(EventQuestUISettingDB_Param? nextParam)
		{
			return default(bool);
		}

		// Token: 0x06002685 RID: 9861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002383")]
		[Address(RVA = "0x1012800A4", Offset = "0x12800A4", VA = "0x1012800A4")]
		public void PlayVoice(bool useGroupSetting)
		{
		}

		// Token: 0x06002686 RID: 9862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002384")]
		[Address(RVA = "0x101280680", Offset = "0x1280680", VA = "0x101280680")]
		public QuestEventAdditiveUIScene()
		{
		}

		// Token: 0x040036BF RID: 14015
		[Token(Token = "0x40027E0")]
		private const string INVALID_CUENAME = "-1";

		// Token: 0x040036C0 RID: 14016
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40027E1")]
		private QuestEventAdditiveUI m_UI;

		// Token: 0x040036C1 RID: 14017
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40027E2")]
		private QuestEventAdditiveUIScene.eStep m_Step;

		// Token: 0x040036C2 RID: 14018
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40027E3")]
		private bool m_WasOpenedLoadingUI;

		// Token: 0x0200091D RID: 2333
		[Token(Token = "0x2000F48")]
		private enum eStep
		{
			// Token: 0x040036C4 RID: 14020
			[Token(Token = "0x4006253")]
			None,
			// Token: 0x040036C5 RID: 14021
			[Token(Token = "0x4006254")]
			Loading,
			// Token: 0x040036C6 RID: 14022
			[Token(Token = "0x4006255")]
			Setup,
			// Token: 0x040036C7 RID: 14023
			[Token(Token = "0x4006256")]
			Idle
		}
	}
}
