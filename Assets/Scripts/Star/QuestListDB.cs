﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200057B RID: 1403
	[Token(Token = "0x200046E")]
	[StructLayout(3)]
	public class QuestListDB : ScriptableObject
	{
		// Token: 0x060015E6 RID: 5606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001496")]
		[Address(RVA = "0x101280688", Offset = "0x1280688", VA = "0x101280688")]
		public QuestListDB()
		{
		}

		// Token: 0x040019E8 RID: 6632
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001352")]
		public QuestListDB_Param[] m_Params;
	}
}
