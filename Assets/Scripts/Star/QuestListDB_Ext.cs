﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200051F RID: 1311
	[Token(Token = "0x2000415")]
	[StructLayout(3)]
	public static class QuestListDB_Ext
	{
		// Token: 0x0600157B RID: 5499 RVA: 0x00009510 File Offset: 0x00007710
		[Token(Token = "0x6001430")]
		[Address(RVA = "0x101280690", Offset = "0x1280690", VA = "0x101280690")]
		public static QuestListDB_Param GetParam(this QuestListDB self, int questID)
		{
			return default(QuestListDB_Param);
		}

		// Token: 0x0600157C RID: 5500 RVA: 0x00009528 File Offset: 0x00007728
		[Token(Token = "0x6001431")]
		[Address(RVA = "0x1012807C0", Offset = "0x12807C0", VA = "0x1012807C0")]
		public static int GetWaveNum(ref QuestListDB_Param questParam)
		{
			return 0;
		}
	}
}
