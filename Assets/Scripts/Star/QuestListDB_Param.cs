﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200057A RID: 1402
	[Token(Token = "0x200046D")]
	[Serializable]
	[StructLayout(0, Size = 120)]
	public struct QuestListDB_Param
	{
		// Token: 0x040019CD RID: 6605
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001337")]
		public int m_ID;

		// Token: 0x040019CE RID: 6606
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001338")]
		public int m_Category;

		// Token: 0x040019CF RID: 6607
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001339")]
		public string m_QuestName;

		// Token: 0x040019D0 RID: 6608
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400133A")]
		public int m_BgID;

		// Token: 0x040019D1 RID: 6609
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400133B")]
		public string m_BGMCueName;

		// Token: 0x040019D2 RID: 6610
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400133C")]
		public string m_LastWaveBGMCueName;

		// Token: 0x040019D3 RID: 6611
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400133D")]
		public byte m_IsWarning;

		// Token: 0x040019D4 RID: 6612
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400133E")]
		public int m_Section;

		// Token: 0x040019D5 RID: 6613
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400133F")]
		public int m_TypeIconID;

		// Token: 0x040019D6 RID: 6614
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001340")]
		public byte m_IsHideElement;

		// Token: 0x040019D7 RID: 6615
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001341")]
		public int m_Stamina;

		// Token: 0x040019D8 RID: 6616
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001342")]
		public int m_KeyItemID;

		// Token: 0x040019D9 RID: 6617
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001343")]
		public int m_KeyItemNum;

		// Token: 0x040019DA RID: 6618
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001344")]
		public int m_RewardMoney;

		// Token: 0x040019DB RID: 6619
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001345")]
		public int m_RewardUserExp;

		// Token: 0x040019DC RID: 6620
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4001346")]
		public int m_RewardCharaExp;

		// Token: 0x040019DD RID: 6621
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4001347")]
		public int m_RewardFriendshipExp;

		// Token: 0x040019DE RID: 6622
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4001348")]
		public byte m_IsRetryLimit;

		// Token: 0x040019DF RID: 6623
		[Cpp2IlInjected.FieldOffset(Offset = "0x55")]
		[Token(Token = "0x4001349")]
		public byte m_IsLoserBattle;

		// Token: 0x040019E0 RID: 6624
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400134A")]
		public int[] m_WaveIDs;

		// Token: 0x040019E1 RID: 6625
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400134B")]
		public byte m_IsMain;

		// Token: 0x040019E2 RID: 6626
		[Cpp2IlInjected.FieldOffset(Offset = "0x61")]
		[Token(Token = "0x400134C")]
		public byte m_IsAdvOnly;

		// Token: 0x040019E3 RID: 6627
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400134D")]
		public int m_AdvID_Prev;

		// Token: 0x040019E4 RID: 6628
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400134E")]
		public int m_AdvID_After;

		// Token: 0x040019E5 RID: 6629
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400134F")]
		public int m_AdvID_AfterBranch;

		// Token: 0x040019E6 RID: 6630
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4001350")]
		public int m_StoreReview;

		// Token: 0x040019E7 RID: 6631
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4001351")]
		public byte m_IsNpcOnly;
	}
}
