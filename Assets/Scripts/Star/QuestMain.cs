﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x020007F6 RID: 2038
	[Token(Token = "0x2000605")]
	[StructLayout(3)]
	public class QuestMain : GameStateMain
	{
		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06001FB8 RID: 8120 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000224")]
		public GameObject QuestLandMarkUIPrefab
		{
			[Token(Token = "0x6001D30")]
			[Address(RVA = "0x10128084C", Offset = "0x128084C", VA = "0x10128084C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06001FB9 RID: 8121 RVA: 0x0000E058 File Offset: 0x0000C258
		// (set) Token: 0x06001FBA RID: 8122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000225")]
		public bool IsQuestStartConfirm
		{
			[Token(Token = "0x6001D31")]
			[Address(RVA = "0x101280854", Offset = "0x1280854", VA = "0x101280854")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001D32")]
			[Address(RVA = "0x10128085C", Offset = "0x128085C", VA = "0x10128085C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06001FBB RID: 8123 RVA: 0x0000E070 File Offset: 0x0000C270
		// (set) Token: 0x06001FBC RID: 8124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000226")]
		public bool IsComingSoonAnim
		{
			[Token(Token = "0x6001D33")]
			[Address(RVA = "0x101280864", Offset = "0x1280864", VA = "0x101280864")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001D34")]
			[Address(RVA = "0x10128086C", Offset = "0x128086C", VA = "0x10128086C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06001FBD RID: 8125 RVA: 0x0000E088 File Offset: 0x0000C288
		// (set) Token: 0x06001FBE RID: 8126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000227")]
		public int ClearAnimChapterId
		{
			[Token(Token = "0x6001D35")]
			[Address(RVA = "0x101280874", Offset = "0x1280874", VA = "0x101280874")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6001D36")]
			[Address(RVA = "0x10128087C", Offset = "0x128087C", VA = "0x10128087C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06001FBF RID: 8127 RVA: 0x0000E0A0 File Offset: 0x0000C2A0
		// (set) Token: 0x06001FC0 RID: 8128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000228")]
		public bool IsReserveFriendList
		{
			[Token(Token = "0x6001D37")]
			[Address(RVA = "0x101280884", Offset = "0x1280884", VA = "0x101280884")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001D38")]
			[Address(RVA = "0x10128088C", Offset = "0x128088C", VA = "0x10128088C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06001FC1 RID: 8129 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001FC2 RID: 8130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000229")]
		public QuestMapScene QuestMapScene
		{
			[Token(Token = "0x6001D39")]
			[Address(RVA = "0x101280894", Offset = "0x1280894", VA = "0x101280894")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001D3A")]
			[Address(RVA = "0x10128089C", Offset = "0x128089C", VA = "0x10128089C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06001FC3 RID: 8131 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700022A")]
		public QuestMapHandler QuestMapHndl
		{
			[Token(Token = "0x6001D3B")]
			[Address(RVA = "0x1012808A4", Offset = "0x12808A4", VA = "0x1012808A4")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06001FC4 RID: 8132 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001FC5 RID: 8133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700022B")]
		public CharaListUI CharaListUI
		{
			[Token(Token = "0x6001D3C")]
			[Address(RVA = "0x1012808B8", Offset = "0x12808B8", VA = "0x1012808B8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001D3D")]
			[Address(RVA = "0x1012808C0", Offset = "0x12808C0", VA = "0x1012808C0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06001FC6 RID: 8134 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001FC7 RID: 8135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700022C")]
		public MasterDisplayUI MasterUI
		{
			[Token(Token = "0x6001D3E")]
			[Address(RVA = "0x1012808C8", Offset = "0x12808C8", VA = "0x1012808C8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001D3F")]
			[Address(RVA = "0x1012808D0", Offset = "0x12808D0", VA = "0x1012808D0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06001FC8 RID: 8136 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06001FC9 RID: 8137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700022D")]
		public QuestEventAdditiveUIScene QuestEventAdditiveUIScene
		{
			[Token(Token = "0x6001D40")]
			[Address(RVA = "0x1012808D8", Offset = "0x12808D8", VA = "0x1012808D8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6001D41")]
			[Address(RVA = "0x1012808E0", Offset = "0x12808E0", VA = "0x1012808E0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06001FCA RID: 8138 RVA: 0x0000E0B8 File Offset: 0x0000C2B8
		// (set) Token: 0x06001FCB RID: 8139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700022E")]
		public float HorizontalScrollNormalizedPosition
		{
			[Token(Token = "0x6001D42")]
			[Address(RVA = "0x1012808E8", Offset = "0x12808E8", VA = "0x1012808E8")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6001D43")]
			[Address(RVA = "0x1012808F0", Offset = "0x12808F0", VA = "0x1012808F0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06001FCC RID: 8140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D44")]
		[Address(RVA = "0x1012808F8", Offset = "0x12808F8", VA = "0x1012808F8")]
		private void Awake()
		{
		}

		// Token: 0x06001FCD RID: 8141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D45")]
		[Address(RVA = "0x101280980", Offset = "0x1280980", VA = "0x101280980")]
		private void Start()
		{
		}

		// Token: 0x06001FCE RID: 8142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D46")]
		[Address(RVA = "0x10128098C", Offset = "0x128098C", VA = "0x10128098C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06001FCF RID: 8143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D47")]
		[Address(RVA = "0x1012809C8", Offset = "0x12809C8", VA = "0x1012809C8", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x06001FD0 RID: 8144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D48")]
		[Address(RVA = "0x101280A24", Offset = "0x1280A24", VA = "0x101280A24")]
		private void BGFilterUpdate()
		{
		}

		// Token: 0x06001FD1 RID: 8145 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001D49")]
		[Address(RVA = "0x101280C48", Offset = "0x1280C48", VA = "0x101280C48", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001FD2 RID: 8146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D4A")]
		[Address(RVA = "0x101280E94", Offset = "0x1280E94", VA = "0x101280E94", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06001FD3 RID: 8147 RVA: 0x0000E0D0 File Offset: 0x0000C2D0
		[Token(Token = "0x6001D4B")]
		[Address(RVA = "0x1012813A0", Offset = "0x12813A0", VA = "0x1012813A0", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06001FD4 RID: 8148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D4C")]
		[Address(RVA = "0x101281438", Offset = "0x1281438", VA = "0x101281438")]
		public void BGLoadCommon(bool fade)
		{
		}

		// Token: 0x06001FD5 RID: 8149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D4D")]
		[Address(RVA = "0x10128156C", Offset = "0x128156C", VA = "0x10128156C")]
		public void BGLoadChapter(int chapterId, bool fade)
		{
		}

		// Token: 0x06001FD6 RID: 8150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D4E")]
		[Address(RVA = "0x1012817D4", Offset = "0x12817D4", VA = "0x1012817D4")]
		public void BGLoadPart(int part, bool fade)
		{
		}

		// Token: 0x06001FD7 RID: 8151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D4F")]
		[Address(RVA = "0x101281934", Offset = "0x1281934", VA = "0x101281934")]
		public void BGLoadEvent(EventQuestUISettingDB_Param? param, bool fade)
		{
		}

		// Token: 0x06001FD8 RID: 8152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D50")]
		[Address(RVA = "0x101281B04", Offset = "0x1281B04", VA = "0x101281B04")]
		public void BGLoadEvent(int eventType, bool fade)
		{
		}

		// Token: 0x06001FD9 RID: 8153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D51")]
		[Address(RVA = "0x101281BD4", Offset = "0x1281BD4", VA = "0x101281BD4")]
		public void BGLoadChara(int charaID, bool fade)
		{
		}

		// Token: 0x06001FDA RID: 8154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D52")]
		[Address(RVA = "0x101281DF8", Offset = "0x1281DF8", VA = "0x101281DF8")]
		public void BGLoadOffer(eTitleType title, bool fade)
		{
		}

		// Token: 0x06001FDB RID: 8155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D53")]
		[Address(RVA = "0x101281FB0", Offset = "0x1281FB0", VA = "0x101281FB0")]
		public void BGClose()
		{
		}

		// Token: 0x06001FDC RID: 8156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D54")]
		[Address(RVA = "0x101282038", Offset = "0x1282038", VA = "0x101282038")]
		public void ForceEnableBGFilter(bool flg)
		{
		}

		// Token: 0x06001FDD RID: 8157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D55")]
		[Address(RVA = "0x101282040", Offset = "0x1282040", VA = "0x101282040")]
		public QuestMain()
		{
		}

		// Token: 0x04002FE0 RID: 12256
		[Token(Token = "0x40024A5")]
		private const string Part2BGResourceName = "BG_Quest_ChapterSelect_1";

		// Token: 0x04002FE1 RID: 12257
		[Token(Token = "0x40024A6")]
		public const int STATE_INIT = 0;

		// Token: 0x04002FE2 RID: 12258
		[Token(Token = "0x40024A7")]
		public const int STATE_QUEST_CATEGORY_SELECT = 1;

		// Token: 0x04002FE3 RID: 12259
		[Token(Token = "0x40024A8")]
		public const int STATE_QUEST_GROUP_SELECT = 2;

		// Token: 0x04002FE4 RID: 12260
		[Token(Token = "0x40024A9")]
		public const int STATE_QUEST_CHAPTER_SELECT = 3;

		// Token: 0x04002FE5 RID: 12261
		[Token(Token = "0x40024AA")]
		public const int STATE_QUEST_SELECT = 4;

		// Token: 0x04002FE6 RID: 12262
		[Token(Token = "0x40024AB")]
		public const int STATE_QUEST_MAP = 5;

		// Token: 0x04002FE7 RID: 12263
		[Token(Token = "0x40024AC")]
		public const int STATE_QUEST_CHARA_SELECT = 6;

		// Token: 0x04002FE8 RID: 12264
		[Token(Token = "0x40024AD")]
		public const int STATE_QUEST_OFFER_SELECT = 7;

		// Token: 0x04002FE9 RID: 12265
		[Token(Token = "0x40024AE")]
		public const int STATE_QUEST_CONFIRM = 8;

		// Token: 0x04002FEA RID: 12266
		[Token(Token = "0x40024AF")]
		public const int STATE_QUEST_CHARALIST_PARTY_EDIT = 9;

		// Token: 0x04002FEB RID: 12267
		[Token(Token = "0x40024B0")]
		public const int STATE_QUEST_FRIEND_SELECT = 10;

		// Token: 0x04002FEC RID: 12268
		[Token(Token = "0x40024B1")]
		public const int STATE_QUEST_MAINPART_SELECT = 11;

		// Token: 0x04002FED RID: 12269
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024B2")]
		[SerializeField]
		private AnimUIPlayer m_BGFilter;

		// Token: 0x04002FEE RID: 12270
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40024B3")]
		private bool m_IsUseBGFilter;

		// Token: 0x04002FEF RID: 12271
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40024B4")]
		[SerializeField]
		private GameObject m_QuestLandMarkUIPrefab;
	}
}
