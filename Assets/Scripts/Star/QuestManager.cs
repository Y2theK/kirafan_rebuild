﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using CommonLogic;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.Town;
using WWWTypes;

namespace Star
{
	// Token: 0x0200091E RID: 2334
	[Token(Token = "0x20006A5")]
	[StructLayout(3)]
	public sealed class QuestManager
	{
		// Token: 0x06002687 RID: 9863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002385")]
		[Address(RVA = "0x101282048", Offset = "0x1282048", VA = "0x101282048")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x06002688 RID: 9864 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002386")]
		[Address(RVA = "0x1012812A0", Offset = "0x12812A0", VA = "0x1012812A0")]
		public QuestManager.QuestData GetQuestData(int questID)
		{
			return null;
		}

		// Token: 0x06002689 RID: 9865 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002387")]
		[Address(RVA = "0x101282188", Offset = "0x1282188", VA = "0x101282188")]
		public Dictionary<long, UserSupportData> GetNpcSupportDatas(int questID)
		{
			return null;
		}

		// Token: 0x0600268A RID: 9866 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002388")]
		[Address(RVA = "0x101282888", Offset = "0x1282888", VA = "0x101282888")]
		public UserSupportData GetNpcSupportData(int questID, long npcID)
		{
			return null;
		}

		// Token: 0x0600268B RID: 9867 RVA: 0x00010548 File Offset: 0x0000E748
		[Token(Token = "0x6002389")]
		[Address(RVA = "0x10128292C", Offset = "0x128292C", VA = "0x10128292C")]
		public bool IsClearQuest(int questID)
		{
			return default(bool);
		}

		// Token: 0x0600268C RID: 9868 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600238A")]
		[Address(RVA = "0x101282950", Offset = "0x1282950", VA = "0x101282950")]
		public bool[] GetEnemyTypes(int questID)
		{
			return null;
		}

		// Token: 0x0600268D RID: 9869 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600238B")]
		[Address(RVA = "0x101282CF8", Offset = "0x1282CF8", VA = "0x101282CF8")]
		public List<int> GetDropItemIDs(int questID)
		{
			return null;
		}

		// Token: 0x0600268E RID: 9870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600238C")]
		[Address(RVA = "0x101282128", Offset = "0x1282128", VA = "0x101282128")]
		private void ClearLastOpenDifficulty()
		{
		}

		// Token: 0x0600268F RID: 9871 RVA: 0x00010560 File Offset: 0x0000E760
		[Token(Token = "0x600238D")]
		[Address(RVA = "0x101283288", Offset = "0x1283288", VA = "0x101283288")]
		public QuestDefine.eDifficulty GetLastOpenDifficulty(int chapterId)
		{
			return QuestDefine.eDifficulty.Normal;
		}

		// Token: 0x06002690 RID: 9872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600238E")]
		[Address(RVA = "0x101283334", Offset = "0x1283334", VA = "0x101283334")]
		public void SetLastOpenDifficulty(int chapterId, QuestDefine.eDifficulty lastDifficulty)
		{
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06002691 RID: 9873 RVA: 0x00010578 File Offset: 0x0000E778
		[Token(Token = "0x17000279")]
		public bool IsNewDispChapter
		{
			[Token(Token = "0x600238F")]
			[Address(RVA = "0x10128339C", Offset = "0x128339C", VA = "0x10128339C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002692 RID: 9874 RVA: 0x00010590 File Offset: 0x0000E790
		[Token(Token = "0x6002390")]
		[Address(RVA = "0x1012833A4", Offset = "0x12833A4", VA = "0x1012833A4")]
		public int GetLastPlayedChapterQuestId(int part)
		{
			return 0;
		}

		// Token: 0x06002693 RID: 9875 RVA: 0x000105A8 File Offset: 0x0000E7A8
		[Token(Token = "0x6002391")]
		[Address(RVA = "0x101283404", Offset = "0x1283404", VA = "0x101283404")]
		public int GetPlayedOpenChapterId(int part)
		{
			return 0;
		}

		// Token: 0x06002694 RID: 9876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002392")]
		[Address(RVA = "0x101283464", Offset = "0x1283464", VA = "0x101283464")]
		public void SetPlayedOpenChapterId(int part, int chapterId)
		{
		}

		// Token: 0x06002695 RID: 9877 RVA: 0x000105C0 File Offset: 0x0000E7C0
		[Token(Token = "0x6002393")]
		[Address(RVA = "0x1012834C0", Offset = "0x12834C0", VA = "0x1012834C0")]
		public bool CheckPlayOpenChapter(int part)
		{
			return default(bool);
		}

		// Token: 0x06002696 RID: 9878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002394")]
		[Address(RVA = "0x1012839E0", Offset = "0x12839E0", VA = "0x1012839E0")]
		private void CreateChapterDatas(QuestChapterArray[] srcs)
		{
		}

		// Token: 0x06002697 RID: 9879 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002395")]
		[Address(RVA = "0x101283874", Offset = "0x1283874", VA = "0x101283874")]
		public List<QuestManager.ChapterData[]> GetChapterList(int part)
		{
			return null;
		}

		// Token: 0x06002698 RID: 9880 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002396")]
		[Address(RVA = "0x101283EE8", Offset = "0x1283EE8", VA = "0x101283EE8")]
		public List<int> GetPartList()
		{
			return null;
		}

		// Token: 0x06002699 RID: 9881 RVA: 0x000105D8 File Offset: 0x0000E7D8
		[Token(Token = "0x6002397")]
		[Address(RVA = "0x101284060", Offset = "0x1284060", VA = "0x101284060")]
		public int ChapterIdToPart(int chapterId)
		{
			return 0;
		}

		// Token: 0x0600269A RID: 9882 RVA: 0x000105F0 File Offset: 0x0000E7F0
		[Token(Token = "0x6002398")]
		[Address(RVA = "0x1012840D8", Offset = "0x12840D8", VA = "0x1012840D8")]
		public bool CheckChapterCleared(QuestManager.ChapterData chapterData)
		{
			return default(bool);
		}

		// Token: 0x0600269B RID: 9883 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002399")]
		[Address(RVA = "0x101283660", Offset = "0x1283660", VA = "0x101283660")]
		public QuestManager.ChapterData GetLastChapterData(int part)
		{
			return null;
		}

		// Token: 0x0600269C RID: 9884 RVA: 0x00010608 File Offset: 0x0000E808
		[Token(Token = "0x600239A")]
		[Address(RVA = "0x101284248", Offset = "0x1284248", VA = "0x101284248")]
		public int GetLastChapterQuestId(int part)
		{
			return 0;
		}

		// Token: 0x0600269D RID: 9885 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600239B")]
		[Address(RVA = "0x101284080", Offset = "0x1284080", VA = "0x101284080")]
		public QuestManager.ChapterData GetChapterData(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			return null;
		}

		// Token: 0x0600269E RID: 9886 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600239C")]
		[Address(RVA = "0x101284348", Offset = "0x1284348", VA = "0x101284348")]
		public QuestManager.ChapterData[] GetChapterDatas(int chapterID)
		{
			return null;
		}

		// Token: 0x0600269F RID: 9887 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600239D")]
		[Address(RVA = "0x1012844EC", Offset = "0x12844EC", VA = "0x1012844EC")]
		public QuestManager.ChapterData GetChapterDataFromQuestID(int questID)
		{
			return null;
		}

		// Token: 0x060026A0 RID: 9888 RVA: 0x00010620 File Offset: 0x0000E820
		[Token(Token = "0x600239E")]
		[Address(RVA = "0x101284830", Offset = "0x1284830", VA = "0x101284830")]
		public int GetNextChapterID(int chapterID)
		{
			return 0;
		}

		// Token: 0x060026A1 RID: 9889 RVA: 0x00010638 File Offset: 0x0000E838
		[Token(Token = "0x600239F")]
		[Address(RVA = "0x101284A4C", Offset = "0x1284A4C", VA = "0x101284A4C")]
		public int CalcChapterClearRatio(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			return 0;
		}

		// Token: 0x060026A2 RID: 9890 RVA: 0x00010650 File Offset: 0x0000E850
		[Token(Token = "0x60023A0")]
		[Address(RVA = "0x101284CC4", Offset = "0x1284CC4", VA = "0x101284CC4")]
		public bool IsClearChapterDifficulty(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			return default(bool);
		}

		// Token: 0x060026A3 RID: 9891 RVA: 0x00010668 File Offset: 0x0000E868
		[Token(Token = "0x60023A1")]
		[Address(RVA = "0x101284D74", Offset = "0x1284D74", VA = "0x101284D74")]
		public int GetFirstViewChapterQuestID(int part)
		{
			return 0;
		}

		// Token: 0x060026A4 RID: 9892 RVA: 0x00010680 File Offset: 0x0000E880
		[Token(Token = "0x60023A2")]
		[Address(RVA = "0x101284F90", Offset = "0x1284F90", VA = "0x101284F90")]
		public bool IsNextComingSoon(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			return default(bool);
		}

		// Token: 0x060026A5 RID: 9893 RVA: 0x00010698 File Offset: 0x0000E898
		[Token(Token = "0x60023A3")]
		[Address(RVA = "0x101284FE4", Offset = "0x1284FE4", VA = "0x101284FE4")]
		public bool IsExistNewChapterQuest(int part)
		{
			return default(bool);
		}

		// Token: 0x060026A6 RID: 9894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023A4")]
		[Address(RVA = "0x101285154", Offset = "0x1285154", VA = "0x101285154")]
		public void SetComingSoonTrigger(bool isFirstClear, int lastPlayedQuestID)
		{
		}

		// Token: 0x060026A7 RID: 9895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023A5")]
		[Address(RVA = "0x1012852E8", Offset = "0x12852E8", VA = "0x1012852E8")]
		public void SetClearAnimChapter(bool isFirstClear, int lastPlayedQuestId)
		{
		}

		// Token: 0x060026A8 RID: 9896 RVA: 0x000106B0 File Offset: 0x0000E8B0
		[Token(Token = "0x60023A6")]
		[Address(RVA = "0x101285428", Offset = "0x1285428", VA = "0x101285428")]
		public bool IsExistTrialAndCollect(QuestManager.ChapterData chapterdata)
		{
			return default(bool);
		}

		// Token: 0x060026A9 RID: 9897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023A7")]
		[Address(RVA = "0x101285474", Offset = "0x1285474", VA = "0x101285474")]
		private void CreateEventGroupDatas(PlayerEventQuest[] srcs)
		{
		}

		// Token: 0x060026AA RID: 9898 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023A8")]
		[Address(RVA = "0x101285D20", Offset = "0x1285D20", VA = "0x101285D20")]
		public List<int> GetAvailableEventTypes()
		{
			return null;
		}

		// Token: 0x060026AB RID: 9899 RVA: 0x000106C8 File Offset: 0x0000E8C8
		[Token(Token = "0x60023A9")]
		[Address(RVA = "0x101285F64", Offset = "0x1285F64", VA = "0x101285F64")]
		public int GetEventGroupDataNum()
		{
			return 0;
		}

		// Token: 0x060026AC RID: 9900 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AA")]
		[Address(RVA = "0x101285FC4", Offset = "0x1285FC4", VA = "0x101285FC4")]
		public QuestManager.EventGroupData GetEventGroupDataAt(int index)
		{
			return null;
		}

		// Token: 0x060026AD RID: 9901 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AB")]
		[Address(RVA = "0x101286078", Offset = "0x1286078", VA = "0x101286078")]
		public QuestManager.EventGroupData GetEventGroupData(int groupID)
		{
			return null;
		}

		// Token: 0x060026AE RID: 9902 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AC")]
		[Address(RVA = "0x101286178", Offset = "0x1286178", VA = "0x101286178")]
		public QuestManager.EventGroupData GetEventGroupDataFromEventID(int eventID)
		{
			return null;
		}

		// Token: 0x060026AF RID: 9903 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AD")]
		[Address(RVA = "0x101286328", Offset = "0x1286328", VA = "0x101286328")]
		public List<QuestManager.EventGroupData> GetEventGroupDatasFromEventType(int eventType)
		{
			return null;
		}

		// Token: 0x060026B0 RID: 9904 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AE")]
		[Address(RVA = "0x10128646C", Offset = "0x128646C", VA = "0x10128646C")]
		public QuestManager.EventGroupData GetFirstEventGroupDataFromEventType(int eventType)
		{
			return null;
		}

		// Token: 0x060026B1 RID: 9905 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023AF")]
		[Address(RVA = "0x10128653C", Offset = "0x128653C", VA = "0x10128653C")]
		public QuestManager.EventGroupData GetEventGroupDataFromQuestID(int questID)
		{
			return null;
		}

		// Token: 0x060026B2 RID: 9906 RVA: 0x000106E0 File Offset: 0x0000E8E0
		[Token(Token = "0x60023B0")]
		[Address(RVA = "0x101286690", Offset = "0x1286690", VA = "0x101286690")]
		public bool IsQuestMap(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060026B3 RID: 9907 RVA: 0x000106F8 File Offset: 0x0000E8F8
		[Token(Token = "0x60023B1")]
		[Address(RVA = "0x1012866B4", Offset = "0x12866B4", VA = "0x1012866B4")]
		public int GetEventGroupDataNumFromEventType(int eventType)
		{
			return 0;
		}

		// Token: 0x060026B4 RID: 9908 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023B2")]
		[Address(RVA = "0x101286790", Offset = "0x1286790", VA = "0x101286790")]
		public QuestManager.EventData GetEventData(int eventID)
		{
			return null;
		}

		// Token: 0x060026B5 RID: 9909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023B3")]
		[Address(RVA = "0x10128696C", Offset = "0x128696C", VA = "0x10128696C")]
		private void CreateCharaQuestDatas(PlayerCharacterQuest[] srcs)
		{
		}

		// Token: 0x060026B6 RID: 9910 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023B4")]
		[Address(RVA = "0x101286AB8", Offset = "0x1286AB8", VA = "0x101286AB8")]
		public List<QuestManager.CharaQuestData> GetCharaQuestDatas()
		{
			return null;
		}

		// Token: 0x060026B7 RID: 9911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023B5")]
		[Address(RVA = "0x101286AC0", Offset = "0x1286AC0", VA = "0x101286AC0")]
		private void CreateOfferQuestDatas(PlayerOfferQuest[] srcs)
		{
		}

		// Token: 0x060026B8 RID: 9912 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023B6")]
		[Address(RVA = "0x101286C74", Offset = "0x1286C74", VA = "0x101286C74")]
		public QuestManager.OfferQuestData GetOfferQuestData(int questId)
		{
			return null;
		}

		// Token: 0x060026B9 RID: 9913 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023B7")]
		[Address(RVA = "0x101286D74", Offset = "0x1286D74", VA = "0x101286D74")]
		public List<QuestManager.OfferQuestData> GetOfferQuestDatas()
		{
			return null;
		}

		// Token: 0x060026BA RID: 9914 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023B8")]
		[Address(RVA = "0x101286D7C", Offset = "0x1286D7C", VA = "0x101286D7C")]
		public List<QuestManager.OfferQuestData> GetAvailableOfferQuestDatas(eTitleType title)
		{
			return null;
		}

		// Token: 0x060026BB RID: 9915 RVA: 0x00010710 File Offset: 0x0000E910
		[Token(Token = "0x60023B9")]
		[Address(RVA = "0x101286EF4", Offset = "0x1286EF4", VA = "0x101286EF4")]
		public bool ExistNewOfferQuest(List<QuestManager.OfferQuestData> list)
		{
			return default(bool);
		}

		// Token: 0x060026BC RID: 9916 RVA: 0x00010728 File Offset: 0x0000E928
		[Token(Token = "0x60023BA")]
		[Address(RVA = "0x101287098", Offset = "0x1287098", VA = "0x101287098")]
		public bool ExistRepeatOfferQuest(List<QuestManager.OfferQuestData> list)
		{
			return default(bool);
		}

		// Token: 0x060026BD RID: 9917 RVA: 0x00010740 File Offset: 0x0000E940
		[Token(Token = "0x60023BB")]
		[Address(RVA = "0x1012871D0", Offset = "0x12871D0", VA = "0x1012871D0")]
		public bool IsExistSaveResponse()
		{
			return default(bool);
		}

		// Token: 0x060026BE RID: 9918 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023BC")]
		[Address(RVA = "0x1012872AC", Offset = "0x12872AC", VA = "0x1012872AC")]
		public BattleResult CreateResult(int questID, bool isAdvOnly, bool isSaveResponse)
		{
			return null;
		}

		// Token: 0x060026BF RID: 9919 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023BD")]
		[Address(RVA = "0x101287354", Offset = "0x1287354", VA = "0x101287354")]
		public BattleResult GetResult()
		{
			return null;
		}

		// Token: 0x060026C0 RID: 9920 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023BE")]
		[Address(RVA = "0x10128735C", Offset = "0x128735C", VA = "0x10128735C")]
		public List<int> GetReadGroupIDs()
		{
			return null;
		}

		// Token: 0x060026C1 RID: 9921 RVA: 0x00010758 File Offset: 0x0000E958
		[Token(Token = "0x60023BF")]
		[Address(RVA = "0x101287364", Offset = "0x1287364", VA = "0x101287364")]
		public bool IsExistReadGroupID(int groupID)
		{
			return default(bool);
		}

		// Token: 0x060026C2 RID: 9922 RVA: 0x00010770 File Offset: 0x0000E970
		[Token(Token = "0x60023C0")]
		[Address(RVA = "0x101287434", Offset = "0x1287434", VA = "0x101287434")]
		public bool IsExistNotSaveReadGroup(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060026C3 RID: 9923 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C1")]
		[Address(RVA = "0x101287528", Offset = "0x1287528", VA = "0x101287528")]
		public QuestManager.EventQuestPeriod GetEventQuestPeriod(int eventType)
		{
			return null;
		}

		// Token: 0x060026C4 RID: 9924 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C2")]
		[Address(RVA = "0x101287628", Offset = "0x1287628", VA = "0x101287628")]
		public List<QuestManager.EventQuestPeriodGroup> GetLockedEventQuestPeriodGroups(int eventType)
		{
			return null;
		}

		// Token: 0x060026C5 RID: 9925 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C3")]
		[Address(RVA = "0x101287794", Offset = "0x1287794", VA = "0x101287794")]
		public List<QuestManager.EventQuestPeriodGroup> GetUnlockedEventQuestPeriodGroups(int eventType)
		{
			return null;
		}

		// Token: 0x060026C6 RID: 9926 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C4")]
		[Address(RVA = "0x101287900", Offset = "0x1287900", VA = "0x101287900")]
		public QuestManager.EventQuestPeriodGroup GetQuestPeriodGroup(int groupID)
		{
			return null;
		}

		// Token: 0x060026C7 RID: 9927 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C5")]
		[Address(RVA = "0x101287A90", Offset = "0x1287A90", VA = "0x101287A90")]
		public QuestManager.StaminaReduction GetStaminaReductionFromQuestID(int questID)
		{
			return null;
		}

		// Token: 0x060026C8 RID: 9928 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C6")]
		[Address(RVA = "0x101287B34", Offset = "0x1287B34", VA = "0x101287B34")]
		public QuestManager.StaminaReduction GetStaminaReductionChapter(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			return null;
		}

		// Token: 0x060026C9 RID: 9929 RVA: 0x00010788 File Offset: 0x0000E988
		[Token(Token = "0x60023C7")]
		[Address(RVA = "0x101287D5C", Offset = "0x1287D5C", VA = "0x101287D5C")]
		public bool CheckExistChapterStaminaReduction()
		{
			return default(bool);
		}

		// Token: 0x060026CA RID: 9930 RVA: 0x000107A0 File Offset: 0x0000E9A0
		[Token(Token = "0x60023C8")]
		[Address(RVA = "0x101287EC4", Offset = "0x1287EC4", VA = "0x101287EC4")]
		public bool CheckExistChapterStaminaReduction(int part)
		{
			return default(bool);
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023C9")]
		[Address(RVA = "0x101287C54", Offset = "0x1287C54", VA = "0x101287C54")]
		public QuestManager.StaminaReduction GetStaminaReductionEventGroup(int groupID)
		{
			return null;
		}

		// Token: 0x060026CC RID: 9932 RVA: 0x000107B8 File Offset: 0x0000E9B8
		[Token(Token = "0x60023CA")]
		[Address(RVA = "0x10128803C", Offset = "0x128803C", VA = "0x10128803C")]
		public bool CheckExistEventGroupStaminaReduction(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060026CD RID: 9933 RVA: 0x000107D0 File Offset: 0x0000E9D0
		[Token(Token = "0x60023CB")]
		[Address(RVA = "0x101288160", Offset = "0x1288160", VA = "0x101288160")]
		public bool CheckExistStaminaReduction()
		{
			return default(bool);
		}

		// Token: 0x060026CE RID: 9934 RVA: 0x000107E8 File Offset: 0x0000E9E8
		[Token(Token = "0x60023CC")]
		[Address(RVA = "0x1012882C8", Offset = "0x12882C8", VA = "0x1012882C8")]
		public bool OpenNoticeMessage(Action onClose)
		{
			return default(bool);
		}

		// Token: 0x060026CF RID: 9935 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023CD")]
		[Address(RVA = "0x10128842C", Offset = "0x128842C", VA = "0x10128842C")]
		public QuestManager.PointEventData GetPointEventData(long pointEventID)
		{
			return null;
		}

		// Token: 0x060026D0 RID: 9936 RVA: 0x00010800 File Offset: 0x0000EA00
		[Token(Token = "0x60023CE")]
		[Address(RVA = "0x10128852C", Offset = "0x128852C", VA = "0x10128852C")]
		public bool IsExpiredPointEventData(long pointEventID)
		{
			return default(bool);
		}

		// Token: 0x060026D1 RID: 9937 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023CF")]
		[Address(RVA = "0x101288630", Offset = "0x1288630", VA = "0x101288630")]
		public QuestManager.PointEventData GetPointEventDataFromQuestID(int questID)
		{
			return null;
		}

		// Token: 0x060026D2 RID: 9938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60023D0")]
		[Address(RVA = "0x10127FB1C", Offset = "0x127FB1C", VA = "0x10127FB1C")]
		public QuestManager.SelectQuest GetSelectQuest()
		{
			return null;
		}

		// Token: 0x14000034 RID: 52
		// (add) Token: 0x060026D3 RID: 9939 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026D4 RID: 9940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000034")]
		private event Action m_OnResponse_QuestGetAll
		{
			[Token(Token = "0x60023D1")]
			[Address(RVA = "0x1012887B8", Offset = "0x12887B8", VA = "0x1012887B8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023D2")]
			[Address(RVA = "0x1012888C4", Offset = "0x12888C4", VA = "0x1012888C4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000035 RID: 53
		// (add) Token: 0x060026D5 RID: 9941 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026D6 RID: 9942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000035")]
		private event Action m_OnResponse_QuestChapterGetAll
		{
			[Token(Token = "0x60023D3")]
			[Address(RVA = "0x1012889D0", Offset = "0x12889D0", VA = "0x1012889D0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023D4")]
			[Address(RVA = "0x101288ADC", Offset = "0x1288ADC", VA = "0x101288ADC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000036 RID: 54
		// (add) Token: 0x060026D7 RID: 9943 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026D8 RID: 9944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000036")]
		private event Action<QuestDefine.eReturnLogAdd, string> m_OnResponse_QuestLogAdd
		{
			[Token(Token = "0x60023D5")]
			[Address(RVA = "0x101288BE8", Offset = "0x1288BE8", VA = "0x101288BE8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023D6")]
			[Address(RVA = "0x101288CF4", Offset = "0x1288CF4", VA = "0x101288CF4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000037 RID: 55
		// (add) Token: 0x060026D9 RID: 9945 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026DA RID: 9946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000037")]
		private event Func<MeigewwwParam, Questlogcomplete, bool> m_OnResponse_QuestLogSet
		{
			[Token(Token = "0x60023D7")]
			[Address(RVA = "0x101288E00", Offset = "0x1288E00", VA = "0x101288E00")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023D8")]
			[Address(RVA = "0x101288F10", Offset = "0x1288F10", VA = "0x101288F10")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000038 RID: 56
		// (add) Token: 0x060026DB RID: 9947 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026DC RID: 9948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000038")]
		private event Func<MeigewwwParam, Questlogretry, bool> m_OnResponse_QuestRetry
		{
			[Token(Token = "0x60023D9")]
			[Address(RVA = "0x101289020", Offset = "0x1289020", VA = "0x101289020")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023DA")]
			[Address(RVA = "0x101289130", Offset = "0x1289130", VA = "0x101289130")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000039 RID: 57
		// (add) Token: 0x060026DD RID: 9949 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026DE RID: 9950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000039")]
		private event Action m_OnResponse_DeleteBSD
		{
			[Token(Token = "0x60023DB")]
			[Address(RVA = "0x101289240", Offset = "0x1289240", VA = "0x101289240")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023DC")]
			[Address(RVA = "0x101289350", Offset = "0x1289350", VA = "0x101289350")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400003A RID: 58
		// (add) Token: 0x060026DF RID: 9951 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026E0 RID: 9952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003A")]
		private event Action m_OnResponse_Read
		{
			[Token(Token = "0x60023DD")]
			[Address(RVA = "0x101289460", Offset = "0x1289460", VA = "0x101289460")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023DE")]
			[Address(RVA = "0x101289570", Offset = "0x1289570", VA = "0x101289570")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400003B RID: 59
		// (add) Token: 0x060026E1 RID: 9953 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026E2 RID: 9954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003B")]
		private event Action m_OnResponse_PointEventGet
		{
			[Token(Token = "0x60023DF")]
			[Address(RVA = "0x101289680", Offset = "0x1289680", VA = "0x101289680")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023E0")]
			[Address(RVA = "0x101289790", Offset = "0x1289790", VA = "0x101289790")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400003C RID: 60
		// (add) Token: 0x060026E3 RID: 9955 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060026E4 RID: 9956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003C")]
		private event Action m_OnResponse_PointEventReward
		{
			[Token(Token = "0x60023E1")]
			[Address(RVA = "0x1012898A0", Offset = "0x12898A0", VA = "0x1012898A0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60023E2")]
			[Address(RVA = "0x1012899B0", Offset = "0x12899B0", VA = "0x1012899B0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060026E5 RID: 9957 RVA: 0x00010818 File Offset: 0x0000EA18
		[Token(Token = "0x60023E3")]
		[Address(RVA = "0x101289AC0", Offset = "0x1289AC0", VA = "0x101289AC0")]
		public bool QuestGetAllApiTypeIsDebugFullOpen()
		{
			return default(bool);
		}

		// Token: 0x060026E6 RID: 9958 RVA: 0x00010830 File Offset: 0x0000EA30
		[Token(Token = "0x60023E4")]
		[Address(RVA = "0x101289AC8", Offset = "0x1289AC8", VA = "0x101289AC8")]
		public bool IsOverGetAllNextTime()
		{
			return default(bool);
		}

		// Token: 0x060026E7 RID: 9959 RVA: 0x00010848 File Offset: 0x0000EA48
		[Token(Token = "0x60023E5")]
		[Address(RVA = "0x101289B70", Offset = "0x1289B70", VA = "0x101289B70")]
		public bool Request_QuestGetAll(Action onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060026E8 RID: 9960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023E6")]
		[Address(RVA = "0x101289C6C", Offset = "0x1289C6C", VA = "0x101289C6C")]
		private void OnResponse_QuestGetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023E7")]
		[Address(RVA = "0x10128B210", Offset = "0x128B210", VA = "0x10128B210")]
		public void UpdateStaminaReductionAvailableFlg()
		{
		}

		// Token: 0x060026EA RID: 9962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023E8")]
		[Address(RVA = "0x10128B37C", Offset = "0x128B37C", VA = "0x10128B37C")]
		private void UpdateOutOfPeriod()
		{
		}

		// Token: 0x060026EB RID: 9963 RVA: 0x00010860 File Offset: 0x0000EA60
		[Token(Token = "0x60023E9")]
		[Address(RVA = "0x10128B658", Offset = "0x128B658", VA = "0x10128B658")]
		public bool Request_QuestChapterGetAll(Action onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060026EC RID: 9964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023EA")]
		[Address(RVA = "0x10128B754", Offset = "0x128B754", VA = "0x10128B754")]
		private void OnResponse_QuestChapterGetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026ED RID: 9965 RVA: 0x00010878 File Offset: 0x0000EA78
		[Token(Token = "0x60023EB")]
		[Address(RVA = "0x10128B8F8", Offset = "0x128B8F8", VA = "0x10128B8F8")]
		public bool Request_QuestLogAdd(bool isEventQuest, int questID, int eventID, long battlePartyMngId, long friendCharaMngID, long questNpcID, int useStamina, Action<QuestDefine.eReturnLogAdd, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026EE RID: 9966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023EC")]
		[Address(RVA = "0x10128BF9C", Offset = "0x128BF9C", VA = "0x10128BF9C")]
		private void OnResponse_QuestLogAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026EF RID: 9967 RVA: 0x00010890 File Offset: 0x0000EA90
		[Token(Token = "0x60023ED")]
		[Address(RVA = "0x10128C7C8", Offset = "0x128C7C8", VA = "0x10128C7C8")]
		public bool Request_QuestLogAddAdvOnly(bool isEventQuest, int questID, int eventID, Action<QuestDefine.eReturnLogAdd, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026F0 RID: 9968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023EE")]
		[Address(RVA = "0x10128C95C", Offset = "0x128C95C", VA = "0x10128C95C")]
		private void OnResponse_QuestLogAddAdvOnly(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026F1 RID: 9969 RVA: 0x000108A8 File Offset: 0x0000EAA8
		[Token(Token = "0x60023EF")]
		[Address(RVA = "0x10128CAD0", Offset = "0x128CAD0", VA = "0x10128CAD0")]
		public bool Request_QuestSuccess(BattlePartyData battlePartyData, Func<MeigewwwParam, Questlogcomplete, bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026F2 RID: 9970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F0")]
		[Address(RVA = "0x10128D5EC", Offset = "0x128D5EC", VA = "0x10128D5EC")]
		private void OnResponse_QuestSuccess(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026F3 RID: 9971 RVA: 0x000108C0 File Offset: 0x0000EAC0
		[Token(Token = "0x60023F1")]
		[Address(RVA = "0x10128F440", Offset = "0x128F440", VA = "0x10128F440")]
		public bool Request_QuestFailed(BattlePartyData battlePartyData, Func<MeigewwwParam, Questlogcomplete, bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026F4 RID: 9972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F2")]
		[Address(RVA = "0x10128F5D8", Offset = "0x128F5D8", VA = "0x10128F5D8")]
		private void OnResponse_QuestFailed(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026F5 RID: 9973 RVA: 0x000108D8 File Offset: 0x0000EAD8
		[Token(Token = "0x60023F3")]
		[Address(RVA = "0x10128F788", Offset = "0x128F788", VA = "0x10128F788")]
		public bool Request_QuestSuccessAdvOnly(Func<MeigewwwParam, Questlogcomplete, bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026F6 RID: 9974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F4")]
		[Address(RVA = "0x10128F99C", Offset = "0x128F99C", VA = "0x10128F99C")]
		private void OnResponse_QuestSuccessAdvOnly(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026F7 RID: 9975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F5")]
		[Address(RVA = "0x10128F124", Offset = "0x128F124", VA = "0x10128F124")]
		private void AddPresentCountFromReward()
		{
		}

		// Token: 0x060026F8 RID: 9976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F6")]
		[Address(RVA = "0x101282104", Offset = "0x1282104", VA = "0x101282104")]
		public void ResetOnQuestLogSet()
		{
		}

		// Token: 0x060026F9 RID: 9977 RVA: 0x000108F0 File Offset: 0x0000EAF0
		[Token(Token = "0x60023F7")]
		[Address(RVA = "0x10128FD28", Offset = "0x128FD28", VA = "0x10128FD28")]
		public bool Request_QuestRetry(Func<MeigewwwParam, Questlogretry, bool> onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026FA RID: 9978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023F8")]
		[Address(RVA = "0x10128FE50", Offset = "0x128FE50", VA = "0x10128FE50")]
		private void OnResponse_QuestRetry(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x060026FB RID: 9979 RVA: 0x00010908 File Offset: 0x0000EB08
		[Token(Token = "0x1700027A")]
		public bool IsRequestingSaveBSD
		{
			[Token(Token = "0x60023F9")]
			[Address(RVA = "0x10128FF70", Offset = "0x128FF70", VA = "0x10128FF70")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060026FC RID: 9980 RVA: 0x00010920 File Offset: 0x0000EB20
		[Token(Token = "0x60023FA")]
		[Address(RVA = "0x10128FF78", Offset = "0x128FF78", VA = "0x10128FF78")]
		public bool Request_SaveBSD(BattleSystemData bsd, int retryCount)
		{
			return default(bool);
		}

		// Token: 0x060026FD RID: 9981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023FB")]
		[Address(RVA = "0x1012900F4", Offset = "0x12900F4", VA = "0x1012900F4")]
		private void OnResponse_SaveBSD(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060026FE RID: 9982 RVA: 0x00010938 File Offset: 0x0000EB38
		[Token(Token = "0x60023FC")]
		[Address(RVA = "0x1012901E0", Offset = "0x12901E0", VA = "0x1012901E0")]
		public bool Request_DeleteBSD(Action onResponse)
		{
			return default(bool);
		}

		// Token: 0x060026FF RID: 9983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023FD")]
		[Address(RVA = "0x1012902D4", Offset = "0x12902D4", VA = "0x1012902D4")]
		private void OnResponse_DeleteBSD(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002700 RID: 9984 RVA: 0x00010950 File Offset: 0x0000EB50
		[Token(Token = "0x60023FE")]
		[Address(RVA = "0x10129036C", Offset = "0x129036C", VA = "0x10129036C")]
		public bool Request_Read(List<int> eventTypes, List<int> groupIDs, Action onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002701 RID: 9985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60023FF")]
		[Address(RVA = "0x101290690", Offset = "0x1290690", VA = "0x101290690")]
		private void OnResponse_Read(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002702 RID: 9986 RVA: 0x00010968 File Offset: 0x0000EB68
		[Token(Token = "0x6002400")]
		[Address(RVA = "0x101290800", Offset = "0x1290800", VA = "0x101290800")]
		public bool Request_PointEventGet(long pointEventId, Action onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002703 RID: 9987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002401")]
		[Address(RVA = "0x101290964", Offset = "0x1290964", VA = "0x101290964")]
		private void OnResponse_PointEventGet(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002704 RID: 9988 RVA: 0x00010980 File Offset: 0x0000EB80
		[Token(Token = "0x6002402")]
		[Address(RVA = "0x101290A7C", Offset = "0x1290A7C", VA = "0x101290A7C")]
		public bool Request_PointEventReward(long pointEventId, Action onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002705 RID: 9989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002403")]
		[Address(RVA = "0x101290BE0", Offset = "0x1290BE0", VA = "0x101290BE0")]
		private void OnResponse_PointEventReward(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002706 RID: 9990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002404")]
		[Address(RVA = "0x10128C244", Offset = "0x128C244", VA = "0x10128C244")]
		public static void wwwConvert(WWWTypes.BattleOccurEnemy src, CommonLogic.BattleOccurEnemy dest)
		{
		}

		// Token: 0x06002707 RID: 9991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002405")]
		[Address(RVA = "0x10128C464", Offset = "0x128C464", VA = "0x10128C464")]
		public static void wwwConvert(WWWTypes.BattleDropItems[] src, List<CommonLogic.BattleDropItems> dest)
		{
		}

		// Token: 0x06002708 RID: 9992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002406")]
		[Address(RVA = "0x101283C84", Offset = "0x1283C84", VA = "0x101283C84")]
		private static void wwwConvert(QuestChapterArray src, QuestManager.ChapterData dest)
		{
		}

		// Token: 0x06002709 RID: 9993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002407")]
		[Address(RVA = "0x10128ABC8", Offset = "0x128ABC8", VA = "0x10128ABC8")]
		public static void wwwConvert(Quest src, ref QuestListDB_Param ref_dest)
		{
		}

		// Token: 0x0600270A RID: 9994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002408")]
		[Address(RVA = "0x1012858FC", Offset = "0x12858FC", VA = "0x1012858FC")]
		private static void wwwConvert(EventQuest src, QuestManager.EventData dest)
		{
		}

		// Token: 0x0600270B RID: 9995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002409")]
		[Address(RVA = "0x10128AF30", Offset = "0x128AF30", VA = "0x10128AF30")]
		private static void wwwConvert(WWWTypes.EventQuestPeriod src, QuestManager.EventQuestPeriod dest)
		{
		}

		// Token: 0x0600270C RID: 9996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600240A")]
		[Address(RVA = "0x101290EBC", Offset = "0x1290EBC", VA = "0x101290EBC")]
		private static void wwwConvert(WWWTypes.EventQuestPeriodGroup src, QuestManager.EventQuestPeriodGroup dest)
		{
		}

		// Token: 0x0600270D RID: 9997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600240B")]
		[Address(RVA = "0x10128B138", Offset = "0x128B138", VA = "0x10128B138")]
		private static void wwwConvert(QuestStaminaReduction src, QuestManager.StaminaReduction dest)
		{
		}

		// Token: 0x0600270E RID: 9998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600240C")]
		[Address(RVA = "0x101291554", Offset = "0x1291554", VA = "0x101291554")]
		public QuestManager()
		{
		}

		// Token: 0x040036C8 RID: 14024
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40027E4")]
		private StringBuilder m_sb;

		// Token: 0x040036C9 RID: 14025
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40027E5")]
		private List<QuestManager.QuestData> m_QuestList;

		// Token: 0x040036CA RID: 14026
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40027E6")]
		private List<QuestManager.ChapterData[]> m_ChapterDatas;

		// Token: 0x040036CB RID: 14027
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40027E7")]
		private Dictionary<int, QuestDefine.eDifficulty> m_LastOpenDifficulty;

		// Token: 0x040036CC RID: 14028
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40027E8")]
		private bool m_IsNewDispChapter;

		// Token: 0x040036CD RID: 14029
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40027E9")]
		public int[] m_LastPlayedChapterQuestIds;

		// Token: 0x040036CE RID: 14030
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40027EA")]
		public int[] m_PlayedOpenChapterIds;

		// Token: 0x040036CF RID: 14031
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40027EB")]
		private List<QuestManager.EventGroupData> m_EventGroupDatas;

		// Token: 0x040036D0 RID: 14032
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40027EC")]
		private List<QuestManager.CharaQuestData> m_CharaQuestDatas;

		// Token: 0x040036D1 RID: 14033
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40027ED")]
		private List<QuestManager.OfferQuestData> m_OfferQuestDatas;

		// Token: 0x040036D2 RID: 14034
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40027EE")]
		public long WorkRecvID;

		// Token: 0x040036D3 RID: 14035
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40027EF")]
		public int WorkQuestID;

		// Token: 0x040036D4 RID: 14036
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40027F0")]
		private long m_WorkBattlePartyMngID;

		// Token: 0x040036D5 RID: 14037
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40027F1")]
		private List<int> m_WorkReadGroupIDs;

		// Token: 0x040036D6 RID: 14038
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40027F2")]
		private long m_WorkPointEventID;

		// Token: 0x040036D7 RID: 14039
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40027F3")]
		public TownBuff WorkTBuff;

		// Token: 0x040036D8 RID: 14040
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40027F4")]
		public CommonLogic.BattleOccurEnemy WorkEnemies;

		// Token: 0x040036D9 RID: 14041
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40027F5")]
		public List<CommonLogic.BattleDropItems> WorkScheduleItems;

		// Token: 0x040036DA RID: 14042
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40027F6")]
		public string SaveResponse;

		// Token: 0x040036DB RID: 14043
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40027F7")]
		private BattleResult m_Result;

		// Token: 0x040036DC RID: 14044
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40027F8")]
		private List<int> m_ReadGroupIDs;

		// Token: 0x040036DD RID: 14045
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40027F9")]
		private List<QuestManager.EventQuestPeriod> m_EventQuestPeriods;

		// Token: 0x040036DE RID: 14046
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40027FA")]
		private List<QuestManager.StaminaReduction> m_StaminaReductions;

		// Token: 0x040036DF RID: 14047
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40027FB")]
		private QuestManager.NoticeData m_NoticeData;

		// Token: 0x040036E0 RID: 14048
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40027FC")]
		private List<QuestManager.PointEventData> m_PointEventDatas;

		// Token: 0x040036E1 RID: 14049
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40027FD")]
		private QuestManager.SelectQuest m_SelectQuest;

		// Token: 0x040036E2 RID: 14050
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40027FE")]
		private DateTime m_GetAllNextTime;

		// Token: 0x040036EC RID: 14060
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002808")]
		private bool m_IsRequestingSaveBSD;

		// Token: 0x040036ED RID: 14061
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4002809")]
		private int m_SaveBSDRetryCount;

		// Token: 0x0200091F RID: 2335
		[Token(Token = "0x2000F49")]
		public class QuestData
		{
			// Token: 0x0600270F RID: 9999 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC0")]
			[Address(RVA = "0x101291A44", Offset = "0x1291A44", VA = "0x101291A44")]
			public QuestData(QuestListDB_Param param, QuestNpc[] npcs, QuestDefine.eClearRank rank, bool isNew, int eventID)
			{
			}

			// Token: 0x06002710 RID: 10000 RVA: 0x00010998 File Offset: 0x0000EB98
			[Token(Token = "0x6005FC1")]
			[Address(RVA = "0x101291AB4", Offset = "0x1291AB4", VA = "0x101291AB4")]
			public bool IsCleared()
			{
				return default(bool);
			}

			// Token: 0x06002711 RID: 10001 RVA: 0x000109B0 File Offset: 0x0000EBB0
			[Token(Token = "0x6005FC2")]
			[Address(RVA = "0x101291AC4", Offset = "0x1291AC4", VA = "0x101291AC4")]
			public bool IsChapterQuest()
			{
				return default(bool);
			}

			// Token: 0x06002712 RID: 10002 RVA: 0x000109C8 File Offset: 0x0000EBC8
			[Token(Token = "0x6005FC3")]
			[Address(RVA = "0x101291AF0", Offset = "0x1291AF0", VA = "0x101291AF0")]
			public bool IsEventQuest()
			{
				return default(bool);
			}

			// Token: 0x040036EE RID: 14062
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006257")]
			public QuestListDB_Param m_Param;

			// Token: 0x040036EF RID: 14063
			[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
			[Token(Token = "0x4006258")]
			public QuestNpc[] m_QuestNpcs;

			// Token: 0x040036F0 RID: 14064
			[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
			[Token(Token = "0x4006259")]
			public QuestDefine.eClearRank m_ClearRank;

			// Token: 0x040036F1 RID: 14065
			[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
			[Token(Token = "0x400625A")]
			public bool m_IsNew;

			// Token: 0x040036F2 RID: 14066
			[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
			[Token(Token = "0x400625B")]
			public int m_EventID;
		}

		// Token: 0x02000920 RID: 2336
		[Token(Token = "0x2000F4A")]
		public class ChapterData
		{
			// Token: 0x06002713 RID: 10003 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC4")]
			[Address(RVA = "0x101291794", Offset = "0x1291794", VA = "0x101291794")]
			public ChapterData()
			{
			}

			// Token: 0x040036F3 RID: 14067
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400625C")]
			public int m_ChapterID;

			// Token: 0x040036F4 RID: 14068
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400625D")]
			public QuestDefine.eDifficulty m_Difficulty;

			// Token: 0x040036F5 RID: 14069
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400625E")]
			public int m_Part;

			// Token: 0x040036F6 RID: 14070
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400625F")]
			public string m_ChapterName;

			// Token: 0x040036F7 RID: 14071
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006260")]
			public int[] m_QuestIDs;

			// Token: 0x040036F8 RID: 14072
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006261")]
			public bool m_IsComingSoon;

			// Token: 0x040036F9 RID: 14073
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4006262")]
			public int m_TrialQuestID;

			// Token: 0x040036FA RID: 14074
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006263")]
			public int m_CollectQuestID;
		}

		// Token: 0x02000921 RID: 2337
		[Token(Token = "0x2000F4B")]
		public class EventGroupData
		{
			// Token: 0x06002714 RID: 10004 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC5")]
			[Address(RVA = "0x10129184C", Offset = "0x129184C", VA = "0x10129184C")]
			public EventGroupData()
			{
			}

			// Token: 0x040036FB RID: 14075
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006264")]
			public int m_EventType;

			// Token: 0x040036FC RID: 14076
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006265")]
			public int m_TradeGroupID;

			// Token: 0x040036FD RID: 14077
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006266")]
			public int m_ChestID;

			// Token: 0x040036FE RID: 14078
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006267")]
			public int m_ChestCostItemID;

			// Token: 0x040036FF RID: 14079
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006268")]
			public int m_ChestCostAmount;

			// Token: 0x04003700 RID: 14080
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006269")]
			public bool m_IsMap;

			// Token: 0x04003701 RID: 14081
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400626A")]
			public int m_GroupID;

			// Token: 0x04003702 RID: 14082
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400626B")]
			public string m_GroupName;

			// Token: 0x04003703 RID: 14083
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400626C")]
			public string m_MultilineGroupName;

			// Token: 0x04003704 RID: 14084
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400626D")]
			public bool m_IsGroupSkip;

			// Token: 0x04003705 RID: 14085
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x400626E")]
			public List<QuestManager.EventData> m_EventDatas;

			// Token: 0x04003706 RID: 14086
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400626F")]
			public bool m_IsOutOfPeriod;
		}

		// Token: 0x02000922 RID: 2338
		[Token(Token = "0x2000F4C")]
		public class EventData
		{
			// Token: 0x06002715 RID: 10005 RVA: 0x000109E0 File Offset: 0x0000EBE0
			[Token(Token = "0x6005FC6")]
			[Address(RVA = "0x1012917A4", Offset = "0x12917A4", VA = "0x1012917A4")]
			public bool ContainCondition(int eventID)
			{
				return default(bool);
			}

			// Token: 0x06002716 RID: 10006 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC7")]
			[Address(RVA = "0x101291844", Offset = "0x1291844", VA = "0x101291844")]
			public EventData()
			{
			}

			// Token: 0x04003707 RID: 14087
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006270")]
			public int m_EventID;

			// Token: 0x04003708 RID: 14088
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006271")]
			public int m_QuestID;

			// Token: 0x04003709 RID: 14089
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006272")]
			public eEventQuestFreqType m_FreqType;

			// Token: 0x0400370A RID: 14090
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006273")]
			public DateTime m_StartAt;

			// Token: 0x0400370B RID: 14091
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006274")]
			public DateTime m_EndAt;

			// Token: 0x0400370C RID: 14092
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006275")]
			public int m_OrderTotal;

			// Token: 0x0400370D RID: 14093
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4006276")]
			public int m_OrderDaily;

			// Token: 0x0400370E RID: 14094
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006277")]
			public int[] m_CondEventQuestIDs;

			// Token: 0x0400370F RID: 14095
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006278")]
			public eTitleType m_ExTitleType;

			// Token: 0x04003710 RID: 14096
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x4006279")]
			public eCharaNamedType m_ExNamedType;

			// Token: 0x04003711 RID: 14097
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x400627A")]
			public eRare m_ExRare;

			// Token: 0x04003712 RID: 14098
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x400627B")]
			public int m_ExCost;

			// Token: 0x04003713 RID: 14099
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400627C")]
			public bool[] m_ExElements;

			// Token: 0x04003714 RID: 14100
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x400627D")]
			public bool[] m_ExClasses;

			// Token: 0x04003715 RID: 14101
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x400627E")]
			public bool m_IsOutOfPeriod;
		}

		// Token: 0x02000923 RID: 2339
		[Token(Token = "0x2000F4D")]
		public class CharaQuestData
		{
			// Token: 0x06002717 RID: 10007 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC8")]
			[Address(RVA = "0x10129179C", Offset = "0x129179C", VA = "0x10129179C")]
			public CharaQuestData()
			{
			}

			// Token: 0x04003716 RID: 14102
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400627F")]
			public int m_QuestID;

			// Token: 0x04003717 RID: 14103
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006280")]
			public int m_EvolutionAfterCharaID;
		}

		// Token: 0x02000924 RID: 2340
		[Token(Token = "0x2000F4E")]
		public class OfferQuestData
		{
			// Token: 0x06002718 RID: 10008 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FC9")]
			[Address(RVA = "0x101291A10", Offset = "0x1291A10", VA = "0x101291A10")]
			public OfferQuestData()
			{
			}

			// Token: 0x04003718 RID: 14104
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006281")]
			public int m_QuestID;

			// Token: 0x04003719 RID: 14105
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006282")]
			public eTitleType m_TitleType;

			// Token: 0x0400371A RID: 14106
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006283")]
			public int m_OfferID;

			// Token: 0x0400371B RID: 14107
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006284")]
			public OfferDefine.eCategory m_Category;

			// Token: 0x0400371C RID: 14108
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006285")]
			public bool m_IsOrder;
		}

		// Token: 0x02000925 RID: 2341
		[Token(Token = "0x2000F4F")]
		public class EventQuestPeriod
		{
			// Token: 0x06002719 RID: 10009 RVA: 0x000109F8 File Offset: 0x0000EBF8
			[Token(Token = "0x6005FCA")]
			[Address(RVA = "0x101291854", Offset = "0x1291854", VA = "0x101291854")]
			public bool IsLossTime()
			{
				return default(bool);
			}

			// Token: 0x0600271A RID: 10010 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FCB")]
			[Address(RVA = "0x101291914", Offset = "0x1291914", VA = "0x101291914")]
			public EventQuestPeriod()
			{
			}

			// Token: 0x0400371D RID: 14109
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006286")]
			public int m_EventType;

			// Token: 0x0400371E RID: 14110
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006287")]
			public DateTime m_StartAt;

			// Token: 0x0400371F RID: 14111
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006288")]
			public DateTime m_EndAt;

			// Token: 0x04003720 RID: 14112
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006289")]
			public DateTime? m_LossTimeAt;

			// Token: 0x04003721 RID: 14113
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400628A")]
			public QuestManager.EventQuestPeriodGroup[] m_EventQuestPeriodGroups;
		}

		// Token: 0x02000926 RID: 2342
		[Token(Token = "0x2000F50")]
		public class EventQuestPeriodGroup
		{
			// Token: 0x0600271B RID: 10011 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FCC")]
			[Address(RVA = "0x10129191C", Offset = "0x129191C", VA = "0x10129191C")]
			public EventQuestPeriodGroup()
			{
			}

			// Token: 0x04003722 RID: 14114
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400628B")]
			public int m_GroupID;

			// Token: 0x04003723 RID: 14115
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400628C")]
			public DateTime m_StartAt;

			// Token: 0x04003724 RID: 14116
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400628D")]
			public DateTime m_EndAt;

			// Token: 0x04003725 RID: 14117
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400628E")]
			public bool m_Unlocked;

			// Token: 0x04003726 RID: 14118
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400628F")]
			public QuestManager.EventQuestPeriodGroupQuest[] m_EventQuestPeriodGroupQuests;
		}

		// Token: 0x02000927 RID: 2343
		[Token(Token = "0x2000F51")]
		public class EventQuestPeriodGroupQuest
		{
			// Token: 0x0600271C RID: 10012 RVA: 0x00010A10 File Offset: 0x0000EC10
			[Token(Token = "0x6005FCD")]
			[Address(RVA = "0x101291924", Offset = "0x1291924", VA = "0x101291924")]
			public bool IsExistCond(int eventQuestID)
			{
				return default(bool);
			}

			// Token: 0x0600271D RID: 10013 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FCE")]
			[Address(RVA = "0x1012919D0", Offset = "0x12919D0", VA = "0x1012919D0")]
			public EventQuestPeriodGroupQuest()
			{
			}

			// Token: 0x04003727 RID: 14119
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006290")]
			public int m_EventQuestID;

			// Token: 0x04003728 RID: 14120
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006291")]
			public int m_QuestID;

			// Token: 0x04003729 RID: 14121
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006292")]
			public int[] m_CondEventQuestIds;

			// Token: 0x0400372A RID: 14122
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006293")]
			public DateTime m_StartAt;

			// Token: 0x0400372B RID: 14123
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006294")]
			public DateTime m_EndAt;

			// Token: 0x0400372C RID: 14124
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006295")]
			public bool m_Unlocked;
		}

		// Token: 0x02000928 RID: 2344
		[Token(Token = "0x2000F52")]
		public class StaminaReduction
		{
			// Token: 0x0600271E RID: 10014 RVA: 0x00010A28 File Offset: 0x0000EC28
			[Token(Token = "0x6005FCF")]
			[Address(RVA = "0x101292128", Offset = "0x1292128", VA = "0x101292128")]
			public bool IsChapterQuest()
			{
				return default(bool);
			}

			// Token: 0x0600271F RID: 10015 RVA: 0x00010A40 File Offset: 0x0000EC40
			[Token(Token = "0x6005FD0")]
			[Address(RVA = "0x101292140", Offset = "0x1292140", VA = "0x101292140")]
			public bool IsEventQuest()
			{
				return default(bool);
			}

			// Token: 0x06002720 RID: 10016 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FD1")]
			[Address(RVA = "0x101292150", Offset = "0x1292150", VA = "0x101292150")]
			public void SetQuestCategory(eQuestCategory category)
			{
			}

			// Token: 0x06002721 RID: 10017 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FD2")]
			[Address(RVA = "0x101292158", Offset = "0x1292158", VA = "0x101292158")]
			public StaminaReduction()
			{
			}

			// Token: 0x0400372D RID: 14125
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006296")]
			private eQuestCategory m_QuestCategory;

			// Token: 0x0400372E RID: 14126
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006297")]
			public int m_TargetID;

			// Token: 0x0400372F RID: 14127
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006298")]
			public QuestDefine.eDifficulty m_Difficulty;

			// Token: 0x04003730 RID: 14128
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006299")]
			public bool m_IsAvailable;

			// Token: 0x04003731 RID: 14129
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400629A")]
			public DateTime m_StartAt;

			// Token: 0x04003732 RID: 14130
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400629B")]
			public DateTime m_EndAt;
		}

		// Token: 0x02000929 RID: 2345
		[Token(Token = "0x2000F53")]
		private class NoticeData
		{
			// Token: 0x06002722 RID: 10018 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FD3")]
			[Address(RVA = "0x1012919D8", Offset = "0x12919D8", VA = "0x1012919D8")]
			public NoticeData(string title, string message)
			{
			}

			// Token: 0x04003733 RID: 14131
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400629C")]
			public string m_Title;

			// Token: 0x04003734 RID: 14132
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400629D")]
			public string m_Message;
		}

		// Token: 0x0200092A RID: 2346
		[Token(Token = "0x2000F54")]
		public class PointEventData
		{
			// Token: 0x06002723 RID: 10019 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FD4")]
			[Address(RVA = "0x101291A18", Offset = "0x1291A18", VA = "0x101291A18")]
			public PointEventData(long pointEventID)
			{
			}

			// Token: 0x04003735 RID: 14133
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400629E")]
			public long m_PointEventID;

			// Token: 0x04003736 RID: 14134
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400629F")]
			public long m_TotalPoint;

			// Token: 0x04003737 RID: 14135
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40062A0")]
			public long m_PlayerTotalPoint;

			// Token: 0x04003738 RID: 14136
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40062A1")]
			public long m_NextRewardNeedTotalPoint;

			// Token: 0x04003739 RID: 14137
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40062A2")]
			public bool m_IsExistNextReward;

			// Token: 0x0400373A RID: 14138
			[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
			[Token(Token = "0x40062A3")]
			public bool m_CanReceiveTotalReward;

			// Token: 0x0400373B RID: 14139
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40062A4")]
			public DateTime? m_ExpiredAt;

			// Token: 0x0400373C RID: 14140
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40062A5")]
			public List<RewardSet> m_GainTotalRewardSets;
		}

		// Token: 0x0200092B RID: 2347
		[Token(Token = "0x2000F55")]
		public class SelectQuest
		{
			// Token: 0x170002A8 RID: 680
			// (get) Token: 0x06002724 RID: 10020 RVA: 0x00010A58 File Offset: 0x0000EC58
			[Token(Token = "0x170006A3")]
			public QuestDefine.eUICategory Category
			{
				[Token(Token = "0x6005FD5")]
				[Address(RVA = "0x101291B00", Offset = "0x1291B00", VA = "0x101291B00")]
				get
				{
					return QuestDefine.eUICategory.None;
				}
			}

			// Token: 0x170002A9 RID: 681
			// (get) Token: 0x06002725 RID: 10021 RVA: 0x00010A70 File Offset: 0x0000EC70
			[Token(Token = "0x170006A4")]
			public int ChapterID
			{
				[Token(Token = "0x6005FD6")]
				[Address(RVA = "0x101291B08", Offset = "0x1291B08", VA = "0x101291B08")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002AA RID: 682
			// (get) Token: 0x06002726 RID: 10022 RVA: 0x00010A88 File Offset: 0x0000EC88
			[Token(Token = "0x170006A5")]
			public int EventType
			{
				[Token(Token = "0x6005FD7")]
				[Address(RVA = "0x101291B10", Offset = "0x1291B10", VA = "0x101291B10")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002AB RID: 683
			// (get) Token: 0x06002727 RID: 10023 RVA: 0x00010AA0 File Offset: 0x0000ECA0
			[Token(Token = "0x170006A6")]
			public int GroupID
			{
				[Token(Token = "0x6005FD8")]
				[Address(RVA = "0x101291B18", Offset = "0x1291B18", VA = "0x101291B18")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002AC RID: 684
			// (get) Token: 0x06002728 RID: 10024 RVA: 0x00010AB8 File Offset: 0x0000ECB8
			[Token(Token = "0x170006A7")]
			public int EventID
			{
				[Token(Token = "0x6005FD9")]
				[Address(RVA = "0x101291B20", Offset = "0x1291B20", VA = "0x101291B20")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002AD RID: 685
			// (get) Token: 0x06002729 RID: 10025 RVA: 0x00010AD0 File Offset: 0x0000ECD0
			[Token(Token = "0x170006A8")]
			public int CharaID
			{
				[Token(Token = "0x6005FDA")]
				[Address(RVA = "0x101291B28", Offset = "0x1291B28", VA = "0x101291B28")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002AE RID: 686
			// (get) Token: 0x0600272A RID: 10026 RVA: 0x00010AE8 File Offset: 0x0000ECE8
			[Token(Token = "0x170006A9")]
			public eTitleType OfferTitleType
			{
				[Token(Token = "0x6005FDB")]
				[Address(RVA = "0x101291B30", Offset = "0x1291B30", VA = "0x101291B30")]
				get
				{
					return eTitleType.Title_0000;
				}
			}

			// Token: 0x170002AF RID: 687
			// (get) Token: 0x0600272B RID: 10027 RVA: 0x00010B00 File Offset: 0x0000ED00
			[Token(Token = "0x170006AA")]
			public int QuestID
			{
				[Token(Token = "0x6005FDC")]
				[Address(RVA = "0x101291B38", Offset = "0x1291B38", VA = "0x101291B38")]
				get
				{
					return 0;
				}
			}

			// Token: 0x170002B0 RID: 688
			// (get) Token: 0x0600272C RID: 10028 RVA: 0x00010B18 File Offset: 0x0000ED18
			[Token(Token = "0x170006AB")]
			public int QuestIDForScrollPosition
			{
				[Token(Token = "0x6005FDD")]
				[Address(RVA = "0x101291B40", Offset = "0x1291B40", VA = "0x101291B40")]
				get
				{
					return 0;
				}
			}

			// Token: 0x0600272D RID: 10029 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FDE")]
			[Address(RVA = "0x101291B48", Offset = "0x1291B48", VA = "0x101291B48")]
			public void SetCategory(QuestDefine.eUICategory value)
			{
			}

			// Token: 0x0600272E RID: 10030 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FDF")]
			[Address(RVA = "0x101291B50", Offset = "0x1291B50", VA = "0x101291B50")]
			public void SetEventType(int value)
			{
			}

			// Token: 0x0600272F RID: 10031 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE0")]
			[Address(RVA = "0x101291B58", Offset = "0x1291B58", VA = "0x101291B58")]
			public void SetGroupID(int value)
			{
			}

			// Token: 0x06002730 RID: 10032 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE1")]
			[Address(RVA = "0x101291B60", Offset = "0x1291B60", VA = "0x101291B60")]
			public void SetEventID(int value)
			{
			}

			// Token: 0x06002731 RID: 10033 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE2")]
			[Address(RVA = "0x101291B68", Offset = "0x1291B68", VA = "0x101291B68")]
			public void SetCharaID(int value)
			{
			}

			// Token: 0x06002732 RID: 10034 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE3")]
			[Address(RVA = "0x101291B70", Offset = "0x1291B70", VA = "0x101291B70")]
			public void SetOfferTitleType(eTitleType titleType)
			{
			}

			// Token: 0x06002733 RID: 10035 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE4")]
			[Address(RVA = "0x101291B78", Offset = "0x1291B78", VA = "0x101291B78")]
			public void SetQuestID(int value)
			{
			}

			// Token: 0x06002734 RID: 10036 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE5")]
			[Address(RVA = "0x101291B80", Offset = "0x1291B80", VA = "0x101291B80")]
			public void SetQuestIDForScrollPosition(int value)
			{
			}

			// Token: 0x06002735 RID: 10037 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE6")]
			[Address(RVA = "0x101291B88", Offset = "0x1291B88", VA = "0x101291B88")]
			public SelectQuest()
			{
			}

			// Token: 0x06002736 RID: 10038 RVA: 0x00010B30 File Offset: 0x0000ED30
			[Token(Token = "0x6005FE7")]
			[Address(RVA = "0x101291BD0", Offset = "0x1291BD0", VA = "0x101291BD0")]
			public bool IsInvalid()
			{
				return default(bool);
			}

			// Token: 0x06002737 RID: 10039 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE8")]
			[Address(RVA = "0x101291BBC", Offset = "0x1291BBC", VA = "0x101291BBC")]
			public void Reset()
			{
			}

			// Token: 0x06002738 RID: 10040 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FE9")]
			[Address(RVA = "0x101291C00", Offset = "0x1291C00", VA = "0x101291C00")]
			public void SetupChapter(int chapterID, int questID)
			{
			}

			// Token: 0x06002739 RID: 10041 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FEA")]
			[Address(RVA = "0x101291C20", Offset = "0x1291C20", VA = "0x101291C20")]
			public void SetupEvent(int groupID, int eventID, int questID, int eventType)
			{
			}

			// Token: 0x0600273A RID: 10042 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FEB")]
			[Address(RVA = "0x101291C44", Offset = "0x1291C44", VA = "0x101291C44")]
			public void SetupCharacter(int charaID, int questID)
			{
			}

			// Token: 0x0600273B RID: 10043 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FEC")]
			[Address(RVA = "0x101291C68", Offset = "0x1291C68", VA = "0x101291C68")]
			public void SetupOffer(eTitleType titleType, int questID)
			{
			}

			// Token: 0x0600273C RID: 10044 RVA: 0x00010B48 File Offset: 0x0000ED48
			[Token(Token = "0x6005FED")]
			[Address(RVA = "0x101291C88", Offset = "0x1291C88", VA = "0x101291C88")]
			public bool IsExistDropExt()
			{
				return default(bool);
			}

			// Token: 0x0600273D RID: 10045 RVA: 0x00010B60 File Offset: 0x0000ED60
			[Token(Token = "0x6005FEE")]
			[Address(RVA = "0x101291D40", Offset = "0x1291D40", VA = "0x101291D40")]
			public bool IsExistUISetting()
			{
				return default(bool);
			}

			// Token: 0x0600273E RID: 10046 RVA: 0x00010B78 File Offset: 0x0000ED78
			[Token(Token = "0x6005FEF")]
			[Address(RVA = "0x101291F40", Offset = "0x1291F40", VA = "0x101291F40")]
			public EventQuestUISettingDB_Param? GetUISetting(bool useGroup)
			{
				return null;
			}

			// Token: 0x0400373D RID: 14141
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40062A6")]
			private QuestDefine.eUICategory m_Category;

			// Token: 0x0400373E RID: 14142
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40062A7")]
			private int m_ChapterID;

			// Token: 0x0400373F RID: 14143
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40062A8")]
			private int m_EventType;

			// Token: 0x04003740 RID: 14144
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40062A9")]
			private int m_GroupID;

			// Token: 0x04003741 RID: 14145
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40062AA")]
			private int m_EventID;

			// Token: 0x04003742 RID: 14146
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40062AB")]
			private int m_CharaID;

			// Token: 0x04003743 RID: 14147
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40062AC")]
			private eTitleType m_OfferTitleType;

			// Token: 0x04003744 RID: 14148
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40062AD")]
			private int m_QuestID;

			// Token: 0x04003745 RID: 14149
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40062AE")]
			private int m_QuestIDForScrollPosition;
		}
	}
}
