﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200092D RID: 2349
	[Token(Token = "0x20006A6")]
	[StructLayout(3)]
	public class QuestMapController
	{
		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06002741 RID: 10049 RVA: 0x00010B90 File Offset: 0x0000ED90
		[Token(Token = "0x1700027B")]
		private float OrthographicSizeMin
		{
			[Token(Token = "0x600240D")]
			[Address(RVA = "0x101292160", Offset = "0x1292160", VA = "0x101292160")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06002742 RID: 10050 RVA: 0x00010BA8 File Offset: 0x0000EDA8
		[Token(Token = "0x1700027C")]
		private float OrthographicSizeMax
		{
			[Token(Token = "0x600240E")]
			[Address(RVA = "0x10129216C", Offset = "0x129216C", VA = "0x10129216C")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06002743 RID: 10051 RVA: 0x00010BC0 File Offset: 0x0000EDC0
		[Token(Token = "0x1700027D")]
		private bool EnableCamera
		{
			[Token(Token = "0x600240F")]
			[Address(RVA = "0x101292174", Offset = "0x1292174", VA = "0x101292174")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06002744 RID: 10052 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700027E")]
		public Camera Camera
		{
			[Token(Token = "0x6002410")]
			[Address(RVA = "0x101292230", Offset = "0x1292230", VA = "0x101292230")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002745 RID: 10053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002411")]
		[Address(RVA = "0x101292238", Offset = "0x1292238", VA = "0x101292238")]
		public void Update()
		{
		}

		// Token: 0x06002746 RID: 10054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002412")]
		[Address(RVA = "0x101292C14", Offset = "0x1292C14", VA = "0x101292C14")]
		public void Destroy()
		{
		}

		// Token: 0x06002747 RID: 10055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002413")]
		[Address(RVA = "0x101292C1C", Offset = "0x1292C1C", VA = "0x101292C1C")]
		public void SetOwner(QuestMapHandler owner)
		{
		}

		// Token: 0x06002748 RID: 10056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002414")]
		[Address(RVA = "0x101292CD0", Offset = "0x1292CD0", VA = "0x101292CD0")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x06002749 RID: 10057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002415")]
		[Address(RVA = "0x101292C24", Offset = "0x1292C24", VA = "0x101292C24")]
		public void DetachCamera()
		{
		}

		// Token: 0x0600274A RID: 10058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002416")]
		[Address(RVA = "0x101292FF0", Offset = "0x1292FF0", VA = "0x101292FF0")]
		public void SetIsControllable(bool flg)
		{
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002417")]
		[Address(RVA = "0x10129275C", Offset = "0x129275C", VA = "0x10129275C")]
		private void ResetOnControllableDisable()
		{
		}

		// Token: 0x0600274C RID: 10060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002418")]
		[Address(RVA = "0x1012927CC", Offset = "0x12927CC", VA = "0x1012927CC")]
		public void SetZoom(float size, bool isForce)
		{
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002419")]
		[Address(RVA = "0x1012928E0", Offset = "0x12928E0", VA = "0x1012928E0")]
		private void CorrectCameraTransform()
		{
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x00010BD8 File Offset: 0x0000EDD8
		[Token(Token = "0x600241A")]
		[Address(RVA = "0x101292F8C", Offset = "0x1292F8C", VA = "0x101292F8C")]
		private Rect CalcMoveRange(float orthographicSize)
		{
			return default(Rect);
		}

		// Token: 0x0600274F RID: 10063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600241B")]
		[Address(RVA = "0x101293000", Offset = "0x1293000", VA = "0x101293000")]
		public QuestMapController()
		{
		}

		// Token: 0x04003747 RID: 14151
		[Token(Token = "0x400280A")]
		private const float BASE_W = 1920f;

		// Token: 0x04003748 RID: 14152
		[Token(Token = "0x400280B")]
		private const float BASE_H = 1080f;

		// Token: 0x04003749 RID: 14153
		[Token(Token = "0x400280C")]
		private const float BASE_PIXEL_PER_UNIT = 100f;

		// Token: 0x0400374A RID: 14154
		[Token(Token = "0x400280D")]
		private const float ORTHOGRAPHICSIZE_MIN = 3.2f;

		// Token: 0x0400374B RID: 14155
		[Token(Token = "0x400280E")]
		private const float INERTIA = 0.2f;

		// Token: 0x0400374C RID: 14156
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400280F")]
		private QuestMapHandler m_Owner;

		// Token: 0x0400374D RID: 14157
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002810")]
		private Camera m_Camera;

		// Token: 0x0400374E RID: 14158
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002811")]
		private Transform m_CameraTransform;

		// Token: 0x0400374F RID: 14159
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002812")]
		private bool m_IsControllable;

		// Token: 0x04003750 RID: 14160
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4002813")]
		private bool m_WasPincheStarted;

		// Token: 0x04003751 RID: 14161
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002814")]
		private float m_OrthographicSizeDefault;

		// Token: 0x04003752 RID: 14162
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002815")]
		private float m_SizeOnStartPinch;

		// Token: 0x04003753 RID: 14163
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002816")]
		private Vector2 m_vCameraMove;

		// Token: 0x04003754 RID: 14164
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002817")]
		private Rect m_MoveRange;

		// Token: 0x04003755 RID: 14165
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002818")]
		private PinchEvent m_PinchEvent;

		// Token: 0x04003756 RID: 14166
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002819")]
		private int m_SavedCameraCullingMask;

		// Token: 0x04003757 RID: 14167
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400281A")]
		private float m_RangeW;

		// Token: 0x04003758 RID: 14168
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400281B")]
		private float m_RangeH;

		// Token: 0x04003759 RID: 14169
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400281C")]
		private float m_Aspect;
	}
}
