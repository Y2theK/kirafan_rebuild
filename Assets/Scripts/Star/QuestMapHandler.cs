﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;
using UnityEngine;

namespace Star
{
	// Token: 0x0200092E RID: 2350
	[Token(Token = "0x20006A7")]
	[StructLayout(3)]
	public class QuestMapHandler : MonoBehaviour
	{
		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06002750 RID: 10064 RVA: 0x00010BF0 File Offset: 0x0000EDF0
		[Token(Token = "0x1700027F")]
		public int MapID
		{
			[Token(Token = "0x600241C")]
			[Address(RVA = "0x1012930AC", Offset = "0x12930AC", VA = "0x1012930AC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06002751 RID: 10065 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000280")]
		public QuestMapController Controller
		{
			[Token(Token = "0x600241D")]
			[Address(RVA = "0x1012930B4", Offset = "0x12930B4", VA = "0x1012930B4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06002752 RID: 10066 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000281")]
		public List<int> OpenedIDs
		{
			[Token(Token = "0x600241E")]
			[Address(RVA = "0x1012930BC", Offset = "0x12930BC", VA = "0x1012930BC")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002753 RID: 10067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600241F")]
		[Address(RVA = "0x1012930C4", Offset = "0x12930C4", VA = "0x1012930C4")]
		private void Update()
		{
		}

		// Token: 0x06002754 RID: 10068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002420")]
		[Address(RVA = "0x101293828", Offset = "0x1293828", VA = "0x101293828")]
		private void OnDestroy()
		{
		}

		// Token: 0x06002755 RID: 10069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002421")]
		[Address(RVA = "0x101293C24", Offset = "0x1293C24", VA = "0x101293C24")]
		public void Setup()
		{
		}

		// Token: 0x06002756 RID: 10070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002422")]
		[Address(RVA = "0x10129382C", Offset = "0x129382C", VA = "0x10129382C")]
		public void Destroy()
		{
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002423")]
		[Address(RVA = "0x101293C80", Offset = "0x1293C80", VA = "0x101293C80")]
		public void AttachCamera(Camera camera)
		{
		}

		// Token: 0x06002758 RID: 10072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002424")]
		[Address(RVA = "0x101293C54", Offset = "0x1293C54", VA = "0x101293C54")]
		public void DetachCamera()
		{
		}

		// Token: 0x06002759 RID: 10073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002425")]
		[Address(RVA = "0x101293CB4", Offset = "0x1293CB4", VA = "0x101293CB4")]
		public void AddPoint(QuestMapHandler.ePointState state, int id, int locatorID, int mapObjID, float scale, float mapObjOffsetX, float mapObjOffsetY, float col_radius, Vector3 col_center, GameObject uiPrefab, int ui_badgeNum, bool ui_isNew, bool ui_isCurrent, string ui_name, float ui_nameOffsetX, float ui_nameOffsetY)
		{
		}

		// Token: 0x0600275A RID: 10074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002426")]
		[Address(RVA = "0x1012948C4", Offset = "0x12948C4", VA = "0x1012948C4")]
		public void SetPointInfo(int index, int ui_badgeNum, bool ui_isNew, bool ui_isCurrent)
		{
		}

		// Token: 0x0600275B RID: 10075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002427")]
		[Address(RVA = "0x101294AA4", Offset = "0x1294AA4", VA = "0x101294AA4")]
		public void SetPointOffset(int index, float offsetX, float offsetY)
		{
		}

		// Token: 0x0600275C RID: 10076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002428")]
		[Address(RVA = "0x101294C14", Offset = "0x1294C14", VA = "0x101294C14")]
		public void SetPointCollision(int index, float radius, float centerX, float centerY)
		{
		}

		// Token: 0x0600275D RID: 10077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002429")]
		[Address(RVA = "0x101294DF4", Offset = "0x1294DF4", VA = "0x101294DF4")]
		public void SetNameBarOffset(int index, float offsetX, float offsetY)
		{
		}

		// Token: 0x0600275E RID: 10078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600242A")]
		[Address(RVA = "0x101294F00", Offset = "0x1294F00", VA = "0x101294F00")]
		public void AddSD(GameObject sdPrefab, int charaID, int locatorID, float scale, float offsetX, float offsetY)
		{
		}

		// Token: 0x0600275F RID: 10079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600242B")]
		[Address(RVA = "0x10129526C", Offset = "0x129526C", VA = "0x10129526C")]
		public void AddSD(GameObject sdPrefab, int charaID, int locatorID, float offsetX, float offsetY)
		{
		}

		// Token: 0x06002760 RID: 10080 RVA: 0x00010C08 File Offset: 0x0000EE08
		[Token(Token = "0x600242C")]
		[Address(RVA = "0x101295280", Offset = "0x1295280", VA = "0x101295280")]
		public static Vector2 GetScreenToOrthoRate()
		{
			return default(Vector2);
		}

		// Token: 0x06002761 RID: 10081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600242D")]
		[Address(RVA = "0x101295350", Offset = "0x1295350", VA = "0x101295350")]
		public void SetSDOffset(int index, float offsetX, float offsetY)
		{
		}

		// Token: 0x06002762 RID: 10082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600242E")]
		[Address(RVA = "0x1012954D4", Offset = "0x12954D4", VA = "0x1012954D4")]
		public void SetSDLocator(int index, int locatorID)
		{
		}

		// Token: 0x06002763 RID: 10083 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600242F")]
		[Address(RVA = "0x1012955E0", Offset = "0x12955E0", VA = "0x1012955E0")]
		public QuestMapHandler.PointData GetPointDataFromID(int id)
		{
			return null;
		}

		// Token: 0x06002764 RID: 10084 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002430")]
		[Address(RVA = "0x1012956E0", Offset = "0x12956E0", VA = "0x1012956E0")]
		public QuestMapHandler.PointData GetPointDataFromLocatorID(int locatorID)
		{
			return null;
		}

		// Token: 0x06002765 RID: 10085 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002431")]
		[Address(RVA = "0x1012957E0", Offset = "0x12957E0", VA = "0x1012957E0")]
		public QuestMapHandler.PointData GetPointDataFromQuestMapObjHndl(QuestMapObjHandler qmoHndl)
		{
			return null;
		}

		// Token: 0x06002766 RID: 10086 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002432")]
		[Address(RVA = "0x1012946D0", Offset = "0x12946D0", VA = "0x1012946D0")]
		public Transform GetLocator(int locatorID)
		{
			return null;
		}

		// Token: 0x06002767 RID: 10087 RVA: 0x00010C20 File Offset: 0x0000EE20
		[Token(Token = "0x6002433")]
		[Address(RVA = "0x10129460C", Offset = "0x129460C", VA = "0x10129460C")]
		private int GetMapObjectPrefabIndexFromMapObjID(int mapObjID)
		{
			return 0;
		}

		// Token: 0x06002768 RID: 10088 RVA: 0x00010C38 File Offset: 0x0000EE38
		[Token(Token = "0x6002434")]
		[Address(RVA = "0x10129361C", Offset = "0x129361C", VA = "0x10129361C")]
		private int CalcNextOpenPointIndex(int currentIndex)
		{
			return 0;
		}

		// Token: 0x06002769 RID: 10089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002435")]
		[Address(RVA = "0x101295918", Offset = "0x1295918", VA = "0x101295918")]
		public void SetTapMapObjFunc(Action<QuestMapHandler.PointData> onTapFunc)
		{
		}

		// Token: 0x0600276A RID: 10090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002436")]
		[Address(RVA = "0x101295920", Offset = "0x1295920", VA = "0x101295920")]
		private void OnTapFunc(QuestMapObjHandler qmoHndl)
		{
		}

		// Token: 0x0600276B RID: 10091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002437")]
		[Address(RVA = "0x101295AA4", Offset = "0x1295AA4", VA = "0x101295AA4")]
		public void PlayIn()
		{
		}

		// Token: 0x0600276C RID: 10092 RVA: 0x00010C50 File Offset: 0x0000EE50
		[Token(Token = "0x6002438")]
		[Address(RVA = "0x101295B9C", Offset = "0x1295B9C", VA = "0x101295B9C")]
		public bool IsPlayIn()
		{
			return default(bool);
		}

		// Token: 0x0600276D RID: 10093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002439")]
		[Address(RVA = "0x101295BB0", Offset = "0x1295BB0", VA = "0x101295BB0")]
		public void PlayOut()
		{
		}

		// Token: 0x0600276E RID: 10094 RVA: 0x00010C68 File Offset: 0x0000EE68
		[Token(Token = "0x600243A")]
		[Address(RVA = "0x101295BBC", Offset = "0x1295BBC", VA = "0x101295BBC")]
		public bool IsPlayOut()
		{
			return default(bool);
		}

		// Token: 0x0600276F RID: 10095 RVA: 0x00010C80 File Offset: 0x0000EE80
		[Token(Token = "0x600243B")]
		[Address(RVA = "0x101295BCC", Offset = "0x1295BCC", VA = "0x101295BCC")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06002770 RID: 10096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600243C")]
		[Address(RVA = "0x101295BDC", Offset = "0x1295BDC", VA = "0x101295BDC")]
		public void ShowUI()
		{
		}

		// Token: 0x06002771 RID: 10097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600243D")]
		[Address(RVA = "0x101295CB4", Offset = "0x1295CB4", VA = "0x101295CB4")]
		public void HideUI()
		{
		}

		// Token: 0x06002772 RID: 10098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600243E")]
		[Address(RVA = "0x101295D8C", Offset = "0x1295D8C", VA = "0x101295D8C")]
		public void SetIsControllable(bool flg)
		{
		}

		// Token: 0x06002773 RID: 10099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600243F")]
		[Address(RVA = "0x101295DDC", Offset = "0x1295DDC", VA = "0x101295DDC")]
		public void SetIsEnableInputQuestMapObjs(bool flg)
		{
		}

		// Token: 0x06002774 RID: 10100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002440")]
		[Address(RVA = "0x101292B40", Offset = "0x1292B40", VA = "0x101292B40")]
		public void ResetHoldQuestMapObjs()
		{
		}

		// Token: 0x06002775 RID: 10101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002441")]
		[Address(RVA = "0x101295EC4", Offset = "0x1295EC4", VA = "0x101295EC4")]
		public QuestMapHandler()
		{
		}

		// Token: 0x0400375A RID: 14170
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400281D")]
		public int m_ID;

		// Token: 0x0400375B RID: 14171
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400281E")]
		public Transform m_BaseTransform;

		// Token: 0x0400375C RID: 14172
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400281F")]
		public MsbHandler m_MsbHndl;

		// Token: 0x0400375D RID: 14173
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002820")]
		public GameObject[] m_QuestMapObjPrefabs;

		// Token: 0x0400375E RID: 14174
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002821")]
		public QuestMapObjHandler[] m_QuestMapObjHndls;

		// Token: 0x0400375F RID: 14175
		[Token(Token = "0x4002822")]
		private const string LocatorPathFormt = "MeshRoot_QuestMap_{0}/QuestMap_{1}(Clone)/loc_{2}";

		// Token: 0x04003760 RID: 14176
		[Token(Token = "0x4002823")]
		public const string APPEAR_EFFECT_ID = "ef_qm_appear";

		// Token: 0x04003761 RID: 14177
		[Token(Token = "0x4002824")]
		private const float APPEAR_EFFECT_SCALE = 0.7f;

		// Token: 0x04003762 RID: 14178
		[Token(Token = "0x4002825")]
		private const float SD_LOCALPOS_OFFSET_Z = 30f;

		// Token: 0x04003763 RID: 14179
		[Token(Token = "0x4002826")]
		private const float APPEAR_EFFECT_LOCALPOS_OFFSET_Z = 40f;

		// Token: 0x04003764 RID: 14180
		[Token(Token = "0x4002827")]
		private const float UI_LOCALPOS_OFFSET_Z = 50f;

		// Token: 0x04003765 RID: 14181
		[Token(Token = "0x4002828")]
		private const float UI_LOCALPOS_OFFSET_Z_SCALE = 0.01f;

		// Token: 0x04003766 RID: 14182
		[Token(Token = "0x4002829")]
		private const float UI_LOCALPOS_OFFSET_OFFSETZ = 0.6f;

		// Token: 0x04003767 RID: 14183
		[Token(Token = "0x400282A")]
		private const float OPENEFFECT_INTERVAL_SEC = 0.3f;

		// Token: 0x04003768 RID: 14184
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400282B")]
		private List<QuestMapHandler.PointData> m_PointDatas;

		// Token: 0x04003769 RID: 14185
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400282C")]
		private List<QuestMapHandler.SdData> m_SdDatas;

		// Token: 0x0400376A RID: 14186
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400282D")]
		private SimpleTimer m_Timer;

		// Token: 0x0400376B RID: 14187
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400282E")]
		private QuestMapController m_Controller;

		// Token: 0x0400376C RID: 14188
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400282F")]
		private int m_OpenIndex;

		// Token: 0x0400376D RID: 14189
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002830")]
		private List<int> m_OpenedIDs;

		// Token: 0x0400376E RID: 14190
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002831")]
		private QuestMapHandler.eStep m_Step;

		// Token: 0x0400376F RID: 14191
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002832")]
		private EffectHandler m_AppearEffect;

		// Token: 0x04003770 RID: 14192
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002833")]
		private Action<QuestMapHandler.PointData> OnTapMapObject;

		// Token: 0x0200092F RID: 2351
		[Token(Token = "0x2000F57")]
		public enum eStep
		{
			// Token: 0x04003772 RID: 14194
			[Token(Token = "0x40062B1")]
			None = -1,
			// Token: 0x04003773 RID: 14195
			[Token(Token = "0x40062B2")]
			Idle,
			// Token: 0x04003774 RID: 14196
			[Token(Token = "0x40062B3")]
			In,
			// Token: 0x04003775 RID: 14197
			[Token(Token = "0x40062B4")]
			In_OpenEffect_0,
			// Token: 0x04003776 RID: 14198
			[Token(Token = "0x40062B5")]
			In_OpenEffect_1,
			// Token: 0x04003777 RID: 14199
			[Token(Token = "0x40062B6")]
			In_OpenEffect_2,
			// Token: 0x04003778 RID: 14200
			[Token(Token = "0x40062B7")]
			Out
		}

		// Token: 0x02000930 RID: 2352
		[Token(Token = "0x2000F58")]
		public enum ePointState
		{
			// Token: 0x0400377A RID: 14202
			[Token(Token = "0x40062B9")]
			Closed,
			// Token: 0x0400377B RID: 14203
			[Token(Token = "0x40062BA")]
			Opened,
			// Token: 0x0400377C RID: 14204
			[Token(Token = "0x40062BB")]
			ReserveOpen
		}

		// Token: 0x02000931 RID: 2353
		[Token(Token = "0x2000F59")]
		public class PointData
		{
			// Token: 0x06002776 RID: 10102 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FF2")]
			[Address(RVA = "0x1012946C0", Offset = "0x12946C0", VA = "0x1012946C0")]
			public PointData()
			{
			}

			// Token: 0x0400377D RID: 14205
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40062BC")]
			public QuestMapHandler.ePointState m_State;

			// Token: 0x0400377E RID: 14206
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40062BD")]
			public int m_ID;

			// Token: 0x0400377F RID: 14207
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40062BE")]
			public int m_LocatorID;

			// Token: 0x04003780 RID: 14208
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40062BF")]
			public Transform m_Locator;

			// Token: 0x04003781 RID: 14209
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40062C0")]
			public GameObject m_QuestMapObj;

			// Token: 0x04003782 RID: 14210
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40062C1")]
			public QuestMapObjHandler m_QuestMapObjHndl;

			// Token: 0x04003783 RID: 14211
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40062C2")]
			public GameObject m_uiObj;

			// Token: 0x04003784 RID: 14212
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40062C3")]
			public QuestLandMarkUI m_ui;
		}

		// Token: 0x02000932 RID: 2354
		[Token(Token = "0x2000F5A")]
		public class SdData
		{
			// Token: 0x06002777 RID: 10103 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FF3")]
			[Address(RVA = "0x101295264", Offset = "0x1295264", VA = "0x101295264")]
			public SdData()
			{
			}

			// Token: 0x04003785 RID: 14213
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40062C4")]
			public GameObject m_obj;

			// Token: 0x04003786 RID: 14214
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40062C5")]
			public QuestMapSDChara m_ui;
		}
	}
}
