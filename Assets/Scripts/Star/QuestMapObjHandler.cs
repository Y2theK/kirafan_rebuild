﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Star
{
	// Token: 0x02000933 RID: 2355
	[Token(Token = "0x20006A8")]
	[StructLayout(3)]
	public class QuestMapObjHandler : MonoBehaviour, IPointerDownHandler, IEventSystemHandler, IPointerClickHandler, IPointerExitHandler
	{
		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06002778 RID: 10104 RVA: 0x00010C98 File Offset: 0x0000EE98
		[Token(Token = "0x17000282")]
		public QuestMapObjHandler.eAnimType CurrentAnimType
		{
			[Token(Token = "0x6002442")]
			[Address(RVA = "0x101293820", Offset = "0x1293820", VA = "0x101293820")]
			get
			{
				return QuestMapObjHandler.eAnimType.Idle;
			}
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06002779 RID: 10105 RVA: 0x00010CB0 File Offset: 0x0000EEB0
		// (set) Token: 0x0600277A RID: 10106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000283")]
		public bool IsEnableInput
		{
			[Token(Token = "0x6002443")]
			[Address(RVA = "0x101295FCC", Offset = "0x1295FCC", VA = "0x101295FCC")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002444")]
			[Address(RVA = "0x101295EB0", Offset = "0x1295EB0", VA = "0x101295EB0")]
			set
			{
			}
		}

		// Token: 0x1400003D RID: 61
		// (add) Token: 0x0600277B RID: 10107 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600277C RID: 10108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003D")]
		public event Action<QuestMapObjHandler> OnTap
		{
			[Token(Token = "0x6002445")]
			[Address(RVA = "0x1012947B8", Offset = "0x12947B8", VA = "0x1012947B8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002446")]
			[Address(RVA = "0x101295FD4", Offset = "0x1295FD4", VA = "0x101295FD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600277D RID: 10109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002447")]
		[Address(RVA = "0x1012960E0", Offset = "0x12960E0", VA = "0x1012960E0")]
		private void Awake()
		{
		}

		// Token: 0x0600277E RID: 10110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002448")]
		[Address(RVA = "0x101296284", Offset = "0x1296284", VA = "0x101296284")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600277F RID: 10111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002449")]
		[Address(RVA = "0x10129628C", Offset = "0x129628C", VA = "0x10129628C")]
		private void Update()
		{
		}

		// Token: 0x06002780 RID: 10112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244A")]
		[Address(RVA = "0x1012963EC", Offset = "0x12963EC", VA = "0x1012963EC")]
		private void OnEnable()
		{
		}

		// Token: 0x06002781 RID: 10113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244B")]
		[Address(RVA = "0x1012963F8", Offset = "0x12963F8", VA = "0x1012963F8", Slot = "4")]
		public void OnPointerDown(PointerEventData eventData)
		{
		}

		// Token: 0x06002782 RID: 10114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244C")]
		[Address(RVA = "0x101296438", Offset = "0x1296438", VA = "0x101296438", Slot = "5")]
		public void OnPointerClick(PointerEventData eventData)
		{
		}

		// Token: 0x06002783 RID: 10115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244D")]
		[Address(RVA = "0x101296528", Offset = "0x1296528", VA = "0x101296528", Slot = "6")]
		public void OnPointerExit(PointerEventData eventData)
		{
		}

		// Token: 0x06002784 RID: 10116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244E")]
		[Address(RVA = "0x101295EB8", Offset = "0x1295EB8", VA = "0x101295EB8")]
		public void ResetHold()
		{
		}

		// Token: 0x06002785 RID: 10117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600244F")]
		[Address(RVA = "0x1012936F0", Offset = "0x12936F0", VA = "0x1012936F0")]
		public void Play(QuestMapObjHandler.eAnimType animType)
		{
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x00010CC8 File Offset: 0x0000EEC8
		[Token(Token = "0x6002450")]
		[Address(RVA = "0x101296310", Offset = "0x1296310", VA = "0x101296310")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002451")]
		[Address(RVA = "0x101296534", Offset = "0x1296534", VA = "0x101296534")]
		private string GetAnimPlayKey(QuestMapObjHandler.eAnimType animType)
		{
			return null;
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002452")]
		[Address(RVA = "0x1012936E4", Offset = "0x12936E4", VA = "0x1012936E4")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002453")]
		[Address(RVA = "0x101296584", Offset = "0x1296584", VA = "0x101296584")]
		public void EnableRender()
		{
		}

		// Token: 0x0600278A RID: 10122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002454")]
		[Address(RVA = "0x1012965CC", Offset = "0x12965CC", VA = "0x1012965CC")]
		public void DisableRender()
		{
		}

		// Token: 0x0600278B RID: 10123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002455")]
		[Address(RVA = "0x10129676C", Offset = "0x129676C", VA = "0x10129676C")]
		public QuestMapObjHandler()
		{
		}

		// Token: 0x04003787 RID: 14215
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002834")]
		private readonly string[] PLAY_KEYS;

		// Token: 0x04003788 RID: 14216
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002835")]
		public int m_ID;

		// Token: 0x04003789 RID: 14217
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002836")]
		public Transform m_Transform;

		// Token: 0x0400378A RID: 14218
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002837")]
		public Animation m_Anim;

		// Token: 0x0400378B RID: 14219
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002838")]
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400378C RID: 14220
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002839")]
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x0400378D RID: 14221
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400283A")]
		public MsbHandler m_MsbHndl;

		// Token: 0x0400378E RID: 14222
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400283B")]
		private QuestMapObjHandler.eAnimType m_CurrentAnimType;

		// Token: 0x0400378F RID: 14223
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400283C")]
		private bool m_IsHold;

		// Token: 0x04003790 RID: 14224
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400283D")]
		private float m_HoldCount;

		// Token: 0x04003791 RID: 14225
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400283E")]
		private bool m_IsEnableInput;

		// Token: 0x02000934 RID: 2356
		[Token(Token = "0x2000F5B")]
		public enum eAnimType
		{
			// Token: 0x04003794 RID: 14228
			[Token(Token = "0x40062C7")]
			None = -1,
			// Token: 0x04003795 RID: 14229
			[Token(Token = "0x40062C8")]
			Idle,
			// Token: 0x04003796 RID: 14230
			[Token(Token = "0x40062C9")]
			Appear,
			// Token: 0x04003797 RID: 14231
			[Token(Token = "0x40062CA")]
			Num
		}
	}
}
