﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200057F RID: 1407
	[Token(Token = "0x2000472")]
	[StructLayout(3)]
	public class QuestMapResourceListDB : ScriptableObject
	{
		// Token: 0x060015E7 RID: 5607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001497")]
		[Address(RVA = "0x10129688C", Offset = "0x129688C", VA = "0x10129688C")]
		public QuestMapResourceListDB()
		{
		}

		// Token: 0x040019FD RID: 6653
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001367")]
		public QuestMapResourceListDB_Param[] m_Params;
	}
}
