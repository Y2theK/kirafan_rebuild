﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200057D RID: 1405
	[Token(Token = "0x2000470")]
	[Serializable]
	[StructLayout(0, Size = 44)]
	public struct QuestMapResourceListDB_Data
	{
		// Token: 0x040019F0 RID: 6640
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400135A")]
		public int m_RefID;

		// Token: 0x040019F1 RID: 6641
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400135B")]
		public int m_LocatorID;

		// Token: 0x040019F2 RID: 6642
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400135C")]
		public int m_MapObjectID;

		// Token: 0x040019F3 RID: 6643
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x400135D")]
		public float m_ColRadius;

		// Token: 0x040019F4 RID: 6644
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400135E")]
		public float m_ColOffsetX;

		// Token: 0x040019F5 RID: 6645
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400135F")]
		public float m_ColOffsetY;

		// Token: 0x040019F6 RID: 6646
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001360")]
		public float m_MapObjectScale;

		// Token: 0x040019F7 RID: 6647
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001361")]
		public float m_MapObjectOffsetX;

		// Token: 0x040019F8 RID: 6648
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001362")]
		public float m_MapObjectOffsetY;

		// Token: 0x040019F9 RID: 6649
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001363")]
		public float m_UINameOffsetX;

		// Token: 0x040019FA RID: 6650
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001364")]
		public float m_UINameOffsetY;
	}
}
