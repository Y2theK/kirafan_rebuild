﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000520 RID: 1312
	[Token(Token = "0x2000416")]
	[StructLayout(3)]
	public static class QuestMapResourceListDB_Ext
	{
		// Token: 0x0600157D RID: 5501 RVA: 0x00009540 File Offset: 0x00007740
		[Token(Token = "0x6001432")]
		[Address(RVA = "0x101296894", Offset = "0x1296894", VA = "0x101296894")]
		public static QuestMapResourceListDB_Param GetParam(this QuestMapResourceListDB self, int mapID)
		{
			return default(QuestMapResourceListDB_Param);
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x00009558 File Offset: 0x00007758
		[Token(Token = "0x6001433")]
		[Address(RVA = "0x10129697C", Offset = "0x129697C", VA = "0x10129697C")]
		public static int FindIndexByRefID(ref QuestMapResourceListDB_Param ref_param, int refID)
		{
			return 0;
		}
	}
}
