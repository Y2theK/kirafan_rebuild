﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200057E RID: 1406
	[Token(Token = "0x2000471")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct QuestMapResourceListDB_Param
	{
		// Token: 0x040019FB RID: 6651
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001365")]
		public int m_MapID;

		// Token: 0x040019FC RID: 6652
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001366")]
		public QuestMapResourceListDB_Data[] m_Datas;
	}
}
