﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000582 RID: 1410
	[Token(Token = "0x2000475")]
	[StructLayout(3)]
	public class QuestMapSDPositionDB : ScriptableObject
	{
		// Token: 0x060015E8 RID: 5608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001498")]
		[Address(RVA = "0x101296A20", Offset = "0x1296A20", VA = "0x101296A20")]
		public QuestMapSDPositionDB()
		{
		}

		// Token: 0x04001A05 RID: 6661
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400136F")]
		public QuestMapSDPositionDB_Param[] m_Params;
	}
}
