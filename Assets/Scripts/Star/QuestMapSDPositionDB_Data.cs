﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000580 RID: 1408
	[Token(Token = "0x2000473")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct QuestMapSDPositionDB_Data
	{
		// Token: 0x040019FE RID: 6654
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001368")]
		public int m_QuestID;

		// Token: 0x040019FF RID: 6655
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001369")]
		public int[] m_LocatorIDs;

		// Token: 0x04001A00 RID: 6656
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400136A")]
		public int[] m_CharaIDs;

		// Token: 0x04001A01 RID: 6657
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400136B")]
		public int[] m_OffsetXs;

		// Token: 0x04001A02 RID: 6658
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400136C")]
		public int[] m_OffsetYs;
	}
}
