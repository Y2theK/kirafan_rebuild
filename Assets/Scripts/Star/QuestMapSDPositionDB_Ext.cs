﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000521 RID: 1313
	[Token(Token = "0x2000417")]
	[StructLayout(3)]
	public static class QuestMapSDPositionDB_Ext
	{
		// Token: 0x0600157F RID: 5503 RVA: 0x00009570 File Offset: 0x00007770
		[Token(Token = "0x6001434")]
		[Address(RVA = "0x101296A28", Offset = "0x1296A28", VA = "0x101296A28")]
		public static QuestMapSDPositionDB_Param GetParam(this QuestMapSDPositionDB self, int mapID)
		{
			return default(QuestMapSDPositionDB_Param);
		}

		// Token: 0x06001580 RID: 5504 RVA: 0x00009588 File Offset: 0x00007788
		[Token(Token = "0x6001435")]
		[Address(RVA = "0x101296B10", Offset = "0x1296B10", VA = "0x101296B10")]
		public static int FindIndexByQuestID(ref QuestMapSDPositionDB_Param ref_param, int questID)
		{
			return 0;
		}
	}
}
