﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000581 RID: 1409
	[Token(Token = "0x2000474")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct QuestMapSDPositionDB_Param
	{
		// Token: 0x04001A03 RID: 6659
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400136D")]
		public int m_MapID;

		// Token: 0x04001A04 RID: 6660
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400136E")]
		public QuestMapSDPositionDB_Data[] m_Datas;
	}
}
