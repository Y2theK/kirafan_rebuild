﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000935 RID: 2357
	[Token(Token = "0x20006A9")]
	[StructLayout(3)]
	public class QuestMapScene
	{
		// Token: 0x170002BA RID: 698
		// (get) Token: 0x0600278C RID: 10124 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000284")]
		public QuestMapHandler QuestMapHndl
		{
			[Token(Token = "0x6002456")]
			[Address(RVA = "0x101296BC0", Offset = "0x1296BC0", VA = "0x101296BC0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x0600278D RID: 10125 RVA: 0x00010CE0 File Offset: 0x0000EEE0
		[Token(Token = "0x17000285")]
		public int MapID
		{
			[Token(Token = "0x6002457")]
			[Address(RVA = "0x101296BC8", Offset = "0x1296BC8", VA = "0x101296BC8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x0600278E RID: 10126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002458")]
		[Address(RVA = "0x101296BD0", Offset = "0x1296BD0", VA = "0x101296BD0")]
		public QuestMapScene()
		{
		}

		// Token: 0x0600278F RID: 10127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002459")]
		[Address(RVA = "0x101296C98", Offset = "0x1296C98", VA = "0x101296C98")]
		public void Update()
		{
		}

		// Token: 0x06002790 RID: 10128 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600245A")]
		[Address(RVA = "0x1012973D4", Offset = "0x12973D4", VA = "0x1012973D4")]
		public static string GetMapResourcePath(int mapID)
		{
			return null;
		}

		// Token: 0x06002791 RID: 10129 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600245B")]
		[Address(RVA = "0x10129744C", Offset = "0x129744C", VA = "0x10129744C")]
		public static string GetMapModelObjectPath(int mapID)
		{
			return null;
		}

		// Token: 0x06002792 RID: 10130 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600245C")]
		[Address(RVA = "0x1012974C4", Offset = "0x12974C4", VA = "0x1012974C4")]
		public static string GetSDResourcePath(int charaID)
		{
			return null;
		}

		// Token: 0x06002793 RID: 10131 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600245D")]
		[Address(RVA = "0x10129753C", Offset = "0x129753C", VA = "0x10129753C")]
		public static string GetSDModelObjectPathForFind()
		{
			return null;
		}

		// Token: 0x06002794 RID: 10132 RVA: 0x00010CF8 File Offset: 0x0000EEF8
		[Token(Token = "0x600245E")]
		[Address(RVA = "0x101297584", Offset = "0x1297584", VA = "0x101297584")]
		public bool Prepare(int eventQuestType, GameObject uiQuestMapObjPrefab)
		{
			return default(bool);
		}

		// Token: 0x06002795 RID: 10133 RVA: 0x00010D10 File Offset: 0x0000EF10
		[Token(Token = "0x600245F")]
		[Address(RVA = "0x101297624", Offset = "0x1297624", VA = "0x101297624")]
		public bool IsCompletePrepare()
		{
			return default(bool);
		}

		// Token: 0x06002796 RID: 10134 RVA: 0x00010D28 File Offset: 0x0000EF28
		[Token(Token = "0x6002460")]
		[Address(RVA = "0x101297634", Offset = "0x1297634", VA = "0x101297634")]
		public bool IsPrepareError()
		{
			return default(bool);
		}

		// Token: 0x06002797 RID: 10135 RVA: 0x00010D40 File Offset: 0x0000EF40
		[Token(Token = "0x6002461")]
		[Address(RVA = "0x101296CD8", Offset = "0x1296CD8", VA = "0x101296CD8")]
		private QuestMapScene.eMode Update_Prepare()
		{
			return QuestMapScene.eMode.Prepare;
		}

		// Token: 0x06002798 RID: 10136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002462")]
		[Address(RVA = "0x101297910", Offset = "0x1297910", VA = "0x101297910")]
		private void ConstructQuestMap(int mapID, GameObject uiQuestMapObjPrefab, List<GameObject> sdPrefabs)
		{
		}

		// Token: 0x06002799 RID: 10137 RVA: 0x00010D58 File Offset: 0x0000EF58
		[Token(Token = "0x6002463")]
		[Address(RVA = "0x101297644", Offset = "0x1297644", VA = "0x101297644")]
		private QuestMapSDPositionDB_Data CalcQuestMapSDPositionData(int mapID)
		{
			return default(QuestMapSDPositionDB_Data);
		}

		// Token: 0x0600279A RID: 10138 RVA: 0x00010D70 File Offset: 0x0000EF70
		[Token(Token = "0x6002464")]
		[Address(RVA = "0x1012982AC", Offset = "0x12982AC", VA = "0x1012982AC")]
		public bool Destory()
		{
			return default(bool);
		}

		// Token: 0x04003798 RID: 14232
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002840")]
		private QuestMapScene.eMode m_Mode;

		// Token: 0x04003799 RID: 14233
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002841")]
		private readonly Vector3 MAP_POSITION;

		// Token: 0x0400379A RID: 14234
		[Token(Token = "0x4002842")]
		private const string MAP_RESOURCE_PATH_FORMAT = "questmap/questmap_{0}.muast";

		// Token: 0x0400379B RID: 14235
		[Token(Token = "0x4002843")]
		private const string MAP_MODEL_OBJECT_PATH_FORMAT = "QuestMap_{0}";

		// Token: 0x0400379C RID: 14236
		[Token(Token = "0x4002844")]
		private const string SD_RESOURCE_PATH_FORMAT = "questmapsd/questmapsd_{0}.muast";

		// Token: 0x0400379D RID: 14237
		[Token(Token = "0x4002845")]
		private const string SD_MODEL_OBJECT_PATH_FOR_FIND = "QuestMapSD_";

		// Token: 0x0400379E RID: 14238
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002846")]
		private GameObject m_QuestMapGO;

		// Token: 0x0400379F RID: 14239
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002847")]
		private QuestMapHandler m_QuestMapHndl;

		// Token: 0x040037A0 RID: 14240
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002848")]
		private ABResourceLoader m_Loader;

		// Token: 0x040037A1 RID: 14241
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002849")]
		private ABResourceObjectHandler m_MapLoadingHndl;

		// Token: 0x040037A2 RID: 14242
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400284A")]
		private List<ABResourceObjectHandler> m_SDLoadingHndls;

		// Token: 0x040037A3 RID: 14243
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400284B")]
		private int m_EventQuestType;

		// Token: 0x040037A4 RID: 14244
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400284C")]
		private GameObject m_uiQuestMapObjPrefab;

		// Token: 0x040037A5 RID: 14245
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400284D")]
		private QuestMapScene.ePrepareStep m_PrepareStep;

		// Token: 0x02000936 RID: 2358
		[Token(Token = "0x2000F5C")]
		public enum eMode
		{
			// Token: 0x040037A7 RID: 14247
			[Token(Token = "0x40062CC")]
			None = -1,
			// Token: 0x040037A8 RID: 14248
			[Token(Token = "0x40062CD")]
			Prepare,
			// Token: 0x040037A9 RID: 14249
			[Token(Token = "0x40062CE")]
			UpdateMain,
			// Token: 0x040037AA RID: 14250
			[Token(Token = "0x40062CF")]
			Destroy
		}

		// Token: 0x02000937 RID: 2359
		[Token(Token = "0x2000F5D")]
		private enum ePrepareStep
		{
			// Token: 0x040037AC RID: 14252
			[Token(Token = "0x40062D1")]
			None = -1,
			// Token: 0x040037AD RID: 14253
			[Token(Token = "0x40062D2")]
			Prepare,
			// Token: 0x040037AE RID: 14254
			[Token(Token = "0x40062D3")]
			Prepare_Wait,
			// Token: 0x040037AF RID: 14255
			[Token(Token = "0x40062D4")]
			End,
			// Token: 0x040037B0 RID: 14256
			[Token(Token = "0x40062D5")]
			Prepare_Error
		}
	}
}
