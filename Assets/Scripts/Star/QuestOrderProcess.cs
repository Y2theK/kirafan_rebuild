﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000938 RID: 2360
	[Token(Token = "0x20006AA")]
	[StructLayout(3)]
	public class QuestOrderProcess
	{
		// Token: 0x170002BC RID: 700
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x00010D88 File Offset: 0x0000EF88
		[Token(Token = "0x17000286")]
		public bool IsAdvOnly
		{
			[Token(Token = "0x6002465")]
			[Address(RVA = "0x101298440", Offset = "0x1298440", VA = "0x101298440")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x0600279C RID: 10140 RVA: 0x00010DA0 File Offset: 0x0000EFA0
		[Token(Token = "0x6002466")]
		[Address(RVA = "0x101298448", Offset = "0x1298448", VA = "0x101298448")]
		public bool Order(int questID, long partyMngID, long friendCharaMngID, long questNpcID, bool isConfirmExKeyItem, Action onOrderSuccess, Action<QuestDefine.eReturnLogAdd> onOrderFailed)
		{
			return default(bool);
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002467")]
		[Address(RVA = "0x101298EA0", Offset = "0x1298EA0", VA = "0x101298EA0")]
		private void OnConfirmUseItem(int btn)
		{
		}

		// Token: 0x0600279E RID: 10142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002468")]
		[Address(RVA = "0x101298D24", Offset = "0x1298D24", VA = "0x101298D24")]
		private void Request()
		{
		}

		// Token: 0x0600279F RID: 10143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002469")]
		[Address(RVA = "0x101298EC4", Offset = "0x1298EC4", VA = "0x101298EC4")]
		private void OnResponse_QuestLogAdd(QuestDefine.eReturnLogAdd result, string errorMessage)
		{
		}

		// Token: 0x060027A0 RID: 10144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600246A")]
		[Address(RVA = "0x101299048", Offset = "0x1299048", VA = "0x101299048")]
		private void OnConfirmMoreStaminaNeeded(int answer)
		{
		}

		// Token: 0x060027A1 RID: 10145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600246B")]
		[Address(RVA = "0x1012990D8", Offset = "0x12990D8", VA = "0x1012990D8")]
		public QuestOrderProcess()
		{
		}

		// Token: 0x040037B1 RID: 14257
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400284E")]
		private int m_OrderQuestID;

		// Token: 0x040037B2 RID: 14258
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400284F")]
		private long m_OrderPartyMngID;

		// Token: 0x040037B3 RID: 14259
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002850")]
		private long m_OrderFriendCharaMngID;

		// Token: 0x040037B4 RID: 14260
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002851")]
		private long m_OrderQuestNpcID;

		// Token: 0x040037B5 RID: 14261
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002852")]
		private Action m_OnOrderSuccess;

		// Token: 0x040037B6 RID: 14262
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002853")]
		private Action<QuestDefine.eReturnLogAdd> m_OnOrderFailed;

		// Token: 0x040037B7 RID: 14263
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002854")]
		private QuestDefine.eReturnLogAdd m_OrderResult;

		// Token: 0x040037B8 RID: 14264
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002855")]
		private bool m_IsConfirmExKeyItem;

		// Token: 0x040037B9 RID: 14265
		[Cpp2IlInjected.FieldOffset(Offset = "0x45")]
		[Token(Token = "0x4002856")]
		private bool m_IsAdvOnly;
	}
}
