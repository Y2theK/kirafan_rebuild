﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007F7 RID: 2039
	[Token(Token = "0x2000606")]
	[StructLayout(3)]
	public class QuestState : GameStateBase
	{
		// Token: 0x06001FDE RID: 8158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D56")]
		[Address(RVA = "0x1012991F4", Offset = "0x12991F4", VA = "0x1012991F4")]
		public QuestState(QuestMain owner)
		{
		}

		// Token: 0x06001FDF RID: 8159 RVA: 0x0000E0E8 File Offset: 0x0000C2E8
		[Token(Token = "0x6001D57")]
		[Address(RVA = "0x101299228", Offset = "0x1299228", VA = "0x101299228", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001FE0 RID: 8160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D58")]
		[Address(RVA = "0x101299230", Offset = "0x1299230", VA = "0x101299230", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001FE1 RID: 8161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D59")]
		[Address(RVA = "0x101299234", Offset = "0x1299234", VA = "0x101299234", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001FE2 RID: 8162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D5A")]
		[Address(RVA = "0x101299238", Offset = "0x1299238", VA = "0x101299238", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001FE3 RID: 8163 RVA: 0x0000E100 File Offset: 0x0000C300
		[Token(Token = "0x6001D5B")]
		[Address(RVA = "0x10129923C", Offset = "0x129923C", VA = "0x10129923C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001FE4 RID: 8164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D5C")]
		[Address(RVA = "0x101299244", Offset = "0x1299244", VA = "0x101299244", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001FE5 RID: 8165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D5D")]
		[Address(RVA = "0x101299248", Offset = "0x1299248", VA = "0x101299248")]
		private void NextStateToUISetting(int nextState, out eEventQuestUISettingCharaDispMode mode, out int charaID, out EventQuestUISettingDB_Param? param)
		{
		}

		// Token: 0x06001FE6 RID: 8166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D5E")]
		[Address(RVA = "0x101299494", Offset = "0x1299494", VA = "0x101299494")]
		protected void PlayInChara(int nextState)
		{
		}

		// Token: 0x06001FE7 RID: 8167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D5F")]
		[Address(RVA = "0x1012996E4", Offset = "0x12996E4", VA = "0x1012996E4")]
		protected void PlayOutChara(int nextState)
		{
		}

		// Token: 0x06001FE8 RID: 8168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D60")]
		[Address(RVA = "0x10129987C", Offset = "0x129987C", VA = "0x10129987C")]
		protected void OnCanceledPartyEditSave()
		{
		}

		// Token: 0x04002FF9 RID: 12281
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40024BE")]
		protected QuestMain m_Owner;

		// Token: 0x04002FFA RID: 12282
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40024BF")]
		protected int m_NextState;
	}
}
