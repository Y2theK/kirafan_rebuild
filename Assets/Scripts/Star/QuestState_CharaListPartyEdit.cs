﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007F8 RID: 2040
	[Token(Token = "0x2000607")]
	[StructLayout(3)]
	public class QuestState_CharaListPartyEdit : QuestState
	{
		// Token: 0x06001FE9 RID: 8169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D61")]
		[Address(RVA = "0x1012998F4", Offset = "0x12998F4", VA = "0x1012998F4")]
		public QuestState_CharaListPartyEdit(QuestMain owner)
		{
		}

		// Token: 0x06001FEA RID: 8170 RVA: 0x0000E118 File Offset: 0x0000C318
		[Token(Token = "0x6001D62")]
		[Address(RVA = "0x101299930", Offset = "0x1299930", VA = "0x101299930", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001FEB RID: 8171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D63")]
		[Address(RVA = "0x101299938", Offset = "0x1299938", VA = "0x101299938", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001FEC RID: 8172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D64")]
		[Address(RVA = "0x101299940", Offset = "0x1299940", VA = "0x101299940", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001FED RID: 8173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D65")]
		[Address(RVA = "0x101299944", Offset = "0x1299944", VA = "0x101299944", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001FEE RID: 8174 RVA: 0x0000E130 File Offset: 0x0000C330
		[Token(Token = "0x6001D66")]
		[Address(RVA = "0x101299948", Offset = "0x1299948", VA = "0x101299948", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001FEF RID: 8175 RVA: 0x0000E148 File Offset: 0x0000C348
		[Token(Token = "0x6001D67")]
		[Address(RVA = "0x101299E38", Offset = "0x1299E38", VA = "0x101299E38")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06001FF0 RID: 8176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D68")]
		[Address(RVA = "0x10129A114", Offset = "0x129A114", VA = "0x10129A114", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001FF1 RID: 8177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D69")]
		[Address(RVA = "0x10129A214", Offset = "0x129A214", VA = "0x10129A214")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04002FFB RID: 12283
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024C0")]
		private QuestState_CharaListPartyEdit.eStep m_Step;

		// Token: 0x04002FFC RID: 12284
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024C1")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x020007F9 RID: 2041
		[Token(Token = "0x2000EC2")]
		private enum eStep
		{
			// Token: 0x04002FFE RID: 12286
			[Token(Token = "0x4005EB2")]
			None = -1,
			// Token: 0x04002FFF RID: 12287
			[Token(Token = "0x4005EB3")]
			First,
			// Token: 0x04003000 RID: 12288
			[Token(Token = "0x4005EB4")]
			LoadWait,
			// Token: 0x04003001 RID: 12289
			[Token(Token = "0x4005EB5")]
			PlayIn,
			// Token: 0x04003002 RID: 12290
			[Token(Token = "0x4005EB6")]
			Main
		}
	}
}
