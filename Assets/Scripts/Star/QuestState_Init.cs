﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020007FA RID: 2042
	[Token(Token = "0x2000608")]
	[StructLayout(3)]
	public class QuestState_Init : QuestState
	{
		// Token: 0x06001FF2 RID: 8178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D6A")]
		[Address(RVA = "0x10129A260", Offset = "0x129A260", VA = "0x10129A260")]
		public QuestState_Init(QuestMain owner)
		{
		}

		// Token: 0x06001FF3 RID: 8179 RVA: 0x0000E160 File Offset: 0x0000C360
		[Token(Token = "0x6001D6B")]
		[Address(RVA = "0x10129A29C", Offset = "0x129A29C", VA = "0x10129A29C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001FF4 RID: 8180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D6C")]
		[Address(RVA = "0x10129A2A4", Offset = "0x129A2A4", VA = "0x10129A2A4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001FF5 RID: 8181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D6D")]
		[Address(RVA = "0x10129A2AC", Offset = "0x129A2AC", VA = "0x10129A2AC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06001FF6 RID: 8182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D6E")]
		[Address(RVA = "0x10129A2B0", Offset = "0x129A2B0", VA = "0x10129A2B0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06001FF7 RID: 8183 RVA: 0x0000E178 File Offset: 0x0000C378
		[Token(Token = "0x6001D6F")]
		[Address(RVA = "0x10129A2B4", Offset = "0x129A2B4", VA = "0x10129A2B4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06001FF8 RID: 8184 RVA: 0x0000E190 File Offset: 0x0000C390
		[Token(Token = "0x6001D70")]
		[Address(RVA = "0x10129B11C", Offset = "0x129B11C", VA = "0x10129B11C")]
		private bool SetupUI()
		{
			return default(bool);
		}

		// Token: 0x06001FF9 RID: 8185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D71")]
		[Address(RVA = "0x10129B278", Offset = "0x129B278", VA = "0x10129B278", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06001FFA RID: 8186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D72")]
		[Address(RVA = "0x10129B27C", Offset = "0x129B27C", VA = "0x10129B27C")]
		private void OnResponse_QuestGetAll()
		{
		}

		// Token: 0x06001FFB RID: 8187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D73")]
		[Address(RVA = "0x10129B338", Offset = "0x129B338", VA = "0x10129B338")]
		private void OnResponse_QuestChapterGetAll()
		{
		}

		// Token: 0x04003003 RID: 12291
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024C2")]
		private QuestState_Init.eStep m_Step;

		// Token: 0x04003004 RID: 12292
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024C3")]
		private bool m_IsDontCloseLoadingUI;

		// Token: 0x020007FB RID: 2043
		[Token(Token = "0x2000EC3")]
		private enum eStep
		{
			// Token: 0x04003006 RID: 12294
			[Token(Token = "0x4005EB8")]
			None = -1,
			// Token: 0x04003007 RID: 12295
			[Token(Token = "0x4005EB9")]
			First,
			// Token: 0x04003008 RID: 12296
			[Token(Token = "0x4005EBA")]
			Request_Wait,
			// Token: 0x04003009 RID: 12297
			[Token(Token = "0x4005EBB")]
			Load_Wait,
			// Token: 0x0400300A RID: 12298
			[Token(Token = "0x4005EBC")]
			CheckNextQuest,
			// Token: 0x0400300B RID: 12299
			[Token(Token = "0x4005EBD")]
			LoadNpcUI,
			// Token: 0x0400300C RID: 12300
			[Token(Token = "0x4005EBE")]
			LoadNpcUI_Wait,
			// Token: 0x0400300D RID: 12301
			[Token(Token = "0x4005EBF")]
			LoadNpcUI_Setup,
			// Token: 0x0400300E RID: 12302
			[Token(Token = "0x4005EC0")]
			LoadBGWait,
			// Token: 0x0400300F RID: 12303
			[Token(Token = "0x4005EC1")]
			End
		}
	}
}
