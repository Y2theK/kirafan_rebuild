﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x020007FC RID: 2044
	[Token(Token = "0x2000609")]
	[StructLayout(3)]
	public class QuestState_QuestCategorySelect : QuestState
	{
		// Token: 0x06001FFC RID: 8188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D74")]
		[Address(RVA = "0x10129B3C4", Offset = "0x129B3C4", VA = "0x10129B3C4")]
		public QuestState_QuestCategorySelect(QuestMain owner)
		{
		}

		// Token: 0x06001FFD RID: 8189 RVA: 0x0000E1A8 File Offset: 0x0000C3A8
		[Token(Token = "0x6001D75")]
		[Address(RVA = "0x10129B3F8", Offset = "0x129B3F8", VA = "0x10129B3F8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001FFE RID: 8190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D76")]
		[Address(RVA = "0x10129B400", Offset = "0x129B400", VA = "0x10129B400", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001FFF RID: 8191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D77")]
		[Address(RVA = "0x10129B408", Offset = "0x129B408", VA = "0x10129B408", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002000 RID: 8192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D78")]
		[Address(RVA = "0x10129B40C", Offset = "0x129B40C", VA = "0x10129B40C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002001 RID: 8193 RVA: 0x0000E1C0 File Offset: 0x0000C3C0
		[Token(Token = "0x6001D79")]
		[Address(RVA = "0x10129B414", Offset = "0x129B414", VA = "0x10129B414", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002002 RID: 8194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D7A")]
		[Address(RVA = "0x10129BC4C", Offset = "0x129BC4C", VA = "0x10129BC4C")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x06002003 RID: 8195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D7B")]
		[Address(RVA = "0x10129BB60", Offset = "0x129BB60", VA = "0x10129BB60")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002004 RID: 8196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D7C")]
		[Address(RVA = "0x10129BBC4", Offset = "0x129BBC4", VA = "0x10129BBC4")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002005 RID: 8197 RVA: 0x0000E1D8 File Offset: 0x0000C3D8
		[Token(Token = "0x6001D7D")]
		[Address(RVA = "0x10129B7C8", Offset = "0x129B7C8", VA = "0x10129B7C8")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002006 RID: 8198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D7E")]
		[Address(RVA = "0x10129BD8C", Offset = "0x129BD8C", VA = "0x10129BD8C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002007 RID: 8199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D7F")]
		[Address(RVA = "0x10129BE94", Offset = "0x129BE94", VA = "0x10129BE94")]
		private void OnClickCategoryCallBack()
		{
		}

		// Token: 0x06002008 RID: 8200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D80")]
		[Address(RVA = "0x10129BCE0", Offset = "0x129BCE0", VA = "0x10129BCE0")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04003010 RID: 12304
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024C4")]
		private QuestState_QuestCategorySelect.eStep m_Step;

		// Token: 0x04003011 RID: 12305
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024C5")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003012 RID: 12306
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024C6")]
		private QuestCategorySelectUI m_UI;

		// Token: 0x020007FD RID: 2045
		[Token(Token = "0x2000EC4")]
		private enum eStep
		{
			// Token: 0x04003014 RID: 12308
			[Token(Token = "0x4005EC3")]
			None = -1,
			// Token: 0x04003015 RID: 12309
			[Token(Token = "0x4005EC4")]
			First,
			// Token: 0x04003016 RID: 12310
			[Token(Token = "0x4005EC5")]
			LoadWait,
			// Token: 0x04003017 RID: 12311
			[Token(Token = "0x4005EC6")]
			PlayIn,
			// Token: 0x04003018 RID: 12312
			[Token(Token = "0x4005EC7")]
			QuestNotice,
			// Token: 0x04003019 RID: 12313
			[Token(Token = "0x4005EC8")]
			Unlock,
			// Token: 0x0400301A RID: 12314
			[Token(Token = "0x4005EC9")]
			Main,
			// Token: 0x0400301B RID: 12315
			[Token(Token = "0x4005ECA")]
			UnloadChildSceneWait
		}
	}
}
