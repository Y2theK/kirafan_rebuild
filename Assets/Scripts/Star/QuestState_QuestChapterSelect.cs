﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x020007FE RID: 2046
	[Token(Token = "0x200060A")]
	[StructLayout(3)]
	public class QuestState_QuestChapterSelect : QuestState
	{
		// Token: 0x06002009 RID: 8201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D81")]
		[Address(RVA = "0x10129C184", Offset = "0x129C184", VA = "0x10129C184")]
		public QuestState_QuestChapterSelect(QuestMain owner)
		{
		}

		// Token: 0x0600200A RID: 8202 RVA: 0x0000E1F0 File Offset: 0x0000C3F0
		[Token(Token = "0x6001D82")]
		[Address(RVA = "0x10129C1C0", Offset = "0x129C1C0", VA = "0x10129C1C0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600200B RID: 8203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D83")]
		[Address(RVA = "0x10129C1C8", Offset = "0x129C1C8", VA = "0x10129C1C8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600200C RID: 8204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D84")]
		[Address(RVA = "0x10129C1D0", Offset = "0x129C1D0", VA = "0x10129C1D0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600200D RID: 8205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D85")]
		[Address(RVA = "0x10129C1D4", Offset = "0x129C1D4", VA = "0x10129C1D4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600200E RID: 8206 RVA: 0x0000E208 File Offset: 0x0000C408
		[Token(Token = "0x6001D86")]
		[Address(RVA = "0x10129C1DC", Offset = "0x129C1DC", VA = "0x10129C1DC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600200F RID: 8207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D87")]
		[Address(RVA = "0x10129CBE4", Offset = "0x129CBE4", VA = "0x10129CBE4")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x06002010 RID: 8208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D88")]
		[Address(RVA = "0x10129CA90", Offset = "0x129CA90", VA = "0x10129CA90")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002011 RID: 8209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D89")]
		[Address(RVA = "0x10129CB5C", Offset = "0x129CB5C", VA = "0x10129CB5C")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002012 RID: 8210 RVA: 0x0000E220 File Offset: 0x0000C420
		[Token(Token = "0x6001D8A")]
		[Address(RVA = "0x10129C658", Offset = "0x129C658", VA = "0x10129C658")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002013 RID: 8211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D8B")]
		[Address(RVA = "0x10129CD24", Offset = "0x129CD24", VA = "0x10129CD24", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002014 RID: 8212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D8C")]
		[Address(RVA = "0x10129CE04", Offset = "0x129CE04", VA = "0x10129CE04")]
		private void OnClickChapterButtonCallBack()
		{
		}

		// Token: 0x06002015 RID: 8213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D8D")]
		[Address(RVA = "0x10129CC78", Offset = "0x129CC78", VA = "0x10129CC78")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x0400301C RID: 12316
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024C7")]
		private QuestState_QuestChapterSelect.eStep m_Step;

		// Token: 0x0400301D RID: 12317
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024C8")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400301E RID: 12318
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024C9")]
		private QuestChapterSelectUI m_UI;

		// Token: 0x020007FF RID: 2047
		[Token(Token = "0x2000EC5")]
		private enum eStep
		{
			// Token: 0x04003020 RID: 12320
			[Token(Token = "0x4005ECC")]
			None = -1,
			// Token: 0x04003021 RID: 12321
			[Token(Token = "0x4005ECD")]
			First,
			// Token: 0x04003022 RID: 12322
			[Token(Token = "0x4005ECE")]
			LoadWait,
			// Token: 0x04003023 RID: 12323
			[Token(Token = "0x4005ECF")]
			PlayIn,
			// Token: 0x04003024 RID: 12324
			[Token(Token = "0x4005ED0")]
			ChapterAnim,
			// Token: 0x04003025 RID: 12325
			[Token(Token = "0x4005ED1")]
			QuestNotice,
			// Token: 0x04003026 RID: 12326
			[Token(Token = "0x4005ED2")]
			Unlock,
			// Token: 0x04003027 RID: 12327
			[Token(Token = "0x4005ED3")]
			Main,
			// Token: 0x04003028 RID: 12328
			[Token(Token = "0x4005ED4")]
			UnloadChildSceneWait
		}
	}
}
