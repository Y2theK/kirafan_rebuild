﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000800 RID: 2048
	[Token(Token = "0x200060B")]
	[StructLayout(3)]
	public class QuestState_QuestCharaSelect : QuestState
	{
		// Token: 0x06002016 RID: 8214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D8E")]
		[Address(RVA = "0x10129D0D4", Offset = "0x129D0D4", VA = "0x10129D0D4")]
		public QuestState_QuestCharaSelect(QuestMain owner)
		{
		}

		// Token: 0x06002017 RID: 8215 RVA: 0x0000E238 File Offset: 0x0000C438
		[Token(Token = "0x6001D8F")]
		[Address(RVA = "0x10129D110", Offset = "0x129D110", VA = "0x10129D110", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002018 RID: 8216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D90")]
		[Address(RVA = "0x10129D118", Offset = "0x129D118", VA = "0x10129D118", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002019 RID: 8217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D91")]
		[Address(RVA = "0x10129D120", Offset = "0x129D120", VA = "0x10129D120", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600201A RID: 8218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D92")]
		[Address(RVA = "0x10129D124", Offset = "0x129D124", VA = "0x10129D124", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600201B RID: 8219 RVA: 0x0000E250 File Offset: 0x0000C450
		[Token(Token = "0x6001D93")]
		[Address(RVA = "0x10129D12C", Offset = "0x129D12C", VA = "0x10129D12C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600201C RID: 8220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D94")]
		[Address(RVA = "0x10129D914", Offset = "0x129D914", VA = "0x10129D914")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x0600201D RID: 8221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D95")]
		[Address(RVA = "0x10129D828", Offset = "0x129D828", VA = "0x10129D828")]
		private void OnGotoADV()
		{
		}

		// Token: 0x0600201E RID: 8222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D96")]
		[Address(RVA = "0x10129D88C", Offset = "0x129D88C", VA = "0x10129D88C")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x0600201F RID: 8223 RVA: 0x0000E268 File Offset: 0x0000C468
		[Token(Token = "0x6001D97")]
		[Address(RVA = "0x10129D4A0", Offset = "0x129D4A0", VA = "0x10129D4A0")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002020 RID: 8224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D98")]
		[Address(RVA = "0x10129DA74", Offset = "0x129DA74", VA = "0x10129DA74", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002021 RID: 8225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D99")]
		[Address(RVA = "0x10129DB94", Offset = "0x129DB94", VA = "0x10129DB94")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002022 RID: 8226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D9A")]
		[Address(RVA = "0x10129D9A8", Offset = "0x129D9A8", VA = "0x10129D9A8")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04003029 RID: 12329
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024CA")]
		private QuestState_QuestCharaSelect.eStep m_Step;

		// Token: 0x0400302A RID: 12330
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024CB")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400302B RID: 12331
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024CC")]
		public QuestCharaSelectUI m_UI;

		// Token: 0x02000801 RID: 2049
		[Token(Token = "0x2000EC6")]
		private enum eStep
		{
			// Token: 0x0400302D RID: 12333
			[Token(Token = "0x4005ED6")]
			None = -1,
			// Token: 0x0400302E RID: 12334
			[Token(Token = "0x4005ED7")]
			First,
			// Token: 0x0400302F RID: 12335
			[Token(Token = "0x4005ED8")]
			LoadWait,
			// Token: 0x04003030 RID: 12336
			[Token(Token = "0x4005ED9")]
			PlayIn,
			// Token: 0x04003031 RID: 12337
			[Token(Token = "0x4005EDA")]
			QuestNotice,
			// Token: 0x04003032 RID: 12338
			[Token(Token = "0x4005EDB")]
			Unlock,
			// Token: 0x04003033 RID: 12339
			[Token(Token = "0x4005EDC")]
			Main,
			// Token: 0x04003034 RID: 12340
			[Token(Token = "0x4005EDD")]
			UnloadChildSceneWait
		}
	}
}
