﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000802 RID: 2050
	[Token(Token = "0x200060C")]
	[StructLayout(3)]
	public class QuestState_QuestConfirm : QuestState
	{
		// Token: 0x06002023 RID: 8227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D9B")]
		[Address(RVA = "0x10129DCC8", Offset = "0x129DCC8", VA = "0x10129DCC8")]
		public QuestState_QuestConfirm(QuestMain owner)
		{
		}

		// Token: 0x06002024 RID: 8228 RVA: 0x0000E280 File Offset: 0x0000C480
		[Token(Token = "0x6001D9C")]
		[Address(RVA = "0x10129DD70", Offset = "0x129DD70", VA = "0x10129DD70", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002025 RID: 8229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D9D")]
		[Address(RVA = "0x10129DD78", Offset = "0x129DD78", VA = "0x10129DD78", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002026 RID: 8230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D9E")]
		[Address(RVA = "0x10129DD80", Offset = "0x129DD80", VA = "0x10129DD80", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002027 RID: 8231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001D9F")]
		[Address(RVA = "0x10129DD84", Offset = "0x129DD84", VA = "0x10129DD84", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002028 RID: 8232 RVA: 0x0000E298 File Offset: 0x0000C498
		[Token(Token = "0x6001DA0")]
		[Address(RVA = "0x10129DD8C", Offset = "0x129DD8C", VA = "0x10129DD8C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002029 RID: 8233 RVA: 0x0000E2B0 File Offset: 0x0000C4B0
		[Token(Token = "0x6001DA1")]
		[Address(RVA = "0x10129E290", Offset = "0x129E290", VA = "0x10129E290")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600202A RID: 8234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA2")]
		[Address(RVA = "0x10129E98C", Offset = "0x129E98C", VA = "0x10129E98C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600202B RID: 8235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA3")]
		[Address(RVA = "0x10129EB3C", Offset = "0x129EB3C", VA = "0x10129EB3C")]
		private void OnClickButton_Edit()
		{
		}

		// Token: 0x0600202C RID: 8236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA4")]
		[Address(RVA = "0x10129ECC4", Offset = "0x129ECC4", VA = "0x10129ECC4")]
		private void OnClickButton_Confirm()
		{
		}

		// Token: 0x0600202D RID: 8237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA5")]
		[Address(RVA = "0x10129ED38", Offset = "0x129ED38", VA = "0x10129ED38")]
		private void QuestOrder()
		{
		}

		// Token: 0x0600202E RID: 8238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA6")]
		[Address(RVA = "0x10129EFE4", Offset = "0x129EFE4", VA = "0x10129EFE4")]
		private void OnOrderSuccess()
		{
		}

		// Token: 0x0600202F RID: 8239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA7")]
		[Address(RVA = "0x10129EFEC", Offset = "0x129EFEC", VA = "0x10129EFEC")]
		private void OnOrderFailed(QuestDefine.eReturnLogAdd result)
		{
		}

		// Token: 0x06002030 RID: 8240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA8")]
		[Address(RVA = "0x10129F050", Offset = "0x129F050", VA = "0x10129F050")]
		private void OnDecideNameChange(string partyName)
		{
		}

		// Token: 0x06002031 RID: 8241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DA9")]
		[Address(RVA = "0x10129EA20", Offset = "0x129EA20", VA = "0x10129EA20")]
		private void RequestPartySet(Action callback)
		{
		}

		// Token: 0x06002032 RID: 8242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DAA")]
		[Address(RVA = "0x10129EBE4", Offset = "0x129EBE4", VA = "0x10129EBE4")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06002033 RID: 8243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DAB")]
		[Address(RVA = "0x10129F0F8", Offset = "0x129F0F8", VA = "0x10129F0F8")]
		public void Request_BattlePartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x06002034 RID: 8244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DAC")]
		[Address(RVA = "0x10129F270", Offset = "0x129F270", VA = "0x10129F270")]
		private void OnResponse_BattlePartySetName(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002035 RID: 8245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DAD")]
		[Address(RVA = "0x10129E674", Offset = "0x129E674", VA = "0x10129E674")]
		private void RequestQuestStartVoice()
		{
		}

		// Token: 0x04003035 RID: 12341
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024CD")]
		private QuestState_QuestConfirm.eStep m_Step;

		// Token: 0x04003036 RID: 12342
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024CE")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003037 RID: 12343
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024CF")]
		private PartyEditUI m_UI;

		// Token: 0x04003038 RID: 12344
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024D0")]
		private QuestOrderProcess m_QuestOrderProcess;

		// Token: 0x04003039 RID: 12345
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40024D1")]
		private string m_RequestPartyName;

		// Token: 0x0400303A RID: 12346
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40024D2")]
		private bool m_IsOutOfPeriod;

		// Token: 0x02000803 RID: 2051
		[Token(Token = "0x2000EC7")]
		private enum eStep
		{
			// Token: 0x0400303C RID: 12348
			[Token(Token = "0x4005EDF")]
			None = -1,
			// Token: 0x0400303D RID: 12349
			[Token(Token = "0x4005EE0")]
			First,
			// Token: 0x0400303E RID: 12350
			[Token(Token = "0x4005EE1")]
			LoadWait,
			// Token: 0x0400303F RID: 12351
			[Token(Token = "0x4005EE2")]
			PlayIn,
			// Token: 0x04003040 RID: 12352
			[Token(Token = "0x4005EE3")]
			Main,
			// Token: 0x04003041 RID: 12353
			[Token(Token = "0x4005EE4")]
			UnloadChildSceneWait
		}
	}
}
