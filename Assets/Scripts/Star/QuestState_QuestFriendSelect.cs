﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000804 RID: 2052
	[Token(Token = "0x200060D")]
	[StructLayout(3)]
	public class QuestState_QuestFriendSelect : QuestState
	{
		// Token: 0x06002036 RID: 8246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DAE")]
		[Address(RVA = "0x10129F450", Offset = "0x129F450", VA = "0x10129F450")]
		public QuestState_QuestFriendSelect(QuestMain owner)
		{
		}

		// Token: 0x06002037 RID: 8247 RVA: 0x0000E2C8 File Offset: 0x0000C4C8
		[Token(Token = "0x6001DAF")]
		[Address(RVA = "0x10129F48C", Offset = "0x129F48C", VA = "0x10129F48C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002038 RID: 8248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB0")]
		[Address(RVA = "0x10129F494", Offset = "0x129F494", VA = "0x10129F494", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002039 RID: 8249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB1")]
		[Address(RVA = "0x10129F49C", Offset = "0x129F49C", VA = "0x10129F49C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600203A RID: 8250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB2")]
		[Address(RVA = "0x10129F4A0", Offset = "0x129F4A0", VA = "0x10129F4A0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600203B RID: 8251 RVA: 0x0000E2E0 File Offset: 0x0000C4E0
		[Token(Token = "0x6001DB3")]
		[Address(RVA = "0x10129F528", Offset = "0x129F528", VA = "0x10129F528", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600203C RID: 8252 RVA: 0x0000E2F8 File Offset: 0x0000C4F8
		[Token(Token = "0x6001DB4")]
		[Address(RVA = "0x10129FA6C", Offset = "0x129FA6C", VA = "0x10129FA6C")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600203D RID: 8253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB5")]
		[Address(RVA = "0x10129FCA8", Offset = "0x129FCA8", VA = "0x10129FCA8")]
		private void OnClickButtonCallback()
		{
		}

		// Token: 0x0600203E RID: 8254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB6")]
		[Address(RVA = "0x10129FEFC", Offset = "0x129FEFC", VA = "0x10129FEFC")]
		private void OnClickReloadButtonCallback()
		{
		}

		// Token: 0x0600203F RID: 8255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB7")]
		[Address(RVA = "0x1012A0088", Offset = "0x12A0088", VA = "0x1012A0088")]
		private void OnReload(bool flg)
		{
		}

		// Token: 0x06002040 RID: 8256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB8")]
		[Address(RVA = "0x1012A01A8", Offset = "0x12A01A8", VA = "0x1012A01A8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002041 RID: 8257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DB9")]
		[Address(RVA = "0x10129FE1C", Offset = "0x129FE1C", VA = "0x10129FE1C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x06002042 RID: 8258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DBA")]
		[Address(RVA = "0x1012A01AC", Offset = "0x12A01AC", VA = "0x1012A01AC")]
		private void OnResponse_GetAll(bool isError)
		{
		}

		// Token: 0x04003042 RID: 12354
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024D3")]
		private QuestState_QuestFriendSelect.eStep m_Step;

		// Token: 0x04003043 RID: 12355
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024D4")]
		private bool m_IsNpcOnly;

		// Token: 0x04003044 RID: 12356
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40024D5")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003045 RID: 12357
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024D6")]
		private QuestFriendSelectUI m_UI;

		// Token: 0x02000805 RID: 2053
		[Token(Token = "0x2000EC8")]
		private enum eStep
		{
			// Token: 0x04003047 RID: 12359
			[Token(Token = "0x4005EE6")]
			None = -1,
			// Token: 0x04003048 RID: 12360
			[Token(Token = "0x4005EE7")]
			First,
			// Token: 0x04003049 RID: 12361
			[Token(Token = "0x4005EE8")]
			Request_Wait,
			// Token: 0x0400304A RID: 12362
			[Token(Token = "0x4005EE9")]
			LoadStart,
			// Token: 0x0400304B RID: 12363
			[Token(Token = "0x4005EEA")]
			LoadWait,
			// Token: 0x0400304C RID: 12364
			[Token(Token = "0x4005EEB")]
			PlayIn,
			// Token: 0x0400304D RID: 12365
			[Token(Token = "0x4005EEC")]
			Main,
			// Token: 0x0400304E RID: 12366
			[Token(Token = "0x4005EED")]
			UnloadChildSceneWait
		}
	}
}
