﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000806 RID: 2054
	[Token(Token = "0x200060E")]
	[StructLayout(3)]
	public class QuestState_QuestGroupSelect : QuestState
	{
		// Token: 0x06002043 RID: 8259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DBB")]
		[Address(RVA = "0x1012A01B8", Offset = "0x12A01B8", VA = "0x1012A01B8")]
		public QuestState_QuestGroupSelect(QuestMain owner)
		{
		}

		// Token: 0x06002044 RID: 8260 RVA: 0x0000E310 File Offset: 0x0000C510
		[Token(Token = "0x6001DBC")]
		[Address(RVA = "0x1012A01F4", Offset = "0x12A01F4", VA = "0x1012A01F4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002045 RID: 8261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DBD")]
		[Address(RVA = "0x1012A01FC", Offset = "0x12A01FC", VA = "0x1012A01FC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002046 RID: 8262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DBE")]
		[Address(RVA = "0x1012A0204", Offset = "0x12A0204", VA = "0x1012A0204", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002047 RID: 8263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DBF")]
		[Address(RVA = "0x1012A0208", Offset = "0x12A0208", VA = "0x1012A0208", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002048 RID: 8264 RVA: 0x0000E328 File Offset: 0x0000C528
		[Token(Token = "0x6001DC0")]
		[Address(RVA = "0x1012A0210", Offset = "0x12A0210", VA = "0x1012A0210", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002049 RID: 8265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC1")]
		[Address(RVA = "0x1012A0DDC", Offset = "0x12A0DDC", VA = "0x1012A0DDC")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x0600204A RID: 8266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC2")]
		[Address(RVA = "0x1012A0C54", Offset = "0x12A0C54", VA = "0x1012A0C54")]
		private void OnGotoADV()
		{
		}

		// Token: 0x0600204B RID: 8267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC3")]
		[Address(RVA = "0x1012A0CB8", Offset = "0x12A0CB8", VA = "0x1012A0CB8")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x0600204C RID: 8268 RVA: 0x0000E340 File Offset: 0x0000C540
		[Token(Token = "0x6001DC4")]
		[Address(RVA = "0x1012A08E4", Offset = "0x12A08E4", VA = "0x1012A08E4")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600204D RID: 8269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC5")]
		[Address(RVA = "0x1012A0F3C", Offset = "0x12A0F3C", VA = "0x1012A0F3C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600204E RID: 8270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC6")]
		[Address(RVA = "0x1012A0FA0", Offset = "0x12A0FA0", VA = "0x1012A0FA0")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600204F RID: 8271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC7")]
		[Address(RVA = "0x1012A0E70", Offset = "0x12A0E70", VA = "0x1012A0E70")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x06002050 RID: 8272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC8")]
		[Address(RVA = "0x1012A12AC", Offset = "0x12A12AC", VA = "0x1012A12AC")]
		private void OnResponsePointEventGet()
		{
		}

		// Token: 0x0400304F RID: 12367
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024D7")]
		private QuestState_QuestGroupSelect.eStep m_Step;

		// Token: 0x04003050 RID: 12368
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024D8")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003051 RID: 12369
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024D9")]
		public QuestGroupSelectUI m_UI;

		// Token: 0x04003052 RID: 12370
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024DA")]
		private bool m_IsVoicePlayed;

		// Token: 0x02000807 RID: 2055
		[Token(Token = "0x2000EC9")]
		private enum eStep
		{
			// Token: 0x04003054 RID: 12372
			[Token(Token = "0x4005EEF")]
			None = -1,
			// Token: 0x04003055 RID: 12373
			[Token(Token = "0x4005EF0")]
			First,
			// Token: 0x04003056 RID: 12374
			[Token(Token = "0x4005EF1")]
			LoadWait,
			// Token: 0x04003057 RID: 12375
			[Token(Token = "0x4005EF2")]
			PointEventRequestWait,
			// Token: 0x04003058 RID: 12376
			[Token(Token = "0x4005EF3")]
			PlayIn,
			// Token: 0x04003059 RID: 12377
			[Token(Token = "0x4005EF4")]
			QuestNotice,
			// Token: 0x0400305A RID: 12378
			[Token(Token = "0x4005EF5")]
			Unlock,
			// Token: 0x0400305B RID: 12379
			[Token(Token = "0x4005EF6")]
			Main,
			// Token: 0x0400305C RID: 12380
			[Token(Token = "0x4005EF7")]
			UnloadChildSceneWait
		}
	}
}
