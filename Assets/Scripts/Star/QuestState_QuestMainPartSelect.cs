﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000808 RID: 2056
	[Token(Token = "0x200060F")]
	[StructLayout(3)]
	public class QuestState_QuestMainPartSelect : QuestState
	{
		// Token: 0x06002051 RID: 8273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DC9")]
		[Address(RVA = "0x1012A12B8", Offset = "0x12A12B8", VA = "0x1012A12B8")]
		public QuestState_QuestMainPartSelect(QuestMain owner)
		{
		}

		// Token: 0x06002052 RID: 8274 RVA: 0x0000E358 File Offset: 0x0000C558
		[Token(Token = "0x6001DCA")]
		[Address(RVA = "0x1012A12F4", Offset = "0x12A12F4", VA = "0x1012A12F4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002053 RID: 8275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DCB")]
		[Address(RVA = "0x1012A12FC", Offset = "0x12A12FC", VA = "0x1012A12FC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002054 RID: 8276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DCC")]
		[Address(RVA = "0x1012A1304", Offset = "0x12A1304", VA = "0x1012A1304", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002055 RID: 8277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DCD")]
		[Address(RVA = "0x1012A1308", Offset = "0x12A1308", VA = "0x1012A1308", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002056 RID: 8278 RVA: 0x0000E370 File Offset: 0x0000C570
		[Token(Token = "0x6001DCE")]
		[Address(RVA = "0x1012A1310", Offset = "0x12A1310", VA = "0x1012A1310", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002057 RID: 8279 RVA: 0x0000E388 File Offset: 0x0000C588
		[Token(Token = "0x6001DCF")]
		[Address(RVA = "0x1012A14DC", Offset = "0x12A14DC", VA = "0x1012A14DC")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002058 RID: 8280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD0")]
		[Address(RVA = "0x1012A17F4", Offset = "0x12A17F4", VA = "0x1012A17F4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002059 RID: 8281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD1")]
		[Address(RVA = "0x1012A1974", Offset = "0x12A1974", VA = "0x1012A1974")]
		private void OnClickPartButton()
		{
		}

		// Token: 0x0600205A RID: 8282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD2")]
		[Address(RVA = "0x1012A18C4", Offset = "0x12A18C4", VA = "0x1012A18C4")]
		private void GotoMenuEnd()
		{
		}

		// Token: 0x0400305D RID: 12381
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024DB")]
		private QuestState_QuestMainPartSelect.eStep m_Step;

		// Token: 0x0400305E RID: 12382
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024DC")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400305F RID: 12383
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024DD")]
		private QuestMainPartSelectUI m_UI;

		// Token: 0x02000809 RID: 2057
		[Token(Token = "0x2000ECA")]
		private enum eStep
		{
			// Token: 0x04003061 RID: 12385
			[Token(Token = "0x4005EF9")]
			None = -1,
			// Token: 0x04003062 RID: 12386
			[Token(Token = "0x4005EFA")]
			First,
			// Token: 0x04003063 RID: 12387
			[Token(Token = "0x4005EFB")]
			LoadWait,
			// Token: 0x04003064 RID: 12388
			[Token(Token = "0x4005EFC")]
			PlayIn,
			// Token: 0x04003065 RID: 12389
			[Token(Token = "0x4005EFD")]
			Main,
			// Token: 0x04003066 RID: 12390
			[Token(Token = "0x4005EFE")]
			UnloadChildSceneWait
		}
	}
}
