﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200080A RID: 2058
	[Token(Token = "0x2000610")]
	[StructLayout(3)]
	public class QuestState_QuestMap : QuestState
	{
		// Token: 0x0600205B RID: 8283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD3")]
		[Address(RVA = "0x1012A1C3C", Offset = "0x12A1C3C", VA = "0x1012A1C3C")]
		public QuestState_QuestMap(QuestMain owner)
		{
		}

		// Token: 0x0600205C RID: 8284 RVA: 0x0000E3A0 File Offset: 0x0000C5A0
		[Token(Token = "0x6001DD4")]
		[Address(RVA = "0x1012A1C78", Offset = "0x12A1C78", VA = "0x1012A1C78", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600205D RID: 8285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD5")]
		[Address(RVA = "0x1012A1C80", Offset = "0x12A1C80", VA = "0x1012A1C80", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600205E RID: 8286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD6")]
		[Address(RVA = "0x1012A1C88", Offset = "0x12A1C88", VA = "0x1012A1C88", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600205F RID: 8287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DD7")]
		[Address(RVA = "0x1012A1C8C", Offset = "0x12A1C8C", VA = "0x1012A1C8C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002060 RID: 8288 RVA: 0x0000E3B8 File Offset: 0x0000C5B8
		[Token(Token = "0x6001DD8")]
		[Address(RVA = "0x1012A1C94", Offset = "0x12A1C94", VA = "0x1012A1C94", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002061 RID: 8289 RVA: 0x0000E3D0 File Offset: 0x0000C5D0
		[Token(Token = "0x6001DD9")]
		[Address(RVA = "0x1012A2760", Offset = "0x12A2760", VA = "0x1012A2760")]
		private bool Request_Read()
		{
			return default(bool);
		}

		// Token: 0x06002062 RID: 8290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DDA")]
		[Address(RVA = "0x1012A2B44", Offset = "0x12A2B44", VA = "0x1012A2B44")]
		private void OnResponse_Read()
		{
		}

		// Token: 0x06002063 RID: 8291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DDB")]
		[Address(RVA = "0x1012A298C", Offset = "0x12A298C", VA = "0x1012A298C")]
		private void PostPlayIn()
		{
		}

		// Token: 0x06002064 RID: 8292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DDC")]
		[Address(RVA = "0x1012A2B48", Offset = "0x12A2B48", VA = "0x1012A2B48")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x06002065 RID: 8293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DDD")]
		[Address(RVA = "0x1012A2A58", Offset = "0x12A2A58", VA = "0x1012A2A58")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002066 RID: 8294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DDE")]
		[Address(RVA = "0x1012A2ABC", Offset = "0x12A2ABC", VA = "0x1012A2ABC")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002067 RID: 8295 RVA: 0x0000E3E8 File Offset: 0x0000C5E8
		[Token(Token = "0x6001DDF")]
		[Address(RVA = "0x1012A248C", Offset = "0x12A248C", VA = "0x1012A248C")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE0")]
		[Address(RVA = "0x1012A2CF0", Offset = "0x12A2CF0", VA = "0x1012A2CF0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002069 RID: 8297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE1")]
		[Address(RVA = "0x1012A2D48", Offset = "0x12A2D48", VA = "0x1012A2D48")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600206A RID: 8298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE2")]
		[Address(RVA = "0x1012A2E68", Offset = "0x12A2E68", VA = "0x1012A2E68")]
		private void OnTapQuestMapObj(QuestMapHandler.PointData pointData)
		{
		}

		// Token: 0x0600206B RID: 8299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE3")]
		[Address(RVA = "0x1012A2BDC", Offset = "0x12A2BDC", VA = "0x1012A2BDC")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04003067 RID: 12391
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024DE")]
		private QuestState_QuestMap.eStep m_Step;

		// Token: 0x04003068 RID: 12392
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024DF")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003069 RID: 12393
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024E0")]
		private QuestSelectUI m_UI;

		// Token: 0x0200080B RID: 2059
		[Token(Token = "0x2000ECB")]
		private enum eStep
		{
			// Token: 0x0400306B RID: 12395
			[Token(Token = "0x4005F00")]
			None = -1,
			// Token: 0x0400306C RID: 12396
			[Token(Token = "0x4005F01")]
			First,
			// Token: 0x0400306D RID: 12397
			[Token(Token = "0x4005F02")]
			LoadWait_0,
			// Token: 0x0400306E RID: 12398
			[Token(Token = "0x4005F03")]
			LoadWait_1,
			// Token: 0x0400306F RID: 12399
			[Token(Token = "0x4005F04")]
			PlayIn,
			// Token: 0x04003070 RID: 12400
			[Token(Token = "0x4005F05")]
			PlayIn_MapWait,
			// Token: 0x04003071 RID: 12401
			[Token(Token = "0x4005F06")]
			PlayIn_APIWait,
			// Token: 0x04003072 RID: 12402
			[Token(Token = "0x4005F07")]
			QuestNotice,
			// Token: 0x04003073 RID: 12403
			[Token(Token = "0x4005F08")]
			Unlock,
			// Token: 0x04003074 RID: 12404
			[Token(Token = "0x4005F09")]
			Main,
			// Token: 0x04003075 RID: 12405
			[Token(Token = "0x4005F0A")]
			UnloadChildSceneWait,
			// Token: 0x04003076 RID: 12406
			[Token(Token = "0x4005F0B")]
			PrepareError,
			// Token: 0x04003077 RID: 12407
			[Token(Token = "0x4005F0C")]
			PrepareErrorWait
		}
	}
}
