﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200080C RID: 2060
	[Token(Token = "0x2000611")]
	[StructLayout(3)]
	public class QuestState_QuestOfferSelect : QuestState
	{
		// Token: 0x0600206C RID: 8300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE4")]
		[Address(RVA = "0x1012A3134", Offset = "0x12A3134", VA = "0x1012A3134")]
		public QuestState_QuestOfferSelect(QuestMain owner)
		{
		}

		// Token: 0x0600206D RID: 8301 RVA: 0x0000E400 File Offset: 0x0000C600
		[Token(Token = "0x6001DE5")]
		[Address(RVA = "0x1012A3170", Offset = "0x12A3170", VA = "0x1012A3170", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600206E RID: 8302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE6")]
		[Address(RVA = "0x1012A3178", Offset = "0x12A3178", VA = "0x1012A3178", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600206F RID: 8303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE7")]
		[Address(RVA = "0x1012A3180", Offset = "0x12A3180", VA = "0x1012A3180", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002070 RID: 8304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DE8")]
		[Address(RVA = "0x1012A3184", Offset = "0x12A3184", VA = "0x1012A3184", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002071 RID: 8305 RVA: 0x0000E418 File Offset: 0x0000C618
		[Token(Token = "0x6001DE9")]
		[Address(RVA = "0x1012A318C", Offset = "0x12A318C", VA = "0x1012A318C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002072 RID: 8306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DEA")]
		[Address(RVA = "0x1012A3958", Offset = "0x12A3958", VA = "0x1012A3958")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x06002073 RID: 8307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DEB")]
		[Address(RVA = "0x1012A386C", Offset = "0x12A386C", VA = "0x1012A386C")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002074 RID: 8308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DEC")]
		[Address(RVA = "0x1012A38D0", Offset = "0x12A38D0", VA = "0x1012A38D0")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002075 RID: 8309 RVA: 0x0000E430 File Offset: 0x0000C630
		[Token(Token = "0x6001DED")]
		[Address(RVA = "0x1012A3500", Offset = "0x12A3500", VA = "0x1012A3500")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002076 RID: 8310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DEE")]
		[Address(RVA = "0x1012A3ABC", Offset = "0x12A3ABC", VA = "0x1012A3ABC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002077 RID: 8311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DEF")]
		[Address(RVA = "0x1012A3B20", Offset = "0x12A3B20", VA = "0x1012A3B20")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002078 RID: 8312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF0")]
		[Address(RVA = "0x1012A39EC", Offset = "0x12A39EC", VA = "0x1012A39EC")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04003078 RID: 12408
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024E1")]
		private QuestState_QuestOfferSelect.eStep m_Step;

		// Token: 0x04003079 RID: 12409
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024E2")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400307A RID: 12410
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024E3")]
		public QuestOfferSelectUI m_UI;

		// Token: 0x0200080D RID: 2061
		[Token(Token = "0x2000ECC")]
		private enum eStep
		{
			// Token: 0x0400307C RID: 12412
			[Token(Token = "0x4005F0E")]
			None = -1,
			// Token: 0x0400307D RID: 12413
			[Token(Token = "0x4005F0F")]
			First,
			// Token: 0x0400307E RID: 12414
			[Token(Token = "0x4005F10")]
			LoadWait,
			// Token: 0x0400307F RID: 12415
			[Token(Token = "0x4005F11")]
			PlayIn,
			// Token: 0x04003080 RID: 12416
			[Token(Token = "0x4005F12")]
			QuestNotice,
			// Token: 0x04003081 RID: 12417
			[Token(Token = "0x4005F13")]
			Unlock,
			// Token: 0x04003082 RID: 12418
			[Token(Token = "0x4005F14")]
			Main,
			// Token: 0x04003083 RID: 12419
			[Token(Token = "0x4005F15")]
			UnloadChildSceneWait
		}
	}
}
