﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200080E RID: 2062
	[Token(Token = "0x2000612")]
	[StructLayout(3)]
	public class QuestState_QuestSelect : QuestState
	{
		// Token: 0x06002079 RID: 8313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF1")]
		[Address(RVA = "0x1012A3C50", Offset = "0x12A3C50", VA = "0x1012A3C50")]
		public QuestState_QuestSelect(QuestMain owner)
		{
		}

		// Token: 0x0600207A RID: 8314 RVA: 0x0000E448 File Offset: 0x0000C648
		[Token(Token = "0x6001DF2")]
		[Address(RVA = "0x1012A3CF8", Offset = "0x12A3CF8", VA = "0x1012A3CF8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600207B RID: 8315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF3")]
		[Address(RVA = "0x1012A3D00", Offset = "0x12A3D00", VA = "0x1012A3D00", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600207C RID: 8316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF4")]
		[Address(RVA = "0x1012A3D08", Offset = "0x12A3D08", VA = "0x1012A3D08", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600207D RID: 8317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF5")]
		[Address(RVA = "0x1012A3D0C", Offset = "0x12A3D0C", VA = "0x1012A3D0C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600207E RID: 8318 RVA: 0x0000E460 File Offset: 0x0000C660
		[Token(Token = "0x6001DF6")]
		[Address(RVA = "0x1012A3D14", Offset = "0x12A3D14", VA = "0x1012A3D14", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600207F RID: 8319 RVA: 0x0000E478 File Offset: 0x0000C678
		[Token(Token = "0x6001DF7")]
		[Address(RVA = "0x1012A4988", Offset = "0x12A4988", VA = "0x1012A4988")]
		private QuestState_QuestSelect.eStep PlayPopup()
		{
			return QuestState_QuestSelect.eStep.First;
		}

		// Token: 0x06002080 RID: 8320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF8")]
		[Address(RVA = "0x1012A4CD0", Offset = "0x12A4CD0", VA = "0x1012A4CD0")]
		private void OnCompleteTipsWindow()
		{
		}

		// Token: 0x06002081 RID: 8321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DF9")]
		[Address(RVA = "0x1012A4CF4", Offset = "0x12A4CF4", VA = "0x1012A4CF4")]
		private void OnConfirmQuestNotice()
		{
		}

		// Token: 0x06002082 RID: 8322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DFA")]
		[Address(RVA = "0x1012A4B48", Offset = "0x12A4B48", VA = "0x1012A4B48")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002083 RID: 8323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DFB")]
		[Address(RVA = "0x1012A4BAC", Offset = "0x12A4BAC", VA = "0x1012A4BAC")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002084 RID: 8324 RVA: 0x0000E490 File Offset: 0x0000C690
		[Token(Token = "0x6001DFC")]
		[Address(RVA = "0x1012A4608", Offset = "0x12A4608", VA = "0x1012A4608")]
		public bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002085 RID: 8325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DFD")]
		[Address(RVA = "0x1012A4E38", Offset = "0x12A4E38", VA = "0x1012A4E38", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002086 RID: 8326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DFE")]
		[Address(RVA = "0x1012A50D0", Offset = "0x12A50D0", VA = "0x1012A50D0")]
		private void OnClickChapterButton()
		{
		}

		// Token: 0x06002087 RID: 8327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001DFF")]
		[Address(RVA = "0x1012A5100", Offset = "0x12A5100", VA = "0x1012A5100")]
		private void OnClickShopButton()
		{
		}

		// Token: 0x06002088 RID: 8328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E00")]
		[Address(RVA = "0x1012A5214", Offset = "0x12A5214", VA = "0x1012A5214")]
		private void GotoQuest()
		{
		}

		// Token: 0x06002089 RID: 8329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E01")]
		[Address(RVA = "0x1012A4D88", Offset = "0x12A4D88", VA = "0x1012A4D88")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x0600208A RID: 8330 RVA: 0x0000E4A8 File Offset: 0x0000C6A8
		[Token(Token = "0x6001E02")]
		[Address(RVA = "0x1012A5340", Offset = "0x12A5340", VA = "0x1012A5340")]
		private bool OnClickQuestButton()
		{
			return default(bool);
		}

		// Token: 0x0600208B RID: 8331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E03")]
		[Address(RVA = "0x1012A5448", Offset = "0x12A5448", VA = "0x1012A5448")]
		private void OnOrderSuccess()
		{
		}

		// Token: 0x0600208C RID: 8332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E04")]
		[Address(RVA = "0x1012A55A4", Offset = "0x12A55A4", VA = "0x1012A55A4")]
		private void OnOrderFailed(QuestDefine.eReturnLogAdd result)
		{
		}

		// Token: 0x04003084 RID: 12420
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40024E4")]
		private QuestState_QuestSelect.eStep m_Step;

		// Token: 0x04003085 RID: 12421
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024E5")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003086 RID: 12422
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024E6")]
		private QuestSelectUI m_UI;

		// Token: 0x04003087 RID: 12423
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024E7")]
		private QuestOrderProcess m_QuestOrderProcess;

		// Token: 0x04003088 RID: 12424
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40024E8")]
		private bool m_IsAdvOnly;

		// Token: 0x04003089 RID: 12425
		[Cpp2IlInjected.FieldOffset(Offset = "0x39")]
		[Token(Token = "0x40024E9")]
		private bool m_WasStartedPrepareQuestMap;

		// Token: 0x0400308A RID: 12426
		[Cpp2IlInjected.FieldOffset(Offset = "0x3A")]
		[Token(Token = "0x40024EA")]
		private bool m_IsVoicePlayed;

		// Token: 0x0200080F RID: 2063
		[Token(Token = "0x2000ECD")]
		private enum eStep
		{
			// Token: 0x0400308C RID: 12428
			[Token(Token = "0x4005F17")]
			None = -1,
			// Token: 0x0400308D RID: 12429
			[Token(Token = "0x4005F18")]
			First,
			// Token: 0x0400308E RID: 12430
			[Token(Token = "0x4005F19")]
			LoadWait_0,
			// Token: 0x0400308F RID: 12431
			[Token(Token = "0x4005F1A")]
			LoadWait_1,
			// Token: 0x04003090 RID: 12432
			[Token(Token = "0x4005F1B")]
			PlayIn,
			// Token: 0x04003091 RID: 12433
			[Token(Token = "0x4005F1C")]
			PopupWait,
			// Token: 0x04003092 RID: 12434
			[Token(Token = "0x4005F1D")]
			Unlock,
			// Token: 0x04003093 RID: 12435
			[Token(Token = "0x4005F1E")]
			Main,
			// Token: 0x04003094 RID: 12436
			[Token(Token = "0x4005F1F")]
			UnloadChildSceneWait,
			// Token: 0x04003095 RID: 12437
			[Token(Token = "0x4005F20")]
			PrepareError,
			// Token: 0x04003096 RID: 12438
			[Token(Token = "0x4005F21")]
			PrepareErrorWait
		}
	}
}
