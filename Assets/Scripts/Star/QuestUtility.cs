﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000939 RID: 2361
	[Token(Token = "0x20006AB")]
	[StructLayout(3)]
	public static class QuestUtility
	{
		// Token: 0x060027A4 RID: 10148 RVA: 0x00010DB8 File Offset: 0x0000EFB8
		[Token(Token = "0x600246E")]
		[Address(RVA = "0x1012A55D0", Offset = "0x12A55D0", VA = "0x1012A55D0")]
		public static TimeSpan CalcTimeSpan(QuestManager.EventData eventData)
		{
			return default(TimeSpan);
		}

		// Token: 0x060027A5 RID: 10149 RVA: 0x00010DD0 File Offset: 0x0000EFD0
		[Token(Token = "0x600246F")]
		[Address(RVA = "0x101298BB8", Offset = "0x1298BB8", VA = "0x101298BB8")]
		public static QuestDefine.eQuestLogAddResult CheckQuestLogAdd(QuestManager.QuestData questData, QuestManager.EventData eventData, long opt_partyMngID, QuestManager.StaminaReduction staminaReduction)
		{
			return QuestDefine.eQuestLogAddResult.Success;
		}

		// Token: 0x060027A6 RID: 10150 RVA: 0x00010DE8 File Offset: 0x0000EFE8
		[Token(Token = "0x6002470")]
		[Address(RVA = "0x1012A5678", Offset = "0x12A5678", VA = "0x1012A5678")]
		public static bool CheckExCondition(QuestManager.EventData eventData, long partyMngID)
		{
			return default(bool);
		}

		// Token: 0x060027A7 RID: 10151 RVA: 0x00010E00 File Offset: 0x0000F000
		[Token(Token = "0x6002471")]
		[Address(RVA = "0x1012A5B94", Offset = "0x12A5B94", VA = "0x1012A5B94")]
		public static QuestDefine.eGroupClearRank GetClearGroupRank(QuestManager.EventGroupData groupData)
		{
			return QuestDefine.eGroupClearRank.None;
		}

		// Token: 0x060027A8 RID: 10152 RVA: 0x00010E18 File Offset: 0x0000F018
		[Token(Token = "0x6002472")]
		[Address(RVA = "0x10129C178", Offset = "0x129C178", VA = "0x10129C178")]
		public static bool IsAvailableEventType(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060027A9 RID: 10153 RVA: 0x00010E30 File Offset: 0x0000F030
		[Token(Token = "0x6002473")]
		[Address(RVA = "0x1012A5CD4", Offset = "0x12A5CD4", VA = "0x1012A5CD4")]
		public static bool IsDisableEventType(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060027AA RID: 10154 RVA: 0x00010E48 File Offset: 0x0000F048
		[Token(Token = "0x6002474")]
		[Address(RVA = "0x1012A5CDC", Offset = "0x12A5CDC", VA = "0x1012A5CDC")]
		public static bool IsDefineEventType(int eventType)
		{
			return default(bool);
		}

		// Token: 0x060027AB RID: 10155 RVA: 0x00010E60 File Offset: 0x0000F060
		[Token(Token = "0x6002475")]
		[Address(RVA = "0x1012A5CE8", Offset = "0x12A5CE8", VA = "0x1012A5CE8")]
		public static int CheckQuestADVTrigger(eQuestADVTriggerType triggerType, int triggerID)
		{
			return 0;
		}

		// Token: 0x060027AC RID: 10156 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002476")]
		[Address(RVA = "0x1012A5DE4", Offset = "0x12A5DE4", VA = "0x1012A5DE4")]
		public static string CheckQuestOPTrigger(int triggerID)
		{
			return null;
		}

		// Token: 0x060027AD RID: 10157 RVA: 0x00010E78 File Offset: 0x0000F078
		[Token(Token = "0x6002477")]
		[Address(RVA = "0x1012A60D8", Offset = "0x12A60D8", VA = "0x1012A60D8")]
		public static bool IsAllNewQuest(int groupID)
		{
			return default(bool);
		}

		// Token: 0x060027AE RID: 10158 RVA: 0x00010E90 File Offset: 0x0000F090
		[Token(Token = "0x6002478")]
		[Address(RVA = "0x1012A6240", Offset = "0x12A6240", VA = "0x1012A6240")]
		public static bool IsExistNewQuest(int groupID)
		{
			return default(bool);
		}

		// Token: 0x060027AF RID: 10159 RVA: 0x00010EA8 File Offset: 0x0000F0A8
		[Token(Token = "0x6002479")]
		[Address(RVA = "0x1012A63A8", Offset = "0x12A63A8", VA = "0x1012A63A8")]
		public static bool IsExistNewCharaQuest(List<QuestManager.CharaQuestData> list)
		{
			return default(bool);
		}

		// Token: 0x060027B0 RID: 10160 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247A")]
		[Address(RVA = "0x1012A64D0", Offset = "0x12A64D0", VA = "0x1012A64D0")]
		public static List<QuestManager.CharaQuestData> GetUnlockedCharaQuestDatas(List<QuestManager.CharaQuestData> src)
		{
			return null;
		}

		// Token: 0x060027B1 RID: 10161 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247B")]
		[Address(RVA = "0x1012A66E4", Offset = "0x12A66E4", VA = "0x1012A66E4")]
		public static Dictionary<eTitleType, List<QuestManager.CharaQuestData>> GetCharaQuestDatasPerTitle()
		{
			return null;
		}

		// Token: 0x060027B2 RID: 10162 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247C")]
		[Address(RVA = "0x1012A6A04", Offset = "0x12A6A04", VA = "0x1012A6A04")]
		public static Dictionary<int, List<QuestManager.CharaQuestData>> GetCharaQuestDatasPerCharaID()
		{
			return null;
		}

		// Token: 0x060027B3 RID: 10163 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247D")]
		[Address(RVA = "0x1012A6C68", Offset = "0x12A6C68", VA = "0x1012A6C68")]
		public static List<QuestManager.CharaQuestData> GetCharaQuestDatasByCharaID(int charaID)
		{
			return null;
		}

		// Token: 0x060027B4 RID: 10164 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247E")]
		[Address(RVA = "0x1012A6D20", Offset = "0x12A6D20", VA = "0x1012A6D20")]
		public static List<eTitleType> GetCharaQuestSortedKey(Dictionary<eTitleType, List<QuestManager.CharaQuestData>> src)
		{
			return null;
		}

		// Token: 0x060027B5 RID: 10165 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600247F")]
		[Address(RVA = "0x1012A70D0", Offset = "0x12A70D0", VA = "0x1012A70D0")]
		public static List<int> GetCharaQuestSortedKey(Dictionary<int, List<QuestManager.CharaQuestData>> src)
		{
			return null;
		}

		// Token: 0x060027B6 RID: 10166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002480")]
		[Address(RVA = "0x1012A1AD8", Offset = "0x12A1AD8", VA = "0x1012A1AD8")]
		public static void SetupSelectQuestChapterFirstView(QuestManager.SelectQuest selectQuest, int part)
		{
		}

		// Token: 0x060027B7 RID: 10167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002481")]
		[Address(RVA = "0x1012A73F0", Offset = "0x12A73F0", VA = "0x1012A73F0")]
		public static void SetupSelectQuestFromQuestID(QuestManager.SelectQuest selectQuest, int questID)
		{
		}

		// Token: 0x060027B8 RID: 10168 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002482")]
		[Address(RVA = "0x10129B0B8", Offset = "0x129B0B8", VA = "0x10129B0B8")]
		public static QuestManager.SelectQuest NextSelectQuest(QuestManager.SelectQuest selectQuest)
		{
			return null;
		}

		// Token: 0x060027B9 RID: 10169 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002483")]
		[Address(RVA = "0x1012A776C", Offset = "0x12A776C", VA = "0x1012A776C")]
		private static QuestManager.SelectQuest CalcNextQuestChapter(QuestManager.SelectQuest selectQuest)
		{
			return null;
		}

		// Token: 0x060027BA RID: 10170 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002484")]
		[Address(RVA = "0x1012A79C8", Offset = "0x12A79C8", VA = "0x1012A79C8")]
		private static QuestManager.SelectQuest CalcNextQuestEvent(QuestManager.SelectQuest selectQuest)
		{
			return null;
		}

		// Token: 0x060027BB RID: 10171 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002485")]
		[Address(RVA = "0x1012A80F8", Offset = "0x12A80F8", VA = "0x1012A80F8")]
		private static QuestManager.SelectQuest CalcNextQuestChara(QuestManager.SelectQuest selectQuest)
		{
			return null;
		}

		// Token: 0x060027BC RID: 10172 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002486")]
		[Address(RVA = "0x1012A8258", Offset = "0x12A8258", VA = "0x1012A8258")]
		public static string GetHelpURL(int eventType)
		{
			return null;
		}

		// Token: 0x060027BD RID: 10173 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002487")]
		[Address(RVA = "0x1012A8370", Offset = "0x12A8370", VA = "0x1012A8370")]
		public static string GetHelpURLFromGroupID(int groupID)
		{
			return null;
		}

		// Token: 0x060027BE RID: 10174 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002488")]
		[Address(RVA = "0x1012A0D40", Offset = "0x12A0D40", VA = "0x1012A0D40")]
		public static string GetHelpURLIfNotOccurred(int eventType)
		{
			return null;
		}

		// Token: 0x060027BF RID: 10175 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002489")]
		[Address(RVA = "0x1012A4C34", Offset = "0x12A4C34", VA = "0x1012A4C34")]
		public static string GetHelpURLFromGroupIDIfNotOccurred(int eventGroup)
		{
			return null;
		}

		// Token: 0x060027C0 RID: 10176 RVA: 0x00010EC0 File Offset: 0x0000F0C0
		[Token(Token = "0x600248A")]
		[Address(RVA = "0x1012A07F4", Offset = "0x12A07F4", VA = "0x1012A07F4")]
		public static long GetPointEventID(int eventType)
		{
			return 0L;
		}

		// Token: 0x060027C1 RID: 10177 RVA: 0x00010ED8 File Offset: 0x0000F0D8
		[Token(Token = "0x600248B")]
		[Address(RVA = "0x101298EAC", Offset = "0x1298EAC", VA = "0x101298EAC")]
		public static int CalcReducedStamina(int rawStamina)
		{
			return 0;
		}

		// Token: 0x060027C2 RID: 10178 RVA: 0x00010EF0 File Offset: 0x0000F0F0
		[Token(Token = "0x600248C")]
		[Address(RVA = "0x101291ADC", Offset = "0x1291ADC", VA = "0x101291ADC")]
		public static bool IsAnyChapterCategory(this eQuestCategory self)
		{
			return default(bool);
		}
	}
}
