﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000584 RID: 1412
	[Token(Token = "0x2000477")]
	[StructLayout(3)]
	public class QuestWaveDropsDB : ScriptableObject
	{
		// Token: 0x060015E9 RID: 5609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001499")]
		[Address(RVA = "0x1012A847C", Offset = "0x12A847C", VA = "0x1012A847C")]
		public QuestWaveDropsDB()
		{
		}

		// Token: 0x04001A0B RID: 6667
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001375")]
		public QuestWaveDropsDB_Param[] m_Params;
	}
}
