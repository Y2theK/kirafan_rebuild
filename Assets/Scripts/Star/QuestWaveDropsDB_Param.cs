﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000583 RID: 1411
	[Token(Token = "0x2000476")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct QuestWaveDropsDB_Param
	{
		// Token: 0x04001A06 RID: 6662
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001370")]
		public long m_ID;

		// Token: 0x04001A07 RID: 6663
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001371")]
		public int m_DropID;

		// Token: 0x04001A08 RID: 6664
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001372")]
		public int m_DropItemID;

		// Token: 0x04001A09 RID: 6665
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001373")]
		public int m_DropItemNum;

		// Token: 0x04001A0A RID: 6666
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001374")]
		public float m_DropProbability;
	}
}
