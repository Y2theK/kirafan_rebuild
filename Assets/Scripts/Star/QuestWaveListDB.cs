﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000586 RID: 1414
	[Token(Token = "0x2000479")]
	[StructLayout(3)]
	public class QuestWaveListDB : ScriptableObject
	{
		// Token: 0x060015EA RID: 5610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149A")]
		[Address(RVA = "0x1012A8484", Offset = "0x12A8484", VA = "0x1012A8484")]
		public QuestWaveListDB()
		{
		}

		// Token: 0x04001A12 RID: 6674
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400137C")]
		public QuestWaveListDB_Param[] m_Params;
	}
}
