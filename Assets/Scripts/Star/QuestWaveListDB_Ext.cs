﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000522 RID: 1314
	[Token(Token = "0x2000418")]
	[StructLayout(3)]
	public static class QuestWaveListDB_Ext
	{
		// Token: 0x06001581 RID: 5505 RVA: 0x000095A0 File Offset: 0x000077A0
		[Token(Token = "0x6001436")]
		[Address(RVA = "0x1012A848C", Offset = "0x12A848C", VA = "0x1012A848C")]
		public static QuestWaveListDB_Param GetParam(this QuestWaveListDB self, int waveID)
		{
			return default(QuestWaveListDB_Param);
		}
	}
}
