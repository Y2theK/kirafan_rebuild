﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000585 RID: 1413
	[Token(Token = "0x2000478")]
	[Serializable]
	[StructLayout(0, Size = 48)]
	public struct QuestWaveListDB_Param
	{
		// Token: 0x04001A0C RID: 6668
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001376")]
		public int m_ID;

		// Token: 0x04001A0D RID: 6669
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001377")]
		public int[] m_QuestEnemyIDs;

		// Token: 0x04001A0E RID: 6670
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001378")]
		public int[] m_QuestRandomIDs;

		// Token: 0x04001A0F RID: 6671
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001379")]
		public int[] m_QuestEnemyLvs;

		// Token: 0x04001A10 RID: 6672
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400137A")]
		public int[] m_DropIDs;

		// Token: 0x04001A11 RID: 6673
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400137B")]
		public float[] m_DispScales;
	}
}
