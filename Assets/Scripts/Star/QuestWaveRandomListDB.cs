﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000588 RID: 1416
	[Token(Token = "0x200047B")]
	[StructLayout(3)]
	public class QuestWaveRandomListDB : ScriptableObject
	{
		// Token: 0x060015EB RID: 5611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149B")]
		[Address(RVA = "0x1012A8594", Offset = "0x12A8594", VA = "0x1012A8594")]
		public QuestWaveRandomListDB()
		{
		}

		// Token: 0x04001A1B RID: 6683
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001385")]
		public QuestWaveRandomListDB_Param[] m_Params;
	}
}
