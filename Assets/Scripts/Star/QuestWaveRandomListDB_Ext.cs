﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000523 RID: 1315
	[Token(Token = "0x2000419")]
	[StructLayout(3)]
	public static class QuestWaveRandomListDB_Ext
	{
		// Token: 0x06001582 RID: 5506 RVA: 0x000095B8 File Offset: 0x000077B8
		[Token(Token = "0x6001437")]
		[Address(RVA = "0x1012A859C", Offset = "0x12A859C", VA = "0x1012A859C")]
		public static QuestWaveRandomListDB_Param GetParam(this QuestWaveRandomListDB self, int randomID)
		{
			return default(QuestWaveRandomListDB_Param);
		}
	}
}
