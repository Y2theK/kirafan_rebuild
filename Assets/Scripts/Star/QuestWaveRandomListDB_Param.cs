﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000587 RID: 1415
	[Token(Token = "0x200047A")]
	[Serializable]
	[StructLayout(0, Size = 56)]
	public struct QuestWaveRandomListDB_Param
	{
		// Token: 0x04001A13 RID: 6675
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400137D")]
		public int m_ID;

		// Token: 0x04001A14 RID: 6676
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400137E")]
		public int m_Num;

		// Token: 0x04001A15 RID: 6677
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400137F")]
		public float[] m_Prob;

		// Token: 0x04001A16 RID: 6678
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001380")]
		public int[] m_QuestEnemyIDs;

		// Token: 0x04001A17 RID: 6679
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001381")]
		public int[] m_QuestRandomIDs;

		// Token: 0x04001A18 RID: 6680
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001382")]
		public int[] m_QuestEnemyLvs;

		// Token: 0x04001A19 RID: 6681
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001383")]
		public int[] m_DropIDs;

		// Token: 0x04001A1A RID: 6682
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001384")]
		public float[] m_DispScales;
	}
}
