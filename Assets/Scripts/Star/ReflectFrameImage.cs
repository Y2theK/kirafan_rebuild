﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000A90 RID: 2704
	[Token(Token = "0x200077F")]
	[StructLayout(3)]
	public class ReflectFrameImage : MonoBehaviour
	{
		// Token: 0x06002E36 RID: 11830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A4D")]
		[Address(RVA = "0x1012A86B0", Offset = "0x12A86B0", VA = "0x1012A86B0")]
		private void Awake()
		{
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06002E37 RID: 11831 RVA: 0x00013AB8 File Offset: 0x00011CB8
		[Token(Token = "0x170002E6")]
		public bool isActive
		{
			[Token(Token = "0x6002A4E")]
			[Address(RVA = "0x1012A88C8", Offset = "0x12A88C8", VA = "0x1012A88C8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002E38 RID: 11832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A4F")]
		[Address(RVA = "0x1012A8820", Offset = "0x12A8820", VA = "0x1012A8820")]
		public void Activate(bool flg)
		{
		}

		// Token: 0x06002E39 RID: 11833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A50")]
		[Address(RVA = "0x1012A88D0", Offset = "0x12A88D0", VA = "0x1012A88D0")]
		public ReflectFrameImage()
		{
		}

		// Token: 0x04003E14 RID: 15892
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002CCB")]
		public FakeReflectionCtrlBank.eType m_ReflectionType;

		// Token: 0x04003E15 RID: 15893
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002CCC")]
		private FakeReflectionCtrl m_FakeReflectionCtrlCache;

		// Token: 0x04003E16 RID: 15894
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CCD")]
		private Image m_Image;

		// Token: 0x04003E17 RID: 15895
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CCE")]
		private bool m_Active;
	}
}
