﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006C3 RID: 1731
	[Token(Token = "0x200057A")]
	[StructLayout(3)]
	public class ResourceFileReader : BinaryReader
	{
		// Token: 0x0600195B RID: 6491 RVA: 0x0000B7A8 File Offset: 0x000099A8
		[Token(Token = "0x60017D7")]
		[Address(RVA = "0x1012A88D8", Offset = "0x12A88D8", VA = "0x1012A88D8")]
		public bool OpenFile(string filename)
		{
			return default(bool);
		}

		// Token: 0x0600195C RID: 6492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017D8")]
		[Address(RVA = "0x1012A8A10", Offset = "0x12A8A10", VA = "0x1012A8A10")]
		public void ReadFile()
		{
		}

		// Token: 0x0600195D RID: 6493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017D9")]
		[Address(RVA = "0x1012A8A14", Offset = "0x12A8A14", VA = "0x1012A8A14")]
		public void CloseFile()
		{
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017DA")]
		[Address(RVA = "0x1012A8A1C", Offset = "0x12A8A1C", VA = "0x1012A8A1C")]
		public ResourceFileReader()
		{
		}
	}
}
