﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008C2 RID: 2242
	[Token(Token = "0x200066C")]
	[StructLayout(3)]
	public class ResourceObjectHandler
	{
		// Token: 0x17000284 RID: 644
		// (get) Token: 0x060024EA RID: 9450 RVA: 0x0000FD80 File Offset: 0x0000DF80
		// (set) Token: 0x060024EB RID: 9451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700025D")]
		public ResourceObjectHandler.eLoadType LoadType
		{
			[Token(Token = "0x600220E")]
			[Address(RVA = "0x1012A8A24", Offset = "0x12A8A24", VA = "0x1012A8A24")]
			[CompilerGenerated]
			get
			{
				return ResourceObjectHandler.eLoadType.GameObject;
			}
			[Token(Token = "0x600220F")]
			[Address(RVA = "0x1012A8A2C", Offset = "0x12A8A2C", VA = "0x1012A8A2C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x060024EC RID: 9452 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060024ED RID: 9453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700025E")]
		public UnityEngine.Object Obj
		{
			[Token(Token = "0x6002210")]
			[Address(RVA = "0x1012A8A34", Offset = "0x12A8A34", VA = "0x1012A8A34")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002211")]
			[Address(RVA = "0x1012A8A3C", Offset = "0x12A8A3C", VA = "0x1012A8A3C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x060024EE RID: 9454 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060024EF RID: 9455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700025F")]
		public ResourceRequest Request
		{
			[Token(Token = "0x6002212")]
			[Address(RVA = "0x1012A8A44", Offset = "0x12A8A44", VA = "0x1012A8A44")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002213")]
			[Address(RVA = "0x1012A8A4C", Offset = "0x12A8A4C", VA = "0x1012A8A4C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x060024F0 RID: 9456 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060024F1 RID: 9457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000260")]
		public string Path
		{
			[Token(Token = "0x6002214")]
			[Address(RVA = "0x1012A8A54", Offset = "0x12A8A54", VA = "0x1012A8A54")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002215")]
			[Address(RVA = "0x1012A8A5C", Offset = "0x12A8A5C", VA = "0x1012A8A5C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x060024F2 RID: 9458 RVA: 0x0000FD98 File Offset: 0x0000DF98
		// (set) Token: 0x060024F3 RID: 9459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000261")]
		public int RefCount
		{
			[Token(Token = "0x6002216")]
			[Address(RVA = "0x1012A8A64", Offset = "0x12A8A64", VA = "0x1012A8A64")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6002217")]
			[Address(RVA = "0x1012A8A6C", Offset = "0x12A8A6C", VA = "0x1012A8A6C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x060024F4 RID: 9460 RVA: 0x0000FDB0 File Offset: 0x0000DFB0
		[Token(Token = "0x6002218")]
		[Address(RVA = "0x1012A8A74", Offset = "0x12A8A74", VA = "0x1012A8A74")]
		public int AddRef()
		{
			return 0;
		}

		// Token: 0x060024F5 RID: 9461 RVA: 0x0000FDC8 File Offset: 0x0000DFC8
		[Token(Token = "0x6002219")]
		[Address(RVA = "0x1012A8A88", Offset = "0x12A8A88", VA = "0x1012A8A88")]
		public int RemoveRef()
		{
			return 0;
		}

		// Token: 0x060024F6 RID: 9462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600221A")]
		[Address(RVA = "0x1012A8A9C", Offset = "0x12A8A9C", VA = "0x1012A8A9C")]
		public ResourceObjectHandler(ResourceRequest request, string path, ResourceObjectHandler.eLoadType loadType)
		{
		}

		// Token: 0x060024F7 RID: 9463 RVA: 0x0000FDE0 File Offset: 0x0000DFE0
		[Token(Token = "0x600221B")]
		[Address(RVA = "0x1012A8AE4", Offset = "0x12A8AE4", VA = "0x1012A8AE4")]
		public bool IsDone()
		{
			return default(bool);
		}

		// Token: 0x060024F8 RID: 9464 RVA: 0x0000FDF8 File Offset: 0x0000DFF8
		[Token(Token = "0x600221C")]
		[Address(RVA = "0x1012A8B70", Offset = "0x12A8B70", VA = "0x1012A8B70")]
		public bool ApplyObj()
		{
			return default(bool);
		}

		// Token: 0x060024F9 RID: 9465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600221D")]
		[Address(RVA = "0x1012A8C44", Offset = "0x12A8C44", VA = "0x1012A8C44")]
		public void DestroyObj()
		{
		}

		// Token: 0x020008C3 RID: 2243
		[Token(Token = "0x2000F26")]
		public enum eLoadType
		{
			// Token: 0x040034DB RID: 13531
			[Token(Token = "0x40061A3")]
			GameObject,
			// Token: 0x040034DC RID: 13532
			[Token(Token = "0x40061A4")]
			Sprite,
			// Token: 0x040034DD RID: 13533
			[Token(Token = "0x40061A5")]
			ScriptableObject
		}
	}
}
