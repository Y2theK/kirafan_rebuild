﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008C1 RID: 2241
	[Token(Token = "0x200066B")]
	[StructLayout(3)]
	public class ResourcesLoader
	{
		// Token: 0x060024E1 RID: 9441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002205")]
		[Address(RVA = "0x1012A8C4C", Offset = "0x12A8C4C", VA = "0x1012A8C4C")]
		public ResourcesLoader(int simultaneouslyNum)
		{
		}

		// Token: 0x060024E2 RID: 9442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002206")]
		[Address(RVA = "0x1012A8D30", Offset = "0x12A8D30", VA = "0x1012A8D30")]
		public void Update()
		{
		}

		// Token: 0x060024E3 RID: 9443 RVA: 0x0000FD38 File Offset: 0x0000DF38
		[Token(Token = "0x6002207")]
		[Address(RVA = "0x1012A97C0", Offset = "0x12A97C0", VA = "0x1012A97C0")]
		public bool IsLoadingResources()
		{
			return default(bool);
		}

		// Token: 0x060024E4 RID: 9444 RVA: 0x0000FD50 File Offset: 0x0000DF50
		[Token(Token = "0x6002208")]
		[Address(RVA = "0x1012A9860", Offset = "0x12A9860", VA = "0x1012A9860")]
		public bool IsLoadedResources()
		{
			return default(bool);
		}

		// Token: 0x060024E5 RID: 9445 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002209")]
		[Address(RVA = "0x1012A98CC", Offset = "0x12A98CC", VA = "0x1012A98CC")]
		public ResourceObjectHandler LoadAsyncFromResources(string path, ResourceObjectHandler.eLoadType loadType = ResourceObjectHandler.eLoadType.GameObject)
		{
			return null;
		}

		// Token: 0x060024E6 RID: 9446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600220A")]
		[Address(RVA = "0x1012A9A44", Offset = "0x12A9A44", VA = "0x1012A9A44")]
		public void UnloadFromResources(ResourceObjectHandler unloadHndl)
		{
		}

		// Token: 0x060024E7 RID: 9447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600220B")]
		[Address(RVA = "0x1012A9B5C", Offset = "0x12A9B5C", VA = "0x1012A9B5C")]
		public void UnloadFromResources(string path)
		{
		}

		// Token: 0x060024E8 RID: 9448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600220C")]
		[Address(RVA = "0x1012A9C50", Offset = "0x12A9C50", VA = "0x1012A9C50")]
		public void UnloadAllResources()
		{
		}

		// Token: 0x060024E9 RID: 9449 RVA: 0x0000FD68 File Offset: 0x0000DF68
		[Token(Token = "0x600220D")]
		[Address(RVA = "0x1012A9C68", Offset = "0x12A9C68", VA = "0x1012A9C68")]
		public bool IsCompleteUnloadAllResources()
		{
			return default(bool);
		}

		// Token: 0x040034CF RID: 13519
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40026A0")]
		private Dictionary<string, ResourceObjectHandler> m_LoadedResources;

		// Token: 0x040034D0 RID: 13520
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40026A1")]
		private Dictionary<string, ResourceObjectHandler> m_LoadingResources;

		// Token: 0x040034D1 RID: 13521
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40026A2")]
		private Dictionary<string, ResourceObjectHandler> m_ReserveResources;

		// Token: 0x040034D2 RID: 13522
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40026A3")]
		private int m_SimultaneouslyNum;

		// Token: 0x040034D3 RID: 13523
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40026A4")]
		private bool m_UnloadAllFlg;

		// Token: 0x040034D4 RID: 13524
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40026A5")]
		private List<string> m_RemoveKeys;
	}
}
