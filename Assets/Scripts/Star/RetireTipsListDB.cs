﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200058C RID: 1420
	[Token(Token = "0x200047F")]
	[StructLayout(3)]
	public class RetireTipsListDB : ScriptableObject
	{
		// Token: 0x060015EC RID: 5612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149C")]
		[Address(RVA = "0x1012A9C78", Offset = "0x12A9C78", VA = "0x1012A9C78")]
		public RetireTipsListDB()
		{
		}

		// Token: 0x04001A2A RID: 6698
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001394")]
		public RetireTipsListDB_Param[] m_Params;
	}
}
