﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200058A RID: 1418
	[Token(Token = "0x200047D")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct RetireTipsListDB_Data
	{
		// Token: 0x04001A25 RID: 6693
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400138F")]
		public int m_ImageID;

		// Token: 0x04001A26 RID: 6694
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001390")]
		public string m_Title;

		// Token: 0x04001A27 RID: 6695
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001391")]
		public string m_Text;
	}
}
