﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000524 RID: 1316
	[Token(Token = "0x200041A")]
	[StructLayout(3)]
	public static class RetireTipsListDB_Ext
	{
		// Token: 0x06001583 RID: 5507 RVA: 0x000095D0 File Offset: 0x000077D0
		[Token(Token = "0x6001438")]
		[Address(RVA = "0x1012A9C80", Offset = "0x12A9C80", VA = "0x1012A9C80")]
		public static RetireTipsListDB_Param GetParam(this RetireTipsListDB self, eRetireTipsListDB id)
		{
			return default(RetireTipsListDB_Param);
		}
	}
}
