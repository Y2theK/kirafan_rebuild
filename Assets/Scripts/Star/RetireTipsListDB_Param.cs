﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200058B RID: 1419
	[Token(Token = "0x200047E")]
	[Serializable]
	[StructLayout(0)]
	public struct RetireTipsListDB_Param
	{
		// Token: 0x04001A28 RID: 6696
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001392")]
		public int m_ID;

		// Token: 0x04001A29 RID: 6697
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001393")]
		public RetireTipsListDB_Data[] m_Datas;
	}
}
