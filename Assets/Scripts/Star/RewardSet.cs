﻿namespace Star {
    public class RewardSet {
        public ePresentType m_Type;
        public int m_ID = -1;
        public int m_Amount;
        public string m_Name;

        public RewardSet() { }

        public RewardSet(ePresentType type, int id, int amount, string name = null) {
            m_Type = type;
            m_ID = id;
            m_Amount = amount;
            m_Name = name;
        }
    }
}
