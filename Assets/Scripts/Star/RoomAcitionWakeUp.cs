﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A4E RID: 2638
	[Token(Token = "0x200075B")]
	[StructLayout(3)]
	public class RoomAcitionWakeUp
	{
		// Token: 0x06002D71 RID: 11633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029CE")]
		[Address(RVA = "0x1012A9DD0", Offset = "0x12A9DD0", VA = "0x1012A9DD0")]
		public RoomAcitionWakeUp()
		{
		}

		// Token: 0x04003CFD RID: 15613
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BFA")]
		public RoomCharaActMode.eMode m_Key;

		// Token: 0x04003CFE RID: 15614
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BFB")]
		public RoomWakeUpKey[] m_Table;
	}
}
