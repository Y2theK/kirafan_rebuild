﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000944 RID: 2372
	[Token(Token = "0x20006B5")]
	[StructLayout(3)]
	public class RoomActionCommand
	{
		// Token: 0x060027EF RID: 10223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024B9")]
		[Address(RVA = "0x1012A9DD8", Offset = "0x12A9DD8", VA = "0x1012A9DD8")]
		public RoomActionCommand()
		{
		}

		// Token: 0x040037DE RID: 14302
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002870")]
		public RoomActionCommand.eActionCode m_Command;

		// Token: 0x040037DF RID: 14303
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002871")]
		public int m_ParamI1;

		// Token: 0x040037E0 RID: 14304
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002872")]
		public int m_ParamI2;

		// Token: 0x040037E1 RID: 14305
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002873")]
		public int m_ParamI3;

		// Token: 0x040037E2 RID: 14306
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002874")]
		public int m_ParamI4;

		// Token: 0x040037E3 RID: 14307
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002875")]
		public Vector4 m_ParamF;

		// Token: 0x040037E4 RID: 14308
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002876")]
		public string m_ParamName;

		// Token: 0x040037E5 RID: 14309
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002877")]
		public bool m_Lock;

		// Token: 0x02000945 RID: 2373
		[Token(Token = "0x2000F5F")]
		public enum eActionCode
		{
			// Token: 0x040037E7 RID: 14311
			[Token(Token = "0x40062E2")]
			Non,
			// Token: 0x040037E8 RID: 14312
			[Token(Token = "0x40062E3")]
			Stop,
			// Token: 0x040037E9 RID: 14313
			[Token(Token = "0x40062E4")]
			Walk,
			// Token: 0x040037EA RID: 14314
			[Token(Token = "0x40062E5")]
			Jump,
			// Token: 0x040037EB RID: 14315
			[Token(Token = "0x40062E6")]
			Action,
			// Token: 0x040037EC RID: 14316
			[Token(Token = "0x40062E7")]
			Event,
			// Token: 0x040037ED RID: 14317
			[Token(Token = "0x40062E8")]
			Erase,
			// Token: 0x040037EE RID: 14318
			[Token(Token = "0x40062E9")]
			Sleep,
			// Token: 0x040037EF RID: 14319
			[Token(Token = "0x40062EA")]
			Pic
		}

		// Token: 0x02000946 RID: 2374
		[Token(Token = "0x2000F60")]
		public enum eTweetAction
		{
			// Token: 0x040037F1 RID: 14321
			[Token(Token = "0x40062EC")]
			Idle,
			// Token: 0x040037F2 RID: 14322
			[Token(Token = "0x40062ED")]
			Enjoy,
			// Token: 0x040037F3 RID: 14323
			[Token(Token = "0x40062EE")]
			Tweet,
			// Token: 0x040037F4 RID: 14324
			[Token(Token = "0x40062EF")]
			Rejoice
		}
	}
}
