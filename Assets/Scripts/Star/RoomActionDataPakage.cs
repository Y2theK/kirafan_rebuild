﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000948 RID: 2376
	[Token(Token = "0x20006B7")]
	[Serializable]
	[StructLayout(3)]
	public class RoomActionDataPakage : IDataBaseResource
	{
		// Token: 0x060027F2 RID: 10226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024BC")]
		[Address(RVA = "0x1012A9EEC", Offset = "0x12A9EEC", VA = "0x1012A9EEC")]
		public static void ReadPakage()
		{
		}

		// Token: 0x060027F3 RID: 10227 RVA: 0x000110A0 File Offset: 0x0000F2A0
		[Token(Token = "0x60024BD")]
		[Address(RVA = "0x1012A9FA8", Offset = "0x12A9FA8", VA = "0x1012A9FA8")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x060027F4 RID: 10228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024BE")]
		[Address(RVA = "0x1012AA008", Offset = "0x12AA008", VA = "0x1012AA008")]
		public static void DataRelease()
		{
		}

		// Token: 0x060027F5 RID: 10229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024BF")]
		[Address(RVA = "0x1012AA070", Offset = "0x12AA070", VA = "0x1012AA070", Slot = "4")]
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
		}

		// Token: 0x060027F6 RID: 10230 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60024C0")]
		[Address(RVA = "0x1012AA69C", Offset = "0x12AA69C", VA = "0x1012AA69C")]
		public static RoomActionScriptDB GetActionScript(int faccesskey)
		{
			return null;
		}

		// Token: 0x060027F7 RID: 10231 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60024C1")]
		[Address(RVA = "0x1012AA7F4", Offset = "0x12AA7F4", VA = "0x1012AA7F4")]
		public static RoomActionScriptDB[] GetAllActionScript()
		{
			return null;
		}

		// Token: 0x060027F8 RID: 10232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C2")]
		[Address(RVA = "0x1012AA464", Offset = "0x12AA464", VA = "0x1012AA464")]
		private static void DataMakeToActionScript(RoomActionScriptDB pdat, ActXlsKeyTag pfile)
		{
		}

		// Token: 0x060027F9 RID: 10233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60024C3")]
		[Address(RVA = "0x1012A9FA0", Offset = "0x12A9FA0", VA = "0x1012A9FA0")]
		public RoomActionDataPakage()
		{
		}

		// Token: 0x040037F5 RID: 14325
		[Token(Token = "0x4002878")]
		private static RoomActionDataPakage ms_Pakage;

		// Token: 0x040037F6 RID: 14326
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002879")]
		private RoomActionScriptDB[] m_TagTable;

		// Token: 0x02000949 RID: 2377
		[Token(Token = "0x2000F61")]
		[StructLayout(2)]
		public struct RoomActionDataHeader
		{
			// Token: 0x040037F7 RID: 14327
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40062F0")]
			public int m_Ver;

			// Token: 0x040037F8 RID: 14328
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40062F1")]
			public int m_Num;

			// Token: 0x040037F9 RID: 14329
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40062F2")]
			public int m_Dummy1;

			// Token: 0x040037FA RID: 14330
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x40062F3")]
			public int m_Dummy2;
		}

		// Token: 0x0200094A RID: 2378
		[Token(Token = "0x2000F62")]
		[StructLayout(2)]
		public struct RoomActionTagHeader
		{
			// Token: 0x040037FB RID: 14331
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40062F4")]
			public int m_KeyID;

			// Token: 0x040037FC RID: 14332
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40062F5")]
			public int m_Size;
		}
	}
}
