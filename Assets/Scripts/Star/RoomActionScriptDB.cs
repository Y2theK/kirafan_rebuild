﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000971 RID: 2417
	[Token(Token = "0x20006D8")]
	[StructLayout(3)]
	public class RoomActionScriptDB
	{
		// Token: 0x0600283A RID: 10298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002504")]
		[Address(RVA = "0x1012AA45C", Offset = "0x12AA45C", VA = "0x1012AA45C")]
		public RoomActionScriptDB()
		{
		}

		// Token: 0x04003895 RID: 14485
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40028EF")]
		public string m_Name;

		// Token: 0x04003896 RID: 14486
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028F0")]
		public int m_AccessKey;

		// Token: 0x04003897 RID: 14487
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028F1")]
		public int m_LinkDir;

		// Token: 0x04003898 RID: 14488
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028F2")]
		public IRoomScriptData[] m_Table;
	}
}
