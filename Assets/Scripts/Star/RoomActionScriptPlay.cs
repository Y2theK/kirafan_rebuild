﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000972 RID: 2418
	[Token(Token = "0x20006D9")]
	[StructLayout(3)]
	public class RoomActionScriptPlay
	{
		// Token: 0x0600283B RID: 10299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002505")]
		[Address(RVA = "0x1012AA854", Offset = "0x12AA854", VA = "0x1012AA854")]
		public void SetUpPlayScene(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, int fscriptno, RoomActionScriptPlay.ActionSciptCallBack pcallback, RoomPartsActionPakage ppartsplay)
		{
		}

		// Token: 0x0600283C RID: 10300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002506")]
		[Address(RVA = "0x1012AAA28", Offset = "0x12AAA28", VA = "0x1012AAA28")]
		public void SetUpPlayScene(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup, RoomActionScriptPlay.ActionSciptCallBack pcallback, RoomPartsActionPakage ppartsplay)
		{
		}

		// Token: 0x0600283D RID: 10301 RVA: 0x000110B8 File Offset: 0x0000F2B8
		[Token(Token = "0x6002507")]
		[Address(RVA = "0x1012AAFE0", Offset = "0x12AAFE0", VA = "0x1012AAFE0")]
		public bool PlayScene(RoomObjectCtrlChara hbase)
		{
			return default(bool);
		}

		// Token: 0x0600283E RID: 10302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002508")]
		[Address(RVA = "0x1012AE5BC", Offset = "0x12AE5BC", VA = "0x1012AE5BC")]
		private void MakeMove(ref CActScriptKeyMove pmovedat)
		{
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002509")]
		[Address(RVA = "0x1012ADAA8", Offset = "0x12ADAA8", VA = "0x1012ADAA8")]
		private void MakeCalcBind(ref CActScriptKeyBind pbinddat)
		{
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250A")]
		[Address(RVA = "0x1012AF15C", Offset = "0x12AF15C", VA = "0x1012AF15C")]
		public void SetUpObjectTrs(ref CActScriptKeyBaseTrs pbasetrs)
		{
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250B")]
		[Address(RVA = "0x1012AFDDC", Offset = "0x12AFDDC", VA = "0x1012AFDDC")]
		public void SetUpObjectTrsNonRev(ref CActScriptKeyBaseTrsNonRev pbasetrs)
		{
		}

		// Token: 0x06002842 RID: 10306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250C")]
		[Address(RVA = "0x1012AF4A4", Offset = "0x12AF4A4", VA = "0x1012AF4A4")]
		public void SetUpObjectPosAnime(ref CActScriptKeyPosAnime pposanime)
		{
		}

		// Token: 0x06002843 RID: 10307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250D")]
		[Address(RVA = "0x1012AF844", Offset = "0x12AF844", VA = "0x1012AF844")]
		public void SetUpObjectRotAnime(ref CActScriptKeyRotAnime protanime)
		{
		}

		// Token: 0x06002844 RID: 10308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250E")]
		[Address(RVA = "0x1012AFC0C", Offset = "0x12AFC0C", VA = "0x1012AFC0C")]
		public void ResetObjectBaseRot(CActScriptKeyResetBaseRot protanime)
		{
		}

		// Token: 0x06002845 RID: 10309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600250F")]
		[Address(RVA = "0x1012AFC08", Offset = "0x12AFC08", VA = "0x1012AFC08")]
		public void SetUpObjectBindRot(ref CActScriptKeyBindRot pbindrot)
		{
		}

		// Token: 0x06002846 RID: 10310 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002510")]
		[Address(RVA = "0x1012B01AC", Offset = "0x12B01AC", VA = "0x1012B01AC")]
		public string GetOwnerToTargetName(string fbasename, CActScriptKeyBind.eNameAttach fgetchange)
		{
			return null;
		}

		// Token: 0x06002847 RID: 10311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002511")]
		[Address(RVA = "0x1012AE33C", Offset = "0x12AE33C", VA = "0x1012AE33C")]
		public void SetViewTargetObject(Transform pentry, uint fuid, bool fentry, bool fbase)
		{
		}

		// Token: 0x06002848 RID: 10312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002512")]
		[Address(RVA = "0x1012AE018", Offset = "0x12AE018", VA = "0x1012AE018")]
		public void SetLinkTargetObject(Transform pentry, uint fuid, bool factive)
		{
		}

		// Token: 0x06002849 RID: 10313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002513")]
		[Address(RVA = "0x1012B0290", Offset = "0x12B0290", VA = "0x1012B0290")]
		public void SetTrsTargetObject(Transform pentry, uint fuid, bool factive)
		{
		}

		// Token: 0x0600284A RID: 10314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002514")]
		[Address(RVA = "0x1012B05B4", Offset = "0x12B05B4", VA = "0x1012B05B4")]
		public void CancelPlay()
		{
		}

		// Token: 0x0600284B RID: 10315 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002515")]
		[Address(RVA = "0x1012B00B4", Offset = "0x12B00B4", VA = "0x1012B00B4")]
		protected Transform CalcTargetTrs()
		{
			return null;
		}

		// Token: 0x0600284C RID: 10316 RVA: 0x000110D0 File Offset: 0x0000F2D0
		[Token(Token = "0x6002516")]
		[Address(RVA = "0x1012AF0B0", Offset = "0x12AF0B0", VA = "0x1012AF0B0")]
		public static bool IsPopMessage(TweetListDB pdb, int fid)
		{
			return default(bool);
		}

		// Token: 0x0600284D RID: 10317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002517")]
		[Address(RVA = "0x1012B0C64", Offset = "0x12B0C64", VA = "0x1012B0C64")]
		public void ReleasePlay()
		{
		}

		// Token: 0x0600284E RID: 10318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002518")]
		[Address(RVA = "0x1012B0D80", Offset = "0x12B0D80", VA = "0x1012B0D80")]
		public void UpdateOffset()
		{
		}

		// Token: 0x0600284F RID: 10319 RVA: 0x000110E8 File Offset: 0x0000F2E8
		[Token(Token = "0x6002519")]
		[Address(RVA = "0x1012B1108", Offset = "0x12B1108", VA = "0x1012B1108")]
		private Vector3 CalcCharaOffset(Vector3 baseOffset)
		{
			return default(Vector3);
		}

		// Token: 0x06002850 RID: 10320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600251A")]
		[Address(RVA = "0x1012B11D4", Offset = "0x12B11D4", VA = "0x1012B11D4")]
		public RoomActionScriptPlay()
		{
		}

		// Token: 0x04003899 RID: 14489
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40028F3")]
		protected RoomActionScriptDB m_ActionScript;

		// Token: 0x0400389A RID: 14490
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40028F4")]
		protected int m_PlayIndex;

		// Token: 0x0400389B RID: 14491
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40028F5")]
		protected int m_PlayTableMax;

		// Token: 0x0400389C RID: 14492
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40028F6")]
		protected float m_PlayTime;

		// Token: 0x0400389D RID: 14493
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40028F7")]
		protected CharacterHandler m_Owner;

		// Token: 0x0400389E RID: 14494
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40028F8")]
		protected RoomAnimAccessMap m_AnimeMap;

		// Token: 0x0400389F RID: 14495
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40028F9")]
		protected CharacterAnim m_AnimePlay;

		// Token: 0x040038A0 RID: 14496
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40028FA")]
		protected IRoomObjectControll m_Target;

		// Token: 0x040038A1 RID: 14497
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40028FB")]
		protected RoomObjectCtrlChara m_Base;

		// Token: 0x040038A2 RID: 14498
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40028FC")]
		protected int m_AnimeStep;

		// Token: 0x040038A3 RID: 14499
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40028FD")]
		protected float m_AnimeTime;

		// Token: 0x040038A4 RID: 14500
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40028FE")]
		protected RoomPartsActionPakage m_PartsAction;

		// Token: 0x040038A5 RID: 14501
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40028FF")]
		protected float m_MoveKeyTime;

		// Token: 0x040038A6 RID: 14502
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002900")]
		protected Vector3 m_BackUpPos;

		// Token: 0x040038A7 RID: 14503
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002901")]
		protected int m_OverlapMessgeID;

		// Token: 0x040038A8 RID: 14504
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002902")]
		protected RoomActionScriptPlay.LocalObjectState[] m_LocalObject;

		// Token: 0x040038A9 RID: 14505
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002903")]
		protected RoomActionScriptPlay.LocalLinkObjectState[] m_LinkObject;

		// Token: 0x040038AA RID: 14506
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002904")]
		protected RoomActionScriptPlay.LocalLinkObjectState[] m_TrsObject;

		// Token: 0x040038AB RID: 14507
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002905")]
		protected IRoomObjectControll.ModelRenderConfig[] m_BaseMdlConfig;

		// Token: 0x040038AC RID: 14508
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002906")]
		protected IRoomObjectControll.ModelRenderConfig[] m_ChrMdlConfig;

		// Token: 0x040038AD RID: 14509
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002907")]
		public RoomActionScriptPlay.ActionSciptCallBack m_Callback;

		// Token: 0x040038AE RID: 14510
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002908")]
		protected Vector3 m_oldOffset;

		// Token: 0x040038AF RID: 14511
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002909")]
		protected int m_oldDataIndex;

		// Token: 0x040038B0 RID: 14512
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400290A")]
		protected float m_leapTimer;

		// Token: 0x040038B1 RID: 14513
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x400290B")]
		protected bool m_isLeap;

		// Token: 0x040038B2 RID: 14514
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400290C")]
		private RoomActionScriptPlay.OffsetTriggerAnimData[] trgAnimData;

		// Token: 0x02000973 RID: 2419
		[Token(Token = "0x2000F69")]
		public struct LocalObjectState
		{
			// Token: 0x040038B3 RID: 14515
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006319")]
			public Transform m_Object;

			// Token: 0x040038B4 RID: 14516
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400631A")]
			public uint m_AccessID;

			// Token: 0x040038B5 RID: 14517
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x400631B")]
			public bool m_Active;

			// Token: 0x040038B6 RID: 14518
			[Cpp2IlInjected.FieldOffset(Offset = "0xD")]
			[Token(Token = "0x400631C")]
			public bool m_BaseUp;
		}

		// Token: 0x02000974 RID: 2420
		[Token(Token = "0x2000F6A")]
		public struct LocalLinkObjectState
		{
			// Token: 0x040038B7 RID: 14519
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400631D")]
			public Transform m_Object;

			// Token: 0x040038B8 RID: 14520
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400631E")]
			public uint m_AccessID;

			// Token: 0x040038B9 RID: 14521
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x400631F")]
			public Vector3 m_BackPos;

			// Token: 0x040038BA RID: 14522
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006320")]
			public Quaternion m_BackRot;
		}

		// Token: 0x02000975 RID: 2421
		// (Invoke) Token: 0x06002852 RID: 10322
		[Token(Token = "0x2000F6B")]
		public delegate bool ActionSciptCallBack(CActScriptKeyProgramEvent pcmd);

		// Token: 0x02000976 RID: 2422
		[Token(Token = "0x2000F6C")]
		public class OffsetTriggerAnimData
		{
			// Token: 0x06002855 RID: 10325 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FF8")]
			[Address(RVA = "0x1012B1BE8", Offset = "0x12B1BE8", VA = "0x1012B1BE8")]
			public OffsetTriggerAnimData(string animName, int stLerpFrame, Vector3 offsetSt, Vector3 offsetEd)
			{
			}

			// Token: 0x040038BB RID: 14523
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006321")]
			public string animName;

			// Token: 0x040038BC RID: 14524
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006322")]
			public int stLerpFrame;

			// Token: 0x040038BD RID: 14525
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006323")]
			public Vector3 lerpSt;

			// Token: 0x040038BE RID: 14526
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006324")]
			public Vector3 lerpEd;
		}
	}
}
