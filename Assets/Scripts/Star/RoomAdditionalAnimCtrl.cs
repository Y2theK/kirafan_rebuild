﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200099F RID: 2463
	[Token(Token = "0x20006FF")]
	[StructLayout(3)]
	public class RoomAdditionalAnimCtrl : MonoBehaviour
	{
		// Token: 0x060028D6 RID: 10454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002598")]
		[Address(RVA = "0x1012B1C6C", Offset = "0x12B1C6C", VA = "0x1012B1C6C")]
		public void Destroy()
		{
		}

		// Token: 0x060028D7 RID: 10455 RVA: 0x00011358 File Offset: 0x0000F558
		[Token(Token = "0x6002599")]
		[Address(RVA = "0x1012B1C78", Offset = "0x12B1C78", VA = "0x1012B1C78")]
		public bool IsDestroyProc()
		{
			return default(bool);
		}

		// Token: 0x060028D8 RID: 10456 RVA: 0x00011370 File Offset: 0x0000F570
		[Token(Token = "0x600259A")]
		[Address(RVA = "0x1012B1C88", Offset = "0x12B1C88", VA = "0x1012B1C88")]
		public bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x060028D9 RID: 10457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600259B")]
		[Address(RVA = "0x1012B1C98", Offset = "0x12B1C98", VA = "0x1012B1C98")]
		public void Setup(RoomObjectCtrlChara owner)
		{
		}

		// Token: 0x060028DA RID: 10458 RVA: 0x00011388 File Offset: 0x0000F588
		[Token(Token = "0x600259C")]
		[Address(RVA = "0x1012B1CD8", Offset = "0x12B1CD8", VA = "0x1012B1CD8")]
		public bool IsLoadedFromAccessKey(string key)
		{
			return default(bool);
		}

		// Token: 0x060028DB RID: 10459 RVA: 0x000113A0 File Offset: 0x0000F5A0
		[Token(Token = "0x600259D")]
		[Address(RVA = "0x1012B1DA4", Offset = "0x12B1DA4", VA = "0x1012B1DA4")]
		public bool IsLoadedFromClipName(string clipName)
		{
			return default(bool);
		}

		// Token: 0x060028DC RID: 10460 RVA: 0x000113B8 File Offset: 0x0000F5B8
		[Token(Token = "0x600259E")]
		[Address(RVA = "0x1012B1E70", Offset = "0x12B1E70", VA = "0x1012B1E70")]
		public bool IsLoadingFromAccessKey(string key)
		{
			return default(bool);
		}

		// Token: 0x060028DD RID: 10461 RVA: 0x000113D0 File Offset: 0x0000F5D0
		[Token(Token = "0x600259F")]
		[Address(RVA = "0x1012B1F3C", Offset = "0x12B1F3C", VA = "0x1012B1F3C")]
		public bool IsLoadingFromClipName(string clipName)
		{
			return default(bool);
		}

		// Token: 0x060028DE RID: 10462 RVA: 0x000113E8 File Offset: 0x0000F5E8
		[Token(Token = "0x60025A0")]
		[Address(RVA = "0x1012B2008", Offset = "0x12B2008", VA = "0x1012B2008")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x060028DF RID: 10463 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60025A1")]
		[Address(RVA = "0x1012B2074", Offset = "0x12B2074", VA = "0x1012B2074")]
		private RoomAdditionalAnimCtrl.LoadAsset GetLoadAssetFromClipName(string clipName)
		{
			return null;
		}

		// Token: 0x060028E0 RID: 10464 RVA: 0x00011400 File Offset: 0x0000F600
		[Token(Token = "0x60025A2")]
		[Address(RVA = "0x1012B2214", Offset = "0x12B2214", VA = "0x1012B2214")]
		public bool Load(string key)
		{
			return default(bool);
		}

		// Token: 0x060028E1 RID: 10465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025A3")]
		[Address(RVA = "0x1012B3270", Offset = "0x12B3270", VA = "0x1012B3270")]
		private void TransformReset(Transform trans)
		{
		}

		// Token: 0x060028E2 RID: 10466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025A4")]
		[Address(RVA = "0x1012B33D8", Offset = "0x12B33D8", VA = "0x1012B33D8")]
		private void SetupAnime(List<RoomAdditionalAnimCtrl.LoadAsset> doneList)
		{
		}

		// Token: 0x060028E3 RID: 10467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025A5")]
		[Address(RVA = "0x1012B3A10", Offset = "0x12B3A10", VA = "0x1012B3A10")]
		private void LoadProc()
		{
		}

		// Token: 0x060028E4 RID: 10468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025A6")]
		[Address(RVA = "0x1012B3E4C", Offset = "0x12B3E4C", VA = "0x1012B3E4C")]
		private void AnimProc()
		{
		}

		// Token: 0x060028E5 RID: 10469 RVA: 0x00011418 File Offset: 0x0000F618
		[Token(Token = "0x60025A7")]
		[Address(RVA = "0x1012B3E50", Offset = "0x12B3E50", VA = "0x1012B3E50")]
		public bool Play(string clipName, float blendSec, WrapMode wrapMode, float spdScale = 1f)
		{
			return default(bool);
		}

		// Token: 0x060028E6 RID: 10470 RVA: 0x00011430 File Offset: 0x0000F630
		[Token(Token = "0x60025A8")]
		[Address(RVA = "0x1012B403C", Offset = "0x12B403C", VA = "0x1012B403C")]
		public bool Pause()
		{
			return default(bool);
		}

		// Token: 0x060028E7 RID: 10471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025A9")]
		[Address(RVA = "0x1012B4158", Offset = "0x12B4158", VA = "0x1012B4158")]
		public void FrameReset()
		{
		}

		// Token: 0x060028E8 RID: 10472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025AA")]
		[Address(RVA = "0x1012B426C", Offset = "0x12B426C", VA = "0x1012B426C")]
		public void ChangeSpdScale(float spdScale)
		{
		}

		// Token: 0x060028E9 RID: 10473 RVA: 0x00011448 File Offset: 0x0000F648
		[Token(Token = "0x60025AB")]
		[Address(RVA = "0x1012B438C", Offset = "0x12B438C", VA = "0x1012B438C")]
		public float GetPlayingSec()
		{
			return 0f;
		}

		// Token: 0x060028EA RID: 10474 RVA: 0x00011460 File Offset: 0x0000F660
		[Token(Token = "0x60025AC")]
		[Address(RVA = "0x1012B4404", Offset = "0x12B4404", VA = "0x1012B4404")]
		public float GetPlayingSec(string clipName)
		{
			return 0f;
		}

		// Token: 0x060028EB RID: 10475 RVA: 0x00011478 File Offset: 0x0000F678
		[Token(Token = "0x60025AD")]
		[Address(RVA = "0x1012B45A8", Offset = "0x12B45A8", VA = "0x1012B45A8")]
		public int GetPlayingFrame(string clipName)
		{
			return 0;
		}

		// Token: 0x060028EC RID: 10476 RVA: 0x00011490 File Offset: 0x0000F690
		[Token(Token = "0x60025AE")]
		[Address(RVA = "0x1012B4714", Offset = "0x12B4714", VA = "0x1012B4714")]
		public float GetMaxSec(string clipName)
		{
			return 0f;
		}

		// Token: 0x060028ED RID: 10477 RVA: 0x000114A8 File Offset: 0x0000F6A8
		[Token(Token = "0x60025AF")]
		[Address(RVA = "0x1012B47DC", Offset = "0x12B47DC", VA = "0x1012B47DC")]
		public bool IsPlayingAnim(string actionKey)
		{
			return default(bool);
		}

		// Token: 0x060028EE RID: 10478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B0")]
		[Address(RVA = "0x1012B499C", Offset = "0x12B499C", VA = "0x1012B499C")]
		public void DestroyProc()
		{
		}

		// Token: 0x060028EF RID: 10479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B1")]
		[Address(RVA = "0x1012B525C", Offset = "0x12B525C", VA = "0x1012B525C")]
		public void Update()
		{
		}

		// Token: 0x060028F0 RID: 10480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B2")]
		[Address(RVA = "0x1012B52EC", Offset = "0x12B52EC", VA = "0x1012B52EC")]
		public RoomAdditionalAnimCtrl()
		{
		}

		// Token: 0x0400393D RID: 14653
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002982")]
		private List<RoomAdditionalAnimCtrl.LoadAsset> m_LoadingList;

		// Token: 0x0400393E RID: 14654
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002983")]
		private List<RoomAdditionalAnimCtrl.LoadAsset> m_LoadedList;

		// Token: 0x0400393F RID: 14655
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002984")]
		private RoomAdditionalAnimCtrl.LoadAsset m_ActiveData;

		// Token: 0x04003940 RID: 14656
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002985")]
		private RoomObjectCtrlChara m_Owner;

		// Token: 0x04003941 RID: 14657
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002986")]
		private Transform m_Transform;

		// Token: 0x04003942 RID: 14658
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002987")]
		private RoomAdditionalAnimCtrl.eDestroyState m_state;

		// Token: 0x020009A0 RID: 2464
		[Token(Token = "0x2000F70")]
		private enum eDestroyState
		{
			// Token: 0x04003944 RID: 14660
			[Token(Token = "0x400632F")]
			State_None,
			// Token: 0x04003945 RID: 14661
			[Token(Token = "0x4006330")]
			State_DisableAutoRetry,
			// Token: 0x04003946 RID: 14662
			[Token(Token = "0x4006331")]
			State_WaitDone,
			// Token: 0x04003947 RID: 14663
			[Token(Token = "0x4006332")]
			State_Release,
			// Token: 0x04003948 RID: 14664
			[Token(Token = "0x4006333")]
			State_Complated
		}

		// Token: 0x020009A1 RID: 2465
		[Token(Token = "0x2000F71")]
		private class AnimeSet
		{
			// Token: 0x060028F1 RID: 10481 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FFC")]
			[Address(RVA = "0x1012B3268", Offset = "0x12B3268", VA = "0x1012B3268")]
			public AnimeSet()
			{
			}

			// Token: 0x04003949 RID: 14665
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006334")]
			public ABResourceObjectHandler m_LoadHandler;

			// Token: 0x0400394A RID: 14666
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006335")]
			public MeigeAnimCtrl m_AnimCtrl;

			// Token: 0x0400394B RID: 14667
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006336")]
			public string[] m_Paths;
		}

		// Token: 0x020009A2 RID: 2466
		[Token(Token = "0x2000F72")]
		private class LoadAsset
		{
			// Token: 0x060028F2 RID: 10482 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005FFD")]
			[Address(RVA = "0x1012B3208", Offset = "0x12B3208", VA = "0x1012B3208")]
			public LoadAsset()
			{
			}

			// Token: 0x0400394C RID: 14668
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006337")]
			public RoomAdditionalAnimCtrl.AnimeSet[] m_Anime;

			// Token: 0x0400394D RID: 14669
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006338")]
			public string m_AccessKey;

			// Token: 0x0400394E RID: 14670
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006339")]
			public string[] m_Clips;
		}
	}
}
