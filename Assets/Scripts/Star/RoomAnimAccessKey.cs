﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A4B RID: 2635
	[Token(Token = "0x2000758")]
	[StructLayout(0)]
	public struct RoomAnimAccessKey
	{
		// Token: 0x04003CF6 RID: 15606
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002BF3")]
		public uint m_AccessKey;

		// Token: 0x04003CF7 RID: 15607
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002BF4")]
		public string m_BaseName;

		// Token: 0x04003CF8 RID: 15608
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BF5")]
		public string[] m_KeyName;
	}
}
