﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A4C RID: 2636
	[Token(Token = "0x2000759")]
	[StructLayout(3)]
	public class RoomAnimAccessMap
	{
		// Token: 0x06002D6A RID: 11626 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029C7")]
		[Address(RVA = "0x1012B55C0", Offset = "0x12B55C0", VA = "0x1012B55C0")]
		public string GetAnimeKey(RoomDefine.eAnim fanim, CharacterDefine.eDir fdir)
		{
			return null;
		}

		// Token: 0x06002D6B RID: 11627 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029C8")]
		[Address(RVA = "0x1012B56A0", Offset = "0x12B56A0", VA = "0x1012B56A0")]
		public string GetAnimeStep(RoomDefine.eAnim fanim, int fstep, CharacterDefine.eDir fdir)
		{
			return null;
		}

		// Token: 0x06002D6C RID: 11628 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029C9")]
		[Address(RVA = "0x1012B5780", Offset = "0x12B5780", VA = "0x1012B5780")]
		public string GetAnimeGroup(int fstep, CharacterDefine.eDir fdir)
		{
			return null;
		}

		// Token: 0x06002D6D RID: 11629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029CA")]
		[Address(RVA = "0x1012B5878", Offset = "0x12B5878", VA = "0x1012B5878")]
		public void SetMarkingGroup(RoomDefine.eAnim fanim)
		{
		}

		// Token: 0x06002D6E RID: 11630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029CB")]
		[Address(RVA = "0x1012B59BC", Offset = "0x12B59BC", VA = "0x1012B59BC")]
		public void SetMarkingGroup(uint fgroup)
		{
		}

		// Token: 0x06002D6F RID: 11631 RVA: 0x00013698 File Offset: 0x00011898
		[Token(Token = "0x60029CC")]
		[Address(RVA = "0x1012B5A60", Offset = "0x12B5A60", VA = "0x1012B5A60")]
		public RoomDefine.eAnim GetCommonAnimIDFromKeyName(string key)
		{
			return RoomDefine.eAnim.Idle;
		}

		// Token: 0x06002D70 RID: 11632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029CD")]
		[Address(RVA = "0x1012B5AF0", Offset = "0x12B5AF0", VA = "0x1012B5AF0")]
		public RoomAnimAccessMap()
		{
		}

		// Token: 0x04003CF9 RID: 15609
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BF6")]
		public RoomAnimAccessKey[] m_BodyMap;

		// Token: 0x04003CFA RID: 15610
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BF7")]
		public int m_MarkingID;
	}
}
