﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A4F RID: 2639
	[Token(Token = "0x200075C")]
	[StructLayout(3)]
	public static class RoomAnimePlay
	{
		// Token: 0x06002D72 RID: 11634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029CF")]
		[Address(RVA = "0x1012B5AF8", Offset = "0x12B5AF8", VA = "0x1012B5AF8")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06002D73 RID: 11635 RVA: 0x000136B0 File Offset: 0x000118B0
		[Token(Token = "0x60029D0")]
		[Address(RVA = "0x1012B5BC0", Offset = "0x12B5BC0", VA = "0x1012B5BC0")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06002D74 RID: 11636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029D1")]
		[Address(RVA = "0x1012B5C20", Offset = "0x12B5C20", VA = "0x1012B5C20")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x06002D75 RID: 11637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029D2")]
		[Address(RVA = "0x1012B5C88", Offset = "0x12B5C88", VA = "0x1012B5C88")]
		public static void GetRoomCharacterAnimSettings(List<AnimSettings> settings, int partsIndex, int foptioncode, int fkey)
		{
		}

		// Token: 0x06002D76 RID: 11638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029D3")]
		[Address(RVA = "0x1012B6504", Offset = "0x12B6504", VA = "0x1012B6504")]
		public static void BodyAnimeUp(CharacterHandler pchara, MeigeResource.Handler phandle, int fkeycode)
		{
		}

		// Token: 0x06002D77 RID: 11639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029D4")]
		[Address(RVA = "0x1012B66E0", Offset = "0x12B66E0", VA = "0x1012B66E0")]
		public static void HeadAnimeUp(CharacterHandler pchara, MeigeResource.Handler phandle, int fkeycode)
		{
		}

		// Token: 0x06002D78 RID: 11640 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029D5")]
		[Address(RVA = "0x1012B68BC", Offset = "0x12B68BC", VA = "0x1012B68BC")]
		public static RoomAnimAccessMap CreateAnimeMap(int fkey)
		{
			return null;
		}

		// Token: 0x06002D79 RID: 11641 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029D6")]
		[Address(RVA = "0x1012B7130", Offset = "0x12B7130", VA = "0x1012B7130")]
		public static RoomAcitionWakeUp GetNextActionParam(RoomCharaActMode.eMode fmode)
		{
			return null;
		}

		// Token: 0x04003CFF RID: 15615
		[Token(Token = "0x4002BFC")]
		public static RoomAnimePlay.RoomAnimePlayTagList ms_RoomAnimeTagDB;

		// Token: 0x02000A50 RID: 2640
		[Token(Token = "0x2000FC1")]
		public class BuildUpSettings
		{
			// Token: 0x06002D7A RID: 11642 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600605D")]
			[Address(RVA = "0x1012B7264", Offset = "0x12B7264", VA = "0x1012B7264")]
			public BuildUpSettings(string fkeyname, string filename, uint fcategory)
			{
			}

			// Token: 0x04003D00 RID: 15616
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006474")]
			public string m_AnimationClipPath;

			// Token: 0x04003D01 RID: 15617
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006475")]
			public string m_ActionKey;

			// Token: 0x04003D02 RID: 15618
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006476")]
			public uint m_Category;
		}

		// Token: 0x02000A51 RID: 2641
		[Token(Token = "0x2000FC2")]
		public class XlsRoomAnimeTagData : IXlsBinForm
		{
			// Token: 0x06002D7B RID: 11643 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600605E")]
			[Address(RVA = "0x1012B7494", Offset = "0x12B7494", VA = "0x1012B7494", Slot = "5")]
			public override void SheetDataTag(string psheetname, int ftablenum, BinaryReader pfiles)
			{
			}

			// Token: 0x06002D7C RID: 11644 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600605F")]
			[Address(RVA = "0x1012B5D28", Offset = "0x12B5D28", VA = "0x1012B5D28")]
			public void CreateSetting(List<AnimSettings> settings, int partsIndex, int foptioncode, int fkey)
			{
			}

			// Token: 0x06002D7D RID: 11645 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006060")]
			[Address(RVA = "0x1012B7E9C", Offset = "0x12B7E9C", VA = "0x1012B7E9C")]
			public void CreateDummySetting(List<AnimSettings> settings, int partsIndex, int foptioncode)
			{
			}

			// Token: 0x06002D7E RID: 11646 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006061")]
			[Address(RVA = "0x1012B7FC0", Offset = "0x12B7FC0", VA = "0x1012B7FC0")]
			public void CreateDatbaseSetting(List<RoomAnimePlay.BuildUpSettings> settings, int partsIndex, int foptioncode, int fkey)
			{
			}

			// Token: 0x06002D7F RID: 11647 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006062")]
			[Address(RVA = "0x1012B73FC", Offset = "0x12B73FC", VA = "0x1012B73FC")]
			public XlsRoomAnimeTagData()
			{
			}

			// Token: 0x04003D03 RID: 15619
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006477")]
			public List<RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess> m_AnimeAccess;

			// Token: 0x04003D04 RID: 15620
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006478")]
			public List<RoomAcitionWakeUp> m_ActionAccess;

			// Token: 0x02000A52 RID: 2642
			[Token(Token = "0x2001348")]
			public class XlsAnimaAccess
			{
				// Token: 0x06002D80 RID: 11648 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006514")]
				[Address(RVA = "0x1012B7DC8", Offset = "0x12B7DC8", VA = "0x1012B7DC8")]
				public XlsAnimaAccess()
				{
				}

				// Token: 0x04003D05 RID: 15621
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x4007593")]
				public uint m_KeyCode;

				// Token: 0x04003D06 RID: 15622
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x4007594")]
				public short m_LR;

				// Token: 0x04003D07 RID: 15623
				[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
				[Token(Token = "0x4007595")]
				public short m_Type;

				// Token: 0x04003D08 RID: 15624
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x4007596")]
				public int m_Num;

				// Token: 0x04003D09 RID: 15625
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x4007597")]
				public string m_BaseName;
			}
		}

		// Token: 0x02000A53 RID: 2643
		[Token(Token = "0x2000FC3")]
		public class RoomAnimePlayTagList : IDataBaseResource
		{
			// Token: 0x06002D81 RID: 11649 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006063")]
			[Address(RVA = "0x1012B72A4", Offset = "0x12B72A4", VA = "0x1012B72A4", Slot = "4")]
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
			}

			// Token: 0x06002D82 RID: 11650 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006064")]
			[Address(RVA = "0x1012B5BB8", Offset = "0x12B5BB8", VA = "0x1012B5BB8")]
			public RoomAnimePlayTagList()
			{
			}

			// Token: 0x04003D0A RID: 15626
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006479")]
			public RoomAnimePlay.XlsRoomAnimeTagData m_AnimeTag;
		}
	}
}
