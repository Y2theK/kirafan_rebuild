﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000998 RID: 2456
	[Token(Token = "0x20006FA")]
	[StructLayout(3)]
	public class RoomBGFitCamera : MonoBehaviour
	{
		// Token: 0x0600289B RID: 10395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600255D")]
		[Address(RVA = "0x1012B8544", Offset = "0x12B8544", VA = "0x1012B8544")]
		private void Awake()
		{
		}

		// Token: 0x0600289C RID: 10396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600255E")]
		[Address(RVA = "0x1012B8628", Offset = "0x12B8628", VA = "0x1012B8628")]
		public RoomBGFitCamera()
		{
		}

		// Token: 0x04003910 RID: 14608
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400295C")]
		public float m_BGWidth;

		// Token: 0x04003911 RID: 14609
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400295D")]
		public float m_BGHeight;

		// Token: 0x04003912 RID: 14610
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400295E")]
		public float m_PixelUnit;
	}
}
