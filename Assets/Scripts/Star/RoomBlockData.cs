﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A22 RID: 2594
	[Token(Token = "0x2000740")]
	[StructLayout(3)]
	public class RoomBlockData
	{
		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06002C32 RID: 11314 RVA: 0x00012CC0 File Offset: 0x00010EC0
		// (set) Token: 0x06002C33 RID: 11315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C0")]
		public Vector2 Pos
		{
			[Token(Token = "0x60028A1")]
			[Address(RVA = "0x1012B8648", Offset = "0x12B8648", VA = "0x1012B8648")]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x60028A2")]
			[Address(RVA = "0x1012B8678", Offset = "0x12B8678", VA = "0x1012B8678")]
			set
			{
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06002C34 RID: 11316 RVA: 0x00012CD8 File Offset: 0x00010ED8
		// (set) Token: 0x06002C35 RID: 11317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C1")]
		public Vector3 PosW
		{
			[Token(Token = "0x60028A3")]
			[Address(RVA = "0x1012B8684", Offset = "0x12B8684", VA = "0x1012B8684")]
			get
			{
				return default(Vector3);
			}
			[Token(Token = "0x60028A4")]
			[Address(RVA = "0x1012B86C0", Offset = "0x12B86C0", VA = "0x1012B86C0")]
			set
			{
			}
		}

		// Token: 0x06002C36 RID: 11318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028A5")]
		[Address(RVA = "0x1012B86CC", Offset = "0x12B86CC", VA = "0x1012B86CC")]
		public void SetPos(float valuex, float valuey)
		{
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06002C37 RID: 11319 RVA: 0x00012CF0 File Offset: 0x00010EF0
		[Token(Token = "0x170002C2")]
		public int PosX
		{
			[Token(Token = "0x60028A6")]
			[Address(RVA = "0x1012B86D8", Offset = "0x12B86D8", VA = "0x1012B86D8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06002C38 RID: 11320 RVA: 0x00012D08 File Offset: 0x00010F08
		[Token(Token = "0x170002C3")]
		public int PosY
		{
			[Token(Token = "0x60028A7")]
			[Address(RVA = "0x1012B86F0", Offset = "0x12B86F0", VA = "0x1012B86F0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06002C39 RID: 11321 RVA: 0x00012D20 File Offset: 0x00010F20
		[Token(Token = "0x170002C4")]
		public float PosSpaceX
		{
			[Token(Token = "0x60028A8")]
			[Address(RVA = "0x1012B8708", Offset = "0x12B8708", VA = "0x1012B8708")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06002C3A RID: 11322 RVA: 0x00012D38 File Offset: 0x00010F38
		[Token(Token = "0x170002C5")]
		public float PosSpaceY
		{
			[Token(Token = "0x60028A9")]
			[Address(RVA = "0x1012B8710", Offset = "0x12B8710", VA = "0x1012B8710")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06002C3B RID: 11323 RVA: 0x00012D50 File Offset: 0x00010F50
		// (set) Token: 0x06002C3C RID: 11324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C6")]
		public Vector2 Size
		{
			[Token(Token = "0x60028AA")]
			[Address(RVA = "0x1012B8718", Offset = "0x12B8718", VA = "0x1012B8718")]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x60028AB")]
			[Address(RVA = "0x1012B8750", Offset = "0x12B8750", VA = "0x1012B8750")]
			set
			{
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06002C3D RID: 11325 RVA: 0x00012D68 File Offset: 0x00010F68
		// (set) Token: 0x06002C3E RID: 11326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C7")]
		public Vector3 SizeW
		{
			[Token(Token = "0x60028AC")]
			[Address(RVA = "0x1012B8760", Offset = "0x12B8760", VA = "0x1012B8760")]
			get
			{
				return default(Vector3);
			}
			[Token(Token = "0x60028AD")]
			[Address(RVA = "0x1012B87A4", Offset = "0x12B87A4", VA = "0x1012B87A4")]
			set
			{
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06002C3F RID: 11327 RVA: 0x00012D80 File Offset: 0x00010F80
		// (set) Token: 0x06002C40 RID: 11328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C8")]
		public int SizeX
		{
			[Token(Token = "0x60028AE")]
			[Address(RVA = "0x1012B87B4", Offset = "0x12B87B4", VA = "0x1012B87B4")]
			get
			{
				return 0;
			}
			[Token(Token = "0x60028AF")]
			[Address(RVA = "0x1012B87BC", Offset = "0x12B87BC", VA = "0x1012B87BC")]
			set
			{
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06002C41 RID: 11329 RVA: 0x00012D98 File Offset: 0x00010F98
		// (set) Token: 0x06002C42 RID: 11330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002C9")]
		public int SizeY
		{
			[Token(Token = "0x60028B0")]
			[Address(RVA = "0x1012B87C4", Offset = "0x12B87C4", VA = "0x1012B87C4")]
			get
			{
				return 0;
			}
			[Token(Token = "0x60028B1")]
			[Address(RVA = "0x1012B87CC", Offset = "0x12B87CC", VA = "0x1012B87CC")]
			set
			{
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06002C43 RID: 11331 RVA: 0x00012DB0 File Offset: 0x00010FB0
		[Token(Token = "0x170002CA")]
		public uint UniqueID
		{
			[Token(Token = "0x60028B2")]
			[Address(RVA = "0x1012B87D4", Offset = "0x12B87D4", VA = "0x1012B87D4")]
			get
			{
				return 0U;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06002C44 RID: 11332 RVA: 0x00012DC8 File Offset: 0x00010FC8
		[Token(Token = "0x170002CB")]
		public RoomBlockData.BlockBounds Bounds
		{
			[Token(Token = "0x60028B3")]
			[Address(RVA = "0x1012B87DC", Offset = "0x12B87DC", VA = "0x1012B87DC")]
			get
			{
				return default(RoomBlockData.BlockBounds);
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06002C45 RID: 11333 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002CC")]
		public IRoomObjectControll RoomObject
		{
			[Token(Token = "0x60028B4")]
			[Address(RVA = "0x1012B8814", Offset = "0x12B8814", VA = "0x1012B8814")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002C46 RID: 11334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028B5")]
		[Address(RVA = "0x1012B881C", Offset = "0x12B881C", VA = "0x1012B881C")]
		public void SetAttachObject(bool fattach)
		{
		}

		// Token: 0x06002C47 RID: 11335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028B6")]
		[Address(RVA = "0x1012B8824", Offset = "0x12B8824", VA = "0x1012B8824")]
		public void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002C48 RID: 11336 RVA: 0x00012DE0 File Offset: 0x00010FE0
		[Token(Token = "0x60028B7")]
		[Address(RVA = "0x1012B882C", Offset = "0x12B882C", VA = "0x1012B882C")]
		public bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002C49 RID: 11337 RVA: 0x00012DF8 File Offset: 0x00010FF8
		[Token(Token = "0x60028B8")]
		[Address(RVA = "0x1012B8834", Offset = "0x12B8834", VA = "0x1012B8834")]
		public bool IsLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002C4A RID: 11338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028B9")]
		[Address(RVA = "0x1012B8854", Offset = "0x12B8854", VA = "0x1012B8854")]
		public RoomBlockData()
		{
		}

		// Token: 0x06002C4B RID: 11339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028BA")]
		[Address(RVA = "0x1012B88E0", Offset = "0x12B88E0", VA = "0x1012B88E0")]
		public RoomBlockData(float posX, float posY, int sizeX, int sizeY)
		{
		}

		// Token: 0x06002C4C RID: 11340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028BB")]
		[Address(RVA = "0x1012B8994", Offset = "0x12B8994", VA = "0x1012B8994")]
		public void SetLinkObject(IRoomObjectControll pobj)
		{
		}

		// Token: 0x04003BC0 RID: 15296
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B13")]
		private float m_PosX;

		// Token: 0x04003BC1 RID: 15297
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002B14")]
		private float m_PosY;

		// Token: 0x04003BC2 RID: 15298
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B15")]
		private int m_SizeX;

		// Token: 0x04003BC3 RID: 15299
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002B16")]
		private int m_SizeY;

		// Token: 0x04003BC4 RID: 15300
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B17")]
		private uint m_UniqueID;

		// Token: 0x04003BC5 RID: 15301
		[Token(Token = "0x4002B18")]
		private static uint m_UniqeIDGenerator;

		// Token: 0x04003BC6 RID: 15302
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002B19")]
		private bool m_AttachLink;

		// Token: 0x04003BC7 RID: 15303
		[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
		[Token(Token = "0x4002B1A")]
		private bool m_isActiveLinkObject;

		// Token: 0x04003BC8 RID: 15304
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B1B")]
		private IRoomObjectControll m_RoomObjHndl;

		// Token: 0x02000A23 RID: 2595
		[Token(Token = "0x2000FB0")]
		public struct BlockBounds
		{
			// Token: 0x06002C4E RID: 11342 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600604B")]
			[Address(RVA = "0x1000335B8", Offset = "0x335B8", VA = "0x1000335B8")]
			public BlockBounds(float _xmin, float _xmax, float _ymin, float _ymax, float _zmin, float _zmax)
			{
			}

			// Token: 0x04003BC9 RID: 15305
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400641E")]
			public float xmin;

			// Token: 0x04003BCA RID: 15306
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x400641F")]
			public float xmax;

			// Token: 0x04003BCB RID: 15307
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006420")]
			public float ymin;

			// Token: 0x04003BCC RID: 15308
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4006421")]
			public float ymax;

			// Token: 0x04003BCD RID: 15309
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006422")]
			public float zmin;

			// Token: 0x04003BCE RID: 15310
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006423")]
			public float zmax;
		}
	}
}
