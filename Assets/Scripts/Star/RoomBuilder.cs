﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A24 RID: 2596
	[Token(Token = "0x2000741")]
	[StructLayout(3)]
	public class RoomBuilder : RoomBuilderBase
	{
		// Token: 0x06002C4F RID: 11343 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60028BD")]
		[Address(RVA = "0x1012B89A0", Offset = "0x12B89A0", VA = "0x1012B89A0", Slot = "6")]
		public override IRoomCharaManager GetCharaManager()
		{
			return null;
		}

		// Token: 0x1400003E RID: 62
		// (add) Token: 0x06002C50 RID: 11344 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C51 RID: 11345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003E")]
		public event Action<IMainComCommand> OnEventSend
		{
			[Token(Token = "0x60028BE")]
			[Address(RVA = "0x1012B89A8", Offset = "0x12B89A8", VA = "0x1012B89A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028BF")]
			[Address(RVA = "0x1012B8AB4", Offset = "0x12B8AB4", VA = "0x1012B8AB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002C52 RID: 11346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028C0")]
		[Address(RVA = "0x1012B8BC0", Offset = "0x12B8BC0", VA = "0x1012B8BC0")]
		public void SetOwner(RoomMain owner)
		{
		}

		// Token: 0x06002C53 RID: 11347 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60028C1")]
		[Address(RVA = "0x1012B8BC8", Offset = "0x12B8BC8", VA = "0x1012B8BC8")]
		public RoomMain GetOwner()
		{
			return null;
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06002C54 RID: 11348 RVA: 0x00012E10 File Offset: 0x00011010
		[Token(Token = "0x170002CD")]
		public bool IsEditNew
		{
			[Token(Token = "0x60028C2")]
			[Address(RVA = "0x1012B8BD0", Offset = "0x12B8BD0", VA = "0x1012B8BD0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06002C55 RID: 11349 RVA: 0x00012E28 File Offset: 0x00011028
		[Token(Token = "0x170002CE")]
		public bool IsBuilt
		{
			[Token(Token = "0x60028C3")]
			[Address(RVA = "0x1012B8BD8", Offset = "0x12B8BD8", VA = "0x1012B8BD8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06002C56 RID: 11350 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002CF")]
		public IRoomObjectControll SelectingObj
		{
			[Token(Token = "0x60028C4")]
			[Address(RVA = "0x1012B8BE0", Offset = "0x12B8BE0", VA = "0x1012B8BE0")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002C57 RID: 11351 RVA: 0x00012E40 File Offset: 0x00011040
		[Token(Token = "0x60028C5")]
		[Address(RVA = "0x1012B8BE8", Offset = "0x12B8BE8", VA = "0x1012B8BE8")]
		public long GetManageID()
		{
			return 0L;
		}

		// Token: 0x06002C58 RID: 11352 RVA: 0x00012E58 File Offset: 0x00011058
		[Token(Token = "0x60028C6")]
		[Address(RVA = "0x1012B8BF0", Offset = "0x12B8BF0", VA = "0x1012B8BF0", Slot = "11")]
		public override int GetFloorNO()
		{
			return 0;
		}

		// Token: 0x06002C59 RID: 11353 RVA: 0x00012E70 File Offset: 0x00011070
		[Token(Token = "0x60028C7")]
		[Address(RVA = "0x1012B8C24", Offset = "0x12B8C24", VA = "0x1012B8C24")]
		public int GetTimeCategory()
		{
			return 0;
		}

		// Token: 0x06002C5A RID: 11354 RVA: 0x00012E88 File Offset: 0x00011088
		[Token(Token = "0x60028C8")]
		[Address(RVA = "0x1012B8C2C", Offset = "0x12B8C2C", VA = "0x1012B8C2C")]
		public int GetFloorFreeSpaceNum(int floorno = 0)
		{
			return 0;
		}

		// Token: 0x06002C5B RID: 11355 RVA: 0x00012EA0 File Offset: 0x000110A0
		[Token(Token = "0x60028C9")]
		[Address(RVA = "0x1012B8C8C", Offset = "0x12B8C8C", VA = "0x1012B8C8C")]
		public bool CheckFreeSpace(RoomObjectListDB_Param param, int floorno = 0)
		{
			return default(bool);
		}

		// Token: 0x1400003F RID: 63
		// (add) Token: 0x06002C5C RID: 11356 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C5D RID: 11357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400003F")]
		public event Action<IRoomObjectControll, bool> OnSelectObject
		{
			[Token(Token = "0x60028CA")]
			[Address(RVA = "0x1012B8D20", Offset = "0x12B8D20", VA = "0x1012B8D20")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028CB")]
			[Address(RVA = "0x1012B8E2C", Offset = "0x12B8E2C", VA = "0x1012B8E2C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000040 RID: 64
		// (add) Token: 0x06002C5E RID: 11358 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C5F RID: 11359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000040")]
		public event Action OnUnSelectObject
		{
			[Token(Token = "0x60028CC")]
			[Address(RVA = "0x1012B8F38", Offset = "0x12B8F38", VA = "0x1012B8F38")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028CD")]
			[Address(RVA = "0x1012B9044", Offset = "0x12B9044", VA = "0x1012B9044")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000041 RID: 65
		// (add) Token: 0x06002C60 RID: 11360 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C61 RID: 11361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000041")]
		public event Action OnSelectingObjectCanPlacement
		{
			[Token(Token = "0x60028CE")]
			[Address(RVA = "0x1012B9150", Offset = "0x12B9150", VA = "0x1012B9150")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028CF")]
			[Address(RVA = "0x1012B9260", Offset = "0x12B9260", VA = "0x1012B9260")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000042 RID: 66
		// (add) Token: 0x06002C62 RID: 11362 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C63 RID: 11363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000042")]
		public event Action OnSelectingObjectCanNotPlacement
		{
			[Token(Token = "0x60028D0")]
			[Address(RVA = "0x1012B9370", Offset = "0x12B9370", VA = "0x1012B9370")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028D1")]
			[Address(RVA = "0x1012B9480", Offset = "0x12B9480", VA = "0x1012B9480")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000043 RID: 67
		// (add) Token: 0x06002C64 RID: 11364 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C65 RID: 11365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000043")]
		public event Action OnObjectDragStart
		{
			[Token(Token = "0x60028D2")]
			[Address(RVA = "0x1012B9590", Offset = "0x12B9590", VA = "0x1012B9590")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028D3")]
			[Address(RVA = "0x1012B96A0", Offset = "0x12B96A0", VA = "0x1012B96A0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000044 RID: 68
		// (add) Token: 0x06002C66 RID: 11366 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002C67 RID: 11367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000044")]
		public event Action OnObjectDragEnd
		{
			[Token(Token = "0x60028D4")]
			[Address(RVA = "0x1012B97B0", Offset = "0x12B97B0", VA = "0x1012B97B0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60028D5")]
			[Address(RVA = "0x1012B98C0", Offset = "0x12B98C0", VA = "0x1012B98C0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002C68 RID: 11368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028D6")]
		[Address(RVA = "0x1012B99D0", Offset = "0x12B99D0", VA = "0x1012B99D0")]
		private void Awake()
		{
		}

		// Token: 0x06002C69 RID: 11369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028D7")]
		[Address(RVA = "0x1012B9B18", Offset = "0x12B9B18", VA = "0x1012B9B18")]
		private void Update()
		{
		}

		// Token: 0x06002C6A RID: 11370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028D8")]
		[Address(RVA = "0x1012BC4D4", Offset = "0x12BC4D4", VA = "0x1012BC4D4")]
		public void Initialize()
		{
		}

		// Token: 0x06002C6B RID: 11371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028D9")]
		[Address(RVA = "0x1012BC764", Offset = "0x12BC764", VA = "0x1012BC764")]
		public void OnDestroy()
		{
		}

		// Token: 0x06002C6C RID: 11372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028DA")]
		[Address(RVA = "0x1012BC76C", Offset = "0x12BC76C", VA = "0x1012BC76C", Slot = "13")]
		public override void Destroy()
		{
		}

		// Token: 0x06002C6D RID: 11373 RVA: 0x00012EB8 File Offset: 0x000110B8
		[Token(Token = "0x60028DB")]
		[Address(RVA = "0x1012BCA8C", Offset = "0x12BCA8C", VA = "0x1012BCA8C")]
		public bool CheckWithProcDestory()
		{
			return default(bool);
		}

		// Token: 0x06002C6E RID: 11374 RVA: 0x00012ED0 File Offset: 0x000110D0
		[Token(Token = "0x60028DC")]
		[Address(RVA = "0x1012BCB60", Offset = "0x12BCB60", VA = "0x1012BCB60")]
		public bool Prepare(long playerId, bool feditmode)
		{
			return default(bool);
		}

		// Token: 0x06002C6F RID: 11375 RVA: 0x00012EE8 File Offset: 0x000110E8
		[Token(Token = "0x60028DD")]
		[Address(RVA = "0x1012BCC64", Offset = "0x12BCC64", VA = "0x1012BCC64", Slot = "4")]
		public override bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06002C70 RID: 11376 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60028DE")]
		[Address(RVA = "0x1012BCC74", Offset = "0x12BCC74", VA = "0x1012BCC74")]
		public IRoomObjectControll GetDummyObject()
		{
			return null;
		}

		// Token: 0x06002C71 RID: 11377 RVA: 0x00012F00 File Offset: 0x00011100
		[Token(Token = "0x60028DF")]
		[Address(RVA = "0x1012BC23C", Offset = "0x12BC23C", VA = "0x1012BC23C")]
		public bool SetSelectingObj()
		{
			return default(bool);
		}

		// Token: 0x06002C72 RID: 11378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E0")]
		[Address(RVA = "0x1012BAB4C", Offset = "0x12BAB4C", VA = "0x1012BAB4C")]
		public void UpdateMain()
		{
		}

		// Token: 0x06002C73 RID: 11379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E1")]
		[Address(RVA = "0x1012BDBB4", Offset = "0x12BDBB4", VA = "0x1012BDBB4")]
		public void RequestPutAway()
		{
		}

		// Token: 0x06002C74 RID: 11380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E2")]
		[Address(RVA = "0x1012BE06C", Offset = "0x12BE06C", VA = "0x1012BE06C")]
		public void RequestDummyPlacement(eRoomObjectCategory fobjcategory, int objID)
		{
		}

		// Token: 0x06002C75 RID: 11381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E3")]
		[Address(RVA = "0x1012BE1C0", Offset = "0x12BE1C0", VA = "0x1012BE1C0")]
		public void RequestObjectToPlacement(eRoomObjectCategory fobjcategory, int objID)
		{
		}

		// Token: 0x06002C76 RID: 11382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E4")]
		[Address(RVA = "0x1012BE34C", Offset = "0x12BE34C", VA = "0x1012BE34C")]
		public void LinkPlaceObjectToManageID(long flinkmanageid)
		{
		}

		// Token: 0x06002C77 RID: 11383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E5")]
		[Address(RVA = "0x1012BE41C", Offset = "0x12BE41C", VA = "0x1012BE41C")]
		public void RequestSellObject(eRoomObjectCategory objCategory, int objID, IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x06002C78 RID: 11384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E6")]
		[Address(RVA = "0x1012BD21C", Offset = "0x12BD21C", VA = "0x1012BD21C")]
		public void RequestEditMode(bool isEditNew)
		{
		}

		// Token: 0x06002C79 RID: 11385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E7")]
		[Address(RVA = "0x1012BE590", Offset = "0x12BE590", VA = "0x1012BE590")]
		public void RequestCancelEditMode()
		{
		}

		// Token: 0x06002C7A RID: 11386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E8")]
		[Address(RVA = "0x1012BE94C", Offset = "0x12BE94C", VA = "0x1012BE94C")]
		public void RequestCancelSelectObject()
		{
		}

		// Token: 0x06002C7B RID: 11387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028E9")]
		[Address(RVA = "0x1012BEAF8", Offset = "0x12BEAF8", VA = "0x1012BEAF8")]
		public void RequestCancelSelectMode()
		{
		}

		// Token: 0x06002C7C RID: 11388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028EA")]
		[Address(RVA = "0x1012BEC80", Offset = "0x12BEC80", VA = "0x1012BEC80")]
		public void RequestFlip()
		{
		}

		// Token: 0x06002C7D RID: 11389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028EB")]
		[Address(RVA = "0x1012BEE70", Offset = "0x12BEE70", VA = "0x1012BEE70")]
		public void RequestPlacement()
		{
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06002C7E RID: 11390 RVA: 0x00012F18 File Offset: 0x00011118
		// (set) Token: 0x06002C7F RID: 11391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002D0")]
		public bool isBuyAndPlacementDataSending
		{
			[Token(Token = "0x60028EC")]
			[Address(RVA = "0x1012BF714", Offset = "0x12BF714", VA = "0x1012BF714")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60028ED")]
			[Address(RVA = "0x1012BF71C", Offset = "0x12BF71C", VA = "0x1012BF71C")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x06002C80 RID: 11392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028EE")]
		[Address(RVA = "0x1012BF724", Offset = "0x12BF724", VA = "0x1012BF724")]
		private void OnBuyAndPlacementData(long[] mngIDs)
		{
		}

		// Token: 0x06002C81 RID: 11393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028EF")]
		[Address(RVA = "0x1012BF9CC", Offset = "0x12BF9CC", VA = "0x1012BF9CC")]
		private void OnPlacementData(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06002C82 RID: 11394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F0")]
		[Address(RVA = "0x1012BF598", Offset = "0x12BF598", VA = "0x1012BF598")]
		public void SendBuyAndPlacementData(eRoomObjectCategory category, int objID, int amount)
		{
		}

		// Token: 0x06002C83 RID: 11395 RVA: 0x00012F30 File Offset: 0x00011130
		[Token(Token = "0x60028F1")]
		[Address(RVA = "0x1012BF9D4", Offset = "0x12BF9D4", VA = "0x1012BF9D4")]
		public int RequestPutAll()
		{
			return 0;
		}

		// Token: 0x06002C84 RID: 11396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F2")]
		[Address(RVA = "0x1012BFDD8", Offset = "0x12BFDD8", VA = "0x1012BFDD8", Slot = "9")]
		public override void RequestCharaMoveMode(bool modeOn)
		{
		}

		// Token: 0x06002C85 RID: 11397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F3")]
		[Address(RVA = "0x1012BBA20", Offset = "0x12BBA20", VA = "0x1012BBA20")]
		public void UpdateMainEdit()
		{
		}

		// Token: 0x06002C86 RID: 11398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F4")]
		[Address(RVA = "0x1012BFF40", Offset = "0x12BFF40", VA = "0x1012BFF40", Slot = "12")]
		protected override void SortRoom()
		{
		}

		// Token: 0x06002C87 RID: 11399 RVA: 0x00012F48 File Offset: 0x00011148
		[Token(Token = "0x60028F5")]
		[Address(RVA = "0x1012BAA00", Offset = "0x12BAA00", VA = "0x1012BAA00")]
		private bool IsActiveObjSetUp()
		{
			return default(bool);
		}

		// Token: 0x06002C88 RID: 11400 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60028F6")]
		[Address(RVA = "0x1012BA7F8", Offset = "0x12BA7F8", VA = "0x1012BA7F8")]
		public IRoomObjectControll InstantiateObject(int fobjresid, CharacterDefine.eDir fdir, int gridX, int gridY, int floorID, long fmanageid, int subkey)
		{
			return null;
		}

		// Token: 0x06002C89 RID: 11401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F7")]
		[Address(RVA = "0x1012BD900", Offset = "0x12BD900", VA = "0x1012BD900")]
		public void RemoveObject(IRoomObjectControll roomObj)
		{
		}

		// Token: 0x06002C8A RID: 11402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F8")]
		[Address(RVA = "0x1012BCD80", Offset = "0x12BCD80", VA = "0x1012BCD80")]
		private void MoveSelectingObj(IRoomObjectControll ptarget, int gridX, int gridY, int floorID, RoomBuilderBase.eEditObject fobjtype = RoomBuilderBase.eEditObject.Non)
		{
		}

		// Token: 0x06002C8B RID: 11403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028F9")]
		[Address(RVA = "0x1012C0694", Offset = "0x12C0694", VA = "0x1012C0694")]
		public void ChangeBG(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002C8C RID: 11404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028FA")]
		[Address(RVA = "0x1012C0754", Offset = "0x12C0754", VA = "0x1012C0754")]
		public void SendActionToMain(IMainComCommand pcmd)
		{
		}

		// Token: 0x06002C8D RID: 11405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028FB")]
		[Address(RVA = "0x1012C07C4", Offset = "0x12C07C4", VA = "0x1012C07C4", Slot = "10")]
		public override void SendActionToOwner(IMainComCommand cmd)
		{
		}

		// Token: 0x06002C8E RID: 11406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028FC")]
		[Address(RVA = "0x1012C07C8", Offset = "0x12C07C8", VA = "0x1012C07C8", Slot = "8")]
		public override void DestroyObjectQue(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002C8F RID: 11407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028FD")]
		[Address(RVA = "0x1012BC41C", Offset = "0x12BC41C", VA = "0x1012BC41C")]
		private void FlushEventRequest()
		{
		}

		// Token: 0x06002C90 RID: 11408 RVA: 0x00012F60 File Offset: 0x00011160
		[Token(Token = "0x60028FE")]
		[Address(RVA = "0x1012BDAA0", Offset = "0x12BDAA0", VA = "0x1012BDAA0")]
		private bool IsEnableTouchControl()
		{
			return default(bool);
		}

		// Token: 0x06002C91 RID: 11409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028FF")]
		[Address(RVA = "0x1012C0890", Offset = "0x12C0890", VA = "0x1012C0890", Slot = "7")]
		public override void RequestEvent(IRoomEventCommand prequest)
		{
		}

		// Token: 0x06002C92 RID: 11410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002900")]
		[Address(RVA = "0x1012C08C8", Offset = "0x12C08C8", VA = "0x1012C08C8")]
		public void RefreshChara(UserFieldCharaData.UpFieldCharaData[] charaMngTable)
		{
		}

		// Token: 0x06002C93 RID: 11411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002901")]
		[Address(RVA = "0x1012C0A00", Offset = "0x12C0A00", VA = "0x1012C0A00")]
		public void StopCharaManagerAction(bool fstop)
		{
		}

		// Token: 0x06002C94 RID: 11412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002902")]
		[Address(RVA = "0x1012C0A4C", Offset = "0x12C0A4C", VA = "0x1012C0A4C", Slot = "5")]
		public override void ObjRequestToEditMode(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002C95 RID: 11413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002903")]
		[Address(RVA = "0x1012BB61C", Offset = "0x12BB61C", VA = "0x1012BB61C")]
		private void CheckTimeBuilding()
		{
		}

		// Token: 0x06002C96 RID: 11414 RVA: 0x00012F78 File Offset: 0x00011178
		[Token(Token = "0x6002904")]
		[Address(RVA = "0x1012C0BB0", Offset = "0x12C0BB0", VA = "0x1012C0BB0")]
		private bool CheckDayAndNightSwitching()
		{
			return default(bool);
		}

		// Token: 0x06002C97 RID: 11415 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002905")]
		[Address(RVA = "0x1012C0D6C", Offset = "0x12C0D6C", VA = "0x1012C0D6C")]
		public RoomUI GetRoomUI()
		{
			return null;
		}

		// Token: 0x06002C98 RID: 11416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002906")]
		[Address(RVA = "0x1012C0D9C", Offset = "0x12C0D9C", VA = "0x1012C0D9C")]
		public RoomBuilder()
		{
		}

		// Token: 0x04003BCF RID: 15311
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002B1C")]
		private RoomEventQue m_EventQue;

		// Token: 0x04003BD0 RID: 15312
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002B1D")]
		[SerializeField]
		public List<RoomFloorCateory> m_FloorCategory;

		// Token: 0x04003BD1 RID: 15313
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002B1E")]
		[SerializeField]
		private RoomCacheObjectData m_ObjectCache;

		// Token: 0x04003BD2 RID: 15314
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002B1F")]
		private List<IRoomObjectControll> m_ActiveRoomObjHndls;

		// Token: 0x04003BD3 RID: 15315
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002B20")]
		private long m_UserRoomManageID;

		// Token: 0x04003BD4 RID: 15316
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002B21")]
		private IRoomObjectControll m_SelectingObj;

		// Token: 0x04003BD5 RID: 15317
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002B22")]
		private RoomBuilder.EditObjectState m_SelectCategory;

		// Token: 0x04003BD6 RID: 15318
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002B23")]
		private IRoomObjectControll m_NewPlacementRoomObj;

		// Token: 0x04003BD7 RID: 15319
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002B24")]
		private bool m_IsEditNew;

		// Token: 0x04003BD8 RID: 15320
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4002B25")]
		private RoomBuilder.SaveDataOnEditStart m_SaveDataOnEditStart;

		// Token: 0x04003BD9 RID: 15321
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002B26")]
		[HideInInspector]
		public IRoomCharaManager m_CharaManager;

		// Token: 0x04003BDA RID: 15322
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002B27")]
		private bool m_IsDragging;

		// Token: 0x04003BDB RID: 15323
		[Cpp2IlInjected.FieldOffset(Offset = "0xB9")]
		[Token(Token = "0x4002B28")]
		private bool m_IsBuilt;

		// Token: 0x04003BDC RID: 15324
		[Cpp2IlInjected.FieldOffset(Offset = "0xBA")]
		[Token(Token = "0x4002B29")]
		private bool m_EventMode;

		// Token: 0x04003BDE RID: 15326
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002B2B")]
		private byte m_TimeCategory;

		// Token: 0x04003BDF RID: 15327
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4002B2C")]
		public int m_TimeCheckHour;

		// Token: 0x04003BE0 RID: 15328
		[Token(Token = "0x4002B2D")]
		private const int SUNRISE_HOUR = 6;

		// Token: 0x04003BE1 RID: 15329
		[Token(Token = "0x4002B2E")]
		private const int SUNSET_HOUR = 18;

		// Token: 0x04003BE2 RID: 15330
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002B2F")]
		private bool m_ActivePlay;

		// Token: 0x04003BE3 RID: 15331
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002B30")]
		private RoomMain m_Owner;

		// Token: 0x04003BE4 RID: 15332
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002B31")]
		private long m_SelectPlayerId;

		// Token: 0x04003BE5 RID: 15333
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002B32")]
		private bool m_FieldPartyInSchedule;

		// Token: 0x04003BE6 RID: 15334
		[Cpp2IlInjected.FieldOffset(Offset = "0xEC")]
		[Token(Token = "0x4002B33")]
		private RoomBuilder.eMode m_Mode;

		// Token: 0x04003BED RID: 15341
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4002B3A")]
		private int m_MainRayLayerMask;

		// Token: 0x04003BEE RID: 15342
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4002B3B")]
		private IRoomObjectControll.ObjTouchState m_ObjTouch;

		// Token: 0x04003BEF RID: 15343
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002B3C")]
		private Vector2 m_TouchStart;

		// Token: 0x04003BF1 RID: 15345
		[Cpp2IlInjected.FieldOffset(Offset = "0x13C")]
		[Token(Token = "0x4002B3E")]
		private int m_EditingTouchGridX;

		// Token: 0x04003BF2 RID: 15346
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4002B3F")]
		private int m_EditingTouchGridY;

		// Token: 0x04003BF3 RID: 15347
		[Cpp2IlInjected.FieldOffset(Offset = "0x144")]
		[Token(Token = "0x4002B40")]
		private int m_EditingTouchFloor;

		// Token: 0x04003BF4 RID: 15348
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4002B41")]
		private bool m_IsEditingTouchGrid;

		// Token: 0x04003BF5 RID: 15349
		[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
		[Token(Token = "0x4002B42")]
		private int m_EditMaskLayer;

		// Token: 0x04003BF6 RID: 15350
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4002B43")]
		private RoomBuilder.EditObjectState m_EditObjState;

		// Token: 0x02000A25 RID: 2597
		[Token(Token = "0x2000FB1")]
		private struct SaveDataOnEditStart
		{
			// Token: 0x04003BF7 RID: 15351
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006424")]
			public int m_GridX;

			// Token: 0x04003BF8 RID: 15352
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4006425")]
			public int m_GridY;

			// Token: 0x04003BF9 RID: 15353
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006426")]
			public CharacterDefine.eDir m_Dir;
		}

		// Token: 0x02000A26 RID: 2598
		[Token(Token = "0x2000FB2")]
		public enum eMode
		{
			// Token: 0x04003BFB RID: 15355
			[Token(Token = "0x4006428")]
			None = -1,
			// Token: 0x04003BFC RID: 15356
			[Token(Token = "0x4006429")]
			Prepare_First,
			// Token: 0x04003BFD RID: 15357
			[Token(Token = "0x400642A")]
			Prepare_Bg,
			// Token: 0x04003BFE RID: 15358
			[Token(Token = "0x400642B")]
			Prepare_Floor,
			// Token: 0x04003BFF RID: 15359
			[Token(Token = "0x400642C")]
			Prepare_Floor_Wait,
			// Token: 0x04003C00 RID: 15360
			[Token(Token = "0x400642D")]
			Prepare_Placements,
			// Token: 0x04003C01 RID: 15361
			[Token(Token = "0x400642E")]
			Prepare_Placements_Wait,
			// Token: 0x04003C02 RID: 15362
			[Token(Token = "0x400642F")]
			Prepare_Charas,
			// Token: 0x04003C03 RID: 15363
			[Token(Token = "0x4006430")]
			Prepare_Charas_Wait,
			// Token: 0x04003C04 RID: 15364
			[Token(Token = "0x4006431")]
			Main,
			// Token: 0x04003C05 RID: 15365
			[Token(Token = "0x4006432")]
			Main_Edit,
			// Token: 0x04003C06 RID: 15366
			[Token(Token = "0x4006433")]
			Main_EditRetry,
			// Token: 0x04003C07 RID: 15367
			[Token(Token = "0x4006434")]
			Main_CharaMove,
			// Token: 0x04003C08 RID: 15368
			[Token(Token = "0x4006435")]
			Num
		}

		// Token: 0x02000A27 RID: 2599
		[Token(Token = "0x2000FB3")]
		public struct EditObjectState
		{
			// Token: 0x04003C09 RID: 15369
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006436")]
			public RoomBuilderBase.eEditObject m_State;

			// Token: 0x04003C0A RID: 15370
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4006437")]
			public bool m_MenuType;
		}
	}
}
