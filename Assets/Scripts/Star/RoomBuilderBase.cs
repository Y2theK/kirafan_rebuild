﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A28 RID: 2600
	[Token(Token = "0x2000742")]
	[StructLayout(3)]
	public abstract class RoomBuilderBase : MonoBehaviour
	{
		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06002C99 RID: 11417 RVA: 0x00012F90 File Offset: 0x00011190
		[Token(Token = "0x170002D1")]
		public bool flgPlayScriptSound
		{
			[Token(Token = "0x6002907")]
			[Address(RVA = "0x1012C0E70", Offset = "0x12C0E70", VA = "0x1012C0E70")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002C9A RID: 11418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002908")]
		[Address(RVA = "0x1012BAAE4", Offset = "0x12BAAE4", VA = "0x1012BAAE4")]
		public void SetFlgPlayScriptSound(RoomBuilderBase.ePlayScriptSoundId id, bool flg)
		{
		}

		// Token: 0x06002C9B RID: 11419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002909")]
		[Address(RVA = "0x1012C0F00", Offset = "0x12C0F00", VA = "0x1012C0F00")]
		public void SetFlgPlayScriptSoundForUI(bool flg)
		{
		}

		// Token: 0x06002C9C RID: 11420 RVA: 0x00012FA8 File Offset: 0x000111A8
		[Token(Token = "0x600290A")]
		[Address(RVA = "0x1012C0F4C", Offset = "0x12C0F4C", VA = "0x1012C0F4C", Slot = "4")]
		public virtual bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06002C9D RID: 11421 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002D2")]
		public Transform CacheTransform
		{
			[Token(Token = "0x600290B")]
			[Address(RVA = "0x1012C0F54", Offset = "0x12C0F54", VA = "0x1012C0F54")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002C9E RID: 11422 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600290C")]
		[Address(RVA = "0x1012C0F5C", Offset = "0x12C0F5C", VA = "0x1012C0F5C")]
		public RoomGridState GetGridStatus(int floorNo)
		{
			return null;
		}

		// Token: 0x06002C9F RID: 11423 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600290D")]
		[Address(RVA = "0x1012C0FAC", Offset = "0x12C0FAC", VA = "0x1012C0FAC")]
		public IRoomFloorManager GetFloorManager(int findex)
		{
			return null;
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06002CA0 RID: 11424 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002D3")]
		public GameObject RoomPickMatchPrefab
		{
			[Token(Token = "0x600290E")]
			[Address(RVA = "0x1012C0FFC", Offset = "0x12C0FFC", VA = "0x1012C0FFC")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002CA1 RID: 11425 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600290F")]
		[Address(RVA = "0x1012C1004", Offset = "0x12C1004", VA = "0x1012C1004")]
		public IRoomObjectControll GetBG()
		{
			return null;
		}

		// Token: 0x06002CA2 RID: 11426 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002910")]
		[Address(RVA = "0x1012C100C", Offset = "0x12C100C", VA = "0x1012C100C")]
		public Camera GetCamera()
		{
			return null;
		}

		// Token: 0x06002CA3 RID: 11427 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002911")]
		[Address(RVA = "0x1012C1014", Offset = "0x12C1014", VA = "0x1012C1014")]
		public RoomGameCamera GetRoomCamera()
		{
			return null;
		}

		// Token: 0x06002CA4 RID: 11428 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002912")]
		[Address(RVA = "0x1012C101C", Offset = "0x12C101C", VA = "0x1012C101C")]
		public GameObject GetHUDObject()
		{
			return null;
		}

		// Token: 0x06002CA5 RID: 11429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002913")]
		[Address(RVA = "0x1012C1024", Offset = "0x12C1024", VA = "0x1012C1024", Slot = "5")]
		public virtual void ObjRequestToEditMode(IRoomObjectControll obj)
		{
		}

		// Token: 0x06002CA6 RID: 11430 RVA: 0x00012FC0 File Offset: 0x000111C0
		[Token(Token = "0x6002914")]
		[Address(RVA = "0x1012C1028", Offset = "0x12C1028", VA = "0x1012C1028")]
		public int GetActiveObjectNum(int floorId)
		{
			return 0;
		}

		// Token: 0x06002CA7 RID: 11431 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002915")]
		[Address(RVA = "0x1012C1098", Offset = "0x12C1098", VA = "0x1012C1098")]
		public IRoomObjectControll GetActiveObjectAt(int floorId, int index)
		{
			return null;
		}

		// Token: 0x06002CA8 RID: 11432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002916")]
		[Address(RVA = "0x1012C1110", Offset = "0x12C1110", VA = "0x1012C1110")]
		public void AttachSortObject(IRoomObjectControll obj, int compkey, bool isChara)
		{
		}

		// Token: 0x06002CA9 RID: 11433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002917")]
		[Address(RVA = "0x1012C11B8", Offset = "0x12C11B8", VA = "0x1012C11B8")]
		public void DetachSortObject(IRoomObjectControll obj)
		{
		}

		// Token: 0x06002CAA RID: 11434 RVA: 0x00012FD8 File Offset: 0x000111D8
		[Token(Token = "0x6002918")]
		[Address(RVA = "0x1012C1248", Offset = "0x12C1248", VA = "0x1012C1248")]
		public bool CheckObjectOnGrid(int gridX, int gridY, int floorID)
		{
			return default(bool);
		}

		// Token: 0x06002CAB RID: 11435 RVA: 0x00012FF0 File Offset: 0x000111F0
		[Token(Token = "0x6002919")]
		[Address(RVA = "0x1012BDAB0", Offset = "0x12BDAB0", VA = "0x1012BDAB0")]
		public IVector3 GetHitGridInfo(Collider col)
		{
			return default(IVector3);
		}

		// Token: 0x06002CAC RID: 11436 RVA: 0x00013008 File Offset: 0x00011208
		[Token(Token = "0x600291A")]
		[Address(RVA = "0x1012BFE94", Offset = "0x12BFE94", VA = "0x1012BFE94")]
		protected bool GetSelectObjToFloorCrossPoint(ref IVector3 pout, Vector3 objpos, Vector3 pos, RoomBuilderBase.eEditObject objtype)
		{
			return default(bool);
		}

		// Token: 0x06002CAD RID: 11437 RVA: 0x00013020 File Offset: 0x00011220
		[Token(Token = "0x600291B")]
		[Address(RVA = "0x1012C12D0", Offset = "0x12C12D0", VA = "0x1012C12D0")]
		public bool GetFloorPoint(ref IVector3 pout, Vector2 ftouchpos, Vector3 fobjpos, int floorid, RoomBuilderBase.eEditObject fobjtype = RoomBuilderBase.eEditObject.Floor)
		{
			return default(bool);
		}

		// Token: 0x06002CAE RID: 11438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600291C")]
		[Address(RVA = "0x1012C1660", Offset = "0x12C1660", VA = "0x1012C1660")]
		public void SetCameraToDebugMode(bool fmode)
		{
		}

		// Token: 0x06002CAF RID: 11439
		[Token(Token = "0x600291D")]
		[Address(Slot = "6")]
		public abstract IRoomCharaManager GetCharaManager();

		// Token: 0x06002CB0 RID: 11440
		[Token(Token = "0x600291E")]
		[Address(Slot = "7")]
		public abstract void RequestEvent(IRoomEventCommand prequest);

		// Token: 0x06002CB1 RID: 11441
		[Token(Token = "0x600291F")]
		[Address(Slot = "8")]
		public abstract void DestroyObjectQue(IRoomObjectControll obj);

		// Token: 0x06002CB2 RID: 11442
		[Token(Token = "0x6002920")]
		[Address(Slot = "9")]
		public abstract void RequestCharaMoveMode(bool modeOn);

		// Token: 0x06002CB3 RID: 11443
		[Token(Token = "0x6002921")]
		[Address(Slot = "10")]
		public abstract void SendActionToOwner(IMainComCommand cmd);

		// Token: 0x06002CB4 RID: 11444
		[Token(Token = "0x6002922")]
		[Address(Slot = "11")]
		public abstract int GetFloorNO();

		// Token: 0x06002CB5 RID: 11445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002923")]
		[Address(RVA = "0x1012C1698", Offset = "0x12C1698", VA = "0x1012C1698", Slot = "12")]
		protected virtual void SortRoom()
		{
		}

		// Token: 0x06002CB6 RID: 11446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002924")]
		[Address(RVA = "0x1012C169C", Offset = "0x12C169C", VA = "0x1012C169C", Slot = "13")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06002CB7 RID: 11447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002925")]
		[Address(RVA = "0x1012C16A0", Offset = "0x12C16A0", VA = "0x1012C16A0")]
		private void LateUpdate()
		{
		}

		// Token: 0x06002CB8 RID: 11448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002926")]
		[Address(RVA = "0x1012C0E10", Offset = "0x12C0E10", VA = "0x1012C0E10")]
		protected RoomBuilderBase()
		{
		}

		// Token: 0x04003C0B RID: 15371
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B44")]
		[SerializeField]
		public RoomGameCamera m_RoomCamera;

		// Token: 0x04003C0C RID: 15372
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B45")]
		[SerializeField]
		protected GameObject m_RoomPickMatchPrefab;

		// Token: 0x04003C0D RID: 15373
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B46")]
		[SerializeField]
		protected GameObject m_CharaHudObject;

		// Token: 0x04003C0E RID: 15374
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002B47")]
		protected RoomGridState[] m_GridStatus;

		// Token: 0x04003C0F RID: 15375
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002B48")]
		protected IRoomFloorManager[] m_FloorObject;

		// Token: 0x04003C10 RID: 15376
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002B49")]
		protected IRoomObjectControll m_Bg;

		// Token: 0x04003C11 RID: 15377
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002B4A")]
		protected Camera m_Camera;

		// Token: 0x04003C12 RID: 15378
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002B4B")]
		protected Transform m_Transform;

		// Token: 0x04003C13 RID: 15379
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002B4C")]
		private bool[] m_flgPlayScriptSounds;

		// Token: 0x02000A29 RID: 2601
		[Token(Token = "0x2000FB4")]
		public enum eEditObject
		{
			// Token: 0x04003C15 RID: 15381
			[Token(Token = "0x4006439")]
			Non,
			// Token: 0x04003C16 RID: 15382
			[Token(Token = "0x400643A")]
			Floor,
			// Token: 0x04003C17 RID: 15383
			[Token(Token = "0x400643B")]
			Wall,
			// Token: 0x04003C18 RID: 15384
			[Token(Token = "0x400643C")]
			Msgboard,
			// Token: 0x04003C19 RID: 15385
			[Token(Token = "0x400643D")]
			Chara
		}

		// Token: 0x02000A2A RID: 2602
		[Token(Token = "0x2000FB5")]
		public enum ePlayScriptSoundId
		{
			// Token: 0x04003C1B RID: 15387
			[Token(Token = "0x400643F")]
			System,
			// Token: 0x04003C1C RID: 15388
			[Token(Token = "0x4006440")]
			UI,
			// Token: 0x04003C1D RID: 15389
			[Token(Token = "0x4006441")]
			Max
		}
	}
}
