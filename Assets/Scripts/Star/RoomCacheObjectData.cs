﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A54 RID: 2644
	[Token(Token = "0x200075D")]
	[Serializable]
	[StructLayout(3)]
	public class RoomCacheObjectData
	{
		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06002D83 RID: 11651 RVA: 0x000136C8 File Offset: 0x000118C8
		[Token(Token = "0x170002D8")]
		public int CacheSize
		{
			[Token(Token = "0x60029D7")]
			[Address(RVA = "0x1012C16AC", Offset = "0x12C16AC", VA = "0x1012C16AC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x06002D84 RID: 11652 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002D9")]
		public GameObject[] CacheObjects
		{
			[Token(Token = "0x60029D8")]
			[Address(RVA = "0x1012C16B4", Offset = "0x12C16B4", VA = "0x1012C16B4")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002D85 RID: 11653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029D9")]
		[Address(RVA = "0x1012BC508", Offset = "0x12BC508", VA = "0x1012BC508")]
		public void Initialize(Transform parent)
		{
		}

		// Token: 0x06002D86 RID: 11654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029DA")]
		[Address(RVA = "0x1012C16BC", Offset = "0x12C16BC", VA = "0x1012C16BC")]
		public void Destroy()
		{
		}

		// Token: 0x06002D87 RID: 11655 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029DB")]
		[Address(RVA = "0x1012C025C", Offset = "0x12C025C", VA = "0x1012C025C")]
		public GameObject GetNextObjectInCache()
		{
			return null;
		}

		// Token: 0x06002D88 RID: 11656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029DC")]
		[Address(RVA = "0x1012C0660", Offset = "0x12C0660", VA = "0x1012C0660")]
		public void BackObject(GameObject pobj)
		{
		}

		// Token: 0x06002D89 RID: 11657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029DD")]
		[Address(RVA = "0x1012C17D4", Offset = "0x12C17D4", VA = "0x1012C17D4")]
		public RoomCacheObjectData()
		{
		}

		// Token: 0x04003D0B RID: 15627
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002BFD")]
		[SerializeField]
		private int m_CacheSize;

		// Token: 0x04003D0C RID: 15628
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BFE")]
		private GameObject[] m_Objects;

		// Token: 0x04003D0D RID: 15629
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BFF")]
		private Transform m_Parent;
	}
}
