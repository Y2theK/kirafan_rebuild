﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F1 RID: 2545
	[Token(Token = "0x2000723")]
	[StructLayout(3)]
	public class RoomCharaActEnjoy : IRoomCharaAct
	{
		// Token: 0x06002A8F RID: 10895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600271E")]
		[Address(RVA = "0x1012C17E4", Offset = "0x12C17E4", VA = "0x1012C17E4")]
		public void RequestEnjoyMode(RoomObjectCtrlChara pbase)
		{
		}

		// Token: 0x06002A90 RID: 10896 RVA: 0x00012108 File Offset: 0x00010308
		[Token(Token = "0x600271F")]
		[Address(RVA = "0x1012C181C", Offset = "0x12C181C", VA = "0x1012C181C", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A91 RID: 10897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002720")]
		[Address(RVA = "0x1012C1858", Offset = "0x12C1858", VA = "0x1012C1858")]
		public RoomCharaActEnjoy()
		{
		}
	}
}
