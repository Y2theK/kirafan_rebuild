﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F2 RID: 2546
	[Token(Token = "0x2000724")]
	[StructLayout(3)]
	public class RoomCharaActIdle : IRoomCharaAct
	{
		// Token: 0x06002A92 RID: 10898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002721")]
		[Address(RVA = "0x1012C1860", Offset = "0x12C1860", VA = "0x1012C1860")]
		public void RequestIdleMode(RoomObjectCtrlChara pbase, bool foveranm, float foffset = 1f)
		{
		}

		// Token: 0x06002A93 RID: 10899 RVA: 0x00012120 File Offset: 0x00010320
		[Token(Token = "0x6002722")]
		[Address(RVA = "0x1012C199C", Offset = "0x12C199C", VA = "0x1012C199C", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A94 RID: 10900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002723")]
		[Address(RVA = "0x1012C1A28", Offset = "0x12C1A28", VA = "0x1012C1A28")]
		public RoomCharaActIdle()
		{
		}

		// Token: 0x04003AC4 RID: 15044
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A52")]
		private float m_waitTimer;
	}
}
