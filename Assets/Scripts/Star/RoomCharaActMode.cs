﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A03 RID: 2563
	[Token(Token = "0x200072F")]
	[StructLayout(3)]
	public class RoomCharaActMode
	{
		// Token: 0x06002B0C RID: 11020 RVA: 0x00012558 File Offset: 0x00010758
		[Token(Token = "0x600278D")]
		[Address(RVA = "0x1012B7DD0", Offset = "0x12B7DD0", VA = "0x1012B7DD0")]
		public static RoomCharaActMode.eMode GetNameToMode(string fname)
		{
			return RoomCharaActMode.eMode.Idle;
		}

		// Token: 0x06002B0D RID: 11021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600278E")]
		[Address(RVA = "0x1012C1A30", Offset = "0x12C1A30", VA = "0x1012C1A30")]
		public RoomCharaActMode()
		{
		}

		// Token: 0x06002B0E RID: 11022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600278F")]
		[Address(RVA = "0x1012C1A94", Offset = "0x12C1A94", VA = "0x1012C1A94")]
		public void ResetNextActionPer(RoomCharaActMode.eMode fmode)
		{
		}

		// Token: 0x04003AFC RID: 15100
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A7E")]
		public int m_NextActionNum;

		// Token: 0x04003AFD RID: 15101
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A7F")]
		public RoomCharaActMode.NextActionState[] m_NextActionPer;

		// Token: 0x02000A04 RID: 2564
		[Token(Token = "0x2000FA2")]
		public enum eMode
		{
			// Token: 0x04003AFF RID: 15103
			[Token(Token = "0x40063F0")]
			None = -1,
			// Token: 0x04003B00 RID: 15104
			[Token(Token = "0x40063F1")]
			Idle,
			// Token: 0x04003B01 RID: 15105
			[Token(Token = "0x40063F2")]
			Move,
			// Token: 0x04003B02 RID: 15106
			[Token(Token = "0x40063F3")]
			Tweet,
			// Token: 0x04003B03 RID: 15107
			[Token(Token = "0x40063F4")]
			Enjoy,
			// Token: 0x04003B04 RID: 15108
			[Token(Token = "0x40063F5")]
			Rejoice,
			// Token: 0x04003B05 RID: 15109
			[Token(Token = "0x40063F6")]
			ObjEvent,
			// Token: 0x04003B06 RID: 15110
			[Token(Token = "0x40063F7")]
			Sleep,
			// Token: 0x04003B07 RID: 15111
			[Token(Token = "0x40063F8")]
			Pic,
			// Token: 0x04003B08 RID: 15112
			[Token(Token = "0x40063F9")]
			RoomOut,
			// Token: 0x04003B09 RID: 15113
			[Token(Token = "0x40063FA")]
			Max
		}

		// Token: 0x02000A05 RID: 2565
		[Token(Token = "0x2000FA3")]
		public struct NextActionState
		{
			// Token: 0x04003B0A RID: 15114
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063FB")]
			public RoomCharaActMode.eMode m_Type;

			// Token: 0x04003B0B RID: 15115
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40063FC")]
			public float m_Per;
		}
	}
}
