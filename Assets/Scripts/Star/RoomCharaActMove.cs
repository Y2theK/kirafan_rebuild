﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009F3 RID: 2547
	[Token(Token = "0x2000725")]
	[StructLayout(3)]
	public class RoomCharaActMove : IRoomCharaAct
	{
		// Token: 0x06002A95 RID: 10901 RVA: 0x00012138 File Offset: 0x00010338
		[Token(Token = "0x6002724")]
		[Address(RVA = "0x1012C1BE8", Offset = "0x12C1BE8", VA = "0x1012C1BE8", Slot = "5")]
		public override bool IsMoveCost(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A96 RID: 10902 RVA: 0x00012150 File Offset: 0x00010350
		[Token(Token = "0x6002725")]
		[Address(RVA = "0x1012C1CAC", Offset = "0x12C1CAC", VA = "0x1012C1CAC")]
		public bool RequestMoveMode(RoomObjectCtrlChara pbase, Vector2 fendPos)
		{
			return default(bool);
		}

		// Token: 0x06002A97 RID: 10903 RVA: 0x00012168 File Offset: 0x00010368
		[Token(Token = "0x6002726")]
		[Address(RVA = "0x1012C1E58", Offset = "0x12C1E58", VA = "0x1012C1E58", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A98 RID: 10904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002727")]
		[Address(RVA = "0x1012C2348", Offset = "0x12C2348", VA = "0x1012C2348")]
		public RoomCharaActMove()
		{
		}

		// Token: 0x04003AC5 RID: 15045
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A53")]
		private List<Vector2> m_Paths;

		// Token: 0x04003AC6 RID: 15046
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A54")]
		private int m_PathIndex;

		// Token: 0x04003AC7 RID: 15047
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002A55")]
		private Vector2 m_MoveBase;

		// Token: 0x04003AC8 RID: 15048
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002A56")]
		private Vector2 m_MoveTarget;

		// Token: 0x04003AC9 RID: 15049
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002A57")]
		private float m_MoveTime;
	}
}
