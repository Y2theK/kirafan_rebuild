﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F4 RID: 2548
	[Token(Token = "0x2000726")]
	[StructLayout(3)]
	public class RoomCharaActNone : IRoomCharaAct
	{
		// Token: 0x06002A99 RID: 10905 RVA: 0x00012180 File Offset: 0x00010380
		[Token(Token = "0x6002728")]
		[Address(RVA = "0x1012C2350", Offset = "0x12C2350", VA = "0x1012C2350", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A9A RID: 10906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002729")]
		[Address(RVA = "0x1012C23A0", Offset = "0x12C23A0", VA = "0x1012C23A0")]
		public RoomCharaActNone()
		{
		}
	}
}
