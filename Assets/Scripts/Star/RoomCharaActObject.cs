﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F5 RID: 2549
	[Token(Token = "0x2000727")]
	[StructLayout(3)]
	public class RoomCharaActObject : IRoomCharaAct
	{
		// Token: 0x06002A9B RID: 10907 RVA: 0x00012198 File Offset: 0x00010398
		[Token(Token = "0x600272A")]
		[Address(RVA = "0x1012C23A8", Offset = "0x12C23A8", VA = "0x1012C23A8")]
		public bool RequestObjectAct(RoomObjectCtrlChara pbase, IRoomObjectControll ptarget)
		{
			return default(bool);
		}

		// Token: 0x06002A9C RID: 10908 RVA: 0x000121B0 File Offset: 0x000103B0
		[Token(Token = "0x600272B")]
		[Address(RVA = "0x1012C24D4", Offset = "0x12C24D4", VA = "0x1012C24D4", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002A9D RID: 10909 RVA: 0x000121C8 File Offset: 0x000103C8
		[Token(Token = "0x600272C")]
		[Address(RVA = "0x1012C26E0", Offset = "0x12C26E0", VA = "0x1012C26E0", Slot = "7")]
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return default(bool);
		}

		// Token: 0x06002A9E RID: 10910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600272D")]
		[Address(RVA = "0x1012C2778", Offset = "0x12C2778", VA = "0x1012C2778")]
		public RoomCharaActObject()
		{
		}

		// Token: 0x04003ACA RID: 15050
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A58")]
		private ICharaRoomAction m_EventAction;
	}
}
