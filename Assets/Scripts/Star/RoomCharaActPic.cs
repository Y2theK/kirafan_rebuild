﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F6 RID: 2550
	[Token(Token = "0x2000728")]
	[StructLayout(3)]
	public class RoomCharaActPic : IRoomCharaAct
	{
		// Token: 0x06002A9F RID: 10911 RVA: 0x000121E0 File Offset: 0x000103E0
		[Token(Token = "0x600272E")]
		[Address(RVA = "0x1012C2780", Offset = "0x12C2780", VA = "0x1012C2780")]
		public bool IsFloating()
		{
			return default(bool);
		}

		// Token: 0x06002AA0 RID: 10912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600272F")]
		[Address(RVA = "0x1012C2790", Offset = "0x12C2790", VA = "0x1012C2790")]
		public void RequestLiftUp(RoomObjectCtrlChara pbase, bool foveranm, RoomObjectPickMatch roomObjPick)
		{
		}

		// Token: 0x06002AA1 RID: 10913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002730")]
		[Address(RVA = "0x1012C2878", Offset = "0x12C2878", VA = "0x1012C2878")]
		public void RequestPullDown(RoomObjectCtrlChara pbase)
		{
		}

		// Token: 0x06002AA2 RID: 10914 RVA: 0x000121F8 File Offset: 0x000103F8
		[Token(Token = "0x6002731")]
		[Address(RVA = "0x1012C2950", Offset = "0x12C2950", VA = "0x1012C2950", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AA3 RID: 10915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002732")]
		[Address(RVA = "0x1012C2B44", Offset = "0x12C2B44", VA = "0x1012C2B44")]
		public RoomCharaActPic()
		{
		}

		// Token: 0x04003ACB RID: 15051
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A59")]
		private RoomCharaActPic.eStep m_step;

		// Token: 0x04003ACC RID: 15052
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A5A")]
		private RoomObjectPickMatch m_RoomObjPick;

		// Token: 0x020009F7 RID: 2551
		[Token(Token = "0x2000F9C")]
		private enum eStep
		{
			// Token: 0x04003ACE RID: 15054
			[Token(Token = "0x40063E4")]
			LiftUp,
			// Token: 0x04003ACF RID: 15055
			[Token(Token = "0x40063E5")]
			Float,
			// Token: 0x04003AD0 RID: 15056
			[Token(Token = "0x40063E6")]
			PullDown
		}
	}
}
