﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F8 RID: 2552
	[Token(Token = "0x2000729")]
	[StructLayout(3)]
	public class RoomCharaActRejoice : IRoomCharaAct
	{
		// Token: 0x06002AA4 RID: 10916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002733")]
		[Address(RVA = "0x1012C2B4C", Offset = "0x12C2B4C", VA = "0x1012C2B4C")]
		public void RequestRejoiceMode(RoomObjectCtrlChara pbase)
		{
		}

		// Token: 0x06002AA5 RID: 10917 RVA: 0x00012210 File Offset: 0x00010410
		[Token(Token = "0x6002734")]
		[Address(RVA = "0x1012C2B84", Offset = "0x12C2B84", VA = "0x1012C2B84", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AA6 RID: 10918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002735")]
		[Address(RVA = "0x1012C2BC0", Offset = "0x12C2BC0", VA = "0x1012C2BC0")]
		public RoomCharaActRejoice()
		{
		}
	}
}
