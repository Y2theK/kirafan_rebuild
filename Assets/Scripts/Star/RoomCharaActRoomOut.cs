﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009F9 RID: 2553
	[Token(Token = "0x200072A")]
	[StructLayout(3)]
	public class RoomCharaActRoomOut : IRoomCharaAct
	{
		// Token: 0x06002AA7 RID: 10919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002736")]
		[Address(RVA = "0x1012C2BC8", Offset = "0x12C2BC8", VA = "0x1012C2BC8")]
		public void RequestRoomOut(RoomObjectCtrlChara pbase, int factionno)
		{
		}

		// Token: 0x06002AA8 RID: 10920 RVA: 0x00012228 File Offset: 0x00010428
		[Token(Token = "0x6002737")]
		[Address(RVA = "0x1012C2C04", Offset = "0x12C2C04", VA = "0x1012C2C04", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AA9 RID: 10921 RVA: 0x00012240 File Offset: 0x00010440
		[Token(Token = "0x6002738")]
		[Address(RVA = "0x1012C2C58", Offset = "0x12C2C58", VA = "0x1012C2C58", Slot = "7")]
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return default(bool);
		}

		// Token: 0x06002AAA RID: 10922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002739")]
		[Address(RVA = "0x1012C2C60", Offset = "0x12C2C60", VA = "0x1012C2C60")]
		public RoomCharaActRoomOut()
		{
		}

		// Token: 0x04003AD1 RID: 15057
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A5B")]
		private bool m_DeadUp;
	}
}
