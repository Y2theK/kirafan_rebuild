﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009FA RID: 2554
	[Token(Token = "0x200072B")]
	[StructLayout(3)]
	public class RoomCharaActSleep : IRoomCharaAct
	{
		// Token: 0x06002AAB RID: 10923 RVA: 0x00012258 File Offset: 0x00010458
		[Token(Token = "0x600273A")]
		[Address(RVA = "0x1012C2C68", Offset = "0x12C2C68", VA = "0x1012C2C68")]
		public bool RequestSleep(RoomObjectCtrlChara pbase, int factionno)
		{
			return default(bool);
		}

		// Token: 0x06002AAC RID: 10924 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600273B")]
		[Address(RVA = "0x1012C2E84", Offset = "0x12C2E84", VA = "0x1012C2E84")]
		private IRoomObjectControll SearchEmptyBed(RoomObjectCtrlChara chara, int idx)
		{
			return null;
		}

		// Token: 0x06002AAD RID: 10925 RVA: 0x00012270 File Offset: 0x00010470
		[Token(Token = "0x600273C")]
		[Address(RVA = "0x1012C3124", Offset = "0x12C3124", VA = "0x1012C3124")]
		private int GridCompare(RoomObjectCtrlBedding a, RoomObjectCtrlBedding b)
		{
			return 0;
		}

		// Token: 0x06002AAE RID: 10926 RVA: 0x00012288 File Offset: 0x00010488
		[Token(Token = "0x600273D")]
		[Address(RVA = "0x1012C3274", Offset = "0x12C3274", VA = "0x1012C3274", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AAF RID: 10927 RVA: 0x000122A0 File Offset: 0x000104A0
		[Token(Token = "0x600273E")]
		[Address(RVA = "0x1012C337C", Offset = "0x12C337C", VA = "0x1012C337C", Slot = "7")]
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return default(bool);
		}

		// Token: 0x06002AB0 RID: 10928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600273F")]
		[Address(RVA = "0x1012C3414", Offset = "0x12C3414", VA = "0x1012C3414")]
		public RoomCharaActSleep()
		{
		}

		// Token: 0x04003AD2 RID: 15058
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A5C")]
		private ICharaRoomAction m_EventAction;
	}
}
