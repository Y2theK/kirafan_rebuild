﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009FB RID: 2555
	[Token(Token = "0x200072C")]
	[StructLayout(3)]
	public class RoomCharaActTweet : IRoomCharaAct
	{
		// Token: 0x06002AB1 RID: 10929 RVA: 0x000122B8 File Offset: 0x000104B8
		[Token(Token = "0x6002740")]
		[Address(RVA = "0x1012C341C", Offset = "0x12C341C", VA = "0x1012C341C")]
		private TweetListDB_Param GetTweetParam(RoomObjectCtrlChara pbase)
		{
			return default(TweetListDB_Param);
		}

		// Token: 0x06002AB2 RID: 10930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002741")]
		[Address(RVA = "0x1012C35E0", Offset = "0x12C35E0", VA = "0x1012C35E0")]
		public void RequestTweetMode(RoomObjectCtrlChara pbase)
		{
		}

		// Token: 0x06002AB3 RID: 10931 RVA: 0x000122D0 File Offset: 0x000104D0
		[Token(Token = "0x6002742")]
		[Address(RVA = "0x1012C3890", Offset = "0x12C3890", VA = "0x1012C3890", Slot = "4")]
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002AB4 RID: 10932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002743")]
		[Address(RVA = "0x1012C3898", Offset = "0x12C3898", VA = "0x1012C3898")]
		public RoomCharaActTweet()
		{
		}
	}
}
