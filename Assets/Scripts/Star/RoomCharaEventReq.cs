﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A06 RID: 2566
	[Token(Token = "0x2000730")]
	[StructLayout(3)]
	public class RoomCharaEventReq
	{
		// Token: 0x06002B0F RID: 11023 RVA: 0x00012570 File Offset: 0x00010770
		[Token(Token = "0x6002790")]
		[Address(RVA = "0x1012C38A0", Offset = "0x12C38A0", VA = "0x1012C38A0")]
		public bool IsIrqRequest()
		{
			return default(bool);
		}

		// Token: 0x06002B10 RID: 11024 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002791")]
		[Address(RVA = "0x1012C38A8", Offset = "0x12C38A8", VA = "0x1012C38A8")]
		public RoomActionCommand GetCommand()
		{
			return null;
		}

		// Token: 0x06002B11 RID: 11025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002792")]
		[Address(RVA = "0x1012C38B0", Offset = "0x12C38B0", VA = "0x1012C38B0")]
		public void ClrCommand()
		{
		}

		// Token: 0x06002B12 RID: 11026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002793")]
		[Address(RVA = "0x1012C38BC", Offset = "0x12C38BC", VA = "0x1012C38BC")]
		public void ClearReq()
		{
		}

		// Token: 0x06002B13 RID: 11027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002794")]
		[Address(RVA = "0x1012C38C4", Offset = "0x12C38C4", VA = "0x1012C38C4")]
		public void CancelEventCommand()
		{
		}

		// Token: 0x06002B14 RID: 11028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002795")]
		[Address(RVA = "0x1012C38E4", Offset = "0x12C38E4", VA = "0x1012C38E4")]
		public void RequestIrqCommand(RoomActionCommand pcmd)
		{
		}

		// Token: 0x06002B15 RID: 11029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002796")]
		[Address(RVA = "0x1012C38F4", Offset = "0x12C38F4", VA = "0x1012C38F4")]
		public RoomCharaEventReq()
		{
		}

		// Token: 0x04003B0C RID: 15116
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A80")]
		public bool m_CommandSend;

		// Token: 0x04003B0D RID: 15117
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4002A81")]
		public bool m_IrqRequest;

		// Token: 0x04003B0E RID: 15118
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A82")]
		public RoomActionCommand m_ActiveEventCmd;
	}
}
