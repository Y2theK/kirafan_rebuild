﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x020009A9 RID: 2473
	[Token(Token = "0x2000700")]
	[StructLayout(3)]
	public class RoomCharaManager : IRoomCharaManager
	{
		// Token: 0x06002901 RID: 10497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B3")]
		[Address(RVA = "0x1012C38FC", Offset = "0x12C38FC", VA = "0x1012C38FC", Slot = "4")]
		public override void SetLinkManager(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002902 RID: 10498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B4")]
		[Address(RVA = "0x1012C39E4", Offset = "0x12C39E4", VA = "0x1012C39E4", Slot = "5")]
		public override void BackLinkManager()
		{
		}

		// Token: 0x06002903 RID: 10499 RVA: 0x00011580 File Offset: 0x0000F780
		[Token(Token = "0x60025B5")]
		[Address(RVA = "0x1012C3A3C", Offset = "0x12C3A3C", VA = "0x1012C3A3C", Slot = "6")]
		public override bool IsBuild()
		{
			return default(bool);
		}

		// Token: 0x06002904 RID: 10500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B6")]
		[Address(RVA = "0x1012C3C28", Offset = "0x12C3C28", VA = "0x1012C3C28")]
		private void CheckHourEventAction(RoomGreet.eCategory fevent)
		{
		}

		// Token: 0x06002905 RID: 10501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B7")]
		[Address(RVA = "0x1012C3FCC", Offset = "0x12C3FCC", VA = "0x1012C3FCC")]
		private void AddRoomCharacter(long fmngid, int froomid)
		{
		}

		// Token: 0x06002906 RID: 10502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B8")]
		[Address(RVA = "0x1012C4654", Offset = "0x12C4654", VA = "0x1012C4654")]
		private void Update()
		{
		}

		// Token: 0x06002907 RID: 10503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025B9")]
		[Address(RVA = "0x1012C52C0", Offset = "0x12C52C0", VA = "0x1012C52C0", Slot = "10")]
		public override void DeleteChara()
		{
		}

		// Token: 0x06002908 RID: 10504 RVA: 0x00011598 File Offset: 0x0000F798
		[Token(Token = "0x60025BA")]
		[Address(RVA = "0x1012C54B8", Offset = "0x12C54B8", VA = "0x1012C54B8", Slot = "11")]
		public override bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06002909 RID: 10505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025BB")]
		[Address(RVA = "0x1012C5548", Offset = "0x12C5548", VA = "0x1012C5548", Slot = "13")]
		public override void RefreshCharaCall()
		{
		}

		// Token: 0x0600290A RID: 10506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025BC")]
		[Address(RVA = "0x1012C55BC", Offset = "0x12C55BC", VA = "0x1012C55BC", Slot = "12")]
		public override void DeleteCharacter(long fmanageid, bool fcutout = false)
		{
		}

		// Token: 0x0600290B RID: 10507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025BD")]
		[Address(RVA = "0x1012C5AE8", Offset = "0x12C5AE8", VA = "0x1012C5AE8")]
		private void CheckRoomInManageChara(long fmanageid, int froomid, bool fcutout)
		{
		}

		// Token: 0x0600290C RID: 10508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025BE")]
		[Address(RVA = "0x1012C5C7C", Offset = "0x12C5C7C", VA = "0x1012C5C7C", Slot = "7")]
		public override void ResetState()
		{
		}

		// Token: 0x0600290D RID: 10509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025BF")]
		[Address(RVA = "0x1012C5E78", Offset = "0x12C5E78", VA = "0x1012C5E78", Slot = "8")]
		public override void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
		}

		// Token: 0x0600290E RID: 10510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C0")]
		[Address(RVA = "0x1012C6054", Offset = "0x12C6054", VA = "0x1012C6054", Slot = "9")]
		public override void SetUpRoomCharacter()
		{
		}

		// Token: 0x0600290F RID: 10511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C1")]
		[Address(RVA = "0x1012C4E18", Offset = "0x12C4E18", VA = "0x1012C4E18")]
		private void CheckSubMemberListUp()
		{
		}

		// Token: 0x06002910 RID: 10512 RVA: 0x000115B0 File Offset: 0x0000F7B0
		[Token(Token = "0x60025C2")]
		[Address(RVA = "0x1012C6180", Offset = "0x12C6180", VA = "0x1012C6180")]
		private bool IsRoomMateNamed(eCharaNamedType fnametype)
		{
			return default(bool);
		}

		// Token: 0x06002911 RID: 10513 RVA: 0x000115C8 File Offset: 0x0000F7C8
		[Token(Token = "0x60025C3")]
		[Address(RVA = "0x1012C6260", Offset = "0x12C6260", VA = "0x1012C6260")]
		public bool SetSleepFloor(long fmanageid, bool finit)
		{
			return default(bool);
		}

		// Token: 0x06002912 RID: 10514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C4")]
		[Address(RVA = "0x1012C6334", Offset = "0x12C6334", VA = "0x1012C6334")]
		public void ClrSleepFloor(long fmanageid, bool fsleep)
		{
		}

		// Token: 0x06002913 RID: 10515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C5")]
		[Address(RVA = "0x1012C6454", Offset = "0x12C6454", VA = "0x1012C6454")]
		public void CheckMemberLimitForGoOut()
		{
		}

		// Token: 0x06002914 RID: 10516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C6")]
		[Address(RVA = "0x1012C5910", Offset = "0x12C5910", VA = "0x1012C5910")]
		private void CheckMemberLimitForMenberChange()
		{
		}

		// Token: 0x06002915 RID: 10517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C7")]
		[Address(RVA = "0x1012C518C", Offset = "0x12C518C", VA = "0x1012C518C")]
		private void CheckSubMemberDropOut()
		{
		}

		// Token: 0x06002916 RID: 10518 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60025C8")]
		[Address(RVA = "0x1012C4068", Offset = "0x12C4068", VA = "0x1012C4068")]
		public RoomCharaPakage CheckEntryCharaManager(long fmanageid, int froomid)
		{
			return null;
		}

		// Token: 0x06002917 RID: 10519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025C9")]
		[Address(RVA = "0x1012C6864", Offset = "0x12C6864", VA = "0x1012C6864")]
		public void AddRoomCharaPakage(RoomCharaPakage pakage)
		{
		}

		// Token: 0x06002918 RID: 10520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025CA")]
		[Address(RVA = "0x1012C6A94", Offset = "0x12C6A94", VA = "0x1012C6A94")]
		public void CallbackCharaState(eCharaBuildUp ftype, long fmanageid, int froomid)
		{
		}

		// Token: 0x06002919 RID: 10521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60025CB")]
		[Address(RVA = "0x1012C6C20", Offset = "0x12C6C20", VA = "0x1012C6C20")]
		public RoomCharaPakage GetCharaSetUp(int fhandle)
		{
			return null;
		}

		// Token: 0x0600291A RID: 10522 RVA: 0x000115E0 File Offset: 0x0000F7E0
		[Token(Token = "0x60025CC")]
		[Address(RVA = "0x1012C6D40", Offset = "0x12C6D40", VA = "0x1012C6D40", Slot = "14")]
		public override int GetCharaPakageMax()
		{
			return 0;
		}

		// Token: 0x0600291B RID: 10523 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60025CD")]
		[Address(RVA = "0x1012C6D6C", Offset = "0x12C6D6C", VA = "0x1012C6D6C")]
		public RoomCharaPakage GetCharaPakage(int findex)
		{
			return null;
		}

		// Token: 0x0600291C RID: 10524 RVA: 0x000115F8 File Offset: 0x0000F7F8
		[Token(Token = "0x60025CE")]
		[Address(RVA = "0x1012C6DBC", Offset = "0x12C6DBC", VA = "0x1012C6DBC", Slot = "15")]
		public override bool IsCharaExist(long managedID)
		{
			return default(bool);
		}

		// Token: 0x0600291D RID: 10525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025CF")]
		[Address(RVA = "0x1012C6ED0", Offset = "0x12C6ED0", VA = "0x1012C6ED0")]
		public RoomCharaManager()
		{
		}

		// Token: 0x04003958 RID: 14680
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002988")]
		[SerializeField]
		public RoomCharaPakage[] m_CharaTable;

		// Token: 0x04003959 RID: 14681
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002989")]
		public int m_EntryNum;

		// Token: 0x0400395A RID: 14682
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400298A")]
		public int m_EntryMax;

		// Token: 0x0400395B RID: 14683
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400298B")]
		public int m_InSubMemberRothe;

		// Token: 0x0400395C RID: 14684
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x400298C")]
		public int m_OutSubMemberRothe;

		// Token: 0x0400395D RID: 14685
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400298D")]
		private short m_WaitUpChara;

		// Token: 0x0400395E RID: 14686
		[Cpp2IlInjected.FieldOffset(Offset = "0x42")]
		[Token(Token = "0x400298E")]
		private short m_WaitDropChara;

		// Token: 0x0400395F RID: 14687
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400298F")]
		private long m_StepTime;

		// Token: 0x04003960 RID: 14688
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002990")]
		private long[] m_SleepManagerID;

		// Token: 0x04003961 RID: 14689
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002991")]
		private int m_SleepCharaNum;

		// Token: 0x04003962 RID: 14690
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002992")]
		private int m_SleepFloorBedNum;

		// Token: 0x04003963 RID: 14691
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002993")]
		private int m_PlayFloorID;

		// Token: 0x04003964 RID: 14692
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002994")]
		private bool m_BuildUp;

		// Token: 0x04003965 RID: 14693
		[Cpp2IlInjected.FieldOffset(Offset = "0x65")]
		[Token(Token = "0x4002995")]
		private bool m_IsDestroy;
	}
}
