﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009AA RID: 2474
	[Token(Token = "0x2000701")]
	[StructLayout(3)]
	public class RoomCharaModels
	{
		// Token: 0x0600291E RID: 10526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D0")]
		[Address(RVA = "0x1012C6ED8", Offset = "0x12C6ED8", VA = "0x1012C6ED8")]
		public RoomCharaModels(int fcharaid, int froomid, IRoomCharaManager pmanager)
		{
		}

		// Token: 0x0600291F RID: 10527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D1")]
		[Address(RVA = "0x1012C6F24", Offset = "0x12C6F24", VA = "0x1012C6F24")]
		public RoomCharaModels(int fcharaid, int froomid, int arousalLv, IRoomCharaManager pmanager)
		{
		}

		// Token: 0x06002920 RID: 10528 RVA: 0x00011610 File Offset: 0x0000F810
		[Token(Token = "0x60025D2")]
		[Address(RVA = "0x1012C6F7C", Offset = "0x12C6F7C", VA = "0x1012C6F7C")]
		public bool IsRoomIn()
		{
			return default(bool);
		}

		// Token: 0x06002921 RID: 10529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D3")]
		[Address(RVA = "0x1012C6F8C", Offset = "0x12C6F8C", VA = "0x1012C6F8C")]
		public void AttachObjectHandle(CharacterHandler fhandle, RoomObjectCtrlChara proomobj)
		{
		}

		// Token: 0x06002922 RID: 10530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D4")]
		[Address(RVA = "0x1012C703C", Offset = "0x12C703C", VA = "0x1012C703C")]
		public void SetUpRoomCharaObject(Transform parent)
		{
		}

		// Token: 0x06002923 RID: 10531 RVA: 0x00011628 File Offset: 0x0000F828
		[Token(Token = "0x60025D5")]
		[Address(RVA = "0x1012C73AC", Offset = "0x12C73AC", VA = "0x1012C73AC")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06002924 RID: 10532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D6")]
		[Address(RVA = "0x1012C7448", Offset = "0x12C7448", VA = "0x1012C7448")]
		public void UpdatePakage(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002925 RID: 10533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D7")]
		[Address(RVA = "0x1012C7708", Offset = "0x12C7708", VA = "0x1012C7708")]
		public void Clear(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002926 RID: 10534 RVA: 0x00011640 File Offset: 0x0000F840
		[Token(Token = "0x60025D8")]
		[Address(RVA = "0x1012C77C8", Offset = "0x12C77C8", VA = "0x1012C77C8")]
		public bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06002927 RID: 10535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025D9")]
		[Address(RVA = "0x1012C75A4", Offset = "0x12C75A4", VA = "0x1012C75A4")]
		public void BuildLink(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002928 RID: 10536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025DA")]
		[Address(RVA = "0x1012C78C8", Offset = "0x12C78C8", VA = "0x1012C78C8")]
		private void SetFreePosition(RoomGridState pgrid, RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002929 RID: 10537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025DB")]
		[Address(RVA = "0x1012C77D8", Offset = "0x12C77D8", VA = "0x1012C77D8")]
		public void AttachCharaHud(GameObject phudobj)
		{
		}

		// Token: 0x0600292A RID: 10538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025DC")]
		[Address(RVA = "0x1012C7A1C", Offset = "0x12C7A1C", VA = "0x1012C7A1C")]
		public void DestroyProc()
		{
		}

		// Token: 0x0600292B RID: 10539 RVA: 0x00011658 File Offset: 0x0000F858
		[Token(Token = "0x60025DD")]
		[Address(RVA = "0x1012C7B50", Offset = "0x12C7B50", VA = "0x1012C7B50")]
		public bool IsDestroyProc()
		{
			return default(bool);
		}

		// Token: 0x04003966 RID: 14694
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002996")]
		public int m_CharaID;

		// Token: 0x04003967 RID: 14695
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002997")]
		public int m_RoomID;

		// Token: 0x04003968 RID: 14696
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002998")]
		public int m_AccessKey;

		// Token: 0x04003969 RID: 14697
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002999")]
		public int m_ArousalLv;

		// Token: 0x0400396A RID: 14698
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400299A")]
		private int m_FloorID;

		// Token: 0x0400396B RID: 14699
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400299B")]
		public CharacterHandler m_Handle;

		// Token: 0x0400396C RID: 14700
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400299C")]
		public RoomObjectCtrlChara m_RoomObj;

		// Token: 0x0400396D RID: 14701
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400299D")]
		private IRoomCharaManager m_Manager;

		// Token: 0x0400396E RID: 14702
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400299E")]
		private RoomCharaModels.eState m_State;

		// Token: 0x0400396F RID: 14703
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x400299F")]
		private RoomCharaModels.eDestroyState m_DestoryState;

		// Token: 0x020009AB RID: 2475
		[Token(Token = "0x2000F79")]
		private enum eState
		{
			// Token: 0x04003971 RID: 14705
			[Token(Token = "0x4006344")]
			Non,
			// Token: 0x04003972 RID: 14706
			[Token(Token = "0x4006345")]
			SetUp,
			// Token: 0x04003973 RID: 14707
			[Token(Token = "0x4006346")]
			SetUpWait,
			// Token: 0x04003974 RID: 14708
			[Token(Token = "0x4006347")]
			Update,
			// Token: 0x04003975 RID: 14709
			[Token(Token = "0x4006348")]
			End
		}

		// Token: 0x020009AC RID: 2476
		[Token(Token = "0x2000F7A")]
		private enum eDestroyState
		{
			// Token: 0x04003977 RID: 14711
			[Token(Token = "0x400634A")]
			state_None,
			// Token: 0x04003978 RID: 14712
			[Token(Token = "0x400634B")]
			state_WaitDestroy,
			// Token: 0x04003979 RID: 14713
			[Token(Token = "0x400634C")]
			state_FinishedDestroy
		}
	}
}
