﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A07 RID: 2567
	[Token(Token = "0x2000731")]
	[StructLayout(3)]
	public class RoomCharaObjSearch
	{
		// Token: 0x06002B16 RID: 11030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002797")]
		[Address(RVA = "0x1012C7B60", Offset = "0x12C7B60", VA = "0x1012C7B60")]
		public RoomCharaObjSearch(int fblock)
		{
		}

		// Token: 0x06002B17 RID: 11031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002798")]
		[Address(RVA = "0x1012C7BD8", Offset = "0x12C7BD8", VA = "0x1012C7BD8")]
		public void ClearSearchInfo()
		{
		}

		// Token: 0x06002B18 RID: 11032 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002799")]
		[Address(RVA = "0x1012C7C54", Offset = "0x12C7C54", VA = "0x1012C7C54")]
		public IRoomObjectControll CalcObjectEventFromCurrentPos(RoomBuilderBase pbuilder, RoomObjectCtrlChara pbase, int floorid)
		{
			return null;
		}

		// Token: 0x04003B0F RID: 15119
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A83")]
		private RoomCharaObjSearch.SearchObjectHandle[] m_SearchTable;

		// Token: 0x04003B10 RID: 15120
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A84")]
		private int m_SearchIndex;

		// Token: 0x04003B11 RID: 15121
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002A85")]
		private int m_SearchNum;

		// Token: 0x04003B12 RID: 15122
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A86")]
		private int m_SearchMax;

		// Token: 0x02000A08 RID: 2568
		[Token(Token = "0x2000FA4")]
		public struct SearchObjectHandle
		{
			// Token: 0x04003B13 RID: 15123
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063FD")]
			public IRoomObjectControll m_Handle;
		}
	}
}
