﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009AD RID: 2477
	[Token(Token = "0x2000702")]
	[StructLayout(3)]
	public class RoomCharaPakage
	{
		// Token: 0x0600292C RID: 10540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025DE")]
		[Address(RVA = "0x1012C6738", Offset = "0x12C6738", VA = "0x1012C6738")]
		public RoomCharaPakage(long fmngid, int froomid, RoomCharaManager pmanager)
		{
		}

		// Token: 0x0600292D RID: 10541 RVA: 0x00011670 File Offset: 0x0000F870
		[Token(Token = "0x60025DF")]
		[Address(RVA = "0x1012C4AB0", Offset = "0x12C4AB0", VA = "0x1012C4AB0")]
		public bool IsScheduleChara()
		{
			return default(bool);
		}

		// Token: 0x0600292E RID: 10542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025E0")]
		[Address(RVA = "0x1012C5908", Offset = "0x12C5908", VA = "0x1012C5908")]
		public void RemoveScheduleMenber()
		{
		}

		// Token: 0x0600292F RID: 10543 RVA: 0x00011688 File Offset: 0x0000F888
		[Token(Token = "0x60025E1")]
		[Address(RVA = "0x1012C6714", Offset = "0x12C6714", VA = "0x1012C6714")]
		public bool IsRoomIn()
		{
			return default(bool);
		}

		// Token: 0x06002930 RID: 10544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025E2")]
		[Address(RVA = "0x1012C7FF0", Offset = "0x12C7FF0", VA = "0x1012C7FF0")]
		public void AttachObjectHandle(CharacterHandler fhandle, RoomObjectCtrlChara proomobj)
		{
		}

		// Token: 0x06002931 RID: 10545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025E3")]
		[Address(RVA = "0x1012C80AC", Offset = "0x12C80AC", VA = "0x1012C80AC")]
		public void SetUpRoomCharaObject(CharacterParam fsetup, int fresid, Transform parent)
		{
		}

		// Token: 0x06002932 RID: 10546 RVA: 0x000116A0 File Offset: 0x0000F8A0
		[Token(Token = "0x60025E4")]
		[Address(RVA = "0x1012C3B20", Offset = "0x12C3B20", VA = "0x1012C3B20")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06002933 RID: 10547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025E5")]
		[Address(RVA = "0x1012C4AB8", Offset = "0x12C4AB8", VA = "0x1012C4AB8")]
		public void UpdatePakage(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x06002934 RID: 10548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025E6")]
		[Address(RVA = "0x1012C8684", Offset = "0x12C8684", VA = "0x1012C8684")]
		private void CheckStateChange(RoomCharaPakage.eState fstate, RoomCharaPakage.StackActCommand pque)
		{
		}

		// Token: 0x06002935 RID: 10549 RVA: 0x000116B8 File Offset: 0x0000F8B8
		[Token(Token = "0x60025E7")]
		[Address(RVA = "0x1012C3F64", Offset = "0x12C3F64", VA = "0x1012C3F64")]
		public bool IsScheduleOfRoom()
		{
			return default(bool);
		}

		// Token: 0x06002936 RID: 10550 RVA: 0x000116D0 File Offset: 0x0000F8D0
		[Token(Token = "0x60025E8")]
		[Address(RVA = "0x1012C88D4", Offset = "0x12C88D4", VA = "0x1012C88D4")]
		public bool IsScheduleOfSleep()
		{
			return default(bool);
		}

		// Token: 0x06002937 RID: 10551 RVA: 0x000116E8 File Offset: 0x0000F8E8
		[Token(Token = "0x60025E9")]
		[Address(RVA = "0x1012C3F34", Offset = "0x12C3F34", VA = "0x1012C3F34")]
		public bool IsScheduleOfRoomNotSleep()
		{
			return default(bool);
		}

		// Token: 0x06002938 RID: 10552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025EA")]
		[Address(RVA = "0x1012C5390", Offset = "0x12C5390", VA = "0x1012C5390")]
		public void Delete()
		{
		}

		// Token: 0x06002939 RID: 10553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025EB")]
		[Address(RVA = "0x1012C43B0", Offset = "0x12C43B0", VA = "0x1012C43B0")]
		public void Clear()
		{
		}

		// Token: 0x0600293A RID: 10554 RVA: 0x00011700 File Offset: 0x0000F900
		[Token(Token = "0x60025EC")]
		[Address(RVA = "0x1012C4AA0", Offset = "0x12C4AA0", VA = "0x1012C4AA0")]
		public bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x0600293B RID: 10555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025ED")]
		[Address(RVA = "0x1012C844C", Offset = "0x12C844C", VA = "0x1012C844C")]
		public void BuildLink(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x0600293C RID: 10556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025EE")]
		[Address(RVA = "0x1012C89F4", Offset = "0x12C89F4", VA = "0x1012C89F4")]
		private void SetFreePosition(RoomGridState pgrid, RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x0600293D RID: 10557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025EF")]
		[Address(RVA = "0x1012C8904", Offset = "0x12C8904", VA = "0x1012C8904")]
		public void AttachCharaHud(GameObject phudobj)
		{
		}

		// Token: 0x0600293E RID: 10558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F0")]
		[Address(RVA = "0x1012C8B48", Offset = "0x12C8B48", VA = "0x1012C8B48")]
		public void FeedBackCharaCallBack(eCallBackType ftype, FieldObjHandleChara punit)
		{
		}

		// Token: 0x0600293F RID: 10559 RVA: 0x00011718 File Offset: 0x0000F918
		[Token(Token = "0x60025F1")]
		[Address(RVA = "0x1012C90D4", Offset = "0x12C90D4", VA = "0x1012C90D4")]
		public bool SetSleep()
		{
			return default(bool);
		}

		// Token: 0x06002940 RID: 10560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F2")]
		[Address(RVA = "0x1012C9168", Offset = "0x12C9168", VA = "0x1012C9168")]
		public void SetWakeUp()
		{
		}

		// Token: 0x06002941 RID: 10561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F3")]
		[Address(RVA = "0x1012C4550", Offset = "0x12C4550", VA = "0x1012C4550")]
		public void SetUpInRoom(Transform parent, bool faction)
		{
		}

		// Token: 0x06002942 RID: 10562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F4")]
		[Address(RVA = "0x1012C6724", Offset = "0x12C6724", VA = "0x1012C6724")]
		public void DropOutRoom()
		{
		}

		// Token: 0x06002943 RID: 10563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F5")]
		[Address(RVA = "0x1012C8EE4", Offset = "0x12C8EE4", VA = "0x1012C8EE4")]
		private void StackActCmd(RoomCharaPakage.eState fstate, RoomActionCommand.eActionCode fcmd, int fkey, bool flock = false)
		{
		}

		// Token: 0x06002944 RID: 10564 RVA: 0x00011730 File Offset: 0x0000F930
		[Token(Token = "0x60025F6")]
		[Address(RVA = "0x1012C83F8", Offset = "0x12C83F8", VA = "0x1012C83F8")]
		private bool IsStackActCmd()
		{
			return default(bool);
		}

		// Token: 0x06002945 RID: 10565 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60025F7")]
		[Address(RVA = "0x1012C8408", Offset = "0x12C8408", VA = "0x1012C8408")]
		private RoomCharaPakage.StackActCommand GetStackQue()
		{
			return null;
		}

		// Token: 0x06002946 RID: 10566 RVA: 0x00011748 File Offset: 0x0000F948
		[Token(Token = "0x60025F8")]
		[Address(RVA = "0x1012C6654", Offset = "0x12C6654", VA = "0x1012C6654")]
		public bool IsStackSetUpQue()
		{
			return default(bool);
		}

		// Token: 0x06002947 RID: 10567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025F9")]
		[Address(RVA = "0x1012C8780", Offset = "0x12C8780", VA = "0x1012C8780")]
		private void SetNextQue()
		{
		}

		// Token: 0x06002948 RID: 10568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025FA")]
		[Address(RVA = "0x1012C496C", Offset = "0x12C496C", VA = "0x1012C496C")]
		public void DestroyProc()
		{
		}

		// Token: 0x06002949 RID: 10569 RVA: 0x00011760 File Offset: 0x0000F960
		[Token(Token = "0x60025FB")]
		[Address(RVA = "0x1012C495C", Offset = "0x12C495C", VA = "0x1012C495C")]
		public bool IsDestroyProc()
		{
			return default(bool);
		}

		// Token: 0x0400397A RID: 14714
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40029A0")]
		public long m_ManageID;

		// Token: 0x0400397B RID: 14715
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40029A1")]
		public int m_RoomID;

		// Token: 0x0400397C RID: 14716
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40029A2")]
		public int m_AccessKey;

		// Token: 0x0400397D RID: 14717
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029A3")]
		public eCharaNamedType m_Named;

		// Token: 0x0400397E RID: 14718
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40029A4")]
		private bool m_IsSchedule;

		// Token: 0x0400397F RID: 14719
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029A5")]
		private int m_FloorID;

		// Token: 0x04003980 RID: 14720
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029A6")]
		public CharacterHandler m_Handle;

		// Token: 0x04003981 RID: 14721
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40029A7")]
		public RoomObjectCtrlChara m_RoomObj;

		// Token: 0x04003982 RID: 14722
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40029A8")]
		private RoomCharaManager m_Manager;

		// Token: 0x04003983 RID: 14723
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029A9")]
		private RoomCharaPakage.StackActCommand[] m_StackQue;

		// Token: 0x04003984 RID: 14724
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029AA")]
		private short m_StackQueNum;

		// Token: 0x04003985 RID: 14725
		[Cpp2IlInjected.FieldOffset(Offset = "0x52")]
		[Token(Token = "0x40029AB")]
		private short STACK_QUE_MAX;

		// Token: 0x04003986 RID: 14726
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40029AC")]
		private RoomCharaPakage.eState m_State;

		// Token: 0x04003987 RID: 14727
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40029AD")]
		private RoomCharaPakage.eState m_SleepKick;

		// Token: 0x04003988 RID: 14728
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40029AE")]
		private RoomCharaPakage.eDestroyState m_DestoryState;

		// Token: 0x020009AE RID: 2478
		[Token(Token = "0x2000F7B")]
		private enum eState
		{
			// Token: 0x0400398A RID: 14730
			[Token(Token = "0x400634E")]
			Non,
			// Token: 0x0400398B RID: 14731
			[Token(Token = "0x400634F")]
			SetUp,
			// Token: 0x0400398C RID: 14732
			[Token(Token = "0x4006350")]
			SetUpWait,
			// Token: 0x0400398D RID: 14733
			[Token(Token = "0x4006351")]
			Update,
			// Token: 0x0400398E RID: 14734
			[Token(Token = "0x4006352")]
			Sleep,
			// Token: 0x0400398F RID: 14735
			[Token(Token = "0x4006353")]
			WakeUp,
			// Token: 0x04003990 RID: 14736
			[Token(Token = "0x4006354")]
			End
		}

		// Token: 0x020009AF RID: 2479
		[Token(Token = "0x2000F7C")]
		private class StackActCommand
		{
			// Token: 0x0600294A RID: 10570 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600600C")]
			[Address(RVA = "0x1012C9208", Offset = "0x12C9208", VA = "0x1012C9208")]
			public StackActCommand()
			{
			}

			// Token: 0x04003991 RID: 14737
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006355")]
			public RoomCharaPakage.eState m_StackState;

			// Token: 0x04003992 RID: 14738
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006356")]
			public RoomActionCommand m_SetUpCmd;
		}

		// Token: 0x020009B0 RID: 2480
		[Token(Token = "0x2000F7D")]
		private enum eDestroyState
		{
			// Token: 0x04003994 RID: 14740
			[Token(Token = "0x4006358")]
			state_None,
			// Token: 0x04003995 RID: 14741
			[Token(Token = "0x4006359")]
			state_WaitDestroy,
			// Token: 0x04003996 RID: 14742
			[Token(Token = "0x400635A")]
			state_FinishedDestroy
		}
	}
}
