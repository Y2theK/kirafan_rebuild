﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009BB RID: 2491
	[Token(Token = "0x2000709")]
	[StructLayout(3)]
	public class RoomComAPIFriendFieldPartyGetAll : INetComHandle
	{
		// Token: 0x0600299E RID: 10654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600264E")]
		[Address(RVA = "0x1012C9210", Offset = "0x12C9210", VA = "0x1012C9210")]
		public RoomComAPIFriendFieldPartyGetAll()
		{
		}

		// Token: 0x040039D0 RID: 14800
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029D7")]
		public long playerId;
	}
}
