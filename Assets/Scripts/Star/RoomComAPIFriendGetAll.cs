﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009BA RID: 2490
	[Token(Token = "0x2000708")]
	[StructLayout(3)]
	public class RoomComAPIFriendGetAll : INetComHandle
	{
		// Token: 0x0600299D RID: 10653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600264D")]
		[Address(RVA = "0x1012C92AC", Offset = "0x12C92AC", VA = "0x1012C92AC")]
		public RoomComAPIFriendGetAll()
		{
		}

		// Token: 0x040039CE RID: 14798
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029D5")]
		public long playerId;

		// Token: 0x040039CF RID: 14799
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029D6")]
		public int floorId;
	}
}
