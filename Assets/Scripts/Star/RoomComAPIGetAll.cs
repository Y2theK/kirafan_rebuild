﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009B9 RID: 2489
	[Token(Token = "0x2000707")]
	[StructLayout(3)]
	public class RoomComAPIGetAll : INetComHandle
	{
		// Token: 0x0600299C RID: 10652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600264C")]
		[Address(RVA = "0x1012C93EC", Offset = "0x12C93EC", VA = "0x1012C93EC")]
		public RoomComAPIGetAll()
		{
		}

		// Token: 0x040039CD RID: 14797
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029D4")]
		public long playerId;
	}
}
