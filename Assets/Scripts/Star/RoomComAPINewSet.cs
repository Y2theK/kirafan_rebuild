﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020009BD RID: 2493
	[Token(Token = "0x200070B")]
	[StructLayout(3)]
	public class RoomComAPINewSet : INetComHandle
	{
		// Token: 0x060029A0 RID: 10656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002650")]
		[Address(RVA = "0x1012C9488", Offset = "0x12C9488", VA = "0x1012C9488")]
		public RoomComAPINewSet()
		{
		}

		// Token: 0x040039D5 RID: 14805
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029DC")]
		public int floorId;

		// Token: 0x040039D6 RID: 14806
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40029DD")]
		public int groupId;

		// Token: 0x040039D7 RID: 14807
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029DE")]
		public PlayerRoomArrangement[] arrangeData;
	}
}
