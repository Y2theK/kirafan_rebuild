﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009C2 RID: 2498
	[Token(Token = "0x2000710")]
	[StructLayout(3)]
	public class RoomComAPIObjAdd : INetComHandle
	{
		// Token: 0x060029A5 RID: 10661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002655")]
		[Address(RVA = "0x1012C9528", Offset = "0x12C9528", VA = "0x1012C9528")]
		public RoomComAPIObjAdd()
		{
		}

		// Token: 0x040039DA RID: 14810
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029E1")]
		public string roomObjectId;

		// Token: 0x040039DB RID: 14811
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029E2")]
		public string amount;
	}
}
