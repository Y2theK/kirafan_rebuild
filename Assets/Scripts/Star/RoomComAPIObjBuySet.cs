﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020009C4 RID: 2500
	[Token(Token = "0x2000712")]
	[StructLayout(3)]
	public class RoomComAPIObjBuySet : INetComHandle
	{
		// Token: 0x060029A7 RID: 10663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002657")]
		[Address(RVA = "0x1012C95C8", Offset = "0x12C95C8", VA = "0x1012C95C8")]
		public RoomComAPIObjBuySet()
		{
		}

		// Token: 0x040039DD RID: 14813
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029E4")]
		public string roomObjectId;

		// Token: 0x040039DE RID: 14814
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029E5")]
		public string amount;

		// Token: 0x040039DF RID: 14815
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40029E6")]
		public long managedRoomId;

		// Token: 0x040039E0 RID: 14816
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40029E7")]
		public int floorId;

		// Token: 0x040039E1 RID: 14817
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40029E8")]
		public int groupId;

		// Token: 0x040039E2 RID: 14818
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40029E9")]
		public PlayerRoomArrangement[] arrangeData;
	}
}
