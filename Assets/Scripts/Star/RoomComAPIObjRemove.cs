﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009C3 RID: 2499
	[Token(Token = "0x2000711")]
	[StructLayout(3)]
	public class RoomComAPIObjRemove : INetComHandle
	{
		// Token: 0x060029A6 RID: 10662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002656")]
		[Address(RVA = "0x1012C9704", Offset = "0x12C9704", VA = "0x1012C9704")]
		public RoomComAPIObjRemove()
		{
		}

		// Token: 0x040039DC RID: 14812
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029E3")]
		public long managedRoomObjectId;
	}
}
