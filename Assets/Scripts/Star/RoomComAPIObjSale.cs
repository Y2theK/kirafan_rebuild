﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009C5 RID: 2501
	[Token(Token = "0x2000713")]
	[StructLayout(3)]
	public class RoomComAPIObjSale : INetComHandle
	{
		// Token: 0x060029A8 RID: 10664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002658")]
		[Address(RVA = "0x1012C97A4", Offset = "0x12C97A4", VA = "0x1012C97A4")]
		public RoomComAPIObjSale()
		{
		}

		// Token: 0x040039E3 RID: 14819
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029EA")]
		public string managedRoomObjectId;
	}
}
