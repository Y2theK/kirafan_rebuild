﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009BE RID: 2494
	[Token(Token = "0x200070C")]
	[StructLayout(3)]
	public class RoomComAPIRemove : INetComHandle
	{
		// Token: 0x060029A1 RID: 10657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002651")]
		[Address(RVA = "0x1012C9844", Offset = "0x12C9844", VA = "0x1012C9844")]
		public RoomComAPIRemove()
		{
		}

		// Token: 0x040039D8 RID: 14808
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029DF")]
		public long managedRoomId;
	}
}
