﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x020009BC RID: 2492
	[Token(Token = "0x200070A")]
	[StructLayout(3)]
	public class RoomComAPISet : INetComHandle
	{
		// Token: 0x0600299F RID: 10655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600264F")]
		[Address(RVA = "0x1012C98E4", Offset = "0x12C98E4", VA = "0x1012C98E4")]
		public RoomComAPISet()
		{
		}

		// Token: 0x040039D1 RID: 14801
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029D8")]
		public long managedRoomId;

		// Token: 0x040039D2 RID: 14802
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40029D9")]
		public int floorId;

		// Token: 0x040039D3 RID: 14803
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40029DA")]
		public int groupId;

		// Token: 0x040039D4 RID: 14804
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40029DB")]
		public PlayerRoomArrangement[] arrangeData;
	}
}
