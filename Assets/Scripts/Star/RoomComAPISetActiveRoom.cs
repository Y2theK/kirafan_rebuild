﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009BF RID: 2495
	[Token(Token = "0x200070D")]
	[StructLayout(3)]
	public class RoomComAPISetActiveRoom : INetComHandle
	{
		// Token: 0x060029A2 RID: 10658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002652")]
		[Address(RVA = "0x1012C9984", Offset = "0x12C9984", VA = "0x1012C9984")]
		public RoomComAPISetActiveRoom()
		{
		}

		// Token: 0x040039D9 RID: 14809
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029E0")]
		public long managedRoomId;
	}
}
