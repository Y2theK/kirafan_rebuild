﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;

namespace Star
{
	// Token: 0x02000811 RID: 2065
	[Token(Token = "0x2000614")]
	[StructLayout(3)]
	public class RoomComActionPopup : IMainComCommand
	{
		// Token: 0x0600208D RID: 8333 RVA: 0x0000E4C0 File Offset: 0x0000C6C0
		[Token(Token = "0x6001E05")]
		[Address(RVA = "0x1012C9A24", Offset = "0x12C9A24", VA = "0x1012C9A24", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x0600208E RID: 8334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E06")]
		[Address(RVA = "0x1012C9A2C", Offset = "0x12C9A2C", VA = "0x1012C9A2C")]
		public RoomComActionPopup()
		{
		}

		// Token: 0x0400309C RID: 12444
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40024F0")]
		public long m_CharaManageID;

		// Token: 0x0400309D RID: 12445
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40024F1")]
		public RoomGreet.eCategory m_CategoryID;
	}
}
