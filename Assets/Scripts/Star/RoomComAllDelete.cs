﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009C8 RID: 2504
	[Token(Token = "0x2000715")]
	[StructLayout(3)]
	public class RoomComAllDelete : IFldNetComModule
	{
		// Token: 0x060029B1 RID: 10673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002661")]
		[Address(RVA = "0x1012C9A34", Offset = "0x12C9A34", VA = "0x1012C9A34")]
		public RoomComAllDelete()
		{
		}

		// Token: 0x060029B2 RID: 10674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002662")]
		[Address(RVA = "0x1012C9A90", Offset = "0x12C9A90", VA = "0x1012C9A90")]
		public void PlaySend()
		{
		}

		// Token: 0x060029B3 RID: 10675 RVA: 0x000119B8 File Offset: 0x0000FBB8
		[Token(Token = "0x6002663")]
		[Address(RVA = "0x1012C9AC8", Offset = "0x12C9AC8", VA = "0x1012C9AC8")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029B4 RID: 10676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002664")]
		[Address(RVA = "0x1012C9BEC", Offset = "0x12C9BEC", VA = "0x1012C9BEC")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029B5 RID: 10677 RVA: 0x000119D0 File Offset: 0x0000FBD0
		[Token(Token = "0x6002665")]
		[Address(RVA = "0x1012C9D6C", Offset = "0x12C9D6C", VA = "0x1012C9D6C")]
		public bool SetUpObjList()
		{
			return default(bool);
		}

		// Token: 0x060029B6 RID: 10678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002666")]
		[Address(RVA = "0x1012C9E24", Offset = "0x12C9E24", VA = "0x1012C9E24")]
		private void CallbackObjSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029B7 RID: 10679 RVA: 0x000119E8 File Offset: 0x0000FBE8
		[Token(Token = "0x6002667")]
		[Address(RVA = "0x1012C9FA4", Offset = "0x12C9FA4", VA = "0x1012C9FA4", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060029B8 RID: 10680 RVA: 0x00011A00 File Offset: 0x0000FC00
		[Token(Token = "0x6002668")]
		[Address(RVA = "0x1012CA044", Offset = "0x12CA044", VA = "0x1012CA044")]
		public bool DefaultDeleteManage()
		{
			return default(bool);
		}

		// Token: 0x060029B9 RID: 10681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002669")]
		[Address(RVA = "0x1012CA2B4", Offset = "0x12CA2B4", VA = "0x1012CA2B4")]
		private void CallbackDeleteUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029BA RID: 10682 RVA: 0x00011A18 File Offset: 0x0000FC18
		[Token(Token = "0x600266A")]
		[Address(RVA = "0x1012CA17C", Offset = "0x12CA17C", VA = "0x1012CA17C")]
		public bool DefaultDeleteObjManage()
		{
			return default(bool);
		}

		// Token: 0x060029BB RID: 10683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600266B")]
		[Address(RVA = "0x1012CA2C0", Offset = "0x12CA2C0", VA = "0x1012CA2C0")]
		private void CallbackDeleteObjUp(INetComHandle phandle)
		{
		}

		// Token: 0x040039F4 RID: 14836
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029EF")]
		private RoomComAllDelete.eStep m_Step;

		// Token: 0x040039F5 RID: 14837
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029F0")]
		private long[] m_Table;

		// Token: 0x040039F6 RID: 14838
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029F1")]
		private int m_DelStep;

		// Token: 0x020009C9 RID: 2505
		[Token(Token = "0x2000F83")]
		public enum eStep
		{
			// Token: 0x040039F8 RID: 14840
			[Token(Token = "0x4006379")]
			GetState,
			// Token: 0x040039F9 RID: 14841
			[Token(Token = "0x400637A")]
			WaitCheck,
			// Token: 0x040039FA RID: 14842
			[Token(Token = "0x400637B")]
			DeleteUp,
			// Token: 0x040039FB RID: 14843
			[Token(Token = "0x400637C")]
			SetUpCheck,
			// Token: 0x040039FC RID: 14844
			[Token(Token = "0x400637D")]
			GetObjState,
			// Token: 0x040039FD RID: 14845
			[Token(Token = "0x400637E")]
			DeleteObjUp,
			// Token: 0x040039FE RID: 14846
			[Token(Token = "0x400637F")]
			End
		}
	}
}
