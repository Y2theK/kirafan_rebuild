﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A41 RID: 2625
	[Token(Token = "0x200074E")]
	[StructLayout(3)]
	public class RoomComBGChange : IRoomEventCommand
	{
		// Token: 0x06002D4E RID: 11598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029AB")]
		[Address(RVA = "0x1012CA2CC", Offset = "0x12CA2CC", VA = "0x1012CA2CC")]
		public RoomComBGChange(int fchangeobj, bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x06002D4F RID: 11599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029AC")]
		[Address(RVA = "0x1012C0C74", Offset = "0x12C0C74", VA = "0x1012C0C74")]
		public RoomComBGChange(long fmanageid, int fresourceid, [Optional] Action callback)
		{
		}

		// Token: 0x06002D50 RID: 11600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029AD")]
		[Address(RVA = "0x1012CA328", Offset = "0x12CA328", VA = "0x1012CA328")]
		private void OnResponse(long[] mngObjIDs)
		{
		}

		// Token: 0x06002D51 RID: 11601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029AE")]
		[Address(RVA = "0x1012CA4C0", Offset = "0x12CA4C0", VA = "0x1012CA4C0")]
		private void OnResponse(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06002D52 RID: 11602 RVA: 0x000135D8 File Offset: 0x000117D8
		[Token(Token = "0x60029AF")]
		[Address(RVA = "0x1012CA4D0", Offset = "0x12CA4D0", VA = "0x1012CA4D0", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x04003CAF RID: 15535
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BAC")]
		public int m_ObjID;

		// Token: 0x04003CB0 RID: 15536
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BAD")]
		public RoomObjectMdlSprite m_NewModel;

		// Token: 0x04003CB1 RID: 15537
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002BAE")]
		public int m_Step;

		// Token: 0x04003CB2 RID: 15538
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002BAF")]
		public int m_ChangeResID;

		// Token: 0x04003CB3 RID: 15539
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002BB0")]
		public long m_ChangeMngID;

		// Token: 0x04003CB4 RID: 15540
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002BB1")]
		public float m_Time;

		// Token: 0x04003CB5 RID: 15541
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002BB2")]
		public bool m_BuyObj;

		// Token: 0x04003CB6 RID: 15542
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002BB3")]
		public Action m_Callback;

		// Token: 0x04003CB7 RID: 15543
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002BB4")]
		private UserRoomData targetRoomData;

		// Token: 0x04003CB8 RID: 15544
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002BB5")]
		private UserRoomObjectData targetObjectData;

		// Token: 0x04003CB9 RID: 15545
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002BB6")]
		private UserRoomData.PlacementData targetPlacementData;

		// Token: 0x04003CBA RID: 15546
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002BB7")]
		private long backupPlacementMngId;

		// Token: 0x04003CBB RID: 15547
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002BB8")]
		private int backupPlacementResId;

		// Token: 0x04003CBC RID: 15548
		[Token(Token = "0x4002BB9")]
		private const eRoomObjectCategory constCategory = eRoomObjectCategory.Background;

		// Token: 0x04003CBD RID: 15549
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002BBA")]
		private RoomBuilder builder;
	}
}
