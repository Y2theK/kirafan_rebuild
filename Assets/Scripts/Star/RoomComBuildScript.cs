﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009CA RID: 2506
	[Token(Token = "0x2000716")]
	[StructLayout(3)]
	public class RoomComBuildScript : IFldNetComModule
	{
		// Token: 0x060029BC RID: 10684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600266C")]
		[Address(RVA = "0x1012BE588", Offset = "0x12BE588", VA = "0x1012BE588")]
		public void SetManageID(long fmanageid)
		{
		}

		// Token: 0x060029BD RID: 10685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600266D")]
		[Address(RVA = "0x1012BDE98", Offset = "0x12BDE98", VA = "0x1012BDE98")]
		public RoomComBuildScript(long froommngid)
		{
		}

		// Token: 0x060029BE RID: 10686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600266E")]
		[Address(RVA = "0x1012BDF44", Offset = "0x12BDF44", VA = "0x1012BDF44")]
		public void StackSendCmd(RoomComBuildScript.eCmd fcalc, long fkeycode, int foption, [Optional] IFldNetComModule.CallBack pcallback)
		{
		}

		// Token: 0x060029BF RID: 10687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600266F")]
		[Address(RVA = "0x1012BE034", Offset = "0x12BE034", VA = "0x1012BE034")]
		public void PlaySend()
		{
		}

		// Token: 0x060029C0 RID: 10688 RVA: 0x00011A30 File Offset: 0x0000FC30
		[Token(Token = "0x6002670")]
		[Address(RVA = "0x1012CA9D4", Offset = "0x12CA9D4", VA = "0x1012CA9D4", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060029C1 RID: 10689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002671")]
		[Address(RVA = "0x1012CAEC0", Offset = "0x12CAEC0", VA = "0x1012CAEC0")]
		private void SendBuySetObjUp()
		{
		}

		// Token: 0x060029C2 RID: 10690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002672")]
		[Address(RVA = "0x1012CB004", Offset = "0x12CB004", VA = "0x1012CB004")]
		private void CallbackObjectBuySet(INetComHandle handle)
		{
		}

		// Token: 0x060029C3 RID: 10691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002673")]
		[Address(RVA = "0x1012CAC00", Offset = "0x12CAC00", VA = "0x1012CAC00")]
		private void SendAddPlacementObject()
		{
		}

		// Token: 0x060029C4 RID: 10692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002674")]
		[Address(RVA = "0x1012CB2F4", Offset = "0x12CB2F4", VA = "0x1012CB2F4")]
		private void CallbackPlacementSet(INetComHandle phandle)
		{
		}

		// Token: 0x060029C5 RID: 10693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002675")]
		[Address(RVA = "0x1012CACF8", Offset = "0x12CACF8", VA = "0x1012CACF8")]
		private void SendStateChange()
		{
		}

		// Token: 0x060029C6 RID: 10694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002676")]
		[Address(RVA = "0x1012CADF0", Offset = "0x12CADF0", VA = "0x1012CADF0")]
		private void SendSaleObject()
		{
		}

		// Token: 0x060029C7 RID: 10695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002677")]
		[Address(RVA = "0x1012CB300", Offset = "0x12CB300", VA = "0x1012CB300")]
		private void CallbackObjectSale(INetComHandle phandle)
		{
		}

		// Token: 0x040039FF RID: 14847
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029F2")]
		public int m_ObjID;

		// Token: 0x04003A00 RID: 14848
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40029F3")]
		public int m_ObjNum;

		// Token: 0x04003A01 RID: 14849
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029F4")]
		public long m_ManageID;

		// Token: 0x04003A02 RID: 14850
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029F5")]
		public long m_RoomMngID;

		// Token: 0x04003A03 RID: 14851
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40029F6")]
		public bool m_CreateObj;

		// Token: 0x04003A04 RID: 14852
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40029F7")]
		private RoomComBuildScript.eStep m_Step;

		// Token: 0x04003A05 RID: 14853
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40029F8")]
		public List<RoomComBuildScript.StackScriptCommand> m_Table;

		// Token: 0x020009CB RID: 2507
		[Token(Token = "0x2000F84")]
		public enum eCmd
		{
			// Token: 0x04003A07 RID: 14855
			[Token(Token = "0x4006381")]
			BuyObj,
			// Token: 0x04003A08 RID: 14856
			[Token(Token = "0x4006382")]
			BuySetObj,
			// Token: 0x04003A09 RID: 14857
			[Token(Token = "0x4006383")]
			AddBuildIn,
			// Token: 0x04003A0A RID: 14858
			[Token(Token = "0x4006384")]
			ChangePlacement,
			// Token: 0x04003A0B RID: 14859
			[Token(Token = "0x4006385")]
			DelPlacement,
			// Token: 0x04003A0C RID: 14860
			[Token(Token = "0x4006386")]
			SaleObject,
			// Token: 0x04003A0D RID: 14861
			[Token(Token = "0x4006387")]
			CallEvt
		}

		// Token: 0x020009CC RID: 2508
		[Token(Token = "0x2000F85")]
		public enum eStep
		{
			// Token: 0x04003A0F RID: 14863
			[Token(Token = "0x4006389")]
			Check,
			// Token: 0x04003A10 RID: 14864
			[Token(Token = "0x400638A")]
			ComEnd,
			// Token: 0x04003A11 RID: 14865
			[Token(Token = "0x400638B")]
			ComCheck,
			// Token: 0x04003A12 RID: 14866
			[Token(Token = "0x400638C")]
			End
		}

		// Token: 0x020009CD RID: 2509
		[Token(Token = "0x2000F86")]
		public class StackScriptCommand
		{
			// Token: 0x060029C8 RID: 10696 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600600E")]
			[Address(RVA = "0x1012CA9CC", Offset = "0x12CA9CC", VA = "0x1012CA9CC")]
			public StackScriptCommand()
			{
			}

			// Token: 0x04003A13 RID: 14867
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400638D")]
			public RoomComBuildScript.eCmd m_NextStep;

			// Token: 0x04003A14 RID: 14868
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400638E")]
			public long m_KeyCode;

			// Token: 0x04003A15 RID: 14869
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400638F")]
			public int m_OptionCode;

			// Token: 0x04003A16 RID: 14870
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006390")]
			public IFldNetComModule.CallBack m_Callback;
		}
	}
}
