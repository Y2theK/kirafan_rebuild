﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A42 RID: 2626
	[Token(Token = "0x200074F")]
	[StructLayout(3)]
	public class RoomComCharaRelease : IRoomEventCommand
	{
		// Token: 0x06002D53 RID: 11603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B0")]
		[Address(RVA = "0x1012C0854", Offset = "0x12C0854", VA = "0x1012C0854")]
		public RoomComCharaRelease(IRoomObjectControll pobj)
		{
		}

		// Token: 0x06002D54 RID: 11604 RVA: 0x000135F0 File Offset: 0x000117F0
		[Token(Token = "0x60029B1")]
		[Address(RVA = "0x1012CB430", Offset = "0x12CB430", VA = "0x1012CB430", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			return default(bool);
		}

		// Token: 0x04003CBE RID: 15550
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BBB")]
		public IRoomObjectControll m_Target;

		// Token: 0x04003CBF RID: 15551
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BBC")]
		public int m_Step;
	}
}
