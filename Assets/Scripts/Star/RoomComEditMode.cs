﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000812 RID: 2066
	[Token(Token = "0x2000615")]
	[StructLayout(3)]
	public class RoomComEditMode : IMainComCommand
	{
		// Token: 0x0600208F RID: 8335 RVA: 0x0000E4D8 File Offset: 0x0000C6D8
		[Token(Token = "0x6001E07")]
		[Address(RVA = "0x1012CB49C", Offset = "0x12CB49C", VA = "0x1012CB49C", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06002090 RID: 8336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E08")]
		[Address(RVA = "0x1012C0BA8", Offset = "0x12C0BA8", VA = "0x1012C0BA8")]
		public RoomComEditMode()
		{
		}
	}
}
