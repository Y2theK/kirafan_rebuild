﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A43 RID: 2627
	[Token(Token = "0x2000750")]
	[StructLayout(3)]
	public class RoomComFloorChange : IRoomEventCommand
	{
		// Token: 0x06002D55 RID: 11605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B2")]
		[Address(RVA = "0x1012CB4A4", Offset = "0x12CB4A4", VA = "0x1012CB4A4")]
		public RoomComFloorChange(int fmodelno, bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x06002D56 RID: 11606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B3")]
		[Address(RVA = "0x1012C0CD0", Offset = "0x12C0CD0", VA = "0x1012C0CD0")]
		public RoomComFloorChange(long fmanageid, int fresourceid, [Optional] Action callback)
		{
		}

		// Token: 0x06002D57 RID: 11607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B4")]
		[Address(RVA = "0x1012CB504", Offset = "0x12CB504", VA = "0x1012CB504")]
		private void OnResponse(long[] mngObjIDs)
		{
		}

		// Token: 0x06002D58 RID: 11608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B5")]
		[Address(RVA = "0x1012CB69C", Offset = "0x12CB69C", VA = "0x1012CB69C")]
		private void OnResponse(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06002D59 RID: 11609 RVA: 0x00013608 File Offset: 0x00011808
		[Token(Token = "0x60029B6")]
		[Address(RVA = "0x1012CB6AC", Offset = "0x12CB6AC", VA = "0x1012CB6AC", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x04003CC0 RID: 15552
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BBD")]
		public int m_ObjID;

		// Token: 0x04003CC1 RID: 15553
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BBE")]
		public RoomObjectMdlSprite m_NewModelMain;

		// Token: 0x04003CC2 RID: 15554
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002BBF")]
		public RoomObjectMdlSprite m_NewModelSub;

		// Token: 0x04003CC3 RID: 15555
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002BC0")]
		public int m_Step;

		// Token: 0x04003CC4 RID: 15556
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002BC1")]
		public int m_Flag;

		// Token: 0x04003CC5 RID: 15557
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002BC2")]
		public float m_Time;

		// Token: 0x04003CC6 RID: 15558
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002BC3")]
		public int m_ChangeResID;

		// Token: 0x04003CC7 RID: 15559
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002BC4")]
		public long m_ChangeMngID;

		// Token: 0x04003CC8 RID: 15560
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002BC5")]
		public bool m_BuyObj;

		// Token: 0x04003CC9 RID: 15561
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002BC6")]
		public Action m_Callback;

		// Token: 0x04003CCA RID: 15562
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002BC7")]
		private UserRoomData targetRoomData;

		// Token: 0x04003CCB RID: 15563
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002BC8")]
		private UserRoomObjectData targetObjectData;

		// Token: 0x04003CCC RID: 15564
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002BC9")]
		private UserRoomData.PlacementData targetPlacementData;

		// Token: 0x04003CCD RID: 15565
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002BCA")]
		private long backupPlacementMngId;

		// Token: 0x04003CCE RID: 15566
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002BCB")]
		private int backupPlacementResId;

		// Token: 0x04003CCF RID: 15567
		[Token(Token = "0x4002BCC")]
		private const eRoomObjectCategory constCategory = eRoomObjectCategory.Floor;

		// Token: 0x04003CD0 RID: 15568
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002BCD")]
		private RoomBuilder builder;
	}
}
