﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009CE RID: 2510
	[Token(Token = "0x2000717")]
	[StructLayout(3)]
	public class RoomComListSetUp : IFldNetComModule
	{
		// Token: 0x060029C9 RID: 10697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002678")]
		[Address(RVA = "0x1012CC0CC", Offset = "0x12CC0CC", VA = "0x1012CC0CC")]
		public RoomComListSetUp()
		{
		}

		// Token: 0x060029CA RID: 10698 RVA: 0x00011A48 File Offset: 0x0000FC48
		[Token(Token = "0x6002679")]
		[Address(RVA = "0x1012CC128", Offset = "0x12CC128", VA = "0x1012CC128")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029CB RID: 10699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600267A")]
		[Address(RVA = "0x1012CC1E0", Offset = "0x12CC1E0", VA = "0x1012CC1E0")]
		private void CallbackGetRoomList(INetComHandle phandle)
		{
		}

		// Token: 0x060029CC RID: 10700 RVA: 0x00011A60 File Offset: 0x0000FC60
		[Token(Token = "0x600267B")]
		[Address(RVA = "0x1012CC354", Offset = "0x12CC354", VA = "0x1012CC354")]
		public bool DefaultListUp()
		{
			return default(bool);
		}

		// Token: 0x060029CD RID: 10701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600267C")]
		[Address(RVA = "0x1012CC480", Offset = "0x12CC480", VA = "0x1012CC480")]
		private void CallbackDefaultRoomList(INetComHandle phandle)
		{
		}

		// Token: 0x060029CE RID: 10702 RVA: 0x00011A78 File Offset: 0x0000FC78
		[Token(Token = "0x600267D")]
		[Address(RVA = "0x1012CC48C", Offset = "0x12CC48C", VA = "0x1012CC48C", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x04003A17 RID: 14871
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029F9")]
		private RoomComListSetUp.eStep m_Step;

		// Token: 0x020009CF RID: 2511
		[Token(Token = "0x2000F87")]
		public enum eStep
		{
			// Token: 0x04003A19 RID: 14873
			[Token(Token = "0x4006392")]
			GetState,
			// Token: 0x04003A1A RID: 14874
			[Token(Token = "0x4006393")]
			WaitCheck,
			// Token: 0x04003A1B RID: 14875
			[Token(Token = "0x4006394")]
			End
		}
	}
}
