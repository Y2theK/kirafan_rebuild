﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000813 RID: 2067
	[Token(Token = "0x2000616")]
	[StructLayout(3)]
	public class RoomComManager
	{
		// Token: 0x06002091 RID: 8337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E09")]
		[Address(RVA = "0x1012CC4CC", Offset = "0x12CC4CC", VA = "0x1012CC4CC")]
		public RoomComManager(int ftablenum)
		{
		}

		// Token: 0x06002092 RID: 8338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E0A")]
		[Address(RVA = "0x1012CC540", Offset = "0x12CC540", VA = "0x1012CC540")]
		public void PushComCommand(IMainComCommand pcmd)
		{
		}

		// Token: 0x06002093 RID: 8339 RVA: 0x0000E4F0 File Offset: 0x0000C6F0
		[Token(Token = "0x6001E0B")]
		[Address(RVA = "0x1012CC5E0", Offset = "0x12CC5E0", VA = "0x1012CC5E0")]
		public int GetComCommandNum()
		{
			return 0;
		}

		// Token: 0x06002094 RID: 8340 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001E0C")]
		[Address(RVA = "0x1012CC5E8", Offset = "0x12CC5E8", VA = "0x1012CC5E8")]
		public IMainComCommand GetComCommand(int findex)
		{
			return null;
		}

		// Token: 0x06002095 RID: 8341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E0D")]
		[Address(RVA = "0x1012CC638", Offset = "0x12CC638", VA = "0x1012CC638")]
		public void Flush()
		{
		}

		// Token: 0x0400309E RID: 12446
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40024F2")]
		public int m_Num;

		// Token: 0x0400309F RID: 12447
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40024F3")]
		public int m_Max;

		// Token: 0x040030A0 RID: 12448
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40024F4")]
		public IMainComCommand[] m_Table;
	}
}
