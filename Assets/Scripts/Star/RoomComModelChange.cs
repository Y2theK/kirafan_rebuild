﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A44 RID: 2628
	[Token(Token = "0x2000751")]
	[StructLayout(3)]
	public class RoomComModelChange : IRoomEventCommand
	{
		// Token: 0x06002D5A RID: 11610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B7")]
		[Address(RVA = "0x1012C0D2C", Offset = "0x12C0D2C", VA = "0x1012C0D2C")]
		public RoomComModelChange()
		{
		}

		// Token: 0x06002D5B RID: 11611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B8")]
		[Address(RVA = "0x1012C0D5C", Offset = "0x12C0D5C", VA = "0x1012C0D5C")]
		public void SetUpChangeModel(IRoomObjectControll pbase, long fbasemanageid, int fchangeobj)
		{
		}

		// Token: 0x06002D5C RID: 11612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029B9")]
		[Address(RVA = "0x1012CC6B4", Offset = "0x12CC6B4", VA = "0x1012CC6B4")]
		public void SetUpChangeModel(IRoomObjectControll pbase, long fbasemanageid, int fchangeobj, int subkey)
		{
		}

		// Token: 0x06002D5D RID: 11613 RVA: 0x00013620 File Offset: 0x00011820
		[Token(Token = "0x60029BA")]
		[Address(RVA = "0x1012CC6C4", Offset = "0x12CC6C4", VA = "0x1012CC6C4", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			return default(bool);
		}

		// Token: 0x04003CD1 RID: 15569
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BCE")]
		public int m_ObjID;

		// Token: 0x04003CD2 RID: 15570
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BCF")]
		public IRoomObjectControll m_Target;

		// Token: 0x04003CD3 RID: 15571
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002BD0")]
		public IRoomObjectControll m_NewModel;

		// Token: 0x04003CD4 RID: 15572
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002BD1")]
		public int m_Step;

		// Token: 0x04003CD5 RID: 15573
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002BD2")]
		public long m_ManageID;
	}
}
