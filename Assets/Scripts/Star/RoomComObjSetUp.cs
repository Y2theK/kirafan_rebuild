﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x020009D0 RID: 2512
	[Token(Token = "0x2000718")]
	[StructLayout(3)]
	public class RoomComObjSetUp : IFldNetComModule
	{
		// Token: 0x060029CF RID: 10703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600267E")]
		[Address(RVA = "0x1012CC96C", Offset = "0x12CC96C", VA = "0x1012CC96C")]
		public RoomComObjSetUp()
		{
		}

		// Token: 0x060029D0 RID: 10704 RVA: 0x00011A90 File Offset: 0x0000FC90
		[Token(Token = "0x600267F")]
		[Address(RVA = "0x1012CC9D0", Offset = "0x12CC9D0", VA = "0x1012CC9D0")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029D1 RID: 10705 RVA: 0x00011AA8 File Offset: 0x0000FCA8
		[Token(Token = "0x6002680")]
		[Address(RVA = "0x1012CCA88", Offset = "0x12CCA88", VA = "0x1012CCA88")]
		public bool DefaultObjAdd()
		{
			return default(bool);
		}

		// Token: 0x060029D2 RID: 10706 RVA: 0x00011AC0 File Offset: 0x0000FCC0
		[Token(Token = "0x6002681")]
		[Address(RVA = "0x1012CCB30", Offset = "0x12CCB30", VA = "0x1012CCB30", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060029D3 RID: 10707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002682")]
		[Address(RVA = "0x1012CCC30", Offset = "0x12CCC30", VA = "0x1012CCC30")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029D4 RID: 10708 RVA: 0x00011AD8 File Offset: 0x0000FCD8
		[Token(Token = "0x6002683")]
		[Address(RVA = "0x1012CCFC0", Offset = "0x12CCFC0", VA = "0x1012CCFC0")]
		public static int MakeSetUpToDBAccessKey(ref DefaultObjectListDB_Param pparam)
		{
			return 0;
		}

		// Token: 0x060029D5 RID: 10709 RVA: 0x00011AF0 File Offset: 0x0000FCF0
		[Token(Token = "0x6002684")]
		[Address(RVA = "0x1012CCD10", Offset = "0x12CCD10", VA = "0x1012CCD10")]
		private bool CheckRoomObjectListKey(GetAll pres)
		{
			return default(bool);
		}

		// Token: 0x04003A1C RID: 14876
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029FA")]
		private RoomComObjSetUp.eStep m_Step;

		// Token: 0x04003A1D RID: 14877
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029FB")]
		private List<RoomComObjSetUp.DefaultSetUpQue> m_SendQue;

		// Token: 0x020009D1 RID: 2513
		[Token(Token = "0x2000F88")]
		public enum eStep
		{
			// Token: 0x04003A1F RID: 14879
			[Token(Token = "0x4006396")]
			GetState,
			// Token: 0x04003A20 RID: 14880
			[Token(Token = "0x4006397")]
			WaitCheck,
			// Token: 0x04003A21 RID: 14881
			[Token(Token = "0x4006398")]
			DefaultObjUp,
			// Token: 0x04003A22 RID: 14882
			[Token(Token = "0x4006399")]
			SetUpCheck,
			// Token: 0x04003A23 RID: 14883
			[Token(Token = "0x400639A")]
			End
		}

		// Token: 0x020009D2 RID: 2514
		[Token(Token = "0x2000F89")]
		public class DefaultSetUpQue
		{
			// Token: 0x060029D6 RID: 10710 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600600F")]
			[Address(RVA = "0x1012CCFF4", Offset = "0x12CCFF4", VA = "0x1012CCFF4")]
			public DefaultSetUpQue(int fobjid, int fnum)
			{
			}

			// Token: 0x060029D7 RID: 10711 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006010")]
			[Address(RVA = "0x1012CD02C", Offset = "0x12CD02C", VA = "0x1012CD02C")]
			public void SendUp(IFldNetComManager pmanager)
			{
			}

			// Token: 0x060029D8 RID: 10712 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006011")]
			[Address(RVA = "0x1012CD124", Offset = "0x12CD124", VA = "0x1012CD124")]
			public void CallbackDefaultAdd(INetComHandle phandle)
			{
			}

			// Token: 0x04003A24 RID: 14884
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400639B")]
			public int m_ObjResID;

			// Token: 0x04003A25 RID: 14885
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400639C")]
			public int m_Num;

			// Token: 0x04003A26 RID: 14886
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400639D")]
			public bool m_ListUp;
		}
	}
}
