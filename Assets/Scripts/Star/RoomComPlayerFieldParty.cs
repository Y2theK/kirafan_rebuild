﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009D3 RID: 2515
	[Token(Token = "0x2000719")]
	[StructLayout(3)]
	public class RoomComPlayerFieldParty : IFldNetComModule
	{
		// Token: 0x060029D9 RID: 10713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002685")]
		[Address(RVA = "0x1012CD31C", Offset = "0x12CD31C", VA = "0x1012CD31C")]
		public RoomComPlayerFieldParty(long fplayerid, RoomFriendFieldCharaIO pio)
		{
		}

		// Token: 0x060029DA RID: 10714 RVA: 0x00011B08 File Offset: 0x0000FD08
		[Token(Token = "0x6002686")]
		[Address(RVA = "0x1012CD394", Offset = "0x12CD394", VA = "0x1012CD394")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029DB RID: 10715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002687")]
		[Address(RVA = "0x1012CD464", Offset = "0x12CD464", VA = "0x1012CD464")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029DC RID: 10716 RVA: 0x00011B20 File Offset: 0x0000FD20
		[Token(Token = "0x6002688")]
		[Address(RVA = "0x1012CD668", Offset = "0x12CD668", VA = "0x1012CD668", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x04003A27 RID: 14887
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029FC")]
		private RoomComPlayerFieldParty.eStep m_Step;

		// Token: 0x04003A28 RID: 14888
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029FD")]
		private long m_PlayerId;

		// Token: 0x04003A29 RID: 14889
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029FE")]
		private RoomFriendFieldCharaIO m_IO;

		// Token: 0x020009D4 RID: 2516
		[Token(Token = "0x2000F8A")]
		public enum eStep
		{
			// Token: 0x04003A2B RID: 14891
			[Token(Token = "0x400639F")]
			GetState,
			// Token: 0x04003A2C RID: 14892
			[Token(Token = "0x40063A0")]
			WaitCheck,
			// Token: 0x04003A2D RID: 14893
			[Token(Token = "0x40063A1")]
			End
		}
	}
}
