﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009D5 RID: 2517
	[Token(Token = "0x200071A")]
	[StructLayout(3)]
	public class RoomComPlayerPlaceObj : IFldNetComModule
	{
		// Token: 0x060029DD RID: 10717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002689")]
		[Address(RVA = "0x1012CD6A8", Offset = "0x12CD6A8", VA = "0x1012CD6A8")]
		public RoomComPlayerPlaceObj(long fplayerid)
		{
		}

		// Token: 0x060029DE RID: 10718 RVA: 0x00011B38 File Offset: 0x0000FD38
		[Token(Token = "0x600268A")]
		[Address(RVA = "0x1012CD71C", Offset = "0x12CD71C", VA = "0x1012CD71C")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029DF RID: 10719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600268B")]
		[Address(RVA = "0x1012CD7EC", Offset = "0x12CD7EC", VA = "0x1012CD7EC")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029E0 RID: 10720 RVA: 0x00011B50 File Offset: 0x0000FD50
		[Token(Token = "0x600268C")]
		[Address(RVA = "0x1012CD8B4", Offset = "0x12CD8B4", VA = "0x1012CD8B4", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x04003A2E RID: 14894
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40029FF")]
		private RoomComPlayerPlaceObj.eStep m_Step;

		// Token: 0x04003A2F RID: 14895
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A00")]
		private long m_PlayerId;

		// Token: 0x020009D6 RID: 2518
		[Token(Token = "0x2000F8B")]
		public enum eStep
		{
			// Token: 0x04003A31 RID: 14897
			[Token(Token = "0x40063A3")]
			GetState,
			// Token: 0x04003A32 RID: 14898
			[Token(Token = "0x40063A4")]
			WaitCheck,
			// Token: 0x04003A33 RID: 14899
			[Token(Token = "0x40063A5")]
			End
		}
	}
}
