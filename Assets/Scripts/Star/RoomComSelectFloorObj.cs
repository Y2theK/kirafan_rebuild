﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A45 RID: 2629
	[Token(Token = "0x2000752")]
	[StructLayout(3)]
	public class RoomComSelectFloorObj : IRoomEventCommand
	{
		// Token: 0x06002D5E RID: 11614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029BB")]
		[Address(RVA = "0x1012CD8F4", Offset = "0x12CD8F4", VA = "0x1012CD8F4")]
		public RoomComSelectFloorObj(int floorID, bool fselect)
		{
		}

		// Token: 0x06002D5F RID: 11615 RVA: 0x00013638 File Offset: 0x00011838
		[Token(Token = "0x60029BC")]
		[Address(RVA = "0x1012CD920", Offset = "0x12CD920", VA = "0x1012CD920", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			return default(bool);
		}

		// Token: 0x04003CD6 RID: 15574
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BD3")]
		private int m_Step;
	}
}
