﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009D7 RID: 2519
	[Token(Token = "0x200071B")]
	[StructLayout(3)]
	public class RoomComSetUp : IFldNetComModule
	{
		// Token: 0x060029E1 RID: 10721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600268D")]
		[Address(RVA = "0x1012CD954", Offset = "0x12CD954", VA = "0x1012CD954")]
		public RoomComSetUp()
		{
		}

		// Token: 0x060029E2 RID: 10722 RVA: 0x00011B68 File Offset: 0x0000FD68
		[Token(Token = "0x600268E")]
		[Address(RVA = "0x1012CD9DC", Offset = "0x12CD9DC", VA = "0x1012CD9DC")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060029E3 RID: 10723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600268F")]
		[Address(RVA = "0x1012CDB04", Offset = "0x12CDB04", VA = "0x1012CDB04")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060029E4 RID: 10724 RVA: 0x00011B80 File Offset: 0x0000FD80
		[Token(Token = "0x6002690")]
		[Address(RVA = "0x1012CDE3C", Offset = "0x12CDE3C", VA = "0x1012CDE3C")]
		public bool DefaultRoomPlace()
		{
			return default(bool);
		}

		// Token: 0x060029E5 RID: 10725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002691")]
		[Address(RVA = "0x1012CDF3C", Offset = "0x12CDF3C", VA = "0x1012CDF3C")]
		private void CallbackDefaultRoomPlace(INetComHandle phandle)
		{
		}

		// Token: 0x060029E6 RID: 10726 RVA: 0x00011B98 File Offset: 0x0000FD98
		[Token(Token = "0x6002692")]
		[Address(RVA = "0x1012CE0D0", Offset = "0x12CE0D0", VA = "0x1012CE0D0")]
		public bool DefaultRoomActive()
		{
			return default(bool);
		}

		// Token: 0x060029E7 RID: 10727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002693")]
		[Address(RVA = "0x1012CE200", Offset = "0x12CE200", VA = "0x1012CE200")]
		private void CallbackDefaultRoomList(INetComHandle phandle)
		{
		}

		// Token: 0x060029E8 RID: 10728 RVA: 0x00011BB0 File Offset: 0x0000FDB0
		[Token(Token = "0x6002694")]
		[Address(RVA = "0x1012CE20C", Offset = "0x12CE20C", VA = "0x1012CE20C", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060029E9 RID: 10729 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002695")]
		[Address(RVA = "0x1012CE330", Offset = "0x12CE330", VA = "0x1012CE330")]
		public static UserRoomData SetUpDefaultRoomKey(DefaultObjectListDB pdb, eDefCheckUpType fcheck)
		{
			return null;
		}

		// Token: 0x060029EA RID: 10730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002696")]
		[Address(RVA = "0x1012CDC04", Offset = "0x12CDC04", VA = "0x1012CDC04")]
		private void CheckDefaultDataUp(int fretroom)
		{
		}

		// Token: 0x060029EB RID: 10731 RVA: 0x00011BC8 File Offset: 0x0000FDC8
		[Token(Token = "0x6002697")]
		[Address(RVA = "0x1012CE8E8", Offset = "0x12CE8E8", VA = "0x1012CE8E8")]
		public static bool CheckDefaultDataUp(eDefCheckUpType fcheck)
		{
			return default(bool);
		}

		// Token: 0x04003A34 RID: 14900
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A01")]
		private RoomComSetUp.eStep m_Step;

		// Token: 0x04003A35 RID: 14901
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A02")]
		private List<RoomComSetUp.eStep> m_DefaultStep;

		// Token: 0x04003A36 RID: 14902
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A03")]
		private string m_DefaultRoomPlacement;

		// Token: 0x04003A37 RID: 14903
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A04")]
		private long m_DefSetUpManageId;

		// Token: 0x020009D8 RID: 2520
		[Token(Token = "0x2000F8C")]
		public enum eStep
		{
			// Token: 0x04003A39 RID: 14905
			[Token(Token = "0x40063A7")]
			GetState,
			// Token: 0x04003A3A RID: 14906
			[Token(Token = "0x40063A8")]
			WaitCheck,
			// Token: 0x04003A3B RID: 14907
			[Token(Token = "0x40063A9")]
			DefaultRoomPlace,
			// Token: 0x04003A3C RID: 14908
			[Token(Token = "0x40063AA")]
			DefaultRoomActive,
			// Token: 0x04003A3D RID: 14909
			[Token(Token = "0x40063AB")]
			SetUpCheck,
			// Token: 0x04003A3E RID: 14910
			[Token(Token = "0x40063AC")]
			DefStepChk,
			// Token: 0x04003A3F RID: 14911
			[Token(Token = "0x40063AD")]
			End
		}
	}
}
