﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000815 RID: 2069
	[Token(Token = "0x2000618")]
	[StructLayout(3)]
	public class RoomComUI : IMainComCommand
	{
		// Token: 0x06002096 RID: 8342 RVA: 0x0000E508 File Offset: 0x0000C708
		[Token(Token = "0x6001E0E")]
		[Address(RVA = "0x1012CEB7C", Offset = "0x12CEB7C", VA = "0x1012CEB7C", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E0F")]
		[Address(RVA = "0x1012CEB84", Offset = "0x12CEB84", VA = "0x1012CEB84")]
		public RoomComUI()
		{
		}

		// Token: 0x040030A4 RID: 12452
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40024F8")]
		public eRoomUICategory m_OpenUI;

		// Token: 0x040030A5 RID: 12453
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40024F9")]
		public long m_ManageID;

		// Token: 0x040030A6 RID: 12454
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40024FA")]
		public int m_CharaID;

		// Token: 0x040030A7 RID: 12455
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40024FB")]
		public IRoomObjectControll m_SelectHandle;

		// Token: 0x040030A8 RID: 12456
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40024FC")]
		public int m_ArousalLv;
	}
}
