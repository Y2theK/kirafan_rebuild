﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020009D9 RID: 2521
	[Token(Token = "0x200071C")]
	[StructLayout(3)]
	public class RoomComUnlockDefault : IFldNetComModule
	{
		// Token: 0x060029EC RID: 10732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002698")]
		[Address(RVA = "0x1012CEB8C", Offset = "0x12CEB8C", VA = "0x1012CEB8C")]
		public RoomComUnlockDefault(int fupkey)
		{
		}

		// Token: 0x060029ED RID: 10733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002699")]
		[Address(RVA = "0x1012CEBFC", Offset = "0x12CEBFC", VA = "0x1012CEBFC")]
		public void PlaySend()
		{
		}

		// Token: 0x060029EE RID: 10734 RVA: 0x00011BE0 File Offset: 0x0000FDE0
		[Token(Token = "0x600269A")]
		[Address(RVA = "0x1012CEC34", Offset = "0x12CEC34", VA = "0x1012CEC34", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060029EF RID: 10735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600269B")]
		[Address(RVA = "0x1012CF3B8", Offset = "0x12CF3B8", VA = "0x1012CF3B8")]
		private void CheckListUpKey(int ftableid, int fobjid)
		{
		}

		// Token: 0x060029F0 RID: 10736 RVA: 0x00011BF8 File Offset: 0x0000FDF8
		[Token(Token = "0x600269C")]
		[Address(RVA = "0x1012CF630", Offset = "0x12CF630", VA = "0x1012CF630")]
		private int GetListUpKeyToTableList(int findex)
		{
			return 0;
		}

		// Token: 0x060029F1 RID: 10737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600269D")]
		[Address(RVA = "0x1012CECDC", Offset = "0x12CECDC", VA = "0x1012CECDC")]
		private void MakeRoomDefaultObjectListUp()
		{
		}

		// Token: 0x060029F2 RID: 10738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600269E")]
		[Address(RVA = "0x1012CF76C", Offset = "0x12CF76C", VA = "0x1012CF76C")]
		public void CallbackDefaultAdd(INetComHandle phandle)
		{
		}

		// Token: 0x060029F3 RID: 10739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600269F")]
		[Address(RVA = "0x1012CF0A8", Offset = "0x12CF0A8", VA = "0x1012CF0A8")]
		private void MakeSubRoomSetUp()
		{
		}

		// Token: 0x060029F4 RID: 10740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026A0")]
		[Address(RVA = "0x1012CFAD0", Offset = "0x12CFAD0", VA = "0x1012CFAD0")]
		private void CallbackDefaultRoomPlace(INetComHandle phandle)
		{
		}

		// Token: 0x04003A40 RID: 14912
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A05")]
		private RoomComUnlockDefault.eStep m_Step;

		// Token: 0x04003A41 RID: 14913
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A06")]
		private List<RoomComUnlockDefault.SetUpKey> m_SetObjNo;

		// Token: 0x04003A42 RID: 14914
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A07")]
		private long[] m_SettingObj;

		// Token: 0x04003A43 RID: 14915
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A08")]
		private bool m_ComUp;

		// Token: 0x04003A44 RID: 14916
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002A09")]
		private eDefCheckUpType m_SetUp;

		// Token: 0x020009DA RID: 2522
		[Token(Token = "0x2000F8D")]
		public enum eStep
		{
			// Token: 0x04003A46 RID: 14918
			[Token(Token = "0x40063AF")]
			GetDatabase,
			// Token: 0x04003A47 RID: 14919
			[Token(Token = "0x40063B0")]
			WaitResourceUp,
			// Token: 0x04003A48 RID: 14920
			[Token(Token = "0x40063B1")]
			SetUpSettingObject,
			// Token: 0x04003A49 RID: 14921
			[Token(Token = "0x40063B2")]
			SettingUpCheck,
			// Token: 0x04003A4A RID: 14922
			[Token(Token = "0x40063B3")]
			SetUpRoomFloor,
			// Token: 0x04003A4B RID: 14923
			[Token(Token = "0x40063B4")]
			RoomFloorUpCheck,
			// Token: 0x04003A4C RID: 14924
			[Token(Token = "0x40063B5")]
			End
		}

		// Token: 0x020009DB RID: 2523
		[Token(Token = "0x2000F8E")]
		public class SetUpKey
		{
			// Token: 0x060029F5 RID: 10741 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006012")]
			[Address(RVA = "0x1012CF628", Offset = "0x12CF628", VA = "0x1012CF628")]
			public SetUpKey()
			{
			}

			// Token: 0x04003A4D RID: 14925
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063B6")]
			public int m_ObjNo;

			// Token: 0x04003A4E RID: 14926
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40063B7")]
			public int m_Num;

			// Token: 0x04003A4F RID: 14927
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40063B8")]
			public List<int> m_TableID;
		}
	}
}
