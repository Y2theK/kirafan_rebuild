﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A46 RID: 2630
	[Token(Token = "0x2000753")]
	[StructLayout(3)]
	public class RoomComWallChange : IRoomEventCommand
	{
		// Token: 0x06002D60 RID: 11616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029BD")]
		[Address(RVA = "0x1012CFBA8", Offset = "0x12CFBA8", VA = "0x1012CFBA8")]
		public RoomComWallChange(int fchangeobj, bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x06002D61 RID: 11617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029BE")]
		[Address(RVA = "0x1012CFC08", Offset = "0x12CFC08", VA = "0x1012CFC08")]
		public RoomComWallChange(long fmanageid, int fresourceid, [Optional] Action callback)
		{
		}

		// Token: 0x06002D62 RID: 11618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029BF")]
		[Address(RVA = "0x1012CFC64", Offset = "0x12CFC64", VA = "0x1012CFC64")]
		private void OnResponse(long[] mngObjIDs)
		{
		}

		// Token: 0x06002D63 RID: 11619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029C0")]
		[Address(RVA = "0x1012CFE04", Offset = "0x12CFE04", VA = "0x1012CFE04")]
		private void OnResponse(IFldNetComModule pmodule)
		{
		}

		// Token: 0x06002D64 RID: 11620 RVA: 0x00013650 File Offset: 0x00011850
		[Token(Token = "0x60029C1")]
		[Address(RVA = "0x1012CFE14", Offset = "0x12CFE14", VA = "0x1012CFE14", Slot = "4")]
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x04003CD7 RID: 15575
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002BD4")]
		public int m_ObjID;

		// Token: 0x04003CD8 RID: 15576
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002BD5")]
		public RoomObjectMdlSprite m_NewModelMain;

		// Token: 0x04003CD9 RID: 15577
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002BD6")]
		public RoomObjectMdlSprite m_NewModelSub;

		// Token: 0x04003CDA RID: 15578
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002BD7")]
		public int m_Step;

		// Token: 0x04003CDB RID: 15579
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002BD8")]
		public int m_Flag;

		// Token: 0x04003CDC RID: 15580
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002BD9")]
		public float m_Time;

		// Token: 0x04003CDD RID: 15581
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002BDA")]
		public int m_ChangeResID;

		// Token: 0x04003CDE RID: 15582
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002BDB")]
		public long m_ChangeMngID;

		// Token: 0x04003CDF RID: 15583
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002BDC")]
		public bool m_BuyObj;

		// Token: 0x04003CE0 RID: 15584
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002BDD")]
		public Action m_Callback;

		// Token: 0x04003CE1 RID: 15585
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002BDE")]
		private UserRoomData targetRoomData;

		// Token: 0x04003CE2 RID: 15586
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002BDF")]
		private UserRoomObjectData targetObjectData;

		// Token: 0x04003CE3 RID: 15587
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002BE0")]
		private UserRoomData.PlacementData targetPlacementData;

		// Token: 0x04003CE4 RID: 15588
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002BE1")]
		private long backupPlacementMngId;

		// Token: 0x04003CE5 RID: 15589
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002BE2")]
		private int backupPlacementResId;

		// Token: 0x04003CE6 RID: 15590
		[Token(Token = "0x4002BE3")]
		private const eRoomObjectCategory constCategory = eRoomObjectCategory.Wall;

		// Token: 0x04003CE7 RID: 15591
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002BE4")]
		private RoomBuilder builder;
	}
}
