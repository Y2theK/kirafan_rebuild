﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A2B RID: 2603
	[Token(Token = "0x2000743")]
	[StructLayout(3)]
	public static class RoomDefine
	{
		// Token: 0x04003C1E RID: 15390
		[Token(Token = "0x4002B4D")]
		public const int ROOM_CHARA_LIMIT = 5;

		// Token: 0x04003C1F RID: 15391
		[Token(Token = "0x4002B4E")]
		public const int FLOOR_MAIN_NO = 0;

		// Token: 0x04003C20 RID: 15392
		[Token(Token = "0x4002B4F")]
		public const int FLOOR_SLEEP_NO = 1;

		// Token: 0x04003C21 RID: 15393
		[Token(Token = "0x4002B50")]
		public const int SORT_OFFSET_KEY = 10;

		// Token: 0x04003C22 RID: 15394
		[Token(Token = "0x4002B51")]
		public const int PLACEMENT_MAX = 50;

		// Token: 0x04003C23 RID: 15395
		[Token(Token = "0x4002B52")]
		public const int DEFAULT_ROOM_FLOOR_ID = 1;

		// Token: 0x04003C24 RID: 15396
		[Token(Token = "0x4002B53")]
		public const long RESERVED_PLACEMENT_MANAGED_ID = -2L;

		// Token: 0x04003C25 RID: 15397
		[Token(Token = "0x4002B54")]
		public const int ORDER_BG = -10000;

		// Token: 0x04003C26 RID: 15398
		[Token(Token = "0x4002B55")]
		public const int ORDER_START = 0;

		// Token: 0x04003C27 RID: 15399
		[Token(Token = "0x4002B56")]
		public const int ORDER_GROUP = 4000;

		// Token: 0x04003C28 RID: 15400
		[Token(Token = "0x4002B57")]
		public const int ORDER_WALL = -1000;

		// Token: 0x04003C29 RID: 15401
		[Token(Token = "0x4002B58")]
		public const int ORDER_FLOOR = -960;

		// Token: 0x04003C2A RID: 15402
		[Token(Token = "0x4002B59")]
		public const int ORDER_FLOOR_GROUP = -100;

		// Token: 0x04003C2B RID: 15403
		[Token(Token = "0x4002B5A")]
		public const int ORDER_NO_HIT = -900;

		// Token: 0x04003C2C RID: 15404
		[Token(Token = "0x4002B5B")]
		public const int ORDER_FOREGROUND = 10000;

		// Token: 0x04003C2D RID: 15405
		[Token(Token = "0x4002B5C")]
		public const int PIXCEL_PER_UNIT = 100;

		// Token: 0x04003C2E RID: 15406
		[Token(Token = "0x4002B5D")]
		public const float CHIP_TEX_W = 100f;

		// Token: 0x04003C2F RID: 15407
		[Token(Token = "0x4002B5E")]
		public const float CHIP_TEX_H = 50f;

		// Token: 0x04003C30 RID: 15408
		[Token(Token = "0x4002B5F")]
		public const float CHIP_TEX_W_HALF = 50f;

		// Token: 0x04003C31 RID: 15409
		[Token(Token = "0x4002B60")]
		public const float CHIP_TEX_H_HALF = 25f;

		// Token: 0x04003C32 RID: 15410
		[Token(Token = "0x4002B61")]
		public const int BG_LAYER_OFFSET = 200;

		// Token: 0x04003C33 RID: 15411
		[Token(Token = "0x4002B62")]
		public const float NO_SORTOBJ_OFFSET = 0.9f;

		// Token: 0x04003C34 RID: 15412
		[Token(Token = "0x4002B63")]
		public const byte HitGroupKeyBase = 1;

		// Token: 0x04003C35 RID: 15413
		[Token(Token = "0x4002B64")]
		public const byte HitGroupKeyFloor = 2;

		// Token: 0x04003C36 RID: 15414
		[Token(Token = "0x4002B65")]
		public const float ADDING_TWEET_TIME = 0.75f;

		// Token: 0x04003C37 RID: 15415
		[Token(Token = "0x4002B66")]
		public const float GRID_PLACEMENT_Z_INTERVAL = 3f;

		// Token: 0x04003C38 RID: 15416
		[Token(Token = "0x4002B67")]
		public const int GRID_STATUS_ANY = -1;

		// Token: 0x04003C39 RID: 15417
		[Token(Token = "0x4002B68")]
		public const int GRID_STATUS_EMPTY = 1;

		// Token: 0x04003C3A RID: 15418
		[Token(Token = "0x4002B69")]
		public static Color GRID_HIT_OK_COLOR;

		// Token: 0x04003C3B RID: 15419
		[Token(Token = "0x4002B6A")]
		public static Color GRID_HIT_NG_COLOR;

		// Token: 0x04003C3C RID: 15420
		[Token(Token = "0x4002B6B")]
		public const int CHARA_MOVE_RANGE = 4;

		// Token: 0x04003C3D RID: 15421
		[Token(Token = "0x4002B6C")]
		public static readonly float[] CHARA_IDLE_PROCESS_TIME;

		// Token: 0x04003C3E RID: 15422
		[Token(Token = "0x4002B6D")]
		public const float CHARA_TWEET_PROCESS_TIME = 4.5f;

		// Token: 0x04003C3F RID: 15423
		[Token(Token = "0x4002B6E")]
		public static int ROOM_SEARCH_SIZE;

		// Token: 0x04003C40 RID: 15424
		[Token(Token = "0x4002B6F")]
		public static IVector2[] ROOM_SEARCH_DIR;

		// Token: 0x04003C41 RID: 15425
		[Token(Token = "0x4002B70")]
		public const float CHARA_SCALE = 1.31f;

		// Token: 0x04003C42 RID: 15426
		[Token(Token = "0x4002B71")]
		public const float CHARA_DISPSCALE_MIN = 0.95f;

		// Token: 0x04003C43 RID: 15427
		[Token(Token = "0x4002B72")]
		public const float CHARA_MOVE_SPEED = 0.45f;

		// Token: 0x04003C44 RID: 15428
		[Token(Token = "0x4002B73")]
		public const float CHARA_MOVE_TIME = 1.24226f;

		// Token: 0x04003C45 RID: 15429
		[Token(Token = "0x4002B74")]
		public const int CHARA_INSERT_ORDER = 2;

		// Token: 0x04003C46 RID: 15430
		[Token(Token = "0x4002B75")]
		public const int WAKE_UP_MSG = 2000;

		// Token: 0x04003C47 RID: 15431
		[Token(Token = "0x4002B76")]
		public const int SLEEP_MSG = 2001;

		// Token: 0x04003C48 RID: 15432
		[Token(Token = "0x4002B77")]
		public const int GO_HOME_MSG = 2002;

		// Token: 0x04003C49 RID: 15433
		[Token(Token = "0x4002B78")]
		public const int GO_TOWN_MSG = 2003;

		// Token: 0x04003C4A RID: 15434
		[Token(Token = "0x4002B79")]
		public const float CHARA_TOUCH_PROFILE = 0.5f;

		// Token: 0x04003C4B RID: 15435
		[Token(Token = "0x4002B7A")]
		public const float CHARA_LONG_TAP_TIME = 0.25f;

		// Token: 0x04003C4C RID: 15436
		[Token(Token = "0x4002B7B")]
		public const float OBJ_TOUCH_EDITMODE = 1f;

		// Token: 0x04003C4D RID: 15437
		[Token(Token = "0x4002B7C")]
		public const int ROOMOBJ_COMPKEY_BASE = 0;

		// Token: 0x04003C4E RID: 15438
		[Token(Token = "0x4002B7D")]
		public const int ROOMOBJ_COMPKEY_CHARA = 1000;

		// Token: 0x04003C4F RID: 15439
		[Token(Token = "0x4002B7E")]
		public const int ROOMOBJ_COMPKEY_FLOOR = 10000;

		// Token: 0x04003C50 RID: 15440
		[Token(Token = "0x4002B7F")]
		public const int MAX_ROOMOBJ_GRID_SIZE = 4;

		// Token: 0x02000A2C RID: 2604
		[Token(Token = "0x2000FB6")]
		public enum eSearchDir
		{
			// Token: 0x04003C52 RID: 15442
			[Token(Token = "0x4006443")]
			Down,
			// Token: 0x04003C53 RID: 15443
			[Token(Token = "0x4006444")]
			Right,
			// Token: 0x04003C54 RID: 15444
			[Token(Token = "0x4006445")]
			Up,
			// Token: 0x04003C55 RID: 15445
			[Token(Token = "0x4006446")]
			Left
		}

		// Token: 0x02000A2D RID: 2605
		[Token(Token = "0x2000FB7")]
		public enum eAnim
		{
			// Token: 0x04003C57 RID: 15447
			[Token(Token = "0x4006448")]
			Invalid = -1,
			// Token: 0x04003C58 RID: 15448
			[Token(Token = "0x4006449")]
			Idle,
			// Token: 0x04003C59 RID: 15449
			[Token(Token = "0x400644A")]
			Walk,
			// Token: 0x04003C5A RID: 15450
			[Token(Token = "0x400644B")]
			Enjoy,
			// Token: 0x04003C5B RID: 15451
			[Token(Token = "0x400644C")]
			Pic,
			// Token: 0x04003C5C RID: 15452
			[Token(Token = "0x400644D")]
			Rejoice,
			// Token: 0x04003C5D RID: 15453
			[Token(Token = "0x400644E")]
			Max
		}
	}
}
