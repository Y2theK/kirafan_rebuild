﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009E3 RID: 2531
	[Token(Token = "0x200071E")]
	[StructLayout(3)]
	public class RoomEnvEffect
	{
		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06002A25 RID: 10789 RVA: 0x00011E38 File Offset: 0x00010038
		[Token(Token = "0x170002A5")]
		public int dbIdx
		{
			[Token(Token = "0x60026C2")]
			[Address(RVA = "0x1012D0BB0", Offset = "0x12D0BB0", VA = "0x1012D0BB0")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06002A26 RID: 10790 RVA: 0x00011E50 File Offset: 0x00010050
		[Token(Token = "0x170002A6")]
		public long uniqueID
		{
			[Token(Token = "0x60026C3")]
			[Address(RVA = "0x1012D0BB8", Offset = "0x12D0BB8", VA = "0x1012D0BB8")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06002A27 RID: 10791 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002A28 RID: 10792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002A7")]
		public GameObject target
		{
			[Token(Token = "0x60026C4")]
			[Address(RVA = "0x1012D0BC0", Offset = "0x12D0BC0", VA = "0x1012D0BC0")]
			get
			{
				return null;
			}
			[Token(Token = "0x60026C5")]
			[Address(RVA = "0x1012D0BC8", Offset = "0x12D0BC8", VA = "0x1012D0BC8")]
			set
			{
			}
		}

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06002A29 RID: 10793 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002A2A RID: 10794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002A8")]
		public Transform joint
		{
			[Token(Token = "0x60026C6")]
			[Address(RVA = "0x1012D0BD0", Offset = "0x12D0BD0", VA = "0x1012D0BD0")]
			get
			{
				return null;
			}
			[Token(Token = "0x60026C7")]
			[Address(RVA = "0x1012D0BD8", Offset = "0x12D0BD8", VA = "0x1012D0BD8")]
			set
			{
			}
		}

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06002A2B RID: 10795 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002A2C RID: 10796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002A9")]
		public EffectComponentBase effect
		{
			[Token(Token = "0x60026C8")]
			[Address(RVA = "0x1012D0BE0", Offset = "0x12D0BE0", VA = "0x1012D0BE0")]
			get
			{
				return null;
			}
			[Token(Token = "0x60026C9")]
			[Address(RVA = "0x1012D0BE8", Offset = "0x12D0BE8", VA = "0x1012D0BE8")]
			set
			{
			}
		}

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06002A2D RID: 10797 RVA: 0x00011E68 File Offset: 0x00010068
		// (set) Token: 0x06002A2E RID: 10798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002AA")]
		public bool active
		{
			[Token(Token = "0x60026CA")]
			[Address(RVA = "0x1012D0BF0", Offset = "0x12D0BF0", VA = "0x1012D0BF0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60026CB")]
			[Address(RVA = "0x1012D0BF8", Offset = "0x12D0BF8", VA = "0x1012D0BF8")]
			set
			{
			}
		}

		// Token: 0x06002A2F RID: 10799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026CC")]
		[Address(RVA = "0x1012D0C00", Offset = "0x12D0C00", VA = "0x1012D0C00")]
		public RoomEnvEffect(int dbIdx, long uniqueID)
		{
		}

		// Token: 0x04003A7E RID: 14974
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002A22")]
		private int m_DBIdx;

		// Token: 0x04003A7F RID: 14975
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A23")]
		private long m_UniqueID;

		// Token: 0x04003A80 RID: 14976
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A24")]
		private GameObject m_Target;

		// Token: 0x04003A81 RID: 14977
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A25")]
		private Transform m_Joint;

		// Token: 0x04003A82 RID: 14978
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A26")]
		private EffectComponentBase m_Effect;

		// Token: 0x04003A83 RID: 14979
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A27")]
		private bool m_Active;
	}
}
