﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000591 RID: 1425
	[Token(Token = "0x2000484")]
	[StructLayout(3)]
	public class RoomEnvEffectDB : ScriptableObject
	{
		// Token: 0x060015ED RID: 5613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149D")]
		[Address(RVA = "0x1012D0C3C", Offset = "0x12D0C3C", VA = "0x1012D0C3C")]
		public RoomEnvEffectDB()
		{
		}

		// Token: 0x04001A45 RID: 6725
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013AF")]
		public RoomEnvEffectDB_Param[] m_Params;
	}
}
