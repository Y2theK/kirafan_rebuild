﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000590 RID: 1424
	[Token(Token = "0x2000483")]
	[Serializable]
	[StructLayout(0)]
	public struct RoomEnvEffectDB_Param
	{
		// Token: 0x04001A37 RID: 6711
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40013A1")]
		public int m_EffectID;

		// Token: 0x04001A38 RID: 6712
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40013A2")]
		public string[] m_Conditions;

		// Token: 0x04001A39 RID: 6713
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40013A3")]
		public int m_ConnectCondition;

		// Token: 0x04001A3A RID: 6714
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013A4")]
		public string m_ConnectTarget;

		// Token: 0x04001A3B RID: 6715
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40013A5")]
		public float m_OffsetPosX;

		// Token: 0x04001A3C RID: 6716
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40013A6")]
		public float m_OffsetPosY;

		// Token: 0x04001A3D RID: 6717
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40013A7")]
		public float m_OffsetPosZ;

		// Token: 0x04001A3E RID: 6718
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40013A8")]
		public float m_OffsetRotX;

		// Token: 0x04001A3F RID: 6719
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40013A9")]
		public float m_OffsetRotY;

		// Token: 0x04001A40 RID: 6720
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40013AA")]
		public float m_OffsetRotZ;

		// Token: 0x04001A41 RID: 6721
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40013AB")]
		public float m_ScaleX;

		// Token: 0x04001A42 RID: 6722
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40013AC")]
		public float m_ScaleY;

		// Token: 0x04001A43 RID: 6723
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40013AD")]
		public float m_ScaleZ;

		// Token: 0x04001A44 RID: 6724
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40013AE")]
		public int m_RenderLayer;
	}
}
