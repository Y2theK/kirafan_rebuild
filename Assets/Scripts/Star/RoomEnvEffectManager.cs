﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009E4 RID: 2532
	[Token(Token = "0x200071F")]
	[StructLayout(3)]
	public class RoomEnvEffectManager : MonoBehaviour
	{
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06002A30 RID: 10800 RVA: 0x00011E80 File Offset: 0x00010080
		[Token(Token = "0x170002AB")]
		private bool isAvailable
		{
			[Token(Token = "0x60026CD")]
			[Address(RVA = "0x1012D0C44", Offset = "0x12D0C44", VA = "0x1012D0C44")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06002A31 RID: 10801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026CE")]
		[Address(RVA = "0x1012D0C4C", Offset = "0x12D0C4C", VA = "0x1012D0C4C")]
		private void Start()
		{
		}

		// Token: 0x06002A32 RID: 10802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026CF")]
		[Address(RVA = "0x1012D0C74", Offset = "0x12D0C74", VA = "0x1012D0C74")]
		private void Update()
		{
		}

		// Token: 0x06002A33 RID: 10803 RVA: 0x00011E98 File Offset: 0x00010098
		[Token(Token = "0x60026D0")]
		[Address(RVA = "0x1012D0F0C", Offset = "0x12D0F0C", VA = "0x1012D0F0C")]
		public bool Setup(RoomBuilderBase builder)
		{
			return default(bool);
		}

		// Token: 0x06002A34 RID: 10804 RVA: 0x00011EB0 File Offset: 0x000100B0
		[Token(Token = "0x60026D1")]
		[Address(RVA = "0x1012D17C4", Offset = "0x12D17C4", VA = "0x1012D17C4")]
		private bool IsHitConnectConditionObject(IRoomObjectControll roomObj, string connectTarget, RoomEnvEffectDB_Param roomEnvEffectParam)
		{
			return default(bool);
		}

		// Token: 0x06002A35 RID: 10805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026D2")]
		[Address(RVA = "0x1012D199C", Offset = "0x12D199C", VA = "0x1012D199C")]
		private void MopdifyObjectList(int dbIdx, IRoomObjectControll roomObj, List<RoomEnvEffect> oldList, List<RoomEnvEffect> newList, bool isMeetCondition)
		{
		}

		// Token: 0x06002A36 RID: 10806 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026D3")]
		[Address(RVA = "0x1012D1D90", Offset = "0x12D1D90", VA = "0x1012D1D90")]
		private RoomEnvEffect SearchRoomEnvEffect(List<RoomEnvEffect> list, int dbIdx, long uniqueID)
		{
			return null;
		}

		// Token: 0x06002A37 RID: 10807 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60026D4")]
		[Address(RVA = "0x1012D1EE0", Offset = "0x12D1EE0", VA = "0x1012D1EE0")]
		private List<IRoomObjectControll> SearchRoomObject(int floorID, eRoomEnvEffectConnectCond target)
		{
			return null;
		}

		// Token: 0x06002A38 RID: 10808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026D5")]
		[Address(RVA = "0x1012D20A8", Offset = "0x12D20A8", VA = "0x1012D20A8")]
		private void TransformOffset(Transform dest, Transform joint, RoomEnvEffectDB_Param effectParam)
		{
		}

		// Token: 0x06002A39 RID: 10809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026D6")]
		[Address(RVA = "0x1012D230C", Offset = "0x12D230C", VA = "0x1012D230C")]
		private void TransformOffset(Transform dest, Vector3 basePos, Vector3 baseEuler, Vector3 baseScale, Vector3 ofsPos, Vector3 ofsEuler, Vector3 ofsScale)
		{
		}

		// Token: 0x06002A3A RID: 10810 RVA: 0x00011EC8 File Offset: 0x000100C8
		[Token(Token = "0x60026D7")]
		[Address(RVA = "0x1012D2500", Offset = "0x12D2500", VA = "0x1012D2500")]
		private bool IsMeetCondition_ExistObj(RoomEnvEffectManager.ICondition conditionUnit, List<IRoomObjectControll> charaList, List<IRoomObjectControll> objectList)
		{
			return default(bool);
		}

		// Token: 0x06002A3B RID: 10811 RVA: 0x00011EE0 File Offset: 0x000100E0
		[Token(Token = "0x60026D8")]
		[Address(RVA = "0x1012D27A0", Offset = "0x12D27A0", VA = "0x1012D27A0")]
		private bool IsMeetCondition_PlayingAnim(RoomEnvEffectManager.ICondition conditionUnit, IRoomObjectControll roomObj)
		{
			return default(bool);
		}

		// Token: 0x06002A3C RID: 10812 RVA: 0x00011EF8 File Offset: 0x000100F8
		[Token(Token = "0x60026D9")]
		[Address(RVA = "0x1012D2968", Offset = "0x12D2968", VA = "0x1012D2968")]
		private bool IsMeetCondition(IRoomObjectControll thisRoomObject, RoomEnvEffectManager.ConditionSet conditionSet, List<IRoomObjectControll> charaList, List<IRoomObjectControll> objectList)
		{
			return default(bool);
		}

		// Token: 0x06002A3D RID: 10813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026DA")]
		[Address(RVA = "0x1012D2B70", Offset = "0x12D2B70", VA = "0x1012D2B70")]
		private void MakeEffectList(List<RoomEnvEffect> effectOldList, List<RoomEnvEffect> effectNewList, List<IRoomObjectControll> charaList, List<IRoomObjectControll> objectList)
		{
		}

		// Token: 0x06002A3E RID: 10814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026DB")]
		[Address(RVA = "0x1012D2FF4", Offset = "0x12D2FF4", VA = "0x1012D2FF4")]
		private void DeleteEffectList(List<RoomEnvEffect> effectList)
		{
		}

		// Token: 0x06002A3F RID: 10815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026DC")]
		[Address(RVA = "0x1012D31A4", Offset = "0x12D31A4", VA = "0x1012D31A4")]
		private void UpdateEffectList(List<RoomEnvEffect> effectList)
		{
		}

		// Token: 0x06002A40 RID: 10816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026DD")]
		[Address(RVA = "0x1012D0C78", Offset = "0x12D0C78", VA = "0x1012D0C78")]
		private void Main()
		{
		}

		// Token: 0x06002A41 RID: 10817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60026DE")]
		[Address(RVA = "0x1012D3780", Offset = "0x12D3780", VA = "0x1012D3780")]
		public RoomEnvEffectManager()
		{
		}

		// Token: 0x04003A84 RID: 14980
		[Token(Token = "0x4002A28")]
		private const string CONDITION_SCRIPT_EXIST_OBJ = "exist_obj:";

		// Token: 0x04003A85 RID: 14981
		[Token(Token = "0x4002A29")]
		private const string CONDITION_SCRIPT_PLAY_ANIM = "play_anim:";

		// Token: 0x04003A86 RID: 14982
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002A2A")]
		[SerializeField]
		private List<EffectComponentBase> m_EffectPrefabs;

		// Token: 0x04003A87 RID: 14983
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002A2B")]
		private List<RoomEnvEffect> m_EffectList;

		// Token: 0x04003A88 RID: 14984
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002A2C")]
		private List<RoomEnvEffectManager.ConditionSet> m_ConditionSetList;

		// Token: 0x04003A89 RID: 14985
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002A2D")]
		private RoomBuilderBase m_Builder;

		// Token: 0x04003A8A RID: 14986
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002A2E")]
		private Transform m_ThisTransform;

		// Token: 0x04003A8B RID: 14987
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002A2F")]
		private bool m_isAvailable;

		// Token: 0x020009E5 RID: 2533
		[Token(Token = "0x2000F95")]
		private enum eConditionType
		{
			// Token: 0x04003A8D RID: 14989
			[Token(Token = "0x40063D0")]
			ExistObj,
			// Token: 0x04003A8E RID: 14990
			[Token(Token = "0x40063D1")]
			PlayAnim,
			// Token: 0x04003A8F RID: 14991
			[Token(Token = "0x40063D2")]
			Max
		}

		// Token: 0x020009E6 RID: 2534
		[Token(Token = "0x2000F96")]
		private interface ICondition
		{
			// Token: 0x06002A42 RID: 10818
			[Token(Token = "0x6006021")]
			[Address(Slot = "0")]
			RoomEnvEffectManager.eConditionType GetConditionType();
		}

		// Token: 0x020009E7 RID: 2535
		[Token(Token = "0x2000F97")]
		private class Condition_ExistObj : RoomEnvEffectManager.ICondition
		{
			// Token: 0x06002A43 RID: 10819 RVA: 0x00011F10 File Offset: 0x00010110
			[Token(Token = "0x6006022")]
			[Address(RVA = "0x1012D3840", Offset = "0x12D3840", VA = "0x1012D3840", Slot = "4")]
			public RoomEnvEffectManager.eConditionType GetConditionType()
			{
				return RoomEnvEffectManager.eConditionType.ExistObj;
			}

			// Token: 0x06002A44 RID: 10820 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006023")]
			[Address(RVA = "0x1012D1384", Offset = "0x12D1384", VA = "0x1012D1384")]
			public Condition_ExistObj(string conditionUnit)
			{
			}

			// Token: 0x06002A45 RID: 10821 RVA: 0x00011F28 File Offset: 0x00010128
			[Token(Token = "0x6006024")]
			[Address(RVA = "0x1012D26DC", Offset = "0x12D26DC", VA = "0x1012D26DC")]
			public bool IsMeet(int resourceID)
			{
				return default(bool);
			}

			// Token: 0x04003A90 RID: 14992
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063D3")]
			private List<int> m_ResourceIDList;
		}

		// Token: 0x020009E9 RID: 2537
		[Token(Token = "0x2000F98")]
		private class Condition_PlayAnim : RoomEnvEffectManager.ICondition
		{
			// Token: 0x06002A48 RID: 10824 RVA: 0x00011F58 File Offset: 0x00010158
			[Token(Token = "0x6006025")]
			[Address(RVA = "0x1012D3860", Offset = "0x12D3860", VA = "0x1012D3860", Slot = "4")]
			public RoomEnvEffectManager.eConditionType GetConditionType()
			{
				return RoomEnvEffectManager.eConditionType.ExistObj;
			}

			// Token: 0x06002A49 RID: 10825 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006026")]
			[Address(RVA = "0x1012D1574", Offset = "0x12D1574", VA = "0x1012D1574")]
			public Condition_PlayAnim(string conditionUnit)
			{
			}

			// Token: 0x06002A4A RID: 10826 RVA: 0x00011F70 File Offset: 0x00010170
			[Token(Token = "0x6006027")]
			[Address(RVA = "0x1012D2874", Offset = "0x12D2874", VA = "0x1012D2874")]
			public bool IsMeet(string clipName)
			{
				return default(bool);
			}

			// Token: 0x04003A92 RID: 14994
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063D4")]
			private List<string> m_ClipNameList;
		}

		// Token: 0x020009EB RID: 2539
		[Token(Token = "0x2000F99")]
		private class ConditionSet
		{
			// Token: 0x06002A4D RID: 10829 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006028")]
			[Address(RVA = "0x1012D1314", Offset = "0x12D1314", VA = "0x1012D1314")]
			public ConditionSet()
			{
			}

			// Token: 0x04003A94 RID: 14996
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40063D5")]
			public List<RoomEnvEffectManager.ICondition[]> list;
		}
	}
}
