﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A74 RID: 2676
	[Token(Token = "0x2000771")]
	[StructLayout(3)]
	public class RoomEventCmd_Barrier : RoomEventCmd_Base
	{
		// Token: 0x06002DE0 RID: 11744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A23")]
		[Address(RVA = "0x1012D3884", Offset = "0x12D3884", VA = "0x1012D3884")]
		public RoomEventCmd_Barrier(int id = 0, [Optional] Action<int> callback)
		{
		}

		// Token: 0x06002DE1 RID: 11745 RVA: 0x000138D8 File Offset: 0x00011AD8
		[Token(Token = "0x6002A24")]
		[Address(RVA = "0x1012D3910", Offset = "0x12D3910", VA = "0x1012D3910", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003D97 RID: 15767
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002C5B")]
		private int id;

		// Token: 0x04003D98 RID: 15768
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C5C")]
		private Action<int> callback;
	}
}
