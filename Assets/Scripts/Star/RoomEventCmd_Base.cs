﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A75 RID: 2677
	[Token(Token = "0x2000772")]
	[StructLayout(3)]
	public abstract class RoomEventCmd_Base : IRoomEventCommand
	{
		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06002DE3 RID: 11747 RVA: 0x000138F0 File Offset: 0x00011AF0
		// (set) Token: 0x06002DE2 RID: 11746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002DE")]
		public bool isBarrier
		{
			[Token(Token = "0x6002A26")]
			[Address(RVA = "0x1012D397C", Offset = "0x12D397C", VA = "0x1012D397C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002A25")]
			[Address(RVA = "0x1012D3974", Offset = "0x12D3974", VA = "0x1012D3974")]
			[CompilerGenerated]
			protected set
			{
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x06002DE5 RID: 11749 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06002DE4 RID: 11748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002DF")]
		public UnitRoomBuilder builder
		{
			[Token(Token = "0x6002A28")]
			[Address(RVA = "0x1012D398C", Offset = "0x12D398C", VA = "0x1012D398C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6002A27")]
			[Address(RVA = "0x1012D3984", Offset = "0x12D3984", VA = "0x1012D3984")]
			[CompilerGenerated]
			protected set
			{
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06002DE6 RID: 11750 RVA: 0x00013908 File Offset: 0x00011B08
		// (set) Token: 0x06002DE7 RID: 11751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002E0")]
		public bool pause
		{
			[Token(Token = "0x6002A29")]
			[Address(RVA = "0x1012D3994", Offset = "0x12D3994", VA = "0x1012D3994")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002A2A")]
			[Address(RVA = "0x1012D399C", Offset = "0x12D399C", VA = "0x1012D399C")]
			set
			{
			}
		}

		// Token: 0x06002DE8 RID: 11752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A2B")]
		[Address(RVA = "0x1012D38CC", Offset = "0x12D38CC", VA = "0x1012D38CC")]
		public RoomEventCmd_Base(UnitRoomBuilder builder, bool isBarrier)
		{
		}

		// Token: 0x06002DE9 RID: 11753
		[Token(Token = "0x6002A2C")]
		[Address(Slot = "5")]
		protected abstract bool CalcRequest();

		// Token: 0x06002DEA RID: 11754 RVA: 0x00013920 File Offset: 0x00011B20
		[Token(Token = "0x6002A2D")]
		[Address(RVA = "0x1012D39A4", Offset = "0x12D39A4", VA = "0x1012D39A4", Slot = "4")]
		public override bool CalcRequest(RoomBuilder builder)
		{
			return default(bool);
		}

		// Token: 0x04003D9B RID: 15771
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C5F")]
		private bool pauseFlg;
	}
}
