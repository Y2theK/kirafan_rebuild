﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A76 RID: 2678
	[Token(Token = "0x2000773")]
	[StructLayout(3)]
	public class RoomEventCmd_BuildObj : RoomEventCmd_Base
	{
		// Token: 0x06002DEB RID: 11755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A2E")]
		[Address(RVA = "0x1012D39C0", Offset = "0x12D39C0", VA = "0x1012D39C0")]
		public RoomEventCmd_BuildObj(UnitRoomBuilder builder, bool isBarrier, BuilderManagedId builderManagedId, RoomCacheObjectData roomCacheObjectData, IRoomFloorManager floorManager, RoomGridState roomGridState, int resId, CharacterDefine.eDir dir, int gridX, int gridY, int floorId, bool alphaIn, bool isNight, RoomEventCmd_BuildObj.Callback callback)
		{
		}

		// Token: 0x06002DEC RID: 11756 RVA: 0x00013938 File Offset: 0x00011B38
		[Token(Token = "0x6002A2F")]
		[Address(RVA = "0x1012D3A70", Offset = "0x12D3A70", VA = "0x1012D3A70", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003D9C RID: 15772
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C60")]
		private RoomCacheObjectData roomCacheObjectData;

		// Token: 0x04003D9D RID: 15773
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C61")]
		private IRoomFloorManager floorManager;

		// Token: 0x04003D9E RID: 15774
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C62")]
		private RoomGridState roomGridState;

		// Token: 0x04003D9F RID: 15775
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C63")]
		private int resId;

		// Token: 0x04003DA0 RID: 15776
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002C64")]
		private CharacterDefine.eDir dir;

		// Token: 0x04003DA1 RID: 15777
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C65")]
		private int gridX;

		// Token: 0x04003DA2 RID: 15778
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C66")]
		private int gridY;

		// Token: 0x04003DA3 RID: 15779
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C67")]
		private int floorId;

		// Token: 0x04003DA4 RID: 15780
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002C68")]
		private bool alphaIn;

		// Token: 0x04003DA5 RID: 15781
		[Cpp2IlInjected.FieldOffset(Offset = "0x55")]
		[Token(Token = "0x4002C69")]
		private bool isNight;

		// Token: 0x04003DA6 RID: 15782
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C6A")]
		private float alphaScale;

		// Token: 0x04003DA7 RID: 15783
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002C6B")]
		private BuilderManagedId builderManagedId;

		// Token: 0x04003DA8 RID: 15784
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002C6C")]
		private RoomEventCmd_BuildObj.Callback callback;

		// Token: 0x04003DA9 RID: 15785
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002C6D")]
		private IRoomObjectControll objHandle;

		// Token: 0x04003DAA RID: 15786
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002C6E")]
		private int step;

		// Token: 0x02000A77 RID: 2679
		// (Invoke) Token: 0x06002DEE RID: 11758
		[Token(Token = "0x2000FD0")]
		public delegate void Callback(BuilderManagedId builderManagedId, IRoomObjectControll objHandle);
	}
}
