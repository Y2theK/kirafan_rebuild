﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A78 RID: 2680
	[Token(Token = "0x2000774")]
	[StructLayout(3)]
	public class RoomEventCmd_CameraMove : RoomEventCmd_Base
	{
		// Token: 0x06002DF1 RID: 11761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A30")]
		[Address(RVA = "0x1012D4214", Offset = "0x12D4214", VA = "0x1012D4214")]
		public RoomEventCmd_CameraMove(bool isBarrier, RoomGameCamera camera, float tgtPosX, float tgtPosY, float tgtZoomSize, float time, RoomEventCmd_CameraMove.Callback callback)
		{
		}

		// Token: 0x06002DF2 RID: 11762 RVA: 0x00013950 File Offset: 0x00011B50
		[Token(Token = "0x6002A31")]
		[Address(RVA = "0x1012D4474", Offset = "0x12D4474", VA = "0x1012D4474", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DAB RID: 15787
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C6F")]
		private RoomGameCamera camera;

		// Token: 0x04003DAC RID: 15788
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C70")]
		private Vector3 tgtPos;

		// Token: 0x04003DAD RID: 15789
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002C71")]
		private Vector3 oldPos;

		// Token: 0x04003DAE RID: 15790
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C72")]
		private float tgtZoomSize;

		// Token: 0x04003DAF RID: 15791
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C73")]
		private float oldZoomSize;

		// Token: 0x04003DB0 RID: 15792
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C74")]
		private float time;

		// Token: 0x04003DB1 RID: 15793
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C75")]
		private RoomEventCmd_CameraMove.Callback callback;

		// Token: 0x04003DB2 RID: 15794
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002C76")]
		private float workTime;

		// Token: 0x02000A79 RID: 2681
		// (Invoke) Token: 0x06002DF4 RID: 11764
		[Token(Token = "0x2000FD1")]
		public delegate void Callback();
	}
}
