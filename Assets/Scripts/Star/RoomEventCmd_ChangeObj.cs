﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A7B RID: 2683
	[Token(Token = "0x2000776")]
	[StructLayout(3)]
	public class RoomEventCmd_ChangeObj : RoomEventCmd_Base
	{
		// Token: 0x06002DFD RID: 11773 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A38")]
		[Address(RVA = "0x1012D4AE0", Offset = "0x12D4AE0", VA = "0x1012D4AE0")]
		public RoomObjChangeEffect SearchEffect(int sizeX, int sizeY)
		{
			return null;
		}

		// Token: 0x06002DFE RID: 11774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A39")]
		[Address(RVA = "0x1012D4CE8", Offset = "0x12D4CE8", VA = "0x1012D4CE8")]
		public RoomEventCmd_ChangeObj(UnitRoomBuilder builder, bool isBarrier, RoomCacheObjectData roomCacheObjectData, IRoomFloorManager[] floorManager, RoomGridState[] roomGridState, RoomEventCmd_ChangeObj.DstObj[] dstObjs, RoomEventCmd_ChangeObj.SrcObj[] srcObjs, bool alphaIn, bool isNight, RoomEventCmd_ChangeObj.ChangeEffect changeEffect, RoomEventCmd_ChangeObj.Callback callback)
		{
		}

		// Token: 0x06002DFF RID: 11775 RVA: 0x000139C8 File Offset: 0x00011BC8
		[Token(Token = "0x6002A3A")]
		[Address(RVA = "0x1012D4E0C", Offset = "0x12D4E0C", VA = "0x1012D4E0C", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x06002E00 RID: 11776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A3B")]
		[Address(RVA = "0x1012D6978", Offset = "0x12D6978", VA = "0x1012D6978")]
		private void DeleteSrcData()
		{
		}

		// Token: 0x06002E01 RID: 11777 RVA: 0x000139E0 File Offset: 0x00011BE0
		[Token(Token = "0x6002A3C")]
		[Address(RVA = "0x1012D6CEC", Offset = "0x12D6CEC", VA = "0x1012D6CEC")]
		private float GetScaleCurve(float rate)
		{
			return 0f;
		}

		// Token: 0x04003DB8 RID: 15800
		[Token(Token = "0x4002C7C")]
		private const float fadeSpeed = 0.08f;

		// Token: 0x04003DB9 RID: 15801
		[Token(Token = "0x4002C7D")]
		private const float scaleSizeRate = 0.9f;

		// Token: 0x04003DBA RID: 15802
		[Token(Token = "0x4002C7E")]
		private const float scaleCurveBoundRate = 0.1f;

		// Token: 0x04003DBB RID: 15803
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C7F")]
		private RoomCacheObjectData roomCacheObjectData;

		// Token: 0x04003DBC RID: 15804
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C80")]
		private IRoomFloorManager[] floorManager;

		// Token: 0x04003DBD RID: 15805
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C81")]
		private RoomGridState[] roomGridState;

		// Token: 0x04003DBE RID: 15806
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C82")]
		private RoomEventCmd_ChangeObj.DstObj[] dstObjs;

		// Token: 0x04003DBF RID: 15807
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C83")]
		private RoomEventCmd_ChangeObj.SrcObj[] srcObjs;

		// Token: 0x04003DC0 RID: 15808
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C84")]
		private IRoomObjectControll[] objHandles;

		// Token: 0x04003DC1 RID: 15809
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C85")]
		private bool alphaIn;

		// Token: 0x04003DC2 RID: 15810
		[Cpp2IlInjected.FieldOffset(Offset = "0x59")]
		[Token(Token = "0x4002C86")]
		private bool isNight;

		// Token: 0x04003DC3 RID: 15811
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002C87")]
		private RoomEventCmd_ChangeObj.ChangeEffect changeEffect;

		// Token: 0x04003DC4 RID: 15812
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002C88")]
		private RoomEventCmd_ChangeObj.Effect[] useEffects;

		// Token: 0x04003DC5 RID: 15813
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002C89")]
		private float timeOffset;

		// Token: 0x04003DC6 RID: 15814
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002C8A")]
		private float fadeRate;

		// Token: 0x04003DC7 RID: 15815
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002C8B")]
		private bool deleted;

		// Token: 0x04003DC8 RID: 15816
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002C8C")]
		private RoomEventCmd_ChangeObj.Callback callback;

		// Token: 0x04003DC9 RID: 15817
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002C8D")]
		private int step;

		// Token: 0x02000A7C RID: 2684
		// (Invoke) Token: 0x06002E03 RID: 11779
		[Token(Token = "0x2000FD2")]
		public delegate void Callback(RoomEventCmd_ChangeObj.DstObj[] dstObjs, IRoomObjectControll[] dstRoomObjs, RoomEventCmd_ChangeObj.SrcObj[] srcObjs);

		// Token: 0x02000A7D RID: 2685
		[Token(Token = "0x2000FD3")]
		public class DstObj
		{
			// Token: 0x06002E06 RID: 11782 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006082")]
			[Address(RVA = "0x1012D7410", Offset = "0x12D7410", VA = "0x1012D7410")]
			public DstObj()
			{
			}

			// Token: 0x04003DCA RID: 15818
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064A8")]
			public int resId;

			// Token: 0x04003DCB RID: 15819
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40064A9")]
			public CharacterDefine.eDir dir;

			// Token: 0x04003DCC RID: 15820
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064AA")]
			public int gridX;

			// Token: 0x04003DCD RID: 15821
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40064AB")]
			public int gridY;

			// Token: 0x04003DCE RID: 15822
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40064AC")]
			public int floorId;

			// Token: 0x04003DCF RID: 15823
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40064AD")]
			public BuilderManagedId builderManagedId;
		}

		// Token: 0x02000A7E RID: 2686
		[Token(Token = "0x2000FD4")]
		public class SrcObj
		{
			// Token: 0x06002E07 RID: 11783 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006083")]
			[Address(RVA = "0x1012D7418", Offset = "0x12D7418", VA = "0x1012D7418")]
			public SrcObj()
			{
			}

			// Token: 0x04003DD0 RID: 15824
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064AE")]
			public IRoomObjectControll roomObj;

			// Token: 0x04003DD1 RID: 15825
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064AF")]
			public BuilderManagedId builderManagedId;
		}

		// Token: 0x02000A7F RID: 2687
		[Token(Token = "0x2000FD5")]
		public class ChangeEffect
		{
			// Token: 0x06002E08 RID: 11784 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006084")]
			[Address(RVA = "0x1012D73FC", Offset = "0x12D73FC", VA = "0x1012D73FC")]
			public ChangeEffect()
			{
			}

			// Token: 0x04003DD2 RID: 15826
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064B0")]
			public RoomObjChangeEffect[] effects;

			// Token: 0x04003DD3 RID: 15827
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064B1")]
			public float srcDataDeleteTiming;
		}

		// Token: 0x02000A80 RID: 2688
		[Token(Token = "0x2000FD6")]
		private class Effect
		{
			// Token: 0x06002E09 RID: 11785 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006085")]
			[Address(RVA = "0x1012D6970", Offset = "0x12D6970", VA = "0x1012D6970")]
			public Effect()
			{
			}

			// Token: 0x04003DD4 RID: 15828
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064B2")]
			public EffectComponentBase effect;

			// Token: 0x04003DD5 RID: 15829
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064B3")]
			public Vector3 defaultScale;

			// Token: 0x04003DD6 RID: 15830
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40064B4")]
			public Transform transform;
		}
	}
}
