﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A81 RID: 2689
	[Token(Token = "0x2000777")]
	[StructLayout(3)]
	public class RoomEventCmd_DelChara : RoomEventCmd_Base
	{
		// Token: 0x06002E0A RID: 11786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A3D")]
		[Address(RVA = "0x1012D7420", Offset = "0x12D7420", VA = "0x1012D7420")]
		public RoomEventCmd_DelChara(BuilderManagedId builderManagedId, UnitRoomCharaManager charaManager, int charaId, RoomEventCmd_DelChara.Callback callback, bool isBarrier)
		{
		}

		// Token: 0x06002E0B RID: 11787 RVA: 0x000139F8 File Offset: 0x00011BF8
		[Token(Token = "0x6002A3E")]
		[Address(RVA = "0x1012D7484", Offset = "0x12D7484", VA = "0x1012D7484", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DD7 RID: 15831
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C8E")]
		private BuilderManagedId builderManagedId;

		// Token: 0x04003DD8 RID: 15832
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C8F")]
		private UnitRoomCharaManager charaManager;

		// Token: 0x04003DD9 RID: 15833
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C90")]
		private int charaId;

		// Token: 0x04003DDA RID: 15834
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C91")]
		private RoomEventCmd_DelChara.Callback callback;

		// Token: 0x02000A82 RID: 2690
		// (Invoke) Token: 0x06002E0D RID: 11789
		[Token(Token = "0x2000FD7")]
		public delegate void Callback(BuilderManagedId builderManagedId);
	}
}
