﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A83 RID: 2691
	[Token(Token = "0x2000778")]
	[StructLayout(3)]
	public class RoomEventCmd_DelObj : RoomEventCmd_Base
	{
		// Token: 0x06002E10 RID: 11792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A3F")]
		[Address(RVA = "0x1012D78E8", Offset = "0x12D78E8", VA = "0x1012D78E8")]
		public RoomEventCmd_DelObj(bool isBarrier, BuilderManagedId builderManagedId, RoomCacheObjectData roomCacheObjectData, IRoomFloorManager floorManager, RoomGridState roomGridState, IRoomObjectControll objHandle, RoomEventCmd_DelObj.Callback callback)
		{
		}

		// Token: 0x06002E11 RID: 11793 RVA: 0x00013A10 File Offset: 0x00011C10
		[Token(Token = "0x6002A40")]
		[Address(RVA = "0x1012D795C", Offset = "0x12D795C", VA = "0x1012D795C", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DDB RID: 15835
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C92")]
		private BuilderManagedId builderManagedId;

		// Token: 0x04003DDC RID: 15836
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C93")]
		private RoomCacheObjectData roomCacheObjectData;

		// Token: 0x04003DDD RID: 15837
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C94")]
		private IRoomFloorManager floorManager;

		// Token: 0x04003DDE RID: 15838
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C95")]
		private RoomGridState roomGridState;

		// Token: 0x04003DDF RID: 15839
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C96")]
		private IRoomObjectControll objHandle;

		// Token: 0x04003DE0 RID: 15840
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C97")]
		private RoomEventCmd_DelObj.Callback callback;

		// Token: 0x02000A84 RID: 2692
		// (Invoke) Token: 0x06002E13 RID: 11795
		[Token(Token = "0x2000FD8")]
		public delegate void Callback(BuilderManagedId builderManagedId);
	}
}
