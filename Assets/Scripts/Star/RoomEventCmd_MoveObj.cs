﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A85 RID: 2693
	[Token(Token = "0x2000779")]
	[StructLayout(3)]
	public class RoomEventCmd_MoveObj : RoomEventCmd_Base
	{
		// Token: 0x06002E16 RID: 11798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A41")]
		[Address(RVA = "0x1012D7E5C", Offset = "0x12D7E5C", VA = "0x1012D7E5C")]
		public RoomEventCmd_MoveObj(bool isBarrier, BuilderManagedId builderManagedId, IRoomObjectControll objHandle, IRoomFloorManager[] floorManager, RoomGridState[] gridStatus, CharacterDefine.eDir dir, int gridX, int gridY, RoomEventCmd_MoveObj.Callback callback)
		{
		}

		// Token: 0x06002E17 RID: 11799 RVA: 0x00013A28 File Offset: 0x00011C28
		[Token(Token = "0x6002A42")]
		[Address(RVA = "0x1012D7EE8", Offset = "0x12D7EE8", VA = "0x1012D7EE8", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DE1 RID: 15841
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C98")]
		private BuilderManagedId builderManagedId;

		// Token: 0x04003DE2 RID: 15842
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C99")]
		private IRoomObjectControll objHandle;

		// Token: 0x04003DE3 RID: 15843
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C9A")]
		private IRoomFloorManager[] floorManager;

		// Token: 0x04003DE4 RID: 15844
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C9B")]
		private RoomGridState[] gridStatus;

		// Token: 0x04003DE5 RID: 15845
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C9C")]
		private CharacterDefine.eDir dir;

		// Token: 0x04003DE6 RID: 15846
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C9D")]
		private int gridX;

		// Token: 0x04003DE7 RID: 15847
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C9E")]
		private int gridY;

		// Token: 0x04003DE8 RID: 15848
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C9F")]
		private RoomEventCmd_MoveObj.Callback callback;

		// Token: 0x02000A86 RID: 2694
		// (Invoke) Token: 0x06002E19 RID: 11801
		[Token(Token = "0x2000FD9")]
		public delegate void Callback(BuilderManagedId builderManagedId, bool isCanPlace);
	}
}
