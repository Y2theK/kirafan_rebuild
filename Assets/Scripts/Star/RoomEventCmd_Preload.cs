﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000A87 RID: 2695
	[Token(Token = "0x200077A")]
	[StructLayout(3)]
	public class RoomEventCmd_Preload : RoomEventCmd_Base
	{
		// Token: 0x06002E1C RID: 11804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A43")]
		[Address(RVA = "0x1012D8DA8", Offset = "0x12D8DA8", VA = "0x1012D8DA8")]
		public RoomEventCmd_Preload(bool isBarrier, int resId, CharacterDefine.eDir dir, bool isNight, [Optional] Action<bool> callback)
		{
		}

		// Token: 0x06002E1D RID: 11805 RVA: 0x00013A40 File Offset: 0x00011C40
		[Token(Token = "0x6002A44")]
		[Address(RVA = "0x1012D8E0C", Offset = "0x12D8E0C", VA = "0x1012D8E0C", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DE9 RID: 15849
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002CA0")]
		private int resId;

		// Token: 0x04003DEA RID: 15850
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CA1")]
		private CharacterDefine.eDir dir;

		// Token: 0x04003DEB RID: 15851
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002CA2")]
		private bool isNight;

		// Token: 0x04003DEC RID: 15852
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CA3")]
		private Action<bool> callback;

		// Token: 0x04003DED RID: 15853
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002CA4")]
		private int step;

		// Token: 0x04003DEE RID: 15854
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002CA5")]
		private MeigeResource.Handler[] loadingHandler;
	}
}
