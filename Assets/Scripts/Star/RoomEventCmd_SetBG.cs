﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A88 RID: 2696
	[Token(Token = "0x200077B")]
	[StructLayout(3)]
	public class RoomEventCmd_SetBG : RoomEventCmd_Base
	{
		// Token: 0x06002E1E RID: 11806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A45")]
		[Address(RVA = "0x1012D93F8", Offset = "0x12D93F8", VA = "0x1012D93F8")]
		public RoomEventCmd_SetBG(UnitRoomBuilder builder, bool isBarrier, int resId, bool isNight, bool crossFade, RoomEventCmd_SetBG.Callback callback)
		{
		}

		// Token: 0x06002E1F RID: 11807 RVA: 0x00013A58 File Offset: 0x00011C58
		[Token(Token = "0x6002A46")]
		[Address(RVA = "0x1012D946C", Offset = "0x12D946C", VA = "0x1012D946C", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DEF RID: 15855
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002CA6")]
		private int resId;

		// Token: 0x04003DF0 RID: 15856
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CA7")]
		private bool isNight;

		// Token: 0x04003DF1 RID: 15857
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4002CA8")]
		private bool crossFade;

		// Token: 0x04003DF2 RID: 15858
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CA9")]
		private RoomEventCmd_SetBG.Callback callback;

		// Token: 0x04003DF3 RID: 15859
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002CAA")]
		private int step;

		// Token: 0x04003DF4 RID: 15860
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002CAB")]
		private float time;

		// Token: 0x04003DF5 RID: 15861
		[Token(Token = "0x4002CAC")]
		private const float crossFadeTime = 0.3f;

		// Token: 0x04003DF6 RID: 15862
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002CAD")]
		private RoomObjectMdlSprite newRoomObj;

		// Token: 0x02000A89 RID: 2697
		// (Invoke) Token: 0x06002E21 RID: 11809
		[Token(Token = "0x2000FDA")]
		public delegate void Callback(IRoomObjectControll objHandle);
	}
}
