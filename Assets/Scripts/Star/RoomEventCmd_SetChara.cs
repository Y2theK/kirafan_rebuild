﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A8A RID: 2698
	[Token(Token = "0x200077C")]
	[StructLayout(3)]
	public class RoomEventCmd_SetChara : RoomEventCmd_Base
	{
		// Token: 0x06002E24 RID: 11812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A47")]
		[Address(RVA = "0x1012D9BFC", Offset = "0x12D9BFC", VA = "0x1012D9BFC")]
		public RoomEventCmd_SetChara(BuilderManagedId builderManagedId, UnitRoomCharaManager charaManager, int charaId, int roomId, int slotId, int x, int y, bool isWakeup, RoomEventCmd_SetChara.Callback callback, bool isBarrier)
		{
		}

		// Token: 0x06002E25 RID: 11813 RVA: 0x00013A70 File Offset: 0x00011C70
		[Token(Token = "0x6002A48")]
		[Address(RVA = "0x1012D9C9C", Offset = "0x12D9C9C", VA = "0x1012D9C9C", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003DF7 RID: 15863
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CAE")]
		private BuilderManagedId builderManagedId;

		// Token: 0x04003DF8 RID: 15864
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CAF")]
		private UnitRoomCharaManager charaManager;

		// Token: 0x04003DF9 RID: 15865
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002CB0")]
		private int charaId;

		// Token: 0x04003DFA RID: 15866
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002CB1")]
		private int roomId;

		// Token: 0x04003DFB RID: 15867
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002CB2")]
		private int slotId;

		// Token: 0x04003DFC RID: 15868
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002CB3")]
		private int x;

		// Token: 0x04003DFD RID: 15869
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002CB4")]
		private int y;

		// Token: 0x04003DFE RID: 15870
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002CB5")]
		private bool isWakeup;

		// Token: 0x04003DFF RID: 15871
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002CB6")]
		private RoomEventCmd_SetChara.Callback callback;

		// Token: 0x04003E00 RID: 15872
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002CB7")]
		private int step;

		// Token: 0x04003E01 RID: 15873
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002CB8")]
		private UnitRoomChara roomChara;

		// Token: 0x02000A8B RID: 2699
		// (Invoke) Token: 0x06002E27 RID: 11815
		[Token(Token = "0x2000FDB")]
		public delegate void Callback(BuilderManagedId builderManagedId, UnitRoomChara roomChara);
	}
}
