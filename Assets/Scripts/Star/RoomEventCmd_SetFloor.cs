﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A8C RID: 2700
	[Token(Token = "0x200077D")]
	[StructLayout(3)]
	public class RoomEventCmd_SetFloor : RoomEventCmd_Base
	{
		// Token: 0x06002E2A RID: 11818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A49")]
		[Address(RVA = "0x1012DA268", Offset = "0x12DA268", VA = "0x1012DA268")]
		public RoomEventCmd_SetFloor(UnitRoomBuilder builder, bool isBarrier, IRoomFloorManager[] floorManager, int resId, bool isNight, bool crossFade, RoomEventCmd_SetFloor.Callback callback)
		{
		}

		// Token: 0x06002E2B RID: 11819 RVA: 0x00013A88 File Offset: 0x00011C88
		[Token(Token = "0x6002A4A")]
		[Address(RVA = "0x1012DA2E4", Offset = "0x12DA2E4", VA = "0x1012DA2E4", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003E02 RID: 15874
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CB9")]
		private IRoomFloorManager[] floorManager;

		// Token: 0x04003E03 RID: 15875
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CBA")]
		private int resId;

		// Token: 0x04003E04 RID: 15876
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002CBB")]
		private bool isNight;

		// Token: 0x04003E05 RID: 15877
		[Cpp2IlInjected.FieldOffset(Offset = "0x35")]
		[Token(Token = "0x4002CBC")]
		private bool crossFade;

		// Token: 0x04003E06 RID: 15878
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002CBD")]
		private RoomEventCmd_SetFloor.Callback callback;

		// Token: 0x04003E07 RID: 15879
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002CBE")]
		private int step;

		// Token: 0x04003E08 RID: 15880
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002CBF")]
		private float time;

		// Token: 0x04003E09 RID: 15881
		[Token(Token = "0x4002CC0")]
		private const float crossFadeTime = 0.2f;

		// Token: 0x04003E0A RID: 15882
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002CC1")]
		private RoomObjectMdlSprite[] newRoomObj;

		// Token: 0x02000A8D RID: 2701
		// (Invoke) Token: 0x06002E2D RID: 11821
		[Token(Token = "0x2000FDC")]
		public delegate void Callback();
	}
}
