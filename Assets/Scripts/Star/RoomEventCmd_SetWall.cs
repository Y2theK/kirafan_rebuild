﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A8E RID: 2702
	[Token(Token = "0x200077E")]
	[StructLayout(3)]
	public class RoomEventCmd_SetWall : RoomEventCmd_Base
	{
		// Token: 0x06002E30 RID: 11824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A4B")]
		[Address(RVA = "0x1012DAE70", Offset = "0x12DAE70", VA = "0x1012DAE70")]
		public RoomEventCmd_SetWall(UnitRoomBuilder builder, bool isBarrier, IRoomFloorManager[] floorManager, int resId, bool isNight, bool crossFade, RoomEventCmd_SetWall.Callback callback)
		{
		}

		// Token: 0x06002E31 RID: 11825 RVA: 0x00013AA0 File Offset: 0x00011CA0
		[Token(Token = "0x6002A4C")]
		[Address(RVA = "0x1012DAEEC", Offset = "0x12DAEEC", VA = "0x1012DAEEC", Slot = "5")]
		protected override bool CalcRequest()
		{
			return default(bool);
		}

		// Token: 0x04003E0B RID: 15883
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002CC2")]
		private IRoomFloorManager[] floorManager;

		// Token: 0x04003E0C RID: 15884
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002CC3")]
		private int resId;

		// Token: 0x04003E0D RID: 15885
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002CC4")]
		private bool isNight;

		// Token: 0x04003E0E RID: 15886
		[Cpp2IlInjected.FieldOffset(Offset = "0x35")]
		[Token(Token = "0x4002CC5")]
		private bool crossFade;

		// Token: 0x04003E0F RID: 15887
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002CC6")]
		private RoomEventCmd_SetWall.Callback callback;

		// Token: 0x04003E10 RID: 15888
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002CC7")]
		private int step;

		// Token: 0x04003E11 RID: 15889
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002CC8")]
		private float time;

		// Token: 0x04003E12 RID: 15890
		[Token(Token = "0x4002CC9")]
		private const float crossFadeTime = 0.2f;

		// Token: 0x04003E13 RID: 15891
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002CCA")]
		private RoomObjectMdlSprite[] newRoomObj;

		// Token: 0x02000A8F RID: 2703
		// (Invoke) Token: 0x06002E33 RID: 11827
		[Token(Token = "0x2000FDD")]
		public delegate void Callback();
	}
}
