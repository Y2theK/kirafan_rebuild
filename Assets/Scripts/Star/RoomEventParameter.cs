﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A2E RID: 2606
	[Token(Token = "0x2000744")]
	[StructLayout(3)]
	public class RoomEventParameter
	{
		// Token: 0x06002CBA RID: 11450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002928")]
		[Address(RVA = "0x1012DBA78", Offset = "0x12DBA78", VA = "0x1012DBA78")]
		public RoomEventParameter()
		{
		}

		// Token: 0x04003C5E RID: 15454
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B80")]
		public eRoomObjectEventType m_Type;

		// Token: 0x04003C5F RID: 15455
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B81")]
		public int[] m_Table;
	}
}
