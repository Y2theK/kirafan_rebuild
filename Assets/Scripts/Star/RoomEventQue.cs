﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A58 RID: 2648
	[Token(Token = "0x2000761")]
	[StructLayout(3)]
	public class RoomEventQue
	{
		// Token: 0x06002D91 RID: 11665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029E5")]
		[Address(RVA = "0x1012DBA80", Offset = "0x12DBA80", VA = "0x1012DBA80")]
		public RoomEventQue(int ftablenum)
		{
		}

		// Token: 0x06002D92 RID: 11666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029E6")]
		[Address(RVA = "0x1012DBAF4", Offset = "0x12DBAF4", VA = "0x1012DBAF4")]
		public void PushComCommand(IRoomEventCommand pcmd)
		{
		}

		// Token: 0x06002D93 RID: 11667 RVA: 0x00013740 File Offset: 0x00011940
		[Token(Token = "0x60029E7")]
		[Address(RVA = "0x1012DBB94", Offset = "0x12DBB94", VA = "0x1012DBB94")]
		public int GetComCommandNum()
		{
			return 0;
		}

		// Token: 0x06002D94 RID: 11668 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029E8")]
		[Address(RVA = "0x1012DBB9C", Offset = "0x12DBB9C", VA = "0x1012DBB9C")]
		public IRoomEventCommand GetComCommand(int findex)
		{
			return null;
		}

		// Token: 0x06002D95 RID: 11669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029E9")]
		[Address(RVA = "0x1012DBBEC", Offset = "0x12DBBEC", VA = "0x1012DBBEC")]
		public void Flush()
		{
		}

		// Token: 0x04003D17 RID: 15639
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002C09")]
		public int m_Num;

		// Token: 0x04003D18 RID: 15640
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002C0A")]
		public int m_Max;

		// Token: 0x04003D19 RID: 15641
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C0B")]
		public IRoomEventCommand[] m_Table;
	}
}
