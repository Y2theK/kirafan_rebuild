﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A2F RID: 2607
	[Token(Token = "0x2000745")]
	[Serializable]
	[StructLayout(3)]
	public class RoomFloorCateory
	{
		// Token: 0x06002CBB RID: 11451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002929")]
		[Address(RVA = "0x1012DBE10", Offset = "0x12DBE10", VA = "0x1012DBE10")]
		public RoomFloorCateory()
		{
		}

		// Token: 0x04003C60 RID: 15456
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B82")]
		public int m_RoomID;

		// Token: 0x04003C61 RID: 15457
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002B83")]
		public RoomFloorCateory.eCategory m_Type;

		// Token: 0x04003C62 RID: 15458
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B84")]
		public float m_PosX;

		// Token: 0x04003C63 RID: 15459
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002B85")]
		public float m_PosY;

		// Token: 0x04003C64 RID: 15460
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B86")]
		public int m_SizeX;

		// Token: 0x04003C65 RID: 15461
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002B87")]
		public int m_SizeY;

		// Token: 0x04003C66 RID: 15462
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B88")]
		public int m_SizeZ;

		// Token: 0x04003C67 RID: 15463
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002B89")]
		public Vector2 m_RoomOffset;

		// Token: 0x04003C68 RID: 15464
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002B8A")]
		public Vector2 m_WallOffset;

		// Token: 0x02000A30 RID: 2608
		[Token(Token = "0x2000FB8")]
		public enum eCategory
		{
			// Token: 0x04003C6A RID: 15466
			[Token(Token = "0x4006450")]
			Main,
			// Token: 0x04003C6B RID: 15467
			[Token(Token = "0x4006451")]
			Sleep
		}
	}
}
