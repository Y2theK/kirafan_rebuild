﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009B1 RID: 2481
	[Token(Token = "0x2000703")]
	[StructLayout(3)]
	public class RoomFriendCharaManager : IRoomCharaManager
	{
		// Token: 0x0600294B RID: 10571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025FC")]
		[Address(RVA = "0x1012DBE18", Offset = "0x12DBE18", VA = "0x1012DBE18", Slot = "4")]
		public override void SetLinkManager(RoomBuilderBase pbuilder)
		{
		}

		// Token: 0x0600294C RID: 10572 RVA: 0x00011778 File Offset: 0x0000F978
		[Token(Token = "0x60025FD")]
		[Address(RVA = "0x1012DBEA8", Offset = "0x12DBEA8", VA = "0x1012DBEA8", Slot = "6")]
		public override bool IsBuild()
		{
			return default(bool);
		}

		// Token: 0x0600294D RID: 10573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025FE")]
		[Address(RVA = "0x1012DBF88", Offset = "0x12DBF88", VA = "0x1012DBF88")]
		private void Update()
		{
		}

		// Token: 0x0600294E RID: 10574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60025FF")]
		[Address(RVA = "0x1012DC190", Offset = "0x12DC190", VA = "0x1012DC190", Slot = "10")]
		public override void DeleteChara()
		{
		}

		// Token: 0x0600294F RID: 10575 RVA: 0x00011790 File Offset: 0x0000F990
		[Token(Token = "0x6002600")]
		[Address(RVA = "0x1012DC264", Offset = "0x12DC264", VA = "0x1012DC264", Slot = "11")]
		public override bool IsCompleteDestory()
		{
			return default(bool);
		}

		// Token: 0x06002950 RID: 10576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002601")]
		[Address(RVA = "0x1012DC2F4", Offset = "0x12DC2F4", VA = "0x1012DC2F4")]
		public void DeleteCharacterS(int fcharaid, bool fcutout = false)
		{
		}

		// Token: 0x06002951 RID: 10577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002602")]
		[Address(RVA = "0x1012DC5EC", Offset = "0x12DC5EC", VA = "0x1012DC5EC")]
		private void CheckRoomInManageChara(int fcharaid, int froomid, bool fcutout)
		{
		}

		// Token: 0x06002952 RID: 10578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002603")]
		[Address(RVA = "0x1012DC8F8", Offset = "0x12DC8F8", VA = "0x1012DC8F8", Slot = "7")]
		public override void ResetState()
		{
		}

		// Token: 0x06002953 RID: 10579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002604")]
		[Address(RVA = "0x1012DCAF8", Offset = "0x12DCAF8", VA = "0x1012DCAF8", Slot = "8")]
		public override void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
		}

		// Token: 0x06002954 RID: 10580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002605")]
		[Address(RVA = "0x1012DCCD4", Offset = "0x12DCCD4", VA = "0x1012DCCD4", Slot = "9")]
		public override void SetUpRoomCharacter()
		{
		}

		// Token: 0x06002955 RID: 10581 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002606")]
		[Address(RVA = "0x1012DC79C", Offset = "0x12DC79C", VA = "0x1012DC79C")]
		public RoomCharaModels CheckEntryCharaManager(int fcharaid, int froomid, int arousalLv)
		{
			return null;
		}

		// Token: 0x06002956 RID: 10582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002607")]
		[Address(RVA = "0x1012DCE18", Offset = "0x12DCE18", VA = "0x1012DCE18")]
		public void AddRoomCharaModels(RoomCharaModels pakage)
		{
		}

		// Token: 0x06002957 RID: 10583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002608")]
		[Address(RVA = "0x1012DCFC4", Offset = "0x12DCFC4", VA = "0x1012DCFC4")]
		public RoomFriendCharaManager()
		{
		}

		// Token: 0x04003997 RID: 14743
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40029AF")]
		[SerializeField]
		public RoomCharaModels[] m_CharaTable;

		// Token: 0x04003998 RID: 14744
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40029B0")]
		public int m_EntryNum;

		// Token: 0x04003999 RID: 14745
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40029B1")]
		public int m_EntryMax;

		// Token: 0x0400399A RID: 14746
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40029B2")]
		private int m_PlayFloorID;
	}
}
