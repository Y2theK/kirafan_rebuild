﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009B2 RID: 2482
	[Token(Token = "0x2000704")]
	[StructLayout(3)]
	public class RoomFriendFieldCharaIO : MonoBehaviour
	{
		// Token: 0x06002958 RID: 10584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002609")]
		[Address(RVA = "0x1012DCFCC", Offset = "0x12DCFCC", VA = "0x1012DCFCC")]
		public RoomFriendFieldCharaIO()
		{
		}

		// Token: 0x0400399B RID: 14747
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40029B3")]
		public UserFieldCharaData m_ListUp;
	}
}
