﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000999 RID: 2457
	[Token(Token = "0x20006FB")]
	[StructLayout(3)]
	public class RoomGameCamera : MonoBehaviour
	{
		// Token: 0x170002BD RID: 701
		// (get) Token: 0x0600289D RID: 10397 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000287")]
		public Transform cachedTransform
		{
			[Token(Token = "0x600255F")]
			[Address(RVA = "0x1012D4328", Offset = "0x12D4328", VA = "0x1012D4328")]
			get
			{
				return null;
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x0600289E RID: 10398 RVA: 0x00011130 File Offset: 0x0000F330
		[Token(Token = "0x17000288")]
		public float sizeMin
		{
			[Token(Token = "0x6002560")]
			[Address(RVA = "0x1012DCFD4", Offset = "0x12DCFD4", VA = "0x1012DCFD4")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x0600289F RID: 10399 RVA: 0x00011148 File Offset: 0x0000F348
		[Token(Token = "0x17000289")]
		public float sizeMax
		{
			[Token(Token = "0x6002561")]
			[Address(RVA = "0x1012DCFDC", Offset = "0x12DCFDC", VA = "0x1012DCFDC")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x060028A0 RID: 10400 RVA: 0x00011160 File Offset: 0x0000F360
		[Token(Token = "0x1700028A")]
		public Rect moveRange
		{
			[Token(Token = "0x6002562")]
			[Address(RVA = "0x1012DCFE4", Offset = "0x12DCFE4", VA = "0x1012DCFE4")]
			get
			{
				return default(Rect);
			}
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x060028A1 RID: 10401 RVA: 0x00011178 File Offset: 0x0000F378
		[Token(Token = "0x1700028B")]
		public Rect moveRect
		{
			[Token(Token = "0x6002563")]
			[Address(RVA = "0x1012DCFF0", Offset = "0x12DCFF0", VA = "0x1012DCFF0")]
			get
			{
				return default(Rect);
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x060028A2 RID: 10402 RVA: 0x00011190 File Offset: 0x0000F390
		[Token(Token = "0x1700028C")]
		public Vector2 moveCenter
		{
			[Token(Token = "0x6002564")]
			[Address(RVA = "0x1012DCFFC", Offset = "0x12DCFFC", VA = "0x1012DCFFC")]
			get
			{
				return default(Vector2);
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x060028A3 RID: 10403 RVA: 0x000111A8 File Offset: 0x0000F3A8
		// (set) Token: 0x060028A4 RID: 10404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700028D")]
		public RoomGameCamera.eCamMoveDecisionType decisionType
		{
			[Token(Token = "0x6002565")]
			[Address(RVA = "0x1012DD004", Offset = "0x12DD004", VA = "0x1012DD004")]
			get
			{
				return RoomGameCamera.eCamMoveDecisionType.type_Normal;
			}
			[Token(Token = "0x6002566")]
			[Address(RVA = "0x1012DD00C", Offset = "0x12DD00C", VA = "0x1012DD00C")]
			set
			{
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x060028A5 RID: 10405 RVA: 0x000111C0 File Offset: 0x0000F3C0
		// (set) Token: 0x060028A6 RID: 10406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700028E")]
		public bool PinchControllable
		{
			[Token(Token = "0x6002567")]
			[Address(RVA = "0x1012DD014", Offset = "0x12DD014", VA = "0x1012DD014")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002568")]
			[Address(RVA = "0x1012DD01C", Offset = "0x12DD01C", VA = "0x1012DD01C")]
			set
			{
			}
		}

		// Token: 0x060028A7 RID: 10407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002569")]
		[Address(RVA = "0x1012DD024", Offset = "0x12DD024", VA = "0x1012DD024")]
		private void Awake()
		{
		}

		// Token: 0x060028A8 RID: 10408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600256A")]
		[Address(RVA = "0x1012DD12C", Offset = "0x12DD12C", VA = "0x1012DD12C")]
		private void Update()
		{
		}

		// Token: 0x060028A9 RID: 10409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600256B")]
		[Address(RVA = "0x1012DDB4C", Offset = "0x12DDB4C", VA = "0x1012DDB4C")]
		public void SetIsControllable(bool flg)
		{
		}

		// Token: 0x060028AA RID: 10410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600256C")]
		[Address(RVA = "0x1012D4650", Offset = "0x12D4650", VA = "0x1012D4650")]
		public void SetZoom(float size)
		{
		}

		// Token: 0x060028AB RID: 10411 RVA: 0x000111D8 File Offset: 0x0000F3D8
		[Token(Token = "0x600256D")]
		[Address(RVA = "0x1012D4330", Offset = "0x12D4330", VA = "0x1012D4330")]
		public float GetZoom()
		{
			return 0f;
		}

		// Token: 0x060028AC RID: 10412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600256E")]
		[Address(RVA = "0x1012DD198", Offset = "0x12DD198", VA = "0x1012DD198")]
		public void OnPinchEvent(RoomPinchState pevent)
		{
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x060028AD RID: 10413 RVA: 0x000111F0 File Offset: 0x0000F3F0
		// (set) Token: 0x060028AE RID: 10414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700028F")]
		public bool IsDragging
		{
			[Token(Token = "0x600256F")]
			[Address(RVA = "0x1012DDE04", Offset = "0x12DDE04", VA = "0x1012DDE04")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002570")]
			[Address(RVA = "0x1012DDB64", Offset = "0x12DDB64", VA = "0x1012DDB64")]
			set
			{
			}
		}

		// Token: 0x060028AF RID: 10415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002571")]
		[Address(RVA = "0x1012DDE14", Offset = "0x12DDE14", VA = "0x1012DDE14")]
		public void SetMoveRange(Rect frect)
		{
		}

		// Token: 0x060028B0 RID: 10416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002572")]
		[Address(RVA = "0x1012DDE6C", Offset = "0x12DDE6C", VA = "0x1012DDE6C")]
		private void UpdateMoveRange()
		{
		}

		// Token: 0x060028B1 RID: 10417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002573")]
		[Address(RVA = "0x1012DDB78", Offset = "0x12DDB78", VA = "0x1012DDB78")]
		private void CalcMoveRange(float orthographicSize)
		{
		}

		// Token: 0x060028B2 RID: 10418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002574")]
		[Address(RVA = "0x1012DD310", Offset = "0x12DD310", VA = "0x1012DD310")]
		private void UpdateMove()
		{
		}

		// Token: 0x060028B3 RID: 10419 RVA: 0x00011208 File Offset: 0x0000F408
		[Token(Token = "0x6002575")]
		[Address(RVA = "0x1012DDEA8", Offset = "0x12DDEA8", VA = "0x1012DDEA8")]
		public bool IsCheckTouchSlide(Vector2 fpos1, Vector2 fpos2)
		{
			return default(bool);
		}

		// Token: 0x060028B4 RID: 10420 RVA: 0x00011220 File Offset: 0x0000F420
		[Token(Token = "0x6002576")]
		[Address(RVA = "0x1012DDCF8", Offset = "0x12DDCF8", VA = "0x1012DDCF8")]
		public Vector3 ClampPosition(Vector3 pos)
		{
			return default(Vector3);
		}

		// Token: 0x060028B5 RID: 10421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002577")]
		[Address(RVA = "0x1012D4360", Offset = "0x12D4360", VA = "0x1012D4360")]
		public void SimulatePosition(ref Vector3 pos, ref float size)
		{
		}

		// Token: 0x060028B6 RID: 10422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002578")]
		[Address(RVA = "0x1012DDF68", Offset = "0x12DDF68", VA = "0x1012DDF68")]
		public RoomGameCamera()
		{
		}

		// Token: 0x04003913 RID: 14611
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400295F")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04003914 RID: 14612
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002960")]
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x04003915 RID: 14613
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002961")]
		private RoomPinchEvent m_PinchEventListener;

		// Token: 0x04003916 RID: 14614
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002962")]
		private RoomPinchState m_PinchState;

		// Token: 0x04003917 RID: 14615
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002963")]
		[SerializeField]
		private float m_ThresholdMoveRatio;

		// Token: 0x04003918 RID: 14616
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002964")]
		private float m_ThresholdMovePixel;

		// Token: 0x04003919 RID: 14617
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002965")]
		[SerializeField]
		private float m_SizeMin;

		// Token: 0x0400391A RID: 14618
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002966")]
		[SerializeField]
		private float m_SizeMax;

		// Token: 0x0400391B RID: 14619
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002967")]
		private bool m_IsControllable;

		// Token: 0x0400391C RID: 14620
		[Cpp2IlInjected.FieldOffset(Offset = "0x49")]
		[Token(Token = "0x4002968")]
		private bool m_IsPinching;

		// Token: 0x0400391D RID: 14621
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002969")]
		private float m_SizeOnStartPinch;

		// Token: 0x0400391E RID: 14622
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400296A")]
		private bool m_flgPinchCtrl;

		// Token: 0x0400391F RID: 14623
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400296B")]
		[SerializeField]
		private float m_Inertia;

		// Token: 0x04003920 RID: 14624
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400296C")]
		[SerializeField]
		private float m_SpaceLimitX;

		// Token: 0x04003921 RID: 14625
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400296D")]
		[SerializeField]
		private float m_SpaceLimitY;

		// Token: 0x04003922 RID: 14626
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400296E")]
		private Rect m_MoveRange;

		// Token: 0x04003923 RID: 14627
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400296F")]
		private Rect m_MoveRect;

		// Token: 0x04003924 RID: 14628
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002970")]
		private Vector2 m_MoveCenter;

		// Token: 0x04003925 RID: 14629
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002971")]
		private Vector2 m_vMove;

		// Token: 0x04003926 RID: 14630
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002972")]
		private RoomGameCamera.eDragState m_DragState;

		// Token: 0x04003927 RID: 14631
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4002973")]
		private RoomGameCamera.eCamMoveDecisionType m_DecisionType;

		// Token: 0x04003928 RID: 14632
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002974")]
		private float m_edgeWidth;

		// Token: 0x04003929 RID: 14633
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4002975")]
		private float m_edgeHeight;

		// Token: 0x0400392A RID: 14634
		[Token(Token = "0x4002976")]
		private const float edgeRangeRate = 0.25f;

		// Token: 0x0200099A RID: 2458
		[Token(Token = "0x2000F6E")]
		private enum eDragState
		{
			// Token: 0x0400392C RID: 14636
			[Token(Token = "0x4006328")]
			None = -1,
			// Token: 0x0400392D RID: 14637
			[Token(Token = "0x4006329")]
			Ready,
			// Token: 0x0400392E RID: 14638
			[Token(Token = "0x400632A")]
			Dragging
		}

		// Token: 0x0200099B RID: 2459
		[Token(Token = "0x2000F6F")]
		public enum eCamMoveDecisionType
		{
			// Token: 0x04003930 RID: 14640
			[Token(Token = "0x400632C")]
			type_Normal,
			// Token: 0x04003931 RID: 14641
			[Token(Token = "0x400632D")]
			type_Edge
		}
	}
}
