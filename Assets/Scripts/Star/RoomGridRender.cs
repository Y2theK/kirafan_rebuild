﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A59 RID: 2649
	[Token(Token = "0x2000762")]
	[StructLayout(3)]
	public class RoomGridRender : MonoBehaviour
	{
		// Token: 0x06002D96 RID: 11670 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029EA")]
		[Address(RVA = "0x1012DE024", Offset = "0x12DE024", VA = "0x1012DE024")]
		public static GameObject CreateGridTile(int fsizex, int fsizey)
		{
			return null;
		}

		// Token: 0x06002D97 RID: 11671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029EB")]
		[Address(RVA = "0x1012DE0D8", Offset = "0x12DE0D8", VA = "0x1012DE0D8")]
		public void CreateModel(int fsizex, int fsizey)
		{
		}

		// Token: 0x06002D98 RID: 11672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029EC")]
		[Address(RVA = "0x1012DE5AC", Offset = "0x12DE5AC", VA = "0x1012DE5AC")]
		public RoomGridRender()
		{
		}

		// Token: 0x04003D1A RID: 15642
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C0C")]
		private Mesh m_GridLine;

		// Token: 0x04003D1B RID: 15643
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C0D")]
		private Material m_Material;

		// Token: 0x04003D1C RID: 15644
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C0E")]
		private MeshRenderer m_Render;
	}
}
