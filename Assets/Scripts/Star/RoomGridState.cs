﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A31 RID: 2609
	[Token(Token = "0x2000746")]
	[StructLayout(3)]
	public class RoomGridState
	{
		// Token: 0x06002CBC RID: 11452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600292A")]
		[Address(RVA = "0x1012DE5B4", Offset = "0x12DE5B4", VA = "0x1012DE5B4")]
		public void SetUp(int fgridx, int fgridy, int fgridz)
		{
		}

		// Token: 0x06002CBD RID: 11453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600292B")]
		[Address(RVA = "0x1012DE7C0", Offset = "0x12DE7C0", VA = "0x1012DE7C0")]
		public void Destroy()
		{
		}

		// Token: 0x06002CBE RID: 11454 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600292C")]
		[Address(RVA = "0x1012DE7C8", Offset = "0x12DE7C8", VA = "0x1012DE7C8")]
		public int[,] GetGridMap()
		{
			return null;
		}

		// Token: 0x06002CBF RID: 11455 RVA: 0x00013038 File Offset: 0x00011238
		[Token(Token = "0x600292D")]
		[Address(RVA = "0x1012DE7D0", Offset = "0x12DE7D0", VA = "0x1012DE7D0")]
		public Vector2 CalcFreeMovePoint()
		{
			return default(Vector2);
		}

		// Token: 0x06002CC0 RID: 11456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600292E")]
		[Address(RVA = "0x1012DE9E8", Offset = "0x12DE9E8", VA = "0x1012DE9E8")]
		public void SetGridStatus(int gridX, int gridY, int sizeX, int sizeY, int status, byte fgroupkey)
		{
		}

		// Token: 0x06002CC1 RID: 11457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600292F")]
		[Address(RVA = "0x1012DECF0", Offset = "0x12DECF0", VA = "0x1012DECF0")]
		public void SetGridGroup(int gridX, int gridY, int sizeX, int sizeY, int status, byte fgroupkey)
		{
		}

		// Token: 0x06002CC2 RID: 11458 RVA: 0x00013050 File Offset: 0x00011250
		[Token(Token = "0x6002930")]
		[Address(RVA = "0x1012D876C", Offset = "0x12D876C", VA = "0x1012D876C")]
		public bool IsAnyGridStatus(int gridX, int gridY, int sizeX, int sizeY, eRoomObjectCategory fobjcategory, byte fobjgroup)
		{
			return default(bool);
		}

		// Token: 0x06002CC3 RID: 11459 RVA: 0x00013068 File Offset: 0x00011268
		[Token(Token = "0x6002931")]
		[Address(RVA = "0x1012DEF08", Offset = "0x12DEF08", VA = "0x1012DEF08")]
		public bool IsAnyGridStatus(int gridX, int gridY)
		{
			return default(bool);
		}

		// Token: 0x06002CC4 RID: 11460 RVA: 0x00013080 File Offset: 0x00011280
		[Token(Token = "0x6002932")]
		[Address(RVA = "0x1012DF06C", Offset = "0x12DF06C", VA = "0x1012DF06C")]
		public Vector2 GetNearistEmptyGridPos(Vector2 baseGridPos)
		{
			return default(Vector2);
		}

		// Token: 0x06002CC5 RID: 11461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002933")]
		[Address(RVA = "0x1012DF250", Offset = "0x12DF250", VA = "0x1012DF250")]
		public RoomGridState()
		{
		}

		// Token: 0x04003C6C RID: 15468
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B8B")]
		public int[,] m_GridStatus;

		// Token: 0x04003C6D RID: 15469
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B8C")]
		public byte[,] m_GroupStatus;

		// Token: 0x04003C6E RID: 15470
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B8D")]
		public int m_SizeX;

		// Token: 0x04003C6F RID: 15471
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002B8E")]
		public int m_SizeY;

		// Token: 0x04003C70 RID: 15472
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B8F")]
		public int m_SizeZ;

		// Token: 0x04003C71 RID: 15473
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002B90")]
		public int m_Max;

		// Token: 0x04003C72 RID: 15474
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002B91")]
		public int m_MoveFreeSpaceNum;

		// Token: 0x04003C73 RID: 15475
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002B92")]
		public int m_SubMoveFreeSpaceNum;

		// Token: 0x02000A32 RID: 2610
		[Token(Token = "0x2000FB9")]
		public struct GridState
		{
			// Token: 0x04003C74 RID: 15476
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006452")]
			public int m_Status;
		}
	}
}
