﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A33 RID: 2611
	[Token(Token = "0x2000747")]
	[StructLayout(3)]
	public class RoomHitConnect : MonoBehaviour
	{
		// Token: 0x06002CC6 RID: 11462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002934")]
		[Address(RVA = "0x1012DF258", Offset = "0x12DF258", VA = "0x1012DF258")]
		public RoomHitConnect()
		{
		}

		// Token: 0x04003C75 RID: 15477
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B93")]
		public IRoomObjectControll m_Link;
	}
}
