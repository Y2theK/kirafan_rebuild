﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A34 RID: 2612
	[Token(Token = "0x2000748")]
	[StructLayout(3)]
	public class RoomHitObject : MonoBehaviour
	{
		// Token: 0x06002CC7 RID: 11463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002935")]
		[Address(RVA = "0x1012DF260", Offset = "0x12DF260", VA = "0x1012DF260")]
		public void SetGridInfo(int fpointx, int fpointy, int fcategoryid)
		{
		}

		// Token: 0x06002CC8 RID: 11464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002936")]
		[Address(RVA = "0x1012DF26C", Offset = "0x12DF26C", VA = "0x1012DF26C")]
		public RoomHitObject()
		{
		}

		// Token: 0x04003C76 RID: 15478
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B94")]
		public int m_GridX;

		// Token: 0x04003C77 RID: 15479
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002B95")]
		public int m_GridY;

		// Token: 0x04003C78 RID: 15480
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B96")]
		public int m_CategoryID;
	}
}
