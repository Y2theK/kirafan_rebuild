﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A5A RID: 2650
	[Token(Token = "0x2000763")]
	[StructLayout(3)]
	public class RoomIso
	{
		// Token: 0x06002D99 RID: 11673 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029ED")]
		[Address(RVA = "0x1012DF274", Offset = "0x12DF274", VA = "0x1012DF274")]
		public static RoomIso.IsoBounds CreateIsoBounds(RoomBlockData block)
		{
			return null;
		}

		// Token: 0x06002D9A RID: 11674 RVA: 0x00013758 File Offset: 0x00011958
		[Token(Token = "0x60029EE")]
		[Address(RVA = "0x1012DF328", Offset = "0x12DF328", VA = "0x1012DF328")]
		public static RoomIso.IsoVerts GetIsoVerts(RoomBlockData block)
		{
			return default(RoomIso.IsoVerts);
		}

		// Token: 0x06002D9B RID: 11675 RVA: 0x00013770 File Offset: 0x00011970
		[Token(Token = "0x60029EF")]
		[Address(RVA = "0x1012DF464", Offset = "0x12DF464", VA = "0x1012DF464")]
		public static RoomIso.IsoNamedVerts GetIsoNamedSpaceVerts(RoomBlockData block)
		{
			return default(RoomIso.IsoNamedVerts);
		}

		// Token: 0x06002D9C RID: 11676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029F0")]
		[Address(RVA = "0x1012DF538", Offset = "0x12DF538", VA = "0x1012DF538")]
		public static void SpaceToIso(ref RoomIso.IsoSpc pret, ref RoomIso.IsoPos3 spacePos)
		{
		}

		// Token: 0x06002D9D RID: 11677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029F1")]
		[Address(RVA = "0x1012DF65C", Offset = "0x12DF65C", VA = "0x1012DF65C")]
		public static void UpdateIsoBounds(RoomIso.IsoBounds pbox, RoomBlockData block)
		{
		}

		// Token: 0x06002D9E RID: 11678 RVA: 0x00013788 File Offset: 0x00011988
		[Token(Token = "0x60029F2")]
		[Address(RVA = "0x1012DF850", Offset = "0x12DF850", VA = "0x1012DF850")]
		public static int GetIsoBoxCross(RoomIso.IsoBounds bounds_a, RoomIso.IsoBounds bounds_b)
		{
			return 0;
		}

		// Token: 0x06002D9F RID: 11679 RVA: 0x000137A0 File Offset: 0x000119A0
		[Token(Token = "0x60029F3")]
		[Address(RVA = "0x1012DFA04", Offset = "0x12DFA04", VA = "0x1012DFA04")]
		public static bool AreRangesDisjoint(float a_min, float a_max, float b_min, float b_max)
		{
			return default(bool);
		}

		// Token: 0x06002DA0 RID: 11680 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029F4")]
		[Address(RVA = "0x1012DFA5C", Offset = "0x12DFA5C", VA = "0x1012DFA5C")]
		public static RoomBlockData GetFrontBlockCross(RoomBlockData a, RoomBlockData b, int fcross)
		{
			return null;
		}

		// Token: 0x06002DA1 RID: 11681 RVA: 0x000137B8 File Offset: 0x000119B8
		[Token(Token = "0x60029F5")]
		[Address(RVA = "0x1012DFB68", Offset = "0x12DFB68", VA = "0x1012DFB68")]
		public static float CalcBlockKey(RoomBlockData pblock)
		{
			return 0f;
		}

		// Token: 0x06002DA2 RID: 11682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029F6")]
		[Address(RVA = "0x1012DFC14", Offset = "0x12DFC14", VA = "0x1012DFC14")]
		public RoomIso()
		{
		}

		// Token: 0x02000A5B RID: 2651
		[Token(Token = "0x2000FC4")]
		public struct IsoSpc
		{
			// Token: 0x06002DA3 RID: 11683 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006065")]
			[Address(RVA = "0x100034528", Offset = "0x34528", VA = "0x100034528")]
			public IsoSpc(float _x, float _y, float _h, float _v)
			{
			}

			// Token: 0x06002DA4 RID: 11684 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006066")]
			[Address(RVA = "0x10003453C", Offset = "0x3453C", VA = "0x10003453C")]
			public void Set(float _x, float _y, float _h, float _v)
			{
			}

			// Token: 0x04003D1D RID: 15645
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400647A")]
			public float x;

			// Token: 0x04003D1E RID: 15646
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x400647B")]
			public float y;

			// Token: 0x04003D1F RID: 15647
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400647C")]
			public float h;

			// Token: 0x04003D20 RID: 15648
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x400647D")]
			public float v;
		}

		// Token: 0x02000A5C RID: 2652
		[Token(Token = "0x2000FC5")]
		public struct IsoPos3
		{
			// Token: 0x06002DA5 RID: 11685 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006067")]
			[Address(RVA = "0x100034510", Offset = "0x34510", VA = "0x100034510")]
			public IsoPos3(float _x, float _y, float _z)
			{
			}

			// Token: 0x06002DA6 RID: 11686 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006068")]
			[Address(RVA = "0x10003451C", Offset = "0x3451C", VA = "0x10003451C")]
			public void Set(float _x, float _y, float _z)
			{
			}

			// Token: 0x04003D21 RID: 15649
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400647E")]
			public float x;

			// Token: 0x04003D22 RID: 15650
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x400647F")]
			public float y;

			// Token: 0x04003D23 RID: 15651
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006480")]
			public float z;
		}

		// Token: 0x02000A5D RID: 2653
		[Token(Token = "0x2000FC6")]
		public class IsoBounds
		{
			// Token: 0x06002DA7 RID: 11687 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006069")]
			[Address(RVA = "0x1012DF448", Offset = "0x12DF448", VA = "0x1012DF448")]
			public void Set(float _xmin, float _xmax, float _ymin, float _ymax, float _hmin, float _hmax)
			{
			}

			// Token: 0x06002DA8 RID: 11688 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600606A")]
			[Address(RVA = "0x1012DF440", Offset = "0x12DF440", VA = "0x1012DF440")]
			public IsoBounds()
			{
			}

			// Token: 0x04003D24 RID: 15652
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006481")]
			public float xmin;

			// Token: 0x04003D25 RID: 15653
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006482")]
			public float xmax;

			// Token: 0x04003D26 RID: 15654
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006483")]
			public float ymin;

			// Token: 0x04003D27 RID: 15655
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006484")]
			public float ymax;

			// Token: 0x04003D28 RID: 15656
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006485")]
			public float hmin;

			// Token: 0x04003D29 RID: 15657
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006486")]
			public float hmax;
		}

		// Token: 0x02000A5E RID: 2654
		[Token(Token = "0x2000FC7")]
		public struct IsoVerts
		{
			// Token: 0x04003D2A RID: 15658
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006487")]
			public RoomIso.IsoSpc leftDown;

			// Token: 0x04003D2B RID: 15659
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006488")]
			public RoomIso.IsoSpc rightDown;

			// Token: 0x04003D2C RID: 15660
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006489")]
			public RoomIso.IsoSpc backDown;

			// Token: 0x04003D2D RID: 15661
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400648A")]
			public RoomIso.IsoSpc frontDown;

			// Token: 0x04003D2E RID: 15662
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400648B")]
			public RoomIso.IsoSpc leftUp;

			// Token: 0x04003D2F RID: 15663
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400648C")]
			public RoomIso.IsoSpc rightUp;

			// Token: 0x04003D30 RID: 15664
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x400648D")]
			public RoomIso.IsoSpc backUp;

			// Token: 0x04003D31 RID: 15665
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x400648E")]
			public RoomIso.IsoSpc frontUp;
		}

		// Token: 0x02000A5F RID: 2655
		[Token(Token = "0x2000FC8")]
		public struct IsoNamedVerts
		{
			// Token: 0x06002DA9 RID: 11689 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600606B")]
			[Address(RVA = "0x100034508", Offset = "0x34508", VA = "0x100034508")]
			public void Make(Vector3 fp, Vector3 fs)
			{
			}

			// Token: 0x04003D32 RID: 15666
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400648F")]
			public RoomIso.IsoPos3 leftDown;

			// Token: 0x04003D33 RID: 15667
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4006490")]
			public RoomIso.IsoPos3 rightDown;

			// Token: 0x04003D34 RID: 15668
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006491")]
			public RoomIso.IsoPos3 backDown;

			// Token: 0x04003D35 RID: 15669
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006492")]
			public RoomIso.IsoPos3 frontDown;

			// Token: 0x04003D36 RID: 15670
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006493")]
			public RoomIso.IsoPos3 leftUp;

			// Token: 0x04003D37 RID: 15671
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4006494")]
			public RoomIso.IsoPos3 rightUp;

			// Token: 0x04003D38 RID: 15672
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006495")]
			public RoomIso.IsoPos3 backUp;

			// Token: 0x04003D39 RID: 15673
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x4006496")]
			public RoomIso.IsoPos3 frontUp;
		}
	}
}
