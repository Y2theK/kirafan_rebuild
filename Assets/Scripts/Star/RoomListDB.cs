﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000593 RID: 1427
	[Token(Token = "0x2000486")]
	[StructLayout(3)]
	public class RoomListDB : ScriptableObject
	{
		// Token: 0x060015EE RID: 5614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149E")]
		[Address(RVA = "0x1012DFC1C", Offset = "0x12DFC1C", VA = "0x1012DFC1C")]
		public RoomListDB()
		{
		}

		// Token: 0x04001A4A RID: 6730
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013B4")]
		public RoomListDB_Param[] m_Params;
	}
}
