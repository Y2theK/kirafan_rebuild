﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000525 RID: 1317
	[Token(Token = "0x200041B")]
	[StructLayout(3)]
	public static class RoomListDB_Ext
	{
		// Token: 0x06001584 RID: 5508 RVA: 0x000095E8 File Offset: 0x000077E8
		[Token(Token = "0x6001439")]
		[Address(RVA = "0x1012DFC24", Offset = "0x12DFC24", VA = "0x1012DFC24")]
		public static RoomListDB_Param GetParam(this RoomListDB self, int roomID)
		{
			return default(RoomListDB_Param);
		}
	}
}
