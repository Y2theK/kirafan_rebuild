﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000592 RID: 1426
	[Token(Token = "0x2000485")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct RoomListDB_Param
	{
		// Token: 0x04001A46 RID: 6726
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40013B0")]
		public int m_ID;

		// Token: 0x04001A47 RID: 6727
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40013B1")]
		public int m_BgID;

		// Token: 0x04001A48 RID: 6728
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40013B2")]
		public int m_WallID;

		// Token: 0x04001A49 RID: 6729
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40013B3")]
		public int m_FloorID;
	}
}
