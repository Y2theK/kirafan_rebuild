﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomObjectResponseTypes;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x0200081A RID: 2074
	[Token(Token = "0x200061D")]
	[StructLayout(3)]
	public class RoomMain : GameStateMain
	{
		// Token: 0x0600209D RID: 8349 RVA: 0x0000E550 File Offset: 0x0000C750
		[Token(Token = "0x6001E15")]
		[Address(RVA = "0x1012DFD0C", Offset = "0x12DFD0C", VA = "0x1012DFD0C")]
		public bool IsIdleStep()
		{
			return default(bool);
		}

		// Token: 0x0600209E RID: 8350 RVA: 0x0000E568 File Offset: 0x0000C768
		[Token(Token = "0x6001E16")]
		[Address(RVA = "0x1012DFD1C", Offset = "0x12DFD1C", VA = "0x1012DFD1C")]
		public bool IsBuyStep()
		{
			return default(bool);
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x0600209F RID: 8351 RVA: 0x0000E580 File Offset: 0x0000C780
		[Token(Token = "0x1700022F")]
		public int editingObjNum
		{
			[Token(Token = "0x6001E17")]
			[Address(RVA = "0x1012DFD2C", Offset = "0x12DFD2C", VA = "0x1012DFD2C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x060020A0 RID: 8352 RVA: 0x0000E598 File Offset: 0x0000C798
		[Token(Token = "0x17000230")]
		public int editingObjMax
		{
			[Token(Token = "0x6001E18")]
			[Address(RVA = "0x1012DFD34", Offset = "0x12DFD34", VA = "0x1012DFD34")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060020A1 RID: 8353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E19")]
		[Address(RVA = "0x1012DFD3C", Offset = "0x12DFD3C", VA = "0x1012DFD3C")]
		public void AddEditingObjNum()
		{
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x060020A2 RID: 8354 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000231")]
		public RoomBuilder Builder
		{
			[Token(Token = "0x6001E1A")]
			[Address(RVA = "0x1012DFD88", Offset = "0x12DFD88", VA = "0x1012DFD88")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x060020A3 RID: 8355 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000232")]
		public RoomUI RoomUI
		{
			[Token(Token = "0x6001E1B")]
			[Address(RVA = "0x1012DFD90", Offset = "0x12DFD90", VA = "0x1012DFD90")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x060020A4 RID: 8356 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000233")]
		public RoomShopListUI ShopUI
		{
			[Token(Token = "0x6001E1C")]
			[Address(RVA = "0x1012DFD98", Offset = "0x12DFD98", VA = "0x1012DFD98")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x060020A5 RID: 8357 RVA: 0x0000E5B0 File Offset: 0x0000C7B0
		[Token(Token = "0x17000234")]
		public SceneDefine.eSceneID NextSceneID
		{
			[Token(Token = "0x6001E1D")]
			[Address(RVA = "0x1012DFDA0", Offset = "0x12DFDA0", VA = "0x1012DFDA0")]
			get
			{
				return SceneDefine.eSceneID.Title;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x060020A7 RID: 8359 RVA: 0x0000E5C8 File Offset: 0x0000C7C8
		// (set) Token: 0x060020A6 RID: 8358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000235")]
		public long PlayerID
		{
			[Token(Token = "0x6001E1F")]
			[Address(RVA = "0x1012DFDB0", Offset = "0x12DFDB0", VA = "0x1012DFDB0")]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6001E1E")]
			[Address(RVA = "0x1012DFDA8", Offset = "0x12DFDA8", VA = "0x1012DFDA8")]
			set
			{
			}
		}

		// Token: 0x060020A8 RID: 8360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E20")]
		[Address(RVA = "0x1012DFDB8", Offset = "0x12DFDB8", VA = "0x1012DFDB8")]
		private void Start()
		{
		}

		// Token: 0x060020A9 RID: 8361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E21")]
		[Address(RVA = "0x1012DFE74", Offset = "0x12DFE74", VA = "0x1012DFE74", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060020AA RID: 8362 RVA: 0x0000E5E0 File Offset: 0x0000C7E0
		[Token(Token = "0x6001E22")]
		[Address(RVA = "0x1012DFF4C", Offset = "0x12DFF4C", VA = "0x1012DFF4C", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x060020AB RID: 8363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E23")]
		[Address(RVA = "0x1012DFF68", Offset = "0x12DFF68", VA = "0x1012DFF68")]
		public void RefreshTutorialStep()
		{
		}

		// Token: 0x060020AC RID: 8364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E24")]
		[Address(RVA = "0x1012E0324", Offset = "0x12E0324", VA = "0x1012E0324", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x060020AD RID: 8365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E25")]
		[Address(RVA = "0x1012E17C4", Offset = "0x12E17C4", VA = "0x1012E17C4")]
		public void OnComEvent(IMainComCommand pcmd)
		{
		}

		// Token: 0x060020AE RID: 8366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E26")]
		[Address(RVA = "0x1012E04E8", Offset = "0x12E04E8", VA = "0x1012E04E8")]
		public void FlushCommand()
		{
		}

		// Token: 0x060020AF RID: 8367 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001E27")]
		[Address(RVA = "0x1012E2CCC", Offset = "0x12E2CCC", VA = "0x1012E2CCC", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x060020B0 RID: 8368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E28")]
		[Address(RVA = "0x1012E0894", Offset = "0x12E0894", VA = "0x1012E0894")]
		private void UpdateStep()
		{
		}

		// Token: 0x060020B1 RID: 8369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E29")]
		[Address(RVA = "0x1012E1838", Offset = "0x12E1838", VA = "0x1012E1838")]
		private void ChangeStep(RoomMain.eStep step)
		{
		}

		// Token: 0x060020B2 RID: 8370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2A")]
		[Address(RVA = "0x1012E3344", Offset = "0x12E3344", VA = "0x1012E3344")]
		private void ObjPlacement()
		{
		}

		// Token: 0x060020B3 RID: 8371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2B")]
		[Address(RVA = "0x1012E3B0C", Offset = "0x12E3B0C", VA = "0x1012E3B0C")]
		public void InitializeBuilder()
		{
		}

		// Token: 0x060020B4 RID: 8372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2C")]
		[Address(RVA = "0x1012E3DAC", Offset = "0x12E3DAC", VA = "0x1012E3DAC")]
		public void OnSelectObject(IRoomObjectControll roomObj, bool ftype)
		{
		}

		// Token: 0x060020B5 RID: 8373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2D")]
		[Address(RVA = "0x1012E3F80", Offset = "0x12E3F80", VA = "0x1012E3F80")]
		public void OnUnSelectObject()
		{
		}

		// Token: 0x060020B6 RID: 8374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2E")]
		[Address(RVA = "0x1012E3FAC", Offset = "0x12E3FAC", VA = "0x1012E3FAC")]
		public void OnSelectingObjectCanPlacement()
		{
		}

		// Token: 0x060020B7 RID: 8375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E2F")]
		[Address(RVA = "0x1012E3FE0", Offset = "0x12E3FE0", VA = "0x1012E3FE0")]
		public void OnSelectingObjectCanNotPlacement()
		{
		}

		// Token: 0x060020B8 RID: 8376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E30")]
		[Address(RVA = "0x1012E4014", Offset = "0x12E4014", VA = "0x1012E4014")]
		public void OnObjectDragStart()
		{
		}

		// Token: 0x060020B9 RID: 8377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E31")]
		[Address(RVA = "0x1012E4048", Offset = "0x12E4048", VA = "0x1012E4048")]
		public void OnObjectDragEnd()
		{
		}

		// Token: 0x060020BA RID: 8378 RVA: 0x0000E5F8 File Offset: 0x0000C7F8
		[Token(Token = "0x6001E32")]
		[Address(RVA = "0x1012E4078", Offset = "0x12E4078", VA = "0x1012E4078")]
		public bool InitializeUI()
		{
			return default(bool);
		}

		// Token: 0x060020BB RID: 8379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E33")]
		[Address(RVA = "0x1012E4778", Offset = "0x12E4778", VA = "0x1012E4778")]
		public void SetupUIOnRoomStart()
		{
		}

		// Token: 0x060020BC RID: 8380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E34")]
		[Address(RVA = "0x1012E4808", Offset = "0x12E4808", VA = "0x1012E4808")]
		public void OnOpenObjListCallBack()
		{
		}

		// Token: 0x060020BD RID: 8381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E35")]
		[Address(RVA = "0x1012E4810", Offset = "0x12E4810", VA = "0x1012E4810")]
		private void OnClosedWindow_ConfirmPutAway()
		{
		}

		// Token: 0x060020BE RID: 8382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E36")]
		[Address(RVA = "0x1012E49B0", Offset = "0x12E49B0", VA = "0x1012E49B0")]
		public void OnConfirmBuyPlace()
		{
		}

		// Token: 0x060020BF RID: 8383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E37")]
		[Address(RVA = "0x1012E4BD4", Offset = "0x12E4BD4", VA = "0x1012E4BD4")]
		private void OnConfirmSizeOver(int btn)
		{
		}

		// Token: 0x060020C0 RID: 8384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E38")]
		[Address(RVA = "0x1012E4C40", Offset = "0x12E4C40", VA = "0x1012E4C40")]
		private void OnResponseBuyPlaceRoomObject(long[] mngObjIDs)
		{
		}

		// Token: 0x060020C1 RID: 8385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E39")]
		[Address(RVA = "0x1012E3764", Offset = "0x12E3764", VA = "0x1012E3764")]
		public void OnResultBuy()
		{
		}

		// Token: 0x060020C2 RID: 8386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3A")]
		[Address(RVA = "0x1012E4D08", Offset = "0x12E4D08", VA = "0x1012E4D08")]
		private void OnCancelSelectBed()
		{
		}

		// Token: 0x060020C3 RID: 8387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3B")]
		[Address(RVA = "0x1012E300C", Offset = "0x12E300C", VA = "0x1012E300C")]
		private void SwapBedding()
		{
		}

		// Token: 0x060020C4 RID: 8388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3C")]
		[Address(RVA = "0x1012E3648", Offset = "0x12E3648", VA = "0x1012E3648")]
		private void SwapBackground(bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x060020C5 RID: 8389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3D")]
		[Address(RVA = "0x1012E352C", Offset = "0x12E352C", VA = "0x1012E352C")]
		private void SwapFloorModel(bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x060020C6 RID: 8390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3E")]
		[Address(RVA = "0x1012E33F4", Offset = "0x12E33F4", VA = "0x1012E33F4")]
		private void SwapWallModel(bool fbuyobj, [Optional] Action callback)
		{
		}

		// Token: 0x060020C7 RID: 8391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E3F")]
		[Address(RVA = "0x1012E4D10", Offset = "0x12E4D10", VA = "0x1012E4D10")]
		private void OnClosedADVWindow(int advID)
		{
		}

		// Token: 0x060020C8 RID: 8392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E40")]
		[Address(RVA = "0x1012E4D60", Offset = "0x12E4D60", VA = "0x1012E4D60")]
		private void OnClickBackButton()
		{
		}

		// Token: 0x060020C9 RID: 8393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E41")]
		[Address(RVA = "0x1012E4D70", Offset = "0x12E4D70", VA = "0x1012E4D70")]
		private void OnClickHomeButton()
		{
		}

		// Token: 0x060020CA RID: 8394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E42")]
		[Address(RVA = "0x1012E4E28", Offset = "0x12E4E28", VA = "0x1012E4E28")]
		private void OnClickBackButton_Selecting()
		{
		}

		// Token: 0x060020CB RID: 8395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E43")]
		[Address(RVA = "0x1012E4E78", Offset = "0x12E4E78", VA = "0x1012E4E78")]
		private void OnCloseGreet()
		{
		}

		// Token: 0x060020CC RID: 8396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E44")]
		[Address(RVA = "0x1012E4E7C", Offset = "0x12E4E7C", VA = "0x1012E4E7C")]
		private void OnClickScheduleButton()
		{
		}

		// Token: 0x060020CD RID: 8397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E45")]
		[Address(RVA = "0x1012E4E84", Offset = "0x12E4E84", VA = "0x1012E4E84")]
		private void OnDecideLiveChara(long[] charaMngIDs)
		{
		}

		// Token: 0x060020CE RID: 8398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E46")]
		[Address(RVA = "0x1012E4F18", Offset = "0x12E4F18", VA = "0x1012E4F18")]
		private void OnCloseSchedule()
		{
		}

		// Token: 0x060020CF RID: 8399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E47")]
		[Address(RVA = "0x1012E4F54", Offset = "0x12E4F54", VA = "0x1012E4F54")]
		private void OnClickStoreAll()
		{
		}

		// Token: 0x060020D0 RID: 8400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E48")]
		[Address(RVA = "0x1012E4F5C", Offset = "0x12E4F5C", VA = "0x1012E4F5C")]
		private void OnConfirmStoreAll(int select)
		{
		}

		// Token: 0x060020D1 RID: 8401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E49")]
		[Address(RVA = "0x1012E5144", Offset = "0x12E5144", VA = "0x1012E5144")]
		public void ExecuteStoreAll()
		{
		}

		// Token: 0x060020D2 RID: 8402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E4A")]
		[Address(RVA = "0x1012E5174", Offset = "0x12E5174", VA = "0x1012E5174")]
		private void OnClickRoomChangeCallBack()
		{
		}

		// Token: 0x060020D3 RID: 8403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E4B")]
		[Address(RVA = "0x1012E52F0", Offset = "0x12E52F0", VA = "0x1012E52F0")]
		private void OnClickFriendRoomCallBack()
		{
		}

		// Token: 0x060020D4 RID: 8404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E4C")]
		[Address(RVA = "0x1012E534C", Offset = "0x12E534C", VA = "0x1012E534C")]
		private void OnConfirmRoomChange(int btn)
		{
		}

		// Token: 0x060020D5 RID: 8405 RVA: 0x0000E610 File Offset: 0x0000C810
		[Token(Token = "0x6001E4D")]
		[Address(RVA = "0x1012E2D9C", Offset = "0x12E2D9C", VA = "0x1012E2D9C")]
		private bool CheckSwap(long objMngID)
		{
			return default(bool);
		}

		// Token: 0x060020D6 RID: 8406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E4E")]
		[Address(RVA = "0x1012E2E64", Offset = "0x12E2E64", VA = "0x1012E2E64")]
		private void ShowSwapHaveNumOver(string objName)
		{
		}

		// Token: 0x060020D7 RID: 8407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E4F")]
		[Address(RVA = "0x1012E52F4", Offset = "0x12E52F4", VA = "0x1012E52F4")]
		private void ExecuteRoomChange()
		{
		}

		// Token: 0x060020D8 RID: 8408 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001E50")]
		[Address(RVA = "0x1012CFDFC", Offset = "0x12CFDFC", VA = "0x1012CFDFC")]
		public RoomObjShopList GetShopObjList()
		{
			return null;
		}

		// Token: 0x060020D9 RID: 8409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E51")]
		[Address(RVA = "0x1012E381C", Offset = "0x12E381C", VA = "0x1012E381C")]
		public void Request_Sale(eRoomObjectCategory category, int objID, int objNum, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x060020DA RID: 8410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E52")]
		[Address(RVA = "0x1012E5414", Offset = "0x12E5414", VA = "0x1012E5414")]
		private void OnResponse_Sale(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060020DB RID: 8411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E53")]
		[Address(RVA = "0x1012E5578", Offset = "0x12E5578", VA = "0x1012E5578")]
		public RoomMain()
		{
		}

		// Token: 0x040030AD RID: 12461
		[Token(Token = "0x4002501")]
		public const int STATE_INIT = 0;

		// Token: 0x040030AE RID: 12462
		[Token(Token = "0x4002502")]
		public const int STATE_MAIN = 1;

		// Token: 0x040030AF RID: 12463
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002503")]
		private RoomMain.eStep m_Step;

		// Token: 0x040030B0 RID: 12464
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002504")]
		private RoomMain.ePlacementType m_PlacementType;

		// Token: 0x040030B1 RID: 12465
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002505")]
		[SerializeField]
		private RoomGameCamera m_GameCamera;

		// Token: 0x040030B2 RID: 12466
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002506")]
		[SerializeField]
		private RoomBuilder m_Builder;

		// Token: 0x040030B3 RID: 12467
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002507")]
		[SerializeField]
		private RoomEnvEffectManager m_EnvEffectManager;

		// Token: 0x040030B4 RID: 12468
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002508")]
		private RoomUI m_UI;

		// Token: 0x040030B5 RID: 12469
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002509")]
		private RoomShopListUI m_ShopUI;

		// Token: 0x040030B6 RID: 12470
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400250A")]
		private SceneDefine.eSceneID m_NextSceneID;

		// Token: 0x040030B7 RID: 12471
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400250B")]
		private eRoomObjectCategory m_Category;

		// Token: 0x040030B8 RID: 12472
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400250C")]
		private int m_ObjID;

		// Token: 0x040030B9 RID: 12473
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400250D")]
		private long m_ObjManageID;

		// Token: 0x040030BA RID: 12474
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400250E")]
		private int m_ObjNum;

		// Token: 0x040030BB RID: 12475
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x400250F")]
		private int m_EditingObjNum;

		// Token: 0x040030BC RID: 12476
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002510")]
		private int m_EditingObjMax;

		// Token: 0x040030BD RID: 12477
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002511")]
		private List<RoomMain.GreetData> m_GreetDataStackList;

		// Token: 0x040030BE RID: 12478
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002512")]
		private long m_PlayerID;

		// Token: 0x040030BF RID: 12479
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002513")]
		private RoomObjShopList m_RoomObjShopList;

		// Token: 0x040030C0 RID: 12480
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002514")]
		private RoomComManager m_ComEvent;

		// Token: 0x0200081B RID: 2075
		[Token(Token = "0x2000ECE")]
		private enum eStep
		{
			// Token: 0x040030C2 RID: 12482
			[Token(Token = "0x4005F23")]
			Idle,
			// Token: 0x040030C3 RID: 12483
			[Token(Token = "0x4005F24")]
			Selecting,
			// Token: 0x040030C4 RID: 12484
			[Token(Token = "0x4005F25")]
			ObjectInfo,
			// Token: 0x040030C5 RID: 12485
			[Token(Token = "0x4005F26")]
			MovePlaceSelect,
			// Token: 0x040030C6 RID: 12486
			[Token(Token = "0x4005F27")]
			StoreConfirm,
			// Token: 0x040030C7 RID: 12487
			[Token(Token = "0x4005F28")]
			PutPlaceSelect,
			// Token: 0x040030C8 RID: 12488
			[Token(Token = "0x4005F29")]
			PutPlaceSelectCancel,
			// Token: 0x040030C9 RID: 12489
			[Token(Token = "0x4005F2A")]
			BuyPlaceSelect,
			// Token: 0x040030CA RID: 12490
			[Token(Token = "0x4005F2B")]
			BuyStoreConfirm,
			// Token: 0x040030CB RID: 12491
			[Token(Token = "0x4005F2C")]
			BuyPlaceRequest,
			// Token: 0x040030CC RID: 12492
			[Token(Token = "0x4005F2D")]
			BuyCompleteConfirm,
			// Token: 0x040030CD RID: 12493
			[Token(Token = "0x4005F2E")]
			ObjList,
			// Token: 0x040030CE RID: 12494
			[Token(Token = "0x4005F2F")]
			BeddingBuyConfirm,
			// Token: 0x040030CF RID: 12495
			[Token(Token = "0x4005F30")]
			SellConfirm,
			// Token: 0x040030D0 RID: 12496
			[Token(Token = "0x4005F31")]
			SellRequest,
			// Token: 0x040030D1 RID: 12497
			[Token(Token = "0x4005F32")]
			SellCompleteConfirm,
			// Token: 0x040030D2 RID: 12498
			[Token(Token = "0x4005F33")]
			SendStockConfirm,
			// Token: 0x040030D3 RID: 12499
			[Token(Token = "0x4005F34")]
			SendStock,
			// Token: 0x040030D4 RID: 12500
			[Token(Token = "0x4005F35")]
			SendStockOverCancelConfirm,
			// Token: 0x040030D5 RID: 12501
			[Token(Token = "0x4005F36")]
			SendStockOverCancel,
			// Token: 0x040030D6 RID: 12502
			[Token(Token = "0x4005F37")]
			BuyCancelConfirm,
			// Token: 0x040030D7 RID: 12503
			[Token(Token = "0x4005F38")]
			BuyCancel,
			// Token: 0x040030D8 RID: 12504
			[Token(Token = "0x4005F39")]
			LimitExtend,
			// Token: 0x040030D9 RID: 12505
			[Token(Token = "0x4005F3A")]
			Schedule,
			// Token: 0x040030DA RID: 12506
			[Token(Token = "0x4005F3B")]
			StoreAllConfirm,
			// Token: 0x040030DB RID: 12507
			[Token(Token = "0x4005F3C")]
			Transit,
			// Token: 0x040030DC RID: 12508
			[Token(Token = "0x4005F3D")]
			ShopStart,
			// Token: 0x040030DD RID: 12509
			[Token(Token = "0x4005F3E")]
			ShopPrepareWait,
			// Token: 0x040030DE RID: 12510
			[Token(Token = "0x4005F3F")]
			DestroyBuilder,
			// Token: 0x040030DF RID: 12511
			[Token(Token = "0x4005F40")]
			UnloadResources,
			// Token: 0x040030E0 RID: 12512
			[Token(Token = "0x4005F41")]
			FinishedUnload
		}

		// Token: 0x0200081C RID: 2076
		[Token(Token = "0x2000ECF")]
		private enum ePlacementType
		{
			// Token: 0x040030E2 RID: 12514
			[Token(Token = "0x4005F43")]
			Invalid = -1,
			// Token: 0x040030E3 RID: 12515
			[Token(Token = "0x4005F44")]
			Buy,
			// Token: 0x040030E4 RID: 12516
			[Token(Token = "0x4005F45")]
			Stock,
			// Token: 0x040030E5 RID: 12517
			[Token(Token = "0x4005F46")]
			Move,
			// Token: 0x040030E6 RID: 12518
			[Token(Token = "0x4005F47")]
			Max
		}

		// Token: 0x0200081D RID: 2077
		[Token(Token = "0x2000ED0")]
		private class GreetData
		{
			// Token: 0x060020DE RID: 8414 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F49")]
			[Address(RVA = "0x1012E17FC", Offset = "0x12E17FC", VA = "0x1012E17FC")]
			public GreetData(long charaMngID, RoomGreet.eCategory category)
			{
			}

			// Token: 0x040030E7 RID: 12519
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005F48")]
			public long m_CharaMngID;

			// Token: 0x040030E8 RID: 12520
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005F49")]
			public RoomGreet.eCategory m_Category;
		}

		// Token: 0x0200081E RID: 2078
		[Token(Token = "0x2000ED1")]
		public class BuySetProcess
		{
			// Token: 0x060020DF RID: 8415 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F4A")]
			[Address(RVA = "0x1012D08A8", Offset = "0x12D08A8", VA = "0x1012D08A8")]
			public static void Request(eRoomObjectCategory category, int objID, int amount, Action<long[]> callback, RoomBuilder builder)
			{
			}

			// Token: 0x060020E0 RID: 8416 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F4B")]
			[Address(RVA = "0x1012E5670", Offset = "0x12E5670", VA = "0x1012E5670")]
			public void Request_BuySet(eRoomObjectCategory category, int objID, int amount, Action<long[]> callback, RoomBuilder builder)
			{
			}

			// Token: 0x060020E1 RID: 8417 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F4C")]
			[Address(RVA = "0x1012E587C", Offset = "0x12E587C", VA = "0x1012E587C")]
			private void OnResponse_BuySet(MeigewwwParam wwwParam)
			{
			}

			// Token: 0x060020E2 RID: 8418 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F4D")]
			[Address(RVA = "0x1012E5660", Offset = "0x12E5660", VA = "0x1012E5660")]
			public BuySetProcess()
			{
			}

			// Token: 0x040030E9 RID: 12521
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005F4A")]
			private int m_ProcessingAccessKey;

			// Token: 0x040030EA RID: 12522
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005F4B")]
			private Action<long[]> m_OnResponseBuySet;

			// Token: 0x040030EB RID: 12523
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005F4C")]
			private RoomBuilder m_Builder;
		}

		// Token: 0x0200081F RID: 2079
		[Token(Token = "0x2000ED2")]
		public class LimitExtendProcess
		{
			// Token: 0x060020E3 RID: 8419 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F4E")]
			[Address(RVA = "0x1012E3A8C", Offset = "0x12E3A8C", VA = "0x1012E3A8C")]
			public static void Request(int deficientNum, Action<bool> limitAddCallback)
			{
			}

			// Token: 0x060020E4 RID: 8420 RVA: 0x0000E628 File Offset: 0x0000C828
			[Token(Token = "0x6005F4F")]
			[Address(RVA = "0x1012E6694", Offset = "0x12E6694", VA = "0x1012E6694")]
			private bool RequestCom(int num)
			{
				return default(bool);
			}

			// Token: 0x060020E5 RID: 8421 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F50")]
			[Address(RVA = "0x1012E67D4", Offset = "0x12E67D4", VA = "0x1012E67D4")]
			private void OnResponseCom(MeigewwwParam wwwParam)
			{
			}

			// Token: 0x060020E6 RID: 8422 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F51")]
			[Address(RVA = "0x1012E5F08", Offset = "0x12E5F08", VA = "0x1012E5F08")]
			private void LimitExtend(int deficientNum, Action<bool> limitAddCallback)
			{
			}

			// Token: 0x060020E7 RID: 8423 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F52")]
			[Address(RVA = "0x1012E6B88", Offset = "0x12E6B88", VA = "0x1012E6B88")]
			private void OnConfirmLimitExtend(int btn)
			{
			}

			// Token: 0x060020E8 RID: 8424 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F53")]
			[Address(RVA = "0x1012E6F7C", Offset = "0x12E6F7C", VA = "0x1012E6F7C")]
			private void OnCallback_Fail(int btn)
			{
			}

			// Token: 0x060020E9 RID: 8425 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F54")]
			[Address(RVA = "0x1012E6AE0", Offset = "0x12E6AE0", VA = "0x1012E6AE0")]
			private void OnCallback_Success(MeigewwwParam wwwParam, Limitadd param)
			{
			}

			// Token: 0x060020EA RID: 8426 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F55")]
			[Address(RVA = "0x1012E6FE0", Offset = "0x12E6FE0", VA = "0x1012E6FE0")]
			private void OnCallback_GemBuy(int btn)
			{
			}

			// Token: 0x060020EB RID: 8427 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F56")]
			[Address(RVA = "0x1012E726C", Offset = "0x12E726C", VA = "0x1012E726C")]
			private void OnCallback_GemBuyEnd()
			{
			}

			// Token: 0x060020EC RID: 8428 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F57")]
			[Address(RVA = "0x1012E5F00", Offset = "0x12E5F00", VA = "0x1012E5F00")]
			public LimitExtendProcess()
			{
			}

			// Token: 0x040030EC RID: 12524
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005F4D")]
			private Action<bool> m_LimitExtendCallback;

			// Token: 0x040030ED RID: 12525
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005F4E")]
			private int m_LimitExtendNum;

			// Token: 0x040030EE RID: 12526
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005F4F")]
			private long m_UseGem;
		}
	}
}
