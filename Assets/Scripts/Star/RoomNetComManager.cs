﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020009C6 RID: 2502
	[Token(Token = "0x2000714")]
	[StructLayout(3)]
	public class RoomNetComManager : IFldNetComManager
	{
		// Token: 0x060029A9 RID: 10665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002659")]
		[Address(RVA = "0x1012E7560", Offset = "0x12E7560", VA = "0x1012E7560")]
		private void SetNextStep(RoomNetComManager.eComStep fnextup)
		{
		}

		// Token: 0x060029AA RID: 10666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600265A")]
		[Address(RVA = "0x1012E79F0", Offset = "0x12E79F0", VA = "0x1012E79F0")]
		public static void SetUpManager(GameObject ptarget, long fplayerId)
		{
		}

		// Token: 0x060029AB RID: 10667 RVA: 0x00011988 File Offset: 0x0000FB88
		[Token(Token = "0x600265B")]
		[Address(RVA = "0x1012E7BA0", Offset = "0x12E7BA0", VA = "0x1012E7BA0")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x060029AC RID: 10668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600265C")]
		[Address(RVA = "0x1012E7C08", Offset = "0x12E7C08", VA = "0x1012E7C08")]
		private void Update()
		{
		}

		// Token: 0x060029AD RID: 10669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600265D")]
		[Address(RVA = "0x1012E7C5C", Offset = "0x12E7C5C", VA = "0x1012E7C5C")]
		private void OnDestroy()
		{
		}

		// Token: 0x060029AE RID: 10670 RVA: 0x000119A0 File Offset: 0x0000FBA0
		[Token(Token = "0x600265E")]
		[Address(RVA = "0x1012E7D28", Offset = "0x12E7D28", VA = "0x1012E7D28")]
		public static bool IsCheckRoomSetUp(int fcheckkey)
		{
			return default(bool);
		}

		// Token: 0x060029AF RID: 10671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600265F")]
		[Address(RVA = "0x1012E7D2C", Offset = "0x12E7D2C", VA = "0x1012E7D2C")]
		public static void SetUpRoomDefault(int fcheckkey)
		{
		}

		// Token: 0x060029B0 RID: 10672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002660")]
		[Address(RVA = "0x1012E7D98", Offset = "0x12E7D98", VA = "0x1012E7D98")]
		public RoomNetComManager()
		{
		}

		// Token: 0x040039E4 RID: 14820
		[Token(Token = "0x40029EB")]
		private static RoomNetComManager Inst;

		// Token: 0x040039E5 RID: 14821
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40029EC")]
		private RoomNetComManager.eComStep m_Step;

		// Token: 0x040039E6 RID: 14822
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40029ED")]
		private long m_EditPlayerId;

		// Token: 0x040039E7 RID: 14823
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40029EE")]
		private bool m_FriendPlaceData;

		// Token: 0x020009C7 RID: 2503
		[Token(Token = "0x2000F82")]
		public enum eComStep
		{
			// Token: 0x040039E9 RID: 14825
			[Token(Token = "0x400636D")]
			ListIn,
			// Token: 0x040039EA RID: 14826
			[Token(Token = "0x400636E")]
			DefObjSetUp,
			// Token: 0x040039EB RID: 14827
			[Token(Token = "0x400636F")]
			DefSetUp,
			// Token: 0x040039EC RID: 14828
			[Token(Token = "0x4006370")]
			DefRoomList,
			// Token: 0x040039ED RID: 14829
			[Token(Token = "0x4006371")]
			TownObjSetUp,
			// Token: 0x040039EE RID: 14830
			[Token(Token = "0x4006372")]
			TownObjSetUpCom,
			// Token: 0x040039EF RID: 14831
			[Token(Token = "0x4006373")]
			TownBuildSetUp,
			// Token: 0x040039F0 RID: 14832
			[Token(Token = "0x4006374")]
			UserDataUp,
			// Token: 0x040039F1 RID: 14833
			[Token(Token = "0x4006375")]
			RoomPlaceGet,
			// Token: 0x040039F2 RID: 14834
			[Token(Token = "0x4006376")]
			FriendPartyGet,
			// Token: 0x040039F3 RID: 14835
			[Token(Token = "0x4006377")]
			Update
		}
	}
}
