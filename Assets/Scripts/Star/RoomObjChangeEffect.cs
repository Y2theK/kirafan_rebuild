﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A7A RID: 2682
	[Token(Token = "0x2000775")]
	[Serializable]
	[StructLayout(3)]
	public class RoomObjChangeEffect
	{
		// Token: 0x06002DF7 RID: 11767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A32")]
		[Address(RVA = "0x1012E7DA0", Offset = "0x12E7DA0", VA = "0x1012E7DA0")]
		public RoomObjChangeEffect(int sizeMinXorY, int sizeMaxXorY, EffectComponentBase effectPrefab, float scaleMinXorY, float scaleMaxXorY)
		{
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06002DF8 RID: 11768 RVA: 0x00013968 File Offset: 0x00011B68
		[Token(Token = "0x170002E1")]
		public int sizeMinXorY
		{
			[Token(Token = "0x6002A33")]
			[Address(RVA = "0x1012E7DF4", Offset = "0x12E7DF4", VA = "0x1012E7DF4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06002DF9 RID: 11769 RVA: 0x00013980 File Offset: 0x00011B80
		[Token(Token = "0x170002E2")]
		public int sizeMaxXorY
		{
			[Token(Token = "0x6002A34")]
			[Address(RVA = "0x1012E7DFC", Offset = "0x12E7DFC", VA = "0x1012E7DFC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06002DFA RID: 11770 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002E3")]
		public EffectComponentBase effectPrefab
		{
			[Token(Token = "0x6002A35")]
			[Address(RVA = "0x1012E7E04", Offset = "0x12E7E04", VA = "0x1012E7E04")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06002DFB RID: 11771 RVA: 0x00013998 File Offset: 0x00011B98
		[Token(Token = "0x170002E4")]
		public float scaleMinXorY
		{
			[Token(Token = "0x6002A36")]
			[Address(RVA = "0x1012E7E0C", Offset = "0x12E7E0C", VA = "0x1012E7E0C")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06002DFC RID: 11772 RVA: 0x000139B0 File Offset: 0x00011BB0
		[Token(Token = "0x170002E5")]
		public float scaleMaxXorY
		{
			[Token(Token = "0x6002A37")]
			[Address(RVA = "0x1012E7E14", Offset = "0x12E7E14", VA = "0x1012E7E14")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x04003DB3 RID: 15795
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002C77")]
		[SerializeField]
		private int m_SizeMinXorY;

		// Token: 0x04003DB4 RID: 15796
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002C78")]
		[SerializeField]
		private int m_SizeMaxXorY;

		// Token: 0x04003DB5 RID: 15797
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C79")]
		[SerializeField]
		private EffectComponentBase m_EffectPrefab;

		// Token: 0x04003DB6 RID: 15798
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C7A")]
		[SerializeField]
		private float m_ScaleMinXorY;

		// Token: 0x04003DB7 RID: 15799
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002C7B")]
		[SerializeField]
		private float m_ScaleMaxXorY;
	}
}
