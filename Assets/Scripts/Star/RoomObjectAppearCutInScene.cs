﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000906 RID: 2310
	[Token(Token = "0x200069D")]
	[StructLayout(3)]
	public sealed class RoomObjectAppearCutInScene : OfferCutInScene
	{
		// Token: 0x060025F5 RID: 9717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600230A")]
		[Address(RVA = "0x1012E7E1C", Offset = "0x12E7E1C", VA = "0x1012E7E1C")]
		public RoomObjectAppearCutInScene()
		{
		}

		// Token: 0x060025F6 RID: 9718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600230B")]
		[Address(RVA = "0x1012E7E84", Offset = "0x12E7E84", VA = "0x1012E7E84", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x060025F7 RID: 9719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600230C")]
		[Address(RVA = "0x1012E7F94", Offset = "0x12E7F94", VA = "0x1012E7F94", Slot = "5")]
		public override void Play()
		{
		}

		// Token: 0x060025F8 RID: 9720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600230D")]
		[Address(RVA = "0x1012E80B8", Offset = "0x12E80B8", VA = "0x1012E80B8", Slot = "6")]
		protected override void OnSkip()
		{
		}

		// Token: 0x060025F9 RID: 9721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600230E")]
		[Address(RVA = "0x1012E7EAC", Offset = "0x12E7EAC", VA = "0x1012E7EAC")]
		private void DestroySound()
		{
		}

		// Token: 0x04003632 RID: 13874
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40027A1")]
		private SoundHandler m_SoundHndl;

		// Token: 0x04003633 RID: 13875
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40027A2")]
		private int m_VoiceRequestID;
	}
}
