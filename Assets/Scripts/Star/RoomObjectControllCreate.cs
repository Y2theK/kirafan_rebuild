﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A09 RID: 2569
	[Token(Token = "0x2000732")]
	[StructLayout(3)]
	public class RoomObjectControllCreate
	{
		// Token: 0x06002B19 RID: 11033 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279A")]
		[Address(RVA = "0x1012E80E0", Offset = "0x12E80E0", VA = "0x1012E80E0")]
		public static RoomObjectModel CreateObjectModel(GameObject pobj, eRoomObjectCategory category)
		{
			return null;
		}

		// Token: 0x06002B1A RID: 11034 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279B")]
		[Address(RVA = "0x1012E81A8", Offset = "0x12E81A8", VA = "0x1012E81A8")]
		public static RoomObjectSprite CreateObjectSprite(GameObject pobj, eRoomObjectCategory category, int objID)
		{
			return null;
		}

		// Token: 0x06002B1B RID: 11035 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279C")]
		[Address(RVA = "0x1012E8204", Offset = "0x12E8204", VA = "0x1012E8204")]
		public static RoomObjectMdlSprite CreateObjectMdlSprite(GameObject pobj, eRoomObjectCategory category, int objID)
		{
			return null;
		}

		// Token: 0x06002B1C RID: 11036 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279D")]
		[Address(RVA = "0x1012E8260", Offset = "0x12E8260", VA = "0x1012E8260")]
		public static IRoomObjectControll CreateCharaObject(GameObject pobj)
		{
			return null;
		}

		// Token: 0x06002B1D RID: 11037 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279E")]
		[Address(RVA = "0x1012E82BC", Offset = "0x12E82BC", VA = "0x1012E82BC")]
		public static IRoomObjectControll SetupSimple(GameObject pobj, eRoomObjectCategory category, int objID, int fsubkey, int fsubkey2, CharacterDefine.eDir dir, int posX, int posY, int defaultSizeX, int defaultSizeY)
		{
			return null;
		}

		// Token: 0x06002B1E RID: 11038 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600279F")]
		[Address(RVA = "0x1012E8500", Offset = "0x12E8500", VA = "0x1012E8500")]
		public static IRoomObjectControll SetupMdlSimple(GameObject pobj, int fsubkey, int fsubkey2, int flayerid, int frenderlayer, int fresourceid, long fmanageid, bool finit)
		{
			return null;
		}

		// Token: 0x06002B1F RID: 11039 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A0")]
		[Address(RVA = "0x1012E899C", Offset = "0x12E899C", VA = "0x1012E899C")]
		public static IRoomObjectControll SetupHandle(GameObject pobj, RoomBuilderBase builder, eRoomObjectCategory category, int objID, CharacterDefine.eDir dir, int posX, int posY, int floorID, long fmanageID, int subkey)
		{
			return null;
		}

		// Token: 0x06002B20 RID: 11040 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A1")]
		[Address(RVA = "0x1012E8DB8", Offset = "0x12E8DB8", VA = "0x1012E8DB8")]
		public static GameObject CreateBGObject()
		{
			return null;
		}

		// Token: 0x06002B21 RID: 11041 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A2")]
		[Address(RVA = "0x1012E8F18", Offset = "0x12E8F18", VA = "0x1012E8F18")]
		public static GameObject CreateBGObject2()
		{
			return null;
		}

		// Token: 0x06002B22 RID: 11042 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A3")]
		[Address(RVA = "0x1012E90BC", Offset = "0x12E90BC", VA = "0x1012E90BC")]
		public static GameObject CreateFloorObject()
		{
			return null;
		}

		// Token: 0x06002B23 RID: 11043 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A4")]
		[Address(RVA = "0x1012E9260", Offset = "0x12E9260", VA = "0x1012E9260")]
		public static GameObject CreateWallObject()
		{
			return null;
		}

		// Token: 0x06002B24 RID: 11044 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027A5")]
		[Address(RVA = "0x1012E9404", Offset = "0x12E9404", VA = "0x1012E9404")]
		public static GameObject CreateRoomObject()
		{
			return null;
		}

		// Token: 0x06002B25 RID: 11045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027A6")]
		[Address(RVA = "0x1012E9534", Offset = "0x12E9534", VA = "0x1012E9534")]
		public RoomObjectControllCreate()
		{
		}
	}
}
