﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A0A RID: 2570
	[Token(Token = "0x2000733")]
	[StructLayout(3)]
	public class RoomObjectCtrlBase : RoomObjectModel
	{
		// Token: 0x06002B26 RID: 11046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027A7")]
		[Address(RVA = "0x1012E953C", Offset = "0x12E953C", VA = "0x1012E953C", Slot = "7")]
		public override void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06002B27 RID: 11047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027A8")]
		[Address(RVA = "0x1012E97E4", Offset = "0x12E97E4", VA = "0x1012E97E4", Slot = "40")]
		public override void SetUpAnimation()
		{
		}

		// Token: 0x06002B28 RID: 11048 RVA: 0x00012588 File Offset: 0x00010788
		[Token(Token = "0x60027A9")]
		[Address(RVA = "0x1012E980C", Offset = "0x12E980C", VA = "0x1012E980C", Slot = "8")]
		public override bool IsAction()
		{
			return default(bool);
		}

		// Token: 0x06002B29 RID: 11049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027AA")]
		[Address(RVA = "0x1012E9814", Offset = "0x12E9814", VA = "0x1012E9814", Slot = "10")]
		public override void SetAttachLink(bool flock)
		{
		}

		// Token: 0x06002B2A RID: 11050 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027AB")]
		[Address(RVA = "0x1012E981C", Offset = "0x12E981C", VA = "0x1012E981C", Slot = "11")]
		public override Transform GetAttachTransform(Vector2 flinkpos)
		{
			return null;
		}

		// Token: 0x06002B2B RID: 11051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027AC")]
		[Address(RVA = "0x1012E9824", Offset = "0x12E9824", VA = "0x1012E9824", Slot = "14")]
		public override void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002B2C RID: 11052 RVA: 0x000125A0 File Offset: 0x000107A0
		[Token(Token = "0x60027AD")]
		[Address(RVA = "0x1012E9928", Offset = "0x12E9928", VA = "0x1012E9928", Slot = "15")]
		public override bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002B2D RID: 11053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027AE")]
		[Address(RVA = "0x1012E9930", Offset = "0x12E9930", VA = "0x1012E9930", Slot = "13")]
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06002B2E RID: 11054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027AF")]
		[Address(RVA = "0x1012E9A90", Offset = "0x12E9A90", VA = "0x1012E9A90", Slot = "16")]
		public override void SetActionSendObj(IRoomObjectControll plink)
		{
		}

		// Token: 0x06002B2F RID: 11055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027B0")]
		[Address(RVA = "0x1012E9A98", Offset = "0x12E9A98", VA = "0x1012E9A98", Slot = "17")]
		public override void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06002B30 RID: 11056 RVA: 0x000125B8 File Offset: 0x000107B8
		[Token(Token = "0x60027B1")]
		[Address(RVA = "0x1012E9BA0", Offset = "0x12E9BA0", VA = "0x1012E9BA0", Slot = "32")]
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return default(bool);
		}

		// Token: 0x06002B31 RID: 11057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027B2")]
		[Address(RVA = "0x1012E9D00", Offset = "0x12E9D00", VA = "0x1012E9D00", Slot = "36")]
		public override void AbortLinkEventObj()
		{
		}

		// Token: 0x06002B32 RID: 11058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027B3")]
		[Address(RVA = "0x1012E9E54", Offset = "0x12E9E54", VA = "0x1012E9E54")]
		public RoomObjectCtrlBase()
		{
		}

		// Token: 0x04003B14 RID: 15124
		[Cpp2IlInjected.FieldOffset(Offset = "0x129")]
		[Token(Token = "0x4002A87")]
		private bool m_ActionWait;

		// Token: 0x04003B15 RID: 15125
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002A88")]
		private IRoomObjectControll m_LinkObj;

		// Token: 0x04003B16 RID: 15126
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4002A89")]
		private IRoomObjectControll m_SubLinkObj;

		// Token: 0x04003B17 RID: 15127
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4002A8A")]
		private Transform m_LocatorTrs;

		// Token: 0x04003B18 RID: 15128
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4002A8B")]
		private bool m_TouchStepUp;

		// Token: 0x04003B19 RID: 15129
		[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
		[Token(Token = "0x4002A8C")]
		private float m_TouchTime;
	}
}
