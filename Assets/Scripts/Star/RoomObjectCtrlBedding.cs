﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A0B RID: 2571
	[Token(Token = "0x2000734")]
	[StructLayout(3)]
	public class RoomObjectCtrlBedding : RoomObjectModel
	{
		// Token: 0x06002B33 RID: 11059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027B4")]
		[Address(RVA = "0x1012E9EB0", Offset = "0x12E9EB0", VA = "0x1012E9EB0", Slot = "7")]
		public override void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06002B34 RID: 11060 RVA: 0x000125D0 File Offset: 0x000107D0
		[Token(Token = "0x60027B5")]
		[Address(RVA = "0x1012EA11C", Offset = "0x12EA11C", VA = "0x1012EA11C", Slot = "12")]
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			return default(bool);
		}

		// Token: 0x06002B35 RID: 11061 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027B6")]
		[Address(RVA = "0x1012EA7D4", Offset = "0x12EA7D4", VA = "0x1012EA7D4", Slot = "11")]
		public override Transform GetAttachTransform(Vector2 fpos)
		{
			return null;
		}

		// Token: 0x06002B36 RID: 11062 RVA: 0x000125E8 File Offset: 0x000107E8
		[Token(Token = "0x60027B7")]
		[Address(RVA = "0x1012EA610", Offset = "0x12EA610", VA = "0x1012EA610")]
		private int CalcAttachIndex(float fgridx, float fgridy)
		{
			return 0;
		}

		// Token: 0x06002B37 RID: 11063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027B8")]
		[Address(RVA = "0x1012EA900", Offset = "0x12EA900", VA = "0x1012EA900", Slot = "9")]
		public override void LinkKeyLock(bool flock)
		{
		}

		// Token: 0x06002B38 RID: 11064 RVA: 0x00012600 File Offset: 0x00010800
		[Token(Token = "0x60027B9")]
		[Address(RVA = "0x1012EA91C", Offset = "0x12EA91C", VA = "0x1012EA91C", Slot = "8")]
		public override bool IsAction()
		{
			return default(bool);
		}

		// Token: 0x06002B39 RID: 11065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027BA")]
		[Address(RVA = "0x1012EA944", Offset = "0x12EA944", VA = "0x1012EA944", Slot = "14")]
		public override void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002B3A RID: 11066 RVA: 0x00012618 File Offset: 0x00010818
		[Token(Token = "0x60027BB")]
		[Address(RVA = "0x1012EAAF4", Offset = "0x12EAAF4", VA = "0x1012EAAF4", Slot = "15")]
		public override bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002B3B RID: 11067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027BC")]
		[Address(RVA = "0x1012EAAFC", Offset = "0x12EAAFC", VA = "0x1012EAAFC", Slot = "13")]
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06002B3C RID: 11068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027BD")]
		[Address(RVA = "0x1012EACE8", Offset = "0x12EACE8", VA = "0x1012EACE8", Slot = "17")]
		public override void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06002B3D RID: 11069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027BE")]
		[Address(RVA = "0x1012EAEA4", Offset = "0x12EAEA4", VA = "0x1012EAEA4", Slot = "22")]
		public override void Destroy()
		{
		}

		// Token: 0x06002B3E RID: 11070 RVA: 0x00012630 File Offset: 0x00010830
		[Token(Token = "0x60027BF")]
		[Address(RVA = "0x1012EB0F0", Offset = "0x12EB0F0", VA = "0x1012EB0F0", Slot = "32")]
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return default(bool);
		}

		// Token: 0x06002B3F RID: 11071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027C0")]
		[Address(RVA = "0x1012EB340", Offset = "0x12EB340", VA = "0x1012EB340", Slot = "36")]
		public override void AbortLinkEventObj()
		{
		}

		// Token: 0x06002B40 RID: 11072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027C1")]
		[Address(RVA = "0x1012EB4CC", Offset = "0x12EB4CC", VA = "0x1012EB4CC")]
		public RoomObjectCtrlBedding()
		{
		}

		// Token: 0x04003B1A RID: 15130
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4002A8D")]
		private int m_ActionWait;

		// Token: 0x04003B1B RID: 15131
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002A8E")]
		private int m_EntryLocater;

		// Token: 0x04003B1C RID: 15132
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4002A8F")]
		private int m_EntryLocaterMax;

		// Token: 0x04003B1D RID: 15133
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4002A90")]
		private int m_LockCnt;

		// Token: 0x04003B1E RID: 15134
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4002A91")]
		private RoomObjectCtrlBedding.TLocaterInfo[] m_Locator;

		// Token: 0x02000A0C RID: 2572
		[Token(Token = "0x2000FA5")]
		public struct TLocaterInfo
		{
			// Token: 0x04003B1F RID: 15135
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40063FE")]
			public Transform m_Trs;

			// Token: 0x04003B20 RID: 15136
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40063FF")]
			public IRoomObjectControll m_Link;

			// Token: 0x04003B21 RID: 15137
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006400")]
			public Vector2 m_GridPos;
		}
	}
}
