﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A0D RID: 2573
	[Token(Token = "0x2000735")]
	[StructLayout(3)]
	public class RoomObjectCtrlChair : RoomObjectModel
	{
		// Token: 0x06002B41 RID: 11073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027C2")]
		[Address(RVA = "0x1012EB4D0", Offset = "0x12EB4D0", VA = "0x1012EB4D0", Slot = "7")]
		public override void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06002B42 RID: 11074 RVA: 0x00012648 File Offset: 0x00010848
		[Token(Token = "0x60027C3")]
		[Address(RVA = "0x1012EC00C", Offset = "0x12EC00C", VA = "0x1012EC00C", Slot = "12")]
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			return default(bool);
		}

		// Token: 0x06002B43 RID: 11075 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027C4")]
		[Address(RVA = "0x1012EC3B0", Offset = "0x12EC3B0", VA = "0x1012EC3B0", Slot = "11")]
		public override Transform GetAttachTransform(Vector2 fpos)
		{
			return null;
		}

		// Token: 0x06002B44 RID: 11076 RVA: 0x00012660 File Offset: 0x00010860
		[Token(Token = "0x60027C5")]
		[Address(RVA = "0x1012EC208", Offset = "0x12EC208", VA = "0x1012EC208")]
		private int CalcAttachIndex(out int fsubindex, float fgridx, float fgridy)
		{
			return 0;
		}

		// Token: 0x06002B45 RID: 11077 RVA: 0x00012678 File Offset: 0x00010878
		[Token(Token = "0x60027C6")]
		[Address(RVA = "0x1012EC4B4", Offset = "0x12EC4B4", VA = "0x1012EC4B4", Slot = "8")]
		public override bool IsAction()
		{
			return default(bool);
		}

		// Token: 0x06002B46 RID: 11078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027C7")]
		[Address(RVA = "0x1012EC4C8", Offset = "0x12EC4C8", VA = "0x1012EC4C8", Slot = "14")]
		public override void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002B47 RID: 11079 RVA: 0x00012690 File Offset: 0x00010890
		[Token(Token = "0x60027C8")]
		[Address(RVA = "0x1012EC6A0", Offset = "0x12EC6A0", VA = "0x1012EC6A0", Slot = "15")]
		public override bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002B48 RID: 11080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027C9")]
		[Address(RVA = "0x1012EC6A8", Offset = "0x12EC6A8", VA = "0x1012EC6A8", Slot = "13")]
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06002B49 RID: 11081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027CA")]
		[Address(RVA = "0x1012ECA00", Offset = "0x12ECA00", VA = "0x1012ECA00", Slot = "17")]
		public override void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06002B4A RID: 11082 RVA: 0x000126A8 File Offset: 0x000108A8
		[Token(Token = "0x60027CB")]
		[Address(RVA = "0x1012ECBF0", Offset = "0x12ECBF0", VA = "0x1012ECBF0", Slot = "32")]
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return default(bool);
		}

		// Token: 0x06002B4B RID: 11083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027CC")]
		[Address(RVA = "0x1012ECD50", Offset = "0x12ECD50", VA = "0x1012ECD50", Slot = "36")]
		public override void AbortLinkEventObj()
		{
		}

		// Token: 0x06002B4C RID: 11084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027CD")]
		[Address(RVA = "0x1012ECEFC", Offset = "0x12ECEFC", VA = "0x1012ECEFC")]
		public RoomObjectCtrlChair()
		{
		}

		// Token: 0x04003B22 RID: 15138
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4002A92")]
		private int m_ActionWait;

		// Token: 0x04003B23 RID: 15139
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002A93")]
		private int m_EntryLocater;

		// Token: 0x04003B24 RID: 15140
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4002A94")]
		private int m_EntryLocaterMax;

		// Token: 0x04003B25 RID: 15141
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4002A95")]
		private RoomObjectCtrlChair.LocaterInfoBase[] m_Locator;

		// Token: 0x04003B26 RID: 15142
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4002A96")]
		private bool m_TouchStepUp;

		// Token: 0x04003B27 RID: 15143
		[Cpp2IlInjected.FieldOffset(Offset = "0x144")]
		[Token(Token = "0x4002A97")]
		private float m_TouchTime;

		// Token: 0x02000A0E RID: 2574
		[Token(Token = "0x2000FA6")]
		public class AttachInfo
		{
			// Token: 0x06002B4D RID: 11085 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006039")]
			[Address(RVA = "0x1012ECF00", Offset = "0x12ECF00", VA = "0x1012ECF00")]
			public AttachInfo()
			{
			}

			// Token: 0x04003B28 RID: 15144
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006401")]
			public int m_SubIndex;
		}

		// Token: 0x02000A0F RID: 2575
		[Token(Token = "0x2000FA7")]
		public class LocaterInfoBase
		{
			// Token: 0x06002B4E RID: 11086 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600603A")]
			[Address(RVA = "0x1012ECF08", Offset = "0x12ECF08", VA = "0x1012ECF08", Slot = "4")]
			public virtual Transform GetTrs(int fsubindex)
			{
				return null;
			}

			// Token: 0x06002B4F RID: 11087 RVA: 0x000126C0 File Offset: 0x000108C0
			[Token(Token = "0x600603B")]
			[Address(RVA = "0x1012ECF10", Offset = "0x12ECF10", VA = "0x1012ECF10", Slot = "5")]
			public virtual Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				return default(Vector2);
			}

			// Token: 0x06002B50 RID: 11088 RVA: 0x000126D8 File Offset: 0x000108D8
			[Token(Token = "0x600603C")]
			[Address(RVA = "0x1012ECF78", Offset = "0x12ECF78", VA = "0x1012ECF78", Slot = "6")]
			public virtual bool IsBind()
			{
				return default(bool);
			}

			// Token: 0x06002B51 RID: 11089 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600603D")]
			[Address(RVA = "0x1012ECF80", Offset = "0x12ECF80", VA = "0x1012ECF80")]
			public LocaterInfoBase()
			{
			}

			// Token: 0x04003B29 RID: 15145
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006402")]
			public IRoomObjectControll m_Link;
		}

		// Token: 0x02000A10 RID: 2576
		[Token(Token = "0x2000FA8")]
		public class LocaterInfoSingle : RoomObjectCtrlChair.LocaterInfoBase
		{
			// Token: 0x06002B52 RID: 11090 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600603E")]
			[Address(RVA = "0x1012ED194", Offset = "0x12ED194", VA = "0x1012ED194", Slot = "4")]
			public override Transform GetTrs(int fsubindex)
			{
				return null;
			}

			// Token: 0x06002B53 RID: 11091 RVA: 0x000126F0 File Offset: 0x000108F0
			[Token(Token = "0x600603F")]
			[Address(RVA = "0x1012ED19C", Offset = "0x12ED19C", VA = "0x1012ED19C", Slot = "5")]
			public override Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				return default(Vector2);
			}

			// Token: 0x06002B54 RID: 11092 RVA: 0x00012708 File Offset: 0x00010908
			[Token(Token = "0x6006040")]
			[Address(RVA = "0x1012ED1A8", Offset = "0x12ED1A8", VA = "0x1012ED1A8", Slot = "6")]
			public override bool IsBind()
			{
				return default(bool);
			}

			// Token: 0x06002B55 RID: 11093 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006041")]
			[Address(RVA = "0x1012EBDD4", Offset = "0x12EBDD4", VA = "0x1012EBDD4")]
			public void SetUpLoc(Transform ptrs)
			{
			}

			// Token: 0x06002B56 RID: 11094 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006042")]
			[Address(RVA = "0x1012EBDCC", Offset = "0x12EBDCC", VA = "0x1012EBDCC")]
			public LocaterInfoSingle()
			{
			}

			// Token: 0x04003B2A RID: 15146
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006403")]
			public Transform m_Trs;

			// Token: 0x04003B2B RID: 15147
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006404")]
			public Vector2 m_GridPos;
		}

		// Token: 0x02000A11 RID: 2577
		[Token(Token = "0x2000FA9")]
		public class LocaterInfoMulti : RoomObjectCtrlChair.LocaterInfoBase
		{
			// Token: 0x06002B57 RID: 11095 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006043")]
			[Address(RVA = "0x1012ECF88", Offset = "0x12ECF88", VA = "0x1012ECF88", Slot = "4")]
			public override Transform GetTrs(int fsubindex)
			{
				return null;
			}

			// Token: 0x06002B58 RID: 11096 RVA: 0x00012720 File Offset: 0x00010920
			[Token(Token = "0x6006044")]
			[Address(RVA = "0x1012ECFD8", Offset = "0x12ECFD8", VA = "0x1012ECFD8", Slot = "5")]
			public override Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				return default(Vector2);
			}

			// Token: 0x06002B59 RID: 11097 RVA: 0x00012738 File Offset: 0x00010938
			[Token(Token = "0x6006045")]
			[Address(RVA = "0x1012ED184", Offset = "0x12ED184", VA = "0x1012ED184", Slot = "6")]
			public override bool IsBind()
			{
				return default(bool);
			}

			// Token: 0x06002B5A RID: 11098 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006046")]
			[Address(RVA = "0x1012EBE24", Offset = "0x12EBE24", VA = "0x1012EBE24")]
			public void SetUpRoc(Transform pbase)
			{
			}

			// Token: 0x06002B5B RID: 11099 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006047")]
			[Address(RVA = "0x1012EBE1C", Offset = "0x12EBE1C", VA = "0x1012EBE1C")]
			public LocaterInfoMulti()
			{
			}

			// Token: 0x04003B2C RID: 15148
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006405")]
			public Transform[] m_Trs;

			// Token: 0x04003B2D RID: 15149
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006406")]
			public Vector2[] m_GridPos;

			// Token: 0x04003B2E RID: 15150
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006407")]
			public int m_DataNum;
		}
	}
}
