﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A12 RID: 2578
	[Token(Token = "0x2000736")]
	[StructLayout(3)]
	public class RoomObjectCtrlChara : IRoomObjectControll
	{
		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06002B5D RID: 11101 RVA: 0x00012750 File Offset: 0x00010950
		// (set) Token: 0x06002B5C RID: 11100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002B8")]
		public bool isPickable
		{
			[Token(Token = "0x60027CF")]
			[Address(RVA = "0x1012ED220", Offset = "0x12ED220", VA = "0x1012ED220")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60027CE")]
			[Address(RVA = "0x1012ED218", Offset = "0x12ED218", VA = "0x1012ED218")]
			set
			{
			}
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06002B5F RID: 11103 RVA: 0x00012768 File Offset: 0x00010968
		// (set) Token: 0x06002B5E RID: 11102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002B9")]
		public bool isEventSealed
		{
			[Token(Token = "0x60027D1")]
			[Address(RVA = "0x1012ED230", Offset = "0x12ED230", VA = "0x1012ED230")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60027D0")]
			[Address(RVA = "0x1012ED228", Offset = "0x12ED228", VA = "0x1012ED228")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06002B61 RID: 11105 RVA: 0x00012780 File Offset: 0x00010980
		// (set) Token: 0x06002B60 RID: 11104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170002BA")]
		public bool reflectMoveFlg
		{
			[Token(Token = "0x60027D3")]
			[Address(RVA = "0x1012ED240", Offset = "0x12ED240", VA = "0x1012ED240")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60027D2")]
			[Address(RVA = "0x1012ED238", Offset = "0x12ED238", VA = "0x1012ED238")]
			set
			{
			}
		}

		// Token: 0x06002B62 RID: 11106 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027D4")]
		[Address(RVA = "0x1012ED248", Offset = "0x12ED248", VA = "0x1012ED248")]
		public CharacterHandler GetHandle()
		{
			return null;
		}

		// Token: 0x06002B63 RID: 11107 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027D5")]
		[Address(RVA = "0x1012ED250", Offset = "0x12ED250", VA = "0x1012ED250")]
		public RoomAnimAccessMap GetAnimeMap()
		{
			return null;
		}

		// Token: 0x06002B64 RID: 11108 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027D6")]
		[Address(RVA = "0x1012ED258", Offset = "0x12ED258", VA = "0x1012ED258")]
		public RoomCharaHud GetUIHud()
		{
			return null;
		}

		// Token: 0x06002B65 RID: 11109 RVA: 0x00012798 File Offset: 0x00010998
		[Token(Token = "0x60027D7")]
		[Address(RVA = "0x1012ED260", Offset = "0x12ED260", VA = "0x1012ED260")]
		public Vector2 GetGridPos()
		{
			return default(Vector2);
		}

		// Token: 0x06002B66 RID: 11110 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027D8")]
		[Address(RVA = "0x1012ED290", Offset = "0x12ED290", VA = "0x1012ED290")]
		public AStar GetAStar()
		{
			return null;
		}

		// Token: 0x06002B67 RID: 11111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027D9")]
		[Address(RVA = "0x1012ED298", Offset = "0x12ED298", VA = "0x1012ED298")]
		public void SetOffsetZ(float foffset)
		{
		}

		// Token: 0x06002B68 RID: 11112 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60027DA")]
		[Address(RVA = "0x1012ED2A0", Offset = "0x12ED2A0", VA = "0x1012ED2A0", Slot = "37")]
		public override IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			return null;
		}

		// Token: 0x06002B69 RID: 11113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027DB")]
		[Address(RVA = "0x1012ED394", Offset = "0x12ED394", VA = "0x1012ED394", Slot = "38")]
		public override void SetSortKey(string ptargetname, int fsortkey, int fsortkeyOffset = 0)
		{
		}

		// Token: 0x06002B6A RID: 11114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027DC")]
		[Address(RVA = "0x1012ED3A0", Offset = "0x12ED3A0", VA = "0x1012ED3A0", Slot = "39")]
		public override void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
		}

		// Token: 0x06002B6B RID: 11115 RVA: 0x000127B0 File Offset: 0x000109B0
		[Token(Token = "0x60027DD")]
		[Address(RVA = "0x1012ED44C", Offset = "0x12ED44C", VA = "0x1012ED44C")]
		public int GetEntryPoint()
		{
			return 0;
		}

		// Token: 0x06002B6C RID: 11116 RVA: 0x000127C8 File Offset: 0x000109C8
		[Token(Token = "0x60027DE")]
		[Address(RVA = "0x1012ED510", Offset = "0x12ED510", VA = "0x1012ED510")]
		public int GetEntryPointInRoom()
		{
			return 0;
		}

		// Token: 0x06002B6D RID: 11117 RVA: 0x000127E0 File Offset: 0x000109E0
		[Token(Token = "0x60027DF")]
		[Address(RVA = "0x1012ED608", Offset = "0x12ED608", VA = "0x1012ED608")]
		public float GetRoomsCharaScale()
		{
			return 0f;
		}

		// Token: 0x06002B6E RID: 11118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E0")]
		[Address(RVA = "0x1012ED6B8", Offset = "0x12ED6B8", VA = "0x1012ED6B8")]
		public void LinkManager(RoomBuilderBase builder, IRoomCharaManager pmanager)
		{
		}

		// Token: 0x06002B6F RID: 11119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E1")]
		[Address(RVA = "0x1012ED6C4", Offset = "0x12ED6C4", VA = "0x1012ED6C4", Slot = "7")]
		public override void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06002B70 RID: 11120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E2")]
		[Address(RVA = "0x1012EE980", Offset = "0x12EE980", VA = "0x1012EE980")]
		public void Initialize(int currentGridX, int currentGridY)
		{
		}

		// Token: 0x06002B71 RID: 11121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E3")]
		[Address(RVA = "0x1012EEACC", Offset = "0x12EEACC", VA = "0x1012EEACC")]
		public void SetCurrentPos(Vector3 pos, int currentGridX, int currentGridY)
		{
		}

		// Token: 0x06002B72 RID: 11122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E4")]
		[Address(RVA = "0x1012EEB74", Offset = "0x12EEB74", VA = "0x1012EEB74")]
		public void SetNewGridStatus(RoomGridState pGridState)
		{
		}

		// Token: 0x06002B73 RID: 11123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E5")]
		[Address(RVA = "0x1012EEBE0", Offset = "0x12EEBE0", VA = "0x1012EEBE0", Slot = "6")]
		public override void ReleaseObj()
		{
		}

		// Token: 0x06002B74 RID: 11124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E6")]
		[Address(RVA = "0x1012EED4C", Offset = "0x12EED4C", VA = "0x1012EED4C")]
		private void LateUpdate()
		{
		}

		// Token: 0x06002B75 RID: 11125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E7")]
		[Address(RVA = "0x1012EF32C", Offset = "0x12EF32C", VA = "0x1012EF32C", Slot = "20")]
		public override void UpdateObj()
		{
		}

		// Token: 0x06002B76 RID: 11126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E8")]
		[Address(RVA = "0x1012EFD60", Offset = "0x12EFD60", VA = "0x1012EFD60", Slot = "5")]
		protected override void DonePreparing()
		{
		}

		// Token: 0x06002B77 RID: 11127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027E9")]
		[Address(RVA = "0x1012EDBEC", Offset = "0x12EDBEC", VA = "0x1012EDBEC")]
		private void ShadowModelUp(MeigeResource.Handler phandle)
		{
		}

		// Token: 0x06002B78 RID: 11128 RVA: 0x000127F8 File Offset: 0x000109F8
		[Token(Token = "0x60027EA")]
		[Address(RVA = "0x1012F015C", Offset = "0x12F015C", VA = "0x1012F015C", Slot = "8")]
		public override bool IsAction()
		{
			return default(bool);
		}

		// Token: 0x06002B79 RID: 11129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027EB")]
		[Address(RVA = "0x1012F0164", Offset = "0x12F0164", VA = "0x1012F0164", Slot = "14")]
		public override void ActivateLinkObject(bool flg)
		{
		}

		// Token: 0x06002B7A RID: 11130 RVA: 0x00012810 File Offset: 0x00010A10
		[Token(Token = "0x60027EC")]
		[Address(RVA = "0x1012F0268", Offset = "0x12F0268", VA = "0x1012F0268", Slot = "15")]
		public override bool IsActiveLinkObject()
		{
			return default(bool);
		}

		// Token: 0x06002B7B RID: 11131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027ED")]
		[Address(RVA = "0x1012F0270", Offset = "0x12F0270", VA = "0x1012F0270", Slot = "13")]
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06002B7C RID: 11132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027EE")]
		[Address(RVA = "0x1012F03E0", Offset = "0x12F03E0", VA = "0x1012F03E0")]
		public void SortingOrderChara(float flayerpos, int sortingOrder, bool isLinked)
		{
		}

		// Token: 0x06002B7D RID: 11133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027EF")]
		[Address(RVA = "0x1012F05D8", Offset = "0x12F05D8", VA = "0x1012F05D8", Slot = "17")]
		public override void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06002B7E RID: 11134 RVA: 0x00012828 File Offset: 0x00010A28
		[Token(Token = "0x60027F0")]
		[Address(RVA = "0x1012F06D8", Offset = "0x12F06D8", VA = "0x1012F06D8")]
		public bool IsWakeUpAction()
		{
			return default(bool);
		}

		// Token: 0x06002B7F RID: 11135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F1")]
		[Address(RVA = "0x1012EB338", Offset = "0x12EB338", VA = "0x1012EB338")]
		public void SetWakeUpAction(bool fset)
		{
		}

		// Token: 0x06002B80 RID: 11136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F2")]
		[Address(RVA = "0x1012F06E0", Offset = "0x12F06E0", VA = "0x1012F06E0")]
		public void ChangeFloor(int floorID)
		{
		}

		// Token: 0x06002B81 RID: 11137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F3")]
		[Address(RVA = "0x1012F0868", Offset = "0x12F0868", VA = "0x1012F0868")]
		public void AttachHud(RoomCharaHud hud)
		{
		}

		// Token: 0x06002B82 RID: 11138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F4")]
		[Address(RVA = "0x1012F0998", Offset = "0x12F0998", VA = "0x1012F0998")]
		private void UpCharaSetDir()
		{
		}

		// Token: 0x06002B83 RID: 11139 RVA: 0x00012840 File Offset: 0x00010A40
		[Token(Token = "0x60027F5")]
		[Address(RVA = "0x1012F0BAC", Offset = "0x12F0BAC", VA = "0x1012F0BAC")]
		public bool IsPlayingAnim(string actionKey)
		{
			return default(bool);
		}

		// Token: 0x06002B84 RID: 11140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F6")]
		[Address(RVA = "0x1012F0D64", Offset = "0x12F0D64", VA = "0x1012F0D64")]
		public void SetFace(int faceno)
		{
		}

		// Token: 0x06002B85 RID: 11141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F7")]
		[Address(RVA = "0x1012F0DB0", Offset = "0x12F0DB0", VA = "0x1012F0DB0")]
		public void PlayMotion(RoomDefine.eAnim ftype, WrapMode fmode)
		{
		}

		// Token: 0x06002B86 RID: 11142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F8")]
		[Address(RVA = "0x1012F0F14", Offset = "0x12F0F14", VA = "0x1012F0F14")]
		public void PlayMotion(RoomDefine.eAnim ftype, WrapMode fmode, int stepNo)
		{
		}

		// Token: 0x06002B87 RID: 11143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027F9")]
		[Address(RVA = "0x1012F0C38", Offset = "0x12F0C38", VA = "0x1012F0C38")]
		public void PlayMotion(string clipName, WrapMode fmode)
		{
		}

		// Token: 0x06002B88 RID: 11144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027FA")]
		[Address(RVA = "0x1012F11CC", Offset = "0x12F11CC", VA = "0x1012F11CC")]
		public void Pause()
		{
		}

		// Token: 0x06002B89 RID: 11145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60027FB")]
		[Address(RVA = "0x1012F1284", Offset = "0x12F1284", VA = "0x1012F1284")]
		public void FrameReset()
		{
		}

		// Token: 0x06002B8A RID: 11146 RVA: 0x00012858 File Offset: 0x00010A58
		[Token(Token = "0x60027FC")]
		[Address(RVA = "0x1012F1080", Offset = "0x12F1080", VA = "0x1012F1080")]
		private RoomDefine.eAnim GetCommonAnimeID(string clipName)
		{
			return RoomDefine.eAnim.Idle;
		}

		// Token: 0x06002B8B RID: 11147 RVA: 0x00012870 File Offset: 0x00010A70
		[Token(Token = "0x60027FD")]
		[Address(RVA = "0x1012F1344", Offset = "0x12F1344", VA = "0x1012F1344")]
		public float GetMaxSec(string clipName)
		{
			return 0f;
		}

		// Token: 0x06002B8C RID: 11148 RVA: 0x00012888 File Offset: 0x00010A88
		[Token(Token = "0x60027FE")]
		[Address(RVA = "0x1012F1430", Offset = "0x12F1430", VA = "0x1012F1430")]
		public float GetPlayingSec()
		{
			return 0f;
		}

		// Token: 0x06002B8D RID: 11149 RVA: 0x000128A0 File Offset: 0x00010AA0
		[Token(Token = "0x60027FF")]
		[Address(RVA = "0x1012F14D0", Offset = "0x12F14D0", VA = "0x1012F14D0")]
		public float GetPlayingSec(string clipName)
		{
			return 0f;
		}

		// Token: 0x06002B8E RID: 11150 RVA: 0x000128B8 File Offset: 0x00010AB8
		[Token(Token = "0x6002800")]
		[Address(RVA = "0x1012F15BC", Offset = "0x12F15BC", VA = "0x1012F15BC")]
		public int GetPlayingFrame(string clipName)
		{
			return 0;
		}

		// Token: 0x06002B8F RID: 11151 RVA: 0x000128D0 File Offset: 0x00010AD0
		[Token(Token = "0x6002801")]
		[Address(RVA = "0x1012F16A4", Offset = "0x12F16A4", VA = "0x1012F16A4")]
		public bool LoadActionAnim(string key)
		{
			return default(bool);
		}

		// Token: 0x06002B90 RID: 11152 RVA: 0x000128E8 File Offset: 0x00010AE8
		[Token(Token = "0x6002802")]
		[Address(RVA = "0x1012F1788", Offset = "0x12F1788", VA = "0x1012F1788")]
		public bool IsLoadingActionAnim()
		{
			return default(bool);
		}

		// Token: 0x06002B91 RID: 11153 RVA: 0x00012900 File Offset: 0x00010B00
		[Token(Token = "0x6002803")]
		[Address(RVA = "0x1012F1828", Offset = "0x12F1828", VA = "0x1012F1828")]
		public bool IsLoadingActionAnim(string key)
		{
			return default(bool);
		}

		// Token: 0x06002B92 RID: 11154 RVA: 0x00012918 File Offset: 0x00010B18
		[Token(Token = "0x6002804")]
		[Address(RVA = "0x1012F18DC", Offset = "0x12F18DC", VA = "0x1012F18DC")]
		public bool IsLoadedActionAnim(string key)
		{
			return default(bool);
		}

		// Token: 0x06002B93 RID: 11155 RVA: 0x00012930 File Offset: 0x00010B30
		[Token(Token = "0x6002805")]
		[Address(RVA = "0x1012F1990", Offset = "0x12F1990", VA = "0x1012F1990")]
		public bool IsPlayingMotion(RoomDefine.eAnim ftype)
		{
			return default(bool);
		}

		// Token: 0x06002B94 RID: 11156 RVA: 0x00012948 File Offset: 0x00010B48
		[Token(Token = "0x6002806")]
		[Address(RVA = "0x1012F19FC", Offset = "0x12F19FC", VA = "0x1012F19FC")]
		public bool IsPlayingMotion(RoomDefine.eAnim ftype, int stepNo)
		{
			return default(bool);
		}

		// Token: 0x06002B95 RID: 11157 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002807")]
		[Address(RVA = "0x1012F1A78", Offset = "0x12F1A78", VA = "0x1012F1A78", Slot = "31")]
		public override string GetPlayingAnim()
		{
			return null;
		}

		// Token: 0x06002B96 RID: 11158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002808")]
		[Address(RVA = "0x1012F1A90", Offset = "0x12F1A90", VA = "0x1012F1A90")]
		public void SetBlink(bool fblink)
		{
		}

		// Token: 0x06002B97 RID: 11159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002809")]
		[Address(RVA = "0x1012F1AC8", Offset = "0x12F1AC8", VA = "0x1012F1AC8")]
		public void SetObjectLinkCut()
		{
		}

		// Token: 0x06002B98 RID: 11160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600280A")]
		[Address(RVA = "0x1012F1ACC", Offset = "0x12F1ACC", VA = "0x1012F1ACC")]
		public void ReleaseEntry()
		{
		}

		// Token: 0x06002B99 RID: 11161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600280B")]
		[Address(RVA = "0x1012EDF44", Offset = "0x12EDF44", VA = "0x1012EDF44")]
		public void SetCharaCollider(GameObject pobj, float fradius, float fhi, float foffset, int fsel, bool frot)
		{
		}

		// Token: 0x06002B9A RID: 11162 RVA: 0x00012960 File Offset: 0x00010B60
		[Token(Token = "0x600280C")]
		[Address(RVA = "0x1012F1B70", Offset = "0x12F1B70", VA = "0x1012F1B70")]
		public bool IsIrqRequest()
		{
			return default(bool);
		}

		// Token: 0x06002B9B RID: 11163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600280D")]
		[Address(RVA = "0x1012F1BA0", Offset = "0x12F1BA0", VA = "0x1012F1BA0")]
		public void CancelEventCommand()
		{
		}

		// Token: 0x06002B9C RID: 11164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600280E")]
		[Address(RVA = "0x1012F1BD0", Offset = "0x12F1BD0", VA = "0x1012F1BD0")]
		public void RequestIrqCommand(RoomActionCommand pcmd)
		{
		}

		// Token: 0x06002B9D RID: 11165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600280F")]
		[Address(RVA = "0x1012F1C08", Offset = "0x12F1C08", VA = "0x1012F1C08")]
		public void CharaDelete()
		{
		}

		// Token: 0x06002B9E RID: 11166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002810")]
		[Address(RVA = "0x1012F1D20", Offset = "0x12F1D20", VA = "0x1012F1D20", Slot = "18")]
		public override void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
		}

		// Token: 0x06002B9F RID: 11167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002811")]
		[Address(RVA = "0x1012F1E8C", Offset = "0x12F1E8C", VA = "0x1012F1E8C")]
		public void SetGridPositionAvoidObj(int fgridx, int fgridy, int floorID)
		{
		}

		// Token: 0x06002BA0 RID: 11168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002812")]
		[Address(RVA = "0x1012F1FC0", Offset = "0x12F1FC0", VA = "0x1012F1FC0")]
		protected void SetPosition(Vector2 fpos)
		{
		}

		// Token: 0x06002BA1 RID: 11169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002813")]
		[Address(RVA = "0x1012F20D4", Offset = "0x12F20D4", VA = "0x1012F20D4")]
		protected void FollowToTarget()
		{
		}

		// Token: 0x06002BA2 RID: 11170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002814")]
		[Address(RVA = "0x1012F2564", Offset = "0x12F2564", VA = "0x1012F2564")]
		public void PushAction()
		{
		}

		// Token: 0x06002BA3 RID: 11171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002815")]
		[Address(RVA = "0x1012F2568", Offset = "0x12F2568", VA = "0x1012F2568")]
		public void PopAction()
		{
		}

		// Token: 0x06002BA4 RID: 11172 RVA: 0x00012978 File Offset: 0x00010B78
		[Token(Token = "0x6002816")]
		[Address(RVA = "0x1012F256C", Offset = "0x12F256C", VA = "0x1012F256C", Slot = "32")]
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return default(bool);
		}

		// Token: 0x06002BA5 RID: 11173 RVA: 0x00012990 File Offset: 0x00010B90
		[Token(Token = "0x6002817")]
		[Address(RVA = "0x1012EEDFC", Offset = "0x12EEDFC", VA = "0x1012EEDFC")]
		private bool TouchActionCharaMove(eTouchStep touchStep, IRoomObjectControll.ObjTouchState objState)
		{
			return default(bool);
		}

		// Token: 0x06002BA6 RID: 11174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002818")]
		[Address(RVA = "0x1012F3224", Offset = "0x12F3224", VA = "0x1012F3224")]
		public void ForceCancelCharaMoveMode()
		{
		}

		// Token: 0x06002BA7 RID: 11175 RVA: 0x000129A8 File Offset: 0x00010BA8
		[Token(Token = "0x6002819")]
		[Address(RVA = "0x1012F266C", Offset = "0x12F266C", VA = "0x1012F266C")]
		private bool TouchActionCommon(eTouchStep touchStep, IRoomObjectControll.ObjTouchState objState)
		{
			return default(bool);
		}

		// Token: 0x06002BA8 RID: 11176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600281A")]
		[Address(RVA = "0x1012F3648", Offset = "0x12F3648", VA = "0x1012F3648", Slot = "34")]
		public override void CreateHitAreaModel(bool fhitcolor = true)
		{
		}

		// Token: 0x06002BA9 RID: 11177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600281B")]
		[Address(RVA = "0x1012F3B48", Offset = "0x12F3B48", VA = "0x1012F3B48", Slot = "35")]
		public override void DestroyHitAreaModel()
		{
		}

		// Token: 0x06002BAA RID: 11178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600281C")]
		[Address(RVA = "0x1012F2E94", Offset = "0x12F2E94", VA = "0x1012F2E94")]
		private void UpdateHitAreaModelColor(bool hitcolor)
		{
		}

		// Token: 0x06002BAB RID: 11179 RVA: 0x000129C0 File Offset: 0x00010BC0
		[Token(Token = "0x600281D")]
		[Address(RVA = "0x1012F3C04", Offset = "0x12F3C04", VA = "0x1012F3C04")]
		public bool IsSetUpModel()
		{
			return default(bool);
		}

		// Token: 0x06002BAC RID: 11180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600281E")]
		[Address(RVA = "0x1012F3C74", Offset = "0x12F3C74", VA = "0x1012F3C74", Slot = "24")]
		public override void SetBaseColor(Color fcolor)
		{
		}

		// Token: 0x06002BAD RID: 11181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600281F")]
		[Address(RVA = "0x1012F3CC8", Offset = "0x12F3CC8", VA = "0x1012F3CC8")]
		private new void Awake()
		{
		}

		// Token: 0x06002BAE RID: 11182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002820")]
		[Address(RVA = "0x1012F3D6C", Offset = "0x12F3D6C", VA = "0x1012F3D6C")]
		protected void InitChara()
		{
		}

		// Token: 0x06002BAF RID: 11183 RVA: 0x000129D8 File Offset: 0x00010BD8
		[Token(Token = "0x6002821")]
		[Address(RVA = "0x1012F3D70", Offset = "0x12F3D70", VA = "0x1012F3D70")]
		public RoomCharaActMode.eMode GetActMode()
		{
			return RoomCharaActMode.eMode.Idle;
		}

		// Token: 0x06002BB0 RID: 11184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002822")]
		[Address(RVA = "0x1012F3D78", Offset = "0x12F3D78", VA = "0x1012F3D78")]
		public void Setup(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06002BB1 RID: 11185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002823")]
		[Address(RVA = "0x1012F4118", Offset = "0x12F4118", VA = "0x1012F4118", Slot = "22")]
		public override void Destroy()
		{
		}

		// Token: 0x06002BB2 RID: 11186 RVA: 0x000129F0 File Offset: 0x00010BF0
		[Token(Token = "0x6002824")]
		[Address(RVA = "0x1012F4150", Offset = "0x12F4150", VA = "0x1012F4150")]
		public bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06002BB3 RID: 11187 RVA: 0x00012A08 File Offset: 0x00010C08
		[Token(Token = "0x6002825")]
		[Address(RVA = "0x1012F4100", Offset = "0x12F4100", VA = "0x1012F4100")]
		private bool ChangeMode(RoomCharaActMode.eMode fmode, IRoomCharaAct pcall)
		{
			return default(bool);
		}

		// Token: 0x06002BB4 RID: 11188 RVA: 0x00012A20 File Offset: 0x00010C20
		[Token(Token = "0x6002826")]
		[Address(RVA = "0x1012F41EC", Offset = "0x12F41EC", VA = "0x1012F41EC")]
		public bool CheckExistAnyOnCurrent(int floorid)
		{
			return default(bool);
		}

		// Token: 0x06002BB5 RID: 11189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002827")]
		[Address(RVA = "0x1012F2B04", Offset = "0x12F2B04", VA = "0x1012F2B04")]
		public void RequestLiftUp()
		{
		}

		// Token: 0x06002BB6 RID: 11190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002828")]
		[Address(RVA = "0x1012F3004", Offset = "0x12F3004", VA = "0x1012F3004")]
		public void RequestPullDown()
		{
		}

		// Token: 0x06002BB7 RID: 11191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002829")]
		[Address(RVA = "0x1012EFA70", Offset = "0x12EFA70", VA = "0x1012EFA70")]
		public void RequestIdleMode(float foffset = 1f)
		{
		}

		// Token: 0x06002BB8 RID: 11192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600282A")]
		[Address(RVA = "0x1012F433C", Offset = "0x12F433C", VA = "0x1012F433C")]
		private void RequestMoveMode(Vector2 fendPos)
		{
		}

		// Token: 0x06002BB9 RID: 11193 RVA: 0x00012A38 File Offset: 0x00010C38
		[Token(Token = "0x600282B")]
		[Address(RVA = "0x1012F43B8", Offset = "0x12F43B8", VA = "0x1012F43B8")]
		private bool RequestObjectEvent(IRoomObjectControll roomObjHndl)
		{
			return default(bool);
		}

		// Token: 0x06002BBA RID: 11194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600282C")]
		[Address(RVA = "0x1012E9E08", Offset = "0x12E9E08", VA = "0x1012E9E08")]
		public void AbortObjEventMode()
		{
		}

		// Token: 0x06002BBB RID: 11195 RVA: 0x00012A50 File Offset: 0x00010C50
		[Token(Token = "0x600282D")]
		[Address(RVA = "0x1012F443C", Offset = "0x12F443C", VA = "0x1012F443C")]
		public bool RequestSleep(int factionno)
		{
			return default(bool);
		}

		// Token: 0x06002BBC RID: 11196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600282E")]
		[Address(RVA = "0x1012F2AEC", Offset = "0x12F2AEC", VA = "0x1012F2AEC")]
		private void RequestTweetForceOff()
		{
		}

		// Token: 0x06002BBD RID: 11197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600282F")]
		[Address(RVA = "0x1012EF4A8", Offset = "0x12EF4A8", VA = "0x1012EF4A8")]
		protected void CheckNextUpdate()
		{
		}

		// Token: 0x06002BBE RID: 11198 RVA: 0x00012A68 File Offset: 0x00010C68
		[Token(Token = "0x6002830")]
		[Address(RVA = "0x1012F35B4", Offset = "0x12F35B4", VA = "0x1012F35B4")]
		private bool EnableCharaMove()
		{
			return default(bool);
		}

		// Token: 0x06002BBF RID: 11199 RVA: 0x00012A80 File Offset: 0x00010C80
		[Token(Token = "0x6002831")]
		[Address(RVA = "0x1012F3590", Offset = "0x12F3590", VA = "0x1012F3590")]
		private float GetLongTapTime()
		{
			return 0f;
		}

		// Token: 0x06002BC0 RID: 11200 RVA: 0x00012A98 File Offset: 0x00010C98
		[Token(Token = "0x6002832")]
		[Address(RVA = "0x1012F44CC", Offset = "0x12F44CC", VA = "0x1012F44CC")]
		private bool IsOpenRoomMenberWindow()
		{
			return default(bool);
		}

		// Token: 0x06002BC1 RID: 11201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002833")]
		[Address(RVA = "0x1012F45FC", Offset = "0x12F45FC", VA = "0x1012F45FC")]
		public RoomObjectCtrlChara()
		{
		}

		// Token: 0x04003B2F RID: 15151
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002A98")]
		[SerializeField]
		protected float m_adjustCharaMoveSpd;

		// Token: 0x04003B30 RID: 15152
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002A99")]
		[SerializeField]
		protected float m_adjustCharaFloatHeight;

		// Token: 0x04003B31 RID: 15153
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4002A9A")]
		[SerializeField]
		protected float m_adjustCharaFloatingDiff;

		// Token: 0x04003B32 RID: 15154
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002A9B")]
		private readonly Vector3 AABB_OFFSET;

		// Token: 0x04003B33 RID: 15155
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002A9C")]
		private readonly AnimationCurve m_PanelAlphaCurve;

		// Token: 0x04003B34 RID: 15156
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002A9D")]
		public bool m_flgCharaMove;

		// Token: 0x04003B35 RID: 15157
		[Cpp2IlInjected.FieldOffset(Offset = "0xD9")]
		[Token(Token = "0x4002A9E")]
		public bool m_flgMissionUnlockable;

		// Token: 0x04003B36 RID: 15158
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002A9F")]
		private IRoomCharaManager m_CharaManager;

		// Token: 0x04003B37 RID: 15159
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002AA0")]
		private IRoomObjectControll m_LinkObj;

		// Token: 0x04003B38 RID: 15160
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4002AA1")]
		private GameObject m_ModelNode;

		// Token: 0x04003B39 RID: 15161
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4002AA2")]
		protected CharacterHandler m_BaseHandle;

		// Token: 0x04003B3A RID: 15162
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4002AA3")]
		protected CharacterAnim m_BaseAnime;

		// Token: 0x04003B3B RID: 15163
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4002AA4")]
		private int m_CharaID;

		// Token: 0x04003B3C RID: 15164
		[Cpp2IlInjected.FieldOffset(Offset = "0x10C")]
		[Token(Token = "0x4002AA5")]
		private int m_ViewCharaID;

		// Token: 0x04003B3D RID: 15165
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4002AA6")]
		public int m_SlotId;

		// Token: 0x04003B3E RID: 15166
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4002AA7")]
		protected RoomAnimAccessMap m_AnimeMap;

		// Token: 0x04003B3F RID: 15167
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4002AA8")]
		protected RoomCharaHud m_RoomCharaHud;

		// Token: 0x04003B40 RID: 15168
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4002AA9")]
		protected Transform m_ShadowHandle;

		// Token: 0x04003B41 RID: 15169
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4002AAA")]
		protected Vector3 m_ShadowPos;

		// Token: 0x04003B42 RID: 15170
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4002AAB")]
		public RoomShadowControll m_Shadow;

		// Token: 0x04003B43 RID: 15171
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4002AAC")]
		public float m_ShadowLength;

		// Token: 0x04003B44 RID: 15172
		[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
		[Token(Token = "0x4002AAD")]
		public float m_ShadowScale;

		// Token: 0x04003B45 RID: 15173
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4002AAE")]
		private IRoomObjectControll.ModelRenderConfig m_DefaultConfig;

		// Token: 0x04003B46 RID: 15174
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4002AAF")]
		private RoomGridState m_GridState;

		// Token: 0x04003B47 RID: 15175
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4002AB0")]
		private AStar m_AStar;

		// Token: 0x04003B48 RID: 15176
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4002AB1")]
		private Renderer m_ShaderRender;

		// Token: 0x04003B49 RID: 15177
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4002AB2")]
		private bool m_ShadowUp;

		// Token: 0x04003B4A RID: 15178
		[Cpp2IlInjected.FieldOffset(Offset = "0x179")]
		[Token(Token = "0x4002AB3")]
		private bool m_WakeUpActions;

		// Token: 0x04003B4B RID: 15179
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4002AB4")]
		private RoomCharaEventReq m_IrqReq;

		// Token: 0x04003B4C RID: 15180
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4002AB5")]
		private RoomAdditionalAnimCtrl m_AdditionalAnimCtrl;

		// Token: 0x04003B4D RID: 15181
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x4002AB6")]
		private string m_LastActionKey;

		// Token: 0x04003B4E RID: 15182
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x4002AB7")]
		private bool m_LastPlayingIsCommonAnim;

		// Token: 0x04003B4F RID: 15183
		[Cpp2IlInjected.FieldOffset(Offset = "0x19C")]
		[Token(Token = "0x4002AB8")]
		private WrapMode m_LastWrapMode;

		// Token: 0x04003B50 RID: 15184
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x4002AB9")]
		private bool m_isPickable;

		// Token: 0x04003B52 RID: 15186
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A2")]
		[Token(Token = "0x4002ABB")]
		private bool m_ReflectMoveFlg;

		// Token: 0x04003B53 RID: 15187
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A4")]
		[Token(Token = "0x4002ABC")]
		public int m_GridX;

		// Token: 0x04003B54 RID: 15188
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4002ABD")]
		public int m_GridY;

		// Token: 0x04003B55 RID: 15189
		[Cpp2IlInjected.FieldOffset(Offset = "0x1AC")]
		[Token(Token = "0x4002ABE")]
		public int m_GridX2;

		// Token: 0x04003B56 RID: 15190
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4002ABF")]
		public int m_GridY2;

		// Token: 0x04003B57 RID: 15191
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B4")]
		[Token(Token = "0x4002AC0")]
		private bool m_TouchStepUp;

		// Token: 0x04003B58 RID: 15192
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B5")]
		[Token(Token = "0x4002AC1")]
		private bool m_TouchToProfile;

		// Token: 0x04003B59 RID: 15193
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x4002AC2")]
		private float m_TouchTime;

		// Token: 0x04003B5A RID: 15194
		[Cpp2IlInjected.FieldOffset(Offset = "0x1BC")]
		[Token(Token = "0x4002AC3")]
		private IVector3 m_TargetPos;

		// Token: 0x04003B5B RID: 15195
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C8")]
		[Token(Token = "0x4002AC4")]
		private float m_FloatingOffset;

		// Token: 0x04003B5C RID: 15196
		[Cpp2IlInjected.FieldOffset(Offset = "0x1CC")]
		[Token(Token = "0x4002AC5")]
		private float m_FloatingTime;

		// Token: 0x04003B5D RID: 15197
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D0")]
		[Token(Token = "0x4002AC6")]
		private float m_heightRate;

		// Token: 0x04003B5E RID: 15198
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D8")]
		[Token(Token = "0x4002AC7")]
		protected GameObject m_HitMarkObj;

		// Token: 0x04003B5F RID: 15199
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E0")]
		[Token(Token = "0x4002AC8")]
		protected Material m_HitMarkMtl;

		// Token: 0x04003B60 RID: 15200
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E8")]
		[Token(Token = "0x4002AC9")]
		private RoomCharaObjSearch m_ObjSearch;

		// Token: 0x04003B61 RID: 15201
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F0")]
		[Token(Token = "0x4002ACA")]
		private RoomCharaActMode.eMode m_Mode;

		// Token: 0x04003B62 RID: 15202
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F8")]
		[Token(Token = "0x4002ACB")]
		public IRoomCharaAct m_CallUpdate;

		// Token: 0x04003B63 RID: 15203
		[Cpp2IlInjected.FieldOffset(Offset = "0x200")]
		[Token(Token = "0x4002ACC")]
		private RoomCharaActMode m_NextMode;

		// Token: 0x04003B64 RID: 15204
		[Cpp2IlInjected.FieldOffset(Offset = "0x208")]
		[Token(Token = "0x4002ACD")]
		private RoomCharaActIdle m_IdleAct;

		// Token: 0x04003B65 RID: 15205
		[Cpp2IlInjected.FieldOffset(Offset = "0x210")]
		[Token(Token = "0x4002ACE")]
		private RoomCharaActMove m_MoveAct;

		// Token: 0x04003B66 RID: 15206
		[Cpp2IlInjected.FieldOffset(Offset = "0x218")]
		[Token(Token = "0x4002ACF")]
		private RoomCharaActObject m_ObjAct;

		// Token: 0x04003B67 RID: 15207
		[Cpp2IlInjected.FieldOffset(Offset = "0x220")]
		[Token(Token = "0x4002AD0")]
		private RoomCharaActRejoice m_RejoiceAct;

		// Token: 0x04003B68 RID: 15208
		[Cpp2IlInjected.FieldOffset(Offset = "0x228")]
		[Token(Token = "0x4002AD1")]
		private RoomCharaActEnjoy m_EnjoyAct;

		// Token: 0x04003B69 RID: 15209
		[Cpp2IlInjected.FieldOffset(Offset = "0x230")]
		[Token(Token = "0x4002AD2")]
		private RoomCharaActTweet m_TweetAct;

		// Token: 0x04003B6A RID: 15210
		[Cpp2IlInjected.FieldOffset(Offset = "0x238")]
		[Token(Token = "0x4002AD3")]
		private RoomCharaActRoomOut m_RoomOutAct;

		// Token: 0x04003B6B RID: 15211
		[Cpp2IlInjected.FieldOffset(Offset = "0x240")]
		[Token(Token = "0x4002AD4")]
		private RoomCharaActSleep m_SleepAct;

		// Token: 0x04003B6C RID: 15212
		[Cpp2IlInjected.FieldOffset(Offset = "0x248")]
		[Token(Token = "0x4002AD5")]
		private RoomCharaActPic m_PicAct;
	}
}
