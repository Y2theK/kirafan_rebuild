﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A14 RID: 2580
	[Token(Token = "0x2000737")]
	[StructLayout(3)]
	public class RoomObjectCtrlWallDecoration : RoomObjectCtrlBase
	{
		// Token: 0x06002BC4 RID: 11204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002834")]
		[Address(RVA = "0x1012F4884", Offset = "0x12F4884", VA = "0x1012F4884", Slot = "18")]
		public override void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
		}

		// Token: 0x06002BC5 RID: 11205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002835")]
		[Address(RVA = "0x1012F4BEC", Offset = "0x12F4BEC", VA = "0x1012F4BEC", Slot = "19")]
		public override void SetGridPosition(int fgridx, int fgridy, IRoomFloorManager floorManager)
		{
		}

		// Token: 0x06002BC6 RID: 11206 RVA: 0x00012AC8 File Offset: 0x00010CC8
		[Token(Token = "0x6002836")]
		[Address(RVA = "0x1012F4F10", Offset = "0x12F4F10", VA = "0x1012F4F10", Slot = "21")]
		public override IVector2 GetPosToSize(int fchkposx, int fchkposy)
		{
			return default(IVector2);
		}

		// Token: 0x06002BC7 RID: 11207 RVA: 0x00012AE0 File Offset: 0x00010CE0
		[Token(Token = "0x6002837")]
		[Address(RVA = "0x1012F4F64", Offset = "0x12F4F64", VA = "0x1012F4F64", Slot = "12")]
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			return default(bool);
		}

		// Token: 0x06002BC8 RID: 11208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002838")]
		[Address(RVA = "0x1012F50BC", Offset = "0x12F50BC", VA = "0x1012F50BC", Slot = "34")]
		public override void CreateHitAreaModel(bool fhitcolor)
		{
		}

		// Token: 0x06002BC9 RID: 11209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002839")]
		[Address(RVA = "0x1012F5CA8", Offset = "0x12F5CA8", VA = "0x1012F5CA8", Slot = "41")]
		public override void UpDirToScale()
		{
		}

		// Token: 0x06002BCA RID: 11210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600283A")]
		[Address(RVA = "0x1012F5F78", Offset = "0x12F5F78", VA = "0x1012F5F78", Slot = "42")]
		public override void UpDirToModelSwap()
		{
		}

		// Token: 0x06002BCB RID: 11211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600283B")]
		[Address(RVA = "0x1012F62C0", Offset = "0x12F62C0", VA = "0x1012F62C0", Slot = "43")]
		protected override void SelectingEnableOn()
		{
		}

		// Token: 0x06002BCC RID: 11212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600283C")]
		[Address(RVA = "0x1012F6778", Offset = "0x12F6778", VA = "0x1012F6778", Slot = "44")]
		protected override void SelectingDisableOn()
		{
		}

		// Token: 0x06002BCD RID: 11213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600283D")]
		[Address(RVA = "0x1012F6C48", Offset = "0x12F6C48", VA = "0x1012F6C48", Slot = "35")]
		public override void DestroyHitAreaModel()
		{
		}

		// Token: 0x06002BCE RID: 11214 RVA: 0x00012AF8 File Offset: 0x00010CF8
		[Token(Token = "0x600283E")]
		[Address(RVA = "0x1012F4B44", Offset = "0x12F4B44", VA = "0x1012F4B44")]
		public CharacterDefine.eDir CalcObjDir(int posx, int posy)
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002BCF RID: 11215 RVA: 0x00012B10 File Offset: 0x00010D10
		[Token(Token = "0x600283F")]
		[Address(RVA = "0x1012F4E90", Offset = "0x12F4E90", VA = "0x1012F4E90")]
		public CharacterDefine.eDir CalcObjDir(int posx, int posy, IRoomFloorManager floorManager)
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002BD0 RID: 11216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002840")]
		[Address(RVA = "0x1012F6DA8", Offset = "0x12F6DA8", VA = "0x1012F6DA8")]
		public RoomObjectCtrlWallDecoration()
		{
		}

		// Token: 0x04003B6F RID: 15215
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4002AD6")]
		private GameObject m_DepthHitMarkObj;

		// Token: 0x04003B70 RID: 15216
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4002AD7")]
		private Material m_DepthHitMarkMtl;
	}
}
