﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000595 RID: 1429
	[Token(Token = "0x2000488")]
	[StructLayout(3)]
	public class RoomObjectFilterCategoryDB : ScriptableObject
	{
		// Token: 0x060015EF RID: 5615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600149F")]
		[Address(RVA = "0x1012F6DAC", Offset = "0x12F6DAC", VA = "0x1012F6DAC")]
		public RoomObjectFilterCategoryDB()
		{
		}

		// Token: 0x04001A4F RID: 6735
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013B9")]
		public RoomObjectFilterCategoryDB_Param[] m_Params;
	}
}
