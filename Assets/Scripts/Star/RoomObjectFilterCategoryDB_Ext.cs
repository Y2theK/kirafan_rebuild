﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000526 RID: 1318
	[Token(Token = "0x200041C")]
	[StructLayout(3)]
	public static class RoomObjectFilterCategoryDB_Ext
	{
		// Token: 0x06001585 RID: 5509 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600143A")]
		[Address(RVA = "0x1012F6DB4", Offset = "0x12F6DB4", VA = "0x1012F6DB4")]
		public static string[] GetCategoryNames(this RoomObjectFilterCategoryDB self, eRoomObjFilterType type)
		{
			return null;
		}

		// Token: 0x06001586 RID: 5510 RVA: 0x00009600 File Offset: 0x00007800
		[Token(Token = "0x600143B")]
		[Address(RVA = "0x1012F6EDC", Offset = "0x12F6EDC", VA = "0x1012F6EDC")]
		public static int GetCategoryNum(this RoomObjectFilterCategoryDB self, eRoomObjFilterType type)
		{
			return 0;
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x00009618 File Offset: 0x00007818
		[Token(Token = "0x600143C")]
		[Address(RVA = "0x1012F6F08", Offset = "0x12F6F08", VA = "0x1012F6F08")]
		public static int GetCategoryIndexFromName(this RoomObjectFilterCategoryDB self, eRoomObjFilterType type, string name)
		{
			return 0;
		}
	}
}
