﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000594 RID: 1428
	[Token(Token = "0x2000487")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct RoomObjectFilterCategoryDB_Param
	{
		// Token: 0x04001A4B RID: 6731
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40013B5")]
		public int m_ID;

		// Token: 0x04001A4C RID: 6732
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40013B6")]
		public int m_Index;

		// Token: 0x04001A4D RID: 6733
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40013B7")]
		public int m_FilterCategory;

		// Token: 0x04001A4E RID: 6734
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40013B8")]
		public string m_Name;
	}
}
