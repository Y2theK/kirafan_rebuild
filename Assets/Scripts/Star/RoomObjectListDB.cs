﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000598 RID: 1432
	[Token(Token = "0x200048B")]
	[StructLayout(3)]
	public class RoomObjectListDB : ScriptableObject
	{
		// Token: 0x060015F0 RID: 5616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014A0")]
		[Address(RVA = "0x1012F6FBC", Offset = "0x12F6FBC", VA = "0x1012F6FBC")]
		public RoomObjectListDB()
		{
		}

		// Token: 0x04001A79 RID: 6777
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013E3")]
		public RoomObjectListDB_Param[] m_Params;
	}
}
