﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000527 RID: 1319
	[Token(Token = "0x200041D")]
	[StructLayout(3)]
	public static class RoomObjectListDB_Ext
	{
		// Token: 0x06001588 RID: 5512 RVA: 0x00009630 File Offset: 0x00007830
		[Token(Token = "0x600143D")]
		[Address(RVA = "0x1012F6FC4", Offset = "0x12F6FC4", VA = "0x1012F6FC4")]
		public static RoomObjectListDB_Param GetParam(this RoomObjectListDB self, int accessID)
		{
			return default(RoomObjectListDB_Param);
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x00009648 File Offset: 0x00007848
		[Token(Token = "0x600143E")]
		[Address(RVA = "0x1012F7100", Offset = "0x12F7100", VA = "0x1012F7100")]
		public static bool CheckBanOnSell(this RoomObjectListDB_Param self)
		{
			return default(bool);
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600143F")]
		[Address(RVA = "0x1012F710C", Offset = "0x12F710C", VA = "0x1012F710C")]
		public static List<RoomObjectListDB_Param> GetAllRoomObjDataFromCategory(this RoomObjectListDB self, eRoomObjectCategory category)
		{
			return null;
		}
	}
}
