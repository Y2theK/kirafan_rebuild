﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000597 RID: 1431
	[Token(Token = "0x200048A")]
	[Serializable]
	[StructLayout(0)]
	public struct RoomObjectListDB_Param
	{
		// Token: 0x04001A55 RID: 6741
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40013BF")]
		public int m_ID;

		// Token: 0x04001A56 RID: 6742
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40013C0")]
		public int m_Category;

		// Token: 0x04001A57 RID: 6743
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40013C1")]
		public int m_TitleType;

		// Token: 0x04001A58 RID: 6744
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40013C2")]
		public string m_Name;

		// Token: 0x04001A59 RID: 6745
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013C3")]
		public int m_SizeX;

		// Token: 0x04001A5A RID: 6746
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40013C4")]
		public int m_SizeY;

		// Token: 0x04001A5B RID: 6747
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40013C5")]
		public int m_Depth;

		// Token: 0x04001A5C RID: 6748
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40013C6")]
		public int m_DBAccessID;

		// Token: 0x04001A5D RID: 6749
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40013C7")]
		public uint m_OptionID;

		// Token: 0x04001A5E RID: 6750
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40013C8")]
		public ushort m_Flag;

		// Token: 0x04001A5F RID: 6751
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40013C9")]
		public string m_initHideObjName;

		// Token: 0x04001A60 RID: 6752
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40013CA")]
		public int m_changeNight;

		// Token: 0x04001A61 RID: 6753
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40013CB")]
		public int m_nonReverce;

		// Token: 0x04001A62 RID: 6754
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40013CC")]
		public ushort m_SearchDown;

		// Token: 0x04001A63 RID: 6755
		[Cpp2IlInjected.FieldOffset(Offset = "0x42")]
		[Token(Token = "0x40013CD")]
		public ushort m_SearchRight;

		// Token: 0x04001A64 RID: 6756
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40013CE")]
		public ushort m_SearchUp;

		// Token: 0x04001A65 RID: 6757
		[Cpp2IlInjected.FieldOffset(Offset = "0x46")]
		[Token(Token = "0x40013CF")]
		public ushort m_SearchLeft;

		// Token: 0x04001A66 RID: 6758
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40013D0")]
		public int m_ObjEventType;

		// Token: 0x04001A67 RID: 6759
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40013D1")]
		public int[] m_ObjEventArgs;

		// Token: 0x04001A68 RID: 6760
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40013D2")]
		public int m_BrunchCondCate;

		// Token: 0x04001A69 RID: 6761
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40013D3")]
		public string m_BrunchCondParam;

		// Token: 0x04001A6A RID: 6762
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40013D4")]
		public int m_BrunchEventType;

		// Token: 0x04001A6B RID: 6763
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40013D5")]
		public int m_BrunchEventID;

		// Token: 0x04001A6C RID: 6764
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40013D6")]
		public int m_BrunchEventVoiceID;

		// Token: 0x04001A6D RID: 6765
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x40013D7")]
		public int m_BuyAmount;

		// Token: 0x04001A6E RID: 6766
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40013D8")]
		public int m_SellAmount;

		// Token: 0x04001A6F RID: 6767
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x40013D9")]
		public short m_MaxNum;

		// Token: 0x04001A70 RID: 6768
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40013DA")]
		public string m_Description;

		// Token: 0x04001A71 RID: 6769
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40013DB")]
		public sbyte m_BargainFlag;

		// Token: 0x04001A72 RID: 6770
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x40013DC")]
		public int m_BargainBuyAmount;

		// Token: 0x04001A73 RID: 6771
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40013DD")]
		public sbyte m_PickUp;

		// Token: 0x04001A74 RID: 6772
		[Cpp2IlInjected.FieldOffset(Offset = "0x91")]
		[Token(Token = "0x40013DE")]
		public sbyte m_Tutorial;

		// Token: 0x04001A75 RID: 6773
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x40013DF")]
		public int m_Sort;

		// Token: 0x04001A76 RID: 6774
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40013E0")]
		public int m_ConditionCategory;

		// Token: 0x04001A77 RID: 6775
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40013E1")]
		public int[] m_ConditionParams;

		// Token: 0x04001A78 RID: 6776
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40013E2")]
		public string[] m_FilterName;
	}
}
