﻿namespace Star {
    public class RoomObjectListUtil {
        public static int CategoryIDToAccessKey(eRoomObjectCategory category, int objID) {
            return (int)category * 100000 + objID;
        }

        public static eRoomObjectCategory GetIDToCategory(int accessId) {
            return (eRoomObjectCategory)(accessId / 100000);
        }

        public static RoomObjectListDB_Param GetObjectParam(eRoomObjectCategory fcategory, int fobjid) {
            RoomObjectListDB roomObjectListDB = GameSystem.Inst.DbMng.RoomObjectListDB;
            RoomObjectListDB_Param result = roomObjectListDB.m_Params[0];
            int key = CategoryIDToAccessKey(fcategory, fobjid);
            for (int i = 0; i < roomObjectListDB.m_Params.Length; i++) {
                if (roomObjectListDB.m_Params[i].m_DBAccessID == key) {
                    result = roomObjectListDB.m_Params[i];
                    break;
                }
            }
            return result;
        }

        public static RoomObjectListDB_Param GetAccessKeyToObjectParam(int faccesskey) {
            RoomObjectListDB roomObjectListDB = GameSystem.Inst.DbMng.RoomObjectListDB;
            RoomObjectListDB_Param result = roomObjectListDB.m_Params[0];
            for (int i = 0; i < roomObjectListDB.m_Params.Length; i++) {
                if (roomObjectListDB.m_Params[i].m_DBAccessID == faccesskey) {
                    result = roomObjectListDB.m_Params[i];
                    break;
                }
            }
            return result;
        }

        public static int UpDataParamToResourceID(UserRoomObjectData pobj) {
            return CategoryIDToAccessKey(pobj.ObjCategory, pobj.ObjID);
        }
    }
}
