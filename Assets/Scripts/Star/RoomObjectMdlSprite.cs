﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A15 RID: 2581
	[Token(Token = "0x2000738")]
	[StructLayout(3)]
	public class RoomObjectMdlSprite : IRoomObjectControll
	{
		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06002BD1 RID: 11217 RVA: 0x00012B28 File Offset: 0x00010D28
		[Token(Token = "0x170002BB")]
		public int SubKeyID
		{
			[Token(Token = "0x6002841")]
			[Address(RVA = "0x1012F759C", Offset = "0x12F759C", VA = "0x1012F759C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06002BD2 RID: 11218 RVA: 0x00012B40 File Offset: 0x00010D40
		[Token(Token = "0x170002BC")]
		public int SubKeyID2
		{
			[Token(Token = "0x6002842")]
			[Address(RVA = "0x1012F75A4", Offset = "0x12F75A4", VA = "0x1012F75A4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06002BD3 RID: 11219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002843")]
		[Address(RVA = "0x1012E882C", Offset = "0x12E882C", VA = "0x1012E882C")]
		public void SetupSimple(eRoomObjectCategory category, int objID, int fsubkey, int fsubkey2, int flayerid, int frenderlayer, bool finit)
		{
		}

		// Token: 0x06002BD4 RID: 11220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002844")]
		[Address(RVA = "0x1012F7618", Offset = "0x12F7618", VA = "0x1012F7618", Slot = "20")]
		public override void UpdateObj()
		{
		}

		// Token: 0x06002BD5 RID: 11221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002845")]
		[Address(RVA = "0x1012F7874", Offset = "0x12F7874", VA = "0x1012F7874", Slot = "22")]
		public override void Destroy()
		{
		}

		// Token: 0x06002BD6 RID: 11222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002846")]
		[Address(RVA = "0x1012F7BD8", Offset = "0x12F7BD8", VA = "0x1012F7BD8", Slot = "24")]
		public override void SetBaseColor(Color fcolor)
		{
		}

		// Token: 0x06002BD7 RID: 11223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002847")]
		[Address(RVA = "0x1012F7D0C", Offset = "0x12F7D0C", VA = "0x1012F7D0C")]
		public void AttachSprite(GameObject sprite)
		{
		}

		// Token: 0x06002BD8 RID: 11224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002848")]
		[Address(RVA = "0x1012F7EA0", Offset = "0x12F7EA0", VA = "0x1012F7EA0")]
		public void SortingOrderMdlSprite(float flayerpos, int sortingOrder, bool isLined)
		{
		}

		// Token: 0x06002BD9 RID: 11225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002849")]
		[Address(RVA = "0x1012F7FEC", Offset = "0x12F7FEC", VA = "0x1012F7FEC")]
		public void SetFadeIn(float ftime)
		{
		}

		// Token: 0x06002BDA RID: 11226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600284A")]
		[Address(RVA = "0x1012F7920", Offset = "0x12F7920", VA = "0x1012F7920")]
		private void ModelDestroy()
		{
		}

		// Token: 0x06002BDB RID: 11227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600284B")]
		[Address(RVA = "0x1012F8490", Offset = "0x12F8490", VA = "0x1012F8490")]
		public RoomObjectMdlSprite()
		{
		}

		// Token: 0x04003B71 RID: 15217
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002AD8")]
		protected int m_SubKeyID;

		// Token: 0x04003B72 RID: 15218
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002AD9")]
		protected int m_SubKeyID2;

		// Token: 0x04003B73 RID: 15219
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002ADA")]
		private Renderer[] m_Render;

		// Token: 0x04003B74 RID: 15220
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002ADB")]
		private GameObject m_Obj;

		// Token: 0x04003B75 RID: 15221
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002ADC")]
		protected int m_SelectingMode;

		// Token: 0x04003B76 RID: 15222
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002ADD")]
		private short[] m_DefaultSortID;

		// Token: 0x04003B77 RID: 15223
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002ADE")]
		private int m_LayerID;

		// Token: 0x04003B78 RID: 15224
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4002ADF")]
		private int m_RenderLayerID;

		// Token: 0x04003B79 RID: 15225
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002AE0")]
		private RoomPartsActionPakage m_Action;
	}
}
