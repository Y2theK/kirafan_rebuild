﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A16 RID: 2582
	[Token(Token = "0x2000739")]
	[StructLayout(3)]
	public class RoomObjectModel : IRoomObjectControll
	{
		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06002BDC RID: 11228 RVA: 0x00012B58 File Offset: 0x00010D58
		[Token(Token = "0x170002BD")]
		public int SubKeyID
		{
			[Token(Token = "0x600284C")]
			[Address(RVA = "0x1012F8498", Offset = "0x12F8498", VA = "0x1012F8498")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06002BDD RID: 11229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600284D")]
		[Address(RVA = "0x1012E8AA4", Offset = "0x12E8AA4", VA = "0x1012E8AA4")]
		public void SetupObj(eRoomObjectCategory category, int objID, CharacterDefine.eDir dir, int posX, int posY, int floorID, int subkey)
		{
		}

		// Token: 0x06002BDE RID: 11230 RVA: 0x00012B70 File Offset: 0x00010D70
		[Token(Token = "0x600284E")]
		[Address(RVA = "0x1012EA2FC", Offset = "0x12EA2FC", VA = "0x1012EA2FC", Slot = "12")]
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			return default(bool);
		}

		// Token: 0x06002BDF RID: 11231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600284F")]
		[Address(RVA = "0x1012F84A8", Offset = "0x12F84A8", VA = "0x1012F84A8", Slot = "20")]
		public override void UpdateObj()
		{
		}

		// Token: 0x06002BE0 RID: 11232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002850")]
		[Address(RVA = "0x1012EAFE0", Offset = "0x12EAFE0", VA = "0x1012EAFE0", Slot = "22")]
		public override void Destroy()
		{
		}

		// Token: 0x06002BE1 RID: 11233 RVA: 0x00012B88 File Offset: 0x00010D88
		[Token(Token = "0x6002851")]
		[Address(RVA = "0x1012F84D4", Offset = "0x12F84D4", VA = "0x1012F84D4", Slot = "23")]
		public override bool CheckWithProcDestory()
		{
			return default(bool);
		}

		// Token: 0x06002BE2 RID: 11234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002852")]
		[Address(RVA = "0x1012F85AC", Offset = "0x12F85AC", VA = "0x1012F85AC", Slot = "24")]
		public override void SetBaseColor(Color fcolor)
		{
		}

		// Token: 0x06002BE3 RID: 11235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002853")]
		[Address(RVA = "0x1012F86F0", Offset = "0x12F86F0", VA = "0x1012F86F0")]
		public void AttachModel(GameObject modelObj)
		{
		}

		// Token: 0x06002BE4 RID: 11236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002854")]
		[Address(RVA = "0x1012F9FA0", Offset = "0x12F9FA0", VA = "0x1012F9FA0", Slot = "40")]
		public virtual void SetUpAnimation()
		{
		}

		// Token: 0x06002BE5 RID: 11237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002855")]
		[Address(RVA = "0x1012F9E60", Offset = "0x12F9E60", VA = "0x1012F9E60")]
		public void MakeColliderLink(GameObject ptarget)
		{
		}

		// Token: 0x06002BE6 RID: 11238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002856")]
		[Address(RVA = "0x1012F9FA4", Offset = "0x12F9FA4", VA = "0x1012F9FA4")]
		public void SortingOrderModel(float flayerpos, int sortingOrder, bool isLined)
		{
		}

		// Token: 0x06002BE7 RID: 11239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002857")]
		[Address(RVA = "0x1012FA1E8", Offset = "0x12FA1E8", VA = "0x1012FA1E8")]
		public void SortingNonOrderModel(float flayerpos, int sortingOrder, bool isLinked)
		{
		}

		// Token: 0x06002BE8 RID: 11240 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002858")]
		[Address(RVA = "0x1012FA478", Offset = "0x12FA478", VA = "0x1012FA478", Slot = "37")]
		public override IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			return null;
		}

		// Token: 0x06002BE9 RID: 11241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002859")]
		[Address(RVA = "0x1012FA690", Offset = "0x12FA690", VA = "0x1012FA690", Slot = "38")]
		public override void SetSortKey(string ptargetname, int fsortkey, int fsortkeyOffset = 0)
		{
		}

		// Token: 0x06002BEA RID: 11242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285A")]
		[Address(RVA = "0x1012FA85C", Offset = "0x12FA85C", VA = "0x1012FA85C", Slot = "39")]
		public override void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
		}

		// Token: 0x06002BEB RID: 11243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285B")]
		[Address(RVA = "0x1012FABC8", Offset = "0x12FABC8", VA = "0x1012FABC8")]
		public void ClearAnimCtrl()
		{
		}

		// Token: 0x06002BEC RID: 11244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285C")]
		[Address(RVA = "0x1012FABD0", Offset = "0x12FABD0", VA = "0x1012FABD0")]
		public void AnimCtrlOpen()
		{
		}

		// Token: 0x06002BED RID: 11245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285D")]
		[Address(RVA = "0x1012FAC6C", Offset = "0x12FAC6C", VA = "0x1012FAC6C")]
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder)
		{
		}

		// Token: 0x06002BEE RID: 11246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285E")]
		[Address(RVA = "0x1012FAD4C", Offset = "0x12FAD4C", VA = "0x1012FAD4C")]
		public void AnimCtrlClose()
		{
		}

		// Token: 0x06002BEF RID: 11247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600285F")]
		[Address(RVA = "0x1012FADE8", Offset = "0x12FADE8", VA = "0x1012FADE8", Slot = "27")]
		public override void PlayMotion(int fmotionid, WrapMode fmode, float fspeed)
		{
		}

		// Token: 0x06002BF0 RID: 11248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002860")]
		[Address(RVA = "0x1012FB09C", Offset = "0x12FB09C", VA = "0x1012FB09C")]
		public void PlayAnim(string actionKey, WrapMode wrapMode)
		{
		}

		// Token: 0x06002BF1 RID: 11249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002861")]
		[Address(RVA = "0x1012FB190", Offset = "0x12FB190", VA = "0x1012FB190", Slot = "28")]
		public override void ResetAnim()
		{
		}

		// Token: 0x06002BF2 RID: 11250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002862")]
		[Address(RVA = "0x1012FB7B0", Offset = "0x12FB7B0", VA = "0x1012FB7B0", Slot = "29")]
		public override void PauseAnime()
		{
		}

		// Token: 0x06002BF3 RID: 11251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002863")]
		[Address(RVA = "0x1012FB84C", Offset = "0x12FB84C", VA = "0x1012FB84C", Slot = "30")]
		public override void ResetAnimeFrame()
		{
		}

		// Token: 0x06002BF4 RID: 11252 RVA: 0x00012BA0 File Offset: 0x00010DA0
		[Token(Token = "0x6002864")]
		[Address(RVA = "0x1012FB640", Offset = "0x12FB640", VA = "0x1012FB640")]
		private bool IsLRAsymmetrical()
		{
			return default(bool);
		}

		// Token: 0x06002BF5 RID: 11253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002865")]
		[Address(RVA = "0x1012F8FF4", Offset = "0x12F8FF4", VA = "0x1012F8FF4")]
		private void CheckModelDirFunc()
		{
		}

		// Token: 0x06002BF6 RID: 11254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002866")]
		[Address(RVA = "0x1012F5DCC", Offset = "0x12F5DCC", VA = "0x1012F5DCC", Slot = "41")]
		public virtual void UpDirToScale()
		{
		}

		// Token: 0x06002BF7 RID: 11255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002867")]
		[Address(RVA = "0x1012F6098", Offset = "0x12F6098", VA = "0x1012F6098", Slot = "42")]
		public virtual void UpDirToModelSwap()
		{
		}

		// Token: 0x06002BF8 RID: 11256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002868")]
		[Address(RVA = "0x1012FB98C", Offset = "0x12FB98C", VA = "0x1012FB98C", Slot = "26")]
		public override void SetSeleting(bool flg, bool isDisable = false)
		{
		}

		// Token: 0x06002BF9 RID: 11257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002869")]
		[Address(RVA = "0x1012F63D4", Offset = "0x12F63D4", VA = "0x1012F63D4", Slot = "43")]
		protected virtual void SelectingEnableOn()
		{
		}

		// Token: 0x06002BFA RID: 11258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600286A")]
		[Address(RVA = "0x1012F688C", Offset = "0x12F688C", VA = "0x1012F688C", Slot = "44")]
		protected virtual void SelectingDisableOn()
		{
		}

		// Token: 0x06002BFB RID: 11259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600286B")]
		[Address(RVA = "0x1012F9550", Offset = "0x12F9550", VA = "0x1012F9550")]
		protected void LinkObjectNode()
		{
		}

		// Token: 0x06002BFC RID: 11260 RVA: 0x00012BB8 File Offset: 0x00010DB8
		[Token(Token = "0x600286C")]
		[Address(RVA = "0x1012FC158", Offset = "0x12FC158", VA = "0x1012FC158", Slot = "33")]
		public override bool IsCharaCheck(RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002BFD RID: 11261 RVA: 0x00012BD0 File Offset: 0x00010DD0
		[Token(Token = "0x600286D")]
		[Address(RVA = "0x1012FC3E4", Offset = "0x12FC3E4", VA = "0x1012FC3E4")]
		private bool IsPopMessage(TweetListDB pdb, int fid)
		{
			return default(bool);
		}

		// Token: 0x06002BFE RID: 11262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600286E")]
		[Address(RVA = "0x1012FC490", Offset = "0x12FC490", VA = "0x1012FC490")]
		protected void ReloadModel(int fobjid)
		{
		}

		// Token: 0x06002BFF RID: 11263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600286F")]
		[Address(RVA = "0x1012FC494", Offset = "0x12FC494", VA = "0x1012FC494")]
		public void UpGridStateGroupUp(RoomGridState pgrid, int fupstate)
		{
		}

		// Token: 0x06002C00 RID: 11264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002870")]
		[Address(RVA = "0x1012FC5A8", Offset = "0x12FC5A8", VA = "0x1012FC5A8")]
		public void UpGridStateDepth(RoomGridState pgrid, int fupstate)
		{
		}

		// Token: 0x06002C01 RID: 11265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002871")]
		[Address(RVA = "0x1012FC82C", Offset = "0x12FC82C", VA = "0x1012FC82C", Slot = "34")]
		public override void CreateHitAreaModel(bool fhitcolor)
		{
		}

		// Token: 0x06002C02 RID: 11266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002872")]
		[Address(RVA = "0x1012FD12C", Offset = "0x12FD12C", VA = "0x1012FD12C", Slot = "35")]
		public override void DestroyHitAreaModel()
		{
		}

		// Token: 0x06002C03 RID: 11267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002873")]
		[Address(RVA = "0x1012FD2B4", Offset = "0x12FD2B4", VA = "0x1012FD2B4")]
		private void CreateCharaHitView(byte fmask, int fposx, int fposy, int faddx, int faddy, int fpoint)
		{
		}

		// Token: 0x06002C04 RID: 11268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002874")]
		[Address(RVA = "0x1012FDA84", Offset = "0x12FDA84", VA = "0x1012FDA84")]
		private void DestroyCharaHit(int fpoint)
		{
		}

		// Token: 0x06002C05 RID: 11269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002875")]
		[Address(RVA = "0x1012FDBB4", Offset = "0x12FDBB4", VA = "0x1012FDBB4", Slot = "45")]
		public virtual void DrawCharaHitView(bool factive)
		{
		}

		// Token: 0x06002C06 RID: 11270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002876")]
		[Address(RVA = "0x1012E9E58", Offset = "0x12E9E58", VA = "0x1012E9E58")]
		public RoomObjectModel()
		{
		}

		// Token: 0x04003B7A RID: 15226
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002AE1")]
		protected int m_SubKeyID;

		// Token: 0x04003B7B RID: 15227
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002AE2")]
		protected GameObject m_Obj;

		// Token: 0x04003B7C RID: 15228
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002AE3")]
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04003B7D RID: 15229
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002AE4")]
		protected MsbHandler m_MsbHndl;

		// Token: 0x04003B7E RID: 15230
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002AE5")]
		protected McatFileHelper m_MabMap;

		// Token: 0x04003B7F RID: 15231
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002AE6")]
		protected int m_SelectingMode;

		// Token: 0x04003B80 RID: 15232
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002AE7")]
		private IRoomObjectControll.ModelRenderConfig[] m_DefaultConfig;

		// Token: 0x04003B81 RID: 15233
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002AE8")]
		private int m_MessageIDCheck;

		// Token: 0x04003B82 RID: 15234
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4002AE9")]
		private RoomPartsActionPakage m_Action;

		// Token: 0x04003B83 RID: 15235
		[Token(Token = "0x4002AEA")]
		public const ushort OPTION_FLG_HIT = 1;

		// Token: 0x04003B84 RID: 15236
		[Token(Token = "0x4002AEB")]
		public const ushort OPTION_FLG_LOOP_ANIM = 2;

		// Token: 0x04003B85 RID: 15237
		[Token(Token = "0x4002AEC")]
		public const ushort OPTION_FLG_OBJ_HIDE = 4;

		// Token: 0x04003B86 RID: 15238
		[Token(Token = "0x4002AED")]
		public const ushort OPTION_FLG_MESSAGE = 8;

		// Token: 0x04003B87 RID: 15239
		[Token(Token = "0x4002AEE")]
		public const int FIND_COUNT_S = 16;

		// Token: 0x04003B88 RID: 15240
		[Token(Token = "0x4002AEF")]
		public const int FIND_COUNT_M = 64;

		// Token: 0x04003B89 RID: 15241
		[Token(Token = "0x4002AF0")]
		public const int FIND_COUNT_L = 128;

		// Token: 0x04003B8A RID: 15242
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4002AF1")]
		private RoomObjectModel.RoomModelGroup[] m_DirMeshGroup;

		// Token: 0x04003B8B RID: 15243
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4002AF2")]
		protected RoomObjectModel.MarkMask m_HitActionDir;

		// Token: 0x04003B8C RID: 15244
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4002AF3")]
		protected ushort m_OptionFlag;

		// Token: 0x04003B8D RID: 15245
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4002AF4")]
		protected string[] m_InitHideObjNames;

		// Token: 0x04003B8E RID: 15246
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4002AF5")]
		protected string m_strInitAnimName;

		// Token: 0x04003B8F RID: 15247
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4002AF6")]
		protected GameObject m_HitMarkObj;

		// Token: 0x04003B90 RID: 15248
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4002AF7")]
		protected Material m_HitMarkMtl;

		// Token: 0x04003B91 RID: 15249
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4002AF8")]
		public bool m_CharaHitView;

		// Token: 0x02000A17 RID: 2583
		[Token(Token = "0x2000FAB")]
		public struct RoomModelGroup
		{
			// Token: 0x06002C07 RID: 11271 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600604A")]
			[Address(RVA = "0x1000346F4", Offset = "0x346F4", VA = "0x1000346F4")]
			public void SetView(bool fview)
			{
			}

			// Token: 0x04003B92 RID: 15250
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400640A")]
			public GameObject[] m_GroupModel;
		}

		// Token: 0x02000A18 RID: 2584
		[Token(Token = "0x2000FAC")]
		[StructLayout(2)]
		public struct MarkMask
		{
			// Token: 0x04003B93 RID: 15251
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400640B")]
			public byte m_Down;

			// Token: 0x04003B94 RID: 15252
			[Cpp2IlInjected.FieldOffset(Offset = "0x1")]
			[Token(Token = "0x400640C")]
			public byte m_Right;

			// Token: 0x04003B95 RID: 15253
			[Cpp2IlInjected.FieldOffset(Offset = "0x2")]
			[Token(Token = "0x400640D")]
			public byte m_Up;

			// Token: 0x04003B96 RID: 15254
			[Cpp2IlInjected.FieldOffset(Offset = "0x3")]
			[Token(Token = "0x400640E")]
			public byte m_Left;

			// Token: 0x04003B97 RID: 15255
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400640F")]
			public uint m_Mask;
		}
	}
}
