﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A60 RID: 2656
	[Token(Token = "0x2000764")]
	[StructLayout(3)]
	public class RoomObjectOption
	{
		// Token: 0x06002DAA RID: 11690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029F7")]
		[Address(RVA = "0x1012FDD2C", Offset = "0x12FDD2C", VA = "0x1012FDD2C")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06002DAB RID: 11691 RVA: 0x000137D0 File Offset: 0x000119D0
		[Token(Token = "0x60029F8")]
		[Address(RVA = "0x1012FDDF4", Offset = "0x12FDDF4", VA = "0x1012FDDF4")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06002DAC RID: 11692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029F9")]
		[Address(RVA = "0x1012FDE54", Offset = "0x12FDE54", VA = "0x1012FDE54")]
		public static void RoomObjectDatabaseRelease()
		{
		}

		// Token: 0x06002DAD RID: 11693 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029FA")]
		[Address(RVA = "0x1012FBFF0", Offset = "0x12FBFF0", VA = "0x1012FBFF0")]
		public static RoomObjectOption.RoomOptionTool GetRoomObjectOption(uint fkeyid)
		{
			return null;
		}

		// Token: 0x06002DAE RID: 11694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029FB")]
		[Address(RVA = "0x1012FDEBC", Offset = "0x12FDEBC", VA = "0x1012FDEBC")]
		public RoomObjectOption()
		{
		}

		// Token: 0x04003D3A RID: 15674
		[Token(Token = "0x4002C0F")]
		public static RoomObjectOption.RoomObjecOptiontDataList ms_RoomObjectOptionListDB;

		// Token: 0x02000A61 RID: 2657
		[Token(Token = "0x2000FC9")]
		public struct RoomObjOptionKey
		{
			// Token: 0x04003D3B RID: 15675
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006497")]
			public byte m_Key;

			// Token: 0x04003D3C RID: 15676
			[Cpp2IlInjected.FieldOffset(Offset = "0x1")]
			[Token(Token = "0x4006498")]
			public byte m_MakeID;

			// Token: 0x04003D3D RID: 15677
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4006499")]
			public float m_PosX;

			// Token: 0x04003D3E RID: 15678
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400649A")]
			public float m_PosY;
		}

		// Token: 0x02000A62 RID: 2658
		[Token(Token = "0x2000FCA")]
		public class RoomOptionTool
		{
			// Token: 0x06002DAF RID: 11695 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600606C")]
			[Address(RVA = "0x1012FF7D0", Offset = "0x12FF7D0", VA = "0x1012FF7D0")]
			public RoomOptionTool()
			{
			}

			// Token: 0x04003D3F RID: 15679
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400649B")]
			public uint m_ID;

			// Token: 0x04003D40 RID: 15680
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400649C")]
			public RoomObjectOption.RoomObjOptionKey[] m_Table;
		}

		// Token: 0x02000A63 RID: 2659
		[Token(Token = "0x2000FCB")]
		public class RoomObjecOptiontDataList : IDataBaseResource
		{
			// Token: 0x06002DB0 RID: 11696 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600606D")]
			[Address(RVA = "0x1012FF450", Offset = "0x12FF450", VA = "0x1012FF450", Slot = "4")]
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
			}

			// Token: 0x06002DB1 RID: 11697 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600606E")]
			[Address(RVA = "0x1012FDDEC", Offset = "0x12FDDEC", VA = "0x1012FDDEC")]
			public RoomObjecOptiontDataList()
			{
			}

			// Token: 0x04003D41 RID: 15681
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400649D")]
			public RoomObjectOption.RoomOptionTool[] m_DataID;
		}

		// Token: 0x02000A64 RID: 2660
		[Token(Token = "0x2000FCC")]
		public class OptionPakageKey
		{
			// Token: 0x06002DB2 RID: 11698 RVA: 0x000137E8 File Offset: 0x000119E8
			[Token(Token = "0x600606F")]
			[Address(RVA = "0x1012FE1C8", Offset = "0x12FE1C8", VA = "0x1012FE1C8")]
			public int GetOptionKeyNum(RoomObjectOptionListDB pdata)
			{
				return 0;
			}

			// Token: 0x06002DB3 RID: 11699 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006070")]
			[Address(RVA = "0x1012FE508", Offset = "0x12FE508", VA = "0x1012FE508")]
			public void MakeOptionKeyData(RoomObjectOptionListDB pdata, ref RoomObjectOption.RoomOptionTool pout)
			{
			}

			// Token: 0x06002DB4 RID: 11700 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006071")]
			[Address(RVA = "0x1012FE018", Offset = "0x12FE018", VA = "0x1012FE018")]
			public OptionPakageKey()
			{
			}

			// Token: 0x04003D42 RID: 15682
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400649E")]
			public uint m_GroupID;

			// Token: 0x04003D43 RID: 15683
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400649F")]
			public List<int> m_TableID;
		}

		// Token: 0x02000A65 RID: 2661
		[Token(Token = "0x2000FCD")]
		public class OptionPakage
		{
			// Token: 0x06002DB5 RID: 11701 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006072")]
			[Address(RVA = "0x1012FDEC4", Offset = "0x12FDEC4", VA = "0x1012FDEC4")]
			public RoomObjectOption.OptionPakageKey GetKey(uint fkey)
			{
				return null;
			}

			// Token: 0x06002DB6 RID: 11702 RVA: 0x00013800 File Offset: 0x00011A00
			[Token(Token = "0x6006073")]
			[Address(RVA = "0x1012FE088", Offset = "0x12FE088", VA = "0x1012FE088")]
			public int GetKeyNum()
			{
				return 0;
			}

			// Token: 0x06002DB7 RID: 11703 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006074")]
			[Address(RVA = "0x1012FE0E8", Offset = "0x12FE0E8", VA = "0x1012FE0E8")]
			public RoomObjectOption.OptionPakageKey GetIndexToKey(int findex)
			{
				return null;
			}

			// Token: 0x06002DB8 RID: 11704 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006075")]
			[Address(RVA = "0x1012FE158", Offset = "0x12FE158", VA = "0x1012FE158")]
			public OptionPakage()
			{
			}

			// Token: 0x04003D44 RID: 15684
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064A0")]
			public List<RoomObjectOption.OptionPakageKey> m_List;
		}
	}
}
