﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A67 RID: 2663
	[Token(Token = "0x2000766")]
	[StructLayout(3)]
	public class RoomObjectOptionListDB : ScriptableObject
	{
		// Token: 0x06002DB9 RID: 11705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029FC")]
		[Address(RVA = "0x1012FF7D8", Offset = "0x12FF7D8", VA = "0x1012FF7D8")]
		public RoomObjectOptionListDB()
		{
		}

		// Token: 0x04003D54 RID: 15700
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C1F")]
		public RoomObjectOptionListDB_Param[] m_Params;
	}
}
