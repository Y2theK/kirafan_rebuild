﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A66 RID: 2662
	[Token(Token = "0x2000765")]
	[Serializable]
	[StructLayout(0, Size = 44)]
	public struct RoomObjectOptionListDB_Param
	{
		// Token: 0x04003D45 RID: 15685
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002C10")]
		public uint m_Group;

		// Token: 0x04003D46 RID: 15686
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002C11")]
		public ushort m_AttachType;

		// Token: 0x04003D47 RID: 15687
		[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
		[Token(Token = "0x4002C12")]
		public ushort m_ActiveNum;

		// Token: 0x04003D48 RID: 15688
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002C13")]
		public byte m_MakeID0;

		// Token: 0x04003D49 RID: 15689
		[Cpp2IlInjected.FieldOffset(Offset = "0x9")]
		[Token(Token = "0x4002C14")]
		public byte m_MakeID1;

		// Token: 0x04003D4A RID: 15690
		[Cpp2IlInjected.FieldOffset(Offset = "0xA")]
		[Token(Token = "0x4002C15")]
		public byte m_MakeID2;

		// Token: 0x04003D4B RID: 15691
		[Cpp2IlInjected.FieldOffset(Offset = "0xB")]
		[Token(Token = "0x4002C16")]
		public byte m_MakeID3;

		// Token: 0x04003D4C RID: 15692
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4002C17")]
		public float m_ObjOffsetX0;

		// Token: 0x04003D4D RID: 15693
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002C18")]
		public float m_ObjOffsetY0;

		// Token: 0x04003D4E RID: 15694
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002C19")]
		public float m_ObjOffsetX1;

		// Token: 0x04003D4F RID: 15695
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C1A")]
		public float m_ObjOffsetY1;

		// Token: 0x04003D50 RID: 15696
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002C1B")]
		public float m_ObjOffsetX2;

		// Token: 0x04003D51 RID: 15697
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C1C")]
		public float m_ObjOffsetY2;

		// Token: 0x04003D52 RID: 15698
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002C1D")]
		public float m_ObjOffsetX3;

		// Token: 0x04003D53 RID: 15699
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C1E")]
		public float m_ObjOffsetY3;
	}
}
