﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A19 RID: 2585
	[Token(Token = "0x200073A")]
	[StructLayout(3)]
	public class RoomObjectPickMatch : MonoBehaviour
	{
		// Token: 0x06002C08 RID: 11272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002877")]
		[Address(RVA = "0x1012FF7E0", Offset = "0x12FF7E0", VA = "0x1012FF7E0")]
		public void Appear()
		{
		}

		// Token: 0x06002C09 RID: 11273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002878")]
		[Address(RVA = "0x1012FF988", Offset = "0x12FF988", VA = "0x1012FF988")]
		public void Disappear()
		{
		}

		// Token: 0x06002C0A RID: 11274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002879")]
		[Address(RVA = "0x1012FFA94", Offset = "0x12FFA94", VA = "0x1012FFA94")]
		public void Suiside()
		{
		}

		// Token: 0x06002C0B RID: 11275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600287A")]
		[Address(RVA = "0x1012FFB60", Offset = "0x12FFB60", VA = "0x1012FFB60")]
		private void Update()
		{
		}

		// Token: 0x06002C0C RID: 11276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600287B")]
		[Address(RVA = "0x101300000", Offset = "0x1300000", VA = "0x101300000")]
		public RoomObjectPickMatch()
		{
		}

		// Token: 0x04003B98 RID: 15256
		[Token(Token = "0x4002AF9")]
		private const int MATCH_ACTION_IDLE = 0;

		// Token: 0x04003B99 RID: 15257
		[Token(Token = "0x4002AFA")]
		private const int MATCH_ACTION_CATCH = 1;

		// Token: 0x04003B9A RID: 15258
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002AFB")]
		[SerializeField]
		private MeshRenderer[] m_meshes;

		// Token: 0x04003B9B RID: 15259
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002AFC")]
		[SerializeField]
		private AnimationCurve m_curveAppaerFade;

		// Token: 0x04003B9C RID: 15260
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002AFD")]
		[SerializeField]
		private AnimationCurve m_curveDisappaerFade;

		// Token: 0x04003B9D RID: 15261
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002AFE")]
		[SerializeField]
		private AnimationCurve m_curveDisappaerMoveY;

		// Token: 0x04003B9E RID: 15262
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002AFF")]
		[SerializeField]
		private float m_appearTime;

		// Token: 0x04003B9F RID: 15263
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002B00")]
		[SerializeField]
		private float m_disappearTime;

		// Token: 0x04003BA0 RID: 15264
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002B01")]
		private RoomObjectPickMatch.eState m_State;

		// Token: 0x04003BA1 RID: 15265
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002B02")]
		private float m_timer;

		// Token: 0x04003BA2 RID: 15266
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002B03")]
		private Vector3 m_basePos;

		// Token: 0x02000A1A RID: 2586
		[Token(Token = "0x2000FAD")]
		private enum eState
		{
			// Token: 0x04003BA4 RID: 15268
			[Token(Token = "0x4006411")]
			None = -1,
			// Token: 0x04003BA5 RID: 15269
			[Token(Token = "0x4006412")]
			Appaer,
			// Token: 0x04003BA6 RID: 15270
			[Token(Token = "0x4006413")]
			Disappear
		}
	}
}
