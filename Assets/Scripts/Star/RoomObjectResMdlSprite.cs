﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000A1D RID: 2589
	[Token(Token = "0x200073D")]
	[StructLayout(3)]
	public class RoomObjectResMdlSprite : IRoomObjectResource
	{
		// Token: 0x06002C1D RID: 11293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600288C")]
		[Address(RVA = "0x101300020", Offset = "0x1300020", VA = "0x101300020", Slot = "6")]
		public override void UpdateRes()
		{
		}

		// Token: 0x06002C1E RID: 11294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600288D")]
		[Address(RVA = "0x101300184", Offset = "0x1300184", VA = "0x101300184", Slot = "4")]
		public override void Setup(IRoomObjectControll hndl)
		{
		}

		// Token: 0x06002C1F RID: 11295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600288E")]
		[Address(RVA = "0x101300274", Offset = "0x1300274", VA = "0x101300274", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06002C20 RID: 11296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600288F")]
		[Address(RVA = "0x1013002A8", Offset = "0x13002A8", VA = "0x1013002A8", Slot = "5")]
		public override void Prepare()
		{
		}

		// Token: 0x06002C21 RID: 11297 RVA: 0x00012C60 File Offset: 0x00010E60
		[Token(Token = "0x6002890")]
		[Address(RVA = "0x101300050", Offset = "0x1300050", VA = "0x101300050")]
		private bool PreparingSprite()
		{
			return default(bool);
		}

		// Token: 0x06002C22 RID: 11298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002891")]
		[Address(RVA = "0x1012F75AC", Offset = "0x12F75AC", VA = "0x1012F75AC")]
		public RoomObjectResMdlSprite()
		{
		}

		// Token: 0x04003BAE RID: 15278
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B0B")]
		private RoomObjectMdlSprite m_Owner;

		// Token: 0x04003BAF RID: 15279
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B0C")]
		private MeigeResource.Handler m_SpriteHndl;
	}
}
