﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A1E RID: 2590
	[Token(Token = "0x200073E")]
	[StructLayout(3)]
	public class RoomObjectResModel : IRoomObjectResource
	{
		// Token: 0x06002C23 RID: 11299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002892")]
		[Address(RVA = "0x10130039C", Offset = "0x130039C", VA = "0x10130039C", Slot = "6")]
		public override void UpdateRes()
		{
		}

		// Token: 0x06002C24 RID: 11300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002893")]
		[Address(RVA = "0x101300524", Offset = "0x1300524", VA = "0x101300524")]
		private void UpdateDestroyPoc()
		{
		}

		// Token: 0x06002C25 RID: 11301 RVA: 0x00012C78 File Offset: 0x00010E78
		[Token(Token = "0x6002894")]
		[Address(RVA = "0x101300598", Offset = "0x1300598", VA = "0x101300598", Slot = "8")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06002C26 RID: 11302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002895")]
		[Address(RVA = "0x1013005A8", Offset = "0x13005A8", VA = "0x1013005A8", Slot = "4")]
		public override void Setup(IRoomObjectControll hndl)
		{
		}

		// Token: 0x06002C27 RID: 11303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002896")]
		[Address(RVA = "0x101300698", Offset = "0x1300698", VA = "0x101300698", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06002C28 RID: 11304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002897")]
		[Address(RVA = "0x1013006DC", Offset = "0x13006DC", VA = "0x1013006DC", Slot = "5")]
		public override void Prepare()
		{
		}

		// Token: 0x06002C29 RID: 11305 RVA: 0x00012C90 File Offset: 0x00010E90
		[Token(Token = "0x6002898")]
		[Address(RVA = "0x1013003D4", Offset = "0x13003D4", VA = "0x1013003D4")]
		private bool PreparingModel()
		{
			return default(bool);
		}

		// Token: 0x06002C2A RID: 11306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002899")]
		[Address(RVA = "0x101300778", Offset = "0x1300778", VA = "0x101300778")]
		private void SetUpBaseAnimation(GameObject pobj)
		{
		}

		// Token: 0x06002C2B RID: 11307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600289A")]
		[Address(RVA = "0x1012F84A0", Offset = "0x12F84A0", VA = "0x1012F84A0")]
		public RoomObjectResModel()
		{
		}

		// Token: 0x04003BB0 RID: 15280
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B0D")]
		private RoomObjectModel m_Owner;

		// Token: 0x04003BB1 RID: 15281
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B0E")]
		private MeigeResource.Handler m_ModelHndl;

		// Token: 0x04003BB2 RID: 15282
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B0F")]
		private RoomObjectResModel.eDestroyState m_destroyState;

		// Token: 0x02000A1F RID: 2591
		[Token(Token = "0x2000FAE")]
		private enum eDestroyState
		{
			// Token: 0x04003BB4 RID: 15284
			[Token(Token = "0x4006415")]
			State_None,
			// Token: 0x04003BB5 RID: 15285
			[Token(Token = "0x4006416")]
			State_DisableAutoRetry,
			// Token: 0x04003BB6 RID: 15286
			[Token(Token = "0x4006417")]
			State_WaitDone,
			// Token: 0x04003BB7 RID: 15287
			[Token(Token = "0x4006418")]
			State_Release,
			// Token: 0x04003BB8 RID: 15288
			[Token(Token = "0x4006419")]
			State_Complated
		}
	}
}
