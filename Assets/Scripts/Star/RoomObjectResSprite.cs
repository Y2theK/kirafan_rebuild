﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000A20 RID: 2592
	[Token(Token = "0x200073F")]
	[StructLayout(3)]
	public class RoomObjectResSprite : IRoomObjectResource
	{
		// Token: 0x06002C2C RID: 11308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600289B")]
		[Address(RVA = "0x101300A44", Offset = "0x1300A44", VA = "0x101300A44", Slot = "6")]
		public override void UpdateRes()
		{
		}

		// Token: 0x06002C2D RID: 11309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600289C")]
		[Address(RVA = "0x101300B84", Offset = "0x1300B84", VA = "0x101300B84", Slot = "4")]
		public override void Setup(IRoomObjectControll hndl)
		{
		}

		// Token: 0x06002C2E RID: 11310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600289D")]
		[Address(RVA = "0x101300C74", Offset = "0x1300C74", VA = "0x101300C74", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06002C2F RID: 11311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600289E")]
		[Address(RVA = "0x101300CA4", Offset = "0x1300CA4", VA = "0x101300CA4", Slot = "5")]
		public override void Prepare()
		{
		}

		// Token: 0x06002C30 RID: 11312 RVA: 0x00012CA8 File Offset: 0x00010EA8
		[Token(Token = "0x600289F")]
		[Address(RVA = "0x101300A88", Offset = "0x1300A88", VA = "0x101300A88")]
		private bool PreparingSprite()
		{
			return default(bool);
		}

		// Token: 0x06002C31 RID: 11313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60028A0")]
		[Address(RVA = "0x101300E98", Offset = "0x1300E98", VA = "0x101300E98")]
		public RoomObjectResSprite()
		{
		}

		// Token: 0x04003BB9 RID: 15289
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B10")]
		private RoomObjectSprite m_Owner;

		// Token: 0x04003BBA RID: 15290
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B11")]
		private MeigeResource.Handler m_SpriteHndl;

		// Token: 0x04003BBB RID: 15291
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002B12")]
		private RoomObjectResSprite.ePrepareStep m_PrepareStep;

		// Token: 0x02000A21 RID: 2593
		[Token(Token = "0x2000FAF")]
		private enum ePrepareStep
		{
			// Token: 0x04003BBD RID: 15293
			[Token(Token = "0x400641B")]
			None = -1,
			// Token: 0x04003BBE RID: 15294
			[Token(Token = "0x400641C")]
			Sprite,
			// Token: 0x04003BBF RID: 15295
			[Token(Token = "0x400641D")]
			Num
		}
	}
}
