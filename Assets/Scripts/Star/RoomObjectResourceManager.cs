﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A35 RID: 2613
	[Token(Token = "0x2000749")]
	[StructLayout(3)]
	public sealed class RoomObjectResourceManager
	{
		// Token: 0x06002CC9 RID: 11465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002937")]
		[Address(RVA = "0x101300EA8", Offset = "0x1300EA8", VA = "0x101300EA8")]
		public RoomObjectResourceManager()
		{
		}

		// Token: 0x06002CCA RID: 11466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002938")]
		[Address(RVA = "0x101301060", Offset = "0x1301060", VA = "0x101301060")]
		public void Update()
		{
		}

		// Token: 0x06002CCB RID: 11467 RVA: 0x00013098 File Offset: 0x00011298
		[Token(Token = "0x6002939")]
		[Address(RVA = "0x1013010E0", Offset = "0x13010E0", VA = "0x1013010E0")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06002CCC RID: 11468 RVA: 0x000130B0 File Offset: 0x000112B0
		[Token(Token = "0x600293A")]
		[Address(RVA = "0x1013010E8", Offset = "0x13010E8", VA = "0x1013010E8")]
		public bool IsLoadingAny()
		{
			return default(bool);
		}

		// Token: 0x06002CCD RID: 11469 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600293B")]
		[Address(RVA = "0x101301180", Offset = "0x1301180", VA = "0x101301180")]
		public ResourceObjectHandler LoadAsyncFromResources(RoomObjectResourceManager.eType type, string path)
		{
			return null;
		}

		// Token: 0x06002CCE RID: 11470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600293C")]
		[Address(RVA = "0x10130123C", Offset = "0x130123C", VA = "0x10130123C")]
		public void UnloadFromResources(RoomObjectResourceManager.eType type, string path)
		{
		}

		// Token: 0x06002CCF RID: 11471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600293D")]
		[Address(RVA = "0x1013012B4", Offset = "0x13012B4", VA = "0x1013012B4")]
		public void UnloadAllResources(RoomObjectResourceManager.eType type)
		{
		}

		// Token: 0x06002CD0 RID: 11472 RVA: 0x000130C8 File Offset: 0x000112C8
		[Token(Token = "0x600293E")]
		[Address(RVA = "0x101301324", Offset = "0x1301324", VA = "0x101301324")]
		public bool IsCompleteUnloadAllResources(RoomObjectResourceManager.eType type)
		{
			return default(bool);
		}

		// Token: 0x06002CD1 RID: 11473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600293F")]
		[Address(RVA = "0x101301394", Offset = "0x1301394", VA = "0x101301394")]
		public void UnloadAllResources()
		{
		}

		// Token: 0x06002CD2 RID: 11474 RVA: 0x000130E0 File Offset: 0x000112E0
		[Token(Token = "0x6002940")]
		[Address(RVA = "0x10130142C", Offset = "0x130142C", VA = "0x10130142C")]
		public bool IsCompleteUnloadAllResources()
		{
			return default(bool);
		}

		// Token: 0x04003C79 RID: 15481
		[Token(Token = "0x4002B97")]
		public const int TYPE_NUM = 3;

		// Token: 0x04003C7A RID: 15482
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002B98")]
		private readonly int[] SIMULTANEOUSLY_LOAD_NUMS;

		// Token: 0x04003C7B RID: 15483
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002B99")]
		private readonly ResourceObjectHandler.eLoadType[] LOAD_TYPES;

		// Token: 0x04003C7C RID: 15484
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002B9A")]
		private ResourcesLoader[] m_Loaders;

		// Token: 0x02000A36 RID: 2614
		[Token(Token = "0x2000FBA")]
		public enum eType
		{
			// Token: 0x04003C7E RID: 15486
			[Token(Token = "0x4006454")]
			Sprite,
			// Token: 0x04003C7F RID: 15487
			[Token(Token = "0x4006455")]
			Model,
			// Token: 0x04003C80 RID: 15488
			[Token(Token = "0x4006456")]
			Anim,
			// Token: 0x04003C81 RID: 15489
			[Token(Token = "0x4006457")]
			Num
		}
	}
}
