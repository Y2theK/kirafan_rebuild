﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A1B RID: 2587
	[Token(Token = "0x200073B")]
	[StructLayout(3)]
	public class RoomObjectSprite : IRoomObjectControll
	{
		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06002C0D RID: 11277 RVA: 0x00012BE8 File Offset: 0x00010DE8
		[Token(Token = "0x170002BE")]
		public int SubKeyID
		{
			[Token(Token = "0x600287C")]
			[Address(RVA = "0x101300D9C", Offset = "0x1300D9C", VA = "0x101300D9C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06002C0E RID: 11278 RVA: 0x00012C00 File Offset: 0x00010E00
		[Token(Token = "0x170002BF")]
		public int SubKeyID2
		{
			[Token(Token = "0x600287D")]
			[Address(RVA = "0x101300DA4", Offset = "0x1300DA4", VA = "0x101300DA4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06002C0F RID: 11279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600287E")]
		[Address(RVA = "0x1012E8398", Offset = "0x12E8398", VA = "0x1012E8398")]
		public void SetupSimple(eRoomObjectCategory category, int objID, int fsubkey, int fsubkey2, CharacterDefine.eDir dir, int posX, int posY, int defaultSizeX, int defaultSizeY)
		{
		}

		// Token: 0x06002C10 RID: 11280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600287F")]
		[Address(RVA = "0x1013014D0", Offset = "0x13014D0", VA = "0x1013014D0", Slot = "22")]
		public override void Destroy()
		{
		}

		// Token: 0x06002C11 RID: 11281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002880")]
		[Address(RVA = "0x101300DAC", Offset = "0x1300DAC", VA = "0x101300DAC")]
		public void AttachSprite(Sprite sprite)
		{
		}

		// Token: 0x06002C12 RID: 11282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002881")]
		[Address(RVA = "0x10130156C", Offset = "0x130156C", VA = "0x10130156C")]
		public void SortingOrderSprite(float flayerpos, int sortingOrder, bool isLinked)
		{
		}

		// Token: 0x06002C13 RID: 11283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002882")]
		[Address(RVA = "0x101301618", Offset = "0x1301618", VA = "0x101301618")]
		public RoomObjectSprite()
		{
		}

		// Token: 0x04003BA7 RID: 15271
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4002B04")]
		protected int m_SubKeyID;

		// Token: 0x04003BA8 RID: 15272
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002B05")]
		protected int m_SubKeyID2;

		// Token: 0x04003BA9 RID: 15273
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4002B06")]
		private SpriteRenderer m_SpriteRender;

		// Token: 0x04003BAA RID: 15274
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002B07")]
		protected int m_SelectingMode;

		// Token: 0x04003BAB RID: 15275
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4002B08")]
		private short m_DefaultSpriteSortID;
	}
}
