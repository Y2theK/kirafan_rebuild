﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A68 RID: 2664
	[Token(Token = "0x2000767")]
	[StructLayout(3)]
	public class RoomPartsActionPakage
	{
		// Token: 0x06002DBA RID: 11706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029FD")]
		[Address(RVA = "0x1012F75B4", Offset = "0x12F75B4", VA = "0x1012F75B4")]
		public RoomPartsActionPakage()
		{
		}

		// Token: 0x06002DBB RID: 11707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60029FE")]
		[Address(RVA = "0x1012F8244", Offset = "0x12F8244", VA = "0x1012F8244")]
		public void EntryAction(IRoomPartsAction paction, uint fuid = 0U)
		{
		}

		// Token: 0x06002DBC RID: 11708 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60029FF")]
		[Address(RVA = "0x101301620", Offset = "0x1301620", VA = "0x101301620")]
		public IRoomPartsAction GetAction(uint fuid)
		{
			return null;
		}

		// Token: 0x06002DBD RID: 11709 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A00")]
		[Address(RVA = "0x1012FBB18", Offset = "0x12FBB18", VA = "0x1012FBB18")]
		public IRoomPartsAction GetAction(Type ftype)
		{
			return null;
		}

		// Token: 0x06002DBE RID: 11710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A01")]
		[Address(RVA = "0x1012F7644", Offset = "0x12F7644", VA = "0x1012F7644")]
		public void PakageUpdate()
		{
		}

		// Token: 0x06002DBF RID: 11711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A02")]
		[Address(RVA = "0x1013016F8", Offset = "0x13016F8", VA = "0x1013016F8")]
		public void CancelAction()
		{
		}

		// Token: 0x06002DC0 RID: 11712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A03")]
		[Address(RVA = "0x101301800", Offset = "0x1301800", VA = "0x101301800")]
		public void Release()
		{
		}

		// Token: 0x04003D55 RID: 15701
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002C20")]
		public int m_Num;

		// Token: 0x04003D56 RID: 15702
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C21")]
		public IRoomPartsAction[] m_Table;
	}
}
