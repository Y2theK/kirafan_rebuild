﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A69 RID: 2665
	[Token(Token = "0x2000768")]
	[StructLayout(3)]
	public class RoomPartsBind : IRoomPartsAction
	{
		// Token: 0x06002DC1 RID: 11713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A04")]
		[Address(RVA = "0x1013018EC", Offset = "0x13018EC", VA = "0x1013018EC")]
		public RoomPartsBind(Transform pbase, Transform ptarget, bool freverse)
		{
		}

		// Token: 0x06002DC2 RID: 11714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A05")]
		[Address(RVA = "0x101301AAC", Offset = "0x1301AAC", VA = "0x101301AAC")]
		public void SetRoomObjectControll(IRoomObjectControll baseObj, IRoomObjectControll targetObj)
		{
		}

		// Token: 0x06002DC3 RID: 11715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A06")]
		[Address(RVA = "0x101301BA4", Offset = "0x1301BA4", VA = "0x101301BA4")]
		public RoomPartsBind(Transform pbase, Transform ptarget)
		{
		}

		// Token: 0x06002DC4 RID: 11716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A07")]
		[Address(RVA = "0x101301C90", Offset = "0x1301C90", VA = "0x101301C90")]
		public void SetOffsetAnime(Vector3 foffsetbase, Vector3 foffsettarget, float ftime)
		{
		}

		// Token: 0x06002DC5 RID: 11717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A08")]
		[Address(RVA = "0x101301CB4", Offset = "0x1301CB4", VA = "0x101301CB4")]
		public void ReverseOffsetAnime(float ftime)
		{
		}

		// Token: 0x06002DC6 RID: 11718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A09")]
		[Address(RVA = "0x101301DA4", Offset = "0x1301DA4", VA = "0x101301DA4")]
		public void SetRotationOffset(Quaternion frot)
		{
		}

		// Token: 0x06002DC7 RID: 11719 RVA: 0x00013818 File Offset: 0x00011A18
		[Token(Token = "0x6002A0A")]
		[Address(RVA = "0x101301E30", Offset = "0x1301E30", VA = "0x101301E30", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04003D57 RID: 15703
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C22")]
		public Transform m_Target;

		// Token: 0x04003D58 RID: 15704
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C23")]
		public Transform m_Base;

		// Token: 0x04003D59 RID: 15705
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C24")]
		public Vector3 m_OffsetBase;

		// Token: 0x04003D5A RID: 15706
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002C25")]
		public Vector3 m_OffsetTarget;

		// Token: 0x04003D5B RID: 15707
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C26")]
		public Vector3 m_BackPos;

		// Token: 0x04003D5C RID: 15708
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002C27")]
		public float m_Time;

		// Token: 0x04003D5D RID: 15709
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C28")]
		public float m_MaxTime;

		// Token: 0x04003D5E RID: 15710
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002C29")]
		public byte m_CalcType;

		// Token: 0x04003D5F RID: 15711
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002C2A")]
		public Vector3 m_OffsetRot;

		// Token: 0x04003D60 RID: 15712
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002C2B")]
		public Vector3 m_MarkRot;

		// Token: 0x04003D61 RID: 15713
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002C2C")]
		public bool m_RotCalc;

		// Token: 0x04003D62 RID: 15714
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002C2D")]
		public IRoomObjectControll m_BaseControll;

		// Token: 0x04003D63 RID: 15715
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002C2E")]
		public IRoomObjectControll m_TargetControll;

		// Token: 0x04003D64 RID: 15716
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002C2F")]
		public int m_TargetRenderConfigIdx;
	}
}
