﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A6A RID: 2666
	[Token(Token = "0x2000769")]
	[StructLayout(3)]
	public class RoomPartsBoundScale : IRoomPartsAction
	{
		// Token: 0x06002DC8 RID: 11720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A0B")]
		[Address(RVA = "0x1012FD09C", Offset = "0x12FD09C", VA = "0x1012FD09C")]
		public RoomPartsBoundScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
		}

		// Token: 0x06002DC9 RID: 11721 RVA: 0x00013830 File Offset: 0x00011A30
		[Token(Token = "0x6002A0C")]
		[Address(RVA = "0x1013024E8", Offset = "0x13024E8", VA = "0x1013024E8", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04003D65 RID: 15717
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C30")]
		private Transform m_Marker;

		// Token: 0x04003D66 RID: 15718
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C31")]
		private Vector3 m_Target;

		// Token: 0x04003D67 RID: 15719
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002C32")]
		private Vector3 m_Base;

		// Token: 0x04003D68 RID: 15720
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C33")]
		private float m_Time;

		// Token: 0x04003D69 RID: 15721
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002C34")]
		private float m_MaxTime;
	}
}
