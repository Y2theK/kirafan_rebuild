﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A6B RID: 2667
	[Token(Token = "0x200076A")]
	[StructLayout(3)]
	public class RoomPartsMove : IRoomPartsAction
	{
		// Token: 0x06002DCA RID: 11722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A0D")]
		[Address(RVA = "0x1012FD008", Offset = "0x12FD008", VA = "0x1012FD008")]
		public RoomPartsMove(Transform ptrs, Vector3 fbasepos, Vector3 ftargetpos, float ftime, RoomPartsMove.ePathType ftype = RoomPartsMove.ePathType.Linear)
		{
		}

		// Token: 0x06002DCB RID: 11723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A0E")]
		[Address(RVA = "0x1013026A8", Offset = "0x13026A8", VA = "0x1013026A8")]
		public RoomPartsMove(Transform ptrs, Vector3 fbasepos, Vector3 fpoint1, Vector3 fpoint2, Vector3 ftargetpos, float ftime)
		{
		}

		// Token: 0x06002DCC RID: 11724 RVA: 0x00013848 File Offset: 0x00011A48
		[Token(Token = "0x6002A0F")]
		[Address(RVA = "0x10130276C", Offset = "0x130276C", VA = "0x10130276C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04003D6A RID: 15722
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C35")]
		private Transform m_Target;

		// Token: 0x04003D6B RID: 15723
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C36")]
		private Vector3 m_BasePos;

		// Token: 0x04003D6C RID: 15724
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002C37")]
		private Vector3 m_TargetPos;

		// Token: 0x04003D6D RID: 15725
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C38")]
		private Vector3 m_Point1;

		// Token: 0x04003D6E RID: 15726
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C39")]
		private Vector3 m_Point2;

		// Token: 0x04003D6F RID: 15727
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002C3A")]
		private float m_Time;

		// Token: 0x04003D70 RID: 15728
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002C3B")]
		private float m_MaxTime;

		// Token: 0x04003D71 RID: 15729
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002C3C")]
		private RoomPartsMove.ePathType m_LineType;

		// Token: 0x02000A6C RID: 2668
		[Token(Token = "0x2000FCE")]
		public enum ePathType
		{
			// Token: 0x04003D73 RID: 15731
			[Token(Token = "0x40064A2")]
			Linear,
			// Token: 0x04003D74 RID: 15732
			[Token(Token = "0x40064A3")]
			Bez,
			// Token: 0x04003D75 RID: 15733
			[Token(Token = "0x40064A4")]
			LocalLinear
		}
	}
}
