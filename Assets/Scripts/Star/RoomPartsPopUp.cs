﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;

namespace Star
{
	// Token: 0x02000A6D RID: 2669
	[Token(Token = "0x200076B")]
	[StructLayout(3)]
	public class RoomPartsPopUp : IRoomPartsAction
	{
		// Token: 0x06002DCD RID: 11725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A10")]
		[Address(RVA = "0x101302B00", Offset = "0x1302B00", VA = "0x101302B00")]
		public RoomPartsPopUp(RoomCharaHud popup, float ftime, int fvoicelink = -1)
		{
		}

		// Token: 0x06002DCE RID: 11726 RVA: 0x00013860 File Offset: 0x00011A60
		[Token(Token = "0x6002A11")]
		[Address(RVA = "0x101302B58", Offset = "0x1302B58", VA = "0x101302B58", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x06002DCF RID: 11727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A12")]
		[Address(RVA = "0x101302C98", Offset = "0x1302C98", VA = "0x101302C98", Slot = "5")]
		public override void PartsCancel()
		{
		}

		// Token: 0x04003D76 RID: 15734
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C3D")]
		public RoomCharaHud m_PopUp;

		// Token: 0x04003D77 RID: 15735
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C3E")]
		public float m_Time;

		// Token: 0x04003D78 RID: 15736
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002C3F")]
		public float m_MaxTime;

		// Token: 0x04003D79 RID: 15737
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C40")]
		public int m_VoiceLink;
	}
}
