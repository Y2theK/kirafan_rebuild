﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A6E RID: 2670
	[Token(Token = "0x200076C")]
	[StructLayout(3)]
	public class RoomPartsPosAnime : IRoomPartsAction
	{
		// Token: 0x06002DD0 RID: 11728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A13")]
		[Address(RVA = "0x101302DAC", Offset = "0x1302DAC", VA = "0x101302DAC")]
		public RoomPartsPosAnime(Transform ptrs, Vector3 fbasepos, Vector3 ftargetpos, float ftime)
		{
		}

		// Token: 0x06002DD1 RID: 11729 RVA: 0x00013878 File Offset: 0x00011A78
		[Token(Token = "0x6002A14")]
		[Address(RVA = "0x101302E3C", Offset = "0x1302E3C", VA = "0x101302E3C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04003D7A RID: 15738
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C41")]
		private Transform m_Target;

		// Token: 0x04003D7B RID: 15739
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C42")]
		private Vector3 m_BasePos;

		// Token: 0x04003D7C RID: 15740
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002C43")]
		private Vector3 m_TargetPos;

		// Token: 0x04003D7D RID: 15741
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002C44")]
		private float m_Time;

		// Token: 0x04003D7E RID: 15742
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002C45")]
		private float m_MaxTime;
	}
}
