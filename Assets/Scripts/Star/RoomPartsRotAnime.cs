﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A6F RID: 2671
	[Token(Token = "0x200076D")]
	[StructLayout(3)]
	public class RoomPartsRotAnime : IRoomPartsAction
	{
		// Token: 0x06002DD2 RID: 11730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A15")]
		[Address(RVA = "0x101302F9C", Offset = "0x1302F9C", VA = "0x101302F9C")]
		public RoomPartsRotAnime(Transform ptrs, Quaternion fbaserot, Quaternion ftargetrot, float ftime)
		{
		}

		// Token: 0x06002DD3 RID: 11731 RVA: 0x00013890 File Offset: 0x00011A90
		[Token(Token = "0x6002A16")]
		[Address(RVA = "0x101303038", Offset = "0x1303038", VA = "0x101303038", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04003D7F RID: 15743
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C46")]
		private Transform m_Target;

		// Token: 0x04003D80 RID: 15744
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C47")]
		private Quaternion m_BaseRot;

		// Token: 0x04003D81 RID: 15745
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C48")]
		private Quaternion m_TargetRot;

		// Token: 0x04003D82 RID: 15746
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C49")]
		private float m_Time;

		// Token: 0x04003D83 RID: 15747
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C4A")]
		private float m_MaxTime;
	}
}
