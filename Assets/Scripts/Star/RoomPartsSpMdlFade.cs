﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A70 RID: 2672
	[Token(Token = "0x200076E")]
	[StructLayout(3)]
	public class RoomPartsSpMdlFade : IRoomPartsAction
	{
		// Token: 0x06002DD4 RID: 11732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A17")]
		[Address(RVA = "0x1012F8138", Offset = "0x12F8138", VA = "0x1012F8138")]
		public RoomPartsSpMdlFade(GameObject ptarget, Color fincolor, Color foutcolor, float ftime)
		{
		}

		// Token: 0x06002DD5 RID: 11733 RVA: 0x000138A8 File Offset: 0x00011AA8
		[Token(Token = "0x6002A18")]
		[Address(RVA = "0x1013032DC", Offset = "0x13032DC", VA = "0x1013032DC", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x06002DD6 RID: 11734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A19")]
		[Address(RVA = "0x1013031BC", Offset = "0x13031BC", VA = "0x1013031BC")]
		public void UpModelColor(Color fcolor)
		{
		}

		// Token: 0x04003D84 RID: 15748
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C4B")]
		private Renderer[] m_Render;

		// Token: 0x04003D85 RID: 15749
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C4C")]
		private Color m_Base;

		// Token: 0x04003D86 RID: 15750
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C4D")]
		private Color m_Target;

		// Token: 0x04003D87 RID: 15751
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C4E")]
		private float m_Time;

		// Token: 0x04003D88 RID: 15752
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C4F")]
		private float m_MaxTime;
	}
}
