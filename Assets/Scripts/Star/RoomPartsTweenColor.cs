﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A71 RID: 2673
	[Token(Token = "0x200076F")]
	[StructLayout(3)]
	public class RoomPartsTweenColor : IRoomPartsAction
	{
		// Token: 0x06002DD7 RID: 11735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A1A")]
		[Address(RVA = "0x1012FBC58", Offset = "0x12FBC58", VA = "0x1012FBC58")]
		public RoomPartsTweenColor(GameObject prender, Color fincolor, Color foutcolor, float ftime)
		{
		}

		// Token: 0x06002DD8 RID: 11736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A1B")]
		[Address(RVA = "0x1012FBFCC", Offset = "0x12FBFCC", VA = "0x1012FBFCC")]
		public void ChangeColor(Color fin, Color fout)
		{
		}

		// Token: 0x06002DD9 RID: 11737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A1C")]
		[Address(RVA = "0x1012FBC4C", Offset = "0x12FBC4C", VA = "0x1012FBC4C")]
		public void SetEndStep()
		{
		}

		// Token: 0x06002DDA RID: 11738 RVA: 0x000138C0 File Offset: 0x00011AC0
		[Token(Token = "0x6002A1D")]
		[Address(RVA = "0x1013033A4", Offset = "0x13033A4", VA = "0x1013033A4", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x06002DDB RID: 11739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A1E")]
		[Address(RVA = "0x1013034DC", Offset = "0x13034DC", VA = "0x1013034DC")]
		private void UpModelColor(Color fcolor)
		{
		}

		// Token: 0x06002DDC RID: 11740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A1F")]
		[Address(RVA = "0x1013036E0", Offset = "0x13036E0", VA = "0x1013036E0")]
		private void BackModelColor()
		{
		}

		// Token: 0x04003D89 RID: 15753
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002C50")]
		private RoomPartsTweenColor.ModelHandle[] m_MsbHandle;

		// Token: 0x04003D8A RID: 15754
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C51")]
		private Color m_Base;

		// Token: 0x04003D8B RID: 15755
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002C52")]
		private Color m_Target;

		// Token: 0x04003D8C RID: 15756
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002C53")]
		private byte m_Step;

		// Token: 0x04003D8D RID: 15757
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002C54")]
		private float m_Time;

		// Token: 0x04003D8E RID: 15758
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002C55")]
		private float m_MaxTime;

		// Token: 0x02000A72 RID: 2674
		[Token(Token = "0x2000FCF")]
		private struct ModelHandle
		{
			// Token: 0x04003D8F RID: 15759
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40064A5")]
			public MsbHandler m_Handle;

			// Token: 0x04003D90 RID: 15760
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40064A6")]
			public int m_Num;

			// Token: 0x04003D91 RID: 15761
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064A7")]
			public Color[] m_Color;
		}
	}
}
