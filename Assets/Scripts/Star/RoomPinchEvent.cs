﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200099D RID: 2461
	[Token(Token = "0x20006FD")]
	[StructLayout(3)]
	public class RoomPinchEvent
	{
		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x060028B8 RID: 10424 RVA: 0x00011238 File Offset: 0x0000F438
		// (set) Token: 0x060028B9 RID: 10425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000290")]
		public float InitialPinchDist
		{
			[Token(Token = "0x600257A")]
			[Address(RVA = "0x101303884", Offset = "0x1303884", VA = "0x101303884")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600257B")]
			[Address(RVA = "0x10130388C", Offset = "0x130388C", VA = "0x10130388C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x060028BA RID: 10426 RVA: 0x00011250 File Offset: 0x0000F450
		// (set) Token: 0x060028BB RID: 10427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000291")]
		public bool IsPinching
		{
			[Token(Token = "0x600257C")]
			[Address(RVA = "0x101303894", Offset = "0x1303894", VA = "0x101303894")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600257D")]
			[Address(RVA = "0x10130389C", Offset = "0x130389C", VA = "0x10130389C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x060028BC RID: 10428 RVA: 0x00011268 File Offset: 0x0000F468
		// (set) Token: 0x060028BD RID: 10429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000292")]
		public Vector2 FocusedPos
		{
			[Token(Token = "0x600257E")]
			[Address(RVA = "0x1013038A4", Offset = "0x13038A4", VA = "0x1013038A4")]
			[CompilerGenerated]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x600257F")]
			[Address(RVA = "0x1013038AC", Offset = "0x13038AC", VA = "0x1013038AC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x060028BE RID: 10430 RVA: 0x00011280 File Offset: 0x0000F480
		// (set) Token: 0x060028BF RID: 10431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000293")]
		public float PinchRatio
		{
			[Token(Token = "0x6002580")]
			[Address(RVA = "0x1013038B8", Offset = "0x13038B8", VA = "0x1013038B8")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6002581")]
			[Address(RVA = "0x1013038C0", Offset = "0x13038C0", VA = "0x1013038C0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060028C0 RID: 10432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002582")]
		[Address(RVA = "0x1013038C8", Offset = "0x13038C8", VA = "0x1013038C8")]
		public void UpdatePinch(RoomPinchState pstate)
		{
		}

		// Token: 0x060028C1 RID: 10433 RVA: 0x00011298 File Offset: 0x0000F498
		[Token(Token = "0x6002583")]
		[Address(RVA = "0x101303AE4", Offset = "0x1303AE4", VA = "0x101303AE4")]
		private Vector2 CalcFocusPos()
		{
			return default(Vector2);
		}

		// Token: 0x060028C2 RID: 10434 RVA: 0x000112B0 File Offset: 0x0000F4B0
		[Token(Token = "0x6002584")]
		[Address(RVA = "0x1013039AC", Offset = "0x13039AC", VA = "0x1013039AC")]
		private float CalcPinchDist()
		{
			return 0f;
		}

		// Token: 0x060028C3 RID: 10435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002585")]
		[Address(RVA = "0x101303C5C", Offset = "0x1303C5C", VA = "0x101303C5C")]
		public RoomPinchEvent()
		{
		}

		// Token: 0x04003935 RID: 14645
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400297A")]
		private float m_CurrentPinchDist;

		// Token: 0x04003936 RID: 14646
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400297B")]
		private float m_OldPinchRatio;
	}
}
