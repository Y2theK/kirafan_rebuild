﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200099C RID: 2460
	[Token(Token = "0x20006FC")]
	[StructLayout(3)]
	public class RoomPinchState
	{
		// Token: 0x060028B7 RID: 10423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002579")]
		[Address(RVA = "0x101303C64", Offset = "0x1303C64", VA = "0x101303C64")]
		public RoomPinchState()
		{
		}

		// Token: 0x04003932 RID: 14642
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002977")]
		public bool m_Event;

		// Token: 0x04003933 RID: 14643
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4002978")]
		public bool IsPinching;

		// Token: 0x04003934 RID: 14644
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002979")]
		public float PinchRatio;
	}
}
