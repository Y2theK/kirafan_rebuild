﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020006C5 RID: 1733
	[Token(Token = "0x200057C")]
	[StructLayout(3)]
	public class RoomServerForm
	{
		// Token: 0x06001964 RID: 6500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60017E0")]
		[Address(RVA = "0x101303C6C", Offset = "0x1303C6C", VA = "0x101303C6C")]
		public RoomServerForm()
		{
		}

		// Token: 0x020006C6 RID: 1734
		[Token(Token = "0x2000E1D")]
		public enum eDataType
		{
			// Token: 0x040029B6 RID: 10678
			[Token(Token = "0x4005ADB")]
			RoomList,
			// Token: 0x040029B7 RID: 10679
			[Token(Token = "0x4005ADC")]
			RoomData
		}

		// Token: 0x020006C7 RID: 1735
		[Token(Token = "0x2000E1E")]
		public class RoomData
		{
			// Token: 0x06001965 RID: 6501 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E48")]
			[Address(RVA = "0x101303C7C", Offset = "0x1303C7C", VA = "0x101303C7C")]
			public RoomData()
			{
			}

			// Token: 0x040029B8 RID: 10680
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005ADD")]
			public long m_BG;

			// Token: 0x040029B9 RID: 10681
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005ADE")]
			public long m_Wall;

			// Token: 0x040029BA RID: 10682
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005ADF")]
			public long m_Floor;

			// Token: 0x040029BB RID: 10683
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005AE0")]
			public int m_RoomID;

			// Token: 0x040029BC RID: 10684
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005AE1")]
			public List<UserRoomData.PlacementData> m_Table;
		}

		// Token: 0x020006C8 RID: 1736
		[Token(Token = "0x2000E1F")]
		public class RoomList
		{
			// Token: 0x06001966 RID: 6502 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E49")]
			[Address(RVA = "0x101303C84", Offset = "0x1303C84", VA = "0x101303C84")]
			public RoomList()
			{
			}

			// Token: 0x040029BD RID: 10685
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AE2")]
			public long m_ActiveRoomID;

			// Token: 0x040029BE RID: 10686
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AE3")]
			public long[] m_List;
		}

		// Token: 0x020006C9 RID: 1737
		[Token(Token = "0x2000E20")]
		public class DataChunk
		{
			// Token: 0x06001967 RID: 6503 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E4A")]
			[Address(RVA = "0x101303C74", Offset = "0x1303C74", VA = "0x101303C74")]
			public DataChunk()
			{
			}

			// Token: 0x040029BF RID: 10687
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005AE4")]
			public int m_Ver;

			// Token: 0x040029C0 RID: 10688
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005AE5")]
			public int m_DataType;

			// Token: 0x040029C1 RID: 10689
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005AE6")]
			public byte[] m_Data;
		}
	}
}
