﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A73 RID: 2675
	[Token(Token = "0x2000770")]
	[StructLayout(3)]
	public class RoomShadowControll
	{
		// Token: 0x06002DDD RID: 11741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A20")]
		[Address(RVA = "0x1012F0030", Offset = "0x12F0030", VA = "0x1012F0030")]
		public void SetControllTrs(Transform pshadow, Transform pnode, Transform proot, float foffset, Vector3 fsize)
		{
		}

		// Token: 0x06002DDE RID: 11742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A21")]
		[Address(RVA = "0x1012EFB48", Offset = "0x12EFB48", VA = "0x1012EFB48")]
		public void UpdateShadow(Transform pnode, float scale = 1f)
		{
		}

		// Token: 0x06002DDF RID: 11743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A22")]
		[Address(RVA = "0x1012EDB68", Offset = "0x12EDB68", VA = "0x1012EDB68")]
		public RoomShadowControll()
		{
		}

		// Token: 0x04003D92 RID: 15762
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002C56")]
		protected Transform m_ShadowHandle;

		// Token: 0x04003D93 RID: 15763
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002C57")]
		protected Vector3 m_ShadowSize;

		// Token: 0x04003D94 RID: 15764
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002C58")]
		protected Transform m_ShadowSizeTrs;

		// Token: 0x04003D95 RID: 15765
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002C59")]
		public float m_ShadowSizeKey;

		// Token: 0x04003D96 RID: 15766
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002C5A")]
		public float m_ShadowLength;
	}
}
