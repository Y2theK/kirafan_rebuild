﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200059D RID: 1437
	[Token(Token = "0x2000490")]
	[StructLayout(3)]
	public class RoomShopListDB : ScriptableObject
	{
		// Token: 0x060015F7 RID: 5623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014A7")]
		[Address(RVA = "0x101303C8C", Offset = "0x1303C8C", VA = "0x101303C8C")]
		public RoomShopListDB()
		{
		}

		// Token: 0x04001A8D RID: 6797
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013F7")]
		public RoomShopListDB_Param[] m_Params;
	}
}
