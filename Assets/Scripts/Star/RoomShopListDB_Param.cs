﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200059C RID: 1436
	[Token(Token = "0x200048F")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct RoomShopListDB_Param
	{
		// Token: 0x04001A86 RID: 6790
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40013F0")]
		public int m_Category;

		// Token: 0x04001A87 RID: 6791
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40013F1")]
		public int m_ID;

		// Token: 0x04001A88 RID: 6792
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40013F2")]
		public int m_BuyPrice;

		// Token: 0x04001A89 RID: 6793
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40013F3")]
		public int m_SellPrice;

		// Token: 0x04001A8A RID: 6794
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40013F4")]
		public string m_BaseDescription;

		// Token: 0x04001A8B RID: 6795
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40013F5")]
		public string m_BufDescription;

		// Token: 0x04001A8C RID: 6796
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40013F6")]
		public ushort m_ListUpID;
	}
}
