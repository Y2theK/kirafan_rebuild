﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000821 RID: 2081
	[Token(Token = "0x200061E")]
	[StructLayout(3)]
	public class RoomState : GameStateBase
	{
		// Token: 0x060020F0 RID: 8432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E56")]
		[Address(RVA = "0x101303C94", Offset = "0x1303C94", VA = "0x101303C94")]
		public RoomState(RoomMain owner)
		{
		}

		// Token: 0x060020F1 RID: 8433 RVA: 0x0000E640 File Offset: 0x0000C840
		[Token(Token = "0x6001E57")]
		[Address(RVA = "0x101303CC8", Offset = "0x1303CC8", VA = "0x101303CC8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060020F2 RID: 8434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E58")]
		[Address(RVA = "0x101303CD0", Offset = "0x1303CD0", VA = "0x101303CD0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060020F3 RID: 8435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E59")]
		[Address(RVA = "0x101303CD4", Offset = "0x1303CD4", VA = "0x101303CD4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060020F4 RID: 8436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E5A")]
		[Address(RVA = "0x101303CD8", Offset = "0x1303CD8", VA = "0x101303CD8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x0000E658 File Offset: 0x0000C858
		[Token(Token = "0x6001E5B")]
		[Address(RVA = "0x101303CDC", Offset = "0x1303CDC", VA = "0x101303CDC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E5C")]
		[Address(RVA = "0x101303CE4", Offset = "0x1303CE4", VA = "0x101303CE4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040030F2 RID: 12530
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002515")]
		protected RoomMain m_Owner;

		// Token: 0x040030F3 RID: 12531
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002516")]
		protected int m_NextState;
	}
}
