﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000822 RID: 2082
	[Token(Token = "0x200061F")]
	[StructLayout(3)]
	public class RoomState_Init : RoomState
	{
		// Token: 0x060020F7 RID: 8439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E5D")]
		[Address(RVA = "0x101303CE8", Offset = "0x1303CE8", VA = "0x101303CE8")]
		public RoomState_Init(RoomMain owner)
		{
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x0000E670 File Offset: 0x0000C870
		[Token(Token = "0x6001E5E")]
		[Address(RVA = "0x101303D1C", Offset = "0x1303D1C", VA = "0x101303D1C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060020F9 RID: 8441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E5F")]
		[Address(RVA = "0x101303D24", Offset = "0x1303D24", VA = "0x101303D24", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060020FA RID: 8442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E60")]
		[Address(RVA = "0x101303D2C", Offset = "0x1303D2C", VA = "0x101303D2C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060020FB RID: 8443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E61")]
		[Address(RVA = "0x101303D30", Offset = "0x1303D30", VA = "0x101303D30", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060020FC RID: 8444 RVA: 0x0000E688 File Offset: 0x0000C888
		[Token(Token = "0x6001E62")]
		[Address(RVA = "0x101303D34", Offset = "0x1303D34", VA = "0x101303D34", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060020FD RID: 8445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E63")]
		[Address(RVA = "0x101304984", Offset = "0x1304984", VA = "0x101304984", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040030F4 RID: 12532
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002517")]
		private RoomState_Init.eStep m_Step;

		// Token: 0x02000823 RID: 2083
		[Token(Token = "0x2000ED3")]
		private enum eStep
		{
			// Token: 0x040030F6 RID: 12534
			[Token(Token = "0x4005F51")]
			None = -1,
			// Token: 0x040030F7 RID: 12535
			[Token(Token = "0x4005F52")]
			First,
			// Token: 0x040030F8 RID: 12536
			[Token(Token = "0x4005F53")]
			UIPrepare_Start,
			// Token: 0x040030F9 RID: 12537
			[Token(Token = "0x4005F54")]
			UIPrepare_Processing,
			// Token: 0x040030FA RID: 12538
			[Token(Token = "0x4005F55")]
			ShopUIPrepare_Start,
			// Token: 0x040030FB RID: 12539
			[Token(Token = "0x4005F56")]
			ShopUIPrepare_Processing,
			// Token: 0x040030FC RID: 12540
			[Token(Token = "0x4005F57")]
			BuilderPrepare_Start,
			// Token: 0x040030FD RID: 12541
			[Token(Token = "0x4005F58")]
			BuilderPrepare_Processing,
			// Token: 0x040030FE RID: 12542
			[Token(Token = "0x4005F59")]
			UIStart,
			// Token: 0x040030FF RID: 12543
			[Token(Token = "0x4005F5A")]
			End
		}
	}
}
