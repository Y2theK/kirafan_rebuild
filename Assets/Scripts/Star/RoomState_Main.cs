﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000824 RID: 2084
	[Token(Token = "0x2000620")]
	[StructLayout(3)]
	public class RoomState_Main : RoomState
	{
		// Token: 0x060020FE RID: 8446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E64")]
		[Address(RVA = "0x101304988", Offset = "0x1304988", VA = "0x101304988")]
		public RoomState_Main(RoomMain owner)
		{
		}

		// Token: 0x060020FF RID: 8447 RVA: 0x0000E6A0 File Offset: 0x0000C8A0
		[Token(Token = "0x6001E65")]
		[Address(RVA = "0x1013049BC", Offset = "0x13049BC", VA = "0x1013049BC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002100 RID: 8448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E66")]
		[Address(RVA = "0x1013049C4", Offset = "0x13049C4", VA = "0x1013049C4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002101 RID: 8449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E67")]
		[Address(RVA = "0x1013049CC", Offset = "0x13049CC", VA = "0x1013049CC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002102 RID: 8450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E68")]
		[Address(RVA = "0x1013049D0", Offset = "0x13049D0", VA = "0x1013049D0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002103 RID: 8451 RVA: 0x0000E6B8 File Offset: 0x0000C8B8
		[Token(Token = "0x6001E69")]
		[Address(RVA = "0x1013049D4", Offset = "0x13049D4", VA = "0x1013049D4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002104 RID: 8452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E6A")]
		[Address(RVA = "0x101304BB4", Offset = "0x1304BB4", VA = "0x101304BB4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002105 RID: 8453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E6B")]
		[Address(RVA = "0x101304C94", Offset = "0x1304C94", VA = "0x101304C94")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003100 RID: 12544
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002518")]
		private RoomState_Main.eStep m_Step;

		// Token: 0x02000825 RID: 2085
		[Token(Token = "0x2000ED4")]
		private enum eStep
		{
			// Token: 0x04003102 RID: 12546
			[Token(Token = "0x4005F5C")]
			None = -1,
			// Token: 0x04003103 RID: 12547
			[Token(Token = "0x4005F5D")]
			First,
			// Token: 0x04003104 RID: 12548
			[Token(Token = "0x4005F5E")]
			Main,
			// Token: 0x04003105 RID: 12549
			[Token(Token = "0x4005F5F")]
			End
		}
	}
}
