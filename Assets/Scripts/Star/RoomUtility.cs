﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000A37 RID: 2615
	[Token(Token = "0x200074A")]
	[StructLayout(3)]
	public static class RoomUtility
	{
		// Token: 0x06002CD3 RID: 11475 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002941")]
		[Address(RVA = "0x101304CE0", Offset = "0x1304CE0", VA = "0x101304CE0")]
		public static string GetSpriteResourcePath(eRoomObjectCategory category, int objID, int fsubkey, int fsubkey2, CharacterDefine.eDir fdir)
		{
			return null;
		}

		// Token: 0x06002CD4 RID: 11476 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002942")]
		[Address(RVA = "0x101305000", Offset = "0x1305000", VA = "0x101305000")]
		public static string GetModelResourcePath(eRoomObjectCategory category, int objID, int fsubkey)
		{
			return null;
		}

		// Token: 0x06002CD5 RID: 11477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002943")]
		[Address(RVA = "0x1013052F8", Offset = "0x13052F8", VA = "0x1013052F8")]
		public static void IsPlacementObject(ref RoomBuilder.EditObjectState pret, eRoomObjectCategory category, int floorID)
		{
		}

		// Token: 0x06002CD6 RID: 11478 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002944")]
		[Address(RVA = "0x101305350", Offset = "0x1305350", VA = "0x101305350")]
		public static string GetRoomShadowResourcePath()
		{
			return null;
		}

		// Token: 0x06002CD7 RID: 11479 RVA: 0x000130F8 File Offset: 0x000112F8
		[Token(Token = "0x6002945")]
		[Address(RVA = "0x101305398", Offset = "0x1305398", VA = "0x101305398")]
		public static bool IsPlacementCountObj(eRoomObjectCategory category)
		{
			return default(bool);
		}

		// Token: 0x06002CD8 RID: 11480 RVA: 0x00013110 File Offset: 0x00011310
		[Token(Token = "0x6002946")]
		[Address(RVA = "0x1013053A8", Offset = "0x13053A8", VA = "0x1013053A8")]
		public static bool IsRoomDatabaseActive()
		{
			return default(bool);
		}

		// Token: 0x06002CD9 RID: 11481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002947")]
		[Address(RVA = "0x101305400", Offset = "0x1305400", VA = "0x101305400")]
		public static void GetRoomCharacterAnimSettings(List<AnimSettings> settings, eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterParam param)
		{
		}

		// Token: 0x06002CDA RID: 11482 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002948")]
		[Address(RVA = "0x1013054E0", Offset = "0x13054E0", VA = "0x1013054E0")]
		public static Transform FindTransform(Transform ptarget, string findname, int fchkcall)
		{
			return null;
		}

		// Token: 0x06002CDB RID: 11483 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002949")]
		[Address(RVA = "0x101305614", Offset = "0x1305614", VA = "0x101305614")]
		public static Transform FindStrTransform(Transform ptarget, string findname, int fchkcall)
		{
			return null;
		}

		// Token: 0x06002CDC RID: 11484 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600294A")]
		[Address(RVA = "0x102354268", Offset = "0x2354268", VA = "0x102354268")]
		public static T FindComponent<T>(Transform trns)
		{
			return null;
		}

		// Token: 0x06002CDD RID: 11485 RVA: 0x00013128 File Offset: 0x00011328
		[Token(Token = "0x600294B")]
		[Address(RVA = "0x10130575C", Offset = "0x130575C", VA = "0x10130575C")]
		public static CharacterDefine.eDir CalcObjectLockDir(IRoomObjectControll ptarget, CharacterHandler pbase)
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002CDE RID: 11486 RVA: 0x00013140 File Offset: 0x00011340
		[Token(Token = "0x600294C")]
		[Address(RVA = "0x1013057EC", Offset = "0x13057EC", VA = "0x1013057EC")]
		public static bool CalcObjectChildDir(IRoomObjectControll ptarget, RoomObjectCtrlChara pbase)
		{
			return default(bool);
		}

		// Token: 0x06002CDF RID: 11487 RVA: 0x00013158 File Offset: 0x00011358
		[Token(Token = "0x600294D")]
		[Address(RVA = "0x1013058C0", Offset = "0x13058C0", VA = "0x1013058C0")]
		public static CharacterDefine.eDir ChangeObjectDir(CharacterDefine.eDir fdir)
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002CE0 RID: 11488 RVA: 0x00013170 File Offset: 0x00011370
		[Token(Token = "0x600294E")]
		[Address(RVA = "0x1013058CC", Offset = "0x13058CC", VA = "0x1013058CC")]
		public static CharacterDefine.eDir ChangeObjectRevDir(CharacterDefine.eDir fdir)
		{
			return CharacterDefine.eDir.L;
		}

		// Token: 0x06002CE1 RID: 11489 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600294F")]
		[Address(RVA = "0x1013058D8", Offset = "0x13058D8", VA = "0x1013058D8")]
		public static Transform GetMeigeControllNode(GameObject pobj)
		{
			return null;
		}

		// Token: 0x06002CE2 RID: 11490 RVA: 0x00013188 File Offset: 0x00011388
		[Token(Token = "0x6002950")]
		[Address(RVA = "0x1013059AC", Offset = "0x13059AC", VA = "0x1013059AC")]
		public static int GetFloorLayer(int fgroupno)
		{
			return 0;
		}

		// Token: 0x06002CE3 RID: 11491 RVA: 0x000131A0 File Offset: 0x000113A0
		[Token(Token = "0x6002951")]
		[Address(RVA = "0x1013059BC", Offset = "0x13059BC", VA = "0x1013059BC")]
		public static int GetWallLayer(int fgroupno)
		{
			return 0;
		}

		// Token: 0x06002CE4 RID: 11492 RVA: 0x000131B8 File Offset: 0x000113B8
		[Token(Token = "0x6002952")]
		[Address(RVA = "0x1013059CC", Offset = "0x13059CC", VA = "0x1013059CC")]
		public static float GetCharaMoveTime(Vector3 ftarget, Vector3 fposition)
		{
			return 0f;
		}

		// Token: 0x06002CE5 RID: 11493 RVA: 0x000131D0 File Offset: 0x000113D0
		[Token(Token = "0x6002953")]
		[Address(RVA = "0x101305AA8", Offset = "0x1305AA8", VA = "0x101305AA8")]
		public static Vector2 GridToTilePos(float gridX, float gridY)
		{
			return default(Vector2);
		}

		// Token: 0x06002CE6 RID: 11494 RVA: 0x000131E8 File Offset: 0x000113E8
		[Token(Token = "0x6002954")]
		[Address(RVA = "0x101305B5C", Offset = "0x1305B5C", VA = "0x101305B5C")]
		public static Vector2 TilePosToGrid(Vector2 tilePos)
		{
			return default(Vector2);
		}

		// Token: 0x06002CE7 RID: 11495 RVA: 0x00013200 File Offset: 0x00011400
		[Token(Token = "0x6002955")]
		[Address(RVA = "0x101305B7C", Offset = "0x1305B7C", VA = "0x101305B7C")]
		public static Vector2 GetBlockSize(float fblockx, float fblocky)
		{
			return default(Vector2);
		}

		// Token: 0x06002CE8 RID: 11496 RVA: 0x00013218 File Offset: 0x00011418
		[Token(Token = "0x6002956")]
		[Address(RVA = "0x101305B80", Offset = "0x1305B80", VA = "0x101305B80")]
		public static Vector2 CalcDummyGridPos(Vector3 fpos)
		{
			return default(Vector2);
		}

		// Token: 0x06002CE9 RID: 11497 RVA: 0x00013230 File Offset: 0x00011430
		[Token(Token = "0x6002957")]
		[Address(RVA = "0x101304FD0", Offset = "0x1304FD0", VA = "0x101304FD0")]
		public static bool ChkChangeNight(eRoomObjectCategory category, int objID)
		{
			return default(bool);
		}

		// Token: 0x06002CEA RID: 11498 RVA: 0x00013248 File Offset: 0x00011448
		[Token(Token = "0x6002958")]
		[Address(RVA = "0x101305C30", Offset = "0x1305C30", VA = "0x101305C30")]
		public static bool ChkChangeNight(int accessKey)
		{
			return default(bool);
		}

		// Token: 0x06002CEB RID: 11499 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002959")]
		[Address(RVA = "0x101305C60", Offset = "0x1305C60", VA = "0x101305C60")]
		public static List<RoomObjHaveList.RoomObj> DoFilterHaveList(List<RoomObjHaveList.RoomObj> baseList)
		{
			return null;
		}

		// Token: 0x06002CEC RID: 11500 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600295A")]
		[Address(RVA = "0x1013060AC", Offset = "0x13060AC", VA = "0x1013060AC")]
		public static List<RoomObjShopList.RoomShopObjData> DoFilterShopList(List<RoomObjShopList.RoomShopObjData> baseList)
		{
			return null;
		}
	}
}
