﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A4D RID: 2637
	[Token(Token = "0x200075A")]
	[StructLayout(0, Size = 8)]
	public struct RoomWakeUpKey
	{
		// Token: 0x04003CFB RID: 15611
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002BF8")]
		public RoomCharaActMode.eMode m_Type;

		// Token: 0x04003CFC RID: 15612
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002BF9")]
		public float m_Percent;
	}
}
