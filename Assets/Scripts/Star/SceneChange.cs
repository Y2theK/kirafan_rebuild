﻿using UnityEngine;

namespace Star {
    public class SceneChange : MonoBehaviour {
        private const float ANIM_TIMESCALE = 2f;

        private static readonly string[] ANIM_KEYS = new string[] {
            "st",
            "ed"
        };

        [SerializeField] private GameObject m_BodyObj;
        [SerializeField] private MsbHandler m_MsbHndl;
        [SerializeField] private MeigeAnimCtrl m_MeigeAnimCtrl;
        [SerializeField] private MeigeAnimClipHolder[] m_MeigeAnimClipHolders;
        [SerializeField] private Transform m_BaseTransform;
        [SerializeField] private int m_BaseWidth = 1334;
        [SerializeField] private int m_BaseHeight = 750;

        private eStep m_Step = eStep.None;

        private void Start() {
            m_MeigeAnimCtrl.Open();
            m_MeigeAnimCtrl.AddClip(m_MsbHndl, m_MeigeAnimClipHolders[0]);
            m_MeigeAnimCtrl.AddClip(m_MsbHndl, m_MeigeAnimClipHolders[1]);
            m_MeigeAnimCtrl.Close();
            m_BodyObj.SetActive(false);
        }

        private void Update() {
            switch (m_Step) {
                case eStep.In:
                    if (!IsPlaying()) {
                        m_MeigeAnimCtrl.Pause();
                        m_Step = eStep.Overlay;
                    }
                    break;
                case eStep.Out:
                    if (!IsPlaying()) {
                        m_BodyObj.SetActive(false);
                        m_MeigeAnimCtrl.Pause();
                        m_Step = eStep.None;
                    }
                    break;
            }
        }

        public void Play(eAnimType animType) {
            AdjustScale();
            m_BodyObj.SetActive(true);
            m_MeigeAnimCtrl.Play(ANIM_KEYS[(int)animType], 0f, WrapMode.ClampForever);
            m_MeigeAnimCtrl.SetAnimationPlaySpeed(ANIM_TIMESCALE);
            if (animType == eAnimType.In) {
                m_Step = eStep.In;
            } else {
                m_Step = eStep.Out;
            }
        }

        public void PlayQuick(eAnimType animType) {
            AdjustScale();
            if (animType == eAnimType.In) {
                m_BodyObj.SetActive(true);
                m_MsbHndl.SetDefaultVisibility();
                m_Step = eStep.Overlay;
            } else {
                m_BodyObj.SetActive(false);
                m_Step = eStep.None;
            }
        }

        public bool IsComplete() {
            return m_Step != eStep.In && m_Step != eStep.Out;
        }

        private bool IsPlaying() {
            return m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
        }

        private void AdjustScale() {
            float baseRatio = m_BaseHeight / m_BaseWidth;
            float screenRatio = Screen.height / Screen.width;
            float scale = Mathf.Max(1f, screenRatio / baseRatio);
            m_BaseTransform.localScale = new Vector3(1f * scale, 1f * scale, 1f);
        }

        public enum eAnimType {
            In,
            Out,
            Num
        }

        private enum eStep {
            None = -1,
            In,
            Overlay,
            Out
        }
    }
}
