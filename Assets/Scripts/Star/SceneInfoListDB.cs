﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005A0 RID: 1440
	[Token(Token = "0x2000493")]
	[StructLayout(3)]
	public class SceneInfoListDB : ScriptableObject
	{
		// Token: 0x060015F8 RID: 5624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014A8")]
		[Address(RVA = "0x101307BE0", Offset = "0x1307BE0", VA = "0x101307BE0")]
		public SceneInfoListDB()
		{
		}

		// Token: 0x04001AD6 RID: 6870
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001440")]
		public SceneInfoListDB_Param[] m_Params;
	}
}
