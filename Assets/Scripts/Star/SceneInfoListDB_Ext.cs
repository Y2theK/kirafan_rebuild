﻿namespace Star {
    public static class SceneInfoListDB_Ext {
        public static SceneInfoListDB_Param GetParam(this SceneInfoListDB self, eSceneInfo sceneInfo) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_SceneID == (int)sceneInfo) {
                    return self.m_Params[i];
                }
            }
            return default;
        }
    }
}
