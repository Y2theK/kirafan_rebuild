﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200059F RID: 1439
	[Token(Token = "0x2000492")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct SceneInfoListDB_Param
	{
		// Token: 0x04001AD3 RID: 6867
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400143D")]
		public int m_SceneID;

		// Token: 0x04001AD4 RID: 6868
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400143E")]
		public string m_ResourceName;

		// Token: 0x04001AD5 RID: 6869
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400143F")]
		public string m_SceneName;
	}
}
