﻿using Meige;
using Meige.AssetBundles;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Star {
    public class SceneLoader : SingletonMonoBehaviour<SceneLoader> {
        private MeigeResource.Handler m_MainSceneHandler;
        private OperationInfo[] m_ChildList;
        private StringBuilder m_sb;

        protected override void Awake() {
            if (CheckInstance()) {
                m_ChildList = new OperationInfo[(int)SceneDefine.eChildSceneID.Num];
                m_sb = new StringBuilder();
                SceneManager.activeSceneChanged += OnActiveSceneChanged;
                SceneManager.sceneLoaded += OnSceneLoaded;
                SceneManager.sceneUnloaded += OnSceneUnloaded;
            }
        }

        public void Update() {
            if (m_MainSceneHandler != null && m_MainSceneHandler.IsError) {
                AssetBundleManager.RestartManifestUpdate();
                MeigeResourceManager.RetryHandler(m_MainSceneHandler);
            }
            for (int i = 0; i < m_ChildList.Length; i++) {
                OperationInfo child = m_ChildList[i];
                if (child != null && child.m_SceneHandler != null && child.m_SceneHandler.IsError) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.RetryHandler(child.m_SceneHandler);
                }
            }
        }

        public void ForceTransitTitleScene() {
            for (int i = 0; i < m_ChildList.Length; i++) {
                OperationInfo opInfo = m_ChildList[i];
                if (opInfo != null) {
                    if (opInfo.m_SceneHandler != null) {
                        MeigeResourceManager.UnloadHandler(opInfo.m_SceneHandler);
                        opInfo.m_SceneHandler = null;
                    }
                    if (opInfo.m_Op != null) {
                        opInfo.m_Op = null;
                    }
                }
                m_ChildList[i] = null;
            }
            TransitSceneAsync(SceneDefine.eSceneID.Title);
        }

        public void TransitSceneAsync(SceneDefine.eSceneID sceneID) {
            if (m_MainSceneHandler != null) {
                MeigeResourceManager.UnloadHandler(m_MainSceneHandler);
                m_MainSceneHandler = null;
            }
            SceneDefine.SceneInfo info = SceneDefine.SCENE_INFOS[sceneID];
            if (info.m_IsAssetBundle) {
                m_sb.Length = 0;
                m_sb.Append("Scene/");
                m_sb.Append(info.m_SceneName);
                m_sb.Append(MeigeResourceManager.AB_EXTENSION);
                m_MainSceneHandler = MeigeResourceManager.LoadHandler(m_sb.ToString(), info.m_SceneName, MeigeResource.Option.Scene);
            } else {
                SceneManager.LoadSceneAsync(info.m_SceneName, LoadSceneMode.Single);
            }
        }

        public void AdditiveChildSceneAsync(SceneDefine.eChildSceneID childSceneID, bool allowSceneActivation = true) {
            if (m_ChildList[(int)childSceneID] == null) {
                m_ChildList[(int)childSceneID] = new OperationInfo();
            }
            OperationInfo opInfo = m_ChildList[(int)childSceneID];
            SceneDefine.SceneInfo info = SceneDefine.CHILD_SCENE_INFOS[childSceneID];
            if (info.m_IsAssetBundle) {
                m_sb.Length = 0;
                m_sb.Append("Scene/Child/");
                m_sb.Append(info.m_SceneName);
                m_sb.Append(MeigeResourceManager.AB_EXTENSION);
                MeigeResource.Option[] options;
                if (allowSceneActivation) {
                    options = new MeigeResource.Option[] {
                        MeigeResource.Option.SceneAdditive
                    };
                } else {
                    options = new MeigeResource.Option[] {
                        MeigeResource.Option.SceneAdditive,
                        MeigeResource.Option.SceneActivationFalse
                    };
                }
                opInfo.m_SceneHandler = MeigeResourceManager.LoadHandler(m_sb.ToString(), info.m_SceneName, options);
                opInfo.m_Op = null;
            } else {
                AsyncOperation op = SceneManager.LoadSceneAsync(info.m_SceneName, LoadSceneMode.Additive);
                op.allowSceneActivation = allowSceneActivation;
                opInfo.m_Op = op;
                opInfo.m_SceneHandler = null;
            }
        }

        public bool IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID childSceneID) {
            OperationInfo opInfo = m_ChildList[(int)childSceneID];
            if (opInfo != null) {
                if (opInfo.m_SceneHandler != null) {
                    return opInfo.m_SceneHandler.IsDone;
                }
                if (opInfo.m_Op != null) {
                    if (opInfo.m_Op.allowSceneActivation) {
                        return opInfo.m_Op.isDone;
                    }
                    return opInfo.m_Op.progress >= 0.9f;
                }
            }
            return false;
        }

        public void ActivationChildScene(SceneDefine.eChildSceneID childSceneID) {
            OperationInfo opInfo = m_ChildList[(int)childSceneID];
            if (opInfo != null) {
                if (opInfo.m_SceneHandler != null) {
                    if (!opInfo.m_SceneHandler.asyncOperation.allowSceneActivation) {
                        opInfo.m_SceneHandler.asyncOperation.allowSceneActivation = true;
                    }
                } else if (opInfo.m_Op != null && !opInfo.m_Op.allowSceneActivation) {
                    opInfo.m_Op.allowSceneActivation = true;
                    opInfo.m_Op = null;
                }
            }
        }

        public void UnloadChildSceneAsync(SceneDefine.eChildSceneID childSceneID) {
            OperationInfo opInfo = m_ChildList[(int)childSceneID];
            if (opInfo != null) {
                if (opInfo.m_SceneHandler != null) {
                    MeigeResourceManager.UnloadHandler(opInfo.m_SceneHandler);
                } else {
                    string name = SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName;
                    opInfo.m_Op = SceneManager.UnloadSceneAsync(name);
                }
            }
        }

        public bool IsCompleteUnloadChildScene(SceneDefine.eChildSceneID childSceneID) {
            OperationInfo opInfo = m_ChildList[(int)childSceneID];
            if (opInfo != null) {
                if (opInfo.m_SceneHandler != null) {
                    if (opInfo.m_SceneHandler.IsUnloadDone) {
                        opInfo.m_SceneHandler = null;
                        return true;
                    }
                    return false;
                } else if (opInfo.m_Op != null) {
                    if (opInfo.m_Op.isDone) {
                        opInfo.m_Op = null;
                        return true;
                    }
                    return false;
                }
            }
            return true;
        }

        public Scene GetScene(SceneDefine.eSceneID sceneID) {
            return SceneManager.GetSceneByName(SceneDefine.SCENE_INFOS[sceneID].m_SceneName);
        }

        public Scene GetChildScene(SceneDefine.eChildSceneID childSceneID) {
            return SceneManager.GetSceneByName(SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName);
        }

        private void OnActiveSceneChanged(Scene preChangedScene, Scene postChangedScene) { }

        private void OnSceneLoaded(Scene loadedScene, LoadSceneMode mode) { }

        private void OnSceneUnloaded(Scene unloaded) { }

        [Conditional("APP_DEBUG")]
        private void StartStopwatch(string key) { }

        [Conditional("APP_DEBUG")]
        private void StopStopwatch(string key) { }

        private class OperationInfo {
            public MeigeResource.Handler m_SceneHandler;
            public AsyncOperation m_Op;
        }
    }
}
