﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200071D RID: 1821
	[Token(Token = "0x200059F")]
	[StructLayout(3)]
	public class ScheduleDataBase
	{
		// Token: 0x06001A98 RID: 6808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001888")]
		[Address(RVA = "0x101308930", Offset = "0x1308930", VA = "0x101308930")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06001A99 RID: 6809 RVA: 0x0000BE50 File Offset: 0x0000A050
		[Token(Token = "0x6001889")]
		[Address(RVA = "0x101308A5C", Offset = "0x1308A5C", VA = "0x101308A5C")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06001A9A RID: 6810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600188A")]
		[Address(RVA = "0x101308AEC", Offset = "0x1308AEC", VA = "0x101308AEC")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x06001A9B RID: 6811 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600188B")]
		[Address(RVA = "0x101308B78", Offset = "0x1308B78", VA = "0x101308B78")]
		public static ScheduleSetUpDataBase GetScheduleSetUpDB()
		{
			return null;
		}

		// Token: 0x06001A9C RID: 6812 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600188C")]
		[Address(RVA = "0x101308BC8", Offset = "0x1308BC8", VA = "0x101308BC8")]
		public static ScheduleHolydayCheck GetScheduleHolyday()
		{
			return null;
		}

		// Token: 0x06001A9D RID: 6813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600188D")]
		[Address(RVA = "0x101308C18", Offset = "0x1308C18", VA = "0x101308C18")]
		public ScheduleDataBase()
		{
		}

		// Token: 0x04002AB8 RID: 10936
		[Token(Token = "0x40022C0")]
		public static ScheduleSetUpDataBase ms_ScheduleDataBase;

		// Token: 0x04002AB9 RID: 10937
		[Token(Token = "0x40022C1")]
		public static ScheduleHolydayCheck ms_ScheduleHolyday;
	}
}
