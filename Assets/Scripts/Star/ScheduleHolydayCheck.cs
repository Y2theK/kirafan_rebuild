﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200071E RID: 1822
	[Token(Token = "0x20005A0")]
	[StructLayout(3)]
	public class ScheduleHolydayCheck : IDataBaseResource
	{
		// Token: 0x06001A9E RID: 6814 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600188E")]
		[Address(RVA = "0x101308C20", Offset = "0x1308C20", VA = "0x101308C20")]
		public ScheduleHolydayDB GetDatabase()
		{
			return null;
		}

		// Token: 0x06001A9F RID: 6815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600188F")]
		[Address(RVA = "0x101308C28", Offset = "0x1308C28", VA = "0x101308C28", Slot = "4")]
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
		}

		// Token: 0x06001AA0 RID: 6816 RVA: 0x0000BE68 File Offset: 0x0000A068
		[Token(Token = "0x6001890")]
		[Address(RVA = "0x101308CB8", Offset = "0x1308CB8", VA = "0x101308CB8")]
		public int GetSecToHolydayType(long ftimesec)
		{
			return 0;
		}

		// Token: 0x06001AA1 RID: 6817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001891")]
		[Address(RVA = "0x101308A54", Offset = "0x1308A54", VA = "0x101308A54")]
		public ScheduleHolydayCheck()
		{
		}

		// Token: 0x04002ABA RID: 10938
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40022C2")]
		private ScheduleHolydayDB m_Database;
	}
}
