﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000682 RID: 1666
	[Token(Token = "0x2000551")]
	[StructLayout(3)]
	public class ScheduleHolydayDB : ScriptableObject
	{
		// Token: 0x0600183C RID: 6204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D7")]
		[Address(RVA = "0x101308F8C", Offset = "0x1308F8C", VA = "0x101308F8C")]
		public ScheduleHolydayDB()
		{
		}

		// Token: 0x040027B7 RID: 10167
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002094")]
		public ScheduleHolydayDB_Param[] m_Params;
	}
}
