﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000681 RID: 1665
	[Token(Token = "0x2000550")]
	[Serializable]
	[StructLayout(0, Size = 12)]
	public struct ScheduleHolydayDB_Param
	{
		// Token: 0x040027B2 RID: 10162
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400208F")]
		public int m_ID;

		// Token: 0x040027B3 RID: 10163
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002090")]
		public short m_OptionGroup;

		// Token: 0x040027B4 RID: 10164
		[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
		[Token(Token = "0x4002091")]
		public short m_Year;

		// Token: 0x040027B5 RID: 10165
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002092")]
		public byte m_Month;

		// Token: 0x040027B6 RID: 10166
		[Cpp2IlInjected.FieldOffset(Offset = "0x9")]
		[Token(Token = "0x4002093")]
		public byte m_Day;
	}
}
