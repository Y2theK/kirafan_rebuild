﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000684 RID: 1668
	[Token(Token = "0x2000553")]
	[StructLayout(3)]
	public class ScheduleNameDB : ScriptableObject
	{
		// Token: 0x0600183D RID: 6205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D8")]
		[Address(RVA = "0x101308F94", Offset = "0x1308F94", VA = "0x101308F94")]
		public ScheduleNameDB()
		{
		}

		// Token: 0x040027CC RID: 10188
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40020A9")]
		public ScheduleNameDB_Param[] m_Params;
	}
}
