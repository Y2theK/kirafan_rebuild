﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000683 RID: 1667
	[Token(Token = "0x2000552")]
	[Serializable]
	[StructLayout(0, Size = 128)]
	public struct ScheduleNameDB_Param
	{
		// Token: 0x040027B8 RID: 10168
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002095")]
		public int m_ID;

		// Token: 0x040027B9 RID: 10169
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002096")]
		public string m_TagName;

		// Token: 0x040027BA RID: 10170
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002097")]
		public ushort[] m_GroupWakeUp;

		// Token: 0x040027BB RID: 10171
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002098")]
		public uint[] m_ResultBaseNo;

		// Token: 0x040027BC RID: 10172
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002099")]
		public short[] m_WakeBaseUp;

		// Token: 0x040027BD RID: 10173
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400209A")]
		public uint[] m_ResultSpNo;

		// Token: 0x040027BE RID: 10174
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400209B")]
		public short[] m_WakeSpUp;

		// Token: 0x040027BF RID: 10175
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400209C")]
		public int[] m_SpMessage;

		// Token: 0x040027C0 RID: 10176
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400209D")]
		public int[] m_BaseMessage;

		// Token: 0x040027C1 RID: 10177
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400209E")]
		public int m_MoveScriptID;

		// Token: 0x040027C2 RID: 10178
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400209F")]
		public int m_DropMakeWakeUp;

		// Token: 0x040027C3 RID: 10179
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40020A0")]
		public uint[] m_DropResultNo;

		// Token: 0x040027C4 RID: 10180
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40020A1")]
		public short[] m_DropWakeUp;

		// Token: 0x040027C5 RID: 10181
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40020A2")]
		public int m_LiinkNo;

		// Token: 0x040027C6 RID: 10182
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40020A3")]
		public uint m_Flags;

		// Token: 0x040027C7 RID: 10183
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40020A4")]
		public string m_DisplayName;

		// Token: 0x040027C8 RID: 10184
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40020A5")]
		public int m_DisplayColor;

		// Token: 0x040027C9 RID: 10185
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x40020A6")]
		public int m_BuildMoveType;

		// Token: 0x040027CA RID: 10186
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40020A7")]
		public int m_BuildMoveCode;

		// Token: 0x040027CB RID: 10187
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x40020A8")]
		public int m_LinkTitle;
	}
}
