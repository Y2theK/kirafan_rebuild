﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000688 RID: 1672
	[Token(Token = "0x2000557")]
	[StructLayout(3)]
	public class ScheduleNameReplaceDB : ScriptableObject
	{
		// Token: 0x0600183E RID: 6206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016D9")]
		[Address(RVA = "0x101308F9C", Offset = "0x1308F9C", VA = "0x101308F9C")]
		public ScheduleNameReplaceDB()
		{
		}

		// Token: 0x040027DD RID: 10205
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40020BA")]
		public ScheduleNameReplaceDB_Param[] m_Params;
	}
}
