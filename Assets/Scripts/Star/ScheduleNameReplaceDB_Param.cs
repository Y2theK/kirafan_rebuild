﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000687 RID: 1671
	[Token(Token = "0x2000556")]
	[Serializable]
	[StructLayout(0)]
	public struct ScheduleNameReplaceDB_Param
	{
		// Token: 0x040027DA RID: 10202
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40020B7")]
		public int m_CharaID;

		// Token: 0x040027DB RID: 10203
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40020B8")]
		public int[] m_ScheduleID;

		// Token: 0x040027DC RID: 10204
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40020B9")]
		public string[] m_ReplaceScheduleName;
	}
}
