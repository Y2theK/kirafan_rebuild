﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200071F RID: 1823
	[Token(Token = "0x20005A1")]
	[StructLayout(3)]
	public class ScheduleNameUtil
	{
		// Token: 0x06001AA2 RID: 6818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001892")]
		[Address(RVA = "0x101308FA4", Offset = "0x1308FA4", VA = "0x101308FA4")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06001AA3 RID: 6819 RVA: 0x0000BE80 File Offset: 0x0000A080
		[Token(Token = "0x6001893")]
		[Address(RVA = "0x1013090DC", Offset = "0x13090DC", VA = "0x1013090DC")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06001AA4 RID: 6820 RVA: 0x0000BE98 File Offset: 0x0000A098
		[Token(Token = "0x6001894")]
		[Address(RVA = "0x10130913C", Offset = "0x130913C", VA = "0x10130913C")]
		public static ScheduleNameDB_Param GetScheduleNameTag(int fkeyid)
		{
			return default(ScheduleNameDB_Param);
		}

		// Token: 0x06001AA5 RID: 6821 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001895")]
		[Address(RVA = "0x101309374", Offset = "0x1309374", VA = "0x101309374")]
		public static ScheduleNameDB GetScheduleDataBase()
		{
			return null;
		}

		// Token: 0x06001AA6 RID: 6822 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001896")]
		[Address(RVA = "0x1013093D4", Offset = "0x13093D4", VA = "0x1013093D4")]
		public static ScheduleNameReplaceDB GetScheduleNameReplaceDataBase()
		{
			return null;
		}

		// Token: 0x06001AA7 RID: 6823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001897")]
		[Address(RVA = "0x101309434", Offset = "0x1309434", VA = "0x101309434")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x06001AA8 RID: 6824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001898")]
		[Address(RVA = "0x101309514", Offset = "0x1309514", VA = "0x101309514")]
		public static void CalcScheduleTagToDropItemKey(UserFieldCharaData.RoomInCharaData pchara, int fscheduletagid, int fscheduletagtime, int fcharaid, eTownMoveState fupmove)
		{
		}

		// Token: 0x06001AA9 RID: 6825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001899")]
		[Address(RVA = "0x10130A014", Offset = "0x130A014", VA = "0x10130A014")]
		public ScheduleNameUtil()
		{
		}

		// Token: 0x04002ABB RID: 10939
		[Token(Token = "0x40022C3")]
		public static ScheduleNameUtil.ScheduleNameData ms_ScheduleNameDB;

		// Token: 0x04002ABC RID: 10940
		[Token(Token = "0x40022C4")]
		public static ScheduleNameUtil.ScheduleNameReplaceData ms_ScheduleNameReplaceDB;

		// Token: 0x02000720 RID: 1824
		[Token(Token = "0x2000E50")]
		public class ScheduleNameData : IDataBaseResource
		{
			// Token: 0x06001AAA RID: 6826 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED2")]
			[Address(RVA = "0x10130A01C", Offset = "0x130A01C", VA = "0x10130A01C", Slot = "4")]
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
			}

			// Token: 0x06001AAB RID: 6827 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED3")]
			[Address(RVA = "0x1013090CC", Offset = "0x13090CC", VA = "0x1013090CC")]
			public ScheduleNameData()
			{
			}

			// Token: 0x04002ABD RID: 10941
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B71")]
			public ScheduleNameDB m_DB;
		}

		// Token: 0x02000721 RID: 1825
		[Token(Token = "0x2000E51")]
		public class ScheduleNameReplaceData : IDataBaseResource
		{
			// Token: 0x06001AAC RID: 6828 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED4")]
			[Address(RVA = "0x10130A0AC", Offset = "0x130A0AC", VA = "0x10130A0AC", Slot = "4")]
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
			}

			// Token: 0x06001AAD RID: 6829 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED5")]
			[Address(RVA = "0x1013090D4", Offset = "0x13090D4", VA = "0x1013090D4")]
			public ScheduleNameReplaceData()
			{
			}

			// Token: 0x04002ABE RID: 10942
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4005B72")]
			public ScheduleNameReplaceDB m_DB;
		}
	}
}
