﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000722 RID: 1826
	[Token(Token = "0x20005A2")]
	[StructLayout(3)]
	public class ScheduleSetUpDataBase : IDataBaseResource
	{
		// Token: 0x06001AAE RID: 6830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600189A")]
		[Address(RVA = "0x10130A13C", Offset = "0x130A13C", VA = "0x10130A13C", Slot = "4")]
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
		}

		// Token: 0x06001AAF RID: 6831 RVA: 0x0000BEB0 File Offset: 0x0000A0B0
		[Token(Token = "0x600189B")]
		[Address(RVA = "0x10130B164", Offset = "0x130B164", VA = "0x10130B164")]
		private static UserScheduleData.eType ChangeXlsKeyToType(int fmakewark)
		{
			return UserScheduleData.eType.Non;
		}

		// Token: 0x06001AB0 RID: 6832 RVA: 0x0000BEC8 File Offset: 0x0000A0C8
		[Token(Token = "0x600189C")]
		[Address(RVA = "0x10130B17C", Offset = "0x130B17C", VA = "0x10130B17C")]
		private bool CheckTownBuildToPoint(int fmakewark, ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid)
		{
			return default(bool);
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600189D")]
		[Address(RVA = "0x10130B29C", Offset = "0x130B29C", VA = "0x10130B29C")]
		public void CreateList(long fmanageid, long fsettime, ref UserScheduleData.ListPack pbase, bool fixroom)
		{
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x0000BEE0 File Offset: 0x0000A0E0
		[Token(Token = "0x600189E")]
		[Address(RVA = "0x10130BF34", Offset = "0x130BF34", VA = "0x10130BF34")]
		public int ChangeFailTag(int fscheduletag, eTitleType ftitle)
		{
			return 0;
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600189F")]
		[Address(RVA = "0x10130C1F8", Offset = "0x130C1F8", VA = "0x10130C1F8")]
		public void ChangeSegState(UserScheduleData.Seg pseg, int fchg)
		{
		}

		// Token: 0x06001AB4 RID: 6836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60018A0")]
		[Address(RVA = "0x101308A4C", Offset = "0x1308A4C", VA = "0x101308A4C")]
		public ScheduleSetUpDataBase()
		{
		}

		// Token: 0x04002ABF RID: 10943
		[Token(Token = "0x40022C5")]
		private const int TIME_HOUR_TO_SEC = 3600;

		// Token: 0x04002AC0 RID: 10944
		[Token(Token = "0x40022C6")]
		public const int ROOM_TAG = 1;

		// Token: 0x04002AC1 RID: 10945
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40022C7")]
		public ScheduleSetupDB m_Database;

		// Token: 0x04002AC2 RID: 10946
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40022C8")]
		private ScheduleSetUpDataBase.ScheduleCategory[] m_CategoryDB;

		// Token: 0x02000723 RID: 1827
		[Token(Token = "0x2000E52")]
		public struct ScheduleUpElement
		{
			// Token: 0x06001AB5 RID: 6837 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED6")]
			[Address(RVA = "0x10003508C", Offset = "0x3508C", VA = "0x10003508C")]
			public void SetUp(ref ScheduleSetupDB_Param pbase)
			{
			}

			// Token: 0x04002AC3 RID: 10947
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005B73")]
			public int m_ID;

			// Token: 0x04002AC4 RID: 10948
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4005B74")]
			public ushort m_BuildPriority;

			// Token: 0x04002AC5 RID: 10949
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005B75")]
			public int m_ScheduleTagSelf;

			// Token: 0x04002AC6 RID: 10950
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4005B76")]
			public int m_ScheduleTagOther;

			// Token: 0x04002AC7 RID: 10951
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B77")]
			public eTitleType m_Content;

			// Token: 0x04002AC8 RID: 10952
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B78")]
			public ushort m_MakePoint;

			// Token: 0x04002AC9 RID: 10953
			[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
			[Token(Token = "0x4005B79")]
			public ushort m_Week;

			// Token: 0x04002ACA RID: 10954
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B7A")]
			public ushort m_Holyday;

			// Token: 0x04002ACB RID: 10955
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005B7B")]
			public eXlsScheduleMakeType m_BuildType;

			// Token: 0x04002ACC RID: 10956
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B7C")]
			public int m_StartTime;

			// Token: 0x04002ACD RID: 10957
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4005B7D")]
			public int m_EndTime;

			// Token: 0x04002ACE RID: 10958
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B7E")]
			public int m_StartLife;

			// Token: 0x04002ACF RID: 10959
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4005B7F")]
			public int m_EndLife;

			// Token: 0x04002AD0 RID: 10960
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4005B80")]
			public short m_BuildUpPer;
		}

		// Token: 0x02000724 RID: 1828
		[Token(Token = "0x2000E53")]
		public class ScheudlePackBase
		{
			// Token: 0x06001AB6 RID: 6838 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005ED7")]
			[Address(RVA = "0x10130B090", Offset = "0x130B090", VA = "0x10130B090")]
			public static ScheduleSetUpDataBase.ScheudlePackBase Create(int fkey)
			{
				return null;
			}

			// Token: 0x06001AB7 RID: 6839 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED8")]
			[Address(RVA = "0x10130D928", Offset = "0x130D928", VA = "0x10130D928", Slot = "4")]
			public virtual void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
			}

			// Token: 0x06001AB8 RID: 6840 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005ED9")]
			[Address(RVA = "0x10130D92C", Offset = "0x130D92C", VA = "0x10130D92C")]
			public ScheudlePackBase()
			{
			}

			// Token: 0x04002AD1 RID: 10961
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B81")]
			public ScheduleSetUpDataBase.ScheduleUpElement[] m_List;
		}

		// Token: 0x02000725 RID: 1829
		[Token(Token = "0x2000E54")]
		public class ScheudlePackNormal : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06001AB9 RID: 6841 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EDA")]
			[Address(RVA = "0x10130E3BC", Offset = "0x130E3BC", VA = "0x10130E3BC", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
			}

			// Token: 0x06001ABA RID: 6842 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EDB")]
			[Address(RVA = "0x10130D920", Offset = "0x130D920", VA = "0x10130D920")]
			public ScheudlePackNormal()
			{
			}
		}

		// Token: 0x02000726 RID: 1830
		[Token(Token = "0x2000E55")]
		public class ScheudlePackContent : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06001ABB RID: 6843 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EDC")]
			[Address(RVA = "0x10130DD08", Offset = "0x130DD08", VA = "0x10130DD08")]
			private void CheckTownBuildToContent(ref TownAccessCheck.ChrCheckDataBase pchrparam, List<ScheduleSetUpDataBase.ScheudlePackContent.Up> flist)
			{
			}

			// Token: 0x06001ABC RID: 6844 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EDD")]
			[Address(RVA = "0x10130DF10", Offset = "0x130DF10", VA = "0x10130DF10", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
			}

			// Token: 0x06001ABD RID: 6845 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EDE")]
			[Address(RVA = "0x10130D910", Offset = "0x130D910", VA = "0x10130D910")]
			public ScheudlePackContent()
			{
			}

			// Token: 0x02000727 RID: 1831
			[Token(Token = "0x2001344")]
			public class Up
			{
				// Token: 0x06001ABE RID: 6846 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600650C")]
				[Address(RVA = "0x10130DEC8", Offset = "0x130DEC8", VA = "0x10130DEC8")]
				public Up(int fpoint, float fper, eTitleType ftitle)
				{
				}

				// Token: 0x04002AD2 RID: 10962
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x400758B")]
				public float m_Per;

				// Token: 0x04002AD3 RID: 10963
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x400758C")]
				public int m_Index;

				// Token: 0x04002AD4 RID: 10964
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x400758D")]
				public eTitleType m_Title;
			}
		}

		// Token: 0x02000728 RID: 1832
		[Token(Token = "0x2000E56")]
		public class ScheudlePackBuff : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06001ABF RID: 6847 RVA: 0x0000BEF8 File Offset: 0x0000A0F8
			[Token(Token = "0x6005EDF")]
			[Address(RVA = "0x10130D934", Offset = "0x130D934", VA = "0x10130D934")]
			private bool CheckTownBuffToPoint(ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid)
			{
				return default(bool);
			}

			// Token: 0x06001AC0 RID: 6848 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE0")]
			[Address(RVA = "0x10130DAA0", Offset = "0x130DAA0", VA = "0x10130DAA0", Slot = "4")]
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
			}

			// Token: 0x06001AC1 RID: 6849 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE1")]
			[Address(RVA = "0x10130D918", Offset = "0x130D918", VA = "0x10130D918")]
			public ScheudlePackBuff()
			{
			}
		}

		// Token: 0x02000729 RID: 1833
		[Token(Token = "0x2000E57")]
		public class ScheduleCategory
		{
			// Token: 0x06001AC2 RID: 6850 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE2")]
			[Address(RVA = "0x10130B088", Offset = "0x130B088", VA = "0x10130B088")]
			public ScheduleCategory()
			{
			}

			// Token: 0x04002AD5 RID: 10965
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B82")]
			public int m_ListUpNum;

			// Token: 0x04002AD6 RID: 10966
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B83")]
			public ScheduleSetUpDataBase.ScheudlePackBase[] m_Pack;
		}

		// Token: 0x0200072A RID: 1834
		[Token(Token = "0x2000E58")]
		public class Seg
		{
			// Token: 0x06001AC3 RID: 6851 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE3")]
			[Address(RVA = "0x10130CD1C", Offset = "0x130CD1C", VA = "0x10130CD1C")]
			public Seg()
			{
			}

			// Token: 0x04002AD7 RID: 10967
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B84")]
			public int m_Type;

			// Token: 0x04002AD8 RID: 10968
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B85")]
			public int m_WakeTime;

			// Token: 0x04002AD9 RID: 10969
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B86")]
			public int m_UseTime;

			// Token: 0x04002ADA RID: 10970
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005B87")]
			public int m_TagNameID;

			// Token: 0x04002ADB RID: 10971
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B88")]
			public int m_ID;

			// Token: 0x04002ADC RID: 10972
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4005B89")]
			public int m_Priority;
		}

		// Token: 0x0200072B RID: 1835
		[Token(Token = "0x2000E59")]
		public class ScheduleUpParam
		{
			// Token: 0x06001AC4 RID: 6852 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE4")]
			[Address(RVA = "0x10130C338", Offset = "0x130C338", VA = "0x10130C338")]
			public ScheduleUpParam(long fmanageid)
			{
			}

			// Token: 0x06001AC5 RID: 6853 RVA: 0x0000BF10 File Offset: 0x0000A110
			[Token(Token = "0x6005EE5")]
			[Address(RVA = "0x10130C3C4", Offset = "0x130C3C4", VA = "0x10130C3C4")]
			public int GetFreeHour()
			{
				return 0;
			}

			// Token: 0x06001AC6 RID: 6854 RVA: 0x0000BF28 File Offset: 0x0000A128
			[Token(Token = "0x6005EE6")]
			[Address(RVA = "0x10130C438", Offset = "0x130C438", VA = "0x10130C438")]
			public int GetFreeHour(int fstart, int fend)
			{
				return 0;
			}

			// Token: 0x06001AC7 RID: 6855 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE7")]
			[Address(RVA = "0x10130C4BC", Offset = "0x130C4BC", VA = "0x10130C4BC")]
			public void AddSeg(ScheduleSetUpDataBase.Seg pseg)
			{
			}

			// Token: 0x06001AC8 RID: 6856 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005EE8")]
			[Address(RVA = "0x10130C5E8", Offset = "0x130C5E8", VA = "0x10130C5E8")]
			public ScheduleSetUpDataBase.Seg GetLastSeg()
			{
				return null;
			}

			// Token: 0x06001AC9 RID: 6857 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EE9")]
			[Address(RVA = "0x10130C6BC", Offset = "0x130C6BC", VA = "0x10130C6BC")]
			private void SortTable()
			{
			}

			// Token: 0x06001ACA RID: 6858 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EEA")]
			[Address(RVA = "0x10130C894", Offset = "0x130C894", VA = "0x10130C894")]
			private void ReactRoomTag()
			{
			}

			// Token: 0x06001ACB RID: 6859 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EEB")]
			[Address(RVA = "0x10130C998", Offset = "0x130C998", VA = "0x10130C998")]
			private void MargeSeg()
			{
			}

			// Token: 0x06001ACC RID: 6860 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EEC")]
			[Address(RVA = "0x10130CB54", Offset = "0x130CB54", VA = "0x10130CB54")]
			private void SetBlankSeg()
			{
			}

			// Token: 0x06001ACD RID: 6861 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6005EED")]
			[Address(RVA = "0x10130CD24", Offset = "0x130CD24", VA = "0x10130CD24")]
			public UserScheduleData.Seg[] CreateSheduleTable(bool fixroom)
			{
				return null;
			}

			// Token: 0x06001ACE RID: 6862 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EEE")]
			[Address(RVA = "0x10130D520", Offset = "0x130D520", VA = "0x10130D520")]
			public void CreateSceduleFixSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
			}

			// Token: 0x06001ACF RID: 6863 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EEF")]
			[Address(RVA = "0x10130D6A0", Offset = "0x130D6A0", VA = "0x10130D6A0")]
			public void CreateSceduleDeriveSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
			}

			// Token: 0x06001AD0 RID: 6864 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EF0")]
			[Address(RVA = "0x10130D7D4", Offset = "0x130D7D4", VA = "0x10130D7D4")]
			public void CreateSceduleFreeSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
			}

			// Token: 0x04002ADD RID: 10973
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B8A")]
			public List<ScheduleSetUpDataBase.Seg> m_List;

			// Token: 0x04002ADE RID: 10974
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B8B")]
			public byte[] m_Mask;
		}
	}
}
