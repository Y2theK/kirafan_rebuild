﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200068A RID: 1674
	[Token(Token = "0x2000559")]
	[StructLayout(3)]
	public class ScheduleSetupDB : ScriptableObject
	{
		// Token: 0x0600183F RID: 6207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016DA")]
		[Address(RVA = "0x10130E5C8", Offset = "0x130E5C8", VA = "0x10130E5C8")]
		public ScheduleSetupDB()
		{
		}

		// Token: 0x040027EC RID: 10220
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40020C9")]
		public ScheduleSetupDB_Param[] m_Params;
	}
}
