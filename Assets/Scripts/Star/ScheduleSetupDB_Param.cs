﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000689 RID: 1673
	[Token(Token = "0x2000558")]
	[Serializable]
	[StructLayout(0, Size = 48)]
	public struct ScheduleSetupDB_Param
	{
		// Token: 0x040027DE RID: 10206
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40020BB")]
		public int m_ID;

		// Token: 0x040027DF RID: 10207
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40020BC")]
		public ushort m_BuildPriority;

		// Token: 0x040027E0 RID: 10208
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40020BD")]
		public int m_ScheduleTagSelf;

		// Token: 0x040027E1 RID: 10209
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40020BE")]
		public int m_ScheduleTagOther;

		// Token: 0x040027E2 RID: 10210
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40020BF")]
		public int m_Content;

		// Token: 0x040027E3 RID: 10211
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40020C0")]
		public ushort m_MakePoint;

		// Token: 0x040027E4 RID: 10212
		[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
		[Token(Token = "0x40020C1")]
		public ushort m_Week;

		// Token: 0x040027E5 RID: 10213
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40020C2")]
		public ushort m_Holyday;

		// Token: 0x040027E6 RID: 10214
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
		[Token(Token = "0x40020C3")]
		public ushort m_BuildType;

		// Token: 0x040027E7 RID: 10215
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40020C4")]
		public int m_StartTime;

		// Token: 0x040027E8 RID: 10216
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40020C5")]
		public int m_EndTime;

		// Token: 0x040027E9 RID: 10217
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40020C6")]
		public int m_StartLife;

		// Token: 0x040027EA RID: 10218
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40020C7")]
		public int m_EndLife;

		// Token: 0x040027EB RID: 10219
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40020C8")]
		public short m_BuildUpPer;
	}
}
