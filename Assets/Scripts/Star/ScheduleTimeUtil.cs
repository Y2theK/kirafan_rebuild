﻿using System;

namespace Star {
    public static class ScheduleTimeUtil {
        public const long BASE_TIME_SEC = 10000000L;
        public const long BASE_TIME_MS = 10000L;
        public const int TIME_HOUR = 3600;

        public static readonly DateTime UnitBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ms_BackTimeKey;
        public static long m_SettingTimeBase;

        private static long GetUniversalTime() {
            return (GameSystem.Inst.ServerTime.ToUniversalTime().Ticks - UnitBaseTime.Ticks) / BASE_TIME_SEC;
        }

        private static long GetLocalTimeBase() {
            return GameSystem.Inst.ServerTime.Ticks - UnitBaseTime.Ticks;
        }

        public static long GetSystemTimeSec() {
            return m_SettingTimeBase / BASE_TIME_SEC;
        }

        public static long GetSystemTimeMs() {
            return m_SettingTimeBase / BASE_TIME_MS;
        }

        public static long GetSystemTimeBase() {
            if (m_SettingTimeBase == 0L) {
                m_SettingTimeBase = GetLocalTimeBase();
            }
            return m_SettingTimeBase;
        }

        public static int GetTimeCalcMode() {
            return 0;
        }

        public static void SetMarkingTime() {
            m_SettingTimeBase = GetLocalTimeBase();
        }

        public static void ResetBaseTime(int fday, int fhour, int fminute) {
            DateTime serverTime = GameSystem.Inst.ServerTime;
            DateTime dateTime = new DateTime(serverTime.Year, serverTime.Month, serverTime.Day, fhour, fminute, 0, DateTimeKind.Local);
            m_SettingTimeBase = dateTime.Ticks - UnitBaseTime.Ticks;
        }

        public static int GetDayTimeMinute(long ftime) {
            return (int)(ftime % 86400L) / 60;
        }

        public static int GetDayTimeSec(long ftime) {
            return (int)(ftime % 86400L);
        }

        public static long GetDayKey(long ftime) {
            return ftime / 86400L;
        }

        public static int ChangeUnitTimeMinute(int fday, int fhour, int fminute) {
            return fday * 60 * 24 + fhour * 60 + fminute;
        }

        public static int ChangeUnitTimeSec(int fday, int fhour, int fminute) {
            return fday * TIME_HOUR * 24 + fhour * TIME_HOUR + fminute * 60;
        }

        public static long ChangeUnitTimeSecToBase(long fsec) {
            return fsec * BASE_TIME_SEC;
        }

        public static DateTime ChangeBaseTimeToDateTime(long fbasetime) {
            return DateTime.FromBinary(fbasetime + UnitBaseTime.Ticks);
        }

        public static DateTime GetManageUnixTime() {
            return DateTime.FromBinary(m_SettingTimeBase + UnitBaseTime.Ticks);
        }

        public static DateTime GetManageUniversalTime() {
            return DateTime.FromBinary(m_SettingTimeBase + UnitBaseTime.Ticks);
        }

        public static DateTime GetMsTimeToDateTime(long ftimes) {
            return DateTime.FromBinary(ftimes * BASE_TIME_MS + UnitBaseTime.Ticks);
        }
    }
}
