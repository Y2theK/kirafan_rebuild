﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000704 RID: 1796
	[Token(Token = "0x2000593")]
	[StructLayout(0, Size = 20)]
	public struct ScheduleUIData
	{
		// Token: 0x06001A46 RID: 6726 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600184E")]
		[Address(RVA = "0x100035094", Offset = "0x35094", VA = "0x100035094")]
		public string GetSegName()
		{
			return null;
		}

		// Token: 0x06001A47 RID: 6727 RVA: 0x0000BCB8 File Offset: 0x00009EB8
		[Token(Token = "0x600184F")]
		[Address(RVA = "0x1000350BC", Offset = "0x350BC", VA = "0x1000350BC")]
		public int GetSegNameID()
		{
			return 0;
		}

		// Token: 0x04002A7E RID: 10878
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40022AB")]
		public eScheduleTypeUIType m_Type;

		// Token: 0x04002A7F RID: 10879
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40022AC")]
		public int m_NameTagID;

		// Token: 0x04002A80 RID: 10880
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40022AD")]
		public int m_SegNameID;

		// Token: 0x04002A81 RID: 10881
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x40022AE")]
		public int m_WakeTime;

		// Token: 0x04002A82 RID: 10882
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022AF")]
		public int m_Time;
	}
}
