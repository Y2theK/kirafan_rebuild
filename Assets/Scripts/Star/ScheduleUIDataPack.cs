﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000705 RID: 1797
	[Token(Token = "0x2000594")]
	[StructLayout(3)]
	public class ScheduleUIDataPack
	{
		// Token: 0x06001A48 RID: 6728 RVA: 0x0000BCD0 File Offset: 0x00009ED0
		[Token(Token = "0x6001850")]
		[Address(RVA = "0x10130EEF4", Offset = "0x130EEF4", VA = "0x10130EEF4")]
		public int GetListNum()
		{
			return 0;
		}

		// Token: 0x06001A49 RID: 6729 RVA: 0x0000BCE8 File Offset: 0x00009EE8
		[Token(Token = "0x6001851")]
		[Address(RVA = "0x10130EF20", Offset = "0x130EF20", VA = "0x10130EF20")]
		public ScheduleUIData GetTable(int findex)
		{
			return default(ScheduleUIData);
		}

		// Token: 0x06001A4A RID: 6730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001852")]
		[Address(RVA = "0x10130EF8C", Offset = "0x130EF8C", VA = "0x10130EF8C")]
		public ScheduleUIDataPack()
		{
		}

		// Token: 0x04002A83 RID: 10883
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022B0")]
		public ScheduleUIData[] m_Table;
	}
}
