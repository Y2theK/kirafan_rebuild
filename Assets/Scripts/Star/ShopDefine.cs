﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A91 RID: 2705
	[Token(Token = "0x2000780")]
	[StructLayout(3)]
	public static class ShopDefine
	{
		// Token: 0x04003E18 RID: 15896
		[Token(Token = "0x4002CCF")]
		public static int ADV_ID_WPN_EVOLUTION;

		// Token: 0x02000A92 RID: 2706
		[Token(Token = "0x2000FDE")]
		public enum eWeaponUpgradeResult
		{
			// Token: 0x04003E1A RID: 15898
			[Token(Token = "0x40064B6")]
			Success,
			// Token: 0x04003E1B RID: 15899
			[Token(Token = "0x40064B7")]
			GoldIsShort,
			// Token: 0x04003E1C RID: 15900
			[Token(Token = "0x40064B8")]
			ItemIsShort,
			// Token: 0x04003E1D RID: 15901
			[Token(Token = "0x40064B9")]
			UpgradeLimit
		}

		// Token: 0x02000A93 RID: 2707
		[Token(Token = "0x2000FDF")]
		public enum eWeaponEvolutionResult
		{
			// Token: 0x04003E1F RID: 15903
			[Token(Token = "0x40064BB")]
			Success,
			// Token: 0x04003E20 RID: 15904
			[Token(Token = "0x40064BC")]
			GoldIsShort,
			// Token: 0x04003E21 RID: 15905
			[Token(Token = "0x40064BD")]
			ItemIsShort
		}

		// Token: 0x02000A94 RID: 2708
		[Token(Token = "0x2000FE0")]
		public enum eWeaponSaleResult
		{
			// Token: 0x04003E23 RID: 15907
			[Token(Token = "0x40064BF")]
			Success
		}

		// Token: 0x02000A95 RID: 2709
		[Token(Token = "0x2000FE1")]
		public enum eWeaponSkillLvUPResult
		{
			// Token: 0x04003E25 RID: 15909
			[Token(Token = "0x40064C1")]
			Success,
			// Token: 0x04003E26 RID: 15910
			[Token(Token = "0x40064C2")]
			ItemIsShort,
			// Token: 0x04003E27 RID: 15911
			[Token(Token = "0x40064C3")]
			skillLvLimit
		}
	}
}
