﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200082B RID: 2091
	[Token(Token = "0x2000624")]
	[StructLayout(3)]
	public class ShopMain : ShopMainBase
	{
		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06002123 RID: 8483 RVA: 0x0000E7A8 File Offset: 0x0000C9A8
		// (set) Token: 0x06002124 RID: 8484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000237")]
		public bool IsTradeListReset
		{
			[Token(Token = "0x6001E89")]
			[Address(RVA = "0x10130EFEC", Offset = "0x130EFEC", VA = "0x10130EFEC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001E8A")]
			[Address(RVA = "0x10130EFF4", Offset = "0x130EFF4", VA = "0x10130EFF4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06002125 RID: 8485 RVA: 0x0000E7C0 File Offset: 0x0000C9C0
		// (set) Token: 0x06002126 RID: 8486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000238")]
		public bool IsLimitList
		{
			[Token(Token = "0x6001E8B")]
			[Address(RVA = "0x10130EFFC", Offset = "0x130EFFC", VA = "0x10130EFFC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001E8C")]
			[Address(RVA = "0x10130F004", Offset = "0x130F004", VA = "0x10130F004")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06002127 RID: 8487 RVA: 0x0000E7D8 File Offset: 0x0000C9D8
		// (set) Token: 0x06002128 RID: 8488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000239")]
		public bool IsRequestVocie
		{
			[Token(Token = "0x6001E8D")]
			[Address(RVA = "0x10130F00C", Offset = "0x130F00C", VA = "0x10130F00C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6001E8E")]
			[Address(RVA = "0x10130F014", Offset = "0x130F014", VA = "0x10130F014")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06002129 RID: 8489 RVA: 0x0000E7F0 File Offset: 0x0000C9F0
		// (set) Token: 0x0600212A RID: 8490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700023A")]
		public long TargetWeaponMngID
		{
			[Token(Token = "0x6001E8F")]
			[Address(RVA = "0x10130F01C", Offset = "0x130F01C", VA = "0x10130F01C")]
			[CompilerGenerated]
			get
			{
				return 0L;
			}
			[Token(Token = "0x6001E90")]
			[Address(RVA = "0x10130F024", Offset = "0x130F024", VA = "0x10130F024")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x0600212B RID: 8491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E91")]
		[Address(RVA = "0x10130F02C", Offset = "0x130F02C", VA = "0x10130F02C")]
		private void Start()
		{
		}

		// Token: 0x0600212C RID: 8492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E92")]
		[Address(RVA = "0x10130F038", Offset = "0x130F038", VA = "0x10130F038")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600212D RID: 8493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E93")]
		[Address(RVA = "0x10130F040", Offset = "0x130F040", VA = "0x10130F040", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x0600212E RID: 8494 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001E94")]
		[Address(RVA = "0x10130F048", Offset = "0x130F048", VA = "0x10130F048", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x0600212F RID: 8495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E95")]
		[Address(RVA = "0x10130F8A4", Offset = "0x130F8A4", VA = "0x10130F8A4", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06002130 RID: 8496 RVA: 0x0000E808 File Offset: 0x0000CA08
		[Token(Token = "0x6001E96")]
		[Address(RVA = "0x10130F95C", Offset = "0x130F95C", VA = "0x10130F95C", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06002131 RID: 8497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E97")]
		[Address(RVA = "0x10130F964", Offset = "0x130F964", VA = "0x10130F964")]
		public ShopMain()
		{
		}

		// Token: 0x04003130 RID: 12592
		[Token(Token = "0x4002537")]
		public const int STATE_INIT = 0;

		// Token: 0x04003131 RID: 12593
		[Token(Token = "0x4002538")]
		public const int STATE_SHOP_TOP = 1;

		// Token: 0x04003132 RID: 12594
		[Token(Token = "0x4002539")]
		public const int STATE_SHOP_TRADE_TOP = 2;

		// Token: 0x04003133 RID: 12595
		[Token(Token = "0x400253A")]
		public const int STATE_SHOP_TRADE_LIST = 3;

		// Token: 0x04003134 RID: 12596
		[Token(Token = "0x400253B")]
		public const int STATE_SHOP_TRADE_SALE = 4;

		// Token: 0x04003135 RID: 12597
		[Token(Token = "0x400253C")]
		public const int STATE_SHOP_CHEST = 5;

		// Token: 0x04003136 RID: 12598
		[Token(Token = "0x400253D")]
		public const int STATE_SHOP_WEAPON_TOP = 10;

		// Token: 0x04003137 RID: 12599
		[Token(Token = "0x400253E")]
		public const int STATE_SHOP_WEAPON_CREATE = 11;

		// Token: 0x04003138 RID: 12600
		[Token(Token = "0x400253F")]
		public const int STATE_SHOP_WEAPON_UPGRADELIST = 12;

		// Token: 0x04003139 RID: 12601
		[Token(Token = "0x4002540")]
		public const int STATE_SHOP_WEAPON_UPGRADE = 13;

		// Token: 0x0400313A RID: 12602
		[Token(Token = "0x4002541")]
		public const int STATE_SHOP_WEAPON_SALE = 14;

		// Token: 0x0400313B RID: 12603
		[Token(Token = "0x4002542")]
		public const int STATE_SHOP_WEAPON_EVOLUTION_LIST = 15;

		// Token: 0x0400313C RID: 12604
		[Token(Token = "0x4002543")]
		public const int STATE_SHOP_WEAPON_EVOLUTION = 16;

		// Token: 0x0400313D RID: 12605
		[Token(Token = "0x4002544")]
		public const int STATE_SHOP_WEAPON_SKILL_LVUP_LIST = 17;

		// Token: 0x0400313E RID: 12606
		[Token(Token = "0x4002545")]
		public const int STATE_SHOP_WEAPON_SKILL_LVUP = 18;

		// Token: 0x0400313F RID: 12607
		[Token(Token = "0x4002546")]
		public const int STATE_SHOP_BUILD_TOP = 20;

		// Token: 0x04003140 RID: 12608
		[Token(Token = "0x4002547")]
		public const int STATE_SHOP_BUILD_BUY = 21;

		// Token: 0x04003141 RID: 12609
		[Token(Token = "0x4002548")]
		public const int STATE_SHOP_BUILD_SALE = 22;

		// Token: 0x04003142 RID: 12610
		[Token(Token = "0x4002549")]
		public const int STATE_SHOP_MARKET = 23;

		// Token: 0x04003145 RID: 12613
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400254C")]
		public NPCCharaDisplayUI m_NpcUI;
	}
}
