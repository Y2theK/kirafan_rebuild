﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x0200082A RID: 2090
	[Token(Token = "0x2000623")]
	[StructLayout(3)]
	public class ShopMainBase : GameStateMain
	{
		// Token: 0x0600211E RID: 8478 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001E84")]
		[Address(RVA = "0x10130F544", Offset = "0x130F544", VA = "0x10130F544")]
		public static ShopTradeTopUI.ShopTradeTopUISelectData PopNextSceneData()
		{
			return null;
		}

		// Token: 0x0600211F RID: 8479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E85")]
		[Address(RVA = "0x10130F618", Offset = "0x130F618", VA = "0x10130F618")]
		public static void PushNextSceneData(ShopTradeTopUI.ShopTradeTopUISelectData nextSceneData)
		{
		}

		// Token: 0x06002120 RID: 8480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E86")]
		[Address(RVA = "0x10130F974", Offset = "0x130F974", VA = "0x10130F974")]
		public static void ReserveTransitLimitTrade(int groupId)
		{
		}

		// Token: 0x06002121 RID: 8481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E87")]
		[Address(RVA = "0x10130FA0C", Offset = "0x130FA0C", VA = "0x10130FA0C")]
		public static void ReserveTransitChest(int chestID)
		{
		}

		// Token: 0x06002122 RID: 8482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E88")]
		[Address(RVA = "0x10130F96C", Offset = "0x130F96C", VA = "0x10130F96C")]
		public ShopMainBase()
		{
		}

		// Token: 0x0400312F RID: 12591
		[Token(Token = "0x4002536")]
		private static ShopTradeTopUI.ShopTradeTopUISelectData m_stNextSceneData;
	}
}
