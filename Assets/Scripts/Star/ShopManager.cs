﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemTradeResponseTypes;
using Meige;
using PlayerResponseTypes;
using RoomObjectResponseTypes;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000A96 RID: 2710
	[Token(Token = "0x2000781")]
	[StructLayout(3)]
	public sealed class ShopManager
	{
		// Token: 0x06002E3B RID: 11835 RVA: 0x00013AD0 File Offset: 0x00011CD0
		[Token(Token = "0x6002A52")]
		[Address(RVA = "0x10130FAA4", Offset = "0x130FAA4", VA = "0x10130FAA4")]
		public int GetTradeListNum()
		{
			return 0;
		}

		// Token: 0x06002E3C RID: 11836 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A53")]
		[Address(RVA = "0x10130FB04", Offset = "0x130FB04", VA = "0x10130FB04")]
		public ShopManager.TradeData GetTradeDataAt(int index)
		{
			return null;
		}

		// Token: 0x06002E3D RID: 11837 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A54")]
		[Address(RVA = "0x10130FBB8", Offset = "0x130FBB8", VA = "0x10130FBB8")]
		public ShopManager.TradeData GetTradeData(long id)
		{
			return null;
		}

		// Token: 0x06002E3E RID: 11838 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A55")]
		[Address(RVA = "0x10130FCB8", Offset = "0x130FCB8", VA = "0x10130FCB8")]
		public List<ShopManager.TradeData> GetTradeList(bool isLimited, int groupId)
		{
			return null;
		}

		// Token: 0x06002E3F RID: 11839 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A56")]
		[Address(RVA = "0x10130FD08", Offset = "0x130FD08", VA = "0x10130FD08")]
		public List<ShopManager.TradeData> GetTradeList(bool isLimited)
		{
			return null;
		}

		// Token: 0x06002E40 RID: 11840 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A57")]
		[Address(RVA = "0x10130FE54", Offset = "0x130FE54", VA = "0x10130FE54")]
		public List<ShopManager.TradeData> GetLimitTradeList(int groupId)
		{
			return null;
		}

		// Token: 0x06002E41 RID: 11841 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A58")]
		[Address(RVA = "0x10130FCC8", Offset = "0x130FCC8", VA = "0x10130FCC8")]
		public List<ShopManager.TradeData> FilterTradeList(List<ShopManager.TradeData> src, bool isLimited, int groupId)
		{
			return null;
		}

		// Token: 0x06002E42 RID: 11842 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A59")]
		[Address(RVA = "0x10130FFBC", Offset = "0x130FFBC", VA = "0x10130FFBC")]
		public List<ShopManager.TradeData> FilterTradeList(List<ShopManager.TradeData> src, bool isLimited)
		{
			return null;
		}

		// Token: 0x06002E43 RID: 11843 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A5A")]
		[Address(RVA = "0x10130FE60", Offset = "0x130FE60", VA = "0x10130FE60")]
		public List<ShopManager.TradeData> FilterTradeList(List<ShopManager.TradeData> src, int groupId)
		{
			return null;
		}

		// Token: 0x14000045 RID: 69
		// (add) Token: 0x06002E44 RID: 11844 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E45 RID: 11845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000045")]
		private event Action<MeigewwwParam, Getrecipe> m_OnResponse_GetTradeRecipe
		{
			[Token(Token = "0x6002A5B")]
			[Address(RVA = "0x101310114", Offset = "0x1310114", VA = "0x101310114")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A5C")]
			[Address(RVA = "0x101310220", Offset = "0x1310220", VA = "0x101310220")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000046 RID: 70
		// (add) Token: 0x06002E46 RID: 11846 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E47 RID: 11847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000046")]
		private event Action<MeigewwwParam, Trade> m_OnResponse_Trade
		{
			[Token(Token = "0x6002A5D")]
			[Address(RVA = "0x10131032C", Offset = "0x131032C", VA = "0x10131032C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A5E")]
			[Address(RVA = "0x101310438", Offset = "0x1310438", VA = "0x101310438")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000047 RID: 71
		// (add) Token: 0x06002E48 RID: 11848 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E49 RID: 11849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000047")]
		private event Action<MeigewwwParam, Itemsale> m_OnResponse_ItemSale
		{
			[Token(Token = "0x6002A5F")]
			[Address(RVA = "0x101310544", Offset = "0x1310544", VA = "0x101310544")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A60")]
			[Address(RVA = "0x101310650", Offset = "0x1310650", VA = "0x101310650")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000048 RID: 72
		// (add) Token: 0x06002E4A RID: 11850 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E4B RID: 11851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000048")]
		private event Action<MeigewwwParam, Weaponmake> m_OnResponse_WeaponMake
		{
			[Token(Token = "0x6002A61")]
			[Address(RVA = "0x10131075C", Offset = "0x131075C", VA = "0x10131075C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A62")]
			[Address(RVA = "0x101310868", Offset = "0x1310868", VA = "0x101310868")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000049 RID: 73
		// (add) Token: 0x06002E4C RID: 11852 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E4D RID: 11853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000049")]
		private event Action<ShopDefine.eWeaponUpgradeResult, string> m_OnResponse_WeaponUpgrade
		{
			[Token(Token = "0x6002A63")]
			[Address(RVA = "0x101310974", Offset = "0x1310974", VA = "0x101310974")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A64")]
			[Address(RVA = "0x101310A80", Offset = "0x1310A80", VA = "0x101310A80")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004A RID: 74
		// (add) Token: 0x06002E4E RID: 11854 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E4F RID: 11855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004A")]
		private event Action<ShopDefine.eWeaponEvolutionResult, string> m_OnResponse_WeaponEvolution
		{
			[Token(Token = "0x6002A65")]
			[Address(RVA = "0x101310B8C", Offset = "0x1310B8C", VA = "0x101310B8C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A66")]
			[Address(RVA = "0x101310C98", Offset = "0x1310C98", VA = "0x101310C98")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004B RID: 75
		// (add) Token: 0x06002E50 RID: 11856 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E51 RID: 11857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004B")]
		private event Action<ShopDefine.eWeaponSaleResult, string> m_OnResponse_WeaponSale
		{
			[Token(Token = "0x6002A67")]
			[Address(RVA = "0x101310DA4", Offset = "0x1310DA4", VA = "0x101310DA4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A68")]
			[Address(RVA = "0x101310EB0", Offset = "0x1310EB0", VA = "0x101310EB0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004C RID: 76
		// (add) Token: 0x06002E52 RID: 11858 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E53 RID: 11859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004C")]
		private event Action<MeigewwwParam, Weaponlimitadd> m_OnResponse_WeaponLimitAdd
		{
			[Token(Token = "0x6002A69")]
			[Address(RVA = "0x101310FBC", Offset = "0x1310FBC", VA = "0x101310FBC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A6A")]
			[Address(RVA = "0x1013110C8", Offset = "0x13110C8", VA = "0x1013110C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004D RID: 77
		// (add) Token: 0x06002E54 RID: 11860 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E55 RID: 11861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004D")]
		private event Action<MeigewwwParam, RoomObjectResponseTypes.Limitadd> m_OnResponse_RoomObjectLimitAdd
		{
			[Token(Token = "0x6002A6B")]
			[Address(RVA = "0x1013111D4", Offset = "0x13111D4", VA = "0x1013111D4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A6C")]
			[Address(RVA = "0x1013112E0", Offset = "0x13112E0", VA = "0x1013112E0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004E RID: 78
		// (add) Token: 0x06002E56 RID: 11862 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E57 RID: 11863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004E")]
		private event Action<MeigewwwParam, TownFacilityResponseTypes.Limitadd> m_OnResponse_TownFacilityLimitAdd
		{
			[Token(Token = "0x6002A6D")]
			[Address(RVA = "0x1013113EC", Offset = "0x13113EC", VA = "0x1013113EC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A6E")]
			[Address(RVA = "0x1013114F8", Offset = "0x13114F8", VA = "0x1013114F8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400004F RID: 79
		// (add) Token: 0x06002E58 RID: 11864 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002E59 RID: 11865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400004F")]
		private event Action<ShopDefine.eWeaponSkillLvUPResult, string> m_OnResponse_WeaponSkillLvUP
		{
			[Token(Token = "0x6002A6F")]
			[Address(RVA = "0x101311604", Offset = "0x1311604", VA = "0x101311604")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002A70")]
			[Address(RVA = "0x101311710", Offset = "0x1311710", VA = "0x101311710")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06002E5A RID: 11866 RVA: 0x00013AE8 File Offset: 0x00011CE8
		[Token(Token = "0x170002E7")]
		public int WeaponUpgradeSuccessType
		{
			[Token(Token = "0x6002A71")]
			[Address(RVA = "0x10131181C", Offset = "0x131181C", VA = "0x10131181C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06002E5B RID: 11867 RVA: 0x00013B00 File Offset: 0x00011D00
		[Token(Token = "0x6002A72")]
		[Address(RVA = "0x101311824", Offset = "0x1311824", VA = "0x101311824")]
		public bool Request_GetTradeRecipe(Action<MeigewwwParam, Getrecipe> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E5C RID: 11868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A73")]
		[Address(RVA = "0x101311958", Offset = "0x1311958", VA = "0x101311958")]
		private void OnResponse_GetTradeRecipe(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E5D RID: 11869 RVA: 0x00013B18 File Offset: 0x00011D18
		[Token(Token = "0x6002A74")]
		[Address(RVA = "0x10131206C", Offset = "0x131206C", VA = "0x10131206C")]
		private int SortComp(ShopManager.TradeData r, ShopManager.TradeData l)
		{
			return 0;
		}

		// Token: 0x06002E5E RID: 11870 RVA: 0x00013B30 File Offset: 0x00011D30
		[Token(Token = "0x6002A75")]
		[Address(RVA = "0x1013121E4", Offset = "0x13121E4", VA = "0x1013121E4")]
		public bool Request_Trade(int recipeID, int index, int count, Action<MeigewwwParam, Trade> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E5F RID: 11871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A76")]
		[Address(RVA = "0x101312364", Offset = "0x1312364", VA = "0x101312364")]
		private void OnResponse_Trade(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E60 RID: 11872 RVA: 0x00013B48 File Offset: 0x00011D48
		[Token(Token = "0x6002A77")]
		[Address(RVA = "0x1013125D0", Offset = "0x13125D0", VA = "0x1013125D0")]
		public bool Request_ItemSale(int itemID, int amount, Action<MeigewwwParam, Itemsale> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E61 RID: 11873 RVA: 0x00013B60 File Offset: 0x00011D60
		[Token(Token = "0x6002A78")]
		[Address(RVA = "0x1013126C4", Offset = "0x13126C4", VA = "0x1013126C4")]
		public bool Request_ItemSale(List<int> itemIDs, List<int> amounts, Action<MeigewwwParam, Itemsale> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E62 RID: 11874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A79")]
		[Address(RVA = "0x101312870", Offset = "0x1312870", VA = "0x101312870")]
		private void OnResponse_ItemSale(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E63 RID: 11875 RVA: 0x00013B78 File Offset: 0x00011D78
		[Token(Token = "0x6002A7A")]
		[Address(RVA = "0x1013129BC", Offset = "0x13129BC", VA = "0x1013129BC")]
		public bool Request_WeaponMake(int recipeID, Action<MeigewwwParam, Weaponmake> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E64 RID: 11876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A7B")]
		[Address(RVA = "0x101312B04", Offset = "0x1312B04", VA = "0x101312B04")]
		private void OnResponse_WeaponMake(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E65 RID: 11877 RVA: 0x00013B90 File Offset: 0x00011D90
		[Token(Token = "0x6002A7C")]
		[Address(RVA = "0x101312C7C", Offset = "0x1312C7C", VA = "0x101312C7C")]
		public bool Request_WeaponUpgrade(long weaponMngID, List<KeyValuePair<int, int>> list, Action<ShopDefine.eWeaponUpgradeResult, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E66 RID: 11878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A7D")]
		[Address(RVA = "0x101313004", Offset = "0x1313004", VA = "0x101313004")]
		private void OnResponse_WeaponUpgrade(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E67 RID: 11879 RVA: 0x00013BA8 File Offset: 0x00011DA8
		[Token(Token = "0x6002A7E")]
		[Address(RVA = "0x10131323C", Offset = "0x131323C", VA = "0x10131323C")]
		public bool Request_WeaponEvolution(long weaponMngID, int recipeID, Action<ShopDefine.eWeaponEvolutionResult, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E68 RID: 11880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A7F")]
		[Address(RVA = "0x1013133A8", Offset = "0x13133A8", VA = "0x1013133A8")]
		private void OnResponse_WeaponEvolution(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E69 RID: 11881 RVA: 0x00013BC0 File Offset: 0x00011DC0
		[Token(Token = "0x6002A80")]
		[Address(RVA = "0x101313600", Offset = "0x1313600", VA = "0x101313600")]
		public bool Request_WeaponSale(List<long> weaponMngIDs, Action<ShopDefine.eWeaponSaleResult, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E6A RID: 11882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A81")]
		[Address(RVA = "0x10131377C", Offset = "0x131377C", VA = "0x10131377C")]
		private void OnResponse_WeaponSale(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E6B RID: 11883 RVA: 0x00013BD8 File Offset: 0x00011DD8
		[Token(Token = "0x6002A82")]
		[Address(RVA = "0x1013138B8", Offset = "0x13138B8", VA = "0x1013138B8")]
		public bool Request_WeaponLimitAdd(Action<MeigewwwParam, Weaponlimitadd> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E6C RID: 11884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A83")]
		[Address(RVA = "0x101313A00", Offset = "0x1313A00", VA = "0x101313A00")]
		private void OnResponse_WeaponLimitAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E6D RID: 11885 RVA: 0x00013BF0 File Offset: 0x00011DF0
		[Token(Token = "0x6002A84")]
		[Address(RVA = "0x101313BC4", Offset = "0x1313BC4", VA = "0x101313BC4")]
		public bool Request_RoomObjectLimitAdd(int num, Action<MeigewwwParam, RoomObjectResponseTypes.Limitadd> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E6E RID: 11886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A85")]
		[Address(RVA = "0x101313D0C", Offset = "0x1313D0C", VA = "0x101313D0C")]
		private void OnResponse_RoomObjectLimitAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E6F RID: 11887 RVA: 0x00013C08 File Offset: 0x00011E08
		[Token(Token = "0x6002A86")]
		[Address(RVA = "0x101313ED0", Offset = "0x1313ED0", VA = "0x101313ED0")]
		public bool Request_TownFacilityLimitAdd(Action<MeigewwwParam, TownFacilityResponseTypes.Limitadd> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E70 RID: 11888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A87")]
		[Address(RVA = "0x101314018", Offset = "0x1314018", VA = "0x101314018")]
		private void OnResponse_TownFacilityLimitAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E71 RID: 11889 RVA: 0x00013C20 File Offset: 0x00011E20
		[Token(Token = "0x6002A88")]
		[Address(RVA = "0x1013141DC", Offset = "0x13141DC", VA = "0x1013141DC")]
		public bool Request_WeaponSkillLvUP(long weaponMngID, int[] itemIDs, int[] itemAmounts, Action<ShopDefine.eWeaponSkillLvUPResult, string> onResponse)
		{
			return default(bool);
		}

		// Token: 0x06002E72 RID: 11890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A89")]
		[Address(RVA = "0x10131435C", Offset = "0x131435C", VA = "0x10131435C")]
		private void OnResponse_WeaponSkillLvUP(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002E73 RID: 11891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A8A")]
		[Address(RVA = "0x101314508", Offset = "0x1314508", VA = "0x101314508")]
		public ShopManager()
		{
		}

		// Token: 0x04003E28 RID: 15912
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002CD0")]
		private List<ShopManager.TradeData> m_TradeList;

		// Token: 0x04003E29 RID: 15913
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002CD1")]
		private int m_SelectedRecipeID;

		// Token: 0x04003E35 RID: 15925
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002CDD")]
		private int m_WeaponUpgradeSuccessType;

		// Token: 0x02000A97 RID: 2711
		[Token(Token = "0x2000FE2")]
		public class TradeSrcData
		{
			// Token: 0x06002E74 RID: 11892 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60060A2")]
			[Address(RVA = "0x101314B78", Offset = "0x1314B78", VA = "0x101314B78")]
			public string GetSrcAmountString()
			{
				return null;
			}

			// Token: 0x06002E75 RID: 11893 RVA: 0x00013C38 File Offset: 0x00011E38
			[Token(Token = "0x60060A3")]
			[Address(RVA = "0x101314F34", Offset = "0x1314F34", VA = "0x101314F34")]
			public bool IsActiveSrc()
			{
				return default(bool);
			}

			// Token: 0x06002E76 RID: 11894 RVA: 0x00013C50 File Offset: 0x00011E50
			[Token(Token = "0x60060A4")]
			[Address(RVA = "0x101314D38", Offset = "0x1314D38", VA = "0x101314D38")]
			public bool IsEnoughSrc()
			{
				return default(bool);
			}

			// Token: 0x06002E77 RID: 11895 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060A5")]
			[Address(RVA = "0x101312064", Offset = "0x1312064", VA = "0x101312064")]
			public TradeSrcData()
			{
			}

			// Token: 0x04003E36 RID: 15926
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064C4")]
			public ePresentType m_SrcType;

			// Token: 0x04003E37 RID: 15927
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40064C5")]
			public int m_SrcID;

			// Token: 0x04003E38 RID: 15928
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064C6")]
			public int m_SrcAmount;
		}

		// Token: 0x02000A98 RID: 2712
		[Token(Token = "0x2000FE3")]
		public class TradeData
		{
			// Token: 0x17000327 RID: 807
			// (get) Token: 0x06002E78 RID: 11896 RVA: 0x00013C68 File Offset: 0x00011E68
			[Token(Token = "0x170006B5")]
			public int RemainNum
			{
				[Token(Token = "0x60060A6")]
				[Address(RVA = "0x10131214C", Offset = "0x131214C", VA = "0x10131214C")]
				get
				{
					return 0;
				}
			}

			// Token: 0x17000328 RID: 808
			// (get) Token: 0x06002E79 RID: 11897 RVA: 0x00013C80 File Offset: 0x00011E80
			[Token(Token = "0x170006B6")]
			public TimeSpan? DeadLineSpan
			{
				[Token(Token = "0x60060A7")]
				[Address(RVA = "0x101314580", Offset = "0x1314580", VA = "0x101314580")]
				get
				{
					return null;
				}
			}

			// Token: 0x06002E7A RID: 11898 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60060A8")]
			[Address(RVA = "0x101314830", Offset = "0x1314830", VA = "0x101314830")]
			public string GetDstName()
			{
				return null;
			}

			// Token: 0x06002E7B RID: 11899 RVA: 0x00013C98 File Offset: 0x00011E98
			[Token(Token = "0x60060A9")]
			[Address(RVA = "0x101314AA0", Offset = "0x1314AA0", VA = "0x101314AA0")]
			public int HowManySrcDatas()
			{
				return 0;
			}

			// Token: 0x06002E7C RID: 11900 RVA: 0x00013CB0 File Offset: 0x00011EB0
			[Token(Token = "0x60060AA")]
			[Address(RVA = "0x101314AB8", Offset = "0x1314AB8", VA = "0x101314AB8")]
			public bool IsAvailable()
			{
				return default(bool);
			}

			// Token: 0x06002E7D RID: 11901 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060AB")]
			[Address(RVA = "0x101312054", Offset = "0x1312054", VA = "0x101312054")]
			public TradeData()
			{
			}

			// Token: 0x04003E39 RID: 15929
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064C7")]
			public int m_ID;

			// Token: 0x04003E3A RID: 15930
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40064C8")]
			public bool m_IsLimited;

			// Token: 0x04003E3B RID: 15931
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064C9")]
			public ePresentType m_DstType;

			// Token: 0x04003E3C RID: 15932
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40064CA")]
			public int m_DstID;

			// Token: 0x04003E3D RID: 15933
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40064CB")]
			public int m_DstAmount;

			// Token: 0x04003E3E RID: 15934
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40064CC")]
			public ePresentType m_AltType;

			// Token: 0x04003E3F RID: 15935
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40064CD")]
			public int m_AltID;

			// Token: 0x04003E40 RID: 15936
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40064CE")]
			public int m_AltAmount;

			// Token: 0x04003E41 RID: 15937
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40064CF")]
			public ShopManager.TradeSrcData[] m_SrcDatas;

			// Token: 0x04003E42 RID: 15938
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40064D0")]
			public DateTime? m_StartAt;

			// Token: 0x04003E43 RID: 15939
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40064D1")]
			public DateTime? m_EndAt;

			// Token: 0x04003E44 RID: 15940
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x40064D2")]
			public int m_LimitCount;

			// Token: 0x04003E45 RID: 15941
			[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
			[Token(Token = "0x40064D3")]
			public int m_TradeMax;

			// Token: 0x04003E46 RID: 15942
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x40064D4")]
			public int m_ResetFlag;

			// Token: 0x04003E47 RID: 15943
			[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
			[Token(Token = "0x40064D5")]
			public int m_TotalTradeCount;

			// Token: 0x04003E48 RID: 15944
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x40064D6")]
			public int m_MonthlyTradeCount;

			// Token: 0x04003E49 RID: 15945
			[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
			[Token(Token = "0x40064D7")]
			public int m_Group;

			// Token: 0x04003E4A RID: 15946
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x40064D8")]
			public int m_EventType;

			// Token: 0x04003E4B RID: 15947
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x40064D9")]
			public int m_Sort;

			// Token: 0x04003E4C RID: 15948
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x40064DA")]
			public int m_LabelID;
		}
	}
}
