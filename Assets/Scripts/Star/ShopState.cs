﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemTradeResponseTypes;
using Meige;

namespace Star
{
	// Token: 0x0200082C RID: 2092
	[Token(Token = "0x2000625")]
	[StructLayout(3)]
	public class ShopState : GameStateBase
	{
		// Token: 0x06002132 RID: 8498 RVA: 0x0000E820 File Offset: 0x0000CA20
		[Token(Token = "0x6001E98")]
		[Address(RVA = "0x101314F44", Offset = "0x1314F44", VA = "0x101314F44")]
		protected static bool IsWaitResponse(ShopState.eIsRequestState state)
		{
			return default(bool);
		}

		// Token: 0x06002133 RID: 8499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E99")]
		[Address(RVA = "0x101314F58", Offset = "0x1314F58", VA = "0x101314F58")]
		public ShopState(ShopMain owner)
		{
		}

		// Token: 0x06002134 RID: 8500 RVA: 0x0000E838 File Offset: 0x0000CA38
		[Token(Token = "0x6001E9A")]
		[Address(RVA = "0x101314F8C", Offset = "0x1314F8C", VA = "0x101314F8C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002135 RID: 8501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E9B")]
		[Address(RVA = "0x101314F94", Offset = "0x1314F94", VA = "0x101314F94", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002136 RID: 8502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E9C")]
		[Address(RVA = "0x101314F98", Offset = "0x1314F98", VA = "0x101314F98", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002137 RID: 8503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001E9D")]
		[Address(RVA = "0x101314F9C", Offset = "0x1314F9C", VA = "0x101314F9C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002138 RID: 8504 RVA: 0x0000E850 File Offset: 0x0000CA50
		[Token(Token = "0x6001E9E")]
		[Address(RVA = "0x101314FA0", Offset = "0x1314FA0", VA = "0x101314FA0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002139 RID: 8505 RVA: 0x0000E868 File Offset: 0x0000CA68
		[Token(Token = "0x6001E9F")]
		[Address(RVA = "0x101314FA8", Offset = "0x1314FA8", VA = "0x101314FA8")]
		protected ShopState.eIsRequestState RequestGetTradeListFirstStep()
		{
			return ShopState.eIsRequestState.Error;
		}

		// Token: 0x0600213A RID: 8506 RVA: 0x0000E880 File Offset: 0x0000CA80
		[Token(Token = "0x6001EA0")]
		[Address(RVA = "0x1013150E8", Offset = "0x13150E8", VA = "0x1013150E8")]
		protected ShopState.eIsRequestState GetGetTradeListRequestState()
		{
			return ShopState.eIsRequestState.Error;
		}

		// Token: 0x0600213B RID: 8507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA1")]
		[Address(RVA = "0x1013150F0", Offset = "0x13150F0", VA = "0x1013150F0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600213C RID: 8508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA2")]
		[Address(RVA = "0x1013150F4", Offset = "0x13150F4", VA = "0x1013150F4")]
		private void OnResponse_GetRecipe(MeigewwwParam wwwParam, Getrecipe param)
		{
		}

		// Token: 0x0600213D RID: 8509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA3")]
		[Address(RVA = "0x1013151FC", Offset = "0x13151FC", VA = "0x1013151FC")]
		private void OnResponse_GetChest(ChestDefine.eReturnChestGetList result, string errMsg)
		{
		}

		// Token: 0x04003148 RID: 12616
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400254F")]
		protected ShopMain m_Owner;

		// Token: 0x04003149 RID: 12617
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002550")]
		protected int m_NextState;

		// Token: 0x0400314A RID: 12618
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002551")]
		private ShopState.eIsRequestState m_GetTradeListRequestState;

		// Token: 0x0200082D RID: 2093
		[Token(Token = "0x2000ED7")]
		protected enum eIsRequestState
		{
			// Token: 0x0400314C RID: 12620
			[Token(Token = "0x4005F6D")]
			Error,
			// Token: 0x0400314D RID: 12621
			[Token(Token = "0x4005F6E")]
			JustSended,
			// Token: 0x0400314E RID: 12622
			[Token(Token = "0x4005F6F")]
			NotSended,
			// Token: 0x0400314F RID: 12623
			[Token(Token = "0x4005F70")]
			WaitResponse,
			// Token: 0x04003150 RID: 12624
			[Token(Token = "0x4005F71")]
			ReceivedResponse
		}
	}
}
