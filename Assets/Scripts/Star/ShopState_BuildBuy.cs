﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;

namespace Star
{
	// Token: 0x0200082E RID: 2094
	[Token(Token = "0x2000626")]
	[StructLayout(3)]
	public class ShopState_BuildBuy : ShopState
	{
		// Token: 0x0600213E RID: 8510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA4")]
		[Address(RVA = "0x10130F73C", Offset = "0x130F73C", VA = "0x10130F73C")]
		public ShopState_BuildBuy(ShopMain owner)
		{
		}

		// Token: 0x0600213F RID: 8511 RVA: 0x0000E898 File Offset: 0x0000CA98
		[Token(Token = "0x6001EA5")]
		[Address(RVA = "0x10131521C", Offset = "0x131521C", VA = "0x10131521C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002140 RID: 8512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA6")]
		[Address(RVA = "0x101315224", Offset = "0x1315224", VA = "0x101315224", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002141 RID: 8513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA7")]
		[Address(RVA = "0x10131522C", Offset = "0x131522C", VA = "0x10131522C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002142 RID: 8514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EA8")]
		[Address(RVA = "0x101315230", Offset = "0x1315230", VA = "0x101315230", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002143 RID: 8515 RVA: 0x0000E8B0 File Offset: 0x0000CAB0
		[Token(Token = "0x6001EA9")]
		[Address(RVA = "0x101315238", Offset = "0x1315238", VA = "0x101315238", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002144 RID: 8516 RVA: 0x0000E8C8 File Offset: 0x0000CAC8
		[Token(Token = "0x6001EAA")]
		[Address(RVA = "0x101315488", Offset = "0x1315488", VA = "0x101315488")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002145 RID: 8517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EAB")]
		[Address(RVA = "0x1013156A8", Offset = "0x13156A8", VA = "0x1013156A8", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002146 RID: 8518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EAC")]
		[Address(RVA = "0x101315748", Offset = "0x1315748", VA = "0x101315748")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002147 RID: 8519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EAD")]
		[Address(RVA = "0x1013156AC", Offset = "0x13156AC", VA = "0x1013156AC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003151 RID: 12625
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002552")]
		private ShopState_BuildBuy.eStep m_Step;

		// Token: 0x04003152 RID: 12626
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002553")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003153 RID: 12627
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002554")]
		private RoomShopListUI m_UI;

		// Token: 0x04003154 RID: 12628
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002555")]
		private RoomObjShopList m_RoomObjShopList;

		// Token: 0x0200082F RID: 2095
		[Token(Token = "0x2000ED8")]
		private enum eStep
		{
			// Token: 0x04003156 RID: 12630
			[Token(Token = "0x4005F73")]
			None = -1,
			// Token: 0x04003157 RID: 12631
			[Token(Token = "0x4005F74")]
			First,
			// Token: 0x04003158 RID: 12632
			[Token(Token = "0x4005F75")]
			LoadStart,
			// Token: 0x04003159 RID: 12633
			[Token(Token = "0x4005F76")]
			LoadWait,
			// Token: 0x0400315A RID: 12634
			[Token(Token = "0x4005F77")]
			PlayIn,
			// Token: 0x0400315B RID: 12635
			[Token(Token = "0x4005F78")]
			Main,
			// Token: 0x0400315C RID: 12636
			[Token(Token = "0x4005F79")]
			UnloadChildSceneWait
		}
	}
}
