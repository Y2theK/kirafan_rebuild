﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Room;

namespace Star
{
	// Token: 0x02000830 RID: 2096
	[Token(Token = "0x2000627")]
	[StructLayout(3)]
	public class ShopState_BuildSale : ShopState
	{
		// Token: 0x06002148 RID: 8520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EAE")]
		[Address(RVA = "0x10130F7CC", Offset = "0x130F7CC", VA = "0x10130F7CC")]
		public ShopState_BuildSale(ShopMain owner)
		{
		}

		// Token: 0x06002149 RID: 8521 RVA: 0x0000E8E0 File Offset: 0x0000CAE0
		[Token(Token = "0x6001EAF")]
		[Address(RVA = "0x10131574C", Offset = "0x131574C", VA = "0x10131574C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600214A RID: 8522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB0")]
		[Address(RVA = "0x101315754", Offset = "0x1315754", VA = "0x101315754", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600214B RID: 8523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB1")]
		[Address(RVA = "0x10131575C", Offset = "0x131575C", VA = "0x10131575C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600214C RID: 8524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB2")]
		[Address(RVA = "0x101315760", Offset = "0x1315760", VA = "0x101315760", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600214D RID: 8525 RVA: 0x0000E8F8 File Offset: 0x0000CAF8
		[Token(Token = "0x6001EB3")]
		[Address(RVA = "0x101315768", Offset = "0x1315768", VA = "0x101315768", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600214E RID: 8526 RVA: 0x0000E910 File Offset: 0x0000CB10
		[Token(Token = "0x6001EB4")]
		[Address(RVA = "0x1013159B8", Offset = "0x13159B8", VA = "0x1013159B8")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600214F RID: 8527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB5")]
		[Address(RVA = "0x101315B84", Offset = "0x1315B84", VA = "0x101315B84", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002150 RID: 8528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB6")]
		[Address(RVA = "0x101315C24", Offset = "0x1315C24", VA = "0x101315C24")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002151 RID: 8529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB7")]
		[Address(RVA = "0x101315B88", Offset = "0x1315B88", VA = "0x101315B88")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0400315D RID: 12637
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002556")]
		private ShopState_BuildSale.eStep m_Step;

		// Token: 0x0400315E RID: 12638
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002557")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400315F RID: 12639
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002558")]
		private RoomShopListUI m_UI;

		// Token: 0x04003160 RID: 12640
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002559")]
		private RoomObjShopList m_RoomObjShopList;

		// Token: 0x02000831 RID: 2097
		[Token(Token = "0x2000ED9")]
		private enum eStep
		{
			// Token: 0x04003162 RID: 12642
			[Token(Token = "0x4005F7B")]
			None = -1,
			// Token: 0x04003163 RID: 12643
			[Token(Token = "0x4005F7C")]
			First,
			// Token: 0x04003164 RID: 12644
			[Token(Token = "0x4005F7D")]
			LoadStart,
			// Token: 0x04003165 RID: 12645
			[Token(Token = "0x4005F7E")]
			LoadWait,
			// Token: 0x04003166 RID: 12646
			[Token(Token = "0x4005F7F")]
			PlayIn,
			// Token: 0x04003167 RID: 12647
			[Token(Token = "0x4005F80")]
			Main,
			// Token: 0x04003168 RID: 12648
			[Token(Token = "0x4005F81")]
			UnloadChildSceneWait
		}
	}
}
