﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using RoomObjectResponseTypes;
using Star.UI.Shop;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000832 RID: 2098
	[Token(Token = "0x2000628")]
	[StructLayout(3)]
	public class ShopState_BuildTop : ShopState
	{
		// Token: 0x06002152 RID: 8530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EB8")]
		[Address(RVA = "0x10130F6FC", Offset = "0x130F6FC", VA = "0x10130F6FC")]
		public ShopState_BuildTop(ShopMain owner)
		{
		}

		// Token: 0x06002153 RID: 8531 RVA: 0x0000E928 File Offset: 0x0000CB28
		[Token(Token = "0x6001EB9")]
		[Address(RVA = "0x101315C28", Offset = "0x1315C28", VA = "0x101315C28", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002154 RID: 8532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EBA")]
		[Address(RVA = "0x101315C30", Offset = "0x1315C30", VA = "0x101315C30", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002155 RID: 8533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EBB")]
		[Address(RVA = "0x101315C38", Offset = "0x1315C38", VA = "0x101315C38", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002156 RID: 8534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EBC")]
		[Address(RVA = "0x101315C3C", Offset = "0x1315C3C", VA = "0x101315C3C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002157 RID: 8535 RVA: 0x0000E940 File Offset: 0x0000CB40
		[Token(Token = "0x6001EBD")]
		[Address(RVA = "0x101315C44", Offset = "0x1315C44", VA = "0x101315C44", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002158 RID: 8536 RVA: 0x0000E958 File Offset: 0x0000CB58
		[Token(Token = "0x6001EBE")]
		[Address(RVA = "0x101315F28", Offset = "0x1315F28", VA = "0x101315F28")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002159 RID: 8537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EBF")]
		[Address(RVA = "0x101316260", Offset = "0x1316260", VA = "0x101316260", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600215A RID: 8538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC0")]
		[Address(RVA = "0x1013163F4", Offset = "0x13163F4", VA = "0x1013163F4")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600215B RID: 8539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC1")]
		[Address(RVA = "0x101317AF0", Offset = "0x1317AF0", VA = "0x101317AF0")]
		private void OnConfirmTutorial(int btn)
		{
		}

		// Token: 0x0600215C RID: 8540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC2")]
		[Address(RVA = "0x1013165E0", Offset = "0x13165E0", VA = "0x1013165E0")]
		private void LimitExtend()
		{
		}

		// Token: 0x0600215D RID: 8541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC3")]
		[Address(RVA = "0x101317068", Offset = "0x1317068", VA = "0x101317068")]
		private void LimitExtendTown()
		{
		}

		// Token: 0x0600215E RID: 8542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC4")]
		[Address(RVA = "0x101317B60", Offset = "0x1317B60", VA = "0x101317B60")]
		private void OnConfirmLimitAdd(int btn)
		{
		}

		// Token: 0x0600215F RID: 8543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC5")]
		[Address(RVA = "0x101317C38", Offset = "0x1317C38", VA = "0x101317C38")]
		private void OnResponseLimitAdd(MeigewwwParam wwwParam, RoomObjectResponseTypes.Limitadd param)
		{
		}

		// Token: 0x06002160 RID: 8544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC6")]
		[Address(RVA = "0x101317C7C", Offset = "0x1317C7C", VA = "0x101317C7C")]
		private void OnConfirmLimitAddTown(int btn)
		{
		}

		// Token: 0x06002161 RID: 8545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC7")]
		[Address(RVA = "0x101317D50", Offset = "0x1317D50", VA = "0x101317D50")]
		private void OnResponseLimitAddTown(MeigewwwParam wwwParam, TownFacilityResponseTypes.Limitadd param)
		{
		}

		// Token: 0x06002162 RID: 8546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC8")]
		[Address(RVA = "0x1013163C0", Offset = "0x13163C0", VA = "0x1013163C0")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003169 RID: 12649
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400255A")]
		private ShopState_BuildTop.eStep m_Step;

		// Token: 0x0400316A RID: 12650
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400255B")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400316B RID: 12651
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400255C")]
		private ShopBuildTopUI m_UI;

		// Token: 0x02000833 RID: 2099
		[Token(Token = "0x2000EDA")]
		private enum eStep
		{
			// Token: 0x0400316D RID: 12653
			[Token(Token = "0x4005F83")]
			None = -1,
			// Token: 0x0400316E RID: 12654
			[Token(Token = "0x4005F84")]
			First,
			// Token: 0x0400316F RID: 12655
			[Token(Token = "0x4005F85")]
			LoadStart,
			// Token: 0x04003170 RID: 12656
			[Token(Token = "0x4005F86")]
			LoadWait,
			// Token: 0x04003171 RID: 12657
			[Token(Token = "0x4005F87")]
			PlayIn,
			// Token: 0x04003172 RID: 12658
			[Token(Token = "0x4005F88")]
			Main,
			// Token: 0x04003173 RID: 12659
			[Token(Token = "0x4005F89")]
			UnloadChildSceneWait
		}
	}
}
