﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000834 RID: 2100
	[Token(Token = "0x2000629")]
	[StructLayout(3)]
	public class ShopState_Chest : ShopState
	{
		// Token: 0x06002163 RID: 8547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EC9")]
		[Address(RVA = "0x10130F6B8", Offset = "0x130F6B8", VA = "0x10130F6B8")]
		public ShopState_Chest(ShopMain owner)
		{
		}

		// Token: 0x06002164 RID: 8548 RVA: 0x0000E970 File Offset: 0x0000CB70
		[Token(Token = "0x6001ECA")]
		[Address(RVA = "0x101317D94", Offset = "0x1317D94", VA = "0x101317D94", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002165 RID: 8549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ECB")]
		[Address(RVA = "0x101317D9C", Offset = "0x1317D9C", VA = "0x101317D9C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002166 RID: 8550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ECC")]
		[Address(RVA = "0x101317DA4", Offset = "0x1317DA4", VA = "0x101317DA4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002167 RID: 8551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ECD")]
		[Address(RVA = "0x101317DA8", Offset = "0x1317DA8", VA = "0x101317DA8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002168 RID: 8552 RVA: 0x0000E988 File Offset: 0x0000CB88
		[Token(Token = "0x6001ECE")]
		[Address(RVA = "0x101317DB0", Offset = "0x1317DB0", VA = "0x101317DB0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002169 RID: 8553 RVA: 0x0000E9A0 File Offset: 0x0000CBA0
		[Token(Token = "0x6001ECF")]
		[Address(RVA = "0x101318168", Offset = "0x1318168", VA = "0x101318168")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600216A RID: 8554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED0")]
		[Address(RVA = "0x101318FC4", Offset = "0x1318FC4", VA = "0x101318FC4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600216B RID: 8555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED1")]
		[Address(RVA = "0x101319138", Offset = "0x1319138", VA = "0x101319138")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600216C RID: 8556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED2")]
		[Address(RVA = "0x101318FE8", Offset = "0x1318FE8", VA = "0x101318FE8")]
		private void SetTransitTradeList()
		{
		}

		// Token: 0x0600216D RID: 8557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED3")]
		[Address(RVA = "0x10131909C", Offset = "0x131909C", VA = "0x10131909C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED4")]
		[Address(RVA = "0x1013195B0", Offset = "0x13195B0", VA = "0x1013195B0")]
		private void GoToMenuEndWithError()
		{
		}

		// Token: 0x0600216F RID: 8559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED5")]
		[Address(RVA = "0x101318E50", Offset = "0x1318E50", VA = "0x101318E50")]
		private void PlayInNpc()
		{
		}

		// Token: 0x06002170 RID: 8560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED6")]
		[Address(RVA = "0x10131966C", Offset = "0x131966C", VA = "0x10131966C")]
		private void PlayOpenChestEffect(int toPresentCount)
		{
		}

		// Token: 0x06002171 RID: 8561 RVA: 0x0000E9B8 File Offset: 0x0000CBB8
		[Token(Token = "0x6001ED7")]
		[Address(RVA = "0x1013185C0", Offset = "0x13185C0", VA = "0x1013185C0")]
		private bool ExecOpenChestEffect()
		{
			return default(bool);
		}

		// Token: 0x06002172 RID: 8562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001ED8")]
		[Address(RVA = "0x101319680", Offset = "0x1319680", VA = "0x101319680")]
		private void PlayChestReset()
		{
		}

		// Token: 0x06002173 RID: 8563 RVA: 0x0000E9D0 File Offset: 0x0000CBD0
		[Token(Token = "0x6001ED9")]
		[Address(RVA = "0x101318B30", Offset = "0x1318B30", VA = "0x101318B30")]
		private bool ExecChestReset()
		{
			return default(bool);
		}

		// Token: 0x06002174 RID: 8564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EDA")]
		[Address(RVA = "0x101319690", Offset = "0x1319690", VA = "0x101319690")]
		private void ChestDrawCallback(ChestDefine.eReturnChestDraw resultCode, int toPresentCount, string errMsg)
		{
		}

		// Token: 0x06002175 RID: 8565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EDB")]
		[Address(RVA = "0x1013197DC", Offset = "0x13197DC", VA = "0x1013197DC")]
		private void ChestGetStepCallback(ChestDefine.eReturnChestGetProgress resultCode, string errMsg)
		{
		}

		// Token: 0x06002176 RID: 8566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EDC")]
		[Address(RVA = "0x1013198A8", Offset = "0x13198A8", VA = "0x1013198A8")]
		private void ChestResetCallback(ChestDefine.eReturnChestReset resultCode, string errMsg)
		{
		}

		// Token: 0x04003174 RID: 12660
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400255D")]
		private ShopState_Chest.eChestErrorCode m_ChestErrorCode;

		// Token: 0x04003175 RID: 12661
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400255E")]
		private ShopState_Chest.eStep m_Step;

		// Token: 0x04003176 RID: 12662
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400255F")]
		private ShopState_Chest.eEffectStep m_EffectStep;

		// Token: 0x04003177 RID: 12663
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002560")]
		private ShopState_Chest.eChestResetStep m_ChestResetStep;

		// Token: 0x04003178 RID: 12664
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002561")]
		private bool m_PlayingOpenChestEffect;

		// Token: 0x04003179 RID: 12665
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4002562")]
		private bool m_PlayingChestReset;

		// Token: 0x0400317A RID: 12666
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002563")]
		private int m_ToPresentCount;

		// Token: 0x0400317B RID: 12667
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002564")]
		private int m_ChestID;

		// Token: 0x0400317C RID: 12668
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002565")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400317D RID: 12669
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002566")]
		private ShopChestUI m_UI;

		// Token: 0x02000835 RID: 2101
		[Token(Token = "0x2000EDB")]
		private enum eChestErrorCode
		{
			// Token: 0x0400317F RID: 12671
			[Token(Token = "0x4005F8B")]
			None,
			// Token: 0x04003180 RID: 12672
			[Token(Token = "0x4005F8C")]
			TradeList
		}

		// Token: 0x02000836 RID: 2102
		[Token(Token = "0x2000EDC")]
		private enum eStep
		{
			// Token: 0x04003182 RID: 12674
			[Token(Token = "0x4005F8E")]
			None = -1,
			// Token: 0x04003183 RID: 12675
			[Token(Token = "0x4005F8F")]
			First,
			// Token: 0x04003184 RID: 12676
			[Token(Token = "0x4005F90")]
			Request_Wait,
			// Token: 0x04003185 RID: 12677
			[Token(Token = "0x4005F91")]
			CheckExist,
			// Token: 0x04003186 RID: 12678
			[Token(Token = "0x4005F92")]
			LoadStart,
			// Token: 0x04003187 RID: 12679
			[Token(Token = "0x4005F93")]
			LoadWait,
			// Token: 0x04003188 RID: 12680
			[Token(Token = "0x4005F94")]
			PlayIn,
			// Token: 0x04003189 RID: 12681
			[Token(Token = "0x4005F95")]
			Main,
			// Token: 0x0400318A RID: 12682
			[Token(Token = "0x4005F96")]
			UnloadChildSceneWait
		}

		// Token: 0x02000837 RID: 2103
		[Token(Token = "0x2000EDD")]
		private enum eEffectStep
		{
			// Token: 0x0400318C RID: 12684
			[Token(Token = "0x4005F98")]
			None,
			// Token: 0x0400318D RID: 12685
			[Token(Token = "0x4005F99")]
			StartCloseUI,
			// Token: 0x0400318E RID: 12686
			[Token(Token = "0x4005F9A")]
			WaitCloseUI,
			// Token: 0x0400318F RID: 12687
			[Token(Token = "0x4005F9B")]
			LoadWaitOpenChest,
			// Token: 0x04003190 RID: 12688
			[Token(Token = "0x4005F9C")]
			PlayOpenChest,
			// Token: 0x04003191 RID: 12689
			[Token(Token = "0x4005F9D")]
			WaitOpenChest,
			// Token: 0x04003192 RID: 12690
			[Token(Token = "0x4005F9E")]
			UnloadWaitOpenChest,
			// Token: 0x04003193 RID: 12691
			[Token(Token = "0x4005F9F")]
			StartResult,
			// Token: 0x04003194 RID: 12692
			[Token(Token = "0x4005FA0")]
			WaitResult,
			// Token: 0x04003195 RID: 12693
			[Token(Token = "0x4005FA1")]
			WaitWindow,
			// Token: 0x04003196 RID: 12694
			[Token(Token = "0x4005FA2")]
			StartReload,
			// Token: 0x04003197 RID: 12695
			[Token(Token = "0x4005FA3")]
			WaitReload,
			// Token: 0x04003198 RID: 12696
			[Token(Token = "0x4005FA4")]
			StartOpenUI,
			// Token: 0x04003199 RID: 12697
			[Token(Token = "0x4005FA5")]
			WaitOpenUI,
			// Token: 0x0400319A RID: 12698
			[Token(Token = "0x4005FA6")]
			Finish
		}

		// Token: 0x02000838 RID: 2104
		[Token(Token = "0x2000EDE")]
		private enum eChestResetStep
		{
			// Token: 0x0400319C RID: 12700
			[Token(Token = "0x4005FA8")]
			None,
			// Token: 0x0400319D RID: 12701
			[Token(Token = "0x4005FA9")]
			StartCloseUI,
			// Token: 0x0400319E RID: 12702
			[Token(Token = "0x4005FAA")]
			WaitCloseUI,
			// Token: 0x0400319F RID: 12703
			[Token(Token = "0x4005FAB")]
			ChestReset,
			// Token: 0x040031A0 RID: 12704
			[Token(Token = "0x4005FAC")]
			WaitReload,
			// Token: 0x040031A1 RID: 12705
			[Token(Token = "0x4005FAD")]
			StartOpenUI,
			// Token: 0x040031A2 RID: 12706
			[Token(Token = "0x4005FAE")]
			WaitOpenUI,
			// Token: 0x040031A3 RID: 12707
			[Token(Token = "0x4005FAF")]
			Finish
		}
	}
}
