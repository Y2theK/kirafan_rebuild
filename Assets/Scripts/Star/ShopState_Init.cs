﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000839 RID: 2105
	[Token(Token = "0x200062A")]
	[StructLayout(3)]
	public class ShopState_Init : ShopState
	{
		// Token: 0x0600217D RID: 8573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EE3")]
		[Address(RVA = "0x10130F598", Offset = "0x130F598", VA = "0x10130F598")]
		public ShopState_Init(ShopMain owner)
		{
		}

		// Token: 0x0600217E RID: 8574 RVA: 0x0000E9E8 File Offset: 0x0000CBE8
		[Token(Token = "0x6001EE4")]
		[Address(RVA = "0x1013199A0", Offset = "0x13199A0", VA = "0x1013199A0", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600217F RID: 8575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EE5")]
		[Address(RVA = "0x1013199A8", Offset = "0x13199A8", VA = "0x1013199A8", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002180 RID: 8576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EE6")]
		[Address(RVA = "0x1013199B0", Offset = "0x13199B0", VA = "0x1013199B0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002181 RID: 8577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EE7")]
		[Address(RVA = "0x1013199B4", Offset = "0x13199B4", VA = "0x1013199B4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002182 RID: 8578 RVA: 0x0000EA00 File Offset: 0x0000CC00
		[Token(Token = "0x6001EE8")]
		[Address(RVA = "0x1013199B8", Offset = "0x13199B8", VA = "0x1013199B8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002183 RID: 8579 RVA: 0x0000EA18 File Offset: 0x0000CC18
		[Token(Token = "0x6001EE9")]
		[Address(RVA = "0x10131A004", Offset = "0x131A004", VA = "0x10131A004")]
		private bool SetupUI()
		{
			return default(bool);
		}

		// Token: 0x06002184 RID: 8580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EEA")]
		[Address(RVA = "0x10131A144", Offset = "0x131A144", VA = "0x10131A144", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040031A4 RID: 12708
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002567")]
		private ShopState_Init.eStep m_Step;

		// Token: 0x040031A5 RID: 12709
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002568")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0200083A RID: 2106
		[Token(Token = "0x2000EDF")]
		private enum eStep
		{
			// Token: 0x040031A7 RID: 12711
			[Token(Token = "0x4005FB1")]
			None = -1,
			// Token: 0x040031A8 RID: 12712
			[Token(Token = "0x4005FB2")]
			First,
			// Token: 0x040031A9 RID: 12713
			[Token(Token = "0x4005FB3")]
			Load_Wait,
			// Token: 0x040031AA RID: 12714
			[Token(Token = "0x4005FB4")]
			Load_WaitBG,
			// Token: 0x040031AB RID: 12715
			[Token(Token = "0x4005FB5")]
			SetupUI,
			// Token: 0x040031AC RID: 12716
			[Token(Token = "0x4005FB6")]
			End
		}
	}
}
