﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x0200083B RID: 2107
	[Token(Token = "0x200062B")]
	[StructLayout(3)]
	public class ShopState_Market : ShopState
	{
		// Token: 0x06002185 RID: 8581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EEB")]
		[Address(RVA = "0x10130F85C", Offset = "0x130F85C", VA = "0x10130F85C")]
		public ShopState_Market(ShopMain owner)
		{
		}

		// Token: 0x06002186 RID: 8582 RVA: 0x0000EA30 File Offset: 0x0000CC30
		[Token(Token = "0x6001EEC")]
		[Address(RVA = "0x10131A148", Offset = "0x131A148", VA = "0x10131A148", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002187 RID: 8583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EED")]
		[Address(RVA = "0x10131A150", Offset = "0x131A150", VA = "0x10131A150", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002188 RID: 8584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EEE")]
		[Address(RVA = "0x10131A158", Offset = "0x131A158", VA = "0x10131A158", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002189 RID: 8585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EEF")]
		[Address(RVA = "0x10131A15C", Offset = "0x131A15C", VA = "0x10131A15C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600218A RID: 8586 RVA: 0x0000EA48 File Offset: 0x0000CC48
		[Token(Token = "0x6001EF0")]
		[Address(RVA = "0x10131A164", Offset = "0x131A164", VA = "0x10131A164", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600218B RID: 8587 RVA: 0x0000EA60 File Offset: 0x0000CC60
		[Token(Token = "0x6001EF1")]
		[Address(RVA = "0x10131A7DC", Offset = "0x131A7DC", VA = "0x10131A7DC")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600218C RID: 8588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF2")]
		[Address(RVA = "0x10131AB2C", Offset = "0x131AB2C", VA = "0x10131AB2C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600218D RID: 8589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF3")]
		[Address(RVA = "0x10131AC0C", Offset = "0x131AC0C", VA = "0x10131AC0C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0600218E RID: 8590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF4")]
		[Address(RVA = "0x10131ACEC", Offset = "0x131ACEC", VA = "0x10131ACEC")]
		public void Purchase(int id)
		{
		}

		// Token: 0x0600218F RID: 8591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF5")]
		[Address(RVA = "0x10131AE38", Offset = "0x131AE38", VA = "0x10131AE38")]
		public void AddShownList(List<StoreManager.Product> list)
		{
		}

		// Token: 0x06002190 RID: 8592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF6")]
		[Address(RVA = "0x10131AED8", Offset = "0x131AED8", VA = "0x10131AED8")]
		public void Exchange(long exchangeShopId, int buyAmount)
		{
		}

		// Token: 0x06002191 RID: 8593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF7")]
		[Address(RVA = "0x10131AFEC", Offset = "0x131AFEC", VA = "0x10131AFEC")]
		private void RefreshUI()
		{
		}

		// Token: 0x06002192 RID: 8594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF8")]
		[Address(RVA = "0x10131B03C", Offset = "0x131B03C", VA = "0x10131B03C")]
		private void OpenScrollList(string title, string message, string subMessage)
		{
		}

		// Token: 0x06002193 RID: 8595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EF9")]
		[Address(RVA = "0x10131B08C", Offset = "0x131B08C", VA = "0x10131B08C")]
		private void BeginReloadUI(int buttonIndex)
		{
		}

		// Token: 0x06002194 RID: 8596 RVA: 0x0000EA78 File Offset: 0x0000CC78
		[Token(Token = "0x6001EFA")]
		[Address(RVA = "0x10131AA20", Offset = "0x131AA20", VA = "0x10131AA20")]
		private bool ExecReloadUI()
		{
			return default(bool);
		}

		// Token: 0x06002195 RID: 8597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EFB")]
		[Address(RVA = "0x10131B14C", Offset = "0x131B14C", VA = "0x10131B14C")]
		private void OnCompleteInitProducts()
		{
		}

		// Token: 0x06002196 RID: 8598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EFC")]
		[Address(RVA = "0x10131B158", Offset = "0x131B158", VA = "0x10131B158")]
		private void OnCompleteSetShownFlow()
		{
		}

		// Token: 0x040031AD RID: 12717
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002569")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x040031AE RID: 12718
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400256A")]
		private ShopState_Market.eStep m_Step;

		// Token: 0x040031AF RID: 12719
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400256B")]
		private bool m_FinishProductSetShown;

		// Token: 0x040031B0 RID: 12720
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400256C")]
		private ShopState_Market.eReloadStep m_ReloadStep;

		// Token: 0x040031B1 RID: 12721
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400256D")]
		private bool m_IsReloadUI;

		// Token: 0x040031B2 RID: 12722
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400256E")]
		private ShopMarketUI m_UI;

		// Token: 0x040031B3 RID: 12723
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400256F")]
		private GemShopAPI_Purchase m_API_Purchase;

		// Token: 0x040031B4 RID: 12724
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002570")]
		private GemShopAPI_SetShown m_API_SetShown;

		// Token: 0x040031B5 RID: 12725
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002571")]
		private GemShopAPI_Exchange m_API_Exchange;

		// Token: 0x0200083C RID: 2108
		[Token(Token = "0x2000EE0")]
		private enum eStep
		{
			// Token: 0x040031B7 RID: 12727
			[Token(Token = "0x4005FB8")]
			None = -1,
			// Token: 0x040031B8 RID: 12728
			[Token(Token = "0x4005FB9")]
			First,
			// Token: 0x040031B9 RID: 12729
			[Token(Token = "0x4005FBA")]
			InitProducts,
			// Token: 0x040031BA RID: 12730
			[Token(Token = "0x4005FBB")]
			InitProducts_Wait,
			// Token: 0x040031BB RID: 12731
			[Token(Token = "0x4005FBC")]
			LoadStart,
			// Token: 0x040031BC RID: 12732
			[Token(Token = "0x4005FBD")]
			LoadWait,
			// Token: 0x040031BD RID: 12733
			[Token(Token = "0x4005FBE")]
			PlayIn,
			// Token: 0x040031BE RID: 12734
			[Token(Token = "0x4005FBF")]
			Main,
			// Token: 0x040031BF RID: 12735
			[Token(Token = "0x4005FC0")]
			UnloadResourceWait,
			// Token: 0x040031C0 RID: 12736
			[Token(Token = "0x4005FC1")]
			SetShownWait,
			// Token: 0x040031C1 RID: 12737
			[Token(Token = "0x4005FC2")]
			UnloadChildScene,
			// Token: 0x040031C2 RID: 12738
			[Token(Token = "0x4005FC3")]
			UnloadChildSceneWait
		}

		// Token: 0x0200083D RID: 2109
		[Token(Token = "0x2000EE1")]
		private enum eReloadStep
		{
			// Token: 0x040031C4 RID: 12740
			[Token(Token = "0x4005FC5")]
			None,
			// Token: 0x040031C5 RID: 12741
			[Token(Token = "0x4005FC6")]
			Close,
			// Token: 0x040031C6 RID: 12742
			[Token(Token = "0x4005FC7")]
			Refresh,
			// Token: 0x040031C7 RID: 12743
			[Token(Token = "0x4005FC8")]
			Open,
			// Token: 0x040031C8 RID: 12744
			[Token(Token = "0x4005FC9")]
			WaitOpen,
			// Token: 0x040031C9 RID: 12745
			[Token(Token = "0x4005FCA")]
			End
		}
	}
}
