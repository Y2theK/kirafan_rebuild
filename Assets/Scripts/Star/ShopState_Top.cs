﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x0200083E RID: 2110
	[Token(Token = "0x200062C")]
	[StructLayout(3)]
	public class ShopState_Top : ShopState
	{
		// Token: 0x06002197 RID: 8599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EFD")]
		[Address(RVA = "0x10130F5D8", Offset = "0x130F5D8", VA = "0x10130F5D8")]
		public ShopState_Top(ShopMain owner)
		{
		}

		// Token: 0x06002198 RID: 8600 RVA: 0x0000EA90 File Offset: 0x0000CC90
		[Token(Token = "0x6001EFE")]
		[Address(RVA = "0x10131B164", Offset = "0x131B164", VA = "0x10131B164", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002199 RID: 8601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001EFF")]
		[Address(RVA = "0x10131B16C", Offset = "0x131B16C", VA = "0x10131B16C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600219A RID: 8602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F00")]
		[Address(RVA = "0x10131B174", Offset = "0x131B174", VA = "0x10131B174", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600219B RID: 8603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F01")]
		[Address(RVA = "0x10131B178", Offset = "0x131B178", VA = "0x10131B178", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600219C RID: 8604 RVA: 0x0000EAA8 File Offset: 0x0000CCA8
		[Token(Token = "0x6001F02")]
		[Address(RVA = "0x10131B180", Offset = "0x131B180", VA = "0x10131B180", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600219D RID: 8605 RVA: 0x0000EAC0 File Offset: 0x0000CCC0
		[Token(Token = "0x6001F03")]
		[Address(RVA = "0x10131B548", Offset = "0x131B548", VA = "0x10131B548")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600219E RID: 8606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F04")]
		[Address(RVA = "0x10131B7B0", Offset = "0x131B7B0", VA = "0x10131B7B0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600219F RID: 8607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F05")]
		[Address(RVA = "0x10131B850", Offset = "0x131B850", VA = "0x10131B850")]
		private void OnClickButton()
		{
		}

		// Token: 0x060021A0 RID: 8608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F06")]
		[Address(RVA = "0x10131B7B4", Offset = "0x131B7B4", VA = "0x10131B7B4")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x060021A1 RID: 8609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F07")]
		[Address(RVA = "0x10131B9A4", Offset = "0x131B9A4", VA = "0x10131B9A4")]
		private void CompleteSetAgeCallback()
		{
		}

		// Token: 0x060021A2 RID: 8610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F08")]
		[Address(RVA = "0x10131B9A8", Offset = "0x131B9A8", VA = "0x10131B9A8")]
		private void OnClickCloseButtonCallback()
		{
		}

		// Token: 0x040031CA RID: 12746
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002572")]
		private ShopState_Top.eStep m_Step;

		// Token: 0x040031CB RID: 12747
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002573")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x040031CC RID: 12748
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002574")]
		private ShopTopUI m_UI;

		// Token: 0x0200083F RID: 2111
		[Token(Token = "0x2000EE2")]
		private enum eStep
		{
			// Token: 0x040031CE RID: 12750
			[Token(Token = "0x4005FCC")]
			None = -1,
			// Token: 0x040031CF RID: 12751
			[Token(Token = "0x4005FCD")]
			First,
			// Token: 0x040031D0 RID: 12752
			[Token(Token = "0x4005FCE")]
			LoadWait,
			// Token: 0x040031D1 RID: 12753
			[Token(Token = "0x4005FCF")]
			PlayIn,
			// Token: 0x040031D2 RID: 12754
			[Token(Token = "0x4005FD0")]
			Main,
			// Token: 0x040031D3 RID: 12755
			[Token(Token = "0x4005FD1")]
			UnloadChildSceneWait
		}
	}
}
