﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using ItemTradeResponseTypes;
using Meige;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000840 RID: 2112
	[Token(Token = "0x200062D")]
	[StructLayout(3)]
	public class ShopState_TradeList : ShopState
	{
		// Token: 0x060021A3 RID: 8611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F09")]
		[Address(RVA = "0x10130F66C", Offset = "0x130F66C", VA = "0x10130F66C")]
		public ShopState_TradeList(ShopMain owner)
		{
		}

		// Token: 0x060021A4 RID: 8612 RVA: 0x0000EAD8 File Offset: 0x0000CCD8
		[Token(Token = "0x6001F0A")]
		[Address(RVA = "0x10131B9D8", Offset = "0x131B9D8", VA = "0x10131B9D8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021A5 RID: 8613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F0B")]
		[Address(RVA = "0x10131B9E0", Offset = "0x131B9E0", VA = "0x10131B9E0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021A6 RID: 8614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F0C")]
		[Address(RVA = "0x10131B9E8", Offset = "0x131B9E8", VA = "0x10131B9E8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021A7 RID: 8615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F0D")]
		[Address(RVA = "0x10131B9EC", Offset = "0x131B9EC", VA = "0x10131B9EC", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021A8 RID: 8616 RVA: 0x0000EAF0 File Offset: 0x0000CCF0
		[Token(Token = "0x6001F0E")]
		[Address(RVA = "0x10131B9F4", Offset = "0x131B9F4", VA = "0x10131B9F4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021A9 RID: 8617 RVA: 0x0000EB08 File Offset: 0x0000CD08
		[Token(Token = "0x6001F0F")]
		[Address(RVA = "0x10131BEC0", Offset = "0x131BEC0", VA = "0x10131BEC0")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021AA RID: 8618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F10")]
		[Address(RVA = "0x10131C5DC", Offset = "0x131C5DC", VA = "0x10131C5DC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021AB RID: 8619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F11")]
		[Address(RVA = "0x10131C67C", Offset = "0x131C67C", VA = "0x10131C67C")]
		private void OnClickButton()
		{
		}

		// Token: 0x060021AC RID: 8620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F12")]
		[Address(RVA = "0x10131C5E0", Offset = "0x131C5E0", VA = "0x10131C5E0")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x060021AD RID: 8621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F13")]
		[Address(RVA = "0x10131C680", Offset = "0x131C680", VA = "0x10131C680")]
		private void OnClickExecuteTradeButtonCallBack(int recipeID, int idx)
		{
		}

		// Token: 0x060021AE RID: 8622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F14")]
		[Address(RVA = "0x10131C758", Offset = "0x131C758", VA = "0x10131C758")]
		private void OnClickExecuteTradeButtonCallBack(int recipeID, int idx, int amount)
		{
		}

		// Token: 0x060021AF RID: 8623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F15")]
		[Address(RVA = "0x10131C834", Offset = "0x131C834", VA = "0x10131C834")]
		private void OnResponse_Trade(MeigewwwParam wwwParam, Trade param)
		{
		}

		// Token: 0x060021B0 RID: 8624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F16")]
		[Address(RVA = "0x10131CC38", Offset = "0x131CC38", VA = "0x10131CC38")]
		private void CloseConfirm(int index)
		{
		}

		// Token: 0x060021B1 RID: 8625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F17")]
		[Address(RVA = "0x10131CDB8", Offset = "0x131CDB8", VA = "0x10131CDB8")]
		private void OnClickChestButtonCallBack()
		{
		}

		// Token: 0x060021B2 RID: 8626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F18")]
		[Address(RVA = "0x10131CE2C", Offset = "0x131CE2C", VA = "0x10131CE2C")]
		private void OnClickQuestButtonCallBack()
		{
		}

		// Token: 0x040031D4 RID: 12756
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002575")]
		private ShopState_TradeList.eStep m_Step;

		// Token: 0x040031D5 RID: 12757
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002576")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x040031D6 RID: 12758
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002577")]
		private ShopTradeListUI m_UI;

		// Token: 0x040031D7 RID: 12759
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002578")]
		private ShopTradeTopUI.ShopTradeTopUISelectData m_SceneData;

		// Token: 0x040031D8 RID: 12760
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002579")]
		private int m_TradeGroupId;

		// Token: 0x040031D9 RID: 12761
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400257A")]
		private List<ShopManager.TradeData> m_TradeDataList;

		// Token: 0x040031DA RID: 12762
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400257B")]
		private int m_EventType;

		// Token: 0x040031DB RID: 12763
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400257C")]
		private int m_ChestID;

		// Token: 0x040031DC RID: 12764
		[Token(Token = "0x400257D")]
		private const float NPC_OFFSETX = -140f;

		// Token: 0x02000841 RID: 2113
		[Token(Token = "0x2000EE3")]
		private enum eStep
		{
			// Token: 0x040031DE RID: 12766
			[Token(Token = "0x4005FD3")]
			None = -1,
			// Token: 0x040031DF RID: 12767
			[Token(Token = "0x4005FD4")]
			First,
			// Token: 0x040031E0 RID: 12768
			[Token(Token = "0x4005FD5")]
			Request_Wait,
			// Token: 0x040031E1 RID: 12769
			[Token(Token = "0x4005FD6")]
			CheckExistRecipe,
			// Token: 0x040031E2 RID: 12770
			[Token(Token = "0x4005FD7")]
			LoadStart,
			// Token: 0x040031E3 RID: 12771
			[Token(Token = "0x4005FD8")]
			LoadWait,
			// Token: 0x040031E4 RID: 12772
			[Token(Token = "0x4005FD9")]
			PlayIn,
			// Token: 0x040031E5 RID: 12773
			[Token(Token = "0x4005FDA")]
			Main,
			// Token: 0x040031E6 RID: 12774
			[Token(Token = "0x4005FDB")]
			UnloadChildSceneWait
		}
	}
}
