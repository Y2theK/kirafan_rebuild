﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000842 RID: 2114
	[Token(Token = "0x200062E")]
	[StructLayout(3)]
	public class ShopState_TradeSale : ShopState
	{
		// Token: 0x060021B3 RID: 8627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F19")]
		[Address(RVA = "0x10131CEE8", Offset = "0x131CEE8", VA = "0x10131CEE8")]
		public ShopState_TradeSale(ShopMain owner)
		{
		}

		// Token: 0x060021B4 RID: 8628 RVA: 0x0000EB20 File Offset: 0x0000CD20
		[Token(Token = "0x6001F1A")]
		[Address(RVA = "0x10131CEFC", Offset = "0x131CEFC", VA = "0x10131CEFC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021B5 RID: 8629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F1B")]
		[Address(RVA = "0x10131CF04", Offset = "0x131CF04", VA = "0x10131CF04", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021B6 RID: 8630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F1C")]
		[Address(RVA = "0x10131CF0C", Offset = "0x131CF0C", VA = "0x10131CF0C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021B7 RID: 8631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F1D")]
		[Address(RVA = "0x10131CF10", Offset = "0x131CF10", VA = "0x10131CF10", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021B8 RID: 8632 RVA: 0x0000EB38 File Offset: 0x0000CD38
		[Token(Token = "0x6001F1E")]
		[Address(RVA = "0x10131CF18", Offset = "0x131CF18", VA = "0x10131CF18", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021B9 RID: 8633 RVA: 0x0000EB50 File Offset: 0x0000CD50
		[Token(Token = "0x6001F1F")]
		[Address(RVA = "0x10131D138", Offset = "0x131D138", VA = "0x10131D138")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021BA RID: 8634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F20")]
		[Address(RVA = "0x10131D378", Offset = "0x131D378", VA = "0x10131D378", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021BB RID: 8635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F21")]
		[Address(RVA = "0x10131D3A0", Offset = "0x131D3A0", VA = "0x10131D3A0")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x060021BC RID: 8636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F22")]
		[Address(RVA = "0x10131D43C", Offset = "0x131D43C", VA = "0x10131D43C")]
		private void OnClickSaleButtonCallBack(int itemID, int amount)
		{
		}

		// Token: 0x060021BD RID: 8637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F23")]
		[Address(RVA = "0x10131D514", Offset = "0x131D514", VA = "0x10131D514")]
		private void OnResponse_ItemSale(MeigewwwParam wwwParam, Itemsale param)
		{
		}

		// Token: 0x040031E7 RID: 12775
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400257E")]
		private ShopState_TradeSale.eStep m_Step;

		// Token: 0x040031E8 RID: 12776
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400257F")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x040031E9 RID: 12777
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002580")]
		private ShopTradeSaleListUI m_UI;

		// Token: 0x02000843 RID: 2115
		[Token(Token = "0x2000EE4")]
		private enum eStep
		{
			// Token: 0x040031EB RID: 12779
			[Token(Token = "0x4005FDD")]
			None = -1,
			// Token: 0x040031EC RID: 12780
			[Token(Token = "0x4005FDE")]
			First,
			// Token: 0x040031ED RID: 12781
			[Token(Token = "0x4005FDF")]
			LoadStart,
			// Token: 0x040031EE RID: 12782
			[Token(Token = "0x4005FE0")]
			LoadWait,
			// Token: 0x040031EF RID: 12783
			[Token(Token = "0x4005FE1")]
			PlayIn,
			// Token: 0x040031F0 RID: 12784
			[Token(Token = "0x4005FE2")]
			Main,
			// Token: 0x040031F1 RID: 12785
			[Token(Token = "0x4005FE3")]
			UnloadChildSceneWait
		}
	}
}
