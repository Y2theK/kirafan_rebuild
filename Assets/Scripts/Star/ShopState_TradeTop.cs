﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000844 RID: 2116
	[Token(Token = "0x200062F")]
	[StructLayout(3)]
	public class ShopState_TradeTop : ShopState
	{
		// Token: 0x060021BE RID: 8638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F24")]
		[Address(RVA = "0x10131D62C", Offset = "0x131D62C", VA = "0x10131D62C")]
		public ShopState_TradeTop(ShopMain owner)
		{
		}

		// Token: 0x060021BF RID: 8639 RVA: 0x0000EB68 File Offset: 0x0000CD68
		[Token(Token = "0x6001F25")]
		[Address(RVA = "0x10131D640", Offset = "0x131D640", VA = "0x10131D640", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021C0 RID: 8640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F26")]
		[Address(RVA = "0x10131D648", Offset = "0x131D648", VA = "0x10131D648", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021C1 RID: 8641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F27")]
		[Address(RVA = "0x10131D650", Offset = "0x131D650", VA = "0x10131D650", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021C2 RID: 8642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F28")]
		[Address(RVA = "0x10131D654", Offset = "0x131D654", VA = "0x10131D654", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021C3 RID: 8643 RVA: 0x0000EB80 File Offset: 0x0000CD80
		[Token(Token = "0x6001F29")]
		[Address(RVA = "0x10131D65C", Offset = "0x131D65C", VA = "0x10131D65C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021C4 RID: 8644 RVA: 0x0000EB98 File Offset: 0x0000CD98
		[Token(Token = "0x6001F2A")]
		[Address(RVA = "0x10131D934", Offset = "0x131D934", VA = "0x10131D934")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021C5 RID: 8645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F2B")]
		[Address(RVA = "0x10131DE44", Offset = "0x131DE44", VA = "0x10131DE44", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021C6 RID: 8646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F2C")]
		[Address(RVA = "0x10131E0A4", Offset = "0x131E0A4", VA = "0x10131E0A4")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x060021C7 RID: 8647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F2D")]
		[Address(RVA = "0x10131E008", Offset = "0x131E008", VA = "0x10131E008")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x040031F2 RID: 12786
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002581")]
		private ShopState_TradeTop.eStep m_Step;

		// Token: 0x040031F3 RID: 12787
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002582")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x040031F4 RID: 12788
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002583")]
		private ShopTradeTopUI m_UI;

		// Token: 0x040031F5 RID: 12789
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002584")]
		private ShopTradeTopUI.ShopTradeTopUISelectData m_SceneData;

		// Token: 0x02000845 RID: 2117
		[Token(Token = "0x2000EE5")]
		private enum eStep
		{
			// Token: 0x040031F7 RID: 12791
			[Token(Token = "0x4005FE5")]
			None = -1,
			// Token: 0x040031F8 RID: 12792
			[Token(Token = "0x4005FE6")]
			First,
			// Token: 0x040031F9 RID: 12793
			[Token(Token = "0x4005FE7")]
			Request_Wait,
			// Token: 0x040031FA RID: 12794
			[Token(Token = "0x4005FE8")]
			LoadStart,
			// Token: 0x040031FB RID: 12795
			[Token(Token = "0x4005FE9")]
			LoadWait,
			// Token: 0x040031FC RID: 12796
			[Token(Token = "0x4005FEA")]
			PlayIn,
			// Token: 0x040031FD RID: 12797
			[Token(Token = "0x4005FEB")]
			Main,
			// Token: 0x040031FE RID: 12798
			[Token(Token = "0x4005FEC")]
			UnloadChildSceneWait
		}
	}
}
