﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000846 RID: 2118
	[Token(Token = "0x2000630")]
	[StructLayout(3)]
	public class ShopState_WeaponCreate : ShopState
	{
		// Token: 0x060021C8 RID: 8648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F2E")]
		[Address(RVA = "0x10131E0A8", Offset = "0x131E0A8", VA = "0x10131E0A8")]
		public ShopState_WeaponCreate(ShopMain owner)
		{
		}

		// Token: 0x060021C9 RID: 8649 RVA: 0x0000EBB0 File Offset: 0x0000CDB0
		[Token(Token = "0x6001F2F")]
		[Address(RVA = "0x10131E0BC", Offset = "0x131E0BC", VA = "0x10131E0BC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021CA RID: 8650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F30")]
		[Address(RVA = "0x10131E0C4", Offset = "0x131E0C4", VA = "0x10131E0C4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021CB RID: 8651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F31")]
		[Address(RVA = "0x10131E0CC", Offset = "0x131E0CC", VA = "0x10131E0CC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021CC RID: 8652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F32")]
		[Address(RVA = "0x10131E0D0", Offset = "0x131E0D0", VA = "0x10131E0D0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021CD RID: 8653 RVA: 0x0000EBC8 File Offset: 0x0000CDC8
		[Token(Token = "0x6001F33")]
		[Address(RVA = "0x10131E0D8", Offset = "0x131E0D8", VA = "0x10131E0D8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021CE RID: 8654 RVA: 0x0000EBE0 File Offset: 0x0000CDE0
		[Token(Token = "0x6001F34")]
		[Address(RVA = "0x10131E364", Offset = "0x131E364", VA = "0x10131E364")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021CF RID: 8655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F35")]
		[Address(RVA = "0x10131E5F0", Offset = "0x131E5F0", VA = "0x10131E5F0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021D0 RID: 8656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F36")]
		[Address(RVA = "0x10131E618", Offset = "0x131E618", VA = "0x10131E618")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x060021D1 RID: 8657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F37")]
		[Address(RVA = "0x10131E6B4", Offset = "0x131E6B4", VA = "0x10131E6B4")]
		private void OnClickExecuteButton(int recipeID)
		{
		}

		// Token: 0x060021D2 RID: 8658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F38")]
		[Address(RVA = "0x10131E77C", Offset = "0x131E77C", VA = "0x10131E77C")]
		private void OnResponse_WeaponMake(MeigewwwParam wwwParam, Weaponmake param)
		{
		}

		// Token: 0x040031FF RID: 12799
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002585")]
		private ShopState_WeaponCreate.eStep m_Step;

		// Token: 0x04003200 RID: 12800
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002586")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003201 RID: 12801
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002587")]
		private ShopWeaponCreateUI m_UI;

		// Token: 0x02000847 RID: 2119
		[Token(Token = "0x2000EE6")]
		private enum eStep
		{
			// Token: 0x04003203 RID: 12803
			[Token(Token = "0x4005FEE")]
			None = -1,
			// Token: 0x04003204 RID: 12804
			[Token(Token = "0x4005FEF")]
			First,
			// Token: 0x04003205 RID: 12805
			[Token(Token = "0x4005FF0")]
			LoadStart,
			// Token: 0x04003206 RID: 12806
			[Token(Token = "0x4005FF1")]
			LoadWait,
			// Token: 0x04003207 RID: 12807
			[Token(Token = "0x4005FF2")]
			PlayIn,
			// Token: 0x04003208 RID: 12808
			[Token(Token = "0x4005FF3")]
			Main,
			// Token: 0x04003209 RID: 12809
			[Token(Token = "0x4005FF4")]
			UnloadChildSceneWait
		}
	}
}
