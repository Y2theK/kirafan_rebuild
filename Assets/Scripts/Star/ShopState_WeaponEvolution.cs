﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000848 RID: 2120
	[Token(Token = "0x2000631")]
	[StructLayout(3)]
	public class ShopState_WeaponEvolution : ShopState
	{
		// Token: 0x060021D3 RID: 8659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F39")]
		[Address(RVA = "0x10131E7F0", Offset = "0x131E7F0", VA = "0x10131E7F0")]
		public ShopState_WeaponEvolution(ShopMain owner)
		{
		}

		// Token: 0x060021D4 RID: 8660 RVA: 0x0000EBF8 File Offset: 0x0000CDF8
		[Token(Token = "0x6001F3A")]
		[Address(RVA = "0x10131E808", Offset = "0x131E808", VA = "0x10131E808", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021D5 RID: 8661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F3B")]
		[Address(RVA = "0x10131E810", Offset = "0x131E810", VA = "0x10131E810", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021D6 RID: 8662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F3C")]
		[Address(RVA = "0x10131E818", Offset = "0x131E818", VA = "0x10131E818", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021D7 RID: 8663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F3D")]
		[Address(RVA = "0x10131E81C", Offset = "0x131E81C", VA = "0x10131E81C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021D8 RID: 8664 RVA: 0x0000EC10 File Offset: 0x0000CE10
		[Token(Token = "0x6001F3E")]
		[Address(RVA = "0x10131E824", Offset = "0x131E824", VA = "0x10131E824", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021D9 RID: 8665 RVA: 0x0000EC28 File Offset: 0x0000CE28
		[Token(Token = "0x6001F3F")]
		[Address(RVA = "0x10131EA28", Offset = "0x131EA28", VA = "0x10131EA28")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021DA RID: 8666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F40")]
		[Address(RVA = "0x10131F0C4", Offset = "0x131F0C4", VA = "0x10131F0C4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021DB RID: 8667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F41")]
		[Address(RVA = "0x10131F1A0", Offset = "0x131F1A0", VA = "0x10131F1A0")]
		private void PlayEvolutionEffect()
		{
		}

		// Token: 0x060021DC RID: 8668 RVA: 0x0000EC40 File Offset: 0x0000CE40
		[Token(Token = "0x6001F42")]
		[Address(RVA = "0x10131EC8C", Offset = "0x131EC8C", VA = "0x10131EC8C")]
		private bool ExecEvolutionEffect()
		{
			return default(bool);
		}

		// Token: 0x060021DD RID: 8669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F43")]
		[Address(RVA = "0x10131F1B0", Offset = "0x131F1B0", VA = "0x10131F1B0")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x060021DE RID: 8670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F44")]
		[Address(RVA = "0x10131F224", Offset = "0x131F224", VA = "0x10131F224")]
		private void OnClickExecuteButton(long weaponMngID, int recipeID)
		{
		}

		// Token: 0x060021DF RID: 8671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F45")]
		[Address(RVA = "0x10131F2FC", Offset = "0x131F2FC", VA = "0x10131F2FC")]
		private void OnResponse_WeaponEvolution(ShopDefine.eWeaponEvolutionResult result, string message)
		{
		}

		// Token: 0x060021E0 RID: 8672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F46")]
		[Address(RVA = "0x10131F104", Offset = "0x131F104", VA = "0x10131F104")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x0400320A RID: 12810
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002588")]
		private ShopState_WeaponEvolution.eStep m_Step;

		// Token: 0x0400320B RID: 12811
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002589")]
		private ShopState_WeaponEvolution.eEvolutionStep m_EvolutionStep;

		// Token: 0x0400320C RID: 12812
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400258A")]
		private bool m_PlayingWeaponEvolution;

		// Token: 0x0400320D RID: 12813
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400258B")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400320E RID: 12814
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400258C")]
		private ShopWeaponEvolutionUI m_UI;

		// Token: 0x02000849 RID: 2121
		[Token(Token = "0x2000EE7")]
		private enum eStep
		{
			// Token: 0x04003210 RID: 12816
			[Token(Token = "0x4005FF6")]
			None = -1,
			// Token: 0x04003211 RID: 12817
			[Token(Token = "0x4005FF7")]
			First,
			// Token: 0x04003212 RID: 12818
			[Token(Token = "0x4005FF8")]
			LoadStart,
			// Token: 0x04003213 RID: 12819
			[Token(Token = "0x4005FF9")]
			LoadWait,
			// Token: 0x04003214 RID: 12820
			[Token(Token = "0x4005FFA")]
			PlayIn,
			// Token: 0x04003215 RID: 12821
			[Token(Token = "0x4005FFB")]
			Main,
			// Token: 0x04003216 RID: 12822
			[Token(Token = "0x4005FFC")]
			UnloadChildSceneWait
		}

		// Token: 0x0200084A RID: 2122
		[Token(Token = "0x2000EE8")]
		private enum eEvolutionStep
		{
			// Token: 0x04003218 RID: 12824
			[Token(Token = "0x4005FFE")]
			None,
			// Token: 0x04003219 RID: 12825
			[Token(Token = "0x4005FFF")]
			PlayEvolutionEffect,
			// Token: 0x0400321A RID: 12826
			[Token(Token = "0x4006000")]
			WaitEvolutionEffect,
			// Token: 0x0400321B RID: 12827
			[Token(Token = "0x4006001")]
			OpenResultWindow,
			// Token: 0x0400321C RID: 12828
			[Token(Token = "0x4006002")]
			WaitResultWindow,
			// Token: 0x0400321D RID: 12829
			[Token(Token = "0x4006003")]
			CloseResultWindow,
			// Token: 0x0400321E RID: 12830
			[Token(Token = "0x4006004")]
			Unlock,
			// Token: 0x0400321F RID: 12831
			[Token(Token = "0x4006005")]
			Finish
		}
	}
}
