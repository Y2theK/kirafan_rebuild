﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x0200084B RID: 2123
	[Token(Token = "0x2000632")]
	[StructLayout(3)]
	public class ShopState_WeaponEvolutionList : ShopState
	{
		// Token: 0x060021E2 RID: 8674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F48")]
		[Address(RVA = "0x10131F36C", Offset = "0x131F36C", VA = "0x10131F36C")]
		public ShopState_WeaponEvolutionList(ShopMain owner)
		{
		}

		// Token: 0x060021E3 RID: 8675 RVA: 0x0000EC58 File Offset: 0x0000CE58
		[Token(Token = "0x6001F49")]
		[Address(RVA = "0x10131F380", Offset = "0x131F380", VA = "0x10131F380", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021E4 RID: 8676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F4A")]
		[Address(RVA = "0x10131F388", Offset = "0x131F388", VA = "0x10131F388", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021E5 RID: 8677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F4B")]
		[Address(RVA = "0x10131F390", Offset = "0x131F390", VA = "0x10131F390", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021E6 RID: 8678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F4C")]
		[Address(RVA = "0x10131F394", Offset = "0x131F394", VA = "0x10131F394", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021E7 RID: 8679 RVA: 0x0000EC70 File Offset: 0x0000CE70
		[Token(Token = "0x6001F4D")]
		[Address(RVA = "0x10131F39C", Offset = "0x131F39C", VA = "0x10131F39C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021E8 RID: 8680 RVA: 0x0000EC88 File Offset: 0x0000CE88
		[Token(Token = "0x6001F4E")]
		[Address(RVA = "0x10131F670", Offset = "0x131F670", VA = "0x10131F670")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021E9 RID: 8681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F4F")]
		[Address(RVA = "0x10131F8A8", Offset = "0x131F8A8", VA = "0x10131F8A8")]
		protected void OnClickButtonCallBack()
		{
		}

		// Token: 0x060021EA RID: 8682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F50")]
		[Address(RVA = "0x10131F948", Offset = "0x131F948", VA = "0x10131F948", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021EB RID: 8683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F51")]
		[Address(RVA = "0x10131F8AC", Offset = "0x131F8AC", VA = "0x10131F8AC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003220 RID: 12832
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400258D")]
		private ShopState_WeaponEvolutionList.eStep m_Step;

		// Token: 0x04003221 RID: 12833
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400258E")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003222 RID: 12834
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400258F")]
		private WeaponListUI m_UI;

		// Token: 0x0200084C RID: 2124
		[Token(Token = "0x2000EE9")]
		private enum eStep
		{
			// Token: 0x04003224 RID: 12836
			[Token(Token = "0x4006007")]
			None = -1,
			// Token: 0x04003225 RID: 12837
			[Token(Token = "0x4006008")]
			First,
			// Token: 0x04003226 RID: 12838
			[Token(Token = "0x4006009")]
			LoadWait,
			// Token: 0x04003227 RID: 12839
			[Token(Token = "0x400600A")]
			PlayIn,
			// Token: 0x04003228 RID: 12840
			[Token(Token = "0x400600B")]
			Main,
			// Token: 0x04003229 RID: 12841
			[Token(Token = "0x400600C")]
			UnloadChildSceneWait
		}
	}
}
