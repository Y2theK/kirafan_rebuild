﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x0200084D RID: 2125
	[Token(Token = "0x2000633")]
	[StructLayout(3)]
	public class ShopState_WeaponSale : ShopState
	{
		// Token: 0x060021EC RID: 8684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F52")]
		[Address(RVA = "0x10131F970", Offset = "0x131F970", VA = "0x10131F970")]
		public ShopState_WeaponSale(ShopMain owner)
		{
		}

		// Token: 0x060021ED RID: 8685 RVA: 0x0000ECA0 File Offset: 0x0000CEA0
		[Token(Token = "0x6001F53")]
		[Address(RVA = "0x10131F98C", Offset = "0x131F98C", VA = "0x10131F98C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021EE RID: 8686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F54")]
		[Address(RVA = "0x10131F994", Offset = "0x131F994", VA = "0x10131F994", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021EF RID: 8687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F55")]
		[Address(RVA = "0x10131F99C", Offset = "0x131F99C", VA = "0x10131F99C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021F0 RID: 8688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F56")]
		[Address(RVA = "0x10131F9A0", Offset = "0x131F9A0", VA = "0x10131F9A0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021F1 RID: 8689 RVA: 0x0000ECB8 File Offset: 0x0000CEB8
		[Token(Token = "0x6001F57")]
		[Address(RVA = "0x10131F9A8", Offset = "0x131F9A8", VA = "0x10131F9A8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021F2 RID: 8690 RVA: 0x0000ECD0 File Offset: 0x0000CED0
		[Token(Token = "0x6001F58")]
		[Address(RVA = "0x10131FCC0", Offset = "0x131FCC0", VA = "0x10131FCC0")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021F3 RID: 8691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F59")]
		[Address(RVA = "0x10131FFF4", Offset = "0x131FFF4", VA = "0x10131FFF4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x060021F4 RID: 8692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F5A")]
		[Address(RVA = "0x1013200D8", Offset = "0x13200D8", VA = "0x1013200D8")]
		protected void OnClickButtonCallBack()
		{
		}

		// Token: 0x060021F5 RID: 8693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F5B")]
		[Address(RVA = "0x10132001C", Offset = "0x132001C", VA = "0x10132001C")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x060021F6 RID: 8694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F5C")]
		[Address(RVA = "0x10132018C", Offset = "0x132018C", VA = "0x10132018C")]
		private void OnClickExecuteButton(List<long> weaponMngIDs)
		{
		}

		// Token: 0x060021F7 RID: 8695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F5D")]
		[Address(RVA = "0x101320254", Offset = "0x1320254", VA = "0x101320254")]
		private void OnResponse_WeaponSale(ShopDefine.eWeaponSaleResult result, string errMsg)
		{
		}

		// Token: 0x0400322A RID: 12842
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002590")]
		private ShopState_WeaponSale.eStep m_Step;

		// Token: 0x0400322B RID: 12843
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002591")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400322C RID: 12844
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002592")]
		private SceneDefine.eChildSceneID m_ListSceneID;

		// Token: 0x0400322D RID: 12845
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002593")]
		private WeaponListUI m_ListUI;

		// Token: 0x0400322E RID: 12846
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002594")]
		private ShopWeaponSaleUI m_UI;

		// Token: 0x0200084E RID: 2126
		[Token(Token = "0x2000EEA")]
		private enum eStep
		{
			// Token: 0x04003230 RID: 12848
			[Token(Token = "0x400600E")]
			None = -1,
			// Token: 0x04003231 RID: 12849
			[Token(Token = "0x400600F")]
			First,
			// Token: 0x04003232 RID: 12850
			[Token(Token = "0x4006010")]
			LoadWait,
			// Token: 0x04003233 RID: 12851
			[Token(Token = "0x4006011")]
			LoadStartList,
			// Token: 0x04003234 RID: 12852
			[Token(Token = "0x4006012")]
			LoadWaitList,
			// Token: 0x04003235 RID: 12853
			[Token(Token = "0x4006013")]
			PlayIn,
			// Token: 0x04003236 RID: 12854
			[Token(Token = "0x4006014")]
			List,
			// Token: 0x04003237 RID: 12855
			[Token(Token = "0x4006015")]
			Main,
			// Token: 0x04003238 RID: 12856
			[Token(Token = "0x4006016")]
			UnloadChildSceneWait
		}
	}
}
