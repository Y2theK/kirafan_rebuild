﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x0200084F RID: 2127
	[Token(Token = "0x2000634")]
	[StructLayout(3)]
	public class ShopState_WeaponSaleList : ShopState
	{
		// Token: 0x060021F8 RID: 8696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F5E")]
		[Address(RVA = "0x101320350", Offset = "0x1320350", VA = "0x101320350")]
		public ShopState_WeaponSaleList(ShopMain owner)
		{
		}

		// Token: 0x060021F9 RID: 8697 RVA: 0x0000ECE8 File Offset: 0x0000CEE8
		[Token(Token = "0x6001F5F")]
		[Address(RVA = "0x101320364", Offset = "0x1320364", VA = "0x101320364", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060021FA RID: 8698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F60")]
		[Address(RVA = "0x10132036C", Offset = "0x132036C", VA = "0x10132036C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x060021FB RID: 8699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F61")]
		[Address(RVA = "0x101320374", Offset = "0x1320374", VA = "0x101320374", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x060021FC RID: 8700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F62")]
		[Address(RVA = "0x101320378", Offset = "0x1320378", VA = "0x101320378", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x060021FD RID: 8701 RVA: 0x0000ED00 File Offset: 0x0000CF00
		[Token(Token = "0x6001F63")]
		[Address(RVA = "0x101320380", Offset = "0x1320380", VA = "0x101320380", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x060021FE RID: 8702 RVA: 0x0000ED18 File Offset: 0x0000CF18
		[Token(Token = "0x6001F64")]
		[Address(RVA = "0x10132060C", Offset = "0x132060C", VA = "0x10132060C")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x060021FF RID: 8703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F65")]
		[Address(RVA = "0x101320844", Offset = "0x1320844", VA = "0x101320844")]
		protected void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002200 RID: 8704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F66")]
		[Address(RVA = "0x101320930", Offset = "0x1320930", VA = "0x101320930", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002201 RID: 8705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F67")]
		[Address(RVA = "0x101320894", Offset = "0x1320894", VA = "0x101320894")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003239 RID: 12857
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002595")]
		private ShopState_WeaponSaleList.eStep m_Step;

		// Token: 0x0400323A RID: 12858
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002596")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400323B RID: 12859
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002597")]
		private WeaponListUI m_UI;

		// Token: 0x02000850 RID: 2128
		[Token(Token = "0x2000EEB")]
		private enum eStep
		{
			// Token: 0x0400323D RID: 12861
			[Token(Token = "0x4006018")]
			None = -1,
			// Token: 0x0400323E RID: 12862
			[Token(Token = "0x4006019")]
			First,
			// Token: 0x0400323F RID: 12863
			[Token(Token = "0x400601A")]
			LoadStart,
			// Token: 0x04003240 RID: 12864
			[Token(Token = "0x400601B")]
			LoadWait,
			// Token: 0x04003241 RID: 12865
			[Token(Token = "0x400601C")]
			PlayIn,
			// Token: 0x04003242 RID: 12866
			[Token(Token = "0x400601D")]
			Main,
			// Token: 0x04003243 RID: 12867
			[Token(Token = "0x400601E")]
			UnloadChildSceneWait
		}
	}
}
