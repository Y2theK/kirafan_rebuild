﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000851 RID: 2129
	[Token(Token = "0x2000635")]
	[StructLayout(3)]
	public class ShopState_WeaponSkillLvUP : ShopState
	{
		// Token: 0x06002202 RID: 8706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F68")]
		[Address(RVA = "0x101320958", Offset = "0x1320958", VA = "0x101320958")]
		public ShopState_WeaponSkillLvUP(ShopMain owner)
		{
		}

		// Token: 0x06002203 RID: 8707 RVA: 0x0000ED30 File Offset: 0x0000CF30
		[Token(Token = "0x6001F69")]
		[Address(RVA = "0x101320970", Offset = "0x1320970", VA = "0x101320970", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002204 RID: 8708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F6A")]
		[Address(RVA = "0x101320978", Offset = "0x1320978", VA = "0x101320978", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002205 RID: 8709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F6B")]
		[Address(RVA = "0x101320980", Offset = "0x1320980", VA = "0x101320980", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002206 RID: 8710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F6C")]
		[Address(RVA = "0x101320984", Offset = "0x1320984", VA = "0x101320984", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002207 RID: 8711 RVA: 0x0000ED48 File Offset: 0x0000CF48
		[Token(Token = "0x6001F6D")]
		[Address(RVA = "0x10132098C", Offset = "0x132098C", VA = "0x10132098C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002208 RID: 8712 RVA: 0x0000ED60 File Offset: 0x0000CF60
		[Token(Token = "0x6001F6E")]
		[Address(RVA = "0x101320B78", Offset = "0x1320B78", VA = "0x101320B78")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002209 RID: 8713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F6F")]
		[Address(RVA = "0x101320D88", Offset = "0x1320D88", VA = "0x101320D88", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F70")]
		[Address(RVA = "0x101320DC8", Offset = "0x1320DC8", VA = "0x101320DC8")]
		private void _GotoMenuEnd()
		{
		}

		// Token: 0x04003244 RID: 12868
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002598")]
		private ShopState_WeaponSkillLvUP.eStep m_Step;

		// Token: 0x04003245 RID: 12869
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002599")]
		private ShopWeaponSkillLvUPUI m_UI;

		// Token: 0x04003246 RID: 12870
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400259A")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x02000852 RID: 2130
		[Token(Token = "0x2000EEC")]
		private enum eStep
		{
			// Token: 0x04003248 RID: 12872
			[Token(Token = "0x4006020")]
			None = -1,
			// Token: 0x04003249 RID: 12873
			[Token(Token = "0x4006021")]
			First,
			// Token: 0x0400324A RID: 12874
			[Token(Token = "0x4006022")]
			LoadStart,
			// Token: 0x0400324B RID: 12875
			[Token(Token = "0x4006023")]
			LoadWait,
			// Token: 0x0400324C RID: 12876
			[Token(Token = "0x4006024")]
			PlayIn,
			// Token: 0x0400324D RID: 12877
			[Token(Token = "0x4006025")]
			Main,
			// Token: 0x0400324E RID: 12878
			[Token(Token = "0x4006026")]
			UnloadChildSceneWait
		}
	}
}
