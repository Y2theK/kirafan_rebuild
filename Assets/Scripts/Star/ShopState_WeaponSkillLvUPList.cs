﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x02000853 RID: 2131
	[Token(Token = "0x2000636")]
	[StructLayout(3)]
	public class ShopState_WeaponSkillLvUPList : ShopState
	{
		// Token: 0x0600220B RID: 8715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F71")]
		[Address(RVA = "0x101320E64", Offset = "0x1320E64", VA = "0x101320E64")]
		public ShopState_WeaponSkillLvUPList(ShopMain owner)
		{
		}

		// Token: 0x0600220C RID: 8716 RVA: 0x0000ED78 File Offset: 0x0000CF78
		[Token(Token = "0x6001F72")]
		[Address(RVA = "0x101320E7C", Offset = "0x1320E7C", VA = "0x101320E7C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600220D RID: 8717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F73")]
		[Address(RVA = "0x101320E84", Offset = "0x1320E84", VA = "0x101320E84", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600220E RID: 8718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F74")]
		[Address(RVA = "0x101320E8C", Offset = "0x1320E8C", VA = "0x101320E8C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600220F RID: 8719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F75")]
		[Address(RVA = "0x101320E90", Offset = "0x1320E90", VA = "0x101320E90", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002210 RID: 8720 RVA: 0x0000ED90 File Offset: 0x0000CF90
		[Token(Token = "0x6001F76")]
		[Address(RVA = "0x101320E98", Offset = "0x1320E98", VA = "0x101320E98", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002211 RID: 8721 RVA: 0x0000EDA8 File Offset: 0x0000CFA8
		[Token(Token = "0x6001F77")]
		[Address(RVA = "0x10132116C", Offset = "0x132116C", VA = "0x10132116C")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002212 RID: 8722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F78")]
		[Address(RVA = "0x1013213A4", Offset = "0x13213A4", VA = "0x1013213A4")]
		protected void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002213 RID: 8723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F79")]
		[Address(RVA = "0x101321444", Offset = "0x1321444", VA = "0x101321444", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002214 RID: 8724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F7A")]
		[Address(RVA = "0x1013213A8", Offset = "0x13213A8", VA = "0x1013213A8")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0400324F RID: 12879
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400259B")]
		private ShopState_WeaponSkillLvUPList.eStep m_Step;

		// Token: 0x04003250 RID: 12880
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400259C")]
		private WeaponListUI m_UI;

		// Token: 0x04003251 RID: 12881
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400259D")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x02000854 RID: 2132
		[Token(Token = "0x2000EED")]
		private enum eStep
		{
			// Token: 0x04003253 RID: 12883
			[Token(Token = "0x4006028")]
			None = -1,
			// Token: 0x04003254 RID: 12884
			[Token(Token = "0x4006029")]
			First,
			// Token: 0x04003255 RID: 12885
			[Token(Token = "0x400602A")]
			LoadWait,
			// Token: 0x04003256 RID: 12886
			[Token(Token = "0x400602B")]
			PlayIn,
			// Token: 0x04003257 RID: 12887
			[Token(Token = "0x400602C")]
			Main,
			// Token: 0x04003258 RID: 12888
			[Token(Token = "0x400602D")]
			UnloadChildSceneWait
		}
	}
}
