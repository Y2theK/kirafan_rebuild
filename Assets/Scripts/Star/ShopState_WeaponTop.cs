﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000855 RID: 2133
	[Token(Token = "0x2000637")]
	[StructLayout(3)]
	public class ShopState_WeaponTop : ShopState
	{
		// Token: 0x06002215 RID: 8725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F7B")]
		[Address(RVA = "0x10132146C", Offset = "0x132146C", VA = "0x10132146C")]
		public ShopState_WeaponTop(ShopMain owner)
		{
		}

		// Token: 0x06002216 RID: 8726 RVA: 0x0000EDC0 File Offset: 0x0000CFC0
		[Token(Token = "0x6001F7C")]
		[Address(RVA = "0x101321480", Offset = "0x1321480", VA = "0x101321480", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002217 RID: 8727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F7D")]
		[Address(RVA = "0x101321488", Offset = "0x1321488", VA = "0x101321488", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002218 RID: 8728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F7E")]
		[Address(RVA = "0x101321490", Offset = "0x1321490", VA = "0x101321490", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002219 RID: 8729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F7F")]
		[Address(RVA = "0x101321494", Offset = "0x1321494", VA = "0x101321494", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600221A RID: 8730 RVA: 0x0000EDD8 File Offset: 0x0000CFD8
		[Token(Token = "0x6001F80")]
		[Address(RVA = "0x10132149C", Offset = "0x132149C", VA = "0x10132149C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600221B RID: 8731 RVA: 0x0000EDF0 File Offset: 0x0000CFF0
		[Token(Token = "0x6001F81")]
		[Address(RVA = "0x1013216F0", Offset = "0x13216F0", VA = "0x1013216F0")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x0600221C RID: 8732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F82")]
		[Address(RVA = "0x101321A3C", Offset = "0x1321A3C", VA = "0x101321A3C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600221D RID: 8733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F83")]
		[Address(RVA = "0x101321C44", Offset = "0x1321C44", VA = "0x101321C44")]
		private void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600221E RID: 8734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F84")]
		[Address(RVA = "0x101321C98", Offset = "0x1321C98", VA = "0x101321C98")]
		private void LimitExtend()
		{
		}

		// Token: 0x0600221F RID: 8735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F85")]
		[Address(RVA = "0x101322720", Offset = "0x1322720", VA = "0x101322720")]
		private void OnConfirmLimitAdd(int btn)
		{
		}

		// Token: 0x06002220 RID: 8736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F86")]
		[Address(RVA = "0x1013227F8", Offset = "0x13227F8", VA = "0x1013227F8")]
		private void OnResponseLimitAdd(MeigewwwParam wwwParam, Weaponlimitadd param)
		{
		}

		// Token: 0x06002221 RID: 8737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F87")]
		[Address(RVA = "0x101321BA8", Offset = "0x1321BA8", VA = "0x101321BA8")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x04003259 RID: 12889
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400259E")]
		private ShopState_WeaponTop.eStep m_Step;

		// Token: 0x0400325A RID: 12890
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400259F")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x0400325B RID: 12891
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40025A0")]
		private ShopWeaponTopUI m_UI;

		// Token: 0x02000856 RID: 2134
		[Token(Token = "0x2000EEE")]
		private enum eStep
		{
			// Token: 0x0400325D RID: 12893
			[Token(Token = "0x400602F")]
			None = -1,
			// Token: 0x0400325E RID: 12894
			[Token(Token = "0x4006030")]
			First,
			// Token: 0x0400325F RID: 12895
			[Token(Token = "0x4006031")]
			LoadStart,
			// Token: 0x04003260 RID: 12896
			[Token(Token = "0x4006032")]
			LoadWait,
			// Token: 0x04003261 RID: 12897
			[Token(Token = "0x4006033")]
			PlayIn,
			// Token: 0x04003262 RID: 12898
			[Token(Token = "0x4006034")]
			Main,
			// Token: 0x04003263 RID: 12899
			[Token(Token = "0x4006035")]
			UnloadChildSceneWait
		}
	}
}
