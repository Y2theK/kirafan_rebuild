﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000857 RID: 2135
	[Token(Token = "0x2000638")]
	[StructLayout(3)]
	public class ShopState_WeaponUpgrade : ShopState
	{
		// Token: 0x06002222 RID: 8738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F88")]
		[Address(RVA = "0x10132283C", Offset = "0x132283C", VA = "0x10132283C")]
		public ShopState_WeaponUpgrade(ShopMain owner)
		{
		}

		// Token: 0x06002223 RID: 8739 RVA: 0x0000EE08 File Offset: 0x0000D008
		[Token(Token = "0x6001F89")]
		[Address(RVA = "0x101322850", Offset = "0x1322850", VA = "0x101322850", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002224 RID: 8740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F8A")]
		[Address(RVA = "0x101322858", Offset = "0x1322858", VA = "0x101322858", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002225 RID: 8741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F8B")]
		[Address(RVA = "0x101322860", Offset = "0x1322860", VA = "0x101322860", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002226 RID: 8742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F8C")]
		[Address(RVA = "0x101322864", Offset = "0x1322864", VA = "0x101322864", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002227 RID: 8743 RVA: 0x0000EE20 File Offset: 0x0000D020
		[Token(Token = "0x6001F8D")]
		[Address(RVA = "0x10132286C", Offset = "0x132286C", VA = "0x10132286C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002228 RID: 8744 RVA: 0x0000EE38 File Offset: 0x0000D038
		[Token(Token = "0x6001F8E")]
		[Address(RVA = "0x101322AF8", Offset = "0x1322AF8", VA = "0x101322AF8")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002229 RID: 8745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F8F")]
		[Address(RVA = "0x101322D5C", Offset = "0x1322D5C", VA = "0x101322D5C", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600222A RID: 8746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F90")]
		[Address(RVA = "0x101322D84", Offset = "0x1322D84", VA = "0x101322D84")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0600222B RID: 8747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F91")]
		[Address(RVA = "0x101322E20", Offset = "0x1322E20", VA = "0x101322E20")]
		private void OnClickExecuteButton(long weaponMngID, List<KeyValuePair<int, int>> list)
		{
		}

		// Token: 0x0600222C RID: 8748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F92")]
		[Address(RVA = "0x101322EF8", Offset = "0x1322EF8", VA = "0x101322EF8")]
		private void OnResponse_WeaponUpgrade(ShopDefine.eWeaponUpgradeResult result, string errMsg)
		{
		}

		// Token: 0x04003264 RID: 12900
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025A1")]
		private ShopState_WeaponUpgrade.eStep m_Step;

		// Token: 0x04003265 RID: 12901
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40025A2")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003266 RID: 12902
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40025A3")]
		private ShopWeaponUpgradeUI m_UI;

		// Token: 0x02000858 RID: 2136
		[Token(Token = "0x2000EEF")]
		private enum eStep
		{
			// Token: 0x04003268 RID: 12904
			[Token(Token = "0x4006037")]
			None = -1,
			// Token: 0x04003269 RID: 12905
			[Token(Token = "0x4006038")]
			First,
			// Token: 0x0400326A RID: 12906
			[Token(Token = "0x4006039")]
			LoadStart,
			// Token: 0x0400326B RID: 12907
			[Token(Token = "0x400603A")]
			LoadWait,
			// Token: 0x0400326C RID: 12908
			[Token(Token = "0x400603B")]
			PlayIn,
			// Token: 0x0400326D RID: 12909
			[Token(Token = "0x400603C")]
			Main,
			// Token: 0x0400326E RID: 12910
			[Token(Token = "0x400603D")]
			UnloadChildSceneWait
		}
	}
}
