﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x02000859 RID: 2137
	[Token(Token = "0x2000639")]
	[StructLayout(3)]
	public class ShopState_WeaponUpgradeList : ShopState
	{
		// Token: 0x0600222D RID: 8749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F93")]
		[Address(RVA = "0x101322FC8", Offset = "0x1322FC8", VA = "0x101322FC8")]
		public ShopState_WeaponUpgradeList(ShopMain owner)
		{
		}

		// Token: 0x0600222E RID: 8750 RVA: 0x0000EE50 File Offset: 0x0000D050
		[Token(Token = "0x6001F94")]
		[Address(RVA = "0x101322FDC", Offset = "0x1322FDC", VA = "0x101322FDC", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600222F RID: 8751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F95")]
		[Address(RVA = "0x101322FE4", Offset = "0x1322FE4", VA = "0x101322FE4", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002230 RID: 8752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F96")]
		[Address(RVA = "0x101322FEC", Offset = "0x1322FEC", VA = "0x101322FEC", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002231 RID: 8753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F97")]
		[Address(RVA = "0x101322FF0", Offset = "0x1322FF0", VA = "0x101322FF0", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002232 RID: 8754 RVA: 0x0000EE68 File Offset: 0x0000D068
		[Token(Token = "0x6001F98")]
		[Address(RVA = "0x101322FF8", Offset = "0x1322FF8", VA = "0x101322FF8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002233 RID: 8755 RVA: 0x0000EE80 File Offset: 0x0000D080
		[Token(Token = "0x6001F99")]
		[Address(RVA = "0x101323280", Offset = "0x1323280", VA = "0x101323280")]
		private bool SetupAndPlayUI()
		{
			return default(bool);
		}

		// Token: 0x06002234 RID: 8756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F9A")]
		[Address(RVA = "0x101323504", Offset = "0x1323504", VA = "0x101323504")]
		protected void OnClickButtonCallBack()
		{
		}

		// Token: 0x06002235 RID: 8757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F9B")]
		[Address(RVA = "0x1013235A4", Offset = "0x13235A4", VA = "0x1013235A4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002236 RID: 8758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F9C")]
		[Address(RVA = "0x101323508", Offset = "0x1323508", VA = "0x101323508")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0400326F RID: 12911
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025A4")]
		private ShopState_WeaponUpgradeList.eStep m_Step;

		// Token: 0x04003270 RID: 12912
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40025A5")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003271 RID: 12913
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40025A6")]
		private WeaponListUI m_UI;

		// Token: 0x0200085A RID: 2138
		[Token(Token = "0x2000EF0")]
		private enum eStep
		{
			// Token: 0x04003273 RID: 12915
			[Token(Token = "0x400603F")]
			None = -1,
			// Token: 0x04003274 RID: 12916
			[Token(Token = "0x4006040")]
			First,
			// Token: 0x04003275 RID: 12917
			[Token(Token = "0x4006041")]
			LoadStart,
			// Token: 0x04003276 RID: 12918
			[Token(Token = "0x4006042")]
			LoadWait,
			// Token: 0x04003277 RID: 12919
			[Token(Token = "0x4006043")]
			PlayIn,
			// Token: 0x04003278 RID: 12920
			[Token(Token = "0x4006044")]
			Main,
			// Token: 0x04003279 RID: 12921
			[Token(Token = "0x4006045")]
			UnloadChildSceneWait
		}
	}
}
