﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000A99 RID: 2713
	[Token(Token = "0x2000782")]
	[StructLayout(3)]
	public static class ShopUtility
	{
		// Token: 0x06002E7E RID: 11902 RVA: 0x00013CC8 File Offset: 0x00011EC8
		[Token(Token = "0x6002A8B")]
		[Address(RVA = "0x1013235CC", Offset = "0x13235CC", VA = "0x1013235CC")]
		public static bool IsTransitWeaponEvolutionAdv()
		{
			return default(bool);
		}

		// Token: 0x06002E7F RID: 11903 RVA: 0x00013CE0 File Offset: 0x00011EE0
		[Token(Token = "0x6002A8C")]
		[Address(RVA = "0x1013236B0", Offset = "0x13236B0", VA = "0x1013236B0")]
		public static int GetSkillLvUPItemID(long weaponMngID)
		{
			return 0;
		}

		// Token: 0x06002E80 RID: 11904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A8D")]
		[Address(RVA = "0x101323898", Offset = "0x1323898", VA = "0x101323898")]
		public static void PackageToVariableIconParam(ref PackageItemListDB_Param packageParam, out ePresentType presentType, out int presentID)
		{
		}
	}
}
