﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Signup;
using UnityEngine;

namespace Star
{
	// Token: 0x0200085B RID: 2139
	[Token(Token = "0x200063A")]
	[StructLayout(3)]
	public class SignupMain : GameStateMain
	{
		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06002237 RID: 8759 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700023B")]
		public SignupUI SignupUI
		{
			[Token(Token = "0x6001F9D")]
			[Address(RVA = "0x101323984", Offset = "0x1323984", VA = "0x101323984")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002238 RID: 8760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F9E")]
		[Address(RVA = "0x10132398C", Offset = "0x132398C", VA = "0x10132398C")]
		private void Start()
		{
		}

		// Token: 0x06002239 RID: 8761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001F9F")]
		[Address(RVA = "0x1013239D0", Offset = "0x13239D0", VA = "0x1013239D0")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600223A RID: 8762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA0")]
		[Address(RVA = "0x1013239D8", Offset = "0x13239D8", VA = "0x1013239D8", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x0600223B RID: 8763 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001FA1")]
		[Address(RVA = "0x1013239E0", Offset = "0x13239E0", VA = "0x1013239E0", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x0600223C RID: 8764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA2")]
		[Address(RVA = "0x101323A90", Offset = "0x1323A90", VA = "0x101323A90")]
		public SignupMain()
		{
		}

		// Token: 0x0400327A RID: 12922
		[Token(Token = "0x40025A7")]
		public const int STATE_MAIN = 0;

		// Token: 0x0400327B RID: 12923
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40025A8")]
		[SerializeField]
		private SignupUI m_UI;
	}
}
