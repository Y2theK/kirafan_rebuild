﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200085C RID: 2140
	[Token(Token = "0x200063B")]
	[StructLayout(3)]
	public class SignupState : GameStateBase
	{
		// Token: 0x0600223D RID: 8765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA3")]
		[Address(RVA = "0x101323A98", Offset = "0x1323A98", VA = "0x101323A98")]
		public SignupState(SignupMain owner)
		{
		}

		// Token: 0x0600223E RID: 8766 RVA: 0x0000EE98 File Offset: 0x0000D098
		[Token(Token = "0x6001FA4")]
		[Address(RVA = "0x101323AC4", Offset = "0x1323AC4", VA = "0x101323AC4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600223F RID: 8767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA5")]
		[Address(RVA = "0x101323ACC", Offset = "0x1323ACC", VA = "0x101323ACC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002240 RID: 8768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA6")]
		[Address(RVA = "0x101323AD0", Offset = "0x1323AD0", VA = "0x101323AD0", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002241 RID: 8769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA7")]
		[Address(RVA = "0x101323AD4", Offset = "0x1323AD4", VA = "0x101323AD4", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002242 RID: 8770 RVA: 0x0000EEB0 File Offset: 0x0000D0B0
		[Token(Token = "0x6001FA8")]
		[Address(RVA = "0x101323AD8", Offset = "0x1323AD8", VA = "0x101323AD8", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002243 RID: 8771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FA9")]
		[Address(RVA = "0x101323AE0", Offset = "0x1323AE0", VA = "0x101323AE0", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0400327C RID: 12924
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025A9")]
		protected SignupMain m_Owner;
	}
}
