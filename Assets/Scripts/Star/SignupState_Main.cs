﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using PlayerResponseTypes;

namespace Star
{
	// Token: 0x0200085D RID: 2141
	[Token(Token = "0x200063C")]
	[StructLayout(3)]
	public class SignupState_Main : SignupState
	{
		// Token: 0x06002244 RID: 8772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FAA")]
		[Address(RVA = "0x101323A5C", Offset = "0x1323A5C", VA = "0x101323A5C")]
		public SignupState_Main(SignupMain owner)
		{
		}

		// Token: 0x06002245 RID: 8773 RVA: 0x0000EEC8 File Offset: 0x0000D0C8
		[Token(Token = "0x6001FAB")]
		[Address(RVA = "0x101323AE4", Offset = "0x1323AE4", VA = "0x101323AE4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002246 RID: 8774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FAC")]
		[Address(RVA = "0x101323AEC", Offset = "0x1323AEC", VA = "0x101323AEC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002247 RID: 8775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FAD")]
		[Address(RVA = "0x101323AF4", Offset = "0x1323AF4", VA = "0x101323AF4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002248 RID: 8776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FAE")]
		[Address(RVA = "0x101323AF8", Offset = "0x1323AF8", VA = "0x101323AF8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002249 RID: 8777 RVA: 0x0000EEE0 File Offset: 0x0000D0E0
		[Token(Token = "0x6001FAF")]
		[Address(RVA = "0x101323AFC", Offset = "0x1323AFC", VA = "0x101323AFC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600224A RID: 8778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB0")]
		[Address(RVA = "0x101324098", Offset = "0x1324098", VA = "0x101324098")]
		private void OnClickDecide()
		{
		}

		// Token: 0x0600224B RID: 8779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB1")]
		[Address(RVA = "0x101324198", Offset = "0x1324198", VA = "0x101324198")]
		private void OnResponse_Signup(MeigewwwParam wwwParam, Signup param)
		{
		}

		// Token: 0x0600224C RID: 8780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB2")]
		[Address(RVA = "0x101324390", Offset = "0x1324390", VA = "0x101324390")]
		private void OnResponse_Login(MeigewwwParam wwwParam, Login param)
		{
		}

		// Token: 0x0600224D RID: 8781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB3")]
		[Address(RVA = "0x101324468", Offset = "0x1324468", VA = "0x101324468")]
		private void OnConfirmInvalidAuthVoided()
		{
		}

		// Token: 0x0600224E RID: 8782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB4")]
		[Address(RVA = "0x101324564", Offset = "0x1324564", VA = "0x101324564")]
		private void OnResponse_GetAll(MeigewwwParam wwwParam, Getall param)
		{
		}

		// Token: 0x0600224F RID: 8783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB5")]
		[Address(RVA = "0x1013245F8", Offset = "0x13245F8", VA = "0x1013245F8")]
		private void OnResponse_QuestGetAll()
		{
		}

		// Token: 0x06002250 RID: 8784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FB6")]
		[Address(RVA = "0x1013246B4", Offset = "0x13246B4", VA = "0x1013246B4")]
		private void OnResponse_QuestChapterGetAll()
		{
		}

		// Token: 0x0400327D RID: 12925
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025AA")]
		private SignupState_Main.eStep m_Step;

		// Token: 0x0200085E RID: 2142
		[Token(Token = "0x2000EF1")]
		private enum eStep
		{
			// Token: 0x0400327F RID: 12927
			[Token(Token = "0x4006047")]
			None = -1,
			// Token: 0x04003280 RID: 12928
			[Token(Token = "0x4006048")]
			First,
			// Token: 0x04003281 RID: 12929
			[Token(Token = "0x4006049")]
			WaitLoadBG,
			// Token: 0x04003282 RID: 12930
			[Token(Token = "0x400604A")]
			FadeIn_Wait,
			// Token: 0x04003283 RID: 12931
			[Token(Token = "0x400604B")]
			Main,
			// Token: 0x04003284 RID: 12932
			[Token(Token = "0x400604C")]
			QuestGetAll,
			// Token: 0x04003285 RID: 12933
			[Token(Token = "0x400604D")]
			QuestGetAll_Wait,
			// Token: 0x04003286 RID: 12934
			[Token(Token = "0x400604E")]
			Mission,
			// Token: 0x04003287 RID: 12935
			[Token(Token = "0x400604F")]
			Mission_Wait,
			// Token: 0x04003288 RID: 12936
			[Token(Token = "0x4006050")]
			TransitNextScene,
			// Token: 0x04003289 RID: 12937
			[Token(Token = "0x4006051")]
			TransitNextScene_Wait
		}
	}
}
