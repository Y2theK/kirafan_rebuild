﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020008C4 RID: 2244
	[Token(Token = "0x200066D")]
	[StructLayout(3)]
	public class SimpleTimer
	{
		// Token: 0x17000289 RID: 649
		// (get) Token: 0x060024FA RID: 9466 RVA: 0x0000FE10 File Offset: 0x0000E010
		[Token(Token = "0x17000262")]
		public float Sec
		{
			[Token(Token = "0x600221E")]
			[Address(RVA = "0x1013246C0", Offset = "0x13246C0", VA = "0x1013246C0")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x060024FB RID: 9467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600221F")]
		[Address(RVA = "0x1013246C8", Offset = "0x13246C8", VA = "0x1013246C8")]
		public SimpleTimer(float sec)
		{
		}

		// Token: 0x060024FC RID: 9468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002220")]
		[Address(RVA = "0x10132470C", Offset = "0x132470C", VA = "0x10132470C")]
		public void SetTimeAndPlay(float sec)
		{
		}

		// Token: 0x060024FD RID: 9469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002221")]
		[Address(RVA = "0x101324704", Offset = "0x1324704", VA = "0x101324704")]
		public void SetTime(float sec)
		{
		}

		// Token: 0x060024FE RID: 9470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002222")]
		[Address(RVA = "0x101324718", Offset = "0x1324718", VA = "0x101324718")]
		public void Start()
		{
		}

		// Token: 0x060024FF RID: 9471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002223")]
		[Address(RVA = "0x101324720", Offset = "0x1324720", VA = "0x101324720")]
		public void Stop()
		{
		}

		// Token: 0x06002500 RID: 9472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002224")]
		[Address(RVA = "0x10132472C", Offset = "0x132472C", VA = "0x10132472C")]
		public void Update(float deltaTime)
		{
		}

		// Token: 0x040034DE RID: 13534
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40026AB")]
		private float m_Time;

		// Token: 0x040034DF RID: 13535
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40026AC")]
		private bool m_IsStop;
	}
}
