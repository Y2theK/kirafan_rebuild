﻿using UnityEngine;

namespace Star {
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {

        [SerializeField] private bool m_IsDontDestroyOnLoad;

        private static T instance;

        public static T Inst {
            get {
                if (instance == null) {
                    instance = FindObjectOfType<T>();
                    if (instance != null) { }
                }
                return instance;
            }
        }

        protected virtual void Awake() {
            CheckInstance();
        }

        protected bool CheckInstance() {
            if (this == Inst) {
                if (m_IsDontDestroyOnLoad) {
                    DontDestroyOnLoad(gameObject);
                }
                return true;
            }
            Destroy(gameObject);
            Destroy(this);
            return false;
        }

        protected virtual void OnDestroy() {
            if (instance == this) {
                instance = null;
            }
        }
    }
}
