﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005A2 RID: 1442
	[Token(Token = "0x2000495")]
	[StructLayout(3)]
	public class SkillActionConvertsDB : ScriptableObject
	{
		// Token: 0x060015F9 RID: 5625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014A9")]
		[Address(RVA = "0x101324754", Offset = "0x1324754", VA = "0x101324754")]
		public SkillActionConvertsDB()
		{
		}

		// Token: 0x04001AD9 RID: 6873
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001443")]
		public SkillActionConvertsDB_Param[] m_Params;
	}
}
