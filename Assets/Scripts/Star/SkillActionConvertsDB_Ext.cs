﻿namespace Star {
    public static class SkillActionConvertsDB_Ext {
        public static string Convert(this SkillActionConvertsDB self, string srcSkillActionID, int dstConvertValue) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (!string.IsNullOrEmpty(self.m_Params[i].m_SrcSkillActionID) && self.m_Params[i].m_SrcSkillActionID == srcSkillActionID) {
                    return string.Format(self.m_Params[i].m_DstSkillActionID, dstConvertValue);
                }
            }
            return srcSkillActionID;
        }
    }
}
