﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005A1 RID: 1441
	[Token(Token = "0x2000494")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SkillActionConvertsDB_Param
	{
		// Token: 0x04001AD7 RID: 6871
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001441")]
		public string m_SrcSkillActionID;

		// Token: 0x04001AD8 RID: 6872
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001442")]
		public string m_DstSkillActionID;
	}
}
