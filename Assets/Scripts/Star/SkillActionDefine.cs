﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200043C RID: 1084
	[Token(Token = "0x200035F")]
	[StructLayout(3)]
	public static class SkillActionDefine
	{
		// Token: 0x04001324 RID: 4900
		[Token(Token = "0x4000DED")]
		public const int SAG_MOTIONID_CHARA_SKILL_RANGE_MIN = 10;

		// Token: 0x04001325 RID: 4901
		[Token(Token = "0x4000DEE")]
		public const int SAG_MOTIONID_CHARA_SKILL_RANGE_MAX = 19;

		// Token: 0x04001326 RID: 4902
		[Token(Token = "0x4000DEF")]
		public const string EFFECT_ID_FORMAT_ATTACK = "ef_btl_{0}_attack_{1}_{2:D2}";

		// Token: 0x04001327 RID: 4903
		[Token(Token = "0x4000DF0")]
		public const string EFFECT_ID_FORMAT_SKILL = "ef_btl_{0}_skill_{1:D2}_{2}_{3:D2}_{4:D2}";

		// Token: 0x04001328 RID: 4904
		[Token(Token = "0x4000DF1")]
		public const string EFFECT_ID_FORMAT_DAMAGE_SINGLE = "ef_btl_dmg_single_{0:D2}";

		// Token: 0x04001329 RID: 4905
		[Token(Token = "0x4000DF2")]
		public const string EFFECT_ID_FORMAT_DAMAGE_ALL = "ef_btl_dmg_all_{0}_{1:D2}";

		// Token: 0x0400132A RID: 4906
		[Token(Token = "0x4000DF3")]
		public const string EFFECT_ID_FORMAT_DAMAGE_EN = "ef_btl_dmg_enemy_attack_{0}_{1:D2}";

		// Token: 0x0400132B RID: 4907
		[Token(Token = "0x4000DF4")]
		public const string EFFECT_KEY_DMG_EN = "_dmg_enemy_";

		// Token: 0x0400132C RID: 4908
		[Token(Token = "0x4000DF5")]
		public const string EFFECT_KEY_DMG = "_dmg_";

		// Token: 0x0400132D RID: 4909
		[Token(Token = "0x4000DF6")]
		public const string EFFECT_KEY_CHARASKILL = "ef_Chara_Battle_";

		// Token: 0x0400132E RID: 4910
		[Token(Token = "0x4000DF7")]
		public const string EFFECT_ID_FORMAT_CHARASKILL = "ef_Chara_Battle_{0}_{1:D2}";
	}
}
