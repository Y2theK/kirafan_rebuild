﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Battle;
using UnityEngine;

namespace Star
{
	// Token: 0x0200043D RID: 1085
	[Token(Token = "0x2000360")]
	[StructLayout(3)]
	public class SkillActionEffectSet
	{
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x0600103D RID: 4157 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000F5")]
		public CharacterHandler TargetCharaHndl
		{
			[Token(Token = "0x6000F0C")]
			[Address(RVA = "0x1013248E4", Offset = "0x13248E4", VA = "0x1013248E4")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F0D")]
		[Address(RVA = "0x1013248EC", Offset = "0x13248EC", VA = "0x1013248EC")]
		public SkillActionEffectSet(CharacterHandler targetCharaHndl)
		{
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F0E")]
		[Address(RVA = "0x1013258A8", Offset = "0x13258A8", VA = "0x1013258A8")]
		public void Destroy()
		{
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F0F")]
		[Address(RVA = "0x101325A04", Offset = "0x1325A04", VA = "0x101325A04")]
		public void Update()
		{
		}

		// Token: 0x06001041 RID: 4161 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F10")]
		[Address(RVA = "0x101325B1C", Offset = "0x1325B1C", VA = "0x101325B1C")]
		private SkillActionEffectSet SetupTo_UpDownEffect(bool isMinus, ePopUpStateIconType popUpIcon, int grade = 0)
		{
			return null;
		}

		// Token: 0x06001042 RID: 4162 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F11")]
		[Address(RVA = "0x1013263F8", Offset = "0x13263F8", VA = "0x1013263F8")]
		private SkillActionEffectSet SetupTo_UpDownEffectCombi(bool isMinus, eSkillContentAndEffectCombiDB efCombi, ePopUpStateIconType popUpIcon, float opt_value)
		{
			return null;
		}

		// Token: 0x06001043 RID: 4163 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F12")]
		[Address(RVA = "0x1013265B8", Offset = "0x13265B8", VA = "0x1013265B8")]
		public SkillActionEffectSet SetupTo_Recover(int opt_power)
		{
			return null;
		}

		// Token: 0x06001044 RID: 4164 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F13")]
		[Address(RVA = "0x101326810", Offset = "0x1326810", VA = "0x101326810")]
		public SkillActionEffectSet SetupTo_DrainRecover()
		{
			return null;
		}

		// Token: 0x06001045 RID: 4165 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F14")]
		[Address(RVA = "0x101326848", Offset = "0x1326848", VA = "0x101326848")]
		public SkillActionEffectSet SetupTo_StatusChange_Up(float[] opt_values)
		{
			return null;
		}

		// Token: 0x06001046 RID: 4166 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F15")]
		[Address(RVA = "0x101326DB4", Offset = "0x1326DB4", VA = "0x101326DB4")]
		public SkillActionEffectSet SetupTo_StatusChange_Down(float[] opt_values)
		{
			return null;
		}

		// Token: 0x06001047 RID: 4167 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F16")]
		[Address(RVA = "0x101327320", Offset = "0x1327320", VA = "0x101327320")]
		public SkillActionEffectSet SetupTo_StatusChangeReset(bool isBoth, bool isBuff)
		{
			return null;
		}

		// Token: 0x06001048 RID: 4168 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F17")]
		[Address(RVA = "0x101327588", Offset = "0x1327588", VA = "0x101327588")]
		public SkillActionEffectSet SetupTo_StatusChangeDisable(bool isBuff)
		{
			return null;
		}

		// Token: 0x06001049 RID: 4169 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F18")]
		[Address(RVA = "0x1013277DC", Offset = "0x13277DC", VA = "0x1013277DC")]
		public SkillActionEffectSet SetupTo_Abnormal(eStateAbnormal opt_stateAbnormal)
		{
			return null;
		}

		// Token: 0x0600104A RID: 4170 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F19")]
		[Address(RVA = "0x101327AF4", Offset = "0x1327AF4", VA = "0x101327AF4")]
		public SkillActionEffectSet SetupTo_AbnormalMiss()
		{
			return null;
		}

		// Token: 0x0600104B RID: 4171 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1A")]
		[Address(RVA = "0x101327BC0", Offset = "0x1327BC0", VA = "0x101327BC0")]
		public SkillActionEffectSet SetupTo_AbnormalRecover()
		{
			return null;
		}

		// Token: 0x0600104C RID: 4172 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1B")]
		[Address(RVA = "0x101327D40", Offset = "0x1327D40", VA = "0x101327D40")]
		public SkillActionEffectSet SetupTo_AbnormalAdditionalProbability(bool opt_isUp)
		{
			return null;
		}

		// Token: 0x0600104D RID: 4173 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1C")]
		[Address(RVA = "0x101327E64", Offset = "0x1327E64", VA = "0x101327E64")]
		public SkillActionEffectSet SetupTo_ElementResist_Up(eElementType opt_elementType, float opt_value)
		{
			return null;
		}

		// Token: 0x0600104E RID: 4174 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1D")]
		[Address(RVA = "0x1013286CC", Offset = "0x13286CC", VA = "0x1013286CC")]
		public SkillActionEffectSet SetupTo_ElementResist_Down(eElementType opt_elementType, float opt_value)
		{
			return null;
		}

		// Token: 0x0600104F RID: 4175 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1E")]
		[Address(RVA = "0x101328E38", Offset = "0x1328E38", VA = "0x101328E38")]
		public SkillActionEffectSet SetupTo_ElementChange(eElementType opt_elementType)
		{
			return null;
		}

		// Token: 0x06001050 RID: 4176 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F1F")]
		[Address(RVA = "0x101328F1C", Offset = "0x1328F1C", VA = "0x101328F1C")]
		public SkillActionEffectSet SetupTo_WeakElementBonus(float opt_value)
		{
			return null;
		}

		// Token: 0x06001051 RID: 4177 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F20")]
		[Address(RVA = "0x10132905C", Offset = "0x132905C", VA = "0x10132905C")]
		public SkillActionEffectSet SetupTo_CriticalDamageUp(float opt_value)
		{
			return null;
		}

		// Token: 0x06001052 RID: 4178 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F21")]
		[Address(RVA = "0x10132919C", Offset = "0x132919C", VA = "0x10132919C")]
		public SkillActionEffectSet SetupTo_CriticalDamageDown(float opt_value)
		{
			return null;
		}

		// Token: 0x06001053 RID: 4179 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F22")]
		[Address(RVA = "0x1013292DC", Offset = "0x13292DC", VA = "0x1013292DC")]
		public SkillActionEffectSet SetupTo_NextAttackUp(bool opt_isAtk, float opt_value)
		{
			return null;
		}

		// Token: 0x06001054 RID: 4180 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F23")]
		[Address(RVA = "0x101329434", Offset = "0x1329434", VA = "0x101329434")]
		public SkillActionEffectSet SetupTo_NextAttackCritical()
		{
			return null;
		}

		// Token: 0x06001055 RID: 4181 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F24")]
		[Address(RVA = "0x101329510", Offset = "0x1329510", VA = "0x101329510")]
		public SkillActionEffectSet SetupTo_Barrier(bool isPlayerReceived, float cutRatio)
		{
			return null;
		}

		// Token: 0x06001056 RID: 4182 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F25")]
		[Address(RVA = "0x101329854", Offset = "0x1329854", VA = "0x101329854")]
		public SkillActionEffectSet SetupTo_RecastChange(bool opt_isUp)
		{
			return null;
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F26")]
		[Address(RVA = "0x10132997C", Offset = "0x132997C", VA = "0x10132997C")]
		public SkillActionEffectSet SetupTo_KiraraJumpGaugeChange(float value)
		{
			return null;
		}

		// Token: 0x06001058 RID: 4184 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F27")]
		[Address(RVA = "0x101329AB0", Offset = "0x1329AB0", VA = "0x101329AB0")]
		public SkillActionEffectSet SetupTo_KiraraJumpGaugeUpOnDamage()
		{
			return null;
		}

		// Token: 0x06001059 RID: 4185 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F28")]
		[Address(RVA = "0x101329AC0", Offset = "0x1329AC0", VA = "0x101329AC0")]
		public SkillActionEffectSet SetupTo_HateChange(bool opt_isUp, float opt_value)
		{
			return null;
		}

		// Token: 0x0600105A RID: 4186 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F29")]
		[Address(RVA = "0x101329EF0", Offset = "0x1329EF0", VA = "0x101329EF0")]
		public SkillActionEffectSet SetupTo_ChargeChange(bool opt_isUp)
		{
			return null;
		}

		// Token: 0x0600105B RID: 4187 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F2A")]
		[Address(RVA = "0x10132A018", Offset = "0x132A018", VA = "0x10132A018")]
		public SkillActionEffectSet SetupTo_ChainCoefChange(float value)
		{
			return null;
		}

		// Token: 0x0600105C RID: 4188 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F2B")]
		[Address(RVA = "0x10132A13C", Offset = "0x132A13C", VA = "0x10132A13C")]
		public SkillActionEffectSet SetupTo_StunRecover()
		{
			return null;
		}

		// Token: 0x0600105D RID: 4189 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F2C")]
		[Address(RVA = "0x10132A170", Offset = "0x132A170", VA = "0x10132A170")]
		public SkillActionEffectSet SetupTo_Regene()
		{
			return null;
		}

		// Token: 0x0600105E RID: 4190 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F2D")]
		[Address(RVA = "0x10132A24C", Offset = "0x132A24C", VA = "0x10132A24C")]
		public SkillActionEffectSet SetupTo_LoadFactorReduce()
		{
			return null;
		}

		// Token: 0x0600105F RID: 4191 RVA: 0x00006FD8 File Offset: 0x000051D8
		[Token(Token = "0x6000F2E")]
		[Address(RVA = "0x1013264C8", Offset = "0x13264C8", VA = "0x1013264C8")]
		private static int CalcGrade(float src, float[] thresholds)
		{
			return 0;
		}

		// Token: 0x06001060 RID: 4192 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F2F")]
		[Address(RVA = "0x10132A3D0", Offset = "0x132A3D0", VA = "0x10132A3D0")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x100135F90", Offset = "0x135F90")]
		public IEnumerator Play(Camera camera, Transform parent, BattleUI battleUI)
		{
			return null;
		}

		// Token: 0x06001061 RID: 4193 RVA: 0x00006FF0 File Offset: 0x000051F0
		[Token(Token = "0x6000F30")]
		[Address(RVA = "0x10132A4C8", Offset = "0x132A4C8", VA = "0x10132A4C8")]
		public bool IsPlaying(bool isCheckTime = false)
		{
			return default(bool);
		}

		// Token: 0x06001062 RID: 4194 RVA: 0x00007008 File Offset: 0x00005208
		[Token(Token = "0x6000F31")]
		[Address(RVA = "0x10132A698", Offset = "0x132A698", VA = "0x10132A698")]
		private int ClampOverlay(int overlayNum)
		{
			return 0;
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F32")]
		[Address(RVA = "0x101325C50", Offset = "0x1325C50", VA = "0x101325C50")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Buff(int overlayNum)
		{
			return null;
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F33")]
		[Address(RVA = "0x101326024", Offset = "0x1326024", VA = "0x101326024")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Debuff(int overlayNum)
		{
			return null;
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F34")]
		[Address(RVA = "0x10132743C", Offset = "0x132743C", VA = "0x10132743C")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_StatusChangeReset(int overlayNum)
		{
			return null;
		}

		// Token: 0x06001066 RID: 4198 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F35")]
		[Address(RVA = "0x101327674", Offset = "0x1327674", VA = "0x101327674")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_StatusChangeDisable(bool isBuff)
		{
			return null;
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F36")]
		[Address(RVA = "0x10132667C", Offset = "0x132667C", VA = "0x10132667C")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Recover(int grade)
		{
			return null;
		}

		// Token: 0x06001068 RID: 4200 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F37")]
		[Address(RVA = "0x101327FB4", Offset = "0x1327FB4", VA = "0x101327FB4")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_ElementUp(int overlayNum, eElementType elementType)
		{
			return null;
		}

		// Token: 0x06001069 RID: 4201 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F38")]
		[Address(RVA = "0x10132881C", Offset = "0x132881C", VA = "0x10132881C")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_ElementDown(int overlayNum, eElementType elementType)
		{
			return null;
		}

		// Token: 0x0600106A RID: 4202 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F39")]
		[Address(RVA = "0x1013278C4", Offset = "0x13278C4", VA = "0x1013278C4")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Abnormal(eStateAbnormal stateAbnormal)
		{
			return null;
		}

		// Token: 0x0600106B RID: 4203 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3A")]
		[Address(RVA = "0x101327BF4", Offset = "0x1327BF4", VA = "0x101327BF4")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_AbnormalRecover(int overlayNum)
		{
			return null;
		}

		// Token: 0x0600106C RID: 4204 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3B")]
		[Address(RVA = "0x10132961C", Offset = "0x132961C", VA = "0x10132961C")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Barrier(bool isPlayerReceived, float cutRatio)
		{
			return null;
		}

		// Token: 0x0600106D RID: 4205 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3C")]
		[Address(RVA = "0x101329C48", Offset = "0x1329C48", VA = "0x101329C48")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_HateUp()
		{
			return null;
		}

		// Token: 0x0600106E RID: 4206 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3D")]
		[Address(RVA = "0x101329D9C", Offset = "0x1329D9C", VA = "0x101329D9C")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_HateDown()
		{
			return null;
		}

		// Token: 0x0600106F RID: 4207 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3E")]
		[Address(RVA = "0x10132A280", Offset = "0x132A280", VA = "0x10132A280")]
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_LoadFactorReduce()
		{
			return null;
		}

		// Token: 0x0400132F RID: 4911
		[Token(Token = "0x4000DF8")]
		private const float EFFECT_PLAY_OVERLAY_OFFSET_Z = -1.5f;

		// Token: 0x04001330 RID: 4912
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000DF9")]
		private readonly string[,] EffectID_ElementUp;

		// Token: 0x04001331 RID: 4913
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DFA")]
		private readonly string[,] EffectID_ElementDown;

		// Token: 0x04001332 RID: 4914
		[Token(Token = "0x4000DFB")]
		private const int BUFFEFFECT_THRESHOLDS_NUM = 4;

		// Token: 0x04001333 RID: 4915
		[Token(Token = "0x4000DFC")]
		private const int BUFFEFFECT_THRESHOLDS_ALLNUM = 12;

		// Token: 0x04001334 RID: 4916
		[Token(Token = "0x4000DFD")]
		private const int EFFECT_THRESHOLDS_NUM = 4;

		// Token: 0x04001335 RID: 4917
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DFE")]
		private List<SkillActionEffectSet.EffectData> m_EffectDatas;

		// Token: 0x04001336 RID: 4918
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DFF")]
		private List<SkillActionEffectSet.IconData> m_IconDatas;

		// Token: 0x04001337 RID: 4919
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E00")]
		private CharacterHandler m_TargetCharaHndl;

		// Token: 0x04001338 RID: 4920
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E01")]
		private Transform m_EffectTo;

		// Token: 0x04001339 RID: 4921
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E02")]
		private int m_OverlayNum;

		// Token: 0x0400133A RID: 4922
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000E03")]
		private bool m_IsDoneEffectDatas;

		// Token: 0x0400133B RID: 4923
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000E04")]
		private List<EffectHandler> m_ActiveEffectList;

		// Token: 0x0400133C RID: 4924
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E05")]
		private BattleUI m_UI;

		// Token: 0x0400133D RID: 4925
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000E06")]
		private float m_dispScale;

		// Token: 0x0200043E RID: 1086
		[Token(Token = "0x2000DB1")]
		private class EffectData
		{
			// Token: 0x06001070 RID: 4208 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF5")]
			[Address(RVA = "0x10132A708", Offset = "0x132A708", VA = "0x10132A708")]
			public EffectData(string effectID, float animSpeed, float delaySec, Vector3 offset, Vector3 scale, bool isPopupIcon, int argIndexCtrlParam_Visible = -1, int argIndexCtrlParam_Color = -1)
			{
			}

			// Token: 0x0400133E RID: 4926
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058B8")]
			public bool m_IsPlayed;

			// Token: 0x0400133F RID: 4927
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058B9")]
			public string m_EffectID;

			// Token: 0x04001340 RID: 4928
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40058BA")]
			public float m_AnimSpeed;

			// Token: 0x04001341 RID: 4929
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40058BB")]
			public float m_DelaySec;

			// Token: 0x04001342 RID: 4930
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40058BC")]
			public Vector3 m_Offset;

			// Token: 0x04001343 RID: 4931
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x40058BD")]
			public Vector3 m_Scale;

			// Token: 0x04001344 RID: 4932
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40058BE")]
			public bool m_IsPopupIcon;

			// Token: 0x04001345 RID: 4933
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x40058BF")]
			public int m_ArgIndexCtrlParam_Visible;

			// Token: 0x04001346 RID: 4934
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40058C0")]
			public int m_ArgIndexCtrlParam_Color;
		}

		// Token: 0x0200043F RID: 1087
		[Token(Token = "0x2000DB2")]
		public class IconData
		{
			// Token: 0x06001071 RID: 4209 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DF6")]
			[Address(RVA = "0x101325C10", Offset = "0x1325C10", VA = "0x101325C10")]
			public IconData(ePopUpStateIconType iconType, int grade = -1, bool isMiss = false)
			{
			}

			// Token: 0x04001347 RID: 4935
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058C1")]
			public ePopUpStateIconType m_IconType;

			// Token: 0x04001348 RID: 4936
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40058C2")]
			public int m_Grade;

			// Token: 0x04001349 RID: 4937
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058C3")]
			public bool m_IsMiss;
		}
	}
}
