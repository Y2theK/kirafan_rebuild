﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200043B RID: 1083
	[Token(Token = "0x200035E")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_Base
	{
		// Token: 0x0600103C RID: 4156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F0B")]
		[Address(RVA = "0x10132AEDC", Offset = "0x132AEDC", VA = "0x10132AEDC")]
		public SkillActionEvent_Base()
		{
		}

		// Token: 0x04001323 RID: 4899
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000DEC")]
		public int m_Frame;
	}
}
