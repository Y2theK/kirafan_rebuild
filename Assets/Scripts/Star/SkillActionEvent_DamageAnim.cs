﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200042C RID: 1068
	[Token(Token = "0x200034F")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_DamageAnim : SkillActionEvent_Base
	{
		// Token: 0x0600102D RID: 4141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EFC")]
		[Address(RVA = "0x10132AEE4", Offset = "0x132AEE4", VA = "0x10132AEE4")]
		public SkillActionEvent_DamageAnim()
		{
		}
	}
}
