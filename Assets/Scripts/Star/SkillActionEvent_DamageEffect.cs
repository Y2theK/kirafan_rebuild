﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200042D RID: 1069
	[Token(Token = "0x2000350")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_DamageEffect : SkillActionEvent_Base
	{
		// Token: 0x0600102E RID: 4142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EFD")]
		[Address(RVA = "0x10132AEEC", Offset = "0x132AEEC", VA = "0x10132AEEC")]
		public SkillActionEvent_DamageEffect()
		{
		}

		// Token: 0x040012E3 RID: 4835
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DAC")]
		public SkillActionInfo_DamageEffect m_DamageEffect;
	}
}
