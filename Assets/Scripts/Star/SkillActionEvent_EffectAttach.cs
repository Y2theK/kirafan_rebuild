﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000432 RID: 1074
	[Token(Token = "0x2000355")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectAttach : SkillActionEvent_Base
	{
		// Token: 0x06001033 RID: 4147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F02")]
		[Address(RVA = "0x10132AEF4", Offset = "0x132AEF4", VA = "0x10132AEF4")]
		public SkillActionEvent_EffectAttach()
		{
		}

		// Token: 0x04001309 RID: 4873
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DD2")]
		public string m_EffectID;

		// Token: 0x0400130A RID: 4874
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DD3")]
		public short m_UniqueID;

		// Token: 0x0400130B RID: 4875
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000DD4")]
		public eSkillActionEffectAttachTo m_AttachTo;

		// Token: 0x0400130C RID: 4876
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DD5")]
		public eSkillActionLocatorType m_AttachToLocatorType;

		// Token: 0x0400130D RID: 4877
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DD6")]
		public Vector2 m_PosOffset;
	}
}
