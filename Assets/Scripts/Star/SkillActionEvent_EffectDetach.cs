﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000433 RID: 1075
	[Token(Token = "0x2000356")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectDetach : SkillActionEvent_Base
	{
		// Token: 0x06001034 RID: 4148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F03")]
		[Address(RVA = "0x10132AEFC", Offset = "0x132AEFC", VA = "0x10132AEFC")]
		public SkillActionEvent_EffectDetach()
		{
		}

		// Token: 0x0400130E RID: 4878
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000DD7")]
		public short m_UniqueID;
	}
}
