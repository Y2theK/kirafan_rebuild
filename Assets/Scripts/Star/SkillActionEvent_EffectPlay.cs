﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200042E RID: 1070
	[Token(Token = "0x2000351")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectPlay : SkillActionEvent_Base
	{
		// Token: 0x0600102F RID: 4143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EFE")]
		[Address(RVA = "0x10132AF04", Offset = "0x132AF04", VA = "0x10132AF04")]
		public SkillActionEvent_EffectPlay()
		{
		}

		// Token: 0x040012E4 RID: 4836
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DAD")]
		public string m_EffectID;

		// Token: 0x040012E5 RID: 4837
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DAE")]
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x040012E6 RID: 4838
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000DAF")]
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x040012E7 RID: 4839
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DB0")]
		public Vector2 m_TargetPosOffset;
	}
}
