﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000430 RID: 1072
	[Token(Token = "0x2000353")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectProjectile_Parabola : SkillActionEvent_Base
	{
		// Token: 0x06001031 RID: 4145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F00")]
		[Address(RVA = "0x10132AF0C", Offset = "0x132AF0C", VA = "0x10132AF0C")]
		public SkillActionEvent_EffectProjectile_Parabola()
		{
		}

		// Token: 0x040012F2 RID: 4850
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DBB")]
		public string m_CallbackKey;

		// Token: 0x040012F3 RID: 4851
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DBC")]
		public string m_EffectID;

		// Token: 0x040012F4 RID: 4852
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DBD")]
		public short m_UniqueID;

		// Token: 0x040012F5 RID: 4853
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DBE")]
		public float m_ArrivalFrame;

		// Token: 0x040012F6 RID: 4854
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000DBF")]
		public float m_GravityAccel;

		// Token: 0x040012F7 RID: 4855
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000DC0")]
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x040012F8 RID: 4856
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000DC1")]
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x040012F9 RID: 4857
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000DC2")]
		public Vector2 m_StartPosOffset;

		// Token: 0x040012FA RID: 4858
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000DC3")]
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x040012FB RID: 4859
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000DC4")]
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x040012FC RID: 4860
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4000DC5")]
		public Vector2 m_TargetPosOffset;
	}
}
