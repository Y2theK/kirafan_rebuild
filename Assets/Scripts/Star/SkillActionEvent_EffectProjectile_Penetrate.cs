﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000431 RID: 1073
	[Token(Token = "0x2000354")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectProjectile_Penetrate : SkillActionEvent_Base
	{
		// Token: 0x06001032 RID: 4146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F01")]
		[Address(RVA = "0x10132AF14", Offset = "0x132AF14", VA = "0x10132AF14")]
		public SkillActionEvent_EffectProjectile_Penetrate()
		{
		}

		// Token: 0x040012FD RID: 4861
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DC6")]
		public string m_CallbackKey;

		// Token: 0x040012FE RID: 4862
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DC7")]
		public string m_EffectID;

		// Token: 0x040012FF RID: 4863
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DC8")]
		public short m_UniqueID;

		// Token: 0x04001300 RID: 4864
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DC9")]
		public float m_Speed;

		// Token: 0x04001301 RID: 4865
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000DCA")]
		public float m_DelaySec;

		// Token: 0x04001302 RID: 4866
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000DCB")]
		public float m_AliveSec;

		// Token: 0x04001303 RID: 4867
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000DCC")]
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x04001304 RID: 4868
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000DCD")]
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x04001305 RID: 4869
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000DCE")]
		public Vector2 m_StartPosOffset;

		// Token: 0x04001306 RID: 4870
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000DCF")]
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x04001307 RID: 4871
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4000DD0")]
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x04001308 RID: 4872
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000DD1")]
		public Vector2 m_TargetPosOffset;
	}
}
