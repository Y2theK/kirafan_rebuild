﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200042F RID: 1071
	[Token(Token = "0x2000352")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_EffectProjectile_Straight : SkillActionEvent_Base
	{
		// Token: 0x06001030 RID: 4144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EFF")]
		[Address(RVA = "0x10132AF1C", Offset = "0x132AF1C", VA = "0x10132AF1C")]
		public SkillActionEvent_EffectProjectile_Straight()
		{
		}

		// Token: 0x040012E8 RID: 4840
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DB1")]
		public string m_CallbackKey;

		// Token: 0x040012E9 RID: 4841
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DB2")]
		public string m_EffectID;

		// Token: 0x040012EA RID: 4842
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DB3")]
		public short m_UniqueID;

		// Token: 0x040012EB RID: 4843
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DB4")]
		public float m_Speed;

		// Token: 0x040012EC RID: 4844
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000DB5")]
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x040012ED RID: 4845
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000DB6")]
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x040012EE RID: 4846
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000DB7")]
		public Vector2 m_StartPosOffset;

		// Token: 0x040012EF RID: 4847
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000DB8")]
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x040012F0 RID: 4848
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000DB9")]
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x040012F1 RID: 4849
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000DBA")]
		public Vector2 m_TargetPosOffset;
	}
}
