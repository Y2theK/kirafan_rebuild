﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000439 RID: 1081
	[Token(Token = "0x200035C")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_PlaySE : SkillActionEvent_Base
	{
		// Token: 0x0600103A RID: 4154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F09")]
		[Address(RVA = "0x10132AF24", Offset = "0x132AF24", VA = "0x10132AF24")]
		public SkillActionEvent_PlaySE()
		{
		}

		// Token: 0x04001321 RID: 4897
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DEA")]
		public string m_CueName;
	}
}
