﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200043A RID: 1082
	[Token(Token = "0x200035D")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_PlayVoice : SkillActionEvent_Base
	{
		// Token: 0x0600103B RID: 4155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F0A")]
		[Address(RVA = "0x10132AF2C", Offset = "0x132AF2C", VA = "0x10132AF2C")]
		public SkillActionEvent_PlayVoice()
		{
		}

		// Token: 0x04001322 RID: 4898
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DEB")]
		public string m_VoiceCueName;
	}
}
