﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200042B RID: 1067
	[Token(Token = "0x200034E")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_Solve : SkillActionEvent_Base
	{
		// Token: 0x0600102C RID: 4140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EFB")]
		[Address(RVA = "0x10132AF34", Offset = "0x132AF34", VA = "0x10132AF34")]
		public SkillActionEvent_Solve()
		{
		}

		// Token: 0x040012DF RID: 4831
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000DA8")]
		public byte m_Index;

		// Token: 0x040012E0 RID: 4832
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DA9")]
		public float m_Option_0;

		// Token: 0x040012E1 RID: 4833
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4000DAA")]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011F28C", Offset = "0x11F28C")]
		public bool m_IsEnableDamageEffect;

		// Token: 0x040012E2 RID: 4834
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DAB")]
		public SkillActionInfo_DamageEffect m_DamageEffect;
	}
}
