﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000434 RID: 1076
	[Token(Token = "0x2000357")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_TrailAttach : SkillActionEvent_Base
	{
		// Token: 0x06001035 RID: 4149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F04")]
		[Address(RVA = "0x10132AF3C", Offset = "0x132AF3C", VA = "0x10132AF3C")]
		public SkillActionEvent_TrailAttach()
		{
		}

		// Token: 0x0400130F RID: 4879
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DD8")]
		public string m_EffectID;

		// Token: 0x04001310 RID: 4880
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DD9")]
		public short m_UniqueID;

		// Token: 0x04001311 RID: 4881
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000DDA")]
		public eSkillActionTrailAttachTo m_AttachTo;

		// Token: 0x04001312 RID: 4882
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DDB")]
		public eSkillActionLocatorType m_AttachToLocatorType;

		// Token: 0x04001313 RID: 4883
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DDC")]
		public Vector2 m_PosOffset;
	}
}
