﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000435 RID: 1077
	[Token(Token = "0x2000358")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_TrailDetach : SkillActionEvent_Base
	{
		// Token: 0x06001036 RID: 4150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F05")]
		[Address(RVA = "0x10132AF44", Offset = "0x132AF44", VA = "0x10132AF44")]
		public SkillActionEvent_TrailDetach()
		{
		}

		// Token: 0x04001314 RID: 4884
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000DDD")]
		public short m_UniqueID;
	}
}
