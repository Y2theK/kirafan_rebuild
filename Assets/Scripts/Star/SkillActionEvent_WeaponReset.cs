﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000438 RID: 1080
	[Token(Token = "0x200035B")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_WeaponReset : SkillActionEvent_Base
	{
		// Token: 0x06001039 RID: 4153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F08")]
		[Address(RVA = "0x10132AF4C", Offset = "0x132AF4C", VA = "0x10132AF4C")]
		public SkillActionEvent_WeaponReset()
		{
		}
	}
}
