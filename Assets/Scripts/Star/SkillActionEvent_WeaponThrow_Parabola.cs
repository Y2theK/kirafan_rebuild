﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000436 RID: 1078
	[Token(Token = "0x2000359")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_WeaponThrow_Parabola : SkillActionEvent_Base
	{
		// Token: 0x06001037 RID: 4151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F06")]
		[Address(RVA = "0x10132AF54", Offset = "0x132AF54", VA = "0x10132AF54")]
		public SkillActionEvent_WeaponThrow_Parabola()
		{
		}

		// Token: 0x04001315 RID: 4885
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DDE")]
		public string m_CallbackKey;

		// Token: 0x04001316 RID: 4886
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000DDF")]
		public bool m_IsLeftWeapon;

		// Token: 0x04001317 RID: 4887
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4000DE0")]
		public bool m_IsRotationLinkAnim;

		// Token: 0x04001318 RID: 4888
		[Cpp2IlInjected.FieldOffset(Offset = "0x22")]
		[Token(Token = "0x4000DE1")]
		public bool m_IsAutoDetachOnArrival;

		// Token: 0x04001319 RID: 4889
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000DE2")]
		public float m_ArrivalFrame;

		// Token: 0x0400131A RID: 4890
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000DE3")]
		public float m_GravityAccel;

		// Token: 0x0400131B RID: 4891
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4000DE4")]
		public eSkillActionWeaponThrowPosType m_TargetPosType;

		// Token: 0x0400131C RID: 4892
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000DE5")]
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x0400131D RID: 4893
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4000DE6")]
		public Vector2 m_TargetPosOffset;
	}
}
