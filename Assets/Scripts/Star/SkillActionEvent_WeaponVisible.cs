﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000437 RID: 1079
	[Token(Token = "0x200035A")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionEvent_WeaponVisible : SkillActionEvent_Base
	{
		// Token: 0x06001038 RID: 4152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F07")]
		[Address(RVA = "0x10132AF5C", Offset = "0x132AF5C", VA = "0x10132AF5C")]
		public SkillActionEvent_WeaponVisible()
		{
		}

		// Token: 0x0400131E RID: 4894
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000DE7")]
		public bool m_IsLeftWeapon;

		// Token: 0x0400131F RID: 4895
		[Cpp2IlInjected.FieldOffset(Offset = "0x15")]
		[Token(Token = "0x4000DE8")]
		public bool m_IsVisible;

		// Token: 0x04001320 RID: 4896
		[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
		[Token(Token = "0x4000DE9")]
		public bool m_IsPosReset;
	}
}
