﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000441 RID: 1089
	[Token(Token = "0x2000361")]
	[StructLayout(3)]
	public class SkillActionGraphics : ScriptableObject
	{
		// Token: 0x06001078 RID: 4216 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F3F")]
		[Address(RVA = "0x10132AF64", Offset = "0x132AF64", VA = "0x10132AF64")]
		public Dictionary<string, int> GetEffectIDs(int charaID, int viewCharaID, eElementType elementType, int classAnimType)
		{
			return null;
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F40")]
		[Address(RVA = "0x10132B700", Offset = "0x132B700", VA = "0x10132B700")]
		public string CreateSkillActKey()
		{
			return null;
		}

		// Token: 0x0600107A RID: 4218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F41")]
		[Address(RVA = "0x10132B7B0", Offset = "0x132B7B0", VA = "0x10132B7B0")]
		public SkillActionGraphics()
		{
		}

		// Token: 0x04001353 RID: 4947
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E07")]
		public string m_ID;

		// Token: 0x04001354 RID: 4948
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E08")]
		public eClassType m_SettingClassType;

		// Token: 0x04001355 RID: 4949
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000E09")]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10011F2C4", Offset = "0x11F2C4")]
		public sbyte m_SettingMotionID;

		// Token: 0x04001356 RID: 4950
		[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
		[Token(Token = "0x4000E0A")]
		public byte m_SettingGrade;

		// Token: 0x04001357 RID: 4951
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E0B")]
		public List<SkillActionEvent_EffectPlay> m_evEffectPlay;

		// Token: 0x04001358 RID: 4952
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E0C")]
		public List<SkillActionEvent_EffectProjectile_Straight> m_evEffectProjectile_Straight;

		// Token: 0x04001359 RID: 4953
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E0D")]
		public List<SkillActionEvent_EffectProjectile_Parabola> m_evEffectProjectile_Parabola;

		// Token: 0x0400135A RID: 4954
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E0E")]
		public List<SkillActionEvent_EffectProjectile_Penetrate> m_evEffectProjectile_Penetrate;

		// Token: 0x0400135B RID: 4955
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000E0F")]
		public List<SkillActionEvent_EffectAttach> m_evEffectAttach;

		// Token: 0x0400135C RID: 4956
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E10")]
		public List<SkillActionEvent_EffectDetach> m_evEffectDetach;

		// Token: 0x0400135D RID: 4957
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000E11")]
		public List<SkillActionEvent_TrailAttach> m_evTrailAttach;

		// Token: 0x0400135E RID: 4958
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000E12")]
		public List<SkillActionEvent_TrailDetach> m_evTrailDetach;

		// Token: 0x0400135F RID: 4959
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000E13")]
		public List<SkillActionEvent_WeaponThrow_Parabola> m_evWeaponThrow_Parabola;

		// Token: 0x04001360 RID: 4960
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000E14")]
		public List<SkillActionEvent_WeaponVisible> m_evWeaponVisible;

		// Token: 0x04001361 RID: 4961
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000E15")]
		public List<SkillActionEvent_WeaponReset> m_evWeaponReset;

		// Token: 0x04001362 RID: 4962
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000E16")]
		public List<SkillActionEvent_PlaySE> m_evPlaySE;

		// Token: 0x04001363 RID: 4963
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000E17")]
		public List<SkillActionEvent_PlayVoice> m_evPlayVoice;
	}
}
