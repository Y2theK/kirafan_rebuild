﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200042A RID: 1066
	[Token(Token = "0x200034D")]
	[Serializable]
	[StructLayout(3)]
	public class SkillActionInfo_DamageEffect
	{
		// Token: 0x17000109 RID: 265
		// (get) Token: 0x06001029 RID: 4137 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000F4")]
		public string TempEffectID
		{
			[Token(Token = "0x6000EF8")]
			[Address(RVA = "0x10132B7B8", Offset = "0x132B7B8", VA = "0x10132B7B8")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000EF9")]
		[Address(RVA = "0x10132B8E4", Offset = "0x132B8E4", VA = "0x10132B8E4")]
		public SkillActionInfo_DamageEffect()
		{
		}

		// Token: 0x040012DB RID: 4827
		[Token(Token = "0x4000DA4")]
		public static readonly byte[] DAMAGE_EFFECT_RANGE;

		// Token: 0x040012DC RID: 4828
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000DA5")]
		public byte m_Grade;

		// Token: 0x040012DD RID: 4829
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4000DA6")]
		public eDmgEffectType m_EffectType;

		// Token: 0x040012DE RID: 4830
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000DA7")]
		public eDmgEffectSeType m_SeType;
	}
}
