﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000442 RID: 1090
	[Token(Token = "0x2000362")]
	[StructLayout(3)]
	public class SkillActionMovementBase
	{
		// Token: 0x1700010D RID: 269
		// (get) Token: 0x0600107B RID: 4219 RVA: 0x00007038 File Offset: 0x00005238
		[Token(Token = "0x170000F6")]
		public bool IsMoving
		{
			[Token(Token = "0x6000F42")]
			[Address(RVA = "0x10132B988", Offset = "0x132B988", VA = "0x10132B988")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x0600107C RID: 4220 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000F7")]
		public Transform MoveObj
		{
			[Token(Token = "0x6000F43")]
			[Address(RVA = "0x10132B990", Offset = "0x132B990", VA = "0x10132B990")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F44")]
		[Address(RVA = "0x10132B998", Offset = "0x132B998", VA = "0x10132B998")]
		public SkillActionMovementBase(Transform moveObj, bool isLocalPos)
		{
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F45")]
		[Address(RVA = "0x10132B9D4", Offset = "0x132B9D4", VA = "0x10132B9D4")]
		public void Play()
		{
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x00007050 File Offset: 0x00005250
		[Token(Token = "0x6000F46")]
		[Address(RVA = "0x10132B9E0", Offset = "0x132B9E0", VA = "0x10132B9E0", Slot = "4")]
		public virtual bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			return default(bool);
		}

		// Token: 0x04001364 RID: 4964
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000E18")]
		protected bool m_IsMoving;

		// Token: 0x04001365 RID: 4965
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E19")]
		protected Transform m_MoveObj;

		// Token: 0x04001366 RID: 4966
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E1A")]
		protected bool m_IsLocalPos;
	}
}
