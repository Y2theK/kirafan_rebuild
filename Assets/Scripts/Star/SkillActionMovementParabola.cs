﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000444 RID: 1092
	[Token(Token = "0x2000364")]
	[StructLayout(3)]
	public class SkillActionMovementParabola : SkillActionMovementBase
	{
		// Token: 0x06001082 RID: 4226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F49")]
		[Address(RVA = "0x10132BA6C", Offset = "0x132BA6C", VA = "0x10132BA6C")]
		public SkillActionMovementParabola(Transform moveObj, bool isLocalPos, Vector3 startPos, Vector3 targetPos, float arrivalSec, float gravityAccel)
		{
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x00007080 File Offset: 0x00005280
		[Token(Token = "0x6000F4A")]
		[Address(RVA = "0x10132BC1C", Offset = "0x132BC1C", VA = "0x10132BC1C", Slot = "4")]
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			return default(bool);
		}

		// Token: 0x0400136D RID: 4973
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000E21")]
		private Vector3 m_StartPos;

		// Token: 0x0400136E RID: 4974
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E22")]
		private Vector3 m_TargetPos;

		// Token: 0x0400136F RID: 4975
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000E23")]
		private float m_ArrivalSec;

		// Token: 0x04001370 RID: 4976
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E24")]
		private float m_g;

		// Token: 0x04001371 RID: 4977
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000E25")]
		private Vector3 m_vMove;

		// Token: 0x04001372 RID: 4978
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E26")]
		private bool m_IsArrival;

		// Token: 0x04001373 RID: 4979
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4000E27")]
		private float m_t;
	}
}
