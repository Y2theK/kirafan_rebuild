﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000445 RID: 1093
	[Token(Token = "0x2000365")]
	[StructLayout(3)]
	public class SkillActionMovementPenetrate : SkillActionMovementBase
	{
		// Token: 0x06001084 RID: 4228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F4B")]
		[Address(RVA = "0x10132BE5C", Offset = "0x132BE5C", VA = "0x10132BE5C")]
		public SkillActionMovementPenetrate(Transform moveObj, bool isLocalPos, Vector3 startPos, List<Vector3> targetPosList, float speed, float delaySec, float aliveSec)
		{
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00007098 File Offset: 0x00005298
		[Token(Token = "0x6000F4C")]
		[Address(RVA = "0x10132C0A8", Offset = "0x132C0A8", VA = "0x10132C0A8", Slot = "4")]
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			return default(bool);
		}

		// Token: 0x04001374 RID: 4980
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000E28")]
		private Vector3 m_StartPos;

		// Token: 0x04001375 RID: 4981
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E29")]
		private List<Vector3> m_TargetPosList;

		// Token: 0x04001376 RID: 4982
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E2A")]
		private float m_Speed;

		// Token: 0x04001377 RID: 4983
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000E2B")]
		private float m_DelaySec;

		// Token: 0x04001378 RID: 4984
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E2C")]
		private float m_AliveSec;

		// Token: 0x04001379 RID: 4985
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000E2D")]
		private Vector3 m_vDir;

		// Token: 0x0400137A RID: 4986
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E2E")]
		private List<bool> m_IsArrivals;

		// Token: 0x0400137B RID: 4987
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000E2F")]
		private float m_t;
	}
}
