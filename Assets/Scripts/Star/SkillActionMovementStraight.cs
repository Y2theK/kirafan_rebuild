﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000443 RID: 1091
	[Token(Token = "0x2000363")]
	[StructLayout(3)]
	public class SkillActionMovementStraight : SkillActionMovementBase
	{
		// Token: 0x06001080 RID: 4224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F47")]
		[Address(RVA = "0x10132C4B0", Offset = "0x132C4B0", VA = "0x10132C4B0")]
		public SkillActionMovementStraight(Transform moveObj, bool isLocalPos, Vector3 startPos, Vector3 targetPos, float speed)
		{
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x00007068 File Offset: 0x00005268
		[Token(Token = "0x6000F48")]
		[Address(RVA = "0x10132C654", Offset = "0x132C654", VA = "0x10132C654", Slot = "4")]
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			return default(bool);
		}

		// Token: 0x04001367 RID: 4967
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4000E1B")]
		private Vector3 m_StartPos;

		// Token: 0x04001368 RID: 4968
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E1C")]
		private Vector3 m_TargetPos;

		// Token: 0x04001369 RID: 4969
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4000E1D")]
		private float m_Speed;

		// Token: 0x0400136A RID: 4970
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E1E")]
		private float m_ArrivalSec;

		// Token: 0x0400136B RID: 4971
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4000E1F")]
		private bool m_IsArrival;

		// Token: 0x0400136C RID: 4972
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000E20")]
		private float m_t;
	}
}
