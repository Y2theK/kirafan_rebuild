﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000446 RID: 1094
	[Token(Token = "0x2000366")]
	[StructLayout(3)]
	public class SkillActionPlan : ScriptableObject, ISerializationCallbackReceiver
	{
		// Token: 0x06001086 RID: 4230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F4D")]
		[Address(RVA = "0x10132C858", Offset = "0x132C858", VA = "0x10132C858", Slot = "4")]
		public void OnBeforeSerialize()
		{
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F4E")]
		[Address(RVA = "0x10132C85C", Offset = "0x132C85C", VA = "0x10132C85C", Slot = "5")]
		public void OnAfterDeserialize()
		{
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F4F")]
		[Address(RVA = "0x10132CA8C", Offset = "0x132CA8C", VA = "0x10132CA8C")]
		public Dictionary<string, int> GetEffectIDs(eElementType elementType)
		{
			return null;
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F50")]
		[Address(RVA = "0x10132D338", Offset = "0x132D338", VA = "0x10132D338")]
		public SkillActionPlan()
		{
		}

		// Token: 0x0400137C RID: 4988
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E30")]
		public string m_ID;

		// Token: 0x0400137D RID: 4989
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E31")]
		public List<SkillActionEvent_Solve> m_evSolve;

		// Token: 0x0400137E RID: 4990
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E32")]
		public List<SkillActionEvent_DamageAnim> m_evDamageAnim;

		// Token: 0x0400137F RID: 4991
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E33")]
		public List<SkillActionEvent_DamageEffect> m_evDamageEffect;

		// Token: 0x04001380 RID: 4992
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E34")]
		public List<SkillActionPlan.CallbackData> m_CallbackDatas;

		// Token: 0x04001381 RID: 4993
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E35")]
		public Dictionary<string, SkillActionPlan.CallbackData> m_CallbackDict;

		// Token: 0x02000447 RID: 1095
		[Token(Token = "0x2000DB4")]
		[Serializable]
		public class CallbackData
		{
			// Token: 0x0600108A RID: 4234 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DFD")]
			[Address(RVA = "0x10132D340", Offset = "0x132D340", VA = "0x10132D340")]
			public CallbackData()
			{
			}

			// Token: 0x04001382 RID: 4994
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058CD")]
			public string m_Key;

			// Token: 0x04001383 RID: 4995
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058CE")]
			public List<SkillActionEvent_Solve> m_evSolve;
		}
	}
}
