﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000448 RID: 1096
	[Token(Token = "0x2000367")]
	[StructLayout(3)]
	public class SkillActionPlayer : MonoBehaviour
	{
		// Token: 0x1400000B RID: 11
		// (add) Token: 0x0600108B RID: 4235 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600108C RID: 4236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400000B")]
		private event SkillActionPlayer.GetRefAnimTimeDelegate GetRefAnimTimeCallback
		{
			[Token(Token = "0x6000F51")]
			[Address(RVA = "0x10132D348", Offset = "0x132D348", VA = "0x10132D348")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6000F52")]
			[Address(RVA = "0x10132D454", Offset = "0x132D454", VA = "0x10132D454")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x0600108D RID: 4237 RVA: 0x000070B0 File Offset: 0x000052B0
		[Token(Token = "0x170000F8")]
		public int PlayingKeyFrame
		{
			[Token(Token = "0x6000F53")]
			[Address(RVA = "0x10132D560", Offset = "0x132D560", VA = "0x10132D560")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x0600108E RID: 4238 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000F9")]
		private Transform OwnerTransform
		{
			[Token(Token = "0x6000F54")]
			[Address(RVA = "0x10132D568", Offset = "0x132D568", VA = "0x10132D568")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600108F RID: 4239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F55")]
		[Address(RVA = "0x10132D620", Offset = "0x132D620", VA = "0x10132D620")]
		public void Setup(BattleSystem battleSystem)
		{
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F56")]
		[Address(RVA = "0x10132D680", Offset = "0x132D680", VA = "0x10132D680")]
		public void Destroy()
		{
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x000070C8 File Offset: 0x000052C8
		[Token(Token = "0x6000F57")]
		[Address(RVA = "0x10132E408", Offset = "0x132E408", VA = "0x10132E408")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x000070E0 File Offset: 0x000052E0
		[Token(Token = "0x6000F58")]
		[Address(RVA = "0x10132E410", Offset = "0x132E410", VA = "0x10132E410")]
		public bool Play(SkillActionPlan sap, SkillActionGraphics sag, CharacterHandler owner, BattleCommandData command, List<CharacterHandler> tgtCharaList, List<CharacterHandler> myCharaList, SkillActionPlayer.GetRefAnimTimeDelegate getRefAnimTimeCallback)
		{
			return default(bool);
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F59")]
		[Address(RVA = "0x10132E370", Offset = "0x132E370", VA = "0x10132E370")]
		public void ClearGetRefAnimTimeCallback()
		{
		}

		// Token: 0x06001094 RID: 4244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5A")]
		[Address(RVA = "0x10132EDD8", Offset = "0x132EDD8", VA = "0x10132EDD8")]
		public void LateUpdate()
		{
		}

		// Token: 0x06001095 RID: 4245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5B")]
		[Address(RVA = "0x10133574C", Offset = "0x133574C", VA = "0x10133574C")]
		public void OnGUIDraw()
		{
		}

		// Token: 0x06001096 RID: 4246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5C")]
		[Address(RVA = "0x101334964", Offset = "0x1334964", VA = "0x101334964")]
		private void UpdatePlayingStatus()
		{
		}

		// Token: 0x06001097 RID: 4247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5D")]
		[Address(RVA = "0x101335758", Offset = "0x1335758", VA = "0x101335758")]
		public void Skip()
		{
		}

		// Token: 0x06001098 RID: 4248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5E")]
		[Address(RVA = "0x10132FF98", Offset = "0x132FF98", VA = "0x10132FF98")]
		private void UpdateEventSolve()
		{
		}

		// Token: 0x06001099 RID: 4249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F5F")]
		[Address(RVA = "0x10133618C", Offset = "0x133618C", VA = "0x10133618C")]
		private void UpdateEventSolve_Exec(SkillActionEvent_Solve ev, CharacterHandler targetCharaHndl, bool isSolveOnSkip)
		{
		}

		// Token: 0x0600109A RID: 4250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F60")]
		[Address(RVA = "0x10133012C", Offset = "0x133012C", VA = "0x10133012C")]
		private void UpdateEventDamageAnim()
		{
		}

		// Token: 0x0600109B RID: 4251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F61")]
		[Address(RVA = "0x101337CAC", Offset = "0x1337CAC", VA = "0x101337CAC")]
		private void UpdateEventDamageAnim_Exec(SkillActionEvent_DamageAnim ev, CharacterHandler targetCharaHndl)
		{
		}

		// Token: 0x0600109C RID: 4252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F62")]
		[Address(RVA = "0x1013302C0", Offset = "0x13302C0", VA = "0x1013302C0")]
		private void UpdateEventDamageEffect()
		{
		}

		// Token: 0x0600109D RID: 4253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F63")]
		[Address(RVA = "0x101337E10", Offset = "0x1337E10", VA = "0x101337E10")]
		private void UpdateEventDamageEffect_Exec(SkillActionEvent_DamageEffect ev, CharacterHandler targetCharaHndl)
		{
		}

		// Token: 0x0600109E RID: 4254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F64")]
		[Address(RVA = "0x1013310A0", Offset = "0x13310A0", VA = "0x1013310A0")]
		private void UpdateEventEffectPlay()
		{
		}

		// Token: 0x0600109F RID: 4255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F65")]
		[Address(RVA = "0x101331684", Offset = "0x1331684", VA = "0x101331684")]
		private void UpdateEventEffectProjectile_Straight()
		{
		}

		// Token: 0x060010A0 RID: 4256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F66")]
		[Address(RVA = "0x101331D38", Offset = "0x1331D38", VA = "0x101331D38")]
		private void UpdateEventEffectProjectile_Parabola()
		{
		}

		// Token: 0x060010A1 RID: 4257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F67")]
		[Address(RVA = "0x101332410", Offset = "0x1332410", VA = "0x101332410")]
		private void UpdateEventEffectProjectile_Penetrate()
		{
		}

		// Token: 0x060010A2 RID: 4258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F68")]
		[Address(RVA = "0x101332AE0", Offset = "0x1332AE0", VA = "0x101332AE0")]
		private void UpdateEventEffectAttach()
		{
		}

		// Token: 0x060010A3 RID: 4259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F69")]
		[Address(RVA = "0x1013330E8", Offset = "0x13330E8", VA = "0x1013330E8")]
		private void UpdateEventEffectDetach()
		{
		}

		// Token: 0x060010A4 RID: 4260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6A")]
		[Address(RVA = "0x10133327C", Offset = "0x133327C", VA = "0x10133327C")]
		private void UpdateEventTrailAttach()
		{
		}

		// Token: 0x060010A5 RID: 4261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6B")]
		[Address(RVA = "0x101333760", Offset = "0x1333760", VA = "0x101333760")]
		private void UpdateEventTrailDetach()
		{
		}

		// Token: 0x060010A6 RID: 4262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6C")]
		[Address(RVA = "0x101330458", Offset = "0x1330458", VA = "0x101330458")]
		private void UpdateEventWeaponThrow_Parabola()
		{
		}

		// Token: 0x060010A7 RID: 4263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6D")]
		[Address(RVA = "0x101330B70", Offset = "0x1330B70", VA = "0x101330B70")]
		private void UpdateEventWeaponVisible()
		{
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6E")]
		[Address(RVA = "0x101330D7C", Offset = "0x1330D7C", VA = "0x101330D7C")]
		private void UpdateEventWeaponReset()
		{
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F6F")]
		[Address(RVA = "0x1013338F4", Offset = "0x13338F4", VA = "0x1013338F4")]
		private void UpdateEventPlaySE()
		{
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F70")]
		[Address(RVA = "0x101333AF0", Offset = "0x1333AF0", VA = "0x101333AF0")]
		private void UpdateEventPlayVoice()
		{
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F71")]
		[Address(RVA = "0x101333DA8", Offset = "0x1333DA8", VA = "0x101333DA8")]
		public void UpdateEffectSet()
		{
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F72")]
		[Address(RVA = "0x101338710", Offset = "0x1338710", VA = "0x101338710")]
		public void StackEffectSetDictionary(SkillActionEffectSet effectSet)
		{
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x000070F8 File Offset: 0x000052F8
		[Token(Token = "0x6000F73")]
		[Address(RVA = "0x101338840", Offset = "0x1338840", VA = "0x101338840")]
		public bool IsRemainStackEffectSet()
		{
			return default(bool);
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x00007110 File Offset: 0x00005310
		[Token(Token = "0x6000F74")]
		[Address(RVA = "0x1013388AC", Offset = "0x13388AC", VA = "0x1013388AC")]
		public bool IsPlayingEffect()
		{
			return default(bool);
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F75")]
		[Address(RVA = "0x101336FC4", Offset = "0x1336FC4", VA = "0x101336FC4")]
		private List<Vector3> GetEffectPos(eSkillActionEffectPosType posType, eSkillActionLocatorType locatorType, Vector3 offset, bool isLocalPos, List<CharacterHandler> out_targetCharaHndls)
		{
			return null;
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x00007128 File Offset: 0x00005328
		[Token(Token = "0x6000F76")]
		[Address(RVA = "0x101337E5C", Offset = "0x1337E5C", VA = "0x101337E5C")]
		private Vector3 GetEffectProjectileStartPos(eSkillActionEffectProjectileStartPosType posType, eSkillActionLocatorType locatorType, Vector3 offset)
		{
			return default(Vector3);
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F77")]
		[Address(RVA = "0x101338360", Offset = "0x1338360", VA = "0x101338360")]
		private Transform GetEffectAttachTo(eSkillActionEffectAttachTo attachTo, eSkillActionLocatorType locatorType)
		{
			return null;
		}

		// Token: 0x060010B2 RID: 4274 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F78")]
		[Address(RVA = "0x10133840C", Offset = "0x133840C", VA = "0x10133840C")]
		private Transform GetTrailAttachTo(eSkillActionTrailAttachTo attachTo, eSkillActionLocatorType locatorType)
		{
			return null;
		}

		// Token: 0x060010B3 RID: 4275 RVA: 0x00007140 File Offset: 0x00005340
		[Token(Token = "0x6000F79")]
		[Address(RVA = "0x1013384B0", Offset = "0x13384B0", VA = "0x1013384B0")]
		private Vector3 GetWeaponThrowPos(eSkillActionWeaponThrowPosType posType, eSkillActionLocatorType locatorType, Vector3 offset, CharacterHandler out_throwTargetcharaHndls)
		{
			return default(Vector3);
		}

		// Token: 0x060010B4 RID: 4276 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F7A")]
		[Address(RVA = "0x101338B28", Offset = "0x1338B28", VA = "0x101338B28")]
		private Transform GetPartyCenter(bool isTgt)
		{
			return null;
		}

		// Token: 0x060010B5 RID: 4277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F7B")]
		[Address(RVA = "0x101334344", Offset = "0x1334344", VA = "0x101334344")]
		private void DetachEffect(short uniqueID)
		{
		}

		// Token: 0x060010B6 RID: 4278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F7C")]
		[Address(RVA = "0x101334440", Offset = "0x1334440", VA = "0x101334440")]
		private void DetachTrail(short uniqueID)
		{
		}

		// Token: 0x060010B7 RID: 4279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F7D")]
		[Address(RVA = "0x101334510", Offset = "0x1334510", VA = "0x101334510")]
		private void OnArrivalProjectile(string callbackKey, Vector3 arrivalPos, CharacterHandler targetCharaHndl)
		{
		}

		// Token: 0x060010B8 RID: 4280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F7E")]
		[Address(RVA = "0x1013373A4", Offset = "0x13373A4", VA = "0x1013373A4")]
		private void PlayDamageEffect(SkillActionInfo_DamageEffect info, CharacterHandler targetCharaHndl, bool isCriticalHit, int registHit_Or_DefaultHit_Or_WeakHit)
		{
		}

		// Token: 0x060010B9 RID: 4281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F7F")]
		[Address(RVA = "0x101338CC0", Offset = "0x1338CC0", VA = "0x101338CC0")]
		public SkillActionPlayer()
		{
		}

		// Token: 0x04001384 RID: 4996
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E36")]
		private SkillActionPlan m_SAP;

		// Token: 0x04001385 RID: 4997
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E37")]
		private SkillActionGraphics m_SAG;

		// Token: 0x04001386 RID: 4998
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E38")]
		private CharacterHandler m_Owner;

		// Token: 0x04001387 RID: 4999
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E39")]
		private BattleCommandData m_Command;

		// Token: 0x04001388 RID: 5000
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E3A")]
		private List<CharacterHandler> m_TgtCharaList;

		// Token: 0x04001389 RID: 5001
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E3B")]
		private List<CharacterHandler> m_MyCharaList;

		// Token: 0x0400138A RID: 5002
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4000E3C")]
		private BattleSystem m_BattleSystem;

		// Token: 0x0400138B RID: 5003
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4000E3D")]
		private Transform m_PlayerPartyCenter;

		// Token: 0x0400138C RID: 5004
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4000E3E")]
		private Transform m_EnemyPartyCenter;

		// Token: 0x0400138D RID: 5005
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4000E3F")]
		private List<EffectHandler> m_ActiveEffectList;

		// Token: 0x0400138E RID: 5006
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4000E40")]
		private Dictionary<short, SkillActionPlayer.AttachedEffectData> m_AttachedEffectList;

		// Token: 0x0400138F RID: 5007
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4000E41")]
		private Dictionary<short, TrailHandler> m_AttachedTrailList;

		// Token: 0x04001390 RID: 5008
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4000E42")]
		private Dictionary<short, SkillActionProjectile> m_ProjectileEffectList;

		// Token: 0x04001391 RID: 5009
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4000E43")]
		private SkillActionPlayer.WeaponThrowData[] m_WeaponThrowDatas;

		// Token: 0x04001392 RID: 5010
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4000E44")]
		private Dictionary<CharacterHandler, List<SkillActionEffectSet>> m_StackEffectSets;

		// Token: 0x04001393 RID: 5011
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4000E45")]
		private Dictionary<CharacterHandler, SkillActionEffectSet> m_PlayingEffectSets;

		// Token: 0x04001394 RID: 5012
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4000E46")]
		private eElementType m_BaseElementType;

		// Token: 0x04001395 RID: 5013
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4000E47")]
		private bool m_IsOverRefAnimTime;

		// Token: 0x04001396 RID: 5014
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4000E48")]
		private float m_PlayingSec;

		// Token: 0x04001397 RID: 5015
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4000E49")]
		private int m_PlayingKeyFrame;

		// Token: 0x04001398 RID: 5016
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4000E4A")]
		private int m_PlayingKeyFrameOld;

		// Token: 0x04001399 RID: 5017
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4000E4B")]
		private int[] m_CurrentDataIndices;

		// Token: 0x0400139A RID: 5018
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4000E4C")]
		private Dictionary<string, bool> m_CallbackEnd;

		// Token: 0x0400139B RID: 5019
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4000E4D")]
		private bool m_IsPlaying;

		// Token: 0x02000449 RID: 1097
		[Token(Token = "0x2000DB5")]
		private class AttachedEffectData
		{
			// Token: 0x060010BA RID: 4282 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DFE")]
			[Address(RVA = "0x101338DA0", Offset = "0x1338DA0", VA = "0x101338DA0")]
			public void Clear()
			{
			}

			// Token: 0x060010BB RID: 4283 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005DFF")]
			[Address(RVA = "0x101338404", Offset = "0x1338404", VA = "0x101338404")]
			public AttachedEffectData()
			{
			}

			// Token: 0x0400139D RID: 5021
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058CF")]
			public EffectHandler m_EffectHndl;

			// Token: 0x0400139E RID: 5022
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058D0")]
			public float m_OriginalPosZ;

			// Token: 0x0400139F RID: 5023
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40058D1")]
			public bool m_IsDetached;
		}

		// Token: 0x0200044A RID: 1098
		[Token(Token = "0x2000DB6")]
		private class WeaponThrowData
		{
			// Token: 0x060010BC RID: 4284 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E00")]
			[Address(RVA = "0x10132E378", Offset = "0x132E378", VA = "0x10132E378")]
			public void Clear()
			{
			}

			// Token: 0x060010BD RID: 4285 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E01")]
			[Address(RVA = "0x10132ED48", Offset = "0x132ED48", VA = "0x10132ED48")]
			public WeaponThrowData()
			{
			}

			// Token: 0x040013A0 RID: 5024
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40058D2")]
			public SkillActionMovementParabola m_Movement;

			// Token: 0x040013A1 RID: 5025
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40058D3")]
			public string m_CallbackKey;

			// Token: 0x040013A2 RID: 5026
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40058D4")]
			public CharacterHandler m_ThrowTargetCharaHndl;

			// Token: 0x040013A3 RID: 5027
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40058D5")]
			public bool m_RotationLinkAnim;

			// Token: 0x040013A4 RID: 5028
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40058D6")]
			public Transform m_RotationTarget;

			// Token: 0x040013A5 RID: 5029
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40058D7")]
			public Transform m_RotationSource;

			// Token: 0x040013A6 RID: 5030
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40058D8")]
			public bool m_IsAutoDetachOnArrival;

			// Token: 0x040013A7 RID: 5031
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40058D9")]
			public List<short> m_AttachedEffectUniqueIDs;

			// Token: 0x040013A8 RID: 5032
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x40058DA")]
			public List<short> m_AttachedTrailUniqueIDs;
		}

		// Token: 0x0200044B RID: 1099
		[Token(Token = "0x2000DB7")]
		public struct RefAnimTime
		{
			// Token: 0x040013A9 RID: 5033
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40058DB")]
			public float m_PlayingSec;

			// Token: 0x040013AA RID: 5034
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40058DC")]
			public float m_MaxSec;

			// Token: 0x040013AB RID: 5035
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40058DD")]
			public WrapMode m_WrapMode;
		}

		// Token: 0x0200044C RID: 1100
		// (Invoke) Token: 0x060010BF RID: 4287
		[Token(Token = "0x2000DB8")]
		public delegate SkillActionPlayer.RefAnimTime GetRefAnimTimeDelegate();
	}
}
