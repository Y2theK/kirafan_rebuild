﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200044D RID: 1101
	[Token(Token = "0x2000368")]
	[StructLayout(3)]
	public class SkillActionProjectile
	{
		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060010C2 RID: 4290 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170000FA")]
		public SkillActionMovementBase Movement
		{
			[Token(Token = "0x6000F80")]
			[Address(RVA = "0x101335750", Offset = "0x1335750", VA = "0x101335750")]
			get
			{
				return null;
			}
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F81")]
		[Address(RVA = "0x101338054", Offset = "0x1338054", VA = "0x101338054")]
		public SkillActionProjectile(string callbackKey, string effectID, int sortingOrder, Transform parent, List<CharacterHandler> damageTargetCharaHndls)
		{
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F82")]
		[Address(RVA = "0x101338E58", Offset = "0x1338E58", VA = "0x101338E58")]
		private void CommonConstruct(string callbackKey, string effectID, int sortingOrder, Transform parent)
		{
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F83")]
		[Address(RVA = "0x10132E258", Offset = "0x132E258", VA = "0x10132E258")]
		public void Destroy()
		{
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F84")]
		[Address(RVA = "0x101334784", Offset = "0x1334784", VA = "0x101334784")]
		public void Update()
		{
		}

		// Token: 0x060010C7 RID: 4295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F85")]
		[Address(RVA = "0x1013380B8", Offset = "0x13380B8", VA = "0x1013380B8")]
		public void MoveStraight(bool isLocalPosition, Vector3 startPos, Vector3 targetPos, float speed)
		{
		}

		// Token: 0x060010C8 RID: 4296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F86")]
		[Address(RVA = "0x101338198", Offset = "0x1338198", VA = "0x101338198")]
		public void MoveParabola(bool isLocalPosition, Vector3 startPos, Vector3 targetPos, float arrivalSec, float gravityAccell)
		{
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F87")]
		[Address(RVA = "0x101338280", Offset = "0x1338280", VA = "0x101338280")]
		public void MovePenetrate(bool isLocalPosition, Vector3 startPos, List<Vector3> targetPosList, float speed, float delaySec, float aliveSec)
		{
		}

		// Token: 0x040013AC RID: 5036
		[Token(Token = "0x4000E4F")]
		private const float FADE_IN_TIME_ON_ARRIVAL = 0.15f;

		// Token: 0x040013AD RID: 5037
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4000E50")]
		private GameObject m_Obj;

		// Token: 0x040013AE RID: 5038
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4000E51")]
		private Transform m_Transform;

		// Token: 0x040013AF RID: 5039
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4000E52")]
		private EffectHandler m_EffectHndl;

		// Token: 0x040013B0 RID: 5040
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4000E53")]
		private SkillActionMovementBase m_Movement;

		// Token: 0x040013B1 RID: 5041
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4000E54")]
		private List<CharacterHandler> m_DamageTargetCharaHndls;

		// Token: 0x040013B2 RID: 5042
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4000E55")]
		private string m_CallbackKey;

		// Token: 0x040013B3 RID: 5043
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4000E56")]
		public Action<string, Vector3, CharacterHandler> OnArrival;
	}
}
