﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200044E RID: 1102
	[Token(Token = "0x2000369")]
	[StructLayout(3)]
	public static class SkillActionUtility
	{
		// Token: 0x060010CA RID: 4298 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F88")]
		[Address(RVA = "0x1013390D4", Offset = "0x13390D4", VA = "0x1013390D4")]
		public static string ConvertEffectID(string srcEffectID, int charaID, int viewCharaID, eElementType elementType, eClassType classType, int classAnimType, sbyte motionID, byte grade)
		{
			return null;
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F89")]
		[Address(RVA = "0x1013398D4", Offset = "0x13398D4", VA = "0x1013398D4")]
		public static string ConvertDmgEffectID(string srcEffectID, eElementType elementType, byte grade, eDmgEffectType dmgEffectType)
		{
			return null;
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F8A")]
		[Address(RVA = "0x1013396E8", Offset = "0x13396E8", VA = "0x1013396E8")]
		private static string ConvertEffectID_class(eClassType classType, int classAnimType)
		{
			return null;
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F8B")]
		[Address(RVA = "0x10133986C", Offset = "0x133986C", VA = "0x10133986C")]
		public static string ConvertEffectID_element(eElementType elementType)
		{
			return null;
		}

		// Token: 0x060010CE RID: 4302 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6000F8C")]
		[Address(RVA = "0x101339A98", Offset = "0x1339A98", VA = "0x101339A98")]
		public static string ConvertEffectID_dmgEnemy(eDmgEffectType dmgEnemyEffectType)
		{
			return null;
		}

		// Token: 0x060010CF RID: 4303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6000F8D")]
		[Address(RVA = "0x101339B00", Offset = "0x1339B00", VA = "0x101339B00")]
		public static void CorrectEffectParam(Camera camera, string effectID, ref Vector3 ref_pos, ref int ref_sortingOrder)
		{
		}
	}
}
