﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005A5 RID: 1445
	[Token(Token = "0x2000498")]
	[StructLayout(3)]
	public class SkillContentAndEffectCombiDB : ScriptableObject
	{
		// Token: 0x060015FA RID: 5626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AA")]
		[Address(RVA = "0x101339C94", Offset = "0x1339C94", VA = "0x101339C94")]
		public SkillContentAndEffectCombiDB()
		{
		}

		// Token: 0x04001AFC RID: 6908
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001466")]
		public SkillContentAndEffectCombiDB_Param[] m_Params;
	}
}
