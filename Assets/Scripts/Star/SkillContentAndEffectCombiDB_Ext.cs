﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200052A RID: 1322
	[Token(Token = "0x2000420")]
	[StructLayout(3)]
	public static class SkillContentAndEffectCombiDB_Ext
	{
		// Token: 0x0600158D RID: 5517 RVA: 0x00009678 File Offset: 0x00007878
		[Token(Token = "0x6001442")]
		[Address(RVA = "0x101339C9C", Offset = "0x1339C9C", VA = "0x101339C9C")]
		public static SkillContentAndEffectCombiDB_Param GetParam(this SkillContentAndEffectCombiDB self, eSkillContentAndEffectCombiDB type)
		{
			return default(SkillContentAndEffectCombiDB_Param);
		}
	}
}
