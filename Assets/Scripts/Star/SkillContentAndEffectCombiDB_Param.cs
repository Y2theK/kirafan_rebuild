﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005A4 RID: 1444
	[Token(Token = "0x2000497")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SkillContentAndEffectCombiDB_Param
	{
		// Token: 0x04001AFA RID: 6906
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001464")]
		public byte m_ID;

		// Token: 0x04001AFB RID: 6907
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001465")]
		public float[] m_Thresholds;
	}
}
