﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005A8 RID: 1448
	[Token(Token = "0x200049B")]
	[StructLayout(3)]
	public class SkillContentListDB : ScriptableObject
	{
		// Token: 0x060015FB RID: 5627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AB")]
		[Address(RVA = "0x101339D04", Offset = "0x1339D04", VA = "0x101339D04")]
		public SkillContentListDB()
		{
		}

		// Token: 0x04001B03 RID: 6915
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400146D")]
		public SkillContentListDB_Param[] m_Params;
	}
}
