﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005A6 RID: 1446
	[Token(Token = "0x2000499")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct SkillContentListDB_Data
	{
		// Token: 0x04001AFD RID: 6909
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001467")]
		public sbyte m_SkillLvCoef;

		// Token: 0x04001AFE RID: 6910
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001468")]
		public int m_Target;

		// Token: 0x04001AFF RID: 6911
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001469")]
		public int m_Type;

		// Token: 0x04001B00 RID: 6912
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400146A")]
		public float[] m_Args;
	}
}
