﻿using System.Collections.Generic;

namespace Star {
    public static class SkillContentListDB_Ext {
        public static SkillContentListDB_Param GetParam(this SkillContentListDB self, int skillID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == skillID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static SkillContentListDB_Data GetData(this SkillContentListDB self, int skillID, int dataIndex) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == skillID) {
                    return self.m_Params[i].m_Datas[dataIndex];
                }
            }
            return default;
        }

        public static List<int> GetRefIdsOnCardType(this SkillContentListDB self, int skillID) {
            List<int> list = new List<int>();
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == skillID) {
                    for (int j = 0; j < self.m_Params[i].m_Datas.Length; j++) {
                        if (self.m_Params[i].m_Datas[j].m_Type == 21) {
                            int item = (int)self.m_Params[i].m_Datas[j].m_Args[1];
                            list.Add(item);
                        }
                    }
                }
            }
            return list;
        }

        public static int GetRecastFromSkillLv(ref SkillListDB_Param param, int skillLv, int[] borderLvs) {
            if (borderLvs != null) {
                for (int i = borderLvs.Length - 1; i >= 0; i--) {
                    if (skillLv >= borderLvs[i]) {
                        return param.m_Recasts[i];
                    }
                }
            }
            return param.m_Recasts[0];
        }

        public static float GetLoadFactorFromSkillLv(ref SkillListDB_Param param, int skillLv, int[] borderLvs) {
            for (int i = borderLvs.Length - 1; i >= 0; i--) {
                if (skillLv >= borderLvs[i]) {
                    return param.m_LoadFactors[i];
                }
            }
            return param.m_LoadFactors[0];
        }
    }
}
