﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005A7 RID: 1447
	[Token(Token = "0x200049A")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SkillContentListDB_Param
	{
		// Token: 0x04001B01 RID: 6913
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400146B")]
		public int m_ID;

		// Token: 0x04001B02 RID: 6914
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400146C")]
		public SkillContentListDB_Data[] m_Datas;
	}
}
