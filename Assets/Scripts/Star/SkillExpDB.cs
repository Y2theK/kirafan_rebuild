﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005AD RID: 1453
	[Token(Token = "0x20004A0")]
	[StructLayout(3)]
	public class SkillExpDB : ScriptableObject
	{
		// Token: 0x060015FC RID: 5628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AC")]
		[Address(RVA = "0x10133A370", Offset = "0x133A370", VA = "0x10133A370")]
		public SkillExpDB()
		{
		}

		// Token: 0x04001B32 RID: 6962
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400149C")]
		public SkillExpDB_Param[] m_Params;
	}
}
