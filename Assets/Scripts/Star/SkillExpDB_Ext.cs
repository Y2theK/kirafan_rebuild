﻿namespace Star {
    public static class SkillExpDB_Ext {
        public static int GetNextExp(this SkillExpDB self, int currentLv, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx != -1) {
                int endIdx = idx;
                for (int i = idx; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_NextExp == 0U) {
                        endIdx = i;
                        break;
                    }
                    if (self.m_Params[i].m_ID != expTableID) {
                        endIdx = i - 1;
                        break;
                    }
                }
                int lvIdx = idx + currentLv - 1;
                if (lvIdx >= 0 && lvIdx <= endIdx) {
                    return (int)self.m_Params[lvIdx].m_NextExp;
                }
            }
            return 0;
        }

        public static long GetNeedRemainExp(this SkillExpDB self, int currentLv, int maxLv, long totalUseNum, sbyte expTableID) {
            long exp = 0L;
            if (maxLv > currentLv || maxLv == -1) {
                bool exist = false;
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_ID == expTableID) {
                        exist = true;
                        break;
                    }
                }
                if (exist) {
                    long[] exps = self.GetNextMaxExps(1, currentLv, expTableID);
                    if (exps != null) {
                        for (int i = 0; i < exps.Length; i++) {
                            exp += exps[i];
                        }
                        exp -= totalUseNum;
                        if (exp < 0L) {
                            exp = 0L;
                        }
                    }
                }
            }
            return exp;
        }

        public static long GetTotalNeedRemainExp(this SkillExpDB self, int limitLv, long totalUseNum, sbyte expTableID) {
            long exp = 0L;
            long[] exps = self.GetNextMaxExps(1, limitLv - 1, expTableID);
            for (int i = 0; i < exps.Length; i++) {
                exp += exps[i];
            }
            exp -= totalUseNum;
            if (exp < 0L) {
                exp = 0L;
            }
            return exp;
        }

        public static long[] GetNextMaxExps(this SkillExpDB self, int lv_a, int lv_b, sbyte expTableID) {
            int diff = lv_b - lv_a + 1;
            long[] exps = null;
            if (diff > 0) {
                exps = new long[diff];
                for (int i = 0; i < diff; i++) {
                    exps[i] = self.GetNextExp(lv_a + i, expTableID);
                }
            }
            return exps;
        }

        public static long GetNeedExp(this SkillExpDB self, int lv_a, int lv_b, sbyte expTableID) {
            long exp = 0L;
            long[] exps = self.GetNextMaxExps(lv_a, lv_b, expTableID);
            if (exps != null) {
                for (int i = 0; i < exps.Length - 1; i++) {
                    exp += exps[i];
                }
            }
            return exp;
        }

        public static int GetMaxLv(this SkillExpDB self, sbyte expTableID) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                for (int i = idx; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_NextExp == 0U && self.m_Params[i].m_ID == expTableID) {
                        return self.m_Params[i].m_Lv;
                    }
                }
            }
            return 1;
        }

        public static int GetSimulateLv(this SkillExpDB self, sbyte expTableID, long totalExp) {
            int idx = -1;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == expTableID) {
                    idx = i;
                    break;
                }
            }
            if (idx >= 0) {
                for (int i = idx; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_ID == expTableID) {
                        if (self.m_Params[i].m_NextExp <= totalExp && self.m_Params[i].m_NextExp != 0) {
                            totalExp -= self.m_Params[i].m_NextExp;
                        } else {
                            return self.m_Params[i].m_Lv;
                        }
                    }
                }
            }
            return 1;
        }
    }
}
