﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005AC RID: 1452
	[Token(Token = "0x200049F")]
	[Serializable]
	[StructLayout(0, Size = 12)]
	public struct SkillExpDB_Param
	{
		// Token: 0x04001B2F RID: 6959
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001499")]
		public int m_ID;

		// Token: 0x04001B30 RID: 6960
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x400149A")]
		public int m_Lv;

		// Token: 0x04001B31 RID: 6961
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400149B")]
		public uint m_NextExp;
	}
}
