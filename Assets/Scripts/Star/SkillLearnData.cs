﻿using System;

namespace Star {
    [Serializable]
    public class SkillLearnData {
        public int SkillID { get; set; }
        public int SkillLv { get; set; }
        public int SkillMaxLv { get; set; }
        public long TotalUseNum { get; set; }
        public sbyte ExpTableID { get; set; }
        public bool IsConfusionSkillID { get; set; }

        public SkillLearnData(int skillID, int skillLv, int skillMaxLv, long totalUseNum, sbyte expTableID, bool isConfusionSkillID = true) {
            SkillID = skillID;
            SkillLv = skillLv;
            SkillMaxLv = skillMaxLv;
            TotalUseNum = totalUseNum;
            ExpTableID = expTableID;
            IsConfusionSkillID = isConfusionSkillID;
        }
    }
}
