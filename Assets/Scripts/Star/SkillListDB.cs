﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005AF RID: 1455
	[Token(Token = "0x20004A2")]
	[StructLayout(3)]
	public class SkillListDB : ScriptableObject
	{
		// Token: 0x060015FD RID: 5629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AD")]
		[Address(RVA = "0x10133AD18", Offset = "0x133AD18", VA = "0x10133AD18")]
		public SkillListDB()
		{
		}

		// Token: 0x04001B40 RID: 6976
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40014AA")]
		public SkillListDB_Param[] m_Params;
	}
}
