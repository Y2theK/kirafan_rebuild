﻿namespace Star {
    public static class SkillListDB_Ext {
        public static SkillListDB_Param GetParam(this SkillListDB self, int skillID) {
            if (skillID != -1) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_ID == skillID) {
                        return self.m_Params[i];
                    }
                }
            }
            return default;
        }

        public static string GetSkillActionPlanID(this SkillListDB self, int skillID) {
            if (skillID != -1) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_ID == skillID) {
                        return self.m_Params[i].m_SAP;
                    }
                }
            }
            return null;
        }

        public static string GetSkillActionGraphicsID(this SkillListDB self, int skillID) {
            if (skillID != -1) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_ID == skillID) {
                        return self.m_Params[i].m_SAG;
                    }
                }
            }
            return null;
        }
    }
}
