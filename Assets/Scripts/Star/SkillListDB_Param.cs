﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005AE RID: 1454
	[Token(Token = "0x20004A1")]
	[Serializable]
	[StructLayout(0, Size = 104)]
	public struct SkillListDB_Param
	{
		// Token: 0x04001B33 RID: 6963
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400149D")]
		public int m_ID;

		// Token: 0x04001B34 RID: 6964
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400149E")]
		public string m_SkillName;

		// Token: 0x04001B35 RID: 6965
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400149F")]
		public string m_SkillDetail;

		// Token: 0x04001B36 RID: 6966
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40014A0")]
		public int m_SkillType;

		// Token: 0x04001B37 RID: 6967
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40014A1")]
		public string m_UniqueSkillScene;

		// Token: 0x04001B38 RID: 6968
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40014A2")]
		public string m_UniqueSkillVoiceCueSheet;

		// Token: 0x04001B39 RID: 6969
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40014A3")]
		public string m_UniqueSkillSeCueSheet;

		// Token: 0x04001B3A RID: 6970
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40014A4")]
		public string m_UniqueSkillBgmCueName;

		// Token: 0x04001B3B RID: 6971
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40014A5")]
		public string m_SAP;

		// Token: 0x04001B3C RID: 6972
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40014A6")]
		public string m_SAG;

		// Token: 0x04001B3D RID: 6973
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40014A7")]
		public int[] m_Recasts;

		// Token: 0x04001B3E RID: 6974
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40014A8")]
		public float[] m_LoadFactors;

		// Token: 0x04001B3F RID: 6975
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40014A9")]
		public byte m_IsCharaCutIn;
	}
}
