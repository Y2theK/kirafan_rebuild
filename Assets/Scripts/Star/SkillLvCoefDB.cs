﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B3 RID: 1459
	[Token(Token = "0x20004A6")]
	[StructLayout(3)]
	public class SkillLvCoefDB : ScriptableObject
	{
		// Token: 0x060015FE RID: 5630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AE")]
		[Address(RVA = "0x10133B03C", Offset = "0x133B03C", VA = "0x10133B03C")]
		public SkillLvCoefDB()
		{
		}

		// Token: 0x04001B51 RID: 6993
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40014BB")]
		public SkillLvCoefDB_Param[] m_Params;
	}
}
