﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005B2 RID: 1458
	[Token(Token = "0x20004A5")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SkillLvCoefDB_Param
	{
		// Token: 0x04001B4E RID: 6990
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40014B8")]
		public int m_LvRangeMin;

		// Token: 0x04001B4F RID: 6991
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x40014B9")]
		public int m_LvRangeMax;

		// Token: 0x04001B50 RID: 6992
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40014BA")]
		public float[] m_Coefs;
	}
}
