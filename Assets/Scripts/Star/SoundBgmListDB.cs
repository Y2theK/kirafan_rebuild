﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B6 RID: 1462
	[Token(Token = "0x20004A9")]
	[StructLayout(3)]
	public class SoundBgmListDB : ScriptableObject
	{
		// Token: 0x060015FF RID: 5631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014AF")]
		[Address(RVA = "0x10133B1CC", Offset = "0x133B1CC", VA = "0x10133B1CC")]
		public SoundBgmListDB()
		{
		}

		// Token: 0x04001B83 RID: 7043
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40014ED")]
		public SoundBgmListDB_Param[] m_Params;
	}
}
