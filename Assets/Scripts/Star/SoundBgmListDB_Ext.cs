﻿using System.Collections.Generic;

namespace Star {
    public static class SoundBgmListDB_Ext {
        public static SoundBgmListDB_Param GetParam(this SoundBgmListDB self, eSoundBgmListDB cueID, Dictionary<eSoundBgmListDB, int> indices) {
            if (indices != null && indices.TryGetValue(cueID, out int idx)) {
                return self.m_Params[idx];
            }
            return default;
        }

        public static bool FindCueSheet(this SoundBgmListDB self, string cueName, out SoundBgmListDB_Param out_param) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CueName == cueName) {
                    out_param = self.m_Params[i];
                    return true;
                }
            }
            out_param = default;
            return false;
        }
    }
}
