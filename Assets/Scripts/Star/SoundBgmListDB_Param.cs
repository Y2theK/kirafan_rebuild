﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005B5 RID: 1461
	[Token(Token = "0x20004A8")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct SoundBgmListDB_Param
	{
		// Token: 0x04001B80 RID: 7040
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40014EA")]
		public int m_ID;

		// Token: 0x04001B81 RID: 7041
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40014EB")]
		public string m_CueName;

		// Token: 0x04001B82 RID: 7042
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40014EC")]
		public int m_CueSheetID;
	}
}
