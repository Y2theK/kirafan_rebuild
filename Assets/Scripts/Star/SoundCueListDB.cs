﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B8 RID: 1464
	[Token(Token = "0x20004AB")]
	[StructLayout(3)]
	public class SoundCueListDB : ScriptableObject
	{
		// Token: 0x06001600 RID: 5632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B0")]
		[Address(RVA = "0x10133B3D8", Offset = "0x133B3D8", VA = "0x10133B3D8")]
		public SoundCueListDB()
		{
		}

		// Token: 0x04001B86 RID: 7046
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40014F0")]
		public SoundCueListDB_Param[] m_Params;
	}
}
