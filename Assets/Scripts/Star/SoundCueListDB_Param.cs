﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005B7 RID: 1463
	[Token(Token = "0x20004AA")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SoundCueListDB_Param
	{
		// Token: 0x04001B84 RID: 7044
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40014EE")]
		public string m_CueName;

		// Token: 0x04001B85 RID: 7045
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40014EF")]
		public string m_CueSheetName;
	}
}
