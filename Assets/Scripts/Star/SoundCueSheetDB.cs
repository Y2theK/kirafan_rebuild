﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005BB RID: 1467
	[Token(Token = "0x20004AE")]
	[StructLayout(3)]
	public class SoundCueSheetDB : ScriptableObject
	{
		// Token: 0x06001601 RID: 5633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B1")]
		[Address(RVA = "0x10133B524", Offset = "0x133B524", VA = "0x10133B524")]
		public SoundCueSheetDB()
		{
		}

		// Token: 0x04001BA7 RID: 7079
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001511")]
		public SoundCueSheetDB_Param[] m_Params;
	}
}
