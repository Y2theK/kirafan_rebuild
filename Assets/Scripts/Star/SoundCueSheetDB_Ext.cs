﻿namespace Star {
    public static class SoundCueSheetDB_Ext {
        public static SoundCueSheetDB_Param GetParam(this SoundCueSheetDB self, string cueSheet) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_CueSheet == cueSheet) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static SoundCueSheetDB_Param GetParam(this SoundCueSheetDB self, eSoundCueSheetDB cueSheetID) {
            return self.m_Params[(int)cueSheetID];
        }

        public static string GetQueSheet(this SoundCueSheetDB self, eSoundCueSheetDB cueSheetID) {
            return self.m_Params[(int)cueSheetID].m_CueSheet;
        }
    }
}
