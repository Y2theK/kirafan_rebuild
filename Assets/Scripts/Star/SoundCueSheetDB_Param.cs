﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005BA RID: 1466
	[Token(Token = "0x20004AD")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct SoundCueSheetDB_Param
	{
		// Token: 0x04001BA4 RID: 7076
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400150E")]
		public string m_CueSheet;

		// Token: 0x04001BA5 RID: 7077
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400150F")]
		public string m_ACB;

		// Token: 0x04001BA6 RID: 7078
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001510")]
		public string m_AWB;
	}
}
