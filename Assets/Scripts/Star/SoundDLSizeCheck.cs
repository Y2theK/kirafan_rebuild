﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AA2 RID: 2722
	[Token(Token = "0x2000785")]
	[StructLayout(3)]
	public class SoundDLSizeCheck : IEnumerator
	{
		// Token: 0x06002E9B RID: 11931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A98")]
		[Address(RVA = "0x10133B704", Offset = "0x133B704", VA = "0x10133B704")]
		public SoundDLSizeCheck()
		{
		}

		// Token: 0x06002E9C RID: 11932 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A99")]
		[Address(RVA = "0x10133B730", Offset = "0x133B730", VA = "0x10133B730")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x100139840", Offset = "0x139840")]
		public IEnumerator CheckAsync_First(GameDefine.eDLScene dlScene)
		{
			return null;
		}

		// Token: 0x06002E9D RID: 11933 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A9A")]
		[Address(RVA = "0x10133B7F4", Offset = "0x133B7F4", VA = "0x10133B7F4")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x1001398A4", Offset = "0x1398A4")]
		public IEnumerator CheckAsync_CueSheet(List<string> cueSheetList)
		{
			return null;
		}

		// Token: 0x06002E9E RID: 11934 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A9B")]
		[Address(RVA = "0x10133B8B8", Offset = "0x133B8B8", VA = "0x10133B8B8")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x100139908", Offset = "0x139908")]
		public IEnumerator CheckAsync_FileList(List<SoundDLSizeCheck.VersionSet> list)
		{
			return null;
		}

		// Token: 0x06002E9F RID: 11935 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002A9C")]
		[Address(RVA = "0x10133B97C", Offset = "0x133B97C", VA = "0x10133B97C")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x10013996C", Offset = "0x13996C")]
		private IEnumerator CheckAsync(List<SoundDLSizeCheck.VersionSet> list)
		{
			return null;
		}

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06002EA0 RID: 11936 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002E8")]
		public object Current
		{
			[Token(Token = "0x6002A9D")]
			[Address(RVA = "0x10133BA40", Offset = "0x133BA40", VA = "0x10133BA40", Slot = "5")]
			get
			{
				return null;
			}
		}

		// Token: 0x06002EA1 RID: 11937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002A9E")]
		[Address(RVA = "0x10133BA48", Offset = "0x133BA48", VA = "0x10133BA48", Slot = "6")]
		public void Reset()
		{
		}

		// Token: 0x06002EA2 RID: 11938 RVA: 0x00013D58 File Offset: 0x00011F58
		[Token(Token = "0x6002A9F")]
		[Address(RVA = "0x10133BA4C", Offset = "0x133BA4C", VA = "0x10133BA4C", Slot = "4")]
		public bool MoveNext()
		{
			return default(bool);
		}

		// Token: 0x06002EA3 RID: 11939 RVA: 0x00013D70 File Offset: 0x00011F70
		[Token(Token = "0x6002AA0")]
		[Address(RVA = "0x10133BA5C", Offset = "0x133BA5C", VA = "0x10133BA5C")]
		public bool IsDone()
		{
			return default(bool);
		}

		// Token: 0x06002EA4 RID: 11940 RVA: 0x00013D88 File Offset: 0x00011F88
		[Token(Token = "0x6002AA1")]
		[Address(RVA = "0x10133BA6C", Offset = "0x133BA6C", VA = "0x10133BA6C")]
		public bool IsError()
		{
			return default(bool);
		}

		// Token: 0x06002EA5 RID: 11941 RVA: 0x00013DA0 File Offset: 0x00011FA0
		[Token(Token = "0x6002AA2")]
		[Address(RVA = "0x10133BA74", Offset = "0x133BA74", VA = "0x10133BA74")]
		public long GetResultCode()
		{
			return 0L;
		}

		// Token: 0x06002EA6 RID: 11942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002AA3")]
		[Address(RVA = "0x10133BA7C", Offset = "0x133BA7C", VA = "0x10133BA7C")]
		public void Restart()
		{
		}

		// Token: 0x04003E7B RID: 15995
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002CF3")]
		private SoundDLSizeCheck.State m_State;

		// Token: 0x04003E7C RID: 15996
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002CF4")]
		private long m_ResultCode;

		// Token: 0x02000AA3 RID: 2723
		[Token(Token = "0x2000FEA")]
		public enum State
		{
			// Token: 0x04003E7E RID: 15998
			[Token(Token = "0x40064F5")]
			Wait,
			// Token: 0x04003E7F RID: 15999
			[Token(Token = "0x40064F6")]
			Checking,
			// Token: 0x04003E80 RID: 16000
			[Token(Token = "0x40064F7")]
			Done
		}

		// Token: 0x02000AA4 RID: 2724
		[Token(Token = "0x2000FEB")]
		public class VersionSet
		{
			// Token: 0x06002EA7 RID: 11943 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060BC")]
			[Address(RVA = "0x10133C428", Offset = "0x133C428", VA = "0x10133C428")]
			public VersionSet(string fn, uint v)
			{
			}

			// Token: 0x04003E81 RID: 16001
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40064F8")]
			public string fileName;

			// Token: 0x04003E82 RID: 16002
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40064F9")]
			public uint version;
		}
	}
}
