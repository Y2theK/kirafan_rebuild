﻿namespace Star {
    public static class SoundDefine {
        public const string ACF_FILE_NAME = "Star.acf";
        public const string EXT_ACF = ".acf";
        public const string EXT_ACB = ".acb";
        public const string EXT_AWB = ".awb";

        public static readonly string[,] BUILTIN_DATAS = new string[2, 3] {
            {
                "BGM_builtin",
                "BGM_builtin.acb",
                "BGM_builtin.awb"
            },
            {
                "System",
                "System.acb",
                string.Empty
            }
        };

        public const int BGM_DEFAULT_FADE_IN_MILI_SEC = 800;
        public const int BGM_DEFAULT_FADE_OUT_MILI_SEC = 800;
        public const int CATEGORY_NUM = 4;

        public static readonly string[] CATEGORY_NAMES = new string[CATEGORY_NUM] {
            "BGM",
            "SE",
            "Voice",
            "Movie"
        };

        public static readonly string CATEGORY_BGM = CATEGORY_NAMES[0];
        public static readonly string CATEGORY_SE = CATEGORY_NAMES[1];
        public static readonly string CATEGORY_VOICE = CATEGORY_NAMES[2];
        public const string CV_SELECTOR = "chara_version";
        public const string CV_DEFAULT_LABEL = "00_default";

        public enum eCategory {
            BGM,
            SE,
            VOICE,
            MOVIE,
            Num
        }

        public enum eLoadStatus {
            None,
            Loading,
            Loaded
        }

        public class Selector {
            public string m_Selector;
            public string m_Label;

            public Selector() { }

            public Selector(string selector, string label) {
                m_Selector = selector;
                m_Label = label;
            }

            public bool IsAvailable() {
                return !string.IsNullOrEmpty(m_Selector) && !string.IsNullOrEmpty(m_Label);
            }
        }
    }
}
