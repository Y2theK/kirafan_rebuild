﻿using System.Collections.Generic;

namespace Star {
    public class SoundFrameController {
        private const int DEFAULT_PREPARE_START_FRAME = 15;

        private int m_PrepareStartFrame;
        private SoundManager m_SoundManager;
        private string m_CueSheetName;
        private string m_AcbFile;
        private string m_AwbFile;
        private List<RequestParam> m_RequestParams = new List<RequestParam>();
        private eStatus m_Status = eStatus.None;
        private bool m_IsPreparing;

        public string CueSheetName => m_CueSheetName;

        public SoundFrameController(SoundManager soundManager, string cueSheetName, string acbFile, string awbFile) {
            m_SoundManager = soundManager;
            m_CueSheetName = cueSheetName;
            m_AcbFile = acbFile;
            m_AwbFile = awbFile;
        }

        public void Destroy(bool stopIfStatusIsNotRemoved = true, bool isUnloadCueSheet = true) {
            m_Status = eStatus.None;
            m_IsPreparing = false;
            for (int i = 0; i < m_RequestParams.Count; i++) {
                RequestParam param = m_RequestParams[i];
                if (param.m_Hndl != null) {
                    m_SoundManager.RemoveHndl(param.m_Hndl, stopIfStatusIsNotRemoved);
                    param.m_Hndl = null;
                }
            }
            m_RequestParams.Clear();
            if (isUnloadCueSheet) {
                m_SoundManager.UnloadCueSheet(m_CueSheetName);
            }
        }

        public void Update(int currentFrame) {
            if (m_SoundManager == null) { return; }
            UpdatePreparingCues();
            switch (m_Status) {
                case eStatus.Preparing_CueSheet:
                    SoundDefine.eLoadStatus loadStatus = m_SoundManager.GetCueSheetLoadStatus(m_CueSheetName);
                    if (loadStatus != SoundDefine.eLoadStatus.Loading) {
                        if (FirstPrepareCues() > 0) {
                            m_Status = eStatus.Preparing_FirstStream;
                        } else {
                            m_Status = eStatus.Stop;
                            m_IsPreparing = false;
                        }
                    }
                    break;
                case eStatus.Preparing_FirstStream:
                    bool preparing = false;
                    for (int i = 0; i < m_RequestParams.Count; i++) {
                        if (m_RequestParams[i].m_Status == eRequestParamStatus.Preparing) {
                            preparing = true;
                            break;
                        }
                    }
                    if (!preparing) {
                        m_Status = eStatus.Stop;
                        m_IsPreparing = false;
                    }
                    break;
                case eStatus.Play:
                    PrepareCues(currentFrame);
                    for (int i = 0; i < m_RequestParams.Count; i++) {
                        RequestParam param = m_RequestParams[i];
                        bool played = param.m_Status == eRequestParamStatus.Played;
                        if (played && param.m_LoopReset) {
                            if (!param.m_LoopResetCheck && currentFrame > param.m_PlayFrame) {
                                param.m_LoopResetCheck = true;
                            } else if (currentFrame <= param.m_PlayFrame) {
                                param.m_LoopResetCheck = false;
                                param.m_Status = eRequestParamStatus.CompletePrepare;
                            }
                        }
                        if (!played && currentFrame >= param.m_PlayFrame) {
                            if (param.m_Hndl != null) {
                                param.m_Hndl.Resume();
                            } else if (param.m_IsVoice) {
                                m_SoundManager.PlayVOICE(m_CueSheetName, param.m_CueName, null, null);
                            } else if (!string.IsNullOrEmpty(param.m_CueName)) {
                                m_SoundManager.PlaySE(m_CueSheetName, param.m_CueName);
                            } else {
                                m_SoundManager.PlaySE(param.m_CueID);
                            }
                            param.m_Status = eRequestParamStatus.Played;
                        }
                    }
                    break;
            }
        }

        public void AddRequestParam(int frame, string cueName, bool isVoice, bool loopReset) {
            m_RequestParams.Add(new RequestParam(frame, cueName, isVoice, loopReset));
        }

        public void AddRequestParam(int frame, eSoundSeListDB cueID, bool loopReset) {
            m_RequestParams.Add(new RequestParam(frame, cueID, loopReset));
        }

        public void Prepare(int prepareStartFrame = DEFAULT_PREPARE_START_FRAME) {
            m_PrepareStartFrame = prepareStartFrame;
            m_RequestParams.Sort(CompareRequestParam);
            m_IsPreparing = m_SoundManager.LoadCueSheet(m_CueSheetName, m_AcbFile, m_AwbFile);
            if (m_IsPreparing) {
                m_Status = eStatus.Preparing_CueSheet;
            } else {
                m_Status = eStatus.Error;
            }
        }

        public void PrepareQuick(int prepareStartFrame = DEFAULT_PREPARE_START_FRAME) {
            m_PrepareStartFrame = prepareStartFrame;
            m_RequestParams.Sort(CompareRequestParam);
            m_Status = eStatus.Stop;
            m_IsPreparing = false;
        }

        public bool IsCompletePrepare() {
            return !m_IsPreparing;
        }

        public bool Play() {
            if (IsCompletePrepare()) {
                return false;
            }
            m_Status = eStatus.Play;
            return true;
        }

        private static int CompareRequestParam(RequestParam A, RequestParam B) {
            return A.m_PlayFrame.CompareTo(B.m_PlayFrame);
        }

        private int FirstPrepareCues() {
            return PrepareCues(0);
        }

        private int PrepareCues(int currentFrame) {
            int preparing = 0;
            for (int i = 0; i < m_RequestParams.Count; i++) {
                RequestParam param = m_RequestParams[i];
                if (param.m_IsVoice) {
                    if (param.m_Status == eRequestParamStatus.None) {
                        if (currentFrame >= param.m_PlayFrame - m_PrepareStartFrame) {
                            if (PrepareCue(param)) {
                                preparing++;
                            }
                        }
                    }
                }
            }
            return preparing;
        }

        private bool PrepareCue(RequestParam reqParam) {
            if (!reqParam.m_IsVoice) { return false; }
            reqParam.m_Hndl = new SoundHandler();
            if (m_SoundManager.PrepareVOICE(m_CueSheetName, reqParam.m_CueName, reqParam.m_Hndl, null, null)) {
                reqParam.m_Status = eRequestParamStatus.Preparing;
                return true;
            }
            reqParam.m_Status = eRequestParamStatus.None;
            return false;
        }

        private void UpdatePreparingCues() {
            for (int i = 0; i < m_RequestParams.Count; i++) {
                RequestParam param = m_RequestParams[i];
                eRequestParamStatus status = param.m_Status;
                if (status == eRequestParamStatus.Preparing) {
                    if (param.m_Hndl != null && param.m_Hndl.IsCompletePrepare()) {
                        param.m_Status = eRequestParamStatus.CompletePrepare;
                    }
                }
            }
        }

        private enum eRequestParamStatus {
            None,
            Preparing,
            CompletePrepare,
            Played
        }

        private class RequestParam {
            public int m_PlayFrame;
            public string m_CueName;
            public eSoundSeListDB m_CueID = eSoundSeListDB.Max;
            public eRequestParamStatus m_Status;
            public bool m_LoopReset;
            public bool m_LoopResetCheck;
            public bool m_IsVoice;
            public SoundHandler m_Hndl;

            public RequestParam(int frame, string cueName, bool isVoice, bool loopReset) {
                m_PlayFrame = frame;
                m_CueName = cueName;
                m_IsVoice = isVoice;
                m_LoopReset = loopReset;
            }

            public RequestParam(int frame, eSoundSeListDB cueID, bool loopReset) {
                m_PlayFrame = frame;
                m_CueID = cueID;
                m_IsVoice = false;
                m_LoopReset = loopReset;
            }
        }

        private enum eStatus {
            None = -1,
            Preparing_CueSheet,
            Preparing_FirstStream,
            Stop,
            Play,
            Error
        }
    }
}
