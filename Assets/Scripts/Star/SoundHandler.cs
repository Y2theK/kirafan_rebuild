﻿namespace Star {
    public class SoundHandler {
        private SoundObject m_SoundObject;

        public void OnMngConstruct(SoundObject so) {
            if (so != null) {
                m_SoundObject = so;
                m_SoundObject.AddRef();
            }
        }

        public void OnMngDestruct(bool stopIfStatusIsNotRemoved) {
            if (m_SoundObject != null) {
                if (stopIfStatusIsNotRemoved && m_SoundObject.GetStatus() != SoundObject.eStatus.Removed) {
                    m_SoundObject.Stop();
                }
                m_SoundObject.RemoveRef();
                m_SoundObject = null;
            }
        }

        public bool IsAvailable() {
            return m_SoundObject != null;
        }

        public bool IsCompletePrepare() {
            return m_SoundObject != null && m_SoundObject.IsCompletePrepare();
        }

        public bool IsPaused() {
            return m_SoundObject != null && m_SoundObject.IsPaused();
        }

        public bool IsPrepare() {
            return m_SoundObject != null && m_SoundObject.GetStatus() == SoundObject.eStatus.Prepare;
        }

        public bool IsPlaying(bool isIgnorePaused = false) {
            return m_SoundObject != null && m_SoundObject.GetStatus() == SoundObject.eStatus.Playing
                && (isIgnorePaused || !m_SoundObject.IsPaused());
        }

        public bool Play() {
            if (m_SoundObject != null) {
                if (m_SoundObject.IsPrepared()) {
                    return m_SoundObject.Resume();
                }
                return m_SoundObject.Play();
            }
            return false;
        }

        public bool Stop() {
            return m_SoundObject != null && m_SoundObject.Stop();
        }

        public bool Suspend() {
            return m_SoundObject != null && m_SoundObject.Suspend();
        }

        // TODO: probably unintentional, might cause bugs
        // should return the result of Resume()
        public bool Resume() {
            if (m_SoundObject != null) {
                m_SoundObject.Resume();
            }
            return false;
        }

        public string GetCueSheet() {
            if (m_SoundObject != null) {
                return m_SoundObject.CueSeet;
            }
            return null;
        }

        public string GetCueName() {
            if (m_SoundObject != null) {
                return m_SoundObject.CueName;
            }
            return null;
        }
    }
}
