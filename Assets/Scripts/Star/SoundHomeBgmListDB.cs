﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005BD RID: 1469
	[Token(Token = "0x20004B0")]
	[StructLayout(3)]
	public class SoundHomeBgmListDB : ScriptableObject
	{
		// Token: 0x06001602 RID: 5634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B2")]
		[Address(RVA = "0x10133E820", Offset = "0x133E820", VA = "0x10133E820")]
		public SoundHomeBgmListDB()
		{
		}

		// Token: 0x04001BAF RID: 7087
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001519")]
		public SoundHomeBgmListDB_Param[] m_Params;
	}
}
