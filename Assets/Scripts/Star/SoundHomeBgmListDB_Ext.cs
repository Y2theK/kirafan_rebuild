﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000532 RID: 1330
	[Token(Token = "0x2000428")]
	[StructLayout(3)]
	public static class SoundHomeBgmListDB_Ext
	{
		// Token: 0x060015A4 RID: 5540 RVA: 0x00009828 File Offset: 0x00007A28
		[Token(Token = "0x6001459")]
		[Address(RVA = "0x10133E828", Offset = "0x133E828", VA = "0x10133E828")]
		public static eSoundBgmListDB GetCueID(this SoundHomeBgmListDB self, DateTime nowTime, ePlayBGMState state)
		{
			return eSoundBgmListDB.BGM_TITLE;
		}

		// Token: 0x060015A5 RID: 5541 RVA: 0x00009840 File Offset: 0x00007A40
		[Token(Token = "0x600145A")]
		[Address(RVA = "0x10133EA70", Offset = "0x133EA70", VA = "0x10133EA70")]
		private static bool HourCheck(SoundHomeBgmListDB_Param param, int hour)
		{
			return default(bool);
		}
	}
}
