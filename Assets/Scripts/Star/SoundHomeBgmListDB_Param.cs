﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005BC RID: 1468
	[Token(Token = "0x20004AF")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct SoundHomeBgmListDB_Param
	{
		// Token: 0x04001BA8 RID: 7080
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001512")]
		public int m_Category;

		// Token: 0x04001BA9 RID: 7081
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001513")]
		public string m_StartAt;

		// Token: 0x04001BAA RID: 7082
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001514")]
		public string m_EndAt;

		// Token: 0x04001BAB RID: 7083
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001515")]
		public int m_UseScene;

		// Token: 0x04001BAC RID: 7084
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001516")]
		public int m_StartHour;

		// Token: 0x04001BAD RID: 7085
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001517")]
		public int m_EndHour;

		// Token: 0x04001BAE RID: 7086
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001518")]
		public int m_CueID;
	}
}
