﻿using Meige;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Star {
    public sealed class SoundManager {
        private const int SOUND_OBJ_POOL_NUM = 32;

        private SoundCueSheetDB m_SoundCueSheetDB;
        private SoundBgmListDB m_SoundBgmListDB;
        private Dictionary<eSoundBgmListDB, int> m_SoundBgmIndices;
        private SoundSeListDB m_SoundSeListDB;
        private Dictionary<eSoundSeListDB, int> m_SoundSeIndices;
        private SoundVoiceListDB m_SoundVoiceListDB;
        private Dictionary<eSoundVoiceListDB, int> m_SoundVoiceIndices;
        private NamedListDB m_NamedListDB;
        private LocalSaveData m_LocalSaveData;

        private List<string> m_CueSheets = new List<string>();
        private List<SoundObject> m_InactiveSoundObjs = new List<SoundObject>();
        private List<SoundObject> m_ActiveSoundObjs = new List<SoundObject>();
        private SoundHandler m_BgmHandler = new SoundHandler();
        private Dictionary<string, int> m_VoiceCueSheetRefCounts = new Dictionary<string, int>();

        public void Update() {
            for (int i = m_ActiveSoundObjs.Count - 1; i >= 0; i--) {
                SoundObject obj = m_ActiveSoundObjs[i];
                obj.Update();
                if (obj.GetStatus() == SoundObject.eStatus.Removed && obj.Ref == 0) {
                    SinkSoundObject(obj);
                }
            }
        }

        public void Destroy() {
            m_LocalSaveData = null;
            m_NamedListDB = null;
            m_SoundCueSheetDB = null;
            m_SoundSeListDB = null;
            m_SoundSeIndices = null;
            m_SoundVoiceListDB = null;
            m_SoundVoiceIndices = null;
            m_SoundBgmListDB = null;
            m_SoundBgmIndices = null;
            for (int i = 0; i < m_InactiveSoundObjs.Count; i++) {
                if (m_InactiveSoundObjs[i] != null) {
                    m_InactiveSoundObjs[i].Destroy();
                    m_InactiveSoundObjs[i] = null;
                }
            }
            m_InactiveSoundObjs.Clear();
            for (int j = 0; j < m_ActiveSoundObjs.Count; j++) {
                if (m_ActiveSoundObjs[j] != null) {
                    m_ActiveSoundObjs[j].Destroy();
                    m_ActiveSoundObjs[j] = null;
                }
            }
            m_ActiveSoundObjs.Clear();
            m_VoiceCueSheetRefCounts.Clear();
        }

        public void Setup(SoundCueSheetDB soundCueSheetDB, SoundSeListDB soundSeListDB, SoundBgmListDB soundBgmListDB, SoundVoiceListDB soundVoiceListDB, NamedListDB namedListDB) {
            m_InactiveSoundObjs.Clear();
            for (int i = 0; i < SOUND_OBJ_POOL_NUM; i++) {
                m_InactiveSoundObjs.Add(new SoundObject());
            }
            SetDatabase(soundCueSheetDB, soundSeListDB, soundBgmListDB, soundVoiceListDB, namedListDB);
        }

        public void ApplySaveData(LocalSaveData localSaveData) {
            m_LocalSaveData = localSaveData;
            if (m_LocalSaveData != null) {
                _SetCategoryVolume(SoundDefine.eCategory.SE, m_LocalSaveData.SeVolume);
                _SetCategoryVolume(SoundDefine.eCategory.BGM, m_LocalSaveData.BgmVolume);
                _SetCategoryVolume(SoundDefine.eCategory.VOICE, m_LocalSaveData.VoiceVolume);
                _SetCategoryVolume(SoundDefine.eCategory.MOVIE, m_LocalSaveData.MovieVolume);
            }
        }

        public void RegisterInstalledAcf() {
            string text = PathUtility.Combine(CRIFileInstaller.InstallPath, SoundDefine.ACF_FILE_NAME);
            if (File.Exists(text)) {
                CriAtomEx.UnregisterAcf();
                CriAtomEx.RegisterAcf(null, text);
            }
            Debug.Log($"RegisterInstalledAcf() acfPath:{text}, isExists:{File.Exists(text)}");
        }

        public void SetDatabase(SoundCueSheetDB soundCueSheetDB, SoundSeListDB soundSeListDB, SoundBgmListDB soundBgmListDB, SoundVoiceListDB soundVoiceListDB, NamedListDB namedListDB) {
            m_SoundCueSheetDB = soundCueSheetDB;
            m_SoundBgmListDB = soundBgmListDB;
            m_SoundBgmIndices = new Dictionary<eSoundBgmListDB, int>();
            for (int i = 0; i < m_SoundBgmListDB.m_Params.Length; i++) {
                m_SoundBgmIndices.Add((eSoundBgmListDB)m_SoundBgmListDB.m_Params[i].m_ID, i);
            }
            m_SoundSeListDB = soundSeListDB;
            try {
                m_SoundSeIndices = new Dictionary<eSoundSeListDB, int>();
                for (int i = 0; i < m_SoundSeListDB.m_Params.Length; i++) {
                    m_SoundSeIndices.Add((eSoundSeListDB)m_SoundSeListDB.m_Params[i].m_ID, i);
                }
            } catch (Exception ex) {
                Debug.LogError(ex);
            }
            m_SoundVoiceListDB = soundVoiceListDB;
            m_SoundVoiceIndices = new Dictionary<eSoundVoiceListDB, int>();
            for (int i = 0; i < m_SoundVoiceListDB.m_Params.Length; i++) {
                m_SoundVoiceIndices.Add((eSoundVoiceListDB)m_SoundVoiceListDB.m_Params[i].m_ID, i);
            }
            m_NamedListDB = namedListDB;
        }

        public SoundCueSheetDB GetSoundCueSheetDB() {
            return m_SoundCueSheetDB;
        }

        public bool LoadCueSheet(string cueSheet, string acbFile, string awbFile) {
            if (string.IsNullOrEmpty(cueSheet) || m_CueSheets.Contains(cueSheet)) { return false; }

            if (string.IsNullOrEmpty(acbFile) && string.IsNullOrEmpty(awbFile)) {
                SoundCueSheetDB_Param param = m_SoundCueSheetDB.GetParam(cueSheet);
                acbFile = param.m_ACB;
                awbFile = param.m_AWB;
            }
            if (string.IsNullOrEmpty(acbFile)) { return false; }

            bool isBuiltin = false;
            for (int i = 0; i < SoundDefine.BUILTIN_DATAS.GetLength(0); i++) {
                if (cueSheet == SoundDefine.BUILTIN_DATAS[i, 0]) {
                    isBuiltin = true;
                    break;
                }
            }
            if (!isBuiltin) {
                acbFile = GenLoadPath(acbFile);
                if (!string.IsNullOrEmpty(awbFile)) {
                    awbFile = GenLoadPath(awbFile);
                }
            }
            CriAtomCueSheet criCueSheet = CriAtom.AddCueSheet(cueSheet, acbFile, awbFile);
            if (criCueSheet != null && criCueSheet.acb != null) {
                m_CueSheets.Add(cueSheet);
                return true;
            }
            CriAtom.RemoveCueSheet(cueSheet);
            return false;
        }

        public string GenLoadPath(string name) {
            if (GameSystem.Inst.CRIFileInstaller.IsStreamAssetsFromToLoadNeeds(name, out _)) {
                return GameSystem.Inst.CRIFileInstaller.GetStreamingRelativePath(name);
            }
            return PathUtility.Combine(CRIFileInstaller.InstallPath, name);
        }

        public bool LoadCueSheet(eSoundCueSheetDB cueSheetID) {
            if (m_SoundCueSheetDB == null) { return false; }

            SoundCueSheetDB_Param param = m_SoundCueSheetDB.GetParam(cueSheetID);
            return LoadCueSheet(param.m_CueSheet, param.m_ACB, param.m_AWB);
        }

        public bool LoadVoiceCueSheet(eCharaNamedType namedType) {
            if (m_NamedListDB == null || namedType == eCharaNamedType.None) { return false; }

            string voiceCueSheet = GetVoiceCueSheet(namedType);
            return LoadVoiceCueSheet(voiceCueSheet);
        }

        public bool LoadVoiceCueSheet(string cueSheet) {
            if (string.IsNullOrEmpty(cueSheet)) { return false; }

            if (m_VoiceCueSheetRefCounts.TryGetValue(cueSheet, out int refs)) {
                refs++;
                m_VoiceCueSheetRefCounts[cueSheet] = refs;
                return false;
            }

            m_VoiceCueSheetRefCounts.Add(cueSheet, 1);
            string acbFile = cueSheet + SoundDefine.EXT_ACB;
            string awbFile = cueSheet + SoundDefine.EXT_AWB;
            return LoadCueSheet(cueSheet, acbFile, awbFile);
        }

        public bool IsLoadingCueSheets() {
            return CriAtom.CueSheetsAreLoading;
        }

        public SoundDefine.eLoadStatus GetCueSheetLoadStatus(eSoundCueSheetDB cueSheetID) {
            return GetCueSheetLoadStatus(m_SoundCueSheetDB.GetQueSheet(cueSheetID));
        }

        public SoundDefine.eLoadStatus GetCueSheetLoadStatus(string cueSheet) {
            CriAtomCueSheet criCueSheet = CriAtom.GetCueSheet(cueSheet);
            if (criCueSheet == null) { return SoundDefine.eLoadStatus.None; }
            if (criCueSheet.IsLoading) { return SoundDefine.eLoadStatus.Loading; }
            return SoundDefine.eLoadStatus.Loaded;
        }

        public SoundDefine.eLoadStatus GetVoiceCueSheetLoadStatus(eCharaNamedType namedType) {
            return GetCueSheetLoadStatus(GetVoiceCueSheet(namedType));
        }

        public int GetWaveNum(string cueSheet, string cueName) {
            CriAtomExAcb acb = CriAtom.GetAcb(cueSheet);
            if (acb != null) {
                if (acb.GetCueInfo(cueName, out CriAtomEx.CueInfo info)) {
                    return SoundUtility.GetWaveNumByCueInfo(info);
                }
            }
            return 0;
        }

        public int GetWaveNum(eCharaNamedType namedType, string cueName) {
            if (m_NamedListDB != null && namedType != eCharaNamedType.None) {
                return GetWaveNum(GetVoiceCueSheet(namedType), cueName);
            }
            return 0;
        }

        public bool UnloadCueSheet(string cueSheet) {
            if (!string.IsNullOrEmpty(cueSheet) && m_CueSheets.Contains(cueSheet)) {
                CriAtom.RemoveCueSheet(cueSheet);
                m_CueSheets.Remove(cueSheet);
                return true;
            }
            return false;
        }

        public bool UnloadCueSheet(eSoundCueSheetDB cueSheetID) {
            return m_SoundCueSheetDB != null && UnloadCueSheet(m_SoundCueSheetDB.GetQueSheet(cueSheetID));
        }

        public bool UnloadVoiceCueSheet(eCharaNamedType namedType) {
            if (m_NamedListDB != null && namedType != eCharaNamedType.None) {
                string voiceCueSheet = GetVoiceCueSheet(namedType);
                return UnloadVoiceCueSheet(voiceCueSheet);
            }
            return false;
        }

        public bool UnloadVoiceCueSheet(string cueSheet) {
            if (m_VoiceCueSheetRefCounts.TryGetValue(cueSheet, out int refs)) {
                refs--;
                m_VoiceCueSheetRefCounts[cueSheet] = refs;
            }
            if (refs <= 0) {
                m_VoiceCueSheetRefCounts.Remove(cueSheet);
                return UnloadCueSheet(cueSheet);
            }
            return false;
        }

        public void ForceResetOnReturnTitle() {
            for (int i = m_ActiveSoundObjs.Count - 1; i >= 0; i--) {
                SoundObject soundObject = m_ActiveSoundObjs[i];
                soundObject.Stop();
                SinkSoundObject(soundObject);
            }
            m_ActiveSoundObjs.Clear();
            for (int j = m_CueSheets.Count - 1; j >= 0; j--) {
                UnloadCueSheet(m_CueSheets[j]);
            }
            m_VoiceCueSheetRefCounts.Clear();
        }

        private SoundObject ScoopSoundObject() {
            if (m_InactiveSoundObjs.Count > 0) {
                SoundObject soundObject = m_InactiveSoundObjs[0];
                m_ActiveSoundObjs.Add(soundObject);
                m_InactiveSoundObjs.Remove(soundObject);
                return soundObject;
            }
            return null;
        }

        private void SinkSoundObject(SoundObject so) {
            if (so != null && m_ActiveSoundObjs.Remove(so)) {
                m_InactiveSoundObjs.Add(so);
            }
        }

        public void RemoveHndl(SoundHandler hndl, bool stopIfStatusIsNotRemoved = true) {
            if (hndl != null) {
                hndl.OnMngDestruct(stopIfStatusIsNotRemoved);
            }
        }

        private bool RequestPrepare(string cueSheet, string cueName, SoundHandler hndl, string selector, string label, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec) {
            if (!string.IsNullOrEmpty(cueSheet) && !string.IsNullOrEmpty(cueName) && hndl != null) {
                SoundObject soundObject = ScoopSoundObject();
                if (soundObject == null) { return false; }

                if (soundObject.Prepare(cueSheet, cueName, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec)) {
                    hndl.OnMngConstruct(soundObject);
                    return true;
                } else {
                    SinkSoundObject(soundObject);
                }
            }
            return false;
        }

        public bool RequestPlay(string cueSheet, string cueName, SoundHandler hndl, string selector, string label, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec) {
            if (!string.IsNullOrEmpty(cueSheet) && !string.IsNullOrEmpty(cueName)) {
                SoundObject soundObject = ScoopSoundObject();
                if (soundObject == null) { return false; }

                if (soundObject.Play(cueSheet, cueName, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec)) {
                    if (hndl != null) {
                        hndl.OnMngConstruct(soundObject);
                    }
                    return true;
                } else {
                    SinkSoundObject(soundObject);
                }
            }
            return false;
        }

        public bool PrepareBGM(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_BgmHandler != null) {
                RemoveHndl(m_BgmHandler, false);
            }
            return RequestPrepare(cueSheet, cueName, m_BgmHandler, null, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PrepareBGM(eSoundBgmListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundBgmIndices == null) { return false; }

            SoundBgmListDB_Param bgmParam = m_SoundBgmListDB.GetParam(cueID, m_SoundBgmIndices);
            SoundCueSheetDB_Param cueParam = m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)bgmParam.m_CueSheetID);
            return PrepareBGM(cueParam.m_CueSheet, bgmParam.m_CueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayBGM(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_BgmHandler != null) {
                RemoveHndl(m_BgmHandler, false);
            }
            return RequestPlay(cueSheet, cueName, m_BgmHandler, null, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayBGM(eSoundBgmListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundBgmIndices == null) { return false; }

            SoundBgmListDB_Param bgmParam = m_SoundBgmListDB.GetParam(cueID, m_SoundBgmIndices);
            SoundCueSheetDB_Param cueParam = m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)bgmParam.m_CueSheetID);
            return PlayBGM(cueParam.m_CueSheet, bgmParam.m_CueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool IsCompletePrepareBGM() {
            return m_BgmHandler != null && m_BgmHandler.IsCompletePrepare();
        }

        public bool StopBGM() {
            return m_BgmHandler != null && m_BgmHandler.Stop();
        }

        public bool SuspendBGM() {
            return m_BgmHandler != null && m_BgmHandler.Suspend();
        }

        public bool ResumeBGM() {
            return m_BgmHandler != null && m_BgmHandler.Resume();
        }

        public bool IsPlayingBGM(eSoundBgmListDB cueID) {
            if (m_SoundBgmIndices == null) { return false; }

            SoundBgmListDB_Param bgmParam = m_SoundBgmListDB.GetParam(cueID, m_SoundBgmIndices);
            SoundCueSheetDB_Param cueParam = m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)bgmParam.m_CueSheetID);
            return IsPlayingBGM(cueParam.m_CueSheet, bgmParam.m_CueName);
        }

        public bool IsPlayingBGM(string cueSheet, string cueName) {
            return m_BgmHandler != null && m_BgmHandler.GetCueSheet() == cueSheet && m_BgmHandler.GetCueName() == cueName && m_BgmHandler.IsPlaying();
        }

        public string GetBGMCueSheet() {
            if (m_BgmHandler != null) {
                return m_BgmHandler.GetCueSheet();
            }
            return null;
        }

        public string GetBGMCueName() {
            if (m_BgmHandler != null) {
                return m_BgmHandler.GetCueName();
            }
            return null;
        }

        public bool PrepareSE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return RequestPrepare(cueSheet, cueName, hndl, null, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PrepareSE(eSoundSeListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundSeIndices == null) { return false; }

            SoundSeListDB_Param seParam = m_SoundSeListDB.GetParam(cueID, m_SoundSeIndices);
            SoundCueSheetDB_Param cueParam = m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)seParam.m_CueSheetID);
            return PrepareSE(cueParam.m_CueSheet, seParam.m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlaySE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return RequestPlay(cueSheet, cueName, hndl, null, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlaySE(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return PlaySE(cueSheet, cueName, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlaySE(eSoundSeListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundSeIndices == null) { return false; }

            SoundSeListDB_Param seParam = m_SoundSeListDB.GetParam(cueID, m_SoundSeIndices);
            SoundCueSheetDB_Param cueParam = m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)seParam.m_CueSheetID);
            return PlaySE(cueParam.m_CueSheet, seParam.m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlaySE(eSoundSeListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return PlaySE(cueID, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PrepareVOICE(string cueSheet, string cueName, SoundHandler hndl, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return RequestPrepare(cueSheet, cueName, hndl, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PrepareVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, SoundHandler hndl, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundVoiceIndices == null || namedType == eCharaNamedType.None) { return false; }

            SoundVoiceListDB_Param param = m_SoundVoiceListDB.GetParam(cueID, m_SoundVoiceIndices);
            return PrepareVOICE(GetVoiceCueSheet(namedType), param.m_CueName, hndl, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayVOICE(string cueSheet, string cueName, SoundHandler hndl, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return RequestPlay(cueSheet, cueName, hndl, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayVOICE(string cueSheet, string cueName, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return PlayVOICE(cueSheet, cueName, null, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, SoundHandler hndl, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            if (m_SoundVoiceIndices == null || namedType == eCharaNamedType.None) { return false; }

            SoundVoiceListDB_Param param = m_SoundVoiceListDB.GetParam(cueID, m_SoundVoiceIndices);
            return PlayVOICE(GetVoiceCueSheet(namedType), param.m_CueName, hndl, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public bool PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, string selector, string label, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1) {
            return PlayVOICE(namedType, cueID, null, selector, label, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
        }

        public string GetVoiceCueSheet(eCharaNamedType namedType) {
            if (namedType != eCharaNamedType.None) {
                return "Voice_" + m_NamedListDB.GetParam(namedType).m_ResouceBaseName;
            }
            return null;
        }

        public void SetCategoryVolume(SoundDefine.eCategory category, float volume, bool isIgnoreSave = false) {
            volume = _SetCategoryVolume(category, volume);
            if (!isIgnoreSave && m_LocalSaveData != null) {
                switch (category) {
                    case SoundDefine.eCategory.BGM:
                        m_LocalSaveData.BgmVolume = volume;
                        break;
                    case SoundDefine.eCategory.SE:
                        m_LocalSaveData.SeVolume = volume;
                        break;
                    case SoundDefine.eCategory.VOICE:
                        m_LocalSaveData.VoiceVolume = volume;
                        break;
                    case SoundDefine.eCategory.MOVIE:
                        m_LocalSaveData.MovieVolume = volume;
                        break;
                }
            }
        }

        private float _SetCategoryVolume(SoundDefine.eCategory category, float volume) {
            volume = Mathf.Clamp01(volume);
            CriAtom.SetCategoryVolume(SoundDefine.CATEGORY_NAMES[(int)category], volume);
            return volume;
        }

        public float GetCategoryVolume(SoundDefine.eCategory category) {
            return CriAtom.GetCategoryVolume(SoundDefine.CATEGORY_NAMES[(int)category]);
        }
    }
}
