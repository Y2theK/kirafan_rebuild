﻿namespace Star {
    public class SoundObject {
        private CriAtomExPlayer m_Player;
        private CriAtomExPlayback m_Playback;

        private bool m_NeedToPlayerUpdateAll;
        private string m_CueSheet;
        private string m_CueName;
        private string m_Selector;
        private string m_Label;
        private bool m_IsPrepared;
        private float m_Volume = 1f;
        private int m_StartTimeMiliSec;
        private int m_FadeInMiliSec = -1;
        private int m_FadeOutMiliSec = -1;

        public string CueSeet => m_CueSheet;
        public string CueName => m_CueName;

        public byte Ref { get; private set; }

        public SoundObject() {
            m_Player = new CriAtomExPlayer();
        }

        public void AddRef() {
            Ref = 1;
        }

        public void RemoveRef() {
            Ref = 0;
        }

        public void Destroy() {
            if (m_Player != null) {
                m_Player.Dispose();
                m_Player = null;
            }
        }

        public void Update() {
            if (m_Player != null && m_NeedToPlayerUpdateAll) {
                m_Player.Update(m_Playback);
                m_NeedToPlayerUpdateAll = false;
            }
        }

        public eStatus GetStatus() {
            if (m_Player != null) {
                CriAtomExPlayback.Status status = m_Playback.GetStatus();
                if (status == CriAtomExPlayback.Status.Prep) {
                    return eStatus.Prepare;
                } else if (status == CriAtomExPlayback.Status.Playing) {
                    return eStatus.Playing;
                } else if (status == CriAtomExPlayback.Status.Removed) {
                    return eStatus.Removed;
                }
            }
            return eStatus.None;
        }

        public bool IsCompletePrepare() {
            return m_Player != null && m_IsPrepared && GetStatus() == eStatus.Playing;
        }

        public bool IsPaused() {
            return m_Player != null && m_Playback.IsPaused();
        }

        public bool IsPrepared() {
            return m_IsPrepared;
        }

        public bool Play(string cueSheet, string cueName, string selector, string label, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec) {
            if (m_Player == null) { return false; }

            m_CueSheet = cueSheet;
            m_CueName = cueName;
            m_Selector = selector;
            m_Label = label;
            m_Volume = volume;
            m_StartTimeMiliSec = startTimeMiliSec;
            m_FadeInMiliSec = fadeInMiliSec;
            m_FadeOutMiliSec = fadeOutMiliSec;
            m_Player.ResetParameters();
            m_Player.SetVolume(volume);
            m_Player.SetStartTime(startTimeMiliSec);
            if (!string.IsNullOrEmpty(selector) && !string.IsNullOrEmpty(label)) {
                m_Player.SetSelectorLabel(selector, label);
            }
            if (fadeInMiliSec >= 0) {
                m_Player.SetEnvelopeAttackTime(fadeInMiliSec);
            }
            if (fadeOutMiliSec >= 0) {
                m_Player.SetEnvelopeReleaseTime(fadeOutMiliSec);
            }
            m_Player.SetCue(CriAtom.GetAcb(m_CueSheet), m_CueName);
            m_Playback = m_Player.Start();
            m_Player.Update(m_Playback);
            m_IsPrepared = false;
            return true;
        }

        public bool Play() {
            return Play(m_CueSheet, m_CueName, m_Selector, m_Label, m_Volume, m_StartTimeMiliSec, m_FadeInMiliSec, m_FadeOutMiliSec);
        }

        public bool Prepare(string cueSheet, string cueName, string selector, string label, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec) {
            if (m_Player == null) { return false; }

            m_CueSheet = cueSheet;
            m_CueName = cueName;
            m_Selector = selector;
            m_Label = label;
            m_Volume = volume;
            m_StartTimeMiliSec = startTimeMiliSec;
            m_FadeInMiliSec = fadeInMiliSec;
            m_FadeOutMiliSec = fadeOutMiliSec;
            m_Player.ResetParameters();
            m_Player.SetVolume(volume);
            m_Player.SetStartTime(startTimeMiliSec);
            if (!string.IsNullOrEmpty(selector) && !string.IsNullOrEmpty(label)) {
                m_Player.SetSelectorLabel(selector, label);
            }
            if (fadeInMiliSec >= 0) {
                m_Player.SetEnvelopeAttackTime(fadeInMiliSec);
            }
            if (fadeOutMiliSec >= 0) {
                m_Player.SetEnvelopeReleaseTime(fadeOutMiliSec);
            }
            m_Player.SetCue(CriAtom.GetAcb(m_CueSheet), m_CueName);
            m_Playback = m_Player.Prepare();
            m_Player.Update(m_Playback);
            m_IsPrepared = true;
            return true;
        }

        public bool Stop() {
            if (m_Player != null) {
                m_Playback.Stop();
                return true;
            }
            return false;
        }

        public bool Suspend() {
            if (m_Player != null) {
                m_Playback.Pause();
                return true;
            }
            return false;
        }

        public bool Resume() {
            if (m_Player != null) {
                if (m_IsPrepared) {
                    m_Playback.Resume(CriAtomEx.ResumeMode.PreparedPlayback);
                    m_IsPrepared = false;
                } else {
                    m_Playback.Resume(CriAtomEx.ResumeMode.PausedPlayback);
                }
                return true;
            }
            return false;
        }

        public enum eStatus {
            None = -1,
            Prepare = 1,
            Playing,
            Removed
        }
    }
}
