﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005C2 RID: 1474
	[Token(Token = "0x20004B5")]
	[StructLayout(3)]
	public class SoundSeListDB : ScriptableObject
	{
		// Token: 0x06001603 RID: 5635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B3")]
		[Address(RVA = "0x101341524", Offset = "0x1341524", VA = "0x101341524")]
		public SoundSeListDB()
		{
		}

		// Token: 0x04001C89 RID: 7305
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40015F3")]
		public SoundSeListDB_Param[] m_Params;
	}
}
