﻿using System.Collections.Generic;

namespace Star {
    public static class SoundSeListDB_Ext {
        public static SoundSeListDB_Param GetParam(this SoundSeListDB self, eSoundSeListDB cueID, Dictionary<eSoundSeListDB, int> indices) {
            if (indices != null && indices.TryGetValue(cueID, out int idx)) {
                return self.m_Params[idx];
            }
            return default;
        }
    }
}
