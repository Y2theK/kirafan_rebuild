﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005C1 RID: 1473
	[Token(Token = "0x20004B4")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct SoundSeListDB_Param
	{
		// Token: 0x04001C86 RID: 7302
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40015F0")]
		public int m_ID;

		// Token: 0x04001C87 RID: 7303
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x40015F1")]
		public string m_CueName;

		// Token: 0x04001C88 RID: 7304
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40015F2")]
		public int m_CueSheetID;
	}
}
