﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000AB1 RID: 2737
	[Token(Token = "0x200078A")]
	[StructLayout(3)]
	public class SoundSeRequest
	{
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06002F2B RID: 12075 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170002ED")]
		private SoundManager SoundMng
		{
			[Token(Token = "0x6002B0D")]
			[Address(RVA = "0x10134152C", Offset = "0x134152C", VA = "0x10134152C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06002F2C RID: 12076 RVA: 0x00014448 File Offset: 0x00012648
		[Token(Token = "0x170002EE")]
		public SoundSeRequest.eStatus Status
		{
			[Token(Token = "0x6002B0E")]
			[Address(RVA = "0x1013415A0", Offset = "0x13415A0", VA = "0x1013415A0")]
			get
			{
				return SoundSeRequest.eStatus.None;
			}
		}

		// Token: 0x06002F2D RID: 12077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B0F")]
		[Address(RVA = "0x1013415A8", Offset = "0x13415A8", VA = "0x1013415A8")]
		public SoundSeRequest(eSoundSeListDB cueID, float delay)
		{
		}

		// Token: 0x06002F2E RID: 12078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B10")]
		[Address(RVA = "0x1013415E4", Offset = "0x13415E4", VA = "0x1013415E4")]
		public void Destroy()
		{
		}

		// Token: 0x06002F2F RID: 12079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B11")]
		[Address(RVA = "0x1013415E8", Offset = "0x13415E8", VA = "0x1013415E8")]
		public void Stop()
		{
		}

		// Token: 0x06002F30 RID: 12080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B12")]
		[Address(RVA = "0x101341634", Offset = "0x1341634", VA = "0x101341634")]
		public void Play(float overrideDelay = -1f)
		{
		}

		// Token: 0x06002F31 RID: 12081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B13")]
		[Address(RVA = "0x101341738", Offset = "0x1341738", VA = "0x101341738")]
		public void Update()
		{
		}

		// Token: 0x06002F32 RID: 12082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002B14")]
		[Address(RVA = "0x101341658", Offset = "0x1341658", VA = "0x101341658")]
		private void PlaySE()
		{
		}

		// Token: 0x04003ED2 RID: 16082
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002D1B")]
		private eSoundSeListDB m_CueID;

		// Token: 0x04003ED3 RID: 16083
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002D1C")]
		private float m_Delay;

		// Token: 0x04003ED4 RID: 16084
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002D1D")]
		private float m_Timer;

		// Token: 0x04003ED5 RID: 16085
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002D1E")]
		private SoundSeRequest.eStatus m_Status;

		// Token: 0x04003ED6 RID: 16086
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002D1F")]
		private SoundHandler m_SoundHndl;

		// Token: 0x04003ED7 RID: 16087
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002D20")]
		private SoundManager m_SoundManager;

		// Token: 0x02000AB2 RID: 2738
		[Token(Token = "0x2000FF4")]
		public enum eStatus
		{
			// Token: 0x04003ED9 RID: 16089
			[Token(Token = "0x4006524")]
			None,
			// Token: 0x04003EDA RID: 16090
			[Token(Token = "0x4006525")]
			Requested,
			// Token: 0x04003EDB RID: 16091
			[Token(Token = "0x4006526")]
			Played
		}
	}
}
