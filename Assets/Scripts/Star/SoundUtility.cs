﻿using System.Collections.Generic;

namespace Star {
    public static class SoundUtility {
        public static SoundDefine.Selector GetCharaSelector(int charaID) {
            CharacterListDB_Param? param = GameSystem.Inst.DbMng.CharaListDB.GetParamNullable(charaID);
            if (param.HasValue && !string.IsNullOrEmpty(param.Value.m_CRILabel)) {
                return new SoundDefine.Selector(SoundDefine.CV_SELECTOR, param.Value.m_CRILabel);
            }
            return new SoundDefine.Selector();
        }

        public static List<SoundDefine.Selector> GetCharaSelectorsOtherThanDefault(eCharaNamedType namedType) {
            HashSet<string> entries = new HashSet<string>();
            List<CharacterListDB_Param> charaParams = GameSystem.Inst.DbMng.CharaListDB.GetParamsByNamedType(namedType);
            for (int i = 0; i < charaParams.Count; i++) {
                CharacterListDB_Param param = charaParams[i];
                if (!string.IsNullOrEmpty(param.m_CRILabel) && param.m_CRILabel != SoundDefine.CV_DEFAULT_LABEL) {
                    entries.Add(param.m_CRILabel);
                }
            }

            List<SoundDefine.Selector> selectors = new List<SoundDefine.Selector>();
            foreach (string label in entries) {
                selectors.Add(new SoundDefine.Selector(SoundDefine.CV_SELECTOR, label));
            }
            return selectors;
        }

        public static int GetWaveNumByCueInfo(CriAtomEx.CueInfo cueInfo) {
            //TODO: wrong CriAtom plugin version
            //if (cueInfo.numRelatedWaveForms != 0) {
            //    return cueInfo.numRelatedWaveForms;
            //}
            return cueInfo.numTracks;
        }
    }
}
