﻿using CommonLogic;
using Meige;
using Meige.AssetBundles;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using ABUtility = Meige.AssetBundles.Utility;

namespace Star {
    public class SoundVersionDictionary {
        private readonly string VersionFileName = "cv.b";
        private const int MaxRetryCount = 5;

        private Dictionary<string, FileInformation> m_Dictionary;
        private Dictionary<string, FileInformation> m_DictionaryForBuiltin;
        private uint m_ErrorCount;
        private bool m_isError;
        private bool m_isDone;

        public SoundVersionDictionary() {
            m_ErrorCount = 0;
            m_isError = false;
            m_isDone = false;
        }

        public FileInformation Search(string name, bool isBuiltin = false) {
            if (isBuiltin) {
                if (m_DictionaryForBuiltin != null && m_DictionaryForBuiltin.TryGetValue(name, out FileInformation info)) {
                    return info;
                }
            } else {
                if (m_Dictionary != null && m_Dictionary.TryGetValue(name, out FileInformation info)) {
                    return info;
                }
            }
            return null;
        }

        public void Clear() {
            m_Dictionary = null;
            m_isError = false;
            m_isDone = false;
            m_ErrorCount = 0;
        }

        private XorShift CreateXorShift() {
            XorShift shift = new XorShift();
            shift.SetS(0x20AB3F93, 0xDD6C6, 0x141E41E, 0xF4320BA5);
            return shift;
        }

        private List<FileInformation> Decode(byte[] encodingSerializedBytes) {
            byte[] bytes = new byte[encodingSerializedBytes.Length];
            XorShift shift = CreateXorShift();
            for (int i = 0; i < encodingSerializedBytes.Length; i++) {
                bytes[i] = (byte)(encodingSerializedBytes[i] - (shift.Get() & 0x7F));
            }
            string str = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject<List<FileInformation>>(str);
        }

        public IEnumerator CreateVersionDictionary() {
            CRIFileInstaller installer = GameSystem.Inst.CRIFileInstaller;
            UnityWebRequest req;
            while (m_Dictionary == null) {
                req = UnityWebRequest.Get(installer.GetSrcPath(VersionFileName));
                req.timeout = (int)AssetBundleManager.timeoutSec;
                yield return req;
                if (!string.IsNullOrEmpty(req.error) || req.responseCode != 200) {
                    req.Dispose();
                    m_ErrorCount++;
                    m_isError = m_ErrorCount >= MaxRetryCount;
                    while (m_isError) {
                        yield return null;
                    }
                    continue;
                }

                m_Dictionary = Decode(req.downloadHandler.data).ToDictionary(e => e.name);
                req.Dispose();
            }
            if (m_DictionaryForBuiltin == null) {
                string path = PathUtility.Combine("CRI", VersionFileName);
                req = UnityWebRequest.Get(ABUtility.GetStreamingAssetURL(path));
                req.timeout = (int)AssetBundleManager.timeoutSec;
                yield return req;
                if (string.IsNullOrEmpty(req.error)) {
                    m_DictionaryForBuiltin = Decode(req.downloadHandler.data).ToDictionary(e => e.name);
                }
                req.Dispose();
            }
            m_isDone = true;
        }

        public bool IsDone() {
            return m_isDone;
        }

        public bool IsError() {
            return m_isError;
        }

        public void Restart() {
            if (m_isError) {
                m_isError = false;
                m_ErrorCount = 0;
            }
        }

        public class FileInformation {
            public string name;
            public string version;
            public long size;
        }
    }
}
