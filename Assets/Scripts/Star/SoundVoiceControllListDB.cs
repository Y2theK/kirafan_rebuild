﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005C6 RID: 1478
	[Token(Token = "0x20004B9")]
	[StructLayout(3)]
	public class SoundVoiceControllListDB : ScriptableObject
	{
		// Token: 0x06001604 RID: 5636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B4")]
		[Address(RVA = "0x101342A0C", Offset = "0x1342A0C", VA = "0x101342A0C")]
		public SoundVoiceControllListDB()
		{
		}

		// Token: 0x04001C9E RID: 7326
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001608")]
		public SoundVoiceControllListDB_Param[] m_Params;
	}
}
