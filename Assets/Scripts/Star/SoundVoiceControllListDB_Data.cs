﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005C4 RID: 1476
	[Token(Token = "0x20004B7")]
	[Serializable]
	[StructLayout(0)]
	public struct SoundVoiceControllListDB_Data
	{
		// Token: 0x04001C97 RID: 7319
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001601")]
		public int m_HourMin;

		// Token: 0x04001C98 RID: 7320
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001602")]
		public int m_HourMax;

		// Token: 0x04001C99 RID: 7321
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001603")]
		public string[] m_CueSheetNames;

		// Token: 0x04001C9A RID: 7322
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001604")]
		public string[] m_CueNames;

		// Token: 0x04001C9B RID: 7323
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001605")]
		public string m_NamedCueName;
	}
}
