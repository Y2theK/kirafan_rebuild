﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000534 RID: 1332
	[Token(Token = "0x200042A")]
	[StructLayout(3)]
	public static class SoundVoiceControllListDB_Ext
	{
		// Token: 0x060015A7 RID: 5543 RVA: 0x00009870 File Offset: 0x00007A70
		[Token(Token = "0x600145C")]
		[Address(RVA = "0x101342A14", Offset = "0x1342A14", VA = "0x101342A14")]
		public static SoundVoiceControllListDB_Param GetParam(this SoundVoiceControllListDB self, eSoundVoiceControllListDB id)
		{
			return default(SoundVoiceControllListDB_Param);
		}
	}
}
