﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005C5 RID: 1477
	[Token(Token = "0x20004B8")]
	[Serializable]
	[StructLayout(0)]
	public struct SoundVoiceControllListDB_Param
	{
		// Token: 0x04001C9C RID: 7324
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001606")]
		public int m_ID;

		// Token: 0x04001C9D RID: 7325
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001607")]
		public SoundVoiceControllListDB_Data[] m_Datas;
	}
}
