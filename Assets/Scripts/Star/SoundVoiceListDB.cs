﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005C9 RID: 1481
	[Token(Token = "0x20004BC")]
	[StructLayout(3)]
	public class SoundVoiceListDB : ScriptableObject
	{
		// Token: 0x06001605 RID: 5637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B5")]
		[Address(RVA = "0x101342AFC", Offset = "0x1342AFC", VA = "0x101342AFC")]
		public SoundVoiceListDB()
		{
		}

		// Token: 0x04001D14 RID: 7444
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400167E")]
		public SoundVoiceListDB_Param[] m_Params;
	}
}
