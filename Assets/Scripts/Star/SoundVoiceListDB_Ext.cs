﻿using System.Collections.Generic;

namespace Star {
    public static class SoundVoiceListDB_Ext {
        public static SoundVoiceListDB_Param GetParam(this SoundVoiceListDB self, eSoundVoiceListDB cueID, Dictionary<eSoundVoiceListDB, int> indices) {
            if (indices != null && indices.TryGetValue(cueID, out int num)) {
                return self.m_Params[num];
            }
            return default;
        }
    }
}
