﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005C8 RID: 1480
	[Token(Token = "0x20004BB")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct SoundVoiceListDB_Param
	{
		// Token: 0x04001D12 RID: 7442
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400167C")]
		public int m_ID;

		// Token: 0x04001D13 RID: 7443
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400167D")]
		public string m_CueName;
	}
}
