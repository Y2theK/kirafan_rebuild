﻿using Meige;
using Meige.AssetBundles;
using UnityEngine;

namespace Star {
    public class SpriteHandler {
        private SpriteManager m_Mng;
        private int DummyWidth;
        private int DummyHeight;
        private bool m_NoticedRetry;

        public bool IsDummySprite { get; set; }
        public Sprite Obj { get; set; }
        public MeigeResource.Handler Hndl { get; set; }
        public string Path { get; set; }
        public int RefCount { get; set; }
        public bool ForceClearCache { get; set; }
        public bool IsRetryOnError { get; set; }

        public int AddRef() {
            ForceClearCache = false;
            return ++RefCount;
        }

        public int RemoveRef() {
            return --RefCount;
        }

        public void ResetRef() {
            RefCount = 0;
        }

        public void ForceResetRef() {
            RefCount = 0;
            ForceClearCache = true;
        }

        public void ForceSetClearCache() {
            ForceClearCache = true;
        }

        public SpriteHandler(SpriteManager mng, MeigeResource.Handler hndl, string path, bool isRetryOnError, int dummyWidth, int dummyHeight) {
            m_Mng = mng;
            Hndl = hndl;
            Path = path;
            RefCount = 1;
            ForceClearCache = false;
            IsRetryOnError = isRetryOnError;
            DummyWidth = dummyWidth;
            DummyHeight = dummyHeight;
            m_NoticedRetry = false;
        }

        public bool IsAvailable() {
            return Hndl == null && Obj != null;
        }

        private bool IsError() {
            return Hndl != null && Hndl.IsError;
        }

        public void ApplyObj() {
            if (Hndl == null || !Hndl.IsDone) { return; }

            if (!IsError()) {
                Obj = null;
                if (Hndl.assets != null) {
                    for (int i = 0; i < Hndl.assets.Length; i++) {
                        if (Hndl.assets[i] is Sprite sprite) {
                            Obj = sprite;
                            break;
                        }
                    }
                }
            }
            IsDummySprite = false;
            if (IsError() && IsRetryOnError && RefCount > 0) {
                if (!m_NoticedRetry) {
                    m_NoticedRetry = true;
                    m_Mng.ShowRetryDialog();
                }
                if (m_Mng.IsExecutingRetryDialog()) { return; }
                AssetBundleManager.RestartManifestUpdate();
                MeigeResourceManager.RetryHandler(Hndl);
                m_NoticedRetry = false;
                return;
            }
            if (IsError() || Obj == null) {
                IsDummySprite = true;
                Obj = Sprite.Create(new Texture2D(DummyWidth, DummyHeight), new Rect(0f, 0f, DummyWidth, DummyHeight), new Vector2(0.5f, 0.5f));
                if (IsError()) {
                    AssetBundleManager.RestartManifestUpdate();
                    MeigeResourceManager.ClearError(Hndl.resourceName);
                }
            }
            MeigeResourceManager.UnloadHandler(Hndl, false);
            Hndl = null;
        }

        public void DestroyObj() {
            if (!IsDummySprite) {
                Resources.UnloadAsset(Obj);
            }
            IsDummySprite = false;
            Obj = null;
            if (Hndl != null) {
                MeigeResourceManager.UnloadHandler(Hndl, false);
                Hndl = null;
            }
        }
    }
}
