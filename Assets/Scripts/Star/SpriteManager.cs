﻿using Meige;
using Meige.AssetBundles;
using Star.UI;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Star {
    public class SpriteManager {
        private const int SPRITE_TYPE_NUM = 44;
        private const float UNLOADUNUSEDASSETS_INTERVAL = 10f;

        private readonly LimitData[] LIMIT = new LimitData[SPRITE_TYPE_NUM] {
            new LimitData(96, true, 128, 128),
            new LimitData(24, true, 360, 512),
            new LimitData(4, false, 360, 512),
            new LimitData(2, false, 750, 1000),
            new LimitData(2, false, 750, 1000),
            new LimitData(18, false, 640, 750),
            new LimitData(18, false, 750, 1000),
            new LimitData(2, false, 750, 1000),
            new LimitData(16, false, 128, 128),
            new LimitData(2, false, 1, 1),
            new LimitData(64, true, 128, 128),
            new LimitData(64, true, 256, 256),
            new LimitData(16, true, 256, 256),
            new LimitData(16, true, 256, 256),
            new LimitData(16, false, 128, 128),
            new LimitData(24, false, 512, 154),
            new LimitData(1, false, 0, 0),
            new LimitData(1, false, 0, 0),
            new LimitData(8, false, 588, 132),
            new LimitData(8, false, 596, 136),
            new LimitData(8, false, 400, 520),
            new LimitData(16, false, 128, 128),
            new LimitData(8, false, 0, 0),
            new LimitData(1, false, 150, 150),
            new LimitData(1, false, 320, 700),
            new LimitData(16, false, 591, 134),
            new LimitData(4, false, 0, 0),
            new LimitData(32, false, 0, 0),
            new LimitData(320, false, 0, 0),
            new LimitData(4, false, 1, 1),
            new LimitData(16, false, 0, 0),
            new LimitData(6, false, 512, 320),
            new LimitData(1, false, 512, 320),
            new LimitData(1, false, 220, 42),
            new LimitData(24, false, 512, 512),
            new LimitData(1, false, 730, 176),
            new LimitData(1, false, 654, 260),
            new LimitData(4, false, 160, 46),
            new LimitData(64, true, 128, 128),
            new LimitData(1, false, 512, 512),
            new LimitData(32, true, 1, 1),
            new LimitData(16, true, 256, 256),
            new LimitData(8, false, 588, 132),
            new LimitData(2, false, 1, 1),
        };

        private Dictionary<string, SpriteHandler>[] m_HandlersArray;
        private List<string>[] m_PathsArray;
        private List<string> m_RemoveKeys;
        private float m_UnloadUnusedAssetsInterval;
        private bool m_IsRequesetUnloadUnusedAssets;
        private StringBuilder m_sb = new StringBuilder();
        private bool m_IsExecutingRetryDialog;
        private List<MeigeResource.Handler> m_DLOnlyResourceHandlerList = new List<MeigeResource.Handler>();
        private List<MeigeResource.Handler> m_DLOnlyRemoveList = new List<MeigeResource.Handler>();
        private List<string> m_NoticedDLOnlyRetryResNameList = new List<string>();

        public SpriteManager() {
            m_HandlersArray = new Dictionary<string, SpriteHandler>[SPRITE_TYPE_NUM];
            m_PathsArray = new List<string>[SPRITE_TYPE_NUM];
            for (int i = 0; i < SPRITE_TYPE_NUM; i++) {
                m_HandlersArray[i] = new Dictionary<string, SpriteHandler>();
                m_PathsArray[i] = new List<string>();
            }
            m_RemoveKeys = new List<string>();
            m_IsExecutingRetryDialog = false;
        }

        public bool IsAvailable() {
            return true;
        }

        public bool IsExecutingRetryDialog() {
            return m_IsExecutingRetryDialog;
        }

        public bool ExistDownloadOnly() {
            return m_DLOnlyResourceHandlerList.Count > 0;
        }

        private void UpdateDLOnly() {
            foreach (MeigeResource.Handler handler in m_DLOnlyResourceHandlerList) {
                if (!handler.IsDone) { continue; }

                if (handler.IsError) {
                    if (!m_NoticedDLOnlyRetryResNameList.Contains(handler.resourceName)) {
                        m_NoticedDLOnlyRetryResNameList.Add(handler.resourceName);
                        ShowRetryDialog();
                    }
                    if (!m_IsExecutingRetryDialog) {
                        if (m_NoticedDLOnlyRetryResNameList.Contains(handler.resourceName)) {
                            m_NoticedDLOnlyRetryResNameList.Remove(handler.resourceName);
                        }
                        AssetBundleManager.RestartManifestUpdate();
                        MeigeResourceManager.RetryHandler(handler);
                    }
                } else {
                    bool found = false;
                    for (int i = 0; i < m_HandlersArray.Length; i++) {
                        foreach (string key in m_HandlersArray[i].Keys) {
                            if (m_HandlersArray[i][key].Hndl == handler) {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found) {
                        MeigeResourceManager.UnloadHandler(handler);
                    }
                    m_DLOnlyRemoveList.Add(handler);
                }
            }
            foreach (MeigeResource.Handler handler in m_DLOnlyRemoveList) {
                m_DLOnlyResourceHandlerList.Remove(handler);
            }
            m_DLOnlyRemoveList.Clear();
        }

        public void Update() {
            UpdateDLOnly();
            for (int i = 0; i < SPRITE_TYPE_NUM; i++) {
                Dictionary<string, SpriteHandler> handlers = m_HandlersArray[i];
                List<string> paths = m_PathsArray[i];
                m_RemoveKeys.Clear();
                foreach (string key in handlers.Keys) {
                    SpriteHandler spriteHandler = handlers[key];
                    if (!spriteHandler.IsAvailable()) {
                        spriteHandler.ApplyObj();
                    } else if ((!LIMIT[i].m_IsCache || spriteHandler.ForceClearCache) && spriteHandler.RefCount == 0) {
                        spriteHandler.DestroyObj();
                        m_RemoveKeys.Add(key);
                        m_IsRequesetUnloadUnusedAssets = true;
                    }
                }
                for (int j = 0; j < m_RemoveKeys.Count; j++) {
                    handlers.Remove(m_RemoveKeys[j]);
                    paths.Remove(m_RemoveKeys[j]);
                }
                while (paths.Count > LIMIT[i].m_LimitNum) {
                    SpriteHandler spriteHandler = handlers[paths[0]];
                    int index;
                    for (index = 0; index < paths.Count; index++) {
                        if (handlers[paths[index]].RefCount <= 0) {
                            spriteHandler = handlers[paths[index]];
                            break;
                        }
                    }
                    if (spriteHandler == null || !spriteHandler.IsAvailable()) {
                        break;
                    }
                    Debug.Log("refcount" + spriteHandler.RefCount);
                    spriteHandler.DestroyObj();
                    handlers.Remove(spriteHandler.Path);
                    paths.RemoveAt(index);
                    m_IsRequesetUnloadUnusedAssets = true;
                }
            }
            if (m_UnloadUnusedAssetsInterval > 0f) {
                m_UnloadUnusedAssetsInterval -= Time.deltaTime;
            }
            if (m_IsRequesetUnloadUnusedAssets && m_UnloadUnusedAssetsInterval <= 0f) {
                if (GameSystem.Inst != null) {
                    GameSystem.Inst.RequesetUnloadUnusedAssets(true);
                }
                m_UnloadUnusedAssetsInterval = UNLOADUNUSEDASSETS_INTERVAL;
                m_IsRequesetUnloadUnusedAssets = false;
            }
        }

        public SpriteHandler LoadAsyncCharaIcon(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/charaicon/charaicon_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.CharaIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncBust(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/bust/bust_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.Bust, m_sb.ToString(), true);
        }

        public SpriteHandler LoadAsyncBustFull(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/bustfull/bustfull_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.BustFull, m_sb.ToString(), false);
        }

        public string GetCharaIllustCardPath(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/characard/characard_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return m_sb.ToString();
        }

        public SpriteHandler LoadAsyncCharaCard(int charaID) {
            return LoadAsync(eSpriteType.CharaCard, GetCharaIllustCardPath(charaID), false);
        }

        public string GetCharaIllustBustPath(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/charaillustbust/charaillust_bust_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return m_sb.ToString();
        }

        public SpriteHandler LoadAsyncCharaIllustBust(int charaID) {
            return LoadAsync(eSpriteType.CharaIllustBust, GetCharaIllustBustPath(charaID), true);
        }

        public SpriteHandler LoadAsyncCharaIllustEdit(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/charaillustedit/charaillust_edit_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.CharaIllustEdit, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncCharaIllustChara(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/charaillustchara/charaillust_chara_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.CharaIllustChara, m_sb.ToString(), true);
        }

        public string GetCharaIllustFullPath(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/charaillustfull/charaillust_full_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return m_sb.ToString();
        }

        public SpriteHandler LoadAsyncCharaIllustFull(int charaID) {
            return LoadAsync(eSpriteType.CharaIllustFull, GetCharaIllustFullPath(charaID), true);
        }

        public SpriteHandler LoadAsyncPLOrderIcon(int charaID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/ordericon/pl_ordericon_");
            m_sb.Append(charaID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.OrderIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncENOrderIcon(int resourceID) {
            m_sb.Length = 0;
            m_sb.Append("texture/charauiresource/ordericon/en_ordericon_");
            m_sb.Append(resourceID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.OrderIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncBackGround(string filename) {
            m_sb.Length = 0;
            m_sb.Append("texture/background/");
            m_sb.Append(filename);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.BackGround, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncUIBackGround(string filename) {
            m_sb.Length = 0;
            m_sb.Append("texture/uibackground/");
            m_sb.Append(filename);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.UIBackGround, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncItemIcon(int itemID) {
            m_sb.Length = 0;
            m_sb.Append("texture/itemicon/itemicon_");
            m_sb.Append(itemID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.ItemIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncWeaponIcon(int weaponID) {
            m_sb.Length = 0;
            m_sb.Append("texture/weaponicon/weaponicon_wpn_");
            m_sb.Append(GameSystem.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_ResourceID_L); //TODO: don't know if this is the correct id
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.WeaponIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncTownObjectIcon(int townObjectId, int lv = 1) {
            m_sb.Length = 0;
            m_sb.Append("texture/townobjecticon/townobjecticon_bld_");
            m_sb.Append(TownUtility.GetResourceNoName(GameSystem.Inst.DbMng.TownObjListDB.GetParam(townObjectId).m_ResourceID, lv, 0).ToLower());
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.TownObjectIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncRoomObjectIcon(eRoomObjectCategory category, int roomObjectID) {
            m_sb.Length = 0;
            m_sb.Append("texture/roomobjecticon/");
            m_sb.Append("roomobjecticon_");
            m_sb.Append(category.ToString().ToLower());
            m_sb.Append("_");
            m_sb.Append(roomObjectID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.TownObjectIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncOrbIcon(int orbID) {
            m_sb.Length = 0;
            m_sb.Append("texture/orbicon/orbicon_");
            m_sb.Append(orbID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.OrbIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncContentTitleLogo(eTitleType titleType) {
            m_sb.Length = 0;
            m_sb.Append("texture/contenttitlelogo/contentslogo");
            m_sb.Append((int)titleType);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.ContentTitleLogo, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncNpcIllust(NpcIllust.eNpc npc) {
            string value = null;
            switch (npc) {
                case NpcIllust.eNpc.Summon:
                    value = "summon";
                    break;
                case NpcIllust.eNpc.Trade:
                    value = "trade";
                    break;
                case NpcIllust.eNpc.Weapon:
                    value = "weapon";
                    break;
                case NpcIllust.eNpc.Build:
                    value = "build";
                    break;
                case NpcIllust.eNpc.Training:
                    value = "training";
                    break;
                case NpcIllust.eNpc.Lamp:
                    value = "lamp";
                    break;
            }
            m_sb.Length = 0;
            m_sb.Append("texture/npc/npc_");
            m_sb.Append(value);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.NpcIllust, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncMasterIllust() {
            m_sb.Length = 0;
            m_sb.Append("texture/master/master_0");
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.MasterIllust, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestEventTypeIcon(int eventType) {
            return LoadAsyncQuestEventTypeIcon(eventType == -1 ? "chapter" : eventType.ToString());
        }

        private SpriteHandler LoadAsyncQuestEventTypeIcon(string resourceNameSuffix) {
            m_sb.Length = 0;
            m_sb.Append("texture/questeventtypeicon/questeventtypeicon_");
            m_sb.Append(resourceNameSuffix);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestEventTypeIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestGroupIcon(int bannerID) {
            m_sb.Length = 0;
            m_sb.Append("texture/questgroupicon/questgroupicon_");
            m_sb.Append(bannerID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestGroupIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestChapterIcon(int chapterId) {
            m_sb.Length = 0;
            m_sb.Append("texture/questchaptericon/questchaptericon_");
            m_sb.Append(chapterId);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestChapterIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestChapterIcon_Cleared(int chapterId) {
            m_sb.Length = 0;
            m_sb.Append("texture/questchaptericon/questchaptericon_");
            m_sb.Append(chapterId);
            m_sb.Append("_cleared");
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestChapterIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestChapterIcon_Soon(int part) {
            m_sb.Length = 0;
            m_sb.Append("texture/questchaptericon/questchaptericon_soon_");
            m_sb.Append(part);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestChapterIcon, m_sb.ToString(), false);
        }

        public bool IsPossibleLoadQuestChapterIcon() {
            return m_HandlersArray[(int)eSpriteType.QuestChapterIcon].Count < LIMIT[(int)eSpriteType.QuestChapterIcon].m_LimitNum;
        }

        public SpriteHandler LoadAsyncQuestTypeIcon(int iconId) {
            m_sb.Length = 0;
            m_sb.Append("texture/questtypeicon/questtypeicon_");
            m_sb.Append(iconId);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestTypeIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncQuestPartImage(int part) {
            m_sb.Length = 0;
            m_sb.Append("texture/questpartimage/questpartimage_");
            m_sb.Append(part);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.QuestPartImage, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncLibraryOriginalCharaIcon(int id) {
            m_sb.Length = 0;
            m_sb.Append("texture/originalcharactericon/originalcharactericon_");
            m_sb.Append(id);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.LibraryOriginalCharaIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncLibraryOriginalCharaIllust(int id) {
            m_sb.Length = 0;
            m_sb.Append("texture/originalcharacterillust/originalcharacterillust_");
            m_sb.Append(id);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.LibraryOriginalCharaIllust, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncADVLibraryIcon(int id) {
            m_sb.Length = 0;
            m_sb.Append("texture/advlibraryicon/advlibraryicon_");
            m_sb.Append(id);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.ADVLibraryIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncGachaItemLabelIcon(string filename) {
            m_sb.Length = 0;
            m_sb.Append("texture/gachaitemlabel/");
            m_sb.Append(filename);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.GachaItemLabel, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncADVStandPic(string resourceBaseName, int poseId) {
            return LoadAsync(eSpriteType.ADVStandPic, ADVUtility.GetADVStandPicResourcePath(resourceBaseName, poseId), false);
        }

        public SpriteHandler LoadAsyncADVFace(string resourceBaseName, int facePatternID, string faceName) {
            return LoadAsync(eSpriteType.ADVFace, ADVUtility.GetADVFaceResourcePath(resourceBaseName, facePatternID, faceName), false);
        }

        public SpriteHandler LoadAsyncADVBackGround(string filename) {
            return LoadAsync(eSpriteType.ADVBackGround, ADVUtility.GetADVBackGroundPath(filename), false);
        }

        public SpriteHandler LoadAsyncADVSprite(string filename) {
            return LoadAsync(eSpriteType.ADVSprite, ADVUtility.GetADVSpritePath(filename), false);
        }

        public SpriteHandler LoadAsyncTutorialTips(eTutorialTipsListDB id, int imageID) {
            m_sb.Length = 0;
            m_sb.Append("texture/tutorialtips/tutorialtips_");
            m_sb.Append((int)id);
            m_sb.Append("_");
            m_sb.Append(imageID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.TutorialTips, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncRetireTips(eRetireTipsListDB id, int imageID) {
            m_sb.Length = 0;
            m_sb.Append("texture/retiretips/retiretips_");
            m_sb.Append((int)id);
            m_sb.Append("_");
            m_sb.Append(imageID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.RetireTips, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncSceneTitle(string resourceName) {
            m_sb.Length = 0;
            m_sb.Append("texture/scenetitle/");
            m_sb.Append(resourceName.ToLower());
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.SceneTitle, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncGachaBannerNoExist() {
            m_sb.Length = 0;
            m_sb.Append("texture/gachabanner/nogacha");
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.GachaBanner, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncGachaBanner(string resourceName, bool first) {
            m_sb.Length = 0;
            if (first) {
                m_sb.Append("texture/gachabanner/gachabannerfirst_");
            } else {
                m_sb.Append("texture/gachabanner/gachabanner_");
            }
            m_sb.Append(resourceName.ToLower());
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.GachaBanner, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncGachaBannerSelection(string resourceName) {
            m_sb.Length = 0;
            m_sb.Append("texture/gachabanner/gachabannerselection_");
            m_sb.Append(resourceName.ToLower());
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.GachaBannerSelection, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncChestBackGround(string resourceName) {
            m_sb.Length = 0;
            m_sb.Append("texture/chest/");
            m_sb.Append(resourceName);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.ChestBackGround, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncShopLabel(int id) {
            m_sb.Length = 0;
            m_sb.Append("texture/shoplabel/shoplabel_");
            m_sb.Append(id);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.ShopLabel, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncPackageIcon(int iconID) {
            m_sb.Length = 0;
            m_sb.Append("texture/packageicon/packageicon_");
            m_sb.Append(iconID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.PackageIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncRegularPassHelpBanner(string helpBannerName) {
            m_sb.Length = 0;
            m_sb.Append("texture/storehelpbanner/");
            m_sb.Append(helpBannerName);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.StoreHelpBanner, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncAchievementImage(int achievementID) {
            m_sb.Length = 0;
            m_sb.Append("texture/achievement/achievement_");
            m_sb.Append(achievementID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.AchievementImage, m_sb.ToString(), true);
        }

        public SpriteHandler LoadAsyncTitleIcon(eTitleType titleType) {
            m_sb.Length = 0;
            m_sb.Append("texture/titleicon/titleicon_");
            m_sb.Append((int)titleType);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.TitleIcon, m_sb.ToString(), false);
        }

        public SpriteHandler LoadAsyncMovieBanner(int bannerID) {
            m_sb.Length = 0;
            m_sb.Append("texture/moviebanner/moviebanner_");
            m_sb.Append(bannerID);
            m_sb.Append(MeigeResourceManager.AB_EXTENSION);
            return LoadAsync(eSpriteType.MovieBanner, m_sb.ToString(), false);
        }

        private SpriteHandler LoadAsync(eSpriteType spriteType, string path, bool isRetryOnError = false) {
            SpriteHandler spriteHandler;
            int type = (int)spriteType;
            if (m_HandlersArray[type].ContainsKey(path)) {
                spriteHandler = m_HandlersArray[type][path];
                spriteHandler.AddRef();
                if (spriteHandler.IsDummySprite) {
                    spriteHandler.DestroyObj();
                    spriteHandler.Hndl = MeigeResourceManager.LoadHandler(path);
                }
                m_PathsArray[type].Remove(path);
            } else {
                bool unloadedDLOnly = false;
                for (int i = m_DLOnlyResourceHandlerList.Count - 1; i >= 0; i--) {
                    if (m_DLOnlyResourceHandlerList[i].resourceName == path) {
                        MeigeResourceManager.UnloadHandler(m_DLOnlyResourceHandlerList[i]);
                        m_DLOnlyResourceHandlerList.RemoveAt(i);
                        unloadedDLOnly = true;
                    }
                }
                MeigeResource.Handler hndl = MeigeResourceManager.LoadHandler(path);
                spriteHandler = new SpriteHandler(this, hndl, path, isRetryOnError, LIMIT[type].m_Width, LIMIT[type].m_Height);
                if (unloadedDLOnly) {
                    Download(path);
                }
                m_HandlersArray[type].Add(path, spriteHandler);
            }
            m_PathsArray[(int)spriteType].Add(path);
            return spriteHandler;
        }

        public void Unload(SpriteHandler hndl) {
            if (hndl != null) {
                hndl.RemoveRef();
            }
        }

        public void UnloadForceCacheClear(SpriteHandler hndl) {
            if (hndl != null) {
                hndl.RemoveRef();
                hndl.ForceSetClearCache();
            }
        }

        public void Download(string path) {
            for (int i = 0; i < m_DLOnlyResourceHandlerList.Count; i++) {
                if (m_DLOnlyResourceHandlerList[i].resourceName == path) {
                    return;
                }
            }
            MeigeResource.Handler handler = MeigeResourceManager.LoadHandler(path, MeigeResource.Option.DownloadOnly);
            if (!m_DLOnlyResourceHandlerList.Contains(handler)) {
                m_DLOnlyResourceHandlerList.Add(handler);
            }
        }

        public void ForceResetOnReturnTitle() {
            for (int i = 0; i < SPRITE_TYPE_NUM; i++) {
                Dictionary<string, SpriteHandler> dictionary = m_HandlersArray[i];
                foreach (SpriteHandler spriteHandler in dictionary.Values) {
                    spriteHandler.ResetRef();
                }
            }
            m_IsExecutingRetryDialog = false;
        }

        public void ShowRetryDialog() {
            if (m_IsExecutingRetryDialog) { return; }

            m_IsExecutingRetryDialog = true;
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.DownloadErrTitle, eText_MessageDB.DownloadErr, (_) => m_IsExecutingRetryDialog = false);
        }

        public void ForceSetCacheClearFlg(eSpriteType type) {
            if (m_HandlersArray[(int)type] != null) {
                foreach (SpriteHandler spriteHandler in m_HandlersArray[(int)type].Values) {
                    spriteHandler.ForceSetClearCache();
                }
            }
        }

        public enum eSpriteType {
            None = -1,
            CharaIcon,
            Bust,
            BustFull,
            CharaCard,
            CharaIllustBust,
            CharaIllustEdit,
            CharaIllustChara,
            CharaIllustFull,
            OrderIcon,
            BackGround,
            ItemIcon,
            WeaponIcon,
            TownObjectIcon,
            RoomObjectIcon,
            OrbIcon,
            ContentTitleLogo,
            NpcIllust,
            MasterIllust,
            QuestEventTypeIcon,
            QuestGroupIcon,
            QuestChapterIcon,
            QuestTypeIcon,
            QuestPartImage,
            LibraryOriginalCharaIcon,
            LibraryOriginalCharaIllust,
            ADVLibraryIcon,
            GachaItemLabel,
            ADVStandPic,
            ADVFace,
            ADVBackGround,
            ADVSprite,
            TutorialTips,
            RetireTips,
            SceneTitle,
            GachaBanner,
            GachaBannerSelection,
            ChestBackGround,
            ShopLabel,
            PackageIcon,
            StoreHelpBanner,
            AchievementImage,
            TitleIcon,
            MovieBanner,
            UIBackGround,
            Num
        }

        private struct LimitData {
            public int m_LimitNum;
            public bool m_IsCache;
            public int m_Width;
            public int m_Height;

            public LimitData(int limitNum, bool isCache, int width, int height) {
                m_LimitNum = limitNum;
                m_IsCache = isCache;
                m_Width = width;
                m_Height = height;
            }
        }
    }
}
