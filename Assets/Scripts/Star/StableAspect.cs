﻿using UnityEngine;

namespace Star {
    public class StableAspect : MonoBehaviour {
        [SerializeField] private float m_Width = 2048f;
        [SerializeField] private float m_Height = 1152;
        [SerializeField] private float m_PixelPerUnit = 100f;
        [SerializeField] private Camera m_Camera;

        public float TargetWidth => m_Width;
        public float TargetHeight => m_Height;

        private void Apply() {
            if (m_Camera != null) {
                m_Camera.orthographicSize = m_Height * 0.5f / m_PixelPerUnit;
                float ratio = Mathf.Min(Screen.height / m_Height, Screen.width / m_Width);
                float viewportW = m_Width * ratio / Screen.width;
                float viewportH = m_Height * ratio / Screen.height;
                m_Camera.rect = new Rect((1 - viewportW) * 0.5f, (1 - viewportH) * 0.5f, viewportW, viewportH);
            }
        }

        private void Start() {
            Apply();
        }
    }
}
