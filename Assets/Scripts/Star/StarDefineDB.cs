﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005DF RID: 1503
	[Token(Token = "0x20004D2")]
	[StructLayout(3)]
	public class StarDefineDB : ScriptableObject
	{
		// Token: 0x06001606 RID: 5638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B6")]
		[Address(RVA = "0x101348A40", Offset = "0x1348A40", VA = "0x101348A40")]
		public StarDefineDB()
		{
		}

		// Token: 0x04001EBA RID: 7866
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001824")]
		public StarDefineDB_Param[] m_Params;
	}
}
