﻿namespace Star {
    public static class StarDefineDB_Ext {
        public static float GetValue(this StarDefineDB self, eStarDefineDB eDef) {
            return self.m_Params[(int)eDef].m_Value;
        }
    }
}
