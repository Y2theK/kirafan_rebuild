﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005DE RID: 1502
	[Token(Token = "0x20004D1")]
	[Serializable]
	[StructLayout(0, Size = 8)]
	public struct StarDefineDB_Param
	{
		// Token: 0x04001EB8 RID: 7864
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001822")]
		public int m_ID;

		// Token: 0x04001EB9 RID: 7865
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001823")]
		public float m_Value;
	}
}
