﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005E3 RID: 1507
	[Token(Token = "0x20004D6")]
	[StructLayout(3)]
	public class StateIconListDB : ScriptableObject
	{
		// Token: 0x06001607 RID: 5639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B7")]
		[Address(RVA = "0x101348AD0", Offset = "0x1348AD0", VA = "0x101348AD0")]
		public StateIconListDB()
		{
		}

		// Token: 0x04001EFD RID: 7933
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001867")]
		public StateIconListDB_Param[] m_Params;
	}
}
