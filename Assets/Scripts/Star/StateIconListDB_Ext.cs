﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000537 RID: 1335
	[Token(Token = "0x200042D")]
	[StructLayout(3)]
	public static class StateIconListDB_Ext
	{
		// Token: 0x060015AA RID: 5546 RVA: 0x000098B8 File Offset: 0x00007AB8
		[Token(Token = "0x600145F")]
		[Address(RVA = "0x101348AD8", Offset = "0x1348AD8", VA = "0x101348AD8")]
		public static StateIconListDB_Param GetParam(this StateIconListDB self, eStateIconType iconType)
		{
			return default(StateIconListDB_Param);
		}
	}
}
