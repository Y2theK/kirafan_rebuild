﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005E2 RID: 1506
	[Token(Token = "0x20004D5")]
	[Serializable]
	[StructLayout(0, Size = 48)]
	public struct StateIconListDB_Param
	{
		// Token: 0x04001EF6 RID: 7926
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001860")]
		public int m_StateIconType;

		// Token: 0x04001EF7 RID: 7927
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001861")]
		public string m_IconName;

		// Token: 0x04001EF8 RID: 7928
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001862")]
		public string m_Descript;

		// Token: 0x04001EF9 RID: 7929
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001863")]
		public string m_IconBaseSpriteName;

		// Token: 0x04001EFA RID: 7930
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001864")]
		public string m_IconSpriteName;

		// Token: 0x04001EFB RID: 7931
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001865")]
		public int m_Sort;

		// Token: 0x04001EFC RID: 7932
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001866")]
		public int m_Category;
	}
}
