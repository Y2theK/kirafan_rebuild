﻿using System;

namespace Star {
    public static class StoreDefine {
        public enum eProductType {
            All = -1,
            Gem,
            Premium,
            GemPack,
            Passport,
            DirectSale,
            Exchange,
            Num
        }

        public enum eUIType {
            Normal,
            FirstOnly
        }

        public enum eRegularPassType {
            None = -1,
            Summon,
            Chara,
            Weapon,
            Num
        }

        public enum eGrantType {
            None,
            OnPurchase,
            OnEvery
        }

        public enum eRemindType {
            None,
            Overlap,
            Expired
        }

        [Flags]
        public enum eBadgeType {
            Undefined = 0,
            None = 1,
            New = 2,
            Update = 4
        }

        public enum eRestockType {
            None,
            Day,
            Week,
            Month
        }
    }
}
