﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x020008A6 RID: 2214
	[Token(Token = "0x200065E")]
	[StructLayout(3)]
	public sealed class StoreManager
	{
		// Token: 0x1700026F RID: 623
		// (get) Token: 0x060023C9 RID: 9161 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700024A")]
		private InAppPurchaser IAP
		{
			[Token(Token = "0x6002106")]
			[Address(RVA = "0x101348C04", Offset = "0x1348C04", VA = "0x101348C04")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x060023CA RID: 9162 RVA: 0x0000F4F8 File Offset: 0x0000D6F8
		// (set) Token: 0x060023CB RID: 9163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700024B")]
		public bool IsNewAny
		{
			[Token(Token = "0x6002107")]
			[Address(RVA = "0x101348C98", Offset = "0x1348C98", VA = "0x101348C98")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002108")]
			[Address(RVA = "0x101348CA0", Offset = "0x1348CA0", VA = "0x101348CA0")]
			set
			{
			}
		}

		// Token: 0x060023CC RID: 9164 RVA: 0x0000F510 File Offset: 0x0000D710
		[Token(Token = "0x6002109")]
		[Address(RVA = "0x101348CA8", Offset = "0x1348CA8", VA = "0x101348CA8")]
		public long GetMonthlyAmount()
		{
			return 0L;
		}

		// Token: 0x060023CD RID: 9165 RVA: 0x0000F528 File Offset: 0x0000D728
		[Token(Token = "0x600210A")]
		[Address(RVA = "0x101348CB0", Offset = "0x1348CB0", VA = "0x101348CB0")]
		public long GetAvailableBalance()
		{
			return 0L;
		}

		// Token: 0x060023CE RID: 9166 RVA: 0x0000F540 File Offset: 0x0000D740
		[Token(Token = "0x600210B")]
		[Address(RVA = "0x101348CB8", Offset = "0x1348CB8", VA = "0x101348CB8")]
		public int GetMonthlyPurchaseAmount()
		{
			return 0;
		}

		// Token: 0x060023CF RID: 9167 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600210C")]
		[Address(RVA = "0x101348DE0", Offset = "0x1348DE0", VA = "0x101348DE0")]
		public List<StoreManager.Product> GetProducts()
		{
			return null;
		}

		// Token: 0x060023D0 RID: 9168 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600210D")]
		[Address(RVA = "0x101348DE8", Offset = "0x1348DE8", VA = "0x101348DE8")]
		public List<StoreManager.Product> GetProducts(StoreDefine.eProductType productType)
		{
			return null;
		}

		// Token: 0x060023D1 RID: 9169 RVA: 0x0000F558 File Offset: 0x0000D758
		[Token(Token = "0x600210E")]
		[Address(RVA = "0x101348F70", Offset = "0x1348F70", VA = "0x101348F70")]
		private bool IsValid(StoreManager.Product _data)
		{
			return default(bool);
		}

		// Token: 0x060023D2 RID: 9170 RVA: 0x0000F570 File Offset: 0x0000D770
		[Token(Token = "0x600210F")]
		[Address(RVA = "0x101349378", Offset = "0x1349378", VA = "0x101349378")]
		public bool ContainsCategory(StoreDefine.eProductType productType)
		{
			return default(bool);
		}

		// Token: 0x060023D3 RID: 9171 RVA: 0x0000F588 File Offset: 0x0000D788
		[Token(Token = "0x6002110")]
		[Address(RVA = "0x1013493F8", Offset = "0x13493F8", VA = "0x1013493F8")]
		public bool ExistNewProduct(StoreDefine.eProductType productType)
		{
			return default(bool);
		}

		// Token: 0x060023D4 RID: 9172 RVA: 0x0000F5A0 File Offset: 0x0000D7A0
		[Token(Token = "0x6002111")]
		[Address(RVA = "0x101349524", Offset = "0x1349524", VA = "0x101349524")]
		public StoreDefine.eBadgeType GetBadgeType(StoreDefine.eProductType productType)
		{
			return StoreDefine.eBadgeType.Undefined;
		}

		// Token: 0x060023D5 RID: 9173 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002112")]
		[Address(RVA = "0x1013496B8", Offset = "0x13496B8", VA = "0x1013496B8")]
		public StoreManager.Product GetProduct(int id)
		{
			return null;
		}

		// Token: 0x060023D6 RID: 9174 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002113")]
		[Address(RVA = "0x1013497C4", Offset = "0x13497C4", VA = "0x1013497C4")]
		public StoreManager.Product GetPremium(int premiumId)
		{
			return null;
		}

		// Token: 0x060023D7 RID: 9175 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002114")]
		[Address(RVA = "0x101349914", Offset = "0x1349914", VA = "0x101349914")]
		public StoreManager.Product GetExchange(long exchangeShopId)
		{
			return null;
		}

		// Token: 0x060023D8 RID: 9176 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002115")]
		[Address(RVA = "0x101349A9C", Offset = "0x1349A9C", VA = "0x101349A9C")]
		public List<StoreManager.PurchaseLog> GetPurchaseLogs()
		{
			return null;
		}

		// Token: 0x060023D9 RID: 9177 RVA: 0x0000F5B8 File Offset: 0x0000D7B8
		[Token(Token = "0x6002116")]
		[Address(RVA = "0x101349AA4", Offset = "0x1349AA4", VA = "0x101349AA4")]
		public bool IsAvailableProduct(StoreManager.Product product)
		{
			return default(bool);
		}

		// Token: 0x060023DA RID: 9178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002117")]
		[Address(RVA = "0x101349AEC", Offset = "0x1349AEC", VA = "0x101349AEC")]
		private void SetupPrefabIndex(List<StoreManager.Product> productList)
		{
		}

		// Token: 0x060023DB RID: 9179 RVA: 0x0000F5D0 File Offset: 0x0000D7D0
		[Token(Token = "0x6002118")]
		[Address(RVA = "0x101349C80", Offset = "0x1349C80", VA = "0x101349C80")]
		public int GetPrefabIndex(StoreDefine.eProductType productType)
		{
			return 0;
		}

		// Token: 0x060023DC RID: 9180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002119")]
		[Address(RVA = "0x101349CD0", Offset = "0x1349CD0", VA = "0x101349CD0")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x060023DD RID: 9181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600211A")]
		[Address(RVA = "0x101349CDC", Offset = "0x1349CDC", VA = "0x101349CDC")]
		public void Update()
		{
		}

		// Token: 0x060023DE RID: 9182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600211B")]
		[Address(RVA = "0x10134A284", Offset = "0x134A284", VA = "0x10134A284")]
		private void OnInitializeProducts(bool isError)
		{
		}

		// Token: 0x060023DF RID: 9183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600211C")]
		[Address(RVA = "0x10134A7B8", Offset = "0x134A7B8", VA = "0x10134A7B8")]
		private void OnCompleteSendReceiptByRestore(ResultCode resultCode)
		{
		}

		// Token: 0x060023E0 RID: 9184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600211D")]
		[Address(RVA = "0x10134A944", Offset = "0x134A944", VA = "0x10134A944")]
		private void OnCompleteSendReceiptByPurchase(ResultCode resultCode)
		{
		}

		// Token: 0x060023E1 RID: 9185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600211E")]
		[Address(RVA = "0x10134AA88", Offset = "0x134AA88", VA = "0x10134AA88")]
		private void OnCompleteRegisterPendingPurchase()
		{
		}

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x060023E2 RID: 9186 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023E3 RID: 9187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001A")]
		private event Action m_OnCompleteSetAge
		{
			[Token(Token = "0x600211F")]
			[Address(RVA = "0x10134ABF8", Offset = "0x134ABF8", VA = "0x10134ABF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002120")]
			[Address(RVA = "0x10134AD04", Offset = "0x134AD04", VA = "0x10134AD04")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060023E4 RID: 9188 RVA: 0x0000F5E8 File Offset: 0x0000D7E8
		[Token(Token = "0x6002121")]
		[Address(RVA = "0x10134AE10", Offset = "0x134AE10", VA = "0x10134AE10")]
		public bool IsNeedCheckAge()
		{
			return default(bool);
		}

		// Token: 0x060023E5 RID: 9189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002122")]
		[Address(RVA = "0x10134AEAC", Offset = "0x134AEAC", VA = "0x10134AEAC")]
		public void Request_SetAge(eAgeType ageType, Action onComplete)
		{
		}

		// Token: 0x060023E6 RID: 9190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002123")]
		[Address(RVA = "0x10134B050", Offset = "0x134B050", VA = "0x10134B050")]
		private void OnResponse_SetAge(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x060023E7 RID: 9191 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023E8 RID: 9192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001B")]
		private event Action m_OnCompleteInitializeProduct
		{
			[Token(Token = "0x6002124")]
			[Address(RVA = "0x10134B248", Offset = "0x134B248", VA = "0x10134B248")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002125")]
			[Address(RVA = "0x10134B354", Offset = "0x134B354", VA = "0x10134B354")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060023E9 RID: 9193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002126")]
		[Address(RVA = "0x10134B460", Offset = "0x134B460", VA = "0x10134B460")]
		public void Request_InitializeProducts(Action onComplete, bool initializeIAP = true, bool isConnectingIcon = true)
		{
		}

		// Token: 0x060023EA RID: 9194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002127")]
		[Address(RVA = "0x10134B6B4", Offset = "0x134B6B4", VA = "0x10134B6B4")]
		private void OnResponse_StoreGetAll(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060023EB RID: 9195 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002128")]
		[Address(RVA = "0x10134BA68", Offset = "0x134BA68", VA = "0x10134BA68")]
		private List<string> RefreshProductList(StoreProduct[] storeProducts)
		{
			return null;
		}

		// Token: 0x060023EC RID: 9196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002129")]
		[Address(RVA = "0x10134BC40", Offset = "0x134BC40", VA = "0x10134BC40")]
		private void RefreshProductListExchange(ExchangeShop[] exchangeShops)
		{
		}

		// Token: 0x060023ED RID: 9197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600212A")]
		[Address(RVA = "0x10134BDAC", Offset = "0x134BDAC", VA = "0x10134BDAC")]
		private static void wwwConvert(StoreProduct src, StoreManager.Product dest)
		{
		}

		// Token: 0x060023EE RID: 9198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600212B")]
		[Address(RVA = "0x10134CEB4", Offset = "0x134CEB4", VA = "0x10134CEB4")]
		private static void wwwConvert(ExchangeShop src, StoreManager.Product dest)
		{
		}

		// Token: 0x060023EF RID: 9199 RVA: 0x0000F600 File Offset: 0x0000D800
		[Token(Token = "0x600212C")]
		[Address(RVA = "0x10134D4E4", Offset = "0x134D4E4", VA = "0x10134D4E4")]
		private static int GetRewardAmount(WWWTypes.PremiumReward[] reward, ePresentType presentType, StoreDefine.eGrantType grantType)
		{
			return 0;
		}

		// Token: 0x060023F0 RID: 9200 RVA: 0x0000F618 File Offset: 0x0000D818
		[Token(Token = "0x600212D")]
		[Address(RVA = "0x10134D604", Offset = "0x134D604", VA = "0x10134D604")]
		private static int CompareSortProduct(StoreManager.Product A, StoreManager.Product B)
		{
			return 0;
		}

		// Token: 0x060023F1 RID: 9201 RVA: 0x0000F630 File Offset: 0x0000D830
		[Token(Token = "0x600212E")]
		[Address(RVA = "0x10134D664", Offset = "0x134D664", VA = "0x10134D664")]
		private static int CompareSortProductLimit(StoreManager.Product A, StoreManager.Product B)
		{
			return 0;
		}

		// Token: 0x060023F2 RID: 9202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600212F")]
		[Address(RVA = "0x10134D8FC", Offset = "0x134D8FC", VA = "0x10134D8FC")]
		public static void SortProduct(List<StoreManager.Product> list)
		{
		}

		// Token: 0x1400001C RID: 28
		// (add) Token: 0x060023F3 RID: 9203 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023F4 RID: 9204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001C")]
		private event Action<StoreManager.Product> m_OnCompletePurchase
		{
			[Token(Token = "0x6002130")]
			[Address(RVA = "0x10134D984", Offset = "0x134D984", VA = "0x10134D984")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002131")]
			[Address(RVA = "0x10134DA90", Offset = "0x134DA90", VA = "0x10134DA90")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400001D RID: 29
		// (add) Token: 0x060023F5 RID: 9205 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023F6 RID: 9206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001D")]
		private event Action m_OnPendingPurchase
		{
			[Token(Token = "0x6002132")]
			[Address(RVA = "0x10134DB9C", Offset = "0x134DB9C", VA = "0x10134DB9C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002133")]
			[Address(RVA = "0x10134DCA8", Offset = "0x134DCA8", VA = "0x10134DCA8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060023F7 RID: 9207 RVA: 0x0000F648 File Offset: 0x0000D848
		[Token(Token = "0x6002134")]
		[Address(RVA = "0x10134DDB4", Offset = "0x134DDB4", VA = "0x10134DDB4")]
		public bool Purchase(int id, Action<StoreManager.Product> onComplete, Action onPending)
		{
			return default(bool);
		}

		// Token: 0x060023F8 RID: 9208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002135")]
		[Address(RVA = "0x10134E130", Offset = "0x134E130", VA = "0x10134E130")]
		private void OnCompleteGetPayload(string payload)
		{
		}

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x060023F9 RID: 9209 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023FA RID: 9210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001E")]
		private event Action<string> m_OnCompleteGetPayload
		{
			[Token(Token = "0x6002136")]
			[Address(RVA = "0x10134E334", Offset = "0x134E334", VA = "0x10134E334")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002137")]
			[Address(RVA = "0x10134E440", Offset = "0x134E440", VA = "0x10134E440")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060023FB RID: 9211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002138")]
		[Address(RVA = "0x10134DF90", Offset = "0x134DF90", VA = "0x10134DF90")]
		private void Request_GetPayload(int id, Action<string> onComplete)
		{
		}

		// Token: 0x060023FC RID: 9212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002139")]
		[Address(RVA = "0x10134E54C", Offset = "0x134E54C", VA = "0x10134E54C")]
		private void OnResponse_GetPayload(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060023FD RID: 9213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600213A")]
		[Address(RVA = "0x10134E974", Offset = "0x134E974", VA = "0x10134E974")]
		private void OnCloseErrorWindow_OnResponse_GetPayload()
		{
		}

		// Token: 0x1400001F RID: 31
		// (add) Token: 0x060023FE RID: 9214 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060023FF RID: 9215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400001F")]
		private event Action m_OnCompletePendingPurchaseRegister
		{
			[Token(Token = "0x600213B")]
			[Address(RVA = "0x10134E9C8", Offset = "0x134E9C8", VA = "0x10134E9C8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600213C")]
			[Address(RVA = "0x10134EAD4", Offset = "0x134EAD4", VA = "0x10134EAD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002400 RID: 9216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600213D")]
		[Address(RVA = "0x10134A5A8", Offset = "0x134A5A8", VA = "0x10134A5A8")]
		private void Request_PendingPurchaseRegister(string bundleId, InAppPurchaser.PendingProduct product, Action callback)
		{
		}

		// Token: 0x06002401 RID: 9217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600213E")]
		[Address(RVA = "0x10134EBE0", Offset = "0x134EBE0", VA = "0x10134EBE0")]
		private void OnResponse_PendingPurchaseRegister(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000020 RID: 32
		// (add) Token: 0x06002402 RID: 9218 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002403 RID: 9219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000020")]
		private event Action<ResultCode> m_OnCompleteSendReceipt
		{
			[Token(Token = "0x600213F")]
			[Address(RVA = "0x10134ECD0", Offset = "0x134ECD0", VA = "0x10134ECD0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002140")]
			[Address(RVA = "0x10134EDDC", Offset = "0x134EDDC", VA = "0x10134EDDC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002404 RID: 9220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002141")]
		[Address(RVA = "0x10134A3E4", Offset = "0x134A3E4", VA = "0x10134A3E4")]
		private void Request_SendReceipt(string receipt, string signature, Action<ResultCode> onComplete)
		{
		}

		// Token: 0x06002405 RID: 9221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002142")]
		[Address(RVA = "0x10134EEE8", Offset = "0x134EEE8", VA = "0x10134EEE8")]
		private void OnResponse_SendReceipt(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000021 RID: 33
		// (add) Token: 0x06002406 RID: 9222 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002407 RID: 9223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000021")]
		private event Action m_OnCompleteGetPurchaseLog
		{
			[Token(Token = "0x6002143")]
			[Address(RVA = "0x10134F274", Offset = "0x134F274", VA = "0x10134F274")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002144")]
			[Address(RVA = "0x10134F380", Offset = "0x134F380", VA = "0x10134F380")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002408 RID: 9224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002145")]
		[Address(RVA = "0x10134F48C", Offset = "0x134F48C", VA = "0x10134F48C")]
		public void Request_PurchaseLog(Action onComplete)
		{
		}

		// Token: 0x06002409 RID: 9225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002146")]
		[Address(RVA = "0x10134F578", Offset = "0x134F578", VA = "0x10134F578")]
		private void OnResponse_GetPurchaseLog(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600240A RID: 9226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002147")]
		[Address(RVA = "0x10134F760", Offset = "0x134F760", VA = "0x10134F760")]
		private static void wwwConvert(PlayerPurchase src, StoreManager.PurchaseLog dest)
		{
		}

		// Token: 0x14000022 RID: 34
		// (add) Token: 0x0600240B RID: 9227 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600240C RID: 9228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000022")]
		private event Action m_OnCompleteProductSetShown
		{
			[Token(Token = "0x6002148")]
			[Address(RVA = "0x10134F808", Offset = "0x134F808", VA = "0x10134F808")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002149")]
			[Address(RVA = "0x10134F914", Offset = "0x134F914", VA = "0x10134F914")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000023 RID: 35
		// (add) Token: 0x0600240D RID: 9229 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600240E RID: 9230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000023")]
		private event Action m_OnCompleteDirectSaleSetShown
		{
			[Token(Token = "0x600214A")]
			[Address(RVA = "0x10134FA20", Offset = "0x134FA20", VA = "0x10134FA20")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600214B")]
			[Address(RVA = "0x10134FB2C", Offset = "0x134FB2C", VA = "0x10134FB2C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000024 RID: 36
		// (add) Token: 0x0600240F RID: 9231 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002410 RID: 9232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000024")]
		private event Action m_OnCompleteExchangeShopSetShown
		{
			[Token(Token = "0x600214C")]
			[Address(RVA = "0x10134FC38", Offset = "0x134FC38", VA = "0x10134FC38")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600214D")]
			[Address(RVA = "0x10134FD44", Offset = "0x134FD44", VA = "0x10134FD44")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002411 RID: 9233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600214E")]
		[Address(RVA = "0x10134FE50", Offset = "0x134FE50", VA = "0x10134FE50")]
		private void UpdateIsNewAny()
		{
		}

		// Token: 0x06002412 RID: 9234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600214F")]
		[Address(RVA = "0x10134FF88", Offset = "0x134FF88", VA = "0x10134FF88")]
		public void Request_ProductSetShown(List<ShownProduct> shownList, Action onComplete)
		{
		}

		// Token: 0x06002413 RID: 9235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002150")]
		[Address(RVA = "0x1013501E8", Offset = "0x13501E8", VA = "0x1013501E8")]
		private void OnResponse_ProductSetShown(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002414 RID: 9236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002151")]
		[Address(RVA = "0x101350384", Offset = "0x1350384", VA = "0x101350384")]
		public void Request_DirectSaleSetShown(List<int> shownList, Action onComplete)
		{
		}

		// Token: 0x06002415 RID: 9237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002152")]
		[Address(RVA = "0x101350500", Offset = "0x1350500", VA = "0x101350500")]
		private void OnResponse_DirectSaleSetShown(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002416 RID: 9238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002153")]
		[Address(RVA = "0x101350600", Offset = "0x1350600", VA = "0x101350600")]
		public void Request_ExchangeShopSetShown(List<long> shownList, Action onComplete)
		{
		}

		// Token: 0x06002417 RID: 9239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002154")]
		[Address(RVA = "0x101350724", Offset = "0x1350724", VA = "0x101350724")]
		private void OnResponse_ExchangeShopSetShown(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000025 RID: 37
		// (add) Token: 0x06002418 RID: 9240 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06002419 RID: 9241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000025")]
		private event Action m_OnCompleteProductRemindSetShown
		{
			[Token(Token = "0x6002155")]
			[Address(RVA = "0x1013507B8", Offset = "0x13507B8", VA = "0x1013507B8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002156")]
			[Address(RVA = "0x1013508C4", Offset = "0x13508C4", VA = "0x1013508C4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600241A RID: 9242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002157")]
		[Address(RVA = "0x1013509D0", Offset = "0x13509D0", VA = "0x1013509D0")]
		public void Request_ProductRemindSetShown(int[] shownPremiumIds, Action onComplete)
		{
		}

		// Token: 0x0600241B RID: 9243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002158")]
		[Address(RVA = "0x101350AD0", Offset = "0x1350AD0", VA = "0x101350AD0")]
		private void OnResponse_ProductRemindSetShown(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000026 RID: 38
		// (add) Token: 0x0600241C RID: 9244 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600241D RID: 9245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000026")]
		private event Action<ResultCode> m_OnCompleteExchangeShopBuy_Success
		{
			[Token(Token = "0x6002159")]
			[Address(RVA = "0x101350BC0", Offset = "0x1350BC0", VA = "0x101350BC0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600215A")]
			[Address(RVA = "0x101350CCC", Offset = "0x1350CCC", VA = "0x101350CCC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000027 RID: 39
		// (add) Token: 0x0600241E RID: 9246 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600241F RID: 9247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000027")]
		private event Action<ResultCode> m_OnCompleteExchangeShopBuy_Failure
		{
			[Token(Token = "0x600215B")]
			[Address(RVA = "0x101350DD8", Offset = "0x1350DD8", VA = "0x101350DD8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600215C")]
			[Address(RVA = "0x101350EE4", Offset = "0x1350EE4", VA = "0x101350EE4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06002420 RID: 9248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600215D")]
		[Address(RVA = "0x101350FF0", Offset = "0x1350FF0", VA = "0x101350FF0")]
		public void Request_ExchangeShopBuy(long exchangeShopId, int buyAmount, Action<ResultCode> onCompleteSuccess, Action<ResultCode> onCompleteFailure)
		{
		}

		// Token: 0x06002421 RID: 9249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600215E")]
		[Address(RVA = "0x101351118", Offset = "0x1351118", VA = "0x101351118")]
		private void OnResponse_ExchangeShopBuy(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06002422 RID: 9250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600215F")]
		[Address(RVA = "0x10134A3E0", Offset = "0x134A3E0", VA = "0x10134A3E0")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100138DE4", Offset = "0x138DE4")]
		private static void LOG(object o)
		{
		}

		// Token: 0x06002423 RID: 9251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002160")]
		[Address(RVA = "0x10134A5A4", Offset = "0x134A5A4", VA = "0x10134A5A4")]
		[Attribute(Name = "ConditionalAttribute", RVA = "0x100138E1C", Offset = "0x138E1C")]
		private static void LOGF(string format, params object[] args)
		{
		}

		// Token: 0x06002424 RID: 9252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002161")]
		[Address(RVA = "0x1013512A4", Offset = "0x13512A4", VA = "0x1013512A4")]
		public StoreManager()
		{
		}

		// Token: 0x04003411 RID: 13329
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400264B")]
		private long m_MonthlyAmount;

		// Token: 0x04003412 RID: 13330
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400264C")]
		private long m_AvailableBalance;

		// Token: 0x04003413 RID: 13331
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400264D")]
		private bool m_IsNewAny;

		// Token: 0x04003414 RID: 13332
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400264E")]
		private List<StoreManager.Product> m_Products;

		// Token: 0x04003415 RID: 13333
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400264F")]
		private List<StoreManager.Product> m_ExchangeProducts;

		// Token: 0x04003416 RID: 13334
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002650")]
		private List<StoreManager.PurchaseLog> m_PurchaseLogs;

		// Token: 0x04003417 RID: 13335
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002651")]
		private StoreManager.eStep m_Step;

		// Token: 0x04003418 RID: 13336
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002652")]
		private int m_RestoreIndex;

		// Token: 0x04003419 RID: 13337
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002653")]
		private InAppPurchaser m_InAppPurchaser;

		// Token: 0x0400341A RID: 13338
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002654")]
		private int[] m_PrefabIndex;

		// Token: 0x0400341C RID: 13340
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002656")]
		private eAgeType m_RequestedAgeType;

		// Token: 0x0400341E RID: 13342
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002658")]
		private bool m_InitializeIAP;

		// Token: 0x0400341F RID: 13343
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002659")]
		private StoreManager.Product m_RequestedProduct;

		// Token: 0x04003426 RID: 13350
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002660")]
		private List<int> m_WorkProductSetShownIDs;

		// Token: 0x020008A7 RID: 2215
		[Token(Token = "0x2000F18")]
		public class Product
		{
			// Token: 0x17000271 RID: 625
			// (get) Token: 0x06002425 RID: 9253 RVA: 0x0000F660 File Offset: 0x0000D860
			[Token(Token = "0x1700069B")]
			public bool IsLimitedSale
			{
				[Token(Token = "0x6005F81")]
				[Address(RVA = "0x101349210", Offset = "0x1349210", VA = "0x101349210")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x06002426 RID: 9254 RVA: 0x0000F678 File Offset: 0x0000D878
			[Token(Token = "0x6005F82")]
			[Address(RVA = "0x1013492CC", Offset = "0x13492CC", VA = "0x1013492CC")]
			public int GetRemainCount()
			{
				return 0;
			}

			// Token: 0x06002427 RID: 9255 RVA: 0x0000F690 File Offset: 0x0000D890
			[Token(Token = "0x6005F83")]
			[Address(RVA = "0x10135148C", Offset = "0x135148C", VA = "0x10135148C")]
			public bool ClientRestock()
			{
				return default(bool);
			}

			// Token: 0x06002428 RID: 9256 RVA: 0x0000F6A8 File Offset: 0x0000D8A8
			[Token(Token = "0x6005F84")]
			[Address(RVA = "0x101351794", Offset = "0x1351794", VA = "0x101351794")]
			private bool IsExpiredNextRestockAt(StoreDefine.eRestockType restockType, DateTime restockAt, DateTime dispEndAt)
			{
				return default(bool);
			}

			// Token: 0x06002429 RID: 9257 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F85")]
			[Address(RVA = "0x10134BDA4", Offset = "0x134BDA4", VA = "0x10134BDA4")]
			public Product()
			{
			}

			// Token: 0x0400342D RID: 13357
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006139")]
			public int m_ID;

			// Token: 0x0400342E RID: 13358
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400613A")]
			public string m_ProductID;

			// Token: 0x0400342F RID: 13359
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400613B")]
			public bool m_IsNew;

			// Token: 0x04003430 RID: 13360
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400613C")]
			public StoreDefine.eProductType m_ProductType;

			// Token: 0x04003431 RID: 13361
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400613D")]
			public string m_Name;

			// Token: 0x04003432 RID: 13362
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400613E")]
			public string m_Desc;

			// Token: 0x04003433 RID: 13363
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400613F")]
			public long m_Price;

			// Token: 0x04003434 RID: 13364
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006140")]
			public DateTime? m_DispStartAt;

			// Token: 0x04003435 RID: 13365
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006141")]
			public DateTime? m_DispEndAt;

			// Token: 0x04003436 RID: 13366
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4006142")]
			public StoreManager.PremiumData m_Premium;

			// Token: 0x04003437 RID: 13367
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4006143")]
			public StoreManager.PackageData m_Package;

			// Token: 0x04003438 RID: 13368
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4006144")]
			public StoreManager.DirectSale m_DirectSale;

			// Token: 0x04003439 RID: 13369
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x4006145")]
			public StoreManager.Exchange m_Exchange;

			// Token: 0x0400343A RID: 13370
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x4006146")]
			public long m_UnlimitedGem;

			// Token: 0x0400343B RID: 13371
			[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
			[Token(Token = "0x4006147")]
			public long m_LimitedGem;

			// Token: 0x0400343C RID: 13372
			[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
			[Token(Token = "0x4006148")]
			public int m_Limit;

			// Token: 0x0400343D RID: 13373
			[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
			[Token(Token = "0x4006149")]
			public int m_PurchaseCount;

			// Token: 0x0400343E RID: 13374
			[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
			[Token(Token = "0x400614A")]
			public int m_LinkID;

			// Token: 0x0400343F RID: 13375
			[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
			[Token(Token = "0x400614B")]
			public int m_uiPriority;

			// Token: 0x04003440 RID: 13376
			[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
			[Token(Token = "0x400614C")]
			public StoreDefine.eUIType m_uiType;

			// Token: 0x04003441 RID: 13377
			[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
			[Token(Token = "0x400614D")]
			public int m_PrefabIndex;
		}

		// Token: 0x020008A8 RID: 2216
		[Token(Token = "0x2000F19")]
		public class PremiumData
		{
			// Token: 0x17000272 RID: 626
			// (get) Token: 0x0600242A RID: 9258 RVA: 0x0000F6C0 File Offset: 0x0000D8C0
			[Token(Token = "0x1700069C")]
			public bool IsAvailable
			{
				[Token(Token = "0x6005F86")]
				[Address(RVA = "0x10134927C", Offset = "0x134927C", VA = "0x10134927C")]
				get
				{
					return default(bool);
				}
			}

			// Token: 0x0600242B RID: 9259 RVA: 0x0000F6D8 File Offset: 0x0000D8D8
			[Token(Token = "0x6005F87")]
			[Address(RVA = "0x10134D790", Offset = "0x134D790", VA = "0x10134D790")]
			public bool IsReleased()
			{
				return default(bool);
			}

			// Token: 0x0600242C RID: 9260 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F88")]
			[Address(RVA = "0x10134D4DC", Offset = "0x134D4DC", VA = "0x10134D4DC")]
			public PremiumData()
			{
			}

			// Token: 0x04003442 RID: 13378
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400614E")]
			public int m_ID;

			// Token: 0x04003443 RID: 13379
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400614F")]
			public string m_Name;

			// Token: 0x04003444 RID: 13380
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006150")]
			public string m_BgName;

			// Token: 0x04003445 RID: 13381
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006151")]
			public string m_BannerName;

			// Token: 0x04003446 RID: 13382
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006152")]
			public string m_HelpBannerName;

			// Token: 0x04003447 RID: 13383
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006153")]
			public int m_ExpiredDay;

			// Token: 0x04003448 RID: 13384
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4006154")]
			public int m_DailyGem;

			// Token: 0x04003449 RID: 13385
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006155")]
			public int m_ProductGem;

			// Token: 0x0400344A RID: 13386
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x4006156")]
			public int m_ProductLimitedGem;

			// Token: 0x0400344B RID: 13387
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006157")]
			public StoreManager.PremiumReward[] m_RewardItem;

			// Token: 0x0400344C RID: 13388
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006158")]
			public DateTime? m_ExpirationDate;

			// Token: 0x0400344D RID: 13389
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4006159")]
			public DateTime? m_ReloadDate;

			// Token: 0x0400344E RID: 13390
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x400615A")]
			public int m_OverlapDay;

			// Token: 0x0400344F RID: 13391
			[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
			[Token(Token = "0x400615B")]
			public int m_PassType;

			// Token: 0x04003450 RID: 13392
			[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
			[Token(Token = "0x400615C")]
			public StoreDefine.eRemindType m_RemindType;

			// Token: 0x04003451 RID: 13393
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x400615D")]
			public string m_Summary;

			// Token: 0x04003452 RID: 13394
			[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
			[Token(Token = "0x400615E")]
			public string m_Notice;
		}

		// Token: 0x020008A9 RID: 2217
		[Token(Token = "0x2000F1A")]
		public class PremiumReward
		{
			// Token: 0x0600242D RID: 9261 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F89")]
			[Address(RVA = "0x10134D5EC", Offset = "0x134D5EC", VA = "0x10134D5EC")]
			public PremiumReward()
			{
			}

			// Token: 0x04003453 RID: 13395
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400615F")]
			public ePresentType m_ContentType;

			// Token: 0x04003454 RID: 13396
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006160")]
			public int m_ContentID;

			// Token: 0x04003455 RID: 13397
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006161")]
			public int m_ContentAmount;

			// Token: 0x04003456 RID: 13398
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006162")]
			public StoreDefine.eGrantType m_GrantType;
		}

		// Token: 0x020008AA RID: 2218
		[Token(Token = "0x2000F1B")]
		public class PackageData
		{
			// Token: 0x0600242E RID: 9262 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F8A")]
			[Address(RVA = "0x10134D4CC", Offset = "0x134D4CC", VA = "0x10134D4CC")]
			public PackageData()
			{
			}

			// Token: 0x04003457 RID: 13399
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006163")]
			public StoreManager.StoreReward[] m_StoreRewards;
		}

		// Token: 0x020008AB RID: 2219
		[Token(Token = "0x2000F1C")]
		public class StoreReward
		{
			// Token: 0x0600242F RID: 9263 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F8B")]
			[Address(RVA = "0x10134D4D4", Offset = "0x134D4D4", VA = "0x10134D4D4")]
			public StoreReward()
			{
			}

			// Token: 0x04003458 RID: 13400
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006164")]
			public int m_ContentType;

			// Token: 0x04003459 RID: 13401
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006165")]
			public int m_ContentID;

			// Token: 0x0400345A RID: 13402
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006166")]
			public int m_ContentAmount;

			// Token: 0x0400345B RID: 13403
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006167")]
			public DateTime? m_ExpiredAt;
		}

		// Token: 0x020008AC RID: 2220
		[Token(Token = "0x2000F1D")]
		public class DirectSale
		{
			// Token: 0x06002430 RID: 9264 RVA: 0x0000F6F0 File Offset: 0x0000D8F0
			[Token(Token = "0x6005F8C")]
			[Address(RVA = "0x101351440", Offset = "0x1351440", VA = "0x101351440")]
			public bool IsNew()
			{
				return default(bool);
			}

			// Token: 0x06002431 RID: 9265 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F8D")]
			[Address(RVA = "0x101351450", Offset = "0x1351450", VA = "0x101351450")]
			public void ClientRestock()
			{
			}

			// Token: 0x06002432 RID: 9266 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F8E")]
			[Address(RVA = "0x10134D5F4", Offset = "0x134D5F4", VA = "0x10134D5F4")]
			public DirectSale()
			{
			}

			// Token: 0x0400345C RID: 13404
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006168")]
			public long m_ID;

			// Token: 0x0400345D RID: 13405
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006169")]
			public StoreDefine.eRestockType m_RestockType;

			// Token: 0x0400345E RID: 13406
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x400616A")]
			public int m_StockMax;

			// Token: 0x0400345F RID: 13407
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400616B")]
			public StoreManager.StoreReward m_Reward;

			// Token: 0x04003460 RID: 13408
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400616C")]
			public string m_DetailText;

			// Token: 0x04003461 RID: 13409
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400616D")]
			public StoreDefine.eBadgeType m_BadgeType;

			// Token: 0x04003462 RID: 13410
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400616E")]
			public DateTime? m_RestockAt;
		}

		// Token: 0x020008AD RID: 2221
		[Token(Token = "0x2000F1E")]
		public class Exchange
		{
			// Token: 0x06002433 RID: 9267 RVA: 0x0000F708 File Offset: 0x0000D908
			[Token(Token = "0x6005F8F")]
			[Address(RVA = "0x101351464", Offset = "0x1351464", VA = "0x101351464")]
			public bool IsNew()
			{
				return default(bool);
			}

			// Token: 0x06002434 RID: 9268 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F90")]
			[Address(RVA = "0x101351474", Offset = "0x1351474", VA = "0x101351474")]
			public void ClientRestock()
			{
			}

			// Token: 0x06002435 RID: 9269 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F91")]
			[Address(RVA = "0x10134D5FC", Offset = "0x134D5FC", VA = "0x10134D5FC")]
			public Exchange()
			{
			}

			// Token: 0x04003463 RID: 13411
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400616F")]
			public long m_ID;

			// Token: 0x04003464 RID: 13412
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006170")]
			public DateTime? m_StartAt;

			// Token: 0x04003465 RID: 13413
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006171")]
			public DateTime? m_EndAt;

			// Token: 0x04003466 RID: 13414
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006172")]
			public StoreDefine.eRestockType m_RestockType;

			// Token: 0x04003467 RID: 13415
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4006173")]
			public int m_StockMax;

			// Token: 0x04003468 RID: 13416
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006174")]
			public int m_uiPriority;

			// Token: 0x04003469 RID: 13417
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006175")]
			public StoreManager.StoreReward m_Src;

			// Token: 0x0400346A RID: 13418
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006176")]
			public StoreManager.StoreReward m_Dest;

			// Token: 0x0400346B RID: 13419
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4006177")]
			public string m_Description;

			// Token: 0x0400346C RID: 13420
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4006178")]
			public string m_DetailText;

			// Token: 0x0400346D RID: 13421
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4006179")]
			public int m_BuyCount;

			// Token: 0x0400346E RID: 13422
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x400617A")]
			public DateTime? m_RestockAt;

			// Token: 0x0400346F RID: 13423
			[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
			[Token(Token = "0x400617B")]
			public StoreDefine.eBadgeType m_BadgeType;
		}

		// Token: 0x020008AE RID: 2222
		[Token(Token = "0x2000F1F")]
		public class PurchaseLog
		{
			// Token: 0x06002436 RID: 9270 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F92")]
			[Address(RVA = "0x10134F758", Offset = "0x134F758", VA = "0x10134F758")]
			public PurchaseLog()
			{
			}

			// Token: 0x04003470 RID: 13424
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400617C")]
			public string m_Name;

			// Token: 0x04003471 RID: 13425
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400617D")]
			public long m_Price;

			// Token: 0x04003472 RID: 13426
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400617E")]
			public DateTime m_Date;

			// Token: 0x04003473 RID: 13427
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400617F")]
			public DateTime? m_ExpirationAt;
		}

		// Token: 0x020008AF RID: 2223
		[Token(Token = "0x2000F20")]
		private enum eStep
		{
			// Token: 0x04003475 RID: 13429
			[Token(Token = "0x4006181")]
			None = -1,
			// Token: 0x04003476 RID: 13430
			[Token(Token = "0x4006182")]
			InitializeProducts_Wait,
			// Token: 0x04003477 RID: 13431
			[Token(Token = "0x4006183")]
			RestoreFirst,
			// Token: 0x04003478 RID: 13432
			[Token(Token = "0x4006184")]
			RestoreAPI,
			// Token: 0x04003479 RID: 13433
			[Token(Token = "0x4006185")]
			RestoreAPI_Wait,
			// Token: 0x0400347A RID: 13434
			[Token(Token = "0x4006186")]
			RestoreFinish,
			// Token: 0x0400347B RID: 13435
			[Token(Token = "0x4006187")]
			PurchaseIAP_Wait,
			// Token: 0x0400347C RID: 13436
			[Token(Token = "0x4006188")]
			PurchaseSendReceiptAPI_Wait,
			// Token: 0x0400347D RID: 13437
			[Token(Token = "0x4006189")]
			PendingPurchaseRegisterAPI_Wait
		}
	}
}
