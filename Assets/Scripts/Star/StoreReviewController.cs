﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Window;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AF0 RID: 2800
	[Token(Token = "0x20007A7")]
	[StructLayout(3)]
	public class StoreReviewController : MonoBehaviour
	{
		// Token: 0x06003171 RID: 12657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D32")]
		[Address(RVA = "0x101351878", Offset = "0x1351878", VA = "0x101351878")]
		private void Update()
		{
		}

		// Token: 0x06003172 RID: 12658 RVA: 0x00015210 File Offset: 0x00013410
		[Token(Token = "0x6002D33")]
		[Address(RVA = "0x1013518D0", Offset = "0x13518D0", VA = "0x1013518D0")]
		public bool CheckConditionFull(int prevLv, int currentLv, int questID)
		{
			return default(bool);
		}

		// Token: 0x06003173 RID: 12659 RVA: 0x00015228 File Offset: 0x00013428
		[Token(Token = "0x6002D34")]
		[Address(RVA = "0x10135192C", Offset = "0x135192C", VA = "0x10135192C")]
		public bool CheckCondition_ReviewVersion()
		{
			return default(bool);
		}

		// Token: 0x06003174 RID: 12660 RVA: 0x00015240 File Offset: 0x00013440
		[Token(Token = "0x6002D35")]
		[Address(RVA = "0x1013519B8", Offset = "0x13519B8", VA = "0x1013519B8")]
		public bool CheckCondition_UserLv(int prevLv, int currentLv)
		{
			return default(bool);
		}

		// Token: 0x06003175 RID: 12661 RVA: 0x00015258 File Offset: 0x00013458
		[Token(Token = "0x6002D36")]
		[Address(RVA = "0x101351A94", Offset = "0x1351A94", VA = "0x101351A94")]
		public bool CheckCondition_Quest(int questID)
		{
			return default(bool);
		}

		// Token: 0x06003176 RID: 12662 RVA: 0x00015270 File Offset: 0x00013470
		[Token(Token = "0x6002D37")]
		[Address(RVA = "0x101351B34", Offset = "0x1351B34", VA = "0x101351B34")]
		public bool Review()
		{
			return default(bool);
		}

		// Token: 0x06003177 RID: 12663 RVA: 0x00015288 File Offset: 0x00013488
		[Token(Token = "0x6002D38")]
		[Address(RVA = "0x101351E14", Offset = "0x1351E14", VA = "0x101351E14")]
		public bool IsOpenWindow()
		{
			return default(bool);
		}

		// Token: 0x06003178 RID: 12664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D39")]
		[Address(RVA = "0x101351E1C", Offset = "0x1351E1C", VA = "0x101351E1C")]
		public void OnWindowYes()
		{
		}

		// Token: 0x06003179 RID: 12665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D3A")]
		[Address(RVA = "0x101351E88", Offset = "0x1351E88", VA = "0x101351E88")]
		public void OnWindowNo()
		{
		}

		// Token: 0x0600317A RID: 12666 RVA: 0x000152A0 File Offset: 0x000134A0
		[Token(Token = "0x6002D3B")]
		[Address(RVA = "0x101351BCC", Offset = "0x1351BCC", VA = "0x101351BCC")]
		private bool IsNativeCall()
		{
			return default(bool);
		}

		// Token: 0x0600317B RID: 12667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D3C")]
		[Address(RVA = "0x101351E68", Offset = "0x1351E68", VA = "0x101351E68")]
		private void OpenStoreReviewURL()
		{
		}

		// Token: 0x0600317C RID: 12668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D3D")]
		[Address(RVA = "0x101351D70", Offset = "0x1351D70", VA = "0x101351D70")]
		private void SaveStoreReviewAppVersion()
		{
		}

		// Token: 0x0600317D RID: 12669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D3E")]
		[Address(RVA = "0x101351EBC", Offset = "0x1351EBC", VA = "0x101351EBC")]
		public StoreReviewController()
		{
		}

		// Token: 0x040040FF RID: 16639
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E27")]
		[SerializeField]
		private CustomWindow m_Window;

		// Token: 0x04004100 RID: 16640
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E28")]
		private bool m_IsOpenWindow;

		// Token: 0x04004101 RID: 16641
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4002E29")]
		private bool m_IsNativeOpen;

		// Token: 0x04004102 RID: 16642
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002E2A")]
		private float m_WorkTimer;

		// Token: 0x04004103 RID: 16643
		[Token(Token = "0x4002E2B")]
		private const int STORE_REIVEW_ON = 1;
	}
}
