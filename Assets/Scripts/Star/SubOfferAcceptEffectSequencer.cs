﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200078A RID: 1930
	[Token(Token = "0x20005C8")]
	[StructLayout(3)]
	public class SubOfferAcceptEffectSequencer : OfferEffectSequencer
	{
		// Token: 0x06001D32 RID: 7474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB0")]
		[Address(RVA = "0x101351EC4", Offset = "0x1351EC4", VA = "0x101351EC4")]
		public void Setup(ContentRoomState_Main ownerState, ContentRoomUI ui, long offerMngId)
		{
		}

		// Token: 0x06001D33 RID: 7475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB1")]
		[Address(RVA = "0x101351F1C", Offset = "0x1351F1C", VA = "0x101351F1C", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x0000D098 File Offset: 0x0000B298
		[Token(Token = "0x6001AB2")]
		[Address(RVA = "0x101351F2C", Offset = "0x1351F2C", VA = "0x101351F2C", Slot = "5")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB3")]
		[Address(RVA = "0x10135246C", Offset = "0x135246C", VA = "0x10135246C")]
		private void OnFinishFlavor()
		{
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB4")]
		[Address(RVA = "0x101352478", Offset = "0x1352478", VA = "0x101352478")]
		public SubOfferAcceptEffectSequencer()
		{
		}

		// Token: 0x04002D7A RID: 11642
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40023A5")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04002D7B RID: 11643
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40023A6")]
		private ContentRoomUI m_UI;

		// Token: 0x04002D7C RID: 11644
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023A7")]
		private SubOfferAcceptEffectSequencer.eStep m_Step;

		// Token: 0x04002D7D RID: 11645
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023A8")]
		private OfferManager.OfferData m_OfferData;

		// Token: 0x04002D7E RID: 11646
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023A9")]
		private FlavorController m_FlavorController;

		// Token: 0x04002D7F RID: 11647
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023AA")]
		private bool m_FinishFlavor;

		// Token: 0x0200078B RID: 1931
		[Token(Token = "0x2000E93")]
		private enum eStep
		{
			// Token: 0x04002D81 RID: 11649
			[Token(Token = "0x4005D4C")]
			None,
			// Token: 0x04002D82 RID: 11650
			[Token(Token = "0x4005D4D")]
			StartCloseUI_SubOfferDetailWindow,
			// Token: 0x04002D83 RID: 11651
			[Token(Token = "0x4005D4E")]
			WaitCloseUI_SubOfferDetailWindow,
			// Token: 0x04002D84 RID: 11652
			[Token(Token = "0x4005D4F")]
			StartCloseUI_SubOfferWindow,
			// Token: 0x04002D85 RID: 11653
			[Token(Token = "0x4005D50")]
			WaitCloseUI_SubOfferWindow,
			// Token: 0x04002D86 RID: 11654
			[Token(Token = "0x4005D51")]
			PlayCutIn,
			// Token: 0x04002D87 RID: 11655
			[Token(Token = "0x4005D52")]
			WaitCutIn,
			// Token: 0x04002D88 RID: 11656
			[Token(Token = "0x4005D53")]
			StartOpenUI,
			// Token: 0x04002D89 RID: 11657
			[Token(Token = "0x4005D54")]
			WaitOpenUI,
			// Token: 0x04002D8A RID: 11658
			[Token(Token = "0x4005D55")]
			OpenPopup,
			// Token: 0x04002D8B RID: 11659
			[Token(Token = "0x4005D56")]
			End
		}
	}
}
