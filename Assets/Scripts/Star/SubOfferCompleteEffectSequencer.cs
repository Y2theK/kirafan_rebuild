﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI;

namespace Star
{
	// Token: 0x0200078C RID: 1932
	[Token(Token = "0x20005C9")]
	[StructLayout(3)]
	public class SubOfferCompleteEffectSequencer : OfferEffectSequencer
	{
		// Token: 0x06001D37 RID: 7479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB5")]
		[Address(RVA = "0x101352480", Offset = "0x1352480", VA = "0x101352480")]
		public void Setup(ContentRoomState_Main ownerState, ContentRoomUI ui, long offerMngId)
		{
		}

		// Token: 0x06001D38 RID: 7480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB6")]
		[Address(RVA = "0x1013524D8", Offset = "0x13524D8", VA = "0x1013524D8", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x06001D39 RID: 7481 RVA: 0x0000D0B0 File Offset: 0x0000B2B0
		[Token(Token = "0x6001AB7")]
		[Address(RVA = "0x1013524E8", Offset = "0x13524E8", VA = "0x1013524E8", Slot = "5")]
		public override bool Update()
		{
			return default(bool);
		}

		// Token: 0x06001D3A RID: 7482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB8")]
		[Address(RVA = "0x101352880", Offset = "0x1352880", VA = "0x101352880")]
		private void OnFinishFlavor()
		{
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001AB9")]
		[Address(RVA = "0x10135288C", Offset = "0x135288C", VA = "0x10135288C")]
		public SubOfferCompleteEffectSequencer()
		{
		}

		// Token: 0x04002D8C RID: 11660
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40023AB")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04002D8D RID: 11661
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40023AC")]
		private ContentRoomUI m_UI;

		// Token: 0x04002D8E RID: 11662
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40023AD")]
		private SubOfferCompleteEffectSequencer.eStep m_Step;

		// Token: 0x04002D8F RID: 11663
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40023AE")]
		private OfferManager.OfferData m_OfferData;

		// Token: 0x04002D90 RID: 11664
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40023AF")]
		private FlavorController m_FlavorController;

		// Token: 0x04002D91 RID: 11665
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40023B0")]
		private bool m_FinishFlavor;

		// Token: 0x0200078D RID: 1933
		[Token(Token = "0x2000E94")]
		private enum eStep
		{
			// Token: 0x04002D93 RID: 11667
			[Token(Token = "0x4005D58")]
			None,
			// Token: 0x04002D94 RID: 11668
			[Token(Token = "0x4005D59")]
			StartCloseUI_SubOfferDetailWindow,
			// Token: 0x04002D95 RID: 11669
			[Token(Token = "0x4005D5A")]
			WaitCloseUI_SubOfferDetailWindow,
			// Token: 0x04002D96 RID: 11670
			[Token(Token = "0x4005D5B")]
			StartCloseUI_SubOfferWindow,
			// Token: 0x04002D97 RID: 11671
			[Token(Token = "0x4005D5C")]
			WaitCloseUI_SubOfferWindow,
			// Token: 0x04002D98 RID: 11672
			[Token(Token = "0x4005D5D")]
			PlayCutIn,
			// Token: 0x04002D99 RID: 11673
			[Token(Token = "0x4005D5E")]
			WaitCutIn,
			// Token: 0x04002D9A RID: 11674
			[Token(Token = "0x4005D5F")]
			StartOpenUI,
			// Token: 0x04002D9B RID: 11675
			[Token(Token = "0x4005D60")]
			WaitOpenUI,
			// Token: 0x04002D9C RID: 11676
			[Token(Token = "0x4005D61")]
			OpenPopup,
			// Token: 0x04002D9D RID: 11677
			[Token(Token = "0x4005D62")]
			End
		}
	}
}
