﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000706 RID: 1798
	[Token(Token = "0x2000595")]
	[StructLayout(3)]
	public class SystemFileWriteManager : MonoBehaviour
	{
		// Token: 0x06001A4B RID: 6731 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001853")]
		[Address(RVA = "0x101352894", Offset = "0x1352894", VA = "0x101352894")]
		public static SystemFileWriteManager GetManager()
		{
			return null;
		}

		// Token: 0x06001A4C RID: 6732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001854")]
		[Address(RVA = "0x1013529A4", Offset = "0x13529A4", VA = "0x1013529A4")]
		public void EntryFileWrite(string pfilename, byte[] pdata)
		{
		}

		// Token: 0x06001A4D RID: 6733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001855")]
		[Address(RVA = "0x101352A64", Offset = "0x1352A64", VA = "0x101352A64")]
		private void Update()
		{
		}

		// Token: 0x06001A4E RID: 6734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001856")]
		[Address(RVA = "0x101352CB4", Offset = "0x1352CB4", VA = "0x101352CB4")]
		public SystemFileWriteManager()
		{
		}

		// Token: 0x04002A84 RID: 10884
		[Token(Token = "0x40022B1")]
		public static SystemFileWriteManager Inst;

		// Token: 0x04002A85 RID: 10885
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022B2")]
		private List<SystemFileWriteManager.FileWriteInfo> m_List;

		// Token: 0x02000707 RID: 1799
		[Token(Token = "0x2000E43")]
		public class FileWriteInfo
		{
			// Token: 0x06001A4F RID: 6735 RVA: 0x0000BD00 File Offset: 0x00009F00
			[Token(Token = "0x6005EBA")]
			[Address(RVA = "0x101352B8C", Offset = "0x1352B8C", VA = "0x101352B8C")]
			public bool UpFunc()
			{
				return default(bool);
			}

			// Token: 0x06001A50 RID: 6736 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EBB")]
			[Address(RVA = "0x101352D24", Offset = "0x1352D24", VA = "0x101352D24")]
			private void CallbackWriteEnd(IAsyncResult pres)
			{
			}

			// Token: 0x06001A51 RID: 6737 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EBC")]
			[Address(RVA = "0x101352A5C", Offset = "0x1352A5C", VA = "0x101352A5C")]
			public FileWriteInfo()
			{
			}

			// Token: 0x04002A86 RID: 10886
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B4C")]
			public string m_Filename;

			// Token: 0x04002A87 RID: 10887
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B4D")]
			public byte[] m_Data;

			// Token: 0x04002A88 RID: 10888
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005B4E")]
			public byte m_Step;

			// Token: 0x04002A89 RID: 10889
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4005B4F")]
			public FileStream m_File;
		}
	}
}
