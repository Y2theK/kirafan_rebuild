﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000708 RID: 1800
	[Token(Token = "0x2000596")]
	[StructLayout(3)]
	public class SystemScheduleLinkManager
	{
		// Token: 0x06001A52 RID: 6738 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001857")]
		[Address(RVA = "0x101352DAC", Offset = "0x1352DAC", VA = "0x101352DAC")]
		public static SystemScheduleLinkManager GetManager()
		{
			return null;
		}

		// Token: 0x06001A53 RID: 6739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001858")]
		[Address(RVA = "0x101352F00", Offset = "0x1352F00", VA = "0x101352F00")]
		private void CreateCashFileName()
		{
		}

		// Token: 0x06001A54 RID: 6740 RVA: 0x0000BD18 File Offset: 0x00009F18
		[Token(Token = "0x6001859")]
		[Address(RVA = "0x101353138", Offset = "0x1353138", VA = "0x101353138")]
		public static long GetChkUpPlayTime()
		{
			return 0L;
		}

		// Token: 0x06001A55 RID: 6741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600185A")]
		[Address(RVA = "0x1013531F8", Offset = "0x13531F8", VA = "0x1013531F8")]
		public static void ChkUpPlayTime(long fsettingtime)
		{
		}

		// Token: 0x06001A56 RID: 6742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600185B")]
		[Address(RVA = "0x101352FE8", Offset = "0x1352FE8", VA = "0x101352FE8")]
		public void ReadBackUpFile()
		{
		}

		// Token: 0x06001A57 RID: 6743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600185C")]
		[Address(RVA = "0x1013535EC", Offset = "0x13535EC", VA = "0x1013535EC")]
		private void ReadCustomCashFile(BinaryReader preader)
		{
		}

		// Token: 0x06001A58 RID: 6744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600185D")]
		[Address(RVA = "0x1013533A8", Offset = "0x13533A8", VA = "0x1013533A8")]
		public void SaveBackUpFile()
		{
		}

		// Token: 0x06001A59 RID: 6745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600185E")]
		[Address(RVA = "0x101353A94", Offset = "0x1353A94", VA = "0x101353A94")]
		private void WriteCustomCashFile(BinaryWrite pwriter)
		{
		}

		// Token: 0x06001A5A RID: 6746 RVA: 0x0000BD30 File Offset: 0x00009F30
		[Token(Token = "0x600185F")]
		[Address(RVA = "0x101353E7C", Offset = "0x1353E7C", VA = "0x101353E7C")]
		public bool IsReserveChara(long fmanageid)
		{
			return default(bool);
		}

		// Token: 0x06001A5B RID: 6747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001860")]
		[Address(RVA = "0x101353F70", Offset = "0x1353F70", VA = "0x101353F70")]
		public void SaveReserveChara(long fmanageid)
		{
		}

		// Token: 0x06001A5C RID: 6748 RVA: 0x0000BD48 File Offset: 0x00009F48
		[Token(Token = "0x6001861")]
		[Address(RVA = "0x1013540B0", Offset = "0x13540B0", VA = "0x1013540B0")]
		public bool IsActionChara(int factionkey)
		{
			return default(bool);
		}

		// Token: 0x06001A5D RID: 6749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001862")]
		[Address(RVA = "0x1013541A4", Offset = "0x13541A4", VA = "0x1013541A4")]
		public void SaveActionChara(long fmanageid, int factionkey)
		{
		}

		// Token: 0x06001A5E RID: 6750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001863")]
		[Address(RVA = "0x101353324", Offset = "0x1353324", VA = "0x101353324")]
		private void ClearReserveSave()
		{
		}

		// Token: 0x06001A5F RID: 6751 RVA: 0x0000BD60 File Offset: 0x00009F60
		[Token(Token = "0x6001864")]
		[Address(RVA = "0x101354344", Offset = "0x1354344", VA = "0x101354344")]
		public int GetReserveIndex()
		{
			return 0;
		}

		// Token: 0x06001A60 RID: 6752 RVA: 0x0000BD78 File Offset: 0x00009F78
		[Token(Token = "0x6001865")]
		[Address(RVA = "0x10135434C", Offset = "0x135434C", VA = "0x10135434C")]
		public int GetReserveSubIndex()
		{
			return 0;
		}

		// Token: 0x06001A61 RID: 6753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001866")]
		[Address(RVA = "0x101354354", Offset = "0x1354354", VA = "0x101354354")]
		public void SetReserveIndex(int findex, int fsubindex)
		{
		}

		// Token: 0x06001A62 RID: 6754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001867")]
		[Address(RVA = "0x101352E68", Offset = "0x1352E68", VA = "0x101352E68")]
		public SystemScheduleLinkManager()
		{
		}

		// Token: 0x04002A8A RID: 10890
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40022B3")]
		public List<SystemScheduleLinkManager.RoomReserveChara> m_ReserveTable;

		// Token: 0x04002A8B RID: 10891
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40022B4")]
		public int m_ReserveIndexCount;

		// Token: 0x04002A8C RID: 10892
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40022B5")]
		public int m_ReserveSubIndexCount;

		// Token: 0x04002A8D RID: 10893
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40022B6")]
		public string m_CashFileName;

		// Token: 0x04002A8E RID: 10894
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40022B7")]
		public List<SystemScheduleLinkManager.RoomActionChara> m_RoomActionKey;

		// Token: 0x04002A8F RID: 10895
		[Token(Token = "0x40022B8")]
		public static SystemScheduleLinkManager Inst;

		// Token: 0x02000709 RID: 1801
		[Token(Token = "0x2000E44")]
		public class RoomReserveChara
		{
			// Token: 0x06001A63 RID: 6755 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EBD")]
			[Address(RVA = "0x1013540A8", Offset = "0x13540A8", VA = "0x1013540A8")]
			public RoomReserveChara()
			{
			}

			// Token: 0x04002A90 RID: 10896
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B50")]
			public long m_ManageID;
		}

		// Token: 0x0200070A RID: 1802
		[Token(Token = "0x2000E45")]
		public enum eSaveTagKey
		{
			// Token: 0x04002A92 RID: 10898
			[Token(Token = "0x4005B52")]
			End,
			// Token: 0x04002A93 RID: 10899
			[Token(Token = "0x4005B53")]
			TimeKey,
			// Token: 0x04002A94 RID: 10900
			[Token(Token = "0x4005B54")]
			ActionTag
		}

		// Token: 0x0200070B RID: 1803
		[Token(Token = "0x2000E46")]
		public class RoomActionChara
		{
			// Token: 0x06001A64 RID: 6756 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EBE")]
			[Address(RVA = "0x1013539EC", Offset = "0x13539EC", VA = "0x1013539EC")]
			public void Serialize(BinaryIO pio)
			{
			}

			// Token: 0x06001A65 RID: 6757 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EBF")]
			[Address(RVA = "0x1013539E4", Offset = "0x13539E4", VA = "0x1013539E4")]
			public RoomActionChara()
			{
			}

			// Token: 0x04002A95 RID: 10901
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B55")]
			public long m_ManageID;

			// Token: 0x04002A96 RID: 10902
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B56")]
			public int m_ActionKey;

			// Token: 0x04002A97 RID: 10903
			[Token(Token = "0x4005B57")]
			public const int VERSION = 0;
		}

		// Token: 0x0200070C RID: 1804
		[Token(Token = "0x2000E47")]
		public class FileHeader
		{
			// Token: 0x06001A66 RID: 6758 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC0")]
			[Address(RVA = "0x101353E6C", Offset = "0x1353E6C", VA = "0x101353E6C")]
			public void SetTagKey(SystemScheduleLinkManager.eSaveTagKey fkey, int fsize, byte flags)
			{
			}

			// Token: 0x06001A67 RID: 6759 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC1")]
			[Address(RVA = "0x10135385C", Offset = "0x135385C", VA = "0x10135385C")]
			public void Serialize(BinaryIO pio)
			{
			}

			// Token: 0x06001A68 RID: 6760 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC2")]
			[Address(RVA = "0x101353854", Offset = "0x1353854", VA = "0x101353854")]
			public FileHeader()
			{
			}

			// Token: 0x04002A98 RID: 10904
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B58")]
			public int m_Size;

			// Token: 0x04002A99 RID: 10905
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B59")]
			public short m_Key;

			// Token: 0x04002A9A RID: 10906
			[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
			[Token(Token = "0x4005B5A")]
			public byte m_Flag;

			// Token: 0x04002A9B RID: 10907
			[Cpp2IlInjected.FieldOffset(Offset = "0x17")]
			[Token(Token = "0x4005B5B")]
			public byte m_Ver;
		}

		// Token: 0x0200070D RID: 1805
		[Token(Token = "0x2000E48")]
		public class FileCliperHeader
		{
			// Token: 0x06001A69 RID: 6761 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC3")]
			[Address(RVA = "0x1013534B8", Offset = "0x13534B8", VA = "0x1013534B8")]
			public void Serialize(BinaryIO pio)
			{
			}

			// Token: 0x06001A6A RID: 6762 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC4")]
			[Address(RVA = "0x101353D38", Offset = "0x1353D38", VA = "0x101353D38")]
			public void MakeCliper(BinaryWrite pio)
			{
			}

			// Token: 0x06001A6B RID: 6763 RVA: 0x0000BD90 File Offset: 0x00009F90
			[Token(Token = "0x6005EC5")]
			[Address(RVA = "0x10135359C", Offset = "0x135359C", VA = "0x10135359C")]
			public bool CliperChk(BinaryReader pio)
			{
				return default(bool);
			}

			// Token: 0x06001A6C RID: 6764 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC6")]
			[Address(RVA = "0x1013534B0", Offset = "0x13534B0", VA = "0x1013534B0")]
			public FileCliperHeader()
			{
			}

			// Token: 0x04002A9C RID: 10908
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B5C")]
			public int m_Size;

			// Token: 0x04002A9D RID: 10909
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005B5D")]
			public ushort m_KeyCode;

			// Token: 0x04002A9E RID: 10910
			[Cpp2IlInjected.FieldOffset(Offset = "0x16")]
			[Token(Token = "0x4005B5E")]
			public ushort m_ChkSum;

			// Token: 0x04002A9F RID: 10911
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005B5F")]
			public byte[] m_Data;
		}

		// Token: 0x0200070E RID: 1806
		[Token(Token = "0x2000E49")]
		public class SystemTimeKey
		{
			// Token: 0x06001A6D RID: 6765 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC7")]
			[Address(RVA = "0x101353974", Offset = "0x1353974", VA = "0x101353974")]
			public void Serialize(BinaryIO pio)
			{
			}

			// Token: 0x06001A6E RID: 6766 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005EC8")]
			[Address(RVA = "0x10135396C", Offset = "0x135396C", VA = "0x10135396C")]
			public SystemTimeKey()
			{
			}

			// Token: 0x04002AA0 RID: 10912
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005B60")]
			public long m_Times;
		}
	}
}
