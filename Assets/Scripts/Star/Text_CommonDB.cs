﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005E6 RID: 1510
	[Token(Token = "0x20004D9")]
	[StructLayout(3)]
	public class Text_CommonDB : ScriptableObject
	{
		// Token: 0x06001608 RID: 5640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B8")]
		[Address(RVA = "0x10135435C", Offset = "0x135435C", VA = "0x10135435C")]
		public Text_CommonDB()
		{
		}

		// Token: 0x04002387 RID: 9095
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001CF1")]
		public Text_CommonDB_Param[] m_Params;
	}
}
