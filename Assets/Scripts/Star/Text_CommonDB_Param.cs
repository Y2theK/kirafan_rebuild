﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005E5 RID: 1509
	[Token(Token = "0x20004D8")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct Text_CommonDB_Param
	{
		// Token: 0x04002385 RID: 9093
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001CEF")]
		public int m_ID;

		// Token: 0x04002386 RID: 9094
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001CF0")]
		public string m_Text;
	}
}
