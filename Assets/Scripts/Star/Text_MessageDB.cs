﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005E9 RID: 1513
	[Token(Token = "0x20004DC")]
	[StructLayout(3)]
	public class Text_MessageDB : ScriptableObject
	{
		// Token: 0x06001609 RID: 5641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014B9")]
		[Address(RVA = "0x101354364", Offset = "0x1354364", VA = "0x101354364")]
		public Text_MessageDB()
		{
		}

		// Token: 0x040024EC RID: 9452
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E56")]
		public Text_MessageDB_Param[] m_Params;
	}
}
