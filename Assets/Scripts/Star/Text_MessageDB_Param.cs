﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005E8 RID: 1512
	[Token(Token = "0x20004DB")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct Text_MessageDB_Param
	{
		// Token: 0x040024EA RID: 9450
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E54")]
		public int m_ID;

		// Token: 0x040024EB RID: 9451
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E55")]
		public string m_Text;
	}
}
