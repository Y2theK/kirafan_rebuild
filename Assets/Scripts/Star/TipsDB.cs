﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005EB RID: 1515
	[Token(Token = "0x20004DE")]
	[StructLayout(3)]
	public class TipsDB : ScriptableObject
	{
		// Token: 0x0600160A RID: 5642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BA")]
		[Address(RVA = "0x10135436C", Offset = "0x135436C", VA = "0x10135436C")]
		public TipsDB()
		{
		}

		// Token: 0x040024F1 RID: 9457
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E5B")]
		public TipsDB_Param[] m_Params;
	}
}
