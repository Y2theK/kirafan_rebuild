﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005EA RID: 1514
	[Token(Token = "0x20004DD")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct TipsDB_Param
	{
		// Token: 0x040024ED RID: 9453
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E57")]
		public int m_ID;

		// Token: 0x040024EE RID: 9454
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001E58")]
		public int m_Category;

		// Token: 0x040024EF RID: 9455
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E59")]
		public string m_Title;

		// Token: 0x040024F0 RID: 9456
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001E5A")]
		public string m_Text;
	}
}
