﻿using System.Collections.Generic;
using UnityEngine;

namespace Star {
    public class TitleBG : MonoBehaviour {
        public Transform m_Transform;
        public MsbHandler m_MsbHndl;
        public MeigeAnimCtrl m_MeigeAnimCtrl;
        public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;
        private eAnimType m_CurrentAnimType = eAnimType.None;

        public eAnimType CurrentAnimType => m_CurrentAnimType;

        private void Awake() {
            if (m_MsbHndl != null && m_MeigeAnimCtrl != null && m_MeigeAnimClipHolders != null) {
                m_MeigeAnimCtrl.Open();
                for (int i = 0; i < m_MeigeAnimClipHolders.Count; i++) {
                    m_MeigeAnimCtrl.AddClip(m_MsbHndl, m_MeigeAnimClipHolders[i]);
                }
                m_MeigeAnimCtrl.Close();
            }
        }

        public void Play(eAnimType animType) {
            if (animType == eAnimType.Loop) {
                m_MeigeAnimCtrl.Play("loop", 0, WrapMode.Loop);
            } else if (animType == eAnimType.Intro) {
                m_MeigeAnimCtrl.Play("start", 0, WrapMode.ClampForever);
            }
        }

        public float GetPlayTime() {
            return m_MeigeAnimCtrl.m_AnimationPlayTime;
        }

        public float GetNormalizePlayTime() {
            return m_MeigeAnimCtrl.m_NormalizedPlayTime;
        }

        public bool IsPlaying() {
            if (m_MeigeAnimCtrl.m_WrapMode == WrapMode.ClampForever) {
                return m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
            }
            return m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play;
        }

        public enum eAnimType {
            None = -1,
            Intro,
            Loop,
            Num
        }
    }
}
