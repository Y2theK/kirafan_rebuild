﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005EE RID: 1518
	[Token(Token = "0x20004E1")]
	[StructLayout(3)]
	public class TitleListDB : ScriptableObject
	{
		// Token: 0x0600160B RID: 5643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BB")]
		[Address(RVA = "0x101354714", Offset = "0x1354714", VA = "0x101354714")]
		public TitleListDB()
		{
		}

		// Token: 0x04002501 RID: 9473
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E6B")]
		public TitleListDB_Param[] m_Params;
	}
}
