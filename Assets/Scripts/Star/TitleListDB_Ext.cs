﻿using System.Collections.Generic;

namespace Star {
    public static class TitleListDB_Ext {
        public static bool IsExist(this TitleListDB self, eTitleType titleType) {
            if (titleType != eTitleType.None) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_TitleType == (int)titleType) {
                        return true;
                    }
                }
            }
            return false;
        }

        public static TitleListDB_Param GetParam(this TitleListDB self, eTitleType titleType) {
            if (titleType != eTitleType.None) {
                for (int i = 0; i < self.m_Params.Length; i++) {
                    if (self.m_Params[i].m_TitleType == (int)titleType) {
                        return self.m_Params[i];
                    }
                }
            }
            return default;
        }

        public static int GetTitleNum(this TitleListDB self) {
            return self.m_Params.Length;
        }

        public static eTitleType[] CalcSortedTitleID(this TitleListDB self) {
            List<SortData> sorted = new List<SortData>();
            for (int i = 0; i < self.m_Params.Length; i++) {
                sorted.Add(new SortData {
                    m_TitleType = (eTitleType)self.m_Params[i].m_TitleType,
                    m_Order = self.m_Params[i].m_Order
                });
            }
            sorted.Sort(CompareTitle);
            eTitleType[] titles = new eTitleType[sorted.Count];
            for (int i = 0; i < titles.Length; i++) {
                titles[i] = sorted[i].m_TitleType;
            }
            return titles;
        }

        private static int CompareTitle(SortData A, SortData B) {
            return A.m_Order.CompareTo(B.m_Order);
        }

        public class SortData {
            public eTitleType m_TitleType;
            public int m_Order;
        }
    }
}
