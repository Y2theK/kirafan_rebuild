﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005ED RID: 1517
	[Token(Token = "0x20004E0")]
	[Serializable]
	[StructLayout(0, Size = 56)]
	public struct TitleListDB_Param
	{
		// Token: 0x040024F9 RID: 9465
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E63")]
		public int m_TitleType;

		// Token: 0x040024FA RID: 9466
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001E64")]
		public int m_Order;

		// Token: 0x040024FB RID: 9467
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E65")]
		public string m_ResouceBaseName;

		// Token: 0x040024FC RID: 9468
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001E66")]
		public string m_DisplayName;

		// Token: 0x040024FD RID: 9469
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E67")]
		public int m_LimitBreakItemID;

		// Token: 0x040024FE RID: 9470
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001E68")]
		public string m_Descript;

		// Token: 0x040024FF RID: 9471
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001E69")]
		public sbyte m_Playable;

		// Token: 0x04002500 RID: 9472
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001E6A")]
		public string m_BGName;
	}
}
