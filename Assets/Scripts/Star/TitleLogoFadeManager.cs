﻿using System;
using UnityEngine;

namespace Star {
    public class TitleLogoFadeManager : MonoBehaviour {
        [SerializeField] private Fade m_Fade;
        [SerializeField] private float m_DefaultFadeOutTime = 0.25f;
        [SerializeField] private float m_DefaultFadeInTime = 0.25f;
        [SerializeField] private GameObject m_BackGround;

        private Color m_CurrentColor = Color.black;

        public void SetColor(Color color) {
            m_CurrentColor = color;
            m_Fade.SetColor(color);
        }

        public Color GetColor() {
            return m_Fade.GetColor();
        }

        public void FadeIn(float time = -1f, Action onCompleteFunc = null) {
            FadeIn(Color.black, time, onCompleteFunc);
        }

        public void FadeInFromCurrentColor(float time = -1f, Action onCompleteFunc = null) {
            FadeIn(m_CurrentColor, time, onCompleteFunc);
        }

        public void FadeIn(Color color, float time = -1f, Action onCompleteFunc = null) {
            if (time < 0f) {
                time = m_DefaultFadeInTime;
            }
            SetColor(color);
            m_Fade.FadeIn(time, onCompleteFunc);
            m_BackGround.SetActive(true);
        }

        public void FadeOut(float time = -1f, Action onCompleteFunc = null) {
            FadeOut(Color.black, time, onCompleteFunc);
        }

        public void FadeOutFromCurrentColor(float time = -1f, Action onCompleteFunc = null) {
            FadeOut(m_CurrentColor, time, onCompleteFunc);
        }

        public void FadeOut(Color color, float time = -1f, Action onCompleteFunc = null) {
            if (time < 0f) {
                time = m_DefaultFadeOutTime;
            }
            SetColor(color);
            m_Fade.FadeOut(time, onCompleteFunc);
            m_BackGround.SetActive(true);
        }

        private void Update() {
            if (!IsEnableRender()) {
                m_BackGround.SetActive(false);
            }
        }

        public bool IsEnableRender() {
            return m_Fade.IsEnableRender();
        }

        public bool IsComplete() {
            return m_Fade.IsComplete();
        }

        public void SetFadeRatio(float ratio) {
            m_Fade.SetFadeRatio(ratio);
        }
    }
}
