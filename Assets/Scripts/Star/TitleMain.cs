﻿using Meige;
using Star.UI;
using Star.UI.Title;
using UnityEngine;
using UnityEngine.UI;

namespace Star {
    public class TitleMain : GameStateMain {
        public const int STATE_INIT = 0;
        public const int STATE_LOGO = 1;
        public const int STATE_TITLE = 2;

        [SerializeField] private TitleBG m_TitleBG;
        [SerializeField] private TitleUI m_UI;
        [SerializeField] private TitleLogoFadeManager m_TitleLogoFadeManager;
        [SerializeField] private StableAspect m_StableAspect;
        [SerializeField] private FixSortOrder m_CineScoSortOrder;
        [SerializeField] private RectTransform m_CineScoA;
        [SerializeField] private RectTransform m_CineScoB;
        [SerializeField] private CanvasScaler m_AspectUIScaler;
        [SerializeField] private TitleLogoGroup m_LogoGroup;
        [SerializeField] private GameObject[] m_LogoObject;

        public TitleBG TitleBG => m_TitleBG;
        public TitleUI TitleUI => m_UI;
        public TitleLogoGroup LogoGroup => m_LogoGroup;
        public TitleLogoFadeManager FadeManager => m_TitleLogoFadeManager;

        private void Start() {
            if (GameSystem.Inst.GlobalParam.IsRequestedReturnTitle) {
                MeigeResourceManager.UnloadHandlerAll();
            }
            SetNextState(STATE_INIT);
            m_UI.Setup();
            GameSystem.Inst.SetCineScoImagesm(false);
            m_TitleLogoFadeManager.SetFadeRatio(1f);
            m_TitleLogoFadeManager.SetColor(Color.white);
            SetupLogoScaler();
            SetupAspect();
        }

        private void OnDestroy() {
            m_TitleBG = null;
            m_UI = null;
        }

        protected override void Update() {
            base.Update();
        }

        public override GameStateBase ChangeState(int stateID) {
            if (m_NextStateID == STATE_TITLE) {
                GameSystem.Inst.TouchEffectMng.ChangeCamera(GameSystem.Inst.SystemUICamera);
                return new TitleState_Title(this);
            }
            else if (m_NextStateID == STATE_LOGO) {
                GameSystem.Inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceOverlay);
                return new TitleState_Logo(this);
            }
            else if (m_NextStateID == STATE_INIT) {
                GameSystem.Inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceOverlay);
                return new TitleState_Init(this);
            }
            return null;
        }

        private void SetupLogoScaler() {
            int screenHeight = Screen.height;
            int screenWidth = Screen.width;
            float screenRatio = (float)screenHeight / screenWidth;
            if (screenRatio < UIDefine.IDEAL_ASPECT_RATIO) {
                float logoScale = screenRatio / UIDefine.IDEAL_ASPECT_RATIO;
                foreach (GameObject logo in m_LogoObject) {
                    logo.transform.localScale = new Vector3(logoScale, logoScale, 1);
                }
            }
        }

        private void SetupAspect() {
            float matchWidthOrHeight;
            float ratio = m_StableAspect.TargetHeight / m_StableAspect.TargetWidth;
            int screenHeight = Screen.height;
            int screenWidth = Screen.width;
            if (ratio <= (float)screenHeight / screenWidth) {
                float newHeight = (1f - (ratio * screenWidth) / screenHeight) * 0.5f * screenHeight;
                m_CineScoA.anchoredPosition = Vector2.zero;
                m_CineScoB.anchoredPosition = new Vector2(0, -(screenHeight - newHeight));
                Vector2 sizeDelta = new Vector2(screenWidth, newHeight);
                m_CineScoA.sizeDelta = sizeDelta;
                m_CineScoB.sizeDelta = sizeDelta;
                matchWidthOrHeight = 0;
            }
            else {
                float newWidth = (1f - (screenHeight / ratio) / screenWidth) * 0.5f * screenWidth;
                m_CineScoA.anchoredPosition = Vector2.zero;
                m_CineScoB.anchoredPosition = new Vector2(screenWidth - newWidth, 0);
                Vector2 sizeDelta = new Vector2(newWidth, screenHeight);
                m_CineScoA.sizeDelta = sizeDelta;
                m_CineScoB.sizeDelta = sizeDelta;
                matchWidthOrHeight = 1;
            }
            m_AspectUIScaler.matchWidthOrHeight = matchWidthOrHeight;
        }

        public void SetupSortOrderCineSco() {
            m_CineScoSortOrder.SetSortOrder(UIDefine.eSortOrderTypeID.FullScreenFilter, 1);
        }

        public void SetActiveCineSco(bool isActive) {
            m_CineScoA.gameObject.SetActive(isActive);
            m_CineScoB.gameObject.SetActive(isActive);
        }
    }
}
