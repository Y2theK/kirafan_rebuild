﻿namespace Star {
    public class TitleState : GameStateBase {
        protected TitleMain m_Owner;

        public TitleState(TitleMain owner) {
            m_Owner = owner;
        }

        public override int GetStateID() {
            return -1;
        }

        public override void OnStateEnter() { }

        public override void OnStateExit() { }

        public override void OnDispose() { }

        public override int OnStateUpdate() {
            return -1;
        }

        protected override void OnClickBackButton(bool isCallFromShortCut) { }
    }
}
