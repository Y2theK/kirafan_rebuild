﻿using Star.UI;
using UnityEngine;

namespace Star {
    public class TitleState_Init : TitleState {
        private eStep m_Step = eStep.None;

        public TitleState_Init(TitleMain owner) : base(owner) { }

        public override int GetStateID() {
            return TitleMain.STATE_INIT;
        }

        public override void OnStateEnter() {
            m_Step = eStep.First;
        }

        public override int OnStateUpdate() {
            GameSystem inst = GameSystem.Inst;
            switch (m_Step) {
                case eStep.First:
                    if (inst != null && inst.IsAvailable()) {
                        if (inst.GlobalParam.IsRequestedReturnTitle) {
                            inst.SoundMng.StopBGM();
                            inst.GlobalParam.IsRequestedReturnTitle = false;
                            inst.RequesetUnloadUnusedAssets(true);
                            inst.LoadingUI.Abort();
                            inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceOverlay);
                            m_Step = eStep.FromReturnTitle_Wait;
                        } else {
                            m_Step = eStep.SystemPrepareVersionGet;
                        }
                    }
                    break;
                case eStep.FromReturnTitle_Wait:
                    if (inst.IsCompleteUnloadUnusedAssets()) {
                        m_Step = eStep.SystemPrepareVersionGet;
                    }
                    break;
                case eStep.SystemPrepareVersionGet:
                    m_Step = eStep.SystemPrepareVersionGet_Wait;
                    inst.LoadingUI.Open(LoadingUI.eDisplayMode.NowLoadingOnly, eTipsCategory.None);
                    inst.LoadingUI.UpdateProgress(0f);
                    inst.PrepareVersionGet();
                    break;
                case eStep.SystemPrepareVersionGet_Wait:
                    if (inst.IsCompletePrepareVersionGet()) {
                        m_Step = eStep.SystemPrepare0;
                    }
                    break;
                case eStep.SystemPrepare0:
                    m_Step = eStep.SystemPrepare0_Wait;
                    inst.PrepareNetwork(false);
                    inst.LoadingUI.UpdateProgress(0.25f);
                    break;
                case eStep.SystemPrepare0_Wait:
                    bool complete = inst.IsCompletePrepareNetowrk(out bool isError);
                    if (!isError) {
                        if (!complete) { break; }
                        m_Step = eStep.SystemPrepare1_Wait;
                        inst.PrepareAssetBundles();
                        inst.LoadingUI.UpdateProgress(0.4f);
                    } else {
                        inst.CmnMsgWindow.Open(
                            CommonMessageWindow.eType.OK,
                            "ダウンロードエラー",
                            "ダウンロード中にエラーが発生しました。　リトライします。",
                            null,
                            OnConfirmDownloadError,
                            null
                        );
                        m_Step = eStep.SystemPrepare0_Error;
                    }
                    break;
                case eStep.SystemPrepare1_Wait:
                    if (inst.IsCompletePrepareAssetBundles()) {
                        m_Step = eStep.SystemPrepare2_Wait;
                        inst.PrepareDatabase();
                        inst.LoadingUI.UpdateProgress(0.55f);
                    }
                    break;
                case eStep.SystemPrepare2_Wait:
                    if (inst.IsCompletePrepareDatabase()) {
                        m_Step = eStep.SystemPrepare3_Wait;
                        inst.CRIFileInstaller.InstallAcf(inst.DbMng.CRIFileVersionDB);
                        inst.LoadingUI.UpdateProgress(0.7f);
                    }
                    break;
                case eStep.SystemPrepare3_Wait:
                    if (!inst.CRIFileInstaller.IsInstalling) {
                        if (inst.CRIFileInstaller.IsError) {
                            inst.CRIFileInstaller.RetryInstall();
                        } else {
                            m_Step = eStep.SystemPrepare4_Wait;
                            inst.PrepareSound(true);
                            inst.LoadingUI.UpdateProgress(0.85f);
                        }
                    }
                    break;
                case eStep.SystemPrepare4_Wait:
                    if (inst.IsCompletePrepareSound()) {
                        inst.LoadingUI.UpdateProgress(1f);
                        inst.LoadingUI.Close();
                        inst.CmnMsgWindow.FullScreenFilterFlag = true;
                        m_Step = eStep.None;
                        return TitleMain.STATE_LOGO;
                    }
                    break;
            }
            return -1;
        }

        private void OnConfirmDownloadError(int answer) {
            m_Step = eStep.SystemPrepare0_Wait;
            GameSystem.Inst.PrepareNetwork(true);
        }

        private enum eStep {
            None = -1,
            First,
            FromReturnTitle_Wait,
            SystemPrepareVersionGet,
            SystemPrepareVersionGet_Wait,
            SystemPrepare0,
            SystemPrepare0_Wait,
            SystemPrepare0_Error,
            SystemPrepare1_Wait,
            SystemPrepare2_Wait,
            SystemPrepare3_Wait,
            SystemPrepare4_Wait
        }
    }
}
