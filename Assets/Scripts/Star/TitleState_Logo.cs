﻿namespace Star {
    public class TitleState_Logo : TitleState {
        private eStep m_Step = eStep.None;

        public TitleState_Logo(TitleMain owner) : base(owner) { }

        public override int GetStateID() {
            return TitleMain.STATE_LOGO;
        }

        public override void OnStateEnter() {
            m_Step = eStep.First;
        }

        public override int OnStateUpdate() {
            if (m_Step == eStep.First) {
                m_Owner.LogoGroup.Open();
                m_Step = eStep.Logo_Wait;
            } else if (m_Step == eStep.Logo_Wait) {
                if (!m_Owner.LogoGroup.IsOpen()) {
                    m_Step = eStep.None;
                    return TitleMain.STATE_TITLE;
                }
            }
            return -1;
        }

        private enum eStep {
            None = -1,
            First,
            Logo_Wait,
            End
        }
    }
}
