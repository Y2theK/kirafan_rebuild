﻿using GachaResponseTypes;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.Title;
using System.Collections;
using UnityEngine;
using WWWTypes;

namespace Star {
    public class TitleState_Title : TitleState {
        private const float DISPLAY_TITLELOGO_SEC = 7.6f;
        private const float DISPLAY_FOTTER_SEC = 8.8f;

        private eStep m_Step = eStep.None;
        private bool m_IsRequestReturnTitle;
        private bool m_IsPreparedBgm;
        private bool m_GameStartFlag;
        private bool m_AutoVoided;
        private bool m_DetectTouch;
        private bool m_CreatingOfflineData;
        private OfflineResourceManager m_OfflineResourceManager = new OfflineResourceManager();

        public TitleState_Title(TitleMain owner) : base(owner) { }

        public override int GetStateID() {
            return TitleMain.STATE_TITLE;
        }

        public override void OnStateEnter() {
            ChangeStep(eStep.Title_0);
        }

        public override void OnDispose() {
            m_OfflineResourceManager.Dispose();
        }

        public override int OnStateUpdate() {
            switch (m_Step) {
                case eStep.Title_0:
                    m_Owner.TitleUI.OnClickGameStartButton += OnClickGameStartButton;
                    m_Owner.TitleUI.OnClickPlayerResetButton += OnClickPlayerResetButton;
                    m_Owner.TitleUI.OnClickPlayerMoveButton += OnClickPlayerMoveButton;
                    m_Owner.TitleUI.OnClickCacheClearButton += OnClickCacheClearButton;
                    m_Owner.TitleUI.OnClickCreateOfflineDataButton += OnClickCreateOfflineDataButton;
                    m_Owner.TitleUI.OnSuccessPlayerMoveCallback += OnPlayerMoveCallback;
                    m_Owner.TitleUI.OnClickDetectTouch += OnClickDetectTouch;

                    int requestCueSheetIndex = 0;
                    int requestNamedIndex = -1;
                    int requestCharIdForSelector = -1;
                    if (!APIUtility.IsNeedSignup()) {
                        requestCueSheetIndex = LocalSaveData.Inst.GetTitleCallCharacterCueSheetIndex();
                        requestNamedIndex = LocalSaveData.Inst.GetTitleCallCharacterNamedListIndex();
                        requestCharIdForSelector = LocalSaveData.Inst.GetTitleCallCharacterIdForSelector();
                        if (requestNamedIndex < 0 && requestCueSheetIndex < 0) {
                            requestCueSheetIndex = GameSystem.Inst.GetTitleCallCharacterCueSheetIndex();
                            requestNamedIndex = -1;
                            requestCharIdForSelector = -1;
                        }
                    }
                    m_Owner.TitleUI.SetRequestVoice(requestCueSheetIndex, requestNamedIndex, requestCharIdForSelector);
                    m_IsPreparedBgm = GameSystem.Inst.SoundMng.PrepareBGM(eSoundBgmListDB.BGM_TITLE);
                    GameSystem.Inst.CmnMsgWindow.FullScreenFilterFlag = true;
                    m_GameStartFlag = false;
                    ChangeStep(eStep.Title_1);
                    break;
                case eStep.Title_1:
                    if (m_IsPreparedBgm && !GameSystem.Inst.SoundMng.IsCompletePrepareBGM()) { break; }

                    if (GameSystem.Inst.SystemUICamera.TryGetComponent(out AspectScope aspectScope)) {
                        aspectScope.FixSafeAreaVertical();
                    }
                    m_Owner.TitleUI.SetupUserInfoText();
                    m_Owner.TitleUI.ShowUIHeader();
                    if (m_Owner.LogoGroup.IsSkipped()) {
                        SkipIntro();
                    } else {
                        m_Owner.TitleBG.Play(TitleBG.eAnimType.Intro);
                    }
                    m_Owner.FadeManager.FadeIn(Color.white);
                    GameSystem.Inst.SoundMng.ResumeBGM();
                    ChangeStep(eStep.Title_2);
                    break;
                case eStep.Title_2:
                    TitleBG bg = m_Owner.TitleBG;
                    TitleUI ui = m_Owner.TitleUI;
                    if (bg.CurrentAnimType == TitleBG.eAnimType.Intro) {
                        if (m_DetectTouch) {
                            SkipIntro();
                            break;
                        }
                        if (bg.IsPlaying()) {
                            if (!ui.IsDisplay(TitleUI.eTitleUIAnim.TitleLogo) && bg.m_MeigeAnimCtrl.m_AnimationPlayTime > DISPLAY_TITLELOGO_SEC) {
                                ui.PlayUITitleLogo();
                            }
                            if (ui.IsDisplay(TitleUI.eTitleUIAnim.Footer) || bg.m_MeigeAnimCtrl.m_AnimationPlayTime <= DISPLAY_FOTTER_SEC) {
                                break;
                            }
                        } else {
                            ui.PlayUITitleLogo();
                        }
                        ui.PlayUIFooter();
                        if (!bg.IsPlaying()) {
                            bg.Play(TitleBG.eAnimType.Loop);
                        }
                    } else if (ui.IsCompleteUIAnim(TitleUI.eTitleUIAnim.Footer)) {
                        ui.SetGoToNextSheetActive(true);
                        m_Owner.SetupSortOrderCineSco();
                        ChangeStep(eStep.Title_Wait);
                    }
                    break;
                case eStep.Title_Wait:
                    m_Owner.TitleUI.SetGoToNextSheetActive(true);
                    UpdateAndroidBackKey();
                    break;
                case eStep.Agreement:
                    ChangeStep(eStep.Agreement_Wait);
                    m_Owner.TitleUI.OpenAgreement(OnCloseAgreement);
                    break;
                case eStep.Login:
                    GameSystem.Inst.LoginManager.Request_Login(m_AutoVoided, OnResponse_Login, OnResponse_GetAll);
                    ChangeStep(eStep.Login_Wait);
                    break;
                case eStep.QuestGetAll:
                    GameSystem.Inst.QuestMng.Request_QuestGetAll(OnResponse_QuestGetAll, true);
                    ChangeStep(eStep.QuestGetAll_Wait);
                    break;
                case eStep.LoadADV_Wait:
                    m_Owner.TitleUI.UpdateCreatingOfflineDataState(m_OfflineResourceManager.ScriptNum, m_OfflineResourceManager.ScriptMax);
                    if (m_OfflineResourceManager.UpdatePrepare()) {
                        m_Owner.TitleUI.SetCreatingOfflineDataWindowOutLineMessage("2/2");
                        ChangeStep(eStep.CreatingOfflineData_Wait);
                    }
                    break;
                case eStep.CreatingOfflineData_Wait:
                    m_Owner.TitleUI.UpdateCreatingOfflineDataState(m_OfflineResourceManager.DownloadedNum, m_OfflineResourceManager.DownloadMax);
                    if (m_OfflineResourceManager.Update()) {
                        m_OfflineResourceManager.Dispose();
                        m_Owner.TitleUI.CloseCreatingOfflineData();
                        m_CreatingOfflineData = false;

                        string msgTitle = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Offline_ABCache_WindowTitle);
                        string msgText = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.Offline_CreatedABCache_WindowText);
                        GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, msgTitle, msgText, string.Empty, 0f);
                        ChangeStep(eStep.Title_Wait);
                    }
                    break;
                case eStep.Mission:
                    GameSystem.Inst.MissionMng.Prepare();
                    ChangeStep(eStep.Mission_Wait);
                    break;
                case eStep.Mission_Wait:
                    if (GameSystem.Inst.MissionMng.IsDonePrepare()) {
                        ChangeStep(eStep.Gacha);
                    }
                    break;
                case eStep.Gacha:
                    GameSystem.Inst.ClearUnplayedGachaAdvertise();
                    GameSystem.Inst.Gacha.Request_GachaGetAll(OnResponse_GachaGetAll, false);
                    GameSystem.Inst.ConnectingIcon.Open();
                    ChangeStep(eStep.Gacha_Wait);
                    break;
                case eStep.EventBanner:
                    GameSystem.Inst.EventBannerLoad.Prepare();
                    GameSystem.Inst.EventBannerLoad.Request_EventBanner(OnResponse_EventBannerGetAll);
                    ChangeStep(eStep.EventBanner_Wait);
                    break;
                case eStep.TransitNextScene:
                    if (!GameSystem.Inst.LoadingUI.IsOverlay()) {
                        GameSystem.Inst.LoadingUI.Open();
                    }
                    ChangeStep(eStep.UnloadResource);
                    break;
                case eStep.UnloadResource:
                    if (GameSystem.Inst.LoadingUI.IsComplete()) {
                        if (GameSystem.Inst.SystemUICamera.TryGetComponent(out AspectScope aspect)) {
                            aspect.RevertSafeAreaVertical();
                        }
                        ChangeStep(eStep.TransitNextScene_Wait);
                    }
                    break;
                case eStep.TransitNextScene_Wait:
                    ChangeStep(eStep.None);
                    m_Owner.TitleUI.gameObject.SetActive(false);
                    m_Owner.SetActiveCineSco(false);
                    if (m_IsRequestReturnTitle) {
                        APIUtility.ReturnTitle(true);
                    } else {
                        SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Download);
                    }
                    if (m_GameStartFlag) {
                        GameSystem.Inst.SetCineScoImagesm(true);
                    }
                    break;
            }
            m_DetectTouch = false;
            return -1;
        }

        private void ChangeStep(eStep step) {
            m_Step = step;
            if (step == eStep.Title_2) {
                m_Owner.TitleUI.SetDetectTouchActive(true);
            } else {
                m_Owner.TitleUI.SetDetectTouchActive(false);
                if (step == eStep.Title_Wait) {
                    return;
                }
            }
            m_Owner.TitleUI.SetGoToNextSheetActive(false);
        }

        private void SkipIntro() {
            m_Owner.TitleBG.Play(TitleBG.eAnimType.Loop);
            m_Owner.TitleUI.ShowUIOnSkip();
        }

        private void OnResponse_EventBannerGetAll() {
            TransitNextSceneStart();
        }

        public void OnResponse_GachaGetAll(MeigewwwParam wwwParam, GetAll param) {
            if (param.GetResult() == ResultCode.SUCCESS) {
                WWWComUtil.SetPlayedGachaAdvertiseList(param);
                ChangeStep(eStep.EventBanner);
            }
        }

        private void OnClickDetectTouch() {
            m_DetectTouch = true;
        }

        public void OnClickGameStartButton() {
            m_GameStartFlag = true;
            if (APIUtility.IsNeedSignup()) {
                LocalSaveData localSave = LocalSaveData.Inst;
                if (localSave != null && !localSave.Agreement) {
                    ChangeStep(eStep.Agreement);
                } else {
                    TransitNextSceneStart();
                }
            } else {
                ChangeStep(eStep.Login);
            }
        }

        public void OnCloseAgreement(bool isYes) {
            if (isYes) {
                LocalSaveData.Inst.Agreement = true;
                LocalSaveData.Inst.Save();
                TransitNextSceneStart();
            } else {
                ChangeStep(eStep.Title_Wait);
            }
        }

        private void OnResponse_Login(MeigewwwParam wwwParam, Login param) {
            if (param == null) { return; }

            ResultCode result = param.GetResult();
            if (result == ResultCode.SUCCESS) {
                return;
            }

            if (result == ResultCode.UUID_NOT_FOUND) {
                APIUtility.ShowErrorWindowOk(APIUtility.ERR_TITLE_NOT_UUID, result.ToMessageString(), null);
                m_Owner.TitleUI.SetUUIDNotFoundState();
            } else if (result == ResultCode.INVALID_AUTH_VOIDED) {
                APIUtility.ShowErrorWindowOk(param.subject, param.message, null);
                m_AutoVoided = true;
            } else {
                APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
            }
            ChangeStep(eStep.Title_Wait);
            if (m_CreatingOfflineData) {
                m_Owner.TitleUI.CloseCreatingOfflineData();
                m_CreatingOfflineData = false;
            }
        }

        private void OnResponse_GetAll(MeigewwwParam wwwParam, Getall param) {
            if (param == null) { return; }

            ResultCode result = param.GetResult();
            if (result == ResultCode.SUCCESS) {
                if (m_CreatingOfflineData) {
                    OfflineSaveData offlineSave = OfflineSaveData.Inst;
                    APIUtility.wwwToUserData(param.player, offlineSave.UserData);
                    APIUtility.wwwToUserData(param.managedCharacters, offlineSave.UserCharaDatas);
                    APIUtility.wwwToUserData(param.managedNamedTypes, offlineSave.UserNamedDatas);
                    offlineSave.UserAdvData.SetAdvIDs(param.advIds);
                    GameSystem.Inst.OfferMng.Request_GetAll(OnResponseOfferGetAll, true);
                } else {
                    ChangeStep(eStep.QuestGetAll);
                }
            } else {
                APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
                ChangeStep(eStep.Title_Wait);
            }
        }

        private void OnResponseOfferGetAll() {
            OfflineSaveData.Inst.SubOffers = GameSystem.Inst.OfferMng.GetSubOffer();
            OfflineSaveData.Inst.Save();
            m_OfflineResourceManager.LoadFirstADV();
            m_Owner.TitleUI.SetCreatingOfflineDataWindowOutLineMessage("1/2");
            ChangeStep(eStep.LoadADV_Wait);
        }

        private void OnResponse_QuestGetAll() {
            GameSystem.Inst.QuestMng.Request_QuestChapterGetAll(OnResponse_QuestChapterGetAll, true);
        }

        private void OnResponse_QuestChapterGetAll() {
            ChangeStep(eStep.Mission);
        }

        private void OnClickPlayerResetButton() {
            GameSystem.Inst.LoginManager.Request_PlayerReset(OnResponse_PlayerReset);
        }

        private void OnResponse_PlayerReset(MeigewwwParam wwwParam, Reset param) {
            if (param == null) { return; }

            ResultCode result = param.GetResult();
            if (result != ResultCode.SUCCESS) {
                APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
                ChangeStep(eStep.Title_Wait);
                return;
            }

            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerResetTitle, eText_MessageDB.PlayerReset, OnClosedConfirm);
        }

        private void OnClickPlayerMoveButton(string moveCode, string moveCodePassward) {
            GameSystem.Inst.LoginManager.Request_PlayerMoveSet(moveCode, moveCodePassward, OnResponse_PlayerMoveSet);
        }

        private void OnResponse_PlayerMoveSet(string errorMessage) {
            if (string.IsNullOrEmpty(errorMessage)) {
                OnPlayerMoveCallback();
                return;
            }

            APIUtility.ShowErrorWindowOk(GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveErrTitle), errorMessage, null);
            ChangeStep(eStep.Title_Wait);
        }

        private void OnPlayerMoveCallback() {
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerMoveTitle, eText_MessageDB.PlayerMove, OnClosedConfirm);
            LocalSaveData.Inst.playedGachaAdvertiseList?.Clear();
        }

        private void OnClickCacheClearButton() {
            m_Owner.StartCoroutine(CacheClear());
        }

        private IEnumerator CacheClear() {
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.None, eText_MessageDB.PlayerCacheClearTitle, eText_MessageDB.PlayerCacheClearWait);
            while (GameSystem.Inst.CmnMsgWindow.IsPlayingAnim()) {
                yield return null;
            }
            yield return MeigeResourceManager.AssetbundleCleanCacheAsync();
            GameSystem.Inst.CRIFileInstaller.CleanCache();
            LocalSaveData.Inst.IsConfirmedDownloadCaution = false;
            LocalSaveData.Inst.Save();
            GameSystem.Inst.CmnMsgWindow.Close();
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerCacheClearTitle, eText_MessageDB.PlayerCacheClear, OnClosedConfirm);
        }

        private void OnClickCreateOfflineDataButton() {
            if (APIUtility.IsNeedSignup()) { return; }

            m_Owner.TitleUI.SetCreatingOfflineDataWindowOutLineMessage("0/2");
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            GameSystem.Inst.CmnMsgWindow.Open(
                CommonMessageWindow.eType.YES_NO,
                dbMng.GetTextCommon(eText_CommonDB.Offline_ABCache_WindowTitle),
                dbMng.GetTextCommon(eText_CommonDB.Offline_ABCache_WindowText),
                null,
                OnConfirmCreateOfflineData
            );
        }

        private void OnConfirmCreateOfflineData(int btn) {
            if (btn != 0) { return; }

            m_CreatingOfflineData = true;
            m_Owner.TitleUI.OpenCreatingOfflineData(OnQuitCreatingOfflineData);
            ChangeStep(eStep.Login);
        }

        private void OnQuitCreatingOfflineData() { }

        private void OnClosedConfirm(int answer) {
            m_IsRequestReturnTitle = true;
            TransitNextSceneStart();
        }

        private void TransitNextSceneStart() {
            ChangeStep(eStep.TransitNextScene);
        }

        private void UpdateAndroidBackKey() {
            if (UIUtility.GetInputAndroidBackKey(m_Owner.TitleUI.GetCacheClearButton())) {
                GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.AppQuitConfirmTitle, eText_MessageDB.AppQuitConfirmMessage, OnConfirmAppQuit);
            }
        }

        private void OnConfirmAppQuit(int answer) {
            if (answer == 1) {
                Application.Quit();
            }
        }

        private enum eStep {
            None = -1,
            Title_0,
            Title_1,
            Title_2,
            Title_Wait,
            Agreement,
            Agreement_Wait,
            Login,
            Login_Wait,
            QuestGetAll,
            QuestGetAll_Wait,
            LoadADV_Wait,
            CreatingOfflineData_Wait,
            Mission,
            Mission_Wait,
            Gacha,
            Gacha_Wait,
            EventBanner,
            EventBanner_Wait,
            TransitNextScene,
            UnloadResource,
            TransitNextScene_Wait
        }
    }
}
