﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F0 RID: 1520
	[Token(Token = "0x20004E3")]
	[StructLayout(3)]
	public class TogetherChargeDefineDB : ScriptableObject
	{
		// Token: 0x0600160C RID: 5644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BC")]
		[Address(RVA = "0x101358E00", Offset = "0x1358E00", VA = "0x101358E00")]
		public TogetherChargeDefineDB()
		{
		}

		// Token: 0x04002503 RID: 9475
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E6D")]
		public TogetherChargeDefineDB_Param[] m_Params;
	}
}
