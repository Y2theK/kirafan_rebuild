﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005EF RID: 1519
	[Token(Token = "0x20004E2")]
	[Serializable]
	[StructLayout(0, Size = 4)]
	public struct TogetherChargeDefineDB_Param
	{
		// Token: 0x04002502 RID: 9474
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E6C")]
		public float m_Value;
	}
}
