﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BE6 RID: 3046
	[Token(Token = "0x2000837")]
	[StructLayout(3)]
	public class TouchEffect : ActionAggregation
	{
		// Token: 0x06003571 RID: 13681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600309B")]
		[Address(RVA = "0x101358E08", Offset = "0x1358E08", VA = "0x101358E08")]
		private void Update()
		{
		}

		// Token: 0x06003572 RID: 13682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600309C")]
		[Address(RVA = "0x101358E70", Offset = "0x1358E70", VA = "0x101358E70")]
		public void Play(Vector2 pos, Canvas canvas, Camera uicamera, Camera wcamera)
		{
		}

		// Token: 0x06003573 RID: 13683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600309D")]
		[Address(RVA = "0x10135919C", Offset = "0x135919C", VA = "0x10135919C")]
		public TouchEffect()
		{
		}

		// Token: 0x040045A6 RID: 17830
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003102")]
		private RectTransform m_RectTransform;
	}
}
