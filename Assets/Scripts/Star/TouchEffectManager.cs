﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BE7 RID: 3047
	[Token(Token = "0x2000838")]
	[StructLayout(3)]
	public class TouchEffectManager : MonoBehaviour
	{
		// Token: 0x06003574 RID: 13684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600309E")]
		[Address(RVA = "0x1013591A4", Offset = "0x13591A4", VA = "0x1013591A4")]
		public void Start()
		{
		}

		// Token: 0x06003575 RID: 13685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600309F")]
		[Address(RVA = "0x101355878", Offset = "0x1355878", VA = "0x101355878")]
		public void ChangeCamera(Camera camera)
		{
		}

		// Token: 0x06003576 RID: 13686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030A0")]
		[Address(RVA = "0x10135572C", Offset = "0x135572C", VA = "0x10135572C")]
		public void ChangeRenderMode(RenderMode mode)
		{
		}

		// Token: 0x06003577 RID: 13687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030A1")]
		[Address(RVA = "0x101359218", Offset = "0x1359218", VA = "0x101359218")]
		public void Setup()
		{
		}

		// Token: 0x06003578 RID: 13688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030A2")]
		[Address(RVA = "0x1013594EC", Offset = "0x13594EC", VA = "0x1013594EC")]
		private void Update()
		{
		}

		// Token: 0x06003579 RID: 13689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030A3")]
		[Address(RVA = "0x101359A48", Offset = "0x1359A48", VA = "0x101359A48")]
		public TouchEffectManager()
		{
		}

		// Token: 0x040045A7 RID: 17831
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003103")]
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x040045A8 RID: 17832
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003104")]
		[SerializeField]
		private Camera m_uiCamera;

		// Token: 0x040045A9 RID: 17833
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003105")]
		[SerializeField]
		private Camera m_wCamera;

		// Token: 0x040045AA RID: 17834
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003106")]
		[SerializeField]
		private int m_EffectLimit;

		// Token: 0x040045AB RID: 17835
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003107")]
		[SerializeField]
		private TouchEffect m_EffectPrefab;

		// Token: 0x040045AC RID: 17836
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003108")]
		private List<TouchEffect> m_ActiveEffects;

		// Token: 0x040045AD RID: 17837
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003109")]
		private List<TouchEffect> m_InactiveEffects;

		// Token: 0x040045AE RID: 17838
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400310A")]
		private List<TouchEffectManager.TouchInfoAdapter> m_TouchInfos;

		// Token: 0x040045AF RID: 17839
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400310B")]
		private int m_FullUILayer;

		// Token: 0x040045B0 RID: 17840
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400310C")]
		private int m_SystemUILayer;

		// Token: 0x02000BE8 RID: 3048
		[Token(Token = "0x2001079")]
		public class TouchInfoAdapter
		{
			// Token: 0x0600357A RID: 13690 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600618C")]
			[Address(RVA = "0x1013594E4", Offset = "0x13594E4", VA = "0x1013594E4")]
			public TouchEffectManager.TouchInfoAdapter Setup(int in_index)
			{
				return null;
			}

			// Token: 0x0600357B RID: 13691 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600618D")]
			[Address(RVA = "0x1013598D0", Offset = "0x13598D0", VA = "0x1013598D0")]
			public void Update()
			{
			}

			// Token: 0x0600357C RID: 13692 RVA: 0x00016908 File Offset: 0x00014B08
			[Token(Token = "0x600618E")]
			[Address(RVA = "0x1013599A8", Offset = "0x13599A8", VA = "0x1013599A8")]
			public TouchEffectManager.TouchInfoAdapter.eState GetState()
			{
				return TouchEffectManager.TouchInfoAdapter.eState.None;
			}

			// Token: 0x0600357D RID: 13693 RVA: 0x00016920 File Offset: 0x00014B20
			[Token(Token = "0x600618F")]
			[Address(RVA = "0x1013599B0", Offset = "0x13599B0", VA = "0x1013599B0")]
			public Vector2 GetPosition()
			{
				return default(Vector2);
			}

			// Token: 0x0600357E RID: 13694 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006190")]
			[Address(RVA = "0x1013594DC", Offset = "0x13594DC", VA = "0x1013594DC")]
			public TouchInfoAdapter()
			{
			}

			// Token: 0x040045B1 RID: 17841
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067FC")]
			private int index;

			// Token: 0x040045B2 RID: 17842
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40067FD")]
			private TouchEffectManager.TouchInfoAdapter.eState state;

			// Token: 0x02000BE9 RID: 3049
			[Token(Token = "0x200134C")]
			public enum eState
			{
				// Token: 0x040045B4 RID: 17844
				[Token(Token = "0x40075AD")]
				None,
				// Token: 0x040045B5 RID: 17845
				[Token(Token = "0x40075AE")]
				TouchJustNow,
				// Token: 0x040045B6 RID: 17846
				[Token(Token = "0x40075AF")]
				Touching
			}
		}
	}
}
