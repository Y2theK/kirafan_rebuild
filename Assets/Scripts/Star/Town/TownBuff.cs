﻿using CommonLogic;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.Town {
    [Serializable]
    public class TownBuff {
        private const int ROUND_DIGIT = 2;

        [SerializeField] private List<BuffData> m_List = new List<BuffData>();

        public static TownBuff CalcTownBuff() {
            TownBuff townBuff = new TownBuff();
            UserTownData townData = GameSystem.Inst.UserDataMng.UserTownData;
            for (int i = 0; i < townData.GetBuildObjectDataNum(); i++) {
                UserTownData.BuildObjectData buildData = townData.GetBuildObjectDataAt(i);
                int id = TownUtility.CalcAreaToContentID(buildData.m_ObjID);
                int lv = buildData.m_Lv;
                if (lv != 0) {
                    TownObjectListDB_Param param = GameSystem.Inst.DbMng.TownObjListDB.GetParam(id);
                    TownObjectBuffDB_Param buffParam = GameSystem.Inst.DbMng.TownObjBuffDB.GetParam(param.m_BuffParamID);
                    if (lv > 0) {
                        for (int j = 0; j < buffParam.m_Datas.Length; j++) {
                            TownObjectBuffDB_Data data = buffParam.m_Datas[j];
                            BuffData buffData = new BuffData();
                            buffData.m_CondType = (eTownObjectBuffTargetConditionType)data.m_TargetConditionType;
                            buffData.m_Cond = data.m_TargetCondition;
                            buffData.m_Param.m_Type = (eTownObjectBuffType)data.m_BuffType;
                            if (lv - 1 < data.m_Value.Length) {
                                buffData.m_Param.m_Val = data.m_Value[lv - 1];
                            } else {
                                Debug.LogWarning($"not found TownObjectBuffDB_Param BuffParamID={param.m_BuffParamID} Lv={lv - 1}");
                            }
                            townBuff.AddBuffData(buffData);
                        }
                    }
                }
            }
            return townBuff;
        }

        private void AddBuffData(BuffData data) {
            m_List.Add(data);
        }

        public static long CalcTownGold() {
            UserTownData townData = GameSystem.Inst.UserDataMng.UserTownData;
            long total = 0L;
            for (int i = 0; i < townData.GetBuildObjectDataNum(); i++) {
                UserTownData.BuildObjectData buildData = townData.GetBuildObjectDataAt(i);
                if (buildData.m_IsOpen) {
                    TownObjectListDB_Param param = GameSystem.Inst.DbMng.TownObjListDB.GetParam(buildData.m_ObjID);
                    DropItemTool dropTable = TownItemDropDatabase.GetKeyIDToDatabase(param.m_LevelUpListID);
                    int lv = buildData.m_Lv;
                    if (dropTable != null && lv > 0) {
                        for (int j = 0; j < dropTable.m_Table.Length; j++) {
                            DropItemPakage package = dropTable.m_Table[j];
                            if (package.m_Level == lv) {
                                int id = package.m_Table[0].m_ResultNo;
                                UserScheduleData.DropState drop = UserTownUtil.GetFieldDropItems(id);
                                if (drop != null && drop.m_Category == UserScheduleData.eDropCategory.Money) {
                                    total += drop.m_Num;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return total;
        }

        public List<BuffParam> GetBuffData(eTownObjectBuffTargetConditionType conditionType, int condition) {
            List<BuffParam> list = new List<BuffParam>();
            for (int i = 0; i < m_List.Count; i++) {
                BuffData data = m_List[i];
                if (data.m_CondType == conditionType && data.m_Cond == condition) {
                    BuffParam param = data.m_Param;
                    bool exist = false;
                    for (int j = 0; j < list.Count; j++) {
                        if (list[j].m_Type == param.m_Type) {
                            exist = true;
                            list[j].m_Val += param.m_Val;
                            break;
                        }
                    }
                    if (!exist) {
                        list.Add(new BuffParam {
                            m_Type = param.m_Type,
                            m_Val = param.m_Val,
                        });
                    }
                }
            }
            for (int i = 0; i < list.Count; i++) {
                list[i].m_Val = (float)Math.Round(list[i].m_Val, ROUND_DIGIT);
            }
            return null;
        }

        public List<BuffParam> GetTypeData(eTownObjectBuffTargetConditionType conditionType, eTownObjectBuffType buffType) {
            List<BuffParam> list = new List<BuffParam>();
            for (int i = 0; i < m_List.Count; i++) {
                BuffData data = m_List[i];
                if (data.m_CondType == conditionType && data.m_Param.m_Type == buffType) {
                    BuffParam param = data.m_Param;
                    bool exist = false;
                    for (int j = 0; j < list.Count; j++) {
                        if (list[j].m_Type == param.m_Type) {
                            exist = true;
                            list[j].m_Val += param.m_Val;
                            break;
                        }
                    }
                    if (!exist) {
                        list.Add(new BuffParam {
                            m_Type = param.m_Type,
                            m_Val = param.m_Val,
                        });
                    }
                }
            }
            for (int i = 0; i < list.Count; i++) {
                list[i].m_Val = (float)Math.Round(list[i].m_Val, ROUND_DIGIT);
            }
            return list;
        }

        public List<BuffParam> GetBuffParam(eTitleType titleType, eClassType classType, eElementType elementType) {
            List<BuffParam> list = new List<BuffParam>();
            for (int i = 0; i < m_List.Count; i++) {
                BuffData data = m_List[i];
                bool valid = false;
                if (data.m_CondType == eTownObjectBuffTargetConditionType.None) {
                    valid = true;
                } else if (data.m_CondType == eTownObjectBuffTargetConditionType.TitleType && data.m_Cond == (int)titleType) {
                    valid = true;
                } else if (data.m_CondType == eTownObjectBuffTargetConditionType.ClassType && data.m_Cond == (int)classType) {
                    valid = true;
                } else if (data.m_CondType == eTownObjectBuffTargetConditionType.ElementType && data.m_Cond == (int)elementType) {
                    valid = true;
                }
                if (valid) {
                    BuffParam param = data.m_Param;
                    bool exist = false;
                    for (int j = 0; j < list.Count; j++) {
                        if (list[j].m_Type == param.m_Type) {
                            exist = true;
                            list[j].m_Val += param.m_Val;
                            break;
                        }
                    }
                    if (!exist) {
                        list.Add(new BuffParam {
                            m_Type = param.m_Type,
                            m_Val = param.m_Val
                        });
                    }
                }
            }
            for (int i = 0; i < list.Count; i++) {
                list[i].m_Val = (float)Math.Round(list[i].m_Val, ROUND_DIGIT);
            }
            return list;
        }

        public float[] GetStatusBuff(eTitleType titleType, eClassType classType, eElementType elementType) {
            float[] array = new float[(int)eCharaStatus.Num];
            List<BuffParam> buffs = GetBuffParam(titleType, classType, elementType);
            for (int i = 0; i < buffs.Count; i++) {
                if ((int)buffs[i].m_Type < (int)eCharaStatus.Num) {
                    array[(int)buffs[i].m_Type] += buffs[i].m_Val;
                }
            }
            for (int i = 0; i < array.Length; i++) {
                array[i] = (float)Math.Round(array[i], ROUND_DIGIT);
            }
            return array;
        }

        public float[] GetBuffParamArray(eTitleType titleType, eClassType classType, eElementType elementType) {
            float[] array = new float[(int)eTownObjectBuffType.Num];
            List<BuffParam> buffParam = GetBuffParam(titleType, classType, elementType);
            for (int i = 0; i < buffParam.Count; i++) {
                array[(int)buffParam[i].m_Type] += buffParam[i].m_Val;
            }
            for (int i = 0; i < array.Length; i++) {
                array[i] = (float)Math.Round(array[i], ROUND_DIGIT);
            }
            return array;
        }

        [Serializable]
        public class BuffParam {
            public eTownObjectBuffType m_Type;
            public float m_Val;
        }

        [Serializable]
        public class BuffData {
            public eTownObjectBuffTargetConditionType m_CondType;
            public int m_Cond;
            public BuffParam m_Param = new BuffParam();
        }
    }
}
