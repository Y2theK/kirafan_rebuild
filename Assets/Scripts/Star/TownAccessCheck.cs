﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000675 RID: 1653
	[Token(Token = "0x2000548")]
	[StructLayout(3)]
	public class TownAccessCheck
	{
		// Token: 0x0600182B RID: 6187 RVA: 0x0000B130 File Offset: 0x00009330
		[Token(Token = "0x60016CD")]
		[Address(RVA = "0x10135B484", Offset = "0x135B484", VA = "0x10135B484")]
		public static bool IsAccessSearchBuild(ref TownAccessCheck.ChrCheckDataBase pbase, ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			return default(bool);
		}

		// Token: 0x0600182C RID: 6188 RVA: 0x0000B148 File Offset: 0x00009348
		[Token(Token = "0x60016CE")]
		[Address(RVA = "0x10135B574", Offset = "0x135B574", VA = "0x10135B574")]
		public static bool IsAccessSearchBuild(ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			return default(bool);
		}

		// Token: 0x0600182D RID: 6189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60016CF")]
		[Address(RVA = "0x10135B624", Offset = "0x135B624", VA = "0x10135B624")]
		public TownAccessCheck()
		{
		}

		// Token: 0x02000676 RID: 1654
		[Token(Token = "0x2000E01")]
		public class ChrCheckDataBase
		{
			// Token: 0x0600182E RID: 6190 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E22")]
			[Address(RVA = "0x10135B70C", Offset = "0x135B70C", VA = "0x10135B70C")]
			public ChrCheckDataBase(long fmanageid)
			{
			}

			// Token: 0x0600182F RID: 6191 RVA: 0x0000B160 File Offset: 0x00009360
			[Token(Token = "0x6005E23")]
			[Address(RVA = "0x10135B858", Offset = "0x135B858", VA = "0x10135B858")]
			public bool IsCheckToCharaClass(int ChkParam)
			{
				return default(bool);
			}

			// Token: 0x06001830 RID: 6192 RVA: 0x0000B178 File Offset: 0x00009378
			[Token(Token = "0x6005E24")]
			[Address(RVA = "0x10135B868", Offset = "0x135B868", VA = "0x10135B868")]
			public bool IsCheckToElement(int ChkParam)
			{
				return default(bool);
			}

			// Token: 0x0400277E RID: 10110
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005A93")]
			public long m_ManageID;

			// Token: 0x0400277F RID: 10111
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005A94")]
			public eTitleType m_Title;

			// Token: 0x04002780 RID: 10112
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005A95")]
			public eClassType m_ClassID;

			// Token: 0x04002781 RID: 10113
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4005A96")]
			public eElementType m_Element;
		}

		// Token: 0x02000677 RID: 1655
		[Token(Token = "0x2000E02")]
		public struct BuildCheckDataBase
		{
			// Token: 0x06001831 RID: 6193 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005E25")]
			[Address(RVA = "0x100036208", Offset = "0x36208", VA = "0x100036208")]
			public void SetBuildParam(long fmanageid)
			{
			}

			// Token: 0x04002782 RID: 10114
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4005A97")]
			public long m_ManageID;

			// Token: 0x04002783 RID: 10115
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4005A98")]
			public int m_ObjID;

			// Token: 0x04002784 RID: 10116
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4005A99")]
			public eTitleType m_Title;

			// Token: 0x04002785 RID: 10117
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4005A9A")]
			public eClassType m_ClassID;

			// Token: 0x04002786 RID: 10118
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4005A9B")]
			public eElementType m_ElementID;

			// Token: 0x04002787 RID: 10119
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4005A9C")]
			public eTownObjectCategory m_Category;

			// Token: 0x04002788 RID: 10120
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4005A9D")]
			public int m_ResourceID;
		}
	}
}
