﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BA3 RID: 2979
	[Token(Token = "0x2000819")]
	[StructLayout(3)]
	public class TownBuildAnime : MonoBehaviour
	{
		// Token: 0x06003450 RID: 13392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FB9")]
		[Address(RVA = "0x10135B878", Offset = "0x135B878", VA = "0x10135B878")]
		public TownBuildAnime()
		{
		}

		// Token: 0x04004460 RID: 17504
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400306D")]
		[SerializeField]
		public TownBuildAnime.AnimeTools[] m_Table;

		// Token: 0x02000BA4 RID: 2980
		[Token(Token = "0x2001057")]
		[Serializable]
		public struct AnimeTools
		{
			// Token: 0x04004461 RID: 17505
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400675F")]
			public TownPartsBuildAnime.eType m_Type;

			// Token: 0x04004462 RID: 17506
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006760")]
			public AnimationClip m_Anime;
		}
	}
}
