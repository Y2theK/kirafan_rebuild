﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BA6 RID: 2982
	[Token(Token = "0x200081B")]
	[StructLayout(3)]
	public class TownBuildChangeAreaParam : ITownEventCommand
	{
		// Token: 0x06003457 RID: 13399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC0")]
		[Address(RVA = "0x10135B880", Offset = "0x135B880", VA = "0x10135B880")]
		public TownBuildChangeAreaParam(int fbuildpoint, long fmanageid, int fobjid)
		{
		}

		// Token: 0x06003458 RID: 13400 RVA: 0x00016350 File Offset: 0x00014550
		[Token(Token = "0x6002FC1")]
		[Address(RVA = "0x10135B8C8", Offset = "0x135B8C8", VA = "0x10135B8C8", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x0400446C RID: 17516
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003077")]
		public int m_BuildPointID;

		// Token: 0x0400446D RID: 17517
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003078")]
		public int m_ObjID;

		// Token: 0x0400446E RID: 17518
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003079")]
		public long m_ManageID;
	}
}
