﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BA7 RID: 2983
	[Token(Token = "0x200081C")]
	[StructLayout(3)]
	public class TownBuildChangeBufParam : ITownEventCommand
	{
		// Token: 0x06003459 RID: 13401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC2")]
		[Address(RVA = "0x10135BAFC", Offset = "0x135BAFC", VA = "0x10135BAFC")]
		public TownBuildChangeBufParam(int fbuildpoint, long fmanageid, int fobjid, [Optional] TownBuildChangeBufParam.BuildUpEvent pcallback)
		{
		}

		// Token: 0x0600345A RID: 13402 RVA: 0x00016368 File Offset: 0x00014568
		[Token(Token = "0x6002FC3")]
		[Address(RVA = "0x10135BB64", Offset = "0x135BB64", VA = "0x10135BB64", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x0400446F RID: 17519
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400307A")]
		public int m_BuildPointID;

		// Token: 0x04004470 RID: 17520
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400307B")]
		public int m_ObjID;

		// Token: 0x04004471 RID: 17521
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400307C")]
		public long m_ManageID;

		// Token: 0x04004472 RID: 17522
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400307D")]
		public TownBuildChangeBufParam.eStep m_Step;

		// Token: 0x04004473 RID: 17523
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400307E")]
		public bool m_InitUp;

		// Token: 0x04004474 RID: 17524
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400307F")]
		public TownBuildChangeBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000BA8 RID: 2984
		[Token(Token = "0x2001058")]
		public enum eStep
		{
			// Token: 0x04004476 RID: 17526
			[Token(Token = "0x4006762")]
			BuildQue,
			// Token: 0x04004477 RID: 17527
			[Token(Token = "0x4006763")]
			SetUpCheck
		}

		// Token: 0x02000BA9 RID: 2985
		// (Invoke) Token: 0x0600345C RID: 13404
		[Token(Token = "0x2001059")]
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
