﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BA5 RID: 2981
	[Token(Token = "0x200081A")]
	[StructLayout(3)]
	public class TownBuildCrossFade : MonoBehaviour
	{
		// Token: 0x06003451 RID: 13393 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FBA")]
		[Address(RVA = "0x10135C540", Offset = "0x135C540", VA = "0x10135C540")]
		public static TownBuildCrossFade CreateFade(TownBuilder pbuilder)
		{
			return null;
		}

		// Token: 0x06003452 RID: 13394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FBB")]
		[Address(RVA = "0x10135C62C", Offset = "0x135C62C", VA = "0x10135C62C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003453 RID: 13395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FBC")]
		[Address(RVA = "0x10135C630", Offset = "0x135C630", VA = "0x10135C630")]
		public void CreateCaptureCamera()
		{
		}

		// Token: 0x06003454 RID: 13396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FBD")]
		[Address(RVA = "0x10135C5E4", Offset = "0x135C5E4", VA = "0x10135C5E4")]
		public void SetUp(TownBuilder pbuilder)
		{
		}

		// Token: 0x06003455 RID: 13397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FBE")]
		[Address(RVA = "0x10135CE3C", Offset = "0x135CE3C", VA = "0x10135CE3C")]
		private void Update()
		{
		}

		// Token: 0x06003456 RID: 13398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FBF")]
		[Address(RVA = "0x10135D59C", Offset = "0x135D59C", VA = "0x10135D59C")]
		public TownBuildCrossFade()
		{
		}

		// Token: 0x04004463 RID: 17507
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400306E")]
		private int m_Step;

		// Token: 0x04004464 RID: 17508
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400306F")]
		private Camera m_RenderCamera;

		// Token: 0x04004465 RID: 17509
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003070")]
		private GameObject m_CaptureCamera;

		// Token: 0x04004466 RID: 17510
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003071")]
		private Material m_FadeMtl;

		// Token: 0x04004467 RID: 17511
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003072")]
		private RenderTexture m_RenderTarget;

		// Token: 0x04004468 RID: 17512
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003073")]
		private RenderTexture m_RenderDepth;

		// Token: 0x04004469 RID: 17513
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003074")]
		private TownBuilder m_Builder;

		// Token: 0x0400446A RID: 17514
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003075")]
		private TownRenderSync m_RenderSync;

		// Token: 0x0400446B RID: 17515
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003076")]
		private float m_FadeTime;
	}
}
