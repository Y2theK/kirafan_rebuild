﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B89 RID: 2953
	[Token(Token = "0x2000807")]
	[StructLayout(3)]
	public class TownBuildLinkPoint
	{
		// Token: 0x060033F8 RID: 13304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F66")]
		[Address(RVA = "0x10135D5A4", Offset = "0x135D5A4", VA = "0x10135D5A4")]
		public TownBuildLinkPoint(eBuildPointCategory ftype)
		{
		}

		// Token: 0x060033F9 RID: 13305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F67")]
		[Address(RVA = "0x10135D5E0", Offset = "0x135D5E0", VA = "0x10135D5E0")]
		public void ChangeManageID(long fmanageid)
		{
		}

		// Token: 0x060033FA RID: 13306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F68")]
		[Address(RVA = "0x10135D5E8", Offset = "0x135D5E8", VA = "0x10135D5E8")]
		public void SetBuilding(bool fset)
		{
		}

		// Token: 0x060033FB RID: 13307 RVA: 0x00015FF0 File Offset: 0x000141F0
		[Token(Token = "0x6002F69")]
		[Address(RVA = "0x10135D5F0", Offset = "0x135D5F0", VA = "0x10135D5F0")]
		public bool IsBuilding()
		{
			return default(bool);
		}

		// Token: 0x060033FC RID: 13308 RVA: 0x00016008 File Offset: 0x00014208
		[Token(Token = "0x6002F6A")]
		[Address(RVA = "0x10135D5F8", Offset = "0x135D5F8", VA = "0x10135D5F8")]
		public eBuildPointCategory GetCategory()
		{
			return eBuildPointCategory.Area;
		}

		// Token: 0x060033FD RID: 13309 RVA: 0x00016020 File Offset: 0x00014220
		[Token(Token = "0x6002F6B")]
		[Address(RVA = "0x10135D600", Offset = "0x135D600", VA = "0x10135D600")]
		public int GetAccessID()
		{
			return 0;
		}

		// Token: 0x060033FE RID: 13310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F6C")]
		[Address(RVA = "0x10135D608", Offset = "0x135D608", VA = "0x10135D608")]
		public void Release()
		{
		}

		// Token: 0x040043E3 RID: 17379
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003010")]
		public eBuildPointCategory m_Type;

		// Token: 0x040043E4 RID: 17380
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4003011")]
		public int m_AccessID;

		// Token: 0x040043E5 RID: 17381
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003012")]
		public long m_AccessMngID;

		// Token: 0x040043E6 RID: 17382
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003013")]
		public bool m_Build;

		// Token: 0x040043E7 RID: 17383
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003014")]
		public GameObject m_Object;

		// Token: 0x040043E8 RID: 17384
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003015")]
		public TownBuildPakage m_Pakage;
	}
}
