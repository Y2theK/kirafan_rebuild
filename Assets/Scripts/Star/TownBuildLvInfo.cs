﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000BED RID: 3053
	[Token(Token = "0x200083B")]
	[StructLayout(3)]
	public class TownBuildLvInfo : MonoBehaviour
	{
		// Token: 0x06003593 RID: 13715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030B8")]
		[Address(RVA = "0x10135D7E0", Offset = "0x135D7E0", VA = "0x10135D7E0")]
		public void SetData(long buildMngID, TownBuilder builder)
		{
		}

		// Token: 0x06003594 RID: 13716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030B9")]
		[Address(RVA = "0x10135D7E8", Offset = "0x135D7E8", VA = "0x10135D7E8")]
		public void UpdateInfo()
		{
		}

		// Token: 0x06003595 RID: 13717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030BA")]
		[Address(RVA = "0x10135D9AC", Offset = "0x135D9AC", VA = "0x10135D9AC")]
		public void SetActive(bool flg)
		{
		}

		// Token: 0x06003596 RID: 13718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030BB")]
		[Address(RVA = "0x10135DA98", Offset = "0x135DA98", VA = "0x10135DA98")]
		public void DestoryRequest()
		{
		}

		// Token: 0x06003597 RID: 13719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60030BC")]
		[Address(RVA = "0x10135DB10", Offset = "0x135DB10", VA = "0x10135DB10")]
		public TownBuildLvInfo()
		{
		}

		// Token: 0x040045C6 RID: 17862
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003117")]
		[SerializeField]
		private Text m_txtLvInfo;

		// Token: 0x040045C7 RID: 17863
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003118")]
		private long m_mngID;

		// Token: 0x040045C8 RID: 17864
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003119")]
		private TownBuilder m_Builder;
	}
}
