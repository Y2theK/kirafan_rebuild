﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BAA RID: 2986
	[Token(Token = "0x200081D")]
	[StructLayout(3)]
	public class TownBuildMoveAreaParam : ITownEventCommand
	{
		// Token: 0x0600345F RID: 13407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC4")]
		[Address(RVA = "0x10135DB20", Offset = "0x135DB20", VA = "0x10135DB20")]
		public TownBuildMoveAreaParam(int fbackpoint, int fbuildpoint, long fmanageid, int fobjid)
		{
		}

		// Token: 0x06003460 RID: 13408 RVA: 0x00016380 File Offset: 0x00014580
		[Token(Token = "0x6002FC5")]
		[Address(RVA = "0x10135DB84", Offset = "0x135DB84", VA = "0x10135DB84", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x04004478 RID: 17528
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003080")]
		public int m_BackBuildPoint;

		// Token: 0x04004479 RID: 17529
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003081")]
		public int m_BuildPointID;

		// Token: 0x0400447A RID: 17530
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003082")]
		public int m_ObjID;

		// Token: 0x0400447B RID: 17531
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003083")]
		public long m_ManageID;

		// Token: 0x0400447C RID: 17532
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003084")]
		public TownBuildMoveAreaParam.eStep m_Step;

		// Token: 0x02000BAB RID: 2987
		[Token(Token = "0x200105A")]
		public enum eStep
		{
			// Token: 0x0400447E RID: 17534
			[Token(Token = "0x4006765")]
			BuildQue,
			// Token: 0x0400447F RID: 17535
			[Token(Token = "0x4006766")]
			SetUpCheck,
			// Token: 0x04004480 RID: 17536
			[Token(Token = "0x4006767")]
			Final
		}
	}
}
