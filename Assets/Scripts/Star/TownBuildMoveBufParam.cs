﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BAC RID: 2988
	[Token(Token = "0x200081E")]
	[StructLayout(3)]
	public class TownBuildMoveBufParam : ITownEventCommand
	{
		// Token: 0x06003461 RID: 13409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC6")]
		[Address(RVA = "0x10135E038", Offset = "0x135E038", VA = "0x10135E038")]
		public TownBuildMoveBufParam(int fbackpoint, int fbuildpoint, long fmanageid, int fobjid, int fsynckey = -1, [Optional] TownBuildMoveBufParam.BuildUpEvent pcallback)
		{
		}

		// Token: 0x06003462 RID: 13410 RVA: 0x00016398 File Offset: 0x00014598
		[Token(Token = "0x6002FC7")]
		[Address(RVA = "0x10135E0B0", Offset = "0x135E0B0", VA = "0x10135E0B0", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x04004481 RID: 17537
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003085")]
		public int m_BackBuildPoint;

		// Token: 0x04004482 RID: 17538
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003086")]
		public int m_BuildPointID;

		// Token: 0x04004483 RID: 17539
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003087")]
		public int m_ObjID;

		// Token: 0x04004484 RID: 17540
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003088")]
		public long m_ManageID;

		// Token: 0x04004485 RID: 17541
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003089")]
		public int m_SyncKey;

		// Token: 0x04004486 RID: 17542
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400308A")]
		public TownBuildMoveBufParam.eStep m_Step;

		// Token: 0x04004487 RID: 17543
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400308B")]
		public TownBuildMoveBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000BAD RID: 2989
		[Token(Token = "0x200105B")]
		public enum eStep
		{
			// Token: 0x04004489 RID: 17545
			[Token(Token = "0x4006769")]
			BuildQue,
			// Token: 0x0400448A RID: 17546
			[Token(Token = "0x400676A")]
			SetUpCheck,
			// Token: 0x0400448B RID: 17547
			[Token(Token = "0x400676B")]
			Final
		}

		// Token: 0x02000BAE RID: 2990
		// (Invoke) Token: 0x06003464 RID: 13412
		[Token(Token = "0x200105C")]
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
