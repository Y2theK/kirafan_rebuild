﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B84 RID: 2948
	[Token(Token = "0x2000804")]
	[StructLayout(3)]
	public class TownBuildPakage : MonoBehaviour
	{
		// Token: 0x060033E5 RID: 13285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F54")]
		[Address(RVA = "0x10135E8A0", Offset = "0x135E8A0", VA = "0x10135E8A0")]
		private void Awake()
		{
		}

		// Token: 0x060033E6 RID: 13286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F55")]
		[Address(RVA = "0x10135E90C", Offset = "0x135E90C", VA = "0x10135E90C")]
		private void StackReq(TownBuildPakage.eStep fmode, ITownObjectHandler phandle)
		{
		}

		// Token: 0x060033E7 RID: 13287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F56")]
		[Address(RVA = "0x10135E9C4", Offset = "0x135E9C4", VA = "0x10135E9C4")]
		public void SetLinkHandle(ITownObjectHandler plink)
		{
		}

		// Token: 0x060033E8 RID: 13288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F57")]
		[Address(RVA = "0x10135E9F8", Offset = "0x135E9F8", VA = "0x10135E9F8")]
		private void Update()
		{
		}

		// Token: 0x060033E9 RID: 13289 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F58")]
		[Address(RVA = "0x10135BAF4", Offset = "0x135BAF4", VA = "0x10135BAF4")]
		public ITownObjectHandler GetHandle()
		{
			return null;
		}

		// Token: 0x060033EA RID: 13290 RVA: 0x00015F90 File Offset: 0x00014190
		[Token(Token = "0x6002F59")]
		[Address(RVA = "0x10135EF00", Offset = "0x135EF00", VA = "0x10135EF00")]
		public bool IsFreeArea()
		{
			return default(bool);
		}

		// Token: 0x060033EB RID: 13291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F5A")]
		[Address(RVA = "0x10135DCD4", Offset = "0x135DCD4", VA = "0x10135DCD4")]
		public void BackBuildMode()
		{
		}

		// Token: 0x060033EC RID: 13292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F5B")]
		[Address(RVA = "0x10135EF70", Offset = "0x135EF70", VA = "0x10135EF70")]
		public void DestroyBuildMode()
		{
		}

		// Token: 0x060033ED RID: 13293 RVA: 0x00015FA8 File Offset: 0x000141A8
		[Token(Token = "0x6002F5C")]
		[Address(RVA = "0x10135EFA4", Offset = "0x135EFA4", VA = "0x10135EFA4")]
		public bool IsSetUpHandle(long ptargetmngid)
		{
			return default(bool);
		}

		// Token: 0x060033EE RID: 13294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F5D")]
		[Address(RVA = "0x10135D694", Offset = "0x135D694", VA = "0x10135D694")]
		public void Release()
		{
		}

		// Token: 0x060033EF RID: 13295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F5E")]
		[Address(RVA = "0x10135F078", Offset = "0x135F078", VA = "0x10135F078")]
		public TownBuildPakage()
		{
		}

		// Token: 0x040043C9 RID: 17353
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002FFD")]
		public ITownObjectHandler m_CtrlHandle;

		// Token: 0x040043CA RID: 17354
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002FFE")]
		public ITownObjectHandler m_BackNode;

		// Token: 0x040043CB RID: 17355
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002FFF")]
		public int m_Index;

		// Token: 0x040043CC RID: 17356
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003000")]
		public int m_Group;

		// Token: 0x040043CD RID: 17357
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003001")]
		public int m_Layer;

		// Token: 0x040043CE RID: 17358
		[Token(Token = "0x4003002")]
		private const short MAX_TABLE = 4;

		// Token: 0x040043CF RID: 17359
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003003")]
		private TownBuildPakage.eStep m_Mode;

		// Token: 0x040043D0 RID: 17360
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003004")]
		public TownBuildPakage.HandleChangeReq[] m_ReqTable;

		// Token: 0x040043D1 RID: 17361
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003005")]
		public short m_ReqNum;

		// Token: 0x02000B85 RID: 2949
		[Token(Token = "0x200104D")]
		public enum eStep
		{
			// Token: 0x040043D3 RID: 17363
			[Token(Token = "0x4006739")]
			Non,
			// Token: 0x040043D4 RID: 17364
			[Token(Token = "0x400673A")]
			ChangeHandle,
			// Token: 0x040043D5 RID: 17365
			[Token(Token = "0x400673B")]
			BackHandle,
			// Token: 0x040043D6 RID: 17366
			[Token(Token = "0x400673C")]
			DestroyQue
		}

		// Token: 0x02000B86 RID: 2950
		[Token(Token = "0x200104E")]
		public struct HandleChangeReq
		{
			// Token: 0x060033F0 RID: 13296 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600614A")]
			[Address(RVA = "0x100036438", Offset = "0x36438", VA = "0x100036438")]
			public void Clear()
			{
			}

			// Token: 0x040043D7 RID: 17367
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400673D")]
			public TownBuildPakage.eStep m_NextMode;

			// Token: 0x040043D8 RID: 17368
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400673E")]
			public ITownObjectHandler m_Handle;
		}
	}
}
