﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B87 RID: 2951
	[Token(Token = "0x2000805")]
	[StructLayout(3)]
	public class TownBuildPoint : MonoBehaviour
	{
		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x060033F1 RID: 13297 RVA: 0x00015FC0 File Offset: 0x000141C0
		[Token(Token = "0x170003A4")]
		public int Index
		{
			[Token(Token = "0x6002F5F")]
			[Address(RVA = "0x10135F080", Offset = "0x135F080", VA = "0x10135F080")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x060033F2 RID: 13298 RVA: 0x00015FD8 File Offset: 0x000141D8
		[Token(Token = "0x170003A5")]
		public int OrderInLayer
		{
			[Token(Token = "0x6002F60")]
			[Address(RVA = "0x10135F088", Offset = "0x135F088", VA = "0x10135F088")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060033F3 RID: 13299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F61")]
		[Address(RVA = "0x10135F090", Offset = "0x135F090", VA = "0x10135F090")]
		private void Awake()
		{
		}

		// Token: 0x060033F4 RID: 13300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F62")]
		[Address(RVA = "0x10135F094", Offset = "0x135F094", VA = "0x10135F094")]
		private void Start()
		{
		}

		// Token: 0x060033F5 RID: 13301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F63")]
		[Address(RVA = "0x10135F09C", Offset = "0x135F09C", VA = "0x10135F09C")]
		public void SetEnable(bool flg)
		{
		}

		// Token: 0x060033F6 RID: 13302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F64")]
		[Address(RVA = "0x10135F5A8", Offset = "0x135F5A8", VA = "0x10135F5A8")]
		private void SetEnableColor(float value)
		{
		}

		// Token: 0x060033F7 RID: 13303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F65")]
		[Address(RVA = "0x10135F670", Offset = "0x135F670", VA = "0x10135F670")]
		public TownBuildPoint()
		{
		}

		// Token: 0x040043D9 RID: 17369
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003006")]
		[SerializeField]
		private SpriteRenderer m_Sprite;

		// Token: 0x040043DA RID: 17370
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003007")]
		[SerializeField]
		private TownBuildArrow m_Arrow;

		// Token: 0x040043DB RID: 17371
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003008")]
		[SerializeField]
		private Collider m_Collider;

		// Token: 0x040043DC RID: 17372
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003009")]
		[SerializeField]
		private int m_Index;

		// Token: 0x040043DD RID: 17373
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400300A")]
		[SerializeField]
		private int m_OrderInLayer;
	}
}
