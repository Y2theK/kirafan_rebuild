﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B7C RID: 2940
	[Token(Token = "0x2000801")]
	[StructLayout(3)]
	public class TownBuildPointDB : ScriptableObject
	{
		// Token: 0x06003381 RID: 13185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EFF")]
		[Address(RVA = "0x10135F678", Offset = "0x135F678", VA = "0x10135F678")]
		public TownBuildPointDB()
		{
		}

		// Token: 0x04004385 RID: 17285
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002FD7")]
		public TownBuildPointDB_Param[] m_Params;
	}
}
