﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B7B RID: 2939
	[Token(Token = "0x2000800")]
	[Serializable]
	[StructLayout(0, Size = 40)]
	public struct TownBuildPointDB_Param
	{
		// Token: 0x0400437C RID: 17276
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002FCE")]
		public int m_Group;

		// Token: 0x0400437D RID: 17277
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4002FCF")]
		public ushort m_AttachType;

		// Token: 0x0400437E RID: 17278
		[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
		[Token(Token = "0x4002FD0")]
		public ushort m_ActiveNum;

		// Token: 0x0400437F RID: 17279
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4002FD1")]
		public string m_MakeID;

		// Token: 0x04004380 RID: 17280
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002FD2")]
		public float m_ObjOffsetX;

		// Token: 0x04004381 RID: 17281
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002FD3")]
		public float m_ObjOffsetY;

		// Token: 0x04004382 RID: 17282
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002FD4")]
		public int m_AccessKey;

		// Token: 0x04004383 RID: 17283
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002FD5")]
		public int m_Layer;

		// Token: 0x04004384 RID: 17284
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002FD6")]
		public int m_WakeUpKey;
	}
}
