﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B7D RID: 2941
	[Token(Token = "0x2000802")]
	[StructLayout(3)]
	public class TownBuildPointUtil : IDataBaseResource
	{
		// Token: 0x06003382 RID: 13186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F00")]
		[Address(RVA = "0x10135F680", Offset = "0x135F680", VA = "0x10135F680", Slot = "4")]
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
		}

		// Token: 0x06003383 RID: 13187 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F01")]
		[Address(RVA = "0x10135F710", Offset = "0x135F710", VA = "0x10135F710")]
		public TownBuildPointDB GetDatabase()
		{
			return null;
		}

		// Token: 0x06003384 RID: 13188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F02")]
		[Address(RVA = "0x10135F718", Offset = "0x135F718", VA = "0x10135F718")]
		public static void DatabaseSetUp()
		{
		}

		// Token: 0x06003385 RID: 13189 RVA: 0x00015D38 File Offset: 0x00013F38
		[Token(Token = "0x6002F03")]
		[Address(RVA = "0x10135F7D4", Offset = "0x135F7D4", VA = "0x10135F7D4")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x06003386 RID: 13190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F04")]
		[Address(RVA = "0x10135F834", Offset = "0x135F834", VA = "0x10135F834")]
		public static void DatabaseRelease()
		{
		}

		// Token: 0x06003387 RID: 13191 RVA: 0x00015D50 File Offset: 0x00013F50
		[Token(Token = "0x6002F05")]
		[Address(RVA = "0x10135F89C", Offset = "0x135F89C", VA = "0x10135F89C")]
		public static int SearchBuildPointDB(string fname)
		{
			return 0;
		}

		// Token: 0x06003388 RID: 13192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F06")]
		[Address(RVA = "0x10135F9C4", Offset = "0x135F9C4", VA = "0x10135F9C4")]
		public static void GetBuildPointDB(out TownBuildPointDB_Param param, int findex)
		{
		}

		// Token: 0x06003389 RID: 13193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F07")]
		[Address(RVA = "0x10135FA84", Offset = "0x135FA84", VA = "0x10135FA84")]
		public static void RepointBuildKey(ref TownBuildPointDB_Param param, Transform ptrs)
		{
		}

		// Token: 0x0600338A RID: 13194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F08")]
		[Address(RVA = "0x10135F7CC", Offset = "0x135F7CC", VA = "0x10135F7CC")]
		public TownBuildPointUtil()
		{
		}

		// Token: 0x04004386 RID: 17286
		[Token(Token = "0x4002FD8")]
		public static TownBuildPointUtil ms_BuildPointDB;

		// Token: 0x04004387 RID: 17287
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002FD9")]
		private TownBuildPointDB m_DB;
	}
}
