﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B8A RID: 2954
	[Token(Token = "0x2000808")]
	[StructLayout(3)]
	public class TownBuildTree
	{
		// Token: 0x060033FF RID: 13311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F6D")]
		[Address(RVA = "0x10135FB0C", Offset = "0x135FB0C", VA = "0x10135FB0C")]
		public TownBuildTree(TownBuilder pbuilder)
		{
		}

		// Token: 0x06003400 RID: 13312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F6E")]
		[Address(RVA = "0x10135FB90", Offset = "0x135FB90", VA = "0x10135FB90")]
		public void AddBuildPoint(TownBuildLinkPoint ppoint)
		{
		}

		// Token: 0x06003401 RID: 13313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F6F")]
		[Address(RVA = "0x10135FCC4", Offset = "0x135FCC4", VA = "0x10135FCC4")]
		public void DeletePoint(int fbuildpoint)
		{
		}

		// Token: 0x06003402 RID: 13314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F70")]
		[Address(RVA = "0x10135FF24", Offset = "0x135FF24", VA = "0x10135FF24")]
		public void Destroy()
		{
		}

		// Token: 0x06003403 RID: 13315 RVA: 0x00016038 File Offset: 0x00014238
		[Token(Token = "0x6002F71")]
		[Address(RVA = "0x10135FFF4", Offset = "0x135FFF4", VA = "0x10135FFF4")]
		public int GetTreeNodeNum()
		{
			return 0;
		}

		// Token: 0x06003404 RID: 13316 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F72")]
		[Address(RVA = "0x10135B9EC", Offset = "0x135B9EC", VA = "0x10135B9EC")]
		public TownBuildLinkPoint GetBuildPointTreeNode(int fbuildpoint)
		{
			return null;
		}

		// Token: 0x06003405 RID: 13317 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F73")]
		[Address(RVA = "0x101360054", Offset = "0x1360054", VA = "0x101360054")]
		public TownBuildLinkPoint GetBuildMngIDTreeNode(long fbuildmngid, int fbuildpoint = -1)
		{
			return null;
		}

		// Token: 0x06003406 RID: 13318 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F74")]
		[Address(RVA = "0x101360204", Offset = "0x1360204", VA = "0x101360204")]
		public TownBuildLinkPoint GetIndexToNode(int findex)
		{
			return null;
		}

		// Token: 0x06003407 RID: 13319 RVA: 0x00016050 File Offset: 0x00014250
		[Token(Token = "0x6002F75")]
		[Address(RVA = "0x101360274", Offset = "0x1360274", VA = "0x101360274")]
		private static int CompToBuildPoint(TownBuildLinkPoint pcomp1, TownBuildLinkPoint pcomp2)
		{
			return 0;
		}

		// Token: 0x06003408 RID: 13320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F76")]
		[Address(RVA = "0x10135FC2C", Offset = "0x135FC2C", VA = "0x10135FC2C")]
		public void SortToBuildPoint()
		{
		}

		// Token: 0x040043E9 RID: 17385
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003016")]
		private List<TownBuildLinkPoint> m_List;

		// Token: 0x040043EA RID: 17386
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003017")]
		private TownBuilder m_Builder;
	}
}
