﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B8B RID: 2955
	[Token(Token = "0x2000809")]
	[StructLayout(3)]
	public class TownBuildUpAreaParam : ITownEventCommand
	{
		// Token: 0x06003409 RID: 13321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F77")]
		[Address(RVA = "0x10135DD84", Offset = "0x135DD84", VA = "0x10135DD84")]
		public TownBuildUpAreaParam(int fbuildpoint, long fmanageid, int fobjid)
		{
		}

		// Token: 0x0600340A RID: 13322 RVA: 0x00016068 File Offset: 0x00014268
		[Token(Token = "0x6002F78")]
		[Address(RVA = "0x1013602D4", Offset = "0x13602D4", VA = "0x1013602D4", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x040043EB RID: 17387
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003018")]
		public int m_BuildPointID;

		// Token: 0x040043EC RID: 17388
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003019")]
		public int m_ObjID;

		// Token: 0x040043ED RID: 17389
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400301A")]
		public long m_ManageID;

		// Token: 0x040043EE RID: 17390
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400301B")]
		public TownBuildUpAreaParam.eStep m_Step;

		// Token: 0x02000B8C RID: 2956
		[Token(Token = "0x200104F")]
		public enum eStep
		{
			// Token: 0x040043F0 RID: 17392
			[Token(Token = "0x4006740")]
			BuildQue,
			// Token: 0x040043F1 RID: 17393
			[Token(Token = "0x4006741")]
			SetUpCheck
		}
	}
}
