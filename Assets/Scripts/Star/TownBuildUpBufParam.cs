﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B8D RID: 2957
	[Token(Token = "0x200080A")]
	[StructLayout(3)]
	public class TownBuildUpBufParam : ITownEventCommand
	{
		// Token: 0x0600340B RID: 13323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F79")]
		[Address(RVA = "0x10135BC5C", Offset = "0x135BC5C", VA = "0x10135BC5C")]
		public TownBuildUpBufParam(int fbuildpoint, long fmanageid, int fobjid, bool finitup = true, [Optional] TownBuildUpBufParam.BuildUpEvent pcallback)
		{
		}

		// Token: 0x0600340C RID: 13324 RVA: 0x00016080 File Offset: 0x00014280
		[Token(Token = "0x6002F7A")]
		[Address(RVA = "0x101360350", Offset = "0x1360350", VA = "0x101360350", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x040043F2 RID: 17394
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400301C")]
		public int m_BuildPointID;

		// Token: 0x040043F3 RID: 17395
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400301D")]
		public int m_ObjID;

		// Token: 0x040043F4 RID: 17396
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400301E")]
		public long m_ManageID;

		// Token: 0x040043F5 RID: 17397
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400301F")]
		public bool m_AccessLinkID;

		// Token: 0x040043F6 RID: 17398
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003020")]
		public bool m_FixModel;

		// Token: 0x040043F7 RID: 17399
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003021")]
		public TownBuildUpBufParam.eStep m_Step;

		// Token: 0x040043F8 RID: 17400
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003022")]
		public bool m_InitUp;

		// Token: 0x040043F9 RID: 17401
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003023")]
		public TownBuildUpBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000B8E RID: 2958
		[Token(Token = "0x2001050")]
		public enum eStep
		{
			// Token: 0x040043FB RID: 17403
			[Token(Token = "0x4006743")]
			BuildQue,
			// Token: 0x040043FC RID: 17404
			[Token(Token = "0x4006744")]
			SetUpCheck
		}

		// Token: 0x02000B8F RID: 2959
		// (Invoke) Token: 0x0600340E RID: 13326
		[Token(Token = "0x2001051")]
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
