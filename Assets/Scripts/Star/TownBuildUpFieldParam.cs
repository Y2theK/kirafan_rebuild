﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B90 RID: 2960
	[Token(Token = "0x200080B")]
	[StructLayout(3)]
	public class TownBuildUpFieldParam : ITownEventCommand
	{
		// Token: 0x06003411 RID: 13329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F7B")]
		[Address(RVA = "0x101360930", Offset = "0x1360930", VA = "0x101360930")]
		public TownBuildUpFieldParam(int fieldno, byte ftimeid, TownBuilder pbuilder)
		{
		}

		// Token: 0x06003412 RID: 13330 RVA: 0x00016098 File Offset: 0x00014298
		[Token(Token = "0x6002F7C")]
		[Address(RVA = "0x101360A88", Offset = "0x1360A88", VA = "0x101360A88", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x040043FD RID: 17405
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003024")]
		public int m_FieldNo;

		// Token: 0x040043FE RID: 17406
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003025")]
		public byte m_TimeID;

		// Token: 0x040043FF RID: 17407
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003026")]
		private ITownObjectHandler m_TownField;

		// Token: 0x02000B91 RID: 2961
		[Token(Token = "0x2001052")]
		public enum eStep
		{
			// Token: 0x04004401 RID: 17409
			[Token(Token = "0x4006746")]
			BuildQue,
			// Token: 0x04004402 RID: 17410
			[Token(Token = "0x4006747")]
			SetUpCheck
		}
	}
}
