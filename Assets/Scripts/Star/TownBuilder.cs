﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B7E RID: 2942
	[Token(Token = "0x2000803")]
	[StructLayout(3)]
	public class TownBuilder : MonoBehaviour
	{
		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x0600338B RID: 13195 RVA: 0x00015D68 File Offset: 0x00013F68
		// (set) Token: 0x0600338C RID: 13196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003A3")]
		public LocalSaveData.eTownBuildLvInfoState buffLvInfoState
		{
			[Token(Token = "0x6002F09")]
			[Address(RVA = "0x10135DA90", Offset = "0x135DA90", VA = "0x10135DA90")]
			get
			{
				return LocalSaveData.eTownBuildLvInfoState.Show;
			}
			[Token(Token = "0x6002F0A")]
			[Address(RVA = "0x101360B40", Offset = "0x1360B40", VA = "0x101360B40")]
			set
			{
			}
		}

		// Token: 0x0600338D RID: 13197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F0B")]
		[Address(RVA = "0x101360B48", Offset = "0x1360B48", VA = "0x101360B48")]
		public void SetOwner(TownMain owner)
		{
		}

		// Token: 0x0600338E RID: 13198 RVA: 0x00015D80 File Offset: 0x00013F80
		[Token(Token = "0x6002F0C")]
		[Address(RVA = "0x101360B50", Offset = "0x1360B50", VA = "0x101360B50")]
		public int GetSceneLevel()
		{
			return 0;
		}

		// Token: 0x0600338F RID: 13199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F0D")]
		[Address(RVA = "0x101360B10", Offset = "0x1360B10", VA = "0x101360B10")]
		public void SetFieldMap(ITownObjectHandler phandle)
		{
		}

		// Token: 0x06003390 RID: 13200 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F0E")]
		[Address(RVA = "0x10135B9C0", Offset = "0x135B9C0", VA = "0x10135B9C0")]
		public TownBuildTree GetBuildTree()
		{
			return null;
		}

		// Token: 0x06003391 RID: 13201 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F0F")]
		[Address(RVA = "0x101360CE0", Offset = "0x1360CE0", VA = "0x101360CE0")]
		public BindTownCharaMap GetCharaBind(long fmngid, bool fcreate = true)
		{
			return null;
		}

		// Token: 0x06003392 RID: 13202 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F10")]
		[Address(RVA = "0x101360E80", Offset = "0x1360E80", VA = "0x101360E80")]
		public GameObject GetMakerGauge()
		{
			return null;
		}

		// Token: 0x06003393 RID: 13203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F11")]
		[Address(RVA = "0x101361464", Offset = "0x1361464", VA = "0x101361464")]
		public void ReleaseMakerGauge(GameObject pobj)
		{
		}

		// Token: 0x06003394 RID: 13204 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F12")]
		[Address(RVA = "0x1013616B0", Offset = "0x13616B0", VA = "0x1013616B0")]
		public GameObject GetGeneralObj(eTownObjectType fobjid)
		{
			return null;
		}

		// Token: 0x06003395 RID: 13205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F13")]
		[Address(RVA = "0x1013616E8", Offset = "0x13616E8", VA = "0x1013616E8")]
		public void ReleaseCharaMarker(GameObject pobj)
		{
		}

		// Token: 0x06003396 RID: 13206 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F14")]
		[Address(RVA = "0x101361720", Offset = "0x1361720", VA = "0x101361720")]
		public GameObject GetCharaHudObj()
		{
			return null;
		}

		// Token: 0x06003397 RID: 13207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F15")]
		[Address(RVA = "0x101361758", Offset = "0x1361758", VA = "0x101361758")]
		public void ReleaseCharaHudObj(GameObject pobj)
		{
		}

		// Token: 0x06003398 RID: 13208 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F16")]
		[Address(RVA = "0x10136179C", Offset = "0x136179C", VA = "0x10136179C")]
		public GameObject GetMenuIcon(eTownMenuType ftype)
		{
			return null;
		}

		// Token: 0x06003399 RID: 13209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F17")]
		[Address(RVA = "0x1013617A4", Offset = "0x13617A4", VA = "0x1013617A4")]
		private void Awake()
		{
		}

		// Token: 0x0600339A RID: 13210 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F18")]
		[Address(RVA = "0x101361848", Offset = "0x1361848", VA = "0x101361848")]
		public TownBuilder.CheckTownFacilitiesList CheckTownFacilities()
		{
			return null;
		}

		// Token: 0x0600339B RID: 13211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F19")]
		[Address(RVA = "0x1013624C0", Offset = "0x13624C0", VA = "0x1013624C0")]
		private void Update()
		{
		}

		// Token: 0x0600339C RID: 13212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F1A")]
		[Address(RVA = "0x101364830", Offset = "0x1364830", VA = "0x101364830")]
		public void Initialize()
		{
		}

		// Token: 0x0600339D RID: 13213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F1B")]
		[Address(RVA = "0x101364C60", Offset = "0x1364C60", VA = "0x101364C60")]
		public void OnDestroy()
		{
		}

		// Token: 0x0600339E RID: 13214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F1C")]
		[Address(RVA = "0x101363DFC", Offset = "0x1363DFC", VA = "0x101363DFC")]
		private void ModelDestory()
		{
		}

		// Token: 0x0600339F RID: 13215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F1D")]
		[Address(RVA = "0x101364C68", Offset = "0x1364C68", VA = "0x101364C68")]
		public void Destroy()
		{
		}

		// Token: 0x060033A0 RID: 13216 RVA: 0x00015D98 File Offset: 0x00013F98
		[Token(Token = "0x6002F1E")]
		[Address(RVA = "0x101364E68", Offset = "0x1364E68", VA = "0x101364E68")]
		public bool Prepare()
		{
			return default(bool);
		}

		// Token: 0x060033A1 RID: 13217 RVA: 0x00015DB0 File Offset: 0x00013FB0
		[Token(Token = "0x6002F1F")]
		[Address(RVA = "0x101364F54", Offset = "0x1364F54", VA = "0x101364F54")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x060033A2 RID: 13218 RVA: 0x00015DC8 File Offset: 0x00013FC8
		[Token(Token = "0x6002F20")]
		[Address(RVA = "0x10135BF88", Offset = "0x135BF88", VA = "0x10135BF88")]
		public bool IsBuildMngIDToBuildUp(long fmanageid)
		{
			return default(bool);
		}

		// Token: 0x060033A3 RID: 13219 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F21")]
		[Address(RVA = "0x101364F64", Offset = "0x1364F64", VA = "0x101364F64")]
		public ITownObjectHandler GetBuildMngIDToHandle(long fmanageid)
		{
			return null;
		}

		// Token: 0x060033A4 RID: 13220 RVA: 0x00015DE0 File Offset: 0x00013FE0
		[Token(Token = "0x6002F22")]
		[Address(RVA = "0x10135BCC8", Offset = "0x135BCC8", VA = "0x10135BCC8")]
		public bool BuildToPoint(TownBuildUpBufParam pbuildup, bool finitup = false)
		{
			return default(bool);
		}

		// Token: 0x060033A5 RID: 13221 RVA: 0x00015DF8 File Offset: 0x00013FF8
		[Token(Token = "0x6002F23")]
		[Address(RVA = "0x10135DDD0", Offset = "0x135DDD0", VA = "0x10135DDD0")]
		public bool BuildToArea(TownBuildUpAreaParam pparam)
		{
			return default(bool);
		}

		// Token: 0x060033A6 RID: 13222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F24")]
		[Address(RVA = "0x101365080", Offset = "0x1365080", VA = "0x101365080")]
		private void CallbackBuildUp(int fbuildpoint, int objID, long fmanageid)
		{
		}

		// Token: 0x060033A7 RID: 13223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F25")]
		[Address(RVA = "0x1013652F8", Offset = "0x13652F8", VA = "0x1013652F8")]
		private void CallbackChangePoint(int buildpoint, int objID, long fmanageid)
		{
		}

		// Token: 0x1400005A RID: 90
		// (add) Token: 0x060033A8 RID: 13224 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060033A9 RID: 13225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005A")]
		public event Action<IMainComCommand> OnEventSend
		{
			[Token(Token = "0x6002F26")]
			[Address(RVA = "0x101365388", Offset = "0x1365388", VA = "0x101365388")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6002F27")]
			[Address(RVA = "0x101365494", Offset = "0x1365494", VA = "0x101365494")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060033AA RID: 13226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F28")]
		[Address(RVA = "0x101364FB4", Offset = "0x1364FB4", VA = "0x101364FB4")]
		private void CancelSelectObject()
		{
		}

		// Token: 0x060033AB RID: 13227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F29")]
		[Address(RVA = "0x1013655A0", Offset = "0x13655A0", VA = "0x1013655A0")]
		public void SetUIMode(bool fset)
		{
		}

		// Token: 0x060033AC RID: 13228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F2A")]
		[Address(RVA = "0x101365684", Offset = "0x1365684", VA = "0x101365684")]
		public void SetMenuActive(bool fmenuactive)
		{
		}

		// Token: 0x060033AD RID: 13229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F2B")]
		[Address(RVA = "0x10136569C", Offset = "0x136569C", VA = "0x10136569C")]
		public void SetMainMode(TownMain.eMode fmode)
		{
		}

		// Token: 0x060033AE RID: 13230 RVA: 0x00015E10 File Offset: 0x00014010
		[Token(Token = "0x6002F2C")]
		[Address(RVA = "0x101365800", Offset = "0x1365800", VA = "0x101365800")]
		public TownMain.eMode GetMode()
		{
			return TownMain.eMode.None;
		}

		// Token: 0x060033AF RID: 13231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F2D")]
		[Address(RVA = "0x10136351C", Offset = "0x136351C", VA = "0x10136351C")]
		public void UpdateMain()
		{
		}

		// Token: 0x060033B0 RID: 13232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F2E")]
		[Address(RVA = "0x101365CA0", Offset = "0x1365CA0", VA = "0x101365CA0")]
		private void ObjectSelectEventCmd(ITownObjectHandler phandle)
		{
		}

		// Token: 0x060033B1 RID: 13233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F2F")]
		[Address(RVA = "0x101365B54", Offset = "0x1365B54", VA = "0x101365B54")]
		private void CloseUIEventObject()
		{
		}

		// Token: 0x060033B2 RID: 13234 RVA: 0x00015E28 File Offset: 0x00014028
		[Token(Token = "0x6002F30")]
		[Address(RVA = "0x101365F40", Offset = "0x1365F40", VA = "0x101365F40")]
		public bool IsSelectableBufFacility(long managedID)
		{
			return default(bool);
		}

		// Token: 0x060033B3 RID: 13235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F31")]
		[Address(RVA = "0x1013660F8", Offset = "0x13660F8", VA = "0x1013660F8")]
		public void SetBuildPointsEnable(TownBuilder.eSelectType fselect, int fbuildID, long fmanageid = 0L, int fmaskbuildpoint = 0)
		{
		}

		// Token: 0x060033B4 RID: 13236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F32")]
		[Address(RVA = "0x101366568", Offset = "0x1366568", VA = "0x101366568")]
		public void ClearSelectUI()
		{
		}

		// Token: 0x060033B5 RID: 13237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F33")]
		[Address(RVA = "0x101364354", Offset = "0x1364354", VA = "0x101364354")]
		private void UpdateAreaSelect()
		{
		}

		// Token: 0x060033B6 RID: 13238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F34")]
		[Address(RVA = "0x101366668", Offset = "0x1366668", VA = "0x101366668")]
		private void DecideBuildAreaPoint(ITownObjectHandler buildPoint)
		{
		}

		// Token: 0x060033B7 RID: 13239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F35")]
		[Address(RVA = "0x1013667D8", Offset = "0x13667D8", VA = "0x1013667D8")]
		private void DecideSwapAreaPoint(ITownObjectHandler buildPoint)
		{
		}

		// Token: 0x060033B8 RID: 13240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F36")]
		[Address(RVA = "0x101366918", Offset = "0x1366918", VA = "0x101366918")]
		private void CloseSelectAreaMarker(int fselect, int ftarget)
		{
		}

		// Token: 0x060033B9 RID: 13241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F37")]
		[Address(RVA = "0x101363F54", Offset = "0x1363F54", VA = "0x101363F54")]
		private void UpdateBufSelect()
		{
		}

		// Token: 0x060033BA RID: 13242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F38")]
		[Address(RVA = "0x101366A88", Offset = "0x1366A88", VA = "0x101366A88")]
		private void DecideBuildBufPoint(ITownObjectHandler buildPoint)
		{
		}

		// Token: 0x060033BB RID: 13243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F39")]
		[Address(RVA = "0x101366BDC", Offset = "0x1366BDC", VA = "0x101366BDC")]
		private void DecideSwapBufPoint(ITownObjectHandler buildPoint)
		{
		}

		// Token: 0x060033BC RID: 13244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F3A")]
		[Address(RVA = "0x101364778", Offset = "0x1364778", VA = "0x101364778")]
		private void FlushEventRequest()
		{
		}

		// Token: 0x060033BD RID: 13245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F3B")]
		[Address(RVA = "0x101363184", Offset = "0x1363184", VA = "0x101363184")]
		public void BuildRequest(ITownEventCommand prequest)
		{
		}

		// Token: 0x060033BE RID: 13246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F3C")]
		[Address(RVA = "0x10135FEEC", Offset = "0x135FEEC", VA = "0x10135FEEC")]
		public void EventRequest(ITownEventCommand prequest)
		{
		}

		// Token: 0x060033BF RID: 13247 RVA: 0x00015E40 File Offset: 0x00014040
		[Token(Token = "0x6002F3D")]
		[Address(RVA = "0x101366D28", Offset = "0x1366D28", VA = "0x101366D28")]
		public int CreateSyncKey(int fuid = -1)
		{
			return 0;
		}

		// Token: 0x060033C0 RID: 13248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F3E")]
		[Address(RVA = "0x101366DFC", Offset = "0x1366DFC", VA = "0x101366DFC")]
		public void DelSyncKey(int fuid)
		{
		}

		// Token: 0x060033C1 RID: 13249 RVA: 0x00015E58 File Offset: 0x00014058
		[Token(Token = "0x6002F3F")]
		[Address(RVA = "0x10135E248", Offset = "0x135E248", VA = "0x10135E248")]
		public bool SetActiveSyncKey(int fuid)
		{
			return default(bool);
		}

		// Token: 0x060033C2 RID: 13250 RVA: 0x00015E70 File Offset: 0x00014070
		[Token(Token = "0x6002F40")]
		[Address(RVA = "0x101366F00", Offset = "0x1366F00", VA = "0x101366F00")]
		public bool IsActiveSyncKey(int fuid)
		{
			return default(bool);
		}

		// Token: 0x060033C3 RID: 13251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F41")]
		[Address(RVA = "0x10136700C", Offset = "0x136700C", VA = "0x10136700C")]
		public void SendHandleAction(ITownHandleAction pact, long ftarget = 0L)
		{
		}

		// Token: 0x060033C4 RID: 13252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F42")]
		[Address(RVA = "0x1013670B0", Offset = "0x13670B0", VA = "0x1013670B0")]
		public void RefreshIcon(UserFieldCharaData.UpFieldCharaData[] charaMngTable)
		{
		}

		// Token: 0x060033C5 RID: 13253 RVA: 0x00015E88 File Offset: 0x00014088
		[Token(Token = "0x6002F43")]
		[Address(RVA = "0x101367300", Offset = "0x1367300", VA = "0x101367300")]
		public bool StackEventQue(IMainComCommand pevent)
		{
			return default(bool);
		}

		// Token: 0x060033C6 RID: 13254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F44")]
		[Address(RVA = "0x101367368", Offset = "0x1367368", VA = "0x101367368")]
		public void ResetBuildPoint()
		{
		}

		// Token: 0x060033C7 RID: 13255 RVA: 0x00015EA0 File Offset: 0x000140A0
		[Token(Token = "0x6002F45")]
		[Address(RVA = "0x101367428", Offset = "0x1367428", VA = "0x101367428")]
		public bool IsEventActiveKey(int fkeyid)
		{
			return default(bool);
		}

		// Token: 0x060033C8 RID: 13256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F46")]
		[Address(RVA = "0x101367520", Offset = "0x1367520", VA = "0x101367520")]
		public void SetEventActeiveKey(int fkeyid, bool factive)
		{
		}

		// Token: 0x060033C9 RID: 13257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F47")]
		[Address(RVA = "0x101363C8C", Offset = "0x1363C8C", VA = "0x101363C8C")]
		private void CheckTimeBuilding()
		{
		}

		// Token: 0x060033CA RID: 13258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F48")]
		[Address(RVA = "0x10135D204", Offset = "0x135D204", VA = "0x10135D204")]
		public void PreLoadResource()
		{
		}

		// Token: 0x060033CB RID: 13259 RVA: 0x00015EB8 File Offset: 0x000140B8
		[Token(Token = "0x6002F49")]
		[Address(RVA = "0x10135D4B0", Offset = "0x135D4B0", VA = "0x10135D4B0")]
		public bool IsEndPreLoadResource()
		{
			return default(bool);
		}

		// Token: 0x060033CC RID: 13260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F4A")]
		[Address(RVA = "0x10135D4E0", Offset = "0x135D4E0", VA = "0x10135D4E0")]
		public void RebuildObject()
		{
		}

		// Token: 0x060033CD RID: 13261 RVA: 0x00015ED0 File Offset: 0x000140D0
		[Token(Token = "0x6002F4B")]
		[Address(RVA = "0x10135D58C", Offset = "0x135D58C", VA = "0x10135D58C")]
		public bool IsSetUpRebuild()
		{
			return default(bool);
		}

		// Token: 0x060033CE RID: 13262 RVA: 0x00015EE8 File Offset: 0x000140E8
		[Token(Token = "0x6002F4C")]
		[Address(RVA = "0x101367590", Offset = "0x1367590", VA = "0x101367590")]
		public bool IsHomeStep()
		{
			return default(bool);
		}

		// Token: 0x060033CF RID: 13263 RVA: 0x00015F00 File Offset: 0x00014100
		[Token(Token = "0x6002F4D")]
		[Address(RVA = "0x101365A68", Offset = "0x1365A68", VA = "0x101365A68")]
		public bool IsEnableTouchInHomeStep(ITownObjectHandler objectHandler)
		{
			return default(bool);
		}

		// Token: 0x060033D0 RID: 13264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F4E")]
		[Address(RVA = "0x101367648", Offset = "0x1367648", VA = "0x101367648")]
		public void SetEnableInputTownUI(bool flg)
		{
		}

		// Token: 0x060033D1 RID: 13265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F4F")]
		[Address(RVA = "0x101365F38", Offset = "0x1365F38", VA = "0x101365F38")]
		public void SetBuildLvViewFlg(bool flg)
		{
		}

		// Token: 0x060033D2 RID: 13266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F50")]
		[Address(RVA = "0x101365808", Offset = "0x1365808", VA = "0x101365808")]
		public void UpdateBuildLvViewFlgFromOutside()
		{
		}

		// Token: 0x060033D3 RID: 13267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F51")]
		[Address(RVA = "0x1013676E0", Offset = "0x13676E0", VA = "0x1013676E0")]
		public void UpdateBuildLvInfo()
		{
		}

		// Token: 0x060033D4 RID: 13268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F52")]
		[Address(RVA = "0x101367834", Offset = "0x1367834", VA = "0x101367834")]
		public TownBuilder()
		{
		}

		// Token: 0x04004388 RID: 17288
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002FDA")]
		private TownEventQue m_EventQue;

		// Token: 0x04004389 RID: 17289
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002FDB")]
		private List<TownBuilder.SyncResult> m_SyncResult;

		// Token: 0x0400438A RID: 17290
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002FDC")]
		private int m_SyncUID;

		// Token: 0x0400438B RID: 17291
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002FDD")]
		private TownResourceObjectData m_CharaCache;

		// Token: 0x0400438C RID: 17292
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002FDE")]
		private TownHudMessageControll m_HudMsgCtrl;

		// Token: 0x0400438D RID: 17293
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002FDF")]
		[SerializeField]
		private TownCacheObjectData m_MarkerCache;

		// Token: 0x0400438E RID: 17294
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002FE0")]
		private long m_UserTownMngID;

		// Token: 0x0400438F RID: 17295
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002FE1")]
		public TownBuilder.ActiveBuildPakage m_ActPakage;

		// Token: 0x04004390 RID: 17296
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002FE2")]
		public TownCamera m_GameCamera;

		// Token: 0x04004391 RID: 17297
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002FE3")]
		private TownCharaManager m_CharaManager;

		// Token: 0x04004392 RID: 17298
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002FE4")]
		private bool m_MenuActive;

		// Token: 0x04004393 RID: 17299
		[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
		[Token(Token = "0x4002FE5")]
		private bool m_MouseFirstTouch;

		// Token: 0x04004394 RID: 17300
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4002FE6")]
		private int m_MenuTouchLock;

		// Token: 0x04004395 RID: 17301
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002FE7")]
		private bool m_MenuBuildLvInfo;

		// Token: 0x04004396 RID: 17302
		[Cpp2IlInjected.FieldOffset(Offset = "0x71")]
		[Token(Token = "0x4002FE8")]
		private bool m_MenuBuildLvInfoOutside;

		// Token: 0x04004397 RID: 17303
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002FE9")]
		private LocalSaveData.eTownBuildLvInfoState m_buffLvInfoState;

		// Token: 0x04004398 RID: 17304
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002FEA")]
		public GameObject m_TraningIcon;

		// Token: 0x04004399 RID: 17305
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002FEB")]
		private TownEffectManager m_EffectMng;

		// Token: 0x0400439A RID: 17306
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002FEC")]
		[NonSerialized]
		public byte m_TimeCategory;

		// Token: 0x0400439B RID: 17307
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002FED")]
		public int m_TimeCheckHour;

		// Token: 0x0400439C RID: 17308
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002FEE")]
		public GameObject m_DebugTownData;

		// Token: 0x0400439D RID: 17309
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002FEF")]
		private Camera m_Camera;

		// Token: 0x0400439E RID: 17310
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002FF0")]
		private TownMain m_Owner;

		// Token: 0x0400439F RID: 17311
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002FF1")]
		private TownBuilder.eMode m_Mode;

		// Token: 0x040043A0 RID: 17312
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002FF2")]
		private ITownObjectHandler m_SelectingObj;

		// Token: 0x040043A1 RID: 17313
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002FF3")]
		private ITownObjectHandler m_BackSelectingObj;

		// Token: 0x040043A3 RID: 17315
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4002FF5")]
		private TownMain.eMode m_BackMode;

		// Token: 0x040043A4 RID: 17316
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4002FF6")]
		private int m_BuildBufID;

		// Token: 0x040043A5 RID: 17317
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4002FF7")]
		private int m_MarkBuildPoint;

		// Token: 0x040043A6 RID: 17318
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002FF8")]
		private long m_BuildManageID;

		// Token: 0x040043A7 RID: 17319
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002FF9")]
		private ITownObjectHandler m_DownBuildPoint;

		// Token: 0x040043A8 RID: 17320
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002FFA")]
		private bool m_IsPointOverUI;

		// Token: 0x040043A9 RID: 17321
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4002FFB")]
		private List<int> m_ActionKey;

		// Token: 0x040043AA RID: 17322
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4002FFC")]
		private TownResourceDownload m_Preload;

		// Token: 0x02000B7F RID: 2943
		[Token(Token = "0x2001048")]
		public enum eSelectType
		{
			// Token: 0x040043AC RID: 17324
			[Token(Token = "0x400671B")]
			FreeArea,
			// Token: 0x040043AD RID: 17325
			[Token(Token = "0x400671C")]
			MaskArea,
			// Token: 0x040043AE RID: 17326
			[Token(Token = "0x400671D")]
			FreeBufPoint,
			// Token: 0x040043AF RID: 17327
			[Token(Token = "0x400671E")]
			MaskBufPoint,
			// Token: 0x040043B0 RID: 17328
			[Token(Token = "0x400671F")]
			Close
		}

		// Token: 0x02000B80 RID: 2944
		[Token(Token = "0x2001049")]
		public class SyncResult
		{
			// Token: 0x060033D6 RID: 13270 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600613B")]
			[Address(RVA = "0x101366DF4", Offset = "0x1366DF4", VA = "0x101366DF4")]
			public SyncResult()
			{
			}

			// Token: 0x040043B1 RID: 17329
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006720")]
			public int m_UID;

			// Token: 0x040043B2 RID: 17330
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006721")]
			public bool m_Sync;
		}

		// Token: 0x02000B81 RID: 2945
		[Token(Token = "0x200104A")]
		public class ActiveBuildPakage
		{
			// Token: 0x060033D7 RID: 13271 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600613C")]
			[Address(RVA = "0x101362F74", Offset = "0x1362F74", VA = "0x101362F74")]
			public ActiveBuildPakage()
			{
			}

			// Token: 0x040043B3 RID: 17331
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006722")]
			public TownBuildTree m_BuildTrees;

			// Token: 0x040043B4 RID: 17332
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006723")]
			public ITownObjectHandler m_TownField;

			// Token: 0x040043B5 RID: 17333
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006724")]
			public List<BindTownCharaMap> m_CharaBuildBind;
		}

		// Token: 0x02000B82 RID: 2946
		[Token(Token = "0x200104B")]
		public enum eMode
		{
			// Token: 0x040043B7 RID: 17335
			[Token(Token = "0x4006726")]
			None = -1,
			// Token: 0x040043B8 RID: 17336
			[Token(Token = "0x4006727")]
			Prepare_First,
			// Token: 0x040043B9 RID: 17337
			[Token(Token = "0x4006728")]
			Prepare_VeriryBuildObjects,
			// Token: 0x040043BA RID: 17338
			[Token(Token = "0x4006729")]
			Prepare_VeriryBuildObjects_Wait,
			// Token: 0x040043BB RID: 17339
			[Token(Token = "0x400672A")]
			Prepare_FieldObject,
			// Token: 0x040043BC RID: 17340
			[Token(Token = "0x400672B")]
			Prepare_BuildObjects_Wait,
			// Token: 0x040043BD RID: 17341
			[Token(Token = "0x400672C")]
			Prepare_CharaObject_Wait,
			// Token: 0x040043BE RID: 17342
			[Token(Token = "0x400672D")]
			Prepare_Final,
			// Token: 0x040043BF RID: 17343
			[Token(Token = "0x400672E")]
			Main,
			// Token: 0x040043C0 RID: 17344
			[Token(Token = "0x400672F")]
			RebuildMode,
			// Token: 0x040043C1 RID: 17345
			[Token(Token = "0x4006730")]
			MenuMode,
			// Token: 0x040043C2 RID: 17346
			[Token(Token = "0x4006731")]
			FreeBufSelect,
			// Token: 0x040043C3 RID: 17347
			[Token(Token = "0x4006732")]
			BufSelect,
			// Token: 0x040043C4 RID: 17348
			[Token(Token = "0x4006733")]
			FreeAreaSelect,
			// Token: 0x040043C5 RID: 17349
			[Token(Token = "0x4006734")]
			AreaSelect,
			// Token: 0x040043C6 RID: 17350
			[Token(Token = "0x4006735")]
			Num
		}

		// Token: 0x02000B83 RID: 2947
		[Token(Token = "0x200104C")]
		public class CheckTownFacilitiesList
		{
			// Token: 0x060033D8 RID: 13272 RVA: 0x00015F18 File Offset: 0x00014118
			[Token(Token = "0x600613D")]
			[Address(RVA = "0x101362420", Offset = "0x1362420", VA = "0x101362420")]
			public bool IsExistData()
			{
				return default(bool);
			}

			// Token: 0x060033D9 RID: 13273 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600613E")]
			[Address(RVA = "0x1013622C8", Offset = "0x13622C8", VA = "0x1013622C8")]
			public void AddRemoveList(long managedID)
			{
			}

			// Token: 0x060033DA RID: 13274 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600613F")]
			[Address(RVA = "0x10136221C", Offset = "0x136221C", VA = "0x10136221C")]
			public void AddChangeList(long managedID)
			{
			}

			// Token: 0x060033DB RID: 13275 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006140")]
			[Address(RVA = "0x101367944", Offset = "0x1367944", VA = "0x101367944")]
			public void RemoveRemoveList(long managedID)
			{
			}

			// Token: 0x060033DC RID: 13276 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006141")]
			[Address(RVA = "0x101362374", Offset = "0x1362374", VA = "0x101362374")]
			public void RemoveChangeList(long managedID)
			{
			}

			// Token: 0x060033DD RID: 13277 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006142")]
			[Address(RVA = "0x1013679F0", Offset = "0x13679F0", VA = "0x1013679F0")]
			public void ClearRemoveList()
			{
			}

			// Token: 0x060033DE RID: 13278 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006143")]
			[Address(RVA = "0x101367A50", Offset = "0x1367A50", VA = "0x101367A50")]
			public void ClearChangeList()
			{
			}

			// Token: 0x060033DF RID: 13279 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006144")]
			[Address(RVA = "0x101367AB0", Offset = "0x1367AB0", VA = "0x101367AB0")]
			public void Clear()
			{
			}

			// Token: 0x060033E0 RID: 13280 RVA: 0x00015F30 File Offset: 0x00014130
			[Token(Token = "0x6006145")]
			[Address(RVA = "0x101363054", Offset = "0x1363054", VA = "0x101363054")]
			public int GetRemoveListNum()
			{
				return 0;
			}

			// Token: 0x060033E1 RID: 13281 RVA: 0x00015F48 File Offset: 0x00014148
			[Token(Token = "0x6006146")]
			[Address(RVA = "0x101363124", Offset = "0x1363124", VA = "0x101363124")]
			public int GetChangeListNum()
			{
				return 0;
			}

			// Token: 0x060033E2 RID: 13282 RVA: 0x00015F60 File Offset: 0x00014160
			[Token(Token = "0x6006147")]
			[Address(RVA = "0x1013630B4", Offset = "0x13630B4", VA = "0x1013630B4")]
			public long GetChangeListAt(int idx)
			{
				return 0L;
			}

			// Token: 0x060033E3 RID: 13283 RVA: 0x00015F78 File Offset: 0x00014178
			[Token(Token = "0x6006148")]
			[Address(RVA = "0x101362FE4", Offset = "0x1362FE4", VA = "0x101362FE4")]
			public long GetRemoveListAt(int idx)
			{
				return 0L;
			}

			// Token: 0x060033E4 RID: 13284 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006149")]
			[Address(RVA = "0x10136218C", Offset = "0x136218C", VA = "0x10136218C")]
			public CheckTownFacilitiesList()
			{
			}

			// Token: 0x040043C7 RID: 17351
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006736")]
			private List<long> m_RemoveList;

			// Token: 0x040043C8 RID: 17352
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006737")]
			private List<long> m_ChangeList;
		}
	}
}
