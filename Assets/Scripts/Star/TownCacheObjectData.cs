﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AF9 RID: 2809
	[Token(Token = "0x20007AD")]
	[Serializable]
	[StructLayout(3)]
	public class TownCacheObjectData
	{
		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x060031AF RID: 12719 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000395")]
		public GameObject Prefab
		{
			[Token(Token = "0x6002D6A")]
			[Address(RVA = "0x101367AD4", Offset = "0x1367AD4", VA = "0x101367AD4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x060031B0 RID: 12720 RVA: 0x000153C0 File Offset: 0x000135C0
		[Token(Token = "0x17000396")]
		public int CacheSize
		{
			[Token(Token = "0x6002D6B")]
			[Address(RVA = "0x101367ADC", Offset = "0x1367ADC", VA = "0x101367ADC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060031B1 RID: 12721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D6C")]
		[Address(RVA = "0x101367AE4", Offset = "0x1367AE4", VA = "0x101367AE4")]
		public void Initialize(GameObject prefab, int cacheSize, Transform parent)
		{
		}

		// Token: 0x060031B2 RID: 12722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D6D")]
		[Address(RVA = "0x1013648DC", Offset = "0x13648DC", VA = "0x1013648DC")]
		public void Initialize(Transform parent)
		{
		}

		// Token: 0x060031B3 RID: 12723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D6E")]
		[Address(RVA = "0x101367AFC", Offset = "0x1367AFC", VA = "0x101367AFC")]
		public void Destroy()
		{
		}

		// Token: 0x060031B4 RID: 12724 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D6F")]
		[Address(RVA = "0x101360EAC", Offset = "0x1360EAC", VA = "0x101360EAC")]
		public GameObject GetNextObjectInCache()
		{
			return null;
		}

		// Token: 0x060031B5 RID: 12725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D70")]
		[Address(RVA = "0x101361498", Offset = "0x1361498", VA = "0x101361498")]
		public void FreeObjectInCache(GameObject pobj)
		{
		}

		// Token: 0x060031B6 RID: 12726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D71")]
		[Address(RVA = "0x101367C38", Offset = "0x1367C38", VA = "0x101367C38")]
		public TownCacheObjectData()
		{
		}

		// Token: 0x04004124 RID: 16676
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E43")]
		[SerializeField]
		private GameObject m_Prefab;

		// Token: 0x04004125 RID: 16677
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E44")]
		[SerializeField]
		private int m_CacheSize;

		// Token: 0x04004126 RID: 16678
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E45")]
		private TownCacheObjectData.TCacheObjectState[] m_Objects;

		// Token: 0x04004127 RID: 16679
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E46")]
		private Transform m_Parent;

		// Token: 0x02000AFA RID: 2810
		[Token(Token = "0x2001019")]
		public class TCacheObjectState
		{
			// Token: 0x060031B7 RID: 12727 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060FE")]
			[Address(RVA = "0x101367AF4", Offset = "0x1367AF4", VA = "0x101367AF4")]
			public TCacheObjectState()
			{
			}

			// Token: 0x04004128 RID: 16680
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400664D")]
			public GameObject m_Object;

			// Token: 0x04004129 RID: 16681
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400664E")]
			public bool m_Active;
		}
	}
}
