﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B00 RID: 2816
	[Token(Token = "0x20007B2")]
	[StructLayout(3)]
	public class TownCamera : MonoBehaviour
	{
		// Token: 0x060031C2 RID: 12738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7B")]
		[Address(RVA = "0x101367C48", Offset = "0x1367C48", VA = "0x101367C48")]
		private void Awake()
		{
		}

		// Token: 0x060031C3 RID: 12739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7C")]
		[Address(RVA = "0x101367D88", Offset = "0x1367D88", VA = "0x101367D88")]
		private void Update()
		{
		}

		// Token: 0x060031C4 RID: 12740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7D")]
		[Address(RVA = "0x101368934", Offset = "0x1368934", VA = "0x101368934")]
		public void SetIsControllable(bool flg)
		{
		}

		// Token: 0x060031C5 RID: 12741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7E")]
		[Address(RVA = "0x101368960", Offset = "0x1368960", VA = "0x101368960")]
		public void PushPopBackUpData()
		{
		}

		// Token: 0x060031C6 RID: 12742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D7F")]
		[Address(RVA = "0x101368AC8", Offset = "0x1368AC8", VA = "0x101368AC8")]
		public void PopBackUpData(bool fnooverwrite)
		{
		}

		// Token: 0x060031C7 RID: 12743 RVA: 0x000153F0 File Offset: 0x000135F0
		[Token(Token = "0x6002D80")]
		[Address(RVA = "0x101368C30", Offset = "0x1368C30", VA = "0x101368C30")]
		public float GetSizeMax()
		{
			return 0f;
		}

		// Token: 0x060031C8 RID: 12744 RVA: 0x00015408 File Offset: 0x00013608
		[Token(Token = "0x6002D81")]
		[Address(RVA = "0x101368C38", Offset = "0x1368C38", VA = "0x101368C38")]
		public float GetSizePer()
		{
			return 0f;
		}

		// Token: 0x060031C9 RID: 12745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D82")]
		[Address(RVA = "0x101368C80", Offset = "0x1368C80", VA = "0x101368C80")]
		public void SetZoom(float size, bool force = true)
		{
		}

		// Token: 0x060031CA RID: 12746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D83")]
		[Address(RVA = "0x101368068", Offset = "0x1368068", VA = "0x101368068")]
		private void OnPinch(TownPinchState listener)
		{
		}

		// Token: 0x060031CB RID: 12747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D84")]
		[Address(RVA = "0x101369034", Offset = "0x1369034", VA = "0x101369034")]
		public void SetPositionKey(Vector2 fmovepos, float ftime)
		{
		}

		// Token: 0x060031CC RID: 12748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D85")]
		[Address(RVA = "0x1013692AC", Offset = "0x13692AC", VA = "0x1013692AC")]
		public void SetSizeKey(float fsize, float ftime, bool force = false)
		{
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x060031CD RID: 12749 RVA: 0x00015420 File Offset: 0x00013620
		// (set) Token: 0x060031CE RID: 12750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000397")]
		public bool IsDragging
		{
			[Token(Token = "0x6002D86")]
			[Address(RVA = "0x101365C90", Offset = "0x1365C90", VA = "0x101365C90")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002D87")]
			[Address(RVA = "0x10136894C", Offset = "0x136894C", VA = "0x10136894C")]
			set
			{
			}
		}

		// Token: 0x060031CF RID: 12751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D88")]
		[Address(RVA = "0x101369414", Offset = "0x1369414", VA = "0x101369414")]
		public void SetCameraMoveVec(Vector2 move)
		{
		}

		// Token: 0x060031D0 RID: 12752 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D89")]
		[Address(RVA = "0x10135CE34", Offset = "0x135CE34", VA = "0x10135CE34")]
		public Camera GetCamera()
		{
			return null;
		}

		// Token: 0x060031D1 RID: 12753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D8A")]
		[Address(RVA = "0x101369420", Offset = "0x1369420", VA = "0x101369420")]
		public void BuildMoveRange()
		{
		}

		// Token: 0x060031D2 RID: 12754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D8B")]
		[Address(RVA = "0x101369424", Offset = "0x1369424", VA = "0x101369424")]
		private void UpdateMoveRange()
		{
		}

		// Token: 0x060031D3 RID: 12755 RVA: 0x00015438 File Offset: 0x00013638
		[Token(Token = "0x6002D8C")]
		[Address(RVA = "0x101368FA0", Offset = "0x1368FA0", VA = "0x101368FA0")]
		private Rect CalcMoveRange(float orthographicSize)
		{
			return default(Rect);
		}

		// Token: 0x060031D4 RID: 12756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D8D")]
		[Address(RVA = "0x1013681DC", Offset = "0x13681DC", VA = "0x1013681DC")]
		private void UpdateMove()
		{
		}

		// Token: 0x060031D5 RID: 12757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D8E")]
		[Address(RVA = "0x10136946C", Offset = "0x136946C", VA = "0x10136946C")]
		public TownCamera()
		{
		}

		// Token: 0x04004138 RID: 16696
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E53")]
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04004139 RID: 16697
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E54")]
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x0400413A RID: 16698
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E55")]
		private TownPinchEvent m_PinchEventListener;

		// Token: 0x0400413B RID: 16699
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002E56")]
		private TownPinchState m_PinchState;

		// Token: 0x0400413C RID: 16700
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002E57")]
		[SerializeField]
		private float m_ThresholdMoveRatio;

		// Token: 0x0400413D RID: 16701
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002E58")]
		private float m_ThresholdMovePixel;

		// Token: 0x0400413E RID: 16702
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002E59")]
		private bool m_IsControllable;

		// Token: 0x0400413F RID: 16703
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E5A")]
		private TownCameraKeyPakage m_Anime;

		// Token: 0x04004140 RID: 16704
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E5B")]
		[SerializeField]
		private float m_SizeMin;

		// Token: 0x04004141 RID: 16705
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002E5C")]
		[SerializeField]
		private float m_SizeMax;

		// Token: 0x04004142 RID: 16706
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E5D")]
		private float m_SizeOnStartPinch;

		// Token: 0x04004143 RID: 16707
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002E5E")]
		private bool m_IsPinching;

		// Token: 0x04004144 RID: 16708
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002E5F")]
		[SerializeField]
		private float m_Inertia;

		// Token: 0x04004145 RID: 16709
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4002E60")]
		private Rect m_MoveRange;

		// Token: 0x04004146 RID: 16710
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002E61")]
		[SerializeField]
		public float m_RangeW;

		// Token: 0x04004147 RID: 16711
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002E62")]
		[SerializeField]
		public float m_RangeH;

		// Token: 0x04004148 RID: 16712
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4002E63")]
		private Vector2 m_vMove;

		// Token: 0x04004149 RID: 16713
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4002E64")]
		private TownCamera.eDragState m_DragState;

		// Token: 0x02000B01 RID: 2817
		[Token(Token = "0x200101B")]
		private enum eDragState
		{
			// Token: 0x0400414B RID: 16715
			[Token(Token = "0x4006652")]
			None = -1,
			// Token: 0x0400414C RID: 16716
			[Token(Token = "0x4006653")]
			Ready,
			// Token: 0x0400414D RID: 16717
			[Token(Token = "0x4006654")]
			Dragging
		}
	}
}
