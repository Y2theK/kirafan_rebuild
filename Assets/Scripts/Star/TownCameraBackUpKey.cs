﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B02 RID: 2818
	[Token(Token = "0x20007B3")]
	[StructLayout(3)]
	public class TownCameraBackUpKey : MonoBehaviour
	{
		// Token: 0x060031D6 RID: 12758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D8F")]
		[Address(RVA = "0x101369528", Offset = "0x1369528", VA = "0x101369528")]
		public TownCameraBackUpKey()
		{
		}

		// Token: 0x0400414E RID: 16718
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E65")]
		public float m_OrthoSize;

		// Token: 0x0400414F RID: 16719
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002E66")]
		public Vector3 m_BackPos;
	}
}
