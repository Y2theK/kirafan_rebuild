﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B03 RID: 2819
	[Token(Token = "0x20007B4")]
	[StructLayout(3)]
	public class TownCameraKeyPakage
	{
		// Token: 0x060031D7 RID: 12759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D90")]
		[Address(RVA = "0x101367D24", Offset = "0x1367D24", VA = "0x101367D24")]
		public TownCameraKeyPakage()
		{
		}

		// Token: 0x060031D8 RID: 12760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D91")]
		[Address(RVA = "0x1013691F4", Offset = "0x13691F4", VA = "0x1013691F4")]
		public void EntryAction(ITownCameraKey paction, int fuid)
		{
		}

		// Token: 0x060031D9 RID: 12761 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D92")]
		[Address(RVA = "0x101369530", Offset = "0x1369530", VA = "0x101369530")]
		public ITownCameraKey GetAction(int fuid)
		{
			return null;
		}

		// Token: 0x060031DA RID: 12762 RVA: 0x00015450 File Offset: 0x00013650
		[Token(Token = "0x6002D93")]
		[Address(RVA = "0x101367E20", Offset = "0x1367E20", VA = "0x101367E20")]
		public bool PakageUpdate()
		{
			return default(bool);
		}

		// Token: 0x060031DB RID: 12763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D94")]
		[Address(RVA = "0x101369608", Offset = "0x1369608", VA = "0x101369608")]
		public void Relase()
		{
		}

		// Token: 0x04004150 RID: 16720
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E67")]
		public int m_Num;

		// Token: 0x04004151 RID: 16721
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E68")]
		public ITownCameraKey[] m_Table;
	}
}
