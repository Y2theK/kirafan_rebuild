﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B04 RID: 2820
	[Token(Token = "0x20007B5")]
	[StructLayout(3)]
	public class TownCameraMove : ITownCameraKey
	{
		// Token: 0x060031DC RID: 12764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D95")]
		[Address(RVA = "0x10136916C", Offset = "0x136916C", VA = "0x10136916C")]
		public TownCameraMove(TownCamera ptarget, Vector3 fbasepos, Vector3 ftargetpos, float ftime)
		{
		}

		// Token: 0x060031DD RID: 12765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D96")]
		[Address(RVA = "0x101369674", Offset = "0x1369674", VA = "0x101369674")]
		public TownCameraMove(TownCamera ptarget, Vector3 fbasepos, Vector3 fpoint1, Vector3 fpoint2, Vector3 ftargetpos, float ftime)
		{
		}

		// Token: 0x060031DE RID: 12766 RVA: 0x00015468 File Offset: 0x00013668
		[Token(Token = "0x6002D97")]
		[Address(RVA = "0x101369738", Offset = "0x1369738", VA = "0x101369738", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004152 RID: 16722
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E69")]
		private Vector3 m_BasePos;

		// Token: 0x04004153 RID: 16723
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002E6A")]
		private Vector3 m_TargetPos;

		// Token: 0x04004154 RID: 16724
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002E6B")]
		private Vector3 m_Point1;

		// Token: 0x04004155 RID: 16725
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002E6C")]
		private Vector3 m_Point2;

		// Token: 0x04004156 RID: 16726
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E6D")]
		private float m_Time;

		// Token: 0x04004157 RID: 16727
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002E6E")]
		private float m_MaxTime;

		// Token: 0x04004158 RID: 16728
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002E6F")]
		private TownCameraMove.ePathType m_LineType;

		// Token: 0x02000B05 RID: 2821
		[Token(Token = "0x200101C")]
		private enum ePathType
		{
			// Token: 0x0400415A RID: 16730
			[Token(Token = "0x4006656")]
			Linear,
			// Token: 0x0400415B RID: 16731
			[Token(Token = "0x4006657")]
			Bez
		}
	}
}
