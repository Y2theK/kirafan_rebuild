﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B06 RID: 2822
	[Token(Token = "0x20007B6")]
	[StructLayout(3)]
	public class TownCameraSize : ITownCameraKey
	{
		// Token: 0x060031DF RID: 12767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D98")]
		[Address(RVA = "0x1013693B4", Offset = "0x13693B4", VA = "0x1013693B4")]
		public TownCameraSize(TownCamera ptarget, float fbasepos, float ftargetpos, float ftime)
		{
		}

		// Token: 0x060031E0 RID: 12768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D99")]
		[Address(RVA = "0x101369A18", Offset = "0x1369A18", VA = "0x101369A18")]
		public TownCameraSize(TownCamera ptarget, float fbasepos, float fpoint1, float fpoint2, float ftargetpos, float ftime)
		{
		}

		// Token: 0x060031E1 RID: 12769 RVA: 0x00015480 File Offset: 0x00013680
		[Token(Token = "0x6002D9A")]
		[Address(RVA = "0x101369A8C", Offset = "0x1369A8C", VA = "0x101369A8C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x0400415C RID: 16732
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E70")]
		private float m_BaseSize;

		// Token: 0x0400415D RID: 16733
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002E71")]
		private float m_TargetSize;

		// Token: 0x0400415E RID: 16734
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002E72")]
		private float m_Point1;

		// Token: 0x0400415F RID: 16735
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002E73")]
		private float m_Point2;

		// Token: 0x04004160 RID: 16736
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002E74")]
		private float m_Time;

		// Token: 0x04004161 RID: 16737
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002E75")]
		private float m_MaxTime;

		// Token: 0x04004162 RID: 16738
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002E76")]
		private TownCameraSize.ePathType m_LineType;

		// Token: 0x02000B07 RID: 2823
		[Token(Token = "0x200101D")]
		private enum ePathType
		{
			// Token: 0x04004164 RID: 16740
			[Token(Token = "0x4006659")]
			Linear,
			// Token: 0x04004165 RID: 16741
			[Token(Token = "0x400665A")]
			Bez
		}
	}
}
