﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BAF RID: 2991
	[Token(Token = "0x200081F")]
	[StructLayout(3)]
	public class TownCharaHud : MonoBehaviour
	{
		// Token: 0x06003467 RID: 13415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC8")]
		[Address(RVA = "0x101369BB4", Offset = "0x1369BB4", VA = "0x101369BB4")]
		private void Awake()
		{
		}

		// Token: 0x06003468 RID: 13416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FC9")]
		[Address(RVA = "0x101369E60", Offset = "0x1369E60", VA = "0x101369E60")]
		public void PlayMotion(int findex)
		{
		}

		// Token: 0x06003469 RID: 13417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FCA")]
		[Address(RVA = "0x101369EF0", Offset = "0x1369EF0", VA = "0x101369EF0")]
		public void SetMessageID(int fmessageid, int fcharaid)
		{
		}

		// Token: 0x0600346A RID: 13418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FCB")]
		[Address(RVA = "0x101369BB8", Offset = "0x1369BB8", VA = "0x101369BB8")]
		private void SetUpBaseComponent()
		{
		}

		// Token: 0x0600346B RID: 13419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FCC")]
		[Address(RVA = "0x10136A100", Offset = "0x136A100", VA = "0x10136A100")]
		public void CancelPlay()
		{
		}

		// Token: 0x0600346C RID: 13420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FCD")]
		[Address(RVA = "0x10136A1AC", Offset = "0x136A1AC", VA = "0x10136A1AC")]
		public void Update()
		{
		}

		// Token: 0x0600346D RID: 13421 RVA: 0x000163B0 File Offset: 0x000145B0
		[Token(Token = "0x6002FCE")]
		[Address(RVA = "0x10136A34C", Offset = "0x136A34C", VA = "0x10136A34C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600346E RID: 13422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FCF")]
		[Address(RVA = "0x10136A35C", Offset = "0x136A35C", VA = "0x10136A35C")]
		public void Destroy()
		{
		}

		// Token: 0x0600346F RID: 13423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD0")]
		[Address(RVA = "0x10136A360", Offset = "0x136A360", VA = "0x10136A360")]
		public TownCharaHud()
		{
		}

		// Token: 0x0400448C RID: 17548
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400308C")]
		private int m_VoicePlayID;

		// Token: 0x0400448D RID: 17549
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400308D")]
		private int m_Step;

		// Token: 0x0400448E RID: 17550
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400308E")]
		private float m_TimeUp;

		// Token: 0x0400448F RID: 17551
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400308F")]
		private AnimationState[] m_AnimeTable;

		// Token: 0x04004490 RID: 17552
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003090")]
		private Animation m_PlayAnime;
	}
}
