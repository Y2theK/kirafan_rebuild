﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B92 RID: 2962
	[Token(Token = "0x200080C")]
	[StructLayout(3)]
	public class TownCharaManager
	{
		// Token: 0x06003413 RID: 13331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F7D")]
		[Address(RVA = "0x1013631BC", Offset = "0x13631BC", VA = "0x1013631BC")]
		public TownCharaManager(TownBuilder pbuilder)
		{
		}

		// Token: 0x06003414 RID: 13332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F7E")]
		[Address(RVA = "0x10136322C", Offset = "0x136322C", VA = "0x10136322C")]
		public void AddCharacter(long fid, bool frepos = true, bool fdebug = false)
		{
		}

		// Token: 0x06003415 RID: 13333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F7F")]
		[Address(RVA = "0x101364D44", Offset = "0x1364D44", VA = "0x101364D44")]
		public void Release()
		{
		}

		// Token: 0x06003416 RID: 13334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F80")]
		[Address(RVA = "0x10136A370", Offset = "0x136A370", VA = "0x10136A370")]
		public void DeleteCharacter(long faccessid)
		{
		}

		// Token: 0x06003417 RID: 13335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F81")]
		[Address(RVA = "0x10136A4D4", Offset = "0x136A4D4", VA = "0x10136A4D4")]
		public void CallbackCharaState(eCharaBuildUp ftype, long fmanageid, int froomid)
		{
		}

		// Token: 0x06003418 RID: 13336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F82")]
		[Address(RVA = "0x101367220", Offset = "0x1367220", VA = "0x101367220")]
		public void RefreshBindPos()
		{
		}

		// Token: 0x04004403 RID: 17411
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003027")]
		public TownCharaManager.TownCharaInfo[] m_Table;

		// Token: 0x04004404 RID: 17412
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003028")]
		public TownBuilder m_Builder;

		// Token: 0x02000B93 RID: 2963
		[Token(Token = "0x2001053")]
		public class TownCharaInfo
		{
			// Token: 0x06003419 RID: 13337 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600614F")]
			[Address(RVA = "0x10136A368", Offset = "0x136A368", VA = "0x10136A368")]
			public TownCharaInfo()
			{
			}

			// Token: 0x04004405 RID: 17413
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006748")]
			public TownObjHandleChara m_Handle;

			// Token: 0x04004406 RID: 17414
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006749")]
			public GameObject m_Object;

			// Token: 0x04004407 RID: 17415
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400674A")]
			public long m_MngID;
		}
	}
}
