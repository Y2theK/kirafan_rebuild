﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BB0 RID: 2992
	[Token(Token = "0x2000820")]
	[StructLayout(3)]
	public class TownCharaModel : MonoBehaviour
	{
		// Token: 0x06003470 RID: 13424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD1")]
		[Address(RVA = "0x10136A4FC", Offset = "0x136A4FC", VA = "0x10136A4FC")]
		public void SetCharaModel(ITownObjectHandler pchara, long fmanageid, float fsize, Vector3 fcenter)
		{
		}

		// Token: 0x06003471 RID: 13425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD2")]
		[Address(RVA = "0x10136A5E4", Offset = "0x136A5E4", VA = "0x10136A5E4")]
		public void ChangeFrame(int frameno)
		{
		}

		// Token: 0x06003472 RID: 13426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD3")]
		[Address(RVA = "0x10136A78C", Offset = "0x136A78C", VA = "0x10136A78C")]
		private void SetStarPos()
		{
		}

		// Token: 0x06003473 RID: 13427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD4")]
		[Address(RVA = "0x10136A938", Offset = "0x136A938", VA = "0x10136A938")]
		private void SetUpModel(Sprite psprite, float fsize, Vector3 fcenter)
		{
		}

		// Token: 0x06003474 RID: 13428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD5")]
		[Address(RVA = "0x10136A608", Offset = "0x136A608", VA = "0x10136A608")]
		private void ChangeFrame()
		{
		}

		// Token: 0x06003475 RID: 13429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD6")]
		[Address(RVA = "0x10136AEA4", Offset = "0x136AEA4", VA = "0x10136AEA4")]
		public void SetRenderActive(bool factive)
		{
		}

		// Token: 0x06003476 RID: 13430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FD7")]
		[Address(RVA = "0x10136AF78", Offset = "0x136AF78", VA = "0x10136AF78")]
		public void PlayMotion(int findex)
		{
		}

		// Token: 0x06003477 RID: 13431 RVA: 0x000163C8 File Offset: 0x000145C8
		[Token(Token = "0x6002FD8")]
		[Address(RVA = "0x10136AF7C", Offset = "0x136AF7C", VA = "0x10136AF7C")]
		public bool IsPlaying(int findex)
		{
			return default(bool);
		}

		// Token: 0x06003478 RID: 13432 RVA: 0x000163E0 File Offset: 0x000145E0
		[Token(Token = "0x6002FD9")]
		[Address(RVA = "0x10136AF84", Offset = "0x136AF84", VA = "0x10136AF84")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06003479 RID: 13433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDA")]
		[Address(RVA = "0x10136AFB4", Offset = "0x136AFB4", VA = "0x10136AFB4")]
		private void Update()
		{
		}

		// Token: 0x0600347A RID: 13434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDB")]
		[Address(RVA = "0x10136B148", Offset = "0x136B148", VA = "0x10136B148")]
		public void ChangeLayer(int flayerid)
		{
		}

		// Token: 0x0600347B RID: 13435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDC")]
		[Address(RVA = "0x10136B14C", Offset = "0x136B14C", VA = "0x10136B14C")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600347C RID: 13436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDD")]
		[Address(RVA = "0x10136B29C", Offset = "0x136B29C", VA = "0x10136B29C")]
		public TownCharaModel()
		{
		}

		// Token: 0x04004491 RID: 17553
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003091")]
		private ITownObjectHandler m_BaseHandle;

		// Token: 0x04004492 RID: 17554
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003092")]
		private SpriteHandler m_Handle;

		// Token: 0x04004493 RID: 17555
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003093")]
		private float m_BaseSize;

		// Token: 0x04004494 RID: 17556
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003094")]
		private Vector3 m_Center;

		// Token: 0x04004495 RID: 17557
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003095")]
		private int m_FrameNo;

		// Token: 0x04004496 RID: 17558
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003096")]
		private GameObject[] m_FrameObj;

		// Token: 0x04004497 RID: 17559
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003097")]
		private GameObject m_Star;

		// Token: 0x04004498 RID: 17560
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003098")]
		private float m_StarTime;

		// Token: 0x04004499 RID: 17561
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003099")]
		private bool m_ActiveView;

		// Token: 0x0400449A RID: 17562
		[Cpp2IlInjected.FieldOffset(Offset = "0x55")]
		[Token(Token = "0x400309A")]
		private bool m_SetUp;
	}
}
