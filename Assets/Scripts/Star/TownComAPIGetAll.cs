﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B11 RID: 2833
	[Token(Token = "0x20007BC")]
	[StructLayout(3)]
	public class TownComAPIGetAll : INetComHandle
	{
		// Token: 0x0600320C RID: 12812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC0")]
		[Address(RVA = "0x10136B2A4", Offset = "0x136B2A4", VA = "0x10136B2A4")]
		public TownComAPIGetAll()
		{
		}

		// Token: 0x04004190 RID: 16784
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E87")]
		public long playerId;
	}
}
