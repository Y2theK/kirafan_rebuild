﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B13 RID: 2835
	[Token(Token = "0x20007BE")]
	[StructLayout(3)]
	public class TownComAPINewSet : INetComHandle
	{
		// Token: 0x0600320E RID: 12814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC2")]
		[Address(RVA = "0x10136B340", Offset = "0x136B340", VA = "0x10136B340")]
		public TownComAPINewSet()
		{
		}

		// Token: 0x04004193 RID: 16787
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E8A")]
		public string gridData;
	}
}
