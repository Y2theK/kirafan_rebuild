﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B1D RID: 2845
	[Token(Token = "0x20007C8")]
	[StructLayout(3)]
	public class TownComAPIObjAdd : INetComHandle
	{
		// Token: 0x06003218 RID: 12824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCC")]
		[Address(RVA = "0x10136B3E0", Offset = "0x136B3E0", VA = "0x10136B3E0")]
		public TownComAPIObjAdd()
		{
		}

		// Token: 0x040041B0 RID: 16816
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002EA7")]
		public string facilityId;

		// Token: 0x040041B1 RID: 16817
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002EA8")]
		public string amount;
	}
}
