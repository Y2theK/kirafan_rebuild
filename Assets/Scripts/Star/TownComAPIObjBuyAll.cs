﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000B1E RID: 2846
	[Token(Token = "0x20007C9")]
	[StructLayout(3)]
	public class TownComAPIObjBuyAll : INetComHandle
	{
		// Token: 0x06003219 RID: 12825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCD")]
		[Address(RVA = "0x10136B480", Offset = "0x136B480", VA = "0x10136B480")]
		public TownComAPIObjBuyAll()
		{
		}

		// Token: 0x040041B2 RID: 16818
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002EA9")]
		public TownFacilityBuyState[] townFacilityBuyStates;
	}
}
