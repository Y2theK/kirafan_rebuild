﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B1B RID: 2843
	[Token(Token = "0x20007C6")]
	[StructLayout(3)]
	public class TownComAPIObjGemLeveUp : INetComHandle
	{
		// Token: 0x06003216 RID: 12822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCA")]
		[Address(RVA = "0x10136B520", Offset = "0x136B520", VA = "0x10136B520")]
		public TownComAPIObjGemLeveUp()
		{
		}

		// Token: 0x040041A7 RID: 16807
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E9E")]
		public long managedTownFacilityId;

		// Token: 0x040041A8 RID: 16808
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E9F")]
		public int nextLevel;

		// Token: 0x040041A9 RID: 16809
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002EA0")]
		public int openState;

		// Token: 0x040041AA RID: 16810
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002EA1")]
		public long actionTime;

		// Token: 0x040041AB RID: 16811
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002EA2")]
		public long remainingTime;
	}
}
