﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B1C RID: 2844
	[Token(Token = "0x20007C7")]
	[StructLayout(3)]
	public class TownComAPIObjItemUp : INetComHandle
	{
		// Token: 0x06003217 RID: 12823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCB")]
		[Address(RVA = "0x10136B65C", Offset = "0x136B65C", VA = "0x10136B65C")]
		public TownComAPIObjItemUp()
		{
		}

		// Token: 0x040041AC RID: 16812
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002EA3")]
		public long managedTownFacilityId;

		// Token: 0x040041AD RID: 16813
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002EA4")]
		public int itemNo;

		// Token: 0x040041AE RID: 16814
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002EA5")]
		public int amount;

		// Token: 0x040041AF RID: 16815
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002EA6")]
		public long actionTime;
	}
}
