﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B1A RID: 2842
	[Token(Token = "0x20007C5")]
	[StructLayout(3)]
	public class TownComAPIObjLeveUp : INetComHandle
	{
		// Token: 0x06003215 RID: 12821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC9")]
		[Address(RVA = "0x10136B6FC", Offset = "0x136B6FC", VA = "0x10136B6FC")]
		public TownComAPIObjLeveUp()
		{
		}

		// Token: 0x040041A3 RID: 16803
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E9A")]
		public long managedTownFacilityId;

		// Token: 0x040041A4 RID: 16804
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E9B")]
		public int nextLevel;

		// Token: 0x040041A5 RID: 16805
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002E9C")]
		public int openState;

		// Token: 0x040041A6 RID: 16806
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E9D")]
		public long actionTime;
	}
}
