﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B19 RID: 2841
	[Token(Token = "0x20007C4")]
	[StructLayout(3)]
	public class TownComAPIObjOpenUp : INetComHandle
	{
		// Token: 0x06003214 RID: 12820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC8")]
		[Address(RVA = "0x10136B79C", Offset = "0x136B79C", VA = "0x10136B79C")]
		public TownComAPIObjOpenUp()
		{
		}

		// Token: 0x0400419F RID: 16799
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E96")]
		public long managedTownFacilityId;

		// Token: 0x040041A0 RID: 16800
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E97")]
		public int nextLevel;

		// Token: 0x040041A1 RID: 16801
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002E98")]
		public int openState;

		// Token: 0x040041A2 RID: 16802
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E99")]
		public long buildTime;
	}
}
