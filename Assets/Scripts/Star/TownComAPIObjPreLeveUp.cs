﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B18 RID: 2840
	[Token(Token = "0x20007C3")]
	[StructLayout(3)]
	public class TownComAPIObjPreLeveUp : INetComHandle
	{
		// Token: 0x06003213 RID: 12819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC7")]
		[Address(RVA = "0x10136B83C", Offset = "0x136B83C", VA = "0x10136B83C")]
		public TownComAPIObjPreLeveUp()
		{
		}

		// Token: 0x0400419B RID: 16795
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E92")]
		public long managedTownFacilityId;

		// Token: 0x0400419C RID: 16796
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E93")]
		public int nextLevel;

		// Token: 0x0400419D RID: 16797
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002E94")]
		public int openState;

		// Token: 0x0400419E RID: 16798
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E95")]
		public long buildTime;
	}
}
