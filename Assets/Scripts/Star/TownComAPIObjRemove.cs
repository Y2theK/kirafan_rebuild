﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B1F RID: 2847
	[Token(Token = "0x20007CA")]
	[StructLayout(3)]
	public class TownComAPIObjRemove : INetComHandle
	{
		// Token: 0x0600321A RID: 12826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCE")]
		[Address(RVA = "0x10136B8DC", Offset = "0x136B8DC", VA = "0x10136B8DC")]
		public TownComAPIObjRemove()
		{
		}

		// Token: 0x040041B3 RID: 16819
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002EAA")]
		public long managedTownFacilityId;
	}
}
