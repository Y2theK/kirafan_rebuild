﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B16 RID: 2838
	[Token(Token = "0x20007C1")]
	[StructLayout(3)]
	public class TownComAPIObjbuildPointSet : INetComHandle
	{
		// Token: 0x06003211 RID: 12817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC5")]
		[Address(RVA = "0x10136B97C", Offset = "0x136B97C", VA = "0x10136B97C")]
		public TownComAPIObjbuildPointSet()
		{
		}

		// Token: 0x04004195 RID: 16789
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E8C")]
		public long managedTownFacilityId;

		// Token: 0x04004196 RID: 16790
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E8D")]
		public int buildPointIndex;

		// Token: 0x04004197 RID: 16791
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4002E8E")]
		public int openState;

		// Token: 0x04004198 RID: 16792
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002E8F")]
		public long buildTime;

		// Token: 0x04004199 RID: 16793
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002E90")]
		public int actionNo;
	}
}
