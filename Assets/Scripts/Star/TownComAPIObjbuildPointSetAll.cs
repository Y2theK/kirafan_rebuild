﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using WWWTypes;

namespace Star
{
	// Token: 0x02000B17 RID: 2839
	[Token(Token = "0x20007C2")]
	[StructLayout(3)]
	public class TownComAPIObjbuildPointSetAll : INetComHandle
	{
		// Token: 0x06003212 RID: 12818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC6")]
		[Address(RVA = "0x10136BA1C", Offset = "0x136BA1C", VA = "0x10136BA1C")]
		public TownComAPIObjbuildPointSetAll()
		{
		}

		// Token: 0x0400419A RID: 16794
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E91")]
		public TownFacilitySetState[] townFacilitySetStates;
	}
}
