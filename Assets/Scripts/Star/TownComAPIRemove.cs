﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B14 RID: 2836
	[Token(Token = "0x20007BF")]
	[StructLayout(3)]
	public class TownComAPIRemove : INetComHandle
	{
		// Token: 0x0600320F RID: 12815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC3")]
		[Address(RVA = "0x10136BABC", Offset = "0x136BABC", VA = "0x10136BABC")]
		public TownComAPIRemove()
		{
		}

		// Token: 0x04004194 RID: 16788
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E8B")]
		public long managedTownId;
	}
}
