﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B12 RID: 2834
	[Token(Token = "0x20007BD")]
	[StructLayout(3)]
	public class TownComAPISet : INetComHandle
	{
		// Token: 0x0600320D RID: 12813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DC1")]
		[Address(RVA = "0x10136BB5C", Offset = "0x136BB5C", VA = "0x10136BB5C")]
		public TownComAPISet()
		{
		}

		// Token: 0x04004191 RID: 16785
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002E88")]
		public long managedTownId;

		// Token: 0x04004192 RID: 16786
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002E89")]
		public string gridData;
	}
}
