﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200086F RID: 2159
	[Token(Token = "0x2000647")]
	[StructLayout(3)]
	public class TownComAddBuildComEnd : IMainComCommand
	{
		// Token: 0x060022A2 RID: 8866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001FFE")]
		[Address(RVA = "0x10136BBFC", Offset = "0x136BBFC", VA = "0x10136BBFC")]
		public TownComAddBuildComEnd(List<FldComBuildScript.BuildUpParam> list)
		{
		}

		// Token: 0x060022A3 RID: 8867 RVA: 0x0000F000 File Offset: 0x0000D200
		[Token(Token = "0x6001FFF")]
		[Address(RVA = "0x10136BC28", Offset = "0x136BC28", VA = "0x10136BC28", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x040032E3 RID: 13027
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025D4")]
		public List<FldComBuildScript.BuildUpParam> m_BuildUpParamList;
	}
}
