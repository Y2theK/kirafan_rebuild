﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B0A RID: 2826
	[Token(Token = "0x20007B9")]
	[StructLayout(3)]
	public class TownComAllDelete : IFldNetComModule
	{
		// Token: 0x060031EF RID: 12783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DA8")]
		[Address(RVA = "0x10136BC30", Offset = "0x136BC30", VA = "0x10136BC30")]
		public TownComAllDelete()
		{
		}

		// Token: 0x060031F0 RID: 12784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DA9")]
		[Address(RVA = "0x10136BC8C", Offset = "0x136BC8C", VA = "0x10136BC8C")]
		public void PlaySend()
		{
		}

		// Token: 0x060031F1 RID: 12785 RVA: 0x00015528 File Offset: 0x00013728
		[Token(Token = "0x6002DAA")]
		[Address(RVA = "0x10136BCC4", Offset = "0x136BCC4", VA = "0x10136BCC4")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060031F2 RID: 12786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DAB")]
		[Address(RVA = "0x10136BDE8", Offset = "0x136BDE8", VA = "0x10136BDE8")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060031F3 RID: 12787 RVA: 0x00015540 File Offset: 0x00013740
		[Token(Token = "0x6002DAC")]
		[Address(RVA = "0x10136BF6C", Offset = "0x136BF6C", VA = "0x10136BF6C")]
		public bool SetUpObjList()
		{
			return default(bool);
		}

		// Token: 0x060031F4 RID: 12788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DAD")]
		[Address(RVA = "0x10136C024", Offset = "0x136C024", VA = "0x10136C024")]
		private void CallbackObjSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060031F5 RID: 12789 RVA: 0x00015558 File Offset: 0x00013758
		[Token(Token = "0x6002DAE")]
		[Address(RVA = "0x10136C1A8", Offset = "0x136C1A8", VA = "0x10136C1A8", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060031F6 RID: 12790 RVA: 0x00015570 File Offset: 0x00013770
		[Token(Token = "0x6002DAF")]
		[Address(RVA = "0x10136C2EC", Offset = "0x136C2EC", VA = "0x10136C2EC")]
		public bool DefaultDeleteManage()
		{
			return default(bool);
		}

		// Token: 0x060031F7 RID: 12791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DB0")]
		[Address(RVA = "0x10136C55C", Offset = "0x136C55C", VA = "0x10136C55C")]
		private void CallbackDeleteUp(INetComHandle phandle)
		{
		}

		// Token: 0x060031F8 RID: 12792 RVA: 0x00015588 File Offset: 0x00013788
		[Token(Token = "0x6002DB1")]
		[Address(RVA = "0x10136C424", Offset = "0x136C424", VA = "0x10136C424")]
		public bool DefaultDeleteObjManage()
		{
			return default(bool);
		}

		// Token: 0x060031F9 RID: 12793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DB2")]
		[Address(RVA = "0x10136C568", Offset = "0x136C568", VA = "0x10136C568")]
		private void CallbackDeleteObjUp(INetComHandle phandle)
		{
		}

		// Token: 0x0400416F RID: 16751
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E80")]
		private TownComAllDelete.eStep m_Step;

		// Token: 0x04004170 RID: 16752
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E81")]
		private long[] m_Table;

		// Token: 0x04004171 RID: 16753
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002E82")]
		private int m_DelStep;

		// Token: 0x02000B0B RID: 2827
		[Token(Token = "0x200101E")]
		public enum eStep
		{
			// Token: 0x04004173 RID: 16755
			[Token(Token = "0x400665C")]
			GetState,
			// Token: 0x04004174 RID: 16756
			[Token(Token = "0x400665D")]
			WaitCheck,
			// Token: 0x04004175 RID: 16757
			[Token(Token = "0x400665E")]
			DeleteUp,
			// Token: 0x04004176 RID: 16758
			[Token(Token = "0x400665F")]
			SetUpCheck,
			// Token: 0x04004177 RID: 16759
			[Token(Token = "0x4006660")]
			GetObjState,
			// Token: 0x04004178 RID: 16760
			[Token(Token = "0x4006661")]
			DeleteObjUp,
			// Token: 0x04004179 RID: 16761
			[Token(Token = "0x4006662")]
			End
		}
	}
}
