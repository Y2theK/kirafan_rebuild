﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000871 RID: 2161
	[Token(Token = "0x2000649")]
	[StructLayout(3)]
	public class TownComBuild : IMainComCommand
	{
		// Token: 0x060022A4 RID: 8868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002000")]
		[Address(RVA = "0x1013668EC", Offset = "0x13668EC", VA = "0x1013668EC")]
		public TownComBuild(eTownCommand fcommand)
		{
		}

		// Token: 0x060022A5 RID: 8869 RVA: 0x0000F018 File Offset: 0x0000D218
		[Token(Token = "0x6002001")]
		[Address(RVA = "0x10136C574", Offset = "0x136C574", VA = "0x10136C574", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x040032E7 RID: 13031
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025D8")]
		public int m_BuildPoint;

		// Token: 0x040032E8 RID: 13032
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40025D9")]
		public int m_ObjID;

		// Token: 0x040032E9 RID: 13033
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025DA")]
		public eTownCommand m_BuildType;
	}
}
