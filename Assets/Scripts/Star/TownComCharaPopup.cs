﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000872 RID: 2162
	[Token(Token = "0x200064A")]
	[StructLayout(3)]
	public class TownComCharaPopup : IMainComCommand
	{
		// Token: 0x060022A6 RID: 8870 RVA: 0x0000F030 File Offset: 0x0000D230
		[Token(Token = "0x6002002")]
		[Address(RVA = "0x101389504", Offset = "0x1389504", VA = "0x101389504", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x060022A7 RID: 8871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002003")]
		[Address(RVA = "0x10138950C", Offset = "0x138950C", VA = "0x10138950C")]
		public TownComCharaPopup()
		{
		}

		// Token: 0x040032EA RID: 13034
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025DB")]
		public int m_CharaID;

		// Token: 0x040032EB RID: 13035
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025DC")]
		public long m_ManageID;

		// Token: 0x040032EC RID: 13036
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025DD")]
		public int m_MessageID;

		// Token: 0x040032ED RID: 13037
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40025DE")]
		public Vector3 m_Position;

		// Token: 0x040032EE RID: 13038
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40025DF")]
		public int m_TownObjectID;

		// Token: 0x040032EF RID: 13039
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40025E0")]
		public int m_SpecialNo;

		// Token: 0x040032F0 RID: 13040
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40025E1")]
		public List<TownPartsGetItem> m_PartsGetItemList;
	}
}
