﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000873 RID: 2163
	[Token(Token = "0x200064B")]
	[StructLayout(3)]
	public class TownComComplete : IMainComCommand
	{
		// Token: 0x060022A8 RID: 8872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002004")]
		[Address(RVA = "0x101389514", Offset = "0x1389514", VA = "0x101389514")]
		public TownComComplete(TownUtility.eResCategory fcategory)
		{
		}

		// Token: 0x060022A9 RID: 8873 RVA: 0x0000F048 File Offset: 0x0000D248
		[Token(Token = "0x6002005")]
		[Address(RVA = "0x101389540", Offset = "0x1389540", VA = "0x101389540", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x040032F1 RID: 13041
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025E2")]
		public long m_ManageID;

		// Token: 0x040032F2 RID: 13042
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025E3")]
		public int m_BuildPoint;

		// Token: 0x040032F3 RID: 13043
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40025E4")]
		public int m_ObjID;

		// Token: 0x040032F4 RID: 13044
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025E5")]
		public TownUtility.eResCategory m_Type;
	}
}
