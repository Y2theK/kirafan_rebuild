﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000874 RID: 2164
	[Token(Token = "0x200064C")]
	[StructLayout(3)]
	public class TownComManager
	{
		// Token: 0x060022AA RID: 8874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002006")]
		[Address(RVA = "0x101389548", Offset = "0x1389548", VA = "0x101389548")]
		public TownComManager(int ftablenum)
		{
		}

		// Token: 0x060022AB RID: 8875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002007")]
		[Address(RVA = "0x1013895BC", Offset = "0x13895BC", VA = "0x1013895BC")]
		public void PushComCommand(IMainComCommand pcmd)
		{
		}

		// Token: 0x060022AC RID: 8876 RVA: 0x0000F060 File Offset: 0x0000D260
		[Token(Token = "0x6002008")]
		[Address(RVA = "0x10138965C", Offset = "0x138965C", VA = "0x10138965C")]
		public int GetComCommandNum()
		{
			return 0;
		}

		// Token: 0x060022AD RID: 8877 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002009")]
		[Address(RVA = "0x101389664", Offset = "0x1389664", VA = "0x101389664")]
		public IMainComCommand GetComCommand(int findex)
		{
			return null;
		}

		// Token: 0x060022AE RID: 8878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600200A")]
		[Address(RVA = "0x1013896B4", Offset = "0x13896B4", VA = "0x1013896B4")]
		public void Flush()
		{
		}

		// Token: 0x040032F5 RID: 13045
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025E6")]
		public int m_Num;

		// Token: 0x040032F6 RID: 13046
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40025E7")]
		public int m_Max;

		// Token: 0x040032F7 RID: 13047
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025E8")]
		public IMainComCommand[] m_Table;
	}
}
