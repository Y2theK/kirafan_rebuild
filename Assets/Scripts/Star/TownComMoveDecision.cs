﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000875 RID: 2165
	[Token(Token = "0x200064D")]
	[StructLayout(3)]
	public class TownComMoveDecision : IMainComCommand
	{
		// Token: 0x060022AF RID: 8879 RVA: 0x0000F078 File Offset: 0x0000D278
		[Token(Token = "0x600200B")]
		[Address(RVA = "0x101389730", Offset = "0x1389730", VA = "0x101389730", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x060022B0 RID: 8880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600200C")]
		[Address(RVA = "0x101389738", Offset = "0x1389738", VA = "0x101389738")]
		public TownComMoveDecision()
		{
		}

		// Token: 0x040032F8 RID: 13048
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025E9")]
		public int m_BaseBuildPoint;

		// Token: 0x040032F9 RID: 13049
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025EA")]
		public long m_BaseMngID;

		// Token: 0x040032FA RID: 13050
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025EB")]
		public int m_TargetBuildPoint;

		// Token: 0x040032FB RID: 13051
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40025EC")]
		public long m_ChangeMngID;
	}
}
