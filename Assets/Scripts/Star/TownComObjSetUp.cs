﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000B0C RID: 2828
	[Token(Token = "0x20007BA")]
	[StructLayout(3)]
	public class TownComObjSetUp : IFldNetComModule
	{
		// Token: 0x060031FA RID: 12794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DB3")]
		[Address(RVA = "0x101389740", Offset = "0x1389740", VA = "0x101389740")]
		public TownComObjSetUp()
		{
		}

		// Token: 0x060031FB RID: 12795 RVA: 0x000155A0 File Offset: 0x000137A0
		[Token(Token = "0x6002DB4")]
		[Address(RVA = "0x1013897A4", Offset = "0x13897A4", VA = "0x1013897A4")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x060031FC RID: 12796 RVA: 0x000155B8 File Offset: 0x000137B8
		[Token(Token = "0x6002DB5")]
		[Address(RVA = "0x101389860", Offset = "0x1389860", VA = "0x101389860")]
		public bool DefaultObjAdd()
		{
			return default(bool);
		}

		// Token: 0x060031FD RID: 12797 RVA: 0x000155D0 File Offset: 0x000137D0
		[Token(Token = "0x6002DB6")]
		[Address(RVA = "0x101389A00", Offset = "0x1389A00", VA = "0x101389A00", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x060031FE RID: 12798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DB7")]
		[Address(RVA = "0x101389B00", Offset = "0x1389B00", VA = "0x101389B00")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x060031FF RID: 12799 RVA: 0x000155E8 File Offset: 0x000137E8
		[Token(Token = "0x6002DB8")]
		[Address(RVA = "0x101389BDC", Offset = "0x1389BDC", VA = "0x101389BDC")]
		private bool CheckDefaultSetUpData(GetAll pres)
		{
			return default(bool);
		}

		// Token: 0x0400417A RID: 16762
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E83")]
		private TownComObjSetUp.eStep m_Step;

		// Token: 0x0400417B RID: 16763
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E84")]
		private List<TownComObjSetUp.DefaultSetUpQue> m_SendQue;

		// Token: 0x02000B0D RID: 2829
		[Token(Token = "0x200101F")]
		public enum eStep
		{
			// Token: 0x0400417D RID: 16765
			[Token(Token = "0x4006664")]
			GetState,
			// Token: 0x0400417E RID: 16766
			[Token(Token = "0x4006665")]
			WaitCheck,
			// Token: 0x0400417F RID: 16767
			[Token(Token = "0x4006666")]
			DefaultObjUp,
			// Token: 0x04004180 RID: 16768
			[Token(Token = "0x4006667")]
			SetUpCheck,
			// Token: 0x04004181 RID: 16769
			[Token(Token = "0x4006668")]
			End
		}

		// Token: 0x02000B0E RID: 2830
		[Token(Token = "0x2001020")]
		public class DefaultSetUpQue
		{
			// Token: 0x06003200 RID: 12800 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006100")]
			[Address(RVA = "0x101389EF8", Offset = "0x1389EF8", VA = "0x101389EF8")]
			public DefaultSetUpQue(int fobjid, bool ftype)
			{
			}

			// Token: 0x06003201 RID: 12801 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006101")]
			[Address(RVA = "0x101389904", Offset = "0x1389904", VA = "0x101389904")]
			public void SendUp(IFldNetComManager pmanager)
			{
			}

			// Token: 0x06003202 RID: 12802 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006102")]
			[Address(RVA = "0x101389F34", Offset = "0x1389F34", VA = "0x101389F34")]
			public void CallbackDefaultAdd(INetComHandle phandle)
			{
			}

			// Token: 0x06003203 RID: 12803 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006103")]
			[Address(RVA = "0x10138A1E0", Offset = "0x138A1E0", VA = "0x10138A1E0")]
			public void CallbackDefaultParamUp(INetComHandle phandle)
			{
			}

			// Token: 0x06003204 RID: 12804 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006104")]
			[Address(RVA = "0x10138A2EC", Offset = "0x138A2EC", VA = "0x10138A2EC")]
			public void CallbackDefaultLevelUp(INetComHandle phandle)
			{
			}

			// Token: 0x04004182 RID: 16770
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006669")]
			public long m_ManageID;

			// Token: 0x04004183 RID: 16771
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400666A")]
			public int m_ObjID;

			// Token: 0x04004184 RID: 16772
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x400666B")]
			public bool m_ListUp;

			// Token: 0x04004185 RID: 16773
			[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
			[Token(Token = "0x400666C")]
			public bool m_Type;

			// Token: 0x04004186 RID: 16774
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400666D")]
			private IFldNetComManager m_Manager;
		}
	}
}
