﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B0F RID: 2831
	[Token(Token = "0x20007BB")]
	[StructLayout(3)]
	public class TownComSetUp : IFldNetComModule
	{
		// Token: 0x06003205 RID: 12805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DB9")]
		[Address(RVA = "0x10138A2F8", Offset = "0x138A2F8", VA = "0x10138A2F8")]
		public TownComSetUp()
		{
		}

		// Token: 0x06003206 RID: 12806 RVA: 0x00015600 File Offset: 0x00013800
		[Token(Token = "0x6002DBA")]
		[Address(RVA = "0x10138A380", Offset = "0x138A380", VA = "0x10138A380")]
		public bool SetUp()
		{
			return default(bool);
		}

		// Token: 0x06003207 RID: 12807 RVA: 0x00015618 File Offset: 0x00013818
		[Token(Token = "0x6002DBB")]
		[Address(RVA = "0x10138A4A8", Offset = "0x138A4A8", VA = "0x10138A4A8")]
		public bool DefaultBuildUp()
		{
			return default(bool);
		}

		// Token: 0x06003208 RID: 12808 RVA: 0x00015630 File Offset: 0x00013830
		[Token(Token = "0x6002DBC")]
		[Address(RVA = "0x10138A63C", Offset = "0x138A63C", VA = "0x10138A63C", Slot = "4")]
		public override bool UpFunc()
		{
			return default(bool);
		}

		// Token: 0x06003209 RID: 12809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DBD")]
		[Address(RVA = "0x10138A754", Offset = "0x138A754", VA = "0x10138A754")]
		private void CallbackSetUp(INetComHandle phandle)
		{
		}

		// Token: 0x0600320A RID: 12810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DBE")]
		[Address(RVA = "0x10138AC60", Offset = "0x138AC60", VA = "0x10138AC60")]
		private void CallbackDefaultBuildUp(INetComHandle phandle)
		{
		}

		// Token: 0x0600320B RID: 12811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DBF")]
		[Address(RVA = "0x10138A850", Offset = "0x138A850", VA = "0x10138A850")]
		private void CheckTownBuildListKey()
		{
		}

		// Token: 0x04004187 RID: 16775
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E85")]
		private TownComSetUp.eStep m_Step;

		// Token: 0x04004188 RID: 16776
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E86")]
		private List<TownComSetUp.eStep> m_DefaultStep;

		// Token: 0x02000B10 RID: 2832
		[Token(Token = "0x2001021")]
		public enum eStep
		{
			// Token: 0x0400418A RID: 16778
			[Token(Token = "0x400666F")]
			GetState,
			// Token: 0x0400418B RID: 16779
			[Token(Token = "0x4006670")]
			WaitCheck,
			// Token: 0x0400418C RID: 16780
			[Token(Token = "0x4006671")]
			DefaultBuildUp,
			// Token: 0x0400418D RID: 16781
			[Token(Token = "0x4006672")]
			SetUpCheck,
			// Token: 0x0400418E RID: 16782
			[Token(Token = "0x4006673")]
			DefStepChk,
			// Token: 0x0400418F RID: 16783
			[Token(Token = "0x4006674")]
			End
		}
	}
}
