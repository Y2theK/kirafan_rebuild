﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000877 RID: 2167
	[Token(Token = "0x200064F")]
	[StructLayout(3)]
	public class TownComUI : IMainComCommand
	{
		// Token: 0x060022B1 RID: 8881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600200D")]
		[Address(RVA = "0x10138AD48", Offset = "0x138AD48", VA = "0x10138AD48")]
		public TownComUI(bool fopen)
		{
		}

		// Token: 0x060022B2 RID: 8882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600200E")]
		[Address(RVA = "0x10138AD74", Offset = "0x138AD74", VA = "0x10138AD74")]
		public void GetBuildInfomation(out int pbuildpoint, out long pbuildmngid, out int pbuildobjid)
		{
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x0000F090 File Offset: 0x0000D290
		[Token(Token = "0x600200F")]
		[Address(RVA = "0x10138ADE8", Offset = "0x138ADE8", VA = "0x10138ADE8", Slot = "2")]
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x0400330A RID: 13066
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40025FB")]
		public bool m_IsOpen;

		// Token: 0x0400330B RID: 13067
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40025FC")]
		public eTownUICategory m_OpenUI;

		// Token: 0x0400330C RID: 13068
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40025FD")]
		public int m_BuildPoint;

		// Token: 0x0400330D RID: 13069
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40025FE")]
		public ITownObjectHandler m_SelectHandle;
	}
}
