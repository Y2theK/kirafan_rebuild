﻿namespace Star {
    public static class TownDefine {
        public const int BUILD_LEVEL_MODEL_CUT = 1;
        public const int AREA_CTXID = 0;
        public const int MENU_BUFID = 0x10000000;
        public const int AREA_CONTENTID = 0x20000000;
        public const int AREA_BUFID = 0x40000000;
        public const int TYPE_MASKID = 0x70000000;
        public const float CHARA_ICON_SIZE = 0.7f;
        public const int CHARA_ICON_MAX_PER_LINE = 5;
        public const int LAYER_OFFSET = 10;
        public static long ROOM_ACCESS_MNG_ID = 1L;
        public const int AREA_OBJID = 1;

        public static void CalcRoomManageID() {
            UserTownData townData = GameSystem.Inst.UserDataMng.UserTownData;
            int dataNum = townData.GetBuildObjectDataNum();
            for (int i = 0; i < dataNum; i++) {
                UserTownData.BuildObjectData data = townData.GetBuildObjectDataAt(i);
                if (data.m_BuildPointIndex == 0) {
                    ROOM_ACCESS_MNG_ID = data.m_ManageID;
                    break;
                }
            }
        }

        public enum eTownLevelUpConditionCategory {
            Gold,
            Kirara,
            UserLv,
            BuildTime
        }
    }
}
