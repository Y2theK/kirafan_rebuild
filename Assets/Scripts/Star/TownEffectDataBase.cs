﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B22 RID: 2850
	[Token(Token = "0x20007CC")]
	[StructLayout(3)]
	public class TownEffectDataBase : MonoBehaviour
	{
		// Token: 0x06003221 RID: 12833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD5")]
		[Address(RVA = "0x10138AF88", Offset = "0x138AF88", VA = "0x10138AF88")]
		public TownEffectDataBase()
		{
		}

		// Token: 0x040041BB RID: 16827
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EAD")]
		[SerializeField]
		public TownEffectDataBase.EffectDataSet[] m_List;

		// Token: 0x02000B23 RID: 2851
		[Token(Token = "0x2001023")]
		[Serializable]
		public struct EffectDataSet
		{
			// Token: 0x040041BC RID: 16828
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x400667A")]
			public string m_FileName;

			// Token: 0x040041BD RID: 16829
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x400667B")]
			public uint m_AccessKey;

			// Token: 0x040041BE RID: 16830
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x400667C")]
			public float m_Size;
		}
	}
}
