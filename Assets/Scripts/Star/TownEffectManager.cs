﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B24 RID: 2852
	[Token(Token = "0x20007CD")]
	[StructLayout(3)]
	public class TownEffectManager : MonoBehaviour
	{
		// Token: 0x06003222 RID: 12834 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002DD6")]
		[Address(RVA = "0x10138AF90", Offset = "0x138AF90", VA = "0x10138AF90")]
		public static TownEffectManager CreateEffectManager(GameObject plink)
		{
			return null;
		}

		// Token: 0x06003223 RID: 12835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD7")]
		[Address(RVA = "0x10138AFEC", Offset = "0x138AFEC", VA = "0x10138AFEC")]
		public static void ReleaseEffectManager(TownEffectManager pmng)
		{
		}

		// Token: 0x06003224 RID: 12836 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002DD8")]
		[Address(RVA = "0x10138B03C", Offset = "0x138B03C", VA = "0x10138B03C")]
		public static TownEffectPlayer CreateEffect(int feffectno)
		{
			return null;
		}

		// Token: 0x06003225 RID: 12837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD9")]
		[Address(RVA = "0x10138B508", Offset = "0x138B508", VA = "0x10138B508")]
		private void Awake()
		{
		}

		// Token: 0x06003226 RID: 12838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DDA")]
		[Address(RVA = "0x10138B5E0", Offset = "0x138B5E0", VA = "0x10138B5E0")]
		public void SetEffectList(string pfilename)
		{
		}

		// Token: 0x06003227 RID: 12839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DDB")]
		[Address(RVA = "0x10138B674", Offset = "0x138B674", VA = "0x10138B674")]
		private void CallbackHandle(MeigeResource.Handler phandle)
		{
		}

		// Token: 0x06003228 RID: 12840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DDC")]
		[Address(RVA = "0x10138B93C", Offset = "0x138B93C", VA = "0x10138B93C")]
		private void Update()
		{
		}

		// Token: 0x06003229 RID: 12841 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002DDD")]
		[Address(RVA = "0x10138B0A4", Offset = "0x138B0A4", VA = "0x10138B0A4")]
		private TownEffectPlayer CreateEffectNode(int feffectno)
		{
			return null;
		}

		// Token: 0x0600322A RID: 12842 RVA: 0x00015660 File Offset: 0x00013860
		[Token(Token = "0x6002DDE")]
		[Address(RVA = "0x10138CA4C", Offset = "0x138CA4C", VA = "0x10138CA4C")]
		private int CreateEffectResource(int feffno)
		{
			return 0;
		}

		// Token: 0x0600322B RID: 12843 RVA: 0x00015678 File Offset: 0x00013878
		[Token(Token = "0x6002DDF")]
		[Address(RVA = "0x10138CDF8", Offset = "0x138CDF8", VA = "0x10138CDF8")]
		private int EntryResource(int feffno)
		{
			return 0;
		}

		// Token: 0x0600322C RID: 12844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE0")]
		[Address(RVA = "0x10138C81C", Offset = "0x138C81C", VA = "0x10138C81C")]
		private void ReleaseEffRes(int fresno)
		{
		}

		// Token: 0x0600322D RID: 12845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE1")]
		[Address(RVA = "0x10138C2E4", Offset = "0x138C2E4", VA = "0x10138C2E4")]
		private void StartResLoad(int fresno)
		{
		}

		// Token: 0x0600322E RID: 12846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE2")]
		[Address(RVA = "0x10138CBA4", Offset = "0x138CBA4", VA = "0x10138CBA4")]
		private void CheckResLoad(int fresno)
		{
		}

		// Token: 0x0600322F RID: 12847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE3")]
		[Address(RVA = "0x10138D0A0", Offset = "0x138D0A0", VA = "0x10138D0A0")]
		private void CallbackResObj(MeigeResource.Handler phandle)
		{
		}

		// Token: 0x06003230 RID: 12848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE4")]
		[Address(RVA = "0x10138D0AC", Offset = "0x138D0AC", VA = "0x10138D0AC")]
		public TownEffectManager()
		{
		}

		// Token: 0x040041BF RID: 16831
		[Token(Token = "0x4002EAE")]
		private static TownEffectManager ms_Inst;

		// Token: 0x040041C0 RID: 16832
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EAF")]
		private TownEffectManager.EffectResource[] m_ResTable;

		// Token: 0x040041C1 RID: 16833
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EB0")]
		private int m_ResMax;

		// Token: 0x040041C2 RID: 16834
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002EB1")]
		private TownEffectPlayer[] m_Player;

		// Token: 0x040041C3 RID: 16835
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002EB2")]
		private int m_PlayerNum;

		// Token: 0x040041C4 RID: 16836
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002EB3")]
		private int m_PlayerMax;

		// Token: 0x040041C5 RID: 16837
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002EB4")]
		private int[] m_WaitResTable;

		// Token: 0x040041C6 RID: 16838
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002EB5")]
		private int m_WaitResNum;

		// Token: 0x040041C7 RID: 16839
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4002EB6")]
		private int m_WaitResMax;

		// Token: 0x040041C8 RID: 16840
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002EB7")]
		private byte m_ResUp;

		// Token: 0x040041C9 RID: 16841
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002EB8")]
		private TownEffectPlayer[] m_WaitPlayerTable;

		// Token: 0x040041CA RID: 16842
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002EB9")]
		private int m_WaitPlayerNum;

		// Token: 0x040041CB RID: 16843
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002EBA")]
		private int m_WaitPlayerMax;

		// Token: 0x040041CC RID: 16844
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002EBB")]
		private TownEffectManager.EffectDataSet[] m_EffList;

		// Token: 0x040041CD RID: 16845
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002EBC")]
		private MeigeResource.Handler m_BaseHandle;

		// Token: 0x02000B25 RID: 2853
		[Token(Token = "0x2001024")]
		public class EffectResource
		{
			// Token: 0x06003231 RID: 12849 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006105")]
			[Address(RVA = "0x10138D098", Offset = "0x138D098", VA = "0x10138D098")]
			public EffectResource()
			{
			}

			// Token: 0x040041CE RID: 16846
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400667D")]
			public int m_EffectNo;

			// Token: 0x040041CF RID: 16847
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400667E")]
			public UnityEngine.Object m_Object;

			// Token: 0x040041D0 RID: 16848
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400667F")]
			public int m_UseNum;

			// Token: 0x040041D1 RID: 16849
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4006680")]
			public bool m_Active;

			// Token: 0x040041D2 RID: 16850
			[Cpp2IlInjected.FieldOffset(Offset = "0x25")]
			[Token(Token = "0x4006681")]
			public bool m_ReadWait;

			// Token: 0x040041D3 RID: 16851
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006682")]
			public MeigeResource.Handler m_Handle;
		}

		// Token: 0x02000B26 RID: 2854
		[Token(Token = "0x2001025")]
		public struct EffectDataSet
		{
			// Token: 0x040041D4 RID: 16852
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006683")]
			public string m_FileName;

			// Token: 0x040041D5 RID: 16853
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006684")]
			public float m_Size;
		}
	}
}
