﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B27 RID: 2855
	[Token(Token = "0x20007CE")]
	[StructLayout(3)]
	public class TownEffectPlayer : MonoBehaviour
	{
		// Token: 0x06003232 RID: 12850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE5")]
		[Address(RVA = "0x10138D0B4", Offset = "0x138D0B4", VA = "0x10138D0B4")]
		private void Awake()
		{
		}

		// Token: 0x06003233 RID: 12851 RVA: 0x00015690 File Offset: 0x00013890
		[Token(Token = "0x6002DE6")]
		[Address(RVA = "0x10138C814", Offset = "0x138C814", VA = "0x10138C814")]
		public bool IsActive()
		{
			return default(bool);
		}

		// Token: 0x06003234 RID: 12852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE7")]
		[Address(RVA = "0x10138CB9C", Offset = "0x138CB9C", VA = "0x10138CB9C")]
		public void SetResourceNo(int fresno)
		{
		}

		// Token: 0x06003235 RID: 12853 RVA: 0x000156A8 File Offset: 0x000138A8
		[Token(Token = "0x6002DE8")]
		[Address(RVA = "0x10138C448", Offset = "0x138C448", VA = "0x10138C448")]
		public int GetResourceNo()
		{
			return 0;
		}

		// Token: 0x06003236 RID: 12854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DE9")]
		[Address(RVA = "0x10138C450", Offset = "0x138C450", VA = "0x10138C450")]
		public void SetUpObject(UnityEngine.Object psetup)
		{
		}

		// Token: 0x06003237 RID: 12855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DEA")]
		[Address(RVA = "0x10138D210", Offset = "0x138D210", VA = "0x10138D210")]
		public void SetTrs(Vector3 fpos, Quaternion frot, Vector3 fscl, int flayer)
		{
		}

		// Token: 0x06003238 RID: 12856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DEB")]
		[Address(RVA = "0x10138D0C0", Offset = "0x138D0C0", VA = "0x10138D0C0")]
		public void SetUpRenderLayer()
		{
		}

		// Token: 0x06003239 RID: 12857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DEC")]
		[Address(RVA = "0x10138D3F0", Offset = "0x138D3F0", VA = "0x10138D3F0")]
		private void Update()
		{
		}

		// Token: 0x0600323A RID: 12858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DED")]
		[Address(RVA = "0x10138D498", Offset = "0x138D498", VA = "0x10138D498")]
		public TownEffectPlayer()
		{
		}

		// Token: 0x040041D6 RID: 16854
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EBD")]
		public bool m_Active;

		// Token: 0x040041D7 RID: 16855
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002EBE")]
		public int m_ResourceNo;

		// Token: 0x040041D8 RID: 16856
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EBF")]
		public int m_LayerNo;

		// Token: 0x040041D9 RID: 16857
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002EC0")]
		public float m_PlayTime;

		// Token: 0x040041DA RID: 16858
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002EC1")]
		public float m_MaxTime;

		// Token: 0x040041DB RID: 16859
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002EC2")]
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040041DC RID: 16860
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002EC3")]
		public GameObject m_EffectNode;
	}
}
