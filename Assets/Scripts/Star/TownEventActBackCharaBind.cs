﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BB1 RID: 2993
	[Token(Token = "0x2000821")]
	[StructLayout(3)]
	public class TownEventActBackCharaBind : ITownEventCommand
	{
		// Token: 0x0600347D RID: 13437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDE")]
		[Address(RVA = "0x10138D4A0", Offset = "0x138D4A0", VA = "0x10138D4A0")]
		public TownEventActBackCharaBind(long fmanageid, int fsyncuid)
		{
		}

		// Token: 0x0600347E RID: 13438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FDF")]
		[Address(RVA = "0x10138D53C", Offset = "0x138D53C", VA = "0x10138D53C")]
		public void AddChara(ITownObjectHandler pchara)
		{
		}

		// Token: 0x0600347F RID: 13439 RVA: 0x000163F8 File Offset: 0x000145F8
		[Token(Token = "0x6002FE0")]
		[Address(RVA = "0x10138D5AC", Offset = "0x138D5AC", VA = "0x10138D5AC", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x0400449B RID: 17563
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400309B")]
		public long m_LinkManageID;

		// Token: 0x0400449C RID: 17564
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400309C")]
		public List<ITownObjectHandler> m_LinkChara;

		// Token: 0x0400449D RID: 17565
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400309D")]
		public int m_SyncUID;
	}
}
