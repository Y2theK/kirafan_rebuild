﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BB2 RID: 2994
	[Token(Token = "0x2000822")]
	[StructLayout(3)]
	public class TownEventQue
	{
		// Token: 0x06003480 RID: 13440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE1")]
		[Address(RVA = "0x10138D6F4", Offset = "0x138D6F4", VA = "0x10138D6F4")]
		public TownEventQue(int ftablenum)
		{
		}

		// Token: 0x06003481 RID: 13441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE2")]
		[Address(RVA = "0x10138D768", Offset = "0x138D768", VA = "0x10138D768")]
		public void PushComCommand(ITownEventCommand pcmd)
		{
		}

		// Token: 0x06003482 RID: 13442 RVA: 0x00016410 File Offset: 0x00014610
		[Token(Token = "0x6002FE3")]
		[Address(RVA = "0x10138D808", Offset = "0x138D808", VA = "0x10138D808")]
		public int GetComCommandNum()
		{
			return 0;
		}

		// Token: 0x06003483 RID: 13443 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FE4")]
		[Address(RVA = "0x10138D810", Offset = "0x138D810", VA = "0x10138D810")]
		public ITownEventCommand GetComCommand(int findex)
		{
			return null;
		}

		// Token: 0x06003484 RID: 13444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE5")]
		[Address(RVA = "0x10138D860", Offset = "0x138D860", VA = "0x10138D860")]
		public void Flush()
		{
		}

		// Token: 0x0400449E RID: 17566
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400309E")]
		public int m_Num;

		// Token: 0x0400449F RID: 17567
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400309F")]
		public int m_Max;

		// Token: 0x040044A0 RID: 17568
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030A0")]
		public ITownEventCommand[] m_Table;
	}
}
