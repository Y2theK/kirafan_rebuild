﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B2D RID: 2861
	[Token(Token = "0x20007D4")]
	[StructLayout(3)]
	public class TownHandleAction : ITownHandleAction
	{
		// Token: 0x06003250 RID: 12880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E03")]
		[Address(RVA = "0x10138DA84", Offset = "0x138DA84", VA = "0x10138DA84")]
		public TownHandleAction()
		{
		}

		// Token: 0x040041FF RID: 16895
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EE6")]
		public ITownObjectHandler m_Target;
	}
}
