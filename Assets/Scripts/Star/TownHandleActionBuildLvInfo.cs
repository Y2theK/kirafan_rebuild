﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B2E RID: 2862
	[Token(Token = "0x20007D5")]
	[StructLayout(3)]
	public class TownHandleActionBuildLvInfo : ITownHandleAction
	{
		// Token: 0x06003251 RID: 12881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E04")]
		[Address(RVA = "0x10138DA8C", Offset = "0x138DA8C", VA = "0x10138DA8C")]
		public TownHandleActionBuildLvInfo(bool viewFlg)
		{
		}

		// Token: 0x04004200 RID: 16896
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EE7")]
		public bool m_viewFlg;
	}
}
