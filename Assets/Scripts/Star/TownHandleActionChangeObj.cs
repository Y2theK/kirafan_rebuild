﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B30 RID: 2864
	[Token(Token = "0x20007D7")]
	[StructLayout(3)]
	public class TownHandleActionChangeObj : ITownHandleAction
	{
		// Token: 0x06003253 RID: 12883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E06")]
		[Address(RVA = "0x10138DAC0", Offset = "0x138DAC0", VA = "0x10138DAC0")]
		public TownHandleActionChangeObj(int fobjid, long fmanageid)
		{
		}

		// Token: 0x04004203 RID: 16899
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EEA")]
		public int m_ObjectID;

		// Token: 0x04004204 RID: 16900
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EEB")]
		public long m_ManageID;
	}
}
