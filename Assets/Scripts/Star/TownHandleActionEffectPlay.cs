﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B31 RID: 2865
	[Token(Token = "0x20007D8")]
	[StructLayout(3)]
	public class TownHandleActionEffectPlay : ITownHandleAction
	{
		// Token: 0x06003254 RID: 12884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E07")]
		[Address(RVA = "0x10138DB04", Offset = "0x138DB04", VA = "0x10138DB04")]
		public TownHandleActionEffectPlay(int feffectno, Vector3 fpos, Vector3 fsize)
		{
		}

		// Token: 0x04004205 RID: 16901
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EEC")]
		public int m_EffectNo;

		// Token: 0x04004206 RID: 16902
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002EED")]
		public Vector3 m_OffsetPos;

		// Token: 0x04004207 RID: 16903
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002EEE")]
		public Vector3 m_Size;
	}
}
