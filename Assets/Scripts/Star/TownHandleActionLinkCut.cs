﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B32 RID: 2866
	[Token(Token = "0x20007D9")]
	[StructLayout(3)]
	public class TownHandleActionLinkCut : ITownHandleAction
	{
		// Token: 0x06003255 RID: 12885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E08")]
		[Address(RVA = "0x10138DB80", Offset = "0x138DB80", VA = "0x10138DB80")]
		public TownHandleActionLinkCut(Transform parent, long fmanageid, int flayerid)
		{
		}

		// Token: 0x04004208 RID: 16904
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EEF")]
		public long m_ManageID;

		// Token: 0x04004209 RID: 16905
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EF0")]
		public int m_LayerID;
	}
}
