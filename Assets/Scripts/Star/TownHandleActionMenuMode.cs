﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B33 RID: 2867
	[Token(Token = "0x20007DA")]
	[StructLayout(3)]
	public class TownHandleActionMenuMode : ITownHandleAction
	{
		// Token: 0x06003256 RID: 12886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E09")]
		[Address(RVA = "0x10138DBC4", Offset = "0x138DBC4", VA = "0x10138DBC4")]
		public TownHandleActionMenuMode(bool fmode)
		{
		}

		// Token: 0x0400420A RID: 16906
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EF1")]
		public bool m_Mode;
	}
}
