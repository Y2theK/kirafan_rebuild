﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B2F RID: 2863
	[Token(Token = "0x20007D6")]
	[StructLayout(3)]
	public class TownHandleActionModelObj : ITownHandleAction
	{
		// Token: 0x06003252 RID: 12882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E05")]
		[Address(RVA = "0x10138DBF8", Offset = "0x138DBF8", VA = "0x10138DBF8")]
		public TownHandleActionModelObj(int fobjid, long fmanageid)
		{
		}

		// Token: 0x04004201 RID: 16897
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EE8")]
		public int m_ObjectID;

		// Token: 0x04004202 RID: 16898
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EE9")]
		public long m_ManageID;
	}
}
