﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B34 RID: 2868
	[Token(Token = "0x20007DB")]
	[StructLayout(3)]
	public class TownHandleActionMovePos : ITownHandleAction
	{
		// Token: 0x06003257 RID: 12887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E0A")]
		[Address(RVA = "0x10138DC3C", Offset = "0x138DC3C", VA = "0x10138DC3C")]
		public TownHandleActionMovePos(long fmanageid, int flayerid)
		{
		}

		// Token: 0x0400420B RID: 16907
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EF2")]
		public Vector3 m_MovePos;

		// Token: 0x0400420C RID: 16908
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002EF3")]
		public long m_ManageID;

		// Token: 0x0400420D RID: 16909
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002EF4")]
		public int m_LayerID;
	}
}
