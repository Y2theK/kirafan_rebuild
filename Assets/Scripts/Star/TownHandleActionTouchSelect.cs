﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B35 RID: 2869
	[Token(Token = "0x20007DC")]
	[StructLayout(3)]
	public class TownHandleActionTouchSelect : ITownHandleAction
	{
		// Token: 0x06003258 RID: 12888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E0B")]
		[Address(RVA = "0x10138DC80", Offset = "0x138DC80", VA = "0x10138DC80")]
		public TownHandleActionTouchSelect(bool fselect)
		{
		}

		// Token: 0x0400420E RID: 16910
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EF5")]
		public bool m_Select;
	}
}
