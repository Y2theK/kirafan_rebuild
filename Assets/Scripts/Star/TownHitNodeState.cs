﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BB3 RID: 2995
	[Token(Token = "0x2000823")]
	[StructLayout(3)]
	public class TownHitNodeState : MonoBehaviour
	{
		// Token: 0x06003485 RID: 13445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE6")]
		[Address(RVA = "0x10138DCB4", Offset = "0x138DCB4", VA = "0x10138DCB4")]
		public void CreateHitTable(int fmax)
		{
		}

		// Token: 0x06003486 RID: 13446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE7")]
		[Address(RVA = "0x10138DD44", Offset = "0x138DD44", VA = "0x10138DD44")]
		public void AddHitNode(GameObject pmodel, GameObject ptrs, eTownOption ftype, int fkey)
		{
		}

		// Token: 0x06003487 RID: 13447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FE8")]
		[Address(RVA = "0x10138DF64", Offset = "0x138DF64", VA = "0x10138DF64")]
		public void AddLocNode(Transform ptrs, eTownOption ftype, int fkey)
		{
		}

		// Token: 0x06003488 RID: 13448 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FE9")]
		[Address(RVA = "0x10138E044", Offset = "0x138E044", VA = "0x10138E044")]
		public GameObject GetHitNode(int fkey)
		{
			return null;
		}

		// Token: 0x06003489 RID: 13449 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FEA")]
		[Address(RVA = "0x10138E12C", Offset = "0x138E12C", VA = "0x10138E12C")]
		public TownHitNodeState.HitState GetHitType(eTownOption ftype)
		{
			return null;
		}

		// Token: 0x0600348A RID: 13450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FEB")]
		[Address(RVA = "0x10138E204", Offset = "0x138E204", VA = "0x10138E204")]
		public void SetHitActive(eTownOption ftype, bool factive)
		{
		}

		// Token: 0x0600348B RID: 13451 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FEC")]
		[Address(RVA = "0x10138E31C", Offset = "0x138E31C", VA = "0x10138E31C")]
		public Transform GetLocNode(int fkey)
		{
			return null;
		}

		// Token: 0x0600348C RID: 13452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FED")]
		[Address(RVA = "0x10138E420", Offset = "0x138E420", VA = "0x10138E420")]
		public TownHitNodeState()
		{
		}

		// Token: 0x040044A1 RID: 17569
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030A1")]
		public TownHitNodeState.HitState[] m_Table;

		// Token: 0x040044A2 RID: 17570
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030A2")]
		public int m_TableNum;

		// Token: 0x040044A3 RID: 17571
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40030A3")]
		public List<TownHitNodeState.LocState> m_LocTable;

		// Token: 0x02000BB4 RID: 2996
		[Token(Token = "0x200105D")]
		[Serializable]
		public class HitState
		{
			// Token: 0x0600348D RID: 13453 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006158")]
			[Address(RVA = "0x10138DF5C", Offset = "0x138DF5C", VA = "0x10138DF5C")]
			public HitState()
			{
			}

			// Token: 0x040044A4 RID: 17572
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400676C")]
			public GameObject m_Model;

			// Token: 0x040044A5 RID: 17573
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400676D")]
			public GameObject m_Node;

			// Token: 0x040044A6 RID: 17574
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400676E")]
			public eTownOption m_Type;

			// Token: 0x040044A7 RID: 17575
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400676F")]
			public int m_HitKey;
		}

		// Token: 0x02000BB5 RID: 2997
		[Token(Token = "0x200105E")]
		public class LocState
		{
			// Token: 0x0600348E RID: 13454 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006159")]
			[Address(RVA = "0x10138E03C", Offset = "0x138E03C", VA = "0x10138E03C")]
			public LocState()
			{
			}

			// Token: 0x040044A8 RID: 17576
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006770")]
			public Transform m_Node;

			// Token: 0x040044A9 RID: 17577
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006771")]
			public eTownOption m_Type;

			// Token: 0x040044AA RID: 17578
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006772")]
			public int m_Key;
		}
	}
}
