﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BB6 RID: 2998
	[Token(Token = "0x2000824")]
	[StructLayout(3)]
	public class TownHudMessageControll
	{
		// Token: 0x0600348F RID: 13455 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002FEE")]
		[Address(RVA = "0x10138E428", Offset = "0x138E428", VA = "0x10138E428")]
		public GameObject GetHudObject(TownResourceObjectData pobjmng, int fobjid)
		{
			return null;
		}

		// Token: 0x06003490 RID: 13456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FEF")]
		[Address(RVA = "0x10138E544", Offset = "0x138E544", VA = "0x10138E544")]
		public void ReleaseHudObject(TownResourceObjectData pobjmng, GameObject pobj)
		{
		}

		// Token: 0x06003491 RID: 13457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF0")]
		[Address(RVA = "0x10138E5E8", Offset = "0x138E5E8", VA = "0x10138E5E8")]
		public TownHudMessageControll()
		{
		}

		// Token: 0x040044AB RID: 17579
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40030A4")]
		private GameObject m_PriFirstHud;
	}
}
