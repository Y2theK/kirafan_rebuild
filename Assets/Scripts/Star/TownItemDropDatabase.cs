﻿using CommonLogic;
using System.Collections.Generic;

namespace Star {
    public class TownItemDropDatabase {
        public static DropItemList ms_DropItemDB;

        public static void DatabaseSetUp() {
            if (ms_DropItemDB == null) {
                ms_DropItemDB = new DropItemList();
                ms_DropItemDB.SetUpResource(GameSystem.Inst.DbMng.TownObjLvUpDB);
            }
        }

        public static bool IsSetUp() {
            return true;
        }

        public static void DatabaseRelease() {
            ms_DropItemDB = null;
        }

        public static DropItemTool GetKeyIDToDatabase(int fkeyid) {
            if (ms_DropItemDB != null) {
                int num = ms_DropItemDB.m_DataList.Length;
                for (int i = 0; i < num; i++) {
                    if (ms_DropItemDB.m_DataList[i].m_KeyID == fkeyid) {
                        return ms_DropItemDB.m_DataList[i];
                    }
                }
            }
            return null;
        }

        public static DropItemTool CalcDropPeformsnce(ref DropItemInfo pstate, int fkeyid, int flevel) {
            DropItemTool drops = GetKeyIDToDatabase(fkeyid);
            if (drops != null) {
                for (int i = 0; i < drops.m_Table.Length; i++) {
                    if (drops.m_Table[i].m_Level == flevel && drops.m_Table[i].m_LimitNum != 0) {
                        pstate.m_TableID = i;
                        pstate.m_UpTime = (long)drops.m_Table[i].m_ChkTime;
                        pstate.m_LimitNum = drops.m_Table[i].m_LimitNum;
                        pstate.m_FirstDropID = drops.m_Table[i].m_Table[0].m_ResultNo;
                        pstate.m_CalcNum = 0;
                        pstate.m_Max = false;
                        break;
                    }
                }
            }
            return drops;
        }

        public class DropItemList {
            public DropItemTool[] m_DataList;

            public void SetUpResource(TownObjectLevelUpDB pdb) {
                OptionPakage options = new OptionPakage();
                for (int i = 0; i < pdb.m_Params.Length; i++) {
                    if (pdb.m_Params[i].m_LimitNum > 0) {
                        OptionPakageKey key = options.GetKey(pdb.m_Params[i].m_ID);
                        key.m_TableID.Add(i);
                    }
                }
                int keyNum = options.GetKeyNum();
                m_DataList = new DropItemTool[keyNum];
                for (int i = 0; i < keyNum; i++) {
                    OptionPakageKey key = options.GetIndexToKey(i);
                    m_DataList[i] = new DropItemTool();
                    key.MakeOptionKeyData(pdb, ref ms_DropItemDB.m_DataList[i]);
                }
            }
        }

        public class OptionPakageKey {
            public int m_KeyID;
            public List<int> m_TableID = new List<int>();

            public void MakeOptionKeyData(TownObjectLevelUpDB pdata, ref DropItemTool pout) {
                pout.m_KeyID = m_KeyID;
                pout.m_Table = new DropItemPakage[m_TableID.Count];
                for (int i = 0; i < m_TableID.Count; i++) {
                    int id = m_TableID[i];
                    pout.m_Table[i].m_Level = pdata.m_Params[id].m_TargetLv;
                    pout.m_Table[i].m_LimitNum = pdata.m_Params[id].m_LimitNum;
                    pout.m_Table[i].m_ChkTime = pdata.m_Params[id].m_WakeTime * 1000;
                    if (pout.m_Table[i].m_LimitNum != 0) {
                        int drops = 10;
                        if (drops > pdata.m_Params[id].m_Parsent.Length) {
                            drops = pdata.m_Params[id].m_Parsent.Length;
                        }
                        if (drops > pdata.m_Params[id].m_ResultNo.Length) {
                            drops = pdata.m_Params[id].m_ResultNo.Length;
                        }
                        pout.m_Table[i].m_Table = new DropItemData[drops];
                        for (int j = 0; j < drops; j++) {
                            pout.m_Table[i].m_Table[j].m_ResultNo = (int)pdata.m_Params[id].m_ResultNo[j];
                            pout.m_Table[i].m_Table[j].m_WakeUp = (short)(pdata.m_Params[id].m_Parsent[j] * 100f);
                        }
                    }
                }
            }
        }

        public class OptionPakage {
            public List<OptionPakageKey> m_List = new List<OptionPakageKey>();

            public OptionPakageKey GetKey(int fkey) {
                for (int i = 0; i < m_List.Count; i++) {
                    if (m_List[i].m_KeyID == fkey) {
                        return m_List[i];
                    }
                }
                OptionPakageKey key = new OptionPakageKey();
                key.m_KeyID = fkey;
                m_List.Add(key);
                return key;
            }

            public int GetKeyNum() {
                return m_List.Count;
            }

            public OptionPakageKey GetIndexToKey(int findex) {
                return m_List[findex];
            }
        }
    }
}
