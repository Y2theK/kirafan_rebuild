﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Home;
using Star.UI.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x02000878 RID: 2168
	[Token(Token = "0x2000650")]
	[StructLayout(3)]
	public class TownMain : GameStateMain
	{
		// Token: 0x17000265 RID: 613
		// (get) Token: 0x060022B4 RID: 8884 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000240")]
		public AdvertiseMovePlayer advertiseMoviePlayer
		{
			[Token(Token = "0x6002010")]
			[Address(RVA = "0x10138F734", Offset = "0x138F734", VA = "0x10138F734")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x060022B5 RID: 8885 RVA: 0x0000F0A8 File Offset: 0x0000D2A8
		[Token(Token = "0x17000241")]
		public TownMain.eMode NowMode
		{
			[Token(Token = "0x6002011")]
			[Address(RVA = "0x10138F73C", Offset = "0x138F73C", VA = "0x10138F73C")]
			get
			{
				return TownMain.eMode.None;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x060022B6 RID: 8886 RVA: 0x0000F0C0 File Offset: 0x0000D2C0
		// (set) Token: 0x060022B7 RID: 8887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000242")]
		public bool IsDoneInfoAPI
		{
			[Token(Token = "0x6002012")]
			[Address(RVA = "0x10138F744", Offset = "0x138F744", VA = "0x10138F744")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002013")]
			[Address(RVA = "0x10138F74C", Offset = "0x138F74C", VA = "0x10138F74C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060022B8 RID: 8888 RVA: 0x0000F0D8 File Offset: 0x0000D2D8
		[Token(Token = "0x6002014")]
		[Address(RVA = "0x10138F754", Offset = "0x138F754", VA = "0x10138F754")]
		public bool IsIdleStep()
		{
			return default(bool);
		}

		// Token: 0x060022B9 RID: 8889 RVA: 0x0000F0F0 File Offset: 0x0000D2F0
		[Token(Token = "0x6002015")]
		[Address(RVA = "0x10138F764", Offset = "0x138F764", VA = "0x10138F764")]
		public bool IsHomeStep()
		{
			return default(bool);
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x060022BA RID: 8890 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000243")]
		public TownBuilder Builder
		{
			[Token(Token = "0x6002016")]
			[Address(RVA = "0x10138F774", Offset = "0x138F774", VA = "0x10138F774")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x060022BB RID: 8891 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000244")]
		public TownUI TownUI
		{
			[Token(Token = "0x6002017")]
			[Address(RVA = "0x10138F77C", Offset = "0x138F77C", VA = "0x10138F77C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x060022BC RID: 8892 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000245")]
		public HomeUI HomeUI
		{
			[Token(Token = "0x6002018")]
			[Address(RVA = "0x10138F784", Offset = "0x138F784", VA = "0x10138F784")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x060022BD RID: 8893 RVA: 0x0000F108 File Offset: 0x0000D308
		// (set) Token: 0x060022BE RID: 8894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000246")]
		public TownMain.eTransit Transit
		{
			[Token(Token = "0x6002019")]
			[Address(RVA = "0x10138F78C", Offset = "0x138F78C", VA = "0x10138F78C")]
			get
			{
				return TownMain.eTransit.None;
			}
			[Token(Token = "0x600201A")]
			[Address(RVA = "0x10138F794", Offset = "0x138F794", VA = "0x10138F794")]
			set
			{
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x060022BF RID: 8895 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060022C0 RID: 8896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000247")]
		public TownMain.BuildRequestData BuildReqData
		{
			[Token(Token = "0x600201B")]
			[Address(RVA = "0x10138F79C", Offset = "0x138F79C", VA = "0x10138F79C")]
			get
			{
				return null;
			}
			[Token(Token = "0x600201C")]
			[Address(RVA = "0x10138F7A4", Offset = "0x138F7A4", VA = "0x10138F7A4")]
			set
			{
			}
		}

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x060022C1 RID: 8897 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060022C2 RID: 8898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000019")]
		private event Action OnTransit
		{
			[Token(Token = "0x600201D")]
			[Address(RVA = "0x10138F7AC", Offset = "0x138F7AC", VA = "0x10138F7AC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600201E")]
			[Address(RVA = "0x10138F8B8", Offset = "0x138F8B8", VA = "0x10138F8B8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060022C3 RID: 8899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600201F")]
		[Address(RVA = "0x10138F9C4", Offset = "0x138F9C4", VA = "0x10138F9C4")]
		public void SetTransitCallBack(Action onTransit)
		{
		}

		// Token: 0x060022C4 RID: 8900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002020")]
		[Address(RVA = "0x10138F9CC", Offset = "0x138F9CC", VA = "0x10138F9CC")]
		private void Start()
		{
		}

		// Token: 0x060022C5 RID: 8901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002021")]
		[Address(RVA = "0x10138FB58", Offset = "0x138FB58", VA = "0x10138FB58", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060022C6 RID: 8902 RVA: 0x0000F120 File Offset: 0x0000D320
		[Token(Token = "0x6002022")]
		[Address(RVA = "0x10138FCCC", Offset = "0x138FCCC", VA = "0x10138FCCC", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x060022C7 RID: 8903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002023")]
		[Address(RVA = "0x10138FCD4", Offset = "0x138FCD4", VA = "0x10138FCD4")]
		public void DestroyReq()
		{
		}

		// Token: 0x060022C8 RID: 8904 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002024")]
		[Address(RVA = "0x10138FD04", Offset = "0x138FD04", VA = "0x10138FD04", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x060022C9 RID: 8905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002025")]
		[Address(RVA = "0x10138FE04", Offset = "0x138FE04", VA = "0x10138FE04")]
		public void RefreshTutorialStep()
		{
		}

		// Token: 0x060022CA RID: 8906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002026")]
		[Address(RVA = "0x101390940", Offset = "0x1390940", VA = "0x101390940")]
		public void InitializeBuilder()
		{
		}

		// Token: 0x060022CB RID: 8907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002027")]
		[Address(RVA = "0x101390A14", Offset = "0x1390A14", VA = "0x101390A14")]
		public void OnComEvent(IMainComCommand pcmd)
		{
		}

		// Token: 0x060022CC RID: 8908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002028")]
		[Address(RVA = "0x101390A48", Offset = "0x1390A48", VA = "0x101390A48")]
		public void FlushCommand()
		{
		}

		// Token: 0x060022CD RID: 8909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002029")]
		[Address(RVA = "0x101393F44", Offset = "0x1393F44", VA = "0x101393F44")]
		public void UpdateEditing()
		{
		}

		// Token: 0x060022CE RID: 8910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202A")]
		[Address(RVA = "0x101394834", Offset = "0x1394834", VA = "0x101394834")]
		private void OnConfirmADVUnlock()
		{
		}

		// Token: 0x060022CF RID: 8911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202B")]
		[Address(RVA = "0x10139432C", Offset = "0x139432C", VA = "0x10139432C")]
		private void UpdateStep()
		{
		}

		// Token: 0x060022D0 RID: 8912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202C")]
		[Address(RVA = "0x1013921D8", Offset = "0x13921D8", VA = "0x1013921D8")]
		private void ChangeStep(TownMain.eStep step)
		{
		}

		// Token: 0x060022D1 RID: 8913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202D")]
		[Address(RVA = "0x101390894", Offset = "0x1390894", VA = "0x101390894")]
		private void SetCameraControllable(bool flg)
		{
		}

		// Token: 0x060022D2 RID: 8914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202E")]
		[Address(RVA = "0x101394BE0", Offset = "0x1394BE0", VA = "0x101394BE0")]
		public void SetEnableInputObj(bool flg)
		{
		}

		// Token: 0x060022D3 RID: 8915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600202F")]
		[Address(RVA = "0x101393A9C", Offset = "0x1393A9C", VA = "0x101393A9C")]
		public void SelectFreeBuff()
		{
		}

		// Token: 0x060022D4 RID: 8916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002030")]
		[Address(RVA = "0x101393B7C", Offset = "0x1393B7C", VA = "0x101393B7C")]
		public void SelectFreeArea()
		{
		}

		// Token: 0x060022D5 RID: 8917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002031")]
		[Address(RVA = "0x101394C30", Offset = "0x1394C30", VA = "0x101394C30")]
		private void OnConfirmBuildContentArea(int btn)
		{
		}

		// Token: 0x060022D6 RID: 8918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002032")]
		[Address(RVA = "0x101394C9C", Offset = "0x1394C9C", VA = "0x101394C9C")]
		public void ExecuteBuild()
		{
		}

		// Token: 0x060022D7 RID: 8919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002033")]
		[Address(RVA = "0x101394CA4", Offset = "0x1394CA4", VA = "0x101394CA4")]
		public void ExecuteLevelUp()
		{
		}

		// Token: 0x060022D8 RID: 8920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002034")]
		[Address(RVA = "0x101394CAC", Offset = "0x1394CAC", VA = "0x101394CAC")]
		public void ExecuteQuick()
		{
		}

		// Token: 0x060022D9 RID: 8921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002035")]
		[Address(RVA = "0x101394CB4", Offset = "0x1394CB4", VA = "0x101394CB4")]
		public void ExecuteSell()
		{
		}

		// Token: 0x060022DA RID: 8922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002036")]
		[Address(RVA = "0x101394CBC", Offset = "0x1394CBC", VA = "0x101394CBC")]
		public void StartPointSelect()
		{
		}

		// Token: 0x060022DB RID: 8923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002037")]
		[Address(RVA = "0x101394EB8", Offset = "0x1394EB8", VA = "0x101394EB8")]
		public void ExecuteMoveObj()
		{
		}

		// Token: 0x060022DC RID: 8924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002038")]
		[Address(RVA = "0x101394EC0", Offset = "0x1394EC0", VA = "0x101394EC0")]
		public void ExecuteStore()
		{
		}

		// Token: 0x060022DD RID: 8925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002039")]
		[Address(RVA = "0x101394910", Offset = "0x1394910", VA = "0x101394910")]
		private void BuildTownObject(TownMain.BuildRequestData data)
		{
		}

		// Token: 0x060022DE RID: 8926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600203A")]
		[Address(RVA = "0x101393EF0", Offset = "0x1393EF0", VA = "0x101393EF0")]
		private void DecideBuildPoint(TownComBuild pcmd)
		{
		}

		// Token: 0x060022DF RID: 8927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600203B")]
		[Address(RVA = "0x101393E9C", Offset = "0x1393E9C", VA = "0x101393E9C")]
		private void DecideBuildArea(TownComBuild pcmd)
		{
		}

		// Token: 0x060022E0 RID: 8928 RVA: 0x0000F138 File Offset: 0x0000D338
		[Token(Token = "0x600203C")]
		[Address(RVA = "0x101394EC8", Offset = "0x1394EC8", VA = "0x101394EC8")]
		public bool InitializeUI()
		{
			return default(bool);
		}

		// Token: 0x060022E1 RID: 8929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600203D")]
		[Address(RVA = "0x101395520", Offset = "0x1395520", VA = "0x101395520")]
		public void StartMode(TownMain.eMode mode)
		{
		}

		// Token: 0x060022E2 RID: 8930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600203E")]
		[Address(RVA = "0x101395A74", Offset = "0x1395A74", VA = "0x101395A74")]
		public void ReleaseInputBlock()
		{
		}

		// Token: 0x060022E3 RID: 8931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600203F")]
		[Address(RVA = "0x101395B0C", Offset = "0x1395B0C", VA = "0x101395B0C")]
		public void EndMode(bool changeMode = false)
		{
		}

		// Token: 0x060022E4 RID: 8932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002040")]
		[Address(RVA = "0x1013958E8", Offset = "0x13958E8", VA = "0x1013958E8")]
		public void SetCameraHomePosition(float time)
		{
		}

		// Token: 0x060022E5 RID: 8933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002041")]
		[Address(RVA = "0x101395CC4", Offset = "0x1395CC4", VA = "0x101395CC4")]
		private void OnClickBuildButton()
		{
		}

		// Token: 0x060022E6 RID: 8934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002042")]
		[Address(RVA = "0x101395CCC", Offset = "0x1395CCC", VA = "0x101395CCC")]
		private void OnClickBuildPointCancelButton()
		{
		}

		// Token: 0x060022E7 RID: 8935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002043")]
		[Address(RVA = "0x101395CD4", Offset = "0x1395CD4", VA = "0x101395CD4")]
		private void OnClickMovePointCancelButton()
		{
		}

		// Token: 0x060022E8 RID: 8936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002044")]
		[Address(RVA = "0x101395CDC", Offset = "0x1395CDC", VA = "0x101395CDC")]
		private void OnConfirmMoveCallBack(int btn)
		{
		}

		// Token: 0x060022E9 RID: 8937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002045")]
		[Address(RVA = "0x101395D38", Offset = "0x1395D38", VA = "0x101395D38")]
		private void OnConfirmSwapArea(int btn)
		{
		}

		// Token: 0x060022EA RID: 8938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002046")]
		[Address(RVA = "0x101395D48", Offset = "0x1395D48", VA = "0x101395D48")]
		private void OnConfirmStore(int btn)
		{
		}

		// Token: 0x060022EB RID: 8939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002047")]
		[Address(RVA = "0x101395D84", Offset = "0x1395D84", VA = "0x101395D84")]
		private void OnDecideLiveChara(long[] charaMngIDs)
		{
		}

		// Token: 0x060022EC RID: 8940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002048")]
		[Address(RVA = "0x101395E1C", Offset = "0x1395E1C", VA = "0x101395E1C")]
		private void ShowGetItems(List<TownMain.ItemStack> list, float delay)
		{
		}

		// Token: 0x060022ED RID: 8941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002049")]
		[Address(RVA = "0x101396104", Offset = "0x1396104", VA = "0x101396104")]
		private void OnClosedCharaTweetWindow()
		{
		}

		// Token: 0x060022EE RID: 8942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204A")]
		[Address(RVA = "0x101396130", Offset = "0x1396130", VA = "0x101396130")]
		public void OnClickTownButtonCallBack()
		{
		}

		// Token: 0x060022EF RID: 8943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204B")]
		[Address(RVA = "0x101393A70", Offset = "0x1393A70", VA = "0x101393A70")]
		public void OnClickRoomButtonCallBack()
		{
		}

		// Token: 0x060022F0 RID: 8944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204C")]
		[Address(RVA = "0x10139615C", Offset = "0x139615C", VA = "0x10139615C")]
		public void OnClickTrainingIcon()
		{
		}

		// Token: 0x060022F1 RID: 8945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204D")]
		[Address(RVA = "0x101396188", Offset = "0x1396188", VA = "0x101396188")]
		public void OnClickContentRoomButtonCallBack()
		{
		}

		// Token: 0x060022F2 RID: 8946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204E")]
		[Address(RVA = "0x101393E68", Offset = "0x1393E68", VA = "0x101393E68")]
		public void ChangeSceneQuest()
		{
		}

		// Token: 0x060022F3 RID: 8947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600204F")]
		[Address(RVA = "0x101393DB8", Offset = "0x1393DB8", VA = "0x101393DB8")]
		public void ChangeSceneShop()
		{
		}

		// Token: 0x060022F4 RID: 8948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002050")]
		[Address(RVA = "0x101393E3C", Offset = "0x1393E3C", VA = "0x101393E3C")]
		public void ChangeSceneEdit()
		{
		}

		// Token: 0x060022F5 RID: 8949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002051")]
		[Address(RVA = "0x101393DE4", Offset = "0x1393DE4", VA = "0x101393DE4")]
		public void ChangeSceneGacha()
		{
		}

		// Token: 0x060022F6 RID: 8950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002052")]
		[Address(RVA = "0x101393E10", Offset = "0x1393E10", VA = "0x101393E10")]
		public void ChangeSceneTraining()
		{
		}

		// Token: 0x060022F7 RID: 8951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002053")]
		[Address(RVA = "0x101396570", Offset = "0x1396570", VA = "0x101396570")]
		public void ChangeSceneUserInfo()
		{
		}

		// Token: 0x060022F8 RID: 8952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002054")]
		[Address(RVA = "0x101393E94", Offset = "0x1393E94", VA = "0x101393E94")]
		public void OpenMissionBoard()
		{
		}

		// Token: 0x060022F9 RID: 8953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002055")]
		[Address(RVA = "0x10139659C", Offset = "0x139659C", VA = "0x10139659C")]
		public void OpenBeginnersMission()
		{
		}

		// Token: 0x060022FA RID: 8954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002056")]
		[Address(RVA = "0x1013965A4", Offset = "0x13965A4", VA = "0x1013965A4")]
		public void OnClickQuestButtonCallBack()
		{
		}

		// Token: 0x060022FB RID: 8955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002057")]
		[Address(RVA = "0x1013966B8", Offset = "0x13966B8", VA = "0x1013966B8")]
		public void OnClickGachaButtonCallBack()
		{
		}

		// Token: 0x060022FC RID: 8956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002058")]
		[Address(RVA = "0x1013966E4", Offset = "0x13966E4", VA = "0x1013966E4")]
		public void OpenPresentWindow()
		{
		}

		// Token: 0x060022FD RID: 8957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002059")]
		[Address(RVA = "0x101394AC0", Offset = "0x1394AC0", VA = "0x101394AC0")]
		public void Request_Sale(long mngID, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x060022FE RID: 8958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600205A")]
		[Address(RVA = "0x101396710", Offset = "0x1396710", VA = "0x101396710")]
		public void OnResponse_Sale(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060022FF RID: 8959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600205B")]
		[Address(RVA = "0x10139688C", Offset = "0x139688C", VA = "0x10139688C")]
		public void TownGridDataReConnection()
		{
		}

		// Token: 0x06002300 RID: 8960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600205C")]
		[Address(RVA = "0x101396A28", Offset = "0x1396A28", VA = "0x101396A28")]
		public TownMain()
		{
		}

		// Token: 0x0400330E RID: 13070
		[Token(Token = "0x40025FF")]
		public const int STATE_INIT = 0;

		// Token: 0x0400330F RID: 13071
		[Token(Token = "0x4002600")]
		public const int STATE_MAIN = 1;

		// Token: 0x04003310 RID: 13072
		[Token(Token = "0x4002601")]
		public const int STATE_HOME = 2;

		// Token: 0x04003311 RID: 13073
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002602")]
		[SerializeField]
		private TownCamera m_GameCamera;

		// Token: 0x04003312 RID: 13074
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002603")]
		[SerializeField]
		private TownBuilder m_Builder;

		// Token: 0x04003313 RID: 13075
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002604")]
		private TownUI m_TownUI;

		// Token: 0x04003314 RID: 13076
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002605")]
		private HomeUI m_HomeUI;

		// Token: 0x04003315 RID: 13077
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002606")]
		[SerializeField]
		private TownMain.StartUpTownInfo m_StartUp;

		// Token: 0x04003316 RID: 13078
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002607")]
		[SerializeField]
		private float m_TownStartSize;

		// Token: 0x04003317 RID: 13079
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002608")]
		[SerializeField]
		private float m_TownStartTransitTime;

		// Token: 0x04003318 RID: 13080
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002609")]
		[SerializeField]
		private AdvertiseMovePlayer m_AdvertiseMoviePlayer;

		// Token: 0x04003319 RID: 13081
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400260A")]
		private TownMain.eMode m_NowMode;

		// Token: 0x0400331A RID: 13082
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400260B")]
		private bool m_IsInitilalizedUI;

		// Token: 0x0400331C RID: 13084
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400260D")]
		private TownMain.eStep m_Step;

		// Token: 0x0400331D RID: 13085
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x400260E")]
		private TownMain.eTransit m_Transit;

		// Token: 0x0400331E RID: 13086
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400260F")]
		private TownMain.BuildRequestData m_BuildRequestData;

		// Token: 0x0400331F RID: 13087
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002610")]
		private int m_MoveBaseBuildPoint;

		// Token: 0x04003320 RID: 13088
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4002611")]
		private int m_MoveTargetBuildPoint;

		// Token: 0x04003321 RID: 13089
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002612")]
		private long m_MoveTargetManageID;

		// Token: 0x04003322 RID: 13090
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002613")]
		private List<TownMain.ItemStack> m_ItemStackList;

		// Token: 0x04003323 RID: 13091
		[Token(Token = "0x4002614")]
		private const float TUTORIALBUILDWAIT = 3f;

		// Token: 0x04003324 RID: 13092
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002615")]
		private float m_TutorialWaitBuildContent;

		// Token: 0x04003325 RID: 13093
		[Token(Token = "0x4002616")]
		private const string TUTORIAL_CONTENT_POINT = "LOC_AREA_00_3/Content2";

		// Token: 0x04003326 RID: 13094
		[Token(Token = "0x4002617")]
		private const string TUTORIAL_WEAPON_POINT = "LOC_AREA_00_3/LOC_07";

		// Token: 0x04003327 RID: 13095
		[Token(Token = "0x4002618")]
		private const string TUTORIAL_MONEY_POINT = "LOC_AREA_00_3/LOC_06";

		// Token: 0x04003328 RID: 13096
		[Token(Token = "0x4002619")]
		private const int TUTORIAL_CONTENT_IDX = 2;

		// Token: 0x04003329 RID: 13097
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400261A")]
		private TownComManager m_ComEvent;

		// Token: 0x0400332A RID: 13098
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400261B")]
		private bool m_IsReserveADVCheck;

		// Token: 0x02000879 RID: 2169
		[Token(Token = "0x2000EF8")]
		[Serializable]
		private class StartUpTownInfo
		{
			// Token: 0x06002309 RID: 8969 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F62")]
			[Address(RVA = "0x101396AFC", Offset = "0x1396AFC", VA = "0x101396AFC")]
			public StartUpTownInfo()
			{
			}

			// Token: 0x0400332C RID: 13100
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006082")]
			public Vector2 m_Pos;

			// Token: 0x0400332D RID: 13101
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006083")]
			public float m_Size;

			// Token: 0x0400332E RID: 13102
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006084")]
			public bool m_Ena;
		}

		// Token: 0x0200087A RID: 2170
		[Token(Token = "0x2000EF9")]
		public enum eMode
		{
			// Token: 0x04003330 RID: 13104
			[Token(Token = "0x4006086")]
			None,
			// Token: 0x04003331 RID: 13105
			[Token(Token = "0x4006087")]
			Home,
			// Token: 0x04003332 RID: 13106
			[Token(Token = "0x4006088")]
			Town
		}

		// Token: 0x0200087B RID: 2171
		[Token(Token = "0x2000EFA")]
		private enum eStep
		{
			// Token: 0x04003334 RID: 13108
			[Token(Token = "0x400608A")]
			Home,
			// Token: 0x04003335 RID: 13109
			[Token(Token = "0x400608B")]
			Idle,
			// Token: 0x04003336 RID: 13110
			[Token(Token = "0x400608C")]
			BuildPointSelect,
			// Token: 0x04003337 RID: 13111
			[Token(Token = "0x400608D")]
			BuildPointSelectDecide,
			// Token: 0x04003338 RID: 13112
			[Token(Token = "0x400608E")]
			BuildPointSelectCancel,
			// Token: 0x04003339 RID: 13113
			[Token(Token = "0x400608F")]
			BuildObjectSelect,
			// Token: 0x0400333A RID: 13114
			[Token(Token = "0x4006090")]
			BuildObjectSelectArea,
			// Token: 0x0400333B RID: 13115
			[Token(Token = "0x4006091")]
			BuildObjectSelectBuff,
			// Token: 0x0400333C RID: 13116
			[Token(Token = "0x4006092")]
			ExecuteBuild,
			// Token: 0x0400333D RID: 13117
			[Token(Token = "0x4006093")]
			ExecuteLevelUp,
			// Token: 0x0400333E RID: 13118
			[Token(Token = "0x4006094")]
			ExecuteQuick,
			// Token: 0x0400333F RID: 13119
			[Token(Token = "0x4006095")]
			ExecuteStore,
			// Token: 0x04003340 RID: 13120
			[Token(Token = "0x4006096")]
			ExecuteSell,
			// Token: 0x04003341 RID: 13121
			[Token(Token = "0x4006097")]
			MovePointSelect,
			// Token: 0x04003342 RID: 13122
			[Token(Token = "0x4006098")]
			MovePointDecide,
			// Token: 0x04003343 RID: 13123
			[Token(Token = "0x4006099")]
			MovePointCancel,
			// Token: 0x04003344 RID: 13124
			[Token(Token = "0x400609A")]
			ExecuteMoveObj,
			// Token: 0x04003345 RID: 13125
			[Token(Token = "0x400609B")]
			Mission,
			// Token: 0x04003346 RID: 13126
			[Token(Token = "0x400609C")]
			Transit,
			// Token: 0x04003347 RID: 13127
			[Token(Token = "0x400609D")]
			BeginnersMission,
			// Token: 0x04003348 RID: 13128
			[Token(Token = "0x400609E")]
			BeginnersMissionEndProduction
		}

		// Token: 0x0200087C RID: 2172
		[Token(Token = "0x2000EFB")]
		public enum eTransit
		{
			// Token: 0x0400334A RID: 13130
			[Token(Token = "0x40060A0")]
			None,
			// Token: 0x0400334B RID: 13131
			[Token(Token = "0x40060A1")]
			Home,
			// Token: 0x0400334C RID: 13132
			[Token(Token = "0x40060A2")]
			Town,
			// Token: 0x0400334D RID: 13133
			[Token(Token = "0x40060A3")]
			Quest,
			// Token: 0x0400334E RID: 13134
			[Token(Token = "0x40060A4")]
			Edit,
			// Token: 0x0400334F RID: 13135
			[Token(Token = "0x40060A5")]
			Room,
			// Token: 0x04003350 RID: 13136
			[Token(Token = "0x40060A6")]
			Gacha,
			// Token: 0x04003351 RID: 13137
			[Token(Token = "0x40060A7")]
			Shop,
			// Token: 0x04003352 RID: 13138
			[Token(Token = "0x40060A8")]
			Training,
			// Token: 0x04003353 RID: 13139
			[Token(Token = "0x40060A9")]
			ADV,
			// Token: 0x04003354 RID: 13140
			[Token(Token = "0x40060AA")]
			Present,
			// Token: 0x04003355 RID: 13141
			[Token(Token = "0x40060AB")]
			Movie,
			// Token: 0x04003356 RID: 13142
			[Token(Token = "0x40060AC")]
			UserInfo,
			// Token: 0x04003357 RID: 13143
			[Token(Token = "0x40060AD")]
			ContentRoom
		}

		// Token: 0x0200087D RID: 2173
		[Token(Token = "0x2000EFC")]
		public class BuildRequestData
		{
			// Token: 0x0600230A RID: 8970 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F63")]
			[Address(RVA = "0x101394904", Offset = "0x1394904", VA = "0x101394904")]
			public void Clear()
			{
			}

			// Token: 0x0600230B RID: 8971 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F64")]
			[Address(RVA = "0x101396B5C", Offset = "0x1396B5C", VA = "0x101396B5C")]
			public BuildRequestData()
			{
			}

			// Token: 0x04003358 RID: 13144
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40060AE")]
			public int m_ObjID;

			// Token: 0x04003359 RID: 13145
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40060AF")]
			public int m_BuildPoint;

			// Token: 0x0400335A RID: 13146
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40060B0")]
			public long m_MngID;
		}

		// Token: 0x0200087E RID: 2174
		[Token(Token = "0x2000EFD")]
		public class ItemStack
		{
			// Token: 0x0600230C RID: 8972 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6005F65")]
			[Address(RVA = "0x1013921A0", Offset = "0x13921A0", VA = "0x1013921A0")]
			public ItemStack(int id, int num)
			{
			}

			// Token: 0x0400335B RID: 13147
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40060B1")]
			public int m_ItemID;

			// Token: 0x0400335C RID: 13148
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40060B2")]
			public int m_Num;
		}
	}
}
