﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B9A RID: 2970
	[Token(Token = "0x2000812")]
	[StructLayout(3)]
	public class TownMakerPoint : MonoBehaviour
	{
		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x0600341C RID: 13340 RVA: 0x000160B0 File Offset: 0x000142B0
		[Token(Token = "0x170003A6")]
		public int Index
		{
			[Token(Token = "0x6002F85")]
			[Address(RVA = "0x101397134", Offset = "0x1397134", VA = "0x101397134")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x0600341D RID: 13341 RVA: 0x000160C8 File Offset: 0x000142C8
		[Token(Token = "0x170003A7")]
		public int OrderInLayer
		{
			[Token(Token = "0x6002F86")]
			[Address(RVA = "0x10139713C", Offset = "0x139713C", VA = "0x10139713C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x0600341E RID: 13342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F87")]
		[Address(RVA = "0x101397144", Offset = "0x1397144", VA = "0x101397144")]
		private void Awake()
		{
		}

		// Token: 0x0600341F RID: 13343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F88")]
		[Address(RVA = "0x101397148", Offset = "0x1397148", VA = "0x101397148")]
		public void SetEnable(bool flg)
		{
		}

		// Token: 0x06003420 RID: 13344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F89")]
		[Address(RVA = "0x101397654", Offset = "0x1397654", VA = "0x101397654")]
		private void SetEnableColor(float value)
		{
		}

		// Token: 0x06003421 RID: 13345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F8A")]
		[Address(RVA = "0x10139771C", Offset = "0x139771C", VA = "0x10139771C")]
		public TownMakerPoint()
		{
		}

		// Token: 0x0400443D RID: 17469
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003059")]
		[SerializeField]
		private SpriteRenderer m_Sprite;

		// Token: 0x0400443E RID: 17470
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400305A")]
		[SerializeField]
		private TownBuildArrow m_Arrow;

		// Token: 0x0400443F RID: 17471
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400305B")]
		[SerializeField]
		private int m_Index;

		// Token: 0x04004440 RID: 17472
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400305C")]
		[SerializeField]
		private int m_OrderInLayer;
	}
}
