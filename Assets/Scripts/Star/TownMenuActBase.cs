﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B37 RID: 2871
	[Token(Token = "0x20007DE")]
	[StructLayout(3)]
	public class TownMenuActBase : ITownMenuAct
	{
		// Token: 0x0600325B RID: 12891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E0E")]
		[Address(RVA = "0x101397724", Offset = "0x1397724", VA = "0x101397724", Slot = "4")]
		public void SetUp(TownBuilder pbuild)
		{
		}

		// Token: 0x0600325C RID: 12892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E0F")]
		[Address(RVA = "0x101397728", Offset = "0x1397728", VA = "0x101397728", Slot = "5")]
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
		}

		// Token: 0x0600325D RID: 12893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E10")]
		[Address(RVA = "0x10139772C", Offset = "0x139772C", VA = "0x10139772C")]
		public TownMenuActBase()
		{
		}
	}
}
