﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B39 RID: 2873
	[Token(Token = "0x20007E0")]
	[StructLayout(3)]
	public class TownMenuActMission : ITownMenuAct
	{
		// Token: 0x06003260 RID: 12896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E13")]
		[Address(RVA = "0x101397800", Offset = "0x1397800", VA = "0x101397800", Slot = "4")]
		public void SetUp(TownBuilder pbuild)
		{
		}

		// Token: 0x06003261 RID: 12897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E14")]
		[Address(RVA = "0x101397808", Offset = "0x1397808", VA = "0x101397808", Slot = "5")]
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
		}

		// Token: 0x06003262 RID: 12898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E15")]
		[Address(RVA = "0x1013977F0", Offset = "0x13977F0", VA = "0x1013977F0")]
		public TownMenuActMission()
		{
		}

		// Token: 0x0400420F RID: 16911
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002EF6")]
		private TownBuilder m_Builder;

		// Token: 0x04004210 RID: 16912
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EF7")]
		private GameObject m_MenuIcon;

		// Token: 0x04004211 RID: 16913
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EF8")]
		private TownPartsPopObject m_IconUp;
	}
}
