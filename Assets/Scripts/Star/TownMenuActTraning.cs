﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B3A RID: 2874
	[Token(Token = "0x20007E1")]
	[StructLayout(3)]
	public class TownMenuActTraning : ITownMenuAct
	{
		// Token: 0x06003263 RID: 12899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E16")]
		[Address(RVA = "0x101397BA4", Offset = "0x1397BA4", VA = "0x101397BA4", Slot = "4")]
		public void SetUp(TownBuilder pbuild)
		{
		}

		// Token: 0x06003264 RID: 12900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E17")]
		[Address(RVA = "0x101397C18", Offset = "0x1397C18", VA = "0x101397C18", Slot = "5")]
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
		}

		// Token: 0x06003265 RID: 12901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E18")]
		[Address(RVA = "0x101397F44", Offset = "0x1397F44", VA = "0x101397F44")]
		private void EntryChara(long fmanageid, TownObjHandleMenu pmenu)
		{
		}

		// Token: 0x06003266 RID: 12902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E19")]
		[Address(RVA = "0x101398358", Offset = "0x1398358", VA = "0x101398358")]
		private void ReleaseCharaChk()
		{
		}

		// Token: 0x06003267 RID: 12903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E1A")]
		[Address(RVA = "0x101397C1C", Offset = "0x1397C1C", VA = "0x101397C1C")]
		private void UpdateCharaIcon(TownObjHandleMenu pmenu)
		{
		}

		// Token: 0x06003268 RID: 12904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E1B")]
		[Address(RVA = "0x1013977E8", Offset = "0x13977E8", VA = "0x1013977E8")]
		public TownMenuActTraning()
		{
		}

		// Token: 0x04004212 RID: 16914
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002EF9")]
		private GameObject m_MenuIcon;

		// Token: 0x04004213 RID: 16915
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EFA")]
		private TownBuilder m_Builder;

		// Token: 0x04004214 RID: 16916
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EFB")]
		private TownMenuActTraning.TownIconModel m_IconUp;

		// Token: 0x04004215 RID: 16917
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002EFC")]
		public TownMenuActTraning.TraningCharaIcon[] m_CharaTable;

		// Token: 0x04004216 RID: 16918
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002EFD")]
		public int m_CharaEntryNum;

		// Token: 0x04004217 RID: 16919
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002EFE")]
		public int m_CharaEntryMax;

		// Token: 0x02000B3B RID: 2875
		[Token(Token = "0x2001026")]
		public class TownIconModel
		{
			// Token: 0x06003269 RID: 12905 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006106")]
			[Address(RVA = "0x101398570", Offset = "0x1398570", VA = "0x101398570")]
			public void SetIconModel(GameObject pobj)
			{
			}

			// Token: 0x0600326A RID: 12906 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006107")]
			[Address(RVA = "0x101398840", Offset = "0x1398840", VA = "0x101398840")]
			public void PlayMotion(int findex)
			{
			}

			// Token: 0x0600326B RID: 12907 RVA: 0x000156F0 File Offset: 0x000138F0
			[Token(Token = "0x6006108")]
			[Address(RVA = "0x1013988E4", Offset = "0x13988E4", VA = "0x1013988E4")]
			public bool IsPlaying(int findex)
			{
				return default(bool);
			}

			// Token: 0x0600326C RID: 12908 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006109")]
			[Address(RVA = "0x101398974", Offset = "0x1398974", VA = "0x101398974")]
			public void ChangeLayer(int flayerid)
			{
			}

			// Token: 0x0600326D RID: 12909 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600610A")]
			[Address(RVA = "0x101398A24", Offset = "0x1398A24", VA = "0x101398A24")]
			public TownIconModel()
			{
			}

			// Token: 0x04004218 RID: 16920
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006685")]
			private Animation m_PlayAnime;

			// Token: 0x04004219 RID: 16921
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006686")]
			private AnimationState[] m_AnimeTable;

			// Token: 0x0400421A RID: 16922
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006687")]
			private MeshRenderer m_Render;

			// Token: 0x0400421B RID: 16923
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006688")]
			public int m_PlayAnimeNo;
		}

		// Token: 0x02000B3C RID: 2876
		[Token(Token = "0x2001027")]
		public class TraningCharaIcon
		{
			// Token: 0x0600326E RID: 12910 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600610B")]
			[Address(RVA = "0x101398184", Offset = "0x1398184", VA = "0x101398184")]
			public void SetUpModel(long fmanageid, TownBuilder pbuilder, TownObjHandleMenu phandle)
			{
			}

			// Token: 0x0600326F RID: 12911 RVA: 0x00015708 File Offset: 0x00013908
			[Token(Token = "0x600610C")]
			[Address(RVA = "0x1013984B0", Offset = "0x13984B0", VA = "0x1013984B0")]
			public bool UpdateModel(TownBuilder pbuilder, TownObjHandleMenu pmenu)
			{
				return default(bool);
			}

			// Token: 0x06003270 RID: 12912 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600610D")]
			[Address(RVA = "0x101398440", Offset = "0x1398440", VA = "0x101398440")]
			public void SetPoint(Vector3 fpos)
			{
			}

			// Token: 0x06003271 RID: 12913 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600610E")]
			[Address(RVA = "0x101398434", Offset = "0x1398434", VA = "0x101398434")]
			public void SetReleaseStep()
			{
			}

			// Token: 0x06003272 RID: 12914 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600610F")]
			[Address(RVA = "0x10139817C", Offset = "0x139817C", VA = "0x10139817C")]
			public TraningCharaIcon()
			{
			}

			// Token: 0x0400421C RID: 16924
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006689")]
			public bool m_Search;

			// Token: 0x0400421D RID: 16925
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400668A")]
			public long m_ManageID;

			// Token: 0x0400421E RID: 16926
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400668B")]
			private GameObject m_Object;

			// Token: 0x0400421F RID: 16927
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400668C")]
			private TownObjHandleMenuChara m_Model;

			// Token: 0x04004220 RID: 16928
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400668D")]
			public int m_Step;
		}
	}
}
