﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BB7 RID: 2999
	[Token(Token = "0x2000825")]
	[StructLayout(3)]
	public class TownModelIO : MonoBehaviour
	{
		// Token: 0x06003492 RID: 13458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF1")]
		[Address(RVA = "0x101398A2C", Offset = "0x1398A2C", VA = "0x101398A2C")]
		private void Awake()
		{
		}

		// Token: 0x06003493 RID: 13459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF2")]
		[Address(RVA = "0x101398E7C", Offset = "0x1398E7C", VA = "0x101398E7C")]
		private void Update()
		{
		}

		// Token: 0x06003494 RID: 13460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF3")]
		[Address(RVA = "0x101398EEC", Offset = "0x1398EEC", VA = "0x101398EEC")]
		private void UpMsbModelColor(Color fcol)
		{
		}

		// Token: 0x06003495 RID: 13461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF4")]
		[Address(RVA = "0x1013990F0", Offset = "0x13990F0", VA = "0x1013990F0")]
		private void UpModelColor(Color fcol)
		{
		}

		// Token: 0x06003496 RID: 13462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF5")]
		[Address(RVA = "0x101399210", Offset = "0x1399210", VA = "0x101399210")]
		public TownModelIO()
		{
		}

		// Token: 0x040044AC RID: 17580
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030A5")]
		public Color ModelColor;

		// Token: 0x040044AD RID: 17581
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40030A6")]
		public Vector2 ModelUvOffset;

		// Token: 0x040044AE RID: 17582
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40030A7")]
		private Color m_BackColor;

		// Token: 0x040044AF RID: 17583
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40030A8")]
		private TownModelIO.MsbModelHandle[] m_MsbHandle;

		// Token: 0x040044B0 RID: 17584
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40030A9")]
		private TownModelIO.ModelHandle[] m_ModelHandle;

		// Token: 0x02000BB8 RID: 3000
		[Token(Token = "0x200105F")]
		private struct MsbModelHandle
		{
			// Token: 0x040044B1 RID: 17585
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006773")]
			public MsbHandler m_Handle;

			// Token: 0x040044B2 RID: 17586
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006774")]
			public int m_Num;

			// Token: 0x040044B3 RID: 17587
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006775")]
			public Color[] m_Color;
		}

		// Token: 0x02000BB9 RID: 3001
		[Token(Token = "0x2001060")]
		private struct ModelHandle
		{
			// Token: 0x040044B4 RID: 17588
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006776")]
			public Renderer m_Handle;
		}
	}
}
