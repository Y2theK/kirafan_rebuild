﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B20 RID: 2848
	[Token(Token = "0x20007CB")]
	[StructLayout(3)]
	public class TownNetComManager : IFldNetComManager
	{
		// Token: 0x0600321B RID: 12827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DCF")]
		[Address(RVA = "0x1013992A0", Offset = "0x13992A0", VA = "0x1013992A0")]
		private void SetNextStep(TownNetComManager.eComStep fnextup)
		{
		}

		// Token: 0x0600321C RID: 12828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD0")]
		[Address(RVA = "0x101399480", Offset = "0x1399480", VA = "0x101399480")]
		public static void SetUpManager(GameObject ptarget, bool resetnet)
		{
		}

		// Token: 0x0600321D RID: 12829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD1")]
		[Address(RVA = "0x101399538", Offset = "0x1399538", VA = "0x101399538")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600321E RID: 12830 RVA: 0x00015648 File Offset: 0x00013848
		[Token(Token = "0x6002DD2")]
		[Address(RVA = "0x1013995EC", Offset = "0x13995EC", VA = "0x1013995EC")]
		public static bool IsSetUp()
		{
			return default(bool);
		}

		// Token: 0x0600321F RID: 12831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD3")]
		[Address(RVA = "0x101399654", Offset = "0x1399654", VA = "0x101399654")]
		private void Update()
		{
		}

		// Token: 0x06003220 RID: 12832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DD4")]
		[Address(RVA = "0x1013996A8", Offset = "0x13996A8", VA = "0x1013996A8")]
		public TownNetComManager()
		{
		}

		// Token: 0x040041B4 RID: 16820
		[Token(Token = "0x4002EAB")]
		private static TownNetComManager Inst;

		// Token: 0x040041B5 RID: 16821
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002EAC")]
		private TownNetComManager.eComStep m_Step;

		// Token: 0x02000B21 RID: 2849
		[Token(Token = "0x2001022")]
		public enum eComStep
		{
			// Token: 0x040041B7 RID: 16823
			[Token(Token = "0x4006676")]
			ListIn,
			// Token: 0x040041B8 RID: 16824
			[Token(Token = "0x4006677")]
			DefObjSetUp,
			// Token: 0x040041B9 RID: 16825
			[Token(Token = "0x4006678")]
			DefSetUp,
			// Token: 0x040041BA RID: 16826
			[Token(Token = "0x4006679")]
			Update
		}
	}
}
