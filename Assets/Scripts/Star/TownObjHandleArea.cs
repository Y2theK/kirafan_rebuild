﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B5B RID: 2907
	[Token(Token = "0x20007F5")]
	[StructLayout(3)]
	public class TownObjHandleArea : ITownObjectHandler
	{
		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x060032CA RID: 13002 RVA: 0x000158D0 File Offset: 0x00013AD0
		// (set) Token: 0x060032C9 RID: 13001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700039E")]
		public override bool isUnderConstruction
		{
			[Token(Token = "0x6002E69")]
			[Address(RVA = "0x101399758", Offset = "0x1399758", VA = "0x101399758", Slot = "5")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002E68")]
			[Address(RVA = "0x1013996B0", Offset = "0x13996B0", VA = "0x1013996B0", Slot = "4")]
			set
			{
			}
		}

		// Token: 0x060032CB RID: 13003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E6A")]
		[Address(RVA = "0x101399760", Offset = "0x1399760", VA = "0x101399760", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x060032CC RID: 13004 RVA: 0x000158E8 File Offset: 0x00013AE8
		[Token(Token = "0x6002E6B")]
		[Address(RVA = "0x101399884", Offset = "0x1399884", VA = "0x101399884")]
		public bool IsCompleteAreaEvt()
		{
			return default(bool);
		}

		// Token: 0x060032CD RID: 13005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E6C")]
		[Address(RVA = "0x10139988C", Offset = "0x139988C", VA = "0x10139988C")]
		private void CreateBuildTree(TownBuildPakage ppakage, GameObject proot, int fbuildpoint, long fbuildmngid, bool ftype)
		{
		}

		// Token: 0x060032CE RID: 13006 RVA: 0x00015900 File Offset: 0x00013B00
		[Token(Token = "0x6002E6D")]
		[Address(RVA = "0x1013999A4", Offset = "0x13999A4", VA = "0x1013999A4", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x060032CF RID: 13007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E6E")]
		[Address(RVA = "0x10139A094", Offset = "0x139A094", VA = "0x10139A094")]
		private void BuildUpAreaHandle()
		{
		}

		// Token: 0x060032D0 RID: 13008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E6F")]
		[Address(RVA = "0x10139A7DC", Offset = "0x139A7DC", VA = "0x10139A7DC")]
		private void Update()
		{
		}

		// Token: 0x060032D1 RID: 13009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E70")]
		[Address(RVA = "0x101399D98", Offset = "0x1399D98", VA = "0x101399D98")]
		public static void MakeBuildPoint(Transform ptrs, TownObjHandleArea.CAreaPointSetUp plist, int fparentkey, int flayerid, int fcnt)
		{
		}

		// Token: 0x060032D2 RID: 13010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E71")]
		[Address(RVA = "0x10139A9A4", Offset = "0x139A9A4", VA = "0x10139A9A4", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x060032D3 RID: 13011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E72")]
		[Address(RVA = "0x10139AAD4", Offset = "0x139AAD4", VA = "0x10139AAD4", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060032D4 RID: 13012 RVA: 0x00015918 File Offset: 0x00013B18
		[Token(Token = "0x6002E73")]
		[Address(RVA = "0x10139AB98", Offset = "0x139AB98", VA = "0x10139AB98", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x060032D5 RID: 13013 RVA: 0x00015930 File Offset: 0x00013B30
		[Token(Token = "0x6002E74")]
		[Address(RVA = "0x10139AFC0", Offset = "0x139AFC0", VA = "0x10139AFC0", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x060032D6 RID: 13014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E75")]
		[Address(RVA = "0x10139B0BC", Offset = "0x139B0BC", VA = "0x10139B0BC")]
		public void CallbackAreaBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
		}

		// Token: 0x060032D7 RID: 13015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E76")]
		[Address(RVA = "0x10139B22C", Offset = "0x139B22C", VA = "0x10139B22C")]
		private void ChangeColliderActive(bool factive)
		{
		}

		// Token: 0x060032D8 RID: 13016 RVA: 0x00015948 File Offset: 0x00013B48
		[Token(Token = "0x6002E77")]
		[Address(RVA = "0x10139B304", Offset = "0x139B304", VA = "0x10139B304")]
		private bool CallAreaCreate(int fkeyid)
		{
			return default(bool);
		}

		// Token: 0x060032D9 RID: 13017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E78")]
		[Address(RVA = "0x10139B770", Offset = "0x139B770", VA = "0x10139B770")]
		public TownObjHandleArea()
		{
		}

		// Token: 0x040042C9 RID: 17097
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002F7C")]
		private bool m_isUnderConstruction;

		// Token: 0x040042CA RID: 17098
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002F7D")]
		private TownObjHandleArea.CAreaPointSetUp m_BuildUpList;

		// Token: 0x040042CB RID: 17099
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002F7E")]
		public int m_BuildStep;

		// Token: 0x040042CC RID: 17100
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002F7F")]
		public int m_ContentAccessKey;

		// Token: 0x040042CD RID: 17101
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002F80")]
		public Transform m_MarkerNode;

		// Token: 0x040042CE RID: 17102
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002F81")]
		private int m_SubTreeNum;

		// Token: 0x040042CF RID: 17103
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002F82")]
		private TownObjHandleArea.TSubTreeState[] m_SubTreeTable;

		// Token: 0x040042D0 RID: 17104
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002F83")]
		public TownObjHandleArea.eState m_State;

		// Token: 0x02000B5C RID: 2908
		[Token(Token = "0x2001033")]
		public class CAreaBuildUpKey
		{
			// Token: 0x060032DA RID: 13018 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600611A")]
			[Address(RVA = "0x10139A864", Offset = "0x139A864", VA = "0x10139A864")]
			public CAreaBuildUpKey(GameObject pobj, TownBuildPakage pakage, int fkey)
			{
			}

			// Token: 0x040042D1 RID: 17105
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066B9")]
			public GameObject m_Object;

			// Token: 0x040042D2 RID: 17106
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066BA")]
			public TownBuildPakage m_Pakage;

			// Token: 0x040042D3 RID: 17107
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066BB")]
			public int m_AccessKey;
		}

		// Token: 0x02000B5D RID: 2909
		[Token(Token = "0x2001034")]
		public class CAreaPointSetUp
		{
			// Token: 0x060032DB RID: 13019 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600611B")]
			[Address(RVA = "0x101399CE0", Offset = "0x1399CE0", VA = "0x101399CE0")]
			public CAreaPointSetUp()
			{
			}

			// Token: 0x060032DC RID: 13020 RVA: 0x00015960 File Offset: 0x00013B60
			[Token(Token = "0x600611C")]
			[Address(RVA = "0x10139A6AC", Offset = "0x139A6AC", VA = "0x10139A6AC")]
			public int GetBufNum()
			{
				return 0;
			}

			// Token: 0x060032DD RID: 13021 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600611D")]
			[Address(RVA = "0x10139A8A4", Offset = "0x139A8A4", VA = "0x10139A8A4")]
			public void AddBufPoint(TownObjHandleArea.CAreaBuildUpKey ppoint)
			{
			}

			// Token: 0x060032DE RID: 13022 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600611E")]
			[Address(RVA = "0x10139A6BC", Offset = "0x139A6BC", VA = "0x10139A6BC")]
			public TownObjHandleArea.CAreaBuildUpKey GetBufPoint(int findex)
			{
				return null;
			}

			// Token: 0x060032DF RID: 13023 RVA: 0x00015978 File Offset: 0x00013B78
			[Token(Token = "0x600611F")]
			[Address(RVA = "0x10139A6B4", Offset = "0x139A6B4", VA = "0x10139A6B4")]
			public int GetContentNum()
			{
				return 0;
			}

			// Token: 0x060032E0 RID: 13024 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006120")]
			[Address(RVA = "0x10139A924", Offset = "0x139A924", VA = "0x10139A924")]
			public void AddContentPoint(TownObjHandleArea.CAreaBuildUpKey ppoint)
			{
			}

			// Token: 0x060032E1 RID: 13025 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006121")]
			[Address(RVA = "0x10139A76C", Offset = "0x139A76C", VA = "0x10139A76C")]
			public TownObjHandleArea.CAreaBuildUpKey GetContentPoint(int findex)
			{
				return null;
			}

			// Token: 0x040042D4 RID: 17108
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066BC")]
			public List<TownObjHandleArea.CAreaBuildUpKey> m_BufNode;

			// Token: 0x040042D5 RID: 17109
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066BD")]
			public int m_BufNum;

			// Token: 0x040042D6 RID: 17110
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066BE")]
			public List<TownObjHandleArea.CAreaBuildUpKey> m_ContentNode;

			// Token: 0x040042D7 RID: 17111
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40066BF")]
			public int m_ContentNum;

			// Token: 0x040042D8 RID: 17112
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40066C0")]
			public List<TownObjHandleArea.CAreaBuildUpKey> m_LocNode;

			// Token: 0x040042D9 RID: 17113
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40066C1")]
			public int m_LocNum;
		}

		// Token: 0x02000B5E RID: 2910
		[Token(Token = "0x2001035")]
		private struct TSubTreeState
		{
			// Token: 0x040042DA RID: 17114
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40066C2")]
			public int m_BuildPoint;
		}

		// Token: 0x02000B5F RID: 2911
		[Token(Token = "0x2001036")]
		public enum eState
		{
			// Token: 0x040042DC RID: 17116
			[Token(Token = "0x40066C4")]
			Init,
			// Token: 0x040042DD RID: 17117
			[Token(Token = "0x40066C5")]
			Main,
			// Token: 0x040042DE RID: 17118
			[Token(Token = "0x40066C6")]
			Sleep,
			// Token: 0x040042DF RID: 17119
			[Token(Token = "0x40066C7")]
			EndStart,
			// Token: 0x040042E0 RID: 17120
			[Token(Token = "0x40066C8")]
			End
		}
	}
}
