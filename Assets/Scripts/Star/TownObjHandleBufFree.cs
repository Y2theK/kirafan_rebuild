﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B60 RID: 2912
	[Token(Token = "0x20007F6")]
	[StructLayout(3)]
	public class TownObjHandleBufFree : ITownObjectHandler
	{
		// Token: 0x060032E2 RID: 13026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E79")]
		[Address(RVA = "0x10139A72C", Offset = "0x139A72C", VA = "0x10139A72C")]
		public void SetupLink(int buildPointIndex, GameObject phitnode)
		{
		}

		// Token: 0x060032E3 RID: 13027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E7A")]
		[Address(RVA = "0x10139B778", Offset = "0x139B778", VA = "0x10139B778")]
		private void Update()
		{
		}

		// Token: 0x060032E4 RID: 13028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E7B")]
		[Address(RVA = "0x10139B8C8", Offset = "0x139B8C8", VA = "0x10139B8C8", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x060032E5 RID: 13029 RVA: 0x00015990 File Offset: 0x00013B90
		[Token(Token = "0x6002E7C")]
		[Address(RVA = "0x10139B904", Offset = "0x139B904", VA = "0x10139B904", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x060032E6 RID: 13030 RVA: 0x000159A8 File Offset: 0x00013BA8
		[Token(Token = "0x6002E7D")]
		[Address(RVA = "0x10139BBBC", Offset = "0x139BBBC", VA = "0x10139BBBC", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x060032E7 RID: 13031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E7E")]
		[Address(RVA = "0x10139BCF4", Offset = "0x139BCF4", VA = "0x10139BCF4")]
		public TownObjHandleBufFree()
		{
		}

		// Token: 0x040042E1 RID: 17121
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002F84")]
		private GameObject m_HitNode;

		// Token: 0x040042E2 RID: 17122
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002F85")]
		public TownObjHandleBufFree.eState m_State;

		// Token: 0x02000B61 RID: 2913
		[Token(Token = "0x2001037")]
		public enum eState
		{
			// Token: 0x040042E4 RID: 17124
			[Token(Token = "0x40066CA")]
			Init,
			// Token: 0x040042E5 RID: 17125
			[Token(Token = "0x40066CB")]
			Main,
			// Token: 0x040042E6 RID: 17126
			[Token(Token = "0x40066CC")]
			Sleep,
			// Token: 0x040042E7 RID: 17127
			[Token(Token = "0x40066CD")]
			WakeUp,
			// Token: 0x040042E8 RID: 17128
			[Token(Token = "0x40066CE")]
			EndStart,
			// Token: 0x040042E9 RID: 17129
			[Token(Token = "0x40066CF")]
			End
		}
	}
}
