﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B62 RID: 2914
	[Token(Token = "0x20007F7")]
	[StructLayout(3)]
	public class TownObjHandleBuild : TownObjModelHandle
	{
		// Token: 0x060032E8 RID: 13032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E7F")]
		[Address(RVA = "0x10139BCFC", Offset = "0x139BCFC", VA = "0x10139BCFC", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x060032E9 RID: 13033 RVA: 0x000159C0 File Offset: 0x00013BC0
		[Token(Token = "0x6002E80")]
		[Address(RVA = "0x10139BF88", Offset = "0x139BF88", VA = "0x10139BF88", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x060032EA RID: 13034 RVA: 0x000159D8 File Offset: 0x00013BD8
		[Token(Token = "0x6002E81")]
		[Address(RVA = "0x10139C3D0", Offset = "0x139C3D0", VA = "0x10139C3D0")]
		public bool IsCompleteBuildEvt()
		{
			return default(bool);
		}

		// Token: 0x060032EB RID: 13035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E82")]
		[Address(RVA = "0x10139C414", Offset = "0x139C414", VA = "0x10139C414")]
		private void Update()
		{
		}

		// Token: 0x060032EC RID: 13036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E83")]
		[Address(RVA = "0x10139C7D8", Offset = "0x139C7D8", VA = "0x10139C7D8", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x060032ED RID: 13037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E84")]
		[Address(RVA = "0x10139CB08", Offset = "0x139CB08", VA = "0x10139CB08", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060032EE RID: 13038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E85")]
		[Address(RVA = "0x10139CC2C", Offset = "0x139CC2C", VA = "0x10139CC2C")]
		private void ReSetUpHandle(int objID, long fmanageid)
		{
		}

		// Token: 0x060032EF RID: 13039 RVA: 0x000159F0 File Offset: 0x00013BF0
		[Token(Token = "0x6002E86")]
		[Address(RVA = "0x10139CE00", Offset = "0x139CE00", VA = "0x10139CE00", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x060032F0 RID: 13040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E87")]
		[Address(RVA = "0x10139D84C", Offset = "0x139D84C", VA = "0x10139D84C")]
		public void SetCompleteGauge()
		{
		}

		// Token: 0x060032F1 RID: 13041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E88")]
		[Address(RVA = "0x10139DB6C", Offset = "0x139DB6C", VA = "0x10139DB6C")]
		public void CallbackItemUpResult(IFldNetComModule phandle)
		{
		}

		// Token: 0x060032F2 RID: 13042 RVA: 0x00015A08 File Offset: 0x00013C08
		[Token(Token = "0x6002E89")]
		[Address(RVA = "0x10139E320", Offset = "0x139E320", VA = "0x10139E320", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x060032F3 RID: 13043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E8A")]
		[Address(RVA = "0x10139EB90", Offset = "0x139EB90", VA = "0x10139EB90")]
		public void CallbackBufBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
		}

		// Token: 0x060032F4 RID: 13044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E8B")]
		[Address(RVA = "0x10139EF88", Offset = "0x139EF88", VA = "0x10139EF88")]
		private void CheckModelChange()
		{
		}

		// Token: 0x060032F5 RID: 13045 RVA: 0x00015A20 File Offset: 0x00013C20
		[Token(Token = "0x6002E8C")]
		[Address(RVA = "0x10139F178", Offset = "0x139F178", VA = "0x10139F178")]
		private bool CallChangeModel(int fkeyid)
		{
			return default(bool);
		}

		// Token: 0x060032F6 RID: 13046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E8D")]
		[Address(RVA = "0x10139F3E4", Offset = "0x139F3E4", VA = "0x10139F3E4")]
		public TownObjHandleBuild()
		{
		}

		// Token: 0x040042EA RID: 17130
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002F86")]
		private bool m_GaugeUp;

		// Token: 0x040042EB RID: 17131
		[Cpp2IlInjected.FieldOffset(Offset = "0x89")]
		[Token(Token = "0x4002F87")]
		private bool m_InitGaugeUp;

		// Token: 0x040042EC RID: 17132
		[Cpp2IlInjected.FieldOffset(Offset = "0x8A")]
		[Token(Token = "0x4002F88")]
		private bool m_ModelMarkUp;

		// Token: 0x040042ED RID: 17133
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002F89")]
		private float m_MakerPos;

		// Token: 0x040042EE RID: 17134
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002F8A")]
		private float m_BaseSize;

		// Token: 0x040042EF RID: 17135
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4002F8B")]
		private Vector3 m_Offset;

		// Token: 0x040042F0 RID: 17136
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002F8C")]
		private eTownObjectCategory m_Category;

		// Token: 0x040042F1 RID: 17137
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4002F8D")]
		private bool m_ItemUp;

		// Token: 0x040042F2 RID: 17138
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002F8E")]
		private TownPartsPopObject m_ItemPopModel;

		// Token: 0x040042F3 RID: 17139
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002F8F")]
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x040042F4 RID: 17140
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002F90")]
		private DropItemInfo m_ItemUpInfo;

		// Token: 0x040042F5 RID: 17141
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4002F91")]
		private bool m_ReBindPos;

		// Token: 0x040042F6 RID: 17142
		[Cpp2IlInjected.FieldOffset(Offset = "0xD9")]
		[Token(Token = "0x4002F92")]
		private byte m_ItemType;

		// Token: 0x040042F7 RID: 17143
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4002F93")]
		private TownBuildLvInfo m_LvInfo;

		// Token: 0x040042F8 RID: 17144
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4002F94")]
		public TownObjHandleBuild.eState m_State;

		// Token: 0x02000B63 RID: 2915
		[Token(Token = "0x2001038")]
		public enum eState
		{
			// Token: 0x040042FA RID: 17146
			[Token(Token = "0x40066D1")]
			Init,
			// Token: 0x040042FB RID: 17147
			[Token(Token = "0x40066D2")]
			Main,
			// Token: 0x040042FC RID: 17148
			[Token(Token = "0x40066D3")]
			Sleep,
			// Token: 0x040042FD RID: 17149
			[Token(Token = "0x40066D4")]
			EndStart,
			// Token: 0x040042FE RID: 17150
			[Token(Token = "0x40066D5")]
			End
		}
	}
}
