﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B64 RID: 2916
	[Token(Token = "0x20007F8")]
	[StructLayout(3)]
	public class TownObjHandleChara : ITownObjectHandler
	{
		// Token: 0x060032F7 RID: 13047 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E8E")]
		[Address(RVA = "0x10139F3EC", Offset = "0x139F3EC", VA = "0x10139F3EC")]
		public TownCharaModel GetModel()
		{
			return null;
		}

		// Token: 0x060032F8 RID: 13048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E8F")]
		[Address(RVA = "0x10139F3F4", Offset = "0x139F3F4", VA = "0x10139F3F4")]
		public void SetUpChara(long fmngid)
		{
		}

		// Token: 0x060032F9 RID: 13049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E90")]
		[Address(RVA = "0x10139F614", Offset = "0x139F614", VA = "0x10139F614")]
		public void BindBuildPoint(bool frebind = false)
		{
		}

		// Token: 0x060032FA RID: 13050 RVA: 0x00015A38 File Offset: 0x00013C38
		[Token(Token = "0x6002E91")]
		[Address(RVA = "0x10139F81C", Offset = "0x139F81C", VA = "0x10139F81C", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x060032FB RID: 13051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E92")]
		[Address(RVA = "0x10139FA2C", Offset = "0x139FA2C", VA = "0x10139FA2C", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x060032FC RID: 13052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E93")]
		[Address(RVA = "0x10139FAD4", Offset = "0x139FAD4", VA = "0x10139FAD4", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060032FD RID: 13053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E94")]
		[Address(RVA = "0x10139FB88", Offset = "0x139FB88", VA = "0x10139FB88")]
		private void Update()
		{
		}

		// Token: 0x060032FE RID: 13054 RVA: 0x00015A50 File Offset: 0x00013C50
		[Token(Token = "0x6002E95")]
		[Address(RVA = "0x10139FDD4", Offset = "0x139FDD4", VA = "0x10139FDD4", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x060032FF RID: 13055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E96")]
		[Address(RVA = "0x1013A0380", Offset = "0x13A0380", VA = "0x1013A0380", Slot = "10")]
		public override void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06003300 RID: 13056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E97")]
		[Address(RVA = "0x10139FFA4", Offset = "0x139FFA4", VA = "0x10139FFA4")]
		private void MovePosition(Vector3 ftarget, long faccessid, int flayerid)
		{
		}

		// Token: 0x06003301 RID: 13057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E98")]
		[Address(RVA = "0x1013A02B0", Offset = "0x13A02B0", VA = "0x1013A02B0")]
		private void BindFade(Transform parent, int flayerid)
		{
		}

		// Token: 0x06003302 RID: 13058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E99")]
		[Address(RVA = "0x1013A0428", Offset = "0x13A0428", VA = "0x1013A0428")]
		public void FeedBackCharaCallBack(eCallBackType ftype, FieldObjHandleChara pbase)
		{
		}

		// Token: 0x06003303 RID: 13059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E9A")]
		[Address(RVA = "0x1013A0568", Offset = "0x13A0568", VA = "0x1013A0568")]
		private void CallbackTouchItemEvt(IFldNetComModule phandle)
		{
		}

		// Token: 0x06003304 RID: 13060 RVA: 0x00015A68 File Offset: 0x00013C68
		[Token(Token = "0x6002E9B")]
		[Address(RVA = "0x1013A0D5C", Offset = "0x13A0D5C", VA = "0x1013A0D5C", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003305 RID: 13061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E9C")]
		[Address(RVA = "0x10139FD9C", Offset = "0x139FD9C", VA = "0x10139FD9C")]
		public void SetCharaHaveItem(int flevel)
		{
		}

		// Token: 0x06003306 RID: 13062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E9D")]
		[Address(RVA = "0x10139F9F4", Offset = "0x139F9F4", VA = "0x10139F9F4")]
		public void ClrCharaHaveItem(int flevel)
		{
		}

		// Token: 0x06003307 RID: 13063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E9E")]
		[Address(RVA = "0x1013A1084", Offset = "0x13A1084", VA = "0x1013A1084")]
		public TownObjHandleChara()
		{
		}

		// Token: 0x040042FF RID: 17151
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002F95")]
		public int m_CharaID;

		// Token: 0x04004300 RID: 17152
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002F96")]
		private TownCharaModel m_CharaIcons;

		// Token: 0x04004301 RID: 17153
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002F97")]
		private GameObject m_RenderObject;

		// Token: 0x04004302 RID: 17154
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002F98")]
		private long m_LinkAccessMngID;

		// Token: 0x04004303 RID: 17155
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002F99")]
		private int m_BuildUpStep;

		// Token: 0x04004304 RID: 17156
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4002F9A")]
		private int m_PopUpMessageID;

		// Token: 0x04004305 RID: 17157
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002F9B")]
		private long m_BindManageID;

		// Token: 0x04004306 RID: 17158
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002F9C")]
		private bool m_ScheduleItemNew;

		// Token: 0x04004307 RID: 17159
		[Cpp2IlInjected.FieldOffset(Offset = "0x91")]
		[Token(Token = "0x4002F9D")]
		private bool m_RebindWait;

		// Token: 0x04004308 RID: 17160
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4002F9E")]
		private TownObjHandleChara.eState m_State;

		// Token: 0x02000B65 RID: 2917
		[Token(Token = "0x2001039")]
		private enum eState
		{
			// Token: 0x0400430A RID: 17162
			[Token(Token = "0x40066D7")]
			Init,
			// Token: 0x0400430B RID: 17163
			[Token(Token = "0x40066D8")]
			Main,
			// Token: 0x0400430C RID: 17164
			[Token(Token = "0x40066D9")]
			Sleep,
			// Token: 0x0400430D RID: 17165
			[Token(Token = "0x40066DA")]
			EndStart,
			// Token: 0x0400430E RID: 17166
			[Token(Token = "0x40066DB")]
			End
		}

		// Token: 0x02000B66 RID: 2918
		[Token(Token = "0x200103A")]
		private enum eCharaUI
		{
			// Token: 0x04004310 RID: 17168
			[Token(Token = "0x40066DD")]
			Non,
			// Token: 0x04004311 RID: 17169
			[Token(Token = "0x40066DE")]
			PopUp,
			// Token: 0x04004312 RID: 17170
			[Token(Token = "0x40066DF")]
			Traning,
			// Token: 0x04004313 RID: 17171
			[Token(Token = "0x40066E0")]
			Item,
			// Token: 0x04004314 RID: 17172
			[Token(Token = "0x40066E1")]
			Money
		}
	}
}
