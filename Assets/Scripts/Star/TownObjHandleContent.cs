﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B67 RID: 2919
	[Token(Token = "0x20007F9")]
	[StructLayout(3)]
	public class TownObjHandleContent : TownObjModelHandle
	{
		// Token: 0x06003308 RID: 13064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E9F")]
		[Address(RVA = "0x1013A108C", Offset = "0x13A108C", VA = "0x1013A108C", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x06003309 RID: 13065 RVA: 0x00015A80 File Offset: 0x00013C80
		[Token(Token = "0x6002EA0")]
		[Address(RVA = "0x1013A128C", Offset = "0x13A128C", VA = "0x1013A128C", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x0600330A RID: 13066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA1")]
		[Address(RVA = "0x1013A15C8", Offset = "0x13A15C8", VA = "0x1013A15C8")]
		private void Update()
		{
		}

		// Token: 0x0600330B RID: 13067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA2")]
		[Address(RVA = "0x1013A16EC", Offset = "0x13A16EC", VA = "0x1013A16EC", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x0600330C RID: 13068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA3")]
		[Address(RVA = "0x1013A17EC", Offset = "0x13A17EC", VA = "0x1013A17EC", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600330D RID: 13069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA4")]
		[Address(RVA = "0x1013A1910", Offset = "0x13A1910", VA = "0x1013A1910")]
		private void ReSetUpHandle(int objID, long fmanageid)
		{
		}

		// Token: 0x0600330E RID: 13070 RVA: 0x00015A98 File Offset: 0x00013C98
		[Token(Token = "0x6002EA5")]
		[Address(RVA = "0x1013A1AF4", Offset = "0x13A1AF4", VA = "0x1013A1AF4", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x0600330F RID: 13071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA6")]
		[Address(RVA = "0x1013A25C0", Offset = "0x13A25C0", VA = "0x1013A25C0")]
		public void CallbackContentsBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
		}

		// Token: 0x06003310 RID: 13072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA7")]
		[Address(RVA = "0x1013A27E4", Offset = "0x13A27E4", VA = "0x1013A27E4")]
		public void CallbackItemUpResult(IFldNetComModule phandle)
		{
		}

		// Token: 0x06003311 RID: 13073 RVA: 0x00015AB0 File Offset: 0x00013CB0
		[Token(Token = "0x6002EA8")]
		[Address(RVA = "0x1013A27E8", Offset = "0x13A27E8", VA = "0x1013A27E8", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003312 RID: 13074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EA9")]
		[Address(RVA = "0x1013A2C04", Offset = "0x13A2C04", VA = "0x1013A2C04")]
		public TownObjHandleContent()
		{
		}

		// Token: 0x04004315 RID: 17173
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002F9F")]
		private float m_MakerPos;

		// Token: 0x04004316 RID: 17174
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002FA0")]
		private float m_BaseSize;

		// Token: 0x04004317 RID: 17175
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002FA1")]
		private Vector3 m_Offset;

		// Token: 0x04004318 RID: 17176
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4002FA2")]
		private bool m_ItemUp;

		// Token: 0x04004319 RID: 17177
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002FA3")]
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x0400431A RID: 17178
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002FA4")]
		private bool m_ReBindPos;

		// Token: 0x0400431B RID: 17179
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4002FA5")]
		private int m_SyncKey;

		// Token: 0x0400431C RID: 17180
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002FA6")]
		public TownObjHandleContent.eState m_State;

		// Token: 0x0400431D RID: 17181
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002FA7")]
		private TownBuildLvInfo m_LvInfo;

		// Token: 0x02000B68 RID: 2920
		[Token(Token = "0x200103B")]
		public enum eState
		{
			// Token: 0x0400431F RID: 17183
			[Token(Token = "0x40066E3")]
			Init,
			// Token: 0x04004320 RID: 17184
			[Token(Token = "0x40066E4")]
			Main,
			// Token: 0x04004321 RID: 17185
			[Token(Token = "0x40066E5")]
			Sleep,
			// Token: 0x04004322 RID: 17186
			[Token(Token = "0x40066E6")]
			EndStart,
			// Token: 0x04004323 RID: 17187
			[Token(Token = "0x40066E7")]
			End
		}
	}
}
