﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B69 RID: 2921
	[Token(Token = "0x20007FA")]
	[StructLayout(3)]
	public class TownObjHandleField : TownObjModelHandle
	{
		// Token: 0x06003313 RID: 13075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EAA")]
		[Address(RVA = "0x1013A2C0C", Offset = "0x13A2C0C", VA = "0x1013A2C0C")]
		private void Update()
		{
		}

		// Token: 0x06003314 RID: 13076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EAB")]
		[Address(RVA = "0x1013A2C24", Offset = "0x13A2C24", VA = "0x1013A2C24", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06003315 RID: 13077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EAC")]
		[Address(RVA = "0x1013A2CEC", Offset = "0x13A2CEC", VA = "0x1013A2CEC")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003316 RID: 13078 RVA: 0x00015AC8 File Offset: 0x00013CC8
		[Token(Token = "0x6002EAD")]
		[Address(RVA = "0x1013A2CF4", Offset = "0x13A2CF4", VA = "0x1013A2CF4", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x06003317 RID: 13079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EAE")]
		[Address(RVA = "0x1013A3BE8", Offset = "0x13A3BE8", VA = "0x1013A3BE8")]
		private void CallBackFreeArea(TownBuildPakage pakage)
		{
		}

		// Token: 0x06003318 RID: 13080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EAF")]
		[Address(RVA = "0x1013A3480", Offset = "0x13A3480", VA = "0x1013A3480")]
		public static void MakeBuildPoint(Transform ptrs, TownObjHandleField.CFieldPointSetUp pup, int fcnt)
		{
		}

		// Token: 0x06003319 RID: 13081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB0")]
		[Address(RVA = "0x1013A4428", Offset = "0x13A4428", VA = "0x1013A4428")]
		public void OpenArea()
		{
		}

		// Token: 0x0600331A RID: 13082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB1")]
		[Address(RVA = "0x1013A4510", Offset = "0x13A4510", VA = "0x1013A4510")]
		public TownObjHandleField()
		{
		}

		// Token: 0x04004324 RID: 17188
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002FA8")]
		private TownObjHandleField.CFieldPointSetUp m_BuildKey;

		// Token: 0x02000B6A RID: 2922
		[Token(Token = "0x200103C")]
		public enum eBuildType
		{
			// Token: 0x04004326 RID: 17190
			[Token(Token = "0x40066E9")]
			Area,
			// Token: 0x04004327 RID: 17191
			[Token(Token = "0x40066EA")]
			Menu,
			// Token: 0x04004328 RID: 17192
			[Token(Token = "0x40066EB")]
			Home
		}

		// Token: 0x02000B6B RID: 2923
		[Token(Token = "0x200103D")]
		public class CFieldBuildUpKey
		{
			// Token: 0x0600331B RID: 13083 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006122")]
			[Address(RVA = "0x1013A3CE4", Offset = "0x13A3CE4", VA = "0x1013A3CE4")]
			public CFieldBuildUpKey(GameObject pobj, TownBuildPakage pakage, eTownOption ftype, int fkey)
			{
			}

			// Token: 0x04004329 RID: 17193
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066EC")]
			public GameObject m_Object;

			// Token: 0x0400432A RID: 17194
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066ED")]
			public TownBuildPakage m_Pakage;

			// Token: 0x0400432B RID: 17195
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066EE")]
			public int m_No;

			// Token: 0x0400432C RID: 17196
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40066EF")]
			public eTownOption m_Type;

			// Token: 0x0400432D RID: 17197
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40066F0")]
			public int m_WakeUpKey;
		}

		// Token: 0x02000B6C RID: 2924
		[Token(Token = "0x200103E")]
		public class CFieldBuildHitKey
		{
			// Token: 0x0600331C RID: 13084 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006123")]
			[Address(RVA = "0x1013A49BC", Offset = "0x13A49BC", VA = "0x1013A49BC")]
			public CFieldBuildHitKey(eTownOption ftype, int fkey)
			{
			}

			// Token: 0x0600331D RID: 13085 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006124")]
			[Address(RVA = "0x1013A47A4", Offset = "0x13A47A4", VA = "0x1013A47A4")]
			public CFieldBuildHitKey(int fkey)
			{
			}

			// Token: 0x0400432E RID: 17198
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066F1")]
			public GameObject m_Node;

			// Token: 0x0400432F RID: 17199
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066F2")]
			public GameObject m_Model;

			// Token: 0x04004330 RID: 17200
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066F3")]
			public eTownOption m_Type;

			// Token: 0x04004331 RID: 17201
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40066F4")]
			public int m_AccessKey;
		}

		// Token: 0x02000B6D RID: 2925
		[Token(Token = "0x200103F")]
		public class CFieldLocatorKey
		{
			// Token: 0x0600331E RID: 13086 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006125")]
			[Address(RVA = "0x1013A4A98", Offset = "0x13A4A98", VA = "0x1013A4A98")]
			public CFieldLocatorKey(Transform pobj, eTownOption ftype, int fkey)
			{
			}

			// Token: 0x04004332 RID: 17202
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066F5")]
			public Transform m_Trs;

			// Token: 0x04004333 RID: 17203
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066F6")]
			public eTownOption m_Type;

			// Token: 0x04004334 RID: 17204
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x40066F7")]
			public int m_AccessKey;
		}

		// Token: 0x02000B6E RID: 2926
		[Token(Token = "0x2001040")]
		public class CFieldBuildGroup
		{
			// Token: 0x0600331F RID: 13087 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006126")]
			[Address(RVA = "0x1013A4518", Offset = "0x13A4518", VA = "0x1013A4518")]
			public CFieldBuildGroup(int fgroupno)
			{
			}

			// Token: 0x06003320 RID: 13088 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006127")]
			[Address(RVA = "0x1013A39FC", Offset = "0x13A39FC", VA = "0x1013A39FC")]
			public TownObjHandleField.CFieldBuildUpKey GetRootNode()
			{
				return null;
			}

			// Token: 0x06003321 RID: 13089 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006128")]
			[Address(RVA = "0x1013A45EC", Offset = "0x13A45EC", VA = "0x1013A45EC")]
			public void CheckLinkHitNode(Transform ptrs, int fkey)
			{
			}

			// Token: 0x06003322 RID: 13090 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006129")]
			[Address(RVA = "0x1013A47D0", Offset = "0x13A47D0", VA = "0x1013A47D0")]
			public void CheckLinkHitModel(Transform ptrs, eTownOption ftype, int fkey)
			{
			}

			// Token: 0x06003323 RID: 13091 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600612A")]
			[Address(RVA = "0x1013A49F4", Offset = "0x13A49F4", VA = "0x1013A49F4")]
			public void EntryLocaterNode(Transform ptrs, eTownOption ftype, int fkey)
			{
			}

			// Token: 0x04004335 RID: 17205
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066F8")]
			public int m_GroupNo;

			// Token: 0x04004336 RID: 17206
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066F9")]
			public List<TownObjHandleField.CFieldBuildUpKey> m_Node;

			// Token: 0x04004337 RID: 17207
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066FA")]
			public List<TownObjHandleField.CFieldBuildHitKey> m_Hit;

			// Token: 0x04004338 RID: 17208
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40066FB")]
			public List<TownObjHandleField.CFieldLocatorKey> m_Loc;
		}

		// Token: 0x02000B6F RID: 2927
		[Token(Token = "0x2001041")]
		public class CFieldPointSetUp
		{
			// Token: 0x06003324 RID: 13092 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600612B")]
			[Address(RVA = "0x1013A33BC", Offset = "0x13A33BC", VA = "0x1013A33BC")]
			public CFieldPointSetUp()
			{
			}

			// Token: 0x06003325 RID: 13093 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600612C")]
			[Address(RVA = "0x1013A2C58", Offset = "0x13A2C58", VA = "0x1013A2C58")]
			public void Release()
			{
			}

			// Token: 0x06003326 RID: 13094 RVA: 0x00015AE0 File Offset: 0x00013CE0
			[Token(Token = "0x600612D")]
			[Address(RVA = "0x1013A392C", Offset = "0x13A392C", VA = "0x1013A392C")]
			public int GetBuildNum()
			{
				return 0;
			}

			// Token: 0x06003327 RID: 13095 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600612E")]
			[Address(RVA = "0x1013A3DA0", Offset = "0x13A3DA0", VA = "0x1013A3DA0")]
			public void AddBuildPoint(TownObjHandleField.CFieldBuildUpKey ppoint, int fgroupno)
			{
			}

			// Token: 0x06003328 RID: 13096 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600612F")]
			[Address(RVA = "0x1013A398C", Offset = "0x13A398C", VA = "0x1013A398C")]
			public TownObjHandleField.CFieldBuildGroup GetBuildPoint(int findex)
			{
				return null;
			}

			// Token: 0x06003329 RID: 13097 RVA: 0x00015AF8 File Offset: 0x00013CF8
			[Token(Token = "0x6006130")]
			[Address(RVA = "0x1013A3B18", Offset = "0x13A3B18", VA = "0x1013A3B18")]
			public int GetMenuNum()
			{
				return 0;
			}

			// Token: 0x0600332A RID: 13098 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006131")]
			[Address(RVA = "0x1013A3D30", Offset = "0x13A3D30", VA = "0x1013A3D30")]
			public void AddMenuPoint(TownObjHandleField.CFieldBuildUpKey ppoint)
			{
			}

			// Token: 0x0600332B RID: 13099 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006132")]
			[Address(RVA = "0x1013A3B78", Offset = "0x13A3B78", VA = "0x1013A3B78")]
			public TownObjHandleField.CFieldBuildUpKey GetMenuPoint(int findex)
			{
				return null;
			}

			// Token: 0x0600332C RID: 13100 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006133")]
			[Address(RVA = "0x1013A3F48", Offset = "0x13A3F48", VA = "0x1013A3F48")]
			public void AddHitNode(Transform ptrs, int fgroupno, int flinkid)
			{
			}

			// Token: 0x0600332D RID: 13101 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006134")]
			[Address(RVA = "0x1013A40D8", Offset = "0x13A40D8", VA = "0x1013A40D8")]
			public void AddHitModel(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
			}

			// Token: 0x0600332E RID: 13102 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006135")]
			[Address(RVA = "0x1013A4280", Offset = "0x13A4280", VA = "0x1013A4280")]
			public void AddGroupEffectPoint(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
			}

			// Token: 0x0600332F RID: 13103 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006136")]
			[Address(RVA = "0x1013A4AD8", Offset = "0x13A4AD8", VA = "0x1013A4AD8")]
			public void AddEffectPoint(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
			}

			// Token: 0x04004339 RID: 17209
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066FC")]
			public List<TownObjHandleField.CFieldBuildGroup> m_BuildNode;

			// Token: 0x0400433A RID: 17210
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066FD")]
			public List<TownObjHandleField.CFieldBuildUpKey> m_MenuNode;

			// Token: 0x0400433B RID: 17211
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066FE")]
			public List<TownObjHandleField.CFieldLocatorKey> m_LocNode;
		}
	}
}
