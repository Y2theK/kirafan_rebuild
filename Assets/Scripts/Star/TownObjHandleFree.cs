﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B70 RID: 2928
	[Token(Token = "0x20007FB")]
	[StructLayout(3)]
	public class TownObjHandleFree : ITownObjectHandler
	{
		// Token: 0x06003330 RID: 13104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB2")]
		[Address(RVA = "0x1013A4B7C", Offset = "0x13A4B7C", VA = "0x1013A4B7C", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x06003331 RID: 13105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB3")]
		[Address(RVA = "0x1013A4BA4", Offset = "0x13A4BA4", VA = "0x1013A4BA4")]
		private void Update()
		{
		}

		// Token: 0x06003332 RID: 13106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB4")]
		[Address(RVA = "0x1013A4E64", Offset = "0x13A4E64", VA = "0x1013A4E64")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003333 RID: 13107 RVA: 0x00015B10 File Offset: 0x00013D10
		[Token(Token = "0x6002EB5")]
		[Address(RVA = "0x1013A4E6C", Offset = "0x13A4E6C", VA = "0x1013A4E6C", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x06003334 RID: 13108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB6")]
		[Address(RVA = "0x1013A4D78", Offset = "0x13A4D78", VA = "0x1013A4D78")]
		private void ChangeColliderActive(bool factive)
		{
		}

		// Token: 0x06003335 RID: 13109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB7")]
		[Address(RVA = "0x1013A3B10", Offset = "0x13A3B10", VA = "0x1013A3B10")]
		public void SetBuildUpCallBack(TownObjHandleFree.CallbackBuildUp pcallback, TownBuildPakage ppakage)
		{
		}

		// Token: 0x06003336 RID: 13110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EB8")]
		[Address(RVA = "0x1013A4C34", Offset = "0x13A4C34", VA = "0x1013A4C34")]
		private void ModelSetUp()
		{
		}

		// Token: 0x06003337 RID: 13111 RVA: 0x00015B28 File Offset: 0x00013D28
		[Token(Token = "0x6002EB9")]
		[Address(RVA = "0x1013A5618", Offset = "0x13A5618", VA = "0x1013A5618", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003338 RID: 13112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EBA")]
		[Address(RVA = "0x1013A5748", Offset = "0x13A5748", VA = "0x1013A5748")]
		public TownObjHandleFree()
		{
		}

		// Token: 0x0400433C RID: 17212
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002FA9")]
		public TownObjHandleFree.eState m_State;

		// Token: 0x0400433D RID: 17213
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002FAA")]
		public TownObjHandleFree.CallbackBuildUp m_CallbackUp;

		// Token: 0x0400433E RID: 17214
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002FAB")]
		public TownBuildPakage m_CallBackLine;

		// Token: 0x0400433F RID: 17215
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002FAC")]
		public GameObject m_HitNode;

		// Token: 0x04004340 RID: 17216
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002FAD")]
		public GameObject m_HitModel;

		// Token: 0x02000B71 RID: 2929
		[Token(Token = "0x2001042")]
		public enum eState
		{
			// Token: 0x04004342 RID: 17218
			[Token(Token = "0x4006700")]
			Init,
			// Token: 0x04004343 RID: 17219
			[Token(Token = "0x4006701")]
			Main,
			// Token: 0x04004344 RID: 17220
			[Token(Token = "0x4006702")]
			Sleep,
			// Token: 0x04004345 RID: 17221
			[Token(Token = "0x4006703")]
			WakeUp,
			// Token: 0x04004346 RID: 17222
			[Token(Token = "0x4006704")]
			EndStart,
			// Token: 0x04004347 RID: 17223
			[Token(Token = "0x4006705")]
			End
		}

		// Token: 0x02000B72 RID: 2930
		// (Invoke) Token: 0x0600333A RID: 13114
		[Token(Token = "0x2001043")]
		public delegate void CallbackBuildUp(TownBuildPakage ppakage);
	}
}
