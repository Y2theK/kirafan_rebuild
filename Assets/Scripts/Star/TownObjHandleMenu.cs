﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B73 RID: 2931
	[Token(Token = "0x20007FC")]
	[StructLayout(3)]
	public class TownObjHandleMenu : TownObjModelHandle
	{
		// Token: 0x0600333D RID: 13117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EBB")]
		[Address(RVA = "0x1013A57AC", Offset = "0x13A57AC", VA = "0x1013A57AC", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x0600333E RID: 13118 RVA: 0x00015B40 File Offset: 0x00013D40
		[Token(Token = "0x6002EBC")]
		[Address(RVA = "0x1013A5BD8", Offset = "0x13A5BD8", VA = "0x1013A5BD8", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x0600333F RID: 13119 RVA: 0x00015B58 File Offset: 0x00013D58
		[Token(Token = "0x6002EBD")]
		[Address(RVA = "0x1013A613C", Offset = "0x13A613C", VA = "0x1013A613C")]
		public bool IsCompleteBuildEvt()
		{
			return default(bool);
		}

		// Token: 0x06003340 RID: 13120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EBE")]
		[Address(RVA = "0x1013A6144", Offset = "0x13A6144", VA = "0x1013A6144")]
		private void Update()
		{
		}

		// Token: 0x06003341 RID: 13121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EBF")]
		[Address(RVA = "0x1013A6578", Offset = "0x13A6578", VA = "0x1013A6578", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06003342 RID: 13122 RVA: 0x00015B70 File Offset: 0x00013D70
		[Token(Token = "0x6002EC0")]
		[Address(RVA = "0x1013A6580", Offset = "0x13A6580", VA = "0x1013A6580", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x06003343 RID: 13123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC1")]
		[Address(RVA = "0x1013A5FB0", Offset = "0x13A5FB0", VA = "0x1013A5FB0")]
		private void SetModelToNoActive()
		{
		}

		// Token: 0x06003344 RID: 13124 RVA: 0x00015B88 File Offset: 0x00013D88
		[Token(Token = "0x6002EC2")]
		[Address(RVA = "0x1013A6D8C", Offset = "0x13A6D8C", VA = "0x1013A6D8C", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003345 RID: 13125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC3")]
		[Address(RVA = "0x1013A71D0", Offset = "0x13A71D0", VA = "0x1013A71D0")]
		public TownObjHandleMenu()
		{
		}

		// Token: 0x04004348 RID: 17224
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002FAE")]
		public float m_MakerPos;

		// Token: 0x04004349 RID: 17225
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4002FAF")]
		private float m_BaseSize;

		// Token: 0x0400434A RID: 17226
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002FB0")]
		private Vector3 m_Offset;

		// Token: 0x0400434B RID: 17227
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4002FB1")]
		public bool m_EventActive;

		// Token: 0x0400434C RID: 17228
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002FB2")]
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x0400434D RID: 17229
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4002FB3")]
		private bool m_BindPos;

		// Token: 0x0400434E RID: 17230
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4002FB4")]
		private ITownMenuAct m_MenuOptionTask;

		// Token: 0x0400434F RID: 17231
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4002FB5")]
		public TownObjHandleMenu.eState m_State;

		// Token: 0x02000B74 RID: 2932
		[Token(Token = "0x2001044")]
		public enum eState
		{
			// Token: 0x04004351 RID: 17233
			[Token(Token = "0x4006707")]
			Init,
			// Token: 0x04004352 RID: 17234
			[Token(Token = "0x4006708")]
			Main,
			// Token: 0x04004353 RID: 17235
			[Token(Token = "0x4006709")]
			Sleep,
			// Token: 0x04004354 RID: 17236
			[Token(Token = "0x400670A")]
			EndStart,
			// Token: 0x04004355 RID: 17237
			[Token(Token = "0x400670B")]
			End
		}
	}
}
