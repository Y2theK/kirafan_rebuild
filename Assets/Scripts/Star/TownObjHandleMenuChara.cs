﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B75 RID: 2933
	[Token(Token = "0x20007FD")]
	[StructLayout(3)]
	public class TownObjHandleMenuChara : ITownObjectHandler
	{
		// Token: 0x06003346 RID: 13126 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002EC4")]
		[Address(RVA = "0x1013A71E0", Offset = "0x13A71E0", VA = "0x1013A71E0")]
		public TownCharaModel GetModel()
		{
			return null;
		}

		// Token: 0x06003347 RID: 13127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC5")]
		[Address(RVA = "0x1013A71E8", Offset = "0x13A71E8", VA = "0x1013A71E8")]
		public void SetUpChara(long fmngid, float fsize, TownObjHandleMenuChara.eMenuType ftype)
		{
		}

		// Token: 0x06003348 RID: 13128 RVA: 0x00015BA0 File Offset: 0x00013DA0
		[Token(Token = "0x6002EC6")]
		[Address(RVA = "0x1013A739C", Offset = "0x13A739C", VA = "0x1013A739C", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x06003349 RID: 13129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC7")]
		[Address(RVA = "0x1013A73CC", Offset = "0x13A73CC", VA = "0x1013A73CC", Slot = "11")]
		public override void DestroyRequest()
		{
		}

		// Token: 0x0600334A RID: 13130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC8")]
		[Address(RVA = "0x1013A7474", Offset = "0x13A7474", VA = "0x1013A7474", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600334B RID: 13131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EC9")]
		[Address(RVA = "0x1013A7524", Offset = "0x13A7524", VA = "0x1013A7524")]
		private void Update()
		{
		}

		// Token: 0x0600334C RID: 13132 RVA: 0x00015BB8 File Offset: 0x00013DB8
		[Token(Token = "0x6002ECA")]
		[Address(RVA = "0x1013A7654", Offset = "0x13A7654", VA = "0x1013A7654", Slot = "12")]
		public override bool RecvEvent(ITownHandleAction pact)
		{
			return default(bool);
		}

		// Token: 0x0600334D RID: 13133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ECB")]
		[Address(RVA = "0x1013A7770", Offset = "0x13A7770", VA = "0x1013A7770")]
		private void MovePosition(Vector3 ftarget, int flayerid)
		{
		}

		// Token: 0x0600334E RID: 13134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ECC")]
		[Address(RVA = "0x1013A7944", Offset = "0x13A7944", VA = "0x1013A7944")]
		private void BindFade(Transform parent, int flayerid)
		{
		}

		// Token: 0x0600334F RID: 13135 RVA: 0x00015BD0 File Offset: 0x00013DD0
		[Token(Token = "0x6002ECD")]
		[Address(RVA = "0x1013A7A30", Offset = "0x13A7A30", VA = "0x1013A7A30", Slot = "13")]
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x06003350 RID: 13136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ECE")]
		[Address(RVA = "0x1013A7B00", Offset = "0x13A7B00", VA = "0x1013A7B00")]
		public TownObjHandleMenuChara()
		{
		}

		// Token: 0x04004356 RID: 17238
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002FB6")]
		private TownCharaModel m_CharaIcons;

		// Token: 0x04004357 RID: 17239
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002FB7")]
		private GameObject m_RenderObject;

		// Token: 0x04004358 RID: 17240
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002FB8")]
		private TownObjHandleMenuChara.eMenuType m_MenuType;

		// Token: 0x04004359 RID: 17241
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4002FB9")]
		private TownObjHandleMenuChara.eState m_State;

		// Token: 0x02000B76 RID: 2934
		[Token(Token = "0x2001045")]
		public enum eMenuType
		{
			// Token: 0x0400435B RID: 17243
			[Token(Token = "0x400670D")]
			Non,
			// Token: 0x0400435C RID: 17244
			[Token(Token = "0x400670E")]
			Traning
		}

		// Token: 0x02000B77 RID: 2935
		[Token(Token = "0x2001046")]
		private enum eState
		{
			// Token: 0x0400435E RID: 17246
			[Token(Token = "0x4006710")]
			Init,
			// Token: 0x0400435F RID: 17247
			[Token(Token = "0x4006711")]
			Main,
			// Token: 0x04004360 RID: 17248
			[Token(Token = "0x4006712")]
			Sleep,
			// Token: 0x04004361 RID: 17249
			[Token(Token = "0x4006713")]
			EndStart,
			// Token: 0x04004362 RID: 17250
			[Token(Token = "0x4006714")]
			End
		}
	}
}
