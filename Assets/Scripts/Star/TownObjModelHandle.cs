﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B78 RID: 2936
	[Token(Token = "0x20007FE")]
	[StructLayout(3)]
	public class TownObjModelHandle : ITownObjectHandler
	{
		// Token: 0x06003351 RID: 13137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ECF")]
		[Address(RVA = "0x1013A5B0C", Offset = "0x13A5B0C", VA = "0x1013A5B0C", Slot = "6")]
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
		}

		// Token: 0x06003352 RID: 13138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED0")]
		[Address(RVA = "0x1013A7B18", Offset = "0x13A7B18", VA = "0x1013A7B18", Slot = "10")]
		public override void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06003353 RID: 13139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED1")]
		[Address(RVA = "0x1013A7BCC", Offset = "0x13A7BCC", VA = "0x1013A7BCC", Slot = "8")]
		public override void Prepare()
		{
		}

		// Token: 0x06003354 RID: 13140 RVA: 0x00015BE8 File Offset: 0x00013DE8
		[Token(Token = "0x6002ED2")]
		[Address(RVA = "0x1013A5EE8", Offset = "0x13A5EE8", VA = "0x1013A5EE8", Slot = "9")]
		protected override bool PrepareMain()
		{
			return default(bool);
		}

		// Token: 0x06003355 RID: 13141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED3")]
		[Address(RVA = "0x1013A6290", Offset = "0x13A6290", VA = "0x1013A6290", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06003356 RID: 13142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED4")]
		[Address(RVA = "0x1013A7E2C", Offset = "0x13A7E2C", VA = "0x1013A7E2C")]
		public void DesModel()
		{
		}

		// Token: 0x06003357 RID: 13143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED5")]
		[Address(RVA = "0x1013A7EFC", Offset = "0x13A7EFC", VA = "0x1013A7EFC")]
		public void AttachModel(GameObject modelObj)
		{
		}

		// Token: 0x06003358 RID: 13144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED6")]
		[Address(RVA = "0x1013A82FC", Offset = "0x13A82FC", VA = "0x1013A82FC", Slot = "14")]
		public virtual void OnBuildUpModel(Transform ptrs)
		{
		}

		// Token: 0x06003359 RID: 13145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED7")]
		[Address(RVA = "0x1013A83A0", Offset = "0x13A83A0", VA = "0x1013A83A0")]
		public void AnimCtrlOpen()
		{
		}

		// Token: 0x0600335A RID: 13146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED8")]
		[Address(RVA = "0x1013A843C", Offset = "0x13A843C", VA = "0x1013A843C")]
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder)
		{
		}

		// Token: 0x0600335B RID: 13147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002ED9")]
		[Address(RVA = "0x1013A851C", Offset = "0x13A851C", VA = "0x1013A851C")]
		public void AnimCtrlClose()
		{
		}

		// Token: 0x0600335C RID: 13148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDA")]
		[Address(RVA = "0x1013A85B8", Offset = "0x13A85B8", VA = "0x1013A85B8")]
		public void ClearAnimCtrl()
		{
		}

		// Token: 0x0600335D RID: 13149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDB")]
		[Address(RVA = "0x1013A7D2C", Offset = "0x13A7D2C", VA = "0x1013A7D2C")]
		public void SetUpAutoPlayAnime()
		{
		}

		// Token: 0x0600335E RID: 13150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDC")]
		[Address(RVA = "0x1013A85C0", Offset = "0x13A85C0", VA = "0x1013A85C0")]
		public void PlayAnim(string actionKey, WrapMode wrapMode)
		{
		}

		// Token: 0x0600335F RID: 13151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDD")]
		[Address(RVA = "0x1013A81EC", Offset = "0x13A81EC", VA = "0x1013A81EC")]
		public void SetSortingOrder(int sortingOrder)
		{
		}

		// Token: 0x06003360 RID: 13152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDE")]
		[Address(RVA = "0x1013A867C", Offset = "0x13A867C", VA = "0x1013A867C")]
		protected void SetRenderFlg(GameObject ptarget, bool flag)
		{
		}

		// Token: 0x06003361 RID: 13153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002EDF")]
		[Address(RVA = "0x1013A71D8", Offset = "0x13A71D8", VA = "0x1013A71D8")]
		public TownObjModelHandle()
		{
		}

		// Token: 0x04004363 RID: 17251
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002FBA")]
		protected TownObjectResource m_TownObjResource;

		// Token: 0x04004364 RID: 17252
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002FBB")]
		protected GameObject m_Obj;

		// Token: 0x04004365 RID: 17253
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002FBC")]
		protected int m_SortingOrder;

		// Token: 0x04004366 RID: 17254
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002FBD")]
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04004367 RID: 17255
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002FBE")]
		protected MsbHandler m_MsbHndl;
	}
}
