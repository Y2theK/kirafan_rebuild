﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F3 RID: 1523
	[Token(Token = "0x20004E6")]
	[StructLayout(3)]
	public class TownObjectBuffDB : ScriptableObject
	{
		// Token: 0x0600160D RID: 5645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BD")]
		[Address(RVA = "0x1013A87E0", Offset = "0x13A87E0", VA = "0x1013A87E0")]
		public TownObjectBuffDB()
		{
		}

		// Token: 0x0400250A RID: 9482
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E74")]
		public TownObjectBuffDB_Param[] m_Params;
	}
}
