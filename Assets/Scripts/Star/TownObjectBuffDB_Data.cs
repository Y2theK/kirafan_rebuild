﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005F1 RID: 1521
	[Token(Token = "0x20004E4")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct TownObjectBuffDB_Data
	{
		// Token: 0x04002504 RID: 9476
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E6E")]
		public int m_TargetConditionType;

		// Token: 0x04002505 RID: 9477
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001E6F")]
		public int m_TargetCondition;

		// Token: 0x04002506 RID: 9478
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E70")]
		public int m_BuffType;

		// Token: 0x04002507 RID: 9479
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001E71")]
		public float[] m_Value;
	}
}
