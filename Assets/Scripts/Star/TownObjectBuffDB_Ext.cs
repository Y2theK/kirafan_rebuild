﻿namespace Star {
    public static class TownObjectBuffDB_Ext {
        public static TownObjectBuffDB_Param GetParam(this TownObjectBuffDB self, int buffListID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == buffListID) {
                    return self.m_Params[i];
                }
            }
            return new TownObjectBuffDB_Param {
                m_Datas = new TownObjectBuffDB_Data[0]
            };
        }

        public static TownObjectBuffDB_Param GetParamByObjID(this TownObjectBuffDB self, int objID) {
            int levelUpListID = GameSystem.Inst.DbMng.TownObjListDB.GetParam(objID).m_LevelUpListID;
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == levelUpListID) {
                    return self.m_Params[i];
                }
            }
            return new TownObjectBuffDB_Param {
                m_Datas = new TownObjectBuffDB_Data[0]
            };
        }
    }
}
