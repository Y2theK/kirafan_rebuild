﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005F2 RID: 1522
	[Token(Token = "0x20004E5")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct TownObjectBuffDB_Param
	{
		// Token: 0x04002508 RID: 9480
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E72")]
		public int m_ID;

		// Token: 0x04002509 RID: 9481
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E73")]
		public TownObjectBuffDB_Data[] m_Datas;
	}
}
