﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F7 RID: 1527
	[Token(Token = "0x20004EA")]
	[StructLayout(3)]
	public class TownObjectBuildSubCodeDB : ScriptableObject
	{
		// Token: 0x0600160E RID: 5646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BE")]
		[Address(RVA = "0x1013A8BA4", Offset = "0x13A8BA4", VA = "0x1013A8BA4")]
		public TownObjectBuildSubCodeDB()
		{
		}

		// Token: 0x04002526 RID: 9510
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E90")]
		public TownObjectBuildSubCodeDB_Param[] m_Params;
	}
}
