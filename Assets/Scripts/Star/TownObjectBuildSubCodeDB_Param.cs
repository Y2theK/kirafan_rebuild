﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005F6 RID: 1526
	[Token(Token = "0x20004E9")]
	[Serializable]
	[StructLayout(0, Size = 20)]
	public struct TownObjectBuildSubCodeDB_Param
	{
		// Token: 0x04002521 RID: 9505
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E8B")]
		public int m_ID;

		// Token: 0x04002522 RID: 9506
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001E8C")]
		public ushort m_Category;

		// Token: 0x04002523 RID: 9507
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E8D")]
		public int m_ActionNo;

		// Token: 0x04002524 RID: 9508
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001E8E")]
		public ushort m_ExtentionScriptID;

		// Token: 0x04002525 RID: 9509
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001E8F")]
		public int m_ExtentionFunKey;
	}
}
