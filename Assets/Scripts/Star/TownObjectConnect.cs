﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B9B RID: 2971
	[Token(Token = "0x2000813")]
	[StructLayout(3)]
	public class TownObjectConnect : MonoBehaviour
	{
		// Token: 0x06003422 RID: 13346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F8B")]
		[Address(RVA = "0x1013A8BAC", Offset = "0x13A8BAC", VA = "0x1013A8BAC")]
		public TownObjectConnect()
		{
		}

		// Token: 0x04004441 RID: 17473
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400305D")]
		public ITownObjectHandler m_Link;
	}
}
