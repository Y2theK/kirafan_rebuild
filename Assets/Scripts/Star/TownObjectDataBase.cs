﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AFB RID: 2811
	[Token(Token = "0x20007AE")]
	[StructLayout(3)]
	public class TownObjectDataBase : MonoBehaviour
	{
		// Token: 0x060031B8 RID: 12728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D72")]
		[Address(RVA = "0x1013A8BB4", Offset = "0x13A8BB4", VA = "0x1013A8BB4")]
		public TownObjectDataBase()
		{
		}

		// Token: 0x0400412A RID: 16682
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E47")]
		public GameObject[] m_List;
	}
}
