﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005FB RID: 1531
	[Token(Token = "0x20004EE")]
	[StructLayout(3)]
	public class TownObjectLevelUpDB : ScriptableObject
	{
		// Token: 0x0600160F RID: 5647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014BF")]
		[Address(RVA = "0x1013A8BBC", Offset = "0x13A8BBC", VA = "0x1013A8BBC")]
		public TownObjectLevelUpDB()
		{
		}

		// Token: 0x04002540 RID: 9536
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001EAA")]
		public TownObjectLevelUpDB_Param[] m_Params;
	}
}
