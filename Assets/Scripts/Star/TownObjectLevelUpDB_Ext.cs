﻿namespace Star {
    public static class TownObjectLevelUpDB_Ext {
        public static TownObjectLevelUpDB_Param GetParam(this TownObjectLevelUpDB self, int levelUpListID, int level) {
            if (level <= 0) {
                level = 1;
            }
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == levelUpListID && self.m_Params[i].m_TargetLv == level) {
                    return self.m_Params[i];
                }
            }
            return default;
        }

        public static TownObjectLevelUpDB_Param GetParamByObjID(this TownObjectLevelUpDB self, int objID, int level) {
            int levelUpListID = GameSystem.Inst.DbMng.TownObjListDB.GetParam(objID).m_LevelUpListID;
            return self.GetParam(levelUpListID, level);
        }
    }
}
