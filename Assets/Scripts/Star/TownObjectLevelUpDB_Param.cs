﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005FA RID: 1530
	[Token(Token = "0x20004ED")]
	[Serializable]
	[StructLayout(0, Size = 80)]
	public struct TownObjectLevelUpDB_Param
	{
		// Token: 0x0400252E RID: 9518
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001E98")]
		public int m_ID;

		// Token: 0x0400252F RID: 9519
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001E99")]
		public int m_TargetLv;

		// Token: 0x04002530 RID: 9520
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001E9A")]
		public int m_GoldAmount;

		// Token: 0x04002531 RID: 9521
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001E9B")]
		public int m_KiraraPoint;

		// Token: 0x04002532 RID: 9522
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001E9C")]
		public int m_UserLv;

		// Token: 0x04002533 RID: 9523
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001E9D")]
		public int m_BuildTime;

		// Token: 0x04002534 RID: 9524
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001E9E")]
		public int m_SaleGoldAmount;

		// Token: 0x04002535 RID: 9525
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001E9F")]
		public int m_SaleKiraraAmount;

		// Token: 0x04002536 RID: 9526
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001EA0")]
		public int m_WakeTime;

		// Token: 0x04002537 RID: 9527
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4001EA1")]
		public int m_LimitNum;

		// Token: 0x04002538 RID: 9528
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001EA2")]
		public float[] m_Parsent;

		// Token: 0x04002539 RID: 9529
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001EA3")]
		public uint[] m_ResultNo;

		// Token: 0x0400253A RID: 9530
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001EA4")]
		public int m_ExtensionFunc;

		// Token: 0x0400253B RID: 9531
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001EA5")]
		public int m_ExtensionKey;

		// Token: 0x0400253C RID: 9532
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001EA6")]
		public int m_TownActionStepKey;

		// Token: 0x0400253D RID: 9533
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001EA7")]
		public int m_TownUseFunc;

		// Token: 0x0400253E RID: 9534
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001EA8")]
		public int m_BuildOptionFunc;

		// Token: 0x0400253F RID: 9535
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4001EA9")]
		public int m_BuildOptionKey;
	}
}
