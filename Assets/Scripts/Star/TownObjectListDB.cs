﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020005FF RID: 1535
	[Token(Token = "0x20004F2")]
	[StructLayout(3)]
	public class TownObjectListDB : ScriptableObject
	{
		// Token: 0x06001610 RID: 5648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C0")]
		[Address(RVA = "0x1013A8E10", Offset = "0x13A8E10", VA = "0x1013A8E10")]
		public TownObjectListDB()
		{
		}

		// Token: 0x04002561 RID: 9569
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001ECB")]
		public TownObjectListDB_Param[] m_Params;
	}
}
