﻿namespace Star {
    public static class TownObjectListDB_Ext {
        public static TownObjectListDB_Param GetParam(this TownObjectListDB self, int townObjectID) {
            for (int i = 0; i < self.m_Params.Length; i++) {
                if (self.m_Params[i].m_ID == townObjectID) {
                    return self.m_Params[i];
                }
            }
            return default;
        }
    }
}
