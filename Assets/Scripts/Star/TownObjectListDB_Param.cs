﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x020005FE RID: 1534
	[Token(Token = "0x20004F1")]
	[Serializable]
	[StructLayout(0, Size = 112)]
	public struct TownObjectListDB_Param
	{
		// Token: 0x04002548 RID: 9544
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001EB2")]
		public int m_ID;

		// Token: 0x04002549 RID: 9545
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001EB3")]
		public int m_sortID;

		// Token: 0x0400254A RID: 9546
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001EB4")]
		public int m_Category;

		// Token: 0x0400254B RID: 9547
		[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
		[Token(Token = "0x4001EB5")]
		public int m_Arg;

		// Token: 0x0400254C RID: 9548
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001EB6")]
		public int m_ResourceID;

		// Token: 0x0400254D RID: 9549
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4001EB7")]
		public int m_TitleType;

		// Token: 0x0400254E RID: 9550
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001EB8")]
		public int m_ElementID;

		// Token: 0x0400254F RID: 9551
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4001EB9")]
		public int m_ClassID;

		// Token: 0x04002550 RID: 9552
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4001EBA")]
		public string m_ObjName;

		// Token: 0x04002551 RID: 9553
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4001EBB")]
		public float m_MarkerPos;

		// Token: 0x04002552 RID: 9554
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4001EBC")]
		public float m_BaseSize;

		// Token: 0x04002553 RID: 9555
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4001EBD")]
		public float m_OffsetX;

		// Token: 0x04002554 RID: 9556
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4001EBE")]
		public float m_OffsetY;

		// Token: 0x04002555 RID: 9557
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4001EBF")]
		public float m_BuildTime;

		// Token: 0x04002556 RID: 9558
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4001EC0")]
		public int m_BuffParamID;

		// Token: 0x04002557 RID: 9559
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4001EC1")]
		public int m_AccessID;

		// Token: 0x04002558 RID: 9560
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4001EC2")]
		public int m_ResultID;

		// Token: 0x04002559 RID: 9561
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4001EC3")]
		public short m_MaxNum;

		// Token: 0x0400255A RID: 9562
		[Cpp2IlInjected.FieldOffset(Offset = "0x4A")]
		[Token(Token = "0x4001EC4")]
		public short m_MaxLevel;

		// Token: 0x0400255B RID: 9563
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4001EC5")]
		public short m_EntryCharaNum;

		// Token: 0x0400255C RID: 9564
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4001EC6")]
		public int m_LevelUpListID;

		// Token: 0x0400255D RID: 9565
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4001EC7")]
		public string m_DetailText;

		// Token: 0x0400255E RID: 9566
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4001EC8")]
		public string m_FlavorText;

		// Token: 0x0400255F RID: 9567
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4001EC9")]
		public int m_ScheduleTagLink;

		// Token: 0x04002560 RID: 9568
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4001ECA")]
		public sbyte m_Tutorial;
	}
}
