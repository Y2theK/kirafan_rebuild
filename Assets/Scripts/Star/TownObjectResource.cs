﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B9C RID: 2972
	[Token(Token = "0x2000814")]
	[StructLayout(3)]
	public class TownObjectResource
	{
		// Token: 0x06003423 RID: 13347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F8C")]
		[Address(RVA = "0x1013A7C28", Offset = "0x13A7C28", VA = "0x1013A7C28")]
		public void Update()
		{
		}

		// Token: 0x06003424 RID: 13348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F8D")]
		[Address(RVA = "0x1013A7B10", Offset = "0x13A7B10", VA = "0x1013A7B10")]
		public void SetupResource(TownObjModelHandle hndl)
		{
		}

		// Token: 0x06003425 RID: 13349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F8E")]
		[Address(RVA = "0x1013A7DFC", Offset = "0x13A7DFC", VA = "0x1013A7DFC")]
		public void Destroy()
		{
		}

		// Token: 0x06003426 RID: 13350 RVA: 0x000160E0 File Offset: 0x000142E0
		[Token(Token = "0x6002F8F")]
		[Address(RVA = "0x1013A7C58", Offset = "0x13A7C58", VA = "0x1013A7C58")]
		public bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06003427 RID: 13351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F90")]
		[Address(RVA = "0x1013A7C1C", Offset = "0x13A7C1C", VA = "0x1013A7C1C")]
		public void Prepare()
		{
		}

		// Token: 0x06003428 RID: 13352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F91")]
		[Address(RVA = "0x1013A8E80", Offset = "0x13A8E80", VA = "0x1013A8E80")]
		private void PrepareModel()
		{
		}

		// Token: 0x06003429 RID: 13353 RVA: 0x000160F8 File Offset: 0x000142F8
		[Token(Token = "0x6002F92")]
		[Address(RVA = "0x1013A8E18", Offset = "0x13A8E18", VA = "0x1013A8E18")]
		private bool PreparingModel()
		{
			return default(bool);
		}

		// Token: 0x0600342A RID: 13354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F93")]
		[Address(RVA = "0x1013A7C60", Offset = "0x13A7C60", VA = "0x1013A7C60")]
		public void SetAttachModel()
		{
		}

		// Token: 0x0600342B RID: 13355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F94")]
		[Address(RVA = "0x1013A9098", Offset = "0x13A9098", VA = "0x1013A9098")]
		private void SetUpBaseAnimation(GameObject pobj)
		{
		}

		// Token: 0x0600342C RID: 13356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F95")]
		[Address(RVA = "0x1013A7B08", Offset = "0x13A7B08", VA = "0x1013A7B08")]
		public TownObjectResource()
		{
		}

		// Token: 0x04004442 RID: 17474
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400305E")]
		private TownObjModelHandle m_Hndl;

		// Token: 0x04004443 RID: 17475
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400305F")]
		private MeigeResource.Handler m_ModelHndl;

		// Token: 0x04004444 RID: 17476
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003060")]
		private bool m_IsPreparing;
	}
}
