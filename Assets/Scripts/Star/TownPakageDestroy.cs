﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BBA RID: 3002
	[Token(Token = "0x2000826")]
	[StructLayout(3)]
	public class TownPakageDestroy : ITownEventCommand
	{
		// Token: 0x06003497 RID: 13463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF6")]
		[Address(RVA = "0x1013A91D0", Offset = "0x13A91D0", VA = "0x1013A91D0")]
		public TownPakageDestroy(TownBuildLinkPoint pnode)
		{
		}

		// Token: 0x06003498 RID: 13464 RVA: 0x00016428 File Offset: 0x00014628
		[Token(Token = "0x6002FF7")]
		[Address(RVA = "0x1013A9204", Offset = "0x13A9204", VA = "0x1013A9204", Slot = "4")]
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			return default(bool);
		}

		// Token: 0x040044B5 RID: 17589
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030AA")]
		private TownBuildLinkPoint m_Node;
	}
}
