﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B3E RID: 2878
	[Token(Token = "0x20007E3")]
	[StructLayout(3)]
	public class TownPartsActionPakage
	{
		// Token: 0x06003276 RID: 12918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E1F")]
		[Address(RVA = "0x1013A92D4", Offset = "0x13A92D4", VA = "0x1013A92D4")]
		public TownPartsActionPakage()
		{
		}

		// Token: 0x06003277 RID: 12919 RVA: 0x00015738 File Offset: 0x00013938
		[Token(Token = "0x6002E20")]
		[Address(RVA = "0x1013A6280", Offset = "0x13A6280", VA = "0x1013A6280")]
		public bool IsEmpty()
		{
			return default(bool);
		}

		// Token: 0x06003278 RID: 12920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E21")]
		[Address(RVA = "0x1013A6B44", Offset = "0x13A6B44", VA = "0x1013A6B44")]
		public void EntryAction(ITownPartsAction paction, int fuid = 0)
		{
		}

		// Token: 0x06003279 RID: 12921 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E22")]
		[Address(RVA = "0x1013A9338", Offset = "0x13A9338", VA = "0x1013A9338")]
		public ITownPartsAction GetAction(int fuid)
		{
			return null;
		}

		// Token: 0x0600327A RID: 12922 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E23")]
		[Address(RVA = "0x1013A6964", Offset = "0x13A6964", VA = "0x1013A6964")]
		public ITownPartsAction GetAction(Type ftype)
		{
			return null;
		}

		// Token: 0x0600327B RID: 12923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E24")]
		[Address(RVA = "0x1013A6348", Offset = "0x13A6348", VA = "0x1013A6348")]
		public void PakageUpdate()
		{
		}

		// Token: 0x0600327C RID: 12924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E25")]
		[Address(RVA = "0x1013A9410", Offset = "0x13A9410", VA = "0x1013A9410")]
		public void Release()
		{
		}

		// Token: 0x04004223 RID: 16931
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002F01")]
		public int m_Num;

		// Token: 0x04004224 RID: 16932
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F02")]
		public ITownPartsAction[] m_Table;
	}
}
