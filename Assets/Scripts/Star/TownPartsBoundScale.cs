﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B3F RID: 2879
	[Token(Token = "0x20007E4")]
	[StructLayout(3)]
	public class TownPartsBoundScale : ITownPartsAction
	{
		// Token: 0x0600327D RID: 12925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E26")]
		[Address(RVA = "0x1013A94FC", Offset = "0x13A94FC", VA = "0x1013A94FC")]
		public TownPartsBoundScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
		}

		// Token: 0x0600327E RID: 12926 RVA: 0x00015750 File Offset: 0x00013950
		[Token(Token = "0x6002E27")]
		[Address(RVA = "0x1013A958C", Offset = "0x13A958C", VA = "0x1013A958C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004225 RID: 16933
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F03")]
		private Transform m_Marker;

		// Token: 0x04004226 RID: 16934
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F04")]
		private Vector3 m_Target;

		// Token: 0x04004227 RID: 16935
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F05")]
		private Vector3 m_Base;

		// Token: 0x04004228 RID: 16936
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F06")]
		private float m_Time;

		// Token: 0x04004229 RID: 16937
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002F07")]
		private float m_MaxTime;
	}
}
