﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B40 RID: 2880
	[Token(Token = "0x20007E5")]
	[StructLayout(3)]
	public class TownPartsBuildAnime : MonoBehaviour
	{
		// Token: 0x0600327F RID: 12927 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002E28")]
		[Address(RVA = "0x1013A974C", Offset = "0x13A974C", VA = "0x1013A974C")]
		public static TownPartsBuildAnime CreateBuildAnime(TownBuilder pbuilder, GameObject ptarget, eTownObjectType fgroupno)
		{
			return null;
		}

		// Token: 0x06003280 RID: 12928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E29")]
		[Address(RVA = "0x1013A9A5C", Offset = "0x13A9A5C", VA = "0x1013A9A5C")]
		public void PlayMotion(TownPartsBuildAnime.eType fmode)
		{
		}

		// Token: 0x06003281 RID: 12929 RVA: 0x00015768 File Offset: 0x00013968
		[Token(Token = "0x6002E2A")]
		[Address(RVA = "0x1013A9B14", Offset = "0x13A9B14", VA = "0x1013A9B14")]
		public bool IsPlaying(TownPartsBuildAnime.eType fmode)
		{
			return default(bool);
		}

		// Token: 0x06003282 RID: 12930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E2B")]
		[Address(RVA = "0x1013A9BCC", Offset = "0x13A9BCC", VA = "0x1013A9BCC")]
		public void DestroyObj(TownBuilder pbuilder)
		{
		}

		// Token: 0x06003283 RID: 12931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E2C")]
		[Address(RVA = "0x1013A9C18", Offset = "0x13A9C18", VA = "0x1013A9C18")]
		public TownPartsBuildAnime()
		{
		}

		// Token: 0x0400422A RID: 16938
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F08")]
		private Animation m_PlayAnime;

		// Token: 0x0400422B RID: 16939
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F09")]
		private GameObject m_BaseNode;

		// Token: 0x0400422C RID: 16940
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F0A")]
		public TownModelIO m_ModelIO;

		// Token: 0x02000B41 RID: 2881
		[Token(Token = "0x2001028")]
		public enum eType
		{
			// Token: 0x0400422E RID: 16942
			[Token(Token = "0x400668F")]
			FadeIn,
			// Token: 0x0400422F RID: 16943
			[Token(Token = "0x4006690")]
			FadeOut
		}
	}
}
