﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B42 RID: 2882
	[Token(Token = "0x20007E6")]
	[StructLayout(3)]
	public class TownPartsBuildGauge : ITownPartsAction
	{
		// Token: 0x06003284 RID: 12932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E2D")]
		[Address(RVA = "0x1013A9C20", Offset = "0x13A9C20", VA = "0x1013A9C20", Slot = "5")]
		public override void Destory()
		{
		}

		// Token: 0x06003285 RID: 12933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E2E")]
		[Address(RVA = "0x1013A9CF8", Offset = "0x13A9CF8", VA = "0x1013A9CF8")]
		public TownPartsBuildGauge(TownBuilder pbuilder, Transform parent, long fmanageid, int flayer, int fdays, bool finit, bool fremake)
		{
		}

		// Token: 0x06003286 RID: 12934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E2F")]
		[Address(RVA = "0x1013AA68C", Offset = "0x13AA68C", VA = "0x1013AA68C")]
		public void SetComplete()
		{
		}

		// Token: 0x06003287 RID: 12935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E30")]
		[Address(RVA = "0x1013AA714", Offset = "0x13AA714", VA = "0x1013AA714")]
		public void SetTerm()
		{
		}

		// Token: 0x06003288 RID: 12936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E31")]
		[Address(RVA = "0x1013AA724", Offset = "0x13AA724", VA = "0x1013AA724")]
		private void GaugeUpCheck()
		{
		}

		// Token: 0x06003289 RID: 12937 RVA: 0x00015780 File Offset: 0x00013980
		[Token(Token = "0x6002E32")]
		[Address(RVA = "0x1013AA8E4", Offset = "0x13AA8E4", VA = "0x1013AA8E4", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004230 RID: 16944
		[Token(Token = "0x4002F0B")]
		private const float MAX_BARSIZE = 61f;

		// Token: 0x04004231 RID: 16945
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F0C")]
		private Transform m_BuildGauge;

		// Token: 0x04004232 RID: 16946
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F0D")]
		private GameObject m_BuildModel;

		// Token: 0x04004233 RID: 16947
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F0E")]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04004234 RID: 16948
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F0F")]
		private MsbHandler m_MsbHandle;

		// Token: 0x04004235 RID: 16949
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F10")]
		private McatFileHelper m_AnimeTable;

		// Token: 0x04004236 RID: 16950
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F11")]
		private TownBuilder m_Builder;

		// Token: 0x04004237 RID: 16951
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002F12")]
		private long m_ManageID;

		// Token: 0x04004238 RID: 16952
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4002F13")]
		private long m_MaxTime;

		// Token: 0x04004239 RID: 16953
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002F14")]
		public float m_Time;

		// Token: 0x0400423A RID: 16954
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4002F15")]
		public bool m_BuildUpType;

		// Token: 0x0400423B RID: 16955
		[Token(Token = "0x4002F16")]
		private const string PLAY_ST = "set";

		// Token: 0x0400423C RID: 16956
		[Token(Token = "0x4002F17")]
		private const string PLAY_LP = "loop";

		// Token: 0x0400423D RID: 16957
		[Token(Token = "0x4002F18")]
		private const string PLAY_COMP_ST = "complete_st";

		// Token: 0x0400423E RID: 16958
		[Token(Token = "0x4002F19")]
		private const string PLAY_COMP_LP = "complete_lp";

		// Token: 0x0400423F RID: 16959
		[Token(Token = "0x4002F1A")]
		private const string PLAY_COMP_ED = "end";

		// Token: 0x04004240 RID: 16960
		[Token(Token = "0x4002F1B")]
		private const int PLAYANM_ST = 0;

		// Token: 0x04004241 RID: 16961
		[Token(Token = "0x4002F1C")]
		private const int PLAYANM_LP = 1;

		// Token: 0x04004242 RID: 16962
		[Token(Token = "0x4002F1D")]
		private const int PLAYANM_BASE_COMP_END = 2;

		// Token: 0x04004243 RID: 16963
		[Token(Token = "0x4002F1E")]
		private const int PLAYANM_BASE_COMP_ST = 3;

		// Token: 0x04004244 RID: 16964
		[Token(Token = "0x4002F1F")]
		private const int PLAYANM_BASE_COMP_LP = 4;

		// Token: 0x04004245 RID: 16965
		[Token(Token = "0x4002F20")]
		private const int PLAYANM_RE_COMP_END = 7;

		// Token: 0x04004246 RID: 16966
		[Token(Token = "0x4002F21")]
		private const int PLAYANM_RE_COMP_ST = 5;

		// Token: 0x04004247 RID: 16967
		[Token(Token = "0x4002F22")]
		private const int PLAYANM_RE_COMP_LP = 6;

		// Token: 0x04004248 RID: 16968
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002F23")]
		private TownPartsBuildGauge.eCalcStep m_CalcStep;

		// Token: 0x02000B43 RID: 2883
		[Token(Token = "0x2001029")]
		public enum eCalcStep
		{
			// Token: 0x0400424A RID: 16970
			[Token(Token = "0x4006692")]
			Init,
			// Token: 0x0400424B RID: 16971
			[Token(Token = "0x4006693")]
			InitGauge,
			// Token: 0x0400424C RID: 16972
			[Token(Token = "0x4006694")]
			BuildGauge,
			// Token: 0x0400424D RID: 16973
			[Token(Token = "0x4006695")]
			Stop_ST,
			// Token: 0x0400424E RID: 16974
			[Token(Token = "0x4006696")]
			Stop,
			// Token: 0x0400424F RID: 16975
			[Token(Token = "0x4006697")]
			Complete,
			// Token: 0x04004250 RID: 16976
			[Token(Token = "0x4006698")]
			End
		}
	}
}
