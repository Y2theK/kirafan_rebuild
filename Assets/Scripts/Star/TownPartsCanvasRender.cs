﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000B44 RID: 2884
	[Token(Token = "0x20007E7")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011CF30", Offset = "0x11CF30")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011CF30", Offset = "0x11CF30")]
	[StructLayout(3)]
	public class TownPartsCanvasRender : Graphic
	{
		// Token: 0x0600328A RID: 12938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E33")]
		[Address(RVA = "0x1013AAAE8", Offset = "0x13AAAE8", VA = "0x1013AAAE8")]
		public void SetMesh(Vector3[] pvert, Vector2[] puv, int[] pindex, Color fcolor, Texture ptex)
		{
		}

		// Token: 0x0600328B RID: 12939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E34")]
		[Address(RVA = "0x1013AAD9C", Offset = "0x13AAD9C", VA = "0x1013AAD9C")]
		public void SetColor(Color fcol)
		{
		}

		// Token: 0x0600328C RID: 12940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E35")]
		[Address(RVA = "0x1013AADAC", Offset = "0x13AADAC", VA = "0x1013AADAC", Slot = "44")]
		protected override void OnPopulateMesh(VertexHelper vbo)
		{
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x0600328D RID: 12941 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700039C")]
		public override Texture mainTexture
		{
			[Token(Token = "0x6002E36")]
			[Address(RVA = "0x1013AB0C8", Offset = "0x13AB0C8", VA = "0x1013AB0C8", Slot = "35")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600328E RID: 12942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E37")]
		[Address(RVA = "0x1013AB0D0", Offset = "0x13AB0D0", VA = "0x1013AB0D0")]
		public TownPartsCanvasRender()
		{
		}

		// Token: 0x04004251 RID: 16977
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4002F24")]
		public Vector3[] m_Vertex;

		// Token: 0x04004252 RID: 16978
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4002F25")]
		public Vector2[] m_UV;

		// Token: 0x04004253 RID: 16979
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4002F26")]
		public int[] m_Index;

		// Token: 0x04004254 RID: 16980
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4002F27")]
		public Texture m_SetTex;

		// Token: 0x04004255 RID: 16981
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4002F28")]
		public int m_VertNum;

		// Token: 0x04004256 RID: 16982
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4002F29")]
		public int m_TriNum;
	}
}
