﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B45 RID: 2885
	[Token(Token = "0x20007E8")]
	[StructLayout(3)]
	public class TownPartsChangeArea : ITownPartsAction
	{
		// Token: 0x0600328F RID: 12943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E38")]
		[Address(RVA = "0x1013AB138", Offset = "0x13AB138", VA = "0x1013AB138")]
		public TownPartsChangeArea(TownBuilder pbuilder, ITownObjectHandler phandle, int fbackbuildpoint, int fobjid)
		{
		}

		// Token: 0x06003290 RID: 12944 RVA: 0x00015798 File Offset: 0x00013998
		[Token(Token = "0x6002E39")]
		[Address(RVA = "0x1013AB194", Offset = "0x13AB194", VA = "0x1013AB194", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004257 RID: 16983
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F2A")]
		private TownBuilder m_Builder;

		// Token: 0x04004258 RID: 16984
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F2B")]
		private float m_MaxTime;

		// Token: 0x04004259 RID: 16985
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002F2C")]
		private float m_Time;

		// Token: 0x0400425A RID: 16986
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F2D")]
		private TownPartsChangeArea.eStep m_Step;

		// Token: 0x0400425B RID: 16987
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F2E")]
		private ITownObjectHandler m_Handle;

		// Token: 0x0400425C RID: 16988
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F2F")]
		private int m_NextObjectID;

		// Token: 0x0400425D RID: 16989
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002F30")]
		private int m_BackBuildPoint;

		// Token: 0x02000B46 RID: 2886
		[Token(Token = "0x200102A")]
		public enum eStep
		{
			// Token: 0x0400425F RID: 16991
			[Token(Token = "0x400669A")]
			FadeOutIn,
			// Token: 0x04004260 RID: 16992
			[Token(Token = "0x400669B")]
			FadeOut,
			// Token: 0x04004261 RID: 16993
			[Token(Token = "0x400669C")]
			ModelChange
		}
	}
}
