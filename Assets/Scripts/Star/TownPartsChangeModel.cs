﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B47 RID: 2887
	[Token(Token = "0x20007E9")]
	[StructLayout(3)]
	public class TownPartsChangeModel : ITownPartsAction
	{
		// Token: 0x06003291 RID: 12945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E3A")]
		[Address(RVA = "0x1013AB300", Offset = "0x13AB300", VA = "0x1013AB300")]
		public TownPartsChangeModel(GameObject pdelobj, ITownObjectHandler pbuild, int fnewobjid, long fmanageid)
		{
		}

		// Token: 0x06003292 RID: 12946 RVA: 0x000157B0 File Offset: 0x000139B0
		[Token(Token = "0x6002E3B")]
		[Address(RVA = "0x1013AB360", Offset = "0x13AB360", VA = "0x1013AB360", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004262 RID: 16994
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F31")]
		private float m_MaxTime;

		// Token: 0x04004263 RID: 16995
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002F32")]
		private float m_Time;

		// Token: 0x04004264 RID: 16996
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F33")]
		private TownPartsChangeModel.eStep m_Step;

		// Token: 0x04004265 RID: 16997
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F34")]
		private ITownObjectHandler m_Build;

		// Token: 0x04004266 RID: 16998
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F35")]
		public GameObject m_DelObj;

		// Token: 0x04004267 RID: 16999
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F36")]
		private long m_ChangeManageID;

		// Token: 0x04004268 RID: 17000
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F37")]
		private int m_ChangeResID;

		// Token: 0x02000B48 RID: 2888
		[Token(Token = "0x200102B")]
		public enum eStep
		{
			// Token: 0x0400426A RID: 17002
			[Token(Token = "0x400669E")]
			FadeOut,
			// Token: 0x0400426B RID: 17003
			[Token(Token = "0x400669F")]
			ModelChange
		}
	}
}
