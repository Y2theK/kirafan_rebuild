﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B49 RID: 2889
	[Token(Token = "0x20007EA")]
	[StructLayout(3)]
	public class TownPartsCharaHud : ITownPartsAction
	{
		// Token: 0x06003293 RID: 12947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E3C")]
		[Address(RVA = "0x1013AB44C", Offset = "0x13AB44C", VA = "0x1013AB44C")]
		public TownPartsCharaHud(TownBuilder pbuilder, Transform parent, Vector3 foffset, int fcharaid, int fmassageid)
		{
		}

		// Token: 0x06003294 RID: 12948 RVA: 0x000157C8 File Offset: 0x000139C8
		[Token(Token = "0x6002E3D")]
		[Address(RVA = "0x1013AB60C", Offset = "0x13AB60C", VA = "0x1013AB60C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x0400426C RID: 17004
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F38")]
		private TownBuilder m_Builder;

		// Token: 0x0400426D RID: 17005
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F39")]
		private GameObject m_HudObject;

		// Token: 0x0400426E RID: 17006
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F3A")]
		private TownCharaHud m_PartsLink;

		// Token: 0x0400426F RID: 17007
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F3B")]
		private AnimationState[] m_AnimeTable;

		// Token: 0x04004270 RID: 17008
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F3C")]
		private int m_VoicePlayID;

		// Token: 0x04004271 RID: 17009
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002F3D")]
		private int m_Step;

		// Token: 0x04004272 RID: 17010
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F3E")]
		private float m_TimeUp;
	}
}
