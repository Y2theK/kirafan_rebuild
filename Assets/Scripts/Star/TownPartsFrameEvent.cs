﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B4A RID: 2890
	[Token(Token = "0x20007EB")]
	[StructLayout(3)]
	public class TownPartsFrameEvent : ITownPartsAction
	{
		// Token: 0x06003295 RID: 12949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E3E")]
		[Address(RVA = "0x1013AB688", Offset = "0x13AB688", VA = "0x1013AB688")]
		public TownPartsFrameEvent()
		{
		}

		// Token: 0x06003296 RID: 12950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E3F")]
		[Address(RVA = "0x1013AB704", Offset = "0x13AB704", VA = "0x1013AB704")]
		public void Stack(int frame, TownPartsFrameEvent.CallEvent pcallback, int fkeyid)
		{
		}

		// Token: 0x06003297 RID: 12951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E40")]
		[Address(RVA = "0x1013AB7FC", Offset = "0x13AB7FC", VA = "0x1013AB7FC")]
		public void Play()
		{
		}

		// Token: 0x06003298 RID: 12952 RVA: 0x000157E0 File Offset: 0x000139E0
		[Token(Token = "0x6002E41")]
		[Address(RVA = "0x1013AB9F4", Offset = "0x13AB9F4", VA = "0x1013AB9F4", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x04004273 RID: 17011
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F3F")]
		private List<TownPartsFrameEvent.StackQue> m_Que;

		// Token: 0x04004274 RID: 17012
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F40")]
		private float m_Times;

		// Token: 0x04004275 RID: 17013
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4002F41")]
		private int m_PlayIndex;

		// Token: 0x04004276 RID: 17014
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F42")]
		private int m_PlayMax;

		// Token: 0x02000B4B RID: 2891
		// (Invoke) Token: 0x0600329A RID: 12954
		[Token(Token = "0x200102C")]
		public delegate bool CallEvent(int fkeycode);

		// Token: 0x02000B4C RID: 2892
		[Token(Token = "0x200102D")]
		public class StackQue
		{
			// Token: 0x0600329D RID: 12957 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006114")]
			[Address(RVA = "0x1013AB7F4", Offset = "0x13AB7F4", VA = "0x1013AB7F4")]
			public StackQue()
			{
			}

			// Token: 0x04004277 RID: 17015
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066A0")]
			public float m_Time;

			// Token: 0x04004278 RID: 17016
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40066A1")]
			public TownPartsFrameEvent.CallEvent m_Callback;

			// Token: 0x04004279 RID: 17017
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066A2")]
			public int m_KeyCode;
		}
	}
}
