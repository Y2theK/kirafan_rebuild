﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000B4D RID: 2893
	[Token(Token = "0x20007EC")]
	[StructLayout(3)]
	public class TownPartsGetItem : ITownPartsAction
	{
		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x0600329E RID: 12958 RVA: 0x000157F8 File Offset: 0x000139F8
		// (set) Token: 0x0600329F RID: 12959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700039D")]
		public bool IsEnableUpdate
		{
			[Token(Token = "0x6002E42")]
			[Address(RVA = "0x1013ABF9C", Offset = "0x13ABF9C", VA = "0x1013ABF9C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002E43")]
			[Address(RVA = "0x1013ABFA4", Offset = "0x13ABFA4", VA = "0x1013ABFA4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060032A0 RID: 12960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E44")]
		[Address(RVA = "0x1013ABFAC", Offset = "0x13ABFAC", VA = "0x1013ABFAC")]
		public TownPartsGetItem(TownBuilder pbuilder, ComItemUpState.eUpCategory fcategory, int fnum, Vector3 fpos, float fdelaytime)
		{
		}

		// Token: 0x060032A1 RID: 12961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E45")]
		[Address(RVA = "0x1013AC9DC", Offset = "0x13AC9DC", VA = "0x1013AC9DC")]
		public TownPartsGetItem(TownBuilder pbuilder, string text, int itemID, int fnum, Vector3 fpos, Vector3 spriteOffset, Vector3 fscl, float fdelaytime, float timeScale = 1f)
		{
		}

		// Token: 0x060032A2 RID: 12962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E46")]
		[Address(RVA = "0x1013AD180", Offset = "0x13AD180", VA = "0x1013AD180", Slot = "5")]
		public override void Destory()
		{
		}

		// Token: 0x060032A3 RID: 12963 RVA: 0x00015810 File Offset: 0x00013A10
		[Token(Token = "0x6002E47")]
		[Address(RVA = "0x1013AD21C", Offset = "0x13AD21C", VA = "0x1013AD21C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x060032A4 RID: 12964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E48")]
		[Address(RVA = "0x1013AD694", Offset = "0x13AD694", VA = "0x1013AD694")]
		private void SetFontFade(float falpha)
		{
		}

		// Token: 0x0400427A RID: 17018
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F43")]
		private TownBuilder m_Builder;

		// Token: 0x0400427B RID: 17019
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F44")]
		private GameObject m_Object;

		// Token: 0x0400427C RID: 17020
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F45")]
		private int m_Step;

		// Token: 0x0400427D RID: 17021
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F46")]
		private float m_Time;

		// Token: 0x0400427E RID: 17022
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F47")]
		private Text[] m_Message;

		// Token: 0x0400427F RID: 17023
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F48")]
		private Image m_TitleImage;

		// Token: 0x04004280 RID: 17024
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F49")]
		private Vector3 m_BasePos;

		// Token: 0x04004281 RID: 17025
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4002F4A")]
		private Vector3 m_TargetPos;

		// Token: 0x04004282 RID: 17026
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4002F4B")]
		private int m_ItemID;

		// Token: 0x04004283 RID: 17027
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4002F4C")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04004284 RID: 17028
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4002F4D")]
		private float m_TimeScale;

		// Token: 0x04004286 RID: 17030
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4002F4F")]
		private TownPartsGetItem.NumberState[] m_Number;

		// Token: 0x04004287 RID: 17031
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4002F50")]
		private int m_NumberNum;

		// Token: 0x02000B4E RID: 2894
		[Token(Token = "0x200102E")]
		public struct NumberState
		{
			// Token: 0x060032A5 RID: 12965 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006115")]
			[Address(RVA = "0x100037498", Offset = "0x37498", VA = "0x100037498")]
			public void SetUpNumber(Transform parent, Sprite psrite, int fno, Vector3 fpos, float fdelay)
			{
			}

			// Token: 0x060032A6 RID: 12966 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006116")]
			[Address(RVA = "0x1000374A0", Offset = "0x374A0", VA = "0x1000374A0")]
			public void Update(float faddtime)
			{
			}

			// Token: 0x060032A7 RID: 12967 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006117")]
			[Address(RVA = "0x1000374A8", Offset = "0x374A8", VA = "0x1000374A8")]
			public void SetFadeOut()
			{
			}

			// Token: 0x060032A8 RID: 12968 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006118")]
			[Address(RVA = "0x1000374B8", Offset = "0x374B8", VA = "0x1000374B8")]
			private void SetFade(float falpha)
			{
			}

			// Token: 0x060032A9 RID: 12969 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006119")]
			[Address(RVA = "0x1000374C0", Offset = "0x374C0", VA = "0x1000374C0")]
			private void CreateRender(Sprite psprite)
			{
			}

			// Token: 0x04004288 RID: 17032
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40066A3")]
			public int m_Step;

			// Token: 0x04004289 RID: 17033
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40066A4")]
			public float m_Time;

			// Token: 0x0400428A RID: 17034
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x40066A5")]
			public GameObject m_Number;

			// Token: 0x0400428B RID: 17035
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40066A6")]
			public Vector3 m_BasePos;

			// Token: 0x0400428C RID: 17036
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40066A7")]
			public TownPartsCanvasRender m_Render;
		}
	}
}
