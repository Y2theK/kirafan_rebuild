﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B4F RID: 2895
	[Token(Token = "0x20007ED")]
	[StructLayout(3)]
	public class TownPartsMaker : ITownPartsAction
	{
		// Token: 0x060032AA RID: 12970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E49")]
		[Address(RVA = "0x1013AE064", Offset = "0x13AE064", VA = "0x1013AE064")]
		public TownPartsMaker(TownBuilder pbuilder, Transform parent, GameObject phitnode, int flayer)
		{
		}

		// Token: 0x060032AB RID: 12971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E4A")]
		[Address(RVA = "0x1013AE148", Offset = "0x13AE148", VA = "0x1013AE148")]
		public void SetEndMark()
		{
		}

		// Token: 0x060032AC RID: 12972 RVA: 0x00015828 File Offset: 0x00013A28
		[Token(Token = "0x6002E4B")]
		[Address(RVA = "0x1013AE150", Offset = "0x13AE150", VA = "0x1013AE150", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x060032AD RID: 12973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E4C")]
		[Address(RVA = "0x1013AE4F0", Offset = "0x13AE4F0", VA = "0x1013AE4F0")]
		private void SetBlinkHitModel()
		{
		}

		// Token: 0x0400428D RID: 17037
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F51")]
		private TownBuilder m_Builder;

		// Token: 0x0400428E RID: 17038
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F52")]
		private GameObject m_Maker;

		// Token: 0x0400428F RID: 17039
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F53")]
		private TownPartsMaker.eCalcStep m_CalcStep;

		// Token: 0x04004290 RID: 17040
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F54")]
		private int m_LayerID;

		// Token: 0x04004291 RID: 17041
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F55")]
		private Renderer[] m_MarkRender;

		// Token: 0x04004292 RID: 17042
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F56")]
		private float m_BlinkTime;

		// Token: 0x04004293 RID: 17043
		[Token(Token = "0x4002F57")]
		private const float BlinkTime = 1.5f;

		// Token: 0x02000B50 RID: 2896
		[Token(Token = "0x200102F")]
		public enum eCalcStep
		{
			// Token: 0x04004295 RID: 17045
			[Token(Token = "0x40066A9")]
			Init,
			// Token: 0x04004296 RID: 17046
			[Token(Token = "0x40066AA")]
			Calc,
			// Token: 0x04004297 RID: 17047
			[Token(Token = "0x40066AB")]
			End
		}
	}
}
