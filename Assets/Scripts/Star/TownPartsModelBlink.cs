﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B51 RID: 2897
	[Token(Token = "0x20007EE")]
	[StructLayout(3)]
	public class TownPartsModelBlink : ITownPartsAction
	{
		// Token: 0x060032AE RID: 12974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E4D")]
		[Address(RVA = "0x1013A6A98", Offset = "0x13A6A98", VA = "0x1013A6A98")]
		public TownPartsModelBlink(GameObject phitnode, float fper, float fline, float fblinktime = 1f)
		{
		}

		// Token: 0x060032AF RID: 12975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E4E")]
		[Address(RVA = "0x1013A6D84", Offset = "0x13A6D84", VA = "0x1013A6D84")]
		public void SetEndMark()
		{
		}

		// Token: 0x060032B0 RID: 12976 RVA: 0x00015840 File Offset: 0x00013A40
		[Token(Token = "0x6002E4F")]
		[Address(RVA = "0x1013AE4F4", Offset = "0x13AE4F4", VA = "0x1013AE4F4", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x060032B1 RID: 12977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E50")]
		[Address(RVA = "0x1013AE720", Offset = "0x13AE720", VA = "0x1013AE720")]
		private void SetBlinkModel()
		{
		}

		// Token: 0x060032B2 RID: 12978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E51")]
		[Address(RVA = "0x1013AE588", Offset = "0x13AE588", VA = "0x1013AE588")]
		private void ResetBlinkModel()
		{
		}

		// Token: 0x04004298 RID: 17048
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F58")]
		private TownPartsModelBlink.eCalcStep m_CalcStep;

		// Token: 0x04004299 RID: 17049
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F59")]
		private MsbHandler[] m_MsbHndl;

		// Token: 0x0400429A RID: 17050
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F5A")]
		private float m_Per;

		// Token: 0x0400429B RID: 17051
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F5B")]
		private float m_Line;

		// Token: 0x0400429C RID: 17052
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F5C")]
		private float m_BlinkTime;

		// Token: 0x0400429D RID: 17053
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002F5D")]
		private float m_BlinkMaxTime;

		// Token: 0x02000B52 RID: 2898
		[Token(Token = "0x2001030")]
		public enum eCalcStep
		{
			// Token: 0x0400429F RID: 17055
			[Token(Token = "0x40066AD")]
			Init,
			// Token: 0x040042A0 RID: 17056
			[Token(Token = "0x40066AE")]
			Calc,
			// Token: 0x040042A1 RID: 17057
			[Token(Token = "0x40066AF")]
			End
		}
	}
}
