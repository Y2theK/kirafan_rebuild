﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B53 RID: 2899
	[Token(Token = "0x20007EF")]
	[StructLayout(3)]
	public class TownPartsModelMaker : ITownPartsAction
	{
		// Token: 0x060032B3 RID: 12979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E52")]
		[Address(RVA = "0x1013AE924", Offset = "0x13AE924", VA = "0x1013AE924")]
		public TownPartsModelMaker(TownBuilder pbuilder, Transform parent, GameObject phitnode, int flayer)
		{
		}

		// Token: 0x060032B4 RID: 12980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E53")]
		[Address(RVA = "0x1013AEA58", Offset = "0x13AEA58", VA = "0x1013AEA58")]
		public void SetEndMark()
		{
		}

		// Token: 0x060032B5 RID: 12981 RVA: 0x00015858 File Offset: 0x00013A58
		[Token(Token = "0x6002E54")]
		[Address(RVA = "0x1013AEA60", Offset = "0x13AEA60", VA = "0x1013AEA60", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x060032B6 RID: 12982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E55")]
		[Address(RVA = "0x1013AED08", Offset = "0x13AED08", VA = "0x1013AED08")]
		private void SetBlinkModel()
		{
		}

		// Token: 0x040042A2 RID: 17058
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F5E")]
		private TownBuilder m_Builder;

		// Token: 0x040042A3 RID: 17059
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F5F")]
		private GameObject m_Maker;

		// Token: 0x040042A4 RID: 17060
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F60")]
		private TownPartsModelMaker.eCalcStep m_CalcStep;

		// Token: 0x040042A5 RID: 17061
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F61")]
		private int m_LayerID;

		// Token: 0x040042A6 RID: 17062
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F62")]
		private MsbHandler[] m_MsbHndl;

		// Token: 0x040042A7 RID: 17063
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F63")]
		private float m_BlinkTime;

		// Token: 0x040042A8 RID: 17064
		[Token(Token = "0x4002F64")]
		private const float BlinkTime = 1.5f;

		// Token: 0x02000B54 RID: 2900
		[Token(Token = "0x2001031")]
		public enum eCalcStep
		{
			// Token: 0x040042AA RID: 17066
			[Token(Token = "0x40066B1")]
			Init,
			// Token: 0x040042AB RID: 17067
			[Token(Token = "0x40066B2")]
			Calc,
			// Token: 0x040042AC RID: 17068
			[Token(Token = "0x40066B3")]
			End
		}
	}
}
