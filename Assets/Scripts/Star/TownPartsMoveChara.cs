﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B55 RID: 2901
	[Token(Token = "0x20007F0")]
	[StructLayout(3)]
	public class TownPartsMoveChara : ITownPartsAction
	{
		// Token: 0x060032B7 RID: 12983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E56")]
		[Address(RVA = "0x1013AEEF4", Offset = "0x13AEEF4", VA = "0x1013AEEF4")]
		public TownPartsMoveChara(TownObjHandleChara phandle, TownBuilder pbuilder, long flinkbuildpoint, long fbuildpoint)
		{
		}

		// Token: 0x060032B8 RID: 12984 RVA: 0x00015870 File Offset: 0x00013A70
		[Token(Token = "0x6002E57")]
		[Address(RVA = "0x1013AEF4C", Offset = "0x13AEF4C", VA = "0x1013AEF4C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x040042AD RID: 17069
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F65")]
		private TownObjHandleChara m_Handle;

		// Token: 0x040042AE RID: 17070
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F66")]
		private TownBuilder m_Builder;

		// Token: 0x040042AF RID: 17071
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F67")]
		private TownPartsMoveChara.eState m_State;

		// Token: 0x040042B0 RID: 17072
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002F68")]
		private long m_LinkBuildPointMngID;

		// Token: 0x040042B1 RID: 17073
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F69")]
		private long m_NextBuildPointMngID;

		// Token: 0x02000B56 RID: 2902
		[Token(Token = "0x2001032")]
		public enum eState
		{
			// Token: 0x040042B3 RID: 17075
			[Token(Token = "0x40066B5")]
			Init,
			// Token: 0x040042B4 RID: 17076
			[Token(Token = "0x40066B6")]
			FadeOut,
			// Token: 0x040042B5 RID: 17077
			[Token(Token = "0x40066B7")]
			ChangePoint,
			// Token: 0x040042B6 RID: 17078
			[Token(Token = "0x40066B8")]
			FadeIn
		}
	}
}
