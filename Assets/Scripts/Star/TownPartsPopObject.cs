﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000B57 RID: 2903
	[Token(Token = "0x20007F1")]
	[StructLayout(3)]
	public class TownPartsPopObject : MonoBehaviour
	{
		// Token: 0x060032B9 RID: 12985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E58")]
		[Address(RVA = "0x1013AF128", Offset = "0x13AF128", VA = "0x1013AF128")]
		public void SetPopObject(TownBuilder pbuilder, GameObject pself, ITownObjectHandler phandle, Vector3 foffset)
		{
		}

		// Token: 0x060032BA RID: 12986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E59")]
		[Address(RVA = "0x1013AF70C", Offset = "0x13AF70C", VA = "0x1013AF70C")]
		public void ChangeIcon(int findex)
		{
		}

		// Token: 0x060032BB RID: 12987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5A")]
		[Address(RVA = "0x1013AF928", Offset = "0x13AF928", VA = "0x1013AF928")]
		public void DestroyRequest()
		{
		}

		// Token: 0x060032BC RID: 12988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5B")]
		[Address(RVA = "0x1013AF934", Offset = "0x13AF934", VA = "0x1013AF934")]
		public void Update()
		{
		}

		// Token: 0x060032BD RID: 12989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5C")]
		[Address(RVA = "0x1013AF8EC", Offset = "0x13AF8EC", VA = "0x1013AF8EC")]
		public void SetViewActive(bool factive)
		{
		}

		// Token: 0x060032BE RID: 12990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5D")]
		[Address(RVA = "0x1013AFA40", Offset = "0x13AFA40", VA = "0x1013AFA40")]
		public void ChangePer(int fcountup, bool fix)
		{
		}

		// Token: 0x060032BF RID: 12991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5E")]
		[Address(RVA = "0x1013AFCCC", Offset = "0x13AFCCC", VA = "0x1013AFCCC")]
		public TownPartsPopObject()
		{
		}

		// Token: 0x040042B7 RID: 17079
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F6A")]
		private GameObject m_Object;

		// Token: 0x040042B8 RID: 17080
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F6B")]
		private TownBuilder m_Builder;

		// Token: 0x040042B9 RID: 17081
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002F6C")]
		private Vector3 m_Offset;

		// Token: 0x040042BA RID: 17082
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002F6D")]
		private byte m_Step;

		// Token: 0x040042BB RID: 17083
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F6E")]
		private GameObject[] m_PopObject;

		// Token: 0x040042BC RID: 17084
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F6F")]
		private GameObject m_CountSheet;

		// Token: 0x040042BD RID: 17085
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4002F70")]
		private Text m_Message;
	}
}
