﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B58 RID: 2904
	[Token(Token = "0x20007F2")]
	[StructLayout(3)]
	public class TownPartsPreModelLoad : ITownPartsAction
	{
		// Token: 0x060032C0 RID: 12992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E5F")]
		[Address(RVA = "0x1013AFCD4", Offset = "0x13AFCD4", VA = "0x1013AFCD4")]
		public TownPartsPreModelLoad()
		{
		}

		// Token: 0x060032C1 RID: 12993 RVA: 0x00015888 File Offset: 0x00013A88
		[Token(Token = "0x6002E60")]
		[Address(RVA = "0x1013AFCDC", Offset = "0x13AFCDC", VA = "0x1013AFCDC", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}
	}
}
