﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B59 RID: 2905
	[Token(Token = "0x20007F3")]
	[StructLayout(3)]
	public class TownPartsTouchScale : ITownPartsAction
	{
		// Token: 0x060032C2 RID: 12994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E61")]
		[Address(RVA = "0x1013A712C", Offset = "0x13A712C", VA = "0x1013A712C")]
		public TownPartsTouchScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
		}

		// Token: 0x060032C3 RID: 12995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E62")]
		[Address(RVA = "0x1013A71BC", Offset = "0x13A71BC", VA = "0x1013A71BC")]
		public void SetStepIn()
		{
		}

		// Token: 0x060032C4 RID: 12996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E63")]
		[Address(RVA = "0x1013A71C4", Offset = "0x13A71C4", VA = "0x1013A71C4")]
		public void SetStepOut()
		{
		}

		// Token: 0x060032C5 RID: 12997 RVA: 0x000158A0 File Offset: 0x00013AA0
		[Token(Token = "0x6002E64")]
		[Address(RVA = "0x1013AFCE4", Offset = "0x13AFCE4", VA = "0x1013AFCE4", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x040042BE RID: 17086
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F71")]
		private Transform m_Marker;

		// Token: 0x040042BF RID: 17087
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F72")]
		private Vector3 m_Target;

		// Token: 0x040042C0 RID: 17088
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F73")]
		private Vector3 m_Base;

		// Token: 0x040042C1 RID: 17089
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F74")]
		private float m_Time;

		// Token: 0x040042C2 RID: 17090
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002F75")]
		private float m_MaxTime;

		// Token: 0x040042C3 RID: 17091
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4002F76")]
		private int m_Step;
	}
}
