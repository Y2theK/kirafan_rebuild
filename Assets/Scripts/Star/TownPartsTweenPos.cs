﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B5A RID: 2906
	[Token(Token = "0x20007F4")]
	[StructLayout(3)]
	public class TownPartsTweenPos : ITownPartsAction
	{
		// Token: 0x060032C6 RID: 12998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E65")]
		[Address(RVA = "0x1013AFF14", Offset = "0x13AFF14", VA = "0x1013AFF14")]
		public TownPartsTweenPos(Transform pself, Vector3 ftarget, float ftime)
		{
		}

		// Token: 0x060032C7 RID: 12999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002E66")]
		[Address(RVA = "0x1013A7930", Offset = "0x13A7930", VA = "0x1013A7930")]
		public void ChangePos(Vector3 ftarget, float ftime)
		{
		}

		// Token: 0x060032C8 RID: 13000 RVA: 0x000158B8 File Offset: 0x00013AB8
		[Token(Token = "0x6002E67")]
		[Address(RVA = "0x1013AFF9C", Offset = "0x13AFF9C", VA = "0x1013AFF9C", Slot = "4")]
		public override bool PartsUpdate()
		{
			return default(bool);
		}

		// Token: 0x040042C4 RID: 17092
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002F77")]
		private Transform m_Marker;

		// Token: 0x040042C5 RID: 17093
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002F78")]
		private Vector3 m_Target;

		// Token: 0x040042C6 RID: 17094
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4002F79")]
		private Vector3 m_Base;

		// Token: 0x040042C7 RID: 17095
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002F7A")]
		private float m_Time;

		// Token: 0x040042C8 RID: 17096
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4002F7B")]
		private float m_MaxTime;
	}
}
