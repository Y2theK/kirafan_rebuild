﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B09 RID: 2825
	[Token(Token = "0x20007B8")]
	[StructLayout(3)]
	public class TownPinchEvent
	{
		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x060031E3 RID: 12771 RVA: 0x00015498 File Offset: 0x00013698
		// (set) Token: 0x060031E4 RID: 12772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000398")]
		public float InitialPinchDist
		{
			[Token(Token = "0x6002D9C")]
			[Address(RVA = "0x1013B0164", Offset = "0x13B0164", VA = "0x1013B0164")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6002D9D")]
			[Address(RVA = "0x1013B016C", Offset = "0x13B016C", VA = "0x1013B016C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x060031E5 RID: 12773 RVA: 0x000154B0 File Offset: 0x000136B0
		// (set) Token: 0x060031E6 RID: 12774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000399")]
		public bool IsPinching
		{
			[Token(Token = "0x6002D9E")]
			[Address(RVA = "0x1013B0174", Offset = "0x13B0174", VA = "0x1013B0174")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6002D9F")]
			[Address(RVA = "0x1013B017C", Offset = "0x13B017C", VA = "0x1013B017C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x060031E7 RID: 12775 RVA: 0x000154C8 File Offset: 0x000136C8
		// (set) Token: 0x060031E8 RID: 12776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700039A")]
		public Vector2 FocusedPos
		{
			[Token(Token = "0x6002DA0")]
			[Address(RVA = "0x1013B0184", Offset = "0x13B0184", VA = "0x1013B0184")]
			[CompilerGenerated]
			get
			{
				return default(Vector2);
			}
			[Token(Token = "0x6002DA1")]
			[Address(RVA = "0x1013B018C", Offset = "0x13B018C", VA = "0x1013B018C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x060031E9 RID: 12777 RVA: 0x000154E0 File Offset: 0x000136E0
		// (set) Token: 0x060031EA RID: 12778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700039B")]
		public float PinchRatio
		{
			[Token(Token = "0x6002DA2")]
			[Address(RVA = "0x1013B0198", Offset = "0x13B0198", VA = "0x1013B0198")]
			[CompilerGenerated]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6002DA3")]
			[Address(RVA = "0x1013B01A0", Offset = "0x13B01A0", VA = "0x1013B01A0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060031EB RID: 12779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DA4")]
		[Address(RVA = "0x1013B01A8", Offset = "0x13B01A8", VA = "0x1013B01A8")]
		public void UpdatePinch(TownPinchState pstate)
		{
		}

		// Token: 0x060031EC RID: 12780 RVA: 0x000154F8 File Offset: 0x000136F8
		[Token(Token = "0x6002DA5")]
		[Address(RVA = "0x1013B03C4", Offset = "0x13B03C4", VA = "0x1013B03C4")]
		private Vector2 CalcFocusPos()
		{
			return default(Vector2);
		}

		// Token: 0x060031ED RID: 12781 RVA: 0x00015510 File Offset: 0x00013710
		[Token(Token = "0x6002DA6")]
		[Address(RVA = "0x1013B028C", Offset = "0x13B028C", VA = "0x1013B028C")]
		private float CalcPinchDist()
		{
			return 0f;
		}

		// Token: 0x060031EE RID: 12782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DA7")]
		[Address(RVA = "0x1013B053C", Offset = "0x13B053C", VA = "0x1013B053C")]
		public TownPinchEvent()
		{
		}

		// Token: 0x04004169 RID: 16745
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E7A")]
		private float m_CurrentPinchDist;

		// Token: 0x0400416A RID: 16746
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002E7B")]
		private float m_OldPinchRatio;
	}
}
