﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B08 RID: 2824
	[Token(Token = "0x20007B7")]
	[StructLayout(3)]
	public class TownPinchState
	{
		// Token: 0x060031E2 RID: 12770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D9B")]
		[Address(RVA = "0x1013B0544", Offset = "0x13B0544", VA = "0x1013B0544")]
		public TownPinchState()
		{
		}

		// Token: 0x04004166 RID: 16742
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E77")]
		public bool m_Event;

		// Token: 0x04004167 RID: 16743
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4002E78")]
		public bool IsPinching;

		// Token: 0x04004168 RID: 16744
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4002E79")]
		public float PinchRatio;
	}
}
