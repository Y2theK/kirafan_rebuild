﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BBB RID: 3003
	[Token(Token = "0x2000827")]
	[StructLayout(3)]
	public class TownRenderSync : MonoBehaviour
	{
		// Token: 0x06003499 RID: 13465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF8")]
		[Address(RVA = "0x1013B054C", Offset = "0x13B054C", VA = "0x1013B054C")]
		public void SetLinkRender(Camera pcamera, RenderTexture prender, RenderTexture pdepth)
		{
		}

		// Token: 0x0600349A RID: 13466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FF9")]
		[Address(RVA = "0x1013B05E8", Offset = "0x13B05E8", VA = "0x1013B05E8")]
		private void OnPostRender()
		{
		}

		// Token: 0x0600349B RID: 13467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FFA")]
		[Address(RVA = "0x1013B0688", Offset = "0x13B0688", VA = "0x1013B0688")]
		public TownRenderSync()
		{
		}

		// Token: 0x040044B6 RID: 17590
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030AB")]
		private Camera m_Camera;

		// Token: 0x040044B7 RID: 17591
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030AC")]
		public bool m_Sync;
	}
}
