﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BBC RID: 3004
	[Token(Token = "0x2000828")]
	[StructLayout(3)]
	public class TownResourceBind : MonoBehaviour
	{
		// Token: 0x0600349C RID: 13468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FFB")]
		[Address(RVA = "0x1013B0690", Offset = "0x13B0690", VA = "0x1013B0690")]
		public TownResourceBind()
		{
		}

		// Token: 0x040044B8 RID: 17592
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030AD")]
		[SerializeField]
		public TownResourceBind.Resource[] m_Table;

		// Token: 0x040044B9 RID: 17593
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030AE")]
		[SerializeField]
		public Vector3 m_PosOffset;

		// Token: 0x02000BBD RID: 3005
		[Token(Token = "0x2001061")]
		[Serializable]
		public struct Resource
		{
			// Token: 0x040044BA RID: 17594
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006777")]
			public UnityEngine.Object m_Res;
		}
	}
}
