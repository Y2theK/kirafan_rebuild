﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000BBE RID: 3006
	[Token(Token = "0x2000829")]
	[StructLayout(3)]
	public class TownResourceDownload : MonoBehaviour
	{
		// Token: 0x0600349D RID: 13469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FFC")]
		[Address(RVA = "0x1013B0708", Offset = "0x13B0708", VA = "0x1013B0708")]
		private void Start()
		{
		}

		// Token: 0x0600349E RID: 13470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FFD")]
		[Address(RVA = "0x1013B0880", Offset = "0x13B0880", VA = "0x1013B0880")]
		private void Update()
		{
		}

		// Token: 0x0600349F RID: 13471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002FFE")]
		[Address(RVA = "0x1013B0B04", Offset = "0x13B0B04", VA = "0x1013B0B04")]
		public void AddResource(string ffilename)
		{
		}

		// Token: 0x060034A0 RID: 13472 RVA: 0x00016440 File Offset: 0x00014640
		[Token(Token = "0x6002FFF")]
		[Address(RVA = "0x1013B0C5C", Offset = "0x13B0C5C", VA = "0x1013B0C5C")]
		public bool IsDownloadEnd()
		{
			return default(bool);
		}

		// Token: 0x060034A1 RID: 13473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003000")]
		[Address(RVA = "0x1013B0CCC", Offset = "0x13B0CCC", VA = "0x1013B0CCC")]
		public TownResourceDownload()
		{
		}

		// Token: 0x040044BB RID: 17595
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030AF")]
		private List<TownResourceDownload.ResDownloadState> m_ListUp;

		// Token: 0x040044BC RID: 17596
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030B0")]
		private int m_BuildUpNum;

		// Token: 0x02000BBF RID: 3007
		[Token(Token = "0x2001062")]
		public class ResDownloadState
		{
			// Token: 0x060034A2 RID: 13474 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600615A")]
			[Address(RVA = "0x1013B0C54", Offset = "0x13B0C54", VA = "0x1013B0C54")]
			public ResDownloadState()
			{
			}

			// Token: 0x040044BD RID: 17597
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006778")]
			public MeigeResource.Handler m_Handle;

			// Token: 0x040044BE RID: 17598
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006779")]
			public string m_Filename;
		}
	}
}
