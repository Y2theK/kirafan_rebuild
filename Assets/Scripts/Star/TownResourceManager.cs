﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000B9D RID: 2973
	[Token(Token = "0x2000815")]
	[StructLayout(3)]
	public sealed class TownResourceManager
	{
		// Token: 0x0600342D RID: 13357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F96")]
		[Address(RVA = "0x1013B0D3C", Offset = "0x13B0D3C", VA = "0x1013B0D3C")]
		public TownResourceManager()
		{
		}

		// Token: 0x0600342E RID: 13358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F97")]
		[Address(RVA = "0x1013B0F08", Offset = "0x13B0F08", VA = "0x1013B0F08")]
		public void Update()
		{
		}

		// Token: 0x0600342F RID: 13359 RVA: 0x00016110 File Offset: 0x00014310
		[Token(Token = "0x6002F98")]
		[Address(RVA = "0x1013B0F90", Offset = "0x13B0F90", VA = "0x1013B0F90")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06003430 RID: 13360 RVA: 0x00016128 File Offset: 0x00014328
		[Token(Token = "0x6002F99")]
		[Address(RVA = "0x1013B0F98", Offset = "0x13B0F98", VA = "0x1013B0F98")]
		public bool IsLoadingAny()
		{
			return default(bool);
		}

		// Token: 0x06003431 RID: 13361 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002F9A")]
		[Address(RVA = "0x1013B1024", Offset = "0x13B1024", VA = "0x1013B1024")]
		public ResourceObjectHandler LoadAsyncFromResources(TownResourceManager.eType type, string path)
		{
			return null;
		}

		// Token: 0x06003432 RID: 13362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F9B")]
		[Address(RVA = "0x1013B10E0", Offset = "0x13B10E0", VA = "0x1013B10E0")]
		public void UnloadFromResources(TownResourceManager.eType type, string path)
		{
		}

		// Token: 0x06003433 RID: 13363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F9C")]
		[Address(RVA = "0x1013B1158", Offset = "0x13B1158", VA = "0x1013B1158")]
		public void UnloadAllResources(TownResourceManager.eType type)
		{
		}

		// Token: 0x06003434 RID: 13364 RVA: 0x00016140 File Offset: 0x00014340
		[Token(Token = "0x6002F9D")]
		[Address(RVA = "0x1013B11C8", Offset = "0x13B11C8", VA = "0x1013B11C8")]
		public bool IsCompleteUnloadAllResources(TownResourceManager.eType type)
		{
			return default(bool);
		}

		// Token: 0x06003435 RID: 13365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002F9E")]
		[Address(RVA = "0x1013B1238", Offset = "0x13B1238", VA = "0x1013B1238")]
		public void UnloadAllResources()
		{
		}

		// Token: 0x06003436 RID: 13366 RVA: 0x00016158 File Offset: 0x00014358
		[Token(Token = "0x6002F9F")]
		[Address(RVA = "0x1013B12D0", Offset = "0x13B12D0", VA = "0x1013B12D0")]
		public bool IsCompleteUnloadAllResources()
		{
			return default(bool);
		}

		// Token: 0x04004445 RID: 17477
		[Token(Token = "0x4003061")]
		public const int TYPE_NUM = 2;

		// Token: 0x04004446 RID: 17478
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003062")]
		private readonly int[] SIMULTANEOUSLY_LOAD_NUMS;

		// Token: 0x04004447 RID: 17479
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003063")]
		private readonly ResourceObjectHandler.eLoadType[] LOAD_TYPES;

		// Token: 0x04004448 RID: 17480
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003064")]
		private ResourcesLoader[] m_Loaders;

		// Token: 0x02000B9E RID: 2974
		[Token(Token = "0x2001055")]
		public enum eType
		{
			// Token: 0x0400444A RID: 17482
			[Token(Token = "0x4006751")]
			Model,
			// Token: 0x0400444B RID: 17483
			[Token(Token = "0x4006752")]
			Anim,
			// Token: 0x0400444C RID: 17484
			[Token(Token = "0x4006753")]
			Num
		}
	}
}
