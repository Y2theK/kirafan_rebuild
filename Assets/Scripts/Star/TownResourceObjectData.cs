﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AFC RID: 2812
	[Token(Token = "0x20007AF")]
	[Serializable]
	[StructLayout(3)]
	public class TownResourceObjectData
	{
		// Token: 0x060031B9 RID: 12729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D73")]
		[Address(RVA = "0x1013B1374", Offset = "0x13B1374", VA = "0x1013B1374")]
		public void Initialize(string pfilename)
		{
		}

		// Token: 0x060031BA RID: 12730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D74")]
		[Address(RVA = "0x1013B14D4", Offset = "0x13B14D4", VA = "0x1013B14D4")]
		private void CallbackResObj(MeigeResource.Handler phandle)
		{
		}

		// Token: 0x060031BB RID: 12731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D75")]
		[Address(RVA = "0x1013B15C0", Offset = "0x13B15C0", VA = "0x1013B15C0")]
		public void Destroy()
		{
		}

		// Token: 0x060031BC RID: 12732 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002D76")]
		[Address(RVA = "0x1013B1778", Offset = "0x13B1778", VA = "0x1013B1778")]
		public GameObject GetNextObjectInCache(int fmodelno)
		{
			return null;
		}

		// Token: 0x060031BD RID: 12733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D77")]
		[Address(RVA = "0x1013B1AE4", Offset = "0x13B1AE4", VA = "0x1013B1AE4")]
		public void FreeObjectInCache(GameObject pobj)
		{
		}

		// Token: 0x060031BE RID: 12734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002D78")]
		[Address(RVA = "0x1013B1CBC", Offset = "0x13B1CBC", VA = "0x1013B1CBC")]
		public TownResourceObjectData()
		{
		}

		// Token: 0x0400412B RID: 16683
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4002E48")]
		private TownResourceObjectData.TCacheObjectState[] m_Objects;

		// Token: 0x0400412C RID: 16684
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002E49")]
		private int m_CacheSize;

		// Token: 0x0400412D RID: 16685
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002E4A")]
		private TownObjectDataBase m_ObjectList;

		// Token: 0x0400412E RID: 16686
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002E4B")]
		private MeigeResource.Handler m_Handle;

		// Token: 0x02000AFD RID: 2813
		[Token(Token = "0x200101A")]
		private class TCacheObjectState
		{
			// Token: 0x060031BF RID: 12735 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60060FF")]
			[Address(RVA = "0x1013B14CC", Offset = "0x13B14CC", VA = "0x1013B14CC")]
			public TCacheObjectState()
			{
			}

			// Token: 0x0400412F RID: 16687
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400664F")]
			public GameObject m_Object;

			// Token: 0x04004130 RID: 16688
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006650")]
			public bool m_Active;
		}
	}
}
