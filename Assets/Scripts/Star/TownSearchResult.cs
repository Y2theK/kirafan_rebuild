﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200069C RID: 1692
	[Token(Token = "0x2000567")]
	[StructLayout(0, Size = 16)]
	public struct TownSearchResult
	{
		// Token: 0x06001869 RID: 6249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6001700")]
		[Address(RVA = "0x1000374C8", Offset = "0x374C8", VA = "0x1000374C8")]
		public void ClearParam()
		{
		}

		// Token: 0x04002939 RID: 10553
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4002209")]
		public eTownMoveState m_UpState;

		// Token: 0x0400293A RID: 10554
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x400220A")]
		public long m_MoveTargetMngID;
	}
}
