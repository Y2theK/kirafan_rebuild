﻿using System.Collections.Generic;

namespace Star {
    public class TownServerForm {
        public enum eDataType {
            Build,
            DebugPlay,
            Dummy
        }

        public class Build {
            public List<UserTownData.BuildObjectUp> m_Table;
        }

        public class DebugPlay {
            public bool m_DebugPlay;
            public int m_Level;
            public long m_PlayTimes;
        }

        public class DataChunk {
            public int m_Ver;
            public int m_DataType;
            public byte[] m_Data;
        }
    }
}
