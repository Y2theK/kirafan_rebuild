﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000601 RID: 1537
	[Token(Token = "0x20004F4")]
	[StructLayout(3)]
	public class TownShopListDB : ScriptableObject
	{
		// Token: 0x06001611 RID: 5649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C1")]
		[Address(RVA = "0x1013B1D5C", Offset = "0x13B1D5C", VA = "0x1013B1D5C")]
		public TownShopListDB()
		{
		}

		// Token: 0x04002563 RID: 9571
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001ECD")]
		public TownShopListDB_Param[] m_Params;
	}
}
