﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000600 RID: 1536
	[Token(Token = "0x20004F3")]
	[Serializable]
	[StructLayout(0, Size = 4)]
	public struct TownShopListDB_Param
	{
		// Token: 0x04002562 RID: 9570
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001ECC")]
		public int m_ID;
	}
}
