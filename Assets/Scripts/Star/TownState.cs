﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000880 RID: 2176
	[Token(Token = "0x2000651")]
	[StructLayout(3)]
	public class TownState : GameStateBase
	{
		// Token: 0x06002312 RID: 8978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002065")]
		[Address(RVA = "0x1013B1D64", Offset = "0x13B1D64", VA = "0x1013B1D64")]
		public TownState(TownMain owner)
		{
		}

		// Token: 0x06002313 RID: 8979 RVA: 0x0000F150 File Offset: 0x0000D350
		[Token(Token = "0x6002066")]
		[Address(RVA = "0x1013B1D98", Offset = "0x13B1D98", VA = "0x1013B1D98", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002314 RID: 8980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002067")]
		[Address(RVA = "0x1013B1DA0", Offset = "0x13B1DA0", VA = "0x1013B1DA0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002315 RID: 8981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002068")]
		[Address(RVA = "0x1013B1DA4", Offset = "0x13B1DA4", VA = "0x1013B1DA4", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002316 RID: 8982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002069")]
		[Address(RVA = "0x1013B1DA8", Offset = "0x13B1DA8", VA = "0x1013B1DA8", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002317 RID: 8983 RVA: 0x0000F168 File Offset: 0x0000D368
		[Token(Token = "0x600206A")]
		[Address(RVA = "0x1013B1DAC", Offset = "0x13B1DAC", VA = "0x1013B1DAC", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002318 RID: 8984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600206B")]
		[Address(RVA = "0x1013B1DB4", Offset = "0x13B1DB4", VA = "0x1013B1DB4", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04003361 RID: 13153
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400261D")]
		protected TownMain m_Owner;

		// Token: 0x04003362 RID: 13154
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400261E")]
		protected int m_NextState;
	}
}
