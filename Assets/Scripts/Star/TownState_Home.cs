﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.LoginBonus;

namespace Star
{
	// Token: 0x02000881 RID: 2177
	[Token(Token = "0x2000652")]
	[StructLayout(3)]
	public class TownState_Home : TownState
	{
		// Token: 0x06002319 RID: 8985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600206C")]
		[Address(RVA = "0x1013B1DB8", Offset = "0x13B1DB8", VA = "0x1013B1DB8")]
		public TownState_Home(TownMain owner)
		{
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x0000F180 File Offset: 0x0000D380
		[Token(Token = "0x600206D")]
		[Address(RVA = "0x1013B1DF4", Offset = "0x13B1DF4", VA = "0x1013B1DF4", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600231B RID: 8987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600206E")]
		[Address(RVA = "0x1013B1DFC", Offset = "0x13B1DFC", VA = "0x1013B1DFC", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600231C RID: 8988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600206F")]
		[Address(RVA = "0x1013B1E04", Offset = "0x13B1E04", VA = "0x1013B1E04", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600231D RID: 8989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002070")]
		[Address(RVA = "0x1013B1E34", Offset = "0x13B1E34", VA = "0x1013B1E34", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600231E RID: 8990 RVA: 0x0000F198 File Offset: 0x0000D398
		[Token(Token = "0x6002071")]
		[Address(RVA = "0x1013B1FB4", Offset = "0x13B1FB4", VA = "0x1013B1FB4", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600231F RID: 8991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002072")]
		[Address(RVA = "0x1013B4FD8", Offset = "0x13B4FD8", VA = "0x1013B4FD8")]
		private void OnGotoADV()
		{
		}

		// Token: 0x06002320 RID: 8992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002073")]
		[Address(RVA = "0x1013B50F0", Offset = "0x13B50F0", VA = "0x1013B50F0")]
		private void OnGotoCharaQuest()
		{
		}

		// Token: 0x06002321 RID: 8993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002074")]
		[Address(RVA = "0x1013B5A68", Offset = "0x13B5A68", VA = "0x1013B5A68")]
		private void OnEndTotalLoginBonusData()
		{
		}

		// Token: 0x06002322 RID: 8994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002075")]
		[Address(RVA = "0x1013B4BBC", Offset = "0x13B4BBC", VA = "0x1013B4BBC")]
		private void RestartQuestFlow()
		{
		}

		// Token: 0x06002323 RID: 8995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002076")]
		[Address(RVA = "0x1013B5A74", Offset = "0x13B5A74", VA = "0x1013B5A74")]
		private void OnConfirmQuestRestart(int answer)
		{
		}

		// Token: 0x06002324 RID: 8996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002077")]
		[Address(RVA = "0x1013B5CD8", Offset = "0x13B5CD8", VA = "0x1013B5CD8")]
		private void OnResponse_PointEventGet()
		{
		}

		// Token: 0x06002325 RID: 8997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002078")]
		[Address(RVA = "0x1013B5FA8", Offset = "0x13B5FA8", VA = "0x1013B5FA8")]
		private void OnContinueRestartFailed(int answer)
		{
		}

		// Token: 0x06002326 RID: 8998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002079")]
		[Address(RVA = "0x1013B6060", Offset = "0x13B6060", VA = "0x1013B6060")]
		private void OnConfirmQuestDelete(int answer)
		{
		}

		// Token: 0x06002327 RID: 8999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207A")]
		[Address(RVA = "0x1013B6134", Offset = "0x13B6134", VA = "0x1013B6134")]
		private void OnResponseDeleteBSD()
		{
		}

		// Token: 0x06002328 RID: 9000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207B")]
		[Address(RVA = "0x1013B3E68", Offset = "0x13B3E68", VA = "0x1013B3E68")]
		private void OpenLoginBonusTotal()
		{
		}

		// Token: 0x06002329 RID: 9001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207C")]
		[Address(RVA = "0x1013B6138", Offset = "0x13B6138", VA = "0x1013B6138")]
		private void OnEndAdvertiseMovie()
		{
		}

		// Token: 0x0600232A RID: 9002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207D")]
		[Address(RVA = "0x1013B41B0", Offset = "0x13B41B0", VA = "0x1013B41B0")]
		private void OpenContentRoomNoticeWindow()
		{
		}

		// Token: 0x0600232B RID: 9003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207E")]
		[Address(RVA = "0x1013B61C8", Offset = "0x13B61C8", VA = "0x1013B61C8")]
		private void OnCloseNoticeWindow(int button)
		{
		}

		// Token: 0x0600232C RID: 9004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600207F")]
		[Address(RVA = "0x1013B6294", Offset = "0x13B6294", VA = "0x1013B6294")]
		private void OnResponse_SetShown()
		{
		}

		// Token: 0x0600232D RID: 9005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002080")]
		[Address(RVA = "0x1013B62A0", Offset = "0x13B62A0", VA = "0x1013B62A0")]
		private void OnCloseTipsWindow()
		{
		}

		// Token: 0x0600232E RID: 9006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002081")]
		[Address(RVA = "0x1013B452C", Offset = "0x13B452C", VA = "0x1013B452C")]
		private void OpenLoginReward()
		{
		}

		// Token: 0x0600232F RID: 9007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002082")]
		[Address(RVA = "0x1013B62AC", Offset = "0x13B62AC", VA = "0x1013B62AC")]
		private void OnConfirmeLoginReward(int ans)
		{
		}

		// Token: 0x06002330 RID: 9008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002083")]
		[Address(RVA = "0x1013B4C94", Offset = "0x13B4C94", VA = "0x1013B4C94")]
		private void OpenInfo()
		{
		}

		// Token: 0x06002331 RID: 9009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002084")]
		[Address(RVA = "0x1013B4EFC", Offset = "0x13B4EFC", VA = "0x1013B4EFC")]
		private void StartUnlock()
		{
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002085")]
		[Address(RVA = "0x1013B62B8", Offset = "0x13B62B8", VA = "0x1013B62B8")]
		private void OnEndPassportRemind(int ans)
		{
		}

		// Token: 0x06002333 RID: 9011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002086")]
		[Address(RVA = "0x1013B5148", Offset = "0x13B5148", VA = "0x1013B5148")]
		private void OpenRemindPremium()
		{
		}

		// Token: 0x06002334 RID: 9012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002087")]
		[Address(RVA = "0x1013B5924", Offset = "0x13B5924", VA = "0x1013B5924")]
		private void HomeStartCommon()
		{
		}

		// Token: 0x06002335 RID: 9013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002088")]
		[Address(RVA = "0x1013B64E0", Offset = "0x13B64E0", VA = "0x1013B64E0")]
		private void OnCompleteTutorialTipsWindow()
		{
		}

		// Token: 0x06002336 RID: 9014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002089")]
		[Address(RVA = "0x1013B6598", Offset = "0x13B6598", VA = "0x1013B6598")]
		private void OnResponseTutorialFinish()
		{
		}

		// Token: 0x06002337 RID: 9015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600208A")]
		[Address(RVA = "0x1013B6668", Offset = "0x13B6668", VA = "0x1013B6668")]
		private void OnResponseLoginBonus()
		{
		}

		// Token: 0x06002338 RID: 9016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600208B")]
		[Address(RVA = "0x1013B6728", Offset = "0x13B6728", VA = "0x1013B6728")]
		private void OnResponseStoreGetAll()
		{
		}

		// Token: 0x06002339 RID: 9017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600208C")]
		[Address(RVA = "0x1013B6C50", Offset = "0x13B6C50", VA = "0x1013B6C50", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0600233A RID: 9018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600208D")]
		[Address(RVA = "0x1013B6E10", Offset = "0x13B6E10", VA = "0x1013B6E10")]
		public void OnTransitCallBack()
		{
		}

		// Token: 0x0600233B RID: 9019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600208E")]
		[Address(RVA = "0x1013B5A04", Offset = "0x13B5A04", VA = "0x1013B5A04")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0600233C RID: 9020 RVA: 0x0000F1B0 File Offset: 0x0000D3B0
		[Token(Token = "0x600208F")]
		[Address(RVA = "0x1013B3BA4", Offset = "0x13B3BA4", VA = "0x1013B3BA4")]
		private bool SetupAndPlayLoginBonus()
		{
			return default(bool);
		}

		// Token: 0x0600233D RID: 9021 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002090")]
		[Address(RVA = "0x1013B6E14", Offset = "0x13B6E14", VA = "0x1013B6E14")]
		private PageUIData CreateLoginBonusUIData(LoginBonus data, bool isEvent)
		{
			return null;
		}

		// Token: 0x0600233E RID: 9022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002091")]
		[Address(RVA = "0x1013B6F90", Offset = "0x13B6F90", VA = "0x1013B6F90")]
		public void OnResponseInfo()
		{
		}

		// Token: 0x0600233F RID: 9023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002092")]
		[Address(RVA = "0x1013B6FD4", Offset = "0x13B6FD4", VA = "0x1013B6FD4")]
		private void OnConfirmAppQuit(int answer)
		{
		}

		// Token: 0x06002340 RID: 9024 RVA: 0x0000F1C8 File Offset: 0x0000D3C8
		[Token(Token = "0x6002093")]
		[Address(RVA = "0x1013B633C", Offset = "0x13B633C", VA = "0x1013B633C")]
		private bool CheckBeginnersMissionTutorial()
		{
			return default(bool);
		}

		// Token: 0x06002341 RID: 9025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002094")]
		[Address(RVA = "0x1013B6420", Offset = "0x13B6420", VA = "0x1013B6420")]
		private void RequestBeginnersMissionTipsAdd()
		{
		}

		// Token: 0x06002342 RID: 9026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002095")]
		[Address(RVA = "0x1013B6FE4", Offset = "0x13B6FE4", VA = "0x1013B6FE4")]
		private void OnResponseBeginnersMissionTipsAdd()
		{
		}

		// Token: 0x06002343 RID: 9027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002096")]
		[Address(RVA = "0x1013B6FE8", Offset = "0x13B6FE8", VA = "0x1013B6FE8")]
		private void OpenBeginnersMissionTutorial()
		{
		}

		// Token: 0x06002344 RID: 9028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002097")]
		[Address(RVA = "0x1013B1E3C", Offset = "0x13B1E3C", VA = "0x1013B1E3C")]
		private void CloseBeginnersMissionTutorial()
		{
		}

		// Token: 0x06002345 RID: 9029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002098")]
		[Address(RVA = "0x1013B7298", Offset = "0x13B7298", VA = "0x1013B7298")]
		public void MissionTransit(IMissionPakage mission)
		{
		}

		// Token: 0x04003363 RID: 13155
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400261F")]
		private TownState_Home.eStep m_Step;

		// Token: 0x04003364 RID: 13156
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002620")]
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04003365 RID: 13157
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4002621")]
		private LoginBonusUI m_LoginBonusUI;

		// Token: 0x04003366 RID: 13158
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4002622")]
		private bool m_IsTutorialFinish;

		// Token: 0x04003367 RID: 13159
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4002623")]
		private bool m_IsQuestRestart;

		// Token: 0x04003368 RID: 13160
		[Cpp2IlInjected.FieldOffset(Offset = "0x32")]
		[Token(Token = "0x4002624")]
		private bool m_IsLoginBonusCall;

		// Token: 0x04003369 RID: 13161
		[Cpp2IlInjected.FieldOffset(Offset = "0x33")]
		[Token(Token = "0x4002625")]
		private bool m_IsGachaAdvertiseBGMSuspend;

		// Token: 0x0400336A RID: 13162
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4002626")]
		private bool m_IsTutorialBeginnersMission;

		// Token: 0x0400336B RID: 13163
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4002627")]
		private eTitleType[] m_ContentRoomNoticeList;

		// Token: 0x02000882 RID: 2178
		[Token(Token = "0x2000EFF")]
		private enum eStep
		{
			// Token: 0x0400336D RID: 13165
			[Token(Token = "0x40060B8")]
			None = -1,
			// Token: 0x0400336E RID: 13166
			[Token(Token = "0x40060B9")]
			First,
			// Token: 0x0400336F RID: 13167
			[Token(Token = "0x40060BA")]
			LoginBonusCheck,
			// Token: 0x04003370 RID: 13168
			[Token(Token = "0x40060BB")]
			LoginBonusCheckWait,
			// Token: 0x04003371 RID: 13169
			[Token(Token = "0x40060BC")]
			LoginBonusLoadWait,
			// Token: 0x04003372 RID: 13170
			[Token(Token = "0x40060BD")]
			LoginBonusPlayIn,
			// Token: 0x04003373 RID: 13171
			[Token(Token = "0x40060BE")]
			LoginBonusWait,
			// Token: 0x04003374 RID: 13172
			[Token(Token = "0x40060BF")]
			UnloadLoginBonusSceneWait_0,
			// Token: 0x04003375 RID: 13173
			[Token(Token = "0x40060C0")]
			UnloadLoginBonusSceneWait_1,
			// Token: 0x04003376 RID: 13174
			[Token(Token = "0x40060C1")]
			GachaAdvertiseCheck,
			// Token: 0x04003377 RID: 13175
			[Token(Token = "0x40060C2")]
			GachaAdvertisePlay,
			// Token: 0x04003378 RID: 13176
			[Token(Token = "0x40060C3")]
			GachaAdvertisePlay2,
			// Token: 0x04003379 RID: 13177
			[Token(Token = "0x40060C4")]
			GachaAdvertiseWait,
			// Token: 0x0400337A RID: 13178
			[Token(Token = "0x40060C5")]
			GachaAdvertiseEnd,
			// Token: 0x0400337B RID: 13179
			[Token(Token = "0x40060C6")]
			InfoBannerRequest,
			// Token: 0x0400337C RID: 13180
			[Token(Token = "0x40060C7")]
			InfoBannerRequestWait,
			// Token: 0x0400337D RID: 13181
			[Token(Token = "0x40060C8")]
			InfoBannerLoadWait,
			// Token: 0x0400337E RID: 13182
			[Token(Token = "0x40060C9")]
			EventBannerRequest,
			// Token: 0x0400337F RID: 13183
			[Token(Token = "0x40060CA")]
			EventBannerRequestWait,
			// Token: 0x04003380 RID: 13184
			[Token(Token = "0x40060CB")]
			HomePlayIn,
			// Token: 0x04003381 RID: 13185
			[Token(Token = "0x40060CC")]
			WaitLoadOpen,
			// Token: 0x04003382 RID: 13186
			[Token(Token = "0x40060CD")]
			HomeSwitch,
			// Token: 0x04003383 RID: 13187
			[Token(Token = "0x40060CE")]
			HomeSwitchWait,
			// Token: 0x04003384 RID: 13188
			[Token(Token = "0x40060CF")]
			InfoWait,
			// Token: 0x04003385 RID: 13189
			[Token(Token = "0x40060D0")]
			Unlock,
			// Token: 0x04003386 RID: 13190
			[Token(Token = "0x40060D1")]
			RemindPremium,
			// Token: 0x04003387 RID: 13191
			[Token(Token = "0x40060D2")]
			RemindPremiumWait,
			// Token: 0x04003388 RID: 13192
			[Token(Token = "0x40060D3")]
			RemindPremiumShown,
			// Token: 0x04003389 RID: 13193
			[Token(Token = "0x40060D4")]
			RemindPremiumShownWait,
			// Token: 0x0400338A RID: 13194
			[Token(Token = "0x40060D5")]
			HomeMain
		}
	}
}
