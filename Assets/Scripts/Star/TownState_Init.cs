﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000884 RID: 2180
	[Token(Token = "0x2000653")]
	[StructLayout(3)]
	public class TownState_Init : TownState
	{
		// Token: 0x0600234A RID: 9034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002099")]
		[Address(RVA = "0x1013B75D0", Offset = "0x13B75D0", VA = "0x1013B75D0")]
		public TownState_Init(TownMain owner)
		{
		}

		// Token: 0x0600234B RID: 9035 RVA: 0x0000F210 File Offset: 0x0000D410
		[Token(Token = "0x600209A")]
		[Address(RVA = "0x1013B7604", Offset = "0x13B7604", VA = "0x1013B7604", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600234C RID: 9036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600209B")]
		[Address(RVA = "0x1013B760C", Offset = "0x13B760C", VA = "0x1013B760C", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600234D RID: 9037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600209C")]
		[Address(RVA = "0x1013B7614", Offset = "0x13B7614", VA = "0x1013B7614", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600234E RID: 9038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600209D")]
		[Address(RVA = "0x1013B7618", Offset = "0x13B7618", VA = "0x1013B7618", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600234F RID: 9039 RVA: 0x0000F228 File Offset: 0x0000D428
		[Token(Token = "0x600209E")]
		[Address(RVA = "0x1013B761C", Offset = "0x13B761C", VA = "0x1013B761C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002350 RID: 9040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600209F")]
		[Address(RVA = "0x1013B7F90", Offset = "0x13B7F90", VA = "0x1013B7F90", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x0400338E RID: 13198
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002628")]
		private TownState_Init.eStep m_Step;

		// Token: 0x02000885 RID: 2181
		[Token(Token = "0x2000F01")]
		private enum eStep
		{
			// Token: 0x04003390 RID: 13200
			[Token(Token = "0x40060DA")]
			None = -1,
			// Token: 0x04003391 RID: 13201
			[Token(Token = "0x40060DB")]
			First,
			// Token: 0x04003392 RID: 13202
			[Token(Token = "0x40060DC")]
			TownUIPrepare_Start,
			// Token: 0x04003393 RID: 13203
			[Token(Token = "0x40060DD")]
			TownUIPrepare_Processing,
			// Token: 0x04003394 RID: 13204
			[Token(Token = "0x40060DE")]
			HomeUIPrepare_Start,
			// Token: 0x04003395 RID: 13205
			[Token(Token = "0x40060DF")]
			HomeUIPrepare_Processing,
			// Token: 0x04003396 RID: 13206
			[Token(Token = "0x40060E0")]
			BuilderPrepare_Start,
			// Token: 0x04003397 RID: 13207
			[Token(Token = "0x40060E1")]
			BuilderPrepare_Processing,
			// Token: 0x04003398 RID: 13208
			[Token(Token = "0x40060E2")]
			MissionUIPrepare_Start,
			// Token: 0x04003399 RID: 13209
			[Token(Token = "0x40060E3")]
			MissionUIPrepare_Processing,
			// Token: 0x0400339A RID: 13210
			[Token(Token = "0x40060E4")]
			CharaIllustDownload,
			// Token: 0x0400339B RID: 13211
			[Token(Token = "0x40060E5")]
			CharaIllustDownloadWait,
			// Token: 0x0400339C RID: 13212
			[Token(Token = "0x40060E6")]
			TownUIStart,
			// Token: 0x0400339D RID: 13213
			[Token(Token = "0x40060E7")]
			End
		}
	}
}
