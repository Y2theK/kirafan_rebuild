﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000886 RID: 2182
	[Token(Token = "0x2000654")]
	[StructLayout(3)]
	public class TownState_Main : TownState
	{
		// Token: 0x06002351 RID: 9041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A0")]
		[Address(RVA = "0x1013B7F94", Offset = "0x13B7F94", VA = "0x1013B7F94")]
		public TownState_Main(TownMain owner)
		{
		}

		// Token: 0x06002352 RID: 9042 RVA: 0x0000F240 File Offset: 0x0000D440
		[Token(Token = "0x60020A1")]
		[Address(RVA = "0x1013B7FC8", Offset = "0x13B7FC8", VA = "0x1013B7FC8", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002353 RID: 9043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A2")]
		[Address(RVA = "0x1013B7FD0", Offset = "0x13B7FD0", VA = "0x1013B7FD0", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002354 RID: 9044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A3")]
		[Address(RVA = "0x1013B7FD8", Offset = "0x13B7FD8", VA = "0x1013B7FD8", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002355 RID: 9045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A4")]
		[Address(RVA = "0x1013B8008", Offset = "0x13B8008", VA = "0x1013B8008", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002356 RID: 9046 RVA: 0x0000F258 File Offset: 0x0000D458
		[Token(Token = "0x60020A5")]
		[Address(RVA = "0x1013B800C", Offset = "0x13B800C", VA = "0x1013B800C", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002357 RID: 9047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A6")]
		[Address(RVA = "0x1013B83BC", Offset = "0x13B83BC", VA = "0x1013B83BC", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06002358 RID: 9048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A7")]
		[Address(RVA = "0x1013B8510", Offset = "0x13B8510", VA = "0x1013B8510")]
		public void OnTransitCallBack()
		{
		}

		// Token: 0x06002359 RID: 9049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A8")]
		[Address(RVA = "0x1013B84AC", Offset = "0x13B84AC", VA = "0x1013B84AC")]
		private void GoToMenuEnd()
		{
		}

		// Token: 0x0400339E RID: 13214
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002629")]
		private TownState_Main.eStep m_Step;

		// Token: 0x02000887 RID: 2183
		[Token(Token = "0x2000F02")]
		private enum eStep
		{
			// Token: 0x040033A0 RID: 13216
			[Token(Token = "0x40060E9")]
			None = -1,
			// Token: 0x040033A1 RID: 13217
			[Token(Token = "0x40060EA")]
			First,
			// Token: 0x040033A2 RID: 13218
			[Token(Token = "0x40060EB")]
			Main,
			// Token: 0x040033A3 RID: 13219
			[Token(Token = "0x40060EC")]
			End
		}
	}
}
