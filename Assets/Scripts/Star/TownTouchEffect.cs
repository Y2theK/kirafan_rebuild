﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B28 RID: 2856
	[Token(Token = "0x20007CF")]
	[StructLayout(3)]
	public class TownTouchEffect : MonoBehaviour
	{
		// Token: 0x0600323B RID: 12859 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6002DEE")]
		[Address(RVA = "0x1013B8514", Offset = "0x13B8514", VA = "0x1013B8514")]
		public static TownTouchEffect CreateTouchEffect()
		{
			return null;
		}

		// Token: 0x0600323C RID: 12860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DEF")]
		[Address(RVA = "0x1013B8594", Offset = "0x13B8594", VA = "0x1013B8594")]
		public void TouchFirst(Vector2 fpos)
		{
		}

		// Token: 0x0600323D RID: 12861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF0")]
		[Address(RVA = "0x1013B86E4", Offset = "0x13B86E4", VA = "0x1013B86E4")]
		public void TouchUp(Vector2 fpos)
		{
		}

		// Token: 0x0600323E RID: 12862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF1")]
		[Address(RVA = "0x1013B8960", Offset = "0x13B8960", VA = "0x1013B8960")]
		private void Update()
		{
		}

		// Token: 0x0600323F RID: 12863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF2")]
		[Address(RVA = "0x1013B8964", Offset = "0x13B8964", VA = "0x1013B8964")]
		public void DestoryRequest()
		{
		}

		// Token: 0x06003240 RID: 12864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF3")]
		[Address(RVA = "0x1013B89DC", Offset = "0x13B89DC", VA = "0x1013B89DC")]
		public TownTouchEffect()
		{
		}

		// Token: 0x040041DD RID: 16861
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EC4")]
		private Vector2 m_TouchPos;

		// Token: 0x040041DE RID: 16862
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EC5")]
		private Vector2 m_TouchBack;
	}
}
