﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B29 RID: 2857
	[Token(Token = "0x20007D0")]
	[StructLayout(3)]
	public class TownTouchLineEffect : MonoBehaviour
	{
		// Token: 0x06003241 RID: 12865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF4")]
		[Address(RVA = "0x1013B894C", Offset = "0x13B894C", VA = "0x1013B894C")]
		public void TouchPos(Vector2 fpos, Vector2 ftarget)
		{
		}

		// Token: 0x06003242 RID: 12866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF5")]
		[Address(RVA = "0x1013B89E4", Offset = "0x13B89E4", VA = "0x1013B89E4")]
		private void LateUpdate()
		{
		}

		// Token: 0x06003243 RID: 12867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6002DF6")]
		[Address(RVA = "0x1013B8DBC", Offset = "0x13B8DBC", VA = "0x1013B8DBC")]
		public TownTouchLineEffect()
		{
		}

		// Token: 0x040041DF RID: 16863
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4002EC6")]
		private Vector2 m_TouchSt;

		// Token: 0x040041E0 RID: 16864
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4002EC7")]
		private Vector2 m_TouchEnd;
	}
}
