﻿using UnityEngine;

namespace Star {
    public static class TownUtility {
        public static string GetResourceNoName(int objID, int flevel, int ftimes = 0) {
            return string.Format("{0:0000}{1:00}", objID, flevel - 1);
        }

        public static string GetModelResourcePath(eResCategory ftype, int objID, int ftime) {
            switch (ftype) {
                case eResCategory.Field:
                    return string.Format("Prefab/Town/Field/BG_town_{0:00}_{1}", objID, ftime);
                case eResCategory.Room:
                case eResCategory.Buf:
                case eResCategory.MenuBuf:
                case eResCategory.Content:
                    return string.Format("Prefab/Town/Building/BLD_{0:000000}_{1}", objID, ftime);
                case eResCategory.Area:
                case eResCategory.AreaFree:
                    return string.Format("Prefab/Town/Area/AREA_{0:000000}_{1}", objID, 0);
                default:
                    return "Prefab/Town/Option/AreaSelectPoint";
            }
        }

        public static bool IsModelResourcePath(eResCategory ftype, int objID) {
            switch (ftype) {
                case eResCategory.Field:
                case eResCategory.Room:
                case eResCategory.Buf:
                case eResCategory.MenuBuf:
                case eResCategory.Content:
                    return true;
                default:
                    return false;
            }
        }

        public static int GetTownObjectLevelUpConfirm(int objID, int targetLv, TownDefine.eTownLevelUpConditionCategory category) {
            TownObjectListDB_Param param = GameSystem.Inst.DbMng.TownObjListDB.GetParam(objID);
            TownObjectLevelUpDB_Param levelUpParam = GameSystem.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, targetLv);
            int value = 0;
            switch (category) {
                case TownDefine.eTownLevelUpConditionCategory.Gold:
                    value = levelUpParam.m_GoldAmount;
                    break;
                case TownDefine.eTownLevelUpConditionCategory.Kirara:
                    value = levelUpParam.m_KiraraPoint;
                    break;
                case TownDefine.eTownLevelUpConditionCategory.UserLv:
                    return levelUpParam.m_UserLv;
                case TownDefine.eTownLevelUpConditionCategory.BuildTime:
                    value = levelUpParam.m_BuildTime;
                    break;
            }
            value += CalcUsePlayerResource(category, levelUpParam.m_TownUseFunc, levelUpParam.m_TownActionStepKey);
            return value;
        }

        private static int CalcUsePlayerResource(TownDefine.eTownLevelUpConditionCategory category, int funckey, int fsubkey) {
            if (funckey == 1) {
                UserTownData townData = GameSystem.Inst.UserDataMng.UserTownData;
                int idx = 0;
                int dataNum = townData.GetBuildObjectDataNum();
                for (int i = 0; i < dataNum; i++) {
                    UserTownData.BuildObjectData data = townData.GetBuildObjectDataAt(i);
                    if (data.m_BuildPointIndex != 0 && GetPointToBuildType(data.m_BuildPointIndex) == 0) {
                        idx++;
                    }
                }
                TownObjectBuildSubCodeDB objSubDB = GameSystem.Inst.DbMng.TownObjBuildSubDB;
                int paramNum = objSubDB.m_Params.Length;
                for (int i = 0; i < paramNum; i++) {
                    TownObjectBuildSubCodeDB_Param param = objSubDB.m_Params[i];
                    if (category == TownDefine.eTownLevelUpConditionCategory.Gold) {
                        if (param.m_ID == idx + fsubkey) {
                            if (param.m_ExtentionScriptID == 0) {
                                return param.m_ExtentionFunKey;
                            }
                        }
                    }
                }
            }
            return 0;
        }

        public static int GetBuildObjNum(int townObjID) {
            int total = 0;
            UserTownData townData = GameSystem.Inst.UserDataMng.UserTownData;
            int dataNum = townData.GetBuildObjectDataNum();
            for (int i = 0; i < dataNum; i++) {
                UserTownData.BuildObjectData data = townData.GetBuildObjectDataAt(i);
                if (data.m_ObjID == townObjID) {
                    total++;
                }
            }
            return total;
        }

        public static int SyncFixKey(int fbaseid) {
            return fbaseid | TownDefine.AREA_BUFID;
        }

        public static int MakeBuildAreaID(int fbaseid, int fsubid = 0) {
            return (fbaseid & 0xFFF) | fsubid << 16 | TownDefine.AREA_CTXID;
        }

        public static int GetBuildAreaID(int fbuildpoint) {
            return fbuildpoint & 0xFFF;
        }

        public static int ChangeBuildAreaID(int fbuildpoint, int fareaid) {
            return (int)(fbuildpoint & 0xFFFFF000) | fareaid;
        }

        public static int MakeBuildPointID(int fbaseid, int fsubid) {
            return (fbaseid & 0xFFF) | fsubid << 16 | TownDefine.AREA_BUFID;
        }

        public static int MakeBuildContentID(int fbaseid) {
            return (fbaseid & 0xFFF) | TownDefine.AREA_CONTENTID;
        }

        public static int MakeMenuPointID(int fbaseid) {
            return fbaseid | TownDefine.MENU_BUFID;
        }

        public static eTownMenuType GetMenuPointEnum(int fbaseid) {
            return (eTownMenuType)(fbaseid & 0xFFF);
        }

        public static int GetPointToBuildType(int fpoint) {
            return fpoint & TownDefine.TYPE_MASKID;
        }

        public static eTownObjectCategory GetResourceCategory(int fid) {
            return (eTownObjectCategory)GameSystem.Inst.DbMng.TownObjListDB.GetParam(fid).m_Category;
        }

        public static Transform FindTransform(Transform ptarget, string findname, int fchkcall) {
            if (ptarget.name == findname) {
                return ptarget;
            }
            if (fchkcall <= 0) {
                return null;
            }
            int childCount = ptarget.childCount;
            for (int i = 0; i < childCount; i++) {
                Transform transform = FindTransform(ptarget.GetChild(i), findname, fchkcall - 1);
                if (transform != null) {
                    return transform;
                }
            }
            return null;
        }

        public static bool IsRemovableObj(int objID) {
            int cat = GameSystem.Inst.DbMng.TownObjListDB.GetParam(objID).m_Category;
            switch ((eTownObjectCategory)cat) {
                case eTownObjectCategory.Item:
                case eTownObjectCategory.Money:
                case eTownObjectCategory.Buf:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsMovableObj(int objID) {
            int cat = GameSystem.Inst.DbMng.TownObjListDB.GetParam(objID).m_Category;
            switch ((eTownObjectCategory)cat) {
                case eTownObjectCategory.Item:
                case eTownObjectCategory.Money:
                case eTownObjectCategory.Buf:
                case eTownObjectCategory.Contents:
                    return true;
                default:
                    return false;
            }
        }

        public static int CalcAreaToContentID(int objID) {
            TownObjectListDB objList = GameSystem.Inst.DbMng.TownObjListDB;
            TownObjectListDB_Param param = objList.GetParam(objID);
            int num = objList.m_Params.Length;
            for (int i = 0; i < num; i++) {
                TownObjectListDB_Param curParam = objList.m_Params[i];
                if (curParam.m_TitleType == param.m_TitleType && curParam.m_Category == (int)eTownObjectCategory.Contents) {
                    return GameSystem.Inst.DbMng.TownObjListDB.m_Params[i].m_ID;
                }
            }
            return param.m_ID;
        }

        public static int GetObjectModelLinkLevel(UserTownData.BuildObjectData pbuildobj, int fobjid) {
            TownObjectListDB_Param param = GameSystem.Inst.DbMng.TownObjListDB.GetParam(fobjid);
            int result = 1;
            if ((param.m_Arg & 1) == 0) {
                result += pbuildobj.m_Lv;
            }
            return result;
        }

        public static void SetRenderLayer(GameObject pobj, int flayer) {
            Renderer[] renderers = pobj.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; i++) {
                renderers[i].sortingOrder = flayer;
            }
        }

        public static eSoundBgmListDB GetTownBgmID(ePlayBGMState state) {
            eSoundBgmListDB bgm = GameSystem.Inst.DbMng.SoundHomeBgmListDB.GetCueID(GameSystem.Inst.ServerTime, state);
            if (bgm == eSoundBgmListDB.None) {
                ScheduleTimeUtil.SetMarkingTime();
                int hour = ScheduleTimeUtil.GetManageUnixTime().Hour;
                if (hour >= 18 || hour < 6) {
                    bgm = eSoundBgmListDB.BGM_TOWN_3;
                } else if (hour >= 6 && hour < 13) {
                    bgm = eSoundBgmListDB.BGM_TOWN_1;
                } else {
                    bgm = eSoundBgmListDB.BGM_TOWN_2;
                }
            }
            return bgm;
        }

        public enum eResCategory {
            Field,
            Room,
            Area,
            Buf,
            AreaFree,
            BufFree,
            MenuBuf,
            Content,
            Chara,
            MenuChara
        }
    }
}
