﻿using UnityEngine;

namespace Star {
    public class TrailHandler : EffectHandlerBase {
        [SerializeField] private TrailRenderer[] m_TrailRenders;

        public override void OnDestroyToCache() { }

        public void Setup(Color[] colors, float startWidth, float endWidth, float time) {
            for (int i = 0; i < m_TrailRenders.Length; i++) {
                m_TrailRenders[i].Clear();
            }
            Gradient gradient = new Gradient();
            if (colors != null && colors.Length >= 2) {
                int numColors = colors.Length;
                float tIncrement = 1f / (numColors - 1);
                GradientColorKey[] color = new GradientColorKey[numColors];
                GradientAlphaKey[] alpha = new GradientAlphaKey[numColors];
                for (int i = 0; i < numColors; i++) {
                    float t;
                    if (i == 0) {
                        t = 1f;
                    } else if (i == numColors - 1) {
                        t = 0f;
                    } else {
                        t = 1f - i * tIncrement;
                    }
                    color[i].color = new Color(colors[i].r, colors[i].g, colors[i].b, 1f);
                    color[i].time = t;
                    alpha[i].alpha = colors[i].a;
                    alpha[i].time = t;
                }
                gradient.SetKeys(color, alpha);
            }
            for (int i = 0; i < m_TrailRenders.Length; i++) {
                m_TrailRenders[i].colorGradient = gradient;
                m_TrailRenders[i].startWidth = startWidth;
                m_TrailRenders[i].endWidth = endWidth;
                m_TrailRenders[i].time = time;
            }
        }

        public void Setup(eElementType elementType) {
            for (int i = 0; i < m_TrailRenders.Length; i++) {
                m_TrailRenders[i].enabled = i == (int)elementType;
            }
        }

        public void SetSortingOrder(int sortingOrder) {
            for (int i = 0; i < m_TrailRenders.Length; i++) {
                m_TrailRenders[i].sortingOrder = sortingOrder;
            }
        }
    }
}
