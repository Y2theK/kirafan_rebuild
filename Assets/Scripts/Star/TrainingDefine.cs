﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BC0 RID: 3008
	[Token(Token = "0x200082A")]
	[StructLayout(3)]
	public static class TrainingDefine
	{
		// Token: 0x040044BF RID: 17599
		[Token(Token = "0x40030B1")]
		public const int TRAINING_PARTY_MAX = 4;

		// Token: 0x040044C0 RID: 17600
		[Token(Token = "0x40030B2")]
		public const int CATEGORY_DIFFICULT_MIN = 1;

		// Token: 0x040044C1 RID: 17601
		[Token(Token = "0x40030B3")]
		public const int CATEGORY_DIFFICULT_MAX = 5;

		// Token: 0x02000BC1 RID: 3009
		[Token(Token = "0x2001063")]
		public enum eCostType
		{
			// Token: 0x040044C3 RID: 17603
			[Token(Token = "0x400677B")]
			Gold,
			// Token: 0x040044C4 RID: 17604
			[Token(Token = "0x400677C")]
			KRRPoint,
			// Token: 0x040044C5 RID: 17605
			[Token(Token = "0x400677D")]
			Num
		}

		// Token: 0x02000BC2 RID: 3010
		[Token(Token = "0x2001064")]
		public enum eRewardCurrencyType
		{
			// Token: 0x040044C7 RID: 17607
			[Token(Token = "0x400677F")]
			Gold,
			// Token: 0x040044C8 RID: 17608
			[Token(Token = "0x4006780")]
			KRRPoint,
			// Token: 0x040044C9 RID: 17609
			[Token(Token = "0x4006781")]
			Num
		}

		// Token: 0x02000BC3 RID: 3011
		[Token(Token = "0x2001065")]
		public enum eOrderCheck
		{
			// Token: 0x040044CB RID: 17611
			[Token(Token = "0x4006783")]
			Success,
			// Token: 0x040044CC RID: 17612
			[Token(Token = "0x4006784")]
			Failed_EmptyCostGold,
			// Token: 0x040044CD RID: 17613
			[Token(Token = "0x4006785")]
			Failed_EmptyCostKRRPoint,
			// Token: 0x040044CE RID: 17614
			[Token(Token = "0x4006786")]
			Failed_CharaLv,
			// Token: 0x040044CF RID: 17615
			[Token(Token = "0x4006787")]
			Failed_PartyNum,
			// Token: 0x040044D0 RID: 17616
			[Token(Token = "0x4006788")]
			Failed_ExistSameNamedTypeOtherSlot,
			// Token: 0x040044D1 RID: 17617
			[Token(Token = "0x4006789")]
			Failed_ExistSameNamedTypeCurrentSlot,
			// Token: 0x040044D2 RID: 17618
			[Token(Token = "0x400678A")]
			Num
		}

		// Token: 0x02000BC4 RID: 3012
		[Token(Token = "0x2001066")]
		public enum eOrderResult
		{
			// Token: 0x040044D4 RID: 17620
			[Token(Token = "0x400678C")]
			Success,
			// Token: 0x040044D5 RID: 17621
			[Token(Token = "0x400678D")]
			OutOfPeriod
		}

		// Token: 0x02000BC5 RID: 3013
		[Token(Token = "0x2001067")]
		public enum eCompleteResult
		{
			// Token: 0x040044D7 RID: 17623
			[Token(Token = "0x400678F")]
			Success,
			// Token: 0x040044D8 RID: 17624
			[Token(Token = "0x4006790")]
			TimeError,
			// Token: 0x040044D9 RID: 17625
			[Token(Token = "0x4006791")]
			AlreadyCompleted
		}

		// Token: 0x02000BC6 RID: 3014
		[Token(Token = "0x2001068")]
		public enum eCancelResult
		{
			// Token: 0x040044DB RID: 17627
			[Token(Token = "0x4006793")]
			Success,
			// Token: 0x040044DC RID: 17628
			[Token(Token = "0x4006794")]
			AlreadyCompleted
		}
	}
}
