﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000888 RID: 2184
	[Token(Token = "0x2000655")]
	[StructLayout(3)]
	public class TrainingMain : GameStateMain
	{
		// Token: 0x0600235A RID: 9050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020A9")]
		[Address(RVA = "0x1013B9EF8", Offset = "0x13B9EF8", VA = "0x1013B9EF8")]
		private void Start()
		{
		}

		// Token: 0x0600235B RID: 9051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020AA")]
		[Address(RVA = "0x1013B9F04", Offset = "0x13B9F04", VA = "0x1013B9F04")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600235C RID: 9052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020AB")]
		[Address(RVA = "0x1013B9F08", Offset = "0x13B9F08", VA = "0x1013B9F08", Slot = "4")]
		protected override void Update()
		{
		}

		// Token: 0x0600235D RID: 9053 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60020AC")]
		[Address(RVA = "0x1013B9F10", Offset = "0x13B9F10", VA = "0x1013B9F10", Slot = "5")]
		public override GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x0600235E RID: 9054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020AD")]
		[Address(RVA = "0x1013BA098", Offset = "0x13BA098", VA = "0x1013BA098", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x0600235F RID: 9055 RVA: 0x0000F270 File Offset: 0x0000D470
		[Token(Token = "0x60020AE")]
		[Address(RVA = "0x1013BA114", Offset = "0x13BA114", VA = "0x1013BA114", Slot = "7")]
		public override bool IsCompleteDestroy()
		{
			return default(bool);
		}

		// Token: 0x06002360 RID: 9056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020AF")]
		[Address(RVA = "0x1013BA11C", Offset = "0x13BA11C", VA = "0x1013BA11C")]
		public TrainingMain()
		{
		}

		// Token: 0x040033A4 RID: 13220
		[Token(Token = "0x400262A")]
		public const int STATE_INIT = 0;

		// Token: 0x040033A5 RID: 13221
		[Token(Token = "0x400262B")]
		public const int STATE_MAIN = 1;
	}
}
