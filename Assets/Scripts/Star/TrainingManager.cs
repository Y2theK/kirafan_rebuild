﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Training;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000BC7 RID: 3015
	[Token(Token = "0x200082B")]
	[StructLayout(3)]
	public sealed class TrainingManager
	{
		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x060034A3 RID: 13475 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060034A4 RID: 13476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003A8")]
		public TrainingUI TrainingUI
		{
			[Token(Token = "0x6003001")]
			[Address(RVA = "0x1013BA124", Offset = "0x13BA124", VA = "0x1013BA124")]
			get
			{
				return null;
			}
			[Token(Token = "0x6003002")]
			[Address(RVA = "0x1013BA12C", Offset = "0x13BA12C", VA = "0x1013BA12C")]
			set
			{
			}
		}

		// Token: 0x060034A5 RID: 13477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003003")]
		[Address(RVA = "0x1013BA134", Offset = "0x13BA134", VA = "0x1013BA134")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x060034A6 RID: 13478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003004")]
		[Address(RVA = "0x1013BA354", Offset = "0x13BA354", VA = "0x1013BA354")]
		public void Update()
		{
		}

		// Token: 0x060034A7 RID: 13479 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003005")]
		[Address(RVA = "0x1013BA4B0", Offset = "0x13BA4B0", VA = "0x1013BA4B0")]
		public List<TrainingManager.SlotData> GetSlotList()
		{
			return null;
		}

		// Token: 0x060034A8 RID: 13480 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003006")]
		[Address(RVA = "0x1013BA4B8", Offset = "0x13BA4B8", VA = "0x1013BA4B8")]
		public TrainingManager.SlotData GetSlotData(int slotID)
		{
			return null;
		}

		// Token: 0x060034A9 RID: 13481 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003007")]
		[Address(RVA = "0x1013BA5B8", Offset = "0x13BA5B8", VA = "0x1013BA5B8")]
		public TrainingManager.SlotData GetSlotDataFromOrderMngID(long orderMngID)
		{
			return null;
		}

		// Token: 0x060034AA RID: 13482 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003008")]
		[Address(RVA = "0x1013BA6B8", Offset = "0x13BA6B8", VA = "0x1013BA6B8")]
		public TrainingManager.SlotData GetSlotDataAt(int index)
		{
			return null;
		}

		// Token: 0x060034AB RID: 13483 RVA: 0x00016458 File Offset: 0x00014658
		[Token(Token = "0x6003009")]
		[Address(RVA = "0x1013BA76C", Offset = "0x13BA76C", VA = "0x1013BA76C")]
		public int GetSlotListIndexFromID(int slotID)
		{
			return 0;
		}

		// Token: 0x060034AC RID: 13484 RVA: 0x00016470 File Offset: 0x00014670
		[Token(Token = "0x600300A")]
		[Address(RVA = "0x1013BA840", Offset = "0x13BA840", VA = "0x1013BA840")]
		public int GetTrainingPartyIndexFromMngID(long mngID, int excludeIndex = -1)
		{
			return 0;
		}

		// Token: 0x060034AD RID: 13485 RVA: 0x00016488 File Offset: 0x00014688
		[Token(Token = "0x600300B")]
		[Address(RVA = "0x1013BAB60", Offset = "0x13BAB60", VA = "0x1013BAB60")]
		public int GetTrainingSlotIDFromMngID(long mngID, int excludeSlotId = -1)
		{
			return 0;
		}

		// Token: 0x060034AE RID: 13486 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600300C")]
		[Address(RVA = "0x1013BACC0", Offset = "0x13BACC0", VA = "0x1013BACC0")]
		public List<long>[] GetAllSlotCharaIDs(int excludeIndex)
		{
			return null;
		}

		// Token: 0x060034AF RID: 13487 RVA: 0x000164A0 File Offset: 0x000146A0
		[Token(Token = "0x600300D")]
		[Address(RVA = "0x1013BAF48", Offset = "0x13BAF48", VA = "0x1013BAF48")]
		public bool IsExistCompleteSlot()
		{
			return default(bool);
		}

		// Token: 0x060034B0 RID: 13488 RVA: 0x000164B8 File Offset: 0x000146B8
		[Token(Token = "0x600300E")]
		[Address(RVA = "0x1013BB14C", Offset = "0x13BB14C", VA = "0x1013BB14C")]
		public bool IsExistEmptySlot()
		{
			return default(bool);
		}

		// Token: 0x060034B1 RID: 13489 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600300F")]
		[Address(RVA = "0x1013BB310", Offset = "0x13BB310", VA = "0x1013BB310")]
		public List<TrainingManager.TrainingData> GetTrainingList()
		{
			return null;
		}

		// Token: 0x060034B2 RID: 13490 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003010")]
		[Address(RVA = "0x1013BB318", Offset = "0x13BB318", VA = "0x1013BB318")]
		public TrainingManager.TrainingData GetTrainingData(int trainingID)
		{
			return null;
		}

		// Token: 0x060034B3 RID: 13491 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003011")]
		[Address(RVA = "0x1013BB454", Offset = "0x13BB454", VA = "0x1013BB454")]
		public TrainingManager.CompletedData GetCompletedData(int trainingID)
		{
			return null;
		}

		// Token: 0x060034B4 RID: 13492 RVA: 0x000164D0 File Offset: 0x000146D0
		[Token(Token = "0x6003012")]
		[Address(RVA = "0x1013BB554", Offset = "0x13BB554", VA = "0x1013BB554")]
		public int GetResultCount()
		{
			return 0;
		}

		// Token: 0x060034B5 RID: 13493 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003013")]
		[Address(RVA = "0x1013BB5B4", Offset = "0x13BB5B4", VA = "0x1013BB5B4")]
		public TrainingManager.OrderResult GetResult(int index)
		{
			return null;
		}

		// Token: 0x060034B6 RID: 13494 RVA: 0x000164E8 File Offset: 0x000146E8
		[Token(Token = "0x6003014")]
		[Address(RVA = "0x1013BB624", Offset = "0x13BB624", VA = "0x1013BB624")]
		public long GetResultOverKRRPoint()
		{
			return 0L;
		}

		// Token: 0x060034B7 RID: 13495 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003015")]
		[Address(RVA = "0x1013BB62C", Offset = "0x13BB62C", VA = "0x1013BB62C")]
		public List<int> GetResultBeforeLvs()
		{
			return null;
		}

		// Token: 0x060034B8 RID: 13496 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003016")]
		[Address(RVA = "0x1013BB940", Offset = "0x13BB940", VA = "0x1013BB940")]
		public List<UserCharacterData> GetResultUserCharaDatas()
		{
			return null;
		}

		// Token: 0x1400005B RID: 91
		// (add) Token: 0x060034B9 RID: 13497 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034BA RID: 13498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005B")]
		private event Action m_OnResponse_GetList
		{
			[Token(Token = "0x6003017")]
			[Address(RVA = "0x1013BBC74", Offset = "0x13BBC74", VA = "0x1013BBC74")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003018")]
			[Address(RVA = "0x1013BBD80", Offset = "0x13BBD80", VA = "0x1013BBD80")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400005C RID: 92
		// (add) Token: 0x060034BB RID: 13499 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034BC RID: 13500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005C")]
		private event Action<TrainingDefine.eOrderResult, string> m_OnResponse_Order
		{
			[Token(Token = "0x6003019")]
			[Address(RVA = "0x1013BBE8C", Offset = "0x13BBE8C", VA = "0x1013BBE8C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600301A")]
			[Address(RVA = "0x1013BBF98", Offset = "0x13BBF98", VA = "0x1013BBF98")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400005D RID: 93
		// (add) Token: 0x060034BD RID: 13501 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034BE RID: 13502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005D")]
		private event Action<TrainingDefine.eCompleteResult, string> m_OnResponse_Complete
		{
			[Token(Token = "0x600301B")]
			[Address(RVA = "0x1013BC0A4", Offset = "0x13BC0A4", VA = "0x1013BC0A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600301C")]
			[Address(RVA = "0x1013BC1B0", Offset = "0x13BC1B0", VA = "0x1013BC1B0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400005E RID: 94
		// (add) Token: 0x060034BF RID: 13503 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034C0 RID: 13504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005E")]
		private event Action<TrainingDefine.eCancelResult, string> m_OnResponse_Cancel
		{
			[Token(Token = "0x600301D")]
			[Address(RVA = "0x1013BC2BC", Offset = "0x13BC2BC", VA = "0x1013BC2BC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600301E")]
			[Address(RVA = "0x1013BC3C8", Offset = "0x13BC3C8", VA = "0x1013BC3C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060034C1 RID: 13505 RVA: 0x00016500 File Offset: 0x00014700
		[Token(Token = "0x600301F")]
		[Address(RVA = "0x1013BC4D4", Offset = "0x13BC4D4", VA = "0x1013BC4D4")]
		public bool Request_GetList(Action onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060034C2 RID: 13506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003020")]
		[Address(RVA = "0x1013BC5D0", Offset = "0x13BC5D0", VA = "0x1013BC5D0")]
		private void OnResponse_GetList(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060034C3 RID: 13507 RVA: 0x00016518 File Offset: 0x00014718
		[Token(Token = "0x6003021")]
		[Address(RVA = "0x1013BC9B4", Offset = "0x13BC9B4", VA = "0x1013BC9B4")]
		public bool Request_Order(List<TrainingManager.TrainingRequestData> requestDataList, Action<TrainingDefine.eOrderResult, string> onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060034C4 RID: 13508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003022")]
		[Address(RVA = "0x1013BCF4C", Offset = "0x13BCF4C", VA = "0x1013BCF4C")]
		private void OnResponse_Order(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060034C5 RID: 13509 RVA: 0x00016530 File Offset: 0x00014730
		[Token(Token = "0x6003023")]
		[Address(RVA = "0x1013BD314", Offset = "0x13BD314", VA = "0x1013BD314")]
		public bool Request_Complete(List<TrainingManager.TrainingCompleteData> dataList, Action<TrainingDefine.eCompleteResult, string> onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060034C6 RID: 13510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003024")]
		[Address(RVA = "0x1013BD7D4", Offset = "0x13BD7D4", VA = "0x1013BD7D4")]
		private void OnResponse_Complete(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060034C7 RID: 13511 RVA: 0x00016548 File Offset: 0x00014748
		[Token(Token = "0x6003025")]
		[Address(RVA = "0x1013BE93C", Offset = "0x13BE93C", VA = "0x1013BE93C")]
		public bool Request_Cancel(long orderMngID, Action<TrainingDefine.eCancelResult, string> onResponse, bool isOpenConnectingIcon = true)
		{
			return default(bool);
		}

		// Token: 0x060034C8 RID: 13512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003026")]
		[Address(RVA = "0x1013BEAF4", Offset = "0x13BEAF4", VA = "0x1013BEAF4")]
		private void OnResponse_Cancel(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060034C9 RID: 13513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003027")]
		[Address(RVA = "0x1013BC6E8", Offset = "0x13BC6E8", VA = "0x1013BC6E8")]
		public static void wwwConvert(TrainingSlotInfo[] src, List<TrainingManager.SlotData> dest)
		{
		}

		// Token: 0x060034CA RID: 13514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003028")]
		[Address(RVA = "0x1013BEDB8", Offset = "0x13BEDB8", VA = "0x1013BEDB8")]
		public static void wwwConvert(TrainingSlotInfo src, TrainingManager.SlotData dest)
		{
		}

		// Token: 0x060034CB RID: 13515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003029")]
		[Address(RVA = "0x1013BC7DC", Offset = "0x13BC7DC", VA = "0x1013BC7DC")]
		public static void wwwConvert(TrainingInfo[] src, List<TrainingManager.TrainingData> dest)
		{
		}

		// Token: 0x060034CC RID: 13516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600302A")]
		[Address(RVA = "0x1013BEFC4", Offset = "0x13BEFC4", VA = "0x1013BEFC4")]
		public static void wwwConvert(TrainingInfo src, TrainingManager.TrainingData dest)
		{
		}

		// Token: 0x060034CD RID: 13517 RVA: 0x00016560 File Offset: 0x00014760
		[Token(Token = "0x600302B")]
		[Address(RVA = "0x1013BC8E4", Offset = "0x13BC8E4", VA = "0x1013BC8E4")]
		public int GetPreCompCount()
		{
			return 0;
		}

		// Token: 0x060034CE RID: 13518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600302C")]
		[Address(RVA = "0x1013BF364", Offset = "0x13BF364", VA = "0x1013BF364")]
		public void LoadUIPrefab()
		{
		}

		// Token: 0x060034CF RID: 13519 RVA: 0x00016578 File Offset: 0x00014778
		[Token(Token = "0x600302D")]
		[Address(RVA = "0x1013BF480", Offset = "0x13BF480", VA = "0x1013BF480")]
		public bool IsLoadedUIPrefab()
		{
			return default(bool);
		}

		// Token: 0x060034D0 RID: 13520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600302E")]
		[Address(RVA = "0x1013BA28C", Offset = "0x13BA28C", VA = "0x1013BA28C")]
		public void ReleaseUI()
		{
		}

		// Token: 0x060034D1 RID: 13521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600302F")]
		[Address(RVA = "0x1013BF4F0", Offset = "0x13BF4F0", VA = "0x1013BF4F0")]
		public GameObject GetUIPrefab()
		{
			return null;
		}

		// Token: 0x060034D2 RID: 13522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003030")]
		[Address(RVA = "0x1013BF4F8", Offset = "0x13BF4F8", VA = "0x1013BF4F8")]
		public TrainingManager()
		{
		}

		// Token: 0x040044DD RID: 17629
		[Token(Token = "0x40030B4")]
		public const int SLOT_CHARA_MAX = 5;

		// Token: 0x040044DE RID: 17630
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40030B5")]
		private readonly string UIPrefabPath;

		// Token: 0x040044DF RID: 17631
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030B6")]
		private TrainingUI m_TrainingUI;

		// Token: 0x040044E0 RID: 17632
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030B7")]
		private GameObject m_UIPrefab;

		// Token: 0x040044E1 RID: 17633
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40030B8")]
		private ABResourceLoader m_Loader;

		// Token: 0x040044E2 RID: 17634
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40030B9")]
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x040044E3 RID: 17635
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40030BA")]
		private List<TrainingManager.SlotData> m_SlotList;

		// Token: 0x040044E4 RID: 17636
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40030BB")]
		private List<TrainingManager.TrainingData> m_TrainingList;

		// Token: 0x040044E5 RID: 17637
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40030BC")]
		private List<TrainingManager.CompletedData> m_CompletedList;

		// Token: 0x040044E6 RID: 17638
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40030BD")]
		private TrainingManager.RequestDataBuffer m_RequestBuffer;

		// Token: 0x040044E7 RID: 17639
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40030BE")]
		private List<TrainingManager.OrderResult> m_ResultList;

		// Token: 0x040044E8 RID: 17640
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40030BF")]
		private long m_ResultOverKRRPoint;

		// Token: 0x02000BC8 RID: 3016
		[Token(Token = "0x2001069")]
		public class SlotData
		{
			// Token: 0x060034D3 RID: 13523 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600615B")]
			[Address(RVA = "0x1013BED0C", Offset = "0x13BED0C", VA = "0x1013BED0C")]
			public SlotData()
			{
			}

			// Token: 0x060034D4 RID: 13524 RVA: 0x00016590 File Offset: 0x00014790
			[Token(Token = "0x600615C")]
			[Address(RVA = "0x1013BAF38", Offset = "0x13BAF38", VA = "0x1013BAF38")]
			public bool IsOrder()
			{
				return default(bool);
			}

			// Token: 0x060034D5 RID: 13525 RVA: 0x000165A8 File Offset: 0x000147A8
			[Token(Token = "0x600615D")]
			[Address(RVA = "0x1013BB07C", Offset = "0x13BB07C", VA = "0x1013BB07C")]
			public bool IsPreComplete()
			{
				return default(bool);
			}

			// Token: 0x060034D6 RID: 13526 RVA: 0x000165C0 File Offset: 0x000147C0
			[Token(Token = "0x600615E")]
			[Address(RVA = "0x1013BF84C", Offset = "0x13BF84C", VA = "0x1013BF84C")]
			public TimeSpan GetRemainAt()
			{
				return default(TimeSpan);
			}

			// Token: 0x060034D7 RID: 13527 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600615F")]
			[Address(RVA = "0x1013BF960", Offset = "0x13BF960", VA = "0x1013BF960")]
			public string GetRemainAtToString()
			{
				return null;
			}

			// Token: 0x060034D8 RID: 13528 RVA: 0x000165D8 File Offset: 0x000147D8
			[Token(Token = "0x6006160")]
			[Address(RVA = "0x1013BA8FC", Offset = "0x13BA8FC", VA = "0x1013BA8FC")]
			public bool checkTrainingMember(long mngID)
			{
				return default(bool);
			}

			// Token: 0x040044ED RID: 17645
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006795")]
			public int m_ID;

			// Token: 0x040044EE RID: 17646
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006796")]
			public bool m_IsOpen;

			// Token: 0x040044EF RID: 17647
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006797")]
			public int m_NeedPlayerLv;

			// Token: 0x040044F0 RID: 17648
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006798")]
			public long m_OrderMngID;

			// Token: 0x040044F1 RID: 17649
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006799")]
			public DateTime m_OrderAt;

			// Token: 0x040044F2 RID: 17650
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400679A")]
			public List<long> m_CharaMngIDs;

			// Token: 0x040044F3 RID: 17651
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400679B")]
			public TrainingManager.TrainingData m_TrainingData;

			// Token: 0x040044F4 RID: 17652
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400679C")]
			public TrainingManager.SlotData.TrainingPosData m_scrlPosBuf;

			// Token: 0x02000BC9 RID: 3017
			[Token(Token = "0x2001349")]
			public class TrainingPosData
			{
				// Token: 0x060034D9 RID: 13529 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006515")]
				[Address(RVA = "0x1013BF768", Offset = "0x13BF768", VA = "0x1013BF768")]
				public TrainingPosData(int trainingID, int tab, float scrollPosGold, float scrollPosKRR)
				{
				}

				// Token: 0x040044F5 RID: 17653
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x4007598")]
				public int m_oldTrainingID;

				// Token: 0x040044F6 RID: 17654
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x4007599")]
				public int m_buffTab;

				// Token: 0x040044F7 RID: 17655
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x400759A")]
				public float[] m_scrollPos;
			}
		}

		// Token: 0x02000BCA RID: 3018
		[Token(Token = "0x200106A")]
		public class TrainingData
		{
			// Token: 0x060034DA RID: 13530 RVA: 0x000165F0 File Offset: 0x000147F0
			[Token(Token = "0x6006161")]
			[Address(RVA = "0x1013BFA80", Offset = "0x13BFA80", VA = "0x1013BFA80")]
			public bool IsLimitTraining()
			{
				return default(bool);
			}

			// Token: 0x060034DB RID: 13531 RVA: 0x00016608 File Offset: 0x00014808
			[Token(Token = "0x6006162")]
			[Address(RVA = "0x1013BFA90", Offset = "0x13BFA90", VA = "0x1013BFA90")]
			public bool IsAvailableSkipGem()
			{
				return default(bool);
			}

			// Token: 0x060034DC RID: 13532 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006163")]
			[Address(RVA = "0x1013BFAA0", Offset = "0x13BFAA0", VA = "0x1013BFAA0")]
			public string GetClearTimeToString()
			{
				return null;
			}

			// Token: 0x060034DD RID: 13533 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006164")]
			[Address(RVA = "0x1013BF308", Offset = "0x13BF308", VA = "0x1013BF308")]
			public TrainingData()
			{
			}

			// Token: 0x040044F8 RID: 17656
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400679D")]
			public int m_TrainingID;

			// Token: 0x040044F9 RID: 17657
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400679E")]
			public string m_TrainingName;

			// Token: 0x040044FA RID: 17658
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400679F")]
			public int m_Category;

			// Token: 0x040044FB RID: 17659
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x40067A0")]
			public int m_OpenRank;

			// Token: 0x040044FC RID: 17660
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40067A1")]
			public int m_LimitNum;

			// Token: 0x040044FD RID: 17661
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x40067A2")]
			public int m_OrderdNum;

			// Token: 0x040044FE RID: 17662
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40067A3")]
			public TrainingDefine.eCostType m_CostType;

			// Token: 0x040044FF RID: 17663
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x40067A4")]
			public int m_Cost;

			// Token: 0x04004500 RID: 17664
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40067A5")]
			public int m_ClearMinute;

			// Token: 0x04004501 RID: 17665
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x40067A6")]
			public int m_SkipGem;

			// Token: 0x04004502 RID: 17666
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40067A7")]
			public int m_CondCharaLv;

			// Token: 0x04004503 RID: 17667
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x40067A8")]
			public int m_CondPartyNum;

			// Token: 0x04004504 RID: 17668
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40067A9")]
			public int m_RewardCharaExp;

			// Token: 0x04004505 RID: 17669
			[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
			[Token(Token = "0x40067AA")]
			public int m_RewardFriendship;

			// Token: 0x04004506 RID: 17670
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x40067AB")]
			public TrainingDefine.eRewardCurrencyType m_RewardCurrencyType;

			// Token: 0x04004507 RID: 17671
			[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
			[Token(Token = "0x40067AC")]
			public int m_RewardCurrencyAmount;

			// Token: 0x04004508 RID: 17672
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x40067AD")]
			public List<TrainingManager.RewardItemData> m_RewardItems;

			// Token: 0x04004509 RID: 17673
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x40067AE")]
			public bool m_IsClear;
		}

		// Token: 0x02000BCB RID: 3019
		[Token(Token = "0x200106B")]
		public class RewardItemData
		{
			// Token: 0x060034DE RID: 13534 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006165")]
			[Address(RVA = "0x1013BF758", Offset = "0x13BF758", VA = "0x1013BF758")]
			private RewardItemData()
			{
			}

			// Token: 0x060034DF RID: 13535 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006166")]
			[Address(RVA = "0x1013BF320", Offset = "0x13BF320", VA = "0x1013BF320")]
			public RewardItemData(int itemID, bool isRare)
			{
			}

			// Token: 0x0400450A RID: 17674
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067AF")]
			public int m_ItemID;

			// Token: 0x0400450B RID: 17675
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40067B0")]
			public bool m_IsRareDisp;
		}

		// Token: 0x02000BCC RID: 3020
		[Token(Token = "0x200106C")]
		public class TrainingRequestData
		{
			// Token: 0x060034E0 RID: 13536 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006167")]
			[Address(RVA = "0x1013BFB98", Offset = "0x13BFB98", VA = "0x1013BFB98")]
			public TrainingRequestData(int slotId, int trainingId, long[] managedCharacterIds)
			{
			}

			// Token: 0x060034E1 RID: 13537 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006168")]
			[Address(RVA = "0x1013BCE98", Offset = "0x13BCE98", VA = "0x1013BCE98")]
			public TrainingOrderParam ConvertData()
			{
				return null;
			}

			// Token: 0x0400450C RID: 17676
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067B1")]
			public int slotId;

			// Token: 0x0400450D RID: 17677
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40067B2")]
			public int trainingId;

			// Token: 0x0400450E RID: 17678
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40067B3")]
			public long[] managedCharacterIds;
		}

		// Token: 0x02000BCD RID: 3021
		[Token(Token = "0x200106D")]
		public class TrainingCompleteData
		{
			// Token: 0x060034E2 RID: 13538 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006169")]
			[Address(RVA = "0x1013BFA44", Offset = "0x13BFA44", VA = "0x1013BFA44")]
			public TrainingCompleteData(long orderId, bool skipGem)
			{
			}

			// Token: 0x060034E3 RID: 13539 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600616A")]
			[Address(RVA = "0x1013BD6F4", Offset = "0x13BD6F4", VA = "0x1013BD6F4")]
			public TrainingCompleteParam ConvertData()
			{
				return null;
			}

			// Token: 0x0400450F RID: 17679
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067B4")]
			public long orderId;

			// Token: 0x04004510 RID: 17680
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40067B5")]
			public int skipGem;
		}

		// Token: 0x02000BCE RID: 3022
		[Token(Token = "0x200106E")]
		public class CompletedData
		{
			// Token: 0x060034E4 RID: 13540 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600616B")]
			[Address(RVA = "0x1013BD790", Offset = "0x13BD790", VA = "0x1013BD790")]
			public CompletedData(int trainingID, string trainingName)
			{
			}

			// Token: 0x04004511 RID: 17681
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067B6")]
			public int m_TrainingID;

			// Token: 0x04004512 RID: 17682
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40067B7")]
			public string m_TrainingName;
		}

		// Token: 0x02000BCF RID: 3023
		[Token(Token = "0x200106F")]
		public class RequestDataBuffer
		{
			// Token: 0x060034E5 RID: 13541 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600616C")]
			[Address(RVA = "0x1013BCD18", Offset = "0x13BCD18", VA = "0x1013BCD18")]
			public void StackSlotDataList(List<TrainingManager.SlotData> slotDataList, List<int> excludeSlotIDs)
			{
			}

			// Token: 0x060034E6 RID: 13542 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600616D")]
			[Address(RVA = "0x1013BD13C", Offset = "0x13BD13C", VA = "0x1013BD13C")]
			public void Copy2SlotDataList(List<TrainingManager.SlotData> slotDataList)
			{
			}

			// Token: 0x060034E7 RID: 13543 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600616E")]
			[Address(RVA = "0x1013BA220", Offset = "0x13BA220", VA = "0x1013BA220")]
			public void Clear()
			{
			}

			// Token: 0x060034E8 RID: 13544 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600616F")]
			[Address(RVA = "0x1013BF60C", Offset = "0x13BF60C", VA = "0x1013BF60C")]
			public RequestDataBuffer()
			{
			}

			// Token: 0x04004513 RID: 17683
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067B8")]
			private Dictionary<int, TrainingManager.SlotData> m_dicSlotData;

			// Token: 0x04004514 RID: 17684
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40067B9")]
			public long m_cancelMngID;
		}

		// Token: 0x02000BD0 RID: 3024
		[Token(Token = "0x2001070")]
		public class OrderResult
		{
			// Token: 0x060034E9 RID: 13545 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006170")]
			[Address(RVA = "0x1013BE88C", Offset = "0x13BE88C", VA = "0x1013BE88C")]
			public OrderResult()
			{
			}

			// Token: 0x060034EA RID: 13546 RVA: 0x00016620 File Offset: 0x00014820
			[Token(Token = "0x6006171")]
			[Address(RVA = "0x1013BF680", Offset = "0x13BF680", VA = "0x1013BF680")]
			public bool IsExistRareDispItem()
			{
				return default(bool);
			}

			// Token: 0x060034EB RID: 13547 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006172")]
			[Address(RVA = "0x1013BB7D8", Offset = "0x13BB7D8", VA = "0x1013BB7D8")]
			public List<int> GetBeforeLvs()
			{
				return null;
			}

			// Token: 0x060034EC RID: 13548 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006173")]
			[Address(RVA = "0x1013BBAEC", Offset = "0x13BBAEC", VA = "0x1013BBAEC")]
			public List<UserCharacterData> GetUserCharaDatas()
			{
				return null;
			}

			// Token: 0x04004515 RID: 17685
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40067BA")]
			public int m_slotID;

			// Token: 0x04004516 RID: 17686
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40067BB")]
			public int m_trainingID;

			// Token: 0x04004517 RID: 17687
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40067BC")]
			public long m_RewardCharaExp;

			// Token: 0x04004518 RID: 17688
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40067BD")]
			public long m_RewardFriendship;

			// Token: 0x04004519 RID: 17689
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40067BE")]
			public long m_RewardGold;

			// Token: 0x0400451A RID: 17690
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40067BF")]
			public long m_RewardKRRPoint;

			// Token: 0x0400451B RID: 17691
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40067C0")]
			public int m_RewardFirstGem;

			// Token: 0x0400451C RID: 17692
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40067C1")]
			public List<TrainingManager.OrderResult.CharaData> m_CharaDatas;

			// Token: 0x0400451D RID: 17693
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40067C2")]
			public List<TrainingManager.OrderResult.ItemData> m_RewardItemDatas;

			// Token: 0x02000BD1 RID: 3025
			[Token(Token = "0x200134A")]
			public class CharaData
			{
				// Token: 0x060034ED RID: 13549 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006516")]
				[Address(RVA = "0x1013BE92C", Offset = "0x13BE92C", VA = "0x1013BE92C")]
				public CharaData()
				{
				}

				// Token: 0x0400451E RID: 17694
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x400759B")]
				public bool m_IsAvailable;

				// Token: 0x0400451F RID: 17695
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x400759C")]
				public long m_MngID;

				// Token: 0x04004520 RID: 17696
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x400759D")]
				public long m_AddExp;

				// Token: 0x04004521 RID: 17697
				[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
				[Token(Token = "0x400759E")]
				public long[] m_NextExps;

				// Token: 0x04004522 RID: 17698
				[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
				[Token(Token = "0x400759F")]
				public int m_BeforeLv;

				// Token: 0x04004523 RID: 17699
				[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
				[Token(Token = "0x40075A0")]
				public long m_BeforeExp;

				// Token: 0x04004524 RID: 17700
				[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
				[Token(Token = "0x40075A1")]
				public int m_AfterLv;

				// Token: 0x04004525 RID: 17701
				[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
				[Token(Token = "0x40075A2")]
				public long m_AfterExp;

				// Token: 0x04004526 RID: 17702
				[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
				[Token(Token = "0x40075A3")]
				public long m_AddFriendshipExp;

				// Token: 0x04004527 RID: 17703
				[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
				[Token(Token = "0x40075A4")]
				public long[] m_FriendshipNextExps;

				// Token: 0x04004528 RID: 17704
				[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
				[Token(Token = "0x40075A5")]
				public int m_BeforeFriendship;

				// Token: 0x04004529 RID: 17705
				[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
				[Token(Token = "0x40075A6")]
				public long m_BeforeFriendshipExp;

				// Token: 0x0400452A RID: 17706
				[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
				[Token(Token = "0x40075A7")]
				public int m_AfterFriendship;

				// Token: 0x0400452B RID: 17707
				[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
				[Token(Token = "0x40075A8")]
				public long m_AfterFriendshipExp;
			}

			// Token: 0x02000BD2 RID: 3026
			[Token(Token = "0x200134B")]
			public class ItemData
			{
				// Token: 0x060034EE RID: 13550 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x6006517")]
				[Address(RVA = "0x1013BE924", Offset = "0x13BE924", VA = "0x1013BE924")]
				public ItemData()
				{
				}

				// Token: 0x0400452C RID: 17708
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x40075A9")]
				public int m_ItemID;

				// Token: 0x0400452D RID: 17709
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x40075AA")]
				public int m_Num;

				// Token: 0x0400452E RID: 17710
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x40075AB")]
				public bool m_IsRareDisp;
			}
		}
	}
}
