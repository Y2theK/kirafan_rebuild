﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000889 RID: 2185
	[Token(Token = "0x2000656")]
	[StructLayout(3)]
	public class TrainingState : GameStateBase
	{
		// Token: 0x06002361 RID: 9057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B0")]
		[Address(RVA = "0x1013BFBD8", Offset = "0x13BFBD8", VA = "0x1013BFBD8")]
		public TrainingState(TrainingMain owner)
		{
		}

		// Token: 0x06002362 RID: 9058 RVA: 0x0000F288 File Offset: 0x0000D488
		[Token(Token = "0x60020B1")]
		[Address(RVA = "0x1013BFC0C", Offset = "0x13BFC0C", VA = "0x1013BFC0C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002363 RID: 9059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B2")]
		[Address(RVA = "0x1013BFC14", Offset = "0x13BFC14", VA = "0x1013BFC14", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B3")]
		[Address(RVA = "0x1013BFC18", Offset = "0x13BFC18", VA = "0x1013BFC18", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B4")]
		[Address(RVA = "0x1013BFC1C", Offset = "0x13BFC1C", VA = "0x1013BFC1C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002366 RID: 9062 RVA: 0x0000F2A0 File Offset: 0x0000D4A0
		[Token(Token = "0x60020B5")]
		[Address(RVA = "0x1013BFC20", Offset = "0x13BFC20", VA = "0x1013BFC20", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002367 RID: 9063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B6")]
		[Address(RVA = "0x1013BFC28", Offset = "0x13BFC28", VA = "0x1013BFC28", Slot = "9")]
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040033A6 RID: 13222
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400262C")]
		protected TrainingMain m_Owner;

		// Token: 0x040033A7 RID: 13223
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400262D")]
		protected int m_NextState;
	}
}
