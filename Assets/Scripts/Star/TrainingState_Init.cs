﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200088A RID: 2186
	[Token(Token = "0x2000657")]
	[StructLayout(3)]
	public class TrainingState_Init : TrainingState
	{
		// Token: 0x06002368 RID: 9064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B7")]
		[Address(RVA = "0x1013B9FE4", Offset = "0x13B9FE4", VA = "0x1013B9FE4")]
		public TrainingState_Init(TrainingMain owner)
		{
		}

		// Token: 0x06002369 RID: 9065 RVA: 0x0000F2B8 File Offset: 0x0000D4B8
		[Token(Token = "0x60020B8")]
		[Address(RVA = "0x1013BFC2C", Offset = "0x13BFC2C", VA = "0x1013BFC2C", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600236A RID: 9066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020B9")]
		[Address(RVA = "0x1013BFC34", Offset = "0x13BFC34", VA = "0x1013BFC34", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020BA")]
		[Address(RVA = "0x1013BFC3C", Offset = "0x13BFC3C", VA = "0x1013BFC3C", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x0600236C RID: 9068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020BB")]
		[Address(RVA = "0x1013BFC40", Offset = "0x13BFC40", VA = "0x1013BFC40", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x0600236D RID: 9069 RVA: 0x0000F2D0 File Offset: 0x0000D4D0
		[Token(Token = "0x60020BC")]
		[Address(RVA = "0x1013BFC44", Offset = "0x13BFC44", VA = "0x1013BFC44", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x0600236E RID: 9070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020BD")]
		[Address(RVA = "0x1013C0358", Offset = "0x13C0358", VA = "0x1013C0358")]
		private void OnResponseGetList()
		{
		}

		// Token: 0x0600236F RID: 9071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020BE")]
		[Address(RVA = "0x1013C0144", Offset = "0x13C0144", VA = "0x1013C0144")]
		private void SetupAndPlayUI()
		{
		}

		// Token: 0x040033A8 RID: 13224
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400262E")]
		private TrainingState_Init.eStep m_Step;

		// Token: 0x040033A9 RID: 13225
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400262F")]
		private bool[] m_IsDoneFlgs;

		// Token: 0x0200088B RID: 2187
		[Token(Token = "0x2000F03")]
		private enum eStep
		{
			// Token: 0x040033AB RID: 13227
			[Token(Token = "0x40060EE")]
			None = -1,
			// Token: 0x040033AC RID: 13228
			[Token(Token = "0x40060EF")]
			First,
			// Token: 0x040033AD RID: 13229
			[Token(Token = "0x40060F0")]
			Load_Wait,
			// Token: 0x040033AE RID: 13230
			[Token(Token = "0x40060F1")]
			End
		}

		// Token: 0x0200088C RID: 2188
		[Token(Token = "0x2000F04")]
		private enum ePrepareTarget
		{
			// Token: 0x040033B0 RID: 13232
			[Token(Token = "0x40060F3")]
			BG,
			// Token: 0x040033B1 RID: 13233
			[Token(Token = "0x40060F4")]
			UI,
			// Token: 0x040033B2 RID: 13234
			[Token(Token = "0x40060F5")]
			TrainingList,
			// Token: 0x040033B3 RID: 13235
			[Token(Token = "0x40060F6")]
			Num
		}
	}
}
