﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200088E RID: 2190
	[Token(Token = "0x2000658")]
	[StructLayout(3)]
	public class TrainingState_Main : TrainingState
	{
		// Token: 0x06002373 RID: 9075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020BF")]
		[Address(RVA = "0x1013BA064", Offset = "0x13BA064", VA = "0x1013BA064")]
		public TrainingState_Main(TrainingMain owner)
		{
		}

		// Token: 0x06002374 RID: 9076 RVA: 0x0000F300 File Offset: 0x0000D500
		[Token(Token = "0x60020C0")]
		[Address(RVA = "0x1013C0418", Offset = "0x13C0418", VA = "0x1013C0418", Slot = "4")]
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C1")]
		[Address(RVA = "0x1013C0420", Offset = "0x13C0420", VA = "0x1013C0420", Slot = "5")]
		public override void OnStateEnter()
		{
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C2")]
		[Address(RVA = "0x1013C0428", Offset = "0x13C0428", VA = "0x1013C0428", Slot = "6")]
		public override void OnStateExit()
		{
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C3")]
		[Address(RVA = "0x1013C042C", Offset = "0x13C042C", VA = "0x1013C042C", Slot = "8")]
		public override void OnDispose()
		{
		}

		// Token: 0x06002378 RID: 9080 RVA: 0x0000F318 File Offset: 0x0000D518
		[Token(Token = "0x60020C4")]
		[Address(RVA = "0x1013C04A0", Offset = "0x13C04A0", VA = "0x1013C04A0", Slot = "7")]
		public override int OnStateUpdate()
		{
			return 0;
		}

		// Token: 0x06002379 RID: 9081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C5")]
		[Address(RVA = "0x1013C0644", Offset = "0x13C0644", VA = "0x1013C0644")]
		private void GotoADV()
		{
		}

		// Token: 0x0600237A RID: 9082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C6")]
		[Address(RVA = "0x1013C0720", Offset = "0x13C0720", VA = "0x1013C0720")]
		private void GotoCharaQuest()
		{
		}

		// Token: 0x0600237B RID: 9083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C7")]
		[Address(RVA = "0x1013C0768", Offset = "0x13C0768", VA = "0x1013C0768")]
		private void GotoReTraining()
		{
		}

		// Token: 0x0600237C RID: 9084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C8")]
		[Address(RVA = "0x1013C07A4", Offset = "0x13C07A4", VA = "0x1013C07A4")]
		private void GotoGlobalShortcut()
		{
		}

		// Token: 0x0600237D RID: 9085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60020C9")]
		[Address(RVA = "0x1013C07B0", Offset = "0x13C07B0", VA = "0x1013C07B0")]
		private void GotoHome()
		{
		}

		// Token: 0x040033B6 RID: 13238
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4002630")]
		private TrainingState_Main.eStep m_Step;

		// Token: 0x0200088F RID: 2191
		[Token(Token = "0x2000F06")]
		private enum eStep
		{
			// Token: 0x040033B8 RID: 13240
			[Token(Token = "0x40060FA")]
			None = -1,
			// Token: 0x040033B9 RID: 13241
			[Token(Token = "0x40060FB")]
			First,
			// Token: 0x040033BA RID: 13242
			[Token(Token = "0x40060FC")]
			Main
		}
	}
}
