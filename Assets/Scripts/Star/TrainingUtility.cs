﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BD3 RID: 3027
	[Token(Token = "0x200082C")]
	[StructLayout(3)]
	public static class TrainingUtility
	{
		// Token: 0x060034EF RID: 13551 RVA: 0x00016638 File Offset: 0x00014838
		[Token(Token = "0x6003031")]
		[Address(RVA = "0x1013C07EC", Offset = "0x13C07EC", VA = "0x1013C07EC")]
		public static bool CheckSameNamedTypeOtherSlot(int slotID, eCharaNamedType namedType, bool otherSlotIsOrder)
		{
			return default(bool);
		}

		// Token: 0x060034F0 RID: 13552 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003032")]
		[Address(RVA = "0x1013C0A1C", Offset = "0x13C0A1C", VA = "0x1013C0A1C")]
		public static bool[] CheckOrder(TrainingManager.TrainingData trainingData, int slotID, List<long> charaMngIDs)
		{
			return null;
		}

		// Token: 0x060034F1 RID: 13553 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003033")]
		[Address(RVA = "0x1013C10D4", Offset = "0x13C10D4", VA = "0x1013C10D4")]
		public static List<TrainingManager.TrainingRequestData> CheckTrainingRestart(List<TrainingManager.TrainingRequestData> reqDataList, out long out_gold, out long out_KRRPoint)
		{
			return null;
		}

		// Token: 0x060034F2 RID: 13554 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003034")]
		[Address(RVA = "0x1013C1410", Offset = "0x13C1410", VA = "0x1013C1410")]
		public static long[] CalcRecommendParty(int slotID, long[] currentCharaMngIDs)
		{
			return null;
		}

		// Token: 0x060034F3 RID: 13555 RVA: 0x00016650 File Offset: 0x00014850
		[Token(Token = "0x6003035")]
		[Address(RVA = "0x1013C1E58", Offset = "0x13C1E58", VA = "0x1013C1E58")]
		private static int RecommendSortComp(UserCharacterData l, UserCharacterData r)
		{
			return 0;
		}

		// Token: 0x060034F4 RID: 13556 RVA: 0x00016668 File Offset: 0x00014868
		[Token(Token = "0x6003036")]
		[Address(RVA = "0x1013C1D50", Offset = "0x13C1D50", VA = "0x1013C1D50")]
		private static long GetRecommendSortKey(UserCharacterData userCharaData)
		{
			return 0L;
		}

		// Token: 0x060034F5 RID: 13557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003037")]
		[Address(RVA = "0x1013C1CE0", Offset = "0x13C1CE0", VA = "0x1013C1CE0")]
		private static void ClearPartyArray(long[] mngIDs)
		{
		}
	}
}
