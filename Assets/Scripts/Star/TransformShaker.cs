﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x020008C8 RID: 2248
	[Token(Token = "0x2000670")]
	[StructLayout(3)]
	public class TransformShaker : MonoBehaviour
	{
		// Token: 0x06002509 RID: 9481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600222B")]
		[Address(RVA = "0x1013C1E98", Offset = "0x13C1E98", VA = "0x1013C1E98")]
		public void Play(float decay, float intensity, Transform refTransform)
		{
		}

		// Token: 0x0600250A RID: 9482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600222C")]
		[Address(RVA = "0x1013C1EF0", Offset = "0x13C1EF0", VA = "0x1013C1EF0")]
		public void Stop()
		{
		}

		// Token: 0x0600250B RID: 9483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600222D")]
		[Address(RVA = "0x1013C1EF8", Offset = "0x13C1EF8", VA = "0x1013C1EF8")]
		public void Update()
		{
		}

		// Token: 0x0600250C RID: 9484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600222E")]
		[Address(RVA = "0x1013C2034", Offset = "0x13C2034", VA = "0x1013C2034")]
		public TransformShaker()
		{
		}

		// Token: 0x040034E3 RID: 13539
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40026AF")]
		private float m_Decay;

		// Token: 0x040034E4 RID: 13540
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40026B0")]
		private float m_Intensity;

		// Token: 0x040034E5 RID: 13541
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40026B1")]
		private float m_WorkIntensity;

		// Token: 0x040034E6 RID: 13542
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40026B2")]
		private Transform m_Transform;

		// Token: 0x040034E7 RID: 13543
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40026B3")]
		private Vector3 m_OriginPos;

		// Token: 0x040034E8 RID: 13544
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40026B4")]
		private bool m_IsShaking;
	}
}
