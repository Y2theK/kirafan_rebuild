﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000BD4 RID: 3028
	[Token(Token = "0x200082D")]
	[StructLayout(3)]
	public static class TutorialDefine
	{
		// Token: 0x0400452F RID: 17711
		[Token(Token = "0x40030C4")]
		public const int GACHA_ID = 1;

		// Token: 0x04004530 RID: 17712
		[Token(Token = "0x40030C5")]
		public const int QUEST_ID = 1100010;

		// Token: 0x04004531 RID: 17713
		[Token(Token = "0x40030C6")]
		public const int ADV_ID_GACHA_PREV = 1000000;

		// Token: 0x04004532 RID: 17714
		[Token(Token = "0x40030C7")]
		public const int ADV_ID_GACHA_AFTER = 1000001;

		// Token: 0x04004533 RID: 17715
		[Token(Token = "0x40030C8")]
		public const int ADV_ID_PROLOGUE_INTRO_1 = 1000002;

		// Token: 0x04004534 RID: 17716
		[Token(Token = "0x40030C9")]
		public const int ADV_ID_PROLOGUE_INTRO_2 = 1000003;

		// Token: 0x04004535 RID: 17717
		[Token(Token = "0x40030CA")]
		public const int ADV_ID_PROLOGUE_END = 1000004;

		// Token: 0x04004536 RID: 17718
		[Token(Token = "0x40030CB")]
		public const int MASTER_ORB_ID = 1;

		// Token: 0x04004537 RID: 17719
		[Token(Token = "0x40030CC")]
		public const int MASTER_ORB_LV = -1;
	}
}
