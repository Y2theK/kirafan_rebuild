﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star
{
	// Token: 0x02000BD5 RID: 3029
	[Token(Token = "0x200082E")]
	[StructLayout(3)]
	public sealed class TutorialManager
	{
		// Token: 0x1400005F RID: 95
		// (add) Token: 0x060034F6 RID: 13558 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034F7 RID: 13559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400005F")]
		private event Action OnCompleteTutorialTipsWindow
		{
			[Token(Token = "0x6003038")]
			[Address(RVA = "0x1013C203C", Offset = "0x13C203C", VA = "0x1013C203C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003039")]
			[Address(RVA = "0x1013C2148", Offset = "0x13C2148", VA = "0x1013C2148")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000060 RID: 96
		// (add) Token: 0x060034F8 RID: 13560 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060034F9 RID: 13561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000060")]
		private event Action m_OnResponseTutorialTipsAdd
		{
			[Token(Token = "0x600303A")]
			[Address(RVA = "0x1013C2254", Offset = "0x13C2254", VA = "0x1013C2254")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600303B")]
			[Address(RVA = "0x1013C2360", Offset = "0x13C2360", VA = "0x1013C2360")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x060034FA RID: 13562 RVA: 0x00016680 File Offset: 0x00014880
		// (set) Token: 0x060034FB RID: 13563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170003A9")]
		public bool IsSkip
		{
			[Token(Token = "0x600303C")]
			[Address(RVA = "0x1013C246C", Offset = "0x13C246C", VA = "0x1013C246C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600303D")]
			[Address(RVA = "0x1013C2474", Offset = "0x13C2474", VA = "0x1013C2474")]
			set
			{
			}
		}

		// Token: 0x060034FC RID: 13564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600303E")]
		[Address(RVA = "0x1013C2478", Offset = "0x13C2478", VA = "0x1013C2478")]
		public TutorialManager()
		{
		}

		// Token: 0x060034FD RID: 13565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600303F")]
		[Address(RVA = "0x1013C24F0", Offset = "0x13C24F0", VA = "0x1013C24F0")]
		public void ForceResetOnReturnTitle()
		{
		}

		// Token: 0x060034FE RID: 13566 RVA: 0x00016698 File Offset: 0x00014898
		[Token(Token = "0x6003040")]
		[Address(RVA = "0x1013C2500", Offset = "0x13C2500", VA = "0x1013C2500")]
		public bool IsFinishedTutorialSeq()
		{
			return default(bool);
		}

		// Token: 0x060034FF RID: 13567 RVA: 0x000166B0 File Offset: 0x000148B0
		[Token(Token = "0x6003041")]
		[Address(RVA = "0x1013C2510", Offset = "0x13C2510", VA = "0x1013C2510")]
		public eTutorialSeq GetTutoarialSeq()
		{
			return eTutorialSeq.Unplayed;
		}

		// Token: 0x06003500 RID: 13568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003042")]
		[Address(RVA = "0x1013C2518", Offset = "0x13C2518", VA = "0x1013C2518")]
		public void SetTutoarialSeq(eTutorialSeq tutorialSeq)
		{
		}

		// Token: 0x06003501 RID: 13569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003043")]
		[Address(RVA = "0x1013C2520", Offset = "0x13C2520", VA = "0x1013C2520")]
		public void SetTutoarialSeq(int tutorialSeq)
		{
		}

		// Token: 0x06003502 RID: 13570 RVA: 0x000166C8 File Offset: 0x000148C8
		[Token(Token = "0x6003044")]
		[Address(RVA = "0x1013C2528", Offset = "0x13C2528", VA = "0x1013C2528")]
		public eTutorialSeq ApplyNextTutorialSeq()
		{
			return eTutorialSeq.Unplayed;
		}

		// Token: 0x06003503 RID: 13571 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003045")]
		[Address(RVA = "0x1013C253C", Offset = "0x13C253C", VA = "0x1013C253C")]
		public List<eTutorialTipsListDB> GetOccurredTipsIDs()
		{
			return null;
		}

		// Token: 0x06003504 RID: 13572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003046")]
		[Address(RVA = "0x1013C2544", Offset = "0x13C2544", VA = "0x1013C2544")]
		public void SetTipsIDs(int[] tipsIDs)
		{
		}

		// Token: 0x06003505 RID: 13573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003047")]
		[Address(RVA = "0x1013C2638", Offset = "0x13C2638", VA = "0x1013C2638")]
		public void AddOccurredTipsID(eTutorialTipsListDB tipsID)
		{
		}

		// Token: 0x06003506 RID: 13574 RVA: 0x000166E0 File Offset: 0x000148E0
		[Token(Token = "0x6003048")]
		[Address(RVA = "0x1013C26E4", Offset = "0x13C26E4", VA = "0x1013C26E4")]
		public bool IsOccurredTips(eTutorialTipsListDB tipsID)
		{
			return default(bool);
		}

		// Token: 0x06003507 RID: 13575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003049")]
		[Address(RVA = "0x1013C24F8", Offset = "0x13C24F8", VA = "0x1013C24F8")]
		public void ClearRequestTips()
		{
		}

		// Token: 0x06003508 RID: 13576 RVA: 0x000166F8 File Offset: 0x000148F8
		[Token(Token = "0x600304A")]
		[Address(RVA = "0x1013C2754", Offset = "0x13C2754", VA = "0x1013C2754")]
		public bool GetRequestTipsBonusCraft()
		{
			return default(bool);
		}

		// Token: 0x06003509 RID: 13577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600304B")]
		[Address(RVA = "0x1013C275C", Offset = "0x13C275C", VA = "0x1013C275C")]
		public void SetRequestTipsBonusCraft(bool flag)
		{
		}

		// Token: 0x0600350A RID: 13578 RVA: 0x00016710 File Offset: 0x00014910
		[Token(Token = "0x600304C")]
		[Address(RVA = "0x1013C2764", Offset = "0x13C2764", VA = "0x1013C2764")]
		public bool OpenTipsWindowIfNotOccurred(eTutorialTipsListDB tipsID, [Optional] Action onComplete, [Optional] Action onOpend)
		{
			return default(bool);
		}

		// Token: 0x0600350B RID: 13579 RVA: 0x00016728 File Offset: 0x00014928
		[Token(Token = "0x600304D")]
		[Address(RVA = "0x1013C289C", Offset = "0x13C289C", VA = "0x1013C289C")]
		public bool OpenTipsWindow_OptionMap(bool ifNotOccurred, int eventQuestType, [Optional] Action onComplete, [Optional] Action onOpend)
		{
			return default(bool);
		}

		// Token: 0x0600350C RID: 13580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600304E")]
		[Address(RVA = "0x1013C27C0", Offset = "0x13C27C0", VA = "0x1013C27C0")]
		public void OpenTipsWindow(eTutorialTipsListDB tipsID, [Optional] Action onComplete, [Optional] Action onOpend)
		{
		}

		// Token: 0x0600350D RID: 13581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600304F")]
		[Address(RVA = "0x1013C2B04", Offset = "0x13C2B04", VA = "0x1013C2B04")]
		private void OnCompleteTutorialTipsUI()
		{
		}

		// Token: 0x0600350E RID: 13582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003050")]
		[Address(RVA = "0x1013C2CC0", Offset = "0x13C2CC0", VA = "0x1013C2CC0")]
		private void Request_CurrentTutorialTipsAdd()
		{
		}

		// Token: 0x0600350F RID: 13583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003051")]
		[Address(RVA = "0x1013C2DD8", Offset = "0x13C2DD8", VA = "0x1013C2DD8")]
		private void OnResponse_CurrentTutorialTipsAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06003510 RID: 13584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003052")]
		[Address(RVA = "0x1013C2E38", Offset = "0x13C2E38", VA = "0x1013C2E38")]
		public void Request_TutorialTipsAdd(int tipsid, Action OnResponse)
		{
		}

		// Token: 0x06003511 RID: 13585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003053")]
		[Address(RVA = "0x1013C2F5C", Offset = "0x13C2F5C", VA = "0x1013C2F5C")]
		private void OnResponse_TutorialTipsAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000061 RID: 97
		// (add) Token: 0x06003512 RID: 13586 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003513 RID: 13587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000061")]
		private event Action m_OnComplete_TutorialFinish
		{
			[Token(Token = "0x6003054")]
			[Address(RVA = "0x1013C2FBC", Offset = "0x13C2FBC", VA = "0x1013C2FBC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003055")]
			[Address(RVA = "0x1013C30C8", Offset = "0x13C30C8", VA = "0x1013C30C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003514 RID: 13588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003056")]
		[Address(RVA = "0x1013C31D4", Offset = "0x13C31D4", VA = "0x1013C31D4")]
		public void Request_TutorialFinish(Action onResponse)
		{
		}

		// Token: 0x06003515 RID: 13589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003057")]
		[Address(RVA = "0x1013C32D4", Offset = "0x13C32D4", VA = "0x1013C32D4")]
		private void OnResponse_TutorialFinish(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000062 RID: 98
		// (add) Token: 0x06003516 RID: 13590 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003517 RID: 13591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000062")]
		private event Action m_OnComplete_UpdateAfterTutorialGacha
		{
			[Token(Token = "0x6003058")]
			[Address(RVA = "0x1013C3400", Offset = "0x13C3400", VA = "0x1013C3400")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003059")]
			[Address(RVA = "0x1013C350C", Offset = "0x13C350C", VA = "0x1013C350C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003518 RID: 13592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600305A")]
		[Address(RVA = "0x1013C3618", Offset = "0x13C3618", VA = "0x1013C3618")]
		public void Request_UpdateAfterTutorialGacha(Action onComplete)
		{
		}

		// Token: 0x06003519 RID: 13593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600305B")]
		[Address(RVA = "0x1013C36E4", Offset = "0x13C36E4", VA = "0x1013C36E4")]
		private void OnResponse_UpdateAfterTutorialGacha_0(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600351A RID: 13594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600305C")]
		[Address(RVA = "0x1013C38B0", Offset = "0x13C38B0", VA = "0x1013C38B0")]
		private void OnResponse_UpdateAfterTutorialGacha_1(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x0600351B RID: 13595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600305D")]
		[Address(RVA = "0x1013C3A24", Offset = "0x13C3A24", VA = "0x1013C3A24")]
		private void OnResponse_UpdateAfterTutorialGacha_2(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x14000063 RID: 99
		// (add) Token: 0x0600351C RID: 13596 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600351D RID: 13597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000063")]
		private event Action m_OnComplete_TutorialQuest
		{
			[Token(Token = "0x600305E")]
			[Address(RVA = "0x1013C3B28", Offset = "0x13C3B28", VA = "0x1013C3B28")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600305F")]
			[Address(RVA = "0x1013C3C34", Offset = "0x13C3C34", VA = "0x1013C3C34")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600351E RID: 13598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003060")]
		[Address(RVA = "0x1013C3D40", Offset = "0x13C3D40", VA = "0x1013C3D40")]
		public void Request_TutorialQuest(Action onComplete)
		{
		}

		// Token: 0x0600351F RID: 13599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003061")]
		[Address(RVA = "0x1013C3E04", Offset = "0x13C3E04", VA = "0x1013C3E04")]
		private void OnResponse_QuestGetAll()
		{
		}

		// Token: 0x06003520 RID: 13600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003062")]
		[Address(RVA = "0x1013C3EC0", Offset = "0x13C3EC0", VA = "0x1013C3EC0")]
		private void OnResponse_QuestChapterGetAll()
		{
		}

		// Token: 0x06003521 RID: 13601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003063")]
		[Address(RVA = "0x1013C4180", Offset = "0x13C4180", VA = "0x1013C4180")]
		private void OnResponse_QuestLogAdd(QuestDefine.eReturnLogAdd ret, string errorMessage)
		{
		}

		// Token: 0x06003522 RID: 13602 RVA: 0x00016740 File Offset: 0x00014940
		[Token(Token = "0x6003064")]
		[Address(RVA = "0x1013C4194", Offset = "0x13C4194", VA = "0x1013C4194")]
		public TutorialManager.eRoomTutorialStep GetRoomTutorialStep()
		{
			return TutorialManager.eRoomTutorialStep.None;
		}

		// Token: 0x06003523 RID: 13603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003065")]
		[Address(RVA = "0x1013C419C", Offset = "0x13C419C", VA = "0x1013C419C")]
		public void SetRoomTutorialStep(TutorialManager.eRoomTutorialStep step)
		{
		}

		// Token: 0x06003524 RID: 13604 RVA: 0x00016758 File Offset: 0x00014958
		[Token(Token = "0x6003066")]
		[Address(RVA = "0x1013C41A4", Offset = "0x13C41A4", VA = "0x1013C41A4")]
		public bool IsRoomTutorial()
		{
			return default(bool);
		}

		// Token: 0x06003525 RID: 13605 RVA: 0x00016770 File Offset: 0x00014970
		[Token(Token = "0x6003067")]
		[Address(RVA = "0x1013C41B4", Offset = "0x13C41B4", VA = "0x1013C41B4")]
		public TutorialManager.eRoomTutorialStep CalcRoomTutorialStep()
		{
			return TutorialManager.eRoomTutorialStep.None;
		}

		// Token: 0x06003526 RID: 13606 RVA: 0x00016788 File Offset: 0x00014988
		[Token(Token = "0x6003068")]
		[Address(RVA = "0x1013C4330", Offset = "0x13C4330", VA = "0x1013C4330")]
		public TutorialManager.eTownTutorialStep GetTownTutorialStep()
		{
			return TutorialManager.eTownTutorialStep.None;
		}

		// Token: 0x06003527 RID: 13607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003069")]
		[Address(RVA = "0x1013C4338", Offset = "0x13C4338", VA = "0x1013C4338")]
		public void SetTownTutorialStep(TutorialManager.eTownTutorialStep step)
		{
		}

		// Token: 0x06003528 RID: 13608 RVA: 0x000167A0 File Offset: 0x000149A0
		[Token(Token = "0x600306A")]
		[Address(RVA = "0x1013C4340", Offset = "0x13C4340", VA = "0x1013C4340")]
		public bool IsTownTutorial()
		{
			return default(bool);
		}

		// Token: 0x06003529 RID: 13609 RVA: 0x000167B8 File Offset: 0x000149B8
		[Token(Token = "0x600306B")]
		[Address(RVA = "0x1013C4350", Offset = "0x13C4350", VA = "0x1013C4350")]
		public TutorialManager.eTownTutorialStep CalcTownTutorialStep()
		{
			return TutorialManager.eTownTutorialStep.None;
		}

		// Token: 0x0600352A RID: 13610 RVA: 0x000167D0 File Offset: 0x000149D0
		[Token(Token = "0x600306C")]
		[Address(RVA = "0x1013C47A8", Offset = "0x13C47A8", VA = "0x1013C47A8")]
		private bool IsBuilding()
		{
			return default(bool);
		}

		// Token: 0x0600352B RID: 13611 RVA: 0x000167E8 File Offset: 0x000149E8
		[Token(Token = "0x600306D")]
		[Address(RVA = "0x1013C48C4", Offset = "0x13C48C4", VA = "0x1013C48C4")]
		private bool IsNotAllOpen()
		{
			return default(bool);
		}

		// Token: 0x04004538 RID: 17720
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40030CD")]
		private eTutorialSeq m_TutorialSeq;

		// Token: 0x04004539 RID: 17721
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40030CE")]
		private List<eTutorialTipsListDB> m_OccurredTipsIDs;

		// Token: 0x0400453A RID: 17722
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40030CF")]
		private eTutorialTipsListDB m_CurrentTutorialTipsID;

		// Token: 0x0400453B RID: 17723
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40030D0")]
		private bool m_RequestTipsBonusCraft;

		// Token: 0x0400453E RID: 17726
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40030D3")]
		private TutorialManager.eTownTutorialStep m_TownTutorialStep;

		// Token: 0x0400453F RID: 17727
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40030D4")]
		private TutorialManager.eRoomTutorialStep m_RoomTutorialStep;

		// Token: 0x04004540 RID: 17728
		[Token(Token = "0x40030D5")]
		private const int DEFAULT_ROOMOBJ_NUM = 4;

		// Token: 0x02000BD6 RID: 3030
		[Token(Token = "0x2001071")]
		public enum eTownTutorialStep
		{
			// Token: 0x04004545 RID: 17733
			[Token(Token = "0x40067C4")]
			None,
			// Token: 0x04004546 RID: 17734
			[Token(Token = "0x40067C5")]
			FirstTips,
			// Token: 0x04004547 RID: 17735
			[Token(Token = "0x40067C6")]
			BuildContent,
			// Token: 0x04004548 RID: 17736
			[Token(Token = "0x40067C7")]
			WaitBuildContent,
			// Token: 0x04004549 RID: 17737
			[Token(Token = "0x40067C8")]
			AfterContentTips,
			// Token: 0x0400454A RID: 17738
			[Token(Token = "0x40067C9")]
			BuildWeapon,
			// Token: 0x0400454B RID: 17739
			[Token(Token = "0x40067CA")]
			WaitBuildWeapon,
			// Token: 0x0400454C RID: 17740
			[Token(Token = "0x40067CB")]
			CompleteWeapon,
			// Token: 0x0400454D RID: 17741
			[Token(Token = "0x40067CC")]
			BuildMoney,
			// Token: 0x0400454E RID: 17742
			[Token(Token = "0x40067CD")]
			WaitBuildMoney,
			// Token: 0x0400454F RID: 17743
			[Token(Token = "0x40067CE")]
			CompleteMoney,
			// Token: 0x04004550 RID: 17744
			[Token(Token = "0x40067CF")]
			AfterBuffTips,
			// Token: 0x04004551 RID: 17745
			[Token(Token = "0x40067D0")]
			EndTips
		}

		// Token: 0x02000BD7 RID: 3031
		[Token(Token = "0x2001072")]
		public enum eRoomTutorialStep
		{
			// Token: 0x04004553 RID: 17747
			[Token(Token = "0x40067D2")]
			None,
			// Token: 0x04004554 RID: 17748
			[Token(Token = "0x40067D3")]
			FirstTips,
			// Token: 0x04004555 RID: 17749
			[Token(Token = "0x40067D4")]
			Buy,
			// Token: 0x04004556 RID: 17750
			[Token(Token = "0x40067D5")]
			MoveTips,
			// Token: 0x04004557 RID: 17751
			[Token(Token = "0x40067D6")]
			EndTips
		}
	}
}
