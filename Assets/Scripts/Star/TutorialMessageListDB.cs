﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000604 RID: 1540
	[Token(Token = "0x20004F7")]
	[StructLayout(3)]
	public class TutorialMessageListDB : ScriptableObject
	{
		// Token: 0x06001612 RID: 5650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C2")]
		[Address(RVA = "0x1013C49C4", Offset = "0x13C49C4", VA = "0x1013C49C4")]
		public TutorialMessageListDB()
		{
		}

		// Token: 0x04002579 RID: 9593
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001EE3")]
		public TutorialMessageListDB_Param[] m_Params;
	}
}
