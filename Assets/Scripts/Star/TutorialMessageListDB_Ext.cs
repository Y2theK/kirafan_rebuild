﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200053F RID: 1343
	[Token(Token = "0x2000434")]
	[StructLayout(3)]
	public static class TutorialMessageListDB_Ext
	{
		// Token: 0x060015B6 RID: 5558 RVA: 0x000099A8 File Offset: 0x00007BA8
		[Token(Token = "0x600146A")]
		[Address(RVA = "0x1013C49CC", Offset = "0x13C49CC", VA = "0x1013C49CC")]
		public static TutorialMessageListDB_Param GetParam(this TutorialMessageListDB self, eTutorialMessageListDB id)
		{
			return default(TutorialMessageListDB_Param);
		}
	}
}
