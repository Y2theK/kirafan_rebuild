﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000603 RID: 1539
	[Token(Token = "0x20004F6")]
	[Serializable]
	[StructLayout(0, Size = 16)]
	public struct TutorialMessageListDB_Param
	{
		// Token: 0x04002576 RID: 9590
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001EE0")]
		public int m_ID;

		// Token: 0x04002577 RID: 9591
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001EE1")]
		public int m_Position;

		// Token: 0x04002578 RID: 9592
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001EE2")]
		public string m_Text;
	}
}
