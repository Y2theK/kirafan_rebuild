﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x02000609 RID: 1545
	[Token(Token = "0x20004FC")]
	[StructLayout(3)]
	public class TutorialTipsListDB : ScriptableObject
	{
		// Token: 0x06001613 RID: 5651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C3")]
		[Address(RVA = "0x1013C4AB4", Offset = "0x13C4AB4", VA = "0x1013C4AB4")]
		public TutorialTipsListDB()
		{
		}

		// Token: 0x040025BB RID: 9659
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F25")]
		public TutorialTipsListDB_Param[] m_Params;
	}
}
