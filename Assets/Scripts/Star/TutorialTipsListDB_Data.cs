﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000607 RID: 1543
	[Token(Token = "0x20004FA")]
	[Serializable]
	[StructLayout(0, Size = 24)]
	public struct TutorialTipsListDB_Data
	{
		// Token: 0x040025B2 RID: 9650
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F1C")]
		public int m_ImageID;

		// Token: 0x040025B3 RID: 9651
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F1D")]
		public string m_Title;

		// Token: 0x040025B4 RID: 9652
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F1E")]
		public string m_Text;
	}
}
