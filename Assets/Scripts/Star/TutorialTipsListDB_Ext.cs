﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000540 RID: 1344
	[Token(Token = "0x2000435")]
	[StructLayout(3)]
	public static class TutorialTipsListDB_Ext
	{
		// Token: 0x060015B7 RID: 5559 RVA: 0x000099C0 File Offset: 0x00007BC0
		[Token(Token = "0x600146B")]
		[Address(RVA = "0x1013C2BCC", Offset = "0x13C2BCC", VA = "0x1013C2BCC")]
		public static TutorialTipsListDB_Param GetParam(this TutorialTipsListDB self, eTutorialTipsListDB id)
		{
			return default(TutorialTipsListDB_Param);
		}

		// Token: 0x060015B8 RID: 5560 RVA: 0x000099D8 File Offset: 0x00007BD8
		[Token(Token = "0x600146C")]
		[Address(RVA = "0x1013C2984", Offset = "0x13C2984", VA = "0x1013C2984")]
		public static int FindID_OptionMap(this TutorialTipsListDB self, int eventQuestType)
		{
			return 0;
		}
	}
}
