﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000608 RID: 1544
	[Token(Token = "0x20004FB")]
	[Serializable]
	[StructLayout(0)]
	public struct TutorialTipsListDB_Param
	{
		// Token: 0x040025B5 RID: 9653
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F1F")]
		public int m_ID;

		// Token: 0x040025B6 RID: 9654
		[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
		[Token(Token = "0x4001F20")]
		public byte m_IsAPI;

		// Token: 0x040025B7 RID: 9655
		[Cpp2IlInjected.FieldOffset(Offset = "0x5")]
		[Token(Token = "0x4001F21")]
		public byte m_IsCloseAfterViewAllPages;

		// Token: 0x040025B8 RID: 9656
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F22")]
		public int m_Option;

		// Token: 0x040025B9 RID: 9657
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F23")]
		public int[] m_OptionArgs;

		// Token: 0x040025BA RID: 9658
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F24")]
		public TutorialTipsListDB_Data[] m_Datas;
	}
}
