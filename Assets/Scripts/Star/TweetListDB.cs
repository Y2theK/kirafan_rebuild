﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star
{
	// Token: 0x0200060D RID: 1549
	[Token(Token = "0x2000500")]
	[StructLayout(3)]
	public class TweetListDB : ScriptableObject
	{
		// Token: 0x06001614 RID: 5652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60014C4")]
		[Address(RVA = "0x1013C4ABC", Offset = "0x13C4ABC", VA = "0x1013C4ABC")]
		public TweetListDB()
		{
		}

		// Token: 0x040025D2 RID: 9682
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F3C")]
		public TweetListDB_Param[] m_Params;
	}
}
