﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x02000541 RID: 1345
	[Token(Token = "0x2000436")]
	[StructLayout(3)]
	public static class TweetListDB_Ext
	{
		// Token: 0x060015B9 RID: 5561 RVA: 0x000099F0 File Offset: 0x00007BF0
		[Token(Token = "0x600146D")]
		[Address(RVA = "0x1013C4AC4", Offset = "0x13C4AC4", VA = "0x1013C4AC4")]
		public static TweetListDB_Param GetParam(this TweetListDB self, int tweetID)
		{
			return default(TweetListDB_Param);
		}

		// Token: 0x060015BA RID: 5562 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600146E")]
		[Address(RVA = "0x1013C4BB8", Offset = "0x13C4BB8", VA = "0x1013C4BB8")]
		public static string GetText(this TweetListDB self, int tweetID)
		{
			return null;
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x00009A08 File Offset: 0x00007C08
		[Token(Token = "0x600146F")]
		[Address(RVA = "0x1013C4BDC", Offset = "0x13C4BDC", VA = "0x1013C4BDC")]
		public static TweetListDB_Param GetParam(this TweetListDB self, eTweetIDBase baseID, int charaId)
		{
			return default(TweetListDB_Param);
		}

		// Token: 0x060015BC RID: 5564 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6001470")]
		[Address(RVA = "0x1013C4F1C", Offset = "0x13C4F1C", VA = "0x1013C4F1C")]
		public static TweetListDB_Param[] GetParams(this TweetListDB self, eTweetIDBase baseID)
		{
			return null;
		}
	}
}
