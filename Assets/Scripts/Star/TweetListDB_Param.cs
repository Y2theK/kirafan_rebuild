﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star
{
	// Token: 0x0200060C RID: 1548
	[Token(Token = "0x20004FF")]
	[Serializable]
	[StructLayout(0, Size = 32)]
	public struct TweetListDB_Param
	{
		// Token: 0x040025CE RID: 9678
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4001F38")]
		public int m_ID;

		// Token: 0x040025CF RID: 9679
		[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
		[Token(Token = "0x4001F39")]
		public string m_Text;

		// Token: 0x040025D0 RID: 9680
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4001F3A")]
		public string m_VoiceCueName;

		// Token: 0x040025D1 RID: 9681
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4001F3B")]
		public int[] m_LimitedCharaId;
	}
}
