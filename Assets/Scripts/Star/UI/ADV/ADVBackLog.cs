﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02001185 RID: 4485
	[Token(Token = "0x2000BA6")]
	[StructLayout(3)]
	public class ADVBackLog : UIGroup
	{
		// Token: 0x170005E8 RID: 1512
		// (set) Token: 0x060058CE RID: 22734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700058C")]
		public float StartScrollPosition
		{
			[Token(Token = "0x6005212")]
			[Address(RVA = "0x1013C5180", Offset = "0x13C5180", VA = "0x1013C5180")]
			set
			{
			}
		}

		// Token: 0x060058CF RID: 22735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005213")]
		[Address(RVA = "0x1013C5188", Offset = "0x13C5188", VA = "0x1013C5188")]
		public void Setup()
		{
		}

		// Token: 0x060058D0 RID: 22736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005214")]
		[Address(RVA = "0x1013C51BC", Offset = "0x13C51BC", VA = "0x1013C51BC", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060058D1 RID: 22737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005215")]
		[Address(RVA = "0x1013C524C", Offset = "0x13C524C", VA = "0x1013C524C", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060058D2 RID: 22738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005216")]
		[Address(RVA = "0x1013C52C4", Offset = "0x13C52C4", VA = "0x1013C52C4")]
		public void Destroy()
		{
		}

		// Token: 0x060058D3 RID: 22739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005217")]
		[Address(RVA = "0x1013C52FC", Offset = "0x13C52FC", VA = "0x1013C52FC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060058D4 RID: 22740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005218")]
		[Address(RVA = "0x1013C53D8", Offset = "0x13C53D8", VA = "0x1013C53D8")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x060058D5 RID: 22741 RVA: 0x0001D310 File Offset: 0x0001B510
		[Token(Token = "0x6005219")]
		[Address(RVA = "0x1013C5464", Offset = "0x13C5464", VA = "0x1013C5464")]
		private bool CheckUpdateAndroidBackKey()
		{
			return default(bool);
		}

		// Token: 0x060058D6 RID: 22742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521A")]
		[Address(RVA = "0x1013C546C", Offset = "0x13C546C", VA = "0x1013C546C")]
		public void AddText(string ADVCharaID, string talker, string str, List<ADVParser.RubyData> rubyList)
		{
		}

		// Token: 0x060058D7 RID: 22743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521B")]
		[Address(RVA = "0x1013C55F8", Offset = "0x13C55F8", VA = "0x1013C55F8")]
		public void RemoveText()
		{
		}

		// Token: 0x060058D8 RID: 22744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521C")]
		[Address(RVA = "0x1013C562C", Offset = "0x13C562C", VA = "0x1013C562C")]
		public ADVBackLog()
		{
		}

		// Token: 0x04006BE3 RID: 27619
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C7D")]
		[SerializeField]
		private ADVBackLogScroll m_Scroll;

		// Token: 0x04006BE4 RID: 27620
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004C7E")]
		[SerializeField]
		private AnimUIPlayer m_WindowAnim;

		// Token: 0x04006BE5 RID: 27621
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004C7F")]
		[SerializeField]
		private CustomButton m_CloseButton;

		// Token: 0x04006BE6 RID: 27622
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004C80")]
		private float m_StartScrollPosition;
	}
}
