﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001186 RID: 4486
	[Token(Token = "0x2000BA7")]
	[StructLayout(3)]
	public class ADVBackLogItem : ScrollItemIcon
	{
		// Token: 0x060058D9 RID: 22745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521D")]
		[Address(RVA = "0x1013C5634", Offset = "0x13C5634", VA = "0x1013C5634", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x060058DA RID: 22746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521E")]
		[Address(RVA = "0x1013C5694", Offset = "0x13C5694", VA = "0x1013C5694", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060058DB RID: 22747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600521F")]
		[Address(RVA = "0x1013C5B34", Offset = "0x13C5B34", VA = "0x1013C5B34")]
		public void AddRuby(string text, Vector2 startPos, Vector2 endPos)
		{
		}

		// Token: 0x060058DC RID: 22748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005220")]
		[Address(RVA = "0x1013C5980", Offset = "0x13C5980", VA = "0x1013C5980")]
		public void ClearRuby()
		{
		}

		// Token: 0x060058DD RID: 22749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005221")]
		[Address(RVA = "0x1013C6500", Offset = "0x13C6500", VA = "0x1013C6500")]
		protected void Update()
		{
		}

		// Token: 0x060058DE RID: 22750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005222")]
		[Address(RVA = "0x1013C6EF0", Offset = "0x13C6EF0", VA = "0x1013C6EF0")]
		private void OnDestroy()
		{
		}

		// Token: 0x060058DF RID: 22751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005223")]
		[Address(RVA = "0x1013C6EFC", Offset = "0x13C6EFC", VA = "0x1013C6EFC", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060058E0 RID: 22752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005224")]
		[Address(RVA = "0x1013C5E28", Offset = "0x13C5E28", VA = "0x1013C5E28")]
		private void Load()
		{
		}

		// Token: 0x060058E1 RID: 22753 RVA: 0x0001D328 File Offset: 0x0001B528
		[Token(Token = "0x6005225")]
		[Address(RVA = "0x1013C6D00", Offset = "0x13C6D00", VA = "0x1013C6D00")]
		private bool IsBodyAvailable()
		{
			return default(bool);
		}

		// Token: 0x060058E2 RID: 22754 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005226")]
		[Address(RVA = "0x1013C70A0", Offset = "0x13C70A0", VA = "0x1013C70A0")]
		public ADVCalcText GetCalcTextObj()
		{
			return null;
		}

		// Token: 0x060058E3 RID: 22755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005227")]
		[Address(RVA = "0x1013C70A8", Offset = "0x13C70A8", VA = "0x1013C70A8")]
		public ADVBackLogItem()
		{
		}

		// Token: 0x04006BE7 RID: 27623
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C81")]
		[SerializeField]
		private Image m_BodyImage;

		// Token: 0x04006BE8 RID: 27624
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C82")]
		[SerializeField]
		private Image m_FaceImage;

		// Token: 0x04006BE9 RID: 27625
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004C83")]
		[SerializeField]
		private RectTransform m_MaskImage;

		// Token: 0x04006BEA RID: 27626
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004C84")]
		[SerializeField]
		private Text m_TalkerTextObj;

		// Token: 0x04006BEB RID: 27627
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004C85")]
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04006BEC RID: 27628
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004C86")]
		[SerializeField]
		private ADVCalcText m_CalcText;

		// Token: 0x04006BED RID: 27629
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C87")]
		private RectTransform m_TextRectTransform;

		// Token: 0x04006BEE RID: 27630
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004C88")]
		private List<Text> m_RubyList;

		// Token: 0x04006BEF RID: 27631
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004C89")]
		private string m_ADVCharaID;

		// Token: 0x04006BF0 RID: 27632
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004C8A")]
		private SpriteHandler m_BodySpriteHandler;

		// Token: 0x04006BF1 RID: 27633
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004C8B")]
		private SpriteHandler m_FaceSpriteHandler;
	}
}
