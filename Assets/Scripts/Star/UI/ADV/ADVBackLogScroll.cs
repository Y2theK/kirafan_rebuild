﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02001188 RID: 4488
	[Token(Token = "0x2000BA9")]
	[StructLayout(3)]
	public class ADVBackLogScroll : ScrollViewBase
	{
		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x060058ED RID: 22765 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000591")]
		public ADVPlayer Player
		{
			[Token(Token = "0x6005231")]
			[Address(RVA = "0x1013C6EE8", Offset = "0x13C6EE8", VA = "0x1013C6EE8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005EE RID: 1518
		// (get) Token: 0x060058EE RID: 22766 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000592")]
		public ADVRubyPool RubyPool
		{
			[Token(Token = "0x6005232")]
			[Address(RVA = "0x1013C6224", Offset = "0x13C6224", VA = "0x1013C6224")]
			get
			{
				return null;
			}
		}

		// Token: 0x060058EF RID: 22767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005233")]
		[Address(RVA = "0x1013C7128", Offset = "0x13C7128", VA = "0x1013C7128", Slot = "19")]
		public override void Destroy()
		{
		}

		// Token: 0x060058F0 RID: 22768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005234")]
		[Address(RVA = "0x1013C7250", Offset = "0x13C7250", VA = "0x1013C7250")]
		public ADVBackLogScroll()
		{
		}

		// Token: 0x04006BF6 RID: 27638
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004C90")]
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04006BF7 RID: 27639
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004C91")]
		[SerializeField]
		private ADVRubyPool m_RubyPool;
	}
}
