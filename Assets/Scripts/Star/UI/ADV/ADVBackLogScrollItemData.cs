﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;

namespace Star.UI.ADV
{
	// Token: 0x02001187 RID: 4487
	[Token(Token = "0x2000BA8")]
	[StructLayout(3)]
	public class ADVBackLogScrollItemData : ScrollItemData
	{
		// Token: 0x170005E9 RID: 1513
		// (get) Token: 0x060058E5 RID: 22757 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060058E4 RID: 22756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700058D")]
		public string ADVCharaID
		{
			[Token(Token = "0x6005229")]
			[Address(RVA = "0x1013C5E20", Offset = "0x13C5E20", VA = "0x1013C5E20")]
			get
			{
				return null;
			}
			[Token(Token = "0x6005228")]
			[Address(RVA = "0x1013C55D8", Offset = "0x13C55D8", VA = "0x1013C55D8")]
			set
			{
			}
		}

		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x060058E7 RID: 22759 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060058E6 RID: 22758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700058E")]
		public string Talker
		{
			[Token(Token = "0x600522B")]
			[Address(RVA = "0x1013C5970", Offset = "0x13C5970", VA = "0x1013C5970")]
			get
			{
				return null;
			}
			[Token(Token = "0x600522A")]
			[Address(RVA = "0x1013C55E0", Offset = "0x13C55E0", VA = "0x1013C55E0")]
			set
			{
			}
		}

		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x060058E9 RID: 22761 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060058E8 RID: 22760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700058F")]
		public string Text
		{
			[Token(Token = "0x600522D")]
			[Address(RVA = "0x1013C5978", Offset = "0x13C5978", VA = "0x1013C5978")]
			get
			{
				return null;
			}
			[Token(Token = "0x600522C")]
			[Address(RVA = "0x1013C55E8", Offset = "0x13C55E8", VA = "0x1013C55E8")]
			set
			{
			}
		}

		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x060058EB RID: 22763 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060058EA RID: 22762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000590")]
		public List<ADVParser.RubyData> RubyList
		{
			[Token(Token = "0x600522F")]
			[Address(RVA = "0x1013C5B2C", Offset = "0x13C5B2C", VA = "0x1013C5B2C")]
			get
			{
				return null;
			}
			[Token(Token = "0x600522E")]
			[Address(RVA = "0x1013C55F0", Offset = "0x13C55F0", VA = "0x1013C55F0")]
			set
			{
			}
		}

		// Token: 0x060058EC RID: 22764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005230")]
		[Address(RVA = "0x1013C5550", Offset = "0x13C5550", VA = "0x1013C5550")]
		public ADVBackLogScrollItemData()
		{
		}

		// Token: 0x04006BF2 RID: 27634
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C8C")]
		private string m_ADVCharaID;

		// Token: 0x04006BF3 RID: 27635
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C8D")]
		private string m_Talker;

		// Token: 0x04006BF4 RID: 27636
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C8E")]
		private string m_Text;

		// Token: 0x04006BF5 RID: 27637
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004C8F")]
		private List<ADVParser.RubyData> m_RubyList;
	}
}
