﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001189 RID: 4489
	[Token(Token = "0x2000BAA")]
	[StructLayout(3)]
	public class ADVCalcText : MonoBehaviour
	{
		// Token: 0x170005EF RID: 1519
		// (get) Token: 0x060058F1 RID: 22769 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000593")]
		public GameObject GameObject
		{
			[Token(Token = "0x6005235")]
			[Address(RVA = "0x1013C7258", Offset = "0x13C7258", VA = "0x1013C7258")]
			get
			{
				return null;
			}
		}

		// Token: 0x060058F2 RID: 22770 RVA: 0x0001D340 File Offset: 0x0001B540
		[Token(Token = "0x6005236")]
		[Address(RVA = "0x1013C72E8", Offset = "0x13C72E8", VA = "0x1013C72E8")]
		public Vector2 CalcLastPosition(string text)
		{
			return default(Vector2);
		}

		// Token: 0x060058F3 RID: 22771 RVA: 0x0001D358 File Offset: 0x0001B558
		[Token(Token = "0x6005237")]
		[Address(RVA = "0x1013C730C", Offset = "0x13C730C", VA = "0x1013C730C")]
		public Vector2 CalcLastPosition(string text, out Text parentObj)
		{
			return default(Vector2);
		}

		// Token: 0x060058F4 RID: 22772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005238")]
		[Address(RVA = "0x1013C7960", Offset = "0x13C7960", VA = "0x1013C7960")]
		public ADVCalcText()
		{
		}

		// Token: 0x04006BF8 RID: 27640
		[Token(Token = "0x4004C92")]
		private const float RUBY_OFFSET_Y = 8f;

		// Token: 0x04006BF9 RID: 27641
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C93")]
		[SerializeField]
		private Text[] m_TextObjs;

		// Token: 0x04006BFA RID: 27642
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C94")]
		private GameObject m_GameObject;
	}
}
