﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x0200118A RID: 4490
	[Token(Token = "0x2000BAB")]
	[StructLayout(3)]
	public class ADVFade : MonoBehaviour
	{
		// Token: 0x170005F0 RID: 1520
		// (get) Token: 0x060058F5 RID: 22773 RVA: 0x0001D370 File Offset: 0x0001B570
		[Token(Token = "0x17000594")]
		public bool IsFade
		{
			[Token(Token = "0x6005239")]
			[Address(RVA = "0x1013C7968", Offset = "0x13C7968", VA = "0x1013C7968")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060058F6 RID: 22774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523A")]
		[Address(RVA = "0x1013C7970", Offset = "0x13C7970", VA = "0x1013C7970")]
		public void Set(Color rgba)
		{
		}

		// Token: 0x060058F7 RID: 22775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523B")]
		[Address(RVA = "0x1013C79E8", Offset = "0x13C79E8", VA = "0x1013C79E8")]
		public void Fade(Color rgba, float time)
		{
		}

		// Token: 0x060058F8 RID: 22776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523C")]
		[Address(RVA = "0x1013C7A18", Offset = "0x13C7A18", VA = "0x1013C7A18")]
		private void Update()
		{
		}

		// Token: 0x060058F9 RID: 22777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523D")]
		[Address(RVA = "0x1013C7B00", Offset = "0x13C7B00", VA = "0x1013C7B00")]
		public ADVFade()
		{
		}

		// Token: 0x04006BFB RID: 27643
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C95")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006BFC RID: 27644
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C96")]
		private Color m_NowColor;

		// Token: 0x04006BFD RID: 27645
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004C97")]
		private bool m_IsFade;

		// Token: 0x04006BFE RID: 27646
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4004C98")]
		private Color m_StartColor;

		// Token: 0x04006BFF RID: 27647
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004C99")]
		private Color m_EndColor;

		// Token: 0x04006C00 RID: 27648
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004C9A")]
		private float m_Duration;

		// Token: 0x04006C01 RID: 27649
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C9B")]
		private float m_Time;
	}
}
