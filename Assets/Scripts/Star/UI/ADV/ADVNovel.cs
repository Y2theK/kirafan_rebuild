﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02001183 RID: 4483
	[Token(Token = "0x2000BA4")]
	[StructLayout(3)]
	public class ADVNovel : UIGroup
	{
		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x060058B5 RID: 22709 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000588")]
		public ADVNovelCaption LastCaption
		{
			[Token(Token = "0x60051F9")]
			[Address(RVA = "0x1013C7B6C", Offset = "0x13C7B6C", VA = "0x1013C7B6C")]
			get
			{
				return null;
			}
		}

		// Token: 0x060058B6 RID: 22710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FA")]
		[Address(RVA = "0x1013C7B74", Offset = "0x13C7B74", VA = "0x1013C7B74")]
		public void Setup()
		{
		}

		// Token: 0x060058B7 RID: 22711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FB")]
		[Address(RVA = "0x1013C7D28", Offset = "0x13C7D28", VA = "0x1013C7D28", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060058B8 RID: 22712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FC")]
		[Address(RVA = "0x1013C7D9C", Offset = "0x13C7D9C", VA = "0x1013C7D9C", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060058B9 RID: 22713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FD")]
		[Address(RVA = "0x1013C7E14", Offset = "0x13C7E14", VA = "0x1013C7E14")]
		public void SetAnchorPosition(eADVAnchor anchor, Vector2 pos)
		{
		}

		// Token: 0x060058BA RID: 22714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FE")]
		[Address(RVA = "0x1013C7C50", Offset = "0x13C7C50", VA = "0x1013C7C50")]
		public void SetAnchorPositionDefault()
		{
		}

		// Token: 0x060058BB RID: 22715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051FF")]
		[Address(RVA = "0x1013C7ED0", Offset = "0x13C7ED0", VA = "0x1013C7ED0")]
		public void ClearText()
		{
		}

		// Token: 0x060058BC RID: 22716 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005200")]
		[Address(RVA = "0x1013C813C", Offset = "0x13C813C", VA = "0x1013C813C")]
		public ADVNovelCaption AddText()
		{
			return null;
		}

		// Token: 0x060058BD RID: 22717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005201")]
		[Address(RVA = "0x1013C86F0", Offset = "0x13C86F0", VA = "0x1013C86F0")]
		public void SetText(string text)
		{
		}

		// Token: 0x060058BE RID: 22718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005202")]
		[Address(RVA = "0x1013C8724", Offset = "0x13C8724", VA = "0x1013C8724")]
		public ADVNovel()
		{
		}

		// Token: 0x04006BCD RID: 27597
		[Token(Token = "0x4004C67")]
		private const float LINESPACE = 12f;

		// Token: 0x04006BCE RID: 27598
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C68")]
		[SerializeField]
		private ADVNovelCaption m_CaptionPrefab;

		// Token: 0x04006BCF RID: 27599
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004C69")]
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04006BD0 RID: 27600
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004C6A")]
		private List<ADVNovelCaption> m_ADVNovelCaptionList;

		// Token: 0x04006BD1 RID: 27601
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004C6B")]
		private Vector2 m_StartPosition;

		// Token: 0x04006BD2 RID: 27602
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004C6C")]
		private Vector2 m_LastPosition;

		// Token: 0x04006BD3 RID: 27603
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004C6D")]
		private eADVAnchor m_Anchor;

		// Token: 0x04006BD4 RID: 27604
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004C6E")]
		private ADVNovelCaption m_LastCaption;

		// Token: 0x04006BD5 RID: 27605
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004C6F")]
		private RectTransform m_RectTransform;

		// Token: 0x04006BD6 RID: 27606
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004C70")]
		private AnimUIPlayer m_Anim;
	}
}
