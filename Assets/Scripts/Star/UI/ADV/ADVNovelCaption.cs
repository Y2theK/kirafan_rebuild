﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001184 RID: 4484
	[Token(Token = "0x2000BA5")]
	[StructLayout(3)]
	public class ADVNovelCaption : MonoBehaviour
	{
		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x060058BF RID: 22719 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000589")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6005203")]
			[Address(RVA = "0x1013C8424", Offset = "0x13C8424", VA = "0x1013C8424")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005E6 RID: 1510
		// (get) Token: 0x060058C0 RID: 22720 RVA: 0x0001D2F8 File Offset: 0x0001B4F8
		// (set) Token: 0x060058C1 RID: 22721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700058A")]
		public bool IsNeedPositionAdjust
		{
			[Token(Token = "0x6005204")]
			[Address(RVA = "0x1013C872C", Offset = "0x13C872C", VA = "0x1013C872C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6005205")]
			[Address(RVA = "0x1013C842C", Offset = "0x13C842C", VA = "0x1013C842C")]
			set
			{
			}
		}

		// Token: 0x170005E7 RID: 1511
		// (get) Token: 0x060058C2 RID: 22722 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700058B")]
		public ADVCalcText CalcTextObj
		{
			[Token(Token = "0x6005206")]
			[Address(RVA = "0x1013C8734", Offset = "0x13C8734", VA = "0x1013C8734")]
			get
			{
				return null;
			}
		}

		// Token: 0x060058C3 RID: 22723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005207")]
		[Address(RVA = "0x1013C835C", Offset = "0x13C835C", VA = "0x1013C835C")]
		public void Setup(ADVRubyPool rubyPool)
		{
		}

		// Token: 0x060058C4 RID: 22724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005208")]
		[Address(RVA = "0x1013C873C", Offset = "0x13C873C", VA = "0x1013C873C")]
		private void Update()
		{
		}

		// Token: 0x060058C5 RID: 22725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005209")]
		[Address(RVA = "0x1013C8768", Offset = "0x13C8768", VA = "0x1013C8768")]
		private void UpdatePosition()
		{
		}

		// Token: 0x060058C6 RID: 22726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520A")]
		[Address(RVA = "0x1013C84C0", Offset = "0x13C84C0", VA = "0x1013C84C0")]
		public void SetText(string text)
		{
		}

		// Token: 0x060058C7 RID: 22727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520B")]
		[Address(RVA = "0x1013C8A6C", Offset = "0x13C8A6C", VA = "0x1013C8A6C")]
		public void CalcSize(string text)
		{
		}

		// Token: 0x060058C8 RID: 22728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520C")]
		[Address(RVA = "0x1013C8434", Offset = "0x13C8434", VA = "0x1013C8434")]
		public void SetPosition(Vector2 position)
		{
		}

		// Token: 0x060058C9 RID: 22729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520D")]
		[Address(RVA = "0x1013C8490", Offset = "0x13C8490", VA = "0x1013C8490")]
		public void SetAnchor(eADVAnchor anchor)
		{
		}

		// Token: 0x060058CA RID: 22730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520E")]
		[Address(RVA = "0x1013C905C", Offset = "0x13C905C", VA = "0x1013C905C")]
		public void AddRuby(ADVParser.RubyData rubyData)
		{
		}

		// Token: 0x060058CB RID: 22731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600520F")]
		[Address(RVA = "0x1013C8028", Offset = "0x13C8028", VA = "0x1013C8028")]
		public void ClearRuby()
		{
		}

		// Token: 0x060058CC RID: 22732 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005210")]
		[Address(RVA = "0x1013C9274", Offset = "0x13C9274", VA = "0x1013C9274")]
		public List<ADVParser.RubyData> GetRubyList()
		{
			return null;
		}

		// Token: 0x060058CD RID: 22733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005211")]
		[Address(RVA = "0x1013C927C", Offset = "0x13C927C", VA = "0x1013C927C")]
		public ADVNovelCaption()
		{
		}

		// Token: 0x04006BD7 RID: 27607
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C71")]
		[SerializeField]
		private Text[] m_TextObj;

		// Token: 0x04006BD8 RID: 27608
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C72")]
		[SerializeField]
		private RectTransform m_RectTransform;

		// Token: 0x04006BD9 RID: 27609
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C73")]
		private ADVCalcText m_CalcText;

		// Token: 0x04006BDA RID: 27610
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004C74")]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04006BDB RID: 27611
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004C75")]
		private List<ADVParser.RubyData> m_RubyList;

		// Token: 0x04006BDC RID: 27612
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004C76")]
		private List<Text> m_RubyObjList;

		// Token: 0x04006BDD RID: 27613
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004C77")]
		private Vector2 m_Pivot;

		// Token: 0x04006BDE RID: 27614
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004C78")]
		private float[] m_Width;

		// Token: 0x04006BDF RID: 27615
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C79")]
		private float[] m_Height;

		// Token: 0x04006BE0 RID: 27616
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C7A")]
		private bool m_Dirty;

		// Token: 0x04006BE1 RID: 27617
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004C7B")]
		private Vector2 m_Pos;

		// Token: 0x04006BE2 RID: 27618
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004C7C")]
		private bool m_IsNeedPositionAdjust;
	}
}
