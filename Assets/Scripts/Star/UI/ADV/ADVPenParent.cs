﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200118B RID: 4491
	[Token(Token = "0x2000BAC")]
	[StructLayout(3)]
	public class ADVPenParent : MonoBehaviour
	{
		// Token: 0x060058FA RID: 22778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523E")]
		[Address(RVA = "0x1013C9314", Offset = "0x13C9314", VA = "0x1013C9314")]
		public void Setup()
		{
		}

		// Token: 0x060058FB RID: 22779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600523F")]
		[Address(RVA = "0x1013C949C", Offset = "0x13C949C", VA = "0x1013C949C")]
		private void Update()
		{
		}

		// Token: 0x060058FC RID: 22780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005240")]
		[Address(RVA = "0x1013C9378", Offset = "0x13C9378", VA = "0x1013C9378")]
		public void SetPenActive(bool active)
		{
		}

		// Token: 0x060058FD RID: 22781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005241")]
		[Address(RVA = "0x1013C94A0", Offset = "0x13C94A0", VA = "0x1013C94A0")]
		public void OnClickScreen()
		{
		}

		// Token: 0x060058FE RID: 22782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005242")]
		[Address(RVA = "0x1013C95A4", Offset = "0x13C95A4", VA = "0x1013C95A4")]
		public ADVPenParent()
		{
		}

		// Token: 0x04006C02 RID: 27650
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C9C")]
		private bool m_IsPenActive;

		// Token: 0x04006C03 RID: 27651
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C9D")]
		[SerializeField]
		private ADVPenParentChildPen m_Pen;

		// Token: 0x04006C04 RID: 27652
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C9E")]
		[SerializeField]
		private ADVPenParentChildPenShadow m_PenShadow;
	}
}
