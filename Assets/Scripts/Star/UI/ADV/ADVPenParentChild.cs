﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200118D RID: 4493
	[Token(Token = "0x2000BAE")]
	[StructLayout(3)]
	public class ADVPenParentChild<iTweenWrapperType> : MonoBehaviour where iTweenWrapperType : iTweenWrapper
	{
		// Token: 0x060058FF RID: 22783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005243")]
		[Address(RVA = "0x1016CD454", Offset = "0x16CD454", VA = "0x1016CD454", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06005900 RID: 22784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005244")]
		[Address(RVA = "0x1016CD4BC", Offset = "0x16CD4BC", VA = "0x1016CD4BC")]
		private void Update()
		{
		}

		// Token: 0x06005901 RID: 22785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005245")]
		[Address(RVA = "0x1016CD5F0", Offset = "0x16CD5F0", VA = "0x1016CD5F0", Slot = "5")]
		public virtual void ChangeStateIdleUp()
		{
		}

		// Token: 0x06005902 RID: 22786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005246")]
		[Address(RVA = "0x1016CD5F4", Offset = "0x16CD5F4", VA = "0x1016CD5F4", Slot = "6")]
		public virtual void ChangeStateIdleDown()
		{
		}

		// Token: 0x06005903 RID: 22787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005247")]
		[Address(RVA = "0x1016CD5F8", Offset = "0x16CD5F8", VA = "0x1016CD5F8", Slot = "7")]
		public virtual void ChangeStateDecided()
		{
		}

		// Token: 0x06005904 RID: 22788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005248")]
		[Address(RVA = "0x1016CD5FC", Offset = "0x16CD5FC", VA = "0x1016CD5FC", Slot = "8")]
		public virtual void ChangeStateDecideDown()
		{
		}

		// Token: 0x06005905 RID: 22789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005249")]
		[Address(RVA = "0x1016CD600", Offset = "0x16CD600", VA = "0x1016CD600", Slot = "9")]
		public virtual void ChangeStateDecideUp()
		{
		}

		// Token: 0x06005906 RID: 22790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524A")]
		[Address(RVA = "0x1016CD604", Offset = "0x16CD604", VA = "0x1016CD604")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06005907 RID: 22791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524B")]
		[Address(RVA = "0x1016CD6CC", Offset = "0x16CD6CC", VA = "0x1016CD6CC")]
		public void OnClickScreen()
		{
		}

		// Token: 0x06005908 RID: 22792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524C")]
		[Address(RVA = "0x1016CD6FC", Offset = "0x16CD6FC", VA = "0x1016CD6FC")]
		public ADVPenParentChild()
		{
		}

		// Token: 0x04006C0C RID: 27660
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004CA6")]
		protected ePenChildAnimationState m_State;

		// Token: 0x04006C0D RID: 27661
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004CA7")]
		[SerializeField]
		protected iTweenWrapperType m_iTweenWrapper;

		// Token: 0x04006C0E RID: 27662
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004CA8")]
		protected RectTransform m_RectTransform;
	}
}
