﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200118E RID: 4494
	[Token(Token = "0x2000BAF")]
	[StructLayout(3)]
	public class ADVPenParentChildPen : ADVPenParentChild<iTweenLocalMoveAddWrapper>
	{
		// Token: 0x06005909 RID: 22793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524D")]
		[Address(RVA = "0x1013C95B4", Offset = "0x13C95B4", VA = "0x1013C95B4", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x0600590A RID: 22794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524E")]
		[Address(RVA = "0x1013C96A4", Offset = "0x13C96A4", VA = "0x1013C96A4", Slot = "5")]
		public override void ChangeStateIdleUp()
		{
		}

		// Token: 0x0600590B RID: 22795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600524F")]
		[Address(RVA = "0x1013C984C", Offset = "0x13C984C", VA = "0x1013C984C", Slot = "6")]
		public override void ChangeStateIdleDown()
		{
		}

		// Token: 0x0600590C RID: 22796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005250")]
		[Address(RVA = "0x1013C99F8", Offset = "0x13C99F8", VA = "0x1013C99F8", Slot = "7")]
		public override void ChangeStateDecided()
		{
		}

		// Token: 0x0600590D RID: 22797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005251")]
		[Address(RVA = "0x1013C9AD8", Offset = "0x13C9AD8", VA = "0x1013C9AD8", Slot = "8")]
		public override void ChangeStateDecideDown()
		{
		}

		// Token: 0x0600590E RID: 22798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005252")]
		[Address(RVA = "0x1013C9C0C", Offset = "0x13C9C0C", VA = "0x1013C9C0C", Slot = "9")]
		public override void ChangeStateDecideUp()
		{
		}

		// Token: 0x0600590F RID: 22799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005253")]
		[Address(RVA = "0x1013C9D3C", Offset = "0x13C9D3C", VA = "0x1013C9D3C")]
		public ADVPenParentChildPen()
		{
		}

		// Token: 0x04006C0F RID: 27663
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004CA9")]
		private Vector3 m_BeforeLocalPosition;

		// Token: 0x04006C10 RID: 27664
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004CAA")]
		private Vector3 m_AfterLocalPosition;

		// Token: 0x04006C11 RID: 27665
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004CAB")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001338D4", Offset = "0x1338D4")]
		private int m_MoveY;
	}
}
