﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200118F RID: 4495
	[Token(Token = "0x2000BB0")]
	[StructLayout(3)]
	public class ADVPenParentChildPenShadow : ADVPenParentChild<iTweenScaleToWrapper>
	{
		// Token: 0x06005910 RID: 22800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005254")]
		[Address(RVA = "0x1013C9D8C", Offset = "0x13C9D8C", VA = "0x1013C9D8C", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06005911 RID: 22801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005255")]
		[Address(RVA = "0x1013C9E0C", Offset = "0x13C9E0C", VA = "0x1013C9E0C", Slot = "5")]
		public override void ChangeStateIdleUp()
		{
		}

		// Token: 0x06005912 RID: 22802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005256")]
		[Address(RVA = "0x1013C9F7C", Offset = "0x13C9F7C", VA = "0x1013C9F7C", Slot = "6")]
		public override void ChangeStateIdleDown()
		{
		}

		// Token: 0x06005913 RID: 22803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005257")]
		[Address(RVA = "0x1013CA0EC", Offset = "0x13CA0EC", VA = "0x1013CA0EC", Slot = "7")]
		public override void ChangeStateDecided()
		{
		}

		// Token: 0x06005914 RID: 22804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005258")]
		[Address(RVA = "0x1013CA170", Offset = "0x13CA170", VA = "0x1013CA170", Slot = "8")]
		public override void ChangeStateDecideDown()
		{
		}

		// Token: 0x06005915 RID: 22805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005259")]
		[Address(RVA = "0x1013CA2A8", Offset = "0x13CA2A8", VA = "0x1013CA2A8", Slot = "9")]
		public override void ChangeStateDecideUp()
		{
		}

		// Token: 0x06005916 RID: 22806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600525A")]
		[Address(RVA = "0x1013CA3E0", Offset = "0x13CA3E0", VA = "0x1013CA3E0")]
		public ADVPenParentChildPenShadow()
		{
		}

		// Token: 0x04006C12 RID: 27666
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004CAC")]
		private Vector3 m_BeforeLocalScale;

		// Token: 0x04006C13 RID: 27667
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004CAD")]
		private Vector3 m_AfterLocalScale;

		// Token: 0x04006C14 RID: 27668
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004CAE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133920", Offset = "0x133920")]
		private float m_AfterLocalScaleX;
	}
}
