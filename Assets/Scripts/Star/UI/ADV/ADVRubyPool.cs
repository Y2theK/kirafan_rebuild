﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001190 RID: 4496
	[Token(Token = "0x2000BB1")]
	[StructLayout(3)]
	public class ADVRubyPool : MonoBehaviour
	{
		// Token: 0x06005917 RID: 22807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600525B")]
		[Address(RVA = "0x1013CA430", Offset = "0x13CA430", VA = "0x1013CA430")]
		public void Setup()
		{
		}

		// Token: 0x06005918 RID: 22808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600525C")]
		[Address(RVA = "0x1013CA49C", Offset = "0x13CA49C", VA = "0x1013CA49C")]
		private void CreateRubyText()
		{
		}

		// Token: 0x06005919 RID: 22809 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600525D")]
		[Address(RVA = "0x1013C622C", Offset = "0x13C622C", VA = "0x1013C622C")]
		public Text ScoopRuby()
		{
			return null;
		}

		// Token: 0x0600591A RID: 22810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600525E")]
		[Address(RVA = "0x1013C6384", Offset = "0x13C6384", VA = "0x1013C6384")]
		public void SinkRuby(Text obj)
		{
		}

		// Token: 0x0600591B RID: 22811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600525F")]
		[Address(RVA = "0x1013CA580", Offset = "0x13CA580", VA = "0x1013CA580")]
		public void ClearRuby()
		{
		}

		// Token: 0x0600591C RID: 22812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005260")]
		[Address(RVA = "0x1013CA624", Offset = "0x13CA624", VA = "0x1013CA624")]
		public void Destroy()
		{
		}

		// Token: 0x0600591D RID: 22813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005261")]
		[Address(RVA = "0x1013CA780", Offset = "0x13CA780", VA = "0x1013CA780")]
		public ADVRubyPool()
		{
		}

		// Token: 0x04006C15 RID: 27669
		[Token(Token = "0x4004CAF")]
		private const int DEFAULT_INSTANCE_NUM = 16;

		// Token: 0x04006C16 RID: 27670
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004CB0")]
		[SerializeField]
		private Text m_RubyPrefab;

		// Token: 0x04006C17 RID: 27671
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004CB1")]
		private List<Text> m_RubyListPool;

		// Token: 0x04006C18 RID: 27672
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004CB2")]
		private List<Text> m_ActiveRubyList;

		// Token: 0x04006C19 RID: 27673
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004CB3")]
		private RectTransform m_RectTransform;

		// Token: 0x04006C1A RID: 27674
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004CB4")]
		private Color m_DefaultColor;
	}
}
