﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001191 RID: 4497
	[Token(Token = "0x2000BB2")]
	[StructLayout(3)]
	public class ADVSystemPanel : UIGroup
	{
		// Token: 0x0600591E RID: 22814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005262")]
		[Address(RVA = "0x1013CA810", Offset = "0x13CA810", VA = "0x1013CA810")]
		public void Start()
		{
		}

		// Token: 0x0600591F RID: 22815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005263")]
		[Address(RVA = "0x1013CA9D4", Offset = "0x13CA9D4", VA = "0x1013CA9D4")]
		private new void Update()
		{
		}

		// Token: 0x06005920 RID: 22816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005264")]
		[Address(RVA = "0x1013CA9DC", Offset = "0x13CA9DC", VA = "0x1013CA9DC", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06005921 RID: 22817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005265")]
		[Address(RVA = "0x1013CAA04", Offset = "0x13CAA04", VA = "0x1013CAA04", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06005922 RID: 22818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005266")]
		[Address(RVA = "0x1013CAA3C", Offset = "0x13CAA3C", VA = "0x1013CAA3C", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06005923 RID: 22819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005267")]
		[Address(RVA = "0x1013CAA44", Offset = "0x13CAA44", VA = "0x1013CAA44")]
		public void Auto(bool immediate)
		{
		}

		// Token: 0x06005924 RID: 22820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005268")]
		[Address(RVA = "0x1013CAA48", Offset = "0x13CAA48", VA = "0x1013CAA48")]
		public void StopAuto(bool immediate)
		{
		}

		// Token: 0x06005925 RID: 22821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005269")]
		[Address(RVA = "0x1013CAA4C", Offset = "0x13CAA4C", VA = "0x1013CAA4C")]
		public void Toggle()
		{
		}

		// Token: 0x06005926 RID: 22822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526A")]
		[Address(RVA = "0x1013CAA0C", Offset = "0x13CAA0C", VA = "0x1013CAA0C")]
		private void ChangeMenuState(ADVSystemPanel.eMenuState state, bool immediate = false)
		{
		}

		// Token: 0x06005927 RID: 22823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526B")]
		[Address(RVA = "0x1013CAB34", Offset = "0x13CAB34", VA = "0x1013CAB34")]
		private void ChangeMenuStateOpen(bool immediate = false)
		{
		}

		// Token: 0x06005928 RID: 22824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526C")]
		[Address(RVA = "0x1013CAA78", Offset = "0x13CAA78", VA = "0x1013CAA78")]
		private void ChangeMenuStateClose(bool immediate = false)
		{
		}

		// Token: 0x06005929 RID: 22825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526D")]
		[Address(RVA = "0x1013CABD4", Offset = "0x13CABD4", VA = "0x1013CABD4")]
		private void ChangeMenuStateAuto(bool immediate = false)
		{
		}

		// Token: 0x0600592A RID: 22826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526E")]
		[Address(RVA = "0x1013CB164", Offset = "0x13CB164", VA = "0x1013CB164")]
		public void EnableRender()
		{
		}

		// Token: 0x0600592B RID: 22827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600526F")]
		[Address(RVA = "0x1013CACF0", Offset = "0x13CACF0", VA = "0x1013CACF0")]
		private void MoveToMenu(Vector3 initPos, Vector3 destPos)
		{
		}

		// Token: 0x0600592C RID: 22828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005270")]
		[Address(RVA = "0x1013CAC78", Offset = "0x13CAC78", VA = "0x1013CAC78")]
		private void SetToggleArrowScaleX(float scale)
		{
		}

		// Token: 0x0600592D RID: 22829 RVA: 0x0001D388 File Offset: 0x0001B588
		[Token(Token = "0x6005271")]
		[Address(RVA = "0x1013CB194", Offset = "0x13CB194", VA = "0x1013CB194")]
		public bool IsMenuOpen()
		{
			return default(bool);
		}

		// Token: 0x0600592E RID: 22830 RVA: 0x0001D3A0 File Offset: 0x0001B5A0
		[Token(Token = "0x6005272")]
		[Address(RVA = "0x1013CB1A4", Offset = "0x13CB1A4", VA = "0x1013CB1A4", Slot = "16")]
		protected override bool CheckInEnd()
		{
			return default(bool);
		}

		// Token: 0x0600592F RID: 22831 RVA: 0x0001D3B8 File Offset: 0x0001B5B8
		[Token(Token = "0x6005273")]
		[Address(RVA = "0x1013CB240", Offset = "0x13CB240", VA = "0x1013CB240", Slot = "17")]
		protected override bool CheckOutEnd()
		{
			return default(bool);
		}

		// Token: 0x06005930 RID: 22832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005274")]
		[Address(RVA = "0x1013CB2DC", Offset = "0x13CB2DC", VA = "0x1013CB2DC")]
		public void OnClickToggle()
		{
		}

		// Token: 0x06005931 RID: 22833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005275")]
		[Address(RVA = "0x1013CB2E0", Offset = "0x13CB2E0", VA = "0x1013CB2E0")]
		public void OnClickAuto()
		{
		}

		// Token: 0x06005932 RID: 22834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005276")]
		[Address(RVA = "0x1013CB344", Offset = "0x13CB344", VA = "0x1013CB344")]
		public ADVSystemPanel()
		{
		}

		// Token: 0x04006C1B RID: 27675
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004CB5")]
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04006C1C RID: 27676
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004CB6")]
		[SerializeField]
		private float m_AnimDuration;

		// Token: 0x04006C1D RID: 27677
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4004CB7")]
		[SerializeField]
		private iTween.EaseType m_EaseType;

		// Token: 0x04006C1E RID: 27678
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004CB8")]
		[SerializeField]
		private Button m_ToggleButton;

		// Token: 0x04006C1F RID: 27679
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004CB9")]
		private Vector3 m_ShowPos;

		// Token: 0x04006C20 RID: 27680
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004CBA")]
		private Vector3 m_HidePos;

		// Token: 0x04006C21 RID: 27681
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004CBB")]
		private Vector3 m_AutoPos;

		// Token: 0x04006C22 RID: 27682
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4004CBC")]
		private ADVSystemPanel.eMenuState m_MenuState;

		// Token: 0x04006C23 RID: 27683
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004CBD")]
		private RectTransform m_Transform;

		// Token: 0x02001192 RID: 4498
		[Token(Token = "0x200129F")]
		public enum eMenuState
		{
			// Token: 0x04006C25 RID: 27685
			[Token(Token = "0x4007295")]
			Hide,
			// Token: 0x04006C26 RID: 27686
			[Token(Token = "0x4007296")]
			Idle,
			// Token: 0x04006C27 RID: 27687
			[Token(Token = "0x4007297")]
			AutoMode
		}
	}
}
