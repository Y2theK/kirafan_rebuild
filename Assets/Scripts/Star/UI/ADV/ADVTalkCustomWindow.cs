﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001196 RID: 4502
	[Token(Token = "0x2000BB5")]
	[StructLayout(3)]
	public class ADVTalkCustomWindow : MonoBehaviour
	{
		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x0600597D RID: 22909 RVA: 0x0001D4F0 File Offset: 0x0001B6F0
		[Token(Token = "0x170005A4")]
		public ADVTalkCustomWindow.eTextState TextState
		{
			[Token(Token = "0x60052C1")]
			[Address(RVA = "0x1013CB35C", Offset = "0x13CB35C", VA = "0x1013CB35C")]
			get
			{
				return ADVTalkCustomWindow.eTextState.None;
			}
		}

		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x0600597E RID: 22910 RVA: 0x0001D508 File Offset: 0x0001B708
		[Token(Token = "0x170005A5")]
		public float DefaultFontSize
		{
			[Token(Token = "0x60052C2")]
			[Address(RVA = "0x1013CB364", Offset = "0x13CB364", VA = "0x1013CB364")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x0600597F RID: 22911 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005A6")]
		public ADVCalcText CalcTextObj
		{
			[Token(Token = "0x60052C3")]
			[Address(RVA = "0x1013CB36C", Offset = "0x13CB36C", VA = "0x1013CB36C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06005980 RID: 22912 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005A7")]
		public ADVRubyPool RubyPool
		{
			[Token(Token = "0x60052C4")]
			[Address(RVA = "0x1013CB374", Offset = "0x13CB374", VA = "0x1013CB374")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06005981 RID: 22913 RVA: 0x0001D520 File Offset: 0x0001B720
		// (set) Token: 0x06005982 RID: 22914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005A8")]
		public bool IsEnableAuto
		{
			[Token(Token = "0x60052C5")]
			[Address(RVA = "0x1013CB37C", Offset = "0x13CB37C", VA = "0x1013CB37C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60052C6")]
			[Address(RVA = "0x1013CB384", Offset = "0x13CB384", VA = "0x1013CB384")]
			set
			{
			}
		}

		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06005983 RID: 22915 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005A9")]
		public GameObject AutoIcon
		{
			[Token(Token = "0x60052C7")]
			[Address(RVA = "0x1013CB39C", Offset = "0x13CB39C", VA = "0x1013CB39C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06005984 RID: 22916 RVA: 0x0001D538 File Offset: 0x0001B738
		[Token(Token = "0x170005AA")]
		public bool IsSetuped
		{
			[Token(Token = "0x60052C8")]
			[Address(RVA = "0x1013CB3A4", Offset = "0x13CB3A4", VA = "0x1013CB3A4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005985 RID: 22917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052C9")]
		[Address(RVA = "0x1013CB3AC", Offset = "0x13CB3AC", VA = "0x1013CB3AC")]
		public void Setup()
		{
		}

		// Token: 0x06005986 RID: 22918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CA")]
		[Address(RVA = "0x1013CB3B4", Offset = "0x13CB3B4", VA = "0x1013CB3B4")]
		public void Setup(bool manualUpdate)
		{
		}

		// Token: 0x06005987 RID: 22919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CB")]
		[Address(RVA = "0x1013CBD80", Offset = "0x13CBD80", VA = "0x1013CBD80")]
		public void SetupParser(ADVParser parser)
		{
		}

		// Token: 0x06005988 RID: 22920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CC")]
		[Address(RVA = "0x1013CBDF0", Offset = "0x13CBDF0", VA = "0x1013CBDF0")]
		public void Destroy()
		{
		}

		// Token: 0x06005989 RID: 22921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CD")]
		[Address(RVA = "0x1013CBEAC", Offset = "0x13CBEAC", VA = "0x1013CBEAC")]
		private void Update()
		{
		}

		// Token: 0x0600598A RID: 22922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CE")]
		[Address(RVA = "0x1013CC538", Offset = "0x13CC538", VA = "0x1013CC538")]
		public void ManualUpdate()
		{
		}

		// Token: 0x0600598B RID: 22923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052CF")]
		[Address(RVA = "0x1013CC548", Offset = "0x13CC548", VA = "0x1013CC548")]
		public void ManualUpdate2()
		{
		}

		// Token: 0x0600598C RID: 22924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D0")]
		[Address(RVA = "0x1013CBFC0", Offset = "0x13CBFC0", VA = "0x1013CBFC0")]
		private void UpdateProcess()
		{
		}

		// Token: 0x0600598D RID: 22925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D1")]
		[Address(RVA = "0x1013CC488", Offset = "0x13CC488", VA = "0x1013CC488")]
		private void UpdateProcess2()
		{
		}

		// Token: 0x0600598E RID: 22926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D2")]
		[Address(RVA = "0x1013CCA5C", Offset = "0x13CCA5C", VA = "0x1013CCA5C")]
		public void NewPage()
		{
		}

		// Token: 0x0600598F RID: 22927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D3")]
		[Address(RVA = "0x1013CCA9C", Offset = "0x13CCA9C", VA = "0x1013CCA9C")]
		public void NewPage(string oldPageTextsAll)
		{
		}

		// Token: 0x06005990 RID: 22928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D4")]
		[Address(RVA = "0x1013CB7B4", Offset = "0x13CB7B4", VA = "0x1013CB7B4")]
		public void SetTalker(string talker)
		{
		}

		// Token: 0x06005991 RID: 22929 RVA: 0x0001D550 File Offset: 0x0001B750
		[Token(Token = "0x60052D5")]
		[Address(RVA = "0x1013CCDB0", Offset = "0x13CCDB0", VA = "0x1013CCDB0")]
		public Vector2 CalcLastPosition(string text)
		{
			return default(Vector2);
		}

		// Token: 0x06005992 RID: 22930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D6")]
		[Address(RVA = "0x1013CCDF8", Offset = "0x13CCDF8", VA = "0x1013CCDF8")]
		private void InitializeText(string text, ADVTalkCustomWindow.eTextState state)
		{
		}

		// Token: 0x06005993 RID: 22931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D7")]
		[Address(RVA = "0x1013CCFB8", Offset = "0x13CCFB8", VA = "0x1013CCFB8")]
		public void SetText(string text, ADVTalkCustomWindow.eTextState state = ADVTalkCustomWindow.eTextState.Start)
		{
		}

		// Token: 0x06005994 RID: 22932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D8")]
		[Address(RVA = "0x1013CC638", Offset = "0x13CC638", VA = "0x1013CC638")]
		private void UpdateText(string text)
		{
		}

		// Token: 0x06005995 RID: 22933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052D9")]
		[Address(RVA = "0x1013CD034", Offset = "0x13CD034", VA = "0x1013CD034")]
		public void StartDisplayText()
		{
		}

		// Token: 0x06005996 RID: 22934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052DA")]
		[Address(RVA = "0x1013CD074", Offset = "0x13CD074", VA = "0x1013CD074")]
		public void SkipTextProgress()
		{
		}

		// Token: 0x06005997 RID: 22935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052DB")]
		[Address(RVA = "0x1013CC928", Offset = "0x13CC928", VA = "0x1013CC928")]
		public void FlushText()
		{
		}

		// Token: 0x06005998 RID: 22936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052DC")]
		[Address(RVA = "0x1013CC78C", Offset = "0x13CC78C", VA = "0x1013CC78C")]
		public void AddRuby(ADVParser.RubyData rubyData)
		{
		}

		// Token: 0x06005999 RID: 22937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052DD")]
		[Address(RVA = "0x1013CCEA4", Offset = "0x13CCEA4", VA = "0x1013CCEA4")]
		public void ClearRuby()
		{
		}

		// Token: 0x0600599A RID: 22938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60052DE")]
		[Address(RVA = "0x1013CD254", Offset = "0x13CD254", VA = "0x1013CD254")]
		public List<ADVParser.RubyData> GetRubyList()
		{
			return null;
		}

		// Token: 0x0600599B RID: 22939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052DF")]
		[Address(RVA = "0x1013CB390", Offset = "0x13CB390", VA = "0x1013CB390")]
		public void EnableAuto(bool flag)
		{
		}

		// Token: 0x0600599C RID: 22940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E0")]
		[Address(RVA = "0x1013CB908", Offset = "0x13CB908", VA = "0x1013CB908")]
		public void EnableAutoIcon(bool flag)
		{
		}

		// Token: 0x0600599D RID: 22941 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60052E1")]
		[Address(RVA = "0x1013CCAD0", Offset = "0x13CCAD0", VA = "0x1013CCAD0")]
		public string ConvertNowAllTextToNowDisplayText(string nowAllText)
		{
			return null;
		}

		// Token: 0x0600599E RID: 22942 RVA: 0x0001D568 File Offset: 0x0001B768
		[Token(Token = "0x60052E2")]
		[Address(RVA = "0x1013CD25C", Offset = "0x13CD25C", VA = "0x1013CD25C")]
		public ADVTalkCustomWindow.eTextState GetTextState()
		{
			return ADVTalkCustomWindow.eTextState.None;
		}

		// Token: 0x0600599F RID: 22943 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60052E3")]
		[Address(RVA = "0x1013CC558", Offset = "0x13CC558", VA = "0x1013CC558")]
		public string GetNowDisplayMessage()
		{
			return null;
		}

		// Token: 0x060059A0 RID: 22944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E4")]
		[Address(RVA = "0x1013CCCC8", Offset = "0x13CCCC8", VA = "0x1013CCCC8")]
		private void SetNowDisplayMessage(string text)
		{
		}

		// Token: 0x060059A1 RID: 22945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E5")]
		[Address(RVA = "0x1013CD264", Offset = "0x13CD264", VA = "0x1013CD264")]
		public void SetTalkWindowFrameSizeDeltaY(float y)
		{
		}

		// Token: 0x060059A2 RID: 22946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E6")]
		[Address(RVA = "0x1013CD318", Offset = "0x13CD318", VA = "0x1013CD318")]
		public void AddTalkWindowTextLocalPositionY(float y)
		{
		}

		// Token: 0x060059A3 RID: 22947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E7")]
		[Address(RVA = "0x1013CD3A0", Offset = "0x13CD3A0", VA = "0x1013CD3A0")]
		public void SetLineSpacing(float lineSpacing)
		{
		}

		// Token: 0x060059A4 RID: 22948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E8")]
		[Address(RVA = "0x1013CD3E0", Offset = "0x13CD3E0", VA = "0x1013CD3E0")]
		public void SetFontSize(int fontSize)
		{
		}

		// Token: 0x060059A5 RID: 22949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052E9")]
		[Address(RVA = "0x1013CD420", Offset = "0x13CD420", VA = "0x1013CD420")]
		public void SetTextProgressPerSec(float textProgressPerSec)
		{
		}

		// Token: 0x060059A6 RID: 22950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052EA")]
		[Address(RVA = "0x1013CB9C8", Offset = "0x13CB9C8", VA = "0x1013CB9C8")]
		public void SetTalkWindowFrameSprite(Sprite talkWindowFrameSprite)
		{
		}

		// Token: 0x060059A7 RID: 22951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052EB")]
		[Address(RVA = "0x1013CD428", Offset = "0x13CD428", VA = "0x1013CD428")]
		public void SetBalloonProtrsionData(Sprite balloonProtrsionSprite, RectTransform fitBalloonProtrsionRect)
		{
		}

		// Token: 0x060059A8 RID: 22952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052EC")]
		[Address(RVA = "0x1013CBAE4", Offset = "0x13CBAE4", VA = "0x1013CBAE4")]
		public void SetImageData(Image destination, Sprite sprite, RectTransform fitRect)
		{
		}

		// Token: 0x060059A9 RID: 22953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052ED")]
		[Address(RVA = "0x1013CC630", Offset = "0x13CC630", VA = "0x1013CC630")]
		public void SetWait(float time)
		{
		}

		// Token: 0x060059AA RID: 22954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052EE")]
		[Address(RVA = "0x1013CB85C", Offset = "0x13CB85C", VA = "0x1013CB85C")]
		public void SetPenActive(bool active)
		{
		}

		// Token: 0x060059AB RID: 22955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052EF")]
		[Address(RVA = "0x1013CD43C", Offset = "0x13CD43C", VA = "0x1013CD43C")]
		public void SetAutoPage(bool active)
		{
		}

		// Token: 0x060059AC RID: 22956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F0")]
		[Address(RVA = "0x1013CD444", Offset = "0x13CD444", VA = "0x1013CD444")]
		public void SetForceDisactiveAutoIcon(bool active)
		{
		}

		// Token: 0x060059AD RID: 22957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F1")]
		[Address(RVA = "0x1013CD44C", Offset = "0x13CD44C", VA = "0x1013CD44C")]
		public void SetIsActiveTapToNextPage(bool active)
		{
		}

		// Token: 0x060059AE RID: 22958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F2")]
		[Address(RVA = "0x1013CD454", Offset = "0x13CD454", VA = "0x1013CD454")]
		public void SetIsActiveTapToProgressSkip(bool active)
		{
		}

		// Token: 0x060059AF RID: 22959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F3")]
		[Address(RVA = "0x1013CD45C", Offset = "0x13CD45C", VA = "0x1013CD45C")]
		public void SetIsActiveTapToAutoCancelBreak(bool active)
		{
		}

		// Token: 0x060059B0 RID: 22960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F4")]
		[Address(RVA = "0x1013CD464", Offset = "0x13CD464", VA = "0x1013CD464")]
		public void SetAutoWait(float wait)
		{
		}

		// Token: 0x060059B1 RID: 22961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F5")]
		[Address(RVA = "0x1013CD46C", Offset = "0x13CD46C", VA = "0x1013CD46C")]
		public void SetAutoPageWait(float wait)
		{
		}

		// Token: 0x060059B2 RID: 22962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F6")]
		[Address(RVA = "0x1013CB78C", Offset = "0x13CB78C", VA = "0x1013CB78C")]
		public void SetHeightVariable(bool active)
		{
		}

		// Token: 0x060059B3 RID: 22963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F7")]
		[Address(RVA = "0x1013CB7A0", Offset = "0x13CB7A0", VA = "0x1013CB7A0")]
		public void SetTapToStartOverRowProgress(bool active)
		{
		}

		// Token: 0x060059B4 RID: 22964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F8")]
		[Address(RVA = "0x1013CD474", Offset = "0x13CD474", VA = "0x1013CD474")]
		public void SetTalkWindowType(ADVTalkCustomWindow.eTalkWindowType type)
		{
		}

		// Token: 0x060059B5 RID: 22965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052F9")]
		[Address(RVA = "0x1013CD47C", Offset = "0x13CD47C", VA = "0x1013CD47C")]
		public void OnClickScreen()
		{
		}

		// Token: 0x060059B6 RID: 22966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052FA")]
		[Address(RVA = "0x1013CD4A0", Offset = "0x13CD4A0", VA = "0x1013CD4A0")]
		public void OnClickScreenTalkWindowProcess()
		{
		}

		// Token: 0x060059B7 RID: 22967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052FB")]
		[Address(RVA = "0x1013CD58C", Offset = "0x13CD58C", VA = "0x1013CD58C")]
		public void OnClickScreenPenProcess()
		{
		}

		// Token: 0x060059B8 RID: 22968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052FC")]
		[Address(RVA = "0x1013CD624", Offset = "0x13CD624", VA = "0x1013CD624")]
		public ADVTalkCustomWindow()
		{
		}

		// Token: 0x04006C46 RID: 27718
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004CD9")]
		private ADVTalkCustomWindow.eTextState m_TextState;

		// Token: 0x04006C47 RID: 27719
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004CDA")]
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04006C48 RID: 27720
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004CDB")]
		private RectTransform m_TextRectTransform;

		// Token: 0x04006C49 RID: 27721
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004CDC")]
		private float m_DefaultFontSize;

		// Token: 0x04006C4A RID: 27722
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004CDD")]
		private ADVParser m_Parser;

		// Token: 0x04006C4B RID: 27723
		[Token(Token = "0x4004CDE")]
		private const int DEFAULT_RUBY_INSTANCE = 8;

		// Token: 0x04006C4C RID: 27724
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004CDF")]
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04006C4D RID: 27725
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004CE0")]
		private List<ADVParser.RubyData> m_RubyList;

		// Token: 0x04006C4E RID: 27726
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004CE1")]
		private List<Text> m_RubyObjList;

		// Token: 0x04006C4F RID: 27727
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004CE2")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133BF0", Offset = "0x133BF0")]
		private bool m_ManualUpdate;

		// Token: 0x04006C50 RID: 27728
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4004CE3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133C3C", Offset = "0x133C3C")]
		private float m_TextProgressPerSec;

		// Token: 0x04006C51 RID: 27729
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004CE4")]
		[SerializeField]
		private ADVCalcText m_CalcTextObj;

		// Token: 0x04006C52 RID: 27730
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004CE5")]
		private string m_NowMessageAll;

		// Token: 0x04006C53 RID: 27731
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004CE6")]
		private List<string> m_NowMessageDisplayeds;

		// Token: 0x04006C54 RID: 27732
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004CE7")]
		private int m_TextCount;

		// Token: 0x04006C55 RID: 27733
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4004CE8")]
		private float m_TextCountTimer;

		// Token: 0x04006C56 RID: 27734
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004CE9")]
		private bool m_IsAuto;

		// Token: 0x04006C57 RID: 27735
		[Cpp2IlInjected.FieldOffset(Offset = "0x81")]
		[Token(Token = "0x4004CEA")]
		[SerializeField]
		private bool m_AutoPage;

		// Token: 0x04006C58 RID: 27736
		[Cpp2IlInjected.FieldOffset(Offset = "0x82")]
		[Token(Token = "0x4004CEB")]
		[SerializeField]
		private bool m_IsActiveTapToNextPage;

		// Token: 0x04006C59 RID: 27737
		[Cpp2IlInjected.FieldOffset(Offset = "0x83")]
		[Token(Token = "0x4004CEC")]
		[SerializeField]
		private bool m_IsActiveTapToProgressSkip;

		// Token: 0x04006C5A RID: 27738
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4004CED")]
		private bool m_IsActiveTapToAutoCancelBreak;

		// Token: 0x04006C5B RID: 27739
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004CEE")]
		[SerializeField]
		private float m_AutoWait;

		// Token: 0x04006C5C RID: 27740
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4004CEF")]
		[SerializeField]
		private float m_AutoPageWait;

		// Token: 0x04006C5D RID: 27741
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004CF0")]
		private float m_AutoWaitCount;

		// Token: 0x04006C5E RID: 27742
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004CF1")]
		[SerializeField]
		private Image m_TalkWindowFrame;

		// Token: 0x04006C5F RID: 27743
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004CF2")]
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x04006C60 RID: 27744
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004CF3")]
		[SerializeField]
		private Text m_TalkerObj;

		// Token: 0x04006C61 RID: 27745
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004CF4")]
		[SerializeField]
		private GameObject m_TalkerGameObject;

		// Token: 0x04006C62 RID: 27746
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004CF5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133D28", Offset = "0x133D28")]
		private string m_TalkerName;

		// Token: 0x04006C63 RID: 27747
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004CF6")]
		[SerializeField]
		private Image m_BalloonProtrsion;

		// Token: 0x04006C64 RID: 27748
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004CF7")]
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x04006C65 RID: 27749
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004CF8")]
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x04006C66 RID: 27750
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004CF9")]
		[SerializeField]
		private ADVPenParent m_PenParent;

		// Token: 0x04006C67 RID: 27751
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004CFA")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133DB4", Offset = "0x133DB4")]
		private bool m_IsPenActive;

		// Token: 0x04006C68 RID: 27752
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4004CFB")]
		[SerializeField]
		private ADVTalkCustomWindow.eTalkWindowType m_WindowType;

		// Token: 0x04006C69 RID: 27753
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004CFC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133E10", Offset = "0x133E10")]
		private bool m_IsHeightVariable;

		// Token: 0x04006C6A RID: 27754
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004CFD")]
		private RectTransform m_TalkerLabelRectTransform;

		// Token: 0x04006C6B RID: 27755
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004CFE")]
		private RectTransform m_TalkWindowRectTransform;

		// Token: 0x04006C6C RID: 27756
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004CFF")]
		private float m_TextBaseHeight;

		// Token: 0x04006C6D RID: 27757
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4004D00")]
		private float m_TextRowHeight;

		// Token: 0x04006C6E RID: 27758
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004D01")]
		private float m_DefaultRows;

		// Token: 0x04006C6F RID: 27759
		[Cpp2IlInjected.FieldOffset(Offset = "0x10C")]
		[Token(Token = "0x4004D02")]
		private float m_TalkerLabelBaseY;

		// Token: 0x04006C70 RID: 27760
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004D03")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133E5C", Offset = "0x133E5C")]
		private bool m_TapToStartOverRowProgress;

		// Token: 0x04006C71 RID: 27761
		[Cpp2IlInjected.FieldOffset(Offset = "0x114")]
		[Token(Token = "0x4004D04")]
		private int m_NowDisplayPageIndex;

		// Token: 0x04006C72 RID: 27762
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004D05")]
		private RectTransform m_AutoIconImageRectTransform;

		// Token: 0x04006C73 RID: 27763
		[Token(Token = "0x4004D06")]
		private const float m_AutoIconImageRotateZ = 120f;

		// Token: 0x04006C74 RID: 27764
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004D07")]
		[SerializeField]
		private GameObject m_AutoIcon;

		// Token: 0x04006C75 RID: 27765
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004D08")]
		private bool m_ForceDisactiveAutoIcon;

		// Token: 0x04006C76 RID: 27766
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4004D09")]
		private float m_WaitTimer;

		// Token: 0x04006C77 RID: 27767
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004D0A")]
		private bool m_IsSetuped;

		// Token: 0x04006C78 RID: 27768
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004D0B")]
		private List<ADVParser.EventType> m_msgEventList;

		// Token: 0x04006C79 RID: 27769
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004D0C")]
		private bool m_StrongTextFlag;

		// Token: 0x02001197 RID: 4503
		[Token(Token = "0x20012A1")]
		public enum eTalkWindowType
		{
			// Token: 0x04006C7B RID: 27771
			[Token(Token = "0x400729C")]
			Normal,
			// Token: 0x04006C7C RID: 27772
			[Token(Token = "0x400729D")]
			HeightVariable,
			// Token: 0x04006C7D RID: 27773
			[Token(Token = "0x400729E")]
			TapToStartOverRowProgress
		}

		// Token: 0x02001198 RID: 4504
		[Token(Token = "0x20012A2")]
		public enum eTextState
		{
			// Token: 0x04006C7F RID: 27775
			[Token(Token = "0x40072A0")]
			None,
			// Token: 0x04006C80 RID: 27776
			[Token(Token = "0x40072A1")]
			StandBy,
			// Token: 0x04006C81 RID: 27777
			[Token(Token = "0x40072A2")]
			Start,
			// Token: 0x04006C82 RID: 27778
			[Token(Token = "0x40072A3")]
			Progress,
			// Token: 0x04006C83 RID: 27779
			[Token(Token = "0x40072A4")]
			TapToStartOverRowProgress,
			// Token: 0x04006C84 RID: 27780
			[Token(Token = "0x40072A5")]
			TapWait,
			// Token: 0x04006C85 RID: 27781
			[Token(Token = "0x40072A6")]
			Stop,
			// Token: 0x04006C86 RID: 27782
			[Token(Token = "0x40072A7")]
			Invalid
		}
	}
}
