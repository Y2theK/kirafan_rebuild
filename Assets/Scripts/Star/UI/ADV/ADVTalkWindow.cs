﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02001193 RID: 4499
	[Token(Token = "0x2000BB3")]
	[StructLayout(3)]
	public class ADVTalkWindow : UIGroup
	{
		// Token: 0x170005F1 RID: 1521
		// (get) Token: 0x06005933 RID: 22835 RVA: 0x0001D3D0 File Offset: 0x0001B5D0
		[Token(Token = "0x17000595")]
		public ADVTalkCustomWindow.eTextState TextState
		{
			[Token(Token = "0x6005277")]
			[Address(RVA = "0x1013CD730", Offset = "0x13CD730", VA = "0x1013CD730")]
			get
			{
				return ADVTalkCustomWindow.eTextState.None;
			}
		}

		// Token: 0x170005F2 RID: 1522
		// (get) Token: 0x06005934 RID: 22836 RVA: 0x0001D3E8 File Offset: 0x0001B5E8
		[Token(Token = "0x17000596")]
		public float DefaultFontSize
		{
			[Token(Token = "0x6005278")]
			[Address(RVA = "0x1013CD7C4", Offset = "0x13CD7C4", VA = "0x1013CD7C4")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170005F3 RID: 1523
		// (get) Token: 0x06005935 RID: 22837 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000597")]
		public ADVCalcText CalcTextObj
		{
			[Token(Token = "0x6005279")]
			[Address(RVA = "0x1013CD854", Offset = "0x13CD854", VA = "0x1013CD854")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005F4 RID: 1524
		// (get) Token: 0x06005936 RID: 22838 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000598")]
		public ADVRubyPool RubyPool
		{
			[Token(Token = "0x600527A")]
			[Address(RVA = "0x1013CD8E8", Offset = "0x13CD8E8", VA = "0x1013CD8E8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005F5 RID: 1525
		// (get) Token: 0x06005937 RID: 22839 RVA: 0x0001D400 File Offset: 0x0001B600
		// (set) Token: 0x06005938 RID: 22840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000599")]
		public bool IsEnableAuto
		{
			[Token(Token = "0x600527B")]
			[Address(RVA = "0x1013CD97C", Offset = "0x13CD97C", VA = "0x1013CD97C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600527C")]
			[Address(RVA = "0x1013CDA18", Offset = "0x13CDA18", VA = "0x1013CDA18")]
			set
			{
			}
		}

		// Token: 0x170005F6 RID: 1526
		// (get) Token: 0x06005939 RID: 22841 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700059A")]
		public GameObject AutoIcon
		{
			[Token(Token = "0x600527D")]
			[Address(RVA = "0x1013CDAD0", Offset = "0x13CDAD0", VA = "0x1013CDAD0")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600593A RID: 22842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600527E")]
		[Address(RVA = "0x1013CDB64", Offset = "0x13CDB64", VA = "0x1013CDB64")]
		public void Setup()
		{
		}

		// Token: 0x0600593B RID: 22843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600527F")]
		[Address(RVA = "0x1013CDC44", Offset = "0x13CDC44", VA = "0x1013CDC44")]
		public void SetupParser(ADVParser parser)
		{
		}

		// Token: 0x0600593C RID: 22844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005280")]
		[Address(RVA = "0x1013CDCF0", Offset = "0x13CDCF0", VA = "0x1013CDCF0")]
		public void Destroy()
		{
		}

		// Token: 0x0600593D RID: 22845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005281")]
		[Address(RVA = "0x1013CDD88", Offset = "0x13CDD88", VA = "0x1013CDD88", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x0600593E RID: 22846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005282")]
		[Address(RVA = "0x1013CE088", Offset = "0x13CE088", VA = "0x1013CE088", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x0600593F RID: 22847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005283")]
		[Address(RVA = "0x1013CE090", Offset = "0x13CE090", VA = "0x1013CE090", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06005940 RID: 22848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005284")]
		[Address(RVA = "0x1013CE098", Offset = "0x13CE098", VA = "0x1013CE098")]
		public void ManualUpdate()
		{
		}

		// Token: 0x06005941 RID: 22849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005285")]
		[Address(RVA = "0x1013CE138", Offset = "0x13CE138", VA = "0x1013CE138")]
		public void ManualUpdate2()
		{
		}

		// Token: 0x06005942 RID: 22850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005286")]
		[Address(RVA = "0x1013CE1D8", Offset = "0x13CE1D8", VA = "0x1013CE1D8")]
		public void SetTalker(string talker)
		{
		}

		// Token: 0x06005943 RID: 22851 RVA: 0x0001D418 File Offset: 0x0001B618
		[Token(Token = "0x6005287")]
		[Address(RVA = "0x1013CE284", Offset = "0x13CE284", VA = "0x1013CE284")]
		public Vector2 CalcLastPosition(string text)
		{
			return default(Vector2);
		}

		// Token: 0x06005944 RID: 22852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005288")]
		[Address(RVA = "0x1013CE32C", Offset = "0x13CE32C", VA = "0x1013CE32C")]
		public void SetText(string text, ADVTalkCustomWindow.eTextState state = ADVTalkCustomWindow.eTextState.Start)
		{
		}

		// Token: 0x06005945 RID: 22853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005289")]
		[Address(RVA = "0x1013CE3E0", Offset = "0x13CE3E0", VA = "0x1013CE3E0")]
		public void StartDisplayText()
		{
		}

		// Token: 0x06005946 RID: 22854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600528A")]
		[Address(RVA = "0x1013CE4A0", Offset = "0x13CE4A0", VA = "0x1013CE4A0")]
		public void SkipTextProgress()
		{
		}

		// Token: 0x06005947 RID: 22855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600528B")]
		[Address(RVA = "0x1013CE538", Offset = "0x13CE538", VA = "0x1013CE538")]
		public void FlushText()
		{
		}

		// Token: 0x06005948 RID: 22856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600528C")]
		[Address(RVA = "0x1013CE5D0", Offset = "0x13CE5D0", VA = "0x1013CE5D0")]
		public void AddRuby(ADVParser.RubyData rubyData)
		{
		}

		// Token: 0x06005949 RID: 22857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600528D")]
		[Address(RVA = "0x1013CE67C", Offset = "0x13CE67C", VA = "0x1013CE67C")]
		public void ClearRuby()
		{
		}

		// Token: 0x0600594A RID: 22858 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600528E")]
		[Address(RVA = "0x1013CE714", Offset = "0x13CE714", VA = "0x1013CE714")]
		public List<ADVParser.RubyData> GetRubyList()
		{
			return null;
		}

		// Token: 0x0600594B RID: 22859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600528F")]
		[Address(RVA = "0x1013CDA1C", Offset = "0x13CDA1C", VA = "0x1013CDA1C")]
		public void EnableAuto(bool flag)
		{
		}

		// Token: 0x0600594C RID: 22860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005290")]
		[Address(RVA = "0x1013CE7CC", Offset = "0x13CE7CC", VA = "0x1013CE7CC")]
		public void EnableAutoIcon(bool flag)
		{
		}

		// Token: 0x0600594D RID: 22861 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6005291")]
		[Address(RVA = "0x1013CE878", Offset = "0x13CE878", VA = "0x1013CE878")]
		public string GetNowDisplayMessage()
		{
			return null;
		}

		// Token: 0x0600594E RID: 22862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005292")]
		[Address(RVA = "0x1013CE914", Offset = "0x13CE914", VA = "0x1013CE914")]
		public void SetPenActive(bool active)
		{
		}

		// Token: 0x0600594F RID: 22863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005293")]
		[Address(RVA = "0x1013CE9C0", Offset = "0x13CE9C0", VA = "0x1013CE9C0")]
		public void SetAutoPage(bool active)
		{
		}

		// Token: 0x06005950 RID: 22864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005294")]
		[Address(RVA = "0x1013CE9C8", Offset = "0x13CE9C8", VA = "0x1013CE9C8")]
		public void SetIsActiveTapToNextPage(bool active)
		{
		}

		// Token: 0x06005951 RID: 22865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005295")]
		[Address(RVA = "0x1013CE9D0", Offset = "0x13CE9D0", VA = "0x1013CE9D0")]
		public void SetIsActiveTapToProgressSkip(bool active)
		{
		}

		// Token: 0x06005952 RID: 22866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005296")]
		[Address(RVA = "0x1013CE9D8", Offset = "0x13CE9D8", VA = "0x1013CE9D8")]
		public void SetHeightVariable(bool active)
		{
		}

		// Token: 0x06005953 RID: 22867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005297")]
		[Address(RVA = "0x1013CEA7C", Offset = "0x13CEA7C", VA = "0x1013CEA7C")]
		public void OnClickScreen()
		{
		}

		// Token: 0x06005954 RID: 22868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005298")]
		[Address(RVA = "0x1013CEB1C", Offset = "0x13CEB1C", VA = "0x1013CEB1C")]
		public void OnClickScreenTalkWindowProcess()
		{
		}

		// Token: 0x06005955 RID: 22869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005299")]
		[Address(RVA = "0x1013CEBB4", Offset = "0x13CEBB4", VA = "0x1013CEBB4")]
		public void OnClickScreenPenProcess()
		{
		}

		// Token: 0x06005956 RID: 22870 RVA: 0x0001D430 File Offset: 0x0001B630
		[Token(Token = "0x600529A")]
		[Address(RVA = "0x1013CDB68", Offset = "0x13CDB68", VA = "0x1013CDB68")]
		protected bool AcquireTalkCustomWindow()
		{
			return default(bool);
		}

		// Token: 0x06005957 RID: 22871 RVA: 0x0001D448 File Offset: 0x0001B648
		[Token(Token = "0x600529B")]
		[Address(RVA = "0x1013CDE54", Offset = "0x13CDE54", VA = "0x1013CDE54")]
		protected bool SetupCustomWindow()
		{
			return default(bool);
		}

		// Token: 0x06005958 RID: 22872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600529C")]
		[Address(RVA = "0x1013CEC4C", Offset = "0x13CEC4C", VA = "0x1013CEC4C")]
		public ADVTalkWindow()
		{
		}

		// Token: 0x04006C28 RID: 27688
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004CBE")]
		private ADVTalkCustomWindow m_TalkCustomWindow;

		// Token: 0x04006C29 RID: 27689
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004CBF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001339BC", Offset = "0x1339BC")]
		private float m_TextProgressPerSec;

		// Token: 0x04006C2A RID: 27690
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004CC0")]
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x04006C2B RID: 27691
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004CC1")]
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x04006C2C RID: 27692
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004CC2")]
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x04006C2D RID: 27693
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004CC3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133A38", Offset = "0x133A38")]
		private bool m_IsPenActive;

		// Token: 0x04006C2E RID: 27694
		[Cpp2IlInjected.FieldOffset(Offset = "0xB1")]
		[Token(Token = "0x4004CC4")]
		[SerializeField]
		private bool m_AutoPage;

		// Token: 0x04006C2F RID: 27695
		[Cpp2IlInjected.FieldOffset(Offset = "0xB2")]
		[Token(Token = "0x4004CC5")]
		[SerializeField]
		private bool m_IsActiveTapToNextPage;

		// Token: 0x04006C30 RID: 27696
		[Cpp2IlInjected.FieldOffset(Offset = "0xB3")]
		[Token(Token = "0x4004CC6")]
		[SerializeField]
		private bool m_IsActiveTapToProgressSkip;

		// Token: 0x04006C31 RID: 27697
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4004CC7")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100133AB4", Offset = "0x133AB4")]
		private bool m_IsHeightVariable;
	}
}
