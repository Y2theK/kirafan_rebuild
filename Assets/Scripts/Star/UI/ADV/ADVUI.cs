﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02001194 RID: 4500
	[Token(Token = "0x2000BB4")]
	[StructLayout(3)]
	public class ADVUI : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x170005F7 RID: 1527
		// (get) Token: 0x06005959 RID: 22873 RVA: 0x0001D460 File Offset: 0x0001B660
		[Token(Token = "0x1700059B")]
		public bool IsRunning
		{
			[Token(Token = "0x600529D")]
			[Address(RVA = "0x1013CEC6C", Offset = "0x13CEC6C", VA = "0x1013CEC6C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005F8 RID: 1528
		// (get) Token: 0x0600595A RID: 22874 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700059C")]
		public ADVTalkWindow talkWindow
		{
			[Token(Token = "0x600529E")]
			[Address(RVA = "0x1013CEC74", Offset = "0x13CEC74", VA = "0x1013CEC74")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x0600595B RID: 22875 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700059D")]
		public ADVNovel novel
		{
			[Token(Token = "0x600529F")]
			[Address(RVA = "0x1013CEC7C", Offset = "0x13CEC7C", VA = "0x1013CEC7C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x0600595C RID: 22876 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700059E")]
		public ADVSystemPanel systemPanel
		{
			[Token(Token = "0x60052A0")]
			[Address(RVA = "0x1013CEC84", Offset = "0x13CEC84", VA = "0x1013CEC84")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x0600595D RID: 22877 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700059F")]
		public ADVFade fade
		{
			[Token(Token = "0x60052A1")]
			[Address(RVA = "0x1013CEC8C", Offset = "0x13CEC8C", VA = "0x1013CEC8C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x0600595E RID: 22878 RVA: 0x0001D478 File Offset: 0x0001B678
		[Token(Token = "0x170005A0")]
		public ADVUI.eTextMode TextMode
		{
			[Token(Token = "0x60052A2")]
			[Address(RVA = "0x1013CEC94", Offset = "0x13CEC94", VA = "0x1013CEC94")]
			get
			{
				return ADVUI.eTextMode.TalkWindow;
			}
		}

		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x0600595F RID: 22879 RVA: 0x0001D490 File Offset: 0x0001B690
		// (set) Token: 0x06005960 RID: 22880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005A1")]
		public bool IsEnableInput
		{
			[Token(Token = "0x60052A3")]
			[Address(RVA = "0x1013CEC9C", Offset = "0x13CEC9C", VA = "0x1013CEC9C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60052A4")]
			[Address(RVA = "0x1013CECA4", Offset = "0x13CECA4", VA = "0x1013CECA4")]
			set
			{
			}
		}

		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x06005961 RID: 22881 RVA: 0x0001D4A8 File Offset: 0x0001B6A8
		// (set) Token: 0x06005962 RID: 22882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170005A2")]
		public bool IsEnableRender
		{
			[Token(Token = "0x60052A5")]
			[Address(RVA = "0x1013CECF8", Offset = "0x13CECF8", VA = "0x1013CECF8")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60052A6")]
			[Address(RVA = "0x1013CED00", Offset = "0x13CED00", VA = "0x1013CED00")]
			set
			{
			}
		}

		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x06005963 RID: 22883 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170005A3")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60052A7")]
			[Address(RVA = "0x1013CEDA8", Offset = "0x13CEDA8", VA = "0x1013CEDA8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005964 RID: 22884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052A8")]
		[Address(RVA = "0x1013CEDB0", Offset = "0x13CEDB0", VA = "0x1013CEDB0")]
		public void Setup(ADVParser parser)
		{
		}

		// Token: 0x06005965 RID: 22885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052A9")]
		[Address(RVA = "0x1013CEF14", Offset = "0x13CEF14", VA = "0x1013CEF14")]
		public void Destroy()
		{
		}

		// Token: 0x06005966 RID: 22886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052AA")]
		[Address(RVA = "0x1013CEFAC", Offset = "0x13CEFAC", VA = "0x1013CEFAC")]
		private void Update()
		{
		}

		// Token: 0x06005967 RID: 22887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052AB")]
		[Address(RVA = "0x1013CEFB0", Offset = "0x13CEFB0", VA = "0x1013CEFB0")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x06005968 RID: 22888 RVA: 0x0001D4C0 File Offset: 0x0001B6C0
		[Token(Token = "0x60052AC")]
		[Address(RVA = "0x1013CF0E8", Offset = "0x13CF0E8", VA = "0x1013CF0E8")]
		private bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06005969 RID: 22889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052AD")]
		[Address(RVA = "0x1013CF258", Offset = "0x13CF258", VA = "0x1013CF258", Slot = "4")]
		public void OnPointerClick(PointerEventData eventData)
		{
		}

		// Token: 0x0600596A RID: 22890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052AE")]
		[Address(RVA = "0x1013CECA8", Offset = "0x13CECA8", VA = "0x1013CECA8")]
		public void EnableInput(bool flag)
		{
		}

		// Token: 0x0600596B RID: 22891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052AF")]
		[Address(RVA = "0x1013CED04", Offset = "0x13CED04", VA = "0x1013CED04")]
		public void EnableRender(bool flag)
		{
		}

		// Token: 0x0600596C RID: 22892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B0")]
		[Address(RVA = "0x1013CF29C", Offset = "0x13CF29C", VA = "0x1013CF29C")]
		public void EnableAuto(bool flag, bool anim = false)
		{
		}

		// Token: 0x0600596D RID: 22893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B1")]
		[Address(RVA = "0x1013CF348", Offset = "0x13CF348", VA = "0x1013CF348")]
		public void EnableAutoIcon(bool flag, bool anim = false)
		{
		}

		// Token: 0x0600596E RID: 22894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B2")]
		[Address(RVA = "0x1013CF2F0", Offset = "0x13CF2F0", VA = "0x1013CF2F0")]
		public void EnableMenuPanelAutoAnimation(bool flag, bool immediate)
		{
		}

		// Token: 0x0600596F RID: 22895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B3")]
		[Address(RVA = "0x1013CF3B4", Offset = "0x13CF3B4", VA = "0x1013CF3B4")]
		public void OpenBackLog()
		{
		}

		// Token: 0x06005970 RID: 22896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B4")]
		[Address(RVA = "0x1013CF41C", Offset = "0x13CF41C", VA = "0x1013CF41C")]
		public void CloseBackLog()
		{
		}

		// Token: 0x06005971 RID: 22897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B5")]
		[Address(RVA = "0x1013CF460", Offset = "0x13CF460", VA = "0x1013CF460")]
		public void SetTextMode(ADVUI.eTextMode mode)
		{
		}

		// Token: 0x06005972 RID: 22898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B6")]
		[Address(RVA = "0x1013CF4D0", Offset = "0x13CF4D0", VA = "0x1013CF4D0")]
		public void AddBackLogText(string ADVCharaID, string dispTalker, string text, List<ADVParser.RubyData> rubyList)
		{
		}

		// Token: 0x06005973 RID: 22899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B7")]
		[Address(RVA = "0x1013CF16C", Offset = "0x13CF16C", VA = "0x1013CF16C")]
		public void OpenSkipWindow()
		{
		}

		// Token: 0x06005974 RID: 22900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B8")]
		[Address(RVA = "0x1013CF52C", Offset = "0x13CF52C", VA = "0x1013CF52C")]
		public void CloseSkipWindow()
		{
		}

		// Token: 0x06005975 RID: 22901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052B9")]
		[Address(RVA = "0x1013CF5D4", Offset = "0x13CF5D4", VA = "0x1013CF5D4")]
		public void AddSkipLogText(string ADVCharaID, string dispTalker, string text, List<ADVParser.RubyData> rubyList)
		{
		}

		// Token: 0x06005976 RID: 22902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052BA")]
		[Address(RVA = "0x1013CF630", Offset = "0x13CF630", VA = "0x1013CF630")]
		public void OpenSkipLog()
		{
		}

		// Token: 0x06005977 RID: 22903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052BB")]
		[Address(RVA = "0x1013CF72C", Offset = "0x13CF72C", VA = "0x1013CF72C")]
		public void CloseSkipLog()
		{
		}

		// Token: 0x06005978 RID: 22904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052BC")]
		[Address(RVA = "0x1013CF80C", Offset = "0x13CF80C", VA = "0x1013CF80C")]
		public void Skip()
		{
		}

		// Token: 0x06005979 RID: 22905 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60052BD")]
		[Address(RVA = "0x1013CF858", Offset = "0x13CF858", VA = "0x1013CF858")]
		public ADVCalcText GetBackLogCalcText()
		{
			return null;
		}

		// Token: 0x0600597A RID: 22906 RVA: 0x0001D4D8 File Offset: 0x0001B6D8
		[Token(Token = "0x60052BE")]
		[Address(RVA = "0x1013CF884", Offset = "0x13CF884", VA = "0x1013CF884")]
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			return default(Vector3);
		}

		// Token: 0x0600597B RID: 22907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052BF")]
		[Address(RVA = "0x1013CF904", Offset = "0x13CF904", VA = "0x1013CF904")]
		public void OnClickSkipWindowFooter()
		{
		}

		// Token: 0x0600597C RID: 22908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60052C0")]
		[Address(RVA = "0x1013CF96C", Offset = "0x13CF96C", VA = "0x1013CF96C")]
		public ADVUI()
		{
		}

		// Token: 0x04006C32 RID: 27698
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004CC8")]
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04006C33 RID: 27699
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004CC9")]
		[SerializeField]
		private ADVTalkWindow m_TalkWindow;

		// Token: 0x04006C34 RID: 27700
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004CCA")]
		[SerializeField]
		private ADVNovel m_ADVNovel;

		// Token: 0x04006C35 RID: 27701
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004CCB")]
		[SerializeField]
		private ADVSystemPanel m_SystemPanel;

		// Token: 0x04006C36 RID: 27702
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004CCC")]
		[SerializeField]
		private CustomButton m_AutoButton;

		// Token: 0x04006C37 RID: 27703
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004CCD")]
		[SerializeField]
		private Image m_BG;

		// Token: 0x04006C38 RID: 27704
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004CCE")]
		[SerializeField]
		private ADVBackLog m_BackLog;

		// Token: 0x04006C39 RID: 27705
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004CCF")]
		[SerializeField]
		private UIGroup m_SkipWindow;

		// Token: 0x04006C3A RID: 27706
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004CD0")]
		[SerializeField]
		private ADVBackLog m_SkipLog;

		// Token: 0x04006C3B RID: 27707
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004CD1")]
		[SerializeField]
		private ADVFade m_Fade;

		// Token: 0x04006C3C RID: 27708
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004CD2")]
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04006C3D RID: 27709
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004CD3")]
		private ADVUI.eTextMode m_TextMode;

		// Token: 0x04006C3E RID: 27710
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4004CD4")]
		private bool m_IsEnableInput;

		// Token: 0x04006C3F RID: 27711
		[Cpp2IlInjected.FieldOffset(Offset = "0x75")]
		[Token(Token = "0x4004CD5")]
		private bool m_IsEnableRender;

		// Token: 0x04006C40 RID: 27712
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004CD6")]
		[SerializeField]
		private ADVBackLogItem m_BackLogItemPrefab;

		// Token: 0x04006C41 RID: 27713
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004CD7")]
		[SerializeField]
		private ADVBackLogItem m_BackLogItem;

		// Token: 0x04006C42 RID: 27714
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004CD8")]
		private RectTransform m_RectTransform;

		// Token: 0x02001195 RID: 4501
		[Token(Token = "0x20012A0")]
		public enum eTextMode
		{
			// Token: 0x04006C44 RID: 27716
			[Token(Token = "0x4007299")]
			TalkWindow,
			// Token: 0x04006C45 RID: 27717
			[Token(Token = "0x400729A")]
			Novel
		}
	}
}
