﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.ADV
{
	// Token: 0x0200118C RID: 4492
	[Token(Token = "0x2000BAD")]
	[StructLayout(3, Size = 4)]
	public enum ePenChildAnimationState
	{
		// Token: 0x04006C06 RID: 27654
		[Token(Token = "0x4004CA0")]
		None,
		// Token: 0x04006C07 RID: 27655
		[Token(Token = "0x4004CA1")]
		IdleUp,
		// Token: 0x04006C08 RID: 27656
		[Token(Token = "0x4004CA2")]
		IdleDown,
		// Token: 0x04006C09 RID: 27657
		[Token(Token = "0x4004CA3")]
		Decided,
		// Token: 0x04006C0A RID: 27658
		[Token(Token = "0x4004CA4")]
		DecideDown,
		// Token: 0x04006C0B RID: 27659
		[Token(Token = "0x4004CA5")]
		DecideUp
	}
}
