﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CD5 RID: 3285
	[Token(Token = "0x20008C5")]
	[StructLayout(3)]
	public class ADVLibraryIcon : ASyncImage
	{
		// Token: 0x06003C20 RID: 15392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DD")]
		[Address(RVA = "0x1013CF97C", Offset = "0x13CF97C", VA = "0x1013CF97C")]
		public void Apply(int advLibID)
		{
		}

		// Token: 0x06003C21 RID: 15393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DE")]
		[Address(RVA = "0x1013CFA8C", Offset = "0x13CFA8C", VA = "0x1013CFA8C", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C22 RID: 15394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DF")]
		[Address(RVA = "0x1013CFC28", Offset = "0x13CFC28", VA = "0x1013CFC28")]
		public ADVLibraryIcon()
		{
		}

		// Token: 0x04004B5D RID: 19293
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40034FA")]
		protected int m_AdvLibID;
	}
}
