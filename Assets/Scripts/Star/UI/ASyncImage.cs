﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class ASyncImage : MonoBehaviour {
        protected RectTransform m_RectTransform;
        protected Image m_Image;
        protected SpriteHandler m_SpriteHandler;
        protected bool m_IsDoneApply;

        public RectTransform RectTransform => m_RectTransform;
        public Image Image => m_Image;

        public virtual void Awake() {
            m_RectTransform = GetComponent<RectTransform>();
            m_Image = GetComponent<Image>();
        }

        public virtual void Start() {
            if (m_RectTransform == null) {
                m_RectTransform = GetComponent<RectTransform>();
            }
            if (m_Image == null) {
                m_Image = GetComponent<Image>();
            }
            if (!m_IsDoneApply && m_Image != null) {
                m_Image.enabled = false;
            }
        }

        public virtual void LateUpdate() {
            if (m_Image.sprite == null && m_SpriteHandler != null && m_SpriteHandler.IsAvailable()) {
                ApplyLoadedSprite();
            } else if (m_Image.enabled && m_SpriteHandler != null && !m_SpriteHandler.IsAvailable()) {
                ReleaseSprite();
            }
        }

        protected virtual void ApplyLoadedSprite() {
            m_Image.sprite = m_SpriteHandler.Obj;
            m_Image.enabled = true;
        }

        protected virtual void ReleaseSprite() {
            m_Image.sprite = null;
            m_Image.enabled = false;
        }

        private void OnDestroy() {
            Destroy();
        }

        public Sprite GetSprite() {
            return IsDoneLoad() ? m_SpriteHandler.Obj : null;
        }

        public virtual bool IsDoneLoad() {
            return m_SpriteHandler != null && m_SpriteHandler.IsAvailable();
        }

        public virtual SpriteHandler GetSpriteHandler() {
            return m_SpriteHandler;
        }

        protected virtual void Apply() {
            if (m_Image == null) {
                m_Image = GetComponent<Image>();
            }
            if (m_IsDoneApply) {
                Destroy();
            }
            m_IsDoneApply = true;
        }

        public virtual void Destroy() {
            if (m_Image != null) {
                m_Image.enabled = false;
                m_Image.sprite = null;
            }
            if (m_SpriteHandler != null && GameSystem.Inst != null && GameSystem.Inst.IsAvailable()) {
                GameSystem.Inst.SpriteMng.Unload(m_SpriteHandler);
            }
            m_SpriteHandler = null;
            m_IsDoneApply = false;
        }
    }
}
