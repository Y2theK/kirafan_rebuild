﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C56 RID: 3158
	[Token(Token = "0x2000876")]
	[StructLayout(3)]
	public abstract class ASyncImageAdapter
	{
		// Token: 0x06003941 RID: 14657
		[Token(Token = "0x600342B")]
		[Address(Slot = "4")]
		public abstract void Apply(int charaID, int weaponID);

		// Token: 0x06003942 RID: 14658
		[Token(Token = "0x600342C")]
		[Address(Slot = "5")]
		public abstract void Destroy();

		// Token: 0x06003943 RID: 14659 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600342D")]
		[Address(RVA = "0x1013D0104", Offset = "0x13D0104", VA = "0x1013D0104")]
		public AnimUIPlayer GetAnimUIPlayer()
		{
			return null;
		}

		// Token: 0x06003944 RID: 14660
		[Token(Token = "0x600342E")]
		[Address(Slot = "6")]
		public abstract bool IsDoneLoad();

		// Token: 0x06003945 RID: 14661
		[Token(Token = "0x600342F")]
		[Address(Slot = "7")]
		protected abstract MonoBehaviour GetMonoBehaviour();

		// Token: 0x06003946 RID: 14662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003430")]
		[Address(RVA = "0x1013D01B8", Offset = "0x13D01B8", VA = "0x1013D01B8")]
		protected ASyncImageAdapter()
		{
		}
	}
}
