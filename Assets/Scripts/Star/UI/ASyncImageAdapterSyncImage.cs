﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C57 RID: 3159
	[Token(Token = "0x2000877")]
	[StructLayout(3)]
	public abstract class ASyncImageAdapterSyncImage : ASyncImageAdapter
	{
		// Token: 0x06003947 RID: 14663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003431")]
		[Address(RVA = "0x1013D01C0", Offset = "0x13D01C0", VA = "0x1013D01C0", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06003948 RID: 14664 RVA: 0x00017E80 File Offset: 0x00016080
		[Token(Token = "0x6003432")]
		[Address(RVA = "0x1013D026C", Offset = "0x13D026C", VA = "0x1013D026C", Slot = "6")]
		public override bool IsDoneLoad()
		{
			return default(bool);
		}

		// Token: 0x06003949 RID: 14665
		[Token(Token = "0x6003433")]
		[Address(Slot = "8")]
		protected abstract ASyncImage GetASyncImage();

		// Token: 0x0600394A RID: 14666 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003434")]
		[Address(RVA = "0x1013D031C", Offset = "0x13D031C", VA = "0x1013D031C", Slot = "7")]
		protected override MonoBehaviour GetMonoBehaviour()
		{
			return null;
		}

		// Token: 0x0600394B RID: 14667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003435")]
		[Address(RVA = "0x1013D0328", Offset = "0x13D0328", VA = "0x1013D0328")]
		protected ASyncImageAdapterSyncImage()
		{
		}
	}
}
