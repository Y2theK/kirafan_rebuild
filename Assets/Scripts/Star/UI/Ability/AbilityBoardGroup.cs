﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001154 RID: 4436
	[Token(Token = "0x2000B81")]
	[StructLayout(3)]
	public class AbilityBoardGroup : UIGroup
	{
		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x06005787 RID: 22407 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000579")]
		public AbilitySlotInfo SlotInfo
		{
			[Token(Token = "0x60050D1")]
			[Address(RVA = "0x1013D0330", Offset = "0x13D0330", VA = "0x1013D0330")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x06005788 RID: 22408 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700057A")]
		public AbilitySlot[] AbilitySlot
		{
			[Token(Token = "0x60050D2")]
			[Address(RVA = "0x1013D0338", Offset = "0x13D0338", VA = "0x1013D0338")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005789 RID: 22409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D3")]
		[Address(RVA = "0x1013D0340", Offset = "0x13D0340", VA = "0x1013D0340")]
		public void Setup(EditState_Ability ownerState)
		{
		}

		// Token: 0x0600578A RID: 22410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D4")]
		[Address(RVA = "0x1013D058C", Offset = "0x13D058C", VA = "0x1013D058C")]
		public void ApplySlotList()
		{
		}

		// Token: 0x0600578B RID: 22411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D5")]
		[Address(RVA = "0x1013D0DA0", Offset = "0x13D0DA0", VA = "0x1013D0DA0")]
		public void Destroy()
		{
		}

		// Token: 0x0600578C RID: 22412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D6")]
		[Address(RVA = "0x1013D1044", Offset = "0x13D1044", VA = "0x1013D1044", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600578D RID: 22413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D7")]
		[Address(RVA = "0x1013D11E0", Offset = "0x13D11E0", VA = "0x1013D11E0", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x0600578E RID: 22414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D8")]
		[Address(RVA = "0x1013D121C", Offset = "0x13D121C", VA = "0x1013D121C")]
		public void LoadBackGround()
		{
		}

		// Token: 0x0600578F RID: 22415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D9")]
		[Address(RVA = "0x1013D10F0", Offset = "0x13D10F0", VA = "0x1013D10F0")]
		public void ApplyBackGround()
		{
		}

		// Token: 0x06005790 RID: 22416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DA")]
		[Address(RVA = "0x1013D0B90", Offset = "0x13D0B90", VA = "0x1013D0B90")]
		private void ApplyCharacterName(string name, eRare rare, eElementType elementType, eClassType classType)
		{
		}

		// Token: 0x06005791 RID: 22417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DB")]
		[Address(RVA = "0x1013D12AC", Offset = "0x13D12AC", VA = "0x1013D12AC")]
		private void ShowAbilitySlotList_Unlocked(int dispSlotNumber, int itemID, bool isActiveSetButton)
		{
		}

		// Token: 0x06005792 RID: 22418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DC")]
		[Address(RVA = "0x1013D19D8", Offset = "0x13D19D8", VA = "0x1013D19D8")]
		private void ShowAbilitySlotList_Locked(int dispSlotNumber, int itemID, int itemAmount, int amount, bool isActiveButton, int prevSlotNumber)
		{
		}

		// Token: 0x06005793 RID: 22419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DD")]
		[Address(RVA = "0x1013D2224", Offset = "0x13D2224", VA = "0x1013D2224")]
		private void BlinkAbilitySlot(int slotIndex)
		{
		}

		// Token: 0x06005794 RID: 22420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DE")]
		[Address(RVA = "0x1013D0CC8", Offset = "0x13D0CC8", VA = "0x1013D0CC8")]
		private void HideAbilitySlotList()
		{
		}

		// Token: 0x06005795 RID: 22421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050DF")]
		[Address(RVA = "0x1013D2354", Offset = "0x13D2354", VA = "0x1013D2354")]
		public void HideCurrentSlotUI()
		{
		}

		// Token: 0x06005796 RID: 22422 RVA: 0x0001CFC8 File Offset: 0x0001B1C8
		[Token(Token = "0x60050E0")]
		[Address(RVA = "0x1013D237C", Offset = "0x13D237C", VA = "0x1013D237C")]
		public Vector2 GetSlotPosition(int slotIndex)
		{
			return default(Vector2);
		}

		// Token: 0x06005797 RID: 22423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E1")]
		[Address(RVA = "0x1013D24C8", Offset = "0x13D24C8", VA = "0x1013D24C8")]
		private void OnClickAbilitySlotCallback(int slotIndex)
		{
		}

		// Token: 0x06005798 RID: 22424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E2")]
		[Address(RVA = "0x1013D2B60", Offset = "0x13D2B60", VA = "0x1013D2B60")]
		public void OnClickBackGround()
		{
		}

		// Token: 0x06005799 RID: 22425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E3")]
		[Address(RVA = "0x1013D2B88", Offset = "0x13D2B88", VA = "0x1013D2B88")]
		public void OnClickAbilitySlotListButton()
		{
		}

		// Token: 0x0600579A RID: 22426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E4")]
		[Address(RVA = "0x1013D2D50", Offset = "0x13D2D50", VA = "0x1013D2D50")]
		public void OnClickAbilitySlotListButton_Upgrade()
		{
		}

		// Token: 0x0600579B RID: 22427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E5")]
		[Address(RVA = "0x1013D2ECC", Offset = "0x13D2ECC", VA = "0x1013D2ECC")]
		public void OnClickStatusButton()
		{
		}

		// Token: 0x0600579C RID: 22428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E6")]
		[Address(RVA = "0x1013D30D0", Offset = "0x13D30D0", VA = "0x1013D30D0")]
		private void OnCloseCharaDetail()
		{
		}

		// Token: 0x0600579D RID: 22429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E7")]
		[Address(RVA = "0x1013D31BC", Offset = "0x13D31BC", VA = "0x1013D31BC")]
		public void SetupAbilitySlotUIEffect()
		{
		}

		// Token: 0x0600579E RID: 22430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E8")]
		[Address(RVA = "0x1013D31C4", Offset = "0x13D31C4", VA = "0x1013D31C4")]
		public void UpdateAbilitySlotUIEffect(float elapsedSec)
		{
		}

		// Token: 0x0600579F RID: 22431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050E9")]
		[Address(RVA = "0x1013D32A0", Offset = "0x13D32A0", VA = "0x1013D32A0")]
		public AbilityBoardGroup()
		{
		}

		// Token: 0x04006A90 RID: 27280
		[Token(Token = "0x4004B5A")]
		private const float ABILITY_SLOT_UI_EFFECT_SEC = 1.1f;

		// Token: 0x04006A91 RID: 27281
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B5B")]
		[SerializeField]
		private Image m_BackGround;

		// Token: 0x04006A92 RID: 27282
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B5C")]
		[SerializeField]
		private VariableIcon m_VariableIconPrefab;

		// Token: 0x04006A93 RID: 27283
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B5D")]
		[SerializeField]
		private AbilitySlot[] m_AbilitySlot;

		// Token: 0x04006A94 RID: 27284
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B5E")]
		[SerializeField]
		private GameObject m_AbilitySlotListParent;

		// Token: 0x04006A95 RID: 27285
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004B5F")]
		[SerializeField]
		private AbilitySlotList m_AbilitySlotList;

		// Token: 0x04006A96 RID: 27286
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004B60")]
		[SerializeField]
		private CustomButton m_AbilitySlotListButton;

		// Token: 0x04006A97 RID: 27287
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004B61")]
		[SerializeField]
		private Text m_AbilitySlotListButtonText;

		// Token: 0x04006A98 RID: 27288
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004B62")]
		[SerializeField]
		private CustomButton m_AbilitySlotListButtonUpgrade;

		// Token: 0x04006A99 RID: 27289
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004B63")]
		[SerializeField]
		private Text m_AbilitySlotListButtonUpgradeText;

		// Token: 0x04006A9A RID: 27290
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004B64")]
		[SerializeField]
		private Text m_CharacterName;

		// Token: 0x04006A9B RID: 27291
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004B65")]
		[SerializeField]
		private Image[] m_CharacterRare;

		// Token: 0x04006A9C RID: 27292
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004B66")]
		[SerializeField]
		private ElementIcon m_CharacterElement;

		// Token: 0x04006A9D RID: 27293
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004B67")]
		[SerializeField]
		private ClassIcon m_CharacterClass;

		// Token: 0x04006A9E RID: 27294
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004B68")]
		[SerializeField]
		private Text m_EquipListButtonText;

		// Token: 0x04006A9F RID: 27295
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004B69")]
		[SerializeField]
		private Text m_StatusButtonText;

		// Token: 0x04006AA0 RID: 27296
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004B6A")]
		private EditState_Ability m_OwnerState;

		// Token: 0x04006AA1 RID: 27297
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004B6B")]
		private AbilityBoardGroup.eButtonType m_ButtonType;

		// Token: 0x04006AA2 RID: 27298
		[Cpp2IlInjected.FieldOffset(Offset = "0x10C")]
		[Token(Token = "0x4004B6C")]
		private bool m_IsDoneAbilitySlotUIEffect;

		// Token: 0x04006AA3 RID: 27299
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004B6D")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04006AA4 RID: 27300
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004B6E")]
		private bool m_IsOpenStatusDetail;

		// Token: 0x04006AA5 RID: 27301
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004B6F")]
		private VariableIcon[] m_VariableIconList;

		// Token: 0x04006AA6 RID: 27302
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004B70")]
		private AbilitySlotInfo m_AbilitySlotInfo;

		// Token: 0x04006AA7 RID: 27303
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004B71")]
		public Action OnClickUnlockButtonCallback;

		// Token: 0x04006AA8 RID: 27304
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004B72")]
		public Action OnClickOpenSphereListButtonCallback;

		// Token: 0x04006AA9 RID: 27305
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004B73")]
		public Action OnClickUpgradeButtonCallback;

		// Token: 0x02001155 RID: 4437
		[Token(Token = "0x2001294")]
		private enum eButtonType
		{
			// Token: 0x04006AAB RID: 27307
			[Token(Token = "0x400726B")]
			Unlock,
			// Token: 0x04006AAC RID: 27308
			[Token(Token = "0x400726C")]
			Set,
			// Token: 0x04006AAD RID: 27309
			[Token(Token = "0x400726D")]
			Change
		}
	}
}
