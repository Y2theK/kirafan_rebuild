﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Ability
{
	// Token: 0x02001156 RID: 4438
	[Token(Token = "0x2000B82")]
	[StructLayout(3)]
	public class AbilityBoardSelectorData : ScrollItemData
	{
		// Token: 0x060057A0 RID: 22432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050EA")]
		[Address(RVA = "0x1013D3344", Offset = "0x13D3344", VA = "0x1013D3344")]
		public AbilityBoardSelectorData(int itemID, string name, Action<int> onClickItemCallback)
		{
		}

		// Token: 0x060057A1 RID: 22433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050EB")]
		[Address(RVA = "0x1013D33CC", Offset = "0x13D33CC", VA = "0x1013D33CC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04006AAE RID: 27310
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B74")]
		public int m_ItemID;

		// Token: 0x04006AAF RID: 27311
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B75")]
		public string m_Name;

		// Token: 0x04006AB0 RID: 27312
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B76")]
		public Action<int> m_OnClickItemCallback;
	}
}
