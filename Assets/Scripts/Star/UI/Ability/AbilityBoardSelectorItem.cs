﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001157 RID: 4439
	[Token(Token = "0x2000B83")]
	[StructLayout(3)]
	public class AbilityBoardSelectorItem : ScrollItemIcon
	{
		// Token: 0x060057A2 RID: 22434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050EC")]
		[Address(RVA = "0x1013D3420", Offset = "0x13D3420", VA = "0x1013D3420", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060057A3 RID: 22435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050ED")]
		[Address(RVA = "0x1013D353C", Offset = "0x13D353C", VA = "0x1013D353C", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060057A4 RID: 22436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050EE")]
		[Address(RVA = "0x1013D3540", Offset = "0x13D3540", VA = "0x1013D3540")]
		public AbilityBoardSelectorItem()
		{
		}

		// Token: 0x04006AB1 RID: 27313
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B77")]
		[SerializeField]
		private Text m_Text;
	}
}
