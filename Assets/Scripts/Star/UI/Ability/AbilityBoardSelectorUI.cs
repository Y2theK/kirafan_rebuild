﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001158 RID: 4440
	[Token(Token = "0x2000B84")]
	[StructLayout(3)]
	public class AbilityBoardSelectorUI : MenuUIBase
	{
		// Token: 0x060057A5 RID: 22437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050EF")]
		[Address(RVA = "0x1013D3548", Offset = "0x13D3548", VA = "0x1013D3548")]
		public void OnDestroy()
		{
		}

		// Token: 0x060057A6 RID: 22438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F0")]
		[Address(RVA = "0x1013D35C4", Offset = "0x13D35C4", VA = "0x1013D35C4")]
		public void Setup(EditState_CharaListAbility ownerState)
		{
		}

		// Token: 0x060057A7 RID: 22439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F1")]
		[Address(RVA = "0x1013D38E4", Offset = "0x13D38E4", VA = "0x1013D38E4")]
		public void SetupParam(List<ItemListDB_Param> itemList, Action onEndSelectorCallback)
		{
		}

		// Token: 0x060057A8 RID: 22440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F2")]
		[Address(RVA = "0x1013D3A98", Offset = "0x13D3A98", VA = "0x1013D3A98", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060057A9 RID: 22441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F3")]
		[Address(RVA = "0x1013D3590", Offset = "0x13D3590", VA = "0x1013D3590")]
		public void UnloadResource()
		{
		}

		// Token: 0x060057AA RID: 22442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F4")]
		[Address(RVA = "0x1013D3AA0", Offset = "0x13D3AA0", VA = "0x1013D3AA0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060057AB RID: 22443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F5")]
		[Address(RVA = "0x1013D3B50", Offset = "0x13D3B50", VA = "0x1013D3B50")]
		private void OnEndSelectorCallback()
		{
		}

		// Token: 0x060057AC RID: 22444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F6")]
		[Address(RVA = "0x1013D3BEC", Offset = "0x13D3BEC", VA = "0x1013D3BEC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060057AD RID: 22445 RVA: 0x0001CFE0 File Offset: 0x0001B1E0
		[Token(Token = "0x60050F7")]
		[Address(RVA = "0x1013D3C20", Offset = "0x13D3C20", VA = "0x1013D3C20", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060057AE RID: 22446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F8")]
		[Address(RVA = "0x1013D3C80", Offset = "0x13D3C80", VA = "0x1013D3C80")]
		public void OnClickCloseButtonCallback()
		{
		}

		// Token: 0x060057AF RID: 22447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050F9")]
		[Address(RVA = "0x1013D3C8C", Offset = "0x13D3C8C", VA = "0x1013D3C8C")]
		private void OnClickAbilityBoard(int itemID)
		{
		}

		// Token: 0x060057B0 RID: 22448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050FA")]
		[Address(RVA = "0x1013D3CE0", Offset = "0x13D3CE0", VA = "0x1013D3CE0")]
		public void OpenConfirmWindow(int abilityBoardItemID, long managedCharacterID, Action<int> onEndConfirmWindowCallback)
		{
		}

		// Token: 0x060057B1 RID: 22449 RVA: 0x0001CFF8 File Offset: 0x0001B1F8
		[Token(Token = "0x60050FB")]
		[Address(RVA = "0x1013D4044", Offset = "0x13D4044", VA = "0x1013D4044")]
		private int GetRequireAbilityFragmentItemID(int abilityBoardItemID)
		{
			return 0;
		}

		// Token: 0x060057B2 RID: 22450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050FC")]
		[Address(RVA = "0x1013D4294", Offset = "0x13D4294", VA = "0x1013D4294")]
		private void OnEndConfirmGroup()
		{
		}

		// Token: 0x060057B3 RID: 22451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050FD")]
		[Address(RVA = "0x1013D433C", Offset = "0x13D433C", VA = "0x1013D433C")]
		public void OnClickDecideConfirmWindow()
		{
		}

		// Token: 0x060057B4 RID: 22452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050FE")]
		[Address(RVA = "0x1013D4374", Offset = "0x13D4374", VA = "0x1013D4374")]
		public void OnClickCancelConfirmWindow()
		{
		}

		// Token: 0x060057B5 RID: 22453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050FF")]
		[Address(RVA = "0x1013D43B0", Offset = "0x13D43B0", VA = "0x1013D43B0")]
		public AbilityBoardSelectorUI()
		{
		}

		// Token: 0x04006AB2 RID: 27314
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004B78")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006AB3 RID: 27315
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004B79")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04006AB4 RID: 27316
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004B7A")]
		[SerializeField]
		private Text m_MainText;

		// Token: 0x04006AB5 RID: 27317
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B7B")]
		[SerializeField]
		private Text m_ButtonText;

		// Token: 0x04006AB6 RID: 27318
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004B7C")]
		[SerializeField]
		private UIGroup m_ConfirmGroup;

		// Token: 0x04006AB7 RID: 27319
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004B7D")]
		[SerializeField]
		private Text m_ConfirmMainText;

		// Token: 0x04006AB8 RID: 27320
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004B7E")]
		[SerializeField]
		private Text m_ConfirmWarningText;

		// Token: 0x04006AB9 RID: 27321
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004B7F")]
		[SerializeField]
		private Text m_ConfirmDecideButtonText;

		// Token: 0x04006ABA RID: 27322
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004B80")]
		[SerializeField]
		private Text m_ConfirmCancelButtonText;

		// Token: 0x04006ABB RID: 27323
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B81")]
		private EditState_CharaListAbility m_OwnerState;

		// Token: 0x04006ABC RID: 27324
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B82")]
		private int m_ConfirmButtonIndex;

		// Token: 0x04006ABD RID: 27325
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B83")]
		private Action m_OnEndSelectorWindowCallback;

		// Token: 0x04006ABE RID: 27326
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B84")]
		private Action<int> m_OnEndConfirmWindowCallback;
	}
}
