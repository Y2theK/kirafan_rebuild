﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001159 RID: 4441
	[Token(Token = "0x2000B85")]
	[StructLayout(3)]
	public class AbilityEquipListGroup : UIGroup
	{
		// Token: 0x060057B6 RID: 22454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005100")]
		[Address(RVA = "0x1013D43B8", Offset = "0x13D43B8", VA = "0x1013D43B8")]
		public void Setup()
		{
		}

		// Token: 0x060057B7 RID: 22455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005101")]
		[Address(RVA = "0x1013D44FC", Offset = "0x13D44FC", VA = "0x1013D44FC")]
		public void SetupParam(long characterManagedID)
		{
		}

		// Token: 0x060057B8 RID: 22456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005102")]
		[Address(RVA = "0x1013D4944", Offset = "0x13D4944", VA = "0x1013D4944")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060057B9 RID: 22457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005103")]
		[Address(RVA = "0x1013D4950", Offset = "0x13D4950", VA = "0x1013D4950")]
		public void Destroy()
		{
		}

		// Token: 0x060057BA RID: 22458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005104")]
		[Address(RVA = "0x1013D4988", Offset = "0x13D4988", VA = "0x1013D4988")]
		public AbilityEquipListGroup()
		{
		}

		// Token: 0x04006ABF RID: 27327
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B85")]
		[SerializeField]
		private AbilityEquipListScrollView m_Scroll;

		// Token: 0x04006AC0 RID: 27328
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B86")]
		[SerializeField]
		private Text m_ButtonText;
	}
}
