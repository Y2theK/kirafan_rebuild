﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Ability
{
	// Token: 0x0200115A RID: 4442
	[Token(Token = "0x2000B86")]
	[StructLayout(3)]
	public class AbilityEquipListScrollData : ScrollItemData
	{
		// Token: 0x060057BB RID: 22459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005105")]
		[Address(RVA = "0x1013D48D4", Offset = "0x13D48D4", VA = "0x1013D48D4")]
		public AbilityEquipListScrollData(bool released, int dispSlotNumber, int itemID, int itemAmount, int amount, int prevSlotNumber)
		{
		}

		// Token: 0x04006AC1 RID: 27329
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B87")]
		public bool m_Released;

		// Token: 0x04006AC2 RID: 27330
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004B88")]
		public int m_DispSlotNumber;

		// Token: 0x04006AC3 RID: 27331
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B89")]
		public int m_ItemID;

		// Token: 0x04006AC4 RID: 27332
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004B8A")]
		public int m_ItemAmount;

		// Token: 0x04006AC5 RID: 27333
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B8B")]
		public int m_Amount;

		// Token: 0x04006AC6 RID: 27334
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004B8C")]
		public int m_PrevSlotNumber;
	}
}
