﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Ability
{
	// Token: 0x0200115B RID: 4443
	[Token(Token = "0x2000B87")]
	[StructLayout(3)]
	public class AbilityEquipListScrollItem : ScrollItemIcon
	{
		// Token: 0x060057BC RID: 22460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005106")]
		[Address(RVA = "0x1013D4990", Offset = "0x13D4990", VA = "0x1013D4990", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060057BD RID: 22461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005107")]
		[Address(RVA = "0x1013D4AF8", Offset = "0x13D4AF8", VA = "0x1013D4AF8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060057BE RID: 22462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005108")]
		[Address(RVA = "0x1013D4B24", Offset = "0x13D4B24", VA = "0x1013D4B24")]
		public AbilityEquipListScrollItem()
		{
		}

		// Token: 0x04006AC7 RID: 27335
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B8D")]
		[SerializeField]
		private AbilitySlotList m_SlotList;
	}
}
