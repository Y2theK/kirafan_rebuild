﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Ability
{
	// Token: 0x0200115C RID: 4444
	[Token(Token = "0x2000B88")]
	[StructLayout(3)]
	public class AbilityEquipListScrollView : ScrollViewBase
	{
		// Token: 0x060057BF RID: 22463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005109")]
		[Address(RVA = "0x1013D4B2C", Offset = "0x13D4B2C", VA = "0x1013D4B2C", Slot = "19")]
		public override void Destroy()
		{
		}

		// Token: 0x060057C0 RID: 22464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600510A")]
		[Address(RVA = "0x1013D4B54", Offset = "0x13D4B54", VA = "0x1013D4B54")]
		public AbilityEquipListScrollView()
		{
		}
	}
}
