﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Ability
{
	// Token: 0x0200115D RID: 4445
	[Token(Token = "0x2000B89")]
	[StructLayout(3)]
	public class AbilityMaterialInfo : MonoBehaviour
	{
		// Token: 0x060057C1 RID: 22465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600510B")]
		[Address(RVA = "0x1013D4B5C", Offset = "0x13D4B5C", VA = "0x1013D4B5C")]
		public void Apply(int itemID, int needNum, int haveNum)
		{
		}

		// Token: 0x060057C2 RID: 22466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600510C")]
		[Address(RVA = "0x1013D4D34", Offset = "0x13D4D34", VA = "0x1013D4D34")]
		public AbilityMaterialInfo()
		{
		}

		// Token: 0x04006AC8 RID: 27336
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B8E")]
		[SerializeField]
		private ItemIconWithDetail m_ItemIconWithDetail;

		// Token: 0x04006AC9 RID: 27337
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B8F")]
		[SerializeField]
		private TextField m_NeedTextField;

		// Token: 0x04006ACA RID: 27338
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B90")]
		[SerializeField]
		private TextField m_HaveTextField;
	}
}
