﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x0200115E RID: 4446
	[Token(Token = "0x2000B8A")]
	[StructLayout(3)]
	public class AbilitySlot : MonoBehaviour
	{
		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x060057C3 RID: 22467 RVA: 0x0001D010 File Offset: 0x0001B210
		// (set) Token: 0x060057C4 RID: 22468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700057B")]
		public bool IsSp
		{
			[Token(Token = "0x600510D")]
			[Address(RVA = "0x1013D4D3C", Offset = "0x13D4D3C", VA = "0x1013D4D3C")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600510E")]
			[Address(RVA = "0x1013D4D44", Offset = "0x13D4D44", VA = "0x1013D4D44")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x060057C5 RID: 22469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600510F")]
		[Address(RVA = "0x1013D4D4C", Offset = "0x13D4D4C", VA = "0x1013D4D4C")]
		private void OnDestroy()
		{
		}

		// Token: 0x060057C6 RID: 22470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005110")]
		[Address(RVA = "0x1013D0CFC", Offset = "0x13D0CFC", VA = "0x1013D0CFC")]
		public void Apply(int index, bool isSp, bool isLock, Action<int> callback)
		{
		}

		// Token: 0x060057C7 RID: 22471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005111")]
		[Address(RVA = "0x1013D4E88", Offset = "0x13D4E88", VA = "0x1013D4E88")]
		private void _Apply(bool isSp, bool isLock)
		{
		}

		// Token: 0x060057C8 RID: 22472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005112")]
		[Address(RVA = "0x1013D5090", Offset = "0x13D5090", VA = "0x1013D5090")]
		public void OnClickCallback()
		{
		}

		// Token: 0x060057C9 RID: 22473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005113")]
		[Address(RVA = "0x1013D22C4", Offset = "0x13D22C4", VA = "0x1013D22C4")]
		public void Blink(bool active)
		{
		}

		// Token: 0x060057CA RID: 22474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005114")]
		[Address(RVA = "0x1013D3294", Offset = "0x13D3294", VA = "0x1013D3294")]
		public void Unlock()
		{
		}

		// Token: 0x060057CB RID: 22475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005115")]
		[Address(RVA = "0x1013D50E4", Offset = "0x13D50E4", VA = "0x1013D50E4")]
		public AbilitySlot()
		{
		}

		// Token: 0x04006ACB RID: 27339
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B91")]
		[SerializeField]
		private Sprite[] m_BlinkSprite;

		// Token: 0x04006ACC RID: 27340
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B92")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x04006ACD RID: 27341
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B93")]
		[SerializeField]
		private Image m_NormalSlot;

		// Token: 0x04006ACE RID: 27342
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004B94")]
		[SerializeField]
		private Image m_NormalSlotPlus;

		// Token: 0x04006ACF RID: 27343
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004B95")]
		[SerializeField]
		private Image m_SpSlot;

		// Token: 0x04006AD0 RID: 27344
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004B96")]
		[SerializeField]
		private Image m_SpSlotPlus;

		// Token: 0x04006AD1 RID: 27345
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004B97")]
		[SerializeField]
		private Image m_LockIcon;

		// Token: 0x04006AD2 RID: 27346
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004B98")]
		[SerializeField]
		private Image m_BlinkImage;

		// Token: 0x04006AD3 RID: 27347
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B99")]
		[SerializeField]
		private BlinkColor m_BlinkColor;

		// Token: 0x04006AD4 RID: 27348
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004B9A")]
		private int m_Index;

		// Token: 0x04006AD5 RID: 27349
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004B9B")]
		private Action<int> m_OnClickCallback;
	}
}
