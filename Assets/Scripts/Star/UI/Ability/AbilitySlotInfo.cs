﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Ability
{
	// Token: 0x0200115F RID: 4447
	[Token(Token = "0x2000B8B")]
	[StructLayout(3)]
	public class AbilitySlotInfo
	{
		// Token: 0x060057CC RID: 22476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005116")]
		[Address(RVA = "0x1013D3314", Offset = "0x13D3314", VA = "0x1013D3314")]
		public AbilitySlotInfo()
		{
		}

		// Token: 0x060057CD RID: 22477 RVA: 0x0001D028 File Offset: 0x0001B228
		[Token(Token = "0x6005117")]
		[Address(RVA = "0x1013D2B50", Offset = "0x13D2B50", VA = "0x1013D2B50")]
		public bool Available()
		{
			return default(bool);
		}

		// Token: 0x060057CE RID: 22478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005118")]
		[Address(RVA = "0x1013D2B14", Offset = "0x13D2B14", VA = "0x1013D2B14")]
		public void Clear()
		{
		}

		// Token: 0x04006AD7 RID: 27351
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004B9D")]
		public int m_SlotIndex;

		// Token: 0x04006AD8 RID: 27352
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4004B9E")]
		public int m_ItemId;

		// Token: 0x04006AD9 RID: 27353
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B9F")]
		public int m_NeedNum;

		// Token: 0x04006ADA RID: 27354
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004BA0")]
		public int m_Gold;
	}
}
