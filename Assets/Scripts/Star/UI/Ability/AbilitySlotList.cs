﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001160 RID: 4448
	[Token(Token = "0x2000B8C")]
	[StructLayout(3)]
	public class AbilitySlotList : MonoBehaviour
	{
		// Token: 0x060057CF RID: 22479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005119")]
		[Address(RVA = "0x1013D1684", Offset = "0x13D1684", VA = "0x1013D1684")]
		public void Apply(int slotNumber, int abilitySphereItemID)
		{
		}

		// Token: 0x060057D0 RID: 22480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511A")]
		[Address(RVA = "0x1013D1B98", Offset = "0x13D1B98", VA = "0x1013D1B98")]
		public void Apply(int slotNumber, int abilityFragmentItemID, int itemAmount, int amount, int prevSlotNumber)
		{
		}

		// Token: 0x060057D1 RID: 22481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511B")]
		[Address(RVA = "0x1013D1018", Offset = "0x13D1018", VA = "0x1013D1018")]
		public void Destroy()
		{
		}

		// Token: 0x060057D2 RID: 22482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511C")]
		[Address(RVA = "0x1013D52E0", Offset = "0x13D52E0", VA = "0x1013D52E0")]
		public AbilitySlotList()
		{
		}

		// Token: 0x04006ADB RID: 27355
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004BA1")]
		[SerializeField]
		private AbilitySphereMiniIcon m_SphereIcon;

		// Token: 0x04006ADC RID: 27356
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004BA2")]
		[SerializeField]
		private Text m_SlotName;

		// Token: 0x04006ADD RID: 27357
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004BA3")]
		[SerializeField]
		private GameObject m_Line1Object;

		// Token: 0x04006ADE RID: 27358
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004BA4")]
		[SerializeField]
		private Text m_Line1Text;

		// Token: 0x04006ADF RID: 27359
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004BA5")]
		[SerializeField]
		private GameObject m_Line3Object;

		// Token: 0x04006AE0 RID: 27360
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004BA6")]
		[SerializeField]
		private Text m_Line3Header;

		// Token: 0x04006AE1 RID: 27361
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004BA7")]
		[SerializeField]
		private Text m_Line3Text1;

		// Token: 0x04006AE2 RID: 27362
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004BA8")]
		[SerializeField]
		private Text m_Line3Text2;

		// Token: 0x04006AE3 RID: 27363
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004BA9")]
		[SerializeField]
		private PrefabCloner m_Line3Cloner1;

		// Token: 0x04006AE4 RID: 27364
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004BAA")]
		[SerializeField]
		private PrefabCloner m_Line3Cloner2;
	}
}
