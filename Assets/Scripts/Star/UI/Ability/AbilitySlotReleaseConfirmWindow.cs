﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001161 RID: 4449
	[Token(Token = "0x2000B8D")]
	[StructLayout(3)]
	public class AbilitySlotReleaseConfirmWindow : UIGroup
	{
		// Token: 0x060057D3 RID: 22483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511D")]
		[Address(RVA = "0x1013D52E8", Offset = "0x13D52E8", VA = "0x1013D52E8")]
		public void Setup(int slotIndex, AbilitySlotInfo slotInfo, Action OnClickYesButton)
		{
		}

		// Token: 0x060057D4 RID: 22484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511E")]
		[Address(RVA = "0x1013D5960", Offset = "0x13D5960", VA = "0x1013D5960")]
		public void OnClickNoButton()
		{
		}

		// Token: 0x060057D5 RID: 22485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600511F")]
		[Address(RVA = "0x1013D596C", Offset = "0x13D596C", VA = "0x1013D596C")]
		public void OnClickYesButton()
		{
		}

		// Token: 0x060057D6 RID: 22486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005120")]
		[Address(RVA = "0x1013D5CE4", Offset = "0x13D5CE4", VA = "0x1013D5CE4")]
		public AbilitySlotReleaseConfirmWindow()
		{
		}

		// Token: 0x04006AE5 RID: 27365
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004BAB")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04006AE6 RID: 27366
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004BAC")]
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04006AE7 RID: 27367
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004BAD")]
		[SerializeField]
		private AbilityMaterialInfo m_AbilityMaterialInfo;

		// Token: 0x04006AE8 RID: 27368
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004BAE")]
		[SerializeField]
		private TextFieldWithItem m_TextFieldGoldNeed;

		// Token: 0x04006AE9 RID: 27369
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004BAF")]
		[SerializeField]
		private TextFieldWithItem m_TextFieldGoldHave;

		// Token: 0x04006AEA RID: 27370
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004BB0")]
		[SerializeField]
		private CustomButton m_NoButton;

		// Token: 0x04006AEB RID: 27371
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004BB1")]
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x04006AEC RID: 27372
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004BB2")]
		[SerializeField]
		private Text m_NoButtonText;

		// Token: 0x04006AED RID: 27373
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004BB3")]
		[SerializeField]
		private Text m_YesButtonText;

		// Token: 0x04006AEE RID: 27374
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004BB4")]
		public Action m_OnClickYesButton;

		// Token: 0x04006AEF RID: 27375
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004BB5")]
		private bool m_NotEnoughNum;

		// Token: 0x04006AF0 RID: 27376
		[Cpp2IlInjected.FieldOffset(Offset = "0xD9")]
		[Token(Token = "0x4004BB6")]
		private bool m_NotEnoughGold;
	}
}
