﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001163 RID: 4451
	[Token(Token = "0x2000B8E")]
	[StructLayout(3)]
	public class AbilitySphereListGroup : UIGroup
	{
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x060057D9 RID: 22489 RVA: 0x0001D058 File Offset: 0x0001B258
		[Token(Token = "0x1700057C")]
		public int EquipSphereId
		{
			[Token(Token = "0x6005121")]
			[Address(RVA = "0x1013D5D44", Offset = "0x13D5D44", VA = "0x1013D5D44")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x060057DA RID: 22490 RVA: 0x0001D070 File Offset: 0x0001B270
		[Token(Token = "0x1700057D")]
		public bool IsSpSlot
		{
			[Token(Token = "0x6005122")]
			[Address(RVA = "0x1013D5D4C", Offset = "0x13D5D4C", VA = "0x1013D5D4C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060057DB RID: 22491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005123")]
		[Address(RVA = "0x1013D5D54", Offset = "0x13D5D54", VA = "0x1013D5D54")]
		public void Setup()
		{
		}

		// Token: 0x060057DC RID: 22492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005124")]
		[Address(RVA = "0x1013D6068", Offset = "0x13D6068", VA = "0x1013D6068")]
		public void SetupParam(bool isSpSlot, int equipSphereId, Action<int> OnDecideSphere, Action backButtonCallback)
		{
		}

		// Token: 0x060057DD RID: 22493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005125")]
		[Address(RVA = "0x1013D6714", Offset = "0x13D6714", VA = "0x1013D6714")]
		public void Destroy()
		{
		}

		// Token: 0x060057DE RID: 22494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005126")]
		[Address(RVA = "0x1013D678C", Offset = "0x13D678C", VA = "0x1013D678C")]
		private void ApplyDecideButton(bool flg)
		{
		}

		// Token: 0x060057DF RID: 22495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005127")]
		[Address(RVA = "0x1013D67C4", Offset = "0x13D67C4", VA = "0x1013D67C4")]
		private void UpdateSphereInfo(int id)
		{
		}

		// Token: 0x060057E0 RID: 22496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005128")]
		[Address(RVA = "0x1013D6AB8", Offset = "0x13D6AB8", VA = "0x1013D6AB8")]
		private void OnClickSphereCallBack(int sphereId)
		{
		}

		// Token: 0x060057E1 RID: 22497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005129")]
		[Address(RVA = "0x1013D6DF4", Offset = "0x13D6DF4", VA = "0x1013D6DF4")]
		public void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x060057E2 RID: 22498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512A")]
		[Address(RVA = "0x1013D6F68", Offset = "0x13D6F68", VA = "0x1013D6F68")]
		public void OnClickFilterButton()
		{
		}

		// Token: 0x060057E3 RID: 22499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512B")]
		[Address(RVA = "0x1013D63B8", Offset = "0x13D63B8", VA = "0x1013D63B8")]
		public void OnExecuteFilter()
		{
		}

		// Token: 0x060057E4 RID: 22500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512C")]
		[Address(RVA = "0x1013D7490", Offset = "0x13D7490", VA = "0x1013D7490")]
		public void OnClickRemoveIconCallback()
		{
		}

		// Token: 0x060057E5 RID: 22501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512D")]
		[Address(RVA = "0x1013D74EC", Offset = "0x13D74EC", VA = "0x1013D74EC")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060057E6 RID: 22502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512E")]
		[Address(RVA = "0x1013D7898", Offset = "0x13D7898", VA = "0x1013D7898")]
		public void OnResponseEquipSphere()
		{
		}

		// Token: 0x060057E7 RID: 22503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600512F")]
		[Address(RVA = "0x1013D78A0", Offset = "0x13D78A0", VA = "0x1013D78A0")]
		public AbilitySphereListGroup()
		{
		}

		// Token: 0x04006AF2 RID: 27378
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004BB7")]
		[SerializeField]
		private Text m_SphereNameText;

		// Token: 0x04006AF3 RID: 27379
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004BB8")]
		[SerializeField]
		private VariableIcon m_SphereItemIcon;

		// Token: 0x04006AF4 RID: 27380
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004BB9")]
		[SerializeField]
		private Text m_SphereDescriptionText;

		// Token: 0x04006AF5 RID: 27381
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004BBA")]
		[SerializeField]
		private Text m_SphereEffectText;

		// Token: 0x04006AF6 RID: 27382
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004BBB")]
		[SerializeField]
		private AbilitySphereScrollView m_Scroll;

		// Token: 0x04006AF7 RID: 27383
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004BBC")]
		[SerializeField]
		private CustomButton m_FilterButton;

		// Token: 0x04006AF8 RID: 27384
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004BBD")]
		[SerializeField]
		private Text m_FilterButtonText;

		// Token: 0x04006AF9 RID: 27385
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004BBE")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04006AFA RID: 27386
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004BBF")]
		[SerializeField]
		private Image m_DecideButtonImage;

		// Token: 0x04006AFB RID: 27387
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004BC0")]
		[SerializeField]
		private Text m_EmptyText;

		// Token: 0x04006AFC RID: 27388
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004BC1")]
		private int m_EquipSphereId;

		// Token: 0x04006AFD RID: 27389
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x4004BC2")]
		private int m_SelectSphereId;

		// Token: 0x04006AFE RID: 27390
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004BC3")]
		private bool m_isSpSlot;

		// Token: 0x04006AFF RID: 27391
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004BC4")]
		private Action<int> m_OnDecideSphere;

		// Token: 0x04006B00 RID: 27392
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004BC5")]
		private Action m_BackButtonCallback;

		// Token: 0x04006B01 RID: 27393
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004BC6")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x04006B02 RID: 27394
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004BC7")]
		private Action<bool> m_SavedBackButtonCallBack;
	}
}
