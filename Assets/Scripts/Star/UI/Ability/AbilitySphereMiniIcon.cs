﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001164 RID: 4452
	[Token(Token = "0x2000B8F")]
	[StructLayout(3)]
	public class AbilitySphereMiniIcon : MonoBehaviour
	{
		// Token: 0x060057E8 RID: 22504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005130")]
		[Address(RVA = "0x1013D50EC", Offset = "0x13D50EC", VA = "0x1013D50EC")]
		public void Apply(bool isLock, bool isBlank, AbilityDefine.eAbilitySphereType sphereType)
		{
		}

		// Token: 0x060057E9 RID: 22505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005131")]
		[Address(RVA = "0x1013D78B0", Offset = "0x13D78B0", VA = "0x1013D78B0")]
		private void OnDestroy()
		{
		}

		// Token: 0x060057EA RID: 22506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005132")]
		[Address(RVA = "0x1013D5210", Offset = "0x13D5210", VA = "0x1013D5210")]
		public void Destroy()
		{
		}

		// Token: 0x060057EB RID: 22507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005133")]
		[Address(RVA = "0x1013D78B4", Offset = "0x13D78B4", VA = "0x1013D78B4")]
		public AbilitySphereMiniIcon()
		{
		}

		// Token: 0x04006B03 RID: 27395
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004BC8")]
		[SerializeField]
		private Sprite m_SphereSpriteLocked;

		// Token: 0x04006B04 RID: 27396
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004BC9")]
		[SerializeField]
		private Sprite m_SphereSpriteBlank;

		// Token: 0x04006B05 RID: 27397
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004BCA")]
		[SerializeField]
		private Sprite[] m_SphereSprite;

		// Token: 0x04006B06 RID: 27398
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004BCB")]
		[SerializeField]
		private Image m_LockIcon;

		// Token: 0x04006B07 RID: 27399
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004BCC")]
		[SerializeField]
		private Image m_SphereIcon;
	}
}
