﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001165 RID: 4453
	[Token(Token = "0x2000B90")]
	[StructLayout(3)]
	public class AbilitySphereScrollItem : ScrollItemIcon
	{
		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x060057EC RID: 22508 RVA: 0x0001D088 File Offset: 0x0001B288
		[Token(Token = "0x1700057E")]
		public bool IsSelected
		{
			[Token(Token = "0x6005134")]
			[Address(RVA = "0x1013D78BC", Offset = "0x13D78BC", VA = "0x1013D78BC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060057ED RID: 22509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005135")]
		[Address(RVA = "0x1013D78EC", Offset = "0x13D78EC", VA = "0x1013D78EC", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060057EE RID: 22510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005136")]
		[Address(RVA = "0x1013D7BD0", Offset = "0x13D7BD0", VA = "0x1013D7BD0")]
		public void RefreshCurrent()
		{
		}

		// Token: 0x060057EF RID: 22511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005137")]
		[Address(RVA = "0x1013D7CE4", Offset = "0x13D7CE4", VA = "0x1013D7CE4", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060057F0 RID: 22512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005138")]
		[Address(RVA = "0x1013D7D10", Offset = "0x13D7D10", VA = "0x1013D7D10")]
		public AbilitySphereScrollItem()
		{
		}

		// Token: 0x04006B08 RID: 27400
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004BCD")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10013318C", Offset = "0x13318C")]
		private GameObject m_SelectedObj;

		// Token: 0x04006B09 RID: 27401
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004BCE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001331D8", Offset = "0x1331D8")]
		private GameObject m_CantEquipObj;

		// Token: 0x04006B0A RID: 27402
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004BCF")]
		[SerializeField]
		private AbilitySphereMiniIcon m_MiniSphereIcon;

		// Token: 0x04006B0B RID: 27403
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004BD0")]
		[SerializeField]
		private Text m_SphereNameText;

		// Token: 0x04006B0C RID: 27404
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004BD1")]
		[SerializeField]
		private TextField m_NumField;
	}
}
