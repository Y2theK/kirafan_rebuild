﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Ability
{
	// Token: 0x02001166 RID: 4454
	[Token(Token = "0x2000B91")]
	[StructLayout(3)]
	public class AbilitySphereScrollItemData : ScrollItemData
	{
		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x060057F1 RID: 22513 RVA: 0x0001D0A0 File Offset: 0x0001B2A0
		[Token(Token = "0x1700057F")]
		public bool IsCurrent
		{
			[Token(Token = "0x6005139")]
			[Address(RVA = "0x1013D7CDC", Offset = "0x13D7CDC", VA = "0x1013D7CDC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x060057F2 RID: 22514 RVA: 0x0001D0B8 File Offset: 0x0001B2B8
		[Token(Token = "0x17000580")]
		public bool IsCantEquip
		{
			[Token(Token = "0x600513A")]
			[Address(RVA = "0x1013D7BC8", Offset = "0x13D7BC8", VA = "0x1013D7BC8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060057F3 RID: 22515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600513B")]
		[Address(RVA = "0x1013D7D18", Offset = "0x13D7D18", VA = "0x1013D7D18")]
		public AbilitySphereScrollItemData(bool isSpSlot, AbilitySpheresDB_Param? param, int num, int equipedSphereId)
		{
		}

		// Token: 0x060057F4 RID: 22516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600513C")]
		[Address(RVA = "0x1013D7DD4", Offset = "0x13D7DD4", VA = "0x1013D7DD4")]
		public void SetCurrent(bool isCurrent)
		{
		}

		// Token: 0x04006B0D RID: 27405
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004BD2")]
		public AbilitySpheresDB_Param? m_SphereParam;

		// Token: 0x04006B0E RID: 27406
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004BD3")]
		public int m_Num;

		// Token: 0x04006B0F RID: 27407
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004BD4")]
		private bool m_IsCurrent;

		// Token: 0x04006B10 RID: 27408
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4004BD5")]
		private bool m_IsCantEquip;
	}
}
