﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Ability
{
	// Token: 0x02001167 RID: 4455
	[Token(Token = "0x2000B92")]
	[StructLayout(3)]
	public class AbilitySphereScrollView : ScrollViewBase
	{
		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x060057F5 RID: 22517 RVA: 0x0001D0D0 File Offset: 0x0001B2D0
		[Token(Token = "0x17000581")]
		public bool IsRemoveIconSelected
		{
			[Token(Token = "0x600513D")]
			[Address(RVA = "0x1013D77FC", Offset = "0x13D77FC", VA = "0x1013D77FC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x060057F6 RID: 22518 RVA: 0x0001D0E8 File Offset: 0x0001B2E8
		[Token(Token = "0x17000582")]
		public bool IsViewRemoveIcon
		{
			[Token(Token = "0x600513E")]
			[Address(RVA = "0x1013D73BC", Offset = "0x13D73BC", VA = "0x1013D73BC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x14000141 RID: 321
		// (add) Token: 0x060057F7 RID: 22519 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060057F8 RID: 22520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000141")]
		public event Action<int> OnClickIcon
		{
			[Token(Token = "0x600513F")]
			[Address(RVA = "0x1013D5E50", Offset = "0x13D5E50", VA = "0x1013D5E50")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6005140")]
			[Address(RVA = "0x1013D7DDC", Offset = "0x13D7DDC", VA = "0x1013D7DDC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000142 RID: 322
		// (add) Token: 0x060057F9 RID: 22521 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060057FA RID: 22522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000142")]
		public event Action OnClickRemoveIcon
		{
			[Token(Token = "0x6005141")]
			[Address(RVA = "0x1013D5F5C", Offset = "0x13D5F5C", VA = "0x1013D5F5C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6005142")]
			[Address(RVA = "0x1013D7EE8", Offset = "0x13D7EE8", VA = "0x1013D7EE8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x060057FB RID: 22523 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000583")]
		public List<AbilitySphereScrollItemData> FilteredDataList
		{
			[Token(Token = "0x6005143")]
			[Address(RVA = "0x1013D73CC", Offset = "0x13D73CC", VA = "0x1013D73CC")]
			get
			{
				return null;
			}
		}

		// Token: 0x060057FC RID: 22524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005144")]
		[Address(RVA = "0x1013D6BE4", Offset = "0x13D6BE4", VA = "0x1013D6BE4")]
		public void SetCurrentCursor(int Id)
		{
		}

		// Token: 0x060057FD RID: 22525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005145")]
		[Address(RVA = "0x1013D73D4", Offset = "0x13D73D4", VA = "0x1013D73D4")]
		public void SetCurrentRemoveIcon(bool flg)
		{
		}

		// Token: 0x060057FE RID: 22526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005146")]
		[Address(RVA = "0x1013D7050", Offset = "0x13D7050", VA = "0x1013D7050")]
		public void SetupList()
		{
		}

		// Token: 0x060057FF RID: 22527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005147")]
		[Address(RVA = "0x1013D7FF4", Offset = "0x13D7FF4", VA = "0x1013D7FF4")]
		private void DoFilter()
		{
		}

		// Token: 0x06005800 RID: 22528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005148")]
		[Address(RVA = "0x1013D823C", Offset = "0x13D823C", VA = "0x1013D823C")]
		public void AddRemoveIcon()
		{
		}

		// Token: 0x06005801 RID: 22529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005149")]
		[Address(RVA = "0x1013D8668", Offset = "0x13D8668", VA = "0x1013D8668", Slot = "14")]
		protected override void OnClickIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x06005802 RID: 22530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514A")]
		[Address(RVA = "0x1013D87B8", Offset = "0x13D87B8", VA = "0x1013D87B8")]
		public void OnClickRemoveIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x06005803 RID: 22531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514B")]
		[Address(RVA = "0x1013D87E4", Offset = "0x13D87E4", VA = "0x1013D87E4", Slot = "19")]
		public override void Destroy()
		{
		}

		// Token: 0x06005804 RID: 22532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514C")]
		[Address(RVA = "0x1013D880C", Offset = "0x13D880C", VA = "0x1013D880C")]
		public AbilitySphereScrollView()
		{
		}

		// Token: 0x04006B11 RID: 27409
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004BD6")]
		[SerializeField]
		private AbilitySphereListGroup m_AbilitySphereListGroup;

		// Token: 0x04006B12 RID: 27410
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004BD7")]
		[SerializeField]
		private GameObject m_RemoveIconPrefab;

		// Token: 0x04006B13 RID: 27411
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004BD8")]
		private RemoveSphereScrollItem m_RemoveIcon;

		// Token: 0x04006B16 RID: 27414
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004BDB")]
		private List<AbilitySphereScrollItemData> m_RawDataList;

		// Token: 0x04006B17 RID: 27415
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004BDC")]
		private List<AbilitySphereScrollItemData> m_FilteredDataList;
	}
}
