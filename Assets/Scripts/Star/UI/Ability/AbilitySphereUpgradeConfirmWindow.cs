﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001168 RID: 4456
	[Token(Token = "0x2000B93")]
	[StructLayout(3)]
	public class AbilitySphereUpgradeConfirmWindow : UIGroup
	{
		// Token: 0x06005805 RID: 22533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514D")]
		[Address(RVA = "0x1013D889C", Offset = "0x13D889C", VA = "0x1013D889C")]
		public void Setup(AbilityDefine.AbilitySphereUpgradeRecipe recipe)
		{
		}

		// Token: 0x06005806 RID: 22534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514E")]
		[Address(RVA = "0x1013D90AC", Offset = "0x13D90AC", VA = "0x1013D90AC")]
		public void OnClickYesButton()
		{
		}

		// Token: 0x06005807 RID: 22535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600514F")]
		[Address(RVA = "0x1013D90E0", Offset = "0x13D90E0", VA = "0x1013D90E0")]
		public void OnClickNoButton()
		{
		}

		// Token: 0x06005808 RID: 22536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005150")]
		[Address(RVA = "0x1013D90EC", Offset = "0x13D90EC", VA = "0x1013D90EC")]
		public AbilitySphereUpgradeConfirmWindow()
		{
		}

		// Token: 0x04006B18 RID: 27416
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004BDD")]
		[SerializeField]
		private Text m_txtConfirmMsg;

		// Token: 0x04006B19 RID: 27417
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004BDE")]
		[SerializeField]
		private AbilityMaterialInfo[] m_abilityMatInfos;

		// Token: 0x04006B1A RID: 27418
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004BDF")]
		[SerializeField]
		private TextFieldWithItem m_textFieldGoldCost;

		// Token: 0x04006B1B RID: 27419
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004BE0")]
		[SerializeField]
		private TextFieldWithItem m_textFieldGoldHave;

		// Token: 0x04006B1C RID: 27420
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004BE1")]
		[SerializeField]
		private CustomButton m_btnYes;

		// Token: 0x04006B1D RID: 27421
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004BE2")]
		[SerializeField]
		private CustomButton m_btnNo;

		// Token: 0x04006B1E RID: 27422
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004BE3")]
		[SerializeField]
		private CustomButton m_btnOK;

		// Token: 0x04006B1F RID: 27423
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004BE4")]
		public Action m_OnclickYesBtn;
	}
}
