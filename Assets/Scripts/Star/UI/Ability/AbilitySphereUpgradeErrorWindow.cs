﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x02001169 RID: 4457
	[Token(Token = "0x2000B94")]
	[StructLayout(3)]
	public class AbilitySphereUpgradeErrorWindow : UIGroup
	{
		// Token: 0x06005809 RID: 22537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005151")]
		[Address(RVA = "0x1013D90F4", Offset = "0x13D90F4", VA = "0x1013D90F4")]
		public void Setup(AbilityDefine.AbilitySphereUpgradeRecipe recipe)
		{
		}

		// Token: 0x0600580A RID: 22538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005152")]
		[Address(RVA = "0x1013D9354", Offset = "0x13D9354", VA = "0x1013D9354")]
		private void OnClickOKButton()
		{
		}

		// Token: 0x0600580B RID: 22539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005153")]
		[Address(RVA = "0x1013D9360", Offset = "0x13D9360", VA = "0x1013D9360")]
		public AbilitySphereUpgradeErrorWindow()
		{
		}

		// Token: 0x04006B20 RID: 27424
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004BE5")]
		[SerializeField]
		private Text m_txtCondition;

		// Token: 0x04006B21 RID: 27425
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004BE6")]
		[SerializeField]
		private CustomButton m_btnOK;
	}
}
