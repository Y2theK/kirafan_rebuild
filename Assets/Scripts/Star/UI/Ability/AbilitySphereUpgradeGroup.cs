﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x0200116A RID: 4458
	[Token(Token = "0x2000B95")]
	[StructLayout(3)]
	public class AbilitySphereUpgradeGroup : UIGroup
	{
		// Token: 0x0600580C RID: 22540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005154")]
		[Address(RVA = "0x1013D9368", Offset = "0x13D9368", VA = "0x1013D9368")]
		private void Awake()
		{
		}

		// Token: 0x0600580D RID: 22541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005155")]
		[Address(RVA = "0x1013D945C", Offset = "0x13D945C", VA = "0x1013D945C")]
		public void Setup(int itemID)
		{
		}

		// Token: 0x0600580E RID: 22542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005156")]
		[Address(RVA = "0x1013D9BB8", Offset = "0x13D9BB8", VA = "0x1013D9BB8")]
		public void Destroy()
		{
		}

		// Token: 0x0600580F RID: 22543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005157")]
		[Address(RVA = "0x1013D9C0C", Offset = "0x13D9C0C", VA = "0x1013D9C0C", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06005810 RID: 22544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005158")]
		[Address(RVA = "0x1013D9DF8", Offset = "0x13D9DF8", VA = "0x1013D9DF8")]
		public void StartEffect()
		{
		}

		// Token: 0x06005811 RID: 22545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005159")]
		[Address(RVA = "0x1013D9C34", Offset = "0x13D9C34", VA = "0x1013D9C34")]
		private void UpdateEffect()
		{
		}

		// Token: 0x06005812 RID: 22546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600515A")]
		[Address(RVA = "0x1013D9E28", Offset = "0x13D9E28", VA = "0x1013D9E28")]
		private void OnClickUpgradeButton()
		{
		}

		// Token: 0x06005813 RID: 22547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600515B")]
		[Address(RVA = "0x1013D9E90", Offset = "0x13D9E90", VA = "0x1013D9E90")]
		private void OnCloseMsgWindowCallback(int answer)
		{
		}

		// Token: 0x06005814 RID: 22548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600515C")]
		[Address(RVA = "0x1013D9E98", Offset = "0x13D9E98", VA = "0x1013D9E98")]
		private void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x06005815 RID: 22549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600515D")]
		[Address(RVA = "0x1013DA00C", Offset = "0x13DA00C", VA = "0x1013DA00C")]
		public AbilitySphereUpgradeGroup()
		{
		}

		// Token: 0x04006B22 RID: 27426
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004BE7")]
		[SerializeField]
		private ItemIconWithDetail m_BaseSphereIcon;

		// Token: 0x04006B23 RID: 27427
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004BE8")]
		[SerializeField]
		private Text m_BaseSphereName;

		// Token: 0x04006B24 RID: 27428
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004BE9")]
		[SerializeField]
		private ItemIconWithDetail m_UpgradedSphereIcon;

		// Token: 0x04006B25 RID: 27429
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004BEA")]
		[SerializeField]
		private Text m_UpgradedSphereName;

		// Token: 0x04006B26 RID: 27430
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004BEB")]
		[SerializeField]
		private AbilityMaterialInfo m_UpgradeMaterialInfo;

		// Token: 0x04006B27 RID: 27431
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004BEC")]
		[SerializeField]
		private VariableIcon m_UpgradedSphereIcon2;

		// Token: 0x04006B28 RID: 27432
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004BED")]
		[SerializeField]
		private Text m_UpgradedSphereName2;

		// Token: 0x04006B29 RID: 27433
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004BEE")]
		[SerializeField]
		private Text m_txtSphereInfo;

		// Token: 0x04006B2A RID: 27434
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004BEF")]
		[SerializeField]
		private Text m_txtCaution;

		// Token: 0x04006B2B RID: 27435
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004BF0")]
		[SerializeField]
		private TextFieldWithItem m_txtField;

		// Token: 0x04006B2C RID: 27436
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004BF1")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04006B2D RID: 27437
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004BF2")]
		[SerializeField]
		private MixWpnUpgradeEffectScene m_MixEffectScene;

		// Token: 0x04006B2E RID: 27438
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004BF3")]
		[SerializeField]
		private Image m_imgBtnTitle;

		// Token: 0x04006B2F RID: 27439
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004BF4")]
		private int m_baseItemID;

		// Token: 0x04006B30 RID: 27440
		[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
		[Token(Token = "0x4004BF5")]
		private int m_upgradedItemID;

		// Token: 0x04006B31 RID: 27441
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004BF6")]
		private AbilityDefine.AbilitySphereUpgradeRecipe m_recipe;

		// Token: 0x04006B32 RID: 27442
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004BF7")]
		private AbilitySphereUpgradeGroup.eStateEffect m_effState;

		// Token: 0x04006B33 RID: 27443
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004BF8")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x04006B34 RID: 27444
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004BF9")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04006B35 RID: 27445
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004BFA")]
		public Action<AbilityDefine.AbilitySphereUpgradeRecipe> m_OnClickButton;

		// Token: 0x04006B36 RID: 27446
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004BFB")]
		public Action m_BackButtonCallback;

		// Token: 0x0200116B RID: 4459
		[Token(Token = "0x2001296")]
		private enum eStateEffect
		{
			// Token: 0x04006B38 RID: 27448
			[Token(Token = "0x4007270")]
			Stay,
			// Token: 0x04006B39 RID: 27449
			[Token(Token = "0x4007271")]
			Move,
			// Token: 0x04006B3A RID: 27450
			[Token(Token = "0x4007272")]
			MixEffect
		}
	}
}
