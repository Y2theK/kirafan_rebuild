﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Ability
{
	// Token: 0x0200116C RID: 4460
	[Token(Token = "0x2000B96")]
	[StructLayout(3)]
	public class AbilityUI : MenuUIBase
	{
		// Token: 0x06005816 RID: 22550 RVA: 0x0001D100 File Offset: 0x0001B300
		[Token(Token = "0x600515E")]
		[Address(RVA = "0x1013D2B24", Offset = "0x13D2B24", VA = "0x1013D2B24")]
		public static int GetPrevSlotNumber(List<AbilitySlotsDB_Param> slotParam, int currentSlotIndex)
		{
			return 0;
		}

		// Token: 0x06005817 RID: 22551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600515F")]
		[Address(RVA = "0x1013DA01C", Offset = "0x13DA01C", VA = "0x1013DA01C")]
		public void OnDestroy()
		{
		}

		// Token: 0x06005818 RID: 22552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005160")]
		[Address(RVA = "0x1013DA028", Offset = "0x13DA028", VA = "0x1013DA028")]
		public void Setup(EditState_Ability ownerState)
		{
		}

		// Token: 0x06005819 RID: 22553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005161")]
		[Address(RVA = "0x1013DA174", Offset = "0x13DA174", VA = "0x1013DA174", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600581A RID: 22554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005162")]
		[Address(RVA = "0x1013DA2E0", Offset = "0x13DA2E0", VA = "0x1013DA2E0")]
		private void Update()
		{
		}

		// Token: 0x0600581B RID: 22555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005163")]
		[Address(RVA = "0x1013DA3EC", Offset = "0x13DA3EC", VA = "0x1013DA3EC")]
		private void ChangeStep(AbilityUI.eStep step)
		{
		}

		// Token: 0x0600581C RID: 22556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005164")]
		[Address(RVA = "0x1013DA618", Offset = "0x13DA618", VA = "0x1013DA618", Slot = "9")]
		public override void SetEnableInput(bool flg)
		{
		}

		// Token: 0x0600581D RID: 22557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005165")]
		[Address(RVA = "0x1013DA650", Offset = "0x13DA650", VA = "0x1013DA650", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600581E RID: 22558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005166")]
		[Address(RVA = "0x1013DA6AC", Offset = "0x13DA6AC", VA = "0x1013DA6AC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600581F RID: 22559 RVA: 0x0001D118 File Offset: 0x0001B318
		[Token(Token = "0x6005167")]
		[Address(RVA = "0x1013DA6EC", Offset = "0x13DA6EC", VA = "0x1013DA6EC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06005820 RID: 22560 RVA: 0x0001D130 File Offset: 0x0001B330
		[Token(Token = "0x6005168")]
		[Address(RVA = "0x1013DA6FC", Offset = "0x13DA6FC", VA = "0x1013DA6FC")]
		public bool IsOpened()
		{
			return default(bool);
		}

		// Token: 0x06005821 RID: 22561 RVA: 0x0001D148 File Offset: 0x0001B348
		[Token(Token = "0x6005169")]
		[Address(RVA = "0x1013DA594", Offset = "0x13DA594", VA = "0x1013DA594")]
		private bool IsDonePlayOut()
		{
			return default(bool);
		}

		// Token: 0x06005822 RID: 22562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600516A")]
		[Address(RVA = "0x1013DA70C", Offset = "0x13DA70C", VA = "0x1013DA70C")]
		public void PlayAbilitySlotReleaseEffect(float uiPositionX, float uiPositionY, Action onFinishCallback)
		{
		}

		// Token: 0x06005823 RID: 22563 RVA: 0x0001D160 File Offset: 0x0001B360
		[Token(Token = "0x600516B")]
		[Address(RVA = "0x1013DA4F4", Offset = "0x13DA4F4", VA = "0x1013DA4F4")]
		public bool IsPlayingAbilitySlotReleaseEffect()
		{
			return default(bool);
		}

		// Token: 0x06005824 RID: 22564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600516C")]
		[Address(RVA = "0x1013DA9D4", Offset = "0x13DA9D4", VA = "0x1013DA9D4")]
		private void OpenMainWindowCallback()
		{
		}

		// Token: 0x06005825 RID: 22565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600516D")]
		[Address(RVA = "0x1013DAA08", Offset = "0x13DAA08", VA = "0x1013DAA08")]
		public void OnClickAbilitySlotListButton()
		{
		}

		// Token: 0x06005826 RID: 22566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600516E")]
		[Address(RVA = "0x1013DAA84", Offset = "0x13DAA84", VA = "0x1013DAA84")]
		public void OnClickReleaseAbilitySlotButton()
		{
		}

		// Token: 0x06005827 RID: 22567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600516F")]
		[Address(RVA = "0x1013DAB7C", Offset = "0x13DAB7C", VA = "0x1013DAB7C")]
		private void OnDecideUnlockAbilitySlot()
		{
		}

		// Token: 0x06005828 RID: 22568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005170")]
		[Address(RVA = "0x1013DAD08", Offset = "0x13DAD08", VA = "0x1013DAD08")]
		private void OnResponseReleaseSlot(int slotIndex)
		{
		}

		// Token: 0x06005829 RID: 22569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005171")]
		[Address(RVA = "0x1013DAE50", Offset = "0x13DAE50", VA = "0x1013DAE50")]
		private void OnFinishAbilitySlotReleaseEffect()
		{
		}

		// Token: 0x0600582A RID: 22570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005172")]
		[Address(RVA = "0x1013DAF08", Offset = "0x13DAF08", VA = "0x1013DAF08")]
		public void OnClickAbilitySphereListButton()
		{
		}

		// Token: 0x0600582B RID: 22571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005173")]
		[Address(RVA = "0x1013DB1F0", Offset = "0x13DB1F0", VA = "0x1013DB1F0")]
		private void OnChangeAbilitySphere(int itemId)
		{
		}

		// Token: 0x0600582C RID: 22572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005174")]
		[Address(RVA = "0x1013DB37C", Offset = "0x13DB37C", VA = "0x1013DB37C")]
		private void OnResponseEquipSphere()
		{
		}

		// Token: 0x0600582D RID: 22573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005175")]
		[Address(RVA = "0x1013DB40C", Offset = "0x13DB40C", VA = "0x1013DB40C")]
		private void OnClickAbilitySphereUpgradeButton()
		{
		}

		// Token: 0x0600582E RID: 22574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005176")]
		[Address(RVA = "0x1013DB550", Offset = "0x13DB550", VA = "0x1013DB550")]
		private void OnClickUpgradeWindowUpgradeButton(AbilityDefine.AbilitySphereUpgradeRecipe recipe)
		{
		}

		// Token: 0x0600582F RID: 22575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005177")]
		[Address(RVA = "0x1013DB73C", Offset = "0x13DB73C", VA = "0x1013DB73C")]
		private void OnCloseMsgWindowCallback(int answer)
		{
		}

		// Token: 0x06005830 RID: 22576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005178")]
		[Address(RVA = "0x1013DBA40", Offset = "0x13DBA40", VA = "0x1013DBA40")]
		private void OnClickUpgradeWindowYesButton()
		{
		}

		// Token: 0x06005831 RID: 22577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005179")]
		[Address(RVA = "0x1013DB74C", Offset = "0x13DB74C", VA = "0x1013DB74C")]
		public void RequestUpgradeAbilitySphere(AbilityDefine.AbilitySphereUpgradeRecipe recipe)
		{
		}

		// Token: 0x06005832 RID: 22578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600517A")]
		[Address(RVA = "0x1013DBA48", Offset = "0x13DBA48", VA = "0x1013DBA48")]
		private void OnResponse_UpgradeSphere()
		{
		}

		// Token: 0x06005833 RID: 22579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600517B")]
		[Address(RVA = "0x1013DBAC8", Offset = "0x13DBAC8", VA = "0x1013DBAC8")]
		public AbilityUI()
		{
		}

		// Token: 0x04006B3B RID: 27451
		[Token(Token = "0x4004BFC")]
		public const string BACKGROUND_NAME_ABILITY_TREE = "uibackground_0";

		// Token: 0x04006B3C RID: 27452
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004BFD")]
		[SerializeField]
		private AbilitySlotReleaseHandler m_AbilitySlotReleaseHandlerPrefab;

		// Token: 0x04006B3D RID: 27453
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004BFE")]
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x04006B3E RID: 27454
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004BFF")]
		[SerializeField]
		private Camera m_EffectCamera;

		// Token: 0x04006B3F RID: 27455
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C00")]
		[SerializeField]
		private AbilityBoardGroup m_MainGroup;

		// Token: 0x04006B40 RID: 27456
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C01")]
		[SerializeField]
		private AbilitySphereListGroup m_AbilitySphereListGroup;

		// Token: 0x04006B41 RID: 27457
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004C02")]
		[SerializeField]
		private AbilityEquipListGroup m_AbilityEquipListGroup;

		// Token: 0x04006B42 RID: 27458
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004C03")]
		[SerializeField]
		private AbilitySlotReleaseConfirmWindow m_AbilitySlotReleaseComfirmWindow;

		// Token: 0x04006B43 RID: 27459
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004C04")]
		[SerializeField]
		private AbilitySphereUpgradeGroup m_AbilitySphereUpgradeGroup;

		// Token: 0x04006B44 RID: 27460
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004C05")]
		[SerializeField]
		private AbilitySphereUpgradeConfirmWindow m_AbilitySphereUpgradeConfirmWindow;

		// Token: 0x04006B45 RID: 27461
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C06")]
		[SerializeField]
		private AbilitySphereUpgradeErrorWindow m_AbilitySphereUpgradeErrorWindow;

		// Token: 0x04006B46 RID: 27462
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004C07")]
		private AbilityUI.eStep m_Step;

		// Token: 0x04006B47 RID: 27463
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004C08")]
		private EditState_Ability m_OwnerState;

		// Token: 0x04006B48 RID: 27464
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004C09")]
		private AbilityDefine.AbilitySphereUpgradeRecipe m_UpgradeRecipe;

		// Token: 0x04006B49 RID: 27465
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004C0A")]
		private bool m_PlayingAbilitySlotReleaseEffect;

		// Token: 0x04006B4A RID: 27466
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004C0B")]
		private Action m_OnFinishAbilitySlotReleaseCallback;

		// Token: 0x04006B4B RID: 27467
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004C0C")]
		private AbilitySlotReleaseHandler m_AbilitySlotReleaseHandler;

		// Token: 0x0200116D RID: 4461
		[Token(Token = "0x2001297")]
		private enum eStep
		{
			// Token: 0x04006B4D RID: 27469
			[Token(Token = "0x4007274")]
			Hide,
			// Token: 0x04006B4E RID: 27470
			[Token(Token = "0x4007275")]
			In,
			// Token: 0x04006B4F RID: 27471
			[Token(Token = "0x4007276")]
			Idle,
			// Token: 0x04006B50 RID: 27472
			[Token(Token = "0x4007277")]
			Out,
			// Token: 0x04006B51 RID: 27473
			[Token(Token = "0x4007278")]
			End
		}
	}
}
