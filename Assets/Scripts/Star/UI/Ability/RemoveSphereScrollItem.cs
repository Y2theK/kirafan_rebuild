﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Ability
{
	// Token: 0x0200116E RID: 4462
	[Token(Token = "0x2000B97")]
	[StructLayout(3)]
	public class RemoveSphereScrollItem : ScrollItemIcon
	{
		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x06005834 RID: 22580 RVA: 0x0001D178 File Offset: 0x0001B378
		[Token(Token = "0x17000584")]
		public bool IsSelected
		{
			[Token(Token = "0x600517C")]
			[Address(RVA = "0x1013DBAD0", Offset = "0x13DBAD0", VA = "0x1013DBAD0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005835 RID: 22581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600517D")]
		[Address(RVA = "0x1013DBB00", Offset = "0x13DBB00", VA = "0x1013DBB00")]
		public void SetText(string str)
		{
		}

		// Token: 0x06005836 RID: 22582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600517E")]
		[Address(RVA = "0x1013DBB40", Offset = "0x13DBB40", VA = "0x1013DBB40")]
		public void SetCurrent(bool flg)
		{
		}

		// Token: 0x06005837 RID: 22583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600517F")]
		[Address(RVA = "0x1013DBB78", Offset = "0x13DBB78", VA = "0x1013DBB78")]
		public RemoveSphereScrollItem()
		{
		}

		// Token: 0x04006B52 RID: 27474
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C0D")]
		[SerializeField]
		private GameObject m_SelectedObj;

		// Token: 0x04006B53 RID: 27475
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C0E")]
		[SerializeField]
		private Text m_ItemText;
	}
}
