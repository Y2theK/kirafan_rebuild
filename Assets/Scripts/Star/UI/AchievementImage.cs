﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CD4 RID: 3284
	[Token(Token = "0x20008C4")]
	[StructLayout(3)]
	public class AchievementImage : ASyncImage
	{
		// Token: 0x06003C1D RID: 15389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DA")]
		[Address(RVA = "0x1013DBB80", Offset = "0x13DBB80", VA = "0x1013DBB80")]
		public void Apply(int achievementID)
		{
		}

		// Token: 0x06003C1E RID: 15390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DB")]
		[Address(RVA = "0x1013DBC90", Offset = "0x13DBC90", VA = "0x1013DBC90", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C1F RID: 15391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036DC")]
		[Address(RVA = "0x1013DBCBC", Offset = "0x13DBCBC", VA = "0x1013DBCBC")]
		public AchievementImage()
		{
		}

		// Token: 0x04004B5C RID: 19292
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40034F9")]
		protected int m_AchievementID;
	}
}
