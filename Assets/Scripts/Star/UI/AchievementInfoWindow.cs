﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C44 RID: 3140
	[Token(Token = "0x2000866")]
	[StructLayout(3)]
	public class AchievementInfoWindow : UIGroup
	{
		// Token: 0x060038C2 RID: 14530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033AC")]
		[Address(RVA = "0x1013DBCCC", Offset = "0x13DBCCC", VA = "0x1013DBCCC")]
		private void Awake()
		{
		}

		// Token: 0x060038C3 RID: 14531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033AD")]
		[Address(RVA = "0x1013DBD6C", Offset = "0x13DBD6C", VA = "0x1013DBD6C")]
		public void Setup(int achvID)
		{
		}

		// Token: 0x060038C4 RID: 14532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033AE")]
		[Address(RVA = "0x1013DBEC8", Offset = "0x13DBEC8", VA = "0x1013DBEC8")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060038C5 RID: 14533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033AF")]
		[Address(RVA = "0x1013DBED4", Offset = "0x13DBED4", VA = "0x1013DBED4")]
		public AchievementInfoWindow()
		{
		}

		// Token: 0x04004802 RID: 18434
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003281")]
		[SerializeField]
		private AchievementImage m_ImgAchv;

		// Token: 0x04004803 RID: 18435
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003282")]
		[SerializeField]
		private Text m_txtInfo;

		// Token: 0x04004804 RID: 18436
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003283")]
		[SerializeField]
		private CustomButton m_BtnClose;

		// Token: 0x04004805 RID: 18437
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003284")]
		[SerializeField]
		private Image m_imgUnknownIcon;
	}
}
