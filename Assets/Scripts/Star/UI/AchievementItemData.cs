﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C47 RID: 3143
	[Token(Token = "0x2000869")]
	[StructLayout(3)]
	public class AchievementItemData : ScrollItemData
	{
		// Token: 0x060038CA RID: 14538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B4")]
		[Address(RVA = "0x1013DBEDC", Offset = "0x13DBEDC", VA = "0x1013DBEDC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x060038CB RID: 14539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B5")]
		[Address(RVA = "0x1013DBF50", Offset = "0x13DBF50", VA = "0x1013DBF50")]
		public AchievementItemData()
		{
		}

		// Token: 0x0400480E RID: 18446
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400328D")]
		public bool m_isNew;

		// Token: 0x0400480F RID: 18447
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400328E")]
		public int m_achievementID;

		// Token: 0x04004810 RID: 18448
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400328F")]
		public Action<int> m_CBOnclickPanel;
	}
}
