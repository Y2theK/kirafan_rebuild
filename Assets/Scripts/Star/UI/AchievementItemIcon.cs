﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C45 RID: 3141
	[Token(Token = "0x2000867")]
	[StructLayout(3)]
	public class AchievementItemIcon : ScrollItemIcon
	{
		// Token: 0x060038C6 RID: 14534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B0")]
		[Address(RVA = "0x1013DBF58", Offset = "0x13DBF58", VA = "0x1013DBF58", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060038C7 RID: 14535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B1")]
		[Address(RVA = "0x1013DC1A0", Offset = "0x13DC1A0", VA = "0x1013DC1A0")]
		public AchievementItemIcon()
		{
		}

		// Token: 0x04004806 RID: 18438
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003285")]
		[SerializeField]
		private AchievementImage m_imgAchvIcon;

		// Token: 0x04004807 RID: 18439
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003286")]
		[SerializeField]
		private Image m_imgNewIcon;

		// Token: 0x04004808 RID: 18440
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003287")]
		[SerializeField]
		private Image m_imgUnknownIcon;

		// Token: 0x04004809 RID: 18441
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003288")]
		private AchievementItemData m_achvData;
	}
}
