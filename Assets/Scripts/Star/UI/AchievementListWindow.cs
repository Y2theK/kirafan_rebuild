﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C48 RID: 3144
	[Token(Token = "0x200086A")]
	[StructLayout(3)]
	public class AchievementListWindow : UIGroup
	{
		// Token: 0x060038CC RID: 14540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B6")]
		[Address(RVA = "0x1013DC1A8", Offset = "0x13DC1A8", VA = "0x1013DC1A8")]
		private void Awake()
		{
		}

		// Token: 0x060038CD RID: 14541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B7")]
		[Address(RVA = "0x1013DC4AC", Offset = "0x13DC4AC", VA = "0x1013DC4AC", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060038CE RID: 14542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B8")]
		[Address(RVA = "0x1013DC9F0", Offset = "0x13DC9F0", VA = "0x1013DC9F0", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060038CF RID: 14543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B9")]
		[Address(RVA = "0x1013DC748", Offset = "0x13DC748", VA = "0x1013DC748")]
		public void SetData()
		{
		}

		// Token: 0x060038D0 RID: 14544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BA")]
		[Address(RVA = "0x1013DCAEC", Offset = "0x13DCAEC", VA = "0x1013DCAEC")]
		private void UpdateAchievementItemList()
		{
		}

		// Token: 0x060038D1 RID: 14545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BB")]
		[Address(RVA = "0x1013DCE08", Offset = "0x13DCE08", VA = "0x1013DCE08")]
		public void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x060038D2 RID: 14546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BC")]
		[Address(RVA = "0x1013DCF4C", Offset = "0x13DCF4C", VA = "0x1013DCF4C")]
		public void OnResponceShown()
		{
		}

		// Token: 0x060038D3 RID: 14547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BD")]
		[Address(RVA = "0x1013DCFFC", Offset = "0x13DCFFC", VA = "0x1013DCFFC", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060038D4 RID: 14548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BE")]
		[Address(RVA = "0x1013DD158", Offset = "0x13DD158", VA = "0x1013DD158")]
		public void SelectedTitleCallback(int title)
		{
		}

		// Token: 0x060038D5 RID: 14549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033BF")]
		[Address(RVA = "0x1013DD568", Offset = "0x13DD568", VA = "0x1013DD568")]
		public void SelectedAchievementCallback(int id)
		{
		}

		// Token: 0x060038D6 RID: 14550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C0")]
		[Address(RVA = "0x1013DD6D0", Offset = "0x13DD6D0", VA = "0x1013DD6D0")]
		public void OnChangeRadioButtonIndex(int index)
		{
		}

		// Token: 0x060038D7 RID: 14551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C1")]
		[Address(RVA = "0x1013DD6DC", Offset = "0x13DD6DC", VA = "0x1013DD6DC")]
		public void ChangeAchievementCallback(int achvID)
		{
		}

		// Token: 0x060038D8 RID: 14552 RVA: 0x00017D90 File Offset: 0x00015F90
		[Token(Token = "0x60033C2")]
		[Address(RVA = "0x1013DCCC8", Offset = "0x13DCCC8", VA = "0x1013DCCC8")]
		private bool isNewFromTitleItemData(eTitleType type)
		{
			return default(bool);
		}

		// Token: 0x060038D9 RID: 14553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C3")]
		[Address(RVA = "0x1013DD710", Offset = "0x13DD710", VA = "0x1013DD710")]
		public AchievementListWindow()
		{
		}

		// Token: 0x04004811 RID: 18449
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003290")]
		[SerializeField]
		private ScrollViewBase m_AchvListScroll;

		// Token: 0x04004812 RID: 18450
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003291")]
		[SerializeField]
		private ScrollViewBase m_TitleListScroll;

		// Token: 0x04004813 RID: 18451
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003292")]
		[SerializeField]
		private RadioButton m_RadioButton;

		// Token: 0x04004814 RID: 18452
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003293")]
		[SerializeField]
		private AchievementImage m_imgNowAchv;

		// Token: 0x04004815 RID: 18453
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003294")]
		[SerializeField]
		private AchievementInfoWindow m_AchvInfoWindow;

		// Token: 0x04004816 RID: 18454
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003295")]
		[SerializeField]
		private ConfirmChangeAchvWindow m_ChangeAchvWindow;

		// Token: 0x04004817 RID: 18455
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003296")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04004818 RID: 18456
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003297")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x04004819 RID: 18457
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003298")]
		private eTitleType m_selectedTitleType;

		// Token: 0x0400481A RID: 18458
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4003299")]
		private AchievementDefine.eFilterType m_selectedFilterType;

		// Token: 0x0400481B RID: 18459
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400329A")]
		private HashSet<eTitleType> m_ShownTitleType;
	}
}
