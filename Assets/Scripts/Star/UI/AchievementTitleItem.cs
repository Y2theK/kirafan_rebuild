﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C49 RID: 3145
	[Token(Token = "0x200086B")]
	[StructLayout(3)]
	public class AchievementTitleItem : ScrollItemIcon
	{
		// Token: 0x060038DA RID: 14554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C4")]
		[Address(RVA = "0x1013DD78C", Offset = "0x13DD78C", VA = "0x1013DD78C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060038DB RID: 14555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C5")]
		[Address(RVA = "0x1013DD414", Offset = "0x13DD414", VA = "0x1013DD414")]
		public void ItemUpdate()
		{
		}

		// Token: 0x060038DC RID: 14556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C6")]
		[Address(RVA = "0x1013DD93C", Offset = "0x13DD93C", VA = "0x1013DD93C")]
		public AchievementTitleItem()
		{
		}

		// Token: 0x0400481C RID: 18460
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400329B")]
		[SerializeField]
		private ContentTitleLogo m_Image;

		// Token: 0x0400481D RID: 18461
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400329C")]
		[SerializeField]
		private Image m_ImgBackground;

		// Token: 0x0400481E RID: 18462
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400329D")]
		[SerializeField]
		private Image m_imgOthers;

		// Token: 0x0400481F RID: 18463
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400329E")]
		[SerializeField]
		private Image m_imgNewIcon;

		// Token: 0x04004820 RID: 18464
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400329F")]
		private TitleItemData m_titleData;
	}
}
