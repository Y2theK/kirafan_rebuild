﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.AeUIAnim
{
	// Token: 0x0200117D RID: 4477
	[Token(Token = "0x2000BA2")]
	[StructLayout(3)]
	public class AeUIAnim : MonoBehaviour
	{
		// Token: 0x0600589E RID: 22686 RVA: 0x0001D2B0 File Offset: 0x0001B4B0
		[Token(Token = "0x60051E6")]
		[Address(RVA = "0x1013DD944", Offset = "0x13DD944", VA = "0x1013DD944")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600589F RID: 22687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051E7")]
		[Address(RVA = "0x1013DD94C", Offset = "0x13DD94C", VA = "0x1013DD94C")]
		private void SetLayerActive(bool flg)
		{
		}

		// Token: 0x060058A0 RID: 22688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051E8")]
		[Address(RVA = "0x1013DDA54", Offset = "0x13DDA54", VA = "0x1013DDA54")]
		private void Start()
		{
		}

		// Token: 0x060058A1 RID: 22689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051E9")]
		[Address(RVA = "0x1013DDA68", Offset = "0x13DDA68", VA = "0x1013DDA68")]
		private void Update()
		{
		}

		// Token: 0x060058A2 RID: 22690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051EA")]
		[Address(RVA = "0x1013DDB8C", Offset = "0x13DDB8C", VA = "0x1013DDB8C")]
		public void Play()
		{
		}

		// Token: 0x060058A3 RID: 22691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051EB")]
		[Address(RVA = "0x1013DDCD8", Offset = "0x13DDCD8", VA = "0x1013DDCD8")]
		public void Stop()
		{
		}

		// Token: 0x060058A4 RID: 22692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051EC")]
		[Address(RVA = "0x1013DDADC", Offset = "0x13DDADC", VA = "0x1013DDADC")]
		private void UpdateLayers(float playSec)
		{
		}

		// Token: 0x060058A5 RID: 22693 RVA: 0x0001D2C8 File Offset: 0x0001B4C8
		[Token(Token = "0x60051ED")]
		[Address(RVA = "0x1013DDC1C", Offset = "0x13DDC1C", VA = "0x1013DDC1C")]
		public float GetMaxSec()
		{
			return 0f;
		}

		// Token: 0x060058A6 RID: 22694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051EE")]
		[Address(RVA = "0x1013DE058", Offset = "0x13DE058", VA = "0x1013DE058")]
		public AeUIAnim()
		{
		}

		// Token: 0x04006BB7 RID: 27575
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C60")]
		private AeUILayer[] m_AeUILayers;

		// Token: 0x04006BB8 RID: 27576
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C61")]
		private bool m_IsPlaying;

		// Token: 0x04006BB9 RID: 27577
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004C62")]
		private float m_PlayingSec;

		// Token: 0x04006BBA RID: 27578
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C63")]
		private float m_MaxSec;

		// Token: 0x0200117E RID: 4478
		[Token(Token = "0x200129C")]
		public enum eInterpolation
		{
			// Token: 0x04006BBC RID: 27580
			[Token(Token = "0x400728C")]
			Linear,
			// Token: 0x04006BBD RID: 27581
			[Token(Token = "0x400728D")]
			Bezier
		}
	}
}
