﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.AeUIAnim
{
	// Token: 0x0200117F RID: 4479
	[Token(Token = "0x2000BA3")]
	[StructLayout(3)]
	public class AeUILayer : MonoBehaviour
	{
		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x060058A7 RID: 22695 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000587")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60051EF")]
			[Address(RVA = "0x1013DE060", Offset = "0x13DE060", VA = "0x1013DE060")]
			get
			{
				return null;
			}
		}

		// Token: 0x060058A8 RID: 22696 RVA: 0x0001D2E0 File Offset: 0x0001B4E0
		[Token(Token = "0x60051F0")]
		[Address(RVA = "0x1013DDEB8", Offset = "0x13DDEB8", VA = "0x1013DDEB8")]
		public float GetMaxSec()
		{
			return 0f;
		}

		// Token: 0x060058A9 RID: 22697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F1")]
		[Address(RVA = "0x1013DDCE8", Offset = "0x13DDCE8", VA = "0x1013DDCE8")]
		public void UpdateTimeLine(float playSec)
		{
		}

		// Token: 0x060058AA RID: 22698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F2")]
		[Address(RVA = "0x1013DE2BC", Offset = "0x13DE2BC", VA = "0x1013DE2BC")]
		public void SetPosition(float[] values)
		{
		}

		// Token: 0x060058AB RID: 22699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F3")]
		[Address(RVA = "0x1013DE36C", Offset = "0x13DE36C", VA = "0x1013DE36C")]
		public void SetRotate(float value)
		{
		}

		// Token: 0x060058AC RID: 22700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F4")]
		[Address(RVA = "0x1013DE434", Offset = "0x13DE434", VA = "0x1013DE434")]
		public void SetScale(float[] values)
		{
		}

		// Token: 0x060058AD RID: 22701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F5")]
		[Address(RVA = "0x1013DE518", Offset = "0x13DE518", VA = "0x1013DE518")]
		public void SetOpacity(float value)
		{
		}

		// Token: 0x060058AE RID: 22702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F6")]
		[Address(RVA = "0x1013DE58C", Offset = "0x13DE58C", VA = "0x1013DE58C")]
		public void SetTimeLine(AeUILayer.TimeLine timeLine)
		{
		}

		// Token: 0x060058AF RID: 22703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F7")]
		[Address(RVA = "0x1013DE71C", Offset = "0x13DE71C", VA = "0x1013DE71C")]
		public void SetImage(Image image)
		{
		}

		// Token: 0x060058B0 RID: 22704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051F8")]
		[Address(RVA = "0x1013DE724", Offset = "0x13DE724", VA = "0x1013DE724")]
		public AeUILayer()
		{
		}

		// Token: 0x04006BBE RID: 27582
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C64")]
		[SerializeField]
		private List<AeUILayer.TimeLine> m_TimeLineList;

		// Token: 0x04006BBF RID: 27583
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C65")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006BC0 RID: 27584
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C66")]
		private RectTransform m_RectTransform;

		// Token: 0x02001180 RID: 4480
		[Token(Token = "0x200129D")]
		[Serializable]
		public class TimeLine
		{
			// Token: 0x060058B1 RID: 22705 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006368")]
			[Address(RVA = "0x1013DE0F8", Offset = "0x13DE0F8", VA = "0x1013DE0F8")]
			public float[] GetValues(float sec)
			{
				return null;
			}

			// Token: 0x060058B2 RID: 22706 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006369")]
			[Address(RVA = "0x1013DE77C", Offset = "0x13DE77C", VA = "0x1013DE77C")]
			private float[] LerpKey(float time, AeUILayer.Key before, AeUILayer.Key after)
			{
				return null;
			}

			// Token: 0x060058B3 RID: 22707 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636A")]
			[Address(RVA = "0x1013DE9D4", Offset = "0x13DE9D4", VA = "0x1013DE9D4")]
			public TimeLine()
			{
			}

			// Token: 0x04006BC1 RID: 27585
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400728E")]
			public AeUILayer.TimeLine.eType m_Type;

			// Token: 0x04006BC2 RID: 27586
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400728F")]
			public AeUILayer.Key[] m_Keys;

			// Token: 0x02001181 RID: 4481
			[Token(Token = "0x2001355")]
			public enum eType
			{
				// Token: 0x04006BC4 RID: 27588
				[Token(Token = "0x40075D1")]
				Position,
				// Token: 0x04006BC5 RID: 27589
				[Token(Token = "0x40075D2")]
				Rotate,
				// Token: 0x04006BC6 RID: 27590
				[Token(Token = "0x40075D3")]
				Scale,
				// Token: 0x04006BC7 RID: 27591
				[Token(Token = "0x40075D4")]
				Opacity,
				// Token: 0x04006BC8 RID: 27592
				[Token(Token = "0x40075D5")]
				Num
			}
		}

		// Token: 0x02001182 RID: 4482
		[Token(Token = "0x200129E")]
		[Serializable]
		public class Key
		{
			// Token: 0x060058B4 RID: 22708 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600636B")]
			[Address(RVA = "0x1013DE72C", Offset = "0x13DE72C", VA = "0x1013DE72C")]
			public Key(float sec, float[] values, AeUIAnim.eInterpolation inInterpolation, AeUIAnim.eInterpolation outInterpolation)
			{
			}

			// Token: 0x04006BC9 RID: 27593
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007290")]
			public AeUIAnim.eInterpolation m_InInterpolation;

			// Token: 0x04006BCA RID: 27594
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007291")]
			public AeUIAnim.eInterpolation m_OutInterpolation;

			// Token: 0x04006BCB RID: 27595
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007292")]
			public float m_Sec;

			// Token: 0x04006BCC RID: 27596
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007293")]
			public float[] m_Values;
		}
	}
}
