﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C68 RID: 3176
	[Token(Token = "0x2000886")]
	[StructLayout(3)]
	public class AgeConfirmWindow : MonoBehaviour
	{
		// Token: 0x060039A3 RID: 14755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348A")]
		[Address(RVA = "0x1013DE9DC", Offset = "0x13DE9DC", VA = "0x1013DE9DC")]
		public void Open(Action CompleteSetAgeCallback, Action onClickCloseButton)
		{
		}

		// Token: 0x060039A4 RID: 14756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348B")]
		[Address(RVA = "0x1013DEA9C", Offset = "0x13DEA9C", VA = "0x1013DEA9C")]
		private void OnEnd()
		{
		}

		// Token: 0x060039A5 RID: 14757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348C")]
		[Address(RVA = "0x1013DEB3C", Offset = "0x13DEB3C", VA = "0x1013DEB3C")]
		public void Close()
		{
		}

		// Token: 0x060039A6 RID: 14758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348D")]
		[Address(RVA = "0x1013DEB70", Offset = "0x13DEB70", VA = "0x1013DEB70")]
		public void OnClickAgeTypeButton(int index)
		{
		}

		// Token: 0x060039A7 RID: 14759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348E")]
		[Address(RVA = "0x1013DEC30", Offset = "0x13DEC30", VA = "0x1013DEC30")]
		private void OnCompleteSetAge()
		{
		}

		// Token: 0x060039A8 RID: 14760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600348F")]
		[Address(RVA = "0x1013DEC68", Offset = "0x13DEC68", VA = "0x1013DEC68")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060039A9 RID: 14761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003490")]
		[Address(RVA = "0x1013DECAC", Offset = "0x13DECAC", VA = "0x1013DECAC")]
		public void ForceHide()
		{
		}

		// Token: 0x060039AA RID: 14762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003491")]
		[Address(RVA = "0x1013DED64", Offset = "0x13DED64", VA = "0x1013DED64")]
		public AgeConfirmWindow()
		{
		}

		// Token: 0x04004885 RID: 18565
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032F1")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004886 RID: 18566
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032F2")]
		private Action m_CompleteSetAgeCallback;

		// Token: 0x04004887 RID: 18567
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032F3")]
		private Action m_OnClickCloseButtonCallback;

		// Token: 0x04004888 RID: 18568
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032F4")]
		private Action m_OnEndCallback;
	}
}
