﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C6A RID: 3178
	[Token(Token = "0x2000888")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011CFBC", Offset = "0x11CFBC")]
	[StructLayout(3)]
	public class AlphaAnimUI : AnimUIBase
	{
		// Token: 0x060039AE RID: 14766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003495")]
		[Address(RVA = "0x1013DED6C", Offset = "0x13DED6C", VA = "0x1013DED6C", Slot = "6")]
		protected override void Prepare()
		{
		}

		// Token: 0x060039AF RID: 14767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003496")]
		[Address(RVA = "0x1013DEE28", Offset = "0x13DEE28", VA = "0x1013DEE28", Slot = "9")]
		protected override void ExecutePlayIn()
		{
		}

		// Token: 0x060039B0 RID: 14768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003497")]
		[Address(RVA = "0x1013DF204", Offset = "0x13DF204", VA = "0x1013DF204", Slot = "10")]
		protected override void ExecutePlayOut()
		{
		}

		// Token: 0x060039B1 RID: 14769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003498")]
		[Address(RVA = "0x1013DF5E0", Offset = "0x13DF5E0", VA = "0x1013DF5E0", Slot = "5")]
		public override void Hide()
		{
		}

		// Token: 0x060039B2 RID: 14770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003499")]
		[Address(RVA = "0x1013DF6E0", Offset = "0x13DF6E0", VA = "0x1013DF6E0", Slot = "4")]
		public override void Show()
		{
		}

		// Token: 0x060039B3 RID: 14771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349A")]
		[Address(RVA = "0x1013DF7E0", Offset = "0x13DF7E0", VA = "0x1013DF7E0")]
		public void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x060039B4 RID: 14772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349B")]
		[Address(RVA = "0x1013DF820", Offset = "0x13DF820", VA = "0x1013DF820")]
		public void OnCompleteInAlpha()
		{
		}

		// Token: 0x060039B5 RID: 14773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349C")]
		[Address(RVA = "0x1013DF874", Offset = "0x13DF874", VA = "0x1013DF874")]
		public void OnCompleteOutAlpha()
		{
		}

		// Token: 0x060039B6 RID: 14774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349D")]
		[Address(RVA = "0x1013DF8C8", Offset = "0x13DF8C8", VA = "0x1013DF8C8")]
		public AlphaAnimUI()
		{
		}

		// Token: 0x0400488B RID: 18571
		[Token(Token = "0x40032F7")]
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x0400488C RID: 18572
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032F8")]
		[SerializeField]
		private bool m_ReturnMode;

		// Token: 0x0400488D RID: 18573
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x40032F9")]
		[SerializeField]
		private bool m_Restart;

		// Token: 0x0400488E RID: 18574
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40032FA")]
		[SerializeField]
		private float m_HideAlpha;

		// Token: 0x0400488F RID: 18575
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40032FB")]
		[SerializeField]
		private float m_ShowAlpha;

		// Token: 0x04004890 RID: 18576
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40032FC")]
		[SerializeField]
		private float m_EndAlpha;

		// Token: 0x04004891 RID: 18577
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40032FD")]
		[SerializeField]
		private float m_AnimDuration;

		// Token: 0x04004892 RID: 18578
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40032FE")]
		[SerializeField]
		private float m_Delay;

		// Token: 0x04004893 RID: 18579
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40032FF")]
		[SerializeField]
		private iTween.EaseType m_EaseType;

		// Token: 0x04004894 RID: 18580
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003300")]
		private GameObject m_GameObject;

		// Token: 0x04004895 RID: 18581
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003301")]
		private CanvasGroup m_CanvasGroup;
	}
}
