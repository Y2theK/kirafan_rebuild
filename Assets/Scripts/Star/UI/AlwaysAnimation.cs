﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C69 RID: 3177
	[Token(Token = "0x2000887")]
	[StructLayout(3)]
	public class AlwaysAnimation : MonoBehaviour
	{
		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x060039AB RID: 14763 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000423")]
		private Animation Animation
		{
			[Token(Token = "0x6003492")]
			[Address(RVA = "0x1013DF8F4", Offset = "0x13DF8F4", VA = "0x1013DF8F4")]
			get
			{
				return null;
			}
		}

		// Token: 0x060039AC RID: 14764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003493")]
		[Address(RVA = "0x1013DF98C", Offset = "0x13DF98C", VA = "0x1013DF98C")]
		private void Update()
		{
		}

		// Token: 0x060039AD RID: 14765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003494")]
		[Address(RVA = "0x1013DFAD4", Offset = "0x13DFAD4", VA = "0x1013DFAD4")]
		public AlwaysAnimation()
		{
		}

		// Token: 0x04004889 RID: 18569
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032F5")]
		private Animation m_Animation;

		// Token: 0x0400488A RID: 18570
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032F6")]
		private float m_BeforeTime;
	}
}
