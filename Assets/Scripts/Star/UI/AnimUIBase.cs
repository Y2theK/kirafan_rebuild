﻿using System;
using UnityEngine;

namespace Star.UI {
    public abstract class AnimUIBase : MonoBehaviour {
        protected bool m_IsPlaying;
        protected bool m_IsDonePrepare;
        protected ePlayMode m_ReservePlay;
        public Action OnAnimCompleteIn;
        public Action OnAnimCompleteOut;

        public bool IsPlaying => m_IsPlaying;

        public void PlayIn() {
            m_ReservePlay = ePlayMode.In;
            m_IsPlaying = true;
        }

        public void PlayOut() {
            m_ReservePlay = ePlayMode.Out;
            m_IsPlaying = true;
        }

        public virtual void Show() {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            m_ReservePlay = ePlayMode.None;
            m_IsPlaying = false;
        }

        public virtual void Hide() {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            m_ReservePlay = ePlayMode.None;
            m_IsPlaying = false;
        }

        protected virtual void Prepare() {
            m_IsDonePrepare = true;
        }

        public virtual void Start() {
            if (!m_IsDonePrepare) {
                Prepare();
            }
        }

        protected virtual void Update() {
            if (m_ReservePlay == ePlayMode.In) {
                m_ReservePlay = ePlayMode.None;
                ExecutePlayIn();
            } else if (m_ReservePlay == ePlayMode.Out) {
                m_ReservePlay = ePlayMode.None;
                ExecutePlayOut();
            }
        }

        protected abstract void ExecutePlayIn();
        protected abstract void ExecutePlayOut();

        protected enum ePlayMode {
            None,
            In,
            Out
        }
    }
}
