﻿using System;
using UnityEngine;

namespace Star.UI {
    public class AnimUIPlayer : MonoBehaviour {
        public const int STATE_HIDE = 0;
        public const int STATE_IN = 1;
        public const int STATE_WAIT = 2;
        public const int STATE_OUT = 3;

        [Tooltip("Hide時にアクティブのままにする")]
        [SerializeField] private bool m_ActiveOnHide;

        [Tooltip("指定ASyncImageのロードが完了していない場合、Playは実行されない")]
        [SerializeField] private ASyncImage[] m_WaitASyncImages;

        private int m_State;
        private AnimUIBase[] m_AnimUIList;
        private GameObject m_GameObject;
        private bool m_IsWaitingSyncImage;

        public event Action OnAnimCompleteIn;
        public event Action OnAnimCompleteOut;

        public int State => m_State;
        public bool IsEnd => m_State == STATE_HIDE || m_State == STATE_WAIT;

        private void Awake() {
            m_GameObject = gameObject;
            m_AnimUIList = GetComponents<AnimUIBase>();
        }

        private void Start() {
            if (m_State != STATE_HIDE) { return; }
            SetState(STATE_HIDE);
        }

        private void LateUpdate() {
            if (m_State == STATE_OUT) {
                if (m_IsWaitingSyncImage) {
                    if (CheckDoneLoad()) {
                        m_IsWaitingSyncImage = false;
                        ExecPlayOut();
                    } else {
                        m_IsWaitingSyncImage = true;
                    }
                } else if (CheckEnd()) {
                    OnAnimCompleteOut?.Invoke();
                    iTween.Stop(m_GameObject);
                    SetState(STATE_HIDE);
                }
            } else if (m_State == STATE_IN) {
                if (m_IsWaitingSyncImage) {
                    if (CheckDoneLoad()) {
                        m_IsWaitingSyncImage = false;
                        ExecPlayIn();
                    } else {
                        m_IsWaitingSyncImage = true;
                    }
                } else if (CheckEnd()) {
                    OnAnimCompleteIn?.Invoke();
                    iTween.Stop(m_GameObject);
                    SetState(STATE_WAIT);
                }
            }
        }

        private void SetState(int state) {
            if (m_GameObject == null) {
                m_GameObject = gameObject;
                m_AnimUIList = GetComponents<AnimUIBase>();
            }
            m_State = state;
            m_GameObject.SetActive(state != STATE_HIDE || m_ActiveOnHide);
        }

        private bool CheckDoneLoad() {
            if (m_WaitASyncImages != null) {
                foreach (ASyncImage image in m_WaitASyncImages) {
                    if (image.GetSpriteHandler() != null && !image.IsDoneLoad()) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void PlayIn() {
            if (m_GameObject == null) {
                m_GameObject = gameObject;
            }
            if (!m_GameObject.activeSelf) {
                m_GameObject.SetActive(true);
            }
            m_AnimUIList = GetComponents<AnimUIBase>();
            SetState(STATE_IN);
            if (CheckDoneLoad()) {
                m_IsWaitingSyncImage = false;
                ExecPlayIn();
            } else {
                m_IsWaitingSyncImage = true;
            }
        }

        private void ExecPlayIn() {
            foreach (AnimUIBase animUI in m_AnimUIList) {
                animUI.PlayIn();
            }
        }

        public void PlayOut() {
            if (m_GameObject == null) {
                m_GameObject = gameObject;
            }
            if (!m_GameObject.activeSelf) {
                m_GameObject.SetActive(true);
            }
            m_AnimUIList = GetComponents<AnimUIBase>();
            SetState(STATE_OUT);
            if (CheckDoneLoad()) {
                m_IsWaitingSyncImage = false;
                ExecPlayOut();
            } else {
                m_IsWaitingSyncImage = true;
            }
        }

        private void ExecPlayOut() {
            foreach (AnimUIBase animUI in m_AnimUIList) {
                animUI.PlayOut();
            }
        }

        public void Hide() {
            m_AnimUIList = GetComponents<AnimUIBase>();
            SetState(STATE_HIDE);
            foreach (AnimUIBase animUI in m_AnimUIList) {
                animUI.Hide();
            }
        }

        public void Show() {
            m_AnimUIList = GetComponents<AnimUIBase>();
            SetState(STATE_WAIT);
            foreach (AnimUIBase animUI in m_AnimUIList) {
                animUI.Show();
            }
        }

        private bool CheckEnd() {
            foreach (AnimUIBase animUI in m_AnimUIList) {
                if (animUI.IsPlaying) {
                    return false;
                }
            }
            return true;
        }
    }
}
