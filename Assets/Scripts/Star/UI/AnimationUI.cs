﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C6B RID: 3179
	[Token(Token = "0x2000889")]
	[StructLayout(3)]
	public class AnimationUI : AnimUIBase
	{
		// Token: 0x060039B7 RID: 14775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349E")]
		[Address(RVA = "0x1013E0984", Offset = "0x13E0984", VA = "0x1013E0984", Slot = "8")]
		protected override void Update()
		{
		}

		// Token: 0x060039B8 RID: 14776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600349F")]
		[Address(RVA = "0x1013E09D0", Offset = "0x13E09D0", VA = "0x1013E09D0", Slot = "9")]
		protected override void ExecutePlayIn()
		{
		}

		// Token: 0x060039B9 RID: 14777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034A0")]
		[Address(RVA = "0x1013E0B58", Offset = "0x13E0B58", VA = "0x1013E0B58", Slot = "10")]
		protected override void ExecutePlayOut()
		{
		}

		// Token: 0x060039BA RID: 14778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034A1")]
		[Address(RVA = "0x1013E0DE4", Offset = "0x13E0DE4", VA = "0x1013E0DE4", Slot = "5")]
		public override void Hide()
		{
		}

		// Token: 0x060039BB RID: 14779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034A2")]
		[Address(RVA = "0x1013E0F6C", Offset = "0x13E0F6C", VA = "0x1013E0F6C", Slot = "4")]
		public override void Show()
		{
		}

		// Token: 0x060039BC RID: 14780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034A3")]
		[Address(RVA = "0x1013E10F4", Offset = "0x13E10F4", VA = "0x1013E10F4")]
		public AnimationUI()
		{
		}

		// Token: 0x04004896 RID: 18582
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003302")]
		[SerializeField]
		private bool m_ReturnMode;

		// Token: 0x04004897 RID: 18583
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003303")]
		[SerializeField]
		private AnimationClip m_InClip;

		// Token: 0x04004898 RID: 18584
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003304")]
		[SerializeField]
		private AnimationClip m_OutClip;

		// Token: 0x04004899 RID: 18585
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003305")]
		private Animation m_Animation;
	}
}
