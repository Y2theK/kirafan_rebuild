﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Arousal
{
	// Token: 0x02001150 RID: 4432
	[Token(Token = "0x2000B7E")]
	[StructLayout(3)]
	public class ArousalCharaPanel : MonoBehaviour
	{
		// Token: 0x06005765 RID: 22373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AF")]
		[Address(RVA = "0x1013E1104", Offset = "0x13E1104", VA = "0x1013E1104")]
		public void RegisterLvUpCallBack(Action onLvUp)
		{
		}

		// Token: 0x06005766 RID: 22374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B0")]
		[Address(RVA = "0x1013E110C", Offset = "0x13E110C", VA = "0x1013E110C")]
		public void Setup(UserArousalResultData data)
		{
		}

		// Token: 0x06005767 RID: 22375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B1")]
		[Address(RVA = "0x1013E15D4", Offset = "0x13E15D4", VA = "0x1013E15D4")]
		private void ApplyLvIcon(eRare rarity, int lv)
		{
		}

		// Token: 0x06005768 RID: 22376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B2")]
		[Address(RVA = "0x1013E17EC", Offset = "0x13E17EC", VA = "0x1013E17EC")]
		private void ApplyGauge(float currentDuplicatedCount, float needDuplicatedCount)
		{
		}

		// Token: 0x06005769 RID: 22377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B3")]
		[Address(RVA = "0x1013E1514", Offset = "0x13E1514", VA = "0x1013E1514")]
		private void ApplyCurrent()
		{
		}

		// Token: 0x0600576A RID: 22378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B4")]
		[Address(RVA = "0x1013E190C", Offset = "0x13E190C", VA = "0x1013E190C")]
		public void PlayGaugeProgress()
		{
		}

		// Token: 0x0600576B RID: 22379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B5")]
		[Address(RVA = "0x1013E1918", Offset = "0x13E1918", VA = "0x1013E1918")]
		public void SkipGaugeProgress()
		{
		}

		// Token: 0x0600576C RID: 22380 RVA: 0x0001CF80 File Offset: 0x0001B180
		[Token(Token = "0x60050B6")]
		[Address(RVA = "0x1013E1C34", Offset = "0x13E1C34", VA = "0x1013E1C34")]
		public bool IsPlayingGaugeProgress()
		{
			return default(bool);
		}

		// Token: 0x0600576D RID: 22381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B7")]
		[Address(RVA = "0x1013E19D0", Offset = "0x13E19D0", VA = "0x1013E19D0")]
		public void PlayItemDisp()
		{
		}

		// Token: 0x0600576E RID: 22382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B8")]
		[Address(RVA = "0x1013E1C3C", Offset = "0x13E1C3C", VA = "0x1013E1C3C")]
		private void Update()
		{
		}

		// Token: 0x0600576F RID: 22383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050B9")]
		[Address(RVA = "0x1013E1408", Offset = "0x13E1408", VA = "0x1013E1408")]
		private void UpdateLvParam(int lv)
		{
		}

		// Token: 0x06005770 RID: 22384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050BA")]
		[Address(RVA = "0x1013E1D64", Offset = "0x13E1D64", VA = "0x1013E1D64")]
		public ArousalCharaPanel()
		{
		}

		// Token: 0x04006A54 RID: 27220
		[Token(Token = "0x4004B22")]
		private const float GAUGE_SPEED = 2f;

		// Token: 0x04006A55 RID: 27221
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B23")]
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x04006A56 RID: 27222
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B24")]
		[SerializeField]
		private GameObject m_ItemObj;

		// Token: 0x04006A57 RID: 27223
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B25")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04006A58 RID: 27224
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004B26")]
		[SerializeField]
		private Text m_ItemNumText;

		// Token: 0x04006A59 RID: 27225
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004B27")]
		[SerializeField]
		private AnimUIPlayer m_ItemIconAnim;

		// Token: 0x04006A5A RID: 27226
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004B28")]
		[SerializeField]
		private AnimUIPlayer m_ItemNumAnim;

		// Token: 0x04006A5B RID: 27227
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004B29")]
		[SerializeField]
		private PrefabCloner m_LvIcon;

		// Token: 0x04006A5C RID: 27228
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004B2A")]
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04006A5D RID: 27229
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B2B")]
		[SerializeField]
		private Text m_NextText;

		// Token: 0x04006A5E RID: 27230
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004B2C")]
		[SerializeField]
		private LevelUpPop m_LvPop;

		// Token: 0x04006A5F RID: 27231
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004B2D")]
		private UserArousalResultData m_Data;

		// Token: 0x04006A60 RID: 27232
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004B2E")]
		private bool m_IsPlaying;

		// Token: 0x04006A61 RID: 27233
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4004B2F")]
		private eRare m_Rarity;

		// Token: 0x04006A62 RID: 27234
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004B30")]
		private int m_CurrentLv;

		// Token: 0x04006A63 RID: 27235
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4004B31")]
		private float m_CurrentDuplicatedCount;

		// Token: 0x04006A64 RID: 27236
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004B32")]
		private int m_NeedDuplicatedCount;

		// Token: 0x04006A65 RID: 27237
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4004B33")]
		private int m_FinishDuplicatedCount;

		// Token: 0x04006A66 RID: 27238
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B34")]
		private Action m_OnLvUp;
	}
}
