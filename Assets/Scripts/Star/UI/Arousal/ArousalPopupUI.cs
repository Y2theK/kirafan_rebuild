﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Arousal
{
	// Token: 0x02001151 RID: 4433
	[Token(Token = "0x2000B7F")]
	[StructLayout(3)]
	public class ArousalPopupUI : MonoBehaviour
	{
		// Token: 0x06005771 RID: 22385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050BB")]
		[Address(RVA = "0x1013E1D6C", Offset = "0x13E1D6C", VA = "0x1013E1D6C")]
		public void Open(List<UserArousalResultData> dataList, Action OnEnd)
		{
		}

		// Token: 0x06005772 RID: 22386 RVA: 0x0001CF98 File Offset: 0x0001B198
		[Token(Token = "0x60050BC")]
		[Address(RVA = "0x1013E24D8", Offset = "0x13E24D8", VA = "0x1013E24D8")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06005773 RID: 22387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050BD")]
		[Address(RVA = "0x1013E2508", Offset = "0x13E2508", VA = "0x1013E2508")]
		private void OnEndMainGroup()
		{
		}

		// Token: 0x06005774 RID: 22388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050BE")]
		[Address(RVA = "0x1013E2514", Offset = "0x13E2514", VA = "0x1013E2514")]
		public void OnClickCloseButtonCallBack()
		{
		}

		// Token: 0x06005775 RID: 22389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050BF")]
		[Address(RVA = "0x1013E2548", Offset = "0x13E2548", VA = "0x1013E2548")]
		public void OnClickSkipButtonCallBack()
		{
		}

		// Token: 0x06005776 RID: 22390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C0")]
		[Address(RVA = "0x1013E2640", Offset = "0x13E2640", VA = "0x1013E2640")]
		private void Update()
		{
		}

		// Token: 0x06005777 RID: 22391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C1")]
		[Address(RVA = "0x1013E2894", Offset = "0x13E2894", VA = "0x1013E2894")]
		private void PlayLvUpSE()
		{
		}

		// Token: 0x06005778 RID: 22392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C2")]
		[Address(RVA = "0x1013E2C48", Offset = "0x13E2C48", VA = "0x1013E2C48")]
		private void OnDestroy()
		{
		}

		// Token: 0x06005779 RID: 22393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C3")]
		[Address(RVA = "0x1013E2DA0", Offset = "0x13E2DA0", VA = "0x1013E2DA0")]
		public ArousalPopupUI()
		{
		}

		// Token: 0x04006A67 RID: 27239
		[Token(Token = "0x4004B35")]
		private const int DISP_CHARA_MAX = 10;

		// Token: 0x04006A68 RID: 27240
		[Token(Token = "0x4004B36")]
		private const int LVUP_SE_MAX = 3;

		// Token: 0x04006A69 RID: 27241
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B37")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006A6A RID: 27242
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B38")]
		[SerializeField]
		private RectTransform m_CharaPanelParent;

		// Token: 0x04006A6B RID: 27243
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B39")]
		[SerializeField]
		private ArousalCharaPanel m_ArousalCharaPanelPrefab;

		// Token: 0x04006A6C RID: 27244
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004B3A")]
		[SerializeField]
		private CustomButton m_SkipButton;

		// Token: 0x04006A6D RID: 27245
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004B3B")]
		[SerializeField]
		private Text m_OtherNumText;

		// Token: 0x04006A6E RID: 27246
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004B3C")]
		private bool m_IsDoneSetup;

		// Token: 0x04006A6F RID: 27247
		[Cpp2IlInjected.FieldOffset(Offset = "0x41")]
		[Token(Token = "0x4004B3D")]
		private bool m_IsPlaying;

		// Token: 0x04006A70 RID: 27248
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004B3E")]
		private List<ArousalCharaPanel> m_PanelList;

		// Token: 0x04006A71 RID: 27249
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004B3F")]
		private Action m_OnEnd;

		// Token: 0x04006A72 RID: 27250
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B40")]
		private SoundHandler m_GaugeSoundHandler;

		// Token: 0x04006A73 RID: 27251
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004B41")]
		private SoundHandler[] m_LvUpSoundHandlers;
	}
}
