﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Town;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Arousal
{
	// Token: 0x02001152 RID: 4434
	[Token(Token = "0x2000B80")]
	[StructLayout(3)]
	public class ArousalResultUI : MonoBehaviour
	{
		// Token: 0x0600577A RID: 22394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C4")]
		[Address(RVA = "0x1013E2E28", Offset = "0x13E2E28", VA = "0x1013E2E28")]
		public void Open(UserArousalResultData resultData, Action OnTap, Action OnEnd)
		{
		}

		// Token: 0x0600577B RID: 22395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C5")]
		[Address(RVA = "0x1013E3AB0", Offset = "0x13E3AB0", VA = "0x1013E3AB0")]
		public void Close()
		{
		}

		// Token: 0x0600577C RID: 22396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C6")]
		[Address(RVA = "0x1013E3AE4", Offset = "0x13E3AE4", VA = "0x1013E3AE4")]
		public void Abort()
		{
		}

		// Token: 0x0600577D RID: 22397 RVA: 0x0001CFB0 File Offset: 0x0001B1B0
		[Token(Token = "0x60050C7")]
		[Address(RVA = "0x1013E3B98", Offset = "0x13E3B98", VA = "0x1013E3B98")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x0600577E RID: 22398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C8")]
		[Address(RVA = "0x1013E3BC8", Offset = "0x13E3BC8", VA = "0x1013E3BC8")]
		private void OnEndMainGroup()
		{
		}

		// Token: 0x0600577F RID: 22399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050C9")]
		[Address(RVA = "0x1013E3BD4", Offset = "0x13E3BD4", VA = "0x1013E3BD4")]
		public void ShowSkipButton(Action OnSkip)
		{
		}

		// Token: 0x06005780 RID: 22400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CA")]
		[Address(RVA = "0x1013E3C0C", Offset = "0x13E3C0C", VA = "0x1013E3C0C")]
		public void HideSkipButton()
		{
		}

		// Token: 0x06005781 RID: 22401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CB")]
		[Address(RVA = "0x1013E3C40", Offset = "0x13E3C40", VA = "0x1013E3C40")]
		public void OnClickNextCallBack()
		{
		}

		// Token: 0x06005782 RID: 22402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CC")]
		[Address(RVA = "0x1013E3C4C", Offset = "0x13E3C4C", VA = "0x1013E3C4C")]
		public void OnClickSkipButtonCallBack()
		{
		}

		// Token: 0x06005783 RID: 22403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CD")]
		[Address(RVA = "0x1013E3C58", Offset = "0x13E3C58", VA = "0x1013E3C58")]
		public void FadeOut(float fadeSec)
		{
		}

		// Token: 0x06005784 RID: 22404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CE")]
		[Address(RVA = "0x1013E3CE0", Offset = "0x13E3CE0", VA = "0x1013E3CE0")]
		public void FadeIn(float fadeSec)
		{
		}

		// Token: 0x06005785 RID: 22405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050CF")]
		[Address(RVA = "0x1013E3D68", Offset = "0x13E3D68", VA = "0x1013E3D68")]
		private void Update()
		{
		}

		// Token: 0x06005786 RID: 22406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050D0")]
		[Address(RVA = "0x1013E3EB4", Offset = "0x13E3EB4", VA = "0x1013E3EB4")]
		public ArousalResultUI()
		{
		}

		// Token: 0x04006A74 RID: 27252
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004B42")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006A75 RID: 27253
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004B43")]
		[SerializeField]
		private UIGroup m_SkipButtonGroup;

		// Token: 0x04006A76 RID: 27254
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004B44")]
		[SerializeField]
		private Image m_Fade;

		// Token: 0x04006A77 RID: 27255
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004B45")]
		[SerializeField]
		private PrefabCloner m_ArousalLvIcon;

		// Token: 0x04006A78 RID: 27256
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004B46")]
		[SerializeField]
		private Text m_BeforeArousalLvText;

		// Token: 0x04006A79 RID: 27257
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004B47")]
		[SerializeField]
		private Text m_AfterArousalLvText;

		// Token: 0x04006A7A RID: 27258
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004B48")]
		[SerializeField]
		private Text m_SkillLvBeforeText;

		// Token: 0x04006A7B RID: 27259
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004B49")]
		[SerializeField]
		private Text m_SkillLvAfterText;

		// Token: 0x04006A7C RID: 27260
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004B4A")]
		[SerializeField]
		private Text m_CostBeforeText;

		// Token: 0x04006A7D RID: 27261
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004B4B")]
		[SerializeField]
		private Text m_CostAfterText;

		// Token: 0x04006A7E RID: 27262
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004B4C")]
		[SerializeField]
		private GameObject m_CostAfterObject;

		// Token: 0x04006A7F RID: 27263
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004B4D")]
		[SerializeField]
		private TextFieldValueChange m_Hp;

		// Token: 0x04006A80 RID: 27264
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004B4E")]
		[SerializeField]
		private TextFieldValueChange m_Atk;

		// Token: 0x04006A81 RID: 27265
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004B4F")]
		[SerializeField]
		private TextFieldValueChange m_Mgc;

		// Token: 0x04006A82 RID: 27266
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B50")]
		[SerializeField]
		private TextFieldValueChange m_Def;

		// Token: 0x04006A83 RID: 27267
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B51")]
		[SerializeField]
		private TextFieldValueChange m_MDef;

		// Token: 0x04006A84 RID: 27268
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B52")]
		[SerializeField]
		private TextFieldValueChange m_Spd;

		// Token: 0x04006A85 RID: 27269
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B53")]
		private bool m_IsDoneSetup;

		// Token: 0x04006A86 RID: 27270
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4004B54")]
		private ArousalResultUI.eFadeState m_FadeState;

		// Token: 0x04006A87 RID: 27271
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004B55")]
		private float m_FadeSec;

		// Token: 0x04006A88 RID: 27272
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004B56")]
		private float m_FadeFinishSec;

		// Token: 0x04006A89 RID: 27273
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004B57")]
		private Action m_OnTap;

		// Token: 0x04006A8A RID: 27274
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004B58")]
		private Action m_OnEnd;

		// Token: 0x04006A8B RID: 27275
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004B59")]
		private Action m_OnSkip;

		// Token: 0x02001153 RID: 4435
		[Token(Token = "0x2001293")]
		private enum eFadeState
		{
			// Token: 0x04006A8D RID: 27277
			[Token(Token = "0x4007267")]
			None,
			// Token: 0x04006A8E RID: 27278
			[Token(Token = "0x4007268")]
			In,
			// Token: 0x04006A8F RID: 27279
			[Token(Token = "0x4007269")]
			Out
		}
	}
}
