﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C4E RID: 3150
	[Token(Token = "0x200086F")]
	[StructLayout(3)]
	public class ArousalLvGauge : MonoBehaviour
	{
		// Token: 0x06003906 RID: 14598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F0")]
		[Address(RVA = "0x1013E3EBC", Offset = "0x13E3EBC", VA = "0x1013E3EBC")]
		private void Start()
		{
		}

		// Token: 0x06003907 RID: 14599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F1")]
		[Address(RVA = "0x1013E3EC0", Offset = "0x13E3EC0", VA = "0x1013E3EC0")]
		public void Apply(eRare rare, int arousalLv, int duplicatedCount)
		{
		}

		// Token: 0x06003908 RID: 14600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F2")]
		[Address(RVA = "0x1013E40EC", Offset = "0x13E40EC", VA = "0x1013E40EC")]
		private void Update()
		{
		}

		// Token: 0x06003909 RID: 14601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F3")]
		[Address(RVA = "0x1013E40F0", Offset = "0x13E40F0", VA = "0x1013E40F0")]
		public ArousalLvGauge()
		{
		}

		// Token: 0x04004840 RID: 18496
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032BC")]
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04004841 RID: 18497
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032BD")]
		[SerializeField]
		private Text m_TextValue;
	}
}
