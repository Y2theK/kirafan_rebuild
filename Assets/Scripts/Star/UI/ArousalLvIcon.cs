﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C4F RID: 3151
	[Token(Token = "0x2000870")]
	[StructLayout(3)]
	public class ArousalLvIcon : MonoBehaviour
	{
		// Token: 0x0600390A RID: 14602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F4")]
		[Address(RVA = "0x1013E16B4", Offset = "0x13E16B4", VA = "0x1013E16B4")]
		public void Apply(int arousalLv, bool isMax = false)
		{
		}

		// Token: 0x0600390B RID: 14603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F5")]
		[Address(RVA = "0x1013E40F8", Offset = "0x13E40F8", VA = "0x1013E40F8")]
		public ArousalLvIcon()
		{
		}

		// Token: 0x04004842 RID: 18498
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032BE")]
		[SerializeField]
		private Image m_ImageNumberMax;

		// Token: 0x04004843 RID: 18499
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032BF")]
		[SerializeField]
		private ImageNumbers m_ImageNumber;
	}
}
