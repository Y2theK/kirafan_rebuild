﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C72 RID: 3186
	[Token(Token = "0x200088F")]
	[StructLayout(3)]
	public class ArousalStar : UIBehaviour
	{
		// Token: 0x060039F2 RID: 14834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D9")]
		[Address(RVA = "0x1013E4100", Offset = "0x13E4100", VA = "0x1013E4100")]
		public void SetUp()
		{
		}

		// Token: 0x060039F3 RID: 14835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DA")]
		[Address(RVA = "0x1013E4170", Offset = "0x13E4170", VA = "0x1013E4170")]
		public void Apply(eRare rare, int arousalLv)
		{
		}

		// Token: 0x060039F4 RID: 14836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DB")]
		[Address(RVA = "0x1013E42EC", Offset = "0x13E42EC", VA = "0x1013E42EC")]
		public ArousalStar()
		{
		}

		// Token: 0x040048CE RID: 18638
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003336")]
		private Image[] m_IconStars;
	}
}
