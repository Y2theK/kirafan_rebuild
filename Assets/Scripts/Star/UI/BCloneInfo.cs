﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C66 RID: 3174
	[Token(Token = "0x2000884")]
	[Serializable]
	[StructLayout(3)]
	public class BCloneInfo
	{
		// Token: 0x06003997 RID: 14743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600347E")]
		[Address(RVA = "0x1013E42F4", Offset = "0x13E42F4", VA = "0x1013E42F4", Slot = "4")]
		public virtual void Setup(Transform parent)
		{
		}

		// Token: 0x06003998 RID: 14744 RVA: 0x00017FA0 File Offset: 0x000161A0
		[Token(Token = "0x600347F")]
		[Address(RVA = "0x1013E42F8", Offset = "0x13E42F8", VA = "0x1013E42F8", Slot = "5")]
		public virtual bool IsExistPrefab()
		{
			return default(bool);
		}

		// Token: 0x06003999 RID: 14745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003480")]
		[Address(RVA = "0x1013E4300", Offset = "0x13E4300", VA = "0x1013E4300", Slot = "6")]
		public virtual void SetLocalPositionX(float x)
		{
		}

		// Token: 0x0600399A RID: 14746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003481")]
		[Address(RVA = "0x1013E4304", Offset = "0x13E4304", VA = "0x1013E4304", Slot = "7")]
		public virtual void SetLocalPositionY(float y)
		{
		}

		// Token: 0x0600399B RID: 14747 RVA: 0x00017FB8 File Offset: 0x000161B8
		[Token(Token = "0x6003482")]
		[Address(RVA = "0x1013E4308", Offset = "0x13E4308", VA = "0x1013E4308", Slot = "8")]
		public virtual float GetAxisRange()
		{
			return 0f;
		}

		// Token: 0x0600399C RID: 14748 RVA: 0x00017FD0 File Offset: 0x000161D0
		[Token(Token = "0x6003483")]
		[Address(RVA = "0x1013E4310", Offset = "0x13E4310", VA = "0x1013E4310", Slot = "9")]
		public virtual int GetDepth()
		{
			return 0;
		}

		// Token: 0x0600399D RID: 14749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003484")]
		[Address(RVA = "0x1013E4318", Offset = "0x13E4318", VA = "0x1013E4318")]
		public BCloneInfo()
		{
		}
	}
}
