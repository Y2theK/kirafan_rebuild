﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C83 RID: 3203
	[Token(Token = "0x2000898")]
	[StructLayout(3)]
	public abstract class BRangeSelector : MonoBehaviour
	{
		// Token: 0x06003A36 RID: 14902 RVA: 0x00018168 File Offset: 0x00016368
		[Token(Token = "0x6003518")]
		[Address(RVA = "0x1013E4320", Offset = "0x13E4320", VA = "0x1013E4320")]
		public static bool eValueTypeToIsInteger(BRangeSelector.eValueType valueType)
		{
			return default(bool);
		}

		// Token: 0x06003A37 RID: 14903 RVA: 0x00018180 File Offset: 0x00016380
		[Token(Token = "0x6003519")]
		[Address(RVA = "0x1013E432C", Offset = "0x13E432C", VA = "0x1013E432C")]
		public static int ePlusMinusToArrayIndex(BRangeSelector.ePlusMinus plusMinus)
		{
			return 0;
		}

		// Token: 0x14000066 RID: 102
		// (add) Token: 0x06003A38 RID: 14904 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003A39 RID: 14905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000066")]
		public event Action OnValueChangedSliderInternal
		{
			[Token(Token = "0x600351A")]
			[Address(RVA = "0x1013E4340", Offset = "0x13E4340", VA = "0x1013E4340")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600351B")]
			[Address(RVA = "0x1013E444C", Offset = "0x13E444C", VA = "0x1013E444C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000067 RID: 103
		// (add) Token: 0x06003A3A RID: 14906 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003A3B RID: 14907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000067")]
		public event Action OnValueChangedButtonInternal
		{
			[Token(Token = "0x600351C")]
			[Address(RVA = "0x1013E4558", Offset = "0x13E4558", VA = "0x1013E4558")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600351D")]
			[Address(RVA = "0x1013E4664", Offset = "0x13E4664", VA = "0x1013E4664")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000068 RID: 104
		// (add) Token: 0x06003A3C RID: 14908 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003A3D RID: 14909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000068")]
		public event Action OnValueChangedButtonHoldInternal
		{
			[Token(Token = "0x600351E")]
			[Address(RVA = "0x1013E4770", Offset = "0x13E4770", VA = "0x1013E4770")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600351F")]
			[Address(RVA = "0x1013E487C", Offset = "0x13E487C", VA = "0x1013E487C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000069 RID: 105
		// (add) Token: 0x06003A3E RID: 14910 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003A3F RID: 14911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000069")]
		public event Action OnValueChanged
		{
			[Token(Token = "0x6003520")]
			[Address(RVA = "0x1013E4988", Offset = "0x13E4988", VA = "0x1013E4988")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003521")]
			[Address(RVA = "0x1013E4A94", Offset = "0x13E4A94", VA = "0x1013E4A94")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003A40 RID: 14912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003522")]
		[Address(RVA = "0x1013E4BA0", Offset = "0x13E4BA0", VA = "0x1013E4BA0")]
		protected void SetupNumbersInformation(BRangeSelector.RangeSelectorNumbersInformation numbersInformation)
		{
		}

		// Token: 0x06003A41 RID: 14913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003523")]
		[Address(RVA = "0x1013E4BB8", Offset = "0x13E4BB8", VA = "0x1013E4BB8")]
		protected void AddSliderValueChangedCallback(UnityAction<float> action)
		{
		}

		// Token: 0x06003A42 RID: 14914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003524")]
		[Address(RVA = "0x1013E4C88", Offset = "0x13E4C88", VA = "0x1013E4C88")]
		protected void AddButtonClickProcess(BRangeSelector.ePlusMinus plusMinus, UnityAction action)
		{
		}

		// Token: 0x06003A43 RID: 14915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003525")]
		[Address(RVA = "0x1013E4D88", Offset = "0x13E4D88", VA = "0x1013E4D88")]
		protected void AddEventTriggerProcess(EventTrigger eventTrigger, EventTriggerType triggerType, UnityAction<BaseEventData> action)
		{
		}

		// Token: 0x06003A44 RID: 14916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003526")]
		[Address(RVA = "0x1013E4EB4", Offset = "0x13E4EB4", VA = "0x1013E4EB4")]
		public void SetValueType(BRangeSelector.eValueType valueType)
		{
		}

		// Token: 0x06003A45 RID: 14917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003527")]
		[Address(RVA = "0x1013E4EF8", Offset = "0x13E4EF8", VA = "0x1013E4EF8")]
		protected void RefreshSlider()
		{
		}

		// Token: 0x06003A46 RID: 14918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003528")]
		[Address(RVA = "0x1013E5104", Offset = "0x13E5104", VA = "0x1013E5104")]
		protected void RefreshButton(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A47 RID: 14919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003529")]
		[Address(RVA = "0x1013E5208", Offset = "0x13E5208", VA = "0x1013E5208")]
		private void Start()
		{
		}

		// Token: 0x06003A48 RID: 14920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352A")]
		[Address(RVA = "0x1013E5258", Offset = "0x13E5258", VA = "0x1013E5258")]
		private void Update()
		{
		}

		// Token: 0x06003A49 RID: 14921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352B")]
		[Address(RVA = "0x1013E5284", Offset = "0x13E5284", VA = "0x1013E5284")]
		protected void UpdateButton(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A4A RID: 14922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352C")]
		[Address(RVA = "0x1013E562C", Offset = "0x13E562C", VA = "0x1013E562C")]
		public void SetSliderActive(bool active)
		{
		}

		// Token: 0x06003A4B RID: 14923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352D")]
		[Address(RVA = "0x1013E541C", Offset = "0x13E541C", VA = "0x1013E541C")]
		private void AdvanceHoldWaitTime(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A4C RID: 14924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352E")]
		[Address(RVA = "0x1013E56F4", Offset = "0x13E56F4", VA = "0x1013E56F4")]
		private void SubtractHoldWaitTime(BRangeSelector.ePlusMinus plusMinus, float value)
		{
		}

		// Token: 0x06003A4D RID: 14925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600352F")]
		[Address(RVA = "0x1013E582C", Offset = "0x13E582C", VA = "0x1013E582C")]
		protected void StartHoldWaitTimer(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A4E RID: 14926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003530")]
		[Address(RVA = "0x1013E5534", Offset = "0x13E5534", VA = "0x1013E5534")]
		private void RestartHoldWaitTimer(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A4F RID: 14927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003531")]
		[Address(RVA = "0x1013E5918", Offset = "0x13E5918", VA = "0x1013E5918")]
		protected void KillHoldWaitTimer(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003A50 RID: 14928 RVA: 0x00018198 File Offset: 0x00016398
		[Token(Token = "0x6003532")]
		[Address(RVA = "0x1013E4FDC", Offset = "0x13E4FDC", VA = "0x1013E4FDC")]
		protected bool IsHold()
		{
			return default(bool);
		}

		// Token: 0x06003A51 RID: 14929 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003533")]
		[Address(RVA = "0x1013E4D5C", Offset = "0x13E4D5C", VA = "0x1013E4D5C")]
		protected CustomButton GetButton(BRangeSelector.ePlusMinus plusMinus)
		{
			return null;
		}

		// Token: 0x06003A52 RID: 14930 RVA: 0x000181B0 File Offset: 0x000163B0
		[Token(Token = "0x6003534")]
		[Address(RVA = "0x1013E593C", Offset = "0x13E593C", VA = "0x1013E593C")]
		protected float GetValueChangeButtonClickedChangeValue(BRangeSelector.ePlusMinus plusMinus)
		{
			return 0f;
		}

		// Token: 0x06003A53 RID: 14931 RVA: 0x000181C8 File Offset: 0x000163C8
		[Token(Token = "0x6003535")]
		[Address(RVA = "0x1013E58EC", Offset = "0x13E58EC", VA = "0x1013E58EC")]
		private float? GetHoldWaitTime(BRangeSelector.ePlusMinus plusMinus)
		{
			return null;
		}

		// Token: 0x06003A54 RID: 14932 RVA: 0x000181E0 File Offset: 0x000163E0
		[Token(Token = "0x6003536")]
		[Address(RVA = "0x1013E52F8", Offset = "0x13E52F8", VA = "0x1013E52F8")]
		public bool IsButtonHold(BRangeSelector.ePlusMinus plusMinus)
		{
			return default(bool);
		}

		// Token: 0x06003A55 RID: 14933 RVA: 0x000181F8 File Offset: 0x000163F8
		[Token(Token = "0x6003537")]
		[Address(RVA = "0x1013E504C", Offset = "0x13E504C", VA = "0x1013E504C")]
		public bool IsCanValueChange(BRangeSelector.ePlusMinus plusMinus)
		{
			return default(bool);
		}

		// Token: 0x06003A56 RID: 14934 RVA: 0x00018210 File Offset: 0x00016410
		[Token(Token = "0x6003538")]
		[Address(RVA = "0x1013E544C", Offset = "0x13E544C", VA = "0x1013E544C")]
		private bool IsTimeOverHoldWait(BRangeSelector.ePlusMinus plusMinus)
		{
			return default(bool);
		}

		// Token: 0x06003A57 RID: 14935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003539")]
		[Address(RVA = "0x1013E5134", Offset = "0x13E5134", VA = "0x1013E5134")]
		private void SetButtonInteractable(BRangeSelector.ePlusMinus plusMinus, bool interactable)
		{
		}

		// Token: 0x06003A58 RID: 14936
		[Token(Token = "0x600353A")]
		[Address(Slot = "4")]
		protected abstract void OnTimeOverHoldWait(BRangeSelector.ePlusMinus plusMinus);

		// Token: 0x06003A59 RID: 14937 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600353B")]
		[Address(RVA = "0x1013E596C", Offset = "0x13E596C", VA = "0x1013E596C")]
		protected BRangeSelector.RangeSelectorMinimumNumbersInformation GetMinimumNumbersInformation()
		{
			return null;
		}

		// Token: 0x06003A5A RID: 14938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600353C")]
		[Address(RVA = "0x1013E5974", Offset = "0x13E5974", VA = "0x1013E5974")]
		protected BRangeSelector.RangeSelectorSelectingNumbersInformation GetSelectingNumbersInformation()
		{
			return null;
		}

		// Token: 0x06003A5B RID: 14939 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600353D")]
		[Address(RVA = "0x1013E597C", Offset = "0x13E597C", VA = "0x1013E597C")]
		protected BRangeSelector.RangeSelectorMaximumNumbersInformation GetMaximumNumbersInformation()
		{
			return null;
		}

		// Token: 0x06003A5C RID: 14940 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600353E")]
		[Address(RVA = "0x1013E5984", Offset = "0x13E5984", VA = "0x1013E5984")]
		protected Slider GetSlider()
		{
			return null;
		}

		// Token: 0x06003A5D RID: 14941 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600353F")]
		[Address(RVA = "0x1013E598C", Offset = "0x13E598C", VA = "0x1013E598C")]
		protected CustomButton GetMinusButton()
		{
			return null;
		}

		// Token: 0x06003A5E RID: 14942 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003540")]
		[Address(RVA = "0x1013E5994", Offset = "0x13E5994", VA = "0x1013E5994")]
		protected CustomButton GetPlusButton()
		{
			return null;
		}

		// Token: 0x06003A5F RID: 14943 RVA: 0x00018228 File Offset: 0x00016428
		[Token(Token = "0x6003541")]
		[Address(RVA = "0x1013E5964", Offset = "0x13E5964", VA = "0x1013E5964")]
		protected float GetValueChangeButtonClickedChangeAbsoluteValue()
		{
			return 0f;
		}

		// Token: 0x06003A60 RID: 14944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003542")]
		[Address(RVA = "0x1013E599C", Offset = "0x13E599C", VA = "0x1013E599C")]
		public void ExecuteOnValueChangedSliderInternal()
		{
		}

		// Token: 0x06003A61 RID: 14945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003543")]
		[Address(RVA = "0x1013E59A8", Offset = "0x13E59A8", VA = "0x1013E59A8")]
		public void ExecuteOnValueChangedButtonInternal()
		{
		}

		// Token: 0x06003A62 RID: 14946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003544")]
		[Address(RVA = "0x1013E59B4", Offset = "0x13E59B4", VA = "0x1013E59B4")]
		public void ExecuteOnValueChangedButtonHoldInternal()
		{
		}

		// Token: 0x06003A63 RID: 14947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003545")]
		[Address(RVA = "0x1013E59C0", Offset = "0x13E59C0", VA = "0x1013E59C0")]
		public void ExecuteOnValueChanged()
		{
		}

		// Token: 0x06003A64 RID: 14948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003546")]
		[Address(RVA = "0x1013E59CC", Offset = "0x13E59CC", VA = "0x1013E59CC")]
		protected BRangeSelector()
		{
		}

		// Token: 0x04004929 RID: 18729
		[Token(Token = "0x4003377")]
		protected const float HOLD_START_WAIT = 0.4f;

		// Token: 0x0400492A RID: 18730
		[Token(Token = "0x4003378")]
		protected const float HOLD_REPEAT_WAIT = 0.075f;

		// Token: 0x0400492B RID: 18731
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003379")]
		[SerializeField]
		private BRangeSelector.RangeSelectorMinimumNumbersInformation m_MinimumNumbersInformation;

		// Token: 0x0400492C RID: 18732
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400337A")]
		[SerializeField]
		private BRangeSelector.RangeSelectorSelectingNumbersInformation m_SelectingNumbersInformation;

		// Token: 0x0400492D RID: 18733
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400337B")]
		[SerializeField]
		private BRangeSelector.RangeSelectorMaximumNumbersInformation m_MaximumNumbersInformation;

		// Token: 0x0400492E RID: 18734
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400337C")]
		[SerializeField]
		private Slider m_Slider;

		// Token: 0x0400492F RID: 18735
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400337D")]
		[SerializeField]
		private CustomButton m_MinusButton;

		// Token: 0x04004930 RID: 18736
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400337E")]
		[SerializeField]
		private CustomButton m_PlusButton;

		// Token: 0x04004931 RID: 18737
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400337F")]
		private float m_ValueChangeButtonClickedChangeAbsoluteValue;

		// Token: 0x04004932 RID: 18738
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4003380")]
		private float? m_MinusWait;

		// Token: 0x04004933 RID: 18739
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003381")]
		private float? m_PlusWait;

		// Token: 0x02000C84 RID: 3204
		[Token(Token = "0x20010B3")]
		public enum eValueType
		{
			// Token: 0x04004939 RID: 18745
			[Token(Token = "0x4006905")]
			Error,
			// Token: 0x0400493A RID: 18746
			[Token(Token = "0x4006906")]
			Integer,
			// Token: 0x0400493B RID: 18747
			[Token(Token = "0x4006907")]
			Float
		}

		// Token: 0x02000C85 RID: 3205
		[Token(Token = "0x20010B4")]
		public enum ePlusMinus
		{
			// Token: 0x0400493D RID: 18749
			[Token(Token = "0x4006909")]
			Error,
			// Token: 0x0400493E RID: 18750
			[Token(Token = "0x400690A")]
			Minus,
			// Token: 0x0400493F RID: 18751
			[Token(Token = "0x400690B")]
			Plus
		}

		// Token: 0x02000C86 RID: 3206
		[Token(Token = "0x20010B5")]
		public enum eChangedValueFromType
		{
			// Token: 0x04004941 RID: 18753
			[Token(Token = "0x400690D")]
			Error,
			// Token: 0x04004942 RID: 18754
			[Token(Token = "0x400690E")]
			None,
			// Token: 0x04004943 RID: 18755
			[Token(Token = "0x400690F")]
			Slider,
			// Token: 0x04004944 RID: 18756
			[Token(Token = "0x4006910")]
			ButtonClick,
			// Token: 0x04004945 RID: 18757
			[Token(Token = "0x4006911")]
			ButtonHold,
			// Token: 0x04004946 RID: 18758
			[Token(Token = "0x4006912")]
			Manual
		}

		// Token: 0x02000C87 RID: 3207
		[Token(Token = "0x20010B6")]
		public abstract class BRangeSelectorNumbersInformation
		{
			// Token: 0x06003A65 RID: 14949 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061D2")]
			[Address(RVA = "0x1013E59DC", Offset = "0x13E59DC", VA = "0x1013E59DC")]
			protected Slider GetSlider()
			{
				return null;
			}

			// Token: 0x06003A66 RID: 14950 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061D3")]
			[Address(RVA = "0x1013E4BB0", Offset = "0x13E4BB0", VA = "0x1013E4BB0")]
			public void SetSlider(Slider slider)
			{
			}

			// Token: 0x06003A67 RID: 14951 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061D4")]
			[Address(RVA = "0x1013E59E4", Offset = "0x13E59E4", VA = "0x1013E59E4", Slot = "4")]
			public virtual void Setup()
			{
			}

			// Token: 0x06003A68 RID: 14952
			[Token(Token = "0x60061D5")]
			[Address(Slot = "5")]
			public abstract float GetValue();

			// Token: 0x06003A69 RID: 14953 RVA: 0x00018240 File Offset: 0x00016440
			[Token(Token = "0x60061D6")]
			[Address(RVA = "0x1013E59E8", Offset = "0x13E59E8", VA = "0x1013E59E8")]
			protected bool IsOriginalValueAvailable()
			{
				return default(bool);
			}

			// Token: 0x06003A6A RID: 14954
			[Token(Token = "0x60061D7")]
			[Address(Slot = "6")]
			protected abstract float GetOriginalValue();

			// Token: 0x06003A6B RID: 14955
			[Token(Token = "0x60061D8")]
			[Address(Slot = "7")]
			public abstract float SetOriginalValue(float value);

			// Token: 0x06003A6C RID: 14956 RVA: 0x00018258 File Offset: 0x00016458
			[Token(Token = "0x60061D9")]
			[Address(RVA = "0x1013E5A58", Offset = "0x13E5A58", VA = "0x1013E5A58")]
			protected bool IsValueStaged()
			{
				return default(bool);
			}

			// Token: 0x06003A6D RID: 14957 RVA: 0x00018270 File Offset: 0x00016470
			[Token(Token = "0x60061DA")]
			[Address(RVA = "0x1013E5AC8", Offset = "0x13E5AC8", VA = "0x1013E5AC8")]
			protected float GetStagingValue()
			{
				return 0f;
			}

			// Token: 0x06003A6E RID: 14958 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061DB")]
			[Address(RVA = "0x1013E5B34", Offset = "0x13E5B34", VA = "0x1013E5B34")]
			protected void StagingValue(float value)
			{
			}

			// Token: 0x06003A6F RID: 14959 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061DC")]
			[Address(RVA = "0x1013E5BAC", Offset = "0x13E5BAC", VA = "0x1013E5BAC")]
			protected void UnstagingValue()
			{
			}

			// Token: 0x06003A70 RID: 14960 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061DD")]
			[Address(RVA = "0x1013E5BB4", Offset = "0x13E5BB4", VA = "0x1013E5BB4")]
			protected void RefreshImageNumbers()
			{
			}

			// Token: 0x06003A71 RID: 14961 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061DE")]
			[Address(RVA = "0x1013E5C78", Offset = "0x13E5C78", VA = "0x1013E5C78")]
			protected void RefreshTextNumbers()
			{
			}

			// Token: 0x06003A72 RID: 14962 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061DF")]
			[Address(RVA = "0x1013E5D54", Offset = "0x13E5D54", VA = "0x1013E5D54")]
			protected BRangeSelectorNumbersInformation()
			{
			}

			// Token: 0x04004947 RID: 18759
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006913")]
			[SerializeField]
			private ImageNumbers m_ImageNumbersNumbers;

			// Token: 0x04004948 RID: 18760
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006914")]
			[SerializeField]
			private Text m_TextNumbers;

			// Token: 0x04004949 RID: 18761
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006915")]
			private Slider m_Slider;

			// Token: 0x0400494A RID: 18762
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006916")]
			private float? m_StagingValue;
		}

		// Token: 0x02000C88 RID: 3208
		[Token(Token = "0x20010B7")]
		[Serializable]
		public abstract class RangeSelectorNumbersInformation : BRangeSelector.BRangeSelectorNumbersInformation
		{
			// Token: 0x06003A73 RID: 14963 RVA: 0x00018288 File Offset: 0x00016488
			[Token(Token = "0x60061E0")]
			[Address(RVA = "0x1013E5F9C", Offset = "0x13E5F9C", VA = "0x1013E5F9C", Slot = "5")]
			public override float GetValue()
			{
				return 0f;
			}

			// Token: 0x06003A74 RID: 14964 RVA: 0x000182A0 File Offset: 0x000164A0
			[Token(Token = "0x60061E1")]
			[Address(RVA = "0x1013E5FF8", Offset = "0x13E5FF8", VA = "0x1013E5FF8")]
			public float SetValue(float value)
			{
				return 0f;
			}

			// Token: 0x06003A75 RID: 14965 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061E2")]
			[Address(RVA = "0x1013E5EE0", Offset = "0x13E5EE0", VA = "0x1013E5EE0")]
			protected RangeSelectorNumbersInformation()
			{
			}
		}

		// Token: 0x02000C89 RID: 3209
		[Token(Token = "0x20010B8")]
		[Serializable]
		public class RangeSelectorMinimumNumbersInformation : BRangeSelector.RangeSelectorNumbersInformation
		{
			// Token: 0x06003A76 RID: 14966 RVA: 0x000182B8 File Offset: 0x000164B8
			[Token(Token = "0x60061E3")]
			[Address(RVA = "0x1013E5EE8", Offset = "0x13E5EE8", VA = "0x1013E5EE8", Slot = "6")]
			protected override float GetOriginalValue()
			{
				return 0f;
			}

			// Token: 0x06003A77 RID: 14967 RVA: 0x000182D0 File Offset: 0x000164D0
			[Token(Token = "0x60061E4")]
			[Address(RVA = "0x1013E5F34", Offset = "0x13E5F34", VA = "0x1013E5F34", Slot = "7")]
			public override float SetOriginalValue(float value)
			{
				return 0f;
			}

			// Token: 0x06003A78 RID: 14968 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061E5")]
			[Address(RVA = "0x1013E5F94", Offset = "0x13E5F94", VA = "0x1013E5F94")]
			public RangeSelectorMinimumNumbersInformation()
			{
			}
		}

		// Token: 0x02000C8A RID: 3210
		[Token(Token = "0x20010B9")]
		[Serializable]
		public class RangeSelectorSelectingNumbersInformation : BRangeSelector.RangeSelectorNumbersInformation
		{
			// Token: 0x06003A79 RID: 14969 RVA: 0x000182E8 File Offset: 0x000164E8
			[Token(Token = "0x60061E6")]
			[Address(RVA = "0x1013E6080", Offset = "0x13E6080", VA = "0x1013E6080", Slot = "6")]
			protected override float GetOriginalValue()
			{
				return 0f;
			}

			// Token: 0x06003A7A RID: 14970 RVA: 0x00018300 File Offset: 0x00016500
			[Token(Token = "0x60061E7")]
			[Address(RVA = "0x1013E60D4", Offset = "0x13E60D4", VA = "0x1013E60D4", Slot = "7")]
			public override float SetOriginalValue(float value)
			{
				return 0f;
			}

			// Token: 0x06003A7B RID: 14971 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061E8")]
			[Address(RVA = "0x1013E612C", Offset = "0x13E612C", VA = "0x1013E612C")]
			public RangeSelectorSelectingNumbersInformation()
			{
			}
		}

		// Token: 0x02000C8B RID: 3211
		[Token(Token = "0x20010BA")]
		[Serializable]
		public class RangeSelectorMaximumNumbersInformation : BRangeSelector.RangeSelectorNumbersInformation
		{
			// Token: 0x06003A7C RID: 14972 RVA: 0x00018318 File Offset: 0x00016518
			[Token(Token = "0x60061E9")]
			[Address(RVA = "0x1013E5E2C", Offset = "0x13E5E2C", VA = "0x1013E5E2C", Slot = "6")]
			protected override float GetOriginalValue()
			{
				return 0f;
			}

			// Token: 0x06003A7D RID: 14973 RVA: 0x00018330 File Offset: 0x00016530
			[Token(Token = "0x60061EA")]
			[Address(RVA = "0x1013E5E78", Offset = "0x13E5E78", VA = "0x1013E5E78", Slot = "7")]
			public override float SetOriginalValue(float value)
			{
				return 0f;
			}

			// Token: 0x06003A7E RID: 14974 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061EB")]
			[Address(RVA = "0x1013E5ED8", Offset = "0x13E5ED8", VA = "0x1013E5ED8")]
			public RangeSelectorMaximumNumbersInformation()
			{
			}
		}

		// Token: 0x02000C8C RID: 3212
		[Token(Token = "0x20010BB")]
		public abstract class PlusMinusPairBaseGen<GenType>
		{
			// Token: 0x06003A7F RID: 14975 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061EC")]
			[Address(RVA = "0x1016CD728", Offset = "0x16CD728", VA = "0x1016CD728")]
			public PlusMinusPairBaseGen(GenType minus, GenType plus)
			{
			}

			// Token: 0x06003A80 RID: 14976 RVA: 0x00018348 File Offset: 0x00016548
			[Token(Token = "0x60061ED")]
			[Address(RVA = "0x1016CD76C", Offset = "0x16CD76C", VA = "0x1016CD76C")]
			public int HowManyCallbacks()
			{
				return 0;
			}

			// Token: 0x06003A81 RID: 14977 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061EE")]
			[Address(RVA = "0x1016CD774", Offset = "0x16CD774", VA = "0x1016CD774")]
			public GenType Get(BRangeSelector.ePlusMinus plusMinus)
			{
				return null;
			}

			// Token: 0x0400494B RID: 18763
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006917")]
			private GenType m_Minus;

			// Token: 0x0400494C RID: 18764
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006918")]
			private GenType m_Plus;
		}

		// Token: 0x02000C8D RID: 3213
		[Token(Token = "0x20010BC")]
		public class OnClickButtonPlusMinusCallbackPair : BRangeSelector.PlusMinusPairBaseGen<UnityAction>
		{
			// Token: 0x06003A82 RID: 14978 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061EF")]
			[Address(RVA = "0x1013E5DC4", Offset = "0x13E5DC4", VA = "0x1013E5DC4")]
			public OnClickButtonPlusMinusCallbackPair([Optional] UnityAction minusCallback, [Optional] UnityAction plusCallback)
			{
			}
		}

		// Token: 0x02000C8E RID: 3214
		[Token(Token = "0x20010BD")]
		public class EventTriggerPlusMinusCallbackPair : BRangeSelector.PlusMinusPairBaseGen<UnityAction<BaseEventData>>
		{
			// Token: 0x06003A83 RID: 14979 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061F0")]
			[Address(RVA = "0x1013E5D5C", Offset = "0x13E5D5C", VA = "0x1013E5D5C")]
			public EventTriggerPlusMinusCallbackPair([Optional] UnityAction<BaseEventData> minusCallback, [Optional] UnityAction<BaseEventData> plusCallback)
			{
			}
		}
	}
}
