﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.BackGround
{
	// Token: 0x0200101C RID: 4124
	[Token(Token = "0x2000AB9")]
	[StructLayout(3)]
	public class UIBackGroundHandler : MonoBehaviour
	{
		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06004EE6 RID: 20198 RVA: 0x0001B930 File Offset: 0x00019B30
		// (set) Token: 0x06004EE7 RID: 20199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004F7")]
		public bool Fade
		{
			[Token(Token = "0x600486C")]
			[Address(RVA = "0x1013E6134", Offset = "0x13E6134", VA = "0x1013E6134")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600486D")]
			[Address(RVA = "0x1013E613C", Offset = "0x13E613C", VA = "0x1013E613C")]
			set
			{
			}
		}

		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06004EE8 RID: 20200 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004F8")]
		public string FilePath
		{
			[Token(Token = "0x600486E")]
			[Address(RVA = "0x1013E6148", Offset = "0x13E6148", VA = "0x1013E6148")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x06004EE9 RID: 20201 RVA: 0x0001B948 File Offset: 0x00019B48
		[Token(Token = "0x170004F9")]
		public UIBackGroundManager.eBGResourceType ResourceType
		{
			[Token(Token = "0x600486F")]
			[Address(RVA = "0x1013E6150", Offset = "0x13E6150", VA = "0x1013E6150")]
			get
			{
				return UIBackGroundManager.eBGResourceType.BackGround;
			}
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06004EEA RID: 20202 RVA: 0x0001B960 File Offset: 0x00019B60
		// (set) Token: 0x06004EEB RID: 20203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004FA")]
		public UIBackGroundManager.eBackGroundOrderID BGOrderID
		{
			[Token(Token = "0x6004870")]
			[Address(RVA = "0x1013E6158", Offset = "0x13E6158", VA = "0x1013E6158")]
			get
			{
				return UIBackGroundManager.eBackGroundOrderID.Low;
			}
			[Token(Token = "0x6004871")]
			[Address(RVA = "0x1013E6160", Offset = "0x13E6160", VA = "0x1013E6160")]
			set
			{
			}
		}

		// Token: 0x06004EEC RID: 20204 RVA: 0x0001B978 File Offset: 0x00019B78
		[Token(Token = "0x6004872")]
		[Address(RVA = "0x1013E6168", Offset = "0x13E6168", VA = "0x1013E6168")]
		public bool IsActive()
		{
			return default(bool);
		}

		// Token: 0x06004EED RID: 20205 RVA: 0x0001B990 File Offset: 0x00019B90
		[Token(Token = "0x6004873")]
		[Address(RVA = "0x1013E6170", Offset = "0x13E6170", VA = "0x1013E6170")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06004EEE RID: 20206 RVA: 0x0001B9A8 File Offset: 0x00019BA8
		[Token(Token = "0x6004874")]
		[Address(RVA = "0x1013E61F0", Offset = "0x13E61F0", VA = "0x1013E61F0")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x06004EEF RID: 20207 RVA: 0x0001B9C0 File Offset: 0x00019BC0
		// (set) Token: 0x06004EF0 RID: 20208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004FB")]
		public bool RetryOnError
		{
			[Token(Token = "0x6004875")]
			[Address(RVA = "0x1013E61E8", Offset = "0x13E61E8", VA = "0x1013E61E8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004876")]
			[Address(RVA = "0x1013E6200", Offset = "0x13E6200", VA = "0x1013E6200")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06004EF1 RID: 20209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004877")]
		[Address(RVA = "0x1013E6208", Offset = "0x13E6208", VA = "0x1013E6208")]
		public void Setup(UIBackGroundManager owner, Camera camera)
		{
		}

		// Token: 0x06004EF2 RID: 20210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004878")]
		[Address(RVA = "0x1013E62D0", Offset = "0x13E62D0", VA = "0x1013E62D0")]
		public void SetCamera(Camera camera)
		{
		}

		// Token: 0x06004EF3 RID: 20211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004879")]
		[Address(RVA = "0x1013E6308", Offset = "0x13E6308", VA = "0x1013E6308")]
		public void SetRenderMode(RenderMode mode)
		{
		}

		// Token: 0x06004EF4 RID: 20212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487A")]
		[Address(RVA = "0x1013E6340", Offset = "0x13E6340", VA = "0x1013E6340")]
		private void LoadSprite(UIBackGroundManager.eBGResourceType bgResType, string fileName)
		{
		}

		// Token: 0x06004EF5 RID: 20213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487B")]
		[Address(RVA = "0x1013E642C", Offset = "0x13E642C", VA = "0x1013E642C")]
		public void Open(UIBackGroundManager.eBGResourceType bgResType, string filename, bool fade, UIDefine.eSortOrderTypeID sortorderID)
		{
		}

		// Token: 0x06004EF6 RID: 20214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487C")]
		[Address(RVA = "0x1013E6788", Offset = "0x13E6788", VA = "0x1013E6788")]
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortorderID, int offset)
		{
		}

		// Token: 0x06004EF7 RID: 20215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487D")]
		[Address(RVA = "0x1013E685C", Offset = "0x13E685C", VA = "0x1013E685C")]
		public void Close()
		{
		}

		// Token: 0x06004EF8 RID: 20216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487E")]
		[Address(RVA = "0x1013E687C", Offset = "0x13E687C", VA = "0x1013E687C")]
		public void SetAnchor(UIBackGroundHandler.eAnchor anchor)
		{
		}

		// Token: 0x06004EF9 RID: 20217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600487F")]
		[Address(RVA = "0x1013E6B00", Offset = "0x13E6B00", VA = "0x1013E6B00")]
		public void ApplySprite()
		{
		}

		// Token: 0x06004EFA RID: 20218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004880")]
		[Address(RVA = "0x1013E6B74", Offset = "0x13E6B74", VA = "0x1013E6B74")]
		private void Update()
		{
		}

		// Token: 0x06004EFB RID: 20219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004881")]
		[Address(RVA = "0x1013E663C", Offset = "0x13E663C", VA = "0x1013E663C")]
		public void Destroy()
		{
		}

		// Token: 0x06004EFC RID: 20220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004882")]
		[Address(RVA = "0x1013E75EC", Offset = "0x13E75EC", VA = "0x1013E75EC")]
		public void ToggleRotate()
		{
		}

		// Token: 0x06004EFD RID: 20221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004883")]
		[Address(RVA = "0x1013E7268", Offset = "0x13E7268", VA = "0x1013E7268")]
		public void RotateVertical(bool flg)
		{
		}

		// Token: 0x06004EFE RID: 20222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004884")]
		[Address(RVA = "0x1013E75FC", Offset = "0x13E75FC", VA = "0x1013E75FC")]
		public UIBackGroundHandler()
		{
		}

		// Token: 0x0400614D RID: 24909
		[Token(Token = "0x4004443")]
		private const float FADEIN_TIME = 0.2f;

		// Token: 0x0400614E RID: 24910
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004444")]
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x0400614F RID: 24911
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004445")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006150 RID: 24912
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004446")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04006151 RID: 24913
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004447")]
		private UIBackGroundManager.eBGResourceType m_BGResourceType;

		// Token: 0x04006152 RID: 24914
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004448")]
		private string m_FileName;

		// Token: 0x04006153 RID: 24915
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004449")]
		private UIBackGroundManager.eBackGroundOrderID m_BGOrderID;

		// Token: 0x04006154 RID: 24916
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x400444A")]
		private bool m_Fade;

		// Token: 0x04006155 RID: 24917
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400444B")]
		private GameObject m_GameObject;

		// Token: 0x04006156 RID: 24918
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400444C")]
		private bool m_IsActive;

		// Token: 0x04006157 RID: 24919
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400444D")]
		private UIBackGroundManager m_Owner;

		// Token: 0x04006158 RID: 24920
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400444E")]
		private UIBackGroundHandler.eStep m_Step;

		// Token: 0x04006159 RID: 24921
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400444F")]
		private Camera m_DefaultCamera;

		// Token: 0x0400615A RID: 24922
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004450")]
		private bool m_IsDonePrepare;

		// Token: 0x0400615B RID: 24923
		[Cpp2IlInjected.FieldOffset(Offset = "0x71")]
		[Token(Token = "0x4004451")]
		private bool m_IsRot;

		// Token: 0x0200101D RID: 4125
		[Token(Token = "0x2001225")]
		public enum eAnchor
		{
			// Token: 0x0400615E RID: 24926
			[Token(Token = "0x4007042")]
			Bottom,
			// Token: 0x0400615F RID: 24927
			[Token(Token = "0x4007043")]
			Middle,
			// Token: 0x04006160 RID: 24928
			[Token(Token = "0x4007044")]
			Top
		}

		// Token: 0x0200101E RID: 4126
		[Token(Token = "0x2001226")]
		private enum eStep
		{
			// Token: 0x04006162 RID: 24930
			[Token(Token = "0x4007046")]
			Hide,
			// Token: 0x04006163 RID: 24931
			[Token(Token = "0x4007047")]
			Load,
			// Token: 0x04006164 RID: 24932
			[Token(Token = "0x4007048")]
			FadeIn,
			// Token: 0x04006165 RID: 24933
			[Token(Token = "0x4007049")]
			Idle,
			// Token: 0x04006166 RID: 24934
			[Token(Token = "0x400704A")]
			FadeOut
		}
	}
}
