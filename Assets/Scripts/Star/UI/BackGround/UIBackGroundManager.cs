﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.BackGround
{
	// Token: 0x0200101F RID: 4127
	[Token(Token = "0x2000ABA")]
	[StructLayout(3)]
	public class UIBackGroundManager : MonoBehaviour
	{
		// Token: 0x06004EFF RID: 20223 RVA: 0x0001B9D8 File Offset: 0x00019BD8
		[Token(Token = "0x6004885")]
		[Address(RVA = "0x1013E7604", Offset = "0x13E7604", VA = "0x1013E7604")]
		public bool IsLoadingAny()
		{
			return default(bool);
		}

		// Token: 0x06004F00 RID: 20224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004886")]
		[Address(RVA = "0x1013E76D8", Offset = "0x13E76D8", VA = "0x1013E76D8")]
		public void Setup()
		{
		}

		// Token: 0x06004F01 RID: 20225 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004887")]
		[Address(RVA = "0x1013E77DC", Offset = "0x13E77DC", VA = "0x1013E77DC")]
		public UIBackGroundHandler GetHandler(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
			return null;
		}

		// Token: 0x06004F02 RID: 20226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004888")]
		[Address(RVA = "0x1013E782C", Offset = "0x13E782C", VA = "0x1013E782C")]
		public void Open(UIBackGroundManager.eBackGroundOrderID bgOrderID, UIBackGroundManager.eBackGroundID bgID, bool fade)
		{
		}

		// Token: 0x06004F03 RID: 20227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004889")]
		[Address(RVA = "0x1013E78F4", Offset = "0x13E78F4", VA = "0x1013E78F4")]
		public void Open(UIBackGroundManager.eBackGroundOrderID bgorderID, string filePath, bool fade)
		{
		}

		// Token: 0x06004F04 RID: 20228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488A")]
		[Address(RVA = "0x1013E7C0C", Offset = "0x13E7C0C", VA = "0x1013E7C0C")]
		public void Change(UIBackGroundManager.eBackGroundOrderID bgOrderID, UIBackGroundManager.eBackGroundID bgID, bool fade)
		{
		}

		// Token: 0x06004F05 RID: 20229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488B")]
		[Address(RVA = "0x1013E7904", Offset = "0x13E7904", VA = "0x1013E7904")]
		public void Change(UIBackGroundManager.eBackGroundOrderID bgorderID, UIBackGroundManager.eBGResourceType bgResType, string filePath, bool fade)
		{
		}

		// Token: 0x06004F06 RID: 20230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488C")]
		[Address(RVA = "0x1013E7CD4", Offset = "0x13E7CD4", VA = "0x1013E7CD4")]
		public void Close(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
		}

		// Token: 0x06004F07 RID: 20231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488D")]
		[Address(RVA = "0x1013E7E24", Offset = "0x13E7E24", VA = "0x1013E7E24")]
		public void Hide(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
		}

		// Token: 0x06004F08 RID: 20232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488E")]
		[Address(RVA = "0x1013E7F50", Offset = "0x13E7F50", VA = "0x1013E7F50")]
		public void Show(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
		}

		// Token: 0x06004F09 RID: 20233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600488F")]
		[Address(RVA = "0x1013E70C0", Offset = "0x13E70C0", VA = "0x1013E70C0")]
		public void Flush()
		{
		}

		// Token: 0x06004F0A RID: 20234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004890")]
		[Address(RVA = "0x1013E807C", Offset = "0x13E807C", VA = "0x1013E807C")]
		public void Clear()
		{
		}

		// Token: 0x06004F0B RID: 20235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004891")]
		[Address(RVA = "0x1013E7490", Offset = "0x13E7490", VA = "0x1013E7490")]
		public void Sink(UIBackGroundHandler handler)
		{
		}

		// Token: 0x06004F0C RID: 20236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004892")]
		[Address(RVA = "0x1013E8184", Offset = "0x13E8184", VA = "0x1013E8184")]
		public UIBackGroundManager()
		{
		}

		// Token: 0x04006167 RID: 24935
		[Token(Token = "0x4004453")]
		private const int BG_INST_NUM = 4;

		// Token: 0x04006168 RID: 24936
		[Token(Token = "0x4004454")]
		public static readonly string[] FileNames;

		// Token: 0x04006169 RID: 24937
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004455")]
		[SerializeField]
		private UIBackGroundHandler m_HandlerPrefab;

		// Token: 0x0400616A RID: 24938
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004456")]
		[SerializeField]
		private Camera m_UICamera;

		// Token: 0x0400616B RID: 24939
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004457")]
		private List<UIBackGroundHandler> m_EmptyHandlerList;

		// Token: 0x0400616C RID: 24940
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004458")]
		private List<UIBackGroundHandler> m_ActiveHandlerList;

		// Token: 0x0400616D RID: 24941
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004459")]
		private UIBackGroundHandler[] m_ActiveHandlers;

		// Token: 0x02001020 RID: 4128
		[Token(Token = "0x2001227")]
		public enum eBackGroundID
		{
			// Token: 0x0400616F RID: 24943
			[Token(Token = "0x400704C")]
			Common,
			// Token: 0x04006170 RID: 24944
			[Token(Token = "0x400704D")]
			Shop,
			// Token: 0x04006171 RID: 24945
			[Token(Token = "0x400704E")]
			Trade,
			// Token: 0x04006172 RID: 24946
			[Token(Token = "0x400704F")]
			Weapon,
			// Token: 0x04006173 RID: 24947
			[Token(Token = "0x4007050")]
			Build,
			// Token: 0x04006174 RID: 24948
			[Token(Token = "0x4007051")]
			Summon,
			// Token: 0x04006175 RID: 24949
			[Token(Token = "0x4007052")]
			Training,
			// Token: 0x04006176 RID: 24950
			[Token(Token = "0x4007053")]
			Quest,
			// Token: 0x04006177 RID: 24951
			[Token(Token = "0x4007054")]
			Menu
		}

		// Token: 0x02001021 RID: 4129
		[Token(Token = "0x2001228")]
		public enum eBackGroundOrderID
		{
			// Token: 0x04006179 RID: 24953
			[Token(Token = "0x4007056")]
			Low,
			// Token: 0x0400617A RID: 24954
			[Token(Token = "0x4007057")]
			OverlayUI,
			// Token: 0x0400617B RID: 24955
			[Token(Token = "0x4007058")]
			OverlayUI1,
			// Token: 0x0400617C RID: 24956
			[Token(Token = "0x4007059")]
			OverlayUI2,
			// Token: 0x0400617D RID: 24957
			[Token(Token = "0x400705A")]
			Num
		}

		// Token: 0x02001022 RID: 4130
		[Token(Token = "0x2001229")]
		public enum eBGResourceType
		{
			// Token: 0x0400617F RID: 24959
			[Token(Token = "0x400705C")]
			BackGround,
			// Token: 0x04006180 RID: 24960
			[Token(Token = "0x400705D")]
			ADVBackGround
		}
	}
}
