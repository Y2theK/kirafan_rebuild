﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class Badge : MonoBehaviour {
        public const int MAX_VALUE = 99;

        [SerializeField] private Text m_Text;

        public void Apply(int value) {
            string text = value.ToString();
            if (value < 1) {
                gameObject.SetActive(false);
            } else {
                gameObject.SetActive(true);
                if (value > MAX_VALUE) {
                    text = $"{MAX_VALUE}+";
                }
            }
            m_Text.text = text;
        }
    }
}
