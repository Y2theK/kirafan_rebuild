﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DD6 RID: 3542
	[Token(Token = "0x2000982")]
	[StructLayout(3)]
	public class BannerScrollItem : InfiniteScrollItem
	{
		// Token: 0x0600416D RID: 16749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFA")]
		[Address(RVA = "0x1013E86B4", Offset = "0x13E86B4", VA = "0x1013E86B4", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x0600416E RID: 16750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFB")]
		[Address(RVA = "0x1013E86B8", Offset = "0x13E86B8", VA = "0x1013E86B8")]
		private void Update()
		{
		}

		// Token: 0x0600416F RID: 16751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFC")]
		[Address(RVA = "0x1013E8854", Offset = "0x13E8854", VA = "0x1013E8854", Slot = "5")]
		public override void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06004170 RID: 16752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFD")]
		[Address(RVA = "0x1013E8A00", Offset = "0x13E8A00", VA = "0x1013E8A00", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004171 RID: 16753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFE")]
		[Address(RVA = "0x1013E8A40", Offset = "0x13E8A40", VA = "0x1013E8A40")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x06004172 RID: 16754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BFF")]
		[Address(RVA = "0x1013E8C70", Offset = "0x13E8C70", VA = "0x1013E8C70")]
		public BannerScrollItem()
		{
		}

		// Token: 0x04005107 RID: 20743
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003950")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04005108 RID: 20744
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003951")]
		[SerializeField]
		private Sprite m_TopSprite;
	}
}
