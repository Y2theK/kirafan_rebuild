﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DD7 RID: 3543
	[Token(Token = "0x2000983")]
	[StructLayout(3)]
	public class BannerScrollItemData : InfiniteScrollItemData
	{
		// Token: 0x06004173 RID: 16755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C00")]
		[Address(RVA = "0x1013E8C78", Offset = "0x13E8C78", VA = "0x1013E8C78")]
		public BannerScrollItemData(int infoID, Sprite sprite, string url)
		{
		}

		// Token: 0x06004174 RID: 16756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C01")]
		[Address(RVA = "0x1013E8CC4", Offset = "0x13E8CC4", VA = "0x1013E8CC4")]
		public void Destroy()
		{
		}

		// Token: 0x04005109 RID: 20745
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003952")]
		public int m_InfoID;

		// Token: 0x0400510A RID: 20746
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003953")]
		public Sprite m_Sprite;

		// Token: 0x0400510B RID: 20747
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003954")]
		public string m_Url;

		// Token: 0x0400510C RID: 20748
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003955")]
		public WebViewGroup m_WebViewGroup;
	}
}
