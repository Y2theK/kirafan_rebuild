﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010E5 RID: 4325
	[Token(Token = "0x2000B38")]
	[StructLayout(3)]
	public class BattleDescription : MonoBehaviour
	{
		// Token: 0x060053EF RID: 21487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D49")]
		[Address(RVA = "0x1013E8CD0", Offset = "0x13E8CD0", VA = "0x1013E8CD0")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x060053F0 RID: 21488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4A")]
		[Address(RVA = "0x1013E8D0C", Offset = "0x13E8D0C", VA = "0x1013E8D0C")]
		public void SetText(string text)
		{
		}

		// Token: 0x060053F1 RID: 21489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4B")]
		[Address(RVA = "0x1013E8DCC", Offset = "0x13E8DCC", VA = "0x1013E8DCC")]
		public BattleDescription()
		{
		}

		// Token: 0x0400662F RID: 26159
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047B2")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04006630 RID: 26160
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047B3")]
		[SerializeField]
		private ScrollText m_ScrollText;
	}
}
