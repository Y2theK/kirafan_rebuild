﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010E6 RID: 4326
	[Token(Token = "0x2000B39")]
	[StructLayout(3)]
	public class BattleElementChange : MonoBehaviour
	{
		// Token: 0x060053F2 RID: 21490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4C")]
		[Address(RVA = "0x1013E8DD4", Offset = "0x13E8DD4", VA = "0x1013E8DD4")]
		public void Apply(eElementType element)
		{
		}

		// Token: 0x060053F3 RID: 21491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4D")]
		[Address(RVA = "0x1013E8E10", Offset = "0x13E8E10", VA = "0x1013E8E10")]
		public void Play(eElementType before, eElementType after)
		{
		}

		// Token: 0x060053F4 RID: 21492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4E")]
		[Address(RVA = "0x1013E8F38", Offset = "0x13E8F38", VA = "0x1013E8F38")]
		public void ApplyAfter()
		{
		}

		// Token: 0x060053F5 RID: 21493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D4F")]
		[Address(RVA = "0x1013E8F70", Offset = "0x13E8F70", VA = "0x1013E8F70")]
		public BattleElementChange()
		{
		}

		// Token: 0x04006631 RID: 26161
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047B4")]
		[SerializeField]
		private ElementIcon m_Icon;

		// Token: 0x04006632 RID: 26162
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047B5")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x04006633 RID: 26163
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047B6")]
		private eElementType m_After;
	}
}
