﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010E7 RID: 4327
	[Token(Token = "0x2000B3A")]
	[StructLayout(3)]
	public class BattleFade : MonoBehaviour
	{
		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x060053F6 RID: 21494 RVA: 0x0001C740 File Offset: 0x0001A940
		[Token(Token = "0x1700053E")]
		public BattleFade.eState state
		{
			[Token(Token = "0x6004D50")]
			[Address(RVA = "0x1013E8F78", Offset = "0x13E8F78", VA = "0x1013E8F78")]
			get
			{
				return BattleFade.eState.Hide;
			}
		}

		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x060053F7 RID: 21495 RVA: 0x0001C758 File Offset: 0x0001A958
		[Token(Token = "0x1700053F")]
		public bool IsPlaying
		{
			[Token(Token = "0x6004D51")]
			[Address(RVA = "0x1013E8F80", Offset = "0x13E8F80", VA = "0x1013E8F80")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060053F8 RID: 21496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D52")]
		[Address(RVA = "0x1013E8F88", Offset = "0x13E8F88", VA = "0x1013E8F88")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x060053F9 RID: 21497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D53")]
		[Address(RVA = "0x1013E9058", Offset = "0x13E9058", VA = "0x1013E9058")]
		public void Show()
		{
		}

		// Token: 0x060053FA RID: 21498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D54")]
		[Address(RVA = "0x1013E948C", Offset = "0x13E948C", VA = "0x1013E948C")]
		public void Hide()
		{
		}

		// Token: 0x060053FB RID: 21499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D55")]
		[Address(RVA = "0x1013E99D0", Offset = "0x13E99D0", VA = "0x1013E99D0")]
		public void ShowQuick()
		{
		}

		// Token: 0x060053FC RID: 21500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D56")]
		[Address(RVA = "0x1013E9074", Offset = "0x13E9074", VA = "0x1013E9074")]
		private void PlayIn()
		{
		}

		// Token: 0x060053FD RID: 21501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D57")]
		[Address(RVA = "0x1013E9AAC", Offset = "0x13E9AAC", VA = "0x1013E9AAC")]
		public void OnCompleteInMove()
		{
		}

		// Token: 0x060053FE RID: 21502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D58")]
		[Address(RVA = "0x1013E94AC", Offset = "0x13E94AC", VA = "0x1013E94AC")]
		private void PlayOut()
		{
		}

		// Token: 0x060053FF RID: 21503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D59")]
		[Address(RVA = "0x1013E9AC8", Offset = "0x13E9AC8", VA = "0x1013E9AC8")]
		public void OnCompleteOutMove()
		{
		}

		// Token: 0x06005400 RID: 21504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D5A")]
		[Address(RVA = "0x1013E9B18", Offset = "0x13E9B18", VA = "0x1013E9B18")]
		public BattleFade()
		{
		}

		// Token: 0x04006634 RID: 26164
		[Token(Token = "0x40047B7")]
		private const float FADE_SEC = 0.2f;

		// Token: 0x04006635 RID: 26165
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047B8")]
		private BattleFade.eState m_State;

		// Token: 0x04006636 RID: 26166
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40047B9")]
		private bool m_IsEnable;

		// Token: 0x04006637 RID: 26167
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
		[Token(Token = "0x40047BA")]
		private bool m_IsPlaying;

		// Token: 0x04006638 RID: 26168
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047BB")]
		private RectTransform m_RectTransform;

		// Token: 0x04006639 RID: 26169
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047BC")]
		private GameObject m_GameObject;

		// Token: 0x0400663A RID: 26170
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047BD")]
		[SerializeField]
		private Image[] m_ColorChangeTargets;

		// Token: 0x0400663B RID: 26171
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047BE")]
		[SerializeField]
		private bool m_Vertical;

		// Token: 0x020010E8 RID: 4328
		[Token(Token = "0x200126E")]
		public enum eState
		{
			// Token: 0x0400663D RID: 26173
			[Token(Token = "0x40071B2")]
			Hide,
			// Token: 0x0400663E RID: 26174
			[Token(Token = "0x40071B3")]
			In,
			// Token: 0x0400663F RID: 26175
			[Token(Token = "0x40071B4")]
			Wait,
			// Token: 0x04006640 RID: 26176
			[Token(Token = "0x40071B5")]
			Out
		}
	}
}
