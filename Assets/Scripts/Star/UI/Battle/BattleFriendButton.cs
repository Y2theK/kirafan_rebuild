﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010E9 RID: 4329
	[Token(Token = "0x2000B3B")]
	[StructLayout(3)]
	public class BattleFriendButton : MonoBehaviour
	{
		// Token: 0x14000121 RID: 289
		// (add) Token: 0x06005401 RID: 21505 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005402 RID: 21506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000121")]
		public event Action OnClick
		{
			[Token(Token = "0x6004D5B")]
			[Address(RVA = "0x1013E9B28", Offset = "0x13E9B28", VA = "0x1013E9B28")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004D5C")]
			[Address(RVA = "0x1013E9C34", Offset = "0x13E9C34", VA = "0x1013E9C34")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06005403 RID: 21507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D5D")]
		[Address(RVA = "0x1013E9D40", Offset = "0x13E9D40", VA = "0x1013E9D40")]
		public void Setup(int charaID)
		{
		}

		// Token: 0x06005404 RID: 21508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D5E")]
		[Address(RVA = "0x1013E9F4C", Offset = "0x13E9F4C", VA = "0x1013E9F4C")]
		public void Destroy()
		{
		}

		// Token: 0x06005405 RID: 21509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D5F")]
		[Address(RVA = "0x1013E9DF0", Offset = "0x13E9DF0", VA = "0x1013E9DF0")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x06005406 RID: 21510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D60")]
		[Address(RVA = "0x1013E9F80", Offset = "0x13E9F80", VA = "0x1013E9F80")]
		public void SetFriendType(CharacterDefine.eFriendType friendType)
		{
		}

		// Token: 0x06005407 RID: 21511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D61")]
		[Address(RVA = "0x1013E9F90", Offset = "0x13E9F90", VA = "0x1013E9F90")]
		public void SetRemainTurn(int turn)
		{
		}

		// Token: 0x06005408 RID: 21512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D62")]
		[Address(RVA = "0x1013E9E40", Offset = "0x13E9E40", VA = "0x1013E9E40")]
		public void SetRemainTurnVisible(bool setVisible)
		{
		}

		// Token: 0x06005409 RID: 21513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D63")]
		[Address(RVA = "0x1013EA06C", Offset = "0x13EA06C", VA = "0x1013EA06C")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x0600540A RID: 21514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D64")]
		[Address(RVA = "0x1013EA0BC", Offset = "0x13EA0BC", VA = "0x1013EA0BC")]
		public BattleFriendButton()
		{
		}

		// Token: 0x04006641 RID: 26177
		[Token(Token = "0x40047BF")]
		private const float FLASH_TIME_SEC = 0.5f;

		// Token: 0x04006642 RID: 26178
		[Token(Token = "0x40047C0")]
		private const float BLINK_TIME_SEC = 0.5f;

		// Token: 0x04006643 RID: 26179
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047C1")]
		private int m_CharaID;

		// Token: 0x04006644 RID: 26180
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047C2")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x04006645 RID: 26181
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047C3")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04006647 RID: 26183
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047C5")]
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x04006648 RID: 26184
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40047C6")]
		[SerializeField]
		private RectTransform m_ImageInfinity;

		// Token: 0x04006649 RID: 26185
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40047C7")]
		[SerializeField]
		private RectTransform m_ImageLast1;

		// Token: 0x0400664A RID: 26186
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40047C8")]
		[SerializeField]
		private RectTransform m_RemainCanvas;

		// Token: 0x0400664B RID: 26187
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40047C9")]
		private bool m_isNPC;
	}
}
