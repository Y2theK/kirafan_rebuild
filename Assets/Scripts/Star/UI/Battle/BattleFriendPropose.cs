﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200113F RID: 4415
	[Token(Token = "0x2000B73")]
	[StructLayout(3)]
	public class BattleFriendPropose : UIGroup
	{
		// Token: 0x060056F3 RID: 22259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005043")]
		[Address(RVA = "0x1013EA0CC", Offset = "0x13EA0CC", VA = "0x1013EA0CC")]
		public void Open(FriendManager.FriendData friendData)
		{
		}

		// Token: 0x060056F4 RID: 22260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005044")]
		[Address(RVA = "0x1013EA168", Offset = "0x13EA168", VA = "0x1013EA168", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060056F5 RID: 22261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005045")]
		[Address(RVA = "0x1013EA198", Offset = "0x13EA198", VA = "0x1013EA198")]
		public void OnClickYesCallBack()
		{
		}

		// Token: 0x060056F6 RID: 22262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005046")]
		[Address(RVA = "0x1013EA270", Offset = "0x13EA270", VA = "0x1013EA270")]
		public void OnResponse(bool isErr)
		{
		}

		// Token: 0x060056F7 RID: 22263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005047")]
		[Address(RVA = "0x1013EA27C", Offset = "0x13EA27C", VA = "0x1013EA27C")]
		public BattleFriendPropose()
		{
		}

		// Token: 0x040069B2 RID: 27058
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004A9D")]
		[SerializeField]
		private MenuFriendListItem m_Item;

		// Token: 0x040069B3 RID: 27059
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004A9E")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x040069B4 RID: 27060
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004A9F")]
		private FriendManager.FriendData m_FriendData;
	}
}
