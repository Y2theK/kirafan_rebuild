﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010EA RID: 4330
	[Token(Token = "0x2000B3C")]
	[StructLayout(3)]
	public class BattleHelpIconDescript : MonoBehaviour
	{
		// Token: 0x0600540B RID: 21515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D65")]
		[Address(RVA = "0x1013EA284", Offset = "0x13EA284", VA = "0x1013EA284")]
		public void Setup(StateIconOwner owner)
		{
		}

		// Token: 0x0600540C RID: 21516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D66")]
		[Address(RVA = "0x1013EA434", Offset = "0x13EA434", VA = "0x1013EA434")]
		public void Apply(eStateIconType type)
		{
		}

		// Token: 0x0600540D RID: 21517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D67")]
		[Address(RVA = "0x1013EA640", Offset = "0x13EA640", VA = "0x1013EA640")]
		public BattleHelpIconDescript()
		{
		}

		// Token: 0x0400664C RID: 26188
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047CA")]
		[SerializeField]
		private RectTransform m_IconLocator;

		// Token: 0x0400664D RID: 26189
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047CB")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x0400664E RID: 26190
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047CC")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x0400664F RID: 26191
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047CD")]
		private StateIconOwner m_Owner;

		// Token: 0x04006650 RID: 26192
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047CE")]
		private StateIcon m_StateIcon;
	}
}
