﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010EB RID: 4331
	[Token(Token = "0x2000B3D")]
	[StructLayout(3)]
	public class BattleHelpIconDescriptTitle : MonoBehaviour
	{
		// Token: 0x0600540E RID: 21518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D68")]
		[Address(RVA = "0x1013EA648", Offset = "0x13EA648", VA = "0x1013EA648")]
		public void Apply(eStateIconCategory category)
		{
		}

		// Token: 0x0600540F RID: 21519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D69")]
		[Address(RVA = "0x1013EA6C0", Offset = "0x13EA6C0", VA = "0x1013EA6C0")]
		public BattleHelpIconDescriptTitle()
		{
		}

		// Token: 0x04006651 RID: 26193
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047CF")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006652 RID: 26194
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047D0")]
		[SerializeField]
		private Sprite m_SpriteAbnormal;

		// Token: 0x04006653 RID: 26195
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047D1")]
		[SerializeField]
		private Sprite m_SpriteBuffDebuff;

		// Token: 0x04006654 RID: 26196
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047D2")]
		[SerializeField]
		private Sprite m_SpriteOther;
	}
}
