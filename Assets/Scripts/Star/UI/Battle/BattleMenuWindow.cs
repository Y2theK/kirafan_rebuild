﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;
using Star.UI.Town;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010EC RID: 4332
	[Token(Token = "0x2000B3E")]
	[StructLayout(3)]
	public class BattleMenuWindow : MonoBehaviour
	{
		// Token: 0x14000122 RID: 290
		// (add) Token: 0x06005410 RID: 21520 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005411 RID: 21521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000122")]
		public event Action OnCancel
		{
			[Token(Token = "0x6004D6A")]
			[Address(RVA = "0x1013EA6C8", Offset = "0x13EA6C8", VA = "0x1013EA6C8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004D6B")]
			[Address(RVA = "0x1013EA7D4", Offset = "0x13EA7D4", VA = "0x1013EA7D4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000123 RID: 291
		// (add) Token: 0x06005412 RID: 21522 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005413 RID: 21523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000123")]
		public event Action OnRetire
		{
			[Token(Token = "0x6004D6C")]
			[Address(RVA = "0x1013EA8E0", Offset = "0x13EA8E0", VA = "0x1013EA8E0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004D6D")]
			[Address(RVA = "0x1013EA9EC", Offset = "0x13EA9EC", VA = "0x1013EA9EC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06005414 RID: 21524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D6E")]
		[Address(RVA = "0x1013EAAF8", Offset = "0x13EAAF8", VA = "0x1013EAAF8")]
		public void Setup(BattleOption battleOption)
		{
		}

		// Token: 0x06005415 RID: 21525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D6F")]
		[Address(RVA = "0x1013EACC4", Offset = "0x13EACC4", VA = "0x1013EACC4")]
		private void SetupHelpWindow()
		{
		}

		// Token: 0x06005416 RID: 21526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D70")]
		[Address(RVA = "0x1013EAFB8", Offset = "0x13EAFB8", VA = "0x1013EAFB8")]
		private void Update()
		{
		}

		// Token: 0x06005417 RID: 21527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D71")]
		[Address(RVA = "0x1013EB1CC", Offset = "0x13EB1CC", VA = "0x1013EB1CC")]
		private void SetState(BattleMenuWindow.eState state)
		{
		}

		// Token: 0x06005418 RID: 21528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D72")]
		[Address(RVA = "0x1013EB248", Offset = "0x13EB248", VA = "0x1013EB248")]
		public void PlayIn()
		{
		}

		// Token: 0x06005419 RID: 21529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D73")]
		[Address(RVA = "0x1013EB354", Offset = "0x13EB354", VA = "0x1013EB354")]
		public void PlayOut()
		{
		}

		// Token: 0x0600541A RID: 21530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D74")]
		[Address(RVA = "0x1013EB438", Offset = "0x13EB438", VA = "0x1013EB438")]
		public void OnClickCloseButtonCallBack()
		{
		}

		// Token: 0x0600541B RID: 21531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D75")]
		[Address(RVA = "0x1013EB444", Offset = "0x13EB444", VA = "0x1013EB444")]
		public void OnClickRetireButtonCallBack()
		{
		}

		// Token: 0x0600541C RID: 21532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D76")]
		[Address(RVA = "0x1013EB450", Offset = "0x13EB450", VA = "0x1013EB450")]
		public void OnClickOpenHelpButtonCallBack()
		{
		}

		// Token: 0x0600541D RID: 21533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D77")]
		[Address(RVA = "0x1013EB4E0", Offset = "0x13EB4E0", VA = "0x1013EB4E0")]
		public void OnClickCloseHelpButtonCallBack()
		{
		}

		// Token: 0x0600541E RID: 21534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D78")]
		[Address(RVA = "0x1013EB568", Offset = "0x13EB568", VA = "0x1013EB568")]
		public void OnClickOpenOptionButtonCallBack()
		{
		}

		// Token: 0x0600541F RID: 21535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D79")]
		[Address(RVA = "0x1013EB5F4", Offset = "0x13EB5F4", VA = "0x1013EB5F4")]
		public void OnCloseOptionGroupCallBack()
		{
		}

		// Token: 0x06005420 RID: 21536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D7A")]
		[Address(RVA = "0x1013EB65C", Offset = "0x13EB65C", VA = "0x1013EB65C")]
		public void OnClickOpenTownBuffButtonCallBack()
		{
		}

		// Token: 0x06005421 RID: 21537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D7B")]
		[Address(RVA = "0x1013EB6E4", Offset = "0x13EB6E4", VA = "0x1013EB6E4")]
		public void OnCloseTownBuffCallBack()
		{
		}

		// Token: 0x06005422 RID: 21538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D7C")]
		[Address(RVA = "0x1013EB74C", Offset = "0x13EB74C", VA = "0x1013EB74C")]
		public BattleMenuWindow()
		{
		}

		// Token: 0x04006655 RID: 26197
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047D3")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04006656 RID: 26198
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047D4")]
		[SerializeField]
		private AnimUIPlayer[] m_Anims;

		// Token: 0x04006657 RID: 26199
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047D5")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006658 RID: 26200
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047D6")]
		[SerializeField]
		private UIGroup m_HelpGroup;

		// Token: 0x04006659 RID: 26201
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047D7")]
		[SerializeField]
		private OptionWindow m_OptionGroup;

		// Token: 0x0400665A RID: 26202
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40047D8")]
		[SerializeField]
		private TownBufWindow m_BuffWindow;

		// Token: 0x0400665B RID: 26203
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40047D9")]
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x0400665C RID: 26204
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40047DA")]
		[SerializeField]
		private RectTransform m_IconDescriptRoot;

		// Token: 0x0400665D RID: 26205
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40047DB")]
		[SerializeField]
		private BattleHelpIconDescriptTitle m_IconDescriptTitlePrefab;

		// Token: 0x0400665E RID: 26206
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40047DC")]
		[SerializeField]
		private BattleHelpIconDescript m_IconDescriptPrefab;

		// Token: 0x0400665F RID: 26207
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40047DD")]
		private BattleMenuWindow.eButton m_Button;

		// Token: 0x04006660 RID: 26208
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40047DE")]
		private bool m_IsDoneHelpSetup;

		// Token: 0x04006663 RID: 26211
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40047E1")]
		private BattleOption m_BattleOption;

		// Token: 0x020010ED RID: 4333
		[Token(Token = "0x200126F")]
		private enum eState
		{
			// Token: 0x04006665 RID: 26213
			[Token(Token = "0x40071B7")]
			Hide,
			// Token: 0x04006666 RID: 26214
			[Token(Token = "0x40071B8")]
			In,
			// Token: 0x04006667 RID: 26215
			[Token(Token = "0x40071B9")]
			Wait,
			// Token: 0x04006668 RID: 26216
			[Token(Token = "0x40071BA")]
			Out,
			// Token: 0x04006669 RID: 26217
			[Token(Token = "0x40071BB")]
			Window,
			// Token: 0x0400666A RID: 26218
			[Token(Token = "0x40071BC")]
			Num
		}

		// Token: 0x020010EE RID: 4334
		[Token(Token = "0x2001270")]
		private enum eButton
		{
			// Token: 0x0400666C RID: 26220
			[Token(Token = "0x40071BE")]
			None,
			// Token: 0x0400666D RID: 26221
			[Token(Token = "0x40071BF")]
			Close,
			// Token: 0x0400666E RID: 26222
			[Token(Token = "0x40071C0")]
			Retire
		}
	}
}
