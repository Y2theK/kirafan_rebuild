﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010EF RID: 4335
	[Token(Token = "0x2000B3F")]
	[StructLayout(3)]
	public class BattleMessage : MonoBehaviour
	{
		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x06005423 RID: 21539 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000540")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004D7D")]
			[Address(RVA = "0x1013EB754", Offset = "0x13EB754", VA = "0x1013EB754")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06005424 RID: 21540 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000541")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004D7E")]
			[Address(RVA = "0x1013EB75C", Offset = "0x13EB75C", VA = "0x1013EB75C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x06005425 RID: 21541 RVA: 0x0001C770 File Offset: 0x0001A970
		[Token(Token = "0x17000542")]
		public int MessageNum
		{
			[Token(Token = "0x6004D7F")]
			[Address(RVA = "0x1013EB764", Offset = "0x13EB764", VA = "0x1013EB764")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x06005426 RID: 21542 RVA: 0x0001C788 File Offset: 0x0001A988
		[Token(Token = "0x17000543")]
		public bool IsDispMessage
		{
			[Token(Token = "0x6004D80")]
			[Address(RVA = "0x1013EB7C4", Offset = "0x13EB7C4", VA = "0x1013EB7C4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005427 RID: 21543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D81")]
		[Address(RVA = "0x1013EB830", Offset = "0x13EB830", VA = "0x1013EB830")]
		public void Setup()
		{
		}

		// Token: 0x06005428 RID: 21544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D82")]
		[Address(RVA = "0x1013EB8E8", Offset = "0x13EB8E8", VA = "0x1013EB8E8")]
		private void Update()
		{
		}

		// Token: 0x06005429 RID: 21545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D83")]
		[Address(RVA = "0x1013EBA58", Offset = "0x13EBA58", VA = "0x1013EBA58")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x0600542A RID: 21546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D84")]
		[Address(RVA = "0x1013EBA90", Offset = "0x13EBA90", VA = "0x1013EBA90")]
		public void SetMessage(string text, bool stack = false)
		{
		}

		// Token: 0x0600542B RID: 21547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D85")]
		[Address(RVA = "0x1013EBBC0", Offset = "0x13EBBC0", VA = "0x1013EBBC0")]
		public BattleMessage()
		{
		}

		// Token: 0x0400666F RID: 26223
		[Token(Token = "0x40047E2")]
		private const float DISPLAY_TIME_MAX = 2f;

		// Token: 0x04006670 RID: 26224
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047E3")]
		private List<string> m_MessageList;

		// Token: 0x04006671 RID: 26225
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40047E4")]
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04006672 RID: 26226
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047E5")]
		private float m_DispTime;

		// Token: 0x04006673 RID: 26227
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047E6")]
		private RectTransform m_RectTransform;

		// Token: 0x04006674 RID: 26228
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047E7")]
		private GameObject m_GameObject;
	}
}
