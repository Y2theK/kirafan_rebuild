﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010F0 RID: 4336
	[Token(Token = "0x2000B40")]
	[StructLayout(3)]
	public class BattleOrderFrame : BattleOrderFrameBase
	{
		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x0600542C RID: 21548 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600542D RID: 21549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000544")]
		public CharacterHandler CharaHandler
		{
			[Token(Token = "0x6004D86")]
			[Address(RVA = "0x1013EBBC8", Offset = "0x13EBBC8", VA = "0x1013EBBC8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6004D87")]
			[Address(RVA = "0x1013EBBD0", Offset = "0x13EBBD0", VA = "0x1013EBBD0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x0600542E RID: 21550 RVA: 0x0001C7A0 File Offset: 0x0001A9A0
		[Token(Token = "0x6004D88")]
		[Address(RVA = "0x1013EBBD8", Offset = "0x13EBBD8", VA = "0x1013EBBD8", Slot = "4")]
		public override bool IsChara()
		{
			return default(bool);
		}

		// Token: 0x0600542F RID: 21551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D89")]
		[Address(RVA = "0x1013EBBE0", Offset = "0x13EBBE0", VA = "0x1013EBBE0")]
		public void Setup(StateIconOwner stateIconOwner)
		{
		}

		// Token: 0x06005430 RID: 21552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8A")]
		[Address(RVA = "0x1013EBD30", Offset = "0x13EBD30", VA = "0x1013EBD30", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06005431 RID: 21553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8B")]
		[Address(RVA = "0x1013EBD68", Offset = "0x13EBD68", VA = "0x1013EBD68")]
		public void ApplyPL(int charaID)
		{
		}

		// Token: 0x06005432 RID: 21554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8C")]
		[Address(RVA = "0x1013EBE04", Offset = "0x13EBE04", VA = "0x1013EBE04")]
		public void ApplyEN(int resourceID)
		{
		}

		// Token: 0x06005433 RID: 21555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8D")]
		[Address(RVA = "0x1013EBCF8", Offset = "0x13EBCF8", VA = "0x1013EBCF8")]
		public void SetTargetCursor(bool flg)
		{
		}

		// Token: 0x06005434 RID: 21556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8E")]
		[Address(RVA = "0x1013EBE9C", Offset = "0x13EBE9C", VA = "0x1013EBE9C")]
		public void SetType(BattleDefine.eJoinMember member)
		{
		}

		// Token: 0x06005435 RID: 21557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D8F")]
		[Address(RVA = "0x1013EBF68", Offset = "0x13EBF68", VA = "0x1013EBF68")]
		public void SetStunIcon(bool flg)
		{
		}

		// Token: 0x06005436 RID: 21558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D90")]
		[Address(RVA = "0x1013EC064", Offset = "0x13EC064", VA = "0x1013EC064")]
		public void ClearAbnormalIcon()
		{
		}

		// Token: 0x06005437 RID: 21559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D91")]
		[Address(RVA = "0x1013EC0D8", Offset = "0x13EC0D8", VA = "0x1013EC0D8")]
		public void SetAbnormal(bool[] abnormal)
		{
		}

		// Token: 0x06005438 RID: 21560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D92")]
		[Address(RVA = "0x1013EC228", Offset = "0x13EC228", VA = "0x1013EC228")]
		public void SetInteractableFrame(bool flg)
		{
		}

		// Token: 0x06005439 RID: 21561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D93")]
		[Address(RVA = "0x1013EC2C4", Offset = "0x13EC2C4", VA = "0x1013EC2C4")]
		public BattleOrderFrame()
		{
		}

		// Token: 0x04006675 RID: 26229
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40047E8")]
		[SerializeField]
		private Sprite[] m_TypeSprite;

		// Token: 0x04006676 RID: 26230
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40047E9")]
		[SerializeField]
		private OrderIcon m_OrderIcon;

		// Token: 0x04006677 RID: 26231
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40047EA")]
		[SerializeField]
		private Image m_Type;

		// Token: 0x04006678 RID: 26232
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40047EB")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04006679 RID: 26233
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40047EC")]
		[SerializeField]
		private GameObject m_TargetObj;

		// Token: 0x0400667A RID: 26234
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40047ED")]
		[SerializeField]
		private GameObject m_DarkObj;

		// Token: 0x0400667B RID: 26235
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40047EE")]
		[SerializeField]
		private GameObject m_StunIconObj;

		// Token: 0x0400667C RID: 26236
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40047EF")]
		[SerializeField]
		private BuffIcon m_AbnormalIcon;

		// Token: 0x0400667D RID: 26237
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40047F0")]
		private bool m_IsAbnormal;

		// Token: 0x0400667E RID: 26238
		[Cpp2IlInjected.FieldOffset(Offset = "0x101")]
		[Token(Token = "0x40047F1")]
		private bool m_IsStun;
	}
}
