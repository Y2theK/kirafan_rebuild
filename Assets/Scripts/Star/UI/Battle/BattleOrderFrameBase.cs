﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010F1 RID: 4337
	[Token(Token = "0x2000B41")]
	[StructLayout(3)]
	public class BattleOrderFrameBase : MonoBehaviour
	{
		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x0600543A RID: 21562 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000545")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004D94")]
			[Address(RVA = "0x1013EC390", Offset = "0x13EC390", VA = "0x1013EC390")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x0600543B RID: 21563 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000546")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004D95")]
			[Address(RVA = "0x1013EC428", Offset = "0x13EC428", VA = "0x1013EC428")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x0600543C RID: 21564 RVA: 0x0001C7B8 File Offset: 0x0001A9B8
		[Token(Token = "0x17000547")]
		public float Alpha
		{
			[Token(Token = "0x6004D96")]
			[Address(RVA = "0x1013EC4C0", Offset = "0x13EC4C0", VA = "0x1013EC4C0")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x0600543E RID: 21566 RVA: 0x0001C7D0 File Offset: 0x0001A9D0
		// (set) Token: 0x0600543D RID: 21565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000548")]
		public float OrderValue
		{
			[Token(Token = "0x6004D98")]
			[Address(RVA = "0x1013EC4F8", Offset = "0x13EC4F8", VA = "0x1013EC4F8")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6004D97")]
			[Address(RVA = "0x1013EC4F0", Offset = "0x13EC4F0", VA = "0x1013EC4F0")]
			set
			{
			}
		}

		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x0600543F RID: 21567 RVA: 0x0001C7E8 File Offset: 0x0001A9E8
		[Token(Token = "0x17000549")]
		public bool IsChaseMove
		{
			[Token(Token = "0x6004D99")]
			[Address(RVA = "0x1013EC500", Offset = "0x13EC500", VA = "0x1013EC500")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06005440 RID: 21568 RVA: 0x0001C800 File Offset: 0x0001AA00
		// (set) Token: 0x06005441 RID: 21569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700054A")]
		public bool IsEnableJump
		{
			[Token(Token = "0x6004D9A")]
			[Address(RVA = "0x1013EC570", Offset = "0x13EC570", VA = "0x1013EC570")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004D9B")]
			[Address(RVA = "0x1013EC578", Offset = "0x13EC578", VA = "0x1013EC578")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06005442 RID: 21570 RVA: 0x0001C818 File Offset: 0x0001AA18
		[Token(Token = "0x6004D9C")]
		[Address(RVA = "0x1013EC580", Offset = "0x13EC580", VA = "0x1013EC580", Slot = "4")]
		public virtual bool IsChara()
		{
			return default(bool);
		}

		// Token: 0x06005443 RID: 21571 RVA: 0x0001C830 File Offset: 0x0001AA30
		[Token(Token = "0x6004D9D")]
		[Address(RVA = "0x1013EC588", Offset = "0x13EC588", VA = "0x1013EC588", Slot = "5")]
		public virtual bool IsCard()
		{
			return default(bool);
		}

		// Token: 0x06005444 RID: 21572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D9E")]
		[Address(RVA = "0x1013EBC50", Offset = "0x13EBC50", VA = "0x1013EBC50", Slot = "6")]
		public virtual void Setup()
		{
		}

		// Token: 0x06005445 RID: 21573 RVA: 0x0001C848 File Offset: 0x0001AA48
		[Token(Token = "0x6004D9F")]
		[Address(RVA = "0x1013EC590", Offset = "0x13EC590", VA = "0x1013EC590")]
		public float GetGoalPos()
		{
			return 0f;
		}

		// Token: 0x06005446 RID: 21574 RVA: 0x0001C860 File Offset: 0x0001AA60
		[Token(Token = "0x6004DA0")]
		[Address(RVA = "0x1013EC5D4", Offset = "0x13EC5D4", VA = "0x1013EC5D4")]
		public Vector2 GetNextPos(float sec)
		{
			return default(Vector2);
		}

		// Token: 0x06005447 RID: 21575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA1")]
		[Address(RVA = "0x1013EC728", Offset = "0x13EC728", VA = "0x1013EC728")]
		public void MoverCollision(float sec, out float left, out float right)
		{
		}

		// Token: 0x06005448 RID: 21576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA2")]
		[Address(RVA = "0x1013EBD64", Offset = "0x13EBD64", VA = "0x1013EBD64", Slot = "7")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06005449 RID: 21577 RVA: 0x0001C878 File Offset: 0x0001AA78
		[Token(Token = "0x6004DA3")]
		[Address(RVA = "0x1013EC830", Offset = "0x13EC830", VA = "0x1013EC830")]
		public bool IsPlayer()
		{
			return default(bool);
		}

		// Token: 0x0600544A RID: 21578 RVA: 0x0001C890 File Offset: 0x0001AA90
		[Token(Token = "0x6004DA4")]
		[Address(RVA = "0x1013EC838", Offset = "0x13EC838", VA = "0x1013EC838")]
		private bool UpdateMoveX()
		{
			return default(bool);
		}

		// Token: 0x0600544B RID: 21579 RVA: 0x0001C8A8 File Offset: 0x0001AAA8
		[Token(Token = "0x6004DA5")]
		[Address(RVA = "0x1013EC9A4", Offset = "0x13EC9A4", VA = "0x1013EC9A4")]
		private bool IsMoveEnd()
		{
			return default(bool);
		}

		// Token: 0x0600544C RID: 21580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA6")]
		[Address(RVA = "0x1013ECA6C", Offset = "0x13ECA6C", VA = "0x1013ECA6C")]
		private void UpdateWithMoveParam()
		{
		}

		// Token: 0x0600544D RID: 21581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA7")]
		[Address(RVA = "0x1013ECCB8", Offset = "0x13ECCB8", VA = "0x1013ECCB8", Slot = "8")]
		public virtual void Update()
		{
		}

		// Token: 0x0600544E RID: 21582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA8")]
		[Address(RVA = "0x1013ECCBC", Offset = "0x13ECCBC", VA = "0x1013ECCBC", Slot = "9")]
		public virtual void LateUpdate()
		{
		}

		// Token: 0x0600544F RID: 21583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DA9")]
		[Address(RVA = "0x1013ECF60", Offset = "0x13ECF60", VA = "0x1013ECF60")]
		public void SetCurrent(bool flg)
		{
		}

		// Token: 0x06005450 RID: 21584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAA")]
		[Address(RVA = "0x1013ECFD8", Offset = "0x13ECFD8", VA = "0x1013ECFD8")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x06005451 RID: 21585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAB")]
		[Address(RVA = "0x1013ED010", Offset = "0x13ED010", VA = "0x1013ED010")]
		public void SetPos(Vector3 position)
		{
		}

		// Token: 0x06005452 RID: 21586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAC")]
		[Address(RVA = "0x1013ED0C8", Offset = "0x13ED0C8", VA = "0x1013ED0C8")]
		public void SetMoveType(BattleOrderFrameBase.eMoveType moveType)
		{
		}

		// Token: 0x06005453 RID: 21587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAD")]
		[Address(RVA = "0x1013ED08C", Offset = "0x13ED08C", VA = "0x1013ED08C")]
		public void StopMove()
		{
		}

		// Token: 0x06005454 RID: 21588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAE")]
		[Address(RVA = "0x1013ED0D0", Offset = "0x13ED0D0", VA = "0x1013ED0D0")]
		public void LaneMove(float goalPosX, float speed)
		{
		}

		// Token: 0x06005455 RID: 21589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DAF")]
		[Address(RVA = "0x1013ED1E8", Offset = "0x13ED1E8", VA = "0x1013ED1E8")]
		public void FreeWaitMove(float goalPosX, float speed)
		{
		}

		// Token: 0x06005456 RID: 21590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB0")]
		[Address(RVA = "0x1013ED254", Offset = "0x13ED254", VA = "0x1013ED254")]
		public void FreeMove(float goalPosX, float speed)
		{
		}

		// Token: 0x06005457 RID: 21591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB1")]
		[Address(RVA = "0x1013ED138", Offset = "0x13ED138", VA = "0x1013ED138")]
		public void MoveToConstSpeed(float goalPosX, float speed, bool detour)
		{
		}

		// Token: 0x06005458 RID: 21592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB2")]
		[Address(RVA = "0x1013ED2FC", Offset = "0x13ED2FC", VA = "0x1013ED2FC")]
		public void MoveToKeepSpeed(float goalPosX, float speed = 0f)
		{
		}

		// Token: 0x06005459 RID: 21593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB3")]
		[Address(RVA = "0x1013ED3CC", Offset = "0x13ED3CC", VA = "0x1013ED3CC")]
		public void ChaseTo(RectTransform chaseTarget, float speed)
		{
		}

		// Token: 0x0600545A RID: 21594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB4")]
		[Address(RVA = "0x1013ED4A8", Offset = "0x13ED4A8", VA = "0x1013ED4A8")]
		public void Detour()
		{
		}

		// Token: 0x0600545B RID: 21595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB5")]
		[Address(RVA = "0x1013ECF30", Offset = "0x13ECF30", VA = "0x1013ECF30")]
		private void MoveWithLaneChange(bool detourMode, bool isFree = false)
		{
		}

		// Token: 0x0600545C RID: 21596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB6")]
		[Address(RVA = "0x1013ED4CC", Offset = "0x13ED4CC", VA = "0x1013ED4CC")]
		public void SetBlink(bool flg)
		{
		}

		// Token: 0x0600545D RID: 21597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB7")]
		[Address(RVA = "0x1013ED504", Offset = "0x13ED504", VA = "0x1013ED504")]
		public void Fade(float value, float duration)
		{
		}

		// Token: 0x0600545E RID: 21598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB8")]
		[Address(RVA = "0x1013ED83C", Offset = "0x13ED83C", VA = "0x1013ED83C")]
		public void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x0600545F RID: 21599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DB9")]
		[Address(RVA = "0x1013ED894", Offset = "0x13ED894", VA = "0x1013ED894")]
		public void OnCompleteAlpha()
		{
		}

		// Token: 0x06005460 RID: 21600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DBA")]
		[Address(RVA = "0x1013ED89C", Offset = "0x13ED89C", VA = "0x1013ED89C")]
		public void SetAlpha(float value)
		{
		}

		// Token: 0x06005461 RID: 21601 RVA: 0x0001C8C0 File Offset: 0x0001AAC0
		[Token(Token = "0x6004DBB")]
		[Address(RVA = "0x1013ED964", Offset = "0x13ED964", VA = "0x1013ED964")]
		public bool isMove()
		{
			return default(bool);
		}

		// Token: 0x06005462 RID: 21602 RVA: 0x0001C8D8 File Offset: 0x0001AAD8
		[Token(Token = "0x6004DBC")]
		[Address(RVA = "0x1013ED998", Offset = "0x13ED998", VA = "0x1013ED998")]
		public BattleOrderFrameBase.eMoveType GetMoveType()
		{
			return BattleOrderFrameBase.eMoveType.Lane;
		}

		// Token: 0x06005463 RID: 21603 RVA: 0x0001C8F0 File Offset: 0x0001AAF0
		[Token(Token = "0x6004DBD")]
		[Address(RVA = "0x1013ED9A0", Offset = "0x13ED9A0", VA = "0x1013ED9A0")]
		public float GetMoveDistance()
		{
			return 0f;
		}

		// Token: 0x06005464 RID: 21604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DBE")]
		[Address(RVA = "0x1013EC2C8", Offset = "0x13EC2C8", VA = "0x1013EC2C8")]
		public BattleOrderFrameBase()
		{
		}

		// Token: 0x04006680 RID: 26240
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40047F3")]
		protected readonly Color PLAYER_COLOR;

		// Token: 0x04006681 RID: 26241
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40047F4")]
		protected readonly Color ENEMY_COLOR;

		// Token: 0x04006682 RID: 26242
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40047F5")]
		protected readonly Color CURRENT_COLOR;

		// Token: 0x04006683 RID: 26243
		[Token(Token = "0x40047F6")]
		protected const float DETOUR_UP_Y = 56f;

		// Token: 0x04006684 RID: 26244
		[Token(Token = "0x40047F7")]
		protected const float DETOUR_DOWN_Y = 0f;

		// Token: 0x04006685 RID: 26245
		[Token(Token = "0x40047F8")]
		protected const float DETOUR_LANEMOVE_SPEED = 256f;

		// Token: 0x04006686 RID: 26246
		[Token(Token = "0x40047F9")]
		protected const float DETOUR_FREEUP_SPEED = 2048f;

		// Token: 0x04006687 RID: 26247
		[Token(Token = "0x40047FA")]
		protected const float DETOUR_FREEDOWN_SPEED = 256f;

		// Token: 0x04006688 RID: 26248
		[Token(Token = "0x40047FB")]
		protected const float JUMPHEIGHT = 38f;

		// Token: 0x04006689 RID: 26249
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40047FC")]
		[SerializeField]
		protected ColorGroup m_BaseColorGroup;

		// Token: 0x0400668A RID: 26250
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40047FD")]
		[SerializeField]
		protected BlinkCanvasGroup m_Blink;

		// Token: 0x0400668B RID: 26251
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40047FE")]
		[SerializeField]
		protected GameObject m_JumpUpObj;

		// Token: 0x0400668C RID: 26252
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40047FF")]
		protected bool m_IsPlayer;

		// Token: 0x0400668D RID: 26253
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004800")]
		protected BattleOrderFrameBase.eMoveType m_MoveType;

		// Token: 0x0400668E RID: 26254
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004801")]
		protected BattleOrderFrameBase.eDetourState m_DetourState;

		// Token: 0x0400668F RID: 26255
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4004802")]
		protected bool m_IsFreeDetour;

		// Token: 0x04006690 RID: 26256
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004803")]
		protected RectTransform m_ChaseTarget;

		// Token: 0x04006691 RID: 26257
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004804")]
		protected float m_ChaseSpeed;

		// Token: 0x04006692 RID: 26258
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004805")]
		protected BattleOrderFrameBase.MoveParam m_MoveParam;

		// Token: 0x04006693 RID: 26259
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004806")]
		protected BattleOrderFrameBase.eJumpState m_JumpState;

		// Token: 0x04006694 RID: 26260
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4004807")]
		protected float m_JumpDuration;

		// Token: 0x04006695 RID: 26261
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004808")]
		protected bool m_IsFade;

		// Token: 0x04006696 RID: 26262
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004809")]
		protected CanvasGroup m_CanvasGroup;

		// Token: 0x04006697 RID: 26263
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400480A")]
		protected float m_OrderValue;

		// Token: 0x04006698 RID: 26264
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400480B")]
		protected RectTransform m_RectTransform;

		// Token: 0x04006699 RID: 26265
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400480C")]
		protected GameObject m_GameObject;

		// Token: 0x020010F2 RID: 4338
		[Token(Token = "0x2001271")]
		public enum eMoveType
		{
			// Token: 0x0400669C RID: 26268
			[Token(Token = "0x40071C2")]
			Lane,
			// Token: 0x0400669D RID: 26269
			[Token(Token = "0x40071C3")]
			Free,
			// Token: 0x0400669E RID: 26270
			[Token(Token = "0x40071C4")]
			FreeStop
		}

		// Token: 0x020010F3 RID: 4339
		[Token(Token = "0x2001272")]
		public enum eDetourState
		{
			// Token: 0x040066A0 RID: 26272
			[Token(Token = "0x40071C6")]
			None,
			// Token: 0x040066A1 RID: 26273
			[Token(Token = "0x40071C7")]
			Down,
			// Token: 0x040066A2 RID: 26274
			[Token(Token = "0x40071C8")]
			Up,
			// Token: 0x040066A3 RID: 26275
			[Token(Token = "0x40071C9")]
			Detour
		}

		// Token: 0x020010F4 RID: 4340
		[Token(Token = "0x2001273")]
		protected class MoveParam
		{
			// Token: 0x06005465 RID: 21605 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006356")]
			[Address(RVA = "0x1013ED2C0", Offset = "0x13ED2C0", VA = "0x1013ED2C0")]
			public MoveParam(float goal, float speed)
			{
			}

			// Token: 0x040066A4 RID: 26276
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40071CA")]
			public float m_Goal;

			// Token: 0x040066A5 RID: 26277
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40071CB")]
			public float m_Speed;
		}

		// Token: 0x020010F5 RID: 4341
		[Token(Token = "0x2001274")]
		public enum eJumpState
		{
			// Token: 0x040066A7 RID: 26279
			[Token(Token = "0x40071CD")]
			None,
			// Token: 0x040066A8 RID: 26280
			[Token(Token = "0x40071CE")]
			Up,
			// Token: 0x040066A9 RID: 26281
			[Token(Token = "0x40071CF")]
			Down
		}
	}
}
