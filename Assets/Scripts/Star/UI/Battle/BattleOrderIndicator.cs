﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010F6 RID: 4342
	[Token(Token = "0x2000B42")]
	[StructLayout(3)]
	public class BattleOrderIndicator : MonoBehaviour
	{
		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x06005466 RID: 21606 RVA: 0x0001C908 File Offset: 0x0001AB08
		[Token(Token = "0x1700054B")]
		public int SelectingIndex
		{
			[Token(Token = "0x6004DBF")]
			[Address(RVA = "0x1013ED9F4", Offset = "0x13ED9F4", VA = "0x1013ED9F4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06005467 RID: 21607 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004DC0")]
		[Address(RVA = "0x1013ED9FC", Offset = "0x13ED9FC", VA = "0x1013ED9FC")]
		private BattleOrderFrame CreateCharaFrame()
		{
			return null;
		}

		// Token: 0x06005468 RID: 21608 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004DC1")]
		[Address(RVA = "0x1013EDB48", Offset = "0x13EDB48", VA = "0x1013EDB48")]
		private BattleOrderSkillFrame CreateSkillFrame()
		{
			return null;
		}

		// Token: 0x06005469 RID: 21609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC2")]
		[Address(RVA = "0x1013EDC60", Offset = "0x13EDC60", VA = "0x1013EDC60")]
		public void Setup(StateIconOwner stateIconOwner)
		{
		}

		// Token: 0x0600546A RID: 21610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC3")]
		[Address(RVA = "0x1013EE210", Offset = "0x13EE210", VA = "0x1013EE210")]
		public void Destroy()
		{
		}

		// Token: 0x0600546B RID: 21611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC4")]
		[Address(RVA = "0x1013EE2DC", Offset = "0x13EE2DC", VA = "0x1013EE2DC")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x0600546C RID: 21612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC5")]
		[Address(RVA = "0x1013EE318", Offset = "0x13EE318", VA = "0x1013EE318")]
		public void SetTargetCursor(BattleDefine.ePartyType party, BattleDefine.eJoinMember join, bool flg)
		{
		}

		// Token: 0x0600546D RID: 21613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC6")]
		[Address(RVA = "0x1013EE4B4", Offset = "0x13EE4B4", VA = "0x1013EE4B4")]
		private void Update()
		{
		}

		// Token: 0x0600546E RID: 21614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC7")]
		[Address(RVA = "0x1013EEBF0", Offset = "0x13EEBF0", VA = "0x1013EEBF0")]
		private void LateUpdate()
		{
		}

		// Token: 0x0600546F RID: 21615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC8")]
		[Address(RVA = "0x1013EF484", Offset = "0x13EF484", VA = "0x1013EF484")]
		private void ApplyTogetherFrameState(int idx, RectTransform parent)
		{
		}

		// Token: 0x06005470 RID: 21616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DC9")]
		[Address(RVA = "0x1013EF674", Offset = "0x13EF674", VA = "0x1013EF674")]
		public void ClearTogetherFrame()
		{
		}

		// Token: 0x06005471 RID: 21617 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004DCA")]
		[Address(RVA = "0x1013EF75C", Offset = "0x13EF75C", VA = "0x1013EF75C")]
		public BattleOrderFrame GetCharaFrame(CharacterHandler charaHandler)
		{
			return null;
		}

		// Token: 0x06005472 RID: 21618 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004DCB")]
		[Address(RVA = "0x1013EFA6C", Offset = "0x13EFA6C", VA = "0x1013EFA6C")]
		public BattleOrderSkillFrame GetSkillFrame(BattleOrder.CardArguments cardArg)
		{
			return null;
		}

		// Token: 0x06005473 RID: 21619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DCC")]
		[Address(RVA = "0x1013EFCE4", Offset = "0x13EFCE4", VA = "0x1013EFCE4")]
		private void RemoveInactiveChara(BattleOrder order)
		{
		}

		// Token: 0x06005474 RID: 21620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DCD")]
		[Address(RVA = "0x1013F0028", Offset = "0x13F0028", VA = "0x1013F0028")]
		private void RemoveInactiveSkill(BattleOrder order)
		{
		}

		// Token: 0x06005475 RID: 21621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DCE")]
		[Address(RVA = "0x1013F0338", Offset = "0x13F0338", VA = "0x1013F0338")]
		public void SetFrames(BattleOrder battleOrder, int selectingIndex, BattleOrderIndicator.eMoveType moveType)
		{
		}

		// Token: 0x06005476 RID: 21622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DCF")]
		[Address(RVA = "0x1013F1C34", Offset = "0x13F1C34", VA = "0x1013F1C34")]
		public void SlideFrames(BattleOrder battleOrder)
		{
		}

		// Token: 0x06005477 RID: 21623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD0")]
		[Address(RVA = "0x1013F1D4C", Offset = "0x13F1D4C", VA = "0x1013F1D4C")]
		public void SlideStunFrames(BattleOrder battleOrder)
		{
		}

		// Token: 0x06005478 RID: 21624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD1")]
		[Address(RVA = "0x1013EE934", Offset = "0x13EE934", VA = "0x1013EE934")]
		public void MoveOwnerFrameToSelectFrame()
		{
		}

		// Token: 0x06005479 RID: 21625 RVA: 0x0001C920 File Offset: 0x0001AB20
		[Token(Token = "0x6004DD2")]
		[Address(RVA = "0x1013EEBB8", Offset = "0x13EEBB8", VA = "0x1013EEBB8")]
		public bool IsSlide()
		{
			return default(bool);
		}

		// Token: 0x0600547A RID: 21626 RVA: 0x0001C938 File Offset: 0x0001AB38
		[Token(Token = "0x6004DD3")]
		[Address(RVA = "0x1013F1D78", Offset = "0x13F1D78", VA = "0x1013F1D78")]
		private bool IsSlideWithOutFirstFrameMove()
		{
			return default(bool);
		}

		// Token: 0x0600547B RID: 21627 RVA: 0x0001C950 File Offset: 0x0001AB50
		[Token(Token = "0x6004DD4")]
		[Address(RVA = "0x1013F1E9C", Offset = "0x13F1E9C", VA = "0x1013F1E9C")]
		public bool IsPopup()
		{
			return default(bool);
		}

		// Token: 0x0600547C RID: 21628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD5")]
		[Address(RVA = "0x1013EF334", Offset = "0x13EF334", VA = "0x1013EF334")]
		private void SetCursorPos(float x)
		{
		}

		// Token: 0x0600547D RID: 21629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD6")]
		[Address(RVA = "0x1013EE198", Offset = "0x13EE198", VA = "0x1013EE198")]
		private void SetCursorEnableRender(bool flg)
		{
		}

		// Token: 0x0600547E RID: 21630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD7")]
		[Address(RVA = "0x1013F19C8", Offset = "0x13F19C8", VA = "0x1013F19C8")]
		private void PlayTurnEffect()
		{
		}

		// Token: 0x0600547F RID: 21631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DD8")]
		[Address(RVA = "0x1013F2164", Offset = "0x13F2164", VA = "0x1013F2164")]
		public BattleOrderIndicator()
		{
		}

		// Token: 0x040066AA RID: 26282
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400480E")]
		private readonly Color ENEMYCOLOR;

		// Token: 0x040066AB RID: 26283
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400480F")]
		private readonly Color PLAYERCOLOR;

		// Token: 0x040066AC RID: 26284
		[Token(Token = "0x4004810")]
		private const int SKILL_FRAME_INST_DEFAULT = 4;

		// Token: 0x040066AD RID: 26285
		[Token(Token = "0x4004811")]
		private const float FRAME_FIRSTSELECT_SPEED = 1600f;

		// Token: 0x040066AE RID: 26286
		[Token(Token = "0x4004812")]
		private const float FRAME_SELECT_SPEED = 800f;

		// Token: 0x040066AF RID: 26287
		[Token(Token = "0x4004813")]
		private const float FRAME_SLIDE_SPEED = 800f;

		// Token: 0x040066B0 RID: 26288
		[Token(Token = "0x4004814")]
		private const float FRAME_MOVE_SPEED = 2000f;

		// Token: 0x040066B1 RID: 26289
		[Token(Token = "0x4004815")]
		private const float FRAME_FADE_TIME = 0.2f;

		// Token: 0x040066B2 RID: 26290
		[Token(Token = "0x4004816")]
		private const float ARROW_HEAD_ANIM_SPEED = 2f;

		// Token: 0x040066B3 RID: 26291
		[Token(Token = "0x4004817")]
		private const float ARROW_HEAD_ANIM_RADIUS = 2f;

		// Token: 0x040066B4 RID: 26292
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004818")]
		[SerializeField]
		private BattleOrderFrame m_FramePrefab;

		// Token: 0x040066B5 RID: 26293
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004819")]
		[SerializeField]
		private BattleOrderSkillFrame m_FrameSkillPrefab;

		// Token: 0x040066B6 RID: 26294
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400481A")]
		[SerializeField]
		private RectTransform m_FrameRoot;

		// Token: 0x040066B7 RID: 26295
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400481B")]
		[SerializeField]
		private RectTransform m_Arrow;

		// Token: 0x040066B8 RID: 26296
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400481C")]
		[SerializeField]
		private RectTransform m_ArrowHead;

		// Token: 0x040066B9 RID: 26297
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400481D")]
		[SerializeField]
		private RectTransform m_TogetherLine;

		// Token: 0x040066BA RID: 26298
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400481E")]
		private GameObject m_TogetherLineObj;

		// Token: 0x040066BB RID: 26299
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400481F")]
		[SerializeField]
		private RectTransform m_TogetherFramePrefab;

		// Token: 0x040066BC RID: 26300
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004820")]
		private RectTransform[] m_TogetherFrames;

		// Token: 0x040066BD RID: 26301
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004821")]
		private GameObject[] m_TogetherFrameObjs;

		// Token: 0x040066BE RID: 26302
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004822")]
		[SerializeField]
		private RectTransform m_TurnEffect;

		// Token: 0x040066BF RID: 26303
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004823")]
		[SerializeField]
		private GearParent m_GearParent;

		// Token: 0x040066C0 RID: 26304
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004824")]
		[SerializeField]
		private GameObject m_GoObj;

		// Token: 0x040066C1 RID: 26305
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004825")]
		[SerializeField]
		private RectTransform m_ScaleTarget;

		// Token: 0x040066C2 RID: 26306
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004826")]
		private List<BattleOrderFrame> m_EmptyCharaFrameInsts;

		// Token: 0x040066C3 RID: 26307
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004827")]
		private List<BattleOrderFrame> m_ActiveCharaFrameInsts;

		// Token: 0x040066C4 RID: 26308
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004828")]
		private List<BattleOrderSkillFrame> m_EmptySkillFrameInsts;

		// Token: 0x040066C5 RID: 26309
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004829")]
		private List<BattleOrderSkillFrame> m_ActiveSkillFrameInsts;

		// Token: 0x040066C6 RID: 26310
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400482A")]
		private List<BattleOrderFrameBase> m_Frames;

		// Token: 0x040066C7 RID: 26311
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400482B")]
		private BattleOrderFrameBase m_FirstFrame;

		// Token: 0x040066C8 RID: 26312
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400482C")]
		private BattleOrderFrameBase m_SelectFrame;

		// Token: 0x040066C9 RID: 26313
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400482D")]
		private BattleOrderFrameBase m_SelectFrameChara;

		// Token: 0x040066CA RID: 26314
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400482E")]
		private BattleOrderFrameBase m_SelectFrameCard;

		// Token: 0x040066CB RID: 26315
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400482F")]
		private BattleOrderFrameBase m_ActionFrame;

		// Token: 0x040066CC RID: 26316
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004830")]
		private float m_FrameWidth;

		// Token: 0x040066CD RID: 26317
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x4004831")]
		private float m_SkillFrameWidth;

		// Token: 0x040066CE RID: 26318
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004832")]
		private float m_FrameSpacePerOrderValue;

		// Token: 0x040066CF RID: 26319
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4004833")]
		private int m_SelectingIndex;

		// Token: 0x040066D0 RID: 26320
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004834")]
		private bool m_IsSelecting;

		// Token: 0x040066D1 RID: 26321
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004835")]
		private List<BattleOrderFrameBase> m_TogetherFrameList;

		// Token: 0x040066D2 RID: 26322
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004836")]
		private bool m_IsReserveMoveFirstFrame;

		// Token: 0x040066D3 RID: 26323
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004837")]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x040066D4 RID: 26324
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004838")]
		private bool m_EnableArrow;

		// Token: 0x040066D5 RID: 26325
		[Cpp2IlInjected.FieldOffset(Offset = "0x129")]
		[Token(Token = "0x4004839")]
		private bool m_EnoughArrowLength;

		// Token: 0x040066D6 RID: 26326
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x400483A")]
		private float m_ArrowHeadAnimCount;

		// Token: 0x040066D7 RID: 26327
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x400483B")]
		private float m_FinalScl;

		// Token: 0x020010F7 RID: 4343
		[Token(Token = "0x2001275")]
		public enum eMoveType
		{
			// Token: 0x040066D9 RID: 26329
			[Token(Token = "0x40071D1")]
			Immediate,
			// Token: 0x040066DA RID: 26330
			[Token(Token = "0x40071D2")]
			Selecting,
			// Token: 0x040066DB RID: 26331
			[Token(Token = "0x40071D3")]
			Slide,
			// Token: 0x040066DC RID: 26332
			[Token(Token = "0x40071D4")]
			Stun
		}
	}
}
