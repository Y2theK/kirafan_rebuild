﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010F8 RID: 4344
	[Token(Token = "0x2000B43")]
	[StructLayout(3)]
	public class BattleOrderSkillFrame : BattleOrderFrameBase
	{
		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x06005480 RID: 21632 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06005481 RID: 21633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700054C")]
		public BattleOrder.CardArguments CardArg
		{
			[Token(Token = "0x6004DD9")]
			[Address(RVA = "0x1013EFC8C", Offset = "0x13EFC8C", VA = "0x1013EFC8C")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6004DDA")]
			[Address(RVA = "0x1013EFC94", Offset = "0x13EFC94", VA = "0x1013EFC94")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06005482 RID: 21634 RVA: 0x0001C968 File Offset: 0x0001AB68
		[Token(Token = "0x6004DDB")]
		[Address(RVA = "0x1013F2318", Offset = "0x13F2318", VA = "0x1013F2318", Slot = "5")]
		public override bool IsCard()
		{
			return default(bool);
		}

		// Token: 0x06005483 RID: 21635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DDC")]
		[Address(RVA = "0x1013F2320", Offset = "0x13F2320", VA = "0x1013F2320", Slot = "6")]
		public override void Setup()
		{
		}

		// Token: 0x06005484 RID: 21636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DDD")]
		[Address(RVA = "0x1013F235C", Offset = "0x13F235C", VA = "0x1013F235C", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06005485 RID: 21637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DDE")]
		[Address(RVA = "0x1013F1708", Offset = "0x13F1708", VA = "0x1013F1708")]
		public void ApplySkill(int skillID, BattleOrder.FrameData frameData)
		{
		}

		// Token: 0x06005486 RID: 21638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DDF")]
		[Address(RVA = "0x1013F1968", Offset = "0x13F1968", VA = "0x1013F1968")]
		public void SetAliveNumCount(int count)
		{
		}

		// Token: 0x06005487 RID: 21639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE0")]
		[Address(RVA = "0x1013EFC9C", Offset = "0x13EFC9C", VA = "0x1013EFC9C")]
		public void Popup()
		{
		}

		// Token: 0x06005488 RID: 21640 RVA: 0x0001C980 File Offset: 0x0001AB80
		[Token(Token = "0x6004DE1")]
		[Address(RVA = "0x1013F2108", Offset = "0x13F2108", VA = "0x1013F2108")]
		public bool IsPopup()
		{
			return default(bool);
		}

		// Token: 0x06005489 RID: 21641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE2")]
		[Address(RVA = "0x1013F16DC", Offset = "0x13F16DC", VA = "0x1013F16DC")]
		public void SkipPopup()
		{
		}

		// Token: 0x0600548A RID: 21642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE3")]
		[Address(RVA = "0x1013F2360", Offset = "0x13F2360", VA = "0x1013F2360")]
		public BattleOrderSkillFrame()
		{
		}

		// Token: 0x040066DD RID: 26333
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400483C")]
		[SerializeField]
		private SkillIcon m_SkillIcon;

		// Token: 0x040066DE RID: 26334
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400483D")]
		[SerializeField]
		private Text m_AliveNum;

		// Token: 0x040066DF RID: 26335
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400483E")]
		[SerializeField]
		private AnimUIPlayer m_PopAnim;

		// Token: 0x040066E0 RID: 26336
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400483F")]
		[SerializeField]
		private AnimUIPlayer m_PopEffectAnim;
	}
}
