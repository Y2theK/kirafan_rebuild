﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001140 RID: 4416
	[Token(Token = "0x2000B74")]
	[StructLayout(3)]
	public class BattleResultUI : MenuUIBase
	{
		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x060056F8 RID: 22264 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700056E")]
		public StoreReviewController StoreReviewController
		{
			[Token(Token = "0x6005048")]
			[Address(RVA = "0x1013F2364", Offset = "0x13F2364", VA = "0x1013F2364")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x060056F9 RID: 22265 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700056F")]
		public BattleFriendPropose ProposeGroup
		{
			[Token(Token = "0x6005049")]
			[Address(RVA = "0x1013F236C", Offset = "0x13F236C", VA = "0x1013F236C")]
			get
			{
				return null;
			}
		}

		// Token: 0x060056FA RID: 22266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504A")]
		[Address(RVA = "0x1013F2374", Offset = "0x13F2374", VA = "0x1013F2374")]
		public void SetReorderParam(bool isFirstClear, bool isContinuous, Action onClickReorder, int itemID, int useNum)
		{
		}

		// Token: 0x060056FB RID: 22267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504B")]
		[Address(RVA = "0x1013F244C", Offset = "0x13F244C", VA = "0x1013F244C")]
		public void Setup(BattleResult result, int orbID, Camera renderCamera, float planeDistance, BattleState_Result stateResult)
		{
		}

		// Token: 0x060056FC RID: 22268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504C")]
		[Address(RVA = "0x1013F3304", Offset = "0x13F3304", VA = "0x1013F3304")]
		private void Update()
		{
		}

		// Token: 0x060056FD RID: 22269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504D")]
		[Address(RVA = "0x1013F38B4", Offset = "0x13F38B4", VA = "0x1013F38B4")]
		private void LateUpdate()
		{
		}

		// Token: 0x060056FE RID: 22270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504E")]
		[Address(RVA = "0x1013F3734", Offset = "0x13F3734", VA = "0x1013F3734")]
		private void SetState(int state)
		{
		}

		// Token: 0x060056FF RID: 22271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600504F")]
		[Address(RVA = "0x1013F3928", Offset = "0x13F3928", VA = "0x1013F3928", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06005700 RID: 22272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005050")]
		[Address(RVA = "0x1013F3A40", Offset = "0x13F3A40", VA = "0x1013F3A40", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06005701 RID: 22273 RVA: 0x0001CE48 File Offset: 0x0001B048
		[Token(Token = "0x6005051")]
		[Address(RVA = "0x1013F3BA4", Offset = "0x13F3BA4", VA = "0x1013F3BA4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06005702 RID: 22274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005052")]
		[Address(RVA = "0x1013F3BB4", Offset = "0x13F3BB4", VA = "0x1013F3BB4")]
		public void OnTapNext()
		{
		}

		// Token: 0x06005703 RID: 22275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005053")]
		[Address(RVA = "0x1013F3FCC", Offset = "0x13F3FCC", VA = "0x1013F3FCC")]
		private void FinishPlayerReward()
		{
		}

		// Token: 0x06005704 RID: 22276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005054")]
		[Address(RVA = "0x1013F40A8", Offset = "0x13F40A8", VA = "0x1013F40A8")]
		private void FinishedFunc_OnOfferComp()
		{
		}

		// Token: 0x06005705 RID: 22277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005055")]
		[Address(RVA = "0x1013F4198", Offset = "0x13F4198", VA = "0x1013F4198")]
		private void FinishedFunc_ShowTapNext(bool b)
		{
		}

		// Token: 0x06005706 RID: 22278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005056")]
		[Address(RVA = "0x1013F4284", Offset = "0x13F4284", VA = "0x1013F4284")]
		public void OnClickNextButtonCallBack()
		{
		}

		// Token: 0x06005707 RID: 22279 RVA: 0x0001CE60 File Offset: 0x0001B060
		[Token(Token = "0x6005057")]
		[Address(RVA = "0x1013F42A0", Offset = "0x13F42A0", VA = "0x1013F42A0")]
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			return default(Vector3);
		}

		// Token: 0x06005708 RID: 22280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005058")]
		[Address(RVA = "0x1013F381C", Offset = "0x13F381C", VA = "0x1013F381C")]
		public void SetCameraPosition(Vector2 pos)
		{
		}

		// Token: 0x06005709 RID: 22281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005059")]
		[Address(RVA = "0x1013F43E0", Offset = "0x13F43E0", VA = "0x1013F43E0")]
		public void OnClickReorderButtonCallBack()
		{
		}

		// Token: 0x0600570A RID: 22282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600505A")]
		[Address(RVA = "0x1013F43EC", Offset = "0x13F43EC", VA = "0x1013F43EC")]
		public BattleResultUI()
		{
		}

		// Token: 0x040069B5 RID: 27061
		[Token(Token = "0x4004AA0")]
		public const int STATE_HIDE = 0;

		// Token: 0x040069B6 RID: 27062
		[Token(Token = "0x4004AA1")]
		public const int STATE_IN = 1;

		// Token: 0x040069B7 RID: 27063
		[Token(Token = "0x4004AA2")]
		public const int STATE_WAIT = 2;

		// Token: 0x040069B8 RID: 27064
		[Token(Token = "0x4004AA3")]
		public const int STATE_OUT = 3;

		// Token: 0x040069B9 RID: 27065
		[Token(Token = "0x4004AA4")]
		public const int STATE_END = 4;

		// Token: 0x040069BA RID: 27066
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004AA5")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040069BB RID: 27067
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004AA6")]
		[SerializeField]
		private ResultPlayerReward m_PlayerRewardUI;

		// Token: 0x040069BC RID: 27068
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004AA7")]
		[SerializeField]
		private ResultCharaReward m_CharaRewardUI;

		// Token: 0x040069BD RID: 27069
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004AA8")]
		[SerializeField]
		private ResultOfferPtReward m_OfferPtRewardUI;

		// Token: 0x040069BE RID: 27070
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004AA9")]
		[SerializeField]
		private ResultItemReward m_ItemRewardUI;

		// Token: 0x040069BF RID: 27071
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004AAA")]
		[SerializeField]
		private Image m_TouchNext;

		// Token: 0x040069C0 RID: 27072
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004AAB")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040069C1 RID: 27073
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004AAC")]
		[SerializeField]
		private StoreReviewController m_StoreReviewController;

		// Token: 0x040069C2 RID: 27074
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004AAD")]
		[SerializeField]
		private BattleFriendPropose m_ProposeGroup;

		// Token: 0x040069C3 RID: 27075
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004AAE")]
		[SerializeField]
		private CustomButton m_ReorderButton;

		// Token: 0x040069C4 RID: 27076
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004AAF")]
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x040069C5 RID: 27077
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004AB0")]
		private BattleResultUI.eSequence m_Sequence;

		// Token: 0x040069C6 RID: 27078
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004AB1")]
		private BattleResult m_BattleResult;

		// Token: 0x040069C7 RID: 27079
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004AB2")]
		private BattleState_Result m_StateResult;

		// Token: 0x040069C8 RID: 27080
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004AB3")]
		private Camera m_Camera;

		// Token: 0x040069C9 RID: 27081
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004AB4")]
		private Transform m_CameraTransform;

		// Token: 0x040069CA RID: 27082
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004AB5")]
		private Canvas m_Canvas;

		// Token: 0x040069CB RID: 27083
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004AB6")]
		private bool m_IsShowChara;

		// Token: 0x040069CC RID: 27084
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004AB7")]
		private Action m_OnClickReorder;

		// Token: 0x040069CD RID: 27085
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004AB8")]
		private bool m_IsSkipOfferPt;

		// Token: 0x040069CE RID: 27086
		[Cpp2IlInjected.FieldOffset(Offset = "0xD9")]
		[Token(Token = "0x4004AB9")]
		private bool m_IsGotRewards;

		// Token: 0x02001141 RID: 4417
		[Token(Token = "0x200128D")]
		private enum eSequence
		{
			// Token: 0x040069D0 RID: 27088
			[Token(Token = "0x400724A")]
			PlayerReward,
			// Token: 0x040069D1 RID: 27089
			[Token(Token = "0x400724B")]
			CharaReward,
			// Token: 0x040069D2 RID: 27090
			[Token(Token = "0x400724C")]
			OfferPtReward,
			// Token: 0x040069D3 RID: 27091
			[Token(Token = "0x400724D")]
			ItemReward,
			// Token: 0x040069D4 RID: 27092
			[Token(Token = "0x400724E")]
			End
		}
	}
}
