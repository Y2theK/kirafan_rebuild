﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010F9 RID: 4345
	[Token(Token = "0x2000B44")]
	[StructLayout(3)]
	public class BattleReward : MonoBehaviour
	{
		// Token: 0x0600548B RID: 21643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE4")]
		[Address(RVA = "0x1013F43FC", Offset = "0x13F43FC", VA = "0x1013F43FC")]
		public void SetRewardNum(int rank, int value, bool isAnim = false)
		{
		}

		// Token: 0x0600548C RID: 21644 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004DE5")]
		[Address(RVA = "0x1013F44FC", Offset = "0x13F44FC", VA = "0x1013F44FC")]
		public RectTransform GetTreasureBoxRectTransform()
		{
			return null;
		}

		// Token: 0x0600548D RID: 21645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE6")]
		[Address(RVA = "0x1013F4504", Offset = "0x13F4504", VA = "0x1013F4504")]
		public BattleReward()
		{
		}

		// Token: 0x040066E2 RID: 26338
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004841")]
		[SerializeField]
		private Image m_TreasureBoxImage;

		// Token: 0x040066E3 RID: 26339
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004842")]
		[SerializeField]
		private RectTransform m_TreasureBox;

		// Token: 0x040066E4 RID: 26340
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004843")]
		[SerializeField]
		private Text m_NumTextObj;

		// Token: 0x040066E5 RID: 26341
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004844")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x040066E6 RID: 26342
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004845")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130E94", Offset = "0x130E94")]
		private Sprite[] m_TreasureSprites;

		// Token: 0x040066E7 RID: 26343
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004846")]
		private int m_Rank;
	}
}
