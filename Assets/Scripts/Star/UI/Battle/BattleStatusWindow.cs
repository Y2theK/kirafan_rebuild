﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010FA RID: 4346
	[Token(Token = "0x2000B45")]
	[StructLayout(3)]
	public class BattleStatusWindow : UIGroup
	{
		// Token: 0x0600548E RID: 21646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE7")]
		[Address(RVA = "0x1013F450C", Offset = "0x13F450C", VA = "0x1013F450C")]
		public void Setup()
		{
		}

		// Token: 0x0600548F RID: 21647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE8")]
		[Address(RVA = "0x1013F45F4", Offset = "0x13F45F4", VA = "0x1013F45F4")]
		public void Open(CharacterHandler charaHndl, bool isEnemy)
		{
		}

		// Token: 0x06005490 RID: 21648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DE9")]
		[Address(RVA = "0x1013F4AD8", Offset = "0x13F4AD8", VA = "0x1013F4AD8", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06005491 RID: 21649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DEA")]
		[Address(RVA = "0x1013F461C", Offset = "0x13F461C", VA = "0x1013F461C")]
		public void Apply(CharacterHandler charaHndl, bool isEnemy)
		{
		}

		// Token: 0x06005492 RID: 21650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DEB")]
		[Address(RVA = "0x1013F4D28", Offset = "0x13F4D28", VA = "0x1013F4D28")]
		private void ApplyStatus(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06005493 RID: 21651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DEC")]
		[Address(RVA = "0x1013F5734", Offset = "0x13F5734", VA = "0x1013F5734")]
		private void AddStatusChange(eStateIconType type)
		{
		}

		// Token: 0x06005494 RID: 21652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DED")]
		[Address(RVA = "0x1013F59DC", Offset = "0x13F59DC", VA = "0x1013F59DC")]
		private void AddExSkill(BattlePassiveSkill.Source source)
		{
		}

		// Token: 0x06005495 RID: 21653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DEE")]
		[Address(RVA = "0x1013F64D4", Offset = "0x13F64D4", VA = "0x1013F64D4")]
		public BattleStatusWindow()
		{
		}

		// Token: 0x040066E8 RID: 26344
		[Token(Token = "0x4004847")]
		private const float CHARASTATUS_PLAYER_POS_Y = -16f;

		// Token: 0x040066E9 RID: 26345
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004848")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130EE0", Offset = "0x130EE0")]
		private RectTransform m_Shift;

		// Token: 0x040066EA RID: 26346
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004849")]
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x040066EB RID: 26347
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400484A")]
		[SerializeField]
		private PassiveIconOwner m_PassiveIconOwner;

		// Token: 0x040066EC RID: 26348
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400484B")]
		[SerializeField]
		private PassiveIconOwner m_AbilityIconOwner;

		// Token: 0x040066ED RID: 26349
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400484C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130F5C", Offset = "0x130F5C")]
		private GameObject m_StatusChangeTitlePrefab;

		// Token: 0x040066EE RID: 26350
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400484D")]
		private GameObject m_StatusChangeTitleInst;

		// Token: 0x040066EF RID: 26351
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400484E")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130FA8", Offset = "0x130FA8")]
		private GameObject m_ExSkillTitlePrefab;

		// Token: 0x040066F0 RID: 26352
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400484F")]
		private GameObject m_ExSkillTitleInst;

		// Token: 0x040066F1 RID: 26353
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004850")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130FF4", Offset = "0x130FF4")]
		private GameObject m_EmptyPrefab;

		// Token: 0x040066F2 RID: 26354
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004851")]
		private GameObject m_EmptyInst;

		// Token: 0x040066F3 RID: 26355
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004852")]
		[SerializeField]
		private BattleStatusWindowStatusChange m_StatusChangePrefab;

		// Token: 0x040066F4 RID: 26356
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004853")]
		private List<BattleStatusWindowStatusChange> m_StatusChangeInstList;

		// Token: 0x040066F5 RID: 26357
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004854")]
		[SerializeField]
		private BattleStatusWindowStatusChange m_ExSkillPrefab;

		// Token: 0x040066F6 RID: 26358
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004855")]
		private List<BattleStatusWindowStatusChange> m_ExSkillInstList;

		// Token: 0x040066F7 RID: 26359
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004856")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040066F8 RID: 26360
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004857")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131070", Offset = "0x131070")]
		private CharacterStatus m_StatusUI;

		// Token: 0x040066F9 RID: 26361
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004858")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001310BC", Offset = "0x1310BC")]
		private FixScrollRect m_Scroll;

		// Token: 0x040066FA RID: 26362
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004859")]
		private Vector2 m_DefaultPosition;

		// Token: 0x040066FB RID: 26363
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x400485A")]
		private Vector2 m_CharaStatusDefaultPosition;
	}
}
