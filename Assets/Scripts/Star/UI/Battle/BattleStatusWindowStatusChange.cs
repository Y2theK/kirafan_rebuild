﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010FB RID: 4347
	[Token(Token = "0x2000B46")]
	[StructLayout(3)]
	public class BattleStatusWindowStatusChange : MonoBehaviour
	{
		// Token: 0x06005496 RID: 21654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DEF")]
		[Address(RVA = "0x1013F4C0C", Offset = "0x13F4C0C", VA = "0x1013F4C0C")]
		public void Clear()
		{
		}

		// Token: 0x06005497 RID: 21655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF0")]
		[Address(RVA = "0x1013F5CAC", Offset = "0x13F5CAC", VA = "0x1013F5CAC")]
		public void Apply(eStateIconType stateIconType, StateIconOwner stateIconOwner)
		{
		}

		// Token: 0x06005498 RID: 21656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF1")]
		[Address(RVA = "0x1013F60AC", Offset = "0x13F60AC", VA = "0x1013F60AC")]
		public void ApplyPassive(BattlePassiveSkill.Source source, PassiveIconOwner passiveIconOwner)
		{
		}

		// Token: 0x06005499 RID: 21657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF2")]
		[Address(RVA = "0x1013F65A4", Offset = "0x13F65A4", VA = "0x1013F65A4")]
		public void ApplyAbilityForStatusUI(AbilityDefine.AbilityPassiveSet abilityPassiveSet, GameObject iconPrefab)
		{
		}

		// Token: 0x0600549A RID: 21658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF3")]
		[Address(RVA = "0x1013F68CC", Offset = "0x13F68CC", VA = "0x1013F68CC")]
		public BattleStatusWindowStatusChange()
		{
		}

		// Token: 0x040066FC RID: 26364
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400485B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131108", Offset = "0x131108")]
		private RectTransform m_IconParent;

		// Token: 0x040066FD RID: 26365
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400485C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131154", Offset = "0x131154")]
		private Text m_NameText;

		// Token: 0x040066FE RID: 26366
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400485D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001311A0", Offset = "0x1311A0")]
		private Text m_DescriptText;

		// Token: 0x040066FF RID: 26367
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400485E")]
		private StateIcon m_StateIcon;

		// Token: 0x04006700 RID: 26368
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400485F")]
		private GameObject m_PassiveIcon;

		// Token: 0x04006701 RID: 26369
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004860")]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04006702 RID: 26370
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004861")]
		private PassiveIconOwner m_PassiveIconOwner;
	}
}
