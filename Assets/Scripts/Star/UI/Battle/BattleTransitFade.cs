﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020010FC RID: 4348
	[Token(Token = "0x2000B47")]
	[StructLayout(3)]
	public class BattleTransitFade : MonoBehaviour
	{
		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x0600549B RID: 21659 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700054D")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004DF4")]
			[Address(RVA = "0x1013F68D4", Offset = "0x13F68D4", VA = "0x1013F68D4")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600549C RID: 21660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF5")]
		[Address(RVA = "0x1013F6964", Offset = "0x13F6964", VA = "0x1013F6964")]
		private void SetColor(Color color)
		{
		}

		// Token: 0x0600549D RID: 21661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF6")]
		[Address(RVA = "0x1013F69C0", Offset = "0x13F69C0", VA = "0x1013F69C0")]
		public void PlayInQuick(Color color, int waveIndex, int waveMax)
		{
		}

		// Token: 0x0600549E RID: 21662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF7")]
		[Address(RVA = "0x1013F6AAC", Offset = "0x13F6AAC", VA = "0x1013F6AAC")]
		public void PlayInQuick(int waveIndex, int waveMax)
		{
		}

		// Token: 0x0600549F RID: 21663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF8")]
		[Address(RVA = "0x1013F6AEC", Offset = "0x13F6AEC", VA = "0x1013F6AEC")]
		public void PlayIn(Color color, int waveIndex, int waveMax)
		{
		}

		// Token: 0x060054A0 RID: 21664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DF9")]
		[Address(RVA = "0x1013F6BF8", Offset = "0x13F6BF8", VA = "0x1013F6BF8")]
		public void PlayIn(int waveIndex, int waveMax)
		{
		}

		// Token: 0x060054A1 RID: 21665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DFA")]
		[Address(RVA = "0x1013F6C38", Offset = "0x13F6C38", VA = "0x1013F6C38")]
		public void PlayIn(Color color)
		{
		}

		// Token: 0x060054A2 RID: 21666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DFB")]
		[Address(RVA = "0x1013F6CF8", Offset = "0x13F6CF8", VA = "0x1013F6CF8")]
		public void PlayIn()
		{
		}

		// Token: 0x060054A3 RID: 21667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DFC")]
		[Address(RVA = "0x1013F6D20", Offset = "0x13F6D20", VA = "0x1013F6D20")]
		public void PlayInQuick(Color color)
		{
		}

		// Token: 0x060054A4 RID: 21668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DFD")]
		[Address(RVA = "0x1013F6DA8", Offset = "0x13F6DA8", VA = "0x1013F6DA8")]
		public void PlayInQuick()
		{
		}

		// Token: 0x060054A5 RID: 21669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004DFE")]
		[Address(RVA = "0x1013F6DD0", Offset = "0x13F6DD0", VA = "0x1013F6DD0")]
		public void PlayOut()
		{
		}

		// Token: 0x060054A6 RID: 21670 RVA: 0x0001C998 File Offset: 0x0001AB98
		[Token(Token = "0x6004DFF")]
		[Address(RVA = "0x1013F6E68", Offset = "0x13F6E68", VA = "0x1013F6E68")]
		public bool IsShowComplete()
		{
			return default(bool);
		}

		// Token: 0x060054A7 RID: 21671 RVA: 0x0001C9B0 File Offset: 0x0001ABB0
		[Token(Token = "0x6004E00")]
		[Address(RVA = "0x1013F6E9C", Offset = "0x13F6E9C", VA = "0x1013F6E9C")]
		public bool IsHideComplete()
		{
			return default(bool);
		}

		// Token: 0x060054A8 RID: 21672 RVA: 0x0001C9C8 File Offset: 0x0001ABC8
		[Token(Token = "0x6004E01")]
		[Address(RVA = "0x1013F6ED0", Offset = "0x13F6ED0", VA = "0x1013F6ED0")]
		public bool IsEnableRender()
		{
			return default(bool);
		}

		// Token: 0x060054A9 RID: 21673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E02")]
		[Address(RVA = "0x1013F6F04", Offset = "0x13F6F04", VA = "0x1013F6F04")]
		public BattleTransitFade()
		{
		}

		// Token: 0x04006703 RID: 26371
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004862")]
		[SerializeField]
		private BattleFade m_Fade;

		// Token: 0x04006704 RID: 26372
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004863")]
		[SerializeField]
		private WaveTitle m_Title;

		// Token: 0x04006705 RID: 26373
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004864")]
		private GameObject m_GameObject;
	}
}
