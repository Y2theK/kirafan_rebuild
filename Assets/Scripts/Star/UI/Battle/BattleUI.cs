﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020010FD RID: 4349
	[Token(Token = "0x2000B48")]
	[StructLayout(3)]
	public class BattleUI : MenuUIBase
	{
		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x060054AA RID: 21674 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700054E")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004E03")]
			[Address(RVA = "0x1013F6F0C", Offset = "0x13F6F0C", VA = "0x1013F6F0C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x060054AB RID: 21675 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700054F")]
		public BattleSystem BattleSystem
		{
			[Token(Token = "0x6004E04")]
			[Address(RVA = "0x1013F6F14", Offset = "0x13F6F14", VA = "0x1013F6F14")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x060054AC RID: 21676 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000550")]
		public BattleTutorial BattleTutorial
		{
			[Token(Token = "0x6004E05")]
			[Address(RVA = "0x1013F6F1C", Offset = "0x13F6F1C", VA = "0x1013F6F1C")]
			get
			{
				return null;
			}
		}

		// Token: 0x14000124 RID: 292
		// (add) Token: 0x060054AD RID: 21677 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054AE RID: 21678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000124")]
		public event Action OnClickMenuButton
		{
			[Token(Token = "0x6004E06")]
			[Address(RVA = "0x1013F6F24", Offset = "0x13F6F24", VA = "0x1013F6F24")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E07")]
			[Address(RVA = "0x1013F7034", Offset = "0x13F7034", VA = "0x1013F7034")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000125 RID: 293
		// (add) Token: 0x060054AF RID: 21679 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054B0 RID: 21680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000125")]
		public event Action OnClickAutoButton
		{
			[Token(Token = "0x6004E08")]
			[Address(RVA = "0x1013F7144", Offset = "0x13F7144", VA = "0x1013F7144")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E09")]
			[Address(RVA = "0x1013F7254", Offset = "0x13F7254", VA = "0x1013F7254")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000126 RID: 294
		// (add) Token: 0x060054B1 RID: 21681 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054B2 RID: 21682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000126")]
		public event Action OnClickSpeedButton
		{
			[Token(Token = "0x6004E0A")]
			[Address(RVA = "0x1013F7364", Offset = "0x13F7364", VA = "0x1013F7364")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E0B")]
			[Address(RVA = "0x1013F7474", Offset = "0x13F7474", VA = "0x1013F7474")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000127 RID: 295
		// (add) Token: 0x060054B3 RID: 21683 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054B4 RID: 21684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000127")]
		public event Action OnClickFriendButton
		{
			[Token(Token = "0x6004E0C")]
			[Address(RVA = "0x1013F7584", Offset = "0x13F7584", VA = "0x1013F7584")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E0D")]
			[Address(RVA = "0x1013F7694", Offset = "0x13F7694", VA = "0x1013F7694")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000128 RID: 296
		// (add) Token: 0x060054B5 RID: 21685 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054B6 RID: 21686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000128")]
		public event Action OnCancelBattleMenuWindow
		{
			[Token(Token = "0x6004E0E")]
			[Address(RVA = "0x1013F77A4", Offset = "0x13F77A4", VA = "0x1013F77A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E0F")]
			[Address(RVA = "0x1013F78B4", Offset = "0x13F78B4", VA = "0x1013F78B4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000129 RID: 297
		// (add) Token: 0x060054B7 RID: 21687 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054B8 RID: 21688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000129")]
		public event Action OnClickRetireButton
		{
			[Token(Token = "0x6004E10")]
			[Address(RVA = "0x1013F79C4", Offset = "0x13F79C4", VA = "0x1013F79C4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E11")]
			[Address(RVA = "0x1013F7AD4", Offset = "0x13F7AD4", VA = "0x1013F7AD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012A RID: 298
		// (add) Token: 0x060054B9 RID: 21689 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054BA RID: 21690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012A")]
		public event Action OnMemberChange
		{
			[Token(Token = "0x6004E12")]
			[Address(RVA = "0x1013F7BE4", Offset = "0x13F7BE4", VA = "0x1013F7BE4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E13")]
			[Address(RVA = "0x1013F7CF4", Offset = "0x13F7CF4", VA = "0x1013F7CF4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012B RID: 299
		// (add) Token: 0x060054BB RID: 21691 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054BC RID: 21692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012B")]
		public event Action<int> OnSelectMemberChange
		{
			[Token(Token = "0x6004E14")]
			[Address(RVA = "0x1013F7E04", Offset = "0x13F7E04", VA = "0x1013F7E04")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E15")]
			[Address(RVA = "0x1013F7F14", Offset = "0x13F7F14", VA = "0x1013F7F14")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012C RID: 300
		// (add) Token: 0x060054BD RID: 21693 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054BE RID: 21694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012C")]
		public event Action<int, BattleDefine.eJoinMember> OnSelectMasterSkill
		{
			[Token(Token = "0x6004E16")]
			[Address(RVA = "0x1013F8024", Offset = "0x13F8024", VA = "0x1013F8024")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E17")]
			[Address(RVA = "0x1013F8134", Offset = "0x13F8134", VA = "0x1013F8134")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012D RID: 301
		// (add) Token: 0x060054BF RID: 21695 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054C0 RID: 21696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012D")]
		public event Action OnCancelMemberChange
		{
			[Token(Token = "0x6004E18")]
			[Address(RVA = "0x1013F8244", Offset = "0x13F8244", VA = "0x1013F8244")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E19")]
			[Address(RVA = "0x1013F8354", Offset = "0x13F8354", VA = "0x1013F8354")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012E RID: 302
		// (add) Token: 0x060054C1 RID: 21697 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054C2 RID: 21698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012E")]
		public event Action OnClickTogetherButton
		{
			[Token(Token = "0x6004E1A")]
			[Address(RVA = "0x1013F8464", Offset = "0x13F8464", VA = "0x1013F8464")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E1B")]
			[Address(RVA = "0x1013F8574", Offset = "0x13F8574", VA = "0x1013F8574")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400012F RID: 303
		// (add) Token: 0x060054C3 RID: 21699 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054C4 RID: 21700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400012F")]
		public event Action<BattleDefine.eJoinMember> OnDecideTogetherMember
		{
			[Token(Token = "0x6004E1C")]
			[Address(RVA = "0x1013F8684", Offset = "0x13F8684", VA = "0x1013F8684")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E1D")]
			[Address(RVA = "0x1013F8794", Offset = "0x13F8794", VA = "0x1013F8794")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000130 RID: 304
		// (add) Token: 0x060054C5 RID: 21701 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054C6 RID: 21702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000130")]
		public event Action<List<BattleDefine.eJoinMember>> OnDecideTogether
		{
			[Token(Token = "0x6004E1E")]
			[Address(RVA = "0x1013F88A4", Offset = "0x13F88A4", VA = "0x1013F88A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E1F")]
			[Address(RVA = "0x1013F89B4", Offset = "0x13F89B4", VA = "0x1013F89B4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000131 RID: 305
		// (add) Token: 0x060054C7 RID: 21703 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054C8 RID: 21704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000131")]
		public event Action OnCancelTogether
		{
			[Token(Token = "0x6004E20")]
			[Address(RVA = "0x1013F8AC4", Offset = "0x13F8AC4", VA = "0x1013F8AC4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E21")]
			[Address(RVA = "0x1013F8BD4", Offset = "0x13F8BD4", VA = "0x1013F8BD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000132 RID: 306
		// (add) Token: 0x060054C9 RID: 21705 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054CA RID: 21706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000132")]
		public event Func<bool> OnJudgeInterruptFriendJoinSelect
		{
			[Token(Token = "0x6004E22")]
			[Address(RVA = "0x1013F8CE4", Offset = "0x13F8CE4", VA = "0x1013F8CE4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E23")]
			[Address(RVA = "0x1013F8DF4", Offset = "0x13F8DF4", VA = "0x1013F8DF4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000133 RID: 307
		// (add) Token: 0x060054CB RID: 21707 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054CC RID: 21708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000133")]
		public event Action OnDecideInterruptFriendJoinSelect
		{
			[Token(Token = "0x6004E24")]
			[Address(RVA = "0x1013F8F04", Offset = "0x13F8F04", VA = "0x1013F8F04")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E25")]
			[Address(RVA = "0x1013F9014", Offset = "0x13F9014", VA = "0x1013F9014")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000134 RID: 308
		// (add) Token: 0x060054CD RID: 21709 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054CE RID: 21710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000134")]
		public event Action OnCancelInterruptFriendJoinSelect
		{
			[Token(Token = "0x6004E26")]
			[Address(RVA = "0x1013F9124", Offset = "0x13F9124", VA = "0x1013F9124")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E27")]
			[Address(RVA = "0x1013F9234", Offset = "0x13F9234", VA = "0x1013F9234")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000135 RID: 309
		// (add) Token: 0x060054CF RID: 21711 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054D0 RID: 21712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000135")]
		public event Action OnCancelStatusWindow
		{
			[Token(Token = "0x6004E28")]
			[Address(RVA = "0x1013F9344", Offset = "0x13F9344", VA = "0x1013F9344")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E29")]
			[Address(RVA = "0x1013F9454", Offset = "0x13F9454", VA = "0x1013F9454")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000136 RID: 310
		// (add) Token: 0x060054D1 RID: 21713 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054D2 RID: 21714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000136")]
		public event Action<int, bool> OnChangeCommandButton
		{
			[Token(Token = "0x6004E2A")]
			[Address(RVA = "0x1013F9564", Offset = "0x13F9564", VA = "0x1013F9564")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E2B")]
			[Address(RVA = "0x1013F9674", Offset = "0x13F9674", VA = "0x1013F9674")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000137 RID: 311
		// (add) Token: 0x060054D3 RID: 21715 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054D4 RID: 21716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000137")]
		public event Action<int> OnClickCommandButton
		{
			[Token(Token = "0x6004E2C")]
			[Address(RVA = "0x1013F9784", Offset = "0x13F9784", VA = "0x1013F9784")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E2D")]
			[Address(RVA = "0x1013F9894", Offset = "0x13F9894", VA = "0x1013F9894")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000138 RID: 312
		// (add) Token: 0x060054D5 RID: 21717 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060054D6 RID: 21718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000138")]
		public event Action OnEndCommandButtonLevelUp
		{
			[Token(Token = "0x6004E2E")]
			[Address(RVA = "0x1013F99A4", Offset = "0x13F99A4", VA = "0x1013F99A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004E2F")]
			[Address(RVA = "0x1013F9AB4", Offset = "0x13F9AB4", VA = "0x1013F9AB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060054D7 RID: 21719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E30")]
		[Address(RVA = "0x1013F9BC4", Offset = "0x13F9BC4", VA = "0x1013F9BC4")]
		public void Awake()
		{
		}

		// Token: 0x060054D8 RID: 21720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E31")]
		[Address(RVA = "0x1013F9C1C", Offset = "0x13F9C1C", VA = "0x1013F9C1C")]
		public void Setup(BattleSystem system, int friendcharaID, int masterOrbID)
		{
		}

		// Token: 0x060054D9 RID: 21721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E32")]
		[Address(RVA = "0x1013FA55C", Offset = "0x13FA55C", VA = "0x1013FA55C")]
		public void PrepareCharaResource(List<int> charaIDs)
		{
		}

		// Token: 0x060054DA RID: 21722 RVA: 0x0001C9E0 File Offset: 0x0001ABE0
		[Token(Token = "0x6004E33")]
		[Address(RVA = "0x1013FA718", Offset = "0x13FA718", VA = "0x1013FA718")]
		public bool IsDonePrepareCharaResource()
		{
			return default(bool);
		}

		// Token: 0x060054DB RID: 21723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E34")]
		[Address(RVA = "0x1013FA858", Offset = "0x13FA858", VA = "0x1013FA858")]
		private void Update()
		{
		}

		// Token: 0x060054DC RID: 21724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E35")]
		[Address(RVA = "0x1013FA9BC", Offset = "0x13FA9BC", VA = "0x1013FA9BC")]
		private void SetState(int state)
		{
		}

		// Token: 0x060054DD RID: 21725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E36")]
		[Address(RVA = "0x1013FAB1C", Offset = "0x13FAB1C", VA = "0x1013FAB1C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060054DE RID: 21726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E37")]
		[Address(RVA = "0x1013FACB0", Offset = "0x13FACB0", VA = "0x1013FACB0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060054DF RID: 21727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E38")]
		[Address(RVA = "0x1013FAE04", Offset = "0x13FAE04", VA = "0x1013FAE04")]
		public void PlayOutWaveOut(bool isFinalWave)
		{
		}

		// Token: 0x060054E0 RID: 21728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E39")]
		[Address(RVA = "0x1013FAED4", Offset = "0x13FAED4", VA = "0x1013FAED4")]
		public void Hide()
		{
		}

		// Token: 0x060054E1 RID: 21729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E3A")]
		[Address(RVA = "0x1013FAEDC", Offset = "0x13FAEDC", VA = "0x1013FAEDC")]
		public void HideWaveOut()
		{
		}

		// Token: 0x060054E2 RID: 21730 RVA: 0x0001C9F8 File Offset: 0x0001ABF8
		[Token(Token = "0x6004E3B")]
		[Address(RVA = "0x1013FAEE0", Offset = "0x13FAEE0", VA = "0x1013FAEE0", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060054E3 RID: 21731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E3C")]
		[Address(RVA = "0x1013FAEF0", Offset = "0x13FAEF0", VA = "0x1013FAEF0", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060054E4 RID: 21732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E3D")]
		[Address(RVA = "0x1013FB1B4", Offset = "0x13FB1B4", VA = "0x1013FB1B4")]
		public void StartCommandInput()
		{
		}

		// Token: 0x060054E5 RID: 21733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E3E")]
		[Address(RVA = "0x1013FB2F8", Offset = "0x13FB2F8", VA = "0x1013FB2F8")]
		private void EndCommandInput()
		{
		}

		// Token: 0x060054E6 RID: 21734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E3F")]
		[Address(RVA = "0x1013FAE3C", Offset = "0x13FAE3C", VA = "0x1013FAE3C")]
		public void ShowMainUIAutoOnly(bool isEnable)
		{
		}

		// Token: 0x060054E7 RID: 21735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E40")]
		[Address(RVA = "0x1013FB364", Offset = "0x13FB364", VA = "0x1013FB364")]
		public void ShowMainUIGroup(bool isAutoOnly)
		{
		}

		// Token: 0x060054E8 RID: 21736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E41")]
		[Address(RVA = "0x1013FAC24", Offset = "0x13FAC24", VA = "0x1013FAC24")]
		public void PlayInTopUI()
		{
		}

		// Token: 0x060054E9 RID: 21737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E42")]
		[Address(RVA = "0x1013FAD68", Offset = "0x13FAD68", VA = "0x1013FAD68")]
		public void PlayOutTopUI(bool isFinalWave = true)
		{
		}

		// Token: 0x060054EA RID: 21738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E43")]
		[Address(RVA = "0x1013FB478", Offset = "0x13FB478", VA = "0x1013FB478")]
		public void PlayInReward()
		{
		}

		// Token: 0x060054EB RID: 21739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E44")]
		[Address(RVA = "0x1013FB4AC", Offset = "0x13FB4AC", VA = "0x1013FB4AC")]
		public void PlayOutReward()
		{
		}

		// Token: 0x060054EC RID: 21740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E45")]
		[Address(RVA = "0x1013FB4E0", Offset = "0x13FB4E0", VA = "0x1013FB4E0")]
		public void HideTopUI()
		{
		}

		// Token: 0x060054ED RID: 21741 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E46")]
		[Address(RVA = "0x1013FB620", Offset = "0x13FB620", VA = "0x1013FB620")]
		public BattleTransitFade GetTransitFade()
		{
			return null;
		}

		// Token: 0x060054EE RID: 21742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E47")]
		[Address(RVA = "0x1013FB44C", Offset = "0x13FB44C", VA = "0x1013FB44C")]
		public void PlayInStatus()
		{
		}

		// Token: 0x060054EF RID: 21743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E48")]
		[Address(RVA = "0x1013FADE0", Offset = "0x13FADE0", VA = "0x1013FADE0")]
		public void PlayOutStatus()
		{
		}

		// Token: 0x060054F0 RID: 21744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E49")]
		[Address(RVA = "0x1013FB628", Offset = "0x13FB628", VA = "0x1013FB628")]
		public void PlayInStatusPlayer()
		{
		}

		// Token: 0x060054F1 RID: 21745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E4A")]
		[Address(RVA = "0x1013FB654", Offset = "0x13FB654", VA = "0x1013FB654")]
		public void PlayInStatusEnemy()
		{
		}

		// Token: 0x060054F2 RID: 21746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E4B")]
		[Address(RVA = "0x1013FB6F8", Offset = "0x13FB6F8", VA = "0x1013FB6F8")]
		public void PlayOutStatusPlayer()
		{
		}

		// Token: 0x060054F3 RID: 21747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E4C")]
		[Address(RVA = "0x1013FB724", Offset = "0x13FB724", VA = "0x1013FB724")]
		public void PlayOutStatusEnemy()
		{
		}

		// Token: 0x060054F4 RID: 21748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E4D")]
		[Address(RVA = "0x1013FB5D8", Offset = "0x13FB5D8", VA = "0x1013FB5D8")]
		public void HideStatus()
		{
		}

		// Token: 0x060054F5 RID: 21749 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E4E")]
		[Address(RVA = "0x1013FB750", Offset = "0x13FB750", VA = "0x1013FB750")]
		public CharacterStatus GetCharacterStatus(BattleDefine.ePartyType party, BattleDefine.eJoinMember join)
		{
			return null;
		}

		// Token: 0x060054F6 RID: 21750 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E4F")]
		[Address(RVA = "0x1013FB774", Offset = "0x13FB774", VA = "0x1013FB774")]
		public CharacterStatus GetPlayerStatus(BattleDefine.eJoinMember join)
		{
			return null;
		}

		// Token: 0x060054F7 RID: 21751 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E50")]
		[Address(RVA = "0x1013FB77C", Offset = "0x13FB77C", VA = "0x1013FB77C")]
		public CharacterStatus GetPlayerStatus(int index)
		{
			return null;
		}

		// Token: 0x060054F8 RID: 21752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E51")]
		[Address(RVA = "0x1013FB804", Offset = "0x13FB804", VA = "0x1013FB804")]
		public void SetPlayerStatusAllEnableRender(bool flg)
		{
		}

		// Token: 0x060054F9 RID: 21753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E52")]
		[Address(RVA = "0x1013FB8A8", Offset = "0x13FB8A8", VA = "0x1013FB8A8")]
		public void SetPlayerStatusPosition(int index, Vector3 worldPoint)
		{
		}

		// Token: 0x060054FA RID: 21754 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E53")]
		[Address(RVA = "0x1013FB778", Offset = "0x13FB778", VA = "0x1013FB778")]
		public CharacterStatus GetEnemyStatus(BattleDefine.eJoinMember join)
		{
			return null;
		}

		// Token: 0x060054FB RID: 21755 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E54")]
		[Address(RVA = "0x1013FB8AC", Offset = "0x13FB8AC", VA = "0x1013FB8AC")]
		public CharacterStatus GetEnemyStatus(int index)
		{
			return null;
		}

		// Token: 0x060054FC RID: 21756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E55")]
		[Address(RVA = "0x1013FB934", Offset = "0x13FB934", VA = "0x1013FB934")]
		public void SetEnemyStatusAllEnableRender(bool flg)
		{
		}

		// Token: 0x060054FD RID: 21757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E56")]
		[Address(RVA = "0x1013FB9D8", Offset = "0x13FB9D8", VA = "0x1013FB9D8")]
		public void SetEnemyStatusPosition(int index, Vector3 worldPoint)
		{
		}

		// Token: 0x060054FE RID: 21758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E57")]
		[Address(RVA = "0x1013FB680", Offset = "0x13FB680", VA = "0x1013FB680")]
		public void SetBuffIconApply()
		{
		}

		// Token: 0x060054FF RID: 21759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E58")]
		[Address(RVA = "0x1013FB9DC", Offset = "0x13FB9DC", VA = "0x1013FB9DC")]
		public void SetWave(int waveIndex, int waveNum)
		{
		}

		// Token: 0x06005500 RID: 21760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E59")]
		[Address(RVA = "0x1013FBB40", Offset = "0x13FBB40", VA = "0x1013FBB40")]
		public void SetTotalPointEnableRender(bool flg)
		{
		}

		// Token: 0x06005501 RID: 21761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E5A")]
		[Address(RVA = "0x1013FBB90", Offset = "0x13FBB90", VA = "0x1013FBB90")]
		public void SetTotalPointValue(long value, bool isMove = false)
		{
		}

		// Token: 0x06005502 RID: 21762 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E5B")]
		[Address(RVA = "0x1013FBBE4", Offset = "0x13FBBE4", VA = "0x1013FBBE4")]
		public CustomButton GetMenuButton()
		{
			return null;
		}

		// Token: 0x06005503 RID: 21763 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E5C")]
		[Address(RVA = "0x1013FBBEC", Offset = "0x13FBBEC", VA = "0x1013FBBEC")]
		public CustomButton GetAutoButton()
		{
			return null;
		}

		// Token: 0x06005504 RID: 21764 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E5D")]
		[Address(RVA = "0x1013FBBF4", Offset = "0x13FBBF4", VA = "0x1013FBBF4")]
		public CustomButton GetSpeedButton()
		{
			return null;
		}

		// Token: 0x06005505 RID: 21765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E5E")]
		[Address(RVA = "0x1013FBBFC", Offset = "0x13FBBFC", VA = "0x1013FBBFC")]
		public void OnClickMenuButtonCallBack()
		{
		}

		// Token: 0x06005506 RID: 21766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E5F")]
		[Address(RVA = "0x1013FBC64", Offset = "0x13FBC64", VA = "0x1013FBC64")]
		public void OnClickAutoButtonCallBack()
		{
		}

		// Token: 0x06005507 RID: 21767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E60")]
		[Address(RVA = "0x1013FBC70", Offset = "0x13FBC70", VA = "0x1013FBC70")]
		public void SetAutoFlag(bool flag)
		{
		}

		// Token: 0x06005508 RID: 21768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E61")]
		[Address(RVA = "0x1013FBCF0", Offset = "0x13FBCF0", VA = "0x1013FBCF0")]
		public void OnClickSpeedButtonCallBack()
		{
		}

		// Token: 0x06005509 RID: 21769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E62")]
		[Address(RVA = "0x1013FBCFC", Offset = "0x13FBCFC", VA = "0x1013FBCFC")]
		public void SetSpeedFlag(bool flag)
		{
		}

		// Token: 0x0600550A RID: 21770 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E63")]
		[Address(RVA = "0x1013FBD34", Offset = "0x13FBD34", VA = "0x1013FBD34")]
		public BattleFriendButton GetFriendButton()
		{
			return null;
		}

		// Token: 0x0600550B RID: 21771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E64")]
		[Address(RVA = "0x1013FBD3C", Offset = "0x13FBD3C", VA = "0x1013FBD3C")]
		public void OnClickBattleFriendButtonCallBack()
		{
		}

		// Token: 0x0600550C RID: 21772 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E65")]
		[Address(RVA = "0x1013FBD48", Offset = "0x13FBD48", VA = "0x1013FBD48")]
		public BattleOrderIndicator GetOrder()
		{
			return null;
		}

		// Token: 0x0600550D RID: 21773 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E66")]
		[Address(RVA = "0x1013FBD50", Offset = "0x13FBD50", VA = "0x1013FBD50")]
		public BattleReward GetReward()
		{
			return null;
		}

		// Token: 0x0600550E RID: 21774 RVA: 0x0001CA10 File Offset: 0x0001AC10
		[Token(Token = "0x6004E67")]
		[Address(RVA = "0x1013FBD58", Offset = "0x13FBD58", VA = "0x1013FBD58")]
		public Vector2 GetWorldRewardPosition()
		{
			return default(Vector2);
		}

		// Token: 0x0600550F RID: 21775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E68")]
		[Address(RVA = "0x1013FBF84", Offset = "0x13FBF84", VA = "0x1013FBF84")]
		public void ShiftDownCommandSet(bool flg, bool immediate = false)
		{
		}

		// Token: 0x06005510 RID: 21776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E69")]
		[Address(RVA = "0x1013FBFCC", Offset = "0x13FBFCC", VA = "0x1013FBFCC")]
		public void SetCommandSetBlank()
		{
		}

		// Token: 0x06005511 RID: 21777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E6A")]
		[Address(RVA = "0x1013FB588", Offset = "0x13FB588", VA = "0x1013FB588")]
		private void SetCommandSetEnableRender(bool flg)
		{
		}

		// Token: 0x06005512 RID: 21778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E6B")]
		[Address(RVA = "0x1013FA4F8", Offset = "0x13FA4F8", VA = "0x1013FA4F8")]
		private void SetCommandSetEnableInput(bool flg)
		{
		}

		// Token: 0x06005513 RID: 21779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E6C")]
		[Address(RVA = "0x1013FC148", Offset = "0x13FC148", VA = "0x1013FC148")]
		public void SetOwnerImage(int charaID, bool isChangeOnOutOfScreen)
		{
		}

		// Token: 0x06005514 RID: 21780 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E6D")]
		[Address(RVA = "0x1013FC2A8", Offset = "0x13FC2A8", VA = "0x1013FC2A8")]
		public GameObject GetOwnerImageObject()
		{
			return null;
		}

		// Token: 0x06005515 RID: 21781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E6E")]
		[Address(RVA = "0x1013FB27C", Offset = "0x13FB27C", VA = "0x1013FB27C")]
		public void SetEnableInputOwnerImage(bool flg)
		{
		}

		// Token: 0x06005516 RID: 21782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E6F")]
		[Address(RVA = "0x1013FC2D8", Offset = "0x13FC2D8", VA = "0x1013FC2D8")]
		public void AppearOwnerImage()
		{
		}

		// Token: 0x06005517 RID: 21783 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E70")]
		[Address(RVA = "0x1013FC338", Offset = "0x13FC338", VA = "0x1013FC338")]
		public BattleDescription GetDescription()
		{
			return null;
		}

		// Token: 0x06005518 RID: 21784 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E71")]
		[Address(RVA = "0x1013FC340", Offset = "0x13FC340", VA = "0x1013FC340")]
		public CommandButton GetCommandButton(int index)
		{
			return null;
		}

		// Token: 0x06005519 RID: 21785 RVA: 0x0001CA28 File Offset: 0x0001AC28
		[Token(Token = "0x6004E72")]
		[Address(RVA = "0x1013FC378", Offset = "0x13FC378", VA = "0x1013FC378")]
		public int GetCommandButtonNum()
		{
			return 0;
		}

		// Token: 0x0600551A RID: 21786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E73")]
		[Address(RVA = "0x1013FC3A8", Offset = "0x13FC3A8", VA = "0x1013FC3A8")]
		private void OnClickCommandButtonCallBack(int index, bool isFirstSelect, bool isSilence)
		{
		}

		// Token: 0x0600551B RID: 21787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E74")]
		[Address(RVA = "0x1013FC074", Offset = "0x13FC074", VA = "0x1013FC074")]
		public void SetAllCommandButtonNotSelect()
		{
		}

		// Token: 0x0600551C RID: 21788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E75")]
		[Address(RVA = "0x1013FC6C0", Offset = "0x13FC6C0", VA = "0x1013FC6C0")]
		public void SetCommandButtonSelecting(int idx)
		{
		}

		// Token: 0x0600551D RID: 21789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E76")]
		[Address(RVA = "0x1013FC774", Offset = "0x13FC774", VA = "0x1013FC774")]
		public void SetCommandButtonDecided(int idx, bool flg)
		{
		}

		// Token: 0x0600551E RID: 21790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E77")]
		[Address(RVA = "0x1013FC838", Offset = "0x13FC838", VA = "0x1013FC838")]
		public void SetAllCommandButtonEnableInput(bool flg)
		{
		}

		// Token: 0x0600551F RID: 21791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E78")]
		[Address(RVA = "0x1013FC8CC", Offset = "0x13FC8CC", VA = "0x1013FC8CC")]
		public void PlayCommandButtonLevelup(int idx)
		{
		}

		// Token: 0x06005520 RID: 21792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E79")]
		[Address(RVA = "0x1013FCA18", Offset = "0x13FCA18", VA = "0x1013FCA18")]
		public void OnEndCommandButtonLevelUpCallBack()
		{
		}

		// Token: 0x06005521 RID: 21793 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E7A")]
		[Address(RVA = "0x1013FCA24", Offset = "0x13FCA24", VA = "0x1013FCA24")]
		public TogetherButton GetTogetherButton()
		{
			return null;
		}

		// Token: 0x06005522 RID: 21794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E7B")]
		[Address(RVA = "0x1013FCA2C", Offset = "0x13FCA2C", VA = "0x1013FCA2C")]
		public void OnClickTogetherButtonCallBack()
		{
		}

		// Token: 0x06005523 RID: 21795 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E7C")]
		[Address(RVA = "0x1013FCA80", Offset = "0x13FCA80", VA = "0x1013FCA80")]
		public TogetherSelect GetTogetherSelect()
		{
			return null;
		}

		// Token: 0x06005524 RID: 21796 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E7D")]
		[Address(RVA = "0x1013FCA88", Offset = "0x13FCA88", VA = "0x1013FCA88")]
		public TogetherCutIn GetTogetherCutIn()
		{
			return null;
		}

		// Token: 0x06005525 RID: 21797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E7E")]
		[Address(RVA = "0x1013FCA90", Offset = "0x13FCA90", VA = "0x1013FCA90")]
		public void OpenTogetherCutInSkipSelect(int[] charaIDs, eElementType[] element, BattleDefine.eJoinMember[] join, int[] skillIDs, List<BattleDefine.eJoinMember> selectOrder)
		{
		}

		// Token: 0x06005526 RID: 21798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E7F")]
		[Address(RVA = "0x1013FCB00", Offset = "0x13FCB00", VA = "0x1013FCB00")]
		public void OpenTogetherSelect(int[] charaIDs, eElementType[] elements, BattleDefine.eJoinMember[] join, int[] skillIDs, int selectableMax)
		{
		}

		// Token: 0x06005527 RID: 21799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E80")]
		[Address(RVA = "0x1013FCC1C", Offset = "0x13FCC1C", VA = "0x1013FCC1C")]
		public void OnDecideTogetherMemberCallBack(BattleDefine.eJoinMember join)
		{
		}

		// Token: 0x06005528 RID: 21800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E81")]
		[Address(RVA = "0x1013FCC7C", Offset = "0x13FCC7C", VA = "0x1013FCC7C")]
		public void OnDecideTogetherCallBack()
		{
		}

		// Token: 0x06005529 RID: 21801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E82")]
		[Address(RVA = "0x1013FCD50", Offset = "0x13FCD50", VA = "0x1013FCD50")]
		public void OnCancelTogetherCallBack()
		{
		}

		// Token: 0x0600552A RID: 21802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E83")]
		[Address(RVA = "0x1013FCA94", Offset = "0x13FCA94", VA = "0x1013FCA94")]
		public void PlayTogetherCutIn(int[] charaIDs)
		{
		}

		// Token: 0x0600552B RID: 21803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E84")]
		[Address(RVA = "0x1013FCDAC", Offset = "0x13FCDAC", VA = "0x1013FCDAC")]
		public void StartTogetherCutInChara()
		{
		}

		// Token: 0x0600552C RID: 21804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E85")]
		[Address(RVA = "0x1013FCDDC", Offset = "0x13FCDDC", VA = "0x1013FCDDC")]
		public void FinishTogetherCutIn()
		{
		}

		// Token: 0x0600552D RID: 21805 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E86")]
		[Address(RVA = "0x1013FCE0C", Offset = "0x13FCE0C", VA = "0x1013FCE0C")]
		public TurnOwnerMarker GetTurnOwnerMarker()
		{
			return null;
		}

		// Token: 0x0600552E RID: 21806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E87")]
		[Address(RVA = "0x1013FCE14", Offset = "0x13FCE14", VA = "0x1013FCE14")]
		public void SetTurnOwnerMarker(bool flg, Vector3 worldPoint)
		{
		}

		// Token: 0x0600552F RID: 21807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E88")]
		[Address(RVA = "0x1013FCF1C", Offset = "0x13FCF1C", VA = "0x1013FCF1C")]
		public void SetTargetMarkerAllEnableRender(bool flg)
		{
		}

		// Token: 0x06005530 RID: 21808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E89")]
		[Address(RVA = "0x1013FCFBC", Offset = "0x13FCFBC", VA = "0x1013FCFBC")]
		public void SetTargetMarker(int idx, bool flg, Transform refTransform, Vector3 offsetFromRefTransform, TargetMarker.eMode mode = TargetMarker.eMode.Normal)
		{
		}

		// Token: 0x06005531 RID: 21809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E8A")]
		[Address(RVA = "0x1013FCEA0", Offset = "0x13FCEA0", VA = "0x1013FCEA0")]
		public void ClearTargetMarkerOrderIcon()
		{
		}

		// Token: 0x06005532 RID: 21810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E8B")]
		[Address(RVA = "0x1013FD0B8", Offset = "0x13FD0B8", VA = "0x1013FD0B8")]
		public void SetTargetMarkerToOrderIcon(BattleDefine.ePartyType party, BattleDefine.eJoinMember join, bool flg)
		{
		}

		// Token: 0x06005533 RID: 21811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E8C")]
		[Address(RVA = "0x1013FD104", Offset = "0x13FD104", VA = "0x1013FD104")]
		public void SetTargetMarkerToOrderIconPlayer(bool[] flg)
		{
		}

		// Token: 0x06005534 RID: 21812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E8D")]
		[Address(RVA = "0x1013FD1A4", Offset = "0x13FD1A4", VA = "0x1013FD1A4")]
		public void SetTargetMarkerToOrderIconEnemy(bool[] flg)
		{
		}

		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06005535 RID: 21813 RVA: 0x0001CA40 File Offset: 0x0001AC40
		// (set) Token: 0x06005536 RID: 21814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000551")]
		public bool IsExectuteRetireFromDirect
		{
			[Token(Token = "0x6004E8E")]
			[Address(RVA = "0x1013FD244", Offset = "0x13FD244", VA = "0x1013FD244")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004E8F")]
			[Address(RVA = "0x1013FD24C", Offset = "0x13FD24C", VA = "0x1013FD24C")]
			set
			{
			}
		}

		// Token: 0x06005537 RID: 21815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E90")]
		[Address(RVA = "0x1013FD254", Offset = "0x13FD254", VA = "0x1013FD254")]
		public void OpenBattleMenuWindow()
		{
		}

		// Token: 0x06005538 RID: 21816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E91")]
		[Address(RVA = "0x1013FD280", Offset = "0x13FD280", VA = "0x1013FD280")]
		public void OnCancelBattleMenuWindowCallBack()
		{
		}

		// Token: 0x06005539 RID: 21817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E92")]
		[Address(RVA = "0x1013FD2AC", Offset = "0x13FD2AC", VA = "0x1013FD2AC")]
		public void OnClickRetireButtonCallBack()
		{
		}

		// Token: 0x0600553A RID: 21818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E93")]
		[Address(RVA = "0x1013FD2B4", Offset = "0x13FD2B4", VA = "0x1013FD2B4")]
		private void ExecuteRetire(bool isExecuteRetireFromMenu)
		{
		}

		// Token: 0x0600553B RID: 21819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E94")]
		[Address(RVA = "0x1013FD2EC", Offset = "0x13FD2EC", VA = "0x1013FD2EC")]
		public void OpenMemberChange()
		{
		}

		// Token: 0x0600553C RID: 21820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E95")]
		[Address(RVA = "0x1013FD31C", Offset = "0x13FD31C", VA = "0x1013FD31C")]
		public void CloseMemberChange()
		{
		}

		// Token: 0x0600553D RID: 21821 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E96")]
		[Address(RVA = "0x1013FD34C", Offset = "0x13FD34C", VA = "0x1013FD34C")]
		public MemberChange GetMemberChange()
		{
			return null;
		}

		// Token: 0x0600553E RID: 21822 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004E97")]
		[Address(RVA = "0x1013FD354", Offset = "0x13FD354", VA = "0x1013FD354")]
		public BenchStatus GetBenchStatus(BattleDefine.eJoinMember bench)
		{
			return null;
		}

		// Token: 0x0600553F RID: 21823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E98")]
		[Address(RVA = "0x1013FD38C", Offset = "0x13FD38C", VA = "0x1013FD38C")]
		public void SetMasterSkillTarget(BattleDefine.ePartyType partyType, BattleDefine.eJoinMember join, int charaIDorResourceID, bool isInteractable)
		{
		}

		// Token: 0x06005540 RID: 21824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E99")]
		[Address(RVA = "0x1013FD410", Offset = "0x13FD410", VA = "0x1013FD410")]
		public void StartMemberChange()
		{
		}

		// Token: 0x06005541 RID: 21825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E9A")]
		[Address(RVA = "0x1013FD4A4", Offset = "0x13FD4A4", VA = "0x1013FD4A4")]
		public void DecideMemberChange(int idx)
		{
		}

		// Token: 0x06005542 RID: 21826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E9B")]
		[Address(RVA = "0x1013FD538", Offset = "0x13FD538", VA = "0x1013FD538")]
		public void DecideMasterSkill(int commandIndex, BattleDefine.eJoinMember target)
		{
		}

		// Token: 0x06005543 RID: 21827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E9C")]
		[Address(RVA = "0x1013FD604", Offset = "0x13FD604", VA = "0x1013FD604")]
		public void CancelMemberChange()
		{
		}

		// Token: 0x06005544 RID: 21828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E9D")]
		[Address(RVA = "0x1013FD658", Offset = "0x13FD658", VA = "0x1013FD658")]
		public void OpenInterruptFriendJoinSelect()
		{
		}

		// Token: 0x06005545 RID: 21829 RVA: 0x0001CA58 File Offset: 0x0001AC58
		[Token(Token = "0x6004E9E")]
		[Address(RVA = "0x1013FD6D0", Offset = "0x13FD6D0", VA = "0x1013FD6D0")]
		public bool JudgeInterruptFriendJoinSelect()
		{
			return default(bool);
		}

		// Token: 0x06005546 RID: 21830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004E9F")]
		[Address(RVA = "0x1013FD730", Offset = "0x13FD730", VA = "0x1013FD730")]
		public void DecideInterruptFriendJoinSelect()
		{
		}

		// Token: 0x06005547 RID: 21831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EA0")]
		[Address(RVA = "0x1013FD780", Offset = "0x13FD780", VA = "0x1013FD780")]
		public void CancelInterruptFriendJoinSelect()
		{
		}

		// Token: 0x06005548 RID: 21832 RVA: 0x0001CA70 File Offset: 0x0001AC70
		[Token(Token = "0x6004EA1")]
		[Address(RVA = "0x1013FD7CC", Offset = "0x13FD7CC", VA = "0x1013FD7CC")]
		public static BattleUI.eDispDamageAbnormal GetDispAbnormalInput(bool isUnhappy, bool isPoison)
		{
			return (BattleUI.eDispDamageAbnormal)0;
		}

		// Token: 0x06005549 RID: 21833 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EA2")]
		[Address(RVA = "0x1013FD7E0", Offset = "0x13FD7E0", VA = "0x1013FD7E0")]
		public DamageValueDisplay GetDamageDisplay()
		{
			return null;
		}

		// Token: 0x0600554A RID: 21834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EA3")]
		[Address(RVA = "0x1013FD7E8", Offset = "0x13FD7E8", VA = "0x1013FD7E8")]
		public void DisplayDamage(int value, bool isCritical, int registHit_Or_DefaultHit_Or_WeakHit, bool isUnhappy, bool isPoison, Transform targetTransform, float offsetYfromTargetTransform)
		{
		}

		// Token: 0x0600554B RID: 21835 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EA4")]
		[Address(RVA = "0x1013FD870", Offset = "0x13FD870", VA = "0x1013FD870")]
		public EventPointValueDisplay GetEventPointDisplay()
		{
			return null;
		}

		// Token: 0x0600554C RID: 21836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EA5")]
		[Address(RVA = "0x1013FD878", Offset = "0x13FD878", VA = "0x1013FD878")]
		public void DisplayEventPoint(int value, Transform targetTransform, float offsetYfromTargetTransform)
		{
		}

		// Token: 0x0600554D RID: 21837 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EA6")]
		[Address(RVA = "0x1013FD8D0", Offset = "0x13FD8D0", VA = "0x1013FD8D0")]
		public PopUpIconDisplay GetPopUpIconDisplay()
		{
			return null;
		}

		// Token: 0x0600554E RID: 21838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EA7")]
		[Address(RVA = "0x1013FD8D8", Offset = "0x13FD8D8", VA = "0x1013FD8D8")]
		public void PopUpIcon(List<SkillActionEffectSet.IconData> iconDatas, Transform targetTransform, float offsetYfromTargetTransform)
		{
		}

		// Token: 0x0600554F RID: 21839 RVA: 0x0001CA88 File Offset: 0x0001AC88
		[Token(Token = "0x6004EA8")]
		[Address(RVA = "0x1013FD930", Offset = "0x13FD930", VA = "0x1013FD930")]
		public bool IsPlayingPopUpIcon()
		{
			return default(bool);
		}

		// Token: 0x06005550 RID: 21840 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EA9")]
		[Address(RVA = "0x1013FD960", Offset = "0x13FD960", VA = "0x1013FD960")]
		public BattleMessage GetBattleMessage()
		{
			return null;
		}

		// Token: 0x06005551 RID: 21841 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EAA")]
		[Address(RVA = "0x1013FD968", Offset = "0x13FD968", VA = "0x1013FD968")]
		public UniqueSkillNameWindow GetUniqueSkillNameWindow()
		{
			return null;
		}

		// Token: 0x06005552 RID: 21842 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EAB")]
		[Address(RVA = "0x1013FD970", Offset = "0x13FD970", VA = "0x1013FD970")]
		public TotalDamageDisplay GetTotalDamageDisplay()
		{
			return null;
		}

		// Token: 0x06005553 RID: 21843 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004EAC")]
		[Address(RVA = "0x1013FD978", Offset = "0x13FD978", VA = "0x1013FD978")]
		public TitlePlayer GetWarningStart()
		{
			return null;
		}

		// Token: 0x06005554 RID: 21844 RVA: 0x0001CAA0 File Offset: 0x0001ACA0
		[Token(Token = "0x6004EAD")]
		[Address(RVA = "0x1013FD980", Offset = "0x13FD980", VA = "0x1013FD980")]
		public Vector2 UIPositionToWorld(Vector2 UIPosition)
		{
			return default(Vector2);
		}

		// Token: 0x06005555 RID: 21845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EAE")]
		[Address(RVA = "0x1013FDA2C", Offset = "0x13FDA2C", VA = "0x1013FDA2C")]
		public void UpdateAndroidBackKey()
		{
		}

		// Token: 0x06005556 RID: 21846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EAF")]
		[Address(RVA = "0x1013FDA6C", Offset = "0x13FDA6C", VA = "0x1013FDA6C")]
		public void UpdateOnPlayerChange(CharacterHandler ch, float togetherValue)
		{
		}

		// Token: 0x06005557 RID: 21847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB0")]
		[Address(RVA = "0x1013FDB64", Offset = "0x13FDB64", VA = "0x1013FDB64")]
		public void OpenStatusWindow(CharacterHandler charaHndl)
		{
		}

		// Token: 0x06005558 RID: 21848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB1")]
		[Address(RVA = "0x1013FDCA4", Offset = "0x13FDCA4", VA = "0x1013FDCA4")]
		private void OnCloseBattleStatusWindow()
		{
		}

		// Token: 0x06005559 RID: 21849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB2")]
		[Address(RVA = "0x1013FDCD0", Offset = "0x13FDCD0", VA = "0x1013FDCD0")]
		public BattleUI()
		{
		}

		// Token: 0x04006706 RID: 26374
		[Token(Token = "0x4004865")]
		public const int STATE_HIDE = 0;

		// Token: 0x04006707 RID: 26375
		[Token(Token = "0x4004866")]
		public const int STATE_IN = 1;

		// Token: 0x04006708 RID: 26376
		[Token(Token = "0x4004867")]
		public const int STATE_WAIT = 2;

		// Token: 0x04006709 RID: 26377
		[Token(Token = "0x4004868")]
		public const int STATE_OUT = 3;

		// Token: 0x0400670A RID: 26378
		[Token(Token = "0x4004869")]
		public const int STATE_END = 4;

		// Token: 0x0400670B RID: 26379
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400486A")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400670C RID: 26380
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400486B")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x0400670D RID: 26381
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400486C")]
		[SerializeField]
		private CharacterStatus[] m_PlayerStatusObjs;

		// Token: 0x0400670E RID: 26382
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400486D")]
		[SerializeField]
		private CharacterStatus[] m_EnemyStatusObjs;

		// Token: 0x0400670F RID: 26383
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400486E")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10013124C", Offset = "0x13124C")]
		private AnimUIPlayer m_PlayerStatusAnim;

		// Token: 0x04006710 RID: 26384
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400486F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131298", Offset = "0x131298")]
		private AnimUIPlayer m_EnemyStatusAnim;

		// Token: 0x04006711 RID: 26385
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004870")]
		[SerializeField]
		private BattleTransitFade m_TransitFade;

		// Token: 0x04006712 RID: 26386
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004871")]
		[SerializeField]
		private UIGroup m_MainUIGroup;

		// Token: 0x04006713 RID: 26387
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004872")]
		[SerializeField]
		private BattleReward m_BattleReward;

		// Token: 0x04006714 RID: 26388
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004873")]
		[SerializeField]
		private UIGroup m_WaveGroup;

		// Token: 0x04006715 RID: 26389
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004874")]
		[SerializeField]
		private Text m_WaveTextObj;

		// Token: 0x04006716 RID: 26390
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004875")]
		[SerializeField]
		private TotalEventPointDisplay m_TotalEventPoint;

		// Token: 0x04006717 RID: 26391
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004876")]
		[SerializeField]
		private CustomButton m_AutoButton;

		// Token: 0x04006718 RID: 26392
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004877")]
		[SerializeField]
		private CustomButton m_MenuButton;

		// Token: 0x04006719 RID: 26393
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004878")]
		[SerializeField]
		private CustomButton m_SpeedButton;

		// Token: 0x0400671A RID: 26394
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004879")]
		[SerializeField]
		private BattleFriendButton m_BattleFriendButton;

		// Token: 0x0400671B RID: 26395
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400487A")]
		[SerializeField]
		private BattleOrderIndicator m_OrderIndicator;

		// Token: 0x0400671C RID: 26396
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400487B")]
		[SerializeField]
		private TogetherButton m_TogetherButton;

		// Token: 0x0400671D RID: 26397
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400487C")]
		[SerializeField]
		private CommandSet m_CommandSet;

		// Token: 0x0400671E RID: 26398
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400487D")]
		[SerializeField]
		private BattleDescription m_Description;

		// Token: 0x0400671F RID: 26399
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400487E")]
		[SerializeField]
		private OwnerImage m_OwnerImage;

		// Token: 0x04006720 RID: 26400
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400487F")]
		[SerializeField]
		private ElementIcon m_OwnerElement;

		// Token: 0x04006721 RID: 26401
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004880")]
		[SerializeField]
		private CommandButtonLevelUp m_CommandButtonLevelUpAnimation;

		// Token: 0x04006722 RID: 26402
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004881")]
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04006723 RID: 26403
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004882")]
		[SerializeField]
		private PassiveIconOwner m_PassiveIconOwner;

		// Token: 0x04006724 RID: 26404
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004883")]
		[SerializeField]
		private PassiveIconOwner m_AbilityIconOwner;

		// Token: 0x04006725 RID: 26405
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004884")]
		[SerializeField]
		private DamageValueDisplay m_DamageValueDisplay;

		// Token: 0x04006726 RID: 26406
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004885")]
		[SerializeField]
		private EventPointValueDisplay m_EventPointValueDisplay;

		// Token: 0x04006727 RID: 26407
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004886")]
		[SerializeField]
		private PopUpIconDisplay m_PopUpIconDisplay;

		// Token: 0x04006728 RID: 26408
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004887")]
		[SerializeField]
		private TargetMarker[] m_TargetMarkers;

		// Token: 0x04006729 RID: 26409
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004888")]
		[SerializeField]
		private TurnOwnerMarker m_TurnOwnerMarker;

		// Token: 0x0400672A RID: 26410
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004889")]
		[SerializeField]
		private MemberChange m_MemberChange;

		// Token: 0x0400672B RID: 26411
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x400488A")]
		[SerializeField]
		private TogetherSelect m_TogetherSelect;

		// Token: 0x0400672C RID: 26412
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x400488B")]
		[SerializeField]
		private TogetherCutIn m_TogetherCutIn;

		// Token: 0x0400672D RID: 26413
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x400488C")]
		[SerializeField]
		private InterruptFriendJoinSelect m_InterruptFriendJoinSelect;

		// Token: 0x0400672E RID: 26414
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x400488D")]
		[SerializeField]
		private BattleMenuWindow m_BattleMenuWindow;

		// Token: 0x0400672F RID: 26415
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x400488E")]
		[SerializeField]
		private BattleStatusWindow m_BattleStatusWindow;

		// Token: 0x04006730 RID: 26416
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x400488F")]
		[SerializeField]
		private BattleMessage m_BattleMessage;

		// Token: 0x04006731 RID: 26417
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4004890")]
		[SerializeField]
		private UniqueSkillNameWindow m_UniqueSkillNameWindow;

		// Token: 0x04006732 RID: 26418
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4004891")]
		[SerializeField]
		private TotalDamageDisplay m_TotalDamageDisplay;

		// Token: 0x04006733 RID: 26419
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4004892")]
		[SerializeField]
		private TitlePlayer m_Warning;

		// Token: 0x04006734 RID: 26420
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4004893")]
		private RectTransform m_RectTransform;

		// Token: 0x04006735 RID: 26421
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x4004894")]
		private int m_SelectMasterSkillCommandIndex;

		// Token: 0x04006736 RID: 26422
		[Cpp2IlInjected.FieldOffset(Offset = "0x194")]
		[Token(Token = "0x4004895")]
		private BattleDefine.eJoinMember m_SelectMasterSkillTarget;

		// Token: 0x04006737 RID: 26423
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x4004896")]
		private List<BattleUI.CharaUIResource> m_CharaResourceList;

		// Token: 0x04006738 RID: 26424
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x4004897")]
		private BattleSystem m_System;

		// Token: 0x04006739 RID: 26425
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4004898")]
		private Camera m_MainCamera;

		// Token: 0x0400673A RID: 26426
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4004899")]
		private BattleTutorial m_Tutorial;

		// Token: 0x04006750 RID: 26448
		[Cpp2IlInjected.FieldOffset(Offset = "0x260")]
		[Token(Token = "0x40048AF")]
		private bool m_IsExectuteRetireFromDirect;

		// Token: 0x020010FE RID: 4350
		[Token(Token = "0x2001276")]
		private enum eUIState
		{
			// Token: 0x04006752 RID: 26450
			[Token(Token = "0x40071D6")]
			Wait,
			// Token: 0x04006753 RID: 26451
			[Token(Token = "0x40071D7")]
			MemberChange,
			// Token: 0x04006754 RID: 26452
			[Token(Token = "0x40071D8")]
			MasterSkill,
			// Token: 0x04006755 RID: 26453
			[Token(Token = "0x40071D9")]
			Together,
			// Token: 0x04006756 RID: 26454
			[Token(Token = "0x40071DA")]
			InterruptFriendJoinSelect,
			// Token: 0x04006757 RID: 26455
			[Token(Token = "0x40071DB")]
			Num
		}

		// Token: 0x020010FF RID: 4351
		[Token(Token = "0x2001277")]
		private class CharaUIResource
		{
			// Token: 0x0600555A RID: 21850 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006357")]
			[Address(RVA = "0x1013FDD48", Offset = "0x13FDD48", VA = "0x1013FDD48")]
			public CharaUIResource()
			{
			}

			// Token: 0x04006758 RID: 26456
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40071DC")]
			public int m_CharaID;

			// Token: 0x04006759 RID: 26457
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40071DD")]
			public SpriteHandler m_BustSpriteHandler;
		}

		// Token: 0x02001100 RID: 4352
		[Token(Token = "0x2001278")]
		public enum eDispDamageAbnormal
		{
			// Token: 0x0400675B RID: 26459
			[Token(Token = "0x40071DF")]
			isUnhappy = 1,
			// Token: 0x0400675C RID: 26460
			[Token(Token = "0x40071E0")]
			isPoison,
			// Token: 0x0400675D RID: 26461
			[Token(Token = "0x40071E1")]
			isUnhappyAndPoison
		}
	}
}
