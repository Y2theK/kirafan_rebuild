﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001101 RID: 4353
	[Token(Token = "0x2000B49")]
	[StructLayout(3)]
	public class BenchStatus : MonoBehaviour
	{
		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x0600555B RID: 21851 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000552")]
		public RectTransform CacheTransform
		{
			[Token(Token = "0x6004EB3")]
			[Address(RVA = "0x1013FDD58", Offset = "0x13FDD58", VA = "0x1013FDD58")]
			get
			{
				return null;
			}
		}

		// Token: 0x0600555C RID: 21852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB4")]
		[Address(RVA = "0x1013FDD60", Offset = "0x13FDD60", VA = "0x1013FDD60")]
		public void Setup(int idx)
		{
		}

		// Token: 0x0600555D RID: 21853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB5")]
		[Address(RVA = "0x1013FDDF0", Offset = "0x13FDDF0", VA = "0x1013FDDF0")]
		public void Destroy()
		{
		}

		// Token: 0x0600555E RID: 21854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB6")]
		[Address(RVA = "0x1013FDE24", Offset = "0x13FDE24", VA = "0x1013FDE24")]
		public void SetupForOpenMemberChange(int charaID, int lv, int maxLv, int currentHp, int maxHp, eElementType elementType, int recast, int recastMax, string nickName, string turnOwnerNickName, bool isStateAbnormalIsolation, bool executedMemberChange, bool isTurnOwnerFriend)
		{
		}

		// Token: 0x0600555F RID: 21855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB7")]
		[Address(RVA = "0x1013FE380", Offset = "0x13FE380", VA = "0x1013FE380")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06005560 RID: 21856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB8")]
		[Address(RVA = "0x1013FDDE8", Offset = "0x13FDDE8", VA = "0x1013FDDE8")]
		public void SetEnableInput(bool flag)
		{
		}

		// Token: 0x06005561 RID: 21857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EB9")]
		[Address(RVA = "0x1013FE3BC", Offset = "0x13FE3BC", VA = "0x1013FE3BC")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x06005562 RID: 21858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBA")]
		[Address(RVA = "0x1013FE65C", Offset = "0x13FE65C", VA = "0x1013FE65C")]
		public BenchStatus()
		{
		}

		// Token: 0x0400675E RID: 26462
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40048B0")]
		[SerializeField]
		protected BustImage m_BustImage;

		// Token: 0x0400675F RID: 26463
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40048B1")]
		[SerializeField]
		private Image m_Frame;

		// Token: 0x04006760 RID: 26464
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40048B2")]
		[SerializeField]
		private Sprite[] m_FrameSprites;

		// Token: 0x04006761 RID: 26465
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40048B3")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04006762 RID: 26466
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40048B4")]
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04006763 RID: 26467
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40048B5")]
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x04006764 RID: 26468
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40048B6")]
		[SerializeField]
		private Text m_LvText;

		// Token: 0x04006765 RID: 26469
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40048B7")]
		[SerializeField]
		private Image m_HPGaugeSub;

		// Token: 0x04006766 RID: 26470
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40048B8")]
		[SerializeField]
		private Image m_HPGauge;

		// Token: 0x04006767 RID: 26471
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40048B9")]
		[SerializeField]
		private Sprite m_OverHPGaugeSprite;

		// Token: 0x04006768 RID: 26472
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40048BA")]
		[SerializeField]
		private Text m_HPText;

		// Token: 0x04006769 RID: 26473
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40048BB")]
		[SerializeField]
		private MemberChange m_MemberChange;

		// Token: 0x0400676A RID: 26474
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40048BC")]
		[SerializeField]
		private GameObject m_ChangeDisable;

		// Token: 0x0400676B RID: 26475
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40048BD")]
		[SerializeField]
		private ImageNumbers m_ImageNumbersBase;

		// Token: 0x0400676C RID: 26476
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40048BE")]
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x0400676D RID: 26477
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40048BF")]
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x0400676E RID: 26478
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40048C0")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x0400676F RID: 26479
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40048C1")]
		private int m_Idx;

		// Token: 0x04006770 RID: 26480
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x40048C2")]
		private int m_RecastValue;

		// Token: 0x04006771 RID: 26481
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40048C3")]
		private int m_RecastMax;

		// Token: 0x04006772 RID: 26482
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x40048C4")]
		private bool m_IsStateAbnormalIsolation;

		// Token: 0x04006773 RID: 26483
		[Cpp2IlInjected.FieldOffset(Offset = "0xAD")]
		[Token(Token = "0x40048C5")]
		private bool m_ExecutedMemberChange;

		// Token: 0x04006774 RID: 26484
		[Cpp2IlInjected.FieldOffset(Offset = "0xAE")]
		[Token(Token = "0x40048C6")]
		private bool m_IsTurnOwnerFriend;

		// Token: 0x04006775 RID: 26485
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40048C7")]
		private string m_TurnOwnerNickName;

		// Token: 0x04006776 RID: 26486
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40048C8")]
		private RectTransform m_Transform;

		// Token: 0x04006777 RID: 26487
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40048C9")]
		private bool m_IsEnableInput;
	}
}
