﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001102 RID: 4354
	[Token(Token = "0x2000B4A")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D3D0", Offset = "0x11D3D0")]
	[StructLayout(3)]
	public class BuffIcon : MonoBehaviour
	{
		// Token: 0x06005563 RID: 21859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBB")]
		[Address(RVA = "0x1013FE664", Offset = "0x13FE664", VA = "0x1013FE664")]
		public void SetStateIconOwner(StateIconOwner owner)
		{
		}

		// Token: 0x06005564 RID: 21860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBC")]
		[Address(RVA = "0x1013FE66C", Offset = "0x13FE66C", VA = "0x1013FE66C")]
		public void Setup()
		{
		}

		// Token: 0x06005565 RID: 21861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBD")]
		[Address(RVA = "0x1013FE874", Offset = "0x13FE874", VA = "0x1013FE874")]
		public void Clear()
		{
		}

		// Token: 0x06005566 RID: 21862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBE")]
		[Address(RVA = "0x1013FE6F0", Offset = "0x13FE6F0", VA = "0x1013FE6F0")]
		public void ClearImmidiate()
		{
		}

		// Token: 0x06005567 RID: 21863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EBF")]
		[Address(RVA = "0x1013FE93C", Offset = "0x13FE93C", VA = "0x1013FE93C")]
		public void SetIcon(eStateIconType type, bool flg)
		{
		}

		// Token: 0x06005568 RID: 21864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC0")]
		[Address(RVA = "0x1013FE9A4", Offset = "0x13FE9A4", VA = "0x1013FE9A4")]
		public void ResetIcon()
		{
		}

		// Token: 0x06005569 RID: 21865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC1")]
		[Address(RVA = "0x1013FEAE8", Offset = "0x13FEAE8", VA = "0x1013FEAE8")]
		private void LateUpdate()
		{
		}

		// Token: 0x0600556A RID: 21866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC2")]
		[Address(RVA = "0x1013FEB14", Offset = "0x13FEB14", VA = "0x1013FEB14")]
		public void Apply()
		{
		}

		// Token: 0x0600556B RID: 21867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC3")]
		[Address(RVA = "0x1013FF310", Offset = "0x13FF310", VA = "0x1013FF310")]
		private void PlayIn()
		{
		}

		// Token: 0x0600556C RID: 21868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC4")]
		[Address(RVA = "0x1013FEFC0", Offset = "0x13FEFC0", VA = "0x1013FEFC0")]
		private void PlayOut()
		{
		}

		// Token: 0x0600556D RID: 21869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC5")]
		[Address(RVA = "0x1013FF664", Offset = "0x13FF664", VA = "0x1013FF664")]
		private void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x0600556E RID: 21870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC6")]
		[Address(RVA = "0x1013FECB4", Offset = "0x13FECB4", VA = "0x1013FECB4")]
		private void ChangeStateIcon()
		{
		}

		// Token: 0x0600556F RID: 21871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC7")]
		[Address(RVA = "0x1013FFA3C", Offset = "0x13FFA3C", VA = "0x1013FFA3C")]
		public BuffIcon()
		{
		}

		// Token: 0x04006778 RID: 26488
		[Token(Token = "0x40048CA")]
		private const float TRANSIT_SEC = 0.2f;

		// Token: 0x04006779 RID: 26489
		[Token(Token = "0x40048CB")]
		private const float DISP_SEC = 1f;

		// Token: 0x0400677A RID: 26490
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40048CC")]
		[SerializeField]
		private StateIconOwner m_IconOwner;

		// Token: 0x0400677B RID: 26491
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40048CD")]
		private StateIcon m_CurrentStateIcon;

		// Token: 0x0400677C RID: 26492
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40048CE")]
		private bool[] m_Buffs;

		// Token: 0x0400677D RID: 26493
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40048CF")]
		private eStateIconType m_CurrentBuff;

		// Token: 0x0400677E RID: 26494
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40048D0")]
		private int m_BuffNum;

		// Token: 0x0400677F RID: 26495
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40048D1")]
		private bool m_IsDirty;

		// Token: 0x04006780 RID: 26496
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40048D2")]
		private GameObject m_GameObject;

		// Token: 0x04006781 RID: 26497
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40048D3")]
		private RectTransform m_RectTransform;

		// Token: 0x04006782 RID: 26498
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40048D4")]
		private CanvasGroup m_CanvasGroup;
	}
}
