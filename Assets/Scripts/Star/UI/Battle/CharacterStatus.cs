﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001103 RID: 4355
	[Token(Token = "0x2000B4B")]
	[StructLayout(3)]
	public class CharacterStatus : MonoBehaviour
	{
		// Token: 0x06005570 RID: 21872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC8")]
		[Address(RVA = "0x1013FFA9C", Offset = "0x13FFA9C", VA = "0x1013FFA9C")]
		public void Setup()
		{
		}

		// Token: 0x06005571 RID: 21873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EC9")]
		[Address(RVA = "0x1013FFDFC", Offset = "0x13FFDFC", VA = "0x1013FFDFC")]
		private void Update()
		{
		}

		// Token: 0x06005572 RID: 21874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECA")]
		[Address(RVA = "0x101400228", Offset = "0x1400228", VA = "0x101400228")]
		private void UpdateHPBar(float currentHP)
		{
		}

		// Token: 0x06005573 RID: 21875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECB")]
		[Address(RVA = "0x1014003E4", Offset = "0x14003E4", VA = "0x1014003E4")]
		public void SetHp(int hp, int maxhp, bool isMoveBar = false)
		{
		}

		// Token: 0x06005574 RID: 21876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECC")]
		[Address(RVA = "0x101400554", Offset = "0x1400554", VA = "0x101400554")]
		public void SetStun(float ratio, bool isMoveBar = false)
		{
		}

		// Token: 0x06005575 RID: 21877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECD")]
		[Address(RVA = "0x101400654", Offset = "0x1400654", VA = "0x101400654")]
		public void SetCharge(int charge, int max)
		{
		}

		// Token: 0x06005576 RID: 21878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECE")]
		[Address(RVA = "0x101400A38", Offset = "0x1400A38", VA = "0x101400A38")]
		public void SetElementType(eElementType element)
		{
		}

		// Token: 0x06005577 RID: 21879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ECF")]
		[Address(RVA = "0x101400B38", Offset = "0x1400B38", VA = "0x101400B38")]
		public void ChangeElement(eElementType before, eElementType after)
		{
		}

		// Token: 0x06005578 RID: 21880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED0")]
		[Address(RVA = "0x101400BF0", Offset = "0x1400BF0", VA = "0x101400BF0")]
		public void SetIdentityIcon(int idx)
		{
		}

		// Token: 0x06005579 RID: 21881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED1")]
		[Address(RVA = "0x101400CD4", Offset = "0x1400CD4", VA = "0x101400CD4")]
		public void SetAbilityPassiveIcon(int num)
		{
		}

		// Token: 0x0600557A RID: 21882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED2")]
		[Address(RVA = "0x101400D1C", Offset = "0x1400D1C", VA = "0x101400D1C")]
		public void SetPassiveIcon(int num)
		{
		}

		// Token: 0x0600557B RID: 21883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED3")]
		[Address(RVA = "0x101400D64", Offset = "0x1400D64", VA = "0x101400D64")]
		public void SetIcons(bool[] flags)
		{
		}

		// Token: 0x0600557C RID: 21884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED4")]
		[Address(RVA = "0x1013FFC34", Offset = "0x13FFC34", VA = "0x1013FFC34")]
		public void SetEnableRender(bool flag, bool immediate = false)
		{
		}

		// Token: 0x0600557D RID: 21885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED5")]
		[Address(RVA = "0x101400EAC", Offset = "0x1400EAC", VA = "0x101400EAC")]
		public void SetAnchoredPosition(Vector2 position)
		{
		}

		// Token: 0x0600557E RID: 21886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED6")]
		[Address(RVA = "0x101400EF4", Offset = "0x1400EF4", VA = "0x101400EF4")]
		public CharacterStatus()
		{
		}

		// Token: 0x04006783 RID: 26499
		[Token(Token = "0x40048D5")]
		private const float HPSPEED = 0.05f;

		// Token: 0x04006784 RID: 26500
		[Token(Token = "0x40048D6")]
		private const float BGHPSPEED = 0.02f;

		// Token: 0x04006785 RID: 26501
		[Token(Token = "0x40048D7")]
		private const float STUNSPEED = 0.05f;

		// Token: 0x04006786 RID: 26502
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40048D8")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04006787 RID: 26503
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40048D9")]
		[SerializeField]
		private BattleElementChange m_ElementChange;

		// Token: 0x04006788 RID: 26504
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40048DA")]
		[SerializeField]
		private Image m_HPGaugeDamage;

		// Token: 0x04006789 RID: 26505
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40048DB")]
		[SerializeField]
		private Image m_HPGaugeSub;

		// Token: 0x0400678A RID: 26506
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40048DC")]
		[SerializeField]
		private Image m_HPGaugeBody;

		// Token: 0x0400678B RID: 26507
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40048DD")]
		[SerializeField]
		private Sprite m_OverHPGaugeSprite;

		// Token: 0x0400678C RID: 26508
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40048DE")]
		[SerializeField]
		private Text m_HPText;

		// Token: 0x0400678D RID: 26509
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40048DF")]
		[SerializeField]
		private GameObject m_ChargeParent;

		// Token: 0x0400678E RID: 26510
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40048E0")]
		private ChargeCountSignal[] m_ChargeCounts;

		// Token: 0x0400678F RID: 26511
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40048E1")]
		[SerializeField]
		private Image m_StunGauge;

		// Token: 0x04006790 RID: 26512
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40048E2")]
		[SerializeField]
		private AnimUIPlayer m_StunEffectAnim;

		// Token: 0x04006791 RID: 26513
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40048E3")]
		[SerializeField]
		private BlinkColor m_StunBlinkColor;

		// Token: 0x04006792 RID: 26514
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40048E4")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131834", Offset = "0x131834")]
		private Image m_EnemyIdentity;

		// Token: 0x04006793 RID: 26515
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40048E5")]
		[SerializeField]
		private Sprite[] m_IdentitySprites;

		// Token: 0x04006794 RID: 26516
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40048E6")]
		[SerializeField]
		private MultiBuffDisplay m_BuffDisp;

		// Token: 0x04006795 RID: 26517
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40048E7")]
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04006796 RID: 26518
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40048E8")]
		private float m_CurrentDispHP;

		// Token: 0x04006797 RID: 26519
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x40048E9")]
		private float m_HP;

		// Token: 0x04006798 RID: 26520
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40048EA")]
		private bool m_IsDirtyHP;

		// Token: 0x04006799 RID: 26521
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x40048EB")]
		private float m_Stun;

		// Token: 0x0400679A RID: 26522
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40048EC")]
		private bool m_IsDirtyStun;

		// Token: 0x0400679B RID: 26523
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40048ED")]
		private GameObject m_GameObject;

		// Token: 0x0400679C RID: 26524
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40048EE")]
		private RectTransform m_RectTransform;
	}
}
