﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001104 RID: 4356
	[Token(Token = "0x2000B4C")]
	[StructLayout(3)]
	public class ChargeCountSignal : MonoBehaviour
	{
		// Token: 0x0600557F RID: 21887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED7")]
		[Address(RVA = "0x101400808", Offset = "0x1400808", VA = "0x101400808")]
		public void Apply(ChargeCountSignal.eState state)
		{
		}

		// Token: 0x06005580 RID: 21888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ED8")]
		[Address(RVA = "0x101400F10", Offset = "0x1400F10", VA = "0x101400F10")]
		public ChargeCountSignal()
		{
		}

		// Token: 0x0400679D RID: 26525
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40048EF")]
		[SerializeField]
		private GameObject m_Empty;

		// Token: 0x0400679E RID: 26526
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40048F0")]
		[SerializeField]
		private GameObject m_Active;

		// Token: 0x0400679F RID: 26527
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40048F1")]
		[SerializeField]
		private Image m_EffectImage;

		// Token: 0x02001105 RID: 4357
		[Token(Token = "0x2001279")]
		public enum eState
		{
			// Token: 0x040067A1 RID: 26529
			[Token(Token = "0x40071E3")]
			None,
			// Token: 0x040067A2 RID: 26530
			[Token(Token = "0x40071E4")]
			Empty,
			// Token: 0x040067A3 RID: 26531
			[Token(Token = "0x40071E5")]
			Active,
			// Token: 0x040067A4 RID: 26532
			[Token(Token = "0x40071E6")]
			Effect
		}
	}
}
