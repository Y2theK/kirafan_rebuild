﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001106 RID: 4358
	[Token(Token = "0x2000B4D")]
	[StructLayout(3)]
	public class CommandButton : MonoBehaviour
	{
		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x06005581 RID: 21889 RVA: 0x0001CAB8 File Offset: 0x0001ACB8
		[Token(Token = "0x17000553")]
		public int CommandIndex
		{
			[Token(Token = "0x6004ED9")]
			[Address(RVA = "0x101400F18", Offset = "0x1400F18", VA = "0x101400F18")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x06005582 RID: 21890 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000554")]
		public RectTransform CacheTransform
		{
			[Token(Token = "0x6004EDA")]
			[Address(RVA = "0x101400F20", Offset = "0x1400F20", VA = "0x101400F20")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x06005583 RID: 21891 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000555")]
		public AnimUIPlayer Anim
		{
			[Token(Token = "0x6004EDB")]
			[Address(RVA = "0x101400F28", Offset = "0x1400F28", VA = "0x101400F28")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x06005584 RID: 21892 RVA: 0x0001CAD0 File Offset: 0x0001ACD0
		[Token(Token = "0x17000556")]
		public bool IsEnableInput
		{
			[Token(Token = "0x6004EDC")]
			[Address(RVA = "0x101400F30", Offset = "0x1400F30", VA = "0x101400F30")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x06005585 RID: 21893 RVA: 0x0001CAE8 File Offset: 0x0001ACE8
		// (set) Token: 0x06005586 RID: 21894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000557")]
		private bool IsEnableBlink
		{
			[Token(Token = "0x6004EDD")]
			[Address(RVA = "0x101400F38", Offset = "0x1400F38", VA = "0x101400F38")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004EDE")]
			[Address(RVA = "0x101400F40", Offset = "0x1400F40", VA = "0x101400F40")]
			set
			{
			}
		}

		// Token: 0x14000139 RID: 313
		// (add) Token: 0x06005587 RID: 21895 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005588 RID: 21896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000139")]
		public event Action<int, bool, bool> OnClickDecideButton
		{
			[Token(Token = "0x6004EDF")]
			[Address(RVA = "0x101401024", Offset = "0x1401024", VA = "0x101401024")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004EE0")]
			[Address(RVA = "0x101401130", Offset = "0x1401130", VA = "0x101401130")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06005589 RID: 21897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE1")]
		[Address(RVA = "0x10140123C", Offset = "0x140123C", VA = "0x10140123C")]
		public void Setup(CommandSet owner, BattleTutorial tutorial)
		{
		}

		// Token: 0x0600558A RID: 21898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE2")]
		[Address(RVA = "0x1014014FC", Offset = "0x14014FC", VA = "0x1014014FC")]
		public void SetEnableRenderFlickArrowLeft(bool flg)
		{
		}

		// Token: 0x0600558B RID: 21899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE3")]
		[Address(RVA = "0x101401574", Offset = "0x1401574", VA = "0x101401574")]
		public void SetEnableRenderFlickArrowRight(bool flg)
		{
		}

		// Token: 0x0600558C RID: 21900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE4")]
		[Address(RVA = "0x1014015EC", Offset = "0x14015EC", VA = "0x1014015EC")]
		private void Update()
		{
		}

		// Token: 0x0600558D RID: 21901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE5")]
		[Address(RVA = "0x1014019D4", Offset = "0x14019D4", VA = "0x1014019D4")]
		public void UpdateStatus()
		{
		}

		// Token: 0x0600558E RID: 21902 RVA: 0x0001CB00 File Offset: 0x0001AD00
		[Token(Token = "0x6004EE6")]
		[Address(RVA = "0x101401A94", Offset = "0x1401A94", VA = "0x101401A94")]
		public bool IsReadyApplyReserve()
		{
			return default(bool);
		}

		// Token: 0x0600558F RID: 21903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE7")]
		[Address(RVA = "0x101401B08", Offset = "0x1401B08", VA = "0x101401B08")]
		public void ApplyReserve()
		{
		}

		// Token: 0x06005590 RID: 21904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE8")]
		[Address(RVA = "0x101402604", Offset = "0x1402604", VA = "0x101402604")]
		public void PlayRecoverAnim()
		{
		}

		// Token: 0x06005591 RID: 21905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EE9")]
		[Address(RVA = "0x101402660", Offset = "0x1402660", VA = "0x101402660")]
		public void SetCommandIndex(int commandIndex)
		{
		}

		// Token: 0x06005592 RID: 21906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EEA")]
		[Address(RVA = "0x101402668", Offset = "0x1402668", VA = "0x101402668")]
		public void Hide()
		{
		}

		// Token: 0x06005593 RID: 21907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EEB")]
		[Address(RVA = "0x1014026F8", Offset = "0x14026F8", VA = "0x1014026F8")]
		public void SetupBlank()
		{
		}

		// Token: 0x06005594 RID: 21908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EEC")]
		[Address(RVA = "0x101402A98", Offset = "0x1402A98", VA = "0x101402A98")]
		public void SetupNormalAttack(eSkillType type, List<eElementType> elements)
		{
		}

		// Token: 0x06005595 RID: 21909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EED")]
		[Address(RVA = "0x101402AA8", Offset = "0x1402AA8", VA = "0x101402AA8")]
		public void SetupSkill(eSkillType type, bool isCharacterSkill, List<eElementType> elements, eClassType classType, bool isSilence)
		{
		}

		// Token: 0x06005596 RID: 21910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EEE")]
		[Address(RVA = "0x101402E14", Offset = "0x1402E14", VA = "0x101402E14")]
		public void PlayOut()
		{
		}

		// Token: 0x06005597 RID: 21911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EEF")]
		[Address(RVA = "0x101402EC4", Offset = "0x1402EC4", VA = "0x101402EC4")]
		public void OnPointerDownCallBack(BaseEventData eventData)
		{
		}

		// Token: 0x06005598 RID: 21912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF0")]
		[Address(RVA = "0x10140335C", Offset = "0x140335C", VA = "0x10140335C")]
		public void OnPointerClickCallBack()
		{
		}

		// Token: 0x06005599 RID: 21913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF1")]
		[Address(RVA = "0x10140341C", Offset = "0x140341C", VA = "0x10140341C")]
		public void OnPointerEnterCallBack(BaseEventData eventData)
		{
		}

		// Token: 0x0600559A RID: 21914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF2")]
		[Address(RVA = "0x10140368C", Offset = "0x140368C", VA = "0x10140368C")]
		public void OnPointerUpCallBack(BaseEventData eventData)
		{
		}

		// Token: 0x0600559B RID: 21915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF3")]
		[Address(RVA = "0x101403690", Offset = "0x1403690", VA = "0x101403690")]
		public void OnPointerExitCallBack(BaseEventData eventData)
		{
		}

		// Token: 0x0600559C RID: 21916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF4")]
		[Address(RVA = "0x1014030F4", Offset = "0x14030F4", VA = "0x1014030F4")]
		public void ScaleUp()
		{
		}

		// Token: 0x0600559D RID: 21917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF5")]
		[Address(RVA = "0x101401774", Offset = "0x1401774", VA = "0x101401774")]
		public void ScaleRevert()
		{
		}

		// Token: 0x0600559E RID: 21918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF6")]
		[Address(RVA = "0x101402674", Offset = "0x1402674", VA = "0x101402674")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x0600559F RID: 21919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF7")]
		[Address(RVA = "0x1014029C8", Offset = "0x14029C8", VA = "0x1014029C8")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x060055A0 RID: 21920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF8")]
		[Address(RVA = "0x10140173C", Offset = "0x140173C", VA = "0x10140173C")]
		public void SetInteractable(bool flg)
		{
		}

		// Token: 0x060055A1 RID: 21921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EF9")]
		[Address(RVA = "0x10140224C", Offset = "0x140224C", VA = "0x10140224C")]
		public void SetSilence(bool flg)
		{
		}

		// Token: 0x060055A2 RID: 21922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EFA")]
		[Address(RVA = "0x101402A44", Offset = "0x1402A44", VA = "0x101402A44")]
		public void SetMode(CommandButton.eMode mode)
		{
		}

		// Token: 0x060055A3 RID: 21923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EFB")]
		[Address(RVA = "0x101400F48", Offset = "0x1400F48", VA = "0x101400F48")]
		private void UpdateAnimState()
		{
		}

		// Token: 0x060055A4 RID: 21924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EFC")]
		[Address(RVA = "0x101402A18", Offset = "0x1402A18", VA = "0x101402A18")]
		public void SetRecastGauge(int value, int max, bool recastRecoverdFlag, [Optional] CharacterBattle charaBattle)
		{
		}

		// Token: 0x060055A5 RID: 21925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004EFD")]
		[Address(RVA = "0x101403694", Offset = "0x1403694", VA = "0x101403694")]
		public CommandButton()
		{
		}

		// Token: 0x040067A5 RID: 26533
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40048F2")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040067A6 RID: 26534
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40048F3")]
		[SerializeField]
		private Image m_ButtonImage;

		// Token: 0x040067A7 RID: 26535
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40048F4")]
		[SerializeField]
		private GameObject m_BlankObj;

		// Token: 0x040067A8 RID: 26536
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40048F5")]
		[SerializeField]
		private GameObject m_Recast;

		// Token: 0x040067A9 RID: 26537
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40048F6")]
		[SerializeField]
		private RecastGauge m_RecastGauge;

		// Token: 0x040067AA RID: 26538
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40048F7")]
		[SerializeField]
		private GameObject m_SilenceImageObj;

		// Token: 0x040067AB RID: 26539
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40048F8")]
		[SerializeField]
		private GameObject[] m_ElementIconObjs;

		// Token: 0x040067AC RID: 26540
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40048F9")]
		[SerializeField]
		private GameObject[] m_ElementIconBaseObjs;

		// Token: 0x040067AD RID: 26541
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40048FA")]
		[SerializeField]
		private ElementIcon[] m_ElementIcons;

		// Token: 0x040067AE RID: 26542
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40048FB")]
		[SerializeField]
		private GameObject m_SelectAnimObj;

		// Token: 0x040067AF RID: 26543
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40048FC")]
		[SerializeField]
		private GameObject m_DecidedAnimObj;

		// Token: 0x040067B0 RID: 26544
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40048FD")]
		[SerializeField]
		private GameObject m_RecoverAnimObj;

		// Token: 0x040067B1 RID: 26545
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40048FE")]
		[SerializeField]
		private GameObject m_SelectScaleObj;

		// Token: 0x040067B2 RID: 26546
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40048FF")]
		[SerializeField]
		private GameObject m_PressObj;

		// Token: 0x040067B3 RID: 26547
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004900")]
		[SerializeField]
		private GameObject m_FlickArrowLeft;

		// Token: 0x040067B4 RID: 26548
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004901")]
		[SerializeField]
		private GameObject m_FlickArrowRight;

		// Token: 0x040067B5 RID: 26549
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004902")]
		private int m_CommandIndex;

		// Token: 0x040067B6 RID: 26550
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4004903")]
		private bool m_IsEnableInput;

		// Token: 0x040067B7 RID: 26551
		[Cpp2IlInjected.FieldOffset(Offset = "0x9D")]
		[Token(Token = "0x4004904")]
		private bool m_IsInteractable;

		// Token: 0x040067B8 RID: 26552
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004905")]
		private CommandButton.eMode m_Mode;

		// Token: 0x040067B9 RID: 26553
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4004906")]
		private bool m_IsNone;

		// Token: 0x040067BA RID: 26554
		[Cpp2IlInjected.FieldOffset(Offset = "0xA5")]
		[Token(Token = "0x4004907")]
		private bool m_IsRecast;

		// Token: 0x040067BB RID: 26555
		[Cpp2IlInjected.FieldOffset(Offset = "0xA6")]
		[Token(Token = "0x4004908")]
		private bool m_IsEnableBlink;

		// Token: 0x040067BC RID: 26556
		[Cpp2IlInjected.FieldOffset(Offset = "0xA7")]
		[Token(Token = "0x4004909")]
		private bool m_IsSilence;

		// Token: 0x040067BD RID: 26557
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400490A")]
		private bool m_IsEnableDecide;

		// Token: 0x040067BE RID: 26558
		[Cpp2IlInjected.FieldOffset(Offset = "0xA9")]
		[Token(Token = "0x400490B")]
		private bool m_IsScaleUp;

		// Token: 0x040067BF RID: 26559
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400490C")]
		private CommandSet m_Owner;

		// Token: 0x040067C0 RID: 26560
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400490D")]
		private GameObject m_GameObject;

		// Token: 0x040067C1 RID: 26561
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400490E")]
		private RectTransform m_RectTransform;

		// Token: 0x040067C2 RID: 26562
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400490F")]
		private AnimUIPlayer m_Anim;

		// Token: 0x040067C3 RID: 26563
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004910")]
		private ColorGroup m_ButtonImageColorGroup;

		// Token: 0x040067C4 RID: 26564
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004911")]
		private CommandButton.Status m_ReserveStatus;

		// Token: 0x040067C5 RID: 26565
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004912")]
		private BattleTutorial m_Tutorial;

		// Token: 0x02001107 RID: 4359
		[Token(Token = "0x200127A")]
		public enum eMode
		{
			// Token: 0x040067C8 RID: 26568
			[Token(Token = "0x40071E8")]
			None,
			// Token: 0x040067C9 RID: 26569
			[Token(Token = "0x40071E9")]
			Select,
			// Token: 0x040067CA RID: 26570
			[Token(Token = "0x40071EA")]
			Decide
		}

		// Token: 0x02001108 RID: 4360
		[Token(Token = "0x200127B")]
		private class Status
		{
			// Token: 0x060055A6 RID: 21926 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006358")]
			[Address(RVA = "0x101402A10", Offset = "0x1402A10", VA = "0x101402A10")]
			public Status()
			{
			}

			// Token: 0x040067CB RID: 26571
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40071EB")]
			public bool m_Display;

			// Token: 0x040067CC RID: 26572
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40071EC")]
			public Sprite m_Sprite;

			// Token: 0x040067CD RID: 26573
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40071ED")]
			public bool m_ColorChange;

			// Token: 0x040067CE RID: 26574
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40071EE")]
			public List<eElementType> m_Elements;

			// Token: 0x040067CF RID: 26575
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40071EF")]
			public bool m_IsSilence;

			// Token: 0x040067D0 RID: 26576
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x40071F0")]
			public CharacterBattle m_CharaBattle;

			// Token: 0x040067D1 RID: 26577
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x40071F1")]
			public int m_RecastValue;

			// Token: 0x040067D2 RID: 26578
			[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
			[Token(Token = "0x40071F2")]
			public int m_RecastMax;

			// Token: 0x040067D3 RID: 26579
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x40071F3")]
			public bool m_RecastRecover;
		}
	}
}
