﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001109 RID: 4361
	[Token(Token = "0x2000B4E")]
	[StructLayout(3)]
	public class CommandButtonLevelUp : MonoBehaviour
	{
		// Token: 0x1400013A RID: 314
		// (add) Token: 0x060055A7 RID: 21927 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060055A8 RID: 21928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013A")]
		public event Action OnEndAnimation
		{
			[Token(Token = "0x6004EFE")]
			[Address(RVA = "0x1014036A4", Offset = "0x14036A4", VA = "0x1014036A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004EFF")]
			[Address(RVA = "0x1014037B0", Offset = "0x14037B0", VA = "0x1014037B0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x060055A9 RID: 21929 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000558")]
		public Animation Animation
		{
			[Token(Token = "0x6004F00")]
			[Address(RVA = "0x1014038BC", Offset = "0x14038BC", VA = "0x1014038BC")]
			get
			{
				return null;
			}
		}

		// Token: 0x060055AA RID: 21930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F01")]
		[Address(RVA = "0x101403954", Offset = "0x1403954", VA = "0x101403954")]
		private void Update()
		{
		}

		// Token: 0x060055AB RID: 21931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F02")]
		[Address(RVA = "0x1014039CC", Offset = "0x14039CC", VA = "0x1014039CC")]
		public void Play()
		{
		}

		// Token: 0x060055AC RID: 21932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F03")]
		[Address(RVA = "0x101403AA4", Offset = "0x1403AA4", VA = "0x101403AA4")]
		public CommandButtonLevelUp()
		{
		}

		// Token: 0x040067D5 RID: 26581
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004915")]
		private Animation m_Animation;
	}
}
