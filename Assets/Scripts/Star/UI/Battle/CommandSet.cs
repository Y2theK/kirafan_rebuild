﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200110A RID: 4362
	[Token(Token = "0x2000B4F")]
	[StructLayout(3)]
	public class CommandSet : MonoBehaviour
	{
		// Token: 0x060055AD RID: 21933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F04")]
		[Address(RVA = "0x101403AAC", Offset = "0x1403AAC", VA = "0x101403AAC")]
		public void Setup(BattleTutorial tutorial)
		{
		}

		// Token: 0x060055AE RID: 21934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F05")]
		[Address(RVA = "0x101403DCC", Offset = "0x1403DCC", VA = "0x101403DCC")]
		private void Update()
		{
		}

		// Token: 0x060055AF RID: 21935 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F06")]
		[Address(RVA = "0x10140413C", Offset = "0x140413C", VA = "0x10140413C")]
		public CommandButton GetButton(int index)
		{
			return null;
		}

		// Token: 0x060055B0 RID: 21936 RVA: 0x0001CB18 File Offset: 0x0001AD18
		[Token(Token = "0x6004F07")]
		[Address(RVA = "0x10140418C", Offset = "0x140418C", VA = "0x10140418C")]
		public int GetButtonNum()
		{
			return 0;
		}

		// Token: 0x060055B1 RID: 21937 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F08")]
		[Address(RVA = "0x101402D70", Offset = "0x1402D70", VA = "0x101402D70")]
		public Sprite GetAttackSprite()
		{
			return null;
		}

		// Token: 0x060055B2 RID: 21938 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F09")]
		[Address(RVA = "0x101402D78", Offset = "0x1402D78", VA = "0x101402D78")]
		public Sprite GetMagicAttackSprite()
		{
			return null;
		}

		// Token: 0x060055B3 RID: 21939 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F0A")]
		[Address(RVA = "0x101402D80", Offset = "0x1402D80", VA = "0x101402D80")]
		public Sprite GetSkillSprite(eSkillType skillType, bool characterSkill)
		{
			return null;
		}

		// Token: 0x060055B4 RID: 21940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F0B")]
		[Address(RVA = "0x1014041B8", Offset = "0x14041B8", VA = "0x1014041B8")]
		public void SetEnableInput(bool flag)
		{
		}

		// Token: 0x060055B5 RID: 21941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F0C")]
		[Address(RVA = "0x101404274", Offset = "0x1404274", VA = "0x101404274")]
		public void PlayIn()
		{
		}

		// Token: 0x060055B6 RID: 21942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F0D")]
		[Address(RVA = "0x10140430C", Offset = "0x140430C", VA = "0x10140430C")]
		public void PlayOut()
		{
		}

		// Token: 0x060055B7 RID: 21943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F0E")]
		[Address(RVA = "0x101402654", Offset = "0x1402654", VA = "0x101402654")]
		public void RequestRecastRecoverSound()
		{
		}

		// Token: 0x060055B8 RID: 21944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F0F")]
		[Address(RVA = "0x1014023CC", Offset = "0x14023CC", VA = "0x1014023CC")]
		public void SetRecastSaveData(CharacterBattle charaBattle, int idx, int recast)
		{
		}

		// Token: 0x060055B9 RID: 21945 RVA: 0x0001CB30 File Offset: 0x0001AD30
		[Token(Token = "0x6004F10")]
		[Address(RVA = "0x101402288", Offset = "0x1402288", VA = "0x101402288")]
		public int GetRecastSaveData(CharacterBattle charaBattle, int idx)
		{
			return 0;
		}

		// Token: 0x060055BA RID: 21946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F11")]
		[Address(RVA = "0x101404404", Offset = "0x1404404", VA = "0x101404404")]
		public void ClearRecastSaveData()
		{
		}

		// Token: 0x060055BB RID: 21947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F12")]
		[Address(RVA = "0x101404464", Offset = "0x1404464", VA = "0x101404464")]
		public void ShiftDown(bool flg, bool immediate = false)
		{
		}

		// Token: 0x060055BC RID: 21948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F13")]
		[Address(RVA = "0x101404B0C", Offset = "0x1404B0C", VA = "0x101404B0C")]
		public CommandSet()
		{
		}

		// Token: 0x040067D6 RID: 26582
		[Token(Token = "0x4004916")]
		public const int COMMAND_MAX = 4;

		// Token: 0x040067D7 RID: 26583
		[Token(Token = "0x4004917")]
		private const float SHIFT_MOVE = 180f;

		// Token: 0x040067D8 RID: 26584
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004918")]
		[SerializeField]
		private float m_CommandSpace;

		// Token: 0x040067D9 RID: 26585
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004919")]
		[SerializeField]
		private Sprite m_SpriteNormalAttack;

		// Token: 0x040067DA RID: 26586
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400491A")]
		[SerializeField]
		private Sprite m_SpriteNormalMagicAttack;

		// Token: 0x040067DB RID: 26587
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400491B")]
		[SerializeField]
		private Sprite m_SpriteSkillAttack;

		// Token: 0x040067DC RID: 26588
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400491C")]
		[SerializeField]
		private Sprite m_SpriteSkillMagic;

		// Token: 0x040067DD RID: 26589
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400491D")]
		[SerializeField]
		private Sprite m_SpriteSkillRecovery;

		// Token: 0x040067DE RID: 26590
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400491E")]
		[SerializeField]
		private Sprite m_SpriteSkillBuff;

		// Token: 0x040067DF RID: 26591
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400491F")]
		[SerializeField]
		private Sprite m_SpriteSkillDebuff;

		// Token: 0x040067E0 RID: 26592
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004920")]
		[SerializeField]
		private Sprite m_SpriteSkillOther;

		// Token: 0x040067E1 RID: 26593
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004921")]
		[SerializeField]
		private Sprite m_SpriteSkillCharacterAttack;

		// Token: 0x040067E2 RID: 26594
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004922")]
		[SerializeField]
		private Sprite m_SpriteSkillCharacterRecovery;

		// Token: 0x040067E3 RID: 26595
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004923")]
		[SerializeField]
		private Sprite m_SpriteSkillCharacterBuff;

		// Token: 0x040067E4 RID: 26596
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004924")]
		[SerializeField]
		private CommandButton m_CommandButtonPrefab;

		// Token: 0x040067E5 RID: 26597
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004925")]
		private CommandButton[] m_CommandButtons;

		// Token: 0x040067E6 RID: 26598
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004926")]
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x040067E7 RID: 26599
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004927")]
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x040067E8 RID: 26600
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004928")]
		[SerializeField]
		private AnimUIPlayer[] m_Anim;

		// Token: 0x040067E9 RID: 26601
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004929")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131B00", Offset = "0x131B00")]
		private GameObject m_ShiftMoveObj;

		// Token: 0x040067EA RID: 26602
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400492A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100131B4C", Offset = "0x131B4C")]
		private AnimUIPlayer[] m_ShiftAnims;

		// Token: 0x040067EB RID: 26603
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400492B")]
		private bool m_RequestRecastSE;

		// Token: 0x040067EC RID: 26604
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400492C")]
		private List<CommandSet.RecastData> m_RecastDataList;

		// Token: 0x0200110B RID: 4363
		[Token(Token = "0x200127C")]
		private class RecastData
		{
			// Token: 0x060055BD RID: 21949 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006359")]
			[Address(RVA = "0x1014043A4", Offset = "0x14043A4", VA = "0x1014043A4")]
			public RecastData()
			{
			}

			// Token: 0x040067ED RID: 26605
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40071F4")]
			public CharacterBattle m_CharaBattle;

			// Token: 0x040067EE RID: 26606
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40071F5")]
			public int[] m_Recasts;
		}
	}
}
