﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200110C RID: 4364
	[Token(Token = "0x2000B50")]
	[StructLayout(3)]
	public class DamageDescript : MonoBehaviour
	{
		// Token: 0x060055BE RID: 21950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F14")]
		[Address(RVA = "0x101404B7C", Offset = "0x1404B7C", VA = "0x101404B7C")]
		public void Apply(DamageDescript.eType type)
		{
		}

		// Token: 0x060055BF RID: 21951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F15")]
		[Address(RVA = "0x101404C90", Offset = "0x1404C90", VA = "0x101404C90")]
		public DamageDescript()
		{
		}

		// Token: 0x040067EF RID: 26607
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400492D")]
		[SerializeField]
		private Sprite m_SpriteCritical;

		// Token: 0x040067F0 RID: 26608
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400492E")]
		[SerializeField]
		private Sprite m_SpriteResist;

		// Token: 0x040067F1 RID: 26609
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400492F")]
		[SerializeField]
		private Sprite m_SpriteWeak;

		// Token: 0x040067F2 RID: 26610
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004930")]
		[SerializeField]
		private Sprite m_SpriteRecovery;

		// Token: 0x040067F3 RID: 26611
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004931")]
		[SerializeField]
		private Sprite m_SpriteUnhappy;

		// Token: 0x040067F4 RID: 26612
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004932")]
		[SerializeField]
		private Sprite m_SpritePoison;

		// Token: 0x040067F5 RID: 26613
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004933")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x0200110D RID: 4365
		[Token(Token = "0x200127D")]
		public enum eType
		{
			// Token: 0x040067F7 RID: 26615
			[Token(Token = "0x40071F7")]
			None,
			// Token: 0x040067F8 RID: 26616
			[Token(Token = "0x40071F8")]
			Critical,
			// Token: 0x040067F9 RID: 26617
			[Token(Token = "0x40071F9")]
			Resist,
			// Token: 0x040067FA RID: 26618
			[Token(Token = "0x40071FA")]
			Weak,
			// Token: 0x040067FB RID: 26619
			[Token(Token = "0x40071FB")]
			Recovery,
			// Token: 0x040067FC RID: 26620
			[Token(Token = "0x40071FC")]
			Unhappy,
			// Token: 0x040067FD RID: 26621
			[Token(Token = "0x40071FD")]
			Poison
		}
	}
}
