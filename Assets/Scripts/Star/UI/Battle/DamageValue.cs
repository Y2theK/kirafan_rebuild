﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200110E RID: 4366
	[Token(Token = "0x2000B51")]
	[StructLayout(3)]
	public class DamageValue : MonoBehaviour
	{
		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x060055C0 RID: 21952 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000559")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004F16")]
			[Address(RVA = "0x101404C98", Offset = "0x1404C98", VA = "0x101404C98")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x060055C1 RID: 21953 RVA: 0x0001CB48 File Offset: 0x0001AD48
		[Token(Token = "0x1700055A")]
		public Vector3 TargetPosition
		{
			[Token(Token = "0x6004F17")]
			[Address(RVA = "0x101404CA0", Offset = "0x1404CA0", VA = "0x101404CA0")]
			get
			{
				return default(Vector3);
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x060055C2 RID: 21954 RVA: 0x0001CB60 File Offset: 0x0001AD60
		[Token(Token = "0x1700055B")]
		public float TimeCount
		{
			[Token(Token = "0x6004F18")]
			[Address(RVA = "0x101404CAC", Offset = "0x1404CAC", VA = "0x101404CAC")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x060055C3 RID: 21955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F19")]
		[Address(RVA = "0x101404CB4", Offset = "0x1404CB4", VA = "0x101404CB4")]
		public void Setup()
		{
		}

		// Token: 0x060055C4 RID: 21956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F1A")]
		[Address(RVA = "0x101404D6C", Offset = "0x1404D6C", VA = "0x101404D6C")]
		public void Display(int value, bool isCritical, int registHit_Or_DefaultHit_Or_WeakHit, bool isUnhappy, bool isPoison, Transform targetTransform, float offsetY)
		{
		}

		// Token: 0x060055C5 RID: 21957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F1B")]
		[Address(RVA = "0x1014054D4", Offset = "0x14054D4", VA = "0x1014054D4")]
		private void Update()
		{
		}

		// Token: 0x060055C6 RID: 21958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F1C")]
		[Address(RVA = "0x1014056AC", Offset = "0x14056AC", VA = "0x1014056AC")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x060055C7 RID: 21959 RVA: 0x0001CB78 File Offset: 0x0001AD78
		[Token(Token = "0x6004F1D")]
		[Address(RVA = "0x1014056E4", Offset = "0x14056E4", VA = "0x1014056E4")]
		public bool IsComplete()
		{
			return default(bool);
		}

		// Token: 0x060055C8 RID: 21960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F1E")]
		[Address(RVA = "0x10140571C", Offset = "0x140571C", VA = "0x10140571C")]
		public DamageValue()
		{
		}

		// Token: 0x040067FE RID: 26622
		[Token(Token = "0x4004934")]
		[SerializeField]
		public static Color m_NormalColor;

		// Token: 0x040067FF RID: 26623
		[Token(Token = "0x4004935")]
		[SerializeField]
		public static Color m_CriticalColor;

		// Token: 0x04006800 RID: 26624
		[Token(Token = "0x4004936")]
		[SerializeField]
		public static Color m_WeakColor;

		// Token: 0x04006801 RID: 26625
		[Token(Token = "0x4004937")]
		[SerializeField]
		public static Color m_ResistColor;

		// Token: 0x04006802 RID: 26626
		[Token(Token = "0x4004938")]
		[SerializeField]
		public static Color m_RecoveryColor;

		// Token: 0x04006803 RID: 26627
		[Token(Token = "0x4004939")]
		[SerializeField]
		public static Color m_UnhappyColor;

		// Token: 0x04006804 RID: 26628
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400493A")]
		[SerializeField]
		private ImageNumbers m_NumbersParent;

		// Token: 0x04006805 RID: 26629
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400493B")]
		private ImageNumber[] m_Numbers;

		// Token: 0x04006806 RID: 26630
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400493C")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04006807 RID: 26631
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400493D")]
		[SerializeField]
		private Material m_Material;

		// Token: 0x04006808 RID: 26632
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400493E")]
		[SerializeField]
		private DamageDescript m_Descript;

		// Token: 0x04006809 RID: 26633
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400493F")]
		[SerializeField]
		private DamageDescript m_FluctuationDescript;

		// Token: 0x0400680A RID: 26634
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004940")]
		private float m_TimeCount;

		// Token: 0x0400680B RID: 26635
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004941")]
		private Vector3 m_Offset;

		// Token: 0x0400680C RID: 26636
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004942")]
		private GameObject m_GameObject;

		// Token: 0x0400680D RID: 26637
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004943")]
		private RectTransform m_RectTransform;

		// Token: 0x0400680E RID: 26638
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004944")]
		private RectTransform m_ParentRectTransform;

		// Token: 0x0400680F RID: 26639
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004945")]
		private Vector3 m_TargetPosition;
	}
}
