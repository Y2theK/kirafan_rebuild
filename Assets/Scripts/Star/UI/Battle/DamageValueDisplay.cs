﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200110F RID: 4367
	[Token(Token = "0x2000B52")]
	[StructLayout(3)]
	public class DamageValueDisplay : MonoBehaviour
	{
		// Token: 0x060055CA RID: 21962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F20")]
		[Address(RVA = "0x1014058BC", Offset = "0x14058BC", VA = "0x1014058BC")]
		public void Setup()
		{
		}

		// Token: 0x060055CB RID: 21963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F21")]
		[Address(RVA = "0x1014059E0", Offset = "0x14059E0", VA = "0x1014059E0")]
		private void Update()
		{
		}

		// Token: 0x060055CC RID: 21964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F22")]
		[Address(RVA = "0x1014062A8", Offset = "0x14062A8", VA = "0x1014062A8")]
		public void Display(int value, bool isCritical, bool isUnhappy, bool isPoison, int registHit_Or_DefaultHit_Or_WeakHit, Transform targetTransform, float offsetY)
		{
		}

		// Token: 0x060055CD RID: 21965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F23")]
		[Address(RVA = "0x101405BD0", Offset = "0x1405BD0", VA = "0x101405BD0")]
		private void DisplayFromReserve(DamageValueDisplay.DamageData data)
		{
		}

		// Token: 0x060055CE RID: 21966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F24")]
		[Address(RVA = "0x101406048", Offset = "0x1406048", VA = "0x101406048")]
		private void RemoveCompleteDisplay()
		{
		}

		// Token: 0x060055CF RID: 21967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F25")]
		[Address(RVA = "0x101406378", Offset = "0x1406378", VA = "0x101406378")]
		public DamageValueDisplay()
		{
		}

		// Token: 0x04006810 RID: 26640
		[Token(Token = "0x4004946")]
		private const int DISPLAY_CACHE_NUM = 16;

		// Token: 0x04006811 RID: 26641
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004947")]
		[SerializeField]
		private DamageValue m_DamageValuePrefab;

		// Token: 0x04006812 RID: 26642
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004948")]
		private List<DamageValue> m_EmptyDamageValueList;

		// Token: 0x04006813 RID: 26643
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004949")]
		private List<DamageValue> m_ActiveDamageValueList;

		// Token: 0x04006814 RID: 26644
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400494A")]
		private List<DamageValue> m_DamageValueListWork;

		// Token: 0x04006815 RID: 26645
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400494B")]
		private List<DamageValueDisplay.DamageData> m_DamageDataList;

		// Token: 0x04006816 RID: 26646
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400494C")]
		private List<DamageValueDisplay.DamageData> m_DamageDataListWork;

		// Token: 0x04006817 RID: 26647
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400494D")]
		private float m_LastDisplayY;

		// Token: 0x04006818 RID: 26648
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400494E")]
		private RectTransform m_RectTransform;

		// Token: 0x02001110 RID: 4368
		[Token(Token = "0x200127E")]
		private struct DamageData
		{
			// Token: 0x04006819 RID: 26649
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40071FE")]
			public int m_Value;

			// Token: 0x0400681A RID: 26650
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x40071FF")]
			public bool m_IsCritical;

			// Token: 0x0400681B RID: 26651
			[Cpp2IlInjected.FieldOffset(Offset = "0x5")]
			[Token(Token = "0x4007200")]
			public bool m_IsUnhappy;

			// Token: 0x0400681C RID: 26652
			[Cpp2IlInjected.FieldOffset(Offset = "0x6")]
			[Token(Token = "0x4007201")]
			public bool m_IsPoison;

			// Token: 0x0400681D RID: 26653
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4007202")]
			public int m_registHit_Or_DefaultHit_Or_WeakHit;

			// Token: 0x0400681E RID: 26654
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007203")]
			public Transform m_TargetTransform;

			// Token: 0x0400681F RID: 26655
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007204")]
			public float m_OffsetY;
		}
	}
}
