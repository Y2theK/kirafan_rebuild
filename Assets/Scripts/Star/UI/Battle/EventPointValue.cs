﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001111 RID: 4369
	[Token(Token = "0x2000B53")]
	[StructLayout(3)]
	public class EventPointValue : MonoBehaviour
	{
		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x060055D0 RID: 21968 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700055C")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004F26")]
			[Address(RVA = "0x101406460", Offset = "0x1406460", VA = "0x101406460")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x060055D1 RID: 21969 RVA: 0x0001CB90 File Offset: 0x0001AD90
		[Token(Token = "0x1700055D")]
		public Vector3 TargetPosition
		{
			[Token(Token = "0x6004F27")]
			[Address(RVA = "0x101406468", Offset = "0x1406468", VA = "0x101406468")]
			get
			{
				return default(Vector3);
			}
		}

		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x060055D2 RID: 21970 RVA: 0x0001CBA8 File Offset: 0x0001ADA8
		[Token(Token = "0x1700055E")]
		public float TimeCount
		{
			[Token(Token = "0x6004F28")]
			[Address(RVA = "0x101406474", Offset = "0x1406474", VA = "0x101406474")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x060055D3 RID: 21971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F29")]
		[Address(RVA = "0x10140647C", Offset = "0x140647C", VA = "0x10140647C")]
		public void Setup()
		{
		}

		// Token: 0x060055D4 RID: 21972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F2A")]
		[Address(RVA = "0x10140650C", Offset = "0x140650C", VA = "0x10140650C")]
		public void Display(int value, Transform targetTransform, float offsetY)
		{
		}

		// Token: 0x060055D5 RID: 21973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F2B")]
		[Address(RVA = "0x101406810", Offset = "0x1406810", VA = "0x101406810")]
		private void Update()
		{
		}

		// Token: 0x060055D6 RID: 21974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F2C")]
		[Address(RVA = "0x1014069E8", Offset = "0x14069E8", VA = "0x1014069E8")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x060055D7 RID: 21975 RVA: 0x0001CBC0 File Offset: 0x0001ADC0
		[Token(Token = "0x6004F2D")]
		[Address(RVA = "0x101406A20", Offset = "0x1406A20", VA = "0x101406A20")]
		public bool IsComplete()
		{
			return default(bool);
		}

		// Token: 0x060055D8 RID: 21976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F2E")]
		[Address(RVA = "0x101406A58", Offset = "0x1406A58", VA = "0x101406A58")]
		public EventPointValue()
		{
		}

		// Token: 0x04006820 RID: 26656
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400494F")]
		[SerializeField]
		private ImageNumbers m_Number;

		// Token: 0x04006821 RID: 26657
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004950")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04006822 RID: 26658
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004951")]
		private float m_TimeCount;

		// Token: 0x04006823 RID: 26659
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004952")]
		private Vector3 m_Offset;

		// Token: 0x04006824 RID: 26660
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004953")]
		private GameObject m_GameObject;

		// Token: 0x04006825 RID: 26661
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004954")]
		private RectTransform m_RectTransform;

		// Token: 0x04006826 RID: 26662
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004955")]
		private RectTransform m_ParentRectTransform;

		// Token: 0x04006827 RID: 26663
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004956")]
		private Vector3 m_TargetPosition;
	}
}
