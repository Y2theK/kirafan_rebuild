﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001112 RID: 4370
	[Token(Token = "0x2000B54")]
	[StructLayout(3)]
	public class EventPointValueDisplay : MonoBehaviour
	{
		// Token: 0x060055D9 RID: 21977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F2F")]
		[Address(RVA = "0x101406A60", Offset = "0x1406A60", VA = "0x101406A60")]
		public void Setup()
		{
		}

		// Token: 0x060055DA RID: 21978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F30")]
		[Address(RVA = "0x101406B84", Offset = "0x1406B84", VA = "0x101406B84")]
		private void Update()
		{
		}

		// Token: 0x060055DB RID: 21979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F31")]
		[Address(RVA = "0x1014073D0", Offset = "0x14073D0", VA = "0x1014073D0")]
		public void Display(int value, Transform targetTransform, float offsetY)
		{
		}

		// Token: 0x060055DC RID: 21980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F32")]
		[Address(RVA = "0x101406D8C", Offset = "0x1406D8C", VA = "0x101406D8C")]
		private void DisplayFromReserve(EventPointValueDisplay.PointData data)
		{
		}

		// Token: 0x060055DD RID: 21981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F33")]
		[Address(RVA = "0x101407170", Offset = "0x1407170", VA = "0x101407170")]
		private void RemoveCompleteDisplay()
		{
		}

		// Token: 0x060055DE RID: 21982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F34")]
		[Address(RVA = "0x10140746C", Offset = "0x140746C", VA = "0x10140746C")]
		public EventPointValueDisplay()
		{
		}

		// Token: 0x04006828 RID: 26664
		[Token(Token = "0x4004957")]
		private const int DISPLAY_CACHE_NUM = 3;

		// Token: 0x04006829 RID: 26665
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004958")]
		[SerializeField]
		private EventPointValue m_EventPointValuePrefab;

		// Token: 0x0400682A RID: 26666
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004959")]
		private List<EventPointValue> m_EmptyInstList;

		// Token: 0x0400682B RID: 26667
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400495A")]
		private List<EventPointValue> m_ActiveInstList;

		// Token: 0x0400682C RID: 26668
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400495B")]
		private List<EventPointValue> m_InstListWork;

		// Token: 0x0400682D RID: 26669
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400495C")]
		private List<EventPointValueDisplay.PointData> m_PointDataList;

		// Token: 0x0400682E RID: 26670
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400495D")]
		private List<EventPointValueDisplay.PointData> m_PointDataListWork;

		// Token: 0x0400682F RID: 26671
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400495E")]
		private float m_LastDisplayY;

		// Token: 0x04006830 RID: 26672
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400495F")]
		private RectTransform m_RectTransform;

		// Token: 0x02001113 RID: 4371
		[Token(Token = "0x200127F")]
		private struct PointData
		{
			// Token: 0x04006831 RID: 26673
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4007205")]
			public int m_Value;

			// Token: 0x04006832 RID: 26674
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4007206")]
			public Transform m_TargetTransform;

			// Token: 0x04006833 RID: 26675
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007207")]
			public float m_OffsetY;
		}
	}
}
