﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001114 RID: 4372
	[Token(Token = "0x2000B55")]
	[StructLayout(3)]
	public class GearParent : MonoBehaviour
	{
		// Token: 0x060055DF RID: 21983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F35")]
		[Address(RVA = "0x101407554", Offset = "0x1407554", VA = "0x101407554")]
		private void Start()
		{
		}

		// Token: 0x060055E0 RID: 21984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F36")]
		[Address(RVA = "0x1014075AC", Offset = "0x14075AC", VA = "0x1014075AC")]
		private void Update()
		{
		}

		// Token: 0x060055E1 RID: 21985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F37")]
		[Address(RVA = "0x101407708", Offset = "0x1407708", VA = "0x101407708")]
		public void SetSpeed(float speed = -1f, bool reverse = false)
		{
		}

		// Token: 0x060055E2 RID: 21986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F38")]
		[Address(RVA = "0x10140772C", Offset = "0x140772C", VA = "0x10140772C")]
		public void Stop()
		{
		}

		// Token: 0x060055E3 RID: 21987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F39")]
		[Address(RVA = "0x101407734", Offset = "0x1407734", VA = "0x101407734")]
		public GearParent()
		{
		}

		// Token: 0x04006834 RID: 26676
		[Token(Token = "0x4004960")]
		public const float DEFAULT_SPEED = 200f;

		// Token: 0x04006835 RID: 26677
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004961")]
		private RectTransform[] m_Gears;

		// Token: 0x04006836 RID: 26678
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004962")]
		private float m_Speed;
	}
}
