﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001115 RID: 4373
	[Token(Token = "0x2000B56")]
	[StructLayout(3)]
	public class HPBarGleam : MonoBehaviour
	{
		// Token: 0x060055E4 RID: 21988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3A")]
		[Address(RVA = "0x10140773C", Offset = "0x140773C", VA = "0x10140773C")]
		private void Start()
		{
		}

		// Token: 0x060055E5 RID: 21989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3B")]
		[Address(RVA = "0x1014077D0", Offset = "0x14077D0", VA = "0x1014077D0")]
		private void Update()
		{
		}

		// Token: 0x060055E6 RID: 21990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3C")]
		[Address(RVA = "0x1014079D8", Offset = "0x14079D8", VA = "0x1014079D8")]
		public HPBarGleam()
		{
		}

		// Token: 0x04006837 RID: 26679
		[Token(Token = "0x4004963")]
		private const float MAXRATE = 3f;

		// Token: 0x04006838 RID: 26680
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004964")]
		[SerializeField]
		private Image m_Bar;

		// Token: 0x04006839 RID: 26681
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004965")]
		[SerializeField]
		private RectTransform m_Gleam;

		// Token: 0x0400683A RID: 26682
		[Token(Token = "0x4004966")]
		private const float SPEED = 1f;

		// Token: 0x0400683B RID: 26683
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004967")]
		private RectTransform m_RectTransform;

		// Token: 0x0400683C RID: 26684
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004968")]
		private RectTransform m_ParentRectTransform;

		// Token: 0x0400683D RID: 26685
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004969")]
		private float m_Rate;
	}
}
