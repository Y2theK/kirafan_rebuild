﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001116 RID: 4374
	[Token(Token = "0x2000B57")]
	[StructLayout(3)]
	public class InterruptFriendJoinSelect : MonoBehaviour
	{
		// Token: 0x060055E7 RID: 21991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3D")]
		[Address(RVA = "0x1014079E0", Offset = "0x14079E0", VA = "0x1014079E0")]
		public void Setup()
		{
		}

		// Token: 0x060055E8 RID: 21992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3E")]
		[Address(RVA = "0x101407B2C", Offset = "0x1407B2C", VA = "0x101407B2C")]
		public void Destroy()
		{
		}

		// Token: 0x060055E9 RID: 21993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F3F")]
		[Address(RVA = "0x101407B30", Offset = "0x1407B30", VA = "0x101407B30")]
		public void PlayIn()
		{
		}

		// Token: 0x060055EA RID: 21994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F40")]
		[Address(RVA = "0x101407C80", Offset = "0x1407C80", VA = "0x101407C80")]
		public void PlayOut()
		{
		}

		// Token: 0x060055EB RID: 21995 RVA: 0x0001CBD8 File Offset: 0x0001ADD8
		[Token(Token = "0x6004F41")]
		[Address(RVA = "0x101407CD0", Offset = "0x1407CD0", VA = "0x101407CD0")]
		public bool IsAnimPlaying()
		{
			return default(bool);
		}

		// Token: 0x060055EC RID: 21996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F42")]
		[Address(RVA = "0x101407D00", Offset = "0x1407D00", VA = "0x101407D00")]
		private void Update()
		{
		}

		// Token: 0x060055ED RID: 21997 RVA: 0x0001CBF0 File Offset: 0x0001ADF0
		[Token(Token = "0x6004F43")]
		[Address(RVA = "0x101407B70", Offset = "0x1407B70", VA = "0x101407B70")]
		private bool SetState(InterruptFriendJoinSelect.eState state)
		{
			return default(bool);
		}

		// Token: 0x060055EE RID: 21998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F44")]
		[Address(RVA = "0x101407E80", Offset = "0x1407E80", VA = "0x101407E80")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x060055EF RID: 21999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F45")]
		[Address(RVA = "0x101407EF8", Offset = "0x1407EF8", VA = "0x101407EF8")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x060055F0 RID: 22000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F46")]
		[Address(RVA = "0x101407E48", Offset = "0x1407E48", VA = "0x101407E48")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x060055F1 RID: 22001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F47")]
		[Address(RVA = "0x101407E38", Offset = "0x1407E38", VA = "0x101407E38")]
		public void UpdateAndroidBackKey()
		{
		}

		// Token: 0x060055F2 RID: 22002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F48")]
		[Address(RVA = "0x101407F50", Offset = "0x1407F50", VA = "0x101407F50")]
		public InterruptFriendJoinSelect()
		{
		}

		// Token: 0x0400683E RID: 26686
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400496A")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x0400683F RID: 26687
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400496B")]
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04006840 RID: 26688
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400496C")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006841 RID: 26689
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400496D")]
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04006842 RID: 26690
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400496E")]
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x04006843 RID: 26691
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400496F")]
		private GameObject m_GameObject;

		// Token: 0x04006844 RID: 26692
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004970")]
		private InterruptFriendJoinSelect.eButton m_SelectButton;

		// Token: 0x02001117 RID: 4375
		[Token(Token = "0x2001280")]
		private enum eState
		{
			// Token: 0x04006846 RID: 26694
			[Token(Token = "0x4007209")]
			Hide,
			// Token: 0x04006847 RID: 26695
			[Token(Token = "0x400720A")]
			In,
			// Token: 0x04006848 RID: 26696
			[Token(Token = "0x400720B")]
			Wait,
			// Token: 0x04006849 RID: 26697
			[Token(Token = "0x400720C")]
			Out,
			// Token: 0x0400684A RID: 26698
			[Token(Token = "0x400720D")]
			Num
		}

		// Token: 0x02001118 RID: 4376
		[Token(Token = "0x2001281")]
		private enum eButton
		{
			// Token: 0x0400684C RID: 26700
			[Token(Token = "0x400720F")]
			None,
			// Token: 0x0400684D RID: 26701
			[Token(Token = "0x4007210")]
			Decide,
			// Token: 0x0400684E RID: 26702
			[Token(Token = "0x4007211")]
			Cancel
		}
	}
}
