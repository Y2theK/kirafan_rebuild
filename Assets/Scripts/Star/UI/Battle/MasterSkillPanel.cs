﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001119 RID: 4377
	[Token(Token = "0x2000B58")]
	[StructLayout(3)]
	public class MasterSkillPanel : MonoBehaviour
	{
		// Token: 0x060055F3 RID: 22003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F49")]
		[Address(RVA = "0x101407F58", Offset = "0x1407F58", VA = "0x101407F58")]
		public void SetData(BattleCommandData command, string text, string detail)
		{
		}

		// Token: 0x060055F4 RID: 22004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F4A")]
		[Address(RVA = "0x101408074", Offset = "0x1408074", VA = "0x101408074")]
		public void SetInteractable(bool flg)
		{
		}

		// Token: 0x060055F5 RID: 22005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F4B")]
		[Address(RVA = "0x1014080AC", Offset = "0x14080AC", VA = "0x1014080AC")]
		public void SetExecuted(bool isExecuted, bool isAnyUsed, eMasterOrbSkillUseType skillUseType)
		{
		}

		// Token: 0x060055F6 RID: 22006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F4C")]
		[Address(RVA = "0x10140818C", Offset = "0x140818C", VA = "0x10140818C")]
		public void SealPanels()
		{
		}

		// Token: 0x060055F7 RID: 22007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F4D")]
		[Address(RVA = "0x101408228", Offset = "0x1408228", VA = "0x101408228")]
		public MasterSkillPanel()
		{
		}

		// Token: 0x0400684F RID: 26703
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004971")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x04006850 RID: 26704
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004972")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04006851 RID: 26705
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004973")]
		[SerializeField]
		private Text m_DetailTextObj;

		// Token: 0x04006852 RID: 26706
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004974")]
		[SerializeField]
		private PrefabCloner m_SkillIcon;

		// Token: 0x04006853 RID: 26707
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004975")]
		[SerializeField]
		private Image m_UsedText;

		// Token: 0x04006854 RID: 26708
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004976")]
		[SerializeField]
		private Image m_CannotUseText;

		// Token: 0x04006855 RID: 26709
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004977")]
		[SerializeField]
		private GameObject[] m_SealableObject;
	}
}
