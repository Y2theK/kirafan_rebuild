﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200111A RID: 4378
	[Token(Token = "0x2000B59")]
	[StructLayout(3)]
	public class MasterSkillTargetWindow : UIGroup
	{
		// Token: 0x1400013B RID: 315
		// (add) Token: 0x060055F8 RID: 22008 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060055F9 RID: 22009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013B")]
		public event Action<int> OnDecide
		{
			[Token(Token = "0x6004F4E")]
			[Address(RVA = "0x101408230", Offset = "0x1408230", VA = "0x101408230")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004F4F")]
			[Address(RVA = "0x10140833C", Offset = "0x140833C", VA = "0x10140833C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060055FA RID: 22010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F50")]
		[Address(RVA = "0x101408448", Offset = "0x1408448", VA = "0x101408448", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060055FB RID: 22011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F51")]
		[Address(RVA = "0x101408450", Offset = "0x1408450", VA = "0x101408450", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060055FC RID: 22012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F52")]
		[Address(RVA = "0x101408698", Offset = "0x1408698", VA = "0x101408698", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x060055FD RID: 22013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F53")]
		[Address(RVA = "0x101408848", Offset = "0x1408848", VA = "0x101408848")]
		public void SetMode(MasterSkillTargetWindow.eMode mode)
		{
		}

		// Token: 0x060055FE RID: 22014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F54")]
		[Address(RVA = "0x1014089DC", Offset = "0x14089DC", VA = "0x1014089DC")]
		public void SetActive(int idx, bool flg)
		{
		}

		// Token: 0x060055FF RID: 22015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F55")]
		[Address(RVA = "0x101408AC8", Offset = "0x1408AC8", VA = "0x101408AC8")]
		public void SetPlayerIcon(int idx, int charaID)
		{
		}

		// Token: 0x06005600 RID: 22016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F56")]
		[Address(RVA = "0x101408D54", Offset = "0x1408D54", VA = "0x101408D54")]
		public void SetEnemyIcon(int idx, int resourceID)
		{
		}

		// Token: 0x06005601 RID: 22017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F57")]
		[Address(RVA = "0x101408FE0", Offset = "0x1408FE0", VA = "0x101408FE0")]
		public void SetInteractable(int idx, bool flg)
		{
		}

		// Token: 0x06005602 RID: 22018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F58")]
		[Address(RVA = "0x101409058", Offset = "0x1409058", VA = "0x101409058")]
		public void OnClickButtonCallBack(int idx)
		{
		}

		// Token: 0x06005603 RID: 22019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F59")]
		[Address(RVA = "0x1014090C8", Offset = "0x14090C8", VA = "0x1014090C8")]
		public void OnClickCancelButtonCallBack()
		{
		}

		// Token: 0x06005604 RID: 22020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5A")]
		[Address(RVA = "0x10140912C", Offset = "0x140912C", VA = "0x10140912C")]
		public MasterSkillTargetWindow()
		{
		}

		// Token: 0x04006856 RID: 26710
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004978")]
		[SerializeField]
		private Image[] m_CharaIconImage;

		// Token: 0x04006857 RID: 26711
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004979")]
		[SerializeField]
		private GameObject[] m_TypeIconObjs;

		// Token: 0x04006858 RID: 26712
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400497A")]
		[SerializeField]
		private CustomButton[] m_CustomButton;

		// Token: 0x04006859 RID: 26713
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400497B")]
		private SpriteHandler[] m_SpriteHandler;

		// Token: 0x0200111B RID: 4379
		[Token(Token = "0x2001282")]
		public enum eMode
		{
			// Token: 0x0400685C RID: 26716
			[Token(Token = "0x4007213")]
			Player,
			// Token: 0x0400685D RID: 26717
			[Token(Token = "0x4007214")]
			Enemy
		}
	}
}
