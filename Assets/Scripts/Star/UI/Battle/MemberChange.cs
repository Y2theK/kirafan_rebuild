﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200111C RID: 4380
	[Token(Token = "0x2000B5A")]
	[StructLayout(3)]
	public class MemberChange : MonoBehaviour
	{
		// Token: 0x06005605 RID: 22021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5B")]
		[Address(RVA = "0x10140918C", Offset = "0x140918C", VA = "0x10140918C")]
		public void Setup(int masterOrbID)
		{
		}

		// Token: 0x06005606 RID: 22022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5C")]
		[Address(RVA = "0x101409728", Offset = "0x1409728", VA = "0x101409728")]
		public void SetPlayerTargetState(int join, int charaID, bool isInteractable)
		{
		}

		// Token: 0x06005607 RID: 22023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5D")]
		[Address(RVA = "0x101409900", Offset = "0x1409900", VA = "0x101409900")]
		public void SetEnemyTargetState(int join, int resourceID, bool isInteractable)
		{
		}

		// Token: 0x06005608 RID: 22024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5E")]
		[Address(RVA = "0x101409AD8", Offset = "0x1409AD8", VA = "0x101409AD8")]
		public void SetMasterOrb(BattleMasterOrbData masterOrbData, BattleSystemData bsd)
		{
		}

		// Token: 0x06005609 RID: 22025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F5F")]
		[Address(RVA = "0x10140A41C", Offset = "0x140A41C", VA = "0x10140A41C")]
		public void Destroy()
		{
		}

		// Token: 0x0600560A RID: 22026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F60")]
		[Address(RVA = "0x10140A500", Offset = "0x140A500", VA = "0x10140A500")]
		public void PlayIn()
		{
		}

		// Token: 0x0600560B RID: 22027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F61")]
		[Address(RVA = "0x10140A884", Offset = "0x140A884", VA = "0x10140A884")]
		public void PlayOut()
		{
		}

		// Token: 0x0600560C RID: 22028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F62")]
		[Address(RVA = "0x10140A888", Offset = "0x140A888", VA = "0x10140A888")]
		public void PlayOutOnCutIn()
		{
		}

		// Token: 0x0600560D RID: 22029 RVA: 0x0001CC08 File Offset: 0x0001AE08
		[Token(Token = "0x6004F63")]
		[Address(RVA = "0x10140A97C", Offset = "0x140A97C", VA = "0x10140A97C")]
		public bool IsAnimPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600560E RID: 22030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F64")]
		[Address(RVA = "0x10140AA78", Offset = "0x140AA78", VA = "0x10140AA78")]
		private void Update()
		{
		}

		// Token: 0x0600560F RID: 22031 RVA: 0x0001CC20 File Offset: 0x0001AE20
		[Token(Token = "0x6004F65")]
		[Address(RVA = "0x10140A774", Offset = "0x140A774", VA = "0x10140A774")]
		private bool SetState(MemberChange.eState state)
		{
			return default(bool);
		}

		// Token: 0x06005610 RID: 22032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F66")]
		[Address(RVA = "0x10140B084", Offset = "0x140B084", VA = "0x10140B084")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x06005611 RID: 22033 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F67")]
		[Address(RVA = "0x10140AFB4", Offset = "0x140AFB4", VA = "0x10140AFB4")]
		public MasterSkillPanel GetMasterSkillPanel(int index)
		{
			return null;
		}

		// Token: 0x06005612 RID: 22034 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004F68")]
		[Address(RVA = "0x10140B0E8", Offset = "0x140B0E8", VA = "0x10140B0E8")]
		public BenchStatus GetBenchStatus(BattleDefine.eJoinMember member)
		{
			return null;
		}

		// Token: 0x06005613 RID: 22035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F69")]
		[Address(RVA = "0x10140B04C", Offset = "0x140B04C", VA = "0x10140B04C")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x06005614 RID: 22036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6A")]
		[Address(RVA = "0x10140B13C", Offset = "0x140B13C", VA = "0x10140B13C")]
		private void CloseSkillConfirm()
		{
		}

		// Token: 0x06005615 RID: 22037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6B")]
		[Address(RVA = "0x1013FE5E4", Offset = "0x13FE5E4", VA = "0x1013FE5E4")]
		public void DecideMember(int idx)
		{
		}

		// Token: 0x06005616 RID: 22038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6C")]
		[Address(RVA = "0x10140B170", Offset = "0x140B170", VA = "0x10140B170")]
		public void OnClickMasterSkill(int idx)
		{
		}

		// Token: 0x06005617 RID: 22039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6D")]
		[Address(RVA = "0x10140B32C", Offset = "0x140B32C", VA = "0x10140B32C")]
		public void OnIdleConfirmWindow()
		{
		}

		// Token: 0x06005618 RID: 22040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6E")]
		[Address(RVA = "0x10140B578", Offset = "0x140B578", VA = "0x10140B578")]
		public void OnClickConfirmSkillButtonCallBack()
		{
		}

		// Token: 0x06005619 RID: 22041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F6F")]
		[Address(RVA = "0x10140B724", Offset = "0x140B724", VA = "0x10140B724")]
		public void OnClickCancelSkillButtonCallBack()
		{
		}

		// Token: 0x0600561A RID: 22042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F70")]
		[Address(RVA = "0x10140B760", Offset = "0x140B760", VA = "0x10140B760")]
		public void OnEndConfirmWindow()
		{
		}

		// Token: 0x0600561B RID: 22043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F71")]
		[Address(RVA = "0x10140BB58", Offset = "0x140BB58", VA = "0x10140BB58")]
		private void SetTargetState(int idx, bool isDisplay, eCharaResourceType resType, int id, bool interactable)
		{
		}

		// Token: 0x0600561C RID: 22044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F72")]
		[Address(RVA = "0x10140B888", Offset = "0x140B888", VA = "0x10140B888")]
		public void OpenTargetWindow()
		{
		}

		// Token: 0x0600561D RID: 22045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F73")]
		[Address(RVA = "0x10140BC64", Offset = "0x140BC64", VA = "0x10140BC64")]
		public void OnClickTargetSelectFootButtonCallBack()
		{
		}

		// Token: 0x0600561E RID: 22046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F74")]
		[Address(RVA = "0x10140BCC4", Offset = "0x140BCC4", VA = "0x10140BCC4")]
		public void OnDecideTarget(int targetIdx)
		{
		}

		// Token: 0x0600561F RID: 22047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F75")]
		[Address(RVA = "0x10140B03C", Offset = "0x140B03C", VA = "0x10140B03C")]
		public void UpdateAndroidBackKey()
		{
		}

		// Token: 0x06005620 RID: 22048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F76")]
		[Address(RVA = "0x10140BCFC", Offset = "0x140BCFC", VA = "0x10140BCFC")]
		public MemberChange()
		{
		}

		// Token: 0x0400685E RID: 26718
		[Token(Token = "0x400497D")]
		private const int MASTER_SKILL_MAX = 3;

		// Token: 0x0400685F RID: 26719
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400497E")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04006860 RID: 26720
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400497F")]
		[SerializeField]
		private BenchStatus[] m_BenchStatus;

		// Token: 0x04006861 RID: 26721
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004980")]
		[SerializeField]
		private Image m_BG;

		// Token: 0x04006862 RID: 26722
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004981")]
		[SerializeField]
		private AnimUIPlayer[] m_Anim;

		// Token: 0x04006863 RID: 26723
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004982")]
		[SerializeField]
		private UIGroup m_MasterSkillWindow;

		// Token: 0x04006864 RID: 26724
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004983")]
		[SerializeField]
		private Text m_MasterOrbLevelTextObj;

		// Token: 0x04006865 RID: 26725
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004984")]
		[SerializeField]
		private Text m_MasterOrbNameTextObj;

		// Token: 0x04006866 RID: 26726
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004985")]
		[SerializeField]
		private MasterSkillPanel[] m_MasterSkillPanel;

		// Token: 0x04006867 RID: 26727
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004986")]
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x04006868 RID: 26728
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004987")]
		[SerializeField]
		private Text m_OrbNoticeText;

		// Token: 0x04006869 RID: 26729
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004988")]
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x0400686A RID: 26730
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004989")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x0400686B RID: 26731
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400498A")]
		[SerializeField]
		private UIGroup m_ConfirmGroup;

		// Token: 0x0400686C RID: 26732
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400498B")]
		[SerializeField]
		private MasterSkillTargetWindow m_TargetWindow;

		// Token: 0x0400686D RID: 26733
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400498C")]
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x0400686E RID: 26734
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400498D")]
		[SerializeField]
		private BattleMasterSkillCutIn m_CutIn;

		// Token: 0x0400686F RID: 26735
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400498E")]
		[SerializeField]
		private Text m_ConfirmSkillNameText;

		// Token: 0x04006870 RID: 26736
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400498F")]
		[SerializeField]
		private CustomButton m_ExecuteSkillButton;

		// Token: 0x04006871 RID: 26737
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004990")]
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04006872 RID: 26738
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004991")]
		private GameObject m_GameObject;

		// Token: 0x04006873 RID: 26739
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004992")]
		private BattleMasterOrbData m_MasterOrbData;

		// Token: 0x04006874 RID: 26740
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004993")]
		private int m_SelectMasterSkillCommandIndex;

		// Token: 0x04006875 RID: 26741
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4004994")]
		private BattleDefine.eJoinMember m_SelectMasterSkillTarget;

		// Token: 0x04006876 RID: 26742
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004995")]
		private MemberChange.TargetState[] m_PlayerTargetState;

		// Token: 0x04006877 RID: 26743
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004996")]
		private MemberChange.TargetState[] m_EnemyTargetState;

		// Token: 0x0200111D RID: 4381
		[Token(Token = "0x2001283")]
		private enum eState
		{
			// Token: 0x04006879 RID: 26745
			[Token(Token = "0x4007216")]
			Hide,
			// Token: 0x0400687A RID: 26746
			[Token(Token = "0x4007217")]
			In,
			// Token: 0x0400687B RID: 26747
			[Token(Token = "0x4007218")]
			Wait,
			// Token: 0x0400687C RID: 26748
			[Token(Token = "0x4007219")]
			Out,
			// Token: 0x0400687D RID: 26749
			[Token(Token = "0x400721A")]
			MemberChange,
			// Token: 0x0400687E RID: 26750
			[Token(Token = "0x400721B")]
			SkillConfirm,
			// Token: 0x0400687F RID: 26751
			[Token(Token = "0x400721C")]
			TargetSelect,
			// Token: 0x04006880 RID: 26752
			[Token(Token = "0x400721D")]
			MasterCutIn,
			// Token: 0x04006881 RID: 26753
			[Token(Token = "0x400721E")]
			Num
		}

		// Token: 0x0200111E RID: 4382
		[Token(Token = "0x2001284")]
		private class TargetState
		{
			// Token: 0x06005621 RID: 22049 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635A")]
			[Address(RVA = "0x101409720", Offset = "0x1409720", VA = "0x101409720")]
			public TargetState()
			{
			}

			// Token: 0x04006882 RID: 26754
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400721F")]
			public bool isActive;

			// Token: 0x04006883 RID: 26755
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007220")]
			public int id;

			// Token: 0x04006884 RID: 26756
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007221")]
			public bool isInteractable;
		}
	}
}
