﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200111F RID: 4383
	[Token(Token = "0x2000B5B")]
	[StructLayout(3)]
	public class MultiBuffDisplay : MonoBehaviour
	{
		// Token: 0x06005622 RID: 22050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F77")]
		[Address(RVA = "0x10140BD74", Offset = "0x140BD74", VA = "0x10140BD74")]
		public void SetStateIconOwner(StateIconOwner owner)
		{
		}

		// Token: 0x06005623 RID: 22051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F78")]
		[Address(RVA = "0x1013FFBC0", Offset = "0x13FFBC0", VA = "0x1013FFBC0")]
		public void Setup()
		{
		}

		// Token: 0x06005624 RID: 22052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F79")]
		[Address(RVA = "0x10140BD7C", Offset = "0x140BD7C", VA = "0x10140BD7C")]
		public void Clear()
		{
		}

		// Token: 0x06005625 RID: 22053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7A")]
		[Address(RVA = "0x101400D0C", Offset = "0x1400D0C", VA = "0x101400D0C")]
		public void SetAbilityPassiveIcon(int num)
		{
		}

		// Token: 0x06005626 RID: 22054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7B")]
		[Address(RVA = "0x101400D54", Offset = "0x1400D54", VA = "0x101400D54")]
		public void SetPassiveIcon(int num)
		{
		}

		// Token: 0x06005627 RID: 22055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7C")]
		[Address(RVA = "0x101400E34", Offset = "0x1400E34", VA = "0x101400E34")]
		public void SetIcon(eStateIconType type, bool flg)
		{
		}

		// Token: 0x06005628 RID: 22056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7D")]
		[Address(RVA = "0x10140C3A8", Offset = "0x140C3A8", VA = "0x10140C3A8")]
		private void LateUpdate()
		{
		}

		// Token: 0x06005629 RID: 22057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7E")]
		[Address(RVA = "0x10140C55C", Offset = "0x140C55C", VA = "0x10140C55C")]
		private void Apply()
		{
		}

		// Token: 0x0600562A RID: 22058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F7F")]
		[Address(RVA = "0x10140C634", Offset = "0x140C634", VA = "0x10140C634")]
		private void ApplyIcon()
		{
		}

		// Token: 0x0600562B RID: 22059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F80")]
		[Address(RVA = "0x10140D2D4", Offset = "0x140D2D4", VA = "0x10140D2D4")]
		private void AdjustIconRectTransform(RectTransform rt, int idx)
		{
		}

		// Token: 0x0600562C RID: 22060 RVA: 0x0001CC38 File Offset: 0x0001AE38
		[Token(Token = "0x6004F81")]
		[Address(RVA = "0x10140CFA4", Offset = "0x140CFA4", VA = "0x10140CFA4")]
		private int GetCurrentPageIconNum()
		{
			return 0;
		}

		// Token: 0x0600562D RID: 22061 RVA: 0x0001CC50 File Offset: 0x0001AE50
		[Token(Token = "0x6004F82")]
		[Address(RVA = "0x10140D0CC", Offset = "0x140D0CC", VA = "0x10140D0CC")]
		private int GetCurrentPagePassiveNum()
		{
			return 0;
		}

		// Token: 0x0600562E RID: 22062 RVA: 0x0001CC68 File Offset: 0x0001AE68
		[Token(Token = "0x6004F83")]
		[Address(RVA = "0x10140D038", Offset = "0x140D038", VA = "0x10140D038")]
		private int GetCurrentPageAbilityPassiveNum()
		{
			return 0;
		}

		// Token: 0x0600562F RID: 22063 RVA: 0x0001CC80 File Offset: 0x0001AE80
		[Token(Token = "0x6004F84")]
		[Address(RVA = "0x10140C60C", Offset = "0x140C60C", VA = "0x10140C60C")]
		private int GetPageNum()
		{
			return 0;
		}

		// Token: 0x06005630 RID: 22064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F85")]
		[Address(RVA = "0x10140D57C", Offset = "0x140D57C", VA = "0x10140D57C")]
		public MultiBuffDisplay()
		{
		}

		// Token: 0x04006885 RID: 26757
		[Token(Token = "0x4004997")]
		private const int MAX_DISP = 6;

		// Token: 0x04006886 RID: 26758
		[Token(Token = "0x4004998")]
		private const float TRANSIT_SEC = 0.2f;

		// Token: 0x04006887 RID: 26759
		[Token(Token = "0x4004999")]
		private const float DISP_SEC = 2f;

		// Token: 0x04006888 RID: 26760
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400499A")]
		[SerializeField]
		private StateIconOwner m_IconOwner;

		// Token: 0x04006889 RID: 26761
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400499B")]
		[SerializeField]
		private PassiveIconOwner m_PassiveIconOwner;

		// Token: 0x0400688A RID: 26762
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400499C")]
		[SerializeField]
		private PassiveIconOwner m_AbilityPassiveIconOwner;

		// Token: 0x0400688B RID: 26763
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400499D")]
		private int m_AbilityPassiveNum;

		// Token: 0x0400688C RID: 26764
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400499E")]
		private List<GameObject> m_AbilityPassiveIconList;

		// Token: 0x0400688D RID: 26765
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400499F")]
		private int m_PassiveNum;

		// Token: 0x0400688E RID: 26766
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40049A0")]
		private List<GameObject> m_PassiveIconList;

		// Token: 0x0400688F RID: 26767
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40049A1")]
		private List<StateIcon> m_StateIconList;

		// Token: 0x04006890 RID: 26768
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40049A2")]
		private bool[] m_Buffs;

		// Token: 0x04006891 RID: 26769
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40049A3")]
		private MultiBuffDisplay.eStep m_Step;

		// Token: 0x04006892 RID: 26770
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40049A4")]
		private bool m_IsBlink;

		// Token: 0x04006893 RID: 26771
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40049A5")]
		private int m_Page;

		// Token: 0x04006894 RID: 26772
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40049A6")]
		private int m_IconNum;

		// Token: 0x04006895 RID: 26773
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40049A7")]
		private bool m_IsDirty;

		// Token: 0x04006896 RID: 26774
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40049A8")]
		private RectTransform m_RectTransform;

		// Token: 0x04006897 RID: 26775
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40049A9")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x02001120 RID: 4384
		[Token(Token = "0x2001285")]
		private enum eStep
		{
			// Token: 0x04006899 RID: 26777
			[Token(Token = "0x4007223")]
			In,
			// Token: 0x0400689A RID: 26778
			[Token(Token = "0x4007224")]
			Idle,
			// Token: 0x0400689B RID: 26779
			[Token(Token = "0x4007225")]
			Out
		}
	}
}
