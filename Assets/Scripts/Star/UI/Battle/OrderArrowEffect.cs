﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001121 RID: 4385
	[Token(Token = "0x2000B5C")]
	[StructLayout(3)]
	public class OrderArrowEffect : MonoBehaviour
	{
		// Token: 0x06005631 RID: 22065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F86")]
		[Address(RVA = "0x10140D65C", Offset = "0x140D65C", VA = "0x10140D65C")]
		private void Start()
		{
		}

		// Token: 0x06005632 RID: 22066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F87")]
		[Address(RVA = "0x10140D9E0", Offset = "0x140D9E0", VA = "0x10140D9E0")]
		private void LateUpdate()
		{
		}

		// Token: 0x06005633 RID: 22067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F88")]
		[Address(RVA = "0x10140DDC8", Offset = "0x140DDC8", VA = "0x10140DDC8")]
		public OrderArrowEffect()
		{
		}

		// Token: 0x0400689C RID: 26780
		[Token(Token = "0x40049AA")]
		protected const float DURATION = 1f;

		// Token: 0x0400689D RID: 26781
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049AB")]
		[SerializeField]
		protected RectTransform[] m_TargetRects;

		// Token: 0x0400689E RID: 26782
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049AC")]
		protected float m_TimeCount;

		// Token: 0x0400689F RID: 26783
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049AD")]
		protected RectTransform m_RectTransform;
	}
}
