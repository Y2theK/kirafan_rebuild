﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001122 RID: 4386
	[Token(Token = "0x2000B5D")]
	[StructLayout(3)]
	public class OrderTogetherFrame : MonoBehaviour
	{
		// Token: 0x06005634 RID: 22068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F89")]
		[Address(RVA = "0x10140DDD0", Offset = "0x140DDD0", VA = "0x10140DDD0")]
		public void SetSpeed(float speed)
		{
		}

		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x06005635 RID: 22069 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700055F")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004F8A")]
			[Address(RVA = "0x10140DDD8", Offset = "0x140DDD8", VA = "0x10140DDD8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x06005636 RID: 22070 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000560")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004F8B")]
			[Address(RVA = "0x10140DE68", Offset = "0x140DE68", VA = "0x10140DE68")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005637 RID: 22071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F8C")]
		[Address(RVA = "0x10140DE70", Offset = "0x140DE70", VA = "0x10140DE70")]
		public void Start()
		{
		}

		// Token: 0x06005638 RID: 22072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F8D")]
		[Address(RVA = "0x10140DED8", Offset = "0x140DED8", VA = "0x10140DED8")]
		public void Update()
		{
		}

		// Token: 0x06005639 RID: 22073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F8E")]
		[Address(RVA = "0x10140E0BC", Offset = "0x140E0BC", VA = "0x10140E0BC")]
		public void SetActive(bool flg)
		{
		}

		// Token: 0x0600563A RID: 22074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F8F")]
		[Address(RVA = "0x10140E0F4", Offset = "0x140E0F4", VA = "0x10140E0F4")]
		public OrderTogetherFrame()
		{
		}

		// Token: 0x040068A0 RID: 26784
		[Token(Token = "0x40049AE")]
		private const int RAINBOW_IMAGE_NUM = 2;

		// Token: 0x040068A1 RID: 26785
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049AF")]
		[SerializeField]
		private RectTransform[] m_RainbowImage;

		// Token: 0x040068A2 RID: 26786
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049B0")]
		[SerializeField]
		private float m_ScrollSpeed;

		// Token: 0x040068A3 RID: 26787
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049B1")]
		private GameObject m_GameObject;

		// Token: 0x040068A4 RID: 26788
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049B2")]
		private RectTransform m_RectTransform;

		// Token: 0x040068A5 RID: 26789
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049B3")]
		private float m_ScrollRate;

		// Token: 0x040068A6 RID: 26790
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40049B4")]
		private Vector2 m_WorkVec;
	}
}
