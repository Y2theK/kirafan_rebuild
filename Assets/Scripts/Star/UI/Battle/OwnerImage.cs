﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001123 RID: 4387
	[Token(Token = "0x2000B5E")]
	[StructLayout(3)]
	public class OwnerImage : MonoBehaviour, IPointerDownHandler, IEventSystemHandler, IPointerUpHandler, IPointerExitHandler
	{
		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x0600563B RID: 22075 RVA: 0x0001CC98 File Offset: 0x0001AE98
		[Token(Token = "0x17000561")]
		public bool IsDisplay
		{
			[Token(Token = "0x6004F90")]
			[Address(RVA = "0x10140E14C", Offset = "0x140E14C", VA = "0x10140E14C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x0600563C RID: 22076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F91")]
		[Address(RVA = "0x10140E154", Offset = "0x140E154", VA = "0x10140E154")]
		public void Setup()
		{
		}

		// Token: 0x0600563D RID: 22077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F92")]
		[Address(RVA = "0x10140E464", Offset = "0x140E464", VA = "0x10140E464")]
		public void Destroy()
		{
		}

		// Token: 0x0600563E RID: 22078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F93")]
		[Address(RVA = "0x10140E500", Offset = "0x140E500", VA = "0x10140E500")]
		private void Update()
		{
		}

		// Token: 0x0600563F RID: 22079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F94")]
		[Address(RVA = "0x10140EA7C", Offset = "0x140EA7C", VA = "0x10140EA7C")]
		private void MoveTo(OwnerImage.ImageHandler handler, float x, float time)
		{
		}

		// Token: 0x06005640 RID: 22080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F95")]
		[Address(RVA = "0x10140ED9C", Offset = "0x140ED9C", VA = "0x10140ED9C")]
		private void OnCompleteMove(OwnerImage.ImageHandler handler)
		{
		}

		// Token: 0x06005641 RID: 22081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F96")]
		[Address(RVA = "0x10140EDC8", Offset = "0x140EDC8", VA = "0x10140EDC8")]
		public void Appear()
		{
		}

		// Token: 0x06005642 RID: 22082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F97")]
		[Address(RVA = "0x10140EDD4", Offset = "0x140EDD4", VA = "0x10140EDD4")]
		public void Disappear()
		{
		}

		// Token: 0x06005643 RID: 22083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F98")]
		[Address(RVA = "0x10140EDF0", Offset = "0x140EDF0", VA = "0x10140EDF0")]
		public void SetSprite(Sprite sprite, bool isChangeOnOutOfScreen)
		{
		}

		// Token: 0x06005644 RID: 22084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F99")]
		[Address(RVA = "0x10140EDE0", Offset = "0x140EDE0", VA = "0x10140EDE0")]
		public void SetEnableInput(bool flag)
		{
		}

		// Token: 0x06005645 RID: 22085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9A")]
		[Address(RVA = "0x10140EFFC", Offset = "0x140EFFC", VA = "0x10140EFFC")]
		public void SetEnableRender(bool flag)
		{
		}

		// Token: 0x06005646 RID: 22086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9B")]
		[Address(RVA = "0x10140F038", Offset = "0x140F038", VA = "0x10140F038", Slot = "4")]
		public void OnPointerDown(PointerEventData eventData)
		{
		}

		// Token: 0x06005647 RID: 22087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9C")]
		[Address(RVA = "0x10140F04C", Offset = "0x140F04C", VA = "0x10140F04C", Slot = "5")]
		public void OnPointerUp(PointerEventData eventData)
		{
		}

		// Token: 0x06005648 RID: 22088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9D")]
		[Address(RVA = "0x10140F054", Offset = "0x140F054", VA = "0x10140F054", Slot = "6")]
		public void OnPointerExit(PointerEventData eventData)
		{
		}

		// Token: 0x06005649 RID: 22089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9E")]
		[Address(RVA = "0x10140F05C", Offset = "0x140F05C", VA = "0x10140F05C")]
		public OwnerImage()
		{
		}

		// Token: 0x040068A7 RID: 26791
		[Token(Token = "0x40049B5")]
		private const float EASE_SPEED = 1f;

		// Token: 0x040068A8 RID: 26792
		[Token(Token = "0x40049B6")]
		private const float FLICK_THRESHOLD = 32f;

		// Token: 0x040068A9 RID: 26793
		[Token(Token = "0x40049B7")]
		private const float HIDE_OFFSETX = 380f;

		// Token: 0x040068AA RID: 26794
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049B8")]
		private bool m_IsDrag;

		// Token: 0x040068AB RID: 26795
		[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
		[Token(Token = "0x40049B9")]
		private bool m_IsDisplay;

		// Token: 0x040068AC RID: 26796
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40049BA")]
		private Vector3 m_StartPosition;

		// Token: 0x040068AD RID: 26797
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049BB")]
		private Vector3 m_HidePosition;

		// Token: 0x040068AE RID: 26798
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049BC")]
		[SerializeField]
		private CanvasScaler m_CanvasScaler;

		// Token: 0x040068AF RID: 26799
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049BD")]
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x040068B0 RID: 26800
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40049BE")]
		[SerializeField]
		private Image[] m_Images;

		// Token: 0x040068B1 RID: 26801
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40049BF")]
		private int m_CurrentIdx;

		// Token: 0x040068B2 RID: 26802
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40049C0")]
		private bool m_IsEnableInput;

		// Token: 0x040068B3 RID: 26803
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40049C1")]
		private OwnerImage.ImageHandler[] m_Handlers;

		// Token: 0x02001124 RID: 4388
		[Token(Token = "0x2001286")]
		public class ImageHandler
		{
			// Token: 0x0600564A RID: 22090 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635B")]
			[Address(RVA = "0x10140E45C", Offset = "0x140E45C", VA = "0x10140E45C")]
			public ImageHandler()
			{
			}

			// Token: 0x040068B4 RID: 26804
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007226")]
			public Image m_Image;

			// Token: 0x040068B5 RID: 26805
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007227")]
			public RectTransform m_RectTransform;

			// Token: 0x040068B6 RID: 26806
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007228")]
			public bool m_IsMoving;
		}
	}
}
