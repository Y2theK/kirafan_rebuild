﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001125 RID: 4389
	[Token(Token = "0x2000B5F")]
	[StructLayout(3)]
	public class PassiveIconOwner : MonoBehaviour
	{
		// Token: 0x0600564B RID: 22091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004F9F")]
		[Address(RVA = "0x10140F070", Offset = "0x140F070", VA = "0x10140F070")]
		public void Setup()
		{
		}

		// Token: 0x0600564C RID: 22092 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004FA0")]
		[Address(RVA = "0x10140D160", Offset = "0x140D160", VA = "0x10140D160")]
		public GameObject ScoopEmptyIcon()
		{
			return null;
		}

		// Token: 0x0600564D RID: 22093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA1")]
		[Address(RVA = "0x10140C100", Offset = "0x140C100", VA = "0x10140C100")]
		public void SinkEmptyIcon(GameObject obj)
		{
		}

		// Token: 0x0600564E RID: 22094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA2")]
		[Address(RVA = "0x10140F1B0", Offset = "0x140F1B0", VA = "0x10140F1B0")]
		public void Destroy()
		{
		}

		// Token: 0x0600564F RID: 22095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA3")]
		[Address(RVA = "0x10140F308", Offset = "0x140F308", VA = "0x10140F308")]
		public PassiveIconOwner()
		{
		}

		// Token: 0x040068B7 RID: 26807
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049C2")]
		[SerializeField]
		private GameObject m_IconPrefab;

		// Token: 0x040068B8 RID: 26808
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049C3")]
		[SerializeField]
		private int m_DefaultCacheSize;

		// Token: 0x040068B9 RID: 26809
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049C4")]
		private RectTransform m_PrefabRect;

		// Token: 0x040068BA RID: 26810
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049C5")]
		private List<GameObject> m_EmptyList;

		// Token: 0x040068BB RID: 26811
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049C6")]
		private List<GameObject> m_ActiveList;

		// Token: 0x040068BC RID: 26812
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049C7")]
		private RectTransform m_RectTransform;
	}
}
