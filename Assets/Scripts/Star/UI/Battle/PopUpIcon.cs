﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001126 RID: 4390
	[Token(Token = "0x2000B60")]
	[StructLayout(3)]
	public class PopUpIcon : MonoBehaviour
	{
		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x06005650 RID: 22096 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000562")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004FA4")]
			[Address(RVA = "0x10140F398", Offset = "0x140F398", VA = "0x10140F398")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x06005651 RID: 22097 RVA: 0x0001CCB0 File Offset: 0x0001AEB0
		[Token(Token = "0x17000563")]
		public Vector3 TargetPosition
		{
			[Token(Token = "0x6004FA5")]
			[Address(RVA = "0x10140F3A0", Offset = "0x140F3A0", VA = "0x10140F3A0")]
			get
			{
				return default(Vector3);
			}
		}

		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x06005652 RID: 22098 RVA: 0x0001CCC8 File Offset: 0x0001AEC8
		[Token(Token = "0x17000564")]
		public float TimeCount
		{
			[Token(Token = "0x6004FA6")]
			[Address(RVA = "0x10140F3AC", Offset = "0x140F3AC", VA = "0x10140F3AC")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x06005653 RID: 22099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA7")]
		[Address(RVA = "0x10140F3B4", Offset = "0x140F3B4", VA = "0x10140F3B4")]
		public void Setup(StateIconOwner stateIconOwner)
		{
		}

		// Token: 0x06005654 RID: 22100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA8")]
		[Address(RVA = "0x10140F504", Offset = "0x140F504", VA = "0x10140F504")]
		public void SetParent(RectTransform parent)
		{
		}

		// Token: 0x06005655 RID: 22101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FA9")]
		[Address(RVA = "0x10140F540", Offset = "0x140F540", VA = "0x10140F540")]
		public void ApplyMiss(bool iconSpace = false)
		{
		}

		// Token: 0x06005656 RID: 22102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FAA")]
		[Address(RVA = "0x10140F5C4", Offset = "0x140F5C4", VA = "0x10140F5C4")]
		public void Apply(SkillActionEffectSet.IconData iconData, bool iconSpace = false)
		{
		}

		// Token: 0x06005657 RID: 22103 RVA: 0x0001CCE0 File Offset: 0x0001AEE0
		[Token(Token = "0x6004FAB")]
		[Address(RVA = "0x10140F98C", Offset = "0x140F98C", VA = "0x10140F98C")]
		public float CalcWidth()
		{
			return 0f;
		}

		// Token: 0x06005658 RID: 22104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FAC")]
		[Address(RVA = "0x10140FA38", Offset = "0x140FA38", VA = "0x10140FA38")]
		public void Display(SkillActionEffectSet.IconData data, Transform targetTransform, float offsetFromTargetTransform, float pivotX, Vector2 offsetUISpace, bool iconSpace)
		{
		}

		// Token: 0x06005659 RID: 22105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FAD")]
		[Address(RVA = "0x10140FCD8", Offset = "0x140FCD8", VA = "0x10140FCD8")]
		private void UpdatePosition()
		{
		}

		// Token: 0x0600565A RID: 22106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FAE")]
		[Address(RVA = "0x10140FBE0", Offset = "0x140FBE0", VA = "0x10140FBE0")]
		public void Clear()
		{
		}

		// Token: 0x0600565B RID: 22107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FAF")]
		[Address(RVA = "0x101410608", Offset = "0x1410608", VA = "0x101410608")]
		private void Update()
		{
		}

		// Token: 0x0600565C RID: 22108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB0")]
		[Address(RVA = "0x1014106AC", Offset = "0x14106AC", VA = "0x1014106AC")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x0600565D RID: 22109 RVA: 0x0001CCF8 File Offset: 0x0001AEF8
		[Token(Token = "0x6004FB1")]
		[Address(RVA = "0x1014106E4", Offset = "0x14106E4", VA = "0x1014106E4")]
		public bool IsComplete()
		{
			return default(bool);
		}

		// Token: 0x0600565E RID: 22110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB2")]
		[Address(RVA = "0x10141071C", Offset = "0x141071C", VA = "0x10141071C")]
		public PopUpIcon()
		{
		}

		// Token: 0x040068BD RID: 26813
		[Token(Token = "0x40049C8")]
		private const float SPACE = 0f;

		// Token: 0x040068BE RID: 26814
		[Token(Token = "0x40049C9")]
		private const float OFFSETY = 64f;

		// Token: 0x040068BF RID: 26815
		[Token(Token = "0x40049CA")]
		public const float ICON_SCALE = 0.8f;

		// Token: 0x040068C0 RID: 26816
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049CB")]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x040068C1 RID: 26817
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049CC")]
		private List<StateIcon> m_StateIconList;

		// Token: 0x040068C2 RID: 26818
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049CD")]
		[SerializeField]
		private GameObject m_IconSpaceObj;

		// Token: 0x040068C3 RID: 26819
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049CE")]
		[SerializeField]
		private RectTransform m_IconParent;

		// Token: 0x040068C4 RID: 26820
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049CF")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x040068C5 RID: 26821
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049D0")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x040068C6 RID: 26822
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40049D1")]
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x040068C7 RID: 26823
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40049D2")]
		[SerializeField]
		private Image m_MissImage;

		// Token: 0x040068C8 RID: 26824
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40049D3")]
		private float m_TimeCount;

		// Token: 0x040068C9 RID: 26825
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40049D4")]
		private float m_Pivot;

		// Token: 0x040068CA RID: 26826
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40049D5")]
		private Vector2 m_OffsetUISpace;

		// Token: 0x040068CB RID: 26827
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40049D6")]
		private bool m_IsAdjust;

		// Token: 0x040068CC RID: 26828
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40049D7")]
		private Vector3 m_TargetPosition;

		// Token: 0x040068CD RID: 26829
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40049D8")]
		private GameObject m_GameObject;

		// Token: 0x040068CE RID: 26830
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40049D9")]
		private RectTransform m_RectTransform;

		// Token: 0x040068CF RID: 26831
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40049DA")]
		private RectTransform m_ParentRectTransform;

		// Token: 0x040068D0 RID: 26832
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40049DB")]
		private RectTransform m_CanvasRectTransform;
	}
}
