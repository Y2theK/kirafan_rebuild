﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001127 RID: 4391
	[Token(Token = "0x2000B61")]
	[StructLayout(3)]
	public class PopUpIconDisplay : MonoBehaviour
	{
		// Token: 0x0600565F RID: 22111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB3")]
		[Address(RVA = "0x10141078C", Offset = "0x141078C", VA = "0x10141078C")]
		public void Setup()
		{
		}

		// Token: 0x06005660 RID: 22112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB4")]
		[Address(RVA = "0x101410928", Offset = "0x1410928", VA = "0x101410928")]
		private void Update()
		{
		}

		// Token: 0x06005661 RID: 22113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB5")]
		[Address(RVA = "0x101411270", Offset = "0x1411270", VA = "0x101411270")]
		public void Display(List<SkillActionEffectSet.IconData> datas, Transform targetTransform, float offsetY)
		{
		}

		// Token: 0x06005662 RID: 22114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB6")]
		[Address(RVA = "0x101410BA4", Offset = "0x1410BA4", VA = "0x101410BA4")]
		private void DisplayFromReserve(PopUpIconDisplay.PopUpIconReserveData data)
		{
		}

		// Token: 0x06005663 RID: 22115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB7")]
		[Address(RVA = "0x101411010", Offset = "0x1411010", VA = "0x101411010")]
		private void RemoveCompleteDisplay()
		{
		}

		// Token: 0x06005664 RID: 22116 RVA: 0x0001CD10 File Offset: 0x0001AF10
		[Token(Token = "0x6004FB8")]
		[Address(RVA = "0x1014116C4", Offset = "0x14116C4", VA = "0x1014116C4")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06005665 RID: 22117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FB9")]
		[Address(RVA = "0x101411730", Offset = "0x1411730", VA = "0x101411730")]
		public PopUpIconDisplay()
		{
		}

		// Token: 0x040068D1 RID: 26833
		[Token(Token = "0x40049DC")]
		private const int DISPLAY_CACHE_NUM = 16;

		// Token: 0x040068D2 RID: 26834
		[Token(Token = "0x40049DD")]
		private const float DELAY_TIME_PER_IDX = 0.1f;

		// Token: 0x040068D3 RID: 26835
		[Token(Token = "0x40049DE")]
		private const float SPACE_WIDTH = 256f;

		// Token: 0x040068D4 RID: 26836
		[Token(Token = "0x40049DF")]
		private const float SPACE_HEIGHT = 32f;

		// Token: 0x040068D5 RID: 26837
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049E0")]
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x040068D6 RID: 26838
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049E1")]
		[SerializeField]
		private PopUpIcon m_PopUpIconPrefab;

		// Token: 0x040068D7 RID: 26839
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049E2")]
		[SerializeField]
		private CanvasScaler m_CanvasScaler;

		// Token: 0x040068D8 RID: 26840
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049E3")]
		private List<PopUpIcon> m_EmptyList;

		// Token: 0x040068D9 RID: 26841
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049E4")]
		private List<PopUpIcon> m_ActiveList;

		// Token: 0x040068DA RID: 26842
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049E5")]
		private List<PopUpIcon> m_WorkList;

		// Token: 0x040068DB RID: 26843
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40049E6")]
		private PopUpIcon m_CalcObj;

		// Token: 0x040068DC RID: 26844
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40049E7")]
		private List<PopUpIconDisplay.PopUpIconReserveData> m_ReserveList;

		// Token: 0x040068DD RID: 26845
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40049E8")]
		private List<PopUpIconDisplay.PopUpIconReserveData> m_ReserveWorkList;

		// Token: 0x040068DE RID: 26846
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40049E9")]
		private RectTransform m_RectTransform;

		// Token: 0x02001128 RID: 4392
		[Token(Token = "0x2001287")]
		private class PopUpIconReserveData
		{
			// Token: 0x06005666 RID: 22118 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635C")]
			[Address(RVA = "0x1014116BC", Offset = "0x14116BC", VA = "0x1014116BC")]
			public PopUpIconReserveData()
			{
			}

			// Token: 0x040068DF RID: 26847
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007229")]
			public SkillActionEffectSet.IconData m_Data;

			// Token: 0x040068E0 RID: 26848
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400722A")]
			public Transform m_TargetTransform;

			// Token: 0x040068E1 RID: 26849
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400722B")]
			public float m_OffsetYFromTargetTransform;

			// Token: 0x040068E2 RID: 26850
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400722C")]
			public float m_PivotX;

			// Token: 0x040068E3 RID: 26851
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400722D")]
			public Vector2 m_OffsetUISpace;

			// Token: 0x040068E4 RID: 26852
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400722E")]
			public float m_Delay;

			// Token: 0x040068E5 RID: 26853
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x400722F")]
			public bool m_IconSpace;
		}
	}
}
