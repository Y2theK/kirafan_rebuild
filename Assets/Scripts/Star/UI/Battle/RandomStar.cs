﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001129 RID: 4393
	[Token(Token = "0x2000B62")]
	[StructLayout(3)]
	public class RandomStar : MonoBehaviour
	{
		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x06005667 RID: 22119 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000565")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004FBA")]
			[Address(RVA = "0x101411818", Offset = "0x1411818", VA = "0x101411818")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x06005668 RID: 22120 RVA: 0x0001CD28 File Offset: 0x0001AF28
		// (set) Token: 0x06005669 RID: 22121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000566")]
		public bool Loop
		{
			[Token(Token = "0x6004FBB")]
			[Address(RVA = "0x101411820", Offset = "0x1411820", VA = "0x101411820")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004FBC")]
			[Address(RVA = "0x101411828", Offset = "0x1411828", VA = "0x101411828")]
			set
			{
			}
		}

		// Token: 0x0600566A RID: 22122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FBD")]
		[Address(RVA = "0x101411830", Offset = "0x1411830", VA = "0x101411830")]
		public void Setup(RandomStar.Status status)
		{
		}

		// Token: 0x0600566B RID: 22123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FBE")]
		[Address(RVA = "0x101411904", Offset = "0x1411904", VA = "0x101411904")]
		public void Play()
		{
		}

		// Token: 0x0600566C RID: 22124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FBF")]
		[Address(RVA = "0x10141209C", Offset = "0x141209C", VA = "0x10141209C")]
		public void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x0600566D RID: 22125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC0")]
		[Address(RVA = "0x1014120DC", Offset = "0x14120DC", VA = "0x1014120DC")]
		public void OnCompleteInAlpha()
		{
		}

		// Token: 0x0600566E RID: 22126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC1")]
		[Address(RVA = "0x1014123CC", Offset = "0x14123CC", VA = "0x1014123CC")]
		public void OnCompleteOutAlpha()
		{
		}

		// Token: 0x0600566F RID: 22127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC2")]
		[Address(RVA = "0x1014123DC", Offset = "0x14123DC", VA = "0x1014123DC")]
		public RandomStar()
		{
		}

		// Token: 0x040068E6 RID: 26854
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049EA")]
		private RandomStar.Status m_Status;

		// Token: 0x040068E7 RID: 26855
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049EB")]
		private bool m_Loop;

		// Token: 0x040068E8 RID: 26856
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049EC")]
		private GameObject m_GameObject;

		// Token: 0x040068E9 RID: 26857
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049ED")]
		private RectTransform m_RectTransform;

		// Token: 0x040068EA RID: 26858
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049EE")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x040068EB RID: 26859
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049EF")]
		private Image m_Image;

		// Token: 0x0200112A RID: 4394
		[Token(Token = "0x2001288")]
		public class Status
		{
			// Token: 0x06005670 RID: 22128 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635D")]
			[Address(RVA = "0x1014123E4", Offset = "0x14123E4", VA = "0x1014123E4")]
			public Status()
			{
			}

			// Token: 0x040068EC RID: 26860
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007230")]
			public float m_StartDistanceMin;

			// Token: 0x040068ED RID: 26861
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007231")]
			public float m_StartDistanceMax;

			// Token: 0x040068EE RID: 26862
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007232")]
			public float m_MoveMin;

			// Token: 0x040068EF RID: 26863
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4007233")]
			public float m_MoveMax;

			// Token: 0x040068F0 RID: 26864
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007234")]
			public float m_MoveSec;

			// Token: 0x040068F1 RID: 26865
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4007235")]
			public float m_Delay;
		}
	}
}
