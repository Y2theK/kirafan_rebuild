﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200112B RID: 4395
	[Token(Token = "0x2000B63")]
	[StructLayout(3)]
	public class RandomStarEmitter : MonoBehaviour
	{
		// Token: 0x06005671 RID: 22129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC3")]
		[Address(RVA = "0x101412400", Offset = "0x1412400", VA = "0x101412400")]
		private void Prepare()
		{
		}

		// Token: 0x06005672 RID: 22130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC4")]
		[Address(RVA = "0x101412698", Offset = "0x1412698", VA = "0x101412698")]
		private void Start()
		{
		}

		// Token: 0x06005673 RID: 22131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC5")]
		[Address(RVA = "0x1014127D0", Offset = "0x14127D0", VA = "0x1014127D0")]
		private void Update()
		{
		}

		// Token: 0x06005674 RID: 22132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC6")]
		[Address(RVA = "0x1014126A8", Offset = "0x14126A8", VA = "0x1014126A8")]
		public void Play()
		{
		}

		// Token: 0x06005675 RID: 22133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC7")]
		[Address(RVA = "0x1014128BC", Offset = "0x14128BC", VA = "0x1014128BC")]
		private void OnDestroy()
		{
		}

		// Token: 0x06005676 RID: 22134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC8")]
		[Address(RVA = "0x1014129B0", Offset = "0x14129B0", VA = "0x1014129B0")]
		private void OnEnable()
		{
		}

		// Token: 0x06005677 RID: 22135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FC9")]
		[Address(RVA = "0x1014129B4", Offset = "0x14129B4", VA = "0x1014129B4")]
		public RandomStarEmitter()
		{
		}

		// Token: 0x040068F2 RID: 26866
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049F0")]
		[SerializeField]
		private RandomStar m_RandomStarPrefab;

		// Token: 0x040068F3 RID: 26867
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40049F1")]
		[SerializeField]
		private int m_StarMax;

		// Token: 0x040068F4 RID: 26868
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40049F2")]
		[SerializeField]
		private float m_StartDistanceMin;

		// Token: 0x040068F5 RID: 26869
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40049F3")]
		[SerializeField]
		private float m_StartDistanceMax;

		// Token: 0x040068F6 RID: 26870
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40049F4")]
		[SerializeField]
		private float m_MoveMin;

		// Token: 0x040068F7 RID: 26871
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40049F5")]
		[SerializeField]
		private float m_MoveMax;

		// Token: 0x040068F8 RID: 26872
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40049F6")]
		[SerializeField]
		private float m_MoveSec;

		// Token: 0x040068F9 RID: 26873
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40049F7")]
		[SerializeField]
		private float m_Delay;

		// Token: 0x040068FA RID: 26874
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40049F8")]
		[SerializeField]
		private bool m_AutoStart;

		// Token: 0x040068FB RID: 26875
		[Cpp2IlInjected.FieldOffset(Offset = "0x3D")]
		[Token(Token = "0x40049F9")]
		[SerializeField]
		private bool m_Loop;

		// Token: 0x040068FC RID: 26876
		[Cpp2IlInjected.FieldOffset(Offset = "0x3E")]
		[Token(Token = "0x40049FA")]
		private bool m_IsDonePrepare;

		// Token: 0x040068FD RID: 26877
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40049FB")]
		private RandomStar[] m_RandomStars;

		// Token: 0x040068FE RID: 26878
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40049FC")]
		private RectTransform m_RectTransform;

		// Token: 0x040068FF RID: 26879
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40049FD")]
		private bool m_IsReservePlay;
	}
}
