﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200112C RID: 4396
	[Token(Token = "0x2000B64")]
	[StructLayout(3)]
	public class RecastGauge : MonoBehaviour
	{
		// Token: 0x06005678 RID: 22136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FCA")]
		[Address(RVA = "0x1014013E0", Offset = "0x14013E0", VA = "0x1014013E0")]
		public void SetOwner(CommandButton owner)
		{
		}

		// Token: 0x06005679 RID: 22137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FCB")]
		[Address(RVA = "0x1014013E8", Offset = "0x14013E8", VA = "0x1014013E8")]
		public void Apply(int value, int max, int valueTo)
		{
		}

		// Token: 0x0600567A RID: 22138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FCC")]
		[Address(RVA = "0x1014129DC", Offset = "0x14129DC", VA = "0x1014129DC")]
		private void Apply(float value, float max)
		{
		}

		// Token: 0x0600567B RID: 22139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FCD")]
		[Address(RVA = "0x101412A34", Offset = "0x1412A34", VA = "0x101412A34")]
		private void Update()
		{
		}

		// Token: 0x0600567C RID: 22140 RVA: 0x0001CD40 File Offset: 0x0001AF40
		[Token(Token = "0x6004FCE")]
		[Address(RVA = "0x101401734", Offset = "0x1401734", VA = "0x101401734")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600567D RID: 22141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FCF")]
		[Address(RVA = "0x101412B2C", Offset = "0x1412B2C", VA = "0x101412B2C")]
		public RecastGauge()
		{
		}

		// Token: 0x04006900 RID: 26880
		[Token(Token = "0x40049FE")]
		private const float GAUGE_SPEED = 1f;

		// Token: 0x04006901 RID: 26881
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40049FF")]
		[SerializeField]
		private Image m_GaugeImage;

		// Token: 0x04006902 RID: 26882
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A00")]
		private float m_CurrentValue;

		// Token: 0x04006903 RID: 26883
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004A01")]
		private float m_ValueTo;

		// Token: 0x04006904 RID: 26884
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A02")]
		private float m_Max;

		// Token: 0x04006905 RID: 26885
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004A03")]
		private float m_Speed;

		// Token: 0x04006906 RID: 26886
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A04")]
		private bool m_IsPlaying;

		// Token: 0x04006907 RID: 26887
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A05")]
		private CommandButton m_Owner;
	}
}
