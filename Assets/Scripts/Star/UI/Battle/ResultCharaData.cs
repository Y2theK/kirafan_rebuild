﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001142 RID: 4418
	[Token(Token = "0x2000B75")]
	[StructLayout(3)]
	public class ResultCharaData : MonoBehaviour
	{
		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x0600570B RID: 22283 RVA: 0x0001CE78 File Offset: 0x0001B078
		[Token(Token = "0x17000570")]
		public bool IsExist
		{
			[Token(Token = "0x600505B")]
			[Address(RVA = "0x101412B34", Offset = "0x1412B34", VA = "0x101412B34")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x0600570C RID: 22284 RVA: 0x0001CE90 File Offset: 0x0001B090
		[Token(Token = "0x17000571")]
		public ResultCharaData.eStep Step
		{
			[Token(Token = "0x600505C")]
			[Address(RVA = "0x101412B3C", Offset = "0x1412B3C", VA = "0x101412B3C")]
			get
			{
				return ResultCharaData.eStep.None;
			}
		}

		// Token: 0x0600570D RID: 22285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600505D")]
		[Address(RVA = "0x101412B44", Offset = "0x1412B44", VA = "0x101412B44")]
		public void Setup()
		{
		}

		// Token: 0x0600570E RID: 22286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600505E")]
		[Address(RVA = "0x101412BB8", Offset = "0x1412BB8", VA = "0x101412BB8")]
		private void Update()
		{
		}

		// Token: 0x0600570F RID: 22287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600505F")]
		[Address(RVA = "0x101412D48", Offset = "0x1412D48", VA = "0x101412D48")]
		public void SetData(int limitBreak)
		{
		}

		// Token: 0x06005710 RID: 22288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005060")]
		[Address(RVA = "0x101412DD8", Offset = "0x1412DD8", VA = "0x101412DD8")]
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExps)
		{
		}

		// Token: 0x06005711 RID: 22289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005061")]
		[Address(RVA = "0x101412E58", Offset = "0x1412E58", VA = "0x101412E58")]
		public void PlayExp()
		{
		}

		// Token: 0x06005712 RID: 22290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005062")]
		[Address(RVA = "0x101412E64", Offset = "0x1412E64", VA = "0x101412E64")]
		public void SetFriendshipData(int beforeLevel, long beforeExp, int afterLevel, long afterExp, sbyte expTableID)
		{
		}

		// Token: 0x06005713 RID: 22291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005063")]
		[Address(RVA = "0x101412EC8", Offset = "0x1412EC8", VA = "0x101412EC8")]
		public void SkipExp()
		{
		}

		// Token: 0x06005714 RID: 22292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005064")]
		[Address(RVA = "0x101412FBC", Offset = "0x1412FBC", VA = "0x101412FBC")]
		public void HideRenderLevelUpPop()
		{
		}

		// Token: 0x06005715 RID: 22293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005065")]
		[Address(RVA = "0x1014130B8", Offset = "0x14130B8", VA = "0x1014130B8")]
		public ResultCharaData()
		{
		}

		// Token: 0x040069D5 RID: 27093
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004ABA")]
		[SerializeField]
		private AnimUIPlayer m_ExpWindowAnim;

		// Token: 0x040069D6 RID: 27094
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004ABB")]
		[SerializeField]
		private LimitBreakIcon m_LBIcon;

		// Token: 0x040069D7 RID: 27095
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004ABC")]
		private bool m_IsExist;

		// Token: 0x040069D8 RID: 27096
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004ABD")]
		[SerializeField]
		private ExpGauge m_EXPGauge;

		// Token: 0x040069D9 RID: 27097
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004ABE")]
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x040069DA RID: 27098
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004ABF")]
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x040069DB RID: 27099
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004AC0")]
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x040069DC RID: 27100
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004AC1")]
		private ResultCharaData.eStep m_Step;

		// Token: 0x040069DD RID: 27101
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004AC2")]
		private float m_TimeCount;

		// Token: 0x040069DE RID: 27102
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004AC3")]
		private int m_Friendship;

		// Token: 0x040069DF RID: 27103
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004AC4")]
		private long m_FriendshipExp;

		// Token: 0x040069E0 RID: 27104
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004AC5")]
		private int m_AfterFrendship;

		// Token: 0x040069E1 RID: 27105
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004AC6")]
		private long m_AfterFriendshipExp;

		// Token: 0x040069E2 RID: 27106
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004AC7")]
		private sbyte m_FriendshipExpTableID;

		// Token: 0x040069E3 RID: 27107
		[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
		[Token(Token = "0x4004AC8")]
		private bool m_GaugeSkipped;

		// Token: 0x02001143 RID: 4419
		[Token(Token = "0x200128E")]
		public enum eStep
		{
			// Token: 0x040069E5 RID: 27109
			[Token(Token = "0x4007250")]
			None,
			// Token: 0x040069E6 RID: 27110
			[Token(Token = "0x4007251")]
			ExpIn,
			// Token: 0x040069E7 RID: 27111
			[Token(Token = "0x4007252")]
			Exp,
			// Token: 0x040069E8 RID: 27112
			[Token(Token = "0x4007253")]
			ExpStop,
			// Token: 0x040069E9 RID: 27113
			[Token(Token = "0x4007254")]
			FriendshipStop
		}
	}
}
