﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001144 RID: 4420
	[Token(Token = "0x2000B76")]
	[StructLayout(3)]
	public class ResultCharaReward : UIGroup
	{
		// Token: 0x06005716 RID: 22294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005066")]
		[Address(RVA = "0x1014130C0", Offset = "0x14130C0", VA = "0x1014130C0", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06005717 RID: 22295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005067")]
		[Address(RVA = "0x101413300", Offset = "0x1413300", VA = "0x101413300")]
		public void Setup()
		{
		}

		// Token: 0x06005718 RID: 22296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005068")]
		[Address(RVA = "0x10141365C", Offset = "0x141365C", VA = "0x10141365C")]
		public void SetRewardExp(long value)
		{
		}

		// Token: 0x06005719 RID: 22297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005069")]
		[Address(RVA = "0x1014136F8", Offset = "0x14136F8", VA = "0x1014136F8")]
		public void SetRewardFriendshipExp(long value)
		{
		}

		// Token: 0x0600571A RID: 22298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506A")]
		[Address(RVA = "0x101413794", Offset = "0x1413794", VA = "0x101413794")]
		public void SetCharaData(int idx, int limitbreak)
		{
		}

		// Token: 0x0600571B RID: 22299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506B")]
		[Address(RVA = "0x101413808", Offset = "0x1413808", VA = "0x101413808")]
		public void SetCharaExpData(int idx, long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExps)
		{
		}

		// Token: 0x0600571C RID: 22300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506C")]
		[Address(RVA = "0x1014138C4", Offset = "0x14138C4", VA = "0x1014138C4")]
		public void SetCharaFriendshipData(int idx, int beforeLevel, long beforeExp, int afterLevel, long afterExp, sbyte expTableID)
		{
		}

		// Token: 0x0600571D RID: 22301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506D")]
		[Address(RVA = "0x101413968", Offset = "0x1413968", VA = "0x101413968", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600571E RID: 22302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506E")]
		[Address(RVA = "0x101413D80", Offset = "0x1413D80", VA = "0x101413D80")]
		private void StopSE()
		{
		}

		// Token: 0x0600571F RID: 22303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600506F")]
		[Address(RVA = "0x101413E28", Offset = "0x1413E28", VA = "0x101413E28")]
		public void Tap()
		{
		}

		// Token: 0x06005720 RID: 22304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005070")]
		[Address(RVA = "0x101413FC4", Offset = "0x1413FC4", VA = "0x101413FC4")]
		public void HideLevelUpPop()
		{
		}

		// Token: 0x06005721 RID: 22305 RVA: 0x0001CEA8 File Offset: 0x0001B0A8
		[Token(Token = "0x6005071")]
		[Address(RVA = "0x10141409C", Offset = "0x141409C", VA = "0x10141409C")]
		public Vector2 GetCharaPosition()
		{
			return default(Vector2);
		}

		// Token: 0x06005722 RID: 22306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005072")]
		[Address(RVA = "0x1014141D0", Offset = "0x14141D0", VA = "0x1014141D0")]
		public ResultCharaReward()
		{
		}

		// Token: 0x040069EA RID: 27114
		[Token(Token = "0x4004AC9")]
		private const float STARTTIME = 0.2f;

		// Token: 0x040069EB RID: 27115
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004ACA")]
		[SerializeField]
		private AnimUIPlayer m_EXPObjAnim;

		// Token: 0x040069EC RID: 27116
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004ACB")]
		[SerializeField]
		private AnimUIPlayer m_FriendshipObjAnim;

		// Token: 0x040069ED RID: 27117
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004ACC")]
		[SerializeField]
		private ResultCharaData[] m_CharaData;

		// Token: 0x040069EE RID: 27118
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004ACD")]
		[SerializeField]
		private Text m_GetExpText;

		// Token: 0x040069EF RID: 27119
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004ACE")]
		[SerializeField]
		private Text m_GetFriendshipExpText;

		// Token: 0x040069F0 RID: 27120
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004ACF")]
		[SerializeField]
		private RectTransform m_CharaPosition;

		// Token: 0x040069F1 RID: 27121
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004AD0")]
		private float m_TimeCount;

		// Token: 0x040069F2 RID: 27122
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4004AD1")]
		private bool m_IsDoneStart;

		// Token: 0x040069F3 RID: 27123
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004AD2")]
		private SoundHandler m_SoundHandler;

		// Token: 0x040069F4 RID: 27124
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004AD3")]
		[SerializeField]
		private Material m_CharaMaskMaterialBase;

		// Token: 0x040069F5 RID: 27125
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004AD4")]
		[SerializeField]
		private Image[] m_CharaMaskImage_PL;

		// Token: 0x040069F6 RID: 27126
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004AD5")]
		private Material[] m_CharaMaskMaterial_PL;

		// Token: 0x040069F7 RID: 27127
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004AD6")]
		private ResultCharaReward.eStep m_Step;

		// Token: 0x02001145 RID: 4421
		[Token(Token = "0x200128F")]
		private enum eStep
		{
			// Token: 0x040069F9 RID: 27129
			[Token(Token = "0x4007256")]
			Idle,
			// Token: 0x040069FA RID: 27130
			[Token(Token = "0x4007257")]
			Exp,
			// Token: 0x040069FB RID: 27131
			[Token(Token = "0x4007258")]
			ExpStop,
			// Token: 0x040069FC RID: 27132
			[Token(Token = "0x4007259")]
			CheckUnlock,
			// Token: 0x040069FD RID: 27133
			[Token(Token = "0x400725A")]
			UnlockWait,
			// Token: 0x040069FE RID: 27134
			[Token(Token = "0x400725B")]
			End
		}
	}
}
