﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Town;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001146 RID: 4422
	[Token(Token = "0x2000B77")]
	[StructLayout(3)]
	public class ResultItemReward : UIGroup
	{
		// Token: 0x06005723 RID: 22307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005073")]
		[Address(RVA = "0x101414230", Offset = "0x1414230", VA = "0x101414230")]
		public void SetGoldData(long get, long have)
		{
		}

		// Token: 0x06005724 RID: 22308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005074")]
		[Address(RVA = "0x10141430C", Offset = "0x141430C", VA = "0x10141430C")]
		public void SetEventPointData(long pointEventID, long gain)
		{
		}

		// Token: 0x06005725 RID: 22309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005075")]
		[Address(RVA = "0x101414458", Offset = "0x1414458", VA = "0x101414458")]
		public void SetReorderData(ResultItemReward.ReorderParam param)
		{
		}

		// Token: 0x06005726 RID: 22310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005076")]
		[Address(RVA = "0x101414460", Offset = "0x1414460", VA = "0x101414460")]
		private void UpdateReorderUI()
		{
		}

		// Token: 0x06005727 RID: 22311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005077")]
		[Address(RVA = "0x101414A5C", Offset = "0x1414A5C", VA = "0x101414A5C")]
		public void SetupScroll()
		{
		}

		// Token: 0x06005728 RID: 22312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005078")]
		[Address(RVA = "0x101414328", Offset = "0x1414328", VA = "0x101414328")]
		public void AddItemData(ResultItemReward.eResultItemType type, int id, int num, int extNum, bool appeal = false)
		{
		}

		// Token: 0x06005729 RID: 22313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005079")]
		[Address(RVA = "0x101414AF0", Offset = "0x1414AF0", VA = "0x101414AF0", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600572A RID: 22314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600507A")]
		[Address(RVA = "0x101414C20", Offset = "0x1414C20", VA = "0x101414C20")]
		private void OpenPointEvent()
		{
		}

		// Token: 0x0600572B RID: 22315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600507B")]
		[Address(RVA = "0x101414D54", Offset = "0x1414D54", VA = "0x101414D54")]
		public ResultItemReward()
		{
		}

		// Token: 0x040069FF RID: 27135
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004AD7")]
		[SerializeField]
		private BattleResultUI m_OwnerUI;

		// Token: 0x04006A00 RID: 27136
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004AD8")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04006A01 RID: 27137
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004AD9")]
		[SerializeField]
		private Text m_GetMoneyTextObj;

		// Token: 0x04006A02 RID: 27138
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004ADA")]
		[SerializeField]
		private Text m_HaveMoneyTextObj;

		// Token: 0x04006A03 RID: 27139
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004ADB")]
		[SerializeField]
		private GameObject m_ReorderEnableObj;

		// Token: 0x04006A04 RID: 27140
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004ADC")]
		[SerializeField]
		private GameObject m_ReorderDisableObj;

		// Token: 0x04006A05 RID: 27141
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004ADD")]
		[SerializeField]
		private Text m_ReorderDisableObjText;

		// Token: 0x04006A06 RID: 27142
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004ADE")]
		[SerializeField]
		private GameObject m_UseStaminaIcon;

		// Token: 0x04006A07 RID: 27143
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004ADF")]
		[SerializeField]
		private GameObject m_HaveStaminaIcon;

		// Token: 0x04006A08 RID: 27144
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004AE0")]
		[SerializeField]
		private ItemIcon m_UseItemIcon;

		// Token: 0x04006A09 RID: 27145
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004AE1")]
		[SerializeField]
		private ItemIcon m_HaveItemIcon;

		// Token: 0x04006A0A RID: 27146
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004AE2")]
		[SerializeField]
		private Text m_UseText;

		// Token: 0x04006A0B RID: 27147
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004AE3")]
		[SerializeField]
		private TextFieldValueChange m_HaveValueChange;

		// Token: 0x04006A0C RID: 27148
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004AE4")]
		[SerializeField]
		private Sprite m_EventPointSprite;

		// Token: 0x04006A0D RID: 27149
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004AE5")]
		[SerializeField]
		private ResultPointEventUIGroup m_PointEventGroup;

		// Token: 0x04006A0E RID: 27150
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004AE6")]
		private ResultItemReward.ReorderParam m_ReorderParam;

		// Token: 0x04006A0F RID: 27151
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004AE7")]
		private long m_CheckedStamina;

		// Token: 0x04006A10 RID: 27152
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004AE8")]
		private long m_PointEventID;

		// Token: 0x04006A11 RID: 27153
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004AE9")]
		private long m_PointGain;

		// Token: 0x02001147 RID: 4423
		[Token(Token = "0x2001290")]
		public class ReorderParam
		{
			// Token: 0x0600572C RID: 22316 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006360")]
			[Address(RVA = "0x101414E44", Offset = "0x1414E44", VA = "0x101414E44")]
			private ReorderParam()
			{
			}

			// Token: 0x0600572D RID: 22317 RVA: 0x0001CEC0 File Offset: 0x0001B0C0
			[Token(Token = "0x6006361")]
			[Address(RVA = "0x101414A3C", Offset = "0x1414A3C", VA = "0x101414A3C")]
			public bool IsInteractable()
			{
				return default(bool);
			}

			// Token: 0x0600572E RID: 22318 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006362")]
			[Address(RVA = "0x101414DE4", Offset = "0x1414DE4", VA = "0x101414DE4")]
			public ReorderParam(bool isFirstClear, bool isContinuous, int itemID, int useNum)
			{
			}

			// Token: 0x04006A12 RID: 27154
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400725C")]
			public bool m_IsFirstClear;

			// Token: 0x04006A13 RID: 27155
			[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
			[Token(Token = "0x400725D")]
			public bool m_IsContinuous;

			// Token: 0x04006A14 RID: 27156
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400725E")]
			public int m_ItemID;

			// Token: 0x04006A15 RID: 27157
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400725F")]
			public int m_UseNum;
		}

		// Token: 0x02001148 RID: 4424
		[Token(Token = "0x2001291")]
		public enum eResultItemType
		{
			// Token: 0x04006A17 RID: 27159
			[Token(Token = "0x4007261")]
			Item,
			// Token: 0x04006A18 RID: 27160
			[Token(Token = "0x4007262")]
			Point
		}
	}
}
