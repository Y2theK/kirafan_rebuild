﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200114A RID: 4426
	[Token(Token = "0x2000B79")]
	[StructLayout(3)]
	public class ResultItemScrollItem : ScrollItemIcon
	{
		// Token: 0x06005733 RID: 22323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005080")]
		[Address(RVA = "0x101414E5C", Offset = "0x1414E5C", VA = "0x101414E5C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06005734 RID: 22324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005081")]
		[Address(RVA = "0x101414F64", Offset = "0x1414F64", VA = "0x101414F64")]
		public void Apply(int itemID, int num, int extNum, bool appeal, Sprite sprite)
		{
		}

		// Token: 0x06005735 RID: 22325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005082")]
		[Address(RVA = "0x1014153B0", Offset = "0x14153B0", VA = "0x1014153B0")]
		public ResultItemScrollItem()
		{
		}

		// Token: 0x04006A1F RID: 27167
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004AF0")]
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04006A20 RID: 27168
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004AF1")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04006A21 RID: 27169
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004AF2")]
		[SerializeField]
		private GameObject m_ReplaceObj;

		// Token: 0x04006A22 RID: 27170
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004AF3")]
		[SerializeField]
		private Image m_ReplaceImage;

		// Token: 0x04006A23 RID: 27171
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004AF4")]
		[SerializeField]
		private Image m_FrameAppeal;

		// Token: 0x04006A24 RID: 27172
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004AF5")]
		public Action<int> OnClick;
	}
}
