﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001149 RID: 4425
	[Token(Token = "0x2000B78")]
	[StructLayout(3)]
	public class ResultItemScrollItemData : ScrollItemData
	{
		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x0600572F RID: 22319 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06005730 RID: 22320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000572")]
		public Action OnClick
		{
			[Token(Token = "0x600507C")]
			[Address(RVA = "0x1014153B8", Offset = "0x14153B8", VA = "0x1014153B8")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600507D")]
			[Address(RVA = "0x101414AE8", Offset = "0x1414AE8", VA = "0x101414AE8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06005731 RID: 22321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600507E")]
		[Address(RVA = "0x101414A90", Offset = "0x1414A90", VA = "0x101414A90")]
		public ResultItemScrollItemData(int itemID, int num, int extNum, bool appeal, Sprite sprite)
		{
		}

		// Token: 0x06005732 RID: 22322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600507F")]
		[Address(RVA = "0x1014153C0", Offset = "0x14153C0", VA = "0x1014153C0", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04006A19 RID: 27161
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004AEA")]
		public int m_ItemID;

		// Token: 0x04006A1A RID: 27162
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004AEB")]
		public int m_Num;

		// Token: 0x04006A1B RID: 27163
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004AEC")]
		public int m_ExtNum;

		// Token: 0x04006A1C RID: 27164
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004AED")]
		public bool m_Appeal;

		// Token: 0x04006A1D RID: 27165
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004AEE")]
		public Sprite m_Sprite;
	}
}
