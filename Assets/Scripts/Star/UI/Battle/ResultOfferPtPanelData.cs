﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200114B RID: 4427
	[Token(Token = "0x2000B7A")]
	[StructLayout(3)]
	public class ResultOfferPtPanelData : MonoBehaviour
	{
		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x06005736 RID: 22326 RVA: 0x0001CED8 File Offset: 0x0001B0D8
		// (set) Token: 0x06005737 RID: 22327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000573")]
		public int TitleOrder
		{
			[Token(Token = "0x6005083")]
			[Address(RVA = "0x101415470", Offset = "0x1415470", VA = "0x101415470")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6005084")]
			[Address(RVA = "0x101415478", Offset = "0x1415478", VA = "0x101415478")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x06005738 RID: 22328 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06005739 RID: 22329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000574")]
		public OfferManager.TitleData TitleBeforeData
		{
			[Token(Token = "0x6005085")]
			[Address(RVA = "0x101415480", Offset = "0x1415480", VA = "0x101415480")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6005086")]
			[Address(RVA = "0x101415488", Offset = "0x1415488", VA = "0x101415488")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x0600573A RID: 22330 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600573B RID: 22331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000575")]
		public OfferManager.TitleData TitleAfterData
		{
			[Token(Token = "0x6005087")]
			[Address(RVA = "0x101415490", Offset = "0x1415490", VA = "0x101415490")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x6005088")]
			[Address(RVA = "0x101415498", Offset = "0x1415498", VA = "0x101415498")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x0600573C RID: 22332 RVA: 0x0001CEF0 File Offset: 0x0001B0F0
		// (set) Token: 0x0600573D RID: 22333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000576")]
		public bool IsNowOccured
		{
			[Token(Token = "0x6005089")]
			[Address(RVA = "0x1014154A0", Offset = "0x14154A0", VA = "0x1014154A0")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600508A")]
			[Address(RVA = "0x1014154A8", Offset = "0x14154A8", VA = "0x1014154A8")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x0600573E RID: 22334 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x0600573F RID: 22335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000577")]
		public OfferTitleDataStatus OfferPtPanelData
		{
			[Token(Token = "0x600508B")]
			[Address(RVA = "0x1014154B0", Offset = "0x14154B0", VA = "0x1014154B0")]
			[CompilerGenerated]
			get
			{
				return null;
			}
			[Token(Token = "0x600508C")]
			[Address(RVA = "0x1014154B8", Offset = "0x14154B8", VA = "0x1014154B8")]
			[CompilerGenerated]
			private set
			{
			}
		}

		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x06005740 RID: 22336 RVA: 0x0001CF08 File Offset: 0x0001B108
		[Token(Token = "0x17000578")]
		public bool IsExist
		{
			[Token(Token = "0x600508D")]
			[Address(RVA = "0x1014154C0", Offset = "0x14154C0", VA = "0x1014154C0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06005741 RID: 22337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600508E")]
		[Address(RVA = "0x1014154C8", Offset = "0x14154C8", VA = "0x1014154C8")]
		private void SetPanelActive(bool isActive)
		{
		}

		// Token: 0x06005742 RID: 22338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600508F")]
		[Address(RVA = "0x101415504", Offset = "0x1415504", VA = "0x101415504")]
		public void Setup()
		{
		}

		// Token: 0x06005743 RID: 22339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005090")]
		[Address(RVA = "0x101415630", Offset = "0x1415630", VA = "0x101415630")]
		public void SetTitle(eTitleType tt, int titleOrder)
		{
		}

		// Token: 0x06005744 RID: 22340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005091")]
		[Address(RVA = "0x101415A04", Offset = "0x1415A04", VA = "0x101415A04")]
		public void SetIcon(int iconIndex, long MngID, CharacterParam chParam)
		{
		}

		// Token: 0x06005745 RID: 22341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005092")]
		[Address(RVA = "0x101415800", Offset = "0x1415800", VA = "0x101415800")]
		public void SetOfferPt(OfferManager.TitleData before, OfferManager.TitleData after)
		{
		}

		// Token: 0x06005746 RID: 22342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005093")]
		[Address(RVA = "0x101415BC0", Offset = "0x1415BC0", VA = "0x101415BC0")]
		public void SetTitleOfferData(OfferManager.TitleData before, OfferManager.TitleData after)
		{
		}

		// Token: 0x06005747 RID: 22343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005094")]
		[Address(RVA = "0x101415BC8", Offset = "0x1415BC8", VA = "0x101415BC8")]
		public void ApplyCountup()
		{
		}

		// Token: 0x06005748 RID: 22344 RVA: 0x0001CF20 File Offset: 0x0001B120
		[Token(Token = "0x6005095")]
		[Address(RVA = "0x101415C00", Offset = "0x1415C00", VA = "0x101415C00")]
		public bool IsCountup()
		{
			return default(bool);
		}

		// Token: 0x06005749 RID: 22345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005096")]
		[Address(RVA = "0x101415C30", Offset = "0x1415C30", VA = "0x101415C30")]
		public ResultOfferPtPanelData()
		{
		}

		// Token: 0x04006A25 RID: 27173
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004AF6")]
		private bool m_IsExist;

		// Token: 0x04006A26 RID: 27174
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004AF7")]
		[SerializeField]
		private GameObject m_TitleObj;

		// Token: 0x04006A27 RID: 27175
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004AF8")]
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x04006A28 RID: 27176
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004AF9")]
		[SerializeField]
		private Text m_GetOfferPtText;

		// Token: 0x04006A29 RID: 27177
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004AFA")]
		[SerializeField]
		private ResultOfferPtPanelData.InnerIcon[] m_Icons;

		// Token: 0x04006A2A RID: 27178
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004AFB")]
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x04006A2F RID: 27183
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004B00")]
		public PrefabCloner offerPtPanel;

		// Token: 0x0200114C RID: 4428
		[Token(Token = "0x2001292")]
		[Serializable]
		private class InnerIcon
		{
			// Token: 0x0600574A RID: 22346 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006363")]
			[Address(RVA = "0x10141596C", Offset = "0x141596C", VA = "0x10141596C")]
			public void SetActive(bool isActive)
			{
			}

			// Token: 0x0600574B RID: 22347 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006364")]
			[Address(RVA = "0x1014155A4", Offset = "0x14155A4", VA = "0x1014155A4")]
			public void SetInvisible()
			{
			}

			// Token: 0x0600574C RID: 22348 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006365")]
			[Address(RVA = "0x101415C38", Offset = "0x1415C38", VA = "0x101415C38")]
			public InnerIcon()
			{
			}

			// Token: 0x04006A31 RID: 27185
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007263")]
			public bool isExist;

			// Token: 0x04006A32 RID: 27186
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007264")]
			public Image NoExist;

			// Token: 0x04006A33 RID: 27187
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007265")]
			public PrefabCloner Exist;
		}
	}
}
