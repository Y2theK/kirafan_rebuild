﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200114D RID: 4429
	[Token(Token = "0x2000B7B")]
	[StructLayout(3)]
	public class ResultOfferPtReward : UIGroup
	{
		// Token: 0x0600574D RID: 22349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005097")]
		[Address(RVA = "0x101415C40", Offset = "0x1415C40", VA = "0x101415C40")]
		public void Setup()
		{
		}

		// Token: 0x0600574E RID: 22350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005098")]
		[Address(RVA = "0x101415DA4", Offset = "0x1415DA4", VA = "0x101415DA4")]
		public void SetTitle(BattleResult result, eTitleType tt, int titleOrder)
		{
		}

		// Token: 0x0600574F RID: 22351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005099")]
		[Address(RVA = "0x101416088", Offset = "0x1416088", VA = "0x101416088")]
		public void SetIcon(int titleOrder, long MngID, CharacterParam chParam)
		{
		}

		// Token: 0x06005750 RID: 22352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600509A")]
		[Address(RVA = "0x101416190", Offset = "0x1416190", VA = "0x101416190")]
		public void SetupPanels()
		{
		}

		// Token: 0x06005751 RID: 22353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600509B")]
		[Address(RVA = "0x1014165D4", Offset = "0x14165D4", VA = "0x1014165D4")]
		public void SetOfferPopup(eTitleTypeConverter titleConv)
		{
		}

		// Token: 0x06005752 RID: 22354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600509C")]
		[Address(RVA = "0x101416908", Offset = "0x1416908", VA = "0x101416908")]
		public void ApplyCountup()
		{
		}

		// Token: 0x06005753 RID: 22355 RVA: 0x0001CF38 File Offset: 0x0001B138
		[Token(Token = "0x600509D")]
		[Address(RVA = "0x1014169E0", Offset = "0x14169E0", VA = "0x1014169E0")]
		public bool IsCountup()
		{
			return default(bool);
		}

		// Token: 0x06005754 RID: 22356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600509E")]
		[Address(RVA = "0x101416ACC", Offset = "0x1416ACC", VA = "0x101416ACC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06005755 RID: 22357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600509F")]
		[Address(RVA = "0x101416B74", Offset = "0x1416B74", VA = "0x101416B74")]
		public void Tap()
		{
		}

		// Token: 0x06005756 RID: 22358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A0")]
		[Address(RVA = "0x101416D20", Offset = "0x1416D20", VA = "0x101416D20")]
		public ResultOfferPtReward()
		{
		}

		// Token: 0x04006A34 RID: 27188
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B02")]
		[SerializeField]
		private BattleResultUI m_OwnerUI;

		// Token: 0x04006A35 RID: 27189
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B03")]
		[SerializeField]
		private ResultOfferPtPanelData[] m_OfferPtPanels;

		// Token: 0x04006A36 RID: 27190
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B04")]
		private List<int> m_IconTitleOrders;

		// Token: 0x04006A37 RID: 27191
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B05")]
		private List<int> m_IconNextIndexs;

		// Token: 0x04006A38 RID: 27192
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004B06")]
		private int m_IconTitleNextIndex;

		// Token: 0x04006A39 RID: 27193
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004B07")]
		private bool m_isPopup;
	}
}
