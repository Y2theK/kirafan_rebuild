﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200114E RID: 4430
	[Token(Token = "0x2000B7C")]
	[StructLayout(3)]
	public class ResultPlayerReward : UIGroup
	{
		// Token: 0x06005757 RID: 22359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A1")]
		[Address(RVA = "0x101416DB0", Offset = "0x1416DB0", VA = "0x101416DB0")]
		public void Prepare()
		{
		}

		// Token: 0x06005758 RID: 22360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A2")]
		[Address(RVA = "0x101416ECC", Offset = "0x1416ECC", VA = "0x101416ECC")]
		public void SetOfferCompPopup(BattleResult result)
		{
		}

		// Token: 0x06005759 RID: 22361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A3")]
		[Address(RVA = "0x101417024", Offset = "0x1417024", VA = "0x101417024", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600575A RID: 22362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A4")]
		[Address(RVA = "0x101417590", Offset = "0x1417590", VA = "0x101417590")]
		public void SetRank(QuestDefine.eClearRank clearRank, int continueCount, int deadCount)
		{
		}

		// Token: 0x0600575B RID: 22363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A5")]
		[Address(RVA = "0x101417A4C", Offset = "0x1417A4C", VA = "0x101417A4C")]
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
		}

		// Token: 0x0600575C RID: 22364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A6")]
		[Address(RVA = "0x101417B50", Offset = "0x1417B50", VA = "0x101417B50")]
		public void ForceUpdateExp()
		{
		}

		// Token: 0x0600575D RID: 22365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050A7")]
		[Address(RVA = "0x101417C20", Offset = "0x1417C20", VA = "0x101417C20", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x0600575E RID: 22366 RVA: 0x0001CF50 File Offset: 0x0001B150
		[Token(Token = "0x60050A8")]
		[Address(RVA = "0x101417CD0", Offset = "0x1417CD0", VA = "0x101417CD0")]
		public bool IsDoneStartExp()
		{
			return default(bool);
		}

		// Token: 0x0600575F RID: 22367 RVA: 0x0001CF68 File Offset: 0x0001B168
		[Token(Token = "0x60050A9")]
		[Address(RVA = "0x101417CD8", Offset = "0x1417CD8", VA = "0x101417CD8")]
		public bool IsPlayingGaugeProgress()
		{
			return default(bool);
		}

		// Token: 0x06005760 RID: 22368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AA")]
		[Address(RVA = "0x101417D08", Offset = "0x1417D08", VA = "0x101417D08")]
		public ResultPlayerReward()
		{
		}

		// Token: 0x04006A3A RID: 27194
		[Token(Token = "0x4004B08")]
		private const float RESULTSTAR_STARTTIME = 0f;

		// Token: 0x04006A3B RID: 27195
		[Token(Token = "0x4004B09")]
		private const float RESULTSTAR_DURATION = 0.1f;

		// Token: 0x04006A3C RID: 27196
		[Token(Token = "0x4004B0A")]
		private const float RESULTSTAR_DELAY = 0.2f;

		// Token: 0x04006A3D RID: 27197
		[Token(Token = "0x4004B0B")]
		private const float EXPINCREASE_STARTTIME = 1f;

		// Token: 0x04006A3E RID: 27198
		[Token(Token = "0x4004B0C")]
		private const int LEVELUP_POPUP_MAX = 2;

		// Token: 0x04006A3F RID: 27199
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B0D")]
		[SerializeField]
		private Sprite[] m_Crowns;

		// Token: 0x04006A40 RID: 27200
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B0E")]
		[SerializeField]
		private GameObject[] m_RankStarObjArray;

		// Token: 0x04006A41 RID: 27201
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B0F")]
		[SerializeField]
		private Animation[] m_RankStarObjAnimArray;

		// Token: 0x04006A42 RID: 27202
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B10")]
		[SerializeField]
		private Text m_RankTextObj;

		// Token: 0x04006A43 RID: 27203
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004B11")]
		[SerializeField]
		private GameObject m_CompleteObj;

		// Token: 0x04006A44 RID: 27204
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004B12")]
		[SerializeField]
		private AnimUIPlayer m_CompleteAnim;

		// Token: 0x04006A45 RID: 27205
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004B13")]
		[SerializeField]
		private Text m_AddExpValue;

		// Token: 0x04006A46 RID: 27206
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004B14")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04006A47 RID: 27207
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004B15")]
		private int m_Rank;

		// Token: 0x04006A48 RID: 27208
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4004B16")]
		private bool m_IsUpdateGold;

		// Token: 0x04006A49 RID: 27209
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004B17")]
		private int m_SoundRankIdx;

		// Token: 0x04006A4A RID: 27210
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x4004B18")]
		private float m_Time;

		// Token: 0x04006A4B RID: 27211
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004B19")]
		private bool m_StartExp;

		// Token: 0x04006A4C RID: 27212
		[Cpp2IlInjected.FieldOffset(Offset = "0xD9")]
		[Token(Token = "0x4004B1A")]
		private bool m_StartSE;

		// Token: 0x04006A4D RID: 27213
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004B1B")]
		private SoundHandler m_SoundHandler;
	}
}
