﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200114F RID: 4431
	[Token(Token = "0x2000B7D")]
	[StructLayout(3)]
	public class ResultPointEventUIGroup : UIGroup
	{
		// Token: 0x06005761 RID: 22369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AB")]
		[Address(RVA = "0x101414C60", Offset = "0x1414C60", VA = "0x101414C60")]
		public void Open(long pointEventID, long gain)
		{
		}

		// Token: 0x06005762 RID: 22370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AC")]
		[Address(RVA = "0x101417D10", Offset = "0x1417D10", VA = "0x101417D10")]
		private void OnResponse()
		{
		}

		// Token: 0x06005763 RID: 22371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AD")]
		[Address(RVA = "0x101417DD4", Offset = "0x1417DD4", VA = "0x101417DD4")]
		public void SetPoint(long gain, long total, long playerTotal, long nextRewardNeedTotal)
		{
		}

		// Token: 0x06005764 RID: 22372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60050AE")]
		[Address(RVA = "0x101417FD4", Offset = "0x1417FD4", VA = "0x101417FD4")]
		public ResultPointEventUIGroup()
		{
		}

		// Token: 0x04006A4E RID: 27214
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004B1C")]
		[SerializeField]
		private Text m_GainPointText;

		// Token: 0x04006A4F RID: 27215
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004B1D")]
		[SerializeField]
		private Text m_PlayerTotalPointText;

		// Token: 0x04006A50 RID: 27216
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004B1E")]
		[SerializeField]
		private Text m_NextRewardNeedTotalPointText;

		// Token: 0x04006A51 RID: 27217
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004B1F")]
		[SerializeField]
		private Text m_TotalPointText;

		// Token: 0x04006A52 RID: 27218
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004B20")]
		private long m_PointEventID;

		// Token: 0x04006A53 RID: 27219
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004B21")]
		private long m_Gain;
	}
}
