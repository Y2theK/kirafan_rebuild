﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200112D RID: 4397
	[Token(Token = "0x2000B65")]
	[StructLayout(3)]
	public class StateIcon : MonoBehaviour
	{
		// Token: 0x0600567E RID: 22142 RVA: 0x0001CD58 File Offset: 0x0001AF58
		[Token(Token = "0x6004FD0")]
		[Address(RVA = "0x101400E14", Offset = "0x1400E14", VA = "0x101400E14")]
		public static eStateIconType ConvertUIStatusIconToType(BattleDefine.eForUIStatusIconFlag icon)
		{
			return eStateIconType.None;
		}

		// Token: 0x0600567F RID: 22143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FD1")]
		[Address(RVA = "0x10140D4E8", Offset = "0x140D4E8", VA = "0x10140D4E8")]
		public static void Sort(List<eStateIconType> iconList)
		{
		}

		// Token: 0x06005680 RID: 22144 RVA: 0x0001CD70 File Offset: 0x0001AF70
		[Token(Token = "0x6004FD2")]
		[Address(RVA = "0x101417FE4", Offset = "0x1417FE4", VA = "0x101417FE4")]
		private static int SortComp(eStateIconType l, eStateIconType r)
		{
			return 0;
		}

		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x06005681 RID: 22145 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000567")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004FD3")]
			[Address(RVA = "0x1013FF878", Offset = "0x13FF878", VA = "0x1013FF878")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x06005682 RID: 22146 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000568")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004FD4")]
			[Address(RVA = "0x1013FF880", Offset = "0x13FF880", VA = "0x1013FF880")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005683 RID: 22147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FD5")]
		[Address(RVA = "0x101418108", Offset = "0x1418108", VA = "0x101418108")]
		public void Setup(StateIconOwner owner)
		{
		}

		// Token: 0x06005684 RID: 22148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FD6")]
		[Address(RVA = "0x1013FF888", Offset = "0x13FF888", VA = "0x1013FF888")]
		public void Apply(eStateIconType type)
		{
		}

		// Token: 0x06005685 RID: 22149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FD7")]
		[Address(RVA = "0x1014181D0", Offset = "0x14181D0", VA = "0x1014181D0")]
		public StateIcon()
		{
		}

		// Token: 0x04006908 RID: 26888
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A06")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100132138", Offset = "0x132138")]
		private Image m_BaseImage;

		// Token: 0x04006909 RID: 26889
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A07")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100132184", Offset = "0x132184")]
		private Image m_MainImage;

		// Token: 0x0400690A RID: 26890
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A08")]
		private StateIconOwner m_Owner;

		// Token: 0x0400690B RID: 26891
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A09")]
		private GameObject m_GameObject;

		// Token: 0x0400690C RID: 26892
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A0A")]
		private RectTransform m_RectTransform;
	}
}
