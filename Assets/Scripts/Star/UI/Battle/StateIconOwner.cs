﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200112E RID: 4398
	[Token(Token = "0x2000B66")]
	[StructLayout(3)]
	public class StateIconOwner : MonoBehaviour
	{
		// Token: 0x06005686 RID: 22150 RVA: 0x0001CD88 File Offset: 0x0001AF88
		[Token(Token = "0x6004FD8")]
		[Address(RVA = "0x10141027C", Offset = "0x141027C", VA = "0x10141027C")]
		public float GetIconWidth()
		{
			return 0f;
		}

		// Token: 0x06005687 RID: 22151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FD9")]
		[Address(RVA = "0x1014181D8", Offset = "0x14181D8", VA = "0x1014181D8")]
		public void Setup()
		{
		}

		// Token: 0x06005688 RID: 22152 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004FDA")]
		[Address(RVA = "0x1013FF6A4", Offset = "0x13FF6A4", VA = "0x1013FF6A4")]
		public GameObject ScoopEmptyIcon()
		{
			return null;
		}

		// Token: 0x06005689 RID: 22153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FDB")]
		[Address(RVA = "0x1013FE8FC", Offset = "0x13FE8FC", VA = "0x1013FE8FC")]
		public void SinkEmptyIcon(StateIcon obj)
		{
		}

		// Token: 0x0600568A RID: 22154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FDC")]
		[Address(RVA = "0x101410360", Offset = "0x1410360", VA = "0x101410360")]
		public void SinkEmptyIcon(GameObject obj)
		{
		}

		// Token: 0x0600568B RID: 22155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FDD")]
		[Address(RVA = "0x101418740", Offset = "0x1418740", VA = "0x101418740")]
		public void Destroy()
		{
		}

		// Token: 0x0600568C RID: 22156 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004FDE")]
		[Address(RVA = "0x101418180", Offset = "0x1418180", VA = "0x101418180")]
		public StateIconOwner.StateIconData GetIconData(eStateIconType type)
		{
			return null;
		}

		// Token: 0x0600568D RID: 22157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FDF")]
		[Address(RVA = "0x101418898", Offset = "0x1418898", VA = "0x101418898")]
		public StateIconOwner()
		{
		}

		// Token: 0x0400690D RID: 26893
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A0B")]
		[SerializeField]
		private StateIcon m_IconPrefab;

		// Token: 0x0400690E RID: 26894
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A0C")]
		[SerializeField]
		private int m_DefaultCacheSize;

		// Token: 0x0400690F RID: 26895
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A0D")]
		private RectTransform m_PrefabRect;

		// Token: 0x04006910 RID: 26896
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A0E")]
		private StateIconOwner.StateIconData[] m_StateIconDatas;

		// Token: 0x04006911 RID: 26897
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A0F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001321F0", Offset = "0x1321F0")]
		private Sprite[] m_IconSprites;

		// Token: 0x04006912 RID: 26898
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A10")]
		private List<GameObject> m_EmptyList;

		// Token: 0x04006913 RID: 26899
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A11")]
		private List<GameObject> m_ActiveList;

		// Token: 0x04006914 RID: 26900
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A12")]
		private RectTransform m_RectTransform;

		// Token: 0x0200112F RID: 4399
		[Token(Token = "0x2001289")]
		public class StateIconData
		{
			// Token: 0x0600568E RID: 22158 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635E")]
			[Address(RVA = "0x1014186F0", Offset = "0x14186F0", VA = "0x1014186F0")]
			public StateIconData(eStateIconType type, StateIconListDB_Param param)
			{
			}

			// Token: 0x04006915 RID: 26901
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007236")]
			public eStateIconType m_Type;

			// Token: 0x04006916 RID: 26902
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007237")]
			public Sprite m_BaseSprite;

			// Token: 0x04006917 RID: 26903
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007238")]
			public Sprite m_MainSprite;

			// Token: 0x04006918 RID: 26904
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007239")]
			public StateIconListDB_Param m_DBParam;
		}
	}
}
