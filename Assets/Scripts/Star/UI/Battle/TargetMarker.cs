﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001130 RID: 4400
	[Token(Token = "0x2000B67")]
	[StructLayout(3)]
	public class TargetMarker : MonoBehaviour
	{
		// Token: 0x0600568F RID: 22159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE0")]
		[Address(RVA = "0x101418940", Offset = "0x1418940", VA = "0x101418940")]
		public void Setup(BattleUI owner)
		{
		}

		// Token: 0x06005690 RID: 22160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE1")]
		[Address(RVA = "0x1014189EC", Offset = "0x14189EC", VA = "0x1014189EC")]
		private void Update()
		{
		}

		// Token: 0x06005691 RID: 22161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE2")]
		[Address(RVA = "0x101418CB4", Offset = "0x1418CB4", VA = "0x101418CB4")]
		public void SetMode(TargetMarker.eMode mode)
		{
		}

		// Token: 0x06005692 RID: 22162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE3")]
		[Address(RVA = "0x101418F98", Offset = "0x1418F98", VA = "0x101418F98")]
		public void SetTransform(Transform refTransform, Vector3 offsetFromRefTransform)
		{
		}

		// Token: 0x06005693 RID: 22163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE4")]
		[Address(RVA = "0x101418AC8", Offset = "0x1418AC8", VA = "0x101418AC8")]
		private void UpdatePosition()
		{
		}

		// Token: 0x06005694 RID: 22164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE5")]
		[Address(RVA = "0x101418FAC", Offset = "0x1418FAC", VA = "0x101418FAC")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06005695 RID: 22165 RVA: 0x0001CDA0 File Offset: 0x0001AFA0
		[Token(Token = "0x6004FE6")]
		[Address(RVA = "0x101418A20", Offset = "0x1418A20", VA = "0x101418A20")]
		public bool IsEnableRender()
		{
			return default(bool);
		}

		// Token: 0x06005696 RID: 22166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE7")]
		[Address(RVA = "0x101419064", Offset = "0x1419064", VA = "0x101419064")]
		public TargetMarker()
		{
		}

		// Token: 0x04006919 RID: 26905
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A13")]
		[SerializeField]
		private Animation[] m_Animations;

		// Token: 0x0400691A RID: 26906
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A14")]
		private Image[] m_Images;

		// Token: 0x0400691B RID: 26907
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A15")]
		[SerializeField]
		private GameObject m_TargetParent;

		// Token: 0x0400691C RID: 26908
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A16")]
		[SerializeField]
		private Sprite m_NormalSprite;

		// Token: 0x0400691D RID: 26909
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A17")]
		[SerializeField]
		private Sprite m_WeakSprite;

		// Token: 0x0400691E RID: 26910
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A18")]
		[SerializeField]
		private Sprite m_ResistSprite;

		// Token: 0x0400691F RID: 26911
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A19")]
		[SerializeField]
		private Image m_WeakLabelImage;

		// Token: 0x04006920 RID: 26912
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A1A")]
		[SerializeField]
		private Image m_ResistLabelImage;

		// Token: 0x04006921 RID: 26913
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004A1B")]
		private BattleUI m_Owner;

		// Token: 0x04006922 RID: 26914
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004A1C")]
		private Transform m_RefTransform;

		// Token: 0x04006923 RID: 26915
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004A1D")]
		private Vector3 m_OffsetFromRefTransform;

		// Token: 0x04006924 RID: 26916
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004A1E")]
		private GameObject m_GameObject;

		// Token: 0x04006925 RID: 26917
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004A1F")]
		private RectTransform m_Transform;

		// Token: 0x02001131 RID: 4401
		[Token(Token = "0x200128A")]
		public enum eMode
		{
			// Token: 0x04006927 RID: 26919
			[Token(Token = "0x400723B")]
			Normal,
			// Token: 0x04006928 RID: 26920
			[Token(Token = "0x400723C")]
			Weak,
			// Token: 0x04006929 RID: 26921
			[Token(Token = "0x400723D")]
			Resist
		}
	}
}
