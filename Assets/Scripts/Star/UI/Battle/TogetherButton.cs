﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001132 RID: 4402
	[Token(Token = "0x2000B68")]
	[StructLayout(3)]
	public class TogetherButton : MonoBehaviour
	{
		// Token: 0x06005697 RID: 22167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE8")]
		[Address(RVA = "0x10141906C", Offset = "0x141906C", VA = "0x10141906C")]
		private void Awake()
		{
		}

		// Token: 0x06005698 RID: 22168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FE9")]
		[Address(RVA = "0x101419450", Offset = "0x1419450", VA = "0x101419450")]
		private void Update()
		{
		}

		// Token: 0x06005699 RID: 22169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FEA")]
		[Address(RVA = "0x101419120", Offset = "0x1419120", VA = "0x101419120")]
		public void Apply(float gauge, bool isImmediate, bool isSealed)
		{
		}

		// Token: 0x0600569A RID: 22170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FEB")]
		[Address(RVA = "0x101419288", Offset = "0x1419288", VA = "0x101419288")]
		private void SetEnableFlash(bool flg)
		{
		}

		// Token: 0x0600569B RID: 22171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FEC")]
		[Address(RVA = "0x10141973C", Offset = "0x141973C", VA = "0x10141973C")]
		public void SetEnableRender(bool flag)
		{
		}

		// Token: 0x0600569C RID: 22172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FED")]
		[Address(RVA = "0x101419778", Offset = "0x1419778", VA = "0x101419778")]
		public void SetEnableInput(bool flag)
		{
		}

		// Token: 0x0600569D RID: 22173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FEE")]
		[Address(RVA = "0x1014196AC", Offset = "0x14196AC", VA = "0x1014196AC")]
		public void SetExecutableTogetherAttack(bool flag)
		{
		}

		// Token: 0x0600569E RID: 22174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FEF")]
		[Address(RVA = "0x101419800", Offset = "0x1419800", VA = "0x101419800")]
		public TogetherButton()
		{
		}

		// Token: 0x0400692A RID: 26922
		[Token(Token = "0x4004A20")]
		private const float GAUGE_INCREASE_SPEED_PER_SEC = 0.5f;

		// Token: 0x0400692B RID: 26923
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A21")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x0400692C RID: 26924
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A22")]
		[SerializeField]
		private Animation m_InteractableAnimation;

		// Token: 0x0400692D RID: 26925
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A23")]
		[SerializeField]
		private GameObject m_SelectedAnimObj;

		// Token: 0x0400692E RID: 26926
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A24")]
		[SerializeField]
		private Image m_GaugeBody;

		// Token: 0x0400692F RID: 26927
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A25")]
		[SerializeField]
		private GameObject m_GaugeMaxObj;

		// Token: 0x04006930 RID: 26928
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A26")]
		[SerializeField]
		private GameObject[] m_StarObjs;

		// Token: 0x04006931 RID: 26929
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A27")]
		private bool m_IsEnableInput;

		// Token: 0x04006932 RID: 26930
		[Cpp2IlInjected.FieldOffset(Offset = "0x49")]
		[Token(Token = "0x4004A28")]
		private bool m_Executable;

		// Token: 0x04006933 RID: 26931
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004A29")]
		private float m_GoalGauge;

		// Token: 0x04006934 RID: 26932
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A2A")]
		private float m_DispGauge;

		// Token: 0x04006935 RID: 26933
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004A2B")]
		private float m_GaugeSpeed;
	}
}
