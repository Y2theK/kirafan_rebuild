﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001133 RID: 4403
	[Token(Token = "0x2000B69")]
	[StructLayout(3)]
	public class TogetherCutIn : MonoBehaviour
	{
		// Token: 0x0600569F RID: 22175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF0")]
		[Address(RVA = "0x101419810", Offset = "0x1419810", VA = "0x101419810")]
		private void Start()
		{
		}

		// Token: 0x060056A0 RID: 22176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF1")]
		[Address(RVA = "0x10141985C", Offset = "0x141985C", VA = "0x10141985C")]
		public void Setup(BattleUI owner)
		{
		}

		// Token: 0x060056A1 RID: 22177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF2")]
		[Address(RVA = "0x101419864", Offset = "0x1419864", VA = "0x101419864")]
		private void Destroy()
		{
		}

		// Token: 0x060056A2 RID: 22178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF3")]
		[Address(RVA = "0x101419900", Offset = "0x1419900", VA = "0x101419900")]
		public void ShowChara()
		{
		}

		// Token: 0x060056A3 RID: 22179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF4")]
		[Address(RVA = "0x10141990C", Offset = "0x141990C", VA = "0x10141990C")]
		public void End()
		{
		}

		// Token: 0x060056A4 RID: 22180 RVA: 0x0001CDB8 File Offset: 0x0001AFB8
		[Token(Token = "0x6004FF5")]
		[Address(RVA = "0x1014199C4", Offset = "0x14199C4", VA = "0x1014199C4")]
		public bool IsFilledScreen()
		{
			return default(bool);
		}

		// Token: 0x060056A5 RID: 22181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF6")]
		[Address(RVA = "0x1014199D4", Offset = "0x14199D4", VA = "0x1014199D4")]
		private void Update()
		{
		}

		// Token: 0x060056A6 RID: 22182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF7")]
		[Address(RVA = "0x101419FC8", Offset = "0x1419FC8", VA = "0x101419FC8")]
		public void Play(int[] charaIDs)
		{
		}

		// Token: 0x060056A7 RID: 22183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FF8")]
		[Address(RVA = "0x10141A29C", Offset = "0x141A29C", VA = "0x10141A29C")]
		public TogetherCutIn()
		{
		}

		// Token: 0x04006936 RID: 26934
		[Token(Token = "0x4004A2C")]
		private const float CHARAIN_START_TIME = 0.1f;

		// Token: 0x04006937 RID: 26935
		[Token(Token = "0x4004A2D")]
		private const float CHARAIN_INTERVAL = 0.2f;

		// Token: 0x04006938 RID: 26936
		[Token(Token = "0x4004A2E")]
		private const float SHOWCHARA_TIME = 1.2f;

		// Token: 0x04006939 RID: 26937
		[Token(Token = "0x4004A2F")]
		private const float ENDFADE_TIME = 0.2f;

		// Token: 0x0400693A RID: 26938
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A30")]
		[SerializeField]
		private GameObject m_CharaObj;

		// Token: 0x0400693B RID: 26939
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A31")]
		[SerializeField]
		private GameObject[] m_CharaObjs;

		// Token: 0x0400693C RID: 26940
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A32")]
		[SerializeField]
		private BustImage[] m_BustImages;

		// Token: 0x0400693D RID: 26941
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A33")]
		[SerializeField]
		private Animation[] m_CharaAnims;

		// Token: 0x0400693E RID: 26942
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A34")]
		[SerializeField]
		private BattleFade m_Fade;

		// Token: 0x0400693F RID: 26943
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A35")]
		[SerializeField]
		private TogetherSelect m_SelectUI;

		// Token: 0x04006940 RID: 26944
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A36")]
		private TogetherCutIn.eStep m_Step;

		// Token: 0x04006941 RID: 26945
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004A37")]
		private bool m_IsReserveShowChara;

		// Token: 0x04006942 RID: 26946
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A38")]
		private float m_Timer;

		// Token: 0x04006943 RID: 26947
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004A39")]
		private int m_InCharaNum;

		// Token: 0x04006944 RID: 26948
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004A3A")]
		private int m_InCharaIdx;

		// Token: 0x04006945 RID: 26949
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004A3B")]
		private BattleUI m_Owner;

		// Token: 0x02001134 RID: 4404
		[Token(Token = "0x200128B")]
		private enum eStep
		{
			// Token: 0x04006947 RID: 26951
			[Token(Token = "0x400723F")]
			Hide,
			// Token: 0x04006948 RID: 26952
			[Token(Token = "0x4007240")]
			WaitStartFade,
			// Token: 0x04006949 RID: 26953
			[Token(Token = "0x4007241")]
			WaitPrepare,
			// Token: 0x0400694A RID: 26954
			[Token(Token = "0x4007242")]
			CharaIn,
			// Token: 0x0400694B RID: 26955
			[Token(Token = "0x4007243")]
			WaitCharaIn,
			// Token: 0x0400694C RID: 26956
			[Token(Token = "0x4007244")]
			ShowChara,
			// Token: 0x0400694D RID: 26957
			[Token(Token = "0x4007245")]
			WaitEndFadeOut,
			// Token: 0x0400694E RID: 26958
			[Token(Token = "0x4007246")]
			Filled
		}
	}
}
