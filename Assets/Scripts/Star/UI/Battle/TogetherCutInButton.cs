﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001135 RID: 4405
	[Token(Token = "0x2000B6A")]
	[StructLayout(3)]
	public class TogetherCutInButton : MonoBehaviour
	{
		// Token: 0x1400013C RID: 316
		// (add) Token: 0x060056A8 RID: 22184 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060056A9 RID: 22185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013C")]
		public event Action<int> OnClickButton
		{
			[Token(Token = "0x6004FF9")]
			[Address(RVA = "0x10141A2A4", Offset = "0x141A2A4", VA = "0x10141A2A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004FFA")]
			[Address(RVA = "0x10141A3B0", Offset = "0x141A3B0", VA = "0x10141A3B0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400013D RID: 317
		// (add) Token: 0x060056AA RID: 22186 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060056AB RID: 22187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013D")]
		public event Action<int> OnHoldButton
		{
			[Token(Token = "0x6004FFB")]
			[Address(RVA = "0x10141A4BC", Offset = "0x141A4BC", VA = "0x10141A4BC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004FFC")]
			[Address(RVA = "0x10141A5C8", Offset = "0x141A5C8", VA = "0x10141A5C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x060056AC RID: 22188 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000569")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004FFD")]
			[Address(RVA = "0x10141A6D4", Offset = "0x141A6D4", VA = "0x10141A6D4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x060056AD RID: 22189 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700056A")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004FFE")]
			[Address(RVA = "0x10141A6DC", Offset = "0x141A6DC", VA = "0x10141A6DC")]
			get
			{
				return null;
			}
		}

		// Token: 0x060056AE RID: 22190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004FFF")]
		[Address(RVA = "0x10141A6E4", Offset = "0x141A6E4", VA = "0x10141A6E4")]
		public void Setup(int idx)
		{
		}

		// Token: 0x060056AF RID: 22191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005000")]
		[Address(RVA = "0x10141AA90", Offset = "0x141AA90", VA = "0x10141AA90")]
		private void Update()
		{
		}

		// Token: 0x060056B0 RID: 22192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005001")]
		[Address(RVA = "0x10141AB78", Offset = "0x141AB78", VA = "0x10141AB78")]
		public void Play()
		{
		}

		// Token: 0x060056B1 RID: 22193 RVA: 0x0001CDD0 File Offset: 0x0001AFD0
		[Token(Token = "0x6005002")]
		[Address(RVA = "0x10141ABBC", Offset = "0x141ABBC", VA = "0x10141ABBC")]
		public bool IsPlay()
		{
			return default(bool);
		}

		// Token: 0x060056B2 RID: 22194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005003")]
		[Address(RVA = "0x10141ABEC", Offset = "0x141ABEC", VA = "0x10141ABEC")]
		public void SetChara(int charaID)
		{
		}

		// Token: 0x060056B3 RID: 22195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005004")]
		[Address(RVA = "0x10141AD28", Offset = "0x141AD28", VA = "0x10141AD28")]
		public void SetCharaElement(eElementType element)
		{
		}

		// Token: 0x060056B4 RID: 22196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005005")]
		[Address(RVA = "0x10141A80C", Offset = "0x141A80C", VA = "0x10141A80C")]
		public void SetNumber(int num)
		{
		}

		// Token: 0x060056B5 RID: 22197 RVA: 0x0001CDE8 File Offset: 0x0001AFE8
		[Token(Token = "0x6005006")]
		[Address(RVA = "0x10141AE28", Offset = "0x141AE28", VA = "0x10141AE28")]
		public int GetNumber()
		{
			return 0;
		}

		// Token: 0x060056B6 RID: 22198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005007")]
		[Address(RVA = "0x10141AE30", Offset = "0x141AE30", VA = "0x10141AE30")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x060056B7 RID: 22199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005008")]
		[Address(RVA = "0x10141AEB0", Offset = "0x141AEB0", VA = "0x10141AEB0")]
		public void OnHoldCallBack()
		{
		}

		// Token: 0x060056B8 RID: 22200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005009")]
		[Address(RVA = "0x10141AF30", Offset = "0x141AF30", VA = "0x10141AF30")]
		public void Flash()
		{
		}

		// Token: 0x060056B9 RID: 22201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600500A")]
		[Address(RVA = "0x10141AFBC", Offset = "0x141AFBC", VA = "0x10141AFBC")]
		public void SetEnableDark(bool flg)
		{
		}

		// Token: 0x060056BA RID: 22202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600500B")]
		[Address(RVA = "0x10141B00C", Offset = "0x141B00C", VA = "0x10141B00C")]
		public TogetherCutInButton()
		{
		}

		// Token: 0x0400694F RID: 26959
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A3C")]
		private readonly Color NUMCOLOR_1;

		// Token: 0x04006950 RID: 26960
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A3D")]
		private readonly Color NUMCOLOR_2;

		// Token: 0x04006951 RID: 26961
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A3E")]
		private readonly Color NUMCOLOR_3;

		// Token: 0x04006952 RID: 26962
		[Token(Token = "0x4004A3F")]
		private const float FLASH_SEC = 0.2f;

		// Token: 0x04006953 RID: 26963
		[Token(Token = "0x4004A40")]
		private const float FRAME_HEIGHT = 262f;

		// Token: 0x04006954 RID: 26964
		[Token(Token = "0x4004A41")]
		private const float DEFAULT_FRAME_POSITION_Y = 250f;

		// Token: 0x04006955 RID: 26965
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A42")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04006956 RID: 26966
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A43")]
		[SerializeField]
		private Image m_BackGroundImage;

		// Token: 0x04006957 RID: 26967
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004A44")]
		[SerializeField]
		private ColorGroup m_BackGroundEffect;

		// Token: 0x04006958 RID: 26968
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004A45")]
		[SerializeField]
		private Sprite[] m_NumberSprite;

		// Token: 0x04006959 RID: 26969
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004A46")]
		[SerializeField]
		private Sprite[] m_NumberExtendSprite;

		// Token: 0x0400695A RID: 26970
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004A47")]
		[SerializeField]
		private GameObject m_NumberObj;

		// Token: 0x0400695B RID: 26971
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004A48")]
		[SerializeField]
		private ColorGroup m_NumberColorGroup;

		// Token: 0x0400695C RID: 26972
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004A49")]
		[SerializeField]
		private Image m_NumberImage;

		// Token: 0x0400695D RID: 26973
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004A4A")]
		[SerializeField]
		private Image m_NumberExtendImage;

		// Token: 0x0400695E RID: 26974
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004A4B")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x0400695F RID: 26975
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004A4C")]
		[SerializeField]
		private Image m_FlashImage;

		// Token: 0x04006960 RID: 26976
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004A4D")]
		[SerializeField]
		private Image m_DarkImage;

		// Token: 0x04006961 RID: 26977
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004A4E")]
		private int m_Idx;

		// Token: 0x04006962 RID: 26978
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004A4F")]
		private int m_Number;

		// Token: 0x04006963 RID: 26979
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004A50")]
		private Animation m_Anim;

		// Token: 0x04006966 RID: 26982
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004A53")]
		private float m_BeforeTime;

		// Token: 0x04006967 RID: 26983
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004A54")]
		private RectTransform m_RectTransform;

		// Token: 0x04006968 RID: 26984
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004A55")]
		private GameObject m_GameObject;
	}
}
