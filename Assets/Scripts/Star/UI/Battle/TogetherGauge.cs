﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001136 RID: 4406
	[Token(Token = "0x2000B6B")]
	[StructLayout(3)]
	public class TogetherGauge : MonoBehaviour
	{
		// Token: 0x060056BB RID: 22203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600500C")]
		[Address(RVA = "0x10141B0D0", Offset = "0x141B0D0", VA = "0x10141B0D0")]
		public void Apply(float value)
		{
		}

		// Token: 0x060056BC RID: 22204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600500D")]
		[Address(RVA = "0x10141B3D4", Offset = "0x141B3D4", VA = "0x10141B3D4")]
		public TogetherGauge()
		{
		}

		// Token: 0x04006969 RID: 26985
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A56")]
		private float[] OCCUPATION;

		// Token: 0x0400696A RID: 26986
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A57")]
		private float[] OCCUPATION_START;

		// Token: 0x0400696B RID: 26987
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A58")]
		private float[] OCCUPATION_END;

		// Token: 0x0400696C RID: 26988
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A59")]
		[SerializeField]
		private Image[] m_GaugeMask;
	}
}
