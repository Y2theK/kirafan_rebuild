﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001137 RID: 4407
	[Token(Token = "0x2000B6C")]
	[StructLayout(3)]
	public class TogetherSelect : UIGroup
	{
		// Token: 0x1400013E RID: 318
		// (add) Token: 0x060056BD RID: 22205 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060056BE RID: 22206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013E")]
		public event Action<BattleDefine.eJoinMember> OnDecideMember
		{
			[Token(Token = "0x600500E")]
			[Address(RVA = "0x10141B4A4", Offset = "0x141B4A4", VA = "0x10141B4A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600500F")]
			[Address(RVA = "0x10141B5B4", Offset = "0x141B5B4", VA = "0x10141B5B4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400013F RID: 319
		// (add) Token: 0x060056BF RID: 22207 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060056C0 RID: 22208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400013F")]
		public event Action OnDecideTogether
		{
			[Token(Token = "0x6005010")]
			[Address(RVA = "0x10141B6C4", Offset = "0x141B6C4", VA = "0x10141B6C4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6005011")]
			[Address(RVA = "0x10141B7D4", Offset = "0x141B7D4", VA = "0x10141B7D4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000140 RID: 320
		// (add) Token: 0x060056C1 RID: 22209 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060056C2 RID: 22210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000140")]
		public event Action OnCancelTogether
		{
			[Token(Token = "0x6005012")]
			[Address(RVA = "0x10141B8E4", Offset = "0x141B8E4", VA = "0x10141B8E4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6005013")]
			[Address(RVA = "0x10141B9F4", Offset = "0x141B9F4", VA = "0x10141B9F4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060056C3 RID: 22211 RVA: 0x0001CE00 File Offset: 0x0001B000
		[Token(Token = "0x6005014")]
		[Address(RVA = "0x10141BB04", Offset = "0x141BB04", VA = "0x10141BB04")]
		public bool IsSelectEnd()
		{
			return default(bool);
		}

		// Token: 0x060056C4 RID: 22212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005015")]
		[Address(RVA = "0x10141BB0C", Offset = "0x141BB0C", VA = "0x10141BB0C")]
		public void Setup()
		{
		}

		// Token: 0x060056C5 RID: 22213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005016")]
		[Address(RVA = "0x10141BD90", Offset = "0x141BD90", VA = "0x10141BD90")]
		public void SetCharacter(int[] charaIDs, eElementType[] elements, int[] skillIDs, BattleDefine.eJoinMember[] join)
		{
		}

		// Token: 0x060056C6 RID: 22214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005017")]
		[Address(RVA = "0x10141C09C", Offset = "0x141C09C", VA = "0x10141C09C")]
		public void SetSelectableMax(int max)
		{
		}

		// Token: 0x060056C7 RID: 22215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005018")]
		[Address(RVA = "0x10141C1D0", Offset = "0x141C1D0", VA = "0x10141C1D0")]
		public void ResetSelectOrder()
		{
		}

		// Token: 0x060056C8 RID: 22216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005019")]
		[Address(RVA = "0x10141C2B4", Offset = "0x141C2B4", VA = "0x10141C2B4", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060056C9 RID: 22217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600501A")]
		[Address(RVA = "0x10141C640", Offset = "0x141C640", VA = "0x10141C640", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060056CA RID: 22218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600501B")]
		[Address(RVA = "0x10141C648", Offset = "0x141C648", VA = "0x10141C648")]
		public void Destroy()
		{
		}

		// Token: 0x060056CB RID: 22219 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600501C")]
		[Address(RVA = "0x10141C6E0", Offset = "0x141C6E0", VA = "0x10141C6E0")]
		public List<BattleDefine.eJoinMember> GetSelectOrderJoin()
		{
			return null;
		}

		// Token: 0x060056CC RID: 22220 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600501D")]
		[Address(RVA = "0x10141C854", Offset = "0x141C854", VA = "0x10141C854")]
		public List<int> GetSelectOrderCharaIDs()
		{
			return null;
		}

		// Token: 0x060056CD RID: 22221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600501E")]
		[Address(RVA = "0x10141C9C8", Offset = "0x141C9C8", VA = "0x10141C9C8")]
		public void OnPressCallBack(int idx)
		{
		}

		// Token: 0x060056CE RID: 22222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600501F")]
		[Address(RVA = "0x10141CBB0", Offset = "0x141CBB0", VA = "0x10141CBB0")]
		public void SelectChara(int idx)
		{
		}

		// Token: 0x060056CF RID: 22223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005020")]
		[Address(RVA = "0x10141D50C", Offset = "0x141D50C", VA = "0x10141D50C")]
		public void OnHoldCallBack(int idx)
		{
		}

		// Token: 0x060056D0 RID: 22224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005021")]
		[Address(RVA = "0x10141D510", Offset = "0x141D510", VA = "0x10141D510")]
		public void DispDescript(int idx)
		{
		}

		// Token: 0x060056D1 RID: 22225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005022")]
		[Address(RVA = "0x10141CAC0", Offset = "0x141CAC0", VA = "0x10141CAC0")]
		public void CancelDescript()
		{
		}

		// Token: 0x060056D2 RID: 22226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005023")]
		[Address(RVA = "0x10141D7CC", Offset = "0x141D7CC", VA = "0x10141D7CC")]
		public void OnClickDecideCallBack()
		{
		}

		// Token: 0x060056D3 RID: 22227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005024")]
		[Address(RVA = "0x10141D89C", Offset = "0x141D89C", VA = "0x10141D89C")]
		public void OnClickCancelCallBack()
		{
		}

		// Token: 0x060056D4 RID: 22228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005025")]
		[Address(RVA = "0x10141D8D8", Offset = "0x141D8D8", VA = "0x10141D8D8")]
		public TogetherSelect()
		{
		}

		// Token: 0x0400696D RID: 26989
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004A5A")]
		[SerializeField]
		private TogetherCutInButton[] m_CutInButtons;

		// Token: 0x0400696E RID: 26990
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004A5B")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x0400696F RID: 26991
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004A5C")]
		[SerializeField]
		private UIGroup m_DescriptWindow;

		// Token: 0x04006970 RID: 26992
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004A5D")]
		[SerializeField]
		private Text m_DescriptNameText;

		// Token: 0x04006971 RID: 26993
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004A5E")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04006972 RID: 26994
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004A5F")]
		[SerializeField]
		private CustomButton m_DescriptCancelImage;

		// Token: 0x04006973 RID: 26995
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004A60")]
		[SerializeField]
		private ColorGroup[] m_GaugeStars;

		// Token: 0x04006974 RID: 26996
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004A61")]
		[SerializeField]
		private GameObject[] m_GaugeStarEffects;

		// Token: 0x04006975 RID: 26997
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004A62")]
		[SerializeField]
		private Animation m_LabelAnim;

		// Token: 0x04006976 RID: 26998
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004A63")]
		[SerializeField]
		private OrderTogetherFrame m_RainbowScrAnim;

		// Token: 0x04006977 RID: 26999
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004A64")]
		private int m_SelectingNumber;

		// Token: 0x04006978 RID: 27000
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004A65")]
		private int[] m_SelectOrder;

		// Token: 0x04006979 RID: 27001
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004A66")]
		private int m_SelectableMax;

		// Token: 0x0400697A RID: 27002
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004A67")]
		private BattleDefine.eJoinMember[] m_JoinMembers;

		// Token: 0x0400697B RID: 27003
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004A68")]
		private int[] m_CharaIDs;

		// Token: 0x0400697C RID: 27004
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004A69")]
		private int[] m_SkillIDs;
	}
}
