﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02001138 RID: 4408
	[Token(Token = "0x2000B6D")]
	[StructLayout(3)]
	public class TotalDamage : MonoBehaviour
	{
		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x060056D5 RID: 22229 RVA: 0x0001CE18 File Offset: 0x0001B018
		[Token(Token = "0x1700056B")]
		public bool IsPlaying
		{
			[Token(Token = "0x6005026")]
			[Address(RVA = "0x10141D8E8", Offset = "0x141D8E8", VA = "0x10141D8E8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060056D6 RID: 22230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005027")]
		[Address(RVA = "0x10141D8F0", Offset = "0x141D8F0", VA = "0x10141D8F0")]
		private void Setup()
		{
		}

		// Token: 0x060056D7 RID: 22231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005028")]
		[Address(RVA = "0x10141DA54", Offset = "0x141DA54", VA = "0x10141DA54")]
		public void PlayIn(int value, int bonus)
		{
		}

		// Token: 0x060056D8 RID: 22232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005029")]
		[Address(RVA = "0x10141DE68", Offset = "0x141DE68", VA = "0x10141DE68")]
		public void PlayOut()
		{
		}

		// Token: 0x060056D9 RID: 22233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600502A")]
		[Address(RVA = "0x10141DE98", Offset = "0x141DE98", VA = "0x10141DE98")]
		private void Update()
		{
		}

		// Token: 0x060056DA RID: 22234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600502B")]
		[Address(RVA = "0x10141E66C", Offset = "0x141E66C", VA = "0x10141E66C")]
		public TotalDamage()
		{
		}

		// Token: 0x04006980 RID: 27008
		[Token(Token = "0x4004A6D")]
		private const float START_SCALE = 1.25f;

		// Token: 0x04006981 RID: 27009
		[Token(Token = "0x4004A6E")]
		private const float SCALE_START_TIME = 0.75f;

		// Token: 0x04006982 RID: 27010
		[Token(Token = "0x4004A6F")]
		private const float SCALE_DURATION = 0.3f;

		// Token: 0x04006983 RID: 27011
		[Token(Token = "0x4004A70")]
		private const float SCALE_TIME = 0.2f;

		// Token: 0x04006984 RID: 27012
		[Token(Token = "0x4004A71")]
		private const float END_TIME = 0f;

		// Token: 0x04006985 RID: 27013
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A72")]
		[SerializeField]
		private ImageNumbers m_Nums;

		// Token: 0x04006986 RID: 27014
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A73")]
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04006987 RID: 27015
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A74")]
		[SerializeField]
		private RectTransform m_Title;

		// Token: 0x04006988 RID: 27016
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A75")]
		[SerializeField]
		private AnimUIPlayer m_BonusAnim;

		// Token: 0x04006989 RID: 27017
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A76")]
		[SerializeField]
		private Image m_BonusImage;

		// Token: 0x0400698A RID: 27018
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A77")]
		[SerializeField]
		private Sprite[] m_BonusSprites;

		// Token: 0x0400698B RID: 27019
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A78")]
		private RectTransform[] m_NumRects;

		// Token: 0x0400698C RID: 27020
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004A79")]
		private int m_Digit;

		// Token: 0x0400698D RID: 27021
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004A7A")]
		private bool m_IsDoneSetup;

		// Token: 0x0400698E RID: 27022
		[Cpp2IlInjected.FieldOffset(Offset = "0x55")]
		[Token(Token = "0x4004A7B")]
		private bool m_IsPlaying;

		// Token: 0x0400698F RID: 27023
		[Cpp2IlInjected.FieldOffset(Offset = "0x56")]
		[Token(Token = "0x4004A7C")]
		private bool m_StartScale;

		// Token: 0x04006990 RID: 27024
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004A7D")]
		private float m_PlayTime;
	}
}
