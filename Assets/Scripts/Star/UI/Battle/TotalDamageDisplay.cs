﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02001139 RID: 4409
	[Token(Token = "0x2000B6E")]
	[StructLayout(3)]
	public class TotalDamageDisplay : MonoBehaviour
	{
		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x060056DB RID: 22235 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700056C")]
		public GameObject GameObject
		{
			[Token(Token = "0x600502C")]
			[Address(RVA = "0x10141E674", Offset = "0x141E674", VA = "0x10141E674")]
			get
			{
				return null;
			}
		}

		// Token: 0x060056DC RID: 22236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600502D")]
		[Address(RVA = "0x10141E704", Offset = "0x141E704", VA = "0x10141E704")]
		public void Play(float time, int totalDamage, int totalRecover, int bonus)
		{
		}

		// Token: 0x060056DD RID: 22237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600502E")]
		[Address(RVA = "0x10141E848", Offset = "0x141E848", VA = "0x10141E848")]
		public void PlayTotalDamageOnly(float time, int totalDamage, int bonus)
		{
		}

		// Token: 0x060056DE RID: 22238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600502F")]
		[Address(RVA = "0x10141E854", Offset = "0x141E854", VA = "0x10141E854")]
		public void PlayTotalRecoverOnly(float time, int totalRecover, int bonus)
		{
		}

		// Token: 0x060056DF RID: 22239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005030")]
		[Address(RVA = "0x10141E864", Offset = "0x141E864", VA = "0x10141E864")]
		private void Update()
		{
		}

		// Token: 0x060056E0 RID: 22240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005031")]
		[Address(RVA = "0x10141E954", Offset = "0x141E954", VA = "0x10141E954")]
		public TotalDamageDisplay()
		{
		}

		// Token: 0x04006991 RID: 27025
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A7E")]
		[SerializeField]
		private TotalDamage m_Number;

		// Token: 0x04006992 RID: 27026
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A7F")]
		[SerializeField]
		private TotalDamage m_RecoverNumber;

		// Token: 0x04006993 RID: 27027
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A80")]
		[SerializeField]
		private GameObject m_NumberObj;

		// Token: 0x04006994 RID: 27028
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A81")]
		[SerializeField]
		private GameObject m_RecoverObj;

		// Token: 0x04006995 RID: 27029
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A82")]
		private float m_PlayTime;

		// Token: 0x04006996 RID: 27030
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004A83")]
		private float m_DisplayTime;

		// Token: 0x04006997 RID: 27031
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A84")]
		private bool m_IsPlay;

		// Token: 0x04006998 RID: 27032
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A85")]
		private GameObject m_GameObject;
	}
}
