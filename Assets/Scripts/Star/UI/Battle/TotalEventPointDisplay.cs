﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200113A RID: 4410
	[Token(Token = "0x2000B6F")]
	[StructLayout(3)]
	public class TotalEventPointDisplay : MonoBehaviour
	{
		// Token: 0x060056E1 RID: 22241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005032")]
		[Address(RVA = "0x10141E95C", Offset = "0x141E95C", VA = "0x10141E95C")]
		public void Apply(int value, bool isMove)
		{
		}

		// Token: 0x060056E2 RID: 22242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005033")]
		[Address(RVA = "0x10141EBDC", Offset = "0x141EBDC", VA = "0x10141EBDC")]
		private void Update()
		{
		}

		// Token: 0x060056E3 RID: 22243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005034")]
		[Address(RVA = "0x10141EA44", Offset = "0x141EA44", VA = "0x10141EA44")]
		private void UpdateDisp()
		{
		}

		// Token: 0x060056E4 RID: 22244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005035")]
		[Address(RVA = "0x10141ED88", Offset = "0x141ED88", VA = "0x10141ED88")]
		public TotalEventPointDisplay()
		{
		}

		// Token: 0x04006999 RID: 27033
		[Token(Token = "0x4004A86")]
		private const float DELAY_SEC = 1f;

		// Token: 0x0400699A RID: 27034
		[Token(Token = "0x4004A87")]
		public const int MAX = 99999;

		// Token: 0x0400699B RID: 27035
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A88")]
		[SerializeField]
		private ImageNumbers m_Number;

		// Token: 0x0400699C RID: 27036
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A89")]
		private float m_CurrentValue;

		// Token: 0x0400699D RID: 27037
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004A8A")]
		private int m_FinalValue;

		// Token: 0x0400699E RID: 27038
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A8B")]
		private List<TotalEventPointDisplay.FinalValue> m_FinalValueList;

		// Token: 0x0200113B RID: 4411
		[Token(Token = "0x200128C")]
		private class FinalValue
		{
			// Token: 0x060056E5 RID: 22245 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600635F")]
			[Address(RVA = "0x10141EBA0", Offset = "0x141EBA0", VA = "0x10141EBA0")]
			public FinalValue(int value, float delay)
			{
			}

			// Token: 0x0400699F RID: 27039
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007247")]
			public int m_FinalValue;

			// Token: 0x040069A0 RID: 27040
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007248")]
			public float m_Delay;
		}
	}
}
