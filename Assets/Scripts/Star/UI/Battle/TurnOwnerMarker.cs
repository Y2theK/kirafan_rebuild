﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200113C RID: 4412
	[Token(Token = "0x2000B70")]
	[StructLayout(3)]
	public class TurnOwnerMarker : MonoBehaviour
	{
		// Token: 0x060056E6 RID: 22246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005036")]
		[Address(RVA = "0x10141EDF8", Offset = "0x141EDF8", VA = "0x10141EDF8")]
		private void Awake()
		{
		}

		// Token: 0x060056E7 RID: 22247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005037")]
		[Address(RVA = "0x10141EE6C", Offset = "0x141EE6C", VA = "0x10141EE6C")]
		private void Update()
		{
		}

		// Token: 0x060056E8 RID: 22248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005038")]
		[Address(RVA = "0x10141EF10", Offset = "0x141EF10", VA = "0x10141EF10")]
		public void SetPosition(float x, float y)
		{
		}

		// Token: 0x060056E9 RID: 22249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005039")]
		[Address(RVA = "0x10141EF6C", Offset = "0x141EF6C", VA = "0x10141EF6C")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x060056EA RID: 22250 RVA: 0x0001CE30 File Offset: 0x0001B030
		[Token(Token = "0x600503A")]
		[Address(RVA = "0x10141EFA8", Offset = "0x141EFA8", VA = "0x10141EFA8")]
		public bool IsEnableRender()
		{
			return default(bool);
		}

		// Token: 0x060056EB RID: 22251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600503B")]
		[Address(RVA = "0x10141EFDC", Offset = "0x141EFDC", VA = "0x10141EFDC")]
		public TurnOwnerMarker()
		{
		}

		// Token: 0x040069A1 RID: 27041
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A8C")]
		[SerializeField]
		private float m_Radius;

		// Token: 0x040069A2 RID: 27042
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004A8D")]
		[SerializeField]
		private float m_AnimSpeed;

		// Token: 0x040069A3 RID: 27043
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A8E")]
		[SerializeField]
		private RectTransform m_ImageTransform;

		// Token: 0x040069A4 RID: 27044
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A8F")]
		private float m_Time;

		// Token: 0x040069A5 RID: 27045
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A90")]
		private GameObject m_GameObject;

		// Token: 0x040069A6 RID: 27046
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A91")]
		private RectTransform m_Transform;
	}
}
