﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200113D RID: 4413
	[Token(Token = "0x2000B71")]
	[StructLayout(3)]
	public class UniqueSkillNameWindow : MonoBehaviour
	{
		// Token: 0x060056EC RID: 22252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600503C")]
		[Address(RVA = "0x10141EFEC", Offset = "0x141EFEC", VA = "0x10141EFEC")]
		public void Setup()
		{
		}

		// Token: 0x060056ED RID: 22253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600503D")]
		[Address(RVA = "0x10141F058", Offset = "0x141F058", VA = "0x10141F058")]
		private void Update()
		{
		}

		// Token: 0x060056EE RID: 22254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600503E")]
		[Address(RVA = "0x10141F0CC", Offset = "0x141F0CC", VA = "0x10141F0CC")]
		public void SetText(string name, string ruby, int Lv)
		{
		}

		// Token: 0x060056EF RID: 22255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600503F")]
		[Address(RVA = "0x10141F2C8", Offset = "0x141F2C8", VA = "0x10141F2C8")]
		public UniqueSkillNameWindow()
		{
		}

		// Token: 0x040069A7 RID: 27047
		[Token(Token = "0x4004A92")]
		private const float DISPLAY_TIME_MAX = 2f;

		// Token: 0x040069A8 RID: 27048
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A93")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x040069A9 RID: 27049
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A94")]
		[SerializeField]
		private Text m_RubyTextObj;

		// Token: 0x040069AA RID: 27050
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A95")]
		[SerializeField]
		private Text m_LvTextObj;

		// Token: 0x040069AB RID: 27051
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004A96")]
		private GameObject m_NameTextGameObj;

		// Token: 0x040069AC RID: 27052
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004A97")]
		private GameObject m_RubyTextGameObj;

		// Token: 0x040069AD RID: 27053
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004A98")]
		private GameObject m_GameObject;

		// Token: 0x040069AE RID: 27054
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004A99")]
		private float m_DisplayTimeCount;
	}
}
