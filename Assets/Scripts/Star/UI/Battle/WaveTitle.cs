﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200113E RID: 4414
	[Token(Token = "0x2000B72")]
	[StructLayout(3)]
	public class WaveTitle : MonoBehaviour
	{
		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x060056F0 RID: 22256 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700056D")]
		public AnimUIPlayer Anim
		{
			[Token(Token = "0x6005040")]
			[Address(RVA = "0x10141F2D0", Offset = "0x141F2D0", VA = "0x10141F2D0")]
			get
			{
				return null;
			}
		}

		// Token: 0x060056F1 RID: 22257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005041")]
		[Address(RVA = "0x10141F368", Offset = "0x141F368", VA = "0x10141F368")]
		public void Apply(int wave, int maxWave)
		{
		}

		// Token: 0x060056F2 RID: 22258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005042")]
		[Address(RVA = "0x10141F3D0", Offset = "0x141F3D0", VA = "0x10141F3D0")]
		public WaveTitle()
		{
		}

		// Token: 0x040069AF RID: 27055
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004A9A")]
		[SerializeField]
		private ImageNumbers m_NowWave;

		// Token: 0x040069B0 RID: 27056
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004A9B")]
		[SerializeField]
		private ImageNumbers m_MaxWave;

		// Token: 0x040069B1 RID: 27057
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004A9C")]
		private AnimUIPlayer m_Anim;
	}
}
