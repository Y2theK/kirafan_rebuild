﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C73 RID: 3187
	[Token(Token = "0x2000890")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D020", Offset = "0x11D020")]
	[StructLayout(3)]
	public class BlinkCanvasGroup : MonoBehaviour
	{
		// Token: 0x060039F5 RID: 14837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DC")]
		[Address(RVA = "0x10141F3D8", Offset = "0x141F3D8", VA = "0x10141F3D8")]
		public void Start()
		{
		}

		// Token: 0x060039F6 RID: 14838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DD")]
		[Address(RVA = "0x10141F440", Offset = "0x141F440", VA = "0x10141F440")]
		private void Update()
		{
		}

		// Token: 0x060039F7 RID: 14839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DE")]
		[Address(RVA = "0x10141F56C", Offset = "0x141F56C", VA = "0x10141F56C")]
		public void Stop()
		{
		}

		// Token: 0x060039F8 RID: 14840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034DF")]
		[Address(RVA = "0x10141F604", Offset = "0x141F604", VA = "0x10141F604")]
		public BlinkCanvasGroup()
		{
		}

		// Token: 0x040048CF RID: 18639
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003337")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x040048D0 RID: 18640
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003338")]
		[SerializeField]
		private float m_MaxValue;

		// Token: 0x040048D1 RID: 18641
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003339")]
		[SerializeField]
		private float m_MinValue;

		// Token: 0x040048D2 RID: 18642
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400333A")]
		[SerializeField]
		private float m_Speed;

		// Token: 0x040048D3 RID: 18643
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400333B")]
		[SerializeField]
		private float m_TimeOffsetSec;

		// Token: 0x040048D4 RID: 18644
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400333C")]
		[SerializeField]
		private bool m_Sync;

		// Token: 0x040048D5 RID: 18645
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400333D")]
		private float m_TimeCount;
	}
}
