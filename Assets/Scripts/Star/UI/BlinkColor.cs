﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C74 RID: 3188
	[Token(Token = "0x2000891")]
	[StructLayout(3)]
	public class BlinkColor : MonoBehaviour
	{
		// Token: 0x060039F9 RID: 14841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E0")]
		[Address(RVA = "0x10141F620", Offset = "0x141F620", VA = "0x10141F620")]
		public void Start()
		{
		}

		// Token: 0x060039FA RID: 14842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E1")]
		[Address(RVA = "0x10141F670", Offset = "0x141F670", VA = "0x10141F670")]
		private void Update()
		{
		}

		// Token: 0x060039FB RID: 14843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E2")]
		[Address(RVA = "0x10141F694", Offset = "0x141F694", VA = "0x10141F694")]
		private void UpdateTimeCount()
		{
		}

		// Token: 0x060039FC RID: 14844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E3")]
		[Address(RVA = "0x10141F6E0", Offset = "0x141F6E0", VA = "0x10141F6E0")]
		private void UpdateColor()
		{
		}

		// Token: 0x060039FD RID: 14845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E4")]
		[Address(RVA = "0x10141F7C4", Offset = "0x141F7C4", VA = "0x10141F7C4")]
		public void ForceUpdate(bool isUpdateTimeCount)
		{
		}

		// Token: 0x060039FE RID: 14846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E5")]
		[Address(RVA = "0x10141F804", Offset = "0x141F804", VA = "0x10141F804")]
		public void Reset()
		{
		}

		// Token: 0x060039FF RID: 14847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034E6")]
		[Address(RVA = "0x10141F880", Offset = "0x141F880", VA = "0x10141F880")]
		public BlinkColor()
		{
		}

		// Token: 0x040048D6 RID: 18646
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400333E")]
		[SerializeField]
		private Graphic m_TargetGraphic;

		// Token: 0x040048D7 RID: 18647
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400333F")]
		[SerializeField]
		private Color m_BlinkColor;

		// Token: 0x040048D8 RID: 18648
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003340")]
		[SerializeField]
		private float m_Speed;

		// Token: 0x040048D9 RID: 18649
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003341")]
		[SerializeField]
		private bool m_Sync;

		// Token: 0x040048DA RID: 18650
		[Cpp2IlInjected.FieldOffset(Offset = "0x35")]
		[Token(Token = "0x4003342")]
		private bool m_IsSaved;

		// Token: 0x040048DB RID: 18651
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003343")]
		private Color m_Color;

		// Token: 0x040048DC RID: 18652
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003344")]
		private float m_TimeCount;
	}
}
