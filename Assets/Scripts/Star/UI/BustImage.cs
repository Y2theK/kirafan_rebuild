﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CD7 RID: 3287
	[Token(Token = "0x20008C7")]
	[StructLayout(3)]
	public class BustImage : ASyncImage
	{
		// Token: 0x06003C31 RID: 15409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036EE")]
		[Address(RVA = "0x10141F8CC", Offset = "0x141F8CC", VA = "0x10141F8CC")]
		public void Apply(int charaID, bool enableViewChange, BustImage.eBustImageType bustImageType = BustImage.eBustImageType.Chara)
		{
		}

		// Token: 0x06003C32 RID: 15410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036EF")]
		[Address(RVA = "0x10141FA54", Offset = "0x141FA54", VA = "0x10141FA54", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C33 RID: 15411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F0")]
		[Address(RVA = "0x10141FA80", Offset = "0x141FA80", VA = "0x10141FA80")]
		public BustImage()
		{
		}

		// Token: 0x04004B62 RID: 19298
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40034FF")]
		protected int m_CharaID;

		// Token: 0x02000CD8 RID: 3288
		[Token(Token = "0x20010D7")]
		public enum eBustImageType
		{
			// Token: 0x04004B64 RID: 19300
			[Token(Token = "0x40069B3")]
			Chara,
			// Token: 0x04004B65 RID: 19301
			[Token(Token = "0x40069B4")]
			Full
		}
	}
}
