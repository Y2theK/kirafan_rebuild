﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C59 RID: 3161
	[Token(Token = "0x2000879")]
	[StructLayout(3)]
	public class BustImageAdapter : ASyncImageAdapterSyncImage
	{
		// Token: 0x0600394F RID: 14671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003439")]
		[Address(RVA = "0x10141FA90", Offset = "0x141FA90", VA = "0x10141FA90")]
		public BustImageAdapter(BustImage bustImage)
		{
		}

		// Token: 0x06003950 RID: 14672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600343A")]
		[Address(RVA = "0x10141FABC", Offset = "0x141FABC", VA = "0x10141FABC", Slot = "4")]
		public override void Apply(int charaID, int weaponID)
		{
		}

		// Token: 0x06003951 RID: 14673 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600343B")]
		[Address(RVA = "0x10141FB70", Offset = "0x141FB70", VA = "0x10141FB70", Slot = "8")]
		protected override ASyncImage GetASyncImage()
		{
			return null;
		}

		// Token: 0x0400485D RID: 18525
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40032D0")]
		private BustImage m_BustImage;
	}
}
