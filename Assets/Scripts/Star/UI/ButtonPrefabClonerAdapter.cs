﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class ButtonPrefabClonerAdapter<ArgType> : MonoBehaviour {
        [SerializeField] private PrefabCloner m_Cloner;
        [SerializeField] private int m_ButtonTextCommonId;
        [SerializeField] private int m_ButtonTextFontSize;
        [SerializeField] private ArgType m_Arg;

        private CustomButton m_Button;
        
        private Action<ArgType> m_OnClickCallback;

        public GameObject Inst => m_Cloner != null ? m_Cloner.Inst : null;

        public T GetInst<T>() where T : MonoBehaviour {
            return m_Cloner != null ? m_Cloner.GetInst<T>() : null;
        }

        public bool IsExistPrefab() {
            return m_Cloner != null && m_Cloner.IsExistPrefab();
        }

        public void Setup(bool force = true) {
            if (m_Cloner != null) {
                m_Cloner.Setup(force);
                m_Button = GetInst<CustomButton>();
                if (m_Button != null) {
                    Text text = m_Button.gameObject.GetComponentInChildren<Text>();
                    text.text = GameSystem.Inst.DbMng.GetTextCommon((eText_CommonDB)m_ButtonTextCommonId);
                    text.fontSize = m_ButtonTextFontSize;
                    m_Button.m_OnClick.AddListener(OnClickButton);
                }
            }
        }

        public void SetOnClickButtonCallback(Action<ArgType> callback) {
            m_OnClickCallback = callback;
        }

        public void OnClickButton() {
            if (m_OnClickCallback != null) {
                m_OnClickCallback(m_Arg);
            }
        }
    }
}
