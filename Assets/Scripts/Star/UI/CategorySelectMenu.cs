﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C8F RID: 3215
	[Token(Token = "0x2000899")]
	[StructLayout(3)]
	public class CategorySelectMenu : MonoBehaviour
	{
		// Token: 0x06003A84 RID: 14980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003547")]
		[Address(RVA = "0x10141FBC8", Offset = "0x141FBC8", VA = "0x10141FBC8")]
		private void Awake()
		{
		}

		// Token: 0x06003A85 RID: 14981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003548")]
		[Address(RVA = "0x10141FD48", Offset = "0x141FD48", VA = "0x10141FD48")]
		private void LateUpdate()
		{
		}

		// Token: 0x06003A86 RID: 14982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003549")]
		[Address(RVA = "0x10141FBCC", Offset = "0x141FBCC", VA = "0x10141FBCC")]
		private void Init()
		{
		}

		// Token: 0x06003A87 RID: 14983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600354A")]
		[Address(RVA = "0x10141FE48", Offset = "0x141FE48", VA = "0x10141FE48")]
		public void SetVerticalNormalizedPosition(float value)
		{
		}

		// Token: 0x06003A88 RID: 14984 RVA: 0x00018360 File Offset: 0x00016560
		[Token(Token = "0x600354B")]
		[Address(RVA = "0x10141FF18", Offset = "0x141FF18", VA = "0x10141FF18")]
		public float GetVerticalScrollValue()
		{
			return 0f;
		}

		// Token: 0x06003A89 RID: 14985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600354C")]
		[Address(RVA = "0x10141FFD8", Offset = "0x141FFD8", VA = "0x10141FFD8")]
		public CategorySelectMenu()
		{
		}

		// Token: 0x0400494D RID: 18765
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003386")]
		private readonly Vector2 SIZEDELTA;

		// Token: 0x0400494E RID: 18766
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003387")]
		[SerializeField]
		private ScrollRect m_Scroll;
	}
}
