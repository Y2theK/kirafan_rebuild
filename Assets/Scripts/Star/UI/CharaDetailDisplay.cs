﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D12 RID: 3346
	[Token(Token = "0x20008F4")]
	[StructLayout(3)]
	public class CharaDetailDisplay : MonoBehaviour
	{
		// Token: 0x06003D53 RID: 15699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600380F")]
		[Address(RVA = "0x101420030", Offset = "0x1420030", VA = "0x101420030")]
		public void Apply(int charaID)
		{
		}

		// Token: 0x06003D54 RID: 15700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003810")]
		[Address(RVA = "0x101420ADC", Offset = "0x1420ADC", VA = "0x101420ADC")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06003D55 RID: 15701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003811")]
		[Address(RVA = "0x101420BCC", Offset = "0x1420BCC", VA = "0x101420BCC")]
		public CharaDetailDisplay()
		{
		}

		// Token: 0x04004CAA RID: 19626
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035E5")]
		[SerializeField]
		private CharaIconWithFrame m_CharaIconWithFrame;

		// Token: 0x04004CAB RID: 19627
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035E6")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004CAC RID: 19628
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035E7")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x04004CAD RID: 19629
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035E8")]
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x04004CAE RID: 19630
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035E9")]
		[SerializeField]
		private SkillInfo[] m_SkillInfos;
	}
}
