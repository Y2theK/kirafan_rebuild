﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using Star.UI.Global;

namespace Star.UI
{
	// Token: 0x02000D83 RID: 3459
	[Token(Token = "0x2000943")]
	[StructLayout(3)]
	public class CharaDetailPlayer : SingletonMonoBehaviour<CharaDetailPlayer>
	{
		// Token: 0x14000078 RID: 120
		// (add) Token: 0x06003FBC RID: 16316 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003FBD RID: 16317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000078")]
		public event Action OnClose
		{
			[Token(Token = "0x6003A61")]
			[Address(RVA = "0x101420BD4", Offset = "0x1420BD4", VA = "0x101420BD4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003A62")]
			[Address(RVA = "0x101420CE0", Offset = "0x1420CE0", VA = "0x101420CE0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x06003FBE RID: 16318 RVA: 0x00018E40 File Offset: 0x00017040
		[Token(Token = "0x1700047A")]
		public int? Friendship
		{
			[Token(Token = "0x6003A63")]
			[Address(RVA = "0x101420DEC", Offset = "0x1420DEC", VA = "0x101420DEC")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x06003FBF RID: 16319 RVA: 0x00018E58 File Offset: 0x00017058
		[Token(Token = "0x1700047B")]
		public long? FriendshipExp
		{
			[Token(Token = "0x6003A64")]
			[Address(RVA = "0x101420DF4", Offset = "0x1420DF4", VA = "0x101420DF4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x06003FC0 RID: 16320 RVA: 0x00018E70 File Offset: 0x00017070
		[Token(Token = "0x1700047C")]
		public bool IsTransitTrigger
		{
			[Token(Token = "0x6003A65")]
			[Address(RVA = "0x101420E00", Offset = "0x1420E00", VA = "0x101420E00")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x06003FC1 RID: 16321 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700047D")]
		public GlobalUI.SceneInfoStack SceneInfoStack
		{
			[Token(Token = "0x6003A66")]
			[Address(RVA = "0x101420E08", Offset = "0x1420E08", VA = "0x101420E08")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004D3 RID: 1235
		// (get) Token: 0x06003FC2 RID: 16322 RVA: 0x00018E88 File Offset: 0x00017088
		[Token(Token = "0x1700047E")]
		public CharaDetailUI.eCharaCommonCategoryType LastCategory
		{
			[Token(Token = "0x6003A67")]
			[Address(RVA = "0x101420E10", Offset = "0x1420E10", VA = "0x101420E10")]
			get
			{
				return CharaDetailUI.eCharaCommonCategoryType.Status;
			}
		}

		// Token: 0x06003FC3 RID: 16323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A68")]
		[Address(RVA = "0x101420E18", Offset = "0x1420E18", VA = "0x101420E18", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06003FC4 RID: 16324 RVA: 0x00018EA0 File Offset: 0x000170A0
		[Token(Token = "0x6003A69")]
		[Address(RVA = "0x101420E68", Offset = "0x1420E68", VA = "0x101420E68")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06003FC5 RID: 16325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6A")]
		[Address(RVA = "0x101420E78", Offset = "0x1420E78", VA = "0x101420E78")]
		public void OpenProfileOnly(int charaId, int arousalLv)
		{
		}

		// Token: 0x06003FC6 RID: 16326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6B")]
		[Address(RVA = "0x101420F50", Offset = "0x1420F50", VA = "0x101420F50")]
		public void Open(long charaMngID, bool profileStart = false)
		{
		}

		// Token: 0x06003FC7 RID: 16327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6C")]
		[Address(RVA = "0x101421070", Offset = "0x1421070", VA = "0x101421070")]
		public void OpenManagedBattlePartyMember(int partyIndex, int partySlotIndex)
		{
		}

		// Token: 0x06003FC8 RID: 16328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6D")]
		[Address(RVA = "0x1014212C0", Offset = "0x14212C0", VA = "0x1014212C0")]
		public void Open(UserSupportData supportData, UserMasterOrbData userOrbData)
		{
		}

		// Token: 0x06003FC9 RID: 16329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6E")]
		[Address(RVA = "0x101420F10", Offset = "0x1420F10", VA = "0x101420F10")]
		public void Open(UserCharacterData userCharacterData, UserWeaponData userWeaponData, UserMasterOrbData userOrbData, UserAbilityData userAbilityData, CharaDetailUI.eMode mode = CharaDetailUI.eMode.Normal, CharaStatusGroup.eStatusCategoryType startTab = CharaStatusGroup.eStatusCategoryType.Status, bool disableVoiceTab = false)
		{
		}

		// Token: 0x06003FCA RID: 16330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A6F")]
		[Address(RVA = "0x101421AE4", Offset = "0x1421AE4", VA = "0x101421AE4")]
		public void Open(CharaDetailUI.eCharaCommonCategoryType initialCategory, UserCharacterData userCharacterData, [Optional] UserWeaponData userWeaponData, [Optional] UserMasterOrbData userOrbData, [Optional] UserAbilityData userAbilityData, CharaDetailUI.eMode mode = CharaDetailUI.eMode.Normal, CharaStatusGroup.eStatusCategoryType startTab = CharaStatusGroup.eStatusCategoryType.Status, bool disableVoiceTab = false)
		{
		}

		// Token: 0x06003FCB RID: 16331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A70")]
		[Address(RVA = "0x101421CBC", Offset = "0x1421CBC", VA = "0x101421CBC")]
		public void SetCategoryPermission(bool permitUpgrade, bool permitLimitBreak, bool permitEvolution)
		{
		}

		// Token: 0x06003FCC RID: 16332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A71")]
		[Address(RVA = "0x101421DB4", Offset = "0x1421DB4", VA = "0x101421DB4")]
		public void SetSceneInfo(eSceneInfo sceneInfo)
		{
		}

		// Token: 0x06003FCD RID: 16333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A72")]
		[Address(RVA = "0x101421DBC", Offset = "0x1421DBC", VA = "0x101421DBC")]
		public void SetPlayOutSceneInfo(bool isPlayOut)
		{
		}

		// Token: 0x06003FCE RID: 16334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A73")]
		[Address(RVA = "0x101421DC4", Offset = "0x1421DC4", VA = "0x101421DC4")]
		public void Close()
		{
		}

		// Token: 0x06003FCF RID: 16335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A74")]
		[Address(RVA = "0x101421FD0", Offset = "0x1421FD0", VA = "0x101421FD0")]
		public void ForceHide()
		{
		}

		// Token: 0x06003FD0 RID: 16336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A75")]
		[Address(RVA = "0x101421FDC", Offset = "0x1421FDC", VA = "0x101421FDC")]
		private void Update()
		{
		}

		// Token: 0x06003FD1 RID: 16337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A76")]
		[Address(RVA = "0x101422A9C", Offset = "0x1422A9C", VA = "0x101422A9C")]
		private void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06003FD2 RID: 16338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A77")]
		[Address(RVA = "0x101422AA0", Offset = "0x1422AA0", VA = "0x101422AA0")]
		private void OnStartIllustView()
		{
		}

		// Token: 0x06003FD3 RID: 16339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A78")]
		[Address(RVA = "0x101422B18", Offset = "0x1422B18", VA = "0x101422B18")]
		private void OnEndIllustView()
		{
		}

		// Token: 0x06003FD4 RID: 16340 RVA: 0x00018EB8 File Offset: 0x000170B8
		[Token(Token = "0x6003A79")]
		[Address(RVA = "0x101421EE4", Offset = "0x1421EE4", VA = "0x101421EE4")]
		private bool Request_CharacterSetShown()
		{
			return default(bool);
		}

		// Token: 0x06003FD5 RID: 16341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7A")]
		[Address(RVA = "0x101422C10", Offset = "0x1422C10", VA = "0x101422C10")]
		public CharaDetailPlayer()
		{
		}

		// Token: 0x04004F19 RID: 20249
		[Token(Token = "0x40037BA")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.CharaDetailUI;

		// Token: 0x04004F1A RID: 20250
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40037BB")]
		private CharaDetailPlayer.eStep m_Step;

		// Token: 0x04004F1B RID: 20251
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40037BC")]
		private CharaDetailUI.eMode m_Mode;

		// Token: 0x04004F1C RID: 20252
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037BD")]
		private CharaDetailUI m_UI;

		// Token: 0x04004F1D RID: 20253
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40037BE")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04004F1E RID: 20254
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40037BF")]
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x04004F1F RID: 20255
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40037C0")]
		private CharaStatusGroup.eStatusCategoryType m_StartTab;

		// Token: 0x04004F20 RID: 20256
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40037C1")]
		private bool m_IsInputBlock;

		// Token: 0x04004F21 RID: 20257
		[Cpp2IlInjected.FieldOffset(Offset = "0x45")]
		[Token(Token = "0x40037C2")]
		private bool m_DisableVoiceTab;

		// Token: 0x04004F22 RID: 20258
		[Cpp2IlInjected.FieldOffset(Offset = "0x46")]
		[Token(Token = "0x40037C3")]
		private bool m_DisableSkillExpGauge;

		// Token: 0x04004F23 RID: 20259
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40037C4")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04004F24 RID: 20260
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40037C5")]
		private UserWeaponData m_UserWeaponData;

		// Token: 0x04004F25 RID: 20261
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40037C6")]
		private UserMasterOrbData m_UserOrbData;

		// Token: 0x04004F26 RID: 20262
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40037C7")]
		private UserAbilityData m_UserAbilityData;

		// Token: 0x04004F27 RID: 20263
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40037C8")]
		private int? m_FriendShip;

		// Token: 0x04004F28 RID: 20264
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40037C9")]
		private long? m_FriendShipExp;

		// Token: 0x04004F29 RID: 20265
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40037CA")]
		private CharaDetailUI.eCharaCommonCategoryType m_InitialCategory;

		// Token: 0x04004F2A RID: 20266
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x40037CB")]
		private CharaDetailUI.eCharaCommonCategoryType m_LastCategory;

		// Token: 0x04004F2B RID: 20267
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40037CC")]
		private bool m_IsTransitTrigger;

		// Token: 0x04004F2C RID: 20268
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40037CD")]
		private bool[] m_CategoryPermission;

		// Token: 0x04004F2D RID: 20269
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40037CE")]
		private eSceneInfo m_SceneInfo;

		// Token: 0x04004F2E RID: 20270
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x40037CF")]
		private bool m_IsPlayOutSceneInfo;

		// Token: 0x02000D84 RID: 3460
		[Token(Token = "0x2001106")]
		private enum eStep
		{
			// Token: 0x04004F31 RID: 20273
			[Token(Token = "0x4006AA9")]
			Hide,
			// Token: 0x04004F32 RID: 20274
			[Token(Token = "0x4006AAA")]
			UILoad,
			// Token: 0x04004F33 RID: 20275
			[Token(Token = "0x4006AAB")]
			LoadWait,
			// Token: 0x04004F34 RID: 20276
			[Token(Token = "0x4006AAC")]
			SetupAndPlayIn,
			// Token: 0x04004F35 RID: 20277
			[Token(Token = "0x4006AAD")]
			Main,
			// Token: 0x04004F36 RID: 20278
			[Token(Token = "0x4006AAE")]
			Unlock,
			// Token: 0x04004F37 RID: 20279
			[Token(Token = "0x4006AAF")]
			UnloadBegin,
			// Token: 0x04004F38 RID: 20280
			[Token(Token = "0x4006AB0")]
			UnloadWait,
			// Token: 0x04004F39 RID: 20281
			[Token(Token = "0x4006AB1")]
			TitleBack
		}
	}
}
