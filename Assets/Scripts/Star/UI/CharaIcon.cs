﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CD9 RID: 3289
	[Token(Token = "0x20008C8")]
	[StructLayout(3)]
	public class CharaIcon : ASyncImage
	{
		// Token: 0x06003C34 RID: 15412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F1")]
		[Address(RVA = "0x101422CB0", Offset = "0x1422CB0", VA = "0x101422CB0")]
		public void Apply(int charaID)
		{
		}

		// Token: 0x06003C35 RID: 15413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F2")]
		[Address(RVA = "0x101422DC0", Offset = "0x1422DC0", VA = "0x101422DC0", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C36 RID: 15414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F3")]
		[Address(RVA = "0x101422DEC", Offset = "0x1422DEC", VA = "0x101422DEC")]
		public CharaIcon()
		{
		}

		// Token: 0x04004B66 RID: 19302
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003500")]
		protected int m_CharaID;
	}
}
