﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CDA RID: 3290
	[Token(Token = "0x20008C9")]
	[StructLayout(3)]
	public class CharaIconFrame : MonoBehaviour
	{
		// Token: 0x06003C37 RID: 15415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F4")]
		[Address(RVA = "0x101422DFC", Offset = "0x1422DFC", VA = "0x101422DFC")]
		public void SetDispDropItem(bool flg, eEventQuestDropExtPlusType plusType)
		{
		}

		// Token: 0x06003C38 RID: 15416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F5")]
		[Address(RVA = "0x101422E2C", Offset = "0x1422E2C", VA = "0x101422E2C")]
		private void UpdateDropItemDisp()
		{
		}

		// Token: 0x06003C39 RID: 15417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F6")]
		[Address(RVA = "0x101422F48", Offset = "0x1422F48", VA = "0x101422F48")]
		private void Update()
		{
		}

		// Token: 0x06003C3A RID: 15418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F7")]
		[Address(RVA = "0x101423214", Offset = "0x1423214", VA = "0x101423214")]
		private void UpdateObjs()
		{
		}

		// Token: 0x06003C3B RID: 15419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F8")]
		[Address(RVA = "0x1014232E0", Offset = "0x14232E0", VA = "0x1014232E0")]
		private void ApplyAlpha(int idx, float alpha)
		{
		}

		// Token: 0x06003C3C RID: 15420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036F9")]
		[Address(RVA = "0x1014233DC", Offset = "0x14233DC", VA = "0x1014233DC")]
		public void SetOwnerIcon(CharaIconWithFrame owner)
		{
		}

		// Token: 0x06003C3D RID: 15421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FA")]
		[Address(RVA = "0x101423084", Offset = "0x1423084", VA = "0x101423084")]
		private void FixScale()
		{
		}

		// Token: 0x06003C3E RID: 15422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FB")]
		[Address(RVA = "0x101423450", Offset = "0x1423450", VA = "0x101423450")]
		private void ApplyCharaParam(int charaID)
		{
		}

		// Token: 0x06003C3F RID: 15423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FC")]
		[Address(RVA = "0x1014236C4", Offset = "0x14236C4", VA = "0x1014236C4")]
		public void ApplyCharaID(int charaID, bool showUserCharaParam = false)
		{
		}

		// Token: 0x06003C40 RID: 15424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FD")]
		[Address(RVA = "0x1014244A4", Offset = "0x14244A4", VA = "0x1014244A4")]
		public void ApplyCharaMngID(long charaMngID)
		{
		}

		// Token: 0x06003C41 RID: 15425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FE")]
		[Address(RVA = "0x101424B08", Offset = "0x1424B08", VA = "0x101424B08")]
		public void ApplySupport(UserSupportData supportData)
		{
		}

		// Token: 0x06003C42 RID: 15426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036FF")]
		[Address(RVA = "0x1014246FC", Offset = "0x14246FC", VA = "0x1014246FC")]
		public void SetLimitBreak(int lb)
		{
		}

		// Token: 0x06003C43 RID: 15427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003700")]
		[Address(RVA = "0x10142483C", Offset = "0x142483C", VA = "0x10142483C")]
		public void SetArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003C44 RID: 15428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003701")]
		[Address(RVA = "0x101424A04", Offset = "0x1424A04", VA = "0x101424A04")]
		public void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003C45 RID: 15429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003702")]
		[Address(RVA = "0x10142446C", Offset = "0x142446C", VA = "0x10142446C")]
		public void SetNew(bool flg)
		{
		}

		// Token: 0x06003C46 RID: 15430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003703")]
		[Address(RVA = "0x101423A40", Offset = "0x1423A40", VA = "0x101423A40")]
		public void ChangeDetailText(CharaIconFrame.eDetailType detailType)
		{
		}

		// Token: 0x06003C47 RID: 15431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003704")]
		[Address(RVA = "0x101424CC0", Offset = "0x1424CC0", VA = "0x101424CC0")]
		public void SetDetailText(string text)
		{
		}

		// Token: 0x06003C48 RID: 15432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003705")]
		[Address(RVA = "0x101424D00", Offset = "0x1424D00", VA = "0x101424D00")]
		public void Destroy()
		{
		}

		// Token: 0x06003C49 RID: 15433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003706")]
		[Address(RVA = "0x101424D04", Offset = "0x1424D04", VA = "0x101424D04")]
		public CharaIconFrame()
		{
		}

		// Token: 0x04004B67 RID: 19303
		[Token(Token = "0x4003501")]
		private const float DEFAULT_DISPTIME_SEC = 3f;

		// Token: 0x04004B68 RID: 19304
		[Token(Token = "0x4003502")]
		private const float DEFAULT_FADETIME_SEC = 0.5f;

		// Token: 0x04004B69 RID: 19305
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003503")]
		[SerializeField]
		private Image m_FrameImage;

		// Token: 0x04004B6A RID: 19306
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003504")]
		[SerializeField]
		private ReflectFrameImage m_Reflection;

		// Token: 0x04004B6B RID: 19307
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003505")]
		[SerializeField]
		private Sprite[] m_FrameSprites;

		// Token: 0x04004B6C RID: 19308
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003506")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04004B6D RID: 19309
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003507")]
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04004B6E RID: 19310
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003508")]
		[SerializeField]
		private LimitBreakIcon m_LBIcon;

		// Token: 0x04004B6F RID: 19311
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003509")]
		[SerializeField]
		private ArousalLvIcon m_ArousalIcon;

		// Token: 0x04004B70 RID: 19312
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400350A")]
		[SerializeField]
		private GameObject m_StatusObj;

		// Token: 0x04004B71 RID: 19313
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400350B")]
		[SerializeField]
		private IconStatusTitle m_StatusTitle;

		// Token: 0x04004B72 RID: 19314
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400350C")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04004B73 RID: 19315
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400350D")]
		[SerializeField]
		private ItemDropExtDisplay m_DropDisp;

		// Token: 0x04004B74 RID: 19316
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400350E")]
		[SerializeField]
		private CanvasGroup[] m_SwitchIconTarget;

		// Token: 0x04004B75 RID: 19317
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400350F")]
		private bool forceActive;

		// Token: 0x04004B76 RID: 19318
		[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
		[Token(Token = "0x4003510")]
		private bool forceActiveArousal;

		// Token: 0x04004B77 RID: 19319
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4003511")]
		private CharaIconFrame.eDetailType m_DetailType;

		// Token: 0x04004B78 RID: 19320
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003512")]
		[SerializeField]
		private GameObject m_NewObj;

		// Token: 0x04004B79 RID: 19321
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003513")]
		private long m_CharaMngID;

		// Token: 0x04004B7A RID: 19322
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003514")]
		private int m_CharaID;

		// Token: 0x04004B7B RID: 19323
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003515")]
		private UserSupportData m_SupportData;

		// Token: 0x04004B7C RID: 19324
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003516")]
		private RectTransform m_RectTransform;

		// Token: 0x04004B7D RID: 19325
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003517")]
		private RectTransform m_OwnerRect;

		// Token: 0x04004B7E RID: 19326
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003518")]
		private bool m_DispDropItem;

		// Token: 0x04004B7F RID: 19327
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4003519")]
		private eEventQuestDropExtPlusType m_PlusType;

		// Token: 0x04004B80 RID: 19328
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400351A")]
		private CharaIconFrame.eStep m_Step;

		// Token: 0x04004B81 RID: 19329
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x400351B")]
		private float m_StepTime;

		// Token: 0x04004B82 RID: 19330
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400351C")]
		private int m_Idx;

		// Token: 0x04004B83 RID: 19331
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x400351D")]
		private float m_DispTimeSec;

		// Token: 0x04004B84 RID: 19332
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400351E")]
		private float m_FadeTimeSec;

		// Token: 0x04004B85 RID: 19333
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x400351F")]
		private bool m_ExistLimitBreak;

		// Token: 0x04004B86 RID: 19334
		[Cpp2IlInjected.FieldOffset(Offset = "0xCD")]
		[Token(Token = "0x4003520")]
		private bool m_ExistArousal;

		// Token: 0x02000CDB RID: 3291
		[Token(Token = "0x20010D8")]
		private enum eStep
		{
			// Token: 0x04004B88 RID: 19336
			[Token(Token = "0x40069B6")]
			Hide,
			// Token: 0x04004B89 RID: 19337
			[Token(Token = "0x40069B7")]
			In,
			// Token: 0x04004B8A RID: 19338
			[Token(Token = "0x40069B8")]
			Idle,
			// Token: 0x04004B8B RID: 19339
			[Token(Token = "0x40069B9")]
			Out
		}

		// Token: 0x02000CDC RID: 3292
		[Token(Token = "0x20010D9")]
		public enum eDetailType
		{
			// Token: 0x04004B8D RID: 19341
			[Token(Token = "0x40069BB")]
			None,
			// Token: 0x04004B8E RID: 19342
			[Token(Token = "0x40069BC")]
			Lv,
			// Token: 0x04004B8F RID: 19343
			[Token(Token = "0x40069BD")]
			HP,
			// Token: 0x04004B90 RID: 19344
			[Token(Token = "0x40069BE")]
			Atk,
			// Token: 0x04004B91 RID: 19345
			[Token(Token = "0x40069BF")]
			Mgc,
			// Token: 0x04004B92 RID: 19346
			[Token(Token = "0x40069C0")]
			Def,
			// Token: 0x04004B93 RID: 19347
			[Token(Token = "0x40069C1")]
			MDef,
			// Token: 0x04004B94 RID: 19348
			[Token(Token = "0x40069C2")]
			Spd,
			// Token: 0x04004B95 RID: 19349
			[Token(Token = "0x40069C3")]
			Cost,
			// Token: 0x04004B96 RID: 19350
			[Token(Token = "0x40069C4")]
			FriendShip
		}
	}
}
