﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CDD RID: 3293
	[Token(Token = "0x20008CA")]
	[StructLayout(3)]
	public class CharaIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003C4A RID: 15434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003707")]
		[Address(RVA = "0x101424D24", Offset = "0x1424D24", VA = "0x101424D24")]
		public void ApplyCharaMngID(long charaMngID)
		{
		}

		// Token: 0x06003C4B RID: 15435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003708")]
		[Address(RVA = "0x101420420", Offset = "0x1420420", VA = "0x101420420")]
		public void ApplyCharaID(int charaID)
		{
		}

		// Token: 0x06003C4C RID: 15436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003709")]
		[Address(RVA = "0x101424EF0", Offset = "0x1424EF0", VA = "0x101424EF0")]
		public void ApplySupport(UserSupportData supportData)
		{
		}

		// Token: 0x06003C4D RID: 15437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370A")]
		[Address(RVA = "0x101425058", Offset = "0x1425058", VA = "0x101425058")]
		public void SetLimitBreak(int lb)
		{
		}

		// Token: 0x06003C4E RID: 15438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370B")]
		[Address(RVA = "0x1014250DC", Offset = "0x14250DC", VA = "0x1014250DC")]
		public void SetArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003C4F RID: 15439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370C")]
		[Address(RVA = "0x101425168", Offset = "0x1425168", VA = "0x101425168")]
		public void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003C50 RID: 15440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370D")]
		[Address(RVA = "0x1014251F4", Offset = "0x14251F4", VA = "0x1014251F4")]
		public void SetNew(bool flg)
		{
		}

		// Token: 0x06003C51 RID: 15441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370E")]
		[Address(RVA = "0x101425278", Offset = "0x1425278", VA = "0x101425278")]
		public void ChangeDetailText(CharaIconFrame.eDetailType detailType)
		{
		}

		// Token: 0x06003C52 RID: 15442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600370F")]
		[Address(RVA = "0x1014252FC", Offset = "0x14252FC", VA = "0x1014252FC")]
		public void ChangeDetailTextToCharaListSort()
		{
		}

		// Token: 0x06003C53 RID: 15443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003710")]
		[Address(RVA = "0x1014253A8", Offset = "0x14253A8", VA = "0x1014253A8")]
		public void ChangeDetailTextToSupportSelectSort()
		{
		}

		// Token: 0x06003C54 RID: 15444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003711")]
		[Address(RVA = "0x101425444", Offset = "0x1425444", VA = "0x101425444")]
		public void SetDropExt(bool flg, eEventQuestDropExtPlusType plusType)
		{
		}

		// Token: 0x06003C55 RID: 15445 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003712")]
		[Address(RVA = "0x101425500", Offset = "0x1425500", VA = "0x101425500")]
		public CharaIconFrame GetCharaIconFrame()
		{
			return null;
		}

		// Token: 0x06003C56 RID: 15446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003713")]
		[Address(RVA = "0x101425560", Offset = "0x1425560", VA = "0x101425560")]
		public void Destroy()
		{
		}

		// Token: 0x06003C57 RID: 15447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003714")]
		[Address(RVA = "0x101425600", Offset = "0x1425600", VA = "0x101425600")]
		public CharaIconWithFrame()
		{
		}

		// Token: 0x04004B97 RID: 19351
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003521")]
		[SerializeField]
		private PrefabCloner m_CharaIconFrame;

		// Token: 0x04004B98 RID: 19352
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003522")]
		[SerializeField]
		private CharaIcon m_CharaIcon;
	}
}
