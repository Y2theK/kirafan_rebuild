﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C5A RID: 3162
	[Token(Token = "0x200087A")]
	[StructLayout(3)]
	public class CharaIconWithFrameAdapter : ASyncImageAdapter
	{
		// Token: 0x06003952 RID: 14674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600343C")]
		[Address(RVA = "0x101425608", Offset = "0x1425608", VA = "0x101425608")]
		public CharaIconWithFrameAdapter(CharaIconWithFrame characterIcon)
		{
		}

		// Token: 0x06003953 RID: 14675 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600343D")]
		[Address(RVA = "0x101425634", Offset = "0x1425634", VA = "0x101425634", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06003954 RID: 14676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600343E")]
		[Address(RVA = "0x1014256CC", Offset = "0x14256CC", VA = "0x1014256CC", Slot = "4")]
		public override void Apply(int charaID, int weaponID)
		{
		}

		// Token: 0x06003955 RID: 14677 RVA: 0x00017E98 File Offset: 0x00016098
		[Token(Token = "0x600343F")]
		[Address(RVA = "0x101425778", Offset = "0x1425778", VA = "0x101425778", Slot = "6")]
		public override bool IsDoneLoad()
		{
			return default(bool);
		}

		// Token: 0x06003956 RID: 14678 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003440")]
		[Address(RVA = "0x101425780", Offset = "0x1425780", VA = "0x101425780", Slot = "7")]
		protected override MonoBehaviour GetMonoBehaviour()
		{
			return null;
		}

		// Token: 0x0400485E RID: 18526
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40032D1")]
		private CharaIconWithFrame m_CharacterIcon;
	}
}
