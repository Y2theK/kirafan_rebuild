﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CDE RID: 3294
	[Token(Token = "0x20008CB")]
	[StructLayout(3)]
	public class CharaIllust : ASyncImage
	{
		// Token: 0x06003C58 RID: 15448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003715")]
		[Address(RVA = "0x101425788", Offset = "0x1425788", VA = "0x101425788")]
		public void Apply(int charaID, bool enableViewChange, CharaIllust.eCharaIllustType type = CharaIllust.eCharaIllustType.Chara)
		{
		}

		// Token: 0x06003C59 RID: 15449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003716")]
		[Address(RVA = "0x101425A6C", Offset = "0x1425A6C", VA = "0x101425A6C", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C5A RID: 15450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003717")]
		[Address(RVA = "0x101425A98", Offset = "0x1425A98", VA = "0x101425A98")]
		public CharaIllust()
		{
		}

		// Token: 0x04004B99 RID: 19353
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003523")]
		protected int m_CharaID;

		// Token: 0x02000CDF RID: 3295
		[Token(Token = "0x20010DA")]
		public enum eCharaIllustType
		{
			// Token: 0x04004B9B RID: 19355
			[Token(Token = "0x40069C6")]
			Chara,
			// Token: 0x04004B9C RID: 19356
			[Token(Token = "0x40069C7")]
			Full,
			// Token: 0x04004B9D RID: 19357
			[Token(Token = "0x40069C8")]
			Bust,
			// Token: 0x04004B9E RID: 19358
			[Token(Token = "0x40069C9")]
			Edit,
			// Token: 0x04004B9F RID: 19359
			[Token(Token = "0x40069CA")]
			Card
		}
	}
}
