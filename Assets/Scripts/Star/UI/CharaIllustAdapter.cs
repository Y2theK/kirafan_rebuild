﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C58 RID: 3160
	[Token(Token = "0x2000878")]
	[StructLayout(3)]
	public class CharaIllustAdapter : ASyncImageAdapterSyncImage
	{
		// Token: 0x0600394C RID: 14668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003436")]
		[Address(RVA = "0x101425AA8", Offset = "0x1425AA8", VA = "0x101425AA8")]
		public CharaIllustAdapter(CharaIllust charaIllust, CharaIllust.eCharaIllustType characterIllustType)
		{
		}

		// Token: 0x0600394D RID: 14669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003437")]
		[Address(RVA = "0x101425AEC", Offset = "0x1425AEC", VA = "0x101425AEC", Slot = "4")]
		public override void Apply(int charaID, int weaponID)
		{
		}

		// Token: 0x0600394E RID: 14670 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003438")]
		[Address(RVA = "0x101425BA4", Offset = "0x1425BA4", VA = "0x101425BA4", Slot = "8")]
		protected override ASyncImage GetASyncImage()
		{
			return null;
		}

		// Token: 0x0400485B RID: 18523
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40032CE")]
		private CharaIllust m_CharaIllust;

		// Token: 0x0400485C RID: 18524
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032CF")]
		private CharaIllust.eCharaIllustType m_CharacterIllustType;
	}
}
