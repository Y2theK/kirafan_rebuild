﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.CharaList
{
	// Token: 0x02001058 RID: 4184
	[Token(Token = "0x2000ADE")]
	[StructLayout(3)]
	public class CharaListCondition
	{
		// Token: 0x06005089 RID: 20617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A0A")]
		[Address(RVA = "0x101425BAC", Offset = "0x1425BAC", VA = "0x101425BAC")]
		public CharaListCondition()
		{
		}

		// Token: 0x0400630B RID: 25355
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400458C")]
		public int m_LvLimitLower;
	}
}
