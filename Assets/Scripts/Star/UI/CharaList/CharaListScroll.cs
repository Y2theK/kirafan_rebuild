﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.CharaList
{
	// Token: 0x02001059 RID: 4185
	[Token(Token = "0x2000ADF")]
	[StructLayout(3)]
	public class CharaListScroll : ScrollViewBase
	{
		// Token: 0x14000113 RID: 275
		// (add) Token: 0x0600508A RID: 20618 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600508B RID: 20619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000113")]
		public event Action<long> OnClickIcon
		{
			[Token(Token = "0x6004A0B")]
			[Address(RVA = "0x101425BD4", Offset = "0x1425BD4", VA = "0x101425BD4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A0C")]
			[Address(RVA = "0x101425CE0", Offset = "0x1425CE0", VA = "0x101425CE0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000114 RID: 276
		// (add) Token: 0x0600508C RID: 20620 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600508D RID: 20621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000114")]
		public event Action<long> OnHoldIcon
		{
			[Token(Token = "0x6004A0D")]
			[Address(RVA = "0x101425DEC", Offset = "0x1425DEC", VA = "0x101425DEC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A0E")]
			[Address(RVA = "0x101425EF8", Offset = "0x1425EF8", VA = "0x101425EF8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000115 RID: 277
		// (add) Token: 0x0600508E RID: 20622 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600508F RID: 20623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000115")]
		public event Action OnClickRemoveIcon
		{
			[Token(Token = "0x6004A0F")]
			[Address(RVA = "0x101426004", Offset = "0x1426004", VA = "0x101426004")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A10")]
			[Address(RVA = "0x101426110", Offset = "0x1426110", VA = "0x101426110")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x06005090 RID: 20624 RVA: 0x0001BE10 File Offset: 0x0001A010
		[Token(Token = "0x17000517")]
		public int CharaNum
		{
			[Token(Token = "0x6004A11")]
			[Address(RVA = "0x10142621C", Offset = "0x142621C", VA = "0x10142621C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x06005091 RID: 20625 RVA: 0x0001BE28 File Offset: 0x0001A028
		[Token(Token = "0x17000518")]
		public CharaListScroll.eMode Mode
		{
			[Token(Token = "0x6004A12")]
			[Address(RVA = "0x10142627C", Offset = "0x142627C", VA = "0x10142627C")]
			get
			{
				return CharaListScroll.eMode.View;
			}
		}

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x06005092 RID: 20626 RVA: 0x0001BE40 File Offset: 0x0001A040
		[Token(Token = "0x17000519")]
		public int PartyIndex
		{
			[Token(Token = "0x6004A13")]
			[Address(RVA = "0x101426284", Offset = "0x1426284", VA = "0x101426284")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06005093 RID: 20627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A14")]
		[Address(RVA = "0x10142628C", Offset = "0x142628C", VA = "0x10142628C")]
		public void SetViewMode()
		{
		}

		// Token: 0x06005094 RID: 20628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A15")]
		[Address(RVA = "0x101427630", Offset = "0x1427630", VA = "0x101427630")]
		public void SetPartyEditMode(int partyIdx, int memberIdx)
		{
		}

		// Token: 0x06005095 RID: 20629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A16")]
		[Address(RVA = "0x101427648", Offset = "0x1427648", VA = "0x101427648")]
		public void SetSupportEditMode(int supportPartyIdx, int supportMemberIdx)
		{
		}

		// Token: 0x06005096 RID: 20630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A17")]
		[Address(RVA = "0x101427664", Offset = "0x1427664", VA = "0x101427664")]
		public void SetUpgradeMode()
		{
		}

		// Token: 0x06005097 RID: 20631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A18")]
		[Address(RVA = "0x101427678", Offset = "0x1427678", VA = "0x101427678")]
		public void SetLimitBreakMode()
		{
		}

		// Token: 0x06005098 RID: 20632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A19")]
		[Address(RVA = "0x10142768C", Offset = "0x142768C", VA = "0x10142768C")]
		public void SetEvolutionMode()
		{
		}

		// Token: 0x06005099 RID: 20633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1A")]
		[Address(RVA = "0x1014276A0", Offset = "0x14276A0", VA = "0x1014276A0")]
		public void SetViewChangeMode()
		{
		}

		// Token: 0x0600509A RID: 20634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1B")]
		[Address(RVA = "0x1014276B4", Offset = "0x14276B4", VA = "0x1014276B4")]
		public void SetAbilityListupMode()
		{
		}

		// Token: 0x0600509B RID: 20635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1C")]
		[Address(RVA = "0x1014276C8", Offset = "0x14276C8", VA = "0x1014276C8")]
		public void SetRoomMemberMode(long[] liveCharaMngIDs, int memberIdx)
		{
		}

		// Token: 0x0600509C RID: 20636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1D")]
		[Address(RVA = "0x101427754", Offset = "0x1427754", VA = "0x101427754")]
		public void SetTrainingMemberMode(List<long>[] slotCharaMngIDs, int editSlotIdx, int editMemberIdx, CharaListCondition condition)
		{
		}

		// Token: 0x0600509D RID: 20637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1E")]
		[Address(RVA = "0x101427898", Offset = "0x1427898", VA = "0x101427898")]
		public void SetFavoriteCharaMode(long[] liveCharaMngIDs, int memberIdx)
		{
		}

		// Token: 0x0600509E RID: 20638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A1F")]
		[Address(RVA = "0x101427924", Offset = "0x1427924", VA = "0x101427924")]
		public void SetContentRoomCharaMode(eTitleType type, long[] charaMngIDs, int memberIdx)
		{
		}

		// Token: 0x0600509F RID: 20639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A20")]
		[Address(RVA = "0x10142629C", Offset = "0x142629C", VA = "0x10142629C")]
		public void SetEnbaleRemoveIcon(bool flg)
		{
		}

		// Token: 0x060050A0 RID: 20640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A21")]
		[Address(RVA = "0x1014279B0", Offset = "0x14279B0", VA = "0x1014279B0")]
		public void SetEventFilterEventType(int eventType)
		{
		}

		// Token: 0x060050A1 RID: 20641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A22")]
		[Address(RVA = "0x1014262A4", Offset = "0x14262A4", VA = "0x1014262A4")]
		private void ApplyMode()
		{
		}

		// Token: 0x060050A2 RID: 20642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A23")]
		[Address(RVA = "0x101427EA4", Offset = "0x1427EA4", VA = "0x101427EA4", Slot = "17")]
		protected override void HideItem(ScrollViewBase.DispItem item)
		{
		}

		// Token: 0x060050A3 RID: 20643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A24")]
		[Address(RVA = "0x101427F48", Offset = "0x1427F48", VA = "0x101427F48", Slot = "18")]
		protected override void ShowItem(ScrollViewBase.DispItem item)
		{
		}

		// Token: 0x060050A4 RID: 20644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A25")]
		[Address(RVA = "0x1014279B8", Offset = "0x14279B8", VA = "0x1014279B8")]
		public void AddRemoveIcon()
		{
		}

		// Token: 0x060050A5 RID: 20645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A26")]
		[Address(RVA = "0x101427FEC", Offset = "0x1427FEC", VA = "0x101427FEC", Slot = "14")]
		protected override void OnClickIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060050A6 RID: 20646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A27")]
		[Address(RVA = "0x10142820C", Offset = "0x142820C", VA = "0x10142820C", Slot = "15")]
		protected override void OnHoldIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060050A7 RID: 20647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A28")]
		[Address(RVA = "0x10142842C", Offset = "0x142842C", VA = "0x10142842C")]
		public void OnClickRemoveIconCallBack(ScrollItemIcon itemIcon)
		{
		}

		// Token: 0x060050A8 RID: 20648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A29")]
		[Address(RVA = "0x101428438", Offset = "0x1428438", VA = "0x101428438")]
		public void SetupList()
		{
		}

		// Token: 0x060050A9 RID: 20649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A2A")]
		[Address(RVA = "0x1014289E4", Offset = "0x14289E4", VA = "0x1014289E4")]
		private void DoFilter()
		{
		}

		// Token: 0x060050AA RID: 20650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A2B")]
		[Address(RVA = "0x101429180", Offset = "0x1429180", VA = "0x101429180")]
		private void DoSort()
		{
		}

		// Token: 0x060050AB RID: 20651 RVA: 0x0001BE58 File Offset: 0x0001A058
		[Token(Token = "0x6004A2C")]
		[Address(RVA = "0x1014292B8", Offset = "0x14292B8", VA = "0x1014292B8")]
		private int SortCompare(CharaListScrollItemData r, CharaListScrollItemData l)
		{
			return 0;
		}

		// Token: 0x060050AC RID: 20652 RVA: 0x0001BE70 File Offset: 0x0001A070
		[Token(Token = "0x6004A2D")]
		[Address(RVA = "0x1014296A8", Offset = "0x14296A8", VA = "0x1014296A8")]
		private int SortByTitleID(int order, CharaListScrollItemData r, CharaListScrollItemData l)
		{
			return 0;
		}

		// Token: 0x060050AD RID: 20653 RVA: 0x0001BE88 File Offset: 0x0001A088
		[Token(Token = "0x6004A2E")]
		[Address(RVA = "0x101429AB0", Offset = "0x1429AB0", VA = "0x101429AB0")]
		private long GetSortKey(int idx)
		{
			return 0L;
		}

		// Token: 0x060050AE RID: 20654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A2F")]
		[Address(RVA = "0x10142A5C4", Offset = "0x142A5C4", VA = "0x10142A5C4", Slot = "16")]
		protected override void UpdateItemPosition()
		{
		}

		// Token: 0x060050AF RID: 20655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A30")]
		[Address(RVA = "0x10142A6E0", Offset = "0x142A6E0", VA = "0x10142A6E0")]
		public CharaListScroll()
		{
		}

		// Token: 0x0400630F RID: 25359
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004590")]
		private CharaListScroll.eMode m_Mode;

		// Token: 0x04006310 RID: 25360
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004591")]
		private List<CharaListScrollItemData> m_RawDataList;

		// Token: 0x04006311 RID: 25361
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004592")]
		private List<CharaListScrollItemData> m_FilteredDataList;

		// Token: 0x04006312 RID: 25362
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004593")]
		[SerializeField]
		private CharaListScrollRemoveItem m_RemoveIconPrefab;

		// Token: 0x04006313 RID: 25363
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004594")]
		private ScrollViewBase.DispItem m_RemoveIconViewItem;

		// Token: 0x04006314 RID: 25364
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004595")]
		private int m_RemoveViewIconNum;

		// Token: 0x04006315 RID: 25365
		[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
		[Token(Token = "0x4004596")]
		private int m_PartyIdx;

		// Token: 0x04006316 RID: 25366
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004597")]
		private int m_MemberIdx;

		// Token: 0x04006317 RID: 25367
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004598")]
		private long[] m_LiveCharaMngIDs;

		// Token: 0x04006318 RID: 25368
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004599")]
		private int m_RoomMemberIdx;

		// Token: 0x04006319 RID: 25369
		[Cpp2IlInjected.FieldOffset(Offset = "0x10C")]
		[Token(Token = "0x400459A")]
		private int m_TrainingSlotIdx;

		// Token: 0x0400631A RID: 25370
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x400459B")]
		private int m_TrainingMemberIdx;

		// Token: 0x0400631B RID: 25371
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x400459C")]
		private List<long>[] m_TrainingSlotCharaMngIDs;

		// Token: 0x0400631C RID: 25372
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x400459D")]
		private long[] m_FavoriteCharaMngIDs;

		// Token: 0x0400631D RID: 25373
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x400459E")]
		private int m_FavoriteCharaIdx;

		// Token: 0x0400631E RID: 25374
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x400459F")]
		private long[] m_ContentRoomMemberMngIDs;

		// Token: 0x0400631F RID: 25375
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x40045A0")]
		private int m_ContentRoomMemberIdx;

		// Token: 0x04006320 RID: 25376
		[Cpp2IlInjected.FieldOffset(Offset = "0x13C")]
		[Token(Token = "0x40045A1")]
		private eTitleType m_titleType;

		// Token: 0x04006321 RID: 25377
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x40045A2")]
		private CharaListCondition m_Condition;

		// Token: 0x04006322 RID: 25378
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x40045A3")]
		private bool m_EnableRemoveIcon;

		// Token: 0x04006323 RID: 25379
		[Cpp2IlInjected.FieldOffset(Offset = "0x14C")]
		[Token(Token = "0x40045A4")]
		private int m_EventFilterEventType;

		// Token: 0x04006324 RID: 25380
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x40045A5")]
		private eTitleTypeConverter m_titleConverter;

		// Token: 0x0200105A RID: 4186
		[Token(Token = "0x200123C")]
		public enum eMode
		{
			// Token: 0x04006326 RID: 25382
			[Token(Token = "0x40070B7")]
			View,
			// Token: 0x04006327 RID: 25383
			[Token(Token = "0x40070B8")]
			PartyEdit,
			// Token: 0x04006328 RID: 25384
			[Token(Token = "0x40070B9")]
			SupportEdit,
			// Token: 0x04006329 RID: 25385
			[Token(Token = "0x40070BA")]
			Upgrade,
			// Token: 0x0400632A RID: 25386
			[Token(Token = "0x40070BB")]
			LimitBreak,
			// Token: 0x0400632B RID: 25387
			[Token(Token = "0x40070BC")]
			Evolution,
			// Token: 0x0400632C RID: 25388
			[Token(Token = "0x40070BD")]
			ViewChange,
			// Token: 0x0400632D RID: 25389
			[Token(Token = "0x40070BE")]
			Room,
			// Token: 0x0400632E RID: 25390
			[Token(Token = "0x40070BF")]
			Training,
			// Token: 0x0400632F RID: 25391
			[Token(Token = "0x40070C0")]
			Favorite,
			// Token: 0x04006330 RID: 25392
			[Token(Token = "0x40070C1")]
			ContentRoom,
			// Token: 0x04006331 RID: 25393
			[Token(Token = "0x40070C2")]
			Ability
		}
	}
}
