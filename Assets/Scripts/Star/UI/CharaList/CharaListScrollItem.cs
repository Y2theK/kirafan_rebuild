﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.CharaList
{
	// Token: 0x0200105B RID: 4187
	[Token(Token = "0x2000AE0")]
	[StructLayout(3)]
	public class CharaListScrollItem : ScrollItemIcon
	{
		// Token: 0x060050B0 RID: 20656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A31")]
		[Address(RVA = "0x10142A7B4", Offset = "0x142A7B4", VA = "0x10142A7B4", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060050B1 RID: 20657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A32")]
		[Address(RVA = "0x10142B6D0", Offset = "0x142B6D0", VA = "0x10142B6D0", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060050B2 RID: 20658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A33")]
		[Address(RVA = "0x10142B768", Offset = "0x142B768", VA = "0x10142B768")]
		public CharaListScrollItem()
		{
		}

		// Token: 0x04006332 RID: 25394
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40045A6")]
		[SerializeField]
		private CustomButton m_CharaIconButton;

		// Token: 0x04006333 RID: 25395
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40045A7")]
		[SerializeField]
		private PrefabCloner m_CharaIconClone;

		// Token: 0x04006334 RID: 25396
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40045A8")]
		private CharaIconWithFrame m_CharaIcon;

		// Token: 0x04006335 RID: 25397
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40045A9")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04006336 RID: 25398
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40045AA")]
		[SerializeField]
		private SwitchFade m_SwitchLabel;

		// Token: 0x04006337 RID: 25399
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40045AB")]
		[SerializeField]
		private CanvasGroup m_IsPartyCharaCG;

		// Token: 0x04006338 RID: 25400
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40045AC")]
		[SerializeField]
		private CanvasGroup m_IsBonusCharaCG;

		// Token: 0x04006339 RID: 25401
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40045AD")]
		[SerializeField]
		private CanvasGroup m_IsTrainingCharaCG;

		// Token: 0x0400633A RID: 25402
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40045AE")]
		[SerializeField]
		private Image m_TrainingImage;

		// Token: 0x0400633B RID: 25403
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40045AF")]
		[SerializeField]
		private Sprite[] m_TrainingSprites;

		// Token: 0x0400633C RID: 25404
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40045B0")]
		[SerializeField]
		private GameObject m_IsScheduleRoomInCharaObj;

		// Token: 0x0400633D RID: 25405
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40045B1")]
		[SerializeField]
		private GameObject m_ViewChangedObj;

		// Token: 0x0400633E RID: 25406
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40045B2")]
		[SerializeField]
		private CanvasGroup m_IsAbilityCG;

		// Token: 0x0400633F RID: 25407
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40045B3")]
		protected CharaListScroll m_CharaScroll;

		// Token: 0x04006340 RID: 25408
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40045B4")]
		protected CharaListScrollItemData m_CharaData;

		// Token: 0x0200105C RID: 4188
		[Token(Token = "0x200123D")]
		public enum eCharaListScrollItemState
		{
			// Token: 0x04006342 RID: 25410
			[Token(Token = "0x40070C4")]
			Normal,
			// Token: 0x04006343 RID: 25411
			[Token(Token = "0x40070C5")]
			Disactive
		}
	}
}
