﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.CharaList
{
	// Token: 0x0200105D RID: 4189
	[Token(Token = "0x2000AE1")]
	[StructLayout(3)]
	public class CharaListScrollItemData : ScrollItemData
	{
		// Token: 0x060050B3 RID: 20659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A34")]
		[Address(RVA = "0x10142899C", Offset = "0x142899C", VA = "0x10142899C")]
		public CharaListScrollItemData()
		{
		}

		// Token: 0x04006344 RID: 25412
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045B5")]
		public int m_CharaIdx;

		// Token: 0x04006345 RID: 25413
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40045B6")]
		public int m_SortedIdx;

		// Token: 0x04006346 RID: 25414
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045B7")]
		public bool m_IsEnable;

		// Token: 0x04006347 RID: 25415
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40045B8")]
		public int m_MemberIdx;

		// Token: 0x04006348 RID: 25416
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045B9")]
		public int m_SelectCharaIdx;

		// Token: 0x04006349 RID: 25417
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40045BA")]
		public int m_TrainingSlotIdx;

		// Token: 0x0400634A RID: 25418
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40045BB")]
		public bool m_IsMarked;

		// Token: 0x0400634B RID: 25419
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40045BC")]
		public CharaListScrollItemData.eMode m_Mode;

		// Token: 0x0400634C RID: 25420
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40045BD")]
		public bool m_CantSelect;

		// Token: 0x0400634D RID: 25421
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40045BE")]
		public CharaListScrollItemData.eCharaListScrollItemDataState m_State;

		// Token: 0x0200105E RID: 4190
		[Token(Token = "0x200123E")]
		public enum eCharaListScrollItemDataState
		{
			// Token: 0x0400634F RID: 25423
			[Token(Token = "0x40070C7")]
			Normal,
			// Token: 0x04006350 RID: 25424
			[Token(Token = "0x40070C8")]
			Disactive,
			// Token: 0x04006351 RID: 25425
			[Token(Token = "0x40070C9")]
			NotEnough
		}

		// Token: 0x0200105F RID: 4191
		[Token(Token = "0x200123F")]
		public enum eMode
		{
			// Token: 0x04006353 RID: 25427
			[Token(Token = "0x40070CB")]
			Edit,
			// Token: 0x04006354 RID: 25428
			[Token(Token = "0x40070CC")]
			Room,
			// Token: 0x04006355 RID: 25429
			[Token(Token = "0x40070CD")]
			Training,
			// Token: 0x04006356 RID: 25430
			[Token(Token = "0x40070CE")]
			Favorite,
			// Token: 0x04006357 RID: 25431
			[Token(Token = "0x40070CF")]
			ViewChange,
			// Token: 0x04006358 RID: 25432
			[Token(Token = "0x40070D0")]
			Ability
		}
	}
}
