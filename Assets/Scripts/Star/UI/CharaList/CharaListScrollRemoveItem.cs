﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.CharaList
{
	// Token: 0x02001060 RID: 4192
	[Token(Token = "0x2000AE2")]
	[StructLayout(3)]
	public class CharaListScrollRemoveItem : ScrollItemIcon
	{
		// Token: 0x060050B4 RID: 20660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A35")]
		[Address(RVA = "0x10142B770", Offset = "0x142B770", VA = "0x10142B770", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x060050B5 RID: 20661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A36")]
		[Address(RVA = "0x10142B778", Offset = "0x142B778", VA = "0x10142B778", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060050B6 RID: 20662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A37")]
		[Address(RVA = "0x10142B780", Offset = "0x142B780", VA = "0x10142B780")]
		public CharaListScrollRemoveItem()
		{
		}

		// Token: 0x04006359 RID: 25433
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40045BF")]
		protected CharaListScroll m_CharaScroll;

		// Token: 0x0400635A RID: 25434
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40045C0")]
		protected CharaListScrollItemData m_CharaData;
	}
}
