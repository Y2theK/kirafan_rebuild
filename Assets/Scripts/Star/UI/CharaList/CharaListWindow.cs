﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.CharaList
{
	// Token: 0x02001061 RID: 4193
	[Token(Token = "0x2000AE3")]
	[StructLayout(3)]
	public class CharaListWindow : UIGroup
	{
		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x060050B7 RID: 20663 RVA: 0x0001BEA0 File Offset: 0x0001A0A0
		[Token(Token = "0x1700051A")]
		public bool IsCanceled
		{
			[Token(Token = "0x6004A38")]
			[Address(RVA = "0x10142B788", Offset = "0x142B788", VA = "0x10142B788")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x060050B8 RID: 20664 RVA: 0x0001BEB8 File Offset: 0x0001A0B8
		[Token(Token = "0x1700051B")]
		public long SelectCharaMngID
		{
			[Token(Token = "0x6004A39")]
			[Address(RVA = "0x10142B790", Offset = "0x142B790", VA = "0x10142B790")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x14000116 RID: 278
		// (add) Token: 0x060050B9 RID: 20665 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060050BA RID: 20666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000116")]
		public event Action<long> OnClickChara
		{
			[Token(Token = "0x6004A3A")]
			[Address(RVA = "0x10142B798", Offset = "0x142B798", VA = "0x10142B798")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A3B")]
			[Address(RVA = "0x10142B8A4", Offset = "0x142B8A4", VA = "0x10142B8A4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000117 RID: 279
		// (add) Token: 0x060050BB RID: 20667 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060050BC RID: 20668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000117")]
		public event Action<long> OnHoldChara
		{
			[Token(Token = "0x6004A3C")]
			[Address(RVA = "0x10142B9B0", Offset = "0x142B9B0", VA = "0x10142B9B0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A3D")]
			[Address(RVA = "0x10142BABC", Offset = "0x142BABC", VA = "0x10142BABC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000118 RID: 280
		// (add) Token: 0x060050BD RID: 20669 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060050BE RID: 20670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000118")]
		public event Action OnClickRemove
		{
			[Token(Token = "0x6004A3E")]
			[Address(RVA = "0x10142BBC8", Offset = "0x142BBC8", VA = "0x10142BBC8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A3F")]
			[Address(RVA = "0x10142BCD4", Offset = "0x142BCD4", VA = "0x10142BCD4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000119 RID: 281
		// (add) Token: 0x060050BF RID: 20671 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060050C0 RID: 20672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000119")]
		public event Action<long> OnSelect
		{
			[Token(Token = "0x6004A40")]
			[Address(RVA = "0x10142BDE0", Offset = "0x142BDE0", VA = "0x10142BDE0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004A41")]
			[Address(RVA = "0x10142BEEC", Offset = "0x142BEEC", VA = "0x10142BEEC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060050C1 RID: 20673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A42")]
		[Address(RVA = "0x10142BFF8", Offset = "0x142BFF8", VA = "0x10142BFF8")]
		public void OpenRoom(long[] liveCharaMngIDs, int memberIdx)
		{
		}

		// Token: 0x060050C2 RID: 20674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A43")]
		[Address(RVA = "0x10142C424", Offset = "0x142C424", VA = "0x10142C424")]
		public void OpenTraining(List<long>[] slotCharaLists, int editSlotIdx, int editMemberIdx, int lowerLv)
		{
		}

		// Token: 0x060050C3 RID: 20675 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A44")]
		[Address(RVA = "0x10142C5E4", Offset = "0x142C5E4", VA = "0x10142C5E4")]
		public void OpenFavoriteChara(long[] liveCharaMngIDs, int memberIdx)
		{
		}

		// Token: 0x060050C4 RID: 20676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A45")]
		[Address(RVA = "0x10142C75C", Offset = "0x142C75C", VA = "0x10142C75C")]
		public void OpenContentRoomMember(eTitleType type, long[] charaMngIDs, int memberIndex)
		{
		}

		// Token: 0x060050C5 RID: 20677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A46")]
		[Address(RVA = "0x10142C8E4", Offset = "0x142C8E4", VA = "0x10142C8E4", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060050C6 RID: 20678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A47")]
		[Address(RVA = "0x10142C8EC", Offset = "0x142C8EC", VA = "0x10142C8EC", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060050C7 RID: 20679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A48")]
		[Address(RVA = "0x10142C170", Offset = "0x142C170", VA = "0x10142C170")]
		private void ExecuteOpen()
		{
		}

		// Token: 0x060050C8 RID: 20680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A49")]
		[Address(RVA = "0x10142CC40", Offset = "0x142CC40", VA = "0x10142CC40")]
		public void ClearCallback()
		{
		}

		// Token: 0x060050C9 RID: 20681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4A")]
		[Address(RVA = "0x10142CC4C", Offset = "0x142CC4C", VA = "0x10142CC4C", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060050CA RID: 20682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4B")]
		[Address(RVA = "0x10142CCDC", Offset = "0x142CCDC", VA = "0x10142CCDC", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060050CB RID: 20683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4C")]
		[Address(RVA = "0x10142CE38", Offset = "0x142CE38", VA = "0x10142CE38")]
		private void OnClickCharaIconCallBack(long charaMngID)
		{
		}

		// Token: 0x060050CC RID: 20684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4D")]
		[Address(RVA = "0x10142D1EC", Offset = "0x142D1EC", VA = "0x10142D1EC")]
		public void DecideChara()
		{
		}

		// Token: 0x060050CD RID: 20685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4E")]
		[Address(RVA = "0x10142CF20", Offset = "0x142CF20", VA = "0x10142CF20")]
		private void OpenCharaDetail(long charaMngID)
		{
		}

		// Token: 0x060050CE RID: 20686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A4F")]
		[Address(RVA = "0x10142D26C", Offset = "0x142D26C", VA = "0x10142D26C")]
		private void OnCloseCharaDetail()
		{
		}

		// Token: 0x060050CF RID: 20687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A50")]
		[Address(RVA = "0x10142D324", Offset = "0x142D324", VA = "0x10142D324")]
		private void OnHoldCharaIconCallBack(long charaMngID)
		{
		}

		// Token: 0x060050D0 RID: 20688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A51")]
		[Address(RVA = "0x10142D3A4", Offset = "0x142D3A4", VA = "0x10142D3A4")]
		private void OnClickRemoveCallBack()
		{
		}

		// Token: 0x060050D1 RID: 20689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A52")]
		[Address(RVA = "0x10142D458", Offset = "0x142D458", VA = "0x10142D458")]
		private void OnConfirmRoomCharaChange(int btn)
		{
		}

		// Token: 0x060050D2 RID: 20690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A53")]
		[Address(RVA = "0x10142D468", Offset = "0x142D468", VA = "0x10142D468")]
		public void Destory()
		{
		}

		// Token: 0x060050D3 RID: 20691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A54")]
		[Address(RVA = "0x10142D4A0", Offset = "0x142D4A0", VA = "0x10142D4A0")]
		public void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x060050D4 RID: 20692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A55")]
		[Address(RVA = "0x10142D4AC", Offset = "0x142D4AC", VA = "0x10142D4AC")]
		public void OpenFilterWindow()
		{
		}

		// Token: 0x060050D5 RID: 20693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A56")]
		[Address(RVA = "0x10142D5C0", Offset = "0x142D5C0", VA = "0x10142D5C0")]
		public void OnExecuteFilter()
		{
		}

		// Token: 0x060050D6 RID: 20694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A57")]
		[Address(RVA = "0x10142D678", Offset = "0x142D678", VA = "0x10142D678")]
		public void OpenSortWindow()
		{
		}

		// Token: 0x060050D7 RID: 20695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A58")]
		[Address(RVA = "0x10142D760", Offset = "0x142D760", VA = "0x10142D760")]
		public void OnExecuteSort()
		{
		}

		// Token: 0x060050D8 RID: 20696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A59")]
		[Address(RVA = "0x10142CB90", Offset = "0x142CB90", VA = "0x10142CB90")]
		public void SetEnableEventFilterButton(bool flg)
		{
		}

		// Token: 0x060050D9 RID: 20697 RVA: 0x0001BED0 File Offset: 0x0001A0D0
		[Token(Token = "0x6004A5A")]
		[Address(RVA = "0x10142CA80", Offset = "0x142CA80", VA = "0x10142CA80")]
		public bool GetEnableEventFilterButton()
		{
			return default(bool);
		}

		// Token: 0x060050DA RID: 20698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A5B")]
		[Address(RVA = "0x10142CBF4", Offset = "0x142CBF4", VA = "0x10142CBF4")]
		public void SetEventFilterButtonDecided(bool isDecided)
		{
		}

		// Token: 0x060050DB RID: 20699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A5C")]
		[Address(RVA = "0x10142D7CC", Offset = "0x142D7CC", VA = "0x10142D7CC")]
		private void ApplyEventFilter()
		{
		}

		// Token: 0x060050DC RID: 20700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A5D")]
		[Address(RVA = "0x10142D8D0", Offset = "0x142D8D0", VA = "0x10142D8D0")]
		public void OnClickEventFilterButtonCallBack()
		{
		}

		// Token: 0x060050DD RID: 20701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A5E")]
		[Address(RVA = "0x10142CAC8", Offset = "0x142CAC8", VA = "0x10142CAC8")]
		public void SetEnableViewChangeResetButton(bool flg)
		{
		}

		// Token: 0x060050DE RID: 20702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A5F")]
		[Address(RVA = "0x10142D918", Offset = "0x142D918", VA = "0x10142D918")]
		public void SetInteractableResetButton(bool flg)
		{
		}

		// Token: 0x060050DF RID: 20703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A60")]
		[Address(RVA = "0x10142D9C8", Offset = "0x142D9C8", VA = "0x10142D9C8")]
		public void OnClickViewChangeResetButtonCallBack()
		{
		}

		// Token: 0x060050E0 RID: 20704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A61")]
		[Address(RVA = "0x10142DB38", Offset = "0x142DB38", VA = "0x10142DB38")]
		private void Request_ViewChangeReset()
		{
		}

		// Token: 0x060050E1 RID: 20705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A62")]
		[Address(RVA = "0x10142DC78", Offset = "0x142DC78", VA = "0x10142DC78")]
		private void OnResponse_ViewChangeReset(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060050E2 RID: 20706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A63")]
		[Address(RVA = "0x10142DDAC", Offset = "0x142DDAC", VA = "0x10142DDAC")]
		public CharaListWindow()
		{
		}

		// Token: 0x0400635B RID: 25435
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40045C1")]
		[SerializeField]
		private CharaListScroll m_Scroll;

		// Token: 0x0400635C RID: 25436
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40045C2")]
		[SerializeField]
		private Text m_CharaNumText;

		// Token: 0x0400635D RID: 25437
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40045C3")]
		[SerializeField]
		private CustomButton m_EventFilterButton;

		// Token: 0x0400635E RID: 25438
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40045C4")]
		[SerializeField]
		private CustomButton m_ViewChangeResetButton;

		// Token: 0x0400635F RID: 25439
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40045C5")]
		private long m_SelectCharaMngID;

		// Token: 0x04006360 RID: 25440
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40045C6")]
		private bool m_IsDonePrepare;

		// Token: 0x04006361 RID: 25441
		[Cpp2IlInjected.FieldOffset(Offset = "0xB1")]
		[Token(Token = "0x40045C7")]
		private bool m_IsCanceled;

		// Token: 0x04006362 RID: 25442
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40045C8")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04006363 RID: 25443
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40045C9")]
		private CharaListWindow.eStep m_Step;

		// Token: 0x04006364 RID: 25444
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40045CA")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x02001062 RID: 4194
		[Token(Token = "0x2001240")]
		private enum eStep
		{
			// Token: 0x0400636A RID: 25450
			[Token(Token = "0x40070D2")]
			Idle,
			// Token: 0x0400636B RID: 25451
			[Token(Token = "0x40070D3")]
			CharaDetail
		}
	}
}
