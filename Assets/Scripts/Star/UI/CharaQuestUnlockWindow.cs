﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C77 RID: 3191
	[Token(Token = "0x2000894")]
	[StructLayout(3)]
	public class CharaQuestUnlockWindow : MonoBehaviour
	{
		// Token: 0x06003A08 RID: 14856 RVA: 0x00018090 File Offset: 0x00016290
		[Token(Token = "0x60034EF")]
		[Address(RVA = "0x10142DDD0", Offset = "0x142DDD0", VA = "0x10142DDD0")]
		public bool GetAnswer()
		{
			return default(bool);
		}

		// Token: 0x06003A09 RID: 14857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F0")]
		[Address(RVA = "0x10142DDD8", Offset = "0x142DDD8", VA = "0x10142DDD8")]
		private void Start()
		{
		}

		// Token: 0x06003A0A RID: 14858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F1")]
		[Address(RVA = "0x10142DE68", Offset = "0x142DE68", VA = "0x10142DE68")]
		private void SetCharaIconInsts(List<int> charaIDList)
		{
		}

		// Token: 0x06003A0B RID: 14859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F2")]
		[Address(RVA = "0x10142E380", Offset = "0x142E380", VA = "0x10142E380")]
		private void SetWindowType(CharaQuestUnlockWindow.eCharaQuestUnlockWindowType type)
		{
		}

		// Token: 0x06003A0C RID: 14860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F3")]
		[Address(RVA = "0x10142E6BC", Offset = "0x142E6BC", VA = "0x10142E6BC")]
		public void Open(List<int> charaIDList, Action<bool> onConfirmGotoQuest)
		{
		}

		// Token: 0x06003A0D RID: 14861 RVA: 0x000180A8 File Offset: 0x000162A8
		[Token(Token = "0x60034F4")]
		[Address(RVA = "0x10142E708", Offset = "0x142E708", VA = "0x10142E708")]
		public bool OpenSubOffer(List<int> charaIDs, Action<bool> onConfirmGotoQuest)
		{
			return default(bool);
		}

		// Token: 0x06003A0E RID: 14862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F5")]
		[Address(RVA = "0x10142E7C0", Offset = "0x142E7C0", VA = "0x10142E7C0")]
		private void OnEnd()
		{
		}

		// Token: 0x06003A0F RID: 14863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F6")]
		[Address(RVA = "0x10142E814", Offset = "0x142E814", VA = "0x10142E814")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003A10 RID: 14864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F7")]
		[Address(RVA = "0x10142E81C", Offset = "0x142E81C", VA = "0x10142E81C")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x06003A11 RID: 14865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F8")]
		[Address(RVA = "0x10142E858", Offset = "0x142E858", VA = "0x10142E858")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x06003A12 RID: 14866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034F9")]
		[Address(RVA = "0x10142E890", Offset = "0x142E890", VA = "0x10142E890")]
		public CharaQuestUnlockWindow()
		{
		}

		// Token: 0x040048E3 RID: 18659
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400334B")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040048E4 RID: 18660
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400334C")]
		[SerializeField]
		private GameObject[] m_CharaIconObjs;

		// Token: 0x040048E5 RID: 18661
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400334D")]
		[SerializeField]
		private PrefabCloner[] m_CharaIconCloners;

		// Token: 0x040048E6 RID: 18662
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400334E")]
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x040048E7 RID: 18663
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400334F")]
		[SerializeField]
		private Text m_MainTitleText;

		// Token: 0x040048E8 RID: 18664
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003350")]
		[SerializeField]
		private Text m_MainText;

		// Token: 0x040048E9 RID: 18665
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003351")]
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x040048EA RID: 18666
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003352")]
		[SerializeField]
		private CustomButton m_NoButton;

		// Token: 0x040048EB RID: 18667
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003353")]
		[SerializeField]
		private Text m_OfferTitleText;

		// Token: 0x040048EC RID: 18668
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003354")]
		[SerializeField]
		private Text m_OfferText;

		// Token: 0x040048ED RID: 18669
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003355")]
		[SerializeField]
		private CustomButton m_CloseButton;

		// Token: 0x040048EE RID: 18670
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003356")]
		private Action<bool> m_OnConfirmGotoQuest;

		// Token: 0x040048EF RID: 18671
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003357")]
		private bool m_Answer;

		// Token: 0x02000C78 RID: 3192
		[Token(Token = "0x20010AB")]
		private enum eCharaQuestUnlockWindowType
		{
			// Token: 0x040048F1 RID: 18673
			[Token(Token = "0x40068EB")]
			MemorialQuest,
			// Token: 0x040048F2 RID: 18674
			[Token(Token = "0x40068EC")]
			SubOffer
		}
	}
}
