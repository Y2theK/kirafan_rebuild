﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C90 RID: 3216
	[Token(Token = "0x200089A")]
	[StructLayout(3)]
	public class CharaStatus : MonoBehaviour
	{
		// Token: 0x06003A8A RID: 14986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600354D")]
		[Address(RVA = "0x101420584", Offset = "0x1420584", VA = "0x101420584")]
		public void Apply(int[] values, [Optional] int[] addValues)
		{
		}

		// Token: 0x06003A8B RID: 14987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600354E")]
		[Address(RVA = "0x10142E898", Offset = "0x142E898", VA = "0x10142E898")]
		public void ApplyNA()
		{
		}

		// Token: 0x06003A8C RID: 14988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600354F")]
		[Address(RVA = "0x10142E9BC", Offset = "0x142E9BC", VA = "0x10142E9BC")]
		public CharaStatus()
		{
		}

		// Token: 0x0400494F RID: 18767
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003388")]
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x04004950 RID: 18768
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003389")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122CF8", Offset = "0x122CF8")]
		private Text[] m_AddValueText;

		// Token: 0x02000C91 RID: 3217
		[Token(Token = "0x20010BE")]
		public enum eStatus
		{
			// Token: 0x04004952 RID: 18770
			[Token(Token = "0x400691A")]
			Hp,
			// Token: 0x04004953 RID: 18771
			[Token(Token = "0x400691B")]
			Atk,
			// Token: 0x04004954 RID: 18772
			[Token(Token = "0x400691C")]
			Mgc,
			// Token: 0x04004955 RID: 18773
			[Token(Token = "0x400691D")]
			Def,
			// Token: 0x04004956 RID: 18774
			[Token(Token = "0x400691E")]
			MDef,
			// Token: 0x04004957 RID: 18775
			[Token(Token = "0x400691F")]
			Spd,
			// Token: 0x04004958 RID: 18776
			[Token(Token = "0x4006920")]
			Num
		}
	}
}
