﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C92 RID: 3218
	[Token(Token = "0x200089B")]
	[StructLayout(3)]
	public class CharaStatusBase : MonoBehaviour
	{
		// Token: 0x06003A8D RID: 14989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003550")]
		[Address(RVA = "0x10142E9C4", Offset = "0x142E9C4", VA = "0x10142E9C4")]
		protected void Apply(string[] values)
		{
		}

		// Token: 0x06003A8E RID: 14990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003551")]
		[Address(RVA = "0x10142E9D0", Offset = "0x142E9D0", VA = "0x10142E9D0")]
		protected void Apply(Text[] targets, string[] values)
		{
		}

		// Token: 0x06003A8F RID: 14991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003552")]
		[Address(RVA = "0x10142EC98", Offset = "0x142EC98", VA = "0x10142EC98")]
		public CharaStatusBase()
		{
		}

		// Token: 0x04004959 RID: 18777
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400338A")]
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x02000C93 RID: 3219
		[Token(Token = "0x20010BF")]
		public enum eStatus
		{
			// Token: 0x0400495B RID: 18779
			[Token(Token = "0x4006922")]
			Hp,
			// Token: 0x0400495C RID: 18780
			[Token(Token = "0x4006923")]
			Atk,
			// Token: 0x0400495D RID: 18781
			[Token(Token = "0x4006924")]
			Mgc,
			// Token: 0x0400495E RID: 18782
			[Token(Token = "0x4006925")]
			Def,
			// Token: 0x0400495F RID: 18783
			[Token(Token = "0x4006926")]
			MDef,
			// Token: 0x04004960 RID: 18784
			[Token(Token = "0x4006927")]
			Spd,
			// Token: 0x04004961 RID: 18785
			[Token(Token = "0x4006928")]
			Num
		}
	}
}
