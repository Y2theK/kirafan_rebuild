﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C94 RID: 3220
	[Token(Token = "0x200089C")]
	[StructLayout(3)]
	public class CharaStatusPercent : CharaStatusBase
	{
		// Token: 0x06003A90 RID: 14992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003553")]
		[Address(RVA = "0x10142ECA0", Offset = "0x142ECA0", VA = "0x10142ECA0")]
		public void Apply(float[] values)
		{
		}

		// Token: 0x06003A91 RID: 14993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003554")]
		[Address(RVA = "0x10142EE50", Offset = "0x142EE50", VA = "0x10142EE50")]
		public CharaStatusPercent()
		{
		}
	}
}
