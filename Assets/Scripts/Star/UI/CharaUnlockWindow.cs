﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C79 RID: 3193
	[Token(Token = "0x2000895")]
	[StructLayout(3)]
	public class CharaUnlockWindow : MonoBehaviour
	{
		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06003A13 RID: 14867 RVA: 0x000180C0 File Offset: 0x000162C0
		[Token(Token = "0x17000429")]
		public bool IsTransitADV
		{
			[Token(Token = "0x60034FA")]
			[Address(RVA = "0x10142EE58", Offset = "0x142EE58", VA = "0x10142EE58")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06003A14 RID: 14868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034FB")]
		[Address(RVA = "0x10142EE60", Offset = "0x142EE60", VA = "0x10142EE60")]
		private void SetPopupType(CharaUnlockWindow.ePopupType pType)
		{
		}

		// Token: 0x06003A15 RID: 14869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034FC")]
		[Address(RVA = "0x10142EF38", Offset = "0x142EF38", VA = "0x10142EF38")]
		private void Start()
		{
		}

		// Token: 0x06003A16 RID: 14870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034FD")]
		[Address(RVA = "0x10142EFC8", Offset = "0x142EFC8", VA = "0x10142EFC8")]
		private void Update()
		{
		}

		// Token: 0x06003A17 RID: 14871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034FE")]
		[Address(RVA = "0x10142EFD8", Offset = "0x142EFD8", VA = "0x10142EFD8")]
		public void AddPopParam(int charaID, int beforeFriendship, int afterFriendship)
		{
		}

		// Token: 0x06003A18 RID: 14872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034FF")]
		[Address(RVA = "0x10142F4A8", Offset = "0x142F4A8", VA = "0x10142F4A8")]
		public void AddOfferPop(eTitleType type, string titleName, bool isRepeat)
		{
		}

		// Token: 0x06003A19 RID: 14873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003500")]
		[Address(RVA = "0x10142F734", Offset = "0x142F734", VA = "0x10142F734")]
		public void AddOfferCompPopOnPlayerResult(eTitleType type, string titleName)
		{
		}

		// Token: 0x06003A1A RID: 14874 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003501")]
		[Address(RVA = "0x10142F8A0", Offset = "0x142F8A0", VA = "0x10142F8A0")]
		private List<int> GetLimitADV(eCharaNamedType named, int beforeFriendship, int afterFriendship)
		{
			return null;
		}

		// Token: 0x06003A1B RID: 14875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003502")]
		[Address(RVA = "0x10142F20C", Offset = "0x142F20C", VA = "0x10142F20C")]
		private void AddUnlockADV(int charaID, int beforeFriendship, int afterFriendship)
		{
		}

		// Token: 0x06003A1C RID: 14876 RVA: 0x000180D8 File Offset: 0x000162D8
		[Token(Token = "0x6003503")]
		[Address(RVA = "0x10142FCC0", Offset = "0x142FCC0", VA = "0x10142FCC0")]
		public bool OpenStackPop()
		{
			return default(bool);
		}

		// Token: 0x06003A1D RID: 14877 RVA: 0x000180F0 File Offset: 0x000162F0
		[Token(Token = "0x6003504")]
		[Address(RVA = "0x101430D6C", Offset = "0x1430D6C", VA = "0x101430D6C")]
		public bool OpenOfferStackPop()
		{
			return default(bool);
		}

		// Token: 0x06003A1E RID: 14878 RVA: 0x00018108 File Offset: 0x00016308
		[Token(Token = "0x6003505")]
		[Address(RVA = "0x1014311F8", Offset = "0x14311F8", VA = "0x1014311F8")]
		public bool OpenOfferCompPopOnPlayerResult(Action onFinished)
		{
			return default(bool);
		}

		// Token: 0x06003A1F RID: 14879 RVA: 0x00018120 File Offset: 0x00016320
		[Token(Token = "0x6003506")]
		[Address(RVA = "0x101431578", Offset = "0x1431578", VA = "0x101431578")]
		public bool OpenStackADVPop(Action OnADVConfirm, [Optional] Action OnEndADVUnlockWindow)
		{
			return default(bool);
		}

		// Token: 0x06003A20 RID: 14880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003507")]
		[Address(RVA = "0x101430B08", Offset = "0x1430B08", VA = "0x101430B08")]
		private void OnEnd()
		{
		}

		// Token: 0x06003A21 RID: 14881 RVA: 0x00018138 File Offset: 0x00016338
		[Token(Token = "0x6003508")]
		[Address(RVA = "0x101431A9C", Offset = "0x1431A9C", VA = "0x101431A9C")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06003A22 RID: 14882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003509")]
		[Address(RVA = "0x101431ACC", Offset = "0x1431ACC", VA = "0x101431ACC")]
		public void OnClickCloseCallBack()
		{
		}

		// Token: 0x06003A23 RID: 14883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600350A")]
		[Address(RVA = "0x101431B04", Offset = "0x1431B04", VA = "0x101431B04")]
		public void OnClickTransitADVCallBack()
		{
		}

		// Token: 0x06003A24 RID: 14884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600350B")]
		[Address(RVA = "0x101431B40", Offset = "0x1431B40", VA = "0x101431B40")]
		public CharaUnlockWindow()
		{
		}

		// Token: 0x040048F3 RID: 18675
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003358")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040048F4 RID: 18676
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003359")]
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x040048F5 RID: 18677
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400335A")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x040048F6 RID: 18678
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400335B")]
		[SerializeField]
		private Text m_MainText;

		// Token: 0x040048F7 RID: 18679
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400335C")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040048F8 RID: 18680
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400335D")]
		[SerializeField]
		private GameObject m_UnlockButtonObj;

		// Token: 0x040048F9 RID: 18681
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400335E")]
		[SerializeField]
		private GameObject m_ADVButtonObj;

		// Token: 0x040048FA RID: 18682
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400335F")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x040048FB RID: 18683
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003360")]
		[SerializeField]
		private GameObject m_TitleObj;

		// Token: 0x040048FC RID: 18684
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003361")]
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x040048FD RID: 18685
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003362")]
		private List<CharaUnlockWindow.PopParam> m_PopParamList;

		// Token: 0x040048FE RID: 18686
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003363")]
		private List<CharaUnlockWindow.PopOfferParam> m_PopOfferParamList;

		// Token: 0x040048FF RID: 18687
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003364")]
		private List<CharaUnlockWindow.PopOfferCompParam> m_PopOfferCompParamList;

		// Token: 0x04004900 RID: 18688
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003365")]
		private List<CharaUnlockWindow.UnlockADVParam> m_UnlockADVList;

		// Token: 0x04004901 RID: 18689
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003366")]
		private Action m_OnConfirmADV;

		// Token: 0x04004902 RID: 18690
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003367")]
		private Action m_OnEndADVUnlockWindow;

		// Token: 0x04004903 RID: 18691
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003368")]
		private Action m_OnEndOfferComp;

		// Token: 0x04004904 RID: 18692
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003369")]
		private bool m_TransitADV;

		// Token: 0x04004905 RID: 18693
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x400336A")]
		private CharaUnlockWindow.ePopupType m_PopupType;

		// Token: 0x02000C7A RID: 3194
		[Token(Token = "0x20010AC")]
		private class PopParam
		{
			// Token: 0x06003A25 RID: 14885 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061CD")]
			[Address(RVA = "0x10142F1CC", Offset = "0x142F1CC", VA = "0x10142F1CC")]
			public PopParam(int charaID, eCharaNamedType named, int friendship)
			{
			}

			// Token: 0x04004906 RID: 18694
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068ED")]
			public int m_CharaID;

			// Token: 0x04004907 RID: 18695
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40068EE")]
			public eCharaNamedType m_NamedType;

			// Token: 0x04004908 RID: 18696
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068EF")]
			public int m_Friendship;
		}

		// Token: 0x02000C7B RID: 3195
		[Token(Token = "0x20010AD")]
		private class PopOfferParam
		{
			// Token: 0x06003A26 RID: 14886 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061CE")]
			[Address(RVA = "0x10142F6F0", Offset = "0x142F6F0", VA = "0x10142F6F0")]
			public PopOfferParam(eTitleType tt, string titleName, bool isRepeat)
			{
			}

			// Token: 0x04004909 RID: 18697
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068F0")]
			public eTitleType m_TitleType;

			// Token: 0x0400490A RID: 18698
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068F1")]
			public string m_TitleName;

			// Token: 0x0400490B RID: 18699
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40068F2")]
			public bool m_IsRepeatOffer;
		}

		// Token: 0x02000C7C RID: 3196
		[Token(Token = "0x20010AE")]
		private class PopOfferCompParam
		{
			// Token: 0x06003A27 RID: 14887 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061CF")]
			[Address(RVA = "0x10142F864", Offset = "0x142F864", VA = "0x10142F864")]
			public PopOfferCompParam(eTitleType tt, string titleName)
			{
			}

			// Token: 0x0400490C RID: 18700
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068F3")]
			public eTitleType m_TitleType;

			// Token: 0x0400490D RID: 18701
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068F4")]
			public string m_TitleName;
		}

		// Token: 0x02000C7D RID: 3197
		[Token(Token = "0x20010AF")]
		private class UnlockADVParam
		{
			// Token: 0x06003A28 RID: 14888 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061D0")]
			[Address(RVA = "0x10142FC88", Offset = "0x142FC88", VA = "0x10142FC88")]
			public UnlockADVParam(int charaID, int advID)
			{
			}

			// Token: 0x0400490E RID: 18702
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068F5")]
			public int m_CharaID;

			// Token: 0x0400490F RID: 18703
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40068F6")]
			public int m_ADVID;
		}

		// Token: 0x02000C7E RID: 3198
		[Token(Token = "0x20010B0")]
		private enum ePopupType
		{
			// Token: 0x04004911 RID: 18705
			[Token(Token = "0x40068F8")]
			Stack,
			// Token: 0x04004912 RID: 18706
			[Token(Token = "0x40068F9")]
			ADV,
			// Token: 0x04004913 RID: 18707
			[Token(Token = "0x40068FA")]
			Offer,
			// Token: 0x04004914 RID: 18708
			[Token(Token = "0x40068FB")]
			OfferComp = 4
		}
	}
}
