﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using Star.UI.GachaSelect;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C4D RID: 3149
	[Token(Token = "0x200086E")]
	[StructLayout(3)]
	public class CharacterInformationPanel : MonoBehaviour
	{
		// Token: 0x060038E8 RID: 14568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D2")]
		[Address(RVA = "0x101431C28", Offset = "0x1431C28", VA = "0x101431C28", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x060038E9 RID: 14569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D3")]
		[Address(RVA = "0x101431F8C", Offset = "0x1431F8C", VA = "0x101431F8C", Slot = "5")]
		public virtual void Destroy()
		{
		}

		// Token: 0x060038EA RID: 14570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D4")]
		[Address(RVA = "0x101432330", Offset = "0x1432330", VA = "0x101432330", Slot = "6")]
		protected virtual void ResetDirtyFlag()
		{
		}

		// Token: 0x060038EB RID: 14571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D5")]
		[Address(RVA = "0x10143233C", Offset = "0x143233C", VA = "0x10143233C", Slot = "7")]
		public virtual void ApplyGachaDetailDropListCharacter(GachaDetailDropListItemDataController gddlidc)
		{
		}

		// Token: 0x060038EC RID: 14572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D6")]
		[Address(RVA = "0x10143252C", Offset = "0x143252C", VA = "0x10143252C")]
		public void SetTitleImage(int charaId)
		{
		}

		// Token: 0x060038ED RID: 14573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D7")]
		[Address(RVA = "0x101432830", Offset = "0x1432830", VA = "0x101432830")]
		public void SetCharaInfoTitle(int charaId)
		{
		}

		// Token: 0x060038EE RID: 14574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D8")]
		[Address(RVA = "0x1014329A0", Offset = "0x14329A0", VA = "0x1014329A0")]
		public void SetCharaInfoTitleCost(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060038EF RID: 14575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D9")]
		[Address(RVA = "0x101432AA4", Offset = "0x1432AA4", VA = "0x101432AA4")]
		public void SetArousalLv2CharaInfoTitle(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060038F0 RID: 14576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DA")]
		[Address(RVA = "0x101432BA0", Offset = "0x1432BA0", VA = "0x101432BA0")]
		public void SetReflection(CharacterDataController cdc)
		{
		}

		// Token: 0x060038F1 RID: 14577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DB")]
		[Address(RVA = "0x101432D34", Offset = "0x1432D34", VA = "0x101432D34")]
		public void SetReflection(bool active)
		{
		}

		// Token: 0x060038F2 RID: 14578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DC")]
		[Address(RVA = "0x101432DE4", Offset = "0x1432DE4", VA = "0x101432DE4")]
		public void RequestLoadCharacterViewCharacterIllustOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
		}

		// Token: 0x060038F3 RID: 14579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DD")]
		[Address(RVA = "0x101432EA0", Offset = "0x1432EA0", VA = "0x101432EA0")]
		public void RequestLoadCharacterViewCharacterBustImageOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
		}

		// Token: 0x060038F4 RID: 14580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DE")]
		[Address(RVA = "0x101432F5C", Offset = "0x1432F5C", VA = "0x101432F5C")]
		public void RequestLoadCharacterViewCharacterIconOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
		}

		// Token: 0x060038F5 RID: 14581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033DF")]
		[Address(RVA = "0x101433018", Offset = "0x1433018", VA = "0x101433018")]
		public void ForceRequestLoadCharacterView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc)
		{
		}

		// Token: 0x060038F6 RID: 14582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E0")]
		[Address(RVA = "0x101432758", Offset = "0x1432758", VA = "0x101432758")]
		public void ForceRequestLoadCharacterView(SelectedCharaInfoCharacterViewController.eViewType viewType, int charaID, bool isUserChara, int weaponID)
		{
		}

		// Token: 0x060038F7 RID: 14583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E1")]
		[Address(RVA = "0x1014330D4", Offset = "0x14330D4", VA = "0x1014330D4")]
		public void SetCharacterIconLimitBreak(int lb)
		{
		}

		// Token: 0x060038F8 RID: 14584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E2")]
		[Address(RVA = "0x101433184", Offset = "0x1433184", VA = "0x101433184")]
		public void SetCharacterIconArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x060038F9 RID: 14585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E3")]
		[Address(RVA = "0x101432690", Offset = "0x1432690", VA = "0x101432690")]
		public void SetEnableRenderCharacterView(bool flg)
		{
		}

		// Token: 0x060038FA RID: 14586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E4")]
		[Address(RVA = "0x10143323C", Offset = "0x143323C", VA = "0x10143323C")]
		public void SetBeforeLevel(int nowLevel, int maxLevel, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x060038FB RID: 14587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E5")]
		[Address(RVA = "0x101433308", Offset = "0x1433308", VA = "0x101433308")]
		public void SetAfterLevel(int nowLevel, int maxLevel, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x060038FC RID: 14588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E6")]
		[Address(RVA = "0x1014333D4", Offset = "0x14333D4", VA = "0x1014333D4")]
		public void SetBeforeLimitBreakIconValue(int value)
		{
		}

		// Token: 0x060038FD RID: 14589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E7")]
		[Address(RVA = "0x101433484", Offset = "0x1433484", VA = "0x101433484")]
		public void SetAfterLimitBreakIconValue(int value)
		{
		}

		// Token: 0x060038FE RID: 14590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E8")]
		[Address(RVA = "0x101433534", Offset = "0x1433534", VA = "0x101433534")]
		public void SetExpDataEquipWeapon(EquipWeaponCharacterDataController partyMember)
		{
		}

		// Token: 0x060038FF RID: 14591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033E9")]
		[Address(RVA = "0x101433650", Offset = "0x1433650", VA = "0x101433650")]
		public void SetCost(int cost)
		{
		}

		// Token: 0x06003900 RID: 14592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033EA")]
		[Address(RVA = "0x101433700", Offset = "0x1433700", VA = "0x101433700")]
		public void ApplyState(EquipWeaponCharacterDataController partyMember)
		{
		}

		// Token: 0x06003901 RID: 14593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033EB")]
		[Address(RVA = "0x1014337B0", Offset = "0x14337B0", VA = "0x1014337B0")]
		public void SetStateWeaponIcon(int weaponId)
		{
		}

		// Token: 0x06003902 RID: 14594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033EC")]
		[Address(RVA = "0x101433860", Offset = "0x1433860", VA = "0x101433860")]
		public void SetStateWeaponIcon(EquipWeaponCharacterDataController partyMember)
		{
		}

		// Token: 0x06003903 RID: 14595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033ED")]
		[Address(RVA = "0x101433910", Offset = "0x1433910", VA = "0x101433910")]
		public void SetStateWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
		}

		// Token: 0x06003904 RID: 14596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033EE")]
		[Address(RVA = "0x1014339C0", Offset = "0x14339C0", VA = "0x1014339C0")]
		protected void ApplyEventQuestInfo(CharacterDataController cdc)
		{
		}

		// Token: 0x06003905 RID: 14597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033EF")]
		[Address(RVA = "0x101433A70", Offset = "0x1433A70", VA = "0x101433A70")]
		public CharacterInformationPanel()
		{
		}

		// Token: 0x04004830 RID: 18480
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032AC")]
		[SerializeField]
		private TitleImage m_TitleImage;

		// Token: 0x04004831 RID: 18481
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032AD")]
		[SerializeField]
		private SelectedCharaInfoTitle m_InfoTitle;

		// Token: 0x04004832 RID: 18482
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032AE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012233C", Offset = "0x12233C")]
		private BustImage m_BustImage;

		// Token: 0x04004833 RID: 18483
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032AF")]
		[SerializeField]
		private SelectedCharaInfoCharacterViewController m_CharaViewController;

		// Token: 0x04004834 RID: 18484
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40032B0")]
		[SerializeField]
		private SelectedCharaInfoState m_InfoState;

		// Token: 0x04004835 RID: 18485
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40032B1")]
		[SerializeField]
		private SelectedCharaInfoArousalLv m_ArousalLv;

		// Token: 0x04004836 RID: 18486
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40032B2")]
		[SerializeField]
		private SelectedCharaInfoFriendship m_Friendship;

		// Token: 0x04004837 RID: 18487
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40032B3")]
		[SerializeField]
		private SelectedCharaInfoDetailInfoList m_SkillList;

		// Token: 0x04004838 RID: 18488
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40032B4")]
		[SerializeField]
		private SelectedCharaInfoProfileMessageField m_ProfileMessage;

		// Token: 0x04004839 RID: 18489
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40032B5")]
		[SerializeField]
		private SelectedCharaInfoEventQuestInfo m_EventQuestInfo;

		// Token: 0x0400483A RID: 18490
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40032B6")]
		[SerializeField]
		private SelectedCharaInfoCharacterVoiceNameField m_CharacterVoiceName;

		// Token: 0x0400483B RID: 18491
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40032B7")]
		[SerializeField]
		private SelectedCharaInfoVoiceList m_VoiceList;

		// Token: 0x0400483C RID: 18492
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40032B8")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122418", Offset = "0x122418")]
		private SelectedCharaInfoHaveTitleTextField[] m_HaveTitleTextFields;

		// Token: 0x0400483D RID: 18493
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40032B9")]
		[SerializeField]
		protected ReflectFrameImage m_Reflection;

		// Token: 0x0400483E RID: 18494
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40032BA")]
		protected bool m_IsDirtyResource;

		// Token: 0x0400483F RID: 18495
		[Cpp2IlInjected.FieldOffset(Offset = "0x89")]
		[Token(Token = "0x40032BB")]
		protected bool m_IsDirtyParameter;
	}
}
