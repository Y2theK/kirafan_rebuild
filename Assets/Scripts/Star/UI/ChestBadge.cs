﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C95 RID: 3221
	[Token(Token = "0x200089D")]
	[StructLayout(3)]
	public class ChestBadge : MonoBehaviour
	{
		// Token: 0x06003A92 RID: 14994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003555")]
		[Address(RVA = "0x101433A80", Offset = "0x1433A80", VA = "0x101433A80")]
		public void Apply(int value)
		{
		}

		// Token: 0x06003A93 RID: 14995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003556")]
		[Address(RVA = "0x101433BA0", Offset = "0x1433BA0", VA = "0x101433BA0")]
		public ChestBadge()
		{
		}

		// Token: 0x04004962 RID: 18786
		[Token(Token = "0x400338B")]
		public const int MAX_VALUE = 999;

		// Token: 0x04004963 RID: 18787
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400338C")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04004964 RID: 18788
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400338D")]
		[SerializeField]
		private Text m_Text;
	}
}
