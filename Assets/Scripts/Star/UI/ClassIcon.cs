﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CE0 RID: 3296
	[Token(Token = "0x20008CC")]
	[StructLayout(3)]
	public class ClassIcon : MonoBehaviour
	{
		// Token: 0x06003C5B RID: 15451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003718")]
		[Address(RVA = "0x1014235D8", Offset = "0x14235D8", VA = "0x1014235D8")]
		public void Apply(eClassType classType)
		{
		}

		// Token: 0x06003C5C RID: 15452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003719")]
		[Address(RVA = "0x101433BA8", Offset = "0x1433BA8", VA = "0x101433BA8")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x06003C5D RID: 15453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371A")]
		[Address(RVA = "0x101433C64", Offset = "0x1433C64", VA = "0x101433C64")]
		public ClassIcon()
		{
		}

		// Token: 0x04004BA0 RID: 19360
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003524")]
		[SerializeField]
		private Sprite m_Fighter;

		// Token: 0x04004BA1 RID: 19361
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003525")]
		[SerializeField]
		private Sprite m_Magician;

		// Token: 0x04004BA2 RID: 19362
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003526")]
		[SerializeField]
		private Sprite m_Priest;

		// Token: 0x04004BA3 RID: 19363
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003527")]
		[SerializeField]
		private Sprite m_Knight;

		// Token: 0x04004BA4 RID: 19364
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003528")]
		[SerializeField]
		private Sprite m_Alchemist;

		// Token: 0x04004BA5 RID: 19365
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003529")]
		private Image m_IconObj;

		// Token: 0x04004BA6 RID: 19366
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400352A")]
		private GameObject m_GameObject;
	}
}
