﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C67 RID: 3175
	[Token(Token = "0x2000885")]
	[Serializable]
	[StructLayout(3)]
	public class CloneInfo<CloneType> : BCloneInfo where CloneType : MonoBehaviour
	{
		// Token: 0x0600399E RID: 14750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003485")]
		[Address(RVA = "0x1016CDFB0", Offset = "0x16CDFB0", VA = "0x1016CDFB0", Slot = "4")]
		public override void Setup(Transform parent)
		{
		}

		// Token: 0x0600399F RID: 14751 RVA: 0x00017FE8 File Offset: 0x000161E8
		[Token(Token = "0x6003486")]
		[Address(RVA = "0x1016CE2EC", Offset = "0x16CE2EC", VA = "0x1016CE2EC", Slot = "5")]
		public override bool IsExistPrefab()
		{
			return default(bool);
		}

		// Token: 0x060039A0 RID: 14752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003487")]
		[Address(RVA = "0x1016CE35C", Offset = "0x16CE35C", VA = "0x1016CE35C", Slot = "6")]
		public override void SetLocalPositionX(float x)
		{
		}

		// Token: 0x060039A1 RID: 14753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003488")]
		[Address(RVA = "0x1016CE3A4", Offset = "0x16CE3A4", VA = "0x1016CE3A4", Slot = "7")]
		public override void SetLocalPositionY(float y)
		{
		}

		// Token: 0x060039A2 RID: 14754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003489")]
		[Address(RVA = "0x1016CE3EC", Offset = "0x16CE3EC", VA = "0x1016CE3EC")]
		public CloneInfo()
		{
		}

		// Token: 0x04004882 RID: 18562
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40032EE")]
		public CloneType m_Prefab;

		// Token: 0x04004883 RID: 18563
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40032EF")]
		public RectTransform m_FitCloneRect;

		// Token: 0x04004884 RID: 18564
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40032F0")]
		[HideInInspector]
		public CloneType m_Clone;
	}
}
