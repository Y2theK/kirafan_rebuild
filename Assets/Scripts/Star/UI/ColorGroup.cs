﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class ColorGroup : MonoBehaviour {
        [SerializeField] private bool m_IgnoreParentColorGroup;

        private Color m_Color = Color.black;
        private Material m_Material;
        private bool m_MaterialChange;
        private float m_Rate;
        private eColorBlendMode m_ColorBlendMode;
        private List<ColorStatus> m_ColorStatusList = new List<ColorStatus>();
        private List<Graphic> m_IgnoreGraphicList = new List<Graphic>();
        private bool m_IsDirty = true;
        private bool m_IsDonePrepare;

        public bool IgnoreParentColorGroup => m_IgnoreParentColorGroup;

        public void AddIgnoreGraphic(Graphic graphic) {
            m_IgnoreGraphicList.Add(graphic);
        }

        private void Prepare() {
            foreach (GraphForButton graph in GetComponentsInChildren<GraphForButton>(true)) {
                graph.Prepare();
            }
            m_IsDonePrepare = true;
        }

        private void LateUpdate() {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            if (m_IsDirty) {
                UpdateColor();
                m_IsDirty = false;
            }
        }

        public void UpdateColor() {
            foreach (ColorGroup colorGroup in GetComponentsInChildren<ColorGroup>(true)) {
                if (colorGroup.gameObject == gameObject) { continue; }
                colorGroup.UpdateColor();
                if (colorGroup.IgnoreParentColorGroup) {
                    foreach (Graphic graphic in GetComponentsInChildren<Graphic>(true)) {
                        if (!m_IgnoreGraphicList.Contains(graphic)) {
                            m_IgnoreGraphicList.Add(graphic);
                        }
                    }
                }
            }
            foreach (Graphic graphic in GetComponentsInChildren<Graphic>(true)) {
                if (m_IgnoreGraphicList.Contains(graphic)) { continue; }
                ColorStatus colorStatus = m_ColorStatusList.Find(s => s.graph == graphic);
                if (colorStatus == null) {
                    colorStatus = new ColorStatus();
                    colorStatus.graph = graphic;
                    colorStatus.m_DefaultColor = graphic.color;
                    colorStatus.material = graphic.material;
                    colorStatus.m_CurrentColor = graphic.color;
                    m_ColorStatusList.Add(colorStatus);
                }
                if ((Mathf.Abs(colorStatus.m_CurrentColor.r - graphic.color.r) > 0.01f
                    || Mathf.Abs(colorStatus.m_CurrentColor.g - graphic.color.g) > 0.01f
                    || Mathf.Abs(colorStatus.m_CurrentColor.b - graphic.color.b) > 0.01f
                    || Mathf.Abs(colorStatus.m_CurrentColor.a - graphic.color.a) > 0.01f)
                    && (Mathf.Abs(colorStatus.m_DefaultColor.r - graphic.color.r) > 0.01f
                    || Mathf.Abs(colorStatus.m_DefaultColor.g - graphic.color.g) > 0.01f
                    || Mathf.Abs(colorStatus.m_DefaultColor.b - graphic.color.b) > 0.01f
                    || Mathf.Abs(colorStatus.m_DefaultColor.a - graphic.color.a) > 0.01f)) {
                    colorStatus.m_DefaultColor = graphic.color;
                }
                if (m_ColorBlendMode == eColorBlendMode.Mul || graphic.GetComponent<Text>() == null) {
                    Color color = colorStatus.m_DefaultColor;
                    if (m_ColorBlendMode == eColorBlendMode.Set) {
                        color = Color.black + (m_Color - Color.black) * m_Rate;
                    } else if (m_ColorBlendMode == eColorBlendMode.Mul) {
                        color = color + (color * m_Color - color) * m_Rate;
                    } else if (m_ColorBlendMode == eColorBlendMode.Normal) {
                        color = color * (1f - m_Rate) + m_Color * m_Rate;
                    }
                    graphic.color = color;
                    if (m_MaterialChange) {
                        graphic.material = m_Material;
                    }
                }
            }
            foreach (GraphForButton graph in GetComponentsInChildren<GraphForButton>(true)) {
                graph.ResetState();
            }
            SaveCurrentColor();
            m_IsDirty = false;
        }

        public void SaveCurrentColor() {
            foreach (ColorGroup colorGroup in GetComponentsInChildren<ColorGroup>(true)) {
                if (colorGroup.gameObject != gameObject) {
                    colorGroup.SaveCurrentColor();
                }
            }
            foreach (ColorStatus colorStatus in m_ColorStatusList) {
                colorStatus.m_CurrentColor = colorStatus.graph.color;
            }
        }

        public void ChangeColor(Color col, eColorBlendMode blendMode = eColorBlendMode.Mul, float rate = 1f) {
            m_Color = col;
            m_MaterialChange = false;
            m_Material = null;
            m_ColorBlendMode = blendMode;
            m_Rate = rate;
            m_IsDirty = true;
            foreach (ColorGroup colorGroup in GetComponentsInParent<ColorGroup>()) {
                colorGroup.m_IsDirty = true;
            }
            if (!m_IsDonePrepare) {
                Prepare();
            }
        }

        public void ChangeColor(Color col, Material mat, eColorBlendMode blendMode = eColorBlendMode.Mul, float rate = 1f) {
            m_Color = col;
            m_MaterialChange = true;
            m_Material = mat;
            m_ColorBlendMode = blendMode;
            m_Rate = rate;
            m_IsDirty = true;
            foreach (ColorGroup colorGroup in GetComponentsInParent<ColorGroup>()) {
                colorGroup.m_IsDirty = true;
            }
            if (!m_IsDonePrepare) {
                Prepare();
            }
        }

        public void RevertColor() {
            m_IsDirty = true;
            m_Color = Color.white;
            m_MaterialChange = false;
            m_Material = null;
            m_Rate = 0f;

            if (m_ColorStatusList == null) { return; }

            foreach (ColorStatus colorStatus in m_ColorStatusList) {
                bool flag = false;
                foreach (Graphic graphic in m_IgnoreGraphicList) {
                    if (graphic == colorStatus.graph) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    colorStatus.graph.color = colorStatus.m_DefaultColor;
                    colorStatus.m_CurrentColor = colorStatus.m_DefaultColor;
                    colorStatus.graph.material = colorStatus.material;
                }
            }
        }

        public enum eColorBlendMode {
            Normal,
            Mul,
            Set
        }

        private class ColorStatus {
            public Graphic graph;
            public Color m_CurrentColor;
            public Color m_DefaultColor;
            public Material material;
        }
    }
}
