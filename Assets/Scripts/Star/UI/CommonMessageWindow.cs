﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI {
    public class CommonMessageWindow : MonoBehaviour {
        private const float WINDOW_HEIGHT_MARGIN = 200f;
        private const float WINDOW_HEIGHT_BUTTON_SPACE = 100f;
        private const float WINDOW_HEIGHT_MAX = 720f;
        private const float TEXTBOX_HEIGHT_MAX = 450f;

        public const int ANSWER_YES = 0;
        public const int ANSWER_NO = 1;

        [SerializeField] private float m_AnimDuration = 1f;
        [SerializeField] private GameObject m_Body;
        [SerializeField] private GameObject m_WindowScaleObj;
        [SerializeField] private Text m_TitleText;
        [SerializeField] private GameObject m_TitleBarObj;
        [SerializeField] private Text m_MainText;
        [SerializeField] private Text m_SubText;
        [SerializeField] private GameObject m_ButtonParent;
        [SerializeField] private GameObject m_ButtonOK;
        [SerializeField] private GameObject m_ButtonYes;
        [SerializeField] private GameObject m_ButtonNo;
        [SerializeField] private InvisibleGraphic m_InvisibleGraphic;
        [SerializeField] private CanvasGroup m_CanvasGroup;
        [SerializeField] private AnimUIPlayer m_BGAnim;
        [SerializeField] private GameObject m_BGSystemUIObj;
        [SerializeField] private GameObject m_BGFullUIObj;

        private RectTransform m_WindowScaleRectTransform;
        private GameObject m_MainTextObj;
        private GameObject m_SubTextObj;
        private bool m_FullScreenFilterFlag;
        private bool m_IsActive;
        private eType m_Type;
        private int m_SelectedAnswerIndex;
        private List<Param> m_List = new List<Param>();
        private CustomButton m_TargetBackButton;
        private GameObject m_GameObject;

        private Action<int> OnClosedWindow;

        public List<Param> WindowList => m_List;
        public bool IsActive => m_IsActive;

        public bool FullScreenFilterFlag {
            get => m_FullScreenFilterFlag;
            set {
                m_FullScreenFilterFlag = value;
                if (m_BGSystemUIObj != null && m_BGSystemUIObj.activeSelf == value) {
                    m_BGSystemUIObj.SetActive(!value);
                }
                if (m_BGFullUIObj != null && m_BGFullUIObj.activeSelf != value) {
                    m_BGFullUIObj.SetActive(value);
                }
            }
        }

        private void Update() {
            UpdateAndroidBackKey();
        }

        private void UpdateAndroidBackKey() {
            UIUtility.UpdateAndroidBackKey(m_TargetBackButton, CheckUpdateAndroidBackKey);
        }

        private bool CheckUpdateAndroidBackKey() {
            return m_IsActive && !IsPlayingAnim();
        }

        public void Setup() {
            m_GameObject = gameObject;
            m_Body.SetActive(false);
            m_WindowScaleRectTransform = m_WindowScaleObj.GetComponent<RectTransform>();
            m_InvisibleGraphic.raycastTarget = false;
            m_MainTextObj = m_MainText.gameObject;
            m_SubTextObj = m_SubText.gameObject;
            m_FullScreenFilterFlag = false;
        }

        public void ChangeYesButtonText(string text) {
            m_ButtonYes.GetComponentInChildren<Text>().text = text;
        }

        public void ChangeNoButtonText(string text) {
            m_ButtonNo.GetComponentInChildren<Text>().text = text;
        }

        public void ChangeOKButtonText(string text) {
            m_ButtonOK.GetComponentInChildren<Text>().text = text;
        }

        public void Open(eType type, string titleText, string text, string subText, float delay, Action<int> onClosedWindowFunc = null) {
            OpenCommonConstruct(type, titleText, text, subText, delay, onClosedWindowFunc);
        }

        public void Open(eType type, string titleText, string text, string subText, Action<int> onClosedWindowFunc = null, ResultCode? resultCode = null) {
            OpenCommonConstruct(type, titleText, text, subText, 0f, onClosedWindowFunc, resultCode);
        }

        public void Open(eType type, string text, float delay, Action<int> onClosedWindowFunc = null) {
            OpenCommonConstruct(type, string.Empty, text, string.Empty, delay, onClosedWindowFunc);
        }

        public void Open(eType type, string text, Action<int> onClosedWindowFunc = null) {
            OpenCommonConstruct(type, string.Empty, text, string.Empty, 0f, onClosedWindowFunc);
        }

        public void Open(eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, float delay, Action<int> onClosedWindowFunc = null) {
            if (GameSystem.Inst == null || !GameSystem.Inst.IsAvailable()) {
                Open(type, string.Empty, string.Empty, string.Empty, onClosedWindowFunc);
            } else {
                string titleText = GameSystem.Inst.DbMng.GetTextMessage(titleTextDBIndex);
                string text = GameSystem.Inst.DbMng.GetTextMessage(textDBIndex);
                Open(type, titleText, text, null, onClosedWindowFunc);
            }
        }

        public void Open(eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, Action<int> onClosedWindowFunc = null) {
            Open(type, titleTextDBIndex, textDBIndex, 0f, onClosedWindowFunc);
        }

        public void Open(eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, eText_MessageDB subTextDBIndex, float delay, Action<int> onClosedWindowFunc = null) {
            if (GameSystem.Inst == null || !GameSystem.Inst.IsAvailable()) {
                Open(type, string.Empty, string.Empty, string.Empty, onClosedWindowFunc);
            } else {
                string titleText = GameSystem.Inst.DbMng.GetTextMessage(titleTextDBIndex);
                string text = GameSystem.Inst.DbMng.GetTextMessage(textDBIndex);
                string subText = GameSystem.Inst.DbMng.GetTextMessage(subTextDBIndex);
                Open(type, titleText, text, subText, onClosedWindowFunc);
            }
        }

        public void Open(eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, eText_MessageDB subTextDBIndex, Action<int> onClosedWindowFunc = null) {
            Open(type, titleTextDBIndex, textDBIndex, subTextDBIndex, 0f, onClosedWindowFunc);
        }

        public void Open(eType type, eText_MessageDB textDBIndex, float delay, Action<int> onClosedWindowFunc = null) {
            if (GameSystem.Inst == null || !GameSystem.Inst.IsAvailable()) {
                Open(type, string.Empty, onClosedWindowFunc);
            } else {
                Open(type, GameSystem.Inst.DbMng.GetTextMessage(textDBIndex), onClosedWindowFunc);
            }
        }

        public void Open(eType type, eText_MessageDB textDBIndex, Action<int> onClosedWindowFunc = null) {
            Open(type, textDBIndex, 0f, onClosedWindowFunc);
        }

        private void OpenCommonConstruct(eType type, string titleText, string text, string subText, float delay, Action<int> onClosedWindowFunc, ResultCode? resultCode = null) {
            m_List.Add(new Param {
                m_Type = type,
                m_TitleText = titleText,
                m_Text = text,
                m_SubText = subText,
                m_Delay = delay,
                OnClosedWindowFunc = onClosedWindowFunc,
                m_ResultCode = resultCode
            });
            if (m_List.Count == 1) {
                OpenFromStack();
            }
            m_InvisibleGraphic.raycastTarget = true;
        }

        private void OpenFromStack() {
            if (m_List.Count <= 0) {
                m_InvisibleGraphic.raycastTarget = false;
                return;
            }
            Param param = m_List[0];
            m_Type = param.m_Type;
            if (string.IsNullOrEmpty(param.m_TitleText)) {
                m_TitleBarObj.SetActive(false);
            } else {
                m_TitleBarObj.SetActive(true);
                m_TitleText.text = param.m_TitleText;
            }
            if (string.IsNullOrEmpty(param.m_Text)) {
                m_MainTextObj.SetActive(false);
            } else {
                m_MainTextObj.SetActive(true);
                m_MainText.text = param.m_Text;
            }
            if (string.IsNullOrEmpty(param.m_SubText)) {
                m_SubTextObj.SetActive(false);
            } else {
                m_SubTextObj.SetActive(true);
                m_SubText.text = param.m_SubText;
            }
            OnClosedWindow = param.OnClosedWindowFunc;
            m_TargetBackButton = null;
            switch (m_Type) {
                case eType.None:
                    m_ButtonOK.SetActive(false);
                    m_ButtonYes.SetActive(false);
                    m_ButtonNo.SetActive(false);
                    break;
                case eType.OK:
                case eType.OK_NoClose:
                    m_ButtonOK.SetActive(true);
                    m_ButtonYes.SetActive(false);
                    m_ButtonNo.SetActive(false);
                    m_TargetBackButton = m_ButtonOK.GetComponent<CustomButton>();
                    break;
                case eType.YES_NO:
                    m_ButtonOK.SetActive(false);
                    m_ButtonYes.SetActive(true);
                    m_ButtonNo.SetActive(true);
                    m_TargetBackButton = m_ButtonNo.GetComponent<CustomButton>();
                    break;
                case eType.Retry:
                    m_ButtonOK.SetActive(true);
                    m_ButtonYes.SetActive(false);
                    m_ButtonNo.SetActive(false);
                    break;
            }
            m_WindowScaleRectTransform.localScale = new Vector3(0.5f, 0.5f, 1f);
            m_CanvasGroup.alpha = 0f;
            iTween.Stop(m_WindowScaleObj);
            iTween.ScaleTo(m_WindowScaleObj, new Hashtable {
                { "x", 1f },
                { "y", 1f },
                { "time", m_AnimDuration },
                { "easeType", iTween.EaseType.linear },
                { "delay", param.m_Delay },
                { "oncomplete", "OnCompleteOpen" },
                { "oncompletetarget", m_GameObject }
            });
            iTween.ValueTo(m_WindowScaleObj, new Hashtable {
                { "from", 0f },
                { "to", 1f },
                { "time", m_AnimDuration },
                { "easeType", iTween.EaseType.linear },
                { "delay", param.m_Delay },
                { "onupdate", "OnUpdateAlpha" },
                { "onupdatetarget", m_GameObject }
            });
            m_BGAnim.PlayIn();
            GameSystem.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_WINDOW_OPEN);
            m_ButtonParent.SetActive(m_Type != eType.None);
            m_InvisibleGraphic.raycastTarget = true;
            SetEnableInput(false);
            m_IsActive = true;
        }

        public void Close() {
            m_WindowScaleRectTransform.localScale = new Vector3(1f, 1f, 1f);
            m_CanvasGroup.alpha = 1f;
            iTween.Stop(m_WindowScaleObj);
            iTween.ScaleTo(m_WindowScaleObj, new Hashtable {
                { "x", 0f },
                { "y", 0f },
                { "time", m_AnimDuration },
                { "easeType", iTween.EaseType.linear },
                { "oncomplete", "OnCompleteClose" },
                { "oncompletetarget", m_GameObject }
            });
            iTween.ValueTo(m_WindowScaleObj, new Hashtable {
                { "from", 1f },
                { "to", 0f },
                { "time", m_AnimDuration },
                { "easeType", iTween.EaseType.linear },
                { "onupdate", "OnUpdateAlpha" },
                { "onupdatetarget", m_GameObject }
            });
            m_BGAnim.PlayOut();
            GameSystem.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_WINDOW_CLOSE);
            SetEnableInput(false);
        }

        public bool IsPlayingAnim() {
            return !m_BGAnim.IsEnd;
        }

        public void SetEnableInput(bool flg) {
            m_CanvasGroup.blocksRaycasts = flg;
        }

        private void OnCompleteOpen() {
            SetEnableInput(true);
        }

        private void OnCompleteClose() {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            if (dbMng.TextCmnDB != null) {
                ChangeYesButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonYES));
                ChangeNoButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonNo));
                ChangeOKButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonOK));
            } else {
                ChangeYesButtonText("はい");
                ChangeNoButtonText("いいえ");
                ChangeOKButtonText("ＯＫ");
            }
            OnClosedWindow.Call(m_SelectedAnswerIndex);
            OnClosedWindow = null;
            m_InvisibleGraphic.raycastTarget = false;
            iTween.Stop(m_GameObject);
            m_Body.SetActive(false);
            m_IsActive = false;
            m_List.RemoveAt(0);
            OpenFromStack();
        }

        public void OnClickButtonOK() {
            m_SelectedAnswerIndex = ANSWER_YES;
            if (m_Type == eType.OK_NoClose) {
                OnClosedWindow.Call(m_SelectedAnswerIndex);
            } else {
                Close();
            }
        }

        public void OnClickButtonYes() {
            m_SelectedAnswerIndex = ANSWER_YES;
            Close();
        }

        public void OnClickButtonNo() {
            m_SelectedAnswerIndex = ANSWER_NO;
            Close();
        }

        public void OnUpdateAlpha(float value) {
            m_CanvasGroup.alpha = value;
        }

        public enum eType {
            None,
            OK,
            YES_NO,
            Retry,
            OK_NoClose
        }

        public class Param {
            public eType m_Type;
            public string m_TitleText;
            public string m_Text;
            public string m_SubText;
            public float m_Delay;
            public Action<int> OnClosedWindowFunc;
            public ResultCode? m_ResultCode;
        }
    }
}
