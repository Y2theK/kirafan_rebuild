﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C4A RID: 3146
	[Token(Token = "0x200086C")]
	[StructLayout(3)]
	public class ConfirmChangeAchvWindow : UIGroup
	{
		// Token: 0x060038DD RID: 14557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C7")]
		[Address(RVA = "0x101436E90", Offset = "0x1436E90", VA = "0x101436E90")]
		private void Awake()
		{
		}

		// Token: 0x060038DE RID: 14558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C8")]
		[Address(RVA = "0x101436F84", Offset = "0x1436F84", VA = "0x101436F84")]
		public void Setup(int achvID)
		{
		}

		// Token: 0x060038DF RID: 14559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033C9")]
		[Address(RVA = "0x101437170", Offset = "0x1437170", VA = "0x101437170")]
		public void OnClickYesBtn()
		{
		}

		// Token: 0x060038E0 RID: 14560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033CA")]
		[Address(RVA = "0x101437234", Offset = "0x1437234", VA = "0x101437234")]
		public void OnClickNoBtn()
		{
		}

		// Token: 0x060038E1 RID: 14561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033CB")]
		[Address(RVA = "0x101437240", Offset = "0x1437240", VA = "0x101437240")]
		public void OnResponceAchvChange()
		{
		}

		// Token: 0x060038E2 RID: 14562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033CC")]
		[Address(RVA = "0x1014372B4", Offset = "0x14372B4", VA = "0x1014372B4")]
		public ConfirmChangeAchvWindow()
		{
		}

		// Token: 0x04004821 RID: 18465
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40032A0")]
		[SerializeField]
		private AchievementImage m_imgAchvBefore;

		// Token: 0x04004822 RID: 18466
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40032A1")]
		[SerializeField]
		private AchievementImage m_imgAchvAfter;

		// Token: 0x04004823 RID: 18467
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40032A2")]
		[SerializeField]
		private Text m_txtAchvInfoBefore;

		// Token: 0x04004824 RID: 18468
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40032A3")]
		[SerializeField]
		private Text m_txtAchvInfoAfter;

		// Token: 0x04004825 RID: 18469
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40032A4")]
		[SerializeField]
		private CustomButton m_btnYes;

		// Token: 0x04004826 RID: 18470
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40032A5")]
		[SerializeField]
		private CustomButton m_btnNo;

		// Token: 0x04004827 RID: 18471
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40032A6")]
		private int m_chgAchvID;

		// Token: 0x04004828 RID: 18472
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40032A7")]
		public Action<int> m_AchvChangeCallback;
	}
}
