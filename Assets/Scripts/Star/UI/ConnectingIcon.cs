﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D62 RID: 3426
	[Token(Token = "0x200092C")]
	[StructLayout(3)]
	public class ConnectingIcon : MonoBehaviour
	{
		// Token: 0x06003EF1 RID: 16113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A0")]
		[Address(RVA = "0x1014372BC", Offset = "0x14372BC", VA = "0x1014372BC")]
		private void Start()
		{
		}

		// Token: 0x06003EF2 RID: 16114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A1")]
		[Address(RVA = "0x1014372C0", Offset = "0x14372C0", VA = "0x1014372C0")]
		public void Setup()
		{
		}

		// Token: 0x06003EF3 RID: 16115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A2")]
		[Address(RVA = "0x101437318", Offset = "0x1437318", VA = "0x101437318")]
		private void SetupIconChild()
		{
		}

		// Token: 0x06003EF4 RID: 16116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A3")]
		[Address(RVA = "0x101437698", Offset = "0x1437698", VA = "0x101437698")]
		public void Open()
		{
		}

		// Token: 0x06003EF5 RID: 16117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A4")]
		[Address(RVA = "0x101437714", Offset = "0x1437714", VA = "0x101437714")]
		private void Update()
		{
		}

		// Token: 0x06003EF6 RID: 16118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A5")]
		[Address(RVA = "0x101437AB4", Offset = "0x1437AB4", VA = "0x101437AB4")]
		public void Close()
		{
		}

		// Token: 0x06003EF7 RID: 16119 RVA: 0x00018C48 File Offset: 0x00016E48
		[Token(Token = "0x60039A6")]
		[Address(RVA = "0x101437ABC", Offset = "0x1437ABC", VA = "0x101437ABC")]
		public bool IsAbleClose()
		{
			return default(bool);
		}

		// Token: 0x06003EF8 RID: 16120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A7")]
		[Address(RVA = "0x101437B0C", Offset = "0x1437B0C", VA = "0x101437B0C")]
		public ConnectingIcon()
		{
		}

		// Token: 0x04004E31 RID: 20017
		[Token(Token = "0x40036FD")]
		private const float m_WaitForSecond = 0.1f;

		// Token: 0x04004E32 RID: 20018
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036FE")]
		[SerializeField]
		private GameObject m_InvisibleGraphic;

		// Token: 0x04004E33 RID: 20019
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036FF")]
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x04004E34 RID: 20020
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003700")]
		[SerializeField]
		private RectTransform[] m_ActionTarget;

		// Token: 0x04004E35 RID: 20021
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003701")]
		[SerializeField]
		private float m_ActionTargetRotateZParSecond;

		// Token: 0x04004E36 RID: 20022
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003702")]
		[SerializeField]
		private ConnectingIconChild[] m_ChildImages;

		// Token: 0x04004E37 RID: 20023
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003703")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012583C", Offset = "0x12583C")]
		private ConnectingIcon.ProgressState[] m_ProgressStates;

		// Token: 0x04004E38 RID: 20024
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003704")]
		private ConnectingIcon.ProgressState[] m_perfectProgressStates;

		// Token: 0x04004E39 RID: 20025
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003705")]
		private bool m_IsOpen;

		// Token: 0x04004E3A RID: 20026
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003706")]
		private float m_Count;

		// Token: 0x04004E3B RID: 20027
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003707")]
		private float m_Wait;

		// Token: 0x04004E3C RID: 20028
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003708")]
		private float m_OpenStartTime;

		// Token: 0x04004E3D RID: 20029
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003709")]
		[SerializeField]
		private float m_OpenFrameMin;

		// Token: 0x04004E3E RID: 20030
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400370A")]
		[SerializeField]
		private float m_FrameParSecond;

		// Token: 0x02000D63 RID: 3427
		[Token(Token = "0x20010FC")]
		[Serializable]
		public class ProgressState
		{
			// Token: 0x06003EF9 RID: 16121 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006204")]
			[Address(RVA = "0x101437B28", Offset = "0x1437B28", VA = "0x101437B28")]
			public ProgressState(Color in_color, Vector3 in_scale)
			{
			}

			// Token: 0x06003EFA RID: 16122 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006205")]
			[Address(RVA = "0x101437BFC", Offset = "0x1437BFC", VA = "0x101437BFC")]
			public ProgressState()
			{
			}

			// Token: 0x04004E3F RID: 20031
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A7D")]
			public Color color;

			// Token: 0x04004E40 RID: 20032
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006A7E")]
			public Vector3 scale;
		}
	}
}
