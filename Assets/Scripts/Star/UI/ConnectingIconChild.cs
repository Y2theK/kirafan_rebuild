﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D64 RID: 3428
	[Token(Token = "0x200092D")]
	[StructLayout(3)]
	public class ConnectingIconChild : MonoBehaviour
	{
		// Token: 0x06003EFB RID: 16123 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60039A8")]
		[Address(RVA = "0x10143757C", Offset = "0x143757C", VA = "0x10143757C")]
		public ConnectingIcon.ProgressState GetHaveProgressState()
		{
			return null;
		}

		// Token: 0x06003EFC RID: 16124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039A9")]
		[Address(RVA = "0x101437688", Offset = "0x1437688", VA = "0x101437688")]
		public void Setup(ConnectingIcon.ProgressState[] progressStates)
		{
		}

		// Token: 0x06003EFD RID: 16125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AA")]
		[Address(RVA = "0x101437A68", Offset = "0x1437A68", VA = "0x101437A68")]
		public void Advance()
		{
		}

		// Token: 0x06003EFE RID: 16126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AB")]
		[Address(RVA = "0x101437C84", Offset = "0x1437C84", VA = "0x101437C84")]
		public void Apply()
		{
		}

		// Token: 0x06003EFF RID: 16127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AC")]
		[Address(RVA = "0x101437690", Offset = "0x1437690", VA = "0x101437690")]
		public void SetProgress(int progress)
		{
		}

		// Token: 0x06003F00 RID: 16128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AD")]
		[Address(RVA = "0x101437DC0", Offset = "0x1437DC0", VA = "0x101437DC0")]
		public ConnectingIconChild()
		{
		}

		// Token: 0x04004E41 RID: 20033
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400370B")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04004E42 RID: 20034
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400370C")]
		private ConnectingIcon.ProgressState[] m_ProgressStates;

		// Token: 0x04004E43 RID: 20035
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400370D")]
		private int m_Progress;
	}
}
