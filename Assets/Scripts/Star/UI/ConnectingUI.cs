﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D65 RID: 3429
	[Token(Token = "0x200092E")]
	[StructLayout(3)]
	public class ConnectingUI : MonoBehaviour
	{
		// Token: 0x06003F01 RID: 16129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AE")]
		[Address(RVA = "0x101437DC8", Offset = "0x1437DC8", VA = "0x101437DC8")]
		private void Start()
		{
		}

		// Token: 0x06003F02 RID: 16130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039AF")]
		[Address(RVA = "0x101437E08", Offset = "0x1437E08", VA = "0x101437E08")]
		public void OnDestroy()
		{
		}

		// Token: 0x06003F03 RID: 16131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B0")]
		[Address(RVA = "0x101437E20", Offset = "0x1437E20", VA = "0x101437E20")]
		public void Destroy()
		{
		}

		// Token: 0x06003F04 RID: 16132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B1")]
		[Address(RVA = "0x101437E2C", Offset = "0x1437E2C", VA = "0x101437E2C")]
		public void Abort()
		{
		}

		// Token: 0x06003F05 RID: 16133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B2")]
		[Address(RVA = "0x10142DC18", Offset = "0x142DC18", VA = "0x10142DC18")]
		public void Open()
		{
		}

		// Token: 0x06003F06 RID: 16134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B3")]
		[Address(RVA = "0x101437E74", Offset = "0x1437E74", VA = "0x101437E74")]
		public void Close()
		{
		}

		// Token: 0x06003F07 RID: 16135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B4")]
		[Address(RVA = "0x101437E7C", Offset = "0x1437E7C", VA = "0x101437E7C")]
		private void Update()
		{
		}

		// Token: 0x06003F08 RID: 16136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B5")]
		[Address(RVA = "0x101437EA4", Offset = "0x1437EA4", VA = "0x101437EA4")]
		private void UpdateIn()
		{
		}

		// Token: 0x06003F09 RID: 16137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B6")]
		[Address(RVA = "0x101437EE8", Offset = "0x1437EE8", VA = "0x101437EE8")]
		private void UpdateIdle()
		{
		}

		// Token: 0x06003F0A RID: 16138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B7")]
		[Address(RVA = "0x101437F78", Offset = "0x1437F78", VA = "0x101437F78")]
		private void UpdateOut()
		{
		}

		// Token: 0x06003F0B RID: 16139 RVA: 0x00018C60 File Offset: 0x00016E60
		[Token(Token = "0x60039B8")]
		[Address(RVA = "0x101437FD0", Offset = "0x1437FD0", VA = "0x101437FD0")]
		private bool IsOpenConnecting()
		{
			return default(bool);
		}

		// Token: 0x06003F0C RID: 16140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039B9")]
		[Address(RVA = "0x10143805C", Offset = "0x143805C", VA = "0x10143805C")]
		public ConnectingUI()
		{
		}

		// Token: 0x04004E44 RID: 20036
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400370E")]
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x04004E45 RID: 20037
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400370F")]
		[SerializeField]
		private ConnectingIcon m_ConnectingIcon;

		// Token: 0x04004E46 RID: 20038
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003710")]
		private bool m_IsOpen;

		// Token: 0x04004E47 RID: 20039
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003711")]
		private bool m_Destroyed;

		// Token: 0x04004E48 RID: 20040
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003712")]
		private ConnectingUI.eStep m_Step;

		// Token: 0x02000D66 RID: 3430
		[Token(Token = "0x20010FD")]
		public enum eStep
		{
			// Token: 0x04004E4A RID: 20042
			[Token(Token = "0x4006A80")]
			None = -1,
			// Token: 0x04004E4B RID: 20043
			[Token(Token = "0x4006A81")]
			In,
			// Token: 0x04004E4C RID: 20044
			[Token(Token = "0x4006A82")]
			Idle,
			// Token: 0x04004E4D RID: 20045
			[Token(Token = "0x4006A83")]
			Out
		}
	}
}
