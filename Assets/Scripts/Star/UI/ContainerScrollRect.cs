﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C96 RID: 3222
	[Token(Token = "0x200089E")]
	[StructLayout(3)]
	public class ContainerScrollRect : FixScrollRect
	{
		// Token: 0x06003A94 RID: 14996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003557")]
		[Address(RVA = "0x10143806C", Offset = "0x143806C", VA = "0x10143806C", Slot = "7")]
		protected override void LateUpdate()
		{
		}

		// Token: 0x06003A95 RID: 14997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003558")]
		[Address(RVA = "0x101438310", Offset = "0x1438310", VA = "0x101438310")]
		public void Destroy()
		{
		}

		// Token: 0x06003A96 RID: 14998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003559")]
		[Address(RVA = "0x1014384D8", Offset = "0x14384D8", VA = "0x1014384D8")]
		public void KeepScrollNormalizePosition(bool flag)
		{
		}

		// Token: 0x06003A97 RID: 14999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600355A")]
		[Address(RVA = "0x1014384E0", Offset = "0x14384E0", VA = "0x1014384E0")]
		public void AddItem(GameObject item)
		{
		}

		// Token: 0x06003A98 RID: 15000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600355B")]
		[Address(RVA = "0x1014380AC", Offset = "0x14380AC", VA = "0x1014380AC")]
		private void Validate()
		{
		}

		// Token: 0x06003A99 RID: 15001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600355C")]
		[Address(RVA = "0x10143877C", Offset = "0x143877C", VA = "0x10143877C")]
		public ContainerScrollRect()
		{
		}

		// Token: 0x04004965 RID: 18789
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400338E")]
		[SerializeField]
		private Transform m_Inner;

		// Token: 0x04004966 RID: 18790
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400338F")]
		private List<GameObject> m_ItemList;

		// Token: 0x04004967 RID: 18791
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003390")]
		private bool m_AddItem;

		// Token: 0x04004968 RID: 18792
		[Cpp2IlInjected.FieldOffset(Offset = "0x51")]
		[Token(Token = "0x4003391")]
		private bool m_Destroy;

		// Token: 0x04004969 RID: 18793
		[Cpp2IlInjected.FieldOffset(Offset = "0x52")]
		[Token(Token = "0x4003392")]
		private bool m_KeepScrollPosition;
	}
}
