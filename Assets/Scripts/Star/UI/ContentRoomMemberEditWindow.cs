﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D67 RID: 3431
	[Token(Token = "0x200092F")]
	[StructLayout(3)]
	public class ContentRoomMemberEditWindow : UIGroup
	{
		// Token: 0x06003F0D RID: 16141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BA")]
		[Address(RVA = "0x1014387EC", Offset = "0x14387EC", VA = "0x1014387EC")]
		private void Awake()
		{
		}

		// Token: 0x06003F0E RID: 16142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BB")]
		[Address(RVA = "0x1014388C0", Offset = "0x14388C0", VA = "0x1014388C0")]
		public void SetData(ContentRoomUI ownerUI, eTitleType titleType, long[] memberMngIDArray, Action<List<long>> callback)
		{
		}

		// Token: 0x06003F0F RID: 16143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BC")]
		[Address(RVA = "0x101438C44", Offset = "0x1438C44", VA = "0x101438C44")]
		private void OnClickIcon(int iconIndex, long mngID)
		{
		}

		// Token: 0x06003F10 RID: 16144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BD")]
		[Address(RVA = "0x101438F14", Offset = "0x1438F14", VA = "0x101438F14")]
		private void OnClickChara(long mngID)
		{
		}

		// Token: 0x06003F11 RID: 16145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BE")]
		[Address(RVA = "0x101438FA0", Offset = "0x1438FA0", VA = "0x101438FA0")]
		private void OnClickRemove()
		{
		}

		// Token: 0x06003F12 RID: 16146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039BF")]
		[Address(RVA = "0x10143902C", Offset = "0x143902C", VA = "0x10143902C")]
		private void CharaListOnClose()
		{
		}

		// Token: 0x06003F13 RID: 16147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C0")]
		[Address(RVA = "0x101439148", Offset = "0x1439148", VA = "0x101439148")]
		private void CharaListOnEnd()
		{
		}

		// Token: 0x06003F14 RID: 16148 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60039C1")]
		[Address(RVA = "0x101438E1C", Offset = "0x1438E1C", VA = "0x101438E1C")]
		private List<long> GetNowCharaMngIDList()
		{
			return null;
		}

		// Token: 0x06003F15 RID: 16149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C2")]
		[Address(RVA = "0x101439354", Offset = "0x1439354", VA = "0x101439354")]
		private void OnClickDecide()
		{
		}

		// Token: 0x06003F16 RID: 16150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C3")]
		[Address(RVA = "0x101439474", Offset = "0x1439474", VA = "0x101439474")]
		private void OnClickCancel()
		{
		}

		// Token: 0x06003F17 RID: 16151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C4")]
		[Address(RVA = "0x1014394CC", Offset = "0x14394CC", VA = "0x1014394CC")]
		public ContentRoomMemberEditWindow()
		{
		}

		// Token: 0x04004E4E RID: 20046
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003713")]
		[SerializeField]
		private ContentRoomMemberIcon[] m_icon;

		// Token: 0x04004E4F RID: 20047
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003714")]
		[SerializeField]
		private CustomButton m_btnDecide;

		// Token: 0x04004E50 RID: 20048
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003715")]
		[SerializeField]
		private CustomButton m_btnCancel;

		// Token: 0x04004E51 RID: 20049
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003716")]
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x04004E52 RID: 20050
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003717")]
		private ContentRoomUI m_OwnerUI;

		// Token: 0x04004E53 RID: 20051
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003718")]
		private int m_SelectedIconIndex;

		// Token: 0x04004E54 RID: 20052
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003719")]
		private Action<List<long>> m_CBPartyDecide;

		// Token: 0x04004E55 RID: 20053
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400371A")]
		private eTitleType m_titleType;
	}
}
