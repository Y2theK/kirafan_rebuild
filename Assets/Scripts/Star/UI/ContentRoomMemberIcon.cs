﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D68 RID: 3432
	[Token(Token = "0x2000930")]
	[StructLayout(3)]
	public class ContentRoomMemberIcon : MonoBehaviour
	{
		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x06003F18 RID: 16152 RVA: 0x00018C78 File Offset: 0x00016E78
		[Token(Token = "0x17000476")]
		public long CharaMngID
		{
			[Token(Token = "0x60039C5")]
			[Address(RVA = "0x10143934C", Offset = "0x143934C", VA = "0x10143934C")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06003F19 RID: 16153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C6")]
		[Address(RVA = "0x101438B1C", Offset = "0x1438B1C", VA = "0x101438B1C")]
		public void SetData(int index, long charaMngID)
		{
		}

		// Token: 0x06003F1A RID: 16154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C7")]
		[Address(RVA = "0x101438B2C", Offset = "0x1438B2C", VA = "0x101438B2C")]
		public void SetBtnCallback(Action<int, long> callback)
		{
		}

		// Token: 0x06003F1B RID: 16155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C8")]
		[Address(RVA = "0x1014397C8", Offset = "0x14397C8", VA = "0x1014397C8")]
		public void RemoveBtnCallback()
		{
		}

		// Token: 0x06003F1C RID: 16156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039C9")]
		[Address(RVA = "0x10143943C", Offset = "0x143943C", VA = "0x10143943C")]
		public void SetEnableInput(bool flg)
		{
		}

		// Token: 0x06003F1D RID: 16157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039CA")]
		[Address(RVA = "0x1014394DC", Offset = "0x14394DC", VA = "0x1014394DC")]
		public void UpdateCharaIcon(long charaMngID)
		{
		}

		// Token: 0x06003F1E RID: 16158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039CB")]
		[Address(RVA = "0x101439884", Offset = "0x1439884", VA = "0x101439884")]
		public void OnClickPanel()
		{
		}

		// Token: 0x06003F1F RID: 16159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039CC")]
		[Address(RVA = "0x1014398EC", Offset = "0x14398EC", VA = "0x1014398EC")]
		public ContentRoomMemberIcon()
		{
		}

		// Token: 0x04004E56 RID: 20054
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400371B")]
		[SerializeField]
		private PrefabCloner m_CharaIcon;

		// Token: 0x04004E57 RID: 20055
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400371C")]
		[SerializeField]
		private Image m_imgEmptyIcon;

		// Token: 0x04004E58 RID: 20056
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400371D")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04004E59 RID: 20057
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400371E")]
		private int m_Index;

		// Token: 0x04004E5A RID: 20058
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400371F")]
		private long m_CharaMngID;

		// Token: 0x04004E5B RID: 20059
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003720")]
		private Action<int, long> m_callbackBtn;
	}
}
