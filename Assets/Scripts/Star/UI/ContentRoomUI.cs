﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D69 RID: 3433
	[Token(Token = "0x2000931")]
	[StructLayout(3)]
	public class ContentRoomUI : MenuUIBase
	{
		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x06003F20 RID: 16160 RVA: 0x00018C90 File Offset: 0x00016E90
		// (set) Token: 0x06003F21 RID: 16161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000477")]
		public bool RunningUI
		{
			[Token(Token = "0x60039CD")]
			[Address(RVA = "0x1014398FC", Offset = "0x14398FC", VA = "0x1014398FC")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60039CE")]
			[Address(RVA = "0x1014394C4", Offset = "0x14394C4", VA = "0x1014394C4")]
			set
			{
			}
		}

		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x06003F22 RID: 16162 RVA: 0x00018CA8 File Offset: 0x00016EA8
		[Token(Token = "0x17000478")]
		public int TransitQuestID
		{
			[Token(Token = "0x60039CF")]
			[Address(RVA = "0x101439904", Offset = "0x1439904", VA = "0x101439904")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06003F23 RID: 16163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D0")]
		[Address(RVA = "0x10143990C", Offset = "0x143990C", VA = "0x10143990C")]
		public void Setup(ContentRoomState_Main ownerState, string bgName, eTitleType titleType)
		{
		}

		// Token: 0x06003F24 RID: 16164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D1")]
		[Address(RVA = "0x10143998C", Offset = "0x143998C", VA = "0x10143998C", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06003F25 RID: 16165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D2")]
		[Address(RVA = "0x101439B1C", Offset = "0x1439B1C", VA = "0x101439B1C")]
		private void Update()
		{
		}

		// Token: 0x06003F26 RID: 16166 RVA: 0x00018CC0 File Offset: 0x00016EC0
		[Token(Token = "0x60039D3")]
		[Address(RVA = "0x101439DCC", Offset = "0x1439DCC", VA = "0x101439DCC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06003F27 RID: 16167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D4")]
		[Address(RVA = "0x101439DDC", Offset = "0x1439DDC", VA = "0x101439DDC", Slot = "9")]
		public override void SetEnableInput(bool isEnable)
		{
		}

		// Token: 0x06003F28 RID: 16168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D5")]
		[Address(RVA = "0x101439E98", Offset = "0x1439E98", VA = "0x101439E98", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06003F29 RID: 16169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D6")]
		[Address(RVA = "0x101439F58", Offset = "0x1439F58", VA = "0x101439F58", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06003F2A RID: 16170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D7")]
		[Address(RVA = "0x10143A018", Offset = "0x143A018", VA = "0x10143A018")]
		public void StartHideUI()
		{
		}

		// Token: 0x06003F2B RID: 16171 RVA: 0x00018CD8 File Offset: 0x00016ED8
		[Token(Token = "0x60039D8")]
		[Address(RVA = "0x10143A0B4", Offset = "0x143A0B4", VA = "0x10143A0B4")]
		public bool IsCompleteHideUI()
		{
			return default(bool);
		}

		// Token: 0x06003F2C RID: 16172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039D9")]
		[Address(RVA = "0x10143A15C", Offset = "0x143A15C", VA = "0x10143A15C")]
		public void StartShowUI()
		{
		}

		// Token: 0x06003F2D RID: 16173 RVA: 0x00018CF0 File Offset: 0x00016EF0
		[Token(Token = "0x60039DA")]
		[Address(RVA = "0x10143A1F8", Offset = "0x143A1F8", VA = "0x10143A1F8")]
		public bool IsCompleteShowUI()
		{
			return default(bool);
		}

		// Token: 0x06003F2E RID: 16174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039DB")]
		[Address(RVA = "0x10143A2A0", Offset = "0x143A2A0", VA = "0x10143A2A0")]
		public void SetContentRoomName(string contentRoomName)
		{
		}

		// Token: 0x06003F2F RID: 16175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039DC")]
		[Address(RVA = "0x10143A2E0", Offset = "0x143A2E0", VA = "0x10143A2E0")]
		public void SetAttentionBadge(bool isActive, int subOfferBadgeCount)
		{
		}

		// Token: 0x06003F30 RID: 16176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039DD")]
		[Address(RVA = "0x10143A348", Offset = "0x143A348", VA = "0x10143A348")]
		public void OpenMainOfferRewardWindow(long mngId, bool requestTips)
		{
		}

		// Token: 0x06003F31 RID: 16177 RVA: 0x00018D08 File Offset: 0x00016F08
		[Token(Token = "0x60039DE")]
		[Address(RVA = "0x10143A448", Offset = "0x143A448", VA = "0x10143A448")]
		public bool IsOpenMainOfferRewardWindow()
		{
			return default(bool);
		}

		// Token: 0x06003F32 RID: 16178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039DF")]
		[Address(RVA = "0x10143A478", Offset = "0x143A478", VA = "0x10143A478")]
		public void OpenSubOfferWindow()
		{
		}

		// Token: 0x06003F33 RID: 16179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E0")]
		[Address(RVA = "0x10143A50C", Offset = "0x143A50C", VA = "0x10143A50C")]
		public void CloseSubOfferWindow()
		{
		}

		// Token: 0x06003F34 RID: 16180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E1")]
		[Address(RVA = "0x10143A540", Offset = "0x143A540", VA = "0x10143A540")]
		public void ApplySubOfferWindow(int characterID)
		{
		}

		// Token: 0x06003F35 RID: 16181 RVA: 0x00018D20 File Offset: 0x00016F20
		[Token(Token = "0x60039E2")]
		[Address(RVA = "0x10143A5BC", Offset = "0x143A5BC", VA = "0x10143A5BC")]
		public bool IsOpenSubOfferWindow()
		{
			return default(bool);
		}

		// Token: 0x06003F36 RID: 16182 RVA: 0x00018D38 File Offset: 0x00016F38
		[Token(Token = "0x60039E3")]
		[Address(RVA = "0x10143A5EC", Offset = "0x143A5EC", VA = "0x10143A5EC")]
		public bool IsCloseSubOfferWindow()
		{
			return default(bool);
		}

		// Token: 0x06003F37 RID: 16183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E4")]
		[Address(RVA = "0x10143A61C", Offset = "0x143A61C", VA = "0x10143A61C")]
		public void OpenSubOfferDetailWindow(OfferManager.OfferData offerData)
		{
		}

		// Token: 0x06003F38 RID: 16184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E5")]
		[Address(RVA = "0x10143A6DC", Offset = "0x143A6DC", VA = "0x10143A6DC")]
		public void CloseSubOfferDetailWindow()
		{
		}

		// Token: 0x06003F39 RID: 16185 RVA: 0x00018D50 File Offset: 0x00016F50
		[Token(Token = "0x60039E6")]
		[Address(RVA = "0x10143A710", Offset = "0x143A710", VA = "0x10143A710")]
		public bool IsOpenSubOfferDetailWindow()
		{
			return default(bool);
		}

		// Token: 0x06003F3A RID: 16186 RVA: 0x00018D68 File Offset: 0x00016F68
		[Token(Token = "0x60039E7")]
		[Address(RVA = "0x10143A740", Offset = "0x143A740", VA = "0x10143A740")]
		public bool IsCloseSubOfferDetailWindow()
		{
			return default(bool);
		}

		// Token: 0x06003F3B RID: 16187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E8")]
		[Address(RVA = "0x10143A770", Offset = "0x143A770", VA = "0x10143A770")]
		private void OnClickSubOfferDetailDecideButton(long offerMngID)
		{
		}

		// Token: 0x06003F3C RID: 16188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039E9")]
		[Address(RVA = "0x10143A7AC", Offset = "0x143A7AC", VA = "0x10143A7AC")]
		public void OpenSubOfferRewardWindow(RewardSet[] rewardSet)
		{
		}

		// Token: 0x06003F3D RID: 16189 RVA: 0x00018D80 File Offset: 0x00016F80
		[Token(Token = "0x60039EA")]
		[Address(RVA = "0x101439DB4", Offset = "0x1439DB4", VA = "0x101439DB4")]
		private bool IsCompleteLoadBG()
		{
			return default(bool);
		}

		// Token: 0x06003F3E RID: 16190 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60039EB")]
		[Address(RVA = "0x10143A4B4", Offset = "0x143A4B4", VA = "0x10143A4B4")]
		private Sprite GetBGSprite()
		{
			return null;
		}

		// Token: 0x06003F3F RID: 16191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039EC")]
		[Address(RVA = "0x10143A810", Offset = "0x143A810", VA = "0x10143A810")]
		private void PreProcessUI(Action updateCallback, Action<bool> backButtonCallback, bool isLoadBG)
		{
		}

		// Token: 0x06003F40 RID: 16192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039ED")]
		[Address(RVA = "0x10143AAAC", Offset = "0x143AAAC", VA = "0x10143AAAC")]
		private void PostProcessUI()
		{
		}

		// Token: 0x06003F41 RID: 16193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039EE")]
		[Address(RVA = "0x10143AB98", Offset = "0x143AB98", VA = "0x10143AB98")]
		public void OnClickMemberButton()
		{
		}

		// Token: 0x06003F42 RID: 16194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039EF")]
		[Address(RVA = "0x10143AC14", Offset = "0x143AC14", VA = "0x10143AC14")]
		private void EditWindowUpdateCallback()
		{
		}

		// Token: 0x06003F43 RID: 16195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F0")]
		[Address(RVA = "0x10143ADBC", Offset = "0x143ADBC", VA = "0x10143ADBC")]
		private void EditWindowOnEndCallback()
		{
		}

		// Token: 0x06003F44 RID: 16196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F1")]
		[Address(RVA = "0x10143AE6C", Offset = "0x143AE6C", VA = "0x10143AE6C")]
		private void DecideRoomMemberCallback(List<long> member)
		{
		}

		// Token: 0x06003F45 RID: 16197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F2")]
		[Address(RVA = "0x10143B078", Offset = "0x143B078", VA = "0x10143B078")]
		private void DecideRoomMemberFinalCallback()
		{
		}

		// Token: 0x06003F46 RID: 16198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F3")]
		[Address(RVA = "0x1014390CC", Offset = "0x14390CC", VA = "0x1014390CC")]
		public void CloseBackGround()
		{
		}

		// Token: 0x06003F47 RID: 16199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F4")]
		[Address(RVA = "0x10143B11C", Offset = "0x143B11C", VA = "0x10143B11C")]
		public void OnClickContentRoomListButton()
		{
		}

		// Token: 0x06003F48 RID: 16200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F5")]
		[Address(RVA = "0x10143B194", Offset = "0x143B194", VA = "0x10143B194")]
		public void OnClickMainOfferButton()
		{
		}

		// Token: 0x06003F49 RID: 16201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F6")]
		[Address(RVA = "0x10143B2A8", Offset = "0x143B2A8", VA = "0x10143B2A8")]
		private void MainOfferUpdateCallback()
		{
		}

		// Token: 0x06003F4A RID: 16202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F7")]
		[Address(RVA = "0x10143B468", Offset = "0x143B468", VA = "0x10143B468")]
		private void MainOfferBackButtonCallback(bool isCallFromShortCut)
		{
		}

		// Token: 0x06003F4B RID: 16203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F8")]
		[Address(RVA = "0x10143B568", Offset = "0x143B568", VA = "0x10143B568")]
		private void MainOfferOnEndCallback()
		{
		}

		// Token: 0x06003F4C RID: 16204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039F9")]
		[Address(RVA = "0x10143B6D4", Offset = "0x143B6D4", VA = "0x10143B6D4")]
		public void OnFinishFinalizeOfferWindow(OfferWindow.eButtonID buttonID, long mngId)
		{
		}

		// Token: 0x06003F4D RID: 16205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FA")]
		[Address(RVA = "0x10143B7A0", Offset = "0x143B7A0", VA = "0x10143B7A0")]
		public void OnClickRewardWindowButton(long mngId)
		{
		}

		// Token: 0x06003F4E RID: 16206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FB")]
		[Address(RVA = "0x10143B8DC", Offset = "0x143B8DC", VA = "0x10143B8DC")]
		private void MainOfferRewardWindowOnEndCallback()
		{
		}

		// Token: 0x06003F4F RID: 16207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FC")]
		[Address(RVA = "0x10143B96C", Offset = "0x143B96C", VA = "0x10143B96C")]
		public void OnClickHideUIButton()
		{
		}

		// Token: 0x06003F50 RID: 16208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FD")]
		[Address(RVA = "0x10143B9A0", Offset = "0x143B9A0", VA = "0x10143B9A0")]
		public void OnClickSubOfferButton()
		{
		}

		// Token: 0x06003F51 RID: 16209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FE")]
		[Address(RVA = "0x10143BA58", Offset = "0x143BA58", VA = "0x10143BA58")]
		private void CharacterOfferUpdateCallback()
		{
		}

		// Token: 0x06003F52 RID: 16210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60039FF")]
		[Address(RVA = "0x10143BB98", Offset = "0x143BB98", VA = "0x10143BB98")]
		private void CharacterOfferBackButtonCallback(bool isCallFromShortCut)
		{
		}

		// Token: 0x06003F53 RID: 16211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A00")]
		[Address(RVA = "0x10143BC98", Offset = "0x143BC98", VA = "0x10143BC98")]
		private void CharacterOfferOnEndCallback()
		{
		}

		// Token: 0x06003F54 RID: 16212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A01")]
		[Address(RVA = "0x10143BDC0", Offset = "0x143BDC0", VA = "0x10143BDC0")]
		public ContentRoomUI()
		{
		}

		// Token: 0x04004E5C RID: 20060
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003721")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04004E5D RID: 20061
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003722")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004E5E RID: 20062
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003723")]
		[SerializeField]
		private Text m_ContentRoomName;

		// Token: 0x04004E5F RID: 20063
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003724")]
		[SerializeField]
		private GameObject m_HideTouchPanel;

		// Token: 0x04004E60 RID: 20064
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003725")]
		[SerializeField]
		private OfferWindow m_OfferWindow;

		// Token: 0x04004E61 RID: 20065
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003726")]
		[SerializeField]
		private SubOfferWindow m_SubOfferWindow;

		// Token: 0x04004E62 RID: 20066
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003727")]
		[SerializeField]
		private MainOfferRewardWindow m_MainOfferRewardWindow;

		// Token: 0x04004E63 RID: 20067
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003728")]
		[SerializeField]
		private SubOfferDetailWindow m_SubOfferDetailWindow;

		// Token: 0x04004E64 RID: 20068
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003729")]
		[SerializeField]
		private SubOfferRewardWindow m_SubOfferRewardWindow;

		// Token: 0x04004E65 RID: 20069
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400372A")]
		[SerializeField]
		private ContentRoomMemberEditWindow m_editWindow;

		// Token: 0x04004E66 RID: 20070
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400372B")]
		[SerializeField]
		private GameObject m_BadgeObject;

		// Token: 0x04004E67 RID: 20071
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400372C")]
		[SerializeField]
		private Badge m_CharaBadgeObject;

		// Token: 0x04004E68 RID: 20072
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400372D")]
		private ContentRoomUI.eStep m_Step;

		// Token: 0x04004E69 RID: 20073
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400372E")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04004E6A RID: 20074
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400372F")]
		private eTitleType m_TitleType;

		// Token: 0x04004E6B RID: 20075
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003730")]
		private string m_BGName;

		// Token: 0x04004E6C RID: 20076
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003731")]
		private SpriteHandler m_BGHandler;

		// Token: 0x04004E6D RID: 20077
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003732")]
		private bool m_SetupUI;

		// Token: 0x04004E6E RID: 20078
		[Cpp2IlInjected.FieldOffset(Offset = "0xC9")]
		[Token(Token = "0x4003733")]
		private bool m_RunningUI;

		// Token: 0x04004E6F RID: 20079
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4003734")]
		private int m_TransitQuestID;

		// Token: 0x04004E70 RID: 20080
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003735")]
		private bool m_IsOccurredOfferEffect;

		// Token: 0x04004E71 RID: 20081
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003736")]
		private Action m_MainStepCallback;

		// Token: 0x04004E72 RID: 20082
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003737")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04004E73 RID: 20083
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003738")]
		private GlobalUI.SceneInfoStack m_InfoStack;

		// Token: 0x02000D6A RID: 3434
		[Token(Token = "0x20010FE")]
		private enum eStep
		{
			// Token: 0x04004E75 RID: 20085
			[Token(Token = "0x4006A85")]
			Hide,
			// Token: 0x04004E76 RID: 20086
			[Token(Token = "0x4006A86")]
			In,
			// Token: 0x04004E77 RID: 20087
			[Token(Token = "0x4006A87")]
			Main,
			// Token: 0x04004E78 RID: 20088
			[Token(Token = "0x4006A88")]
			Out,
			// Token: 0x04004E79 RID: 20089
			[Token(Token = "0x4006A89")]
			End,
			// Token: 0x04004E7A RID: 20090
			[Token(Token = "0x4006A8A")]
			Num
		}
	}
}
