﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CE1 RID: 3297
	[Token(Token = "0x20008CD")]
	[StructLayout(3)]
	public class ContentTitleLogo : ASyncImage
	{
		// Token: 0x06003C5E RID: 15454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371B")]
		[Address(RVA = "0x10143BE20", Offset = "0x143BE20", VA = "0x10143BE20")]
		public void Apply(eTitleType titleType)
		{
		}

		// Token: 0x06003C5F RID: 15455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371C")]
		[Address(RVA = "0x10143BF30", Offset = "0x143BF30", VA = "0x10143BF30", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C60 RID: 15456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371D")]
		[Address(RVA = "0x10143BF5C", Offset = "0x143BF5C", VA = "0x10143BF5C")]
		public ContentTitleLogo()
		{
		}

		// Token: 0x04004BA7 RID: 19367
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400352B")]
		protected eTitleType m_TitleType;
	}
}
