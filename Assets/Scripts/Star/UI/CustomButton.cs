﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Star.UI {
    [RequireComponent(typeof(InvisibleGraphic), typeof(ColorGroup))]
    public class CustomButton : MonoBehaviour, IPointerClickHandler, IEventSystemHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {
        public const float SCROLL_THRESHOLD = 32f;
        public const float HOLD_THRESHOLD = 0.5f;

        [SerializeField] private bool m_Interactable = true;
        [SerializeField] private bool m_InteractableColorChange = true;
        [SerializeField] private bool m_EnableInputOnNotInteractable;
        [SerializeField] private bool m_EnableInputHoldOnNotInteractable;
        [SerializeField] private bool m_EnableInputOnInputBlock;
        [SerializeField] private bool m_IsIgnoreGroup;
        [SerializeField] private RectTransform m_BodyRectTransform;
        [SerializeField] private ePreset m_Preset;
        [SerializeField] private float m_TransitSec;
        [SerializeField] private bool m_PlayMove;
        [SerializeField] private Vector3 m_MoveVec = new Vector3(0f, -10f, 0f);
        [SerializeField] private bool m_PlayScale;
        [SerializeField] private Vector3 m_ScaleVec = Vector3.one;
        [SerializeField] private bool m_PlayExtend = true;
        [SerializeField] private float m_ExtendWidth = 5f;
        [SerializeField] private bool m_PlayAnim;
        [SerializeField] private AnimationClip m_WaitAnim;
        [SerializeField] private AnimationClip m_PressAnim;
        [SerializeField] private AnimationClip m_ReleaseAnim;
        [SerializeField] private AnimationClip m_DecideAnim;
        [SerializeField] private bool m_PlayColorTint;
        [SerializeField] private Material m_ColorTintMaterial;
        [SerializeField] private Color m_PressColor = Color.gray;
        [SerializeField] private Color m_DecideColor = new Color(0.7f, 0.6f, 0.5f, 1f);
        [SerializeField] private bool m_PlaySwapObj;
        [SerializeField] private bool m_PlaySEPress;
        [SerializeField] private eSoundSeListDB m_PressSound;
        [SerializeField] private bool m_PlaySEClick;
        [SerializeField] private eSoundSeListDB m_ClickSound;
        [SerializeField] private bool m_PlaySEHold;
        [SerializeField] private eSoundSeListDB m_HoldSound;

        public UnityEvent m_OnClick;
        public UnityEvent m_OnHold;

        private bool m_InteractableParent = true;
        private eButtonState m_CurrentButtonState = eButtonState.Wait;
        private float m_TransitRate;
        private bool m_IsDoneApplyPreset;
        private SwapObj[] m_SwapObjs;
        private GraphForButton[] m_GraphForButtons;
        private ColorGroup m_ColorGroup;
        private Animation m_Animation;
        private bool m_IsHold;
        private float m_HoldCount;
        private bool m_EnableNGSE = true;
        private eNGSEType m_NGSEType;
        private float m_BeforeTime;
        private bool m_IsEnableInputParent = true;
        private bool m_IsEnableInputSelf = true;
        private bool m_IsDecided;
        private bool m_DecideColorChange = true;
        private Vector3 m_DefaultLocalPosition;
        private Vector3 m_DefaultLocalScale;
        private float m_DefaultRectWidth;

        public ColorGroup ColorGroup {
            get {
                if (m_ColorGroup == null) {
                    m_ColorGroup = GetComponent<ColorGroup>();
                }
                return m_ColorGroup;
            }
        }

        public bool IsEnableInput {
            get => m_IsEnableInputParent && m_IsEnableInputSelf;
            set => m_IsEnableInputSelf = value;

        }

        public bool IsEnableInputParent {
            get => m_IsEnableInputParent;
            set => m_IsEnableInputParent = value;

        }

        public bool Interactable {
            get => m_Interactable && m_InteractableParent;
            set {
                m_Interactable = value;
                ApplyStandardColor();
            }
        }

        public bool InteractableParent {
            get => m_InteractableParent;
            set {
                m_InteractableParent = value;
                ApplyStandardColor();
            }
        }

        public bool EnableInputOnNotInteractable {
            get => m_EnableInputHoldOnNotInteractable;
            set => m_EnableInputHoldOnNotInteractable = value;
        }

        public bool IsDecided {
            get => m_IsDecided;
            set {
                m_IsDecided = value;
                ApplyStandardColor();
            }
        }

        public bool IgnoreGroup {
            get => m_IsIgnoreGroup;
            set => m_IsIgnoreGroup = value;
        }

        public bool IsHold => m_IsHold;

        public void AddListerner(UnityAction onClickCallBack) {
            m_OnClick.AddListener(onClickCallBack);
        }

        public void RemoveListener(UnityAction onClickCallBack) {
            m_OnClick.RemoveListener(onClickCallBack);
        }

        public void SetRepeatButtonSE() {
            m_PlaySEPress = true;
            m_PressSound = eSoundSeListDB.SYS_DECISION_1;
            m_PlaySEClick = false;
            m_PlaySEHold = false;
            m_NGSEType = eNGSEType.Press;
        }

        private void ApplyPreset() {
            if (m_IsDoneApplyPreset) { return; }
            m_IsDoneApplyPreset = true;

            switch (m_Preset) {
                case ePreset.CommonButton:
                case ePreset.Button:
                case ePreset.Icon:
                case ePreset.Panel:
                    m_TransitSec = 0.2f;
                    m_PlayMove = false;
                    m_PlayScale = false;
                    m_PlayExtend = false;
                    m_ExtendWidth = 0f;
                    m_PlayAnim = false;
                    m_PlayColorTint = true;
                    m_PressColor = new Color(0.75f, 0.75f, 0.75f);
                    m_DecideColor = new Color(1f, 0.7f, 0.4f);
                    m_ColorTintMaterial = null;
                    m_PlaySwapObj = true;
                    m_DecideColorChange = false;
                    break;
                case ePreset.Tab:
                    m_TransitSec = 0.2f;
                    m_PlayMove = false;
                    m_PlayScale = false;
                    m_PlayExtend = false;
                    m_PlayAnim = false;
                    m_PlayColorTint = true;
                    m_ColorTintMaterial = null;
                    m_PressColor = new Color(0.75f, 0.75f, 0.75f);
                    m_DecideColor = new Color(1f, 0.7f, 0.4f);
                    m_PlaySwapObj = false;
                    break;
            }
            ApplyStandardColor();
        }

        private void Start() {
            ApplyPreset();
            m_SwapObjs = GetComponentsInChildren<SwapObj>(true);
            m_GraphForButtons = GetComponentsInChildren<GraphForButton>(true);
            if (m_CurrentButtonState == eButtonState.None) {
                ChangeButtonState(eButtonState.Wait);
            }
            if (m_BodyRectTransform == null) {
                m_BodyRectTransform = GetComponent<RectTransform>().Find("body") as RectTransform;
            }
            if (m_BodyRectTransform != null) {
                m_DefaultLocalPosition = m_BodyRectTransform.localPosition;
                m_DefaultLocalScale = m_BodyRectTransform.localScale;
            }
            ApplyStandardColor();
        }

        private void Update() {
            if (!IsEnableInput || (!Interactable && !m_EnableInputHoldOnNotInteractable) || (!m_EnableInputOnInputBlock && GameSystem.Inst.InputBlock.IsBlockInput())) {
                m_IsHold = false;
            }
            if (m_BodyRectTransform != null && m_DefaultRectWidth == 0f) {
                m_DefaultRectWidth = m_BodyRectTransform.rect.width;
            }
            m_HoldCount += Time.realtimeSinceStartup - m_BeforeTime;
            m_BeforeTime = Time.realtimeSinceStartup;
            if (m_IsHold) {
                if (m_HoldCount > HOLD_THRESHOLD) {
                    ExecuteHold();
                    m_IsHold = false;
                }
            } else if (m_PlayAnim && m_CurrentButtonState == eButtonState.Release && (m_Animation == null || m_ReleaseAnim == null || !m_Animation.IsPlaying(m_ReleaseAnim.name))) {
                if (m_Animation != null) {
                    m_Animation.Stop();
                }
                ChangeButtonState(eButtonState.Wait);
            }
            if (m_PlayMove || m_PlayScale || m_PlayExtend || m_PlayColorTint) {
                if (m_CurrentButtonState == eButtonState.Press || m_CurrentButtonState == eButtonState.Release) {
                    float num;
                    if (m_TransitSec > 0f) {
                        num = Time.deltaTime * 1f / m_TransitSec;
                    } else {
                        num = 1f;
                    }
                    if (m_CurrentButtonState == eButtonState.Press) {
                        m_TransitRate += num;
                    } else if (m_CurrentButtonState == eButtonState.Release) {
                        m_TransitRate -= num;
                    }
                    if (m_TransitRate > 1f) {
                        m_TransitRate = 1f;
                    } else if (m_TransitRate < 0f) {
                        m_TransitRate = 0f;
                        if (m_CurrentButtonState == eButtonState.Release) {
                            ChangeButtonState(eButtonState.Wait);
                        }
                    }
                    if (m_BodyRectTransform) {
                        if (m_PlayMove) {
                            m_BodyRectTransform.localPosition = m_DefaultLocalPosition + m_MoveVec * m_TransitRate;
                        }
                        if (m_PlayScale || m_PlayExtend) {
                            Vector3 vector = m_DefaultLocalScale;
                            if (m_PlayScale) {
                                vector = m_DefaultLocalScale + (m_ScaleVec - m_DefaultLocalScale) * m_TransitRate;
                            }
                            if (m_PlayExtend && m_DefaultRectWidth > 0f) {
                                vector *= 1f + (m_ExtendWidth / m_DefaultRectWidth) * m_TransitRate;
                            }
                            m_BodyRectTransform.localScale = vector;
                        }
                    }
                    if (m_PlayColorTint) {
                        ApplyStandardColor();
                    }
                } else if (m_BodyRectTransform) {
                    m_BodyRectTransform.localPosition = m_DefaultLocalPosition;
                    m_BodyRectTransform.localScale = m_DefaultLocalScale;
                }
                if (m_CurrentButtonState == eButtonState.Press && InputTouch.Inst != null && InputTouch.Inst.GetAvailableTouchCnt() == 0) {
                    ChangeButtonState(eButtonState.Release);
                }
            }
        }

        public void OnPointerClick(PointerEventData eventData) {
            bool judge = JudgeOnPointer(eventData, IsEnableInput, m_EnableInputOnInputBlock);
            if (!judge) { return; }

            if (!Interactable) {
                if (m_EnableNGSE && m_NGSEType == eNGSEType.Click) {
                    GameSystem.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG);
                }
            }
            m_EnableNGSE = true;
            if (Interactable || EnableInputOnNotInteractable) {
                if (IsHold) {
                    if (m_HoldCount > HOLD_THRESHOLD) {
                        ExecuteHold();
                    } else {
                        ExecuteClick();
                    }
                    m_IsHold = false;
                    ApplyStandardColor();
                    if (m_CurrentButtonState == eButtonState.Press) {
                        ChangeButtonState(eButtonState.Release);
                        ResetHold();
                    }
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData) {
            m_EnableNGSE = true;
            bool judge = JudgeOnPointer(eventData, IsEnableInput, m_EnableInputOnInputBlock);
            if (!judge) { return; }

            if (!Interactable && !m_EnableInputOnNotInteractable && !m_EnableInputHoldOnNotInteractable) {
                if (m_NGSEType == eNGSEType.Press) {
                    GameSystem.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG);
                }
            } else {
                ApplyStandardColor();
                ChangeButtonState(eButtonState.Press);
                ResetHold();
                m_IsHold = true;
            }
        }

        public void OnPointerUp(PointerEventData eventData) {
            bool judge = JudgeOnPointer(eventData, IsEnableInput, m_EnableInputOnInputBlock);
            if (judge && (Interactable || EnableInputOnNotInteractable) && IsHold) {
                if (!eventData.IsPointerMoving() && m_CurrentButtonState == eButtonState.Press) {
                    ForceRelease();
                    m_IsHold = false;
                }
            }
        }

        public void ForceRelease() {
            ApplyStandardColor();
            if (m_CurrentButtonState == eButtonState.Press && m_HoldCount < HOLD_THRESHOLD) {
                ExecuteClick();
                m_IsHold = false;
            }
            m_IsHold = false;
            ChangeButtonState(eButtonState.Release);
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (eventData.pointerId == 0 && m_CurrentButtonState == eButtonState.Press) {
                ApplyStandardColor();
                ChangeButtonState(eButtonState.Release);
                m_IsHold = false;
            }
        }

        public void ForceEnableHold(bool flg) {
            if ((IsEnableInput && Interactable) || EnableInputOnNotInteractable) {
                if (flg) {
                    ChangeButtonState(eButtonState.Press);
                    ResetHold();
                    m_TransitRate = 0;
                } else if (m_CurrentButtonState == eButtonState.Press) {
                    ChangeButtonState(eButtonState.Release);
                    ResetHold();
                }
            }
        }

        private void OnEnable() {
            ResetHold();
        }

        private void ResetHold() {
            m_IsHold = false;
            m_HoldCount = 0f;
            m_BeforeTime = Time.realtimeSinceStartup;
        }

        public void ExecuteClick() {
            if (m_PlaySEClick && Interactable) {
                GameSystem.Inst.SoundMng.PlaySE(m_ClickSound);
                m_EnableNGSE = false;
            }
            m_OnClick?.Invoke();
            ForceEnableHold(false);
        }

        private void ExecuteHold() {
            m_OnHold?.Invoke();
            if (m_PlaySEHold) {
                GameSystem.Inst.SoundMng.PlaySE(m_HoldSound);
            }
        }

        private void ChangeButtonState(eButtonState state) {
            if (state == eButtonState.Press && m_CurrentButtonState != eButtonState.Press && m_PlaySEPress) {
                GameSystem.Inst.SoundMng.PlaySE(m_PressSound);
            }
            m_CurrentButtonState = state;
            if (m_PlayAnim) {
                AnimationClip clip = null;
                switch (state) {
                    case eButtonState.Wait:
                        clip = m_WaitAnim;
                        break;
                    case eButtonState.Press:
                        clip = m_PressAnim;
                        if (m_PlaySEPress) {
                            GameSystem.Inst.SoundMng.PlaySE(m_PressSound);
                        }
                        break;
                    case eButtonState.Release:
                        clip = m_ReleaseAnim;
                        break;
                    case eButtonState.Decide:
                        clip = m_DecideAnim;
                        break;
                }
                PlayAnim(clip);
            }
            if (m_GraphForButtons != null && m_GraphForButtons.Length > 0) {
                GraphForButton.eState graphState = GraphForButton.eState.Default;
                if (!m_Interactable) {
                    graphState = GraphForButton.eState.Disable;
                } else if (state == eButtonState.Press) {
                    graphState = GraphForButton.eState.Press;
                }
                for (int i = 0; i < m_GraphForButtons.Length; i++) {
                    m_GraphForButtons[i].ChangeState(graphState);
                }
            }
            if (m_PlaySwapObj) {
                eSwapObjState eSwapObjState = eSwapObjState.Default;
                if (!Interactable) {
                    eSwapObjState = eSwapObjState.NotInteractable;
                } else if (m_CurrentButtonState == eButtonState.Press) {
                    eSwapObjState = eSwapObjState.Press;
                } else if (m_IsDecided) {
                    eSwapObjState = eSwapObjState.Decide;
                }
                if (m_SwapObjs != null && m_SwapObjs.Length > 0) {
                    for (int j = 0; j < m_SwapObjs.Length; j++) {
                        if (!m_SwapObjs[j].IgnoreCustomButton) {
                            CustomButton customButton = null;
                            Transform transform = m_SwapObjs[j].GetComponent<Transform>();
                            while (transform != null) {
                                if (transform.TryGetComponent(out customButton)) { break; }
                                transform = transform.parent;
                            }
                            if (customButton == this) {
                                if ((int)eSwapObjState < m_SwapObjs[j].GetTargetObjNum()) {
                                    m_SwapObjs[j].Swap((int)eSwapObjState);
                                } else if (eSwapObjState == eSwapObjState.Decide) {
                                    m_SwapObjs[j].Swap((int)eSwapObjState.NotInteractable);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void PlayAnim(AnimationClip clip) {
            if (m_Animation == null) {
                if (!TryGetComponent(out m_Animation)) {
                    m_Animation = gameObject.AddComponent<Animation>();
                    if (m_Animation == null) { return; }
                }
                if (m_WaitAnim != null) {
                    m_Animation.AddClip(m_WaitAnim, m_WaitAnim.name);
                }
                if (m_PressAnim != null) {
                    m_Animation.AddClip(m_PressAnim, m_PressAnim.name);
                }
                if (m_ReleaseAnim != null) {
                    m_Animation.AddClip(m_ReleaseAnim, m_ReleaseAnim.name);
                }
                if (m_DecideAnim != null) {
                    m_Animation.AddClip(m_DecideAnim, m_DecideAnim.name);
                }
            }
            m_Animation.playAutomatically = false;
            if (clip != null) {
                m_Animation.CrossFade(clip.name, 0.2f);
            } else {
                m_BodyRectTransform.localPosition = m_DefaultLocalPosition;
            }
        }

        private void ApplyStandardColor() {
            ApplyPreset();
            if (Interactable) {
                if (ColorGroup != null && m_InteractableColorChange) {
                    if (m_IsDecided && m_DecideColorChange) {
                        if (m_ColorTintMaterial == null) {
                            ColorGroup.ChangeColor(m_DecideColor, ColorGroup.eColorBlendMode.Mul);
                        } else {
                            ColorGroup.ChangeColor(m_DecideColor, m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set);
                        }
                        m_TransitRate = 0f;
                    } else if (m_PlayColorTint) {
                        if (m_ColorTintMaterial == null) {
                            ColorGroup.ChangeColor(m_PressColor, ColorGroup.eColorBlendMode.Mul, m_TransitRate);
                        } else {
                            ColorGroup.ChangeColor(m_PressColor, m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set, m_TransitRate);
                        }
                    }
                }
                if (m_GraphForButtons != null) {
                    for (int i = 0; i < m_GraphForButtons.Length; i++) {
                        m_GraphForButtons[i].SetDecide(m_IsDecided);
                        if (!Interactable) {
                            m_GraphForButtons[i].ChangeState(GraphForButton.eState.Disable);
                        } else {
                            m_GraphForButtons[i].ChangeState(GraphForButton.eState.Default);
                        }
                    }
                }
                ChangeButtonState(m_CurrentButtonState);
                if (!Interactable && ColorGroup != null && m_InteractableColorChange) {
                    ColorGroup.ChangeColor(Color.gray, null);
                }
            }
        }

        public static bool JudgeOnPointer(PointerEventData eventData, bool isEnableInput, bool isEnableInputOnInputBlock) {
            if (eventData.pointerId > 0) { return false; }
            if (!isEnableInput) { return false; }
            if (!isEnableInputOnInputBlock && GameSystem.Inst.InputBlock.IsBlockInput()) {
                return false;
            }
            if (eventData.position.x > Mathf.Epsilon && eventData.position.x < Screen.width - Mathf.Epsilon) {
                if (eventData.position.y > Mathf.Epsilon && eventData.position.y < Screen.height - Mathf.Epsilon) {
                    return true;
                }
            }
            return false;
        }

        public void SetTransitRate(float rate) {
            m_TransitRate = rate;
            ApplyStandardColor();
        }

        private enum eButtonState {
            None,
            Wait,
            Press,
            Release,
            Decide
        }

        public enum ePreset {
            None,
            CommonButton,
            Button,
            Icon,
            Panel,
            Tab
        }

        private enum eSwapObjState {
            Default,
            Press,
            NotInteractable,
            Decide
        }

        private enum eNGSEType {
            Click,
            Press
        }
    }
}
