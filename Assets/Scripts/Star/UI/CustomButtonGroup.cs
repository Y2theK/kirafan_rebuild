﻿using UnityEngine;

namespace Star.UI {
    public class CustomButtonGroup : MonoBehaviour {
        [SerializeField] private bool m_IsEnableInput = true;
        [SerializeField] private bool m_IsInteractable = true;

        private bool m_IsEnableInputParent = true;

        public bool IsEnableInput {
            get => m_IsEnableInput && m_IsEnableInputParent;
            set {
                m_IsEnableInput = value;
                UpdateInput();
            }
        }

        public bool Interactable {
            get => m_IsInteractable;
            set {
                m_IsInteractable = value;
                CustomButton[] customButtons = GetComponentsInChildren<CustomButton>(true);
                for (int i = 0; i < customButtons.Length; i++) {
                    if (!customButtons[i].IgnoreGroup) {
                        customButtons[i].InteractableParent = value;
                    }
                }
            }
        }

        public bool IsEnableInputParent {
            set => m_IsEnableInputParent = value;
        }

        private void Start() {
            UpdateInput();
            Interactable = m_IsInteractable;
        }

        private void UpdateInput() {
            CustomButton[] customButtons = GetComponentsInChildren<CustomButton>(true);
            for (int i = 0; i < customButtons.Length; i++) {
                customButtons[i].IsEnableInputParent = IsEnableInput;
            }
            CustomButtonGroup[] customButtonGroups = GetComponentsInChildren<CustomButtonGroup>(true);
            for (int j = 1; j < customButtonGroups.Length; j++) {
                customButtonGroups[j].m_IsEnableInputParent = IsEnableInput;
                customButtonGroups[j].UpdateInput();
            }
        }

        public bool ExistHoldButton() {
            CustomButton[] customButtons = GetComponentsInChildren<CustomButton>(true);
            for (int i = 0; i < customButtons.Length; i++) {
                if (customButtons[i].IsHold) {
                    return true;
                }
            }
            return false;
        }
    }
}
