﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C97 RID: 3223
	[Token(Token = "0x200089F")]
	[StructLayout(3)]
	public class CustomSlider : MonoBehaviour
	{
		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x06003A9A RID: 15002 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700042B")]
		public Slider Slider
		{
			[Token(Token = "0x600355D")]
			[Address(RVA = "0x10143E7D0", Offset = "0x143E7D0", VA = "0x10143E7D0")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x06003A9B RID: 15003 RVA: 0x00018378 File Offset: 0x00016578
		// (set) Token: 0x06003A9C RID: 15004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700042C")]
		public float NormalizedValue
		{
			[Token(Token = "0x600355E")]
			[Address(RVA = "0x10143E7D8", Offset = "0x143E7D8", VA = "0x10143E7D8")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x600355F")]
			[Address(RVA = "0x10143E808", Offset = "0x143E808", VA = "0x10143E808")]
			set
			{
			}
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x06003A9D RID: 15005 RVA: 0x00018390 File Offset: 0x00016590
		// (set) Token: 0x06003A9E RID: 15006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700042D")]
		public float Value
		{
			[Token(Token = "0x6003560")]
			[Address(RVA = "0x10143E96C", Offset = "0x143E96C", VA = "0x10143E96C")]
			get
			{
				return 0f;
			}
			[Token(Token = "0x6003561")]
			[Address(RVA = "0x10143E9A4", Offset = "0x143E9A4", VA = "0x10143E9A4")]
			set
			{
			}
		}

		// Token: 0x1400006A RID: 106
		// (add) Token: 0x06003A9F RID: 15007 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003AA0 RID: 15008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400006A")]
		public event Action OnValueChanged
		{
			[Token(Token = "0x6003562")]
			[Address(RVA = "0x10143E9F8", Offset = "0x143E9F8", VA = "0x10143E9F8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003563")]
			[Address(RVA = "0x10143EB04", Offset = "0x143EB04", VA = "0x10143EB04")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003AA1 RID: 15009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003564")]
		[Address(RVA = "0x10143EC10", Offset = "0x143EC10", VA = "0x10143EC10")]
		private void Start()
		{
		}

		// Token: 0x06003AA2 RID: 15010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003565")]
		[Address(RVA = "0x10143E878", Offset = "0x143E878", VA = "0x10143E878")]
		private void OnValueChangedCallBack(float value)
		{
		}

		// Token: 0x06003AA3 RID: 15011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003566")]
		[Address(RVA = "0x10143ECCC", Offset = "0x143ECCC", VA = "0x10143ECCC")]
		public CustomSlider()
		{
		}

		// Token: 0x0400496A RID: 18794
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003393")]
		[SerializeField]
		private Slider m_Slider;

		// Token: 0x0400496B RID: 18795
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003394")]
		[SerializeField]
		private Text m_ValueText;
	}
}
