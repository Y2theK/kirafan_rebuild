﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E1B RID: 3611
	[Token(Token = "0x20009AD")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D2D0", Offset = "0x11D2D0")]
	[StructLayout(3)]
	public class DataClearButton : MonoBehaviour, IPointerClickHandler, IEventSystemHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
	{
		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x060042CE RID: 17102 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700048B")]
		public ColorGroup ColorGroup
		{
			[Token(Token = "0x6003D46")]
			[Address(RVA = "0x10143ECD4", Offset = "0x143ECD4", VA = "0x10143ECD4")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x060042CF RID: 17103 RVA: 0x00019578 File Offset: 0x00017778
		// (set) Token: 0x060042D0 RID: 17104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700048C")]
		public bool IsEnableInput
		{
			[Token(Token = "0x6003D47")]
			[Address(RVA = "0x10143ED9C", Offset = "0x143ED9C", VA = "0x10143ED9C")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D48")]
			[Address(RVA = "0x10143EDBC", Offset = "0x143EDBC", VA = "0x10143EDBC")]
			set
			{
			}
		}

		// Token: 0x170004E2 RID: 1250
		// (get) Token: 0x060042D1 RID: 17105 RVA: 0x00019590 File Offset: 0x00017790
		// (set) Token: 0x060042D2 RID: 17106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700048D")]
		public bool IsEnableInputParent
		{
			[Token(Token = "0x6003D49")]
			[Address(RVA = "0x10143EDC4", Offset = "0x143EDC4", VA = "0x10143EDC4")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D4A")]
			[Address(RVA = "0x10143EDCC", Offset = "0x143EDCC", VA = "0x10143EDCC")]
			set
			{
			}
		}

		// Token: 0x170004E3 RID: 1251
		// (get) Token: 0x060042D3 RID: 17107 RVA: 0x000195A8 File Offset: 0x000177A8
		// (set) Token: 0x060042D4 RID: 17108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700048E")]
		public bool Interactable
		{
			[Token(Token = "0x6003D4B")]
			[Address(RVA = "0x10143EDD4", Offset = "0x143EDD4", VA = "0x10143EDD4")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D4C")]
			[Address(RVA = "0x10143EDF4", Offset = "0x143EDF4", VA = "0x10143EDF4")]
			set
			{
			}
		}

		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x060042D5 RID: 17109 RVA: 0x000195C0 File Offset: 0x000177C0
		// (set) Token: 0x060042D6 RID: 17110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700048F")]
		public bool InteractableParent
		{
			[Token(Token = "0x6003D4D")]
			[Address(RVA = "0x10143F2B8", Offset = "0x143F2B8", VA = "0x10143F2B8")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D4E")]
			[Address(RVA = "0x10143F2C0", Offset = "0x143F2C0", VA = "0x10143F2C0")]
			set
			{
			}
		}

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x060042D8 RID: 17112 RVA: 0x000195D8 File Offset: 0x000177D8
		// (set) Token: 0x060042D7 RID: 17111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000490")]
		public bool IsDecided
		{
			[Token(Token = "0x6003D50")]
			[Address(RVA = "0x10143F2D0", Offset = "0x143F2D0", VA = "0x10143F2D0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D4F")]
			[Address(RVA = "0x10143F2C8", Offset = "0x143F2C8", VA = "0x10143F2C8")]
			set
			{
			}
		}

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x060042D9 RID: 17113 RVA: 0x000195F0 File Offset: 0x000177F0
		// (set) Token: 0x060042DA RID: 17114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000491")]
		public bool IgnoreGroup
		{
			[Token(Token = "0x6003D51")]
			[Address(RVA = "0x10143F2D8", Offset = "0x143F2D8", VA = "0x10143F2D8")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003D52")]
			[Address(RVA = "0x10143F2E0", Offset = "0x143F2E0", VA = "0x10143F2E0")]
			set
			{
			}
		}

		// Token: 0x060042DB RID: 17115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D53")]
		[Address(RVA = "0x10143F2E8", Offset = "0x143F2E8", VA = "0x10143F2E8")]
		public void AddListerner(UnityAction onClickCallBack)
		{
		}

		// Token: 0x060042DC RID: 17116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D54")]
		[Address(RVA = "0x10143F320", Offset = "0x143F320", VA = "0x10143F320")]
		public void RemoveListener(UnityAction onClickCallBack)
		{
		}

		// Token: 0x060042DD RID: 17117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D55")]
		[Address(RVA = "0x10143F358", Offset = "0x143F358", VA = "0x10143F358")]
		private void ApplyPreset()
		{
		}

		// Token: 0x060042DE RID: 17118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D56")]
		[Address(RVA = "0x10143F554", Offset = "0x143F554", VA = "0x10143F554")]
		private void Start()
		{
		}

		// Token: 0x060042DF RID: 17119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D57")]
		[Address(RVA = "0x10143FB1C", Offset = "0x143FB1C", VA = "0x10143FB1C")]
		private void Update()
		{
		}

		// Token: 0x060042E0 RID: 17120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D58")]
		[Address(RVA = "0x1014403D4", Offset = "0x14403D4", VA = "0x1014403D4", Slot = "4")]
		public void OnPointerClick(PointerEventData eventData)
		{
		}

		// Token: 0x060042E1 RID: 17121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D59")]
		[Address(RVA = "0x101440850", Offset = "0x1440850", VA = "0x101440850", Slot = "5")]
		public void OnPointerDown(PointerEventData eventData)
		{
		}

		// Token: 0x060042E2 RID: 17122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5A")]
		[Address(RVA = "0x101440950", Offset = "0x1440950", VA = "0x101440950", Slot = "6")]
		public void OnPointerUp(PointerEventData eventData)
		{
		}

		// Token: 0x060042E3 RID: 17123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5B")]
		[Address(RVA = "0x1014409D8", Offset = "0x14409D8", VA = "0x1014409D8")]
		public void ForceRelease()
		{
		}

		// Token: 0x060042E4 RID: 17124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5C")]
		[Address(RVA = "0x101440A2C", Offset = "0x1440A2C", VA = "0x101440A2C", Slot = "7")]
		public void OnPointerExit(PointerEventData eventData)
		{
		}

		// Token: 0x060042E5 RID: 17125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5D")]
		[Address(RVA = "0x101440A90", Offset = "0x1440A90", VA = "0x101440A90")]
		public void ForceEnableHold(bool flg)
		{
		}

		// Token: 0x060042E6 RID: 17126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5E")]
		[Address(RVA = "0x101440B08", Offset = "0x1440B08", VA = "0x101440B08")]
		private void OnEnable()
		{
		}

		// Token: 0x060042E7 RID: 17127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D5F")]
		[Address(RVA = "0x101440624", Offset = "0x1440624", VA = "0x101440624")]
		private void ResetHold()
		{
		}

		// Token: 0x060042E8 RID: 17128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D60")]
		[Address(RVA = "0x101440564", Offset = "0x1440564", VA = "0x101440564")]
		private void ExecuteClick()
		{
		}

		// Token: 0x060042E9 RID: 17129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D61")]
		[Address(RVA = "0x10144031C", Offset = "0x144031C", VA = "0x10144031C")]
		private void ExecuteHold()
		{
		}

		// Token: 0x060042EA RID: 17130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D62")]
		[Address(RVA = "0x10143F778", Offset = "0x143F778", VA = "0x10143F778")]
		private void ChangeButtonState(DataClearButton.eButtonState state)
		{
		}

		// Token: 0x060042EB RID: 17131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D63")]
		[Address(RVA = "0x101440B0C", Offset = "0x1440B0C", VA = "0x101440B0C")]
		private void PlayAnim(AnimationClip clip)
		{
		}

		// Token: 0x060042EC RID: 17132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D64")]
		[Address(RVA = "0x10143EDFC", Offset = "0x143EDFC", VA = "0x10143EDFC")]
		private void ApplyStandardColor()
		{
		}

		// Token: 0x060042ED RID: 17133 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003D65")]
		[Address(RVA = "0x101440F14", Offset = "0x1440F14", VA = "0x101440F14")]
		public string GetMethodName(MethodBase method)
		{
			return null;
		}

		// Token: 0x060042EE RID: 17134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D66")]
		[Address(RVA = "0x10144115C", Offset = "0x144115C", VA = "0x10144115C")]
		public DataClearButton()
		{
		}

		// Token: 0x040052F3 RID: 21235
		[Token(Token = "0x4003AA7")]
		public const float SCROLL_THRESHOLD = 24f;

		// Token: 0x040052F4 RID: 21236
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003AA8")]
		[SerializeField]
		private float m_HoldThreshold;

		// Token: 0x040052F5 RID: 21237
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003AA9")]
		[SerializeField]
		private Text m_OutputHoldTime;

		// Token: 0x040052F6 RID: 21238
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003AAA")]
		private string m_PreHoldTimeText;

		// Token: 0x040052F7 RID: 21239
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003AAB")]
		[SerializeField]
		private Image m_OutFrameImage;

		// Token: 0x040052F8 RID: 21240
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003AAC")]
		[SerializeField]
		private bool m_Interactable;

		// Token: 0x040052F9 RID: 21241
		[Cpp2IlInjected.FieldOffset(Offset = "0x39")]
		[Token(Token = "0x4003AAD")]
		private bool m_InteractableParent;

		// Token: 0x040052FA RID: 21242
		[Cpp2IlInjected.FieldOffset(Offset = "0x3A")]
		[Token(Token = "0x4003AAE")]
		[SerializeField]
		private bool m_InteractableColorChange;

		// Token: 0x040052FB RID: 21243
		[Cpp2IlInjected.FieldOffset(Offset = "0x3B")]
		[Token(Token = "0x4003AAF")]
		[SerializeField]
		private bool m_EnableInputOnNotInteractable;

		// Token: 0x040052FC RID: 21244
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003AB0")]
		[SerializeField]
		private bool m_EnableInputHoldOnNotInteractable;

		// Token: 0x040052FD RID: 21245
		[Cpp2IlInjected.FieldOffset(Offset = "0x3D")]
		[Token(Token = "0x4003AB1")]
		[SerializeField]
		private bool m_IsIgnoreGroup;

		// Token: 0x040052FE RID: 21246
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003AB2")]
		[SerializeField]
		private RectTransform m_BodyRectTransform;

		// Token: 0x040052FF RID: 21247
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003AB3")]
		private DataClearButton.eButtonState m_CurrentButtonState;

		// Token: 0x04005300 RID: 21248
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4003AB4")]
		private float m_TransitRate;

		// Token: 0x04005301 RID: 21249
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003AB5")]
		[SerializeField]
		private DataClearButton.ePreset m_Preset;

		// Token: 0x04005302 RID: 21250
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003AB6")]
		private bool m_IsDoneApplyPreset;

		// Token: 0x04005303 RID: 21251
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003AB7")]
		[SerializeField]
		private float m_TransitSec;

		// Token: 0x04005304 RID: 21252
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003AB8")]
		[SerializeField]
		private bool m_PlayMove;

		// Token: 0x04005305 RID: 21253
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003AB9")]
		[SerializeField]
		private Vector3 m_MoveVec;

		// Token: 0x04005306 RID: 21254
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4003ABA")]
		[SerializeField]
		private bool m_PlayScale;

		// Token: 0x04005307 RID: 21255
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003ABB")]
		[SerializeField]
		private Vector3 m_ScaleVec;

		// Token: 0x04005308 RID: 21256
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4003ABC")]
		[SerializeField]
		private bool m_PlayExtend;

		// Token: 0x04005309 RID: 21257
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003ABD")]
		[SerializeField]
		private float m_ExtendWidth;

		// Token: 0x0400530A RID: 21258
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003ABE")]
		[SerializeField]
		private bool m_PlayAnim;

		// Token: 0x0400530B RID: 21259
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003ABF")]
		[SerializeField]
		private AnimationClip m_WaitAnim;

		// Token: 0x0400530C RID: 21260
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003AC0")]
		[SerializeField]
		private AnimationClip m_PressAnim;

		// Token: 0x0400530D RID: 21261
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003AC1")]
		[SerializeField]
		private AnimationClip m_ReleaseAnim;

		// Token: 0x0400530E RID: 21262
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003AC2")]
		[SerializeField]
		private AnimationClip m_DecideAnim;

		// Token: 0x0400530F RID: 21263
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003AC3")]
		[SerializeField]
		private bool m_PlayColorTint;

		// Token: 0x04005310 RID: 21264
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003AC4")]
		[SerializeField]
		private Material m_ColorTintMaterial;

		// Token: 0x04005311 RID: 21265
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003AC5")]
		[SerializeField]
		private Color m_PressColor;

		// Token: 0x04005312 RID: 21266
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003AC6")]
		[SerializeField]
		private Color m_DecideColor;

		// Token: 0x04005313 RID: 21267
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003AC7")]
		[SerializeField]
		private bool m_PlaySwapObj;

		// Token: 0x04005314 RID: 21268
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003AC8")]
		private SwapObj[] m_SwapObjs;

		// Token: 0x04005315 RID: 21269
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003AC9")]
		[SerializeField]
		private bool m_PlaySEPress;

		// Token: 0x04005316 RID: 21270
		[Cpp2IlInjected.FieldOffset(Offset = "0xEC")]
		[Token(Token = "0x4003ACA")]
		[SerializeField]
		private eSoundSeListDB m_PressSound;

		// Token: 0x04005317 RID: 21271
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003ACB")]
		[SerializeField]
		private bool m_PlaySEClick;

		// Token: 0x04005318 RID: 21272
		[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
		[Token(Token = "0x4003ACC")]
		[SerializeField]
		private eSoundSeListDB m_ClickSound;

		// Token: 0x04005319 RID: 21273
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003ACD")]
		[SerializeField]
		private bool m_PlaySEHold;

		// Token: 0x0400531A RID: 21274
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x4003ACE")]
		[SerializeField]
		private eSoundSeListDB m_HoldSound;

		// Token: 0x0400531B RID: 21275
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003ACF")]
		[SerializeField]
		public UnityEvent m_OnClick;

		// Token: 0x0400531C RID: 21276
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003AD0")]
		[SerializeField]
		public UnityEvent m_OnHold;

		// Token: 0x0400531D RID: 21277
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003AD1")]
		private GraphForButton[] m_GraphForButtons;

		// Token: 0x0400531E RID: 21278
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003AD2")]
		private ColorGroup m_ColorGroup;

		// Token: 0x0400531F RID: 21279
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003AD3")]
		private Animation m_Animation;

		// Token: 0x04005320 RID: 21280
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003AD4")]
		private bool m_IsHold;

		// Token: 0x04005321 RID: 21281
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4003AD5")]
		private float m_HoldCount;

		// Token: 0x04005322 RID: 21282
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003AD6")]
		private float m_BeforeTime;

		// Token: 0x04005323 RID: 21283
		[Cpp2IlInjected.FieldOffset(Offset = "0x134")]
		[Token(Token = "0x4003AD7")]
		private bool m_IsEnableInputParent;

		// Token: 0x04005324 RID: 21284
		[Cpp2IlInjected.FieldOffset(Offset = "0x135")]
		[Token(Token = "0x4003AD8")]
		private bool m_IsEnableInputSelf;

		// Token: 0x04005325 RID: 21285
		[Cpp2IlInjected.FieldOffset(Offset = "0x136")]
		[Token(Token = "0x4003AD9")]
		private bool m_IsDecided;

		// Token: 0x04005326 RID: 21286
		[Cpp2IlInjected.FieldOffset(Offset = "0x137")]
		[Token(Token = "0x4003ADA")]
		private bool m_DecideColorChange;

		// Token: 0x04005327 RID: 21287
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003ADB")]
		private Vector3 m_DefaultLocalPosition;

		// Token: 0x04005328 RID: 21288
		[Cpp2IlInjected.FieldOffset(Offset = "0x144")]
		[Token(Token = "0x4003ADC")]
		private Vector3 m_DefaultLocalScale;

		// Token: 0x04005329 RID: 21289
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4003ADD")]
		private float m_DefaultRectWidth;

		// Token: 0x02000E1C RID: 3612
		[Token(Token = "0x2001134")]
		private enum eButtonState
		{
			// Token: 0x0400532B RID: 21291
			[Token(Token = "0x4006B96")]
			None,
			// Token: 0x0400532C RID: 21292
			[Token(Token = "0x4006B97")]
			Wait,
			// Token: 0x0400532D RID: 21293
			[Token(Token = "0x4006B98")]
			Press,
			// Token: 0x0400532E RID: 21294
			[Token(Token = "0x4006B99")]
			Release,
			// Token: 0x0400532F RID: 21295
			[Token(Token = "0x4006B9A")]
			Decide
		}

		// Token: 0x02000E1D RID: 3613
		[Token(Token = "0x2001135")]
		public enum ePreset
		{
			// Token: 0x04005331 RID: 21297
			[Token(Token = "0x4006B9C")]
			None,
			// Token: 0x04005332 RID: 21298
			[Token(Token = "0x4006B9D")]
			CommonButton,
			// Token: 0x04005333 RID: 21299
			[Token(Token = "0x4006B9E")]
			Button,
			// Token: 0x04005334 RID: 21300
			[Token(Token = "0x4006B9F")]
			Icon,
			// Token: 0x04005335 RID: 21301
			[Token(Token = "0x4006BA0")]
			Panel,
			// Token: 0x04005336 RID: 21302
			[Token(Token = "0x4006BA1")]
			Tab
		}

		// Token: 0x02000E1E RID: 3614
		[Token(Token = "0x2001136")]
		private enum eSwapObjState
		{
			// Token: 0x04005338 RID: 21304
			[Token(Token = "0x4006BA3")]
			Default,
			// Token: 0x04005339 RID: 21305
			[Token(Token = "0x4006BA4")]
			Press,
			// Token: 0x0400533A RID: 21306
			[Token(Token = "0x4006BA5")]
			Decide
		}
	}
}
