﻿using Star.UI.Window;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class DataInheritenceSelectContnt : CustomWindow {
        [SerializeField] private ButtonStruct m_ALButtonUsePlatformForiOS;
        [SerializeField] private ButtonStruct m_ALButtonUsePlatformForAndroid;
        [SerializeField] private ButtonStruct m_ALButtonUseIDAndPassword;
        [SerializeField] private ButtonStruct m_CloseButton;
        [SerializeField] private Sprite m_AndroidIconTexture;
        [SerializeField] private Sprite m_IOSIconTexture;

        private DataInheritenceWindowContent.eType m_WindowType;

        private Action m_OnClickALButtonUsePlatform;
        private Action m_OnClickALButtonUseIDAndPassword;
        private Action m_OnCkickCloseButton;

        public void Setup(bool isCofiguration) {
#if UNITY_IOS
			ButtonStruct alButtonUsePlatform = m_ALButtonUsePlatformForiOS;
            eText_MessageDB configMessage = eText_MessageDB.PlayerMoveSettingButtonTitle_iOS_UseMenu;
            eText_MessageDB decideMessage = eText_MessageDB.PlayerMoveSettingButtonTitle_iOS_UseTitle;
			Sprite iconTexture = m_IOSIconTexture;

			if (m_ALButtonUsePlatformForAndroid.m_Button != null) {
				m_ALButtonUsePlatformForAndroid.m_Button.gameObject.SetActive(false);
			}
#elif UNITY_ANDROID
            ButtonStruct alButtonUsePlatform = m_ALButtonUsePlatformForAndroid;
            eText_MessageDB configMessage = eText_MessageDB.PlayerMoveSettingButtonTitle_Android_UseMenu;
            eText_MessageDB decideMessage = eText_MessageDB.PlayerMoveSettingButtonTitle_Android_UseTitle;
            Sprite iconTexture = m_AndroidIconTexture;

            if (m_ALButtonUsePlatformForiOS.m_Button != null) {
                m_ALButtonUsePlatformForiOS.m_Button.gameObject.SetActive(false);
            }
#else
            ButtonStruct alButtonUsePlatform = default;
            eText_MessageDB configMessage = default;
            eText_MessageDB decideMessage = default;
            Sprite iconTexture = null;

            if (m_ALButtonUsePlatformForAndroid.m_Button != null) {
                m_ALButtonUsePlatformForAndroid.m_Button.gameObject.SetActive(false);
            }
            if (m_ALButtonUsePlatformForiOS.m_Button != null) {
                m_ALButtonUsePlatformForiOS.m_Button.gameObject.SetActive(false);
            }
#endif
            if (isCofiguration) {
                if (m_Title != null && m_Title.m_TextObj != null) {
                    m_Title.m_TextObj.text = GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingSetting);
                }
                if (alButtonUsePlatform.m_ButtonText != null) {
                    alButtonUsePlatform.m_ButtonText.text = GameSystem.Inst.DbMng.GetTextMessage(configMessage);
                }
            } else {
                if (m_Title != null && m_Title.m_TextObj != null) {
                    m_Title.m_TextObj.text = GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingConfirm);
                }
                if (alButtonUsePlatform.m_ButtonText != null) {
                    alButtonUsePlatform.m_ButtonText.text = GameSystem.Inst.DbMng.GetTextMessage(decideMessage);
                }
            }
            if (alButtonUsePlatform.m_ButtonTextImage != null) {
                if (iconTexture != null) {
                    alButtonUsePlatform.m_ButtonTextImage.enabled = true;
                    alButtonUsePlatform.m_ButtonTextImage.color = Color.white;
                    alButtonUsePlatform.m_ButtonTextImage.sprite = iconTexture;
                } else {
                    alButtonUsePlatform.m_ButtonTextImage.enabled = false;
                }
            }
            m_ALButtonUseIDAndPassword.m_ButtonText.text = GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingButtonTitle_UseIDPassword);
        }

        public void SetCallback(Action Callback_ALButtonUsePlatform, Action Callback_ALButtonIDAndPassword, Action Callback_CloseButton) {
            m_OnClickALButtonUsePlatform = Callback_ALButtonUsePlatform;
            m_OnClickALButtonUseIDAndPassword = Callback_ALButtonIDAndPassword;
            m_OnCkickCloseButton = Callback_CloseButton;
        }

        public void SetCallbackALButtonUsePlatform(Action callback) {
            m_OnClickALButtonUsePlatform = callback;
        }

        public void SetCallbackALButtonIDAndPassword(Action callback) {
            m_OnClickALButtonUseIDAndPassword = callback;
        }

        public void SetCallbackCloseButton(Action callback) {
            m_OnCkickCloseButton = callback;
        }

        public void OnClickALButtonUsePlatform() {
            if (m_OnClickALButtonUsePlatform != null) {
                m_OnClickALButtonUsePlatform();
            }
        }

        public void OnClickALButtonUseIDAndPassword() {
            if (m_OnClickALButtonUseIDAndPassword != null) {
                m_OnClickALButtonUseIDAndPassword();
            }
        }

        public void OnCkickCloseButton() {
            if (m_OnCkickCloseButton != null) {
                m_OnCkickCloseButton();
            }
        }

        private enum eMode {
            Invalid = -1,
            Open,
            Main,
            AccountLinkOpen,
            AccountLinkProc,
            AccountLinkClose,
            Close
        }

        [Serializable]
        private struct ButtonStruct {
            public CustomButton m_Button;
            public Image m_ButtonTextImage;
            public Text m_ButtonText;
        }
    }
}
