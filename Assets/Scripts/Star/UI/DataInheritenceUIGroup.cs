﻿using Star.UI.Window;
using System;
using UnityEngine;

namespace Star.UI {
    public class DataInheritenceUIGroup : UIGroup {
        [SerializeField] private DataInheritenceWindowContent.eType m_InheritenceWindowType;
        [SerializeField] private DataInheritenceWindowContent m_Content;
        [SerializeField] private DataInheritenceSelectContnt m_DataInheritenceSelectWindow;
        [SerializeField] private CustomWindow m_DataInheritenceWindow;

        private Action m_RestartCallback;
        private eMode m_Mode = eMode.Invalid;

        public override void Open() {
            m_DataInheritenceSelectWindow.Setup(false);
            m_DataInheritenceSelectWindow.SetCallback(OnClickALButtonPlatform, OnClickALButtonUseIDAndPassword, OnClickCloseButton);
            SetMode(eMode.Open);
        }

        public override void Close() {
            if (m_CustomWindow == m_DataInheritenceWindow) {
                m_DataInheritenceWindow.Close();
                SetMode(eMode.Open);
            } else {
                base.Close();
                m_Mode = eMode.Close;
            }
        }

        public void SetOnClickButtonCallback(Action<int> callback) {
            if (m_Content != null) {
                m_Content.SetOnClickButtonCallback(callback);
            }
        }

        public void SetOnPlayerMoveSuccessCallback(Action callback) {
            m_RestartCallback = callback;
        }

        public override void Update() {
            base.Update();
            switch (m_Mode) {
                case eMode.Open:
                    if (!m_DataInheritenceSelectWindow.IsPlayingAnim()) {
                        SetMode(eMode.MainSelect);
                    }
                    break;
                case eMode.AccountLinkWait:
                    if (AccountLinker.GetState() != AccountLinker.eResult.Access) {
                        SetMode(eMode.AccountLinkEndDialog);
                    }
                    break;
            }
        }

        private void SetMode(eMode mode) {
            Debug.Log("IMODE : " + mode);
            m_Mode = mode;
            switch (mode) {
                case eMode.Open:
                    m_CustomWindow = m_DataInheritenceSelectWindow;
                    base.Open();
                    if (m_Content != null) {
                        m_Content.Setup();
                        m_Content.SetWindowType(m_InheritenceWindowType);
                    }
                    break;
                case eMode.AccountLinkNotifyDialog:
                    if (m_InheritenceWindowType == DataInheritenceWindowContent.eType.ExecuteWindow) {
                        eText_MessageDB title = eText_MessageDB.PlayerMoveConfirmTitle;
#if UNITY_IOS
						eText_MessageDB message = eText_MessageDB.PlayerMoveConfirm_iOS;
#else
                        eText_MessageDB message = eText_MessageDB.PlayerMoveConfirm_Android;
#endif
                        GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, title, message, OnClickNotifyConfirm);
                    } else if (m_InheritenceWindowType == DataInheritenceWindowContent.eType.ConfigurationWindow) {
                        eText_MessageDB title = eText_MessageDB.PlayerMoveSettingConfirm;
#if UNITY_IOS
						eText_MessageDB message = eText_MessageDB.PlayerMoveSettingConfirm_iOS;
#else
                        eText_MessageDB message = eText_MessageDB.PlayerMoveSettingConfirm_Android;
#endif
                        GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, title, message, OnClickNotifyConfirm);
                    }
                    break;
                case eMode.AccountLinkWait:
                    GameSystem.Inst.ConnectingIcon.Open();
                    AccountLinker.CheckCreate();
                    if (m_InheritenceWindowType == DataInheritenceWindowContent.eType.ConfigurationWindow) {
                        AccountLinker.SetAcountLinkData();
                    } else if (m_InheritenceWindowType == DataInheritenceWindowContent.eType.ExecuteWindow) {
                        AccountLinker.BuildUpAcountLinkPlayer();
                    }
                    break;
                case eMode.AccountLinkEndDialog:
                    AccountLinker.eResult state = AccountLinker.GetState();
                    if (state == AccountLinker.eResult.Success) {
                        m_RestartCallback();
                        m_DataInheritenceSelectWindow.Close();
                    } else if (state == AccountLinker.eResult.LogInError) {
#if UNITY_IOS
						string message = GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingResultError_iOS);
#else
                        string message = GameSystem.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingResultError_Android);
#endif
                        APIUtility.ShowErrorWindowOk(message, OnClickResultErrorConfirm);
                        SetMode(eMode.MainSelect);
                    } else if (state == AccountLinker.eResult.ComError) {
                        SetMode(eMode.MainSelect);
                    }
                    GameSystem.Inst.ConnectingIcon.Close();
                    break;
            }
        }

        public void OnClickALButtonPlatform() {
            if (m_Mode == eMode.MainSelect) {
                SetMode(eMode.AccountLinkNotifyDialog);
            }
        }

        public void OnClickALButtonUseIDAndPassword() {
            if (m_Mode == eMode.MainSelect) {
                m_DataInheritenceSelectWindow.Close();
                m_Content.Setup();
                m_Content.SetWindowType(m_InheritenceWindowType);
                m_DataInheritenceWindow.Open();
                m_CustomWindow = m_DataInheritenceWindow;
            }
        }

        public void OnClickCloseButton() {
            if (m_Mode == eMode.MainSelect || m_Mode == eMode.MainIDAndPass) {
                Close();
            }
        }

        public void OnClickNotifyConfirm(int idx) {
            if (idx == 0) {
                SetMode(eMode.AccountLinkWait);
            } else {
                SetMode(eMode.Open);
            }
        }

        public void OnClickResultSuccessConfirm(int idx) {
            SetMode(eMode.Close);
        }

        public void OnClickResultErrorConfirm() {
            SetMode(eMode.MainSelect);
        }

        public string GetIdString() {
            return m_Content != null ? m_Content.GetIdString() : null;
        }

        public string GetPasswordString() {
            return m_Content != null ? m_Content.GetPasswordString() : null;
        }

        public void SetIdString(string id) {
            if (m_Content != null) {
                m_Content.SetIdString(id);
            }
        }

        public void SetPasswordString(string password) {
            if (m_Content != null) {
                m_Content.SetPasswordString(password);
            }
        }

        public void SetDeadlineString(DateTime deadline) {
            if (m_Content != null) {
                string deadlineStr = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDeadlineValue, new object[]
                {
                    deadline.Year,
                    deadline.Month,
                    deadline.Day,
                    deadline.Hour,
                    deadline.Minute
                });
                m_Content.SetDeadlineString(deadlineStr);
            }
        }

        private enum eMode {
            Invalid = -1,
            Open,
            MainSelect,
            MainIDAndPass,
            AccountLinkNotifyDialog,
            AccountLinkWait,
            AccountLinkEndDialog,
            Close
        }
    }
}
