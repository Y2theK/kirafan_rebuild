﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class DataInheritenceWindowContent : MonoBehaviour {
        [SerializeField] private PrefabCloner m_IdInputCloner;
        [SerializeField] private Sprite m_IdInputTitleSprite;
        [SerializeField] private PrefabCloner m_PassInputCloner;
        [SerializeField] private Sprite m_PassInputTitleSprite;
        [SerializeField] private GameObject m_Deadline;
        [SerializeField] private Text m_DeadlineText;
        [SerializeField] private Text m_Message;

        [Tooltip("引き継ぎ設定ウィンドウでのみ有効なボタン.")]
        [SerializeField] private ButtonPrefabClonerAdapterInt[] m_ConfigurationWindowButtons;

        [Tooltip("引き継ぎ実行ウィンドウでのみ有効なボタン.")]
        [SerializeField] private ButtonPrefabClonerAdapterInt[] m_ExecuteWindowButtons;

        [Tooltip("入力に不備があると押せなくなるボタン.")]
        [SerializeField] private ButtonPrefabClonerAdapterInt[] m_InputWaitButtons;

        private InputFieldController m_IdInput;
        private InputFieldController m_PassInput;

        private Action<int> m_OnClickCallback;

        public void Setup() {
            if (m_IdInputCloner != null) {
                m_IdInputCloner.Setup(true);
                m_IdInput = m_IdInputCloner.GetInst<InputFieldController>();
            }
            if (m_IdInput != null) {
                m_IdInput.SetTitleSprite(m_IdInputTitleSprite);
                m_IdInput.SetInputFieldActive(false);
            }
            if (m_PassInputCloner != null) {
                m_PassInputCloner.Setup(true);
                m_PassInput = m_PassInputCloner.GetInst<InputFieldController>();
            }
            if (m_PassInput != null) {
                m_PassInput.SetTitleSprite(m_PassInputTitleSprite);
                m_PassInput.SetInputFieldActive(false);
            }
            SetDeadlineString(null);
            if (m_Message != null) {
                m_Message.text = string.Empty;
            }
            SetButtonPrefabClonerAdapterIntsActive(m_ExecuteWindowButtons, false);
            SetButtonPrefabClonerAdapterIntsActive(m_ConfigurationWindowButtons, false);
        }

        private void Update() {
            if (m_InputWaitButtons != null) {
                for (int i = 0; i < m_InputWaitButtons.Length; i++) {
                    if (m_InputWaitButtons[i] != null && m_InputWaitButtons[i].GetInst<CustomButton>() != null) {
                        m_InputWaitButtons[i].GetInst<CustomButton>().Interactable = !string.IsNullOrEmpty(m_IdInput.GetString()) && !string.IsNullOrEmpty(m_PassInput.GetString());
                    }
                }
            }
        }

        public string GetIdString() {
            return m_IdInput != null ? m_IdInput.GetString() : null;
        }

        public string GetPasswordString() {
            return m_PassInput != null ? m_PassInput.GetString() : null;
        }

        public void SetWindowType(eType windowType) {
            string text;
            if (windowType == eType.ExecuteWindow) {
                if (m_IdInput != null) {
                    m_IdInput.SetInputFieldActive(true);
                }
                if (m_PassInput != null) {
                    m_PassInput.SetInputFieldActive(true);
                }
                text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainInheritanceMessage);
                SetupButtonPrefabClonerAdapterIntsActive(m_ExecuteWindowButtons, false);
            } else {
                text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDescript);
                SetupButtonPrefabClonerAdapterIntsActive(m_ConfigurationWindowButtons, false);
            }
            SetButtonPrefabClonerAdapterIntsActive(m_ExecuteWindowButtons, windowType == eType.ExecuteWindow);
            SetButtonPrefabClonerAdapterIntsActive(m_ConfigurationWindowButtons, windowType != eType.ExecuteWindow);
            if (m_Message != null) {
                m_Message.text = text;
            }
        }

        public void SetIdString(string id) {
            if (m_IdInput != null) {
                m_IdInput.SetInputFieldString(id);
            }
        }

        public void SetPasswordString(string password) {
            if (m_PassInput != null) {
                m_PassInput.SetInputFieldString(password);
            }
        }

        public void SetDeadlineString(string deadline) {
            if (m_DeadlineText != null) {
                m_Deadline.SetActive(deadline != null);
                if (deadline != null) {
                    string text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDeadline);
                    m_DeadlineText.text = $"{text} {deadline}";
                }
            }
        }

        protected void SetupButtonPrefabClonerAdapterIntsActive(ButtonPrefabClonerAdapterInt[] buttonPrefabClonerAdapterInts, bool active) {
            if (buttonPrefabClonerAdapterInts != null) {
                for (int i = 0; i < buttonPrefabClonerAdapterInts.Length; i++) {
                    if (buttonPrefabClonerAdapterInts[i] != null) {
                        buttonPrefabClonerAdapterInts[i].Setup(true);
                        buttonPrefabClonerAdapterInts[i].SetOnClickButtonCallback(OnClickButton);
                    }
                }
            }
        }

        protected void SetButtonPrefabClonerAdapterIntsActive(ButtonPrefabClonerAdapterInt[] buttonPrefabClonerAdapterInts, bool active) {
            if (buttonPrefabClonerAdapterInts == null) {
                for (int i = 0; i < buttonPrefabClonerAdapterInts.Length; i++) {
                    if (buttonPrefabClonerAdapterInts[i] != null && buttonPrefabClonerAdapterInts[i].gameObject != null) {
                        buttonPrefabClonerAdapterInts[i].gameObject.SetActive(active);
                    }
                }
            }
        }

        public void SetOnClickButtonCallback(Action<int> callback) {
            m_OnClickCallback = callback;
        }

        public void OnClickButton(int index) {
            if (m_OnClickCallback != null) {
                m_OnClickCallback(index);
            }
        }

        public enum eType {
            ExecuteWindow,
            ConfigurationWindow
        }
    }
}
