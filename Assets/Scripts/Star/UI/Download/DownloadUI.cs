﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Download
{
	// Token: 0x02001019 RID: 4121
	[Token(Token = "0x2000AB8")]
	[StructLayout(3)]
	public class DownloadUI : MenuUIBase
	{
		// Token: 0x06004ED1 RID: 20177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485D")]
		[Address(RVA = "0x1014434C4", Offset = "0x14434C4", VA = "0x1014434C4")]
		public void Setup(bool isPlayMovie)
		{
		}

		// Token: 0x06004ED2 RID: 20178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485E")]
		[Address(RVA = "0x1014436C4", Offset = "0x14436C4", VA = "0x1014436C4")]
		private void Update()
		{
		}

		// Token: 0x06004ED3 RID: 20179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485F")]
		[Address(RVA = "0x101443AD0", Offset = "0x1443AD0", VA = "0x101443AD0")]
		private void ChangeStep(DownloadUI.eStep step)
		{
		}

		// Token: 0x06004ED4 RID: 20180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004860")]
		[Address(RVA = "0x101443DA0", Offset = "0x1443DA0", VA = "0x101443DA0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004ED5 RID: 20181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004861")]
		[Address(RVA = "0x101443E5C", Offset = "0x1443E5C", VA = "0x101443E5C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004ED6 RID: 20182 RVA: 0x0001B8E8 File Offset: 0x00019AE8
		[Token(Token = "0x6004862")]
		[Address(RVA = "0x101443F18", Offset = "0x1443F18", VA = "0x101443F18", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004ED7 RID: 20183 RVA: 0x0001B900 File Offset: 0x00019B00
		[Token(Token = "0x6004863")]
		[Address(RVA = "0x101443F28", Offset = "0x1443F28", VA = "0x101443F28")]
		public int GetLoadedSprNum()
		{
			return 0;
		}

		// Token: 0x06004ED8 RID: 20184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004864")]
		[Address(RVA = "0x101443F30", Offset = "0x1443F30", VA = "0x101443F30")]
		public void UpdateDownloadStatus(float progress)
		{
		}

		// Token: 0x06004ED9 RID: 20185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004865")]
		[Address(RVA = "0x1014440DC", Offset = "0x14440DC", VA = "0x1014440DC")]
		public void TouchDisplay()
		{
		}

		// Token: 0x06004EDA RID: 20186 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004866")]
		[Address(RVA = "0x101444158", Offset = "0x1444158", VA = "0x101444158")]
		[Attribute(Name = "IteratorStateMachineAttribute", RVA = "0x10013C98C", Offset = "0x13C98C")]
		private IEnumerator DiactivateVisibleOnPlayMovieObject(float waitTime)
		{
			return null;
		}

		// Token: 0x06004EDB RID: 20187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004867")]
		[Address(RVA = "0x10144421C", Offset = "0x144421C", VA = "0x10144421C")]
		public void SetDLSizeChecking(bool isDLSizeChecking)
		{
		}

		// Token: 0x06004EDC RID: 20188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004868")]
		[Address(RVA = "0x101443C4C", Offset = "0x1443C4C", VA = "0x101443C4C")]
		private void UpdateDLSizeChecking()
		{
		}

		// Token: 0x06004EDD RID: 20189 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004869")]
		[Address(RVA = "0x101444320", Offset = "0x1444320", VA = "0x101444320")]
		private string GetDLSizeCheckingString(int index)
		{
			return null;
		}

		// Token: 0x06004EDE RID: 20190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600486A")]
		[Address(RVA = "0x101444444", Offset = "0x1444444", VA = "0x101444444")]
		public DownloadUI()
		{
		}

		// Token: 0x0400612C RID: 24876
		[Token(Token = "0x400442D")]
		private const int SPRITE_MAX = 4;

		// Token: 0x0400612D RID: 24877
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400442E")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x0400612E RID: 24878
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400442F")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x0400612F RID: 24879
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004430")]
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x04006130 RID: 24880
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004431")]
		[SerializeField]
		private GameObject m_MovieObject;

		// Token: 0x04006131 RID: 24881
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004432")]
		[SerializeField]
		private GameObject m_InvisibleOnPlayMoive;

		// Token: 0x04006132 RID: 24882
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004433")]
		[SerializeField]
		private GameObject m_VisibleOnPlayMovie;

		// Token: 0x04006133 RID: 24883
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004434")]
		[SerializeField]
		private InfiniteScroll m_Scroll;

		// Token: 0x04006134 RID: 24884
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004435")]
		[SerializeField]
		private GameObject m_ScrObj;

		// Token: 0x04006135 RID: 24885
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004436")]
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04006136 RID: 24886
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004437")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04006137 RID: 24887
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004438")]
		private DownloadUI.eStep m_Step;

		// Token: 0x04006138 RID: 24888
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4004439")]
		private bool m_IsPlayMovie;

		// Token: 0x04006139 RID: 24889
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400443A")]
		private float m_Progress;

		// Token: 0x0400613A RID: 24890
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400443B")]
		private List<DownloadImageItemData> m_ItemList;

		// Token: 0x0400613B RID: 24891
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400443C")]
		private ResourceRequest m_ResReq;

		// Token: 0x0400613C RID: 24892
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400443D")]
		private int m_LoadIdx;

		// Token: 0x0400613D RID: 24893
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400443E")]
		private Coroutine m_Coroutine_DiactivateVisibleOnPlayMovieObject;

		// Token: 0x0400613E RID: 24894
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400443F")]
		private StringBuilder m_sb;

		// Token: 0x0400613F RID: 24895
		[Token(Token = "0x4004440")]
		private static readonly string[] FILE_DLSIZE_CHECKING_STRINGS;

		// Token: 0x04006140 RID: 24896
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004441")]
		private float m_DLSizeCheckCounter;

		// Token: 0x04006141 RID: 24897
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4004442")]
		private bool m_IsDLSizeChecking;

		// Token: 0x0200101A RID: 4122
		[Token(Token = "0x2001223")]
		private enum eStep
		{
			// Token: 0x04006143 RID: 24899
			[Token(Token = "0x4007037")]
			Hide,
			// Token: 0x04006144 RID: 24900
			[Token(Token = "0x4007038")]
			In,
			// Token: 0x04006145 RID: 24901
			[Token(Token = "0x4007039")]
			Idle,
			// Token: 0x04006146 RID: 24902
			[Token(Token = "0x400703A")]
			Out,
			// Token: 0x04006147 RID: 24903
			[Token(Token = "0x400703B")]
			Unload,
			// Token: 0x04006148 RID: 24904
			[Token(Token = "0x400703C")]
			End
		}
	}
}
