﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D82 RID: 3458
	[Token(Token = "0x2000942")]
	[StructLayout(3)]
	public class DownloadImageItem : InfiniteScrollItem
	{
		// Token: 0x06003FB9 RID: 16313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5E")]
		[Address(RVA = "0x1014447EC", Offset = "0x14447EC", VA = "0x1014447EC", Slot = "5")]
		public override void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06003FBA RID: 16314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5F")]
		[Address(RVA = "0x101444978", Offset = "0x1444978", VA = "0x101444978", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06003FBB RID: 16315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A60")]
		[Address(RVA = "0x1014449B8", Offset = "0x14449B8", VA = "0x1014449B8")]
		public DownloadImageItem()
		{
		}

		// Token: 0x04004F18 RID: 20248
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40037B9")]
		[SerializeField]
		private Image m_Image;
	}
}
