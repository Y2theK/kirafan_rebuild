﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D81 RID: 3457
	[Token(Token = "0x2000941")]
	[StructLayout(3)]
	public class DownloadImageItemData : InfiniteScrollItemData
	{
		// Token: 0x06003FB8 RID: 16312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5D")]
		[Address(RVA = "0x1014436BC", Offset = "0x14436BC", VA = "0x1014436BC")]
		public DownloadImageItemData()
		{
		}

		// Token: 0x04004F17 RID: 20247
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40037B8")]
		public Sprite m_Sprite;
	}
}
