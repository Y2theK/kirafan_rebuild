﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001095 RID: 4245
	[Token(Token = "0x2000B03")]
	[StructLayout(3)]
	public class AbilityInfoGroup : UIGroup
	{
		// Token: 0x060051E6 RID: 20966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B4F")]
		[Address(RVA = "0x1014449C0", Offset = "0x14449C0", VA = "0x1014449C0")]
		public void SetUp(UserAbilityData data)
		{
		}

		// Token: 0x060051E7 RID: 20967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B50")]
		[Address(RVA = "0x101444C38", Offset = "0x1444C38", VA = "0x101444C38")]
		public AbilityInfoGroup()
		{
		}

		// Token: 0x04006431 RID: 25649
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004660")]
		[SerializeField]
		private Transform m_parent;

		// Token: 0x04006432 RID: 25650
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004661")]
		[SerializeField]
		private GameObject m_ExSkillPrefab;

		// Token: 0x04006433 RID: 25651
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004662")]
		[SerializeField]
		private CustomButton m_BtnClose;

		// Token: 0x04006434 RID: 25652
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004663")]
		[SerializeField]
		private GameObject m_AbilityIconPrefab;
	}
}
