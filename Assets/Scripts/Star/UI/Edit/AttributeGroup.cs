﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001096 RID: 4246
	[Token(Token = "0x2000B04")]
	[StructLayout(3)]
	public class AttributeGroup : UIGroup
	{
		// Token: 0x060051E9 RID: 20969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B52")]
		[Address(RVA = "0x101444C4C", Offset = "0x1444C4C", VA = "0x101444C4C", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060051EA RID: 20970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B53")]
		[Address(RVA = "0x1014451D0", Offset = "0x14451D0", VA = "0x1014451D0")]
		public AttributeGroup()
		{
		}

		// Token: 0x04006435 RID: 25653
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004664")]
		[SerializeField]
		private Sprite[] m_ClassSpriteArray;

		// Token: 0x04006436 RID: 25654
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004665")]
		[SerializeField]
		private Sprite[] m_ElementSpriteArray;

		// Token: 0x04006437 RID: 25655
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004666")]
		[SerializeField]
		private Image[] m_ClassIcon;

		// Token: 0x04006438 RID: 25656
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004667")]
		[SerializeField]
		private Text[] m_ClassText;

		// Token: 0x04006439 RID: 25657
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004668")]
		[SerializeField]
		private Image[] m_ElementIcon;

		// Token: 0x0400643A RID: 25658
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004669")]
		[SerializeField]
		private Text[] m_ElementText;

		// Token: 0x0400643B RID: 25659
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400466A")]
		[SerializeField]
		private Text m_ClassLabel;

		// Token: 0x0400643C RID: 25660
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400466B")]
		[SerializeField]
		private Text m_ElementLabel;
	}
}
