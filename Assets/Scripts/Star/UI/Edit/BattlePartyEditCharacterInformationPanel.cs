﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010B9 RID: 4281
	[Token(Token = "0x2000B17")]
	[StructLayout(3)]
	public class BattlePartyEditCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x060052E9 RID: 21225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C4F")]
		[Address(RVA = "0x1014451D8", Offset = "0x14451D8", VA = "0x1014451D8", Slot = "10")]
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060052EA RID: 21226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C50")]
		[Address(RVA = "0x101445278", Offset = "0x1445278", VA = "0x101445278", Slot = "12")]
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060052EB RID: 21227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C51")]
		[Address(RVA = "0x101445464", Offset = "0x1445464", VA = "0x101445464")]
		public BattlePartyEditCharacterInformationPanel()
		{
		}
	}
}
