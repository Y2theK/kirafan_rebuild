﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001097 RID: 4247
	[Token(Token = "0x2000B05")]
	[StructLayout(3)]
	public class CharaCommonGroup : UIGroup
	{
		// Token: 0x060051EB RID: 20971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B54")]
		[Address(RVA = "0x10144546C", Offset = "0x144546C", VA = "0x10144546C")]
		public void Setup()
		{
		}

		// Token: 0x060051EC RID: 20972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B55")]
		[Address(RVA = "0x101445780", Offset = "0x1445780", VA = "0x101445780", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060051ED RID: 20973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B56")]
		[Address(RVA = "0x10144581C", Offset = "0x144581C", VA = "0x10144581C")]
		public void Destroy()
		{
		}

		// Token: 0x060051EE RID: 20974 RVA: 0x0001C170 File Offset: 0x0001A370
		[Token(Token = "0x6004B57")]
		[Address(RVA = "0x10144585C", Offset = "0x144585C", VA = "0x10144585C")]
		public CharaDetailUI.eCharaCommonCategoryType GetCurrentCategory()
		{
			return CharaDetailUI.eCharaCommonCategoryType.Status;
		}

		// Token: 0x060051EF RID: 20975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B58")]
		[Address(RVA = "0x10144588C", Offset = "0x144588C", VA = "0x10144588C")]
		public void SetCurrentCategory(CharaDetailUI.eCharaCommonCategoryType type)
		{
		}

		// Token: 0x060051F0 RID: 20976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B59")]
		[Address(RVA = "0x1014458C4", Offset = "0x14458C4", VA = "0x1014458C4")]
		public void SetActiveCategory(CharaDetailUI.eCharaCommonCategoryType type, bool isActive)
		{
		}

		// Token: 0x060051F1 RID: 20977 RVA: 0x0001C188 File Offset: 0x0001A388
		[Token(Token = "0x6004B5A")]
		[Address(RVA = "0x10144590C", Offset = "0x144590C", VA = "0x10144590C")]
		public bool GetInteractableCategory(CharaDetailUI.eCharaCommonCategoryType type)
		{
			return default(bool);
		}

		// Token: 0x060051F2 RID: 20978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B5B")]
		[Address(RVA = "0x101445944", Offset = "0x1445944", VA = "0x101445944")]
		public void SetInteractableCategory(CharaDetailUI.eCharaCommonCategoryType type, bool isActive)
		{
		}

		// Token: 0x060051F3 RID: 20979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B5C")]
		[Address(RVA = "0x10144598C", Offset = "0x144598C", VA = "0x10144598C")]
		public void SetEnableRadioButton(bool isEnable)
		{
		}

		// Token: 0x060051F4 RID: 20980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B5D")]
		[Address(RVA = "0x1014459C4", Offset = "0x14459C4", VA = "0x1014459C4")]
		public void SetEnableRadioButton(CharaDetailUI.eCharaCommonCategoryType type, bool isEnable)
		{
		}

		// Token: 0x060051F5 RID: 20981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B5E")]
		[Address(RVA = "0x1014457CC", Offset = "0x14457CC", VA = "0x1014457CC")]
		private void OnChangeIndex(int index)
		{
		}

		// Token: 0x060051F6 RID: 20982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B5F")]
		[Address(RVA = "0x101445A0C", Offset = "0x1445A0C", VA = "0x101445A0C")]
		public CharaCommonGroup()
		{
		}

		// Token: 0x0400643D RID: 25661
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400466C")]
		[SerializeField]
		private RadioButton m_RadioButton;

		// Token: 0x0400643E RID: 25662
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400466D")]
		public Action OnClickStatus;

		// Token: 0x0400643F RID: 25663
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400466E")]
		public Action OnClickUpgrade;

		// Token: 0x04006440 RID: 25664
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400466F")]
		public Action OnClickLimitBreak;

		// Token: 0x04006441 RID: 25665
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004670")]
		public Action OnClickEvolution;
	}
}
