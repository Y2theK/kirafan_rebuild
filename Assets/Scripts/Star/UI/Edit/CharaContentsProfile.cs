﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001098 RID: 4248
	[Token(Token = "0x2000B06")]
	[StructLayout(3)]
	public class CharaContentsProfile : MonoBehaviour
	{
		// Token: 0x060051F7 RID: 20983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B60")]
		[Address(RVA = "0x101445A14", Offset = "0x1445A14", VA = "0x101445A14")]
		public void Apply(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x060051F8 RID: 20984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B61")]
		[Address(RVA = "0x101445CBC", Offset = "0x1445CBC", VA = "0x101445CBC")]
		public void Destroy()
		{
		}

		// Token: 0x060051F9 RID: 20985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B62")]
		[Address(RVA = "0x101445D10", Offset = "0x1445D10", VA = "0x101445D10")]
		public CharaContentsProfile()
		{
		}

		// Token: 0x04006442 RID: 25666
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004671")]
		[SerializeField]
		private TitleImage m_TitleImage;

		// Token: 0x04006443 RID: 25667
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004672")]
		[SerializeField]
		private Text m_CV;

		// Token: 0x04006444 RID: 25668
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004673")]
		[SerializeField]
		private Text m_ProfileText;

		// Token: 0x04006445 RID: 25669
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004674")]
		[SerializeField]
		private SelectedCharaInfoScheduleDropStateInfo m_PresentInfo;
	}
}
