﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001099 RID: 4249
	[Token(Token = "0x2000B07")]
	[StructLayout(3)]
	public class CharaContentsSkill : MonoBehaviour
	{
		// Token: 0x060051FA RID: 20986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B63")]
		[Address(RVA = "0x101445D18", Offset = "0x1445D18", VA = "0x101445D18")]
		public void Apply(UserCharacterData userCharacterData, [Optional] UserWeaponData userWeaponData, [Optional] UserAbilityData userAbilityData, bool disableSkillExpGauge = false, int initialSkillIndex = -1)
		{
		}

		// Token: 0x060051FB RID: 20987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B64")]
		[Address(RVA = "0x10144777C", Offset = "0x144777C", VA = "0x10144777C")]
		public void Destroy()
		{
		}

		// Token: 0x060051FC RID: 20988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B65")]
		[Address(RVA = "0x101447960", Offset = "0x1447960", VA = "0x101447960")]
		public void Dispose()
		{
		}

		// Token: 0x060051FD RID: 20989 RVA: 0x0001C1A0 File Offset: 0x0001A3A0
		[Token(Token = "0x6004B66")]
		[Address(RVA = "0x101447984", Offset = "0x1447984", VA = "0x101447984")]
		public int GetCurrentSkillIndex()
		{
			return 0;
		}

		// Token: 0x060051FE RID: 20990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B67")]
		[Address(RVA = "0x101447218", Offset = "0x1447218", VA = "0x101447218")]
		private void OnClickSkillIcon(int index)
		{
		}

		// Token: 0x060051FF RID: 20991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B68")]
		[Address(RVA = "0x10144798C", Offset = "0x144798C", VA = "0x10144798C")]
		private void ApplySkillInfo(SkillIcon.eSkillDBType skillDBType, SkillLearnData skillLearnData, eElementType element, bool isAppealSkillMaxLevel)
		{
		}

		// Token: 0x06005200 RID: 20992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B69")]
		[Address(RVA = "0x10144696C", Offset = "0x144696C", VA = "0x10144696C")]
		private void ApplySkillIcon_UniqueSkill(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x06005201 RID: 20993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6A")]
		[Address(RVA = "0x1014465F8", Offset = "0x14465F8", VA = "0x1014465F8")]
		private void ApplySkillIcon_ClassSkill(UserCharacterData userCharacterData, int index)
		{
		}

		// Token: 0x06005202 RID: 20994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6B")]
		[Address(RVA = "0x1014463A0", Offset = "0x14463A0", VA = "0x1014463A0")]
		private void ApplySkillIcon_ActiveSkill(UserWeaponData userWeaponData)
		{
		}

		// Token: 0x06005203 RID: 20995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6C")]
		[Address(RVA = "0x101446BE8", Offset = "0x1446BE8", VA = "0x101446BE8")]
		private void ApplySkillIcon_PassiveSkill(UserWeaponData userWeaponData)
		{
		}

		// Token: 0x06005204 RID: 20996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6D")]
		[Address(RVA = "0x101446220", Offset = "0x1446220", VA = "0x101446220")]
		private void ApplySkillIcon_AbilitySphereSkill(UserAbilityData userAbilityData)
		{
		}

		// Token: 0x06005205 RID: 20997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6E")]
		[Address(RVA = "0x101446E68", Offset = "0x1446E68", VA = "0x101446E68")]
		private void ApplyWeaponInfo(UserWeaponData userWeaponData)
		{
		}

		// Token: 0x06005206 RID: 20998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B6F")]
		[Address(RVA = "0x101447E54", Offset = "0x1447E54", VA = "0x101447E54")]
		public CharaContentsSkill()
		{
		}

		// Token: 0x04006446 RID: 25670
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004675")]
		[SerializeField]
		private SelectedSkillIcon m_SelectedSkillIconPrefab;

		// Token: 0x04006447 RID: 25671
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004676")]
		[SerializeField]
		private SkillInfoGauge m_SkillInfo;

		// Token: 0x04006448 RID: 25672
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004677")]
		[SerializeField]
		private RectTransform m_SelectedSkillIconParent;

		// Token: 0x04006449 RID: 25673
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004678")]
		[SerializeField]
		private GameObject m_WeaponInfoOnGroup;

		// Token: 0x0400644A RID: 25674
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004679")]
		[SerializeField]
		private GameObject m_WeaponInfoOffGroup;

		// Token: 0x0400644B RID: 25675
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400467A")]
		[SerializeField]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x0400644C RID: 25676
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400467B")]
		[SerializeField]
		private Text m_WeaponName;

		// Token: 0x0400644D RID: 25677
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400467C")]
		[SerializeField]
		private Text m_WeaponLevel;

		// Token: 0x0400644E RID: 25678
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400467D")]
		[SerializeField]
		private Text m_WeaponCost;

		// Token: 0x0400644F RID: 25679
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400467E")]
		[SerializeField]
		private Text m_NoWeaponText;

		// Token: 0x04006450 RID: 25680
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400467F")]
		private int m_CurrentSkillIndex;

		// Token: 0x04006451 RID: 25681
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004680")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04006452 RID: 25682
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004681")]
		private UserWeaponData m_UserWeaponData;

		// Token: 0x04006453 RID: 25683
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004682")]
		private bool m_DisableSkillExpGauge;

		// Token: 0x04006454 RID: 25684
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004683")]
		private UserAbilityData m_UserAbilityData;

		// Token: 0x04006455 RID: 25685
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004684")]
		private List<SelectedSkillIcon> m_SelectedSkillIconList;

		// Token: 0x04006456 RID: 25686
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004685")]
		public Action<int> OnClickSkillIconCallback;

		// Token: 0x04006457 RID: 25687
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004686")]
		public Action<UserAbilityData> OnClickAbilityIconCallback;
	}
}
