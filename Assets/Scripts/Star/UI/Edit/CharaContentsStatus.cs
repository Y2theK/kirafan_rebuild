﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200109A RID: 4250
	[Token(Token = "0x2000B08")]
	[StructLayout(3)]
	public class CharaContentsStatus : MonoBehaviour
	{
		// Token: 0x06005207 RID: 20999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B70")]
		[Address(RVA = "0x101447ECC", Offset = "0x1447ECC", VA = "0x101447ECC")]
		public void Setup()
		{
		}

		// Token: 0x06005208 RID: 21000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B71")]
		[Address(RVA = "0x1014483D4", Offset = "0x14483D4", VA = "0x1014483D4")]
		public void Apply(int friendship, long friendshipExp, UserCharacterData userCharacterData, UserWeaponData userWeaponData, UserMasterOrbData userOrbData, UserAbilityData userAbilityData)
		{
		}

		// Token: 0x06005209 RID: 21001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B72")]
		[Address(RVA = "0x1014490F4", Offset = "0x14490F4", VA = "0x1014490F4")]
		public void Destroy()
		{
		}

		// Token: 0x0600520A RID: 21002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B73")]
		[Address(RVA = "0x101448F08", Offset = "0x1448F08", VA = "0x101448F08")]
		private void SetCostText(int charaCost, int weaponCost)
		{
		}

		// Token: 0x0600520B RID: 21003 RVA: 0x0001C1B8 File Offset: 0x0001A3B8
		[Token(Token = "0x6004B74")]
		[Address(RVA = "0x101449104", Offset = "0x1449104", VA = "0x101449104")]
		public bool GetCharacterToggleState()
		{
			return default(bool);
		}

		// Token: 0x0600520C RID: 21004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B75")]
		[Address(RVA = "0x101449140", Offset = "0x1449140", VA = "0x101449140")]
		public void SetCharacterToggleState(bool flag)
		{
		}

		// Token: 0x0600520D RID: 21005 RVA: 0x0001C1D0 File Offset: 0x0001A3D0
		[Token(Token = "0x6004B76")]
		[Address(RVA = "0x101449178", Offset = "0x1449178", VA = "0x101449178")]
		public bool GetEvolutionToggleState()
		{
			return default(bool);
		}

		// Token: 0x0600520E RID: 21006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B77")]
		[Address(RVA = "0x1014491C4", Offset = "0x14491C4", VA = "0x1014491C4")]
		public void SetEvolutionToggleState(bool flag)
		{
		}

		// Token: 0x0600520F RID: 21007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B78")]
		[Address(RVA = "0x1014491FC", Offset = "0x14491FC", VA = "0x1014491FC")]
		public void SetEvolutionToggleUndecided()
		{
		}

		// Token: 0x06005210 RID: 21008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B79")]
		[Address(RVA = "0x10144922C", Offset = "0x144922C", VA = "0x10144922C")]
		public void SetActiveCharacterToggleState(bool state, bool isActive)
		{
		}

		// Token: 0x06005211 RID: 21009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7A")]
		[Address(RVA = "0x101449344", Offset = "0x1449344", VA = "0x101449344")]
		public void SetActiveEvolutionToggleState(bool state, bool isActive)
		{
		}

		// Token: 0x06005212 RID: 21010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7B")]
		[Address(RVA = "0x10144945C", Offset = "0x144945C", VA = "0x10144945C")]
		public void SaveToggleState()
		{
		}

		// Token: 0x06005213 RID: 21011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7C")]
		[Address(RVA = "0x101449A2C", Offset = "0x1449A2C", VA = "0x101449A2C")]
		public void LoadToggleState()
		{
		}

		// Token: 0x06005214 RID: 21012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7D")]
		[Address(RVA = "0x101449ECC", Offset = "0x1449ECC", VA = "0x101449ECC")]
		public void OnClickStatusDetailButton()
		{
		}

		// Token: 0x06005215 RID: 21013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7E")]
		[Address(RVA = "0x101449ED8", Offset = "0x1449ED8", VA = "0x101449ED8")]
		public void OnClickInfoButton()
		{
		}

		// Token: 0x06005216 RID: 21014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B7F")]
		[Address(RVA = "0x101449EE4", Offset = "0x1449EE4", VA = "0x101449EE4")]
		private void OnChangeIndexCharacterSwitch(int index)
		{
		}

		// Token: 0x06005217 RID: 21015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B80")]
		[Address(RVA = "0x101449F44", Offset = "0x1449F44", VA = "0x101449F44")]
		private void OnChangeIndexEvolutionSwitch(int index)
		{
		}

		// Token: 0x06005218 RID: 21016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B81")]
		[Address(RVA = "0x101449FA4", Offset = "0x1449FA4", VA = "0x101449FA4")]
		public CharaContentsStatus()
		{
		}

		// Token: 0x04006458 RID: 25688
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004687")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04006459 RID: 25689
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004688")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x0400645A RID: 25690
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004689")]
		[SerializeField]
		private LevelDisplay m_LevelInfo;

		// Token: 0x0400645B RID: 25691
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400468A")]
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x0400645C RID: 25692
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400468B")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x0400645D RID: 25693
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400468C")]
		[SerializeField]
		private ArousalLvIcon m_ArousalIcon;

		// Token: 0x0400645E RID: 25694
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400468D")]
		[SerializeField]
		private ArousalLvGauge m_AroualExpGauge;

		// Token: 0x0400645F RID: 25695
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400468E")]
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x04006460 RID: 25696
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400468F")]
		[SerializeField]
		private CustomButton m_StatusDetailButton;

		// Token: 0x04006461 RID: 25697
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004690")]
		[SerializeField]
		private RadioButton m_CharacterToggle;

		// Token: 0x04006462 RID: 25698
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004691")]
		[SerializeField]
		private RadioButton m_EvolutionToggle;

		// Token: 0x04006463 RID: 25699
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004692")]
		private List<bool> m_SaveToggleState;

		// Token: 0x04006464 RID: 25700
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004693")]
		public Action OnClickStatusDetailCallback;

		// Token: 0x04006465 RID: 25701
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004694")]
		public Action OnClickInfoCallback;

		// Token: 0x04006466 RID: 25702
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004695")]
		public Action<int> OnClickCharacterCallback;

		// Token: 0x04006467 RID: 25703
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004696")]
		public Action<int> OnClickEvolutionCallback;
	}
}
