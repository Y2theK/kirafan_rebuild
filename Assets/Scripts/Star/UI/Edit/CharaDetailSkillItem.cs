﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200109B RID: 4251
	[Token(Token = "0x2000B09")]
	[StructLayout(3)]
	public class CharaDetailSkillItem : ScrollItemIcon
	{
		// Token: 0x06005219 RID: 21017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B82")]
		[Address(RVA = "0x10144A014", Offset = "0x144A014", VA = "0x10144A014", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600521A RID: 21018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B83")]
		[Address(RVA = "0x10144A410", Offset = "0x144A410", VA = "0x10144A410")]
		public CharaDetailSkillItem()
		{
		}

		// Token: 0x04006468 RID: 25704
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004697")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006469 RID: 25705
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004698")]
		[SerializeField]
		private Text m_SkillNameTextObj;

		// Token: 0x0400646A RID: 25706
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004699")]
		[SerializeField]
		private Text m_SkillLevelTextObj;

		// Token: 0x0400646B RID: 25707
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400469A")]
		[SerializeField]
		private Text m_DetailTextObj;
	}
}
