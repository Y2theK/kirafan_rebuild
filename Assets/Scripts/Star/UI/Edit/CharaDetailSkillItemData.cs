﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x0200109C RID: 4252
	[Token(Token = "0x2000B0A")]
	[StructLayout(3)]
	public class CharaDetailSkillItemData : ScrollItemData
	{
		// Token: 0x0600521B RID: 21019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B84")]
		[Address(RVA = "0x10144A418", Offset = "0x144A418", VA = "0x10144A418")]
		public CharaDetailSkillItemData()
		{
		}

		// Token: 0x0400646C RID: 25708
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400469B")]
		public int m_SkillID;

		// Token: 0x0400646D RID: 25709
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400469C")]
		public int m_Level;

		// Token: 0x0400646E RID: 25710
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400469D")]
		public int m_MaxLevel;
	}
}
