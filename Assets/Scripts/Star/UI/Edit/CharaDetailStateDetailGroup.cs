﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200109D RID: 4253
	[Token(Token = "0x2000B0B")]
	[StructLayout(3)]
	public class CharaDetailStateDetailGroup : UIGroup
	{
		// Token: 0x0600521C RID: 21020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B85")]
		[Address(RVA = "0x10144A444", Offset = "0x144A444", VA = "0x10144A444")]
		public void SetCharacter(CharaDetailUI.CharaDetailInfo detailInfo)
		{
		}

		// Token: 0x0600521D RID: 21021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B86")]
		[Address(RVA = "0x10144AAD0", Offset = "0x144AAD0", VA = "0x10144AAD0")]
		private void ApplyState(OutputCharaParam? beforeParam, [Optional] OutputCharaParam? afterParam)
		{
		}

		// Token: 0x0600521E RID: 21022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B87")]
		[Address(RVA = "0x10144B188", Offset = "0x144B188", VA = "0x10144B188")]
		public CharaDetailStateDetailGroup()
		{
		}

		// Token: 0x0400646F RID: 25711
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400469E")]
		[SerializeField]
		private CharaStatusPercent m_StatusFriendship;

		// Token: 0x04006470 RID: 25712
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400469F")]
		[SerializeField]
		private CharaStatusPercent m_StatusDisplayTown;

		// Token: 0x04006471 RID: 25713
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40046A0")]
		[SerializeField]
		private CharaStatus m_StatusDisplayWeapon;

		// Token: 0x04006472 RID: 25714
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40046A1")]
		[SerializeField]
		private CharaStatus m_StatusDisplayArousal;

		// Token: 0x04006473 RID: 25715
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40046A2")]
		[SerializeField]
		private CharaStatusPercent m_StatusDisplayOrb;

		// Token: 0x04006474 RID: 25716
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40046A3")]
		[SerializeField]
		private CharaStatus m_StatusDisplayAblitySphere;

		// Token: 0x04006475 RID: 25717
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40046A4")]
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x04006476 RID: 25718
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40046A5")]
		[SerializeField]
		private ArousalLvIcon m_IconArousalLv;
	}
}
