﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI.Edit
{
	// Token: 0x0200109E RID: 4254
	[Token(Token = "0x2000B0C")]
	[StructLayout(3)]
	public class CharaDetailUI : MenuUIBase
	{
		// Token: 0x0600521F RID: 21023 RVA: 0x0001C1E8 File Offset: 0x0001A3E8
		[Token(Token = "0x6004B88")]
		[Address(RVA = "0x10144B190", Offset = "0x144B190", VA = "0x10144B190")]
		private static SelectedCharaInfoCharacterViewController.eViewType ConvertToViewType(int index)
		{
			return SelectedCharaInfoCharacterViewController.eViewType.Begin;
		}

		// Token: 0x06005220 RID: 21024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B89")]
		[Address(RVA = "0x10144B1B0", Offset = "0x144B1B0", VA = "0x10144B1B0")]
		public void SetCategoryPermission(bool permitUpgrade, bool permitLimitBreak, bool permitEvolution)
		{
		}

		// Token: 0x06005221 RID: 21025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B8A")]
		[Address(RVA = "0x10144B2A8", Offset = "0x144B2A8", VA = "0x10144B2A8")]
		public void Setup(CharaDetailPlayer owner, CharaDetailUI.eCharaCommonCategoryType firstCategory, UserCharacterData userCharacterData, UserWeaponData userWeaponData, UserMasterOrbData userOrbData, UserAbilityData userAbilityData, CharaDetailUI.eMode mode = CharaDetailUI.eMode.Normal, bool disableVoiceTab = false, bool disableSkillExpGauge = false, CharaStatusGroup.eStatusCategoryType startTab = CharaStatusGroup.eStatusCategoryType.Status)
		{
		}

		// Token: 0x06005222 RID: 21026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B8B")]
		[Address(RVA = "0x10144C48C", Offset = "0x144C48C", VA = "0x10144C48C")]
		private void Apply(UserCharacterData userCharacterData, UserWeaponData userWeaponData, CharaDetailUI.eMode mode, int? friendship, long? friendshipExp, bool disableSkillExpGauge, CharaStatusGroup.eStatusCategoryType startTab)
		{
		}

		// Token: 0x06005223 RID: 21027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B8C")]
		[Address(RVA = "0x10144D5D8", Offset = "0x144D5D8", VA = "0x10144D5D8", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06005224 RID: 21028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B8D")]
		[Address(RVA = "0x10144D868", Offset = "0x144D868", VA = "0x10144D868")]
		private void OnDestroy()
		{
		}

		// Token: 0x06005225 RID: 21029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B8E")]
		[Address(RVA = "0x10144D874", Offset = "0x144D874", VA = "0x10144D874")]
		private void Update()
		{
		}

		// Token: 0x06005226 RID: 21030 RVA: 0x0001C200 File Offset: 0x0001A400
		[Token(Token = "0x6004B8F")]
		[Address(RVA = "0x10144F008", Offset = "0x144F008", VA = "0x10144F008")]
		private bool UpdateSubGroup()
		{
			return default(bool);
		}

		// Token: 0x06005227 RID: 21031 RVA: 0x0001C218 File Offset: 0x0001A418
		[Token(Token = "0x6004B90")]
		[Address(RVA = "0x10144F378", Offset = "0x144F378", VA = "0x10144F378")]
		private bool UpdateEffectUpgrade()
		{
			return default(bool);
		}

		// Token: 0x06005228 RID: 21032 RVA: 0x0001C230 File Offset: 0x0001A430
		[Token(Token = "0x6004B91")]
		[Address(RVA = "0x10144F518", Offset = "0x144F518", VA = "0x10144F518")]
		private bool UpdateEffectLimitBreak()
		{
			return default(bool);
		}

		// Token: 0x06005229 RID: 21033 RVA: 0x0001C248 File Offset: 0x0001A448
		[Token(Token = "0x6004B92")]
		[Address(RVA = "0x10144F6D4", Offset = "0x144F6D4", VA = "0x10144F6D4")]
		private bool UpdateEffectEvolution()
		{
			return default(bool);
		}

		// Token: 0x0600522A RID: 21034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B93")]
		[Address(RVA = "0x10144FE5C", Offset = "0x144FE5C", VA = "0x10144FE5C")]
		private void PlayUpgradeExpGauge()
		{
		}

		// Token: 0x0600522B RID: 21035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B94")]
		[Address(RVA = "0x10144BFB8", Offset = "0x144BFB8", VA = "0x10144BFB8")]
		public void DispEvolutionLevel(bool isActive)
		{
		}

		// Token: 0x0600522C RID: 21036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B95")]
		[Address(RVA = "0x10145017C", Offset = "0x145017C", VA = "0x10145017C")]
		public void EvolutionFadeOut(float startAlpha, float endAlpha, float sec)
		{
		}

		// Token: 0x0600522D RID: 21037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B96")]
		[Address(RVA = "0x101450190", Offset = "0x1450190", VA = "0x101450190")]
		public void EvolutionFadeOutUpdate()
		{
		}

		// Token: 0x0600522E RID: 21038 RVA: 0x0001C260 File Offset: 0x0001A460
		[Token(Token = "0x6004B97")]
		[Address(RVA = "0x10145027C", Offset = "0x145027C", VA = "0x10145027C")]
		public bool EvolutionFadeOutIsFinish()
		{
			return default(bool);
		}

		// Token: 0x0600522F RID: 21039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B98")]
		[Address(RVA = "0x10145028C", Offset = "0x145028C", VA = "0x10145028C")]
		public void ReplaceCardIllust()
		{
		}

		// Token: 0x06005230 RID: 21040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B99")]
		[Address(RVA = "0x10144FD04", Offset = "0x144FD04", VA = "0x10144FD04")]
		private void RefreshUI()
		{
		}

		// Token: 0x06005231 RID: 21041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9A")]
		[Address(RVA = "0x1014506BC", Offset = "0x14506BC", VA = "0x1014506BC")]
		private void RefreshRadioButton(CharaDetailUI.eCharaCommonCategoryType currentCategory, bool activateOnly = false)
		{
		}

		// Token: 0x06005232 RID: 21042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9B")]
		[Address(RVA = "0x10144E2FC", Offset = "0x144E2FC", VA = "0x10144E2FC")]
		private void ChangeStep(CharaDetailUI.eStep step)
		{
		}

		// Token: 0x06005233 RID: 21043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9C")]
		[Address(RVA = "0x101450B5C", Offset = "0x1450B5C", VA = "0x101450B5C", Slot = "9")]
		public override void SetEnableInput(bool flg)
		{
		}

		// Token: 0x06005234 RID: 21044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9D")]
		[Address(RVA = "0x101450C7C", Offset = "0x1450C7C", VA = "0x101450C7C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06005235 RID: 21045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9E")]
		[Address(RVA = "0x101450D38", Offset = "0x1450D38", VA = "0x101450D38", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06005236 RID: 21046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B9F")]
		[Address(RVA = "0x10144E118", Offset = "0x144E118", VA = "0x10144E118")]
		private void PlayInUI()
		{
		}

		// Token: 0x06005237 RID: 21047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BA0")]
		[Address(RVA = "0x10144FC5C", Offset = "0x144FC5C", VA = "0x10144FC5C")]
		private void OpenSubGroup(CharaDetailUI.eCharaCommonCategoryType category)
		{
		}

		// Token: 0x06005238 RID: 21048 RVA: 0x0001C278 File Offset: 0x0001A478
		[Token(Token = "0x6004BA1")]
		[Address(RVA = "0x10144E238", Offset = "0x144E238", VA = "0x10144E238")]
		private bool IsDonePlayInSubGroup(CharaDetailUI.eCharaCommonCategoryType category)
		{
			return default(bool);
		}

		// Token: 0x06005239 RID: 21049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BA2")]
		[Address(RVA = "0x10144FBB4", Offset = "0x144FBB4", VA = "0x10144FBB4")]
		private void CloseSubGroup(CharaDetailUI.eCharaCommonCategoryType category)
		{
		}

		// Token: 0x0600523A RID: 21050 RVA: 0x0001C290 File Offset: 0x0001A490
		[Token(Token = "0x6004BA3")]
		[Address(RVA = "0x10144FABC", Offset = "0x144FABC", VA = "0x10144FABC")]
		private bool IsDonePlayOutSubGroup(CharaDetailUI.eCharaCommonCategoryType category)
		{
			return default(bool);
		}

		// Token: 0x0600523B RID: 21051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BA4")]
		[Address(RVA = "0x101450AB8", Offset = "0x1450AB8", VA = "0x101450AB8")]
		private void SetEnableInputSubGroup(CharaDetailUI.eCharaCommonCategoryType category)
		{
		}

		// Token: 0x0600523C RID: 21052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BA5")]
		[Address(RVA = "0x101450EFC", Offset = "0x1450EFC", VA = "0x101450EFC")]
		public void CompleteUnlock()
		{
		}

		// Token: 0x0600523D RID: 21053 RVA: 0x0001C2A8 File Offset: 0x0001A4A8
		[Token(Token = "0x6004BA6")]
		[Address(RVA = "0x101450F04", Offset = "0x1450F04", VA = "0x101450F04", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600523E RID: 21054 RVA: 0x0001C2C0 File Offset: 0x0001A4C0
		[Token(Token = "0x6004BA7")]
		[Address(RVA = "0x101450F14", Offset = "0x1450F14", VA = "0x101450F14")]
		public bool IsWaitingUnlock()
		{
			return default(bool);
		}

		// Token: 0x0600523F RID: 21055 RVA: 0x0001C2D8 File Offset: 0x0001A4D8
		[Token(Token = "0x6004BA8")]
		[Address(RVA = "0x101450F28", Offset = "0x1450F28", VA = "0x101450F28")]
		public CharaDetailUI.eStep GetStep()
		{
			return CharaDetailUI.eStep.Hide;
		}

		// Token: 0x06005240 RID: 21056 RVA: 0x0001C2F0 File Offset: 0x0001A4F0
		[Token(Token = "0x6004BA9")]
		[Address(RVA = "0x10144D2A8", Offset = "0x144D2A8", VA = "0x10144D2A8")]
		public SelectedCharaInfoCharacterViewController.eViewType GetActiveViewType()
		{
			return SelectedCharaInfoCharacterViewController.eViewType.Begin;
		}

		// Token: 0x06005241 RID: 21057 RVA: 0x0001C308 File Offset: 0x0001A508
		[Token(Token = "0x6004BAA")]
		[Address(RVA = "0x101450F5C", Offset = "0x1450F5C", VA = "0x101450F5C")]
		public CharaDetailUI.eCharaCommonCategoryType GetCurrentCategory()
		{
			return CharaDetailUI.eCharaCommonCategoryType.Status;
		}

		// Token: 0x06005242 RID: 21058 RVA: 0x0001C320 File Offset: 0x0001A520
		[Token(Token = "0x6004BAB")]
		[Address(RVA = "0x10144D2DC", Offset = "0x144D2DC", VA = "0x10144D2DC")]
		private int GetDisplayCharacterID()
		{
			return 0;
		}

		// Token: 0x06005243 RID: 21059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BAC")]
		[Address(RVA = "0x101450FB4", Offset = "0x1450FB4", VA = "0x101450FB4")]
		private void OpenAbilityInfo(UserAbilityData data)
		{
		}

		// Token: 0x06005244 RID: 21060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BAD")]
		[Address(RVA = "0x101451014", Offset = "0x1451014", VA = "0x101451014")]
		private void OpenUpgradeConfirmWindow()
		{
		}

		// Token: 0x06005245 RID: 21061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BAE")]
		[Address(RVA = "0x101451884", Offset = "0x1451884", VA = "0x101451884")]
		private void OnClickDecideButtonCallback()
		{
		}

		// Token: 0x06005246 RID: 21062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BAF")]
		[Address(RVA = "0x101451A9C", Offset = "0x1451A9C", VA = "0x101451A9C")]
		public void OnClickViewTabButton(int index)
		{
		}

		// Token: 0x06005247 RID: 21063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB0")]
		[Address(RVA = "0x101451BDC", Offset = "0x1451BDC", VA = "0x101451BDC")]
		public void OnClickSwitchEvolutionButton(int index)
		{
		}

		// Token: 0x06005248 RID: 21064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB1")]
		[Address(RVA = "0x101451D08", Offset = "0x1451D08", VA = "0x101451D08")]
		private void OnFinishInCallback()
		{
		}

		// Token: 0x06005249 RID: 21065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB2")]
		[Address(RVA = "0x101451DA4", Offset = "0x1451DA4", VA = "0x101451DA4")]
		public void OnClickOpenStateDetailCallback()
		{
		}

		// Token: 0x0600524A RID: 21066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB3")]
		[Address(RVA = "0x101451DAC", Offset = "0x1451DAC", VA = "0x101451DAC")]
		public void OnClickCloseStateDetailCallback()
		{
		}

		// Token: 0x0600524B RID: 21067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB4")]
		[Address(RVA = "0x101451DE0", Offset = "0x1451DE0", VA = "0x101451DE0")]
		public void OnClickInfoButtonCallback()
		{
		}

		// Token: 0x0600524C RID: 21068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB5")]
		[Address(RVA = "0x101451E14", Offset = "0x1451E14", VA = "0x101451E14")]
		public void OnClickCloseAttributeWindow()
		{
		}

		// Token: 0x0600524D RID: 21069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB6")]
		[Address(RVA = "0x101451E48", Offset = "0x1451E48", VA = "0x101451E48")]
		public void OnClickIllustCallBack()
		{
		}

		// Token: 0x0600524E RID: 21070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB7")]
		[Address(RVA = "0x101451E74", Offset = "0x1451E74", VA = "0x101451E74")]
		public void OnClickScreen()
		{
		}

		// Token: 0x0600524F RID: 21071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB8")]
		[Address(RVA = "0x101451E80", Offset = "0x1451E80", VA = "0x101451E80")]
		private void OnClickCategory_Status()
		{
		}

		// Token: 0x06005250 RID: 21072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BB9")]
		[Address(RVA = "0x101451ED0", Offset = "0x1451ED0", VA = "0x101451ED0")]
		private void OnClickCategory_Upgrade()
		{
		}

		// Token: 0x06005251 RID: 21073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBA")]
		[Address(RVA = "0x101451F20", Offset = "0x1451F20", VA = "0x101451F20")]
		private void OnClickCategory_LimitBreak()
		{
		}

		// Token: 0x06005252 RID: 21074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBB")]
		[Address(RVA = "0x101451F70", Offset = "0x1451F70", VA = "0x101451F70")]
		private void OnClickCategory_Evolution()
		{
		}

		// Token: 0x06005253 RID: 21075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBC")]
		[Address(RVA = "0x101451FC0", Offset = "0x1451FC0", VA = "0x101451FC0")]
		private void OnResponse_Upgrade(ResultCode resultCode, int successLevel)
		{
		}

		// Token: 0x06005254 RID: 21076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBD")]
		[Address(RVA = "0x101451FEC", Offset = "0x1451FEC", VA = "0x101451FEC")]
		private void OnResponse_LimitBreak(ResultCode resultCode, int limitBreakBefore, int limitBreakAfter)
		{
		}

		// Token: 0x06005255 RID: 21077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBE")]
		[Address(RVA = "0x10145201C", Offset = "0x145201C", VA = "0x10145201C")]
		private void OnResponse_Evolution(ResultCode resultCode, CharaDetailUI.LevelInfo beforeLevelInfo, CharaDetailUI.LevelInfo afterLevelInfo)
		{
		}

		// Token: 0x06005256 RID: 21078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BBF")]
		[Address(RVA = "0x101452048", Offset = "0x1452048", VA = "0x101452048")]
		public CharaDetailUI()
		{
		}

		// Token: 0x04006477 RID: 25719
		[Token(Token = "0x40046A6")]
		private const float ILLUST_VIEW_FADETIME_IN = 0.1f;

		// Token: 0x04006478 RID: 25720
		[Token(Token = "0x40046A7")]
		private const float ILLUST_VIEW_FADETIME_OUT = 0.1f;

		// Token: 0x04006479 RID: 25721
		[Token(Token = "0x40046A8")]
		private const float ILLUST_VIEW_FADETIME_IN_RARE = 0.1f;

		// Token: 0x0400647A RID: 25722
		[Token(Token = "0x40046A9")]
		private const float ILLUST_VIEW_FADETIME_OUT_RARE = 0.1f;

		// Token: 0x0400647B RID: 25723
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40046AA")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FD98", Offset = "0x12FD98")]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400647C RID: 25724
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40046AB")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FDE4", Offset = "0x12FDE4")]
		private UIGroup[] m_MainGroups;

		// Token: 0x0400647D RID: 25725
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40046AC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FE30", Offset = "0x12FE30")]
		private UIGroup[] m_SubGroups;

		// Token: 0x0400647E RID: 25726
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40046AD")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FE7C", Offset = "0x12FE7C")]
		private CharaDetailStateDetailGroup m_StateDetailGroup;

		// Token: 0x0400647F RID: 25727
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40046AE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FEC8", Offset = "0x12FEC8")]
		private AttributeGroup m_AttributeGroup;

		// Token: 0x04006480 RID: 25728
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40046AF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FF14", Offset = "0x12FF14")]
		private UpgradeConfirmGroup m_UpgradeConfirmGroup;

		// Token: 0x04006481 RID: 25729
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40046B0")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FF60", Offset = "0x12FF60")]
		private UIGroup m_IllustViewGroup;

		// Token: 0x04006482 RID: 25730
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40046B1")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FFAC", Offset = "0x12FFAC")]
		private GameObject m_MenuObject;

		// Token: 0x04006483 RID: 25731
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40046B2")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FFF8", Offset = "0x12FFF8")]
		private CharaIllust m_ViewIllust;

		// Token: 0x04006484 RID: 25732
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40046B3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130044", Offset = "0x130044")]
		private Image m_IllustBG;

		// Token: 0x04006485 RID: 25733
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40046B4")]
		[SerializeField]
		private CharaCommonGroup m_CharaCommonGroup;

		// Token: 0x04006486 RID: 25734
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40046B5")]
		[SerializeField]
		private CharaStatusGroup m_CharaStatusGroup;

		// Token: 0x04006487 RID: 25735
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40046B6")]
		[SerializeField]
		private CharaUpgradeGroup m_CharaUpgradeGroup;

		// Token: 0x04006488 RID: 25736
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40046B7")]
		[SerializeField]
		private CharaLimitBreakGroup m_CharaLimitBreakGroup;

		// Token: 0x04006489 RID: 25737
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40046B8")]
		[SerializeField]
		private CharaEvolutionGroup m_CharaEvolutionGroup;

		// Token: 0x0400648A RID: 25738
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40046B9")]
		[SerializeField]
		private AbilityInfoGroup m_AbilityInfoGroup;

		// Token: 0x0400648B RID: 25739
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40046BA")]
		[SerializeField]
		private SelectedCharaInfoCharacterViewController m_CharaViewController;

		// Token: 0x0400648C RID: 25740
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40046BB")]
		[SerializeField]
		private GameObject m_TouchScreen;

		// Token: 0x0400648D RID: 25741
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40046BC")]
		[SerializeField]
		private GameObject m_EvolutionLevelRoot;

		// Token: 0x0400648E RID: 25742
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40046BD")]
		[SerializeField]
		private CanvasGroup m_EvolutionLevelCanvasGroup;

		// Token: 0x0400648F RID: 25743
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40046BE")]
		[SerializeField]
		private LevelDisplay m_EvolutionLevelDisplayBefore;

		// Token: 0x04006490 RID: 25744
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40046BF")]
		[SerializeField]
		private LevelDisplay m_EvolutionLevelDisplayAfter;

		// Token: 0x04006491 RID: 25745
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40046C0")]
		private CharaDetailPlayer m_Owner;

		// Token: 0x04006492 RID: 25746
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40046C1")]
		private CharaDetailUI.eStep m_Step;

		// Token: 0x04006493 RID: 25747
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x40046C2")]
		private CharaDetailUI.eSubGroupStep m_SubStep;

		// Token: 0x04006494 RID: 25748
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40046C3")]
		private CharaDetailUI.eEffectStep m_EffectStep;

		// Token: 0x04006495 RID: 25749
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x40046C4")]
		private CharaDetailUI.EnhanceEffectType m_EnhanceEffectType;

		// Token: 0x04006496 RID: 25750
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x40046C5")]
		private EnhanceUpgrade m_EnhanceUpgrade;

		// Token: 0x04006497 RID: 25751
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x40046C6")]
		private EnhanceLimitBreak m_EnhanceLimitBreak;

		// Token: 0x04006498 RID: 25752
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x40046C7")]
		private EnhanceEvolution m_EnhanceEvolution;

		// Token: 0x04006499 RID: 25753
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x40046C8")]
		private CharaDetailUI.eCharaCommonCategoryType m_SubGroupCategoryCurrent;

		// Token: 0x0400649A RID: 25754
		[Cpp2IlInjected.FieldOffset(Offset = "0x124")]
		[Token(Token = "0x40046C9")]
		private CharaDetailUI.eCharaCommonCategoryType m_SubGroupCategoryNext;

		// Token: 0x0400649B RID: 25755
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x40046CA")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x0400649C RID: 25756
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x40046CB")]
		private UserWeaponData m_UserWeaponData;

		// Token: 0x0400649D RID: 25757
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x40046CC")]
		private UserMasterOrbData m_UserOrbData;

		// Token: 0x0400649E RID: 25758
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x40046CD")]
		private UserAbilityData m_UserAbilityData;

		// Token: 0x0400649F RID: 25759
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x40046CE")]
		private SpriteHandler m_CharacterIllustBefore;

		// Token: 0x040064A0 RID: 25760
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x40046CF")]
		private SpriteHandler m_CharacterIllustAfter;

		// Token: 0x040064A1 RID: 25761
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x40046D0")]
		private bool m_IsRareIllust;

		// Token: 0x040064A2 RID: 25762
		[Cpp2IlInjected.FieldOffset(Offset = "0x159")]
		[Token(Token = "0x40046D1")]
		private bool m_ChangeCardIllustView;

		// Token: 0x040064A3 RID: 25763
		[Cpp2IlInjected.FieldOffset(Offset = "0x15C")]
		[Token(Token = "0x40046D2")]
		private int m_Friendship;

		// Token: 0x040064A4 RID: 25764
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x40046D3")]
		private long m_FriendshipExp;

		// Token: 0x040064A5 RID: 25765
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x40046D4")]
		private int[] m_CharaEvolutionSwitchID;

		// Token: 0x040064A6 RID: 25766
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x40046D5")]
		private bool[] m_CategoryPermission;

		// Token: 0x040064A7 RID: 25767
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x40046D6")]
		private int m_UpgradeSuccessLevel;

		// Token: 0x040064A8 RID: 25768
		[Cpp2IlInjected.FieldOffset(Offset = "0x17C")]
		[Token(Token = "0x40046D7")]
		private int m_LimitBreakBefore;

		// Token: 0x040064A9 RID: 25769
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x40046D8")]
		private int m_LimitBreakAfter;

		// Token: 0x040064AA RID: 25770
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x40046D9")]
		private CharaDetailUI.LevelInfo m_EvolutionLevelInfoBefore;

		// Token: 0x040064AB RID: 25771
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x40046DA")]
		private CharaDetailUI.LevelInfo m_EvolutionLevelInfoAfter;

		// Token: 0x040064AC RID: 25772
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x40046DB")]
		private float m_EvolutionFadeElapsed;

		// Token: 0x040064AD RID: 25773
		[Cpp2IlInjected.FieldOffset(Offset = "0x19C")]
		[Token(Token = "0x40046DC")]
		private float m_EvolutionFadeElapsedMax;

		// Token: 0x040064AE RID: 25774
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x40046DD")]
		private float m_EvolutionFadeStartAlpha;

		// Token: 0x040064AF RID: 25775
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A4")]
		[Token(Token = "0x40046DE")]
		private float m_EvolutionFadeEndAlpha;

		// Token: 0x040064B0 RID: 25776
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x40046DF")]
		public Action OnStartViewIllust;

		// Token: 0x040064B1 RID: 25777
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x40046E0")]
		public Action OnEndViewIllust;

		// Token: 0x040064B2 RID: 25778
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x40046E1")]
		public Action OnClickScreenCallback;

		// Token: 0x0200109F RID: 4255
		[Token(Token = "0x2001253")]
		public enum eCharaCommonCategoryType
		{
			// Token: 0x040064B4 RID: 25780
			[Token(Token = "0x4007106")]
			Status,
			// Token: 0x040064B5 RID: 25781
			[Token(Token = "0x4007107")]
			Upgrade,
			// Token: 0x040064B6 RID: 25782
			[Token(Token = "0x4007108")]
			LimitBreak,
			// Token: 0x040064B7 RID: 25783
			[Token(Token = "0x4007109")]
			Evolution,
			// Token: 0x040064B8 RID: 25784
			[Token(Token = "0x400710A")]
			Num
		}

		// Token: 0x020010A0 RID: 4256
		[Token(Token = "0x2001254")]
		public enum eStep
		{
			// Token: 0x040064BA RID: 25786
			[Token(Token = "0x400710C")]
			Hide,
			// Token: 0x040064BB RID: 25787
			[Token(Token = "0x400710D")]
			BGIn,
			// Token: 0x040064BC RID: 25788
			[Token(Token = "0x400710E")]
			In,
			// Token: 0x040064BD RID: 25789
			[Token(Token = "0x400710F")]
			Idle,
			// Token: 0x040064BE RID: 25790
			[Token(Token = "0x4007110")]
			IdleSubGroup,
			// Token: 0x040064BF RID: 25791
			[Token(Token = "0x4007111")]
			UpdateEffectUpgrade,
			// Token: 0x040064C0 RID: 25792
			[Token(Token = "0x4007112")]
			UpdateEffectLimitBreak,
			// Token: 0x040064C1 RID: 25793
			[Token(Token = "0x4007113")]
			UpdateEffectEvolution,
			// Token: 0x040064C2 RID: 25794
			[Token(Token = "0x4007114")]
			WaitUnlock_Upgrade,
			// Token: 0x040064C3 RID: 25795
			[Token(Token = "0x4007115")]
			WaitUnlock_LimitBreak,
			// Token: 0x040064C4 RID: 25796
			[Token(Token = "0x4007116")]
			WaitUnlock_Evolution,
			// Token: 0x040064C5 RID: 25797
			[Token(Token = "0x4007117")]
			Out,
			// Token: 0x040064C6 RID: 25798
			[Token(Token = "0x4007118")]
			End,
			// Token: 0x040064C7 RID: 25799
			[Token(Token = "0x4007119")]
			StateDetail,
			// Token: 0x040064C8 RID: 25800
			[Token(Token = "0x400711A")]
			IllustViewIn,
			// Token: 0x040064C9 RID: 25801
			[Token(Token = "0x400711B")]
			IllustView,
			// Token: 0x040064CA RID: 25802
			[Token(Token = "0x400711C")]
			IllustViewLoad,
			// Token: 0x040064CB RID: 25803
			[Token(Token = "0x400711D")]
			IllustViewRotateFadeOut,
			// Token: 0x040064CC RID: 25804
			[Token(Token = "0x400711E")]
			IllustViewRotateFadeIn,
			// Token: 0x040064CD RID: 25805
			[Token(Token = "0x400711F")]
			IllustViewVertical,
			// Token: 0x040064CE RID: 25806
			[Token(Token = "0x4007120")]
			IllustViewOut
		}

		// Token: 0x020010A1 RID: 4257
		[Token(Token = "0x2001255")]
		private enum eSubGroupStep
		{
			// Token: 0x040064D0 RID: 25808
			[Token(Token = "0x4007122")]
			None,
			// Token: 0x040064D1 RID: 25809
			[Token(Token = "0x4007123")]
			StartCloseCurrent,
			// Token: 0x040064D2 RID: 25810
			[Token(Token = "0x4007124")]
			WaitCloseCurrent,
			// Token: 0x040064D3 RID: 25811
			[Token(Token = "0x4007125")]
			StartOpenNext,
			// Token: 0x040064D4 RID: 25812
			[Token(Token = "0x4007126")]
			WaitOpenNext,
			// Token: 0x040064D5 RID: 25813
			[Token(Token = "0x4007127")]
			End
		}

		// Token: 0x020010A2 RID: 4258
		[Token(Token = "0x2001256")]
		private enum eEffectStep
		{
			// Token: 0x040064D7 RID: 25815
			[Token(Token = "0x4007129")]
			None,
			// Token: 0x040064D8 RID: 25816
			[Token(Token = "0x400712A")]
			Load,
			// Token: 0x040064D9 RID: 25817
			[Token(Token = "0x400712B")]
			WaitResource,
			// Token: 0x040064DA RID: 25818
			[Token(Token = "0x400712C")]
			Wait,
			// Token: 0x040064DB RID: 25819
			[Token(Token = "0x400712D")]
			End
		}

		// Token: 0x020010A3 RID: 4259
		[Token(Token = "0x2001257")]
		private enum EnhanceEffectType
		{
			// Token: 0x040064DD RID: 25821
			[Token(Token = "0x400712F")]
			None,
			// Token: 0x040064DE RID: 25822
			[Token(Token = "0x4007130")]
			Upgrade,
			// Token: 0x040064DF RID: 25823
			[Token(Token = "0x4007131")]
			LimitBreak,
			// Token: 0x040064E0 RID: 25824
			[Token(Token = "0x4007132")]
			Evolution
		}

		// Token: 0x020010A4 RID: 4260
		[Token(Token = "0x2001258")]
		public enum eMode
		{
			// Token: 0x040064E2 RID: 25826
			[Token(Token = "0x4007134")]
			Normal,
			// Token: 0x040064E3 RID: 25827
			[Token(Token = "0x4007135")]
			StateOnly,
			// Token: 0x040064E4 RID: 25828
			[Token(Token = "0x4007136")]
			ProfileOnly
		}

		// Token: 0x020010A5 RID: 4261
		[Token(Token = "0x2001259")]
		public class CharaDetailInfo
		{
			// Token: 0x06005257 RID: 21079 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006347")]
			[Address(RVA = "0x1014490CC", Offset = "0x14490CC", VA = "0x1014490CC")]
			public CharaDetailInfo()
			{
			}

			// Token: 0x040064E5 RID: 25829
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007137")]
			public int m_CharaID;

			// Token: 0x040064E6 RID: 25830
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007138")]
			public int m_Lv;

			// Token: 0x040064E7 RID: 25831
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007139")]
			public long m_Exp;

			// Token: 0x040064E8 RID: 25832
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400713A")]
			public eCharaNamedType m_NamedType;

			// Token: 0x040064E9 RID: 25833
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x400713B")]
			public int m_FriendShip;

			// Token: 0x040064EA RID: 25834
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400713C")]
			public eRare m_RareType;

			// Token: 0x040064EB RID: 25835
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x400713D")]
			public int m_ArousalLv;

			// Token: 0x040064EC RID: 25836
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x400713E")]
			public int m_WeaponID;

			// Token: 0x040064ED RID: 25837
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x400713F")]
			public int m_WeaponLv;

			// Token: 0x040064EE RID: 25838
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4007140")]
			public int m_OrbID;

			// Token: 0x040064EF RID: 25839
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4007141")]
			public int m_OrbLv;

			// Token: 0x040064F0 RID: 25840
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4007142")]
			public int[] m_AbilityEquipItemIDs;
		}

		// Token: 0x020010A6 RID: 4262
		[Token(Token = "0x200125A")]
		public class LevelInfo
		{
			// Token: 0x06005258 RID: 21080 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006348")]
			[Address(RVA = "0x1014520DC", Offset = "0x14520DC", VA = "0x1014520DC")]
			public LevelInfo()
			{
			}

			// Token: 0x040064F1 RID: 25841
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007143")]
			public int m_CharaID;

			// Token: 0x040064F2 RID: 25842
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007144")]
			public int m_LimitBreak;

			// Token: 0x040064F3 RID: 25843
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007145")]
			public int m_NowLevel;

			// Token: 0x040064F4 RID: 25844
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4007146")]
			public int m_MaxLevel;
		}
	}
}
