﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200108A RID: 4234
	[Token(Token = "0x2000AFB")]
	[StructLayout(3)]
	public abstract class CharaEditSceneMaterialPanel : MonoBehaviour
	{
		// Token: 0x1400011A RID: 282
		// (add) Token: 0x0600518D RID: 20877 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600518E RID: 20878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011A")]
		public event Action OnMaterialsChanged
		{
			[Token(Token = "0x6004AFC")]
			[Address(RVA = "0x1014520E4", Offset = "0x14520E4", VA = "0x1014520E4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004AFD")]
			[Address(RVA = "0x1014521F0", Offset = "0x14521F0", VA = "0x1014521F0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600518F RID: 20879
		[Token(Token = "0x6004AFE")]
		[Address(Slot = "4")]
		public abstract int GetSelectButtonIdx();

		// Token: 0x06005190 RID: 20880
		[Token(Token = "0x6004AFF")]
		[Address(Slot = "5")]
		protected abstract CustomButton GetDecideButton();

		// Token: 0x06005191 RID: 20881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B00")]
		[Address(RVA = "0x1014522FC", Offset = "0x14522FC", VA = "0x1014522FC")]
		public void SetDecideButtonInteractable(bool interactable)
		{
		}

		// Token: 0x06005192 RID: 20882
		[Token(Token = "0x6004B01")]
		[Address(Slot = "6")]
		public abstract void OnClickSelectButton();

		// Token: 0x06005193 RID: 20883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B02")]
		[Address(RVA = "0x1014523B4", Offset = "0x14523B4", VA = "0x1014523B4", Slot = "7")]
		public virtual void OnClickSelectButton(int idx)
		{
		}

		// Token: 0x06005194 RID: 20884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B03")]
		[Address(RVA = "0x1014523C0", Offset = "0x14523C0", VA = "0x1014523C0")]
		protected void ExecuteOnMaterialsChanged()
		{
		}

		// Token: 0x06005195 RID: 20885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B04")]
		[Address(RVA = "0x1014523CC", Offset = "0x14523CC", VA = "0x1014523CC")]
		protected CharaEditSceneMaterialPanel()
		{
		}

		// Token: 0x040063FF RID: 25599
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400463A")]
		public Action<int, int> ReceiveUseItem;
	}
}
