﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI.Edit
{
	// Token: 0x020010A7 RID: 4263
	[Token(Token = "0x2000B0D")]
	[StructLayout(3)]
	public class CharaEvolutionGroup : UIGroup
	{
		// Token: 0x06005259 RID: 21081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC0")]
		[Address(RVA = "0x10144D298", Offset = "0x144D298", VA = "0x10144D298")]
		public void SetOwnerUI(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x0600525A RID: 21082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC1")]
		[Address(RVA = "0x10144D2A0", Offset = "0x144D2A0", VA = "0x10144D2A0")]
		public void Setup(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x0600525B RID: 21083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC2")]
		[Address(RVA = "0x10144D858", Offset = "0x144D858", VA = "0x10144D858")]
		public void Destroy()
		{
		}

		// Token: 0x0600525C RID: 21084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC3")]
		[Address(RVA = "0x1014523D4", Offset = "0x14523D4", VA = "0x1014523D4")]
		protected void Apply()
		{
		}

		// Token: 0x0600525D RID: 21085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC4")]
		[Address(RVA = "0x101452FD4", Offset = "0x1452FD4", VA = "0x101452FD4")]
		private void ApplyLevel(int nowLevel, int maxLevel, int afterMaxLevel, int limitBreakLevel, int afterLimitBreakLevel)
		{
		}

		// Token: 0x0600525E RID: 21086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC5")]
		[Address(RVA = "0x101452D30", Offset = "0x1452D30", VA = "0x101452D30")]
		private void SetupMaterial(long charaMngID, CharacterEvolutionListDB_Param evolutionListParam)
		{
		}

		// Token: 0x0600525F RID: 21087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC6")]
		[Address(RVA = "0x1014506B4", Offset = "0x14506B4", VA = "0x1014506B4")]
		public void Refresh(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x06005260 RID: 21088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC7")]
		[Address(RVA = "0x101453208", Offset = "0x1453208", VA = "0x101453208")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06005261 RID: 21089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC8")]
		[Address(RVA = "0x101453480", Offset = "0x1453480", VA = "0x101453480")]
		private void OnClickButtonCallback_DecideButton(int idx)
		{
		}

		// Token: 0x06005262 RID: 21090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BC9")]
		[Address(RVA = "0x1014535C4", Offset = "0x14535C4", VA = "0x1014535C4")]
		public CharaEvolutionGroup()
		{
		}

		// Token: 0x040064F5 RID: 25845
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40046E2")]
		[SerializeField]
		private LevelDisplay m_BeforeLevel;

		// Token: 0x040064F6 RID: 25846
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40046E3")]
		[SerializeField]
		private LevelDisplay m_AfterLevel;

		// Token: 0x040064F7 RID: 25847
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40046E4")]
		[SerializeField]
		private EvolutionItemInformation[] m_ItemInfomation;

		// Token: 0x040064F8 RID: 25848
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40046E5")]
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x040064F9 RID: 25849
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40046E6")]
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x040064FA RID: 25850
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40046E7")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040064FB RID: 25851
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40046E8")]
		[SerializeField]
		private CustomButton m_FailDecideButton;

		// Token: 0x040064FC RID: 25852
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40046E9")]
		[SerializeField]
		private Text m_EvolvedText;

		// Token: 0x040064FD RID: 25853
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40046EA")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x040064FE RID: 25854
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40046EB")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x040064FF RID: 25855
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40046EC")]
		private List<EditDefine.eEvolutionFailureReason> m_FailReason;

		// Token: 0x04006500 RID: 25856
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40046ED")]
		public Action<ResultCode, CharaDetailUI.LevelInfo, CharaDetailUI.LevelInfo> OnResponseCallback;
	}
}
