﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI.Edit
{
	// Token: 0x020010A8 RID: 4264
	[Token(Token = "0x2000B0E")]
	[StructLayout(3)]
	public class CharaLimitBreakGroup : UIGroup
	{
		// Token: 0x06005263 RID: 21091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCA")]
		[Address(RVA = "0x10144CEA8", Offset = "0x144CEA8", VA = "0x10144CEA8")]
		public void SetOwnerUI(CharaDetailUI ownerUI)
		{
		}

		// Token: 0x06005264 RID: 21092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCB")]
		[Address(RVA = "0x10144CEB0", Offset = "0x144CEB0", VA = "0x10144CEB0")]
		public void Setup(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x06005265 RID: 21093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCC")]
		[Address(RVA = "0x10144D848", Offset = "0x144D848", VA = "0x10144D848")]
		public void Destroy()
		{
		}

		// Token: 0x06005266 RID: 21094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCD")]
		[Address(RVA = "0x101453634", Offset = "0x1453634", VA = "0x101453634")]
		protected void Apply()
		{
		}

		// Token: 0x06005267 RID: 21095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCE")]
		[Address(RVA = "0x101453674", Offset = "0x1453674", VA = "0x101453674")]
		private void ApplyInternal(List<KeyValuePair<int, int>> selectItem)
		{
		}

		// Token: 0x06005268 RID: 21096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BCF")]
		[Address(RVA = "0x1014542BC", Offset = "0x14542BC", VA = "0x1014542BC")]
		private void ApplyLevel(int nowLevel, int maxLevel, int afterMaxLevel, int limitBreakLevel, int addLimitBreakLevel)
		{
		}

		// Token: 0x06005269 RID: 21097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BD0")]
		[Address(RVA = "0x1014544F0", Offset = "0x14544F0", VA = "0x1014544F0")]
		private void ApplySkillLevel(int beforeUniqueSkillMaxLevel, int afterUniqueSkillMaxLevel, int beforeClassSkillMaxLevel, int afterClassSkillMaxLevel)
		{
		}

		// Token: 0x0600526A RID: 21098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BD1")]
		[Address(RVA = "0x101454648", Offset = "0x1454648", VA = "0x101454648")]
		private void ApplyInfo(long haveGold, long needGold, bool buttonInteractable)
		{
		}

		// Token: 0x0600526B RID: 21099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BD2")]
		[Address(RVA = "0x101450670", Offset = "0x1450670", VA = "0x101450670")]
		public void Refresh(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x0600526C RID: 21100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BD3")]
		[Address(RVA = "0x101454800", Offset = "0x1454800", VA = "0x101454800")]
		private void OnMaterialsChanged()
		{
		}

		// Token: 0x0600526D RID: 21101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BD4")]
		[Address(RVA = "0x101454838", Offset = "0x1454838", VA = "0x101454838")]
		private void ReceiveUseItem(int itemID, int itemNum)
		{
		}

		// Token: 0x0600526E RID: 21102 RVA: 0x0001C338 File Offset: 0x0001A538
		[Token(Token = "0x6004BD5")]
		[Address(RVA = "0x10145410C", Offset = "0x145410C", VA = "0x10145410C")]
		private int CalcAddLimitBreakLevel(List<KeyValuePair<int, int>> itemList, List<KeyValuePair<int, int>> itemRequiredNumList, int excludeItemID = -1)
		{
			return 0;
		}

		// Token: 0x0600526F RID: 21103 RVA: 0x0001C350 File Offset: 0x0001A550
		[Token(Token = "0x6004BD6")]
		[Address(RVA = "0x101454B48", Offset = "0x1454B48", VA = "0x101454B48")]
		private int CalcAddLimitBreakLevel(int itemID, int itemNum, List<KeyValuePair<int, int>> itemRequiredNumList)
		{
			return 0;
		}

		// Token: 0x06005270 RID: 21104 RVA: 0x0001C368 File Offset: 0x0001A568
		[Token(Token = "0x6004BD7")]
		[Address(RVA = "0x101454B70", Offset = "0x1454B70", VA = "0x101454B70")]
		private int GetItemRequiredNum(int itemID, List<KeyValuePair<int, int>> itemRequiredNumList)
		{
			return 0;
		}

		// Token: 0x06005271 RID: 21105 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004BD8")]
		[Address(RVA = "0x101454CDC", Offset = "0x1454CDC", VA = "0x101454CDC")]
		private int[] CreateItemArray(List<KeyValuePair<int, int>> selectItem)
		{
			return null;
		}

		// Token: 0x06005272 RID: 21106 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004BD9")]
		[Address(RVA = "0x101454EEC", Offset = "0x1454EEC", VA = "0x101454EEC")]
		private List<KeyValuePair<int, int>> CreateOrderList(List<KeyValuePair<int, int>> selectItem)
		{
			return null;
		}

		// Token: 0x06005273 RID: 21107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDA")]
		[Address(RVA = "0x1014550F4", Offset = "0x14550F4", VA = "0x1014550F4")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06005274 RID: 21108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDB")]
		[Address(RVA = "0x101455944", Offset = "0x1455944", VA = "0x101455944")]
		private void OnClickButtonCallback_DecideButton(int idx)
		{
		}

		// Token: 0x06005275 RID: 21109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDC")]
		[Address(RVA = "0x101455F90", Offset = "0x1455F90", VA = "0x101455F90")]
		private void OnClickButtonCallback_ConfirmAllClass(int idx)
		{
		}

		// Token: 0x06005276 RID: 21110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDD")]
		[Address(RVA = "0x101455E04", Offset = "0x1455E04", VA = "0x101455E04")]
		public void RequestLimitBreakRoutine()
		{
		}

		// Token: 0x06005277 RID: 21111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDE")]
		[Address(RVA = "0x101455F9C", Offset = "0x1455F9C", VA = "0x101455F9C")]
		public void OnClickResetButton()
		{
		}

		// Token: 0x06005278 RID: 21112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BDF")]
		[Address(RVA = "0x101455FDC", Offset = "0x1455FDC", VA = "0x101455FDC")]
		public CharaLimitBreakGroup()
		{
		}

		// Token: 0x04006501 RID: 25857
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40046EE")]
		[SerializeField]
		private LevelDisplay m_BeforeLevel;

		// Token: 0x04006502 RID: 25858
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40046EF")]
		[SerializeField]
		private LevelDisplay m_AfterLevel;

		// Token: 0x04006503 RID: 25859
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40046F0")]
		[SerializeField]
		private Text m_UniqueSkillLevelText;

		// Token: 0x04006504 RID: 25860
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40046F1")]
		[SerializeField]
		private Text m_UniqueSkillLevelTextAfter;

		// Token: 0x04006505 RID: 25861
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40046F2")]
		[SerializeField]
		private Text m_SkillLevelText;

		// Token: 0x04006506 RID: 25862
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40046F3")]
		[SerializeField]
		private Text m_SkillLevelTextAfter;

		// Token: 0x04006507 RID: 25863
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40046F4")]
		[SerializeField]
		private UpgradeMaterialPanel m_MaterialPanel;

		// Token: 0x04006508 RID: 25864
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40046F5")]
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x04006509 RID: 25865
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40046F6")]
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x0400650A RID: 25866
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40046F7")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x0400650B RID: 25867
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40046F8")]
		private CharaDetailUI m_OwnerUI;

		// Token: 0x0400650C RID: 25868
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40046F9")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x0400650D RID: 25869
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40046FA")]
		private List<KeyValuePair<int, int>> m_ItemRequiredNumList;

		// Token: 0x0400650E RID: 25870
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40046FB")]
		public Action<ResultCode, int, int> OnResponseCallback;
	}
}
