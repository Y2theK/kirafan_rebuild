﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010B0 RID: 4272
	[Token(Token = "0x2000B13")]
	[StructLayout(3)]
	public class CharaListUI : MenuUIBase
	{
		// Token: 0x17000584 RID: 1412
		// (get) Token: 0x060052BB RID: 21179 RVA: 0x0001C3C8 File Offset: 0x0001A5C8
		[Token(Token = "0x17000528")]
		public CharaListUI.eButton SelectButton
		{
			[Token(Token = "0x6004C21")]
			[Address(RVA = "0x101455FE4", Offset = "0x1455FE4", VA = "0x101455FE4")]
			get
			{
				return CharaListUI.eButton.Chara;
			}
		}

		// Token: 0x17000585 RID: 1413
		// (get) Token: 0x060052BC RID: 21180 RVA: 0x0001C3E0 File Offset: 0x0001A5E0
		[Token(Token = "0x17000529")]
		public long SelectCharaMngID
		{
			[Token(Token = "0x6004C22")]
			[Address(RVA = "0x101455FEC", Offset = "0x1455FEC", VA = "0x101455FEC")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x060052BD RID: 21181 RVA: 0x0001C3F8 File Offset: 0x0001A5F8
		[Token(Token = "0x1700052A")]
		public int PartyIndex
		{
			[Token(Token = "0x6004C23")]
			[Address(RVA = "0x101455FF4", Offset = "0x1455FF4", VA = "0x101455FF4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x060052BE RID: 21182 RVA: 0x0001C410 File Offset: 0x0001A610
		[Token(Token = "0x1700052B")]
		public int MemberIndex
		{
			[Token(Token = "0x6004C24")]
			[Address(RVA = "0x101455FFC", Offset = "0x1455FFC", VA = "0x101455FFC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060052BF RID: 21183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C25")]
		[Address(RVA = "0x101456004", Offset = "0x1456004", VA = "0x101456004")]
		private void Start()
		{
		}

		// Token: 0x060052C0 RID: 21184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C26")]
		[Address(RVA = "0x101456008", Offset = "0x1456008", VA = "0x101456008")]
		public void SetViewMode()
		{
		}

		// Token: 0x060052C1 RID: 21185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C27")]
		[Address(RVA = "0x101456048", Offset = "0x1456048", VA = "0x101456048")]
		public void SetPartyEditMode(int partyIndex, int memberIndex)
		{
		}

		// Token: 0x060052C2 RID: 21186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C28")]
		[Address(RVA = "0x1014560A4", Offset = "0x14560A4", VA = "0x1014560A4")]
		public void SetSupportEditMode(int supportPartyIndex, int supportMemberIndex)
		{
		}

		// Token: 0x060052C3 RID: 21187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C29")]
		[Address(RVA = "0x101456100", Offset = "0x1456100", VA = "0x101456100")]
		public void SetUpgradeMode()
		{
		}

		// Token: 0x060052C4 RID: 21188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2A")]
		[Address(RVA = "0x101456140", Offset = "0x1456140", VA = "0x101456140")]
		public void SetEvolutionMode()
		{
		}

		// Token: 0x060052C5 RID: 21189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2B")]
		[Address(RVA = "0x101456180", Offset = "0x1456180", VA = "0x101456180")]
		public void SetLimitBreakMode()
		{
		}

		// Token: 0x060052C6 RID: 21190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2C")]
		[Address(RVA = "0x1014561C0", Offset = "0x14561C0", VA = "0x1014561C0")]
		public void SetViewChangeMode()
		{
		}

		// Token: 0x060052C7 RID: 21191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2D")]
		[Address(RVA = "0x101456200", Offset = "0x1456200", VA = "0x101456200")]
		public void SetAbilityListupMode()
		{
		}

		// Token: 0x060052C8 RID: 21192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2E")]
		[Address(RVA = "0x101456240", Offset = "0x1456240", VA = "0x101456240")]
		public void SetScrollValue(float value)
		{
		}

		// Token: 0x060052C9 RID: 21193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C2F")]
		[Address(RVA = "0x101456280", Offset = "0x1456280", VA = "0x101456280")]
		public void ClearCallback()
		{
		}

		// Token: 0x060052CA RID: 21194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C30")]
		[Address(RVA = "0x1014562B0", Offset = "0x14562B0", VA = "0x1014562B0")]
		public void SetOnClickCallback(Action<long> callback)
		{
		}

		// Token: 0x060052CB RID: 21195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C31")]
		[Address(RVA = "0x1014562E8", Offset = "0x14562E8", VA = "0x1014562E8")]
		public void Setup(bool isPlayOutOnHoldChara = false)
		{
		}

		// Token: 0x060052CC RID: 21196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C32")]
		[Address(RVA = "0x1014564E0", Offset = "0x14564E0", VA = "0x1014564E0")]
		private void Update()
		{
		}

		// Token: 0x060052CD RID: 21197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C33")]
		[Address(RVA = "0x101456688", Offset = "0x1456688", VA = "0x101456688")]
		private void ChangeStep(CharaListUI.eStep step)
		{
		}

		// Token: 0x060052CE RID: 21198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C34")]
		[Address(RVA = "0x101456790", Offset = "0x1456790", VA = "0x101456790", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060052CF RID: 21199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C35")]
		[Address(RVA = "0x101456B00", Offset = "0x1456B00", VA = "0x101456B00", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060052D0 RID: 21200 RVA: 0x0001C428 File Offset: 0x0001A628
		[Token(Token = "0x6004C36")]
		[Address(RVA = "0x101456C24", Offset = "0x1456C24", VA = "0x101456C24", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060052D1 RID: 21201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C37")]
		[Address(RVA = "0x101456C34", Offset = "0x1456C34", VA = "0x101456C34", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060052D2 RID: 21202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C38")]
		[Address(RVA = "0x101456C78", Offset = "0x1456C78", VA = "0x101456C78")]
		public void OnClickCharaIconCallBack(long charaMngID)
		{
		}

		// Token: 0x060052D3 RID: 21203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C39")]
		[Address(RVA = "0x101456C8C", Offset = "0x1456C8C", VA = "0x101456C8C")]
		public void OnHoldCharaIconCallBack(long charaMngID)
		{
		}

		// Token: 0x060052D4 RID: 21204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3A")]
		[Address(RVA = "0x101456CA4", Offset = "0x1456CA4", VA = "0x101456CA4")]
		public void OnClickRemoveIconCallBack()
		{
		}

		// Token: 0x060052D5 RID: 21205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3B")]
		[Address(RVA = "0x101456CB8", Offset = "0x1456CB8", VA = "0x101456CB8")]
		public CharaListUI()
		{
		}

		// Token: 0x04006556 RID: 25942
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400472C")]
		private CharaListUI.eStep m_Step;

		// Token: 0x04006557 RID: 25943
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400472D")]
		[SerializeField]
		private CharaListScroll m_Scroll;

		// Token: 0x04006558 RID: 25944
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400472E")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04006559 RID: 25945
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400472F")]
		[SerializeField]
		private CharaListWindow m_CharaListGroup;

		// Token: 0x0400655A RID: 25946
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004730")]
		private CharaListUI.eButton m_SelectButton;

		// Token: 0x0400655B RID: 25947
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004731")]
		private long m_SelectCharaMngID;

		// Token: 0x0400655C RID: 25948
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004732")]
		private int m_PartyIndex;

		// Token: 0x0400655D RID: 25949
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4004733")]
		private int m_MemberIndex;

		// Token: 0x020010B1 RID: 4273
		[Token(Token = "0x200125E")]
		private enum eStep
		{
			// Token: 0x0400655F RID: 25951
			[Token(Token = "0x400715F")]
			Hide,
			// Token: 0x04006560 RID: 25952
			[Token(Token = "0x4007160")]
			In,
			// Token: 0x04006561 RID: 25953
			[Token(Token = "0x4007161")]
			Idle,
			// Token: 0x04006562 RID: 25954
			[Token(Token = "0x4007162")]
			Out,
			// Token: 0x04006563 RID: 25955
			[Token(Token = "0x4007163")]
			Window,
			// Token: 0x04006564 RID: 25956
			[Token(Token = "0x4007164")]
			End,
			// Token: 0x04006565 RID: 25957
			[Token(Token = "0x4007165")]
			Num
		}

		// Token: 0x020010B2 RID: 4274
		[Token(Token = "0x200125F")]
		public enum eButton
		{
			// Token: 0x04006567 RID: 25959
			[Token(Token = "0x4007167")]
			None = -1,
			// Token: 0x04006568 RID: 25960
			[Token(Token = "0x4007168")]
			Chara,
			// Token: 0x04006569 RID: 25961
			[Token(Token = "0x4007169")]
			CharaHold,
			// Token: 0x0400656A RID: 25962
			[Token(Token = "0x400716A")]
			Remove
		}

		// Token: 0x020010B3 RID: 4275
		[Token(Token = "0x2001260")]
		public enum eMode
		{
			// Token: 0x0400656C RID: 25964
			[Token(Token = "0x400716C")]
			View,
			// Token: 0x0400656D RID: 25965
			[Token(Token = "0x400716D")]
			PartyEdit,
			// Token: 0x0400656E RID: 25966
			[Token(Token = "0x400716E")]
			Upgrade,
			// Token: 0x0400656F RID: 25967
			[Token(Token = "0x400716F")]
			LimitBreak,
			// Token: 0x04006570 RID: 25968
			[Token(Token = "0x4007170")]
			Evolution
		}
	}
}
