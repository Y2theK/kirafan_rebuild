﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010A9 RID: 4265
	[Token(Token = "0x2000B0F")]
	[StructLayout(3)]
	public class CharaStatusGroup : UIGroup
	{
		// Token: 0x06005279 RID: 21113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE0")]
		[Address(RVA = "0x10144C130", Offset = "0x144C130", VA = "0x10144C130")]
		public void Setup(UserCharacterData userCharacterData, UserWeaponData userWeaponData, UserMasterOrbData userOrbData, UserAbilityData userAbilityData)
		{
		}

		// Token: 0x0600527A RID: 21114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE1")]
		[Address(RVA = "0x10144D348", Offset = "0x144D348", VA = "0x10144D348")]
		public void SetFriendship(int friendship, long friendshipExp)
		{
		}

		// Token: 0x0600527B RID: 21115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE2")]
		[Address(RVA = "0x10144D430", Offset = "0x144D430", VA = "0x10144D430")]
		public void SetSkillExpGauge(bool disable)
		{
		}

		// Token: 0x0600527C RID: 21116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE3")]
		[Address(RVA = "0x101456CD0", Offset = "0x1456CD0", VA = "0x101456CD0", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x0600527D RID: 21117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE4")]
		[Address(RVA = "0x10144D7A0", Offset = "0x144D7A0", VA = "0x10144D7A0")]
		public void Destroy()
		{
		}

		// Token: 0x0600527E RID: 21118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE5")]
		[Address(RVA = "0x101450334", Offset = "0x1450334", VA = "0x101450334")]
		public void Refresh(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x0600527F RID: 21119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE6")]
		[Address(RVA = "0x10144D354", Offset = "0x144D354", VA = "0x10144D354")]
		public void SetCurrentCategory(CharaStatusGroup.eStatusCategoryType type)
		{
		}

		// Token: 0x06005280 RID: 21120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE7")]
		[Address(RVA = "0x1014570E4", Offset = "0x14570E4", VA = "0x1014570E4")]
		public void SetActiveCategory(CharaStatusGroup.eStatusCategoryType type, bool isActive)
		{
		}

		// Token: 0x06005281 RID: 21121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE8")]
		[Address(RVA = "0x10144CB58", Offset = "0x144CB58", VA = "0x10144CB58")]
		public void SetInteractableCategory(CharaStatusGroup.eStatusCategoryType type, bool isActive)
		{
		}

		// Token: 0x06005282 RID: 21122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BE9")]
		[Address(RVA = "0x10145712C", Offset = "0x145712C", VA = "0x10145712C")]
		public void SetActiveCharacterToggleState(bool state, bool isActive)
		{
		}

		// Token: 0x06005283 RID: 21123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BEA")]
		[Address(RVA = "0x10144D38C", Offset = "0x144D38C", VA = "0x10144D38C")]
		public void SetActiveEvolutionToggleState(bool state, bool isActive)
		{
		}

		// Token: 0x06005284 RID: 21124 RVA: 0x0001C380 File Offset: 0x0001A580
		[Token(Token = "0x6004BEB")]
		[Address(RVA = "0x101450F30", Offset = "0x1450F30", VA = "0x101450F30")]
		public bool GetCharacterToggleState()
		{
			return default(bool);
		}

		// Token: 0x06005285 RID: 21125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BEC")]
		[Address(RVA = "0x10144FB80", Offset = "0x144FB80", VA = "0x10144FB80")]
		public void SetCharacterToggleState(bool flag)
		{
		}

		// Token: 0x06005286 RID: 21126 RVA: 0x0001C398 File Offset: 0x0001A598
		[Token(Token = "0x6004BED")]
		[Address(RVA = "0x101450F88", Offset = "0x1450F88", VA = "0x101450F88")]
		public bool GetEvolutionToggleState()
		{
			return default(bool);
		}

		// Token: 0x06005287 RID: 21127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BEE")]
		[Address(RVA = "0x10144D3FC", Offset = "0x144D3FC", VA = "0x10144D3FC")]
		public void SetEvolutionToggleState(bool flag)
		{
		}

		// Token: 0x06005288 RID: 21128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BEF")]
		[Address(RVA = "0x10144D3D0", Offset = "0x144D3D0", VA = "0x10144D3D0")]
		public void SetEvolutionToggleUndecided()
		{
		}

		// Token: 0x06005289 RID: 21129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF0")]
		[Address(RVA = "0x10144D528", Offset = "0x144D528", VA = "0x10144D528")]
		public void SaveToggleState()
		{
		}

		// Token: 0x0600528A RID: 21130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF1")]
		[Address(RVA = "0x101451D78", Offset = "0x1451D78", VA = "0x101451D78")]
		public void LoadToggleState()
		{
		}

		// Token: 0x0600528B RID: 21131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF2")]
		[Address(RVA = "0x10144D438", Offset = "0x144D438", VA = "0x10144D438")]
		public void SetStatusDetailCallback(Action callback)
		{
		}

		// Token: 0x0600528C RID: 21132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF3")]
		[Address(RVA = "0x10144D468", Offset = "0x144D468", VA = "0x10144D468")]
		public void SetStatusInfoCallback(Action callback)
		{
		}

		// Token: 0x0600528D RID: 21133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF4")]
		[Address(RVA = "0x10144D498", Offset = "0x144D498", VA = "0x10144D498")]
		public void SetCharacterToggleCallback(Action<int> callback)
		{
		}

		// Token: 0x0600528E RID: 21134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF5")]
		[Address(RVA = "0x10144D4C8", Offset = "0x144D4C8", VA = "0x10144D4C8")]
		public void SetEvolutionToggleCallback(Action<int> callback)
		{
		}

		// Token: 0x0600528F RID: 21135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF6")]
		[Address(RVA = "0x10144D4F8", Offset = "0x144D4F8", VA = "0x10144D4F8")]
		public void SetSkillAbilityInfoCallback(Action<UserAbilityData> callback)
		{
		}

		// Token: 0x06005290 RID: 21136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF7")]
		[Address(RVA = "0x101456D1C", Offset = "0x1456D1C", VA = "0x101456D1C")]
		private void OnChangeIndex(int index)
		{
		}

		// Token: 0x06005291 RID: 21137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BF8")]
		[Address(RVA = "0x101457170", Offset = "0x1457170", VA = "0x101457170")]
		public CharaStatusGroup()
		{
		}

		// Token: 0x0400650F RID: 25871
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40046FC")]
		[SerializeField]
		private RadioButton m_RadioButton;

		// Token: 0x04006510 RID: 25872
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40046FD")]
		[SerializeField]
		private CharaContentsStatus m_Status;

		// Token: 0x04006511 RID: 25873
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40046FE")]
		[SerializeField]
		private CharaContentsSkill m_Skill;

		// Token: 0x04006512 RID: 25874
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40046FF")]
		[SerializeField]
		private CharaContentsProfile m_Profile;

		// Token: 0x04006513 RID: 25875
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004700")]
		[SerializeField]
		private SelectedCharaInfoVoiceList m_VoiceList;

		// Token: 0x04006514 RID: 25876
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004701")]
		private int m_FriendShip;

		// Token: 0x04006515 RID: 25877
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004702")]
		private long m_FriendShipExp;

		// Token: 0x04006516 RID: 25878
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004703")]
		private bool m_DisableSkillExpGauge;

		// Token: 0x04006517 RID: 25879
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4004704")]
		private int m_PrevSkillIndex;

		// Token: 0x04006518 RID: 25880
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004705")]
		private UserCharacterData m_UserCharaData;

		// Token: 0x04006519 RID: 25881
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004706")]
		private UserWeaponData m_UserWeaponData;

		// Token: 0x0400651A RID: 25882
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004707")]
		private UserMasterOrbData m_UserOrbData;

		// Token: 0x0400651B RID: 25883
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004708")]
		private UserAbilityData m_UserAbilityData;

		// Token: 0x020010AA RID: 4266
		[Token(Token = "0x200125B")]
		public enum eStatusCategoryType
		{
			// Token: 0x0400651D RID: 25885
			[Token(Token = "0x4007148")]
			Status,
			// Token: 0x0400651E RID: 25886
			[Token(Token = "0x4007149")]
			Skill,
			// Token: 0x0400651F RID: 25887
			[Token(Token = "0x400714A")]
			Profile,
			// Token: 0x04006520 RID: 25888
			[Token(Token = "0x400714B")]
			Voice
		}
	}
}
