﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010AB RID: 4267
	[Token(Token = "0x2000B10")]
	[StructLayout(3)]
	public class CharaUpgradeGroup : UIGroup
	{
		// Token: 0x17000582 RID: 1410
		// (get) Token: 0x06005292 RID: 21138 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000526")]
		public UpgradeMaterialPanel Materialpanel
		{
			[Token(Token = "0x6004BF9")]
			[Address(RVA = "0x101451A94", Offset = "0x1451A94", VA = "0x101451A94")]
			get
			{
				return null;
			}
		}

		// Token: 0x06005293 RID: 21139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BFA")]
		[Address(RVA = "0x10144CBA0", Offset = "0x144CBA0", VA = "0x10144CBA0")]
		public void Setup(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x06005294 RID: 21140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BFB")]
		[Address(RVA = "0x10144D840", Offset = "0x144D840", VA = "0x10144D840")]
		public void Destroy()
		{
		}

		// Token: 0x06005295 RID: 21141 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004BFC")]
		[Address(RVA = "0x101451184", Offset = "0x1451184", VA = "0x101451184")]
		public CharaUpgradeGroup.UpgradeConfirmInfo GetUpgradeConfirmInfo()
		{
			return null;
		}

		// Token: 0x06005296 RID: 21142 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004BFD")]
		[Address(RVA = "0x1014571C0", Offset = "0x14571C0", VA = "0x1014571C0")]
		private string GetNowLevelString(int nowLevel, int afterLevel)
		{
			return null;
		}

		// Token: 0x06005297 RID: 21143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BFE")]
		[Address(RVA = "0x101457180", Offset = "0x1457180", VA = "0x101457180")]
		protected void Apply()
		{
		}

		// Token: 0x06005298 RID: 21144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004BFF")]
		[Address(RVA = "0x1014572B0", Offset = "0x14572B0", VA = "0x1014572B0")]
		private void ApplyInternal(List<KeyValuePair<int, int>> list)
		{
		}

		// Token: 0x06005299 RID: 21145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C00")]
		[Address(RVA = "0x101457708", Offset = "0x1457708", VA = "0x101457708")]
		private void ApplyLevel(int nowLevel, int workingLevel, int maxLevel, int limitBreak)
		{
		}

		// Token: 0x0600529A RID: 21146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C01")]
		[Address(RVA = "0x1014577BC", Offset = "0x14577BC", VA = "0x1014577BC")]
		private void ApplyInfo(long haveGold, long needGold, bool buttonInteractable)
		{
		}

		// Token: 0x0600529B RID: 21147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C02")]
		[Address(RVA = "0x10145046C", Offset = "0x145046C", VA = "0x10145046C")]
		public void Refresh(UserCharacterData userCharacterData)
		{
		}

		// Token: 0x0600529C RID: 21148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C03")]
		[Address(RVA = "0x101451A08", Offset = "0x1451A08", VA = "0x101451A08")]
		public void BackupBeforeLevel()
		{
		}

		// Token: 0x0600529D RID: 21149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C04")]
		[Address(RVA = "0x10144FE88", Offset = "0x144FE88", VA = "0x10144FE88")]
		public void PlayGauge()
		{
		}

		// Token: 0x0600529E RID: 21150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C05")]
		[Address(RVA = "0x101457974", Offset = "0x1457974", VA = "0x101457974")]
		public void StopGauge()
		{
		}

		// Token: 0x0600529F RID: 21151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C06")]
		[Address(RVA = "0x101457B14", Offset = "0x1457B14", VA = "0x101457B14")]
		private void OnGaugeLevelup(int workingLevel)
		{
		}

		// Token: 0x060052A0 RID: 21152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C07")]
		[Address(RVA = "0x101457BC8", Offset = "0x1457BC8", VA = "0x101457BC8")]
		private void OnMaterialsChanged()
		{
		}

		// Token: 0x060052A1 RID: 21153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C08")]
		[Address(RVA = "0x101457C00", Offset = "0x1457C00", VA = "0x101457C00")]
		private void ReceiveUseItem(int itemID, int itemNum)
		{
		}

		// Token: 0x060052A2 RID: 21154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C09")]
		[Address(RVA = "0x1014580D0", Offset = "0x14580D0", VA = "0x1014580D0")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060052A3 RID: 21155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C0A")]
		[Address(RVA = "0x1014580DC", Offset = "0x14580DC", VA = "0x1014580DC")]
		public void OnClickResetButton()
		{
		}

		// Token: 0x060052A4 RID: 21156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C0B")]
		[Address(RVA = "0x10145811C", Offset = "0x145811C", VA = "0x10145811C")]
		public CharaUpgradeGroup()
		{
		}

		// Token: 0x04006521 RID: 25889
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004709")]
		[SerializeField]
		private LevelDisplay m_LevelInfo;

		// Token: 0x04006522 RID: 25890
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400470A")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04006523 RID: 25891
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400470B")]
		[SerializeField]
		private UpgradeMaterialPanel m_MaterialPanel;

		// Token: 0x04006524 RID: 25892
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400470C")]
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x04006525 RID: 25893
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400470D")]
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x04006526 RID: 25894
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400470E")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04006527 RID: 25895
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400470F")]
		private UserCharacterData m_UserCharacterData;

		// Token: 0x04006528 RID: 25896
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004710")]
		private CharaUpgradeGroup.UpgradeConfirmInfo m_UpgradeConfirmInfo;

		// Token: 0x04006529 RID: 25897
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004711")]
		private int m_AfterCharaLevel;

		// Token: 0x0400652A RID: 25898
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004712")]
		private long m_TakeExp;

		// Token: 0x0400652B RID: 25899
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004713")]
		private int m_BeforeLevel;

		// Token: 0x0400652C RID: 25900
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004714")]
		private long m_BeforeExp;

		// Token: 0x0400652D RID: 25901
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004715")]
		public Action OnClickDecideButtonCallback;

		// Token: 0x020010AC RID: 4268
		[Token(Token = "0x200125C")]
		public class UpgradeConfirmInfo
		{
			// Token: 0x060052A5 RID: 21157 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006349")]
			[Address(RVA = "0x101458184", Offset = "0x1458184", VA = "0x101458184")]
			public UpgradeConfirmInfo()
			{
			}

			// Token: 0x0400652E RID: 25902
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400714C")]
			public string m_BeforeNowLevelStr;

			// Token: 0x0400652F RID: 25903
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400714D")]
			public string m_AfterNowLevelStr;

			// Token: 0x04006530 RID: 25904
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x400714E")]
			public string m_MaxLevelStr;

			// Token: 0x04006531 RID: 25905
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x400714F")]
			public string m_GetExp;

			// Token: 0x04006532 RID: 25906
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4007150")]
			public int[] m_BeforeParam;

			// Token: 0x04006533 RID: 25907
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4007151")]
			public int[] m_AfterParam;
		}
	}
}
