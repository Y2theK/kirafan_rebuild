﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010B4 RID: 4276
	[Token(Token = "0x2000B14")]
	[StructLayout(3)]
	public class EditAdditiveSceneCharacterInformationPanel : CharacterInformationPanel
	{
		// Token: 0x060052D6 RID: 21206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3C")]
		[Address(RVA = "0x1014581F8", Offset = "0x14581F8", VA = "0x1014581F8")]
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060052D7 RID: 21207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3D")]
		[Address(RVA = "0x101458290", Offset = "0x1458290", VA = "0x101458290", Slot = "8")]
		protected virtual void ApplyResource(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060052D8 RID: 21208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3E")]
		[Address(RVA = "0x101458294", Offset = "0x1458294", VA = "0x101458294", Slot = "9")]
		protected virtual void ApplyParameter(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060052D9 RID: 21209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C3F")]
		[Address(RVA = "0x101458298", Offset = "0x1458298", VA = "0x101458298")]
		public EditAdditiveSceneCharacterInformationPanel()
		{
		}
	}
}
