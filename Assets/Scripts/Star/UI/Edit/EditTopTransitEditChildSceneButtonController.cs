﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010B5 RID: 4277
	[Token(Token = "0x2000B15")]
	[StructLayout(3)]
	public class EditTopTransitEditChildSceneButtonController : MonoBehaviour
	{
		// Token: 0x060052DA RID: 21210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C40")]
		[Address(RVA = "0x1014582A0", Offset = "0x14582A0", VA = "0x1014582A0")]
		public void Setup(EditTopUI parent)
		{
		}

		// Token: 0x060052DB RID: 21211 RVA: 0x0001C440 File Offset: 0x0001A640
		[Token(Token = "0x6004C41")]
		[Address(RVA = "0x1014582A8", Offset = "0x14582A8", VA = "0x1014582A8")]
		public EditTopUI.eButton GetButtonType()
		{
			return EditTopUI.eButton.None;
		}

		// Token: 0x060052DC RID: 21212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C42")]
		[Address(RVA = "0x1014582B0", Offset = "0x14582B0", VA = "0x1014582B0")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x060052DD RID: 21213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C43")]
		[Address(RVA = "0x1014583A8", Offset = "0x14583A8", VA = "0x1014583A8")]
		public void OnClickButton()
		{
		}

		// Token: 0x060052DE RID: 21214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C44")]
		[Address(RVA = "0x1014584E4", Offset = "0x14584E4", VA = "0x1014584E4")]
		public EditTopTransitEditChildSceneButtonController()
		{
		}

		// Token: 0x04006571 RID: 25969
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004734")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04006572 RID: 25970
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004735")]
		[SerializeField]
		private EditTopUI.eButton m_ButtonType;

		// Token: 0x04006573 RID: 25971
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004736")]
		private EditTopUI m_Parent;
	}
}
