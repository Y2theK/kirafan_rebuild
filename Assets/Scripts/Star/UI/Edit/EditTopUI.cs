﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010B6 RID: 4278
	[Token(Token = "0x2000B16")]
	[StructLayout(3)]
	public class EditTopUI : MenuUIBase
	{
		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x060052DF RID: 21215 RVA: 0x0001C458 File Offset: 0x0001A658
		[Token(Token = "0x1700052C")]
		public EditTopUI.eButton SelectButton
		{
			[Token(Token = "0x6004C45")]
			[Address(RVA = "0x1014584EC", Offset = "0x14584EC", VA = "0x1014584EC")]
			get
			{
				return EditTopUI.eButton.None;
			}
		}

		// Token: 0x060052E0 RID: 21216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C46")]
		[Address(RVA = "0x1014584F4", Offset = "0x14584F4", VA = "0x1014584F4")]
		public void Setup(EditMain.eTopType topType, float scrollValue)
		{
		}

		// Token: 0x060052E1 RID: 21217 RVA: 0x0001C470 File Offset: 0x0001A670
		[Token(Token = "0x6004C47")]
		[Address(RVA = "0x1014586A4", Offset = "0x14586A4", VA = "0x1014586A4")]
		public float GetScrollValue()
		{
			return 0f;
		}

		// Token: 0x060052E2 RID: 21218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C48")]
		[Address(RVA = "0x1014586D4", Offset = "0x14586D4", VA = "0x1014586D4")]
		private void Update()
		{
		}

		// Token: 0x060052E3 RID: 21219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C49")]
		[Address(RVA = "0x101458864", Offset = "0x1458864", VA = "0x101458864")]
		private void ChangeStep(EditTopUI.eStep step)
		{
		}

		// Token: 0x060052E4 RID: 21220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C4A")]
		[Address(RVA = "0x101458954", Offset = "0x1458954", VA = "0x101458954", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060052E5 RID: 21221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C4B")]
		[Address(RVA = "0x101458A10", Offset = "0x1458A10", VA = "0x101458A10", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060052E6 RID: 21222 RVA: 0x0001C488 File Offset: 0x0001A688
		[Token(Token = "0x6004C4C")]
		[Address(RVA = "0x101458ACC", Offset = "0x1458ACC", VA = "0x101458ACC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060052E7 RID: 21223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C4D")]
		[Address(RVA = "0x101458448", Offset = "0x1458448", VA = "0x101458448")]
		public void OnClickButtonCallBack(EditTopUI.eButton buttonId)
		{
		}

		// Token: 0x060052E8 RID: 21224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C4E")]
		[Address(RVA = "0x101458ADC", Offset = "0x1458ADC", VA = "0x101458ADC")]
		public EditTopUI()
		{
		}

		// Token: 0x04006574 RID: 25972
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004737")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04006575 RID: 25973
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004738")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006576 RID: 25974
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004739")]
		[SerializeField]
		private CategorySelectMenu m_MenuScroll;

		// Token: 0x04006577 RID: 25975
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400473A")]
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x04006578 RID: 25976
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400473B")]
		[SerializeField]
		private EditTopTransitEditChildSceneButtonController[] m_TransitButtons;

		// Token: 0x04006579 RID: 25977
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400473C")]
		private EditTopUI.eStep m_Step;

		// Token: 0x0400657A RID: 25978
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x400473D")]
		private EditTopUI.eButton m_SelectButton;

		// Token: 0x020010B7 RID: 4279
		[Token(Token = "0x2001261")]
		private enum eStep
		{
			// Token: 0x0400657C RID: 25980
			[Token(Token = "0x4007172")]
			Hide,
			// Token: 0x0400657D RID: 25981
			[Token(Token = "0x4007173")]
			In,
			// Token: 0x0400657E RID: 25982
			[Token(Token = "0x4007174")]
			Idle,
			// Token: 0x0400657F RID: 25983
			[Token(Token = "0x4007175")]
			Out,
			// Token: 0x04006580 RID: 25984
			[Token(Token = "0x4007176")]
			End
		}

		// Token: 0x020010B8 RID: 4280
		[Token(Token = "0x2001262")]
		public enum eButton
		{
			// Token: 0x04006582 RID: 25986
			[Token(Token = "0x4007178")]
			None,
			// Token: 0x04006583 RID: 25987
			[Token(Token = "0x4007179")]
			PartyEdit,
			// Token: 0x04006584 RID: 25988
			[Token(Token = "0x400717A")]
			SupportPartyEdit,
			// Token: 0x04006585 RID: 25989
			[Token(Token = "0x400717B")]
			Upgrade,
			// Token: 0x04006586 RID: 25990
			[Token(Token = "0x400717C")]
			LimitBreak,
			// Token: 0x04006587 RID: 25991
			[Token(Token = "0x400717D")]
			Evolution,
			// Token: 0x04006588 RID: 25992
			[Token(Token = "0x400717E")]
			CharaView,
			// Token: 0x04006589 RID: 25993
			[Token(Token = "0x400717F")]
			ViewChange,
			// Token: 0x0400658A RID: 25994
			[Token(Token = "0x4007180")]
			Ability
		}
	}
}
