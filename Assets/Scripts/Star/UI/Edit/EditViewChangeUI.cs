﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010E3 RID: 4323
	[Token(Token = "0x2000B37")]
	[StructLayout(3)]
	public class EditViewChangeUI : MenuUIBase
	{
		// Token: 0x060053E7 RID: 21479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D41")]
		[Address(RVA = "0x101458AE4", Offset = "0x1458AE4", VA = "0x101458AE4")]
		public void Setup(long managedCharaId)
		{
		}

		// Token: 0x060053E8 RID: 21480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D42")]
		[Address(RVA = "0x101458BE4", Offset = "0x1458BE4", VA = "0x101458BE4", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060053E9 RID: 21481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D43")]
		[Address(RVA = "0x101458DD8", Offset = "0x1458DD8", VA = "0x101458DD8", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060053EA RID: 21482 RVA: 0x0001C728 File Offset: 0x0001A928
		[Token(Token = "0x6004D44")]
		[Address(RVA = "0x101458EFC", Offset = "0x1458EFC", VA = "0x101458EFC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060053EB RID: 21483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D45")]
		[Address(RVA = "0x101458F0C", Offset = "0x1458F0C", VA = "0x101458F0C")]
		private void Update()
		{
		}

		// Token: 0x060053EC RID: 21484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D46")]
		[Address(RVA = "0x101458CA0", Offset = "0x1458CA0", VA = "0x101458CA0")]
		private void ChangeStep(EditViewChangeUI.eStep step)
		{
		}

		// Token: 0x060053ED RID: 21485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D47")]
		[Address(RVA = "0x101459080", Offset = "0x1459080", VA = "0x101459080")]
		public void CallBackViewCharaIDSet()
		{
		}

		// Token: 0x060053EE RID: 21486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D48")]
		[Address(RVA = "0x10145908C", Offset = "0x145908C", VA = "0x10145908C")]
		public EditViewChangeUI()
		{
		}

		// Token: 0x04006625 RID: 26149
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40047AE")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04006626 RID: 26150
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40047AF")]
		[SerializeField]
		private ViewSelectWindow m_SelectWindow;

		// Token: 0x04006627 RID: 26151
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40047B0")]
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04006628 RID: 26152
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40047B1")]
		private EditViewChangeUI.eStep m_Step;

		// Token: 0x020010E4 RID: 4324
		[Token(Token = "0x200126D")]
		private enum eStep
		{
			// Token: 0x0400662A RID: 26154
			[Token(Token = "0x40071AC")]
			Hide,
			// Token: 0x0400662B RID: 26155
			[Token(Token = "0x40071AD")]
			In,
			// Token: 0x0400662C RID: 26156
			[Token(Token = "0x40071AE")]
			Idle,
			// Token: 0x0400662D RID: 26157
			[Token(Token = "0x40071AF")]
			Out,
			// Token: 0x0400662E RID: 26158
			[Token(Token = "0x40071B0")]
			End
		}
	}
}
