﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200108B RID: 4235
	[Token(Token = "0x2000AFC")]
	[StructLayout(3)]
	public class EvolutionItemInformation : MonoBehaviour
	{
		// Token: 0x06005196 RID: 20886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B05")]
		[Address(RVA = "0x101459094", Offset = "0x1459094", VA = "0x101459094")]
		public void Apply(int itemID, int havenum, int usenum)
		{
		}

		// Token: 0x06005197 RID: 20887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B06")]
		[Address(RVA = "0x10145934C", Offset = "0x145934C", VA = "0x10145934C")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06005198 RID: 20888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B07")]
		[Address(RVA = "0x101459388", Offset = "0x1459388", VA = "0x101459388")]
		private void Update()
		{
		}

		// Token: 0x06005199 RID: 20889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B08")]
		[Address(RVA = "0x101459424", Offset = "0x1459424", VA = "0x101459424")]
		public EvolutionItemInformation()
		{
		}

		// Token: 0x04006400 RID: 25600
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400463B")]
		[SerializeField]
		private ItemIconWithDetail m_ItemDetail;

		// Token: 0x04006401 RID: 25601
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400463C")]
		[SerializeField]
		private TextField m_NeedNumText;

		// Token: 0x04006402 RID: 25602
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400463D")]
		[SerializeField]
		private TextField m_HaveNumText;
	}
}
