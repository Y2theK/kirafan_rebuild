﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x02001068 RID: 4200
	[Token(Token = "0x2000AE7")]
	[StructLayout(3)]
	public class LevelInformation
	{
		// Token: 0x0600511E RID: 20766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A9D")]
		[Address(RVA = "0x10145942C", Offset = "0x145942C", VA = "0x10145942C")]
		public LevelInformation(int now, int max)
		{
		}

		// Token: 0x0600511F RID: 20767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A9E")]
		[Address(RVA = "0x101459464", Offset = "0x1459464", VA = "0x101459464")]
		public LevelInformation()
		{
		}

		// Token: 0x04006399 RID: 25497
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40045F3")]
		public int m_Now;

		// Token: 0x0400639A RID: 25498
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40045F4")]
		public int m_Max;
	}
}
