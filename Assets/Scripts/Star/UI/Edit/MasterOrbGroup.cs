﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010BA RID: 4282
	[Token(Token = "0x2000B18")]
	[StructLayout(3)]
	public class MasterOrbGroup : UIGroup
	{
		// Token: 0x060052EC RID: 21228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C52")]
		[Address(RVA = "0x101459490", Offset = "0x1459490", VA = "0x101459490")]
		public void Setup()
		{
		}

		// Token: 0x060052ED RID: 21229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C53")]
		[Address(RVA = "0x101459A94", Offset = "0x1459A94", VA = "0x101459A94")]
		public void Open(int targetPartyIndex, Action onClose)
		{
		}

		// Token: 0x060052EE RID: 21230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C54")]
		[Address(RVA = "0x101459AC8", Offset = "0x1459AC8", VA = "0x101459AC8", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060052EF RID: 21231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C55")]
		[Address(RVA = "0x10145A4C4", Offset = "0x145A4C4", VA = "0x10145A4C4", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060052F0 RID: 21232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C56")]
		[Address(RVA = "0x10145A5A8", Offset = "0x145A5A8", VA = "0x10145A5A8")]
		public void OnClickEquipCallBack()
		{
		}

		// Token: 0x060052F1 RID: 21233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C57")]
		[Address(RVA = "0x10145A7E4", Offset = "0x145A7E4", VA = "0x10145A7E4")]
		private void OnConfirmEquipCallBack(int answer)
		{
		}

		// Token: 0x060052F2 RID: 21234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C58")]
		[Address(RVA = "0x10145A90C", Offset = "0x145A90C", VA = "0x10145A90C")]
		private void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x060052F3 RID: 21235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C59")]
		[Address(RVA = "0x101459E64", Offset = "0x1459E64", VA = "0x101459E64")]
		private void SelectOrb(int orbId)
		{
		}

		// Token: 0x060052F4 RID: 21236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5A")]
		[Address(RVA = "0x10145A990", Offset = "0x145A990", VA = "0x10145A990")]
		private void UpdateSkillDisp()
		{
		}

		// Token: 0x060052F5 RID: 21237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5B")]
		[Address(RVA = "0x10145B2D8", Offset = "0x145B2D8", VA = "0x10145B2D8")]
		private void OnSelectSkill(int idx)
		{
		}

		// Token: 0x060052F6 RID: 21238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5C")]
		[Address(RVA = "0x10145B2E0", Offset = "0x145B2E0", VA = "0x10145B2E0")]
		public MasterOrbGroup()
		{
		}

		// Token: 0x0400658B RID: 25995
		[Token(Token = "0x400473E")]
		private const int SKILL_MAX = 3;

		// Token: 0x0400658C RID: 25996
		[Token(Token = "0x400473F")]
		private const int SKILL_IDX_START = 1;

		// Token: 0x0400658D RID: 25997
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004740")]
		[SerializeField]
		private Text m_OrbNameText;

		// Token: 0x0400658E RID: 25998
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004741")]
		[SerializeField]
		private Text m_OrbLvText;

		// Token: 0x0400658F RID: 25999
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004742")]
		[SerializeField]
		private Text m_OrbSkillUseTypeText;

		// Token: 0x04006590 RID: 26000
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004743")]
		[SerializeField]
		private OrbIcon m_OrbIcon;

		// Token: 0x04006591 RID: 26001
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004744")]
		[SerializeField]
		private SkillInfoMasterOrb m_SkillInfo;

		// Token: 0x04006592 RID: 26002
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004745")]
		[SerializeField]
		private SelectedSkillIcon m_SelectedSkillIconPrefab;

		// Token: 0x04006593 RID: 26003
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004746")]
		[SerializeField]
		private RectTransform m_SelectedSkillParent;

		// Token: 0x04006594 RID: 26004
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004747")]
		[SerializeField]
		private ScrollViewBase m_OrbScroll;

		// Token: 0x04006595 RID: 26005
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004748")]
		[SerializeField]
		private CustomButton m_EquipButton;

		// Token: 0x04006596 RID: 26006
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004749")]
		[SerializeField]
		private RectTransform[] m_SkillIndexIcons;

		// Token: 0x04006597 RID: 26007
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400474A")]
		private SelectedSkillIcon[] m_SelectedSkillIcons;

		// Token: 0x04006598 RID: 26008
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400474B")]
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x04006599 RID: 26009
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400474C")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x0400659A RID: 26010
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400474D")]
		private Action m_OnClose;

		// Token: 0x0400659B RID: 26011
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400474E")]
		protected int m_TargetPartyIndex;

		// Token: 0x0400659C RID: 26012
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x400474F")]
		private int m_SelectOrbId;

		// Token: 0x0400659D RID: 26013
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004750")]
		private int m_SelectSkillIdx;
	}
}
