﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010BD RID: 4285
	[Token(Token = "0x2000B1A")]
	[StructLayout(3)]
	public class MasterOrbPanel : MonoBehaviour
	{
		// Token: 0x060052FB RID: 21243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5F")]
		[Address(RVA = "0x10145B2F0", Offset = "0x145B2F0", VA = "0x10145B2F0")]
		public void Setup()
		{
		}

		// Token: 0x060052FC RID: 21244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C60")]
		[Address(RVA = "0x10145B374", Offset = "0x145B374", VA = "0x10145B374")]
		public void SetTargetPartyIndex(int partyIdx)
		{
		}

		// Token: 0x060052FD RID: 21245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C61")]
		[Address(RVA = "0x10145B37C", Offset = "0x145B37C", VA = "0x10145B37C")]
		public void ApplyTargetPartyData()
		{
		}

		// Token: 0x060052FE RID: 21246 RVA: 0x0001C4A0 File Offset: 0x0001A6A0
		[Token(Token = "0x6004C62")]
		[Address(RVA = "0x10145B510", Offset = "0x145B510", VA = "0x10145B510")]
		public long GetCurrentMasterOrbManagedID()
		{
			return 0L;
		}

		// Token: 0x060052FF RID: 21247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C63")]
		[Address(RVA = "0x10145B590", Offset = "0x145B590", VA = "0x10145B590")]
		private void Prev()
		{
		}

		// Token: 0x06005300 RID: 21248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C64")]
		[Address(RVA = "0x10145B718", Offset = "0x145B718", VA = "0x10145B718")]
		private void Next()
		{
		}

		// Token: 0x06005301 RID: 21249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C65")]
		[Address(RVA = "0x10145B8A0", Offset = "0x145B8A0", VA = "0x10145B8A0")]
		public void OnClickLeftButton()
		{
		}

		// Token: 0x06005302 RID: 21250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C66")]
		[Address(RVA = "0x10145B8A4", Offset = "0x145B8A4", VA = "0x10145B8A4")]
		public void OnClickRightButton()
		{
		}

		// Token: 0x06005303 RID: 21251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C67")]
		[Address(RVA = "0x10145B8A8", Offset = "0x145B8A8", VA = "0x10145B8A8")]
		public MasterOrbPanel()
		{
		}

		// Token: 0x040065A7 RID: 26023
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004756")]
		[SerializeField]
		private OrbIcon m_Icon;

		// Token: 0x040065A8 RID: 26024
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004757")]
		[SerializeField]
		private CustomButton m_LeftButton;

		// Token: 0x040065A9 RID: 26025
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004758")]
		[SerializeField]
		private CustomButton m_RightButton;

		// Token: 0x040065AA RID: 26026
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004759")]
		private int m_TargetPartyIndex;

		// Token: 0x040065AB RID: 26027
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400475A")]
		private int m_OrbIndex;

		// Token: 0x040065AC RID: 26028
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400475B")]
		private List<UserMasterOrbData> m_UserMasterOrbDatas;
	}
}
