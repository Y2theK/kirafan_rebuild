﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010BB RID: 4283
	[Token(Token = "0x2000B19")]
	[StructLayout(3)]
	public class OrbListItem : ScrollItemIcon
	{
		// Token: 0x060052F7 RID: 21239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5D")]
		[Address(RVA = "0x10145B8F4", Offset = "0x145B8F4", VA = "0x10145B8F4", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060052F8 RID: 21240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C5E")]
		[Address(RVA = "0x10145BD5C", Offset = "0x145BD5C", VA = "0x10145BD5C")]
		public OrbListItem()
		{
		}

		// Token: 0x0400659E RID: 26014
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004751")]
		[SerializeField]
		private OrbIcon m_OrbIcon;

		// Token: 0x0400659F RID: 26015
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004752")]
		[SerializeField]
		private Text m_OrbNameText;

		// Token: 0x040065A0 RID: 26016
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004753")]
		[SerializeField]
		private Text m_OrbLvText;

		// Token: 0x040065A1 RID: 26017
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004754")]
		[SerializeField]
		private GameObject m_IsEquipped;

		// Token: 0x040065A2 RID: 26018
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004755")]
		[SerializeField]
		private GameObject m_IsSelected;

		// Token: 0x020010BC RID: 4284
		[Token(Token = "0x2001263")]
		public class OrbListItemData : ScrollItemData
		{
			// Token: 0x060052F9 RID: 21241 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600634A")]
			[Address(RVA = "0x10145BD64", Offset = "0x145BD64", VA = "0x10145BD64", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x060052FA RID: 21242 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600634B")]
			[Address(RVA = "0x101459864", Offset = "0x1459864", VA = "0x101459864")]
			public OrbListItemData()
			{
			}

			// Token: 0x040065A3 RID: 26019
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007181")]
			public int m_OrbId;

			// Token: 0x040065A4 RID: 26020
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4007182")]
			public bool m_IsEquipped;

			// Token: 0x040065A5 RID: 26021
			[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
			[Token(Token = "0x4007183")]
			public bool m_IsSelected;

			// Token: 0x040065A6 RID: 26022
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007184")]
			public Action<int> m_OnClick;
		}
	}
}
