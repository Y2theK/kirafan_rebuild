﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010CF RID: 4303
	[Token(Token = "0x2000B28")]
	[StructLayout(3)]
	public class PartyCharaPanelBase : EnumerationCharaPanelAdapterExGen<PartyEditPartyPanelBase>
	{
		// Token: 0x0600536B RID: 21355 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CC9")]
		[Address(RVA = "0x10145BDCC", Offset = "0x145BDCC", VA = "0x10145BDCC", Slot = "7")]
		public override EnumerationCharaPanel CreateCore()
		{
			return null;
		}

		// Token: 0x0600536C RID: 21356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCA")]
		[Address(RVA = "0x10145BE30", Offset = "0x145BE30", VA = "0x10145BE30")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x0600536D RID: 21357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCB")]
		[Address(RVA = "0x10145BF10", Offset = "0x145BF10", VA = "0x10145BF10")]
		public void OnClickWeaponCallBack()
		{
		}

		// Token: 0x0600536E RID: 21358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCC")]
		[Address(RVA = "0x10145BFF0", Offset = "0x145BFF0", VA = "0x10145BFF0")]
		public void OnHoldCharaCallBack()
		{
		}

		// Token: 0x0600536F RID: 21359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCD")]
		[Address(RVA = "0x10145C0D0", Offset = "0x145C0D0", VA = "0x10145C0D0")]
		public PartyCharaPanelBase()
		{
		}

		// Token: 0x020010D0 RID: 4304
		[Token(Token = "0x2001268")]
		public enum eButton
		{
			// Token: 0x040065EB RID: 26091
			[Token(Token = "0x4007191")]
			Chara,
			// Token: 0x040065EC RID: 26092
			[Token(Token = "0x4007192")]
			Weapon
		}
	}
}
