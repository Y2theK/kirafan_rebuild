﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010D1 RID: 4305
	[Token(Token = "0x2000B29")]
	[StructLayout(3)]
	public class PartyDetailCharaPanel : MonoBehaviour
	{
		// Token: 0x06005370 RID: 21360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCE")]
		[Address(RVA = "0x10145C120", Offset = "0x145C120", VA = "0x10145C120")]
		public void Clear()
		{
		}

		// Token: 0x06005371 RID: 21361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CCF")]
		[Address(RVA = "0x10145C154", Offset = "0x145C154", VA = "0x10145C154")]
		public void Apply(UserCharacterData userCharaData, UserWeaponData userWeaponData, UserMasterOrbData userOrbData, UserAbilityData userAbilityData)
		{
		}

		// Token: 0x06005372 RID: 21362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD0")]
		[Address(RVA = "0x10145CAD0", Offset = "0x145CAD0", VA = "0x10145CAD0")]
		public void Apply(UserSupportData userSupportData, UserMasterOrbData userOrbData)
		{
		}

		// Token: 0x06005373 RID: 21363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD1")]
		[Address(RVA = "0x10145C6E0", Offset = "0x145C6E0", VA = "0x10145C6E0")]
		private void ApplyParam(int charaID, int weaponID, int maxLv, int charaCost, OutputCharaParam param)
		{
		}

		// Token: 0x06005374 RID: 21364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD2")]
		[Address(RVA = "0x10145CFE8", Offset = "0x145CFE8", VA = "0x10145CFE8")]
		public PartyDetailCharaPanel()
		{
		}

		// Token: 0x040065ED RID: 26093
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400478E")]
		[SerializeField]
		private GameObject m_CharaObj;

		// Token: 0x040065EE RID: 26094
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400478F")]
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x040065EF RID: 26095
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004790")]
		[SerializeField]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x040065F0 RID: 26096
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004791")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x040065F1 RID: 26097
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004792")]
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x040065F2 RID: 26098
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004793")]
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x040065F3 RID: 26099
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004794")]
		[SerializeField]
		private Text m_LvText;

		// Token: 0x040065F4 RID: 26100
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004795")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x040065F5 RID: 26101
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004796")]
		[SerializeField]
		private CharaStatus m_CharaStatus;
	}
}
