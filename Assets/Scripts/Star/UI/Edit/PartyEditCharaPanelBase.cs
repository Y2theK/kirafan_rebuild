﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010D3 RID: 4307
	[Token(Token = "0x2000B2B")]
	[StructLayout(3)]
	public class PartyEditCharaPanelBase : PartyCharaPanelBase
	{
		// Token: 0x06005378 RID: 21368 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CD6")]
		[Address(RVA = "0x10145CFF0", Offset = "0x145CFF0", VA = "0x10145CFF0", Slot = "7")]
		public override EnumerationCharaPanel CreateCore()
		{
			return null;
		}

		// Token: 0x06005379 RID: 21369 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CD7")]
		[Address(RVA = "0x10145D054", Offset = "0x145D054", VA = "0x10145D054")]
		public Image GetFrame()
		{
			return null;
		}

		// Token: 0x0600537A RID: 21370 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CD8")]
		[Address(RVA = "0x10145D05C", Offset = "0x145D05C", VA = "0x10145D05C")]
		public Sprite[] GetRareFrameSprites()
		{
			return null;
		}

		// Token: 0x0600537B RID: 21371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD9")]
		[Address(RVA = "0x10145BDC8", Offset = "0x145BDC8", VA = "0x10145BDC8")]
		public PartyEditCharaPanelBase()
		{
		}

		// Token: 0x040065F6 RID: 26102
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004797")]
		[SerializeField]
		private Image m_Frame;

		// Token: 0x040065F7 RID: 26103
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004798")]
		[SerializeField]
		private Sprite[] m_RareFrameSprites;
	}
}
