﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010D2 RID: 4306
	[Token(Token = "0x2000B2A")]
	[StructLayout(3)]
	public class PartyEditCharaPanelBaseCore : PartyCharaPanelBaseCore
	{
		// Token: 0x06005375 RID: 21365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD3")]
		[Address(RVA = "0x10145D064", Offset = "0x145D064", VA = "0x10145D064", Slot = "6")]
		protected override void ApplyProcess(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x06005376 RID: 21366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD4")]
		[Address(RVA = "0x10145D164", Offset = "0x145D164", VA = "0x10145D164")]
		protected void ApplyFrameImage(int rare)
		{
		}

		// Token: 0x06005377 RID: 21367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CD5")]
		[Address(RVA = "0x10145D04C", Offset = "0x145D04C", VA = "0x10145D04C")]
		public PartyEditCharaPanelBaseCore()
		{
		}
	}
}
