﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C0 RID: 4288
	[Token(Token = "0x2000B1C")]
	[StructLayout(3)]
	public class PartyEditControlPanel : MonoBehaviour
	{
		// Token: 0x06005307 RID: 21255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C69")]
		[Address(RVA = "0x10145D424", Offset = "0x145D424", VA = "0x10145D424")]
		public void Setup(PartyEditControlPanel.eMode mode, PartyEditMainGroup mainGroup)
		{
		}

		// Token: 0x06005308 RID: 21256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C6A")]
		[Address(RVA = "0x10145D960", Offset = "0x145D960", VA = "0x10145D960")]
		private void ApplyEventBonus()
		{
		}

		// Token: 0x06005309 RID: 21257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C6B")]
		[Address(RVA = "0x10145DB8C", Offset = "0x145DB8C", VA = "0x10145DB8C")]
		public void OnChangePage()
		{
		}

		// Token: 0x0600530A RID: 21258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C6C")]
		[Address(RVA = "0x10145DCC8", Offset = "0x145DCC8", VA = "0x10145DCC8")]
		public void OnADVSkipToggleCallBack()
		{
		}

		// Token: 0x0600530B RID: 21259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C6D")]
		[Address(RVA = "0x10145DD8C", Offset = "0x145DD8C", VA = "0x10145DD8C")]
		public void UpdateUseStamina()
		{
		}

		// Token: 0x0600530C RID: 21260 RVA: 0x0001C4D0 File Offset: 0x0001A6D0
		[Token(Token = "0x6004C6E")]
		[Address(RVA = "0x10145DDBC", Offset = "0x145DDBC", VA = "0x10145DDBC")]
		public long GetCurrentMasterOrbManagedId()
		{
			return 0L;
		}

		// Token: 0x0600530D RID: 21261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C6F")]
		[Address(RVA = "0x10145DDE8", Offset = "0x145DDE8", VA = "0x10145DDE8")]
		public void UpdateMasterOrb()
		{
		}

		// Token: 0x0600530E RID: 21262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C70")]
		[Address(RVA = "0x10145DE14", Offset = "0x145DE14", VA = "0x10145DE14")]
		public PartyEditControlPanel()
		{
		}

		// Token: 0x040065AE RID: 26030
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400475C")]
		[SerializeField]
		private GameObject m_EventBonusObj;

		// Token: 0x040065AF RID: 26031
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400475D")]
		[SerializeField]
		private GameObject m_ElementObj;

		// Token: 0x040065B0 RID: 26032
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400475E")]
		[SerializeField]
		private GameObject m_EnemyTypeObj;

		// Token: 0x040065B1 RID: 26033
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400475F")]
		[SerializeField]
		private GameObject m_MasterOrbPanelObj;

		// Token: 0x040065B2 RID: 26034
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004760")]
		[SerializeField]
		private GameObject m_ResetObj;

		// Token: 0x040065B3 RID: 26035
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004761")]
		[SerializeField]
		private GameObject m_DetailObj;

		// Token: 0x040065B4 RID: 26036
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004762")]
		[SerializeField]
		private GameObject m_RecommendObj;

		// Token: 0x040065B5 RID: 26037
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004763")]
		[SerializeField]
		private GameObject m_ToggleObj;

		// Token: 0x040065B6 RID: 26038
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004764")]
		[SerializeField]
		private GameObject m_DecideButtonObj;

		// Token: 0x040065B7 RID: 26039
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004765")]
		[SerializeField]
		private GameObject m_StartButtonObj;

		// Token: 0x040065B8 RID: 26040
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004766")]
		[SerializeField]
		private ItemDropExtDisplay m_DropDisp;

		// Token: 0x040065B9 RID: 26041
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004767")]
		[SerializeField]
		private ElementIconList m_EnemyTypeElementList;

		// Token: 0x040065BA RID: 26042
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004768")]
		[SerializeField]
		private MasterOrbPanel m_OrbPanel;

		// Token: 0x040065BB RID: 26043
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004769")]
		[SerializeField]
		private CustomButton m_ResetButton;

		// Token: 0x040065BC RID: 26044
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400476A")]
		[SerializeField]
		private ToggleSwitch m_ADVSkipToggle;

		// Token: 0x040065BD RID: 26045
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400476B")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040065BE RID: 26046
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400476C")]
		[SerializeField]
		private QuestStartButtonPanel m_StartButtonPanel;

		// Token: 0x040065BF RID: 26047
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400476D")]
		private PartyEditMainGroup m_MainGroup;

		// Token: 0x020010C1 RID: 4289
		[Token(Token = "0x2001265")]
		public enum eMode
		{
			// Token: 0x040065C1 RID: 26049
			[Token(Token = "0x4007187")]
			Edit,
			// Token: 0x040065C2 RID: 26050
			[Token(Token = "0x4007188")]
			Quest,
			// Token: 0x040065C3 RID: 26051
			[Token(Token = "0x4007189")]
			Confirm
		}
	}
}
