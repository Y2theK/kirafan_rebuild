﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C2 RID: 4290
	[Token(Token = "0x2000B1D")]
	[StructLayout(3)]
	public class PartyEditMainGroup : PartyEditMainGroupBase
	{
		// Token: 0x0600530F RID: 21263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C71")]
		[Address(RVA = "0x10145DE1C", Offset = "0x145DE1C", VA = "0x10145DE1C", Slot = "18")]
		public override void Setup(PartyEditUIBase.eMode mode, PartyEditUIBase ownerUI, int startPartyIndex, EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x06005310 RID: 21264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C72")]
		[Address(RVA = "0x10145E61C", Offset = "0x145E61C", VA = "0x10145E61C")]
		public void SetSupportCharaDataWrapper(CharacterDataWrapperBase cdwb)
		{
		}

		// Token: 0x06005311 RID: 21265 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C73")]
		[Address(RVA = "0x10145E624", Offset = "0x145E624", VA = "0x10145E624", Slot = "20")]
		protected override EquipWeaponPartiesController GetPartyCtrl()
		{
			return null;
		}

		// Token: 0x06005312 RID: 21266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C74")]
		[Address(RVA = "0x10145E6C4", Offset = "0x145E6C4", VA = "0x10145E6C4", Slot = "21")]
		public override void OnChangePage()
		{
		}

		// Token: 0x06005313 RID: 21267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C75")]
		[Address(RVA = "0x10145E6F4", Offset = "0x145E6F4", VA = "0x10145E6F4")]
		public void OpenRecommendedOrganizationButton()
		{
		}

		// Token: 0x06005314 RID: 21268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C76")]
		[Address(RVA = "0x10145E7DC", Offset = "0x145E7DC", VA = "0x10145E7DC")]
		public void ExecuteRecommended()
		{
		}

		// Token: 0x06005315 RID: 21269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C77")]
		[Address(RVA = "0x10145ED34", Offset = "0x145ED34", VA = "0x10145ED34")]
		public void UpdateUseStamina()
		{
		}

		// Token: 0x06005316 RID: 21270 RVA: 0x0001C4E8 File Offset: 0x0001A6E8
		[Token(Token = "0x6004C78")]
		[Address(RVA = "0x10145ED60", Offset = "0x145ED60", VA = "0x10145ED60")]
		public long GetCurrentMasterOrbManagedId()
		{
			return 0L;
		}

		// Token: 0x06005317 RID: 21271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C79")]
		[Address(RVA = "0x10145ED8C", Offset = "0x145ED8C", VA = "0x10145ED8C")]
		public void UpdateMasterOrb()
		{
		}

		// Token: 0x06005318 RID: 21272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C7A")]
		[Address(RVA = "0x10145EDB8", Offset = "0x145EDB8", VA = "0x10145EDB8")]
		public PartyEditMainGroup()
		{
		}

		// Token: 0x040065C4 RID: 26052
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400476E")]
		[SerializeField]
		protected PartyEditControlPanel m_ControlPanel;

		// Token: 0x040065C5 RID: 26053
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400476F")]
		private CharacterDataWrapperBase m_SupportCharaDataWrapper;
	}
}
