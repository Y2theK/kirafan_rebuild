﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010D4 RID: 4308
	[Token(Token = "0x2000B2C")]
	[StructLayout(3)]
	public abstract class PartyEditMainGroupBase : UIGroup
	{
		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x0600537C RID: 21372 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000534")]
		public EquipWeaponPartyMemberController SelectPartyChara
		{
			[Token(Token = "0x6004CDA")]
			[Address(RVA = "0x10145EDC8", Offset = "0x145EDC8", VA = "0x10145EDC8")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000591 RID: 1425
		// (get) Token: 0x0600537D RID: 21373 RVA: 0x0001C5D8 File Offset: 0x0001A7D8
		[Token(Token = "0x17000535")]
		public int SelectPartyIndex
		{
			[Token(Token = "0x6004CDB")]
			[Address(RVA = "0x10145DB2C", Offset = "0x145DB2C", VA = "0x10145DB2C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000592 RID: 1426
		// (get) Token: 0x0600537E RID: 21374 RVA: 0x0001C5F0 File Offset: 0x0001A7F0
		[Token(Token = "0x17000536")]
		public long SelectPartyMngID
		{
			[Token(Token = "0x6004CDC")]
			[Address(RVA = "0x10145EE94", Offset = "0x145EE94", VA = "0x10145EE94")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000593 RID: 1427
		// (get) Token: 0x0600537F RID: 21375 RVA: 0x0001C608 File Offset: 0x0001A808
		[Token(Token = "0x17000537")]
		public int SelectCharaIndex
		{
			[Token(Token = "0x6004CDD")]
			[Address(RVA = "0x10145EF00", Offset = "0x145EF00", VA = "0x10145EF00")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000594 RID: 1428
		// (get) Token: 0x06005380 RID: 21376 RVA: 0x0001C620 File Offset: 0x0001A820
		[Token(Token = "0x17000538")]
		public long SelectCharaMngID
		{
			[Token(Token = "0x6004CDE")]
			[Address(RVA = "0x10145EF28", Offset = "0x145EF28", VA = "0x10145EF28")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06005381 RID: 21377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CDF")]
		[Address(RVA = "0x10145E080", Offset = "0x145E080", VA = "0x10145E080", Slot = "18")]
		public virtual void Setup(PartyEditUIBase.eMode mode, PartyEditUIBase ownerUI, int startPartyIndex, EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x06005382 RID: 21378 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CE0")]
		[Address(RVA = "0x10145F8B8", Offset = "0x145F8B8", VA = "0x10145F8B8", Slot = "19")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06005383 RID: 21379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CE1")]
		[Address(RVA = "0x10145ECEC", Offset = "0x145ECEC", VA = "0x10145ECEC")]
		public void Refresh()
		{
		}

		// Token: 0x06005384 RID: 21380
		[Token(Token = "0x6004CE2")]
		[Address(Slot = "20")]
		protected abstract EquipWeaponPartiesController GetPartyCtrl();

		// Token: 0x06005385 RID: 21381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CE3")]
		[Address(RVA = "0x10145E2F4", Offset = "0x145E2F4", VA = "0x10145E2F4")]
		public void SetOpenNameChangeButtonInteractive(bool Interactive)
		{
		}

		// Token: 0x06005386 RID: 21382 RVA: 0x0001C638 File Offset: 0x0001A838
		[Token(Token = "0x6004CE4")]
		[Address(RVA = "0x10145FB30", Offset = "0x145FB30", VA = "0x10145FB30")]
		public int HowManyChildPanelAllChildren()
		{
			return 0;
		}

		// Token: 0x06005387 RID: 21383 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CE5")]
		[Address(RVA = "0x10145FBA8", Offset = "0x145FBA8", VA = "0x10145FBA8")]
		public PartyPanelData GetSelectedPartyData()
		{
			return null;
		}

		// Token: 0x06005388 RID: 21384 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CE6")]
		[Address(RVA = "0x10145DB58", Offset = "0x145DB58", VA = "0x10145DB58")]
		public PartyPanelData GetPartyData(int idx)
		{
			return null;
		}

		// Token: 0x06005389 RID: 21385 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CE7")]
		[Address(RVA = "0x10145FD28", Offset = "0x145FD28", VA = "0x10145FD28")]
		public string GetSelectedPartyName()
		{
			return null;
		}

		// Token: 0x0600538A RID: 21386 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CE8")]
		[Address(RVA = "0x10145FD94", Offset = "0x145FD94", VA = "0x10145FD94")]
		public string GetPartyName(int partyIndex)
		{
			return null;
		}

		// Token: 0x0600538B RID: 21387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CE9")]
		[Address(RVA = "0x10145FF6C", Offset = "0x145FF6C", VA = "0x10145FF6C")]
		public void SetPartyName(string name)
		{
		}

		// Token: 0x0600538C RID: 21388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CEA")]
		[Address(RVA = "0x10145E6F0", Offset = "0x145E6F0", VA = "0x10145E6F0", Slot = "21")]
		public virtual void OnChangePage()
		{
		}

		// Token: 0x0600538D RID: 21389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CEB")]
		[Address(RVA = "0x10146001C", Offset = "0x146001C", VA = "0x10146001C")]
		public void ForceReleaseDrag()
		{
		}

		// Token: 0x0600538E RID: 21390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CEC")]
		[Address(RVA = "0x10145EDC0", Offset = "0x145EDC0", VA = "0x10145EDC0")]
		protected PartyEditMainGroupBase()
		{
		}

		// Token: 0x040065F8 RID: 26104
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004799")]
		[SerializeField]
		protected PartyScroll m_Scroll;

		// Token: 0x040065F9 RID: 26105
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400479A")]
		[SerializeField]
		protected PartyEditPartyNamePanel m_PartyNamePanel;

		// Token: 0x040065FA RID: 26106
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400479B")]
		protected PartyEditUIBase m_OwnerUI;

		// Token: 0x040065FB RID: 26107
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400479C")]
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x040065FC RID: 26108
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400479D")]
		protected EquipWeaponPartiesController m_Parties;

		// Token: 0x040065FD RID: 26109
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400479E")]
		protected bool m_AddScrollCallback;
	}
}
