﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010D5 RID: 4309
	[Token(Token = "0x2000B2D")]
	[StructLayout(3)]
	public class PartyEditPartyCharaPanelCharacterInformation : EditAdditiveSceneCharacterInformationPanel
	{
		// Token: 0x0600538F RID: 21391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CED")]
		[Address(RVA = "0x101460050", Offset = "0x1460050", VA = "0x101460050", Slot = "8")]
		protected sealed override void ApplyResource(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005390 RID: 21392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CEE")]
		[Address(RVA = "0x1014600A0", Offset = "0x14600A0", VA = "0x1014600A0", Slot = "10")]
		protected virtual void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005391 RID: 21393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CEF")]
		[Address(RVA = "0x101460140", Offset = "0x1460140", VA = "0x101460140", Slot = "11")]
		protected virtual void ApplyResourceNoExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005392 RID: 21394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF0")]
		[Address(RVA = "0x101460174", Offset = "0x1460174", VA = "0x101460174", Slot = "9")]
		protected sealed override void ApplyParameter(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005393 RID: 21395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF1")]
		[Address(RVA = "0x1014601C4", Offset = "0x14601C4", VA = "0x1014601C4", Slot = "12")]
		protected virtual void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005394 RID: 21396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF2")]
		[Address(RVA = "0x101460390", Offset = "0x1460390", VA = "0x101460390", Slot = "13")]
		protected virtual void ApplyParameterNoExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06005395 RID: 21397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF3")]
		[Address(RVA = "0x101460394", Offset = "0x1460394", VA = "0x101460394")]
		public PartyEditPartyCharaPanelCharacterInformation()
		{
		}
	}
}
