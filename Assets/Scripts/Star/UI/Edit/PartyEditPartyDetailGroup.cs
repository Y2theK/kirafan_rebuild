﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C3 RID: 4291
	[Token(Token = "0x2000B1E")]
	[StructLayout(3)]
	public class PartyEditPartyDetailGroup : UIGroup
	{
		// Token: 0x06005319 RID: 21273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C7B")]
		[Address(RVA = "0x10146039C", Offset = "0x146039C", VA = "0x10146039C")]
		public void ApplyUserBattleParty(long partyMngId, int orbId, UserSupportData support)
		{
		}

		// Token: 0x0600531A RID: 21274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C7C")]
		[Address(RVA = "0x1014607B0", Offset = "0x14607B0", VA = "0x1014607B0")]
		public void ApplyUserSupportParty(long supportPartyMngId)
		{
		}

		// Token: 0x0600531B RID: 21275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C7D")]
		[Address(RVA = "0x1014609F0", Offset = "0x14609F0", VA = "0x1014609F0")]
		public void ApplySupportPartyList(List<UserSupportData> list)
		{
		}

		// Token: 0x0600531C RID: 21276 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C7E")]
		[Address(RVA = "0x1014606A4", Offset = "0x14606A4", VA = "0x1014606A4")]
		private PartyDetailCharaPanel GetOrCreateInst(int idx)
		{
			return null;
		}

		// Token: 0x0600531D RID: 21277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C7F")]
		[Address(RVA = "0x101460B00", Offset = "0x1460B00", VA = "0x101460B00")]
		public PartyEditPartyDetailGroup()
		{
		}

		// Token: 0x040065C6 RID: 26054
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004770")]
		[SerializeField]
		private PartyDetailCharaPanel m_PanelPrefab;

		// Token: 0x040065C7 RID: 26055
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004771")]
		[SerializeField]
		private RectTransform m_PanelParent;

		// Token: 0x040065C8 RID: 26056
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004772")]
		private List<PartyDetailCharaPanel> m_InstList;
	}
}
