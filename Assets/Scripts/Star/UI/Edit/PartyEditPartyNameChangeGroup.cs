﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C4 RID: 4292
	[Token(Token = "0x2000B1F")]
	[StructLayout(3)]
	public class PartyEditPartyNameChangeGroup : UIGroup
	{
		// Token: 0x0600531E RID: 21278 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C80")]
		[Address(RVA = "0x101460B70", Offset = "0x1460B70", VA = "0x101460B70")]
		public CustomButton GetDecideButton()
		{
			return null;
		}

		// Token: 0x0600531F RID: 21279 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C81")]
		[Address(RVA = "0x101460B78", Offset = "0x1460B78", VA = "0x101460B78")]
		public string GetInputFieldText()
		{
			return null;
		}

		// Token: 0x06005320 RID: 21280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C82")]
		[Address(RVA = "0x101460BA8", Offset = "0x1460BA8", VA = "0x101460BA8")]
		public void SetInputFieldText(string text)
		{
		}

		// Token: 0x06005321 RID: 21281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C83")]
		[Address(RVA = "0x101460BE0", Offset = "0x1460BE0", VA = "0x101460BE0")]
		public PartyEditPartyNameChangeGroup()
		{
		}

		// Token: 0x040065C9 RID: 26057
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004773")]
		[SerializeField]
		private TextInput m_NameChangeInputFieldEx;

		// Token: 0x040065CA RID: 26058
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004774")]
		[SerializeField]
		private CustomButton m_NameChangeDecideButton;
	}
}
