﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010C5 RID: 4293
	[Token(Token = "0x2000B20")]
	[StructLayout(3)]
	public class PartyEditPartyNamePanel : MonoBehaviour
	{
		// Token: 0x06005322 RID: 21282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C84")]
		[Address(RVA = "0x10145F8B4", Offset = "0x145F8B4", VA = "0x10145F8B4")]
		public void Setup()
		{
		}

		// Token: 0x06005323 RID: 21283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C85")]
		[Address(RVA = "0x101460BE8", Offset = "0x1460BE8", VA = "0x101460BE8")]
		private void Update()
		{
		}

		// Token: 0x06005324 RID: 21284 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C86")]
		[Address(RVA = "0x101460CA0", Offset = "0x1460CA0", VA = "0x101460CA0")]
		public string GetPartyName()
		{
			return null;
		}

		// Token: 0x06005325 RID: 21285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C87")]
		[Address(RVA = "0x101460CD8", Offset = "0x1460CD8", VA = "0x101460CD8")]
		public void SetPartyName(string partyName)
		{
		}

		// Token: 0x06005326 RID: 21286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C88")]
		[Address(RVA = "0x10145FAC0", Offset = "0x145FAC0", VA = "0x10145FAC0")]
		public void SetTouchInteractive(bool interactive)
		{
		}

		// Token: 0x06005327 RID: 21287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C89")]
		[Address(RVA = "0x101460D18", Offset = "0x1460D18", VA = "0x101460D18")]
		public PartyEditPartyNamePanel()
		{
		}

		// Token: 0x040065CB RID: 26059
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004775")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130760", Offset = "0x130760")]
		private Text m_PartyName;

		// Token: 0x040065CC RID: 26060
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004776")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040065CD RID: 26061
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004777")]
		[SerializeField]
		private GameObject m_GoIcon;

		// Token: 0x040065CE RID: 26062
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004778")]
		private int m_RebuildFrames;
	}
}
