﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010D8 RID: 4312
	[Token(Token = "0x2000B2F")]
	[StructLayout(3)]
	public abstract class PartyEditPartyPanelBase : EnumerationPanelBaseExGen<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
	{
		// Token: 0x0600539E RID: 21406 RVA: 0x0001C650 File Offset: 0x0001A850
		[Token(Token = "0x6004CF8")]
		[Address(RVA = "0x101460D28", Offset = "0x1460D28", VA = "0x101460D28")]
		protected bool AcquirePartyScroll()
		{
			return default(bool);
		}

		// Token: 0x0600539F RID: 21407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF9")]
		[Address(RVA = "0x10145BEDC", Offset = "0x145BEDC", VA = "0x10145BEDC")]
		public void OnClickCharaPanelChara(int slotIndex)
		{
		}

		// Token: 0x060053A0 RID: 21408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CFA")]
		[Address(RVA = "0x10145BFBC", Offset = "0x145BFBC", VA = "0x10145BFBC")]
		public void OnClickCharaPanelWeapon(int slotIndex)
		{
		}

		// Token: 0x060053A1 RID: 21409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CFB")]
		[Address(RVA = "0x10145C09C", Offset = "0x145C09C", VA = "0x10145C09C")]
		public void OnHoldCharaPanelChara(int slotIndex)
		{
		}

		// Token: 0x060053A2 RID: 21410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CFC")]
		[Address(RVA = "0x10146105C", Offset = "0x146105C", VA = "0x10146105C")]
		protected PartyEditPartyPanelBase()
		{
		}

		// Token: 0x040065FF RID: 26111
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400479F")]
		protected PartyScroll m_PartyScroll;
	}
}
