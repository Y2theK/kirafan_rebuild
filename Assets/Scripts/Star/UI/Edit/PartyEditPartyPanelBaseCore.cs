﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010D6 RID: 4310
	[Token(Token = "0x2000B2E")]
	[StructLayout(3)]
	public abstract class PartyEditPartyPanelBaseCore : EnumerationPanelCoreGen<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
	{
		// Token: 0x06005396 RID: 21398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF4")]
		[Address(RVA = "0x101460EC4", Offset = "0x1460EC4", VA = "0x101460EC4")]
		public void OnClickCharaPanelChara(int slotIndex)
		{
		}

		// Token: 0x06005397 RID: 21399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF5")]
		[Address(RVA = "0x101460F4C", Offset = "0x1460F4C", VA = "0x101460F4C")]
		public void OnClickCharaPanelWeapon(int slotIndex)
		{
		}

		// Token: 0x06005398 RID: 21400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF6")]
		[Address(RVA = "0x101460FD4", Offset = "0x1460FD4", VA = "0x101460FD4")]
		public void OnHoldCharaPanelChara(int slotIndex)
		{
		}

		// Token: 0x06005399 RID: 21401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CF7")]
		[Address(RVA = "0x1014610AC", Offset = "0x14610AC", VA = "0x1014610AC")]
		protected PartyEditPartyPanelBaseCore()
		{
		}

		// Token: 0x020010D7 RID: 4311
		[Token(Token = "0x2001269")]
		[Serializable]
		public class SharedInstanceExOverrideBase : EnumerationPanelCoreGenBase<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>.SharedInstanceEx<PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
		{
			// Token: 0x0600539A RID: 21402 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006352")]
			[Address(RVA = "0x1014610FC", Offset = "0x14610FC", VA = "0x1014610FC")]
			public SharedInstanceExOverrideBase()
			{
			}

			// Token: 0x0600539B RID: 21403 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006353")]
			[Address(RVA = "0x10146114C", Offset = "0x146114C", VA = "0x10146114C")]
			public SharedInstanceExOverrideBase(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
			}

			// Token: 0x0600539C RID: 21404 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006354")]
			[Address(RVA = "0x1014611FC", Offset = "0x14611FC", VA = "0x1014611FC", Slot = "4")]
			public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase Clone(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				return null;
			}

			// Token: 0x0600539D RID: 21405 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006355")]
			[Address(RVA = "0x1014611C4", Offset = "0x14611C4", VA = "0x1014611C4")]
			private PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CloneNewMemberOnly(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				return null;
			}

			// Token: 0x040065FE RID: 26110
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007193")]
			public PartyScroll m_PartyScroll;
		}
	}
}
