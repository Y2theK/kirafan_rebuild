﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010C8 RID: 4296
	[Token(Token = "0x2000B23")]
	[StructLayout(3)]
	public class PartyEditPartyPanelCore : PartyEditPartyPanelBaseCore
	{
		// Token: 0x0600533C RID: 21308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C9E")]
		[Address(RVA = "0x10146128C", Offset = "0x146128C", VA = "0x10146128C", Slot = "8")]
		public override void Apply(EnumerationPanelData data)
		{
		}

		// Token: 0x0600533D RID: 21309 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C9F")]
		[Address(RVA = "0x1014613A4", Offset = "0x14613A4", VA = "0x1014613A4", Slot = "12")]
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase GetSharedIntance()
		{
			return null;
		}

		// Token: 0x0600533E RID: 21310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CA0")]
		[Address(RVA = "0x1014613AC", Offset = "0x14613AC", VA = "0x1014613AC", Slot = "13")]
		public override void SetSharedIntance(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase arg)
		{
		}

		// Token: 0x0600533F RID: 21311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CA1")]
		[Address(RVA = "0x10146149C", Offset = "0x146149C", VA = "0x10146149C")]
		public PartyEditPartyPanelCore()
		{
		}

		// Token: 0x040065D6 RID: 26070
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004780")]
		protected PartyEditPartyPanelCore.SharedInstanceExOverride m_SharedIntance;

		// Token: 0x020010C9 RID: 4297
		[Token(Token = "0x2001266")]
		[Serializable]
		public class SharedInstanceExOverride : PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase
		{
			// Token: 0x06005340 RID: 21312 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600634E")]
			[Address(RVA = "0x1014614A0", Offset = "0x14614A0", VA = "0x1014614A0")]
			public SharedInstanceExOverride()
			{
			}

			// Token: 0x06005341 RID: 21313 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600634F")]
			[Address(RVA = "0x1014614A4", Offset = "0x14614A4", VA = "0x1014614A4")]
			public SharedInstanceExOverride(PartyEditPartyPanelCore.SharedInstanceExOverride argument)
			{
			}

			// Token: 0x06005342 RID: 21314 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006350")]
			[Address(RVA = "0x1014614AC", Offset = "0x14614AC", VA = "0x1014614AC", Slot = "4")]
			public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase Clone(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				return null;
			}

			// Token: 0x06005343 RID: 21315 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006351")]
			[Address(RVA = "0x1014614A8", Offset = "0x14614A8", VA = "0x1014614A8")]
			private PartyEditPartyPanelCore.SharedInstanceExOverride CloneNewMemberOnly(PartyEditPartyPanelCore.SharedInstanceExOverride argument)
			{
				return null;
			}
		}
	}
}
