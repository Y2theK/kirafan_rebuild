﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C6 RID: 4294
	[Token(Token = "0x2000B21")]
	[StructLayout(3)]
	public class PartyEditQuestStartGroup : UIGroup
	{
		// Token: 0x06005328 RID: 21288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8A")]
		[Address(RVA = "0x1014615A4", Offset = "0x14615A4", VA = "0x1014615A4", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06005329 RID: 21289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8B")]
		[Address(RVA = "0x101461604", Offset = "0x1461604", VA = "0x101461604")]
		public void ShowWarning()
		{
		}

		// Token: 0x0600532A RID: 21290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8C")]
		[Address(RVA = "0x101461664", Offset = "0x1461664", VA = "0x101461664", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600532B RID: 21291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8D")]
		[Address(RVA = "0x101461710", Offset = "0x1461710", VA = "0x101461710")]
		public PartyEditQuestStartGroup()
		{
		}

		// Token: 0x040065CF RID: 26063
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004779")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001307CC", Offset = "0x1307CC")]
		private GameObject m_SortiePartyComfirmed;

		// Token: 0x040065D0 RID: 26064
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400477A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100130818", Offset = "0x130818")]
		private AnimUIPlayer m_UnableEditWarningAnim;

		// Token: 0x040065D1 RID: 26065
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400477B")]
		private float m_Timer;
	}
}
