﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010E1 RID: 4321
	[Token(Token = "0x2000B35")]
	[StructLayout(3)]
	public class PartyEditSupportPartyPanelCore : PartyEditPartyPanelBaseCore
	{
		// Token: 0x060053E1 RID: 21473 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D3B")]
		[Address(RVA = "0x101461718", Offset = "0x1461718", VA = "0x101461718", Slot = "12")]
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase GetSharedIntance()
		{
			return null;
		}

		// Token: 0x060053E2 RID: 21474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D3C")]
		[Address(RVA = "0x101461720", Offset = "0x1461720", VA = "0x101461720", Slot = "13")]
		public override void SetSharedIntance(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase arg)
		{
		}

		// Token: 0x060053E3 RID: 21475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D3D")]
		[Address(RVA = "0x101461728", Offset = "0x1461728", VA = "0x101461728")]
		public PartyEditSupportPartyPanelCore()
		{
		}

		// Token: 0x04006624 RID: 26148
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40047AD")]
		protected PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase m_SharedIntance;
	}
}
