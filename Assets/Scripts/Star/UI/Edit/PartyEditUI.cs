﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010C7 RID: 4295
	[Token(Token = "0x2000B22")]
	[StructLayout(3)]
	public class PartyEditUI : PartyEditUIBase
	{
		// Token: 0x0600532C RID: 21292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8E")]
		[Address(RVA = "0x10146172C", Offset = "0x146172C", VA = "0x10146172C", Slot = "10")]
		public override void Setup(PartyEditUIBase.eMode mode, int startPartyIndex, EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x0600532D RID: 21293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C8F")]
		[Address(RVA = "0x101461860", Offset = "0x1461860", VA = "0x101461860")]
		public void SetupQuest(PartyEditUIBase.eMode mode, int startPartyIndex, UserSupportData supportData)
		{
		}

		// Token: 0x0600532E RID: 21294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C90")]
		[Address(RVA = "0x101461A04", Offset = "0x1461A04", VA = "0x101461A04", Slot = "12")]
		protected override void RemoveMember(int partyIndex, int memberIndex)
		{
		}

		// Token: 0x0600532F RID: 21295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C91")]
		[Address(RVA = "0x101461AF4", Offset = "0x1461AF4", VA = "0x101461AF4", Slot = "13")]
		public override void OpenCharaDetailPlayer()
		{
		}

		// Token: 0x06005330 RID: 21296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C92")]
		[Address(RVA = "0x101461BA4", Offset = "0x1461BA4", VA = "0x101461BA4", Slot = "17")]
		protected override void OpenWeapon(int partyIndex, int slotIndex)
		{
		}

		// Token: 0x06005331 RID: 21297 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004C93")]
		[Address(RVA = "0x101461C30", Offset = "0x1461C30", VA = "0x101461C30", Slot = "14")]
		public override string GetGlobalParamPartyName(long partyMngId)
		{
			return null;
		}

		// Token: 0x06005332 RID: 21298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C94")]
		[Address(RVA = "0x101461CE8", Offset = "0x1461CE8", VA = "0x101461CE8")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x06005333 RID: 21299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C95")]
		[Address(RVA = "0x101461D08", Offset = "0x1461D08", VA = "0x101461D08")]
		public void OnClickRecommendedOrganizationButton()
		{
		}

		// Token: 0x06005334 RID: 21300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C96")]
		[Address(RVA = "0x101461DC0", Offset = "0x1461DC0", VA = "0x101461DC0", Slot = "15")]
		public override void OnClickCharaCallBack()
		{
		}

		// Token: 0x06005335 RID: 21301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C97")]
		[Address(RVA = "0x101461E28", Offset = "0x1461E28", VA = "0x101461E28", Slot = "18")]
		public override void OnClickOpenPartyDetailButton()
		{
		}

		// Token: 0x06005336 RID: 21302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C98")]
		[Address(RVA = "0x101462024", Offset = "0x1462024", VA = "0x101462024")]
		public void UpdateUseStamina()
		{
		}

		// Token: 0x06005337 RID: 21303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C99")]
		[Address(RVA = "0x1014620DC", Offset = "0x14620DC", VA = "0x1014620DC")]
		public void OpenAttributeGroup()
		{
		}

		// Token: 0x06005338 RID: 21304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C9A")]
		[Address(RVA = "0x101462110", Offset = "0x1462110", VA = "0x101462110")]
		public void OpenMasterOrbGroup()
		{
		}

		// Token: 0x06005339 RID: 21305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C9B")]
		[Address(RVA = "0x1014621CC", Offset = "0x14621CC", VA = "0x1014621CC")]
		private void _OpenMasterOrbGroup()
		{
		}

		// Token: 0x0600533A RID: 21306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C9C")]
		[Address(RVA = "0x1014622BC", Offset = "0x14622BC", VA = "0x1014622BC")]
		public void OnCloseMasterOrbGroup()
		{
		}

		// Token: 0x0600533B RID: 21307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C9D")]
		[Address(RVA = "0x101462388", Offset = "0x1462388", VA = "0x101462388")]
		public PartyEditUI()
		{
		}

		// Token: 0x040065D2 RID: 26066
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400477C")]
		[SerializeField]
		private PartyEditQuestStartGroup m_QuestStartGroup;

		// Token: 0x040065D3 RID: 26067
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400477D")]
		[SerializeField]
		private AttributeGroup m_AttributeGroup;

		// Token: 0x040065D4 RID: 26068
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400477E")]
		[SerializeField]
		private MasterOrbGroup m_MasterOrbGroup;

		// Token: 0x040065D5 RID: 26069
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400477F")]
		private UserSupportData m_SupportData;
	}
}
