﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020010D9 RID: 4313
	[Token(Token = "0x2000B30")]
	[StructLayout(3)]
	public abstract class PartyEditUIBase : MenuUIBase
	{
		// Token: 0x060053A3 RID: 21411 RVA: 0x0001C668 File Offset: 0x0001A868
		[Token(Token = "0x6004CFD")]
		[Address(RVA = "0x1014623A8", Offset = "0x14623A8", VA = "0x1014623A8")]
		public static bool ModeToWeaponActive(PartyEditUIBase.eMode mode)
		{
			return default(bool);
		}

		// Token: 0x060053A4 RID: 21412 RVA: 0x0001C680 File Offset: 0x0001A880
		[Token(Token = "0x6004CFE")]
		[Address(RVA = "0x1014623B4", Offset = "0x14623B4", VA = "0x1014623B4")]
		public static EnumerationPanelCoreBase.eMode ConvertPartyEditUIBaseeModeToEnumerationPanelCoreBaseeMode(PartyEditUIBase.eMode mode)
		{
			return EnumerationPanelCoreBase.eMode.Edit;
		}

		// Token: 0x17000595 RID: 1429
		// (get) Token: 0x060053A5 RID: 21413 RVA: 0x0001C698 File Offset: 0x0001A898
		// (set) Token: 0x060053A6 RID: 21414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000539")]
		public PartyEditUIBase.eButton SelectButton
		{
			[Token(Token = "0x6004CFF")]
			[Address(RVA = "0x1014623C4", Offset = "0x14623C4", VA = "0x1014623C4")]
			get
			{
				return PartyEditUIBase.eButton.Back;
			}
			[Token(Token = "0x6004D00")]
			[Address(RVA = "0x1014623CC", Offset = "0x14623CC", VA = "0x1014623CC")]
			set
			{
			}
		}

		// Token: 0x17000596 RID: 1430
		// (get) Token: 0x060053A7 RID: 21415 RVA: 0x0001C6B0 File Offset: 0x0001A8B0
		[Token(Token = "0x1700053A")]
		public int SelectPartyIndex
		{
			[Token(Token = "0x6004D01")]
			[Address(RVA = "0x1014623D4", Offset = "0x14623D4", VA = "0x1014623D4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000597 RID: 1431
		// (get) Token: 0x060053A8 RID: 21416 RVA: 0x0001C6C8 File Offset: 0x0001A8C8
		[Token(Token = "0x1700053B")]
		public long SelectPartyMngID
		{
			[Token(Token = "0x6004D02")]
			[Address(RVA = "0x101462400", Offset = "0x1462400", VA = "0x101462400")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000598 RID: 1432
		// (get) Token: 0x060053A9 RID: 21417 RVA: 0x0001C6E0 File Offset: 0x0001A8E0
		[Token(Token = "0x1700053C")]
		public int SelectCharaIndex
		{
			[Token(Token = "0x6004D03")]
			[Address(RVA = "0x10146242C", Offset = "0x146242C", VA = "0x10146242C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x060053AA RID: 21418 RVA: 0x0001C6F8 File Offset: 0x0001A8F8
		[Token(Token = "0x1700053D")]
		public long SelectCharaMngID
		{
			[Token(Token = "0x6004D04")]
			[Address(RVA = "0x101462458", Offset = "0x1462458", VA = "0x101462458")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x1400011F RID: 287
		// (add) Token: 0x060053AB RID: 21419 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060053AC RID: 21420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011F")]
		public event Action OnClickButton
		{
			[Token(Token = "0x6004D05")]
			[Address(RVA = "0x101462484", Offset = "0x1462484", VA = "0x101462484")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004D06")]
			[Address(RVA = "0x101462590", Offset = "0x1462590", VA = "0x101462590")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000120 RID: 288
		// (add) Token: 0x060053AD RID: 21421 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060053AE RID: 21422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000120")]
		public event Action<string> OnDecideNameChange
		{
			[Token(Token = "0x6004D07")]
			[Address(RVA = "0x10146269C", Offset = "0x146269C", VA = "0x10146269C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004D08")]
			[Address(RVA = "0x1014627A8", Offset = "0x14627A8", VA = "0x1014627A8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060053AF RID: 21423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D09")]
		[Address(RVA = "0x1014617F4", Offset = "0x14617F4", VA = "0x1014617F4", Slot = "10")]
		public virtual void Setup(PartyEditUIBase.eMode mode, int startPartyIndex, EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x060053B0 RID: 21424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D0A")]
		[Address(RVA = "0x101461CFC", Offset = "0x1461CFC", VA = "0x101461CFC")]
		protected void ExecuteOnClickButton()
		{
		}

		// Token: 0x060053B1 RID: 21425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D0B")]
		[Address(RVA = "0x1014628B4", Offset = "0x14628B4", VA = "0x1014628B4")]
		protected void ExecuteOnDecideNameChange(string arg)
		{
		}

		// Token: 0x060053B2 RID: 21426 RVA: 0x0001C710 File Offset: 0x0001A910
		[Token(Token = "0x6004D0C")]
		[Address(RVA = "0x101462914", Offset = "0x1462914", VA = "0x101462914", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060053B3 RID: 21427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D0D")]
		[Address(RVA = "0x101462924", Offset = "0x1462924", VA = "0x101462924")]
		private void Update()
		{
		}

		// Token: 0x060053B4 RID: 21428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D0E")]
		[Address(RVA = "0x101462928", Offset = "0x1462928", VA = "0x101462928")]
		private void UpdateStep()
		{
		}

		// Token: 0x060053B5 RID: 21429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D0F")]
		[Address(RVA = "0x101462C74", Offset = "0x1462C74", VA = "0x101462C74", Slot = "11")]
		protected virtual void UpdateStepEnd()
		{
		}

		// Token: 0x060053B6 RID: 21430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D10")]
		[Address(RVA = "0x101462AA4", Offset = "0x1462AA4", VA = "0x101462AA4")]
		protected void ChangeStep(PartyEditUIBase.eStep step)
		{
		}

		// Token: 0x060053B7 RID: 21431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D11")]
		[Address(RVA = "0x101462D20", Offset = "0x1462D20", VA = "0x101462D20", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060053B8 RID: 21432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D12")]
		[Address(RVA = "0x101462D74", Offset = "0x1462D74", VA = "0x101462D74", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060053B9 RID: 21433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D13")]
		[Address(RVA = "0x101462DC8", Offset = "0x1462DC8", VA = "0x101462DC8")]
		public void Dissolution()
		{
		}

		// Token: 0x060053BA RID: 21434
		[Token(Token = "0x6004D14")]
		[Address(Slot = "12")]
		protected abstract void RemoveMember(int partyIndex, int memberIndex);

		// Token: 0x060053BB RID: 21435
		[Token(Token = "0x6004D15")]
		[Address(Slot = "13")]
		public abstract void OpenCharaDetailPlayer();

		// Token: 0x060053BC RID: 21436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D16")]
		[Address(RVA = "0x101462C78", Offset = "0x1462C78", VA = "0x101462C78")]
		public void SetPartyNameChangeInputFieldText(string partyName)
		{
		}

		// Token: 0x060053BD RID: 21437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D17")]
		[Address(RVA = "0x101462E60", Offset = "0x1462E60", VA = "0x101462E60")]
		public void CloseNameChangeWindow()
		{
		}

		// Token: 0x060053BE RID: 21438 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D18")]
		[Address(RVA = "0x101462ED8", Offset = "0x1462ED8", VA = "0x101462ED8")]
		public string GetPartyName(int partyID)
		{
			return null;
		}

		// Token: 0x060053BF RID: 21439 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D19")]
		[Address(RVA = "0x101462F0C", Offset = "0x1462F0C", VA = "0x101462F0C", Slot = "14")]
		public virtual string GetGlobalParamPartyName(long partyMngId)
		{
			return null;
		}

		// Token: 0x060053C0 RID: 21440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D1A")]
		[Address(RVA = "0x101461E14", Offset = "0x1461E14", VA = "0x101461E14", Slot = "15")]
		public virtual void OnClickCharaCallBack()
		{
		}

		// Token: 0x060053C1 RID: 21441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D1B")]
		[Address(RVA = "0x101462FC4", Offset = "0x1462FC4", VA = "0x101462FC4", Slot = "16")]
		public virtual void OnHoldCharaCallBack()
		{
		}

		// Token: 0x060053C2 RID: 21442
		[Token(Token = "0x6004D1C")]
		[Address(Slot = "17")]
		protected abstract void OpenWeapon(int partyIndex, int slotIndex);

		// Token: 0x060053C3 RID: 21443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D1D")]
		[Address(RVA = "0x1014630B0", Offset = "0x14630B0", VA = "0x1014630B0")]
		public void OnClickWeaponCallBack()
		{
		}

		// Token: 0x060053C4 RID: 21444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D1E")]
		[Address(RVA = "0x10146330C", Offset = "0x146330C", VA = "0x10146330C")]
		public void OnNameChangeButton()
		{
		}

		// Token: 0x060053C5 RID: 21445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D1F")]
		[Address(RVA = "0x101463314", Offset = "0x1463314", VA = "0x101463314")]
		public void OnClickDecideNameChangeButton()
		{
		}

		// Token: 0x060053C6 RID: 21446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D20")]
		[Address(RVA = "0x1014633D0", Offset = "0x14633D0", VA = "0x1014633D0")]
		public void OnCancelNameChangeButton()
		{
		}

		// Token: 0x060053C7 RID: 21447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D21")]
		[Address(RVA = "0x1014633D4", Offset = "0x14633D4", VA = "0x1014633D4")]
		public void OnClickResetButton()
		{
		}

		// Token: 0x060053C8 RID: 21448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D22")]
		[Address(RVA = "0x101463540", Offset = "0x1463540", VA = "0x101463540")]
		public void OnClickCheckResetWindowButton(int index)
		{
		}

		// Token: 0x060053C9 RID: 21449 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D23")]
		[Address(RVA = "0x101461FF0", Offset = "0x1461FF0", VA = "0x101461FF0")]
		public PartyPanelData GetPartyData(int idx)
		{
			return null;
		}

		// Token: 0x060053CA RID: 21450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D24")]
		[Address(RVA = "0x101461FEC", Offset = "0x1461FEC", VA = "0x101461FEC", Slot = "18")]
		public virtual void OnClickOpenPartyDetailButton()
		{
		}

		// Token: 0x060053CB RID: 21451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D25")]
		[Address(RVA = "0x1014635D0", Offset = "0x14635D0", VA = "0x1014635D0")]
		public void OnClickClosePartyDetailButton()
		{
		}

		// Token: 0x060053CC RID: 21452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D26")]
		[Address(RVA = "0x101463670", Offset = "0x1463670", VA = "0x101463670")]
		public void OnChangePage()
		{
		}

		// Token: 0x060053CD RID: 21453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D27")]
		[Address(RVA = "0x101462398", Offset = "0x1462398", VA = "0x101462398")]
		protected PartyEditUIBase()
		{
		}

		// Token: 0x04006600 RID: 26112
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40047A0")]
		[SerializeField]
		protected PartyEditMainGroupBase m_MainGroup;

		// Token: 0x04006601 RID: 26113
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40047A1")]
		[SerializeField]
		private PartyEditPartyNameChangeGroup m_PartyNameChangeGroup;

		// Token: 0x04006602 RID: 26114
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40047A2")]
		[SerializeField]
		protected PartyEditPartyDetailGroup m_PartyDetailGroup;

		// Token: 0x04006603 RID: 26115
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40047A3")]
		private PartyEditUIBase.eStep m_Step;

		// Token: 0x04006604 RID: 26116
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40047A4")]
		protected PartyEditUIBase.eButton m_SelectButton;

		// Token: 0x04006605 RID: 26117
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40047A5")]
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x020010DA RID: 4314
		[Token(Token = "0x200126A")]
		public enum eMode
		{
			// Token: 0x04006609 RID: 26121
			[Token(Token = "0x4007195")]
			Edit,
			// Token: 0x0400660A RID: 26122
			[Token(Token = "0x4007196")]
			Quest,
			// Token: 0x0400660B RID: 26123
			[Token(Token = "0x4007197")]
			QuestStart,
			// Token: 0x0400660C RID: 26124
			[Token(Token = "0x4007198")]
			View,
			// Token: 0x0400660D RID: 26125
			[Token(Token = "0x4007199")]
			PartyDetail
		}

		// Token: 0x020010DB RID: 4315
		[Token(Token = "0x200126B")]
		protected enum eStep
		{
			// Token: 0x0400660F RID: 26127
			[Token(Token = "0x400719B")]
			Hide,
			// Token: 0x04006610 RID: 26128
			[Token(Token = "0x400719C")]
			In,
			// Token: 0x04006611 RID: 26129
			[Token(Token = "0x400719D")]
			Out,
			// Token: 0x04006612 RID: 26130
			[Token(Token = "0x400719E")]
			Idle,
			// Token: 0x04006613 RID: 26131
			[Token(Token = "0x400719F")]
			NameChange,
			// Token: 0x04006614 RID: 26132
			[Token(Token = "0x40071A0")]
			NameChangeClose,
			// Token: 0x04006615 RID: 26133
			[Token(Token = "0x40071A1")]
			ConfirmBack,
			// Token: 0x04006616 RID: 26134
			[Token(Token = "0x40071A2")]
			CharaDetail,
			// Token: 0x04006617 RID: 26135
			[Token(Token = "0x40071A3")]
			Weapon,
			// Token: 0x04006618 RID: 26136
			[Token(Token = "0x40071A4")]
			End,
			// Token: 0x04006619 RID: 26137
			[Token(Token = "0x40071A5")]
			Num
		}

		// Token: 0x020010DC RID: 4316
		[Token(Token = "0x200126C")]
		public enum eButton
		{
			// Token: 0x0400661B RID: 26139
			[Token(Token = "0x40071A7")]
			None = -1,
			// Token: 0x0400661C RID: 26140
			[Token(Token = "0x40071A8")]
			Back,
			// Token: 0x0400661D RID: 26141
			[Token(Token = "0x40071A9")]
			Chara,
			// Token: 0x0400661E RID: 26142
			[Token(Token = "0x40071AA")]
			Decide
		}
	}
}
