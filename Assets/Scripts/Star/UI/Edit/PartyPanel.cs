﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010CA RID: 4298
	[Token(Token = "0x2000B24")]
	[StructLayout(3)]
	public class PartyPanel : PartyEditPartyPanelBase
	{
		// Token: 0x06005344 RID: 21316 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CA2")]
		[Address(RVA = "0x1014636A8", Offset = "0x14636A8", VA = "0x1014636A8", Slot = "7")]
		public override PartyEditPartyPanelBaseCore CreateCore()
		{
			return null;
		}

		// Token: 0x06005345 RID: 21317 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CA3")]
		[Address(RVA = "0x101463700", Offset = "0x1463700", VA = "0x101463700", Slot = "8")]
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CreateArgument()
		{
			return null;
		}

		// Token: 0x06005346 RID: 21318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CA4")]
		[Address(RVA = "0x101463864", Offset = "0x1463864", VA = "0x101463864")]
		public PartyPanel()
		{
		}
	}
}
