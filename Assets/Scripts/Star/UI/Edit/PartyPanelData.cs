﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010CB RID: 4299
	[Token(Token = "0x2000B25")]
	[StructLayout(3)]
	public class PartyPanelData : EnumerationPanelData
	{
		// Token: 0x06005347 RID: 21319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CA5")]
		[Address(RVA = "0x101463868", Offset = "0x1463868", VA = "0x101463868")]
		public PartyPanelData()
		{
		}

		// Token: 0x040065D7 RID: 26071
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004781")]
		public Action<PartyCharaPanelBase.eButton> m_OnClickCharaPanel;

		// Token: 0x040065D8 RID: 26072
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004782")]
		public Action<PartyCharaPanelBase.eButton> m_OnHoldCharaPanel;
	}
}
