﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010CC RID: 4300
	[Token(Token = "0x2000B26")]
	[StructLayout(3)]
	public class PartyScroll : InfiniteScrollEx
	{
		// Token: 0x06005348 RID: 21320 RVA: 0x0001C500 File Offset: 0x0001A700
		[Token(Token = "0x6004CA6")]
		[Address(RVA = "0x101463870", Offset = "0x1463870", VA = "0x101463870")]
		public static bool ModeToEditActive(PartyScroll.eMode mode)
		{
			return default(bool);
		}

		// Token: 0x06005349 RID: 21321 RVA: 0x0001C518 File Offset: 0x0001A718
		[Token(Token = "0x6004CA7")]
		[Address(RVA = "0x101463878", Offset = "0x1463878", VA = "0x101463878")]
		public static bool ModeToScrollActive(PartyScroll.eMode mode)
		{
			return default(bool);
		}

		// Token: 0x0600534A RID: 21322 RVA: 0x0001C530 File Offset: 0x0001A730
		[Token(Token = "0x6004CA8")]
		[Address(RVA = "0x101463884", Offset = "0x1463884", VA = "0x101463884")]
		public static bool ModeToTouchActive(PartyScroll.eMode mode)
		{
			return default(bool);
		}

		// Token: 0x0600534B RID: 21323 RVA: 0x0001C548 File Offset: 0x0001A748
		[Token(Token = "0x6004CA9")]
		[Address(RVA = "0x10146388C", Offset = "0x146388C", VA = "0x10146388C")]
		public static EnumerationPanelCoreBase.eMode ConvertPartyScrolleModeToEnumerationPanelCoreBaseeMode(PartyScroll.eMode mode)
		{
			return EnumerationPanelCoreBase.eMode.Edit;
		}

		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x0600534C RID: 21324 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700052D")]
		public PartyPanelData SelectParty
		{
			[Token(Token = "0x6004CAA")]
			[Address(RVA = "0x10146389C", Offset = "0x146389C", VA = "0x10146389C")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x0600534D RID: 21325 RVA: 0x0001C560 File Offset: 0x0001A760
		[Token(Token = "0x1700052E")]
		public int SelectPartyIdx
		{
			[Token(Token = "0x6004CAB")]
			[Address(RVA = "0x10145ECAC", Offset = "0x145ECAC", VA = "0x10145ECAC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x0600534E RID: 21326 RVA: 0x0001C578 File Offset: 0x0001A778
		[Token(Token = "0x1700052F")]
		public long SelectPartyMngID
		{
			[Token(Token = "0x6004CAC")]
			[Address(RVA = "0x10145EEC0", Offset = "0x145EEC0", VA = "0x10145EEC0")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x0600534F RID: 21327 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000530")]
		public EquipWeaponPartyMemberController SelectPartyChara
		{
			[Token(Token = "0x6004CAD")]
			[Address(RVA = "0x1014639B4", Offset = "0x14639B4", VA = "0x1014639B4")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x06005350 RID: 21328 RVA: 0x0001C590 File Offset: 0x0001A790
		[Token(Token = "0x17000531")]
		public int SelectCharaIdx
		{
			[Token(Token = "0x6004CAE")]
			[Address(RVA = "0x1014639B8", Offset = "0x14639B8", VA = "0x1014639B8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x06005351 RID: 21329 RVA: 0x0001C5A8 File Offset: 0x0001A7A8
		[Token(Token = "0x17000532")]
		public long SelectCharaMngID
		{
			[Token(Token = "0x6004CAF")]
			[Address(RVA = "0x1014639E8", Offset = "0x14639E8", VA = "0x1014639E8")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x06005352 RID: 21330 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000533")]
		public string PartyName
		{
			[Token(Token = "0x6004CB0")]
			[Address(RVA = "0x10145FD54", Offset = "0x145FD54", VA = "0x10145FD54")]
			get
			{
				return null;
			}
		}

		// Token: 0x1400011B RID: 283
		// (add) Token: 0x06005353 RID: 21331 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005354 RID: 21332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011B")]
		public event Action OnChangePage
		{
			[Token(Token = "0x6004CB1")]
			[Address(RVA = "0x10145F480", Offset = "0x145F480", VA = "0x10145F480")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004CB2")]
			[Address(RVA = "0x101463A18", Offset = "0x1463A18", VA = "0x101463A18")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400011C RID: 284
		// (add) Token: 0x06005355 RID: 21333 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005356 RID: 21334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011C")]
		public event Action OnClickChara
		{
			[Token(Token = "0x6004CB3")]
			[Address(RVA = "0x10145F58C", Offset = "0x145F58C", VA = "0x10145F58C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004CB4")]
			[Address(RVA = "0x101463B24", Offset = "0x1463B24", VA = "0x101463B24")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400011D RID: 285
		// (add) Token: 0x06005357 RID: 21335 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005358 RID: 21336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011D")]
		public event Action OnHoldChara
		{
			[Token(Token = "0x6004CB5")]
			[Address(RVA = "0x10145F698", Offset = "0x145F698", VA = "0x10145F698")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004CB6")]
			[Address(RVA = "0x101463C30", Offset = "0x1463C30", VA = "0x101463C30")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400011E RID: 286
		// (add) Token: 0x06005359 RID: 21337 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600535A RID: 21338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400011E")]
		public event Action OnClickWeapon
		{
			[Token(Token = "0x6004CB7")]
			[Address(RVA = "0x10145F7A4", Offset = "0x145F7A4", VA = "0x10145F7A4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004CB8")]
			[Address(RVA = "0x101463D3C", Offset = "0x1463D3C", VA = "0x101463D3C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600535B RID: 21339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CB9")]
		[Address(RVA = "0x10145EF50", Offset = "0x145EF50", VA = "0x10145EF50")]
		public void Setup(EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x0600535C RID: 21340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CBA")]
		[Address(RVA = "0x10145F8EC", Offset = "0x145F8EC", VA = "0x10145F8EC")]
		public void Refresh(EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x0600535D RID: 21341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CBB")]
		[Address(RVA = "0x101463E4C", Offset = "0x1463E4C", VA = "0x101463E4C", Slot = "8")]
		protected override void Update()
		{
		}

		// Token: 0x0600535E RID: 21342 RVA: 0x0001C5C0 File Offset: 0x0001A7C0
		[Token(Token = "0x6004CBC")]
		[Address(RVA = "0x10145FB74", Offset = "0x145FB74", VA = "0x10145FB74")]
		public int HowManyChildPanelAllChildren()
		{
			return 0;
		}

		// Token: 0x0600535F RID: 21343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CBD")]
		[Address(RVA = "0x10145E3A0", Offset = "0x145E3A0", VA = "0x10145E3A0")]
		public void SetEditMode(PartyScroll.eMode mode)
		{
		}

		// Token: 0x06005360 RID: 21344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CBE")]
		[Address(RVA = "0x10145F150", Offset = "0x145F150", VA = "0x10145F150")]
		public void SetSelectPartyIdx(int idx, bool scroll, bool immediate)
		{
		}

		// Token: 0x06005361 RID: 21345 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CBF")]
		[Address(RVA = "0x10145FBE4", Offset = "0x145FBE4", VA = "0x10145FBE4")]
		public PartyPanelData GetPartyData(int idx)
		{
			return null;
		}

		// Token: 0x06005362 RID: 21346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC0")]
		[Address(RVA = "0x10145FFA0", Offset = "0x145FFA0", VA = "0x10145FFA0")]
		public void SetPartyName(string name)
		{
		}

		// Token: 0x06005363 RID: 21347 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CC1")]
		[Address(RVA = "0x10145FDC8", Offset = "0x145FDC8", VA = "0x10145FDC8")]
		public string GetPartyName(int PartyIndex)
		{
			return null;
		}

		// Token: 0x06005364 RID: 21348 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004CC2")]
		[Address(RVA = "0x10145EE64", Offset = "0x145EE64", VA = "0x10145EE64")]
		public EquipWeaponPartyMemberController GetSelectedPartyChara()
		{
			return null;
		}

		// Token: 0x06005365 RID: 21349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC3")]
		[Address(RVA = "0x10145E2A4", Offset = "0x145E2A4", VA = "0x10145E2A4")]
		public void SetPageSignalActive(bool flag)
		{
		}

		// Token: 0x06005366 RID: 21350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC4")]
		[Address(RVA = "0x101464138", Offset = "0x1464138", VA = "0x101464138", Slot = "14")]
		protected override void OnClickScrollButton(int addIdx)
		{
		}

		// Token: 0x06005367 RID: 21351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC5")]
		[Address(RVA = "0x10146416C", Offset = "0x146416C", VA = "0x10146416C")]
		public void OnClickPartyChara(PartyCharaPanelBase.eButton place)
		{
		}

		// Token: 0x06005368 RID: 21352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC6")]
		[Address(RVA = "0x1014643AC", Offset = "0x14643AC", VA = "0x1014643AC")]
		public void OnHoldPartyChara(PartyCharaPanelBase.eButton place)
		{
		}

		// Token: 0x06005369 RID: 21353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004CC7")]
		[Address(RVA = "0x1014643C0", Offset = "0x14643C0", VA = "0x1014643C0")]
		public PartyScroll()
		{
		}

		// Token: 0x040065D9 RID: 26073
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004783")]
		[SerializeField]
		private RectTransform m_Window;

		// Token: 0x040065DA RID: 26074
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004784")]
		[SerializeField]
		private Text m_PartyNameObj;

		// Token: 0x040065DB RID: 26075
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004785")]
		[SerializeField]
		private Text m_PartyCostObj;

		// Token: 0x040065DC RID: 26076
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004786")]
		private PartyScroll.eMode m_Mode;

		// Token: 0x040065DD RID: 26077
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004787")]
		private EquipWeaponPartiesController m_parties;

		// Token: 0x040065DE RID: 26078
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004788")]
		private int m_PreSelectPartyIdx;

		// Token: 0x040065DF RID: 26079
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4004789")]
		private int m_PartyMaxCost;

		// Token: 0x020010CD RID: 4301
		[Token(Token = "0x2001267")]
		public enum eMode
		{
			// Token: 0x040065E5 RID: 26085
			[Token(Token = "0x400718B")]
			Edit,
			// Token: 0x040065E6 RID: 26086
			[Token(Token = "0x400718C")]
			Quest,
			// Token: 0x040065E7 RID: 26087
			[Token(Token = "0x400718D")]
			QuestStart,
			// Token: 0x040065E8 RID: 26088
			[Token(Token = "0x400718E")]
			View,
			// Token: 0x040065E9 RID: 26089
			[Token(Token = "0x400718F")]
			PartyDetail
		}
	}
}
