﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200108C RID: 4236
	[Token(Token = "0x2000AFD")]
	[StructLayout(3)]
	public class SelectItemPanel : MonoBehaviour
	{
		// Token: 0x0600519A RID: 20890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B09")]
		[Address(RVA = "0x1014643D0", Offset = "0x14643D0", VA = "0x1014643D0")]
		public void Setup()
		{
		}

		// Token: 0x0600519B RID: 20891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B0A")]
		[Address(RVA = "0x101464624", Offset = "0x1464624", VA = "0x101464624")]
		public void ResetUseItem()
		{
		}

		// Token: 0x0600519C RID: 20892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B0B")]
		[Address(RVA = "0x1014646FC", Offset = "0x14646FC", VA = "0x1014646FC")]
		public void Apply(List<KeyValuePair<int, int>> list)
		{
		}

		// Token: 0x0600519D RID: 20893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B0C")]
		[Address(RVA = "0x101464A04", Offset = "0x1464A04", VA = "0x101464A04")]
		public void SetActiveSelectItemIcon(bool active)
		{
		}

		// Token: 0x0600519E RID: 20894 RVA: 0x0001BFF0 File Offset: 0x0001A1F0
		[Token(Token = "0x6004B0D")]
		[Address(RVA = "0x101464AD8", Offset = "0x1464AD8", VA = "0x101464AD8")]
		public int GetLineupCount()
		{
			return 0;
		}

		// Token: 0x0600519F RID: 20895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B0E")]
		[Address(RVA = "0x101464AF0", Offset = "0x1464AF0", VA = "0x101464AF0")]
		public void OnClickIconCallBack(int idx)
		{
		}

		// Token: 0x060051A0 RID: 20896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B0F")]
		[Address(RVA = "0x101464BD0", Offset = "0x1464BD0", VA = "0x101464BD0")]
		public SelectItemPanel()
		{
		}

		// Token: 0x04006403 RID: 25603
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400463E")]
		[SerializeField]
		private GameObject[] m_UseItemObjs;

		// Token: 0x04006404 RID: 25604
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400463F")]
		private SelectItemPanel.UseItemIconInfo[] m_UseItemIconInfo;

		// Token: 0x0200108D RID: 4237
		[Token(Token = "0x2001250")]
		public class UseItemIconInfo
		{
			// Token: 0x17000577 RID: 1399
			// (get) Token: 0x060051A1 RID: 20897 RVA: 0x0001C008 File Offset: 0x0001A208
			[Token(Token = "0x170006D1")]
			public int ID
			{
				[Token(Token = "0x6006341")]
				[Address(RVA = "0x101464BC8", Offset = "0x1464BC8", VA = "0x101464BC8")]
				get
				{
					return 0;
				}
			}

			// Token: 0x060051A2 RID: 20898 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006342")]
			[Address(RVA = "0x101464528", Offset = "0x1464528", VA = "0x101464528")]
			public UseItemIconInfo(GameObject gO)
			{
			}

			// Token: 0x060051A3 RID: 20899 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006343")]
			[Address(RVA = "0x101464990", Offset = "0x1464990", VA = "0x101464990")]
			public void ApplyIcon(int id, int num)
			{
			}

			// Token: 0x060051A4 RID: 20900 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006344")]
			[Address(RVA = "0x1014646CC", Offset = "0x14646CC", VA = "0x1014646CC")]
			public void Reset()
			{
			}

			// Token: 0x060051A5 RID: 20901 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006345")]
			[Address(RVA = "0x10146492C", Offset = "0x146492C", VA = "0x10146492C")]
			public void SetActive(bool active)
			{
			}

			// Token: 0x060051A6 RID: 20902 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006346")]
			[Address(RVA = "0x101464AA0", Offset = "0x1464AA0", VA = "0x101464AA0")]
			public void SetActiveItemIcon(bool active)
			{
			}

			// Token: 0x04006405 RID: 25605
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070F9")]
			private GameObject m_GameObject;

			// Token: 0x04006406 RID: 25606
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070FA")]
			private ItemIconWithFrame m_IconComponent;

			// Token: 0x04006407 RID: 25607
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40070FB")]
			private ImageNumbers m_Number;

			// Token: 0x04006408 RID: 25608
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40070FC")]
			private int m_ID;
		}
	}
}
