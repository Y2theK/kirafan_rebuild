﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001063 RID: 4195
	[Token(Token = "0x2000AE4")]
	[StructLayout(3)]
	public class SelectedCharaInfoArousalLv : SelectedCharaInfoHaveTitleField
	{
		// Token: 0x060050E4 RID: 20708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A65")]
		[Address(RVA = "0x101464BD8", Offset = "0x1464BD8", VA = "0x101464BD8")]
		public void SetArousalLv(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060050E5 RID: 20709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A66")]
		[Address(RVA = "0x101464D84", Offset = "0x1464D84", VA = "0x101464D84")]
		private void Start()
		{
		}

		// Token: 0x060050E6 RID: 20710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A67")]
		[Address(RVA = "0x101464D88", Offset = "0x1464D88", VA = "0x101464D88")]
		private void Update()
		{
		}

		// Token: 0x060050E7 RID: 20711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A68")]
		[Address(RVA = "0x101464D8C", Offset = "0x1464D8C", VA = "0x101464D8C")]
		public SelectedCharaInfoArousalLv()
		{
		}

		// Token: 0x0400636C RID: 25452
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045CF")]
		[SerializeField]
		private ArousalLvIcon m_Icon;

		// Token: 0x0400636D RID: 25453
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045D0")]
		[SerializeField]
		private ArousalLvGauge m_AroualExpGauge;

		// Token: 0x0400636E RID: 25454
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40045D1")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x0400636F RID: 25455
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40045D2")]
		[SerializeField]
		private GameObject m_NextTextGameObject;

		// Token: 0x04006370 RID: 25456
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40045D3")]
		[SerializeField]
		private Text m_NextExpValue;

		// Token: 0x04006371 RID: 25457
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40045D4")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EBC4", Offset = "0x12EBC4")]
		private RectTransform m_LayoutGroupTransform;
	}
}
