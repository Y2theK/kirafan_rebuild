﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001065 RID: 4197
	[Token(Token = "0x2000AE6")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterViewController : MonoBehaviour
	{
		// Token: 0x060050FB RID: 20731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7C")]
		[Address(RVA = "0x101464D94", Offset = "0x1464D94", VA = "0x101464D94")]
		public void Setup()
		{
		}

		// Token: 0x060050FC RID: 20732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7D")]
		[Address(RVA = "0x1014651CC", Offset = "0x14651CC", VA = "0x1014651CC")]
		public void SetManualUpdate(bool manualUpdate)
		{
		}

		// Token: 0x060050FD RID: 20733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7E")]
		[Address(RVA = "0x10146535C", Offset = "0x146535C", VA = "0x10146535C")]
		public void Destroy()
		{
		}

		// Token: 0x060050FE RID: 20734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7F")]
		[Address(RVA = "0x1014654E8", Offset = "0x14654E8", VA = "0x1014654E8")]
		private void Update()
		{
		}

		// Token: 0x060050FF RID: 20735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A80")]
		[Address(RVA = "0x1014656BC", Offset = "0x14656BC", VA = "0x1014656BC")]
		public void ManualUpdate()
		{
		}

		// Token: 0x06005100 RID: 20736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A81")]
		[Address(RVA = "0x1014654F8", Offset = "0x14654F8", VA = "0x1014654F8")]
		private void ManualUpdateInternal()
		{
		}

		// Token: 0x06005101 RID: 20737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A82")]
		[Address(RVA = "0x101465B34", Offset = "0x1465B34", VA = "0x101465B34", Slot = "4")]
		public virtual void PlayIn(bool autoIdle = true)
		{
		}

		// Token: 0x06005102 RID: 20738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A83")]
		[Address(RVA = "0x101465B74", Offset = "0x1465B74", VA = "0x101465B74", Slot = "5")]
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
		}

		// Token: 0x06005103 RID: 20739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A84")]
		[Address(RVA = "0x101465E44", Offset = "0x1465E44", VA = "0x101465E44", Slot = "6")]
		public virtual void FinishIn()
		{
		}

		// Token: 0x06005104 RID: 20740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A85")]
		[Address(RVA = "0x10146610C", Offset = "0x146610C", VA = "0x10146610C")]
		public void PlayOut()
		{
		}

		// Token: 0x06005105 RID: 20741 RVA: 0x0001BEE8 File Offset: 0x0001A0E8
		[Token(Token = "0x6004A86")]
		[Address(RVA = "0x1014656CC", Offset = "0x14656CC", VA = "0x1014656CC")]
		public SelectedCharaInfoCharacterViewBase.eState GetState()
		{
			return SelectedCharaInfoCharacterViewBase.eState.Invalid;
		}

		// Token: 0x06005106 RID: 20742 RVA: 0x0001BF00 File Offset: 0x0001A100
		[Token(Token = "0x6004A87")]
		[Address(RVA = "0x1014662AC", Offset = "0x14662AC", VA = "0x1014662AC")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06005107 RID: 20743 RVA: 0x0001BF18 File Offset: 0x0001A118
		[Token(Token = "0x6004A88")]
		[Address(RVA = "0x1014662D0", Offset = "0x14662D0", VA = "0x1014662D0")]
		public bool IsDonePlayOut()
		{
			return default(bool);
		}

		// Token: 0x06005108 RID: 20744 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004A89")]
		[Address(RVA = "0x1014662EC", Offset = "0x14662EC", VA = "0x1014662EC")]
		public List<SelectedCharaInfoCharacterViewController.eViewType> GetActiveViewTypes()
		{
			return null;
		}

		// Token: 0x06005109 RID: 20745 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004A8A")]
		[Address(RVA = "0x1014664C8", Offset = "0x14664C8", VA = "0x1014664C8")]
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair GetViewRequestFlagPair(SelectedCharaInfoCharacterViewController.eViewType viewType)
		{
			return null;
		}

		// Token: 0x0600510A RID: 20746 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004A8B")]
		[Address(RVA = "0x101466468", Offset = "0x1466468", VA = "0x101466468")]
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair GetViewRequestFlagPair(int nViewType)
		{
			return null;
		}

		// Token: 0x0600510B RID: 20747 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004A8C")]
		[Address(RVA = "0x1014650D0", Offset = "0x14650D0", VA = "0x1014650D0")]
		private SelectedCharaInfoCharacterViewBase GetView(int nViewType)
		{
			return null;
		}

		// Token: 0x0600510C RID: 20748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A8D")]
		[Address(RVA = "0x1014664CC", Offset = "0x14664CC", VA = "0x1014664CC")]
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
		}

		// Token: 0x0600510D RID: 20749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A8E")]
		[Address(RVA = "0x10146663C", Offset = "0x146663C", VA = "0x10146663C")]
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, CharacterDataController cdc, bool automaticDisplay = true)
		{
		}

		// Token: 0x0600510E RID: 20750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A8F")]
		[Address(RVA = "0x101466594", Offset = "0x1466594", VA = "0x101466594")]
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, int charaID, bool isUserChara, int weaponID, bool automaticDisplay = true, [Optional] Action callback)
		{
		}

		// Token: 0x0600510F RID: 20751 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A90")]
		[Address(RVA = "0x101466800", Offset = "0x1466800", VA = "0x101466800")]
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
		}

		// Token: 0x06005110 RID: 20752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A91")]
		[Address(RVA = "0x1014668C4", Offset = "0x14668C4", VA = "0x1014668C4")]
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, int charaID, bool isUserChara, int weaponID, bool automaticDisplay = true)
		{
		}

		// Token: 0x06005111 RID: 20753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A92")]
		[Address(RVA = "0x10146592C", Offset = "0x146592C", VA = "0x10146592C")]
		protected void RequestLoadAll()
		{
		}

		// Token: 0x06005112 RID: 20754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A93")]
		[Address(RVA = "0x1014650F4", Offset = "0x14650F4", VA = "0x1014650F4")]
		private void ResetNextRequestFlags()
		{
		}

		// Token: 0x06005113 RID: 20755 RVA: 0x0001BF30 File Offset: 0x0001A130
		[Token(Token = "0x6004A94")]
		[Address(RVA = "0x1014666D8", Offset = "0x14666D8", VA = "0x1014666D8")]
		private int GetDisplayCharaId(int charaId)
		{
			return 0;
		}

		// Token: 0x06005114 RID: 20756 RVA: 0x0001BF48 File Offset: 0x0001A148
		[Token(Token = "0x6004A95")]
		[Address(RVA = "0x101466A78", Offset = "0x1466A78", VA = "0x101466A78")]
		public bool IsDisplayBeforeEvolution()
		{
			return default(bool);
		}

		// Token: 0x06005115 RID: 20757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A96")]
		[Address(RVA = "0x1014650EC", Offset = "0x14650EC", VA = "0x1014650EC")]
		public void SetIsDisplayBeforeEvolution(bool isDisplayBeforeEvolution)
		{
		}

		// Token: 0x06005116 RID: 20758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A97")]
		[Address(RVA = "0x101466A80", Offset = "0x1466A80", VA = "0x101466A80")]
		public void SetLimitBreak(int lb)
		{
		}

		// Token: 0x06005117 RID: 20759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A98")]
		[Address(RVA = "0x101466C0C", Offset = "0x1466C0C", VA = "0x101466C0C")]
		public void SetArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06005118 RID: 20760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A99")]
		[Address(RVA = "0x101466DA8", Offset = "0x1466DA8", VA = "0x101466DA8")]
		public void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06005119 RID: 20761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A9A")]
		[Address(RVA = "0x101466F44", Offset = "0x1466F44", VA = "0x101466F44")]
		public void Replace(int charaID, bool isUserChara)
		{
		}

		// Token: 0x0600511A RID: 20762 RVA: 0x0001BF60 File Offset: 0x0001A160
		[Token(Token = "0x6004A9B")]
		[Address(RVA = "0x101466A54", Offset = "0x1466A54", VA = "0x101466A54")]
		private int GetViewCharaID()
		{
			return 0;
		}

		// Token: 0x0600511B RID: 20763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A9C")]
		[Address(RVA = "0x1014670C8", Offset = "0x14670C8", VA = "0x1014670C8")]
		public SelectedCharaInfoCharacterViewController()
		{
		}

		// Token: 0x04006382 RID: 25474
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045E5")]
		[SerializeField]
		private Image m_ViewFrame;

		// Token: 0x04006383 RID: 25475
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045E6")]
		[SerializeField]
		private SelectedCharaInfoModelArea m_ModelArea;

		// Token: 0x04006384 RID: 25476
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045E7")]
		[SerializeField]
		private SelectedCharaInfoCharacterIllust m_CharacterIllust;

		// Token: 0x04006385 RID: 25477
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40045E8")]
		[SerializeField]
		private SelectedCharaInfoCharacterBustImage m_BustImage;

		// Token: 0x04006386 RID: 25478
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40045E9")]
		[SerializeField]
		private SelectedCharaInfoCharacterIcon m_CharacterIcon;

		// Token: 0x04006387 RID: 25479
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40045EA")]
		[SerializeField]
		private bool m_IsEnableChangeViewCharaID;

		// Token: 0x04006388 RID: 25480
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40045EB")]
		private int m_CharaID;

		// Token: 0x04006389 RID: 25481
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40045EC")]
		private int m_WeaponID;

		// Token: 0x0400638A RID: 25482
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40045ED")]
		private bool m_IsUserChara;

		// Token: 0x0400638B RID: 25483
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40045EE")]
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair[] m_ViewRequestFlagPairs;

		// Token: 0x0400638C RID: 25484
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40045EF")]
		private bool m_AutomaticStateAdvance;

		// Token: 0x0400638D RID: 25485
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40045F0")]
		private Action m_FinishInCallback;

		// Token: 0x0400638E RID: 25486
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40045F1")]
		private bool m_ManualUpdate;

		// Token: 0x0400638F RID: 25487
		[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
		[Token(Token = "0x40045F2")]
		private bool m_IsDisplayBeforeEvolution;

		// Token: 0x02001066 RID: 4198
		[Token(Token = "0x2001241")]
		public enum eViewType
		{
			// Token: 0x04006391 RID: 25489
			[Token(Token = "0x40070D5")]
			Begin,
			// Token: 0x04006392 RID: 25490
			[Token(Token = "0x40070D6")]
			Model = 0,
			// Token: 0x04006393 RID: 25491
			[Token(Token = "0x40070D7")]
			Illust,
			// Token: 0x04006394 RID: 25492
			[Token(Token = "0x40070D8")]
			Bust,
			// Token: 0x04006395 RID: 25493
			[Token(Token = "0x40070D9")]
			Icon,
			// Token: 0x04006396 RID: 25494
			[Token(Token = "0x40070DA")]
			End
		}

		// Token: 0x02001067 RID: 4199
		[Token(Token = "0x2001242")]
		private class ViewRequestFlagPair
		{
			// Token: 0x0600511C RID: 20764 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600632F")]
			[Address(RVA = "0x1014670D8", Offset = "0x14670D8", VA = "0x1014670D8")]
			public ViewRequestFlagPair()
			{
			}

			// Token: 0x0600511D RID: 20765 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006330")]
			[Address(RVA = "0x1014650A0", Offset = "0x14650A0", VA = "0x1014650A0")]
			public ViewRequestFlagPair(SelectedCharaInfoCharacterViewBase charaView)
			{
			}

			// Token: 0x04006397 RID: 25495
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070DB")]
			public SelectedCharaInfoCharacterViewBase m_CharaView;

			// Token: 0x04006398 RID: 25496
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070DC")]
			public bool m_NextRequestFlag;
		}
	}
}
