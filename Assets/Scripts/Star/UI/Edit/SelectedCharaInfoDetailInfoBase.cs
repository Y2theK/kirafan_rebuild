﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001069 RID: 4201
	[Token(Token = "0x2000AE8")]
	[StructLayout(3)]
	public class SelectedCharaInfoDetailInfoBase : MonoBehaviour
	{
		// Token: 0x06005120 RID: 20768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A9F")]
		[Address(RVA = "0x101467104", Offset = "0x1467104", VA = "0x101467104", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06005121 RID: 20769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA0")]
		[Address(RVA = "0x101467108", Offset = "0x1467108", VA = "0x101467108", Slot = "5")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06005122 RID: 20770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA1")]
		[Address(RVA = "0x10146710C", Offset = "0x146710C", VA = "0x10146710C", Slot = "6")]
		public virtual void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005123 RID: 20771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA2")]
		[Address(RVA = "0x101467110", Offset = "0x1467110", VA = "0x101467110", Slot = "7")]
		public virtual void ApplyEmpty()
		{
		}

		// Token: 0x06005124 RID: 20772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA3")]
		[Address(RVA = "0x101467114", Offset = "0x1467114", VA = "0x101467114")]
		public SelectedCharaInfoDetailInfoBase()
		{
		}

		// Token: 0x0200106A RID: 4202
		[Token(Token = "0x2001243")]
		public abstract class ApplyArgumentBase
		{
			// Token: 0x06005125 RID: 20773 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006331")]
			[Address(RVA = "0x10146711C", Offset = "0x146711C", VA = "0x10146711C")]
			protected ApplyArgumentBase()
			{
			}
		}
	}
}
