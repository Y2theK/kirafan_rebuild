﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200106B RID: 4203
	[Token(Token = "0x2000AE9")]
	[StructLayout(3)]
	public class SelectedCharaInfoDetailInfoGroup : SelectedCharaInfoDetailInfoGroupBase
	{
		// Token: 0x06005126 RID: 20774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA4")]
		[Address(RVA = "0x101467124", Offset = "0x1467124", VA = "0x101467124", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06005127 RID: 20775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA5")]
		[Address(RVA = "0x10146712C", Offset = "0x146712C", VA = "0x10146712C")]
		protected void Setup(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType)
		{
		}

		// Token: 0x06005128 RID: 20776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA6")]
		[Address(RVA = "0x1014672D4", Offset = "0x14672D4", VA = "0x1014672D4", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06005129 RID: 20777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA7")]
		[Address(RVA = "0x1014673AC", Offset = "0x14673AC", VA = "0x1014673AC", Slot = "6")]
		public override void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x0600512A RID: 20778 RVA: 0x0001BF78 File Offset: 0x0001A178
		[Token(Token = "0x6004AA8")]
		[Address(RVA = "0x10146742C", Offset = "0x146742C", VA = "0x10146742C", Slot = "7")]
		public override SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.None;
		}

		// Token: 0x0600512B RID: 20779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AA9")]
		[Address(RVA = "0x101467434", Offset = "0x1467434", VA = "0x101467434")]
		public SelectedCharaInfoDetailInfoGroup()
		{
		}

		// Token: 0x0400639B RID: 25499
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045F5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EFD4", Offset = "0x12EFD4")]
		protected SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType m_GroupType;

		// Token: 0x0400639C RID: 25500
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045F6")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F020", Offset = "0x12F020")]
		private SelectedCharaInfoDetailInfoBase[] m_SkillInfos;
	}
}
