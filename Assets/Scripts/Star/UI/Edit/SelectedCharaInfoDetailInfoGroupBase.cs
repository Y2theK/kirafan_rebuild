﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200106C RID: 4204
	[Token(Token = "0x2000AEA")]
	[StructLayout(3)]
	public class SelectedCharaInfoDetailInfoGroupBase : MonoBehaviour
	{
		// Token: 0x0600512C RID: 20780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AAA")]
		[Address(RVA = "0x101467444", Offset = "0x1467444", VA = "0x101467444", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x0600512D RID: 20781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AAB")]
		[Address(RVA = "0x101467448", Offset = "0x1467448", VA = "0x101467448", Slot = "5")]
		public virtual void Destroy()
		{
		}

		// Token: 0x0600512E RID: 20782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AAC")]
		[Address(RVA = "0x10146744C", Offset = "0x146744C", VA = "0x10146744C", Slot = "6")]
		public virtual void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x0600512F RID: 20783 RVA: 0x0001BF90 File Offset: 0x0001A190
		[Token(Token = "0x6004AAD")]
		[Address(RVA = "0x101467450", Offset = "0x1467450", VA = "0x101467450", Slot = "7")]
		public virtual SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.None;
		}

		// Token: 0x06005130 RID: 20784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AAE")]
		[Address(RVA = "0x10146743C", Offset = "0x146743C", VA = "0x10146743C")]
		public SelectedCharaInfoDetailInfoGroupBase()
		{
		}

		// Token: 0x0400639D RID: 25501
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045F7")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F06C", Offset = "0x12F06C")]
		protected HaveImageTitle m_GroupTypeLabel;

		// Token: 0x0200106D RID: 4205
		[Token(Token = "0x2001244")]
		public enum eSkillInfoGroupType
		{
			// Token: 0x0400639F RID: 25503
			[Token(Token = "0x40070DE")]
			None,
			// Token: 0x040063A0 RID: 25504
			[Token(Token = "0x40070DF")]
			UniquieSkill,
			// Token: 0x040063A1 RID: 25505
			[Token(Token = "0x40070E0")]
			GeneralSkill,
			// Token: 0x040063A2 RID: 25506
			[Token(Token = "0x40070E1")]
			Weapon,
			// Token: 0x040063A3 RID: 25507
			[Token(Token = "0x40070E2")]
			WeaponSkill,
			// Token: 0x040063A4 RID: 25508
			[Token(Token = "0x40070E3")]
			MasterSkill,
			// Token: 0x040063A5 RID: 25509
			[Token(Token = "0x40070E4")]
			ScheduleDropState
		}
	}
}
