﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200106E RID: 4206
	[Token(Token = "0x2000AEB")]
	[StructLayout(3)]
	public class SelectedCharaInfoDetailInfoGroupBaseExGen<DetailInfoType, ArgumentType> : SelectedCharaInfoDetailInfoGroupBase where DetailInfoType : SelectedCharaInfoDetailInfoBase where ArgumentType : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
	{
		// Token: 0x06005131 RID: 20785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AAF")]
		[Address(RVA = "0x1016CE418", Offset = "0x16CE418", VA = "0x1016CE418", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06005132 RID: 20786 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB0")]
		[Address(RVA = "0x1016CE4C8", Offset = "0x16CE4C8", VA = "0x1016CE4C8")]
		protected void SetupProcess(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType type, string title)
		{
		}

		// Token: 0x06005133 RID: 20787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB1")]
		[Address(RVA = "0x1016CE504", Offset = "0x16CE504", VA = "0x1016CE504", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06005134 RID: 20788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB2")]
		[Address(RVA = "0x1016CE5DC", Offset = "0x16CE5DC", VA = "0x1016CE5DC", Slot = "6")]
		public override void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005135 RID: 20789 RVA: 0x0001BFA8 File Offset: 0x0001A1A8
		[Token(Token = "0x6004AB3")]
		[Address(RVA = "0x1016CE65C", Offset = "0x16CE65C", VA = "0x1016CE65C", Slot = "7")]
		public override SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.None;
		}

		// Token: 0x06005136 RID: 20790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB4")]
		[Address(RVA = "0x1016CE664", Offset = "0x16CE664", VA = "0x1016CE664")]
		public SelectedCharaInfoDetailInfoGroupBaseExGen()
		{
		}

		// Token: 0x040063A6 RID: 25510
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40045F8")]
		protected SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType m_GroupType;

		// Token: 0x040063A7 RID: 25511
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x40045F9")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F0B8", Offset = "0x12F0B8")]
		private DetailInfoType[] m_SkillInfos;
	}
}
