﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200106F RID: 4207
	[Token(Token = "0x2000AEC")]
	[StructLayout(3)]
	public class SelectedCharaInfoDetailInfoList : MonoBehaviour
	{
		// Token: 0x06005137 RID: 20791 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004AB5")]
		[Address(RVA = "0x101467458", Offset = "0x1467458", VA = "0x101467458")]
		private static string ConvertToRichText(string text, SelectedCharaInfoLevelInfo.eEmphasisExpressionType type)
		{
			return null;
		}

		// Token: 0x06005138 RID: 20792 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004AB6")]
		[Address(RVA = "0x1014674DC", Offset = "0x14674DC", VA = "0x1014674DC")]
		private static SelectedCharaInfoLevelInfo.EmphasisExpressionInfo MakeEmphasisExpressionInfo(SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument argEx)
		{
			return null;
		}

		// Token: 0x06005139 RID: 20793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB7")]
		[Address(RVA = "0x1014675EC", Offset = "0x14675EC", VA = "0x1014675EC")]
		public void Setup()
		{
		}

		// Token: 0x0600513A RID: 20794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB8")]
		[Address(RVA = "0x10146778C", Offset = "0x146778C", VA = "0x10146778C")]
		public void Destroy()
		{
		}

		// Token: 0x0600513B RID: 20795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AB9")]
		[Address(RVA = "0x101467864", Offset = "0x1467864", VA = "0x101467864")]
		public void ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x0600513C RID: 20796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABA")]
		[Address(RVA = "0x101467EF8", Offset = "0x1467EF8", VA = "0x101467EF8")]
		public void SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, bool active)
		{
		}

		// Token: 0x0600513D RID: 20797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABB")]
		[Address(RVA = "0x101468018", Offset = "0x1468018", VA = "0x101468018")]
		public SelectedCharaInfoDetailInfoList()
		{
		}

		// Token: 0x040063A8 RID: 25512
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045FA")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F104", Offset = "0x12F104")]
		private SelectedCharaInfoDetailInfoGroup[] m_SkillInfoGroup;

		// Token: 0x040063A9 RID: 25513
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045FB")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F150", Offset = "0x12F150")]
		private SelectedCharaInfoDetailInfoGroupBase[] m_SkillInfoGroups;

		// Token: 0x040063AA RID: 25514
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045FC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F19C", Offset = "0x12F19C")]
		private Text m_UniqueSkillLevel;

		// Token: 0x040063AB RID: 25515
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40045FD")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F1E8", Offset = "0x12F1E8")]
		private Text m_GeneralSkillLevel;
	}
}
