﻿using System;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001080 RID: 4224
	[Token(Token = "0x2000AF3")]
	[StructLayout(3)]
	public class SelectedCharaInfoEventQuestInfo : MonoBehaviour
	{
		// Token: 0x0600515A RID: 20826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACD")]
		[Address(RVA = "0x101468020", Offset = "0x1468020", VA = "0x101468020")]
		public void Setup()
		{
		}

		// Token: 0x0600515B RID: 20827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACE")]
		[Address(RVA = "0x101468024", Offset = "0x1468024", VA = "0x101468024")]
		public void Destroy()
		{
		}

		// Token: 0x0600515C RID: 20828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACF")]
		[Address(RVA = "0x101468028", Offset = "0x1468028", VA = "0x101468028")]
		public void Apply(CharacterDataController cdc)
		{
		}

		// Token: 0x0600515D RID: 20829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD0")]
		[Address(RVA = "0x101468080", Offset = "0x1468080", VA = "0x101468080")]
		protected void Apply(int charaId, eEventQuestDropExtPlusType plusType)
		{
		}

		// Token: 0x0600515E RID: 20830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD1")]
		[Address(RVA = "0x10146834C", Offset = "0x146834C", VA = "0x10146834C")]
		public void SetDispDropItem(int charaId, bool flg, eEventQuestDropExtPlusType plusType)
		{
		}

		// Token: 0x0600515F RID: 20831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD2")]
		[Address(RVA = "0x10146838C", Offset = "0x146838C", VA = "0x10146838C")]
		private void UpdateDropItemDisp()
		{
		}

		// Token: 0x06005160 RID: 20832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD3")]
		[Address(RVA = "0x1014684A8", Offset = "0x14684A8", VA = "0x1014684A8")]
		public SelectedCharaInfoEventQuestInfo()
		{
		}

		// Token: 0x040063CA RID: 25546
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400460B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F430", Offset = "0x12F430")]
		private SwitchFade m_SwitchLabel;

		// Token: 0x040063CB RID: 25547
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400460C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F47C", Offset = "0x12F47C")]
		private CanvasGroup m_IsBonusCharaCG;

		// Token: 0x040063CC RID: 25548
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400460D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F4C8", Offset = "0x12F4C8")]
		private ItemDropExtDisplay m_DropDisp;

		// Token: 0x040063CD RID: 25549
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400460E")]
		private int m_CharaID;

		// Token: 0x040063CE RID: 25550
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400460F")]
		private bool m_DispDropItem;

		// Token: 0x040063CF RID: 25551
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004610")]
		private eEventQuestDropExtPlusType m_PlusType;
	}
}
