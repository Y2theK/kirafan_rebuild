﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001081 RID: 4225
	[Token(Token = "0x2000AF4")]
	[StructLayout(3)]
	public class SelectedCharaInfoFriendship : SelectedCharaInfoHaveTitleField
	{
		// Token: 0x06005161 RID: 20833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD4")]
		[Address(RVA = "0x1014684B8", Offset = "0x14684B8", VA = "0x1014684B8")]
		private void Update()
		{
		}

		// Token: 0x06005162 RID: 20834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD5")]
		[Address(RVA = "0x101468540", Offset = "0x1468540", VA = "0x101468540")]
		public SelectedCharaInfoFriendship()
		{
		}

		// Token: 0x040063D0 RID: 25552
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004611")]
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x040063D1 RID: 25553
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004612")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F524", Offset = "0x12F524")]
		private RectTransform m_LayoutGroupTransform;

		// Token: 0x040063D2 RID: 25554
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004613")]
		private int m_frame;
	}
}
