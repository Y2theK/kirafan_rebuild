﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001082 RID: 4226
	[Token(Token = "0x2000AF5")]
	[StructLayout(3)]
	public class SelectedCharaInfoFriendshipIcon : MonoBehaviour
	{
		// Token: 0x06005163 RID: 20835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD6")]
		[Address(RVA = "0x101468548", Offset = "0x1468548", VA = "0x101468548")]
		public void Setup()
		{
		}

		// Token: 0x06005164 RID: 20836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD7")]
		[Address(RVA = "0x10146854C", Offset = "0x146854C", VA = "0x10146854C")]
		public void Destroy()
		{
		}

		// Token: 0x06005165 RID: 20837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD8")]
		[Address(RVA = "0x101468550", Offset = "0x1468550", VA = "0x101468550")]
		public void SetState(SelectedCharaInfoFriendshipIcon.eState state)
		{
		}

		// Token: 0x06005166 RID: 20838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AD9")]
		[Address(RVA = "0x10146860C", Offset = "0x146860C", VA = "0x10146860C")]
		public SelectedCharaInfoFriendshipIcon()
		{
		}

		// Token: 0x040063D3 RID: 25555
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004614")]
		[SerializeField]
		private GameObject m_Complete;

		// Token: 0x040063D4 RID: 25556
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004615")]
		[SerializeField]
		private GameObject m_NotComplete;

		// Token: 0x02001083 RID: 4227
		[Token(Token = "0x200124E")]
		public enum eState
		{
			// Token: 0x040063D6 RID: 25558
			[Token(Token = "0x40070F4")]
			Complete,
			// Token: 0x040063D7 RID: 25559
			[Token(Token = "0x40070F5")]
			NotComplete,
			// Token: 0x040063D8 RID: 25560
			[Token(Token = "0x40070F6")]
			NotExist
		}
	}
}
