﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001070 RID: 4208
	[Token(Token = "0x2000AED")]
	[StructLayout(3)]
	public class SelectedCharaInfoScheduleDropStateInfo : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x0600513E RID: 20798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABC")]
		[Address(RVA = "0x101468614", Offset = "0x1468614", VA = "0x101468614")]
		public void Apply(int charaID)
		{
		}

		// Token: 0x0600513F RID: 20799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABD")]
		[Address(RVA = "0x101468704", Offset = "0x1468704", VA = "0x101468704", Slot = "6")]
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005140 RID: 20800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABE")]
		[Address(RVA = "0x101468A90", Offset = "0x1468A90", VA = "0x101468A90", Slot = "7")]
		public override void ApplyEmpty()
		{
		}

		// Token: 0x06005141 RID: 20801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ABF")]
		[Address(RVA = "0x101468A94", Offset = "0x1468A94", VA = "0x101468A94", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06005142 RID: 20802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC0")]
		[Address(RVA = "0x101468B3C", Offset = "0x1468B3C", VA = "0x101468B3C")]
		public SelectedCharaInfoScheduleDropStateInfo()
		{
		}

		// Token: 0x040063AC RID: 25516
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045FE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F234", Offset = "0x12F234")]
		private SelectedCharaInfoScheduleDropStateInfo.ScheduleDropStateInfoParam[] m_ScheduleDropStateInfoParams;

		// Token: 0x040063AD RID: 25517
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045FF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F280", Offset = "0x12F280")]
		private Image m_ScheduleDropStateImage;

		// Token: 0x040063AE RID: 25518
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004600")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F2CC", Offset = "0x12F2CC")]
		private Text m_ScheduleDropStateMessage;

		// Token: 0x02001071 RID: 4209
		[Token(Token = "0x2001245")]
		[Serializable]
		public class ScheduleDropStateInfoParam
		{
			// Token: 0x06005143 RID: 20803 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006332")]
			[Address(RVA = "0x101468B34", Offset = "0x1468B34", VA = "0x101468B34")]
			public void Destroy()
			{
			}

			// Token: 0x06005144 RID: 20804 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006333")]
			[Address(RVA = "0x101468B44", Offset = "0x1468B44", VA = "0x101468B44")]
			public ScheduleDropStateInfoParam()
			{
			}

			// Token: 0x040063AF RID: 25519
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070E5")]
			public int m_DropItemKey;

			// Token: 0x040063B0 RID: 25520
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070E6")]
			public Sprite m_SpriteAssets;

			// Token: 0x040063B1 RID: 25521
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40070E7")]
			public int m_TextCommonId;
		}

		// Token: 0x02001072 RID: 4210
		[Token(Token = "0x2001246")]
		public class ApplyArgument : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x06005145 RID: 20805 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006334")]
			[Address(RVA = "0x1014686C8", Offset = "0x14686C8", VA = "0x1014686C8")]
			public ApplyArgument(int in_charaId, UserScheduleData.DropState in_dropState)
			{
			}

			// Token: 0x040063B2 RID: 25522
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070E8")]
			public int charaId;

			// Token: 0x040063B3 RID: 25523
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070E9")]
			public UserScheduleData.DropState dropState;
		}
	}
}
