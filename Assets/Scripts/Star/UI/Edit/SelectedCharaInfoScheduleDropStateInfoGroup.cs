﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x02001073 RID: 4211
	[Token(Token = "0x2000AEE")]
	[StructLayout(3)]
	public class SelectedCharaInfoScheduleDropStateInfoGroup : SelectedCharaInfoDetailInfoGroupBaseExGen<SelectedCharaInfoScheduleDropStateInfo, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase>
	{
		// Token: 0x06005146 RID: 20806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC1")]
		[Address(RVA = "0x101468B4C", Offset = "0x1468B4C", VA = "0x101468B4C", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06005147 RID: 20807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC2")]
		[Address(RVA = "0x101468BAC", Offset = "0x1468BAC", VA = "0x101468BAC")]
		public SelectedCharaInfoScheduleDropStateInfoGroup()
		{
		}
	}
}
