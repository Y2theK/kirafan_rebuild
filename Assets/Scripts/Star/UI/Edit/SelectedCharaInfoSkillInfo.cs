﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x02001074 RID: 4212
	[Token(Token = "0x2000AEF")]
	[StructLayout(3)]
	public class SelectedCharaInfoSkillInfo : SelectedCharaInfoSkillInfoBase
	{
		// Token: 0x06005148 RID: 20808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC3")]
		[Address(RVA = "0x101468BFC", Offset = "0x1468BFC", VA = "0x101468BFC", Slot = "6")]
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005149 RID: 20809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC4")]
		[Address(RVA = "0x101468DEC", Offset = "0x1468DEC", VA = "0x101468DEC")]
		public SelectedCharaInfoSkillInfo()
		{
		}

		// Token: 0x02001075 RID: 4213
		[Token(Token = "0x2001247")]
		public class ApplyArgument : SelectedCharaInfoSkillInfoBase.SkillInfoApplyArgumentBase
		{
			// Token: 0x0600514A RID: 20810 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006335")]
			[Address(RVA = "0x101468DFC", Offset = "0x1468DFC", VA = "0x101468DFC")]
			public ApplyArgument(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, LevelInformation in_beforeLevel, long in_remainExp, bool isArouse)
			{
			}

			// Token: 0x040063B4 RID: 25524
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40070EA")]
			public long remainExp;

			// Token: 0x040063B5 RID: 25525
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x40070EB")]
			public bool bAroused;
		}
	}
}
