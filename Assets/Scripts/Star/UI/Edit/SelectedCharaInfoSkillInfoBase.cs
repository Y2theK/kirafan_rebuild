﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001076 RID: 4214
	[Token(Token = "0x2000AF0")]
	[StructLayout(3)]
	public class SelectedCharaInfoSkillInfoBase : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x0600514B RID: 20811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC5")]
		[Address(RVA = "0x101468D00", Offset = "0x1468D00", VA = "0x101468D00")]
		protected void ApplySkillInfo(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, [Optional] LevelInformation beforeLevel, long remainExp = -1L, bool isAroused = false)
		{
		}

		// Token: 0x0600514C RID: 20812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC6")]
		[Address(RVA = "0x101468EB8", Offset = "0x1468EB8", VA = "0x101468EB8", Slot = "7")]
		public override void ApplyEmpty()
		{
		}

		// Token: 0x0600514D RID: 20813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC7")]
		[Address(RVA = "0x101468DF4", Offset = "0x1468DF4", VA = "0x101468DF4")]
		public SelectedCharaInfoSkillInfoBase()
		{
		}

		// Token: 0x040063B6 RID: 25526
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004601")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F318", Offset = "0x12F318")]
		private SkillInfo m_SkillInfo;

		// Token: 0x02001077 RID: 4215
		[Token(Token = "0x2001248")]
		public abstract class SkillInfoApplyArgumentBase : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x0600514E RID: 20814 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006336")]
			[Address(RVA = "0x101468E68", Offset = "0x1468E68", VA = "0x101468E68")]
			public SkillInfoApplyArgumentBase(SkillIcon.eSkillDBType in_skillDBType, int in_skillID, eElementType in_ownerElement, LevelInformation in_beforeLevel)
			{
			}

			// Token: 0x040063B7 RID: 25527
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070EC")]
			public SkillIcon.eSkillDBType skillDBType;

			// Token: 0x040063B8 RID: 25528
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40070ED")]
			public int skillID;

			// Token: 0x040063B9 RID: 25529
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070EE")]
			public eElementType ownerElement;

			// Token: 0x040063BA RID: 25530
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x40070EF")]
			public LevelInformation beforeLevel;
		}
	}
}
