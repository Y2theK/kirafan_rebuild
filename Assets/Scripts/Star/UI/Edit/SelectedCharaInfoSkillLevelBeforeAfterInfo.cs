﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001078 RID: 4216
	[Token(Token = "0x2000AF1")]
	[StructLayout(3)]
	public class SelectedCharaInfoSkillLevelBeforeAfterInfo : SelectedCharaInfoSkillInfoBase
	{
		// Token: 0x0600514F RID: 20815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC8")]
		[Address(RVA = "0x101468EF0", Offset = "0x1468EF0", VA = "0x101468EF0", Slot = "6")]
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005150 RID: 20816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AC9")]
		[Address(RVA = "0x101469190", Offset = "0x1469190", VA = "0x101469190")]
		public SelectedCharaInfoSkillLevelBeforeAfterInfo()
		{
		}

		// Token: 0x040063BB RID: 25531
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004602")]
		[SerializeField]
		private SelectedCharaInfoLevelInfo m_afterLevelInfo;

		// Token: 0x02001079 RID: 4217
		[Token(Token = "0x2001249")]
		public class ApplyArgument : SelectedCharaInfoSkillInfoBase.SkillInfoApplyArgumentBase
		{
			// Token: 0x06005151 RID: 20817 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006337")]
			[Address(RVA = "0x101469198", Offset = "0x1469198", VA = "0x101469198")]
			public ApplyArgument(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, LevelInformation in_beforeLevel, LevelInformation in_afterLevel)
			{
			}

			// Token: 0x040063BC RID: 25532
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x40070F0")]
			public LevelInformation afterLevel;
		}
	}
}
