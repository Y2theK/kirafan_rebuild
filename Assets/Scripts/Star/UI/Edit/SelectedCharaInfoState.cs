﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001084 RID: 4228
	[Token(Token = "0x2000AF6")]
	[StructLayout(3)]
	public class SelectedCharaInfoState : MonoBehaviour
	{
		// Token: 0x06005167 RID: 20839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADA")]
		[Address(RVA = "0x1014691EC", Offset = "0x14691EC", VA = "0x1014691EC")]
		public void Setup()
		{
		}

		// Token: 0x06005168 RID: 20840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADB")]
		[Address(RVA = "0x10146960C", Offset = "0x146960C", VA = "0x10146960C")]
		public void Destroy()
		{
		}

		// Token: 0x06005169 RID: 20841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADC")]
		[Address(RVA = "0x1014696A8", Offset = "0x14696A8", VA = "0x1014696A8")]
		public void SetBeforeLevel(int nowLevel, int maxLevel, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x0600516A RID: 20842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADD")]
		[Address(RVA = "0x1014697C8", Offset = "0x14697C8", VA = "0x1014697C8")]
		public void SetAfterLevel(int nowLevel, int maxLevel, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x0600516B RID: 20843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADE")]
		[Address(RVA = "0x1014698E8", Offset = "0x14698E8", VA = "0x1014698E8")]
		public void SetBeforeLimitBreakIconValue(int value)
		{
		}

		// Token: 0x0600516C RID: 20844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ADF")]
		[Address(RVA = "0x1014699E8", Offset = "0x14699E8", VA = "0x1014699E8")]
		public void SetAfterLimitBreakIconValue(int value)
		{
		}

		// Token: 0x0600516D RID: 20845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE0")]
		[Address(RVA = "0x101469AE8", Offset = "0x1469AE8", VA = "0x101469AE8")]
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
		}

		// Token: 0x0600516E RID: 20846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE1")]
		[Address(RVA = "0x101469C24", Offset = "0x1469C24", VA = "0x101469C24")]
		public void SetWeaponIcon(int weaponId)
		{
		}

		// Token: 0x0600516F RID: 20847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE2")]
		[Address(RVA = "0x101469CD8", Offset = "0x1469CD8", VA = "0x101469CD8")]
		public void SetWeaponIcon(EquipWeaponCharacterDataController partyMember)
		{
		}

		// Token: 0x06005170 RID: 20848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE3")]
		[Address(RVA = "0x101469D88", Offset = "0x1469D88", VA = "0x101469D88")]
		public void SetWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
		}

		// Token: 0x06005171 RID: 20849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE4")]
		[Address(RVA = "0x101469E38", Offset = "0x1469E38", VA = "0x101469E38")]
		public void SetCost(int cost)
		{
		}

		// Token: 0x06005172 RID: 20850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE5")]
		[Address(RVA = "0x101469EF4", Offset = "0x1469EF4", VA = "0x101469EF4")]
		public void ApplyState(EquipWeaponCharacterDataController partyMember)
		{
		}

		// Token: 0x06005173 RID: 20851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE6")]
		[Address(RVA = "0x10146A8A0", Offset = "0x146A8A0", VA = "0x10146A8A0")]
		public SelectedCharaInfoState()
		{
		}

		// Token: 0x040063D9 RID: 25561
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004616")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F590", Offset = "0x12F590")]
		private SelectedCharaInfoLevelData m_LevelState;

		// Token: 0x040063DA RID: 25562
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004617")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F5DC", Offset = "0x12F5DC")]
		private SelectedCharaInfoLevelInfo m_LevelInfo;

		// Token: 0x040063DB RID: 25563
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004618")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F628", Offset = "0x12F628")]
		private SelectedCharaInfoLevelInfo m_AfterLevelInfo;

		// Token: 0x040063DC RID: 25564
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004619")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F674", Offset = "0x12F674")]
		private ExpGaugeWrapper m_ExpGaugeWrapper;

		// Token: 0x040063DD RID: 25565
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400461A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F6C0", Offset = "0x12F6C0")]
		private ExpGauge m_ExpGauge;

		// Token: 0x040063DE RID: 25566
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400461B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F70C", Offset = "0x12F70C")]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x040063DF RID: 25567
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400461C")]
		private WeaponIconWithFrame m_WeaponIconComponent;

		// Token: 0x040063E0 RID: 25568
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400461D")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x040063E1 RID: 25569
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400461E")]
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x040063E2 RID: 25570
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400461F")]
		[SerializeField]
		private CharaStatus m_StatusDisplayAfter;

		// Token: 0x040063E3 RID: 25571
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004620")]
		[SerializeField]
		private CharaStatusPercent m_StatusFriendship;

		// Token: 0x040063E4 RID: 25572
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004621")]
		[SerializeField]
		private CharaStatusPercent m_StatusDisplayTown;

		// Token: 0x040063E5 RID: 25573
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004622")]
		[SerializeField]
		private CharaStatus m_StatusDisplayWeapon;

		// Token: 0x040063E6 RID: 25574
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004623")]
		[SerializeField]
		private CharaStatus m_StatusDisplayArousal;

		// Token: 0x040063E7 RID: 25575
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004624")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F7C8", Offset = "0x12F7C8")]
		private SelectedCharaInfoHaveTitleTextField[] m_HaveTitleTextFields;

		// Token: 0x040063E8 RID: 25576
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004625")]
		[SerializeField]
		private ArousalLvIcon m_IconArousalLv;
	}
}
