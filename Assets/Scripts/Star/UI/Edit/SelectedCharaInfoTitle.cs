﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001064 RID: 4196
	[Token(Token = "0x2000AE5")]
	[StructLayout(3)]
	public class SelectedCharaInfoTitle : MonoBehaviour
	{
		// Token: 0x060050E8 RID: 20712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A69")]
		[Address(RVA = "0x10146A8A8", Offset = "0x146A8A8", VA = "0x10146A8A8")]
		public void Constructor(bool activeClassIcon = true, bool activeElementIcon = true, bool activeRareStar = true, bool activeName = true, bool activeUnderLine = true, float rareStarClonePositionY = 12f)
		{
		}

		// Token: 0x060050E9 RID: 20713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6A")]
		[Address(RVA = "0x10146A980", Offset = "0x146A980", VA = "0x10146A980")]
		public void Setup(int charaMngId)
		{
		}

		// Token: 0x060050EA RID: 20714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6B")]
		[Address(RVA = "0x10146A984", Offset = "0x146A984", VA = "0x10146A984")]
		public void Setup(UserCharacterData data)
		{
		}

		// Token: 0x060050EB RID: 20715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6C")]
		[Address(RVA = "0x10146A988", Offset = "0x146A988", VA = "0x10146A988")]
		public void Setup()
		{
		}

		// Token: 0x060050EC RID: 20716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6D")]
		[Address(RVA = "0x10146ABD8", Offset = "0x146ABD8", VA = "0x10146ABD8")]
		public void Destroy()
		{
		}

		// Token: 0x060050ED RID: 20717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6E")]
		[Address(RVA = "0x10146ACCC", Offset = "0x146ACCC", VA = "0x10146ACCC")]
		public void SetTitleImage(eTitleType title)
		{
		}

		// Token: 0x060050EE RID: 20718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A6F")]
		[Address(RVA = "0x10146AD7C", Offset = "0x146AD7C", VA = "0x10146AD7C")]
		public void SetClassIcon(eClassType classType)
		{
		}

		// Token: 0x060050EF RID: 20719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A70")]
		[Address(RVA = "0x10146AE2C", Offset = "0x146AE2C", VA = "0x10146AE2C")]
		public void SetElementIcon(eElementType elementType)
		{
		}

		// Token: 0x060050F0 RID: 20720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A71")]
		[Address(RVA = "0x10146AED8", Offset = "0x146AED8", VA = "0x10146AED8")]
		public void SetCharacterRareStars(eRare rare)
		{
		}

		// Token: 0x060050F1 RID: 20721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A72")]
		[Address(RVA = "0x10146AFD8", Offset = "0x146AFD8", VA = "0x10146AFD8")]
		public void SetName(string name)
		{
		}

		// Token: 0x060050F2 RID: 20722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A73")]
		[Address(RVA = "0x10146B090", Offset = "0x146B090", VA = "0x10146B090")]
		public void SetCostText(int cost)
		{
		}

		// Token: 0x060050F3 RID: 20723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A74")]
		[Address(RVA = "0x10146B14C", Offset = "0x146B14C", VA = "0x10146B14C")]
		public void SetCostText(int charaCost, int weaponCost)
		{
		}

		// Token: 0x060050F4 RID: 20724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A75")]
		[Address(RVA = "0x10146B348", Offset = "0x146B348", VA = "0x10146B348")]
		public void SetWeaponIcon(int weaponId)
		{
		}

		// Token: 0x060050F5 RID: 20725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A76")]
		[Address(RVA = "0x10146B3FC", Offset = "0x146B3FC", VA = "0x10146B3FC")]
		public void SetWeaponIconRareStarActive(bool active)
		{
		}

		// Token: 0x060050F6 RID: 20726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A77")]
		[Address(RVA = "0x10146B4E4", Offset = "0x146B4E4", VA = "0x10146B4E4")]
		public void SetBackGround(Sprite background)
		{
		}

		// Token: 0x060050F7 RID: 20727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A78")]
		[Address(RVA = "0x10146B594", Offset = "0x146B594", VA = "0x10146B594")]
		public void SetBoxBackGround(Sprite boxBackgroundSprite)
		{
		}

		// Token: 0x060050F8 RID: 20728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A79")]
		[Address(RVA = "0x10146B640", Offset = "0x146B640", VA = "0x10146B640")]
		public void SetUnderLine(Sprite underLine)
		{
		}

		// Token: 0x060050F9 RID: 20729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7A")]
		[Address(RVA = "0x10146B6F0", Offset = "0x146B6F0", VA = "0x10146B6F0")]
		public void SetArousalStar(eRare rare, int arousalLv)
		{
		}

		// Token: 0x060050FA RID: 20730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004A7B")]
		[Address(RVA = "0x10146B7A8", Offset = "0x146B7A8", VA = "0x10146B7A8")]
		public SelectedCharaInfoTitle()
		{
		}

		// Token: 0x04006372 RID: 25458
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40045D5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EC10", Offset = "0x12EC10")]
		private TitleImage m_TitleImage;

		// Token: 0x04006373 RID: 25459
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40045D6")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EC5C", Offset = "0x12EC5C")]
		private ClassIcon m_ClassIcon;

		// Token: 0x04006374 RID: 25460
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40045D7")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012ECA8", Offset = "0x12ECA8")]
		private ElementIcon m_ElementIcon;

		// Token: 0x04006375 RID: 25461
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40045D8")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012ECF4", Offset = "0x12ECF4")]
		private PrefabCloner m_RareStarCloner;

		// Token: 0x04006376 RID: 25462
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40045D9")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012ED40", Offset = "0x12ED40")]
		private RareStar m_RareStar;

		// Token: 0x04006377 RID: 25463
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40045DA")]
		[SerializeField]
		private ArousalStar m_ArousalStar;

		// Token: 0x04006378 RID: 25464
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40045DB")]
		[SerializeField]
		private RareIcon m_RareIcon;

		// Token: 0x04006379 RID: 25465
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40045DC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EDAC", Offset = "0x12EDAC")]
		private Text m_NameText;

		// Token: 0x0400637A RID: 25466
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40045DD")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EDF8", Offset = "0x12EDF8")]
		private Text m_CostText;

		// Token: 0x0400637B RID: 25467
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40045DE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EE44", Offset = "0x12EE44")]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x0400637C RID: 25468
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40045DF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EE90", Offset = "0x12EE90")]
		private Image m_BackGround;

		// Token: 0x0400637D RID: 25469
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40045E0")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EEDC", Offset = "0x12EEDC")]
		private Image[] m_BoxBackGround;

		// Token: 0x0400637E RID: 25470
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40045E1")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012EF28", Offset = "0x12EF28")]
		private Image m_UnderLine;

		// Token: 0x0400637F RID: 25471
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40045E2")]
		private List<GameObject> m_ObjectsArray;

		// Token: 0x04006380 RID: 25472
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40045E3")]
		private RareStar m_RareStarComponent;

		// Token: 0x04006381 RID: 25473
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40045E4")]
		private WeaponIconWithFrame m_WeaponIconComponent;
	}
}
