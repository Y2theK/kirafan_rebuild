﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001085 RID: 4229
	[Token(Token = "0x2000AF7")]
	[StructLayout(3)]
	public class SelectedCharaInfoVoiceList : MonoBehaviour
	{
		// Token: 0x06005174 RID: 20852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE7")]
		[Address(RVA = "0x10146B7B0", Offset = "0x146B7B0", VA = "0x10146B7B0")]
		public void Setup()
		{
		}

		// Token: 0x06005175 RID: 20853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE8")]
		[Address(RVA = "0x10146B7E4", Offset = "0x146B7E4", VA = "0x10146B7E4")]
		public void Destroy()
		{
		}

		// Token: 0x06005176 RID: 20854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AE9")]
		[Address(RVA = "0x10146B888", Offset = "0x146B888", VA = "0x10146B888")]
		public void Dispose()
		{
		}

		// Token: 0x06005177 RID: 20855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AEA")]
		[Address(RVA = "0x10146B8AC", Offset = "0x146B8AC", VA = "0x10146B8AC")]
		public void SetVoiceListItems(eCharaNamedType charaNamed, int nowFriendship)
		{
		}

		// Token: 0x06005178 RID: 20856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AEB")]
		[Address(RVA = "0x10146BA94", Offset = "0x146BA94", VA = "0x10146BA94")]
		public SelectedCharaInfoVoiceList()
		{
		}

		// Token: 0x040063E9 RID: 25577
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004626")]
		[SerializeField]
		private ScrollViewBase m_ScrollView;
	}
}
