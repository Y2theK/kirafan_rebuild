﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001086 RID: 4230
	[Token(Token = "0x2000AF8")]
	[StructLayout(3)]
	public class SelectedCharaInfoVoiceListItem : ScrollItemIcon
	{
		// Token: 0x06005179 RID: 20857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AEC")]
		[Address(RVA = "0x10146BA9C", Offset = "0x146BA9C", VA = "0x10146BA9C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600517A RID: 20858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AED")]
		[Address(RVA = "0x10146C008", Offset = "0x146C008", VA = "0x10146C008")]
		private void Update()
		{
		}

		// Token: 0x0600517B RID: 20859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AEE")]
		[Address(RVA = "0x10146C2B8", Offset = "0x146C2B8", VA = "0x10146C2B8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x0600517C RID: 20860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AEF")]
		[Address(RVA = "0x10146BDF0", Offset = "0x146BDF0", VA = "0x10146BDF0")]
		public void SetCategory(string category)
		{
		}

		// Token: 0x0600517D RID: 20861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF0")]
		[Address(RVA = "0x10146BE30", Offset = "0x146BE30", VA = "0x10146BE30")]
		public void SetVoiceTitle(string title)
		{
		}

		// Token: 0x0600517E RID: 20862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF1")]
		[Address(RVA = "0x10146BE70", Offset = "0x146BE70", VA = "0x10146BE70")]
		protected void SetLock(SelectedCharaInfoVoiceListItemData data)
		{
		}

		// Token: 0x0600517F RID: 20863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF2")]
		[Address(RVA = "0x10146C34C", Offset = "0x146C34C", VA = "0x10146C34C")]
		protected void SetLock(string charaName, int needFriendship)
		{
		}

		// Token: 0x06005180 RID: 20864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF3")]
		[Address(RVA = "0x10146C434", Offset = "0x146C434", VA = "0x10146C434")]
		protected void SetLock(string lockMessage)
		{
		}

		// Token: 0x06005181 RID: 20865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF4")]
		[Address(RVA = "0x10146BF48", Offset = "0x146BF48", VA = "0x10146BF48")]
		private void SetEqualizerNoPlaying()
		{
		}

		// Token: 0x06005182 RID: 20866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF5")]
		[Address(RVA = "0x10146C1C0", Offset = "0x146C1C0", VA = "0x10146C1C0")]
		private void SetEqualizerFirstIndex()
		{
		}

		// Token: 0x06005183 RID: 20867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF6")]
		[Address(RVA = "0x10146C698", Offset = "0x146C698", VA = "0x10146C698")]
		public void OnClickItem()
		{
		}

		// Token: 0x06005184 RID: 20868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF7")]
		[Address(RVA = "0x10146CA30", Offset = "0x146CA30", VA = "0x10146CA30")]
		public SelectedCharaInfoVoiceListItem()
		{
		}

		// Token: 0x040063EA RID: 25578
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004627")]
		[SerializeField]
		private Text m_Category;

		// Token: 0x040063EB RID: 25579
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004628")]
		[SerializeField]
		private Text m_VoiceTitle;

		// Token: 0x040063EC RID: 25580
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004629")]
		[SerializeField]
		private SelectedCharaInfoVoiceListItemLock m_Lock;

		// Token: 0x040063ED RID: 25581
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400462A")]
		[SerializeField]
		private SelectedCharaInfoVoiceListItem.EqualizerInformation[] m_EqualizerInformations;

		// Token: 0x040063EE RID: 25582
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400462B")]
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x040063EF RID: 25583
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400462C")]
		private SelectedCharaInfoVoiceListItemData m_VoiceListItemData;

		// Token: 0x040063F0 RID: 25584
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400462D")]
		private int m_UId;

		// Token: 0x040063F1 RID: 25585
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x400462E")]
		private int m_Index;

		// Token: 0x040063F2 RID: 25586
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400462F")]
		private float m_WaitTime;

		// Token: 0x02001087 RID: 4231
		[Token(Token = "0x200124F")]
		[Serializable]
		public class EqualizerInformation
		{
			// Token: 0x06005185 RID: 20869 RVA: 0x0001BFD8 File Offset: 0x0001A1D8
			[Token(Token = "0x600633D")]
			[Address(RVA = "0x10146C1B8", Offset = "0x146C1B8", VA = "0x10146C1B8")]
			public float GetWaitTime()
			{
				return 0f;
			}

			// Token: 0x06005186 RID: 20870 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600633E")]
			[Address(RVA = "0x10146C5E8", Offset = "0x146C5E8", VA = "0x10146C5E8")]
			public void SetActive(bool active)
			{
			}

			// Token: 0x06005187 RID: 20871 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600633F")]
			[Address(RVA = "0x10146C344", Offset = "0x146C344", VA = "0x10146C344")]
			public void Destroy()
			{
			}

			// Token: 0x06005188 RID: 20872 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006340")]
			[Address(RVA = "0x10146CA40", Offset = "0x146CA40", VA = "0x10146CA40")]
			public EqualizerInformation()
			{
			}

			// Token: 0x040063F3 RID: 25587
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070F7")]
			[SerializeField]
			[Attribute(Name = "TooltipAttribute", RVA = "0x100134B7C", Offset = "0x134B7C")]
			private float m_WaitTime;

			// Token: 0x040063F4 RID: 25588
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40070F8")]
			[SerializeField]
			[Attribute(Name = "TooltipAttribute", RVA = "0x100134BC8", Offset = "0x134BC8")]
			private GameObject m_Object;
		}
	}
}
