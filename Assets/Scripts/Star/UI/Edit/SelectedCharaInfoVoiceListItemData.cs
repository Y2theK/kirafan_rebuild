﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x02001088 RID: 4232
	[Token(Token = "0x2000AF9")]
	[StructLayout(3)]
	public class SelectedCharaInfoVoiceListItemData : ScrollItemData
	{
		// Token: 0x06005189 RID: 20873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF8")]
		[Address(RVA = "0x10146CA48", Offset = "0x146CA48", VA = "0x10146CA48")]
		public SelectedCharaInfoVoiceListItemData()
		{
		}

		// Token: 0x0600518A RID: 20874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AF9")]
		[Address(RVA = "0x10146BA2C", Offset = "0x146BA2C", VA = "0x10146BA2C")]
		public SelectedCharaInfoVoiceListItemData(eCharaNamedType in_charaNamed, int in_needFriendship, ProfileVoiceListDB_Param param)
		{
		}

		// Token: 0x040063F5 RID: 25589
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004630")]
		public eCharaNamedType charaNamed;

		// Token: 0x040063F6 RID: 25590
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004631")]
		public string category;

		// Token: 0x040063F7 RID: 25591
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004632")]
		public string voiceTitle;

		// Token: 0x040063F8 RID: 25592
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004633")]
		public string cueName;

		// Token: 0x040063F9 RID: 25593
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004634")]
		public int needFriendship;

		// Token: 0x040063FA RID: 25594
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004635")]
		public int spCategoryID;

		// Token: 0x040063FB RID: 25595
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004636")]
		public int[] spParams;
	}
}
