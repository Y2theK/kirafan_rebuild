﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001089 RID: 4233
	[Token(Token = "0x2000AFA")]
	[StructLayout(3)]
	public class SelectedCharaInfoVoiceListItemLock : ScrollItemIcon
	{
		// Token: 0x0600518B RID: 20875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AFA")]
		[Address(RVA = "0x10146C4E0", Offset = "0x146C4E0", VA = "0x10146C4E0")]
		public void SetLock(string conditionMeesage)
		{
		}

		// Token: 0x0600518C RID: 20876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004AFB")]
		[Address(RVA = "0x10146CA50", Offset = "0x146CA50", VA = "0x10146CA50")]
		public SelectedCharaInfoVoiceListItemLock()
		{
		}

		// Token: 0x040063FC RID: 25596
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004637")]
		[SerializeField]
		private GameObject m_BlockSheet;

		// Token: 0x040063FD RID: 25597
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004638")]
		[SerializeField]
		private Text m_Message;
	}
}
