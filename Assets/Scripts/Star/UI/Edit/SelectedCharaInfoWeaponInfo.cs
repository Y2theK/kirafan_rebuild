﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200107A RID: 4218
	[Token(Token = "0x2000AF2")]
	[StructLayout(3)]
	public class SelectedCharaInfoWeaponInfo : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x06005152 RID: 20818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACA")]
		[Address(RVA = "0x10146CA58", Offset = "0x146CA58", VA = "0x10146CA58", Slot = "6")]
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06005153 RID: 20819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACB")]
		[Address(RVA = "0x10146D628", Offset = "0x146D628", VA = "0x10146D628", Slot = "7")]
		public override void ApplyEmpty()
		{
		}

		// Token: 0x06005154 RID: 20820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004ACC")]
		[Address(RVA = "0x10146D658", Offset = "0x146D658", VA = "0x10146D658")]
		public SelectedCharaInfoWeaponInfo()
		{
		}

		// Token: 0x040063BD RID: 25533
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004603")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F374", Offset = "0x12F374")]
		private WeaponInfo m_WeaponInfo;

		// Token: 0x040063BE RID: 25534
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004604")]
		[SerializeField]
		private GameObject m_SkillTitle;

		// Token: 0x040063BF RID: 25535
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004605")]
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x040063C0 RID: 25536
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004606")]
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x040063C1 RID: 25537
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004607")]
		[SerializeField]
		private GameObject m_PassiveSkillTitle;

		// Token: 0x040063C2 RID: 25538
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004608")]
		[SerializeField]
		private GameObject m_ExistPassive;

		// Token: 0x040063C3 RID: 25539
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004609")]
		[SerializeField]
		private GameObject m_NoExistPassive;

		// Token: 0x040063C4 RID: 25540
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400460A")]
		[SerializeField]
		private Text m_Cost;

		// Token: 0x0200107B RID: 4219
		[Token(Token = "0x200124A")]
		public abstract class WeaponApplyArgumentBase : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x06005155 RID: 20821 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006338")]
			[Address(RVA = "0x10146D728", Offset = "0x146D728", VA = "0x10146D728")]
			public WeaponApplyArgumentBase(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType in_argumentType)
			{
			}

			// Token: 0x06005156 RID: 20822 RVA: 0x0001BFC0 File Offset: 0x0001A1C0
			[Token(Token = "0x6006339")]
			[Address(RVA = "0x10146D620", Offset = "0x146D620", VA = "0x10146D620")]
			public SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType GetArgumentType()
			{
				return SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType.userWeaponData;
			}

			// Token: 0x040063C5 RID: 25541
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40070F1")]
			private SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType argumentType;

			// Token: 0x0200107C RID: 4220
			[Token(Token = "0x2001354")]
			public enum eArgumentType
			{
				// Token: 0x040063C7 RID: 25543
				[Token(Token = "0x40075CE")]
				userWeaponData,
				// Token: 0x040063C8 RID: 25544
				[Token(Token = "0x40075CF")]
				userSupportData
			}
		}

		// Token: 0x0200107D RID: 4221
		[Token(Token = "0x200124B")]
		public abstract class ApplyArgument<ArgumentType> : SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase
		{
			// Token: 0x06005157 RID: 20823 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600633A")]
			[Address(RVA = "0x1016CE690", Offset = "0x16CE690", VA = "0x1016CE690")]
			public ApplyArgument(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType in_argumentType, ArgumentType in_argument)
			{
			}

			// Token: 0x040063C9 RID: 25545
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x40070F2")]
			public ArgumentType argument;
		}

		// Token: 0x0200107E RID: 4222
		[Token(Token = "0x200124C")]
		public class ApplyArgumentUserWeaponData : SelectedCharaInfoWeaponInfo.ApplyArgument<UserWeaponData>
		{
			// Token: 0x06005158 RID: 20824 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600633B")]
			[Address(RVA = "0x10146D6C4", Offset = "0x146D6C4", VA = "0x10146D6C4")]
			public ApplyArgumentUserWeaponData(UserWeaponData in_argument)
			{
			}
		}

		// Token: 0x0200107F RID: 4223
		[Token(Token = "0x200124D")]
		public class ApplyArgumentEquipWeaponPartyMemberController : SelectedCharaInfoWeaponInfo.ApplyArgument<EquipWeaponCharacterDataController>
		{
			// Token: 0x06005159 RID: 20825 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600633C")]
			[Address(RVA = "0x10146D660", Offset = "0x146D660", VA = "0x10146D660")]
			public ApplyArgumentEquipWeaponPartyMemberController(EquipWeaponCharacterDataController in_argument)
			{
			}
		}
	}
}
