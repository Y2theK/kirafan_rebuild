﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010AD RID: 4269
	[Token(Token = "0x2000B11")]
	[StructLayout(3)]
	public class SelectedSkillIcon : MonoBehaviour
	{
		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x060052A6 RID: 21158 RVA: 0x0001C3B0 File Offset: 0x0001A5B0
		// (set) Token: 0x060052A7 RID: 21159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000527")]
		public KeyValuePair<SelectedSkillIcon.eSkillType, int> SkillPair
		{
			[Token(Token = "0x6004C0C")]
			[Address(RVA = "0x10146D754", Offset = "0x146D754", VA = "0x10146D754")]
			get
			{
				return default(KeyValuePair<SelectedSkillIcon.eSkillType, int>);
			}
			[Token(Token = "0x6004C0D")]
			[Address(RVA = "0x10145B160", Offset = "0x145B160", VA = "0x10145B160")]
			set
			{
			}
		}

		// Token: 0x060052A8 RID: 21160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C0E")]
		[Address(RVA = "0x10146D75C", Offset = "0x146D75C", VA = "0x10146D75C")]
		public void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int level, bool isCurrent = false)
		{
		}

		// Token: 0x060052A9 RID: 21161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C0F")]
		[Address(RVA = "0x10146D97C", Offset = "0x146D97C", VA = "0x10146D97C")]
		public void ApplyPassive(int skillID, eElementType ownerElement, bool isCurrent = false)
		{
		}

		// Token: 0x060052AA RID: 21162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C10")]
		[Address(RVA = "0x10146DB20", Offset = "0x146DB20", VA = "0x10146DB20")]
		public void ApplyPassiveLock(int skillID, eElementType ownerElement, bool isCurrent = false)
		{
		}

		// Token: 0x060052AB RID: 21163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C11")]
		[Address(RVA = "0x10145B168", Offset = "0x145B168", VA = "0x10145B168")]
		public void ApplyMasterSkill(int skillID)
		{
		}

		// Token: 0x060052AC RID: 21164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C12")]
		[Address(RVA = "0x10145B1B8", Offset = "0x145B1B8", VA = "0x10145B1B8")]
		public void ApplyMasterSkillLock()
		{
		}

		// Token: 0x060052AD RID: 21165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C13")]
		[Address(RVA = "0x10145AFE0", Offset = "0x145AFE0", VA = "0x10145AFE0")]
		public void ApplyOrbBuff()
		{
		}

		// Token: 0x060052AE RID: 21166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C14")]
		[Address(RVA = "0x10146DCC4", Offset = "0x146DCC4", VA = "0x10146DCC4")]
		public void ApplyAbilitySphere()
		{
		}

		// Token: 0x060052AF RID: 21167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C15")]
		[Address(RVA = "0x10146DE44", Offset = "0x146DE44", VA = "0x10146DE44")]
		public void ApplyEmpty()
		{
		}

		// Token: 0x060052B0 RID: 21168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C16")]
		[Address(RVA = "0x101459874", Offset = "0x1459874", VA = "0x101459874")]
		public void SetIndex(int index)
		{
		}

		// Token: 0x060052B1 RID: 21169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C17")]
		[Address(RVA = "0x10145987C", Offset = "0x145987C", VA = "0x10145987C")]
		public void SetCurrent(bool isCurrent)
		{
		}

		// Token: 0x060052B2 RID: 21170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C18")]
		[Address(RVA = "0x10146DF2C", Offset = "0x146DF2C", VA = "0x10146DF2C")]
		public void UpdateEnable()
		{
		}

		// Token: 0x060052B3 RID: 21171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C19")]
		[Address(RVA = "0x10145986C", Offset = "0x145986C", VA = "0x10145986C")]
		public void SetCallback(Action<int> callback)
		{
		}

		// Token: 0x060052B4 RID: 21172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1A")]
		[Address(RVA = "0x10146DFCC", Offset = "0x146DFCC", VA = "0x10146DFCC")]
		public void OnClickButton()
		{
		}

		// Token: 0x060052B5 RID: 21173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1B")]
		[Address(RVA = "0x10146E020", Offset = "0x146E020", VA = "0x10146E020")]
		public void Destroy()
		{
		}

		// Token: 0x060052B6 RID: 21174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1C")]
		[Address(RVA = "0x10146E028", Offset = "0x146E028", VA = "0x10146E028")]
		public SelectedSkillIcon()
		{
		}

		// Token: 0x04006534 RID: 25908
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004716")]
		[SerializeField]
		private GameObject m_ClonerGroup;

		// Token: 0x04006535 RID: 25909
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004717")]
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x04006536 RID: 25910
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004718")]
		[SerializeField]
		private GameObject m_LevelGroup;

		// Token: 0x04006537 RID: 25911
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004719")]
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x04006538 RID: 25912
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400471A")]
		[SerializeField]
		private GameObject m_CursorGroup;

		// Token: 0x04006539 RID: 25913
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400471B")]
		[SerializeField]
		private Image m_Filter;

		// Token: 0x0400653A RID: 25914
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400471C")]
		[SerializeField]
		private Image m_BG;

		// Token: 0x0400653B RID: 25915
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400471D")]
		[SerializeField]
		private Image m_LockIcon;

		// Token: 0x0400653C RID: 25916
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400471E")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x0400653D RID: 25917
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400471F")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x0400653E RID: 25918
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004720")]
		private KeyValuePair<SelectedSkillIcon.eSkillType, int> m_SkillType;

		// Token: 0x0400653F RID: 25919
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004721")]
		private int m_Index;

		// Token: 0x04006540 RID: 25920
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004722")]
		private Action<int> m_OnClickButtonCallback;

		// Token: 0x020010AE RID: 4270
		[Token(Token = "0x200125D")]
		public enum eSkillType
		{
			// Token: 0x04006542 RID: 25922
			[Token(Token = "0x4007153")]
			None,
			// Token: 0x04006543 RID: 25923
			[Token(Token = "0x4007154")]
			Unique,
			// Token: 0x04006544 RID: 25924
			[Token(Token = "0x4007155")]
			Class,
			// Token: 0x04006545 RID: 25925
			[Token(Token = "0x4007156")]
			Active,
			// Token: 0x04006546 RID: 25926
			[Token(Token = "0x4007157")]
			Passive,
			// Token: 0x04006547 RID: 25927
			[Token(Token = "0x4007158")]
			PassiveLock,
			// Token: 0x04006548 RID: 25928
			[Token(Token = "0x4007159")]
			Master,
			// Token: 0x04006549 RID: 25929
			[Token(Token = "0x400715A")]
			MasterLock,
			// Token: 0x0400654A RID: 25930
			[Token(Token = "0x400715B")]
			MasterOrbBuff,
			// Token: 0x0400654B RID: 25931
			[Token(Token = "0x400715C")]
			AbilitySphere,
			// Token: 0x0400654C RID: 25932
			[Token(Token = "0x400715D")]
			AbilitySphereEquip
		}
	}
}
