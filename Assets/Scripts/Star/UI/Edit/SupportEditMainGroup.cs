﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010DD RID: 4317
	[Token(Token = "0x2000B31")]
	[StructLayout(3)]
	public class SupportEditMainGroup : PartyEditMainGroupBase
	{
		// Token: 0x060053CE RID: 21454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D28")]
		[Address(RVA = "0x10146E030", Offset = "0x146E030", VA = "0x10146E030", Slot = "18")]
		public override void Setup(PartyEditUIBase.eMode mode, PartyEditUIBase ownerUI, int startPartyIndex, EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x060053CF RID: 21455 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D29")]
		[Address(RVA = "0x10146E320", Offset = "0x146E320", VA = "0x10146E320", Slot = "20")]
		protected override EquipWeaponPartiesController GetPartyCtrl()
		{
			return null;
		}

		// Token: 0x060053D0 RID: 21456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2A")]
		[Address(RVA = "0x10146E390", Offset = "0x146E390", VA = "0x10146E390", Slot = "21")]
		public override void OnChangePage()
		{
		}

		// Token: 0x060053D1 RID: 21457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2B")]
		[Address(RVA = "0x10146E3E8", Offset = "0x146E3E8", VA = "0x10146E3E8")]
		public SupportEditMainGroup()
		{
		}

		// Token: 0x0400661F RID: 26143
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40047A8")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04006620 RID: 26144
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40047A9")]
		[SerializeField]
		protected CustomButton m_ResetButton;
	}
}
