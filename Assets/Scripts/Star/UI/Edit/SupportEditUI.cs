﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010DE RID: 4318
	[Token(Token = "0x2000B32")]
	[StructLayout(3)]
	public class SupportEditUI : PartyEditUIBase
	{
		// Token: 0x060053D2 RID: 21458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2C")]
		[Address(RVA = "0x10146E3F0", Offset = "0x146E3F0", VA = "0x10146E3F0")]
		public void Setup(List<UserSupportData> supportDatas, string partyName, int supportLimit, PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.View)
		{
		}

		// Token: 0x060053D3 RID: 21459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2D")]
		[Address(RVA = "0x10146E558", Offset = "0x146E558", VA = "0x10146E558", Slot = "10")]
		public override void Setup(PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, int startPartyIndex = -1, [Optional] EquipWeaponPartiesController parties)
		{
		}

		// Token: 0x060053D4 RID: 21460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2E")]
		[Address(RVA = "0x10146E550", Offset = "0x146E550", VA = "0x10146E550")]
		protected void Setup(PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, int startPartyIndex = -1, [Optional] EquipWeaponPartiesController parties, [Optional] List<UserSupportData> supportDatas)
		{
		}

		// Token: 0x060053D5 RID: 21461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D2F")]
		[Address(RVA = "0x10146E560", Offset = "0x146E560", VA = "0x10146E560", Slot = "15")]
		public override void OnClickCharaCallBack()
		{
		}

		// Token: 0x060053D6 RID: 21462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D30")]
		[Address(RVA = "0x10146E588", Offset = "0x146E588", VA = "0x10146E588", Slot = "18")]
		public override void OnClickOpenPartyDetailButton()
		{
		}

		// Token: 0x060053D7 RID: 21463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D31")]
		[Address(RVA = "0x10146E730", Offset = "0x146E730", VA = "0x10146E730", Slot = "11")]
		protected override void UpdateStepEnd()
		{
		}

		// Token: 0x060053D8 RID: 21464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D32")]
		[Address(RVA = "0x10146E738", Offset = "0x146E738", VA = "0x10146E738", Slot = "12")]
		protected override void RemoveMember(int partyIndex, int memberIndex)
		{
		}

		// Token: 0x060053D9 RID: 21465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D33")]
		[Address(RVA = "0x10146E828", Offset = "0x146E828", VA = "0x10146E828", Slot = "13")]
		public override void OpenCharaDetailPlayer()
		{
		}

		// Token: 0x060053DA RID: 21466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D34")]
		[Address(RVA = "0x10146EB38", Offset = "0x146EB38", VA = "0x10146EB38", Slot = "17")]
		protected override void OpenWeapon(int partyIndex, int slotIndex)
		{
		}

		// Token: 0x060053DB RID: 21467 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D35")]
		[Address(RVA = "0x10146EBC4", Offset = "0x146EBC4", VA = "0x10146EBC4", Slot = "14")]
		public override string GetGlobalParamPartyName(long partyMngId)
		{
			return null;
		}

		// Token: 0x060053DC RID: 21468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D36")]
		[Address(RVA = "0x10146EC7C", Offset = "0x146EC7C", VA = "0x10146EC7C")]
		public SupportEditUI()
		{
		}

		// Token: 0x04006621 RID: 26145
		[Token(Token = "0x40047AA")]
		private const int PARTYPANEL_CHARA_ALL = 8;

		// Token: 0x04006622 RID: 26146
		[Token(Token = "0x40047AB")]
		private const int PARTYPANEL_CHARA_ENABLES = 8;

		// Token: 0x04006623 RID: 26147
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40047AC")]
		private List<UserSupportData> m_SupportDatas;
	}
}
