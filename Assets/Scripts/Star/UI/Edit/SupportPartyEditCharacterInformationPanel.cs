﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010E0 RID: 4320
	[Token(Token = "0x2000B34")]
	[StructLayout(3)]
	public class SupportPartyEditCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x060053DE RID: 21470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D38")]
		[Address(RVA = "0x10146EC90", Offset = "0x146EC90", VA = "0x10146EC90", Slot = "10")]
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060053DF RID: 21471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D39")]
		[Address(RVA = "0x10146ED30", Offset = "0x146ED30", VA = "0x10146ED30", Slot = "12")]
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x060053E0 RID: 21472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D3A")]
		[Address(RVA = "0x10146EF1C", Offset = "0x146EF1C", VA = "0x10146EF1C")]
		public SupportPartyEditCharacterInformationPanel()
		{
		}
	}
}
