﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x020010E2 RID: 4322
	[Token(Token = "0x2000B36")]
	[StructLayout(3)]
	public class SupportPartyPanel : PartyEditPartyPanelBase
	{
		// Token: 0x060053E4 RID: 21476 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D3E")]
		[Address(RVA = "0x10146EF24", Offset = "0x146EF24", VA = "0x10146EF24", Slot = "7")]
		public override PartyEditPartyPanelBaseCore CreateCore()
		{
			return null;
		}

		// Token: 0x060053E5 RID: 21477 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004D3F")]
		[Address(RVA = "0x10146EF7C", Offset = "0x146EF7C", VA = "0x10146EF7C", Slot = "8")]
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CreateArgument()
		{
			return null;
		}

		// Token: 0x060053E6 RID: 21478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004D40")]
		[Address(RVA = "0x10146F0DC", Offset = "0x146F0DC", VA = "0x10146F0DC")]
		public SupportPartyPanel()
		{
		}
	}
}
