﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020010AF RID: 4271
	[Token(Token = "0x2000B12")]
	[StructLayout(3)]
	public class UpgradeConfirmGroup : UIGroup
	{
		// Token: 0x060052B7 RID: 21175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1D")]
		[Address(RVA = "0x10146F0E0", Offset = "0x146F0E0", VA = "0x10146F0E0")]
		public void Setup(string beforeNowLevelStr, string afterNowLevelStr, string maxLevelStr, string expStr, int[] values, int[] addValues)
		{
		}

		// Token: 0x060052B8 RID: 21176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1E")]
		[Address(RVA = "0x10146F3A4", Offset = "0x146F3A4", VA = "0x10146F3A4")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060052B9 RID: 21177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C1F")]
		[Address(RVA = "0x10146F3B0", Offset = "0x146F3B0", VA = "0x10146F3B0")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x060052BA RID: 21178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004C20")]
		[Address(RVA = "0x10146F3BC", Offset = "0x146F3BC", VA = "0x10146F3BC")]
		public UpgradeConfirmGroup()
		{
		}

		// Token: 0x0400654D RID: 25933
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004723")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x0400654E RID: 25934
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004724")]
		[SerializeField]
		private Text m_UpgradeMessage;

		// Token: 0x0400654F RID: 25935
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004725")]
		[SerializeField]
		private LevelDisplay m_BeforeLevel;

		// Token: 0x04006550 RID: 25936
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004726")]
		[SerializeField]
		private LevelDisplay m_AfterLevel;

		// Token: 0x04006551 RID: 25937
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004727")]
		[SerializeField]
		private Text m_GetExp;

		// Token: 0x04006552 RID: 25938
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004728")]
		[SerializeField]
		private CharaStatus m_CharaStatus;

		// Token: 0x04006553 RID: 25939
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004729")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x04006554 RID: 25940
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400472A")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x04006555 RID: 25941
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400472B")]
		public Action OnClickDecideCallback;
	}
}
