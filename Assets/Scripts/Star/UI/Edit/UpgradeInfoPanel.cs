﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200108E RID: 4238
	[Token(Token = "0x2000AFE")]
	[StructLayout(3)]
	public class UpgradeInfoPanel : MonoBehaviour
	{
		// Token: 0x060051A7 RID: 20903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B10")]
		[Address(RVA = "0x10146F3C4", Offset = "0x146F3C4", VA = "0x10146F3C4")]
		public void Setup()
		{
		}

		// Token: 0x060051A8 RID: 20904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B11")]
		[Address(RVA = "0x10146F3C8", Offset = "0x146F3C8", VA = "0x10146F3C8")]
		public void Apply(long exp, long gold)
		{
		}

		// Token: 0x060051A9 RID: 20905 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004B12")]
		[Address(RVA = "0x10146F550", Offset = "0x146F550", VA = "0x10146F550")]
		public CustomButton GetDecideButton()
		{
			return null;
		}

		// Token: 0x060051AA RID: 20906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B13")]
		[Address(RVA = "0x10146F558", Offset = "0x146F558", VA = "0x10146F558")]
		public UpgradeInfoPanel()
		{
		}

		// Token: 0x04006409 RID: 25609
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004640")]
		[SerializeField]
		private Text m_ExpText;

		// Token: 0x0400640A RID: 25610
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004641")]
		[SerializeField]
		private Text m_MoneyText;

		// Token: 0x0400640B RID: 25611
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004642")]
		[SerializeField]
		private CustomButton m_DecideButton;
	}
}
