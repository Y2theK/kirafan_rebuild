﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Edit
{
	// Token: 0x0200108F RID: 4239
	[Token(Token = "0x2000AFF")]
	[StructLayout(3)]
	public class UpgradeMaterialData : ScrollItemData
	{
		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x060051AB RID: 20907 RVA: 0x0001C020 File Offset: 0x0001A220
		// (set) Token: 0x060051AC RID: 20908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700051C")]
		public int ItemID
		{
			[Token(Token = "0x6004B14")]
			[Address(RVA = "0x10146F560", Offset = "0x146F560", VA = "0x10146F560")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004B15")]
			[Address(RVA = "0x10146F568", Offset = "0x146F568", VA = "0x10146F568")]
			set
			{
			}
		}

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x060051AD RID: 20909 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060051AE RID: 20910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700051D")]
		public string ItemName
		{
			[Token(Token = "0x6004B16")]
			[Address(RVA = "0x10146F570", Offset = "0x146F570", VA = "0x10146F570")]
			get
			{
				return null;
			}
			[Token(Token = "0x6004B17")]
			[Address(RVA = "0x10146F578", Offset = "0x146F578", VA = "0x10146F578")]
			set
			{
			}
		}

		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x060051AF RID: 20911 RVA: 0x0001C038 File Offset: 0x0001A238
		// (set) Token: 0x060051B0 RID: 20912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700051E")]
		public int HaveNum
		{
			[Token(Token = "0x6004B18")]
			[Address(RVA = "0x10146F580", Offset = "0x146F580", VA = "0x10146F580")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004B19")]
			[Address(RVA = "0x10146F588", Offset = "0x146F588", VA = "0x10146F588")]
			set
			{
			}
		}

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x060051B1 RID: 20913 RVA: 0x0001C050 File Offset: 0x0001A250
		// (set) Token: 0x060051B2 RID: 20914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700051F")]
		public int UseNum
		{
			[Token(Token = "0x6004B1A")]
			[Address(RVA = "0x10146F590", Offset = "0x146F590", VA = "0x10146F590")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004B1B")]
			[Address(RVA = "0x10146F598", Offset = "0x146F598", VA = "0x10146F598")]
			set
			{
			}
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x060051B3 RID: 20915 RVA: 0x0001C068 File Offset: 0x0001A268
		// (set) Token: 0x060051B4 RID: 20916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000520")]
		public bool IsEnableAdd
		{
			[Token(Token = "0x6004B1C")]
			[Address(RVA = "0x10146F5A0", Offset = "0x146F5A0", VA = "0x10146F5A0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004B1D")]
			[Address(RVA = "0x10146F5A8", Offset = "0x146F5A8", VA = "0x10146F5A8")]
			set
			{
			}
		}

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x060051B5 RID: 20917 RVA: 0x0001C080 File Offset: 0x0001A280
		// (set) Token: 0x060051B6 RID: 20918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000521")]
		public int LimitCount
		{
			[Token(Token = "0x6004B1E")]
			[Address(RVA = "0x10146F5B0", Offset = "0x146F5B0", VA = "0x10146F5B0")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004B1F")]
			[Address(RVA = "0x10146F5B8", Offset = "0x146F5B8", VA = "0x10146F5B8")]
			set
			{
			}
		}

		// Token: 0x060051B7 RID: 20919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B20")]
		[Address(RVA = "0x10146F5C0", Offset = "0x146F5C0", VA = "0x10146F5C0")]
		public UpgradeMaterialData()
		{
		}

		// Token: 0x0400640C RID: 25612
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004643")]
		private int m_ItemID;

		// Token: 0x0400640D RID: 25613
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004644")]
		private string m_ItemName;

		// Token: 0x0400640E RID: 25614
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004645")]
		private int m_HaveNum;

		// Token: 0x0400640F RID: 25615
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004646")]
		private int m_UseNum;

		// Token: 0x04006410 RID: 25616
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004647")]
		private bool m_IsEnableAdd;

		// Token: 0x04006411 RID: 25617
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4004648")]
		private int m_LimitCount;
	}
}
