﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02001090 RID: 4240
	[Token(Token = "0x2000B00")]
	[StructLayout(3)]
	public class UpgradeMaterialIcon : ScrollItemIcon
	{
		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x060051B8 RID: 20920 RVA: 0x0001C098 File Offset: 0x0001A298
		// (set) Token: 0x060051B9 RID: 20921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000522")]
		public bool IsDirty
		{
			[Token(Token = "0x6004B21")]
			[Address(RVA = "0x10146F5C8", Offset = "0x146F5C8", VA = "0x10146F5C8")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004B22")]
			[Address(RVA = "0x10146F5D0", Offset = "0x146F5D0", VA = "0x10146F5D0")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060051BA RID: 20922 RVA: 0x0001C0B0 File Offset: 0x0001A2B0
		[Token(Token = "0x6004B23")]
		[Address(RVA = "0x10146F5D8", Offset = "0x146F5D8", VA = "0x10146F5D8")]
		public int GetItemID()
		{
			return 0;
		}

		// Token: 0x060051BB RID: 20923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B24")]
		[Address(RVA = "0x10146F668", Offset = "0x146F668", VA = "0x10146F668", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060051BC RID: 20924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B25")]
		[Address(RVA = "0x10146FBA4", Offset = "0x146FBA4", VA = "0x10146FBA4")]
		protected void UpdateDisplay(bool enableCallback)
		{
		}

		// Token: 0x060051BD RID: 20925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B26")]
		[Address(RVA = "0x10146FF08", Offset = "0x146FF08", VA = "0x10146FF08")]
		public void UpdateUI()
		{
		}

		// Token: 0x060051BE RID: 20926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B27")]
		[Address(RVA = "0x1014705A0", Offset = "0x14705A0", VA = "0x1014705A0")]
		public void UpdateEnableIncrease()
		{
		}

		// Token: 0x060051BF RID: 20927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B28")]
		[Address(RVA = "0x101470774", Offset = "0x1470774", VA = "0x101470774")]
		public void ForceUpdateNumber(int useNum)
		{
		}

		// Token: 0x060051C0 RID: 20928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B29")]
		[Address(RVA = "0x1014707B8", Offset = "0x14707B8", VA = "0x1014707B8")]
		private void OnChangeCurrentNum()
		{
		}

		// Token: 0x060051C1 RID: 20929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B2A")]
		[Address(RVA = "0x1014707CC", Offset = "0x14707CC", VA = "0x1014707CC")]
		public UpgradeMaterialIcon()
		{
		}

		// Token: 0x04006412 RID: 25618
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004649")]
		[SerializeField]
		private PrefabCloner m_ItemIconCloner;

		// Token: 0x04006413 RID: 25619
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400464A")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04006414 RID: 25620
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400464B")]
		[SerializeField]
		private Text m_HaveNumTextObj;

		// Token: 0x04006415 RID: 25621
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400464C")]
		[SerializeField]
		private PrefabCloner m_NumberSelectCloner;

		// Token: 0x04006416 RID: 25622
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400464D")]
		private ItemIconWithFrame m_ItemIcon;

		// Token: 0x04006417 RID: 25623
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400464E")]
		private NumberSelect m_NumberSelect;

		// Token: 0x04006418 RID: 25624
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400464F")]
		private UpgradeMaterialData m_UpgradeMaterialData;
	}
}
