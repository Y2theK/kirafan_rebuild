﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001091 RID: 4241
	[Token(Token = "0x2000B01")]
	[StructLayout(3)]
	public class UpgradeMaterialList : ScrollViewBase
	{
		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x060051C3 RID: 20931 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060051C2 RID: 20930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000523")]
		public UpgradeMaterialPanel Owner
		{
			[Token(Token = "0x6004B2C")]
			[Address(RVA = "0x10146FD94", Offset = "0x146FD94", VA = "0x10146FD94")]
			get
			{
				return null;
			}
			[Token(Token = "0x6004B2B")]
			[Address(RVA = "0x1014707D4", Offset = "0x14707D4", VA = "0x1014707D4")]
			set
			{
			}
		}

		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x060051C4 RID: 20932 RVA: 0x0001C0C8 File Offset: 0x0001A2C8
		// (set) Token: 0x060051C5 RID: 20933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000524")]
		public int SelectPage
		{
			[Token(Token = "0x6004B2D")]
			[Address(RVA = "0x1014707DC", Offset = "0x14707DC", VA = "0x1014707DC")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004B2E")]
			[Address(RVA = "0x1014707E4", Offset = "0x14707E4", VA = "0x1014707E4")]
			set
			{
			}
		}

		// Token: 0x060051C6 RID: 20934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B2F")]
		[Address(RVA = "0x1014707EC", Offset = "0x14707EC", VA = "0x1014707EC")]
		public void Setup(UpgradeMaterialList.eMode mode, UpgradeMaterialPanel owner, long charaMngID)
		{
		}

		// Token: 0x060051C7 RID: 20935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B30")]
		[Address(RVA = "0x101470804", Offset = "0x1470804", VA = "0x101470804", Slot = "7")]
		public override void Setup()
		{
		}

		// Token: 0x060051C8 RID: 20936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B31")]
		[Address(RVA = "0x101471990", Offset = "0x1471990", VA = "0x101471990", Slot = "9")]
		public override void LateUpdate()
		{
		}

		// Token: 0x060051C9 RID: 20937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B32")]
		[Address(RVA = "0x101471700", Offset = "0x1471700", VA = "0x101471700")]
		public void AddItem(ScrollItemData item, int page)
		{
		}

		// Token: 0x060051CA RID: 20938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B33")]
		[Address(RVA = "0x1014718B0", Offset = "0x14718B0", VA = "0x1014718B0")]
		public void ChangePage(int page, bool force = false)
		{
		}

		// Token: 0x060051CB RID: 20939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B34")]
		[Address(RVA = "0x101471BB0", Offset = "0x1471BB0", VA = "0x101471BB0")]
		public void ResetUseNum()
		{
		}

		// Token: 0x060051CC RID: 20940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B35")]
		[Address(RVA = "0x101471FE8", Offset = "0x1471FE8", VA = "0x101471FE8")]
		public void UpdateEnableIncrease()
		{
		}

		// Token: 0x060051CD RID: 20941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B36")]
		[Address(RVA = "0x1014721A4", Offset = "0x14721A4", VA = "0x1014721A4")]
		public void ForceUpdateNumber(List<KeyValuePair<int, int>> orderList)
		{
		}

		// Token: 0x060051CE RID: 20942 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004B37")]
		[Address(RVA = "0x1014717BC", Offset = "0x14717BC", VA = "0x1014717BC")]
		private UpgradeMaterialData CreateMaterialData(int itemID, string itemName, int limitCount = 0)
		{
			return null;
		}

		// Token: 0x060051CF RID: 20943 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004B38")]
		[Address(RVA = "0x1014715F8", Offset = "0x14715F8", VA = "0x1014715F8")]
		private UIItemSortUtility.ItemSortData<UpgradeMaterialData> CreateItemSortData(ItemListDB_Param itemParam, int limitCount = 0)
		{
			return null;
		}

		// Token: 0x060051D0 RID: 20944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B39")]
		[Address(RVA = "0x101472560", Offset = "0x1472560", VA = "0x101472560")]
		public UpgradeMaterialList()
		{
		}

		// Token: 0x0400641A RID: 25626
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004651")]
		[SerializeField]
		private UpgradeMaterialPanel m_Owner;

		// Token: 0x0400641B RID: 25627
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004652")]
		private List<ScrollItemData>[] m_ItemDataLists;

		// Token: 0x0400641C RID: 25628
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004653")]
		private List<ScrollItemData> m_AllItemDataList;

		// Token: 0x0400641D RID: 25629
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004654")]
		private int m_SelectPage;

		// Token: 0x0400641E RID: 25630
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4004655")]
		private UpgradeMaterialList.eMode m_Mode;

		// Token: 0x0400641F RID: 25631
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004656")]
		private long m_CharaMngID;

		// Token: 0x04006420 RID: 25632
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004657")]
		private int includeSpecifyLength;

		// Token: 0x02001092 RID: 4242
		[Token(Token = "0x2001251")]
		public enum eMode
		{
			// Token: 0x04006422 RID: 25634
			[Token(Token = "0x40070FE")]
			Chara,
			// Token: 0x04006423 RID: 25635
			[Token(Token = "0x40070FF")]
			Weapon,
			// Token: 0x04006424 RID: 25636
			[Token(Token = "0x4007100")]
			LimitBreak
		}
	}
}
