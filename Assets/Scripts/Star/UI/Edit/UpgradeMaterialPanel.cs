﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02001093 RID: 4243
	[Token(Token = "0x2000B02")]
	[StructLayout(3)]
	public class UpgradeMaterialPanel : CharaEditSceneMaterialPanel
	{
		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x060051D1 RID: 20945 RVA: 0x0001C0E0 File Offset: 0x0001A2E0
		// (set) Token: 0x060051D2 RID: 20946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000525")]
		public bool IsEnableIncrease
		{
			[Token(Token = "0x6004B3A")]
			[Address(RVA = "0x101472578", Offset = "0x1472578", VA = "0x101472578")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004B3B")]
			[Address(RVA = "0x101472580", Offset = "0x1472580", VA = "0x101472580")]
			set
			{
			}
		}

		// Token: 0x060051D3 RID: 20947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B3C")]
		[Address(RVA = "0x101472588", Offset = "0x1472588", VA = "0x101472588")]
		public void Setup(UpgradeMaterialPanel.eMode mode, long charaMngID)
		{
		}

		// Token: 0x060051D4 RID: 20948 RVA: 0x0001C0F8 File Offset: 0x0001A2F8
		[Token(Token = "0x6004B3D")]
		[Address(RVA = "0x1014729BC", Offset = "0x14729BC", VA = "0x1014729BC")]
		public bool IsEmptyLineup()
		{
			return default(bool);
		}

		// Token: 0x060051D5 RID: 20949 RVA: 0x0001C110 File Offset: 0x0001A310
		[Token(Token = "0x6004B3E")]
		[Address(RVA = "0x101472A3C", Offset = "0x1472A3C", VA = "0x101472A3C")]
		public int GetLineupCount()
		{
			return 0;
		}

		// Token: 0x060051D6 RID: 20950 RVA: 0x0001C128 File Offset: 0x0001A328
		[Token(Token = "0x6004B3F")]
		[Address(RVA = "0x101472A44", Offset = "0x1472A44", VA = "0x101472A44", Slot = "4")]
		public override int GetSelectButtonIdx()
		{
			return 0;
		}

		// Token: 0x060051D7 RID: 20951 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004B40")]
		[Address(RVA = "0x101472AE4", Offset = "0x1472AE4", VA = "0x101472AE4", Slot = "5")]
		protected override CustomButton GetDecideButton()
		{
			return null;
		}

		// Token: 0x060051D8 RID: 20952 RVA: 0x0001C140 File Offset: 0x0001A340
		[Token(Token = "0x6004B41")]
		[Address(RVA = "0x101470478", Offset = "0x1470478", VA = "0x101470478")]
		public int GetUseItemNum(int itemID)
		{
			return 0;
		}

		// Token: 0x060051D9 RID: 20953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B42")]
		[Address(RVA = "0x101472B78", Offset = "0x1472B78", VA = "0x101472B78")]
		public void SetSelectedItemNum(int itemID, int num)
		{
		}

		// Token: 0x060051DA RID: 20954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B43")]
		[Address(RVA = "0x101472820", Offset = "0x1472820", VA = "0x101472820")]
		public void ResetUseItem(bool isClear = false)
		{
		}

		// Token: 0x060051DB RID: 20955 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004B44")]
		[Address(RVA = "0x101472D98", Offset = "0x1472D98", VA = "0x101472D98")]
		public List<KeyValuePair<int, int>> GetSelectItem()
		{
			return null;
		}

		// Token: 0x060051DC RID: 20956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B45")]
		[Address(RVA = "0x101472DA0", Offset = "0x1472DA0", VA = "0x101472DA0")]
		public void ApplyUpgradeInfoPanel(long exp, int amount)
		{
		}

		// Token: 0x060051DD RID: 20957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B46")]
		[Address(RVA = "0x101472E54", Offset = "0x1472E54", VA = "0x101472E54")]
		public void SetInteractableDecideButton(bool interactable)
		{
		}

		// Token: 0x060051DE RID: 20958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B47")]
		[Address(RVA = "0x101472F10", Offset = "0x1472F10", VA = "0x101472F10")]
		public void UpdateEnableIncrease()
		{
		}

		// Token: 0x060051DF RID: 20959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B48")]
		[Address(RVA = "0x101472F3C", Offset = "0x1472F3C", VA = "0x101472F3C")]
		public void ForceUpdateNumber(List<KeyValuePair<int, int>> orderList)
		{
		}

		// Token: 0x060051E0 RID: 20960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B49")]
		[Address(RVA = "0x101472F70", Offset = "0x1472F70", VA = "0x101472F70")]
		public void SetActiveSelectItemIcon(bool active)
		{
		}

		// Token: 0x060051E1 RID: 20961 RVA: 0x0001C158 File Offset: 0x0001A358
		[Token(Token = "0x6004B4A")]
		[Address(RVA = "0x10146FD9C", Offset = "0x146FD9C", VA = "0x10146FD9C")]
		public bool CheckEnableIncrease(int itemID)
		{
			return default(bool);
		}

		// Token: 0x060051E2 RID: 20962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B4B")]
		[Address(RVA = "0x10147301C", Offset = "0x147301C", VA = "0x10147301C", Slot = "6")]
		public override void OnClickSelectButton()
		{
		}

		// Token: 0x060051E3 RID: 20963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B4C")]
		[Address(RVA = "0x101473064", Offset = "0x1473064", VA = "0x101473064", Slot = "7")]
		public override void OnClickSelectButton(int idx)
		{
		}

		// Token: 0x060051E4 RID: 20964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B4D")]
		[Address(RVA = "0x101471BA8", Offset = "0x1471BA8", VA = "0x101471BA8")]
		public void CallExecuteOnMaterialsChanged()
		{
		}

		// Token: 0x060051E5 RID: 20965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004B4E")]
		[Address(RVA = "0x1014730B4", Offset = "0x14730B4", VA = "0x1014730B4")]
		public UpgradeMaterialPanel()
		{
		}

		// Token: 0x04006425 RID: 25637
		[Token(Token = "0x4004658")]
		public const int USE_MATERIAL_MAX = 999;

		// Token: 0x04006426 RID: 25638
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004659")]
		[SerializeField]
		private SelectItemPanel m_InfoSelectItem;

		// Token: 0x04006427 RID: 25639
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400465A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F994", Offset = "0x12F994")]
		private TabGroup m_TabGroup;

		// Token: 0x04006428 RID: 25640
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400465B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012F9E0", Offset = "0x12F9E0")]
		private UpgradeMaterialList m_Scroll;

		// Token: 0x04006429 RID: 25641
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400465C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012FA2C", Offset = "0x12FA2C")]
		private UpgradeInfoPanel m_InfoPanel;

		// Token: 0x0400642A RID: 25642
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400465D")]
		private bool m_IsEnableIncrease;

		// Token: 0x0400642B RID: 25643
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400465E")]
		private int m_LineupCount;

		// Token: 0x0400642C RID: 25644
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400465F")]
		private List<KeyValuePair<int, int>> m_SelectItem;

		// Token: 0x02001094 RID: 4244
		[Token(Token = "0x2001252")]
		public enum eMode
		{
			// Token: 0x0400642E RID: 25646
			[Token(Token = "0x4007102")]
			Chara,
			// Token: 0x0400642F RID: 25647
			[Token(Token = "0x4007103")]
			Weapon,
			// Token: 0x04006430 RID: 25648
			[Token(Token = "0x4007104")]
			LimitBreak
		}
	}
}
