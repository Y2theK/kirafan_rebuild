﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CE2 RID: 3298
	[Token(Token = "0x20008CE")]
	[StructLayout(3)]
	public class ElementIcon : MonoBehaviour
	{
		// Token: 0x06003C61 RID: 15457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371E")]
		[Address(RVA = "0x10145CEE4", Offset = "0x145CEE4", VA = "0x10145CEE4")]
		public void Apply(eElementType element)
		{
		}

		// Token: 0x06003C62 RID: 15458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600371F")]
		[Address(RVA = "0x101473134", Offset = "0x1473134", VA = "0x101473134")]
		public void SetActive(bool flag)
		{
		}

		// Token: 0x06003C63 RID: 15459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003720")]
		[Address(RVA = "0x1014731F0", Offset = "0x14731F0", VA = "0x1014731F0")]
		public ElementIcon()
		{
		}

		// Token: 0x04004BA8 RID: 19368
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400352C")]
		[SerializeField]
		private Sprite m_Random;

		// Token: 0x04004BA9 RID: 19369
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400352D")]
		[SerializeField]
		private Sprite m_Fire;

		// Token: 0x04004BAA RID: 19370
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400352E")]
		[SerializeField]
		private Sprite m_Water;

		// Token: 0x04004BAB RID: 19371
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400352F")]
		[SerializeField]
		private Sprite m_Earth;

		// Token: 0x04004BAC RID: 19372
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003530")]
		[SerializeField]
		private Sprite m_Wind;

		// Token: 0x04004BAD RID: 19373
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003531")]
		[SerializeField]
		private Sprite m_Moon;

		// Token: 0x04004BAE RID: 19374
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003532")]
		[SerializeField]
		private Sprite m_Sun;

		// Token: 0x04004BAF RID: 19375
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003533")]
		private Image m_IconObj;

		// Token: 0x04004BB0 RID: 19376
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003534")]
		private GameObject m_GameObject;
	}
}
