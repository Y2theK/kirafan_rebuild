﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CB8 RID: 3256
	[Token(Token = "0x20008B9")]
	[StructLayout(3)]
	public class ElementIconList : MonoBehaviour
	{
		// Token: 0x06003BC0 RID: 15296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003682")]
		[Address(RVA = "0x10145D8C0", Offset = "0x145D8C0", VA = "0x10145D8C0")]
		public void ApplyNowSelectedQuest()
		{
		}

		// Token: 0x06003BC1 RID: 15297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003683")]
		[Address(RVA = "0x1014731F8", Offset = "0x14731F8", VA = "0x1014731F8")]
		public void Apply(int QuestId)
		{
		}

		// Token: 0x06003BC2 RID: 15298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003684")]
		[Address(RVA = "0x101473290", Offset = "0x1473290", VA = "0x101473290")]
		public void Apply(bool[] elementsFlags)
		{
		}

		// Token: 0x06003BC3 RID: 15299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003685")]
		[Address(RVA = "0x10147373C", Offset = "0x147373C", VA = "0x10147373C")]
		public ElementIconList()
		{
		}

		// Token: 0x04004A96 RID: 19094
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003498")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100123A3C", Offset = "0x123A3C")]
		private ElementIcon m_Prefab;

		// Token: 0x04004A97 RID: 19095
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003499")]
		[SerializeField]
		private Vector2 m_IconSize;

		// Token: 0x04004A98 RID: 19096
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400349A")]
		private List<ElementIcon> m_Icons;
	}
}
