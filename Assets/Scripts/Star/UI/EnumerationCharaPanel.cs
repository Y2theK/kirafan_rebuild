﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D9B RID: 3483
	[Token(Token = "0x2000954")]
	[StructLayout(3)]
	public class EnumerationCharaPanel : EnumerationCharaPanelStandardBase
	{
		// Token: 0x06004038 RID: 16440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD7")]
		[Address(RVA = "0x101473744", Offset = "0x1473744", VA = "0x101473744", Slot = "4")]
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
		}

		// Token: 0x06004039 RID: 16441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD8")]
		[Address(RVA = "0x1014737A4", Offset = "0x14737A4", VA = "0x1014737A4")]
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x0600403A RID: 16442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD9")]
		[Address(RVA = "0x101473808", Offset = "0x1473808", VA = "0x101473808")]
		public EnumerationCharaPanel()
		{
		}
	}
}
