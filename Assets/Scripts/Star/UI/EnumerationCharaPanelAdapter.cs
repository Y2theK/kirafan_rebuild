﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D8D RID: 3469
	[Token(Token = "0x200094B")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapter : EnumerationCharaPanelAdapterBaseExGen<EnumerationCharaPanel>
	{
		// Token: 0x06004007 RID: 16391 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AAC")]
		[Address(RVA = "0x10147385C", Offset = "0x147385C", VA = "0x10147385C", Slot = "8")]
		public virtual EnumerationCharaPanelStandardBase.SharedInstanceEx CreateArgument()
		{
			return null;
		}

		// Token: 0x06004008 RID: 16392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AAD")]
		[Address(RVA = "0x101473AEC", Offset = "0x1473AEC", VA = "0x101473AEC")]
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x06004009 RID: 16393 RVA: 0x00018F18 File Offset: 0x00017118
		[Token(Token = "0x6003AAE")]
		[Address(RVA = "0x101473AFC", Offset = "0x1473AFC", VA = "0x101473AFC")]
		public int GetSlotIndex()
		{
			return 0;
		}

		// Token: 0x0600400A RID: 16394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AAF")]
		[Address(RVA = "0x101473B7C", Offset = "0x1473B7C", VA = "0x101473B7C")]
		protected EnumerationCharaPanelAdapter()
		{
		}
	}
}
