﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D8B RID: 3467
	[Token(Token = "0x2000949")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapterBase : MonoBehaviour
	{
		// Token: 0x06003FFF RID: 16383
		[Token(Token = "0x6003AA4")]
		[Address(Slot = "4")]
		public abstract void Destroy();

		// Token: 0x06004000 RID: 16384
		[Token(Token = "0x6003AA5")]
		[Address(Slot = "5")]
		protected abstract EnumerationPanelBase GetParent();

		// Token: 0x06004001 RID: 16385
		[Token(Token = "0x6003AA6")]
		[Address(Slot = "6")]
		public abstract void SetMode(EnumerationCharaPanelBase.eMode mode);

		// Token: 0x06004002 RID: 16386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA7")]
		[Address(RVA = "0x101473BCC", Offset = "0x1473BCC", VA = "0x101473BCC")]
		protected EnumerationCharaPanelAdapterBase()
		{
		}

		// Token: 0x04004F6C RID: 20332
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40037FC")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126148", Offset = "0x126148")]
		protected GameObject m_DataContent;

		// Token: 0x04004F6D RID: 20333
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40037FD")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126194", Offset = "0x126194")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

		// Token: 0x04004F6E RID: 20334
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037FE")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001261E0", Offset = "0x1261E0")]
		protected GameObject m_Blank;

		// Token: 0x04004F6F RID: 20335
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40037FF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012622C", Offset = "0x12622C")]
		protected GameObject m_EmptyObj;

		// Token: 0x04004F70 RID: 20336
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003800")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126278", Offset = "0x126278")]
		protected GameObject m_FriendObj;

		// Token: 0x04004F71 RID: 20337
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003801")]
		[SerializeField]
		protected GameObject m_NpcOnlyObj;

		// Token: 0x04004F72 RID: 20338
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003802")]
		[SerializeField]
		protected EnumerationCharaPanelBase.SharedInstance m_SerializedConstructInstances;

		// Token: 0x04004F73 RID: 20339
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003803")]
		protected EnumerationCharaPanelBase.SharedInstance m_SharedInstance;
	}
}
