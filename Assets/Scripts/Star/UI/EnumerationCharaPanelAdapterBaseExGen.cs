﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D8C RID: 3468
	[Token(Token = "0x200094A")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapterBaseExGen<CoreType> : EnumerationCharaPanelAdapterBase where CoreType : EnumerationCharaPanelBase, new()
	{
		// Token: 0x06004003 RID: 16387 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AA8")]
		[Address(RVA = "0x1016CE6D8", Offset = "0x16CE6D8", VA = "0x1016CE6D8", Slot = "7")]
		public virtual CoreType CreateCore()
		{
			return null;
		}

		// Token: 0x06004004 RID: 16388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AA9")]
		[Address(RVA = "0x1016CE6EC", Offset = "0x16CE6EC", VA = "0x1016CE6EC", Slot = "4")]
		public override void Destroy()
		{
		}

		// Token: 0x06004005 RID: 16389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AAA")]
		[Address(RVA = "0x1016CE704", Offset = "0x16CE704", VA = "0x1016CE704", Slot = "6")]
		public override void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
		}

		// Token: 0x06004006 RID: 16390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AAB")]
		[Address(RVA = "0x1016CE718", Offset = "0x16CE718", VA = "0x1016CE718")]
		protected EnumerationCharaPanelAdapterBaseExGen()
		{
		}

		// Token: 0x04004F74 RID: 20340
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003804")]
		protected CoreType m_Core;
	}
}
