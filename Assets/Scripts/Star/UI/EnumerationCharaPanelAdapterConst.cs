﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D8F RID: 3471
	[Token(Token = "0x200094D")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapterConst : EnumerationCharaPanelAdapterBaseExGen<EnumerationCharaPanelConst>
	{
		// Token: 0x0600400E RID: 16398 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AB3")]
		[Address(RVA = "0x101473BD4", Offset = "0x1473BD4", VA = "0x101473BD4", Slot = "8")]
		public virtual EnumerationCharaPanelStandardBase.SharedInstanceEx CreateArgument(EquipWeaponPartyMemberController partyMemberController)
		{
			return null;
		}

		// Token: 0x0600400F RID: 16399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB4")]
		[Address(RVA = "0x101473E28", Offset = "0x1473E28", VA = "0x101473E28")]
		public void Apply()
		{
		}

		// Token: 0x06004010 RID: 16400 RVA: 0x00018F30 File Offset: 0x00017130
		[Token(Token = "0x6003AB5")]
		[Address(RVA = "0x101473E74", Offset = "0x1473E74", VA = "0x101473E74")]
		public int GetSlotIndex()
		{
			return 0;
		}

		// Token: 0x06004011 RID: 16401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB6")]
		[Address(RVA = "0x101473E88", Offset = "0x1473E88", VA = "0x101473E88")]
		protected EnumerationCharaPanelAdapterConst()
		{
		}
	}
}
