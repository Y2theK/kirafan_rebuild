﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D90 RID: 3472
	[Token(Token = "0x200094E")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapterConstExGen<ParentType> : EnumerationCharaPanelAdapterConst where ParentType : EnumerationPanelBase
	{
		// Token: 0x06004012 RID: 16402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB7")]
		[Address(RVA = "0x1016CE744", Offset = "0x16CE744", VA = "0x1016CE744", Slot = "9")]
		public virtual void Setup(ParentType parent, EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x06004013 RID: 16403 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AB8")]
		[Address(RVA = "0x1016CE7D4", Offset = "0x16CE7D4", VA = "0x1016CE7D4", Slot = "5")]
		protected override EnumerationPanelBase GetParent()
		{
			return null;
		}

		// Token: 0x06004014 RID: 16404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB9")]
		[Address(RVA = "0x1016CE7DC", Offset = "0x16CE7DC", VA = "0x1016CE7DC")]
		protected EnumerationCharaPanelAdapterConstExGen()
		{
		}

		// Token: 0x04004F76 RID: 20342
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003806")]
		protected ParentType m_Parent;
	}
}
