﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D8E RID: 3470
	[Token(Token = "0x200094C")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelAdapterExGen<ParentType> : EnumerationCharaPanelAdapter where ParentType : EnumerationPanelBase
	{
		// Token: 0x0600400B RID: 16395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB0")]
		[Address(RVA = "0x1016CE808", Offset = "0x16CE808", VA = "0x1016CE808", Slot = "9")]
		public virtual void Setup(ParentType parent)
		{
		}

		// Token: 0x0600400C RID: 16396 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AB1")]
		[Address(RVA = "0x1016CE888", Offset = "0x16CE888", VA = "0x1016CE888", Slot = "5")]
		protected override EnumerationPanelBase GetParent()
		{
			return null;
		}

		// Token: 0x0600400D RID: 16397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AB2")]
		[Address(RVA = "0x1016CE890", Offset = "0x16CE890", VA = "0x1016CE890")]
		protected EnumerationCharaPanelAdapterExGen()
		{
		}

		// Token: 0x04004F75 RID: 20341
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003805")]
		protected ParentType m_Parent;
	}
}
