﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D93 RID: 3475
	[Token(Token = "0x2000951")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelBase
	{
		// Token: 0x0600401E RID: 16414 RVA: 0x00018F48 File Offset: 0x00017148
		[Token(Token = "0x6003AC3")]
		[Address(RVA = "0x101473ED8", Offset = "0x1473ED8", VA = "0x101473ED8")]
		public static WeaponIconWithFrame.eMode ConvertEnumerationCharaPanelBaseeModeToWeaponIconWithFrameeMode(EnumerationCharaPanelBase.eMode mode)
		{
			return WeaponIconWithFrame.eMode.Normal;
		}

		// Token: 0x0600401F RID: 16415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC4")]
		[Address(RVA = "0x101473EF8", Offset = "0x1473EF8", VA = "0x101473EF8", Slot = "4")]
		public virtual void Setup(EnumerationCharaPanelBase.SharedInstance sharedInstance)
		{
		}

		// Token: 0x06004020 RID: 16416
		[Token(Token = "0x6003AC5")]
		[Address(Slot = "5")]
		public abstract void Destroy();

		// Token: 0x06004021 RID: 16417 RVA: 0x00018F60 File Offset: 0x00017160
		[Token(Token = "0x6003AC6")]
		[Address(RVA = "0x101473EFC", Offset = "0x1473EFC", VA = "0x101473EFC")]
		protected EnumerationCharaPanelBase.eMode GetMode()
		{
			return EnumerationCharaPanelBase.eMode.Edit;
		}

		// Token: 0x06004022 RID: 16418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC7")]
		[Address(RVA = "0x101473F04", Offset = "0x1473F04", VA = "0x101473F04")]
		public void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
		}

		// Token: 0x06004023 RID: 16419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC8")]
		[Address(RVA = "0x101473F0C", Offset = "0x1473F0C", VA = "0x101473F0C")]
		protected EnumerationCharaPanelBase()
		{
		}

		// Token: 0x04004F80 RID: 20352
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003810")]
		private EnumerationCharaPanelBase.eMode m_Mode;

		// Token: 0x02000D94 RID: 3476
		[Token(Token = "0x2001108")]
		public enum eMode
		{
			// Token: 0x04004F82 RID: 20354
			[Token(Token = "0x4006ABA")]
			Edit,
			// Token: 0x04004F83 RID: 20355
			[Token(Token = "0x4006ABB")]
			Quest,
			// Token: 0x04004F84 RID: 20356
			[Token(Token = "0x4006ABC")]
			QuestStart,
			// Token: 0x04004F85 RID: 20357
			[Token(Token = "0x4006ABD")]
			View,
			// Token: 0x04004F86 RID: 20358
			[Token(Token = "0x4006ABE")]
			PartyDetail
		}

		// Token: 0x02000D95 RID: 3477
		[Token(Token = "0x2001109")]
		public enum eButton
		{
			// Token: 0x04004F88 RID: 20360
			[Token(Token = "0x4006AC0")]
			Chara,
			// Token: 0x04004F89 RID: 20361
			[Token(Token = "0x4006AC1")]
			Weapon
		}

		// Token: 0x02000D96 RID: 3478
		[Token(Token = "0x200110A")]
		[Serializable]
		public class SharedInstance
		{
			// Token: 0x06004024 RID: 16420 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620E")]
			[Address(RVA = "0x101473F14", Offset = "0x1473F14", VA = "0x101473F14")]
			public SharedInstance()
			{
			}

			// Token: 0x06004025 RID: 16421 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620F")]
			[Address(RVA = "0x101473F1C", Offset = "0x1473F1C", VA = "0x101473F1C")]
			public SharedInstance(EnumerationCharaPanelBase.SharedInstance argument)
			{
			}

			// Token: 0x04004F8A RID: 20362
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006AC2")]
			[SerializeField]
			public GameObject m_DataContent;

			// Token: 0x04004F8B RID: 20363
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006AC3")]
			[SerializeField]
			[Attribute(Name = "TooltipAttribute", RVA = "0x100134A48", Offset = "0x134A48")]
			public EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

			// Token: 0x04004F8C RID: 20364
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006AC4")]
			[SerializeField]
			public GameObject m_Blank;

			// Token: 0x04004F8D RID: 20365
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006AC5")]
			[SerializeField]
			public GameObject m_EmptyObj;

			// Token: 0x04004F8E RID: 20366
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006AC6")]
			[SerializeField]
			public GameObject m_FriendObj;

			// Token: 0x04004F8F RID: 20367
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006AC7")]
			[SerializeField]
			public GameObject m_NpcOnlyObj;
		}

		// Token: 0x02000D97 RID: 3479
		[Token(Token = "0x200110B")]
		[Serializable]
		public abstract class SharedInstanceExGen<ParentType> : EnumerationCharaPanelBase.SharedInstance where ParentType : MonoBehaviour
		{
			// Token: 0x06004026 RID: 16422 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006210")]
			[Address(RVA = "0x1016CE8BC", Offset = "0x16CE8BC", VA = "0x1016CE8BC")]
			public SharedInstanceExGen()
			{
			}

			// Token: 0x06004027 RID: 16423 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006211")]
			[Address(RVA = "0x1016CE8E8", Offset = "0x16CE8E8", VA = "0x1016CE8E8")]
			public SharedInstanceExGen(EnumerationCharaPanelBase.SharedInstance argument)
			{
			}

			// Token: 0x04004F90 RID: 20368
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006AC8")]
			public ParentType parent;
		}
	}
}
