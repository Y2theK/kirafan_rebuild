﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaSelect;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D98 RID: 3480
	[Token(Token = "0x2000952")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelBaseExGen<ParentType, SetupArgumentType> : EnumerationCharaPanelBase where ParentType : MonoBehaviour where SetupArgumentType : EnumerationCharaPanelBase.SharedInstanceExGen<ParentType>
	{
		// Token: 0x06004028 RID: 16424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC9")]
		[Address(RVA = "0x1016CE91C", Offset = "0x16CE91C", VA = "0x1016CE91C", Slot = "4")]
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
		}

		// Token: 0x06004029 RID: 16425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACA")]
		[Address(RVA = "0x1016CEA30", Offset = "0x16CEA30", VA = "0x1016CEA30", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x0600402A RID: 16426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACB")]
		[Address(RVA = "0x1016CEAF0", Offset = "0x16CEAF0", VA = "0x1016CEAF0")]
		private void ApplyPanelState(eCharacterDataType characterDataType)
		{
		}

		// Token: 0x0600402B RID: 16427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACC")]
		[Address(RVA = "0x1016CED14", Offset = "0x16CED14", VA = "0x1016CED14")]
		private void SetActive(bool flag)
		{
		}

		// Token: 0x0600402C RID: 16428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACD")]
		[Address(RVA = "0x1016CEE10", Offset = "0x16CEE10", VA = "0x1016CEE10")]
		private void FriendMode(bool flg)
		{
		}

		// Token: 0x0600402D RID: 16429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACE")]
		[Address(RVA = "0x1016CEF90", Offset = "0x16CEF90", VA = "0x1016CEF90")]
		public void ApplyGachaDetailDropListCharacter(GachaDetailDropListItemDataController gddlidc)
		{
		}

		// Token: 0x0600402E RID: 16430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ACF")]
		[Address(RVA = "0x1016CF0E0", Offset = "0x16CF0E0", VA = "0x1016CF0E0", Slot = "6")]
		protected virtual void ApplyProcess(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x0600402F RID: 16431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD0")]
		[Address(RVA = "0x1016CF1E8", Offset = "0x16CF1E8", VA = "0x1016CF1E8", Slot = "7")]
		protected virtual void ApplyExistCharacter(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x06004030 RID: 16432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD1")]
		[Address(RVA = "0x1016CF1EC", Offset = "0x16CF1EC", VA = "0x1016CF1EC", Slot = "8")]
		protected virtual void ApplyEmpty()
		{
		}

		// Token: 0x06004031 RID: 16433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD2")]
		[Address(RVA = "0x1016CF1F0", Offset = "0x16CF1F0", VA = "0x1016CF1F0", Slot = "9")]
		protected virtual void ApplyLock()
		{
		}

		// Token: 0x06004032 RID: 16434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD3")]
		[Address(RVA = "0x1016CF1F4", Offset = "0x16CF1F4", VA = "0x1016CF1F4")]
		private void SetWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
		}

		// Token: 0x06004033 RID: 16435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD4")]
		[Address(RVA = "0x1016CF23C", Offset = "0x16CF23C", VA = "0x1016CF23C")]
		protected EnumerationCharaPanelBaseExGen()
		{
		}

		// Token: 0x04004F91 RID: 20369
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003811")]
		protected SetupArgumentType m_SharedIntance;
	}
}
