﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D9C RID: 3484
	[Token(Token = "0x2000955")]
	[StructLayout(3)]
	public class EnumerationCharaPanelConst : EnumerationCharaPanelStandardBase
	{
		// Token: 0x0600403B RID: 16443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ADA")]
		[Address(RVA = "0x101473FE0", Offset = "0x1473FE0", VA = "0x101473FE0", Slot = "4")]
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
		}

		// Token: 0x0600403C RID: 16444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ADB")]
		[Address(RVA = "0x101473E38", Offset = "0x1473E38", VA = "0x101473E38")]
		public void Apply()
		{
		}

		// Token: 0x0600403D RID: 16445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ADC")]
		[Address(RVA = "0x101474040", Offset = "0x1474040", VA = "0x101474040")]
		public EnumerationCharaPanelConst()
		{
		}
	}
}
