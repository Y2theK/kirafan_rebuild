﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D9D RID: 3485
	[Token(Token = "0x2000956")]
	[StructLayout(3)]
	public class EnumerationCharaPanelScrollItemIcon : EnumerationCharaPanelBaseExGen<EnumerationCharaPanelScrollItemIconAdapterBase, EnumerationCharaPanelScrollItemIcon.SharedInstanceEx>
	{
		// Token: 0x0600403E RID: 16446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ADD")]
		[Address(RVA = "0x101474044", Offset = "0x1474044", VA = "0x101474044")]
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x0600403F RID: 16447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ADE")]
		[Address(RVA = "0x101474050", Offset = "0x1474050", VA = "0x101474050")]
		public EnumerationCharaPanelScrollItemIcon()
		{
		}

		// Token: 0x02000D9E RID: 3486
		[Token(Token = "0x200110D")]
		[Serializable]
		public class SharedInstanceEx : EnumerationCharaPanelBase.SharedInstanceExGen<EnumerationCharaPanelScrollItemIconAdapterBase>
		{
			// Token: 0x06004040 RID: 16448 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006214")]
			[Address(RVA = "0x1014740A0", Offset = "0x14740A0", VA = "0x1014740A0")]
			public SharedInstanceEx()
			{
			}

			// Token: 0x06004041 RID: 16449 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006215")]
			[Address(RVA = "0x1014740F0", Offset = "0x14740F0", VA = "0x1014740F0")]
			public SharedInstanceEx(EnumerationCharaPanelBase.SharedInstance argument)
			{
			}
		}
	}
}
