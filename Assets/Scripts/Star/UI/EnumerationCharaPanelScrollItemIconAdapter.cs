﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D92 RID: 3474
	[Token(Token = "0x2000950")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelScrollItemIconAdapter : EnumerationCharaPanelScrollItemIconAdapterBase
	{
		// Token: 0x0600401D RID: 16413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC2")]
		[Address(RVA = "0x101474150", Offset = "0x1474150", VA = "0x101474150")]
		protected EnumerationCharaPanelScrollItemIconAdapter()
		{
		}
	}
}
