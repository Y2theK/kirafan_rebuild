﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using Star.UI.GachaSelect;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D91 RID: 3473
	[Token(Token = "0x200094F")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelScrollItemIconAdapterBase : ScrollItemIcon
	{
		// Token: 0x06004015 RID: 16405 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003ABA")]
		[Address(RVA = "0x101474160", Offset = "0x1474160", VA = "0x101474160", Slot = "8")]
		public virtual EnumerationCharaPanelScrollItemIcon CreateCore()
		{
			return null;
		}

		// Token: 0x06004016 RID: 16406 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003ABB")]
		[Address(RVA = "0x1014741B8", Offset = "0x14741B8", VA = "0x1014741B8", Slot = "9")]
		public virtual EnumerationCharaPanelScrollItemIcon.SharedInstanceEx CreateArgument()
		{
			return null;
		}

		// Token: 0x06004017 RID: 16407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ABC")]
		[Address(RVA = "0x1014743A0", Offset = "0x14743A0", VA = "0x1014743A0", Slot = "10")]
		public virtual void Setup(EnumerationPanelScrollItemIconBase parent)
		{
		}

		// Token: 0x06004018 RID: 16408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ABD")]
		[Address(RVA = "0x101474408", Offset = "0x1474408", VA = "0x101474408", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004019 RID: 16409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ABE")]
		[Address(RVA = "0x101474420", Offset = "0x1474420", VA = "0x101474420", Slot = "11")]
		public virtual void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x0600401A RID: 16410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003ABF")]
		[Address(RVA = "0x101474438", Offset = "0x1474438", VA = "0x101474438", Slot = "12")]
		public virtual void ApplyGachaDetailDropListCharacter(GachaDetailDropListItemDataController gddlidc)
		{
		}

		// Token: 0x0600401B RID: 16411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC0")]
		[Address(RVA = "0x1014744AC", Offset = "0x14744AC", VA = "0x1014744AC")]
		public void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
		}

		// Token: 0x0600401C RID: 16412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AC1")]
		[Address(RVA = "0x101474158", Offset = "0x1474158", VA = "0x101474158")]
		protected EnumerationCharaPanelScrollItemIconAdapterBase()
		{
		}

		// Token: 0x04004F77 RID: 20343
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003807")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001262E4", Offset = "0x1262E4")]
		private GameObject m_DataContent;

		// Token: 0x04004F78 RID: 20344
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003808")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126330", Offset = "0x126330")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

		// Token: 0x04004F79 RID: 20345
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003809")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012637C", Offset = "0x12637C")]
		private GameObject m_Blank;

		// Token: 0x04004F7A RID: 20346
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400380A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001263C8", Offset = "0x1263C8")]
		private GameObject m_EmptyObj;

		// Token: 0x04004F7B RID: 20347
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400380B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126414", Offset = "0x126414")]
		private GameObject m_FriendObj;

		// Token: 0x04004F7C RID: 20348
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400380C")]
		[SerializeField]
		private EnumerationCharaPanelBase.SharedInstance m_SerializedConstructInstances;

		// Token: 0x04004F7D RID: 20349
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400380D")]
		protected EnumerationPanelScrollItemIconBase m_Parent;

		// Token: 0x04004F7E RID: 20350
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400380E")]
		private EnumerationCharaPanelScrollItemIcon m_Core;

		// Token: 0x04004F7F RID: 20351
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400380F")]
		protected EnumerationCharaPanelScrollItemIcon.SharedInstanceEx m_SharedInstance;
	}
}
