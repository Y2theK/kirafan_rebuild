﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D99 RID: 3481
	[Token(Token = "0x2000953")]
	[StructLayout(3)]
	public abstract class EnumerationCharaPanelStandardBase : EnumerationCharaPanelBaseExGen<EnumerationCharaPanelAdapterBase, EnumerationCharaPanelStandardBase.SharedInstanceEx>
	{
		// Token: 0x06004034 RID: 16436 RVA: 0x00018F78 File Offset: 0x00017178
		[Token(Token = "0x6003AD5")]
		[Address(RVA = "0x101473B10", Offset = "0x1473B10", VA = "0x101473B10")]
		public int GetSlotIndex()
		{
			return 0;
		}

		// Token: 0x06004035 RID: 16437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AD6")]
		[Address(RVA = "0x10147380C", Offset = "0x147380C", VA = "0x10147380C")]
		protected EnumerationCharaPanelStandardBase()
		{
		}

		// Token: 0x02000D9A RID: 3482
		[Token(Token = "0x200110C")]
		[Serializable]
		public class SharedInstanceEx : EnumerationCharaPanelBase.SharedInstanceExGen<EnumerationCharaPanelAdapterBase>
		{
			// Token: 0x06004036 RID: 16438 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006212")]
			[Address(RVA = "0x1014744BC", Offset = "0x14744BC", VA = "0x1014744BC")]
			public SharedInstanceEx()
			{
			}

			// Token: 0x06004037 RID: 16439 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006213")]
			[Address(RVA = "0x101473A8C", Offset = "0x1473A8C", VA = "0x101473A8C")]
			public SharedInstanceEx(EnumerationCharaPanelBase.SharedInstance argument)
			{
			}

			// Token: 0x04004F92 RID: 20370
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006AC9")]
			public EquipWeaponPartyMemberController partyMemberController;
		}
	}
}
