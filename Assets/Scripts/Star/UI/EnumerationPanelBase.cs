﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DAD RID: 3501
	[Token(Token = "0x2000960")]
	[StructLayout(3)]
	public abstract class EnumerationPanelBase : InfiniteScrollItem
	{
		// Token: 0x06004086 RID: 16518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B14")]
		[Address(RVA = "0x10147450C", Offset = "0x147450C", VA = "0x10147450C")]
		protected EnumerationPanelBase()
		{
		}
	}
}
