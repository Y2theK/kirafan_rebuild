﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DAE RID: 3502
	[Token(Token = "0x2000961")]
	[StructLayout(3)]
	public abstract class EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelBase where ThisType : EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelAdapterBase where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
		// Token: 0x06004087 RID: 16519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B15")]
		[Address(RVA = "0x1016CF268", Offset = "0x16CF268", VA = "0x1016CF268")]
		protected EnumerationPanelBaseExGen()
		{
		}

		// Token: 0x06004088 RID: 16520
		[Token(Token = "0x6003B16")]
		[Address(Slot = "7")]
		public abstract CoreType CreateCore();

		// Token: 0x06004089 RID: 16521 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B17")]
		[Address(RVA = "0x1016CF29C", Offset = "0x16CF29C", VA = "0x1016CF29C", Slot = "8")]
		public virtual SetupArgumentType CreateArgument()
		{
			return null;
		}

		// Token: 0x0600408A RID: 16522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B18")]
		[Address(RVA = "0x1016CF46C", Offset = "0x16CF46C", VA = "0x1016CF46C", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x0600408B RID: 16523 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B19")]
		[Address(RVA = "0x1016CF50C", Offset = "0x16CF50C", VA = "0x1016CF50C", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x0600408C RID: 16524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B1A")]
		[Address(RVA = "0x1016CF544", Offset = "0x16CF544", VA = "0x1016CF544")]
		private void Update()
		{
		}

		// Token: 0x0600408D RID: 16525 RVA: 0x00019050 File Offset: 0x00017250
		[Token(Token = "0x6003B1B")]
		[Address(RVA = "0x1016CF55C", Offset = "0x16CF55C", VA = "0x1016CF55C")]
		public int GetPartyIndex()
		{
			return 0;
		}

		// Token: 0x0600408E RID: 16526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B1C")]
		[Address(RVA = "0x1016CF564", Offset = "0x16CF564", VA = "0x1016CF564")]
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
		}

		// Token: 0x0600408F RID: 16527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B1D")]
		[Address(RVA = "0x1016CF584", Offset = "0x16CF584", VA = "0x1016CF584", Slot = "5")]
		public override void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x04004FAC RID: 20396
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003822")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126518", Offset = "0x126518")]
		protected CharaPanelType m_CharaPanelPrefab;

		// Token: 0x04004FAD RID: 20397
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003823")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126564", Offset = "0x126564")]
		protected RectTransform m_CharaPanelParent;

		// Token: 0x04004FAE RID: 20398
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003824")]
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x04004FAF RID: 20399
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003825")]
		protected BasedDataType m_PartyData;

		// Token: 0x04004FB0 RID: 20400
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003826")]
		protected int m_CharaPanelNum;

		// Token: 0x04004FB1 RID: 20401
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003827")]
		[SerializeField]
		protected SetupArgumentType m_SerializedConstructInstances;

		// Token: 0x04004FB2 RID: 20402
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003828")]
		protected CoreType m_Core;

		// Token: 0x04004FB3 RID: 20403
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003829")]
		protected SetupArgumentType m_SharedInstance;
	}
}
