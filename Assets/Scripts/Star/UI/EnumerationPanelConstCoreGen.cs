﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DA7 RID: 3495
	[Token(Token = "0x200095B")]
	[StructLayout(3)]
	public class EnumerationPanelConstCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterConstExGen<ParentType> where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x0600406D RID: 16493 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AFF")]
		[Address(RVA = "0x1016CF698", Offset = "0x16CF698", VA = "0x1016CF698", Slot = "7")]
		protected override CharaPanelType InstantiateProcess()
		{
			return null;
		}

		// Token: 0x0600406E RID: 16494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B00")]
		[Address(RVA = "0x1016CF850", Offset = "0x16CF850", VA = "0x1016CF850", Slot = "10")]
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
		}

		// Token: 0x0600406F RID: 16495 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B01")]
		[Address(RVA = "0x1016CF960", Offset = "0x16CF960", VA = "0x1016CF960", Slot = "12")]
		public override SetupArgumentType GetSharedIntance()
		{
			return null;
		}

		// Token: 0x06004070 RID: 16496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B02")]
		[Address(RVA = "0x1016CF968", Offset = "0x16CF968", VA = "0x1016CF968", Slot = "13")]
		public override void SetSharedIntance(SetupArgumentType arg)
		{
		}

		// Token: 0x06004071 RID: 16497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B03")]
		[Address(RVA = "0x1016CF970", Offset = "0x16CF970", VA = "0x1016CF970")]
		public EnumerationPanelConstCoreGen()
		{
		}

		// Token: 0x04004FA1 RID: 20385
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003817")]
		protected SetupArgumentType m_SharedIntance;
	}
}
