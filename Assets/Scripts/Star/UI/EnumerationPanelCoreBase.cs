﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D9F RID: 3487
	[Token(Token = "0x2000957")]
	[StructLayout(3)]
	public abstract class EnumerationPanelCoreBase
	{
		// Token: 0x06004042 RID: 16450 RVA: 0x00018F90 File Offset: 0x00017190
		[Token(Token = "0x6003ADF")]
		[Address(RVA = "0x101474514", Offset = "0x1474514", VA = "0x101474514")]
		public static bool ModeToTouchActive(EnumerationPanelCoreBase.eMode mode)
		{
			return default(bool);
		}

		// Token: 0x06004043 RID: 16451 RVA: 0x00018FA8 File Offset: 0x000171A8
		[Token(Token = "0x6003AE0")]
		[Address(RVA = "0x10147451C", Offset = "0x147451C", VA = "0x10147451C")]
		public static EnumerationCharaPanelBase.eMode ConvertEnumerationPanelCoreBaseeModeToEnumerationCharaPanelBaseeMode(EnumerationPanelCoreBase.eMode mode)
		{
			return EnumerationCharaPanelBase.eMode.Edit;
		}

		// Token: 0x06004044 RID: 16452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AE1")]
		[Address(RVA = "0x10147452C", Offset = "0x147452C", VA = "0x10147452C")]
		protected EnumerationPanelCoreBase()
		{
		}

		// Token: 0x02000DA0 RID: 3488
		[Token(Token = "0x200110E")]
		public enum eMode
		{
			// Token: 0x04004F94 RID: 20372
			[Token(Token = "0x4006ACB")]
			Edit,
			// Token: 0x04004F95 RID: 20373
			[Token(Token = "0x4006ACC")]
			Quest,
			// Token: 0x04004F96 RID: 20374
			[Token(Token = "0x4006ACD")]
			QuestStart,
			// Token: 0x04004F97 RID: 20375
			[Token(Token = "0x4006ACE")]
			View,
			// Token: 0x04004F98 RID: 20376
			[Token(Token = "0x4006ACF")]
			PartyDetail
		}

		// Token: 0x02000DA1 RID: 3489
		[Token(Token = "0x200110F")]
		[Serializable]
		public class SharedInstanceBase
		{
			// Token: 0x06004045 RID: 16453 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006216")]
			[Address(RVA = "0x101474534", Offset = "0x1474534", VA = "0x101474534")]
			public SharedInstanceBase()
			{
			}

			// Token: 0x06004046 RID: 16454 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006217")]
			[Address(RVA = "0x10147453C", Offset = "0x147453C", VA = "0x10147453C")]
			public SharedInstanceBase(EnumerationPanelCoreBase.SharedInstanceBase argument)
			{
			}

			// Token: 0x06004047 RID: 16455 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006218")]
			[Address(RVA = "0x101474578", Offset = "0x1474578", VA = "0x101474578")]
			protected EnumerationPanelCoreBase.SharedInstanceBase CloneNewMemberOnlyBase(EnumerationPanelCoreBase.SharedInstanceBase argument)
			{
				return null;
			}

			// Token: 0x04004F99 RID: 20377
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006AD0")]
			[SerializeField]
			[Attribute(Name = "TooltipAttribute", RVA = "0x100134AD4", Offset = "0x134AD4")]
			public RectTransform m_CharaPanelParent;
		}

		// Token: 0x02000DA2 RID: 3490
		[Token(Token = "0x2001110")]
		[Serializable]
		public abstract class SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> : EnumerationPanelCoreBase.SharedInstanceBase where ThisType : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ParentType : MonoBehaviour where CharaPanelType : MonoBehaviour
		{
			// Token: 0x06004048 RID: 16456 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006219")]
			[Address(RVA = "0x1016CF9AC", Offset = "0x16CF9AC", VA = "0x1016CF9AC")]
			public SharedInstanceBaseExGen()
			{
			}

			// Token: 0x06004049 RID: 16457 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600621A")]
			[Address(RVA = "0x1016CF9D8", Offset = "0x16CF9D8", VA = "0x1016CF9D8")]
			public SharedInstanceBaseExGen(ThisType argument)
			{
			}

			// Token: 0x0600404A RID: 16458 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600621B")]
			[Address(RVA = "0x1016CFA50", Offset = "0x16CFA50", VA = "0x1016CFA50", Slot = "4")]
			public virtual ThisType Clone(ThisType argument)
			{
				return null;
			}

			// Token: 0x0600404B RID: 16459 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600621C")]
			[Address(RVA = "0x1016CFAC8", Offset = "0x16CFAC8", VA = "0x1016CFAC8")]
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				return null;
			}

			// Token: 0x04004F9A RID: 20378
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006AD1")]
			public ParentType parent;

			// Token: 0x04004F9B RID: 20379
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006AD2")]
			[SerializeField]
			[Attribute(Name = "TooltipAttribute", RVA = "0x100134B20", Offset = "0x134B20")]
			public CharaPanelType m_CharaPanelPrefab;
		}
	}
}
