﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DA3 RID: 3491
	[Token(Token = "0x2000958")]
	[StructLayout(3)]
	public abstract class EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBase where ParentType : MonoBehaviour where CharaPanelType : MonoBehaviour where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreBase.SharedInstanceBaseExGen<SetupArgumentType, ParentType, CharaPanelType>
	{
		// Token: 0x0600404C RID: 16460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AE2")]
		[Address(RVA = "0x1016CFBA8", Offset = "0x16CFBA8", VA = "0x1016CFBA8", Slot = "4")]
		public virtual void Setup(SetupArgumentType argument)
		{
		}

		// Token: 0x0600404D RID: 16461
		[Token(Token = "0x6003AE3")]
		[Address(Slot = "5")]
		public abstract void Destroy();

		// Token: 0x0600404E RID: 16462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AE4")]
		[Address(RVA = "0x1016CFBE0", Offset = "0x16CFBE0", VA = "0x1016CFBE0", Slot = "6")]
		public virtual void Update()
		{
		}

		// Token: 0x0600404F RID: 16463
		[Token(Token = "0x6003AE5")]
		[Address(Slot = "7")]
		protected abstract CharaPanelType InstantiateProcess();

		// Token: 0x06004050 RID: 16464 RVA: 0x00018FC0 File Offset: 0x000171C0
		[Token(Token = "0x6003AE6")]
		[Address(RVA = "0x1016CFBE4", Offset = "0x16CFBE4", VA = "0x1016CFBE4")]
		protected int HowManyAllChildren()
		{
			return 0;
		}

		// Token: 0x06004051 RID: 16465 RVA: 0x00018FD8 File Offset: 0x000171D8
		[Token(Token = "0x6003AE7")]
		[Address(RVA = "0x1016CFC14", Offset = "0x16CFC14", VA = "0x1016CFC14")]
		protected int HowManyEnableChildren()
		{
			return 0;
		}

		// Token: 0x06004052 RID: 16466 RVA: 0x00018FF0 File Offset: 0x000171F0
		[Token(Token = "0x6003AE8")]
		[Address(RVA = "0x1016CFC60", Offset = "0x16CFC60", VA = "0x1016CFC60")]
		public int GetNowSelectedCharaSlotIndex()
		{
			return 0;
		}

		// Token: 0x06004053 RID: 16467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AE9")]
		[Address(RVA = "0x1016CFC8C", Offset = "0x16CFC8C", VA = "0x1016CFC8C")]
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
		}

		// Token: 0x06004054 RID: 16468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AEA")]
		[Address(RVA = "0x1016CFD48", Offset = "0x16CFD48", VA = "0x1016CFD48")]
		public void SetNowSelectedCharaSlotIndex(int index)
		{
		}

		// Token: 0x06004055 RID: 16469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AEB")]
		[Address(RVA = "0x1016CFD78", Offset = "0x16CFD78", VA = "0x1016CFD78", Slot = "8")]
		public virtual void Apply(EnumerationPanelData data)
		{
		}

		// Token: 0x06004056 RID: 16470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AEC")]
		[Address(RVA = "0x1016CFE60", Offset = "0x16CFE60", VA = "0x1016CFE60", Slot = "9")]
		protected virtual void ApplyCharaData(int idx)
		{
		}

		// Token: 0x06004057 RID: 16471
		[Token(Token = "0x6003AED")]
		[Address(Slot = "10")]
		protected abstract void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx);

		// Token: 0x06004058 RID: 16472 RVA: 0x00019008 File Offset: 0x00017208
		[Token(Token = "0x6003AEE")]
		[Address(RVA = "0x1016D0010", Offset = "0x16D0010", VA = "0x1016D0010")]
		protected int GetInstantiatePerFrame()
		{
			return 0;
		}

		// Token: 0x06004059 RID: 16473 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AEF")]
		[Address(RVA = "0x1016D0018", Offset = "0x16D0018", VA = "0x1016D0018")]
		protected RectTransform GetCharaPanelParent()
		{
			return null;
		}

		// Token: 0x0600405A RID: 16474 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AF0")]
		[Address(RVA = "0x1016D005C", Offset = "0x16D005C", VA = "0x1016D005C")]
		protected BasedDataType GetPartyData()
		{
			return null;
		}

		// Token: 0x0600405B RID: 16475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AF1")]
		[Address(RVA = "0x1016D0064", Offset = "0x16D0064", VA = "0x1016D0064", Slot = "11")]
		protected virtual void SetPartyData(BasedDataType data)
		{
		}

		// Token: 0x0600405C RID: 16476 RVA: 0x00019020 File Offset: 0x00017220
		[Token(Token = "0x6003AF2")]
		[Address(RVA = "0x1016D0104", Offset = "0x16D0104", VA = "0x1016D0104")]
		protected bool AcquireChildrenNumsInfo()
		{
			return default(bool);
		}

		// Token: 0x0600405D RID: 16477 RVA: 0x00019038 File Offset: 0x00017238
		[Token(Token = "0x6003AF3")]
		[Address(RVA = "0x1016D0114", Offset = "0x16D0114", VA = "0x1016D0114")]
		protected bool AcquirePanelsArray()
		{
			return default(bool);
		}

		// Token: 0x0600405E RID: 16478
		[Token(Token = "0x6003AF4")]
		[Address(Slot = "12")]
		public abstract SetupArgumentType GetSharedIntance();

		// Token: 0x0600405F RID: 16479
		[Token(Token = "0x6003AF5")]
		[Address(Slot = "13")]
		public abstract void SetSharedIntance(SetupArgumentType arg);

		// Token: 0x06004060 RID: 16480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AF6")]
		[Address(RVA = "0x1016D01B4", Offset = "0x16D01B4", VA = "0x1016D01B4")]
		protected EnumerationPanelCoreBaseExGen()
		{
		}

		// Token: 0x04004F9C RID: 20380
		[Token(Token = "0x4003812")]
		private const int INSTANTIATE_PER_FRAME = 6;

		// Token: 0x04004F9D RID: 20381
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003813")]
		protected EnumerationPanelCoreBase.eMode m_Mode;

		// Token: 0x04004F9E RID: 20382
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003814")]
		protected BasedDataType m_PartyData;

		// Token: 0x04004F9F RID: 20383
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003815")]
		protected CharaPanelType[] m_CharaPanels;

		// Token: 0x04004FA0 RID: 20384
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003816")]
		protected int m_CharaPanelNum;
	}
}
