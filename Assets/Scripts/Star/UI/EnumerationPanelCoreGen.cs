﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DA6 RID: 3494
	[Token(Token = "0x200095A")]
	[StructLayout(3)]
	public abstract class EnumerationPanelCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterExGen<ParentType> where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x0600406A RID: 16490 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AFC")]
		[Address(RVA = "0x1016D0678", Offset = "0x16D0678", VA = "0x1016D0678", Slot = "7")]
		protected override CharaPanelType InstantiateProcess()
		{
			return null;
		}

		// Token: 0x0600406B RID: 16491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AFD")]
		[Address(RVA = "0x1016D08F0", Offset = "0x16D08F0", VA = "0x1016D08F0", Slot = "10")]
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
		}

		// Token: 0x0600406C RID: 16492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AFE")]
		[Address(RVA = "0x1016D0998", Offset = "0x16D0998", VA = "0x1016D0998")]
		protected EnumerationPanelCoreGen()
		{
		}
	}
}
