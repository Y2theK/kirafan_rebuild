﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DA4 RID: 3492
	[Token(Token = "0x2000959")]
	[StructLayout(3)]
	public abstract class EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterBase where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x06004061 RID: 16481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AF7")]
		[Address(RVA = "0x1016D03C0", Offset = "0x16D03C0", VA = "0x1016D03C0", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06004062 RID: 16482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AF8")]
		[Address(RVA = "0x1016D0468", Offset = "0x16D0468", VA = "0x1016D0468", Slot = "6")]
		public override void Update()
		{
		}

		// Token: 0x06004063 RID: 16483 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003AF9")]
		[Address(RVA = "0x1016D0630", Offset = "0x16D0630", VA = "0x1016D0630", Slot = "7")]
		protected override CharaPanelType InstantiateProcess()
		{
			return null;
		}

		// Token: 0x06004064 RID: 16484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AFA")]
		[Address(RVA = "0x1016D0638", Offset = "0x16D0638", VA = "0x1016D0638", Slot = "10")]
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
		}

		// Token: 0x06004065 RID: 16485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003AFB")]
		[Address(RVA = "0x1016D063C", Offset = "0x16D063C", VA = "0x1016D063C")]
		protected EnumerationPanelCoreGenBase()
		{
		}

		// Token: 0x02000DA5 RID: 3493
		[Token(Token = "0x2001111")]
		[Serializable]
		public class SharedInstanceEx<ThisType> : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ThisType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<ThisType>
		{
			// Token: 0x06004066 RID: 16486 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600621D")]
			[Address(RVA = "0x1016D01E0", Offset = "0x16D01E0", VA = "0x1016D01E0")]
			public SharedInstanceEx()
			{
			}

			// Token: 0x06004067 RID: 16487 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600621E")]
			[Address(RVA = "0x1016D021C", Offset = "0x16D021C", VA = "0x1016D021C")]
			public SharedInstanceEx(ThisType argument)
			{
			}

			// Token: 0x06004068 RID: 16488 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600621F")]
			[Address(RVA = "0x1016D0290", Offset = "0x16D0290", VA = "0x1016D0290", Slot = "4")]
			public override ThisType Clone(ThisType argument)
			{
				return null;
			}

			// Token: 0x06004069 RID: 16489 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006220")]
			[Address(RVA = "0x1016D0304", Offset = "0x16D0304", VA = "0x1016D0304")]
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				return null;
			}
		}
	}
}
