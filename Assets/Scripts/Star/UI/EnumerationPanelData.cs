﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaSelect;

namespace Star.UI
{
	// Token: 0x02000DAF RID: 3503
	[Token(Token = "0x2000962")]
	[StructLayout(3)]
	public class EnumerationPanelData : InfiniteScrollItemData
	{
		// Token: 0x06004090 RID: 16528 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B1E")]
		[Address(RVA = "0x1014745B0", Offset = "0x14745B0", VA = "0x1014745B0")]
		public EquipWeaponPartyMemberController GetSelectedMember()
		{
			return null;
		}

		// Token: 0x06004091 RID: 16529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B1F")]
		[Address(RVA = "0x1014745E8", Offset = "0x14745E8", VA = "0x1014745E8")]
		public EnumerationPanelData()
		{
		}

		// Token: 0x04004FB4 RID: 20404
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400382A")]
		public EquipWeaponPartyController m_Party;

		// Token: 0x04004FB5 RID: 20405
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400382B")]
		public GachaDetailDropListItemDataControllers m_GachaDetailDropListItemDataControllers;

		// Token: 0x04004FB6 RID: 20406
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400382C")]
		public int m_NowSelectedCharaSlotIndex;
	}
}
