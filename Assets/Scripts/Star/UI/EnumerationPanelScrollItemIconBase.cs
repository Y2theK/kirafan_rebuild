﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DAA RID: 3498
	[Token(Token = "0x200095D")]
	[StructLayout(3)]
	public abstract class EnumerationPanelScrollItemIconBase : MonoBehaviour
	{
		// Token: 0x0600407C RID: 16508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B0A")]
		[Address(RVA = "0x1014745F0", Offset = "0x14745F0", VA = "0x1014745F0")]
		protected EnumerationPanelScrollItemIconBase()
		{
		}
	}
}
