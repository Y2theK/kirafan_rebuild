﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DAB RID: 3499
	[Token(Token = "0x200095E")]
	[StructLayout(3)]
	public abstract class EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelScrollItemIconBase where ThisType : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapterBase where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
		// Token: 0x0600407D RID: 16509
		[Token(Token = "0x6003B0B")]
		[Address(Slot = "4")]
		public abstract CoreType CreateCore();

		// Token: 0x0600407E RID: 16510 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B0C")]
		[Address(RVA = "0x1016D09D4", Offset = "0x16D09D4", VA = "0x1016D09D4", Slot = "5")]
		public virtual SetupArgumentType CreateArgument()
		{
			return null;
		}

		// Token: 0x0600407F RID: 16511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B0D")]
		[Address(RVA = "0x1016D0BA4", Offset = "0x16D0BA4", VA = "0x1016D0BA4", Slot = "6")]
		public virtual void Setup()
		{
		}

		// Token: 0x06004080 RID: 16512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B0E")]
		[Address(RVA = "0x1016D0C20", Offset = "0x16D0C20", VA = "0x1016D0C20", Slot = "7")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06004081 RID: 16513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B0F")]
		[Address(RVA = "0x1016D0C38", Offset = "0x16D0C38", VA = "0x1016D0C38")]
		private void Update()
		{
		}

		// Token: 0x06004082 RID: 16514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B10")]
		[Address(RVA = "0x1016D0C50", Offset = "0x16D0C50", VA = "0x1016D0C50")]
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
		}

		// Token: 0x06004083 RID: 16515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B11")]
		[Address(RVA = "0x1016D0C70", Offset = "0x16D0C70", VA = "0x1016D0C70", Slot = "8")]
		public virtual void Apply(EnumerationPanelData data)
		{
		}

		// Token: 0x06004084 RID: 16516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B12")]
		[Address(RVA = "0x1016D0C88", Offset = "0x16D0C88", VA = "0x1016D0C88")]
		protected EnumerationPanelScrollItemIconBaseExGen()
		{
		}

		// Token: 0x04004FA3 RID: 20387
		[Token(Token = "0x4003819")]
		private const int INSTANTIATE_PER_FRAME = 6;

		// Token: 0x04004FA4 RID: 20388
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100126470", Offset = "0x126470")]
		protected CharaPanelType m_CharaPanelPrefab;

		// Token: 0x04004FA5 RID: 20389
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001264BC", Offset = "0x1264BC")]
		protected RectTransform m_CharaPanelParent;

		// Token: 0x04004FA6 RID: 20390
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381C")]
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x04004FA7 RID: 20391
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381D")]
		protected BasedDataType m_PartyData;

		// Token: 0x04004FA8 RID: 20392
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381E")]
		protected int m_CharaPanelNum;

		// Token: 0x04004FA9 RID: 20393
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x400381F")]
		[SerializeField]
		protected SetupArgumentType m_SerializedConstructInstances;

		// Token: 0x04004FAA RID: 20394
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003820")]
		private CoreType m_Core;

		// Token: 0x04004FAB RID: 20395
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003821")]
		protected SetupArgumentType m_SharedInstance;
	}
}
