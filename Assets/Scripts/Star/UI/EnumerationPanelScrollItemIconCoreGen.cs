﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DA8 RID: 3496
	[Token(Token = "0x200095C")]
	[StructLayout(3)]
	public class EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelScrollItemIconBase where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapterBase where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x06004072 RID: 16498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B04")]
		[Address(RVA = "0x1016D0EA8", Offset = "0x16D0EA8", VA = "0x1016D0EA8", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06004073 RID: 16499 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B05")]
		[Address(RVA = "0x1016D0F50", Offset = "0x16D0F50", VA = "0x1016D0F50", Slot = "7")]
		protected override CharaPanelType InstantiateProcess()
		{
			return null;
		}

		// Token: 0x06004074 RID: 16500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B06")]
		[Address(RVA = "0x1016D1108", Offset = "0x16D1108", VA = "0x1016D1108", Slot = "10")]
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
		}

		// Token: 0x06004075 RID: 16501 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B07")]
		[Address(RVA = "0x1016D1228", Offset = "0x16D1228", VA = "0x1016D1228", Slot = "12")]
		public override SetupArgumentType GetSharedIntance()
		{
			return null;
		}

		// Token: 0x06004076 RID: 16502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B08")]
		[Address(RVA = "0x1016D1230", Offset = "0x16D1230", VA = "0x1016D1230", Slot = "13")]
		public override void SetSharedIntance(SetupArgumentType arg)
		{
		}

		// Token: 0x06004077 RID: 16503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B09")]
		[Address(RVA = "0x1016D1238", Offset = "0x16D1238", VA = "0x1016D1238")]
		public EnumerationPanelScrollItemIconCoreGen()
		{
		}

		// Token: 0x04004FA2 RID: 20386
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4003818")]
		protected SetupArgumentType m_SharedIntance;

		// Token: 0x02000DA9 RID: 3497
		[Token(Token = "0x2001112")]
		[Serializable]
		public class SharedInstanceEx<ThisType> : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ThisType : EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<ThisType>
		{
			// Token: 0x06004078 RID: 16504 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006221")]
			[Address(RVA = "0x1016D0CB4", Offset = "0x16D0CB4", VA = "0x1016D0CB4")]
			public SharedInstanceEx()
			{
			}

			// Token: 0x06004079 RID: 16505 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006222")]
			[Address(RVA = "0x1016D0CF0", Offset = "0x16D0CF0", VA = "0x1016D0CF0")]
			public SharedInstanceEx(ThisType argument)
			{
			}

			// Token: 0x0600407A RID: 16506 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006223")]
			[Address(RVA = "0x1016D0D78", Offset = "0x16D0D78", VA = "0x1016D0D78", Slot = "4")]
			public override ThisType Clone(ThisType argument)
			{
				return null;
			}

			// Token: 0x0600407B RID: 16507 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006224")]
			[Address(RVA = "0x1016D0DEC", Offset = "0x16D0DEC", VA = "0x1016D0DEC")]
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				return null;
			}
		}
	}
}
