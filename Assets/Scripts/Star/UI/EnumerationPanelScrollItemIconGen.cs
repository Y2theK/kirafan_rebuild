﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DAC RID: 3500
	[Token(Token = "0x200095F")]
	[StructLayout(3)]
	public abstract class EnumerationPanelScrollItemIconGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where ThisType : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapter where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
		// Token: 0x06004085 RID: 16517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B13")]
		[Address(RVA = "0x1016D1274", Offset = "0x16D1274", VA = "0x1016D1274")]
		protected EnumerationPanelScrollItemIconGen()
		{
		}
	}
}
