﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DD9 RID: 3545
	[Token(Token = "0x2000985")]
	[StructLayout(3)]
	public class EventBannerLoader
	{
		// Token: 0x0600417F RID: 16767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C0C")]
		[Address(RVA = "0x1014745F8", Offset = "0x14745F8", VA = "0x1014745F8")]
		public void Request_EventBanner(Action onResponse)
		{
		}

		// Token: 0x06004180 RID: 16768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C0D")]
		[Address(RVA = "0x1014746AC", Offset = "0x14746AC", VA = "0x1014746AC")]
		private void OnResponse_EventBanner(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06004181 RID: 16769 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003C0E")]
		[Address(RVA = "0x101474B6C", Offset = "0x1474B6C", VA = "0x101474B6C")]
		public List<EventBannerLoader.EventBannerData> GetList()
		{
			return null;
		}

		// Token: 0x06004182 RID: 16770 RVA: 0x00019218 File Offset: 0x00017418
		[Token(Token = "0x6003C0F")]
		[Address(RVA = "0x101474B74", Offset = "0x1474B74", VA = "0x101474B74")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06004183 RID: 16771 RVA: 0x00019230 File Offset: 0x00017430
		[Token(Token = "0x6003C10")]
		[Address(RVA = "0x101474B7C", Offset = "0x1474B7C", VA = "0x101474B7C")]
		public bool IsSuccessPrepare()
		{
			return default(bool);
		}

		// Token: 0x06004184 RID: 16772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C11")]
		[Address(RVA = "0x101474BB0", Offset = "0x1474BB0", VA = "0x101474BB0")]
		public void Prepare()
		{
		}

		// Token: 0x06004185 RID: 16773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C12")]
		[Address(RVA = "0x101474C60", Offset = "0x1474C60", VA = "0x101474C60")]
		public void UpdatePrepare()
		{
		}

		// Token: 0x06004186 RID: 16774 RVA: 0x00019248 File Offset: 0x00017448
		[Token(Token = "0x6003C13")]
		[Address(RVA = "0x101474D2C", Offset = "0x1474D2C", VA = "0x101474D2C")]
		public bool GetIsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06004187 RID: 16775 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003C14")]
		[Address(RVA = "0x101474D34", Offset = "0x1474D34", VA = "0x101474D34")]
		public Sprite GetSprite(string imgID)
		{
			return null;
		}

		// Token: 0x06004188 RID: 16776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C15")]
		[Address(RVA = "0x101474F04", Offset = "0x1474F04", VA = "0x101474F04")]
		public void Destroy()
		{
		}

		// Token: 0x06004189 RID: 16777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C16")]
		[Address(RVA = "0x101474F40", Offset = "0x1474F40", VA = "0x101474F40")]
		public EventBannerLoader()
		{
		}

		// Token: 0x04005117 RID: 20759
		[Token(Token = "0x4003960")]
		public const string BANNER_PATH = "texture/eventbanner.muast";

		// Token: 0x04005118 RID: 20760
		[Token(Token = "0x4003961")]
		public const string BANNER_NAME_BASE = "";

		// Token: 0x04005119 RID: 20761
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003962")]
		private List<EventBannerLoader.EventBannerData> m_EventBannerData;

		// Token: 0x0400511A RID: 20762
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003963")]
		private ABResourceLoader m_Loader;

		// Token: 0x0400511B RID: 20763
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003964")]
		private ABResourceObjectHandler m_Hndl;

		// Token: 0x0400511C RID: 20764
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003965")]
		private bool m_IsDonePrepare;

		// Token: 0x0400511D RID: 20765
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003966")]
		private Action m_OnResponse;

		// Token: 0x02000DDA RID: 3546
		[Token(Token = "0x200111A")]
		public class EventBannerData
		{
			// Token: 0x0600418A RID: 16778 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006226")]
			[Address(RVA = "0x101474AF0", Offset = "0x1474AF0", VA = "0x101474AF0")]
			public EventBannerData(int id, string category, string imgID, float offsetX, float offsetY, DateTime startAt, DateTime endAt, DateTime dispEndAt, int[] pickupCharacterIds)
			{
			}

			// Token: 0x0400511E RID: 20766
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B00")]
			public int m_ID;

			// Token: 0x0400511F RID: 20767
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006B01")]
			public float m_OffsetX;

			// Token: 0x04005120 RID: 20768
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B02")]
			public float m_OffsetY;

			// Token: 0x04005121 RID: 20769
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006B03")]
			public string m_Category;

			// Token: 0x04005122 RID: 20770
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006B04")]
			public string m_ImgID;

			// Token: 0x04005123 RID: 20771
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006B05")]
			public DateTime m_StartAt;

			// Token: 0x04005124 RID: 20772
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006B06")]
			public DateTime m_EndAt;

			// Token: 0x04005125 RID: 20773
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006B07")]
			public DateTime m_DispEndAt;

			// Token: 0x04005126 RID: 20774
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006B08")]
			public int[] m_PickupCharacterIds;
		}
	}
}
