﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CB9 RID: 3257
	[Token(Token = "0x20008BA")]
	[StructLayout(3)]
	public class ExpGauge : MonoBehaviour
	{
		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x06003BC4 RID: 15300 RVA: 0x00018600 File Offset: 0x00016800
		[Token(Token = "0x17000442")]
		public bool IsPlaying
		{
			[Token(Token = "0x6003686")]
			[Address(RVA = "0x101474FB0", Offset = "0x1474FB0", VA = "0x101474FB0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06003BC5 RID: 15301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003687")]
		[Address(RVA = "0x101474FB8", Offset = "0x1474FB8", VA = "0x101474FB8")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003BC6 RID: 15302 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003688")]
		[Address(RVA = "0x101475084", Offset = "0x1475084", VA = "0x101475084")]
		public LevelUpPop[] GetLevelUpPops()
		{
			return null;
		}

		// Token: 0x06003BC7 RID: 15303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003689")]
		[Address(RVA = "0x10147508C", Offset = "0x147508C", VA = "0x10147508C")]
		public void Setup()
		{
		}

		// Token: 0x06003BC8 RID: 15304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368A")]
		[Address(RVA = "0x101475344", Offset = "0x1475344", VA = "0x101475344")]
		private void Update()
		{
		}

		// Token: 0x06003BC9 RID: 15305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368B")]
		[Address(RVA = "0x1014762CC", Offset = "0x14762CC", VA = "0x1014762CC")]
		public void Play(bool playSE = true)
		{
		}

		// Token: 0x06003BCA RID: 15306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368C")]
		[Address(RVA = "0x101476308", Offset = "0x1476308", VA = "0x101476308")]
		private void PlaySE()
		{
		}

		// Token: 0x06003BCB RID: 15307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368D")]
		[Address(RVA = "0x101474FDC", Offset = "0x1474FDC", VA = "0x101474FDC")]
		private void StopSE()
		{
		}

		// Token: 0x06003BCC RID: 15308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368E")]
		[Address(RVA = "0x1014763D0", Offset = "0x14763D0", VA = "0x1014763D0")]
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
		}

		// Token: 0x06003BCD RID: 15309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600368F")]
		[Address(RVA = "0x101476494", Offset = "0x1476494", VA = "0x101476494")]
		public void SetRewardExp(long rewardExp)
		{
		}

		// Token: 0x06003BCE RID: 15310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003690")]
		[Address(RVA = "0x10147649C", Offset = "0x147649C", VA = "0x10147649C")]
		public void SetExpData(long rewardExp, int beforeLevel, long beforeNowExp, int afterLevel, long afterNowExp, int maxLevel, long[] needExp)
		{
		}

		// Token: 0x06003BCF RID: 15311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003691")]
		[Address(RVA = "0x1014765CC", Offset = "0x14765CC", VA = "0x1014765CC")]
		public void SetSimulateGauge(long nowExp, long needExp, int addLv)
		{
		}

		// Token: 0x06003BD0 RID: 15312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003692")]
		[Address(RVA = "0x101476878", Offset = "0x1476878", VA = "0x101476878")]
		public void SetSimulateOff()
		{
		}

		// Token: 0x06003BD1 RID: 15313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003693")]
		[Address(RVA = "0x101476884", Offset = "0x1476884", VA = "0x101476884")]
		public void SkipSimulate()
		{
		}

		// Token: 0x06003BD2 RID: 15314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003694")]
		[Address(RVA = "0x101475C70", Offset = "0x1475C70", VA = "0x101475C70")]
		private void PopUpLevelUp()
		{
		}

		// Token: 0x06003BD3 RID: 15315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003695")]
		[Address(RVA = "0x101476890", Offset = "0x1476890", VA = "0x101476890")]
		public void HidePopUp()
		{
		}

		// Token: 0x06003BD4 RID: 15316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003696")]
		[Address(RVA = "0x101476944", Offset = "0x1476944", VA = "0x101476944")]
		public void SkipUpdateExp(bool popup = true)
		{
		}

		// Token: 0x06003BD5 RID: 15317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003697")]
		[Address(RVA = "0x101475DE0", Offset = "0x1475DE0", VA = "0x101475DE0")]
		private void UpdateExpObj()
		{
		}

		// Token: 0x06003BD6 RID: 15318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003698")]
		[Address(RVA = "0x101476998", Offset = "0x1476998", VA = "0x101476998")]
		public void SetLevelText(Text text)
		{
		}

		// Token: 0x06003BD7 RID: 15319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003699")]
		[Address(RVA = "0x1014769A0", Offset = "0x14769A0", VA = "0x1014769A0")]
		public void SetActiveExtendGauge(bool isActive)
		{
		}

		// Token: 0x06003BD8 RID: 15320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600369A")]
		[Address(RVA = "0x101476A68", Offset = "0x1476A68", VA = "0x101476A68")]
		public void SetActiveSimulateGauge(bool isActive)
		{
		}

		// Token: 0x06003BD9 RID: 15321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600369B")]
		[Address(RVA = "0x101476B18", Offset = "0x1476B18", VA = "0x101476B18")]
		public ExpGauge()
		{
		}

		// Token: 0x04004A99 RID: 19097
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400349B")]
		private float SIMURATE_GAUGE_SPEED;

		// Token: 0x04004A9A RID: 19098
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400349C")]
		private float SIMURATE_GAUGE_SPEED_RATE;

		// Token: 0x04004A9B RID: 19099
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400349D")]
		private int m_LevelUpPopUpUseIdx;

		// Token: 0x04004A9C RID: 19100
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x400349E")]
		private int m_MaxLevel;

		// Token: 0x04004A9D RID: 19101
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400349F")]
		private long m_RewardExp;

		// Token: 0x04004A9E RID: 19102
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40034A0")]
		private long m_BeforeExp;

		// Token: 0x04004A9F RID: 19103
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40034A1")]
		private long m_AfterExp;

		// Token: 0x04004AA0 RID: 19104
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40034A2")]
		private long m_NowExp;

		// Token: 0x04004AA1 RID: 19105
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40034A3")]
		private int m_BeforeLv;

		// Token: 0x04004AA2 RID: 19106
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40034A4")]
		private int m_AfterLv;

		// Token: 0x04004AA3 RID: 19107
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40034A5")]
		private int m_NowLv;

		// Token: 0x04004AA4 RID: 19108
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40034A6")]
		private long[] m_NeedExps;

		// Token: 0x04004AA5 RID: 19109
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40034A7")]
		private bool m_IsPlaying;

		// Token: 0x04004AA6 RID: 19110
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40034A8")]
		private SoundHandler m_SoundHandler;

		// Token: 0x04004AA7 RID: 19111
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40034A9")]
		[SerializeField]
		private Image m_ExpGaugeBodyObj;

		// Token: 0x04004AA8 RID: 19112
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40034AA")]
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x04004AA9 RID: 19113
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40034AB")]
		private Outline m_Outline;

		// Token: 0x04004AAA RID: 19114
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40034AC")]
		[SerializeField]
		private Text m_GetText;

		// Token: 0x04004AAB RID: 19115
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40034AD")]
		[SerializeField]
		private Text m_NextText;

		// Token: 0x04004AAC RID: 19116
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40034AE")]
		[SerializeField]
		private bool m_DisplayMaxLevel;

		// Token: 0x04004AAD RID: 19117
		[Cpp2IlInjected.FieldOffset(Offset = "0x99")]
		[Token(Token = "0x40034AF")]
		[SerializeField]
		private bool m_IsFriendshipMode;

		// Token: 0x04004AAE RID: 19118
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40034B0")]
		[SerializeField]
		private LevelUpPop[] m_LevelUpImageObj;

		// Token: 0x04004AAF RID: 19119
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40034B1")]
		private RectTransform[] m_LevelUpImageObjRectTransform;

		// Token: 0x04004AB0 RID: 19120
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40034B2")]
		[SerializeField]
		private Image m_ExtendGauge;

		// Token: 0x04004AB1 RID: 19121
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40034B3")]
		[SerializeField]
		private GameObject m_SimulateGaugeObj;

		// Token: 0x04004AB2 RID: 19122
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40034B4")]
		[SerializeField]
		private Image m_SimulateGauge;

		// Token: 0x04004AB3 RID: 19123
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40034B5")]
		private bool m_Simulate;

		// Token: 0x04004AB4 RID: 19124
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x40034B6")]
		private float m_SimulateRate;

		// Token: 0x04004AB5 RID: 19125
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40034B7")]
		private float m_SimulateRateDisp;

		// Token: 0x04004AB6 RID: 19126
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x40034B8")]
		private int m_SimulateAddLv;

		// Token: 0x04004AB7 RID: 19127
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40034B9")]
		private long m_SimulateExp;

		// Token: 0x04004AB8 RID: 19128
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40034BA")]
		private long m_SimulateNeedExp;

		// Token: 0x04004AB9 RID: 19129
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40034BB")]
		public Action<int> OnLevelupCallback;
	}
}
