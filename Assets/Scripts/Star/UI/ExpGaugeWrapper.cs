﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C65 RID: 3173
	[Token(Token = "0x2000883")]
	[StructLayout(3)]
	public class ExpGaugeWrapper : MonoBehaviour
	{
		// Token: 0x0600398E RID: 14734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003475")]
		[Address(RVA = "0x101476B34", Offset = "0x1476B34", VA = "0x101476B34")]
		public void Setup()
		{
		}

		// Token: 0x0600398F RID: 14735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003476")]
		[Address(RVA = "0x101476B38", Offset = "0x1476B38", VA = "0x101476B38")]
		private void Update()
		{
		}

		// Token: 0x06003990 RID: 14736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003477")]
		[Address(RVA = "0x101476C40", Offset = "0x1476C40", VA = "0x101476C40")]
		public void SetDestination(Text destination)
		{
		}

		// Token: 0x06003991 RID: 14737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003478")]
		[Address(RVA = "0x101476C48", Offset = "0x1476C48", VA = "0x101476C48")]
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
		}

		// Token: 0x06003992 RID: 14738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003479")]
		[Address(RVA = "0x101476CA4", Offset = "0x1476CA4", VA = "0x101476CA4")]
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
		}

		// Token: 0x06003993 RID: 14739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600347A")]
		[Address(RVA = "0x101476D20", Offset = "0x1476D20", VA = "0x101476D20")]
		public void PlayExpGauge()
		{
		}

		// Token: 0x06003994 RID: 14740 RVA: 0x00017F88 File Offset: 0x00016188
		[Token(Token = "0x600347B")]
		[Address(RVA = "0x101476D50", Offset = "0x1476D50", VA = "0x101476D50")]
		public bool IsPlayingExpGauge()
		{
			return default(bool);
		}

		// Token: 0x06003995 RID: 14741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600347C")]
		[Address(RVA = "0x101476D7C", Offset = "0x1476D7C", VA = "0x101476D7C")]
		public void SkipExpGaugePlaying()
		{
		}

		// Token: 0x06003996 RID: 14742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600347D")]
		[Address(RVA = "0x101476DAC", Offset = "0x1476DAC", VA = "0x101476DAC")]
		public ExpGaugeWrapper()
		{
		}

		// Token: 0x0400487F RID: 18559
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032EB")]
		[SerializeField]
		private ExpGauge m_Gauge;

		// Token: 0x04004880 RID: 18560
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032EC")]
		[SerializeField]
		private Text m_Source;

		// Token: 0x04004881 RID: 18561
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032ED")]
		[SerializeField]
		private Text m_Destination;
	}
}
