﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CE3 RID: 3299
	[Token(Token = "0x20008CF")]
	[StructLayout(3)]
	public class FaceIcon : MonoBehaviour
	{
		// Token: 0x06003C64 RID: 15460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003721")]
		[Address(RVA = "0x101476DB4", Offset = "0x1476DB4", VA = "0x101476DB4")]
		public void Apply(int charaID)
		{
		}

		// Token: 0x06003C65 RID: 15461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003722")]
		[Address(RVA = "0x101476E0C", Offset = "0x1476E0C", VA = "0x101476E0C")]
		public void Destroy()
		{
		}

		// Token: 0x06003C66 RID: 15462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003723")]
		[Address(RVA = "0x101476E50", Offset = "0x1476E50", VA = "0x101476E50")]
		public FaceIcon()
		{
		}

		// Token: 0x04004BB1 RID: 19377
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003535")]
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x04004BB2 RID: 19378
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003536")]
		protected int m_CharaID;
	}
}
