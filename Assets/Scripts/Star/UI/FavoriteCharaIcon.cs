﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DED RID: 3565
	[Token(Token = "0x2000991")]
	[StructLayout(3)]
	public class FavoriteCharaIcon : MonoBehaviour
	{
		// Token: 0x060041ED RID: 16877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C73")]
		[Address(RVA = "0x101476E60", Offset = "0x1476E60", VA = "0x101476E60")]
		public void SetData(int index, long charaMngID)
		{
		}

		// Token: 0x060041EE RID: 16878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C74")]
		[Address(RVA = "0x101476F0C", Offset = "0x1476F0C", VA = "0x101476F0C")]
		public void UpdateCharaIcon(long charaMngID)
		{
		}

		// Token: 0x060041EF RID: 16879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C75")]
		[Address(RVA = "0x10147702C", Offset = "0x147702C", VA = "0x10147702C")]
		private void CB_OnClickButton()
		{
		}

		// Token: 0x060041F0 RID: 16880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C76")]
		[Address(RVA = "0x101477084", Offset = "0x1477084", VA = "0x101477084")]
		public FavoriteCharaIcon()
		{
		}

		// Token: 0x040051E0 RID: 20960
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40039F2")]
		[SerializeField]
		private CharaIconWithFrame m_CharaIcon;

		// Token: 0x040051E1 RID: 20961
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40039F3")]
		[SerializeField]
		private Image m_imgEmptyIcon;

		// Token: 0x040051E2 RID: 20962
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40039F4")]
		[SerializeField]
		private CustomButton m_customBtn;

		// Token: 0x040051E3 RID: 20963
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40039F5")]
		public Action<int, long> m_OnClickCallback;

		// Token: 0x040051E4 RID: 20964
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40039F6")]
		private int m_Index;

		// Token: 0x040051E5 RID: 20965
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40039F7")]
		private long m_CharaMngID;
	}
}
