﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Filter
{
	// Token: 0x02001023 RID: 4131
	[Token(Token = "0x2000ABB")]
	[StructLayout(3)]
	public class FilterButton : MonoBehaviour
	{
		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x06004F0E RID: 20238 RVA: 0x0001B9F0 File Offset: 0x00019BF0
		// (set) Token: 0x06004F0F RID: 20239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004FC")]
		public int Idx
		{
			[Token(Token = "0x6004894")]
			[Address(RVA = "0x10147708C", Offset = "0x147708C", VA = "0x10147708C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004895")]
			[Address(RVA = "0x101477094", Offset = "0x1477094", VA = "0x101477094")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x14000102 RID: 258
		// (add) Token: 0x06004F10 RID: 20240 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004F11 RID: 20241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000102")]
		public event Action<int> OnClick
		{
			[Token(Token = "0x6004896")]
			[Address(RVA = "0x10147709C", Offset = "0x147709C", VA = "0x10147709C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004897")]
			[Address(RVA = "0x1014771A8", Offset = "0x14771A8", VA = "0x1014771A8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004F12 RID: 20242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004898")]
		[Address(RVA = "0x1014772B4", Offset = "0x14772B4", VA = "0x1014772B4")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x06004F13 RID: 20243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004899")]
		[Address(RVA = "0x101477308", Offset = "0x1477308", VA = "0x101477308")]
		public void SetDecided(bool flag)
		{
		}

		// Token: 0x06004F14 RID: 20244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600489A")]
		[Address(RVA = "0x101477340", Offset = "0x1477340", VA = "0x101477340")]
		public FilterButton()
		{
		}

		// Token: 0x04006181 RID: 24961
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400445A")]
		[SerializeField]
		private CustomButton m_CustomButton;
	}
}
