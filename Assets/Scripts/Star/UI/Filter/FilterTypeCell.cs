﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Filter
{
	// Token: 0x02001024 RID: 4132
	[Token(Token = "0x2000ABC")]
	[StructLayout(3)]
	public class FilterTypeCell : MonoBehaviour
	{
		// Token: 0x17000558 RID: 1368
		// (get) Token: 0x06004F15 RID: 20245 RVA: 0x0001BA08 File Offset: 0x00019C08
		[Token(Token = "0x170004FD")]
		public FilterWindow.eCellMode CellMode
		{
			[Token(Token = "0x600489B")]
			[Address(RVA = "0x101477348", Offset = "0x1477348", VA = "0x101477348")]
			get
			{
				return FilterWindow.eCellMode.Choice;
			}
		}

		// Token: 0x06004F16 RID: 20246 RVA: 0x0001BA20 File Offset: 0x00019C20
		[Token(Token = "0x600489C")]
		[Address(RVA = "0x101477350", Offset = "0x1477350", VA = "0x101477350")]
		public UISettings.eFilterCategory GetFilterCategory()
		{
			return UISettings.eFilterCategory.Class;
		}

		// Token: 0x06004F17 RID: 20247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600489D")]
		[Address(RVA = "0x101477358", Offset = "0x1477358", VA = "0x101477358")]
		public void Setup(UISettings.eFilterCategory category, FilterWindow.eCellMode cellMode, eText_CommonDB eTextCommon, string[] buttonNames, bool contentTitle)
		{
		}

		// Token: 0x06004F18 RID: 20248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600489E")]
		[Address(RVA = "0x101477AAC", Offset = "0x1477AAC", VA = "0x101477AAC")]
		public void Choice(int buttonIdx)
		{
		}

		// Token: 0x06004F19 RID: 20249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600489F")]
		[Address(RVA = "0x101477B40", Offset = "0x1477B40", VA = "0x101477B40")]
		public void SetFilter(bool[] buttonFlags)
		{
		}

		// Token: 0x06004F1A RID: 20250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048A0")]
		[Address(RVA = "0x101477960", Offset = "0x1477960", VA = "0x101477960")]
		public void SetAll(bool flag)
		{
		}

		// Token: 0x06004F1B RID: 20251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048A1")]
		[Address(RVA = "0x101477CF0", Offset = "0x1477CF0", VA = "0x101477CF0")]
		public void SetForceApply(bool all, bool[] flags)
		{
		}

		// Token: 0x06004F1C RID: 20252 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048A2")]
		[Address(RVA = "0x1014775BC", Offset = "0x14775BC", VA = "0x1014775BC")]
		private FilterButton CreateButton(int idx, string name)
		{
			return null;
		}

		// Token: 0x06004F1D RID: 20253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048A3")]
		[Address(RVA = "0x101478138", Offset = "0x1478138", VA = "0x101478138")]
		public void RebuildLayoutImmdiate()
		{
		}

		// Token: 0x06004F1E RID: 20254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048A4")]
		[Address(RVA = "0x101478290", Offset = "0x1478290", VA = "0x101478290")]
		private void OnClickFilterButtonCallBack(int idx)
		{
		}

		// Token: 0x06004F1F RID: 20255 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048A5")]
		[Address(RVA = "0x1014783C0", Offset = "0x14783C0", VA = "0x1014783C0")]
		public bool[] GetFilterFlagsWithAll()
		{
			return null;
		}

		// Token: 0x06004F20 RID: 20256 RVA: 0x0001BA38 File Offset: 0x00019C38
		[Token(Token = "0x60048A6")]
		[Address(RVA = "0x1014784AC", Offset = "0x14784AC", VA = "0x1014784AC")]
		public bool GetFilterFlagAll()
		{
			return default(bool);
		}

		// Token: 0x06004F21 RID: 20257 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048A7")]
		[Address(RVA = "0x1014784B4", Offset = "0x14784B4", VA = "0x1014784B4")]
		public bool[] GetFilterFlags()
		{
			return null;
		}

		// Token: 0x06004F22 RID: 20258 RVA: 0x0001BA50 File Offset: 0x00019C50
		[Token(Token = "0x60048A8")]
		[Address(RVA = "0x1014784BC", Offset = "0x14784BC", VA = "0x1014784BC")]
		public eText_CommonDB GeteTextCommon()
		{
			return eText_CommonDB.GameTitle;
		}

		// Token: 0x06004F23 RID: 20259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048A9")]
		[Address(RVA = "0x1014784C4", Offset = "0x14784C4", VA = "0x1014784C4")]
		public FilterTypeCell()
		{
		}

		// Token: 0x04006184 RID: 24964
		[Token(Token = "0x400445D")]
		public const int ALLBUTTON_IDX = -1;

		// Token: 0x04006185 RID: 24965
		[Token(Token = "0x400445E")]
		public const float ALLBUTTON_WIDTH = 203f;

		// Token: 0x04006186 RID: 24966
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400445F")]
		[SerializeField]
		private FilterButton m_HeadButtonPrefab;

		// Token: 0x04006187 RID: 24967
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004460")]
		[SerializeField]
		private FilterButton m_BodyButtonPrefab;

		// Token: 0x04006188 RID: 24968
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004461")]
		[SerializeField]
		private FilterButton m_TailButtonPrefab;

		// Token: 0x04006189 RID: 24969
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004462")]
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x0400618A RID: 24970
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004463")]
		[SerializeField]
		private RectTransform m_TitleButtonAllParent;

		// Token: 0x0400618B RID: 24971
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004464")]
		[SerializeField]
		private RectTransform m_TitleButtonParent;

		// Token: 0x0400618C RID: 24972
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004465")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x0400618D RID: 24973
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004466")]
		private UISettings.eFilterCategory m_FilterCategory;

		// Token: 0x0400618E RID: 24974
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004467")]
		private eText_CommonDB m_eTextCommon;

		// Token: 0x0400618F RID: 24975
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004468")]
		private int m_ButtonNum;

		// Token: 0x04006190 RID: 24976
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004469")]
		private List<FilterButton> m_ButtonList;

		// Token: 0x04006191 RID: 24977
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400446A")]
		private bool m_All;

		// Token: 0x04006192 RID: 24978
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400446B")]
		private bool[] m_ButtonFlags;

		// Token: 0x04006193 RID: 24979
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400446C")]
		private FilterWindow.eCellMode m_CellMode;

		// Token: 0x04006194 RID: 24980
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x400446D")]
		private bool m_TitleMode;
	}
}
