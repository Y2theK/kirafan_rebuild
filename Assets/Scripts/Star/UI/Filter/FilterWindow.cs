﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Filter
{
	// Token: 0x02001025 RID: 4133
	[Token(Token = "0x2000ABD")]
	[StructLayout(3)]
	public class FilterWindow : UIGroup
	{
		// Token: 0x14000103 RID: 259
		// (add) Token: 0x06004F24 RID: 20260 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004F25 RID: 20261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000103")]
		public event Action OnExecute
		{
			[Token(Token = "0x60048AA")]
			[Address(RVA = "0x10147853C", Offset = "0x147853C", VA = "0x10147853C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60048AB")]
			[Address(RVA = "0x101478648", Offset = "0x1478648", VA = "0x101478648")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004F26 RID: 20262 RVA: 0x0001BA68 File Offset: 0x00019C68
		[Token(Token = "0x60048AC")]
		[Address(RVA = "0x101478754", Offset = "0x1478754", VA = "0x101478754")]
		private int ModifyFilterIndex(UISettings.eFilterCategory category, eTitleTypeConverter conv, int j)
		{
			return 0;
		}

		// Token: 0x06004F27 RID: 20263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048AD")]
		[Address(RVA = "0x1014788E4", Offset = "0x14788E4", VA = "0x1014788E4")]
		public void Setup(UISettings.eUsingType_Filter usingType)
		{
		}

		// Token: 0x06004F28 RID: 20264 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048AE")]
		[Address(RVA = "0x101479F60", Offset = "0x1479F60", VA = "0x101479F60")]
		public void AddType(FilterWindow.FilterTypeData type)
		{
		}

		// Token: 0x06004F29 RID: 20265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048AF")]
		[Address(RVA = "0x10147923C", Offset = "0x147923C", VA = "0x10147923C")]
		public void AddElementFilterType()
		{
		}

		// Token: 0x06004F2A RID: 20266 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048B0")]
		[Address(RVA = "0x10147A120", Offset = "0x147A120", VA = "0x10147A120")]
		private string[] GetRarityFilterNames(eRare minRarity, eRare maxRarity)
		{
			return null;
		}

		// Token: 0x06004F2B RID: 20267 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B1")]
		[Address(RVA = "0x1014790C8", Offset = "0x14790C8", VA = "0x1014790C8")]
		public void AddRareCharaFilterType()
		{
		}

		// Token: 0x06004F2C RID: 20268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B2")]
		[Address(RVA = "0x101479180", Offset = "0x1479180", VA = "0x101479180")]
		public void AddRareWeaponFilterType()
		{
		}

		// Token: 0x06004F2D RID: 20269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B3")]
		[Address(RVA = "0x101478F68", Offset = "0x1478F68", VA = "0x101478F68")]
		public void AddClassFilterType()
		{
		}

		// Token: 0x06004F2E RID: 20270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B4")]
		[Address(RVA = "0x1014793F4", Offset = "0x14793F4", VA = "0x1014793F4")]
		public void AddTitleFilterType()
		{
		}

		// Token: 0x06004F2F RID: 20271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B5")]
		[Address(RVA = "0x101479AC4", Offset = "0x1479AC4", VA = "0x101479AC4")]
		public void AddHighRareCharaFilterType()
		{
		}

		// Token: 0x06004F30 RID: 20272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B6")]
		[Address(RVA = "0x1014794A0", Offset = "0x14794A0", VA = "0x1014794A0")]
		public void AddDeadlineFilterType()
		{
		}

		// Token: 0x06004F31 RID: 20273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B7")]
		[Address(RVA = "0x101479608", Offset = "0x1479608", VA = "0x101479608")]
		public void AddPresentTypeFilterType()
		{
		}

		// Token: 0x06004F32 RID: 20274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048B8")]
		[Address(RVA = "0x1014797A4", Offset = "0x14797A4", VA = "0x1014797A4")]
		public void AddStatusPriorityFilterType()
		{
		}

		// Token: 0x06004F33 RID: 20275 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048B9")]
		[Address(RVA = "0x10147A2B4", Offset = "0x147A2B4", VA = "0x10147A2B4")]
		private FilterWindow.FilterTypeData GetRoomFilterType(eRoomObjFilterType filterType, UISettings.eFilterCategory filterCategory, eText_CommonDB filterText, bool IsContentTitleMode)
		{
			return null;
		}

		// Token: 0x06004F34 RID: 20276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BA")]
		[Address(RVA = "0x1014799E4", Offset = "0x14799E4", VA = "0x1014799E4")]
		private void AddRoomObjectSeasonType()
		{
		}

		// Token: 0x06004F35 RID: 20277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BB")]
		[Address(RVA = "0x101479A1C", Offset = "0x1479A1C", VA = "0x101479A1C")]
		private void AddRoomObjectMotifType()
		{
		}

		// Token: 0x06004F36 RID: 20278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BC")]
		[Address(RVA = "0x101479A54", Offset = "0x1479A54", VA = "0x101479A54")]
		private void AddRoomObjectCharaActionType()
		{
		}

		// Token: 0x06004F37 RID: 20279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BD")]
		[Address(RVA = "0x101479A8C", Offset = "0x1479A8C", VA = "0x101479A8C")]
		private void AddRoomObjectCategoryType()
		{
		}

		// Token: 0x06004F38 RID: 20280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BE")]
		[Address(RVA = "0x101479B80", Offset = "0x1479B80", VA = "0x101479B80")]
		private void AddAbilitySphereDisplayFilterType()
		{
		}

		// Token: 0x06004F39 RID: 20281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048BF")]
		[Address(RVA = "0x101479CC4", Offset = "0x1479CC4", VA = "0x101479CC4")]
		private void AddAbilitySphereFilterType()
		{
		}

		// Token: 0x06004F3A RID: 20282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C0")]
		[Address(RVA = "0x101479E84", Offset = "0x1479E84", VA = "0x101479E84")]
		public void SetFilter(int filterTypeIdx, bool all, bool[] flags)
		{
		}

		// Token: 0x06004F3B RID: 20283 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048C1")]
		[Address(RVA = "0x10147A394", Offset = "0x147A394", VA = "0x10147A394")]
		public bool[] GetFilterFlag(int filterTypeIdx)
		{
			return null;
		}

		// Token: 0x06004F3C RID: 20284 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60048C2")]
		[Address(RVA = "0x10147A418", Offset = "0x147A418", VA = "0x10147A418")]
		public FilterTypeCell GetFilterType(int filterTypeIdx)
		{
			return null;
		}

		// Token: 0x06004F3D RID: 20285 RVA: 0x0001BA80 File Offset: 0x00019C80
		[Token(Token = "0x60048C3")]
		[Address(RVA = "0x10147A488", Offset = "0x147A488", VA = "0x10147A488")]
		public int GetFirstChoice(int filterTypeIdx)
		{
			return 0;
		}

		// Token: 0x06004F3E RID: 20286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C4")]
		[Address(RVA = "0x101479F20", Offset = "0x1479F20", VA = "0x101479F20")]
		public void SetHeaderText(string header)
		{
		}

		// Token: 0x06004F3F RID: 20287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C5")]
		[Address(RVA = "0x10147A56C", Offset = "0x147A56C", VA = "0x10147A56C", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F40 RID: 20288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C6")]
		[Address(RVA = "0x10147A600", Offset = "0x147A600", VA = "0x10147A600", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004F41 RID: 20289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C7")]
		[Address(RVA = "0x10147A608", Offset = "0x147A608", VA = "0x10147A608")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x06004F42 RID: 20290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C8")]
		[Address(RVA = "0x10147AD88", Offset = "0x147AD88", VA = "0x10147AD88")]
		private void FilterResultZeroCallback(int btnno)
		{
		}

		// Token: 0x06004F43 RID: 20291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048C9")]
		[Address(RVA = "0x10147B06C", Offset = "0x147B06C", VA = "0x10147B06C")]
		public void OnClickDefaultButtonCallBack()
		{
		}

		// Token: 0x06004F44 RID: 20292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048CA")]
		[Address(RVA = "0x10147B238", Offset = "0x147B238", VA = "0x10147B238")]
		private void AlignmentCells()
		{
		}

		// Token: 0x06004F45 RID: 20293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048CB")]
		[Address(RVA = "0x10147B728", Offset = "0x147B728", VA = "0x10147B728", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004F46 RID: 20294 RVA: 0x0001BA98 File Offset: 0x00019C98
		[Token(Token = "0x60048CC")]
		[Address(RVA = "0x10147AC18", Offset = "0x147AC18", VA = "0x10147AC18")]
		private bool CheckFilterResult()
		{
			return default(bool);
		}

		// Token: 0x06004F47 RID: 20295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048CD")]
		[Address(RVA = "0x10147B760", Offset = "0x147B760", VA = "0x10147B760")]
		public FilterWindow()
		{
		}

		// Token: 0x04006195 RID: 24981
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400446E")]
		private readonly eText_CommonDB[] FILTER_LABEL_COMMONTYPE;

		// Token: 0x04006196 RID: 24982
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400446F")]
		private readonly eText_CommonDB[] FILTER_LABEL_ABILITYSPHERE;

		// Token: 0x04006197 RID: 24983
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004470")]
		private List<FilterWindow.FilterTypeData> m_TypeList;

		// Token: 0x04006198 RID: 24984
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004471")]
		[SerializeField]
		public FilterTypeCell m_CellPrefab;

		// Token: 0x04006199 RID: 24985
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004472")]
		[SerializeField]
		private FilterTypeCell m_CellChoicePrefab;

		// Token: 0x0400619A RID: 24986
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004473")]
		[SerializeField]
		private FilterTypeCell m_CellContentTitlePrefab;

		// Token: 0x0400619B RID: 24987
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004474")]
		private List<FilterTypeCell> m_CellList;

		// Token: 0x0400619C RID: 24988
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004475")]
		[SerializeField]
		private ScrollRect m_Scroll;

		// Token: 0x0400619D RID: 24989
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004476")]
		[SerializeField]
		private Text m_HeaderText;

		// Token: 0x0400619E RID: 24990
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004477")]
		private bool m_IsDirty;

		// Token: 0x040061A0 RID: 24992
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004479")]
		private UISettings.eUsingType_Filter m_UsingType;

		// Token: 0x040061A1 RID: 24993
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400447A")]
		private Dictionary<UISettings.eFilterCategory, bool[]> m_dicBeforeFilterFlags;

		// Token: 0x040061A2 RID: 24994
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400447B")]
		private bool[] m_beforeAllFlags;

		// Token: 0x040061A3 RID: 24995
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400447C")]
		private eTitleTypeConverter m_Conv;

		// Token: 0x02001026 RID: 4134
		[Token(Token = "0x200122A")]
		public enum eCellMode
		{
			// Token: 0x040061A5 RID: 24997
			[Token(Token = "0x400705F")]
			Choice,
			// Token: 0x040061A6 RID: 24998
			[Token(Token = "0x4007060")]
			Filter,
			// Token: 0x040061A7 RID: 24999
			[Token(Token = "0x4007061")]
			FilterSingleSelect
		}

		// Token: 0x02001027 RID: 4135
		[Token(Token = "0x200122B")]
		public class FilterTypeData
		{
			// Token: 0x06004F48 RID: 20296 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600632A")]
			[Address(RVA = "0x10147A0C8", Offset = "0x147A0C8", VA = "0x10147A0C8")]
			public FilterTypeData(UISettings.eFilterCategory category, FilterWindow.eCellMode cellMode, bool contentTitleMode, eText_CommonDB eTextCommon, string[] buttonNames)
			{
			}

			// Token: 0x040061A8 RID: 25000
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007062")]
			public UISettings.eFilterCategory m_FilterCategory;

			// Token: 0x040061A9 RID: 25001
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4007063")]
			public FilterWindow.eCellMode m_CellMode;

			// Token: 0x040061AA RID: 25002
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007064")]
			public bool m_ContentTitleMode;

			// Token: 0x040061AB RID: 25003
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4007065")]
			public eText_CommonDB m_eTextCommon;

			// Token: 0x040061AC RID: 25004
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007066")]
			public string[] m_ButtonNames;
		}
	}
}
