﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Filter
{
	// Token: 0x02001028 RID: 4136
	[Token(Token = "0x2000ABE")]
	[StructLayout(3)]
	public class SortWindow : UIGroup
	{
		// Token: 0x14000104 RID: 260
		// (add) Token: 0x06004F49 RID: 20297 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004F4A RID: 20298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000104")]
		public event Action OnExecute
		{
			[Token(Token = "0x60048CE")]
			[Address(RVA = "0x10147B8A8", Offset = "0x147B8A8", VA = "0x10147B8A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60048CF")]
			[Address(RVA = "0x10147B9B4", Offset = "0x147B9B4", VA = "0x10147B9B4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004F4B RID: 20299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D0")]
		[Address(RVA = "0x10147BAC0", Offset = "0x147BAC0", VA = "0x10147BAC0")]
		public void Setup(UISettings.eUsingType_Sort usingType)
		{
		}

		// Token: 0x06004F4C RID: 20300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D1")]
		[Address(RVA = "0x10147BF00", Offset = "0x147BF00", VA = "0x10147BF00")]
		public void SetupButtons(eText_CommonDB[] keyNames, int columnMax)
		{
		}

		// Token: 0x06004F4D RID: 20301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D2")]
		[Address(RVA = "0x10147C2C0", Offset = "0x147C2C0", VA = "0x10147C2C0")]
		public void SetupButtons(string[] keyNames, int columnMax = 5)
		{
		}

		// Token: 0x06004F4E RID: 20302 RVA: 0x0001BAB0 File Offset: 0x00019CB0
		[Token(Token = "0x60048D3")]
		[Address(RVA = "0x10147C60C", Offset = "0x147C60C", VA = "0x10147C60C")]
		public ushort GetSelectSortKey()
		{
			return 0;
		}

		// Token: 0x06004F4F RID: 20303 RVA: 0x0001BAC8 File Offset: 0x00019CC8
		[Token(Token = "0x60048D4")]
		[Address(RVA = "0x10147C614", Offset = "0x147C614", VA = "0x10147C614")]
		public UISettings.eSortOrderType GetSortOrderType()
		{
			return UISettings.eSortOrderType.Ascending;
		}

		// Token: 0x06004F50 RID: 20304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D5")]
		[Address(RVA = "0x10147C088", Offset = "0x147C088", VA = "0x10147C088")]
		public void SetOrderValue(ushort orderValue)
		{
		}

		// Token: 0x06004F51 RID: 20305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D6")]
		[Address(RVA = "0x10147C1E8", Offset = "0x147C1E8", VA = "0x10147C1E8")]
		public void SetOrderType(UISettings.eSortOrderType orderType)
		{
		}

		// Token: 0x06004F52 RID: 20306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D7")]
		[Address(RVA = "0x10147C61C", Offset = "0x147C61C", VA = "0x10147C61C")]
		public void SetHeaderText(string header)
		{
		}

		// Token: 0x06004F53 RID: 20307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D8")]
		[Address(RVA = "0x10147C620", Offset = "0x147C620", VA = "0x10147C620", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F54 RID: 20308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048D9")]
		[Address(RVA = "0x10147C6B4", Offset = "0x147C6B4", VA = "0x10147C6B4", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004F55 RID: 20309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DA")]
		[Address(RVA = "0x10147C6BC", Offset = "0x147C6BC", VA = "0x10147C6BC")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x06004F56 RID: 20310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DB")]
		[Address(RVA = "0x10147C7F4", Offset = "0x147C7F4", VA = "0x10147C7F4")]
		public void OnClickCancelButtonCallBack()
		{
		}

		// Token: 0x06004F57 RID: 20311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DC")]
		[Address(RVA = "0x10147C800", Offset = "0x147C800", VA = "0x10147C800")]
		public void OnClickDefaultButtonCallBack()
		{
		}

		// Token: 0x06004F58 RID: 20312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DD")]
		[Address(RVA = "0x10147C884", Offset = "0x147C884", VA = "0x10147C884")]
		private void OnClickSortKeyButtonCallBack(int idx)
		{
		}

		// Token: 0x06004F59 RID: 20313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DE")]
		[Address(RVA = "0x10147C900", Offset = "0x147C900", VA = "0x10147C900")]
		private void OnClickSortOrderTypeButtonCallBack(int idx)
		{
		}

		// Token: 0x06004F5A RID: 20314 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048DF")]
		[Address(RVA = "0x10147C904", Offset = "0x147C904", VA = "0x10147C904")]
		public SortWindow()
		{
		}

		// Token: 0x040061AD RID: 25005
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400447D")]
		private readonly eText_CommonDB[] CHARALIST_SORT;

		// Token: 0x040061AE RID: 25006
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400447E")]
		private readonly UISettings.eSortValue_Chara[] CHARALIST_ORDER_VALUE;

		// Token: 0x040061AF RID: 25007
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400447F")]
		private readonly eText_CommonDB[] WEAPONLIST_SORT;

		// Token: 0x040061B0 RID: 25008
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004480")]
		private readonly UISettings.eSortValue_Weapon[] WEAPONLIST_ORDER_VALUE;

		// Token: 0x040061B1 RID: 25009
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004481")]
		private readonly eText_CommonDB[] SUPPORT_SORT;

		// Token: 0x040061B2 RID: 25010
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004482")]
		private readonly UISettings.eSortValue_Support[] SUPPORT_ORDER_VALUE;

		// Token: 0x040061B3 RID: 25011
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004483")]
		[SerializeField]
		private FilterButton m_ButtonPrefab;

		// Token: 0x040061B4 RID: 25012
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004484")]
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x040061B5 RID: 25013
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004485")]
		[SerializeField]
		private RectTransform m_ButtonParentLow;

		// Token: 0x040061B6 RID: 25014
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004486")]
		[SerializeField]
		private RectTransform m_OrderParent;

		// Token: 0x040061B7 RID: 25015
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004487")]
		private List<FilterButton> m_OrderKeyButtonList;

		// Token: 0x040061B8 RID: 25016
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004488")]
		private List<FilterButton> m_OrderTypeButtonList;

		// Token: 0x040061B9 RID: 25017
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004489")]
		private ushort m_SortOrderValue;

		// Token: 0x040061BA RID: 25018
		[Cpp2IlInjected.FieldOffset(Offset = "0xEC")]
		[Token(Token = "0x400448A")]
		private UISettings.eSortOrderType m_SortOrderType;

		// Token: 0x040061BC RID: 25020
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400448C")]
		private UISettings.eUsingType_Sort m_UsingType;

		// Token: 0x040061BD RID: 25021
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x400448D")]
		private List<ushort> m_OrderValueList;

		// Token: 0x02001029 RID: 4137
		[Token(Token = "0x200122C")]
		public enum eSortOrderType
		{
			// Token: 0x040061BF RID: 25023
			[Token(Token = "0x4007068")]
			Ascending,
			// Token: 0x040061C0 RID: 25024
			[Token(Token = "0x4007069")]
			Descending,
			// Token: 0x040061C1 RID: 25025
			[Token(Token = "0x400706A")]
			Num
		}
	}
}
