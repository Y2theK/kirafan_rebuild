﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CBA RID: 3258
	[Token(Token = "0x20008BB")]
	[StructLayout(3)]
	public class FilterManager
	{
		// Token: 0x06003BDA RID: 15322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600369C")]
		[Address(RVA = "0x10147CAD4", Offset = "0x147CAD4", VA = "0x10147CAD4")]
		public void SetupDefault()
		{
		}

		// Token: 0x06003BDB RID: 15323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600369D")]
		[Address(RVA = "0x10147D058", Offset = "0x147D058", VA = "0x10147D058")]
		public void SetDefault(FilterManager.eListType type)
		{
		}

		// Token: 0x06003BDC RID: 15324 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600369E")]
		[Address(RVA = "0x10147D088", Offset = "0x147D088", VA = "0x10147D088")]
		public FilterManager.FilterParam GetParam(FilterManager.eListType type)
		{
			return null;
		}

		// Token: 0x06003BDD RID: 15325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600369F")]
		[Address(RVA = "0x10147CBFC", Offset = "0x147CBFC", VA = "0x10147CBFC")]
		private void SetDefault(FilterManager.FilterParam filterParam, FilterManager.eListType type)
		{
		}

		// Token: 0x06003BDE RID: 15326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A0")]
		[Address(RVA = "0x10147D0E0", Offset = "0x147D0E0", VA = "0x10147D0E0")]
		public FilterManager()
		{
		}

		// Token: 0x04004ABA RID: 19130
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40034BC")]
		public readonly FilterManager.eFilterCategory[][] FILTER_CATEGORY;

		// Token: 0x04004ABB RID: 19131
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40034BD")]
		public FilterManager.FilterParam[] m_FilterParams;

		// Token: 0x02000CBB RID: 3259
		[Token(Token = "0x20010C7")]
		public enum eListType
		{
			// Token: 0x04004ABD RID: 19133
			[Token(Token = "0x4006951")]
			None = -1,
			// Token: 0x04004ABE RID: 19134
			[Token(Token = "0x4006952")]
			Chara,
			// Token: 0x04004ABF RID: 19135
			[Token(Token = "0x4006953")]
			Weapon,
			// Token: 0x04004AC0 RID: 19136
			[Token(Token = "0x4006954")]
			Support,
			// Token: 0x04004AC1 RID: 19137
			[Token(Token = "0x4006955")]
			Present,
			// Token: 0x04004AC2 RID: 19138
			[Token(Token = "0x4006956")]
			Num
		}

		// Token: 0x02000CBC RID: 3260
		[Token(Token = "0x20010C8")]
		public enum eFilterCategory
		{
			// Token: 0x04004AC4 RID: 19140
			[Token(Token = "0x4006958")]
			Class,
			// Token: 0x04004AC5 RID: 19141
			[Token(Token = "0x4006959")]
			Rare,
			// Token: 0x04004AC6 RID: 19142
			[Token(Token = "0x400695A")]
			Element,
			// Token: 0x04004AC7 RID: 19143
			[Token(Token = "0x400695B")]
			Title,
			// Token: 0x04004AC8 RID: 19144
			[Token(Token = "0x400695C")]
			DeadLine,
			// Token: 0x04004AC9 RID: 19145
			[Token(Token = "0x400695D")]
			PresentType,
			// Token: 0x04004ACA RID: 19146
			[Token(Token = "0x400695E")]
			Num
		}

		// Token: 0x02000CBD RID: 3261
		[Token(Token = "0x20010C9")]
		public enum eFilterDeadLine
		{
			// Token: 0x04004ACC RID: 19148
			[Token(Token = "0x4006960")]
			Exist,
			// Token: 0x04004ACD RID: 19149
			[Token(Token = "0x4006961")]
			NoExist,
			// Token: 0x04004ACE RID: 19150
			[Token(Token = "0x4006962")]
			Num
		}

		// Token: 0x02000CBE RID: 3262
		[Token(Token = "0x20010CA")]
		public enum eFilterPresentType
		{
			// Token: 0x04004AD0 RID: 19152
			[Token(Token = "0x4006964")]
			Chara,
			// Token: 0x04004AD1 RID: 19153
			[Token(Token = "0x4006965")]
			Weapon,
			// Token: 0x04004AD2 RID: 19154
			[Token(Token = "0x4006966")]
			Item,
			// Token: 0x04004AD3 RID: 19155
			[Token(Token = "0x4006967")]
			Gold,
			// Token: 0x04004AD4 RID: 19156
			[Token(Token = "0x4006968")]
			Etc,
			// Token: 0x04004AD5 RID: 19157
			[Token(Token = "0x4006969")]
			Num
		}

		// Token: 0x02000CBF RID: 3263
		[Token(Token = "0x20010CB")]
		public class FilterParam
		{
			// Token: 0x06003BDF RID: 15327 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061F2")]
			[Address(RVA = "0x10147D374", Offset = "0x147D374", VA = "0x10147D374")]
			public FilterManager.FilterParam.FilterCategoryParam GetCategoryParam(FilterManager.eFilterCategory category)
			{
				return null;
			}

			// Token: 0x06003BE0 RID: 15328 RVA: 0x00018618 File Offset: 0x00016818
			[Token(Token = "0x60061F3")]
			[Address(RVA = "0x10147D458", Offset = "0x147D458", VA = "0x10147D458")]
			public bool Check(FilterManager.eFilterCategory category, int idx)
			{
				return default(bool);
			}

			// Token: 0x06003BE1 RID: 15329 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061F4")]
			[Address(RVA = "0x10147CBF4", Offset = "0x147CBF4", VA = "0x10147CBF4")]
			public FilterParam()
			{
			}

			// Token: 0x04004AD6 RID: 19158
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400696A")]
			public FilterManager.eListType m_Type;

			// Token: 0x04004AD7 RID: 19159
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400696B")]
			public FilterManager.FilterParam.FilterCategoryParam[] m_TypeParams;

			// Token: 0x02000CC0 RID: 3264
			[Token(Token = "0x200134E")]
			public class FilterCategoryParam
			{
				// Token: 0x06003BE2 RID: 15330 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600651A")]
				[Address(RVA = "0x10147D0D8", Offset = "0x147D0D8", VA = "0x10147D0D8")]
				public FilterCategoryParam()
				{
				}

				// Token: 0x04004AD8 RID: 19160
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x40075B2")]
				public FilterManager.eFilterCategory m_Category;

				// Token: 0x04004AD9 RID: 19161
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x40075B3")]
				public bool m_All;

				// Token: 0x04004ADA RID: 19162
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x40075B4")]
				public bool[] m_Flags;
			}
		}
	}
}
