﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Filter;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB0 RID: 3504
	[Token(Token = "0x2000963")]
	[StructLayout(3)]
	public class FilterUI : MenuUIBase
	{
		// Token: 0x06004092 RID: 16530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B20")]
		[Address(RVA = "0x10147D4DC", Offset = "0x147D4DC", VA = "0x10147D4DC")]
		public void Setup(UISettings.eUsingType_Filter usingType, Action OnExecute)
		{
		}

		// Token: 0x06004093 RID: 16531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B21")]
		[Address(RVA = "0x10147D70C", Offset = "0x147D70C", VA = "0x10147D70C")]
		private void Update()
		{
		}

		// Token: 0x06004094 RID: 16532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B22")]
		[Address(RVA = "0x10147D8BC", Offset = "0x147D8BC", VA = "0x10147D8BC")]
		private void ChangeStep(FilterUI.eStep step)
		{
		}

		// Token: 0x06004095 RID: 16533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B23")]
		[Address(RVA = "0x10147D9C4", Offset = "0x147D9C4", VA = "0x10147D9C4", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004096 RID: 16534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B24")]
		[Address(RVA = "0x10147DA80", Offset = "0x147DA80", VA = "0x10147DA80", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004097 RID: 16535 RVA: 0x00019068 File Offset: 0x00017268
		[Token(Token = "0x6003B25")]
		[Address(RVA = "0x10147DB20", Offset = "0x147DB20", VA = "0x10147DB20", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004098 RID: 16536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B26")]
		[Address(RVA = "0x10147DB30", Offset = "0x147DB30", VA = "0x10147DB30")]
		public FilterUI()
		{
		}

		// Token: 0x04004FB7 RID: 20407
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400382D")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04004FB8 RID: 20408
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400382E")]
		[SerializeField]
		private FilterWindow m_FilterWindow;

		// Token: 0x04004FB9 RID: 20409
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400382F")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04004FBA RID: 20410
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003830")]
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04004FBB RID: 20411
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003831")]
		private FilterUI.eStep m_Step;

		// Token: 0x04004FBC RID: 20412
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003832")]
		public Action OnStartOut;

		// Token: 0x02000DB1 RID: 3505
		[Token(Token = "0x2001113")]
		private enum eStep
		{
			// Token: 0x04004FBE RID: 20414
			[Token(Token = "0x4006AD4")]
			Hide,
			// Token: 0x04004FBF RID: 20415
			[Token(Token = "0x4006AD5")]
			In,
			// Token: 0x04004FC0 RID: 20416
			[Token(Token = "0x4006AD6")]
			Idle,
			// Token: 0x04004FC1 RID: 20417
			[Token(Token = "0x4006AD7")]
			Out,
			// Token: 0x04004FC2 RID: 20418
			[Token(Token = "0x4006AD8")]
			End
		}
	}
}
