﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DF4 RID: 3572
	[Token(Token = "0x2000996")]
	[StructLayout(3)]
	public class FilterUIArg : OverlayUIArgBase
	{
		// Token: 0x06004210 RID: 16912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C96")]
		[Address(RVA = "0x10147DB38", Offset = "0x147DB38", VA = "0x10147DB38")]
		public FilterUIArg(UISettings.eUsingType_Filter usingType, Action OnExecute)
		{
		}

		// Token: 0x0400520C RID: 21004
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003A12")]
		public UISettings.eUsingType_Filter m_UsingType;

		// Token: 0x0400520D RID: 21005
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4003A13")]
		public bool m_IsSupportTitleSelect;

		// Token: 0x0400520E RID: 21006
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A14")]
		public Action m_OnFilterExecute;
	}
}
