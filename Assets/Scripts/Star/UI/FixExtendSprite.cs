﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CC9 RID: 3273
	[Token(Token = "0x20008BE")]
	[StructLayout(3)]
	public class FixExtendSprite : MonoBehaviour
	{
		// Token: 0x06003BEF RID: 15343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036AC")]
		[Address(RVA = "0x10147DB74", Offset = "0x147DB74", VA = "0x10147DB74")]
		public void FixSprite()
		{
		}

		// Token: 0x06003BF0 RID: 15344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036AD")]
		[Address(RVA = "0x10147DB78", Offset = "0x147DB78", VA = "0x10147DB78")]
		public FixExtendSprite()
		{
		}
	}
}
