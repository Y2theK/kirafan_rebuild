﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CCA RID: 3274
	[Token(Token = "0x20008BF")]
	[StructLayout(3)]
	public class FixFontText : MonoBehaviour
	{
		// Token: 0x06003BF1 RID: 15345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036AE")]
		[Address(RVA = "0x10147DB80", Offset = "0x147DB80", VA = "0x10147DB80")]
		public void SetEnableChangeOutlineColor(bool flg)
		{
		}

		// Token: 0x06003BF2 RID: 15346 RVA: 0x00018630 File Offset: 0x00016830
		[Token(Token = "0x60036AF")]
		[Address(RVA = "0x10147DB88", Offset = "0x147DB88", VA = "0x10147DB88")]
		public bool GetEnableChangeOutlineColor()
		{
			return default(bool);
		}

		// Token: 0x06003BF3 RID: 15347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036B0")]
		[Address(RVA = "0x10147DB90", Offset = "0x147DB90", VA = "0x10147DB90")]
		private void Awake()
		{
		}

		// Token: 0x06003BF4 RID: 15348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036B1")]
		[Address(RVA = "0x10147DB94", Offset = "0x147DB94", VA = "0x10147DB94")]
		private void Apply()
		{
		}

		// Token: 0x06003BF5 RID: 15349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036B2")]
		[Address(RVA = "0x10147DEAC", Offset = "0x147DEAC", VA = "0x10147DEAC")]
		public FixFontText()
		{
		}

		// Token: 0x04004B0A RID: 19210
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40034C1")]
		private readonly Color m_BlackColor;

		// Token: 0x04004B0B RID: 19211
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40034C2")]
		private readonly Color m_WhiteColor;

		// Token: 0x04004B0C RID: 19212
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40034C3")]
		private readonly Color m_WarningColor;

		// Token: 0x04004B0D RID: 19213
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40034C4")]
		private readonly Color m_BlueColor;

		// Token: 0x04004B0E RID: 19214
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40034C5")]
		[SerializeField]
		private FixFontText.eColorType m_TextColorType;

		// Token: 0x04004B0F RID: 19215
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40034C6")]
		private bool m_EnableChangeOutlineColor;

		// Token: 0x02000CCB RID: 3275
		[Token(Token = "0x20010D2")]
		private enum eColorType
		{
			// Token: 0x04004B11 RID: 19217
			[Token(Token = "0x4006999")]
			None,
			// Token: 0x04004B12 RID: 19218
			[Token(Token = "0x400699A")]
			Black,
			// Token: 0x04004B13 RID: 19219
			[Token(Token = "0x400699B")]
			White,
			// Token: 0x04004B14 RID: 19220
			[Token(Token = "0x400699C")]
			Warning,
			// Token: 0x04004B15 RID: 19221
			[Token(Token = "0x400699D")]
			Blue
		}
	}
}
