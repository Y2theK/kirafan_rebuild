﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C99 RID: 3225
	[Token(Token = "0x20008A1")]
	[StructLayout(3)]
	public class FixScrollRect : MonoBehaviour, IBeginDragHandler, IEventSystemHandler, IDragHandler, IEndDragHandler
	{
		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x06003AB2 RID: 15026 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700042F")]
		public ScrollRect ScrollRect
		{
			[Token(Token = "0x6003575")]
			[Address(RVA = "0x10147DF94", Offset = "0x147DF94", VA = "0x10147DF94")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06003AB3 RID: 15027 RVA: 0x000183A8 File Offset: 0x000165A8
		[Token(Token = "0x17000430")]
		public float ScrollValue
		{
			[Token(Token = "0x6003576")]
			[Address(RVA = "0x10147E02C", Offset = "0x147E02C", VA = "0x10147E02C")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x06003AB4 RID: 15028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003577")]
		[Address(RVA = "0x10147E0CC", Offset = "0x147E0CC", VA = "0x10147E0CC")]
		private void Start()
		{
		}

		// Token: 0x06003AB5 RID: 15029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003578")]
		[Address(RVA = "0x10147E3D4", Offset = "0x147E3D4", VA = "0x10147E3D4")]
		public void SetVerticalNormalizedPosition(float value)
		{
		}

		// Token: 0x06003AB6 RID: 15030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003579")]
		[Address(RVA = "0x10147E544", Offset = "0x147E544", VA = "0x10147E544", Slot = "7")]
		protected virtual void LateUpdate()
		{
		}

		// Token: 0x06003AB7 RID: 15031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357A")]
		[Address(RVA = "0x10147EB84", Offset = "0x147EB84", VA = "0x10147EB84")]
		public void OnVerticalScrollBarValueChanged(float value)
		{
		}

		// Token: 0x06003AB8 RID: 15032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357B")]
		[Address(RVA = "0x10147ECB8", Offset = "0x147ECB8", VA = "0x10147ECB8")]
		public void OnScrollRectValueChanged(Vector2 pos)
		{
		}

		// Token: 0x06003AB9 RID: 15033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357C")]
		[Address(RVA = "0x10147EE00", Offset = "0x147EE00", VA = "0x10147EE00", Slot = "4")]
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003ABA RID: 15034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357D")]
		[Address(RVA = "0x10147EE04", Offset = "0x147EE04", VA = "0x10147EE04", Slot = "5")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003ABB RID: 15035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357E")]
		[Address(RVA = "0x10147F070", Offset = "0x147F070", VA = "0x10147F070", Slot = "6")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003ABC RID: 15036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600357F")]
		[Address(RVA = "0x10147F130", Offset = "0x147F130", VA = "0x10147F130")]
		public FixScrollRect()
		{
		}

		// Token: 0x0400497A RID: 18810
		[Token(Token = "0x40033A3")]
		private const float VERTICAL_SCROLLBAR_MINSIZE = 0.2f;

		// Token: 0x0400497B RID: 18811
		[Token(Token = "0x40033A4")]
		private const float SCROLLBAR_WIDTH = 24f;

		// Token: 0x0400497C RID: 18812
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033A5")]
		[SerializeField]
		private Scrollbar m_VerticalScrollBar;

		// Token: 0x0400497D RID: 18813
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033A6")]
		private GameObject m_VerticalScrollBarObj;

		// Token: 0x0400497E RID: 18814
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033A7")]
		private float m_ScrollMaxVertical;

		// Token: 0x0400497F RID: 18815
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40033A8")]
		private ScrollRect m_ScrollRect;

		// Token: 0x04004980 RID: 18816
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40033A9")]
		private Vector2 m_DefaultSizeDelta;
	}
}
