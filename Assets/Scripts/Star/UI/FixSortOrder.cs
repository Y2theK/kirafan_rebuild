﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    [RequireComponent(typeof(Canvas), typeof(GraphicRaycaster))]
    public class FixSortOrder : MonoBehaviour {
        [SerializeField] private UIDefine.eSortOrderTypeID m_SortOrder = UIDefine.eSortOrderTypeID.Inherited;
        [SerializeField] private bool m_isUseSaveAreaCamera;
        [SerializeField] private int m_Offset;

        private bool m_IsDirty = true;
        private Canvas m_Canvas;
        private Camera m_BackupCamera;
        private RectTransform m_RectTransform;

        public RectTransform RectTransform {
            get {
                if (m_RectTransform == null) {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        public UIDefine.eSortOrderTypeID GetRawSortOrder() {
            return m_SortOrder;
        }

        public UIDefine.eSortOrderTypeID GetSortOrderID() {
            UIDefine.eSortOrderTypeID result = m_SortOrder;
            if (m_SortOrder == UIDefine.eSortOrderTypeID.Inherited) {
                FixSortOrder parent = RectTransform.parent.GetComponentInParent<FixSortOrder>();
                if (parent == null) {
                    result = UIDefine.eSortOrderTypeID.UI;
                } else {
                    result = parent.GetSortOrderID();
                }
            }
            return result;
        }

        public int GetSortOrder() {
            int result = UIDefine.SORT_ORDER[(int)m_SortOrder] + m_Offset;
            if (m_SortOrder == UIDefine.eSortOrderTypeID.Inherited) {
                result = UIDefine.SORT_ORDER[(int)UIDefine.eSortOrderTypeID.UI];
                Transform parent = RectTransform.parent;
                if (parent != null) {
                    FixSortOrder componentInParent = parent.GetComponentInParent<FixSortOrder>();
                    if (componentInParent != null) {
                        result = componentInParent.GetSortOrder() + m_Offset;
                    }
                }
            }
            return result;
        }

        public int GetOffset() {
            return m_Offset;
        }

        public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrderType, int offset = 0) {
            m_SortOrder = sortOrderType;
            m_Offset = offset;
            m_IsDirty = true;
            Apply();
        }

        private void Apply() {
            if (!TryGetComponent(out m_Canvas)) {
                return;
            }
            m_Canvas.overrideSorting = true;
            m_Canvas.sortingOrder = GetSortOrder();
            m_IsDirty = false;
            FixSafeArea();
        }

        private void Start() {
            Apply();
        }

        private void Update() {
            if (m_Canvas != null && m_Canvas.worldCamera != m_BackupCamera) {
                m_BackupCamera = m_Canvas.worldCamera;
                Apply();
            } else if (m_IsDirty) {
                Apply();
            }
            if (m_SortOrder == UIDefine.eSortOrderTypeID.TitleUI) {
                ApplyCamera();
            }
            if (m_Canvas != null && m_Canvas.worldCamera == null && GameSystem.Inst != null && GameSystem.Inst.IsAvailable()) {
                ApplyCamera();
            }
        }

        private Camera GetNowCamera() {
            if (m_isUseSaveAreaCamera) {
                return GameSystem.Inst.SaveAreaCamera;
            }
            UIDefine.eCameraType cameraType = UIDefine.CAMERA_TYPE[(int)GetSortOrderID()];
            return cameraType switch {
                UIDefine.eCameraType.System => GameSystem.Inst.SystemUICamera,
                UIDefine.eCameraType.UI => GameSystem.Inst.UICamera,
                UIDefine.eCameraType.FullUI => GameSystem.Inst.FullUICamera,
                _ => null
            };
        }

        public void ApplyCamera() {
            m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
            m_Canvas.worldCamera = GetNowCamera();
            FixSafeArea();
        }

        private void FixSafeArea() {
            if (m_Canvas == null) { return; }
            if (!m_Canvas.TryGetComponent<CanvasScaler>(out var scaler)) { return; }
            if (m_Canvas.worldCamera == null) { return; }

            Rect worldRect = m_Canvas.worldCamera.rect;
            float size = Mathf.Min(worldRect.width, worldRect.height);
            scaler.referenceResolution = new Vector2(UIDefine.SCREEN_SIZE_X / size, UIDefine.SCREEN_SIZE_Y / size);

            float val1, val2;
            if (worldRect.width < 1f || worldRect.height < 1f) {
                if (worldRect.width > 1f) {
                    scaler.matchWidthOrHeight = 1f;
                    return;
                }
                val1 = worldRect.height;
                val2 = 1f;
            } else {
                val1 = Screen.height / Screen.width;
                val2 = UIDefine.IDEAL_ASPECT_RATIO;
            }
            if (val1 < val2) { return; }
            scaler.matchWidthOrHeight = 0f;
        }
    }
}
