﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CCD RID: 3277
	[Token(Token = "0x20008C1")]
	[StructLayout(3)]
	public class FriendshipGauge : MonoBehaviour
	{
		// Token: 0x06003C03 RID: 15363 RVA: 0x000186A8 File Offset: 0x000168A8
		[Token(Token = "0x60036C0")]
		[Address(RVA = "0x10147FE64", Offset = "0x147FE64", VA = "0x10147FE64")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x1400006D RID: 109
		// (add) Token: 0x06003C04 RID: 15364 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003C05 RID: 15365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400006D")]
		public event Action m_OnLevelUp
		{
			[Token(Token = "0x60036C1")]
			[Address(RVA = "0x10147FE74", Offset = "0x147FE74", VA = "0x10147FE74")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60036C2")]
			[Address(RVA = "0x10147FF80", Offset = "0x147FF80", VA = "0x10147FF80")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400006E RID: 110
		// (add) Token: 0x06003C06 RID: 15366 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003C07 RID: 15367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400006E")]
		public event Action m_OnFinishPlay
		{
			[Token(Token = "0x60036C3")]
			[Address(RVA = "0x10148008C", Offset = "0x148008C", VA = "0x10148008C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60036C4")]
			[Address(RVA = "0x101480198", Offset = "0x1480198", VA = "0x101480198")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003C08 RID: 15368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036C5")]
		[Address(RVA = "0x1014802A4", Offset = "0x14802A4", VA = "0x1014802A4")]
		public void Apply(int friendship, long exp, sbyte expTableID)
		{
		}

		// Token: 0x06003C09 RID: 15369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036C6")]
		[Address(RVA = "0x101480450", Offset = "0x1480450", VA = "0x101480450")]
		public void Play(int friendship, long exp, int afterFrendship, long afterExp, sbyte expTableID)
		{
		}

		// Token: 0x06003C0A RID: 15370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036C7")]
		[Address(RVA = "0x101480540", Offset = "0x1480540", VA = "0x101480540")]
		public void Skip(bool popup = true)
		{
		}

		// Token: 0x06003C0B RID: 15371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036C8")]
		[Address(RVA = "0x1014805B4", Offset = "0x14805B4", VA = "0x1014805B4")]
		private void Update()
		{
		}

		// Token: 0x06003C0C RID: 15372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036C9")]
		[Address(RVA = "0x101480778", Offset = "0x1480778", VA = "0x101480778")]
		public void HidePop()
		{
		}

		// Token: 0x06003C0D RID: 15373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036CA")]
		[Address(RVA = "0x1014807A8", Offset = "0x14807A8", VA = "0x1014807A8")]
		public void SetSimulateGauge(int afterFriendshipLv, long afterExp)
		{
		}

		// Token: 0x06003C0E RID: 15374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036CB")]
		[Address(RVA = "0x101480EC0", Offset = "0x1480EC0", VA = "0x101480EC0")]
		public void SetSimulateOff()
		{
		}

		// Token: 0x06003C0F RID: 15375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036CC")]
		[Address(RVA = "0x10148108C", Offset = "0x148108C", VA = "0x10148108C")]
		public FriendshipGauge()
		{
		}

		// Token: 0x04004B1D RID: 19229
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40034CE")]
		[SerializeField]
		private ImageNumbers m_ImageNumber;

		// Token: 0x04004B1E RID: 19230
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40034CF")]
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04004B1F RID: 19231
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40034D0")]
		[SerializeField]
		private Text m_TextValue;

		// Token: 0x04004B20 RID: 19232
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40034D1")]
		[SerializeField]
		private LvupPopPlayer m_LvPopPlayer;

		// Token: 0x04004B21 RID: 19233
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40034D2")]
		[SerializeField]
		private Image m_ExtendGauge;

		// Token: 0x04004B22 RID: 19234
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40034D3")]
		[SerializeField]
		private GameObject m_SimulateGaugeObj;

		// Token: 0x04004B23 RID: 19235
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40034D4")]
		[SerializeField]
		private Image m_SimulateGauge;

		// Token: 0x04004B24 RID: 19236
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40034D5")]
		private float m_SimulateRate;

		// Token: 0x04004B25 RID: 19237
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40034D6")]
		private long m_SimulateCurrentExp;

		// Token: 0x04004B26 RID: 19238
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40034D7")]
		private long m_SimulateNextExp;

		// Token: 0x04004B27 RID: 19239
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40034D8")]
		private FriendshipGauge.eStep m_Step;

		// Token: 0x04004B28 RID: 19240
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40034D9")]
		private int m_Lv;

		// Token: 0x04004B29 RID: 19241
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40034DA")]
		private long m_Exp;

		// Token: 0x04004B2A RID: 19242
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40034DB")]
		private int m_AfterLv;

		// Token: 0x04004B2B RID: 19243
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40034DC")]
		private long m_AfterExp;

		// Token: 0x04004B2C RID: 19244
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40034DD")]
		private sbyte m_TableID;

		// Token: 0x04004B2D RID: 19245
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40034DE")]
		private long m_NextExp;

		// Token: 0x02000CCE RID: 3278
		[Token(Token = "0x20010D3")]
		private enum eStep
		{
			// Token: 0x04004B31 RID: 19249
			[Token(Token = "0x400699F")]
			None,
			// Token: 0x04004B32 RID: 19250
			[Token(Token = "0x40069A0")]
			Play
		}
	}
}
