﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DB8 RID: 3512
	[Token(Token = "0x2000969")]
	[StructLayout(3)]
	public class GachaBonusListItem : MonoBehaviour
	{
		// Token: 0x170004D4 RID: 1236
		// (get) Token: 0x060040A8 RID: 16552 RVA: 0x00019098 File Offset: 0x00017298
		// (set) Token: 0x060040A9 RID: 16553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700047F")]
		public bool hidden
		{
			[Token(Token = "0x6003B36")]
			[Address(RVA = "0x101481094", Offset = "0x1481094", VA = "0x101481094")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003B37")]
			[Address(RVA = "0x1014810D0", Offset = "0x14810D0", VA = "0x1014810D0")]
			set
			{
			}
		}

		// Token: 0x060040AA RID: 16554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B38")]
		[Address(RVA = "0x101481114", Offset = "0x1481114", VA = "0x101481114")]
		public void SetTitleData(string strTitle)
		{
		}

		// Token: 0x060040AB RID: 16555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B39")]
		[Address(RVA = "0x1014812A0", Offset = "0x14812A0", VA = "0x1014812A0")]
		public void SetStepNumData(int stepNum, bool isNowStep)
		{
		}

		// Token: 0x060040AC RID: 16556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3A")]
		[Address(RVA = "0x101481468", Offset = "0x1481468", VA = "0x101481468")]
		public void SetBonusItemData(UserItemData data)
		{
		}

		// Token: 0x060040AD RID: 16557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3B")]
		[Address(RVA = "0x1014816AC", Offset = "0x14816AC", VA = "0x1014816AC")]
		public void SetMessage(string message)
		{
		}

		// Token: 0x060040AE RID: 16558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3C")]
		[Address(RVA = "0x1014817C8", Offset = "0x14817C8", VA = "0x1014817C8")]
		public void SetEmpty()
		{
		}

		// Token: 0x060040AF RID: 16559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3D")]
		[Address(RVA = "0x101481860", Offset = "0x1481860", VA = "0x101481860")]
		public GachaBonusListItem()
		{
		}

		// Token: 0x04004FD6 RID: 20438
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003840")]
		[SerializeField]
		private GameObject m_goTitle;

		// Token: 0x04004FD7 RID: 20439
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003841")]
		[SerializeField]
		private GameObject m_goStepNum;

		// Token: 0x04004FD8 RID: 20440
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003842")]
		[SerializeField]
		private GameObject m_goItemDetail;

		// Token: 0x04004FD9 RID: 20441
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003843")]
		[SerializeField]
		private GameObject m_goItemMessage;

		// Token: 0x04004FDA RID: 20442
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003844")]
		[SerializeField]
		protected CanvasGroup m_canvasGroup;

		// Token: 0x04004FDB RID: 20443
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003845")]
		[SerializeField]
		private GachaBonusListItemTitle m_title;

		// Token: 0x04004FDC RID: 20444
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003846")]
		[SerializeField]
		private GachaBonusListItemStepNum m_stepNum;

		// Token: 0x04004FDD RID: 20445
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003847")]
		[SerializeField]
		private GachaBonusListItemItemDetail m_itemDetail;

		// Token: 0x04004FDE RID: 20446
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003848")]
		[SerializeField]
		private GachaBonusListItemMessage m_Message;
	}
}
