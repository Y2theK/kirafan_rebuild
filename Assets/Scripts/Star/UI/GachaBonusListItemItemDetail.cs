﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB6 RID: 3510
	[Token(Token = "0x2000967")]
	[Serializable]
	[StructLayout(3)]
	internal sealed class GachaBonusListItemItemDetail
	{
		// Token: 0x060040A4 RID: 16548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B32")]
		[Address(RVA = "0x101481508", Offset = "0x1481508", VA = "0x101481508")]
		public void SetData(UserItemData itemData)
		{
		}

		// Token: 0x060040A5 RID: 16549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B33")]
		[Address(RVA = "0x101481868", Offset = "0x1481868", VA = "0x101481868")]
		public GachaBonusListItemItemDetail()
		{
		}

		// Token: 0x04004FD2 RID: 20434
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400383C")]
		[SerializeField]
		private VariableIconWithFrame icon;

		// Token: 0x04004FD3 RID: 20435
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400383D")]
		[SerializeField]
		private Text textName;

		// Token: 0x04004FD4 RID: 20436
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400383E")]
		[SerializeField]
		private Text textNum;
	}
}
