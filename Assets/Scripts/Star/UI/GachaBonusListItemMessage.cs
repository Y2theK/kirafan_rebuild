﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB7 RID: 3511
	[Token(Token = "0x2000968")]
	[Serializable]
	[StructLayout(3)]
	internal sealed class GachaBonusListItemMessage
	{
		// Token: 0x060040A6 RID: 16550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B34")]
		[Address(RVA = "0x101481788", Offset = "0x1481788", VA = "0x101481788")]
		public void SetData(string message)
		{
		}

		// Token: 0x060040A7 RID: 16551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B35")]
		[Address(RVA = "0x101481870", Offset = "0x1481870", VA = "0x101481870")]
		public GachaBonusListItemMessage()
		{
		}

		// Token: 0x04004FD5 RID: 20437
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400383F")]
		[SerializeField]
		private Text textMessage;
	}
}
