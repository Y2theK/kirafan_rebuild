﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB5 RID: 3509
	[Token(Token = "0x2000966")]
	[Serializable]
	[StructLayout(3)]
	internal sealed class GachaBonusListItemStepNum
	{
		// Token: 0x060040A2 RID: 16546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B30")]
		[Address(RVA = "0x101481348", Offset = "0x1481348", VA = "0x101481348")]
		public void SetData(int stepNum, bool isNowStep)
		{
		}

		// Token: 0x060040A3 RID: 16547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B31")]
		[Address(RVA = "0x101481878", Offset = "0x1481878", VA = "0x101481878")]
		public GachaBonusListItemStepNum()
		{
		}

		// Token: 0x04004FCE RID: 20430
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003838")]
		[SerializeField]
		private ImageNumbers goNumbers;

		// Token: 0x04004FCF RID: 20431
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003839")]
		[SerializeField]
		private Image goBalloon;

		// Token: 0x04004FD0 RID: 20432
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400383A")]
		[SerializeField]
		private RectTransform trfNum10;

		// Token: 0x04004FD1 RID: 20433
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400383B")]
		[SerializeField]
		private Transform trfBase;
	}
}
