﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB4 RID: 3508
	[Token(Token = "0x2000965")]
	[Serializable]
	[StructLayout(3)]
	internal sealed class GachaBonusListItemTitle
	{
		// Token: 0x060040A0 RID: 16544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B2E")]
		[Address(RVA = "0x1014811D4", Offset = "0x14811D4", VA = "0x1014811D4")]
		public void SetData(string title)
		{
		}

		// Token: 0x060040A1 RID: 16545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B2F")]
		[Address(RVA = "0x101481880", Offset = "0x1481880", VA = "0x101481880")]
		public GachaBonusListItemTitle()
		{
		}

		// Token: 0x04004FCD RID: 20429
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003837")]
		[SerializeField]
		private Text textTitle;
	}
}
