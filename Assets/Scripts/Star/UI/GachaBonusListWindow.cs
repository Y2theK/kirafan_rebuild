﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DB9 RID: 3513
	[Token(Token = "0x200096A")]
	[StructLayout(3)]
	public class GachaBonusListWindow : UIGroup
	{
		// Token: 0x060040B0 RID: 16560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3E")]
		[Address(RVA = "0x101481888", Offset = "0x1481888", VA = "0x101481888", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x060040B1 RID: 16561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B3F")]
		[Address(RVA = "0x101481890", Offset = "0x1481890", VA = "0x101481890")]
		public void Open(Gacha.GachaData data)
		{
		}

		// Token: 0x060040B2 RID: 16562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B40")]
		[Address(RVA = "0x101481E50", Offset = "0x1481E50", VA = "0x101481E50")]
		private void CreateListItem_Title(string titleName)
		{
		}

		// Token: 0x060040B3 RID: 16563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B41")]
		[Address(RVA = "0x1014820D4", Offset = "0x14820D4", VA = "0x1014820D4")]
		private void CreateListItem_StepCount(int stepNum, bool isNowStep)
		{
		}

		// Token: 0x060040B4 RID: 16564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B42")]
		[Address(RVA = "0x101482390", Offset = "0x1482390", VA = "0x101482390")]
		private void CreateListItem_BonusDetail(UserItemData data)
		{
		}

		// Token: 0x060040B5 RID: 16565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B43")]
		[Address(RVA = "0x101482614", Offset = "0x1482614", VA = "0x101482614")]
		private void CreateListItem_Message(string message)
		{
		}

		// Token: 0x060040B6 RID: 16566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B44")]
		[Address(RVA = "0x101482898", Offset = "0x1482898", VA = "0x101482898")]
		private void CreateListItem_Empty()
		{
		}

		// Token: 0x060040B7 RID: 16567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B45")]
		[Address(RVA = "0x101482B08", Offset = "0x1482B08", VA = "0x101482B08", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060040B8 RID: 16568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B46")]
		[Address(RVA = "0x101482E08", Offset = "0x1482E08", VA = "0x101482E08")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040B9 RID: 16569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B47")]
		[Address(RVA = "0x101481D10", Offset = "0x1481D10", VA = "0x101481D10")]
		private void ListItemClear()
		{
		}

		// Token: 0x060040BA RID: 16570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B48")]
		[Address(RVA = "0x101482C78", Offset = "0x1482C78", VA = "0x101482C78")]
		protected void OptimizeInfo(GachaBonusListItem listItem, RectTransform rtfPanel)
		{
		}

		// Token: 0x060040BB RID: 16571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B49")]
		[Address(RVA = "0x101482E14", Offset = "0x1482E14", VA = "0x101482E14")]
		public GachaBonusListWindow()
		{
		}

		// Token: 0x04004FDF RID: 20447
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003849")]
		[SerializeField]
		private Transform m_ListParent;

		// Token: 0x04004FE0 RID: 20448
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400384A")]
		[SerializeField]
		private GameObject m_ListItemPrefab;

		// Token: 0x04004FE1 RID: 20449
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400384B")]
		[SerializeField]
		private RectTransform m_rtfListMask;

		// Token: 0x04004FE2 RID: 20450
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400384C")]
		[SerializeField]
		private RectTransform m_rtfScrollRect;

		// Token: 0x04004FE3 RID: 20451
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400384D")]
		[SerializeField]
		private RectTransform m_rtfContents;

		// Token: 0x04004FE4 RID: 20452
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400384E")]
		[SerializeField]
		private GridLayoutGroup m_glgContents;

		// Token: 0x04004FE5 RID: 20453
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400384F")]
		private Dictionary<GachaBonusListItem, RectTransform> m_dicListItem;

		// Token: 0x04004FE6 RID: 20454
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003850")]
		private int m_listStartIndex;
	}
}
