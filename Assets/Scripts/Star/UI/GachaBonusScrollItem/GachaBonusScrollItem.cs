﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaBonusScrollItem
{
	// Token: 0x02000FDE RID: 4062
	[Token(Token = "0x2000A90")]
	[StructLayout(3)]
	public class GachaBonusScrollItem : ScrollItemIcon
	{
		// Token: 0x06004D69 RID: 19817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046FE")]
		[Address(RVA = "0x101482E8C", Offset = "0x1482E8C", VA = "0x101482E8C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004D6A RID: 19818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046FF")]
		[Address(RVA = "0x101482F88", Offset = "0x1482F88", VA = "0x101482F88")]
		public void Apply(int itemID, int num)
		{
		}

		// Token: 0x06004D6B RID: 19819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004700")]
		[Address(RVA = "0x101483128", Offset = "0x1483128", VA = "0x101483128")]
		public GachaBonusScrollItem()
		{
		}

		// Token: 0x04005F6F RID: 24431
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40042EC")]
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04005F70 RID: 24432
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40042ED")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04005F71 RID: 24433
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40042EE")]
		public Action<int> OnClick;
	}
}
