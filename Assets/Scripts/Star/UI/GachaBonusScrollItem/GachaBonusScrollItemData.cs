﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaBonusScrollItem
{
	// Token: 0x02000FDD RID: 4061
	[Token(Token = "0x2000A8F")]
	[StructLayout(3)]
	public class GachaBonusScrollItemData : ScrollItemData
	{
		// Token: 0x06004D67 RID: 19815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046FC")]
		[Address(RVA = "0x101483130", Offset = "0x1483130", VA = "0x101483130")]
		public GachaBonusScrollItemData(int itemID, int num)
		{
		}

		// Token: 0x06004D68 RID: 19816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046FD")]
		[Address(RVA = "0x101483168", Offset = "0x1483168", VA = "0x101483168", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005F6D RID: 24429
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40042EA")]
		public int m_ItemID;

		// Token: 0x04005F6E RID: 24430
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40042EB")]
		public int m_Num;
	}
}
