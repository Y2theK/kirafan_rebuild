﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DBA RID: 3514
	[Token(Token = "0x200096B")]
	[StructLayout(3)]
	public class GachaDrawPointConfirmWindow : UIGroup
	{
		// Token: 0x170004D5 RID: 1237
		// (get) Token: 0x060040BC RID: 16572 RVA: 0x000190B0 File Offset: 0x000172B0
		[Token(Token = "0x17000480")]
		public int SelectCharaID
		{
			[Token(Token = "0x6003B4A")]
			[Address(RVA = "0x1014831F8", Offset = "0x14831F8", VA = "0x1014831F8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060040BD RID: 16573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B4B")]
		[Address(RVA = "0x101483200", Offset = "0x1483200", VA = "0x101483200")]
		public void Setup(Gacha.DrawPointReward chara, int currentPoint, Action onClickOK)
		{
		}

		// Token: 0x060040BE RID: 16574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B4C")]
		[Address(RVA = "0x1014838AC", Offset = "0x14838AC", VA = "0x1014838AC")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040BF RID: 16575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B4D")]
		[Address(RVA = "0x1014838B8", Offset = "0x14838B8", VA = "0x1014838B8")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060040C0 RID: 16576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B4E")]
		[Address(RVA = "0x1014838E8", Offset = "0x14838E8", VA = "0x1014838E8")]
		public GachaDrawPointConfirmWindow()
		{
		}

		// Token: 0x04004FE7 RID: 20455
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003851")]
		[SerializeField]
		private int WINDOW_HEIGHT_WARNING;

		// Token: 0x04004FE8 RID: 20456
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003852")]
		[SerializeField]
		private int WINDOW_HEIGHT_NOWARNING;

		// Token: 0x04004FE9 RID: 20457
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003853")]
		[SerializeField]
		private RectTransform m_Anim;

		// Token: 0x04004FEA RID: 20458
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003854")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04004FEB RID: 20459
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003855")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04004FEC RID: 20460
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003856")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004FED RID: 20461
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003857")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x04004FEE RID: 20462
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003858")]
		[SerializeField]
		private ColorGroup m_CharaIconColorGroup;

		// Token: 0x04004FEF RID: 20463
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003859")]
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x04004FF0 RID: 20464
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400385A")]
		[SerializeField]
		private CharaIconFrame m_CharaIconFrame;

		// Token: 0x04004FF1 RID: 20465
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400385B")]
		[SerializeField]
		private GameObject m_PossessionObj;

		// Token: 0x04004FF2 RID: 20466
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400385C")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x04004FF3 RID: 20467
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400385D")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x04004FF4 RID: 20468
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400385E")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x04004FF5 RID: 20469
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400385F")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04004FF6 RID: 20470
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003860")]
		private Action m_OnClickOK;

		// Token: 0x04004FF7 RID: 20471
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003861")]
		private int m_SelectCharaID;
	}
}
