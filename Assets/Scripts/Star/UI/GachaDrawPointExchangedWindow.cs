﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DBB RID: 3515
	[Token(Token = "0x200096C")]
	[StructLayout(3)]
	public class GachaDrawPointExchangedWindow : UIGroup
	{
		// Token: 0x060040C1 RID: 16577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B4F")]
		[Address(RVA = "0x1014838F8", Offset = "0x14838F8", VA = "0x1014838F8")]
		public void Setup(int drawPt, int itemNum)
		{
		}

		// Token: 0x060040C2 RID: 16578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B50")]
		[Address(RVA = "0x101483E50", Offset = "0x1483E50", VA = "0x101483E50")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040C3 RID: 16579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B51")]
		[Address(RVA = "0x101483E80", Offset = "0x1483E80", VA = "0x101483E80")]
		public GachaDrawPointExchangedWindow()
		{
		}

		// Token: 0x04004FF8 RID: 20472
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003862")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04004FF9 RID: 20473
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003863")]
		[SerializeField]
		private Text[] m_Message;

		// Token: 0x04004FFA RID: 20474
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003864")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x04004FFB RID: 20475
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003865")]
		[SerializeField]
		private Text m_CloseButtonText;

		// Token: 0x04004FFC RID: 20476
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003866")]
		public Action OnClickClose;
	}
}
