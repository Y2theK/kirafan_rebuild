﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DBC RID: 3516
	[Token(Token = "0x200096D")]
	[StructLayout(3)]
	public class GachaDrawPointSelectScrollItem : ScrollItemIcon
	{
		// Token: 0x060040C4 RID: 16580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B52")]
		[Address(RVA = "0x101483E88", Offset = "0x1483E88", VA = "0x101483E88", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060040C5 RID: 16581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B53")]
		[Address(RVA = "0x1014841AC", Offset = "0x14841AC", VA = "0x1014841AC")]
		public void RefreshCurrent()
		{
		}

		// Token: 0x060040C6 RID: 16582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B54")]
		[Address(RVA = "0x101484370", Offset = "0x1484370", VA = "0x101484370")]
		public void OnClick()
		{
		}

		// Token: 0x060040C7 RID: 16583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B55")]
		[Address(RVA = "0x10148446C", Offset = "0x148446C", VA = "0x10148446C")]
		public void OnHold()
		{
		}

		// Token: 0x060040C8 RID: 16584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B56")]
		[Address(RVA = "0x101484568", Offset = "0x1484568", VA = "0x101484568")]
		public GachaDrawPointSelectScrollItem()
		{
		}

		// Token: 0x04004FFD RID: 20477
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003867")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04004FFE RID: 20478
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003868")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04004FFF RID: 20479
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003869")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001268F0", Offset = "0x1268F0")]
		private GameObject m_PossessionObj;

		// Token: 0x04005000 RID: 20480
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400386A")]
		[SerializeField]
		private GameObject m_Cursor;

		// Token: 0x04005001 RID: 20481
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400386B")]
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x04005002 RID: 20482
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400386C")]
		[SerializeField]
		private Text m_ExchangeText;
	}
}
