﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DBD RID: 3517
	[Token(Token = "0x200096E")]
	[StructLayout(3)]
	public class GachaDrawPointSelectScrollItemData : ScrollItemData
	{
		// Token: 0x170004D6 RID: 1238
		// (get) Token: 0x060040C9 RID: 16585 RVA: 0x000190C8 File Offset: 0x000172C8
		[Token(Token = "0x17000481")]
		public bool IsCurrent
		{
			[Token(Token = "0x6003B57")]
			[Address(RVA = "0x101484368", Offset = "0x1484368", VA = "0x101484368")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060040CA RID: 16586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B58")]
		[Address(RVA = "0x101484570", Offset = "0x1484570", VA = "0x101484570")]
		public GachaDrawPointSelectScrollItemData(Gacha.DrawPointReward chara, Action<int, int> onClick, Action<int> onHold)
		{
		}

		// Token: 0x060040CB RID: 16587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B59")]
		[Address(RVA = "0x101484610", Offset = "0x1484610", VA = "0x101484610")]
		public void SetCurrent(bool isCurrent)
		{
		}

		// Token: 0x04005003 RID: 20483
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400386D")]
		public int m_CharaID;

		// Token: 0x04005004 RID: 20484
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400386E")]
		public int m_ExchangePoint;

		// Token: 0x04005005 RID: 20485
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400386F")]
		public bool m_Possession;

		// Token: 0x04005006 RID: 20486
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003870")]
		public Action<int, int> m_OnClick;

		// Token: 0x04005007 RID: 20487
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003871")]
		public Action<int> m_OnHold;

		// Token: 0x04005008 RID: 20488
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003872")]
		private bool m_IsCurrent;
	}
}
