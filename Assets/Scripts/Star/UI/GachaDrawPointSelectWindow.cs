﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaSelect;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DBF RID: 3519
	[Token(Token = "0x2000970")]
	[StructLayout(3)]
	public class GachaDrawPointSelectWindow : UIGroup
	{
		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x060040CE RID: 16590 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000482")]
		public Gacha.DrawPointReward SelectChara
		{
			[Token(Token = "0x6003B5C")]
			[Address(RVA = "0x10148481C", Offset = "0x148481C", VA = "0x10148481C")]
			get
			{
				return null;
			}
		}

		// Token: 0x060040CF RID: 16591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B5D")]
		[Address(RVA = "0x101484824", Offset = "0x1484824", VA = "0x101484824")]
		public void Setup()
		{
		}

		// Token: 0x060040D0 RID: 16592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B5E")]
		[Address(RVA = "0x101484858", Offset = "0x1484858", VA = "0x101484858")]
		public void SetupParam(GachaSelectUI.UIGachaData uiGachaData, int drawPoint, Action OnDecideCharacter)
		{
		}

		// Token: 0x060040D1 RID: 16593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B5F")]
		[Address(RVA = "0x101485238", Offset = "0x1485238", VA = "0x101485238", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060040D2 RID: 16594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B60")]
		[Address(RVA = "0x101485284", Offset = "0x1485284", VA = "0x101485284")]
		private void ApplyButton(bool flg)
		{
		}

		// Token: 0x060040D3 RID: 16595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B61")]
		[Address(RVA = "0x101485300", Offset = "0x1485300", VA = "0x101485300")]
		private void OnClickCharaCallBack(int charaId, int point)
		{
		}

		// Token: 0x060040D4 RID: 16596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B62")]
		[Address(RVA = "0x1014853AC", Offset = "0x14853AC", VA = "0x1014853AC")]
		private void OnHoldCharaCallBack(int charaID)
		{
		}

		// Token: 0x060040D5 RID: 16597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B63")]
		[Address(RVA = "0x1014854C0", Offset = "0x14854C0", VA = "0x1014854C0")]
		public void OnCloseCharaDetail()
		{
		}

		// Token: 0x060040D6 RID: 16598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B64")]
		[Address(RVA = "0x101485558", Offset = "0x1485558", VA = "0x101485558")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040D7 RID: 16599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B65")]
		[Address(RVA = "0x10148558C", Offset = "0x148558C", VA = "0x10148558C")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060040D8 RID: 16600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B66")]
		[Address(RVA = "0x101485598", Offset = "0x1485598", VA = "0x101485598")]
		public GachaDrawPointSelectWindow()
		{
		}

		// Token: 0x04005009 RID: 20489
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003873")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x0400500A RID: 20490
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003874")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x0400500B RID: 20491
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003875")]
		[SerializeField]
		private Text m_PeriodText;

		// Token: 0x0400500C RID: 20492
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003876")]
		[SerializeField]
		private Text m_NotEnoughText;

		// Token: 0x0400500D RID: 20493
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003877")]
		[SerializeField]
		private GachaDrawPointSelectScrollView m_Scroll;

		// Token: 0x0400500E RID: 20494
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003878")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x0400500F RID: 20495
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003879")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x04005010 RID: 20496
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400387A")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x04005011 RID: 20497
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400387B")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04005012 RID: 20498
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400387C")]
		private Action m_OnDecideCharacter;

		// Token: 0x04005013 RID: 20499
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400387D")]
		private int m_PlayerDrawPoint;

		// Token: 0x04005014 RID: 20500
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400387E")]
		private Gacha.DrawPointReward m_SelectChara;

		// Token: 0x04005015 RID: 20501
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400387F")]
		public Action OnHoldChara;

		// Token: 0x04005016 RID: 20502
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003880")]
		public Action OnClickClose;
	}
}
