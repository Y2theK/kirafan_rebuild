﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FDF RID: 4063
	[Token(Token = "0x2000A91")]
	[StructLayout(3)]
	public class GachaAppearCutIn : MonoBehaviour
	{
		// Token: 0x06004D6C RID: 19820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004701")]
		[Address(RVA = "0x101485600", Offset = "0x1485600", VA = "0x101485600")]
		private void Update()
		{
		}

		// Token: 0x06004D6D RID: 19821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004702")]
		[Address(RVA = "0x10148587C", Offset = "0x148587C", VA = "0x10148587C")]
		private void StartIllustMove()
		{
		}

		// Token: 0x06004D6E RID: 19822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004703")]
		[Address(RVA = "0x1014859AC", Offset = "0x14859AC", VA = "0x1014859AC")]
		private void UpdateIllustMove()
		{
		}

		// Token: 0x06004D6F RID: 19823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004704")]
		[Address(RVA = "0x101485AD0", Offset = "0x1485AD0", VA = "0x101485AD0")]
		public void Prepare(int charaID, float[] illustPosOffsets)
		{
		}

		// Token: 0x06004D70 RID: 19824 RVA: 0x0001B4F8 File Offset: 0x000196F8
		[Token(Token = "0x6004705")]
		[Address(RVA = "0x101485C4C", Offset = "0x1485C4C", VA = "0x101485C4C")]
		public bool IsDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06004D71 RID: 19825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004706")]
		[Address(RVA = "0x101485C54", Offset = "0x1485C54", VA = "0x101485C54")]
		public void Destroy()
		{
		}

		// Token: 0x06004D72 RID: 19826 RVA: 0x0001B510 File Offset: 0x00019710
		[Token(Token = "0x6004707")]
		[Address(RVA = "0x101485CB8", Offset = "0x1485CB8", VA = "0x101485CB8")]
		public bool Play()
		{
			return default(bool);
		}

		// Token: 0x06004D73 RID: 19827 RVA: 0x0001B528 File Offset: 0x00019728
		[Token(Token = "0x6004708")]
		[Address(RVA = "0x101485CD4", Offset = "0x1485CD4", VA = "0x101485CD4")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06004D74 RID: 19828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004709")]
		[Address(RVA = "0x101485CE8", Offset = "0x1485CE8", VA = "0x101485CE8")]
		public GachaAppearCutIn()
		{
		}

		// Token: 0x04005F72 RID: 24434
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40042EF")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04005F73 RID: 24435
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40042F0")]
		[SerializeField]
		private float m_Speed;

		// Token: 0x04005F74 RID: 24436
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40042F1")]
		[SerializeField]
		private float m_DisplayCutSec;

		// Token: 0x04005F75 RID: 24437
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40042F2")]
		[SerializeField]
		private float m_FadeOutSec;

		// Token: 0x04005F76 RID: 24438
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40042F3")]
		[SerializeField]
		private float m_FadeWaitSec;

		// Token: 0x04005F77 RID: 24439
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40042F4")]
		[SerializeField]
		private float m_FadeInSec;

		// Token: 0x04005F78 RID: 24440
		[Token(Token = "0x40042F5")]
		private const int CUT_NUM = 2;

		// Token: 0x04005F79 RID: 24441
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40042F6")]
		private float[] DEFAULT_ILLUST_POS_OFFSETS;

		// Token: 0x04005F7A RID: 24442
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40042F7")]
		private GachaAppearCutIn.eStep m_Step;

		// Token: 0x04005F7B RID: 24443
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40042F8")]
		private bool m_IsDonePrepare;

		// Token: 0x04005F7C RID: 24444
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40042F9")]
		private float m_t;

		// Token: 0x04005F7D RID: 24445
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40042FA")]
		private float[] m_IllustPosOffsets;

		// Token: 0x04005F7E RID: 24446
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40042FB")]
		private int m_CutInIndex;

		// Token: 0x02000FE0 RID: 4064
		[Token(Token = "0x2001212")]
		private enum eStep
		{
			// Token: 0x04005F80 RID: 24448
			[Token(Token = "0x4006FC7")]
			None = -1,
			// Token: 0x04005F81 RID: 24449
			[Token(Token = "0x4006FC8")]
			Preparing,
			// Token: 0x04005F82 RID: 24450
			[Token(Token = "0x4006FC9")]
			Playing_0,
			// Token: 0x04005F83 RID: 24451
			[Token(Token = "0x4006FCA")]
			Playing_1,
			// Token: 0x04005F84 RID: 24452
			[Token(Token = "0x4006FCB")]
			Playing_2
		}
	}
}
