﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FE1 RID: 4065
	[Token(Token = "0x2000A92")]
	[StructLayout(3)]
	public class GachaCharaFlavor : MonoBehaviour
	{
		// Token: 0x140000F1 RID: 241
		// (add) Token: 0x06004D75 RID: 19829 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D76 RID: 19830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F1")]
		public event Action OnEnd
		{
			[Token(Token = "0x600470A")]
			[Address(RVA = "0x101485DC0", Offset = "0x1485DC0", VA = "0x101485DC0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600470B")]
			[Address(RVA = "0x101485ECC", Offset = "0x1485ECC", VA = "0x101485ECC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004D77 RID: 19831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600470C")]
		[Address(RVA = "0x101485FD8", Offset = "0x1485FD8", VA = "0x101485FD8")]
		public void SetCharaIDs(int[] charaIDs)
		{
		}

		// Token: 0x06004D78 RID: 19832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600470D")]
		[Address(RVA = "0x101485FE0", Offset = "0x1485FE0", VA = "0x101485FE0")]
		public void Open()
		{
		}

		// Token: 0x06004D79 RID: 19833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600470E")]
		[Address(RVA = "0x1014860F8", Offset = "0x14860F8", VA = "0x1014860F8")]
		public void Update()
		{
		}

		// Token: 0x06004D7A RID: 19834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600470F")]
		[Address(RVA = "0x101486580", Offset = "0x1486580", VA = "0x101486580")]
		public void Destroy()
		{
		}

		// Token: 0x06004D7B RID: 19835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004710")]
		[Address(RVA = "0x101486634", Offset = "0x1486634", VA = "0x101486634")]
		public void Close()
		{
		}

		// Token: 0x06004D7C RID: 19836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004711")]
		[Address(RVA = "0x10148600C", Offset = "0x148600C", VA = "0x10148600C")]
		private void LoadChara()
		{
		}

		// Token: 0x06004D7D RID: 19837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004712")]
		[Address(RVA = "0x101486668", Offset = "0x1486668", VA = "0x101486668")]
		public void GoNext()
		{
		}

		// Token: 0x06004D7E RID: 19838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004713")]
		[Address(RVA = "0x10148674C", Offset = "0x148674C", VA = "0x10148674C")]
		public GachaCharaFlavor()
		{
		}

		// Token: 0x04005F85 RID: 24453
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40042FC")]
		private int[] m_CharaIDs;

		// Token: 0x04005F86 RID: 24454
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40042FD")]
		private int m_CurrentCharaIdx;

		// Token: 0x04005F87 RID: 24455
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40042FE")]
		private bool m_IsEndVoice;

		// Token: 0x04005F88 RID: 24456
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40042FF")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04005F89 RID: 24457
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004300")]
		[SerializeField]
		private AnimUIPlayer m_CharaIllustAnim;

		// Token: 0x04005F8A RID: 24458
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004301")]
		[SerializeField]
		private GachaCharaFlavorTalkWindowGroup m_TalkWindowGroup;

		// Token: 0x04005F8C RID: 24460
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004303")]
		private GachaCharaFlavor.eStep m_Step;

		// Token: 0x04005F8D RID: 24461
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004304")]
		private int m_RequestVoiceUniqueID;

		// Token: 0x02000FE2 RID: 4066
		[Token(Token = "0x2001213")]
		private enum eStep
		{
			// Token: 0x04005F8F RID: 24463
			[Token(Token = "0x4006FCD")]
			None,
			// Token: 0x04005F90 RID: 24464
			[Token(Token = "0x4006FCE")]
			Prepare,
			// Token: 0x04005F91 RID: 24465
			[Token(Token = "0x4006FCF")]
			CharaIn,
			// Token: 0x04005F92 RID: 24466
			[Token(Token = "0x4006FD0")]
			CharaInWait,
			// Token: 0x04005F93 RID: 24467
			[Token(Token = "0x4006FD1")]
			Talk,
			// Token: 0x04005F94 RID: 24468
			[Token(Token = "0x4006FD2")]
			CharaOut,
			// Token: 0x04005F95 RID: 24469
			[Token(Token = "0x4006FD3")]
			CharaOutWait,
			// Token: 0x04005F96 RID: 24470
			[Token(Token = "0x4006FD4")]
			End
		}
	}
}
