﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.ADV;
using Star.UI.ADV;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FE3 RID: 4067
	[Token(Token = "0x2000A93")]
	[StructLayout(3)]
	public class GachaCharaFlavorTalkWindowGroup : UIGroup
	{
		// Token: 0x06004D7F RID: 19839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004714")]
		[Address(RVA = "0x101486514", Offset = "0x1486514", VA = "0x101486514")]
		public void SetCharaID(int charaID)
		{
		}

		// Token: 0x06004D80 RID: 19840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004715")]
		[Address(RVA = "0x101486754", Offset = "0x1486754", VA = "0x101486754", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004D81 RID: 19841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004716")]
		[Address(RVA = "0x101486630", Offset = "0x1486630", VA = "0x101486630")]
		public void Destroy()
		{
		}

		// Token: 0x06004D82 RID: 19842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004717")]
		[Address(RVA = "0x10148654C", Offset = "0x148654C", VA = "0x10148654C")]
		public void EndVoice()
		{
		}

		// Token: 0x06004D83 RID: 19843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004718")]
		[Address(RVA = "0x1014866B0", Offset = "0x14866B0", VA = "0x1014866B0")]
		public void GoNext()
		{
		}

		// Token: 0x06004D84 RID: 19844 RVA: 0x0001B540 File Offset: 0x00019740
		[Token(Token = "0x6004719")]
		[Address(RVA = "0x10148651C", Offset = "0x148651C", VA = "0x10148651C")]
		public ADVTalkCustomWindow.eTextState GetTalkWindowTextState()
		{
			return ADVTalkCustomWindow.eTextState.None;
		}

		// Token: 0x06004D85 RID: 19845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471A")]
		[Address(RVA = "0x101486A8C", Offset = "0x1486A8C", VA = "0x101486A8C")]
		public GachaCharaFlavorTalkWindowGroup()
		{
		}

		// Token: 0x04005F97 RID: 24471
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004305")]
		private ADVTalkCustomWindow m_TalkWindow;

		// Token: 0x04005F98 RID: 24472
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004306")]
		private ADVParser m_Parser;

		// Token: 0x04005F99 RID: 24473
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004307")]
		private int m_CharaId;
	}
}
