﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FE4 RID: 4068
	[Token(Token = "0x2000A94")]
	[StructLayout(3)]
	public class GachaCutIn : UIGroup
	{
		// Token: 0x06004D86 RID: 19846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471B")]
		[Address(RVA = "0x101486AF4", Offset = "0x1486AF4", VA = "0x101486AF4")]
		public void Open(string message, string cueSheet, string cueName)
		{
		}

		// Token: 0x06004D87 RID: 19847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471C")]
		[Address(RVA = "0x101486BC8", Offset = "0x1486BC8", VA = "0x101486BC8")]
		public void OpenMessageOnly(string message, bool isAutoClose = true)
		{
		}

		// Token: 0x06004D88 RID: 19848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471D")]
		[Address(RVA = "0x101486EE0", Offset = "0x1486EE0", VA = "0x101486EE0", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004D89 RID: 19849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471E")]
		[Address(RVA = "0x101486F70", Offset = "0x1486F70", VA = "0x101486F70")]
		public void Destroy()
		{
		}

		// Token: 0x06004D8A RID: 19850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600471F")]
		[Address(RVA = "0x101486FD0", Offset = "0x1486FD0", VA = "0x101486FD0", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004D8B RID: 19851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004720")]
		[Address(RVA = "0x101487104", Offset = "0x1487104", VA = "0x101487104")]
		public void SetTalker(int charaId)
		{
		}

		// Token: 0x06004D8C RID: 19852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004721")]
		[Address(RVA = "0x1014871B4", Offset = "0x14871B4", VA = "0x1014871B4")]
		public void SetTalker(string name)
		{
		}

		// Token: 0x06004D8D RID: 19853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004722")]
		[Address(RVA = "0x101487264", Offset = "0x1487264", VA = "0x101487264")]
		public GachaCutIn()
		{
		}

		// Token: 0x04005F9A RID: 24474
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004308")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012CCE8", Offset = "0x12CCE8")]
		private Text m_Text;

		// Token: 0x04005F9B RID: 24475
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004309")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012CD34", Offset = "0x12CD34")]
		private float m_DisplayTimeNoVoice;

		// Token: 0x04005F9C RID: 24476
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x400430A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012CD80", Offset = "0x12CD80")]
		private float m_DisplayTimeAfterPlayedVoice;

		// Token: 0x04005F9D RID: 24477
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400430B")]
		private float m_TimeCount;

		// Token: 0x04005F9E RID: 24478
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x400430C")]
		private bool m_IsAutoClose;

		// Token: 0x04005F9F RID: 24479
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400430D")]
		private ADVTalkCustomWindow m_TalkWindow;

		// Token: 0x04005FA0 RID: 24480
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400430E")]
		[SerializeField]
		private bool m_SetTalkWindowDefaultFontSize;

		// Token: 0x04005FA1 RID: 24481
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x400430F")]
		[SerializeField]
		private int m_TalkWindowDefaultFontSize;

		// Token: 0x04005FA2 RID: 24482
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004310")]
		[SerializeField]
		private bool m_SetTalkWindowTextSizeDeltaY;

		// Token: 0x04005FA3 RID: 24483
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4004311")]
		[SerializeField]
		private float m_TalkWindowTextSizeDeltaY;

		// Token: 0x04005FA4 RID: 24484
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004312")]
		[SerializeField]
		private bool m_SetTalkWindowTextLocalPositionY;

		// Token: 0x04005FA5 RID: 24485
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4004313")]
		[SerializeField]
		private float m_TalkWindowTextLocalPositionY;

		// Token: 0x04005FA6 RID: 24486
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004314")]
		[SerializeField]
		private bool m_SetTalkWindowTextLineSpacing;

		// Token: 0x04005FA7 RID: 24487
		[Cpp2IlInjected.FieldOffset(Offset = "0xC4")]
		[Token(Token = "0x4004315")]
		[SerializeField]
		private float m_TalkWindowTextLineSpacing;

		// Token: 0x04005FA8 RID: 24488
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004316")]
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x04005FA9 RID: 24489
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004317")]
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x04005FAA RID: 24490
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004318")]
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x04005FAB RID: 24491
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004319")]
		private int m_VoiceCtrlReqID;
	}
}
