﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FE5 RID: 4069
	[Token(Token = "0x2000A95")]
	[StructLayout(3)]
	public class GachaItemLabel : MonoBehaviour
	{
		// Token: 0x06004D8E RID: 19854 RVA: 0x0001B558 File Offset: 0x00019758
		[Token(Token = "0x6004723")]
		[Address(RVA = "0x10148729C", Offset = "0x148729C", VA = "0x10148729C")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x06004D8F RID: 19855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004724")]
		[Address(RVA = "0x1014872AC", Offset = "0x14872AC", VA = "0x1014872AC")]
		public void Play(int itemID)
		{
		}

		// Token: 0x06004D90 RID: 19856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004725")]
		[Address(RVA = "0x101487770", Offset = "0x1487770", VA = "0x101487770")]
		public void Stop()
		{
		}

		// Token: 0x06004D91 RID: 19857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004726")]
		[Address(RVA = "0x101487810", Offset = "0x1487810", VA = "0x101487810")]
		public void Destroy()
		{
		}

		// Token: 0x06004D92 RID: 19858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004727")]
		[Address(RVA = "0x101487968", Offset = "0x1487968", VA = "0x101487968")]
		private void Update()
		{
		}

		// Token: 0x06004D93 RID: 19859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004728")]
		[Address(RVA = "0x101487C94", Offset = "0x1487C94", VA = "0x101487C94")]
		public GachaItemLabel()
		{
		}

		// Token: 0x04005FAC RID: 24492
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400431A")]
		[SerializeField]
		private Image[] m_Image;

		// Token: 0x04005FAD RID: 24493
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400431B")]
		private SpriteHandler[] m_Handler;

		// Token: 0x04005FAE RID: 24494
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400431C")]
		private GameObject m_GameObject;

		// Token: 0x04005FAF RID: 24495
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400431D")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x04005FB0 RID: 24496
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400431E")]
		private GachaItemLabel.eStep m_Step;

		// Token: 0x02000FE6 RID: 4070
		[Token(Token = "0x2001214")]
		public enum eImage
		{
			// Token: 0x04005FB2 RID: 24498
			[Token(Token = "0x4006FD6")]
			Top,
			// Token: 0x04005FB3 RID: 24499
			[Token(Token = "0x4006FD7")]
			Bottom,
			// Token: 0x04005FB4 RID: 24500
			[Token(Token = "0x4006FD8")]
			Item,
			// Token: 0x04005FB5 RID: 24501
			[Token(Token = "0x4006FD9")]
			Num
		}

		// Token: 0x02000FE7 RID: 4071
		[Token(Token = "0x2001215")]
		public enum eStep
		{
			// Token: 0x04005FB7 RID: 24503
			[Token(Token = "0x4006FDB")]
			None,
			// Token: 0x04005FB8 RID: 24504
			[Token(Token = "0x4006FDC")]
			Prepare,
			// Token: 0x04005FB9 RID: 24505
			[Token(Token = "0x4006FDD")]
			Play,
			// Token: 0x04005FBA RID: 24506
			[Token(Token = "0x4006FDE")]
			PlayWait,
			// Token: 0x04005FBB RID: 24507
			[Token(Token = "0x4006FDF")]
			Finish
		}
	}
}
