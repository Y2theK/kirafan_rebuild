﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Arousal;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FE8 RID: 4072
	[Token(Token = "0x2000A96")]
	[StructLayout(3)]
	public class GachaPlayUI : MenuUIBase
	{
		// Token: 0x140000F2 RID: 242
		// (add) Token: 0x06004D94 RID: 19860 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D95 RID: 19861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F2")]
		public event Action OnClick
		{
			[Token(Token = "0x6004729")]
			[Address(RVA = "0x101487CF4", Offset = "0x1487CF4", VA = "0x101487CF4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600472A")]
			[Address(RVA = "0x101487E00", Offset = "0x1487E00", VA = "0x101487E00")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F3 RID: 243
		// (add) Token: 0x06004D96 RID: 19862 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D97 RID: 19863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F3")]
		public event Action OnCloseWaitTouch
		{
			[Token(Token = "0x600472B")]
			[Address(RVA = "0x101487F0C", Offset = "0x1487F0C", VA = "0x101487F0C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600472C")]
			[Address(RVA = "0x101488018", Offset = "0x1488018", VA = "0x101488018")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F4 RID: 244
		// (add) Token: 0x06004D98 RID: 19864 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D99 RID: 19865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F4")]
		public event Action OnCloseCutIn
		{
			[Token(Token = "0x600472D")]
			[Address(RVA = "0x101488124", Offset = "0x1488124", VA = "0x101488124")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600472E")]
			[Address(RVA = "0x101488230", Offset = "0x1488230", VA = "0x101488230")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F5 RID: 245
		// (add) Token: 0x06004D9A RID: 19866 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D9B RID: 19867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F5")]
		public event Action OnCloseShowChara
		{
			[Token(Token = "0x600472F")]
			[Address(RVA = "0x10148833C", Offset = "0x148833C", VA = "0x10148833C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004730")]
			[Address(RVA = "0x101488448", Offset = "0x1488448", VA = "0x101488448")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F6 RID: 246
		// (add) Token: 0x06004D9C RID: 19868 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D9D RID: 19869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F6")]
		public event Action OnCloseCharaFlavors
		{
			[Token(Token = "0x6004731")]
			[Address(RVA = "0x101488554", Offset = "0x1488554", VA = "0x101488554")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004732")]
			[Address(RVA = "0x101488660", Offset = "0x1488660", VA = "0x101488660")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F7 RID: 247
		// (add) Token: 0x06004D9E RID: 19870 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D9F RID: 19871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F7")]
		public event Action OnClickCloseResult
		{
			[Token(Token = "0x6004733")]
			[Address(RVA = "0x10148876C", Offset = "0x148876C", VA = "0x10148876C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004734")]
			[Address(RVA = "0x101488878", Offset = "0x1488878", VA = "0x101488878")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F8 RID: 248
		// (add) Token: 0x06004DA0 RID: 19872 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004DA1 RID: 19873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F8")]
		public event Action OnClickRedraw
		{
			[Token(Token = "0x6004735")]
			[Address(RVA = "0x101488984", Offset = "0x1488984", VA = "0x101488984")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004736")]
			[Address(RVA = "0x101488A94", Offset = "0x1488A94", VA = "0x101488A94")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000F9 RID: 249
		// (add) Token: 0x06004DA2 RID: 19874 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004DA3 RID: 19875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F9")]
		public event Action OnCloseResult
		{
			[Token(Token = "0x6004737")]
			[Address(RVA = "0x101488BA4", Offset = "0x1488BA4", VA = "0x101488BA4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004738")]
			[Address(RVA = "0x101488CB4", Offset = "0x1488CB4", VA = "0x101488CB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004DA4 RID: 19876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004739")]
		[Address(RVA = "0x101488DC4", Offset = "0x1488DC4", VA = "0x101488DC4")]
		private void Start()
		{
		}

		// Token: 0x06004DA5 RID: 19877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600473A")]
		[Address(RVA = "0x101488EEC", Offset = "0x1488EEC", VA = "0x101488EEC")]
		private void Update()
		{
		}

		// Token: 0x06004DA6 RID: 19878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600473B")]
		[Address(RVA = "0x101488EFC", Offset = "0x1488EFC", VA = "0x101488EFC")]
		public void Setup()
		{
		}

		// Token: 0x06004DA7 RID: 19879 RVA: 0x0001B570 File Offset: 0x00019770
		[Token(Token = "0x600473C")]
		[Address(RVA = "0x1014896B8", Offset = "0x14896B8", VA = "0x1014896B8")]
		private bool ChangeState(GachaPlayUI.eUIState state)
		{
			return default(bool);
		}

		// Token: 0x06004DA8 RID: 19880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600473D")]
		[Address(RVA = "0x101489708", Offset = "0x1489708", VA = "0x101489708", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004DA9 RID: 19881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600473E")]
		[Address(RVA = "0x10148970C", Offset = "0x148970C", VA = "0x10148970C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004DAA RID: 19882 RVA: 0x0001B588 File Offset: 0x00019788
		[Token(Token = "0x600473F")]
		[Address(RVA = "0x101489710", Offset = "0x1489710", VA = "0x101489710", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004DAB RID: 19883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004740")]
		[Address(RVA = "0x10148974C", Offset = "0x148974C", VA = "0x10148974C", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004DAC RID: 19884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004741")]
		[Address(RVA = "0x10148985C", Offset = "0x148985C", VA = "0x10148985C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004DAD RID: 19885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004742")]
		[Address(RVA = "0x101489944", Offset = "0x1489944", VA = "0x101489944")]
		public void LoadCharaIllust(int charaID)
		{
		}

		// Token: 0x06004DAE RID: 19886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004743")]
		[Address(RVA = "0x101488DCC", Offset = "0x1488DCC", VA = "0x101488DCC")]
		public void SetOnClickAllSkipButtonCallback(Action callback)
		{
		}

		// Token: 0x06004DAF RID: 19887 RVA: 0x0001B5A0 File Offset: 0x000197A0
		[Token(Token = "0x6004744")]
		[Address(RVA = "0x1014899BC", Offset = "0x14899BC", VA = "0x1014899BC")]
		public bool IsLoadingCharaIllust()
		{
			return default(bool);
		}

		// Token: 0x06004DB0 RID: 19888 RVA: 0x0001B5B8 File Offset: 0x000197B8
		[Token(Token = "0x6004745")]
		[Address(RVA = "0x101489A44", Offset = "0x1489A44", VA = "0x101489A44")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06004DB1 RID: 19889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004746")]
		[Address(RVA = "0x101489A80", Offset = "0x1489A80", VA = "0x101489A80")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x06004DB2 RID: 19890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004747")]
		[Address(RVA = "0x101489B18", Offset = "0x1489B18", VA = "0x101489B18")]
		public void AbortOnAllSkip()
		{
		}

		// Token: 0x06004DB3 RID: 19891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004748")]
		[Address(RVA = "0x101489B90", Offset = "0x1489B90", VA = "0x101489B90")]
		public void PlayInNpc()
		{
		}

		// Token: 0x06004DB4 RID: 19892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004749")]
		[Address(RVA = "0x101489C08", Offset = "0x1489C08", VA = "0x101489C08")]
		public void PlayOutNpc()
		{
		}

		// Token: 0x06004DB5 RID: 19893 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600474A")]
		[Address(RVA = "0x101489C80", Offset = "0x1489C80", VA = "0x101489C80")]
		public UIGroup GetWaitTouchGroup()
		{
			return null;
		}

		// Token: 0x06004DB6 RID: 19894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600474B")]
		[Address(RVA = "0x101489C88", Offset = "0x1489C88", VA = "0x101489C88")]
		public void OpenWaitTouchGroup()
		{
		}

		// Token: 0x06004DB7 RID: 19895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600474C")]
		[Address(RVA = "0x101489E20", Offset = "0x1489E20", VA = "0x101489E20")]
		public void CloseWaitTouchGroup()
		{
		}

		// Token: 0x06004DB8 RID: 19896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600474D")]
		[Address(RVA = "0x101489E74", Offset = "0x1489E74", VA = "0x101489E74")]
		private void OnCloseWaitTouchCallBack()
		{
		}

		// Token: 0x06004DB9 RID: 19897 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600474E")]
		[Address(RVA = "0x101489EA0", Offset = "0x1489EA0", VA = "0x101489EA0")]
		public GachaCutIn GetCutInGroup()
		{
			return null;
		}

		// Token: 0x06004DBA RID: 19898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600474F")]
		[Address(RVA = "0x101489EA8", Offset = "0x1489EA8", VA = "0x101489EA8")]
		public void OpenCutIn(string text, string cueSheet, string cueName)
		{
		}

		// Token: 0x06004DBB RID: 19899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004750")]
		[Address(RVA = "0x101489F14", Offset = "0x1489F14", VA = "0x101489F14")]
		public void CloseCutIn()
		{
		}

		// Token: 0x06004DBC RID: 19900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004751")]
		[Address(RVA = "0x101489F48", Offset = "0x1489F48", VA = "0x101489F48")]
		private void OnCloseCutInCallBack()
		{
		}

		// Token: 0x06004DBD RID: 19901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004752")]
		[Address(RVA = "0x101489F74", Offset = "0x1489F74", VA = "0x101489F74")]
		public void OpenShowChara(bool highAnim)
		{
		}

		// Token: 0x06004DBE RID: 19902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004753")]
		[Address(RVA = "0x10148A07C", Offset = "0x148A07C", VA = "0x10148A07C")]
		public void OpenShowItem(int itemID)
		{
		}

		// Token: 0x06004DBF RID: 19903 RVA: 0x0001B5D0 File Offset: 0x000197D0
		[Token(Token = "0x6004754")]
		[Address(RVA = "0x10148A0DC", Offset = "0x148A0DC", VA = "0x10148A0DC")]
		public bool IsCompleteOpenShowChara()
		{
			return default(bool);
		}

		// Token: 0x06004DC0 RID: 19904 RVA: 0x0001B5E8 File Offset: 0x000197E8
		[Token(Token = "0x6004755")]
		[Address(RVA = "0x10148A10C", Offset = "0x148A10C", VA = "0x10148A10C")]
		public bool IsCompleteOpenShowItem()
		{
			return default(bool);
		}

		// Token: 0x06004DC1 RID: 19905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004756")]
		[Address(RVA = "0x10148A140", Offset = "0x148A140", VA = "0x10148A140")]
		public void CloseShowItem()
		{
		}

		// Token: 0x06004DC2 RID: 19906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004757")]
		[Address(RVA = "0x10148A1A4", Offset = "0x148A1A4", VA = "0x10148A1A4")]
		public void CloseShowChara()
		{
		}

		// Token: 0x06004DC3 RID: 19907 RVA: 0x0001B600 File Offset: 0x00019800
		[Token(Token = "0x6004758")]
		[Address(RVA = "0x10148A1D8", Offset = "0x148A1D8", VA = "0x10148A1D8")]
		public bool IsCompleteCloseShowChara()
		{
			return default(bool);
		}

		// Token: 0x06004DC4 RID: 19908 RVA: 0x0001B618 File Offset: 0x00019818
		[Token(Token = "0x6004759")]
		[Address(RVA = "0x10148A208", Offset = "0x148A208", VA = "0x10148A208")]
		public bool IsCompleteCloseShowItem()
		{
			return default(bool);
		}

		// Token: 0x06004DC5 RID: 19909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600475A")]
		[Address(RVA = "0x10148A238", Offset = "0x148A238", VA = "0x10148A238")]
		private void OnCloseShowCharaCallBack()
		{
		}

		// Token: 0x06004DC6 RID: 19910 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600475B")]
		[Address(RVA = "0x10148A264", Offset = "0x148A264", VA = "0x10148A264")]
		public GachaAppearCutIn GetAppearCutIn()
		{
			return null;
		}

		// Token: 0x06004DC7 RID: 19911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600475C")]
		[Address(RVA = "0x10148A26C", Offset = "0x148A26C", VA = "0x10148A26C")]
		private void OnClickCloseResultCallBack()
		{
		}

		// Token: 0x06004DC8 RID: 19912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600475D")]
		[Address(RVA = "0x10148A278", Offset = "0x148A278", VA = "0x10148A278")]
		private void OnClickRedrawCallBack()
		{
		}

		// Token: 0x06004DC9 RID: 19913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600475E")]
		[Address(RVA = "0x10148A284", Offset = "0x148A284", VA = "0x10148A284")]
		private void OnCloseResultCallBack()
		{
		}

		// Token: 0x06004DCA RID: 19914 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600475F")]
		[Address(RVA = "0x10148A2B0", Offset = "0x148A2B0", VA = "0x10148A2B0")]
		public void OnClickReplayYesCallBack()
		{
		}

		// Token: 0x06004DCB RID: 19915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004760")]
		[Address(RVA = "0x10148A2F4", Offset = "0x148A2F4", VA = "0x10148A2F4")]
		public void OnClickReplayNoCallBack()
		{
		}

		// Token: 0x06004DCC RID: 19916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004761")]
		[Address(RVA = "0x10148A338", Offset = "0x148A338", VA = "0x10148A338")]
		private void OnCloseCharaFlavorsCallBack()
		{
		}

		// Token: 0x06004DCD RID: 19917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004762")]
		[Address(RVA = "0x10148A364", Offset = "0x148A364", VA = "0x10148A364")]
		public void OpenReplayWindow(Action OnClickYes, Action OnClickNo)
		{
		}

		// Token: 0x06004DCE RID: 19918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004763")]
		[Address(RVA = "0x10148A39C", Offset = "0x148A39C", VA = "0x10148A39C")]
		public void OpenmBonusWindow()
		{
		}

		// Token: 0x06004DCF RID: 19919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004764")]
		[Address(RVA = "0x10148A3D0", Offset = "0x148A3D0", VA = "0x10148A3D0")]
		public void OnClickBonusClose()
		{
		}

		// Token: 0x06004DD0 RID: 19920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004765")]
		[Address(RVA = "0x10148A45C", Offset = "0x148A45C", VA = "0x10148A45C")]
		public void OpenCharaFlavors(int[] charaIDs)
		{
		}

		// Token: 0x06004DD1 RID: 19921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004766")]
		[Address(RVA = "0x10148A4C8", Offset = "0x148A4C8", VA = "0x10148A4C8")]
		public void OpenResult(List<Gacha.Result> gachaResults, List<Gacha.BonusItem> gachaBonusItems, Gacha.GachaData gachaData)
		{
		}

		// Token: 0x06004DD2 RID: 19922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004767")]
		[Address(RVA = "0x10148A428", Offset = "0x148A428", VA = "0x10148A428")]
		public void CloseResult()
		{
		}

		// Token: 0x06004DD3 RID: 19923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004768")]
		[Address(RVA = "0x10148AB84", Offset = "0x148AB84", VA = "0x10148AB84")]
		public void OpenRedrawConfirmGroup(string title, string mainText, string subText, string warningText, Action onClickDecideButtonCallback)
		{
		}

		// Token: 0x06004DD4 RID: 19924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004769")]
		[Address(RVA = "0x10148AEEC", Offset = "0x148AEEC", VA = "0x10148AEEC")]
		public void OpenRedrawConfirmGroup(string title, string mainText, string subText)
		{
		}

		// Token: 0x06004DD5 RID: 19925 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600476A")]
		[Address(RVA = "0x10148AF6C", Offset = "0x148AF6C", VA = "0x10148AF6C")]
		public ArousalPopupUI GetArousalPopupUI()
		{
			return null;
		}

		// Token: 0x06004DD6 RID: 19926 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600476B")]
		[Address(RVA = "0x10148AF74", Offset = "0x148AF74", VA = "0x10148AF74")]
		public ArousalResultUI GetArousalResultUI()
		{
			return null;
		}

		// Token: 0x06004DD7 RID: 19927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600476C")]
		[Address(RVA = "0x10148AF7C", Offset = "0x148AF7C", VA = "0x10148AF7C")]
		public GachaPlayUI()
		{
		}

		// Token: 0x04005FBC RID: 24508
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400431F")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04005FBD RID: 24509
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004320")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005FBE RID: 24510
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004321")]
		[SerializeField]
		private UIGroup m_WaitTouchGroup;

		// Token: 0x04005FBF RID: 24511
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004322")]
		[SerializeField]
		private GachaCutIn m_GachaCutIn;

		// Token: 0x04005FC0 RID: 24512
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004323")]
		[SerializeField]
		private GachaShowChara m_GachaShowChara;

		// Token: 0x04005FC1 RID: 24513
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004324")]
		[SerializeField]
		private GachaAppearCutIn m_GachaAppearCutIn;

		// Token: 0x04005FC2 RID: 24514
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004325")]
		[SerializeField]
		private UIGroup m_GachaShowItem;

		// Token: 0x04005FC3 RID: 24515
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004326")]
		[SerializeField]
		private GachaItemLabel m_GachaItemLabel;

		// Token: 0x04005FC4 RID: 24516
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004327")]
		[SerializeField]
		private GachaCharaFlavor m_CharaFlavor;

		// Token: 0x04005FC5 RID: 24517
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004328")]
		[SerializeField]
		private GachaResult m_GachaResult;

		// Token: 0x04005FC6 RID: 24518
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004329")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04005FC7 RID: 24519
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400432A")]
		[SerializeField]
		private GachaWaitTapNpc m_WaitTapNpc;

		// Token: 0x04005FC8 RID: 24520
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400432B")]
		[SerializeField]
		private UIGroup m_ReplayWindow;

		// Token: 0x04005FC9 RID: 24521
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400432C")]
		[SerializeField]
		private CustomButton m_AllSkipButton;

		// Token: 0x04005FCA RID: 24522
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400432D")]
		[SerializeField]
		private BonusWindowGroup m_BonusWindowGroup;

		// Token: 0x04005FCB RID: 24523
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400432E")]
		[SerializeField]
		private GachaRedrawConfirmGroup m_RedrawConfirmGroup;

		// Token: 0x04005FCC RID: 24524
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400432F")]
		[SerializeField]
		private ArousalPopupUI m_ArousalPopupUI;

		// Token: 0x04005FCD RID: 24525
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004330")]
		[SerializeField]
		private ArousalResultUI m_ArousalResultUI;

		// Token: 0x04005FD6 RID: 24534
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004339")]
		private Action OnClickReplayYes;

		// Token: 0x04005FD7 RID: 24535
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x400433A")]
		private Action OnClickReplayNo;

		// Token: 0x02000FE9 RID: 4073
		[Token(Token = "0x2001216")]
		public enum eUIState
		{
			// Token: 0x04005FD9 RID: 24537
			[Token(Token = "0x4006FE1")]
			End,
			// Token: 0x04005FDA RID: 24538
			[Token(Token = "0x4006FE2")]
			Idle,
			// Token: 0x04005FDB RID: 24539
			[Token(Token = "0x4006FE3")]
			WaitTouch,
			// Token: 0x04005FDC RID: 24540
			[Token(Token = "0x4006FE4")]
			CutIn,
			// Token: 0x04005FDD RID: 24541
			[Token(Token = "0x4006FE5")]
			ShowChara,
			// Token: 0x04005FDE RID: 24542
			[Token(Token = "0x4006FE6")]
			CharaFlavor,
			// Token: 0x04005FDF RID: 24543
			[Token(Token = "0x4006FE7")]
			Result,
			// Token: 0x04005FE0 RID: 24544
			[Token(Token = "0x4006FE8")]
			Num
		}
	}
}
