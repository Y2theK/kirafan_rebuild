﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FEA RID: 4074
	[Token(Token = "0x2000A97")]
	[StructLayout(3)]
	public class GachaRedrawConfirmGroup : UIGroup
	{
		// Token: 0x06004DD8 RID: 19928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600476D")]
		[Address(RVA = "0x10148AC14", Offset = "0x148AC14", VA = "0x10148AC14")]
		public void Setup(string title, string mainText, string subText, string warningText, Action onClickDecideButtonCallback)
		{
		}

		// Token: 0x06004DD9 RID: 19929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600476E")]
		[Address(RVA = "0x10148AF84", Offset = "0x148AF84", VA = "0x10148AF84")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06004DDA RID: 19930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600476F")]
		[Address(RVA = "0x10148AFB4", Offset = "0x148AFB4", VA = "0x10148AFB4")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x06004DDB RID: 19931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004770")]
		[Address(RVA = "0x10148AFC0", Offset = "0x148AFC0", VA = "0x10148AFC0")]
		public GachaRedrawConfirmGroup()
		{
		}

		// Token: 0x04005FE1 RID: 24545
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400433B")]
		[SerializeField]
		private Text m_MainText;

		// Token: 0x04005FE2 RID: 24546
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400433C")]
		[SerializeField]
		private Text m_SubText;

		// Token: 0x04005FE3 RID: 24547
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400433D")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x04005FE4 RID: 24548
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400433E")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x04005FE5 RID: 24549
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400433F")]
		[SerializeField]
		private GameObject m_CancelButton;

		// Token: 0x04005FE6 RID: 24550
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004340")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x04005FE7 RID: 24551
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004341")]
		[SerializeField]
		private float[] m_WindowHeight;

		// Token: 0x04005FE8 RID: 24552
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004342")]
		private Action m_OnClickDecideButtonCallback;
	}
}
