﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaBonusScrollItem;
using Star.UI.Town;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FEB RID: 4075
	[Token(Token = "0x2000A98")]
	[StructLayout(3)]
	public class GachaResult : UIGroup
	{
		// Token: 0x140000FA RID: 250
		// (add) Token: 0x06004DDC RID: 19932 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004DDD RID: 19933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FA")]
		public event Action OnClickClose
		{
			[Token(Token = "0x6004771")]
			[Address(RVA = "0x101489498", Offset = "0x1489498", VA = "0x101489498")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004772")]
			[Address(RVA = "0x10148B070", Offset = "0x148B070", VA = "0x10148B070")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000FB RID: 251
		// (add) Token: 0x06004DDE RID: 19934 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004DDF RID: 19935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FB")]
		public event Action OnClickRedraw
		{
			[Token(Token = "0x6004773")]
			[Address(RVA = "0x1014895A8", Offset = "0x14895A8", VA = "0x1014895A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004774")]
			[Address(RVA = "0x10148B180", Offset = "0x148B180", VA = "0x10148B180")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004DE0 RID: 19936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004775")]
		[Address(RVA = "0x1014892A8", Offset = "0x14892A8", VA = "0x1014892A8")]
		public void Setup()
		{
		}

		// Token: 0x06004DE1 RID: 19937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004776")]
		[Address(RVA = "0x10148A51C", Offset = "0x148A51C", VA = "0x10148A51C")]
		public void Open(List<Gacha.Result> gachaResults, List<Gacha.BonusItem> gachaBonusItems, Gacha.GachaData gachaData)
		{
		}

		// Token: 0x06004DE2 RID: 19938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004777")]
		[Address(RVA = "0x10148B290", Offset = "0x148B290", VA = "0x10148B290", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004DE3 RID: 19939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004778")]
		[Address(RVA = "0x101489830", Offset = "0x1489830", VA = "0x101489830")]
		public void Destroy()
		{
		}

		// Token: 0x06004DE4 RID: 19940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004779")]
		[Address(RVA = "0x10148B2DC", Offset = "0x148B2DC", VA = "0x10148B2DC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004DE5 RID: 19941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477A")]
		[Address(RVA = "0x10148C11C", Offset = "0x148C11C", VA = "0x10148C11C")]
		public void OnClickCancelButtonCallBack()
		{
		}

		// Token: 0x06004DE6 RID: 19942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477B")]
		[Address(RVA = "0x10148C128", Offset = "0x148C128", VA = "0x10148C128")]
		public void OnClickRedrawButton()
		{
		}

		// Token: 0x06004DE7 RID: 19943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477C")]
		[Address(RVA = "0x10148C134", Offset = "0x148C134", VA = "0x10148C134", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06004DE8 RID: 19944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477D")]
		[Address(RVA = "0x10148BEFC", Offset = "0x148BEFC", VA = "0x10148BEFC")]
		private void PlayResultVoice()
		{
		}

		// Token: 0x06004DE9 RID: 19945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477E")]
		[Address(RVA = "0x10148BDD0", Offset = "0x148BDD0", VA = "0x10148BDD0")]
		private void MoveResultGroupTitleBar()
		{
		}

		// Token: 0x06004DEA RID: 19946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600477F")]
		[Address(RVA = "0x10148C2E0", Offset = "0x148C2E0", VA = "0x10148C2E0")]
		private void MoveResultGroupTitleBarPart(RectTransform transform)
		{
		}

		// Token: 0x06004DEB RID: 19947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004780")]
		[Address(RVA = "0x10148C3AC", Offset = "0x148C3AC", VA = "0x10148C3AC")]
		public GachaResult()
		{
		}

		// Token: 0x04005FE9 RID: 24553
		[Token(Token = "0x4004343")]
		private const int ICON_MAX = 10;

		// Token: 0x04005FEA RID: 24554
		[Token(Token = "0x4004344")]
		private const int ITEM_ICON_MAX = 5;

		// Token: 0x04005FEB RID: 24555
		[Token(Token = "0x4004345")]
		private const float ICON_POP_SPEED = 16f;

		// Token: 0x04005FEC RID: 24556
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004346")]
		private List<Gacha.Result> m_GachaResults;

		// Token: 0x04005FED RID: 24557
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004347")]
		private List<Gacha.BonusItem> m_GachaBonusItems;

		// Token: 0x04005FEE RID: 24558
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004348")]
		[SerializeField]
		private GachaResultCharaIcon m_IconPrefab;

		// Token: 0x04005FEF RID: 24559
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004349")]
		[SerializeField]
		private GachaBonusScrollItem m_ItemIconPrefab;

		// Token: 0x04005FF0 RID: 24560
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400434A")]
		[SerializeField]
		private RectTransform m_CharaIconParent;

		// Token: 0x04005FF1 RID: 24561
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400434B")]
		[SerializeField]
		private RectTransform m_ItemIconParent;

		// Token: 0x04005FF2 RID: 24562
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400434C")]
		[SerializeField]
		private GachaCutIn m_CutIn;

		// Token: 0x04005FF3 RID: 24563
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400434D")]
		[SerializeField]
		private Text m_DrawPointTitle;

		// Token: 0x04005FF4 RID: 24564
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400434E")]
		[SerializeField]
		private TextFieldValueChange m_DrawPoint;

		// Token: 0x04005FF5 RID: 24565
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400434F")]
		[SerializeField]
		private CustomButton m_RedrawButton;

		// Token: 0x04005FF6 RID: 24566
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004350")]
		[SerializeField]
		private Text m_RedrawButtonText;

		// Token: 0x04005FF7 RID: 24567
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004351")]
		[SerializeField]
		private RectTransform m_ButtonGroupTransform;

		// Token: 0x04005FF8 RID: 24568
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004352")]
		private GachaResult.eStep m_Step;

		// Token: 0x04005FF9 RID: 24569
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004353")]
		private GachaResultCharaIcon[] m_CharaIcons;

		// Token: 0x04005FFA RID: 24570
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004354")]
		private GachaBonusScrollItem[] m_ItemIcons;

		// Token: 0x04005FFB RID: 24571
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004355")]
		private float m_ResultCharaPlayInCount;

		// Token: 0x04005FFC RID: 24572
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4004356")]
		private int m_ResultCharaPlayInIdx;

		// Token: 0x04005FFF RID: 24575
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004359")]
		private Canvas m_CanvasCache;

		// Token: 0x04006000 RID: 24576
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x400435A")]
		private float[] m_ButtonGroupTransformHeight;

		// Token: 0x02000FEC RID: 4076
		[Token(Token = "0x2001217")]
		private enum eStep
		{
			// Token: 0x04006002 RID: 24578
			[Token(Token = "0x4006FEA")]
			None,
			// Token: 0x04006003 RID: 24579
			[Token(Token = "0x4006FEB")]
			Prepare,
			// Token: 0x04006004 RID: 24580
			[Token(Token = "0x4006FEC")]
			WaitOpen,
			// Token: 0x04006005 RID: 24581
			[Token(Token = "0x4006FED")]
			Main,
			// Token: 0x04006006 RID: 24582
			[Token(Token = "0x4006FEE")]
			WaitClose,
			// Token: 0x04006007 RID: 24583
			[Token(Token = "0x4006FEF")]
			End
		}
	}
}
