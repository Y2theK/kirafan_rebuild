﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FED RID: 4077
	[Token(Token = "0x2000A99")]
	[StructLayout(3)]
	public class GachaResultCharaIcon : MonoBehaviour
	{
		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06004DEC RID: 19948 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004ED")]
		public GameObject GameObject
		{
			[Token(Token = "0x6004781")]
			[Address(RVA = "0x10148BA54", Offset = "0x148BA54", VA = "0x10148BA54")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06004DED RID: 19949 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004EE")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6004782")]
			[Address(RVA = "0x10148C48C", Offset = "0x148C48C", VA = "0x10148C48C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06004DEE RID: 19950 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004EF")]
		public CanvasGroup CanvasGroup
		{
			[Token(Token = "0x6004783")]
			[Address(RVA = "0x10148BAE4", Offset = "0x148BAE4", VA = "0x10148BAE4")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06004DEF RID: 19951 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004F0")]
		public CustomButton CustomButton
		{
			[Token(Token = "0x6004784")]
			[Address(RVA = "0x10148C524", Offset = "0x148C524", VA = "0x10148C524")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004DF0 RID: 19952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004785")]
		[Address(RVA = "0x10148BB7C", Offset = "0x148BB7C", VA = "0x10148BB7C")]
		public void Load(int charaID)
		{
		}

		// Token: 0x06004DF1 RID: 19953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004786")]
		[Address(RVA = "0x10148BC40", Offset = "0x148BC40", VA = "0x10148BC40")]
		public void SetItemID(int itemID)
		{
		}

		// Token: 0x06004DF2 RID: 19954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004787")]
		[Address(RVA = "0x10148C0C8", Offset = "0x148C0C8", VA = "0x10148C0C8")]
		public void PlayIn()
		{
		}

		// Token: 0x06004DF3 RID: 19955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004788")]
		[Address(RVA = "0x10148C248", Offset = "0x148C248", VA = "0x10148C248")]
		public void Destroy()
		{
		}

		// Token: 0x06004DF4 RID: 19956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004789")]
		[Address(RVA = "0x10148C5BC", Offset = "0x148C5BC", VA = "0x10148C5BC")]
		public void OnClickButton()
		{
		}

		// Token: 0x06004DF5 RID: 19957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600478A")]
		[Address(RVA = "0x10148C788", Offset = "0x148C788", VA = "0x10148C788")]
		public void OnCloseCharaDetail()
		{
		}

		// Token: 0x06004DF6 RID: 19958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600478B")]
		[Address(RVA = "0x10148C83C", Offset = "0x148C83C", VA = "0x10148C83C")]
		public GachaResultCharaIcon()
		{
		}

		// Token: 0x04006008 RID: 24584
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400435B")]
		[SerializeField]
		private PrefabCloner m_CharaIconWithFrameCloner;

		// Token: 0x04006009 RID: 24585
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400435C")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x0400600A RID: 24586
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400435D")]
		[SerializeField]
		private GameObject m_ItemObj;

		// Token: 0x0400600B RID: 24587
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400435E")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x0400600C RID: 24588
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400435F")]
		private GameObject m_GameObject;

		// Token: 0x0400600D RID: 24589
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004360")]
		private RectTransform m_RectTransform;

		// Token: 0x0400600E RID: 24590
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004361")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x0400600F RID: 24591
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004362")]
		private CustomButton m_CustomButton;

		// Token: 0x04006010 RID: 24592
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004363")]
		private int m_CharaId;
	}
}
