﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FEE RID: 4078
	[Token(Token = "0x2000A9A")]
	[StructLayout(3)]
	public class GachaShowChara : UIGroup
	{
		// Token: 0x06004DF7 RID: 19959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600478C")]
		[Address(RVA = "0x101489978", Offset = "0x1489978", VA = "0x101489978")]
		public void LoadCharaImage(int charaID)
		{
		}

		// Token: 0x06004DF8 RID: 19960 RVA: 0x0001B630 File Offset: 0x00019830
		[Token(Token = "0x600478D")]
		[Address(RVA = "0x101489A08", Offset = "0x1489A08", VA = "0x101489A08")]
		public bool IsLoading()
		{
			return default(bool);
		}

		// Token: 0x06004DF9 RID: 19961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600478E")]
		[Address(RVA = "0x101489FD4", Offset = "0x1489FD4", VA = "0x101489FD4")]
		public void PlayAnim(int idx)
		{
		}

		// Token: 0x06004DFA RID: 19962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600478F")]
		[Address(RVA = "0x1014897FC", Offset = "0x14897FC", VA = "0x1014897FC")]
		public void Destroy()
		{
		}

		// Token: 0x06004DFB RID: 19963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004790")]
		[Address(RVA = "0x10148C844", Offset = "0x148C844", VA = "0x10148C844", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06004DFC RID: 19964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004791")]
		[Address(RVA = "0x10148C888", Offset = "0x148C888", VA = "0x10148C888")]
		public GachaShowChara()
		{
		}

		// Token: 0x04006011 RID: 24593
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004364")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04006012 RID: 24594
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004365")]
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x04006013 RID: 24595
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004366")]
		private int m_CharaID;
	}
}
