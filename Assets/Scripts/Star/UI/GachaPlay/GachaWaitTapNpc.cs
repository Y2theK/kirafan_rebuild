﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000FEF RID: 4079
	[Token(Token = "0x2000A9B")]
	[StructLayout(3)]
	public class GachaWaitTapNpc : MonoBehaviour
	{
		// Token: 0x06004DFD RID: 19965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004792")]
		[Address(RVA = "0x101489274", Offset = "0x1489274", VA = "0x101489274")]
		public void LoadNpc()
		{
		}

		// Token: 0x06004DFE RID: 19966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004793")]
		[Address(RVA = "0x10148C898", Offset = "0x148C898", VA = "0x10148C898")]
		private void Update()
		{
		}

		// Token: 0x06004DFF RID: 19967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004794")]
		[Address(RVA = "0x101489D0C", Offset = "0x1489D0C", VA = "0x101489D0C")]
		public void SetEnableAnim(bool flg)
		{
		}

		// Token: 0x06004E00 RID: 19968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004795")]
		[Address(RVA = "0x10148C930", Offset = "0x148C930", VA = "0x10148C930")]
		public GachaWaitTapNpc()
		{
		}

		// Token: 0x04006014 RID: 24596
		[Token(Token = "0x4004367")]
		private const float MAX_WAIT = 2f;

		// Token: 0x04006015 RID: 24597
		[Token(Token = "0x4004368")]
		private const float MIN_WAIT = 2f;

		// Token: 0x04006016 RID: 24598
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004369")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04006017 RID: 24599
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400436A")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04006018 RID: 24600
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400436B")]
		private bool m_IsEnableAnim;

		// Token: 0x04006019 RID: 24601
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400436C")]
		private float m_Wait;
	}
}
