﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DC0 RID: 3520
	[Token(Token = "0x2000971")]
	[StructLayout(3)]
	public class GachaPlayConfirmWindow : UIGroup
	{
		// Token: 0x060040D9 RID: 16601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B67")]
		[Address(RVA = "0x10148C940", Offset = "0x148C940", VA = "0x10148C940")]
		public void Setup(long haveUnlimitedGem, long playedUnlimitedGem, long haveLimitedGem, long playedLimitedGem, int amount, int playNum, bool isUseRateTicket, DateTime startAt, DateTime endAt)
		{
		}

		// Token: 0x060040DA RID: 16602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B68")]
		[Address(RVA = "0x10148E0B4", Offset = "0x148E0B4", VA = "0x10148E0B4")]
		public void Setup(DateTime startAt, DateTime endAt)
		{
		}

		// Token: 0x060040DB RID: 16603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B69")]
		[Address(RVA = "0x10148E238", Offset = "0x148E238", VA = "0x10148E238")]
		public void Setup(string ticketName, int useNum, int playNum, DateTime startAt, DateTime endAt)
		{
		}

		// Token: 0x060040DC RID: 16604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6A")]
		[Address(RVA = "0x10148D0E0", Offset = "0x148D0E0", VA = "0x10148D0E0")]
		private void SetTitle(DatabaseManager dbMng)
		{
		}

		// Token: 0x060040DD RID: 16605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6B")]
		[Address(RVA = "0x10148D144", Offset = "0x148D144", VA = "0x10148D144")]
		private void SetPeriod(DatabaseManager dbMng, DateTime startAt, DateTime endAt)
		{
		}

		// Token: 0x060040DE RID: 16606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6C")]
		[Address(RVA = "0x10148D7C8", Offset = "0x148D7C8", VA = "0x10148D7C8")]
		private void SetWindowHeight(bool wide)
		{
		}

		// Token: 0x060040DF RID: 16607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6D")]
		[Address(RVA = "0x10148DFA8", Offset = "0x148DFA8", VA = "0x10148DFA8")]
		private void SetGemMessageExist(bool exist)
		{
		}

		// Token: 0x060040E0 RID: 16608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6E")]
		[Address(RVA = "0x10148E50C", Offset = "0x148E50C", VA = "0x10148E50C")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060040E1 RID: 16609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B6F")]
		[Address(RVA = "0x10148E570", Offset = "0x148E570", VA = "0x10148E570")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040E2 RID: 16610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B70")]
		[Address(RVA = "0x10148E5A0", Offset = "0x148E5A0", VA = "0x10148E5A0")]
		public GachaPlayConfirmWindow()
		{
		}

		// Token: 0x04005017 RID: 20503
		[Token(Token = "0x4003881")]
		private const float WINDOW_HEIGHT_WIDE = 536f;

		// Token: 0x04005018 RID: 20504
		[Token(Token = "0x4003882")]
		private const float WINDOW_HEIGHT = 416f;

		// Token: 0x04005019 RID: 20505
		[Token(Token = "0x4003883")]
		private const float FOOTER_Y_WIDE = 0f;

		// Token: 0x0400501A RID: 20506
		[Token(Token = "0x4003884")]
		private const float FOOTER_Y = 27f;

		// Token: 0x0400501B RID: 20507
		[Token(Token = "0x4003885")]
		private const float MESSAGE_0_Y_WIDE = -16f;

		// Token: 0x0400501C RID: 20508
		[Token(Token = "0x4003886")]
		private const float MESSAGE_0_Y = -24f;

		// Token: 0x0400501D RID: 20509
		[Token(Token = "0x4003887")]
		private const float WARNING_Y_WIDE = 50f;

		// Token: 0x0400501E RID: 20510
		[Token(Token = "0x4003888")]
		private const float WARNING_Y = 34f;

		// Token: 0x0400501F RID: 20511
		[Token(Token = "0x4003889")]
		private const float PERIOD_Y_WIDE = 2f;

		// Token: 0x04005020 RID: 20512
		[Token(Token = "0x400388A")]
		private const float PERIOD_Y = -4f;

		// Token: 0x04005021 RID: 20513
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400388B")]
		private readonly Vector2 BUTTON_0_POS_WIDE;

		// Token: 0x04005022 RID: 20514
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400388C")]
		private readonly Vector2 BUTTON_0_POS;

		// Token: 0x04005023 RID: 20515
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400388D")]
		private readonly Vector2 BUTTON_1_POS_WIDE;

		// Token: 0x04005024 RID: 20516
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400388E")]
		private readonly Vector2 BUTTON_1_POS;

		// Token: 0x04005025 RID: 20517
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400388F")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04005026 RID: 20518
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003890")]
		[SerializeField]
		private Text[] m_Message;

		// Token: 0x04005027 RID: 20519
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003891")]
		[SerializeField]
		private GameObject m_Arrow;

		// Token: 0x04005028 RID: 20520
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003892")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x04005029 RID: 20521
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003893")]
		[SerializeField]
		private Text m_PeriodText;

		// Token: 0x0400502A RID: 20522
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003894")]
		[SerializeField]
		private GameObject m_AnimRoot;

		// Token: 0x0400502B RID: 20523
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003895")]
		[SerializeField]
		private GameObject m_Footer;

		// Token: 0x0400502C RID: 20524
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003896")]
		[SerializeField]
		private CustomButton[] m_Buttons;

		// Token: 0x0400502D RID: 20525
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003897")]
		public Action<int> OnClickDecide;

		// Token: 0x0400502E RID: 20526
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003898")]
		public Action OnClickClose;
	}
}
