﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DC1 RID: 3521
	[Token(Token = "0x2000972")]
	[StructLayout(3)]
	public class GachaRateTicketWindow : UIGroup
	{
		// Token: 0x060040E3 RID: 16611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B71")]
		[Address(RVA = "0x10148E660", Offset = "0x148E660", VA = "0x10148E660")]
		public void Setup(string title, string message, string itemName, int itemHaveNum, int itemUseNum, bool isHighRarity)
		{
		}

		// Token: 0x060040E4 RID: 16612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B72")]
		[Address(RVA = "0x10148EBEC", Offset = "0x148EBEC", VA = "0x10148EBEC", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060040E5 RID: 16613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B73")]
		[Address(RVA = "0x10148EF0C", Offset = "0x148EF0C", VA = "0x10148EF0C")]
		public void Destroy()
		{
		}

		// Token: 0x060040E6 RID: 16614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B74")]
		[Address(RVA = "0x10148EF14", Offset = "0x148EF14", VA = "0x10148EF14", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x060040E7 RID: 16615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B75")]
		[Address(RVA = "0x10148EC1C", Offset = "0x148EC1C", VA = "0x10148EC1C")]
		private void ApplyButton(bool isCheckOn)
		{
		}

		// Token: 0x060040E8 RID: 16616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B76")]
		[Address(RVA = "0x10148EF3C", Offset = "0x148EF3C", VA = "0x10148EF3C")]
		public void OnClickToggle()
		{
		}

		// Token: 0x060040E9 RID: 16617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B77")]
		[Address(RVA = "0x10148EF54", Offset = "0x148EF54", VA = "0x10148EF54")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x060040EA RID: 16618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B78")]
		[Address(RVA = "0x10148EF60", Offset = "0x148EF60", VA = "0x10148EF60")]
		public void OnClickDecideButtonTicket()
		{
		}

		// Token: 0x060040EB RID: 16619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B79")]
		[Address(RVA = "0x10148EFC4", Offset = "0x148EFC4", VA = "0x10148EFC4")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060040EC RID: 16620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7A")]
		[Address(RVA = "0x10148F028", Offset = "0x148F028", VA = "0x10148F028")]
		public void OnClickRateButton()
		{
		}

		// Token: 0x060040ED RID: 16621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7B")]
		[Address(RVA = "0x10148F034", Offset = "0x148F034", VA = "0x10148F034")]
		public GachaRateTicketWindow()
		{
		}

		// Token: 0x0400502F RID: 20527
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003899")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04005030 RID: 20528
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400389A")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04005031 RID: 20529
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400389B")]
		[SerializeField]
		private GameObject m_CheckOn;

		// Token: 0x04005032 RID: 20530
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400389C")]
		[SerializeField]
		private GameObject m_CheckOff;

		// Token: 0x04005033 RID: 20531
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400389D")]
		[SerializeField]
		private Text m_CheckText;

		// Token: 0x04005034 RID: 20532
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400389E")]
		[SerializeField]
		private Text m_HaveText;

		// Token: 0x04005035 RID: 20533
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400389F")]
		[SerializeField]
		private Text m_DecideButtonTicketText;

		// Token: 0x04005036 RID: 20534
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40038A0")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x04005037 RID: 20535
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40038A1")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x04005038 RID: 20536
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40038A2")]
		[SerializeField]
		private Text m_RateButtonText;

		// Token: 0x04005039 RID: 20537
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40038A3")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x0400503A RID: 20538
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40038A4")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x0400503B RID: 20539
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40038A5")]
		[SerializeField]
		private CustomButton m_DecideButtonTicket;

		// Token: 0x0400503C RID: 20540
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40038A6")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x0400503D RID: 20541
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40038A7")]
		[SerializeField]
		private CustomButton m_RateButton;

		// Token: 0x0400503E RID: 20542
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40038A8")]
		private bool m_IsUseTicket;

		// Token: 0x0400503F RID: 20543
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x40038A9")]
		private int m_ItemHaveNum;

		// Token: 0x04005040 RID: 20544
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x40038AA")]
		private int m_ItemUseNum;

		// Token: 0x04005041 RID: 20545
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x40038AB")]
		private string m_ConfirmText;

		// Token: 0x04005042 RID: 20546
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x40038AC")]
		public Action<bool> OnClickDecide;

		// Token: 0x04005043 RID: 20547
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x40038AD")]
		public Action OpenRateWindow;
	}
}
