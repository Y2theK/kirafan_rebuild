﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001002 RID: 4098
	[Token(Token = "0x2000AAD")]
	[StructLayout(3)]
	public class GachaBanner : InfiniteScrollItem
	{
		// Token: 0x06004E3E RID: 20030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047CF")]
		[Address(RVA = "0x10148F08C", Offset = "0x148F08C", VA = "0x10148F08C", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06004E3F RID: 20031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D0")]
		[Address(RVA = "0x10148F090", Offset = "0x148F090", VA = "0x10148F090")]
		private void Update()
		{
		}

		// Token: 0x06004E40 RID: 20032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D1")]
		[Address(RVA = "0x10148F22C", Offset = "0x148F22C", VA = "0x10148F22C", Slot = "5")]
		public override void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06004E41 RID: 20033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D2")]
		[Address(RVA = "0x10148F3A8", Offset = "0x148F3A8", VA = "0x10148F3A8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004E42 RID: 20034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D3")]
		[Address(RVA = "0x10148F3E8", Offset = "0x148F3E8", VA = "0x10148F3E8")]
		public GachaBanner()
		{
		}

		// Token: 0x04006036 RID: 24630
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004388")]
		[SerializeField]
		private Image m_Image;
	}
}
