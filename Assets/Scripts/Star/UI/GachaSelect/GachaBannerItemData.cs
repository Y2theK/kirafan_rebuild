﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001003 RID: 4099
	[Token(Token = "0x2000AAE")]
	[StructLayout(3)]
	public class GachaBannerItemData : InfiniteScrollItemData
	{
		// Token: 0x06004E43 RID: 20035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D4")]
		[Address(RVA = "0x10148F3F0", Offset = "0x148F3F0", VA = "0x10148F3F0")]
		public GachaBannerItemData(string gachaBannerID)
		{
		}

		// Token: 0x06004E44 RID: 20036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D5")]
		[Address(RVA = "0x10148F41C", Offset = "0x148F41C", VA = "0x10148F41C")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x06004E45 RID: 20037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D6")]
		[Address(RVA = "0x10148F424", Offset = "0x148F424", VA = "0x10148F424")]
		public void Destroy()
		{
		}

		// Token: 0x04006037 RID: 24631
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004389")]
		public Sprite m_Sprite;

		// Token: 0x04006038 RID: 24632
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400438A")]
		public string m_GachaBannerID;
	}
}
