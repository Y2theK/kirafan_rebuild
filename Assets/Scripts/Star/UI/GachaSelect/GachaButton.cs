﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001004 RID: 4100
	[Token(Token = "0x2000AAF")]
	[StructLayout(3)]
	public class GachaButton : MonoBehaviour
	{
		// Token: 0x140000FC RID: 252
		// (add) Token: 0x06004E46 RID: 20038 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E47 RID: 20039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FC")]
		public event Action<GachaSelectUI.eCostPattern> OnClickGachaButton
		{
			[Token(Token = "0x60047D7")]
			[Address(RVA = "0x10148F42C", Offset = "0x148F42C", VA = "0x10148F42C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60047D8")]
			[Address(RVA = "0x10148F538", Offset = "0x148F538", VA = "0x10148F538")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004E48 RID: 20040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047D9")]
		[Address(RVA = "0x10148F644", Offset = "0x148F644", VA = "0x10148F644")]
		public void Apply(GachaSelectUI.eCostPattern costPattern, GachaSelectUI.UIGachaData.UIGachaButtonData buttonData)
		{
		}

		// Token: 0x06004E49 RID: 20041 RVA: 0x0001B738 File Offset: 0x00019938
		[Token(Token = "0x60047DA")]
		[Address(RVA = "0x1014904BC", Offset = "0x14904BC", VA = "0x1014904BC")]
		public bool ApplyDrawPoint(GachaSelectUI.UIGachaData gachaData)
		{
			return default(bool);
		}

		// Token: 0x06004E4A RID: 20042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047DB")]
		[Address(RVA = "0x10148F870", Offset = "0x148F870", VA = "0x10148F870")]
		private void SetSprite(GachaButton.eGachaButtonType type, int drawNumSpriteIndex)
		{
		}

		// Token: 0x06004E4B RID: 20043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047DC")]
		[Address(RVA = "0x10148FED8", Offset = "0x148FED8", VA = "0x10148FED8")]
		public void SetState(bool interactable, GachaDefine.ePlayType playType, GachaDefine.eFreeDrawType freeType, bool isUnlimited, int useNum, int itemID = -1)
		{
		}

		// Token: 0x06004E4C RID: 20044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047DD")]
		[Address(RVA = "0x1014905BC", Offset = "0x14905BC", VA = "0x1014905BC")]
		private void Move(RectTransform rt, float x)
		{
		}

		// Token: 0x06004E4D RID: 20045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047DE")]
		[Address(RVA = "0x101490690", Offset = "0x1490690", VA = "0x101490690")]
		public void OnClickButtonCallBack()
		{
		}

		// Token: 0x06004E4E RID: 20046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047DF")]
		[Address(RVA = "0x1014906E4", Offset = "0x14906E4", VA = "0x1014906E4")]
		public GachaButton()
		{
		}

		// Token: 0x04006039 RID: 24633
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400438B")]
		[SerializeField]
		private Sprite m_SpriteButtonYellow;

		// Token: 0x0400603A RID: 24634
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400438C")]
		[SerializeField]
		private Sprite m_SpriteButtonRed;

		// Token: 0x0400603B RID: 24635
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400438D")]
		[SerializeField]
		private Sprite m_SpriteButtonPurple;

		// Token: 0x0400603C RID: 24636
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400438E")]
		[SerializeField]
		private Sprite m_SpriteButtonGradation;

		// Token: 0x0400603D RID: 24637
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400438F")]
		[SerializeField]
		private Sprite m_SpriteButtonCream;

		// Token: 0x0400603E RID: 24638
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004390")]
		[SerializeField]
		private Sprite m_SpriteLabelGem1;

		// Token: 0x0400603F RID: 24639
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004391")]
		[SerializeField]
		private Sprite m_SpriteLabelGem10;

		// Token: 0x04006040 RID: 24640
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004392")]
		[SerializeField]
		private Sprite m_SpriteLabelFirst10;

		// Token: 0x04006041 RID: 24641
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004393")]
		[SerializeField]
		private Sprite m_SpriteLabelDay;

		// Token: 0x04006042 RID: 24642
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004394")]
		[SerializeField]
		private Sprite m_SpriteLabelTicket;

		// Token: 0x04006043 RID: 24643
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004395")]
		[SerializeField]
		private Sprite m_SpriteLabelLimitTicket;

		// Token: 0x04006044 RID: 24644
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004396")]
		[SerializeField]
		private Sprite m_SpriteLabelFree1;

		// Token: 0x04006045 RID: 24645
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004397")]
		[SerializeField]
		private Sprite m_SpriteLabelFree10;

		// Token: 0x04006046 RID: 24646
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004398")]
		[SerializeField]
		private Sprite[] m_SpriteLabelDrawNum;

		// Token: 0x04006047 RID: 24647
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004399")]
		[SerializeField]
		private Sprite m_SpriteLabelDrawPoint;

		// Token: 0x04006048 RID: 24648
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400439A")]
		[SerializeField]
		private Image m_ButtonImageNormal;

		// Token: 0x04006049 RID: 24649
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400439B")]
		[SerializeField]
		private Image m_ButtonImagePush;

		// Token: 0x0400604A RID: 24650
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400439C")]
		[SerializeField]
		private Image m_ButtonImageLabel;

		// Token: 0x0400604B RID: 24651
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400439D")]
		[SerializeField]
		private Image m_ButtonImageDrawNum;

		// Token: 0x0400604C RID: 24652
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400439E")]
		[SerializeField]
		private Image m_ButtonImageDrawSummon;

		// Token: 0x0400604D RID: 24653
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400439F")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x0400604E RID: 24654
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40043A0")]
		[SerializeField]
		private RectTransform m_VariableIconClonerRT;

		// Token: 0x0400604F RID: 24655
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40043A1")]
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x04006050 RID: 24656
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40043A2")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012D428", Offset = "0x12D428")]
		private GameObject m_UnlimitedGemObj;

		// Token: 0x04006051 RID: 24657
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40043A3")]
		[SerializeField]
		private Text m_CostNumText;

		// Token: 0x04006052 RID: 24658
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40043A4")]
		[SerializeField]
		private float[] m_IconPos;

		// Token: 0x04006053 RID: 24659
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40043A5")]
		private GachaSelectUI.eCostPattern m_CostPattern;

		// Token: 0x02001005 RID: 4101
		[Token(Token = "0x2001219")]
		public enum eGachaButtonType
		{
			// Token: 0x04006056 RID: 24662
			[Token(Token = "0x4006FF2")]
			None,
			// Token: 0x04006057 RID: 24663
			[Token(Token = "0x4006FF3")]
			Gem1,
			// Token: 0x04006058 RID: 24664
			[Token(Token = "0x4006FF4")]
			Gem10,
			// Token: 0x04006059 RID: 24665
			[Token(Token = "0x4006FF5")]
			Unlimited1,
			// Token: 0x0400605A RID: 24666
			[Token(Token = "0x4006FF6")]
			Unlimited10,
			// Token: 0x0400605B RID: 24667
			[Token(Token = "0x4006FF7")]
			First10,
			// Token: 0x0400605C RID: 24668
			[Token(Token = "0x4006FF8")]
			Day,
			// Token: 0x0400605D RID: 24669
			[Token(Token = "0x4006FF9")]
			Ticket,
			// Token: 0x0400605E RID: 24670
			[Token(Token = "0x4006FFA")]
			LimitTicket,
			// Token: 0x0400605F RID: 24671
			[Token(Token = "0x4006FFB")]
			Free1,
			// Token: 0x04006060 RID: 24672
			[Token(Token = "0x4006FFC")]
			Free10,
			// Token: 0x04006061 RID: 24673
			[Token(Token = "0x4006FFD")]
			UnlimitedVariable,
			// Token: 0x04006062 RID: 24674
			[Token(Token = "0x4006FFE")]
			DrawPoint
		}
	}
}
