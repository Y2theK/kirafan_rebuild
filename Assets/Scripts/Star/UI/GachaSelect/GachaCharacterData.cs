﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF7 RID: 4087
	[Token(Token = "0x2000AA3")]
	[StructLayout(3)]
	public class GachaCharacterData : GachaDetailDropListItemData
	{
		// Token: 0x06004E18 RID: 19992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047AD")]
		[Address(RVA = "0x101490794", Offset = "0x1490794", VA = "0x101490794")]
		public GachaCharacterData(int charaId)
		{
		}

		// Token: 0x06004E19 RID: 19993 RVA: 0x0001B660 File Offset: 0x00019860
		[Token(Token = "0x60047AE")]
		[Address(RVA = "0x101490950", Offset = "0x1490950", VA = "0x101490950")]
		public int GetCharaId()
		{
			return 0;
		}

		// Token: 0x06004E1A RID: 19994 RVA: 0x0001B678 File Offset: 0x00019878
		[Token(Token = "0x60047AF")]
		[Address(RVA = "0x101490958", Offset = "0x1490958", VA = "0x101490958")]
		public eCharaNamedType GetCharaNamedType()
		{
			return eCharaNamedType.Named_0000;
		}

		// Token: 0x06004E1B RID: 19995 RVA: 0x0001B690 File Offset: 0x00019890
		[Token(Token = "0x60047B0")]
		[Address(RVA = "0x101490960", Offset = "0x1490960", VA = "0x101490960")]
		public eClassType GetClassType()
		{
			return eClassType.Fighter;
		}

		// Token: 0x06004E1C RID: 19996 RVA: 0x0001B6A8 File Offset: 0x000198A8
		[Token(Token = "0x60047B1")]
		[Address(RVA = "0x101490968", Offset = "0x1490968", VA = "0x101490968")]
		public eElementType GetElementType()
		{
			return eElementType.Fire;
		}

		// Token: 0x06004E1D RID: 19997 RVA: 0x0001B6C0 File Offset: 0x000198C0
		[Token(Token = "0x60047B2")]
		[Address(RVA = "0x101490970", Offset = "0x1490970", VA = "0x101490970")]
		public eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x0400602A RID: 24618
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400437D")]
		private int m_CharaId;

		// Token: 0x0400602B RID: 24619
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400437E")]
		protected CharacterListDB_Param cldbp;

		// Token: 0x0400602C RID: 24620
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400437F")]
		protected SkillLearnData usld;

		// Token: 0x0400602D RID: 24621
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004380")]
		protected List<SkillLearnData> cslds;
	}
}
