﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF3 RID: 4083
	[Token(Token = "0x2000A9F")]
	[StructLayout(3)]
	public class GachaDetailCharaPanel : EnumerationCharaPanelScrollItemIconAdapter
	{
		// Token: 0x06004E07 RID: 19975 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600479C")]
		[Address(RVA = "0x101490978", Offset = "0x1490978", VA = "0x101490978", Slot = "8")]
		public override EnumerationCharaPanelScrollItemIcon CreateCore()
		{
			return null;
		}

		// Token: 0x06004E08 RID: 19976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600479D")]
		[Address(RVA = "0x1014909DC", Offset = "0x14909DC", VA = "0x1014909DC", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004E09 RID: 19977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600479E")]
		[Address(RVA = "0x101491288", Offset = "0x1491288", VA = "0x101491288")]
		private void SetRare(eRare rare)
		{
		}

		// Token: 0x06004E0A RID: 19978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600479F")]
		[Address(RVA = "0x1014910BC", Offset = "0x14910BC", VA = "0x1014910BC")]
		private void SetActiveSafety(MonoBehaviour mb, bool active)
		{
		}

		// Token: 0x06004E0B RID: 19979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A0")]
		[Address(RVA = "0x10149119C", Offset = "0x149119C", VA = "0x10149119C")]
		private void SetActiveSafety(GameObject go, bool active)
		{
		}

		// Token: 0x06004E0C RID: 19980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A1")]
		[Address(RVA = "0x101491390", Offset = "0x1491390", VA = "0x101491390")]
		public GachaDetailCharaPanel()
		{
		}

		// Token: 0x0400601C RID: 24604
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400436F")]
		[SerializeField]
		private RareStar m_TitleRare;

		// Token: 0x0400601D RID: 24605
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004370")]
		[SerializeField]
		private RareIcon m_TitleRareIcon;

		// Token: 0x0400601E RID: 24606
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004371")]
		[SerializeField]
		private GameObject m_TitlePickUp;

		// Token: 0x0400601F RID: 24607
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004372")]
		[SerializeField]
		private GameObject m_TitleLine;

		// Token: 0x04006020 RID: 24608
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004373")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04006021 RID: 24609
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004374")]
		private GachaDetailScrollItemData m_DataEx;
	}
}
