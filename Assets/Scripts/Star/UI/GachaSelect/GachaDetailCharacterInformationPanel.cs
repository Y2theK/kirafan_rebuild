﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF0 RID: 4080
	[Token(Token = "0x2000A9C")]
	[StructLayout(3)]
	public class GachaDetailCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x06004E01 RID: 19969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004796")]
		[Address(RVA = "0x101491398", Offset = "0x1491398", VA = "0x101491398", Slot = "10")]
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06004E02 RID: 19970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004797")]
		[Address(RVA = "0x101491448", Offset = "0x1491448", VA = "0x101491448", Slot = "12")]
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06004E03 RID: 19971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004798")]
		[Address(RVA = "0x101491614", Offset = "0x1491614", VA = "0x101491614", Slot = "6")]
		protected override void ResetDirtyFlag()
		{
		}

		// Token: 0x06004E04 RID: 19972 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004799")]
		[Address(RVA = "0x101491620", Offset = "0x1491620", VA = "0x101491620")]
		public GachaDetailCharacterInformationPanel()
		{
		}
	}
}
