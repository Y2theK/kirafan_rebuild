﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF6 RID: 4086
	[Token(Token = "0x2000AA2")]
	[StructLayout(3)]
	public class GachaDetailDropListItemData : EnumerationObjectData
	{
		// Token: 0x06004E16 RID: 19990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047AB")]
		[Address(RVA = "0x101490920", Offset = "0x1490920", VA = "0x101490920")]
		protected GachaDetailDropListItemData(eGachaDetailDropListItemDataType gachaDetailDropListItemDataType)
		{
		}

		// Token: 0x06004E17 RID: 19991 RVA: 0x0001B648 File Offset: 0x00019848
		[Token(Token = "0x60047AC")]
		[Address(RVA = "0x101491628", Offset = "0x1491628", VA = "0x101491628")]
		public eGachaDetailDropListItemDataType GetGachaDetailDropListItemDataType()
		{
			return eGachaDetailDropListItemDataType.Error;
		}

		// Token: 0x04006029 RID: 24617
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400437C")]
		private eGachaDetailDropListItemDataType m_GachaDetailDropListItemDataType;
	}
}
