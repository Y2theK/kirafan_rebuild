﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FFB RID: 4091
	[Token(Token = "0x2000AA7")]
	[StructLayout(3)]
	public abstract class GachaDetailDropListItemDataController
	{
		// Token: 0x06004E24 RID: 20004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047B9")]
		[Address(RVA = "0x101491630", Offset = "0x1491630", VA = "0x101491630")]
		protected GachaDetailDropListItemDataController()
		{
		}

		// Token: 0x06004E25 RID: 20005 RVA: 0x0001B708 File Offset: 0x00019908
		[Token(Token = "0x60047BA")]
		[Address(RVA = "0x101491098", Offset = "0x1491098", VA = "0x101491098")]
		public eGachaDetailDropListItemDataType GetGachaDetailDropListItemDataType()
		{
			return eGachaDetailDropListItemDataType.Error;
		}

		// Token: 0x06004E26 RID: 20006
		[Token(Token = "0x60047BB")]
		[Address(Slot = "4")]
		public abstract GachaDetailDropListItemData GetGachaDetailDropListItemData();
	}
}
