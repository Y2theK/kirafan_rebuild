﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FFC RID: 4092
	[Token(Token = "0x2000AA8")]
	[StructLayout(3)]
	public class GachaDetailDropListItemDataControllerExGen<GachaDetailDropListItemDataType> : GachaDetailDropListItemDataController where GachaDetailDropListItemDataType : GachaDetailDropListItemData
	{
		// Token: 0x06004E27 RID: 20007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047BC")]
		[Address(RVA = "0x1016D12B0", Offset = "0x16D12B0", VA = "0x1016D12B0")]
		public GachaDetailDropListItemDataControllerExGen(GachaDetailDropListItemDataType gachaDetailDropListItemData)
		{
		}

		// Token: 0x06004E28 RID: 20008 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047BD")]
		[Address(RVA = "0x1016D12E8", Offset = "0x16D12E8", VA = "0x1016D12E8", Slot = "4")]
		public override GachaDetailDropListItemData GetGachaDetailDropListItemData()
		{
			return null;
		}

		// Token: 0x06004E29 RID: 20009 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047BE")]
		[Address(RVA = "0x1016D1324", Offset = "0x16D1324", VA = "0x1016D1324")]
		public GachaDetailDropListItemDataType GetGachaDetailDropListItemDataEx()
		{
			return null;
		}

		// Token: 0x04006031 RID: 24625
		[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
		[Token(Token = "0x4004384")]
		private GachaDetailDropListItemDataType m_GachaDetailDropListItemData;
	}
}
