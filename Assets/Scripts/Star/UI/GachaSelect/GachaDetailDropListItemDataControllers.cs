﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FFD RID: 4093
	[Token(Token = "0x2000AA9")]
	[StructLayout(3)]
	public class GachaDetailDropListItemDataControllers
	{
		// Token: 0x06004E2A RID: 20010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047BF")]
		[Address(RVA = "0x101491638", Offset = "0x1491638", VA = "0x101491638")]
		public GachaDetailDropListItemDataControllers()
		{
		}

		// Token: 0x06004E2B RID: 20011 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047C0")]
		[Address(RVA = "0x101491640", Offset = "0x1491640", VA = "0x101491640")]
		public GachaDetailDropListItemDataControllers Setup(List<GachaDetailDropListItemData> gachaDetailDropListItemData)
		{
			return null;
		}

		// Token: 0x06004E2C RID: 20012 RVA: 0x0001B720 File Offset: 0x00019920
		[Token(Token = "0x60047C1")]
		[Address(RVA = "0x101491AE4", Offset = "0x1491AE4", VA = "0x101491AE4")]
		public int HowManyMembers()
		{
			return 0;
		}

		// Token: 0x06004E2D RID: 20013 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047C2")]
		[Address(RVA = "0x101491B10", Offset = "0x1491B10", VA = "0x101491B10")]
		public GachaDetailDropListItemDataController GetMemberEx(int index)
		{
			return null;
		}

		// Token: 0x04006032 RID: 24626
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004385")]
		private GachaDetailDropListItemDataController[] m_GachaDetailDropListItemDataController;
	}
}
