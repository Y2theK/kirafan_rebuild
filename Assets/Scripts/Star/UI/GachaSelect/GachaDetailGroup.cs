﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF4 RID: 4084
	[Token(Token = "0x2000AA0")]
	[StructLayout(3)]
	public class GachaDetailGroup : UIGroup
	{
		// Token: 0x06004E0D RID: 19981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A2")]
		[Address(RVA = "0x101491B60", Offset = "0x1491B60", VA = "0x101491B60")]
		public void Setup()
		{
		}

		// Token: 0x06004E0E RID: 19982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A3")]
		[Address(RVA = "0x101491C00", Offset = "0x1491C00", VA = "0x101491C00")]
		public void Destroy()
		{
		}

		// Token: 0x06004E0F RID: 19983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A4")]
		[Address(RVA = "0x101491CA0", Offset = "0x1491CA0", VA = "0x101491CA0")]
		public void SetGachaId(List<int> charaIds)
		{
		}

		// Token: 0x06004E10 RID: 19984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A5")]
		[Address(RVA = "0x101491F98", Offset = "0x1491F98", VA = "0x101491F98")]
		public void SetLineUpCharacterIds(List<int> pickupCharaIDList, List<int> normalCharaIDList)
		{
		}

		// Token: 0x06004E11 RID: 19985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A6")]
		[Address(RVA = "0x101491FE4", Offset = "0x1491FE4", VA = "0x101491FE4")]
		public void SetLineUpItems(List<GachaDetailDropListItemData> pickUpItems, List<GachaDetailDropListItemData> normalItems)
		{
		}

		// Token: 0x06004E12 RID: 19986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A7")]
		[Address(RVA = "0x101492100", Offset = "0x1492100", VA = "0x101492100")]
		public void SetLineUpItems(List<List<GachaDetailDropListItemData>> itemLists)
		{
		}

		// Token: 0x06004E13 RID: 19987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047A8")]
		[Address(RVA = "0x101491E80", Offset = "0x1491E80", VA = "0x101491E80")]
		public void SetEnumerationDatas(List<GachaDetailDropListItemData> enumerationDatas)
		{
		}

		// Token: 0x06004E14 RID: 19988 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047A9")]
		[Address(RVA = "0x101491CCC", Offset = "0x1491CCC", VA = "0x101491CCC")]
		public List<GachaDetailDropListItemData> ConvertCharaIdsToGachaListDatas(List<int> charaIds, bool isPickUp = false)
		{
			return null;
		}

		// Token: 0x06004E15 RID: 19989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047AA")]
		[Address(RVA = "0x101492408", Offset = "0x1492408", VA = "0x101492408")]
		public GachaDetailGroup()
		{
		}

		// Token: 0x04006022 RID: 24610
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004375")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012D23C", Offset = "0x12D23C")]
		protected GachaDetailPanel m_GachaDetailPanel;
	}
}
