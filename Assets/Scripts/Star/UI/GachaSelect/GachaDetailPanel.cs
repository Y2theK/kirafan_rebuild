﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001001 RID: 4097
	[Token(Token = "0x2000AAC")]
	[StructLayout(3)]
	public class GachaDetailPanel : EnumerationPanelScrollItemIconGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore, GachaDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x06004E38 RID: 20024 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047C9")]
		[Address(RVA = "0x101492410", Offset = "0x1492410", VA = "0x101492410", Slot = "4")]
		public override GachaDetailPanelCore CreateCore()
		{
			return null;
		}

		// Token: 0x06004E39 RID: 20025 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047CA")]
		[Address(RVA = "0x1014924B8", Offset = "0x14924B8", VA = "0x1014924B8", Slot = "5")]
		public override GachaDetailPanelCore.SharedInstanceExOverride CreateArgument()
		{
			return null;
		}

		// Token: 0x06004E3A RID: 20026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047CB")]
		[Address(RVA = "0x101492674", Offset = "0x1492674", VA = "0x101492674", Slot = "6")]
		public override void Setup()
		{
		}

		// Token: 0x06004E3B RID: 20027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047CC")]
		[Address(RVA = "0x1014926C4", Offset = "0x14926C4", VA = "0x1014926C4", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004E3C RID: 20028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047CD")]
		[Address(RVA = "0x101492714", Offset = "0x1492714", VA = "0x101492714")]
		private void Update()
		{
		}

		// Token: 0x06004E3D RID: 20029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047CE")]
		[Address(RVA = "0x101492718", Offset = "0x1492718", VA = "0x101492718")]
		public GachaDetailPanel()
		{
		}

		// Token: 0x04006034 RID: 24628
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004386")]
		[SerializeField]
		private GachaDetailCharaPanel m_CharaPanelPrefabEx;

		// Token: 0x04006035 RID: 24629
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004387")]
		[SerializeField]
		private ScrollViewBase m_ScrollViewBase;
	}
}
