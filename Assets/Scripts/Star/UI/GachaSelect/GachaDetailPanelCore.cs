﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FFF RID: 4095
	[Token(Token = "0x2000AAB")]
	[StructLayout(3)]
	public class GachaDetailPanelCore : EnumerationPanelScrollItemIconCoreGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x06004E2F RID: 20015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047C4")]
		[Address(RVA = "0x101492768", Offset = "0x1492768", VA = "0x101492768", Slot = "4")]
		public override void Setup(GachaDetailPanelCore.SharedInstanceExOverride argument)
		{
		}

		// Token: 0x06004E30 RID: 20016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047C5")]
		[Address(RVA = "0x1014927F8", Offset = "0x14927F8", VA = "0x1014927F8", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06004E31 RID: 20017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047C6")]
		[Address(RVA = "0x10149287C", Offset = "0x149287C", VA = "0x10149287C", Slot = "11")]
		protected override void SetPartyData(GachaDetailPanelData data)
		{
		}

		// Token: 0x06004E32 RID: 20018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047C7")]
		[Address(RVA = "0x10149290C", Offset = "0x149290C", VA = "0x10149290C", Slot = "9")]
		protected override void ApplyCharaData(int idx)
		{
		}

		// Token: 0x06004E33 RID: 20019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047C8")]
		[Address(RVA = "0x101492468", Offset = "0x1492468", VA = "0x101492468")]
		public GachaDetailPanelCore()
		{
		}

		// Token: 0x02001000 RID: 4096
		[Token(Token = "0x2001218")]
		[Serializable]
		public class SharedInstanceExOverride : EnumerationPanelScrollItemIconCoreGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore.SharedInstanceExOverride>.SharedInstanceEx<GachaDetailPanelCore.SharedInstanceExOverride>
		{
			// Token: 0x06004E34 RID: 20020 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600631C")]
			[Address(RVA = "0x101492A08", Offset = "0x1492A08", VA = "0x101492A08")]
			public SharedInstanceExOverride()
			{
			}

			// Token: 0x06004E35 RID: 20021 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600631D")]
			[Address(RVA = "0x101492614", Offset = "0x1492614", VA = "0x101492614")]
			public SharedInstanceExOverride(GachaDetailPanelCore.SharedInstanceExOverride argument)
			{
			}

			// Token: 0x06004E36 RID: 20022 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600631E")]
			[Address(RVA = "0x101492A58", Offset = "0x1492A58", VA = "0x101492A58", Slot = "4")]
			public override GachaDetailPanelCore.SharedInstanceExOverride Clone(GachaDetailPanelCore.SharedInstanceExOverride argument)
			{
				return null;
			}

			// Token: 0x06004E37 RID: 20023 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600631F")]
			[Address(RVA = "0x101492AE8", Offset = "0x1492AE8", VA = "0x101492AE8")]
			private GachaDetailPanelCore.SharedInstanceExOverride CloneNewMemberOnly(GachaDetailPanelCore.SharedInstanceExOverride argument)
			{
				return null;
			}

			// Token: 0x04006033 RID: 24627
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006FF0")]
			[SerializeField]
			public ScrollViewBase m_ScrollViewBase;
		}
	}
}
