﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF1 RID: 4081
	[Token(Token = "0x2000A9D")]
	[StructLayout(3)]
	public class GachaDetailScrollItemData : ScrollItemData
	{
		// Token: 0x06004E05 RID: 19973 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600479A")]
		[Address(RVA = "0x101492A00", Offset = "0x1492A00", VA = "0x101492A00")]
		public GachaDetailScrollItemData()
		{
		}

		// Token: 0x0400601A RID: 24602
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400436D")]
		public GachaDetailPanel parent;

		// Token: 0x0400601B RID: 24603
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400436E")]
		public GachaDetailDropListItemDataController partyMemberController;
	}
}
