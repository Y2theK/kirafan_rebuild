﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF8 RID: 4088
	[Token(Token = "0x2000AA4")]
	[StructLayout(3)]
	public class GachaEmptyData : GachaDetailDropListItemData
	{
		// Token: 0x06004E1E RID: 19998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047B3")]
		[Address(RVA = "0x101491AB4", Offset = "0x1491AB4", VA = "0x101491AB4")]
		public GachaEmptyData()
		{
		}
	}
}
