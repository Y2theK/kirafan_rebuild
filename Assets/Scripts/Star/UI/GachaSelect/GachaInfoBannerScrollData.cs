﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001007 RID: 4103
	[Token(Token = "0x2000AB1")]
	[StructLayout(3)]
	public class GachaInfoBannerScrollData : ScrollItemData
	{
		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06004E56 RID: 20054 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004F1")]
		public GachaSelectUI OwnerUI
		{
			[Token(Token = "0x60047E7")]
			[Address(RVA = "0x101492B20", Offset = "0x1492B20", VA = "0x101492B20")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004E57 RID: 20055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E8")]
		[Address(RVA = "0x101492B28", Offset = "0x1492B28", VA = "0x101492B28")]
		public GachaInfoBannerScrollData(GachaSelectUI ownerUI, string bannerID, int index, GachaDefine.eFreeDrawType freeType = GachaDefine.eFreeDrawType.None, bool containUnlimitedGemVariable = false)
		{
		}

		// Token: 0x06004E58 RID: 20056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E9")]
		[Address(RVA = "0x101492B80", Offset = "0x1492B80", VA = "0x101492B80")]
		public void SetCurrent(bool current)
		{
		}

		// Token: 0x0400606B RID: 24683
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40043AF")]
		private GachaSelectUI m_OwnerUI;

		// Token: 0x0400606C RID: 24684
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40043B0")]
		public string m_BannerID;

		// Token: 0x0400606D RID: 24685
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40043B1")]
		public int m_Index;

		// Token: 0x0400606E RID: 24686
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40043B2")]
		public bool m_IsCurrent;

		// Token: 0x0400606F RID: 24687
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40043B3")]
		public GachaDefine.eFreeDrawType m_FreeType;

		// Token: 0x04006070 RID: 24688
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40043B4")]
		public bool m_ContainUnlimitedGemVariable;
	}
}
