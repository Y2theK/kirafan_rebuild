﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001006 RID: 4102
	[Token(Token = "0x2000AB0")]
	[StructLayout(3)]
	public class GachaInfoBannerScrollItem : ScrollItemIcon
	{
		// Token: 0x06004E4F RID: 20047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E0")]
		[Address(RVA = "0x101492B88", Offset = "0x1492B88", VA = "0x101492B88", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004E50 RID: 20048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E1")]
		[Address(RVA = "0x1014932BC", Offset = "0x14932BC", VA = "0x1014932BC", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004E51 RID: 20049 RVA: 0x0001B750 File Offset: 0x00019950
		[Token(Token = "0x60047E2")]
		[Address(RVA = "0x1014932F0", Offset = "0x14932F0", VA = "0x1014932F0")]
		public int GetIndex()
		{
			return 0;
		}

		// Token: 0x06004E52 RID: 20050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E3")]
		[Address(RVA = "0x101493150", Offset = "0x1493150", VA = "0x101493150")]
		public void RefreshCursor()
		{
		}

		// Token: 0x06004E53 RID: 20051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E4")]
		[Address(RVA = "0x101493450", Offset = "0x1493450", VA = "0x101493450")]
		public void OnClickBanner()
		{
		}

		// Token: 0x06004E54 RID: 20052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E5")]
		[Address(RVA = "0x1014935A0", Offset = "0x14935A0", VA = "0x1014935A0")]
		public void Lock(bool isLock)
		{
		}

		// Token: 0x06004E55 RID: 20053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047E6")]
		[Address(RVA = "0x101493738", Offset = "0x1493738", VA = "0x101493738")]
		public GachaInfoBannerScrollItem()
		{
		}

		// Token: 0x04006063 RID: 24675
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40043A7")]
		[SerializeField]
		private Image m_Banner;

		// Token: 0x04006064 RID: 24676
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40043A8")]
		[SerializeField]
		private Image m_Cursor;

		// Token: 0x04006065 RID: 24677
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40043A9")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04006066 RID: 24678
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40043AA")]
		[SerializeField]
		private GameObject m_FreeObj;

		// Token: 0x04006067 RID: 24679
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40043AB")]
		[SerializeField]
		private Image m_Free1Image;

		// Token: 0x04006068 RID: 24680
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40043AC")]
		[SerializeField]
		private Image m_Free10Image;

		// Token: 0x04006069 RID: 24681
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40043AD")]
		[SerializeField]
		private Image m_VariableImage;

		// Token: 0x0400606A RID: 24682
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40043AE")]
		private bool m_Lock;
	}
}
