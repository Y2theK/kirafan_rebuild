﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001008 RID: 4104
	[Token(Token = "0x2000AB2")]
	[StructLayout(3)]
	public class GachaInfoBannerScrollView : ScrollViewBase
	{
		// Token: 0x1700054B RID: 1355
		// (set) Token: 0x06004E59 RID: 20057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004F2")]
		public Func<GameObject, bool> OnBeginDragCallback
		{
			[Token(Token = "0x60047EA")]
			[Address(RVA = "0x101493740", Offset = "0x1493740", VA = "0x101493740")]
			set
			{
			}
		}

		// Token: 0x1700054C RID: 1356
		// (set) Token: 0x06004E5A RID: 20058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004F3")]
		public Action<GameObject> OnEndDragCallback
		{
			[Token(Token = "0x60047EB")]
			[Address(RVA = "0x1014937F0", Offset = "0x14937F0", VA = "0x1014937F0")]
			set
			{
			}
		}

		// Token: 0x06004E5B RID: 20059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047EC")]
		[Address(RVA = "0x1014938A0", Offset = "0x14938A0", VA = "0x1014938A0", Slot = "7")]
		public override void Setup()
		{
		}

		// Token: 0x06004E5C RID: 20060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047ED")]
		[Address(RVA = "0x101493968", Offset = "0x1493968", VA = "0x101493968", Slot = "11")]
		public override void RemoveAll()
		{
		}

		// Token: 0x06004E5D RID: 20061 RVA: 0x0001B768 File Offset: 0x00019968
		[Token(Token = "0x60047EE")]
		[Address(RVA = "0x1014939D4", Offset = "0x14939D4", VA = "0x1014939D4")]
		private int CalcItemCountJustFit(bool isRoundUp)
		{
			return 0;
		}

		// Token: 0x06004E5E RID: 20062 RVA: 0x0001B780 File Offset: 0x00019980
		[Token(Token = "0x60047EF")]
		[Address(RVA = "0x101493AA0", Offset = "0x1493AA0", VA = "0x101493AA0")]
		public int CalcDummyCount(bool isRoundUp)
		{
			return 0;
		}

		// Token: 0x06004E5F RID: 20063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F0")]
		[Address(RVA = "0x101493B50", Offset = "0x1493B50", VA = "0x101493B50")]
		public void AddDummy(ScrollItemData item)
		{
		}

		// Token: 0x06004E60 RID: 20064 RVA: 0x0001B798 File Offset: 0x00019998
		[Token(Token = "0x60047F1")]
		[Address(RVA = "0x101493BD4", Offset = "0x1493BD4", VA = "0x101493BD4")]
		public bool SetSelectIndex(int index)
		{
			return default(bool);
		}

		// Token: 0x06004E61 RID: 20065 RVA: 0x0001B7B0 File Offset: 0x000199B0
		[Token(Token = "0x60047F2")]
		[Address(RVA = "0x101493DA4", Offset = "0x1493DA4", VA = "0x101493DA4")]
		public bool Prev()
		{
			return default(bool);
		}

		// Token: 0x06004E62 RID: 20066 RVA: 0x0001B7C8 File Offset: 0x000199C8
		[Token(Token = "0x60047F3")]
		[Address(RVA = "0x1014941B0", Offset = "0x14941B0", VA = "0x1014941B0")]
		public bool Next()
		{
			return default(bool);
		}

		// Token: 0x06004E63 RID: 20067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F4")]
		[Address(RVA = "0x1014942E0", Offset = "0x14942E0", VA = "0x1014942E0")]
		public void ApplyEasing(int index)
		{
		}

		// Token: 0x06004E64 RID: 20068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F5")]
		[Address(RVA = "0x101494304", Offset = "0x1494304", VA = "0x101494304")]
		public void ReserveImmediateScroll(int index)
		{
		}

		// Token: 0x06004E65 RID: 20069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F6")]
		[Address(RVA = "0x101494314", Offset = "0x1494314", VA = "0x101494314", Slot = "14")]
		protected override void OnClickIconCallBack(ScrollItemIcon icon)
		{
		}

		// Token: 0x06004E66 RID: 20070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F7")]
		[Address(RVA = "0x1014943E4", Offset = "0x14943E4", VA = "0x1014943E4")]
		public void Update()
		{
		}

		// Token: 0x06004E67 RID: 20071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F8")]
		[Address(RVA = "0x101494490", Offset = "0x1494490", VA = "0x101494490", Slot = "9")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004E68 RID: 20072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047F9")]
		[Address(RVA = "0x101494B58", Offset = "0x1494B58", VA = "0x101494B58")]
		public void Lock(bool isLock)
		{
		}

		// Token: 0x06004E69 RID: 20073 RVA: 0x0001B7E0 File Offset: 0x000199E0
		[Token(Token = "0x60047FA")]
		[Address(RVA = "0x101494D84", Offset = "0x1494D84", VA = "0x101494D84")]
		public bool IsDrag()
		{
			return default(bool);
		}

		// Token: 0x06004E6A RID: 20074 RVA: 0x0001B7F8 File Offset: 0x000199F8
		[Token(Token = "0x60047FB")]
		[Address(RVA = "0x101494950", Offset = "0x1494950", VA = "0x101494950")]
		private float CalcNormalizePosition(int index, int indexMax, float rate = 0f)
		{
			return 0f;
		}

		// Token: 0x06004E6B RID: 20075 RVA: 0x0001B810 File Offset: 0x00019A10
		[Token(Token = "0x60047FC")]
		[Address(RVA = "0x101493ED4", Offset = "0x1493ED4", VA = "0x101493ED4")]
		private bool IsOutsideViewport(int index)
		{
			return default(bool);
		}

		// Token: 0x06004E6C RID: 20076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047FD")]
		[Address(RVA = "0x101494E20", Offset = "0x1494E20", VA = "0x101494E20")]
		public GachaInfoBannerScrollView()
		{
		}

		// Token: 0x04006071 RID: 24689
		[Token(Token = "0x40043B5")]
		protected const float ADJUST_SPEED = 10f;

		// Token: 0x04006072 RID: 24690
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40043B6")]
		public Action OnDragAction;

		// Token: 0x04006073 RID: 24691
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40043B7")]
		private List<ScrollItemData> m_DummyItemDataList;

		// Token: 0x04006074 RID: 24692
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40043B8")]
		private ScrollRectEx m_ScrollRectEx;

		// Token: 0x04006075 RID: 24693
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40043B9")]
		private int m_CurrentIndex;

		// Token: 0x04006076 RID: 24694
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x40043BA")]
		private bool m_UpdateCursorAnimation;

		// Token: 0x04006077 RID: 24695
		[Cpp2IlInjected.FieldOffset(Offset = "0xCD")]
		[Token(Token = "0x40043BB")]
		private bool m_PlayingMoveAnimation;

		// Token: 0x04006078 RID: 24696
		[Cpp2IlInjected.FieldOffset(Offset = "0xCE")]
		[Token(Token = "0x40043BC")]
		private bool m_ExecEasing;

		// Token: 0x04006079 RID: 24697
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40043BD")]
		private float m_EaseEndPosition;

		// Token: 0x0400607A RID: 24698
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x40043BE")]
		private bool m_ImmediateScroll;

		// Token: 0x0400607B RID: 24699
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40043BF")]
		private int m_ImmediateScrollTargetIndex;
	}
}
