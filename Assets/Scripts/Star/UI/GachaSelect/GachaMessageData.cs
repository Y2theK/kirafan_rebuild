﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FFA RID: 4090
	[Token(Token = "0x2000AA6")]
	[StructLayout(3)]
	public class GachaMessageData : GachaDetailDropListItemData
	{
		// Token: 0x06004E22 RID: 20002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047B7")]
		[Address(RVA = "0x101492384", Offset = "0x1492384", VA = "0x101492384")]
		public GachaMessageData(string in_message)
		{
		}

		// Token: 0x06004E23 RID: 20003 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60047B8")]
		[Address(RVA = "0x101491388", Offset = "0x1491388", VA = "0x101491388")]
		public string GetMessage()
		{
			return null;
		}

		// Token: 0x04006030 RID: 24624
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004383")]
		private string message;
	}
}
