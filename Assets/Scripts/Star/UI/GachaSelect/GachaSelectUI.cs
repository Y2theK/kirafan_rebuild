﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001010 RID: 4112
	[Token(Token = "0x2000AB7")]
	[StructLayout(3)]
	public class GachaSelectUI : MenuUIBase
	{
		// Token: 0x140000FD RID: 253
		// (add) Token: 0x06004E88 RID: 20104 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E89 RID: 20105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FD")]
		public event Action<int, GachaDefine.ePlayType> OnClickDecideButton
		{
			[Token(Token = "0x6004819")]
			[Address(RVA = "0x101494E28", Offset = "0x1494E28", VA = "0x101494E28")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600481A")]
			[Address(RVA = "0x101494F38", Offset = "0x1494F38", VA = "0x101494F38")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000FE RID: 254
		// (add) Token: 0x06004E8A RID: 20106 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E8B RID: 20107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FE")]
		public event Action<int, GachaDefine.ePlayType> OnClickDecideStepButton
		{
			[Token(Token = "0x600481B")]
			[Address(RVA = "0x101495048", Offset = "0x1495048", VA = "0x101495048")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600481C")]
			[Address(RVA = "0x101495158", Offset = "0x1495158", VA = "0x101495158")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000FF RID: 255
		// (add) Token: 0x06004E8C RID: 20108 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E8D RID: 20109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000FF")]
		public event Action<int> OnClickDecideGachaBoxButton
		{
			[Token(Token = "0x600481D")]
			[Address(RVA = "0x101495268", Offset = "0x1495268", VA = "0x101495268")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600481E")]
			[Address(RVA = "0x101495378", Offset = "0x1495378", VA = "0x101495378")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000100 RID: 256
		// (add) Token: 0x06004E8E RID: 20110 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E8F RID: 20111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000100")]
		public event Action<int, int> OnClickDecidePointGachaButton
		{
			[Token(Token = "0x600481F")]
			[Address(RVA = "0x101495488", Offset = "0x1495488", VA = "0x101495488")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004820")]
			[Address(RVA = "0x101495598", Offset = "0x1495598", VA = "0x101495598")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000101 RID: 257
		// (add) Token: 0x06004E90 RID: 20112 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004E91 RID: 20113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000101")]
		public event Action OnClickPointToItemCloseCallBack
		{
			[Token(Token = "0x6004821")]
			[Address(RVA = "0x1014956A8", Offset = "0x14956A8", VA = "0x1014956A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004822")]
			[Address(RVA = "0x1014957B8", Offset = "0x14957B8", VA = "0x1014957B8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1700054E RID: 1358
		// (set) Token: 0x06004E92 RID: 20114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004F5")]
		public bool KeepResource
		{
			[Token(Token = "0x6004823")]
			[Address(RVA = "0x1014958C8", Offset = "0x14958C8", VA = "0x1014958C8")]
			set
			{
			}
		}

		// Token: 0x1700054F RID: 1359
		// (set) Token: 0x06004E93 RID: 20115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004F6")]
		public bool ReserveCancelDrag
		{
			[Token(Token = "0x6004824")]
			[Address(RVA = "0x1014958D0", Offset = "0x14958D0", VA = "0x1014958D0")]
			set
			{
			}
		}

		// Token: 0x06004E94 RID: 20116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004825")]
		[Address(RVA = "0x1014958D8", Offset = "0x14958D8", VA = "0x1014958D8")]
		private void Start()
		{
		}

		// Token: 0x06004E95 RID: 20117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004826")]
		[Address(RVA = "0x1014958DC", Offset = "0x14958DC", VA = "0x1014958DC")]
		public void SetOwnerState(GachaState_Main ownerState)
		{
		}

		// Token: 0x06004E96 RID: 20118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004827")]
		[Address(RVA = "0x1014958E4", Offset = "0x14958E4", VA = "0x1014958E4")]
		public void Setup(Gacha gacha)
		{
		}

		// Token: 0x06004E97 RID: 20119 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004828")]
		[Address(RVA = "0x101492FFC", Offset = "0x1492FFC", VA = "0x101492FFC")]
		public Sprite GetBannerListSprite(string name)
		{
			return null;
		}

		// Token: 0x06004E98 RID: 20120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004829")]
		[Address(RVA = "0x101493564", Offset = "0x1493564", VA = "0x101493564")]
		public void SetCurrentGacha(int index)
		{
		}

		// Token: 0x06004E99 RID: 20121 RVA: 0x0001B858 File Offset: 0x00019A58
		[Token(Token = "0x600482A")]
		[Address(RVA = "0x101497BB8", Offset = "0x1497BB8", VA = "0x101497BB8")]
		private int SortComp(GachaSelectUI.UIGachaData r, GachaSelectUI.UIGachaData l)
		{
			return 0;
		}

		// Token: 0x06004E9A RID: 20122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600482B")]
		[Address(RVA = "0x101497C38", Offset = "0x1497C38", VA = "0x101497C38")]
		public void Refresh()
		{
		}

		// Token: 0x06004E9B RID: 20123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600482C")]
		[Address(RVA = "0x1014977A0", Offset = "0x14977A0", VA = "0x1014977A0")]
		private void LoadBanner()
		{
		}

		// Token: 0x06004E9C RID: 20124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600482D")]
		[Address(RVA = "0x101497B00", Offset = "0x1497B00", VA = "0x101497B00")]
		private void LoadBannerList()
		{
		}

		// Token: 0x06004E9D RID: 20125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600482E")]
		[Address(RVA = "0x101497D94", Offset = "0x1497D94", VA = "0x101497D94")]
		private void OnConfirmRetry(int btn)
		{
		}

		// Token: 0x06004E9E RID: 20126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600482F")]
		[Address(RVA = "0x101497D98", Offset = "0x1497D98", VA = "0x101497D98")]
		private void RetryErrLoad()
		{
		}

		// Token: 0x06004E9F RID: 20127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004830")]
		[Address(RVA = "0x1014980DC", Offset = "0x14980DC", VA = "0x1014980DC")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004EA0 RID: 20128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004831")]
		[Address(RVA = "0x1014980E8", Offset = "0x14980E8", VA = "0x1014980E8", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004EA1 RID: 20129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004832")]
		[Address(RVA = "0x1014982D8", Offset = "0x14982D8", VA = "0x1014982D8")]
		public void OnScrollCallBack()
		{
		}

		// Token: 0x06004EA2 RID: 20130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004833")]
		[Address(RVA = "0x101498438", Offset = "0x1498438", VA = "0x101498438")]
		public void OnScrollCallBackBannerList()
		{
		}

		// Token: 0x06004EA3 RID: 20131 RVA: 0x0001B870 File Offset: 0x00019A70
		[Token(Token = "0x6004834")]
		[Address(RVA = "0x10149850C", Offset = "0x149850C", VA = "0x10149850C")]
		private bool OnBeginDragCallback_GachaBanner(GameObject obj)
		{
			return default(bool);
		}

		// Token: 0x06004EA4 RID: 20132 RVA: 0x0001B888 File Offset: 0x00019A88
		[Token(Token = "0x6004835")]
		[Address(RVA = "0x10149865C", Offset = "0x149865C", VA = "0x10149865C")]
		private bool OnBeginDragCallback_GachaList(GameObject obj)
		{
			return default(bool);
		}

		// Token: 0x06004EA5 RID: 20133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004836")]
		[Address(RVA = "0x1014987A8", Offset = "0x14987A8", VA = "0x1014987A8")]
		private void OnEndDragCallback(GameObject obj)
		{
		}

		// Token: 0x06004EA6 RID: 20134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004837")]
		[Address(RVA = "0x101498874", Offset = "0x1498874", VA = "0x101498874")]
		public void ReleaseDrag()
		{
		}

		// Token: 0x06004EA7 RID: 20135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004838")]
		[Address(RVA = "0x1014988AC", Offset = "0x14988AC", VA = "0x1014988AC")]
		public void AdjustGachaBanner(int gachaID)
		{
		}

		// Token: 0x06004EA8 RID: 20136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004839")]
		[Address(RVA = "0x1014989C0", Offset = "0x14989C0", VA = "0x1014989C0")]
		private void Update()
		{
		}

		// Token: 0x06004EA9 RID: 20137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600483A")]
		[Address(RVA = "0x101499494", Offset = "0x1499494", VA = "0x101499494")]
		private void ChangeStep(GachaSelectUI.eStep step)
		{
		}

		// Token: 0x06004EAA RID: 20138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600483B")]
		[Address(RVA = "0x10149AAC8", Offset = "0x149AAC8", VA = "0x10149AAC8", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004EAB RID: 20139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600483C")]
		[Address(RVA = "0x1014993D8", Offset = "0x14993D8", VA = "0x1014993D8")]
		private void ExecutePlayIn()
		{
		}

		// Token: 0x06004EAC RID: 20140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600483D")]
		[Address(RVA = "0x10149AADC", Offset = "0x149AADC", VA = "0x10149AADC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004EAD RID: 20141 RVA: 0x0001B8A0 File Offset: 0x00019AA0
		[Token(Token = "0x600483E")]
		[Address(RVA = "0x10149AB98", Offset = "0x149AB98", VA = "0x10149AB98", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004EAE RID: 20142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600483F")]
		[Address(RVA = "0x1014984CC", Offset = "0x14984CC", VA = "0x1014984CC")]
		private void LockGachaBanner(bool isLock)
		{
		}

		// Token: 0x06004EAF RID: 20143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004840")]
		[Address(RVA = "0x1014983B4", Offset = "0x14983B4", VA = "0x1014983B4")]
		private void LockGachaList(bool isLock)
		{
		}

		// Token: 0x06004EB0 RID: 20144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004841")]
		[Address(RVA = "0x10149ABA8", Offset = "0x149ABA8", VA = "0x10149ABA8")]
		public void OnClickButtonCallback(GachaSelectUI.eCostPattern costPattern)
		{
		}

		// Token: 0x06004EB1 RID: 20145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004842")]
		[Address(RVA = "0x10149AE00", Offset = "0x149AE00", VA = "0x10149AE00")]
		public void OnClickSingleButtonCallBack()
		{
		}

		// Token: 0x06004EB2 RID: 20146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004843")]
		[Address(RVA = "0x10149AF08", Offset = "0x149AF08", VA = "0x10149AF08")]
		public void OnClickMultiButtonCallBack()
		{
		}

		// Token: 0x06004EB3 RID: 20147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004844")]
		[Address(RVA = "0x10149ACF8", Offset = "0x149ACF8", VA = "0x10149ACF8")]
		public void OnClickUnlimitedGemButtonCallBack()
		{
		}

		// Token: 0x06004EB4 RID: 20148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004845")]
		[Address(RVA = "0x10149ABE0", Offset = "0x149ABE0", VA = "0x10149ABE0")]
		public void OnClickDrawPtButtonCallBack()
		{
		}

		// Token: 0x06004EB5 RID: 20149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004846")]
		[Address(RVA = "0x10149B014", Offset = "0x149B014", VA = "0x10149B014")]
		private void OnDecideDrawPointCharacter()
		{
		}

		// Token: 0x06004EB6 RID: 20150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004847")]
		[Address(RVA = "0x10149B150", Offset = "0x149B150", VA = "0x10149B150")]
		private void OnHoldPointSelectChara()
		{
		}

		// Token: 0x06004EB7 RID: 20151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004848")]
		[Address(RVA = "0x10149B184", Offset = "0x149B184", VA = "0x10149B184")]
		private void OnClickPointSelectCharaCloseButton()
		{
		}

		// Token: 0x06004EB8 RID: 20152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004849")]
		[Address(RVA = "0x10149B1B8", Offset = "0x149B1B8", VA = "0x10149B1B8")]
		private void ExecuteDrawPointGacha()
		{
		}

		// Token: 0x06004EB9 RID: 20153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484A")]
		[Address(RVA = "0x10149B2D4", Offset = "0x149B2D4", VA = "0x10149B2D4")]
		public void OpenGachaDrawPointExchangedWindow(int num)
		{
		}

		// Token: 0x06004EBA RID: 20154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484B")]
		[Address(RVA = "0x10149B33C", Offset = "0x149B33C", VA = "0x10149B33C")]
		private void OnClickPointToPointCloseButton()
		{
		}

		// Token: 0x06004EBB RID: 20155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484C")]
		[Address(RVA = "0x101499584", Offset = "0x1499584", VA = "0x101499584")]
		private void UpdateButtonState()
		{
		}

		// Token: 0x06004EBC RID: 20156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484D")]
		[Address(RVA = "0x10149B348", Offset = "0x149B348", VA = "0x10149B348")]
		private void SetStepupNumber(bool active, int currentStep = 0, int maxStep = 0)
		{
		}

		// Token: 0x06004EBD RID: 20157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484E")]
		[Address(RVA = "0x10149B3DC", Offset = "0x149B3DC", VA = "0x10149B3DC")]
		public void OnClickURLCallBack()
		{
		}

		// Token: 0x06004EBE RID: 20158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600484F")]
		[Address(RVA = "0x10149B57C", Offset = "0x149B57C", VA = "0x10149B57C")]
		private void OnEndWebviewGroup()
		{
		}

		// Token: 0x06004EBF RID: 20159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004850")]
		[Address(RVA = "0x10149B584", Offset = "0x149B584", VA = "0x10149B584")]
		public void OnClickOpenGachaDetailButton()
		{
		}

		// Token: 0x06004EC0 RID: 20160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004851")]
		[Address(RVA = "0x10149B674", Offset = "0x149B674", VA = "0x10149B674")]
		public void OnClickBonusListButton()
		{
		}

		// Token: 0x06004EC1 RID: 20161 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004852")]
		[Address(RVA = "0x10149B808", Offset = "0x149B808", VA = "0x10149B808")]
		public void OnEndBonusListButton()
		{
		}

		// Token: 0x06004EC2 RID: 20162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004853")]
		[Address(RVA = "0x10149B810", Offset = "0x149B810", VA = "0x10149B810")]
		public void OnClickUpArrow()
		{
		}

		// Token: 0x06004EC3 RID: 20163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004854")]
		[Address(RVA = "0x10149B86C", Offset = "0x149B86C", VA = "0x10149B86C")]
		public void OnClickDownArrow()
		{
		}

		// Token: 0x06004EC4 RID: 20164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004855")]
		[Address(RVA = "0x10149B8C8", Offset = "0x149B8C8", VA = "0x10149B8C8")]
		public void OnChangeCurrent()
		{
		}

		// Token: 0x06004EC5 RID: 20165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004856")]
		[Address(RVA = "0x10149B978", Offset = "0x149B978", VA = "0x10149B978")]
		public void OpenGachaBox()
		{
		}

		// Token: 0x06004EC6 RID: 20166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004857")]
		[Address(RVA = "0x10149BA18", Offset = "0x149BA18", VA = "0x10149BA18")]
		public void OpenGachaRateTicketWindow(string title, string message, string itemName, int itemHaveNum, int itemUseNum, bool isHighRarity, Action<bool> OnClickDecideCallback)
		{
		}

		// Token: 0x06004EC7 RID: 20167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004858")]
		[Address(RVA = "0x10149BB3C", Offset = "0x149BB3C", VA = "0x10149BB3C")]
		private void CloseGachaPlayConfirmWindowCallback()
		{
		}

		// Token: 0x06004EC8 RID: 20168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004859")]
		[Address(RVA = "0x10149BBA0", Offset = "0x149BBA0", VA = "0x10149BBA0")]
		public void OpenGachaPlayConfirmWindow(Gacha.GachaData gachaData, GachaDefine.ePlayType playType, string itemName, int playNum, int amount, bool isUseRateTicket, Action<int> confirmGachaPlay)
		{
		}

		// Token: 0x06004EC9 RID: 20169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485A")]
		[Address(RVA = "0x10149AAB0", Offset = "0x149AAB0", VA = "0x10149AAB0")]
		private void UpdateLoaderBannerList()
		{
		}

		// Token: 0x06004ECA RID: 20170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485B")]
		[Address(RVA = "0x10149BF2C", Offset = "0x149BF2C", VA = "0x10149BF2C")]
		private void UpdateLoaderInternal(ABResourceLoader loader, ABResourceObjectHandler handler, List<Sprite> spriteList, ref bool isDone, bool isUpdate)
		{
		}

		// Token: 0x06004ECB RID: 20171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600485C")]
		[Address(RVA = "0x10149C114", Offset = "0x149C114", VA = "0x10149C114")]
		public GachaSelectUI()
		{
		}

		// Token: 0x040060AA RID: 24746
		[Token(Token = "0x40043DF")]
		private const float BUTTON_APPEAR_WAIT = 0f;

		// Token: 0x040060AB RID: 24747
		[Token(Token = "0x40043E0")]
		private const string URL_WITH_PARAM = "{0}?gachaID={1}";

		// Token: 0x040060AC RID: 24748
		[Token(Token = "0x40043E1")]
		public const string GACHA_LIST_PATH = "texture/gachaList.muast";

		// Token: 0x040060AD RID: 24749
		[Token(Token = "0x40043E2")]
		private const string GACHA_LIST_BASE_NAME = "GachaList_";

		// Token: 0x040060AE RID: 24750
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40043E3")]
		private float[] INFO_BUTTON_SPACING_TABLE;

		// Token: 0x040060AF RID: 24751
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40043E4")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040060B0 RID: 24752
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40043E5")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040060B1 RID: 24753
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40043E6")]
		[SerializeField]
		private GachaDetailGroup m_GachaDetailGroup;

		// Token: 0x040060B2 RID: 24754
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40043E7")]
		[SerializeField]
		private GachaRateTicketWindow m_GachaRateTicketWindow;

		// Token: 0x040060B3 RID: 24755
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40043E8")]
		[SerializeField]
		private GachaDrawPointSelectWindow m_GachaDrawPointSelectWindow;

		// Token: 0x040060B4 RID: 24756
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40043E9")]
		[SerializeField]
		private GachaDrawPointConfirmWindow m_GachaDrawPointConfirmWindow;

		// Token: 0x040060B5 RID: 24757
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40043EA")]
		[SerializeField]
		private GachaDrawPointExchangedWindow m_GachaDrawPointExchangedWindow;

		// Token: 0x040060B6 RID: 24758
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40043EB")]
		[SerializeField]
		private GachaPlayConfirmWindow m_GachaPlayConfirmWindow;

		// Token: 0x040060B7 RID: 24759
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40043EC")]
		[SerializeField]
		private WebViewGroup m_WebviewGroup;

		// Token: 0x040060B8 RID: 24760
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40043ED")]
		[SerializeField]
		private InfiniteScrollEx m_GachaScroll;

		// Token: 0x040060B9 RID: 24761
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40043EE")]
		[SerializeField]
		private GachaInfoBannerScrollView m_GachaInfoBannerScroll;

		// Token: 0x040060BA RID: 24762
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40043EF")]
		[SerializeField]
		private GameObject m_GachaInfoBannerObj;

		// Token: 0x040060BB RID: 24763
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40043F0")]
		[SerializeField]
		private CustomButton m_GachaInfoBannerArrowUp;

		// Token: 0x040060BC RID: 24764
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40043F1")]
		[SerializeField]
		private CustomButton m_GachaInfoBannerArrowDown;

		// Token: 0x040060BD RID: 24765
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40043F2")]
		[SerializeField]
		private UIGroup m_GachaDataAnim;

		// Token: 0x040060BE RID: 24766
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40043F3")]
		[SerializeField]
		private GachaButton m_DrawPtButton;

		// Token: 0x040060BF RID: 24767
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40043F4")]
		[SerializeField]
		private GachaButton m_DayButton;

		// Token: 0x040060C0 RID: 24768
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40043F5")]
		[SerializeField]
		private GachaButton m_SingleButton;

		// Token: 0x040060C1 RID: 24769
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40043F6")]
		[SerializeField]
		private GachaButton m_MultiButton;

		// Token: 0x040060C2 RID: 24770
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40043F7")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012D7A0", Offset = "0x12D7A0")]
		private GameObject m_InfoButtonsObj;

		// Token: 0x040060C3 RID: 24771
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40043F8")]
		[SerializeField]
		private GameObject m_InfoButtonsRate;

		// Token: 0x040060C4 RID: 24772
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40043F9")]
		[SerializeField]
		private GameObject m_InfoButtonsList;

		// Token: 0x040060C5 RID: 24773
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40043FA")]
		[SerializeField]
		private GameObject m_InfoButtonsBonus;

		// Token: 0x040060C6 RID: 24774
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40043FB")]
		[SerializeField]
		private HorizontalLayoutGroup m_InfoButtonsLayoutGroup;

		// Token: 0x040060C7 RID: 24775
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x40043FC")]
		[SerializeField]
		private GameObject m_StepupObj;

		// Token: 0x040060C8 RID: 24776
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x40043FD")]
		[SerializeField]
		private ImageNumbers m_StepupValueCurrent;

		// Token: 0x040060C9 RID: 24777
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x40043FE")]
		[SerializeField]
		private ImageNumbers m_StepupValueMax;

		// Token: 0x040060CA RID: 24778
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x40043FF")]
		[SerializeField]
		private GameObject m_BalloonObj;

		// Token: 0x040060CB RID: 24779
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004400")]
		[SerializeField]
		private Image m_BalloonFree1;

		// Token: 0x040060CC RID: 24780
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004401")]
		[SerializeField]
		private Image m_BalloonFree10;

		// Token: 0x040060CD RID: 24781
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004402")]
		[SerializeField]
		private Image m_BalloonVariable;

		// Token: 0x040060CE RID: 24782
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004403")]
		[SerializeField]
		private Text m_DrawPointText;

		// Token: 0x040060CF RID: 24783
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4004404")]
		[SerializeField]
		private Text m_DrawPointNumText;

		// Token: 0x040060D0 RID: 24784
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4004405")]
		[SerializeField]
		private Text m_DrawPointWarningText;

		// Token: 0x040060D1 RID: 24785
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4004406")]
		[SerializeField]
		private Text m_UnlimitedGemNumText;

		// Token: 0x040060D2 RID: 24786
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4004407")]
		[SerializeField]
		private Text m_LimitedGemNumText;

		// Token: 0x040060D3 RID: 24787
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4004408")]
		[SerializeField]
		private GameObject m_TicketNumObj;

		// Token: 0x040060D4 RID: 24788
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4004409")]
		[SerializeField]
		private Text m_TicketHaveNumText;

		// Token: 0x040060D5 RID: 24789
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x400440A")]
		[SerializeField]
		private VariableIcon m_TicketHaveNumIcon;

		// Token: 0x040060D6 RID: 24790
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x400440B")]
		[SerializeField]
		private GameObject m_RateTicketNumObj;

		// Token: 0x040060D7 RID: 24791
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x400440C")]
		[SerializeField]
		private Text m_RateTicketHaveNumText;

		// Token: 0x040060D8 RID: 24792
		[Cpp2IlInjected.FieldOffset(Offset = "0x190")]
		[Token(Token = "0x400440D")]
		[SerializeField]
		private VariableIcon m_RateTicketHaveNumIcon;

		// Token: 0x040060D9 RID: 24793
		[Cpp2IlInjected.FieldOffset(Offset = "0x198")]
		[Token(Token = "0x400440E")]
		[SerializeField]
		private GameObject m_RetryKeyholderNumObj;

		// Token: 0x040060DA RID: 24794
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A0")]
		[Token(Token = "0x400440F")]
		[SerializeField]
		private Text m_RetryKeyholderHaveNumText;

		// Token: 0x040060DB RID: 24795
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x4004410")]
		[SerializeField]
		private VariableIcon m_RetryKeyholderHaveNumIcon;

		// Token: 0x040060DC RID: 24796
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x4004411")]
		[SerializeField]
		private Text m_PeriodText;

		// Token: 0x040060DD RID: 24797
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x4004412")]
		[SerializeField]
		private GameObject m_PeriodObj;

		// Token: 0x040060DE RID: 24798
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C0")]
		[Token(Token = "0x4004413")]
		[SerializeField]
		private GameObject m_NoGachaObj;

		// Token: 0x040060DF RID: 24799
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C8")]
		[Token(Token = "0x4004414")]
		[SerializeField]
		private Image m_NoGachaImage;

		// Token: 0x040060E0 RID: 24800
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D0")]
		[Token(Token = "0x4004415")]
		[SerializeField]
		private GachaBonusListWindow m_BonusListWindow;

		// Token: 0x040060E1 RID: 24801
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D8")]
		[Token(Token = "0x4004416")]
		private List<GachaBannerItemData> m_GachaBannerDataList;

		// Token: 0x040060E2 RID: 24802
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E0")]
		[Token(Token = "0x4004417")]
		private float m_ButtonAppearWait;

		// Token: 0x040060E3 RID: 24803
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E4")]
		[Token(Token = "0x4004418")]
		private GachaSelectUI.eBannerListLockState m_BannerListLockState;

		// Token: 0x040060E4 RID: 24804
		[Cpp2IlInjected.FieldOffset(Offset = "0x1E8")]
		[Token(Token = "0x4004419")]
		private GachaSelectUI.eStep m_Step;

		// Token: 0x040060E5 RID: 24805
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F0")]
		[Token(Token = "0x400441A")]
		private GachaState_Main m_OwnerState;

		// Token: 0x040060E6 RID: 24806
		[Cpp2IlInjected.FieldOffset(Offset = "0x1F8")]
		[Token(Token = "0x400441B")]
		private SpriteHandler[] m_Hndls;

		// Token: 0x040060E7 RID: 24807
		[Cpp2IlInjected.FieldOffset(Offset = "0x200")]
		[Token(Token = "0x400441C")]
		private ABResourceLoader m_BannerListLoader;

		// Token: 0x040060E8 RID: 24808
		[Cpp2IlInjected.FieldOffset(Offset = "0x208")]
		[Token(Token = "0x400441D")]
		private ABResourceObjectHandler m_BannerListHandle;

		// Token: 0x040060E9 RID: 24809
		[Cpp2IlInjected.FieldOffset(Offset = "0x210")]
		[Token(Token = "0x400441E")]
		private bool m_IsDonePrepare;

		// Token: 0x040060EA RID: 24810
		[Cpp2IlInjected.FieldOffset(Offset = "0x211")]
		[Token(Token = "0x400441F")]
		private bool m_IsDoneLoadBanner;

		// Token: 0x040060EB RID: 24811
		[Cpp2IlInjected.FieldOffset(Offset = "0x212")]
		[Token(Token = "0x4004420")]
		private bool m_IsDoneLoadBannerList;

		// Token: 0x040060EC RID: 24812
		[Cpp2IlInjected.FieldOffset(Offset = "0x213")]
		[Token(Token = "0x4004421")]
		private bool m_ExecBannerList;

		// Token: 0x040060ED RID: 24813
		[Cpp2IlInjected.FieldOffset(Offset = "0x214")]
		[Token(Token = "0x4004422")]
		private bool m_KeepResource;

		// Token: 0x040060EE RID: 24814
		[Cpp2IlInjected.FieldOffset(Offset = "0x215")]
		[Token(Token = "0x4004423")]
		private bool m_ReserveCancelDrag;

		// Token: 0x040060EF RID: 24815
		[Cpp2IlInjected.FieldOffset(Offset = "0x218")]
		[Token(Token = "0x4004424")]
		private KeyValuePair<GachaSelectUI.eScrollObjIndex, GameObject> m_DraggingObject;

		// Token: 0x040060F0 RID: 24816
		[Cpp2IlInjected.FieldOffset(Offset = "0x228")]
		[Token(Token = "0x4004425")]
		private Gacha m_Gacha;

		// Token: 0x040060F1 RID: 24817
		[Cpp2IlInjected.FieldOffset(Offset = "0x230")]
		[Token(Token = "0x4004426")]
		private List<Sprite> m_BannerListList;

		// Token: 0x040060F7 RID: 24823
		[Cpp2IlInjected.FieldOffset(Offset = "0x260")]
		[Token(Token = "0x400442C")]
		private List<GachaSelectUI.UIGachaData> m_GachaDataList;

		// Token: 0x02001011 RID: 4113
		[Token(Token = "0x200121D")]
		private enum eBannerListLockState
		{
			// Token: 0x040060F9 RID: 24825
			[Token(Token = "0x400700F")]
			None,
			// Token: 0x040060FA RID: 24826
			[Token(Token = "0x4007010")]
			Unlock,
			// Token: 0x040060FB RID: 24827
			[Token(Token = "0x4007011")]
			Lock
		}

		// Token: 0x02001012 RID: 4114
		[Token(Token = "0x200121E")]
		private enum eScrollObjIndex
		{
			// Token: 0x040060FD RID: 24829
			[Token(Token = "0x4007013")]
			None,
			// Token: 0x040060FE RID: 24830
			[Token(Token = "0x4007014")]
			GachaBanner,
			// Token: 0x040060FF RID: 24831
			[Token(Token = "0x4007015")]
			GachaList
		}

		// Token: 0x02001013 RID: 4115
		[Token(Token = "0x200121F")]
		private enum eStep
		{
			// Token: 0x04006101 RID: 24833
			[Token(Token = "0x4007017")]
			Hide,
			// Token: 0x04006102 RID: 24834
			[Token(Token = "0x4007018")]
			Prepare,
			// Token: 0x04006103 RID: 24835
			[Token(Token = "0x4007019")]
			In,
			// Token: 0x04006104 RID: 24836
			[Token(Token = "0x400701A")]
			Idle,
			// Token: 0x04006105 RID: 24837
			[Token(Token = "0x400701B")]
			Out,
			// Token: 0x04006106 RID: 24838
			[Token(Token = "0x400701C")]
			Clean,
			// Token: 0x04006107 RID: 24839
			[Token(Token = "0x400701D")]
			End
		}

		// Token: 0x02001014 RID: 4116
		[Token(Token = "0x2001220")]
		public enum eCostPattern
		{
			// Token: 0x04006109 RID: 24841
			[Token(Token = "0x400701F")]
			None = -1,
			// Token: 0x0400610A RID: 24842
			[Token(Token = "0x4007020")]
			Day,
			// Token: 0x0400610B RID: 24843
			[Token(Token = "0x4007021")]
			Single,
			// Token: 0x0400610C RID: 24844
			[Token(Token = "0x4007022")]
			Multi,
			// Token: 0x0400610D RID: 24845
			[Token(Token = "0x4007023")]
			DrawPoint,
			// Token: 0x0400610E RID: 24846
			[Token(Token = "0x4007024")]
			Num
		}

		// Token: 0x02001015 RID: 4117
		[Token(Token = "0x2001221")]
		public class UIGachaData
		{
			// Token: 0x06004ECC RID: 20172 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006320")]
			[Address(RVA = "0x101496664", Offset = "0x1496664", VA = "0x101496664")]
			public UIGachaData(Gacha.GachaData gachaData)
			{
			}

			// Token: 0x06004ECD RID: 20173 RVA: 0x0001B8B8 File Offset: 0x00019AB8
			[Token(Token = "0x6006321")]
			[Address(RVA = "0x101497798", Offset = "0x1497798", VA = "0x101497798")]
			public bool ContainUnlimitedGemVariable()
			{
				return default(bool);
			}

			// Token: 0x0400610F RID: 24847
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4007025")]
			public bool m_IsStep;

			// Token: 0x04006110 RID: 24848
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4007026")]
			public string m_GachaName;

			// Token: 0x04006111 RID: 24849
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007027")]
			public int m_GachaID;

			// Token: 0x04006112 RID: 24850
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007028")]
			public string m_GachaBannerID;

			// Token: 0x04006113 RID: 24851
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4007029")]
			public string m_Url;

			// Token: 0x04006114 RID: 24852
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x400702A")]
			public int m_CurrentStep;

			// Token: 0x04006115 RID: 24853
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x400702B")]
			public int m_MaxStep;

			// Token: 0x04006116 RID: 24854
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x400702C")]
			public int m_PlayerDrawPoint;

			// Token: 0x04006117 RID: 24855
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x400702D")]
			public Gacha.DrawPointReward[] m_DrawPointReward;

			// Token: 0x04006118 RID: 24856
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x400702E")]
			public DateTime m_StartAt;

			// Token: 0x04006119 RID: 24857
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x400702F")]
			public DateTime m_EndAt;

			// Token: 0x0400611A RID: 24858
			[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
			[Token(Token = "0x4007030")]
			public int m_Sort;

			// Token: 0x0400611B RID: 24859
			[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
			[Token(Token = "0x4007031")]
			public GachaDefine.eFreeDrawType m_FreeType;

			// Token: 0x0400611C RID: 24860
			[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
			[Token(Token = "0x4007032")]
			public bool m_ContainUnlimitedGemVariable;

			// Token: 0x0400611D RID: 24861
			[Cpp2IlInjected.FieldOffset(Offset = "0x69")]
			[Token(Token = "0x4007033")]
			public bool m_HasPeriod;

			// Token: 0x0400611E RID: 24862
			[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
			[Token(Token = "0x4007034")]
			public GachaSelectUI.UIGachaData.UIGachaButtonData[] m_ButtonDatas;

			// Token: 0x02001016 RID: 4118
			[Token(Token = "0x2001352")]
			public enum eExCondition
			{
				// Token: 0x04006120 RID: 24864
				[Token(Token = "0x40075C2")]
				None,
				// Token: 0x04006121 RID: 24865
				[Token(Token = "0x40075C3")]
				First10
			}

			// Token: 0x02001017 RID: 4119
			[Token(Token = "0x2001353")]
			public class UIGachaButtonData
			{
				// Token: 0x06004ECE RID: 20174 RVA: 0x00002050 File Offset: 0x00000250
				[Token(Token = "0x600651D")]
				[Address(RVA = "0x10149C22C", Offset = "0x149C22C", VA = "0x10149C22C")]
				public UIGachaButtonData()
				{
				}

				// Token: 0x04006122 RID: 24866
				[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
				[Token(Token = "0x40075C4")]
				public GachaDefine.ePlayType m_PlayType;

				// Token: 0x04006123 RID: 24867
				[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
				[Token(Token = "0x40075C5")]
				public GachaDefine.eFreeDrawType m_FreeType;

				// Token: 0x04006124 RID: 24868
				[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
				[Token(Token = "0x40075C6")]
				public bool m_Exist;

				// Token: 0x04006125 RID: 24869
				[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
				[Token(Token = "0x40075C7")]
				public bool m_Interactable;

				// Token: 0x04006126 RID: 24870
				[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
				[Token(Token = "0x40075C8")]
				public bool m_IsUnlimited;

				// Token: 0x04006127 RID: 24871
				[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
				[Token(Token = "0x40075C9")]
				public int m_ItemID;

				// Token: 0x04006128 RID: 24872
				[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
				[Token(Token = "0x40075CA")]
				public int m_CostNum;

				// Token: 0x04006129 RID: 24873
				[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
				[Token(Token = "0x40075CB")]
				public int m_DrawNum;

				// Token: 0x0400612A RID: 24874
				[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
				[Token(Token = "0x40075CC")]
				public GachaSelectUI.UIGachaData.eExCondition m_ExCondition;
			}
		}
	}
}
