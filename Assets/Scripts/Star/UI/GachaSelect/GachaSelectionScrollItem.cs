﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200100A RID: 4106
	[Token(Token = "0x2000AB4")]
	[StructLayout(3)]
	public class GachaSelectionScrollItem : ScrollItemIcon
	{
		// Token: 0x06004E70 RID: 20080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004801")]
		[Address(RVA = "0x10149C24C", Offset = "0x149C24C", VA = "0x10149C24C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004E71 RID: 20081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004802")]
		[Address(RVA = "0x10149C3F4", Offset = "0x149C3F4", VA = "0x10149C3F4")]
		public void RefreshCurrent()
		{
		}

		// Token: 0x06004E72 RID: 20082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004803")]
		[Address(RVA = "0x10149C5B4", Offset = "0x149C5B4", VA = "0x10149C5B4")]
		public void OnClick()
		{
		}

		// Token: 0x06004E73 RID: 20083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004804")]
		[Address(RVA = "0x10149C6B0", Offset = "0x149C6B0", VA = "0x10149C6B0")]
		public void OnHold()
		{
		}

		// Token: 0x06004E74 RID: 20084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004805")]
		[Address(RVA = "0x10149C7AC", Offset = "0x149C7AC", VA = "0x10149C7AC")]
		public GachaSelectionScrollItem()
		{
		}

		// Token: 0x04006081 RID: 24705
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40043C5")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04006082 RID: 24706
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40043C6")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04006083 RID: 24707
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40043C7")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012D534", Offset = "0x12D534")]
		private GameObject m_PossessionObj;

		// Token: 0x04006084 RID: 24708
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40043C8")]
		[SerializeField]
		private GameObject m_CurrentCursor;
	}
}
