﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02001009 RID: 4105
	[Token(Token = "0x2000AB3")]
	[StructLayout(3)]
	public class GachaSelectionScrollItemData : ScrollItemData
	{
		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06004E6D RID: 20077 RVA: 0x0001B828 File Offset: 0x00019A28
		[Token(Token = "0x170004F4")]
		public bool IsCurrent
		{
			[Token(Token = "0x60047FE")]
			[Address(RVA = "0x10149C5AC", Offset = "0x149C5AC", VA = "0x10149C5AC")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004E6E RID: 20078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047FF")]
		[Address(RVA = "0x10149C7B4", Offset = "0x149C7B4", VA = "0x10149C7B4")]
		public GachaSelectionScrollItemData(int charaID, Action<int> onClick, Action<int> onHold)
		{
		}

		// Token: 0x06004E6F RID: 20079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004800")]
		[Address(RVA = "0x10149C80C", Offset = "0x149C80C", VA = "0x10149C80C")]
		public void SetCurrent(bool isCurrent)
		{
		}

		// Token: 0x0400607C RID: 24700
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40043C0")]
		public int m_CharaID;

		// Token: 0x0400607D RID: 24701
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40043C1")]
		public bool m_Possession;

		// Token: 0x0400607E RID: 24702
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40043C2")]
		public Action<int> m_OnClick;

		// Token: 0x0400607F RID: 24703
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40043C3")]
		public Action<int> m_OnHold;

		// Token: 0x04006080 RID: 24704
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40043C4")]
		private bool m_IsCurrent;
	}
}
