﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200100B RID: 4107
	[Token(Token = "0x2000AB5")]
	[StructLayout(3)]
	public class GachaSelectionScrollView : ScrollViewBase
	{
		// Token: 0x06004E75 RID: 20085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004806")]
		[Address(RVA = "0x10149C814", Offset = "0x149C814", VA = "0x10149C814")]
		public void SetCurrentCursor(int charaId)
		{
		}

		// Token: 0x06004E76 RID: 20086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004807")]
		[Address(RVA = "0x10149CA10", Offset = "0x149CA10", VA = "0x10149CA10")]
		public GachaSelectionScrollView()
		{
		}
	}
}
