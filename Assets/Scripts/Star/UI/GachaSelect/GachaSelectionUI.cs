﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200100C RID: 4108
	[Token(Token = "0x2000AB6")]
	[StructLayout(3)]
	public class GachaSelectionUI : MenuUIBase
	{
		// Token: 0x06004E77 RID: 20087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004808")]
		[Address(RVA = "0x10149CA18", Offset = "0x149CA18", VA = "0x10149CA18")]
		public void Setup(int gachaID, Gacha gacha, GachaDefine.ePlayType playType, GachaState_Main ownerState)
		{
		}

		// Token: 0x06004E78 RID: 20088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004809")]
		[Address(RVA = "0x10149CD1C", Offset = "0x149CD1C", VA = "0x10149CD1C")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004E79 RID: 20089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480A")]
		[Address(RVA = "0x10149CD28", Offset = "0x149CD28", VA = "0x10149CD28", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004E7A RID: 20090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480B")]
		[Address(RVA = "0x10149CEBC", Offset = "0x149CEBC", VA = "0x10149CEBC")]
		private void Update()
		{
		}

		// Token: 0x06004E7B RID: 20091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480C")]
		[Address(RVA = "0x10149DCA4", Offset = "0x149DCA4", VA = "0x10149DCA4")]
		private void ChangeStep(GachaSelectionUI.eStep step)
		{
		}

		// Token: 0x06004E7C RID: 20092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480D")]
		[Address(RVA = "0x10149E29C", Offset = "0x149E29C", VA = "0x10149E29C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004E7D RID: 20093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480E")]
		[Address(RVA = "0x10149D9D4", Offset = "0x149D9D4", VA = "0x10149D9D4")]
		private void ExecPlayIn()
		{
		}

		// Token: 0x06004E7E RID: 20094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600480F")]
		[Address(RVA = "0x10149E2A4", Offset = "0x149E2A4", VA = "0x10149E2A4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004E7F RID: 20095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004810")]
		[Address(RVA = "0x10149DD94", Offset = "0x149DD94", VA = "0x10149DD94")]
		private void RefreshCharacterInfo(int charaId)
		{
		}

		// Token: 0x06004E80 RID: 20096 RVA: 0x0001B840 File Offset: 0x00019A40
		[Token(Token = "0x6004811")]
		[Address(RVA = "0x10149E434", Offset = "0x149E434", VA = "0x10149E434", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004E81 RID: 20097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004812")]
		[Address(RVA = "0x10149E444", Offset = "0x149E444", VA = "0x10149E444")]
		private void OnClickCharaCallBack(int charaID)
		{
		}

		// Token: 0x06004E82 RID: 20098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004813")]
		[Address(RVA = "0x10149E554", Offset = "0x149E554", VA = "0x10149E554")]
		private void OnHoldCharaCallBack(int charaID)
		{
		}

		// Token: 0x06004E83 RID: 20099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004814")]
		[Address(RVA = "0x10149E44C", Offset = "0x149E44C", VA = "0x10149E44C")]
		private void OnActionCharaCallBackImpl(int charaID, GachaSelectionUI.eChangeBustupType type)
		{
		}

		// Token: 0x06004E84 RID: 20100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004815")]
		[Address(RVA = "0x10149E068", Offset = "0x149E068", VA = "0x10149E068")]
		private void OpenCharacterDetail(int charaID)
		{
		}

		// Token: 0x06004E85 RID: 20101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004816")]
		[Address(RVA = "0x10149E664", Offset = "0x149E664", VA = "0x10149E664")]
		public void OnClickSelectionDecideButton()
		{
		}

		// Token: 0x06004E86 RID: 20102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004817")]
		[Address(RVA = "0x10149E670", Offset = "0x149E670", VA = "0x10149E670")]
		private void OnCloseCharaDetailPlayer()
		{
		}

		// Token: 0x06004E87 RID: 20103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004818")]
		[Address(RVA = "0x10149E724", Offset = "0x149E724", VA = "0x10149E724")]
		public GachaSelectionUI()
		{
		}

		// Token: 0x04006085 RID: 24709
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40043C9")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04006086 RID: 24710
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40043CA")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006087 RID: 24711
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40043CB")]
		[SerializeField]
		private GachaSelectionScrollView m_Scroll;

		// Token: 0x04006088 RID: 24712
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40043CC")]
		[SerializeField]
		private CharaIllust[] m_CharaIllust;

		// Token: 0x04006089 RID: 24713
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40043CD")]
		[SerializeField]
		private AnimUIPlayer[] m_CharaIllustAnim;

		// Token: 0x0400608A RID: 24714
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40043CE")]
		[SerializeField]
		private MoveAnimUI[] m_CharaMoveAnim;

		// Token: 0x0400608B RID: 24715
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40043CF")]
		[SerializeField]
		private AnimUIPlayer[] m_CharaInfoAnim;

		// Token: 0x0400608C RID: 24716
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40043D0")]
		[SerializeField]
		private Text[] m_CharacterName;

		// Token: 0x0400608D RID: 24717
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40043D1")]
		[SerializeField]
		private Text[] m_CharacterVoice;

		// Token: 0x0400608E RID: 24718
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40043D2")]
		[SerializeField]
		private ContentTitleLogo[] m_ContentTitleLogo;

		// Token: 0x0400608F RID: 24719
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40043D3")]
		[SerializeField]
		private Text m_WindowMessage;

		// Token: 0x04006090 RID: 24720
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40043D4")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04006091 RID: 24721
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40043D5")]
		[SerializeField]
		private eSoundSeListDB m_CharaIconSelectSE;

		// Token: 0x04006092 RID: 24722
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40043D6")]
		[SerializeField]
		private GameObject m_NamePlateSubGO;

		// Token: 0x04006093 RID: 24723
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40043D7")]
		private GachaSelectionUI.eStep m_Step;

		// Token: 0x04006094 RID: 24724
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40043D8")]
		public Action<int> OnClickSelection;

		// Token: 0x04006095 RID: 24725
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40043D9")]
		public Action OnClickDecideButton;

		// Token: 0x04006096 RID: 24726
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40043DA")]
		private GachaState_Main m_OwnerState;

		// Token: 0x04006097 RID: 24727
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40043DB")]
		private int m_CurrentIllustIndex;

		// Token: 0x04006098 RID: 24728
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x40043DC")]
		private bool m_ChangeBustupCharacter;

		// Token: 0x04006099 RID: 24729
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40043DD")]
		private GachaSelectionUI.eChangeBustupPhase m_ChangeBustupPhase;

		// Token: 0x0400609A RID: 24730
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x40043DE")]
		private GachaSelectionUI.eChangeBustupType m_ChangeBustupType;

		// Token: 0x0200100D RID: 4109
		[Token(Token = "0x200121A")]
		private enum eChangeBustupPhase
		{
			// Token: 0x0400609C RID: 24732
			[Token(Token = "0x4007000")]
			None,
			// Token: 0x0400609D RID: 24733
			[Token(Token = "0x4007001")]
			Change,
			// Token: 0x0400609E RID: 24734
			[Token(Token = "0x4007002")]
			ChangeWait,
			// Token: 0x0400609F RID: 24735
			[Token(Token = "0x4007003")]
			OpenDetail
		}

		// Token: 0x0200100E RID: 4110
		[Token(Token = "0x200121B")]
		private enum eChangeBustupType
		{
			// Token: 0x040060A1 RID: 24737
			[Token(Token = "0x4007005")]
			Click,
			// Token: 0x040060A2 RID: 24738
			[Token(Token = "0x4007006")]
			Hold
		}

		// Token: 0x0200100F RID: 4111
		[Token(Token = "0x200121C")]
		private enum eStep
		{
			// Token: 0x040060A4 RID: 24740
			[Token(Token = "0x4007008")]
			Hide,
			// Token: 0x040060A5 RID: 24741
			[Token(Token = "0x4007009")]
			Prepare,
			// Token: 0x040060A6 RID: 24742
			[Token(Token = "0x400700A")]
			In,
			// Token: 0x040060A7 RID: 24743
			[Token(Token = "0x400700B")]
			Idle,
			// Token: 0x040060A8 RID: 24744
			[Token(Token = "0x400700C")]
			Out,
			// Token: 0x040060A9 RID: 24745
			[Token(Token = "0x400700D")]
			End
		}
	}
}
