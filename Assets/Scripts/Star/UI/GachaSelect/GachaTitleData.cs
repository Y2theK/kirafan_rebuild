﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF9 RID: 4089
	[Token(Token = "0x2000AA5")]
	[StructLayout(3)]
	public class GachaTitleData : GachaDetailDropListItemData
	{
		// Token: 0x06004E1F RID: 19999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60047B4")]
		[Address(RVA = "0x1014923C4", Offset = "0x14923C4", VA = "0x1014923C4")]
		public GachaTitleData(eRare in_rare, bool in_isPickUp)
		{
		}

		// Token: 0x06004E20 RID: 20000 RVA: 0x0001B6D8 File Offset: 0x000198D8
		[Token(Token = "0x60047B5")]
		[Address(RVA = "0x101491280", Offset = "0x1491280", VA = "0x101491280")]
		public eRare GetRarity()
		{
			return eRare.Star1;
		}

		// Token: 0x06004E21 RID: 20001 RVA: 0x0001B6F0 File Offset: 0x000198F0
		[Token(Token = "0x60047B6")]
		[Address(RVA = "0x101491278", Offset = "0x1491278", VA = "0x101491278")]
		public bool IsPickUp()
		{
			return default(bool);
		}

		// Token: 0x0400602E RID: 24622
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004381")]
		private eRare rare;

		// Token: 0x0400602F RID: 24623
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004382")]
		private bool isPickUp;
	}
}
