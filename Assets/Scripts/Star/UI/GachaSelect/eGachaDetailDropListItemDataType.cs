﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000FF5 RID: 4085
	[Token(Token = "0x2000AA1")]
	[StructLayout(3, Size = 4)]
	public enum eGachaDetailDropListItemDataType
	{
		// Token: 0x04006024 RID: 24612
		[Token(Token = "0x4004377")]
		Error,
		// Token: 0x04006025 RID: 24613
		[Token(Token = "0x4004378")]
		Character,
		// Token: 0x04006026 RID: 24614
		[Token(Token = "0x4004379")]
		Empty,
		// Token: 0x04006027 RID: 24615
		[Token(Token = "0x400437A")]
		Title,
		// Token: 0x04006028 RID: 24616
		[Token(Token = "0x400437B")]
		Message
	}
}
