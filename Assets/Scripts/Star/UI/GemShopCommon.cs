﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DC2 RID: 3522
	[Token(Token = "0x2000973")]
	[StructLayout(3)]
	public class GemShopCommon
	{
		// Token: 0x060040EE RID: 16622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7C")]
		[Address(RVA = "0x10149E734", Offset = "0x149E734", VA = "0x10149E734")]
		public void Setup()
		{
		}

		// Token: 0x060040EF RID: 16623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7D")]
		[Address(RVA = "0x10149EA80", Offset = "0x149EA80", VA = "0x10149EA80")]
		private void LoadRegularPassHelpBanner(string bannerName)
		{
		}

		// Token: 0x060040F0 RID: 16624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7E")]
		[Address(RVA = "0x10149EB14", Offset = "0x149EB14", VA = "0x10149EB14")]
		private void UnloadRegularPassHelpBanner()
		{
		}

		// Token: 0x060040F1 RID: 16625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B7F")]
		[Address(RVA = "0x10149EBB0", Offset = "0x149EBB0", VA = "0x10149EBB0")]
		public void UnloadResource()
		{
		}

		// Token: 0x060040F2 RID: 16626 RVA: 0x000190E0 File Offset: 0x000172E0
		[Token(Token = "0x6003B80")]
		[Address(RVA = "0x10149EE9C", Offset = "0x149EE9C", VA = "0x10149EE9C")]
		public bool IsCompleteUnload()
		{
			return default(bool);
		}

		// Token: 0x060040F3 RID: 16627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B81")]
		[Address(RVA = "0x10149EF2C", Offset = "0x149EF2C", VA = "0x10149EF2C")]
		public void Destroy()
		{
		}

		// Token: 0x060040F4 RID: 16628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B82")]
		[Address(RVA = "0x10149EF30", Offset = "0x149EF30", VA = "0x10149EF30")]
		public void ClearReplaceList()
		{
		}

		// Token: 0x060040F5 RID: 16629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B83")]
		[Address(RVA = "0x10149EFD8", Offset = "0x149EFD8", VA = "0x10149EFD8")]
		public void Update_OnIdle(GemShopCommon.eCategory category)
		{
		}

		// Token: 0x060040F6 RID: 16630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B84")]
		[Address(RVA = "0x10149FAF4", Offset = "0x149FAF4", VA = "0x10149FAF4")]
		public void Update()
		{
		}

		// Token: 0x060040F7 RID: 16631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B85")]
		[Address(RVA = "0x10149FFC8", Offset = "0x149FFC8", VA = "0x10149FFC8")]
		public void AddScrollItemData(ScrollViewBase scrollView, GemShopCommon.eCategory category, List<StoreManager.Product> list)
		{
		}

		// Token: 0x060040F8 RID: 16632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B86")]
		[Address(RVA = "0x1014A20BC", Offset = "0x14A20BC", VA = "0x1014A20BC")]
		public void RefreshPrefabIndex(GemShopScrollView scroll, GemShopCommon.eCategory category, List<StoreDefine.eProductType> list)
		{
		}

		// Token: 0x060040F9 RID: 16633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B87")]
		[Address(RVA = "0x1014A24A8", Offset = "0x14A24A8", VA = "0x1014A24A8")]
		public void ClientRestock()
		{
		}

		// Token: 0x060040FA RID: 16634 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B88")]
		[Address(RVA = "0x1014A25FC", Offset = "0x14A25FC", VA = "0x1014A25FC")]
		public List<StoreManager.Product> GetCurrentProduct(List<StoreDefine.eProductType> productTypeList)
		{
			return null;
		}

		// Token: 0x060040FB RID: 16635 RVA: 0x000190F8 File Offset: 0x000172F8
		[Token(Token = "0x6003B89")]
		[Address(RVA = "0x1014A2810", Offset = "0x14A2810", VA = "0x1014A2810")]
		public bool ExistNewProduct(List<StoreDefine.eProductType> checkList)
		{
			return default(bool);
		}

		// Token: 0x060040FC RID: 16636 RVA: 0x00019110 File Offset: 0x00017310
		[Token(Token = "0x6003B8A")]
		[Address(RVA = "0x1014A298C", Offset = "0x14A298C", VA = "0x1014A298C")]
		public StoreDefine.eBadgeType GetBadgeType(List<StoreDefine.eProductType> checkList)
		{
			return StoreDefine.eBadgeType.Undefined;
		}

		// Token: 0x060040FD RID: 16637 RVA: 0x00019128 File Offset: 0x00017328
		[Token(Token = "0x6003B8B")]
		[Address(RVA = "0x1014A2B08", Offset = "0x14A2B08", VA = "0x1014A2B08")]
		public bool ContainsCategory(List<StoreDefine.eProductType> checkList)
		{
			return default(bool);
		}

		// Token: 0x060040FE RID: 16638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B8C")]
		[Address(RVA = "0x1014A2C84", Offset = "0x14A2C84", VA = "0x1014A2C84")]
		public void ReplaceEventPassBG(GemShopPassItem item, string storeBGID)
		{
		}

		// Token: 0x060040FF RID: 16639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B8D")]
		[Address(RVA = "0x1014A2D8C", Offset = "0x14A2D8C", VA = "0x1014A2D8C")]
		public void ReplaceEventPassBanner(GemShopPassItem item, string bannerID)
		{
		}

		// Token: 0x06004100 RID: 16640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B8E")]
		[Address(RVA = "0x1014A2EE4", Offset = "0x14A2EE4", VA = "0x1014A2EE4")]
		public void ReplaceRegularPassBanner(GemShopRegularPassItem item, string bannerID)
		{
		}

		// Token: 0x06004101 RID: 16641 RVA: 0x00019140 File Offset: 0x00017340
		[Token(Token = "0x6003B8F")]
		[Address(RVA = "0x1014A2FFC", Offset = "0x14A2FFC", VA = "0x1014A2FFC")]
		public bool IsValidRegularPassBannerID(string bannerID)
		{
			return default(bool);
		}

		// Token: 0x06004102 RID: 16642 RVA: 0x00019158 File Offset: 0x00017358
		[Token(Token = "0x6003B90")]
		[Address(RVA = "0x1014A3070", Offset = "0x14A3070", VA = "0x1014A3070")]
		public bool IsValidRegularPassHelpBannerID(string helpBannerID)
		{
			return default(bool);
		}

		// Token: 0x06004103 RID: 16643 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B91")]
		[Address(RVA = "0x10149F508", Offset = "0x149F508", VA = "0x10149F508")]
		private Sprite GetEventPassBG(string name)
		{
			return null;
		}

		// Token: 0x06004104 RID: 16644 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003B92")]
		[Address(RVA = "0x10149F8BC", Offset = "0x149F8BC", VA = "0x10149F8BC")]
		private Sprite GetRegularPassBanner(string name)
		{
			return null;
		}

		// Token: 0x06004105 RID: 16645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B93")]
		[Address(RVA = "0x1014A3074", Offset = "0x14A3074", VA = "0x1014A3074")]
		public void OpenWebView(int idx)
		{
		}

		// Token: 0x06004106 RID: 16646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B94")]
		[Address(RVA = "0x1014A31E4", Offset = "0x14A31E4", VA = "0x1014A31E4")]
		private void OnClickRecipeButtonCallBack(int id)
		{
		}

		// Token: 0x06004107 RID: 16647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B95")]
		[Address(RVA = "0x1014A3244", Offset = "0x14A3244", VA = "0x1014A3244")]
		private void OnClickExchangeButtonCallBack(StoreManager.Product product)
		{
		}

		// Token: 0x06004108 RID: 16648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B96")]
		[Address(RVA = "0x1014A32A4", Offset = "0x14A32A4", VA = "0x1014A32A4")]
		public void OpenDetailWindow(int id)
		{
		}

		// Token: 0x06004109 RID: 16649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B97")]
		[Address(RVA = "0x1014A3350", Offset = "0x14A3350", VA = "0x1014A3350")]
		public void OpenDetailWindow(StoreManager.Product product)
		{
		}

		// Token: 0x0600410A RID: 16650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B98")]
		[Address(RVA = "0x1014A35A4", Offset = "0x14A35A4", VA = "0x1014A35A4")]
		private void CreateDetailWindowText_GemPack(StoreManager.Product product, out string out_title, out string out_message)
		{
		}

		// Token: 0x0600410B RID: 16651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B99")]
		[Address(RVA = "0x1014A4208", Offset = "0x14A4208", VA = "0x1014A4208")]
		private void CreateDetailWindowText_Premium(StoreManager.Product product, out string out_title, out string out_message)
		{
		}

		// Token: 0x0600410C RID: 16652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B9A")]
		[Address(RVA = "0x1014A4B6C", Offset = "0x14A4B6C", VA = "0x1014A4B6C")]
		private void CreateDetailWindowText_Regular(StoreManager.Product product, out string out_title, out string out_message)
		{
		}

		// Token: 0x0600410D RID: 16653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B9B")]
		[Address(RVA = "0x1014A4CEC", Offset = "0x14A4CEC", VA = "0x1014A4CEC")]
		public GemShopCommon()
		{
		}

		// Token: 0x04005044 RID: 20548
		[Token(Token = "0x40038AE")]
		private const string IGNORE_KEYWORD = "None";

		// Token: 0x04005045 RID: 20549
		[Token(Token = "0x40038AF")]
		private const int RESOURCE_INDEX_EVENT_PASS_BG = 0;

		// Token: 0x04005046 RID: 20550
		[Token(Token = "0x40038B0")]
		private const int RESOURCE_INDEX_REGULAR_PASS_BANNER = 1;

		// Token: 0x04005047 RID: 20551
		[Token(Token = "0x40038B1")]
		private static readonly string[] RESOURCE_PATH;

		// Token: 0x04005048 RID: 20552
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40038B2")]
		private List<KeyValuePair<GemShopPassItem, string>> m_ReplaceEventPassBGList;

		// Token: 0x04005049 RID: 20553
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40038B3")]
		private List<KeyValuePair<GemShopPassItem, string>> m_ReplaceEventPassBannerList;

		// Token: 0x0400504A RID: 20554
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40038B4")]
		private List<KeyValuePair<GemShopRegularPassItem, string>> m_ReplaceRegularPassBannerList;

		// Token: 0x0400504B RID: 20555
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40038B5")]
		private bool[] m_IsLoadCompleteResource;

		// Token: 0x0400504C RID: 20556
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40038B6")]
		private ABResourceLoader[] m_Loader;

		// Token: 0x0400504D RID: 20557
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40038B7")]
		private ABResourceObjectHandler[] m_ResourceHandler;

		// Token: 0x0400504E RID: 20558
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40038B8")]
		private SpriteHandler m_RegularPassHelpBanner;

		// Token: 0x0400504F RID: 20559
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40038B9")]
		private List<Sprite> m_EventPassBGSpriteList;

		// Token: 0x04005050 RID: 20560
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40038BA")]
		private List<Sprite> m_RegularPassBannerSpriteList;

		// Token: 0x04005051 RID: 20561
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40038BB")]
		public Action<int> OnClickPurchase;

		// Token: 0x04005052 RID: 20562
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40038BC")]
		public Action<StoreManager.Product> OnClickExchange;

		// Token: 0x04005053 RID: 20563
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40038BD")]
		public Action<string, string, string, bool> OpenDetailWindowCallback;

		// Token: 0x04005054 RID: 20564
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40038BE")]
		public Action<Sprite> ReceiveRegularPassHelpBannerCallback;

		// Token: 0x02000DC3 RID: 3523
		[Token(Token = "0x2001115")]
		public enum eCategory
		{
			// Token: 0x04005056 RID: 20566
			[Token(Token = "0x4006AE0")]
			Gem,
			// Token: 0x04005057 RID: 20567
			[Token(Token = "0x4006AE1")]
			Event,
			// Token: 0x04005058 RID: 20568
			[Token(Token = "0x4006AE2")]
			Regular,
			// Token: 0x04005059 RID: 20569
			[Token(Token = "0x4006AE3")]
			DirectSale,
			// Token: 0x0400505A RID: 20570
			[Token(Token = "0x4006AE4")]
			Exchange,
			// Token: 0x0400505B RID: 20571
			[Token(Token = "0x4006AE5")]
			Num
		}
	}
}
