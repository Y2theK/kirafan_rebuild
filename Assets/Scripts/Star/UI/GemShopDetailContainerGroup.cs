﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DC4 RID: 3524
	[Token(Token = "0x2000974")]
	[StructLayout(3)]
	public class GemShopDetailContainerGroup : UIGroup
	{
		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x0600410F RID: 16655 RVA: 0x00019170 File Offset: 0x00017370
		[Token(Token = "0x17000483")]
		public bool IsEmptySprite
		{
			[Token(Token = "0x6003B9D")]
			[Address(RVA = "0x1014A4EF8", Offset = "0x14A4EF8", VA = "0x1014A4EF8")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004110 RID: 16656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B9E")]
		[Address(RVA = "0x1014A4F84", Offset = "0x14A4F84", VA = "0x1014A4F84", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004111 RID: 16657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B9F")]
		[Address(RVA = "0x1014A5000", Offset = "0x14A5000", VA = "0x1014A5000", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x06004112 RID: 16658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA0")]
		[Address(RVA = "0x1014A5064", Offset = "0x14A5064", VA = "0x1014A5064")]
		public void Setup(string title, string message, string buttonText, bool existImage)
		{
		}

		// Token: 0x06004113 RID: 16659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA1")]
		[Address(RVA = "0x1014A5468", Offset = "0x14A5468", VA = "0x1014A5468")]
		public void UnloadResource()
		{
		}

		// Token: 0x06004114 RID: 16660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA2")]
		[Address(RVA = "0x1014A502C", Offset = "0x14A502C", VA = "0x1014A502C")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x06004115 RID: 16661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA3")]
		[Address(RVA = "0x1014A5358", Offset = "0x14A5358", VA = "0x1014A5358")]
		private void AdjustDetailWindowPivot(float contentHeight)
		{
		}

		// Token: 0x06004116 RID: 16662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA4")]
		[Address(RVA = "0x1014A5470", Offset = "0x14A5470", VA = "0x1014A5470")]
		public void OnClickButton()
		{
		}

		// Token: 0x06004117 RID: 16663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA5")]
		[Address(RVA = "0x1014A547C", Offset = "0x14A547C", VA = "0x1014A547C")]
		public GemShopDetailContainerGroup()
		{
		}

		// Token: 0x0400505C RID: 20572
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40038BF")]
		[SerializeField]
		private Text m_DetailTitle;

		// Token: 0x0400505D RID: 20573
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40038C0")]
		[SerializeField]
		private RectTransform m_DetailContainerGroupImageParent;

		// Token: 0x0400505E RID: 20574
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40038C1")]
		[SerializeField]
		private Image m_DetailContainerGroupImage;

		// Token: 0x0400505F RID: 20575
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40038C2")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04005060 RID: 20576
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40038C3")]
		[SerializeField]
		private Text m_DetailCloseButtonText;

		// Token: 0x04005061 RID: 20577
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40038C4")]
		[SerializeField]
		private ScrollRect m_DetailScrollRect;

		// Token: 0x04005062 RID: 20578
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40038C5")]
		[SerializeField]
		private VerticalLayoutGroup m_ContentVerticalLayout;

		// Token: 0x04005063 RID: 20579
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40038C6")]
		private bool m_AdjustScroll;
	}
}
