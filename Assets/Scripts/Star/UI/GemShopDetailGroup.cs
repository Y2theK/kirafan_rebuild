﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DC5 RID: 3525
	[Token(Token = "0x2000975")]
	[StructLayout(3)]
	public class GemShopDetailGroup : UIGroup
	{
		// Token: 0x06004118 RID: 16664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA6")]
		[Address(RVA = "0x1014A5484", Offset = "0x14A5484", VA = "0x1014A5484", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004119 RID: 16665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA7")]
		[Address(RVA = "0x1014A5500", Offset = "0x14A5500", VA = "0x1014A5500")]
		public void Setup(string title, string message, string buttonText)
		{
		}

		// Token: 0x0600411A RID: 16666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BA8")]
		[Address(RVA = "0x1014A5780", Offset = "0x14A5780", VA = "0x1014A5780")]
		private void AdjustDetailWindowSize(float contentHeight)
		{
		}

		// Token: 0x0600411B RID: 16667 RVA: 0x00019188 File Offset: 0x00017388
		[Token(Token = "0x6003BA9")]
		[Address(RVA = "0x1014A5AE8", Offset = "0x14A5AE8", VA = "0x1014A5AE8")]
		private Vector2 CalcDetailWindowContentMargin()
		{
			return default(Vector2);
		}

		// Token: 0x0600411C RID: 16668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAA")]
		[Address(RVA = "0x1014A59D8", Offset = "0x14A59D8", VA = "0x1014A59D8")]
		private void AdjustDetailWindowPivot(float contentHeight)
		{
		}

		// Token: 0x0600411D RID: 16669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAB")]
		[Address(RVA = "0x1014A5C64", Offset = "0x14A5C64", VA = "0x1014A5C64")]
		public void OnClickButton()
		{
		}

		// Token: 0x0600411E RID: 16670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAC")]
		[Address(RVA = "0x1014A5C70", Offset = "0x14A5C70", VA = "0x1014A5C70")]
		public GemShopDetailGroup()
		{
		}

		// Token: 0x04005064 RID: 20580
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40038C7")]
		[SerializeField]
		private Text m_DetailTitle;

		// Token: 0x04005065 RID: 20581
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40038C8")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04005066 RID: 20582
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40038C9")]
		[SerializeField]
		private Text m_DetailCloseButtonText;

		// Token: 0x04005067 RID: 20583
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40038CA")]
		[SerializeField]
		private RectTransform m_DetailContent;

		// Token: 0x04005068 RID: 20584
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40038CB")]
		[SerializeField]
		private ScrollRect m_DetailScrollRect;

		// Token: 0x04005069 RID: 20585
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40038CC")]
		[SerializeField]
		private VerticalLayoutGroup m_ContentVerticalLayout;

		// Token: 0x0400506A RID: 20586
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40038CD")]
		[SerializeField]
		private RectTransform[] m_DetailContentFrameElement;

		// Token: 0x0400506B RID: 20587
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40038CE")]
		[SerializeField]
		private RectTransform[] m_DetailWindowElement;

		// Token: 0x0400506C RID: 20588
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40038CF")]
		[SerializeField]
		private bool m_ClampDetailWindowContentSize;

		// Token: 0x0400506D RID: 20589
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x40038D0")]
		[SerializeField]
		private Vector2 m_DetailWindowContentMaxSize;

		// Token: 0x0400506E RID: 20590
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x40038D1")]
		[SerializeField]
		private Vector2 m_DetailWindowContentMinSize;

		// Token: 0x0400506F RID: 20591
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x40038D2")]
		private bool m_AdjustScroll;
	}
}
