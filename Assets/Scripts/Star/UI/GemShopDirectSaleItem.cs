﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DE6 RID: 3558
	[Token(Token = "0x200098C")]
	[StructLayout(3)]
	public class GemShopDirectSaleItem : ScrollItemIcon
	{
		// Token: 0x060041C7 RID: 16839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C53")]
		[Address(RVA = "0x1014A5C78", Offset = "0x14A5C78", VA = "0x1014A5C78", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060041C8 RID: 16840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C54")]
		[Address(RVA = "0x1014A5CA0", Offset = "0x14A5CA0", VA = "0x1014A5CA0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060041C9 RID: 16841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C55")]
		[Address(RVA = "0x1014A65EC", Offset = "0x14A65EC", VA = "0x1014A65EC")]
		public void HelpButtonCallback()
		{
		}

		// Token: 0x060041CA RID: 16842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C56")]
		[Address(RVA = "0x1014A6610", Offset = "0x14A6610", VA = "0x1014A6610")]
		public GemShopDirectSaleItem()
		{
		}

		// Token: 0x0400517F RID: 20863
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40039A4")]
		[SerializeField]
		private Text m_SalesTitle;

		// Token: 0x04005180 RID: 20864
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40039A5")]
		[SerializeField]
		private Text m_SalesPeriod;

		// Token: 0x04005181 RID: 20865
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40039A6")]
		[SerializeField]
		private CustomButton m_HelpButton;

		// Token: 0x04005182 RID: 20866
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40039A7")]
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x04005183 RID: 20867
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40039A8")]
		[SerializeField]
		private Image m_LimitedRibbon;

		// Token: 0x04005184 RID: 20868
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40039A9")]
		[SerializeField]
		private Image[] m_RestockTypeLabel;

		// Token: 0x04005185 RID: 20869
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40039AA")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x04005186 RID: 20870
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40039AB")]
		[SerializeField]
		private Text m_StockTitle;

		// Token: 0x04005187 RID: 20871
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40039AC")]
		[SerializeField]
		private Text m_StockCount;

		// Token: 0x04005188 RID: 20872
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40039AD")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x04005189 RID: 20873
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40039AE")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400518A RID: 20874
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40039AF")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x0400518B RID: 20875
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40039B0")]
		[SerializeField]
		private GameObject m_SoldOutObject;

		// Token: 0x0400518C RID: 20876
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40039B1")]
		[SerializeField]
		private GameObject m_RestockRemainObject;

		// Token: 0x0400518D RID: 20877
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40039B2")]
		[SerializeField]
		private Text m_RestockRemainTitle;

		// Token: 0x0400518E RID: 20878
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40039B3")]
		[SerializeField]
		private Text m_RestockRemainText;

		// Token: 0x0400518F RID: 20879
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40039B4")]
		private GemShopDirectSaleItem.GemShopDirectSaleItemData m_ItemData;

		// Token: 0x02000DE7 RID: 3559
		[Token(Token = "0x2001120")]
		public class GemShopDirectSaleItemData : ScrollItemData
		{
			// Token: 0x060041CB RID: 16843 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006227")]
			[Address(RVA = "0x1014A0D54", Offset = "0x14A0D54", VA = "0x1014A0D54")]
			public GemShopDirectSaleItemData(StoreManager.Product product, Action<int> callback, GemShopCommon gemShopCommon)
			{
			}

			// Token: 0x060041CC RID: 16844 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006228")]
			[Address(RVA = "0x1014A6618", Offset = "0x14A6618", VA = "0x1014A6618", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x060041CD RID: 16845 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006229")]
			[Address(RVA = "0x1014A6608", Offset = "0x14A6608", VA = "0x1014A6608")]
			public GemShopCommon GetGemShopCommon()
			{
				return null;
			}

			// Token: 0x04005190 RID: 20880
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B24")]
			public StoreManager.Product m_Product;

			// Token: 0x04005191 RID: 20881
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006B25")]
			public string m_PeriodText;

			// Token: 0x04005192 RID: 20882
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006B26")]
			public string m_RestockRemainText;

			// Token: 0x04005193 RID: 20883
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006B27")]
			public bool m_SoldOut;

			// Token: 0x04005194 RID: 20884
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4006B28")]
			public ePresentType m_PresentType;

			// Token: 0x04005195 RID: 20885
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006B29")]
			public int m_PresentID;

			// Token: 0x04005196 RID: 20886
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006B2A")]
			private GemShopCommon m_GemShopCommon;

			// Token: 0x04005197 RID: 20887
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006B2B")]
			private Action<int> OnClickCallBack;
		}
	}
}
