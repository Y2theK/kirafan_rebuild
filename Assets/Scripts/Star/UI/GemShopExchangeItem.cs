﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DE8 RID: 3560
	[Token(Token = "0x200098D")]
	[StructLayout(3)]
	public class GemShopExchangeItem : ScrollItemIcon
	{
		// Token: 0x060041CE RID: 16846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C57")]
		[Address(RVA = "0x1014A6680", Offset = "0x14A6680", VA = "0x1014A6680", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060041CF RID: 16847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C58")]
		[Address(RVA = "0x1014A66A8", Offset = "0x14A66A8", VA = "0x1014A66A8", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060041D0 RID: 16848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C59")]
		[Address(RVA = "0x1014A7190", Offset = "0x14A7190", VA = "0x1014A7190")]
		public void HelpButtonCallback()
		{
		}

		// Token: 0x060041D1 RID: 16849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5A")]
		[Address(RVA = "0x1014A71B4", Offset = "0x14A71B4", VA = "0x1014A71B4")]
		public GemShopExchangeItem()
		{
		}

		// Token: 0x04005198 RID: 20888
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40039B5")]
		[SerializeField]
		private Text m_SalesTitle;

		// Token: 0x04005199 RID: 20889
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40039B6")]
		[SerializeField]
		private Text m_SalesPeriod;

		// Token: 0x0400519A RID: 20890
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40039B7")]
		[SerializeField]
		private CustomButton m_HelpButton;

		// Token: 0x0400519B RID: 20891
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40039B8")]
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x0400519C RID: 20892
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40039B9")]
		[SerializeField]
		private ColorGroup m_ClonerColorGroup;

		// Token: 0x0400519D RID: 20893
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40039BA")]
		[SerializeField]
		private Image m_LimitedRibbon;

		// Token: 0x0400519E RID: 20894
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40039BB")]
		[SerializeField]
		private Image[] m_RestockTypeLabel;

		// Token: 0x0400519F RID: 20895
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40039BC")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x040051A0 RID: 20896
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40039BD")]
		[SerializeField]
		private Text m_StockTitle;

		// Token: 0x040051A1 RID: 20897
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40039BE")]
		[SerializeField]
		private Text m_StockCount;

		// Token: 0x040051A2 RID: 20898
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40039BF")]
		[SerializeField]
		private Text m_PriceTitle;

		// Token: 0x040051A3 RID: 20899
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40039C0")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x040051A4 RID: 20900
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40039C1")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040051A5 RID: 20901
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40039C2")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040051A6 RID: 20902
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40039C3")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x040051A7 RID: 20903
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40039C4")]
		[SerializeField]
		private ColorGroup m_DecideButtonColorGroup;

		// Token: 0x040051A8 RID: 20904
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40039C5")]
		[SerializeField]
		private GameObject m_SoldOutObject;

		// Token: 0x040051A9 RID: 20905
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40039C6")]
		[SerializeField]
		private GameObject m_RestockRemainObject;

		// Token: 0x040051AA RID: 20906
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40039C7")]
		[SerializeField]
		private Text m_RestockRemainTitle;

		// Token: 0x040051AB RID: 20907
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40039C8")]
		[SerializeField]
		private Text m_RestockRemainText;

		// Token: 0x040051AC RID: 20908
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40039C9")]
		private GemShopExchangeItem.GemShopExchangeItemData m_ItemData;

		// Token: 0x02000DE9 RID: 3561
		[Token(Token = "0x2001121")]
		public class GemShopExchangeItemData : ScrollItemData
		{
			// Token: 0x060041D2 RID: 16850 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600622A")]
			[Address(RVA = "0x1014A16B8", Offset = "0x14A16B8", VA = "0x1014A16B8")]
			public GemShopExchangeItemData(StoreManager.Product product, Action<StoreManager.Product> callback, GemShopCommon gemShopCommon)
			{
			}

			// Token: 0x060041D3 RID: 16851 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600622B")]
			[Address(RVA = "0x1014A71BC", Offset = "0x14A71BC", VA = "0x1014A71BC", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x060041D4 RID: 16852 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600622C")]
			[Address(RVA = "0x1014A71AC", Offset = "0x14A71AC", VA = "0x1014A71AC")]
			public GemShopCommon GetGemShopCommon()
			{
				return null;
			}

			// Token: 0x040051AD RID: 20909
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B2C")]
			public StoreManager.Product m_Product;

			// Token: 0x040051AE RID: 20910
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006B2D")]
			public string m_PeriodText;

			// Token: 0x040051AF RID: 20911
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006B2E")]
			public string m_RestockRemainText;

			// Token: 0x040051B0 RID: 20912
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006B2F")]
			public bool m_Available;

			// Token: 0x040051B1 RID: 20913
			[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
			[Token(Token = "0x4006B30")]
			public bool m_SoldOut;

			// Token: 0x040051B2 RID: 20914
			[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
			[Token(Token = "0x4006B31")]
			public ePresentType m_PresentType;

			// Token: 0x040051B3 RID: 20915
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006B32")]
			public int m_PresentID;

			// Token: 0x040051B4 RID: 20916
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006B33")]
			public string m_ProductName;

			// Token: 0x040051B5 RID: 20917
			[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
			[Token(Token = "0x4006B34")]
			public string m_ProductDesc;

			// Token: 0x040051B6 RID: 20918
			[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
			[Token(Token = "0x4006B35")]
			private GemShopCommon m_GemShopCommon;

			// Token: 0x040051B7 RID: 20919
			[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
			[Token(Token = "0x4006B36")]
			private Action<StoreManager.Product> OnClickCallBack;
		}
	}
}
