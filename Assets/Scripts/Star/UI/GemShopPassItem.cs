﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DC8 RID: 3528
	[Token(Token = "0x2000978")]
	[StructLayout(3)]
	public class GemShopPassItem : ScrollItemIcon
	{
		// Token: 0x06004124 RID: 16676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB2")]
		[Address(RVA = "0x1014A7210", Offset = "0x14A7210", VA = "0x1014A7210", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004125 RID: 16677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB3")]
		[Address(RVA = "0x1014A7268", Offset = "0x14A7268", VA = "0x1014A7268", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004126 RID: 16678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB4")]
		[Address(RVA = "0x10149F68C", Offset = "0x149F68C", VA = "0x10149F68C")]
		public void ReplaceStoreBG(Sprite sprite)
		{
		}

		// Token: 0x06004127 RID: 16679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB5")]
		[Address(RVA = "0x10149F7A4", Offset = "0x149F7A4", VA = "0x10149F7A4")]
		public void ReplaceBanner(Sprite sprite)
		{
		}

		// Token: 0x06004128 RID: 16680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB6")]
		[Address(RVA = "0x1014A7D48", Offset = "0x14A7D48", VA = "0x1014A7D48")]
		public void HelpButtonCallback()
		{
		}

		// Token: 0x06004129 RID: 16681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB7")]
		[Address(RVA = "0x1014A7D64", Offset = "0x14A7D64", VA = "0x1014A7D64")]
		public GemShopPassItem()
		{
		}

		// Token: 0x04005082 RID: 20610
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40038E5")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x04005083 RID: 20611
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40038E6")]
		[SerializeField]
		private Text m_ProductName2;

		// Token: 0x04005084 RID: 20612
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40038E7")]
		[SerializeField]
		private Text m_PurchaseLabel;

		// Token: 0x04005085 RID: 20613
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40038E8")]
		[SerializeField]
		private Text m_PurchaseDetail;

		// Token: 0x04005086 RID: 20614
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40038E9")]
		[SerializeField]
		private Text m_LoginBonusLabel;

		// Token: 0x04005087 RID: 20615
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40038EA")]
		[SerializeField]
		private Text m_LoginBonusDetail;

		// Token: 0x04005088 RID: 20616
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40038EB")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x04005089 RID: 20617
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40038EC")]
		[SerializeField]
		private Image m_ExpiredBG;

		// Token: 0x0400508A RID: 20618
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40038ED")]
		[SerializeField]
		private Text m_Expired;

		// Token: 0x0400508B RID: 20619
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40038EE")]
		[SerializeField]
		private GameObject m_StampBG;

		// Token: 0x0400508C RID: 20620
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40038EF")]
		[SerializeField]
		private Text m_ExpiredDay;

		// Token: 0x0400508D RID: 20621
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40038F0")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x0400508E RID: 20622
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40038F1")]
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x0400508F RID: 20623
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40038F2")]
		[SerializeField]
		private CustomButton m_HelpButton;

		// Token: 0x04005090 RID: 20624
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40038F3")]
		[SerializeField]
		private Image m_StoreBG;

		// Token: 0x04005091 RID: 20625
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40038F4")]
		[SerializeField]
		private Image m_Banner;

		// Token: 0x04005092 RID: 20626
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40038F5")]
		private GemShopPassItemData m_ItemData;
	}
}
