﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DC7 RID: 3527
	[Token(Token = "0x2000977")]
	[StructLayout(3)]
	public class GemShopPassItemData : ScrollItemData
	{
		// Token: 0x06004121 RID: 16673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAF")]
		[Address(RVA = "0x1014A0550", Offset = "0x14A0550", VA = "0x1014A0550")]
		public GemShopPassItemData(StoreManager.Product product, Action<int> callback, GemShopCommon gemShopCommon)
		{
		}

		// Token: 0x06004122 RID: 16674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB0")]
		[Address(RVA = "0x1014A7DF8", Offset = "0x14A7DF8", VA = "0x1014A7DF8", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x06004123 RID: 16675 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003BB1")]
		[Address(RVA = "0x1014A7D40", Offset = "0x14A7D40", VA = "0x1014A7D40")]
		public GemShopCommon GetGemShopCommon()
		{
			return null;
		}

		// Token: 0x04005074 RID: 20596
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40038D7")]
		public int m_ID;

		// Token: 0x04005075 RID: 20597
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40038D8")]
		public string m_Name;

		// Token: 0x04005076 RID: 20598
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40038D9")]
		private Action<int> OnClickCallBack;

		// Token: 0x04005077 RID: 20599
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40038DA")]
		private GemShopCommon m_GemShopCommon;

		// Token: 0x04005078 RID: 20600
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40038DB")]
		public long m_PresentPrice;

		// Token: 0x04005079 RID: 20601
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40038DC")]
		public DateTime? m_ExpiredAt;

		// Token: 0x0400507A RID: 20602
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40038DD")]
		public DateTime? m_PurchaseExpiredAt;

		// Token: 0x0400507B RID: 20603
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40038DE")]
		public int m_ExpiredDay;

		// Token: 0x0400507C RID: 20604
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40038DF")]
		public string m_StoreBGID;

		// Token: 0x0400507D RID: 20605
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40038E0")]
		public string m_BannerID;

		// Token: 0x0400507E RID: 20606
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40038E1")]
		public GemShopPassProductInfo m_ProductItem;

		// Token: 0x0400507F RID: 20607
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40038E2")]
		public GemShopPassProductInfo m_ProductGem;

		// Token: 0x04005080 RID: 20608
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40038E3")]
		public GemShopPassProductInfo m_LoginBonus;

		// Token: 0x04005081 RID: 20609
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40038E4")]
		public bool m_SoldOut;
	}
}
