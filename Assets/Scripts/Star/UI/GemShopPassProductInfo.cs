﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DC6 RID: 3526
	[Token(Token = "0x2000976")]
	[StructLayout(3)]
	public class GemShopPassProductInfo
	{
		// Token: 0x0600411F RID: 16671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAD")]
		[Address(RVA = "0x1014A7DAC", Offset = "0x14A7DAC", VA = "0x1014A7DAC")]
		public GemShopPassProductInfo(ePresentType presentType, int id, int amount, int totalAmount)
		{
		}

		// Token: 0x06004120 RID: 16672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BAE")]
		[Address(RVA = "0x1014A7D6C", Offset = "0x14A7D6C", VA = "0x1014A7D6C")]
		public GemShopPassProductInfo(ePresentType presentType, int id, int amount)
		{
		}

		// Token: 0x04005070 RID: 20592
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40038D3")]
		public ePresentType m_PresentType;

		// Token: 0x04005071 RID: 20593
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x40038D4")]
		public int m_ID;

		// Token: 0x04005072 RID: 20594
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40038D5")]
		public int m_Amount;

		// Token: 0x04005073 RID: 20595
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40038D6")]
		public int m_TotalAmount;
	}
}
