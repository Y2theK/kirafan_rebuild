﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DC9 RID: 3529
	[Token(Token = "0x2000979")]
	[StructLayout(3)]
	public class GemShopPlayer : SingletonMonoBehaviour<GemShopPlayer>
	{
		// Token: 0x0600412A RID: 16682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BB8")]
		[Address(RVA = "0x1014A7E4C", Offset = "0x14A7E4C", VA = "0x1014A7E4C", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x0600412B RID: 16683 RVA: 0x000191A0 File Offset: 0x000173A0
		[Token(Token = "0x6003BB9")]
		[Address(RVA = "0x1014A7E9C", Offset = "0x14A7E9C", VA = "0x1014A7E9C")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x0600412C RID: 16684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBA")]
		[Address(RVA = "0x1014A7EAC", Offset = "0x14A7EAC", VA = "0x1014A7EAC")]
		public void Open(int type, Action OnEnd)
		{
		}

		// Token: 0x0600412D RID: 16685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBB")]
		[Address(RVA = "0x1014A7EB4", Offset = "0x14A7EB4", VA = "0x1014A7EB4")]
		public void Open(int type, Action OnEnd, GemShopCommon.eCategory firstCategory)
		{
		}

		// Token: 0x0600412E RID: 16686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBC")]
		[Address(RVA = "0x1014A7FF4", Offset = "0x14A7FF4", VA = "0x1014A7FF4")]
		public void Close()
		{
		}

		// Token: 0x0600412F RID: 16687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBD")]
		[Address(RVA = "0x1014A80A8", Offset = "0x14A80A8", VA = "0x1014A80A8")]
		public void ForceHide()
		{
		}

		// Token: 0x06004130 RID: 16688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBE")]
		[Address(RVA = "0x1014A80E8", Offset = "0x14A80E8", VA = "0x1014A80E8")]
		private void Update()
		{
		}

		// Token: 0x06004131 RID: 16689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BBF")]
		[Address(RVA = "0x1014A8E34", Offset = "0x14A8E34", VA = "0x1014A8E34")]
		private void OnCloseAgeConfirm()
		{
		}

		// Token: 0x06004132 RID: 16690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC0")]
		[Address(RVA = "0x1014A8EE0", Offset = "0x14A8EE0", VA = "0x1014A8EE0")]
		private void OnCloseAgeConfirm_Cancel()
		{
		}

		// Token: 0x06004133 RID: 16691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC1")]
		[Address(RVA = "0x1014A8F8C", Offset = "0x14A8F8C", VA = "0x1014A8F8C")]
		private void OnCompleteInitProducts()
		{
		}

		// Token: 0x06004134 RID: 16692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC2")]
		[Address(RVA = "0x1014A96C4", Offset = "0x14A96C4", VA = "0x1014A96C4")]
		private void OnRequestPurchaseCallBack(int id)
		{
		}

		// Token: 0x06004135 RID: 16693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC3")]
		[Address(RVA = "0x1014A9810", Offset = "0x14A9810", VA = "0x1014A9810")]
		private void ProductSetShownCallback()
		{
		}

		// Token: 0x06004136 RID: 16694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC4")]
		[Address(RVA = "0x1014A981C", Offset = "0x14A981C", VA = "0x1014A981C")]
		private void RefreshUI()
		{
		}

		// Token: 0x06004137 RID: 16695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC5")]
		[Address(RVA = "0x1014A9848", Offset = "0x14A9848", VA = "0x1014A9848")]
		private void OpenScrollList(string title, string message, string subMessage)
		{
		}

		// Token: 0x06004138 RID: 16696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC6")]
		[Address(RVA = "0x1014A990C", Offset = "0x14A990C", VA = "0x1014A990C")]
		private void BeginReloadUI(int buttonIndex)
		{
		}

		// Token: 0x06004139 RID: 16697 RVA: 0x000191B8 File Offset: 0x000173B8
		[Token(Token = "0x6003BC7")]
		[Address(RVA = "0x1014A8CD8", Offset = "0x14A8CD8", VA = "0x1014A8CD8")]
		private bool ExecReloadUI()
		{
			return default(bool);
		}

		// Token: 0x0600413A RID: 16698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC8")]
		[Address(RVA = "0x1014A99C0", Offset = "0x14A99C0", VA = "0x1014A99C0")]
		public void AddShownList(List<StoreManager.Product> list)
		{
		}

		// Token: 0x0600413B RID: 16699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BC9")]
		[Address(RVA = "0x1014A9A60", Offset = "0x14A9A60", VA = "0x1014A9A60")]
		public GemShopPlayer()
		{
		}

		// Token: 0x04005093 RID: 20627
		[Token(Token = "0x40038F6")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.GemShopUI;

		// Token: 0x04005094 RID: 20628
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40038F7")]
		private GemShopPlayer.eStep m_Step;

		// Token: 0x04005095 RID: 20629
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40038F8")]
		private GemShopUI m_UI;

		// Token: 0x04005096 RID: 20630
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40038F9")]
		private GemShopCommon.eCategory m_FirstCategory;

		// Token: 0x04005097 RID: 20631
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40038FA")]
		private Action m_OnEndCallBack;

		// Token: 0x04005098 RID: 20632
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40038FB")]
		private bool m_IsReloadUI;

		// Token: 0x04005099 RID: 20633
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40038FC")]
		private GemShopPlayer.eReloadStep m_ReloadStep;

		// Token: 0x0400509A RID: 20634
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40038FD")]
		private bool m_FinishProductSetShown;

		// Token: 0x0400509B RID: 20635
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40038FE")]
		private GemShopAPI_Purchase m_API_Purchase;

		// Token: 0x0400509C RID: 20636
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40038FF")]
		private GemShopAPI_SetShown m_API_SetShown;

		// Token: 0x02000DCA RID: 3530
		[Token(Token = "0x2001116")]
		private enum eReloadStep
		{
			// Token: 0x0400509E RID: 20638
			[Token(Token = "0x4006AE7")]
			None,
			// Token: 0x0400509F RID: 20639
			[Token(Token = "0x4006AE8")]
			Close,
			// Token: 0x040050A0 RID: 20640
			[Token(Token = "0x4006AE9")]
			Refresh,
			// Token: 0x040050A1 RID: 20641
			[Token(Token = "0x4006AEA")]
			Open,
			// Token: 0x040050A2 RID: 20642
			[Token(Token = "0x4006AEB")]
			WaitOpen,
			// Token: 0x040050A3 RID: 20643
			[Token(Token = "0x4006AEC")]
			End
		}

		// Token: 0x02000DCB RID: 3531
		[Token(Token = "0x2001117")]
		private enum eStep
		{
			// Token: 0x040050A5 RID: 20645
			[Token(Token = "0x4006AEE")]
			Hide,
			// Token: 0x040050A6 RID: 20646
			[Token(Token = "0x4006AEF")]
			LoadWait,
			// Token: 0x040050A7 RID: 20647
			[Token(Token = "0x4006AF0")]
			SetupAndPlayIn,
			// Token: 0x040050A8 RID: 20648
			[Token(Token = "0x4006AF1")]
			SetAge,
			// Token: 0x040050A9 RID: 20649
			[Token(Token = "0x4006AF2")]
			SetAge_Wait,
			// Token: 0x040050AA RID: 20650
			[Token(Token = "0x4006AF3")]
			SetAge_Cancel,
			// Token: 0x040050AB RID: 20651
			[Token(Token = "0x4006AF4")]
			InitProducts,
			// Token: 0x040050AC RID: 20652
			[Token(Token = "0x4006AF5")]
			InitProducts_Wait,
			// Token: 0x040050AD RID: 20653
			[Token(Token = "0x4006AF6")]
			Main,
			// Token: 0x040050AE RID: 20654
			[Token(Token = "0x4006AF7")]
			UnloadResourceWait,
			// Token: 0x040050AF RID: 20655
			[Token(Token = "0x4006AF8")]
			UnloadWait
		}
	}
}
