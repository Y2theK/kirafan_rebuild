﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DCC RID: 3532
	[Token(Token = "0x200097A")]
	[StructLayout(3)]
	public class GemShopPurchaseListGroup : UIGroup
	{
		// Token: 0x0600413C RID: 16700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCA")]
		[Address(RVA = "0x1014A9AB8", Offset = "0x14A9AB8", VA = "0x1014A9AB8")]
		public void Setup(string title, string message, string subMessage)
		{
		}

		// Token: 0x0600413D RID: 16701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCB")]
		[Address(RVA = "0x1014A9E0C", Offset = "0x14A9E0C", VA = "0x1014A9E0C", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x0600413E RID: 16702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCC")]
		[Address(RVA = "0x1014A9E80", Offset = "0x14A9E80", VA = "0x1014A9E80")]
		public void OnClickButton()
		{
		}

		// Token: 0x0600413F RID: 16703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCD")]
		[Address(RVA = "0x1014A9E8C", Offset = "0x14A9E8C", VA = "0x1014A9E8C")]
		public GemShopPurchaseListGroup()
		{
		}

		// Token: 0x040050B0 RID: 20656
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003900")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x040050B1 RID: 20657
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003901")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x040050B2 RID: 20658
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003902")]
		[SerializeField]
		private Text m_SubMessage;

		// Token: 0x040050B3 RID: 20659
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003903")]
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x040050B4 RID: 20660
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003904")]
		[SerializeField]
		private VerticalLayoutGroup m_ContentVerticalLayout;

		// Token: 0x040050B5 RID: 20661
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003905")]
		private bool m_AdjustPivot;
	}
}
