﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DCD RID: 3533
	[Token(Token = "0x200097B")]
	[StructLayout(3)]
	public class GemShopRecipeItem : ScrollItemIcon
	{
		// Token: 0x06004140 RID: 16704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCE")]
		[Address(RVA = "0x1014A9E94", Offset = "0x14A9E94", VA = "0x1014A9E94", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004141 RID: 16705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BCF")]
		[Address(RVA = "0x1014AA454", Offset = "0x14AA454", VA = "0x1014AA454")]
		public void HelpButtonCallback()
		{
		}

		// Token: 0x06004142 RID: 16706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD0")]
		[Address(RVA = "0x1014AA478", Offset = "0x14AA478", VA = "0x1014AA478")]
		public GemShopRecipeItem()
		{
		}

		// Token: 0x040050B6 RID: 20662
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003906")]
		[SerializeField]
		private Text m_TitleNameText;

		// Token: 0x040050B7 RID: 20663
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003907")]
		[SerializeField]
		private GameObject m_SpecialObj;

		// Token: 0x040050B8 RID: 20664
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003908")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040050B9 RID: 20665
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003909")]
		[SerializeField]
		private Text m_AmountText;

		// Token: 0x040050BA RID: 20666
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400390A")]
		[SerializeField]
		private Text m_ExpiredAt;

		// Token: 0x040050BB RID: 20667
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400390B")]
		[SerializeField]
		private Image m_GemImage;

		// Token: 0x040050BC RID: 20668
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400390C")]
		[SerializeField]
		private Image m_GemPackImage;

		// Token: 0x040050BD RID: 20669
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400390D")]
		[SerializeField]
		private GameObject m_PurchaseLimitObj;

		// Token: 0x040050BE RID: 20670
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400390E")]
		[SerializeField]
		private Text m_PurchaseLimitText;

		// Token: 0x040050BF RID: 20671
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400390F")]
		[SerializeField]
		private Text m_PurchaseLimitLabel;

		// Token: 0x040050C0 RID: 20672
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003910")]
		[SerializeField]
		private GameObject m_HelpButtonObj;

		// Token: 0x040050C1 RID: 20673
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003911")]
		private GemShopRecipeItemData m_ItemData;
	}
}
