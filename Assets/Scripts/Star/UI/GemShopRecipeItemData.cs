﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DCE RID: 3534
	[Token(Token = "0x200097C")]
	[StructLayout(3)]
	public class GemShopRecipeItemData : ScrollItemData
	{
		// Token: 0x06004143 RID: 16707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD1")]
		[Address(RVA = "0x1014A03C0", Offset = "0x14A03C0", VA = "0x1014A03C0")]
		public GemShopRecipeItemData(int id, string title, string detail, long amount, int purchaseLimit, int purchaseCount, StoreDefine.eUIType uiType, StoreDefine.eProductType productType, DateTime? expiredAt, Action<int> onClick, GemShopCommon gemShopCommon)
		{
		}

		// Token: 0x06004144 RID: 16708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD2")]
		[Address(RVA = "0x1014AA480", Offset = "0x14AA480", VA = "0x1014AA480", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x06004145 RID: 16709 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003BD3")]
		[Address(RVA = "0x1014AA470", Offset = "0x14AA470", VA = "0x1014AA470")]
		public GemShopCommon GetGemShopCommon()
		{
			return null;
		}

		// Token: 0x040050C2 RID: 20674
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003912")]
		public int m_Id;

		// Token: 0x040050C3 RID: 20675
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003913")]
		public string m_Title;

		// Token: 0x040050C4 RID: 20676
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003914")]
		public string m_Detail;

		// Token: 0x040050C5 RID: 20677
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003915")]
		public string m_Amount;

		// Token: 0x040050C6 RID: 20678
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003916")]
		public int m_PurchaseLimit;

		// Token: 0x040050C7 RID: 20679
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003917")]
		public int m_PurchaseCount;

		// Token: 0x040050C8 RID: 20680
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003918")]
		public StoreDefine.eUIType m_UIType;

		// Token: 0x040050C9 RID: 20681
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003919")]
		public StoreDefine.eProductType m_ProductType;

		// Token: 0x040050CA RID: 20682
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400391A")]
		public DateTime? m_ExpiredAt;

		// Token: 0x040050CB RID: 20683
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400391B")]
		private GemShopCommon m_GemShopCommon;

		// Token: 0x040050CC RID: 20684
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400391C")]
		private Action<int> OnClickCallBack;
	}
}
