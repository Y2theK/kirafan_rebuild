﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DD0 RID: 3536
	[Token(Token = "0x200097E")]
	[StructLayout(3)]
	public class GemShopRegularPassItem : ScrollItemIcon
	{
		// Token: 0x06004149 RID: 16713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD7")]
		[Address(RVA = "0x1014AA4D4", Offset = "0x14AA4D4", VA = "0x1014AA4D4", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x0600414A RID: 16714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD8")]
		[Address(RVA = "0x1014AA51C", Offset = "0x14AA51C", VA = "0x1014AA51C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600414B RID: 16715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD9")]
		[Address(RVA = "0x10149FA44", Offset = "0x149FA44", VA = "0x10149FA44")]
		public void ReplaceBanner(Sprite sprite)
		{
		}

		// Token: 0x0600414C RID: 16716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BDA")]
		[Address(RVA = "0x1014AAC1C", Offset = "0x14AAC1C", VA = "0x1014AAC1C")]
		public void HelpButtonCallback()
		{
		}

		// Token: 0x0600414D RID: 16717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BDB")]
		[Address(RVA = "0x1014AAC38", Offset = "0x14AAC38", VA = "0x1014AAC38")]
		public GemShopRegularPassItem()
		{
		}

		// Token: 0x040050D9 RID: 20697
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003929")]
		[SerializeField]
		private Sprite[] m_SpriteTable;

		// Token: 0x040050DA RID: 20698
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400392A")]
		[SerializeField]
		private Image m_ProductImage;

		// Token: 0x040050DB RID: 20699
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400392B")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x040050DC RID: 20700
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400392C")]
		[SerializeField]
		private Text m_ProductName2;

		// Token: 0x040050DD RID: 20701
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400392D")]
		[SerializeField]
		private Text m_Expired;

		// Token: 0x040050DE RID: 20702
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400392E")]
		[SerializeField]
		private Text m_ReloadRemaining;

		// Token: 0x040050DF RID: 20703
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400392F")]
		[SerializeField]
		private Text m_ProductText;

		// Token: 0x040050E0 RID: 20704
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003930")]
		[SerializeField]
		private GameObject m_StampBG;

		// Token: 0x040050E1 RID: 20705
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003931")]
		[SerializeField]
		private Text m_WariningText;

		// Token: 0x040050E2 RID: 20706
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003932")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x040050E3 RID: 20707
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003933")]
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x040050E4 RID: 20708
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003934")]
		[SerializeField]
		private GameObject m_BalloonObj;

		// Token: 0x040050E5 RID: 20709
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003935")]
		[SerializeField]
		private Image m_BalloonImage;

		// Token: 0x040050E6 RID: 20710
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003936")]
		[SerializeField]
		private GameObject[] m_WhiteMask;

		// Token: 0x040050E7 RID: 20711
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003937")]
		private GemShopRegularPassItemData m_ItemData;
	}
}
