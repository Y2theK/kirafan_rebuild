﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DCF RID: 3535
	[Token(Token = "0x200097D")]
	[StructLayout(3)]
	public class GemShopRegularPassItemData : ScrollItemData
	{
		// Token: 0x06004146 RID: 16710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD4")]
		[Address(RVA = "0x1014A0988", Offset = "0x14A0988", VA = "0x1014A0988")]
		public GemShopRegularPassItemData(StoreManager.Product product, Action<int> callback, GemShopCommon gemShopCommon)
		{
		}

		// Token: 0x06004147 RID: 16711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BD5")]
		[Address(RVA = "0x1014AAC40", Offset = "0x14AAC40", VA = "0x1014AAC40", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x06004148 RID: 16712 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003BD6")]
		[Address(RVA = "0x1014AAC14", Offset = "0x14AAC14", VA = "0x1014AAC14")]
		public GemShopCommon GetGemShopCommon()
		{
			return null;
		}

		// Token: 0x040050CD RID: 20685
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400391D")]
		public int m_ID;

		// Token: 0x040050CE RID: 20686
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400391E")]
		public string m_Name;

		// Token: 0x040050CF RID: 20687
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400391F")]
		private Action<int> OnClickCallBack;

		// Token: 0x040050D0 RID: 20688
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003920")]
		private GemShopCommon m_GemShopCommon;

		// Token: 0x040050D1 RID: 20689
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003921")]
		public StoreDefine.eRegularPassType m_Type;

		// Token: 0x040050D2 RID: 20690
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003922")]
		public long m_PresentPrice;

		// Token: 0x040050D3 RID: 20691
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003923")]
		public string m_ExpiredAt;

		// Token: 0x040050D4 RID: 20692
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003924")]
		public string m_ReloadAt;

		// Token: 0x040050D5 RID: 20693
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003925")]
		public int m_OverlapDay;

		// Token: 0x040050D6 RID: 20694
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003926")]
		public string m_ProductText;

		// Token: 0x040050D7 RID: 20695
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003927")]
		public string m_BannerID;

		// Token: 0x040050D8 RID: 20696
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003928")]
		public bool m_SoldOut;
	}
}
