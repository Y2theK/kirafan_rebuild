﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DD1 RID: 3537
	[Token(Token = "0x200097F")]
	[StructLayout(3)]
	public class GemShopScrollView : ScrollViewBase
	{
		// Token: 0x0600414E RID: 16718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BDC")]
		[Address(RVA = "0x1014A22A0", Offset = "0x14A22A0", VA = "0x1014A22A0")]
		public void SetCurrentPrefab(int category, int index)
		{
		}

		// Token: 0x0600414F RID: 16719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BDD")]
		[Address(RVA = "0x1014AAC94", Offset = "0x14AAC94", VA = "0x1014AAC94")]
		public GemShopScrollView()
		{
		}

		// Token: 0x040050E8 RID: 20712
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003938")]
		[SerializeField]
		protected GemShopScrollView.ScrollItemIconArray[] m_ItemIconPrefabList;

		// Token: 0x02000DD2 RID: 3538
		[Token(Token = "0x2001118")]
		[Serializable]
		protected class ScrollItemIconArray
		{
			// Token: 0x06004150 RID: 16720 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006225")]
			[Address(RVA = "0x1014AAC9C", Offset = "0x14AAC9C", VA = "0x1014AAC9C")]
			public ScrollItemIconArray()
			{
			}

			// Token: 0x040050E9 RID: 20713
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006AF9")]
			public ScrollItemIcon[] m_Array;
		}
	}
}
