﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DD3 RID: 3539
	[Token(Token = "0x2000980")]
	[StructLayout(3)]
	public class GemShopUI : MenuUIBase
	{
		// Token: 0x1400007A RID: 122
		// (add) Token: 0x06004151 RID: 16721 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004152 RID: 16722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007A")]
		public event Action<int> OnRequestPurchase
		{
			[Token(Token = "0x6003BDE")]
			[Address(RVA = "0x1014A8B54", Offset = "0x14A8B54", VA = "0x1014A8B54")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003BDF")]
			[Address(RVA = "0x1014AACA4", Offset = "0x14AACA4", VA = "0x1014AACA4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004153 RID: 16723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE0")]
		[Address(RVA = "0x1014AADB0", Offset = "0x14AADB0", VA = "0x1014AADB0")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004154 RID: 16724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE1")]
		[Address(RVA = "0x1014AADDC", Offset = "0x14AADDC", VA = "0x1014AADDC", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004155 RID: 16725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE2")]
		[Address(RVA = "0x1014A8C60", Offset = "0x14A8C60", VA = "0x1014A8C60")]
		public void UnloadResource()
		{
		}

		// Token: 0x06004156 RID: 16726 RVA: 0x000191D0 File Offset: 0x000173D0
		[Token(Token = "0x6003BE3")]
		[Address(RVA = "0x1014A8DF0", Offset = "0x14A8DF0", VA = "0x1014A8DF0")]
		public bool IsCompleteUnload()
		{
			return default(bool);
		}

		// Token: 0x06004157 RID: 16727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE4")]
		[Address(RVA = "0x1014A8750", Offset = "0x14A8750", VA = "0x1014A8750")]
		public void Setup(GemShopCommon.eCategory firstCategory = GemShopCommon.eCategory.Gem)
		{
		}

		// Token: 0x06004158 RID: 16728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE5")]
		[Address(RVA = "0x1014AAEE4", Offset = "0x14AAEE4", VA = "0x1014AAEE4")]
		private void SetupTab(GemShopCommon.eCategory initialCategory)
		{
		}

		// Token: 0x06004159 RID: 16729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE6")]
		[Address(RVA = "0x1014A9070", Offset = "0x14A9070", VA = "0x1014A9070")]
		public void Refresh()
		{
		}

		// Token: 0x0600415A RID: 16730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE7")]
		[Address(RVA = "0x1014AB640", Offset = "0x14AB640", VA = "0x1014AB640")]
		private void RefreshNewBadgePosition()
		{
		}

		// Token: 0x0600415B RID: 16731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE8")]
		[Address(RVA = "0x1014AB8A4", Offset = "0x14AB8A4", VA = "0x1014AB8A4")]
		private void Update()
		{
		}

		// Token: 0x0600415C RID: 16732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BE9")]
		[Address(RVA = "0x1014ABA8C", Offset = "0x14ABA8C", VA = "0x1014ABA8C")]
		private void ChangeStep(GemShopUI.eStep step)
		{
		}

		// Token: 0x0600415D RID: 16733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BEA")]
		[Address(RVA = "0x1014ABB78", Offset = "0x14ABB78", VA = "0x1014ABB78", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600415E RID: 16734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BEB")]
		[Address(RVA = "0x1014ABC34", Offset = "0x14ABC34", VA = "0x1014ABC34", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600415F RID: 16735 RVA: 0x000191E8 File Offset: 0x000173E8
		[Token(Token = "0x6003BEC")]
		[Address(RVA = "0x1014ABD20", Offset = "0x14ABD20", VA = "0x1014ABD20", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004160 RID: 16736 RVA: 0x00019200 File Offset: 0x00017400
		[Token(Token = "0x6003BED")]
		[Address(RVA = "0x1014A99B0", Offset = "0x14A99B0", VA = "0x1014A99B0")]
		public bool IsOpened()
		{
			return default(bool);
		}

		// Token: 0x06004161 RID: 16737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BEE")]
		[Address(RVA = "0x1014ABD30", Offset = "0x14ABD30", VA = "0x1014ABD30")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004162 RID: 16738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BEF")]
		[Address(RVA = "0x1014ABDAC", Offset = "0x14ABDAC", VA = "0x1014ABDAC")]
		private void OnClickRecipeButtonCallBack(int id)
		{
		}

		// Token: 0x06004163 RID: 16739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF0")]
		[Address(RVA = "0x1014ABE0C", Offset = "0x14ABE0C", VA = "0x1014ABE0C")]
		private void OpenDetailWindow(string title, string message, string buttonText, bool existImage)
		{
		}

		// Token: 0x06004164 RID: 16740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF1")]
		[Address(RVA = "0x1014ABEBC", Offset = "0x14ABEBC", VA = "0x1014ABEBC")]
		private void ReplaceRegularPassHelpBanner(Sprite sprite)
		{
		}

		// Token: 0x06004165 RID: 16741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF2")]
		[Address(RVA = "0x1014ABF48", Offset = "0x14ABF48", VA = "0x1014ABF48")]
		public void OnClickURLButton(int idx)
		{
		}

		// Token: 0x06004166 RID: 16742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF3")]
		[Address(RVA = "0x1014ABF78", Offset = "0x14ABF78", VA = "0x1014ABF78")]
		private void OnChangeTabCallBack(int tabIndex)
		{
		}

		// Token: 0x06004167 RID: 16743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF4")]
		[Address(RVA = "0x1014A9894", Offset = "0x14A9894", VA = "0x1014A9894")]
		public void OpenPurchaseListWindow(string title, string message, string subMessage)
		{
		}

		// Token: 0x06004168 RID: 16744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF5")]
		[Address(RVA = "0x1014ABF80", Offset = "0x14ABF80", VA = "0x1014ABF80")]
		public GemShopUI()
		{
		}

		// Token: 0x040050EA RID: 20714
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003939")]
		private GemShopCommon.eCategory m_CurrentCategory;

		// Token: 0x040050EB RID: 20715
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x400393A")]
		private GemShopUI.eStep m_Step;

		// Token: 0x040050EC RID: 20716
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400393B")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040050ED RID: 20717
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400393C")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040050EE RID: 20718
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400393D")]
		[SerializeField]
		private GemShopDetailGroup m_DetailGroup;

		// Token: 0x040050EF RID: 20719
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400393E")]
		[SerializeField]
		private GemShopDetailContainerGroup m_DetailContainerGroup;

		// Token: 0x040050F0 RID: 20720
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400393F")]
		[SerializeField]
		private GemShopPurchaseListGroup m_PurchaseListGroup;

		// Token: 0x040050F1 RID: 20721
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003940")]
		[SerializeField]
		private GemShopScrollView m_Scroll;

		// Token: 0x040050F2 RID: 20722
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003941")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x040050F3 RID: 20723
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003942")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x040050F4 RID: 20724
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003943")]
		[SerializeField]
		private Badge m_NewBadgePrefab;

		// Token: 0x040050F5 RID: 20725
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003944")]
		private Vector2[] m_TabPosition;

		// Token: 0x040050F6 RID: 20726
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003945")]
		private List<StoreDefine.eProductType>[] m_ProductTypeTable;

		// Token: 0x040050F7 RID: 20727
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003946")]
		private Badge[] m_NewBadge;

		// Token: 0x040050F8 RID: 20728
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003947")]
		private bool m_UnloadResource;

		// Token: 0x040050F9 RID: 20729
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003948")]
		private GemShopCommon m_GemShopCommon;

		// Token: 0x02000DD4 RID: 3540
		[Token(Token = "0x2001119")]
		private enum eStep
		{
			// Token: 0x040050FC RID: 20732
			[Token(Token = "0x4006AFB")]
			Hide,
			// Token: 0x040050FD RID: 20733
			[Token(Token = "0x4006AFC")]
			In,
			// Token: 0x040050FE RID: 20734
			[Token(Token = "0x4006AFD")]
			Idle,
			// Token: 0x040050FF RID: 20735
			[Token(Token = "0x4006AFE")]
			Out,
			// Token: 0x04005100 RID: 20736
			[Token(Token = "0x4006AFF")]
			End
		}
	}
}
