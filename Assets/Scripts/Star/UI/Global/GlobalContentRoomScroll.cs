﻿namespace Star.UI.Global {
    public class GlobalContentRoomScroll : ScrollViewBase {
        public void UnloadAll() {
            for (int i = 0; i < m_DispItemList.Count; i++) {
                m_DispItemList[i].m_ItemIconObject.Destroy();
            }
            GameSystem.Inst.SpriteMng.ForceSetCacheClearFlg(SpriteManager.eSpriteType.TitleIcon);
        }
    }
}
