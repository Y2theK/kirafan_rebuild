﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global {
    public class GlobalContentRoomScrollItem : ScrollItemIcon {
        [SerializeField] protected CustomButton m_TransitButton;
        [SerializeField] protected GameObject m_UnlockedObj;
        [SerializeField] protected TitleIcon m_TitleIcon;
        [SerializeField] protected PrefabCloner m_OfferStatusCloner;
        [SerializeField] protected Sprite m_SpriteOccurred;
        [SerializeField] protected Sprite m_SpriteWIP;
        [SerializeField] protected Sprite m_SpriteWIPReach;
        [SerializeField] protected GameObject m_AttentionObj;
        [SerializeField] protected Text m_LockedText;

        protected override void ApplyData() {
            base.ApplyData();
            if (m_Data != null) {
                ItemData data = (ItemData)m_Data;
                if (data.m_TitleData != null) {
                    eTitleType titleType = data.m_TitleData.m_TitleType;
                    bool unlocked = data.m_TitleData.m_IsUnlocked;
                    m_TransitButton.Interactable = unlocked;
                    m_UnlockedObj.SetActive(unlocked);
                    m_LockedText.gameObject.SetActive(!unlocked);
                    m_AttentionObj.SetActive(data.m_TitleData.IsAttention());
                    if (unlocked) {
                        m_LockedText.text = null;
                        m_TitleIcon.Apply(titleType);
                        m_OfferStatusCloner.GetInst<OfferTitleDataStatus>().Apply(data.m_TitleData);
                    } else {
                        string displayName = GameSystem.Inst.DbMng.TitleListDB.GetParam(titleType).m_DisplayName;
                        GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.ContentRoomListLockedText, displayName);
                    }
                }
            }
        }

        public override void Destroy() {
            base.Destroy();
            m_TitleIcon.Destroy();
        }

        public class ItemData : ScrollItemData {
            public OfferManager.TitleData m_TitleData;
            public Action<OfferManager.TitleData> m_OnClick;

            public ItemData(OfferManager.TitleData titleData, Action<OfferManager.TitleData> onClick) {
                m_TitleData = titleData;
                m_OnClick = onClick;
            }

            public override void OnClickItemCallBack() {
                base.OnClickItemCallBack();
                m_OnClick.Call(m_TitleData);
            }
        }
    }
}
