﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Global {
    public class GlobalContentRoomWindow : MonoBehaviour {
        private const int CONTENTROOMLIST_BADGE_POS_X = 126;
        private const int CONTENTROOMLIST_BADGE_POS_Y = -4;

        [SerializeField] private GameObject m_MainOfferObj;
        [SerializeField] private GameObject m_SubOfferObj;
        [SerializeField] private TabGroup m_TabGroup;
        [SerializeField] private Badge m_BadgePrefab;
        [SerializeField] private GlobalContentRoomScroll m_MainOfferScroll;
        [SerializeField] private GlobalContentRoomScroll m_SubOfferScroll;

        private Badge[] m_Badges;
        private eMode m_Mode;

        public Action<OfferManager.TitleData> m_OnClick;

        public void Setup() {
            m_MainOfferScroll.Setup();
            m_SubOfferScroll.Setup();
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            string[] buttonTexts = new string[2] {
                dbMng.GetTextCommon(eText_CommonDB.ContentRoomListTabMain),
                dbMng.GetTextCommon(eText_CommonDB.ContentRoomListTabSub)
            };
            m_TabGroup.Setup(buttonTexts);
            m_TabGroup.OnChangeIdx += CallbackChangeTab;
            if (m_Badges == null) {
                m_Badges = new Badge[m_TabGroup.GetButtonNum()];
                for (int i = 0; i < m_Badges.Length; i++) {
                    RectTransform parent = m_TabGroup.GetButton(i).Button.GetComponent<RectTransform>();
                    Badge badge = Instantiate(m_BadgePrefab, parent);
                    badge.GetComponent<RectTransform>().anchoredPosition = new Vector2(CONTENTROOMLIST_BADGE_POS_X, CONTENTROOMLIST_BADGE_POS_Y);
                    m_Badges[i] = badge;
                }
            }
            m_Mode = eMode.MainOffer;
        }

        private void CallbackChangeTab(int idx) {
            ApplyList((eMode)idx);
        }

        public void Unload() {
            m_MainOfferScroll.UnloadAll();
            m_SubOfferScroll.UnloadAll();
        }

        public void SetBadgeValue(int main, int sub) {
            m_Badges[(int)eMode.MainOffer].Apply(main);
            m_Badges[(int)eMode.SubOffer].Apply(sub);
        }

        public eMode GetCurrentMode() {
            return m_Mode;
        }

        public void ApplyList(eMode mode) {
            m_Mode = mode;
            m_MainOfferObj.SetActive(mode == eMode.MainOffer);
            m_SubOfferObj.SetActive(mode == eMode.SubOffer);
            m_TabGroup.ChangeIdx((int)mode);
            OfferManager.TitleData[] titles = GameSystem.Inst.OfferMng.GetTitles();
            if (mode == eMode.MainOffer) {
                m_MainOfferScroll.RemoveAllData();
                if (titles != null) {
                    List<OfferManager.TitleData> titleList = new List<OfferManager.TitleData>(titles);
                    titleList.Sort(ContentRoomListSortComp);
                    for (int i = 0; i < titleList.Count; i++) {
                        OfferManager.TitleData title = titleList[i];
                        if (title.m_IsReleased) {
                            m_MainOfferScroll.AddItem(new GlobalContentRoomScrollItem.ItemData(title, m_OnClick));
                        }
                    }
                }
            } else if (mode == eMode.SubOffer) {
                m_SubOfferScroll.RemoveAllData();
                if (titles != null) {
                    List<OfferManager.TitleData> titleList = new List<OfferManager.TitleData>(titles);
                    titleList.Sort(SubOfferListSortComp);
                    for (int i = 0; i < titleList.Count; i++) {
                        OfferManager.TitleData title = titleList[i];
                        if (title.m_IsReleased) {
                            m_SubOfferScroll.AddItem(new GlobalSubOfferScrollItem.ItemData(title, m_OnClick));
                        }
                    }
                }
            }
        }

        private int ContentRoomListSortComp(OfferManager.TitleData l, OfferManager.TitleData r) {
            if (l.m_IsUnlocked == r.m_IsUnlocked) {
                int lOrder = GameSystem.Inst.DbMng.TitleListDB.GetParam(l.m_TitleType).m_Order;
                int rOrder = GameSystem.Inst.DbMng.TitleListDB.GetParam(r.m_TitleType).m_Order;
                return lOrder.CompareTo(rOrder);
            }
            return r.m_IsUnlocked.CompareTo(l.m_IsUnlocked);
        }

        private int SubOfferListSortComp(OfferManager.TitleData l, OfferManager.TitleData r) {
            if (l.m_IsUnlocked == r.m_IsUnlocked) {
                int lOrder = GameSystem.Inst.DbMng.TitleListDB.GetParam(l.m_TitleType).m_Order;
                int rOrder = GameSystem.Inst.DbMng.TitleListDB.GetParam(r.m_TitleType).m_Order;
                return lOrder.CompareTo(rOrder);
            }
            return r.m_IsExistSubOffer.CompareTo(l.m_IsExistSubOffer);
        }

        public enum eMode {
            MainOffer,
            SubOffer
        }
    }
}
