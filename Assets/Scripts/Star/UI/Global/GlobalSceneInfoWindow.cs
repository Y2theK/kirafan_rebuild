﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global {
    public class GlobalSceneInfoWindow : UIGroup {
        [SerializeField] private GameObject[] m_BackButtonObjs;
        [SerializeField] private Image m_SceneTitle;

        private eBackButtonIconType m_BackButtonIconType;
        private eSceneInfo m_CurrentSceneInfo = eSceneInfo.None;
        private bool m_IsChange;
        private SpriteHandler m_Handler;

        public bool IsChange => m_IsChange;

        public void SetBackButtonIconType(eBackButtonIconType iconType) {
            if (m_BackButtonIconType != iconType) {
                m_BackButtonIconType = iconType;
                if (!m_IsChange) {
                    Hide();
                    Open();
                }
            }
        }

        public void SetInfo(eSceneInfo sceneInfo) {
            if (m_CurrentSceneInfo == sceneInfo) {
                Open();
                return;
            }
            m_CurrentSceneInfo = sceneInfo;
            m_SceneTitle.sprite = null;
            m_SceneTitle.enabled = false;
            if (m_Handler != null) {
                GameSystem.Inst.SpriteMng.Unload(m_Handler);
                m_Handler = null;
            }
            if (sceneInfo != eSceneInfo.None) {
                SceneInfoListDB_Param param = GameSystem.Inst.DbMng.SceneInfoListDB.GetParam(sceneInfo);
                m_Handler = GameSystem.Inst.SpriteMng.LoadAsyncSceneTitle(param.m_ResourceName);
                if (!m_IsChange) {
                    Hide();
                    Open();
                }
            }
        }

        public void SetInteractable(bool flg) {
            CustomButtonGroup.Interactable = flg;
        }

        private void ChangeInfo() {
            for (int i = 0; i < m_BackButtonObjs.Length; i++) {
                if (i == (int)m_BackButtonIconType) {
                    m_BackButtonObjs[(int)m_BackButtonIconType].SetActive(true);
                } else {
                    m_BackButtonObjs[i].SetActive(false);
                }
            }
        }

        public override void Open() {
            ChangeInfo();
            base.Open();
        }

        public override void Close() {
            base.Close();
            m_IsChange = false;
        }

        public override void Hide() {
            base.Hide();
            m_IsChange = false;
        }

        private void CloseChange() {
            base.Close();
            m_IsChange = true;
        }

        public override void OnFinishFinalize() {
            base.OnFinishFinalize();
            if (m_IsChange) {
                m_IsChange = false;
                Open();
            }
        }

        public override void Update() {
            base.Update();
            if (!m_SceneTitle.enabled && m_Handler != null && m_Handler.IsAvailable()) {
                m_SceneTitle.sprite = m_Handler.Obj;
                m_SceneTitle.enabled = true;
            }
        }

        public enum eBackButtonIconType {
            Back,
            Home,
            Town
        }
    }
}
