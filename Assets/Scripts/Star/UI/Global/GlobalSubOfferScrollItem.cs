﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global
{
	// Token: 0x02000FD4 RID: 4052
	[Token(Token = "0x2000A8C")]
	[StructLayout(3)]
	public class GlobalSubOfferScrollItem : ScrollItemIcon
	{
		// Token: 0x06004D04 RID: 19716 RVA: 0x0001B378 File Offset: 0x00019578
		[Token(Token = "0x60046A2")]
		[Address(RVA = "0x1014AD7A4", Offset = "0x14AD7A4", VA = "0x1014AD7A4")]
		private bool HaveItem()
		{
			return default(bool);
		}

		// Token: 0x06004D05 RID: 19717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046A3")]
		[Address(RVA = "0x1014AD82C", Offset = "0x14AD82C", VA = "0x1014AD82C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004D06 RID: 19718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046A4")]
		[Address(RVA = "0x1014AE0BC", Offset = "0x14AE0BC", VA = "0x1014AE0BC", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004D07 RID: 19719 RVA: 0x0001B390 File Offset: 0x00019590
		[Token(Token = "0x60046A5")]
		[Address(RVA = "0x1014ADF28", Offset = "0x14ADF28", VA = "0x1014ADF28")]
		private OfferDefine.eState GetSubOfferState(OfferManager.TitleData titleData)
		{
			return OfferDefine.eState.Nothing;
		}

		// Token: 0x06004D08 RID: 19720 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60046A6")]
		[Address(RVA = "0x1014ADF90", Offset = "0x14ADF90", VA = "0x1014ADF90")]
		private string GetSubOfferCount(OfferManager.TitleData titleData, OfferDefine.eState state)
		{
			return null;
		}

		// Token: 0x06004D09 RID: 19721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046A7")]
		[Address(RVA = "0x1014AE0FC", Offset = "0x14AE0FC", VA = "0x1014AE0FC")]
		public GlobalSubOfferScrollItem()
		{
		}

		// Token: 0x04005F02 RID: 24322
		[Token(Token = "0x40042A4")]
		private const int SUBOFFER_COUNTSTOP = 999;

		// Token: 0x04005F03 RID: 24323
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40042A5")]
		[SerializeField]
		protected CustomButton m_TransitButton;

		// Token: 0x04005F04 RID: 24324
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40042A6")]
		[SerializeField]
		protected GameObject m_UnlockedObj;

		// Token: 0x04005F05 RID: 24325
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40042A7")]
		[SerializeField]
		protected TitleIcon m_TitleIcon;

		// Token: 0x04005F06 RID: 24326
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40042A8")]
		[SerializeField]
		protected Image m_StateIcon;

		// Token: 0x04005F07 RID: 24327
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40042A9")]
		[SerializeField]
		protected Sprite m_SpriteNothing;

		// Token: 0x04005F08 RID: 24328
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40042AA")]
		[SerializeField]
		protected Sprite m_SpriteOccurred;

		// Token: 0x04005F09 RID: 24329
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40042AB")]
		[SerializeField]
		protected Sprite m_SpriteWIP;

		// Token: 0x04005F0A RID: 24330
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40042AC")]
		[SerializeField]
		protected Sprite m_SpriteWIPReach;

		// Token: 0x04005F0B RID: 24331
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40042AD")]
		[SerializeField]
		protected Text m_StateDescript;

		// Token: 0x04005F0C RID: 24332
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40042AE")]
		[SerializeField]
		protected Text m_CountText;

		// Token: 0x04005F0D RID: 24333
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40042AF")]
		[SerializeField]
		protected GameObject m_AttentionObj;

		// Token: 0x04005F0E RID: 24334
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40042B0")]
		[SerializeField]
		protected Text m_LockedText;

		// Token: 0x02000FD5 RID: 4053
		[Token(Token = "0x200120C")]
		public class ItemData : ScrollItemData
		{
			// Token: 0x06004D0A RID: 19722 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006313")]
			[Address(RVA = "0x1014C6D24", Offset = "0x14C6D24", VA = "0x1014C6D24")]
			public ItemData(OfferManager.TitleData titleData, Action<OfferManager.TitleData> onClick)
			{
			}

			// Token: 0x06004D0B RID: 19723 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006314")]
			[Address(RVA = "0x1014C6D5C", Offset = "0x14C6D5C", VA = "0x1014C6D5C", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x04005F0F RID: 24335
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006FA1")]
			public OfferManager.TitleData m_TitleData;

			// Token: 0x04005F10 RID: 24336
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006FA2")]
			public Action<OfferManager.TitleData> m_OnClick;
		}
	}
}
