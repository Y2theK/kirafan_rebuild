﻿using Meige;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI.Global {
    public class GlobalUI : MonoBehaviour {
        public static float BADGE_REFRESH_SEC = 1800f;

        [SerializeField] private UIStateController m_StateController;
        [SerializeField] private AnimUIPlayer[] m_Anims;
        [SerializeField] private CustomButtonGroup m_CustomButtonGroup;
        [SerializeField] private UIGroup m_BaseWindow;
        [SerializeField] private GlobalSceneInfoWindow m_SceneInfoWindow;
        [SerializeField] private UIGroup m_ShortCutWindow;
        [SerializeField] private ImageNumbers m_LvImageNumbers;
        [SerializeField] private Image m_ExpGauge;
        [SerializeField] private Text m_ExpRemainText;
        [SerializeField] private Image m_StaminaGauge;
        [SerializeField] private Text m_StaminaText;
        [SerializeField] private GameObject m_StaminaRemainObj;
        [SerializeField] private GameObject m_StaminaRemainBaseObj;
        [SerializeField] private ImageNumbers m_StaminaRemainTimeMin;
        [SerializeField] private ImageNumbers m_StaminaRemainTimeSec;
        [SerializeField] private CustomButton m_AddStaminaButton;
        [SerializeField] private Text m_GemTextObj;
        [SerializeField] private CustomButton m_GemAddButton;
        [SerializeField] private Text m_GoldTextObj;
        [SerializeField] private GameObject m_ShortCutObj;
        [SerializeField] private GameObject m_ContentRoomObj;
        [SerializeField] private GlobalContentRoomWindow m_ContentRoomWindow;
        [SerializeField] private CustomButton m_ShortCutButton;
        [SerializeField] private PrefabCloner m_ShortCutToggleBadge;
        [SerializeField] private CustomButton m_CancelButton;
        [SerializeField] private RectTransform m_ShortCutButtonParent;
        [SerializeField] private ShortCutButton m_ShortCutButtonPrefab;
        [SerializeField] private CustomButton m_BackButton;
        [SerializeField] private GameObject m_BackButtonRaycastTargetObj;
        [SerializeField] private List<GameObject> m_VipLight;
        [SerializeField] private SwitchFade m_MarketSwitchFade;
        [SerializeField] private CanvasGroup m_MarketNewIcon;
        [SerializeField] private CanvasGroup m_MarketUpdateIcon;

        private List<ShortCutButton> m_ShortCutButtonInstList = new List<ShortCutButton>();
        private List<UIGroup> m_EnableInputUIGroupList = new List<UIGroup>();
        private List<SceneInfoStack> m_InfoStack = new List<SceneInfoStack>();

        private eShortCutButton m_SelectedShortCutButton = eShortCutButton.None;
        private int m_CurrentUserLevel = -1;
        private int m_MaxExp = -1;
        private long m_CurrentExp = -1;
        private bool m_BackButtonOnly;
        private long m_GoldOld = -1;
        private long m_GemOld = -1;
        private bool m_IsEnableUpdateDisp = true;
        private bool m_EnableInput = true;

        private StringBuilder m_sb = new StringBuilder();

        private bool m_CurrentIsNewAnyProducts;

        public bool IsSelectedShortCut => m_SelectedShortCutButton != eShortCutButton.None;
        public eShortCutButton SelectedShortCutButton => m_SelectedShortCutButton;

        private event Action<bool> OnClickBackButton;

        public void Setup() {
            m_StateController.Setup((int)eUIState.Num, (int)eUIState.Hide, false);
            m_StateController.AddTransit((int)eUIState.Hide, (int)eUIState.In, true);
            m_StateController.AddTransit((int)eUIState.In, (int)eUIState.Wait, true);
            m_StateController.AddTransit((int)eUIState.In, (int)eUIState.Out, true);
            m_StateController.AddTransit((int)eUIState.Wait, (int)eUIState.Out, true);
            m_StateController.AddTransit((int)eUIState.Wait, (int)eUIState.ShortCutWait, true);
            m_StateController.AddTransit((int)eUIState.Out, (int)eUIState.Hide, true);
            m_StateController.AddTransit((int)eUIState.Out, (int)eUIState.In, true);
            m_StateController.AddTransit((int)eUIState.ShortCutWait, (int)eUIState.Wait, true);
            m_StateController.AddTransit((int)eUIState.ShortCutWait, (int)eUIState.Out, true);
            m_StateController.AddOnEnter((int)eUIState.Wait, OnEnterWait);
            m_StateController.AddOnExit((int)eUIState.Wait, OnExitWait);
            m_CancelButton.gameObject.SetActive(false);
            SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.None);
            SetBackButtonCallBack(null);
            m_ContentRoomWindow.Setup();
            m_ContentRoomWindow.m_OnClick = OnClickContentRoomListCallBack;
            RectTransform[] parents = m_ShortCutButtonParent.GetComponentsInChildren<RectTransform>();
            for (int i = 0; i + 1 < parents.Length && i < SHORTCUT_ALIGNMENTS.Length; i++) {
                eShortCutButton shortcut = SHORTCUT_ALIGNMENTS[i];
                if (shortcut != eShortCutButton.None) {
                    RectTransform parent = parents[i + 1];
                    ShortCutButton button = Instantiate(m_ShortCutButtonPrefab, parent);
                    button.SetShortCutTransit(shortcut);
                    button.OnClick += OnShortCutButtonCallBack;
                    m_ShortCutButtonInstList.Add(button);
                }
            }
            gameObject.SetActive(false);
        }

        public void SetActiveGemAddButton(bool isActive) {
            m_GemAddButton.gameObject.SetActive(isActive);
        }

        private void SetVipEventLight(bool currentIsNewAnyProducts) {
            foreach (GameObject light in m_VipLight) {
                light.SetActive(currentIsNewAnyProducts);
            }
        }

        private void Update() {
            GameSystem game = GameSystem.Inst;
            if (game != null && game.IsAvailable() && m_IsEnableUpdateDisp) {
                UserData userData = game.UserDataMng.UserData;
                SetUserStamina(userData.Stamina.GetValue(), userData.Stamina.GetValueMax());
                SetUserRemainSec(userData.Stamina.GetRemainSec());
                if (m_CurrentUserLevel != userData.Lv) {
                    m_CurrentUserLevel = userData.Lv;
                    SetUserLevel(userData.Lv);
                    m_MaxExp = game.DbMng.MasterRankDB.GetNextExp(userData.Lv);
                }
                if (m_CurrentExp != userData.LvExp) {
                    m_CurrentExp = userData.LvExp;
                    SetUserExp(userData.Lv, userData.LvExp, m_MaxExp);
                }
                if (m_CurrentIsNewAnyProducts != game.StoreMng.IsNewAny) {
                    m_CurrentIsNewAnyProducts = game.StoreMng.IsNewAny;
                    SetVipEventLight(m_CurrentIsNewAnyProducts);
                }
                SetUserGold(userData.Gold);
                SetUserGem(userData.Gem);
                if (game.GlobalParam.RefreshBadgeTimeSec <= 0f) {
                    if (Utility.CheckTapUI(m_BackButtonRaycastTargetObj)) {
                        Request_GetBadge();
                        game.GlobalParam.RefreshBadgeTimeSec = BADGE_REFRESH_SEC;
                    }
                }
                SetMissionBadge(MissionManager.Inst.GetNewListUpNum());
                SetPresentBadge(game.UserDataMng.NoticePresentCount);
                SetFriendBadge(game.UserDataMng.NoticeFriendCount);
                SetTrainingBadge(game.UserDataMng.NoticeTrainingCount);
                SetLibraryBadge(game.UserDataMng.NoticeLibraryCount);
                SetContentRoomBadge(game.UserDataMng.NoticeMainOfferCount, game.UserDataMng.NoticeSubOfferCount);
                ApplyTotalBadge();
            }
            UpdateAndroidBackKey();
            eUIState nowState = (eUIState)m_StateController.NowState;
            if (nowState == eUIState.ShortCutWait) {
                m_StateController.ChangeState((int)eUIState.Wait);
            } else if (nowState == eUIState.Out) {
                if (!m_StateController.IsPlayingInOut()) {
                    bool canTransition = true;
                    for (int i = 0; i < m_Anims.Length; i++) {
                        if (m_Anims[i].State == (int)eUIState.Out) {
                            canTransition = false;
                            break;
                        }
                    }
                    if (canTransition) {
                        m_CancelButton.gameObject.SetActive(false);
                        m_StateController.ChangeState((int)eUIState.Hide);
                        gameObject.SetActive(false);
                    }
                }
            } else if (nowState == eUIState.In) {
                bool canTransition = true;
                for (int i = 0; i < m_Anims.Length; i++) {
                    if (m_Anims[i].State == (int)eUIState.In) {
                        canTransition = false;
                        break;
                    }
                }
                if (canTransition) {
                    m_CancelButton.gameObject.SetActive(false);
                    m_StateController.ChangeState((int)eUIState.Wait);
                }
            }
        }

        public bool Request_GetBadge() {
            PlayerRequestTypes.Getbadgecount param = new PlayerRequestTypes.Getbadgecount();
            MeigewwwParam wwwParam = PlayerRequest.Getbadgecount(param, OnResponse_GetBadge);
            NetworkQueueManager.Request(wwwParam);
            return true;
        }

        private void OnResponse_GetBadge(MeigewwwParam wwwParam) {
            PlayerResponseTypes.Getbadgecount resp = PlayerResponse.Getbadgecount(wwwParam);
            if (resp != null && resp.GetResult() == ResultCode.SUCCESS) {
                UserDataManager userDataMng = GameSystem.Inst.UserDataMng;
                userDataMng.NoticeFriendCount = resp.friendProposedCount;
                userDataMng.NoticePresentCount = resp.presentCount;
                userDataMng.NoticeTrainingCount = resp.trainingCount;
            }
        }

        private void OnEnterWait() {
            m_BaseWindow.SetEnableInput(true);
            m_SceneInfoWindow.SetEnableInput(true);
            UpdateInput();
        }

        private void OnExitWait() {
            m_BaseWindow.SetEnableInput(false);
            m_SceneInfoWindow.SetEnableInput(false);
            UpdateInput();
        }

        public void Open() {
            SetBackButtonCallBack(null);
            OpenKeepCallBack();
        }

        public void Close() {
            SetBackButtonCallBack(null);
            CloseKeepCallBack();
        }

        public void OpenKeepCallBack() {
            gameObject.SetActive(true);
            if (m_StateController.ChangeState((int)eUIState.In)) {
                for (int i = 0; i < m_Anims.Length; i++) {
                    m_Anims[i].PlayIn();
                }
                if (!m_BackButtonOnly) {
                    m_BaseWindow.Open();
                } else {
                    m_BaseWindow.Hide();
                }
                m_SceneInfoWindow.Open();
                UpdateInput();
            }
        }

        public void CloseKeepCallBack() {
            if (m_StateController.ChangeState((int)eUIState.Out)) {
                for (int i = 0; i < m_Anims.Length; i++) {
                    m_Anims[i].PlayOut();
                }
                CloseShortCutWindow();
                m_BaseWindow.Close();
                m_SceneInfoWindow.Close();
                UpdateInput();
            }
        }

        public bool IsOpen() {
            int nowState = m_StateController.NowState;
            return nowState != (int)eUIState.Hide && nowState != (int)eUIState.Out;
        }

        public bool IsWait() {
            return m_StateController.NowState == (int)eUIState.Wait;
        }

        public bool IsHide() {
            return m_StateController.NowState == (int)eUIState.Hide;
        }

        public void SetEnableInput(bool flg) {
            m_EnableInput = flg;
            UpdateInput();
        }

        private void UpdateInput() {
            int enabledUICount = 0;
            for (int i = m_EnableInputUIGroupList.Count; i >= 0; i--) {
                UIGroup group = m_EnableInputUIGroupList[i];
                if (group == null) {
                    m_EnableInputUIGroupList.RemoveAt(i);
                } else if (group.IsEnableInput()) {
                    enabledUICount++;
                }
            }
            bool enabled = m_EnableInput && m_StateController.NowState == (int)eUIState.Wait && enabledUICount > 0;
            if (m_CustomButtonGroup.IsEnableInput != enabled) {
                m_CustomButtonGroup.IsEnableInput = enabled;
            }
        }

        public void SetInteractable(bool flg) {
            SetEnableInput(flg);
        }

        public bool GetInteractable() {
            return m_CustomButtonGroup.Interactable;
        }

        public void SetBackButtonInteractable(bool flg) {
            m_SceneInfoWindow.SetInteractable(flg);
        }

        public void SetBackButtonOnly(bool only) {
            m_BackButtonOnly = only;
            if (m_BackButtonOnly) {
                m_BaseWindow.GameObject.SetActive(false);
            } else {
                m_BaseWindow.GameObject.SetActive(true);
            }
        }

        public void SetBackButtonCallBack(Action<bool> callback) {
            OnClickBackButton = null;
            OnClickBackButton += callback;
        }

        public Action<bool> GetBackButtonCallBack() {
            return OnClickBackButton;
        }

        public void SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo) {
            ClearSceneInfoStack();
            AddSceneInfoStack(backButtonType, sceneInfo);
        }

        public void ClearSceneInfoStack() {
            m_InfoStack.Clear();
        }

        public SceneInfoStack AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo) {
            SceneInfoStack sceneInfoStack = new SceneInfoStack(backButtonType, sceneInfo);
            m_InfoStack.Add(sceneInfoStack);
            if (m_InfoStack.Count > 0) {
                m_SceneInfoWindow.SetBackButtonIconType(m_InfoStack[m_InfoStack.Count - 1].m_BackButtonType);
                m_SceneInfoWindow.SetInfo(m_InfoStack[m_InfoStack.Count - 1].m_SceneInfo);
            }
            return sceneInfoStack;
        }

        public void RemoveSceneInfoStack(SceneInfoStack stack) {
            m_InfoStack.Remove(stack);
            if (m_InfoStack.Count > 0) {
                m_SceneInfoWindow.SetBackButtonIconType(m_InfoStack[m_InfoStack.Count - 1].m_BackButtonType);
                m_SceneInfoWindow.SetInfo(m_InfoStack[m_InfoStack.Count - 1].m_SceneInfo);
            }
        }

        public void ApplySceneInfoLatest() {
            if (m_InfoStack.Count > 0) {
                m_SceneInfoWindow.SetBackButtonIconType(m_InfoStack[m_InfoStack.Count - 1].m_BackButtonType);
                m_SceneInfoWindow.SetInfo(m_InfoStack[m_InfoStack.Count - 1].m_SceneInfo);
            }
        }

        public void CloseSceneInfo() {
            m_SceneInfoWindow.Close();
        }

        public void HideSceneInfo() {
            m_SceneInfoWindow.Hide();
        }

        public bool IsPlayingInOutSceneInfo() {
            return m_SceneInfoWindow.IsPlayingInOut;
        }

        public void SetEnableUpdateDisp(bool flg) {
            m_IsEnableUpdateDisp = flg;
        }

        public void SetUserLevel(int lv) {
            m_LvImageNumbers.SetValue(lv);
        }

        public void SetUserExp(int lv, long exp, long maxExp) {
            if (lv < UIUtility.GetMasterMaxLv()) {
                if (maxExp > 0L) {
                    m_ExpGauge.fillAmount = exp / (float)maxExp;
                } else {
                    m_ExpGauge.fillAmount = 0f;
                }
                m_ExpRemainText.text = (maxExp - exp).ToString();
            } else {
                m_ExpGauge.fillAmount = 1f;
                m_ExpRemainText.text = UIUtility.GetMasterMaxLvRemainString();
            }
        }

        public void SetUserStamina(long stamina, long staminaMax) {
            m_sb.Length = 0;
            m_sb.Append(UIUtility.StaminaValueToString(stamina));
            m_sb.Append("/");
            m_sb.Append(UIUtility.StaminaValueToString(staminaMax, false));
            m_StaminaText.text = m_sb.ToString();
            if (staminaMax > 0L) {
                m_StaminaGauge.fillAmount = stamina / (float)staminaMax;
            } else {
                m_StaminaGauge.fillAmount = 0f;
            }
        }

        public void SetUserRemainSec(float sec) {
            int mins = (int)sec / 60;
            int secs = (int)sec - mins * 60;
            if (sec <= 0f) {
                if (m_StaminaRemainObj.activeSelf) {
                    m_StaminaRemainObj.SetActive(false);
                    m_StaminaRemainBaseObj.SetActive(false);
                }
            } else {
                if (!m_StaminaRemainObj.activeSelf) {
                    m_StaminaRemainObj.SetActive(true);
                    m_StaminaRemainBaseObj.SetActive(true);
                }
                m_StaminaRemainTimeMin.SetValue(mins);
                m_StaminaRemainTimeSec.SetValue(secs);
            }
        }

        public void SetUserGold(long gold) {
            if (m_GoldOld != gold) {
                m_GoldTextObj.text = UIUtility.GoldValueToString(gold);
                m_GoldOld = gold;
            }
        }

        public void SetUserGem(long gem) {
            if (m_GemOld != gem) {
                m_GemTextObj.text = UIUtility.GemValueToString(gem);
                m_GemOld = gem;
            }
        }

        private void SetListMode(eListMode mode) {
            m_ShortCutObj.SetActive(mode == eListMode.ShortCut);
            m_ContentRoomObj.SetActive(mode == eListMode.ContentRoom);
            if (m_ContentRoomObj.activeSelf) {
                m_ContentRoomWindow.ApplyList(m_ContentRoomWindow.GetCurrentMode());
            } else {
                m_ContentRoomWindow.Unload();
            }
            CustomButton[] buttons = m_ShortCutWindow.GetComponentsInChildren<CustomButton>();
            foreach (CustomButton button in buttons) {
                button.SetTransitRate(0f);
            }
        }

        public void ToggleShortCutWindow() {
            if (m_ShortCutWindow.IsOpenOrIn()) {
                if (m_ContentRoomObj.activeSelf) {
                    SetListMode(eListMode.ShortCut);
                } else {
                    CloseShortCutWindow();
                }
            } else {
                OpenShortCutWindow();
            }
        }

        private void OpenShortCutWindow() {
            SetListMode(eListMode.ShortCut);
            m_ShortCutWindow.Open();
            m_CancelButton.gameObject.SetActive(true);
            m_CancelButton.IsEnableInput = true;
            UpdateMarketUI();
        }

        private void UpdateMarketUI() {
            StoreDefine.eBadgeType badge = GameSystem.Inst.StoreMng.GetBadgeType(StoreDefine.eProductType.All);
            List<CanvasGroup> canvasGroups = new List<CanvasGroup>();
            if (badge.HasFlag(StoreDefine.eBadgeType.New)) {
                canvasGroups.Add(m_MarketNewIcon);
            } else {
                m_MarketNewIcon.gameObject.SetActive(false);
            }
            if (badge.HasFlag(StoreDefine.eBadgeType.Update)) {
                canvasGroups.Add(m_MarketUpdateIcon);
            } else {
                m_MarketUpdateIcon.gameObject.SetActive(false);
            }
            m_MarketSwitchFade.SetTarget(canvasGroups.ToArray());
        }

        private void CloseShortCutWindow() {
            if (m_ShortCutWindow.IsOpenOrIn()) {
                m_ShortCutWindow.Close();
                SetListMode(eListMode.ShortCut);
            }
            m_CancelButton.gameObject.SetActive(false);
            m_CancelButton.IsEnableInput = false;
        }

        public bool IsShortCutWindowOpen() {
            return m_ShortCutWindow != null && m_ShortCutWindow.IsOpen();
        }

        public void OnShortCutButtonCallBack(eShortCutButton shortCut) {
            if (shortCut == eShortCutButton.Mission) {
                GameSystem.Inst.MissionMng.OpenUI();
            } else if (shortCut == eShortCutButton.ContentRoom) {
                SetListMode(eListMode.ContentRoom);
            } else if (shortCut == eShortCutButton.Info) {
                InfoUIArg arg = new InfoUIArg(null, null, -1);
                GameSystem.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Info, arg);
            } else {
                StartShortCut(shortCut);
            }
        }

        public void OnClickHomeButtonCallBack() {
            OnShortCutButtonCallBack(eShortCutButton.Home);
        }

        public void OnClickMarketButtonCallBack() {
            GameSystem game = GameSystem.Inst;
            if (game.StoreMng.IsNeedCheckAge()) {
                game.AgeConfirmWindow.Open(() => OnShortCutButtonCallBack(eShortCutButton.Market), null);
            } else {
                OnShortCutButtonCallBack(eShortCutButton.Market);
            }
        }

        public void OpenContentRoomList() {
            if (!m_ShortCutWindow.IsOpen()) {
                OpenShortCutWindow();
            }
            SetListMode(eListMode.ContentRoom);
        }

        public void OnClickContentRoomListCallBack(OfferManager.TitleData titleData) {
            GameSystem game = GameSystem.Inst;
            if (game.GlobalParam.CurrentContentRoomTitleType == titleData.m_TitleType) {
                CloseShortCutWindow();
            } else {
                game.GlobalParam.NextContentRoomTitleType = titleData.m_TitleType;
                StartShortCut(eShortCutButton.ContentRoom);
            }
        }

        private void StartShortCut(eShortCutButton shortCut) {
            if (m_StateController.ChangeState((int)eUIState.ShortCutWait)) {
                m_SelectedShortCutButton = shortCut;
                CloseShortCutWindow();
                if (OnClickBackButton != null) {
                    OnClickBackButton(true);
                } else {
                    StartCoroutine(ShortCutCoroutine());
                }
            }
        }

        private IEnumerator ShortCutCoroutine() {
            GameSystem.Inst.LoadingUI.OpenQuickIfNotDisplay();
            while (!GameSystem.Inst.LoadingUI.IsComplete()) {
                yield return new WaitForEndOfFrame();
            }
            ExecShortCut();
        }

        public bool ExecShortCut() {
            if (m_SelectedShortCutButton == eShortCutButton.None) {
                return false;
            }
            switch (m_SelectedShortCutButton) {
                case eShortCutButton.Home:
                    GameSystem.Inst.GlobalParam.IsHomeModeStart = true;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Town);
                    break;
                case eShortCutButton.Quest:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Quest);
                    break;
                case eShortCutButton.Training:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Training);
                    break;
                case eShortCutButton.Gacha:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Gacha);
                    break;
                case eShortCutButton.Edit:
                    GameSystem.Inst.GlobalParam.IsMenuUpgradeStart = false;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Edit);
                    break;
                case eShortCutButton.Upgrade:
                    GameSystem.Inst.GlobalParam.IsMenuUpgradeStart = true;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Edit);
                    break;
                case eShortCutButton.ShopTrade:
                    GameSystem.Inst.GlobalParam.ShopStartType = GameGlobalParameter.eShopStartType.Trade;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Shop);
                    break;
                case eShortCutButton.ShopWeapon:
                    if (ShopUtility.IsTransitWeaponEvolutionAdv()) {
                        GameGlobalParameter globalParam = GameSystem.Inst.GlobalParam;
                        globalParam.SetAdvParam(ShopDefine.ADV_ID_WPN_EVOLUTION, SceneDefine.eSceneID.Shop);
                        globalParam.ShopStartType = GameGlobalParameter.eShopStartType.Weapon;
                        SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
                    } else {
                        GameSystem.Inst.GlobalParam.ShopStartType = GameGlobalParameter.eShopStartType.Weapon;
                        SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Shop);
                    }
                    break;
                case eShortCutButton.ShopBuild:
                    GameSystem.Inst.GlobalParam.ShopStartType = GameGlobalParameter.eShopStartType.Build;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Shop);
                    break;
                case eShortCutButton.Library:
                    GameSystem.Inst.GlobalParam.IsMenuFriendStart = false;
                    GameSystem.Inst.GlobalParam.IsMenuLibraryStart = true;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Menu);
                    break;
                case eShortCutButton.Room:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Room);
                    break;
                case eShortCutButton.Present:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Present);
                    break;
                case eShortCutButton.Menu:
                    GameSystem.Inst.GlobalParam.IsMenuFriendStart = false;
                    GameSystem.Inst.GlobalParam.IsMenuLibraryStart = false;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Menu);
                    break;
                case eShortCutButton.ContentRoom:
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.ContentRoom);
                    break;
                case eShortCutButton.Market:
                    GameSystem.Inst.GlobalParam.ShopStartType = GameGlobalParameter.eShopStartType.Market;
                    SceneLoader.Inst.TransitSceneAsync(SceneDefine.eSceneID.Shop);
                    break;
            }
            m_SelectedShortCutButton = eShortCutButton.None;
            return true;
        }

        public void ResetSelectedShortCut() {
            m_SelectedShortCutButton = eShortCutButton.None;
        }

        private void SetMissionBadge(int value) {
            SetBadgeValue(eShortCutButton.Mission, value);
        }

        private void SetFriendBadge(int value) {
            SetBadgeValue(eShortCutButton.Menu, value);
        }

        private void SetPresentBadge(int value) {
            SetBadgeValue(eShortCutButton.Present, value);
        }

        private void SetTrainingBadge(int value) {
            SetBadgeValue(eShortCutButton.Training, value);
        }

        private void SetLibraryBadge(int value) {
            SetBadgeValue(eShortCutButton.Library, value);
        }

        private void SetContentRoomBadge(int main, int sub) {
            SetBadgeValue(eShortCutButton.ContentRoom, main + sub);
            m_ContentRoomWindow.SetBadgeValue(main, sub);
        }

        private void SetBadgeValue(eShortCutButton shortCutButton, int value) {
            for (int i = 0; i < m_ShortCutButtonInstList.Count; i++) {
                ShortCutButton button = m_ShortCutButtonInstList[i];
                if (button != null && button.GetShortCutTransit() == shortCutButton) {
                    if (button.GetBadgeValue() != value) {
                        button.SetBadgeValue(value);
                    }
                }
            }
        }

        private void ApplyTotalBadge() {
            int total = 0;
            for (int i = 0; i < m_ShortCutButtonInstList.Count; i++) {
                ShortCutButton button = m_ShortCutButtonInstList[i];
                if (button != null) {
                    total += m_ShortCutButtonInstList[i].GetBadgeValue();
                }
            }
            m_ShortCutToggleBadge.GetInst<Badge>().Apply(total);
        }

        public void OnClickBackButtonCallBack() {
            CloseShortCutWindow();
            ResetSelectedShortCut();
            OnClickBackButton.Call(false);
        }

        public void OnClickAddStaminaButtonCallBack() {
            CloseShortCutWindow();
            StaminaShopPlayer.Inst.Open(0, null);
        }

        public void OnClickAddGemButtonCallBack() {
            CloseShortCutWindow();
            GemShopPlayer.Inst.Open(0, null);
        }

        public void OnClickCancel() {
            CloseShortCutWindow();
        }

        public void SetEnableInputFromUIGroup(UIGroup uiGroup, bool flg) {
            int idx = m_EnableInputUIGroupList.IndexOf(uiGroup);
            if (idx == -1 && flg) {
                m_EnableInputUIGroupList.Add(uiGroup);
            } else if (idx != -1 && !flg) {
                m_EnableInputUIGroupList.RemoveAt(idx);
            }
            UpdateInput();
        }

        private void UpdateAndroidBackKey() {
            UIUtility.UpdateAndroidBackKey(m_BackButton, CheckUpdateAndroidBackKey);
        }

        private bool CheckUpdateAndroidBackKey() {
            return Utility.CheckTapUI(m_BackButtonRaycastTargetObj);
        }

        private readonly eShortCutButton[] SHORTCUT_ALIGNMENTS = new eShortCutButton[15] {
            eShortCutButton.Quest,
            eShortCutButton.Mission,
            eShortCutButton.Training,
            eShortCutButton.Gacha,
            eShortCutButton.Edit,
            eShortCutButton.Upgrade,
            eShortCutButton.ShopTrade,
            eShortCutButton.ShopWeapon,
            eShortCutButton.ShopBuild,
            eShortCutButton.Library,
            eShortCutButton.Room,
            eShortCutButton.ContentRoom,
            eShortCutButton.Present,
            eShortCutButton.Info,
            eShortCutButton.Menu
        };

        private enum eUIState {
            Hide,
            In,
            Wait,
            Out,
            ShortCutWait,
            Num
        }

        private enum eListMode {
            ShortCut,
            ContentRoom
        }

        public class SceneInfoStack {
            public GlobalSceneInfoWindow.eBackButtonIconType m_BackButtonType;
            public eSceneInfo m_SceneInfo;

            public SceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo) {
                m_BackButtonType = backButtonType;
                m_SceneInfo = sceneInfo;
            }
        }

        public enum eShortCutButton {
            None = -1,
            Home,
            Quest,
            Mission,
            Training,
            Gacha,
            Edit,
            Upgrade,
            ShopTrade,
            ShopWeapon,
            ShopBuild,
            Library,
            Room,
            Present,
            Info,
            Menu,
            ContentRoom,
            Market,
            Num
        }
    }
}
