﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global
{
	// Token: 0x02000FDC RID: 4060
	[Token(Token = "0x2000A8E")]
	[StructLayout(3)]
	public class ShortCutButton : MonoBehaviour
	{
		// Token: 0x140000F0 RID: 240
		// (add) Token: 0x06004D5E RID: 19806 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004D5F RID: 19807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000F0")]
		public event Action<GlobalUI.eShortCutButton> OnClick
		{
			[Token(Token = "0x60046F3")]
			[Address(RVA = "0x1014C75C4", Offset = "0x14C75C4", VA = "0x1014C75C4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60046F4")]
			[Address(RVA = "0x1014CB6BC", Offset = "0x14CB6BC", VA = "0x1014CB6BC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004D60 RID: 19808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046F5")]
		[Address(RVA = "0x1014C754C", Offset = "0x14C754C", VA = "0x1014C754C")]
		public void SetShortCutTransit(GlobalUI.eShortCutButton shortCut)
		{
		}

		// Token: 0x06004D61 RID: 19809 RVA: 0x0001B4C8 File Offset: 0x000196C8
		[Token(Token = "0x60046F6")]
		[Address(RVA = "0x1014CAF1C", Offset = "0x14CAF1C", VA = "0x1014CAF1C")]
		public GlobalUI.eShortCutButton GetShortCutTransit()
		{
			return GlobalUI.eShortCutButton.Home;
		}

		// Token: 0x06004D62 RID: 19810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046F7")]
		[Address(RVA = "0x1014CAF2C", Offset = "0x14CAF2C", VA = "0x1014CAF2C")]
		public void SetBadgeValue(int badgeValue = -1)
		{
		}

		// Token: 0x06004D63 RID: 19811 RVA: 0x0001B4E0 File Offset: 0x000196E0
		[Token(Token = "0x60046F8")]
		[Address(RVA = "0x1014CAF24", Offset = "0x14CAF24", VA = "0x1014CAF24")]
		public int GetBadgeValue()
		{
			return 0;
		}

		// Token: 0x06004D64 RID: 19812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046F9")]
		[Address(RVA = "0x1014CB7C8", Offset = "0x14CB7C8", VA = "0x1014CB7C8")]
		public void OnClickButtonCallBack()
		{
		}

		// Token: 0x06004D65 RID: 19813 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60046FA")]
		[Address(RVA = "0x1014CB81C", Offset = "0x14CB81C", VA = "0x1014CB81C")]
		public CustomButton GetButton()
		{
			return null;
		}

		// Token: 0x06004D66 RID: 19814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60046FB")]
		[Address(RVA = "0x1014CB824", Offset = "0x14CB824", VA = "0x1014CB824")]
		public ShortCutButton()
		{
		}

		// Token: 0x04005F66 RID: 24422
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40042E3")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04005F67 RID: 24423
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40042E4")]
		private GlobalUI.eShortCutButton m_ShortCut;

		// Token: 0x04005F68 RID: 24424
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40042E5")]
		[SerializeField]
		private Sprite[] m_Sprite;

		// Token: 0x04005F69 RID: 24425
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40042E6")]
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x04005F6A RID: 24426
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40042E7")]
		[SerializeField]
		private PrefabCloner m_BadgeCloner;

		// Token: 0x04005F6B RID: 24427
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40042E8")]
		private int m_BadgeValue;
	}
}
