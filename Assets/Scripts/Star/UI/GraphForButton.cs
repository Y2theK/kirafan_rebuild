﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class GraphForButton : MonoBehaviour {
        private static readonly Color BASE_COLOR = new Color(1f, 1f, 1f);
        private static readonly Color TEXT_COLOR = new Color(0.43137255f, 0.25490198f, 0.20784314f);
        private static readonly Color DISABLE_COLOR = new Color(0.627451f, 0.627451f, 0.627451f);
        private static readonly Color DISABLE_TEXT_COLOR = new Color(0.19607843f, 0.19607843f, 0.19607843f);
        private static readonly Color TOGGLE_DECIDED_COLOR = new Color(1f, 0.60784316f, 0.7254902f);

        [SerializeField] private ePreset m_Preset;
        [SerializeField] private bool m_RenderDefault = true;
        [SerializeField] private bool m_RenderPress;
        [SerializeField] private Color m_PressColor = BASE_COLOR;
        [SerializeField] private bool m_RenderDisable = true;
        [SerializeField] private Color m_DisableColor = Color.gray;
        [SerializeField] private Color m_DecidedColor = BASE_COLOR;
        [SerializeField] private eDecideObjState m_RenderDecideState;
        [SerializeField] private Animation m_PressedAnim;

        private Graphic[] m_Graphics;
        private Color[] m_DefaultColors;
        private bool m_FixDefaultColor;
        private Color m_FixedDefaultColor = Color.white;
        private bool m_IsDecide;
        private eState m_CurrentState;
        private bool m_IsDonePrepare;
        private GameObject m_GameObject;

        public void Prepare() {
            m_Graphics = GetComponentsInChildren<Graphic>(true);
            m_DefaultColors = new Color[m_Graphics.Length];
            for (int i = 0; i < m_Graphics.Length; i++) {
                m_DefaultColors[i] = m_Graphics[i].color;
            }
            switch (m_Preset) {
                case ePreset.CommonButtonBase:
                    m_RenderDefault = true;
                    m_FixDefaultColor = true;
                    m_FixedDefaultColor = BASE_COLOR;
                    m_RenderPress = true;
                    m_PressColor = TEXT_COLOR;
                    m_RenderDisable = true;
                    m_DisableColor = DISABLE_COLOR;
                    m_DecidedColor = TEXT_COLOR;
                    break;
                case ePreset.CommonButtonText:
                    m_RenderDefault = true;
                    m_FixDefaultColor = true;
                    m_FixedDefaultColor = TEXT_COLOR;
                    m_RenderPress = true;
                    m_PressColor = TEXT_COLOR;
                    m_RenderDisable = true;
                    m_DisableColor = DISABLE_TEXT_COLOR;
                    m_DecidedColor = BASE_COLOR;
                    break;
                case ePreset.CommonButtonShadow:
                    m_RenderDefault = true;
                    m_FixDefaultColor = false;
                    m_RenderPress = false;
                    m_RenderDisable = false;
                    break;
                case ePreset.Icon:
                    m_RenderDefault = true;
                    m_FixDefaultColor = false;
                    m_RenderPress = true;
                    m_PressColor = Color.gray;
                    m_RenderDisable = true;
                    m_DisableColor = Color.gray;
                    m_DecidedColor = BASE_COLOR;
                    break;
                case ePreset.ToggleButtonBase:
                    m_RenderDefault = true;
                    m_FixDefaultColor = true;
                    m_FixedDefaultColor = BASE_COLOR;
                    m_RenderPress = true;
                    m_PressColor = TOGGLE_DECIDED_COLOR;
                    m_RenderDisable = true;
                    m_DisableColor = DISABLE_TEXT_COLOR;
                    m_DecidedColor = TOGGLE_DECIDED_COLOR;
                    break;
                case ePreset.ToggleButtonText:
                    m_RenderDefault = true;
                    m_FixDefaultColor = true;
                    m_FixedDefaultColor = TEXT_COLOR;
                    m_RenderPress = true;
                    m_PressColor = BASE_COLOR;
                    m_RenderDisable = true;
                    m_DisableColor = DISABLE_TEXT_COLOR;
                    m_DecidedColor = BASE_COLOR;
                    break;
                case ePreset.TabButtonText:
                    m_RenderDefault = true;
                    m_FixDefaultColor = true;
                    m_FixedDefaultColor = TEXT_COLOR;
                    m_RenderPress = true;
                    m_PressColor = TEXT_COLOR;
                    m_RenderDisable = true;
                    m_DisableColor = DISABLE_TEXT_COLOR;
                    m_DecidedColor = TEXT_COLOR;
                    break;
            }
            if (m_PressedAnim != null) {
                m_PressedAnim.gameObject.SetActive(false);
            }
            m_IsDonePrepare = true;
            m_GameObject = gameObject;
        }

        private void Start() {
            if (!m_IsDonePrepare) {
                Prepare();
            }
        }

        private void OnDisable() {
            if (m_PressedAnim != null) {
                m_PressedAnim.Stop();
                m_PressedAnim.gameObject.SetActive(false);
            }
        }

        public bool CheckRenderDecide() {
            if (m_RenderDecideState == eDecideObjState.NotDecideOnly) {
                return !m_IsDecide;
            }
            if (m_RenderDecideState == eDecideObjState.DecideOnly) {
                return m_IsDecide;
            }
            return m_RenderDecideState == eDecideObjState.Each;
        }

        public void SetDecide(bool flg) {
            m_IsDecide = flg;
            ChangeState(m_CurrentState);
        }

        public void ResetState() {
            ChangeState(m_CurrentState);
        }

        public void ChangeState(eState state) {
            if (!m_IsDonePrepare) {
                Prepare();
            }
            m_CurrentState = state;
            if (state == eState.Default) {
                for (int i = 0; i < m_Graphics.Length; i++) {
                    if (m_IsDecide) {
                        m_GameObject.SetActive(m_RenderPress && CheckRenderDecide());
                        if (m_RenderPress) {
                            m_Graphics[i].color = m_DecidedColor;
                        }
                    } else {
                        m_GameObject.SetActive(m_RenderDefault && CheckRenderDecide());
                        if (m_FixDefaultColor) {
                            m_Graphics[i].color = m_FixedDefaultColor;
                        } else {
                            m_Graphics[i].color = m_DefaultColors[i];
                        }
                    }
                }
            } else if (state == eState.Disable) {
                for (int i = 0; i < m_Graphics.Length; i++) {
                    m_GameObject.SetActive(m_RenderDisable && CheckRenderDecide());
                    if (m_RenderDisable) {
                        m_Graphics[i].color = m_DisableColor;
                    }
                }
            } else if (state == eState.Press) {
                for (int i = 0; i < m_Graphics.Length; i++) {
                    m_GameObject.SetActive(m_RenderPress && CheckRenderDecide());
                    if (m_RenderPress) {
                        m_Graphics[i].color = m_PressColor;
                    }
                }
                if (m_PressedAnim != null) {
                    m_PressedAnim.gameObject.SetActive(true);
                    m_PressedAnim.Play();
                }
            }
        }

        public enum ePreset {
            None,
            CommonButtonBase,
            CommonButtonText,
            CommonButtonShadow,
            Icon,
            ToggleButtonBase,
            ToggleButtonText,
            TabButtonText
        }

        public enum eDecideObjState {
            Each,
            DecideOnly,
            NotDecideOnly
        }

        public enum eState {
            Default,
            Press,
            Disable
        }
    }
}
