﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CD3 RID: 3283
	[Token(Token = "0x20008C3")]
	[StructLayout(3)]
	public class HaveImageTitle : MonoBehaviour
	{
		// Token: 0x06003C19 RID: 15385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036D6")]
		[Address(RVA = "0x1014CC524", Offset = "0x14CC524", VA = "0x1014CC524")]
		public void Setup()
		{
		}

		// Token: 0x06003C1A RID: 15386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036D7")]
		[Address(RVA = "0x1014CC528", Offset = "0x14CC528", VA = "0x1014CC528")]
		public void SetTitle(string title)
		{
		}

		// Token: 0x06003C1B RID: 15387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036D8")]
		[Address(RVA = "0x1014CC5E0", Offset = "0x14CC5E0", VA = "0x1014CC5E0")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06003C1C RID: 15388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036D9")]
		[Address(RVA = "0x1014CC61C", Offset = "0x14CC61C", VA = "0x1014CC61C")]
		public HaveImageTitle()
		{
		}

		// Token: 0x04004B5A RID: 19290
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40034F7")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x04004B5B RID: 19291
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40034F8")]
		[SerializeField]
		private Image m_Line;
	}
}
