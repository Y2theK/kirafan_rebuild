﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Home
{
	// Token: 0x02001172 RID: 4466
	[Token(Token = "0x2000B9A")]
	[StructLayout(3)]
	public class GachaBannerUIData
	{
		// Token: 0x06005839 RID: 22585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005181")]
		[Address(RVA = "0x1014CC624", Offset = "0x14CC624", VA = "0x1014CC624")]
		public GachaBannerUIData(GachaBannerUIData.eBannerType bannerType, int id, float offsetX, float offsetY, DateTime endAt, DateTime dispEndAt)
		{
		}

		// Token: 0x04006B60 RID: 27488
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004C18")]
		public GachaBannerUIData.eBannerType m_BannerType;

		// Token: 0x04006B61 RID: 27489
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4004C19")]
		public int m_ID;

		// Token: 0x04006B62 RID: 27490
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C1A")]
		public float m_OffsetX;

		// Token: 0x04006B63 RID: 27491
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004C1B")]
		public float m_OffsetY;

		// Token: 0x04006B64 RID: 27492
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C1C")]
		public DateTime m_EndAt;

		// Token: 0x04006B65 RID: 27493
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C1D")]
		public DateTime m_DispEndAt;

		// Token: 0x02001173 RID: 4467
		[Token(Token = "0x2001299")]
		public enum eBannerType
		{
			// Token: 0x04006B67 RID: 27495
			[Token(Token = "0x400727D")]
			PickUp,
			// Token: 0x04006B68 RID: 27496
			[Token(Token = "0x400727E")]
			Free
		}
	}
}
