﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Home
{
	// Token: 0x02001176 RID: 4470
	[Token(Token = "0x2000B9C")]
	[StructLayout(3)]
	public class HomeCharaIllust : MonoBehaviour, IBeginDragHandler, IEventSystemHandler, IDragHandler, IEndDragHandler
	{
		// Token: 0x06005845 RID: 22597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600518D")]
		[Address(RVA = "0x1014CC688", Offset = "0x14CC688", VA = "0x1014CC688")]
		public void SetEnablePlayVoice(bool flg)
		{
		}

		// Token: 0x06005846 RID: 22598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600518E")]
		[Address(RVA = "0x1014CC690", Offset = "0x14CC690", VA = "0x1014CC690")]
		public void StopVoice()
		{
		}

		// Token: 0x06005847 RID: 22599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600518F")]
		[Address(RVA = "0x1014CC734", Offset = "0x14CC734", VA = "0x1014CC734")]
		public void Setup()
		{
		}

		// Token: 0x06005848 RID: 22600 RVA: 0x0001D1D8 File Offset: 0x0001B3D8
		[Token(Token = "0x6005190")]
		[Address(RVA = "0x1014CCB8C", Offset = "0x14CCB8C", VA = "0x1014CCB8C")]
		private bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005849 RID: 22601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005191")]
		[Address(RVA = "0x1014CCBFC", Offset = "0x14CCBFC", VA = "0x1014CCBFC")]
		private void Update()
		{
		}

		// Token: 0x0600584A RID: 22602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005192")]
		[Address(RVA = "0x1014CD05C", Offset = "0x14CD05C", VA = "0x1014CD05C")]
		public void Destroy()
		{
		}

		// Token: 0x0600584B RID: 22603 RVA: 0x0001D1F0 File Offset: 0x0001B3F0
		[Token(Token = "0x6005193")]
		[Address(RVA = "0x1014CD150", Offset = "0x14CD150", VA = "0x1014CD150")]
		public bool IsDisplay()
		{
			return default(bool);
		}

		// Token: 0x0600584C RID: 22604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005194")]
		[Address(RVA = "0x1014CD1CC", Offset = "0x14CD1CC", VA = "0x1014CD1CC", Slot = "4")]
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x0600584D RID: 22605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005195")]
		[Address(RVA = "0x1014CD1D8", Offset = "0x14CD1D8", VA = "0x1014CD1D8", Slot = "5")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x0600584E RID: 22606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005196")]
		[Address(RVA = "0x1014CD2B8", Offset = "0x14CD2B8", VA = "0x1014CD2B8", Slot = "6")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x0600584F RID: 22607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005197")]
		[Address(RVA = "0x1014CD2CC", Offset = "0x14CD2CC", VA = "0x1014CD2CC")]
		public void DisplayFirstChara()
		{
		}

		// Token: 0x06005850 RID: 22608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005198")]
		[Address(RVA = "0x1014CD400", Offset = "0x14CD400", VA = "0x1014CD400")]
		public void HideTalkWindow()
		{
		}

		// Token: 0x06005851 RID: 22609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005199")]
		[Address(RVA = "0x1014CC8D8", Offset = "0x14CC8D8", VA = "0x1014CC8D8")]
		public void NextChara()
		{
		}

		// Token: 0x06005852 RID: 22610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519A")]
		[Address(RVA = "0x1014CD430", Offset = "0x14CD430", VA = "0x1014CD430")]
		private void SetChara(int charaID)
		{
		}

		// Token: 0x06005853 RID: 22611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519B")]
		[Address(RVA = "0x1014CD458", Offset = "0x14CD458", VA = "0x1014CD458")]
		private void DisappearChara()
		{
		}

		// Token: 0x06005854 RID: 22612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519C")]
		[Address(RVA = "0x1014CD4CC", Offset = "0x14CD4CC", VA = "0x1014CD4CC")]
		private void AppearChara()
		{
		}

		// Token: 0x06005855 RID: 22613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519D")]
		[Address(RVA = "0x1014CD580", Offset = "0x14CD580", VA = "0x1014CD580")]
		public HomeCharaIllust()
		{
		}

		// Token: 0x04006B7F RID: 27519
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C30")]
		[SerializeField]
		private CharaIllust[] m_CharaIllusts;

		// Token: 0x04006B80 RID: 27520
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C31")]
		[SerializeField]
		private AnimUIPlayer[] m_IllustAnims;

		// Token: 0x04006B81 RID: 27521
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C32")]
		private RectTransform[] m_IllustRectTransforms;

		// Token: 0x04006B82 RID: 27522
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004C33")]
		[SerializeField]
		private AnimUIPlayer m_TalkWindow;

		// Token: 0x04006B83 RID: 27523
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004C34")]
		[SerializeField]
		private Text m_TalkTextObj;

		// Token: 0x04006B84 RID: 27524
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004C35")]
		private int m_CurrentIdx;

		// Token: 0x04006B85 RID: 27525
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004C36")]
		private float m_Threshold;

		// Token: 0x04006B86 RID: 27526
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004C37")]
		private bool m_IsHold;

		// Token: 0x04006B87 RID: 27527
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004C38")]
		private int m_RoomInCharaIdx;

		// Token: 0x04006B88 RID: 27528
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004C39")]
		private int m_CharaID;

		// Token: 0x04006B89 RID: 27529
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C3A")]
		private TweetListDB m_TweetListDB;

		// Token: 0x04006B8A RID: 27530
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C3B")]
		private int m_VoiceReqID;

		// Token: 0x04006B8B RID: 27531
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004C3C")]
		private bool m_IsEnablePlayVoice;
	}
}
