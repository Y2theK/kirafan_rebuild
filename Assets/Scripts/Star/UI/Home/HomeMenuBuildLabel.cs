﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x02001177 RID: 4471
	[Token(Token = "0x2000B9D")]
	[StructLayout(3)]
	public class HomeMenuBuildLabel : MonoBehaviour
	{
		// Token: 0x06005856 RID: 22614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519E")]
		[Address(RVA = "0x1014CD5A4", Offset = "0x14CD5A4", VA = "0x1014CD5A4")]
		private void Update()
		{
		}

		// Token: 0x06005857 RID: 22615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600519F")]
		[Address(RVA = "0x1014CD5A8", Offset = "0x14CD5A8", VA = "0x1014CD5A8")]
		private void UpdateInternal()
		{
		}

		// Token: 0x06005858 RID: 22616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A0")]
		[Address(RVA = "0x1014CD6D0", Offset = "0x14CD6D0", VA = "0x1014CD6D0")]
		public HomeMenuBuildLabel()
		{
		}

		// Token: 0x04006B8C RID: 27532
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C3D")]
		[SerializeField]
		private Vector3 m_Position;

		// Token: 0x04006B8D RID: 27533
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C3E")]
		[SerializeField]
		private RectTransform m_TargetRectTransform;
	}
}
