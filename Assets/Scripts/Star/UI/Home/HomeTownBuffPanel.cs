﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x02001178 RID: 4472
	[Token(Token = "0x2000B9E")]
	[StructLayout(3)]
	public class HomeTownBuffPanel : MonoBehaviour
	{
		// Token: 0x06005859 RID: 22617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A1")]
		[Address(RVA = "0x1014CD6D8", Offset = "0x14CD6D8", VA = "0x1014CD6D8")]
		public void Destroy()
		{
		}

		// Token: 0x0600585A RID: 22618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A2")]
		[Address(RVA = "0x1014CD70C", Offset = "0x14CD70C", VA = "0x1014CD70C")]
		public void Apply(int charaID, eElementType elementType)
		{
		}

		// Token: 0x0600585B RID: 22619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A3")]
		[Address(RVA = "0x1014CD7AC", Offset = "0x14CD7AC", VA = "0x1014CD7AC")]
		public void Apply(int charaID, eClassType classType)
		{
		}

		// Token: 0x0600585C RID: 22620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A4")]
		[Address(RVA = "0x1014CD84C", Offset = "0x14CD84C", VA = "0x1014CD84C")]
		public HomeTownBuffPanel()
		{
		}

		// Token: 0x04006B8E RID: 27534
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C3F")]
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x04006B8F RID: 27535
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C40")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04006B90 RID: 27536
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C41")]
		[SerializeField]
		private ClassIcon m_ClassIcon;
	}
}
