﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x02001179 RID: 4473
	[Token(Token = "0x2000B9F")]
	[StructLayout(3)]
	public class HomeTownBuffWindow : MonoBehaviour
	{
		// Token: 0x0600585D RID: 22621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A5")]
		[Address(RVA = "0x1014CD854", Offset = "0x14CD854", VA = "0x1014CD854")]
		public void Setup()
		{
		}

		// Token: 0x0600585E RID: 22622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A6")]
		[Address(RVA = "0x1014CD908", Offset = "0x14CD908", VA = "0x1014CD908")]
		public void Destroy()
		{
		}

		// Token: 0x0600585F RID: 22623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A7")]
		[Address(RVA = "0x1014CDA64", Offset = "0x14CDA64", VA = "0x1014CDA64")]
		public void Apply()
		{
		}

		// Token: 0x06005860 RID: 22624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A8")]
		[Address(RVA = "0x1014CDD60", Offset = "0x14CDD60", VA = "0x1014CDD60")]
		public HomeTownBuffWindow()
		{
		}

		// Token: 0x04006B91 RID: 27537
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C42")]
		[SerializeField]
		private HomeTownBuffPanel[] m_Panels;
	}
}
