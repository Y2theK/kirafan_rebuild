﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x0200117A RID: 4474
	[Token(Token = "0x2000BA0")]
	[StructLayout(3)]
	public class HomeTransitButton : MonoBehaviour
	{
		// Token: 0x06005861 RID: 22625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051A9")]
		[Address(RVA = "0x1014CDD68", Offset = "0x14CDD68", VA = "0x1014CDD68")]
		public void SetBadgeValue(int value)
		{
		}

		// Token: 0x06005862 RID: 22626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051AA")]
		[Address(RVA = "0x1014CDDF0", Offset = "0x14CDDF0", VA = "0x1014CDDF0")]
		public HomeTransitButton()
		{
		}

		// Token: 0x04006B92 RID: 27538
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C43")]
		[SerializeField]
		private PrefabCloner m_BadgeCloner;
	}
}
