﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.LoginBonus;
using Star.UI.Mission;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x0200117B RID: 4475
	[Token(Token = "0x2000BA1")]
	[StructLayout(3)]
	public class HomeUI : MenuUIBase
	{
		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x06005863 RID: 22627 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000585")]
		public AnimUIPlayer userInfoBtnAnim
		{
			[Token(Token = "0x60051AB")]
			[Address(RVA = "0x1014CDDF8", Offset = "0x14CDDF8", VA = "0x1014CDDF8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x06005864 RID: 22628 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000586")]
		public AnimUIPlayer beginnersMissionBtnAnim
		{
			[Token(Token = "0x60051AC")]
			[Address(RVA = "0x1014CDE00", Offset = "0x14CDE00", VA = "0x1014CDE00")]
			get
			{
				return null;
			}
		}

		// Token: 0x14000143 RID: 323
		// (add) Token: 0x06005865 RID: 22629 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005866 RID: 22630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000143")]
		private event Action OnEndTotalLoginBonusData
		{
			[Token(Token = "0x60051AD")]
			[Address(RVA = "0x1014CDE08", Offset = "0x14CDE08", VA = "0x1014CDE08")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051AE")]
			[Address(RVA = "0x1014CDF14", Offset = "0x14CDF14", VA = "0x1014CDF14")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000144 RID: 324
		// (add) Token: 0x06005867 RID: 22631 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005868 RID: 22632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000144")]
		private event Action<int> m_OnEndScrollTextGroup
		{
			[Token(Token = "0x60051AF")]
			[Address(RVA = "0x1014CE020", Offset = "0x14CE020", VA = "0x1014CE020")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051B0")]
			[Address(RVA = "0x1014CE12C", Offset = "0x14CE12C", VA = "0x1014CE12C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000145 RID: 325
		// (add) Token: 0x06005869 RID: 22633 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600586A RID: 22634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000145")]
		public event Action OnClickPresentButton
		{
			[Token(Token = "0x60051B1")]
			[Address(RVA = "0x1014CE238", Offset = "0x14CE238", VA = "0x1014CE238")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051B2")]
			[Address(RVA = "0x1014CE344", Offset = "0x14CE344", VA = "0x1014CE344")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000146 RID: 326
		// (add) Token: 0x0600586B RID: 22635 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600586C RID: 22636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000146")]
		public event Action OnClickMissionButton
		{
			[Token(Token = "0x60051B3")]
			[Address(RVA = "0x1014CE450", Offset = "0x14CE450", VA = "0x1014CE450")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051B4")]
			[Address(RVA = "0x1014CE55C", Offset = "0x14CE55C", VA = "0x1014CE55C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000147 RID: 327
		// (add) Token: 0x0600586D RID: 22637 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600586E RID: 22638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000147")]
		public event Action OnClickUserInfoButton
		{
			[Token(Token = "0x60051B5")]
			[Address(RVA = "0x1014CE668", Offset = "0x14CE668", VA = "0x1014CE668")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051B6")]
			[Address(RVA = "0x1014CE774", Offset = "0x14CE774", VA = "0x1014CE774")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000148 RID: 328
		// (add) Token: 0x0600586F RID: 22639 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005870 RID: 22640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000148")]
		public event Action OnClickBeginnersMissionButton
		{
			[Token(Token = "0x60051B7")]
			[Address(RVA = "0x1014CE880", Offset = "0x14CE880", VA = "0x1014CE880")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051B8")]
			[Address(RVA = "0x1014CE990", Offset = "0x14CE990", VA = "0x1014CE990")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000149 RID: 329
		// (add) Token: 0x06005871 RID: 22641 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005872 RID: 22642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000149")]
		public event Action OnClickQuestButton
		{
			[Token(Token = "0x60051B9")]
			[Address(RVA = "0x1014CEAA0", Offset = "0x14CEAA0", VA = "0x1014CEAA0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051BA")]
			[Address(RVA = "0x1014CEBB0", Offset = "0x14CEBB0", VA = "0x1014CEBB0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400014A RID: 330
		// (add) Token: 0x06005873 RID: 22643 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005874 RID: 22644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400014A")]
		public event Action OnClickGachaButton
		{
			[Token(Token = "0x60051BB")]
			[Address(RVA = "0x1014CECC0", Offset = "0x14CECC0", VA = "0x1014CECC0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051BC")]
			[Address(RVA = "0x1014CEDD0", Offset = "0x14CEDD0", VA = "0x1014CEDD0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400014B RID: 331
		// (add) Token: 0x06005875 RID: 22645 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06005876 RID: 22646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400014B")]
		public event Action OnEnd
		{
			[Token(Token = "0x60051BD")]
			[Address(RVA = "0x1014CEEE0", Offset = "0x14CEEE0", VA = "0x1014CEEE0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60051BE")]
			[Address(RVA = "0x1014CEFF0", Offset = "0x14CEFF0", VA = "0x1014CEFF0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06005877 RID: 22647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051BF")]
		[Address(RVA = "0x1014CF100", Offset = "0x14CF100", VA = "0x1014CF100")]
		private void OnDestroy()
		{
		}

		// Token: 0x06005878 RID: 22648 RVA: 0x0001D208 File Offset: 0x0001B408
		[Token(Token = "0x60051C0")]
		[Address(RVA = "0x1014CF118", Offset = "0x14CF118", VA = "0x1014CF118")]
		public bool CheckRaycast()
		{
			return default(bool);
		}

		// Token: 0x06005879 RID: 22649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051C1")]
		[Address(RVA = "0x1014CF148", Offset = "0x14CF148", VA = "0x1014CF148")]
		public void Setup(TownMain owner)
		{
		}

		// Token: 0x0600587A RID: 22650 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60051C2")]
		[Address(RVA = "0x1014CF8E0", Offset = "0x14CF8E0", VA = "0x1014CF8E0")]
		public InfoBanner GetInfoBanner()
		{
			return null;
		}

		// Token: 0x0600587B RID: 22651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051C3")]
		[Address(RVA = "0x1014CF8E8", Offset = "0x14CF8E8", VA = "0x1014CF8E8")]
		public void OpenTotalLogin(TotalLoginUIData totalLoginBonusData, Action onEndTotalLoginBonusData)
		{
		}

		// Token: 0x0600587C RID: 22652 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60051C4")]
		[Address(RVA = "0x1014D01FC", Offset = "0x14D01FC", VA = "0x1014D01FC")]
		public ScrollTextUIGroup GetScrollTextUIGroup()
		{
			return null;
		}

		// Token: 0x0600587D RID: 22653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051C5")]
		[Address(RVA = "0x1014D0204", Offset = "0x14D0204", VA = "0x1014D0204")]
		public void OpenScrollTextGroup(Action<int> onEnd)
		{
		}

		// Token: 0x0600587E RID: 22654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051C6")]
		[Address(RVA = "0x1014D023C", Offset = "0x14D023C", VA = "0x1014D023C")]
		public void SetUpBeggnersMission()
		{
		}

		// Token: 0x0600587F RID: 22655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051C7")]
		[Address(RVA = "0x1014D05BC", Offset = "0x14D05BC", VA = "0x1014D05BC")]
		public void OpenBeggnersMission()
		{
		}

		// Token: 0x06005880 RID: 22656 RVA: 0x0001D220 File Offset: 0x0001B420
		[Token(Token = "0x60051C8")]
		[Address(RVA = "0x1014D065C", Offset = "0x14D065C", VA = "0x1014D065C")]
		public bool IsBeginnersMissionUIClosed()
		{
			return default(bool);
		}

		// Token: 0x06005881 RID: 22657 RVA: 0x0001D238 File Offset: 0x0001B438
		[Token(Token = "0x60051C9")]
		[Address(RVA = "0x1014D0700", Offset = "0x14D0700", VA = "0x1014D0700")]
		public bool IsReadyBeginnersMissonUI()
		{
			return default(bool);
		}

		// Token: 0x06005882 RID: 22658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CA")]
		[Address(RVA = "0x1014D07C0", Offset = "0x14D07C0", VA = "0x1014D07C0")]
		public void ReleaseBeginnersMission()
		{
		}

		// Token: 0x06005883 RID: 22659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CB")]
		[Address(RVA = "0x1014D0888", Offset = "0x14D0888", VA = "0x1014D0888")]
		private void Update()
		{
		}

		// Token: 0x06005884 RID: 22660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CC")]
		[Address(RVA = "0x1014D1178", Offset = "0x14D1178", VA = "0x1014D1178", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06005885 RID: 22661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CD")]
		[Address(RVA = "0x1014D12CC", Offset = "0x14D12CC", VA = "0x1014D12CC")]
		private void OnEndLoginGroup()
		{
		}

		// Token: 0x06005886 RID: 22662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CE")]
		[Address(RVA = "0x1014D12D8", Offset = "0x14D12D8", VA = "0x1014D12D8")]
		private void OnEndScrollGroup()
		{
		}

		// Token: 0x06005887 RID: 22663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051CF")]
		[Address(RVA = "0x1014D134C", Offset = "0x14D134C", VA = "0x1014D134C")]
		public void SetEnablePlayVoice(bool flg)
		{
		}

		// Token: 0x06005888 RID: 22664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D0")]
		[Address(RVA = "0x1014D137C", Offset = "0x14D137C", VA = "0x1014D137C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06005889 RID: 22665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D1")]
		[Address(RVA = "0x1014D1448", Offset = "0x14D1448", VA = "0x1014D1448", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600588A RID: 22666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D2")]
		[Address(RVA = "0x1014D0CEC", Offset = "0x14D0CEC", VA = "0x1014D0CEC")]
		public void SetPresentBadgeValue(int value)
		{
		}

		// Token: 0x0600588B RID: 22667 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D3")]
		[Address(RVA = "0x1014D0CB8", Offset = "0x14D0CB8", VA = "0x1014D0CB8")]
		public void SetMissionBadgeValue(int value)
		{
		}

		// Token: 0x0600588C RID: 22668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D4")]
		[Address(RVA = "0x1014D0D20", Offset = "0x14D0D20", VA = "0x1014D0D20")]
		public void SetBeginnersMissionBadgevalue()
		{
		}

		// Token: 0x0600588D RID: 22669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D5")]
		[Address(RVA = "0x1014D0DC4", Offset = "0x14D0DC4", VA = "0x1014D0DC4")]
		public void SetUserInfoBadgeValue()
		{
		}

		// Token: 0x0600588E RID: 22670 RVA: 0x0001D250 File Offset: 0x0001B450
		[Token(Token = "0x60051D6")]
		[Address(RVA = "0x1014D14E4", Offset = "0x14D14E4", VA = "0x1014D14E4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600588F RID: 22671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D7")]
		[Address(RVA = "0x1014D1588", Offset = "0x14D1588", VA = "0x1014D1588")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x06005890 RID: 22672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D8")]
		[Address(RVA = "0x1014D1264", Offset = "0x14D1264", VA = "0x1014D1264")]
		private void OpenMainGroup()
		{
		}

		// Token: 0x06005891 RID: 22673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051D9")]
		[Address(RVA = "0x1014D158C", Offset = "0x14D158C", VA = "0x1014D158C")]
		public void CloseMainGroup()
		{
		}

		// Token: 0x06005892 RID: 22674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DA")]
		[Address(RVA = "0x1014D15C0", Offset = "0x14D15C0", VA = "0x1014D15C0")]
		public void OnClickPresentButtonCallBack()
		{
		}

		// Token: 0x06005893 RID: 22675 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DB")]
		[Address(RVA = "0x1014D15CC", Offset = "0x14D15CC", VA = "0x1014D15CC")]
		public void OnClickMissionButtonCallBack()
		{
		}

		// Token: 0x06005894 RID: 22676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DC")]
		[Address(RVA = "0x1014D15D8", Offset = "0x14D15D8", VA = "0x1014D15D8")]
		public void OnClickUserInfoButtonCallBack()
		{
		}

		// Token: 0x06005895 RID: 22677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DD")]
		[Address(RVA = "0x1014D15E4", Offset = "0x14D15E4", VA = "0x1014D15E4")]
		public void OnClickBeginnersMissionButtonCallBack()
		{
		}

		// Token: 0x06005896 RID: 22678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DE")]
		[Address(RVA = "0x1014D15F0", Offset = "0x14D15F0", VA = "0x1014D15F0")]
		public void OnClickQuestButtonCallBack()
		{
		}

		// Token: 0x06005897 RID: 22679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051DF")]
		[Address(RVA = "0x1014D15FC", Offset = "0x14D15FC", VA = "0x1014D15FC")]
		public void OnClickGachaButtonCallBack()
		{
		}

		// Token: 0x06005898 RID: 22680 RVA: 0x0001D268 File Offset: 0x0001B468
		[Token(Token = "0x60051E0")]
		[Address(RVA = "0x1014D1608", Offset = "0x14D1608", VA = "0x1014D1608")]
		public int GetEventID()
		{
			return 0;
		}

		// Token: 0x06005899 RID: 22681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051E1")]
		[Address(RVA = "0x1014CF850", Offset = "0x14CF850", VA = "0x1014CF850")]
		public void BeginnersMissionButtonActive(bool active)
		{
		}

		// Token: 0x0600589A RID: 22682 RVA: 0x0001D280 File Offset: 0x0001B480
		[Token(Token = "0x60051E2")]
		[Address(RVA = "0x1014D163C", Offset = "0x14D163C", VA = "0x1014D163C")]
		public bool CheckBtnAnimAllHide()
		{
			return default(bool);
		}

		// Token: 0x0600589B RID: 22683 RVA: 0x0001D298 File Offset: 0x0001B498
		[Token(Token = "0x60051E3")]
		[Address(RVA = "0x1014D16A4", Offset = "0x14D16A4", VA = "0x1014D16A4")]
		public bool CheckEndBtnAnim()
		{
			return default(bool);
		}

		// Token: 0x0600589C RID: 22684 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60051E4")]
		[Address(RVA = "0x1014D170C", Offset = "0x14D170C", VA = "0x1014D170C")]
		public CustomButton GetBeginnersMissionButton()
		{
			return null;
		}

		// Token: 0x0600589D RID: 22685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60051E5")]
		[Address(RVA = "0x1014D176C", Offset = "0x14D176C", VA = "0x1014D176C")]
		public HomeUI()
		{
		}

		// Token: 0x04006B93 RID: 27539
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004C44")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04006B94 RID: 27540
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004C45")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04006B95 RID: 27541
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004C46")]
		[SerializeField]
		private TotalLoginGroup m_LoginGroup;

		// Token: 0x04006B96 RID: 27542
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C47")]
		[SerializeField]
		private ScrollTextUIGroup m_ScrollTextUIGroup;

		// Token: 0x04006B97 RID: 27543
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C48")]
		[SerializeField]
		private HomeCharaIllust m_HomeCharaIllust;

		// Token: 0x04006B98 RID: 27544
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004C49")]
		[SerializeField]
		private InfoBanner m_InfoBanner;

		// Token: 0x04006B99 RID: 27545
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004C4A")]
		[SerializeField]
		private HomeTransitButton m_PresentButton;

		// Token: 0x04006B9A RID: 27546
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004C4B")]
		[SerializeField]
		private HomeTransitButton m_MissionButton;

		// Token: 0x04006B9B RID: 27547
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004C4C")]
		[SerializeField]
		private HomeTransitButton m_UserInfoButton;

		// Token: 0x04006B9C RID: 27548
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C4D")]
		[SerializeField]
		private HomeTransitButton m_BeginnersMissionButton;

		// Token: 0x04006B9D RID: 27549
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004C4E")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04006B9E RID: 27550
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004C4F")]
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x04006B9F RID: 27551
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004C50")]
		[SerializeField]
		private TownEventBanner m_TownEventQuestBanner;

		// Token: 0x04006BA0 RID: 27552
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004C51")]
		[SerializeField]
		private TownEventBanner m_TownGachaEventBanner;

		// Token: 0x04006BA1 RID: 27553
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004C52")]
		[SerializeField]
		private Transform m_tfBeginnersMissionParent;

		// Token: 0x04006BA2 RID: 27554
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004C53")]
		[SerializeField]
		private AnimUIPlayer m_userInfoBtnAnim;

		// Token: 0x04006BA3 RID: 27555
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004C54")]
		[SerializeField]
		private AnimUIPlayer m_beginnersMissionBtnAnim;

		// Token: 0x04006BA4 RID: 27556
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004C55")]
		private PanelMissionUI m_BeginnersMission;

		// Token: 0x04006BA5 RID: 27557
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004C56")]
		private TownMain m_Owner;

		// Token: 0x0200117C RID: 4476
		[Token(Token = "0x200129B")]
		public enum eUIState
		{
			// Token: 0x04006BB0 RID: 27568
			[Token(Token = "0x4007284")]
			Hide,
			// Token: 0x04006BB1 RID: 27569
			[Token(Token = "0x4007285")]
			In,
			// Token: 0x04006BB2 RID: 27570
			[Token(Token = "0x4007286")]
			Out,
			// Token: 0x04006BB3 RID: 27571
			[Token(Token = "0x4007287")]
			End,
			// Token: 0x04006BB4 RID: 27572
			[Token(Token = "0x4007288")]
			Main,
			// Token: 0x04006BB5 RID: 27573
			[Token(Token = "0x4007289")]
			Window,
			// Token: 0x04006BB6 RID: 27574
			[Token(Token = "0x400728A")]
			Num
		}
	}
}
