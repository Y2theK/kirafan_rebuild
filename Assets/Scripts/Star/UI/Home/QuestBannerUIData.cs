﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Home
{
	// Token: 0x02001170 RID: 4464
	[Token(Token = "0x2000B99")]
	[StructLayout(3)]
	public class QuestBannerUIData
	{
		// Token: 0x06005838 RID: 22584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005180")]
		[Address(RVA = "0x1014D1774", Offset = "0x14D1774", VA = "0x1014D1774")]
		public QuestBannerUIData(QuestBannerUIData.eBannerType bannerType, int id, string imgID, float offsetX, float offsetY)
		{
		}

		// Token: 0x04006B58 RID: 27480
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004C13")]
		public QuestBannerUIData.eBannerType m_BannerType;

		// Token: 0x04006B59 RID: 27481
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4004C14")]
		public int m_ID;

		// Token: 0x04006B5A RID: 27482
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C15")]
		public string m_ImgID;

		// Token: 0x04006B5B RID: 27483
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C16")]
		public float m_OffsetX;

		// Token: 0x04006B5C RID: 27484
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004C17")]
		public float m_OffsetY;

		// Token: 0x02001171 RID: 4465
		[Token(Token = "0x2001298")]
		public enum eBannerType
		{
			// Token: 0x04006B5E RID: 27486
			[Token(Token = "0x400727A")]
			Event,
			// Token: 0x04006B5F RID: 27487
			[Token(Token = "0x400727B")]
			StaminaReduction
		}
	}
}
