﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Home
{
	// Token: 0x02001174 RID: 4468
	[Token(Token = "0x2000B9B")]
	[StructLayout(3)]
	public class TownEventBanner : MonoBehaviour
	{
		// Token: 0x0600583A RID: 22586 RVA: 0x0001D190 File Offset: 0x0001B390
		[Token(Token = "0x6005182")]
		[Address(RVA = "0x1014D17C8", Offset = "0x14D17C8", VA = "0x1014D17C8")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x0600583B RID: 22587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005183")]
		[Address(RVA = "0x1014D17F8", Offset = "0x14D17F8", VA = "0x1014D17F8")]
		private void SetupQuestBanner(bool isExistStaminaReduction)
		{
		}

		// Token: 0x0600583C RID: 22588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005184")]
		[Address(RVA = "0x1014D1AAC", Offset = "0x14D1AAC", VA = "0x1014D1AAC")]
		private void SetUpPickUpCharaIcon(bool isExistFreeGacha)
		{
		}

		// Token: 0x0600583D RID: 22589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005185")]
		[Address(RVA = "0x1014CF480", Offset = "0x14CF480", VA = "0x1014CF480")]
		public void SetUp()
		{
		}

		// Token: 0x0600583E RID: 22590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005186")]
		[Address(RVA = "0x1014D0E68", Offset = "0x14D0E68", VA = "0x1014D0E68")]
		public void SetDonePrepare(bool donePrepare)
		{
		}

		// Token: 0x0600583F RID: 22591 RVA: 0x0001D1A8 File Offset: 0x0001B3A8
		[Token(Token = "0x6005187")]
		[Address(RVA = "0x1014D0E60", Offset = "0x14D0E60", VA = "0x1014D0E60")]
		public bool GetDonePrepare()
		{
			return default(bool);
		}

		// Token: 0x06005840 RID: 22592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005188")]
		[Address(RVA = "0x1014D0E70", Offset = "0x14D0E70", VA = "0x1014D0E70")]
		public void Update()
		{
		}

		// Token: 0x06005841 RID: 22593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6005189")]
		[Address(RVA = "0x1014D21D8", Offset = "0x14D21D8", VA = "0x1014D21D8")]
		public void EventBannerImageChange()
		{
		}

		// Token: 0x06005842 RID: 22594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600518A")]
		[Address(RVA = "0x1014D1E7C", Offset = "0x14D1E7C", VA = "0x1014D1E7C")]
		private void PickUpCharaImageChange()
		{
		}

		// Token: 0x06005843 RID: 22595 RVA: 0x0001D1C0 File Offset: 0x0001B3C0
		[Token(Token = "0x600518B")]
		[Address(RVA = "0x1014D1634", Offset = "0x14D1634", VA = "0x1014D1634")]
		public int GetEventID()
		{
			return 0;
		}

		// Token: 0x06005844 RID: 22596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600518C")]
		[Address(RVA = "0x1014D250C", Offset = "0x14D250C", VA = "0x1014D250C")]
		public TownEventBanner()
		{
		}

		// Token: 0x04006B69 RID: 27497
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004C1E")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04006B6A RID: 27498
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004C1F")]
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04006B6B RID: 27499
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004C20")]
		[SerializeField]
		private Text m_EndTimeAtText;

		// Token: 0x04006B6C RID: 27500
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004C21")]
		private List<EventBannerLoader.EventBannerData> m_EventBannerData;

		// Token: 0x04006B6D RID: 27501
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004C22")]
		private int m_Index;

		// Token: 0x04006B6E RID: 27502
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4004C23")]
		private bool m_IsDonePrepare;

		// Token: 0x04006B6F RID: 27503
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004C24")]
		private TownEventBanner.eMode m_Mode;

		// Token: 0x04006B70 RID: 27504
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004C25")]
		private bool m_SetUpShake;

		// Token: 0x04006B71 RID: 27505
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004C26")]
		private float m_Span;

		// Token: 0x04006B72 RID: 27506
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4004C27")]
		private int m_EventID;

		// Token: 0x04006B73 RID: 27507
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004C28")]
		[SerializeField]
		private string m_CategoryString;

		// Token: 0x04006B74 RID: 27508
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004C29")]
		private List<QuestBannerUIData> m_QuestBannerUIDataList;

		// Token: 0x04006B75 RID: 27509
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004C2A")]
		private List<GachaBannerUIData> m_GachaBannerUIData;

		// Token: 0x04006B76 RID: 27510
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004C2B")]
		[SerializeField]
		private Sprite m_StaminaReductionBannerSprite;

		// Token: 0x04006B77 RID: 27511
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004C2C")]
		[SerializeField]
		private GameObject m_PickUpCharaObj;

		// Token: 0x04006B78 RID: 27512
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004C2D")]
		[SerializeField]
		private GameObject m_FreeGachaObj;

		// Token: 0x04006B79 RID: 27513
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004C2E")]
		private SpriteHandler m_Handle;

		// Token: 0x04006B7A RID: 27514
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004C2F")]
		private bool m_HandleUpdateFlag;

		// Token: 0x02001175 RID: 4469
		[Token(Token = "0x200129A")]
		private enum eMode
		{
			// Token: 0x04006B7C RID: 27516
			[Token(Token = "0x4007280")]
			None,
			// Token: 0x04006B7D RID: 27517
			[Token(Token = "0x4007281")]
			Shake,
			// Token: 0x04006B7E RID: 27518
			[Token(Token = "0x4007282")]
			ScaleUpDown
		}
	}
}
