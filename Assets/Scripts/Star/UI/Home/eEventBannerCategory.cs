﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Home
{
	// Token: 0x0200116F RID: 4463
	[Token(Token = "0x2000B98")]
	[StructLayout(3, Size = 4)]
	public enum eEventBannerCategory
	{
		// Token: 0x04006B55 RID: 27477
		[Token(Token = "0x4004C10")]
		Quest = 1,
		// Token: 0x04006B56 RID: 27478
		[Token(Token = "0x4004C11")]
		Gacha,
		// Token: 0x04006B57 RID: 27479
		[Token(Token = "0x4004C12")]
		Num
	}
}
