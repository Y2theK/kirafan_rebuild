﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C9F RID: 3231
	[Token(Token = "0x20008A7")]
	[StructLayout(3)]
	public class HorizontalScrollView : MonoBehaviour, IDragHandler, IEventSystemHandler, IEndDragHandler
	{
		// Token: 0x06003AFD RID: 15101 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C0")]
		[Address(RVA = "0x1014D25D4", Offset = "0x14D25D4", VA = "0x1014D25D4")]
		public void Setup()
		{
		}

		// Token: 0x06003AFE RID: 15102 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C1")]
		[Address(RVA = "0x1014D2720", Offset = "0x14D2720", VA = "0x1014D2720")]
		public void Destroy()
		{
		}

		// Token: 0x06003AFF RID: 15103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C2")]
		[Address(RVA = "0x1014D28F4", Offset = "0x14D28F4", VA = "0x1014D28F4")]
		public void AddItem(ScrollItemData data)
		{
		}

		// Token: 0x06003B00 RID: 15104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C3")]
		[Address(RVA = "0x1014D2970", Offset = "0x14D2970", VA = "0x1014D2970")]
		public void RemoveAll()
		{
		}

		// Token: 0x06003B01 RID: 15105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C4")]
		[Address(RVA = "0x1014D29DC", Offset = "0x14D29DC", VA = "0x1014D29DC")]
		public void ForceApply()
		{
		}

		// Token: 0x06003B02 RID: 15106 RVA: 0x00018438 File Offset: 0x00016638
		[Token(Token = "0x60035C5")]
		[Address(RVA = "0x1014D2AA8", Offset = "0x14D2AA8", VA = "0x1014D2AA8")]
		public float GetNormalizedPosition()
		{
			return 0f;
		}

		// Token: 0x06003B03 RID: 15107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C6")]
		[Address(RVA = "0x1014D2D40", Offset = "0x14D2D40", VA = "0x1014D2D40")]
		public void SetNormalizedPosition(float normalizedPosition)
		{
		}

		// Token: 0x06003B04 RID: 15108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C7")]
		[Address(RVA = "0x1014D2D94", Offset = "0x14D2D94", VA = "0x1014D2D94")]
		private void Update()
		{
		}

		// Token: 0x06003B05 RID: 15109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C8")]
		[Address(RVA = "0x1014D3394", Offset = "0x14D3394", VA = "0x1014D3394")]
		private void UpdateItemWidth()
		{
		}

		// Token: 0x06003B06 RID: 15110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035C9")]
		[Address(RVA = "0x1014D2AEC", Offset = "0x14D2AEC", VA = "0x1014D2AEC")]
		private void UpdateContentRect()
		{
		}

		// Token: 0x06003B07 RID: 15111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035CA")]
		[Address(RVA = "0x1014D2DC4", Offset = "0x14D2DC4", VA = "0x1014D2DC4")]
		private void UpdateItemPosition()
		{
		}

		// Token: 0x06003B08 RID: 15112 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60035CB")]
		[Address(RVA = "0x1014D34AC", Offset = "0x14D34AC", VA = "0x1014D34AC")]
		private ScrollItemIcon GetorCreateInactiveInstance()
		{
			return null;
		}

		// Token: 0x06003B09 RID: 15113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035CC")]
		[Address(RVA = "0x1014D37C8", Offset = "0x14D37C8", VA = "0x1014D37C8", Slot = "4")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003B0A RID: 15114 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035CD")]
		[Address(RVA = "0x1014D3A34", Offset = "0x14D3A34", VA = "0x1014D3A34", Slot = "5")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003B0B RID: 15115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035CE")]
		[Address(RVA = "0x1014D3AF4", Offset = "0x14D3AF4", VA = "0x1014D3AF4")]
		public HorizontalScrollView()
		{
		}

		// Token: 0x040049B0 RID: 18864
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033D9")]
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x040049B1 RID: 18865
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033DA")]
		[SerializeField]
		private ScrollItemIcon m_ItemPrefab;

		// Token: 0x040049B2 RID: 18866
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033DB")]
		private List<ScrollItemData> m_DataList;

		// Token: 0x040049B3 RID: 18867
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40033DC")]
		private List<ScrollItemIcon> m_InstList;

		// Token: 0x040049B4 RID: 18868
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40033DD")]
		private List<ScrollItemIcon> m_InactiveInstList;

		// Token: 0x040049B5 RID: 18869
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40033DE")]
		private List<ScrollItemIcon> m_ActiveInstList;

		// Token: 0x040049B6 RID: 18870
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40033DF")]
		private bool m_IsDirty;

		// Token: 0x040049B7 RID: 18871
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40033E0")]
		private float m_ItemWidth;

		// Token: 0x040049B8 RID: 18872
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40033E1")]
		private RectTransform m_ViewPort;

		// Token: 0x040049B9 RID: 18873
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40033E2")]
		private RectTransform m_Content;
	}
}
