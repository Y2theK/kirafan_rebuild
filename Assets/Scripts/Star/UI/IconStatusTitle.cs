﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CE4 RID: 3300
	[Token(Token = "0x20008D0")]
	[StructLayout(3)]
	public class IconStatusTitle : MonoBehaviour
	{
		// Token: 0x06003C67 RID: 15463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003724")]
		[Address(RVA = "0x1014D3AFC", Offset = "0x14D3AFC", VA = "0x1014D3AFC")]
		public void Apply(IconStatusTitle.eType type)
		{
		}

		// Token: 0x06003C68 RID: 15464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003725")]
		[Address(RVA = "0x1014D3BE0", Offset = "0x14D3BE0", VA = "0x1014D3BE0")]
		public IconStatusTitle()
		{
		}

		// Token: 0x04004BB3 RID: 19379
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003537")]
		[SerializeField]
		private Sprite m_SpriteLv;

		// Token: 0x04004BB4 RID: 19380
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003538")]
		[SerializeField]
		private Sprite m_SpriteHp;

		// Token: 0x04004BB5 RID: 19381
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003539")]
		[SerializeField]
		private Sprite m_SpriteAtk;

		// Token: 0x04004BB6 RID: 19382
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400353A")]
		[SerializeField]
		private Sprite m_SpriteMgc;

		// Token: 0x04004BB7 RID: 19383
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400353B")]
		[SerializeField]
		private Sprite m_SpriteDef;

		// Token: 0x04004BB8 RID: 19384
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400353C")]
		[SerializeField]
		private Sprite m_SpriteMDef;

		// Token: 0x04004BB9 RID: 19385
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400353D")]
		[SerializeField]
		private Sprite m_SpriteSpd;

		// Token: 0x04004BBA RID: 19386
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400353E")]
		[SerializeField]
		private Sprite m_SpriteCost;

		// Token: 0x04004BBB RID: 19387
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400353F")]
		[SerializeField]
		private Sprite m_SpriteFriendShip;

		// Token: 0x04004BBC RID: 19388
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003540")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x02000CE5 RID: 3301
		[Token(Token = "0x20010DB")]
		public enum eType
		{
			// Token: 0x04004BBE RID: 19390
			[Token(Token = "0x40069CC")]
			Lv,
			// Token: 0x04004BBF RID: 19391
			[Token(Token = "0x40069CD")]
			Hp,
			// Token: 0x04004BC0 RID: 19392
			[Token(Token = "0x40069CE")]
			Atk,
			// Token: 0x04004BC1 RID: 19393
			[Token(Token = "0x40069CF")]
			Mgc,
			// Token: 0x04004BC2 RID: 19394
			[Token(Token = "0x40069D0")]
			Def,
			// Token: 0x04004BC3 RID: 19395
			[Token(Token = "0x40069D1")]
			MDef,
			// Token: 0x04004BC4 RID: 19396
			[Token(Token = "0x40069D2")]
			Spd,
			// Token: 0x04004BC5 RID: 19397
			[Token(Token = "0x40069D3")]
			Cost,
			// Token: 0x04004BC6 RID: 19398
			[Token(Token = "0x40069D4")]
			FriendShip
		}
	}
}
