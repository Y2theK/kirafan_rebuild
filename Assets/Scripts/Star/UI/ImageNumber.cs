﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D08 RID: 3336
	[Token(Token = "0x20008EC")]
	[StructLayout(3)]
	public class ImageNumber : MonoBehaviour
	{
		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x06003CF6 RID: 15606 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700044B")]
		public RectTransform CacheRectTransform
		{
			[Token(Token = "0x60037B2")]
			[Address(RVA = "0x1014D3BE8", Offset = "0x14D3BE8", VA = "0x1014D3BE8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003CF7 RID: 15607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B3")]
		[Address(RVA = "0x1014D3C98", Offset = "0x14D3C98", VA = "0x1014D3C98")]
		private void Awake()
		{
		}

		// Token: 0x06003CF8 RID: 15608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B4")]
		[Address(RVA = "0x1014D3D40", Offset = "0x14D3D40", VA = "0x1014D3D40")]
		public void SetValue(int value)
		{
		}

		// Token: 0x06003CF9 RID: 15609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B5")]
		[Address(RVA = "0x1014D3E34", Offset = "0x14D3E34", VA = "0x1014D3E34")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06003CFA RID: 15610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B6")]
		[Address(RVA = "0x1014D3E9C", Offset = "0x14D3E9C", VA = "0x1014D3E9C")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06003CFB RID: 15611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B7")]
		[Address(RVA = "0x1014D3ED8", Offset = "0x14D3ED8", VA = "0x1014D3ED8")]
		public ImageNumber()
		{
		}

		// Token: 0x04004C60 RID: 19552
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035A3")]
		[SerializeField]
		private Image m_Image;

		// Token: 0x04004C61 RID: 19553
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035A4")]
		[SerializeField]
		private Sprite m_0;

		// Token: 0x04004C62 RID: 19554
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035A5")]
		[SerializeField]
		private Sprite m_1;

		// Token: 0x04004C63 RID: 19555
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035A6")]
		[SerializeField]
		private Sprite m_2;

		// Token: 0x04004C64 RID: 19556
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035A7")]
		[SerializeField]
		private Sprite m_3;

		// Token: 0x04004C65 RID: 19557
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40035A8")]
		[SerializeField]
		private Sprite m_4;

		// Token: 0x04004C66 RID: 19558
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40035A9")]
		[SerializeField]
		private Sprite m_5;

		// Token: 0x04004C67 RID: 19559
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40035AA")]
		[SerializeField]
		private Sprite m_6;

		// Token: 0x04004C68 RID: 19560
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40035AB")]
		[SerializeField]
		private Sprite m_7;

		// Token: 0x04004C69 RID: 19561
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40035AC")]
		[SerializeField]
		private Sprite m_8;

		// Token: 0x04004C6A RID: 19562
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40035AD")]
		[SerializeField]
		private Sprite m_9;

		// Token: 0x04004C6B RID: 19563
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40035AE")]
		private RectTransform m_Transform;
	}
}
