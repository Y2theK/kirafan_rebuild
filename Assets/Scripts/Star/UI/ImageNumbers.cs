﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D09 RID: 3337
	[Token(Token = "0x20008ED")]
	[StructLayout(3)]
	public class ImageNumbers : MonoBehaviour
	{
		// Token: 0x06003CFC RID: 15612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B8")]
		[Address(RVA = "0x1014C9904", Offset = "0x14C9904", VA = "0x1014C9904")]
		public void SetValue(long value)
		{
		}

		// Token: 0x06003CFD RID: 15613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037B9")]
		[Address(RVA = "0x1014D3EE0", Offset = "0x14D3EE0", VA = "0x1014D3EE0")]
		public void SetColor(Color color)
		{
		}

		// Token: 0x06003CFE RID: 15614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037BA")]
		[Address(RVA = "0x1014D3FC0", Offset = "0x14D3FC0", VA = "0x1014D3FC0")]
		public void SetScale(float scale)
		{
		}

		// Token: 0x06003CFF RID: 15615 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60037BB")]
		[Address(RVA = "0x1014D40D0", Offset = "0x14D40D0", VA = "0x1014D40D0")]
		public ImageNumber GetNumber(int idx)
		{
			return null;
		}

		// Token: 0x06003D00 RID: 15616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037BC")]
		[Address(RVA = "0x1014D4120", Offset = "0x14D4120", VA = "0x1014D4120")]
		public ImageNumbers()
		{
		}

		// Token: 0x04004C6C RID: 19564
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035AF")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012462C", Offset = "0x12462C")]
		private ImageNumber[] m_Numbers;

		// Token: 0x04004C6D RID: 19565
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035B0")]
		[SerializeField]
		private bool m_RenderOffOnZero;

		// Token: 0x04004C6E RID: 19566
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x40035B1")]
		[SerializeField]
		private bool m_EnableAutoCenterling;

		// Token: 0x04004C6F RID: 19567
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40035B2")]
		[SerializeField]
		private float m_CenterlingOneDigitInterval;

		// Token: 0x04004C70 RID: 19568
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035B3")]
		private RectTransform m_RectTransform;
	}
}
