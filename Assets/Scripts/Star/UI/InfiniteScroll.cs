﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Star.UI
{
	// Token: 0x02000D0A RID: 3338
	[Token(Token = "0x20008EE")]
	[StructLayout(3)]
	public class InfiniteScroll : MonoBehaviour, IBeginDragHandler, IEventSystemHandler, IDragHandler, IEndDragHandler
	{
		// Token: 0x1400006F RID: 111
		// (add) Token: 0x06003D01 RID: 15617 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003D02 RID: 15618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400006F")]
		public event Action OnChangedCurrent
		{
			[Token(Token = "0x60037BD")]
			[Address(RVA = "0x1014D4138", Offset = "0x14D4138", VA = "0x1014D4138")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60037BE")]
			[Address(RVA = "0x1014D4244", Offset = "0x14D4244", VA = "0x1014D4244")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000070 RID: 112
		// (add) Token: 0x06003D03 RID: 15619 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003D04 RID: 15620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000070")]
		public event Action OnDragAction
		{
			[Token(Token = "0x60037BF")]
			[Address(RVA = "0x1014D4350", Offset = "0x14D4350", VA = "0x1014D4350")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60037C0")]
			[Address(RVA = "0x1014D445C", Offset = "0x14D445C", VA = "0x1014D445C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003D05 RID: 15621 RVA: 0x00018768 File Offset: 0x00016968
		[Token(Token = "0x60037C1")]
		[Address(RVA = "0x1014D4568", Offset = "0x14D4568", VA = "0x1014D4568")]
		public int GetNowSelect()
		{
			return 0;
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x06003D06 RID: 15622 RVA: 0x00018780 File Offset: 0x00016980
		// (set) Token: 0x06003D07 RID: 15623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700044C")]
		public bool EnableReserveFlick
		{
			[Token(Token = "0x60037C2")]
			[Address(RVA = "0x1014D4570", Offset = "0x14D4570", VA = "0x1014D4570")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x60037C3")]
			[Address(RVA = "0x1014D4578", Offset = "0x14D4578", VA = "0x1014D4578")]
			set
			{
			}
		}

		// Token: 0x06003D08 RID: 15624 RVA: 0x00018798 File Offset: 0x00016998
		[Token(Token = "0x60037C4")]
		[Address(RVA = "0x1014D4580", Offset = "0x14D4580", VA = "0x1014D4580")]
		public int GetNowSelectDataIdx()
		{
			return 0;
		}

		// Token: 0x06003D09 RID: 15625 RVA: 0x000187B0 File Offset: 0x000169B0
		[Token(Token = "0x60037C5")]
		[Address(RVA = "0x1014D46E0", Offset = "0x14D46E0", VA = "0x1014D46E0")]
		public float GetNormalizedScrollValue()
		{
			return 0f;
		}

		// Token: 0x06003D0A RID: 15626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037C6")]
		[Address(RVA = "0x1014D4738", Offset = "0x14D4738", VA = "0x1014D4738")]
		public void SetNotInfiniteFlag(bool flg)
		{
		}

		// Token: 0x06003D0B RID: 15627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037C7")]
		[Address(RVA = "0x1014D47DC", Offset = "0x14D47DC", VA = "0x1014D47DC", Slot = "7")]
		public virtual void Setup()
		{
		}

		// Token: 0x06003D0C RID: 15628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037C8")]
		[Address(RVA = "0x1014D4A08", Offset = "0x14D4A08", VA = "0x1014D4A08", Slot = "8")]
		protected virtual void Update()
		{
		}

		// Token: 0x06003D0D RID: 15629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037C9")]
		[Address(RVA = "0x1014D5A18", Offset = "0x14D5A18", VA = "0x1014D5A18", Slot = "9")]
		protected virtual void LateUpdate()
		{
		}

		// Token: 0x06003D0E RID: 15630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CA")]
		[Address(RVA = "0x1014D520C", Offset = "0x14D520C", VA = "0x1014D520C")]
		private void AdjustPosition(bool immediate = false)
		{
		}

		// Token: 0x06003D0F RID: 15631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CB")]
		[Address(RVA = "0x1014D6728", Offset = "0x14D6728", VA = "0x1014D6728")]
		protected void RefreshOverScroll()
		{
		}

		// Token: 0x06003D10 RID: 15632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CC")]
		[Address(RVA = "0x1014D6968", Offset = "0x14D6968", VA = "0x1014D6968")]
		protected void ScrollEase(float goal)
		{
		}

		// Token: 0x06003D11 RID: 15633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CD")]
		[Address(RVA = "0x1014D5A54", Offset = "0x14D5A54", VA = "0x1014D5A54")]
		protected void OnUpdateEase()
		{
		}

		// Token: 0x06003D12 RID: 15634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CE")]
		[Address(RVA = "0x1014D6978", Offset = "0x14D6978", VA = "0x1014D6978")]
		protected void OnCompleteEase()
		{
		}

		// Token: 0x06003D13 RID: 15635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037CF")]
		[Address(RVA = "0x1014D5018", Offset = "0x14D5018", VA = "0x1014D5018")]
		protected void StopEase()
		{
		}

		// Token: 0x06003D14 RID: 15636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D0")]
		[Address(RVA = "0x1014D6980", Offset = "0x14D6980", VA = "0x1014D6980")]
		public void OnScrollValueChanged()
		{
		}

		// Token: 0x06003D15 RID: 15637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D1")]
		[Address(RVA = "0x1014D5CC8", Offset = "0x14D5CC8", VA = "0x1014D5CC8")]
		public void UpdateItemPosition()
		{
		}

		// Token: 0x06003D16 RID: 15638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D2")]
		[Address(RVA = "0x1014D69E8", Offset = "0x14D69E8", VA = "0x1014D69E8")]
		public void AddItem(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06003D17 RID: 15639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D3")]
		[Address(RVA = "0x1014D70C0", Offset = "0x14D70C0", VA = "0x1014D70C0")]
		public void ChangeItemData(int idx, InfiniteScrollItemData data)
		{
		}

		// Token: 0x06003D18 RID: 15640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D4")]
		[Address(RVA = "0x1014D72E8", Offset = "0x14D72E8", VA = "0x1014D72E8")]
		public void Remove(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06003D19 RID: 15641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D5")]
		[Address(RVA = "0x1014D7360", Offset = "0x14D7360", VA = "0x1014D7360")]
		public void RemoveAll()
		{
		}

		// Token: 0x06003D1A RID: 15642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D6")]
		[Address(RVA = "0x1014D7140", Offset = "0x14D7140", VA = "0x1014D7140")]
		public void Refresh()
		{
		}

		// Token: 0x06003D1B RID: 15643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D7")]
		[Address(RVA = "0x1014D73C8", Offset = "0x14D73C8", VA = "0x1014D73C8")]
		public void ForceApply()
		{
		}

		// Token: 0x06003D1C RID: 15644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D8")]
		[Address(RVA = "0x1014D7500", Offset = "0x14D7500", VA = "0x1014D7500", Slot = "10")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06003D1D RID: 15645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037D9")]
		[Address(RVA = "0x1014D5114", Offset = "0x14D5114", VA = "0x1014D5114")]
		public void SetSelectIdx(int idx, bool move)
		{
		}

		// Token: 0x06003D1E RID: 15646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DA")]
		[Address(RVA = "0x1014D5020", Offset = "0x14D5020", VA = "0x1014D5020")]
		private void SetNowSelect(int idx)
		{
		}

		// Token: 0x06003D1F RID: 15647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DB")]
		[Address(RVA = "0x1014D7658", Offset = "0x14D7658", VA = "0x1014D7658")]
		public void OnClickSignalCallBack(int idx)
		{
		}

		// Token: 0x06003D20 RID: 15648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DC")]
		[Address(RVA = "0x1014D7808", Offset = "0x14D7808", VA = "0x1014D7808")]
		public void OnClickRightButtonCallBack()
		{
		}

		// Token: 0x06003D21 RID: 15649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DD")]
		[Address(RVA = "0x1014D7A38", Offset = "0x14D7A38", VA = "0x1014D7A38")]
		public void OnClickLeftButtonCallBack()
		{
		}

		// Token: 0x06003D22 RID: 15650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DE")]
		[Address(RVA = "0x1014D7C68", Offset = "0x14D7C68", VA = "0x1014D7C68", Slot = "4")]
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003D23 RID: 15651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037DF")]
		[Address(RVA = "0x1014D7C6C", Offset = "0x14D7C6C", VA = "0x1014D7C6C", Slot = "5")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003D24 RID: 15652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E0")]
		[Address(RVA = "0x1014D5148", Offset = "0x14D5148", VA = "0x1014D5148")]
		private void ForceEnableHoldAllButton()
		{
		}

		// Token: 0x06003D25 RID: 15653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E1")]
		[Address(RVA = "0x1014D7E64", Offset = "0x14D7E64", VA = "0x1014D7E64", Slot = "6")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003D26 RID: 15654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E2")]
		[Address(RVA = "0x1014D7F2C", Offset = "0x14D7F2C", VA = "0x1014D7F2C", Slot = "11")]
		public virtual void ForceReleaseDrag()
		{
		}

		// Token: 0x06003D27 RID: 15655 RVA: 0x000187C8 File Offset: 0x000169C8
		[Token(Token = "0x60037E3")]
		[Address(RVA = "0x1014D8000", Offset = "0x14D8000", VA = "0x1014D8000")]
		public bool IsStable()
		{
			return default(bool);
		}

		// Token: 0x06003D28 RID: 15656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E4")]
		[Address(RVA = "0x1014D8024", Offset = "0x14D8024", VA = "0x1014D8024")]
		public InfiniteScroll()
		{
		}

		// Token: 0x04004C71 RID: 19569
		[Token(Token = "0x40035B4")]
		private const int MIN_DISP_ITEM = 3;

		// Token: 0x04004C72 RID: 19570
		[Token(Token = "0x40035B5")]
		private const float MINSPEED = 0.01f;

		// Token: 0x04004C73 RID: 19571
		[Token(Token = "0x40035B6")]
		protected const float FLICK_THRESHOLD = 0.4f;

		// Token: 0x04004C74 RID: 19572
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035B7")]
		protected ScrollRectEx m_ScrollRect;

		// Token: 0x04004C75 RID: 19573
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035B8")]
		[SerializeField]
		private InfiniteScrollItem m_ItemPrefab;

		// Token: 0x04004C76 RID: 19574
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035B9")]
		[SerializeField]
		protected InfiniteScroll.eScrollDirection m_Direction;

		// Token: 0x04004C77 RID: 19575
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40035BA")]
		[SerializeField]
		protected bool m_Adjust;

		// Token: 0x04004C78 RID: 19576
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035BB")]
		[SerializeField]
		protected float m_AdjustSpeed;

		// Token: 0x04004C79 RID: 19577
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035BC")]
		[SerializeField]
		protected PageSignal m_Signal;

		// Token: 0x04004C7A RID: 19578
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40035BD")]
		protected List<InfiniteScrollItemData> m_ItemList;

		// Token: 0x04004C7B RID: 19579
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40035BE")]
		protected List<InfiniteScrollItem> m_EmptyInstList;

		// Token: 0x04004C7C RID: 19580
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40035BF")]
		protected List<InfiniteScrollItem> m_ActiveInstList;

		// Token: 0x04004C7D RID: 19581
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40035C0")]
		protected float m_ScrollValue;

		// Token: 0x04004C7E RID: 19582
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x40035C1")]
		protected float m_ScrollMax;

		// Token: 0x04004C7F RID: 19583
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40035C2")]
		protected float m_ItemWidth;

		// Token: 0x04004C80 RID: 19584
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x40035C3")]
		protected float m_ItemHeight;

		// Token: 0x04004C81 RID: 19585
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40035C4")]
		protected int m_MaxDispItem;

		// Token: 0x04004C82 RID: 19586
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x40035C5")]
		protected int m_DispItemNum;

		// Token: 0x04004C83 RID: 19587
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40035C6")]
		protected int m_NowSelect;

		// Token: 0x04004C84 RID: 19588
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x40035C7")]
		protected float m_ScrollMove;

		// Token: 0x04004C85 RID: 19589
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40035C8")]
		protected bool m_IsDonePrepare;

		// Token: 0x04004C86 RID: 19590
		[Cpp2IlInjected.FieldOffset(Offset = "0x79")]
		[Token(Token = "0x40035C9")]
		protected bool m_IsDirty;

		// Token: 0x04004C87 RID: 19591
		[Cpp2IlInjected.FieldOffset(Offset = "0x7A")]
		[Token(Token = "0x40035CA")]
		protected bool m_IsEasing;

		// Token: 0x04004C88 RID: 19592
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x40035CB")]
		protected float m_EaseGoal;

		// Token: 0x04004C89 RID: 19593
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40035CC")]
		protected bool m_IsReserveFlick;

		// Token: 0x04004C8A RID: 19594
		[Cpp2IlInjected.FieldOffset(Offset = "0x81")]
		[Token(Token = "0x40035CD")]
		protected bool m_EnableReserveFlick;

		// Token: 0x04004C8D RID: 19597
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40035D0")]
		protected bool m_NotInfinite;

		// Token: 0x02000D0B RID: 3339
		[Token(Token = "0x20010E3")]
		protected enum eScrollDirection
		{
			// Token: 0x04004C8F RID: 19599
			[Token(Token = "0x4006A0D")]
			Horizontal,
			// Token: 0x04004C90 RID: 19600
			[Token(Token = "0x4006A0E")]
			Vertical
		}
	}
}
