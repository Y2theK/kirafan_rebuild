﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D0C RID: 3340
	[Token(Token = "0x20008EF")]
	[StructLayout(3)]
	public class InfiniteScrollEx : InfiniteScroll
	{
		// Token: 0x170004A2 RID: 1186
		// (set) Token: 0x06003D29 RID: 15657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700044D")]
		public Func<GameObject, bool> OnBeginDragCallback
		{
			[Token(Token = "0x60037E5")]
			[Address(RVA = "0x1014D80EC", Offset = "0x14D80EC", VA = "0x1014D80EC")]
			set
			{
			}
		}

		// Token: 0x170004A3 RID: 1187
		// (set) Token: 0x06003D2A RID: 15658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700044E")]
		public Action<GameObject> OnEndDragCallback
		{
			[Token(Token = "0x60037E6")]
			[Address(RVA = "0x1014D8124", Offset = "0x14D8124", VA = "0x1014D8124")]
			set
			{
			}
		}

		// Token: 0x06003D2B RID: 15659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E7")]
		[Address(RVA = "0x1014D815C", Offset = "0x14D815C", VA = "0x1014D815C", Slot = "8")]
		protected override void Update()
		{
		}

		// Token: 0x06003D2C RID: 15660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E8")]
		[Address(RVA = "0x1014D8188", Offset = "0x14D8188", VA = "0x1014D8188")]
		protected void InfiniteScrollUpdate()
		{
		}

		// Token: 0x06003D2D RID: 15661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037E9")]
		[Address(RVA = "0x1014D818C", Offset = "0x14D818C", VA = "0x1014D818C")]
		public void UpdateScrollActive()
		{
		}

		// Token: 0x06003D2E RID: 15662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037EA")]
		[Address(RVA = "0x1014D8248", Offset = "0x14D8248", VA = "0x1014D8248")]
		public void UpdateScrollButtonActive()
		{
		}

		// Token: 0x06003D2F RID: 15663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037EB")]
		[Address(RVA = "0x1014D8398", Offset = "0x14D8398", VA = "0x1014D8398", Slot = "12")]
		public virtual void OnLeftButton()
		{
		}

		// Token: 0x06003D30 RID: 15664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037EC")]
		[Address(RVA = "0x1014D83B0", Offset = "0x14D83B0", VA = "0x1014D83B0", Slot = "13")]
		public virtual void OnRightButton()
		{
		}

		// Token: 0x06003D31 RID: 15665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037ED")]
		[Address(RVA = "0x1014D83C8", Offset = "0x14D83C8", VA = "0x1014D83C8", Slot = "14")]
		protected virtual void OnClickScrollButton(int addIdx)
		{
		}

		// Token: 0x06003D32 RID: 15666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037EE")]
		[Address(RVA = "0x1014D83F8", Offset = "0x14D83F8", VA = "0x1014D83F8", Slot = "15")]
		public virtual void Lock(bool isLock)
		{
		}

		// Token: 0x06003D33 RID: 15667 RVA: 0x000187E0 File Offset: 0x000169E0
		[Token(Token = "0x60037EF")]
		[Address(RVA = "0x1014D845C", Offset = "0x14D845C", VA = "0x1014D845C", Slot = "16")]
		public virtual bool IsDrag()
		{
			return default(bool);
		}

		// Token: 0x06003D34 RID: 15668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037F0")]
		[Address(RVA = "0x1014D848C", Offset = "0x14D848C", VA = "0x1014D848C", Slot = "17")]
		public virtual void ReleaseDrag()
		{
		}

		// Token: 0x06003D35 RID: 15669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037F1")]
		[Address(RVA = "0x1014D84C4", Offset = "0x14D84C4", VA = "0x1014D84C4")]
		public InfiniteScrollEx()
		{
		}

		// Token: 0x04004C91 RID: 19601
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40035D1")]
		[SerializeField]
		protected GameObject m_RightButtonObj;

		// Token: 0x04004C92 RID: 19602
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40035D2")]
		[SerializeField]
		protected GameObject m_LeftButtonObj;

		// Token: 0x04004C93 RID: 19603
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40035D3")]
		protected InfiniteScrollEx.eActiveControlType m_ScrollActiveControlType;

		// Token: 0x04004C94 RID: 19604
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x40035D4")]
		protected InfiniteScrollEx.eActiveControlType m_ButtonActiveControlType;

		// Token: 0x02000D0D RID: 3341
		[Token(Token = "0x20010E4")]
		public enum eActiveControlType
		{
			// Token: 0x04004C96 RID: 19606
			[Token(Token = "0x4006A10")]
			None,
			// Token: 0x04004C97 RID: 19607
			[Token(Token = "0x4006A11")]
			AlwaysDeactive,
			// Token: 0x04004C98 RID: 19608
			[Token(Token = "0x4006A12")]
			AlwaysActive,
			// Token: 0x04004C99 RID: 19609
			[Token(Token = "0x4006A13")]
			AutoActive
		}
	}
}
