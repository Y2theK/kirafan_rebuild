﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D0E RID: 3342
	[Token(Token = "0x20008F0")]
	[StructLayout(3)]
	public class InfiniteScrollItem : MonoBehaviour
	{
		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x06003D36 RID: 15670 RVA: 0x000187F8 File Offset: 0x000169F8
		// (set) Token: 0x06003D37 RID: 15671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700044F")]
		public int PosIdx
		{
			[Token(Token = "0x60037F2")]
			[Address(RVA = "0x1014D69D0", Offset = "0x14D69D0", VA = "0x1014D69D0")]
			get
			{
				return 0;
			}
			[Token(Token = "0x60037F3")]
			[Address(RVA = "0x1014D69E0", Offset = "0x14D69E0", VA = "0x1014D69E0")]
			set
			{
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x06003D38 RID: 15672 RVA: 0x00018810 File Offset: 0x00016A10
		// (set) Token: 0x06003D39 RID: 15673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000450")]
		public int DataIdx
		{
			[Token(Token = "0x60037F4")]
			[Address(RVA = "0x1014D69D8", Offset = "0x14D69D8", VA = "0x1014D69D8")]
			get
			{
				return 0;
			}
			[Token(Token = "0x60037F5")]
			[Address(RVA = "0x1014D4F78", Offset = "0x14D4F78", VA = "0x1014D4F78")]
			set
			{
			}
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x06003D3A RID: 15674 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000451")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60037F6")]
			[Address(RVA = "0x1014D4EE0", Offset = "0x14D4EE0", VA = "0x1014D4EE0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x06003D3B RID: 15675 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000452")]
		public GameObject GameObject
		{
			[Token(Token = "0x60037F7")]
			[Address(RVA = "0x1014D4F80", Offset = "0x14D4F80", VA = "0x1014D4F80")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x06003D3C RID: 15676 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x06003D3D RID: 15677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000453")]
		public InfiniteScroll Scroll
		{
			[Token(Token = "0x60037F8")]
			[Address(RVA = "0x1014D84D0", Offset = "0x14D84D0", VA = "0x1014D84D0")]
			get
			{
				return null;
			}
			[Token(Token = "0x60037F9")]
			[Address(RVA = "0x1014D5010", Offset = "0x14D5010", VA = "0x1014D5010")]
			set
			{
			}
		}

		// Token: 0x06003D3E RID: 15678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037FA")]
		[Address(RVA = "0x1014D84D8", Offset = "0x14D84D8", VA = "0x1014D84D8", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x06003D3F RID: 15679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037FB")]
		[Address(RVA = "0x1014D84DC", Offset = "0x14D84DC", VA = "0x1014D84DC", Slot = "5")]
		public virtual void Apply(InfiniteScrollItemData data)
		{
		}

		// Token: 0x06003D40 RID: 15680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037FC")]
		[Address(RVA = "0x1014D84E4", Offset = "0x14D84E4", VA = "0x1014D84E4", Slot = "6")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06003D41 RID: 15681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037FD")]
		[Address(RVA = "0x1014D84E8", Offset = "0x14D84E8", VA = "0x1014D84E8")]
		public InfiniteScrollItem()
		{
		}

		// Token: 0x04004C9A RID: 19610
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035D5")]
		protected InfiniteScroll m_Scroll;

		// Token: 0x04004C9B RID: 19611
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035D6")]
		protected InfiniteScrollItemData m_Data;

		// Token: 0x04004C9C RID: 19612
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035D7")]
		protected int m_PosIdx;

		// Token: 0x04004C9D RID: 19613
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40035D8")]
		protected int m_DataIdx;

		// Token: 0x04004C9E RID: 19614
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035D9")]
		private RectTransform m_RectTransform;

		// Token: 0x04004C9F RID: 19615
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035DA")]
		private GameObject m_GameObject;
	}
}
