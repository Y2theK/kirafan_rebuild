﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D0F RID: 3343
	[Token(Token = "0x20008F1")]
	[StructLayout(3)]
	public class InfiniteScrollItemData
	{
		// Token: 0x06003D42 RID: 15682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60037FE")]
		[Address(RVA = "0x1014D84F0", Offset = "0x14D84F0", VA = "0x1014D84F0")]
		public InfiniteScrollItemData()
		{
		}
	}
}
