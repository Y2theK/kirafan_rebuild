﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DD8 RID: 3544
	[Token(Token = "0x2000984")]
	[StructLayout(3)]
	public class InfoBanner : MonoBehaviour
	{
		// Token: 0x06004175 RID: 16757 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003C02")]
		[Address(RVA = "0x1014D84F8", Offset = "0x14D84F8", VA = "0x1014D84F8")]
		public InfoBannerLoader GetBannerLoader()
		{
			return null;
		}

		// Token: 0x06004176 RID: 16758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C03")]
		[Address(RVA = "0x1014CF44C", Offset = "0x14CF44C", VA = "0x1014CF44C")]
		public void Setup()
		{
		}

		// Token: 0x06004177 RID: 16759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C04")]
		[Address(RVA = "0x1014D8500", Offset = "0x14D8500", VA = "0x1014D8500")]
		public void RequestGetAndPrepare(Action OnResponse)
		{
		}

		// Token: 0x06004178 RID: 16760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C05")]
		[Address(RVA = "0x1014D89C4", Offset = "0x14D89C4", VA = "0x1014D89C4")]
		private void OnResponseCallBack()
		{
		}

		// Token: 0x06004179 RID: 16761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C06")]
		[Address(RVA = "0x1014D89D8", Offset = "0x14D89D8", VA = "0x1014D89D8")]
		private void Update()
		{
		}

		// Token: 0x0600417A RID: 16762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C07")]
		[Address(RVA = "0x1014D9160", Offset = "0x14D9160", VA = "0x1014D9160")]
		private void AddTopBanner()
		{
		}

		// Token: 0x0600417B RID: 16763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C08")]
		[Address(RVA = "0x1014D91E0", Offset = "0x14D91E0", VA = "0x1014D91E0")]
		private void AddWeeklyQuestBanner(int id)
		{
		}

		// Token: 0x0600417C RID: 16764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C09")]
		[Address(RVA = "0x1014D14B0", Offset = "0x14D14B0", VA = "0x1014D14B0")]
		public void Destroy()
		{
		}

		// Token: 0x0600417D RID: 16765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C0A")]
		[Address(RVA = "0x1014D95C8", Offset = "0x14D95C8", VA = "0x1014D95C8")]
		private void UpdateAutoSlide()
		{
		}

		// Token: 0x0600417E RID: 16766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C0B")]
		[Address(RVA = "0x1014D9678", Offset = "0x14D9678", VA = "0x1014D9678")]
		public InfoBanner()
		{
		}

		// Token: 0x0400510D RID: 20749
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003956")]
		[SerializeField]
		private GameObject m_Obj;

		// Token: 0x0400510E RID: 20750
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003957")]
		[SerializeField]
		private InfiniteScroll m_BannerScroll;

		// Token: 0x0400510F RID: 20751
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003958")]
		[SerializeField]
		private Sprite m_WeeklyQuestSprite;

		// Token: 0x04005110 RID: 20752
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003959")]
		[SerializeField]
		private WebViewGroup m_WebViewGroupForWeeklyQuest;

		// Token: 0x04005111 RID: 20753
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400395A")]
		private InfoBannerLoader m_BannerLoader;

		// Token: 0x04005112 RID: 20754
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400395B")]
		private bool m_IsDoneRequest;

		// Token: 0x04005113 RID: 20755
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400395C")]
		public Action m_OnResponse;

		// Token: 0x04005114 RID: 20756
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400395D")]
		private bool m_IsDoneSetupScroll;

		// Token: 0x04005115 RID: 20757
		[Token(Token = "0x400395E")]
		private const float m_AutoScrollInterval = 5f;

		// Token: 0x04005116 RID: 20758
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x400395F")]
		private float m_AutoScrollTime;
	}
}
