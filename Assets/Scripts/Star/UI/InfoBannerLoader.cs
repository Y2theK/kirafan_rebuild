﻿using Meige;
using System;
using System.Collections.Generic;
using UnityEngine;
using WWWTypes;
using GetAllReq = InformationRequestTypes.Getall;
using GetAllResp = InformationResponseTypes.Getall;

namespace Star.UI {
    public class InfoBannerLoader {
        public const string BANNER_PATH = "texture/infobanner.muast";
        public const string BANNER_NAME_BASE = "InfoBanner_";
        public const string BANNER_PATH_LARGE = "texture/infobannerlarge.muast";
        public const string BANNER_NAME_BASE_LARGE = "InfoBannerLarge_";

        private List<InformationData> m_InfoList = new List<InformationData>();
        private ABResourceLoader m_Loader;
        private ABResourceObjectHandler[] m_Hndl = new ABResourceObjectHandler[(int)eInformationBannerType.Num];
        private bool m_IsDonePrepare;
        private List<eInformationBannerType> m_ErrorList = new List<eInformationBannerType>();
        private bool m_IsExecutingRetryDialog;

        private Action m_OnResponse;

        public void Request_InfoBanner(Action onResponse, bool isNeedInfoAPI) {
            m_OnResponse = onResponse;
            if (!isNeedInfoAPI) {
                m_OnResponse();
                return;
            }
            GetAllReq param = new GetAllReq() { platform = Platform.Get() };
            MeigewwwParam wwwParam = InformationRequest.Getall(param, OnResponse_InfoBanner);
            NetworkQueueManager.Request(wwwParam);
        }

        private void OnResponse_InfoBanner(MeigewwwParam wwwParam) {
            GetAllResp getall = InformationResponse.Getall(wwwParam);
            if (getall == null) { return; }

            ResultCode result = getall.GetResult();
            if (result == ResultCode.SUCCESS) {
                if (getall.informations == null || getall.informations.Length <= 0) {
                    m_InfoList.Clear();
                } else {
                    for (int i = 0; i < getall.informations.Length; i++) {
                        Information info = getall.informations[i];
                        if (info.isFeatured != -1) {
                            m_InfoList.Add(new InformationData(info.id, info.imgId, info.url, false, info.startAt, info.endAt, info.dispStartAt, info.dispEndAt, info.sort));
                        }
                    }
                    m_InfoList.Sort(SortComp);
                }
            }
            m_OnResponse.Call();
        }

        private int SortComp(InformationData a, InformationData b) {
            int sort = a.m_Sort.CompareTo(b.m_Sort);
            if (sort != 0) { return sort; }
            return a.m_ID.CompareTo(b.m_ID);
        }

        public List<InformationData> GetList() {
            return m_InfoList;
        }

        public bool IsDonePrepare(eInformationBannerType bannerType) {
            ABResourceObjectHandler hndl = m_Hndl[(int)bannerType];
            return hndl != null && hndl.IsDone() && !hndl.IsError() && m_ErrorList.Count == 0;
        }

        public void Prepare(eInformationBannerType bannerType) {
            if (m_Loader == null) {
                m_Loader = new ABResourceLoader();
                m_ErrorList = new List<eInformationBannerType>();
                m_IsExecutingRetryDialog = false;
            }
            if (bannerType == eInformationBannerType.Normal) {
                m_Hndl[0] = m_Loader.Load(BANNER_PATH);
            } else {
                ABResourceObjectHandler hndl = m_Loader.Load(BANNER_PATH_LARGE);
                hndl.SetDisableAutoRetry(true);
                m_Hndl[(int)bannerType] = hndl;
            }
            m_IsDonePrepare = false;
        }

        public void UpdatePrepare() {
            if (!m_IsDonePrepare && m_Loader != null) {
                m_Loader.Update();
                m_IsDonePrepare = true;
                for (int i = 0; i < m_Hndl.Length; i++) {
                    ABResourceObjectHandler hndl = m_Hndl[i];
                    if (hndl.IsError()) {
                        m_ErrorList.Add((eInformationBannerType)i);
                        m_Loader.Unload(hndl);
                        m_Hndl[i] = null;
                    } else if (!hndl.IsDone()) {
                        m_IsDonePrepare = false;
                        return;
                    }
                }
                if (m_IsDonePrepare && m_ErrorList.Count > 0) {
                    m_IsDonePrepare = false;
                    if (!m_IsExecutingRetryDialog) {
                        m_IsExecutingRetryDialog = true;
                        GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.DownloadErrTitle, eText_MessageDB.DownloadErr, (x) => {
                            m_IsExecutingRetryDialog = false;
                            RetryErrorResource();
                        });
                    }
                }
            }
        }

        private void RetryErrorResource() {
            for (int i = 0; i < m_ErrorList.Count; i++) {
                Prepare(m_ErrorList[i]);
            }
            m_ErrorList.Clear();
        }

        public Sprite GetSprite(string imgID, eInformationBannerType bannerType) {
            string name = null;
            if (bannerType == eInformationBannerType.Normal) {
                name = BANNER_NAME_BASE + imgID;
            } else if (bannerType == eInformationBannerType.Large) {
                name = BANNER_NAME_BASE_LARGE + imgID;
            }
            Sprite result = null;
            ABResourceObjectHandler hndl = m_Hndl[(int)bannerType];
            for (int i = 0; i < hndl.Objs.Length; i++) {
                if (hndl.Objs[i] is Sprite sprite && sprite.name == name) {
                    result = sprite;
                    break;
                }
            }
            return result;
        }

        public void Destroy() { }
    }
}
