﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DDD RID: 3549
	[Token(Token = "0x2000988")]
	[StructLayout(3)]
	public class InfoUI : MenuUIBase
	{
		// Token: 0x06004198 RID: 16792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C24")]
		[Address(RVA = "0x1014D9C5C", Offset = "0x14D9C5C", VA = "0x1014D9C5C")]
		private void SetSelectTab(InfoUI.eTabButton tab)
		{
		}

		// Token: 0x06004199 RID: 16793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C25")]
		[Address(RVA = "0x1014D9DA8", Offset = "0x14D9DA8", VA = "0x1014D9DA8")]
		private void CallbackFromWebView(string msg)
		{
		}

		// Token: 0x0600419A RID: 16794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C26")]
		[Address(RVA = "0x1014D9DBC", Offset = "0x14D9DBC", VA = "0x1014D9DBC")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600419B RID: 16795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C27")]
		[Address(RVA = "0x1014D9E68", Offset = "0x14D9E68", VA = "0x1014D9E68")]
		public void OnClickTabButton(int tabButtonIdx)
		{
		}

		// Token: 0x0600419C RID: 16796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C28")]
		[Address(RVA = "0x1014D9F9C", Offset = "0x14D9F9C", VA = "0x1014D9F9C")]
		public void Setup(string startUrl, InfoBannerLoader loader, int readInformationID)
		{
		}

		// Token: 0x0600419D RID: 16797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C29")]
		[Address(RVA = "0x1014D9E90", Offset = "0x14D9E90", VA = "0x1014D9E90")]
		private void OpenWindow(string url)
		{
		}

		// Token: 0x0600419E RID: 16798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C2A")]
		[Address(RVA = "0x1014DA5BC", Offset = "0x14DA5BC", VA = "0x1014DA5BC")]
		private void OnDoneRequest()
		{
		}

		// Token: 0x0600419F RID: 16799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C2B")]
		[Address(RVA = "0x1014DA5C8", Offset = "0x14DA5C8", VA = "0x1014DA5C8")]
		private void Update()
		{
		}

		// Token: 0x060041A0 RID: 16800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C2C")]
		[Address(RVA = "0x1014DA218", Offset = "0x14DA218", VA = "0x1014DA218")]
		private void ChangeStep(InfoUI.eStep step)
		{
		}

		// Token: 0x060041A1 RID: 16801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C2D")]
		[Address(RVA = "0x1014DB128", Offset = "0x14DB128", VA = "0x1014DB128", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060041A2 RID: 16802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C2E")]
		[Address(RVA = "0x1014DB170", Offset = "0x14DB170", VA = "0x1014DB170", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060041A3 RID: 16803 RVA: 0x00019290 File Offset: 0x00017490
		[Token(Token = "0x6003C2F")]
		[Address(RVA = "0x1014DB344", Offset = "0x14DB344", VA = "0x1014DB344", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060041A4 RID: 16804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C30")]
		[Address(RVA = "0x1014DB354", Offset = "0x14DB354", VA = "0x1014DB354")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060041A5 RID: 16805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C31")]
		[Address(RVA = "0x1014DB37C", Offset = "0x14DB37C", VA = "0x1014DB37C")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x060041A6 RID: 16806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C32")]
		[Address(RVA = "0x1014DB388", Offset = "0x14DB388", VA = "0x1014DB388")]
		private void OnClickBannerButton()
		{
		}

		// Token: 0x060041A7 RID: 16807 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C33")]
		[Address(RVA = "0x1014D9DB8", Offset = "0x14D9DB8", VA = "0x1014D9DB8")]
		public void OnClickBannerButton(string url)
		{
		}

		// Token: 0x060041A8 RID: 16808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C34")]
		[Address(RVA = "0x1014DB118", Offset = "0x14DB118", VA = "0x1014DB118")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x060041A9 RID: 16809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C35")]
		[Address(RVA = "0x1014DB3F0", Offset = "0x14DB3F0", VA = "0x1014DB3F0")]
		public InfoUI()
		{
		}

		// Token: 0x04005138 RID: 20792
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003978")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005139 RID: 20793
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003979")]
		[SerializeField]
		private UIGroup m_BGGroup;

		// Token: 0x0400513A RID: 20794
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400397A")]
		[SerializeField]
		private UIGroup m_TopUIGroup;

		// Token: 0x0400513B RID: 20795
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400397B")]
		[SerializeField]
		private UIGroup m_DetailUIGroup;

		// Token: 0x0400513C RID: 20796
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400397C")]
		[SerializeField]
		private ScrollViewBase m_ScrollView;

		// Token: 0x0400513D RID: 20797
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400397D")]
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x0400513E RID: 20798
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400397E")]
		[SerializeField]
		private CustomButton[] m_TabButtons;

		// Token: 0x0400513F RID: 20799
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400397F")]
		private SwapObj[] m_SwapObjects;

		// Token: 0x04005140 RID: 20800
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003980")]
		private InfoUI.eStep m_Step;

		// Token: 0x04005141 RID: 20801
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003981")]
		private string m_ActiveURL;

		// Token: 0x04005142 RID: 20802
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003982")]
		private int m_ReadInformationID;

		// Token: 0x04005143 RID: 20803
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003983")]
		private InfoUI.eTabButton m_SelectTab;

		// Token: 0x04005144 RID: 20804
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003984")]
		private InfoUI.eType m_Type;

		// Token: 0x04005145 RID: 20805
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003985")]
		private InfoBannerLoader m_BannerLoader;

		// Token: 0x04005146 RID: 20806
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003986")]
		private bool m_isDoneRequest;

		// Token: 0x04005147 RID: 20807
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003987")]
		[SerializeField]
		private RectTransform m_WebViewTargetForTop;

		// Token: 0x04005148 RID: 20808
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003988")]
		[SerializeField]
		private RectTransform m_WebViewTargetForDetails;

		// Token: 0x04005149 RID: 20809
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003989")]
		[SerializeField]
		private WebViewGroup m_WebViewGroup;

		// Token: 0x02000DDE RID: 3550
		[Token(Token = "0x200111B")]
		private enum eStep
		{
			// Token: 0x0400514B RID: 20811
			[Token(Token = "0x4006B0A")]
			Hide,
			// Token: 0x0400514C RID: 20812
			[Token(Token = "0x4006B0B")]
			WaitBannerLoad,
			// Token: 0x0400514D RID: 20813
			[Token(Token = "0x4006B0C")]
			In,
			// Token: 0x0400514E RID: 20814
			[Token(Token = "0x4006B0D")]
			ChildWindowOpen,
			// Token: 0x0400514F RID: 20815
			[Token(Token = "0x4006B0E")]
			WebWindowClose,
			// Token: 0x04005150 RID: 20816
			[Token(Token = "0x4006B0F")]
			WebWindowOpen,
			// Token: 0x04005151 RID: 20817
			[Token(Token = "0x4006B10")]
			Idle,
			// Token: 0x04005152 RID: 20818
			[Token(Token = "0x4006B11")]
			Out,
			// Token: 0x04005153 RID: 20819
			[Token(Token = "0x4006B12")]
			End
		}

		// Token: 0x02000DDF RID: 3551
		[Token(Token = "0x200111C")]
		public enum eTabButton
		{
			// Token: 0x04005155 RID: 20821
			[Token(Token = "0x4006B14")]
			Notice,
			// Token: 0x04005156 RID: 20822
			[Token(Token = "0x4006B15")]
			Mente,
			// Token: 0x04005157 RID: 20823
			[Token(Token = "0x4006B16")]
			Update,
			// Token: 0x04005158 RID: 20824
			[Token(Token = "0x4006B17")]
			Max
		}

		// Token: 0x02000DE0 RID: 3552
		[Token(Token = "0x200111D")]
		private enum eType
		{
			// Token: 0x0400515A RID: 20826
			[Token(Token = "0x4006B19")]
			Top,
			// Token: 0x0400515B RID: 20827
			[Token(Token = "0x4006B1A")]
			Details,
			// Token: 0x0400515C RID: 20828
			[Token(Token = "0x4006B1B")]
			Max
		}
	}
}
