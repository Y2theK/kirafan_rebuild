﻿namespace Star.UI {
    public class InfoUIArg : OverlayUIArgBase {
        public string m_StartUrl;
        public InfoBannerLoader m_BannerLoader;
        public int m_ReadInformationIdOnClose;

        public InfoUIArg(string url, InfoBannerLoader loader, int readInformationIdOnClose) {
            m_StartUrl = url;
            m_BannerLoader = loader;
            m_ReadInformationIdOnClose = readInformationIdOnClose;
        }
    }
}
