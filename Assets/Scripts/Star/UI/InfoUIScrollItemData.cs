﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DDC RID: 3548
	[Token(Token = "0x2000987")]
	[StructLayout(3)]
	public class InfoUIScrollItemData : ScrollItemData
	{
		// Token: 0x06004197 RID: 16791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C23")]
		[Address(RVA = "0x1014DB110", Offset = "0x14DB110", VA = "0x1014DB110")]
		public InfoUIScrollItemData()
		{
		}

		// Token: 0x04005132 RID: 20786
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003972")]
		public int m_ID;

		// Token: 0x04005133 RID: 20787
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003973")]
		public string m_URL;

		// Token: 0x04005134 RID: 20788
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003974")]
		public Sprite m_Sprite;

		// Token: 0x04005135 RID: 20789
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003975")]
		public DateTime m_startTime;

		// Token: 0x04005136 RID: 20790
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003976")]
		public DateTime m_endTime;

		// Token: 0x04005137 RID: 20791
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003977")]
		public InfoUI m_InfoUI;
	}
}
