﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class InputFieldController : MonoBehaviour {
        [Tooltip("null許容.")]
        [SerializeField] private Text m_Title;

        [Tooltip("null許容.")]
        [SerializeField] private Image m_Image;

        [Tooltip("null許容.")]
        [SerializeField] private InputField m_InputField;

        [Tooltip("null許容.InputFieldが有効な場合に出すアイコン.")]
        [SerializeField] private GameObject m_ActiveEditIcon;

        public string GetString() {
            return m_InputField != null ? m_InputField.text : null;
        }

        public void SetTitleString(string title) {
            if (m_Title != null) {
                m_Title.text = title;
            }
        }

        public void SetTitleSprite(Sprite sprite) {
            if (m_Image != null) {
                m_Image.sprite = sprite;
            }
        }

        public void SetInputFieldActive(bool active) {
            if (m_InputField != null) {
                m_InputField.enabled = active;
            }
            if (m_ActiveEditIcon != null) {
                m_ActiveEditIcon.SetActive(active);
            }
        }

        public void SetInputFieldString(string text) {
            if (m_InputField != null) {
                m_InputField.text = text;
            }
        }
    }
}
