﻿using UnityEngine.UI;

namespace Star.UI {
    public class InvisibleGraphic : Graphic {
        protected override void OnPopulateMesh(VertexHelper vh) {
            base.OnPopulateMesh(vh);
            vh.Clear();
        }
    }
}
