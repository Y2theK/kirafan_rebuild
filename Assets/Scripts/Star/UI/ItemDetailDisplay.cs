﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D13 RID: 3347
	[Token(Token = "0x20008F5")]
	[StructLayout(3)]
	public class ItemDetailDisplay : MonoBehaviour
	{
		// Token: 0x06003D56 RID: 15702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003812")]
		[Address(RVA = "0x1014DB8BC", Offset = "0x14DB8BC", VA = "0x1014DB8BC")]
		public void ApplyGem(int num = -1)
		{
		}

		// Token: 0x06003D57 RID: 15703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003813")]
		[Address(RVA = "0x1014DBA8C", Offset = "0x14DBA8C", VA = "0x1014DBA8C")]
		public void ApplyGold(int num = -1)
		{
		}

		// Token: 0x06003D58 RID: 15704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003814")]
		[Address(RVA = "0x1014DBC9C", Offset = "0x14DBC9C", VA = "0x1014DBC9C")]
		public void ApplyKirara(int num = -1)
		{
		}

		// Token: 0x06003D59 RID: 15705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003815")]
		[Address(RVA = "0x1014DBEAC", Offset = "0x14DBEAC", VA = "0x1014DBEAC")]
		public void ApplyItem(int itemID, int num = -1)
		{
		}

		// Token: 0x06003D5A RID: 15706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003816")]
		[Address(RVA = "0x1014DC0B4", Offset = "0x14DC0B4", VA = "0x1014DC0B4")]
		public void ApplyPackage(int itemID, int num = -1)
		{
		}

		// Token: 0x06003D5B RID: 15707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003817")]
		[Address(RVA = "0x1014DC2B4", Offset = "0x14DC2B4", VA = "0x1014DC2B4")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06003D5C RID: 15708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003818")]
		[Address(RVA = "0x1014DC3A4", Offset = "0x14DC3A4", VA = "0x1014DC3A4")]
		public ItemDetailDisplay()
		{
		}

		// Token: 0x04004CAF RID: 19631
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035EA")]
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x04004CB0 RID: 19632
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035EB")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004CB1 RID: 19633
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035EC")]
		[SerializeField]
		private Text m_DetailText;
	}
}
