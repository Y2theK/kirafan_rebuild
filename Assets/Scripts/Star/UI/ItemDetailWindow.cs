﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D14 RID: 3348
	[Token(Token = "0x20008F6")]
	[StructLayout(3)]
	public class ItemDetailWindow : UIGroup
	{
		// Token: 0x06003D5D RID: 15709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003819")]
		[Address(RVA = "0x1014DC3AC", Offset = "0x14DC3AC", VA = "0x1014DC3AC")]
		public void OpenItem(int itemID)
		{
		}

		// Token: 0x06003D5E RID: 15710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381A")]
		[Address(RVA = "0x1014DC4BC", Offset = "0x14DC4BC", VA = "0x1014DC4BC")]
		public void OpenGem()
		{
		}

		// Token: 0x06003D5F RID: 15711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381B")]
		[Address(RVA = "0x1014DC518", Offset = "0x14DC518", VA = "0x1014DC518")]
		public void OpenGold()
		{
		}

		// Token: 0x06003D60 RID: 15712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381C")]
		[Address(RVA = "0x1014DC574", Offset = "0x14DC574", VA = "0x1014DC574")]
		public void OpenKP()
		{
		}

		// Token: 0x06003D61 RID: 15713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381D")]
		[Address(RVA = "0x1014DC5D0", Offset = "0x14DC5D0", VA = "0x1014DC5D0")]
		public void OpenPresent(ePresentType type, int id)
		{
		}

		// Token: 0x06003D62 RID: 15714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381E")]
		[Address(RVA = "0x1014DC60C", Offset = "0x14DC60C", VA = "0x1014DC60C")]
		public ItemDetailWindow()
		{
		}

		// Token: 0x04004CB2 RID: 19634
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40035ED")]
		[SerializeField]
		private ItemDetailDisplay m_Detail;

		// Token: 0x04004CB3 RID: 19635
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40035EE")]
		[SerializeField]
		private GameObject m_objHaveNum;

		// Token: 0x04004CB4 RID: 19636
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40035EF")]
		[SerializeField]
		private Text m_HaveNumText;
	}
}
