﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using CommonLogic;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D1F RID: 3359
	[Token(Token = "0x20008FD")]
	[StructLayout(3)]
	public class ItemDropExtDisplay : MonoBehaviour
	{
		// Token: 0x06003D98 RID: 15768 RVA: 0x00018840 File Offset: 0x00016A40
		[Token(Token = "0x6003854")]
		[Address(RVA = "0x1014DC614", Offset = "0x14DC614", VA = "0x1014DC614")]
		public int GetSwitchNum()
		{
			return 0;
		}

		// Token: 0x06003D99 RID: 15769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003855")]
		[Address(RVA = "0x1014DC6B4", Offset = "0x14DC6B4", VA = "0x1014DC6B4")]
		public void SetFromDropExtTable(Dictionary<int, int> dropExtTable)
		{
		}

		// Token: 0x06003D9A RID: 15770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003856")]
		[Address(RVA = "0x1014DCDB8", Offset = "0x14DCDB8", VA = "0x1014DCDB8")]
		public void SetFromCharaID(int charaID, eEventQuestDropExtPlusType plusType = eEventQuestDropExtPlusType.Self)
		{
		}

		// Token: 0x06003D9B RID: 15771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003857")]
		[Address(RVA = "0x1014DCEE8", Offset = "0x14DCEE8", VA = "0x1014DCEE8")]
		public void Clear()
		{
		}

		// Token: 0x06003D9C RID: 15772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003858")]
		[Address(RVA = "0x1014DCEF0", Offset = "0x14DCEF0", VA = "0x1014DCEF0")]
		public void NextDisp()
		{
		}

		// Token: 0x06003D9D RID: 15773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003859")]
		[Address(RVA = "0x1014DCF10", Offset = "0x14DCF10", VA = "0x1014DCF10")]
		private void Update()
		{
		}

		// Token: 0x06003D9E RID: 15774 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385A")]
		[Address(RVA = "0x1014DD028", Offset = "0x14DD028", VA = "0x1014DD028")]
		private void UpdateObjs()
		{
		}

		// Token: 0x06003D9F RID: 15775 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385B")]
		[Address(RVA = "0x1014DC988", Offset = "0x14DC988", VA = "0x1014DC988")]
		private void ApplyAlpha(int idx, float alpha)
		{
		}

		// Token: 0x06003DA0 RID: 15776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385C")]
		[Address(RVA = "0x1014DD20C", Offset = "0x14DD20C", VA = "0x1014DD20C")]
		private void Hide()
		{
		}

		// Token: 0x06003DA1 RID: 15777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600385D")]
		[Address(RVA = "0x1014DD430", Offset = "0x14DD430", VA = "0x1014DD430")]
		public ItemDropExtDisplay()
		{
		}

		// Token: 0x04004CF2 RID: 19698
		[Token(Token = "0x4003616")]
		private const float DEFAULT_DISPTIME_SEC = 3f;

		// Token: 0x04004CF3 RID: 19699
		[Token(Token = "0x4003617")]
		private const float DEFAULT_FADETIME_SEC = 0.5f;

		// Token: 0x04004CF4 RID: 19700
		[Token(Token = "0x4003618")]
		private const float QUICK_FADESPEED_SCALE = 3f;

		// Token: 0x04004CF5 RID: 19701
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003619")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04004CF6 RID: 19702
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400361A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124BDC", Offset = "0x124BDC")]
		private Text m_Text;

		// Token: 0x04004CF7 RID: 19703
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400361B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124C28", Offset = "0x124C28")]
		private CanvasGroup m_FadeTarget;

		// Token: 0x04004CF8 RID: 19704
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400361C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124C74", Offset = "0x124C74")]
		private CanvasGroup m_DefaultTarget;

		// Token: 0x04004CF9 RID: 19705
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400361D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124CC0", Offset = "0x124CC0")]
		private float m_DispTimeSec;

		// Token: 0x04004CFA RID: 19706
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x400361E")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124D0C", Offset = "0x124D0C")]
		private float m_FadeTimeSec;

		// Token: 0x04004CFB RID: 19707
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400361F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124D58", Offset = "0x124D58")]
		private bool m_Sync;

		// Token: 0x04004CFC RID: 19708
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003620")]
		private int m_Idx;

		// Token: 0x04004CFD RID: 19709
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003621")]
		private Dictionary<int, int> m_DropExtTable;

		// Token: 0x04004CFE RID: 19710
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003622")]
		private int[] m_ItemIDs;

		// Token: 0x04004CFF RID: 19711
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003623")]
		private int[] m_ItemNums;

		// Token: 0x04004D00 RID: 19712
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003624")]
		private ItemDropExtDisplay.eStep m_Step;

		// Token: 0x04004D01 RID: 19713
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4003625")]
		private float m_StepTime;

		// Token: 0x04004D02 RID: 19714
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003626")]
		private bool m_IsQuickFade;

		// Token: 0x02000D20 RID: 3360
		[Token(Token = "0x20010E9")]
		private enum eStep
		{
			// Token: 0x04004D04 RID: 19716
			[Token(Token = "0x4006A2C")]
			Hide,
			// Token: 0x04004D05 RID: 19717
			[Token(Token = "0x4006A2D")]
			In,
			// Token: 0x04004D06 RID: 19718
			[Token(Token = "0x4006A2E")]
			Idle,
			// Token: 0x04004D07 RID: 19719
			[Token(Token = "0x4006A2F")]
			Out
		}
	}
}
