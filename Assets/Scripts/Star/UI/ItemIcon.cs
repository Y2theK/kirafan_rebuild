﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CE6 RID: 3302
	[Token(Token = "0x20008D1")]
	[StructLayout(3)]
	public class ItemIcon : ASyncImage
	{
		// Token: 0x06003C69 RID: 15465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003726")]
		[Address(RVA = "0x1014DD320", Offset = "0x14DD320", VA = "0x1014DD320")]
		public void Apply(int itemID)
		{
		}

		// Token: 0x06003C6A RID: 15466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003727")]
		[Address(RVA = "0x1014DD4B8", Offset = "0x14DD4B8", VA = "0x1014DD4B8", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C6B RID: 15467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003728")]
		[Address(RVA = "0x1014DD4E4", Offset = "0x14DD4E4", VA = "0x1014DD4E4")]
		public ItemIcon()
		{
		}

		// Token: 0x04004BC7 RID: 19399
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003541")]
		protected int m_ItemID;
	}
}
