﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CE7 RID: 3303
	[Token(Token = "0x20008D2")]
	[StructLayout(3)]
	public class ItemIconWithDetail : MonoBehaviour
	{
		// Token: 0x06003C6C RID: 15468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003729")]
		[Address(RVA = "0x1014DD4F4", Offset = "0x14DD4F4", VA = "0x1014DD4F4")]
		public void Apply(int itemID, bool appeal = false)
		{
		}

		// Token: 0x06003C6D RID: 15469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600372A")]
		[Address(RVA = "0x1014DD7BC", Offset = "0x14DD7BC", VA = "0x1014DD7BC")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x06003C6E RID: 15470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600372B")]
		[Address(RVA = "0x1014DD83C", Offset = "0x14DD83C", VA = "0x1014DD83C")]
		public ItemIconWithDetail()
		{
		}

		// Token: 0x04004BC8 RID: 19400
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003542")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04004BC9 RID: 19401
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003543")]
		private int m_ItemID;
	}
}
