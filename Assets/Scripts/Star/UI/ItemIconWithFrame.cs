﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CE8 RID: 3304
	[Token(Token = "0x20008D3")]
	[StructLayout(3)]
	public class ItemIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003C6F RID: 15471 RVA: 0x000186F0 File Offset: 0x000168F0
		[Token(Token = "0x600372C")]
		[Address(RVA = "0x1014DD588", Offset = "0x14DD588", VA = "0x1014DD588")]
		public bool Apply(int itemID, bool appeal = false, bool visibleRarity = true)
		{
			return default(bool);
		}

		// Token: 0x06003C70 RID: 15472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600372D")]
		[Address(RVA = "0x1014DD84C", Offset = "0x14DD84C", VA = "0x1014DD84C")]
		public void Destroy()
		{
		}

		// Token: 0x06003C71 RID: 15473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600372E")]
		[Address(RVA = "0x1014DD8EC", Offset = "0x14DD8EC", VA = "0x1014DD8EC")]
		public void SetEnableRenderItemIcon(bool flg)
		{
		}

		// Token: 0x06003C72 RID: 15474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600372F")]
		[Address(RVA = "0x1014DD93C", Offset = "0x14DD93C", VA = "0x1014DD93C")]
		public ItemIconWithFrame()
		{
		}

		// Token: 0x04004BCA RID: 19402
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003544")]
		[SerializeField]
		private ItemIcon m_Icon;

		// Token: 0x04004BCB RID: 19403
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003545")]
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x04004BCC RID: 19404
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003546")]
		[SerializeField]
		private Image m_Ribbon;

		// Token: 0x04004BCD RID: 19405
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003547")]
		[SerializeField]
		private ReflectFrameImage m_Reflection;
	}
}
