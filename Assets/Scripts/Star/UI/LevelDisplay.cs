﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C9A RID: 3226
	[Token(Token = "0x20008A2")]
	[StructLayout(3)]
	public class LevelDisplay : MonoBehaviour
	{
		// Token: 0x06003ABD RID: 15037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003580")]
		[Address(RVA = "0x1014DD944", Offset = "0x14DD944", VA = "0x1014DD944")]
		public void SetLevel(int nowLevel, int maxLevel)
		{
		}

		// Token: 0x06003ABE RID: 15038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003581")]
		[Address(RVA = "0x1014DD9D8", Offset = "0x14DD9D8", VA = "0x1014DD9D8")]
		public void SetLevel(string nowLevelStr, string maxLevelStr)
		{
		}

		// Token: 0x06003ABF RID: 15039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003582")]
		[Address(RVA = "0x1014DDBBC", Offset = "0x14DDBBC", VA = "0x1014DDBBC")]
		public void SetLimitBreakIconValue(int value)
		{
		}

		// Token: 0x06003AC0 RID: 15040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003583")]
		[Address(RVA = "0x1014DDD0C", Offset = "0x14DDD0C", VA = "0x1014DDD0C")]
		public LevelDisplay()
		{
		}

		// Token: 0x04004981 RID: 18817
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033AA")]
		[SerializeField]
		private LimitBreakIcon m_LimitBreakIcon;

		// Token: 0x04004982 RID: 18818
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033AB")]
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x04004983 RID: 18819
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033AC")]
		[SerializeField]
		private RectTransform m_LevelTextTransform;
	}
}
