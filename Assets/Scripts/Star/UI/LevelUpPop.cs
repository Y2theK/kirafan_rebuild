﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D2D RID: 3373
	[Token(Token = "0x2000908")]
	[StructLayout(3)]
	public class LevelUpPop : MonoBehaviour
	{
		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x06003DCE RID: 15822 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000455")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6003886")]
			[Address(RVA = "0x1014DDD14", Offset = "0x14DDD14", VA = "0x1014DDD14")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003DCF RID: 15823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003887")]
		[Address(RVA = "0x1014DDDAC", Offset = "0x14DDDAC", VA = "0x1014DDDAC")]
		public void Play()
		{
		}

		// Token: 0x06003DD0 RID: 15824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003888")]
		[Address(RVA = "0x1014DDE2C", Offset = "0x14DDE2C", VA = "0x1014DDE2C")]
		private void Update()
		{
		}

		// Token: 0x06003DD1 RID: 15825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003889")]
		[Address(RVA = "0x1014DDEEC", Offset = "0x14DDEEC", VA = "0x1014DDEEC")]
		public LevelUpPop()
		{
		}

		// Token: 0x04004D1E RID: 19742
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003636")]
		[SerializeField]
		private AnimUIPlayer m_Arrow;

		// Token: 0x04004D1F RID: 19743
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003637")]
		[SerializeField]
		private AnimUIPlayer m_Label;

		// Token: 0x04004D20 RID: 19744
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003638")]
		private RectTransform m_RectTransform;

		// Token: 0x04004D21 RID: 19745
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003639")]
		private LevelUpPop.eStep m_Step;

		// Token: 0x02000D2E RID: 3374
		[Token(Token = "0x20010EC")]
		private enum eStep
		{
			// Token: 0x04004D23 RID: 19747
			[Token(Token = "0x4006A38")]
			None,
			// Token: 0x04004D24 RID: 19748
			[Token(Token = "0x4006A39")]
			PlayIn,
			// Token: 0x04004D25 RID: 19749
			[Token(Token = "0x4006A3A")]
			PlayOut
		}
	}
}
