﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CE9 RID: 3305
	[Token(Token = "0x20008D4")]
	[StructLayout(3)]
	public class LimitBreakIcon : MonoBehaviour
	{
		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x06003C73 RID: 15475 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000446")]
		public GameObject GameObject
		{
			[Token(Token = "0x6003730")]
			[Address(RVA = "0x1014DDEF4", Offset = "0x14DDEF4", VA = "0x1014DDEF4")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003C74 RID: 15476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003731")]
		[Address(RVA = "0x1014DDC68", Offset = "0x14DDC68", VA = "0x1014DDC68")]
		public void SetValue(int value)
		{
		}

		// Token: 0x06003C75 RID: 15477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003732")]
		[Address(RVA = "0x1014DDF84", Offset = "0x14DDF84", VA = "0x1014DDF84")]
		public LimitBreakIcon()
		{
		}

		// Token: 0x04004BCE RID: 19406
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003548")]
		[SerializeField]
		private GameObject[] m_IconImageObjs;

		// Token: 0x04004BCF RID: 19407
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003549")]
		private GameObject m_GameObject;
	}
}
