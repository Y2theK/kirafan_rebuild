﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class LoadingUI : MonoBehaviour {
        private const float DELAY_SEC = 2f;

        [SerializeField] private GameObject m_Body;
        [SerializeField] private LoadingUITipsWindow m_TipsWindow;
        [SerializeField] private AnimUIPlayer m_TipsWindowAnim;
        [SerializeField] private CustomButton m_TipsWindowButton;

        [Tooltip("切り替え演出.")]
        [SerializeField] private SceneChange m_SceneChange;

        [Tooltip("タッチ判定を無効化するためのシート.")]
        [SerializeField] private GameObject m_InvisibleGraphic;

        [Tooltip("シーン固有のアニメーション演出など")]
        [SerializeField] private LoadingUIAccessories m_Accessories;

        [Tooltip("進捗バー")]
        [SerializeField] private GameObject m_ProgressBar;

        [SerializeField] private Image m_Gauge;
        [SerializeField] private RectTransform m_FitSafeAreaTarget;

        private eStep m_Step = eStep.None;
        private eTipsCategory m_TipsCategory = eTipsCategory.None;
        private eDisplayMode m_DisplayMode = eDisplayMode.Default;
        private float m_Progress = -1;
        private float m_DelayAccessoriesCount;

        private void Start() {
            m_Body.SetActive(false);
            if (m_TipsWindowButton != null) {
                m_TipsWindowButton.enabled = false;
            }
            m_InvisibleGraphic.SetActive(false);
            m_Accessories.Setup();
            GameSystem.Inst.CmnMsgWindow.FullScreenFilterFlag = true;
        }

        public bool IsOverlay() {
            return m_InvisibleGraphic.activeSelf;
        }

        public void Abort() {
            m_Step = eStep.None;
            m_TipsCategory = eTipsCategory.None;
            if (m_TipsWindowButton != null) {
                m_TipsWindowButton.enabled = false;
            }
            m_Body.SetActive(false);
            m_TipsWindowAnim.Hide();
            m_Accessories.Close();
            m_DelayAccessoriesCount = -1f;
            m_InvisibleGraphic.SetActive(false);
            if (m_SceneChange != null) {
                m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
            }
            GameSystem.Inst.TouchEffectMng.ChangeCamera(GameSystem.Inst.SystemUICamera);
            GameSystem.Inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceCamera);
        }

        private void FitSafeArea(bool flg) {
            if (m_FitSafeAreaTarget != null) {
                float ratio = 1f;
                if (flg) {
                    ratio = GameSystem.Inst.UICamera.rect.width / GameSystem.Inst.FullUICamera.rect.width;
                }
                m_FitSafeAreaTarget.localScale = new Vector3(ratio, ratio, 1f);
                float height = GetComponent<RectTransform>().rect.height;
                m_FitSafeAreaTarget.sizeDelta = new Vector2(0f, (1 - ratio) * height / ratio);
            }
        }

        private void Open(eTipsCategory tipsCategory) {
            m_InvisibleGraphic.SetActive(true);
            m_ProgressBar.SetActive(false);
            SetProgress(0f);
            m_Body.SetActive(true);
            m_TipsWindowAnim.Hide();
            m_TipsCategory = tipsCategory;
            if (tipsCategory != eTipsCategory.None) {
                PlayTips();
            }
            m_Step = eStep.In;
            if (m_DisplayMode == eDisplayMode.NowLoadingOnly) {
                FitSafeArea(true);
            } else {
                FitSafeArea(false);
                GameSystem.Inst.CmnMsgWindow.FullScreenFilterFlag = true;
            }
            m_DelayAccessoriesCount = -1f;
        }

        public void Open(eDisplayMode displayMode = eDisplayMode.Default, eTipsCategory tipsCategory = eTipsCategory.None) {
            m_DisplayMode = displayMode;
            if (displayMode == eDisplayMode.NowLoadingOnly) {
                tipsCategory = eTipsCategory.None;
            }
            Open(tipsCategory);
            if (m_SceneChange != null) {
                if (m_DisplayMode == eDisplayMode.NowLoadingOnly) {
                    m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
                } else {
                    m_SceneChange.Play(SceneChange.eAnimType.In);
                }
            }
        }

        public void OpenQuickIfNotDisplay() {
            if (!m_Body.activeSelf) {
                m_DisplayMode = eDisplayMode.Default;
                Open(eTipsCategory.None);
                if (m_SceneChange != null) {
                    m_SceneChange.PlayQuick(SceneChange.eAnimType.In);
                }
            }
        }

        public void EnableDelay() {
            m_DelayAccessoriesCount = DELAY_SEC;
        }

        public void Close() {
            m_InvisibleGraphic.SetActive(true);
            m_Accessories.Close();
            m_ProgressBar.SetActive(false);
            m_Body.SetActive(true);
            CloseTips();
            GameSystem.Inst.FadeMng.SetFadeRatio(0f);
            m_Step = eStep.Out;
            if (m_SceneChange != null) {
                if (m_DisplayMode == eDisplayMode.NowLoadingOnly) {
                    m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
                } else {
                    m_SceneChange.Play(SceneChange.eAnimType.Out);
                }
            }
            GameSystem.Inst.CmnMsgWindow.FullScreenFilterFlag = false;
            GameSystem.Inst.TouchEffectMng.ChangeCamera(GameSystem.Inst.SystemUICamera);
            GameSystem.Inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceCamera);
        }

        public bool IsComplete() {
            return m_Step != eStep.In && m_Step != eStep.Out;
        }

        private void Update() {
            switch (m_Step) {
                case eStep.In:
                    UpdateIn();
                    break;
                case eStep.Overlay:
                    UpdateOverlay();
                    break;
                case eStep.Out:
                    UpdateOut();
                    break;
            }
        }

        private void UpdateIn() {
            if (m_SceneChange != null && !m_SceneChange.IsComplete()) { return; }

            if (m_DisplayMode != eDisplayMode.NowLoadingOnly) {
                GameSystem.Inst.TouchEffectMng.ChangeRenderMode(RenderMode.ScreenSpaceOverlay);
                if (m_TipsCategory != eTipsCategory.None) {
                    m_TipsWindowAnim.PlayIn();
                }
            }
            if (m_DelayAccessoriesCount <= 0f) {
                m_Accessories.Open();
            }
            m_Step = eStep.Overlay;
        }

        private void UpdateOverlay() {
            if (m_DelayAccessoriesCount > 0f) {
                m_DelayAccessoriesCount -= Time.deltaTime;
                if (m_DelayAccessoriesCount <= 0f) {
                    m_Accessories.Open();
                    m_DelayAccessoriesCount = 0f;
                }
            }
        }

        private void UpdateOut() {
            if (m_SceneChange != null && !m_SceneChange.IsComplete()) { return; }

            m_InvisibleGraphic.SetActive(false);
            if (MissionManager.Inst != null) {
                MissionManager.Inst.ExecuteReserveGetAllOnLoadOpen();
            }
            m_TipsWindowAnim.Hide();
            m_Body.SetActive(false);
            m_Step = eStep.None;
        }

        private void PlayTips() {
            m_TipsWindow.Play(m_TipsCategory);
            if (m_TipsWindowButton != null) {
                m_TipsWindowButton.enabled = true;
            }
        }

        private void CloseTips() {
            if (m_TipsWindowAnim.State == AnimUIPlayer.STATE_IN) {
                m_TipsWindowAnim.Show();
            }
            m_TipsWindowAnim.PlayOut();
            if (m_TipsWindowButton != null) {
                m_TipsWindowButton.enabled = false;
            }
        }

        public void UpdateProgress(float progress) {
            if (m_SceneChange != null && !m_SceneChange.IsComplete()) { return; }

            if (!m_ProgressBar.activeSelf) {
                m_ProgressBar.SetActive(true);
            }
            progress = Mathf.Clamp01(progress);
            if (m_Progress != progress) {
                SetProgress(progress);
            }
        }

        private void SetProgress(float progress) {
            m_Progress = progress;
            m_Gauge.fillAmount = progress;
        }

        private enum eStep {
            None = -1,
            In,
            Overlay,
            Out
        }

        public enum eDisplayMode {
            Default,
            NowLoadingOnly
        }
    }
}
