﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class LoadingUIAccessories : MonoBehaviour {
        private const float LOOPTIME = 1f;

        [Tooltip("読み込み中表示本体.")]
        [SerializeField] private Image m_LabelImage;

        [Tooltip("読み込み中表示本体.")]
        [SerializeField] private Image[] m_LabelDots;

        [Tooltip("アニメーションを行うキャラクタ.")]
        [SerializeField] private GameObject m_MoveLoadIconGameObject;

        private float m_Count;
        private bool m_MatchAct = true;

        protected void Start() { }

        public void Setup() {
            m_LabelImage.gameObject.SetActive(false);
            for (int i = 0; i < m_LabelDots.Length; i++) {
                m_LabelDots[i].gameObject.SetActive(false);
            }
            m_MoveLoadIconGameObject.SetActive(false);
            m_MatchAct = false;
        }

        public void Open() {
            m_LabelImage.gameObject.SetActive(true);
            for (int i = 0; i < m_LabelDots.Length; i++) {
                m_LabelDots[i].gameObject.SetActive(false);
            }
            m_MoveLoadIconGameObject.SetActive(true);
            m_MatchAct = true;
            m_Count = 0f;
        }

        public void Close() {
            m_LabelImage.gameObject.SetActive(false);
            for (int i = 0; i < m_LabelDots.Length; i++) {
                m_LabelDots[i].gameObject.SetActive(false);
            }
            m_MoveLoadIconGameObject.SetActive(false);
            m_MatchAct = false;
        }

        private void Update() {
            if (m_LabelImage.gameObject.activeSelf) {
                for (int i = 0; i < m_LabelDots.Length; i++) {
                    m_LabelDots[i].gameObject.SetActive(i + 1 < m_Count);
                }
                if (m_Count >= 4f) {
                    m_Count = 0f;
                }
                m_Count += 4f * Time.deltaTime / LOOPTIME;
            }
            if (m_MoveLoadIconGameObject.activeSelf) {
                m_MoveLoadIconGameObject.SetActive(m_MatchAct);
            }
        }
    }
}
