﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class LoadingUITipsWindow : MonoBehaviour {
        [Tooltip("Tipsタイトル.")]
        [SerializeField] private Text m_Title;

        [Tooltip("Tips文章.")]
        [SerializeField] private Text m_Text;

        private List<TipsDB_Param> m_ParamList = new List<TipsDB_Param>();
        private int m_NowIndex = -1;

        public void Play(eTipsCategory tipsCategory) {
            m_ParamList.Clear();
            TipsDB tips = GameSystem.Inst.DbMng.TipsLoadingDB;
            for (int i = 0; i < tips.m_Params.Length; i++) {
                if (tips.m_Params[i].m_Category == (int)tipsCategory || tips.m_Params[i].m_Category == (int)eTipsCategory.Common) {
                    m_ParamList.Add(tips.m_Params[i]);
                }
            }
            m_NowIndex = -1;
            ChangeText();
        }

        private void ChangeText() {
            string title = string.Empty;
            string text = string.Empty;
            if (m_ParamList.Count > 0) {
                if (m_NowIndex == -1) {
                    m_NowIndex = Random.Range(0, m_ParamList.Count);
                } else {
                    m_NowIndex = (m_NowIndex + Random.Range(1, m_ParamList.Count - 1)) % m_ParamList.Count;
                }
                title = m_ParamList[m_NowIndex].m_Title;
                text = m_ParamList[m_NowIndex].m_Text;
            }
            m_Title.text = title;
            m_Text.text = text;
        }

        public void OnClickTipsWindow() {
            ChangeText();
        }
    }
}
