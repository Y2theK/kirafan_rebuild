﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FAF RID: 4015
	[Token(Token = "0x2000A73")]
	[StructLayout(3)]
	public class BonusPanel : MonoBehaviour
	{
		// Token: 0x06004C4B RID: 19531 RVA: 0x0001B168 File Offset: 0x00019368
		[Token(Token = "0x60045F4")]
		[Address(RVA = "0x1014DF60C", Offset = "0x14DF60C", VA = "0x1014DF60C")]
		public int GetDay()
		{
			return 0;
		}

		// Token: 0x06004C4C RID: 19532 RVA: 0x0001B180 File Offset: 0x00019380
		[Token(Token = "0x60045F5")]
		[Address(RVA = "0x1014DF614", Offset = "0x14DF614", VA = "0x1014DF614")]
		public int GetIconID()
		{
			return 0;
		}

		// Token: 0x06004C4D RID: 19533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F6")]
		[Address(RVA = "0x1014DF61C", Offset = "0x14DF61C", VA = "0x1014DF61C")]
		public void Apply(RewardUIData uiData, bool dispDayImage)
		{
		}

		// Token: 0x06004C4E RID: 19534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F7")]
		[Address(RVA = "0x1014DF8BC", Offset = "0x14DF8BC", VA = "0x1014DF8BC")]
		public void ApplyIcon(Sprite sprite)
		{
		}

		// Token: 0x06004C4F RID: 19535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F8")]
		[Address(RVA = "0x1014DF8F4", Offset = "0x14DF8F4", VA = "0x1014DF8F4")]
		public void Destroy()
		{
		}

		// Token: 0x06004C50 RID: 19536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F9")]
		[Address(RVA = "0x1014DF928", Offset = "0x14DF928", VA = "0x1014DF928")]
		public void SetIconState(BonusPanel.eIconState state)
		{
		}

		// Token: 0x06004C51 RID: 19537 RVA: 0x0001B198 File Offset: 0x00019398
		[Token(Token = "0x60045FA")]
		[Address(RVA = "0x1014DF9E0", Offset = "0x14DF9E0", VA = "0x1014DF9E0")]
		public bool IsPlayingAnim()
		{
			return default(bool);
		}

		// Token: 0x06004C52 RID: 19538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045FB")]
		[Address(RVA = "0x1014DFA44", Offset = "0x14DFA44", VA = "0x1014DFA44")]
		public BonusPanel()
		{
		}

		// Token: 0x04005E29 RID: 24105
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004201")]
		[SerializeField]
		private Image m_Icon;

		// Token: 0x04005E2A RID: 24106
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004202")]
		[SerializeField]
		private AnimUIPlayer m_GetIcon;

		// Token: 0x04005E2B RID: 24107
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004203")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012C33C", Offset = "0x12C33C")]
		private Image m_DayImage;

		// Token: 0x04005E2C RID: 24108
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004204")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012C388", Offset = "0x12C388")]
		private Sprite[] m_EventDaySprites;

		// Token: 0x04005E2D RID: 24109
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004205")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012C3D4", Offset = "0x12C3D4")]
		private RectTransform m_GetIconScale;

		// Token: 0x04005E2E RID: 24110
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004206")]
		private int m_Day;

		// Token: 0x04005E2F RID: 24111
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4004207")]
		private int m_IconID;

		// Token: 0x02000FB0 RID: 4016
		[Token(Token = "0x2001200")]
		public enum eIconState
		{
			// Token: 0x04005E31 RID: 24113
			[Token(Token = "0x4006F6C")]
			None,
			// Token: 0x04005E32 RID: 24114
			[Token(Token = "0x4006F6D")]
			Anim,
			// Token: 0x04005E33 RID: 24115
			[Token(Token = "0x4006F6E")]
			Received
		}
	}
}
