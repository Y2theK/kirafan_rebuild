﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FB4 RID: 4020
	[Token(Token = "0x2000A76")]
	[StructLayout(3)]
	public class LoginBonusUI : MenuUIBase
	{
		// Token: 0x06004C55 RID: 19541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045FE")]
		[Address(RVA = "0x1014DFA4C", Offset = "0x14DFA4C", VA = "0x1014DFA4C")]
		private void Update()
		{
		}

		// Token: 0x06004C56 RID: 19542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045FF")]
		[Address(RVA = "0x1014DFA50", Offset = "0x14DFA50", VA = "0x1014DFA50")]
		private void UpdateStep()
		{
		}

		// Token: 0x06004C57 RID: 19543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004600")]
		[Address(RVA = "0x1014E071C", Offset = "0x14E071C", VA = "0x1014E071C")]
		private void ChangeStep(LoginBonusUI.eStep step)
		{
		}

		// Token: 0x06004C58 RID: 19544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004601")]
		[Address(RVA = "0x1014E0EC0", Offset = "0x14E0EC0", VA = "0x1014E0EC0")]
		public void Setup(List<PageUIData> uiDataList)
		{
		}

		// Token: 0x06004C59 RID: 19545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004602")]
		[Address(RVA = "0x1014E1014", Offset = "0x14E1014", VA = "0x1014E1014", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004C5A RID: 19546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004603")]
		[Address(RVA = "0x1014E10C0", Offset = "0x14E10C0", VA = "0x1014E10C0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004C5B RID: 19547 RVA: 0x0001B1B0 File Offset: 0x000193B0
		[Token(Token = "0x6004604")]
		[Address(RVA = "0x1014E1238", Offset = "0x14E1238", VA = "0x1014E1238", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004C5C RID: 19548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004605")]
		[Address(RVA = "0x1014E1248", Offset = "0x14E1248", VA = "0x1014E1248", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004C5D RID: 19549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004606")]
		[Address(RVA = "0x1014E0898", Offset = "0x14E0898", VA = "0x1014E0898")]
		private void PrepareBonus()
		{
		}

		// Token: 0x06004C5E RID: 19550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004607")]
		[Address(RVA = "0x1014E0400", Offset = "0x14E0400", VA = "0x1014E0400")]
		private void ApplyBG(int imageID)
		{
		}

		// Token: 0x06004C5F RID: 19551 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004608")]
		[Address(RVA = "0x1014E0554", Offset = "0x14E0554", VA = "0x1014E0554")]
		private Sprite GetIcon(int iconID)
		{
			return null;
		}

		// Token: 0x06004C60 RID: 19552 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004609")]
		[Address(RVA = "0x1014E068C", Offset = "0x14E068C", VA = "0x1014E068C")]
		private void InStart()
		{
		}

		// Token: 0x06004C61 RID: 19553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460A")]
		[Address(RVA = "0x1014E128C", Offset = "0x14E128C", VA = "0x1014E128C")]
		public void OnClickSkipButtonCallBack()
		{
		}

		// Token: 0x06004C62 RID: 19554 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460B")]
		[Address(RVA = "0x1014E135C", Offset = "0x14E135C", VA = "0x1014E135C")]
		public void OnClickSkipAllButtonCallBack()
		{
		}

		// Token: 0x06004C63 RID: 19555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460C")]
		[Address(RVA = "0x1014E12B8", Offset = "0x14E12B8", VA = "0x1014E12B8")]
		private void SkipStamp()
		{
		}

		// Token: 0x06004C64 RID: 19556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460D")]
		[Address(RVA = "0x1014E0888", Offset = "0x14E0888", VA = "0x1014E0888")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x06004C65 RID: 19557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460E")]
		[Address(RVA = "0x1014E13B0", Offset = "0x14E13B0", VA = "0x1014E13B0")]
		public LoginBonusUI()
		{
		}

		// Token: 0x04005E3E RID: 24126
		[Token(Token = "0x400420F")]
		private const int ROW_MAX = 4;

		// Token: 0x04005E3F RID: 24127
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004210")]
		private LoginBonusUI.eStep m_Step;

		// Token: 0x04005E40 RID: 24128
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004211")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005E41 RID: 24129
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004212")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005E42 RID: 24130
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004213")]
		[SerializeField]
		private BonusPanel m_BonusPanelPrefab;

		// Token: 0x04005E43 RID: 24131
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004214")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012C450", Offset = "0x12C450")]
		private RectTransform m_IconParentNormal;

		// Token: 0x04005E44 RID: 24132
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004215")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012C49C", Offset = "0x12C49C")]
		private RectTransform[] m_IconParentEvents;

		// Token: 0x04005E45 RID: 24133
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004216")]
		[SerializeField]
		private Image m_BGImage;

		// Token: 0x04005E46 RID: 24134
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004217")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04005E47 RID: 24135
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004218")]
		[SerializeField]
		private AnimUIPlayer m_BalloonAnim;

		// Token: 0x04005E48 RID: 24136
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004219")]
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x04005E49 RID: 24137
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400421A")]
		[SerializeField]
		private CustomButton m_SkipButton;

		// Token: 0x04005E4A RID: 24138
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400421B")]
		private List<PageUIData> m_PageUIDataList;

		// Token: 0x04005E4B RID: 24139
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400421C")]
		private List<BonusPanel> m_BonusPanelInstList;

		// Token: 0x04005E4C RID: 24140
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400421D")]
		private int m_StampIdx;

		// Token: 0x04005E4D RID: 24141
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x400421E")]
		private bool m_IsEvent;

		// Token: 0x04005E4E RID: 24142
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400421F")]
		private int m_ImageID;

		// Token: 0x04005E4F RID: 24143
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004220")]
		private ABResourceLoader m_Loader;

		// Token: 0x04005E50 RID: 24144
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004221")]
		private ABResourceObjectHandler m_BackGroundResourceHandler;

		// Token: 0x04005E51 RID: 24145
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004222")]
		private ABResourceObjectHandler m_IconResourceHandler;

		// Token: 0x04005E52 RID: 24146
		[Token(Token = "0x4004223")]
		private const string BG_RESOURCE_PATH = "texture/loginbonusbackground.muast";

		// Token: 0x04005E53 RID: 24147
		[Token(Token = "0x4004224")]
		private const string ICON_RESOURCE_PATH = "texture/loginbonusicon.muast";

		// Token: 0x04005E54 RID: 24148
		[Token(Token = "0x4004225")]
		private const string BG_OBJ_BASENAME = "LoginBonusBackGround_";

		// Token: 0x04005E55 RID: 24149
		[Token(Token = "0x4004226")]
		private const string ICON_OBJ_BASENAME = "LoginBonusIcon_";

		// Token: 0x04005E56 RID: 24150
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004227")]
		private List<Sprite> m_BGSpriteList;

		// Token: 0x04005E57 RID: 24151
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004228")]
		private List<Sprite> m_IconSpriteList;

		// Token: 0x04005E58 RID: 24152
		[Token(Token = "0x4004229")]
		private const float BALLOON_DELAY_SEC = 0.1f;

		// Token: 0x04005E59 RID: 24153
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400422A")]
		private float m_BalloonDelayCount;

		// Token: 0x02000FB5 RID: 4021
		[Token(Token = "0x2001202")]
		private enum eStep
		{
			// Token: 0x04005E5B RID: 24155
			[Token(Token = "0x4006F73")]
			Hide,
			// Token: 0x04005E5C RID: 24156
			[Token(Token = "0x4006F74")]
			WaitPrepare,
			// Token: 0x04005E5D RID: 24157
			[Token(Token = "0x4006F75")]
			In,
			// Token: 0x04005E5E RID: 24158
			[Token(Token = "0x4006F76")]
			Stamp,
			// Token: 0x04005E5F RID: 24159
			[Token(Token = "0x4006F77")]
			WaitStamp,
			// Token: 0x04005E60 RID: 24160
			[Token(Token = "0x4006F78")]
			Idle,
			// Token: 0x04005E61 RID: 24161
			[Token(Token = "0x4006F79")]
			Out,
			// Token: 0x04005E62 RID: 24162
			[Token(Token = "0x4006F7A")]
			End,
			// Token: 0x04005E63 RID: 24163
			[Token(Token = "0x4006F7B")]
			Num
		}
	}
}
