﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FB2 RID: 4018
	[Token(Token = "0x2000A75")]
	[StructLayout(3)]
	public class PageUIData
	{
		// Token: 0x06004C54 RID: 19540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045FD")]
		[Address(RVA = "0x1014E1470", Offset = "0x14E1470", VA = "0x1014E1470")]
		public PageUIData(PageUIData.eType type, int imgID, List<RewardUIData> rewardList, int currentIdx)
		{
		}

		// Token: 0x04005E37 RID: 24119
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400420B")]
		public PageUIData.eType m_Type;

		// Token: 0x04005E38 RID: 24120
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x400420C")]
		public int m_ImgID;

		// Token: 0x04005E39 RID: 24121
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400420D")]
		public List<RewardUIData> m_RewardList;

		// Token: 0x04005E3A RID: 24122
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400420E")]
		public int m_CurrentIdx;

		// Token: 0x02000FB3 RID: 4019
		[Token(Token = "0x2001201")]
		public enum eType
		{
			// Token: 0x04005E3C RID: 24124
			[Token(Token = "0x4006F70")]
			Normal,
			// Token: 0x04005E3D RID: 24125
			[Token(Token = "0x4006F71")]
			Event
		}
	}
}
