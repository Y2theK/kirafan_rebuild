﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FB1 RID: 4017
	[Token(Token = "0x2000A74")]
	[StructLayout(3)]
	public class RewardUIData
	{
		// Token: 0x06004C53 RID: 19539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045FC")]
		[Address(RVA = "0x1014E14C0", Offset = "0x14E14C0", VA = "0x1014E14C0")]
		public RewardUIData(bool isEvent, int iconID, int day)
		{
		}

		// Token: 0x04005E34 RID: 24116
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4004208")]
		public bool m_IsEvent;

		// Token: 0x04005E35 RID: 24117
		[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
		[Token(Token = "0x4004209")]
		public int m_IconID;

		// Token: 0x04005E36 RID: 24118
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400420A")]
		public int m_Day;
	}
}
