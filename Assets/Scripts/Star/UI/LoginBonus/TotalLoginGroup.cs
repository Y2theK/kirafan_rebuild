﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FB7 RID: 4023
	[Token(Token = "0x2000A78")]
	[StructLayout(3)]
	public class TotalLoginGroup : UIGroup
	{
		// Token: 0x06004C67 RID: 19559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004610")]
		[Address(RVA = "0x1014CF934", Offset = "0x14CF934", VA = "0x1014CF934")]
		public void Open(TotalLoginUIData uiData)
		{
		}

		// Token: 0x06004C68 RID: 19560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004611")]
		[Address(RVA = "0x1014E1500", Offset = "0x14E1500", VA = "0x1014E1500")]
		public TotalLoginGroup()
		{
		}

		// Token: 0x04005E68 RID: 24168
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400422F")]
		public readonly float[] WINDOW_WIDTH;

		// Token: 0x04005E69 RID: 24169
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004230")]
		[SerializeField]
		private Text m_DayText;

		// Token: 0x04005E6A RID: 24170
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004231")]
		[SerializeField]
		private RewardPanel[] m_Panels;

		// Token: 0x04005E6B RID: 24171
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004232")]
		[SerializeField]
		private RectTransform m_WindowRect;
	}
}
