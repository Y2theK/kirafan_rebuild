﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.LoginBonus
{
	// Token: 0x02000FB6 RID: 4022
	[Token(Token = "0x2000A77")]
	[StructLayout(3)]
	public class TotalLoginUIData
	{
		// Token: 0x06004C66 RID: 19558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600460F")]
		[Address(RVA = "0x1014E1578", Offset = "0x14E1578", VA = "0x1014E1578")]
		public TotalLoginUIData(int day, ePresentType[] type, int[] id, int[] amount)
		{
		}

		// Token: 0x04005E64 RID: 24164
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x400422B")]
		public int m_Day;

		// Token: 0x04005E65 RID: 24165
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400422C")]
		public ePresentType[] m_Type;

		// Token: 0x04005E66 RID: 24166
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400422D")]
		public int[] m_ID;

		// Token: 0x04005E67 RID: 24167
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400422E")]
		public int[] m_Amount;
	}
}
