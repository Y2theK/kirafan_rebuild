﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D2F RID: 3375
	[Token(Token = "0x2000909")]
	[StructLayout(3)]
	public class LvupPopPlayer : MonoBehaviour
	{
		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x06003DD2 RID: 15826 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000456")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x600388A")]
			[Address(RVA = "0x1014E15C8", Offset = "0x14E15C8", VA = "0x1014E15C8")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003DD3 RID: 15827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600388B")]
		[Address(RVA = "0x1014E1660", Offset = "0x14E1660", VA = "0x1014E1660")]
		public void Play()
		{
		}

		// Token: 0x06003DD4 RID: 15828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600388C")]
		[Address(RVA = "0x1014E1770", Offset = "0x14E1770", VA = "0x1014E1770")]
		public void HidePopUp()
		{
		}

		// Token: 0x06003DD5 RID: 15829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600388D")]
		[Address(RVA = "0x1014E1824", Offset = "0x14E1824", VA = "0x1014E1824")]
		public LvupPopPlayer()
		{
		}

		// Token: 0x04004D26 RID: 19750
		[Token(Token = "0x400363A")]
		private const int INST_NUM = 2;

		// Token: 0x04004D27 RID: 19751
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400363B")]
		[SerializeField]
		private LevelUpPop[] m_Pops;

		// Token: 0x04004D28 RID: 19752
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400363C")]
		private int m_Idx;

		// Token: 0x04004D29 RID: 19753
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400363D")]
		private RectTransform m_RectTransform;
	}
}
