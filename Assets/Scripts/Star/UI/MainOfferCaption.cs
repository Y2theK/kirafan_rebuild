﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D6B RID: 3435
	[Token(Token = "0x2000932")]
	[StructLayout(3)]
	public class MainOfferCaption : MonoBehaviour
	{
		// Token: 0x06003F55 RID: 16213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A02")]
		[Address(RVA = "0x1014E182C", Offset = "0x14E182C", VA = "0x1014E182C")]
		public void Apply(int index, string title, string clientName)
		{
		}

		// Token: 0x06003F56 RID: 16214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A03")]
		[Address(RVA = "0x1014E1AAC", Offset = "0x14E1AAC", VA = "0x1014E1AAC")]
		public MainOfferCaption()
		{
		}

		// Token: 0x04004E7B RID: 20091
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003739")]
		[SerializeField]
		public Text m_Index;

		// Token: 0x04004E7C RID: 20092
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400373A")]
		[SerializeField]
		public Text m_Title;

		// Token: 0x04004E7D RID: 20093
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400373B")]
		[SerializeField]
		public Text m_ClientName;
	}
}
