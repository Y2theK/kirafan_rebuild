﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D6C RID: 3436
	[Token(Token = "0x2000933")]
	[StructLayout(3)]
	public class MainOfferDetailField : MonoBehaviour
	{
		// Token: 0x06003F57 RID: 16215 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A04")]
		[Address(RVA = "0x1014E1AB4", Offset = "0x14E1AB4", VA = "0x1014E1AB4")]
		public void Apply(OfferManager.OfferData offerData)
		{
		}

		// Token: 0x06003F58 RID: 16216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A05")]
		[Address(RVA = "0x1014E1D24", Offset = "0x14E1D24", VA = "0x1014E1D24")]
		public void Destroy()
		{
		}

		// Token: 0x06003F59 RID: 16217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A06")]
		[Address(RVA = "0x1014E1D54", Offset = "0x14E1D54", VA = "0x1014E1D54")]
		protected void OnDestroy()
		{
		}

		// Token: 0x06003F5A RID: 16218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A07")]
		[Address(RVA = "0x1014E1D58", Offset = "0x14E1D58", VA = "0x1014E1D58")]
		public MainOfferDetailField()
		{
		}

		// Token: 0x04004E7E RID: 20094
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400373C")]
		[SerializeField]
		private OfferStateIcon m_StateIcon;

		// Token: 0x04004E7F RID: 20095
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400373D")]
		[SerializeField]
		private MainOfferCaption m_Caption;

		// Token: 0x04004E80 RID: 20096
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400373E")]
		[SerializeField]
		private Text m_FlavorText;

		// Token: 0x04004E81 RID: 20097
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400373F")]
		[SerializeField]
		private Text m_ConditionText;
	}
}
