﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D6D RID: 3437
	[Token(Token = "0x2000934")]
	[StructLayout(3)]
	public class MainOfferListItemData : ScrollItemData
	{
		// Token: 0x06003F5B RID: 16219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A08")]
		[Address(RVA = "0x1014E1D60", Offset = "0x14E1D60", VA = "0x1014E1D60")]
		public MainOfferListItemData(int index, eTitleType titleType, int level, string title, string clientName, OfferDefine.eState state, int remainingPoint, long mngId, Action<long, int> callback)
		{
		}

		// Token: 0x06003F5C RID: 16220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A09")]
		[Address(RVA = "0x1014E1DD8", Offset = "0x14E1DD8", VA = "0x1014E1DD8", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04004E82 RID: 20098
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003740")]
		public int m_Index;

		// Token: 0x04004E83 RID: 20099
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003741")]
		public eTitleType m_TitleType;

		// Token: 0x04004E84 RID: 20100
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003742")]
		public int m_Level;

		// Token: 0x04004E85 RID: 20101
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003743")]
		public string m_Title;

		// Token: 0x04004E86 RID: 20102
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003744")]
		public string m_ClientName;

		// Token: 0x04004E87 RID: 20103
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003745")]
		public OfferDefine.eState m_State;

		// Token: 0x04004E88 RID: 20104
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003746")]
		public int m_RemainingPoint;

		// Token: 0x04004E89 RID: 20105
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003747")]
		public long m_MngId;

		// Token: 0x04004E8A RID: 20106
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003748")]
		public Action<long, int> m_OnClickItemCallback;
	}
}
