﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D6E RID: 3438
	[Token(Token = "0x2000935")]
	[StructLayout(3)]
	public class MainOfferListItemIcon : ScrollItemIcon
	{
		// Token: 0x06003F5D RID: 16221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0A")]
		[Address(RVA = "0x1014E1E2C", Offset = "0x14E1E2C", VA = "0x1014E1E2C", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06003F5E RID: 16222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0B")]
		[Address(RVA = "0x1014E1E5C", Offset = "0x14E1E5C", VA = "0x1014E1E5C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06003F5F RID: 16223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0C")]
		[Address(RVA = "0x1014E21E4", Offset = "0x14E21E4", VA = "0x1014E21E4")]
		public MainOfferListItemIcon()
		{
		}

		// Token: 0x04004E8B RID: 20107
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003749")]
		[SerializeField]
		private OfferStateIcon m_Icon;

		// Token: 0x04004E8C RID: 20108
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400374A")]
		[SerializeField]
		private MainOfferCaption m_Caption;

		// Token: 0x04004E8D RID: 20109
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400374B")]
		[SerializeField]
		private OfferPointCounter m_OfferPointCounter;

		// Token: 0x04004E8E RID: 20110
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400374C")]
		[SerializeField]
		private Text m_LockOnText;

		// Token: 0x04004E8F RID: 20111
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400374D")]
		[SerializeField]
		private GameObject m_LockOnObject;

		// Token: 0x04004E90 RID: 20112
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400374E")]
		[SerializeField]
		private GameObject m_LockOffObject;
	}
}
