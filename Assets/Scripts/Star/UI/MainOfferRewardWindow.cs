﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D6F RID: 3439
	[Token(Token = "0x2000936")]
	[StructLayout(3)]
	public class MainOfferRewardWindow : UIGroup
	{
		// Token: 0x06003F60 RID: 16224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0D")]
		[Address(RVA = "0x1014E21EC", Offset = "0x14E21EC", VA = "0x1014E21EC")]
		public void Setup()
		{
		}

		// Token: 0x06003F61 RID: 16225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0E")]
		[Address(RVA = "0x1014E2220", Offset = "0x14E2220", VA = "0x1014E2220")]
		public void SetupParam(OfferManager.OfferData offerData, bool requestTips)
		{
		}

		// Token: 0x06003F62 RID: 16226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A0F")]
		[Address(RVA = "0x1014E2ACC", Offset = "0x14E2ACC", VA = "0x1014E2ACC")]
		public void Destroy()
		{
		}

		// Token: 0x06003F63 RID: 16227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A10")]
		[Address(RVA = "0x1014E2B48", Offset = "0x14E2B48", VA = "0x1014E2B48", Slot = "12")]
		public override void OnFinishPlayIn()
		{
		}

		// Token: 0x06003F64 RID: 16228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A11")]
		[Address(RVA = "0x1014E2BF0", Offset = "0x14E2BF0", VA = "0x1014E2BF0", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x06003F65 RID: 16229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A12")]
		[Address(RVA = "0x1014E2C18", Offset = "0x14E2C18", VA = "0x1014E2C18", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06003F66 RID: 16230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A13")]
		[Address(RVA = "0x1014E2C40", Offset = "0x14E2C40", VA = "0x1014E2C40")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x06003F67 RID: 16231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A14")]
		[Address(RVA = "0x1014E2C4C", Offset = "0x14E2C4C", VA = "0x1014E2C4C")]
		public MainOfferRewardWindow()
		{
		}

		// Token: 0x04004E91 RID: 20113
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400374F")]
		[SerializeField]
		private GameObject m_OrbReward;

		// Token: 0x04004E92 RID: 20114
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003750")]
		[SerializeField]
		private GameObject m_OrbBlank;

		// Token: 0x04004E93 RID: 20115
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003751")]
		[SerializeField]
		private Text m_OrbBlankText;

		// Token: 0x04004E94 RID: 20116
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003752")]
		[SerializeField]
		private OrbIcon m_OrbIcon;

		// Token: 0x04004E95 RID: 20117
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003753")]
		[SerializeField]
		private Text m_OrbText;

		// Token: 0x04004E96 RID: 20118
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003754")]
		[SerializeField]
		private GameObject m_RoomItemReward;

		// Token: 0x04004E97 RID: 20119
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003755")]
		[SerializeField]
		private GameObject m_RoomItemBlank;

		// Token: 0x04004E98 RID: 20120
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003756")]
		[SerializeField]
		private Text m_RoomItemBlankText;

		// Token: 0x04004E99 RID: 20121
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003757")]
		[SerializeField]
		private RoomObjectIcon m_RoomItemIcon;

		// Token: 0x04004E9A RID: 20122
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003758")]
		[SerializeField]
		private Text m_RoomItemText;

		// Token: 0x04004E9B RID: 20123
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003759")]
		[SerializeField]
		private ScrollViewBase m_ScrollItem;

		// Token: 0x04004E9C RID: 20124
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400375A")]
		[SerializeField]
		private Text m_GoldText;

		// Token: 0x04004E9D RID: 20125
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400375B")]
		private bool m_RequestTips;
	}
}
