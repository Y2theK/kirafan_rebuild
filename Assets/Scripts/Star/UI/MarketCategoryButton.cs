﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DEA RID: 3562
	[Token(Token = "0x200098E")]
	[StructLayout(3)]
	public class MarketCategoryButton : MonoBehaviour
	{
		// Token: 0x060041D5 RID: 16853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5B")]
		[Address(RVA = "0x1014E2C54", Offset = "0x14E2C54", VA = "0x1014E2C54")]
		public void Init(int index, Action<int> onClickButtonCallback)
		{
		}

		// Token: 0x060041D6 RID: 16854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5C")]
		[Address(RVA = "0x1014E2C60", Offset = "0x14E2C60", VA = "0x1014E2C60")]
		public void Apply(string text, bool decided, bool isNew, bool isUpdate)
		{
		}

		// Token: 0x060041D7 RID: 16855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5D")]
		[Address(RVA = "0x1014E2FD8", Offset = "0x14E2FD8", VA = "0x1014E2FD8")]
		public void OnClickButton()
		{
		}

		// Token: 0x060041D8 RID: 16856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5E")]
		[Address(RVA = "0x1014E302C", Offset = "0x14E302C", VA = "0x1014E302C")]
		public MarketCategoryButton()
		{
		}

		// Token: 0x040051B8 RID: 20920
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40039CA")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040051B9 RID: 20921
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40039CB")]
		[SerializeField]
		private Text m_ButtonText;

		// Token: 0x040051BA RID: 20922
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40039CC")]
		[SerializeField]
		private SwitchFade m_SwitchFade;

		// Token: 0x040051BB RID: 20923
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40039CD")]
		[SerializeField]
		private CanvasGroup[] m_CanvasGroupNew;

		// Token: 0x040051BC RID: 20924
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40039CE")]
		[SerializeField]
		private CanvasGroup[] m_CanvasGroupUpdate;

		// Token: 0x040051BD RID: 20925
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40039CF")]
		[SerializeField]
		private CanvasGroup[] m_CanvasGroupMulti;

		// Token: 0x040051BE RID: 20926
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40039D0")]
		[SerializeField]
		private GameObject[] m_BadgeObject;

		// Token: 0x040051BF RID: 20927
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40039D1")]
		private int m_Index;

		// Token: 0x040051C0 RID: 20928
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40039D2")]
		private Action<int> m_OnClickButtonCallback;
	}
}
