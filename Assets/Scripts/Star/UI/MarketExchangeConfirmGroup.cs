﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DEB RID: 3563
	[Token(Token = "0x200098F")]
	[StructLayout(3)]
	public class MarketExchangeConfirmGroup : UIGroup
	{
		// Token: 0x060041D9 RID: 16857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C5F")]
		[Address(RVA = "0x1014E3034", Offset = "0x14E3034", VA = "0x1014E3034")]
		public void Setup(StoreManager.Product product, int buyAmount)
		{
		}

		// Token: 0x060041DA RID: 16858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C60")]
		[Address(RVA = "0x1014E3840", Offset = "0x14E3840", VA = "0x1014E3840")]
		private void SetPeriod(DatabaseManager dbMng, DateTime startAt, DateTime endAt)
		{
		}

		// Token: 0x060041DB RID: 16859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C61")]
		[Address(RVA = "0x1014E3DF0", Offset = "0x14E3DF0", VA = "0x1014E3DF0")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060041DC RID: 16860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C62")]
		[Address(RVA = "0x1014E3E04", Offset = "0x14E3E04", VA = "0x1014E3E04")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x060041DD RID: 16861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C63")]
		[Address(RVA = "0x1014E3E14", Offset = "0x14E3E14", VA = "0x1014E3E14", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060041DE RID: 16862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C64")]
		[Address(RVA = "0x1014E3EA4", Offset = "0x14E3EA4", VA = "0x1014E3EA4")]
		public MarketExchangeConfirmGroup()
		{
		}

		// Token: 0x040051C1 RID: 20929
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40039D3")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x040051C2 RID: 20930
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40039D4")]
		[SerializeField]
		private Text m_PriceTitle;

		// Token: 0x040051C3 RID: 20931
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40039D5")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x040051C4 RID: 20932
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40039D6")]
		[SerializeField]
		private Text m_HaveGemTitle;

		// Token: 0x040051C5 RID: 20933
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40039D7")]
		[SerializeField]
		private Text m_HaveGemText;

		// Token: 0x040051C6 RID: 20934
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40039D8")]
		[SerializeField]
		private Text m_WarningText;

		// Token: 0x040051C7 RID: 20935
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40039D9")]
		[SerializeField]
		private Text m_PeriodText;

		// Token: 0x040051C8 RID: 20936
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40039DA")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x040051C9 RID: 20937
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40039DB")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x040051CA RID: 20938
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40039DC")]
		private StoreManager.Product m_Product;

		// Token: 0x040051CB RID: 20939
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40039DD")]
		private int m_BuyAmount;

		// Token: 0x040051CC RID: 20940
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x40039DE")]
		private bool m_DecideButton;

		// Token: 0x040051CD RID: 20941
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40039DF")]
		public Action<StoreManager.Product, int> OnClickDecideButtonCallback;

		// Token: 0x040051CE RID: 20942
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40039E0")]
		public Action<StoreManager.Product> OnClickCancelButtonCallback;
	}
}
