﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000DEC RID: 3564
	[Token(Token = "0x2000990")]
	[StructLayout(3)]
	public class MarketExchangeSelectGroup : UIGroup
	{
		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x060041DF RID: 16863 RVA: 0x000192D8 File Offset: 0x000174D8
		// (set) Token: 0x060041E0 RID: 16864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000484")]
		public bool IsDirty
		{
			[Token(Token = "0x6003C65")]
			[Address(RVA = "0x1014E3EAC", Offset = "0x14E3EAC", VA = "0x1014E3EAC")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003C66")]
			[Address(RVA = "0x1014E3EB4", Offset = "0x14E3EB4", VA = "0x1014E3EB4")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060041E1 RID: 16865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C67")]
		[Address(RVA = "0x1014E3EBC", Offset = "0x14E3EBC", VA = "0x1014E3EBC")]
		public void Setup(StoreManager.Product product)
		{
		}

		// Token: 0x060041E2 RID: 16866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C68")]
		[Address(RVA = "0x1014E49B8", Offset = "0x14E49B8", VA = "0x1014E49B8", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060041E3 RID: 16867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C69")]
		[Address(RVA = "0x1014E4414", Offset = "0x14E4414", VA = "0x1014E4414")]
		private void UpdateStockCount()
		{
		}

		// Token: 0x060041E4 RID: 16868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C6A")]
		[Address(RVA = "0x1014E45D0", Offset = "0x14E45D0", VA = "0x1014E45D0")]
		private void UpdateUseGemText()
		{
		}

		// Token: 0x060041E5 RID: 16869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C6B")]
		[Address(RVA = "0x1014E465C", Offset = "0x14E465C", VA = "0x1014E465C")]
		private void UpdateHaveGemText()
		{
		}

		// Token: 0x060041E6 RID: 16870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C6C")]
		[Address(RVA = "0x1014E4B58", Offset = "0x14E4B58", VA = "0x1014E4B58")]
		private void OnChangeCurrentNum()
		{
		}

		// Token: 0x060041E7 RID: 16871 RVA: 0x000192F0 File Offset: 0x000174F0
		[Token(Token = "0x6003C6D")]
		[Address(RVA = "0x1014E48E4", Offset = "0x14E48E4", VA = "0x1014E48E4")]
		private int UnlimitedGemToTradeCount()
		{
			return 0;
		}

		// Token: 0x060041E8 RID: 16872 RVA: 0x00019308 File Offset: 0x00017508
		[Token(Token = "0x6003C6E")]
		[Address(RVA = "0x1014E4A9C", Offset = "0x14E4A9C", VA = "0x1014E4A9C")]
		private int Clamp(int tradeCount)
		{
			return 0;
		}

		// Token: 0x060041E9 RID: 16873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C6F")]
		[Address(RVA = "0x1014E4B64", Offset = "0x14E4B64", VA = "0x1014E4B64")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x060041EA RID: 16874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C70")]
		[Address(RVA = "0x1014E4B78", Offset = "0x14E4B78", VA = "0x1014E4B78")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x060041EB RID: 16875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C71")]
		[Address(RVA = "0x1014E4B88", Offset = "0x14E4B88", VA = "0x1014E4B88", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060041EC RID: 16876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C72")]
		[Address(RVA = "0x1014E4BFC", Offset = "0x14E4BFC", VA = "0x1014E4BFC")]
		public MarketExchangeSelectGroup()
		{
		}

		// Token: 0x040051CF RID: 20943
		[Token(Token = "0x40039E1")]
		private const int DEFAULT_BUY_AMOUNT = 1;

		// Token: 0x040051D0 RID: 20944
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40039E2")]
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x040051D1 RID: 20945
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40039E3")]
		[SerializeField]
		private Text m_ProductName;

		// Token: 0x040051D2 RID: 20946
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40039E4")]
		[SerializeField]
		private Text m_StockTitle;

		// Token: 0x040051D3 RID: 20947
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40039E5")]
		[SerializeField]
		private Text m_StockCount;

		// Token: 0x040051D4 RID: 20948
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40039E6")]
		[SerializeField]
		private NumberSelect m_NumberSelect;

		// Token: 0x040051D5 RID: 20949
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40039E7")]
		[SerializeField]
		private Text m_PriceTitle;

		// Token: 0x040051D6 RID: 20950
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40039E8")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x040051D7 RID: 20951
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40039E9")]
		[SerializeField]
		private Text m_HaveGemTitle;

		// Token: 0x040051D8 RID: 20952
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40039EA")]
		[SerializeField]
		private Text m_HaveGemText;

		// Token: 0x040051D9 RID: 20953
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40039EB")]
		[SerializeField]
		private Text m_DecideButtonText;

		// Token: 0x040051DA RID: 20954
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40039EC")]
		[SerializeField]
		private Text m_CancelButtonText;

		// Token: 0x040051DB RID: 20955
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40039ED")]
		private StoreManager.Product m_Product;

		// Token: 0x040051DC RID: 20956
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40039EE")]
		private int m_BuyAmount;

		// Token: 0x040051DD RID: 20957
		[Cpp2IlInjected.FieldOffset(Offset = "0xEC")]
		[Token(Token = "0x40039EF")]
		private bool m_DecideButton;

		// Token: 0x040051DE RID: 20958
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40039F0")]
		public Action<StoreManager.Product, int> OnClickDecideButtonCallback;
	}
}
