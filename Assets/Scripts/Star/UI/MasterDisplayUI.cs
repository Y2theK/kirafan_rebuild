﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DEE RID: 3566
	[Token(Token = "0x2000992")]
	[StructLayout(3)]
	public class MasterDisplayUI : MenuUIBase
	{
		// Token: 0x060041F1 RID: 16881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C77")]
		[Address(RVA = "0x1014E4C0C", Offset = "0x14E4C0C", VA = "0x1014E4C0C")]
		public void SetCompletePlayInCallBack(Action action)
		{
		}

		// Token: 0x060041F2 RID: 16882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C78")]
		[Address(RVA = "0x1014E4C14", Offset = "0x14E4C14", VA = "0x1014E4C14")]
		public void Setup()
		{
		}

		// Token: 0x060041F3 RID: 16883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C79")]
		[Address(RVA = "0x1014E4CC0", Offset = "0x14E4CC0", VA = "0x1014E4CC0")]
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrder, int offset)
		{
		}

		// Token: 0x060041F4 RID: 16884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C7A")]
		[Address(RVA = "0x1014E4D40", Offset = "0x14E4D40", VA = "0x1014E4D40")]
		private void Update()
		{
		}

		// Token: 0x060041F5 RID: 16885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C7B")]
		[Address(RVA = "0x1014E4F24", Offset = "0x14E4F24", VA = "0x1014E4F24")]
		private void ChangeStep(MasterDisplayUI.eStep step)
		{
		}

		// Token: 0x060041F6 RID: 16886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C7C")]
		[Address(RVA = "0x1014E502C", Offset = "0x14E502C", VA = "0x1014E502C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060041F7 RID: 16887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C7D")]
		[Address(RVA = "0x1014E517C", Offset = "0x14E517C", VA = "0x1014E517C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060041F8 RID: 16888 RVA: 0x00019320 File Offset: 0x00017520
		[Token(Token = "0x6003C7E")]
		[Address(RVA = "0x1014E521C", Offset = "0x14E521C", VA = "0x1014E521C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060041F9 RID: 16889 RVA: 0x00019338 File Offset: 0x00017538
		[Token(Token = "0x6003C7F")]
		[Address(RVA = "0x1014E522C", Offset = "0x14E522C", VA = "0x1014E522C")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x060041FA RID: 16890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C80")]
		[Address(RVA = "0x1014E523C", Offset = "0x14E523C", VA = "0x1014E523C")]
		public MasterDisplayUI()
		{
		}

		// Token: 0x040051E6 RID: 20966
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40039F8")]
		private MasterDisplayUI.eStep m_Step;

		// Token: 0x040051E7 RID: 20967
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40039F9")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040051E8 RID: 20968
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40039FA")]
		[SerializeField]
		private RectTransform m_OffsetObj;

		// Token: 0x040051E9 RID: 20969
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40039FB")]
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x040051EA RID: 20970
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40039FC")]
		[SerializeField]
		private AnimUIPlayer m_MasterAnim;

		// Token: 0x040051EB RID: 20971
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40039FD")]
		private Action m_OnCompletePlayIn;

		// Token: 0x02000DEF RID: 3567
		[Token(Token = "0x2001122")]
		private enum eStep
		{
			// Token: 0x040051ED RID: 20973
			[Token(Token = "0x4006B38")]
			Hide,
			// Token: 0x040051EE RID: 20974
			[Token(Token = "0x4006B39")]
			In,
			// Token: 0x040051EF RID: 20975
			[Token(Token = "0x4006B3A")]
			Idle,
			// Token: 0x040051F0 RID: 20976
			[Token(Token = "0x4006B3B")]
			Out,
			// Token: 0x040051F1 RID: 20977
			[Token(Token = "0x4006B3C")]
			End
		}
	}
}
