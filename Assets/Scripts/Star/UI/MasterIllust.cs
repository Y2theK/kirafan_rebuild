﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CEA RID: 3306
	[Token(Token = "0x20008D5")]
	[StructLayout(3)]
	public class MasterIllust : ASyncImage
	{
		// Token: 0x06003C76 RID: 15478 RVA: 0x00018708 File Offset: 0x00016908
		[Token(Token = "0x6003733")]
		[Address(RVA = "0x1014E5244", Offset = "0x14E5244", VA = "0x1014E5244", Slot = "9")]
		public override bool IsDoneLoad()
		{
			return default(bool);
		}

		// Token: 0x06003C77 RID: 15479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003734")]
		[Address(RVA = "0x1014E52A4", Offset = "0x14E52A4", VA = "0x1014E52A4", Slot = "5")]
		public override void Start()
		{
		}

		// Token: 0x06003C78 RID: 15480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003735")]
		[Address(RVA = "0x1014E5358", Offset = "0x14E5358", VA = "0x1014E5358", Slot = "11")]
		protected override void Apply()
		{
		}

		// Token: 0x06003C79 RID: 15481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003736")]
		[Address(RVA = "0x1014E54A4", Offset = "0x14E54A4", VA = "0x1014E54A4")]
		public void Apply(int orbID)
		{
		}

		// Token: 0x06003C7A RID: 15482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003737")]
		[Address(RVA = "0x1014E50E0", Offset = "0x14E50E0", VA = "0x1014E50E0")]
		public void ApplyCurrentEquip()
		{
		}

		// Token: 0x06003C7B RID: 15483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003738")]
		[Address(RVA = "0x1014E5638", Offset = "0x14E5638", VA = "0x1014E5638", Slot = "6")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06003C7C RID: 15484 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003739")]
		[Address(RVA = "0x1014E576C", Offset = "0x14E576C", VA = "0x1014E576C")]
		protected void ApplyLoadedOrbSprite()
		{
		}

		// Token: 0x06003C7D RID: 15485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373A")]
		[Address(RVA = "0x1014E5884", Offset = "0x14E5884", VA = "0x1014E5884")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003C7E RID: 15486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373B")]
		[Address(RVA = "0x1014E5890", Offset = "0x14E5890", VA = "0x1014E5890", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C7F RID: 15487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373C")]
		[Address(RVA = "0x1014E5A10", Offset = "0x14E5A10", VA = "0x1014E5A10")]
		public MasterIllust()
		{
		}

		// Token: 0x04004BD0 RID: 19408
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400354A")]
		[SerializeField]
		private Image m_OrbImage;

		// Token: 0x04004BD1 RID: 19409
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400354B")]
		private SpriteHandler m_OrbHandler;

		// Token: 0x04004BD2 RID: 19410
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400354C")]
		private int m_OrbID;
	}
}
