﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EEA RID: 3818
	[Token(Token = "0x2000A18")]
	[StructLayout(3)]
	public class IFriendUIData : ScrollItemData
	{
		// Token: 0x140000BA RID: 186
		// (add) Token: 0x060047F0 RID: 18416 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047F1 RID: 18417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BA")]
		public event Action<MenuFriendListItem.eAction> m_OnAction
		{
			[Token(Token = "0x6004232")]
			[Address(RVA = "0x1014E5A20", Offset = "0x14E5A20", VA = "0x1014E5A20")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004233")]
			[Address(RVA = "0x1014E5B2C", Offset = "0x14E5B2C", VA = "0x1014E5B2C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060047F2 RID: 18418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004234")]
		[Address(RVA = "0x1014E5C38", Offset = "0x14E5C38", VA = "0x1014E5C38")]
		public IFriendUIData(FriendManager.FriendData pdata, Action<MenuFriendListItem.eAction> OnResponse)
		{
		}

		// Token: 0x060047F3 RID: 18419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004235")]
		[Address(RVA = "0x1014E5C78", Offset = "0x14E5C78", VA = "0x1014E5C78")]
		public void ApplyFriendData(FriendManager.FriendData pdata)
		{
		}

		// Token: 0x060047F4 RID: 18420 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004236")]
		[Address(RVA = "0x1014E5DB0", Offset = "0x14E5DB0", VA = "0x1014E5DB0")]
		public void OnAction(MenuFriendListItem.eAction action)
		{
		}

		// Token: 0x040059AA RID: 22954
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F1F")]
		public long m_FriendMngID;

		// Token: 0x040059AB RID: 22955
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003F20")]
		public long m_PlayerID;

		// Token: 0x040059AC RID: 22956
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003F21")]
		public string m_Name;

		// Token: 0x040059AD RID: 22957
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003F22")]
		public string m_Comment;

		// Token: 0x040059AE RID: 22958
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003F23")]
		public string m_MyCode;

		// Token: 0x040059AF RID: 22959
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F24")]
		public int m_Level;

		// Token: 0x040059B0 RID: 22960
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003F25")]
		public int m_AchievementID;

		// Token: 0x040059B1 RID: 22961
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F26")]
		public UserSupportData m_UserSupportData;

		// Token: 0x040059B2 RID: 22962
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F27")]
		public DateTime m_LoginTime;

		// Token: 0x040059B3 RID: 22963
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F28")]
		public IFriendUIData.eSelect m_Select;

		// Token: 0x02000EEB RID: 3819
		[Token(Token = "0x2001198")]
		public enum eSelect
		{
			// Token: 0x040059B6 RID: 22966
			[Token(Token = "0x4006DD5")]
			Non = -1,
			// Token: 0x040059B7 RID: 22967
			[Token(Token = "0x4006DD6")]
			Base,
			// Token: 0x040059B8 RID: 22968
			[Token(Token = "0x4006DD7")]
			OpponentPropose,
			// Token: 0x040059B9 RID: 22969
			[Token(Token = "0x4006DD8")]
			SelfPropose,
			// Token: 0x040059BA RID: 22970
			[Token(Token = "0x4006DD9")]
			Guest,
			// Token: 0x040059BB RID: 22971
			[Token(Token = "0x4006DDA")]
			RoomBase,
			// Token: 0x040059BC RID: 22972
			[Token(Token = "0x4006DDB")]
			RoomOpponentPropose,
			// Token: 0x040059BD RID: 22973
			[Token(Token = "0x4006DDC")]
			RoomSelfPropose,
			// Token: 0x040059BE RID: 22974
			[Token(Token = "0x4006DDD")]
			RoomGuest
		}
	}
}
