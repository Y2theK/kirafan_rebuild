﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F16 RID: 3862
	[Token(Token = "0x2000A38")]
	[StructLayout(3)]
	public class ITitleUIData : ScrollItemData
	{
		// Token: 0x140000CC RID: 204
		// (add) Token: 0x060048BB RID: 18619 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048BC RID: 18620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CC")]
		public event Action<eTitleType> OnClickTitle
		{
			[Token(Token = "0x60042FB")]
			[Address(RVA = "0x1014E5E10", Offset = "0x14E5E10", VA = "0x1014E5E10")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042FC")]
			[Address(RVA = "0x1014E5F1C", Offset = "0x14E5F1C", VA = "0x1014E5F1C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000CD RID: 205
		// (add) Token: 0x060048BD RID: 18621 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048BE RID: 18622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CD")]
		public event Action<int> OnClickWord
		{
			[Token(Token = "0x60042FD")]
			[Address(RVA = "0x1014E6028", Offset = "0x14E6028", VA = "0x1014E6028")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042FE")]
			[Address(RVA = "0x1014E6134", Offset = "0x14E6134", VA = "0x1014E6134")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048BF RID: 18623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042FF")]
		[Address(RVA = "0x1014E6240", Offset = "0x14E6240", VA = "0x1014E6240", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x060048C0 RID: 18624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004300")]
		[Address(RVA = "0x1014E62AC", Offset = "0x14E62AC", VA = "0x1014E62AC")]
		public ITitleUIData()
		{
		}

		// Token: 0x04005A9B RID: 23195
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003FC2")]
		public string m_TitleName;

		// Token: 0x04005A9C RID: 23196
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003FC3")]
		public eTitleType m_Type;

		// Token: 0x04005A9D RID: 23197
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003FC4")]
		public int m_WordIdx;

		// Token: 0x04005A9E RID: 23198
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003FC5")]
		public bool m_IsSelected;
	}
}
