﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000EF4 RID: 3828
	[Token(Token = "0x2000A1D")]
	[StructLayout(3)]
	public class ItemListScrollItem : ScrollItemIcon
	{
		// Token: 0x0600481E RID: 18462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004260")]
		[Address(RVA = "0x1014E62B4", Offset = "0x14E62B4", VA = "0x1014E62B4", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600481F RID: 18463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004261")]
		[Address(RVA = "0x1014E63B0", Offset = "0x14E63B0", VA = "0x1014E63B0")]
		public void Apply(int itemID, int num)
		{
		}

		// Token: 0x06004820 RID: 18464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004262")]
		[Address(RVA = "0x1014E64B0", Offset = "0x14E64B0", VA = "0x1014E64B0")]
		public ItemListScrollItem()
		{
		}

		// Token: 0x040059F4 RID: 23028
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F49")]
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x040059F5 RID: 23029
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F4A")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x040059F6 RID: 23030
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F4B")]
		public Action<int> OnClick;
	}
}
