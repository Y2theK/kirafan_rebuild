﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EF3 RID: 3827
	[Token(Token = "0x2000A1C")]
	[StructLayout(3)]
	public class ItemListScrollItemData : ScrollItemData
	{
		// Token: 0x0600481C RID: 18460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600425E")]
		[Address(RVA = "0x1014E64B8", Offset = "0x14E64B8", VA = "0x1014E64B8")]
		public ItemListScrollItemData(int itemID, int num)
		{
		}

		// Token: 0x0600481D RID: 18461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600425F")]
		[Address(RVA = "0x1014E64F0", Offset = "0x14E64F0", VA = "0x1014E64F0", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040059F2 RID: 23026
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F47")]
		public int m_ItemID;

		// Token: 0x040059F3 RID: 23027
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003F48")]
		public int m_Num;
	}
}
