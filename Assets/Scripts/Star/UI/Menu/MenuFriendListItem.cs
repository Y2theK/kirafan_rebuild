﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000EE8 RID: 3816
	[Token(Token = "0x2000A17")]
	[StructLayout(3)]
	public class MenuFriendListItem : ScrollItemIcon
	{
		// Token: 0x060047E4 RID: 18404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004226")]
		[Address(RVA = "0x1014E6580", Offset = "0x14E6580", VA = "0x1014E6580", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x060047E5 RID: 18405 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004227")]
		[Address(RVA = "0x1014E6588", Offset = "0x14E6588", VA = "0x1014E6588")]
		public void Apply()
		{
		}

		// Token: 0x060047E6 RID: 18406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004228")]
		[Address(RVA = "0x1014E6594", Offset = "0x14E6594", VA = "0x1014E6594", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060047E7 RID: 18407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004229")]
		[Address(RVA = "0x1014E69F8", Offset = "0x14E69F8", VA = "0x1014E69F8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060047E8 RID: 18408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422A")]
		[Address(RVA = "0x1014E6A30", Offset = "0x14E6A30", VA = "0x1014E6A30")]
		public void OnCallbackButton(int fbuttonno)
		{
		}

		// Token: 0x060047E9 RID: 18409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422B")]
		[Address(RVA = "0x1014E6F50", Offset = "0x14E6F50", VA = "0x1014E6F50")]
		private void OnConfirm(int btn)
		{
		}

		// Token: 0x060047EA RID: 18410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422C")]
		[Address(RVA = "0x1014E7360", Offset = "0x14E7360", VA = "0x1014E7360")]
		private void OnResponse_Terminate(bool isError)
		{
		}

		// Token: 0x060047EB RID: 18411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422D")]
		[Address(RVA = "0x1014E74DC", Offset = "0x14E74DC", VA = "0x1014E74DC")]
		private void OnResponse_Accept(bool isError)
		{
		}

		// Token: 0x060047EC RID: 18412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422E")]
		[Address(RVA = "0x1014E7658", Offset = "0x14E7658", VA = "0x1014E7658")]
		private void OnResponse_Refuse(bool isError)
		{
		}

		// Token: 0x060047ED RID: 18413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600422F")]
		[Address(RVA = "0x1014E77D4", Offset = "0x14E77D4", VA = "0x1014E77D4")]
		private void OnResponse_Cancel(bool isError)
		{
		}

		// Token: 0x060047EE RID: 18414 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004230")]
		[Address(RVA = "0x1014E7950", Offset = "0x14E7950", VA = "0x1014E7950")]
		private void OnResponse_Propose(bool isError)
		{
		}

		// Token: 0x060047EF RID: 18415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004231")]
		[Address(RVA = "0x1014E7ACC", Offset = "0x14E7ACC", VA = "0x1014E7ACC")]
		public MenuFriendListItem()
		{
		}

		// Token: 0x04005999 RID: 22937
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F17")]
		[SerializeField]
		private Text m_UserName;

		// Token: 0x0400599A RID: 22938
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F18")]
		[SerializeField]
		private Text m_Comment;

		// Token: 0x0400599B RID: 22939
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F19")]
		[SerializeField]
		private Text m_Level;

		// Token: 0x0400599C RID: 22940
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003F1A")]
		[SerializeField]
		private Text m_Login;

		// Token: 0x0400599D RID: 22941
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003F1B")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x0400599E RID: 22942
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003F1C")]
		[SerializeField]
		private AchievementImage m_ImgUserAchv;

		// Token: 0x0400599F RID: 22943
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F1D")]
		[SerializeField]
		private GameObject[] m_ButtonGroup;

		// Token: 0x040059A0 RID: 22944
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003F1E")]
		private MenuFriendListItem.eAction m_Select;

		// Token: 0x02000EE9 RID: 3817
		[Token(Token = "0x2001197")]
		public enum eAction
		{
			// Token: 0x040059A2 RID: 22946
			[Token(Token = "0x4006DCC")]
			None = -1,
			// Token: 0x040059A3 RID: 22947
			[Token(Token = "0x4006DCD")]
			RoomIn,
			// Token: 0x040059A4 RID: 22948
			[Token(Token = "0x4006DCE")]
			Support,
			// Token: 0x040059A5 RID: 22949
			[Token(Token = "0x4006DCF")]
			Terminate,
			// Token: 0x040059A6 RID: 22950
			[Token(Token = "0x4006DD0")]
			Accept = 10,
			// Token: 0x040059A7 RID: 22951
			[Token(Token = "0x4006DD1")]
			Refuse,
			// Token: 0x040059A8 RID: 22952
			[Token(Token = "0x4006DD2")]
			Cancel = 20,
			// Token: 0x040059A9 RID: 22953
			[Token(Token = "0x4006DD3")]
			Propose = 30
		}
	}
}
