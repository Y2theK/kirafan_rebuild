﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Home;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000EEF RID: 3823
	[Token(Token = "0x2000A1A")]
	[StructLayout(3)]
	public class MenuFriendUI : MenuFriendUIBase
	{
		// Token: 0x140000BB RID: 187
		// (add) Token: 0x060047FD RID: 18429 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047FE RID: 18430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BB")]
		public event Action OnFriendRoom
		{
			[Token(Token = "0x600423F")]
			[Address(RVA = "0x1014E7ADC", Offset = "0x14E7ADC", VA = "0x1014E7ADC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004240")]
			[Address(RVA = "0x1014E7BEC", Offset = "0x14E7BEC", VA = "0x1014E7BEC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060047FF RID: 18431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004241")]
		[Address(RVA = "0x1014E7CFC", Offset = "0x14E7CFC", VA = "0x1014E7CFC")]
		public void Setup()
		{
		}

		// Token: 0x06004800 RID: 18432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004242")]
		[Address(RVA = "0x1014E8188", Offset = "0x14E8188", VA = "0x1014E8188")]
		public void OnClickSortButton()
		{
		}

		// Token: 0x06004801 RID: 18433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004243")]
		[Address(RVA = "0x1014E819C", Offset = "0x14E819C", VA = "0x1014E819C")]
		public void SetSort(MenuFriendUIBase.eListSortOrderType order)
		{
		}

		// Token: 0x06004802 RID: 18434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004244")]
		[Address(RVA = "0x1014E8308", Offset = "0x14E8308", VA = "0x1014E8308")]
		private void OnResponse_GetAll(bool isError)
		{
		}

		// Token: 0x06004803 RID: 18435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004245")]
		[Address(RVA = "0x1014E8344", Offset = "0x14E8344", VA = "0x1014E8344")]
		private void RefreshFriendList()
		{
		}

		// Token: 0x06004804 RID: 18436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004246")]
		[Address(RVA = "0x1014E87F4", Offset = "0x14E87F4", VA = "0x1014E87F4")]
		private void Update()
		{
		}

		// Token: 0x06004805 RID: 18437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004247")]
		[Address(RVA = "0x1014E8984", Offset = "0x14E8984", VA = "0x1014E8984")]
		private void ChangeStep(MenuFriendUI.eStep step)
		{
		}

		// Token: 0x06004806 RID: 18438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004248")]
		[Address(RVA = "0x1014E8A74", Offset = "0x14E8A74", VA = "0x1014E8A74", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004807 RID: 18439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004249")]
		[Address(RVA = "0x1014E8B30", Offset = "0x14E8B30", VA = "0x1014E8B30", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004808 RID: 18440 RVA: 0x0001A4D8 File Offset: 0x000186D8
		[Token(Token = "0x600424A")]
		[Address(RVA = "0x1014E8BEC", Offset = "0x14E8BEC", VA = "0x1014E8BEC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004809 RID: 18441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600424B")]
		[Address(RVA = "0x1014E8BFC", Offset = "0x14E8BFC", VA = "0x1014E8BFC")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x0600480A RID: 18442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600424C")]
		[Address(RVA = "0x1014E8C00", Offset = "0x14E8C00", VA = "0x1014E8C00")]
		public void OnCopyButtonCallBack()
		{
		}

		// Token: 0x0600480B RID: 18443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600424D")]
		[Address(RVA = "0x1014E8C98", Offset = "0x14E8C98", VA = "0x1014E8C98")]
		public void OnEndEditInputField()
		{
		}

		// Token: 0x0600480C RID: 18444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600424E")]
		[Address(RVA = "0x1014E8D08", Offset = "0x14E8D08", VA = "0x1014E8D08")]
		public void OnFindButtonCallBack()
		{
		}

		// Token: 0x0600480D RID: 18445 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600424F")]
		[Address(RVA = "0x1014E8EEC", Offset = "0x14E8EEC", VA = "0x1014E8EEC")]
		private void OnResponse_Search(bool isError)
		{
		}

		// Token: 0x0600480E RID: 18446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004250")]
		[Address(RVA = "0x1014E81E0", Offset = "0x14E81E0", VA = "0x1014E81E0")]
		public void OnClickCategoryButtonCallBack()
		{
		}

		// Token: 0x0600480F RID: 18447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004251")]
		[Address(RVA = "0x1014E9128", Offset = "0x14E9128", VA = "0x1014E9128")]
		private void SetUpBoardItem(int ftabno)
		{
		}

		// Token: 0x06004810 RID: 18448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004252")]
		[Address(RVA = "0x1014E94F4", Offset = "0x14E94F4", VA = "0x1014E94F4")]
		private void OnAction(MenuFriendListItem.eAction action)
		{
		}

		// Token: 0x06004811 RID: 18449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004253")]
		[Address(RVA = "0x1014E95E0", Offset = "0x14E95E0", VA = "0x1014E95E0")]
		public MenuFriendUI()
		{
		}

		// Token: 0x040059CB RID: 22987
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F2D")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040059CC RID: 22988
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F2E")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040059CD RID: 22989
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F2F")]
		[SerializeField]
		private CustomButton m_FindButton;

		// Token: 0x040059CE RID: 22990
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003F30")]
		[SerializeField]
		private CustomButton m_SortButton;

		// Token: 0x040059CF RID: 22991
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003F31")]
		[SerializeField]
		private GameObject m_SortASC;

		// Token: 0x040059D0 RID: 22992
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003F32")]
		[SerializeField]
		private GameObject m_SortDSC;

		// Token: 0x040059D1 RID: 22993
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F33")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x040059D2 RID: 22994
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003F34")]
		[SerializeField]
		private ScrollViewBase m_ListView;

		// Token: 0x040059D3 RID: 22995
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003F35")]
		[SerializeField]
		private ScrollViewBase m_ProposeListView;

		// Token: 0x040059D4 RID: 22996
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003F36")]
		[SerializeField]
		private ScrollViewBase m_PendingListView;

		// Token: 0x040059D5 RID: 22997
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003F37")]
		[SerializeField]
		private HomeTransitButton m_PendingIcon;

		// Token: 0x040059D6 RID: 22998
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003F38")]
		[SerializeField]
		private InputField m_FindInput;

		// Token: 0x040059D7 RID: 22999
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003F39")]
		[SerializeField]
		private Text m_FriendNum;

		// Token: 0x040059D8 RID: 23000
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003F3A")]
		[SerializeField]
		private GameObject[] m_TabSelWindow;

		// Token: 0x040059D9 RID: 23001
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003F3B")]
		private long m_FindPlayerID;

		// Token: 0x040059DA RID: 23002
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003F3C")]
		[SerializeField]
		private PrefabCloner m_FindResultCloner;

		// Token: 0x040059DB RID: 23003
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003F3D")]
		[SerializeField]
		private Text m_UserCode;

		// Token: 0x040059DC RID: 23004
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003F3E")]
		private bool m_Sort;

		// Token: 0x040059DD RID: 23005
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003F3F")]
		private MenuFriendListItem m_FindPlayerData;

		// Token: 0x040059DE RID: 23006
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003F40")]
		private MenuFriendUI.eStep m_Step;

		// Token: 0x040059DF RID: 23007
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003F41")]
		private IFriendUIData m_FindPlayer;

		// Token: 0x02000EF0 RID: 3824
		[Token(Token = "0x200119B")]
		private enum eStep
		{
			// Token: 0x040059E2 RID: 23010
			[Token(Token = "0x4006DE8")]
			Hide,
			// Token: 0x040059E3 RID: 23011
			[Token(Token = "0x4006DE9")]
			Wait,
			// Token: 0x040059E4 RID: 23012
			[Token(Token = "0x4006DEA")]
			In,
			// Token: 0x040059E5 RID: 23013
			[Token(Token = "0x4006DEB")]
			Idle,
			// Token: 0x040059E6 RID: 23014
			[Token(Token = "0x4006DEC")]
			Out,
			// Token: 0x040059E7 RID: 23015
			[Token(Token = "0x4006DED")]
			End
		}
	}
}
