﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EEC RID: 3820
	[Token(Token = "0x2000A19")]
	[StructLayout(3)]
	public abstract class MenuFriendUIBase : MenuUIBase
	{
		// Token: 0x060047F5 RID: 18421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004237")]
		[Address(RVA = "0x1014E8644", Offset = "0x14E8644", VA = "0x1014E8644")]
		protected void ClearList(MenuFriendUIBase.eFriendListType type)
		{
		}

		// Token: 0x060047F6 RID: 18422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004238")]
		[Address(RVA = "0x1014E8684", Offset = "0x14E8684", VA = "0x1014E8684")]
		protected void AddList(MenuFriendUIBase.eFriendListType to, IFriendUIData from)
		{
		}

		// Token: 0x060047F7 RID: 18423 RVA: 0x0001A4A8 File Offset: 0x000186A8
		[Token(Token = "0x6004239")]
		[Address(RVA = "0x1014E93CC", Offset = "0x14E93CC", VA = "0x1014E93CC")]
		protected int HowManyListItems(MenuFriendUIBase.eFriendListType type)
		{
			return 0;
		}

		// Token: 0x060047F8 RID: 18424 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600423A")]
		[Address(RVA = "0x1014E9448", Offset = "0x14E9448", VA = "0x1014E9448")]
		protected IFriendUIData GetListItem(MenuFriendUIBase.eFriendListType type, int index)
		{
			return null;
		}

		// Token: 0x060047F9 RID: 18425 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600423B")]
		[Address(RVA = "0x1014E9798", Offset = "0x14E9798", VA = "0x1014E9798")]
		private List<IFriendUIData> GetList(MenuFriendUIBase.eFriendListType type)
		{
			return null;
		}

		// Token: 0x060047FA RID: 18426 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600423C")]
		[Address(RVA = "0x1014E95F0", Offset = "0x14E95F0", VA = "0x1014E95F0")]
		private Wrapper<List<IFriendUIData>> GetListPointer(MenuFriendUIBase.eFriendListType type)
		{
			return null;
		}

		// Token: 0x060047FB RID: 18427 RVA: 0x0001A4C0 File Offset: 0x000186C0
		[Token(Token = "0x600423D")]
		[Address(RVA = "0x1014E96F8", Offset = "0x14E96F8", VA = "0x1014E96F8")]
		private bool Acquire(MenuFriendUIBase.eFriendListType type)
		{
			return default(bool);
		}

		// Token: 0x060047FC RID: 18428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600423E")]
		[Address(RVA = "0x1014E95E8", Offset = "0x14E95E8", VA = "0x1014E95E8")]
		protected MenuFriendUIBase()
		{
		}

		// Token: 0x040059BF RID: 22975
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F2A")]
		private Wrapper<List<IFriendUIData>> m_FriendList;

		// Token: 0x040059C0 RID: 22976
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F2B")]
		private Wrapper<List<IFriendUIData>> m_ProposeFriendList;

		// Token: 0x040059C1 RID: 22977
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F2C")]
		private Wrapper<List<IFriendUIData>> m_PendingFriendList;

		// Token: 0x02000EED RID: 3821
		[Token(Token = "0x2001199")]
		protected enum eFriendListType
		{
			// Token: 0x040059C3 RID: 22979
			[Token(Token = "0x4006DDF")]
			Invalid,
			// Token: 0x040059C4 RID: 22980
			[Token(Token = "0x4006DE0")]
			Friend,
			// Token: 0x040059C5 RID: 22981
			[Token(Token = "0x4006DE1")]
			Propose,
			// Token: 0x040059C6 RID: 22982
			[Token(Token = "0x4006DE2")]
			Pending
		}

		// Token: 0x02000EEE RID: 3822
		[Token(Token = "0x200119A")]
		public enum eListSortOrderType
		{
			// Token: 0x040059C8 RID: 22984
			[Token(Token = "0x4006DE4")]
			Invalid,
			// Token: 0x040059C9 RID: 22985
			[Token(Token = "0x4006DE5")]
			Asc,
			// Token: 0x040059CA RID: 22986
			[Token(Token = "0x4006DE6")]
			Desc
		}
	}
}
