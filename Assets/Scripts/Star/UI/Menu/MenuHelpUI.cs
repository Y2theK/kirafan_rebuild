﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EF1 RID: 3825
	[Token(Token = "0x2000A1B")]
	[StructLayout(3)]
	public class MenuHelpUI : MenuUIBase
	{
		// Token: 0x06004812 RID: 18450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004254")]
		[Address(RVA = "0x1014E97DC", Offset = "0x14E97DC", VA = "0x1014E97DC")]
		public void Setup()
		{
		}

		// Token: 0x06004813 RID: 18451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004255")]
		[Address(RVA = "0x1014E9804", Offset = "0x14E9804", VA = "0x1014E9804")]
		private void CallbackBackButton(bool flg)
		{
		}

		// Token: 0x06004814 RID: 18452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004256")]
		[Address(RVA = "0x1014E981C", Offset = "0x14E981C", VA = "0x1014E981C")]
		private void Update()
		{
		}

		// Token: 0x06004815 RID: 18453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004257")]
		[Address(RVA = "0x1014E99D0", Offset = "0x14E99D0", VA = "0x1014E99D0")]
		private void ChangeStep(MenuHelpUI.eStep step)
		{
		}

		// Token: 0x06004816 RID: 18454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004258")]
		[Address(RVA = "0x1014E9AD0", Offset = "0x14E9AD0", VA = "0x1014E9AD0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004817 RID: 18455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004259")]
		[Address(RVA = "0x1014E9BA0", Offset = "0x14E9BA0", VA = "0x1014E9BA0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004818 RID: 18456 RVA: 0x0001A4F0 File Offset: 0x000186F0
		[Token(Token = "0x600425A")]
		[Address(RVA = "0x1014E9C5C", Offset = "0x14E9C5C", VA = "0x1014E9C5C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004819 RID: 18457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600425B")]
		[Address(RVA = "0x1014E9810", Offset = "0x14E9810", VA = "0x1014E9810")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x0600481A RID: 18458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600425C")]
		[Address(RVA = "0x1014E9AC0", Offset = "0x14E9AC0", VA = "0x1014E9AC0")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x0600481B RID: 18459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600425D")]
		[Address(RVA = "0x1014E9C6C", Offset = "0x14E9C6C", VA = "0x1014E9C6C")]
		public MenuHelpUI()
		{
		}

		// Token: 0x040059E8 RID: 23016
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F43")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040059E9 RID: 23017
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F44")]
		[SerializeField]
		private WebViewGroup m_WebViewGroup;

		// Token: 0x040059EA RID: 23018
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F45")]
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x040059EB RID: 23019
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F46")]
		private MenuHelpUI.eStep m_Step;

		// Token: 0x02000EF2 RID: 3826
		[Token(Token = "0x200119C")]
		private enum eStep
		{
			// Token: 0x040059ED RID: 23021
			[Token(Token = "0x4006DEF")]
			Hide,
			// Token: 0x040059EE RID: 23022
			[Token(Token = "0x4006DF0")]
			In,
			// Token: 0x040059EF RID: 23023
			[Token(Token = "0x4006DF1")]
			Idle,
			// Token: 0x040059F0 RID: 23024
			[Token(Token = "0x4006DF2")]
			Out,
			// Token: 0x040059F1 RID: 23025
			[Token(Token = "0x4006DF3")]
			End
		}
	}
}
