﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EF5 RID: 3829
	[Token(Token = "0x2000A1E")]
	[StructLayout(3)]
	public class MenuLibraryADVCharaGroup : UIGroup
	{
		// Token: 0x140000BC RID: 188
		// (add) Token: 0x06004821 RID: 18465 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004822 RID: 18466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BC")]
		public event Action<eCharaNamedType> OnClickNamed
		{
			[Token(Token = "0x6004263")]
			[Address(RVA = "0x1014E9C74", Offset = "0x14E9C74", VA = "0x1014E9C74")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004264")]
			[Address(RVA = "0x1014E9D80", Offset = "0x14E9D80", VA = "0x1014E9D80")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004823 RID: 18467 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004265")]
		[Address(RVA = "0x1014E9E8C", Offset = "0x14E9E8C", VA = "0x1014E9E8C")]
		public void Setup()
		{
		}

		// Token: 0x06004824 RID: 18468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004266")]
		[Address(RVA = "0x1014E9EC0", Offset = "0x14E9EC0", VA = "0x1014E9EC0")]
		public void ListupTitleChara(eTitleType title)
		{
		}

		// Token: 0x06004825 RID: 18469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004267")]
		[Address(RVA = "0x1014EA4B4", Offset = "0x14EA4B4", VA = "0x1014EA4B4")]
		private void OnClickNamedCallBack(eCharaNamedType named)
		{
		}

		// Token: 0x06004826 RID: 18470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004268")]
		[Address(RVA = "0x1014EA514", Offset = "0x14EA514", VA = "0x1014EA514")]
		public MenuLibraryADVCharaGroup()
		{
		}

		// Token: 0x040059F7 RID: 23031
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F4C")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
