﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000EF6 RID: 3830
	[Token(Token = "0x2000A1F")]
	[StructLayout(3)]
	public class MenuLibraryADVCharaItem : ScrollItemIcon
	{
		// Token: 0x06004827 RID: 18471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004269")]
		[Address(RVA = "0x1014EA51C", Offset = "0x14EA51C", VA = "0x1014EA51C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004828 RID: 18472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600426A")]
		[Address(RVA = "0x1014EA698", Offset = "0x14EA698", VA = "0x1014EA698", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004829 RID: 18473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600426B")]
		[Address(RVA = "0x1014EA6D8", Offset = "0x14EA6D8", VA = "0x1014EA6D8")]
		public MenuLibraryADVCharaItem()
		{
		}

		// Token: 0x040059F9 RID: 23033
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F4E")]
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x040059FA RID: 23034
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F4F")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x040059FB RID: 23035
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F50")]
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
