﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EF7 RID: 3831
	[Token(Token = "0x2000A20")]
	[StructLayout(3)]
	public class MenuLibraryADVCharaItemData : ScrollItemData
	{
		// Token: 0x140000BD RID: 189
		// (add) Token: 0x0600482A RID: 18474 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600482B RID: 18475 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BD")]
		public event Action<eCharaNamedType> OnClickCallBack
		{
			[Token(Token = "0x600426C")]
			[Address(RVA = "0x1014EA3A8", Offset = "0x14EA3A8", VA = "0x1014EA3A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600426D")]
			[Address(RVA = "0x1014EA6E0", Offset = "0x14EA6E0", VA = "0x1014EA6E0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600482C RID: 18476 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600426E")]
		[Address(RVA = "0x1014EA0B0", Offset = "0x14EA0B0", VA = "0x1014EA0B0")]
		public MenuLibraryADVCharaItemData(eCharaNamedType named)
		{
		}

		// Token: 0x0600482D RID: 18477 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600426F")]
		[Address(RVA = "0x1014EA7EC", Offset = "0x14EA7EC", VA = "0x1014EA7EC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040059FC RID: 23036
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F51")]
		public string m_Title;

		// Token: 0x040059FD RID: 23037
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003F52")]
		public eCharaNamedType m_Named;

		// Token: 0x040059FE RID: 23038
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003F53")]
		public int m_CharaID;

		// Token: 0x040059FF RID: 23039
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003F54")]
		public bool m_IsNew;
	}
}
