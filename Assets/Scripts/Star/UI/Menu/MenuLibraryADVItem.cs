﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000EF8 RID: 3832
	[Token(Token = "0x2000A21")]
	[StructLayout(3)]
	public class MenuLibraryADVItem : ScrollItemIcon
	{
		// Token: 0x0600482E RID: 18478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004270")]
		[Address(RVA = "0x1014EA840", Offset = "0x14EA840", VA = "0x1014EA840", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600482F RID: 18479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004271")]
		[Address(RVA = "0x1014EAA18", Offset = "0x14EAA18", VA = "0x1014EAA18")]
		public MenuLibraryADVItem()
		{
		}

		// Token: 0x04005A01 RID: 23041
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F56")]
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04005A02 RID: 23042
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F57")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04005A03 RID: 23043
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F58")]
		[SerializeField]
		private GameObject m_NewIcon;

		// Token: 0x04005A04 RID: 23044
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003F59")]
		[SerializeField]
		private GameObject m_ComicIcon;
	}
}
