﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EF9 RID: 3833
	[Token(Token = "0x2000A22")]
	[StructLayout(3)]
	public class MenuLibraryADVItemData : ScrollItemData
	{
		// Token: 0x140000BE RID: 190
		// (add) Token: 0x06004830 RID: 18480 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004831 RID: 18481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BE")]
		public event Action<int> OnClickCallBack
		{
			[Token(Token = "0x6004272")]
			[Address(RVA = "0x1014EAA20", Offset = "0x14EAA20", VA = "0x1014EAA20")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004273")]
			[Address(RVA = "0x1014EAB2C", Offset = "0x14EAB2C", VA = "0x1014EAB2C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004832 RID: 18482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004274")]
		[Address(RVA = "0x1014EAC38", Offset = "0x14EAC38", VA = "0x1014EAC38")]
		public MenuLibraryADVItemData(int advID)
		{
		}

		// Token: 0x06004833 RID: 18483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004275")]
		[Address(RVA = "0x1014EAE60", Offset = "0x14EAE60", VA = "0x1014EAE60", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005A05 RID: 23045
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F5A")]
		public string m_Title;

		// Token: 0x04005A06 RID: 23046
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003F5B")]
		public int m_ADVID;

		// Token: 0x04005A07 RID: 23047
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003F5C")]
		public string m_LockText;

		// Token: 0x04005A08 RID: 23048
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003F5D")]
		public bool m_IsNew;

		// Token: 0x04005A09 RID: 23049
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003F5E")]
		public bool m_IsComic;
	}
}
