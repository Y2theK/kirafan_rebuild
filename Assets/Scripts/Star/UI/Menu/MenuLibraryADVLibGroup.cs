﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EFA RID: 3834
	[Token(Token = "0x2000A23")]
	[StructLayout(3)]
	public class MenuLibraryADVLibGroup : UIGroup
	{
		// Token: 0x140000BF RID: 191
		// (add) Token: 0x06004834 RID: 18484 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004835 RID: 18485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000BF")]
		public event Action<int> OnClickADVLib
		{
			[Token(Token = "0x6004276")]
			[Address(RVA = "0x1014EAEB4", Offset = "0x14EAEB4", VA = "0x1014EAEB4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004277")]
			[Address(RVA = "0x1014EAFC0", Offset = "0x14EAFC0", VA = "0x1014EAFC0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004836 RID: 18486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004278")]
		[Address(RVA = "0x1014EB0CC", Offset = "0x14EB0CC", VA = "0x1014EB0CC")]
		public void Setup()
		{
		}

		// Token: 0x06004837 RID: 18487 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004279")]
		[Address(RVA = "0x1014EB100", Offset = "0x14EB100", VA = "0x1014EB100")]
		public void Open(eADVLibraryCategory category)
		{
		}

		// Token: 0x06004838 RID: 18488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600427A")]
		[Address(RVA = "0x1014EB440", Offset = "0x14EB440", VA = "0x1014EB440")]
		public void OpenByPart(eADVLibraryCategory category, int part)
		{
		}

		// Token: 0x06004839 RID: 18489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600427B")]
		[Address(RVA = "0x1014EB5C0", Offset = "0x14EB5C0", VA = "0x1014EB5C0")]
		private void OnClickADVLibCallBack(int advLibID)
		{
		}

		// Token: 0x0600483A RID: 18490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600427C")]
		[Address(RVA = "0x1014EB620", Offset = "0x14EB620", VA = "0x1014EB620")]
		public MenuLibraryADVLibGroup()
		{
		}

		// Token: 0x04005A0B RID: 23051
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F60")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
