﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EFB RID: 3835
	[Token(Token = "0x2000A24")]
	[StructLayout(3)]
	public class MenuLibraryADVLibItem : ScrollItemIcon
	{
		// Token: 0x0600483B RID: 18491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600427D")]
		[Address(RVA = "0x1014EB628", Offset = "0x14EB628", VA = "0x1014EB628", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600483C RID: 18492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600427E")]
		[Address(RVA = "0x1014EB730", Offset = "0x14EB730", VA = "0x1014EB730")]
		public MenuLibraryADVLibItem()
		{
		}

		// Token: 0x04005A0D RID: 23053
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F62")]
		[SerializeField]
		private ADVLibraryIcon m_Icon;
	}
}
