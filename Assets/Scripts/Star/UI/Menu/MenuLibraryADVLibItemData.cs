﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000EFC RID: 3836
	[Token(Token = "0x2000A25")]
	[StructLayout(3)]
	public class MenuLibraryADVLibItemData : ScrollItemData
	{
		// Token: 0x140000C0 RID: 192
		// (add) Token: 0x0600483D RID: 18493 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600483E RID: 18494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C0")]
		public event Action<int> OnClickCallBack
		{
			[Token(Token = "0x600427F")]
			[Address(RVA = "0x1014EB334", Offset = "0x14EB334", VA = "0x1014EB334")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004280")]
			[Address(RVA = "0x1014EB738", Offset = "0x14EB738", VA = "0x1014EB738")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600483F RID: 18495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004281")]
		[Address(RVA = "0x1014EB278", Offset = "0x14EB278", VA = "0x1014EB278")]
		public MenuLibraryADVLibItemData(int advLibID)
		{
		}

		// Token: 0x06004840 RID: 18496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004282")]
		[Address(RVA = "0x1014EB844", Offset = "0x14EB844", VA = "0x1014EB844", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005A0E RID: 23054
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F63")]
		public string m_Title;

		// Token: 0x04005A0F RID: 23055
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003F64")]
		public int m_ADVLibID;
	}
}
