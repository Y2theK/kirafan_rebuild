﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EFD RID: 3837
	[Token(Token = "0x2000A26")]
	[StructLayout(3)]
	public class MenuLibraryADVListGroup : UIGroup
	{
		// Token: 0x140000C1 RID: 193
		// (add) Token: 0x06004841 RID: 18497 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004842 RID: 18498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C1")]
		public event Action<int> OnClickADV
		{
			[Token(Token = "0x6004283")]
			[Address(RVA = "0x1014EB898", Offset = "0x14EB898", VA = "0x1014EB898")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004284")]
			[Address(RVA = "0x1014EB9A4", Offset = "0x14EB9A4", VA = "0x1014EB9A4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004843 RID: 18499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004285")]
		[Address(RVA = "0x1014EBAB0", Offset = "0x14EBAB0", VA = "0x1014EBAB0")]
		public void Setup()
		{
		}

		// Token: 0x06004844 RID: 18500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004286")]
		[Address(RVA = "0x1014EBAE4", Offset = "0x14EBAE4", VA = "0x1014EBAE4")]
		public void Open(List<int> advList, int advid = -1)
		{
		}

		// Token: 0x06004845 RID: 18501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004287")]
		[Address(RVA = "0x1014EBCC0", Offset = "0x14EBCC0", VA = "0x1014EBCC0")]
		private void OnClickADVCallBack(int advID)
		{
		}

		// Token: 0x06004846 RID: 18502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004288")]
		[Address(RVA = "0x1014EBD20", Offset = "0x14EBD20", VA = "0x1014EBD20")]
		public MenuLibraryADVListGroup()
		{
		}

		// Token: 0x04005A11 RID: 23057
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F66")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
