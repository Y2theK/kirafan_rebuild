﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EFE RID: 3838
	[Token(Token = "0x2000A27")]
	[StructLayout(3)]
	public class MenuLibraryADVPartLibGroup : UIGroup
	{
		// Token: 0x140000C2 RID: 194
		// (add) Token: 0x06004847 RID: 18503 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004848 RID: 18504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C2")]
		public event Action<int> OnClickADVLib
		{
			[Token(Token = "0x6004289")]
			[Address(RVA = "0x1014EBD28", Offset = "0x14EBD28", VA = "0x1014EBD28")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600428A")]
			[Address(RVA = "0x1014EBE34", Offset = "0x14EBE34", VA = "0x1014EBE34")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004849 RID: 18505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600428B")]
		[Address(RVA = "0x1014EBF40", Offset = "0x14EBF40", VA = "0x1014EBF40")]
		public void Setup()
		{
		}

		// Token: 0x0600484A RID: 18506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600428C")]
		[Address(RVA = "0x1014EBF74", Offset = "0x14EBF74", VA = "0x1014EBF74", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x0600484B RID: 18507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600428D")]
		[Address(RVA = "0x1014EC224", Offset = "0x14EC224", VA = "0x1014EC224")]
		private void OnClickADVLibCallBack(int advLibID)
		{
		}

		// Token: 0x0600484C RID: 18508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600428E")]
		[Address(RVA = "0x1014EC284", Offset = "0x14EC284", VA = "0x1014EC284")]
		public MenuLibraryADVPartLibGroup()
		{
		}

		// Token: 0x04005A13 RID: 23059
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F68")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
