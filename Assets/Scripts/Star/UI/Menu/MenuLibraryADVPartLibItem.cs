﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000EFF RID: 3839
	[Token(Token = "0x2000A28")]
	[StructLayout(3)]
	public class MenuLibraryADVPartLibItem : ScrollItemIcon
	{
		// Token: 0x0600484D RID: 18509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600428F")]
		[Address(RVA = "0x1014EC28C", Offset = "0x14EC28C", VA = "0x1014EC28C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600484E RID: 18510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004290")]
		[Address(RVA = "0x1014EC394", Offset = "0x14EC394", VA = "0x1014EC394")]
		public MenuLibraryADVPartLibItem()
		{
		}

		// Token: 0x04005A15 RID: 23061
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F6A")]
		[SerializeField]
		private QuestPartImage m_Icon;
	}
}
