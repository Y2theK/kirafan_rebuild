﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F00 RID: 3840
	[Token(Token = "0x2000A29")]
	[StructLayout(3)]
	public class MenuLibraryADVPartLibItemData : ScrollItemData
	{
		// Token: 0x140000C3 RID: 195
		// (add) Token: 0x0600484F RID: 18511 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004850 RID: 18512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C3")]
		public event Action<int> OnClickCallBack
		{
			[Token(Token = "0x6004291")]
			[Address(RVA = "0x1014EC118", Offset = "0x14EC118", VA = "0x1014EC118")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004292")]
			[Address(RVA = "0x1014EC39C", Offset = "0x14EC39C", VA = "0x1014EC39C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004851 RID: 18513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004293")]
		[Address(RVA = "0x1014EC0EC", Offset = "0x14EC0EC", VA = "0x1014EC0EC")]
		public MenuLibraryADVPartLibItemData(int part)
		{
		}

		// Token: 0x06004852 RID: 18514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004294")]
		[Address(RVA = "0x1014EC4A8", Offset = "0x14EC4A8", VA = "0x1014EC4A8", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005A16 RID: 23062
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F6B")]
		public int m_part;
	}
}
