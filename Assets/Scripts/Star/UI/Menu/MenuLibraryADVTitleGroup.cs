﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F01 RID: 3841
	[Token(Token = "0x2000A2A")]
	[StructLayout(3)]
	public class MenuLibraryADVTitleGroup : UIGroup
	{
		// Token: 0x140000C4 RID: 196
		// (add) Token: 0x06004853 RID: 18515 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004854 RID: 18516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C4")]
		public event Action<eTitleType> OnClickTitle
		{
			[Token(Token = "0x6004295")]
			[Address(RVA = "0x1014EC4FC", Offset = "0x14EC4FC", VA = "0x1014EC4FC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004296")]
			[Address(RVA = "0x1014EC608", Offset = "0x14EC608", VA = "0x1014EC608")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004855 RID: 18517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004297")]
		[Address(RVA = "0x1014EC714", Offset = "0x14EC714", VA = "0x1014EC714")]
		public void Setup()
		{
		}

		// Token: 0x06004856 RID: 18518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004298")]
		[Address(RVA = "0x1014ECF48", Offset = "0x14ECF48", VA = "0x1014ECF48")]
		public void Open(eTitleType titleType)
		{
		}

		// Token: 0x06004857 RID: 18519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004299")]
		[Address(RVA = "0x1014ED030", Offset = "0x14ED030", VA = "0x1014ED030")]
		private void OnClickADVTitleCallBack(eTitleType titleType)
		{
		}

		// Token: 0x06004858 RID: 18520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600429A")]
		[Address(RVA = "0x1014ED090", Offset = "0x14ED090", VA = "0x1014ED090")]
		public MenuLibraryADVTitleGroup()
		{
		}

		// Token: 0x04005A18 RID: 23064
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F6D")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005A19 RID: 23065
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003F6E")]
		private List<eTitleType> m_TitleTypes;
	}
}
