﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F02 RID: 3842
	[Token(Token = "0x2000A2B")]
	[StructLayout(3)]
	public class MenuLibraryADVTitleItem : ScrollItemIcon
	{
		// Token: 0x06004859 RID: 18521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600429B")]
		[Address(RVA = "0x1014ED100", Offset = "0x14ED100", VA = "0x1014ED100", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600485A RID: 18522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600429C")]
		[Address(RVA = "0x1014ED238", Offset = "0x14ED238", VA = "0x1014ED238")]
		public MenuLibraryADVTitleItem()
		{
		}

		// Token: 0x04005A1B RID: 23067
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F70")]
		[SerializeField]
		private ContentTitleLogo m_Logo;

		// Token: 0x04005A1C RID: 23068
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F71")]
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
