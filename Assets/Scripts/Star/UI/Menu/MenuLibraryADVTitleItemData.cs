﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F03 RID: 3843
	[Token(Token = "0x2000A2C")]
	[StructLayout(3)]
	public class MenuLibraryADVTitleItemData : ScrollItemData
	{
		// Token: 0x140000C5 RID: 197
		// (add) Token: 0x0600485B RID: 18523 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600485C RID: 18524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C5")]
		public event Action<eTitleType> OnClickCallBack
		{
			[Token(Token = "0x600429D")]
			[Address(RVA = "0x1014ECE3C", Offset = "0x14ECE3C", VA = "0x1014ECE3C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600429E")]
			[Address(RVA = "0x1014ED240", Offset = "0x14ED240", VA = "0x1014ED240")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600485D RID: 18525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600429F")]
		[Address(RVA = "0x1014ECA48", Offset = "0x14ECA48", VA = "0x1014ECA48")]
		public MenuLibraryADVTitleItemData(eTitleType titleType)
		{
		}

		// Token: 0x0600485E RID: 18526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A0")]
		[Address(RVA = "0x1014ED34C", Offset = "0x14ED34C", VA = "0x1014ED34C", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005A1D RID: 23069
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F72")]
		public eTitleType m_Title;

		// Token: 0x04005A1E RID: 23070
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003F73")]
		public bool m_IsNew;
	}
}
