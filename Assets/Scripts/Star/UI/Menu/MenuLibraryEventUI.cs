﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.GachaPlay;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F04 RID: 3844
	[Token(Token = "0x2000A2D")]
	[StructLayout(3)]
	public class MenuLibraryEventUI : MenuUIBase
	{
		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x0600485F RID: 18527 RVA: 0x0001A508 File Offset: 0x00018708
		// (set) Token: 0x06004860 RID: 18528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004D2")]
		public int SelectADVID
		{
			[Token(Token = "0x60042A1")]
			[Address(RVA = "0x1014ED3A0", Offset = "0x14ED3A0", VA = "0x1014ED3A0")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x60042A2")]
			[Address(RVA = "0x1014ED3A8", Offset = "0x14ED3A8", VA = "0x1014ED3A8")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x140000C6 RID: 198
		// (add) Token: 0x06004861 RID: 18529 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004862 RID: 18530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C6")]
		public event Action OnTransit
		{
			[Token(Token = "0x60042A3")]
			[Address(RVA = "0x1014ED3B0", Offset = "0x14ED3B0", VA = "0x1014ED3B0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042A4")]
			[Address(RVA = "0x1014ED4BC", Offset = "0x14ED4BC", VA = "0x1014ED4BC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004863 RID: 18531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A5")]
		[Address(RVA = "0x1014ED5C8", Offset = "0x14ED5C8", VA = "0x1014ED5C8")]
		public void Setup(NPCCharaDisplayUI npcUI)
		{
		}

		// Token: 0x06004864 RID: 18532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A6")]
		[Address(RVA = "0x1014EDE18", Offset = "0x14EDE18", VA = "0x1014EDE18")]
		private void Update()
		{
		}

		// Token: 0x06004865 RID: 18533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A7")]
		[Address(RVA = "0x1014EE0B0", Offset = "0x14EE0B0", VA = "0x1014EE0B0")]
		private void ChangeStep(MenuLibraryEventUI.eStep step)
		{
		}

		// Token: 0x06004866 RID: 18534 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A8")]
		[Address(RVA = "0x1014EE1A0", Offset = "0x14EE1A0", VA = "0x1014EE1A0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004867 RID: 18535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042A9")]
		[Address(RVA = "0x1014EE4B0", Offset = "0x14EE4B0", VA = "0x1014EE4B0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004868 RID: 18536 RVA: 0x0001A520 File Offset: 0x00018720
		[Token(Token = "0x60042AA")]
		[Address(RVA = "0x1014EE614", Offset = "0x14EE614", VA = "0x1014EE614", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004869 RID: 18537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042AB")]
		[Address(RVA = "0x1014EE624", Offset = "0x14EE624", VA = "0x1014EE624")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x0600486A RID: 18538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042AC")]
		[Address(RVA = "0x1014EE628", Offset = "0x14EE628", VA = "0x1014EE628")]
		public void OnClickCategoryCallBack(int btn)
		{
		}

		// Token: 0x0600486B RID: 18539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042AD")]
		[Address(RVA = "0x1014EE710", Offset = "0x14EE710", VA = "0x1014EE710")]
		public void OnClickADVTitleCallBack(eTitleType title)
		{
		}

		// Token: 0x0600486C RID: 18540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042AE")]
		[Address(RVA = "0x1014EE798", Offset = "0x14EE798", VA = "0x1014EE798")]
		public void OnClickADVLibCallBack(int advLibID)
		{
		}

		// Token: 0x0600486D RID: 18541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042AF")]
		[Address(RVA = "0x1014EE820", Offset = "0x14EE820", VA = "0x1014EE820")]
		public void OnClickADVPartLibCallBack(int part)
		{
		}

		// Token: 0x0600486E RID: 18542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B0")]
		[Address(RVA = "0x1014EE898", Offset = "0x14EE898", VA = "0x1014EE898")]
		public void OnClickNamedCallBack(eCharaNamedType named)
		{
		}

		// Token: 0x0600486F RID: 18543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B1")]
		[Address(RVA = "0x1014EE9B4", Offset = "0x14EE9B4", VA = "0x1014EE9B4")]
		private void OnResponseOfferGetAll()
		{
		}

		// Token: 0x06004870 RID: 18544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B2")]
		[Address(RVA = "0x1014EF5B0", Offset = "0x14EF5B0", VA = "0x1014EF5B0")]
		public void OnClickADVCallBack(int advID)
		{
		}

		// Token: 0x06004871 RID: 18545 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B3")]
		[Address(RVA = "0x1014EF7A0", Offset = "0x14EF7A0", VA = "0x1014EF7A0")]
		public void OnClickSingleOrCross(int btn)
		{
		}

		// Token: 0x06004872 RID: 18546 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B4")]
		[Address(RVA = "0x1014F026C", Offset = "0x14F026C", VA = "0x1014F026C")]
		public void OnClickSummonCallBack(int charaID)
		{
		}

		// Token: 0x06004873 RID: 18547 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B5")]
		[Address(RVA = "0x1014F040C", Offset = "0x14F040C", VA = "0x1014F040C")]
		public void OnClickSkipFlavor()
		{
		}

		// Token: 0x06004874 RID: 18548 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B6")]
		[Address(RVA = "0x1014F043C", Offset = "0x14F043C", VA = "0x1014F043C")]
		private void OnCloseGachaFlavor()
		{
		}

		// Token: 0x06004875 RID: 18549 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B7")]
		[Address(RVA = "0x1014F0520", Offset = "0x14F0520", VA = "0x1014F0520")]
		private void OnClickFlavorCallBack(eFlavorConditionType conditionType, int conditionId)
		{
		}

		// Token: 0x06004876 RID: 18550 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B8")]
		[Address(RVA = "0x1014F06B0", Offset = "0x14F06B0", VA = "0x1014F06B0")]
		private void OnLoadedFlavor()
		{
		}

		// Token: 0x06004877 RID: 18551 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042B9")]
		[Address(RVA = "0x1014F0788", Offset = "0x14F0788", VA = "0x1014F0788")]
		private void OnFinishFlavor()
		{
		}

		// Token: 0x06004878 RID: 18552 RVA: 0x0001A538 File Offset: 0x00018738
		[Token(Token = "0x60042BA")]
		[Address(RVA = "0x1014F084C", Offset = "0x14F084C", VA = "0x1014F084C")]
		public bool ExecuteBack()
		{
			return default(bool);
		}

		// Token: 0x06004879 RID: 18553 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042BB")]
		[Address(RVA = "0x1014F0C90", Offset = "0x14F0C90", VA = "0x1014F0C90")]
		public MenuLibraryEventUI()
		{
		}

		// Token: 0x04005A20 RID: 23072
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F75")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005A21 RID: 23073
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F76")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005A22 RID: 23074
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F77")]
		[SerializeField]
		private CustomButton m_MainScenarioButton;

		// Token: 0x04005A23 RID: 23075
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F78")]
		[SerializeField]
		private CustomButton m_WriterScenarioButton;

		// Token: 0x04005A24 RID: 23076
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F79")]
		[SerializeField]
		private CustomButton m_EventScenarioButton;

		// Token: 0x04005A25 RID: 23077
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F7A")]
		[SerializeField]
		private Badge m_CharaScenarioBadge;

		// Token: 0x04005A26 RID: 23078
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003F7B")]
		[SerializeField]
		private MenuLibraryADVLibGroup m_ADVLibGroup;

		// Token: 0x04005A27 RID: 23079
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003F7C")]
		[SerializeField]
		private MenuLibraryADVPartLibGroup m_ADVPartLibGroup;

		// Token: 0x04005A28 RID: 23080
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003F7D")]
		[SerializeField]
		private MenuLibraryADVListGroup m_ADVListGroup;

		// Token: 0x04005A29 RID: 23081
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F7E")]
		[SerializeField]
		private MenuLibraryADVTitleGroup m_TitleGroup;

		// Token: 0x04005A2A RID: 23082
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003F7F")]
		[SerializeField]
		private MenuLibraryADVCharaGroup m_CharaGroup;

		// Token: 0x04005A2B RID: 23083
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003F80")]
		[SerializeField]
		private MenuLibrarySingleOrCrossGroup m_SingleOrCrossGroup;

		// Token: 0x04005A2C RID: 23084
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003F81")]
		[SerializeField]
		private MenuLibrarySummonGroup m_SummonGroup;

		// Token: 0x04005A2D RID: 23085
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003F82")]
		[SerializeField]
		private MenuLibraryFlavorGroup m_FlavorGroup;

		// Token: 0x04005A2E RID: 23086
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003F83")]
		[SerializeField]
		private GachaCharaFlavor m_GachaFlavor;

		// Token: 0x04005A2F RID: 23087
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003F84")]
		[SerializeField]
		private GameObject m_FlavorSkipButtonObj;

		// Token: 0x04005A30 RID: 23088
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003F85")]
		private UIGroup[] m_UIGroups;

		// Token: 0x04005A31 RID: 23089
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003F86")]
		private int m_ADVLibID;

		// Token: 0x04005A32 RID: 23090
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4003F87")]
		private int m_ADVPartLibID;

		// Token: 0x04005A33 RID: 23091
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003F88")]
		private eCharaNamedType m_NamedType;

		// Token: 0x04005A34 RID: 23092
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x4003F89")]
		private GameGlobalParameter.eCharaADVType m_CharaADVType;

		// Token: 0x04005A35 RID: 23093
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003F8A")]
		private MenuLibraryEventUI.eStep m_Step;

		// Token: 0x04005A36 RID: 23094
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x4003F8B")]
		private MenuLibraryEventUI.eState m_CurrentState;

		// Token: 0x04005A39 RID: 23097
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003F8E")]
		private NPCCharaDisplayUI m_NpcUI;

		// Token: 0x04005A3A RID: 23098
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003F8F")]
		private FlavorController m_FlavorController;

		// Token: 0x02000F05 RID: 3845
		[Token(Token = "0x200119D")]
		private enum eStep
		{
			// Token: 0x04005A3C RID: 23100
			[Token(Token = "0x4006DF5")]
			Hide,
			// Token: 0x04005A3D RID: 23101
			[Token(Token = "0x4006DF6")]
			In,
			// Token: 0x04005A3E RID: 23102
			[Token(Token = "0x4006DF7")]
			Idle,
			// Token: 0x04005A3F RID: 23103
			[Token(Token = "0x4006DF8")]
			Out,
			// Token: 0x04005A40 RID: 23104
			[Token(Token = "0x4006DF9")]
			End
		}

		// Token: 0x02000F06 RID: 3846
		[Token(Token = "0x200119E")]
		public enum eState
		{
			// Token: 0x04005A42 RID: 23106
			[Token(Token = "0x4006DFB")]
			Main,
			// Token: 0x04005A43 RID: 23107
			[Token(Token = "0x4006DFC")]
			ADVPart,
			// Token: 0x04005A44 RID: 23108
			[Token(Token = "0x4006DFD")]
			ADVLib,
			// Token: 0x04005A45 RID: 23109
			[Token(Token = "0x4006DFE")]
			ADV,
			// Token: 0x04005A46 RID: 23110
			[Token(Token = "0x4006DFF")]
			Title,
			// Token: 0x04005A47 RID: 23111
			[Token(Token = "0x4006E00")]
			Chara,
			// Token: 0x04005A48 RID: 23112
			[Token(Token = "0x4006E01")]
			SingleOrCross,
			// Token: 0x04005A49 RID: 23113
			[Token(Token = "0x4006E02")]
			Summon,
			// Token: 0x04005A4A RID: 23114
			[Token(Token = "0x4006E03")]
			PlayingGachaFlavor,
			// Token: 0x04005A4B RID: 23115
			[Token(Token = "0x4006E04")]
			FlavorSelect,
			// Token: 0x04005A4C RID: 23116
			[Token(Token = "0x4006E05")]
			PlayingFlavor
		}
	}
}
