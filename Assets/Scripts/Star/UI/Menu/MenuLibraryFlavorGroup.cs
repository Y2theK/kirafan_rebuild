﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F07 RID: 3847
	[Token(Token = "0x2000A2E")]
	[StructLayout(3)]
	public class MenuLibraryFlavorGroup : UIGroup
	{
		// Token: 0x140000C7 RID: 199
		// (add) Token: 0x0600487A RID: 18554 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600487B RID: 18555 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C7")]
		public event Action<eFlavorConditionType, int> OnClick
		{
			[Token(Token = "0x60042BC")]
			[Address(RVA = "0x1014EDD0C", Offset = "0x14EDD0C", VA = "0x1014EDD0C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042BD")]
			[Address(RVA = "0x1014F0CA8", Offset = "0x14F0CA8", VA = "0x1014F0CA8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600487C RID: 18556 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042BE")]
		[Address(RVA = "0x1014EDCD8", Offset = "0x14EDCD8", VA = "0x1014EDCD8")]
		public void Setup()
		{
		}

		// Token: 0x0600487D RID: 18557 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042BF")]
		[Address(RVA = "0x1014EFC54", Offset = "0x14EFC54", VA = "0x1014EFC54")]
		public void Open(eCharaNamedType named)
		{
		}

		// Token: 0x0600487E RID: 18558 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C0")]
		[Address(RVA = "0x1014F0DF8", Offset = "0x14F0DF8", VA = "0x1014F0DF8")]
		private void OnClickCallBack(eFlavorConditionType conditionType, int conditionId)
		{
		}

		// Token: 0x0600487F RID: 18559 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C1")]
		[Address(RVA = "0x1014F0E60", Offset = "0x14F0E60", VA = "0x1014F0E60")]
		public MenuLibraryFlavorGroup()
		{
		}

		// Token: 0x04005A4D RID: 23117
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003F90")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
