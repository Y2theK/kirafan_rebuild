﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F08 RID: 3848
	[Token(Token = "0x2000A2F")]
	[StructLayout(3)]
	public class MenuLibraryFlavorItem : ScrollItemIcon
	{
		// Token: 0x06004880 RID: 18560 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C2")]
		[Address(RVA = "0x1014F0E68", Offset = "0x14F0E68", VA = "0x1014F0E68", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004881 RID: 18561 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C3")]
		[Address(RVA = "0x1014F0F78", Offset = "0x14F0F78", VA = "0x1014F0F78")]
		public MenuLibraryFlavorItem()
		{
		}

		// Token: 0x04005A4F RID: 23119
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F92")]
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x02000F09 RID: 3849
		[Token(Token = "0x200119F")]
		public class MenuLibraryFlavorItemData : ScrollItemData
		{
			// Token: 0x06004882 RID: 18562 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006271")]
			[Address(RVA = "0x1014F0DB4", Offset = "0x14F0DB4", VA = "0x1014F0DB4")]
			public MenuLibraryFlavorItemData(FlavorsDB_Param param, string offerTitle)
			{
			}

			// Token: 0x06004883 RID: 18563 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006272")]
			[Address(RVA = "0x1014F0F80", Offset = "0x14F0F80", VA = "0x1014F0F80", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x04005A50 RID: 23120
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006E06")]
			public FlavorsDB_Param m_FlavorParam;

			// Token: 0x04005A51 RID: 23121
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006E07")]
			public string m_Title;

			// Token: 0x04005A52 RID: 23122
			[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
			[Token(Token = "0x4006E08")]
			public Action<eFlavorConditionType, int> OnClickCallBack;
		}
	}
}
