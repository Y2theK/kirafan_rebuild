﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F0A RID: 3850
	[Token(Token = "0x2000A30")]
	[StructLayout(3)]
	public class MenuLibraryOPMovieUI : MenuUIBase
	{
		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x06004884 RID: 18564 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004D3")]
		public string MovieFileName
		{
			[Token(Token = "0x60042C4")]
			[Address(RVA = "0x1014F0FD4", Offset = "0x14F0FD4", VA = "0x1014F0FD4")]
			get
			{
				return null;
			}
		}

		// Token: 0x140000C8 RID: 200
		// (add) Token: 0x06004885 RID: 18565 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004886 RID: 18566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C8")]
		public event Action OnClickButton
		{
			[Token(Token = "0x60042C5")]
			[Address(RVA = "0x1014F0FDC", Offset = "0x14F0FDC", VA = "0x1014F0FDC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042C6")]
			[Address(RVA = "0x1014F10E8", Offset = "0x14F10E8", VA = "0x1014F10E8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004887 RID: 18567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C7")]
		[Address(RVA = "0x1014F11F4", Offset = "0x14F11F4", VA = "0x1014F11F4")]
		public void Setup()
		{
		}

		// Token: 0x06004888 RID: 18568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C8")]
		[Address(RVA = "0x1014F141C", Offset = "0x14F141C", VA = "0x1014F141C")]
		private void Update()
		{
		}

		// Token: 0x06004889 RID: 18569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042C9")]
		[Address(RVA = "0x1014F15AC", Offset = "0x14F15AC", VA = "0x1014F15AC")]
		private void ChangeStep(MenuLibraryOPMovieUI.eStep step)
		{
		}

		// Token: 0x0600488A RID: 18570 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042CA")]
		[Address(RVA = "0x1014F169C", Offset = "0x14F169C", VA = "0x1014F169C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600488B RID: 18571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042CB")]
		[Address(RVA = "0x1014F1758", Offset = "0x14F1758", VA = "0x1014F1758", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600488C RID: 18572 RVA: 0x0001A550 File Offset: 0x00018750
		[Token(Token = "0x60042CC")]
		[Address(RVA = "0x1014F1814", Offset = "0x14F1814", VA = "0x1014F1814", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600488D RID: 18573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042CD")]
		[Address(RVA = "0x1014F1824", Offset = "0x14F1824", VA = "0x1014F1824")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x0600488E RID: 18574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042CE")]
		[Address(RVA = "0x1014F1830", Offset = "0x14F1830", VA = "0x1014F1830")]
		public void OnSelectButtonCallBack(string fileName)
		{
		}

		// Token: 0x0600488F RID: 18575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042CF")]
		[Address(RVA = "0x1014F1840", Offset = "0x14F1840", VA = "0x1014F1840")]
		public MenuLibraryOPMovieUI()
		{
		}

		// Token: 0x04005A53 RID: 23123
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F93")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005A54 RID: 23124
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F94")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005A55 RID: 23125
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F95")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005A56 RID: 23126
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F96")]
		private MenuLibraryOPMovieUI.eStep m_Step;

		// Token: 0x04005A57 RID: 23127
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F97")]
		private string m_movieFileName;

		// Token: 0x02000F0B RID: 3851
		[Token(Token = "0x20011A0")]
		private enum eStep
		{
			// Token: 0x04005A5A RID: 23130
			[Token(Token = "0x4006E0A")]
			Hide,
			// Token: 0x04005A5B RID: 23131
			[Token(Token = "0x4006E0B")]
			In,
			// Token: 0x04005A5C RID: 23132
			[Token(Token = "0x4006E0C")]
			Idle,
			// Token: 0x04005A5D RID: 23133
			[Token(Token = "0x4006E0D")]
			Out,
			// Token: 0x04005A5E RID: 23134
			[Token(Token = "0x4006E0E")]
			End
		}
	}
}
