﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F0C RID: 3852
	[Token(Token = "0x2000A31")]
	[StructLayout(3)]
	public class MenuLibraryOriginalCharaUI : MenuUIBase
	{
		// Token: 0x06004890 RID: 18576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D0")]
		[Address(RVA = "0x1014F1848", Offset = "0x14F1848", VA = "0x1014F1848")]
		public void Setup()
		{
		}

		// Token: 0x06004891 RID: 18577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D1")]
		[Address(RVA = "0x1014F1EF0", Offset = "0x14F1EF0", VA = "0x1014F1EF0", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004892 RID: 18578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D2")]
		[Address(RVA = "0x1014F2018", Offset = "0x14F2018", VA = "0x1014F2018")]
		private void Update()
		{
		}

		// Token: 0x06004893 RID: 18579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D3")]
		[Address(RVA = "0x1014F233C", Offset = "0x14F233C", VA = "0x1014F233C")]
		private void ChangeStep(MenuLibraryOriginalCharaUI.eStep step)
		{
		}

		// Token: 0x06004894 RID: 18580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D4")]
		[Address(RVA = "0x1014F242C", Offset = "0x14F242C", VA = "0x1014F242C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004895 RID: 18581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D5")]
		[Address(RVA = "0x1014F24E8", Offset = "0x14F24E8", VA = "0x1014F24E8", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004896 RID: 18582 RVA: 0x0001A568 File Offset: 0x00018768
		[Token(Token = "0x60042D6")]
		[Address(RVA = "0x1014F25A4", Offset = "0x14F25A4", VA = "0x1014F25A4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004897 RID: 18583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D7")]
		[Address(RVA = "0x1014F25B4", Offset = "0x14F25B4", VA = "0x1014F25B4")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004898 RID: 18584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D8")]
		[Address(RVA = "0x1014F1BB8", Offset = "0x14F1BB8", VA = "0x1014F1BB8")]
		private void OnClickWordButtonCallBack(int idx)
		{
		}

		// Token: 0x06004899 RID: 18585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042D9")]
		[Address(RVA = "0x1014F25C0", Offset = "0x14F25C0", VA = "0x1014F25C0")]
		public void OnClickIllustButtonCallBack()
		{
		}

		// Token: 0x0600489A RID: 18586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042DA")]
		[Address(RVA = "0x1014F25F4", Offset = "0x14F25F4", VA = "0x1014F25F4")]
		public MenuLibraryOriginalCharaUI()
		{
		}

		// Token: 0x04005A5F RID: 23135
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003F99")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005A60 RID: 23136
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003F9A")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005A61 RID: 23137
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003F9B")]
		[SerializeField]
		private UIGroup m_IllustGroup;

		// Token: 0x04005A62 RID: 23138
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003F9C")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005A63 RID: 23139
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003F9D")]
		[SerializeField]
		private Image m_Icon;

		// Token: 0x04005A64 RID: 23140
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003F9E")]
		[SerializeField]
		private Image m_Illust;

		// Token: 0x04005A65 RID: 23141
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003F9F")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04005A66 RID: 23142
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003FA0")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005A67 RID: 23143
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003FA1")]
		[SerializeField]
		private Text m_IllustratorText;

		// Token: 0x04005A68 RID: 23144
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FA2")]
		[SerializeField]
		private Text m_CVText;

		// Token: 0x04005A69 RID: 23145
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003FA3")]
		private List<ITitleUIData> m_DataList;

		// Token: 0x04005A6A RID: 23146
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003FA4")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04005A6B RID: 23147
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003FA5")]
		private SpriteHandler m_SpriteHandlerIllust;

		// Token: 0x04005A6C RID: 23148
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003FA6")]
		private int m_CurrentIdx;

		// Token: 0x04005A6D RID: 23149
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4003FA7")]
		private MenuLibraryOriginalCharaUI.eStep m_Step;

		// Token: 0x02000F0D RID: 3853
		[Token(Token = "0x20011A1")]
		private enum eStep
		{
			// Token: 0x04005A6F RID: 23151
			[Token(Token = "0x4006E10")]
			Hide,
			// Token: 0x04005A70 RID: 23152
			[Token(Token = "0x4006E11")]
			In,
			// Token: 0x04005A71 RID: 23153
			[Token(Token = "0x4006E12")]
			Idle,
			// Token: 0x04005A72 RID: 23154
			[Token(Token = "0x4006E13")]
			Out,
			// Token: 0x04005A73 RID: 23155
			[Token(Token = "0x4006E14")]
			End
		}
	}
}
