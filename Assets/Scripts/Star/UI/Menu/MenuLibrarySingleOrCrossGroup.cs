﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F0E RID: 3854
	[Token(Token = "0x2000A32")]
	[StructLayout(3)]
	public class MenuLibrarySingleOrCrossGroup : UIGroup
	{
		// Token: 0x0600489B RID: 18587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042DB")]
		[Address(RVA = "0x1014EEA14", Offset = "0x14EEA14", VA = "0x1014EEA14")]
		public void Open(eCharaNamedType named)
		{
		}

		// Token: 0x0600489C RID: 18588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042DC")]
		[Address(RVA = "0x1014F266C", Offset = "0x14F266C", VA = "0x1014F266C")]
		public MenuLibrarySingleOrCrossGroup()
		{
		}

		// Token: 0x04005A74 RID: 23156
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FA8")]
		[SerializeField]
		private CustomButton m_SummonButton;

		// Token: 0x04005A75 RID: 23157
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003FA9")]
		[SerializeField]
		private CustomButton m_SingleButton;

		// Token: 0x04005A76 RID: 23158
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003FAA")]
		[SerializeField]
		private CustomButton m_CrossButton;

		// Token: 0x04005A77 RID: 23159
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003FAB")]
		[SerializeField]
		private CustomButton m_WeaponButton;

		// Token: 0x04005A78 RID: 23160
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003FAC")]
		[SerializeField]
		private CustomButton m_OfferButton;

		// Token: 0x04005A79 RID: 23161
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003FAD")]
		[SerializeField]
		private Text m_SingleText;

		// Token: 0x04005A7A RID: 23162
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003FAE")]
		[SerializeField]
		private Text m_CrossText;

		// Token: 0x04005A7B RID: 23163
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003FAF")]
		[SerializeField]
		private Text m_WeaponText;

		// Token: 0x04005A7C RID: 23164
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003FB0")]
		[SerializeField]
		private GameObject m_SingleNew;

		// Token: 0x04005A7D RID: 23165
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003FB1")]
		[SerializeField]
		private GameObject m_CrossNew;

		// Token: 0x04005A7E RID: 23166
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003FB2")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005A7F RID: 23167
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003FB3")]
		[SerializeField]
		private BustImage m_BustImage;
	}
}
