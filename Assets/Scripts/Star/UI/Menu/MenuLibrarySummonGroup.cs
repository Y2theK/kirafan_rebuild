﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F0F RID: 3855
	[Token(Token = "0x2000A33")]
	[StructLayout(3)]
	public class MenuLibrarySummonGroup : UIGroup
	{
		// Token: 0x140000C9 RID: 201
		// (add) Token: 0x0600489D RID: 18589 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600489E RID: 18590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000C9")]
		public event Action<int> OnClickSummon
		{
			[Token(Token = "0x60042DD")]
			[Address(RVA = "0x1014EDBCC", Offset = "0x14EDBCC", VA = "0x1014EDBCC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042DE")]
			[Address(RVA = "0x1014F2674", Offset = "0x14F2674", VA = "0x1014F2674")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600489F RID: 18591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042DF")]
		[Address(RVA = "0x1014EDB98", Offset = "0x14EDB98", VA = "0x1014EDB98")]
		public void Setup()
		{
		}

		// Token: 0x060048A0 RID: 18592 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E0")]
		[Address(RVA = "0x1014EFA54", Offset = "0x14EFA54", VA = "0x1014EFA54")]
		public void Open(eCharaNamedType named)
		{
		}

		// Token: 0x060048A1 RID: 18593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E1")]
		[Address(RVA = "0x1014F2ABC", Offset = "0x14F2ABC", VA = "0x1014F2ABC")]
		private void OnClickSummonCallBack(int charaID)
		{
		}

		// Token: 0x060048A2 RID: 18594 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E2")]
		[Address(RVA = "0x1014F2B1C", Offset = "0x14F2B1C", VA = "0x1014F2B1C")]
		public MenuLibrarySummonGroup()
		{
		}

		// Token: 0x04005A80 RID: 23168
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FB4")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
