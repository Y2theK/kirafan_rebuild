﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F10 RID: 3856
	[Token(Token = "0x2000A34")]
	[StructLayout(3)]
	public class MenuLibrarySummonItem : ScrollItemIcon
	{
		// Token: 0x060048A3 RID: 18595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E3")]
		[Address(RVA = "0x1014F2B24", Offset = "0x14F2B24", VA = "0x1014F2B24", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060048A4 RID: 18596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E4")]
		[Address(RVA = "0x1014F2C34", Offset = "0x14F2C34", VA = "0x1014F2C34")]
		public MenuLibrarySummonItem()
		{
		}

		// Token: 0x04005A82 RID: 23170
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FB6")]
		[SerializeField]
		private Text m_TitleName;
	}
}
