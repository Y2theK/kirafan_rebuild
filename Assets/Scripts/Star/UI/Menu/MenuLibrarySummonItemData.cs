﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F11 RID: 3857
	[Token(Token = "0x2000A35")]
	[StructLayout(3)]
	public class MenuLibrarySummonItemData : ScrollItemData
	{
		// Token: 0x140000CA RID: 202
		// (add) Token: 0x060048A5 RID: 18597 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048A6 RID: 18598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CA")]
		public event Action<int> OnClickCallBack
		{
			[Token(Token = "0x60042E5")]
			[Address(RVA = "0x1014F29B0", Offset = "0x14F29B0", VA = "0x1014F29B0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042E6")]
			[Address(RVA = "0x1014F2C3C", Offset = "0x14F2C3C", VA = "0x1014F2C3C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048A7 RID: 18599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E7")]
		[Address(RVA = "0x1014F2780", Offset = "0x14F2780", VA = "0x1014F2780")]
		public MenuLibrarySummonItemData(int charaID)
		{
		}

		// Token: 0x060048A8 RID: 18600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E8")]
		[Address(RVA = "0x1014F2D48", Offset = "0x14F2D48", VA = "0x1014F2D48", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005A83 RID: 23171
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003FB7")]
		public string m_Title;

		// Token: 0x04005A84 RID: 23172
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003FB8")]
		public int m_CharaID;
	}
}
