﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F12 RID: 3858
	[Token(Token = "0x2000A36")]
	[StructLayout(3)]
	public class MenuLibraryTitleItem : ScrollItemIcon
	{
		// Token: 0x060048A9 RID: 18601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042E9")]
		[Address(RVA = "0x1014F2D9C", Offset = "0x14F2D9C", VA = "0x1014F2D9C", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x060048AA RID: 18602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042EA")]
		[Address(RVA = "0x1014F2DA4", Offset = "0x14F2DA4", VA = "0x1014F2DA4")]
		public void Apply()
		{
		}

		// Token: 0x060048AB RID: 18603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042EB")]
		[Address(RVA = "0x1014F2DB0", Offset = "0x14F2DB0", VA = "0x1014F2DB0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060048AC RID: 18604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042EC")]
		[Address(RVA = "0x1014F2F00", Offset = "0x14F2F00", VA = "0x1014F2F00", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060048AD RID: 18605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042ED")]
		[Address(RVA = "0x1014F2F08", Offset = "0x14F2F08", VA = "0x1014F2F08")]
		public void OnCallbackButton()
		{
		}

		// Token: 0x060048AE RID: 18606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042EE")]
		[Address(RVA = "0x1014F2F0C", Offset = "0x14F2F0C", VA = "0x1014F2F0C")]
		public MenuLibraryTitleItem()
		{
		}

		// Token: 0x04005A86 RID: 23174
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FBA")]
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04005A87 RID: 23175
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FBB")]
		[SerializeField]
		private CustomButton m_Button;
	}
}
