﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F13 RID: 3859
	[Token(Token = "0x2000A37")]
	[StructLayout(3)]
	public class MenuLibraryUI : MenuUIBase
	{
		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x060048AF RID: 18607 RVA: 0x0001A580 File Offset: 0x00018780
		[Token(Token = "0x170004D4")]
		public MenuLibraryUI.eButton SelectButton
		{
			[Token(Token = "0x60042EF")]
			[Address(RVA = "0x1014F2F14", Offset = "0x14F2F14", VA = "0x1014F2F14")]
			get
			{
				return MenuLibraryUI.eButton.TitleExp;
			}
		}

		// Token: 0x140000CB RID: 203
		// (add) Token: 0x060048B0 RID: 18608 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048B1 RID: 18609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CB")]
		public event Action OnClickButton
		{
			[Token(Token = "0x60042F0")]
			[Address(RVA = "0x1014F2F1C", Offset = "0x14F2F1C", VA = "0x1014F2F1C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60042F1")]
			[Address(RVA = "0x1014F3028", Offset = "0x14F3028", VA = "0x1014F3028")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048B2 RID: 18610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F2")]
		[Address(RVA = "0x1014F3134", Offset = "0x14F3134", VA = "0x1014F3134")]
		public void Setup()
		{
		}

		// Token: 0x060048B3 RID: 18611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F3")]
		[Address(RVA = "0x1014F31E4", Offset = "0x14F31E4", VA = "0x1014F31E4")]
		private void Update()
		{
		}

		// Token: 0x060048B4 RID: 18612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F4")]
		[Address(RVA = "0x1014F3374", Offset = "0x14F3374", VA = "0x1014F3374")]
		private void ChangeStep(MenuLibraryUI.eStep step)
		{
		}

		// Token: 0x060048B5 RID: 18613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F5")]
		[Address(RVA = "0x1014F3464", Offset = "0x14F3464", VA = "0x1014F3464", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060048B6 RID: 18614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F6")]
		[Address(RVA = "0x1014F3520", Offset = "0x14F3520", VA = "0x1014F3520", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060048B7 RID: 18615 RVA: 0x0001A598 File Offset: 0x00018798
		[Token(Token = "0x60042F7")]
		[Address(RVA = "0x1014F35DC", Offset = "0x14F35DC", VA = "0x1014F35DC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060048B8 RID: 18616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F8")]
		[Address(RVA = "0x1014F35EC", Offset = "0x14F35EC", VA = "0x1014F35EC")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060048B9 RID: 18617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042F9")]
		[Address(RVA = "0x1014F35F8", Offset = "0x14F35F8", VA = "0x1014F35F8")]
		public void OnSelectButtonCallBack(int fbutton)
		{
		}

		// Token: 0x060048BA RID: 18618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60042FA")]
		[Address(RVA = "0x1014F3608", Offset = "0x14F3608", VA = "0x1014F3608")]
		public MenuLibraryUI()
		{
		}

		// Token: 0x04005A88 RID: 23176
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FBC")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005A89 RID: 23177
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FBD")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005A8A RID: 23178
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FBE")]
		[SerializeField]
		private Badge m_ADVNewBadge;

		// Token: 0x04005A8B RID: 23179
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FBF")]
		private MenuLibraryUI.eButton m_SelectButton;

		// Token: 0x04005A8C RID: 23180
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003FC0")]
		private MenuLibraryUI.eStep m_Step;

		// Token: 0x02000F14 RID: 3860
		[Token(Token = "0x20011A2")]
		public enum eButton
		{
			// Token: 0x04005A8F RID: 23183
			[Token(Token = "0x4006E16")]
			None = -1,
			// Token: 0x04005A90 RID: 23184
			[Token(Token = "0x4006E17")]
			TitleExp,
			// Token: 0x04005A91 RID: 23185
			[Token(Token = "0x4006E18")]
			Word,
			// Token: 0x04005A92 RID: 23186
			[Token(Token = "0x4006E19")]
			OriginalChara,
			// Token: 0x04005A93 RID: 23187
			[Token(Token = "0x4006E1A")]
			ADV,
			// Token: 0x04005A94 RID: 23188
			[Token(Token = "0x4006E1B")]
			OPMovie
		}

		// Token: 0x02000F15 RID: 3861
		[Token(Token = "0x20011A3")]
		private enum eStep
		{
			// Token: 0x04005A96 RID: 23190
			[Token(Token = "0x4006E1D")]
			Hide,
			// Token: 0x04005A97 RID: 23191
			[Token(Token = "0x4006E1E")]
			In,
			// Token: 0x04005A98 RID: 23192
			[Token(Token = "0x4006E1F")]
			Idle,
			// Token: 0x04005A99 RID: 23193
			[Token(Token = "0x4006E20")]
			Out,
			// Token: 0x04005A9A RID: 23194
			[Token(Token = "0x4006E21")]
			End
		}
	}
}
