﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F17 RID: 3863
	[Token(Token = "0x2000A39")]
	[StructLayout(3)]
	public class MenuLibraryWordUI : MenuUIBase
	{
		// Token: 0x060048C1 RID: 18625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004301")]
		[Address(RVA = "0x1014F3618", Offset = "0x14F3618", VA = "0x1014F3618")]
		public void Setup(MenuLibraryWordUI.eMode mode)
		{
		}

		// Token: 0x060048C2 RID: 18626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004302")]
		[Address(RVA = "0x1014F3EC8", Offset = "0x14F3EC8", VA = "0x1014F3EC8")]
		private void Update()
		{
		}

		// Token: 0x060048C3 RID: 18627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004303")]
		[Address(RVA = "0x1014F4058", Offset = "0x14F4058", VA = "0x1014F4058")]
		private void ChangeStep(MenuLibraryWordUI.eStep step)
		{
		}

		// Token: 0x060048C4 RID: 18628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004304")]
		[Address(RVA = "0x1014F4148", Offset = "0x14F4148", VA = "0x1014F4148", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060048C5 RID: 18629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004305")]
		[Address(RVA = "0x1014F4204", Offset = "0x14F4204", VA = "0x1014F4204", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060048C6 RID: 18630 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004306")]
		[Address(RVA = "0x1014F42C0", Offset = "0x14F42C0", VA = "0x1014F42C0")]
		public TitleListDB GetTitleListDB()
		{
			return null;
		}

		// Token: 0x060048C7 RID: 18631 RVA: 0x0001A5B0 File Offset: 0x000187B0
		[Token(Token = "0x6004307")]
		[Address(RVA = "0x1014F4338", Offset = "0x14F4338", VA = "0x1014F4338", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060048C8 RID: 18632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004308")]
		[Address(RVA = "0x1014F4348", Offset = "0x14F4348", VA = "0x1014F4348")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060048C9 RID: 18633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004309")]
		[Address(RVA = "0x1014F3B7C", Offset = "0x14F3B7C", VA = "0x1014F3B7C")]
		private void OnClickTitleButtonCallBack(eTitleType ftitle)
		{
		}

		// Token: 0x060048CA RID: 18634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430A")]
		[Address(RVA = "0x1014F3D10", Offset = "0x14F3D10", VA = "0x1014F3D10")]
		private void OnClickWordButtonCallBack(int idx)
		{
		}

		// Token: 0x060048CB RID: 18635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430B")]
		[Address(RVA = "0x1014F4354", Offset = "0x14F4354", VA = "0x1014F4354")]
		public MenuLibraryWordUI()
		{
		}

		// Token: 0x04005AA1 RID: 23201
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FC8")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005AA2 RID: 23202
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FC9")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005AA3 RID: 23203
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FCA")]
		[SerializeField]
		private GameObject m_TitleObj;

		// Token: 0x04005AA4 RID: 23204
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FCB")]
		[SerializeField]
		private GameObject m_WordObj;

		// Token: 0x04005AA5 RID: 23205
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FCC")]
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x04005AA6 RID: 23206
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003FCD")]
		[SerializeField]
		private Text m_WordText;

		// Token: 0x04005AA7 RID: 23207
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003FCE")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04005AA8 RID: 23208
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003FCF")]
		[SerializeField]
		private Text m_WordDescriptText;

		// Token: 0x04005AA9 RID: 23209
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003FD0")]
		[SerializeField]
		private ScrollField m_ScrollField;

		// Token: 0x04005AAA RID: 23210
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FD1")]
		[SerializeField]
		private ScrollViewBase m_TitleList;

		// Token: 0x04005AAB RID: 23211
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003FD2")]
		private List<ITitleUIData> m_DataList;

		// Token: 0x04005AAC RID: 23212
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003FD3")]
		private MenuLibraryWordUI.eStep m_Step;

		// Token: 0x04005AAD RID: 23213
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4003FD4")]
		private MenuLibraryWordUI.eMode m_Mode;

		// Token: 0x02000F18 RID: 3864
		[Token(Token = "0x20011A4")]
		private enum eStep
		{
			// Token: 0x04005AAF RID: 23215
			[Token(Token = "0x4006E23")]
			Hide,
			// Token: 0x04005AB0 RID: 23216
			[Token(Token = "0x4006E24")]
			In,
			// Token: 0x04005AB1 RID: 23217
			[Token(Token = "0x4006E25")]
			Idle,
			// Token: 0x04005AB2 RID: 23218
			[Token(Token = "0x4006E26")]
			Out,
			// Token: 0x04005AB3 RID: 23219
			[Token(Token = "0x4006E27")]
			End
		}

		// Token: 0x02000F19 RID: 3865
		[Token(Token = "0x20011A5")]
		public enum eMode
		{
			// Token: 0x04005AB5 RID: 23221
			[Token(Token = "0x4006E29")]
			ContentTitle,
			// Token: 0x04005AB6 RID: 23222
			[Token(Token = "0x4006E2A")]
			Word
		}
	}
}
