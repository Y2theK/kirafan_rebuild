﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F1A RID: 3866
	[Token(Token = "0x2000A3A")]
	[StructLayout(3)]
	public class MenuNoticeItem : ScrollItemIcon
	{
		// Token: 0x060048CC RID: 18636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430C")]
		[Address(RVA = "0x1014F43C4", Offset = "0x14F43C4", VA = "0x1014F43C4")]
		public void Apply()
		{
		}

		// Token: 0x060048CD RID: 18637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430D")]
		[Address(RVA = "0x1014F43D0", Offset = "0x14F43D0", VA = "0x1014F43D0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060048CE RID: 18638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430E")]
		[Address(RVA = "0x1014F4548", Offset = "0x14F4548", VA = "0x1014F4548")]
		public void OnClickToggleSwitchCallBack()
		{
		}

		// Token: 0x060048CF RID: 18639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600430F")]
		[Address(RVA = "0x1014F46D0", Offset = "0x14F46D0", VA = "0x1014F46D0")]
		public MenuNoticeItem()
		{
		}

		// Token: 0x04005AB7 RID: 23223
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FD5")]
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04005AB8 RID: 23224
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FD6")]
		[SerializeField]
		private ToggleSwitch m_ToggleSwitch;
	}
}
