﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F1B RID: 3867
	[Token(Token = "0x2000A3B")]
	[StructLayout(3)]
	public class MenuNoticeItemData : ScrollItemData
	{
		// Token: 0x140000CE RID: 206
		// (add) Token: 0x060048D0 RID: 18640 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048D1 RID: 18641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CE")]
		public event Action<NotificationController.ePush, bool> OnSwitch
		{
			[Token(Token = "0x6004310")]
			[Address(RVA = "0x1014F46D8", Offset = "0x14F46D8", VA = "0x1014F46D8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004311")]
			[Address(RVA = "0x1014F47E4", Offset = "0x14F47E4", VA = "0x1014F47E4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048D2 RID: 18642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004312")]
		[Address(RVA = "0x1014F48F0", Offset = "0x14F48F0", VA = "0x1014F48F0")]
		public MenuNoticeItemData(NotificationController.ePush push, string title, bool flg, bool interactable)
		{
		}

		// Token: 0x060048D3 RID: 18643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004313")]
		[Address(RVA = "0x1014F4678", Offset = "0x14F4678", VA = "0x1014F4678")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x04005AB9 RID: 23225
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003FD7")]
		public NotificationController.ePush m_Push;

		// Token: 0x04005ABA RID: 23226
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003FD8")]
		public string m_Title;

		// Token: 0x04005ABB RID: 23227
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003FD9")]
		public bool m_Flag;

		// Token: 0x04005ABC RID: 23228
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003FDA")]
		public bool m_Interactable;
	}
}
