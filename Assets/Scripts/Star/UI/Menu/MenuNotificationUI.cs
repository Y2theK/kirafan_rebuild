﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F1C RID: 3868
	[Token(Token = "0x2000A3C")]
	[StructLayout(3)]
	public class MenuNotificationUI : MenuUIBase
	{
		// Token: 0x140000CF RID: 207
		// (add) Token: 0x060048D4 RID: 18644 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048D5 RID: 18645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000CF")]
		public event Action OnClickDecideButton
		{
			[Token(Token = "0x6004314")]
			[Address(RVA = "0x1014F4944", Offset = "0x14F4944", VA = "0x1014F4944")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004315")]
			[Address(RVA = "0x1014F4A50", Offset = "0x14F4A50", VA = "0x1014F4A50")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048D6 RID: 18646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004316")]
		[Address(RVA = "0x1014F4B5C", Offset = "0x14F4B5C", VA = "0x1014F4B5C")]
		public void Setup()
		{
		}

		// Token: 0x060048D7 RID: 18647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004317")]
		[Address(RVA = "0x1014F4F08", Offset = "0x14F4F08", VA = "0x1014F4F08")]
		private void Update()
		{
		}

		// Token: 0x060048D8 RID: 18648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004318")]
		[Address(RVA = "0x1014F5098", Offset = "0x14F5098", VA = "0x1014F5098")]
		private void ChangeStep(MenuNotificationUI.eStep step)
		{
		}

		// Token: 0x060048D9 RID: 18649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004319")]
		[Address(RVA = "0x1014F5188", Offset = "0x14F5188", VA = "0x1014F5188", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060048DA RID: 18650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600431A")]
		[Address(RVA = "0x1014F5244", Offset = "0x14F5244", VA = "0x1014F5244", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060048DB RID: 18651 RVA: 0x0001A5C8 File Offset: 0x000187C8
		[Token(Token = "0x600431B")]
		[Address(RVA = "0x1014F5300", Offset = "0x14F5300", VA = "0x1014F5300", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060048DC RID: 18652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600431C")]
		[Address(RVA = "0x1014F5310", Offset = "0x14F5310", VA = "0x1014F5310")]
		public void OnDecideButtonCallBack()
		{
		}

		// Token: 0x060048DD RID: 18653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600431D")]
		[Address(RVA = "0x1014F531C", Offset = "0x14F531C", VA = "0x1014F531C")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060048DE RID: 18654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600431E")]
		[Address(RVA = "0x1014F53A8", Offset = "0x14F53A8", VA = "0x1014F53A8")]
		public void OnClickToggleButtonCallBack(NotificationController.ePush push, bool flg)
		{
		}

		// Token: 0x060048DF RID: 18655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600431F")]
		[Address(RVA = "0x1014F56BC", Offset = "0x14F56BC", VA = "0x1014F56BC")]
		public MenuNotificationUI()
		{
		}

		// Token: 0x04005ABE RID: 23230
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FDC")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005ABF RID: 23231
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FDD")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005AC0 RID: 23232
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FDE")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005AC1 RID: 23233
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FDF")]
		[SerializeField]
		private MenuNoticeItem m_AllItem;

		// Token: 0x04005AC2 RID: 23234
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FE0")]
		private MenuNotificationUI.eStep m_Step;

		// Token: 0x04005AC3 RID: 23235
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003FE1")]
		private List<MenuNoticeItemData> m_DataList;

		// Token: 0x02000F1D RID: 3869
		[Token(Token = "0x20011A6")]
		private enum eStep
		{
			// Token: 0x04005AC6 RID: 23238
			[Token(Token = "0x4006E2C")]
			Hide,
			// Token: 0x04005AC7 RID: 23239
			[Token(Token = "0x4006E2D")]
			In,
			// Token: 0x04005AC8 RID: 23240
			[Token(Token = "0x4006E2E")]
			Idle,
			// Token: 0x04005AC9 RID: 23241
			[Token(Token = "0x4006E2F")]
			Out,
			// Token: 0x04005ACA RID: 23242
			[Token(Token = "0x4006E30")]
			End
		}

		// Token: 0x02000F1E RID: 3870
		[Token(Token = "0x20011A7")]
		public enum eNotice
		{
			// Token: 0x04005ACC RID: 23244
			[Token(Token = "0x4006E32")]
			All,
			// Token: 0x04005ACD RID: 23245
			[Token(Token = "0x4006E33")]
			Info,
			// Token: 0x04005ACE RID: 23246
			[Token(Token = "0x4006E34")]
			Stamina,
			// Token: 0x04005ACF RID: 23247
			[Token(Token = "0x4006E35")]
			Num
		}
	}
}
