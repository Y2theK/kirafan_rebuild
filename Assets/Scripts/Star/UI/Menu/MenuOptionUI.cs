﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F1F RID: 3871
	[Token(Token = "0x2000A3D")]
	[StructLayout(3)]
	public class MenuOptionUI : MenuUIBase
	{
		// Token: 0x060048E0 RID: 18656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004320")]
		[Address(RVA = "0x1014F572C", Offset = "0x14F572C", VA = "0x1014F572C")]
		public void Setup()
		{
		}

		// Token: 0x060048E1 RID: 18657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004321")]
		[Address(RVA = "0x1014F57C8", Offset = "0x14F57C8", VA = "0x1014F57C8")]
		private void Update()
		{
		}

		// Token: 0x060048E2 RID: 18658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004322")]
		[Address(RVA = "0x1014F5958", Offset = "0x14F5958", VA = "0x1014F5958")]
		private void ChangeStep(MenuOptionUI.eStep step)
		{
		}

		// Token: 0x060048E3 RID: 18659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004323")]
		[Address(RVA = "0x1014F5A48", Offset = "0x14F5A48", VA = "0x1014F5A48", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060048E4 RID: 18660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004324")]
		[Address(RVA = "0x1014F62A4", Offset = "0x14F62A4", VA = "0x1014F62A4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060048E5 RID: 18661 RVA: 0x0001A5E0 File Offset: 0x000187E0
		[Token(Token = "0x6004325")]
		[Address(RVA = "0x1014F6344", Offset = "0x14F6344", VA = "0x1014F6344", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060048E6 RID: 18662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004326")]
		[Address(RVA = "0x1014F6354", Offset = "0x14F6354", VA = "0x1014F6354")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060048E7 RID: 18663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004327")]
		[Address(RVA = "0x1014F6358", Offset = "0x14F6358", VA = "0x1014F6358")]
		public void OnCopyButtonCallBack()
		{
		}

		// Token: 0x060048E8 RID: 18664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004328")]
		[Address(RVA = "0x1014F635C", Offset = "0x14F635C", VA = "0x1014F635C")]
		public MenuOptionUI()
		{
		}

		// Token: 0x04005AD0 RID: 23248
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FE3")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005AD1 RID: 23249
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FE4")]
		[SerializeField]
		private OptionWindow m_OptionGroup;

		// Token: 0x04005AD2 RID: 23250
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FE5")]
		private MenuOptionUI.eStep m_Step;

		// Token: 0x02000F20 RID: 3872
		[Token(Token = "0x20011A8")]
		private enum eStep
		{
			// Token: 0x04005AD4 RID: 23252
			[Token(Token = "0x4006E37")]
			Hide,
			// Token: 0x04005AD5 RID: 23253
			[Token(Token = "0x4006E38")]
			In,
			// Token: 0x04005AD6 RID: 23254
			[Token(Token = "0x4006E39")]
			Idle,
			// Token: 0x04005AD7 RID: 23255
			[Token(Token = "0x4006E3A")]
			Out,
			// Token: 0x04005AD8 RID: 23256
			[Token(Token = "0x4006E3B")]
			End
		}
	}
}
