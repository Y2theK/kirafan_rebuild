﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F21 RID: 3873
	[Token(Token = "0x2000A3E")]
	[StructLayout(3)]
	public class MenuPlayerMoveConfigurationUI : MenuUIBase
	{
		// Token: 0x140000D0 RID: 208
		// (add) Token: 0x060048E9 RID: 18665 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048EA RID: 18666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D0")]
		public event Action<string> m_OnClickInputPasswordDecideButton
		{
			[Token(Token = "0x6004329")]
			[Address(RVA = "0x1014F6364", Offset = "0x14F6364", VA = "0x1014F6364")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600432A")]
			[Address(RVA = "0x1014F6470", Offset = "0x14F6470", VA = "0x1014F6470")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000D1 RID: 209
		// (add) Token: 0x060048EB RID: 18667 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060048EC RID: 18668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D1")]
		public event Action m_OnClickPlayerMoveDataCloseButton
		{
			[Token(Token = "0x600432B")]
			[Address(RVA = "0x1014F657C", Offset = "0x14F657C", VA = "0x1014F657C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600432C")]
			[Address(RVA = "0x1014F6688", Offset = "0x14F6688", VA = "0x1014F6688")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060048ED RID: 18669 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600432D")]
		[Address(RVA = "0x1014F6794", Offset = "0x14F6794", VA = "0x1014F6794")]
		private void Start()
		{
		}

		// Token: 0x060048EE RID: 18670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600432E")]
		[Address(RVA = "0x1014F6798", Offset = "0x14F6798", VA = "0x1014F6798")]
		public void Setup()
		{
		}

		// Token: 0x060048EF RID: 18671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600432F")]
		[Address(RVA = "0x1014F67C4", Offset = "0x14F67C4", VA = "0x1014F67C4")]
		private void Update()
		{
		}

		// Token: 0x060048F0 RID: 18672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004330")]
		[Address(RVA = "0x1014F69E8", Offset = "0x14F69E8", VA = "0x1014F69E8")]
		private void ChangeStep(MenuPlayerMoveConfigurationUI.eStep step)
		{
		}

		// Token: 0x060048F1 RID: 18673 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004331")]
		[Address(RVA = "0x1014F6AE0", Offset = "0x14F6AE0", VA = "0x1014F6AE0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060048F2 RID: 18674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004332")]
		[Address(RVA = "0x1014F6BB4", Offset = "0x14F6BB4", VA = "0x1014F6BB4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060048F3 RID: 18675 RVA: 0x0001A5F8 File Offset: 0x000187F8
		[Token(Token = "0x6004333")]
		[Address(RVA = "0x1014F6BF4", Offset = "0x14F6BF4", VA = "0x1014F6BF4")]
		public MenuPlayerMoveConfigurationUI.eWindowType GetActiveWindowType()
		{
			return MenuPlayerMoveConfigurationUI.eWindowType.None;
		}

		// Token: 0x060048F4 RID: 18676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004334")]
		[Address(RVA = "0x1014F6BFC", Offset = "0x14F6BFC", VA = "0x1014F6BFC")]
		public void SetOpenWindowType(MenuPlayerMoveConfigurationUI.eWindowType openWindowType)
		{
		}

		// Token: 0x060048F5 RID: 18677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004335")]
		[Address(RVA = "0x1014F6C3C", Offset = "0x14F6C3C", VA = "0x1014F6C3C")]
		public void SetInheritanceWindowIdPassword(string moveCode, string movePassward, DateTime moveDeadLine)
		{
		}

		// Token: 0x060048F6 RID: 18678 RVA: 0x0001A610 File Offset: 0x00018810
		[Token(Token = "0x6004336")]
		[Address(RVA = "0x1014F6AD8", Offset = "0x14F6AD8", VA = "0x1014F6AD8")]
		public bool IsDecided()
		{
			return default(bool);
		}

		// Token: 0x060048F7 RID: 18679 RVA: 0x0001A628 File Offset: 0x00018828
		[Token(Token = "0x6004337")]
		[Address(RVA = "0x1014F6FD4", Offset = "0x14F6FD4", VA = "0x1014F6FD4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060048F8 RID: 18680 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004338")]
		[Address(RVA = "0x1014F6FE4", Offset = "0x14F6FE4", VA = "0x1014F6FE4")]
		public void OnClickCancelButtonCallBack()
		{
		}

		// Token: 0x060048F9 RID: 18681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004339")]
		[Address(RVA = "0x1014F6FF4", Offset = "0x14F6FF4", VA = "0x1014F6FF4")]
		public void OnClickInputPasswordDecideButton()
		{
		}

		// Token: 0x060048FA RID: 18682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600433A")]
		[Address(RVA = "0x1014F7024", Offset = "0x14F7024", VA = "0x1014F7024")]
		public void OnClickPlayerMoveDataCloseButton(int index)
		{
		}

		// Token: 0x060048FB RID: 18683 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600433B")]
		[Address(RVA = "0x1014F7054", Offset = "0x14F7054", VA = "0x1014F7054")]
		public UIGroup GetSelectGroup()
		{
			return null;
		}

		// Token: 0x060048FC RID: 18684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600433C")]
		[Address(RVA = "0x1014F705C", Offset = "0x14F705C", VA = "0x1014F705C")]
		public MenuPlayerMoveConfigurationUI()
		{
		}

		// Token: 0x04005AD9 RID: 23257
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FE6")]
		[SerializeField]
		private UIGroup m_InputPasswordGroup;

		// Token: 0x04005ADA RID: 23258
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FE7")]
		[SerializeField]
		private UIGroup m_ResultGroup;

		// Token: 0x04005ADB RID: 23259
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FE8")]
		[SerializeField]
		private UIGroup m_SelectGroup;

		// Token: 0x04005ADC RID: 23260
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FE9")]
		[SerializeField]
		private InputField m_PassWordInputField;

		// Token: 0x04005ADD RID: 23261
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FEA")]
		[SerializeField]
		private CustomButton m_InputPasswordDecideButton;

		// Token: 0x04005ADE RID: 23262
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003FEB")]
		[SerializeField]
		private Text m_IDText;

		// Token: 0x04005ADF RID: 23263
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003FEC")]
		[SerializeField]
		private Text m_PassWordText;

		// Token: 0x04005AE0 RID: 23264
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003FED")]
		[SerializeField]
		private Text m_DeadlineText;

		// Token: 0x04005AE1 RID: 23265
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003FEE")]
		private MenuPlayerMoveConfigurationUI.eWindowType m_ActiveWindowType;

		// Token: 0x04005AE2 RID: 23266
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FEF")]
		private UIGroup m_ActiveUIGroup;

		// Token: 0x04005AE3 RID: 23267
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003FF0")]
		private bool m_IsDecided;

		// Token: 0x04005AE4 RID: 23268
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003FF1")]
		private MenuPlayerMoveConfigurationUI.eStep m_Step;

		// Token: 0x02000F22 RID: 3874
		[Token(Token = "0x20011A9")]
		public enum eWindowType
		{
			// Token: 0x04005AE8 RID: 23272
			[Token(Token = "0x4006E3D")]
			None,
			// Token: 0x04005AE9 RID: 23273
			[Token(Token = "0x4006E3E")]
			InputPassword,
			// Token: 0x04005AEA RID: 23274
			[Token(Token = "0x4006E3F")]
			PlayerMoveData
		}

		// Token: 0x02000F23 RID: 3875
		[Token(Token = "0x20011AA")]
		private enum eStep
		{
			// Token: 0x04005AEC RID: 23276
			[Token(Token = "0x4006E41")]
			Hide,
			// Token: 0x04005AED RID: 23277
			[Token(Token = "0x4006E42")]
			In,
			// Token: 0x04005AEE RID: 23278
			[Token(Token = "0x4006E43")]
			Idle,
			// Token: 0x04005AEF RID: 23279
			[Token(Token = "0x4006E44")]
			Out,
			// Token: 0x04005AF0 RID: 23280
			[Token(Token = "0x4006E45")]
			End
		}
	}
}
