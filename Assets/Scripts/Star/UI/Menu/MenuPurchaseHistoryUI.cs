﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F24 RID: 3876
	[Token(Token = "0x2000A3F")]
	[StructLayout(3)]
	public class MenuPurchaseHistoryUI : MenuUIBase
	{
		// Token: 0x060048FD RID: 18685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600433D")]
		[Address(RVA = "0x1014F7064", Offset = "0x14F7064", VA = "0x1014F7064")]
		private void Start()
		{
		}

		// Token: 0x060048FE RID: 18686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600433E")]
		[Address(RVA = "0x1014F7068", Offset = "0x14F7068", VA = "0x1014F7068")]
		public void Setup()
		{
		}

		// Token: 0x060048FF RID: 18687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600433F")]
		[Address(RVA = "0x1014F73E8", Offset = "0x14F73E8", VA = "0x1014F73E8")]
		private void Refresh()
		{
		}

		// Token: 0x06004900 RID: 18688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004340")]
		[Address(RVA = "0x1014F7648", Offset = "0x14F7648", VA = "0x1014F7648")]
		private void Update()
		{
		}

		// Token: 0x06004901 RID: 18689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004341")]
		[Address(RVA = "0x1014F7804", Offset = "0x14F7804", VA = "0x1014F7804")]
		private void ChangeStep(MenuPurchaseHistoryUI.eStep step)
		{
		}

		// Token: 0x06004902 RID: 18690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004342")]
		[Address(RVA = "0x1014F7A18", Offset = "0x14F7A18", VA = "0x1014F7A18", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004903 RID: 18691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004343")]
		[Address(RVA = "0x1014F7AD4", Offset = "0x14F7AD4", VA = "0x1014F7AD4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004904 RID: 18692 RVA: 0x0001A640 File Offset: 0x00018840
		[Token(Token = "0x6004344")]
		[Address(RVA = "0x1014F7B90", Offset = "0x14F7B90", VA = "0x1014F7B90", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004905 RID: 18693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004345")]
		[Address(RVA = "0x1014F7BA0", Offset = "0x14F7BA0", VA = "0x1014F7BA0")]
		public void OnDecideButtonCallBack()
		{
		}

		// Token: 0x06004906 RID: 18694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004346")]
		[Address(RVA = "0x1014F7BAC", Offset = "0x14F7BAC", VA = "0x1014F7BAC")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004907 RID: 18695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004347")]
		[Address(RVA = "0x1014F7BB8", Offset = "0x14F7BB8", VA = "0x1014F7BB8")]
		public void OnClickAllButtonCallBack()
		{
		}

		// Token: 0x06004908 RID: 18696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004348")]
		[Address(RVA = "0x1014F7BBC", Offset = "0x14F7BBC", VA = "0x1014F7BBC")]
		public void OnClickToggleButtonCallBack()
		{
		}

		// Token: 0x06004909 RID: 18697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004349")]
		[Address(RVA = "0x1014F7BEC", Offset = "0x14F7BEC", VA = "0x1014F7BEC")]
		public void OnClickShowLimitButton()
		{
		}

		// Token: 0x0600490A RID: 18698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434A")]
		[Address(RVA = "0x1014F7C04", Offset = "0x14F7C04", VA = "0x1014F7C04")]
		public void OnPeriodCheckEnd()
		{
		}

		// Token: 0x0600490B RID: 18699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434B")]
		[Address(RVA = "0x1014F822C", Offset = "0x14F822C", VA = "0x1014F822C")]
		public MenuPurchaseHistoryUI()
		{
		}

		// Token: 0x04005AF1 RID: 23281
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003FF4")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005AF2 RID: 23282
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003FF5")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005AF3 RID: 23283
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003FF6")]
		[SerializeField]
		private TextField m_PaidGemField;

		// Token: 0x04005AF4 RID: 23284
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FF7")]
		[SerializeField]
		private TextField m_FreeGemField;

		// Token: 0x04005AF5 RID: 23285
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FF8")]
		[SerializeField]
		private TextField m_AllGemField;

		// Token: 0x04005AF6 RID: 23286
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003FF9")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005AF7 RID: 23287
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003FFA")]
		[SerializeField]
		private CustomButton m_ShowLimitButton;

		// Token: 0x04005AF8 RID: 23288
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003FFB")]
		[SerializeField]
		private PassportPeriodWindow m_PassportPeriodWindow;

		// Token: 0x04005AF9 RID: 23289
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003FFC")]
		private MenuPurchaseHistoryUI.eStep m_Step;

		// Token: 0x04005AFA RID: 23290
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003FFD")]
		private List<MenuPurchaseItemData> m_DataList;

		// Token: 0x02000F25 RID: 3877
		[Token(Token = "0x20011AB")]
		private enum eStep
		{
			// Token: 0x04005AFC RID: 23292
			[Token(Token = "0x4006E47")]
			Hide,
			// Token: 0x04005AFD RID: 23293
			[Token(Token = "0x4006E48")]
			In,
			// Token: 0x04005AFE RID: 23294
			[Token(Token = "0x4006E49")]
			Idle,
			// Token: 0x04005AFF RID: 23295
			[Token(Token = "0x4006E4A")]
			Out,
			// Token: 0x04005B00 RID: 23296
			[Token(Token = "0x4006E4B")]
			PeriodCheck,
			// Token: 0x04005B01 RID: 23297
			[Token(Token = "0x4006E4C")]
			Period,
			// Token: 0x04005B02 RID: 23298
			[Token(Token = "0x4006E4D")]
			End
		}
	}
}
