﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F27 RID: 3879
	[Token(Token = "0x2000A40")]
	[StructLayout(3)]
	public class MenuPurchaseItem : ScrollItemIcon
	{
		// Token: 0x06004910 RID: 18704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434C")]
		[Address(RVA = "0x1014F8418", Offset = "0x14F8418", VA = "0x1014F8418")]
		public void Apply()
		{
		}

		// Token: 0x06004911 RID: 18705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434D")]
		[Address(RVA = "0x1014F8424", Offset = "0x14F8424", VA = "0x1014F8424", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004912 RID: 18706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434E")]
		[Address(RVA = "0x1014F886C", Offset = "0x14F886C", VA = "0x1014F886C")]
		public MenuPurchaseItem()
		{
		}

		// Token: 0x04005B06 RID: 23302
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003FFE")]
		public Text m_Date;

		// Token: 0x04005B07 RID: 23303
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003FFF")]
		public Text m_ProductName;

		// Token: 0x04005B08 RID: 23304
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004000")]
		public Text m_Price;

		// Token: 0x04005B09 RID: 23305
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004001")]
		public Text m_ExpiredAt;
	}
}
