﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F28 RID: 3880
	[Token(Token = "0x2000A41")]
	[StructLayout(3)]
	public class MenuPurchaseItemData : ScrollItemData
	{
		// Token: 0x06004913 RID: 18707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600434F")]
		[Address(RVA = "0x1014F75B4", Offset = "0x14F75B4", VA = "0x1014F75B4")]
		public MenuPurchaseItemData(StoreManager.PurchaseLog data)
		{
		}

		// Token: 0x04005B0A RID: 23306
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004002")]
		public DateTime m_Date;

		// Token: 0x04005B0B RID: 23307
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004003")]
		public long m_Price;

		// Token: 0x04005B0C RID: 23308
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004004")]
		public string m_ProductName;

		// Token: 0x04005B0D RID: 23309
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004005")]
		public DateTime? m_ExpiredAt;
	}
}
