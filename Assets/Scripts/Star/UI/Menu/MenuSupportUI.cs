﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F2C RID: 3884
	[Token(Token = "0x2000A44")]
	[StructLayout(3)]
	public class MenuSupportUI : MenuUIBase
	{
		// Token: 0x0600491E RID: 18718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004359")]
		[Address(RVA = "0x1014F8874", Offset = "0x14F8874", VA = "0x1014F8874")]
		public void Setup()
		{
		}

		// Token: 0x0600491F RID: 18719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600435A")]
		[Address(RVA = "0x1014F889C", Offset = "0x14F889C", VA = "0x1014F889C")]
		private void Update()
		{
		}

		// Token: 0x06004920 RID: 18720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600435B")]
		[Address(RVA = "0x1014F8A2C", Offset = "0x14F8A2C", VA = "0x1014F8A2C")]
		private void ChangeStep(MenuSupportUI.eStep step)
		{
		}

		// Token: 0x06004921 RID: 18721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600435C")]
		[Address(RVA = "0x1014F8B1C", Offset = "0x14F8B1C", VA = "0x1014F8B1C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004922 RID: 18722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600435D")]
		[Address(RVA = "0x1014F8BD8", Offset = "0x14F8BD8", VA = "0x1014F8BD8", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004923 RID: 18723 RVA: 0x0001A6A0 File Offset: 0x000188A0
		[Token(Token = "0x600435E")]
		[Address(RVA = "0x1014F8C94", Offset = "0x14F8C94", VA = "0x1014F8C94", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004924 RID: 18724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600435F")]
		[Address(RVA = "0x1014F8CA4", Offset = "0x14F8CA4", VA = "0x1014F8CA4")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004925 RID: 18725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004360")]
		[Address(RVA = "0x1014F8CA8", Offset = "0x14F8CA8", VA = "0x1014F8CA8")]
		public void OnSelectButtonCallBack(int fbutton)
		{
		}

		// Token: 0x06004926 RID: 18726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004361")]
		[Address(RVA = "0x1014F8DA0", Offset = "0x14F8DA0", VA = "0x1014F8DA0")]
		public void OnClickPurchaseHistory()
		{
		}

		// Token: 0x06004927 RID: 18727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004362")]
		[Address(RVA = "0x1014F8DAC", Offset = "0x14F8DAC", VA = "0x1014F8DAC")]
		public MenuSupportUI()
		{
		}

		// Token: 0x04005B16 RID: 23318
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400400B")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005B17 RID: 23319
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400400C")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005B18 RID: 23320
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400400D")]
		private MenuSupportUI.eStep m_Step;

		// Token: 0x04005B19 RID: 23321
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400400E")]
		public Action m_OnClickPurchaseHistory;

		// Token: 0x02000F2D RID: 3885
		[Token(Token = "0x20011AE")]
		private enum eStep
		{
			// Token: 0x04005B1B RID: 23323
			[Token(Token = "0x4006E55")]
			Hide,
			// Token: 0x04005B1C RID: 23324
			[Token(Token = "0x4006E56")]
			In,
			// Token: 0x04005B1D RID: 23325
			[Token(Token = "0x4006E57")]
			Idle,
			// Token: 0x04005B1E RID: 23326
			[Token(Token = "0x4006E58")]
			Out,
			// Token: 0x04005B1F RID: 23327
			[Token(Token = "0x4006E59")]
			End
		}
	}
}
