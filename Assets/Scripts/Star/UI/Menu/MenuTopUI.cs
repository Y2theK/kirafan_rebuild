﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F2E RID: 3886
	[Token(Token = "0x2000A45")]
	[StructLayout(3)]
	public class MenuTopUI : MenuUIBase
	{
		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06004928 RID: 18728 RVA: 0x0001A6B8 File Offset: 0x000188B8
		[Token(Token = "0x170004D5")]
		public MenuTopUI.eButton SelectButton
		{
			[Token(Token = "0x6004363")]
			[Address(RVA = "0x1014F8DB4", Offset = "0x14F8DB4", VA = "0x1014F8DB4")]
			get
			{
				return MenuTopUI.eButton.UserInfomation;
			}
		}

		// Token: 0x06004929 RID: 18729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004364")]
		[Address(RVA = "0x1014F8DBC", Offset = "0x14F8DBC", VA = "0x1014F8DBC", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600492A RID: 18730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004365")]
		[Address(RVA = "0x1014F8DC4", Offset = "0x14F8DC4", VA = "0x1014F8DC4")]
		public void Setup()
		{
		}

		// Token: 0x0600492B RID: 18731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004366")]
		[Address(RVA = "0x1014F8E90", Offset = "0x14F8E90", VA = "0x1014F8E90")]
		private void Update()
		{
		}

		// Token: 0x0600492C RID: 18732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004367")]
		[Address(RVA = "0x1014F9020", Offset = "0x14F9020", VA = "0x1014F9020")]
		private void ChangeStep(MenuTopUI.eStep step)
		{
		}

		// Token: 0x0600492D RID: 18733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004368")]
		[Address(RVA = "0x1014F9110", Offset = "0x14F9110", VA = "0x1014F9110", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600492E RID: 18734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004369")]
		[Address(RVA = "0x1014F91CC", Offset = "0x14F91CC", VA = "0x1014F91CC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600492F RID: 18735 RVA: 0x0001A6D0 File Offset: 0x000188D0
		[Token(Token = "0x600436A")]
		[Address(RVA = "0x1014F9288", Offset = "0x14F9288", VA = "0x1014F9288", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004930 RID: 18736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600436B")]
		[Address(RVA = "0x1014F9298", Offset = "0x14F9298", VA = "0x1014F9298")]
		public void OnClickButtonCallBack(int buttonId)
		{
		}

		// Token: 0x06004931 RID: 18737 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600436C")]
		[Address(RVA = "0x1014F9498", Offset = "0x14F9498", VA = "0x1014F9498")]
		public void OnClickCloseButton(int buttonId)
		{
		}

		// Token: 0x06004932 RID: 18738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600436D")]
		[Address(RVA = "0x1014F94F8", Offset = "0x14F94F8", VA = "0x1014F94F8")]
		public void OnConfirmTransit(int btn)
		{
		}

		// Token: 0x06004933 RID: 18739 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600436E")]
		[Address(RVA = "0x1014F9638", Offset = "0x14F9638", VA = "0x1014F9638")]
		public MenuTopUI()
		{
		}

		// Token: 0x04005B20 RID: 23328
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400400F")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005B21 RID: 23329
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004010")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005B22 RID: 23330
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004011")]
		[SerializeField]
		private Badge m_FriendBadge;

		// Token: 0x04005B23 RID: 23331
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004012")]
		[SerializeField]
		private ItemListGroup m_ItemListGroup;

		// Token: 0x04005B24 RID: 23332
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004013")]
		private MenuTopUI.eStep m_Step;

		// Token: 0x04005B25 RID: 23333
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004014")]
		private MenuTopUI.eButton m_SelectButton;

		// Token: 0x02000F2F RID: 3887
		[Token(Token = "0x20011AF")]
		private enum eStep
		{
			// Token: 0x04005B27 RID: 23335
			[Token(Token = "0x4006E5B")]
			Hide,
			// Token: 0x04005B28 RID: 23336
			[Token(Token = "0x4006E5C")]
			In,
			// Token: 0x04005B29 RID: 23337
			[Token(Token = "0x4006E5D")]
			Idle,
			// Token: 0x04005B2A RID: 23338
			[Token(Token = "0x4006E5E")]
			Out,
			// Token: 0x04005B2B RID: 23339
			[Token(Token = "0x4006E5F")]
			End
		}

		// Token: 0x02000F30 RID: 3888
		[Token(Token = "0x20011B0")]
		public enum eButton
		{
			// Token: 0x04005B2D RID: 23341
			[Token(Token = "0x4006E61")]
			None = -1,
			// Token: 0x04005B2E RID: 23342
			[Token(Token = "0x4006E62")]
			UserInfomation,
			// Token: 0x04005B2F RID: 23343
			[Token(Token = "0x4006E63")]
			Purchase,
			// Token: 0x04005B30 RID: 23344
			[Token(Token = "0x4006E64")]
			Friend,
			// Token: 0x04005B31 RID: 23345
			[Token(Token = "0x4006E65")]
			Help,
			// Token: 0x04005B32 RID: 23346
			[Token(Token = "0x4006E66")]
			Option,
			// Token: 0x04005B33 RID: 23347
			[Token(Token = "0x4006E67")]
			Notification,
			// Token: 0x04005B34 RID: 23348
			[Token(Token = "0x4006E68")]
			Ask,
			// Token: 0x04005B35 RID: 23349
			[Token(Token = "0x4006E69")]
			Site,
			// Token: 0x04005B36 RID: 23350
			[Token(Token = "0x4006E6A")]
			Twitter,
			// Token: 0x04005B37 RID: 23351
			[Token(Token = "0x4006E6B")]
			Title,
			// Token: 0x04005B38 RID: 23352
			[Token(Token = "0x4006E6C")]
			PlayerMoveConfiguration,
			// Token: 0x04005B39 RID: 23353
			[Token(Token = "0x4006E6D")]
			Library,
			// Token: 0x04005B3A RID: 23354
			[Token(Token = "0x4006E6E")]
			ItemList
		}
	}
}
