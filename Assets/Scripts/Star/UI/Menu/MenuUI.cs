﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Menu
{
	// Token: 0x02000F31 RID: 3889
	[Token(Token = "0x2000A46")]
	[StructLayout(3)]
	public class MenuUI : MenuUIBase
	{
		// Token: 0x06004934 RID: 18740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600436F")]
		[Address(RVA = "0x1014F9648", Offset = "0x14F9648", VA = "0x1014F9648")]
		public void Setup()
		{
		}

		// Token: 0x06004935 RID: 18741 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004370")]
		[Address(RVA = "0x1014F964C", Offset = "0x14F964C", VA = "0x1014F964C")]
		private void Update()
		{
		}

		// Token: 0x06004936 RID: 18742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004371")]
		[Address(RVA = "0x1014F9650", Offset = "0x14F9650", VA = "0x1014F9650", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004937 RID: 18743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004372")]
		[Address(RVA = "0x1014F9654", Offset = "0x14F9654", VA = "0x1014F9654", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004938 RID: 18744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004373")]
		[Address(RVA = "0x1014F9658", Offset = "0x14F9658", VA = "0x1014F9658", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004939 RID: 18745 RVA: 0x0001A6E8 File Offset: 0x000188E8
		[Token(Token = "0x6004374")]
		[Address(RVA = "0x1014F9660", Offset = "0x14F9660", VA = "0x1014F9660", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600493A RID: 18746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004375")]
		[Address(RVA = "0x1014F9668", Offset = "0x14F9668", VA = "0x1014F9668")]
		public void SetEnableObjInput(bool flg)
		{
		}

		// Token: 0x0600493B RID: 18747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004376")]
		[Address(RVA = "0x1014F966C", Offset = "0x14F966C", VA = "0x1014F966C")]
		public MenuUI()
		{
		}
	}
}
