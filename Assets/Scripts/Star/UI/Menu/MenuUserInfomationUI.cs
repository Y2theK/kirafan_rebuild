﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F32 RID: 3890
	[Token(Token = "0x2000A47")]
	[StructLayout(3)]
	public class MenuUserInfomationUI : MenuUIBase
	{
		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x0600493C RID: 18748 RVA: 0x0001A700 File Offset: 0x00018900
		// (set) Token: 0x0600493D RID: 18749 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004D6")]
		public bool m_IsDirtyUserData
		{
			[Token(Token = "0x6004377")]
			[Address(RVA = "0x1014F9674", Offset = "0x14F9674", VA = "0x1014F9674")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004378")]
			[Address(RVA = "0x1014F967C", Offset = "0x14F967C", VA = "0x1014F967C")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x0600493E RID: 18750 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004379")]
		[Address(RVA = "0x1014F9684", Offset = "0x14F9684", VA = "0x1014F9684")]
		public string GetInputUserName()
		{
			return null;
		}

		// Token: 0x0600493F RID: 18751 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600437A")]
		[Address(RVA = "0x1014F96BC", Offset = "0x14F96BC", VA = "0x1014F96BC")]
		public string GetInputCommentName()
		{
			return null;
		}

		// Token: 0x06004940 RID: 18752 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600437B")]
		[Address(RVA = "0x1014F96F4", Offset = "0x14F96F4", VA = "0x1014F96F4")]
		public long[] GetFavoriteCharaList()
		{
			return null;
		}

		// Token: 0x06004941 RID: 18753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600437C")]
		[Address(RVA = "0x1014F96FC", Offset = "0x14F96FC", VA = "0x1014F96FC")]
		public void Setup()
		{
		}

		// Token: 0x06004942 RID: 18754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600437D")]
		[Address(RVA = "0x1014F9CBC", Offset = "0x14F9CBC", VA = "0x1014F9CBC")]
		private void UpdateCharaIllust()
		{
		}

		// Token: 0x06004943 RID: 18755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600437E")]
		[Address(RVA = "0x1014F9F10", Offset = "0x14F9F10", VA = "0x1014F9F10")]
		public void Refresh()
		{
		}

		// Token: 0x06004944 RID: 18756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600437F")]
		[Address(RVA = "0x1014FA058", Offset = "0x14FA058", VA = "0x1014FA058")]
		private void Update()
		{
		}

		// Token: 0x06004945 RID: 18757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004380")]
		[Address(RVA = "0x1014FA1CC", Offset = "0x14FA1CC", VA = "0x1014FA1CC")]
		private void ChangeStep(MenuUserInfomationUI.eStep step)
		{
		}

		// Token: 0x06004946 RID: 18758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004381")]
		[Address(RVA = "0x1014FA2E0", Offset = "0x14FA2E0", VA = "0x1014FA2E0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004947 RID: 18759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004382")]
		[Address(RVA = "0x1014FA39C", Offset = "0x14FA39C", VA = "0x1014FA39C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004948 RID: 18760 RVA: 0x0001A718 File Offset: 0x00018918
		[Token(Token = "0x6004383")]
		[Address(RVA = "0x1014FA458", Offset = "0x14FA458", VA = "0x1014FA458", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004949 RID: 18761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004384")]
		[Address(RVA = "0x1014FA468", Offset = "0x14FA468", VA = "0x1014FA468")]
		public void OnCopyButtonCallBack()
		{
		}

		// Token: 0x0600494A RID: 18762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004385")]
		[Address(RVA = "0x1014FA500", Offset = "0x14FA500", VA = "0x1014FA500")]
		public void OnClickUserNameCallBack()
		{
		}

		// Token: 0x0600494B RID: 18763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004386")]
		[Address(RVA = "0x1014FA5A4", Offset = "0x14FA5A4", VA = "0x1014FA5A4")]
		public void OnDecideUserNameCallBack()
		{
		}

		// Token: 0x0600494C RID: 18764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004387")]
		[Address(RVA = "0x1014FA630", Offset = "0x14FA630", VA = "0x1014FA630")]
		public void OnClickUserCommentCallBack()
		{
		}

		// Token: 0x0600494D RID: 18765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004388")]
		[Address(RVA = "0x1014FA6D4", Offset = "0x14FA6D4", VA = "0x1014FA6D4")]
		public void OnDecideUserCommentCallBack()
		{
		}

		// Token: 0x0600494E RID: 18766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004389")]
		[Address(RVA = "0x1014FA760", Offset = "0x14FA760", VA = "0x1014FA760")]
		public void OnClickCharIcon(int index, long mngId)
		{
		}

		// Token: 0x0600494F RID: 18767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438A")]
		[Address(RVA = "0x1014FA7F0", Offset = "0x14FA7F0", VA = "0x1014FA7F0")]
		public void OnEndCharaListCallBack()
		{
		}

		// Token: 0x06004950 RID: 18768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438B")]
		[Address(RVA = "0x1014FA82C", Offset = "0x14FA82C", VA = "0x1014FA82C")]
		public void OnClickChara(long mngId)
		{
		}

		// Token: 0x06004951 RID: 18769 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438C")]
		[Address(RVA = "0x1014FA9A4", Offset = "0x14FA9A4", VA = "0x1014FA9A4")]
		public void OnClickRemove()
		{
		}

		// Token: 0x06004952 RID: 18770 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438D")]
		[Address(RVA = "0x1014FAB14", Offset = "0x14FAB14", VA = "0x1014FAB14")]
		public void OnClickAchvSetting()
		{
		}

		// Token: 0x06004953 RID: 18771 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438E")]
		[Address(RVA = "0x1014FABD0", Offset = "0x14FABD0", VA = "0x1014FABD0")]
		public void AchievementGetAllCallback()
		{
		}

		// Token: 0x06004954 RID: 18772 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600438F")]
		[Address(RVA = "0x1014FAE7C", Offset = "0x14FAE7C", VA = "0x1014FAE7C")]
		public void OnCloseAchvSetting()
		{
		}

		// Token: 0x06004955 RID: 18773 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004390")]
		[Address(RVA = "0x1014FAFE4", Offset = "0x14FAFE4", VA = "0x1014FAFE4")]
		public MenuUserInfomationUI()
		{
		}

		// Token: 0x04005B3B RID: 23355
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004015")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04005B3C RID: 23356
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004016")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005B3D RID: 23357
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004017")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04005B3E RID: 23358
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004018")]
		private MenuUserInfomationUI.eStep m_Step;

		// Token: 0x04005B3F RID: 23359
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004019")]
		[SerializeField]
		private UIGroup m_UserNameInputGroup;

		// Token: 0x04005B40 RID: 23360
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400401A")]
		[SerializeField]
		private TextInput m_UserNameInput;

		// Token: 0x04005B41 RID: 23361
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400401B")]
		[SerializeField]
		private Text m_UserName;

		// Token: 0x04005B42 RID: 23362
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400401C")]
		[SerializeField]
		private UIGroup m_UserCommentInputGroup;

		// Token: 0x04005B43 RID: 23363
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400401D")]
		[SerializeField]
		private TextInput m_UserCommentInput;

		// Token: 0x04005B44 RID: 23364
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400401E")]
		[SerializeField]
		private Text m_UserComment;

		// Token: 0x04005B45 RID: 23365
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400401F")]
		[SerializeField]
		private Text m_MyCode;

		// Token: 0x04005B46 RID: 23366
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004020")]
		[SerializeField]
		private ReflectFrameImage m_Reflection;

		// Token: 0x04005B47 RID: 23367
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004021")]
		[SerializeField]
		private FavoriteCharaIcon[] m_favoriteCharaIcon;

		// Token: 0x04005B48 RID: 23368
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004022")]
		[SerializeField]
		private CharaListWindow m_charaListWindow;

		// Token: 0x04005B49 RID: 23369
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004023")]
		[SerializeField]
		private AchievementImage m_ImgUserAchv;

		// Token: 0x04005B4A RID: 23370
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004024")]
		[SerializeField]
		private CustomButton m_BtnACHVSetting;

		// Token: 0x04005B4B RID: 23371
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004025")]
		[SerializeField]
		private GameObject m_AchvListWindowPrefab;

		// Token: 0x04005B4C RID: 23372
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004026")]
		[SerializeField]
		private PrefabCloner m_AchvBadgeCloner;

		// Token: 0x04005B4D RID: 23373
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004027")]
		private AchievementListWindow m_AchvListWindow;

		// Token: 0x04005B4E RID: 23374
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004028")]
		private long[] m_listCharaMngID;

		// Token: 0x04005B4F RID: 23375
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004029")]
		private int m_selectedIndex;

		// Token: 0x02000F33 RID: 3891
		[Token(Token = "0x20011B1")]
		private enum eStep
		{
			// Token: 0x04005B52 RID: 23378
			[Token(Token = "0x4006E70")]
			Hide,
			// Token: 0x04005B53 RID: 23379
			[Token(Token = "0x4006E71")]
			In,
			// Token: 0x04005B54 RID: 23380
			[Token(Token = "0x4006E72")]
			Idle,
			// Token: 0x04005B55 RID: 23381
			[Token(Token = "0x4006E73")]
			Out,
			// Token: 0x04005B56 RID: 23382
			[Token(Token = "0x4006E74")]
			End
		}
	}
}
