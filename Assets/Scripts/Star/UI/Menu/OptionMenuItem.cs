﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F34 RID: 3892
	[Token(Token = "0x2000A48")]
	[StructLayout(3)]
	public class OptionMenuItem : MonoBehaviour
	{
		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06004956 RID: 18774 RVA: 0x0001A730 File Offset: 0x00018930
		[Token(Token = "0x170004D7")]
		public OptionMenuItem.eType Type
		{
			[Token(Token = "0x6004391")]
			[Address(RVA = "0x1014FAFEC", Offset = "0x14FAFEC", VA = "0x1014FAFEC")]
			get
			{
				return OptionMenuItem.eType.Title;
			}
		}

		// Token: 0x140000D2 RID: 210
		// (add) Token: 0x06004957 RID: 18775 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004958 RID: 18776 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D2")]
		public event Action OnValueChanged
		{
			[Token(Token = "0x6004392")]
			[Address(RVA = "0x1014FAFF4", Offset = "0x14FAFF4", VA = "0x1014FAFF4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004393")]
			[Address(RVA = "0x1014FB100", Offset = "0x14FB100", VA = "0x1014FB100")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004959 RID: 18777 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004394")]
		[Address(RVA = "0x1014FB20C", Offset = "0x14FB20C", VA = "0x1014FB20C")]
		private void Start()
		{
		}

		// Token: 0x0600495A RID: 18778 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004395")]
		[Address(RVA = "0x1014FB2E4", Offset = "0x14FB2E4", VA = "0x1014FB2E4")]
		public void RegisterSliderStartingNormalizedValue(float value)
		{
		}

		// Token: 0x0600495B RID: 18779 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004396")]
		[Address(RVA = "0x1014FB410", Offset = "0x14FB410", VA = "0x1014FB410")]
		public void RegisterSliderDefaultNormalizedValue(float value)
		{
		}

		// Token: 0x0600495C RID: 18780 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004397")]
		[Address(RVA = "0x1014FB488", Offset = "0x14FB488", VA = "0x1014FB488")]
		public void RegisterToggleStartingValue(bool value)
		{
		}

		// Token: 0x0600495D RID: 18781 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004398")]
		[Address(RVA = "0x1014FB520", Offset = "0x14FB520", VA = "0x1014FB520")]
		public void RegisterToggleDefaultValue(bool value)
		{
		}

		// Token: 0x0600495E RID: 18782 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004399")]
		[Address(RVA = "0x1014FB598", Offset = "0x14FB598", VA = "0x1014FB598")]
		public void RegisterSelectorStartingValue(int value)
		{
		}

		// Token: 0x0600495F RID: 18783 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600439A")]
		[Address(RVA = "0x1014FB6C0", Offset = "0x14FB6C0", VA = "0x1014FB6C0")]
		public void RegisterSelectorDefaultValue(int value)
		{
		}

		// Token: 0x06004960 RID: 18784 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600439B")]
		[Address(RVA = "0x1014FB738", Offset = "0x14FB738", VA = "0x1014FB738")]
		public void Revert()
		{
		}

		// Token: 0x06004961 RID: 18785 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600439C")]
		[Address(RVA = "0x1014FBA28", Offset = "0x14FBA28", VA = "0x1014FBA28")]
		public void Restore()
		{
		}

		// Token: 0x06004962 RID: 18786 RVA: 0x0001A748 File Offset: 0x00018948
		[Token(Token = "0x600439D")]
		[Address(RVA = "0x1014FBC5C", Offset = "0x14FBC5C", VA = "0x1014FBC5C")]
		public bool IsMatchValueAndStartingValue()
		{
			return default(bool);
		}

		// Token: 0x06004963 RID: 18787 RVA: 0x0001A760 File Offset: 0x00018960
		[Token(Token = "0x600439E")]
		[Address(RVA = "0x1014FBDA4", Offset = "0x14FBDA4", VA = "0x1014FBDA4")]
		public bool IsMatchValueAndDefaultValue()
		{
			return default(bool);
		}

		// Token: 0x06004964 RID: 18788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600439F")]
		[Address(RVA = "0x1014FBEEC", Offset = "0x14FBEEC", VA = "0x1014FBEEC")]
		public void SetTitleText(string text)
		{
		}

		// Token: 0x06004965 RID: 18789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A0")]
		[Address(RVA = "0x1014FBF2C", Offset = "0x14FBF2C", VA = "0x1014FBF2C")]
		public void SetSelectorTexts(eText_CommonDB[] texts)
		{
		}

		// Token: 0x06004966 RID: 18790 RVA: 0x0001A778 File Offset: 0x00018978
		[Token(Token = "0x60043A1")]
		[Address(RVA = "0x1014FBF34", Offset = "0x14FBF34", VA = "0x1014FBF34")]
		public float GetSliderNormalizedValue()
		{
			return 0f;
		}

		// Token: 0x06004967 RID: 18791 RVA: 0x0001A790 File Offset: 0x00018990
		[Token(Token = "0x60043A2")]
		[Address(RVA = "0x1014FBFE0", Offset = "0x14FBFE0", VA = "0x1014FBFE0")]
		public float GetSliderValue()
		{
			return 0f;
		}

		// Token: 0x06004968 RID: 18792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A3")]
		[Address(RVA = "0x1014FB368", Offset = "0x14FB368", VA = "0x1014FB368")]
		public void SetSliderNormalizedValue(float value)
		{
		}

		// Token: 0x06004969 RID: 18793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A4")]
		[Address(RVA = "0x1014FC08C", Offset = "0x14FC08C", VA = "0x1014FC08C")]
		[Obsolete]
		public void SetNormalizedSliderValue(float value)
		{
		}

		// Token: 0x0600496A RID: 18794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A5")]
		[Address(RVA = "0x1014FC090", Offset = "0x14FC090", VA = "0x1014FC090")]
		public void SetSliderValue(float value)
		{
		}

		// Token: 0x0600496B RID: 18795 RVA: 0x0001A7A8 File Offset: 0x000189A8
		[Token(Token = "0x60043A6")]
		[Address(RVA = "0x1014FC138", Offset = "0x14FC138", VA = "0x1014FC138")]
		public bool GetToggleValue()
		{
			return default(bool);
		}

		// Token: 0x0600496C RID: 18796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A7")]
		[Address(RVA = "0x1014FB96C", Offset = "0x14FB96C", VA = "0x1014FB96C")]
		public void SetToggleValue(bool value)
		{
		}

		// Token: 0x0600496D RID: 18797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A8")]
		[Address(RVA = "0x1014FC1E4", Offset = "0x14FC1E4", VA = "0x1014FC1E4")]
		public void OnClickToggleButtonCallBack()
		{
		}

		// Token: 0x0600496E RID: 18798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043A9")]
		[Address(RVA = "0x1014FC1F0", Offset = "0x14FC1F0", VA = "0x1014FC1F0")]
		public void OnValueChangedCallBack()
		{
		}

		// Token: 0x0600496F RID: 18799 RVA: 0x0001A7C0 File Offset: 0x000189C0
		[Token(Token = "0x60043AA")]
		[Address(RVA = "0x1014FC1FC", Offset = "0x14FC1FC", VA = "0x1014FC1FC")]
		public int GetSelectorValue()
		{
			return 0;
		}

		// Token: 0x06004970 RID: 18800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043AB")]
		[Address(RVA = "0x1014FC28C", Offset = "0x14FC28C", VA = "0x1014FC28C")]
		public void SetSelectorValue(int value)
		{
		}

		// Token: 0x06004971 RID: 18801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043AC")]
		[Address(RVA = "0x1014FC3D8", Offset = "0x14FC3D8", VA = "0x1014FC3D8")]
		public void OnClickSelectorButton(int dir)
		{
		}

		// Token: 0x06004972 RID: 18802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043AD")]
		[Address(RVA = "0x1014FC464", Offset = "0x14FC464", VA = "0x1014FC464")]
		public OptionMenuItem()
		{
		}

		// Token: 0x04005B57 RID: 23383
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400402B")]
		[SerializeField]
		private OptionMenuItem.eType m_Type;

		// Token: 0x04005B58 RID: 23384
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400402C")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04005B59 RID: 23385
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400402D")]
		[SerializeField]
		private CustomSlider m_Slider;

		// Token: 0x04005B5A RID: 23386
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400402E")]
		[SerializeField]
		private ToggleSwitch m_ToggleSwitch;

		// Token: 0x04005B5B RID: 23387
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400402F")]
		[SerializeField]
		private Text m_SelectorText;

		// Token: 0x04005B5C RID: 23388
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004030")]
		private float? m_SliderStartingValue;

		// Token: 0x04005B5D RID: 23389
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004031")]
		private float? m_SliderDefaultValue;

		// Token: 0x04005B5E RID: 23390
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004032")]
		private bool? m_ToggleStartingValue;

		// Token: 0x04005B5F RID: 23391
		[Cpp2IlInjected.FieldOffset(Offset = "0x52")]
		[Token(Token = "0x4004033")]
		private bool? m_ToggleDefaultValue;

		// Token: 0x04005B60 RID: 23392
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004034")]
		private int? m_SelectorStartingValue;

		// Token: 0x04005B61 RID: 23393
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4004035")]
		private int? m_SelectorDefaultValue;

		// Token: 0x04005B62 RID: 23394
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004036")]
		private int m_SelectorSelectedIdx;

		// Token: 0x04005B63 RID: 23395
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004037")]
		private eText_CommonDB[] m_SelectorStrings;

		// Token: 0x02000F35 RID: 3893
		[Token(Token = "0x20011B2")]
		public enum eType
		{
			// Token: 0x04005B66 RID: 23398
			[Token(Token = "0x4006E76")]
			Title,
			// Token: 0x04005B67 RID: 23399
			[Token(Token = "0x4006E77")]
			Slider,
			// Token: 0x04005B68 RID: 23400
			[Token(Token = "0x4006E78")]
			Toggle,
			// Token: 0x04005B69 RID: 23401
			[Token(Token = "0x4006E79")]
			Toggle2,
			// Token: 0x04005B6A RID: 23402
			[Token(Token = "0x4006E7A")]
			Selector
		}
	}
}
