﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F36 RID: 3894
	[Token(Token = "0x2000A49")]
	[StructLayout(3)]
	public class OptionWindow : UIGroup
	{
		// Token: 0x06004973 RID: 18803 RVA: 0x0001A7D8 File Offset: 0x000189D8
		[Token(Token = "0x60043AE")]
		[Address(RVA = "0x1014FC46C", Offset = "0x14FC46C", VA = "0x1014FC46C")]
		public static OptionWindow.eCategory ConverteOptionItemToeCategory(OptionWindow.eOptionItem itemType)
		{
			return OptionWindow.eCategory.Sound;
		}

		// Token: 0x06004974 RID: 18804 RVA: 0x0001A7F0 File Offset: 0x000189F0
		[Token(Token = "0x60043AF")]
		[Address(RVA = "0x1014FC490", Offset = "0x14FC490", VA = "0x1014FC490")]
		public static OptionMenuItem.eType ConverteOptionItemToOptionMenuItemeType(OptionWindow.eOptionItem itemType)
		{
			return OptionMenuItem.eType.Title;
		}

		// Token: 0x06004975 RID: 18805 RVA: 0x0001A808 File Offset: 0x00018A08
		[Token(Token = "0x60043B0")]
		[Address(RVA = "0x1014FC4B0", Offset = "0x14FC4B0", VA = "0x1014FC4B0")]
		public static eText_CommonDB ConverteOptionItemToeText_CommonDB(OptionWindow.eOptionItem itemType)
		{
			return eText_CommonDB.GameTitle;
		}

		// Token: 0x06004976 RID: 18806 RVA: 0x0001A820 File Offset: 0x00018A20
		[Token(Token = "0x60043B1")]
		[Address(RVA = "0x1014FC4D4", Offset = "0x14FC4D4", VA = "0x1014FC4D4")]
		public static bool IsBattleOption(OptionWindow.eOptionItem itemType)
		{
			return default(bool);
		}

		// Token: 0x06004977 RID: 18807 RVA: 0x0001A838 File Offset: 0x00018A38
		[Token(Token = "0x60043B2")]
		[Address(RVA = "0x1014FC4F4", Offset = "0x14FC4F4", VA = "0x1014FC4F4")]
		public static SoundDefine.eCategory ConverteOptionItemToSoundDefineeCategory(OptionWindow.eOptionItem itemType)
		{
			return SoundDefine.eCategory.BGM;
		}

		// Token: 0x06004978 RID: 18808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B3")]
		[Address(RVA = "0x1014F5B00", Offset = "0x14F5B00", VA = "0x1014F5B00")]
		public void Open(BattleOption battleOption)
		{
		}

		// Token: 0x06004979 RID: 18809 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B4")]
		[Address(RVA = "0x1014FCA5C", Offset = "0x14FCA5C", VA = "0x1014FCA5C", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x0600497A RID: 18810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B5")]
		[Address(RVA = "0x1014FCBC4", Offset = "0x14FCBC4", VA = "0x1014FCBC4")]
		private void OnChangeBGMVolume()
		{
		}

		// Token: 0x0600497B RID: 18811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B6")]
		[Address(RVA = "0x1014FCC04", Offset = "0x14FCC04", VA = "0x1014FCC04")]
		private void OnChangeSEVolume()
		{
		}

		// Token: 0x0600497C RID: 18812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B7")]
		[Address(RVA = "0x1014FCC20", Offset = "0x14FCC20", VA = "0x1014FCC20")]
		private void OnChangeVoiceVolume()
		{
		}

		// Token: 0x0600497D RID: 18813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B8")]
		[Address(RVA = "0x1014FCC3C", Offset = "0x14FCC3C", VA = "0x1014FCC3C")]
		private void OnChangeMovieVolume()
		{
		}

		// Token: 0x0600497E RID: 18814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043B9")]
		[Address(RVA = "0x1014FCBE0", Offset = "0x14FCBE0", VA = "0x1014FCBE0")]
		private void OnChangeVolume(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x0600497F RID: 18815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BA")]
		[Address(RVA = "0x1014FCD34", Offset = "0x14FCD34", VA = "0x1014FCD34")]
		private void OnValueChanged()
		{
		}

		// Token: 0x06004980 RID: 18816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BB")]
		[Address(RVA = "0x1014FCD40", Offset = "0x14FCD40", VA = "0x1014FCD40")]
		private void SetDirty()
		{
		}

		// Token: 0x06004981 RID: 18817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BC")]
		[Address(RVA = "0x1014FC8F4", Offset = "0x14FC8F4", VA = "0x1014FC8F4")]
		private void RefreshRestoreButtonInteractable()
		{
		}

		// Token: 0x06004982 RID: 18818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BD")]
		[Address(RVA = "0x1014FCD4C", Offset = "0x14FCD4C", VA = "0x1014FCD4C")]
		public void Decide()
		{
		}

		// Token: 0x06004983 RID: 18819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BE")]
		[Address(RVA = "0x1014FD740", Offset = "0x14FD740", VA = "0x1014FD740")]
		public void Cancel()
		{
		}

		// Token: 0x06004984 RID: 18820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043BF")]
		[Address(RVA = "0x1014FD7CC", Offset = "0x14FD7CC", VA = "0x1014FD7CC")]
		private void ApplyDefault()
		{
		}

		// Token: 0x06004985 RID: 18821 RVA: 0x0001A850 File Offset: 0x00018A50
		[Token(Token = "0x60043C0")]
		[Address(RVA = "0x1014FC504", Offset = "0x14FC504", VA = "0x1014FC504")]
		private float GetSliderStartingNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return 0f;
		}

		// Token: 0x06004986 RID: 18822 RVA: 0x0001A868 File Offset: 0x00018A68
		[Token(Token = "0x60043C1")]
		[Address(RVA = "0x1014FD9C4", Offset = "0x14FD9C4", VA = "0x1014FD9C4")]
		private float GetSliderStartingNormalizedValue(int nItemType)
		{
			return 0f;
		}

		// Token: 0x06004987 RID: 18823 RVA: 0x0001A880 File Offset: 0x00018A80
		[Token(Token = "0x60043C2")]
		[Address(RVA = "0x1014FC654", Offset = "0x14FC654", VA = "0x1014FC654")]
		private bool GetToggleStartingValue(OptionWindow.eOptionItem itemType)
		{
			return default(bool);
		}

		// Token: 0x06004988 RID: 18824 RVA: 0x0001A898 File Offset: 0x00018A98
		[Token(Token = "0x60043C3")]
		[Address(RVA = "0x1014FDB4C", Offset = "0x14FDB4C", VA = "0x1014FDB4C")]
		private bool GetToggleStartingValue(int nItemType)
		{
			return default(bool);
		}

		// Token: 0x06004989 RID: 18825 RVA: 0x0001A8B0 File Offset: 0x00018AB0
		[Token(Token = "0x60043C4")]
		[Address(RVA = "0x1014FC7A4", Offset = "0x14FC7A4", VA = "0x1014FC7A4")]
		private int GetSelectorStartingValue(OptionWindow.eOptionItem itemType)
		{
			return 0;
		}

		// Token: 0x0600498A RID: 18826 RVA: 0x0001A8C8 File Offset: 0x00018AC8
		[Token(Token = "0x60043C5")]
		[Address(RVA = "0x1014FDF30", Offset = "0x14FDF30", VA = "0x1014FDF30")]
		private int GetSelectorStartingValue(int nItemType)
		{
			return 0;
		}

		// Token: 0x0600498B RID: 18827 RVA: 0x0001A8E0 File Offset: 0x00018AE0
		[Token(Token = "0x60043C6")]
		[Address(RVA = "0x1014FC5AC", Offset = "0x14FC5AC", VA = "0x1014FC5AC")]
		private float GetSliderDefaultNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return 0f;
		}

		// Token: 0x0600498C RID: 18828 RVA: 0x0001A8F8 File Offset: 0x00018AF8
		[Token(Token = "0x60043C7")]
		[Address(RVA = "0x1014FDFF8", Offset = "0x14FDFF8", VA = "0x1014FDFF8")]
		private float GetSliderDefaultNormalizedValue(int nItemType)
		{
			return 0f;
		}

		// Token: 0x0600498D RID: 18829 RVA: 0x0001A910 File Offset: 0x00018B10
		[Token(Token = "0x60043C8")]
		[Address(RVA = "0x1014FC6FC", Offset = "0x14FC6FC", VA = "0x1014FC6FC")]
		private bool GetToggleDefaultValue(OptionWindow.eOptionItem itemType)
		{
			return default(bool);
		}

		// Token: 0x0600498E RID: 18830 RVA: 0x0001A928 File Offset: 0x00018B28
		[Token(Token = "0x60043C9")]
		[Address(RVA = "0x1014FE120", Offset = "0x14FE120", VA = "0x1014FE120")]
		private bool GetToggleDefaultValue(int nItemType)
		{
			return default(bool);
		}

		// Token: 0x0600498F RID: 18831 RVA: 0x0001A940 File Offset: 0x00018B40
		[Token(Token = "0x60043CA")]
		[Address(RVA = "0x1014FC84C", Offset = "0x14FC84C", VA = "0x1014FC84C")]
		private int GetSelectorDefaultValue(OptionWindow.eOptionItem itemType)
		{
			return 0;
		}

		// Token: 0x06004990 RID: 18832 RVA: 0x0001A958 File Offset: 0x00018B58
		[Token(Token = "0x60043CB")]
		[Address(RVA = "0x1014FE3EC", Offset = "0x14FE3EC", VA = "0x1014FE3EC")]
		private int GetSelectorDefaultValue(int nItemType)
		{
			return 0;
		}

		// Token: 0x06004991 RID: 18833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043CC")]
		[Address(RVA = "0x1014FE468", Offset = "0x14FE468", VA = "0x1014FE468")]
		private void RegisterSliderStartingNormalizedValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x06004992 RID: 18834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043CD")]
		[Address(RVA = "0x1014FE46C", Offset = "0x14FE46C", VA = "0x1014FE46C")]
		private void RegisterSliderStartingNormalizedValue(int nItemType)
		{
		}

		// Token: 0x06004993 RID: 18835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043CE")]
		[Address(RVA = "0x1014FE518", Offset = "0x14FE518", VA = "0x1014FE518")]
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x06004994 RID: 18836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043CF")]
		[Address(RVA = "0x1014FE4F0", Offset = "0x14FE4F0", VA = "0x1014FE4F0")]
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, int nItemType)
		{
		}

		// Token: 0x06004995 RID: 18837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D0")]
		[Address(RVA = "0x1014FC508", Offset = "0x14FC508", VA = "0x1014FC508")]
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, float value)
		{
		}

		// Token: 0x06004996 RID: 18838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D1")]
		[Address(RVA = "0x1014FE540", Offset = "0x14FE540", VA = "0x1014FE540")]
		private void RegisterToggleStartingValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x06004997 RID: 18839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D2")]
		[Address(RVA = "0x1014FE544", Offset = "0x14FE544", VA = "0x1014FE544")]
		private void RegisterToggleStartingValue(int nItemType)
		{
		}

		// Token: 0x06004998 RID: 18840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D3")]
		[Address(RVA = "0x1014FE600", Offset = "0x14FE600", VA = "0x1014FE600")]
		private void RegisterToggleStartingValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x06004999 RID: 18841 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D4")]
		[Address(RVA = "0x1014FE5D4", Offset = "0x14FE5D4", VA = "0x1014FE5D4")]
		private void RegisterToggleStartingValue(OptionMenuItem item, int nItemType)
		{
		}

		// Token: 0x0600499A RID: 18842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D5")]
		[Address(RVA = "0x1014FC658", Offset = "0x14FC658", VA = "0x1014FC658")]
		private void RegisterToggleStartingValue(OptionMenuItem item, bool value)
		{
		}

		// Token: 0x0600499B RID: 18843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D6")]
		[Address(RVA = "0x1014FC7A8", Offset = "0x14FC7A8", VA = "0x1014FC7A8")]
		private void RegisterSelectorStartingValue(OptionMenuItem item, int value)
		{
		}

		// Token: 0x0600499C RID: 18844 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D7")]
		[Address(RVA = "0x1014FE62C", Offset = "0x14FE62C", VA = "0x1014FE62C")]
		private void RegisterSliderDefaultNormalizedValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x0600499D RID: 18845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D8")]
		[Address(RVA = "0x1014FE630", Offset = "0x14FE630", VA = "0x1014FE630")]
		private void RegisterSliderDefaultNormalizedValue(int nItemType)
		{
		}

		// Token: 0x0600499E RID: 18846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043D9")]
		[Address(RVA = "0x1014FE6DC", Offset = "0x14FE6DC", VA = "0x1014FE6DC")]
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x0600499F RID: 18847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DA")]
		[Address(RVA = "0x1014FE6B4", Offset = "0x14FE6B4", VA = "0x1014FE6B4")]
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, int nItemType)
		{
		}

		// Token: 0x060049A0 RID: 18848 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DB")]
		[Address(RVA = "0x1014FC5B0", Offset = "0x14FC5B0", VA = "0x1014FC5B0")]
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, float value)
		{
		}

		// Token: 0x060049A1 RID: 18849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DC")]
		[Address(RVA = "0x1014FE704", Offset = "0x14FE704", VA = "0x1014FE704")]
		private void RegisterToggleDefaultValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049A2 RID: 18850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DD")]
		[Address(RVA = "0x1014FE708", Offset = "0x14FE708", VA = "0x1014FE708")]
		private void RegisterToggleDefaultValue(int nItemType)
		{
		}

		// Token: 0x060049A3 RID: 18851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DE")]
		[Address(RVA = "0x1014FE7C0", Offset = "0x14FE7C0", VA = "0x1014FE7C0")]
		private void RegisterToggleDefaultValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049A4 RID: 18852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043DF")]
		[Address(RVA = "0x1014FE794", Offset = "0x14FE794", VA = "0x1014FE794")]
		private void RegisterToggleDefaultValue(OptionMenuItem item, int nItemType)
		{
		}

		// Token: 0x060049A5 RID: 18853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E0")]
		[Address(RVA = "0x1014FC700", Offset = "0x14FC700", VA = "0x1014FC700")]
		private void RegisterToggleDefaultValue(OptionMenuItem item, bool value)
		{
		}

		// Token: 0x060049A6 RID: 18854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E1")]
		[Address(RVA = "0x1014FC850", Offset = "0x14FC850", VA = "0x1014FC850")]
		private void RegisterSelectorDefaultValue(OptionMenuItem item, int value)
		{
		}

		// Token: 0x060049A7 RID: 18855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E2")]
		[Address(RVA = "0x1014FE7EC", Offset = "0x14FE7EC", VA = "0x1014FE7EC")]
		private void CommitOptionDataSliderValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049A8 RID: 18856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E3")]
		[Address(RVA = "0x1014FCF0C", Offset = "0x14FCF0C", VA = "0x1014FCF0C")]
		private void CommitOptionDataSliderValue(int nItemType)
		{
		}

		// Token: 0x060049A9 RID: 18857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E4")]
		[Address(RVA = "0x1014FE86C", Offset = "0x14FE86C", VA = "0x1014FE86C")]
		private void CommitOptionDataToggleValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049AA RID: 18858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E5")]
		[Address(RVA = "0x1014FD124", Offset = "0x14FD124", VA = "0x1014FD124")]
		private void CommitOptionDataToggleValue(int nItemType)
		{
		}

		// Token: 0x060049AB RID: 18859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E6")]
		[Address(RVA = "0x1014FD650", Offset = "0x14FD650", VA = "0x1014FD650")]
		private void CommitOptionDataSelectorValue(int nItemType)
		{
		}

		// Token: 0x060049AC RID: 18860 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E7")]
		[Address(RVA = "0x1014FD7A8", Offset = "0x14FD7A8", VA = "0x1014FD7A8")]
		private void RevertSoundManagerSliderValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049AD RID: 18861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E8")]
		[Address(RVA = "0x1014FE96C", Offset = "0x14FE96C", VA = "0x1014FE96C")]
		private void RevertSoundManagerSliderValue(int nItemType)
		{
		}

		// Token: 0x060049AE RID: 18862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043E9")]
		[Address(RVA = "0x1014FE994", Offset = "0x14FE994", VA = "0x1014FE994")]
		private void RevertSliderValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049AF RID: 18863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043EA")]
		[Address(RVA = "0x1014FE998", Offset = "0x14FE998", VA = "0x1014FE998")]
		private void RevertSliderValue(int nItemType)
		{
		}

		// Token: 0x060049B0 RID: 18864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043EB")]
		[Address(RVA = "0x1014FE9F8", Offset = "0x14FE9F8", VA = "0x1014FE9F8")]
		private void RevertToggleValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049B1 RID: 18865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043EC")]
		[Address(RVA = "0x1014FE9FC", Offset = "0x14FE9FC", VA = "0x1014FE9FC")]
		private void RevertToggleValue(int nItemType)
		{
		}

		// Token: 0x060049B2 RID: 18866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043ED")]
		[Address(RVA = "0x1014FEA00", Offset = "0x14FEA00", VA = "0x1014FEA00")]
		private void RevertValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049B3 RID: 18867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043EE")]
		[Address(RVA = "0x1014FE99C", Offset = "0x14FE99C", VA = "0x1014FE99C")]
		private void RevertValue(int nItemType)
		{
		}

		// Token: 0x060049B4 RID: 18868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043EF")]
		[Address(RVA = "0x1014FEA04", Offset = "0x14FEA04", VA = "0x1014FEA04")]
		private void RevertValue(OptionMenuItem item)
		{
		}

		// Token: 0x060049B5 RID: 18869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F0")]
		[Address(RVA = "0x1014FEA94", Offset = "0x14FEA94", VA = "0x1014FEA94")]
		private void RestoreSliderValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049B6 RID: 18870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F1")]
		[Address(RVA = "0x1014FD8F4", Offset = "0x14FD8F4", VA = "0x1014FD8F4")]
		private void RestoreSliderValue(int nItemType)
		{
		}

		// Token: 0x060049B7 RID: 18871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F2")]
		[Address(RVA = "0x1014FEAF4", Offset = "0x14FEAF4", VA = "0x1014FEAF4")]
		private void RestoreToggleValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049B8 RID: 18872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F3")]
		[Address(RVA = "0x1014FD938", Offset = "0x14FD938", VA = "0x1014FD938")]
		private void RestoreToggleValue(int nItemType)
		{
		}

		// Token: 0x060049B9 RID: 18873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F4")]
		[Address(RVA = "0x1014FEAF8", Offset = "0x14FEAF8", VA = "0x1014FEAF8")]
		private void RestoreSelectorValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049BA RID: 18874 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F5")]
		[Address(RVA = "0x1014FD980", Offset = "0x14FD980", VA = "0x1014FD980")]
		private void RestoreSelectorValue(int nItemType)
		{
		}

		// Token: 0x060049BB RID: 18875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F6")]
		[Address(RVA = "0x1014FEAFC", Offset = "0x14FEAFC", VA = "0x1014FEAFC")]
		private void RestoreValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049BC RID: 18876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F7")]
		[Address(RVA = "0x1014FEA98", Offset = "0x14FEA98", VA = "0x1014FEA98")]
		private void RestoreValue(int nItemType)
		{
		}

		// Token: 0x060049BD RID: 18877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F8")]
		[Address(RVA = "0x1014FEB00", Offset = "0x14FEB00", VA = "0x1014FEB00")]
		private void RestoreValue(OptionMenuItem item)
		{
		}

		// Token: 0x060049BE RID: 18878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043F9")]
		[Address(RVA = "0x1014FCC74", Offset = "0x14FCC74", VA = "0x1014FCC74")]
		private void SetSoundManagerSliderValue(OptionWindow.eOptionItem itemType, float value)
		{
		}

		// Token: 0x060049BF RID: 18879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FA")]
		[Address(RVA = "0x1014FE990", Offset = "0x14FE990", VA = "0x1014FE990")]
		private void SetSoundManagerSliderValue(int nItemType, float value)
		{
		}

		// Token: 0x060049C0 RID: 18880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FB")]
		[Address(RVA = "0x1014FEB90", Offset = "0x14FEB90", VA = "0x1014FEB90")]
		private void IsMatchValueAndStartingValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049C1 RID: 18881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FC")]
		[Address(RVA = "0x1014FEB94", Offset = "0x14FEB94", VA = "0x1014FEB94")]
		private void IsMatchValueAndStartingValue(int nItemType)
		{
		}

		// Token: 0x060049C2 RID: 18882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FD")]
		[Address(RVA = "0x1014FEBF0", Offset = "0x14FEBF0", VA = "0x1014FEBF0")]
		private void IsMatchValueAndStartingValue(OptionMenuItem item)
		{
		}

		// Token: 0x060049C3 RID: 18883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FE")]
		[Address(RVA = "0x1014FEC80", Offset = "0x14FEC80", VA = "0x1014FEC80")]
		private void IsMatchValueAndDefaultValue(OptionWindow.eOptionItem itemType)
		{
		}

		// Token: 0x060049C4 RID: 18884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60043FF")]
		[Address(RVA = "0x1014FEC84", Offset = "0x14FEC84", VA = "0x1014FEC84")]
		private void IsMatchValueAndDefaultValue(int nItemType)
		{
		}

		// Token: 0x060049C5 RID: 18885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004400")]
		[Address(RVA = "0x1014FECE0", Offset = "0x14FECE0", VA = "0x1014FECE0")]
		private void IsMatchValueAndDefaultValue(OptionMenuItem item)
		{
		}

		// Token: 0x060049C6 RID: 18886 RVA: 0x0001A970 File Offset: 0x00018B70
		[Token(Token = "0x6004401")]
		[Address(RVA = "0x1014FCC70", Offset = "0x14FCC70", VA = "0x1014FCC70")]
		private float GetSliderNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return 0f;
		}

		// Token: 0x060049C7 RID: 18887 RVA: 0x0001A988 File Offset: 0x00018B88
		[Token(Token = "0x6004402")]
		[Address(RVA = "0x1014FE7F0", Offset = "0x14FE7F0", VA = "0x1014FE7F0")]
		private float GetSliderNormalizedValue(int nItemType)
		{
			return 0f;
		}

		// Token: 0x060049C8 RID: 18888 RVA: 0x0001A9A0 File Offset: 0x00018BA0
		[Token(Token = "0x6004403")]
		[Address(RVA = "0x1014FED70", Offset = "0x14FED70", VA = "0x1014FED70")]
		private float GetSliderNormalizedValue(OptionMenuItem item)
		{
			return 0f;
		}

		// Token: 0x060049C9 RID: 18889 RVA: 0x0001A9B8 File Offset: 0x00018BB8
		[Token(Token = "0x6004404")]
		[Address(RVA = "0x1014FEE04", Offset = "0x14FEE04", VA = "0x1014FEE04")]
		private bool GetToggleValue(OptionWindow.eOptionItem itemType)
		{
			return default(bool);
		}

		// Token: 0x060049CA RID: 18890 RVA: 0x0001A9D0 File Offset: 0x00018BD0
		[Token(Token = "0x6004405")]
		[Address(RVA = "0x1014FE870", Offset = "0x14FE870", VA = "0x1014FE870")]
		private bool GetToggleValue(int nItemType)
		{
			return default(bool);
		}

		// Token: 0x060049CB RID: 18891 RVA: 0x0001A9E8 File Offset: 0x00018BE8
		[Token(Token = "0x6004406")]
		[Address(RVA = "0x1014FEE08", Offset = "0x14FEE08", VA = "0x1014FEE08")]
		private bool GetToggleValue(OptionMenuItem item)
		{
			return default(bool);
		}

		// Token: 0x060049CC RID: 18892 RVA: 0x0001AA00 File Offset: 0x00018C00
		[Token(Token = "0x6004407")]
		[Address(RVA = "0x1014FE8F0", Offset = "0x14FE8F0", VA = "0x1014FE8F0")]
		private int GetSelectorValue(int nItemType)
		{
			return 0;
		}

		// Token: 0x060049CD RID: 18893 RVA: 0x0001AA18 File Offset: 0x00018C18
		[Token(Token = "0x6004408")]
		[Address(RVA = "0x1014FEE9C", Offset = "0x14FEE9C", VA = "0x1014FEE9C")]
		private int GetSelectorValue(OptionMenuItem item)
		{
			return 0;
		}

		// Token: 0x060049CE RID: 18894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004409")]
		[Address(RVA = "0x1014FEF30", Offset = "0x14FEF30", VA = "0x1014FEF30")]
		private void SetSliderNormalizedValue(OptionWindow.eOptionItem itemType, float value)
		{
		}

		// Token: 0x060049CF RID: 18895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440A")]
		[Address(RVA = "0x1014FEF34", Offset = "0x14FEF34", VA = "0x1014FEF34")]
		private void SetSliderNormalizedValue(int nItemType, float value)
		{
		}

		// Token: 0x060049D0 RID: 18896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440B")]
		[Address(RVA = "0x1014FF088", Offset = "0x14FF088", VA = "0x1014FF088")]
		private void SetSliderNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType, float value)
		{
		}

		// Token: 0x060049D1 RID: 18897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440C")]
		[Address(RVA = "0x1014FEFC4", Offset = "0x14FEFC4", VA = "0x1014FEFC4")]
		private void SetSliderNormalizedValue(OptionMenuItem item, int nItemType, float value)
		{
		}

		// Token: 0x060049D2 RID: 18898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440D")]
		[Address(RVA = "0x1014FF08C", Offset = "0x14FF08C", VA = "0x1014FF08C")]
		private void SetToggleValue(OptionWindow.eOptionItem itemType, bool value)
		{
		}

		// Token: 0x060049D3 RID: 18899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440E")]
		[Address(RVA = "0x1014FF090", Offset = "0x14FF090", VA = "0x1014FF090")]
		private void SetToggleValue(int nItemType, bool value)
		{
		}

		// Token: 0x060049D4 RID: 18900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600440F")]
		[Address(RVA = "0x1014FF1D4", Offset = "0x14FF1D4", VA = "0x1014FF1D4")]
		private void SetToggleValue(OptionMenuItem item, OptionWindow.eOptionItem itemType, bool value)
		{
		}

		// Token: 0x060049D5 RID: 18901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004410")]
		[Address(RVA = "0x1014FF118", Offset = "0x14FF118", VA = "0x1014FF118")]
		private void SetToggleValue(OptionMenuItem item, int nItemType, bool value)
		{
		}

		// Token: 0x060049D6 RID: 18902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004411")]
		[Address(RVA = "0x1014FF1D8", Offset = "0x14FF1D8", VA = "0x1014FF1D8")]
		public void OnClickCancelButtonCallback()
		{
		}

		// Token: 0x060049D7 RID: 18903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004412")]
		[Address(RVA = "0x1014FF388", Offset = "0x14FF388", VA = "0x1014FF388")]
		public void OnClickCheckCancelDialogButtonCallback(int index)
		{
		}

		// Token: 0x060049D8 RID: 18904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004413")]
		[Address(RVA = "0x1014FF394", Offset = "0x14FF394", VA = "0x1014FF394")]
		public void OnClickDefalutButtonCallBack()
		{
		}

		// Token: 0x060049D9 RID: 18905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004414")]
		[Address(RVA = "0x1014FF554", Offset = "0x14FF554", VA = "0x1014FF554")]
		public void OnClickCheckDefalutDialogButtonCallBack(int index)
		{
		}

		// Token: 0x060049DA RID: 18906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004415")]
		[Address(RVA = "0x1014FF588", Offset = "0x14FF588", VA = "0x1014FF588")]
		public OptionWindow()
		{
		}

		// Token: 0x04005B6B RID: 23403
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004039")]
		public OptionWindow.OptionItemData[] m_ItemDatas;

		// Token: 0x04005B6C RID: 23404
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400403A")]
		[SerializeField]
		private FixScrollRect m_Scroll;

		// Token: 0x04005B6D RID: 23405
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400403B")]
		[SerializeField]
		private RectTransform m_ItemParentSound;

		// Token: 0x04005B6E RID: 23406
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400403C")]
		[SerializeField]
		private RectTransform m_ItemParentBattle;

		// Token: 0x04005B6F RID: 23407
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400403D")]
		[SerializeField]
		private RectTransform m_ItemParentTown;

		// Token: 0x04005B70 RID: 23408
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400403E")]
		[SerializeField]
		private RectTransform m_ItemParentDownload;

		// Token: 0x04005B71 RID: 23409
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400403F")]
		[SerializeField]
		private OptionMenuItem m_ItemSliderPrefab;

		// Token: 0x04005B72 RID: 23410
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004040")]
		[SerializeField]
		private OptionMenuItem m_ItemTogglePrefab;

		// Token: 0x04005B73 RID: 23411
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004041")]
		[SerializeField]
		private OptionMenuItem m_ItemToggle2Prefab;

		// Token: 0x04005B74 RID: 23412
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004042")]
		[SerializeField]
		private OptionMenuItem m_ItemSelectorPrefab;

		// Token: 0x04005B75 RID: 23413
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004043")]
		[SerializeField]
		private GameObject m_TownOnlyObject;

		// Token: 0x04005B76 RID: 23414
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004044")]
		[SerializeField]
		private CustomButton m_RestoreButton;

		// Token: 0x04005B77 RID: 23415
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004045")]
		private OptionMenuItem[] m_Objs;

		// Token: 0x04005B78 RID: 23416
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004046")]
		private bool m_IsDirty;

		// Token: 0x04005B79 RID: 23417
		[Cpp2IlInjected.FieldOffset(Offset = "0xF1")]
		[Token(Token = "0x4004047")]
		private bool m_IsDirtyMovieSlider;

		// Token: 0x04005B7A RID: 23418
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004048")]
		private BattleOption m_BattleOption;

		// Token: 0x02000F37 RID: 3895
		[Token(Token = "0x20011B3")]
		public enum eCategory
		{
			// Token: 0x04005B7C RID: 23420
			[Token(Token = "0x4006E7C")]
			Sound,
			// Token: 0x04005B7D RID: 23421
			[Token(Token = "0x4006E7D")]
			Battle,
			// Token: 0x04005B7E RID: 23422
			[Token(Token = "0x4006E7E")]
			Town,
			// Token: 0x04005B7F RID: 23423
			[Token(Token = "0x4006E7F")]
			VoiceDownload_Story,
			// Token: 0x04005B80 RID: 23424
			[Token(Token = "0x4006E80")]
			VoiceDownload_Event,
			// Token: 0x04005B81 RID: 23425
			[Token(Token = "0x4006E81")]
			VoiceDownload_Memorial
		}

		// Token: 0x02000F38 RID: 3896
		[Token(Token = "0x20011B4")]
		public enum eOptionItem
		{
			// Token: 0x04005B83 RID: 23427
			[Token(Token = "0x4006E83")]
			None = -1,
			// Token: 0x04005B84 RID: 23428
			[Token(Token = "0x4006E84")]
			BGM,
			// Token: 0x04005B85 RID: 23429
			[Token(Token = "0x4006E85")]
			SE,
			// Token: 0x04005B86 RID: 23430
			[Token(Token = "0x4006E86")]
			Voice,
			// Token: 0x04005B87 RID: 23431
			[Token(Token = "0x4006E87")]
			Movie,
			// Token: 0x04005B88 RID: 23432
			[Token(Token = "0x4006E88")]
			BattlePlayerSpeed,
			// Token: 0x04005B89 RID: 23433
			[Token(Token = "0x4006E89")]
			BattleEnemySpeed,
			// Token: 0x04005B8A RID: 23434
			[Token(Token = "0x4006E8A")]
			BattleTogetherSpeed,
			// Token: 0x04005B8B RID: 23435
			[Token(Token = "0x4006E8B")]
			SkipTogetherAttackInput,
			// Token: 0x04005B8C RID: 23436
			[Token(Token = "0x4006E8C")]
			BattleSkipUniqueSkill,
			// Token: 0x04005B8D RID: 23437
			[Token(Token = "0x4006E8D")]
			SkipGreet,
			// Token: 0x04005B8E RID: 23438
			[Token(Token = "0x4006E8E")]
			BattleUseSkillInAuto,
			// Token: 0x04005B8F RID: 23439
			[Token(Token = "0x4006E8F")]
			BattleAutoCancel,
			// Token: 0x04005B90 RID: 23440
			[Token(Token = "0x4006E90")]
			ADV_Story_FullVoiceDownload,
			// Token: 0x04005B91 RID: 23441
			[Token(Token = "0x4006E91")]
			ADV_Event_FullVoiceDownload,
			// Token: 0x04005B92 RID: 23442
			[Token(Token = "0x4006E92")]
			ADV_Memorial_FullVoiceDownload,
			// Token: 0x04005B93 RID: 23443
			[Token(Token = "0x4006E93")]
			Num
		}

		// Token: 0x02000F39 RID: 3897
		[Token(Token = "0x20011B5")]
		public struct OptionItemData
		{
			// Token: 0x060049DB RID: 18907 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006278")]
			[Address(RVA = "0x1000381C0", Offset = "0x381C0", VA = "0x1000381C0")]
			public OptionItemData(OptionWindow.eCategory category, OptionWindow.eOptionItem option, OptionMenuItem.eType type, eText_CommonDB title, bool isBattleOption = false)
			{
			}

			// Token: 0x060049DC RID: 18908 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006279")]
			[Address(RVA = "0x1000381D4", Offset = "0x381D4", VA = "0x1000381D4")]
			public OptionItemData(OptionWindow.eCategory category, OptionWindow.eOptionItem option, OptionMenuItem.eType type, eText_CommonDB title, eText_CommonDB[] selectorStrings, bool isBattleOption = false)
			{
			}

			// Token: 0x060049DD RID: 18909 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600627A")]
			[Address(RVA = "0x1000381E8", Offset = "0x381E8", VA = "0x1000381E8")]
			public OptionItemData(OptionWindow.eOptionItem option)
			{
			}

			// Token: 0x04005B94 RID: 23444
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006E94")]
			public OptionWindow.eCategory m_Category;

			// Token: 0x04005B95 RID: 23445
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4006E95")]
			public OptionWindow.eOptionItem m_Option;

			// Token: 0x04005B96 RID: 23446
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006E96")]
			public OptionMenuItem.eType m_Type;

			// Token: 0x04005B97 RID: 23447
			[Cpp2IlInjected.FieldOffset(Offset = "0xC")]
			[Token(Token = "0x4006E97")]
			public eText_CommonDB m_Title;

			// Token: 0x04005B98 RID: 23448
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006E98")]
			public bool m_IsBattleOption;

			// Token: 0x04005B99 RID: 23449
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006E99")]
			public eText_CommonDB[] m_SelectorStrings;
		}
	}
}
