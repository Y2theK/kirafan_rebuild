﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000F29 RID: 3881
	[Token(Token = "0x2000A42")]
	[StructLayout(3)]
	public class PassportPeriodUnit : ScrollItemIcon
	{
		// Token: 0x06004914 RID: 18708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004350")]
		[Address(RVA = "0x1014FFC18", Offset = "0x14FFC18", VA = "0x1014FFC18")]
		public void SetTitleText(string text)
		{
		}

		// Token: 0x06004915 RID: 18709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004351")]
		[Address(RVA = "0x1014FFCD0", Offset = "0x14FFCD0", VA = "0x1014FFCD0")]
		public void SetPeriodDateText(string text)
		{
		}

		// Token: 0x06004916 RID: 18710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004352")]
		[Address(RVA = "0x1014FFD80", Offset = "0x14FFD80", VA = "0x1014FFD80")]
		public void SetRepeatDateText(string text)
		{
		}

		// Token: 0x06004917 RID: 18711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004353")]
		[Address(RVA = "0x1014FFE30", Offset = "0x14FFE30", VA = "0x1014FFE30", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004918 RID: 18712 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004354")]
		[Address(RVA = "0x1014FFED4", Offset = "0x14FFED4", VA = "0x1014FFED4")]
		public PassportPeriodUnit()
		{
		}

		// Token: 0x04005B0E RID: 23310
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004006")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x04005B0F RID: 23311
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004007")]
		[SerializeField]
		private TextField m_TextField_Period;

		// Token: 0x04005B10 RID: 23312
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004008")]
		[SerializeField]
		private TextField m_TextField_Repeat;
	}
}
