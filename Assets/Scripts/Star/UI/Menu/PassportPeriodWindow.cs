﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000F2A RID: 3882
	[Token(Token = "0x2000A43")]
	[StructLayout(3)]
	public class PassportPeriodWindow : UIGroup
	{
		// Token: 0x06004919 RID: 18713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004355")]
		[Address(RVA = "0x1014FFEDC", Offset = "0x14FFEDC", VA = "0x1014FFEDC")]
		public void Start()
		{
		}

		// Token: 0x0600491A RID: 18714 RVA: 0x0001A688 File Offset: 0x00018888
		[Token(Token = "0x6004356")]
		[Address(RVA = "0x1014FFF10", Offset = "0x14FFF10", VA = "0x1014FFF10")]
		public bool Add(string titleText, string periodDateText, string repeatDateText)
		{
			return default(bool);
		}

		// Token: 0x0600491B RID: 18715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004357")]
		[Address(RVA = "0x101500034", Offset = "0x1500034", VA = "0x101500034")]
		public void Clear()
		{
		}

		// Token: 0x0600491C RID: 18716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004358")]
		[Address(RVA = "0x101500068", Offset = "0x1500068", VA = "0x101500068")]
		public PassportPeriodWindow()
		{
		}

		// Token: 0x04005B11 RID: 23313
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004009")]
		[SerializeField]
		private Transform m_UnitParentTransform;

		// Token: 0x04005B12 RID: 23314
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400400A")]
		[SerializeField]
		private ScrollViewBase m_ScrollView;

		// Token: 0x02000F2B RID: 3883
		[Token(Token = "0x20011AD")]
		public class PassportPeriodData : ScrollItemData
		{
			// Token: 0x0600491D RID: 18717 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006277")]
			[Address(RVA = "0x1014FFFF4", Offset = "0x14FFFF4", VA = "0x1014FFFF4")]
			public PassportPeriodData(string titleText, string periodDateText, string repeatDateText)
			{
			}

			// Token: 0x04005B13 RID: 23315
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006E51")]
			public string m_titleText;

			// Token: 0x04005B14 RID: 23316
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006E52")]
			public string m_periodDateText;

			// Token: 0x04005B15 RID: 23317
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006E53")]
			public string m_repeatDateText;
		}
	}
}
