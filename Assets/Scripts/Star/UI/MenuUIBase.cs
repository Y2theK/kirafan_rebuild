﻿using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public abstract class MenuUIBase : MonoBehaviour {
        protected CanvasScaler m_UICanvasScaler;
        private GraphicRaycaster m_RayCaster;
        protected UIGroupController m_UIGroupController;
        protected int m_State;
        protected GameObject m_GameObject;

        public int State => m_State;

        public GameObject GameObject {
            get {
                if (m_GameObject == null) {
                    m_GameObject = gameObject;
                }
                return m_GameObject;
            }
        }

        public abstract void PlayIn();
        public abstract void PlayOut();
        public abstract bool IsMenuEnd();

        public virtual void Destroy() { }

        public virtual void OnDisable() {
            if (GameSystem.Inst != null && GameSystem.Inst.IsAvailable()) {
                GameSystem.Inst.InputBlock.SetBlockObj(gameObject, false);
            }
        }

        public virtual void SetEnableInput(bool flg) {
            if (m_RayCaster == null) {
                m_RayCaster = GameObject.GetComponent<GraphicRaycaster>();
            }
            if (m_RayCaster != null) {
                m_RayCaster.enabled = flg;
            }
            if (m_UIGroupController == null) {
                m_UIGroupController = GetComponent<UIGroupController>();
            }
            if (m_UIGroupController != null) {
                m_UIGroupController.SetEnableInput(flg);
            }
        }

        public Vector2 WorldToUIPosition(Vector3 worldPoint) {
            Vector2 rawPos = RectTransformUtility.WorldToScreenPoint(Camera.main, worldPoint);
            return RawScreenPositionToUICameraPosition(rawPos);
        }

        protected Vector2 RawScreenPositionToUICameraPosition(Vector2 rawPos) {
            if (m_UICanvasScaler == null) {
                m_UICanvasScaler = GetComponent<CanvasScaler>();
            }
            Vector2 halfScreen = UIUtility.GetScaledScreenSize(m_UICanvasScaler) / 2f;
            return UIUtility.RawScreenPositionToScaledScreenPosition(m_UICanvasScaler, rawPos) - halfScreen;
        }
    }
}
