﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FC9 RID: 4041
	// (Invoke) Token: 0x06004CD2 RID: 19666
	[Token(Token = "0x2000A86")]
	[StructLayout(3, Size = 8)]
	public delegate void CallbackSelectResult(eButton fselect, MissionPopUpWindow pcall);
}
