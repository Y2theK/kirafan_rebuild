﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FBC RID: 4028
	[Token(Token = "0x2000A7A")]
	[StructLayout(3)]
	public class MissionPopUpExchangeComplete : MissionPopUpWindow
	{
		// Token: 0x06004C8C RID: 19596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004633")]
		[Address(RVA = "0x1015009EC", Offset = "0x15009EC", VA = "0x1015009EC")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004C8D RID: 19597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004634")]
		[Address(RVA = "0x1015009F8", Offset = "0x15009F8", VA = "0x1015009F8")]
		public MissionPopUpExchangeComplete()
		{
		}
	}
}
