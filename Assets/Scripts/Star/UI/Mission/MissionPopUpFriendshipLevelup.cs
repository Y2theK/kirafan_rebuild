﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FBD RID: 4029
	[Token(Token = "0x2000A7B")]
	[StructLayout(3)]
	public class MissionPopUpFriendshipLevelup : MissionPopUpWindow
	{
		// Token: 0x06004C8E RID: 19598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004635")]
		[Address(RVA = "0x101500A08", Offset = "0x1500A08", VA = "0x101500A08")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004C8F RID: 19599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004636")]
		[Address(RVA = "0x101500A14", Offset = "0x1500A14", VA = "0x101500A14")]
		public MissionPopUpFriendshipLevelup()
		{
		}
	}
}
