﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FBE RID: 4030
	[Token(Token = "0x2000A7C")]
	[StructLayout(3)]
	public class MissionPopUpItemExchange : MissionPopUpWindow
	{
		// Token: 0x06004C90 RID: 19600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004637")]
		[Address(RVA = "0x101500A1C", Offset = "0x1500A1C", VA = "0x101500A1C")]
		public void OnYesButtonCallBack()
		{
		}

		// Token: 0x06004C91 RID: 19601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004638")]
		[Address(RVA = "0x101500A28", Offset = "0x1500A28", VA = "0x101500A28")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004C92 RID: 19602 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004639")]
		[Address(RVA = "0x101500A34", Offset = "0x1500A34", VA = "0x101500A34")]
		public MissionPopUpItemExchange()
		{
		}
	}
}
