﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FBF RID: 4031
	[Token(Token = "0x2000A7D")]
	[StructLayout(3)]
	public class MissionPopUpMissionComplete : MissionPopUpWindow
	{
		// Token: 0x06004C93 RID: 19603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463A")]
		[Address(RVA = "0x101500A3C", Offset = "0x1500A3C", VA = "0x101500A3C")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004C94 RID: 19604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463B")]
		[Address(RVA = "0x101500A48", Offset = "0x1500A48", VA = "0x101500A48")]
		public MissionPopUpMissionComplete()
		{
		}
	}
}
