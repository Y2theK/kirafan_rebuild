﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FC0 RID: 4032
	[Token(Token = "0x2000A7E")]
	[StructLayout(3)]
	public class MissionPopUpPlayerInfomation : MissionPopUpWindow
	{
		// Token: 0x06004C95 RID: 19605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463C")]
		[Address(RVA = "0x101500A50", Offset = "0x1500A50", VA = "0x101500A50")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x06004C96 RID: 19606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463D")]
		[Address(RVA = "0x101500A5C", Offset = "0x1500A5C", VA = "0x101500A5C")]
		public void OnAdoptButtonCallBack()
		{
		}

		// Token: 0x06004C97 RID: 19607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463E")]
		[Address(RVA = "0x101500A68", Offset = "0x1500A68", VA = "0x101500A68")]
		public void OnNoAdoptButtonCallBack()
		{
		}

		// Token: 0x06004C98 RID: 19608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600463F")]
		[Address(RVA = "0x101500A74", Offset = "0x1500A74", VA = "0x101500A74")]
		public MissionPopUpPlayerInfomation()
		{
		}
	}
}
