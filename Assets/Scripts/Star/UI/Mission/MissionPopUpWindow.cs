﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FC1 RID: 4033
	[Token(Token = "0x2000A7F")]
	[StructLayout(3)]
	public class MissionPopUpWindow : UIGroup
	{
		// Token: 0x06004C99 RID: 19609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004640")]
		[Address(RVA = "0x101500A00", Offset = "0x1500A00", VA = "0x101500A00")]
		public MissionPopUpWindow()
		{
		}

		// Token: 0x04005E90 RID: 24208
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004249")]
		protected CallbackSelectResult m_CallbackResult;
	}
}
