﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission {
    public class MissionScroll {
        private List<IMissionUIData> m_ListUp;
        private ScrollViewBase m_Scroll;
        private DateTime m_LimitTime;
        private int m_CompNum;
        private int m_PreCompNum;
        private eMissionCategory m_Category;
        private GameObject m_ListInfo;
        private GameObject m_LimitInfo;
        private Badge m_Badge;
        private MissionUI m_OwnerUI;

        public eMissionCategory Category => m_Category;

        public int GetAllListNum() {
            return m_ListUp.Count;
        }

        public int GetCompListNum() {
            return m_CompNum;
        }

        public int GetPreCompNum() {
            return m_PreCompNum;
        }

        public DateTime GetLimitTime() {
            return m_LimitTime;
        }

        public void SetupBoard(MissionUI ownerUI, ScrollViewBase pscroll, eMissionCategory category, GameObject plistinfo, GameObject plimitinfo, Badge badge) {
            m_ListInfo = plistinfo;
            m_LimitInfo = plimitinfo;
            m_Scroll = pscroll;
            m_Category = category;
            m_Badge = badge;
            m_OwnerUI = ownerUI;
            Listup();
        }

        public void Listup() {
            MissionUIDataPack data = MissionManager.Inst.GetCategoryToMissionUIData(m_Category);
            m_ListUp = new List<IMissionUIData>();
            if (data.m_Table.Length != 0) {
                m_LimitTime = data.m_Table[0].m_LimitTime;
            }
            for (int i = 0; i < data.GetListNum(); i++) {
                switch (data.m_Table[i].m_State) {
                    case eMissionState.Non:
                    case eMissionState.PrePop:
                    case eMissionState.PreComp:
                    case eMissionState.Comp:
                        m_ListUp.Add(data.m_Table[i]);
                        break;
                }
            }
            RebuildScrollFromList(true);
        }

        private void RebuildScrollFromList(bool sort) {
            if (sort) {
                m_ListUp.Sort(SortCompare);
                for (int i = 0; i < m_ListUp.Count; i++) {
                    m_ListUp[i].m_SortedIdx = i;
                }
            }
            float scrollValue = m_Scroll.GetScrollValue();
            m_Scroll.RemoveAllData();
            m_CompNum = 0;
            m_PreCompNum = 0;
            for (int i = 0; i < m_ListUp.Count; i++) {
                m_Scroll.AddItem(new MissionScrollItemData {
                    m_UIData = m_ListUp[i],
                    m_RootList = this
                });
                switch (m_ListUp[i].m_State) {
                    case eMissionState.PrePop:
                    case eMissionState.PreComp:
                        m_PreCompNum++;
                        break;
                    case eMissionState.Comp:
                        m_CompNum++;
                        break;
                }
            }
            m_Scroll.SetScrollValue(scrollValue);
            m_Badge.Apply(GetPreCompNum());
        }

        private int SortCompare(IMissionUIData r, IMissionUIData l) {
            int rState = r.m_State == eMissionState.Comp ? 1 : 0;
            if (r.m_State == eMissionState.PrePop || r.m_State == eMissionState.PreComp || (rState == 1 && r.m_Completed)) {
                rState = -1;
            }
            int lState = l.m_State == eMissionState.Comp ? 1 : 0;
            if (l.m_State == eMissionState.PrePop || l.m_State == eMissionState.PreComp || (lState == 1 && l.m_Completed)) {
                lState = -1;
            }

            int stateCompare = rState.CompareTo(lState);
            if (stateCompare != 0) {
                return stateCompare;
            }
            int prioCompare = r.m_uiPriority.CompareTo(l.m_uiPriority);
            if (prioCompare != 0) {
                return prioCompare;
            }
            int idCompare = r.m_DatabaseID.CompareTo(l.m_DatabaseID);
            if (idCompare != 0) {
                return idCompare;
            }
            return r.m_SortedIdx.CompareTo(l.m_SortedIdx);
        }

        public void ReleaseUp() { }

        public int CheckAllComplete() {
            int newComp = 0;
            for (int i = 0; i < m_ListUp.Count; i++) {
                IMissionUIData data = m_ListUp[i];
                if (data.m_State == eMissionState.PrePop || data.m_State == eMissionState.PreComp) {
                    MissionManager.Inst.CompleteMission(data.m_ManageID);
                    data.m_State = eMissionState.Comp;
                    data.m_Completed = true;
                    newComp++;
                }
            }
            return newComp;
        }

        public void CompleteMission(long fmanageid) {
            if (m_OwnerUI != null) {
                m_OwnerUI.StartSendingCompState();
            }
            MissionManager.Inst.CompleteMission(fmanageid);
            for (int i = 0; i < m_ListUp.Count; i++) {
                if (m_ListUp[i].m_ManageID == fmanageid) {
                    m_ListUp[i].m_State = eMissionState.Comp;
                    m_ListUp[i].m_Completed = true;
                }
            }
        }

        public void SetActive(bool active) {
            m_Scroll.gameObject.SetActive(active);
        }

        public void RefreshScrollList() {
            m_Scroll.RemoveAll();
            for (int i = 0; i < m_ListUp.Count; i++) {
                m_Scroll.AddItem(new MissionScrollItemData {
                    m_UIData = m_ListUp[i],
                    m_RootList = this
                });
            }
        }

        public void ListRefresh(bool factivemenu) {
            MissionUIDataPack data = MissionManager.Inst.GetCategoryToMissionUIData(m_Category);
            for (int i = 0; i < data.GetListNum(); i++) {
                bool exist = false;
                for (int j = 0; j < m_ListUp.Count; j++) {
                    if (m_ListUp[j].m_ManageID == data.m_Table[i].m_ManageID) {
                        if (m_ListUp[j].m_State != eMissionState.Comp) {
                            m_ListUp[j].m_State = data.m_Table[i].m_State;
                        }
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    m_ListUp.Add(data.m_Table[i]);
                }
            }
            RebuildScrollFromList(true);
            if (factivemenu) {
                SetUMissionListInfo();
            }
        }

        public void SetUMissionListInfo() {
            Text text = UIUtility.FindHrcObject(m_ListInfo.transform, "ListUp", typeof(Text)) as Text;
            if (text != null) {
                string format = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionSegMsgHaveFix);
                text.text = string.Format(format, GetCompListNum(), GetAllListNum());
            }
        }

        public void SetUMissionLimitInfo() {
            Text text = UIUtility.FindHrcObject(m_LimitInfo.transform, "TimeUp", typeof(Text)) as Text;
            if (text != null) {
                switch (m_Category) {
                    case eMissionCategory.Day:
                    case eMissionCategory.Week:
                        m_LimitInfo.SetActive(true);
                        SetItemTime(text, m_LimitTime, false);
                        break;
                    case eMissionCategory.Unlock:
                    case eMissionCategory.Event:
                        m_LimitInfo.SetActive(false);
                        break;
                }
            }
        }

        private void GetTimeTickToTimes(out int fday, out int fhour, out int fminute, out int fsec, long ftimes) {
            ftimes /= 10000000L;
            fday = (int)(ftimes / 86400L);
            ftimes %= 86400L;
            fhour = (int)(ftimes / 3600L);
            ftimes %= 3600L;
            fminute = (int)(ftimes / 60L);
            fsec = (int)(ftimes % 60L);
        }

        protected void SetItemTime(Text ptarget, DateTime fsettime, bool fdebugs) {
            long ticks;
            if (fdebugs) {
                ticks = ScheduleTimeUtil.GetManageUniversalTime().Ticks;
            } else {
                ticks = GameSystem.Inst.ServerTime.Ticks;
            }
            if (fsettime.Ticks > ticks) {
                ptarget.text = UIUtility.GetMissionLimitSpanString(fsettime - new DateTime(ticks));
            } else {
                ptarget.text = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeOver);
            }
        }
    }
}
