﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000FC3 RID: 4035
	[Token(Token = "0x2000A81")]
	[StructLayout(3)]
	public class MissionScrollItem : ScrollItemIcon
	{
		// Token: 0x06004CAE RID: 19630 RVA: 0x0001B2A0 File Offset: 0x000194A0
		[Token(Token = "0x6004655")]
		[Address(RVA = "0x1015020C8", Offset = "0x15020C8", VA = "0x1015020C8")]
		public eMissionCategory GetCategory()
		{
			return eMissionCategory.Day;
		}

		// Token: 0x06004CAF RID: 19631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004656")]
		[Address(RVA = "0x1015020D0", Offset = "0x15020D0", VA = "0x1015020D0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004CB0 RID: 19632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004657")]
		[Address(RVA = "0x10150224C", Offset = "0x150224C", VA = "0x10150224C", Slot = "8")]
		public virtual void SetUIData(IMissionUIData uiData)
		{
		}

		// Token: 0x06004CB1 RID: 19633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004658")]
		[Address(RVA = "0x1015023AC", Offset = "0x15023AC", VA = "0x1015023AC")]
		protected void SetID(long fmanageid)
		{
		}

		// Token: 0x06004CB2 RID: 19634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004659")]
		[Address(RVA = "0x1015027B4", Offset = "0x15027B4", VA = "0x1015027B4", Slot = "9")]
		public virtual void SetComplete(int rateValue, int rateMax, eMissionState fstate)
		{
		}

		// Token: 0x06004CB3 RID: 19635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465A")]
		[Address(RVA = "0x1015023B4", Offset = "0x15023B4", VA = "0x1015023B4")]
		private void SetTarget(string targetText)
		{
		}

		// Token: 0x06004CB4 RID: 19636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465B")]
		[Address(RVA = "0x101502468", Offset = "0x1502468", VA = "0x101502468")]
		private void SetReward(eMissionRewardCategory rewardType, int rewardID)
		{
		}

		// Token: 0x06004CB5 RID: 19637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465C")]
		[Address(RVA = "0x101502E78", Offset = "0x1502E78", VA = "0x101502E78")]
		public void OnClickCompleteButtonCallBack()
		{
		}

		// Token: 0x06004CB6 RID: 19638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465D")]
		[Address(RVA = "0x101502F6C", Offset = "0x1502F6C", VA = "0x101502F6C")]
		protected void SetChara(CharaIcon pcharicon, Text pcharaname, long charaMngID)
		{
		}

		// Token: 0x06004CB7 RID: 19639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465E")]
		[Address(RVA = "0x101503150", Offset = "0x1503150", VA = "0x101503150")]
		protected void SetCharaIcon(CharaIcon pcharicon, Text pcharaname, int charaID)
		{
		}

		// Token: 0x06004CB8 RID: 19640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600465F")]
		[Address(RVA = "0x101503188", Offset = "0x1503188", VA = "0x101503188")]
		private void GetTimeTickToTimes(out int fhour, out int fminute, out int fsec, long ftimes)
		{
		}

		// Token: 0x06004CB9 RID: 19641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004660")]
		[Address(RVA = "0x1015031FC", Offset = "0x15031FC", VA = "0x1015031FC")]
		protected void SetItemTime(Text ptarget, DateTime fsettime, bool fdebugs)
		{
		}

		// Token: 0x06004CBA RID: 19642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004661")]
		[Address(RVA = "0x10150339C", Offset = "0x150339C", VA = "0x10150339C")]
		private void Update()
		{
		}

		// Token: 0x06004CBB RID: 19643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004662")]
		[Address(RVA = "0x1015035BC", Offset = "0x15035BC", VA = "0x1015035BC")]
		public MissionScrollItem()
		{
		}

		// Token: 0x04005E9B RID: 24219
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004254")]
		private long m_MissionManageID;

		// Token: 0x04005E9C RID: 24220
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004255")]
		protected eMissionState m_State;

		// Token: 0x04005E9D RID: 24221
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4004256")]
		protected eMissionCategory m_Category;

		// Token: 0x04005E9E RID: 24222
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004257")]
		[SerializeField]
		protected CustomButton m_CompleteButton;

		// Token: 0x04005E9F RID: 24223
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004258")]
		[SerializeField]
		protected Text m_TargetTextObj;

		// Token: 0x04005EA0 RID: 24224
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004259")]
		[SerializeField]
		protected Text m_RewardTextObj;

		// Token: 0x04005EA1 RID: 24225
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400425A")]
		[SerializeField]
		protected Text m_RateValueTextObj;

		// Token: 0x04005EA2 RID: 24226
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400425B")]
		protected MissionScrollItem.BlinkUIImage m_Blink;

		// Token: 0x02000FC4 RID: 4036
		[Token(Token = "0x2001206")]
		public class BlinkUIImage
		{
			// Token: 0x06004CBC RID: 19644 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600630A")]
			[Address(RVA = "0x101502D28", Offset = "0x1502D28", VA = "0x101502D28")]
			public BlinkUIImage()
			{
			}

			// Token: 0x06004CBD RID: 19645 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600630B")]
			[Address(RVA = "0x1015035CC", Offset = "0x15035CC", VA = "0x1015035CC")]
			public BlinkUIImage(Color max, Color min, float time)
			{
			}

			// Token: 0x06004CBE RID: 19646 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600630C")]
			[Address(RVA = "0x101502DE4", Offset = "0x1502DE4", VA = "0x101502DE4")]
			public void SetGameObject(GameObject pobj)
			{
			}

			// Token: 0x06004CBF RID: 19647 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600630D")]
			[Address(RVA = "0x1015033AC", Offset = "0x15033AC", VA = "0x1015033AC")]
			public void UpdateFunc()
			{
			}

			// Token: 0x04005EA3 RID: 24227
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F8A")]
			public List<Image> m_ImageList;

			// Token: 0x04005EA4 RID: 24228
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006F8B")]
			public float m_Time;

			// Token: 0x04005EA5 RID: 24229
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006F8C")]
			public Color m_MaxCol;

			// Token: 0x04005EA6 RID: 24230
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4006F8D")]
			public Color m_MinCol;

			// Token: 0x04005EA7 RID: 24231
			[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
			[Token(Token = "0x4006F8E")]
			public float m_blinkTime;
		}
	}
}
