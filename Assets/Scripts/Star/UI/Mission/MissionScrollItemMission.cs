﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000FC6 RID: 4038
	[Token(Token = "0x2000A83")]
	[StructLayout(3)]
	public class MissionScrollItemMission : MissionScrollItem
	{
		// Token: 0x06004CC1 RID: 19649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004664")]
		[Address(RVA = "0x1015036E4", Offset = "0x15036E4", VA = "0x1015036E4", Slot = "8")]
		public override void SetUIData(IMissionUIData uiData)
		{
		}

		// Token: 0x06004CC2 RID: 19650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004665")]
		[Address(RVA = "0x101503D00", Offset = "0x1503D00", VA = "0x101503D00")]
		public MissionScrollItemMission()
		{
		}

		// Token: 0x04005EAA RID: 24234
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400425E")]
		[SerializeField]
		private PrefabCloner m_RewardIcon;

		// Token: 0x04005EAB RID: 24235
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400425F")]
		[SerializeField]
		private GameObject m_goLimitInfo;

		// Token: 0x04005EAC RID: 24236
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004260")]
		[SerializeField]
		private Text m_LimitTermText;

		// Token: 0x04005EAD RID: 24237
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004261")]
		[SerializeField]
		private Image m_RateBar;
	}
}
