﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000FC7 RID: 4039
	[Token(Token = "0x2000A84")]
	[StructLayout(3)]
	public class MissionScrollItemPanel : MissionScrollItem
	{
		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06004CC3 RID: 19651 RVA: 0x0001B2B8 File Offset: 0x000194B8
		// (set) Token: 0x06004CC4 RID: 19652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004E7")]
		public bool hidden
		{
			[Token(Token = "0x6004666")]
			[Address(RVA = "0x101503D10", Offset = "0x1503D10", VA = "0x101503D10")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004667")]
			[Address(RVA = "0x101503D4C", Offset = "0x1503D4C", VA = "0x101503D4C")]
			set
			{
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06004CC5 RID: 19653 RVA: 0x0001B2D0 File Offset: 0x000194D0
		// (set) Token: 0x06004CC6 RID: 19654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004E8")]
		public int panelIndex
		{
			[Token(Token = "0x6004668")]
			[Address(RVA = "0x101503D90", Offset = "0x1503D90", VA = "0x101503D90")]
			get
			{
				return 0;
			}
			[Token(Token = "0x6004669")]
			[Address(RVA = "0x101503D98", Offset = "0x1503D98", VA = "0x101503D98")]
			set
			{
			}
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06004CC7 RID: 19655 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004E9")]
		public IMissionUIData missionUIData
		{
			[Token(Token = "0x600466A")]
			[Address(RVA = "0x101503DA0", Offset = "0x1503DA0", VA = "0x101503DA0")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004CC8 RID: 19656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600466B")]
		[Address(RVA = "0x101503DA8", Offset = "0x1503DA8", VA = "0x101503DA8")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004CC9 RID: 19657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600466C")]
		[Address(RVA = "0x101503DC0", Offset = "0x1503DC0", VA = "0x101503DC0", Slot = "8")]
		public override void SetUIData(IMissionUIData uiData)
		{
		}

		// Token: 0x06004CCA RID: 19658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600466D")]
		[Address(RVA = "0x101503FA0", Offset = "0x1503FA0", VA = "0x101503FA0", Slot = "9")]
		public override void SetComplete(int rateValue, int rateMax, eMissionState state)
		{
		}

		// Token: 0x06004CCB RID: 19659 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600466E")]
		[Address(RVA = "0x1015047D0", Offset = "0x15047D0", VA = "0x1015047D0")]
		public void OnClickButtonCallBack()
		{
		}

		// Token: 0x06004CCC RID: 19660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600466F")]
		[Address(RVA = "0x101504684", Offset = "0x1504684", VA = "0x101504684")]
		protected void SetCompPanel(bool active)
		{
		}

		// Token: 0x06004CCD RID: 19661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004670")]
		[Address(RVA = "0x1015043C8", Offset = "0x15043C8", VA = "0x1015043C8")]
		protected void SetPreCompPanel(bool active)
		{
		}

		// Token: 0x06004CCE RID: 19662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004671")]
		[Address(RVA = "0x101504878", Offset = "0x1504878", VA = "0x101504878")]
		public void SetSelectPanel(bool active)
		{
		}

		// Token: 0x06004CCF RID: 19663 RVA: 0x0001B2E8 File Offset: 0x000194E8
		[Token(Token = "0x6004672")]
		[Address(RVA = "0x1015049C8", Offset = "0x15049C8", VA = "0x1015049C8")]
		public bool isComplete()
		{
			return default(bool);
		}

		// Token: 0x06004CD0 RID: 19664 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004673")]
		[Address(RVA = "0x1015049D8", Offset = "0x15049D8", VA = "0x1015049D8")]
		public MissionScrollItemPanel()
		{
		}

		// Token: 0x04005EAE RID: 24238
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004262")]
		private Color PANELCOLOR_SELECTED;

		// Token: 0x04005EAF RID: 24239
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004263")]
		private Color PANELCOLOR_PRECOMP;

		// Token: 0x04005EB0 RID: 24240
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004264")]
		private Color PANELCOLOR_COMPLETE;

		// Token: 0x04005EB1 RID: 24241
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004265")]
		private readonly float BLINK_TIME;

		// Token: 0x04005EB2 RID: 24242
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004266")]
		[SerializeField]
		private Text m_LimitTermText;

		// Token: 0x04005EB3 RID: 24243
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004267")]
		[SerializeField]
		protected Image m_imgSelectPanel;

		// Token: 0x04005EB4 RID: 24244
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004268")]
		[SerializeField]
		protected ImageNumbers m_imgNumbers;

		// Token: 0x04005EB5 RID: 24245
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004269")]
		[SerializeField]
		protected CanvasGroup m_canvasGroup;

		// Token: 0x04005EB6 RID: 24246
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400426A")]
		[SerializeField]
		protected Image m_rateBar;

		// Token: 0x04005EB7 RID: 24247
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400426B")]
		protected int m_panelIndex;

		// Token: 0x04005EB8 RID: 24248
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400426C")]
		protected IMissionUIData m_stackUIData;

		// Token: 0x04005EB9 RID: 24249
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x400426D")]
		public Action<int, IMissionUIData> m_btnCallbackViewReward;

		// Token: 0x04005EBA RID: 24250
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x400426E")]
		public Action<int, IMissionUIData> m_btnCallbackGetReward;

		// Token: 0x04005EBB RID: 24251
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x400426F")]
		public Action m_btnCallbackComplete;
	}
}
