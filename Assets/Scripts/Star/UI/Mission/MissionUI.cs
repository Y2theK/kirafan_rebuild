﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Mission {
    public class MissionUI : MenuUIBase {
        private const int BADGE_POS_X = 108;
        private const int BADGE_POS_Y = -4;

        [SerializeField] private UIStateController m_StateController;
        [SerializeField] private AnimUIPlayer[] m_AnimList;
        [SerializeField] private UIGroup m_MainWindow;
        [SerializeField] private TabGroup m_TabWindow;
        [SerializeField] private ScrollViewBase m_ScrollNormal;
        [SerializeField] private ScrollViewBase m_ScrollLimit;
        [SerializeField] private ScrollViewBase m_ScrollDay;
        [SerializeField] private ScrollViewBase m_ScrollDrop;
        [SerializeField] private GameObject m_MissionListInfo;
        [SerializeField] private GameObject m_MissionLimitInfo;
        [SerializeField] private Badge m_BadgePrefab;

        private Badge[] m_Badges;
        private MissionScroll[] m_MissionList;
        private float m_TimeChk;
        private eMissionCategory m_CurrentCategory = eMissionCategory.None;
        private Dictionary<eMissionCategory, eText_CommonDB> m_CategoryTable = new Dictionary<eMissionCategory, eText_CommonDB>();
        private bool m_IsSendingComp;

        public event Action OnEnd;

        public void Setup() {
            m_StateController.Setup((int)eUIState.Num, (int)eUIState.Hide);
            m_StateController.AddTransit((int)eUIState.Hide, (int)eUIState.In);
            m_StateController.AddTransit((int)eUIState.In, (int)eUIState.Main);
            m_StateController.AddTransit((int)eUIState.Main, (int)eUIState.Out);
            m_StateController.AddTransit((int)eUIState.Out, (int)eUIState.End);
            m_CategoryTable.Clear();
            m_CategoryTable.Add(eMissionCategory.Day, eText_CommonDB.MissionTabNormal);
            m_CategoryTable.Add(eMissionCategory.Week, eText_CommonDB.MissionTabLimit);
            m_CategoryTable.Add(eMissionCategory.Unlock, eText_CommonDB.MissionTabDay);
            m_CategoryTable.Add(eMissionCategory.Event, eText_CommonDB.MissionTabDrop);
            string[] texts = new string[m_CategoryTable.Count];
            for (int i = 0; i < texts.Length; i++) {
                texts[i] = GameSystem.Inst.DbMng.GetTextCommon(m_CategoryTable[(eMissionCategory)i]);
            }
            m_TabWindow.Setup(texts);
            if (m_Badges == null) {
                m_Badges = new Badge[m_TabWindow.GetButtonNum()];
                for (int i = 0; i < m_TabWindow.GetButtonNum(); i++) {
                    RectTransform parentTransform = m_TabWindow.GetButton(i).Button.GetComponent<RectTransform>();
                    m_Badges[i] = Instantiate(m_BadgePrefab, parentTransform);
                    RectTransform badgeTransform = m_Badges[i].GetComponent<RectTransform>();
                    badgeTransform.anchoredPosition = new Vector2(BADGE_POS_X, BADGE_POS_Y);
                }
            }
            m_ScrollNormal.Setup();
            m_ScrollLimit.Setup();
            m_ScrollDay.Setup();
            m_ScrollDrop.Setup();
            ScrollViewBase[] scrolls = new ScrollViewBase[] {
                m_ScrollNormal,
                m_ScrollLimit,
                m_ScrollDay,
                m_ScrollDrop,
            };
            m_MissionList = new MissionScroll[scrolls.Length];
            for (int i = 0; i < m_MissionList.Length; i++) {
                m_MissionList[i] = new MissionScroll();
                m_MissionList[i].SetupBoard(this, scrolls[i], (eMissionCategory)i, m_MissionListInfo, m_MissionLimitInfo, m_Badges[i]);
            }
            GameSystem.Inst.SetRoomActionScriptPlaySoundFlg(RoomBuilderBase.ePlayScriptSoundId.System, false);
            m_MainWindow.OnClose += () => GameSystem.Inst.SetRoomActionScriptPlaySoundFlg(RoomBuilderBase.ePlayScriptSoundId.System, true);
            ChangeCategory(eMissionCategory.Day);
            m_IsSendingComp = false;
        }

        public void StartSendingCompState() {
            m_IsSendingComp = true;
        }

        public override void PlayIn() {
            if (m_StateController.ChangeState((int)eUIState.In)) {
                for (int i = 0; i < m_AnimList.Length; i++) {
                    m_AnimList[i].PlayIn();
                }
                m_MainWindow.Open();
                UIUtility.PlayVoiceFromFavoriteChara(eSoundVoiceListDB.voice_012);
            }
        }

        public override void PlayOut() {
            if (m_StateController.ChangeState((int)eUIState.Out)) {
                for (int i = 0; i < m_AnimList.Length; i++) {
                    m_AnimList[i].PlayOut();
                }
                m_MainWindow.Close();
                for (int j = 0; j < 4; j++) {
                    m_MissionList[j].ReleaseUp();
                    m_MissionList[j] = null;
                }
                MissionManager.Inst.ReleaseRefreshCallback();
                m_MissionList = null;
            }
        }

        public void PlayAllComplete() {
            int completeNum = 0;
            for (int i = 0; i < m_MissionList.Length; i++) {
                completeNum += m_MissionList[i].CheckAllComplete();
            }
            if (completeNum > 0) {
                int cat = (int)m_CurrentCategory;
                m_MissionList[cat].RefreshScrollList();
                m_MissionList[cat].SetUMissionListInfo();
                UIUtility.PlayVoiceFromFavoriteChara(eSoundVoiceListDB.voice_013);
                StartSendingCompState();
            }
        }

        private void Update() {
            switch ((eUIState)m_StateController.NowState) {
                case eUIState.In:
                    bool inDone = true;
                    for (int i = 0; i < m_AnimList.Length; i++) {
                        if (!m_AnimList[i].IsEnd) {
                            inDone = false;
                            break;
                        }
                    }
                    if (inDone && m_MainWindow.IsDonePlayIn) {
                        m_StateController.ChangeState((int)eUIState.Main);
                    }
                    break;
                case eUIState.Out:
                    bool outDone = true;
                    for (int i = 0; i < m_AnimList.Length; i++) {
                        if (!m_AnimList[i].IsEnd) {
                            outDone = false;
                            break;
                        }
                    }
                    if (outDone && m_MainWindow.IsDonePlayOut) {
                        m_StateController.ChangeState((int)eUIState.End);
                        OnEnd.Call();
                    }
                    break;
                case eUIState.Main:
                    m_TimeChk += Time.deltaTime;
                    if (m_TimeChk >= 1f) {
                        m_MissionList[(int)m_CurrentCategory].SetUMissionLimitInfo();
                        m_TimeChk -= 1f;
                    }
                    if (m_IsSendingComp) {
                        MissionManager.Inst.EntryRefreshCallback(CallbackRefresh);
                        m_IsSendingComp = false;
                    }
                    break;
            }
        }

        public override bool IsMenuEnd() {
            return m_StateController.NowState == (int)eUIState.End;
        }

        public void OpenMainWindow() {
            if (m_StateController.ChangeState((int)eUIState.Main)) {
                m_MainWindow.Open();
            }
        }

        public void CloseMainWindow() {
            m_MainWindow.Close();
        }

        public void ChangeTab() {
            OnClickCategoryButtonCallBack(m_TabWindow.SelectIdx);
        }

        public void OnClickCategoryButtonCallBack(int index) {
            if (m_CurrentCategory != (eMissionCategory)index && index < m_CategoryTable.Count) {
                ChangeCategory((eMissionCategory)index);
            }
        }

        private void ChangeCategory(eMissionCategory category) {
            m_CurrentCategory = category;
            for (int i = 0; i < m_MissionList.Length; i++) {
                m_MissionList[i].SetActive(m_MissionList[i].Category == category);
            }
            m_MissionList[(int)category].RefreshScrollList();
            m_MissionList[(int)category].SetUMissionListInfo();
            m_MissionList[(int)category].SetUMissionLimitInfo();
        }

        public void CallbackRefresh() {
            for (int i = 0; i < m_MissionList.Length; i++) {
                m_MissionList[i].SetActive(m_MissionList[i].Category == m_CurrentCategory);
            }
        }

        private enum eUIState {
            Hide,
            In,
            Out,
            End,
            Main,
            Num
        }
    }
}
