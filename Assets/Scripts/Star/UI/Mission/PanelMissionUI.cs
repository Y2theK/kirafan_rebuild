﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000FB8 RID: 4024
	[Token(Token = "0x2000A79")]
	[StructLayout(3)]
	public class PanelMissionUI : UIGroup
	{
		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x06004C69 RID: 19561 RVA: 0x0001B1C8 File Offset: 0x000193C8
		[Token(Token = "0x170004E5")]
		public PanelMissionUI.eLocalState localState
		{
			[Token(Token = "0x6004612")]
			[Address(RVA = "0x101506420", Offset = "0x1506420", VA = "0x101506420")]
			get
			{
				return PanelMissionUI.eLocalState.PreOpen;
			}
		}

		// Token: 0x06004C6A RID: 19562 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004613")]
		[Address(RVA = "0x101506428", Offset = "0x1506428", VA = "0x101506428")]
		private void Awake()
		{
		}

		// Token: 0x06004C6B RID: 19563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004614")]
		[Address(RVA = "0x101506684", Offset = "0x1506684", VA = "0x101506684")]
		private void Start()
		{
		}

		// Token: 0x06004C6C RID: 19564 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004615")]
		[Address(RVA = "0x101506688", Offset = "0x1506688", VA = "0x101506688", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004C6D RID: 19565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004616")]
		[Address(RVA = "0x101506A4C", Offset = "0x1506A4C", VA = "0x101506A4C", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06004C6E RID: 19566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004617")]
		[Address(RVA = "0x101506CF0", Offset = "0x1506CF0", VA = "0x101506CF0")]
		public void SetUp(TownState_Home stateHome)
		{
		}

		// Token: 0x06004C6F RID: 19567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004618")]
		[Address(RVA = "0x1015071B8", Offset = "0x15071B8", VA = "0x1015071B8", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004C70 RID: 19568 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004619")]
		[Address(RVA = "0x1015071E4", Offset = "0x15071E4", VA = "0x1015071E4", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004C71 RID: 19569 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600461A")]
		[Address(RVA = "0x1015073DC", Offset = "0x15073DC", VA = "0x1015073DC")]
		public void OnBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x06004C72 RID: 19570 RVA: 0x0001B1E0 File Offset: 0x000193E0
		[Token(Token = "0x600461B")]
		[Address(RVA = "0x1015073E8", Offset = "0x15073E8", VA = "0x1015073E8")]
		public bool isReady()
		{
			return default(bool);
		}

		// Token: 0x06004C73 RID: 19571 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600461C")]
		[Address(RVA = "0x101506BDC", Offset = "0x1506BDC", VA = "0x101506BDC")]
		private void Release()
		{
		}

		// Token: 0x06004C74 RID: 19572 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600461D")]
		[Address(RVA = "0x10150741C", Offset = "0x150741C", VA = "0x10150741C", Slot = "18")]
		protected virtual void StateFunc_None()
		{
		}

		// Token: 0x06004C75 RID: 19573 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600461E")]
		[Address(RVA = "0x101507420", Offset = "0x1507420", VA = "0x101507420", Slot = "19")]
		protected virtual void StateFunc_PreOpen()
		{
		}

		// Token: 0x06004C76 RID: 19574 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600461F")]
		[Address(RVA = "0x101507424", Offset = "0x1507424", VA = "0x101507424", Slot = "20")]
		protected virtual void StateFunc_GetMissionData()
		{
		}

		// Token: 0x06004C77 RID: 19575 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004620")]
		[Address(RVA = "0x101507570", Offset = "0x1507570", VA = "0x101507570", Slot = "21")]
		protected virtual void StateFunc_WaitAPI()
		{
		}

		// Token: 0x06004C78 RID: 19576 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004621")]
		[Address(RVA = "0x101507574", Offset = "0x1507574", VA = "0x101507574", Slot = "22")]
		protected virtual void StateFunc_UISetUp()
		{
		}

		// Token: 0x06004C79 RID: 19577 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004622")]
		[Address(RVA = "0x101507D6C", Offset = "0x1507D6C", VA = "0x101507D6C", Slot = "23")]
		protected virtual void StateFunc_Idle()
		{
		}

		// Token: 0x06004C7A RID: 19578 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004623")]
		[Address(RVA = "0x101507F1C", Offset = "0x1507F1C", VA = "0x101507F1C", Slot = "24")]
		protected virtual void StateFunc_WaitGetRewardAll()
		{
		}

		// Token: 0x06004C7B RID: 19579 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004624")]
		[Address(RVA = "0x1015077B0", Offset = "0x15077B0", VA = "0x1015077B0")]
		private void CreateMissionList(Dictionary<int, IMissionUIData> uiDataList)
		{
		}

		// Token: 0x06004C7C RID: 19580 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004625")]
		[Address(RVA = "0x101507FF0", Offset = "0x1507FF0", VA = "0x101507FF0", Slot = "25")]
		protected virtual void UpdateRewardList(int index, IMissionUIData uiData)
		{
		}

		// Token: 0x06004C7D RID: 19581 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004626")]
		[Address(RVA = "0x101507BA0", Offset = "0x1507BA0", VA = "0x101507BA0")]
		private void UpdateRewardAllBtn()
		{
		}

		// Token: 0x06004C7E RID: 19582 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004627")]
		[Address(RVA = "0x101506F58", Offset = "0x1506F58", VA = "0x101506F58")]
		private void SetNaviTweet(PanelMissionUI.eTweetType tweetType)
		{
		}

		// Token: 0x06004C7F RID: 19583 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004628")]
		[Address(RVA = "0x101508908", Offset = "0x1508908", VA = "0x101508908")]
		public void CB_ViewReward(int index, IMissionUIData uiData)
		{
		}

		// Token: 0x06004C80 RID: 19584 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004629")]
		[Address(RVA = "0x101508960", Offset = "0x1508960", VA = "0x101508960")]
		public void CB_GetReward(int index, IMissionUIData uiData)
		{
		}

		// Token: 0x06004C81 RID: 19585 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462A")]
		[Address(RVA = "0x101508B10", Offset = "0x1508B10", VA = "0x101508B10")]
		public void CB_Complete()
		{
		}

		// Token: 0x06004C82 RID: 19586 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462B")]
		[Address(RVA = "0x101508B20", Offset = "0x1508B20", VA = "0x101508B20")]
		public void CB_RewardDetail(IMissionPakage.IMissionReward reward)
		{
		}

		// Token: 0x06004C83 RID: 19587 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462C")]
		[Address(RVA = "0x101508C68", Offset = "0x1508C68", VA = "0x101508C68")]
		public void CB_ToChallengeScene()
		{
		}

		// Token: 0x06004C84 RID: 19588 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462D")]
		[Address(RVA = "0x101508D7C", Offset = "0x1508D7C", VA = "0x101508D7C")]
		public void CB_GetRewardAll()
		{
		}

		// Token: 0x06004C85 RID: 19589 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462E")]
		[Address(RVA = "0x101509030", Offset = "0x1509030", VA = "0x101509030")]
		private void CB_Cloased()
		{
		}

		// Token: 0x06004C86 RID: 19590 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600462F")]
		[Address(RVA = "0x10150681C", Offset = "0x150681C", VA = "0x10150681C")]
		protected void OptimizeInfo(MissionScrollItemPanel panel)
		{
		}

		// Token: 0x06004C87 RID: 19591 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004630")]
		[Address(RVA = "0x101509058", Offset = "0x1509058", VA = "0x101509058")]
		public PanelMissionUI()
		{
		}

		// Token: 0x04005E6C RID: 24172
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004233")]
		[SerializeField]
		protected eMissionCategory m_targetCategory;

		// Token: 0x04005E6D RID: 24173
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4004234")]
		[SerializeField]
		protected NpcIllust.eNpc m_NaviNpcCharaId;

		// Token: 0x04005E6E RID: 24174
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004235")]
		[SerializeField]
		protected NpcIllust m_imgNavi;

		// Token: 0x04005E6F RID: 24175
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004236")]
		[SerializeField]
		protected Text m_txtNaviTweet;

		// Token: 0x04005E70 RID: 24176
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004237")]
		[SerializeField]
		protected eText_MessageDB[] m_naviMsgId;

		// Token: 0x04005E71 RID: 24177
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004238")]
		[SerializeField]
		protected GameObject m_goRewardList;

		// Token: 0x04005E72 RID: 24178
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004239")]
		[SerializeField]
		protected VariableIconWithFrame[] m_rewardIcons;

		// Token: 0x04005E73 RID: 24179
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400423A")]
		[SerializeField]
		protected GameObject[] m_rewardNums;

		// Token: 0x04005E74 RID: 24180
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400423B")]
		[SerializeField]
		protected CustomButton[] m_btnRewardDetail;

		// Token: 0x04005E75 RID: 24181
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400423C")]
		[SerializeField]
		protected CustomButton m_btnRewardGetAll;

		// Token: 0x04005E76 RID: 24182
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400423D")]
		[SerializeField]
		protected CustomButton m_sceneSkipBtn;

		// Token: 0x04005E77 RID: 24183
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400423E")]
		[SerializeField]
		protected GameObject m_MissionInfoPanelPrefab;

		// Token: 0x04005E78 RID: 24184
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400423F")]
		[SerializeField]
		protected Transform m_tfMissionPanelListParent;

		// Token: 0x04005E79 RID: 24185
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004240")]
		[SerializeField]
		protected ImageNumbers m_missionNo;

		// Token: 0x04005E7A RID: 24186
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004241")]
		[SerializeField]
		protected RectTransform m_rtfListMask;

		// Token: 0x04005E7B RID: 24187
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004242")]
		private Dictionary<long, MissionScrollItemPanel> m_panelList;

		// Token: 0x04005E7C RID: 24188
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004243")]
		protected Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04005E7D RID: 24189
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004244")]
		protected GlobalUI.SceneInfoStack m_InfoStack;

		// Token: 0x04005E7E RID: 24190
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004245")]
		protected PanelMissionUI.eLocalState m_localState;

		// Token: 0x04005E7F RID: 24191
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004246")]
		protected long m_selectedMngId;

		// Token: 0x04005E80 RID: 24192
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004247")]
		protected Dictionary<PanelMissionUI.eLocalState, Action> m_dicLocalStateAction;

		// Token: 0x04005E81 RID: 24193
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004248")]
		private TownState_Home m_stateHome;

		// Token: 0x02000FB9 RID: 4025
		[Token(Token = "0x2001203")]
		private enum eTweetType
		{
			// Token: 0x04005E83 RID: 24195
			[Token(Token = "0x4006F7D")]
			Init,
			// Token: 0x04005E84 RID: 24196
			[Token(Token = "0x4006F7E")]
			MissionComp
		}

		// Token: 0x02000FBA RID: 4026
		[Token(Token = "0x2001204")]
		public enum eLocalState
		{
			// Token: 0x04005E86 RID: 24198
			[Token(Token = "0x4006F80")]
			None = -1,
			// Token: 0x04005E87 RID: 24199
			[Token(Token = "0x4006F81")]
			PreOpen,
			// Token: 0x04005E88 RID: 24200
			[Token(Token = "0x4006F82")]
			GetMissionData,
			// Token: 0x04005E89 RID: 24201
			[Token(Token = "0x4006F83")]
			WaitAPI,
			// Token: 0x04005E8A RID: 24202
			[Token(Token = "0x4006F84")]
			UISetUp,
			// Token: 0x04005E8B RID: 24203
			[Token(Token = "0x4006F85")]
			Idle,
			// Token: 0x04005E8C RID: 24204
			[Token(Token = "0x4006F86")]
			WaitGetRewardAll,
			// Token: 0x04005E8D RID: 24205
			[Token(Token = "0x4006F87")]
			Closed
		}
	}
}
