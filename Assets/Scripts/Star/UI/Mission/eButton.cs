﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Mission
{
	// Token: 0x02000FC8 RID: 4040
	[Token(Token = "0x2000A85")]
	[StructLayout(3, Size = 4)]
	public enum eButton
	{
		// Token: 0x04005EBD RID: 24253
		[Token(Token = "0x4004271")]
		None,
		// Token: 0x04005EBE RID: 24254
		[Token(Token = "0x4004272")]
		Yes,
		// Token: 0x04005EBF RID: 24255
		[Token(Token = "0x4004273")]
		No,
		// Token: 0x04005EC0 RID: 24256
		[Token(Token = "0x4004274")]
		Cancel,
		// Token: 0x04005EC1 RID: 24257
		[Token(Token = "0x4004275")]
		Back
	}
}
