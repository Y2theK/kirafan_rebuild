﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C9B RID: 3227
	[Token(Token = "0x20008A3")]
	[StructLayout(3)]
	public class MixPopUp : MonoBehaviour
	{
		// Token: 0x06003AC1 RID: 15041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003584")]
		[Address(RVA = "0x101509190", Offset = "0x1509190", VA = "0x101509190")]
		public void PlayIn(int successLv = -1)
		{
		}

		// Token: 0x06003AC2 RID: 15042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003585")]
		[Address(RVA = "0x1015092DC", Offset = "0x15092DC", VA = "0x1015092DC")]
		public void PlayOut()
		{
		}

		// Token: 0x06003AC3 RID: 15043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003586")]
		[Address(RVA = "0x10150930C", Offset = "0x150930C", VA = "0x10150930C")]
		public void PlayInOut(int successLv)
		{
		}

		// Token: 0x06003AC4 RID: 15044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003587")]
		[Address(RVA = "0x10150933C", Offset = "0x150933C", VA = "0x10150933C")]
		private void Update()
		{
		}

		// Token: 0x06003AC5 RID: 15045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003588")]
		[Address(RVA = "0x1015093CC", Offset = "0x15093CC", VA = "0x1015093CC")]
		public MixPopUp()
		{
		}

		// Token: 0x04004984 RID: 18820
		[Token(Token = "0x40033AD")]
		private const float INTERVAL_INOUT = 1f;

		// Token: 0x04004985 RID: 18821
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033AE")]
		[SerializeField]
		private GameObject m_Great;

		// Token: 0x04004986 RID: 18822
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033AF")]
		[SerializeField]
		private GameObject m_Big;

		// Token: 0x04004987 RID: 18823
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033B0")]
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04004988 RID: 18824
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40033B1")]
		private bool m_AutoOut;

		// Token: 0x04004989 RID: 18825
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40033B2")]
		private float m_Wait;
	}
}
