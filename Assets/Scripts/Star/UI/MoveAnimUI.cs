﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C6F RID: 3183
	[Token(Token = "0x200088C")]
	[StructLayout(3)]
	public class MoveAnimUI : AnimUIBase
	{
		// Token: 0x1700047C RID: 1148
		// (set) Token: 0x060039DB RID: 14811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000427")]
		public Vector3 ShowPosOffset
		{
			[Token(Token = "0x60034C2")]
			[Address(RVA = "0x1015093D4", Offset = "0x15093D4", VA = "0x1015093D4")]
			set
			{
			}
		}

		// Token: 0x060039DC RID: 14812 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C3")]
		[Address(RVA = "0x1015093E0", Offset = "0x15093E0", VA = "0x1015093E0", Slot = "6")]
		protected override void Prepare()
		{
		}

		// Token: 0x060039DD RID: 14813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C4")]
		[Address(RVA = "0x1015095F8", Offset = "0x15095F8", VA = "0x1015095F8", Slot = "9")]
		protected override void ExecutePlayIn()
		{
		}

		// Token: 0x060039DE RID: 14814 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C5")]
		[Address(RVA = "0x101509C04", Offset = "0x1509C04", VA = "0x101509C04", Slot = "10")]
		protected override void ExecutePlayOut()
		{
		}

		// Token: 0x060039DF RID: 14815 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C6")]
		[Address(RVA = "0x10150A220", Offset = "0x150A220", VA = "0x10150A220", Slot = "5")]
		public override void Hide()
		{
		}

		// Token: 0x060039E0 RID: 14816 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C7")]
		[Address(RVA = "0x10150A31C", Offset = "0x150A31C", VA = "0x10150A31C", Slot = "4")]
		public override void Show()
		{
		}

		// Token: 0x060039E1 RID: 14817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C8")]
		[Address(RVA = "0x10150A470", Offset = "0x150A470", VA = "0x10150A470")]
		public void OnCompleteInMove()
		{
		}

		// Token: 0x060039E2 RID: 14818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034C9")]
		[Address(RVA = "0x10150A58C", Offset = "0x150A58C", VA = "0x10150A58C")]
		public void OnCompleteOutMove()
		{
		}

		// Token: 0x060039E3 RID: 14819 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CA")]
		[Address(RVA = "0x10150A650", Offset = "0x150A650", VA = "0x10150A650")]
		public MoveAnimUI()
		{
		}

		// Token: 0x040048AF RID: 18607
		[Token(Token = "0x4003317")]
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x040048B0 RID: 18608
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003318")]
		[SerializeField]
		private bool m_ReturnMode;

		// Token: 0x040048B1 RID: 18609
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003319")]
		[SerializeField]
		private bool m_Restart;

		// Token: 0x040048B2 RID: 18610
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400331A")]
		[SerializeField]
		private Vector3 m_HidePos;

		// Token: 0x040048B3 RID: 18611
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400331B")]
		[SerializeField]
		private Vector3 m_EndPos;

		// Token: 0x040048B4 RID: 18612
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x400331C")]
		private Vector3 m_ShowPos;

		// Token: 0x040048B5 RID: 18613
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400331D")]
		private Vector3 m_ShowPosOffset;

		// Token: 0x040048B6 RID: 18614
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x400331E")]
		private Vector3 m_FixedHidePos;

		// Token: 0x040048B7 RID: 18615
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400331F")]
		private Vector3 m_FixedEndPos;

		// Token: 0x040048B8 RID: 18616
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x4003320")]
		[SerializeField]
		private float m_AnimDuration;

		// Token: 0x040048B9 RID: 18617
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003321")]
		[SerializeField]
		private iTween.EaseType m_EaseType;

		// Token: 0x040048BA RID: 18618
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003322")]
		private GameObject m_GameObject;

		// Token: 0x040048BB RID: 18619
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003323")]
		private RectTransform m_RectTransform;
	}
}
