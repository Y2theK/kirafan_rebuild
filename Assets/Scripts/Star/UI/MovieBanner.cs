﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CEB RID: 3307
	[Token(Token = "0x20008D6")]
	[StructLayout(3)]
	public class MovieBanner : ASyncImage
	{
		// Token: 0x06003C80 RID: 15488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373D")]
		[Address(RVA = "0x10150A6FC", Offset = "0x150A6FC", VA = "0x10150A6FC")]
		public void Apply(int bannerId)
		{
		}

		// Token: 0x06003C81 RID: 15489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373E")]
		[Address(RVA = "0x10150A80C", Offset = "0x150A80C", VA = "0x10150A80C", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C82 RID: 15490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600373F")]
		[Address(RVA = "0x10150A838", Offset = "0x150A838", VA = "0x10150A838")]
		public MovieBanner()
		{
		}

		// Token: 0x04004BD3 RID: 19411
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400354D")]
		protected int m_BannerID;
	}
}
