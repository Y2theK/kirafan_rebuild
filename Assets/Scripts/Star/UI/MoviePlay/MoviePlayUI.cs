﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Star.UI.MoviePlay
{
	// Token: 0x02000EE6 RID: 3814
	[Token(Token = "0x2000A16")]
	[StructLayout(3)]
	public class MoviePlayUI : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x140000B8 RID: 184
		// (add) Token: 0x060047CE RID: 18382 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047CF RID: 18383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B8")]
		private event Action OnConfirmSkip
		{
			[Token(Token = "0x6004210")]
			[Address(RVA = "0x10150A848", Offset = "0x150A848", VA = "0x10150A848")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004211")]
			[Address(RVA = "0x10150A954", Offset = "0x150A954", VA = "0x10150A954")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B9 RID: 185
		// (add) Token: 0x060047D0 RID: 18384 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047D1 RID: 18385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B9")]
		private event Action OnExecSkip
		{
			[Token(Token = "0x6004212")]
			[Address(RVA = "0x10150AA60", Offset = "0x150AA60", VA = "0x10150AA60")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004213")]
			[Address(RVA = "0x10150AB6C", Offset = "0x150AB6C", VA = "0x10150AB6C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x060047D2 RID: 18386 RVA: 0x0001A478 File Offset: 0x00018678
		// (set) Token: 0x060047D3 RID: 18387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004D1")]
		public bool IsEnableInput
		{
			[Token(Token = "0x6004214")]
			[Address(RVA = "0x10150AC78", Offset = "0x150AC78", VA = "0x10150AC78")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004215")]
			[Address(RVA = "0x10150AC80", Offset = "0x150AC80", VA = "0x10150AC80")]
			set
			{
			}
		}

		// Token: 0x060047D4 RID: 18388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004216")]
		[Address(RVA = "0x10150AC90", Offset = "0x150AC90", VA = "0x10150AC90")]
		public void Start()
		{
		}

		// Token: 0x060047D5 RID: 18389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004217")]
		[Address(RVA = "0x10150ACE8", Offset = "0x150ACE8", VA = "0x10150ACE8")]
		public void Setup(MoviePlayUI.eSkipMode mode)
		{
		}

		// Token: 0x060047D6 RID: 18390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004218")]
		[Address(RVA = "0x10150AD18", Offset = "0x150AD18", VA = "0x10150AD18")]
		public void Destroy()
		{
		}

		// Token: 0x060047D7 RID: 18391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004219")]
		[Address(RVA = "0x10150AD24", Offset = "0x150AD24", VA = "0x10150AD24")]
		private void Update()
		{
		}

		// Token: 0x060047D8 RID: 18392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600421A")]
		[Address(RVA = "0x10150AF44", Offset = "0x150AF44", VA = "0x10150AF44")]
		private void OnDestroy()
		{
		}

		// Token: 0x060047D9 RID: 18393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600421B")]
		[Address(RVA = "0x10150AF50", Offset = "0x150AF50", VA = "0x10150AF50", Slot = "4")]
		public void OnPointerClick(PointerEventData eventData)
		{
		}

		// Token: 0x060047DA RID: 18394 RVA: 0x0001A490 File Offset: 0x00018690
		[Token(Token = "0x600421C")]
		[Address(RVA = "0x10150AF54", Offset = "0x150AF54", VA = "0x10150AF54")]
		private bool CheckEnableSkipButtonOnTap()
		{
			return default(bool);
		}

		// Token: 0x060047DB RID: 18395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600421D")]
		[Address(RVA = "0x10150ACEC", Offset = "0x150ACEC", VA = "0x10150ACEC")]
		public void SetSkipMode(MoviePlayUI.eSkipMode mode)
		{
		}

		// Token: 0x060047DC RID: 18396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600421E")]
		[Address(RVA = "0x10150AFE0", Offset = "0x150AFE0", VA = "0x10150AFE0")]
		public void SetConfirmSkip(Action onConfirmSkip)
		{
		}

		// Token: 0x060047DD RID: 18397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600421F")]
		[Address(RVA = "0x10150AFE8", Offset = "0x150AFE8", VA = "0x10150AFE8")]
		public void SetExecSkip(Action onExecSkip)
		{
		}

		// Token: 0x060047DE RID: 18398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004220")]
		[Address(RVA = "0x10150AC88", Offset = "0x150AC88", VA = "0x10150AC88")]
		public void EnableInput(bool flag)
		{
		}

		// Token: 0x060047DF RID: 18399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004221")]
		[Address(RVA = "0x10150AC98", Offset = "0x150AC98", VA = "0x10150AC98")]
		public void SetEnableSkipButton(bool flg)
		{
		}

		// Token: 0x060047E0 RID: 18400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004222")]
		[Address(RVA = "0x10150AFF0", Offset = "0x150AFF0", VA = "0x10150AFF0")]
		public void OpenSkipWindow()
		{
		}

		// Token: 0x060047E1 RID: 18401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004223")]
		[Address(RVA = "0x10150B250", Offset = "0x150B250", VA = "0x10150B250")]
		private void OnConfirmSkipWindow(int answer)
		{
		}

		// Token: 0x060047E2 RID: 18402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004224")]
		[Address(RVA = "0x10150AEEC", Offset = "0x150AEEC", VA = "0x10150AEEC")]
		private void UpdateAndroidBackKey()
		{
		}

		// Token: 0x060047E3 RID: 18403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004225")]
		[Address(RVA = "0x10150B314", Offset = "0x150B314", VA = "0x10150B314")]
		public MoviePlayUI()
		{
		}

		// Token: 0x0400598C RID: 22924
		[Token(Token = "0x4003F0E")]
		private const float SKIP_BTN_DISPLAY_SEC_ON_TAP = 5f;

		// Token: 0x0400598D RID: 22925
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003F0F")]
		[SerializeField]
		private MovieCanvas m_MovieCanvas;

		// Token: 0x0400598E RID: 22926
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003F10")]
		[SerializeField]
		private CustomButton m_SkipButton;

		// Token: 0x0400598F RID: 22927
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003F11")]
		private MoviePlayUI.eSkipMode m_SkipMode;

		// Token: 0x04005990 RID: 22928
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003F12")]
		private float m_SkipBtnDisplayTimer;

		// Token: 0x04005991 RID: 22929
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003F13")]
		private bool m_IsSkipBtnDisplayTimerUpdate;

		// Token: 0x04005992 RID: 22930
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003F14")]
		private bool m_IsEnableInput;

		// Token: 0x02000EE7 RID: 3815
		[Token(Token = "0x2001196")]
		public enum eSkipMode
		{
			// Token: 0x04005996 RID: 22934
			[Token(Token = "0x4006DC8")]
			Disable,
			// Token: 0x04005997 RID: 22935
			[Token(Token = "0x4006DC9")]
			Always,
			// Token: 0x04005998 RID: 22936
			[Token(Token = "0x4006DCA")]
			Tap
		}
	}
}
