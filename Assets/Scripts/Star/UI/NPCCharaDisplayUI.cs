﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DF0 RID: 3568
	[Token(Token = "0x2000993")]
	[StructLayout(3)]
	public class NPCCharaDisplayUI : MenuUIBase
	{
		// Token: 0x060041FB RID: 16891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C81")]
		[Address(RVA = "0x10150B324", Offset = "0x150B324", VA = "0x10150B324", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060041FC RID: 16892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C82")]
		[Address(RVA = "0x10150B330", Offset = "0x150B330", VA = "0x10150B330")]
		public void SetCompletePlayInCallBack(Action action)
		{
		}

		// Token: 0x060041FD RID: 16893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C83")]
		[Address(RVA = "0x10150B338", Offset = "0x150B338", VA = "0x10150B338")]
		public void PlayInVoice(eSoundVoiceControllListDB voiceCtrl)
		{
		}

		// Token: 0x060041FE RID: 16894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C84")]
		[Address(RVA = "0x10150B348", Offset = "0x150B348", VA = "0x10150B348")]
		public void Setup()
		{
		}

		// Token: 0x060041FF RID: 16895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C85")]
		[Address(RVA = "0x10150B3F4", Offset = "0x150B3F4", VA = "0x10150B3F4")]
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrder, int offset)
		{
		}

		// Token: 0x06004200 RID: 16896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C86")]
		[Address(RVA = "0x10150B474", Offset = "0x150B474", VA = "0x10150B474")]
		private void Update()
		{
		}

		// Token: 0x06004201 RID: 16897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C87")]
		[Address(RVA = "0x10150B9E4", Offset = "0x150B9E4", VA = "0x10150B9E4")]
		private void ChangeStep(NPCCharaDisplayUI.eStep step)
		{
		}

		// Token: 0x06004202 RID: 16898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C88")]
		[Address(RVA = "0x10150BAEC", Offset = "0x150BAEC", VA = "0x10150BAEC", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004203 RID: 16899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C89")]
		[Address(RVA = "0x10150BB8C", Offset = "0x150BB8C", VA = "0x10150BB8C")]
		public void PlayInNpc(NpcIllust.eNpc npc)
		{
		}

		// Token: 0x06004204 RID: 16900 RVA: 0x00019350 File Offset: 0x00017550
		[Token(Token = "0x6003C8A")]
		[Address(RVA = "0x10150B918", Offset = "0x150B918", VA = "0x10150B918")]
		public Vector2 GetFixedPosition(NpcIllust.eNpc npc)
		{
			return default(Vector2);
		}

		// Token: 0x06004205 RID: 16901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C8B")]
		[Address(RVA = "0x10150BDEC", Offset = "0x150BDEC", VA = "0x10150BDEC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004206 RID: 16902 RVA: 0x00019368 File Offset: 0x00017568
		[Token(Token = "0x6003C8C")]
		[Address(RVA = "0x10150BEF0", Offset = "0x150BEF0", VA = "0x10150BEF0", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004207 RID: 16903 RVA: 0x00019380 File Offset: 0x00017580
		[Token(Token = "0x6003C8D")]
		[Address(RVA = "0x10150BF00", Offset = "0x150BF00", VA = "0x10150BF00")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06004208 RID: 16904 RVA: 0x00019398 File Offset: 0x00017598
		[Token(Token = "0x6003C8E")]
		[Address(RVA = "0x10150BF10", Offset = "0x150BF10", VA = "0x10150BF10")]
		public bool IsDisplay()
		{
			return default(bool);
		}

		// Token: 0x06004209 RID: 16905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C8F")]
		[Address(RVA = "0x10150BDD4", Offset = "0x150BDD4", VA = "0x10150BDD4")]
		public void SetOffset(Vector2 offset, bool immediate)
		{
		}

		// Token: 0x0600420A RID: 16906 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C90")]
		[Address(RVA = "0x10150BF20", Offset = "0x150BF20", VA = "0x10150BF20")]
		public void SetOffset(Vector2 offset, float sec)
		{
		}

		// Token: 0x0600420B RID: 16907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C91")]
		[Address(RVA = "0x10150C000", Offset = "0x150C000", VA = "0x10150C000")]
		public NPCCharaDisplayUI()
		{
		}

		// Token: 0x040051F2 RID: 20978
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40039FE")]
		private readonly Vector2[] POSITIONS;

		// Token: 0x040051F3 RID: 20979
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40039FF")]
		private readonly Vector3[] SCALES;

		// Token: 0x040051F4 RID: 20980
		[Token(Token = "0x4003A00")]
		private const float DEFAULT_MOVESEC = 0.2f;

		// Token: 0x040051F5 RID: 20981
		[Token(Token = "0x4003A01")]
		private const int SPEED_COEF = 2;

		// Token: 0x040051F6 RID: 20982
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003A02")]
		private NPCCharaDisplayUI.eStep m_Step;

		// Token: 0x040051F7 RID: 20983
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A03")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040051F8 RID: 20984
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A04")]
		[SerializeField]
		private RectTransform m_OffsetObj;

		// Token: 0x040051F9 RID: 20985
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A05")]
		[SerializeField]
		private NpcIllust m_NPCIllust;

		// Token: 0x040051FA RID: 20986
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A06")]
		private NpcIllust.eNpc m_Current;

		// Token: 0x040051FB RID: 20987
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003A07")]
		private Action m_OnCompletePlayIn;

		// Token: 0x040051FC RID: 20988
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003A08")]
		private bool m_IsReserveVocieCtrl;

		// Token: 0x040051FD RID: 20989
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003A09")]
		private eSoundVoiceControllListDB m_VoiceCtrl;

		// Token: 0x040051FE RID: 20990
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003A0A")]
		private int m_VoiceReqID;

		// Token: 0x040051FF RID: 20991
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003A0B")]
		private Vector2 m_Offset;

		// Token: 0x04005200 RID: 20992
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003A0C")]
		private Vector2 m_StartPos;

		// Token: 0x04005201 RID: 20993
		[Cpp2IlInjected.FieldOffset(Offset = "0x9C")]
		[Token(Token = "0x4003A0D")]
		private float m_LerpSec;

		// Token: 0x04005202 RID: 20994
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003A0E")]
		private float m_LerpTimeCount;

		// Token: 0x02000DF1 RID: 3569
		[Token(Token = "0x2001123")]
		private enum eStep
		{
			// Token: 0x04005204 RID: 20996
			[Token(Token = "0x4006B3E")]
			Hide,
			// Token: 0x04005205 RID: 20997
			[Token(Token = "0x4006B3F")]
			In,
			// Token: 0x04005206 RID: 20998
			[Token(Token = "0x4006B40")]
			Idle,
			// Token: 0x04005207 RID: 20999
			[Token(Token = "0x4006B41")]
			Out,
			// Token: 0x04005208 RID: 21000
			[Token(Token = "0x4006B42")]
			End
		}
	}
}
