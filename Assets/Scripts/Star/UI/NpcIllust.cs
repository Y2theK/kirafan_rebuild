﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CEC RID: 3308
	[Token(Token = "0x20008D7")]
	[StructLayout(3)]
	public class NpcIllust : ASyncImage
	{
		// Token: 0x06003C83 RID: 15491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003740")]
		[Address(RVA = "0x1015070AC", Offset = "0x15070AC", VA = "0x1015070AC")]
		public void Apply(NpcIllust.eNpc npc)
		{
		}

		// Token: 0x06003C84 RID: 15492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003741")]
		[Address(RVA = "0x10150C518", Offset = "0x150C518", VA = "0x10150C518", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C85 RID: 15493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003742")]
		[Address(RVA = "0x10150C540", Offset = "0x150C540", VA = "0x10150C540")]
		public NpcIllust()
		{
		}

		// Token: 0x04004BD4 RID: 19412
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400354E")]
		private NpcIllust.eNpc m_Current;

		// Token: 0x02000CED RID: 3309
		[Token(Token = "0x20010DC")]
		public enum eNpc
		{
			// Token: 0x04004BD6 RID: 19414
			[Token(Token = "0x40069D6")]
			None,
			// Token: 0x04004BD7 RID: 19415
			[Token(Token = "0x40069D7")]
			Master,
			// Token: 0x04004BD8 RID: 19416
			[Token(Token = "0x40069D8")]
			Summon,
			// Token: 0x04004BD9 RID: 19417
			[Token(Token = "0x40069D9")]
			Trade,
			// Token: 0x04004BDA RID: 19418
			[Token(Token = "0x40069DA")]
			Weapon,
			// Token: 0x04004BDB RID: 19419
			[Token(Token = "0x40069DB")]
			Build,
			// Token: 0x04004BDC RID: 19420
			[Token(Token = "0x40069DC")]
			Training,
			// Token: 0x04004BDD RID: 19421
			[Token(Token = "0x40069DD")]
			Lamp
		}
	}
}
