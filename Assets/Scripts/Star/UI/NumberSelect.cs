﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C9C RID: 3228
	[Token(Token = "0x20008A4")]
	[StructLayout(3)]
	public class NumberSelect : MonoBehaviour
	{
		// Token: 0x1400006B RID: 107
		// (add) Token: 0x06003AC6 RID: 15046 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003AC7 RID: 15047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400006B")]
		public event Action OnChangeCurrentNum
		{
			[Token(Token = "0x6003589")]
			[Address(RVA = "0x10150C548", Offset = "0x150C548", VA = "0x10150C548")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600358A")]
			[Address(RVA = "0x10150C654", Offset = "0x150C654", VA = "0x10150C654")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06003AC8 RID: 15048 RVA: 0x000183C0 File Offset: 0x000165C0
		// (set) Token: 0x06003AC9 RID: 15049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000431")]
		public bool AddInteractable
		{
			[Token(Token = "0x600358B")]
			[Address(RVA = "0x10150C760", Offset = "0x150C760", VA = "0x10150C760")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600358C")]
			[Address(RVA = "0x10150C768", Offset = "0x150C768", VA = "0x10150C768")]
			set
			{
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x06003ACA RID: 15050 RVA: 0x000183D8 File Offset: 0x000165D8
		// (set) Token: 0x06003ACB RID: 15051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000432")]
		public bool SubInteractable
		{
			[Token(Token = "0x600358D")]
			[Address(RVA = "0x10150C810", Offset = "0x150C810", VA = "0x10150C810")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600358E")]
			[Address(RVA = "0x10150C818", Offset = "0x150C818", VA = "0x10150C818")]
			set
			{
			}
		}

		// Token: 0x06003ACC RID: 15052 RVA: 0x000183F0 File Offset: 0x000165F0
		[Token(Token = "0x600358F")]
		[Address(RVA = "0x10150C824", Offset = "0x150C824", VA = "0x10150C824")]
		public int GetCurrentNum()
		{
			return 0;
		}

		// Token: 0x06003ACD RID: 15053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003590")]
		[Address(RVA = "0x10150C82C", Offset = "0x150C82C", VA = "0x10150C82C")]
		public void SetCurrentNum(int value, bool enableCallback = true)
		{
		}

		// Token: 0x06003ACE RID: 15054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003591")]
		[Address(RVA = "0x10150C774", Offset = "0x150C774", VA = "0x10150C774")]
		public void SetRange(int min, int max)
		{
		}

		// Token: 0x06003ACF RID: 15055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003592")]
		[Address(RVA = "0x10150C9A0", Offset = "0x150C9A0", VA = "0x10150C9A0")]
		public void OnClickSubButton()
		{
		}

		// Token: 0x06003AD0 RID: 15056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003593")]
		[Address(RVA = "0x10150C9A4", Offset = "0x150C9A4", VA = "0x10150C9A4")]
		public void OnClickAddButton()
		{
		}

		// Token: 0x06003AD1 RID: 15057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003594")]
		[Address(RVA = "0x10150C9A8", Offset = "0x150C9A8", VA = "0x10150C9A8")]
		public void StartHoldSub()
		{
		}

		// Token: 0x06003AD2 RID: 15058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003595")]
		[Address(RVA = "0x10150CA54", Offset = "0x150CA54", VA = "0x10150CA54")]
		public void StartHoldAdd()
		{
		}

		// Token: 0x06003AD3 RID: 15059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003596")]
		[Address(RVA = "0x10150C988", Offset = "0x150C988", VA = "0x10150C988")]
		public void EndHoldSub()
		{
		}

		// Token: 0x06003AD4 RID: 15060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003597")]
		[Address(RVA = "0x10150C994", Offset = "0x150C994", VA = "0x10150C994")]
		public void EndHoldAdd()
		{
		}

		// Token: 0x06003AD5 RID: 15061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003598")]
		[Address(RVA = "0x10150CB00", Offset = "0x150CB00", VA = "0x10150CB00")]
		private void Start()
		{
		}

		// Token: 0x06003AD6 RID: 15062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003599")]
		[Address(RVA = "0x10150CB50", Offset = "0x150CB50", VA = "0x10150CB50")]
		private void Update()
		{
		}

		// Token: 0x06003AD7 RID: 15063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359A")]
		[Address(RVA = "0x10150CC9C", Offset = "0x150CC9C", VA = "0x10150CC9C")]
		public void SetChangeCurrentNumCallBack(Action callBack)
		{
		}

		// Token: 0x06003AD8 RID: 15064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359B")]
		[Address(RVA = "0x10150CCA4", Offset = "0x150CCA4", VA = "0x10150CCA4")]
		public void UpdateColor()
		{
		}

		// Token: 0x06003AD9 RID: 15065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359C")]
		[Address(RVA = "0x10150CD24", Offset = "0x150CD24", VA = "0x10150CD24")]
		public NumberSelect()
		{
		}

		// Token: 0x0400498A RID: 18826
		[Token(Token = "0x40033B3")]
		private const float HOLD_START_WAIT = 0.4f;

		// Token: 0x0400498B RID: 18827
		[Token(Token = "0x40033B4")]
		private const float HOLD_REPEAT_WAIT = 0.075f;

		// Token: 0x0400498C RID: 18828
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033B5")]
		[SerializeField]
		private CustomButton m_AddButton;

		// Token: 0x0400498D RID: 18829
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033B6")]
		[SerializeField]
		private CustomButton m_SubButton;

		// Token: 0x0400498E RID: 18830
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033B7")]
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x0400498F RID: 18831
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40033B8")]
		[SerializeField]
		private AnimationCurve m_CurveNumberAccel;

		// Token: 0x04004990 RID: 18832
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40033B9")]
		private int m_CurrentNum;

		// Token: 0x04004991 RID: 18833
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40033BA")]
		private int m_Min;

		// Token: 0x04004992 RID: 18834
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40033BB")]
		private int m_Max;

		// Token: 0x04004993 RID: 18835
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40033BC")]
		private bool m_HoldSub;

		// Token: 0x04004994 RID: 18836
		[Cpp2IlInjected.FieldOffset(Offset = "0x45")]
		[Token(Token = "0x40033BD")]
		private bool m_HoldAdd;

		// Token: 0x04004995 RID: 18837
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40033BE")]
		private float m_SubWait;

		// Token: 0x04004996 RID: 18838
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40033BF")]
		private float m_AddWait;

		// Token: 0x04004997 RID: 18839
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40033C0")]
		private float m_SubHoldTime;

		// Token: 0x04004998 RID: 18840
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40033C1")]
		private float m_AddHoldTime;

		// Token: 0x04004999 RID: 18841
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40033C2")]
		private bool m_AddInteractable;

		// Token: 0x0400499A RID: 18842
		[Cpp2IlInjected.FieldOffset(Offset = "0x59")]
		[Token(Token = "0x40033C3")]
		private bool m_SubInteractable;
	}
}
