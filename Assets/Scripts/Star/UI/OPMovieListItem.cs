﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DF3 RID: 3571
	[Token(Token = "0x2000995")]
	[StructLayout(3)]
	public class OPMovieListItem : ScrollItemIcon
	{
		// Token: 0x0600420E RID: 16910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C94")]
		[Address(RVA = "0x10150D0D0", Offset = "0x150D0D0", VA = "0x10150D0D0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600420F RID: 16911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C95")]
		[Address(RVA = "0x10150D1E0", Offset = "0x150D1E0", VA = "0x10150D1E0")]
		public OPMovieListItem()
		{
		}

		// Token: 0x0400520B RID: 21003
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A11")]
		[SerializeField]
		private MovieBanner m_Icon;
	}
}
