﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DF2 RID: 3570
	[Token(Token = "0x2000994")]
	[StructLayout(3)]
	public class OPMovieListItemData : ScrollItemData
	{
		// Token: 0x0600420C RID: 16908 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C92")]
		[Address(RVA = "0x10150D1E8", Offset = "0x150D1E8", VA = "0x10150D1E8", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x0600420D RID: 16909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C93")]
		[Address(RVA = "0x10150D28C", Offset = "0x150D28C", VA = "0x10150D28C")]
		public OPMovieListItemData()
		{
		}

		// Token: 0x04005209 RID: 21001
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A0F")]
		public MoviePlayListDB_Param m_Param;

		// Token: 0x0400520A RID: 21002
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A10")]
		public Action<string> m_OnClickCallback;
	}
}
