﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D70 RID: 3440
	[Token(Token = "0x2000937")]
	[StructLayout(3)]
	public class OfferCharaIcon : MonoBehaviour
	{
		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x06003F68 RID: 16232 RVA: 0x00018D98 File Offset: 0x00016F98
		[Token(Token = "0x17000479")]
		public int CharaID
		{
			[Token(Token = "0x6003A15")]
			[Address(RVA = "0x10150D294", Offset = "0x150D294", VA = "0x10150D294")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06003F69 RID: 16233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A16")]
		[Address(RVA = "0x10150D29C", Offset = "0x150D29C", VA = "0x10150D29C")]
		public void Apply(int charaID, OfferCharaIcon.eState state)
		{
		}

		// Token: 0x06003F6A RID: 16234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A17")]
		[Address(RVA = "0x10150D460", Offset = "0x150D460", VA = "0x10150D460")]
		public void Destroy()
		{
		}

		// Token: 0x06003F6B RID: 16235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A18")]
		[Address(RVA = "0x10150D498", Offset = "0x150D498", VA = "0x10150D498")]
		public void SetActive(bool isActive)
		{
		}

		// Token: 0x06003F6C RID: 16236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A19")]
		[Address(RVA = "0x10150D3A8", Offset = "0x150D3A8", VA = "0x10150D3A8")]
		public void SetCursor(bool isActive)
		{
		}

		// Token: 0x06003F6D RID: 16237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1A")]
		[Address(RVA = "0x10150D3E0", Offset = "0x150D3E0", VA = "0x10150D3E0")]
		public void SetLock(bool isActive)
		{
		}

		// Token: 0x06003F6E RID: 16238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1B")]
		[Address(RVA = "0x10150D428", Offset = "0x150D428", VA = "0x10150D428")]
		public void SetAttentionBadge(bool isActive)
		{
		}

		// Token: 0x06003F6F RID: 16239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1C")]
		[Address(RVA = "0x10150D4D4", Offset = "0x150D4D4", VA = "0x10150D4D4")]
		public void OnClickCallback()
		{
		}

		// Token: 0x06003F70 RID: 16240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1D")]
		[Address(RVA = "0x10150D528", Offset = "0x150D528", VA = "0x10150D528")]
		public OfferCharaIcon()
		{
		}

		// Token: 0x04004E9E RID: 20126
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400375C")]
		[SerializeField]
		private Image m_BG;

		// Token: 0x04004E9F RID: 20127
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400375D")]
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x04004EA0 RID: 20128
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400375E")]
		[SerializeField]
		private GameObject m_Cursor;

		// Token: 0x04004EA1 RID: 20129
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400375F")]
		[SerializeField]
		private GameObject m_LockIcon;

		// Token: 0x04004EA2 RID: 20130
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003760")]
		[SerializeField]
		private GameObject m_AttentionBadge;

		// Token: 0x04004EA3 RID: 20131
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003761")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04004EA4 RID: 20132
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003762")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04004EA5 RID: 20133
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003763")]
		private int m_CharaID;

		// Token: 0x04004EA6 RID: 20134
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003764")]
		private OfferCharaIcon.eState m_State;

		// Token: 0x04004EA7 RID: 20135
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003765")]
		public Action<int, OfferCharaIcon.eState> m_OnClickCallback;

		// Token: 0x02000D71 RID: 3441
		[Token(Token = "0x20010FF")]
		public enum eState
		{
			// Token: 0x04004EA9 RID: 20137
			[Token(Token = "0x4006A8C")]
			Enable,
			// Token: 0x04004EAA RID: 20138
			[Token(Token = "0x4006A8D")]
			BadgeOrder,
			// Token: 0x04004EAB RID: 20139
			[Token(Token = "0x4006A8E")]
			BadgeComplete,
			// Token: 0x04004EAC RID: 20140
			[Token(Token = "0x4006A8F")]
			Disable
		}

		// Token: 0x02000D72 RID: 3442
		[Token(Token = "0x2001100")]
		public class OfferCharaData
		{
			// Token: 0x06003F71 RID: 16241 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006206")]
			[Address(RVA = "0x10150D538", Offset = "0x150D538", VA = "0x10150D538")]
			public OfferCharaData()
			{
			}

			// Token: 0x06003F72 RID: 16242 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006207")]
			[Address(RVA = "0x10150D540", Offset = "0x150D540", VA = "0x10150D540")]
			public OfferCharaData(int charaID, OfferCharaIcon.eState state)
			{
			}

			// Token: 0x06003F73 RID: 16243 RVA: 0x00018DB0 File Offset: 0x00016FB0
			[Token(Token = "0x6006208")]
			[Address(RVA = "0x10150D418", Offset = "0x150D418", VA = "0x10150D418")]
			public static bool IsBadge(OfferCharaIcon.eState state)
			{
				return default(bool);
			}

			// Token: 0x06003F74 RID: 16244 RVA: 0x00018DC8 File Offset: 0x00016FC8
			[Token(Token = "0x6006209")]
			[Address(RVA = "0x10150D578", Offset = "0x150D578", VA = "0x10150D578")]
			public static int Sort(OfferCharaIcon.OfferCharaData a, OfferCharaIcon.OfferCharaData b)
			{
				return 0;
			}

			// Token: 0x04004EAD RID: 20141
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A90")]
			public int m_CharaID;

			// Token: 0x04004EAE RID: 20142
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006A91")]
			public OfferCharaIcon.eState m_State;
		}
	}
}
