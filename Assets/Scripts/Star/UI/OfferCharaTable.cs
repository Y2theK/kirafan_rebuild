﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D73 RID: 3443
	[Token(Token = "0x2000938")]
	[StructLayout(3)]
	public class OfferCharaTable : MonoBehaviour
	{
		// Token: 0x06003F75 RID: 16245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1E")]
		[Address(RVA = "0x10150D5F8", Offset = "0x150D5F8", VA = "0x10150D5F8")]
		public void Apply(List<OfferCharaIcon.OfferCharaData> dataList, int currentCharaID)
		{
		}

		// Token: 0x06003F76 RID: 16246 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A1F")]
		[Address(RVA = "0x10150D600", Offset = "0x150D600", VA = "0x10150D600")]
		public void Apply(List<OfferCharaIcon.OfferCharaData> dataList)
		{
		}

		// Token: 0x06003F77 RID: 16247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A20")]
		[Address(RVA = "0x10150DB08", Offset = "0x150DB08", VA = "0x10150DB08")]
		public void Destroy()
		{
		}

		// Token: 0x06003F78 RID: 16248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A21")]
		[Address(RVA = "0x10150DCBC", Offset = "0x150DCBC", VA = "0x10150DCBC")]
		private void OnClickCharaIconCallback(int charaID, OfferCharaIcon.eState state)
		{
		}

		// Token: 0x06003F79 RID: 16249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A22")]
		[Address(RVA = "0x10150D8C0", Offset = "0x150D8C0", VA = "0x10150D8C0")]
		private void ApplyPage(int page, int currentCharaID)
		{
		}

		// Token: 0x06003F7A RID: 16250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A23")]
		[Address(RVA = "0x10150E020", Offset = "0x150E020", VA = "0x10150E020")]
		public void OnClickNextPageButton()
		{
		}

		// Token: 0x06003F7B RID: 16251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A24")]
		[Address(RVA = "0x10150E03C", Offset = "0x150E03C", VA = "0x10150E03C")]
		public void OnClickPrevPageButton()
		{
		}

		// Token: 0x06003F7C RID: 16252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A25")]
		[Address(RVA = "0x10150E05C", Offset = "0x150E05C", VA = "0x10150E05C")]
		public OfferCharaTable()
		{
		}

		// Token: 0x04004EAF RID: 20143
		[Token(Token = "0x4003766")]
		private const int ICON_LISTUP_COUNT = 3;

		// Token: 0x04004EB0 RID: 20144
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003767")]
		[SerializeField]
		private Transform m_CharaIconRoot;

		// Token: 0x04004EB1 RID: 20145
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003768")]
		[SerializeField]
		private OfferCharaIcon m_CharaIconPrefab;

		// Token: 0x04004EB2 RID: 20146
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003769")]
		[SerializeField]
		private CustomButton m_NextPageButton;

		// Token: 0x04004EB3 RID: 20147
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400376A")]
		[SerializeField]
		private CustomButton m_PrevPageButton;

		// Token: 0x04004EB4 RID: 20148
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400376B")]
		private List<OfferCharaIcon> m_CharaIconList;

		// Token: 0x04004EB5 RID: 20149
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400376C")]
		private int m_Page;

		// Token: 0x04004EB6 RID: 20150
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x400376D")]
		private int m_PageMax;

		// Token: 0x04004EB7 RID: 20151
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400376E")]
		private int m_CurrentCharaID;

		// Token: 0x04004EB8 RID: 20152
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400376F")]
		private List<OfferCharaIcon.OfferCharaData> m_DataList;

		// Token: 0x04004EB9 RID: 20153
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003770")]
		public Action<int> m_OnClickPageChangeButton;
	}
}
