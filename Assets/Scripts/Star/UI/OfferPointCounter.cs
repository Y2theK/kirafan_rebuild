﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D74 RID: 3444
	[Token(Token = "0x2000939")]
	[StructLayout(3)]
	public class OfferPointCounter : MonoBehaviour
	{
		// Token: 0x06003F7D RID: 16253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A26")]
		[Address(RVA = "0x10150E06C", Offset = "0x150E06C", VA = "0x10150E06C")]
		public void Apply(int point)
		{
		}

		// Token: 0x06003F7E RID: 16254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A27")]
		[Address(RVA = "0x10150E178", Offset = "0x150E178", VA = "0x10150E178")]
		public OfferPointCounter()
		{
		}

		// Token: 0x04004EBA RID: 20154
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003771")]
		[SerializeField]
		private Text m_Point;

		// Token: 0x04004EBB RID: 20155
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003772")]
		[SerializeField]
		private Text m_Remaining;
	}
}
