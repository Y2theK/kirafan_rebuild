﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D75 RID: 3445
	[Token(Token = "0x200093A")]
	[StructLayout(3)]
	public class OfferStateIcon : MonoBehaviour
	{
		// Token: 0x06003F7F RID: 16255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A28")]
		[Address(RVA = "0x10150E180", Offset = "0x150E180", VA = "0x10150E180")]
		public void Apply(OfferDefine.eState state)
		{
		}

		// Token: 0x06003F80 RID: 16256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A29")]
		[Address(RVA = "0x10150E248", Offset = "0x150E248", VA = "0x10150E248")]
		public void Destroy()
		{
		}

		// Token: 0x06003F81 RID: 16257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2A")]
		[Address(RVA = "0x10150E2A0", Offset = "0x150E2A0", VA = "0x10150E2A0")]
		public OfferStateIcon()
		{
		}

		// Token: 0x04004EBC RID: 20156
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003773")]
		[SerializeField]
		private Image m_Icon;

		// Token: 0x04004EBD RID: 20157
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003774")]
		[SerializeField]
		private Sprite m_IconCompleted;

		// Token: 0x04004EBE RID: 20158
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003775")]
		[SerializeField]
		private Sprite m_IconWIP_Reach;

		// Token: 0x04004EBF RID: 20159
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003776")]
		[SerializeField]
		private Sprite m_IconWIP;

		// Token: 0x04004EC0 RID: 20160
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003777")]
		[SerializeField]
		private Sprite m_IconOccurred;

		// Token: 0x04004EC1 RID: 20161
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003778")]
		[SerializeField]
		private Sprite m_IconNothing;
	}
}
