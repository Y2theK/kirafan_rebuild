﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C9D RID: 3229
	[Token(Token = "0x20008A5")]
	[StructLayout(3)]
	public class OfferTitleDataStatus : MonoBehaviour
	{
		// Token: 0x06003ADA RID: 15066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359D")]
		[Address(RVA = "0x10150E2A8", Offset = "0x150E2A8", VA = "0x10150E2A8")]
		private void OnDestroy()
		{
		}

		// Token: 0x06003ADB RID: 15067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359E")]
		[Address(RVA = "0x10150E394", Offset = "0x150E394", VA = "0x10150E394")]
		public void Clear()
		{
		}

		// Token: 0x06003ADC RID: 15068 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600359F")]
		[Address(RVA = "0x10150E4F4", Offset = "0x150E4F4", VA = "0x10150E4F4")]
		public void Apply(OfferManager.TitleData data)
		{
		}

		// Token: 0x06003ADD RID: 15069 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A0")]
		[Address(RVA = "0x10150ECFC", Offset = "0x150ECFC", VA = "0x10150ECFC")]
		public void ApplyCountup(OfferManager.TitleData afterTitleData)
		{
		}

		// Token: 0x06003ADE RID: 15070 RVA: 0x00018408 File Offset: 0x00016608
		[Token(Token = "0x60035A1")]
		[Address(RVA = "0x10150EE50", Offset = "0x150EE50", VA = "0x10150EE50")]
		public bool IsCountUp()
		{
			return default(bool);
		}

		// Token: 0x06003ADF RID: 15071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A2")]
		[Address(RVA = "0x10150EF54", Offset = "0x150EF54", VA = "0x10150EF54")]
		public void Skip()
		{
		}

		// Token: 0x06003AE0 RID: 15072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A3")]
		[Address(RVA = "0x10150E560", Offset = "0x150E560", VA = "0x10150E560")]
		private void SetState(OfferDefine.eCategory category, OfferDefine.eState state)
		{
		}

		// Token: 0x06003AE1 RID: 15073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A4")]
		[Address(RVA = "0x10150E9C0", Offset = "0x150E9C0", VA = "0x10150E9C0")]
		protected void SetPoint(int point, int max)
		{
		}

		// Token: 0x06003AE2 RID: 15074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A5")]
		[Address(RVA = "0x10150F0D8", Offset = "0x150F0D8", VA = "0x10150F0D8")]
		private void Update()
		{
		}

		// Token: 0x06003AE3 RID: 15075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A6")]
		[Address(RVA = "0x10150EF94", Offset = "0x150EF94", VA = "0x10150EF94")]
		private void PlayStateChangeAnim()
		{
		}

		// Token: 0x06003AE4 RID: 15076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A7")]
		[Address(RVA = "0x10150ED7C", Offset = "0x150ED7C", VA = "0x10150ED7C")]
		private void PlaySE(eSoundSeListDB sound)
		{
		}

		// Token: 0x06003AE5 RID: 15077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A8")]
		[Address(RVA = "0x10150E2AC", Offset = "0x150E2AC", VA = "0x10150E2AC")]
		private void StopSE()
		{
		}

		// Token: 0x06003AE6 RID: 15078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035A9")]
		[Address(RVA = "0x10150F214", Offset = "0x150F214", VA = "0x10150F214")]
		public OfferTitleDataStatus()
		{
		}

		// Token: 0x0400499C RID: 18844
		[Token(Token = "0x40033C5")]
		protected const float COUNTUP_SEC = 1f;

		// Token: 0x0400499D RID: 18845
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40033C6")]
		[SerializeField]
		private GameObject m_MainPointObj;

		// Token: 0x0400499E RID: 18846
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40033C7")]
		[SerializeField]
		private GameObject m_RepeatPointObj;

		// Token: 0x0400499F RID: 18847
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40033C8")]
		[SerializeField]
		private Text m_TotalPointText;

		// Token: 0x040049A0 RID: 18848
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40033C9")]
		[SerializeField]
		private Text m_ShortageText;

		// Token: 0x040049A1 RID: 18849
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40033CA")]
		[SerializeField]
		private Text m_RepeatShortageText;

		// Token: 0x040049A2 RID: 18850
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40033CB")]
		[SerializeField]
		private Image m_StateIcon;

		// Token: 0x040049A3 RID: 18851
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40033CC")]
		[SerializeField]
		protected Sprite m_SpriteOccurred;

		// Token: 0x040049A4 RID: 18852
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40033CD")]
		[SerializeField]
		protected Sprite m_SpriteWIP;

		// Token: 0x040049A5 RID: 18853
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40033CE")]
		[SerializeField]
		protected Sprite m_SpriteWIPReach;

		// Token: 0x040049A6 RID: 18854
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40033CF")]
		[SerializeField]
		protected Sprite m_SpriteRepeatWIP;

		// Token: 0x040049A7 RID: 18855
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40033D0")]
		[SerializeField]
		protected Text m_StateDescript;

		// Token: 0x040049A8 RID: 18856
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40033D1")]
		[SerializeField]
		private AnimUIPlayer[] m_StateChangePointAnim;

		// Token: 0x040049A9 RID: 18857
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40033D2")]
		[SerializeField]
		private AnimUIPlayer[] m_StateChangeStateAnim;

		// Token: 0x040049AA RID: 18858
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40033D3")]
		protected float m_CountUpRate;

		// Token: 0x040049AB RID: 18859
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x40033D4")]
		protected int m_BeforePoint;

		// Token: 0x040049AC RID: 18860
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40033D5")]
		protected OfferManager.TitleData m_AfterTitleData;

		// Token: 0x040049AD RID: 18861
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40033D6")]
		protected SoundHandler m_SoundHandler;
	}
}
