﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D76 RID: 3446
	[Token(Token = "0x200093B")]
	[StructLayout(3)]
	public class OfferWindow : UIGroup
	{
		// Token: 0x06003F82 RID: 16258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2B")]
		[Address(RVA = "0x10150F21C", Offset = "0x150F21C", VA = "0x10150F21C")]
		public void Setup()
		{
		}

		// Token: 0x06003F83 RID: 16259 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2C")]
		[Address(RVA = "0x10150F250", Offset = "0x150F250", VA = "0x10150F250")]
		public void SetupParam(Sprite bgSprite, eTitleType titleType, Action<OfferWindow.eButtonID, long> finishFinalizeCallback, Action<long> rewardWindowButtonCallback)
		{
		}

		// Token: 0x06003F84 RID: 16260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2D")]
		[Address(RVA = "0x10150F3D4", Offset = "0x150F3D4", VA = "0x10150F3D4", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06003F85 RID: 16261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2E")]
		[Address(RVA = "0x10150F8E0", Offset = "0x150F8E0", VA = "0x10150F8E0")]
		public void Destroy()
		{
		}

		// Token: 0x06003F86 RID: 16262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A2F")]
		[Address(RVA = "0x10150F960", Offset = "0x150F960", VA = "0x10150F960", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06003F87 RID: 16263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A30")]
		[Address(RVA = "0x10150F988", Offset = "0x150F988", VA = "0x10150F988", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x06003F88 RID: 16264 RVA: 0x00018DE0 File Offset: 0x00016FE0
		[Token(Token = "0x6003A31")]
		[Address(RVA = "0x10150F9FC", Offset = "0x150F9FC", VA = "0x10150F9FC")]
		public bool IsTopPage()
		{
			return default(bool);
		}

		// Token: 0x06003F89 RID: 16265 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A32")]
		[Address(RVA = "0x10150FA0C", Offset = "0x150FA0C", VA = "0x10150FA0C")]
		public void Back()
		{
		}

		// Token: 0x06003F8A RID: 16266 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A33")]
		[Address(RVA = "0x10150F7DC", Offset = "0x150F7DC", VA = "0x10150F7DC")]
		private void SetActive(OfferWindow.eMode mode, int scrollItemIndex = 0)
		{
		}

		// Token: 0x06003F8B RID: 16267 RVA: 0x00018DF8 File Offset: 0x00016FF8
		[Token(Token = "0x6003A34")]
		[Address(RVA = "0x10150FA1C", Offset = "0x150FA1C", VA = "0x10150FA1C")]
		private bool ShowDetail(eTitleType titleType, long managedId)
		{
			return default(bool);
		}

		// Token: 0x06003F8C RID: 16268 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A35")]
		[Address(RVA = "0x10150FC20", Offset = "0x150FC20", VA = "0x10150FC20")]
		private void OnClickMainOfferListItem(long managedId, int index)
		{
		}

		// Token: 0x06003F8D RID: 16269 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A36")]
		[Address(RVA = "0x10150FC34", Offset = "0x150FC34", VA = "0x10150FC34")]
		public void OnClickTransitQuestButton()
		{
		}

		// Token: 0x06003F8E RID: 16270 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A37")]
		[Address(RVA = "0x10150FDE8", Offset = "0x150FDE8", VA = "0x10150FDE8")]
		private void OnCloseConfirmTransitQuest(int button)
		{
		}

		// Token: 0x06003F8F RID: 16271 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A38")]
		[Address(RVA = "0x10150FE04", Offset = "0x150FE04", VA = "0x10150FE04")]
		public void OnClickRewardWindowButton()
		{
		}

		// Token: 0x06003F90 RID: 16272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A39")]
		[Address(RVA = "0x10150FE74", Offset = "0x150FE74", VA = "0x10150FE74")]
		public void OnClickOfferOrderButton()
		{
		}

		// Token: 0x06003F91 RID: 16273 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3A")]
		[Address(RVA = "0x10150FE88", Offset = "0x150FE88", VA = "0x10150FE88")]
		public void OnClickOfferCompleteButton()
		{
		}

		// Token: 0x06003F92 RID: 16274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3B")]
		[Address(RVA = "0x10150FE9C", Offset = "0x150FE9C", VA = "0x10150FE9C")]
		public OfferWindow()
		{
		}

		// Token: 0x04004EC2 RID: 20162
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003779")]
		[SerializeField]
		private Image m_BackGround;

		// Token: 0x04004EC3 RID: 20163
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400377A")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04004EC4 RID: 20164
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400377B")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04004EC5 RID: 20165
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400377C")]
		[SerializeField]
		private GameObject m_DetailRoot;

		// Token: 0x04004EC6 RID: 20166
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400377D")]
		[SerializeField]
		private MainOfferDetailField m_DetailField;

		// Token: 0x04004EC7 RID: 20167
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400377E")]
		[SerializeField]
		private Text m_TransitQuestButtonText;

		// Token: 0x04004EC8 RID: 20168
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400377F")]
		[SerializeField]
		private Text m_RewardItemButtonText;

		// Token: 0x04004EC9 RID: 20169
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003780")]
		[SerializeField]
		private CustomButton m_TransitQuestButton;

		// Token: 0x04004ECA RID: 20170
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003781")]
		[SerializeField]
		private CustomButton m_OrderButton;

		// Token: 0x04004ECB RID: 20171
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003782")]
		[SerializeField]
		private CustomButton m_CompleteButton;

		// Token: 0x04004ECC RID: 20172
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003783")]
		private eTitleType m_TitleType;

		// Token: 0x04004ECD RID: 20173
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x4003784")]
		private OfferWindow.eMode m_Mode;

		// Token: 0x04004ECE RID: 20174
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003785")]
		private OfferWindow.eButtonID m_ButtonID;

		// Token: 0x04004ECF RID: 20175
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4003786")]
		private int m_LastSelectedIndex;

		// Token: 0x04004ED0 RID: 20176
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003787")]
		private OfferManager.OfferData m_OfferData;

		// Token: 0x04004ED1 RID: 20177
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003788")]
		private Action<OfferWindow.eButtonID, long> m_FinishFinalizeCallback;

		// Token: 0x04004ED2 RID: 20178
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003789")]
		private Action<long> m_RewardWindowButtonCallback;

		// Token: 0x02000D77 RID: 3447
		[Token(Token = "0x2001101")]
		private enum eMode
		{
			// Token: 0x04004ED4 RID: 20180
			[Token(Token = "0x4006A93")]
			List,
			// Token: 0x04004ED5 RID: 20181
			[Token(Token = "0x4006A94")]
			Detail
		}

		// Token: 0x02000D78 RID: 3448
		[Token(Token = "0x2001102")]
		public enum eButtonID
		{
			// Token: 0x04004ED7 RID: 20183
			[Token(Token = "0x4006A96")]
			None,
			// Token: 0x04004ED8 RID: 20184
			[Token(Token = "0x4006A97")]
			TransitQuest,
			// Token: 0x04004ED9 RID: 20185
			[Token(Token = "0x4006A98")]
			RewardWindow,
			// Token: 0x04004EDA RID: 20186
			[Token(Token = "0x4006A99")]
			OfferOrder,
			// Token: 0x04004EDB RID: 20187
			[Token(Token = "0x4006A9A")]
			OfferComplete
		}
	}
}
