﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000D29 RID: 3369
	// (Invoke) Token: 0x06003DB7 RID: 15799
	[Token(Token = "0x2000904")]
	[StructLayout(3, Size = 8)]
	public delegate void OniTweenCompleteDelegate(object obj);
}
