﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CEE RID: 3310
	[Token(Token = "0x20008D8")]
	[StructLayout(3)]
	public class OrbIcon : ASyncImage
	{
		// Token: 0x06003C86 RID: 15494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003743")]
		[Address(RVA = "0x1015103F8", Offset = "0x15103F8", VA = "0x1015103F8")]
		public void Apply(int orbID)
		{
		}

		// Token: 0x06003C87 RID: 15495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003744")]
		[Address(RVA = "0x101510508", Offset = "0x1510508", VA = "0x101510508", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C88 RID: 15496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003745")]
		[Address(RVA = "0x101510534", Offset = "0x1510534", VA = "0x101510534")]
		public OrbIcon()
		{
		}

		// Token: 0x04004BDE RID: 19422
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400354F")]
		protected int m_OrbID;
	}
}
