﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CEF RID: 3311
	[Token(Token = "0x20008D9")]
	[StructLayout(3)]
	public class OrbIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003C89 RID: 15497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003746")]
		[Address(RVA = "0x101510544", Offset = "0x1510544", VA = "0x101510544")]
		public void Apply(int orbID)
		{
		}

		// Token: 0x06003C8A RID: 15498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003747")]
		[Address(RVA = "0x101510578", Offset = "0x1510578", VA = "0x101510578")]
		public void Destroy()
		{
		}

		// Token: 0x06003C8B RID: 15499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003748")]
		[Address(RVA = "0x101510618", Offset = "0x1510618", VA = "0x101510618")]
		public OrbIconWithFrame()
		{
		}

		// Token: 0x04004BDF RID: 19423
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003550")]
		[SerializeField]
		private OrbIcon m_Icon;
	}
}
