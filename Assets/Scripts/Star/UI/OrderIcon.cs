﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF0 RID: 3312
	[Token(Token = "0x20008DA")]
	[StructLayout(3)]
	public class OrderIcon : ASyncImage
	{
		// Token: 0x06003C8C RID: 15500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003749")]
		[Address(RVA = "0x101510620", Offset = "0x1510620", VA = "0x101510620")]
		public void ApplyPL(int charaID)
		{
		}

		// Token: 0x06003C8D RID: 15501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374A")]
		[Address(RVA = "0x101510738", Offset = "0x1510738", VA = "0x101510738")]
		public void ApplyEN(int resourceID)
		{
		}

		// Token: 0x06003C8E RID: 15502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374B")]
		[Address(RVA = "0x101510870", Offset = "0x1510870", VA = "0x101510870", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C8F RID: 15503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374C")]
		[Address(RVA = "0x10151089C", Offset = "0x151089C", VA = "0x10151089C")]
		public OrderIcon()
		{
		}

		// Token: 0x04004BE0 RID: 19424
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003551")]
		protected OrderIcon.eType m_Type;

		// Token: 0x04004BE1 RID: 19425
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003552")]
		protected int m_ID;

		// Token: 0x02000CF1 RID: 3313
		[Token(Token = "0x20010DD")]
		protected enum eType
		{
			// Token: 0x04004BE3 RID: 19427
			[Token(Token = "0x40069DF")]
			PL,
			// Token: 0x04004BE4 RID: 19428
			[Token(Token = "0x40069E0")]
			EN
		}
	}
}
