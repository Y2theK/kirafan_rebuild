﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DF5 RID: 3573
	[Token(Token = "0x2000997")]
	[StructLayout(3)]
	public class OverlayFilterPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06004211 RID: 16913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C97")]
		[Address(RVA = "0x1015108AC", Offset = "0x15108AC", VA = "0x1015108AC", Slot = "7")]
		public override void Open(OverlayUIArgBase arg, Action OnClose, Action OnEnd)
		{
		}

		// Token: 0x06004212 RID: 16914 RVA: 0x000193B0 File Offset: 0x000175B0
		[Token(Token = "0x6003C98")]
		[Address(RVA = "0x101510BB8", Offset = "0x1510BB8", VA = "0x101510BB8", Slot = "5")]
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06004213 RID: 16915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C99")]
		[Address(RVA = "0x101510BC0", Offset = "0x1510BC0", VA = "0x101510BC0", Slot = "11")]
		protected override void Setup(OverlayUIArgBase arg)
		{
		}

		// Token: 0x06004214 RID: 16916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C9A")]
		[Address(RVA = "0x101510DD8", Offset = "0x1510DD8", VA = "0x101510DD8", Slot = "12")]
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
		}

		// Token: 0x06004215 RID: 16917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C9B")]
		[Address(RVA = "0x101510E18", Offset = "0x1510E18", VA = "0x101510E18")]
		public OverlayFilterPlayer()
		{
		}

		// Token: 0x0400520F RID: 21007
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A15")]
		private SceneDefine.eChildSceneID m_SceneID;

		// Token: 0x04005210 RID: 21008
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A16")]
		protected FilterUI m_FilterUI;
	}
}
