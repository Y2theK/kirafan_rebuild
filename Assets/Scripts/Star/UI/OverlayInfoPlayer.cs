﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DF7 RID: 3575
	[Token(Token = "0x2000999")]
	[StructLayout(3)]
	public class OverlayInfoPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06004217 RID: 16919 RVA: 0x000193C8 File Offset: 0x000175C8
		[Token(Token = "0x6003C9D")]
		[Address(RVA = "0x101510E30", Offset = "0x1510E30", VA = "0x101510E30", Slot = "5")]
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06004218 RID: 16920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C9E")]
		[Address(RVA = "0x101510E38", Offset = "0x1510E38", VA = "0x101510E38", Slot = "11")]
		protected override void Setup(OverlayUIArgBase arg)
		{
		}

		// Token: 0x06004219 RID: 16921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003C9F")]
		[Address(RVA = "0x101510F88", Offset = "0x1510F88", VA = "0x101510F88", Slot = "12")]
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
		}

		// Token: 0x0600421A RID: 16922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CA0")]
		[Address(RVA = "0x101510FBC", Offset = "0x1510FBC", VA = "0x101510FBC")]
		public OverlayInfoPlayer()
		{
		}

		// Token: 0x04005214 RID: 21012
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A1A")]
		private SceneDefine.eChildSceneID m_SceneID;

		// Token: 0x04005215 RID: 21013
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A1B")]
		protected InfoUI m_InfoUI;
	}
}
