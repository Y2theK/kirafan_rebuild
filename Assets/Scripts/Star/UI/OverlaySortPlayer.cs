﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000E00 RID: 3584
	[Token(Token = "0x200099E")]
	[StructLayout(3)]
	public class OverlaySortPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06004232 RID: 16946 RVA: 0x00019440 File Offset: 0x00017640
		[Token(Token = "0x6003CB6")]
		[Address(RVA = "0x101510FCC", Offset = "0x1510FCC", VA = "0x101510FCC", Slot = "5")]
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return SceneDefine.eChildSceneID.QuestCategorySelectUI;
		}

		// Token: 0x06004233 RID: 16947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB7")]
		[Address(RVA = "0x101510FD4", Offset = "0x1510FD4", VA = "0x101510FD4", Slot = "11")]
		protected override void Setup(OverlayUIArgBase arg)
		{
		}

		// Token: 0x06004234 RID: 16948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB8")]
		[Address(RVA = "0x1015111EC", Offset = "0x15111EC", VA = "0x1015111EC", Slot = "12")]
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
		}

		// Token: 0x06004235 RID: 16949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB9")]
		[Address(RVA = "0x101511220", Offset = "0x1511220", VA = "0x101511220")]
		public OverlaySortPlayer()
		{
		}

		// Token: 0x04005233 RID: 21043
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A27")]
		private SceneDefine.eChildSceneID m_SceneID;

		// Token: 0x04005234 RID: 21044
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A28")]
		protected SortUI m_SortUI;
	}
}
