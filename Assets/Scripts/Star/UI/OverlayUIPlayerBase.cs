﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DF9 RID: 3577
	[Token(Token = "0x200099B")]
	[StructLayout(3)]
	public abstract class OverlayUIPlayerBase
	{
		// Token: 0x0600421C RID: 16924 RVA: 0x000193E0 File Offset: 0x000175E0
		[Token(Token = "0x6003CA2")]
		[Address(RVA = "0x101511238", Offset = "0x1511238", VA = "0x101511238", Slot = "4")]
		public virtual bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x0600421D RID: 16925 RVA: 0x000193F8 File Offset: 0x000175F8
		[Token(Token = "0x6003CA3")]
		[Address(RVA = "0x101511248", Offset = "0x1511248", VA = "0x101511248")]
		public bool IsDoneLoad()
		{
			return default(bool);
		}

		// Token: 0x0600421E RID: 16926
		[Token(Token = "0x6003CA4")]
		[Address(Slot = "5")]
		public abstract SceneDefine.eChildSceneID GetUISceneID();

		// Token: 0x0600421F RID: 16927 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003CA5")]
		[Address(RVA = "0x102354494", Offset = "0x2354494", VA = "0x102354494", Slot = "6")]
		public virtual T GetUI<T>() where T : MenuUIBase
		{
			return null;
		}

		// Token: 0x06004220 RID: 16928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CA6")]
		[Address(RVA = "0x101510A54", Offset = "0x1510A54", VA = "0x101510A54", Slot = "7")]
		public virtual void Open(OverlayUIArgBase arg, Action OnClose, Action OnEnd)
		{
		}

		// Token: 0x06004221 RID: 16929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CA7")]
		[Address(RVA = "0x101511258", Offset = "0x1511258", VA = "0x101511258")]
		public void Unload()
		{
		}

		// Token: 0x06004222 RID: 16930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CA8")]
		[Address(RVA = "0x1015112EC", Offset = "0x15112EC", VA = "0x1015112EC", Slot = "8")]
		protected virtual void Destroy()
		{
		}

		// Token: 0x06004223 RID: 16931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CA9")]
		[Address(RVA = "0x101511370", Offset = "0x1511370", VA = "0x101511370", Slot = "9")]
		public virtual void UpdateStep()
		{
		}

		// Token: 0x06004224 RID: 16932 RVA: 0x00019410 File Offset: 0x00017610
		[Token(Token = "0x6003CAA")]
		[Address(RVA = "0x1015115F8", Offset = "0x15115F8", VA = "0x1015115F8", Slot = "10")]
		protected virtual bool SetupAndPlayIn()
		{
			return default(bool);
		}

		// Token: 0x06004225 RID: 16933
		[Token(Token = "0x6003CAB")]
		[Address(Slot = "11")]
		protected abstract void Setup(OverlayUIArgBase arg);

		// Token: 0x06004226 RID: 16934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CAC")]
		[Address(RVA = "0x101510E0C", Offset = "0x1510E0C", VA = "0x101510E0C", Slot = "12")]
		protected virtual void OnClickBackButtonCallBack(bool isShortCut)
		{
		}

		// Token: 0x06004227 RID: 16935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CAD")]
		[Address(RVA = "0x101511770", Offset = "0x1511770", VA = "0x101511770", Slot = "13")]
		protected virtual void GoToMenuEnd()
		{
		}

		// Token: 0x06004228 RID: 16936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CAE")]
		[Address(RVA = "0x101510E28", Offset = "0x1510E28", VA = "0x101510E28")]
		protected OverlayUIPlayerBase()
		{
		}

		// Token: 0x04005216 RID: 21014
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003A1C")]
		protected OverlayUIPlayerBase.eLoadStep m_OverlayStep;

		// Token: 0x04005217 RID: 21015
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A1D")]
		protected MenuUIBase m_UI;

		// Token: 0x04005218 RID: 21016
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A1E")]
		protected OverlayUIArgBase m_Arg;

		// Token: 0x04005219 RID: 21017
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A1F")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x0400521A RID: 21018
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003A20")]
		private Action m_OnClose;

		// Token: 0x0400521B RID: 21019
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003A21")]
		private Action m_OnEnd;

		// Token: 0x02000DFA RID: 3578
		[Token(Token = "0x2001124")]
		public enum eLoadStep
		{
			// Token: 0x0400521D RID: 21021
			[Token(Token = "0x4006B44")]
			None,
			// Token: 0x0400521E RID: 21022
			[Token(Token = "0x4006B45")]
			Load,
			// Token: 0x0400521F RID: 21023
			[Token(Token = "0x4006B46")]
			LoadWait,
			// Token: 0x04005220 RID: 21024
			[Token(Token = "0x4006B47")]
			Setup,
			// Token: 0x04005221 RID: 21025
			[Token(Token = "0x4006B48")]
			Loaded,
			// Token: 0x04005222 RID: 21026
			[Token(Token = "0x4006B49")]
			UnloadWait
		}
	}
}
