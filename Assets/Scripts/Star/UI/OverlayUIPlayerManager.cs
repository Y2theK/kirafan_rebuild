﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DFB RID: 3579
	[Token(Token = "0x200099C")]
	[StructLayout(3)]
	public class OverlayUIPlayerManager : MonoBehaviour
	{
		// Token: 0x06004229 RID: 16937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CAF")]
		[Address(RVA = "0x10151181C", Offset = "0x151181C", VA = "0x10151181C")]
		public void Open(OverlayUIPlayerManager.eOverlaySceneID id, OverlayUIArgBase arg, [Optional] Action OnClose, [Optional] Action OnEnd)
		{
		}

		// Token: 0x0600422A RID: 16938 RVA: 0x00019428 File Offset: 0x00017628
		[Token(Token = "0x6003CB0")]
		[Address(RVA = "0x1015119F4", Offset = "0x15119F4", VA = "0x1015119F4")]
		public bool IsOpen(OverlayUIPlayerManager.eOverlaySceneID id)
		{
			return default(bool);
		}

		// Token: 0x0600422B RID: 16939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB1")]
		[Address(RVA = "0x101511B60", Offset = "0x1511B60", VA = "0x101511B60")]
		public void Clear()
		{
		}

		// Token: 0x0600422C RID: 16940 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003CB2")]
		[Address(RVA = "0x10235454C", Offset = "0x235454C", VA = "0x10235454C")]
		public T GetUI<T>(OverlayUIPlayerManager.eOverlaySceneID id) where T : OverlayUIPlayerBase
		{
			return null;
		}

		// Token: 0x0600422D RID: 16941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB3")]
		[Address(RVA = "0x101511BC0", Offset = "0x1511BC0", VA = "0x101511BC0")]
		private void Update()
		{
		}

		// Token: 0x0600422E RID: 16942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB4")]
		[Address(RVA = "0x101512070", Offset = "0x1512070", VA = "0x101512070")]
		public OverlayUIPlayerManager()
		{
		}

		// Token: 0x04005223 RID: 21027
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A22")]
		private List<OverlayUIPlayerManager.PlayerInfo> m_PlayerList;

		// Token: 0x04005224 RID: 21028
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A23")]
		private bool m_IsLoading;

		// Token: 0x04005225 RID: 21029
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A24")]
		private List<OverlayUIPlayerManager.OpenRequestParam> m_ReqParam;

		// Token: 0x02000DFC RID: 3580
		[Token(Token = "0x2001125")]
		public enum eOverlaySceneID
		{
			// Token: 0x04005227 RID: 21031
			[Token(Token = "0x4006B4B")]
			Filter,
			// Token: 0x04005228 RID: 21032
			[Token(Token = "0x4006B4C")]
			Sort,
			// Token: 0x04005229 RID: 21033
			[Token(Token = "0x4006B4D")]
			Info,
			// Token: 0x0400522A RID: 21034
			[Token(Token = "0x4006B4E")]
			Num
		}

		// Token: 0x02000DFD RID: 3581
		[Token(Token = "0x2001126")]
		public class PlayerInfo
		{
			// Token: 0x0600422F RID: 16943 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600622D")]
			[Address(RVA = "0x101511B24", Offset = "0x1511B24", VA = "0x101511B24")]
			public PlayerInfo(OverlayUIPlayerManager.eOverlaySceneID id, OverlayUIPlayerBase player)
			{
			}

			// Token: 0x0400522B RID: 21035
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B4F")]
			public OverlayUIPlayerBase m_Player;

			// Token: 0x0400522C RID: 21036
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B50")]
			public OverlayUIPlayerManager.eOverlaySceneID m_ID;
		}

		// Token: 0x02000DFE RID: 3582
		[Token(Token = "0x2001127")]
		private class OpenRequestParam
		{
			// Token: 0x06004230 RID: 16944 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600622E")]
			[Address(RVA = "0x101511AD4", Offset = "0x1511AD4", VA = "0x101511AD4")]
			public OpenRequestParam(OverlayUIPlayerManager.eOverlaySceneID id, OverlayUIArgBase arg, Action onClose, Action onEnd)
			{
			}

			// Token: 0x0400522D RID: 21037
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B51")]
			public OverlayUIPlayerManager.eOverlaySceneID m_ID;

			// Token: 0x0400522E RID: 21038
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B52")]
			public OverlayUIArgBase m_Arg;

			// Token: 0x0400522F RID: 21039
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006B53")]
			public Action m_OnClose;

			// Token: 0x04005230 RID: 21040
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006B54")]
			public Action m_OnEnd;
		}
	}
}
