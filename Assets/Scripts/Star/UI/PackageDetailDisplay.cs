﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D15 RID: 3349
	[Token(Token = "0x20008F7")]
	[StructLayout(3)]
	public class PackageDetailDisplay : MonoBehaviour
	{
		// Token: 0x06003D63 RID: 15715 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600381F")]
		[Address(RVA = "0x101512108", Offset = "0x1512108", VA = "0x101512108")]
		private void Start()
		{
		}

		// Token: 0x06003D64 RID: 15716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003820")]
		[Address(RVA = "0x10151210C", Offset = "0x151210C", VA = "0x10151210C")]
		public void Apply(int itemID)
		{
		}

		// Token: 0x06003D65 RID: 15717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003821")]
		[Address(RVA = "0x101512738", Offset = "0x1512738", VA = "0x101512738")]
		private void Update()
		{
		}

		// Token: 0x06003D66 RID: 15718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003822")]
		[Address(RVA = "0x10151273C", Offset = "0x151273C", VA = "0x10151273C")]
		public void SetActive(bool active)
		{
		}

		// Token: 0x06003D67 RID: 15719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003823")]
		[Address(RVA = "0x10151282C", Offset = "0x151282C", VA = "0x10151282C")]
		public PackageDetailDisplay()
		{
		}

		// Token: 0x04004CB5 RID: 19637
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035F0")]
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x04004CB6 RID: 19638
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035F1")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004CB7 RID: 19639
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035F2")]
		[SerializeField]
		private Transform m_ListParent;

		// Token: 0x04004CB8 RID: 19640
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035F3")]
		[SerializeField]
		private ShopPackageContentDetail m_ContentPrefab;

		// Token: 0x04004CB9 RID: 19641
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035F4")]
		[SerializeField]
		private Text m_DetailText;
	}
}
