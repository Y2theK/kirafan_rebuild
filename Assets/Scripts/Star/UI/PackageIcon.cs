﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF2 RID: 3314
	[Token(Token = "0x20008DB")]
	[StructLayout(3)]
	public class PackageIcon : ASyncImage
	{
		// Token: 0x06003C90 RID: 15504 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374D")]
		[Address(RVA = "0x101512834", Offset = "0x1512834", VA = "0x101512834")]
		public void Apply(int packageID)
		{
		}

		// Token: 0x06003C91 RID: 15505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374E")]
		[Address(RVA = "0x101512998", Offset = "0x1512998", VA = "0x101512998", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C92 RID: 15506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600374F")]
		[Address(RVA = "0x1015129C4", Offset = "0x15129C4", VA = "0x1015129C4")]
		public PackageIcon()
		{
		}

		// Token: 0x04004BE5 RID: 19429
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003553")]
		protected int m_PackageID;
	}
}
