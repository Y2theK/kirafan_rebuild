﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CF3 RID: 3315
	[Token(Token = "0x20008DC")]
	[StructLayout(3)]
	public class PackageIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003C93 RID: 15507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003750")]
		[Address(RVA = "0x1015129D4", Offset = "0x15129D4", VA = "0x1015129D4")]
		public void Apply(int itemID)
		{
		}

		// Token: 0x06003C94 RID: 15508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003751")]
		[Address(RVA = "0x101512A34", Offset = "0x1512A34", VA = "0x101512A34")]
		public void Destroy()
		{
		}

		// Token: 0x06003C95 RID: 15509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003752")]
		[Address(RVA = "0x101512AD4", Offset = "0x1512AD4", VA = "0x101512AD4")]
		public void SetEnableRenderItemIcon(bool flg)
		{
		}

		// Token: 0x06003C96 RID: 15510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003753")]
		[Address(RVA = "0x101512B24", Offset = "0x1512B24", VA = "0x101512B24")]
		public PackageIconWithFrame()
		{
		}

		// Token: 0x04004BE6 RID: 19430
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003554")]
		[SerializeField]
		private PackageIcon m_Icon;
	}
}
