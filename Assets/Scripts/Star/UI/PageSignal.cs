﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D10 RID: 3344
	[Token(Token = "0x20008F2")]
	[StructLayout(3)]
	public class PageSignal : MonoBehaviour
	{
		// Token: 0x14000071 RID: 113
		// (add) Token: 0x06003D43 RID: 15683 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003D44 RID: 15684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000071")]
		public event Action<int> OnClickButton
		{
			[Token(Token = "0x60037FF")]
			[Address(RVA = "0x101512B2C", Offset = "0x1512B2C", VA = "0x101512B2C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003800")]
			[Address(RVA = "0x101512C38", Offset = "0x1512C38", VA = "0x101512C38")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003D45 RID: 15685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003801")]
		[Address(RVA = "0x101512D44", Offset = "0x1512D44", VA = "0x101512D44")]
		private void Start()
		{
		}

		// Token: 0x06003D46 RID: 15686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003802")]
		[Address(RVA = "0x101512D90", Offset = "0x1512D90", VA = "0x101512D90")]
		public void SetPageMax(int max)
		{
		}

		// Token: 0x06003D47 RID: 15687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003803")]
		[Address(RVA = "0x101512EA4", Offset = "0x1512EA4", VA = "0x101512EA4")]
		public void SetPage(int currentPage)
		{
		}

		// Token: 0x06003D48 RID: 15688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003804")]
		[Address(RVA = "0x101512FEC", Offset = "0x1512FEC", VA = "0x101512FEC")]
		private void Update()
		{
		}

		// Token: 0x06003D49 RID: 15689 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003805")]
		[Address(RVA = "0x101513344", Offset = "0x1513344", VA = "0x101513344")]
		public void OnClickSignalButton(int idx)
		{
		}

		// Token: 0x06003D4A RID: 15690 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003806")]
		[Address(RVA = "0x1015133A4", Offset = "0x15133A4", VA = "0x1015133A4")]
		public void Lock(bool isLock)
		{
		}

		// Token: 0x06003D4B RID: 15691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003807")]
		[Address(RVA = "0x101513548", Offset = "0x1513548", VA = "0x101513548")]
		public PageSignal()
		{
		}

		// Token: 0x04004CA0 RID: 19616
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035DB")]
		[SerializeField]
		private Sprite m_SpriteOff;

		// Token: 0x04004CA1 RID: 19617
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40035DC")]
		[SerializeField]
		private Sprite m_SpriteOn;

		// Token: 0x04004CA2 RID: 19618
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40035DD")]
		[SerializeField]
		private PageSignalButton m_Signal;

		// Token: 0x04004CA3 RID: 19619
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40035DE")]
		private List<PageSignalButton> m_SignalList;

		// Token: 0x04004CA4 RID: 19620
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40035DF")]
		private int m_Max;

		// Token: 0x04004CA5 RID: 19621
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40035E0")]
		private int m_Current;
	}
}
