﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D11 RID: 3345
	[Token(Token = "0x20008F3")]
	[StructLayout(3)]
	public class PageSignalButton : MonoBehaviour
	{
		// Token: 0x14000072 RID: 114
		// (add) Token: 0x06003D4C RID: 15692 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003D4D RID: 15693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000072")]
		public event Action<int> OnClick
		{
			[Token(Token = "0x6003808")]
			[Address(RVA = "0x101513238", Offset = "0x1513238", VA = "0x101513238")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003809")]
			[Address(RVA = "0x1015135B8", Offset = "0x15135B8", VA = "0x1015135B8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x06003D4E RID: 15694 RVA: 0x00018828 File Offset: 0x00016A28
		// (set) Token: 0x06003D4F RID: 15695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000454")]
		public int Idx
		{
			[Token(Token = "0x600380A")]
			[Address(RVA = "0x1015136C4", Offset = "0x15136C4", VA = "0x1015136C4")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600380B")]
			[Address(RVA = "0x101513230", Offset = "0x1513230", VA = "0x101513230")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06003D50 RID: 15696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600380C")]
		[Address(RVA = "0x101512FB4", Offset = "0x1512FB4", VA = "0x101512FB4")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x06003D51 RID: 15697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600380D")]
		[Address(RVA = "0x1015136CC", Offset = "0x15136CC", VA = "0x1015136CC")]
		public void OnClickButtonCallBack()
		{
		}

		// Token: 0x06003D52 RID: 15698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600380E")]
		[Address(RVA = "0x101513720", Offset = "0x1513720", VA = "0x101513720")]
		public PageSignalButton()
		{
		}

		// Token: 0x04004CA7 RID: 19623
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40035E2")]
		[SerializeField]
		private Image m_Image;
	}
}
