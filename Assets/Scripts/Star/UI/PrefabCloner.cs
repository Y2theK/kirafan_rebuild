﻿using UnityEngine;

namespace Star.UI {
    public class PrefabCloner : MonoBehaviour {
        [SerializeField] private GameObject m_Prefab;

        [Tooltip("生成したInstのサイズを自身にあわせる")]
        [SerializeField] private bool m_FitInstanceRect = true;

        private GameObject m_CloneInstance;

        public GameObject Inst {
            get {
                if (m_CloneInstance == null) {
                    Setup(true);
                }
                return m_CloneInstance;
            }
        }

        public T GetInst<T>() where T : MonoBehaviour {
            return Inst.GetComponent<T>();
        }

        public bool IsExistPrefab() {
            return m_Prefab != null;
        }

        public void Setup(bool force = true) {
            if (m_CloneInstance != null) {
                if (!force) { return; }
                Destroy(m_CloneInstance);
            }

            RectTransform transform = GetComponent<RectTransform>();
            m_CloneInstance = Instantiate(m_Prefab, transform);
            RectTransform cloneTransform = m_CloneInstance.GetComponent<RectTransform>();
            if (m_FitInstanceRect) {
                cloneTransform.anchorMin = new Vector2(0f, 0f);
                cloneTransform.anchorMax = new Vector2(1f, 1f);
                cloneTransform.sizeDelta = new Vector2(0f, 0f);
            }
            cloneTransform.anchoredPosition = Vector2.zero;
            cloneTransform.localScale = Vector3.one;
            PrefabCloner[] subCloners = m_CloneInstance.GetComponentsInChildren<PrefabCloner>();
            for (int i = 1; i < subCloners.Length; i++) {
                subCloners[i].Setup(true);
            }
        }
    }
}
