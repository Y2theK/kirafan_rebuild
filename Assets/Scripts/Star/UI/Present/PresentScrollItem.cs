﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Present
{
	// Token: 0x02000EDE RID: 3806
	[Token(Token = "0x2000A12")]
	[StructLayout(3)]
	public class PresentScrollItem : ScrollItemIcon
	{
		// Token: 0x06004797 RID: 18327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DD")]
		[Address(RVA = "0x101513B5C", Offset = "0x1513B5C", VA = "0x101513B5C")]
		public void SetEnableGet(bool flg)
		{
		}

		// Token: 0x06004798 RID: 18328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DE")]
		[Address(RVA = "0x101513BA8", Offset = "0x1513BA8", VA = "0x101513BA8", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004799 RID: 18329 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DF")]
		[Address(RVA = "0x1015148C8", Offset = "0x15148C8", VA = "0x1015148C8")]
		public void OnClickGetButtonCallBack()
		{
		}

		// Token: 0x0600479A RID: 18330 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E0")]
		[Address(RVA = "0x101514A28", Offset = "0x1514A28", VA = "0x101514A28")]
		public PresentScrollItem()
		{
		}

		// Token: 0x0400594D RID: 22861
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003EDF")]
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x0400594E RID: 22862
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003EE0")]
		[SerializeField]
		private GameObject m_HistoryObj;

		// Token: 0x0400594F RID: 22863
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003EE1")]
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x04005950 RID: 22864
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003EE2")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005951 RID: 22865
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003EE3")]
		[SerializeField]
		private Text m_DeadLineText;

		// Token: 0x04005952 RID: 22866
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003EE4")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04005953 RID: 22867
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003EE5")]
		[SerializeField]
		private Text m_CreatedText;

		// Token: 0x04005954 RID: 22868
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003EE6")]
		[SerializeField]
		private Text m_ReceivedText;

		// Token: 0x04005955 RID: 22869
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003EE7")]
		[SerializeField]
		private GameObject m_GetIcon;
	}
}
