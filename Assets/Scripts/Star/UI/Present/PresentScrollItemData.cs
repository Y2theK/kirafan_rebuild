﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Present
{
	// Token: 0x02000EDF RID: 3807
	[Token(Token = "0x2000A13")]
	[StructLayout(3)]
	public class PresentScrollItemData : ScrollItemData
	{
		// Token: 0x0600479B RID: 18331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E1")]
		[Address(RVA = "0x101514A30", Offset = "0x1514A30", VA = "0x101514A30")]
		public PresentScrollItemData(long presentMngID, ePresentType type, int id, string name, string descript, bool isHistory, bool isExistDeadline, DateTime deadLine, DateTime receivedAt, DateTime createdAt)
		{
		}

		// Token: 0x0600479C RID: 18332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E2")]
		[Address(RVA = "0x1015149C0", Offset = "0x15149C0", VA = "0x1015149C0")]
		public void ExecuteGetPresent()
		{
		}

		// Token: 0x04005956 RID: 22870
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003EE8")]
		public long m_PresentMngID;

		// Token: 0x04005957 RID: 22871
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003EE9")]
		public ePresentType m_Type;

		// Token: 0x04005958 RID: 22872
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003EEA")]
		public int m_ID;

		// Token: 0x04005959 RID: 22873
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003EEB")]
		public string m_Name;

		// Token: 0x0400595A RID: 22874
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003EEC")]
		public string m_Descript;

		// Token: 0x0400595B RID: 22875
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003EED")]
		public bool m_IsExistDeadline;

		// Token: 0x0400595C RID: 22876
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003EEE")]
		public DateTime m_DeadLine;

		// Token: 0x0400595D RID: 22877
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003EEF")]
		public DateTime m_ReceivedAt;

		// Token: 0x0400595E RID: 22878
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003EF0")]
		public DateTime m_CreatedAt;

		// Token: 0x0400595F RID: 22879
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003EF1")]
		public bool m_IsHistroy;

		// Token: 0x04005960 RID: 22880
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003EF2")]
		public Action<long> m_OnClick;
	}
}
