﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Arousal;
using UnityEngine;

namespace Star.UI.Present
{
	// Token: 0x02000EE0 RID: 3808
	[Token(Token = "0x2000A14")]
	[StructLayout(3)]
	public class PresentUI : MenuUIBase
	{
		// Token: 0x17000525 RID: 1317
		// (get) Token: 0x0600479D RID: 18333 RVA: 0x0001A3D0 File Offset: 0x000185D0
		[Token(Token = "0x170004CE")]
		public PresentUI.eButton SelectButton
		{
			[Token(Token = "0x60041E3")]
			[Address(RVA = "0x101514AC4", Offset = "0x1514AC4", VA = "0x101514AC4")]
			get
			{
				return PresentUI.eButton.Get;
			}
		}

		// Token: 0x0600479E RID: 18334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E4")]
		[Address(RVA = "0x101514ACC", Offset = "0x1514ACC", VA = "0x101514ACC")]
		public void Setup()
		{
		}

		// Token: 0x0600479F RID: 18335 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60041E5")]
		[Address(RVA = "0x101514E30", Offset = "0x1514E30", VA = "0x101514E30")]
		public PresentWindow GetPresentWindow()
		{
			return null;
		}

		// Token: 0x060047A0 RID: 18336 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60041E6")]
		[Address(RVA = "0x101514E38", Offset = "0x1514E38", VA = "0x101514E38")]
		public ArousalPopupUI GetArousalPopupUI()
		{
			return null;
		}

		// Token: 0x060047A1 RID: 18337 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60041E7")]
		[Address(RVA = "0x101514E40", Offset = "0x1514E40", VA = "0x101514E40")]
		public ArousalResultUI GetArousalResultUI()
		{
			return null;
		}

		// Token: 0x060047A2 RID: 18338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E8")]
		[Address(RVA = "0x101514E48", Offset = "0x1514E48", VA = "0x101514E48")]
		private void Update()
		{
		}

		// Token: 0x060047A3 RID: 18339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041E9")]
		[Address(RVA = "0x1015150C4", Offset = "0x15150C4", VA = "0x1015150C4")]
		private void ChangeStep(PresentUI.eStep step)
		{
		}

		// Token: 0x060047A4 RID: 18340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041EA")]
		[Address(RVA = "0x101515260", Offset = "0x1515260", VA = "0x101515260", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060047A5 RID: 18341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041EB")]
		[Address(RVA = "0x1015154A4", Offset = "0x15154A4", VA = "0x1015154A4")]
		private void LateUpdate()
		{
		}

		// Token: 0x060047A6 RID: 18342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041EC")]
		[Address(RVA = "0x10151557C", Offset = "0x151557C", VA = "0x10151557C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060047A7 RID: 18343 RVA: 0x0001A3E8 File Offset: 0x000185E8
		[Token(Token = "0x60041ED")]
		[Address(RVA = "0x10151561C", Offset = "0x151561C", VA = "0x10151561C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060047A8 RID: 18344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041EE")]
		[Address(RVA = "0x10151562C", Offset = "0x151562C", VA = "0x10151562C")]
		public PresentUI()
		{
		}

		// Token: 0x04005961 RID: 22881
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003EF3")]
		private PresentUI.eStep m_Step;

		// Token: 0x04005962 RID: 22882
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003EF4")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005963 RID: 22883
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003EF5")]
		[SerializeField]
		private PresentWindow m_PresentWindow;

		// Token: 0x04005964 RID: 22884
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003EF6")]
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04005965 RID: 22885
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003EF7")]
		[SerializeField]
		private ArousalPopupUI m_ArousalPopupUI;

		// Token: 0x04005966 RID: 22886
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003EF8")]
		[SerializeField]
		private ArousalResultUI m_ArousalResultUI;

		// Token: 0x04005967 RID: 22887
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003EF9")]
		private PresentUI.eButton m_SelectButton;

		// Token: 0x04005968 RID: 22888
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003EFA")]
		private bool m_EraseGetIcon;

		// Token: 0x02000EE1 RID: 3809
		[Token(Token = "0x2001192")]
		private enum eStep
		{
			// Token: 0x0400596A RID: 22890
			[Token(Token = "0x4006DB8")]
			Hide,
			// Token: 0x0400596B RID: 22891
			[Token(Token = "0x4006DB9")]
			In,
			// Token: 0x0400596C RID: 22892
			[Token(Token = "0x4006DBA")]
			Idle,
			// Token: 0x0400596D RID: 22893
			[Token(Token = "0x4006DBB")]
			Out,
			// Token: 0x0400596E RID: 22894
			[Token(Token = "0x4006DBC")]
			End
		}

		// Token: 0x02000EE2 RID: 3810
		[Token(Token = "0x2001193")]
		public enum eButton
		{
			// Token: 0x04005970 RID: 22896
			[Token(Token = "0x4006DBE")]
			None = -1,
			// Token: 0x04005971 RID: 22897
			[Token(Token = "0x4006DBF")]
			Get,
			// Token: 0x04005972 RID: 22898
			[Token(Token = "0x4006DC0")]
			History
		}
	}
}
