﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Present
{
	// Token: 0x02000EE3 RID: 3811
	[Token(Token = "0x2000A15")]
	[StructLayout(3)]
	public class PresentWindow : UIGroup
	{
		// Token: 0x140000B3 RID: 179
		// (add) Token: 0x060047A9 RID: 18345 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047AA RID: 18346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B3")]
		public event Action<long> OnClickExecuteGet
		{
			[Token(Token = "0x60041EF")]
			[Address(RVA = "0x10151563C", Offset = "0x151563C", VA = "0x10151563C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041F0")]
			[Address(RVA = "0x101515748", Offset = "0x1515748", VA = "0x101515748")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B4 RID: 180
		// (add) Token: 0x060047AB RID: 18347 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047AC RID: 18348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B4")]
		public event Action<List<long>> OnClickGetMultiButton
		{
			[Token(Token = "0x60041F1")]
			[Address(RVA = "0x101515854", Offset = "0x1515854", VA = "0x101515854")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041F2")]
			[Address(RVA = "0x101515960", Offset = "0x1515960", VA = "0x101515960")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B5 RID: 181
		// (add) Token: 0x060047AD RID: 18349 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047AE RID: 18350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B5")]
		public event Action OnEndResultWindow
		{
			[Token(Token = "0x60041F3")]
			[Address(RVA = "0x101515A6C", Offset = "0x1515A6C", VA = "0x101515A6C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041F4")]
			[Address(RVA = "0x101515B78", Offset = "0x1515B78", VA = "0x101515B78")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B6 RID: 182
		// (add) Token: 0x060047AF RID: 18351 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047B0 RID: 18352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B6")]
		public event Action OnClickHistoryButton
		{
			[Token(Token = "0x60041F5")]
			[Address(RVA = "0x101515C84", Offset = "0x1515C84", VA = "0x101515C84")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041F6")]
			[Address(RVA = "0x101515D94", Offset = "0x1515D94", VA = "0x101515D94")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B7 RID: 183
		// (add) Token: 0x060047B1 RID: 18353 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060047B2 RID: 18354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B7")]
		public event Action OnClickReloadButton
		{
			[Token(Token = "0x60041F7")]
			[Address(RVA = "0x101515EA4", Offset = "0x1515EA4", VA = "0x101515EA4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041F8")]
			[Address(RVA = "0x101515FB4", Offset = "0x1515FB4", VA = "0x101515FB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x060047B3 RID: 18355 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004CF")]
		public CustomButton GetMultiButton
		{
			[Token(Token = "0x60041F9")]
			[Address(RVA = "0x10151549C", Offset = "0x151549C", VA = "0x10151549C")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x060047B4 RID: 18356 RVA: 0x0001A400 File Offset: 0x00018600
		[Token(Token = "0x170004D0")]
		public bool IsGetMulti
		{
			[Token(Token = "0x60041FA")]
			[Address(RVA = "0x1015160C4", Offset = "0x15160C4", VA = "0x1015160C4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x060047B5 RID: 18357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041FB")]
		[Address(RVA = "0x101514C58", Offset = "0x1514C58", VA = "0x101514C58")]
		public void Setup()
		{
		}

		// Token: 0x060047B6 RID: 18358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041FC")]
		[Address(RVA = "0x101516904", Offset = "0x1516904", VA = "0x101516904", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060047B7 RID: 18359 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041FD")]
		[Address(RVA = "0x101516968", Offset = "0x1516968", VA = "0x101516968")]
		private void OnEndResultCallBack()
		{
		}

		// Token: 0x060047B8 RID: 18360 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60041FE")]
		[Address(RVA = "0x101516974", Offset = "0x1516974", VA = "0x101516974")]
		private PresentScrollItemData DataToUIData(PresentManager.PresentData data, bool isHistory)
		{
			return null;
		}

		// Token: 0x060047B9 RID: 18361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041FF")]
		[Address(RVA = "0x1015160D4", Offset = "0x15160D4", VA = "0x1015160D4")]
		public void Refresh()
		{
		}

		// Token: 0x060047BA RID: 18362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004200")]
		[Address(RVA = "0x1015165D8", Offset = "0x15165D8", VA = "0x1015165D8")]
		private void SetMode(PresentWindow.eMode mode)
		{
		}

		// Token: 0x060047BB RID: 18363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004201")]
		[Address(RVA = "0x101516E94", Offset = "0x1516E94", VA = "0x101516E94")]
		private void OnClickExecuteGetCallBack(long mngID)
		{
		}

		// Token: 0x060047BC RID: 18364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004202")]
		[Address(RVA = "0x101517150", Offset = "0x1517150", VA = "0x101517150")]
		public void OnClickGetMultiButtonCallBack()
		{
		}

		// Token: 0x060047BD RID: 18365 RVA: 0x0001A418 File Offset: 0x00018618
		[Token(Token = "0x6004203")]
		[Address(RVA = "0x101516F40", Offset = "0x1516F40", VA = "0x101516F40")]
		public int CheckHaveChara(long presentMngID)
		{
			return 0;
		}

		// Token: 0x060047BE RID: 18366 RVA: 0x0001A430 File Offset: 0x00018630
		[Token(Token = "0x6004204")]
		[Address(RVA = "0x101517344", Offset = "0x1517344", VA = "0x101517344")]
		public bool CheckSameCharaPresent(long checkPresentMngID, List<long> presentMngIDList)
		{
			return default(bool);
		}

		// Token: 0x060047BF RID: 18367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004205")]
		[Address(RVA = "0x101516FF4", Offset = "0x1516FF4", VA = "0x101516FF4")]
		public void ConfirmCharaConv()
		{
		}

		// Token: 0x060047C0 RID: 18368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004206")]
		[Address(RVA = "0x10151761C", Offset = "0x151761C", VA = "0x10151761C")]
		private void OnConfirmCharaConv(int btn)
		{
		}

		// Token: 0x060047C1 RID: 18369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004207")]
		[Address(RVA = "0x101517534", Offset = "0x1517534", VA = "0x101517534")]
		private void CallGetMultiButton(List<long> presentMngIDs)
		{
		}

		// Token: 0x060047C2 RID: 18370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004208")]
		[Address(RVA = "0x101517844", Offset = "0x1517844", VA = "0x101517844")]
		public void OpenResult(List<PresentManager.PresentData> list)
		{
		}

		// Token: 0x060047C3 RID: 18371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004209")]
		[Address(RVA = "0x101517AAC", Offset = "0x1517AAC", VA = "0x101517AAC")]
		public void OnClickHistoryButtonCallBack()
		{
		}

		// Token: 0x060047C4 RID: 18372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420A")]
		[Address(RVA = "0x101517AB8", Offset = "0x1517AB8", VA = "0x101517AB8")]
		public void ChangeHistoryMode()
		{
		}

		// Token: 0x060047C5 RID: 18373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420B")]
		[Address(RVA = "0x101517AD0", Offset = "0x1517AD0", VA = "0x101517AD0")]
		public void OnClickReturnButtonCallBack()
		{
		}

		// Token: 0x060047C6 RID: 18374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420C")]
		[Address(RVA = "0x101517AE8", Offset = "0x1517AE8", VA = "0x101517AE8")]
		public void OnClickReloadButtonCallBack()
		{
		}

		// Token: 0x060047C7 RID: 18375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420D")]
		[Address(RVA = "0x101517AF4", Offset = "0x1517AF4", VA = "0x101517AF4")]
		public void OpenFilterWindow()
		{
		}

		// Token: 0x060047C8 RID: 18376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420E")]
		[Address(RVA = "0x101516ACC", Offset = "0x1516ACC", VA = "0x101516ACC")]
		public void ExecuteFilter()
		{
		}

		// Token: 0x060047C9 RID: 18377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600420F")]
		[Address(RVA = "0x101517BD8", Offset = "0x1517BD8", VA = "0x101517BD8")]
		public PresentWindow()
		{
		}

		// Token: 0x04005973 RID: 22899
		[Token(Token = "0x4003EFB")]
		private const int MULTIMAX = 99;

		// Token: 0x04005974 RID: 22900
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003EFC")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005975 RID: 22901
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003EFD")]
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x04005976 RID: 22902
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003EFE")]
		[SerializeField]
		private GameObject m_HistoryObj;

		// Token: 0x04005977 RID: 22903
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003EFF")]
		[SerializeField]
		private RewardSetDisplayGroup m_ResultGroup;

		// Token: 0x04005978 RID: 22904
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003F00")]
		[SerializeField]
		private CustomButton m_GetMultiButton;

		// Token: 0x04005979 RID: 22905
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003F01")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x0400597A RID: 22906
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003F02")]
		private PresentWindow.eMode m_Mode;

		// Token: 0x0400597B RID: 22907
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4003F03")]
		private bool m_ReserveOpen;

		// Token: 0x0400597C RID: 22908
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003F04")]
		private List<PresentScrollItemData> m_DataList;

		// Token: 0x0400597D RID: 22909
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003F05")]
		private List<PresentScrollItemData> m_FilteredDataList;

		// Token: 0x0400597E RID: 22910
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003F06")]
		private List<PresentScrollItemData> m_HistoryList;

		// Token: 0x0400597F RID: 22911
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003F07")]
		private long m_ReadyGetMngID;

		// Token: 0x04005980 RID: 22912
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003F08")]
		private List<long> m_ReadyGetMngIDList;

		// Token: 0x02000EE4 RID: 3812
		[Token(Token = "0x2001194")]
		public enum eMode
		{
			// Token: 0x04005987 RID: 22919
			[Token(Token = "0x4006DC2")]
			Normal,
			// Token: 0x04005988 RID: 22920
			[Token(Token = "0x4006DC3")]
			History
		}
	}
}
