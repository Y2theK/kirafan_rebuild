﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EA1 RID: 3745
	[Token(Token = "0x20009F0")]
	[StructLayout(3)]
	public class ChapterOpenAnim : MonoBehaviour
	{
		// Token: 0x06004643 RID: 17987 RVA: 0x00019ED8 File Offset: 0x000180D8
		[Token(Token = "0x600409C")]
		[Address(RVA = "0x101517D70", Offset = "0x1517D70", VA = "0x101517D70")]
		public bool IsDoneSetTarget()
		{
			return default(bool);
		}

		// Token: 0x06004644 RID: 17988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600409D")]
		[Address(RVA = "0x101517DE0", Offset = "0x1517DE0", VA = "0x101517DE0")]
		private void Start()
		{
		}

		// Token: 0x06004645 RID: 17989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600409E")]
		[Address(RVA = "0x101517E14", Offset = "0x1517E14", VA = "0x101517E14")]
		private void OnDestroy()
		{
		}

		// Token: 0x06004646 RID: 17990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600409F")]
		[Address(RVA = "0x101518014", Offset = "0x1518014", VA = "0x101518014")]
		public void SetChapterPanelObj(ChapterPanel obj)
		{
		}

		// Token: 0x06004647 RID: 17991 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60040A0")]
		[Address(RVA = "0x101518A70", Offset = "0x1518A70", VA = "0x101518A70")]
		public ChapterPanel GetChapterPanelObj()
		{
			return null;
		}

		// Token: 0x06004648 RID: 17992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A1")]
		[Address(RVA = "0x101518A78", Offset = "0x1518A78", VA = "0x101518A78")]
		public void Play(bool isLabelEffect = true)
		{
		}

		// Token: 0x06004649 RID: 17993 RVA: 0x00019EF0 File Offset: 0x000180F0
		[Token(Token = "0x60040A2")]
		[Address(RVA = "0x101518CF4", Offset = "0x1518CF4", VA = "0x101518CF4")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x0600464A RID: 17994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A3")]
		[Address(RVA = "0x101518D04", Offset = "0x1518D04", VA = "0x101518D04")]
		public void Initialize()
		{
		}

		// Token: 0x0600464B RID: 17995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A4")]
		[Address(RVA = "0x101518D9C", Offset = "0x1518D9C", VA = "0x101518D9C")]
		public void SetUpFirst()
		{
		}

		// Token: 0x0600464C RID: 17996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A5")]
		[Address(RVA = "0x101518F30", Offset = "0x1518F30", VA = "0x101518F30")]
		private void Update()
		{
		}

		// Token: 0x0600464D RID: 17997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A6")]
		[Address(RVA = "0x1015198B4", Offset = "0x15198B4", VA = "0x1015198B4")]
		public void PlayIdle()
		{
		}

		// Token: 0x0600464E RID: 17998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A7")]
		[Address(RVA = "0x1015198E4", Offset = "0x15198E4", VA = "0x1015198E4")]
		public void OnClickWaitTapCallBack()
		{
		}

		// Token: 0x0600464F RID: 17999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040A8")]
		[Address(RVA = "0x101519934", Offset = "0x1519934", VA = "0x101519934")]
		public ChapterOpenAnim()
		{
		}

		// Token: 0x04005742 RID: 22338
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003D5B")]
		private ChapterPanel m_TargetChapterPanel;

		// Token: 0x04005743 RID: 22339
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003D5C")]
		[SerializeField]
		private Image m_ImageDark;

		// Token: 0x04005744 RID: 22340
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003D5D")]
		[SerializeField]
		private Image m_ImageWhite;

		// Token: 0x04005745 RID: 22341
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003D5E")]
		[SerializeField]
		private Image m_ImageAdd;

		// Token: 0x04005746 RID: 22342
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003D5F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100129720", Offset = "0x129720")]
		private GameObject m_ScreenObj;

		// Token: 0x04005747 RID: 22343
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D60")]
		[SerializeField]
		private Image m_Fade;

		// Token: 0x04005748 RID: 22344
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D61")]
		[SerializeField]
		private GameObject m_LabelObj;

		// Token: 0x04005749 RID: 22345
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D62")]
		[SerializeField]
		private RectTransform m_PanelEffectRect;

		// Token: 0x0400574A RID: 22346
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D63")]
		[SerializeField]
		private AlphaAnimUI m_OutAnim;

		// Token: 0x0400574B RID: 22347
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D64")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x0400574C RID: 22348
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D65")]
		[SerializeField]
		private Animation m_IdleAnim;

		// Token: 0x0400574D RID: 22349
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D66")]
		private bool m_IsFirstFadeOut;

		// Token: 0x0400574E RID: 22350
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D67")]
		[SerializeField]
		private PixelCrashWrapper m_PixelCrashWrapperPrefab;

		// Token: 0x0400574F RID: 22351
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D68")]
		private PixelCrashWrapper m_PixelCrashWrapper;

		// Token: 0x04005750 RID: 22352
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D69")]
		private ChapterPanel m_ChapterPanelClone;

		// Token: 0x04005751 RID: 22353
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D6A")]
		[SerializeField]
		private Canvas m_SaveAreasCanvas;

		// Token: 0x04005752 RID: 22354
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003D6B")]
		private PostProcessRenderer m_PostProcessRenderer;

		// Token: 0x04005753 RID: 22355
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003D6C")]
		private float m_FadeoutRate;

		// Token: 0x04005754 RID: 22356
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4003D6D")]
		private readonly Color m_PixelCrashBaseColor;

		// Token: 0x04005755 RID: 22357
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4003D6E")]
		private bool m_IsLabelEffect;

		// Token: 0x04005756 RID: 22358
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003D6F")]
		private ChapterOpenAnim.eStep m_Step;

		// Token: 0x02000EA2 RID: 3746
		[Token(Token = "0x2001177")]
		public enum eStep
		{
			// Token: 0x04005758 RID: 22360
			[Token(Token = "0x4006D31")]
			None,
			// Token: 0x04005759 RID: 22361
			[Token(Token = "0x4006D32")]
			Play,
			// Token: 0x0400575A RID: 22362
			[Token(Token = "0x4006D33")]
			PlayInterval,
			// Token: 0x0400575B RID: 22363
			[Token(Token = "0x4006D34")]
			PlaySecond,
			// Token: 0x0400575C RID: 22364
			[Token(Token = "0x4006D35")]
			WaitTap,
			// Token: 0x0400575D RID: 22365
			[Token(Token = "0x4006D36")]
			Out
		}
	}
}
