﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EA3 RID: 3747
	[Token(Token = "0x20009F1")]
	[StructLayout(3)]
	public class ChapterPanel : MonoBehaviour
	{
		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06004650 RID: 18000 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004B2")]
		public GameObject GameObject
		{
			[Token(Token = "0x60040A9")]
			[Address(RVA = "0x101519998", Offset = "0x1519998", VA = "0x101519998")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06004651 RID: 18001 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004B3")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60040AA")]
			[Address(RVA = "0x1015199A0", Offset = "0x15199A0", VA = "0x1015199A0")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x06004652 RID: 18002 RVA: 0x00019F08 File Offset: 0x00018108
		[Token(Token = "0x170004B4")]
		public int Idx
		{
			[Token(Token = "0x60040AB")]
			[Address(RVA = "0x1015199A8", Offset = "0x15199A8", VA = "0x1015199A8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x140000A8 RID: 168
		// (add) Token: 0x06004653 RID: 18003 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004654 RID: 18004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A8")]
		public event Action<int> OnClickPanel
		{
			[Token(Token = "0x60040AC")]
			[Address(RVA = "0x1015199B0", Offset = "0x15199B0", VA = "0x1015199B0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60040AD")]
			[Address(RVA = "0x101519ABC", Offset = "0x1519ABC", VA = "0x101519ABC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004655 RID: 18005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040AE")]
		[Address(RVA = "0x101519BC8", Offset = "0x1519BC8", VA = "0x101519BC8")]
		public void Setup()
		{
		}

		// Token: 0x06004656 RID: 18006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040AF")]
		[Address(RVA = "0x101519C30", Offset = "0x1519C30", VA = "0x101519C30")]
		private void Update()
		{
		}

		// Token: 0x06004657 RID: 18007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B0")]
		[Address(RVA = "0x101519C34", Offset = "0x1519C34", VA = "0x101519C34")]
		public void UpdateSprite()
		{
		}

		// Token: 0x06004658 RID: 18008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B1")]
		[Address(RVA = "0x101519E2C", Offset = "0x1519E2C", VA = "0x101519E2C")]
		public void SetData(int idx, ChapterPanel.ChapterData data)
		{
		}

		// Token: 0x06004659 RID: 18009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B2")]
		[Address(RVA = "0x10151A944", Offset = "0x151A944", VA = "0x10151A944")]
		public void UnloadSprite()
		{
		}

		// Token: 0x0600465A RID: 18010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B3")]
		[Address(RVA = "0x101519D70", Offset = "0x1519D70", VA = "0x101519D70")]
		public void SetSprite(Sprite sprite)
		{
		}

		// Token: 0x0600465B RID: 18011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B4")]
		[Address(RVA = "0x10151AA2C", Offset = "0x151AA2C", VA = "0x10151AA2C")]
		public void OnClickButtonCallBack()
		{
		}

		// Token: 0x0600465C RID: 18012 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60040B5")]
		[Address(RVA = "0x101518A68", Offset = "0x1518A68", VA = "0x101518A68")]
		public Image GetImage()
		{
			return null;
		}

		// Token: 0x0600465D RID: 18013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B6")]
		[Address(RVA = "0x10151AA80", Offset = "0x151AA80", VA = "0x10151AA80")]
		public void AdditionalItem()
		{
		}

		// Token: 0x0600465E RID: 18014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040B7")]
		[Address(RVA = "0x10151ACDC", Offset = "0x151ACDC", VA = "0x10151ACDC")]
		public ChapterPanel()
		{
		}

		// Token: 0x0400575E RID: 22366
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003D70")]
		[SerializeField]
		private Image m_ChapterTitleImage;

		// Token: 0x0400575F RID: 22367
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003D71")]
		[SerializeField]
		private GameObject m_NewIcon;

		// Token: 0x04005760 RID: 22368
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003D72")]
		[SerializeField]
		private GameObject m_TrialIcon;

		// Token: 0x04005761 RID: 22369
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003D73")]
		[SerializeField]
		private GameObject m_CollectIcon;

		// Token: 0x04005762 RID: 22370
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003D74")]
		[SerializeField]
		private GameObject[] m_DifficultyObjs;

		// Token: 0x04005763 RID: 22371
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D75")]
		[SerializeField]
		private GameObject m_ScenarioClearRateObj;

		// Token: 0x04005764 RID: 22372
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D76")]
		[SerializeField]
		private GameObject m_TrialClearObj;

		// Token: 0x04005765 RID: 22373
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D77")]
		[SerializeField]
		private GameObject m_CollectClearObj;

		// Token: 0x04005766 RID: 22374
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D78")]
		[SerializeField]
		private Text[] m_DifficultyClearRateTexts;

		// Token: 0x04005767 RID: 22375
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D79")]
		[SerializeField]
		private Text m_ScenarioClearRateText;

		// Token: 0x04005768 RID: 22376
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D7A")]
		[SerializeField]
		private Text m_TrialClearText;

		// Token: 0x04005769 RID: 22377
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D7B")]
		[SerializeField]
		private Text m_CollectClearText;

		// Token: 0x0400576A RID: 22378
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D7C")]
		private int m_Idx;

		// Token: 0x0400576B RID: 22379
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D7D")]
		private ChapterPanel.ChapterData m_Data;

		// Token: 0x0400576C RID: 22380
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D7E")]
		private GameObject m_GameObject;

		// Token: 0x0400576D RID: 22381
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D7F")]
		private RectTransform m_RectTransform;

		// Token: 0x02000EA4 RID: 3748
		[Token(Token = "0x2001178")]
		public class ChapterData
		{
			// Token: 0x0600465F RID: 18015 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600625A")]
			[Address(RVA = "0x10151ACE4", Offset = "0x151ACE4", VA = "0x10151ACE4")]
			public void LoadSprite()
			{
			}

			// Token: 0x06004660 RID: 18016 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600625B")]
			[Address(RVA = "0x10151AE38", Offset = "0x151AE38", VA = "0x10151AE38")]
			public void LoadOverlaySprite()
			{
			}

			// Token: 0x06004661 RID: 18017 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600625C")]
			[Address(RVA = "0x10151A990", Offset = "0x151A990", VA = "0x10151A990")]
			public void UnloadSprite()
			{
			}

			// Token: 0x06004662 RID: 18018 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600625D")]
			[Address(RVA = "0x10151AED4", Offset = "0x151AED4", VA = "0x10151AED4")]
			public void UnloadOverlaySprite()
			{
			}

			// Token: 0x06004663 RID: 18019 RVA: 0x00019F20 File Offset: 0x00018120
			[Token(Token = "0x600625E")]
			[Address(RVA = "0x10151AF70", Offset = "0x151AF70", VA = "0x10151AF70")]
			public bool IsLoading()
			{
				return default(bool);
			}

			// Token: 0x06004664 RID: 18020 RVA: 0x00019F38 File Offset: 0x00018138
			[Token(Token = "0x600625F")]
			[Address(RVA = "0x10151AFBC", Offset = "0x151AFBC", VA = "0x10151AFBC")]
			public bool IsEnableSpriteHandler()
			{
				return default(bool);
			}

			// Token: 0x06004665 RID: 18021 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006260")]
			[Address(RVA = "0x101519CF8", Offset = "0x1519CF8", VA = "0x101519CF8")]
			public Sprite GetSprite()
			{
				return null;
			}

			// Token: 0x06004666 RID: 18022 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006261")]
			[Address(RVA = "0x10151AFCC", Offset = "0x151AFCC", VA = "0x10151AFCC")]
			public ChapterData()
			{
			}

			// Token: 0x0400576F RID: 22383
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006D37")]
			public int m_Part;

			// Token: 0x04005770 RID: 22384
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006D38")]
			public int m_ChapterID;

			// Token: 0x04005771 RID: 22385
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006D39")]
			public bool m_IsChapterClear;

			// Token: 0x04005772 RID: 22386
			[Cpp2IlInjected.FieldOffset(Offset = "0x19")]
			[Token(Token = "0x4006D3A")]
			public bool m_IsTrialClear;

			// Token: 0x04005773 RID: 22387
			[Cpp2IlInjected.FieldOffset(Offset = "0x1A")]
			[Token(Token = "0x4006D3B")]
			public bool m_IsCollectClear;

			// Token: 0x04005774 RID: 22388
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006D3C")]
			public int[] m_ClearPercents;

			// Token: 0x04005775 RID: 22389
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006D3D")]
			public bool m_IsComingSoon;

			// Token: 0x04005776 RID: 22390
			[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
			[Token(Token = "0x4006D3E")]
			public bool m_AdditionalItem;

			// Token: 0x04005777 RID: 22391
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006D3F")]
			private SpriteHandler m_SpriteHandler;

			// Token: 0x04005778 RID: 22392
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006D40")]
			private SpriteHandler m_OverlaySpriteHandler;
		}
	}
}
