﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EA5 RID: 3749
	[Token(Token = "0x20009F2")]
	[StructLayout(3)]
	public class ChapterSelectScroll : MonoBehaviour, IDragHandler, IEventSystemHandler, IEndDragHandler
	{
		// Token: 0x140000A9 RID: 169
		// (add) Token: 0x06004667 RID: 18023 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004668 RID: 18024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A9")]
		public event Action<int> OnClickChapterButton
		{
			[Token(Token = "0x60040B8")]
			[Address(RVA = "0x10151AFD4", Offset = "0x151AFD4", VA = "0x10151AFD4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60040B9")]
			[Address(RVA = "0x10151B0E0", Offset = "0x151B0E0", VA = "0x10151B0E0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004669 RID: 18025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040BA")]
		[Address(RVA = "0x10151B1EC", Offset = "0x151B1EC", VA = "0x10151B1EC")]
		public void Setup()
		{
		}

		// Token: 0x0600466A RID: 18026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040BB")]
		[Address(RVA = "0x10151B32C", Offset = "0x151B32C", VA = "0x10151B32C")]
		public void Destroy()
		{
		}

		// Token: 0x0600466B RID: 18027 RVA: 0x00019F50 File Offset: 0x00018150
		[Token(Token = "0x60040BC")]
		[Address(RVA = "0x10151B588", Offset = "0x151B588", VA = "0x10151B588")]
		public bool IsLoadingAny()
		{
			return default(bool);
		}

		// Token: 0x0600466C RID: 18028 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60040BD")]
		[Address(RVA = "0x10151B664", Offset = "0x151B664", VA = "0x10151B664")]
		public ChapterPanel GetChapterPanel(int chapterIdx)
		{
			return null;
		}

		// Token: 0x0600466D RID: 18029 RVA: 0x00019F68 File Offset: 0x00018168
		[Token(Token = "0x60040BE")]
		[Address(RVA = "0x10151B764", Offset = "0x151B764", VA = "0x10151B764")]
		public int GetExistChapterPanelNum()
		{
			return 0;
		}

		// Token: 0x0600466E RID: 18030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040BF")]
		[Address(RVA = "0x10151B7C4", Offset = "0x151B7C4", VA = "0x10151B7C4")]
		private void UpdateContentWidth()
		{
		}

		// Token: 0x0600466F RID: 18031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C0")]
		[Address(RVA = "0x10151B948", Offset = "0x151B948", VA = "0x10151B948")]
		private void Update()
		{
		}

		// Token: 0x06004670 RID: 18032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C1")]
		[Address(RVA = "0x10151BF5C", Offset = "0x151BF5C", VA = "0x10151BF5C")]
		private void UpdateItemPosition()
		{
		}

		// Token: 0x06004671 RID: 18033 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60040C2")]
		[Address(RVA = "0x10151C188", Offset = "0x151C188", VA = "0x10151C188")]
		public List<int> GetShowIdxList(int centerIndex)
		{
			return null;
		}

		// Token: 0x06004672 RID: 18034 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60040C3")]
		[Address(RVA = "0x10151CB70", Offset = "0x151CB70", VA = "0x10151CB70")]
		public List<int> GetShowIdxList()
		{
			return null;
		}

		// Token: 0x06004673 RID: 18035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C4")]
		[Address(RVA = "0x10151C290", Offset = "0x151C290", VA = "0x10151C290")]
		private void InactiveChapterPanel(IEnumerable<int> list)
		{
		}

		// Token: 0x06004674 RID: 18036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C5")]
		[Address(RVA = "0x10151C568", Offset = "0x151C568", VA = "0x10151C568")]
		private void ActiveChapterPanel(IEnumerable<int> list)
		{
		}

		// Token: 0x06004675 RID: 18037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C6")]
		[Address(RVA = "0x10151CB78", Offset = "0x151CB78", VA = "0x10151CB78")]
		public void AddItem(ChapterPanel.ChapterData data)
		{
		}

		// Token: 0x06004676 RID: 18038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C7")]
		[Address(RVA = "0x10151B324", Offset = "0x151B324", VA = "0x10151B324")]
		public void SetSelectChapterIdx(int chapterIdx)
		{
		}

		// Token: 0x06004677 RID: 18039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C8")]
		[Address(RVA = "0x10151BDDC", Offset = "0x151BDDC", VA = "0x10151BDDC")]
		public void SetCurrentIdx(int currentIdx, bool immediate = false)
		{
		}

		// Token: 0x06004678 RID: 18040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040C9")]
		[Address(RVA = "0x10151CE74", Offset = "0x151CE74", VA = "0x10151CE74")]
		public void ForceUpdateSprite()
		{
		}

		// Token: 0x06004679 RID: 18041 RVA: 0x00019F80 File Offset: 0x00018180
		[Token(Token = "0x60040CA")]
		[Address(RVA = "0x10151BDC4", Offset = "0x151BDC4", VA = "0x10151BDC4")]
		private float GetIdxPosition(int idx)
		{
			return 0f;
		}

		// Token: 0x0600467A RID: 18042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040CB")]
		[Address(RVA = "0x10151CF38", Offset = "0x151CF38", VA = "0x10151CF38", Slot = "4")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x0600467B RID: 18043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040CC")]
		[Address(RVA = "0x10151CF44", Offset = "0x151CF44", VA = "0x10151CF44", Slot = "5")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x0600467C RID: 18044 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040CD")]
		[Address(RVA = "0x10151CF4C", Offset = "0x151CF4C", VA = "0x10151CF4C")]
		public void OnLeftButtonCallBack()
		{
		}

		// Token: 0x0600467D RID: 18045 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040CE")]
		[Address(RVA = "0x10151CF5C", Offset = "0x151CF5C", VA = "0x10151CF5C")]
		public void OnRightButtonCallBack()
		{
		}

		// Token: 0x0600467E RID: 18046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040CF")]
		[Address(RVA = "0x10151CF6C", Offset = "0x151CF6C", VA = "0x10151CF6C")]
		public void OnClickChapterButtonCallBack(int idx)
		{
		}

		// Token: 0x0600467F RID: 18047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040D0")]
		[Address(RVA = "0x10151D16C", Offset = "0x151D16C", VA = "0x10151D16C")]
		public void ActiveAdditionalItem(int selectIdx)
		{
		}

		// Token: 0x06004680 RID: 18048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040D1")]
		[Address(RVA = "0x10151D26C", Offset = "0x151D26C", VA = "0x10151D26C")]
		public ChapterSelectScroll()
		{
		}

		// Token: 0x04005779 RID: 22393
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003D81")]
		private int MAX_SHOW_PANEL;

		// Token: 0x0400577A RID: 22394
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003D82")]
		private int PANEL_MAX_PER_PAGE;

		// Token: 0x0400577B RID: 22395
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003D83")]
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x0400577C RID: 22396
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003D84")]
		[SerializeField]
		private ChapterPanel m_ChapterPanelPrefab;

		// Token: 0x0400577D RID: 22397
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003D85")]
		[SerializeField]
		private CustomButton m_LButton;

		// Token: 0x0400577E RID: 22398
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003D86")]
		[SerializeField]
		private CustomButton m_RButton;

		// Token: 0x0400577F RID: 22399
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D87")]
		private float m_Space;

		// Token: 0x04005780 RID: 22400
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D88")]
		private List<ChapterPanel.ChapterData> m_DataList;

		// Token: 0x04005781 RID: 22401
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D89")]
		private List<ChapterPanel> m_InstList;

		// Token: 0x04005782 RID: 22402
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D8A")]
		private List<ChapterPanel> m_EmptyInstList;

		// Token: 0x04005783 RID: 22403
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D8B")]
		private List<ChapterPanel> m_ActiveInstList;

		// Token: 0x04005784 RID: 22404
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D8C")]
		private bool m_IsDirty;

		// Token: 0x04005785 RID: 22405
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4003D8D")]
		private float m_PanelWidth;

		// Token: 0x04005786 RID: 22406
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D8E")]
		private bool m_Drag;

		// Token: 0x04005787 RID: 22407
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003D8F")]
		private float m_ScrollRate;

		// Token: 0x04005788 RID: 22408
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D90")]
		private int m_CurrentIdx;

		// Token: 0x0400578A RID: 22410
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D92")]
		private RectTransform m_ViewPort;

		// Token: 0x0400578B RID: 22411
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D93")]
		private RectTransform m_Content;
	}
}
