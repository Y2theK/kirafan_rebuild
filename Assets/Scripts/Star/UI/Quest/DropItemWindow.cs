﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000ED2 RID: 3794
	[Token(Token = "0x2000A0A")]
	[StructLayout(3)]
	public class DropItemWindow : UIGroup
	{
		// Token: 0x06004743 RID: 18243 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004189")]
		[Address(RVA = "0x10151D39C", Offset = "0x151D39C", VA = "0x10151D39C")]
		public void Open(List<int> dropItemIDs)
		{
		}

		// Token: 0x06004744 RID: 18244 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600418A")]
		[Address(RVA = "0x10151D43C", Offset = "0x151D43C", VA = "0x10151D43C", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004745 RID: 18245 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600418B")]
		[Address(RVA = "0x10151D5D8", Offset = "0x151D5D8", VA = "0x10151D5D8", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06004746 RID: 18246 RVA: 0x0001A2E0 File Offset: 0x000184E0
		[Token(Token = "0x600418C")]
		[Address(RVA = "0x10151D724", Offset = "0x151D724", VA = "0x10151D724", Slot = "14")]
		public override bool IsCompleteFinalize()
		{
			return default(bool);
		}

		// Token: 0x06004747 RID: 18247 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600418D")]
		[Address(RVA = "0x10151D72C", Offset = "0x151D72C", VA = "0x10151D72C")]
		public DropItemWindow()
		{
		}

		// Token: 0x040058C5 RID: 22725
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003E6C")]
		[SerializeField]
		private ItemIconWithDetail m_ItemIconPrefab;

		// Token: 0x040058C6 RID: 22726
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003E6D")]
		[SerializeField]
		private RectTransform m_ItemIconParent;

		// Token: 0x040058C7 RID: 22727
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003E6E")]
		private List<int> m_ItemIDList;

		// Token: 0x040058C8 RID: 22728
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003E6F")]
		private List<ItemIconWithDetail> m_InstList;
	}
}
