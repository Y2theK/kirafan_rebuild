﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EBA RID: 3770
	[Token(Token = "0x20009FC")]
	[StructLayout(3)]
	public class LapQuestCategory : MonoBehaviour
	{
		// Token: 0x060046C4 RID: 18116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004110")]
		[Address(RVA = "0x10151D79C", Offset = "0x151D79C", VA = "0x10151D79C")]
		public void Setup(bool isNew, bool isStaminaReduction, bool isExistRepeat)
		{
		}

		// Token: 0x060046C5 RID: 18117 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004111")]
		[Address(RVA = "0x10151D9E0", Offset = "0x151D9E0", VA = "0x10151D9E0")]
		public LapQuestCategory()
		{
		}

		// Token: 0x04005814 RID: 22548
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003DE5")]
		[SerializeField]
		private SwitchFade m_SwitchFade;

		// Token: 0x04005815 RID: 22549
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003DE6")]
		[SerializeField]
		private CanvasGroup m_NewImage;

		// Token: 0x04005816 RID: 22550
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003DE7")]
		[SerializeField]
		private CanvasGroup m_StaminaImage;

		// Token: 0x04005817 RID: 22551
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003DE8")]
		[SerializeField]
		private CanvasGroup m_ExistImage;

		// Token: 0x04005818 RID: 22552
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003DE9")]
		[SerializeField]
		private GameObject m_SpeechBubble;
	}
}
