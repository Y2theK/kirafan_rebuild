﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000E9B RID: 3739
	[Token(Token = "0x20009ED")]
	[StructLayout(3)]
	public class QuestCategoryItem : ScrollItemIcon
	{
		// Token: 0x0600462C RID: 17964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004085")]
		[Address(RVA = "0x10151D9E8", Offset = "0x151D9E8", VA = "0x10151D9E8", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600462D RID: 17965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004086")]
		[Address(RVA = "0x10151DF3C", Offset = "0x151DF3C", VA = "0x10151DF3C")]
		public QuestCategoryItem()
		{
		}

		// Token: 0x0400571A RID: 22298
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D42")]
		[SerializeField]
		private QuestEventTypeIcon m_Icon;

		// Token: 0x0400571B RID: 22299
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D43")]
		[SerializeField]
		private GameObject m_SpanObj;

		// Token: 0x0400571C RID: 22300
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D44")]
		[SerializeField]
		private Text m_SpanText;

		// Token: 0x0400571D RID: 22301
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D45")]
		[SerializeField]
		private SwitchFade m_SwitchFade;

		// Token: 0x0400571E RID: 22302
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D46")]
		[SerializeField]
		private CanvasGroup m_NewIcon;

		// Token: 0x0400571F RID: 22303
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D47")]
		[SerializeField]
		private CanvasGroup m_StaminaReduction;

		// Token: 0x04005720 RID: 22304
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D48")]
		[SerializeField]
		private CanvasGroup m_NewChapter;

		// Token: 0x04005721 RID: 22305
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D49")]
		[SerializeField]
		private GameObject m_Loading;

		// Token: 0x02000E9C RID: 3740
		[Token(Token = "0x2001174")]
		public enum eSpanDispType
		{
			// Token: 0x04005723 RID: 22307
			[Token(Token = "0x4006D22")]
			None,
			// Token: 0x04005724 RID: 22308
			[Token(Token = "0x4006D23")]
			Span,
			// Token: 0x04005725 RID: 22309
			[Token(Token = "0x4006D24")]
			LossTime
		}
	}
}
