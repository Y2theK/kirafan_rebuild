﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000E9D RID: 3741
	[Token(Token = "0x20009EE")]
	[StructLayout(3)]
	public class QuestCategoryItemData : ScrollItemData
	{
		// Token: 0x140000A7 RID: 167
		// (add) Token: 0x0600462E RID: 17966 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600462F RID: 17967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A7")]
		public event Action<QuestDefine.eUICategory, int> OnClick
		{
			[Token(Token = "0x6004087")]
			[Address(RVA = "0x10151DF44", Offset = "0x151DF44", VA = "0x10151DF44")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004088")]
			[Address(RVA = "0x10151E050", Offset = "0x151E050", VA = "0x10151E050")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004630 RID: 17968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004089")]
		[Address(RVA = "0x10151E15C", Offset = "0x151E15C", VA = "0x10151E15C")]
		public QuestCategoryItemData(QuestDefine.eUICategory questUICategory, int eventType, QuestCategoryItem.eSpanDispType spanType, TimeSpan span, bool isExistStaminaReduction, bool isNew, bool isNewChapter, Action<QuestDefine.eUICategory, int> callback)
		{
		}

		// Token: 0x06004631 RID: 17969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600408A")]
		[Address(RVA = "0x10151E1E0", Offset = "0x151E1E0", VA = "0x10151E1E0", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005726 RID: 22310
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003D4A")]
		public QuestDefine.eUICategory m_QuestUICategory;

		// Token: 0x04005727 RID: 22311
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003D4B")]
		public int m_EventType;

		// Token: 0x04005728 RID: 22312
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003D4C")]
		public QuestCategoryItem.eSpanDispType m_SpanDispType;

		// Token: 0x04005729 RID: 22313
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003D4D")]
		public TimeSpan m_Span;

		// Token: 0x0400572A RID: 22314
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003D4E")]
		public bool m_IsExistStaminaReduction;

		// Token: 0x0400572B RID: 22315
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003D4F")]
		public bool m_IsNew;

		// Token: 0x0400572C RID: 22316
		[Cpp2IlInjected.FieldOffset(Offset = "0x32")]
		[Token(Token = "0x4003D50")]
		public bool m_IsNewChapter;
	}
}
