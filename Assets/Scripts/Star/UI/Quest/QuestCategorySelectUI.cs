﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000E9E RID: 3742
	[Token(Token = "0x20009EF")]
	[StructLayout(3)]
	public class QuestCategorySelectUI : MenuUIBase
	{
		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06004632 RID: 17970 RVA: 0x00019E60 File Offset: 0x00018060
		[Token(Token = "0x170004AE")]
		public bool IsSelectQuestUICategory
		{
			[Token(Token = "0x600408B")]
			[Address(RVA = "0x10151E234", Offset = "0x151E234", VA = "0x10151E234")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06004633 RID: 17971 RVA: 0x00019E78 File Offset: 0x00018078
		[Token(Token = "0x170004AF")]
		public QuestDefine.eUICategory SelectQuestUICategory
		{
			[Token(Token = "0x600408C")]
			[Address(RVA = "0x10151E244", Offset = "0x151E244", VA = "0x10151E244")]
			get
			{
				return QuestDefine.eUICategory.None;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06004634 RID: 17972 RVA: 0x00019E90 File Offset: 0x00018090
		[Token(Token = "0x170004B0")]
		public int SelectEventType
		{
			[Token(Token = "0x600408D")]
			[Address(RVA = "0x10151E24C", Offset = "0x151E24C", VA = "0x10151E24C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06004635 RID: 17973 RVA: 0x00019EA8 File Offset: 0x000180A8
		[Token(Token = "0x170004B1")]
		public float ScrollNormalizedPosition
		{
			[Token(Token = "0x600408E")]
			[Address(RVA = "0x10151E254", Offset = "0x151E254", VA = "0x10151E254")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x06004636 RID: 17974 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600408F")]
		[Address(RVA = "0x10151E284", Offset = "0x151E284", VA = "0x10151E284")]
		public void Setup(float normalizePosition)
		{
		}

		// Token: 0x06004637 RID: 17975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004090")]
		[Address(RVA = "0x10151EAB8", Offset = "0x151EAB8", VA = "0x10151EAB8")]
		private void LapQuestSetup(QuestCategorySelectUI.eQuestType questType)
		{
		}

		// Token: 0x06004638 RID: 17976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004091")]
		[Address(RVA = "0x10151E8B4", Offset = "0x151E8B4", VA = "0x10151E8B4")]
		private void AddEventTypeButton(int eventType)
		{
		}

		// Token: 0x06004639 RID: 17977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004092")]
		[Address(RVA = "0x10151ECD8", Offset = "0x151ECD8", VA = "0x10151ECD8")]
		private void Update()
		{
		}

		// Token: 0x0600463A RID: 17978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004093")]
		[Address(RVA = "0x10151EF08", Offset = "0x151EF08", VA = "0x10151EF08")]
		private void ChangeStep(QuestCategorySelectUI.eStep step)
		{
		}

		// Token: 0x0600463B RID: 17979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004094")]
		[Address(RVA = "0x10151F0A4", Offset = "0x151F0A4", VA = "0x10151F0A4", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600463C RID: 17980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004095")]
		[Address(RVA = "0x10151F14C", Offset = "0x151F14C", VA = "0x10151F14C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600463D RID: 17981 RVA: 0x00019EC0 File Offset: 0x000180C0
		[Token(Token = "0x6004096")]
		[Address(RVA = "0x10151F1EC", Offset = "0x151F1EC", VA = "0x10151F1EC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600463E RID: 17982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004097")]
		[Address(RVA = "0x10151F1FC", Offset = "0x151F1FC", VA = "0x10151F1FC")]
		public void OnClickQuestCategoryButtonCallBack(QuestDefine.eUICategory questUICategory, int eventType)
		{
		}

		// Token: 0x0600463F RID: 17983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004098")]
		[Address(RVA = "0x10151F298", Offset = "0x151F298", VA = "0x10151F298")]
		public void OnClickDayOfWeekQuestButtonCallBack()
		{
		}

		// Token: 0x06004640 RID: 17984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004099")]
		[Address(RVA = "0x10151F32C", Offset = "0x151F32C", VA = "0x10151F32C")]
		public void OnClickCraftQuestButtonCallBack()
		{
		}

		// Token: 0x06004641 RID: 17985 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600409A")]
		[Address(RVA = "0x10151F3C0", Offset = "0x151F3C0", VA = "0x10151F3C0")]
		public void OnClickMemorialQuestButtonCallBack()
		{
		}

		// Token: 0x06004642 RID: 17986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600409B")]
		[Address(RVA = "0x10151F454", Offset = "0x151F454", VA = "0x10151F454")]
		public QuestCategorySelectUI()
		{
		}

		// Token: 0x0400572E RID: 22318
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D52")]
		private QuestCategorySelectUI.eStep m_Step;

		// Token: 0x0400572F RID: 22319
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D53")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005730 RID: 22320
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D54")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005731 RID: 22321
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D55")]
		[SerializeField]
		private HorizontalScrollView m_HorizontalScroll;

		// Token: 0x04005732 RID: 22322
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D56")]
		[SerializeField]
		private LapQuestCategory[] m_LapQuestCategories;

		// Token: 0x04005733 RID: 22323
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D57")]
		private CustomButton[] m_CategoryButtons;

		// Token: 0x04005734 RID: 22324
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D58")]
		public QuestDefine.eUICategory m_SelectQuestUICategory;

		// Token: 0x04005735 RID: 22325
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003D59")]
		private int m_SelectEventType;

		// Token: 0x04005736 RID: 22326
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D5A")]
		public Action OnClickQuestUICategory;

		// Token: 0x02000E9F RID: 3743
		[Token(Token = "0x2001175")]
		private enum eQuestType
		{
			// Token: 0x04005738 RID: 22328
			[Token(Token = "0x4006D26")]
			None = -1,
			// Token: 0x04005739 RID: 22329
			[Token(Token = "0x4006D27")]
			DayOfWeek,
			// Token: 0x0400573A RID: 22330
			[Token(Token = "0x4006D28")]
			OfferQuest,
			// Token: 0x0400573B RID: 22331
			[Token(Token = "0x4006D29")]
			CharaQuest
		}

		// Token: 0x02000EA0 RID: 3744
		[Token(Token = "0x2001176")]
		private enum eStep
		{
			// Token: 0x0400573D RID: 22333
			[Token(Token = "0x4006D2B")]
			Hide,
			// Token: 0x0400573E RID: 22334
			[Token(Token = "0x4006D2C")]
			In,
			// Token: 0x0400573F RID: 22335
			[Token(Token = "0x4006D2D")]
			Idle,
			// Token: 0x04005740 RID: 22336
			[Token(Token = "0x4006D2E")]
			Out,
			// Token: 0x04005741 RID: 22337
			[Token(Token = "0x4006D2F")]
			End
		}
	}
}
