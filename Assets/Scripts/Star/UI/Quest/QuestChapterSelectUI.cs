﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.AeUIAnim;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EA8 RID: 3752
	[Token(Token = "0x20009F3")]
	[StructLayout(3)]
	public class QuestChapterSelectUI : MenuUIBase
	{
		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06004686 RID: 18054 RVA: 0x00019FC8 File Offset: 0x000181C8
		[Token(Token = "0x170004B5")]
		public QuestChapterSelectUI.eButton SelectButton
		{
			[Token(Token = "0x60040D2")]
			[Address(RVA = "0x10151F464", Offset = "0x151F464", VA = "0x10151F464")]
			get
			{
				return QuestChapterSelectUI.eButton.Back;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06004687 RID: 18055 RVA: 0x00019FE0 File Offset: 0x000181E0
		[Token(Token = "0x170004B6")]
		public int SelectChapterId
		{
			[Token(Token = "0x60040D3")]
			[Address(RVA = "0x10151F46C", Offset = "0x151F46C", VA = "0x10151F46C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06004688 RID: 18056 RVA: 0x00019FF8 File Offset: 0x000181F8
		[Token(Token = "0x170004B7")]
		public bool IsPlayChapterAnim
		{
			[Token(Token = "0x60040D4")]
			[Address(RVA = "0x10151F474", Offset = "0x151F474", VA = "0x10151F474")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004689 RID: 18057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040D5")]
		[Address(RVA = "0x10151F4F8", Offset = "0x151F4F8", VA = "0x10151F4F8")]
		private void Awake()
		{
		}

		// Token: 0x0600468A RID: 18058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040D6")]
		[Address(RVA = "0x10151F550", Offset = "0x151F550", VA = "0x10151F550")]
		private void OnDestroy()
		{
		}

		// Token: 0x0600468B RID: 18059 RVA: 0x0001A010 File Offset: 0x00018210
		[Token(Token = "0x60040D7")]
		[Address(RVA = "0x10151F55C", Offset = "0x151F55C", VA = "0x10151F55C")]
		public static bool UseClearedTexture(int part)
		{
			return default(bool);
		}

		// Token: 0x0600468C RID: 18060 RVA: 0x0001A028 File Offset: 0x00018228
		[Token(Token = "0x60040D8")]
		[Address(RVA = "0x10151F568", Offset = "0x151F568", VA = "0x10151F568")]
		private static QuestChapterSelectUI.eOpenAnimType GetOpenAnimType(int part)
		{
			return QuestChapterSelectUI.eOpenAnimType.Ae;
		}

		// Token: 0x0600468D RID: 18061 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040D9")]
		[Address(RVA = "0x10151F574", Offset = "0x151F574", VA = "0x10151F574")]
		public void Setup(int chapterID, bool playClearAnim, bool playOpenAnim)
		{
		}

		// Token: 0x0600468E RID: 18062 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DA")]
		[Address(RVA = "0x10151FDA4", Offset = "0x151FDA4", VA = "0x10151FDA4", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x0600468F RID: 18063 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DB")]
		[Address(RVA = "0x10151FEC8", Offset = "0x151FEC8", VA = "0x10151FEC8")]
		private void Update()
		{
		}

		// Token: 0x06004690 RID: 18064 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DC")]
		[Address(RVA = "0x101520E68", Offset = "0x1520E68", VA = "0x101520E68")]
		private void ChangeStep(QuestChapterSelectUI.eStep step)
		{
		}

		// Token: 0x06004691 RID: 18065 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DD")]
		[Address(RVA = "0x101520F70", Offset = "0x1520F70", VA = "0x101520F70", Slot = "9")]
		public override void SetEnableInput(bool isEnable)
		{
		}

		// Token: 0x06004692 RID: 18066 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DE")]
		[Address(RVA = "0x101521018", Offset = "0x1521018", VA = "0x101521018", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004693 RID: 18067 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040DF")]
		[Address(RVA = "0x101521020", Offset = "0x1521020", VA = "0x101521020", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004694 RID: 18068 RVA: 0x0001A040 File Offset: 0x00018240
		[Token(Token = "0x60040E0")]
		[Address(RVA = "0x101521138", Offset = "0x1521138", VA = "0x101521138", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004695 RID: 18069 RVA: 0x0001A058 File Offset: 0x00018258
		[Token(Token = "0x60040E1")]
		[Address(RVA = "0x101521148", Offset = "0x1521148", VA = "0x101521148")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06004696 RID: 18070 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E2")]
		[Address(RVA = "0x101521158", Offset = "0x1521158", VA = "0x101521158")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x06004697 RID: 18071 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E3")]
		[Address(RVA = "0x10152116C", Offset = "0x152116C", VA = "0x10152116C")]
		public void OnClickChapterButtonCallBack(int chapterId)
		{
		}

		// Token: 0x06004698 RID: 18072 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E4")]
		[Address(RVA = "0x101521180", Offset = "0x1521180", VA = "0x101521180")]
		private void OnResponseOpenAnimSave()
		{
		}

		// Token: 0x06004699 RID: 18073 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E5")]
		[Address(RVA = "0x1015211A8", Offset = "0x15211A8", VA = "0x1015211A8")]
		public QuestChapterSelectUI()
		{
		}

		// Token: 0x0400578F RID: 22415
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D94")]
		private QuestChapterSelectUI.eStep m_Step;

		// Token: 0x04005790 RID: 22416
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D95")]
		[SerializeField]
		private ChapterSelectScroll m_Scroll;

		// Token: 0x04005791 RID: 22417
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D96")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimArray;

		// Token: 0x04005792 RID: 22418
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D97")]
		[SerializeField]
		private UIGroup[] m_UIGroups;

		// Token: 0x04005793 RID: 22419
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D98")]
		[SerializeField]
		private ChapterOpenAnim m_ChapterOpenAnim;

		// Token: 0x04005794 RID: 22420
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D99")]
		[SerializeField]
		private AeUIAnim m_ClearAnim;

		// Token: 0x04005795 RID: 22421
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D9A")]
		[SerializeField]
		private AeUIAnim m_OpenAnimAe;

		// Token: 0x04005796 RID: 22422
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D9B")]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04005797 RID: 22423
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D9C")]
		private QuestChapterSelectUI.eButton m_SelectButton;

		// Token: 0x04005798 RID: 22424
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003D9D")]
		private int m_SelectChapterId;

		// Token: 0x04005799 RID: 22425
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D9E")]
		private int m_Part;

		// Token: 0x0400579A RID: 22426
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D9F")]
		private List<ChapterPanel.ChapterData> m_List;

		// Token: 0x0400579B RID: 22427
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003DA0")]
		private ChapterPanel m_ChapterPanelClone;

		// Token: 0x0400579C RID: 22428
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003DA1")]
		private int m_OpenChapterIdx;

		// Token: 0x0400579D RID: 22429
		[Cpp2IlInjected.FieldOffset(Offset = "0xA4")]
		[Token(Token = "0x4003DA2")]
		private bool m_IsPlayOpenAnim;

		// Token: 0x0400579E RID: 22430
		[Cpp2IlInjected.FieldOffset(Offset = "0xA5")]
		[Token(Token = "0x4003DA3")]
		private bool m_IsPlayClearAnim;

		// Token: 0x0400579F RID: 22431
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003DA4")]
		private float m_AeSpriteChangeSecCount;

		// Token: 0x040057A0 RID: 22432
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003DA5")]
		private Dictionary<int, List<ChapterPanel>> m_ChapterPanelHolder;

		// Token: 0x040057A1 RID: 22433
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003DA6")]
		public Action OnClickChapter;

		// Token: 0x040057A2 RID: 22434
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003DA7")]
		private GeneralFlagAPI m_GeneralFlagAPI;

		// Token: 0x040057A3 RID: 22435
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003DA8")]
		private SoundSeRequest m_SeRequest_Open1;

		// Token: 0x040057A4 RID: 22436
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003DA9")]
		private SoundSeRequest m_SeRequest_Clear2;

		// Token: 0x040057A5 RID: 22437
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003DAA")]
		private SoundSeRequest m_SeRequest_Open2;

		// Token: 0x040057A6 RID: 22438
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003DAB")]
		private int m_ClearChapterIdx;

		// Token: 0x02000EA9 RID: 3753
		[Token(Token = "0x200117B")]
		private enum eStep
		{
			// Token: 0x040057A8 RID: 22440
			[Token(Token = "0x4006D45")]
			Hide,
			// Token: 0x040057A9 RID: 22441
			[Token(Token = "0x4006D46")]
			LoadWait,
			// Token: 0x040057AA RID: 22442
			[Token(Token = "0x4006D47")]
			In,
			// Token: 0x040057AB RID: 22443
			[Token(Token = "0x4006D48")]
			ClearAnim,
			// Token: 0x040057AC RID: 22444
			[Token(Token = "0x4006D49")]
			ClearAnimWait,
			// Token: 0x040057AD RID: 22445
			[Token(Token = "0x4006D4A")]
			OpenAnimCheck,
			// Token: 0x040057AE RID: 22446
			[Token(Token = "0x4006D4B")]
			OpenAnimStart,
			// Token: 0x040057AF RID: 22447
			[Token(Token = "0x4006D4C")]
			OpenAnimWait,
			// Token: 0x040057B0 RID: 22448
			[Token(Token = "0x4006D4D")]
			OpenAnimAe,
			// Token: 0x040057B1 RID: 22449
			[Token(Token = "0x4006D4E")]
			OpenAnimAeWait,
			// Token: 0x040057B2 RID: 22450
			[Token(Token = "0x4006D4F")]
			OpenAnimSaveCheck,
			// Token: 0x040057B3 RID: 22451
			[Token(Token = "0x4006D50")]
			OpenAnimSaveRequest,
			// Token: 0x040057B4 RID: 22452
			[Token(Token = "0x4006D51")]
			OpenAnimSaveWait,
			// Token: 0x040057B5 RID: 22453
			[Token(Token = "0x4006D52")]
			Idle,
			// Token: 0x040057B6 RID: 22454
			[Token(Token = "0x4006D53")]
			Out,
			// Token: 0x040057B7 RID: 22455
			[Token(Token = "0x4006D54")]
			End
		}

		// Token: 0x02000EAA RID: 3754
		[Token(Token = "0x200117C")]
		public enum eButton
		{
			// Token: 0x040057B9 RID: 22457
			[Token(Token = "0x4006D56")]
			None = -1,
			// Token: 0x040057BA RID: 22458
			[Token(Token = "0x4006D57")]
			Back,
			// Token: 0x040057BB RID: 22459
			[Token(Token = "0x4006D58")]
			Chapter
		}

		// Token: 0x02000EAB RID: 3755
		[Token(Token = "0x200117D")]
		private enum eOpenAnimType
		{
			// Token: 0x040057BD RID: 22461
			[Token(Token = "0x4006D5A")]
			Ae,
			// Token: 0x040057BE RID: 22462
			[Token(Token = "0x4006D5B")]
			PixelCrash
		}
	}
}
