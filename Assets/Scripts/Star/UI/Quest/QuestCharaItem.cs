﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EAD RID: 3757
	[Token(Token = "0x20009F5")]
	[StructLayout(3)]
	public class QuestCharaItem : ScrollItemIcon
	{
		// Token: 0x0600469C RID: 18076 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E8")]
		[Address(RVA = "0x10152125C", Offset = "0x152125C", VA = "0x10152125C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x0600469D RID: 18077 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E9")]
		[Address(RVA = "0x1015214C8", Offset = "0x15214C8", VA = "0x1015214C8")]
		public QuestCharaItem()
		{
		}

		// Token: 0x040057C3 RID: 22467
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DB0")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040057C4 RID: 22468
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DB1")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x040057C5 RID: 22469
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DB2")]
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x040057C6 RID: 22470
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003DB3")]
		[SerializeField]
		private GameObject m_NewIcon;

		// Token: 0x040057C7 RID: 22471
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003DB4")]
		[SerializeField]
		private Text m_LockText;
	}
}
