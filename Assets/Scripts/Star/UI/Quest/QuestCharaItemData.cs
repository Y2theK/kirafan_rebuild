﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000EAC RID: 3756
	[Token(Token = "0x20009F4")]
	[StructLayout(3)]
	public class QuestCharaItemData : ScrollItemData
	{
		// Token: 0x0600469A RID: 18074 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E6")]
		[Address(RVA = "0x1015214D0", Offset = "0x15214D0", VA = "0x1015214D0")]
		public QuestCharaItemData(int charaID, bool isNew, string lockText, Action<int> OnClick)
		{
		}

		// Token: 0x0600469B RID: 18075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040E7")]
		[Address(RVA = "0x101521520", Offset = "0x1521520", VA = "0x101521520", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040057BF RID: 22463
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003DAC")]
		public int m_CharaID;

		// Token: 0x040057C0 RID: 22464
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003DAD")]
		public bool m_IsNew;

		// Token: 0x040057C1 RID: 22465
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003DAE")]
		public string m_LockText;

		// Token: 0x040057C2 RID: 22466
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003DAF")]
		public Action<int> m_OnClick;
	}
}
