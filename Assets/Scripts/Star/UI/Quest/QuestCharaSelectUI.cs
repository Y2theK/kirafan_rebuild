﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EAE RID: 3758
	[Token(Token = "0x20009F6")]
	[StructLayout(3)]
	public class QuestCharaSelectUI : MenuUIBase
	{
		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x0600469E RID: 18078 RVA: 0x0001A070 File Offset: 0x00018270
		[Token(Token = "0x170004B8")]
		public QuestCharaSelectUI.eGroup CurrentGroup
		{
			[Token(Token = "0x60040EA")]
			[Address(RVA = "0x101521580", Offset = "0x1521580", VA = "0x101521580")]
			get
			{
				return QuestCharaSelectUI.eGroup.None;
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x0600469F RID: 18079 RVA: 0x0001A088 File Offset: 0x00018288
		[Token(Token = "0x170004B9")]
		public QuestCharaSelectUI.eButton SelectButton
		{
			[Token(Token = "0x60040EB")]
			[Address(RVA = "0x101521588", Offset = "0x1521588", VA = "0x101521588")]
			get
			{
				return QuestCharaSelectUI.eButton.Chara;
			}
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x060046A0 RID: 18080 RVA: 0x0001A0A0 File Offset: 0x000182A0
		[Token(Token = "0x170004BA")]
		public int SelectCharaID
		{
			[Token(Token = "0x60040EC")]
			[Address(RVA = "0x101521590", Offset = "0x1521590", VA = "0x101521590")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060046A1 RID: 18081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040ED")]
		[Address(RVA = "0x101521598", Offset = "0x1521598", VA = "0x101521598")]
		public void Setup()
		{
		}

		// Token: 0x060046A2 RID: 18082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040EE")]
		[Address(RVA = "0x101521900", Offset = "0x1521900", VA = "0x101521900")]
		public void OpenTitleList()
		{
		}

		// Token: 0x060046A3 RID: 18083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040EF")]
		[Address(RVA = "0x101521964", Offset = "0x1521964", VA = "0x101521964")]
		public void OpenCharaList(eTitleType titleType)
		{
		}

		// Token: 0x060046A4 RID: 18084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F0")]
		[Address(RVA = "0x101521F94", Offset = "0x1521F94", VA = "0x101521F94")]
		public void OpenCharaList(int charaID)
		{
		}

		// Token: 0x060046A5 RID: 18085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F1")]
		[Address(RVA = "0x101522274", Offset = "0x1522274", VA = "0x101522274")]
		private void Update()
		{
		}

		// Token: 0x060046A6 RID: 18086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F2")]
		[Address(RVA = "0x101522474", Offset = "0x1522474", VA = "0x101522474")]
		private void ChangeStep(QuestCharaSelectUI.eStep step)
		{
		}

		// Token: 0x060046A7 RID: 18087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F3")]
		[Address(RVA = "0x10152257C", Offset = "0x152257C", VA = "0x10152257C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060046A8 RID: 18088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F4")]
		[Address(RVA = "0x101522638", Offset = "0x1522638", VA = "0x101522638", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060046A9 RID: 18089 RVA: 0x0001A0B8 File Offset: 0x000182B8
		[Token(Token = "0x60040F5")]
		[Address(RVA = "0x10152271C", Offset = "0x152271C", VA = "0x10152271C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060046AA RID: 18090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F6")]
		[Address(RVA = "0x10152272C", Offset = "0x152272C", VA = "0x10152272C")]
		private void OnClickTitleCallBack(eTitleType title)
		{
		}

		// Token: 0x060046AB RID: 18091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F7")]
		[Address(RVA = "0x101522730", Offset = "0x1522730", VA = "0x101522730")]
		private void OnClickCharaCallBack(int charaID)
		{
		}

		// Token: 0x060046AC RID: 18092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F8")]
		[Address(RVA = "0x101522740", Offset = "0x1522740", VA = "0x101522740")]
		public QuestCharaSelectUI()
		{
		}

		// Token: 0x040057C8 RID: 22472
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003DB5")]
		private QuestCharaSelectUI.eStep m_Step;

		// Token: 0x040057C9 RID: 22473
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003DB6")]
		private QuestCharaSelectUI.eGroup m_CurrentGroup;

		// Token: 0x040057CA RID: 22474
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003DB7")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040057CB RID: 22475
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003DB8")]
		[SerializeField]
		private UIGroup m_TitleGroup;

		// Token: 0x040057CC RID: 22476
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DB9")]
		[SerializeField]
		private ScrollViewBase m_TitleScroll;

		// Token: 0x040057CD RID: 22477
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DBA")]
		[SerializeField]
		private UIGroup m_CharaGroup;

		// Token: 0x040057CE RID: 22478
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DBB")]
		[SerializeField]
		private ScrollViewBase m_CharaScroll;

		// Token: 0x040057CF RID: 22479
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003DBC")]
		private QuestCharaSelectUI.eButton m_SelectButton;

		// Token: 0x040057D0 RID: 22480
		[Cpp2IlInjected.FieldOffset(Offset = "0x74")]
		[Token(Token = "0x4003DBD")]
		private int m_SelectCharaID;

		// Token: 0x040057D1 RID: 22481
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003DBE")]
		public Action OnClickButton;

		// Token: 0x02000EAF RID: 3759
		[Token(Token = "0x200117E")]
		private enum eStep
		{
			// Token: 0x040057D3 RID: 22483
			[Token(Token = "0x4006D5D")]
			Hide,
			// Token: 0x040057D4 RID: 22484
			[Token(Token = "0x4006D5E")]
			In,
			// Token: 0x040057D5 RID: 22485
			[Token(Token = "0x4006D5F")]
			Idle,
			// Token: 0x040057D6 RID: 22486
			[Token(Token = "0x4006D60")]
			Out,
			// Token: 0x040057D7 RID: 22487
			[Token(Token = "0x4006D61")]
			End
		}

		// Token: 0x02000EB0 RID: 3760
		[Token(Token = "0x200117F")]
		public enum eGroup
		{
			// Token: 0x040057D9 RID: 22489
			[Token(Token = "0x4006D63")]
			None,
			// Token: 0x040057DA RID: 22490
			[Token(Token = "0x4006D64")]
			Title,
			// Token: 0x040057DB RID: 22491
			[Token(Token = "0x4006D65")]
			Chara
		}

		// Token: 0x02000EB1 RID: 3761
		[Token(Token = "0x2001180")]
		public enum eButton
		{
			// Token: 0x040057DD RID: 22493
			[Token(Token = "0x4006D67")]
			None = -1,
			// Token: 0x040057DE RID: 22494
			[Token(Token = "0x4006D68")]
			Chara
		}
	}
}
