﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EB3 RID: 3763
	[Token(Token = "0x20009F8")]
	[StructLayout(3)]
	public class QuestContentTitleItem : ScrollItemIcon
	{
		// Token: 0x060046AF RID: 18095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FB")]
		[Address(RVA = "0x101522750", Offset = "0x1522750", VA = "0x101522750", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060046B0 RID: 18096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FC")]
		[Address(RVA = "0x101522894", Offset = "0x1522894", VA = "0x101522894")]
		public QuestContentTitleItem()
		{
		}

		// Token: 0x040057E2 RID: 22498
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DC2")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040057E3 RID: 22499
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DC3")]
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x040057E4 RID: 22500
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DC4")]
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
