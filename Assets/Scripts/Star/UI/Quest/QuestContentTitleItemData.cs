﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000EB2 RID: 3762
	[Token(Token = "0x20009F7")]
	[StructLayout(3)]
	public class QuestContentTitleItemData : ScrollItemData
	{
		// Token: 0x060046AD RID: 18093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040F9")]
		[Address(RVA = "0x1015218BC", Offset = "0x15218BC", VA = "0x1015218BC")]
		public QuestContentTitleItemData(eTitleType titleType, bool isNew, Action<eTitleType> OnClick)
		{
		}

		// Token: 0x060046AE RID: 18094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FA")]
		[Address(RVA = "0x10152289C", Offset = "0x152289C", VA = "0x10152289C", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040057DF RID: 22495
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003DBF")]
		public eTitleType m_TitleType;

		// Token: 0x040057E0 RID: 22496
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003DC0")]
		public bool m_IsNew;

		// Token: 0x040057E1 RID: 22497
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003DC1")]
		public Action<eTitleType> m_OnClick;
	}
}
