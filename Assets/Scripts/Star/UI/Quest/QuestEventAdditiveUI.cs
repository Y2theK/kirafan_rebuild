﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EC4 RID: 3780
	[Token(Token = "0x2000A02")]
	[StructLayout(3)]
	public class QuestEventAdditiveUI : MenuUIBase
	{
		// Token: 0x060046E3 RID: 18147 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600412D")]
		[Address(RVA = "0x1015228FC", Offset = "0x15228FC", VA = "0x1015228FC")]
		public void Setup(EventQuestUISettingDB_Param? param)
		{
		}

		// Token: 0x060046E4 RID: 18148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600412E")]
		[Address(RVA = "0x101522D38", Offset = "0x1522D38", VA = "0x101522D38")]
		public void ClearChara()
		{
		}

		// Token: 0x060046E5 RID: 18149 RVA: 0x0001A1C0 File Offset: 0x000183C0
		[Token(Token = "0x600412F")]
		[Address(RVA = "0x101522F20", Offset = "0x1522F20", VA = "0x101522F20")]
		private bool IsExistChara()
		{
			return default(bool);
		}

		// Token: 0x060046E6 RID: 18150 RVA: 0x0001A1D8 File Offset: 0x000183D8
		[Token(Token = "0x6004130")]
		[Address(RVA = "0x101522F44", Offset = "0x1522F44", VA = "0x101522F44")]
		private bool IsIllustAnimEnd()
		{
			return default(bool);
		}

		// Token: 0x060046E7 RID: 18151 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004131")]
		[Address(RVA = "0x101522FC4", Offset = "0x1522FC4", VA = "0x101522FC4")]
		private void Update()
		{
		}

		// Token: 0x060046E8 RID: 18152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004132")]
		[Address(RVA = "0x101523328", Offset = "0x1523328", VA = "0x101523328", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060046E9 RID: 18153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004133")]
		[Address(RVA = "0x1015233C0", Offset = "0x15233C0", VA = "0x1015233C0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060046EA RID: 18154 RVA: 0x0001A1F0 File Offset: 0x000183F0
		[Token(Token = "0x6004134")]
		[Address(RVA = "0x101523428", Offset = "0x1523428", VA = "0x101523428", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060046EB RID: 18155 RVA: 0x0001A208 File Offset: 0x00018408
		[Token(Token = "0x6004135")]
		[Address(RVA = "0x101523438", Offset = "0x1523438", VA = "0x101523438")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x060046EC RID: 18156 RVA: 0x0001A220 File Offset: 0x00018420
		[Token(Token = "0x6004136")]
		[Address(RVA = "0x101523448", Offset = "0x1523448", VA = "0x101523448")]
		public int GetDisplayCharaID()
		{
			return 0;
		}

		// Token: 0x060046ED RID: 18157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004137")]
		[Address(RVA = "0x101523450", Offset = "0x1523450", VA = "0x101523450")]
		public void PlayVoice(int charaID, string voiceCueName)
		{
		}

		// Token: 0x060046EE RID: 18158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004138")]
		[Address(RVA = "0x1015230FC", Offset = "0x15230FC", VA = "0x1015230FC")]
		private void PlayReserveVoice()
		{
		}

		// Token: 0x060046EF RID: 18159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004139")]
		[Address(RVA = "0x101522DF8", Offset = "0x1522DF8", VA = "0x101522DF8")]
		public void StopVoice()
		{
		}

		// Token: 0x060046F0 RID: 18160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600413A")]
		[Address(RVA = "0x101523514", Offset = "0x1523514", VA = "0x101523514")]
		public QuestEventAdditiveUI()
		{
		}

		// Token: 0x04005847 RID: 22599
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E04")]
		private QuestEventAdditiveUI.eStep m_Step;

		// Token: 0x04005848 RID: 22600
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003E05")]
		[SerializeField]
		private AnimUIPlayer m_CharaAnim;

		// Token: 0x04005849 RID: 22601
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003E06")]
		[SerializeField]
		private AnimUIPlayer m_NpcAnim;

		// Token: 0x0400584A RID: 22602
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E07")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x0400584B RID: 22603
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003E08")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x0400584C RID: 22604
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E09")]
		private int m_CharaID;

		// Token: 0x0400584D RID: 22605
		[Cpp2IlInjected.FieldOffset(Offset = "0x6C")]
		[Token(Token = "0x4003E0A")]
		private NpcIllust.eNpc m_Npc;

		// Token: 0x0400584E RID: 22606
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E0B")]
		private QuestEventAdditiveUI.ReserveVoiceParam m_ReserveVoiveParam;

		// Token: 0x0400584F RID: 22607
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E0C")]
		private int m_VoiceReqID;

		// Token: 0x02000EC5 RID: 3781
		[Token(Token = "0x2001188")]
		public enum eStep
		{
			// Token: 0x04005851 RID: 22609
			[Token(Token = "0x4006D8D")]
			Hide,
			// Token: 0x04005852 RID: 22610
			[Token(Token = "0x4006D8E")]
			In,
			// Token: 0x04005853 RID: 22611
			[Token(Token = "0x4006D8F")]
			Idle,
			// Token: 0x04005854 RID: 22612
			[Token(Token = "0x4006D90")]
			Out
		}

		// Token: 0x02000EC6 RID: 3782
		[Token(Token = "0x2001189")]
		private class ReserveVoiceParam
		{
			// Token: 0x060046F1 RID: 18161 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006269")]
			[Address(RVA = "0x1015234D0", Offset = "0x15234D0", VA = "0x1015234D0")]
			public ReserveVoiceParam(int charaID, string voiceCueName)
			{
			}

			// Token: 0x060046F2 RID: 18162 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600626A")]
			[Address(RVA = "0x101522F10", Offset = "0x1522F10", VA = "0x101522F10")]
			public void Clear()
			{
			}

			// Token: 0x04005855 RID: 22613
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006D91")]
			public int m_CharaID;

			// Token: 0x04005856 RID: 22614
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006D92")]
			public string m_VoiceCueName;
		}
	}
}
