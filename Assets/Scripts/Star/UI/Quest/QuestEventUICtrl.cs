﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EC7 RID: 3783
	[Token(Token = "0x2000A03")]
	[StructLayout(3)]
	public class QuestEventUICtrl : MonoBehaviour
	{
		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x060046F3 RID: 18163 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004C1")]
		public WebViewGroup WebviewGroup
		{
			[Token(Token = "0x600413B")]
			[Address(RVA = "0x10152359C", Offset = "0x152359C", VA = "0x10152359C")]
			get
			{
				return null;
			}
		}

		// Token: 0x060046F4 RID: 18164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600413C")]
		[Address(RVA = "0x1015235A4", Offset = "0x15235A4", VA = "0x1015235A4")]
		public void Setup(Action OnClickChapterButton, Action OnClickShopButton, Action OnClickChestButton)
		{
		}

		// Token: 0x060046F5 RID: 18165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600413D")]
		[Address(RVA = "0x1015236E0", Offset = "0x15236E0", VA = "0x1015236E0")]
		public void ApplyEmpty()
		{
		}

		// Token: 0x060046F6 RID: 18166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600413E")]
		[Address(RVA = "0x101523E60", Offset = "0x1523E60", VA = "0x101523E60")]
		public void ApplyChapterUI(QuestManager.StaminaReduction staminaReduction)
		{
		}

		// Token: 0x060046F7 RID: 18167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600413F")]
		[Address(RVA = "0x101523E98", Offset = "0x1523E98", VA = "0x101523E98")]
		public void ApplyEventUI(QuestManager.EventGroupData groupData, bool useGroupURL, QuestManager.StaminaReduction staminaReduction)
		{
		}

		// Token: 0x060046F8 RID: 18168 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004140")]
		[Address(RVA = "0x101523718", Offset = "0x1523718", VA = "0x101523718")]
		private void Apply(bool isActiveChapter, bool isActiveWriterHelp, bool isActiveShop, bool isActiveChest, bool isActiveHelp, bool isActivePointEvent, QuestManager.StaminaReduction staminaReduction)
		{
		}

		// Token: 0x060046F9 RID: 18169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004141")]
		[Address(RVA = "0x10152433C", Offset = "0x152433C", VA = "0x10152433C")]
		public void OnClickChapterButtonCallBack()
		{
		}

		// Token: 0x060046FA RID: 18170 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004142")]
		[Address(RVA = "0x101524348", Offset = "0x1524348", VA = "0x101524348")]
		public void OnClickShopButtonCallBack()
		{
		}

		// Token: 0x060046FB RID: 18171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004143")]
		[Address(RVA = "0x101524354", Offset = "0x1524354", VA = "0x101524354")]
		public void OnClickChestButtonCallBack()
		{
		}

		// Token: 0x060046FC RID: 18172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004144")]
		[Address(RVA = "0x101524360", Offset = "0x1524360", VA = "0x101524360")]
		public void OnClickHelpButtonCallBack()
		{
		}

		// Token: 0x060046FD RID: 18173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004145")]
		[Address(RVA = "0x101524510", Offset = "0x1524510", VA = "0x101524510")]
		private void OnEndWebViewCallBack()
		{
		}

		// Token: 0x060046FE RID: 18174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004146")]
		[Address(RVA = "0x101524664", Offset = "0x1524664", VA = "0x101524664")]
		public void OnClickReceivePointRewardButtonCallBack()
		{
		}

		// Token: 0x060046FF RID: 18175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004147")]
		[Address(RVA = "0x101524780", Offset = "0x1524780", VA = "0x101524780")]
		private void OnResponseReceivePointRewardCallBack()
		{
		}

		// Token: 0x06004700 RID: 18176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004148")]
		[Address(RVA = "0x101524948", Offset = "0x1524948", VA = "0x101524948")]
		private void OnEndReceiveGroupCallBack()
		{
		}

		// Token: 0x06004701 RID: 18177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004149")]
		[Address(RVA = "0x101524A08", Offset = "0x1524A08", VA = "0x101524A08")]
		private void OnResponsePointEventGet()
		{
		}

		// Token: 0x06004702 RID: 18178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600414A")]
		[Address(RVA = "0x101524AB4", Offset = "0x1524AB4", VA = "0x101524AB4")]
		public QuestEventUICtrl()
		{
		}

		// Token: 0x04005857 RID: 22615
		[Token(Token = "0x4003E0D")]
		private const float LIST_SIZEDELTA_Y_WITH_UNDER_UI = 90f;

		// Token: 0x04005858 RID: 22616
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E0E")]
		[SerializeField]
		private WebViewGroup m_WebViewGroup;

		// Token: 0x04005859 RID: 22617
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E0F")]
		[SerializeField]
		private GameObject m_WebViewTitleEvent;

		// Token: 0x0400585A RID: 22618
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E10")]
		[SerializeField]
		private GameObject m_WebViewTitleQuest;

		// Token: 0x0400585B RID: 22619
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E11")]
		[SerializeField]
		private RewardSetDisplayGroup m_ReceiveGroup;

		// Token: 0x0400585C RID: 22620
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E12")]
		[SerializeField]
		private GameObject m_HorizontalLayoutObj;

		// Token: 0x0400585D RID: 22621
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E13")]
		[SerializeField]
		private CustomButton m_ShopButton;

		// Token: 0x0400585E RID: 22622
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003E14")]
		[SerializeField]
		private CustomButton m_ChestButton;

		// Token: 0x0400585F RID: 22623
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003E15")]
		[SerializeField]
		private CustomButton m_HelpButton;

		// Token: 0x04005860 RID: 22624
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E16")]
		[SerializeField]
		private CustomButton m_ChapterButton;

		// Token: 0x04005861 RID: 22625
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003E17")]
		[SerializeField]
		private CustomButton m_WriterHelpButton;

		// Token: 0x04005862 RID: 22626
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E18")]
		[SerializeField]
		private ChestBadge m_ChestBadge;

		// Token: 0x04005863 RID: 22627
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E19")]
		[SerializeField]
		private QuestPointEventDisplay m_PointEventDisplay;

		// Token: 0x04005864 RID: 22628
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E1A")]
		[SerializeField]
		private StaminaReductionIcon m_StaminaReducitonIcon;

		// Token: 0x04005865 RID: 22629
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003E1B")]
		[SerializeField]
		private RectTransform m_ListScrollRect;

		// Token: 0x04005866 RID: 22630
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003E1C")]
		private QuestManager.EventGroupData m_EventGroupData;

		// Token: 0x04005867 RID: 22631
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003E1D")]
		private bool m_IsUseGroupURL;

		// Token: 0x04005868 RID: 22632
		[Cpp2IlInjected.FieldOffset(Offset = "0x91")]
		[Token(Token = "0x4003E1E")]
		private bool m_ListScrollShifted;

		// Token: 0x04005869 RID: 22633
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003E1F")]
		private float m_ListScrollDefaultSizeDeltaY;

		// Token: 0x0400586A RID: 22634
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003E20")]
		private long m_ReceivePointEventID;

		// Token: 0x0400586B RID: 22635
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003E21")]
		public Action m_OnClickChapterButton;

		// Token: 0x0400586C RID: 22636
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003E22")]
		public Action m_OnClickShopButton;

		// Token: 0x0400586D RID: 22637
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003E23")]
		public Action m_OnClickChestButton;
	}
}
