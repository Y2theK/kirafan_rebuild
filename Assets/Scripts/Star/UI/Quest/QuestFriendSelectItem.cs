﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EC8 RID: 3784
	[Token(Token = "0x2000A04")]
	[StructLayout(3)]
	public class QuestFriendSelectItem : ScrollItemIcon
	{
		// Token: 0x06004703 RID: 18179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600414B")]
		[Address(RVA = "0x101524AC4", Offset = "0x1524AC4", VA = "0x101524AC4", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004704 RID: 18180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600414C")]
		[Address(RVA = "0x101525444", Offset = "0x1525444", VA = "0x101525444", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004705 RID: 18181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600414D")]
		[Address(RVA = "0x1015254C8", Offset = "0x15254C8", VA = "0x1015254C8")]
		public void OpenSupportCharaDetail()
		{
		}

		// Token: 0x06004706 RID: 18182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600414E")]
		[Address(RVA = "0x101525638", Offset = "0x1525638", VA = "0x101525638")]
		public QuestFriendSelectItem()
		{
		}

		// Token: 0x0400586E RID: 22638
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E24")]
		[SerializeField]
		private GameObject m_ValidObj;

		// Token: 0x0400586F RID: 22639
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003E25")]
		[SerializeField]
		private GameObject m_EmptyObj;

		// Token: 0x04005870 RID: 22640
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E26")]
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x04005871 RID: 22641
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E27")]
		[SerializeField]
		private GameObject m_BonusIcon;

		// Token: 0x04005872 RID: 22642
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E28")]
		[SerializeField]
		private Text m_UserNameText;

		// Token: 0x04005873 RID: 22643
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003E29")]
		[SerializeField]
		private Text m_TitleNameText;

		// Token: 0x04005874 RID: 22644
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003E2A")]
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x04005875 RID: 22645
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003E2B")]
		[SerializeField]
		private Text m_WeaponNameText;

		// Token: 0x04005876 RID: 22646
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003E2C")]
		[SerializeField]
		private Text m_WeaponLvText;

		// Token: 0x04005877 RID: 22647
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003E2D")]
		[SerializeField]
		private Text m_UserLvText;

		// Token: 0x04005878 RID: 22648
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003E2E")]
		[SerializeField]
		private SwapObj m_FriendSwapObj;

		// Token: 0x04005879 RID: 22649
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003E2F")]
		[SerializeField]
		private GameObject m_PCObj;

		// Token: 0x0400587A RID: 22650
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003E30")]
		[SerializeField]
		private GameObject m_NPCObj;

		// Token: 0x0400587B RID: 22651
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003E31")]
		[SerializeField]
		private AchievementImage m_ImgUserAchv;
	}
}
