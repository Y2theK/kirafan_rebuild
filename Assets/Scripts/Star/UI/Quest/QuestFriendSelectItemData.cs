﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000EC9 RID: 3785
	[Token(Token = "0x2000A05")]
	[StructLayout(3)]
	public class QuestFriendSelectItemData : ScrollItemData
	{
		// Token: 0x140000AA RID: 170
		// (add) Token: 0x06004707 RID: 18183 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004708 RID: 18184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AA")]
		public event Action<FriendManager.FriendData, long, UserSupportData> OnClick
		{
			[Token(Token = "0x600414F")]
			[Address(RVA = "0x101525640", Offset = "0x1525640", VA = "0x101525640")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004150")]
			[Address(RVA = "0x10152574C", Offset = "0x152574C", VA = "0x10152574C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004709 RID: 18185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004151")]
		[Address(RVA = "0x101525858", Offset = "0x1525858", VA = "0x101525858")]
		public QuestFriendSelectItemData(FriendManager.FriendData friendData, UserSupportData supportData, Action<FriendManager.FriendData, long, UserSupportData> onClick)
		{
		}

		// Token: 0x0600470A RID: 18186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004152")]
		[Address(RVA = "0x101525994", Offset = "0x1525994", VA = "0x101525994")]
		public QuestFriendSelectItemData(long npcID, bool isEnable, UserSupportData userSupportData, Action<FriendManager.FriendData, long, UserSupportData> onClick)
		{
		}

		// Token: 0x0600470B RID: 18187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004153")]
		[Address(RVA = "0x101525A7C", Offset = "0x1525A7C", VA = "0x101525A7C")]
		public QuestFriendSelectItemData(Action<FriendManager.FriendData, long, UserSupportData> onClick)
		{
		}

		// Token: 0x0600470C RID: 18188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004154")]
		[Address(RVA = "0x101525AE4", Offset = "0x1525AE4", VA = "0x101525AE4", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x0400587C RID: 22652
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E32")]
		public string m_UserName;

		// Token: 0x0400587D RID: 22653
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E33")]
		public int m_UserLv;

		// Token: 0x0400587E RID: 22654
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003E34")]
		public int m_CharaID;

		// Token: 0x0400587F RID: 22655
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E35")]
		public int m_WeaponID;

		// Token: 0x04005880 RID: 22656
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003E36")]
		public int m_WeaponLv;

		// Token: 0x04005881 RID: 22657
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E37")]
		public DateTime m_LogInTime;

		// Token: 0x04005882 RID: 22658
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E38")]
		public QuestFriendSelectItemData.eFriendType m_FriendType;

		// Token: 0x04005883 RID: 22659
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E39")]
		public FriendManager.FriendData m_FriendData;

		// Token: 0x04005884 RID: 22660
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003E3A")]
		public UserSupportData m_SupportData;

		// Token: 0x04005885 RID: 22661
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003E3B")]
		public long m_NpcID;

		// Token: 0x04005886 RID: 22662
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E3C")]
		public int m_AchivementID;

		// Token: 0x04005887 RID: 22663
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003E3D")]
		public bool m_IsEnable;

		// Token: 0x04005888 RID: 22664
		[Cpp2IlInjected.FieldOffset(Offset = "0x5D")]
		[Token(Token = "0x4003E3E")]
		public bool m_IsEmpty;

		// Token: 0x02000ECA RID: 3786
		[Token(Token = "0x200118A")]
		public enum eFriendType
		{
			// Token: 0x0400588B RID: 22667
			[Token(Token = "0x4006D94")]
			Friend,
			// Token: 0x0400588C RID: 22668
			[Token(Token = "0x4006D95")]
			Guest,
			// Token: 0x0400588D RID: 22669
			[Token(Token = "0x4006D96")]
			Npc
		}
	}
}
