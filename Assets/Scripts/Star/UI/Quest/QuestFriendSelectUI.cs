﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000ECB RID: 3787
	[Token(Token = "0x2000A06")]
	[StructLayout(3)]
	public class QuestFriendSelectUI : MenuUIBase
	{
		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x0600470D RID: 18189 RVA: 0x0001A238 File Offset: 0x00018438
		[Token(Token = "0x170004C2")]
		public QuestFriendSelectUI.eButton SelectButton
		{
			[Token(Token = "0x6004155")]
			[Address(RVA = "0x101525B48", Offset = "0x1525B48", VA = "0x101525B48")]
			get
			{
				return QuestFriendSelectUI.eButton.Back;
			}
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x0600470E RID: 18190 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004C3")]
		public FriendManager.FriendData SelectFriendData
		{
			[Token(Token = "0x6004156")]
			[Address(RVA = "0x101525B50", Offset = "0x1525B50", VA = "0x101525B50")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x0600470F RID: 18191 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004C4")]
		public UserSupportData SelectSupport
		{
			[Token(Token = "0x6004157")]
			[Address(RVA = "0x101525B58", Offset = "0x1525B58", VA = "0x101525B58")]
			get
			{
				return null;
			}
		}

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06004710 RID: 18192 RVA: 0x0001A250 File Offset: 0x00018450
		[Token(Token = "0x170004C5")]
		public long NpcID
		{
			[Token(Token = "0x6004158")]
			[Address(RVA = "0x101525B60", Offset = "0x1525B60", VA = "0x101525B60")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x140000AB RID: 171
		// (add) Token: 0x06004711 RID: 18193 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004712 RID: 18194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AB")]
		public event Action OnClickButton
		{
			[Token(Token = "0x6004159")]
			[Address(RVA = "0x101525B68", Offset = "0x1525B68", VA = "0x101525B68")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600415A")]
			[Address(RVA = "0x101525C74", Offset = "0x1525C74", VA = "0x101525C74")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000AC RID: 172
		// (add) Token: 0x06004713 RID: 18195 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004714 RID: 18196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AC")]
		public event Action OnClickReloadButton
		{
			[Token(Token = "0x600415B")]
			[Address(RVA = "0x101525D80", Offset = "0x1525D80", VA = "0x101525D80")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600415C")]
			[Address(RVA = "0x101525E8C", Offset = "0x1525E8C", VA = "0x101525E8C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004715 RID: 18197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600415D")]
		[Address(RVA = "0x101525F98", Offset = "0x1525F98", VA = "0x101525F98")]
		public void Setup()
		{
		}

		// Token: 0x06004716 RID: 18198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600415E")]
		[Address(RVA = "0x101526EF0", Offset = "0x1526EF0", VA = "0x101526EF0")]
		public void Refresh()
		{
		}

		// Token: 0x06004717 RID: 18199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600415F")]
		[Address(RVA = "0x1015263C4", Offset = "0x15263C4", VA = "0x1015263C4")]
		private void Apply()
		{
		}

		// Token: 0x06004718 RID: 18200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004160")]
		[Address(RVA = "0x101526EFC", Offset = "0x1526EFC", VA = "0x101526EFC")]
		private void Update()
		{
		}

		// Token: 0x06004719 RID: 18201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004161")]
		[Address(RVA = "0x1015270F8", Offset = "0x15270F8", VA = "0x1015270F8")]
		private void ChangeStep(QuestFriendSelectUI.eStep step)
		{
		}

		// Token: 0x0600471A RID: 18202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004162")]
		[Address(RVA = "0x101527294", Offset = "0x1527294", VA = "0x101527294", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600471B RID: 18203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004163")]
		[Address(RVA = "0x101527334", Offset = "0x1527334", VA = "0x101527334", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600471C RID: 18204 RVA: 0x0001A268 File Offset: 0x00018468
		[Token(Token = "0x6004164")]
		[Address(RVA = "0x101527528", Offset = "0x1527528", VA = "0x101527528", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600471D RID: 18205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004165")]
		[Address(RVA = "0x101527538", Offset = "0x1527538", VA = "0x101527538")]
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x0600471E RID: 18206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004166")]
		[Address(RVA = "0x101527548", Offset = "0x1527548", VA = "0x101527548")]
		public void OnClickFriendButtonCallBack(FriendManager.FriendData friendData, long npcID, UserSupportData support)
		{
		}

		// Token: 0x0600471F RID: 18207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004167")]
		[Address(RVA = "0x101527564", Offset = "0x1527564", VA = "0x101527564")]
		public void OnClickReloadButtonCallBack()
		{
		}

		// Token: 0x06004720 RID: 18208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004168")]
		[Address(RVA = "0x101527664", Offset = "0x1527664", VA = "0x101527664")]
		public void OpenFilterWindow()
		{
		}

		// Token: 0x06004721 RID: 18209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004169")]
		[Address(RVA = "0x10152774C", Offset = "0x152774C", VA = "0x10152774C")]
		public void OnExecuteFilter()
		{
		}

		// Token: 0x06004722 RID: 18210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600416A")]
		[Address(RVA = "0x101527750", Offset = "0x1527750", VA = "0x101527750")]
		public void OpenSortWindow()
		{
		}

		// Token: 0x06004723 RID: 18211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600416B")]
		[Address(RVA = "0x101527838", Offset = "0x1527838", VA = "0x101527838")]
		public void OnExecuteSort()
		{
		}

		// Token: 0x06004724 RID: 18212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600416C")]
		[Address(RVA = "0x10152783C", Offset = "0x152783C", VA = "0x10152783C")]
		public void OnClickEventFilterButtonCallBack()
		{
		}

		// Token: 0x06004725 RID: 18213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600416D")]
		[Address(RVA = "0x1015278B0", Offset = "0x15278B0", VA = "0x1015278B0")]
		public QuestFriendSelectUI()
		{
		}

		// Token: 0x0400588E RID: 22670
		[Token(Token = "0x4003E40")]
		private const float RELOAD_WAIT = 15f;

		// Token: 0x0400588F RID: 22671
		[Token(Token = "0x4003E41")]
		private const float NOSELECTBUTTON_CPU_OFFSET = -158f;

		// Token: 0x04005890 RID: 22672
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E42")]
		private QuestFriendSelectUI.eStep m_Step;

		// Token: 0x04005891 RID: 22673
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003E43")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005892 RID: 22674
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003E44")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005893 RID: 22675
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E45")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005894 RID: 22676
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003E46")]
		[SerializeField]
		private CustomButton m_EventFilterButton;

		// Token: 0x04005895 RID: 22677
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E47")]
		[SerializeField]
		private GameObject m_NpcOnlyObj;

		// Token: 0x04005896 RID: 22678
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E48")]
		[SerializeField]
		private CustomButton m_ReloadButton;

		// Token: 0x04005897 RID: 22679
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E49")]
		[SerializeField]
		private CustomButton m_FilterButton;

		// Token: 0x04005898 RID: 22680
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003E4A")]
		[SerializeField]
		private CustomButton m_SortButton;

		// Token: 0x04005899 RID: 22681
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003E4B")]
		private QuestFriendSelectUI.eButton m_SelectButton;

		// Token: 0x0400589A RID: 22682
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003E4C")]
		private FriendManager.FriendData m_SelectFriendData;

		// Token: 0x0400589B RID: 22683
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003E4D")]
		private UserSupportData m_SelectSupport;

		// Token: 0x0400589C RID: 22684
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003E4E")]
		private long m_NpcID;

		// Token: 0x0400589D RID: 22685
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003E4F")]
		private float m_ReloadWaitTime;

		// Token: 0x0400589E RID: 22686
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4003E50")]
		private bool m_IsNpcOnly;

		// Token: 0x02000ECC RID: 3788
		[Token(Token = "0x200118B")]
		private enum eStep
		{
			// Token: 0x040058A2 RID: 22690
			[Token(Token = "0x4006D98")]
			Hide,
			// Token: 0x040058A3 RID: 22691
			[Token(Token = "0x4006D99")]
			In,
			// Token: 0x040058A4 RID: 22692
			[Token(Token = "0x4006D9A")]
			Idle,
			// Token: 0x040058A5 RID: 22693
			[Token(Token = "0x4006D9B")]
			Out,
			// Token: 0x040058A6 RID: 22694
			[Token(Token = "0x4006D9C")]
			End
		}

		// Token: 0x02000ECD RID: 3789
		[Token(Token = "0x200118C")]
		public enum eButton
		{
			// Token: 0x040058A8 RID: 22696
			[Token(Token = "0x4006D9E")]
			None = -1,
			// Token: 0x040058A9 RID: 22697
			[Token(Token = "0x4006D9F")]
			Back,
			// Token: 0x040058AA RID: 22698
			[Token(Token = "0x4006DA0")]
			Friend
		}
	}
}
