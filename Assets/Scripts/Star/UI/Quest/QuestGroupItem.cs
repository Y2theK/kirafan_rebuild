﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EB4 RID: 3764
	[Token(Token = "0x20009F9")]
	[StructLayout(3)]
	public class QuestGroupItem : ScrollItemIcon
	{
		// Token: 0x060046B1 RID: 18097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FD")]
		[Address(RVA = "0x1015278D0", Offset = "0x15278D0", VA = "0x1015278D0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060046B2 RID: 18098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FE")]
		[Address(RVA = "0x1015286A8", Offset = "0x15286A8", VA = "0x1015286A8")]
		public QuestGroupItem()
		{
		}

		// Token: 0x040057E5 RID: 22501
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DC5")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040057E6 RID: 22502
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DC6")]
		[SerializeField]
		private QuestGroupIcon m_QuestGroupIcon;

		// Token: 0x040057E7 RID: 22503
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DC7")]
		[SerializeField]
		private Image m_ClearIcon;

		// Token: 0x040057E8 RID: 22504
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003DC8")]
		[SerializeField]
		private Sprite m_ClearIconSprite;

		// Token: 0x040057E9 RID: 22505
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003DC9")]
		[SerializeField]
		private Sprite m_ClearIconCompleteSprite;

		// Token: 0x040057EA RID: 22506
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003DCA")]
		[SerializeField]
		private GameObject m_SpanObj;

		// Token: 0x040057EB RID: 22507
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003DCB")]
		[SerializeField]
		private Text m_SpanText;

		// Token: 0x040057EC RID: 22508
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003DCC")]
		[SerializeField]
		private GameObject m_StaminaReductionObj;

		// Token: 0x040057ED RID: 22509
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003DCD")]
		[SerializeField]
		private GameObject m_StartTimeObj;

		// Token: 0x040057EE RID: 22510
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003DCE")]
		[SerializeField]
		private Text m_StartTimeText;

		// Token: 0x040057EF RID: 22511
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003DCF")]
		[SerializeField]
		private GameObject m_LockObj;

		// Token: 0x040057F0 RID: 22512
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003DD0")]
		[SerializeField]
		private Text m_LockText;

		// Token: 0x040057F1 RID: 22513
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003DD1")]
		[SerializeField]
		private GameObject m_NewIconObj;

		// Token: 0x02000EB5 RID: 3765
		[Token(Token = "0x2001181")]
		public enum eState
		{
			// Token: 0x040057F3 RID: 22515
			[Token(Token = "0x4006D6A")]
			Available,
			// Token: 0x040057F4 RID: 22516
			[Token(Token = "0x4006D6B")]
			Locked,
			// Token: 0x040057F5 RID: 22517
			[Token(Token = "0x4006D6C")]
			Finished
		}
	}
}
