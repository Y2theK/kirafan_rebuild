﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000EB6 RID: 3766
	[Token(Token = "0x20009FA")]
	[StructLayout(3)]
	public class QuestGroupItemData : ScrollItemData
	{
		// Token: 0x060046B3 RID: 18099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60040FF")]
		[Address(RVA = "0x1015286B0", Offset = "0x15286B0", VA = "0x1015286B0")]
		public QuestGroupItemData(int idx, int groupID, QuestGroupItem.eState state, QuestManager.StaminaReduction reduction, QuestManager.EventQuestPeriodGroup period)
		{
		}

		// Token: 0x060046B4 RID: 18100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004100")]
		[Address(RVA = "0x101528704", Offset = "0x1528704", VA = "0x101528704", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040057F6 RID: 22518
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003DD2")]
		public int m_Idx;

		// Token: 0x040057F7 RID: 22519
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003DD3")]
		public int m_GroupID;

		// Token: 0x040057F8 RID: 22520
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003DD4")]
		public QuestGroupItem.eState m_State;

		// Token: 0x040057F9 RID: 22521
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003DD5")]
		public QuestManager.StaminaReduction m_StaminaReduction;

		// Token: 0x040057FA RID: 22522
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003DD6")]
		public QuestManager.EventQuestPeriodGroup m_Period;

		// Token: 0x040057FB RID: 22523
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003DD7")]
		public Action<int> OnClick;

		// Token: 0x040057FC RID: 22524
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003DD8")]
		public Action<int> OnClickDropItem;
	}
}
