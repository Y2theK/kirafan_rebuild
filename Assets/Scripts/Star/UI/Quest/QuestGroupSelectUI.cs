﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EB7 RID: 3767
	[Token(Token = "0x20009FB")]
	[StructLayout(3)]
	public class QuestGroupSelectUI : MenuUIBase
	{
		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x060046B5 RID: 18101 RVA: 0x0001A0D0 File Offset: 0x000182D0
		[Token(Token = "0x170004BB")]
		public QuestGroupSelectUI.eButton SelectButton
		{
			[Token(Token = "0x6004101")]
			[Address(RVA = "0x101528764", Offset = "0x1528764", VA = "0x101528764")]
			get
			{
				return QuestGroupSelectUI.eButton.Group;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x060046B6 RID: 18102 RVA: 0x0001A0E8 File Offset: 0x000182E8
		[Token(Token = "0x170004BC")]
		public int SelectGroupID
		{
			[Token(Token = "0x6004102")]
			[Address(RVA = "0x10152876C", Offset = "0x152876C", VA = "0x10152876C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x060046B7 RID: 18103 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004BD")]
		public List<QuestManager.EventGroupData> EventGroupDataList
		{
			[Token(Token = "0x6004103")]
			[Address(RVA = "0x101528774", Offset = "0x1528774", VA = "0x101528774")]
			get
			{
				return null;
			}
		}

		// Token: 0x060046B8 RID: 18104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004104")]
		[Address(RVA = "0x10152877C", Offset = "0x152877C", VA = "0x10152877C")]
		public void Setup(int eventType)
		{
		}

		// Token: 0x060046B9 RID: 18105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004105")]
		[Address(RVA = "0x10152927C", Offset = "0x152927C", VA = "0x10152927C")]
		private void Update()
		{
		}

		// Token: 0x060046BA RID: 18106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004106")]
		[Address(RVA = "0x10152944C", Offset = "0x152944C", VA = "0x10152944C")]
		private void ChangeStep(QuestGroupSelectUI.eStep step)
		{
		}

		// Token: 0x060046BB RID: 18107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004107")]
		[Address(RVA = "0x1015295E8", Offset = "0x15295E8", VA = "0x1015295E8", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060046BC RID: 18108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004108")]
		[Address(RVA = "0x101529700", Offset = "0x1529700", VA = "0x101529700", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060046BD RID: 18109 RVA: 0x0001A100 File Offset: 0x00018300
		[Token(Token = "0x6004109")]
		[Address(RVA = "0x1015297D8", Offset = "0x15297D8", VA = "0x1015297D8", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060046BE RID: 18110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600410A")]
		[Address(RVA = "0x1015297E8", Offset = "0x15297E8", VA = "0x1015297E8")]
		public void OnClickGroupButtonCallBack(int groupID)
		{
		}

		// Token: 0x060046BF RID: 18111 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600410B")]
		[Address(RVA = "0x1015297F8", Offset = "0x15297F8", VA = "0x1015297F8")]
		private void OnClickShopButtonCallBack()
		{
		}

		// Token: 0x060046C0 RID: 18112 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600410C")]
		[Address(RVA = "0x10152980C", Offset = "0x152980C", VA = "0x10152980C")]
		private void OnClickChestButtonCallBack()
		{
		}

		// Token: 0x060046C1 RID: 18113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600410D")]
		[Address(RVA = "0x101529820", Offset = "0x1529820", VA = "0x101529820")]
		public void OpenURL()
		{
		}

		// Token: 0x060046C2 RID: 18114 RVA: 0x0001A118 File Offset: 0x00018318
		[Token(Token = "0x600410E")]
		[Address(RVA = "0x10152984C", Offset = "0x152984C", VA = "0x10152984C")]
		public bool IsReadyPlayVoice()
		{
			return default(bool);
		}

		// Token: 0x060046C3 RID: 18115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600410F")]
		[Address(RVA = "0x1015298AC", Offset = "0x15298AC", VA = "0x1015298AC")]
		public QuestGroupSelectUI()
		{
		}

		// Token: 0x040057FD RID: 22525
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003DD9")]
		private QuestGroupSelectUI.eStep m_Step;

		// Token: 0x040057FE RID: 22526
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003DDA")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040057FF RID: 22527
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003DDB")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005800 RID: 22528
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DDC")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005801 RID: 22529
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DDD")]
		[SerializeField]
		private GameObject m_LossTimeObj;

		// Token: 0x04005802 RID: 22530
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DDE")]
		[SerializeField]
		private AnimUIPlayer m_EventNameAnim;

		// Token: 0x04005803 RID: 22531
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003DDF")]
		[SerializeField]
		private Text m_EventNameText;

		// Token: 0x04005804 RID: 22532
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003DE0")]
		[SerializeField]
		private QuestEventUICtrl m_EventUICtrl;

		// Token: 0x04005805 RID: 22533
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003DE1")]
		private QuestGroupSelectUI.eButton m_SelectButton;

		// Token: 0x04005806 RID: 22534
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4003DE2")]
		private int m_SelectGroupID;

		// Token: 0x04005807 RID: 22535
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003DE3")]
		private List<QuestManager.EventGroupData> m_EventGroupDataList;

		// Token: 0x04005808 RID: 22536
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003DE4")]
		public Action OnClickButton;

		// Token: 0x02000EB8 RID: 3768
		[Token(Token = "0x2001182")]
		private enum eStep
		{
			// Token: 0x0400580A RID: 22538
			[Token(Token = "0x4006D6E")]
			Hide,
			// Token: 0x0400580B RID: 22539
			[Token(Token = "0x4006D6F")]
			In,
			// Token: 0x0400580C RID: 22540
			[Token(Token = "0x4006D70")]
			Idle,
			// Token: 0x0400580D RID: 22541
			[Token(Token = "0x4006D71")]
			Out,
			// Token: 0x0400580E RID: 22542
			[Token(Token = "0x4006D72")]
			End
		}

		// Token: 0x02000EB9 RID: 3769
		[Token(Token = "0x2001183")]
		public enum eButton
		{
			// Token: 0x04005810 RID: 22544
			[Token(Token = "0x4006D74")]
			None = -1,
			// Token: 0x04005811 RID: 22545
			[Token(Token = "0x4006D75")]
			Group,
			// Token: 0x04005812 RID: 22546
			[Token(Token = "0x4006D76")]
			Shop,
			// Token: 0x04005813 RID: 22547
			[Token(Token = "0x4006D77")]
			Chest
		}
	}
}
