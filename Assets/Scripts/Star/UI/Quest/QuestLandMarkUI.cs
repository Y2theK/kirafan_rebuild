﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000ECF RID: 3791
	[Token(Token = "0x2000A07")]
	[StructLayout(3)]
	public class QuestLandMarkUI : MonoBehaviour
	{
		// Token: 0x06004728 RID: 18216 RVA: 0x0001A298 File Offset: 0x00018498
		[Token(Token = "0x600416E")]
		[Address(RVA = "0x1015298BC", Offset = "0x15298BC", VA = "0x1015298BC")]
		public bool IsPlayIn()
		{
			return default(bool);
		}

		// Token: 0x06004729 RID: 18217 RVA: 0x0001A2B0 File Offset: 0x000184B0
		[Token(Token = "0x600416F")]
		[Address(RVA = "0x1015298F8", Offset = "0x15298F8", VA = "0x1015298F8")]
		public bool IsPlayOut()
		{
			return default(bool);
		}

		// Token: 0x0600472A RID: 18218 RVA: 0x0001A2C8 File Offset: 0x000184C8
		[Token(Token = "0x6004170")]
		[Address(RVA = "0x101529934", Offset = "0x1529934", VA = "0x101529934")]
		public bool IsAnim()
		{
			return default(bool);
		}

		// Token: 0x0600472B RID: 18219 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004171")]
		[Address(RVA = "0x10152996C", Offset = "0x152996C", VA = "0x10152996C")]
		public void PlayIn()
		{
		}

		// Token: 0x0600472C RID: 18220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004172")]
		[Address(RVA = "0x101529A48", Offset = "0x1529A48", VA = "0x101529A48")]
		public void Show()
		{
		}

		// Token: 0x0600472D RID: 18221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004173")]
		[Address(RVA = "0x101529AEC", Offset = "0x1529AEC", VA = "0x101529AEC")]
		public void PlayOut()
		{
		}

		// Token: 0x0600472E RID: 18222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004174")]
		[Address(RVA = "0x101529B90", Offset = "0x1529B90", VA = "0x101529B90")]
		public void Hide()
		{
		}

		// Token: 0x0600472F RID: 18223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004175")]
		[Address(RVA = "0x1015299E8", Offset = "0x15299E8", VA = "0x1015299E8")]
		public void PlayInNewIcon()
		{
		}

		// Token: 0x06004730 RID: 18224 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004176")]
		[Address(RVA = "0x101529B30", Offset = "0x1529B30", VA = "0x101529B30")]
		public void PlayOutNewIcon()
		{
		}

		// Token: 0x06004731 RID: 18225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004177")]
		[Address(RVA = "0x101529A18", Offset = "0x1529A18", VA = "0x101529A18")]
		public void PlayInCurrentIcon()
		{
		}

		// Token: 0x06004732 RID: 18226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004178")]
		[Address(RVA = "0x101529B60", Offset = "0x1529B60", VA = "0x101529B60")]
		public void PlayOutCurrentIcon()
		{
		}

		// Token: 0x06004733 RID: 18227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004179")]
		[Address(RVA = "0x101529BFC", Offset = "0x1529BFC", VA = "0x101529BFC")]
		public void SetBadgeValue(int value)
		{
		}

		// Token: 0x06004734 RID: 18228 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417A")]
		[Address(RVA = "0x101529C84", Offset = "0x1529C84", VA = "0x101529C84")]
		public void SetNameBarText(string name)
		{
		}

		// Token: 0x06004735 RID: 18229 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417B")]
		[Address(RVA = "0x101529CC4", Offset = "0x1529CC4", VA = "0x101529CC4")]
		public void SetIsNew(bool isNew)
		{
		}

		// Token: 0x06004736 RID: 18230 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417C")]
		[Address(RVA = "0x101529D08", Offset = "0x1529D08", VA = "0x101529D08")]
		public void SetIsCurrent(bool isCurrent)
		{
		}

		// Token: 0x06004737 RID: 18231 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417D")]
		[Address(RVA = "0x101529D4C", Offset = "0x1529D4C", VA = "0x101529D4C")]
		public void SetIsADVOnly(bool isADVOnly)
		{
		}

		// Token: 0x06004738 RID: 18232 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417E")]
		[Address(RVA = "0x101529D54", Offset = "0x1529D54", VA = "0x101529D54")]
		public void SetNameBarLocalPositionOffset(Vector3 offset)
		{
		}

		// Token: 0x06004739 RID: 18233 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600417F")]
		[Address(RVA = "0x101529E74", Offset = "0x1529E74", VA = "0x101529E74")]
		public QuestLandMarkUI()
		{
		}

		// Token: 0x040058AC RID: 22700
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E53")]
		[SerializeField]
		private AnimUIPlayer m_NewIconAnim;

		// Token: 0x040058AD RID: 22701
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E54")]
		[SerializeField]
		private AnimUIPlayer m_CurrentIconAnim;

		// Token: 0x040058AE RID: 22702
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E55")]
		[SerializeField]
		private PrefabCloner m_BadgeCloner;

		// Token: 0x040058AF RID: 22703
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E56")]
		[SerializeField]
		private GameObject m_ADVOnlyIconObj;

		// Token: 0x040058B0 RID: 22704
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E57")]
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x040058B1 RID: 22705
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E58")]
		[SerializeField]
		private Text m_NameBarText;

		// Token: 0x040058B2 RID: 22706
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003E59")]
		[SerializeField]
		private Transform m_NameBarTransform;

		// Token: 0x040058B3 RID: 22707
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003E5A")]
		private Vector3 m_NameBarDefaultLocalPos;

		// Token: 0x040058B4 RID: 22708
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003E5B")]
		private bool m_InitializedNameBarDefaultLocalPos;

		// Token: 0x040058B5 RID: 22709
		[Cpp2IlInjected.FieldOffset(Offset = "0x5D")]
		[Token(Token = "0x4003E5C")]
		private bool m_IsNew;

		// Token: 0x040058B6 RID: 22710
		[Cpp2IlInjected.FieldOffset(Offset = "0x5E")]
		[Token(Token = "0x4003E5D")]
		private bool m_IsCurrent;

		// Token: 0x040058B7 RID: 22711
		[Cpp2IlInjected.FieldOffset(Offset = "0x5F")]
		[Token(Token = "0x4003E5E")]
		private bool m_IsADVOnly;
	}
}
