﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000ED3 RID: 3795
	[Token(Token = "0x2000A0B")]
	[StructLayout(3)]
	public class QuestLimit : MonoBehaviour
	{
		// Token: 0x06004748 RID: 18248 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600418E")]
		[Address(RVA = "0x101529EEC", Offset = "0x1529EEC", VA = "0x101529EEC")]
		public void SetupClassLimit(eClassType limitClass)
		{
		}

		// Token: 0x06004749 RID: 18249 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600418F")]
		[Address(RVA = "0x101529F24", Offset = "0x1529F24", VA = "0x101529F24")]
		public void SetupElementLimit(eElementType elementType)
		{
		}

		// Token: 0x0600474A RID: 18250 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004190")]
		[Address(RVA = "0x101529F5C", Offset = "0x1529F5C", VA = "0x101529F5C")]
		public void SetupNamedLimit(eCharaNamedType named)
		{
		}

		// Token: 0x0600474B RID: 18251 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004191")]
		[Address(RVA = "0x10152A02C", Offset = "0x152A02C", VA = "0x10152A02C")]
		public void SetupRareLimit(eRare rare)
		{
		}

		// Token: 0x0600474C RID: 18252 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004192")]
		[Address(RVA = "0x10152A1C0", Offset = "0x152A1C0", VA = "0x10152A1C0")]
		public void SetupCostLimit(int cost)
		{
		}

		// Token: 0x0600474D RID: 18253 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004193")]
		[Address(RVA = "0x10152A32C", Offset = "0x152A32C", VA = "0x10152A32C")]
		public void SetupTitleLimit(eTitleType title)
		{
		}

		// Token: 0x0600474E RID: 18254 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004194")]
		[Address(RVA = "0x10152A3FC", Offset = "0x152A3FC", VA = "0x10152A3FC")]
		public QuestLimit()
		{
		}

		// Token: 0x040058C9 RID: 22729
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E70")]
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x040058CA RID: 22730
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E71")]
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x040058CB RID: 22731
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E72")]
		[SerializeField]
		private Text m_NamedText;

		// Token: 0x040058CC RID: 22732
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E73")]
		[SerializeField]
		private RareStar m_Rare;

		// Token: 0x040058CD RID: 22733
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E74")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x040058CE RID: 22734
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E75")]
		[SerializeField]
		private Text m_TitleText;
	}
}
