﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000ED4 RID: 3796
	[Token(Token = "0x2000A0C")]
	[StructLayout(3)]
	public class QuestListItem : ScrollItemIcon
	{
		// Token: 0x0600474F RID: 18255 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004195")]
		[Address(RVA = "0x10152A404", Offset = "0x152A404", VA = "0x10152A404", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004750 RID: 18256 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004196")]
		[Address(RVA = "0x10152AFE8", Offset = "0x152AFE8", VA = "0x10152AFE8")]
		private void UpdateStaminaText()
		{
		}

		// Token: 0x06004751 RID: 18257 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004197")]
		[Address(RVA = "0x10152C1F8", Offset = "0x152C1F8", VA = "0x10152C1F8")]
		private void Update()
		{
		}

		// Token: 0x06004752 RID: 18258 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004198")]
		[Address(RVA = "0x10152B3C8", Offset = "0x152B3C8", VA = "0x10152B3C8")]
		private void ApplyLimit()
		{
		}

		// Token: 0x06004753 RID: 18259 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004199")]
		[Address(RVA = "0x10152C2D0", Offset = "0x152C2D0", VA = "0x10152C2D0")]
		public GameObject ScoopSeparate(RectTransform parent)
		{
			return null;
		}

		// Token: 0x06004754 RID: 18260 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600419A")]
		[Address(RVA = "0x10152C4FC", Offset = "0x152C4FC", VA = "0x10152C4FC")]
		public void OnClickDropItemButtonCallBack()
		{
		}

		// Token: 0x06004755 RID: 18261 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600419B")]
		[Address(RVA = "0x10152C5F8", Offset = "0x152C5F8", VA = "0x10152C5F8")]
		public QuestListItem()
		{
		}

		// Token: 0x040058CF RID: 22735
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003E76")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040058D0 RID: 22736
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003E77")]
		[SerializeField]
		private Image m_BaseImage;

		// Token: 0x040058D1 RID: 22737
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E78")]
		[SerializeField]
		private Sprite[] m_BaseSprite;

		// Token: 0x040058D2 RID: 22738
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E79")]
		[SerializeField]
		private ImageNumbers m_Number;

		// Token: 0x040058D3 RID: 22739
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E7A")]
		[SerializeField]
		private QuestTypeIcon m_TypeIcon;

		// Token: 0x040058D4 RID: 22740
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003E7B")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x040058D5 RID: 22741
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003E7C")]
		[SerializeField]
		private GameObject m_QuestObj;

		// Token: 0x040058D6 RID: 22742
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003E7D")]
		[SerializeField]
		private GameObject m_ADVOnly;

		// Token: 0x040058D7 RID: 22743
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003E7E")]
		[SerializeField]
		private GameObject m_CostStamina;

		// Token: 0x040058D8 RID: 22744
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003E7F")]
		[SerializeField]
		private GameObject m_CostItem;

		// Token: 0x040058D9 RID: 22745
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003E80")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x040058DA RID: 22746
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003E81")]
		[SerializeField]
		private Text m_CostValue;

		// Token: 0x040058DB RID: 22747
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003E82")]
		[SerializeField]
		private GameObject m_ElementObj;

		// Token: 0x040058DC RID: 22748
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003E83")]
		[SerializeField]
		private GameObject[] m_ElementIcons;

		// Token: 0x040058DD RID: 22749
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003E84")]
		[SerializeField]
		private GameObject m_ElementRandomObj;

		// Token: 0x040058DE RID: 22750
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003E85")]
		[SerializeField]
		private CustomButton m_DropButton;

		// Token: 0x040058DF RID: 22751
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003E86")]
		[SerializeField]
		private Image m_ClearIcon;

		// Token: 0x040058E0 RID: 22752
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003E87")]
		[SerializeField]
		private Sprite[] m_ClearIconSprites;

		// Token: 0x040058E1 RID: 22753
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003E88")]
		[SerializeField]
		private GameObject m_SpanObj;

		// Token: 0x040058E2 RID: 22754
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003E89")]
		[SerializeField]
		private Text m_SpanText;

		// Token: 0x040058E3 RID: 22755
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003E8A")]
		[SerializeField]
		private Text m_LockText;

		// Token: 0x040058E4 RID: 22756
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003E8B")]
		[SerializeField]
		private GameObject m_ValidObj;

		// Token: 0x040058E5 RID: 22757
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003E8C")]
		[SerializeField]
		private GameObject m_LimitParentObj;

		// Token: 0x040058E6 RID: 22758
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003E8D")]
		[SerializeField]
		private GameObject m_SeparateLimitPrefab;

		// Token: 0x040058E7 RID: 22759
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003E8E")]
		private List<GameObject> m_EmptySeparateList;

		// Token: 0x040058E8 RID: 22760
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003E8F")]
		private List<GameObject> m_ActiveSeparateList;

		// Token: 0x040058E9 RID: 22761
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003E90")]
		[SerializeField]
		private QuestLimit m_ClassLimitPrefab;

		// Token: 0x040058EA RID: 22762
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003E91")]
		private List<QuestLimit> m_ClassLimitList;

		// Token: 0x040058EB RID: 22763
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003E92")]
		[SerializeField]
		private QuestLimit m_ElementLimitPrefab;

		// Token: 0x040058EC RID: 22764
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4003E93")]
		private List<QuestLimit> m_ElementLimitList;

		// Token: 0x040058ED RID: 22765
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4003E94")]
		[SerializeField]
		private QuestLimit m_NamedLimitPrefab;

		// Token: 0x040058EE RID: 22766
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4003E95")]
		private QuestLimit m_NamedLimit;

		// Token: 0x040058EF RID: 22767
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4003E96")]
		[SerializeField]
		private QuestLimit m_RareLimitPrefab;

		// Token: 0x040058F0 RID: 22768
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4003E97")]
		private QuestLimit m_RareLimit;

		// Token: 0x040058F1 RID: 22769
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4003E98")]
		[SerializeField]
		private QuestLimit m_CostLimitPrefab;

		// Token: 0x040058F2 RID: 22770
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4003E99")]
		private QuestLimit m_CostLimit;

		// Token: 0x040058F3 RID: 22771
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4003E9A")]
		[SerializeField]
		private QuestLimit m_ContentLimitPrefab;

		// Token: 0x040058F4 RID: 22772
		[Cpp2IlInjected.FieldOffset(Offset = "0x180")]
		[Token(Token = "0x4003E9B")]
		private QuestLimit m_ContentLimit;

		// Token: 0x040058F5 RID: 22773
		[Cpp2IlInjected.FieldOffset(Offset = "0x188")]
		[Token(Token = "0x4003E9C")]
		private long m_CheckedStamina;
	}
}
