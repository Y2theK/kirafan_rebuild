﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000ED5 RID: 3797
	[Token(Token = "0x2000A0D")]
	[StructLayout(3)]
	public class QuestListItemData : ScrollItemData
	{
		// Token: 0x06004756 RID: 18262 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600419C")]
		[Address(RVA = "0x10152C6D0", Offset = "0x152C6D0", VA = "0x10152C6D0")]
		public QuestListItemData(int section, int questID, QuestDefine.eDifficulty difficulty, QuestManager.EventData eventData, string lockText, bool isStaminaReduction, bool existDropItem)
		{
		}

		// Token: 0x06004757 RID: 18263 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600419D")]
		[Address(RVA = "0x10152C750", Offset = "0x152C750", VA = "0x10152C750", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x06004758 RID: 18264 RVA: 0x0001A2F8 File Offset: 0x000184F8
		[Token(Token = "0x600419E")]
		[Address(RVA = "0x10152AEB8", Offset = "0x152AEB8", VA = "0x10152AEB8")]
		public bool IsLocked()
		{
			return default(bool);
		}

		// Token: 0x040058F6 RID: 22774
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E9D")]
		public int m_Section;

		// Token: 0x040058F7 RID: 22775
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003E9E")]
		public int m_QuestID;

		// Token: 0x040058F8 RID: 22776
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E9F")]
		public QuestDefine.eDifficulty m_Difficulty;

		// Token: 0x040058F9 RID: 22777
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003EA0")]
		public QuestManager.EventData m_EventData;

		// Token: 0x040058FA RID: 22778
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003EA1")]
		public string m_LockText;

		// Token: 0x040058FB RID: 22779
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003EA2")]
		public TimeSpan m_TimeSpan;

		// Token: 0x040058FC RID: 22780
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003EA3")]
		public bool m_IsStaminaReduction;

		// Token: 0x040058FD RID: 22781
		[Cpp2IlInjected.FieldOffset(Offset = "0x41")]
		[Token(Token = "0x4003EA4")]
		public bool m_ExistDropItem;

		// Token: 0x040058FE RID: 22782
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003EA5")]
		public Action<int, int> OnClick;

		// Token: 0x040058FF RID: 22783
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003EA6")]
		public Action<int> OnClickDropItem;
	}
}
