﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EBB RID: 3771
	[Token(Token = "0x20009FD")]
	[StructLayout(3)]
	public class QuestMainPartSelectUI : MenuUIBase
	{
		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x060046C6 RID: 18118 RVA: 0x0001A130 File Offset: 0x00018330
		[Token(Token = "0x170004BE")]
		public int SelectPart
		{
			[Token(Token = "0x6004112")]
			[Address(RVA = "0x10152C7C4", Offset = "0x152C7C4", VA = "0x10152C7C4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x060046C7 RID: 18119 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004113")]
		[Address(RVA = "0x10152C7CC", Offset = "0x152C7CC", VA = "0x10152C7CC")]
		public void Setup()
		{
		}

		// Token: 0x060046C8 RID: 18120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004114")]
		[Address(RVA = "0x10152CAC8", Offset = "0x152CAC8", VA = "0x10152CAC8")]
		private void Update()
		{
		}

		// Token: 0x060046C9 RID: 18121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004115")]
		[Address(RVA = "0x10152CD24", Offset = "0x152CD24", VA = "0x10152CD24")]
		private void ChangeStep(QuestMainPartSelectUI.eStep step)
		{
		}

		// Token: 0x060046CA RID: 18122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004116")]
		[Address(RVA = "0x10152CDF8", Offset = "0x152CDF8", VA = "0x10152CDF8", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060046CB RID: 18123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004117")]
		[Address(RVA = "0x10152CEB8", Offset = "0x152CEB8", VA = "0x10152CEB8", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060046CC RID: 18124 RVA: 0x0001A148 File Offset: 0x00018348
		[Token(Token = "0x6004118")]
		[Address(RVA = "0x10152CF78", Offset = "0x152CF78", VA = "0x10152CF78", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060046CD RID: 18125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004119")]
		[Address(RVA = "0x10152CF88", Offset = "0x152CF88", VA = "0x10152CF88")]
		public void OnClickPartButtonCallBack(int partType)
		{
		}

		// Token: 0x060046CE RID: 18126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411A")]
		[Address(RVA = "0x10152CF98", Offset = "0x152CF98", VA = "0x10152CF98")]
		public QuestMainPartSelectUI()
		{
		}

		// Token: 0x04005819 RID: 22553
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003DEA")]
		private QuestMainPartSelectUI.eStep m_Step;

		// Token: 0x0400581A RID: 22554
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003DEB")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400581B RID: 22555
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003DEC")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x0400581C RID: 22556
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DED")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x0400581D RID: 22557
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DEE")]
		private int m_SelectPart;

		// Token: 0x0400581E RID: 22558
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DEF")]
		public Action OnClickQuestPart;

		// Token: 0x02000EBC RID: 3772
		[Token(Token = "0x2001184")]
		private enum eStep
		{
			// Token: 0x04005820 RID: 22560
			[Token(Token = "0x4006D79")]
			Hide,
			// Token: 0x04005821 RID: 22561
			[Token(Token = "0x4006D7A")]
			In,
			// Token: 0x04005822 RID: 22562
			[Token(Token = "0x4006D7B")]
			Idle,
			// Token: 0x04005823 RID: 22563
			[Token(Token = "0x4006D7C")]
			Out,
			// Token: 0x04005824 RID: 22564
			[Token(Token = "0x4006D7D")]
			End
		}
	}
}
