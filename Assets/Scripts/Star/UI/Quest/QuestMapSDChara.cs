﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000ED0 RID: 3792
	[Token(Token = "0x2000A08")]
	[StructLayout(3)]
	public class QuestMapSDChara : MonoBehaviour
	{
		// Token: 0x0600473A RID: 18234 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004180")]
		[Address(RVA = "0x10152CFA8", Offset = "0x152CFA8", VA = "0x10152CFA8")]
		private void Update()
		{
		}

		// Token: 0x0600473B RID: 18235 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004181")]
		[Address(RVA = "0x10152D010", Offset = "0x152D010", VA = "0x10152D010")]
		private void ApplySprite(int idx)
		{
		}

		// Token: 0x0600473C RID: 18236 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004182")]
		[Address(RVA = "0x10152D09C", Offset = "0x152D09C", VA = "0x10152D09C")]
		public void PlayIn()
		{
		}

		// Token: 0x0600473D RID: 18237 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004183")]
		[Address(RVA = "0x10152D0D8", Offset = "0x152D0D8", VA = "0x10152D0D8")]
		public void Show()
		{
		}

		// Token: 0x0600473E RID: 18238 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004184")]
		[Address(RVA = "0x10152D114", Offset = "0x152D114", VA = "0x10152D114")]
		public void PlayOut()
		{
		}

		// Token: 0x0600473F RID: 18239 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004185")]
		[Address(RVA = "0x10152D144", Offset = "0x152D144", VA = "0x10152D144")]
		public void Hide()
		{
		}

		// Token: 0x06004740 RID: 18240 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004186")]
		[Address(RVA = "0x10152D174", Offset = "0x152D174", VA = "0x10152D174")]
		public QuestMapSDChara()
		{
		}

		// Token: 0x040058B8 RID: 22712
		[Token(Token = "0x4003E5F")]
		private const float CHANGE_TIME = 1f;

		// Token: 0x040058B9 RID: 22713
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E60")]
		public int m_CharaID;

		// Token: 0x040058BA RID: 22714
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E61")]
		public Sprite[] m_Sprites;

		// Token: 0x040058BB RID: 22715
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E62")]
		public Image m_Image;

		// Token: 0x040058BC RID: 22716
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E63")]
		public AnimUIPlayer m_AnimUIPlayer;

		// Token: 0x040058BD RID: 22717
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E64")]
		private int m_SpriteIdx;

		// Token: 0x040058BE RID: 22718
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003E65")]
		private float m_CurrentTime;
	}
}
