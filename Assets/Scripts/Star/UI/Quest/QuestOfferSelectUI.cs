﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EBF RID: 3775
	[Token(Token = "0x2000A00")]
	[StructLayout(3)]
	public class QuestOfferSelectUI : MenuUIBase
	{
		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x060046D4 RID: 18132 RVA: 0x0001A160 File Offset: 0x00018360
		[Token(Token = "0x170004BF")]
		public QuestOfferSelectUI.eButton SelectButton
		{
			[Token(Token = "0x6004120")]
			[Address(RVA = "0x10152D17C", Offset = "0x152D17C", VA = "0x10152D17C")]
			get
			{
				return QuestOfferSelectUI.eButton.Title;
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x060046D5 RID: 18133 RVA: 0x0001A178 File Offset: 0x00018378
		[Token(Token = "0x170004C0")]
		public eTitleType SelectedTitle
		{
			[Token(Token = "0x6004121")]
			[Address(RVA = "0x10152D184", Offset = "0x152D184", VA = "0x10152D184")]
			get
			{
				return eTitleType.Title_0000;
			}
		}

		// Token: 0x060046D6 RID: 18134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004122")]
		[Address(RVA = "0x10152D18C", Offset = "0x152D18C", VA = "0x10152D18C")]
		public void Setup(eTitleType scrollValueTitle)
		{
		}

		// Token: 0x060046D7 RID: 18135 RVA: 0x0001A190 File Offset: 0x00018390
		[Token(Token = "0x6004123")]
		[Address(RVA = "0x10152D808", Offset = "0x152D808", VA = "0x10152D808")]
		private int SortComp(QuestOfferTitleItem.QuestOfferTitleItemData l, QuestOfferTitleItem.QuestOfferTitleItemData r)
		{
			return 0;
		}

		// Token: 0x060046D8 RID: 18136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004124")]
		[Address(RVA = "0x10152D978", Offset = "0x152D978", VA = "0x10152D978")]
		private void Update()
		{
		}

		// Token: 0x060046D9 RID: 18137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004125")]
		[Address(RVA = "0x10152DB38", Offset = "0x152DB38", VA = "0x10152DB38")]
		private void ChangeStep(QuestOfferSelectUI.eStep step)
		{
		}

		// Token: 0x060046DA RID: 18138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004126")]
		[Address(RVA = "0x10152DC40", Offset = "0x152DC40", VA = "0x10152DC40", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060046DB RID: 18139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004127")]
		[Address(RVA = "0x10152DCFC", Offset = "0x152DCFC", VA = "0x10152DCFC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060046DC RID: 18140 RVA: 0x0001A1A8 File Offset: 0x000183A8
		[Token(Token = "0x6004128")]
		[Address(RVA = "0x10152DDB8", Offset = "0x152DDB8", VA = "0x10152DDB8", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060046DD RID: 18141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004129")]
		[Address(RVA = "0x10152DDC8", Offset = "0x152DDC8", VA = "0x10152DDC8")]
		private void OnClickTitleCallBack(eTitleType title)
		{
		}

		// Token: 0x060046DE RID: 18142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600412A")]
		[Address(RVA = "0x10152DDD8", Offset = "0x152DDD8", VA = "0x10152DDD8")]
		public QuestOfferSelectUI()
		{
		}

		// Token: 0x0400582C RID: 22572
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003DF7")]
		private QuestOfferSelectUI.eStep m_Step;

		// Token: 0x0400582D RID: 22573
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003DF8")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400582E RID: 22574
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003DF9")]
		[SerializeField]
		private UIGroup m_UIGroup;

		// Token: 0x0400582F RID: 22575
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DFA")]
		[SerializeField]
		private ScrollViewBase m_TitleScroll;

		// Token: 0x04005830 RID: 22576
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DFB")]
		private QuestOfferSelectUI.eButton m_SelectButton;

		// Token: 0x04005831 RID: 22577
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4003DFC")]
		private eTitleType m_SelectedTitle;

		// Token: 0x04005832 RID: 22578
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DFD")]
		public Action OnClickButton;

		// Token: 0x02000EC0 RID: 3776
		[Token(Token = "0x2001185")]
		private enum eStep
		{
			// Token: 0x04005834 RID: 22580
			[Token(Token = "0x4006D7F")]
			Hide,
			// Token: 0x04005835 RID: 22581
			[Token(Token = "0x4006D80")]
			In,
			// Token: 0x04005836 RID: 22582
			[Token(Token = "0x4006D81")]
			Idle,
			// Token: 0x04005837 RID: 22583
			[Token(Token = "0x4006D82")]
			Out,
			// Token: 0x04005838 RID: 22584
			[Token(Token = "0x4006D83")]
			End
		}

		// Token: 0x02000EC1 RID: 3777
		[Token(Token = "0x2001186")]
		public enum eButton
		{
			// Token: 0x0400583A RID: 22586
			[Token(Token = "0x4006D85")]
			None = -1,
			// Token: 0x0400583B RID: 22587
			[Token(Token = "0x4006D86")]
			Title
		}
	}
}
