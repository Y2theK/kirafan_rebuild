﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EC2 RID: 3778
	[Token(Token = "0x2000A01")]
	[StructLayout(3)]
	public class QuestOfferTitleItem : ScrollItemIcon
	{
		// Token: 0x060046DF RID: 18143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600412B")]
		[Address(RVA = "0x10152DDE8", Offset = "0x152DDE8", VA = "0x10152DDE8", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060046E0 RID: 18144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600412C")]
		[Address(RVA = "0x10152E09C", Offset = "0x152E09C", VA = "0x10152E09C")]
		public QuestOfferTitleItem()
		{
		}

		// Token: 0x0400583C RID: 22588
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DFE")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x0400583D RID: 22589
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DFF")]
		[SerializeField]
		private SwitchFade m_SwitchFade;

		// Token: 0x0400583E RID: 22590
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003E00")]
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x0400583F RID: 22591
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003E01")]
		[SerializeField]
		private CanvasGroup m_NewIcon;

		// Token: 0x04005840 RID: 22592
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003E02")]
		[SerializeField]
		private CanvasGroup m_ExistRepeat;

		// Token: 0x04005841 RID: 22593
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003E03")]
		[SerializeField]
		private GameObject m_LockedObj;

		// Token: 0x02000EC3 RID: 3779
		[Token(Token = "0x2001187")]
		public class QuestOfferTitleItemData : ScrollItemData
		{
			// Token: 0x060046E1 RID: 18145 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006267")]
			[Address(RVA = "0x10152D7C4", Offset = "0x152D7C4", VA = "0x10152D7C4")]
			public QuestOfferTitleItemData(eTitleType titleType, Action<eTitleType> OnClick)
			{
			}

			// Token: 0x060046E2 RID: 18146 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006268")]
			[Address(RVA = "0x10152E0A4", Offset = "0x152E0A4", VA = "0x10152E0A4", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x04005842 RID: 22594
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006D87")]
			public eTitleType m_TitleType;

			// Token: 0x04005843 RID: 22595
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006D88")]
			public bool m_IsLocked;

			// Token: 0x04005844 RID: 22596
			[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
			[Token(Token = "0x4006D89")]
			public bool m_IsNew;

			// Token: 0x04005845 RID: 22597
			[Cpp2IlInjected.FieldOffset(Offset = "0x1E")]
			[Token(Token = "0x4006D8A")]
			public bool m_IsExistRepeat;

			// Token: 0x04005846 RID: 22598
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006D8B")]
			private Action<eTitleType> m_OnClick;
		}
	}
}
