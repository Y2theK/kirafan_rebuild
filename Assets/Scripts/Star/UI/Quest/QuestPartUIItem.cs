﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000EBD RID: 3773
	[Token(Token = "0x20009FE")]
	[StructLayout(3)]
	public class QuestPartUIItem : ScrollItemIcon
	{
		// Token: 0x060046CF RID: 18127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411B")]
		[Address(RVA = "0x10152E104", Offset = "0x152E104", VA = "0x10152E104", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060046D0 RID: 18128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411C")]
		[Address(RVA = "0x10152E370", Offset = "0x152E370", VA = "0x10152E370", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x060046D1 RID: 18129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411D")]
		[Address(RVA = "0x10152E3B0", Offset = "0x152E3B0", VA = "0x10152E3B0")]
		public void OnClickItemCallBack()
		{
		}

		// Token: 0x060046D2 RID: 18130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411E")]
		[Address(RVA = "0x10152E4AC", Offset = "0x152E4AC", VA = "0x10152E4AC")]
		public QuestPartUIItem()
		{
		}

		// Token: 0x04005825 RID: 22565
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003DF0")]
		[SerializeField]
		private QuestPartImage m_Image;

		// Token: 0x04005826 RID: 22566
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003DF1")]
		[SerializeField]
		private GameObject m_NewIcon;

		// Token: 0x04005827 RID: 22567
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003DF2")]
		[SerializeField]
		private GameObject m_StaminaIcon;
	}
}
