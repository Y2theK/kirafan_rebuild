﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Quest
{
	// Token: 0x02000EBE RID: 3774
	[Token(Token = "0x20009FF")]
	[StructLayout(3)]
	public class QuestPartUIItemData : ScrollItemData
	{
		// Token: 0x060046D3 RID: 18131 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600411F")]
		[Address(RVA = "0x10152CA74", Offset = "0x152CA74", VA = "0x10152CA74")]
		public QuestPartUIItemData(int partType, bool isNew, bool isStaminaReduction, Action<int> callback)
		{
		}

		// Token: 0x04005828 RID: 22568
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003DF3")]
		public int m_PartType;

		// Token: 0x04005829 RID: 22569
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003DF4")]
		public bool m_IsNew;

		// Token: 0x0400582A RID: 22570
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
		[Token(Token = "0x4003DF5")]
		public bool m_IsStaminaReduction;

		// Token: 0x0400582B RID: 22571
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003DF6")]
		public Action<int> m_OnClick;
	}
}
