﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000ED1 RID: 3793
	[Token(Token = "0x2000A09")]
	[StructLayout(3)]
	public class QuestPointEventDisplay : MonoBehaviour
	{
		// Token: 0x06004741 RID: 18241 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004187")]
		[Address(RVA = "0x1015240B4", Offset = "0x15240B4", VA = "0x1015240B4")]
		public void Apply(QuestManager.PointEventData pointEventData)
		{
		}

		// Token: 0x06004742 RID: 18242 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004188")]
		[Address(RVA = "0x10152E4B4", Offset = "0x152E4B4", VA = "0x10152E4B4")]
		public QuestPointEventDisplay()
		{
		}

		// Token: 0x040058BF RID: 22719
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003E66")]
		[SerializeField]
		private ImageNumbers m_TotalPoint;

		// Token: 0x040058C0 RID: 22720
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003E67")]
		[SerializeField]
		private ImageNumbers m_NextPoint;

		// Token: 0x040058C1 RID: 22721
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003E68")]
		[SerializeField]
		private ImageNumbers m_PlayerTotalPoint;

		// Token: 0x040058C2 RID: 22722
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003E69")]
		[SerializeField]
		private GameObject m_ExistNextRewardObj;

		// Token: 0x040058C3 RID: 22723
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003E6A")]
		[SerializeField]
		private GameObject m_NoExistNextRewardObj;

		// Token: 0x040058C4 RID: 22724
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003E6B")]
		[SerializeField]
		private CustomButton m_ReceiveButton;
	}
}
