﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000ED6 RID: 3798
	[Token(Token = "0x2000A0E")]
	[StructLayout(3)]
	public class QuestSelectUI : MenuUIBase
	{
		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06004759 RID: 18265 RVA: 0x0001A310 File Offset: 0x00018510
		[Token(Token = "0x170004C6")]
		public QuestSelectUI.eButton SelectButton
		{
			[Token(Token = "0x600419F")]
			[Address(RVA = "0x10152E4BC", Offset = "0x152E4BC", VA = "0x10152E4BC")]
			get
			{
				return QuestSelectUI.eButton.Chapter;
			}
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x0600475A RID: 18266 RVA: 0x0001A328 File Offset: 0x00018528
		[Token(Token = "0x170004C7")]
		public QuestDefine.eDifficulty SelectDifficulty
		{
			[Token(Token = "0x60041A0")]
			[Address(RVA = "0x10152E4C4", Offset = "0x152E4C4", VA = "0x10152E4C4")]
			get
			{
				return QuestDefine.eDifficulty.Normal;
			}
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x0600475B RID: 18267 RVA: 0x0001A340 File Offset: 0x00018540
		[Token(Token = "0x170004C8")]
		public int SelectQuestID
		{
			[Token(Token = "0x60041A1")]
			[Address(RVA = "0x10152E4CC", Offset = "0x152E4CC", VA = "0x10152E4CC")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x0600475C RID: 18268 RVA: 0x0001A358 File Offset: 0x00018558
		[Token(Token = "0x170004C9")]
		public int SelectEventID
		{
			[Token(Token = "0x60041A2")]
			[Address(RVA = "0x10152E4D4", Offset = "0x152E4D4", VA = "0x10152E4D4")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x0600475D RID: 18269 RVA: 0x0001A370 File Offset: 0x00018570
		[Token(Token = "0x170004CA")]
		public QuestSelectUI.eMode Mode
		{
			[Token(Token = "0x60041A3")]
			[Address(RVA = "0x10152E4DC", Offset = "0x152E4DC", VA = "0x10152E4DC")]
			get
			{
				return QuestSelectUI.eMode.Select;
			}
		}

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x0600475E RID: 18270 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004CB")]
		public QuestManager.EventGroupData CurrentGroupData
		{
			[Token(Token = "0x60041A4")]
			[Address(RVA = "0x10152E4E4", Offset = "0x152E4E4", VA = "0x10152E4E4")]
			get
			{
				return null;
			}
		}

		// Token: 0x140000AD RID: 173
		// (add) Token: 0x0600475F RID: 18271 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004760 RID: 18272 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AD")]
		public event Func<bool> OnClickQuestButton
		{
			[Token(Token = "0x60041A5")]
			[Address(RVA = "0x10152E4EC", Offset = "0x152E4EC", VA = "0x10152E4EC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041A6")]
			[Address(RVA = "0x10152E5F8", Offset = "0x152E5F8", VA = "0x10152E5F8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000AE RID: 174
		// (add) Token: 0x06004761 RID: 18273 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004762 RID: 18274 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AE")]
		public event Action OnClickChapterButton
		{
			[Token(Token = "0x60041A7")]
			[Address(RVA = "0x10152E704", Offset = "0x152E704", VA = "0x10152E704")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041A8")]
			[Address(RVA = "0x10152E810", Offset = "0x152E810", VA = "0x10152E810")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000AF RID: 175
		// (add) Token: 0x06004763 RID: 18275 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004764 RID: 18276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000AF")]
		public event Action OnClickShopButton
		{
			[Token(Token = "0x60041A9")]
			[Address(RVA = "0x10152E91C", Offset = "0x152E91C", VA = "0x10152E91C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041AA")]
			[Address(RVA = "0x10152EA28", Offset = "0x152EA28", VA = "0x10152EA28")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004765 RID: 18277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041AB")]
		[Address(RVA = "0x10152EB34", Offset = "0x152EB34", VA = "0x10152EB34")]
		public void Setup()
		{
		}

		// Token: 0x06004766 RID: 18278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041AC")]
		[Address(RVA = "0x10152EDCC", Offset = "0x152EDCC", VA = "0x10152EDCC")]
		public void RefreshList()
		{
		}

		// Token: 0x06004767 RID: 18279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041AD")]
		[Address(RVA = "0x10152F348", Offset = "0x152F348", VA = "0x10152F348")]
		private void ApplyChapterQuestList(int chapterID, QuestDefine.eDifficulty difficulty)
		{
		}

		// Token: 0x06004768 RID: 18280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041AE")]
		[Address(RVA = "0x1015307D4", Offset = "0x15307D4", VA = "0x1015307D4")]
		public void ApplyEventQuestList(int groupID)
		{
		}

		// Token: 0x06004769 RID: 18281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041AF")]
		[Address(RVA = "0x10152FD44", Offset = "0x152FD44", VA = "0x10152FD44")]
		public void ApplyCharaQuestList(int charaID)
		{
		}

		// Token: 0x0600476A RID: 18282 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B0")]
		[Address(RVA = "0x1015302EC", Offset = "0x15302EC", VA = "0x1015302EC")]
		public void ApplyOfferQuestList(eTitleType titleType)
		{
		}

		// Token: 0x0600476B RID: 18283 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B1")]
		[Address(RVA = "0x101531044", Offset = "0x1531044", VA = "0x101531044")]
		private void ApplySectionToScrollData(List<QuestListItemData> dataList)
		{
		}

		// Token: 0x0600476C RID: 18284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B2")]
		[Address(RVA = "0x10152F11C", Offset = "0x152F11C", VA = "0x10152F11C")]
		private void SetScrollPositionByQuestID(int questID)
		{
		}

		// Token: 0x0600476D RID: 18285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B3")]
		[Address(RVA = "0x1015311B8", Offset = "0x15311B8", VA = "0x1015311B8")]
		private void Update()
		{
		}

		// Token: 0x0600476E RID: 18286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B4")]
		[Address(RVA = "0x101531430", Offset = "0x1531430", VA = "0x101531430")]
		private void ChangeStep(QuestSelectUI.eStep step)
		{
		}

		// Token: 0x0600476F RID: 18287 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B5")]
		[Address(RVA = "0x101531538", Offset = "0x1531538", VA = "0x101531538", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004770 RID: 18288 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B6")]
		[Address(RVA = "0x1015317C8", Offset = "0x15317C8", VA = "0x1015317C8")]
		public void PlayInMapUIOnly()
		{
		}

		// Token: 0x06004771 RID: 18289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B7")]
		[Address(RVA = "0x1015315DC", Offset = "0x15315DC", VA = "0x1015315DC")]
		private void PlayInWithoutList()
		{
		}

		// Token: 0x06004772 RID: 18290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041B8")]
		[Address(RVA = "0x1015318F4", Offset = "0x15318F4", VA = "0x1015318F4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004773 RID: 18291 RVA: 0x0001A388 File Offset: 0x00018588
		[Token(Token = "0x60041B9")]
		[Address(RVA = "0x101531ADC", Offset = "0x1531ADC", VA = "0x101531ADC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004774 RID: 18292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BA")]
		[Address(RVA = "0x101531AEC", Offset = "0x1531AEC", VA = "0x101531AEC")]
		public void OnClickChapterSelectButtonCallBack()
		{
		}

		// Token: 0x06004775 RID: 18293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BB")]
		[Address(RVA = "0x101531AFC", Offset = "0x1531AFC", VA = "0x101531AFC")]
		public void OnClickQuestButtonCallBack(int questID, int eventID)
		{
		}

		// Token: 0x06004776 RID: 18294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BC")]
		[Address(RVA = "0x101531B7C", Offset = "0x1531B7C", VA = "0x101531B7C")]
		public void OnClickDifficultyButtonCallBack(int difficulty)
		{
		}

		// Token: 0x06004777 RID: 18295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BD")]
		[Address(RVA = "0x101531B98", Offset = "0x1531B98", VA = "0x101531B98")]
		public void OnClickDropItemButtonCallBack(int questID)
		{
		}

		// Token: 0x06004778 RID: 18296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BE")]
		[Address(RVA = "0x101531C44", Offset = "0x1531C44", VA = "0x101531C44")]
		private void OnClickShopButtonCallBack()
		{
		}

		// Token: 0x06004779 RID: 18297 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041BF")]
		[Address(RVA = "0x101531C58", Offset = "0x1531C58", VA = "0x101531C58")]
		private void OnClickChestButtonCallBack()
		{
		}

		// Token: 0x0600477A RID: 18298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041C0")]
		[Address(RVA = "0x101531C6C", Offset = "0x1531C6C", VA = "0x101531C6C")]
		public void OpenURL()
		{
		}

		// Token: 0x0600477B RID: 18299 RVA: 0x0001A3A0 File Offset: 0x000185A0
		[Token(Token = "0x60041C1")]
		[Address(RVA = "0x101531C98", Offset = "0x1531C98", VA = "0x101531C98")]
		public bool IsReadyPlayVoice()
		{
			return default(bool);
		}

		// Token: 0x0600477C RID: 18300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041C2")]
		[Address(RVA = "0x101531CF8", Offset = "0x1531CF8", VA = "0x101531CF8")]
		public void PlayVoice()
		{
		}

		// Token: 0x0600477D RID: 18301 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041C3")]
		[Address(RVA = "0x1015319F4", Offset = "0x15319F4", VA = "0x1015319F4")]
		public void StopVoice()
		{
		}

		// Token: 0x0600477E RID: 18302 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041C4")]
		[Address(RVA = "0x101531F94", Offset = "0x1531F94", VA = "0x101531F94")]
		public QuestSelectUI()
		{
		}

		// Token: 0x04005900 RID: 22784
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003EA7")]
		private QuestSelectUI.eStep m_Step;

		// Token: 0x04005901 RID: 22785
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003EA8")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005902 RID: 22786
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003EA9")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005903 RID: 22787
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003EAA")]
		[SerializeField]
		private UIGroup m_MapGroup;

		// Token: 0x04005904 RID: 22788
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003EAB")]
		[SerializeField]
		private ScrollViewBase m_QuestListScroll;

		// Token: 0x04005905 RID: 22789
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003EAC")]
		[SerializeField]
		private GameObject m_StoryObj;

		// Token: 0x04005906 RID: 22790
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003EAD")]
		[SerializeField]
		private DropItemWindow m_DropWindow;

		// Token: 0x04005907 RID: 22791
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003EAE")]
		[SerializeField]
		private CustomButton m_DifficultyButtonNormal;

		// Token: 0x04005908 RID: 22792
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003EAF")]
		[SerializeField]
		private CustomButton m_DifficultyButtonHard;

		// Token: 0x04005909 RID: 22793
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003EB0")]
		[SerializeField]
		private CustomButton m_DifficultyButtonStar;

		// Token: 0x0400590A RID: 22794
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003EB1")]
		[SerializeField]
		private AnimUIPlayer m_EventNameAnim;

		// Token: 0x0400590B RID: 22795
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003EB2")]
		[SerializeField]
		private Text m_EventNameText;

		// Token: 0x0400590C RID: 22796
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003EB3")]
		[SerializeField]
		private QuestEventUICtrl m_EventUICtrl;

		// Token: 0x0400590D RID: 22797
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003EB4")]
		private QuestSelectUI.eButton m_SelectButton;

		// Token: 0x0400590E RID: 22798
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4003EB5")]
		private int m_SelectQuestID;

		// Token: 0x0400590F RID: 22799
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003EB6")]
		private int m_SelectEventID;

		// Token: 0x04005910 RID: 22800
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4003EB7")]
		private QuestDefine.eDifficulty m_SelectDifficulty;

		// Token: 0x04005911 RID: 22801
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003EB8")]
		private int m_ChapterID;

		// Token: 0x04005912 RID: 22802
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003EB9")]
		private QuestManager.EventGroupData m_CurrentGroupData;

		// Token: 0x04005913 RID: 22803
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003EBA")]
		private int m_VoiceReqID;

		// Token: 0x04005914 RID: 22804
		[Cpp2IlInjected.FieldOffset(Offset = "0xCC")]
		[Token(Token = "0x4003EBB")]
		private QuestSelectUI.eMode m_Mode;

		// Token: 0x02000ED7 RID: 3799
		[Token(Token = "0x200118E")]
		private enum eStep
		{
			// Token: 0x04005919 RID: 22809
			[Token(Token = "0x4006DA3")]
			Hide,
			// Token: 0x0400591A RID: 22810
			[Token(Token = "0x4006DA4")]
			In,
			// Token: 0x0400591B RID: 22811
			[Token(Token = "0x4006DA5")]
			Idle,
			// Token: 0x0400591C RID: 22812
			[Token(Token = "0x4006DA6")]
			Out,
			// Token: 0x0400591D RID: 22813
			[Token(Token = "0x4006DA7")]
			End
		}

		// Token: 0x02000ED8 RID: 3800
		[Token(Token = "0x200118F")]
		public enum eButton
		{
			// Token: 0x0400591F RID: 22815
			[Token(Token = "0x4006DA9")]
			None = -1,
			// Token: 0x04005920 RID: 22816
			[Token(Token = "0x4006DAA")]
			Chapter,
			// Token: 0x04005921 RID: 22817
			[Token(Token = "0x4006DAB")]
			Quest,
			// Token: 0x04005922 RID: 22818
			[Token(Token = "0x4006DAC")]
			Shop,
			// Token: 0x04005923 RID: 22819
			[Token(Token = "0x4006DAD")]
			Chest
		}

		// Token: 0x02000ED9 RID: 3801
		[Token(Token = "0x2001190")]
		public enum eMode
		{
			// Token: 0x04005925 RID: 22821
			[Token(Token = "0x4006DAF")]
			Select,
			// Token: 0x04005926 RID: 22822
			[Token(Token = "0x4006DB0")]
			Map
		}
	}
}
