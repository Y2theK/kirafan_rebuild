﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EDA RID: 3802
	[Token(Token = "0x2000A0F")]
	[StructLayout(3)]
	public class StageSelectPanel : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x0600477F RID: 18303 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004CC")]
		public GameObject GameObject
		{
			[Token(Token = "0x60041C5")]
			[Address(RVA = "0x101531FBC", Offset = "0x1531FBC", VA = "0x101531FBC")]
			get
			{
				return null;
			}
		}

		// Token: 0x17000524 RID: 1316
		// (get) Token: 0x06004780 RID: 18304 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004CD")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x60041C6")]
			[Address(RVA = "0x101531FC4", Offset = "0x1531FC4", VA = "0x101531FC4")]
			get
			{
				return null;
			}
		}

		// Token: 0x140000B0 RID: 176
		// (add) Token: 0x06004781 RID: 18305 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004782 RID: 18306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B0")]
		public event Action<int> OnClickButton
		{
			[Token(Token = "0x60041C7")]
			[Address(RVA = "0x101531FCC", Offset = "0x1531FCC", VA = "0x101531FCC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041C8")]
			[Address(RVA = "0x1015320D8", Offset = "0x15320D8", VA = "0x1015320D8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000B1 RID: 177
		// (add) Token: 0x06004783 RID: 18307 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004784 RID: 18308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B1")]
		public event Action<int> OnClickPanel
		{
			[Token(Token = "0x60041C9")]
			[Address(RVA = "0x1015321E4", Offset = "0x15321E4", VA = "0x1015321E4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041CA")]
			[Address(RVA = "0x1015322F0", Offset = "0x15322F0", VA = "0x1015322F0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004785 RID: 18309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041CB")]
		[Address(RVA = "0x1015323FC", Offset = "0x15323FC", VA = "0x1015323FC")]
		private void Awake()
		{
		}

		// Token: 0x06004786 RID: 18310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041CC")]
		[Address(RVA = "0x101532464", Offset = "0x1532464", VA = "0x101532464")]
		public void SetStageData(int idx, StageSelectPanel.StageData data)
		{
		}

		// Token: 0x06004787 RID: 18311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041CD")]
		[Address(RVA = "0x1015326B0", Offset = "0x15326B0", VA = "0x1015326B0")]
		public void OnTapBattleButton()
		{
		}

		// Token: 0x06004788 RID: 18312 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041CE")]
		[Address(RVA = "0x101532714", Offset = "0x1532714", VA = "0x101532714", Slot = "4")]
		public void OnPointerClick(PointerEventData eventdata)
		{
		}

		// Token: 0x06004789 RID: 18313 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041CF")]
		[Address(RVA = "0x1015327C0", Offset = "0x15327C0", VA = "0x1015327C0")]
		public StageSelectPanel()
		{
		}

		// Token: 0x04005927 RID: 22823
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003EBF")]
		[SerializeField]
		private Text m_NumberObj;

		// Token: 0x04005928 RID: 22824
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003EC0")]
		[SerializeField]
		private Text m_TitleObj;

		// Token: 0x04005929 RID: 22825
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003EC1")]
		[SerializeField]
		private GameObject m_EnemyTypeObj;

		// Token: 0x0400592A RID: 22826
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003EC2")]
		[SerializeField]
		private GameObject m_MissionObj;

		// Token: 0x0400592B RID: 22827
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003EC3")]
		[SerializeField]
		private Text m_StaminaText;

		// Token: 0x0400592C RID: 22828
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003EC4")]
		[SerializeField]
		private Button m_BattleButton;

		// Token: 0x0400592D RID: 22829
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003EC5")]
		[SerializeField]
		private GameObject m_ClearMark;

		// Token: 0x0400592E RID: 22830
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003EC6")]
		private int m_Idx;

		// Token: 0x0400592F RID: 22831
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003EC7")]
		private GameObject m_GameObject;

		// Token: 0x04005930 RID: 22832
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003EC8")]
		private RectTransform m_RectTransform;

		// Token: 0x02000EDB RID: 3803
		[Token(Token = "0x2001191")]
		public struct StageData
		{
			// Token: 0x04005933 RID: 22835
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006DB1")]
			public int m_StageID;

			// Token: 0x04005934 RID: 22836
			[Cpp2IlInjected.FieldOffset(Offset = "0x4")]
			[Token(Token = "0x4006DB2")]
			public int m_Number;

			// Token: 0x04005935 RID: 22837
			[Cpp2IlInjected.FieldOffset(Offset = "0x8")]
			[Token(Token = "0x4006DB3")]
			public string m_Title;

			// Token: 0x04005936 RID: 22838
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006DB4")]
			public int m_Stamina;

			// Token: 0x04005937 RID: 22839
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006DB5")]
			public int m_ClearRank;

			// Token: 0x04005938 RID: 22840
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006DB6")]
			public bool m_IsClear;
		}
	}
}
