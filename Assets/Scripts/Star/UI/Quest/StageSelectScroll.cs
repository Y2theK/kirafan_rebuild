﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EDC RID: 3804
	[Token(Token = "0x2000A10")]
	[StructLayout(3)]
	public class StageSelectScroll : MonoBehaviour, IDragHandler, IEventSystemHandler, IEndDragHandler
	{
		// Token: 0x140000B2 RID: 178
		// (add) Token: 0x0600478A RID: 18314 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600478B RID: 18315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000B2")]
		public event Action<int> OnDecideStage
		{
			[Token(Token = "0x60041D0")]
			[Address(RVA = "0x1015327C8", Offset = "0x15327C8", VA = "0x1015327C8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60041D1")]
			[Address(RVA = "0x1015328D4", Offset = "0x15328D4", VA = "0x1015328D4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600478C RID: 18316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D2")]
		[Address(RVA = "0x1015329E0", Offset = "0x15329E0", VA = "0x1015329E0")]
		public void Setup()
		{
		}

		// Token: 0x0600478D RID: 18317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D3")]
		[Address(RVA = "0x101532AF4", Offset = "0x1532AF4", VA = "0x101532AF4")]
		private void Update()
		{
		}

		// Token: 0x0600478E RID: 18318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D4")]
		[Address(RVA = "0x101532EC4", Offset = "0x1532EC4", VA = "0x101532EC4")]
		private void UpdateItemPosition()
		{
		}

		// Token: 0x0600478F RID: 18319 RVA: 0x0001A3B8 File Offset: 0x000185B8
		[Token(Token = "0x60041D5")]
		[Address(RVA = "0x1015334C8", Offset = "0x15334C8", VA = "0x1015334C8")]
		public int AddItem(StageSelectPanel.StageData data)
		{
			return 0;
		}

		// Token: 0x06004790 RID: 18320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D6")]
		[Address(RVA = "0x101533734", Offset = "0x1533734", VA = "0x101533734")]
		public void OnTapButton(int idx)
		{
		}

		// Token: 0x06004791 RID: 18321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D7")]
		[Address(RVA = "0x1015337E8", Offset = "0x15337E8", VA = "0x1015337E8")]
		public void OnTapPanel(int idx)
		{
		}

		// Token: 0x06004792 RID: 18322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D8")]
		[Address(RVA = "0x1015337F0", Offset = "0x15337F0", VA = "0x1015337F0", Slot = "4")]
		public void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06004793 RID: 18323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041D9")]
		[Address(RVA = "0x1015337FC", Offset = "0x15337FC", VA = "0x1015337FC", Slot = "5")]
		public void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06004794 RID: 18324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DA")]
		[Address(RVA = "0x101533804", Offset = "0x1533804", VA = "0x101533804")]
		public StageSelectScroll()
		{
		}

		// Token: 0x04005939 RID: 22841
		[Token(Token = "0x4003ECB")]
		private const float SELECT_PANEL_SCALE = 1f;

		// Token: 0x0400593A RID: 22842
		[Token(Token = "0x4003ECC")]
		private const float NONSELECT_PANEL_SCALE = 0.5f;

		// Token: 0x0400593B RID: 22843
		[Token(Token = "0x4003ECD")]
		private const int MAX_SHOW_PANEL = 5;

		// Token: 0x0400593C RID: 22844
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003ECE")]
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x0400593D RID: 22845
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003ECF")]
		[SerializeField]
		private StageSelectPanel m_StageSelectPanelPrefab;

		// Token: 0x0400593E RID: 22846
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003ED0")]
		[SerializeField]
		private float m_Space;

		// Token: 0x0400593F RID: 22847
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003ED1")]
		private int m_ItemNum;

		// Token: 0x04005940 RID: 22848
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003ED2")]
		private List<StageSelectPanel.StageData> m_DataList;

		// Token: 0x04005941 RID: 22849
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003ED3")]
		private List<StageSelectPanel> m_InstList;

		// Token: 0x04005942 RID: 22850
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003ED4")]
		private List<StageSelectPanel> m_EmptyInstList;

		// Token: 0x04005943 RID: 22851
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003ED5")]
		private bool m_IsDirty;

		// Token: 0x04005944 RID: 22852
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4003ED6")]
		private int m_SelectStage;

		// Token: 0x04005945 RID: 22853
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003ED7")]
		private float m_PanelHeight;

		// Token: 0x04005946 RID: 22854
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4003ED8")]
		private bool m_Drag;

		// Token: 0x04005947 RID: 22855
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003ED9")]
		private float m_ScrollRate;

		// Token: 0x04005949 RID: 22857
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003EDB")]
		private RectTransform m_ViewPort;

		// Token: 0x0400594A RID: 22858
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003EDC")]
		private RectTransform m_Content;
	}
}
