﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000EDD RID: 3805
	[Token(Token = "0x2000A11")]
	[StructLayout(3)]
	public class StaminaReductionIcon : MonoBehaviour
	{
		// Token: 0x06004795 RID: 18325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DB")]
		[Address(RVA = "0x10152422C", Offset = "0x152422C", VA = "0x10152422C")]
		public void Apply(QuestManager.StaminaReduction reduction)
		{
		}

		// Token: 0x06004796 RID: 18326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60041DC")]
		[Address(RVA = "0x101533818", Offset = "0x1533818", VA = "0x101533818")]
		public StaminaReductionIcon()
		{
		}

		// Token: 0x0400594B RID: 22859
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003EDD")]
		[SerializeField]
		private GameObject m_SpanBG;

		// Token: 0x0400594C RID: 22860
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003EDE")]
		[SerializeField]
		private Text m_SpanText;
	}
}
