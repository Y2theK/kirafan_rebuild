﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF4 RID: 3316
	[Token(Token = "0x20008DD")]
	[StructLayout(3)]
	public class QuestEventTypeIcon : ASyncImage
	{
		// Token: 0x06003C97 RID: 15511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003754")]
		[Address(RVA = "0x10151DE28", Offset = "0x151DE28", VA = "0x10151DE28")]
		public void Apply(int eventType)
		{
		}

		// Token: 0x06003C98 RID: 15512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003755")]
		[Address(RVA = "0x101533820", Offset = "0x1533820", VA = "0x101533820", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C99 RID: 15513 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003756")]
		[Address(RVA = "0x101533828", Offset = "0x1533828", VA = "0x101533828")]
		public QuestEventTypeIcon()
		{
		}
	}
}
