﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF5 RID: 3317
	[Token(Token = "0x20008DE")]
	[StructLayout(3)]
	public class QuestGroupIcon : ASyncImage
	{
		// Token: 0x06003C9A RID: 15514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003757")]
		[Address(RVA = "0x101528598", Offset = "0x1528598", VA = "0x101528598")]
		public void Apply(int bannerID)
		{
		}

		// Token: 0x06003C9B RID: 15515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003758")]
		[Address(RVA = "0x101533830", Offset = "0x1533830", VA = "0x101533830", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C9C RID: 15516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003759")]
		[Address(RVA = "0x10153385C", Offset = "0x153385C", VA = "0x10153385C")]
		public QuestGroupIcon()
		{
		}

		// Token: 0x04004BE7 RID: 19431
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003555")]
		protected int m_BannerID;
	}
}
