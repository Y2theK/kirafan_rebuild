﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF6 RID: 3318
	[Token(Token = "0x20008DF")]
	[StructLayout(3)]
	public class QuestPartImage : ASyncImage
	{
		// Token: 0x06003C9D RID: 15517 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375A")]
		[Address(RVA = "0x10152E260", Offset = "0x152E260", VA = "0x10152E260")]
		public void Apply(int part)
		{
		}

		// Token: 0x06003C9E RID: 15518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375B")]
		[Address(RVA = "0x10153386C", Offset = "0x153386C", VA = "0x10153386C", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003C9F RID: 15519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375C")]
		[Address(RVA = "0x101533898", Offset = "0x1533898", VA = "0x101533898")]
		public QuestPartImage()
		{
		}

		// Token: 0x04004BE8 RID: 19432
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003556")]
		protected int m_part;
	}
}
