﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D85 RID: 3461
	[Token(Token = "0x2000944")]
	[StructLayout(3)]
	public class QuestStartButtonPanel : MonoBehaviour
	{
		// Token: 0x06003FD6 RID: 16342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7B")]
		[Address(RVA = "0x1015338A8", Offset = "0x15338A8", VA = "0x1015338A8")]
		public void Apply(int questID)
		{
		}

		// Token: 0x06003FD7 RID: 16343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7C")]
		[Address(RVA = "0x101533D44", Offset = "0x1533D44", VA = "0x101533D44")]
		private void Update()
		{
		}

		// Token: 0x06003FD8 RID: 16344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7D")]
		[Address(RVA = "0x101533AD8", Offset = "0x1533AD8", VA = "0x101533AD8")]
		public void UpdateUseStamina()
		{
		}

		// Token: 0x06003FD9 RID: 16345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7E")]
		[Address(RVA = "0x101533BE0", Offset = "0x1533BE0", VA = "0x101533BE0")]
		private void UpdateCostText()
		{
		}

		// Token: 0x06003FDA RID: 16346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A7F")]
		[Address(RVA = "0x101533E20", Offset = "0x1533E20", VA = "0x101533E20")]
		public QuestStartButtonPanel()
		{
		}

		// Token: 0x04004F3A RID: 20282
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40037D1")]
		[SerializeField]
		private GameObject m_CostStaminaObj;

		// Token: 0x04004F3B RID: 20283
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40037D2")]
		[SerializeField]
		private Text m_CostValue;

		// Token: 0x04004F3C RID: 20284
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037D3")]
		[SerializeField]
		private GameObject m_CostItemObj;

		// Token: 0x04004F3D RID: 20285
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40037D4")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04004F3E RID: 20286
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40037D5")]
		private int m_QuestID;

		// Token: 0x04004F3F RID: 20287
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40037D6")]
		private int m_Use;

		// Token: 0x04004F40 RID: 20288
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40037D7")]
		private long m_Current;

		// Token: 0x04004F41 RID: 20289
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40037D8")]
		private int m_UseItemID;
	}
}
