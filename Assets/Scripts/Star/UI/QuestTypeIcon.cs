﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF7 RID: 3319
	[Token(Token = "0x20008E0")]
	[StructLayout(3)]
	public class QuestTypeIcon : ASyncImage
	{
		// Token: 0x06003CA0 RID: 15520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375D")]
		[Address(RVA = "0x10152AED8", Offset = "0x152AED8", VA = "0x10152AED8")]
		public void Apply(int id)
		{
		}

		// Token: 0x06003CA1 RID: 15521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375E")]
		[Address(RVA = "0x101533E34", Offset = "0x1533E34", VA = "0x101533E34", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CA2 RID: 15522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600375F")]
		[Address(RVA = "0x101533E60", Offset = "0x1533E60", VA = "0x101533E60")]
		public QuestTypeIcon()
		{
		}

		// Token: 0x04004BE9 RID: 19433
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003557")]
		protected int m_IconID;
	}
}
