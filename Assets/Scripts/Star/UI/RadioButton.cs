﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D32 RID: 3378
	[Token(Token = "0x200090C")]
	[StructLayout(3)]
	public class RadioButton : MonoBehaviour
	{
		// Token: 0x06003DE6 RID: 15846 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600389E")]
		[Address(RVA = "0x101533E70", Offset = "0x1533E70", VA = "0x101533E70")]
		public void Setup(string[] buttonText)
		{
		}

		// Token: 0x06003DE7 RID: 15847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600389F")]
		[Address(RVA = "0x101534284", Offset = "0x1534284", VA = "0x101534284")]
		public void Destroy()
		{
		}

		// Token: 0x06003DE8 RID: 15848 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60038A0")]
		[Address(RVA = "0x101534420", Offset = "0x1534420", VA = "0x101534420")]
		public RadioButtonParts GetButton(int index)
		{
			return null;
		}

		// Token: 0x06003DE9 RID: 15849 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A1")]
		[Address(RVA = "0x1015344D0", Offset = "0x15344D0", VA = "0x1015344D0")]
		public void SetActive(int index, bool isActive)
		{
		}

		// Token: 0x06003DEA RID: 15850 RVA: 0x000188E8 File Offset: 0x00016AE8
		[Token(Token = "0x60038A2")]
		[Address(RVA = "0x1015345AC", Offset = "0x15345AC", VA = "0x1015345AC")]
		public bool GetInteractable(int index)
		{
			return default(bool);
		}

		// Token: 0x06003DEB RID: 15851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A3")]
		[Address(RVA = "0x101534660", Offset = "0x1534660", VA = "0x101534660")]
		public void SetInteractable(int index, bool isActive)
		{
		}

		// Token: 0x06003DEC RID: 15852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A4")]
		[Address(RVA = "0x101534724", Offset = "0x1534724", VA = "0x101534724")]
		public void SetEnable(int index, bool isEnable)
		{
		}

		// Token: 0x06003DED RID: 15853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A5")]
		[Address(RVA = "0x1015347E8", Offset = "0x15347E8", VA = "0x1015347E8")]
		public void SetEnable(bool isEnable)
		{
		}

		// Token: 0x06003DEE RID: 15854 RVA: 0x00018900 File Offset: 0x00016B00
		[Token(Token = "0x60038A6")]
		[Address(RVA = "0x101534918", Offset = "0x1534918", VA = "0x101534918")]
		public int GetCurrent()
		{
			return 0;
		}

		// Token: 0x06003DEF RID: 15855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A7")]
		[Address(RVA = "0x101534920", Offset = "0x1534920", VA = "0x101534920")]
		public void SetCurrent(int index)
		{
		}

		// Token: 0x06003DF0 RID: 15856 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A8")]
		[Address(RVA = "0x101534928", Offset = "0x1534928", VA = "0x101534928")]
		public void Undecided()
		{
		}

		// Token: 0x06003DF1 RID: 15857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038A9")]
		[Address(RVA = "0x1015341B4", Offset = "0x15341B4", VA = "0x1015341B4")]
		public void Refresh()
		{
		}

		// Token: 0x06003DF2 RID: 15858 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038AA")]
		[Address(RVA = "0x101534998", Offset = "0x1534998", VA = "0x101534998")]
		private void OnClickCallback(int index)
		{
		}

		// Token: 0x06003DF3 RID: 15859 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038AB")]
		[Address(RVA = "0x101534A20", Offset = "0x1534A20", VA = "0x101534A20")]
		public RadioButton()
		{
		}

		// Token: 0x04004D32 RID: 19762
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003646")]
		[SerializeField]
		private RadioButtonParts m_MainButtonPrefab;

		// Token: 0x04004D33 RID: 19763
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003647")]
		[SerializeField]
		private RadioButtonParts m_LeftButtonPrefab;

		// Token: 0x04004D34 RID: 19764
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003648")]
		[SerializeField]
		private RadioButtonParts m_RightButtonPrefab;

		// Token: 0x04004D35 RID: 19765
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003649")]
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x04004D36 RID: 19766
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400364A")]
		private List<RadioButtonParts> m_ButtonList;

		// Token: 0x04004D37 RID: 19767
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400364B")]
		private int m_CurrentIndex;

		// Token: 0x04004D38 RID: 19768
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400364C")]
		public Action<int> OnChangeIndex;
	}
}
