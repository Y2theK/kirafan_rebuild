﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D33 RID: 3379
	[Token(Token = "0x200090D")]
	[StructLayout(3)]
	public class RadioButtonParts : MonoBehaviour
	{
		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x06003DF4 RID: 15860 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700045A")]
		public CustomButton Button
		{
			[Token(Token = "0x60038AC")]
			[Address(RVA = "0x101534A90", Offset = "0x1534A90", VA = "0x101534A90")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003DF5 RID: 15861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038AD")]
		[Address(RVA = "0x1015340F8", Offset = "0x15340F8", VA = "0x1015340F8")]
		public void SetWidth(float width)
		{
		}

		// Token: 0x06003DF6 RID: 15862 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038AE")]
		[Address(RVA = "0x101534170", Offset = "0x1534170", VA = "0x101534170")]
		public void SetLabel(int index, string text)
		{
		}

		// Token: 0x06003DF7 RID: 15863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038AF")]
		[Address(RVA = "0x101534934", Offset = "0x1534934", VA = "0x101534934")]
		public void SetCurrent(bool isCurrent)
		{
		}

		// Token: 0x06003DF8 RID: 15864 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B0")]
		[Address(RVA = "0x10153455C", Offset = "0x153455C", VA = "0x10153455C")]
		public void SetActive(bool isActive)
		{
		}

		// Token: 0x06003DF9 RID: 15865 RVA: 0x00018918 File Offset: 0x00016B18
		[Token(Token = "0x60038B1")]
		[Address(RVA = "0x101534630", Offset = "0x1534630", VA = "0x101534630")]
		public bool GetInteractable()
		{
			return default(bool);
		}

		// Token: 0x06003DFA RID: 15866 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B2")]
		[Address(RVA = "0x1015346EC", Offset = "0x15346EC", VA = "0x1015346EC")]
		public void SetInteractable(bool isInteractable)
		{
		}

		// Token: 0x06003DFB RID: 15867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B3")]
		[Address(RVA = "0x1015347B0", Offset = "0x15347B0", VA = "0x1015347B0")]
		public void SetEnable(bool isEnable)
		{
		}

		// Token: 0x06003DFC RID: 15868 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B4")]
		[Address(RVA = "0x1015343D0", Offset = "0x15343D0", VA = "0x1015343D0")]
		public void Destroy()
		{
		}

		// Token: 0x06003DFD RID: 15869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B5")]
		[Address(RVA = "0x101534A98", Offset = "0x1534A98", VA = "0x101534A98")]
		public void OnClickButton()
		{
		}

		// Token: 0x06003DFE RID: 15870 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B6")]
		[Address(RVA = "0x101534AEC", Offset = "0x1534AEC", VA = "0x101534AEC")]
		public RadioButtonParts()
		{
		}

		// Token: 0x04004D39 RID: 19769
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400364D")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04004D3A RID: 19770
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400364E")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04004D3B RID: 19771
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400364F")]
		[SerializeField]
		private RectTransform m_RectTransform;

		// Token: 0x04004D3C RID: 19772
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003650")]
		private int m_Index;

		// Token: 0x04004D3D RID: 19773
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003651")]
		public Action<int> OnClickCallback;
	}
}
