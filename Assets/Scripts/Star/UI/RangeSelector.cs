﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Star.UI
{
	// Token: 0x02000C9E RID: 3230
	[Token(Token = "0x20008A6")]
	[StructLayout(3)]
	public class RangeSelector : BRangeSelector
	{
		// Token: 0x06003AE7 RID: 15079 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035AA")]
		[Address(RVA = "0x101534AFC", Offset = "0x1534AFC", VA = "0x101534AFC")]
		public void Setup()
		{
		}

		// Token: 0x06003AE8 RID: 15080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035AB")]
		[Address(RVA = "0x101534E54", Offset = "0x1534E54", VA = "0x101534E54")]
		private void SetupButton(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003AE9 RID: 15081 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035AC")]
		[Address(RVA = "0x101534EE4", Offset = "0x1534EE4", VA = "0x101534EE4")]
		private void AddButtonHoldProcess(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003AEA RID: 15082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035AD")]
		[Address(RVA = "0x101535064", Offset = "0x1535064", VA = "0x101535064")]
		private void AddEventTriggerProcess(EventTrigger eventTrigger, EventTriggerType triggerType, BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003AEB RID: 15083 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60035AE")]
		[Address(RVA = "0x1015350B0", Offset = "0x15350B0", VA = "0x1015350B0")]
		protected UnityAction<BaseEventData> GetEventTriggerCallback(EventTriggerType triggerType, BRangeSelector.ePlusMinus plusMinus)
		{
			return null;
		}

		// Token: 0x06003AEC RID: 15084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035AF")]
		[Address(RVA = "0x101535144", Offset = "0x1535144", VA = "0x101535144")]
		public void Refresh()
		{
		}

		// Token: 0x06003AED RID: 15085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B0")]
		[Address(RVA = "0x101535184", Offset = "0x1535184", VA = "0x101535184")]
		public void SetMinimumValue(float value)
		{
		}

		// Token: 0x06003AEE RID: 15086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B1")]
		[Address(RVA = "0x1015351DC", Offset = "0x15351DC", VA = "0x1015351DC")]
		public void SetMaximumValue(float value)
		{
		}

		// Token: 0x06003AEF RID: 15087 RVA: 0x00018420 File Offset: 0x00016620
		[Token(Token = "0x60035B2")]
		[Address(RVA = "0x101535234", Offset = "0x1535234", VA = "0x101535234")]
		public float GetValue()
		{
			return 0f;
		}

		// Token: 0x06003AF0 RID: 15088 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B3")]
		[Address(RVA = "0x101535264", Offset = "0x1535264", VA = "0x101535264")]
		public void SetValue(float value)
		{
		}

		// Token: 0x06003AF1 RID: 15089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B4")]
		[Address(RVA = "0x10153533C", Offset = "0x153533C", VA = "0x10153533C")]
		private void SetValueProcess(BRangeSelector.eChangedValueFromType changedValueFromType, float value)
		{
		}

		// Token: 0x06003AF2 RID: 15090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B5")]
		[Address(RVA = "0x1015353F0", Offset = "0x15353F0", VA = "0x1015353F0")]
		public void SetInteractable(bool interactable)
		{
		}

		// Token: 0x06003AF3 RID: 15091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B6")]
		[Address(RVA = "0x101535490", Offset = "0x1535490", VA = "0x101535490")]
		private void OnSliderValueChangedCallBack(float value)
		{
		}

		// Token: 0x06003AF4 RID: 15092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B7")]
		[Address(RVA = "0x101535498", Offset = "0x1535498", VA = "0x101535498")]
		private void OnClickMinusButtonCallBack()
		{
		}

		// Token: 0x06003AF5 RID: 15093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B8")]
		[Address(RVA = "0x10153549C", Offset = "0x153549C", VA = "0x10153549C")]
		private void OnClickPlusButtonCallBack()
		{
		}

		// Token: 0x06003AF6 RID: 15094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035B9")]
		[Address(RVA = "0x1015354A0", Offset = "0x15354A0", VA = "0x1015354A0")]
		private void OnClickValueChangeButtonCallBack(float value)
		{
		}

		// Token: 0x06003AF7 RID: 15095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BA")]
		[Address(RVA = "0x1015354F4", Offset = "0x15354F4", VA = "0x1015354F4")]
		public void OnPointerDownMinusButtonCallback(BaseEventData eventData)
		{
		}

		// Token: 0x06003AF8 RID: 15096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BB")]
		[Address(RVA = "0x101535550", Offset = "0x1535550", VA = "0x101535550")]
		public void OnPointerDownPlusButtonCallback(BaseEventData eventData)
		{
		}

		// Token: 0x06003AF9 RID: 15097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BC")]
		[Address(RVA = "0x1015355A8", Offset = "0x15355A8", VA = "0x1015355A8")]
		public void OnPointerUpMinusButtonCallback(BaseEventData eventData)
		{
		}

		// Token: 0x06003AFA RID: 15098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BD")]
		[Address(RVA = "0x1015355B4", Offset = "0x15355B4", VA = "0x1015355B4")]
		public void OnPointerUpPlusButtonCallback(BaseEventData eventData)
		{
		}

		// Token: 0x06003AFB RID: 15099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BE")]
		[Address(RVA = "0x1015355C0", Offset = "0x15355C0", VA = "0x1015355C0", Slot = "4")]
		protected override void OnTimeOverHoldWait(BRangeSelector.ePlusMinus plusMinus)
		{
		}

		// Token: 0x06003AFC RID: 15100 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035BF")]
		[Address(RVA = "0x101535628", Offset = "0x1535628", VA = "0x101535628")]
		public RangeSelector()
		{
		}

		// Token: 0x040049AE RID: 18862
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40033D7")]
		private BRangeSelector.OnClickButtonPlusMinusCallbackPair m_OnClickButtonCallbackPair;

		// Token: 0x040049AF RID: 18863
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40033D8")]
		private Dictionary<EventTriggerType, BRangeSelector.EventTriggerPlusMinusCallbackPair> m_DictEventTriggerCallbackPair;
	}
}
