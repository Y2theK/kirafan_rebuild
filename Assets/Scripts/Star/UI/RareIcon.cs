﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D34 RID: 3380
	[Token(Token = "0x200090E")]
	[StructLayout(3)]
	public class RareIcon : UIBehaviour
	{
		// Token: 0x06003DFF RID: 15871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B7")]
		[Address(RVA = "0x101535630", Offset = "0x1535630", VA = "0x101535630")]
		public void Apply(eRare rare)
		{
		}

		// Token: 0x06003E00 RID: 15872 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B8")]
		[Address(RVA = "0x101535760", Offset = "0x1535760", VA = "0x101535760")]
		private void SetIconActive(bool active)
		{
		}

		// Token: 0x06003E01 RID: 15873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038B9")]
		[Address(RVA = "0x10153588C", Offset = "0x153588C", VA = "0x10153588C")]
		public RareIcon()
		{
		}

		// Token: 0x04004D3E RID: 19774
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003652")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124EA0", Offset = "0x124EA0")]
		private Image m_ImgIcon;

		// Token: 0x04004D3F RID: 19775
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003653")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124EEC", Offset = "0x124EEC")]
		private Sprite[] m_SprIcon;
	}
}
