﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D35 RID: 3381
	[Token(Token = "0x200090F")]
	[StructLayout(3)]
	public class RareStar : UIBehaviour, ILayoutGroup, ILayoutController
	{
		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x06003E02 RID: 15874 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700045B")]
		private RectTransform rectTransform
		{
			[Token(Token = "0x60038BA")]
			[Address(RVA = "0x101535894", Offset = "0x1535894", VA = "0x101535894")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003E03 RID: 15875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038BB")]
		[Address(RVA = "0x10152A060", Offset = "0x152A060", VA = "0x10152A060")]
		public void Apply(eRare rare)
		{
		}

		// Token: 0x06003E04 RID: 15876 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038BC")]
		[Address(RVA = "0x10153592C", Offset = "0x153592C", VA = "0x10153592C", Slot = "5")]
		protected override void OnEnable()
		{
		}

		// Token: 0x06003E05 RID: 15877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038BD")]
		[Address(RVA = "0x1015359E8", Offset = "0x15359E8", VA = "0x1015359E8", Slot = "7")]
		protected override void OnDisable()
		{
		}

		// Token: 0x06003E06 RID: 15878 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038BE")]
		[Address(RVA = "0x101535A68", Offset = "0x1535A68", VA = "0x101535A68", Slot = "10")]
		protected override void OnRectTransformDimensionsChange()
		{
		}

		// Token: 0x06003E07 RID: 15879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038BF")]
		[Address(RVA = "0x101535A6C", Offset = "0x1535A6C", VA = "0x101535A6C", Slot = "19")]
		protected virtual void OnTransformChildrenChanged()
		{
		}

		// Token: 0x06003E08 RID: 15880 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C0")]
		[Address(RVA = "0x101535A70", Offset = "0x1535A70", VA = "0x101535A70", Slot = "12")]
		protected override void OnTransformParentChanged()
		{
		}

		// Token: 0x06003E09 RID: 15881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C1")]
		[Address(RVA = "0x101535954", Offset = "0x1535954", VA = "0x101535954")]
		protected void SetDirty()
		{
		}

		// Token: 0x06003E0A RID: 15882 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C2")]
		[Address(RVA = "0x101535A74", Offset = "0x1535A74", VA = "0x101535A74", Slot = "20")]
		public virtual void SetLayoutHorizontal()
		{
		}

		// Token: 0x06003E0B RID: 15883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C3")]
		[Address(RVA = "0x101535A78", Offset = "0x1535A78", VA = "0x101535A78", Slot = "21")]
		public virtual void SetLayoutVertical()
		{
		}

		// Token: 0x06003E0C RID: 15884 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C4")]
		[Address(RVA = "0x101535A7C", Offset = "0x1535A7C", VA = "0x101535A7C")]
		private void Rebuild()
		{
		}

		// Token: 0x06003E0D RID: 15885 RVA: 0x00018930 File Offset: 0x00016B30
		[Token(Token = "0x60038C5")]
		[Address(RVA = "0x101535CB0", Offset = "0x1535CB0", VA = "0x101535CB0")]
		public float CalcWidth()
		{
			return 0f;
		}

		// Token: 0x06003E0E RID: 15886 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C6")]
		[Address(RVA = "0x101535F54", Offset = "0x1535F54", VA = "0x101535F54")]
		public RareStar()
		{
		}

		// Token: 0x04004D40 RID: 19776
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003654")]
		private RectTransform m_RectTransform;

		// Token: 0x04004D41 RID: 19777
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003655")]
		private Image[] m_Stars;

		// Token: 0x04004D42 RID: 19778
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003656")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100124F38", Offset = "0x124F38")]
		private bool m_FitParent;

		// Token: 0x04004D43 RID: 19779
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003657")]
		[NonSerialized]
		private RectTransform m_Rect;
	}
}
