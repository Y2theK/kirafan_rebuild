﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D36 RID: 3382
	[Token(Token = "0x2000910")]
	[StructLayout(3)]
	public class RecipeMaterial : MonoBehaviour
	{
		// Token: 0x06003E0F RID: 15887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C7")]
		[Address(RVA = "0x101535F5C", Offset = "0x1535F5C", VA = "0x101535F5C")]
		public void Apply(int itemID, int useNum, int haveNum)
		{
		}

		// Token: 0x06003E10 RID: 15888 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C8")]
		[Address(RVA = "0x1015361A0", Offset = "0x15361A0", VA = "0x1015361A0")]
		public void SetEnableRenderItemIcon(bool flg)
		{
		}

		// Token: 0x06003E11 RID: 15889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038C9")]
		[Address(RVA = "0x101536228", Offset = "0x1536228", VA = "0x101536228")]
		public void OnClickIconCallBack()
		{
		}

		// Token: 0x06003E12 RID: 15890 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CA")]
		[Address(RVA = "0x1015362AC", Offset = "0x15362AC", VA = "0x1015362AC")]
		public RecipeMaterial()
		{
		}

		// Token: 0x04004D44 RID: 19780
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003658")]
		[SerializeField]
		private PrefabCloner m_ItemIconCloner;

		// Token: 0x04004D45 RID: 19781
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003659")]
		[SerializeField]
		private Text m_UseText;

		// Token: 0x04004D46 RID: 19782
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400365A")]
		[SerializeField]
		private Text m_HaveText;

		// Token: 0x04004D47 RID: 19783
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400365B")]
		private int m_ItemID;
	}
}
