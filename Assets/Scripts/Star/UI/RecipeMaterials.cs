﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D37 RID: 3383
	[Token(Token = "0x2000911")]
	[StructLayout(3)]
	public class RecipeMaterials : MonoBehaviour
	{
		// Token: 0x06003E13 RID: 15891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CB")]
		[Address(RVA = "0x1015362BC", Offset = "0x15362BC", VA = "0x1015362BC")]
		public void Setup()
		{
		}

		// Token: 0x06003E14 RID: 15892 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CC")]
		[Address(RVA = "0x101536424", Offset = "0x1536424", VA = "0x101536424")]
		public void Apply(int[] itemIDs, int[] useNums)
		{
		}

		// Token: 0x06003E15 RID: 15893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CD")]
		[Address(RVA = "0x101536688", Offset = "0x1536688", VA = "0x101536688")]
		public void SetEnableRenderItemIcon(bool flg)
		{
		}

		// Token: 0x06003E16 RID: 15894 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CE")]
		[Address(RVA = "0x101536724", Offset = "0x1536724", VA = "0x101536724")]
		public RecipeMaterials()
		{
		}

		// Token: 0x04004D48 RID: 19784
		[Token(Token = "0x400365C")]
		private const int MATERIAL_MAX = 5;

		// Token: 0x04004D49 RID: 19785
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400365D")]
		[SerializeField]
		private RecipeMaterial m_MaterialPrefab;

		// Token: 0x04004D4A RID: 19786
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400365E")]
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x04004D4B RID: 19787
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400365F")]
		private RecipeMaterial[] m_Materials;

		// Token: 0x04004D4C RID: 19788
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003660")]
		private bool m_IsDoneSetup;
	}
}
