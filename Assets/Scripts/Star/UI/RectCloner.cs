﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D38 RID: 3384
	[Token(Token = "0x2000912")]
	[StructLayout(3)]
	public class RectCloner : MonoBehaviour
	{
		// Token: 0x06003E17 RID: 15895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038CF")]
		[Address(RVA = "0x101536784", Offset = "0x1536784", VA = "0x101536784")]
		private void Update()
		{
		}

		// Token: 0x06003E18 RID: 15896 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D0")]
		[Address(RVA = "0x10153687C", Offset = "0x153687C", VA = "0x10153687C")]
		public void Setup(int frames = 10)
		{
		}

		// Token: 0x06003E19 RID: 15897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D1")]
		[Address(RVA = "0x101536884", Offset = "0x1536884", VA = "0x101536884")]
		public RectCloner()
		{
		}

		// Token: 0x04004D4D RID: 19789
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003661")]
		[SerializeField]
		private RectTransform m_Source;

		// Token: 0x04004D4E RID: 19790
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003662")]
		[SerializeField]
		private RectTransform m_Destination;

		// Token: 0x04004D4F RID: 19791
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003663")]
		[SerializeField]
		private bool m_IsClonePosition;

		// Token: 0x04004D50 RID: 19792
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003664")]
		[SerializeField]
		private bool m_IsCloneSize;

		// Token: 0x04004D51 RID: 19793
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003665")]
		private int m_Frames;
	}
}
