﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D39 RID: 3385
	[Token(Token = "0x2000913")]
	[StructLayout(3)]
	public class ReplaceTextWithDB : MonoBehaviour
	{
		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x06003E1A RID: 15898 RVA: 0x00018948 File Offset: 0x00016B48
		[Token(Token = "0x1700045C")]
		public ReplaceTextWithDB.eDBType DBType
		{
			[Token(Token = "0x60038D2")]
			[Address(RVA = "0x10153688C", Offset = "0x153688C", VA = "0x10153688C")]
			get
			{
				return ReplaceTextWithDB.eDBType.None;
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x06003E1B RID: 15899 RVA: 0x00018960 File Offset: 0x00016B60
		[Token(Token = "0x1700045D")]
		public eText_CommonDB CommonIndex
		{
			[Token(Token = "0x60038D3")]
			[Address(RVA = "0x101536894", Offset = "0x1536894", VA = "0x101536894")]
			get
			{
				return eText_CommonDB.GameTitle;
			}
		}

		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x06003E1C RID: 15900 RVA: 0x00018978 File Offset: 0x00016B78
		[Token(Token = "0x1700045E")]
		public eText_MessageDB MessageIndex
		{
			[Token(Token = "0x60038D4")]
			[Address(RVA = "0x10153689C", Offset = "0x153689C", VA = "0x10153689C")]
			get
			{
				return eText_MessageDB.Dummy_0;
			}
		}

		// Token: 0x06003E1D RID: 15901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D5")]
		[Address(RVA = "0x1015368A4", Offset = "0x15368A4", VA = "0x1015368A4")]
		private void Start()
		{
		}

		// Token: 0x06003E1E RID: 15902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D6")]
		[Address(RVA = "0x101536900", Offset = "0x1536900", VA = "0x101536900")]
		private void Update()
		{
		}

		// Token: 0x06003E1F RID: 15903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D7")]
		[Address(RVA = "0x101536910", Offset = "0x1536910", VA = "0x101536910")]
		private void ReplaceInRuntime()
		{
		}

		// Token: 0x06003E20 RID: 15904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D8")]
		[Address(RVA = "0x101536A84", Offset = "0x1536A84", VA = "0x101536A84")]
		public ReplaceTextWithDB()
		{
		}

		// Token: 0x04004D52 RID: 19794
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003666")]
		[SerializeField]
		private ReplaceTextWithDB.eDBType m_DBType;

		// Token: 0x04004D53 RID: 19795
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003667")]
		[SerializeField]
		private eText_CommonDB m_CommonIndex;

		// Token: 0x04004D54 RID: 19796
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003668")]
		[SerializeField]
		private eText_MessageDB m_MessageIndex;

		// Token: 0x04004D55 RID: 19797
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003669")]
		private Text m_Text;

		// Token: 0x04004D56 RID: 19798
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400366A")]
		private bool m_IsReplaced;

		// Token: 0x02000D3A RID: 3386
		[Token(Token = "0x20010ED")]
		public enum eDBType
		{
			// Token: 0x04004D58 RID: 19800
			[Token(Token = "0x4006A3C")]
			None,
			// Token: 0x04004D59 RID: 19801
			[Token(Token = "0x4006A3D")]
			Common,
			// Token: 0x04004D5A RID: 19802
			[Token(Token = "0x4006A3E")]
			Message
		}
	}
}
