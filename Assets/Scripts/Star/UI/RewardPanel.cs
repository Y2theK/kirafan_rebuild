﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E01 RID: 3585
	[Token(Token = "0x200099F")]
	[StructLayout(3)]
	public class RewardPanel : MonoBehaviour
	{
		// Token: 0x06004236 RID: 16950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBA")]
		[Address(RVA = "0x101536A94", Offset = "0x1536A94", VA = "0x101536A94")]
		public void ApplyItem(int itemID, int num)
		{
		}

		// Token: 0x06004237 RID: 16951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBB")]
		[Address(RVA = "0x101536C0C", Offset = "0x1536C0C", VA = "0x101536C0C")]
		public void ApplyGem(long num)
		{
		}

		// Token: 0x06004238 RID: 16952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBC")]
		[Address(RVA = "0x101536D54", Offset = "0x1536D54", VA = "0x101536D54")]
		public void ApplyGold(long num)
		{
		}

		// Token: 0x06004239 RID: 16953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBD")]
		[Address(RVA = "0x101536E9C", Offset = "0x1536E9C", VA = "0x101536E9C")]
		public void ApplyKRR(long num)
		{
		}

		// Token: 0x0600423A RID: 16954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBE")]
		[Address(RVA = "0x101536FE4", Offset = "0x1536FE4", VA = "0x101536FE4")]
		public void ApplyRoomObj(int accesskey, long num)
		{
		}

		// Token: 0x0600423B RID: 16955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CBF")]
		[Address(RVA = "0x101537108", Offset = "0x1537108", VA = "0x101537108")]
		public void ApplyTownObj(int objID, long num)
		{
		}

		// Token: 0x0600423C RID: 16956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC0")]
		[Address(RVA = "0x101537270", Offset = "0x1537270", VA = "0x101537270")]
		public void ApplyChara(int charaID)
		{
		}

		// Token: 0x0600423D RID: 16957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC1")]
		[Address(RVA = "0x1015373B0", Offset = "0x15373B0", VA = "0x1015373B0")]
		public void ApplyWeapon(int weaponID, long num)
		{
		}

		// Token: 0x0600423E RID: 16958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC2")]
		[Address(RVA = "0x101537518", Offset = "0x1537518", VA = "0x101537518")]
		public RewardPanel()
		{
		}

		// Token: 0x04005235 RID: 21045
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A29")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04005236 RID: 21046
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A2A")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005237 RID: 21047
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A2B")]
		[SerializeField]
		private Text m_NumText;
	}
}
