﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.BackGround;
using WWWTypes;

namespace Star.UI
{
	// Token: 0x02000E02 RID: 3586
	[Token(Token = "0x20009A0")]
	[StructLayout(3)]
	public class RewardPlayer : SingletonMonoBehaviour<RewardPlayer>
	{
		// Token: 0x0600423F RID: 16959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC3")]
		[Address(RVA = "0x101537520", Offset = "0x1537520", VA = "0x101537520", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06004240 RID: 16960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC4")]
		[Address(RVA = "0x101537570", Offset = "0x1537570", VA = "0x101537570")]
		private void AddDisplayData(RewardPlayer.DisplayData data)
		{
		}

		// Token: 0x06004241 RID: 16961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC5")]
		[Address(RVA = "0x10153769C", Offset = "0x153769C", VA = "0x10153769C")]
		private void FlushDisplay()
		{
		}

		// Token: 0x06004242 RID: 16962 RVA: 0x00019458 File Offset: 0x00017658
		[Token(Token = "0x6003CC6")]
		[Address(RVA = "0x1015376FC", Offset = "0x15376FC", VA = "0x1015376FC")]
		private int GetDisplayStackNum()
		{
			return 0;
		}

		// Token: 0x06004243 RID: 16963 RVA: 0x00019470 File Offset: 0x00017670
		[Token(Token = "0x6003CC7")]
		[Address(RVA = "0x10153775C", Offset = "0x153775C", VA = "0x10153775C")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06004244 RID: 16964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC8")]
		[Address(RVA = "0x10153776C", Offset = "0x153776C", VA = "0x10153776C")]
		public void SetReserveOpen(bool flg)
		{
		}

		// Token: 0x06004245 RID: 16965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CC9")]
		[Address(RVA = "0x101537774", Offset = "0x1537774", VA = "0x101537774")]
		public void OpenQuest(BattleResult result, Action OnEnd, bool useBG = true)
		{
		}

		// Token: 0x06004246 RID: 16966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCA")]
		[Address(RVA = "0x101537EC0", Offset = "0x1537EC0", VA = "0x101537EC0")]
		public void OpenADV(RewardPlayer.ADVReward advReward, Action OnEnd)
		{
		}

		// Token: 0x06004247 RID: 16967 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCB")]
		[Address(RVA = "0x101537FC0", Offset = "0x1537FC0", VA = "0x101537FC0")]
		public void OpenTraining(RewardPlayer.TrainingReward trainingReward, Action OnEnd)
		{
		}

		// Token: 0x06004248 RID: 16968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCC")]
		[Address(RVA = "0x101537D6C", Offset = "0x1537D6C", VA = "0x101537D6C")]
		private void ExecOpen(Action OnEnd)
		{
		}

		// Token: 0x06004249 RID: 16969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCD")]
		[Address(RVA = "0x10153818C", Offset = "0x153818C", VA = "0x10153818C")]
		public void ForceHide()
		{
		}

		// Token: 0x0600424A RID: 16970 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCE")]
		[Address(RVA = "0x101538194", Offset = "0x1538194", VA = "0x101538194")]
		private void Update()
		{
		}

		// Token: 0x0600424B RID: 16971 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CCF")]
		[Address(RVA = "0x101539B2C", Offset = "0x1539B2C", VA = "0x101539B2C")]
		public RewardPlayer()
		{
		}

		// Token: 0x04005238 RID: 21048
		[Token(Token = "0x4003A2C")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.RewardUI;

		// Token: 0x04005239 RID: 21049
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003A2D")]
		private RewardPlayer.eStep m_Step;

		// Token: 0x0400523A RID: 21050
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A2E")]
		private RewardUI m_UI;

		// Token: 0x0400523B RID: 21051
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A2F")]
		private bool m_IsReserveOpen;

		// Token: 0x0400523C RID: 21052
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003A30")]
		private Action m_OnEndCallBack;

		// Token: 0x0400523D RID: 21053
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003A31")]
		private bool m_UseBG;

		// Token: 0x0400523E RID: 21054
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003A32")]
		private UIBackGroundManager.eBackGroundID m_BgID;

		// Token: 0x0400523F RID: 21055
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A33")]
		public List<RewardPlayer.DisplayData> m_DisplayStack;

		// Token: 0x02000E03 RID: 3587
		[Token(Token = "0x2001128")]
		private enum eStep
		{
			// Token: 0x04005241 RID: 21057
			[Token(Token = "0x4006B56")]
			Hide,
			// Token: 0x04005242 RID: 21058
			[Token(Token = "0x4006B57")]
			WaitFadeOut,
			// Token: 0x04005243 RID: 21059
			[Token(Token = "0x4006B58")]
			LoadBG,
			// Token: 0x04005244 RID: 21060
			[Token(Token = "0x4006B59")]
			LoadWaitBG,
			// Token: 0x04005245 RID: 21061
			[Token(Token = "0x4006B5A")]
			LoadUI,
			// Token: 0x04005246 RID: 21062
			[Token(Token = "0x4006B5B")]
			LoadWaitUI,
			// Token: 0x04005247 RID: 21063
			[Token(Token = "0x4006B5C")]
			SetupAndPlayIn,
			// Token: 0x04005248 RID: 21064
			[Token(Token = "0x4006B5D")]
			Main,
			// Token: 0x04005249 RID: 21065
			[Token(Token = "0x4006B5E")]
			WaitFinalizeFadeOut,
			// Token: 0x0400524A RID: 21066
			[Token(Token = "0x4006B5F")]
			UnloadWait
		}

		// Token: 0x02000E04 RID: 3588
		[Token(Token = "0x2001129")]
		public enum eRewardType
		{
			// Token: 0x0400524C RID: 21068
			[Token(Token = "0x4006B61")]
			None,
			// Token: 0x0400524D RID: 21069
			[Token(Token = "0x4006B62")]
			FirstClear,
			// Token: 0x0400524E RID: 21070
			[Token(Token = "0x4006B63")]
			GroupClear,
			// Token: 0x0400524F RID: 21071
			[Token(Token = "0x4006B64")]
			Complete,
			// Token: 0x04005250 RID: 21072
			[Token(Token = "0x4006B65")]
			ADV,
			// Token: 0x04005251 RID: 21073
			[Token(Token = "0x4006B66")]
			Training
		}

		// Token: 0x02000E05 RID: 3589
		[Token(Token = "0x200112A")]
		public class ADVReward
		{
			// Token: 0x0600424C RID: 16972 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600622F")]
			[Address(RVA = "0x101539BB4", Offset = "0x1539BB4", VA = "0x101539BB4")]
			private ADVReward()
			{
			}

			// Token: 0x0600424D RID: 16973 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006230")]
			[Address(RVA = "0x101539BBC", Offset = "0x1539BBC", VA = "0x101539BBC")]
			public ADVReward(List<eCharaNamedType> namedList, int advGem)
			{
			}

			// Token: 0x04005252 RID: 21074
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B67")]
			public List<eCharaNamedType> m_NamedList;

			// Token: 0x04005253 RID: 21075
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B68")]
			public int m_AdvGem;
		}

		// Token: 0x02000E06 RID: 3590
		[Token(Token = "0x200112B")]
		public class TrainingReward
		{
			// Token: 0x0600424E RID: 16974 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006231")]
			[Address(RVA = "0x101539BF8", Offset = "0x1539BF8", VA = "0x101539BF8")]
			private TrainingReward()
			{
			}

			// Token: 0x0600424F RID: 16975 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006232")]
			[Address(RVA = "0x101539C00", Offset = "0x1539C00", VA = "0x101539C00")]
			public TrainingReward(int trainingGem, string trainingName)
			{
			}

			// Token: 0x04005254 RID: 21076
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B69")]
			public string m_TrainingName;

			// Token: 0x04005255 RID: 21077
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B6A")]
			public int m_TrainingGem;
		}

		// Token: 0x02000E07 RID: 3591
		[Token(Token = "0x200112C")]
		public class DisplayData
		{
			// Token: 0x06004250 RID: 16976 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006233")]
			[Address(RVA = "0x101537D28", Offset = "0x1537D28", VA = "0x101537D28")]
			public DisplayData(RewardPlayer.eRewardType type, QuestReward reward, string text)
			{
			}

			// Token: 0x06004251 RID: 16977 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006234")]
			[Address(RVA = "0x101537F88", Offset = "0x1537F88", VA = "0x101537F88")]
			public DisplayData(RewardPlayer.ADVReward advReward)
			{
			}

			// Token: 0x06004252 RID: 16978 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006235")]
			[Address(RVA = "0x101538148", Offset = "0x1538148", VA = "0x101538148")]
			public DisplayData(RewardPlayer.TrainingReward trainingReward, string text)
			{
			}

			// Token: 0x06004253 RID: 16979 RVA: 0x00019488 File Offset: 0x00017688
			[Token(Token = "0x6006236")]
			[Address(RVA = "0x101537608", Offset = "0x1537608", VA = "0x101537608")]
			public bool IsEmpty()
			{
				return default(bool);
			}

			// Token: 0x04005256 RID: 21078
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006B6B")]
			public RewardPlayer.eRewardType m_RewardType;

			// Token: 0x04005257 RID: 21079
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006B6C")]
			public QuestReward m_Reward;

			// Token: 0x04005258 RID: 21080
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006B6D")]
			public string m_Text;

			// Token: 0x04005259 RID: 21081
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006B6E")]
			public RewardPlayer.ADVReward m_ADVReward;

			// Token: 0x0400525A RID: 21082
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006B6F")]
			public RewardPlayer.TrainingReward m_TrainingReward;
		}
	}
}
