﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D3B RID: 3387
	[Token(Token = "0x2000914")]
	[StructLayout(3)]
	public class RewardSetDisplayGroup : UIGroup
	{
		// Token: 0x06003E21 RID: 15905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038D9")]
		[Address(RVA = "0x1015248F0", Offset = "0x15248F0", VA = "0x1015248F0")]
		public void Open(List<RewardSet> list)
		{
		}

		// Token: 0x06003E22 RID: 15906 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60038DA")]
		[Address(RVA = "0x101539C3C", Offset = "0x1539C3C", VA = "0x101539C3C")]
		private string GetResultString(List<RewardSet> list)
		{
			return null;
		}

		// Token: 0x06003E23 RID: 15907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038DB")]
		[Address(RVA = "0x101539D74", Offset = "0x1539D74", VA = "0x101539D74")]
		public RewardSetDisplayGroup()
		{
		}

		// Token: 0x04004D5B RID: 19803
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400366B")]
		[SerializeField]
		private Text m_ResultText;
	}
}
