﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E08 RID: 3592
	[Token(Token = "0x20009A1")]
	[StructLayout(3)]
	public class RewardUI : MenuUIBase
	{
		// Token: 0x06004254 RID: 16980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD0")]
		[Address(RVA = "0x1015387B0", Offset = "0x15387B0", VA = "0x1015387B0")]
		public void ApplyDisplayData(RewardPlayer.DisplayData displayData)
		{
		}

		// Token: 0x06004255 RID: 16981 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD1")]
		[Address(RVA = "0x101539D7C", Offset = "0x1539D7C", VA = "0x101539D7C")]
		private void Update()
		{
		}

		// Token: 0x06004256 RID: 16982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD2")]
		[Address(RVA = "0x101539F4C", Offset = "0x1539F4C", VA = "0x101539F4C")]
		private void ChangeStep(RewardUI.eStep step)
		{
		}

		// Token: 0x06004257 RID: 16983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD3")]
		[Address(RVA = "0x10153A074", Offset = "0x153A074", VA = "0x10153A074", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004258 RID: 16984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD4")]
		[Address(RVA = "0x10153A130", Offset = "0x153A130", VA = "0x10153A130", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004259 RID: 16985 RVA: 0x000194A0 File Offset: 0x000176A0
		[Token(Token = "0x6003CD5")]
		[Address(RVA = "0x10153A1EC", Offset = "0x153A1EC", VA = "0x10153A1EC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600425A RID: 16986 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD6")]
		[Address(RVA = "0x10153A1FC", Offset = "0x153A1FC", VA = "0x10153A1FC")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x0600425B RID: 16987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD7")]
		[Address(RVA = "0x10153A208", Offset = "0x153A208", VA = "0x10153A208")]
		public RewardUI()
		{
		}

		// Token: 0x0400525B RID: 21083
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A34")]
		public readonly float[] WINDOW_WIDTH;

		// Token: 0x0400525C RID: 21084
		[Token(Token = "0x4003A35")]
		public const float WINDOW_HEIGHT_EXTEND = 40f;

		// Token: 0x0400525D RID: 21085
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A36")]
		private RewardUI.eStep m_Step;

		// Token: 0x0400525E RID: 21086
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003A37")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400525F RID: 21087
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A38")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005260 RID: 21088
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A39")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04005261 RID: 21089
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A3A")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04005262 RID: 21090
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A3B")]
		[SerializeField]
		private Text m_NoteText;

		// Token: 0x04005263 RID: 21091
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003A3C")]
		[SerializeField]
		private RewardPanel[] m_RewardPanels;

		// Token: 0x04005264 RID: 21092
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003A3D")]
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04005265 RID: 21093
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003A3E")]
		private float m_DefaultHeight;

		// Token: 0x02000E09 RID: 3593
		[Token(Token = "0x200112D")]
		private enum eStep
		{
			// Token: 0x04005267 RID: 21095
			[Token(Token = "0x4006B71")]
			Hide,
			// Token: 0x04005268 RID: 21096
			[Token(Token = "0x4006B72")]
			In,
			// Token: 0x04005269 RID: 21097
			[Token(Token = "0x4006B73")]
			Idle,
			// Token: 0x0400526A RID: 21098
			[Token(Token = "0x4006B74")]
			Out,
			// Token: 0x0400526B RID: 21099
			[Token(Token = "0x4006B75")]
			End
		}
	}
}
