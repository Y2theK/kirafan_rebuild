﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000E71 RID: 3697
	[Token(Token = "0x20009D7")]
	[StructLayout(3)]
	public class ADVButton : MonoBehaviour
	{
		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x060044FB RID: 17659 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004A2")]
		public List<int> ScheduleAdvIDs
		{
			[Token(Token = "0x6003F67")]
			[Address(RVA = "0x10153A288", Offset = "0x153A288", VA = "0x10153A288")]
			get
			{
				return null;
			}
		}

		// Token: 0x060044FC RID: 17660 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F68")]
		[Address(RVA = "0x10153A290", Offset = "0x153A290", VA = "0x10153A290")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x060044FD RID: 17661 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F69")]
		[Address(RVA = "0x10153A34C", Offset = "0x153A34C", VA = "0x10153A34C")]
		public void SetScheduleAdvIDs(List<int> scheduleAdvIDs)
		{
		}

		// Token: 0x060044FE RID: 17662 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F6A")]
		[Address(RVA = "0x10153A358", Offset = "0x153A358", VA = "0x10153A358")]
		public ADVButton()
		{
		}

		// Token: 0x040055E8 RID: 21992
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C81")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040055E9 RID: 21993
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C82")]
		private List<int> m_ScheduleAdvIDs;

		// Token: 0x040055EA RID: 21994
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003C83")]
		private bool m_IsEnableRender;
	}
}
