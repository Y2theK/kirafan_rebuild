﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E72 RID: 3698
	[Token(Token = "0x20009D8")]
	[StructLayout(3)]
	public class CharaTweet : MonoBehaviour
	{
		// Token: 0x060044FF RID: 17663 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F6B")]
		[Address(RVA = "0x10153A368", Offset = "0x153A368", VA = "0x10153A368")]
		public void Open(string text)
		{
		}

		// Token: 0x06004500 RID: 17664 RVA: 0x00019AB8 File Offset: 0x00017CB8
		[Token(Token = "0x6003F6C")]
		[Address(RVA = "0x10153A3DC", Offset = "0x153A3DC", VA = "0x10153A3DC")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06004501 RID: 17665 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F6D")]
		[Address(RVA = "0x10153A410", Offset = "0x153A410", VA = "0x10153A410")]
		public void Close()
		{
		}

		// Token: 0x06004502 RID: 17666 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F6E")]
		[Address(RVA = "0x10153A448", Offset = "0x153A448", VA = "0x10153A448")]
		public CharaTweet()
		{
		}

		// Token: 0x040055EB RID: 21995
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C84")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x040055EC RID: 21996
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C85")]
		private RectTransform m_Transform;
	}
}
