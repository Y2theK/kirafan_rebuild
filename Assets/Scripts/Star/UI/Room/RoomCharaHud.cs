﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000E73 RID: 3699
	[Token(Token = "0x20009D9")]
	[StructLayout(3)]
	public class RoomCharaHud : MonoBehaviour
	{
		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x06004503 RID: 17667 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004A3")]
		public RectTransform CacheTransform
		{
			[Token(Token = "0x6003F6F")]
			[Address(RVA = "0x10153A450", Offset = "0x153A450", VA = "0x10153A450")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004504 RID: 17668 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F70")]
		[Address(RVA = "0x10153A458", Offset = "0x153A458", VA = "0x10153A458")]
		private void Awake()
		{
		}

		// Token: 0x06004505 RID: 17669 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003F71")]
		[Address(RVA = "0x10153A4FC", Offset = "0x153A4FC", VA = "0x10153A4FC")]
		public CharaTweet GetCharaTweet()
		{
			return null;
		}

		// Token: 0x06004506 RID: 17670 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F72")]
		[Address(RVA = "0x10153A504", Offset = "0x153A504", VA = "0x10153A504")]
		public RoomCharaHud()
		{
		}

		// Token: 0x040055ED RID: 21997
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C86")]
		[SerializeField]
		private CharaTweet m_CharaTweet;

		// Token: 0x040055EE RID: 21998
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C87")]
		private RectTransform m_Transform;
	}
}
