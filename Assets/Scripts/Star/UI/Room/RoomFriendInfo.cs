﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E74 RID: 3700
	[Token(Token = "0x20009DA")]
	[StructLayout(3)]
	public class RoomFriendInfo : MonoBehaviour
	{
		// Token: 0x06004507 RID: 17671 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F73")]
		[Address(RVA = "0x10153A50C", Offset = "0x153A50C", VA = "0x10153A50C")]
		public void Apply(long playerID)
		{
		}

		// Token: 0x06004508 RID: 17672 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F74")]
		[Address(RVA = "0x10153A640", Offset = "0x153A640", VA = "0x10153A640")]
		public RoomFriendInfo()
		{
		}

		// Token: 0x040055EF RID: 21999
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C88")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x040055F0 RID: 22000
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C89")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040055F1 RID: 22001
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003C8A")]
		[SerializeField]
		private ImageNumbers m_Rank;
	}
}
