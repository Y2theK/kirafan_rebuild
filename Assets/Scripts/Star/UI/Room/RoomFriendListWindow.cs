﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Menu;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000E75 RID: 3701
	[Token(Token = "0x20009DB")]
	[StructLayout(3)]
	public class RoomFriendListWindow : UIGroup
	{
		// Token: 0x14000090 RID: 144
		// (add) Token: 0x06004509 RID: 17673 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600450A RID: 17674 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000090")]
		public event Action OnRoomChange
		{
			[Token(Token = "0x6003F75")]
			[Address(RVA = "0x10153A648", Offset = "0x153A648", VA = "0x10153A648")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003F76")]
			[Address(RVA = "0x10153A754", Offset = "0x153A754", VA = "0x10153A754")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600450B RID: 17675 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F77")]
		[Address(RVA = "0x10153A860", Offset = "0x153A860", VA = "0x10153A860")]
		public void Open(bool request)
		{
		}

		// Token: 0x0600450C RID: 17676 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F78")]
		[Address(RVA = "0x10153A950", Offset = "0x153A950", VA = "0x10153A950")]
		private void OnResponse_GetAll(bool isError)
		{
		}

		// Token: 0x0600450D RID: 17677 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F79")]
		[Address(RVA = "0x10153ABF0", Offset = "0x153ABF0", VA = "0x10153ABF0")]
		private void OnActionResponse(MenuFriendListItem.eAction action)
		{
		}

		// Token: 0x0600450E RID: 17678 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F7A")]
		[Address(RVA = "0x10153B140", Offset = "0x153B140", VA = "0x10153B140")]
		public RoomFriendListWindow()
		{
		}

		// Token: 0x040055F2 RID: 22002
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C8B")]
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
