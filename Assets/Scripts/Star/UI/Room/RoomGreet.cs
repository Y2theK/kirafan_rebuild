﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E77 RID: 3703
	[Token(Token = "0x20009DC")]
	[StructLayout(3)]
	public class RoomGreet : UIGroup
	{
		// Token: 0x06004512 RID: 17682 RVA: 0x00019AD0 File Offset: 0x00017CD0
		[Token(Token = "0x6003F7B")]
		[Address(RVA = "0x10153B21C", Offset = "0x153B21C", VA = "0x10153B21C")]
		public bool IsEnableOpen()
		{
			return default(bool);
		}

		// Token: 0x06004513 RID: 17683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F7C")]
		[Address(RVA = "0x10153B3E8", Offset = "0x153B3E8", VA = "0x10153B3E8")]
		public void Open(List<long> charaMngIDs, RoomGreet.eCategory category)
		{
		}

		// Token: 0x06004514 RID: 17684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F7D")]
		[Address(RVA = "0x10153B6A8", Offset = "0x153B6A8", VA = "0x10153B6A8")]
		private void Prepare(int charaID)
		{
		}

		// Token: 0x06004515 RID: 17685 RVA: 0x00019AE8 File Offset: 0x00017CE8
		[Token(Token = "0x6003F7E")]
		[Address(RVA = "0x10153B6B4", Offset = "0x153B6B4", VA = "0x10153B6B4")]
		private bool UpdatePrepare()
		{
			return default(bool);
		}

		// Token: 0x06004516 RID: 17686 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F7F")]
		[Address(RVA = "0x10153B814", Offset = "0x153B814", VA = "0x10153B814", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004517 RID: 17687 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F80")]
		[Address(RVA = "0x10153B820", Offset = "0x153B820", VA = "0x10153B820", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004518 RID: 17688 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F81")]
		[Address(RVA = "0x10153B8A4", Offset = "0x153B8A4", VA = "0x10153B8A4")]
		public RoomGreet()
		{
		}

		// Token: 0x040055F6 RID: 22006
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C8D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100128DFC", Offset = "0x128DFC")]
		private float m_DisplayTime;

		// Token: 0x040055F7 RID: 22007
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C8E")]
		[SerializeField]
		private List<GameObject> m_CharaIconList;

		// Token: 0x040055F8 RID: 22008
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003C8F")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x040055F9 RID: 22009
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003C90")]
		[SerializeField]
		private RectTransform m_RayCastCheckRect;

		// Token: 0x040055FA RID: 22010
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003C91")]
		private int m_CharaID;

		// Token: 0x040055FB RID: 22011
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4003C92")]
		private RoomGreet.eCategory m_Category;

		// Token: 0x040055FC RID: 22012
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003C93")]
		private string m_TweetText;

		// Token: 0x040055FD RID: 22013
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003C94")]
		private bool m_IsOpen;

		// Token: 0x040055FE RID: 22014
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x4003C95")]
		private float m_WaitTime;

		// Token: 0x040055FF RID: 22015
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003C96")]
		private bool m_IsDonePrepare;

		// Token: 0x02000E78 RID: 3704
		[Token(Token = "0x2001161")]
		public enum eCategory
		{
			// Token: 0x04005601 RID: 22017
			[Token(Token = "0x4006CB3")]
			None,
			// Token: 0x04005602 RID: 22018
			[Token(Token = "0x4006CB4")]
			Morning,
			// Token: 0x04005603 RID: 22019
			[Token(Token = "0x4006CB5")]
			Night,
			// Token: 0x04005604 RID: 22020
			[Token(Token = "0x4006CB6")]
			WakeUp,
			// Token: 0x04005605 RID: 22021
			[Token(Token = "0x4006CB7")]
			GoTown,
			// Token: 0x04005606 RID: 22022
			[Token(Token = "0x4006CB8")]
			GoHome,
			// Token: 0x04005607 RID: 22023
			[Token(Token = "0x4006CB9")]
			Sleep,
			// Token: 0x04005608 RID: 22024
			[Token(Token = "0x4006CBA")]
			Visit
		}
	}
}
