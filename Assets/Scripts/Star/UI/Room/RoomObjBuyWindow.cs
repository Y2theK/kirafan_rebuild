﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E7A RID: 3706
	[Token(Token = "0x20009DE")]
	[StructLayout(3)]
	public class RoomObjBuyWindow : UIGroup
	{
		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x0600451A RID: 17690 RVA: 0x00019B00 File Offset: 0x00017D00
		[Token(Token = "0x170004A4")]
		public RoomObjBuyWindow.eButton SelectButton
		{
			[Token(Token = "0x6003F83")]
			[Address(RVA = "0x10153B92C", Offset = "0x153B92C", VA = "0x10153B92C")]
			get
			{
				return RoomObjBuyWindow.eButton.Yes;
			}
		}

		// Token: 0x0600451B RID: 17691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F84")]
		[Address(RVA = "0x10153B934", Offset = "0x153B934", VA = "0x10153B934")]
		public void Open(RoomObjShopList.RoomShopObjData shopObjData, RoomObjBuyWindow.eMode mode, UnityAction onClose)
		{
		}

		// Token: 0x0600451C RID: 17692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F85")]
		[Address(RVA = "0x10153B9B8", Offset = "0x153B9B8", VA = "0x10153B9B8")]
		public void Open(eRoomObjectCategory category, int objID, long amount, RoomObjBuyWindow.eMode mode, UnityAction OnClose)
		{
		}

		// Token: 0x0600451D RID: 17693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F86")]
		[Address(RVA = "0x10153C6D8", Offset = "0x153C6D8", VA = "0x10153C6D8", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600451E RID: 17694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F87")]
		[Address(RVA = "0x10153C880", Offset = "0x153C880", VA = "0x10153C880", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x0600451F RID: 17695 RVA: 0x00019B18 File Offset: 0x00017D18
		[Token(Token = "0x6003F88")]
		[Address(RVA = "0x10153C8BC", Offset = "0x153C8BC", VA = "0x10153C8BC")]
		public int GetSelectNum()
		{
			return 0;
		}

		// Token: 0x06004520 RID: 17696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F89")]
		[Address(RVA = "0x10153C8EC", Offset = "0x153C8EC", VA = "0x10153C8EC")]
		public void OnChangeCurrentNumCallBack()
		{
		}

		// Token: 0x06004521 RID: 17697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8A")]
		[Address(RVA = "0x10153C520", Offset = "0x153C520", VA = "0x10153C520")]
		private void ApplyUseGold(long priceChange)
		{
		}

		// Token: 0x06004522 RID: 17698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8B")]
		[Address(RVA = "0x10153C91C", Offset = "0x153C91C", VA = "0x10153C91C")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x06004523 RID: 17699 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8C")]
		[Address(RVA = "0x10153CA00", Offset = "0x153CA00", VA = "0x10153CA00")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x06004524 RID: 17700 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8D")]
		[Address(RVA = "0x10153CA14", Offset = "0x153CA14", VA = "0x10153CA14")]
		public RoomObjBuyWindow()
		{
		}

		// Token: 0x04005609 RID: 22025
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C97")]
		[SerializeField]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x0400560A RID: 22026
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C98")]
		[SerializeField]
		private Text m_ObjNameTextObj;

		// Token: 0x0400560B RID: 22027
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003C99")]
		[SerializeField]
		private Text m_BeforeGoldObj;

		// Token: 0x0400560C RID: 22028
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003C9A")]
		[SerializeField]
		private Text m_AfterGoldObj;

		// Token: 0x0400560D RID: 22029
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003C9B")]
		[SerializeField]
		private Text m_ConfirmTextBuy;

		// Token: 0x0400560E RID: 22030
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003C9C")]
		[SerializeField]
		private Text m_ConfirmTextPlace;

		// Token: 0x0400560F RID: 22031
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003C9D")]
		[SerializeField]
		private GameObject m_NumSelectObj;

		// Token: 0x04005610 RID: 22032
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003C9E")]
		[SerializeField]
		private NumberSelect m_NumSelect;

		// Token: 0x04005611 RID: 22033
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003C9F")]
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x04005612 RID: 22034
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003CA0")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x04005613 RID: 22035
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003CA1")]
		[SerializeField]
		private GameObject m_BuyObject;

		// Token: 0x04005614 RID: 22036
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003CA2")]
		[SerializeField]
		private GameObject m_PlaceObject;

		// Token: 0x04005615 RID: 22037
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003CA3")]
		private RoomObjBuyWindow.eMode m_Mode;

		// Token: 0x04005616 RID: 22038
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003CA4")]
		private long m_Amount;

		// Token: 0x04005617 RID: 22039
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003CA5")]
		private UnityAction m_CloseAction;

		// Token: 0x04005618 RID: 22040
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003CA6")]
		private RoomObjBuyWindow.eButton m_SelectButton;

		// Token: 0x04005619 RID: 22041
		[Cpp2IlInjected.FieldOffset(Offset = "0x104")]
		[Token(Token = "0x4003CA7")]
		private bool m_IsReserveAttach;

		// Token: 0x02000E7B RID: 3707
		[Token(Token = "0x2001162")]
		public enum eMode
		{
			// Token: 0x0400561B RID: 22043
			[Token(Token = "0x4006CBC")]
			BuySendStockNumSelect,
			// Token: 0x0400561C RID: 22044
			[Token(Token = "0x4006CBD")]
			BuyPlaceNumSelect,
			// Token: 0x0400561D RID: 22045
			[Token(Token = "0x4006CBE")]
			SellNumSelect,
			// Token: 0x0400561E RID: 22046
			[Token(Token = "0x4006CBF")]
			PlaceNumSelect
		}

		// Token: 0x02000E7C RID: 3708
		[Token(Token = "0x2001163")]
		public enum eButton
		{
			// Token: 0x04005620 RID: 22048
			[Token(Token = "0x4006CC1")]
			Invalid = -1,
			// Token: 0x04005621 RID: 22049
			[Token(Token = "0x4006CC2")]
			Yes,
			// Token: 0x04005622 RID: 22050
			[Token(Token = "0x4006CC3")]
			No
		}
	}
}
