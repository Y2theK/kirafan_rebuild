﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E7D RID: 3709
	[Token(Token = "0x20009DF")]
	[StructLayout(3)]
	public class RoomObjCancelSendStockWindow : UIGroup
	{
		// Token: 0x06004525 RID: 17701 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8E")]
		[Address(RVA = "0x10153CA24", Offset = "0x153CA24", VA = "0x10153CA24")]
		public void Open(RoomObjCancelSendStockWindow.eType type, eRoomObjectCategory category, int objID, int amount, UnityAction OnClose)
		{
		}

		// Token: 0x06004526 RID: 17702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F8F")]
		[Address(RVA = "0x10153CE8C", Offset = "0x153CE8C", VA = "0x10153CE8C", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x06004527 RID: 17703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F90")]
		[Address(RVA = "0x10153CEC8", Offset = "0x153CEC8", VA = "0x10153CEC8")]
		public void OnClickFooterButtonCallBack()
		{
		}

		// Token: 0x06004528 RID: 17704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F91")]
		[Address(RVA = "0x10153CED4", Offset = "0x153CED4", VA = "0x10153CED4")]
		public RoomObjCancelSendStockWindow()
		{
		}

		// Token: 0x04005623 RID: 22051
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CA8")]
		[SerializeField]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x04005624 RID: 22052
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CA9")]
		[SerializeField]
		private Text m_ObjNameTextObj;

		// Token: 0x04005625 RID: 22053
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CAA")]
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04005626 RID: 22054
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003CAB")]
		[SerializeField]
		private Text m_NumberText;

		// Token: 0x04005627 RID: 22055
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003CAC")]
		private UnityAction m_CloseAction;

		// Token: 0x02000E7E RID: 3710
		[Token(Token = "0x2001164")]
		public enum eType
		{
			// Token: 0x04005629 RID: 22057
			[Token(Token = "0x4006CC5")]
			SendStock,
			// Token: 0x0400562A RID: 22058
			[Token(Token = "0x4006CC6")]
			CancelSendStock,
			// Token: 0x0400562B RID: 22059
			[Token(Token = "0x4006CC7")]
			CancelBuy
		}
	}
}
