﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E7F RID: 3711
	[Token(Token = "0x20009E0")]
	[StructLayout(3)]
	public class RoomObjConfirmWindow : UIGroup
	{
		// Token: 0x06004529 RID: 17705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F92")]
		[Address(RVA = "0x10153CEDC", Offset = "0x153CEDC", VA = "0x10153CEDC")]
		public void Open(eRoomObjectCategory category, int objID, RoomObjConfirmWindow.eMode mode, bool enableNextPlacement, UnityAction OnClose)
		{
		}

		// Token: 0x0600452A RID: 17706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F93")]
		[Address(RVA = "0x10153D4F4", Offset = "0x153D4F4", VA = "0x10153D4F4", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x0600452B RID: 17707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F94")]
		[Address(RVA = "0x10153D530", Offset = "0x153D530", VA = "0x10153D530")]
		public void OnClickFooterButtonCallBack()
		{
		}

		// Token: 0x0600452C RID: 17708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F95")]
		[Address(RVA = "0x10153D53C", Offset = "0x153D53C", VA = "0x10153D53C")]
		public RoomObjConfirmWindow()
		{
		}

		// Token: 0x0400562C RID: 22060
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CAD")]
		[SerializeField]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x0400562D RID: 22061
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CAE")]
		[SerializeField]
		private Text m_ObjNameTextObj;

		// Token: 0x0400562E RID: 22062
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CAF")]
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x0400562F RID: 22063
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003CB0")]
		private UnityAction m_CloseAction;

		// Token: 0x02000E80 RID: 3712
		[Token(Token = "0x2001165")]
		public enum eMode
		{
			// Token: 0x04005631 RID: 22065
			[Token(Token = "0x4006CC9")]
			Store,
			// Token: 0x04005632 RID: 22066
			[Token(Token = "0x4006CCA")]
			BuyAndPlacement,
			// Token: 0x04005633 RID: 22067
			[Token(Token = "0x4006CCB")]
			BuyAndStock,
			// Token: 0x04005634 RID: 22068
			[Token(Token = "0x4006CCC")]
			Sell
		}
	}
}
