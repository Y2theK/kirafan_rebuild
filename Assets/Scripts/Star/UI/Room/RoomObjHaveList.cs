﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Room
{
	// Token: 0x02000E81 RID: 3713
	[Token(Token = "0x20009E1")]
	[StructLayout(3)]
	public class RoomObjHaveList
	{
		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x0600452D RID: 17709 RVA: 0x00019B30 File Offset: 0x00017D30
		[Token(Token = "0x170004A5")]
		public bool IsAvailable
		{
			[Token(Token = "0x6003F96")]
			[Address(RVA = "0x10153D544", Offset = "0x153D544", VA = "0x10153D544")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x0600452E RID: 17710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F97")]
		[Address(RVA = "0x10153D54C", Offset = "0x153D54C", VA = "0x10153D54C")]
		public void BufferingObjList()
		{
		}

		// Token: 0x0600452F RID: 17711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F98")]
		[Address(RVA = "0x10153D674", Offset = "0x153D674", VA = "0x10153D674")]
		public void Open(bool useFilter)
		{
		}

		// Token: 0x06004530 RID: 17712 RVA: 0x00019B48 File Offset: 0x00017D48
		[Token(Token = "0x6003F99")]
		[Address(RVA = "0x10153D684", Offset = "0x153D684", VA = "0x10153D684")]
		public bool UpdateOpen()
		{
			return default(bool);
		}

		// Token: 0x06004531 RID: 17713 RVA: 0x00019B60 File Offset: 0x00017D60
		[Token(Token = "0x6003F9A")]
		[Address(RVA = "0x10153DAD0", Offset = "0x153DAD0", VA = "0x10153DAD0")]
		private int Comp(RoomObjHaveList.RoomObj x, RoomObjHaveList.RoomObj y)
		{
			return 0;
		}

		// Token: 0x06004532 RID: 17714 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003F9B")]
		[Address(RVA = "0x10153DB78", Offset = "0x153DB78", VA = "0x10153DB78")]
		public RoomObjHaveList.RoomObj GetItem(int idx)
		{
			return null;
		}

		// Token: 0x06004533 RID: 17715 RVA: 0x00019B78 File Offset: 0x00017D78
		[Token(Token = "0x6003F9C")]
		[Address(RVA = "0x10153DC30", Offset = "0x153DC30", VA = "0x10153DC30")]
		public int GetItemNum()
		{
			return 0;
		}

		// Token: 0x06004534 RID: 17716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F9D")]
		[Address(RVA = "0x10153D9B4", Offset = "0x153D9B4", VA = "0x10153D9B4")]
		public void ClearFilter()
		{
		}

		// Token: 0x06004535 RID: 17717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F9E")]
		[Address(RVA = "0x10153DCA8", Offset = "0x153DCA8", VA = "0x10153DCA8")]
		public void DoFilter(eRoomObjectCategory category)
		{
		}

		// Token: 0x06004536 RID: 17718 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003F9F")]
		[Address(RVA = "0x10153DE18", Offset = "0x153DE18", VA = "0x10153DE18")]
		public RoomObjHaveList.RoomObj GetFilteredItem(int idx)
		{
			return null;
		}

		// Token: 0x06004537 RID: 17719 RVA: 0x00019B90 File Offset: 0x00017D90
		[Token(Token = "0x6003FA0")]
		[Address(RVA = "0x10153DED0", Offset = "0x153DED0", VA = "0x10153DED0")]
		public int GetFilteredItemNum()
		{
			return 0;
		}

		// Token: 0x06004538 RID: 17720 RVA: 0x00019BA8 File Offset: 0x00017DA8
		[Token(Token = "0x6003FA1")]
		[Address(RVA = "0x10153DF48", Offset = "0x153DF48", VA = "0x10153DF48")]
		public int GetSimulateFilteredItemNum(eRoomObjectCategory category)
		{
			return 0;
		}

		// Token: 0x06004539 RID: 17721 RVA: 0x00019BC0 File Offset: 0x00017DC0
		[Token(Token = "0x6003FA2")]
		[Address(RVA = "0x10153E044", Offset = "0x153E044", VA = "0x10153E044")]
		public int GetSimulateFilteredAllItemNum()
		{
			return 0;
		}

		// Token: 0x0600453A RID: 17722 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA3")]
		[Address(RVA = "0x10153E0D0", Offset = "0x153E0D0", VA = "0x10153E0D0")]
		public RoomObjHaveList()
		{
		}

		// Token: 0x04005635 RID: 22069
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003CB1")]
		private bool m_IsAvailable;

		// Token: 0x04005636 RID: 22070
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4003CB2")]
		private bool m_IsOpen;

		// Token: 0x04005637 RID: 22071
		[Cpp2IlInjected.FieldOffset(Offset = "0x12")]
		[Token(Token = "0x4003CB3")]
		private bool m_flgUseFilter;

		// Token: 0x04005638 RID: 22072
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003CB4")]
		private List<RoomObjHaveList.RoomObj> m_ObjList;

		// Token: 0x04005639 RID: 22073
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003CB5")]
		private List<RoomObjHaveList.RoomObj> m_FilteredObjList;

		// Token: 0x02000E82 RID: 3714
		[Token(Token = "0x2001166")]
		public class RoomObj
		{
			// Token: 0x0600453B RID: 17723 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600624A")]
			[Address(RVA = "0x10153D9AC", Offset = "0x153D9AC", VA = "0x10153D9AC")]
			public RoomObj()
			{
			}

			// Token: 0x0400563A RID: 22074
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006CCD")]
			public eRoomObjectCategory category;

			// Token: 0x0400563B RID: 22075
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006CCE")]
			public int id;

			// Token: 0x0400563C RID: 22076
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006CCF")]
			public int sortID;
		}
	}
}
