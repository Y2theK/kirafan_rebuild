﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E83 RID: 3715
	[Token(Token = "0x20009E2")]
	[StructLayout(3)]
	public class RoomObjInfo : MonoBehaviour
	{
		// Token: 0x0600453C RID: 17724 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA4")]
		[Address(RVA = "0x10153E0D8", Offset = "0x153E0D8", VA = "0x10153E0D8")]
		public void Apply(eRoomObjectCategory category, int objID, long buyPrice)
		{
		}

		// Token: 0x0600453D RID: 17725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA5")]
		[Address(RVA = "0x10153E5D0", Offset = "0x153E5D0", VA = "0x10153E5D0")]
		public void Destroy()
		{
		}

		// Token: 0x0600453E RID: 17726 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA6")]
		[Address(RVA = "0x10153E604", Offset = "0x153E604", VA = "0x10153E604")]
		public RoomObjInfo()
		{
		}

		// Token: 0x0400563D RID: 22077
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003CB6")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x0400563E RID: 22078
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003CB7")]
		[SerializeField]
		private RoomObjectIcon m_Icon;

		// Token: 0x0400563F RID: 22079
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003CB8")]
		[SerializeField]
		private Text m_Descript;

		// Token: 0x04005640 RID: 22080
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003CB9")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04005641 RID: 22081
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003CBA")]
		[SerializeField]
		private Text m_SizeText;

		// Token: 0x04005642 RID: 22082
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003CBB")]
		[SerializeField]
		private Text m_BuyPriceText;

		// Token: 0x04005643 RID: 22083
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003CBC")]
		[SerializeField]
		private Text m_SalePriceText;
	}
}
