﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000E84 RID: 3716
	[Token(Token = "0x20009E3")]
	[StructLayout(3)]
	public class RoomObjInfoWindow : UIGroup
	{
		// Token: 0x0600453F RID: 17727 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA7")]
		[Address(RVA = "0x10153E60C", Offset = "0x153E60C", VA = "0x10153E60C")]
		public void Open(eRoomObjectCategory category, int objID)
		{
		}

		// Token: 0x06004540 RID: 17728 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA8")]
		[Address(RVA = "0x10153E664", Offset = "0x153E664", VA = "0x10153E664")]
		public RoomObjInfoWindow()
		{
		}

		// Token: 0x04005644 RID: 22084
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CBD")]
		[SerializeField]
		private RoomObjInfo m_ObjInfo;
	}
}
