﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E85 RID: 3717
	[Token(Token = "0x20009E4")]
	[StructLayout(3)]
	public class RoomObjListItem : ScrollItemIcon
	{
		// Token: 0x06004541 RID: 17729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FA9")]
		[Address(RVA = "0x10153E66C", Offset = "0x153E66C", VA = "0x10153E66C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004542 RID: 17730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAA")]
		[Address(RVA = "0x10153EED8", Offset = "0x153EED8", VA = "0x10153EED8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004543 RID: 17731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAB")]
		[Address(RVA = "0x10153EF10", Offset = "0x153EF10", VA = "0x10153EF10")]
		public void SetAttachCanvas()
		{
		}

		// Token: 0x06004544 RID: 17732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAC")]
		[Address(RVA = "0x10153F0E8", Offset = "0x153F0E8", VA = "0x10153F0E8")]
		public void OnPlacementButtonCallBack()
		{
		}

		// Token: 0x06004545 RID: 17733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAD")]
		[Address(RVA = "0x10153F2B4", Offset = "0x153F2B4", VA = "0x10153F2B4")]
		public void OnSellButtonCallBack()
		{
		}

		// Token: 0x06004546 RID: 17734 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAE")]
		[Address(RVA = "0x10153F480", Offset = "0x153F480", VA = "0x10153F480")]
		public void OnBuyButtonCallBack()
		{
		}

		// Token: 0x06004547 RID: 17735 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FAF")]
		[Address(RVA = "0x10153F6B0", Offset = "0x153F6B0", VA = "0x10153F6B0")]
		public RoomObjListItem()
		{
		}

		// Token: 0x04005645 RID: 22085
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003CBE")]
		[SerializeField]
		private RoomObjectIcon m_Icon;

		// Token: 0x04005646 RID: 22086
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003CBF")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04005647 RID: 22087
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003CC0")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100129048", Offset = "0x129048")]
		private Text m_HaveNumTextObj;

		// Token: 0x04005648 RID: 22088
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003CC1")]
		[SerializeField]
		private Text m_CostTextObj;

		// Token: 0x04005649 RID: 22089
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003CC2")]
		[SerializeField]
		private Text m_SalePriceTextObj;

		// Token: 0x0400564A RID: 22090
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003CC3")]
		[SerializeField]
		private GameObject m_HaveObj;

		// Token: 0x0400564B RID: 22091
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CC4")]
		[SerializeField]
		private GameObject m_ShopObj;

		// Token: 0x0400564C RID: 22092
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CC5")]
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x0400564D RID: 22093
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CC6")]
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x0400564E RID: 22094
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003CC7")]
		[SerializeField]
		private CustomButton m_SellButton;

		// Token: 0x0400564F RID: 22095
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003CC8")]
		[SerializeField]
		private GameObject m_SaleIconObj;

		// Token: 0x04005650 RID: 22096
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003CC9")]
		[SerializeField]
		private GameObject m_PickupIconObj;

		// Token: 0x04005651 RID: 22097
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003CCA")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;
	}
}
