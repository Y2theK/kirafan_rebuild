﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E89 RID: 3721
	[Token(Token = "0x20009E7")]
	[StructLayout(3)]
	public class RoomObjListWindow : UIGroup
	{
		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06004553 RID: 17747 RVA: 0x00019BD8 File Offset: 0x00017DD8
		[Token(Token = "0x170004A6")]
		public RoomObjListWindow.eButton SelectButton
		{
			[Token(Token = "0x6003FBB")]
			[Address(RVA = "0x10153F6D0", Offset = "0x153F6D0", VA = "0x10153F6D0")]
			get
			{
				return RoomObjListWindow.eButton.None;
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06004554 RID: 17748 RVA: 0x00019BF0 File Offset: 0x00017DF0
		[Token(Token = "0x170004A7")]
		public int SelectObjID
		{
			[Token(Token = "0x6003FBC")]
			[Address(RVA = "0x10153F6D8", Offset = "0x153F6D8", VA = "0x10153F6D8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06004555 RID: 17749 RVA: 0x00019C08 File Offset: 0x00017E08
		[Token(Token = "0x170004A8")]
		public eRoomObjectCategory SelectObjCategory
		{
			[Token(Token = "0x6003FBD")]
			[Address(RVA = "0x10153F6E0", Offset = "0x153F6E0", VA = "0x10153F6E0")]
			get
			{
				return eRoomObjectCategory.Desk;
			}
		}

		// Token: 0x06004556 RID: 17750 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FBE")]
		[Address(RVA = "0x10153F6E8", Offset = "0x153F6E8", VA = "0x10153F6E8")]
		public void Setup(RoomObjShopList shopList)
		{
		}

		// Token: 0x06004557 RID: 17751 RVA: 0x00019C20 File Offset: 0x00017E20
		[Token(Token = "0x6003FBF")]
		[Address(RVA = "0x10153F968", Offset = "0x153F968", VA = "0x10153F968")]
		public int CategoryToTabIdx(eRoomObjectCategory category)
		{
			return 0;
		}

		// Token: 0x06004558 RID: 17752 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC0")]
		[Address(RVA = "0x10153F970", Offset = "0x153F970", VA = "0x10153F970")]
		public void Open(RoomObjListWindow.eMode mode)
		{
		}

		// Token: 0x06004559 RID: 17753 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC1")]
		[Address(RVA = "0x1015402BC", Offset = "0x15402BC", VA = "0x1015402BC", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x0600455A RID: 17754 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC2")]
		[Address(RVA = "0x1015403C4", Offset = "0x15403C4", VA = "0x1015403C4", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x0600455B RID: 17755 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC3")]
		[Address(RVA = "0x101540278", Offset = "0x1540278", VA = "0x101540278")]
		private void Apply()
		{
		}

		// Token: 0x0600455C RID: 17756 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC4")]
		[Address(RVA = "0x101540984", Offset = "0x1540984", VA = "0x101540984")]
		private void ApplyScroll()
		{
		}

		// Token: 0x0600455D RID: 17757 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC5")]
		[Address(RVA = "0x1015400EC", Offset = "0x15400EC", VA = "0x1015400EC")]
		private void RestoreBackupScrollAndTabIdx()
		{
		}

		// Token: 0x0600455E RID: 17758 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC6")]
		[Address(RVA = "0x1015416D4", Offset = "0x15416D4", VA = "0x1015416D4")]
		private void BackupScroll()
		{
		}

		// Token: 0x0600455F RID: 17759 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC7")]
		[Address(RVA = "0x10154181C", Offset = "0x154181C", VA = "0x10154181C")]
		private void BackupTabIdx()
		{
		}

		// Token: 0x06004560 RID: 17760 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC8")]
		[Address(RVA = "0x101541928", Offset = "0x1541928", VA = "0x101541928")]
		public void OnPlacementButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
		}

		// Token: 0x06004561 RID: 17761 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FC9")]
		[Address(RVA = "0x101541960", Offset = "0x1541960", VA = "0x101541960")]
		public void OnSellButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
		}

		// Token: 0x06004562 RID: 17762 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCA")]
		[Address(RVA = "0x101541998", Offset = "0x1541998", VA = "0x101541998")]
		public void OnClickFilterButtonCallback()
		{
		}

		// Token: 0x06004563 RID: 17763 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCB")]
		[Address(RVA = "0x101541A90", Offset = "0x1541A90", VA = "0x101541A90")]
		public void OnExecuteFilter()
		{
		}

		// Token: 0x06004564 RID: 17764 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCC")]
		[Address(RVA = "0x101540530", Offset = "0x1540530", VA = "0x101540530")]
		private void UpdateTabStatus()
		{
		}

		// Token: 0x06004565 RID: 17765 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCD")]
		[Address(RVA = "0x101541CA8", Offset = "0x1541CA8", VA = "0x101541CA8")]
		public void OnBuyButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
		}

		// Token: 0x06004566 RID: 17766 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCE")]
		[Address(RVA = "0x101541CE0", Offset = "0x1541CE0", VA = "0x101541CE0")]
		public void OnClickTabCallBack(int tabIdx)
		{
		}

		// Token: 0x06004567 RID: 17767 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FCF")]
		[Address(RVA = "0x101540888", Offset = "0x1540888", VA = "0x101540888")]
		private void ChangeCategory(int tabIdx)
		{
		}

		// Token: 0x06004568 RID: 17768 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FD0")]
		[Address(RVA = "0x101541F58", Offset = "0x1541F58", VA = "0x101541F58")]
		public RoomObjListWindow()
		{
		}

		// Token: 0x0400565F RID: 22111
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CD5")]
		private RoomObjListWindow.eMode m_Mode;

		// Token: 0x04005660 RID: 22112
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003CD6")]
		private RoomObjListWindow.eButton m_SelectButton;

		// Token: 0x04005661 RID: 22113
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CD7")]
		private int m_SelectObjID;

		// Token: 0x04005662 RID: 22114
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x4003CD8")]
		private eRoomObjectCategory m_SelectObjCategory;

		// Token: 0x04005663 RID: 22115
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CD9")]
		private RoomObjHaveList m_HaveList;

		// Token: 0x04005664 RID: 22116
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003CDA")]
		private RoomObjShopList m_ShopList;

		// Token: 0x04005665 RID: 22117
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003CDB")]
		[SerializeField]
		private Text m_PlacementNumText;

		// Token: 0x04005666 RID: 22118
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003CDC")]
		[SerializeField]
		private Text m_StoreNumText;

		// Token: 0x04005667 RID: 22119
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003CDD")]
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x04005668 RID: 22120
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003CDE")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005669 RID: 22121
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003CDF")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x0400566A RID: 22122
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003CE0")]
		[SerializeField]
		private CustomButton m_CloseButton;

		// Token: 0x0400566B RID: 22123
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003CE1")]
		[SerializeField]
		private CustomButton m_FilterButton;

		// Token: 0x0400566C RID: 22124
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003CE2")]
		private bool m_IsDonePrepare;

		// Token: 0x0400566D RID: 22125
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003CE3")]
		private GameObject m_GameObject;

		// Token: 0x0400566E RID: 22126
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003CE4")]
		private bool m_IsOpenTutorial;

		// Token: 0x0400566F RID: 22127
		[Cpp2IlInjected.FieldOffset(Offset = "0xF4")]
		[Token(Token = "0x4003CE5")]
		private float m_BackupScrollRateReserve;

		// Token: 0x02000E8A RID: 3722
		[Token(Token = "0x2001168")]
		public enum eMode
		{
			// Token: 0x04005671 RID: 22129
			[Token(Token = "0x4006CD4")]
			Have,
			// Token: 0x04005672 RID: 22130
			[Token(Token = "0x4006CD5")]
			HaveBedding,
			// Token: 0x04005673 RID: 22131
			[Token(Token = "0x4006CD6")]
			Shop,
			// Token: 0x04005674 RID: 22132
			[Token(Token = "0x4006CD7")]
			Max
		}

		// Token: 0x02000E8B RID: 3723
		[Token(Token = "0x2001169")]
		public enum eButton
		{
			// Token: 0x04005676 RID: 22134
			[Token(Token = "0x4006CD9")]
			None,
			// Token: 0x04005677 RID: 22135
			[Token(Token = "0x4006CDA")]
			Placement,
			// Token: 0x04005678 RID: 22136
			[Token(Token = "0x4006CDB")]
			Sell,
			// Token: 0x04005679 RID: 22137
			[Token(Token = "0x4006CDC")]
			Buy
		}

		// Token: 0x02000E8C RID: 3724
		[Token(Token = "0x200116A")]
		public class RoomObjListBackupParam
		{
			// Token: 0x06004569 RID: 17769 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600624B")]
			[Address(RVA = "0x101541FD0", Offset = "0x1541FD0", VA = "0x101541FD0")]
			public RoomObjListBackupParam()
			{
			}

			// Token: 0x0600456A RID: 17770 RVA: 0x00019C38 File Offset: 0x00017E38
			[Token(Token = "0x600624C")]
			[Address(RVA = "0x101541684", Offset = "0x1541684", VA = "0x101541684")]
			public float GetScrollRate(RoomObjListWindow.eMode mode)
			{
				return 0f;
			}

			// Token: 0x0600456B RID: 17771 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600624D")]
			[Address(RVA = "0x1015417C0", Offset = "0x15417C0", VA = "0x1015417C0")]
			public void SetScrollRate(RoomObjListWindow.eMode mode, float rate)
			{
			}

			// Token: 0x0600456C RID: 17772 RVA: 0x00019C50 File Offset: 0x00017E50
			[Token(Token = "0x600624E")]
			[Address(RVA = "0x101541634", Offset = "0x1541634", VA = "0x101541634")]
			public int GetTabIdx(RoomObjListWindow.eMode mode)
			{
				return 0;
			}

			// Token: 0x0600456D RID: 17773 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600624F")]
			[Address(RVA = "0x1015418CC", Offset = "0x15418CC", VA = "0x1015418CC")]
			public void SetTabIdx(RoomObjListWindow.eMode mode, int idx)
			{
			}

			// Token: 0x170004FE RID: 1278
			// (get) Token: 0x0600456E RID: 17774 RVA: 0x00002052 File Offset: 0x00000252
			// (set) Token: 0x0600456F RID: 17775 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006CB")]
			public List<RoomObjShopList.RoomShopObjData> RoomObjShopList
			{
				[Token(Token = "0x6006250")]
				[Address(RVA = "0x1015420CC", Offset = "0x15420CC", VA = "0x1015420CC")]
				get
				{
					return null;
				}
				[Token(Token = "0x6006251")]
				[Address(RVA = "0x1015420D4", Offset = "0x15420D4", VA = "0x1015420D4")]
				set
				{
				}
			}

			// Token: 0x170004FF RID: 1279
			// (get) Token: 0x06004570 RID: 17776 RVA: 0x00002052 File Offset: 0x00000252
			// (set) Token: 0x06004571 RID: 17777 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x170006CC")]
			public List<RoomObjHaveList.RoomObj> RoomObjHaveList
			{
				[Token(Token = "0x6006252")]
				[Address(RVA = "0x101542168", Offset = "0x1542168", VA = "0x101542168")]
				get
				{
					return null;
				}
				[Token(Token = "0x6006253")]
				[Address(RVA = "0x10153D5E0", Offset = "0x153D5E0", VA = "0x10153D5E0")]
				set
				{
				}
			}

			// Token: 0x06004572 RID: 17778 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006254")]
			[Address(RVA = "0x101542170", Offset = "0x1542170", VA = "0x101542170")]
			public void ClearScroll()
			{
			}

			// Token: 0x06004573 RID: 17779 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006255")]
			[Address(RVA = "0x1015421D8", Offset = "0x15421D8", VA = "0x1015421D8")]
			public void ClearTab()
			{
			}

			// Token: 0x06004574 RID: 17780 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006256")]
			[Address(RVA = "0x101540340", Offset = "0x1540340", VA = "0x101540340")]
			public void ClearTmpDataList()
			{
			}

			// Token: 0x06004575 RID: 17781 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006257")]
			[Address(RVA = "0x1015420A0", Offset = "0x15420A0", VA = "0x1015420A0")]
			public void Clear()
			{
			}

			// Token: 0x0400567A RID: 22138
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006CDD")]
			private float[] m_ScrollRate;

			// Token: 0x0400567B RID: 22139
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006CDE")]
			private int[] m_TabIdx;

			// Token: 0x0400567C RID: 22140
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006CDF")]
			private List<RoomObjShopList.RoomShopObjData> m_RoomObjShopList;

			// Token: 0x0400567D RID: 22141
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006CE0")]
			private List<RoomObjHaveList.RoomObj> m_RoomObjHaveList;
		}
	}
}
