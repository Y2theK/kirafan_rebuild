﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E8D RID: 3725
	[Token(Token = "0x20009E8")]
	[StructLayout(3)]
	public class RoomObjSelectWindow : UIGroup
	{
		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06004576 RID: 17782 RVA: 0x00019C68 File Offset: 0x00017E68
		[Token(Token = "0x170004A9")]
		public RoomObjSelectWindow.eButton SelectButton
		{
			[Token(Token = "0x6003FD1")]
			[Address(RVA = "0x101542280", Offset = "0x1542280", VA = "0x101542280")]
			get
			{
				return RoomObjSelectWindow.eButton.None;
			}
		}

		// Token: 0x06004577 RID: 17783 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FD2")]
		[Address(RVA = "0x101542288", Offset = "0x1542288", VA = "0x101542288")]
		public CustomButton GetEditCancelButton()
		{
			return null;
		}

		// Token: 0x06004578 RID: 17784 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FD3")]
		[Address(RVA = "0x101542290", Offset = "0x1542290", VA = "0x101542290")]
		public CustomButton GetSendStockButton()
		{
			return null;
		}

		// Token: 0x06004579 RID: 17785 RVA: 0x00019C80 File Offset: 0x00017E80
		[Token(Token = "0x6003FD4")]
		[Address(RVA = "0x101542298", Offset = "0x1542298", VA = "0x101542298", Slot = "16")]
		protected override bool CheckInEnd()
		{
			return default(bool);
		}

		// Token: 0x0600457A RID: 17786 RVA: 0x00019C98 File Offset: 0x00017E98
		[Token(Token = "0x6003FD5")]
		[Address(RVA = "0x101542358", Offset = "0x1542358", VA = "0x101542358", Slot = "17")]
		protected override bool CheckOutEnd()
		{
			return default(bool);
		}

		// Token: 0x0600457B RID: 17787 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FD6")]
		[Address(RVA = "0x101542418", Offset = "0x1542418", VA = "0x101542418")]
		public void OpenInfo(eRoomObjectCategory category, int objID)
		{
		}

		// Token: 0x0600457C RID: 17788 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FD7")]
		[Address(RVA = "0x1015425A0", Offset = "0x15425A0", VA = "0x1015425A0")]
		public void OpenEdit(eRoomObjectCategory category, int objID, string nameSuffix, bool canFlip = true)
		{
		}

		// Token: 0x0600457D RID: 17789 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FD8")]
		[Address(RVA = "0x101542A20", Offset = "0x1542A20", VA = "0x101542A20")]
		public void OpenBuy(eRoomObjectCategory category, int objID, string nameSuffix, bool canFlip = true)
		{
		}

		// Token: 0x0600457E RID: 17790 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FD9")]
		[Address(RVA = "0x101542F58", Offset = "0x1542F58", VA = "0x101542F58")]
		public void OpenMission()
		{
		}

		// Token: 0x0600457F RID: 17791 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDA")]
		[Address(RVA = "0x101543178", Offset = "0x1543178", VA = "0x101543178")]
		public void ChangeNameSuffix(string nameSuffix)
		{
		}

		// Token: 0x06004580 RID: 17792 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDB")]
		[Address(RVA = "0x1015431C4", Offset = "0x15431C4", VA = "0x1015431C4", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004581 RID: 17793 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDC")]
		[Address(RVA = "0x1015433A0", Offset = "0x15433A0", VA = "0x1015433A0")]
		public void SetPlaceButtonEnable(bool flg)
		{
		}

		// Token: 0x06004582 RID: 17794 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDD")]
		[Address(RVA = "0x101542834", Offset = "0x1542834", VA = "0x101542834")]
		private void SetReverceButtonEnableInEditButton(bool flg)
		{
		}

		// Token: 0x06004583 RID: 17795 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDE")]
		[Address(RVA = "0x101542D6C", Offset = "0x1542D6C", VA = "0x101542D6C")]
		private void SetReverceButtonEnableInBuyButton(bool flg)
		{
		}

		// Token: 0x06004584 RID: 17796 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FDF")]
		[Address(RVA = "0x101543404", Offset = "0x1543404", VA = "0x101543404")]
		public void SetFriendmode(bool flg)
		{
		}

		// Token: 0x06004585 RID: 17797 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE0")]
		[Address(RVA = "0x1015434A4", Offset = "0x15434A4", VA = "0x1015434A4")]
		public void ResetSelectButton()
		{
		}

		// Token: 0x06004586 RID: 17798 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE1")]
		[Address(RVA = "0x1015434AC", Offset = "0x15434AC", VA = "0x1015434AC")]
		public void OnClickInfoButton()
		{
		}

		// Token: 0x06004587 RID: 17799 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE2")]
		[Address(RVA = "0x1015434B8", Offset = "0x15434B8", VA = "0x1015434B8")]
		public void OnClickMoveButton()
		{
		}

		// Token: 0x06004588 RID: 17800 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE3")]
		[Address(RVA = "0x1015434C4", Offset = "0x15434C4", VA = "0x1015434C4")]
		public void OnClickPutAwayButton()
		{
		}

		// Token: 0x06004589 RID: 17801 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE4")]
		[Address(RVA = "0x1015434D0", Offset = "0x15434D0", VA = "0x1015434D0")]
		public void OnClickPlacementButton()
		{
		}

		// Token: 0x0600458A RID: 17802 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE5")]
		[Address(RVA = "0x1015434DC", Offset = "0x15434DC", VA = "0x1015434DC")]
		public void OnClickBuyButton()
		{
		}

		// Token: 0x0600458B RID: 17803 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE6")]
		[Address(RVA = "0x1015434E8", Offset = "0x15434E8", VA = "0x1015434E8")]
		public void OnClickFlipButton()
		{
		}

		// Token: 0x0600458C RID: 17804 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE7")]
		[Address(RVA = "0x1015434F4", Offset = "0x15434F4", VA = "0x1015434F4")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x0600458D RID: 17805 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE8")]
		[Address(RVA = "0x101543500", Offset = "0x1543500", VA = "0x101543500")]
		public void OnClickSendStockButton()
		{
		}

		// Token: 0x0600458E RID: 17806 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FE9")]
		[Address(RVA = "0x10154350C", Offset = "0x154350C", VA = "0x10154350C")]
		public RoomObjSelectWindow()
		{
		}

		// Token: 0x0400567E RID: 22142
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CE6")]
		private RoomObjSelectWindow.eButton m_SelectButton;

		// Token: 0x0400567F RID: 22143
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CE7")]
		[SerializeField]
		private Text m_ObjName;

		// Token: 0x04005680 RID: 22144
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CE8")]
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x04005681 RID: 22145
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003CE9")]
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x04005682 RID: 22146
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003CEA")]
		[SerializeField]
		private CustomButton m_FlipButton;

		// Token: 0x04005683 RID: 22147
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003CEB")]
		[SerializeField]
		private CustomButton m_EditCancelButton;

		// Token: 0x04005684 RID: 22148
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003CEC")]
		[SerializeField]
		private CustomButton m_BuyFlipButton;

		// Token: 0x04005685 RID: 22149
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003CED")]
		[SerializeField]
		private CustomButton m_BuyCancelButton;

		// Token: 0x04005686 RID: 22150
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003CEE")]
		[SerializeField]
		private CustomButton m_SendStockButton;

		// Token: 0x04005687 RID: 22151
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003CEF")]
		[SerializeField]
		private AnimUIPlayer m_TextAnim;

		// Token: 0x04005688 RID: 22152
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003CF0")]
		[SerializeField]
		private AnimUIPlayer m_InfoButtonsAnim;

		// Token: 0x04005689 RID: 22153
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003CF1")]
		[SerializeField]
		private AnimUIPlayer m_EditButtonsAnim;

		// Token: 0x0400568A RID: 22154
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003CF2")]
		[SerializeField]
		private AnimUIPlayer m_BuyButtonsAnim;

		// Token: 0x0400568B RID: 22155
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003CF3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100129294", Offset = "0x129294")]
		private CustomButton[] m_FriendInvalidButtons;

		// Token: 0x0400568C RID: 22156
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003CF4")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x0400568D RID: 22157
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003CF5")]
		private string m_EditingObjName;

		// Token: 0x02000E8E RID: 3726
		[Token(Token = "0x200116B")]
		public enum eMode
		{
			// Token: 0x0400568F RID: 22159
			[Token(Token = "0x4006CE2")]
			Info,
			// Token: 0x04005690 RID: 22160
			[Token(Token = "0x4006CE3")]
			Edit,
			// Token: 0x04005691 RID: 22161
			[Token(Token = "0x4006CE4")]
			Buy
		}

		// Token: 0x02000E8F RID: 3727
		[Token(Token = "0x200116C")]
		public enum eButton
		{
			// Token: 0x04005693 RID: 22163
			[Token(Token = "0x4006CE6")]
			None,
			// Token: 0x04005694 RID: 22164
			[Token(Token = "0x4006CE7")]
			Info,
			// Token: 0x04005695 RID: 22165
			[Token(Token = "0x4006CE8")]
			Move,
			// Token: 0x04005696 RID: 22166
			[Token(Token = "0x4006CE9")]
			PutAway,
			// Token: 0x04005697 RID: 22167
			[Token(Token = "0x4006CEA")]
			Placement,
			// Token: 0x04005698 RID: 22168
			[Token(Token = "0x4006CEB")]
			Buy,
			// Token: 0x04005699 RID: 22169
			[Token(Token = "0x4006CEC")]
			Flip,
			// Token: 0x0400569A RID: 22170
			[Token(Token = "0x4006CED")]
			Cancel,
			// Token: 0x0400569B RID: 22171
			[Token(Token = "0x4006CEE")]
			SendStock
		}
	}
}
