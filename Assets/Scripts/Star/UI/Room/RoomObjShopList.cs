﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star.UI.Room
{
	// Token: 0x02000E90 RID: 3728
	[Token(Token = "0x20009E9")]
	[StructLayout(3)]
	public class RoomObjShopList
	{
		// Token: 0x14000094 RID: 148
		// (add) Token: 0x0600458F RID: 17807 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004590 RID: 17808 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000094")]
		private event Action OnResponseSale
		{
			[Token(Token = "0x6003FEA")]
			[Address(RVA = "0x101543564", Offset = "0x1543564", VA = "0x101543564")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003FEB")]
			[Address(RVA = "0x101543670", Offset = "0x1543670", VA = "0x101543670")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06004591 RID: 17809 RVA: 0x00019CB0 File Offset: 0x00017EB0
		[Token(Token = "0x170004AA")]
		public bool IsAvailable
		{
			[Token(Token = "0x6003FEC")]
			[Address(RVA = "0x1015401E4", Offset = "0x15401E4", VA = "0x1015401E4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004592 RID: 17810 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FED")]
		[Address(RVA = "0x1015401EC", Offset = "0x15401EC", VA = "0x1015401EC")]
		public void Open()
		{
		}

		// Token: 0x06004593 RID: 17811 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FEE")]
		[Address(RVA = "0x101541424", Offset = "0x1541424", VA = "0x101541424")]
		public void BufferingObjList()
		{
		}

		// Token: 0x06004594 RID: 17812 RVA: 0x00019CC8 File Offset: 0x00017EC8
		[Token(Token = "0x6003FEF")]
		[Address(RVA = "0x101540510", Offset = "0x1540510", VA = "0x101540510")]
		public bool UpdateOpen()
		{
			return default(bool);
		}

		// Token: 0x06004595 RID: 17813 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FF0")]
		[Address(RVA = "0x10154382C", Offset = "0x154382C", VA = "0x10154382C")]
		public void MarkRefresh()
		{
		}

		// Token: 0x06004596 RID: 17814 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FF1")]
		[Address(RVA = "0x1015438B4", Offset = "0x15438B4", VA = "0x1015438B4")]
		public RoomObjShopList.RoomShopObjData GetItemAt(int idx)
		{
			return null;
		}

		// Token: 0x06004597 RID: 17815 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FF2")]
		[Address(RVA = "0x10154396C", Offset = "0x154396C", VA = "0x10154396C")]
		public RoomObjShopList.RoomShopObjData GetItem(eRoomObjectCategory category, int id)
		{
			return null;
		}

		// Token: 0x06004598 RID: 17816 RVA: 0x00019CE0 File Offset: 0x00017EE0
		[Token(Token = "0x6003FF3")]
		[Address(RVA = "0x101543AB4", Offset = "0x1543AB4", VA = "0x101543AB4")]
		public int GetItemNum()
		{
			return 0;
		}

		// Token: 0x06004599 RID: 17817 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FF4")]
		[Address(RVA = "0x101541CE4", Offset = "0x1541CE4", VA = "0x101541CE4")]
		public void ClearFilter()
		{
		}

		// Token: 0x0600459A RID: 17818 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FF5")]
		[Address(RVA = "0x101541DF4", Offset = "0x1541DF4", VA = "0x101541DF4")]
		public void DoFilter(eRoomObjectCategory category)
		{
		}

		// Token: 0x0600459B RID: 17819 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FF6")]
		[Address(RVA = "0x101543B2C", Offset = "0x1543B2C", VA = "0x101543B2C")]
		public List<RoomObjShopList.RoomShopObjData> GetFilteredItemList()
		{
			return null;
		}

		// Token: 0x0600459C RID: 17820 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003FF7")]
		[Address(RVA = "0x1015412F4", Offset = "0x15412F4", VA = "0x1015412F4")]
		public RoomObjShopList.RoomShopObjData GetFilteredItem(int idx)
		{
			return null;
		}

		// Token: 0x0600459D RID: 17821 RVA: 0x00019CF8 File Offset: 0x00017EF8
		[Token(Token = "0x6003FF8")]
		[Address(RVA = "0x1015413AC", Offset = "0x15413AC", VA = "0x1015413AC")]
		public int GetFilteredItemNum()
		{
			return 0;
		}

		// Token: 0x0600459E RID: 17822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FF9")]
		[Address(RVA = "0x10154377C", Offset = "0x154377C", VA = "0x10154377C")]
		public void Request_GetList(MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x0600459F RID: 17823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FFA")]
		[Address(RVA = "0x101543B44", Offset = "0x1543B44", VA = "0x101543B44")]
		private void OnResponse_GetList(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060045A0 RID: 17824 RVA: 0x00019D10 File Offset: 0x00017F10
		[Token(Token = "0x6003FFB")]
		[Address(RVA = "0x101544140", Offset = "0x1544140", VA = "0x101544140")]
		private int Comp(RoomObjShopList.RoomShopObjData x, RoomObjShopList.RoomShopObjData y)
		{
			return 0;
		}

		// Token: 0x060045A1 RID: 17825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FFC")]
		[Address(RVA = "0x101544220", Offset = "0x1544220", VA = "0x101544220")]
		public void ExecuteSale(eRoomObjectCategory category, int objID, int objNum, Action OnRespone)
		{
		}

		// Token: 0x060045A2 RID: 17826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FFD")]
		[Address(RVA = "0x1015442C4", Offset = "0x15442C4", VA = "0x1015442C4")]
		public void Request_Sale(eRoomObjectCategory category, int objID, int objNum, MeigewwwParam.Callback callback)
		{
		}

		// Token: 0x060045A3 RID: 17827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FFE")]
		[Address(RVA = "0x101544540", Offset = "0x1544540", VA = "0x101544540")]
		private void OnResponse_Sale(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x060045A4 RID: 17828 RVA: 0x00019D28 File Offset: 0x00017F28
		[Token(Token = "0x6003FFF")]
		[Address(RVA = "0x101541BAC", Offset = "0x1541BAC", VA = "0x101541BAC")]
		public int GetSimulateFilteredItemNum(eRoomObjectCategory category)
		{
			return 0;
		}

		// Token: 0x060045A5 RID: 17829 RVA: 0x00019D40 File Offset: 0x00017F40
		[Token(Token = "0x6004000")]
		[Address(RVA = "0x10154472C", Offset = "0x154472C", VA = "0x10154472C")]
		public int GetSimulateFilteredAllItemNum()
		{
			return 0;
		}

		// Token: 0x060045A6 RID: 17830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004001")]
		[Address(RVA = "0x1015447B8", Offset = "0x15447B8", VA = "0x1015447B8")]
		public RoomObjShopList()
		{
		}

		// Token: 0x0400569C RID: 22172
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003CF6")]
		private bool m_IsAvailable;

		// Token: 0x0400569D RID: 22173
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x4003CF7")]
		private bool m_IsOpen;

		// Token: 0x0400569E RID: 22174
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003CF8")]
		private List<RoomObjShopList.RoomShopObjData> m_ObjList;

		// Token: 0x0400569F RID: 22175
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003CF9")]
		private List<RoomObjShopList.RoomShopObjData> m_FilteredObjList;

		// Token: 0x040056A0 RID: 22176
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003CFA")]
		private UserRoomObjectData m_SaleObjData;

		// Token: 0x040056A1 RID: 22177
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003CFB")]
		private List<long> m_SaleMngIDList;

		// Token: 0x02000E91 RID: 3729
		[Token(Token = "0x200116D")]
		public class RoomShopObjData
		{
			// Token: 0x060045A7 RID: 17831 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006258")]
			[Address(RVA = "0x101544138", Offset = "0x1544138", VA = "0x101544138")]
			public RoomShopObjData()
			{
			}

			// Token: 0x040056A3 RID: 22179
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006CEF")]
			public eRoomObjectCategory category;

			// Token: 0x040056A4 RID: 22180
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006CF0")]
			public int id;

			// Token: 0x040056A5 RID: 22181
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006CF1")]
			public int cost;

			// Token: 0x040056A6 RID: 22182
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006CF2")]
			public bool sale;

			// Token: 0x040056A7 RID: 22183
			[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
			[Token(Token = "0x4006CF3")]
			public bool isPickup;

			// Token: 0x040056A8 RID: 22184
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006CF4")]
			public int sortID;
		}
	}
}
