﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Room
{
	// Token: 0x02000E86 RID: 3718
	[Token(Token = "0x20009E5")]
	[StructLayout(3)]
	public class RoomObjectItemData : ScrollItemData
	{
		// Token: 0x06004548 RID: 17736 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FB0")]
		[Address(RVA = "0x101540F9C", Offset = "0x1540F9C", VA = "0x101540F9C")]
		public RoomObjectItemData()
		{
		}

		// Token: 0x14000091 RID: 145
		// (add) Token: 0x06004549 RID: 17737 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600454A RID: 17738 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000091")]
		public event Action<eRoomObjectCategory, int> OnPlacement
		{
			[Token(Token = "0x6003FB1")]
			[Address(RVA = "0x101540FD0", Offset = "0x1540FD0", VA = "0x101540FD0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003FB2")]
			[Address(RVA = "0x101544830", Offset = "0x1544830", VA = "0x101544830")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000092 RID: 146
		// (add) Token: 0x0600454B RID: 17739 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600454C RID: 17740 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000092")]
		public event Action<eRoomObjectCategory, int> OnSell
		{
			[Token(Token = "0x6003FB3")]
			[Address(RVA = "0x1015410DC", Offset = "0x15410DC", VA = "0x1015410DC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003FB4")]
			[Address(RVA = "0x10154493C", Offset = "0x154493C", VA = "0x10154493C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000093 RID: 147
		// (add) Token: 0x0600454D RID: 17741 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600454E RID: 17742 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000093")]
		public event Action<eRoomObjectCategory, int> OnBuy
		{
			[Token(Token = "0x6003FB5")]
			[Address(RVA = "0x1015411E8", Offset = "0x15411E8", VA = "0x1015411E8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003FB6")]
			[Address(RVA = "0x101544A48", Offset = "0x1544A48", VA = "0x101544A48")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600454F RID: 17743 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FB7")]
		[Address(RVA = "0x10153F260", Offset = "0x153F260", VA = "0x10153F260")]
		public void OnPlacementButtonCallBack()
		{
		}

		// Token: 0x06004550 RID: 17744 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FB8")]
		[Address(RVA = "0x10153F42C", Offset = "0x153F42C", VA = "0x10153F42C")]
		public void OnSellButtonCallBack()
		{
		}

		// Token: 0x06004551 RID: 17745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003FB9")]
		[Address(RVA = "0x10153F65C", Offset = "0x153F65C", VA = "0x10153F65C")]
		public void OnBuyButtonCallBack()
		{
		}

		// Token: 0x04005652 RID: 22098
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003CCB")]
		public RoomObjectItemData.eMode m_Mode;

		// Token: 0x04005653 RID: 22099
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003CCC")]
		public eRoomObjectCategory m_ObjCategory;

		// Token: 0x04005654 RID: 22100
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003CCD")]
		public int m_ObjID;

		// Token: 0x04005655 RID: 22101
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003CCE")]
		public int m_Cost;

		// Token: 0x04005656 RID: 22102
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003CCF")]
		public bool m_IsSale;

		// Token: 0x04005657 RID: 22103
		[Cpp2IlInjected.FieldOffset(Offset = "0x29")]
		[Token(Token = "0x4003CD0")]
		public bool m_IsPickup;

		// Token: 0x04005658 RID: 22104
		[Cpp2IlInjected.FieldOffset(Offset = "0x2A")]
		[Token(Token = "0x4003CD1")]
		public bool m_SwapBedding;

		// Token: 0x02000E87 RID: 3719
		[Token(Token = "0x2001167")]
		public enum eMode
		{
			// Token: 0x0400565D RID: 22109
			[Token(Token = "0x4006CD1")]
			Have,
			// Token: 0x0400565E RID: 22110
			[Token(Token = "0x4006CD2")]
			Shop
		}
	}
}
