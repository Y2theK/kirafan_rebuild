﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI.Room
{
	// Token: 0x02000E92 RID: 3730
	[Token(Token = "0x20009EA")]
	[StructLayout(3)]
	public class RoomShopConfirmGroup : UIGroup
	{
		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x060045A8 RID: 17832 RVA: 0x00019D58 File Offset: 0x00017F58
		[Token(Token = "0x170004AB")]
		public RoomShopConfirmGroup.eButton SelectButton
		{
			[Token(Token = "0x6004002")]
			[Address(RVA = "0x101544B54", Offset = "0x1544B54", VA = "0x101544B54")]
			get
			{
				return RoomShopConfirmGroup.eButton.None;
			}
		}

		// Token: 0x060045A9 RID: 17833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004003")]
		[Address(RVA = "0x101544B5C", Offset = "0x1544B5C", VA = "0x101544B5C")]
		public void Open(RoomObjShopList.RoomShopObjData shopObjData, UnityAction onClose)
		{
		}

		// Token: 0x060045AA RID: 17834 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004004")]
		[Address(RVA = "0x101544BD8", Offset = "0x1544BD8", VA = "0x101544BD8")]
		public void Open(eRoomObjectCategory category, int objID, long amount, UnityAction OnClose)
		{
		}

		// Token: 0x060045AB RID: 17835 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004005")]
		[Address(RVA = "0x101544D78", Offset = "0x1544D78", VA = "0x101544D78", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x060045AC RID: 17836 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004006")]
		[Address(RVA = "0x101544F68", Offset = "0x1544F68", VA = "0x101544F68", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060045AD RID: 17837 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004007")]
		[Address(RVA = "0x101544FD4", Offset = "0x1544FD4", VA = "0x101544FD4")]
		public void OnClickStoreButtonCallBack()
		{
		}

		// Token: 0x060045AE RID: 17838 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004008")]
		[Address(RVA = "0x101544FE8", Offset = "0x1544FE8", VA = "0x101544FE8")]
		public void OnClickPlaceButtonCallBack()
		{
		}

		// Token: 0x060045AF RID: 17839 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004009")]
		[Address(RVA = "0x101545130", Offset = "0x1545130", VA = "0x101545130")]
		public void OnClickCloseButtonCallBack()
		{
		}

		// Token: 0x060045B0 RID: 17840 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600400A")]
		[Address(RVA = "0x101545144", Offset = "0x1545144", VA = "0x101545144")]
		public RoomShopConfirmGroup()
		{
		}

		// Token: 0x040056A9 RID: 22185
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003CFD")]
		[SerializeField]
		private RoomObjInfo m_RoomObjInfo;

		// Token: 0x040056AA RID: 22186
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003CFE")]
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x040056AB RID: 22187
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003CFF")]
		[SerializeField]
		private CustomButton m_StoreButton;

		// Token: 0x040056AC RID: 22188
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003D00")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x040056AD RID: 22189
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003D01")]
		private RoomShopConfirmGroup.eButton m_SelectButton;

		// Token: 0x040056AE RID: 22190
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003D02")]
		private UnityAction m_CloseAction;

		// Token: 0x040056AF RID: 22191
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003D03")]
		private bool m_AttachedCanvas;

		// Token: 0x02000E93 RID: 3731
		[Token(Token = "0x200116E")]
		public enum eButton
		{
			// Token: 0x040056B1 RID: 22193
			[Token(Token = "0x4006CF6")]
			None,
			// Token: 0x040056B2 RID: 22194
			[Token(Token = "0x4006CF7")]
			Close,
			// Token: 0x040056B3 RID: 22195
			[Token(Token = "0x4006CF8")]
			Store,
			// Token: 0x040056B4 RID: 22196
			[Token(Token = "0x4006CF9")]
			Place
		}
	}
}
