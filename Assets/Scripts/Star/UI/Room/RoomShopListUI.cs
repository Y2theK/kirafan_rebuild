﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000E94 RID: 3732
	[Token(Token = "0x20009EB")]
	[StructLayout(3)]
	public class RoomShopListUI : MenuUIBase
	{
		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x060045B1 RID: 17841 RVA: 0x00019D70 File Offset: 0x00017F70
		// (set) Token: 0x060045B2 RID: 17842 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004AC")]
		public int selectNum
		{
			[Token(Token = "0x600400B")]
			[Address(RVA = "0x10154514C", Offset = "0x154514C", VA = "0x10154514C")]
			[CompilerGenerated]
			get
			{
				return 0;
			}
			[Token(Token = "0x600400C")]
			[Address(RVA = "0x101545154", Offset = "0x1545154", VA = "0x101545154")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x060045B3 RID: 17843 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600400D")]
		[Address(RVA = "0x10154515C", Offset = "0x154515C", VA = "0x10154515C")]
		public void OnCloseBuyWindow()
		{
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x060045B4 RID: 17844 RVA: 0x00019D88 File Offset: 0x00017F88
		[Token(Token = "0x170004AD")]
		public RoomShopListUI.eTransit Transit
		{
			[Token(Token = "0x600400E")]
			[Address(RVA = "0x1015451AC", Offset = "0x15451AC", VA = "0x1015451AC")]
			get
			{
				return RoomShopListUI.eTransit.None;
			}
		}

		// Token: 0x060045B5 RID: 17845 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600400F")]
		[Address(RVA = "0x1015451B4", Offset = "0x15451B4", VA = "0x1015451B4")]
		public void SetMode(RoomObjListWindow.eMode mode)
		{
		}

		// Token: 0x060045B6 RID: 17846 RVA: 0x00019DA0 File Offset: 0x00017FA0
		[Token(Token = "0x6004010")]
		[Address(RVA = "0x1015451BC", Offset = "0x15451BC", VA = "0x1015451BC")]
		public RoomObjListWindow.eMode GetMode()
		{
			return RoomObjListWindow.eMode.Have;
		}

		// Token: 0x060045B7 RID: 17847 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004011")]
		[Address(RVA = "0x1015451C4", Offset = "0x15451C4", VA = "0x1015451C4")]
		public void Setup(RoomObjShopList shopList, RoomBuilder builder)
		{
		}

		// Token: 0x060045B8 RID: 17848 RVA: 0x00019DB8 File Offset: 0x00017FB8
		[Token(Token = "0x6004012")]
		[Address(RVA = "0x1015452B0", Offset = "0x15452B0", VA = "0x1015452B0")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x060045B9 RID: 17849 RVA: 0x00019DD0 File Offset: 0x00017FD0
		[Token(Token = "0x6004013")]
		[Address(RVA = "0x1015452CC", Offset = "0x15452CC", VA = "0x1015452CC")]
		private bool CheckSwap(long objMngID)
		{
			return default(bool);
		}

		// Token: 0x060045BA RID: 17850 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004014")]
		[Address(RVA = "0x1015453F8", Offset = "0x15453F8", VA = "0x1015453F8")]
		private void Update()
		{
		}

		// Token: 0x060045BB RID: 17851 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004015")]
		[Address(RVA = "0x101545D18", Offset = "0x1545D18", VA = "0x101545D18")]
		private void ChangeStep(RoomShopListUI.eStep step)
		{
		}

		// Token: 0x060045BC RID: 17852 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004016")]
		[Address(RVA = "0x1015463B0", Offset = "0x15463B0", VA = "0x1015463B0")]
		private void OnResponseBuy(long[] mngID)
		{
		}

		// Token: 0x060045BD RID: 17853 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004017")]
		[Address(RVA = "0x1015465E8", Offset = "0x15465E8", VA = "0x1015465E8")]
		public void OnResponseSale()
		{
		}

		// Token: 0x060045BE RID: 17854 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004018")]
		[Address(RVA = "0x1015465F0", Offset = "0x15465F0", VA = "0x1015465F0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060045BF RID: 17855 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004019")]
		[Address(RVA = "0x101546694", Offset = "0x1546694", VA = "0x101546694", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060045C0 RID: 17856 RVA: 0x00019DE8 File Offset: 0x00017FE8
		[Token(Token = "0x600401A")]
		[Address(RVA = "0x1015467B8", Offset = "0x15467B8", VA = "0x1015467B8", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060045C1 RID: 17857 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600401B")]
		[Address(RVA = "0x1015467C8", Offset = "0x15467C8", VA = "0x1015467C8")]
		public RoomShopListUI()
		{
		}

		// Token: 0x040056B5 RID: 22197
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D04")]
		private RoomShopListUI.eStep m_Step;

		// Token: 0x040056B6 RID: 22198
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D05")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040056B7 RID: 22199
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D06")]
		[SerializeField]
		private RoomObjListWindow m_ObjListGroup;

		// Token: 0x040056B8 RID: 22200
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D07")]
		[SerializeField]
		private RoomObjBuyWindow m_BuyWindow;

		// Token: 0x040056BA RID: 22202
		[Cpp2IlInjected.FieldOffset(Offset = "0x64")]
		[Token(Token = "0x4003D09")]
		private RoomObjBuyWindow.eButton selectBuyWindowButton;

		// Token: 0x040056BB RID: 22203
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D0A")]
		[SerializeField]
		private RoomShopConfirmGroup m_ConfirmGroup;

		// Token: 0x040056BC RID: 22204
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D0B")]
		private RoomObjListWindow.eMode m_Mode;

		// Token: 0x040056BD RID: 22205
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D0C")]
		private RoomBuilder m_builder;

		// Token: 0x040056BE RID: 22206
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D0D")]
		private RoomShopListUI.eTransit m_Transit;

		// Token: 0x040056BF RID: 22207
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D0E")]
		private RoomObjShopList m_ShopList;

		// Token: 0x02000E95 RID: 3733
		[Token(Token = "0x200116F")]
		private enum eStep
		{
			// Token: 0x040056C1 RID: 22209
			[Token(Token = "0x4006CFB")]
			Hide,
			// Token: 0x040056C2 RID: 22210
			[Token(Token = "0x4006CFC")]
			In,
			// Token: 0x040056C3 RID: 22211
			[Token(Token = "0x4006CFD")]
			Idle,
			// Token: 0x040056C4 RID: 22212
			[Token(Token = "0x4006CFE")]
			Confirm,
			// Token: 0x040056C5 RID: 22213
			[Token(Token = "0x4006CFF")]
			NumSelect,
			// Token: 0x040056C6 RID: 22214
			[Token(Token = "0x4006D00")]
			NumSelectStock,
			// Token: 0x040056C7 RID: 22215
			[Token(Token = "0x4006D01")]
			LimitExtend,
			// Token: 0x040056C8 RID: 22216
			[Token(Token = "0x4006D02")]
			LimitExtendFail,
			// Token: 0x040056C9 RID: 22217
			[Token(Token = "0x4006D03")]
			BuyResponseWait,
			// Token: 0x040056CA RID: 22218
			[Token(Token = "0x4006D04")]
			Sale,
			// Token: 0x040056CB RID: 22219
			[Token(Token = "0x4006D05")]
			SaleResponseWait,
			// Token: 0x040056CC RID: 22220
			[Token(Token = "0x4006D06")]
			Out,
			// Token: 0x040056CD RID: 22221
			[Token(Token = "0x4006D07")]
			End
		}

		// Token: 0x02000E96 RID: 3734
		[Token(Token = "0x2001170")]
		public enum eTransit
		{
			// Token: 0x040056CF RID: 22223
			[Token(Token = "0x4006D09")]
			None,
			// Token: 0x040056D0 RID: 22224
			[Token(Token = "0x4006D0A")]
			Back,
			// Token: 0x040056D1 RID: 22225
			[Token(Token = "0x4006D0B")]
			Room
		}
	}
}
