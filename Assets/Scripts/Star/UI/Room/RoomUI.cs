﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Schedule;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000E97 RID: 3735
	[Token(Token = "0x20009EC")]
	[StructLayout(3)]
	public class RoomUI : MenuUIBase
	{
		// Token: 0x060045C4 RID: 17860 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600401E")]
		[Address(RVA = "0x101546A20", Offset = "0x1546A20", VA = "0x101546A20")]
		private string GetEditNumberSuffix(int idx, int max)
		{
			return null;
		}

		// Token: 0x060045C5 RID: 17861 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600401F")]
		[Address(RVA = "0x101546ABC", Offset = "0x1546ABC", VA = "0x101546ABC")]
		public void SetEditingNumSuffix(int idx, int max)
		{
		}

		// Token: 0x14000095 RID: 149
		// (add) Token: 0x060045C6 RID: 17862 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045C7 RID: 17863 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000095")]
		public event Action OnClickMoveButton
		{
			[Token(Token = "0x6004020")]
			[Address(RVA = "0x101546AF8", Offset = "0x1546AF8", VA = "0x101546AF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004021")]
			[Address(RVA = "0x101546C08", Offset = "0x1546C08", VA = "0x101546C08")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000096 RID: 150
		// (add) Token: 0x060045C8 RID: 17864 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045C9 RID: 17865 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000096")]
		public event Action OnClickInfoButton
		{
			[Token(Token = "0x6004022")]
			[Address(RVA = "0x101546D18", Offset = "0x1546D18", VA = "0x101546D18")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004023")]
			[Address(RVA = "0x101546E28", Offset = "0x1546E28", VA = "0x101546E28")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000097 RID: 151
		// (add) Token: 0x060045CA RID: 17866 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045CB RID: 17867 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000097")]
		public event Action OnClickPutAwayButton
		{
			[Token(Token = "0x6004024")]
			[Address(RVA = "0x101546F38", Offset = "0x1546F38", VA = "0x101546F38")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004025")]
			[Address(RVA = "0x101547048", Offset = "0x1547048", VA = "0x101547048")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000098 RID: 152
		// (add) Token: 0x060045CC RID: 17868 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045CD RID: 17869 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000098")]
		public event Action OnClickPlacementButton
		{
			[Token(Token = "0x6004026")]
			[Address(RVA = "0x101547158", Offset = "0x1547158", VA = "0x101547158")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004027")]
			[Address(RVA = "0x101547268", Offset = "0x1547268", VA = "0x101547268")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x14000099 RID: 153
		// (add) Token: 0x060045CE RID: 17870 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045CF RID: 17871 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000099")]
		public event Action OnClickBuyButton
		{
			[Token(Token = "0x6004028")]
			[Address(RVA = "0x101547378", Offset = "0x1547378", VA = "0x101547378")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004029")]
			[Address(RVA = "0x101547488", Offset = "0x1547488", VA = "0x101547488")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009A RID: 154
		// (add) Token: 0x060045D0 RID: 17872 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045D1 RID: 17873 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009A")]
		public event Action OnClickFlipButton
		{
			[Token(Token = "0x600402A")]
			[Address(RVA = "0x101547598", Offset = "0x1547598", VA = "0x101547598")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600402B")]
			[Address(RVA = "0x1015476A8", Offset = "0x15476A8", VA = "0x1015476A8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009B RID: 155
		// (add) Token: 0x060045D2 RID: 17874 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045D3 RID: 17875 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009B")]
		public event Action OnClickPlaceCancelButton
		{
			[Token(Token = "0x600402C")]
			[Address(RVA = "0x1015477B8", Offset = "0x15477B8", VA = "0x1015477B8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600402D")]
			[Address(RVA = "0x1015478C8", Offset = "0x15478C8", VA = "0x1015478C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009C RID: 156
		// (add) Token: 0x060045D4 RID: 17876 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045D5 RID: 17877 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009C")]
		public event Action OnCloseGreet
		{
			[Token(Token = "0x600402E")]
			[Address(RVA = "0x1015479D8", Offset = "0x15479D8", VA = "0x1015479D8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600402F")]
			[Address(RVA = "0x101547AE8", Offset = "0x1547AE8", VA = "0x101547AE8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009D RID: 157
		// (add) Token: 0x060045D6 RID: 17878 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045D7 RID: 17879 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009D")]
		public event Action OnCancelSelectBed
		{
			[Token(Token = "0x6004030")]
			[Address(RVA = "0x101547BF8", Offset = "0x1547BF8", VA = "0x101547BF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004031")]
			[Address(RVA = "0x101547D08", Offset = "0x1547D08", VA = "0x101547D08")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009E RID: 158
		// (add) Token: 0x060045D8 RID: 17880 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045D9 RID: 17881 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009E")]
		public event Action OnOpenObjList
		{
			[Token(Token = "0x6004032")]
			[Address(RVA = "0x101547E18", Offset = "0x1547E18", VA = "0x101547E18")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004033")]
			[Address(RVA = "0x101547F28", Offset = "0x1547F28", VA = "0x101547F28")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x1400009F RID: 159
		// (add) Token: 0x060045DA RID: 17882 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045DB RID: 17883 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400009F")]
		public event Action OnClickScheduleButton
		{
			[Token(Token = "0x6004034")]
			[Address(RVA = "0x101548038", Offset = "0x1548038", VA = "0x101548038")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004035")]
			[Address(RVA = "0x101548148", Offset = "0x1548148", VA = "0x101548148")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A0 RID: 160
		// (add) Token: 0x060045DC RID: 17884 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045DD RID: 17885 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A0")]
		public event Action<long[]> OnDecideLiveCharas
		{
			[Token(Token = "0x6004036")]
			[Address(RVA = "0x101548258", Offset = "0x1548258", VA = "0x101548258")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004037")]
			[Address(RVA = "0x101548368", Offset = "0x1548368", VA = "0x101548368")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A1 RID: 161
		// (add) Token: 0x060045DE RID: 17886 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045DF RID: 17887 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A1")]
		public event Action OnClosedScheduleWindow
		{
			[Token(Token = "0x6004038")]
			[Address(RVA = "0x101548478", Offset = "0x1548478", VA = "0x101548478")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004039")]
			[Address(RVA = "0x101548588", Offset = "0x1548588", VA = "0x101548588")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A2 RID: 162
		// (add) Token: 0x060045E0 RID: 17888 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045E1 RID: 17889 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A2")]
		public event Action OnClickBackButton_Selecting
		{
			[Token(Token = "0x600403A")]
			[Address(RVA = "0x101548698", Offset = "0x1548698", VA = "0x101548698")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600403B")]
			[Address(RVA = "0x1015487A8", Offset = "0x15487A8", VA = "0x1015487A8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A3 RID: 163
		// (add) Token: 0x060045E2 RID: 17890 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045E3 RID: 17891 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A3")]
		public event Action OnClickStoreAllButton
		{
			[Token(Token = "0x600403C")]
			[Address(RVA = "0x1015488B8", Offset = "0x15488B8", VA = "0x1015488B8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600403D")]
			[Address(RVA = "0x1015489C8", Offset = "0x15489C8", VA = "0x1015489C8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A4 RID: 164
		// (add) Token: 0x060045E4 RID: 17892 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045E5 RID: 17893 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A4")]
		public event Action OnClickRoomChangeButton
		{
			[Token(Token = "0x600403E")]
			[Address(RVA = "0x101548AD8", Offset = "0x1548AD8", VA = "0x101548AD8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600403F")]
			[Address(RVA = "0x101548BE8", Offset = "0x1548BE8", VA = "0x101548BE8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A5 RID: 165
		// (add) Token: 0x060045E6 RID: 17894 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045E7 RID: 17895 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A5")]
		public event Action OnClickFriendRoomButton
		{
			[Token(Token = "0x6004040")]
			[Address(RVA = "0x101548CF8", Offset = "0x1548CF8", VA = "0x101548CF8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004041")]
			[Address(RVA = "0x101548E08", Offset = "0x1548E08", VA = "0x101548E08")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000A6 RID: 166
		// (add) Token: 0x060045E8 RID: 17896 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060045E9 RID: 17897 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000A6")]
		public event Action OnClickTownButton
		{
			[Token(Token = "0x6004042")]
			[Address(RVA = "0x101548F18", Offset = "0x1548F18", VA = "0x101548F18")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004043")]
			[Address(RVA = "0x101549028", Offset = "0x1549028", VA = "0x101549028")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060045EA RID: 17898 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004044")]
		[Address(RVA = "0x101549138", Offset = "0x1549138", VA = "0x101549138")]
		public void SetInteractableAllButton(bool flg)
		{
		}

		// Token: 0x060045EB RID: 17899 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004045")]
		[Address(RVA = "0x10154920C", Offset = "0x154920C", VA = "0x10154920C")]
		public void RefreshTutorialStep()
		{
		}

		// Token: 0x060045EC RID: 17900 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004046")]
		[Address(RVA = "0x101549570", Offset = "0x1549570", VA = "0x101549570")]
		public void ToggleSlideMenu()
		{
		}

		// Token: 0x060045ED RID: 17901 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004047")]
		[Address(RVA = "0x101549640", Offset = "0x1549640", VA = "0x101549640")]
		public void Setup(RoomMain owner, RoomShopListUI shopUI, long playerID)
		{
		}

		// Token: 0x060045EE RID: 17902 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004048")]
		[Address(RVA = "0x10154A02C", Offset = "0x154A02C", VA = "0x10154A02C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060045EF RID: 17903 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004049")]
		[Address(RVA = "0x10154A110", Offset = "0x154A110", VA = "0x10154A110", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060045F0 RID: 17904 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600404A")]
		[Address(RVA = "0x10154A218", Offset = "0x154A218", VA = "0x10154A218")]
		private void LateUpdate()
		{
		}

		// Token: 0x060045F1 RID: 17905 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600404B")]
		[Address(RVA = "0x10154A6A4", Offset = "0x154A6A4", VA = "0x10154A6A4")]
		private void Update()
		{
		}

		// Token: 0x060045F2 RID: 17906 RVA: 0x00019E00 File Offset: 0x00018000
		[Token(Token = "0x600404C")]
		[Address(RVA = "0x10154B55C", Offset = "0x154B55C", VA = "0x10154B55C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060045F3 RID: 17907 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600404D")]
		[Address(RVA = "0x10154B598", Offset = "0x154B598", VA = "0x10154B598")]
		public void SetEnableObjInput(bool flg)
		{
		}

		// Token: 0x060045F4 RID: 17908 RVA: 0x00019E18 File Offset: 0x00018018
		[Token(Token = "0x600404E")]
		[Address(RVA = "0x10154B628", Offset = "0x154B628", VA = "0x10154B628")]
		public bool CheckRaycast()
		{
			return default(bool);
		}

		// Token: 0x060045F5 RID: 17909 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600404F")]
		[Address(RVA = "0x10154B658", Offset = "0x154B658", VA = "0x10154B658")]
		public void SetEnableGreet(bool flg)
		{
		}

		// Token: 0x060045F6 RID: 17910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004050")]
		[Address(RVA = "0x10154AA58", Offset = "0x154AA58", VA = "0x10154AA58")]
		public void OpenMainWindow()
		{
		}

		// Token: 0x060045F7 RID: 17911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004051")]
		[Address(RVA = "0x10154B660", Offset = "0x154B660", VA = "0x10154B660")]
		public void CloseMainWindow()
		{
		}

		// Token: 0x060045F8 RID: 17912 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004052")]
		[Address(RVA = "0x10154B694", Offset = "0x154B694", VA = "0x10154B694")]
		public UIGroup GetObjInfoWindow()
		{
			return null;
		}

		// Token: 0x060045F9 RID: 17913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004053")]
		[Address(RVA = "0x10154B69C", Offset = "0x154B69C", VA = "0x10154B69C")]
		public void OpenObjInfoWindow(eRoomObjectCategory category, int roomObjID)
		{
		}

		// Token: 0x060045FA RID: 17914 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004054")]
		[Address(RVA = "0x10154B718", Offset = "0x154B718", VA = "0x10154B718")]
		public UIGroup GetObjConfirmWindow()
		{
			return null;
		}

		// Token: 0x060045FB RID: 17915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004055")]
		[Address(RVA = "0x10154B720", Offset = "0x154B720", VA = "0x10154B720")]
		public void OpenObjConfirmWindow(eRoomObjectCategory category, int roomObjID, RoomObjConfirmWindow.eMode mode, UnityAction OnClose)
		{
		}

		// Token: 0x060045FC RID: 17916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004056")]
		[Address(RVA = "0x10154B898", Offset = "0x154B898", VA = "0x10154B898")]
		public void OpenObjConfirmWindow2(eRoomObjectCategory category, int roomObjID, RoomObjConfirmWindow.eMode mode, UnityAction OnClose)
		{
		}

		// Token: 0x060045FD RID: 17917 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004057")]
		[Address(RVA = "0x10154B934", Offset = "0x154B934", VA = "0x10154B934")]
		public RoomObjBuyWindow GetObjBuyWindow()
		{
			return null;
		}

		// Token: 0x060045FE RID: 17918 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004058")]
		[Address(RVA = "0x10154B93C", Offset = "0x154B93C", VA = "0x10154B93C")]
		public RoomObjSelectWindow GetRoomObjSelectWindow()
		{
			return null;
		}

		// Token: 0x060045FF RID: 17919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004059")]
		[Address(RVA = "0x10154B944", Offset = "0x154B944", VA = "0x10154B944")]
		public void OpenSelectWindowInfo(eRoomObjectCategory category, int objID)
		{
		}

		// Token: 0x06004600 RID: 17920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405A")]
		[Address(RVA = "0x10154B9E4", Offset = "0x154B9E4", VA = "0x10154B9E4")]
		public void OpenSelectWindowEdit(eRoomObjectCategory category, int objID, bool isMove, int editingObjMax)
		{
		}

		// Token: 0x06004601 RID: 17921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405B")]
		[Address(RVA = "0x10154BAF8", Offset = "0x154BAF8", VA = "0x10154BAF8")]
		public void OpenSelectWindowBuy(eRoomObjectCategory category, int objID, int editingObjMax)
		{
		}

		// Token: 0x06004602 RID: 17922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405C")]
		[Address(RVA = "0x10154BBC4", Offset = "0x154BBC4", VA = "0x10154BBC4")]
		public void OpenSelectWindowMission()
		{
		}

		// Token: 0x06004603 RID: 17923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405D")]
		[Address(RVA = "0x10154BC48", Offset = "0x154BC48", VA = "0x10154BC48")]
		public void CloseSelectWindow()
		{
		}

		// Token: 0x06004604 RID: 17924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405E")]
		[Address(RVA = "0x10154BC7C", Offset = "0x154BC7C", VA = "0x10154BC7C")]
		public void RequestGreet(long charaMngID, RoomGreet.eCategory category)
		{
		}

		// Token: 0x06004605 RID: 17925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600405F")]
		[Address(RVA = "0x10154AACC", Offset = "0x154AACC", VA = "0x10154AACC")]
		public void OpenGreet()
		{
		}

		// Token: 0x06004606 RID: 17926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004060")]
		[Address(RVA = "0x10154C154", Offset = "0x154C154", VA = "0x10154C154")]
		public void OnCloseGreetCallBack()
		{
		}

		// Token: 0x06004607 RID: 17927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004061")]
		[Address(RVA = "0x10154C180", Offset = "0x154C180", VA = "0x10154C180")]
		public void SetEnablePlacementButton(bool flg)
		{
		}

		// Token: 0x06004608 RID: 17928 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004062")]
		[Address(RVA = "0x10154C1B4", Offset = "0x154C1B4", VA = "0x10154C1B4")]
		public ADVButton GetADVButton()
		{
			return null;
		}

		// Token: 0x06004609 RID: 17929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004063")]
		[Address(RVA = "0x10154C1BC", Offset = "0x154C1BC", VA = "0x10154C1BC")]
		public void OnClickMoveButtonCallBack()
		{
		}

		// Token: 0x0600460A RID: 17930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004064")]
		[Address(RVA = "0x10154C1C8", Offset = "0x154C1C8", VA = "0x10154C1C8")]
		public void OnClickInfoButtonCallBack()
		{
		}

		// Token: 0x0600460B RID: 17931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004065")]
		[Address(RVA = "0x10154C1D4", Offset = "0x154C1D4", VA = "0x10154C1D4")]
		public void OnClickPutAwayButtonCallBack()
		{
		}

		// Token: 0x0600460C RID: 17932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004066")]
		[Address(RVA = "0x10154C1E0", Offset = "0x154C1E0", VA = "0x10154C1E0")]
		public void OnClickPlaceButtonCallBack()
		{
		}

		// Token: 0x0600460D RID: 17933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004067")]
		[Address(RVA = "0x10154C1EC", Offset = "0x154C1EC", VA = "0x10154C1EC")]
		public void OnClickBuyButtonCallBack()
		{
		}

		// Token: 0x0600460E RID: 17934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004068")]
		[Address(RVA = "0x10154C1F8", Offset = "0x154C1F8", VA = "0x10154C1F8")]
		public void OnClickFlipButtonCallBack()
		{
		}

		// Token: 0x0600460F RID: 17935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004069")]
		[Address(RVA = "0x10154C204", Offset = "0x154C204", VA = "0x10154C204")]
		public void OnClickPlaceCancelButtonCallBack()
		{
		}

		// Token: 0x06004610 RID: 17936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406A")]
		[Address(RVA = "0x10154C210", Offset = "0x154C210", VA = "0x10154C210")]
		public void OnClickHaveButtonCallBack()
		{
		}

		// Token: 0x06004611 RID: 17937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406B")]
		[Address(RVA = "0x10154C274", Offset = "0x154C274", VA = "0x10154C274")]
		public void OnClickShopButtonCallBack()
		{
		}

		// Token: 0x06004612 RID: 17938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406C")]
		[Address(RVA = "0x10154C2E8", Offset = "0x154C2E8", VA = "0x10154C2E8")]
		public void OnClickStoreAllButtonCallBack()
		{
		}

		// Token: 0x06004613 RID: 17939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406D")]
		[Address(RVA = "0x10154C2F4", Offset = "0x154C2F4", VA = "0x10154C2F4")]
		public void OpenObjSelectGroup()
		{
		}

		// Token: 0x06004614 RID: 17940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406E")]
		[Address(RVA = "0x10154C35C", Offset = "0x154C35C", VA = "0x10154C35C")]
		public void CloseObjSelectGroup()
		{
		}

		// Token: 0x06004615 RID: 17941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600406F")]
		[Address(RVA = "0x10154C390", Offset = "0x154C390", VA = "0x10154C390")]
		public void OnClickCancelObjSelect()
		{
		}

		// Token: 0x06004616 RID: 17942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004070")]
		[Address(RVA = "0x10154C3D4", Offset = "0x154C3D4", VA = "0x10154C3D4")]
		public void OnClickScheduleButtonCallBack()
		{
		}

		// Token: 0x06004617 RID: 17943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004071")]
		[Address(RVA = "0x10154C3E0", Offset = "0x154C3E0", VA = "0x10154C3E0")]
		public void OpenScheduleWindow(int roomIdx, long[] liveCharaMngID, RoomBuilder builder)
		{
		}

		// Token: 0x06004618 RID: 17944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004072")]
		[Address(RVA = "0x10154C734", Offset = "0x154C734", VA = "0x10154C734")]
		public void OnDecideLiveMember()
		{
		}

		// Token: 0x06004619 RID: 17945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004073")]
		[Address(RVA = "0x10154C7A4", Offset = "0x154C7A4", VA = "0x10154C7A4")]
		public void DecideScheduleCharaChange()
		{
		}

		// Token: 0x0600461A RID: 17946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004074")]
		[Address(RVA = "0x10154C7D8", Offset = "0x154C7D8", VA = "0x10154C7D8")]
		public void OnCloseScheduleCallBack()
		{
		}

		// Token: 0x0600461B RID: 17947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004075")]
		[Address(RVA = "0x10154C864", Offset = "0x154C864", VA = "0x10154C864")]
		public void OnClickHideButtonCallBack()
		{
		}

		// Token: 0x0600461C RID: 17948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004076")]
		[Address(RVA = "0x10154C870", Offset = "0x154C870", VA = "0x10154C870")]
		public void StartUIHide()
		{
		}

		// Token: 0x0600461D RID: 17949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004077")]
		[Address(RVA = "0x10154B470", Offset = "0x154B470", VA = "0x10154B470")]
		public void EndUIHide(bool forceEnd)
		{
		}

		// Token: 0x0600461E RID: 17950 RVA: 0x00019E30 File Offset: 0x00018030
		[Token(Token = "0x6004078")]
		[Address(RVA = "0x10154C93C", Offset = "0x154C93C", VA = "0x10154C93C")]
		public bool IsUIHide()
		{
			return default(bool);
		}

		// Token: 0x0600461F RID: 17951 RVA: 0x00019E48 File Offset: 0x00018048
		[Token(Token = "0x6004079")]
		[Address(RVA = "0x10154C978", Offset = "0x154C978", VA = "0x10154C978")]
		public bool IsOpenScheduleWindow()
		{
			return default(bool);
		}

		// Token: 0x06004620 RID: 17952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407A")]
		[Address(RVA = "0x10154C9B4", Offset = "0x154C9B4", VA = "0x10154C9B4")]
		public void OnClickRoomChangeButtonCallBack()
		{
		}

		// Token: 0x06004621 RID: 17953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407B")]
		[Address(RVA = "0x10154C9C0", Offset = "0x154C9C0", VA = "0x10154C9C0")]
		public void OnClickBackButtonCallBack_Main()
		{
		}

		// Token: 0x06004622 RID: 17954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407C")]
		[Address(RVA = "0x10154C9CC", Offset = "0x154C9CC", VA = "0x10154C9CC")]
		public void OnClickBackButtonCallBack_Selecting()
		{
		}

		// Token: 0x06004623 RID: 17955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407D")]
		[Address(RVA = "0x10154C9D8", Offset = "0x154C9D8", VA = "0x10154C9D8")]
		public void OnClickBackButtonCallBack_StoreEdit()
		{
		}

		// Token: 0x06004624 RID: 17956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407E")]
		[Address(RVA = "0x10154CA1C", Offset = "0x154CA1C", VA = "0x10154CA1C")]
		public void OnClickBackButtonCallBack_ShopEdit()
		{
		}

		// Token: 0x06004625 RID: 17957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600407F")]
		[Address(RVA = "0x10154CA60", Offset = "0x154CA60", VA = "0x10154CA60")]
		public void OpenFriendListWindow()
		{
		}

		// Token: 0x06004626 RID: 17958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004080")]
		[Address(RVA = "0x10154CA90", Offset = "0x154CA90", VA = "0x10154CA90")]
		public void OnRoomChangeCallBack()
		{
		}

		// Token: 0x06004627 RID: 17959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004081")]
		[Address(RVA = "0x10154CAF4", Offset = "0x154CAF4", VA = "0x10154CAF4")]
		public void OnReturnPlayerRoomButtonCallBack()
		{
		}

		// Token: 0x06004628 RID: 17960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004082")]
		[Address(RVA = "0x10154CBBC", Offset = "0x154CBBC", VA = "0x10154CBBC")]
		public void OpenSpecial(long mngID, int tweetID)
		{
		}

		// Token: 0x06004629 RID: 17961 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004083")]
		[Address(RVA = "0x10154CC04", Offset = "0x154CC04", VA = "0x10154CC04")]
		public RoomObjCancelSendStockWindow GetRoomObjCancelSendStockWindow()
		{
			return null;
		}

		// Token: 0x0600462A RID: 17962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004084")]
		[Address(RVA = "0x10154CC0C", Offset = "0x154CC0C", VA = "0x10154CC0C")]
		public RoomUI()
		{
		}

		// Token: 0x040056D2 RID: 22226
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003D0F")]
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x040056D3 RID: 22227
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003D10")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040056D4 RID: 22228
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003D11")]
		[SerializeField]
		private Text m_ClockTextObj;

		// Token: 0x040056D5 RID: 22229
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003D12")]
		[SerializeField]
		private RoomMainWindow m_MainWindow;

		// Token: 0x040056D6 RID: 22230
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003D13")]
		[SerializeField]
		private RoomGreet m_Greet;

		// Token: 0x040056D7 RID: 22231
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003D14")]
		[SerializeField]
		private RoomObjSelectWindow m_SelectWindow;

		// Token: 0x040056D8 RID: 22232
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003D15")]
		[SerializeField]
		private RoomObjInfoWindow m_ObjInfoWindow;

		// Token: 0x040056D9 RID: 22233
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003D16")]
		[SerializeField]
		private RoomObjConfirmWindow m_ObjConfirmWindow;

		// Token: 0x040056DA RID: 22234
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003D17")]
		[SerializeField]
		private RoomObjBuyWindow m_ObjBuyWindow;

		// Token: 0x040056DB RID: 22235
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003D18")]
		[SerializeField]
		private UIGroup m_ObjSelectGroup;

		// Token: 0x040056DC RID: 22236
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003D19")]
		[SerializeField]
		private ScheduleWindow m_ScheduleWindow;

		// Token: 0x040056DD RID: 22237
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003D1A")]
		[SerializeField]
		private UIGroup m_UIHideGroup;

		// Token: 0x040056DE RID: 22238
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003D1B")]
		[SerializeField]
		private RoomFriendInfo m_FriendInfo;

		// Token: 0x040056DF RID: 22239
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003D1C")]
		[SerializeField]
		private CustomButton m_HideButton;

		// Token: 0x040056E0 RID: 22240
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003D1D")]
		[SerializeField]
		private CustomButton m_RoomChangeButton;

		// Token: 0x040056E1 RID: 22241
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003D1E")]
		[SerializeField]
		private RoomFriendListWindow m_FriendListWindow;

		// Token: 0x040056E2 RID: 22242
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003D1F")]
		[SerializeField]
		private GameObject m_ReturnButton;

		// Token: 0x040056E3 RID: 22243
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003D20")]
		[SerializeField]
		private GameObject m_SlideMenu;

		// Token: 0x040056E4 RID: 22244
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003D21")]
		[SerializeField]
		private GameObject m_Invisible;

		// Token: 0x040056E5 RID: 22245
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003D22")]
		[SerializeField]
		private SpecialTweet m_SpecialTweet;

		// Token: 0x040056E6 RID: 22246
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003D23")]
		[SerializeField]
		private CustomButton m_SlideButton;

		// Token: 0x040056E7 RID: 22247
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003D24")]
		[SerializeField]
		private CustomButton m_ShopButton;

		// Token: 0x040056E8 RID: 22248
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003D25")]
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x040056E9 RID: 22249
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003D26")]
		[SerializeField]
		private RoomObjCancelSendStockWindow m_RoomObjCancelSendStockWindow;

		// Token: 0x040056EA RID: 22250
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003D27")]
		private bool m_IsPlayerRoom;

		// Token: 0x040056EB RID: 22251
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003D28")]
		private List<RoomUI.GreetRequest> m_GreetStack;

		// Token: 0x040056EC RID: 22252
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003D29")]
		private bool m_IsEnableOpenGreet;

		// Token: 0x040056ED RID: 22253
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003D2A")]
		private RoomMain m_Owner;

		// Token: 0x040056EE RID: 22254
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003D2B")]
		private bool m_IsUIHideSelf;

		// Token: 0x040056EF RID: 22255
		[Cpp2IlInjected.FieldOffset(Offset = "0x124")]
		[Token(Token = "0x4003D2C")]
		private float m_UIHideCount;

		// Token: 0x040056F0 RID: 22256
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003D2D")]
		private RoomShopListUI m_ShopUI;

		// Token: 0x04005703 RID: 22275
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C0")]
		[Token(Token = "0x4003D40")]
		private RoomUI.eRoomTutorialStep m_TutorialStep;

		// Token: 0x04005704 RID: 22276
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C4")]
		[Token(Token = "0x4003D41")]
		private bool m_IsDirtyTutorialUI;

		// Token: 0x02000E98 RID: 3736
		[Token(Token = "0x2001171")]
		private enum eUIState
		{
			// Token: 0x04005706 RID: 22278
			[Token(Token = "0x4006D0D")]
			Hide,
			// Token: 0x04005707 RID: 22279
			[Token(Token = "0x4006D0E")]
			In,
			// Token: 0x04005708 RID: 22280
			[Token(Token = "0x4006D0F")]
			Out,
			// Token: 0x04005709 RID: 22281
			[Token(Token = "0x4006D10")]
			End,
			// Token: 0x0400570A RID: 22282
			[Token(Token = "0x4006D11")]
			Main,
			// Token: 0x0400570B RID: 22283
			[Token(Token = "0x4006D12")]
			Select,
			// Token: 0x0400570C RID: 22284
			[Token(Token = "0x4006D13")]
			Window,
			// Token: 0x0400570D RID: 22285
			[Token(Token = "0x4006D14")]
			Greet,
			// Token: 0x0400570E RID: 22286
			[Token(Token = "0x4006D15")]
			Schedule,
			// Token: 0x0400570F RID: 22287
			[Token(Token = "0x4006D16")]
			ObjSelect,
			// Token: 0x04005710 RID: 22288
			[Token(Token = "0x4006D17")]
			UIHide,
			// Token: 0x04005711 RID: 22289
			[Token(Token = "0x4006D18")]
			Num
		}

		// Token: 0x02000E99 RID: 3737
		[Token(Token = "0x2001172")]
		private class GreetRequest
		{
			// Token: 0x0600462B RID: 17963 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006259")]
			[Address(RVA = "0x10154C0A4", Offset = "0x154C0A4", VA = "0x10154C0A4")]
			public GreetRequest(long charaMngID, RoomGreet.eCategory category)
			{
			}

			// Token: 0x04005712 RID: 22290
			[Token(Token = "0x4006D19")]
			public const float LIFE = 600f;

			// Token: 0x04005713 RID: 22291
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006D1A")]
			public List<long> m_CharaMngIDs;

			// Token: 0x04005714 RID: 22292
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006D1B")]
			public RoomGreet.eCategory m_Category;

			// Token: 0x04005715 RID: 22293
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006D1C")]
			public float m_StockTime;
		}

		// Token: 0x02000E9A RID: 3738
		[Token(Token = "0x2001173")]
		public enum eRoomTutorialStep
		{
			// Token: 0x04005717 RID: 22295
			[Token(Token = "0x4006D1E")]
			None,
			// Token: 0x04005718 RID: 22296
			[Token(Token = "0x4006D1F")]
			OpenSlide,
			// Token: 0x04005719 RID: 22297
			[Token(Token = "0x4006D20")]
			OpenShop
		}
	}
}
