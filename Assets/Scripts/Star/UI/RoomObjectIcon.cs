﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CF8 RID: 3320
	[Token(Token = "0x20008E1")]
	[StructLayout(3)]
	public class RoomObjectIcon : ASyncImage
	{
		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06003CA3 RID: 15523 RVA: 0x00018720 File Offset: 0x00016920
		[Token(Token = "0x17000447")]
		public eRoomObjectCategory Category
		{
			[Token(Token = "0x6003760")]
			[Address(RVA = "0x10154CC84", Offset = "0x154CC84", VA = "0x10154CC84")]
			get
			{
				return eRoomObjectCategory.Desk;
			}
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06003CA4 RID: 15524 RVA: 0x00018738 File Offset: 0x00016938
		[Token(Token = "0x17000448")]
		public int RoomObjectId
		{
			[Token(Token = "0x6003761")]
			[Address(RVA = "0x10154CC8C", Offset = "0x154CC8C", VA = "0x10154CC8C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06003CA5 RID: 15525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003762")]
		[Address(RVA = "0x10153C3FC", Offset = "0x153C3FC", VA = "0x10153C3FC")]
		public void Apply(eRoomObjectCategory category, int roomObjectId)
		{
		}

		// Token: 0x06003CA6 RID: 15526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003763")]
		[Address(RVA = "0x10154CC94", Offset = "0x154CC94", VA = "0x10154CC94", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CA7 RID: 15527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003764")]
		[Address(RVA = "0x10154CCC4", Offset = "0x154CCC4", VA = "0x10154CCC4")]
		public RoomObjectIcon()
		{
		}

		// Token: 0x04004BEA RID: 19434
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003558")]
		protected eRoomObjectCategory m_RoomObjectCategory;

		// Token: 0x04004BEB RID: 19435
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003559")]
		protected int m_RoomObjectId;
	}
}
