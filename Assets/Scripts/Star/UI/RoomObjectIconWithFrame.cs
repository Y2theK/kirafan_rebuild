﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CF9 RID: 3321
	[Token(Token = "0x20008E2")]
	[StructLayout(3)]
	public class RoomObjectIconWithFrame : MonoBehaviour
	{
		// Token: 0x06003CA8 RID: 15528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003765")]
		[Address(RVA = "0x10154CCD8", Offset = "0x154CCD8", VA = "0x10154CCD8")]
		public void Apply(eRoomObjectCategory category, int roomObjectID)
		{
		}

		// Token: 0x06003CA9 RID: 15529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003766")]
		[Address(RVA = "0x10154CD1C", Offset = "0x154CD1C", VA = "0x10154CD1C")]
		public void Destroy()
		{
		}

		// Token: 0x06003CAA RID: 15530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003767")]
		[Address(RVA = "0x10154CDBC", Offset = "0x154CDBC", VA = "0x10154CDBC")]
		public RoomObjectIconWithFrame()
		{
		}

		// Token: 0x04004BEC RID: 19436
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400355A")]
		[SerializeField]
		private RoomObjectIcon m_Icon;
	}
}
