﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C70 RID: 3184
	[Token(Token = "0x200088D")]
	[StructLayout(3)]
	public class ScaleAnimUI : AnimUIBase
	{
		// Token: 0x060039E4 RID: 14820 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CB")]
		[Address(RVA = "0x10154CDC4", Offset = "0x154CDC4", VA = "0x10154CDC4", Slot = "6")]
		protected override void Prepare()
		{
		}

		// Token: 0x060039E5 RID: 14821 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CC")]
		[Address(RVA = "0x10154CE94", Offset = "0x154CE94", VA = "0x10154CE94", Slot = "9")]
		protected override void ExecutePlayIn()
		{
		}

		// Token: 0x060039E6 RID: 14822 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CD")]
		[Address(RVA = "0x10154D1DC", Offset = "0x154D1DC", VA = "0x10154D1DC", Slot = "10")]
		protected override void ExecutePlayOut()
		{
		}

		// Token: 0x060039E7 RID: 14823 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CE")]
		[Address(RVA = "0x10154D524", Offset = "0x154D524", VA = "0x10154D524", Slot = "5")]
		public override void Hide()
		{
		}

		// Token: 0x060039E8 RID: 14824 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034CF")]
		[Address(RVA = "0x10154D5E8", Offset = "0x154D5E8", VA = "0x10154D5E8", Slot = "4")]
		public override void Show()
		{
		}

		// Token: 0x060039E9 RID: 14825 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D0")]
		[Address(RVA = "0x10154D6D4", Offset = "0x154D6D4", VA = "0x10154D6D4")]
		public void OnCompleteIn()
		{
		}

		// Token: 0x060039EA RID: 14826 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D1")]
		[Address(RVA = "0x10154D73C", Offset = "0x154D73C", VA = "0x10154D73C")]
		public void OnCompleteOut()
		{
		}

		// Token: 0x060039EB RID: 14827 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D2")]
		[Address(RVA = "0x10154D7A4", Offset = "0x154D7A4", VA = "0x10154D7A4")]
		public ScaleAnimUI()
		{
		}

		// Token: 0x040048BC RID: 18620
		[Token(Token = "0x4003324")]
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x040048BD RID: 18621
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003325")]
		[SerializeField]
		private bool m_ReturnMode;

		// Token: 0x040048BE RID: 18622
		[Cpp2IlInjected.FieldOffset(Offset = "0x31")]
		[Token(Token = "0x4003326")]
		[SerializeField]
		private bool m_Restart;

		// Token: 0x040048BF RID: 18623
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003327")]
		[SerializeField]
		private Vector3 m_HideScale;

		// Token: 0x040048C0 RID: 18624
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003328")]
		[SerializeField]
		private Vector3 m_ShowScale;

		// Token: 0x040048C1 RID: 18625
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x4003329")]
		[SerializeField]
		private Vector3 m_EndScale;

		// Token: 0x040048C2 RID: 18626
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400332A")]
		[SerializeField]
		private float m_AnimDuration;

		// Token: 0x040048C3 RID: 18627
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x400332B")]
		[SerializeField]
		private float m_Delay;

		// Token: 0x040048C4 RID: 18628
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400332C")]
		[SerializeField]
		private iTween.EaseType m_EaseType;

		// Token: 0x040048C5 RID: 18629
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400332D")]
		private GameObject m_GameObject;

		// Token: 0x040048C6 RID: 18630
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400332E")]
		private RectTransform m_RectTransform;
	}
}
