﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI
{
	// Token: 0x02000DD5 RID: 3541
	[Token(Token = "0x2000981")]
	[StructLayout(3)]
	public class SceneInfoSetter : MonoBehaviour
	{
		// Token: 0x06004169 RID: 16745 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF6")]
		[Address(RVA = "0x10154D85C", Offset = "0x154D85C", VA = "0x10154D85C")]
		public void Open()
		{
		}

		// Token: 0x0600416A RID: 16746 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF7")]
		[Address(RVA = "0x10154DAD0", Offset = "0x154DAD0", VA = "0x10154DAD0")]
		public void Close()
		{
		}

		// Token: 0x0600416B RID: 16747 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF8")]
		[Address(RVA = "0x10154DBBC", Offset = "0x154DBBC", VA = "0x10154DBBC")]
		public void OnClickBackButtonCallBack(bool isShortCut)
		{
		}

		// Token: 0x0600416C RID: 16748 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003BF9")]
		[Address(RVA = "0x10154DBFC", Offset = "0x154DBFC", VA = "0x10154DBFC")]
		public SceneInfoSetter()
		{
		}

		// Token: 0x04005101 RID: 20737
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400394A")]
		[SerializeField]
		private UnityAction OnClickBackButton;

		// Token: 0x04005102 RID: 20738
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400394B")]
		[SerializeField]
		private UnityAction OnClickShortCutButton;

		// Token: 0x04005103 RID: 20739
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400394C")]
		[SerializeField]
		private GlobalSceneInfoWindow.eBackButtonIconType m_BackButtonType;

		// Token: 0x04005104 RID: 20740
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400394D")]
		[SerializeField]
		private string m_SceneDefineName;

		// Token: 0x04005105 RID: 20741
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400394E")]
		private Action<bool> m_SavedCallBack;

		// Token: 0x04005106 RID: 20742
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400394F")]
		private GlobalUI.SceneInfoStack m_SceneInfoStack;
	}
}
