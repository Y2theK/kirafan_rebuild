﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6A RID: 3690
	[Token(Token = "0x20009D1")]
	[StructLayout(3)]
	public class ScheduleChara : MonoBehaviour
	{
		// Token: 0x1400008E RID: 142
		// (add) Token: 0x060044C5 RID: 17605 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060044C6 RID: 17606 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400008E")]
		public event Action<int> OnClickScheduleChara
		{
			[Token(Token = "0x6003F31")]
			[Address(RVA = "0x10154DC04", Offset = "0x154DC04", VA = "0x10154DC04")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003F32")]
			[Address(RVA = "0x10154DD10", Offset = "0x154DD10", VA = "0x10154DD10")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060044C7 RID: 17607 RVA: 0x00019A70 File Offset: 0x00017C70
		[Token(Token = "0x6003F33")]
		[Address(RVA = "0x10154DE1C", Offset = "0x154DE1C", VA = "0x10154DE1C")]
		public long GetCharaMngID()
		{
			return 0L;
		}

		// Token: 0x060044C8 RID: 17608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F34")]
		[Address(RVA = "0x10154DE24", Offset = "0x154DE24", VA = "0x10154DE24")]
		public void SetInteractable(bool flg)
		{
		}

		// Token: 0x060044C9 RID: 17609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F35")]
		[Address(RVA = "0x10154DE5C", Offset = "0x154DE5C", VA = "0x10154DE5C")]
		public void Apply(int idx, long charaMngID)
		{
		}

		// Token: 0x060044CA RID: 17610 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F36")]
		[Address(RVA = "0x10154DE68", Offset = "0x154DE68", VA = "0x10154DE68")]
		public void Apply()
		{
		}

		// Token: 0x060044CB RID: 17611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F37")]
		[Address(RVA = "0x10154E03C", Offset = "0x154E03C", VA = "0x10154E03C")]
		public void Refresh()
		{
		}

		// Token: 0x060044CC RID: 17612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F38")]
		[Address(RVA = "0x10154E114", Offset = "0x154E114", VA = "0x10154E114")]
		public void Destroy()
		{
		}

		// Token: 0x060044CD RID: 17613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F39")]
		[Address(RVA = "0x10154E18C", Offset = "0x154E18C", VA = "0x10154E18C")]
		public void OnClickCallBack()
		{
		}

		// Token: 0x060044CE RID: 17614 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3A")]
		[Address(RVA = "0x10154E1E0", Offset = "0x154E1E0", VA = "0x10154E1E0")]
		public void OnHoldCallBack()
		{
		}

		// Token: 0x060044CF RID: 17615 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3B")]
		[Address(RVA = "0x10154E2C8", Offset = "0x154E2C8", VA = "0x10154E2C8")]
		public void OnCloseDetail()
		{
		}

		// Token: 0x060044D0 RID: 17616 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3C")]
		[Address(RVA = "0x10154E2CC", Offset = "0x154E2CC", VA = "0x10154E2CC")]
		public ScheduleChara()
		{
		}

		// Token: 0x040055B3 RID: 21939
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C50")]
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x040055B4 RID: 21940
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C51")]
		[SerializeField]
		private GameObject m_EmptyBody;

		// Token: 0x040055B5 RID: 21941
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003C52")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x040055B6 RID: 21942
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003C53")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040055B7 RID: 21943
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003C54")]
		private int m_MemberIdx;

		// Token: 0x040055B8 RID: 21944
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003C55")]
		private long m_CharaMngID;
	}
}
