﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6B RID: 3691
	[Token(Token = "0x20009D2")]
	[StructLayout(3)]
	public class ScheduleCharaSwapGroup : UIGroup
	{
		// Token: 0x060044D1 RID: 17617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3D")]
		[Address(RVA = "0x10154E2DC", Offset = "0x154E2DC", VA = "0x10154E2DC")]
		public void Open(long outMngID, long inMngID, Action OnDecide)
		{
		}

		// Token: 0x060044D2 RID: 17618 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3E")]
		[Address(RVA = "0x10154E5AC", Offset = "0x154E5AC", VA = "0x10154E5AC")]
		public void OnClickDecideCallBack()
		{
		}

		// Token: 0x060044D3 RID: 17619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F3F")]
		[Address(RVA = "0x10154E5E0", Offset = "0x154E5E0", VA = "0x10154E5E0")]
		public ScheduleCharaSwapGroup()
		{
		}

		// Token: 0x040055BA RID: 21946
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C57")]
		[SerializeField]
		private PrefabCloner m_OutCharaCloner;

		// Token: 0x040055BB RID: 21947
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C58")]
		[SerializeField]
		private Text m_OutCharaNameText;

		// Token: 0x040055BC RID: 21948
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003C59")]
		[SerializeField]
		private PrefabCloner m_InCharaCloner;

		// Token: 0x040055BD RID: 21949
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003C5A")]
		[SerializeField]
		private Text m_InCharaNameText;

		// Token: 0x040055BE RID: 21950
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003C5B")]
		private Action m_OnDecide;
	}
}
