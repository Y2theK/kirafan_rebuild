﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6C RID: 3692
	[Token(Token = "0x20009D3")]
	[StructLayout(3)]
	public class ScheduleItem : MonoBehaviour
	{
		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x060044D4 RID: 17620 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700049E")]
		public RectTransform RectTransform
		{
			[Token(Token = "0x6003F40")]
			[Address(RVA = "0x10154E5E8", Offset = "0x154E5E8", VA = "0x10154E5E8")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x060044D5 RID: 17621 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700049F")]
		public GameObject GameObject
		{
			[Token(Token = "0x6003F41")]
			[Address(RVA = "0x10154E680", Offset = "0x154E680", VA = "0x10154E680")]
			get
			{
				return null;
			}
		}

		// Token: 0x060044D6 RID: 17622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F42")]
		[Address(RVA = "0x10154E710", Offset = "0x154E710", VA = "0x10154E710")]
		public void SetSchedule(string scheduleName, Sprite baseSprite, float length)
		{
		}

		// Token: 0x060044D7 RID: 17623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F43")]
		[Address(RVA = "0x10154E898", Offset = "0x154E898", VA = "0x10154E898")]
		public ScheduleItem()
		{
		}

		// Token: 0x040055BF RID: 21951
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C5C")]
		[SerializeField]
		private Image m_BaseImage;

		// Token: 0x040055C0 RID: 21952
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003C5D")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040055C1 RID: 21953
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003C5E")]
		private RectTransform m_Rect;

		// Token: 0x040055C2 RID: 21954
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003C5F")]
		private GameObject m_GameObject;
	}
}
