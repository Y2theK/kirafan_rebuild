﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6D RID: 3693
	[Token(Token = "0x20009D4")]
	[StructLayout(3)]
	public class ScheduleTimeItem : ScrollItemIcon
	{
		// Token: 0x060044D8 RID: 17624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F44")]
		[Address(RVA = "0x10154E8A0", Offset = "0x154E8A0", VA = "0x10154E8A0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x060044D9 RID: 17625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F45")]
		[Address(RVA = "0x10154EA28", Offset = "0x154EA28", VA = "0x10154EA28")]
		public ScheduleTimeItem()
		{
		}

		// Token: 0x040055C3 RID: 21955
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003C60")]
		[SerializeField]
		private Text m_Text;
	}
}
