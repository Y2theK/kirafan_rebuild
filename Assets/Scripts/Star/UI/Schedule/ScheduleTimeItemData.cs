﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6E RID: 3694
	[Token(Token = "0x20009D5")]
	[StructLayout(3)]
	public class ScheduleTimeItemData : ScrollItemData
	{
		// Token: 0x060044DA RID: 17626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F46")]
		[Address(RVA = "0x10154EA38", Offset = "0x154EA38", VA = "0x10154EA38")]
		public ScheduleTimeItemData()
		{
		}

		// Token: 0x040055C4 RID: 21956
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003C61")]
		public string m_Text;
	}
}
