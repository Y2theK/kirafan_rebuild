﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.CharaList;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000E6F RID: 3695
	[Token(Token = "0x20009D6")]
	[StructLayout(3)]
	public class ScheduleWindow : UIGroup
	{
		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x060044DB RID: 17627 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004A0")]
		public long[] LiveCharaMngIDs
		{
			[Token(Token = "0x6003F47")]
			[Address(RVA = "0x10154C79C", Offset = "0x154C79C", VA = "0x10154C79C")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x060044DC RID: 17628 RVA: 0x00019A88 File Offset: 0x00017C88
		[Token(Token = "0x170004A1")]
		public bool IsEnd
		{
			[Token(Token = "0x6003F48")]
			[Address(RVA = "0x10154C838", Offset = "0x154C838", VA = "0x10154C838")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1400008F RID: 143
		// (add) Token: 0x060044DD RID: 17629 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060044DE RID: 17630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400008F")]
		public event Action OnChangeChara
		{
			[Token(Token = "0x6003F49")]
			[Address(RVA = "0x101549F1C", Offset = "0x1549F1C", VA = "0x101549F1C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003F4A")]
			[Address(RVA = "0x10154EADC", Offset = "0x154EADC", VA = "0x10154EADC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x060044DF RID: 17631 RVA: 0x00019AA0 File Offset: 0x00017CA0
		[Token(Token = "0x6003F4B")]
		[Address(RVA = "0x10154EA40", Offset = "0x154EA40", VA = "0x10154EA40")]
		public bool IsExistEdit()
		{
			return default(bool);
		}

		// Token: 0x060044E0 RID: 17632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F4C")]
		[Address(RVA = "0x101549B0C", Offset = "0x1549B0C", VA = "0x101549B0C")]
		public void Setup()
		{
		}

		// Token: 0x060044E1 RID: 17633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F4D")]
		[Address(RVA = "0x10154EEAC", Offset = "0x154EEAC", VA = "0x10154EEAC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x060044E2 RID: 17634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F4E")]
		[Address(RVA = "0x10154F524", Offset = "0x154F524", VA = "0x10154F524", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x060044E3 RID: 17635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F4F")]
		[Address(RVA = "0x10154F248", Offset = "0x154F248", VA = "0x10154F248")]
		public void OnScrollValueChanged()
		{
		}

		// Token: 0x060044E4 RID: 17636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F50")]
		[Address(RVA = "0x10154F5F4", Offset = "0x154F5F4", VA = "0x10154F5F4")]
		private void Apply(long[] charaMngIDs, bool[] editModes)
		{
		}

		// Token: 0x060044E5 RID: 17637 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003F51")]
		[Address(RVA = "0x1015502F4", Offset = "0x15502F4", VA = "0x1015502F4")]
		private ScheduleItem ScoopInst(RectTransform parent)
		{
			return null;
		}

		// Token: 0x060044E6 RID: 17638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F52")]
		[Address(RVA = "0x101550164", Offset = "0x1550164", VA = "0x101550164")]
		private void SinkAll()
		{
		}

		// Token: 0x060044E7 RID: 17639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F53")]
		[Address(RVA = "0x10154F484", Offset = "0x154F484", VA = "0x10154F484")]
		private void SetRemainTime(int remainTimeSec)
		{
		}

		// Token: 0x060044E8 RID: 17640 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F54")]
		[Address(RVA = "0x10154EBEC", Offset = "0x154EBEC", VA = "0x10154EBEC")]
		private void ApplyEnableEdit(bool flg)
		{
		}

		// Token: 0x060044E9 RID: 17641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F55")]
		[Address(RVA = "0x10154C45C", Offset = "0x154C45C", VA = "0x10154C45C")]
		public void Open(int roomIdx, long[] charaMngIDs, RoomBuilder builder)
		{
		}

		// Token: 0x060044EA RID: 17642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F56")]
		[Address(RVA = "0x1015504F0", Offset = "0x15504F0", VA = "0x1015504F0")]
		public void OpenFirst()
		{
		}

		// Token: 0x060044EB RID: 17643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F57")]
		[Address(RVA = "0x101550630", Offset = "0x1550630", VA = "0x101550630", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060044EC RID: 17644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F58")]
		[Address(RVA = "0x101550664", Offset = "0x1550664", VA = "0x101550664", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x060044ED RID: 17645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F59")]
		[Address(RVA = "0x101550804", Offset = "0x1550804", VA = "0x101550804")]
		public void OnBackButtonCallBack(bool isCallFromShortCut)
		{
		}

		// Token: 0x060044EE RID: 17646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5A")]
		[Address(RVA = "0x101550A10", Offset = "0x1550A10", VA = "0x101550A10")]
		private void OnConfirmRevertCallBack(int btn)
		{
		}

		// Token: 0x060044EF RID: 17647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5B")]
		[Address(RVA = "0x1015508F0", Offset = "0x15508F0", VA = "0x1015508F0")]
		private void ExecuteBack()
		{
		}

		// Token: 0x060044F0 RID: 17648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5C")]
		[Address(RVA = "0x101550A1C", Offset = "0x1550A1C", VA = "0x101550A1C", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x060044F1 RID: 17649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5D")]
		[Address(RVA = "0x101550AA8", Offset = "0x1550AA8", VA = "0x101550AA8")]
		public void Destroy()
		{
		}

		// Token: 0x060044F2 RID: 17650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5E")]
		[Address(RVA = "0x101550B3C", Offset = "0x1550B3C", VA = "0x101550B3C")]
		public void OpenCharaList(int memberIdx)
		{
		}

		// Token: 0x060044F3 RID: 17651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F5F")]
		[Address(RVA = "0x101550C0C", Offset = "0x1550C0C", VA = "0x101550C0C")]
		public void OnSelectCallBack(long mngID)
		{
		}

		// Token: 0x060044F4 RID: 17652 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F60")]
		[Address(RVA = "0x101550C3C", Offset = "0x1550C3C", VA = "0x101550C3C")]
		private void OnDecideSwap()
		{
		}

		// Token: 0x060044F5 RID: 17653 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F61")]
		[Address(RVA = "0x101550C6C", Offset = "0x1550C6C", VA = "0x101550C6C")]
		public void OnEndCharaListCallBack()
		{
		}

		// Token: 0x060044F6 RID: 17654 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F62")]
		[Address(RVA = "0x101550E08", Offset = "0x1550E08", VA = "0x101550E08")]
		public void OnClickRoomToggleButtonCallBack()
		{
		}

		// Token: 0x060044F7 RID: 17655 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F63")]
		[Address(RVA = "0x10154ECCC", Offset = "0x154ECCC", VA = "0x10154ECCC")]
		private void ApplyRoomChangeButton()
		{
		}

		// Token: 0x060044F8 RID: 17656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F64")]
		[Address(RVA = "0x101550E40", Offset = "0x1550E40", VA = "0x101550E40")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x060044F9 RID: 17657 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F65")]
		[Address(RVA = "0x101550F10", Offset = "0x1550F10", VA = "0x101550F10")]
		public void OnConfirmChangeCharaCallBack(int btn)
		{
		}

		// Token: 0x060044FA RID: 17658 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F66")]
		[Address(RVA = "0x101550F24", Offset = "0x1550F24", VA = "0x101550F24")]
		public ScheduleWindow()
		{
		}

		// Token: 0x040055C5 RID: 21957
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003C62")]
		private ScheduleWindow.eStep m_Step;

		// Token: 0x040055C6 RID: 21958
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003C63")]
		[SerializeField]
		private CustomButton m_SubRoomButton;

		// Token: 0x040055C7 RID: 21959
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003C64")]
		[SerializeField]
		private PrefabCloner[] m_CharaCloners;

		// Token: 0x040055C8 RID: 21960
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003C65")]
		private ScheduleChara[] m_ScheduleCharas;

		// Token: 0x040055C9 RID: 21961
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003C66")]
		[SerializeField]
		private float m_LengthHour;

		// Token: 0x040055CA RID: 21962
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003C67")]
		[SerializeField]
		private ScrollRect[] m_Scrolls;

		// Token: 0x040055CB RID: 21963
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003C68")]
		[SerializeField]
		private GameObject[] m_Emptys;

		// Token: 0x040055CC RID: 21964
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003C69")]
		[SerializeField]
		private ScrollRect m_HndleScroll;

		// Token: 0x040055CD RID: 21965
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003C6A")]
		private RectTransform m_ViewPort;

		// Token: 0x040055CE RID: 21966
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003C6B")]
		private RectTransform m_Content;

		// Token: 0x040055CF RID: 21967
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003C6C")]
		[SerializeField]
		private ScrollRect m_TimeScroll;

		// Token: 0x040055D0 RID: 21968
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003C6D")]
		[SerializeField]
		private Text m_TimeTextPrefab;

		// Token: 0x040055D1 RID: 21969
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4003C6E")]
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x040055D2 RID: 21970
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4003C6F")]
		[SerializeField]
		private ScheduleCharaSwapGroup m_SwapGroup;

		// Token: 0x040055D3 RID: 21971
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4003C70")]
		[SerializeField]
		private ScheduleItem m_ScheduleItemPrefab;

		// Token: 0x040055D4 RID: 21972
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4003C71")]
		[SerializeField]
		private Sprite[] m_BaseSprites;

		// Token: 0x040055D5 RID: 21973
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4003C72")]
		[SerializeField]
		private AnimUIPlayer m_DecideButtonAnim;

		// Token: 0x040055D6 RID: 21974
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4003C73")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040055D7 RID: 21975
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4003C74")]
		[SerializeField]
		private TextField m_RemainTimeText;

		// Token: 0x040055D8 RID: 21976
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4003C75")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x040055D9 RID: 21977
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4003C76")]
		private GlobalUI.SceneInfoStack m_InfoStack;

		// Token: 0x040055DA RID: 21978
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003C77")]
		private List<ScheduleItem> m_EmptyInstList;

		// Token: 0x040055DB RID: 21979
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003C78")]
		private List<ScheduleItem> m_ActiveInstList;

		// Token: 0x040055DC RID: 21980
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4003C79")]
		private long[] m_CharaMngIDs;

		// Token: 0x040055DD RID: 21981
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4003C7A")]
		private bool[] m_EditModes;

		// Token: 0x040055DE RID: 21982
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4003C7B")]
		private int m_MemberIdx;

		// Token: 0x040055DF RID: 21983
		[Cpp2IlInjected.FieldOffset(Offset = "0x154")]
		[Token(Token = "0x4003C7C")]
		public int m_RoomIdx;

		// Token: 0x040055E0 RID: 21984
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4003C7D")]
		private bool m_IsEnableEdit;

		// Token: 0x040055E1 RID: 21985
		[Cpp2IlInjected.FieldOffset(Offset = "0x159")]
		[Token(Token = "0x4003C7E")]
		private bool m_IsDirty;

		// Token: 0x040055E2 RID: 21986
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4003C7F")]
		private RoomBuilder m_builder;

		// Token: 0x02000E70 RID: 3696
		[Token(Token = "0x200115F")]
		private enum eStep
		{
			// Token: 0x040055E5 RID: 21989
			[Token(Token = "0x4006CAD")]
			WaitSchedule,
			// Token: 0x040055E6 RID: 21990
			[Token(Token = "0x4006CAE")]
			Schedule,
			// Token: 0x040055E7 RID: 21991
			[Token(Token = "0x4006CAF")]
			CharaList
		}
	}
}
