﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI {
    public class ScrollBarInherited : Scrollbar {
        private ScrollRect m_ScrollRect;
        private float m_DefaultDecelerationRate;
        private bool m_StopInertia;

        public void ForcePressState() {
            DoStateTransition(SelectionState.Pressed, false);
        }

        public override void OnPointerDown(PointerEventData eventData) {
            base.OnPointerDown(eventData);
            StopInertia();
        }

        public override void OnPointerExit(PointerEventData eventData) {
            SelectionState state = currentSelectionState;
            base.OnPointerExit(eventData);
            DoStateTransition(state, true);
        }

        public void LateUpdate() {
            if (m_StopInertia && m_ScrollRect != null) {
                if (m_ScrollRect.decelerationRate <= 0f) {
                    m_ScrollRect.decelerationRate = m_DefaultDecelerationRate;
                }
                m_StopInertia = false;
            }
        }

        public void StopInertia() {
            if (m_ScrollRect == null) {
                m_ScrollRect = GetComponentInParent<ScrollRect>();
            }
            if (m_ScrollRect != null && !m_StopInertia) {
                m_StopInertia = true;
                m_DefaultDecelerationRate = m_ScrollRect.decelerationRate;
                m_ScrollRect.decelerationRate = 0f;
            }
        }

        public static ScrollBarInherited SwapScrollComponent(Scrollbar scrollbar) {
            Graphic graphic = scrollbar.targetGraphic;
            RectTransform handle = scrollbar.handleRect;
            Direction direction = scrollbar.direction;
            GameObject barObject = scrollbar.gameObject;
            DestroyImmediate(scrollbar);
            ScrollBarInherited swap = barObject.AddComponent<ScrollBarInherited>();
            swap.targetGraphic = graphic;
            swap.handleRect = handle;
            swap.direction = direction;
            return swap;
        }
    }
}
