﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C98 RID: 3224
	[Token(Token = "0x20008A0")]
	[StructLayout(3)]
	public class ScrollField : UIBehaviour, ILayoutGroup, ILayoutController
	{
		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x06003AA4 RID: 15012 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700042E")]
		private RectTransform rectTransform
		{
			[Token(Token = "0x6003567")]
			[Address(RVA = "0x101551414", Offset = "0x1551414", VA = "0x101551414")]
			get
			{
				return null;
			}
		}

		// Token: 0x06003AA5 RID: 15013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003568")]
		[Address(RVA = "0x1015514AC", Offset = "0x15514AC", VA = "0x1015514AC", Slot = "5")]
		protected override void OnEnable()
		{
		}

		// Token: 0x06003AA6 RID: 15014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003569")]
		[Address(RVA = "0x101551568", Offset = "0x1551568", VA = "0x101551568", Slot = "7")]
		protected override void OnDisable()
		{
		}

		// Token: 0x06003AA7 RID: 15015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356A")]
		[Address(RVA = "0x1015515F4", Offset = "0x15515F4", VA = "0x1015515F4", Slot = "10")]
		protected override void OnRectTransformDimensionsChange()
		{
		}

		// Token: 0x06003AA8 RID: 15016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356B")]
		[Address(RVA = "0x1015515F8", Offset = "0x15515F8", VA = "0x1015515F8", Slot = "19")]
		protected virtual void OnTransformChildrenChanged()
		{
		}

		// Token: 0x06003AA9 RID: 15017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356C")]
		[Address(RVA = "0x1015515FC", Offset = "0x15515FC", VA = "0x1015515FC", Slot = "20")]
		public virtual void SetLayoutHorizontal()
		{
		}

		// Token: 0x06003AAA RID: 15018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356D")]
		[Address(RVA = "0x101551600", Offset = "0x1551600", VA = "0x101551600", Slot = "21")]
		public virtual void SetLayoutVertical()
		{
		}

		// Token: 0x06003AAB RID: 15019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356E")]
		[Address(RVA = "0x1015514D4", Offset = "0x15514D4", VA = "0x1015514D4")]
		protected void SetDirty()
		{
		}

		// Token: 0x06003AAC RID: 15020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600356F")]
		[Address(RVA = "0x101552B64", Offset = "0x1552B64", VA = "0x101552B64")]
		private void LateUpdate()
		{
		}

		// Token: 0x06003AAD RID: 15021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003570")]
		[Address(RVA = "0x101551A20", Offset = "0x1551A20", VA = "0x101551A20")]
		public void RebuildField()
		{
		}

		// Token: 0x06003AAE RID: 15022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003571")]
		[Address(RVA = "0x101552B8C", Offset = "0x1552B8C", VA = "0x101552B8C")]
		public void SetFieldPosition(Vector2 pos)
		{
		}

		// Token: 0x06003AAF RID: 15023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003572")]
		[Address(RVA = "0x101552C44", Offset = "0x1552C44", VA = "0x101552C44")]
		public void SetFieldSize(Vector2 sizeDelta)
		{
		}

		// Token: 0x06003AB0 RID: 15024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003573")]
		[Address(RVA = "0x101551638", Offset = "0x1551638", VA = "0x101551638")]
		public void FitTarget(RectTransform target)
		{
		}

		// Token: 0x06003AB1 RID: 15025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003574")]
		[Address(RVA = "0x101552C8C", Offset = "0x1552C8C", VA = "0x101552C8C")]
		public ScrollField()
		{
		}

		// Token: 0x0400496D RID: 18797
		[Token(Token = "0x4003396")]
		private const float MARGIN_X = 5f;

		// Token: 0x0400496E RID: 18798
		[Token(Token = "0x4003397")]
		private const float MARGIN_Y = 2f;

		// Token: 0x0400496F RID: 18799
		[Token(Token = "0x4003398")]
		private const float OFFSET_Y = 0f;

		// Token: 0x04004970 RID: 18800
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003399")]
		[SerializeField]
		private RectTransform m_Top;

		// Token: 0x04004971 RID: 18801
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400339A")]
		[SerializeField]
		private RectTransform m_Body;

		// Token: 0x04004972 RID: 18802
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400339B")]
		[SerializeField]
		private RectTransform m_Bottom;

		// Token: 0x04004973 RID: 18803
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400339C")]
		[SerializeField]
		private RectTransform m_Left;

		// Token: 0x04004974 RID: 18804
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400339D")]
		[SerializeField]
		private RectTransform m_Right;

		// Token: 0x04004975 RID: 18805
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400339E")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122E04", Offset = "0x122E04")]
		private RectTransform m_FitTarget;

		// Token: 0x04004976 RID: 18806
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400339F")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122E50", Offset = "0x122E50")]
		private RectTransform m_TopFrame;

		// Token: 0x04004977 RID: 18807
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40033A0")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122E9C", Offset = "0x122E9C")]
		private RectTransform m_BottomFrame;

		// Token: 0x04004978 RID: 18808
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40033A1")]
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x04004979 RID: 18809
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40033A2")]
		private DrivenRectTransformTracker m_Tracker;
	}
}
