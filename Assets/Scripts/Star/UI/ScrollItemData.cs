﻿namespace Star.UI {
    public abstract class ScrollItemData {
        private ScrollItemIcon m_Icon;

        public void SetIcon(ScrollItemIcon icon) {
            m_Icon = icon;
        }

        public void RefreshItem() {
            if (m_Icon != null) {
                m_Icon.Refresh();
            }
        }

        public virtual void OnClickItemCallBack() { }
    }
}
