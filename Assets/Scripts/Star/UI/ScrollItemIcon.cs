﻿using System;
using UnityEngine;

namespace Star.UI {
    public class ScrollItemIcon : MonoBehaviour {
        protected ScrollItemData m_Data;
        protected GameObject m_GameObject;
        protected RectTransform m_RectTransform;
        protected bool m_IsDark;
        protected bool m_IsEnableClick = true;
        protected bool m_IsEnableHold = true;
        protected ScrollViewBase m_Scroll;

        private CustomButton m_Button;

        public Action<ScrollItemIcon> OnClickIcon;
        public event Action<ScrollItemIcon> OnHoldIcon;

        public GameObject GameObject {
            get {
                if (m_GameObject == null) {
                    m_GameObject = gameObject;
                }
                return m_GameObject;
            }
        }

        public RectTransform RectTransform {
            get {
                if (m_RectTransform == null) {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        public ScrollViewBase Scroll {
            set => m_Scroll = value;
        }

        protected virtual void Awake() { }

        public void SetItemData(ScrollItemData data) {
            m_Data = data;
            data.SetIcon(this);
            ApplyData();
        }

        public ScrollItemData GetItemData() {
            return m_Data;
        }

        protected virtual void ApplyData() {
            if (m_Button == null) {
                m_Button = GetComponent<CustomButton>();
            }
            if (m_Button != null) {
                m_Button.Interactable = !m_IsDark;
            }
        }

        public void OnClickCallBack() {
            if (m_IsEnableClick) {
                if (m_Data != null) {
                    m_Data.OnClickItemCallBack();
                }
                OnClickIcon.Call(this);
            }
        }

        public void OnHoldCallBack() {
            if (m_IsEnableHold) {
                OnHoldIcon.Call(this);
            }
        }

        public virtual void Destroy() { }

        public virtual void Refresh() {
            ApplyData();
        }
    }
}
