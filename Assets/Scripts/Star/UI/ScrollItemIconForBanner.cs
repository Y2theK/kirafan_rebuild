﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CA2 RID: 3234
	[Token(Token = "0x20008AA")]
	[StructLayout(3)]
	public class ScrollItemIconForBanner : ScrollItemIcon
	{
		// Token: 0x06003B1E RID: 15134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E1")]
		[Address(RVA = "0x1015532C8", Offset = "0x15532C8", VA = "0x1015532C8", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06003B1F RID: 15135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E2")]
		[Address(RVA = "0x101553480", Offset = "0x1553480", VA = "0x101553480")]
		private void SetBannerSprite(Sprite sprite)
		{
		}

		// Token: 0x06003B20 RID: 15136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E3")]
		[Address(RVA = "0x101553530", Offset = "0x1553530", VA = "0x101553530")]
		private void SetTimeRemaining(DateTime startTime, DateTime endTime)
		{
		}

		// Token: 0x06003B21 RID: 15137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E4")]
		[Address(RVA = "0x1015539C4", Offset = "0x15539C4", VA = "0x1015539C4")]
		public void EnableNewIcon(bool flg)
		{
		}

		// Token: 0x06003B22 RID: 15138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E5")]
		[Address(RVA = "0x101553A74", Offset = "0x1553A74", VA = "0x101553A74")]
		public void OnClickButton()
		{
		}

		// Token: 0x06003B23 RID: 15139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60035E6")]
		[Address(RVA = "0x101553AC0", Offset = "0x1553AC0", VA = "0x101553AC0")]
		public ScrollItemIconForBanner()
		{
		}

		// Token: 0x040049C5 RID: 18885
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40033EE")]
		public Image m_BannerImage;

		// Token: 0x040049C6 RID: 18886
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40033EF")]
		public Text m_BannerText;

		// Token: 0x040049C7 RID: 18887
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40033F0")]
		public Image m_NewIcon;

		// Token: 0x040049C8 RID: 18888
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40033F1")]
		public CustomButton m_CustomButton;

		// Token: 0x040049C9 RID: 18889
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40033F2")]
		public InfoUIScrollItemData m_ItemData;

		// Token: 0x040049CA RID: 18890
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40033F3")]
		private bool m_IsDoneSetup;
	}
}
