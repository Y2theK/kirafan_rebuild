﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D3D RID: 3389
	[Token(Token = "0x2000916")]
	[StructLayout(3)]
	public class ScrollRectEx : ScrollRect
	{
		// Token: 0x170004B4 RID: 1204
		// (get) Token: 0x06003E2B RID: 15915 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x1700045F")]
		public ScrollRect ScrollRect
		{
			[Token(Token = "0x60038E3")]
			[Address(RVA = "0x101553AD0", Offset = "0x1553AD0", VA = "0x101553AD0")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004B5 RID: 1205
		// (get) Token: 0x06003E2C RID: 15916 RVA: 0x00018990 File Offset: 0x00016B90
		[Token(Token = "0x17000460")]
		public bool IsDrag
		{
			[Token(Token = "0x60038E4")]
			[Address(RVA = "0x101553AD4", Offset = "0x1553AD4", VA = "0x101553AD4")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x06003E2D RID: 15917 RVA: 0x000189A8 File Offset: 0x00016BA8
		[Token(Token = "0x17000461")]
		public float DragTime
		{
			[Token(Token = "0x60038E5")]
			[Address(RVA = "0x101553ADC", Offset = "0x1553ADC", VA = "0x101553ADC")]
			get
			{
				return 0f;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (set) Token: 0x06003E2E RID: 15918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000462")]
		public Func<GameObject, bool> OnBeginDragCallback
		{
			[Token(Token = "0x60038E6")]
			[Address(RVA = "0x101553AE4", Offset = "0x1553AE4", VA = "0x101553AE4")]
			set
			{
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (set) Token: 0x06003E2F RID: 15919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000463")]
		public Action<GameObject> OnEndDragCallback
		{
			[Token(Token = "0x60038E7")]
			[Address(RVA = "0x101553AEC", Offset = "0x1553AEC", VA = "0x101553AEC")]
			set
			{
			}
		}

		// Token: 0x06003E30 RID: 15920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038E8")]
		[Address(RVA = "0x101553AF4", Offset = "0x1553AF4", VA = "0x101553AF4")]
		private void Update()
		{
		}

		// Token: 0x06003E31 RID: 15921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038E9")]
		[Address(RVA = "0x101553B3C", Offset = "0x1553B3C", VA = "0x101553B3C", Slot = "44")]
		public override void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003E32 RID: 15922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038EA")]
		[Address(RVA = "0x101553C14", Offset = "0x1553C14", VA = "0x101553C14", Slot = "46")]
		public override void OnDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003E33 RID: 15923 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038EB")]
		[Address(RVA = "0x101553C28", Offset = "0x1553C28", VA = "0x101553C28", Slot = "45")]
		public override void OnEndDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06003E34 RID: 15924 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038EC")]
		[Address(RVA = "0x101553CB0", Offset = "0x1553CB0", VA = "0x101553CB0", Slot = "61")]
		public virtual void ReleaseDrag()
		{
		}

		// Token: 0x06003E35 RID: 15925 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038ED")]
		[Address(RVA = "0x101553CBC", Offset = "0x1553CBC", VA = "0x101553CBC")]
		public ScrollRectEx()
		{
		}

		// Token: 0x04004D5F RID: 19807
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x400366F")]
		private bool m_IsDrag;

		// Token: 0x04004D60 RID: 19808
		[Cpp2IlInjected.FieldOffset(Offset = "0x12C")]
		[Token(Token = "0x4003670")]
		private float m_DragTime;

		// Token: 0x04004D61 RID: 19809
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4003671")]
		private Func<GameObject, bool> m_OnBeginDragCallback;

		// Token: 0x04004D62 RID: 19810
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4003672")]
		private Action<GameObject> m_OnEndDragCallback;
	}
}
