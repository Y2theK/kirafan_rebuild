﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D3E RID: 3390
	[Token(Token = "0x2000917")]
	[StructLayout(3)]
	public class ScrollText : MonoBehaviour
	{
		// Token: 0x06003E36 RID: 15926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038EE")]
		[Address(RVA = "0x101553CC4", Offset = "0x1553CC4", VA = "0x101553CC4")]
		private void Awake()
		{
		}

		// Token: 0x06003E37 RID: 15927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038EF")]
		[Address(RVA = "0x101553D1C", Offset = "0x1553D1C", VA = "0x101553D1C")]
		private void Start()
		{
		}

		// Token: 0x06003E38 RID: 15928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F0")]
		[Address(RVA = "0x101553EDC", Offset = "0x1553EDC", VA = "0x101553EDC")]
		private void Update()
		{
		}

		// Token: 0x06003E39 RID: 15929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F1")]
		[Address(RVA = "0x101553DA8", Offset = "0x1553DA8", VA = "0x101553DA8")]
		public void ResetScroll()
		{
		}

		// Token: 0x06003E3A RID: 15930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F2")]
		[Address(RVA = "0x101554274", Offset = "0x1554274", VA = "0x101554274")]
		public ScrollText()
		{
		}

		// Token: 0x04004D63 RID: 19811
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003673")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04004D64 RID: 19812
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003674")]
		private float m_SpeedPerSec;

		// Token: 0x04004D65 RID: 19813
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4003675")]
		private float m_StartSec;

		// Token: 0x04004D66 RID: 19814
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003676")]
		private float m_ReStartWaitSec;

		// Token: 0x04004D67 RID: 19815
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003677")]
		private float m_InOutAlphaSpeedPerSec;

		// Token: 0x04004D68 RID: 19816
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003678")]
		private ScrollText.eState m_State;

		// Token: 0x04004D69 RID: 19817
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003679")]
		private float m_StateTime;

		// Token: 0x04004D6A RID: 19818
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400367A")]
		private Vector3 m_StartPosition;

		// Token: 0x04004D6B RID: 19819
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400367B")]
		private RectTransform m_RectTransform;

		// Token: 0x04004D6C RID: 19820
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400367C")]
		private RectTransform m_TextRectTransform;

		// Token: 0x02000D3F RID: 3391
		[Token(Token = "0x20010EE")]
		private enum eState
		{
			// Token: 0x04004D6E RID: 19822
			[Token(Token = "0x4006A40")]
			In,
			// Token: 0x04004D6F RID: 19823
			[Token(Token = "0x4006A41")]
			Start,
			// Token: 0x04004D70 RID: 19824
			[Token(Token = "0x4006A42")]
			Scroll,
			// Token: 0x04004D71 RID: 19825
			[Token(Token = "0x4006A43")]
			End,
			// Token: 0x04004D72 RID: 19826
			[Token(Token = "0x4006A44")]
			Out
		}
	}
}
