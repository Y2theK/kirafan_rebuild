﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D40 RID: 3392
	[Token(Token = "0x2000918")]
	[StructLayout(3)]
	public class ScrollTextUIGroup : UIGroup
	{
		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06003E3B RID: 15931 RVA: 0x000189C0 File Offset: 0x00016BC0
		[Token(Token = "0x17000464")]
		public int SelectButtonIdx
		{
			[Token(Token = "0x60038F3")]
			[Address(RVA = "0x101554290", Offset = "0x1554290", VA = "0x101554290")]
			get
			{
				return 0;
			}
		}

		// Token: 0x06003E3C RID: 15932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F4")]
		[Address(RVA = "0x101554298", Offset = "0x1554298", VA = "0x101554298", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06003E3D RID: 15933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F5")]
		[Address(RVA = "0x1015542D8", Offset = "0x15542D8", VA = "0x1015542D8")]
		public void SetText(string title, string header, string body)
		{
		}

		// Token: 0x06003E3E RID: 15934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F6")]
		[Address(RVA = "0x101554384", Offset = "0x1554384", VA = "0x101554384")]
		public void ClearButtons()
		{
		}

		// Token: 0x06003E3F RID: 15935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F7")]
		[Address(RVA = "0x101554438", Offset = "0x1554438", VA = "0x101554438")]
		public void SetButtonParam(int idx, string text)
		{
		}

		// Token: 0x06003E40 RID: 15936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F8")]
		[Address(RVA = "0x1015545DC", Offset = "0x15545DC", VA = "0x1015545DC")]
		public void OnClickButtonCallBack(int idx)
		{
		}

		// Token: 0x06003E41 RID: 15937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038F9")]
		[Address(RVA = "0x1015545EC", Offset = "0x15545EC", VA = "0x1015545EC")]
		public ScrollTextUIGroup()
		{
		}

		// Token: 0x04004D73 RID: 19827
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400367D")]
		[SerializeField]
		private FixScrollRect m_Scroll;

		// Token: 0x04004D74 RID: 19828
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400367E")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04004D75 RID: 19829
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400367F")]
		[SerializeField]
		private Text m_HeaderText;

		// Token: 0x04004D76 RID: 19830
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003680")]
		[SerializeField]
		private Text m_BodyText;

		// Token: 0x04004D77 RID: 19831
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003681")]
		[SerializeField]
		private CustomButton[] m_Buttons;

		// Token: 0x04004D78 RID: 19832
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003682")]
		private int m_SelectButtonIdx;

		// Token: 0x04004D79 RID: 19833
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003683")]
		private Action<int> m_OnEndCallBack;
	}
}
