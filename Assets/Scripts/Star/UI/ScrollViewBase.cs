﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI {
    [RequireComponent(typeof(ScrollRect))]
    public class ScrollViewBase : MonoBehaviour, IBeginDragHandler, IEventSystemHandler, IDragHandler, IEndDragHandler {
        private const float VERTICAL_SCROLLBAR_MINHEIGHT = 32f;

        protected List<ScrollItemData> m_ItemDataList;
        protected List<DispItem> m_DispItemList;
        protected List<DispItem> m_ActiveDispItemList;
        protected List<DispItem> m_EmptyDispItemList;
        protected int m_ViewItemStartIdx;
        protected bool m_IsDirty;
        protected float m_ScrollMaxVertical;
        protected int m_DispItemMaxY;
        protected int m_DispItemMax;
        protected float m_MarginX;
        protected ScrollRect m_ScrollRect;
        protected RectTransform m_ScrollRectTransform;
        protected RectTransform m_Content;
        protected float m_ItemWidth;
        protected float m_ItemHeight;
        protected int m_CurrentScrollX = -1;
        protected int m_CurrentScrollY = -1;
        private int m_ReserveSetIdx = -1;

        [Tooltip("対応するスクロールバー")]
        [SerializeField] protected Scrollbar m_VerticalBar;

        [Tooltip("一行に並ぶアイテム数")]
        [SerializeField] protected int m_DispItemMaxX = 1;

        [Tooltip("余白")]
        [SerializeField] protected float m_MarginY;

        [SerializeField] protected float m_SpaceX;

        [Tooltip("間隔")]
        [SerializeField] protected float m_SpaceY;

        [SerializeField] protected ScrollItemIcon m_ItemIconPrefab;

        [Tooltip("項目がない場合にアクティブになる")]
        [SerializeField] protected GameObject m_EmptyObj;

        public List<ScrollItemData> GetDataList() {
            return m_ItemDataList;
        }

        public virtual void Setup() {
            Rect itemRect = m_ItemIconPrefab.GetComponent<RectTransform>().rect;
            m_ItemWidth = itemRect.width;
            m_ItemHeight = itemRect.height;
            m_ItemDataList = new List<ScrollItemData>();
            m_DispItemList = new List<DispItem>();
            m_ActiveDispItemList = new List<DispItem>();
            m_EmptyDispItemList = new List<DispItem>();
            m_ScrollRect = GetComponent<ScrollRect>();
            m_ScrollRectTransform = m_ScrollRect.GetComponent<RectTransform>();
            m_Content = m_ScrollRect.content;
            m_ScrollRect.onValueChanged.AddListener(OnScrollRectValueChanged);
            m_ItemDataList.Clear();
            if (m_VerticalBar != null) {
                m_VerticalBar = ScrollBarInherited.SwapScrollComponent(m_VerticalBar);
                m_VerticalBar.gameObject.SetActive(false);
                m_VerticalBar.value = 1f;
                m_VerticalBar.onValueChanged.AddListener(OnVerticalScrollBarValueChanged);
            }
            SetScrollNormalizedPosition(1f);
            m_CurrentScrollX = -1;
            m_CurrentScrollY = -1;
            CalcViewItemMax();
            m_IsDirty = true;
        }

        protected virtual void RebuildScroll() {
            InstantiateItem();
            int itemNum = m_ItemDataList.Count + m_ViewItemStartIdx;
            int dispItemNum = m_DispItemMaxX > 0 ? itemNum / m_DispItemMaxX : 0;
            if (itemNum % m_DispItemMaxX > 0) {
                dispItemNum++;
            }
            RectTransform viewport = m_ScrollRect.viewport;
            m_Content.sizeDelta = new Vector2(m_Content.sizeDelta.x, dispItemNum * (m_ItemHeight + m_SpaceY) + m_MarginY * 2f - m_SpaceY);
            m_ScrollMaxVertical = m_Content.rect.height - viewport.rect.height;
            if (m_VerticalBar != null) {
                if (m_Content.rect.height < viewport.rect.height) {
                    m_VerticalBar.gameObject.SetActive(false);
                    viewport.anchorMin = Vector2.zero;
                    viewport.anchorMax = Vector2.one;
                    viewport.sizeDelta = Vector2.zero;
                    viewport.anchoredPosition = Vector2.zero;
                } else {
                    m_VerticalBar.gameObject.SetActive(true);
                    viewport.anchorMin = Vector2.zero;
                    viewport.anchorMax = Vector2.one;
                    viewport.sizeDelta = new Vector2(-m_VerticalBar.GetComponent<RectTransform>().sizeDelta.x, 0f);
                    viewport.anchoredPosition = Vector2.zero;
                }
            }
            if (m_EmptyObj != null) {
                if (itemNum <= 0 && !m_EmptyObj.activeSelf) {
                    m_EmptyObj.SetActive(true);
                } else if (itemNum > 0 && m_EmptyObj.activeSelf) {
                    m_EmptyObj.SetActive(false);
                }
            }
            m_IsDirty = false;
        }

        public virtual void LateUpdate() {
            if (m_IsDirty) {
                RebuildScroll();
                UpdateItemPosition();
            }
            if (m_ReserveSetIdx != -1) {
                SetScrollValueFromIdx(m_ReserveSetIdx);
            }
            if (m_Content != null && m_VerticalBar != null && m_Content.rect.height > 0f) {
                RectTransform content = m_ScrollRect.content;
                RectTransform viewport = m_ScrollRect.viewport;
                float viewHeight = viewport.rect.height;
                if (m_Content.anchoredPosition.y < 0f) {
                    viewHeight += content.anchoredPosition.y;
                } else if (content.anchoredPosition.y > content.rect.height - viewport.rect.height) {
                    viewHeight -= content.anchoredPosition.y - (content.rect.height - viewport.rect.height);
                }
                m_VerticalBar.size = viewHeight / m_Content.rect.height;
                RectTransform barTransform = m_VerticalBar.GetComponent<RectTransform>();
                if (barTransform.rect.height * m_VerticalBar.size < VERTICAL_SCROLLBAR_MINHEIGHT) {
                    m_VerticalBar.size = VERTICAL_SCROLLBAR_MINHEIGHT / barTransform.rect.height;
                }
            }
            if (m_ScrollRect == null) {
                m_ScrollRect = GetComponent<ScrollRect>();
            }
        }

        public virtual void AddItem(ScrollItemData item) {
            m_ItemDataList.Add(item);
            m_IsDirty = true;
        }

        public ScrollItemIcon GetItemInst(int idx) {
            if (m_DispItemList.Count <= idx) {
                return null;
            }
            return m_DispItemList[idx].m_ItemIconObject;
        }

        public ScrollItemIcon[] GetItemInstAll() {
            if (m_DispItemList.Count > 0) {
                List<ScrollItemIcon> list = new List<ScrollItemIcon>();
                for (int i = 0; i < m_DispItemList.Count; i++) {
                    list.Add(m_DispItemList[i].m_ItemIconObject);
                }
                return list.ToArray();
            }
            return null;
        }

        public virtual void RemoveAll() {
            m_ScrollMaxVertical = 0f;
            if (m_DispItemList != null) {
                for (int i = 0; i < m_DispItemList.Count; i++) {
                    m_DispItemList[i].m_ItemIconObject.Destroy();
                    Destroy(m_DispItemList[i].m_ItemIconObject.gameObject);
                }
            }
            m_ItemDataList = new List<ScrollItemData>();
            m_DispItemList = new List<DispItem>();
            m_EmptyDispItemList = new List<DispItem>();
            m_ActiveDispItemList = new List<DispItem>();
            m_ItemDataList.Clear();
            SetScrollNormalizedPosition(1f);
            RebuildScroll();
            m_CurrentScrollX = -1;
            m_CurrentScrollY = -1;
        }

        public void RemoveAllData() {
            m_ScrollMaxVertical = 0f;
            m_ScrollRect.velocity = Vector2.zero;
            SetScrollNormalizedPosition(1f);
            m_ItemDataList.Clear();
            m_IsDirty = true;
            Refresh();
        }

        public void Refresh() {
            for (int i = 0; i < m_EmptyDispItemList.Count; i++) {
                m_EmptyDispItemList[i].m_ItemIconObject.GameObject.SetActive(false);
            }
            for (int j = 0; j < m_ActiveDispItemList.Count; j++) {
                m_ActiveDispItemList[j].m_Visible = false;
                m_EmptyDispItemList.Add(m_ActiveDispItemList[j]);
                m_ActiveDispItemList[j].m_ItemIconObject.gameObject.SetActive(false);
            }
            m_ActiveDispItemList.Clear();
            m_CurrentScrollX = -1;
            m_CurrentScrollY = -1;
            m_IsDirty = true;
        }

        public void ForceApply() {
            for (int i = 0; i < m_DispItemList.Count; i++) {
                if (m_DispItemList[i].m_Visible) {
                    m_DispItemList[i].m_ItemIconObject.Refresh();
                }
            }
        }

        protected void OnVerticalScrollBarValueChanged(float value) {
            if (m_ItemDataList.Count > 0 && m_ScrollMaxVertical > 0f) {
                if (m_VerticalBar != null) {
                    m_Content.localPosY((1f - value) * m_ScrollMaxVertical);
                }
                UpdateItemPosition();
            }
        }

        public void OnScrollRectValueChanged(Vector2 scrollPosition) {
            if (m_ItemDataList.Count > 0 && m_ScrollMaxVertical > 0f) {
                if (m_VerticalBar != null) {
                    m_VerticalBar.value = 1f - m_Content.localPosition.y / m_ScrollMaxVertical;
                }
                UpdateItemPosition();
            }
        }

        public virtual void SetScrollNormalizedPosition(float position) {
            if (m_IsDirty) {
                RebuildScroll();
            }
            if (m_ScrollMaxVertical <= 0f) {
                position = 1f;
            }
            m_ScrollRect.verticalNormalizedPosition = position;
            if (m_VerticalBar != null) {
                m_VerticalBar.value = position;
            }
            m_IsDirty = true;
        }

        public void SetScrollValue(float value) {
            if (m_IsDirty) {
                RebuildScroll();
            }
            if (m_ScrollMaxVertical > 0f) {
                value = Mathf.Clamp(value, 0f, m_ScrollMaxVertical);
            }
            m_Content.anchoredPosition = new Vector2(0f, value);
            if (m_VerticalBar != null) {
                m_VerticalBar.value = 1f - m_Content.localPosition.y / m_ScrollMaxVertical;
            }
            m_IsDirty = true;
        }

        public void SetScrollValueFromIdx(int idx) {
            if (m_ScrollMaxVertical > 0f) {
                if (m_IsDirty) {
                    RebuildScroll();
                }
                float value = idx * (m_ItemHeight + m_SpaceY) + m_MarginY;
                SetScrollValue(Mathf.Clamp(value, 0f, m_ScrollMaxVertical));
                idx = -1;
            }
            m_ReserveSetIdx = idx;
        }

        public float GetScrollValue() {
            return m_Content.localPosition.y;
        }

        protected void CalcViewItemMax() {
            float height = m_ScrollRect.viewport.rect.height;
            if (height > 0f) {
                m_DispItemMaxY = (int)(height / (m_ItemHeight + m_SpaceY)) + 2;
                m_DispItemMax = m_DispItemMaxX * m_DispItemMaxY;
                m_MarginX = (m_Content.rect.width - (m_ItemWidth + m_SpaceX) * m_DispItemMaxX) * 0.5f;
            }
        }

        protected virtual void InstantiateItem() {
            while (m_DispItemList.Count < m_DispItemMax && m_DispItemList.Count < m_ItemDataList.Count) {
                DispItem dispItem = new DispItem() {
                    m_Idx = m_ItemDataList.Count - 1,
                    m_Visible = false,
                    m_PositionX = (m_ItemDataList.Count - 1) % m_DispItemMaxX,
                    m_PositionY = (m_ItemDataList.Count - 1) / m_DispItemMaxY
                };
                ScrollItemIcon icon = Instantiate(m_ItemIconPrefab, m_Content);
                icon.Scroll = this;
                icon.RectTransform.pivot = new Vector2(0f, 1f);
                icon.RectTransform.anchorMin = new Vector2(0f, 1f);
                icon.RectTransform.anchorMax = new Vector2(0f, 1f);
                icon.RectTransform.localScale = Vector3.one;
                icon.OnClickIcon += OnClickIconCallBack;
                icon.OnHoldIcon += OnHoldIconCallBack;
                icon.GameObject.SetActive(false);
                dispItem.m_ItemIconObject = icon;
                m_DispItemList.Add(dispItem);
                m_EmptyDispItemList.Add(dispItem);
            }
        }

        protected virtual void OnClickIconCallBack(ScrollItemIcon icon) { }

        protected virtual void OnHoldIconCallBack(ScrollItemIcon icon) { }

        protected virtual void UpdateItemPosition() {
            Vector2 anchor = m_Content.anchoredPosition;
            float width = m_ItemWidth + m_SpaceX;
            float height = m_ItemHeight + m_SpaceY;
            m_MarginX = (m_Content.rect.width - width * m_DispItemMaxX + m_SpaceX) * 0.5f;
            int scrollX = (int)((anchor.x - m_MarginX) / width);
            int scrollY = (int)((anchor.y - m_MarginY * 0.5f) / height);
            if (scrollX < 0) {
                scrollX = 0;
            }
            if (scrollY < 0) {
                scrollY = 0;
            }
            if (m_CurrentScrollX == scrollX && m_CurrentScrollY == scrollY) {
                return;
            }
            m_CurrentScrollX = scrollX;
            m_CurrentScrollY = scrollY;
            int idx = 0;
            while (m_ActiveDispItemList.Count > 0) {
                if (!CheckOutOfView(m_ActiveDispItemList[idx])) {
                    break;
                }
                HideItem(m_ActiveDispItemList[idx]);
                m_EmptyDispItemList.Add(m_ActiveDispItemList[idx]);
                m_ActiveDispItemList.RemoveAt(idx);
            }
            while (m_ActiveDispItemList.Count > 0) {
                idx = m_ActiveDispItemList.Count - 1;
                if (!CheckOutOfView(m_ActiveDispItemList[idx])) {
                    break;
                }
                HideItem(m_ActiveDispItemList[idx]);
                m_EmptyDispItemList.Add(m_ActiveDispItemList[idx]);
                m_ActiveDispItemList.RemoveAt(idx);
            }
            for (int i = 0; i < m_DispItemMaxY; i++) {
                for (int j = 0; j < m_DispItemMaxX; j++) {
                    int xIdx = m_CurrentScrollX + j;
                    int yIdx = m_CurrentScrollY + i;
                    int dispIdx = m_DispItemMaxX * yIdx + xIdx;
                    if (dispIdx < m_ViewItemStartIdx
                        || xIdx < 0
                        || xIdx >= m_DispItemMaxX
                        || yIdx < 0
                        || dispIdx >= m_ItemDataList.Count + m_ViewItemStartIdx) {
                        continue;
                    }
                    int itemIdx = m_ActiveDispItemList.Count;
                    for (int k = 0; k < m_ActiveDispItemList.Count; k++) {
                        DispItem item = m_ActiveDispItemList[k];
                        if (item.m_Visible) {
                            if (item.m_PositionX == xIdx && item.m_PositionY == yIdx) {
                                itemIdx = -1;
                                break;
                            }
                            if (item.m_PositionY > yIdx || (item.m_PositionX >= xIdx && item.m_PositionY == yIdx)) {
                                itemIdx = k;
                                break;
                            }
                        }
                    }
                    if (itemIdx > -1) {
                        dispIdx -= m_ViewItemStartIdx;
                        if (m_EmptyDispItemList.Count > 0) {
                            DispItem item = m_EmptyDispItemList[0];
                            item.m_PositionX = xIdx;
                            item.m_PositionY = yIdx;
                            item.m_Idx = dispIdx;
                            ShowItem(item);
                            anchor = new Vector2(xIdx * width + m_MarginX, -yIdx * height - m_MarginY);
                            item.m_ItemIconObject.RectTransform.anchoredPosition = anchor;
                            item.m_ItemIconObject.RectTransform.localScale = Vector2.one;
                            item.m_ItemIconObject.SetItemData(m_ItemDataList[dispIdx]);
                            if (itemIdx < m_ActiveDispItemList.Count) {
                                m_ActiveDispItemList.Insert(itemIdx, item);
                            } else {
                                m_ActiveDispItemList.Add(item);
                            }
                            m_EmptyDispItemList.RemoveAt(0);
                        } else {
                            Debug.LogWarning($"Item Object Pool is empty " +
                                $"m_EmptyDispItemList.Count = {m_EmptyDispItemList.Count} " +
                                $"m_ActiveDispItemList.Count = {m_ActiveDispItemList.Count}");
                        }
                    }
                }
            }
        }

        protected bool CheckOutOfView(DispItem item) {
            return !item.m_Visible
                || item.m_PositionX < m_CurrentScrollX
                || item.m_PositionX >= m_CurrentScrollX + m_DispItemMaxX
                || item.m_PositionY < m_CurrentScrollY
                || item.m_PositionY >= m_CurrentScrollY + m_DispItemMaxY
                || item.m_PositionX < 0
                || item.m_PositionX >= m_DispItemMaxX
                || item.m_PositionY < 0
                || m_ItemDataList.Count + m_ViewItemStartIdx <= item.m_PositionY * m_DispItemMaxX + item.m_PositionX;
        }

        protected virtual void HideItem(DispItem item) {
            item.m_Visible = false;
            item.m_ItemIconObject.gameObject.SetActive(false);
        }

        protected virtual void ShowItem(DispItem item) {
            item.m_Visible = true;
            if (!item.m_ItemIconObject.GameObject.activeSelf) {
                item.m_ItemIconObject.GameObject.SetActive(true);
            }
            item.m_Visible = true;
        }

        public virtual void Destroy() {
            RemoveAll();
        }

        public void OnBeginDrag(PointerEventData eventData) { }

        public void OnDrag(PointerEventData eventData) {
            if (m_ScrollRect.vertical) {
                RectTransform transform = GetComponent<RectTransform>();
                RectTransformUtility.ScreenPointToLocalPointInRectangle(transform, eventData.position, GameSystem.Inst.UICamera, out Vector2 a);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(transform, eventData.pressPosition, GameSystem.Inst.UICamera, out Vector2 b);
                if ((a - b).sqrMagnitude > 1024f) {
                    CustomButton[] buttons = GetComponentsInChildren<CustomButton>();
                    for (int i = 0; i < buttons.Length; i++) {
                        buttons[i].ForceEnableHold(false);
                    }
                }
            }
        }

        public void OnEndDrag(PointerEventData eventData) {
            CustomButton[] buttons = GetComponentsInChildren<CustomButton>();
            for (int i = 0; i < buttons.Length; i++) {
                buttons[i].ForceRelease();
            }
        }

        protected class DispItem {
            public int m_Idx;
            public int m_PositionX;
            public int m_PositionY;
            public ScrollItemIcon m_ItemIconObject;
            public bool m_Visible;
        }
    }
}
