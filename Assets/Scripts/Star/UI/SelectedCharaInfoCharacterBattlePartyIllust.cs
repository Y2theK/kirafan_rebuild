﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C50 RID: 3152
	[Token(Token = "0x2000871")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterBattlePartyIllust : SelectedCharaInfoCharacterIllust
	{
		// Token: 0x0600390C RID: 14604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F6")]
		[Address(RVA = "0x101557550", Offset = "0x1557550", VA = "0x101557550", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x0600390D RID: 14605 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F7")]
		[Address(RVA = "0x1015575C8", Offset = "0x15575C8", VA = "0x1015575C8")]
		public SelectedCharaInfoCharacterBattlePartyIllust()
		{
		}
	}
}
