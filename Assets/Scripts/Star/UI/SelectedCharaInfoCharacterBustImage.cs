﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C51 RID: 3153
	[Token(Token = "0x2000872")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterBustImage : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x0600390E RID: 14606 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60033F8")]
		[Address(RVA = "0x1015575F8", Offset = "0x15575F8", VA = "0x1015575F8", Slot = "24")]
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return null;
		}

		// Token: 0x0600390F RID: 14607 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033F9")]
		[Address(RVA = "0x101557660", Offset = "0x1557660", VA = "0x101557660", Slot = "6")]
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, Action callback)
		{
		}

		// Token: 0x06003910 RID: 14608 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033FA")]
		[Address(RVA = "0x101557ACC", Offset = "0x1557ACC", VA = "0x101557ACC")]
		public SelectedCharaInfoCharacterBustImage()
		{
		}

		// Token: 0x04004844 RID: 18500
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40032C0")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001224B4", Offset = "0x1224B4")]
		private BustImage m_BustImage;

		// Token: 0x04004845 RID: 18501
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40032C1")]
		private Vector3? m_BasePosition;

		// Token: 0x04004846 RID: 18502
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40032C2")]
		private float m_BaseScale;
	}
}
