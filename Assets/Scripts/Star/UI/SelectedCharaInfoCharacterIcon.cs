﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C52 RID: 3154
	[Token(Token = "0x2000873")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterIcon : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x06003911 RID: 14609 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033FB")]
		[Address(RVA = "0x101557AEC", Offset = "0x1557AEC", VA = "0x101557AEC", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06003912 RID: 14610 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60033FC")]
		[Address(RVA = "0x101557BCC", Offset = "0x1557BCC", VA = "0x101557BCC", Slot = "24")]
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return null;
		}

		// Token: 0x06003913 RID: 14611 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033FD")]
		[Address(RVA = "0x101557C34", Offset = "0x1557C34", VA = "0x101557C34", Slot = "17")]
		public override void SetLimitBreak(int lb)
		{
		}

		// Token: 0x06003914 RID: 14612 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033FE")]
		[Address(RVA = "0x101557C6C", Offset = "0x1557C6C", VA = "0x101557C6C", Slot = "18")]
		public override void SetArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003915 RID: 14613 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033FF")]
		[Address(RVA = "0x101557CB4", Offset = "0x1557CB4", VA = "0x101557CB4", Slot = "19")]
		public override void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x06003916 RID: 14614 RVA: 0x00017DC0 File Offset: 0x00015FC0
		[Token(Token = "0x6003400")]
		[Address(RVA = "0x101557CFC", Offset = "0x1557CFC", VA = "0x101557CFC", Slot = "21")]
		public override bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x06003917 RID: 14615 RVA: 0x00017DD8 File Offset: 0x00015FD8
		[Token(Token = "0x6003401")]
		[Address(RVA = "0x101557D04", Offset = "0x1557D04", VA = "0x101557D04", Slot = "22")]
		public override bool IsAnimStateIn()
		{
			return default(bool);
		}

		// Token: 0x06003918 RID: 14616 RVA: 0x00017DF0 File Offset: 0x00015FF0
		[Token(Token = "0x6003402")]
		[Address(RVA = "0x101557D64", Offset = "0x1557D64", VA = "0x101557D64", Slot = "23")]
		public override bool IsAnimStateOut()
		{
			return default(bool);
		}

		// Token: 0x06003919 RID: 14617 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003403")]
		[Address(RVA = "0x101557DC4", Offset = "0x1557DC4", VA = "0x101557DC4")]
		public SelectedCharaInfoCharacterIcon()
		{
		}

		// Token: 0x04004847 RID: 18503
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40032C3")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100122500", Offset = "0x122500")]
		private PrefabCloner m_CharacterIconCloner;

		// Token: 0x04004848 RID: 18504
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40032C4")]
		private CharaIconWithFrame m_CharacterIcon;
	}
}
