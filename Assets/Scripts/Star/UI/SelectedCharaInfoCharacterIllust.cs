﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C53 RID: 3155
	[Token(Token = "0x2000874")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterIllust : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x0600391A RID: 14618 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003404")]
		[Address(RVA = "0x101557DD4", Offset = "0x1557DD4", VA = "0x101557DD4", Slot = "24")]
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return null;
		}

		// Token: 0x0600391B RID: 14619 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003405")]
		[Address(RVA = "0x101557E4C", Offset = "0x1557E4C", VA = "0x101557E4C", Slot = "19")]
		public override void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x0600391C RID: 14620 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003406")]
		[Address(RVA = "0x101557F54", Offset = "0x1557F54", VA = "0x101557F54", Slot = "20")]
		public override void Replace(int charaID)
		{
		}

		// Token: 0x0600391D RID: 14621 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003407")]
		[Address(RVA = "0x1015575E0", Offset = "0x15575E0", VA = "0x1015575E0")]
		public SelectedCharaInfoCharacterIllust()
		{
		}

		// Token: 0x04004849 RID: 18505
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40032C5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012254C", Offset = "0x12254C")]
		private CharaIllust m_CharacterIllust;

		// Token: 0x0400484A RID: 18506
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40032C6")]
		[SerializeField]
		protected ReflectFrameImage m_Reflection;

		// Token: 0x0400484B RID: 18507
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40032C7")]
		protected CharaIllust.eCharaIllustType m_CharacterIllustType;
	}
}
