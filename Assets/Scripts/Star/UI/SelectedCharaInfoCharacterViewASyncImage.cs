﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C5B RID: 3163
	[Token(Token = "0x200087B")]
	[StructLayout(3)]
	public abstract class SelectedCharaInfoCharacterViewASyncImage : SelectedCharaInfoCharacterViewBase
	{
		// Token: 0x06003957 RID: 14679 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003441")]
		[Address(RVA = "0x101557590", Offset = "0x1557590", VA = "0x101557590", Slot = "4")]
		public override void Setup()
		{
		}

		// Token: 0x06003958 RID: 14680
		[Token(Token = "0x6003442")]
		[Address(Slot = "24")]
		protected abstract ASyncImageAdapter CreateASyncImageAdapter();

		// Token: 0x06003959 RID: 14681 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003443")]
		[Address(RVA = "0x101558004", Offset = "0x1558004", VA = "0x101558004", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x0600395A RID: 14682 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003444")]
		[Address(RVA = "0x101557A40", Offset = "0x1557A40", VA = "0x101557A40", Slot = "6")]
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, Action callback)
		{
		}

		// Token: 0x0600395B RID: 14683 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003445")]
		[Address(RVA = "0x10155805C", Offset = "0x155805C", VA = "0x10155805C", Slot = "11")]
		public override void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
		}

		// Token: 0x0600395C RID: 14684 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003446")]
		[Address(RVA = "0x1015581BC", Offset = "0x15581BC", VA = "0x1015581BC", Slot = "14")]
		public override void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
		}

		// Token: 0x0600395D RID: 14685 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003447")]
		[Address(RVA = "0x1015582AC", Offset = "0x15582AC", VA = "0x1015582AC", Slot = "15")]
		public override void FinishWait()
		{
		}

		// Token: 0x0600395E RID: 14686 RVA: 0x00017EB0 File Offset: 0x000160B0
		[Token(Token = "0x6003448")]
		[Address(RVA = "0x10155831C", Offset = "0x155831C", VA = "0x10155831C", Slot = "21")]
		public override bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600395F RID: 14687 RVA: 0x00017EC8 File Offset: 0x000160C8
		[Token(Token = "0x6003449")]
		[Address(RVA = "0x101557D34", Offset = "0x1557D34", VA = "0x101557D34", Slot = "22")]
		public override bool IsAnimStateIn()
		{
			return default(bool);
		}

		// Token: 0x06003960 RID: 14688 RVA: 0x00017EE0 File Offset: 0x000160E0
		[Token(Token = "0x600344A")]
		[Address(RVA = "0x101557D94", Offset = "0x1557D94", VA = "0x101557D94", Slot = "23")]
		public override bool IsAnimStateOut()
		{
			return default(bool);
		}

		// Token: 0x06003961 RID: 14689 RVA: 0x00017EF8 File Offset: 0x000160F8
		[Token(Token = "0x600344B")]
		[Address(RVA = "0x101558344", Offset = "0x1558344", VA = "0x101558344")]
		public int GetAnimState()
		{
			return 0;
		}

		// Token: 0x06003962 RID: 14690 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600344C")]
		[Address(RVA = "0x101558028", Offset = "0x1558028", VA = "0x101558028")]
		protected ASyncImageAdapter GetASyncImageAdapter()
		{
			return null;
		}

		// Token: 0x06003963 RID: 14691 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600344D")]
		[Address(RVA = "0x101557ADC", Offset = "0x1557ADC", VA = "0x101557ADC")]
		protected SelectedCharaInfoCharacterViewASyncImage()
		{
		}

		// Token: 0x0400485F RID: 18527
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40032D2")]
		protected ASyncImageAdapter m_ASyncImage;

		// Token: 0x04004860 RID: 18528
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40032D3")]
		protected AnimUIPlayer m_AnimPlayer;
	}
}
