﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C54 RID: 3156
	[Token(Token = "0x2000875")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterViewBase : MonoBehaviour
	{
		// Token: 0x0600391E RID: 14622 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003408")]
		[Address(RVA = "0x101557F98", Offset = "0x1557F98", VA = "0x101557F98", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x0600391F RID: 14623 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003409")]
		[Address(RVA = "0x101558464", Offset = "0x1558464", VA = "0x101558464")]
		public void SetManualUpdate(bool manualUpdate)
		{
		}

		// Token: 0x06003920 RID: 14624 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340A")]
		[Address(RVA = "0x101558020", Offset = "0x1558020", VA = "0x101558020", Slot = "5")]
		public virtual void Destroy()
		{
		}

		// Token: 0x06003921 RID: 14625 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340B")]
		[Address(RVA = "0x101558030", Offset = "0x1558030", VA = "0x101558030", Slot = "6")]
		public virtual void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid, [Optional] Action callback)
		{
		}

		// Token: 0x06003922 RID: 14626 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340C")]
		[Address(RVA = "0x1015584C4", Offset = "0x15584C4", VA = "0x1015584C4", Slot = "7")]
		public virtual void ForceRequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
		}

		// Token: 0x06003923 RID: 14627 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340D")]
		[Address(RVA = "0x1015585B4", Offset = "0x15585B4", VA = "0x1015585B4")]
		protected void Update()
		{
		}

		// Token: 0x06003924 RID: 14628 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340E")]
		[Address(RVA = "0x1015585CC", Offset = "0x15585CC", VA = "0x1015585CC")]
		public void ManualUpdate()
		{
		}

		// Token: 0x06003925 RID: 14629 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600340F")]
		[Address(RVA = "0x1015585E4", Offset = "0x15585E4", VA = "0x1015585E4", Slot = "8")]
		protected virtual void ManualUpdateInternal()
		{
		}

		// Token: 0x06003926 RID: 14630 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003410")]
		[Address(RVA = "0x101558740", Offset = "0x1558740", VA = "0x101558740")]
		public void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, EquipWeaponCharacterDataController ewcdc)
		{
		}

		// Token: 0x06003927 RID: 14631 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003411")]
		[Address(RVA = "0x1015587CC", Offset = "0x15587CC", VA = "0x1015587CC")]
		public void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, CharacterDataController cdc)
		{
		}

		// Token: 0x06003928 RID: 14632 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003412")]
		[Address(RVA = "0x101558828", Offset = "0x1558828", VA = "0x101558828", Slot = "9")]
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, int charaId, int weaponId = -1)
		{
		}

		// Token: 0x06003929 RID: 14633 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003413")]
		[Address(RVA = "0x1015588D0", Offset = "0x15588D0", VA = "0x1015588D0", Slot = "10")]
		[Obsolete]
		public virtual void RequestPlayIn()
		{
		}

		// Token: 0x0600392A RID: 14634 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003414")]
		[Address(RVA = "0x101558168", Offset = "0x1558168", VA = "0x101558168", Slot = "11")]
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Idle)
		{
		}

		// Token: 0x0600392B RID: 14635 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003415")]
		[Address(RVA = "0x1015588D8", Offset = "0x15588D8", VA = "0x1015588D8", Slot = "12")]
		public virtual void FinishIn()
		{
		}

		// Token: 0x0600392C RID: 14636 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003416")]
		[Address(RVA = "0x101558920", Offset = "0x1558920", VA = "0x101558920", Slot = "13")]
		[Obsolete]
		public virtual void PlayOut(bool autoFinish)
		{
		}

		// Token: 0x0600392D RID: 14637 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003417")]
		[Address(RVA = "0x101558290", Offset = "0x1558290", VA = "0x101558290", Slot = "14")]
		public virtual void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Num)
		{
		}

		// Token: 0x0600392E RID: 14638 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003418")]
		[Address(RVA = "0x1015582E4", Offset = "0x15582E4", VA = "0x1015582E4", Slot = "15")]
		public virtual void FinishWait()
		{
		}

		// Token: 0x0600392F RID: 14639 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003419")]
		[Address(RVA = "0x101558964", Offset = "0x1558964", VA = "0x101558964", Slot = "16")]
		public virtual void Recovery()
		{
		}

		// Token: 0x06003930 RID: 14640 RVA: 0x00017E08 File Offset: 0x00016008
		[Token(Token = "0x600341A")]
		[Address(RVA = "0x101557A38", Offset = "0x1557A38", VA = "0x101557A38")]
		public SelectedCharaInfoCharacterViewBase.eState GetState()
		{
			return SelectedCharaInfoCharacterViewBase.eState.Invalid;
		}

		// Token: 0x06003931 RID: 14641 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600341B")]
		[Address(RVA = "0x1015583F4", Offset = "0x15583F4", VA = "0x1015583F4")]
		protected void SetState(SelectedCharaInfoCharacterViewBase.eState state)
		{
		}

		// Token: 0x06003932 RID: 14642 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600341C")]
		[Address(RVA = "0x10155846C", Offset = "0x155846C", VA = "0x10155846C")]
		protected void SetStateAndRegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState state, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
		}

		// Token: 0x06003933 RID: 14643 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600341D")]
		[Address(RVA = "0x101558560", Offset = "0x1558560", VA = "0x101558560")]
		protected void RegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState begin, SelectedCharaInfoCharacterViewBase.eState end)
		{
		}

		// Token: 0x06003934 RID: 14644 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600341E")]
		[Address(RVA = "0x1015589D0", Offset = "0x15589D0", VA = "0x1015589D0")]
		protected void ResetAllowStateAdvanceAll()
		{
		}

		// Token: 0x06003935 RID: 14645 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600341F")]
		[Address(RVA = "0x10155897C", Offset = "0x155897C", VA = "0x10155897C")]
		protected void RegisterStateAutomaticAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
		}

		// Token: 0x06003936 RID: 14646 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003420")]
		[Address(RVA = "0x101558978", Offset = "0x1558978", VA = "0x101558978")]
		protected void ResetAllowStateAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
		}

		// Token: 0x06003937 RID: 14647 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003421")]
		[Address(RVA = "0x101558A28", Offset = "0x1558A28", VA = "0x101558A28")]
		protected void ResetAllowStateAdvance(int nAutomaticAdvanceState)
		{
		}

		// Token: 0x06003938 RID: 14648 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003422")]
		[Address(RVA = "0x101558A78", Offset = "0x1558A78", VA = "0x101558A78", Slot = "17")]
		public virtual void SetLimitBreak(int lb)
		{
		}

		// Token: 0x06003939 RID: 14649 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003423")]
		[Address(RVA = "0x101558A7C", Offset = "0x1558A7C", VA = "0x101558A7C", Slot = "18")]
		public virtual void SetArousal(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x0600393A RID: 14650 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003424")]
		[Address(RVA = "0x101558A80", Offset = "0x1558A80", VA = "0x101558A80", Slot = "19")]
		public virtual void SetReflection(eRare rarity, int arousalLevel)
		{
		}

		// Token: 0x0600393B RID: 14651 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003425")]
		[Address(RVA = "0x101558A84", Offset = "0x1558A84", VA = "0x101558A84", Slot = "20")]
		public virtual void Replace(int charaID)
		{
		}

		// Token: 0x0600393C RID: 14652 RVA: 0x00017E20 File Offset: 0x00016020
		[Token(Token = "0x6003426")]
		[Address(RVA = "0x1015586D8", Offset = "0x15586D8", VA = "0x1015586D8")]
		protected bool IsAllowStateAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
			return default(bool);
		}

		// Token: 0x0600393D RID: 14653 RVA: 0x00017E38 File Offset: 0x00016038
		[Token(Token = "0x6003427")]
		[Address(RVA = "0x101558A88", Offset = "0x1558A88", VA = "0x101558A88", Slot = "21")]
		public virtual bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600393E RID: 14654 RVA: 0x00017E50 File Offset: 0x00016050
		[Token(Token = "0x6003428")]
		[Address(RVA = "0x101558A90", Offset = "0x1558A90", VA = "0x101558A90", Slot = "22")]
		public virtual bool IsAnimStateIn()
		{
			return default(bool);
		}

		// Token: 0x0600393F RID: 14655 RVA: 0x00017E68 File Offset: 0x00016068
		[Token(Token = "0x6003429")]
		[Address(RVA = "0x101558A98", Offset = "0x1558A98", VA = "0x101558A98", Slot = "23")]
		public virtual bool IsAnimStateOut()
		{
			return default(bool);
		}

		// Token: 0x06003940 RID: 14656 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600342A")]
		[Address(RVA = "0x1015583E4", Offset = "0x15583E4", VA = "0x1015583E4")]
		public SelectedCharaInfoCharacterViewBase()
		{
		}

		// Token: 0x0400484C RID: 18508
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032C8")]
		private SelectedCharaInfoCharacterViewBase.eState m_State;

		// Token: 0x0400484D RID: 18509
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40032C9")]
		protected int m_CharaID;

		// Token: 0x0400484E RID: 18510
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032CA")]
		protected int m_WeaponID;

		// Token: 0x0400484F RID: 18511
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032CB")]
		private bool[] m_AllowStateAdvances;

		// Token: 0x04004850 RID: 18512
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032CC")]
		private bool m_ManualUpdate;

		// Token: 0x04004851 RID: 18513
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40032CD")]
		private Action m_FinishInCallback;

		// Token: 0x02000C55 RID: 3157
		[Token(Token = "0x20010A7")]
		public enum eState
		{
			// Token: 0x04004853 RID: 18515
			[Token(Token = "0x40068D7")]
			Invalid,
			// Token: 0x04004854 RID: 18516
			[Token(Token = "0x40068D8")]
			Wait,
			// Token: 0x04004855 RID: 18517
			[Token(Token = "0x40068D9")]
			Prepare,
			// Token: 0x04004856 RID: 18518
			[Token(Token = "0x40068DA")]
			Prepared,
			// Token: 0x04004857 RID: 18519
			[Token(Token = "0x40068DB")]
			In,
			// Token: 0x04004858 RID: 18520
			[Token(Token = "0x40068DC")]
			Idle,
			// Token: 0x04004859 RID: 18521
			[Token(Token = "0x40068DD")]
			Out,
			// Token: 0x0400485A RID: 18522
			[Token(Token = "0x40068DE")]
			Num
		}
	}
}
