﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C5D RID: 3165
	[Token(Token = "0x200087D")]
	[StructLayout(3)]
	public class SelectedCharaInfoCharacterVoiceNameField : SelectedCharaInfoHaveTitleTextField
	{
		// Token: 0x06003970 RID: 14704 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345A")]
		[Address(RVA = "0x101558AA0", Offset = "0x1558AA0", VA = "0x101558AA0")]
		public void SetCharacterVoiceName(string text)
		{
		}

		// Token: 0x06003971 RID: 14705 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345B")]
		[Address(RVA = "0x101558C0C", Offset = "0x1558C0C", VA = "0x101558C0C")]
		private void Update()
		{
		}

		// Token: 0x06003972 RID: 14706 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345C")]
		[Address(RVA = "0x101558D08", Offset = "0x1558D08", VA = "0x101558D08")]
		public SelectedCharaInfoCharacterVoiceNameField()
		{
		}

		// Token: 0x0400486A RID: 18538
		[Token(Token = "0x40032DD")]
		public const int REBUILD_FRAMES = 30;

		// Token: 0x0400486B RID: 18539
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032DE")]
		private int m_Frames;
	}
}
