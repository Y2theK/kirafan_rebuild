﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C63 RID: 3171
	[Token(Token = "0x2000881")]
	[StructLayout(3)]
	public class SelectedCharaInfoHaveTitleField : MonoBehaviour
	{
		// Token: 0x06003989 RID: 14729 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003470")]
		[Address(RVA = "0x101558D18", Offset = "0x1558D18", VA = "0x101558D18", Slot = "4")]
		public virtual void Setup()
		{
		}

		// Token: 0x0600398A RID: 14730 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003471")]
		[Address(RVA = "0x101558D1C", Offset = "0x1558D1C", VA = "0x101558D1C", Slot = "5")]
		public virtual void Destroy()
		{
		}

		// Token: 0x0600398B RID: 14731 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003472")]
		[Address(RVA = "0x101558D20", Offset = "0x1558D20", VA = "0x101558D20")]
		public SelectedCharaInfoHaveTitleField()
		{
		}

		// Token: 0x0400487D RID: 18557
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032E9")]
		[SerializeField]
		private HaveImageTitle m_title;
	}
}
