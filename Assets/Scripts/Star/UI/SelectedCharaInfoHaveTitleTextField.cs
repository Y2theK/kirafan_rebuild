﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C64 RID: 3172
	[Token(Token = "0x2000882")]
	[StructLayout(3)]
	public class SelectedCharaInfoHaveTitleTextField : SelectedCharaInfoHaveTitleField
	{
		// Token: 0x0600398C RID: 14732 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003473")]
		[Address(RVA = "0x101558B54", Offset = "0x1558B54", VA = "0x101558B54")]
		public void SetText(string text)
		{
		}

		// Token: 0x0600398D RID: 14733 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003474")]
		[Address(RVA = "0x101558D10", Offset = "0x1558D10", VA = "0x101558D10")]
		public SelectedCharaInfoHaveTitleTextField()
		{
		}

		// Token: 0x0400487E RID: 18558
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032EA")]
		[SerializeField]
		private Text m_text;
	}
}
