﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C5F RID: 3167
	[Token(Token = "0x200087F")]
	[StructLayout(3)]
	public class SelectedCharaInfoLevelData : MonoBehaviour
	{
		// Token: 0x06003974 RID: 14708 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345E")]
		[Address(RVA = "0x101558D28", Offset = "0x1558D28", VA = "0x101558D28")]
		public void Setup()
		{
		}

		// Token: 0x06003975 RID: 14709 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345F")]
		[Address(RVA = "0x101558D2C", Offset = "0x1558D2C", VA = "0x101558D2C")]
		public void SetBeforeLevel(int nowLevel, int maxLevel)
		{
		}

		// Token: 0x06003976 RID: 14710 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003460")]
		[Address(RVA = "0x101558ED8", Offset = "0x1558ED8", VA = "0x101558ED8")]
		public void SetAfterLevel(int nowLevel, int maxLevel)
		{
		}

		// Token: 0x06003977 RID: 14711 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003461")]
		[Address(RVA = "0x1015590BC", Offset = "0x15590BC", VA = "0x1015590BC")]
		public void PlayAfterLevelAnimation()
		{
		}

		// Token: 0x06003978 RID: 14712 RVA: 0x00017F58 File Offset: 0x00016158
		[Token(Token = "0x6003462")]
		[Address(RVA = "0x101559158", Offset = "0x1559158", VA = "0x101559158")]
		public int GetAfterLevelAnimationState()
		{
			return 0;
		}

		// Token: 0x06003979 RID: 14713 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003463")]
		[Address(RVA = "0x1015591F8", Offset = "0x15591F8", VA = "0x1015591F8")]
		public void SetBeforeLimitBreakIconValue(int value)
		{
		}

		// Token: 0x0600397A RID: 14714 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003464")]
		[Address(RVA = "0x1015592A8", Offset = "0x15592A8", VA = "0x1015592A8")]
		public void SetAfterLimitBreakIconValue(int value)
		{
		}

		// Token: 0x0600397B RID: 14715 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003465")]
		[Address(RVA = "0x101559358", Offset = "0x1559358", VA = "0x101559358")]
		public Text GetAfterLevelText()
		{
			return null;
		}

		// Token: 0x0600397C RID: 14716 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003466")]
		[Address(RVA = "0x101559360", Offset = "0x1559360", VA = "0x101559360")]
		public SelectedCharaInfoLevelData()
		{
		}

		// Token: 0x0400486C RID: 18540
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032DF")]
		[SerializeField]
		private LimitBreakIcon m_BeforeLimitBreakIcon;

		// Token: 0x0400486D RID: 18541
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032E0")]
		[SerializeField]
		private Text m_BeforeLevelText;

		// Token: 0x0400486E RID: 18542
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032E1")]
		[SerializeField]
		private LimitBreakIcon m_AfterLimitBreakIcon;

		// Token: 0x0400486F RID: 18543
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032E2")]
		[SerializeField]
		private Text m_AfterLevelText;

		// Token: 0x04004870 RID: 18544
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40032E3")]
		[SerializeField]
		private AnimUIPlayer m_AfterLevelTextAnim;
	}
}
