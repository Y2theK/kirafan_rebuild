﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C60 RID: 3168
	[Token(Token = "0x2000880")]
	[StructLayout(3)]
	public class SelectedCharaInfoLevelInfo : MonoBehaviour
	{
		// Token: 0x0600397D RID: 14717 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003467")]
		[Address(RVA = "0x101559368", Offset = "0x1559368", VA = "0x101559368")]
		public void Setup()
		{
		}

		// Token: 0x0600397E RID: 14718 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003468")]
		[Address(RVA = "0x101559370", Offset = "0x1559370", VA = "0x101559370")]
		private void Update()
		{
		}

		// Token: 0x0600397F RID: 14719 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003469")]
		[Address(RVA = "0x101559424", Offset = "0x1559424", VA = "0x101559424")]
		public void SetLevel(int nowLevel, int maxLevel, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x06003980 RID: 14720 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600346A")]
		[Address(RVA = "0x1015594C0", Offset = "0x15594C0", VA = "0x1015594C0")]
		public void SetLevel(string nowLevelStr, string maxLevelStr, [Optional] SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info)
		{
		}

		// Token: 0x06003981 RID: 14721 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600346B")]
		[Address(RVA = "0x101559910", Offset = "0x1559910", VA = "0x101559910")]
		public void PlayAnimation()
		{
		}

		// Token: 0x06003982 RID: 14722 RVA: 0x00017F70 File Offset: 0x00016170
		[Token(Token = "0x600346C")]
		[Address(RVA = "0x1015599AC", Offset = "0x15599AC", VA = "0x1015599AC")]
		public int GetAnimationState()
		{
			return 0;
		}

		// Token: 0x06003983 RID: 14723 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600346D")]
		[Address(RVA = "0x101559A4C", Offset = "0x1559A4C", VA = "0x101559A4C")]
		public void SetLimitBreakIconValue(int value)
		{
		}

		// Token: 0x06003984 RID: 14724 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600346E")]
		[Address(RVA = "0x101559AFC", Offset = "0x1559AFC", VA = "0x101559AFC")]
		public Text GetLevelText()
		{
			return null;
		}

		// Token: 0x06003985 RID: 14725 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600346F")]
		[Address(RVA = "0x101559B04", Offset = "0x1559B04", VA = "0x101559B04")]
		public SelectedCharaInfoLevelInfo()
		{
		}

		// Token: 0x04004871 RID: 18545
		[Token(Token = "0x40032E4")]
		public const int REBUILD_FRAMES = 30;

		// Token: 0x04004872 RID: 18546
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032E5")]
		[SerializeField]
		private LimitBreakIcon m_LimitBreakIcon;

		// Token: 0x04004873 RID: 18547
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032E6")]
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x04004874 RID: 18548
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032E7")]
		[SerializeField]
		private AnimUIPlayer m_LevelTextAnim;

		// Token: 0x04004875 RID: 18549
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40032E8")]
		private int m_Frames;

		// Token: 0x02000C61 RID: 3169
		[Token(Token = "0x20010A8")]
		public enum eEmphasisExpressionType
		{
			// Token: 0x04004877 RID: 18551
			[Token(Token = "0x40068E0")]
			Normal,
			// Token: 0x04004878 RID: 18552
			[Token(Token = "0x40068E1")]
			Up,
			// Token: 0x04004879 RID: 18553
			[Token(Token = "0x40068E2")]
			Down
		}

		// Token: 0x02000C62 RID: 3170
		[Token(Token = "0x20010A9")]
		public class EmphasisExpressionInfo
		{
			// Token: 0x06003986 RID: 14726 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061CA")]
			[Address(RVA = "0x101559844", Offset = "0x1559844", VA = "0x101559844")]
			public static string eEmphasisExpressionTypeToTagStartString(SelectedCharaInfoLevelInfo.eEmphasisExpressionType tag)
			{
				return null;
			}

			// Token: 0x06003987 RID: 14727 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60061CB")]
			[Address(RVA = "0x1015598B0", Offset = "0x15598B0", VA = "0x1015598B0")]
			public static string eEmphasisExpressionTypeToTagEndString(SelectedCharaInfoLevelInfo.eEmphasisExpressionType tag)
			{
				return null;
			}

			// Token: 0x06003988 RID: 14728 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061CC")]
			[Address(RVA = "0x101559818", Offset = "0x1559818", VA = "0x101559818")]
			public EmphasisExpressionInfo()
			{
			}

			// Token: 0x0400487A RID: 18554
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x40068E3")]
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType now;

			// Token: 0x0400487B RID: 18555
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x40068E4")]
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType slash;

			// Token: 0x0400487C RID: 18556
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x40068E5")]
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType max;
		}
	}
}
