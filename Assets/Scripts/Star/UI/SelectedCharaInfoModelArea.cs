﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000C5C RID: 3164
	[Token(Token = "0x200087C")]
	[StructLayout(3)]
	public class SelectedCharaInfoModelArea : SelectedCharaInfoCharacterViewBase
	{
		// Token: 0x06003964 RID: 14692 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600344E")]
		[Address(RVA = "0x101559B0C", Offset = "0x1559B0C", VA = "0x101559B0C", Slot = "6")]
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, Action callback)
		{
		}

		// Token: 0x06003965 RID: 14693 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600344F")]
		[Address(RVA = "0x101559BE8", Offset = "0x1559BE8", VA = "0x101559BE8")]
		public void SetStencilParamForResult(int idx)
		{
		}

		// Token: 0x06003966 RID: 14694 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003450")]
		[Address(RVA = "0x101559DD0", Offset = "0x1559DD0", VA = "0x101559DD0", Slot = "5")]
		public override void Destroy()
		{
		}

		// Token: 0x06003967 RID: 14695 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003451")]
		[Address(RVA = "0x101559FB8", Offset = "0x1559FB8", VA = "0x101559FB8")]
		private void DestroyFrameImage()
		{
		}

		// Token: 0x06003968 RID: 14696 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003452")]
		[Address(RVA = "0x10155A064", Offset = "0x155A064", VA = "0x10155A064", Slot = "11")]
		public override void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
		}

		// Token: 0x06003969 RID: 14697 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003453")]
		[Address(RVA = "0x10155A760", Offset = "0x155A760", VA = "0x10155A760", Slot = "14")]
		public override void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
		}

		// Token: 0x0600396A RID: 14698 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003454")]
		[Address(RVA = "0x10155A77C", Offset = "0x155A77C", VA = "0x10155A77C", Slot = "15")]
		public override void FinishWait()
		{
		}

		// Token: 0x0600396B RID: 14699 RVA: 0x00017F10 File Offset: 0x00016110
		[Token(Token = "0x6003455")]
		[Address(RVA = "0x10155A9F8", Offset = "0x155A9F8", VA = "0x10155A9F8", Slot = "21")]
		public override bool IsPreparing()
		{
			return default(bool);
		}

		// Token: 0x0600396C RID: 14700 RVA: 0x00017F28 File Offset: 0x00016128
		[Token(Token = "0x6003456")]
		[Address(RVA = "0x10155AA28", Offset = "0x155AA28", VA = "0x10155AA28", Slot = "22")]
		public override bool IsAnimStateIn()
		{
			return default(bool);
		}

		// Token: 0x0600396D RID: 14701 RVA: 0x00017F40 File Offset: 0x00016140
		[Token(Token = "0x6003457")]
		[Address(RVA = "0x10155AA30", Offset = "0x155AA30", VA = "0x10155AA30", Slot = "23")]
		public override bool IsAnimStateOut()
		{
			return default(bool);
		}

		// Token: 0x0600396E RID: 14702 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003458")]
		[Address(RVA = "0x10155AA38", Offset = "0x155AA38", VA = "0x10155AA38")]
		public void OnPushButton()
		{
		}

		// Token: 0x0600396F RID: 14703 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003459")]
		[Address(RVA = "0x10155AB90", Offset = "0x155AB90", VA = "0x10155AB90")]
		public SelectedCharaInfoModelArea()
		{
		}

		// Token: 0x04004861 RID: 18529
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40032D4")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001225A8", Offset = "0x1225A8")]
		private RectTransform m_CharaParent;

		// Token: 0x04004862 RID: 18530
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40032D5")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001225F4", Offset = "0x1225F4")]
		private Canvas m_UICanvas;

		// Token: 0x04004863 RID: 18531
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40032D6")]
		private CharacterHandler m_CharaHandl;

		// Token: 0x04004864 RID: 18532
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40032D7")]
		[SerializeField]
		private Image m_CharaFrameImage;

		// Token: 0x04004865 RID: 18533
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40032D8")]
		[SerializeField]
		private Image m_CharaFrameMaskImage;

		// Token: 0x04004866 RID: 18534
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40032D9")]
		[SerializeField]
		private Image[] m_BGImage;

		// Token: 0x04004867 RID: 18535
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40032DA")]
		[SerializeField]
		private Texture2D[] m_RarityFrameTexture;

		// Token: 0x04004868 RID: 18536
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40032DB")]
		private CharacterDefine.eDir m_NowDir;

		// Token: 0x04004869 RID: 18537
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40032DC")]
		private Sprite m_FrameImageSprite;
	}
}
