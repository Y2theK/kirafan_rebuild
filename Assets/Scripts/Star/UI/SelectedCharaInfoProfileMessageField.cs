﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C5E RID: 3166
	[Token(Token = "0x200087E")]
	[StructLayout(3)]
	public class SelectedCharaInfoProfileMessageField : SelectedCharaInfoHaveTitleTextField
	{
		// Token: 0x06003973 RID: 14707 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600345D")]
		[Address(RVA = "0x10155ABF8", Offset = "0x155ABF8", VA = "0x10155ABF8")]
		public SelectedCharaInfoProfileMessageField()
		{
		}
	}
}
