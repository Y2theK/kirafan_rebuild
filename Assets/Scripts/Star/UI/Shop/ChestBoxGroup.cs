﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F3C RID: 3900
	[Token(Token = "0x2000A4B")]
	[StructLayout(3)]
	public class ChestBoxGroup : UIGroup
	{
		// Token: 0x060049F9 RID: 18937 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004431")]
		[Address(RVA = "0x10155AC00", Offset = "0x155AC00", VA = "0x10155AC00")]
		public void Setup()
		{
		}

		// Token: 0x060049FA RID: 18938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004432")]
		[Address(RVA = "0x10155AC34", Offset = "0x155AC34", VA = "0x10155AC34")]
		public void Reload(Chest.ChestData chestData, List<Chest.ChestContent> contentsList)
		{
		}

		// Token: 0x060049FB RID: 18939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004433")]
		[Address(RVA = "0x10155B170", Offset = "0x155B170", VA = "0x10155B170", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x060049FC RID: 18940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004434")]
		[Address(RVA = "0x10155B178", Offset = "0x155B178", VA = "0x10155B178")]
		public ChestBoxGroup()
		{
		}

		// Token: 0x04005BB5 RID: 23477
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400405E")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005BB6 RID: 23478
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400405F")]
		[SerializeField]
		private Text m_LabelText;

		// Token: 0x04005BB7 RID: 23479
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004060")]
		[SerializeField]
		private Text m_CountText;

		// Token: 0x04005BB8 RID: 23480
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004061")]
		[SerializeField]
		private CustomButton m_ResetButton;

		// Token: 0x04005BB9 RID: 23481
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004062")]
		[SerializeField]
		private Text m_ResetButtonText;
	}
}
