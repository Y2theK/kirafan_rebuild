﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F3E RID: 3902
	[Token(Token = "0x2000A4D")]
	[StructLayout(3)]
	public class ChestBoxScrollItem : ScrollItemIcon
	{
		// Token: 0x060049FF RID: 18943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004437")]
		[Address(RVA = "0x10155B180", Offset = "0x155B180", VA = "0x10155B180", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004A00 RID: 18944 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004438")]
		[Address(RVA = "0x10155B27C", Offset = "0x155B27C", VA = "0x10155B27C")]
		public void Apply(ChestBoxScrollItemData itemData)
		{
		}

		// Token: 0x06004A01 RID: 18945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004439")]
		[Address(RVA = "0x10155B6A8", Offset = "0x155B6A8", VA = "0x10155B6A8")]
		public ChestBoxScrollItem()
		{
		}

		// Token: 0x04005BC0 RID: 23488
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004069")]
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x04005BC1 RID: 23489
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400406A")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04005BC2 RID: 23490
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400406B")]
		[SerializeField]
		private Text m_LabelText;

		// Token: 0x04005BC3 RID: 23491
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400406C")]
		[SerializeField]
		private Text m_CountText;

		// Token: 0x04005BC4 RID: 23492
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400406D")]
		[SerializeField]
		private GameObject m_IsSpecialBG;

		// Token: 0x04005BC5 RID: 23493
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400406E")]
		[SerializeField]
		private GameObject m_IsSpecialObj;
	}
}
