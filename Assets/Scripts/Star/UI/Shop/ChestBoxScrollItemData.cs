﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F3D RID: 3901
	[Token(Token = "0x2000A4C")]
	[StructLayout(3)]
	public class ChestBoxScrollItemData : ScrollItemData
	{
		// Token: 0x060049FD RID: 18941 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004435")]
		[Address(RVA = "0x10155B10C", Offset = "0x155B10C", VA = "0x10155B10C")]
		public ChestBoxScrollItemData(ePresentType presentType, int id, int amount, int remain, int max, bool isSpecial)
		{
		}

		// Token: 0x060049FE RID: 18942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004436")]
		[Address(RVA = "0x10155B6B0", Offset = "0x155B6B0", VA = "0x10155B6B0", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005BBA RID: 23482
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004063")]
		public ePresentType m_PresentType;

		// Token: 0x04005BBB RID: 23483
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4004064")]
		public int m_ID;

		// Token: 0x04005BBC RID: 23484
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004065")]
		public int m_Amount;

		// Token: 0x04005BBD RID: 23485
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004066")]
		public int m_Remain;

		// Token: 0x04005BBE RID: 23486
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004067")]
		public int m_Max;

		// Token: 0x04005BBF RID: 23487
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004068")]
		public bool m_IsSpecial;
	}
}
