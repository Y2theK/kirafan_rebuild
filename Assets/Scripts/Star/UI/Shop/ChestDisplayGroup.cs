﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F3F RID: 3903
	[Token(Token = "0x2000A4E")]
	[StructLayout(3)]
	public class ChestDisplayGroup : UIGroup
	{
		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06004A02 RID: 18946 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x170004D8")]
		public Image ChestBackGround
		{
			[Token(Token = "0x600443A")]
			[Address(RVA = "0x10155B784", Offset = "0x155B784", VA = "0x10155B784")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004A03 RID: 18947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600443B")]
		[Address(RVA = "0x10155B78C", Offset = "0x155B78C", VA = "0x10155B78C", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06004A04 RID: 18948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600443C")]
		[Address(RVA = "0x10155B9D4", Offset = "0x155B9D4", VA = "0x10155B9D4")]
		public void Setup(Chest.ChestData chestData, bool enableQuestButton = true, bool enableTradeButton = true)
		{
		}

		// Token: 0x06004A05 RID: 18949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600443D")]
		[Address(RVA = "0x10155BADC", Offset = "0x155BADC", VA = "0x10155BADC")]
		public void Reload(Chest.ChestData chestData)
		{
		}

		// Token: 0x06004A06 RID: 18950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600443E")]
		[Address(RVA = "0x10155BA44", Offset = "0x155BA44", VA = "0x10155BA44")]
		public void SetEnableTransitButton(bool gotoQuestButton, bool gotoTradeButton)
		{
		}

		// Token: 0x06004A07 RID: 18951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600443F")]
		[Address(RVA = "0x10155B7DC", Offset = "0x155B7DC", VA = "0x10155B7DC")]
		public void SetItemIcon(Sprite sprite, bool isDummy = false)
		{
		}

		// Token: 0x06004A08 RID: 18952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004440")]
		[Address(RVA = "0x10155C72C", Offset = "0x155C72C", VA = "0x10155C72C")]
		public ChestDisplayGroup()
		{
		}

		// Token: 0x04005BC6 RID: 23494
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400406F")]
		[SerializeField]
		private ColorGroup m_MainPrizeCG;

		// Token: 0x04005BC7 RID: 23495
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004070")]
		[SerializeField]
		private PrefabCloner m_MainPrizeCloner;

		// Token: 0x04005BC8 RID: 23496
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004071")]
		[SerializeField]
		private Image m_HitBG;

		// Token: 0x04005BC9 RID: 23497
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004072")]
		[SerializeField]
		private Image m_HitBand;

		// Token: 0x04005BCA RID: 23498
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004073")]
		[SerializeField]
		private CustomButton m_ResetButton;

		// Token: 0x04005BCB RID: 23499
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004074")]
		[SerializeField]
		private Text m_ResetButtonText;

		// Token: 0x04005BCC RID: 23500
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004075")]
		[SerializeField]
		private Text m_CheckContentButtonText;

		// Token: 0x04005BCD RID: 23501
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004076")]
		[SerializeField]
		private CustomButton m_DrawButtonSingle;

		// Token: 0x04005BCE RID: 23502
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004077")]
		[SerializeField]
		private CustomButton m_DrawButtonMulti;

		// Token: 0x04005BCF RID: 23503
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004078")]
		[SerializeField]
		private Text m_DrawButtonSingleText;

		// Token: 0x04005BD0 RID: 23504
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004079")]
		[SerializeField]
		private Text m_DrawButtonMultiText;

		// Token: 0x04005BD1 RID: 23505
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x400407A")]
		[SerializeField]
		private Text m_DrawButtonSinglePurchaseText;

		// Token: 0x04005BD2 RID: 23506
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400407B")]
		[SerializeField]
		private Text m_DrawButtonMultiPurchaseText;

		// Token: 0x04005BD3 RID: 23507
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400407C")]
		[SerializeField]
		private Text m_RequiredItemCountLabel;

		// Token: 0x04005BD4 RID: 23508
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400407D")]
		[SerializeField]
		private Text m_RequiredItemCountText;

		// Token: 0x04005BD5 RID: 23509
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x400407E")]
		[SerializeField]
		private Text m_MainPrizeItemNameText;

		// Token: 0x04005BD6 RID: 23510
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x400407F")]
		[SerializeField]
		private Text m_BoxCountText;

		// Token: 0x04005BD7 RID: 23511
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004080")]
		[SerializeField]
		private Text m_CountText;

		// Token: 0x04005BD8 RID: 23512
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004081")]
		[SerializeField]
		private CustomButton m_GotoQuestButton;

		// Token: 0x04005BD9 RID: 23513
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004082")]
		[SerializeField]
		private CustomButton m_GotoTradeButton;

		// Token: 0x04005BDA RID: 23514
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004083")]
		[SerializeField]
		private Image m_ChestBackGround;

		// Token: 0x04005BDB RID: 23515
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004084")]
		[SerializeField]
		private Image m_GetLabel;

		// Token: 0x04005BDC RID: 23516
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004085")]
		[SerializeField]
		private Image m_ItemIconSingle;

		// Token: 0x04005BDD RID: 23517
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004086")]
		[SerializeField]
		private Image m_ItemIconMulti;

		// Token: 0x04005BDE RID: 23518
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4004087")]
		[SerializeField]
		private Image m_ItemIconHave;

		// Token: 0x04005BDF RID: 23519
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4004088")]
		[SerializeField]
		private ChestBadge m_ChestBadge;
	}
}
