﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F40 RID: 3904
	[Token(Token = "0x2000A4F")]
	[StructLayout(3)]
	public class ChestResultGroup : UIGroup
	{
		// Token: 0x06004A09 RID: 18953 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004441")]
		[Address(RVA = "0x10155C734", Offset = "0x155C734", VA = "0x10155C734")]
		public void Setup()
		{
		}

		// Token: 0x06004A0A RID: 18954 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004442")]
		[Address(RVA = "0x10155C9BC", Offset = "0x155C9BC", VA = "0x10155C9BC")]
		public void Reload(List<Chest.Result> list)
		{
		}

		// Token: 0x06004A0B RID: 18955 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004443")]
		[Address(RVA = "0x10155CEC4", Offset = "0x155CEC4", VA = "0x10155CEC4", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004A0C RID: 18956 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004444")]
		[Address(RVA = "0x10155CECC", Offset = "0x155CECC", VA = "0x10155CECC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004A0D RID: 18957 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004445")]
		[Address(RVA = "0x10155D160", Offset = "0x155D160", VA = "0x10155D160")]
		private void PopCompleteCallback()
		{
		}

		// Token: 0x06004A0E RID: 18958 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004446")]
		[Address(RVA = "0x10155D164", Offset = "0x155D164", VA = "0x10155D164")]
		public ChestResultGroup()
		{
		}

		// Token: 0x04005BE0 RID: 23520
		[Token(Token = "0x4004089")]
		private const float ICON_POP_SPEED = 16f;

		// Token: 0x04005BE1 RID: 23521
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400408A")]
		[SerializeField]
		private ChestResultScrollView m_Scroll;

		// Token: 0x04005BE2 RID: 23522
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400408B")]
		private bool m_PopFinishTrigger;

		// Token: 0x04005BE3 RID: 23523
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400408C")]
		private GameObject m_LockObject;
	}
}
