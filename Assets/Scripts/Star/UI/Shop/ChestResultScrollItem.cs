﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F42 RID: 3906
	[Token(Token = "0x2000A51")]
	[StructLayout(3)]
	public class ChestResultScrollItem : ScrollItemIcon
	{
		// Token: 0x06004A11 RID: 18961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004449")]
		[Address(RVA = "0x10155D16C", Offset = "0x155D16C", VA = "0x10155D16C", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004A12 RID: 18962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600444A")]
		[Address(RVA = "0x10155D268", Offset = "0x155D268", VA = "0x10155D268")]
		public void Apply(ChestResultScrollItemData itemData)
		{
		}

		// Token: 0x06004A13 RID: 18963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600444B")]
		[Address(RVA = "0x10155D45C", Offset = "0x155D45C", VA = "0x10155D45C")]
		public static void ShowItemCallback(ScrollItemIcon scrollItemIcon)
		{
		}

		// Token: 0x06004A14 RID: 18964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600444C")]
		[Address(RVA = "0x10155D620", Offset = "0x155D620", VA = "0x10155D620")]
		public static void PopSoundCallback(ScrollItemIcon scrollItemIcon)
		{
		}

		// Token: 0x06004A15 RID: 18965 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600444D")]
		[Address(RVA = "0x10155D7F8", Offset = "0x155D7F8", VA = "0x10155D7F8")]
		public ChestResultScrollItem()
		{
		}

		// Token: 0x04005BE8 RID: 23528
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004091")]
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x04005BE9 RID: 23529
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004092")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04005BEA RID: 23530
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004093")]
		[SerializeField]
		private GameObject m_IsSpecialBG;

		// Token: 0x04005BEB RID: 23531
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004094")]
		[SerializeField]
		private GameObject m_IsSpecialObj;

		// Token: 0x04005BEC RID: 23532
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004095")]
		[SerializeField]
		private GameObject m_AnimationTarget;

		// Token: 0x04005BED RID: 23533
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004096")]
		private ChestResultScrollItemData m_ItemData;
	}
}
