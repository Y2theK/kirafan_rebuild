﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F41 RID: 3905
	[Token(Token = "0x2000A50")]
	[StructLayout(3)]
	public class ChestResultScrollItemData : ScrollItemData
	{
		// Token: 0x06004A0F RID: 18959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004447")]
		[Address(RVA = "0x10155CD08", Offset = "0x155CD08", VA = "0x10155CD08")]
		public ChestResultScrollItemData(ePresentType presentType, int id, int amount, bool isSpecial)
		{
		}

		// Token: 0x06004A10 RID: 18960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004448")]
		[Address(RVA = "0x10155D800", Offset = "0x155D800", VA = "0x10155D800", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005BE4 RID: 23524
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400408D")]
		public ePresentType m_PresentType;

		// Token: 0x04005BE5 RID: 23525
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400408E")]
		public int m_ID;

		// Token: 0x04005BE6 RID: 23526
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400408F")]
		public int m_Amount;

		// Token: 0x04005BE7 RID: 23527
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x4004090")]
		public bool m_IsSpecial;
	}
}
