﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F43 RID: 3907
	[Token(Token = "0x2000A52")]
	[StructLayout(3)]
	public class ChestResultScrollView : ScrollViewBase
	{
		// Token: 0x06004A16 RID: 18966 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600444E")]
		[Address(RVA = "0x10155CD58", Offset = "0x155CD58", VA = "0x10155CD58")]
		public void SetupPopAnimation(float popSpeed, Action<ScrollItemIcon> showItemCallback, Action<ScrollItemIcon> popSoundCallback, Action popCompleteCallback)
		{
		}

		// Token: 0x06004A17 RID: 18967 RVA: 0x0001AA90 File Offset: 0x00018C90
		[Token(Token = "0x600444F")]
		[Address(RVA = "0x10155CFAC", Offset = "0x155CFAC", VA = "0x10155CFAC")]
		public bool ExecPopAnimation()
		{
			return default(bool);
		}

		// Token: 0x06004A18 RID: 18968 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004450")]
		[Address(RVA = "0x10155D8C8", Offset = "0x155D8C8", VA = "0x10155D8C8", Slot = "9")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004A19 RID: 18969 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004451")]
		[Address(RVA = "0x10155DB54", Offset = "0x155DB54", VA = "0x10155DB54")]
		public ChestResultScrollView()
		{
		}

		// Token: 0x04005BEE RID: 23534
		[Cpp2IlInjected.FieldOffset(Offset = "0xAC")]
		[Token(Token = "0x4004097")]
		private float m_PopSpeed;

		// Token: 0x04005BEF RID: 23535
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004098")]
		private float m_Elapsed;

		// Token: 0x04005BF0 RID: 23536
		[Cpp2IlInjected.FieldOffset(Offset = "0xB4")]
		[Token(Token = "0x4004099")]
		private int m_PrizeCount;

		// Token: 0x04005BF1 RID: 23537
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400409A")]
		private int m_AnimationIconIndex;

		// Token: 0x04005BF2 RID: 23538
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x400409B")]
		private bool m_FinishPopAnimation;

		// Token: 0x04005BF3 RID: 23539
		[Cpp2IlInjected.FieldOffset(Offset = "0xBD")]
		[Token(Token = "0x400409C")]
		private bool m_FirstProcess;

		// Token: 0x04005BF4 RID: 23540
		[Cpp2IlInjected.FieldOffset(Offset = "0xBE")]
		[Token(Token = "0x400409D")]
		private bool m_LastProcess;

		// Token: 0x04005BF5 RID: 23541
		[Cpp2IlInjected.FieldOffset(Offset = "0xBF")]
		[Token(Token = "0x400409E")]
		private bool m_ScrollBarFlag;

		// Token: 0x04005BF6 RID: 23542
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400409F")]
		private Action<ScrollItemIcon> m_ShowItemCallback;

		// Token: 0x04005BF7 RID: 23543
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40040A0")]
		private Action<ScrollItemIcon> m_PopSoundCallback;

		// Token: 0x04005BF8 RID: 23544
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40040A1")]
		private Action m_PopCompleteCallback;
	}
}
