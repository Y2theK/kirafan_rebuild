﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F45 RID: 3909
	[Token(Token = "0x2000A53")]
	[StructLayout(3)]
	public class ShopBuildTopUI : MenuUIBase
	{
		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06004A1D RID: 18973 RVA: 0x0001AAC0 File Offset: 0x00018CC0
		[Token(Token = "0x170004D9")]
		public ShopBuildTopUI.eMainButton SelectButton
		{
			[Token(Token = "0x6004452")]
			[Address(RVA = "0x10155DC0C", Offset = "0x155DC0C", VA = "0x10155DC0C")]
			get
			{
				return ShopBuildTopUI.eMainButton.Buy;
			}
		}

		// Token: 0x140000D3 RID: 211
		// (add) Token: 0x06004A1E RID: 18974 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A1F RID: 18975 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D3")]
		public event Action OnClickMainButton
		{
			[Token(Token = "0x6004453")]
			[Address(RVA = "0x10155DC14", Offset = "0x155DC14", VA = "0x10155DC14")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004454")]
			[Address(RVA = "0x10155DD20", Offset = "0x155DD20", VA = "0x10155DD20")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A20 RID: 18976 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004455")]
		[Address(RVA = "0x10155DE2C", Offset = "0x155DE2C", VA = "0x10155DE2C")]
		public void Setup()
		{
		}

		// Token: 0x06004A21 RID: 18977 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004456")]
		[Address(RVA = "0x10155DED8", Offset = "0x155DED8", VA = "0x10155DED8")]
		private void Update()
		{
		}

		// Token: 0x06004A22 RID: 18978 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004457")]
		[Address(RVA = "0x10155E108", Offset = "0x155E108", VA = "0x10155E108")]
		private void ChangeStep(ShopBuildTopUI.eStep step)
		{
		}

		// Token: 0x06004A23 RID: 18979 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004458")]
		[Address(RVA = "0x10155E3E8", Offset = "0x155E3E8", VA = "0x10155E3E8", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004A24 RID: 18980 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004459")]
		[Address(RVA = "0x10155E3F0", Offset = "0x155E3F0", VA = "0x10155E3F0", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004A25 RID: 18981 RVA: 0x0001AAD8 File Offset: 0x00018CD8
		[Token(Token = "0x600445A")]
		[Address(RVA = "0x10155E490", Offset = "0x155E490", VA = "0x10155E490", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004A26 RID: 18982 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600445B")]
		[Address(RVA = "0x10155E4A0", Offset = "0x155E4A0", VA = "0x10155E4A0")]
		public void OnClickMainButtonCallBack(int btn)
		{
		}

		// Token: 0x06004A27 RID: 18983 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600445C")]
		[Address(RVA = "0x10155E4B0", Offset = "0x155E4B0", VA = "0x10155E4B0")]
		public void ResetSelectButton()
		{
		}

		// Token: 0x06004A28 RID: 18984 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600445D")]
		[Address(RVA = "0x10155E4BC", Offset = "0x155E4BC", VA = "0x10155E4BC")]
		public ShopBuildTopUI()
		{
		}

		// Token: 0x04005BFB RID: 23547
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40040A2")]
		private ShopBuildTopUI.eStep m_Step;

		// Token: 0x04005BFC RID: 23548
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40040A3")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005BFD RID: 23549
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40040A4")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005BFE RID: 23550
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40040A5")]
		private ShopBuildTopUI.eMainButton m_SelectButton;

		// Token: 0x04005BFF RID: 23551
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40040A6")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x02000F46 RID: 3910
		[Token(Token = "0x20011B8")]
		private enum eStep
		{
			// Token: 0x04005C02 RID: 23554
			[Token(Token = "0x4006EA3")]
			Hide,
			// Token: 0x04005C03 RID: 23555
			[Token(Token = "0x4006EA4")]
			BGLoad,
			// Token: 0x04005C04 RID: 23556
			[Token(Token = "0x4006EA5")]
			In,
			// Token: 0x04005C05 RID: 23557
			[Token(Token = "0x4006EA6")]
			Idle,
			// Token: 0x04005C06 RID: 23558
			[Token(Token = "0x4006EA7")]
			Out,
			// Token: 0x04005C07 RID: 23559
			[Token(Token = "0x4006EA8")]
			End
		}

		// Token: 0x02000F47 RID: 3911
		[Token(Token = "0x20011B9")]
		public enum eMainButton
		{
			// Token: 0x04005C09 RID: 23561
			[Token(Token = "0x4006EAA")]
			None = -1,
			// Token: 0x04005C0A RID: 23562
			[Token(Token = "0x4006EAB")]
			Buy,
			// Token: 0x04005C0B RID: 23563
			[Token(Token = "0x4006EAC")]
			Sale,
			// Token: 0x04005C0C RID: 23564
			[Token(Token = "0x4006EAD")]
			LimitExtend,
			// Token: 0x04005C0D RID: 23565
			[Token(Token = "0x4006EAE")]
			LimitExtendTown
		}
	}
}
