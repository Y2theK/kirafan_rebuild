﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F48 RID: 3912
	[Token(Token = "0x2000A54")]
	[StructLayout(3)]
	public class ShopChestUI : MenuUIBase
	{
		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06004A29 RID: 18985 RVA: 0x0001AAF0 File Offset: 0x00018CF0
		[Token(Token = "0x170004DA")]
		public ShopChestUI.eMainButton SelectButton
		{
			[Token(Token = "0x600445E")]
			[Address(RVA = "0x10155E4CC", Offset = "0x155E4CC", VA = "0x10155E4CC")]
			get
			{
				return ShopChestUI.eMainButton.Draw;
			}
		}

		// Token: 0x140000D4 RID: 212
		// (add) Token: 0x06004A2A RID: 18986 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A2B RID: 18987 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D4")]
		public event Action OnClickMainButton
		{
			[Token(Token = "0x600445F")]
			[Address(RVA = "0x10155E4D4", Offset = "0x155E4D4", VA = "0x10155E4D4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004460")]
			[Address(RVA = "0x10155E5E0", Offset = "0x155E5E0", VA = "0x10155E5E0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A2C RID: 18988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004461")]
		[Address(RVA = "0x10155E6EC", Offset = "0x155E6EC", VA = "0x10155E6EC")]
		public void Setup(int chestID, bool enableQuestButton = true, bool enableTradeButton = true)
		{
		}

		// Token: 0x06004A2D RID: 18989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004462")]
		[Address(RVA = "0x10155E878", Offset = "0x155E878", VA = "0x10155E878", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004A2E RID: 18990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004463")]
		[Address(RVA = "0x10155E990", Offset = "0x155E990", VA = "0x10155E990")]
		private void Update()
		{
		}

		// Token: 0x06004A2F RID: 18991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004464")]
		[Address(RVA = "0x10155F064", Offset = "0x155F064", VA = "0x10155F064")]
		private void ChangeStep(ShopChestUI.eStep step)
		{
		}

		// Token: 0x06004A30 RID: 18992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004465")]
		[Address(RVA = "0x10155F68C", Offset = "0x155F68C", VA = "0x10155F68C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004A31 RID: 18993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004466")]
		[Address(RVA = "0x10155F694", Offset = "0x155F694", VA = "0x10155F694", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004A32 RID: 18994 RVA: 0x0001AB08 File Offset: 0x00018D08
		[Token(Token = "0x6004467")]
		[Address(RVA = "0x10155F734", Offset = "0x155F734", VA = "0x10155F734", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004A33 RID: 18995 RVA: 0x0001AB20 File Offset: 0x00018D20
		[Token(Token = "0x6004468")]
		[Address(RVA = "0x10155F744", Offset = "0x155F744", VA = "0x10155F744")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06004A34 RID: 18996 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004469")]
		[Address(RVA = "0x10155F384", Offset = "0x155F384", VA = "0x10155F384")]
		private Sprite GetBGSprite(string name)
		{
			return null;
		}

		// Token: 0x06004A35 RID: 18997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600446A")]
		[Address(RVA = "0x10155F754", Offset = "0x155F754", VA = "0x10155F754")]
		public void Reload()
		{
		}

		// Token: 0x06004A36 RID: 18998 RVA: 0x0001AB38 File Offset: 0x00018D38
		[Token(Token = "0x600446B")]
		[Address(RVA = "0x10155F894", Offset = "0x155F894", VA = "0x10155F894")]
		public bool IsFinishReload()
		{
			return default(bool);
		}

		// Token: 0x06004A37 RID: 18999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600446C")]
		[Address(RVA = "0x10155F89C", Offset = "0x155F89C", VA = "0x10155F89C")]
		public void ReloadCurrentContents()
		{
		}

		// Token: 0x06004A38 RID: 19000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600446D")]
		[Address(RVA = "0x10155F9A8", Offset = "0x155F9A8", VA = "0x10155F9A8")]
		public void OpenResult()
		{
		}

		// Token: 0x06004A39 RID: 19001 RVA: 0x0001AB50 File Offset: 0x00018D50
		[Token(Token = "0x600446E")]
		[Address(RVA = "0x10155FA6C", Offset = "0x155FA6C", VA = "0x10155FA6C")]
		public bool IsClosedResult()
		{
			return default(bool);
		}

		// Token: 0x06004A3A RID: 19002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600446F")]
		[Address(RVA = "0x10155FA9C", Offset = "0x155FA9C", VA = "0x10155FA9C")]
		public void OnClickDrawButtonCallBack()
		{
		}

		// Token: 0x06004A3B RID: 19003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004470")]
		[Address(RVA = "0x10155FAAC", Offset = "0x155FAAC", VA = "0x10155FAAC")]
		public void OnClickDrawMultiButtonCallBack()
		{
		}

		// Token: 0x06004A3C RID: 19004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004471")]
		[Address(RVA = "0x10155FAC0", Offset = "0x155FAC0", VA = "0x10155FAC0")]
		public void OnClickResetButtonCallBack()
		{
		}

		// Token: 0x06004A3D RID: 19005 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004472")]
		[Address(RVA = "0x10155FC28", Offset = "0x155FC28", VA = "0x10155FC28")]
		public void OnConfirmReset(int btn)
		{
		}

		// Token: 0x06004A3E RID: 19006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004473")]
		[Address(RVA = "0x10155FC44", Offset = "0x155FC44", VA = "0x10155FC44")]
		public void OnClickResetButtonCallBackBoxGroup()
		{
		}

		// Token: 0x06004A3F RID: 19007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004474")]
		[Address(RVA = "0x10155FDAC", Offset = "0x155FDAC", VA = "0x10155FDAC")]
		public void OnConfirmResetBoxGroup(int btn)
		{
		}

		// Token: 0x06004A40 RID: 19008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004475")]
		[Address(RVA = "0x10155FDC8", Offset = "0x155FDC8", VA = "0x10155FDC8")]
		public void OnClickQuestButtonCallBack()
		{
		}

		// Token: 0x06004A41 RID: 19009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004476")]
		[Address(RVA = "0x10155FDDC", Offset = "0x155FDDC", VA = "0x10155FDDC")]
		public void OnClickTradeButtonCallBack()
		{
		}

		// Token: 0x06004A42 RID: 19010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004477")]
		[Address(RVA = "0x10155FDF0", Offset = "0x155FDF0", VA = "0x10155FDF0")]
		public void OnClickHelpButtonCallBack()
		{
		}

		// Token: 0x06004A43 RID: 19011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004478")]
		[Address(RVA = "0x10155FE04", Offset = "0x155FE04", VA = "0x10155FE04")]
		public void OnClickCheckBoxButtonCallBack()
		{
		}

		// Token: 0x06004A44 RID: 19012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004479")]
		[Address(RVA = "0x10155FE18", Offset = "0x155FE18", VA = "0x10155FE18")]
		public void PlayEffect(bool isRare)
		{
		}

		// Token: 0x06004A45 RID: 19013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600447A")]
		[Address(RVA = "0x10155F49C", Offset = "0x155F49C", VA = "0x10155F49C")]
		private void UpdateEffect()
		{
		}

		// Token: 0x06004A46 RID: 19014 RVA: 0x0001AB68 File Offset: 0x00018D68
		[Token(Token = "0x600447B")]
		[Address(RVA = "0x1015600F0", Offset = "0x15600F0", VA = "0x1015600F0")]
		public bool isPlayEffect()
		{
			return default(bool);
		}

		// Token: 0x06004A47 RID: 19015 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600447C")]
		[Address(RVA = "0x10156018C", Offset = "0x156018C", VA = "0x10156018C")]
		public ShopChestUI()
		{
		}

		// Token: 0x04005C0E RID: 23566
		[Token(Token = "0x40040A8")]
		private const string CHEST_EFFECT_PACK_NAME = "ef_Chest";

		// Token: 0x04005C0F RID: 23567
		[Token(Token = "0x40040A9")]
		private const string CHEST_EFFECT_NORMAL_NAME = "ef_ChestDraw_1";

		// Token: 0x04005C10 RID: 23568
		[Token(Token = "0x40040AA")]
		private const string CHEST_EFFECT_RARE_NAME = "ef_ChestDraw_2";

		// Token: 0x04005C11 RID: 23569
		[Token(Token = "0x40040AB")]
		private const string BG_RESOURCE_PATH = "texture/chest.muast";

		// Token: 0x04005C12 RID: 23570
		[Token(Token = "0x40040AC")]
		private const string BG_OBJ_BASENAME = "ChestBG_";

		// Token: 0x04005C13 RID: 23571
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40040AD")]
		private ShopChestUI.eStep m_Step;

		// Token: 0x04005C14 RID: 23572
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40040AE")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005C15 RID: 23573
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40040AF")]
		[SerializeField]
		private ChestDisplayGroup m_MainGroup;

		// Token: 0x04005C16 RID: 23574
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40040B0")]
		[SerializeField]
		private ChestBoxGroup m_BoxGroup;

		// Token: 0x04005C17 RID: 23575
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40040B1")]
		[SerializeField]
		private ChestResultGroup m_ResultGroup;

		// Token: 0x04005C18 RID: 23576
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40040B2")]
		[SerializeField]
		private GameObject m_effectRoot;

		// Token: 0x04005C19 RID: 23577
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40040B3")]
		private EffectHandler m_ChestOpenEffHandler;

		// Token: 0x04005C1A RID: 23578
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40040B4")]
		private ShopChestUI.eMainButton m_SelectButton;

		// Token: 0x04005C1B RID: 23579
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x40040B5")]
		private int m_ChestID;

		// Token: 0x04005C1C RID: 23580
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40040B6")]
		private SpriteHandler m_SpriteHandlerItemIcon;

		// Token: 0x04005C1D RID: 23581
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40040B7")]
		private string m_CurrentChestBGName;

		// Token: 0x04005C1E RID: 23582
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40040B8")]
		private ABResourceLoader m_Loader;

		// Token: 0x04005C1F RID: 23583
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40040B9")]
		private ABResourceObjectHandler m_ResourceHandler;

		// Token: 0x04005C20 RID: 23584
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40040BA")]
		private List<Sprite> m_BGSpriteList;

		// Token: 0x02000F49 RID: 3913
		[Token(Token = "0x20011BA")]
		private enum eStep
		{
			// Token: 0x04005C23 RID: 23587
			[Token(Token = "0x4006EB0")]
			Hide,
			// Token: 0x04005C24 RID: 23588
			[Token(Token = "0x4006EB1")]
			BGLoad,
			// Token: 0x04005C25 RID: 23589
			[Token(Token = "0x4006EB2")]
			ChestBGLoad,
			// Token: 0x04005C26 RID: 23590
			[Token(Token = "0x4006EB3")]
			EffectLoad,
			// Token: 0x04005C27 RID: 23591
			[Token(Token = "0x4006EB4")]
			In,
			// Token: 0x04005C28 RID: 23592
			[Token(Token = "0x4006EB5")]
			Idle,
			// Token: 0x04005C29 RID: 23593
			[Token(Token = "0x4006EB6")]
			Out,
			// Token: 0x04005C2A RID: 23594
			[Token(Token = "0x4006EB7")]
			End
		}

		// Token: 0x02000F4A RID: 3914
		[Token(Token = "0x20011BB")]
		public enum eMainButton
		{
			// Token: 0x04005C2C RID: 23596
			[Token(Token = "0x4006EB9")]
			None = -1,
			// Token: 0x04005C2D RID: 23597
			[Token(Token = "0x4006EBA")]
			Draw,
			// Token: 0x04005C2E RID: 23598
			[Token(Token = "0x4006EBB")]
			DrawMulti,
			// Token: 0x04005C2F RID: 23599
			[Token(Token = "0x4006EBC")]
			Reset,
			// Token: 0x04005C30 RID: 23600
			[Token(Token = "0x4006EBD")]
			Quest,
			// Token: 0x04005C31 RID: 23601
			[Token(Token = "0x4006EBE")]
			Trade,
			// Token: 0x04005C32 RID: 23602
			[Token(Token = "0x4006EBF")]
			Help,
			// Token: 0x04005C33 RID: 23603
			[Token(Token = "0x4006EC0")]
			CheckStep
		}
	}
}
