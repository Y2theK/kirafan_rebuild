﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F3A RID: 3898
	[Token(Token = "0x2000A4A")]
	[StructLayout(3)]
	public class ShopMarketUI : MenuUIBase
	{
		// Token: 0x060049DE RID: 18910 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004416")]
		[Address(RVA = "0x101560204", Offset = "0x1560204", VA = "0x101560204")]
		public void OnDestroy()
		{
		}

		// Token: 0x060049DF RID: 18911 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004417")]
		[Address(RVA = "0x1015602A0", Offset = "0x15602A0", VA = "0x1015602A0")]
		public void Setup(ShopState_Market ownerState)
		{
		}

		// Token: 0x060049E0 RID: 18912 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004418")]
		[Address(RVA = "0x101560CD0", Offset = "0x1560CD0", VA = "0x101560CD0", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x060049E1 RID: 18913 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004419")]
		[Address(RVA = "0x101560230", Offset = "0x1560230", VA = "0x101560230")]
		public void UnloadResource()
		{
		}

		// Token: 0x060049E2 RID: 18914 RVA: 0x0001AA30 File Offset: 0x00018C30
		[Token(Token = "0x600441A")]
		[Address(RVA = "0x101560D0C", Offset = "0x1560D0C", VA = "0x101560D0C")]
		public bool IsCompleteUnload()
		{
			return default(bool);
		}

		// Token: 0x060049E3 RID: 18915 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600441B")]
		[Address(RVA = "0x101560D3C", Offset = "0x1560D3C", VA = "0x101560D3C")]
		public void Refresh()
		{
		}

		// Token: 0x060049E4 RID: 18916 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600441C")]
		[Address(RVA = "0x101560EE4", Offset = "0x1560EE4", VA = "0x101560EE4")]
		public void RefreshButton()
		{
		}

		// Token: 0x060049E5 RID: 18917 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600441D")]
		[Address(RVA = "0x101561214", Offset = "0x1561214", VA = "0x101561214")]
		public void RefreshPageButton()
		{
		}

		// Token: 0x060049E6 RID: 18918 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600441E")]
		[Address(RVA = "0x101561170", Offset = "0x1561170", VA = "0x101561170")]
		private void GetBadgeFlag(GemShopCommon.eCategory category, out bool isNew, out bool isUpdate)
		{
		}

		// Token: 0x060049E7 RID: 18919 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600441F")]
		[Address(RVA = "0x101561310", Offset = "0x1561310", VA = "0x101561310")]
		private void Update()
		{
		}

		// Token: 0x060049E8 RID: 18920 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004420")]
		[Address(RVA = "0x101561684", Offset = "0x1561684", VA = "0x101561684")]
		private void ChangeStep(ShopMarketUI.eStep step)
		{
		}

		// Token: 0x060049E9 RID: 18921 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004421")]
		[Address(RVA = "0x10156178C", Offset = "0x156178C", VA = "0x10156178C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060049EA RID: 18922 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004422")]
		[Address(RVA = "0x1015618A4", Offset = "0x15618A4", VA = "0x1015618A4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060049EB RID: 18923 RVA: 0x0001AA48 File Offset: 0x00018C48
		[Token(Token = "0x6004423")]
		[Address(RVA = "0x1015619BC", Offset = "0x15619BC", VA = "0x1015619BC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060049EC RID: 18924 RVA: 0x0001AA60 File Offset: 0x00018C60
		[Token(Token = "0x6004424")]
		[Address(RVA = "0x1015619CC", Offset = "0x15619CC", VA = "0x1015619CC")]
		public bool IsOpened()
		{
			return default(bool);
		}

		// Token: 0x060049ED RID: 18925 RVA: 0x0001AA78 File Offset: 0x00018C78
		[Token(Token = "0x6004425")]
		[Address(RVA = "0x101560C5C", Offset = "0x1560C5C", VA = "0x101560C5C")]
		private GemShopCommon.eCategory GetCurrentCategory(int index, int page)
		{
			return GemShopCommon.eCategory.Gem;
		}

		// Token: 0x060049EE RID: 18926 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004426")]
		[Address(RVA = "0x1015619DC", Offset = "0x15619DC", VA = "0x1015619DC")]
		public void OnClickChangeCategoryButton(int index)
		{
		}

		// Token: 0x060049EF RID: 18927 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004427")]
		[Address(RVA = "0x101561A10", Offset = "0x1561A10", VA = "0x101561A10")]
		public void OnClickChangePageButton(int index)
		{
		}

		// Token: 0x060049F0 RID: 18928 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004428")]
		[Address(RVA = "0x101561A38", Offset = "0x1561A38", VA = "0x101561A38")]
		public void OnClickURLButton(int idx)
		{
		}

		// Token: 0x060049F1 RID: 18929 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004429")]
		[Address(RVA = "0x101561A70", Offset = "0x1561A70", VA = "0x101561A70")]
		private void Purchase(int id)
		{
		}

		// Token: 0x060049F2 RID: 18930 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442A")]
		[Address(RVA = "0x101561A84", Offset = "0x1561A84", VA = "0x101561A84")]
		private void OpenExchangeSelectGroup(StoreManager.Product product)
		{
		}

		// Token: 0x060049F3 RID: 18931 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442B")]
		[Address(RVA = "0x101561B54", Offset = "0x1561B54", VA = "0x101561B54")]
		private void OpenExchangeConfirmGroup(StoreManager.Product product, int buyAmount)
		{
		}

		// Token: 0x060049F4 RID: 18932 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442C")]
		[Address(RVA = "0x101561C74", Offset = "0x1561C74", VA = "0x101561C74")]
		private void Exchange(StoreManager.Product product, int buyAmount)
		{
		}

		// Token: 0x060049F5 RID: 18933 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442D")]
		[Address(RVA = "0x101561CE0", Offset = "0x1561CE0", VA = "0x101561CE0")]
		private void OpenDetailWindow(string title, string message, string buttonText, bool existImage)
		{
		}

		// Token: 0x060049F6 RID: 18934 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442E")]
		[Address(RVA = "0x101561D98", Offset = "0x1561D98", VA = "0x101561D98")]
		private void ReplaceRegularPassHelpBanner(Sprite sprite)
		{
		}

		// Token: 0x060049F7 RID: 18935 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600442F")]
		[Address(RVA = "0x101561E2C", Offset = "0x1561E2C", VA = "0x101561E2C")]
		public void OpenPurchaseListWindow(string title, string message, string subMessage)
		{
		}

		// Token: 0x060049F8 RID: 18936 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004430")]
		[Address(RVA = "0x101561EA8", Offset = "0x1561EA8", VA = "0x101561EA8")]
		public ShopMarketUI()
		{
		}

		// Token: 0x04005B9A RID: 23450
		[Token(Token = "0x4004049")]
		private const int BUTTON_NUM = 4;

		// Token: 0x04005B9B RID: 23451
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400404A")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005B9C RID: 23452
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400404B")]
		[SerializeField]
		private UIGroup[] m_MainGroup;

		// Token: 0x04005B9D RID: 23453
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400404C")]
		[SerializeField]
		private GemShopDetailGroup m_DetailGroup;

		// Token: 0x04005B9E RID: 23454
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400404D")]
		[SerializeField]
		private GemShopDetailContainerGroup m_DetailContainerGroup;

		// Token: 0x04005B9F RID: 23455
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400404E")]
		[SerializeField]
		private GemShopPurchaseListGroup m_PurchaseListGroup;

		// Token: 0x04005BA0 RID: 23456
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400404F")]
		[SerializeField]
		private MarketExchangeSelectGroup m_MarketExchangeSelectGroup;

		// Token: 0x04005BA1 RID: 23457
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004050")]
		[SerializeField]
		private MarketExchangeConfirmGroup m_MarketExchangeConfirmGroup;

		// Token: 0x04005BA2 RID: 23458
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004051")]
		[SerializeField]
		private Text m_UnlimitedGemCount;

		// Token: 0x04005BA3 RID: 23459
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004052")]
		[SerializeField]
		private GemShopScrollView m_Scroll;

		// Token: 0x04005BA4 RID: 23460
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004053")]
		[SerializeField]
		private MarketCategoryButton[] m_MarketCategoryButton;

		// Token: 0x04005BA5 RID: 23461
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004054")]
		[SerializeField]
		private CustomButton[] m_PageButton;

		// Token: 0x04005BA6 RID: 23462
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004055")]
		private ShopMarketUI.eStep m_Step;

		// Token: 0x04005BA7 RID: 23463
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004056")]
		private ShopState_Market m_OwnerState;

		// Token: 0x04005BA8 RID: 23464
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004057")]
		private GemShopCommon.eCategory m_CurrentCategory;

		// Token: 0x04005BA9 RID: 23465
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004058")]
		private List<StoreDefine.eProductType>[] m_ProductTypeTable;

		// Token: 0x04005BAA RID: 23466
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004059")]
		private List<GemShopCommon.eCategory> m_ButtonType;

		// Token: 0x04005BAB RID: 23467
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400405A")]
		private string[] m_ButtonText;

		// Token: 0x04005BAC RID: 23468
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400405B")]
		private int m_Page;

		// Token: 0x04005BAD RID: 23469
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400405C")]
		private long m_GemOld;

		// Token: 0x04005BAE RID: 23470
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x400405D")]
		private GemShopCommon m_GemShopCommon;

		// Token: 0x02000F3B RID: 3899
		[Token(Token = "0x20011B6")]
		private enum eStep
		{
			// Token: 0x04005BB0 RID: 23472
			[Token(Token = "0x4006E9B")]
			Hide,
			// Token: 0x04005BB1 RID: 23473
			[Token(Token = "0x4006E9C")]
			In,
			// Token: 0x04005BB2 RID: 23474
			[Token(Token = "0x4006E9D")]
			Idle,
			// Token: 0x04005BB3 RID: 23475
			[Token(Token = "0x4006E9E")]
			Out,
			// Token: 0x04005BB4 RID: 23476
			[Token(Token = "0x4006E9F")]
			End
		}
	}
}
