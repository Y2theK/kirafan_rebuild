﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F4B RID: 3915
	[Token(Token = "0x2000A55")]
	[StructLayout(3)]
	public class ShopTopUI : MenuUIBase
	{
		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06004A48 RID: 19016 RVA: 0x0001AB80 File Offset: 0x00018D80
		[Token(Token = "0x170004DB")]
		public ShopTopUI.eMainButton SelectButton
		{
			[Token(Token = "0x600447D")]
			[Address(RVA = "0x101561F10", Offset = "0x1561F10", VA = "0x101561F10")]
			get
			{
				return ShopTopUI.eMainButton.Trade;
			}
		}

		// Token: 0x140000D5 RID: 213
		// (add) Token: 0x06004A49 RID: 19017 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A4A RID: 19018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D5")]
		public event Action OnClickMainButton
		{
			[Token(Token = "0x600447E")]
			[Address(RVA = "0x101561F18", Offset = "0x1561F18", VA = "0x101561F18")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600447F")]
			[Address(RVA = "0x101562024", Offset = "0x1562024", VA = "0x101562024")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A4B RID: 19019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004480")]
		[Address(RVA = "0x101562130", Offset = "0x1562130", VA = "0x101562130")]
		public void Setup()
		{
		}

		// Token: 0x06004A4C RID: 19020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004481")]
		[Address(RVA = "0x1015621DC", Offset = "0x15621DC", VA = "0x1015621DC")]
		private void Update()
		{
		}

		// Token: 0x06004A4D RID: 19021 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004482")]
		[Address(RVA = "0x1015623F0", Offset = "0x15623F0", VA = "0x1015623F0")]
		private void ChangeStep(ShopTopUI.eStep step)
		{
		}

		// Token: 0x06004A4E RID: 19022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004483")]
		[Address(RVA = "0x101562614", Offset = "0x1562614", VA = "0x101562614", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004A4F RID: 19023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004484")]
		[Address(RVA = "0x1015626B4", Offset = "0x15626B4", VA = "0x1015626B4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004A50 RID: 19024 RVA: 0x0001AB98 File Offset: 0x00018D98
		[Token(Token = "0x6004485")]
		[Address(RVA = "0x101562754", Offset = "0x1562754", VA = "0x101562754", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004A51 RID: 19025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004486")]
		[Address(RVA = "0x101562764", Offset = "0x1562764", VA = "0x101562764")]
		public void OnClickMainButtonCallBack(int btn)
		{
		}

		// Token: 0x06004A52 RID: 19026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004487")]
		[Address(RVA = "0x101562774", Offset = "0x1562774", VA = "0x101562774")]
		public void ClearSelectButton()
		{
		}

		// Token: 0x06004A53 RID: 19027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004488")]
		[Address(RVA = "0x101562780", Offset = "0x1562780", VA = "0x101562780")]
		public ShopTopUI()
		{
		}

		// Token: 0x04005C34 RID: 23604
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40040BC")]
		private ShopTopUI.eStep m_Step;

		// Token: 0x04005C35 RID: 23605
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40040BD")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005C36 RID: 23606
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40040BE")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005C37 RID: 23607
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40040BF")]
		private ShopTopUI.eMainButton m_SelectButton;

		// Token: 0x02000F4C RID: 3916
		[Token(Token = "0x20011BC")]
		private enum eStep
		{
			// Token: 0x04005C3A RID: 23610
			[Token(Token = "0x4006EC2")]
			Hide,
			// Token: 0x04005C3B RID: 23611
			[Token(Token = "0x4006EC3")]
			In,
			// Token: 0x04005C3C RID: 23612
			[Token(Token = "0x4006EC4")]
			Idle,
			// Token: 0x04005C3D RID: 23613
			[Token(Token = "0x4006EC5")]
			Out,
			// Token: 0x04005C3E RID: 23614
			[Token(Token = "0x4006EC6")]
			End
		}

		// Token: 0x02000F4D RID: 3917
		[Token(Token = "0x20011BD")]
		public enum eMainButton
		{
			// Token: 0x04005C40 RID: 23616
			[Token(Token = "0x4006EC8")]
			None = -1,
			// Token: 0x04005C41 RID: 23617
			[Token(Token = "0x4006EC9")]
			Trade,
			// Token: 0x04005C42 RID: 23618
			[Token(Token = "0x4006ECA")]
			Weapon,
			// Token: 0x04005C43 RID: 23619
			[Token(Token = "0x4006ECB")]
			Build,
			// Token: 0x04005C44 RID: 23620
			[Token(Token = "0x4006ECC")]
			Market
		}
	}
}
