﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F4E RID: 3918
	[Token(Token = "0x2000A56")]
	[StructLayout(3)]
	public class ShopTradeConfirmGroup : UIGroup
	{
		// Token: 0x140000D6 RID: 214
		// (add) Token: 0x06004A54 RID: 19028 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A55 RID: 19029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D6")]
		public event Action<int, int> OnClickExecute
		{
			[Token(Token = "0x6004489")]
			[Address(RVA = "0x101562790", Offset = "0x1562790", VA = "0x101562790")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600448A")]
			[Address(RVA = "0x10156289C", Offset = "0x156289C", VA = "0x10156289C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000D7 RID: 215
		// (add) Token: 0x06004A56 RID: 19030 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A57 RID: 19031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D7")]
		public event Action OnClickMaterialButton
		{
			[Token(Token = "0x600448B")]
			[Address(RVA = "0x1015629A8", Offset = "0x15629A8", VA = "0x1015629A8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600448C")]
			[Address(RVA = "0x101562AB4", Offset = "0x1562AB4", VA = "0x101562AB4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A58 RID: 19032 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600448D")]
		[Address(RVA = "0x101562BC0", Offset = "0x1562BC0", VA = "0x101562BC0")]
		public ShopManager.TradeData GetCurrentTradeData()
		{
			return null;
		}

		// Token: 0x06004A59 RID: 19033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600448E")]
		[Address(RVA = "0x101562BC8", Offset = "0x1562BC8", VA = "0x101562BC8")]
		public void Setup()
		{
		}

		// Token: 0x06004A5A RID: 19034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600448F")]
		[Address(RVA = "0x101562C6C", Offset = "0x1562C6C", VA = "0x101562C6C")]
		public void Open(ShopManager.TradeData tradeData)
		{
		}

		// Token: 0x06004A5B RID: 19035 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004490")]
		[Address(RVA = "0x101563614", Offset = "0x1563614", VA = "0x101563614")]
		public void Refresh()
		{
		}

		// Token: 0x06004A5C RID: 19036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004491")]
		[Address(RVA = "0x101562DBC", Offset = "0x1562DBC", VA = "0x101562DBC")]
		private void Apply(ShopManager.TradeData tradeData)
		{
		}

		// Token: 0x06004A5D RID: 19037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004492")]
		[Address(RVA = "0x10156361C", Offset = "0x156361C", VA = "0x10156361C")]
		private void ApplyItem(int itemID, int num)
		{
		}

		// Token: 0x06004A5E RID: 19038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004493")]
		[Address(RVA = "0x101563730", Offset = "0x1563730", VA = "0x101563730")]
		private void ApplyChara(int charaID)
		{
		}

		// Token: 0x06004A5F RID: 19039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004494")]
		[Address(RVA = "0x10156383C", Offset = "0x156383C", VA = "0x10156383C")]
		private void ApplyWeapon(int weaponID, int num = -1)
		{
		}

		// Token: 0x06004A60 RID: 19040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004495")]
		[Address(RVA = "0x101563FA0", Offset = "0x1563FA0", VA = "0x101563FA0")]
		private void ApplyGold(int num)
		{
		}

		// Token: 0x06004A61 RID: 19041 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004496")]
		[Address(RVA = "0x1015640AC", Offset = "0x15640AC", VA = "0x1015640AC")]
		private void ApplyKRRPoint(int num)
		{
		}

		// Token: 0x06004A62 RID: 19042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004497")]
		[Address(RVA = "0x1015641B8", Offset = "0x15641B8", VA = "0x1015641B8")]
		private void ApplyPackage(int num)
		{
		}

		// Token: 0x06004A63 RID: 19043 RVA: 0x0001ABB0 File Offset: 0x00018DB0
		[Token(Token = "0x6004498")]
		[Address(RVA = "0x10156461C", Offset = "0x156461C", VA = "0x10156461C")]
		public int GetSelectIdx()
		{
			return 0;
		}

		// Token: 0x06004A64 RID: 19044 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004499")]
		[Address(RVA = "0x101564624", Offset = "0x1564624", VA = "0x101564624")]
		public string GetSelectingSrcName()
		{
			return null;
		}

		// Token: 0x06004A65 RID: 19045 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600449A")]
		[Address(RVA = "0x10156462C", Offset = "0x156462C", VA = "0x10156462C")]
		public string GetSrcName(int index)
		{
			return null;
		}

		// Token: 0x06004A66 RID: 19046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600449B")]
		[Address(RVA = "0x101564778", Offset = "0x1564778", VA = "0x101564778")]
		public void SetExecuteCallbackType(ShopTradeConfirmGroup.eExecuteCallbackType executeCallbackType)
		{
		}

		// Token: 0x06004A67 RID: 19047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600449C")]
		[Address(RVA = "0x101564780", Offset = "0x1564780", VA = "0x101564780")]
		public void OnClickExecuteButtonCallBack(int idx)
		{
		}

		// Token: 0x06004A68 RID: 19048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600449D")]
		[Address(RVA = "0x101564F30", Offset = "0x1564F30", VA = "0x101564F30")]
		private void OnConfirmGemIsShort(int answer)
		{
		}

		// Token: 0x06004A69 RID: 19049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600449E")]
		[Address(RVA = "0x101564E78", Offset = "0x1564E78", VA = "0x101564E78")]
		private void OnConfirmExec(int btn)
		{
		}

		// Token: 0x06004A6A RID: 19050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600449F")]
		[Address(RVA = "0x101564FAC", Offset = "0x1564FAC", VA = "0x101564FAC")]
		public ShopTradeConfirmGroup()
		{
		}

		// Token: 0x04005C45 RID: 23621
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40040C1")]
		[SerializeField]
		private ItemDetailDisplay m_ItemDetailDisp;

		// Token: 0x04005C46 RID: 23622
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40040C2")]
		[SerializeField]
		private CharaDetailDisplay m_CharaDetailDisp;

		// Token: 0x04005C47 RID: 23623
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40040C3")]
		[SerializeField]
		private PackageDetailDisplay m_PackageDetailDisp;

		// Token: 0x04005C48 RID: 23624
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40040C4")]
		[SerializeField]
		private WeaponInfo m_WeaponInfo;

		// Token: 0x04005C49 RID: 23625
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40040C5")]
		[SerializeField]
		private ShopTradeSrc[] m_Srcs;

		// Token: 0x04005C4A RID: 23626
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40040C6")]
		[SerializeField]
		private Text m_LimitText;

		// Token: 0x04005C4B RID: 23627
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40040C7")]
		[SerializeField]
		private Text m_DeadLineText;

		// Token: 0x04005C4C RID: 23628
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40040C8")]
		[SerializeField]
		private Text m_WeaponHaveNumText;

		// Token: 0x04005C4D RID: 23629
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40040C9")]
		[SerializeField]
		private Text m_ItemHaveNumText;

		// Token: 0x04005C4E RID: 23630
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40040CA")]
		[SerializeField]
		private FixScrollRect[] m_DetailScrolls;

		// Token: 0x04005C4F RID: 23631
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40040CB")]
		private ShopManager.TradeData m_TradeData;

		// Token: 0x04005C50 RID: 23632
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40040CC")]
		private ShopTradeConfirmGroup.eExecuteCallbackType m_ExecuteCallbackType;

		// Token: 0x04005C51 RID: 23633
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x40040CD")]
		private int m_SelectIdx;

		// Token: 0x04005C52 RID: 23634
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40040CE")]
		private ShopTradeConfirmGroup.eReasonDisable m_ReasonTrade;

		// Token: 0x02000F4F RID: 3919
		[Token(Token = "0x20011BE")]
		public enum eExecuteCallbackType
		{
			// Token: 0x04005C56 RID: 23638
			[Token(Token = "0x4006ECE")]
			Error,
			// Token: 0x04005C57 RID: 23639
			[Token(Token = "0x4006ECF")]
			Execute,
			// Token: 0x04005C58 RID: 23640
			[Token(Token = "0x4006ED0")]
			Decide
		}

		// Token: 0x02000F50 RID: 3920
		[Token(Token = "0x20011BF")]
		private enum eReasonDisable
		{
			// Token: 0x04005C5A RID: 23642
			[Token(Token = "0x4006ED2")]
			None,
			// Token: 0x04005C5B RID: 23643
			[Token(Token = "0x4006ED3")]
			WeaponLimit,
			// Token: 0x04005C5C RID: 23644
			[Token(Token = "0x4006ED4")]
			KRRPointLimit
		}
	}
}
