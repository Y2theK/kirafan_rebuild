﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F51 RID: 3921
	[Token(Token = "0x2000A57")]
	[StructLayout(3)]
	public class ShopTradeListItem : ScrollItemIcon
	{
		// Token: 0x06004A6B RID: 19051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044A0")]
		[Address(RVA = "0x101564FBC", Offset = "0x1564FBC", VA = "0x101564FBC", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06004A6C RID: 19052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044A1")]
		[Address(RVA = "0x101565018", Offset = "0x1565018", VA = "0x101565018", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004A6D RID: 19053 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044A2")]
		[Address(RVA = "0x101565F74", Offset = "0x1565F74", VA = "0x101565F74")]
		public ShopTradeListItem()
		{
		}

		// Token: 0x04005C5D RID: 23645
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40040D1")]
		[SerializeField]
		private PrefabCloner m_DstVariableIconWithFrameCloner;

		// Token: 0x04005C5E RID: 23646
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40040D2")]
		[SerializeField]
		private ShopItemLabel m_ImgLabel;

		// Token: 0x04005C5F RID: 23647
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40040D3")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005C60 RID: 23648
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40040D4")]
		[SerializeField]
		private Text m_LimitText;

		// Token: 0x04005C61 RID: 23649
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40040D5")]
		[SerializeField]
		private Text m_DeadlineText;

		// Token: 0x04005C62 RID: 23650
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40040D6")]
		[SerializeField]
		private GameObject[] m_SrcObj;

		// Token: 0x04005C63 RID: 23651
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40040D7")]
		[SerializeField]
		private PrefabCloner[] m_SrcVariableIconCloners;

		// Token: 0x04005C64 RID: 23652
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40040D8")]
		[SerializeField]
		private Text[] m_SrcAmountTexts;

		// Token: 0x04005C65 RID: 23653
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40040D9")]
		[SerializeField]
		private GameObject m_HaveNumTextObj;

		// Token: 0x04005C66 RID: 23654
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40040DA")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04005C67 RID: 23655
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40040DB")]
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04005C68 RID: 23656
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40040DC")]
		[SerializeField]
		private ColorGroup m_CantExecCG;

		// Token: 0x04005C69 RID: 23657
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40040DD")]
		private bool m_CantExec;

		// Token: 0x04005C6A RID: 23658
		[Cpp2IlInjected.FieldOffset(Offset = "0xBC")]
		[Token(Token = "0x40040DE")]
		private Vector2 m_nameSize;
	}
}
