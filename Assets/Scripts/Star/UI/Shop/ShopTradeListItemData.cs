﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F52 RID: 3922
	[Token(Token = "0x2000A58")]
	[StructLayout(3)]
	public class ShopTradeListItemData : ScrollItemData
	{
		// Token: 0x140000D8 RID: 216
		// (add) Token: 0x06004A6E RID: 19054 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A6F RID: 19055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D8")]
		public event Action<ShopManager.TradeData> OnClick
		{
			[Token(Token = "0x60044A3")]
			[Address(RVA = "0x101565F7C", Offset = "0x1565F7C", VA = "0x101565F7C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044A4")]
			[Address(RVA = "0x101566088", Offset = "0x1566088", VA = "0x101566088")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A70 RID: 19056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044A5")]
		[Address(RVA = "0x101566194", Offset = "0x1566194", VA = "0x101566194")]
		public ShopTradeListItemData(ShopManager.TradeData tradeData)
		{
		}

		// Token: 0x06004A71 RID: 19057 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044A6")]
		[Address(RVA = "0x101566268", Offset = "0x1566268", VA = "0x101566268")]
		[Obsolete]
		public ShopManager.TradeData GetData()
		{
			return null;
		}

		// Token: 0x06004A72 RID: 19058 RVA: 0x0001ABC8 File Offset: 0x00018DC8
		[Token(Token = "0x60044A7")]
		[Address(RVA = "0x101565DA4", Offset = "0x1565DA4", VA = "0x101565DA4")]
		public ePresentType GetDstType()
		{
			return ePresentType.None;
		}

		// Token: 0x06004A73 RID: 19059 RVA: 0x0001ABE0 File Offset: 0x00018DE0
		[Token(Token = "0x60044A8")]
		[Address(RVA = "0x101565D78", Offset = "0x1565D78", VA = "0x101565D78")]
		public int GetDstID()
		{
			return 0;
		}

		// Token: 0x06004A74 RID: 19060 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044A9")]
		[Address(RVA = "0x101566270", Offset = "0x1566270", VA = "0x101566270")]
		public ShopManager.TradeSrcData[] GetSrcDatas()
		{
			return null;
		}

		// Token: 0x06004A75 RID: 19061 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044AA")]
		[Address(RVA = "0x101565EFC", Offset = "0x1565EFC", VA = "0x101565EFC")]
		public ShopManager.TradeSrcData GetSrcData(int index)
		{
			return null;
		}

		// Token: 0x06004A76 RID: 19062 RVA: 0x0001ABF8 File Offset: 0x00018DF8
		[Token(Token = "0x60044AB")]
		[Address(RVA = "0x101565EC0", Offset = "0x1565EC0", VA = "0x101565EC0")]
		public int HowManySrcDatas()
		{
			return 0;
		}

		// Token: 0x06004A77 RID: 19063 RVA: 0x0001AC10 File Offset: 0x00018E10
		[Token(Token = "0x60044AC")]
		[Address(RVA = "0x101566288", Offset = "0x1566288", VA = "0x101566288")]
		public ePresentType GetSrcType(int index)
		{
			return ePresentType.None;
		}

		// Token: 0x06004A78 RID: 19064 RVA: 0x0001AC28 File Offset: 0x00018E28
		[Token(Token = "0x60044AD")]
		[Address(RVA = "0x1015662A4", Offset = "0x15662A4", VA = "0x1015662A4")]
		public int GetSrcID(int index)
		{
			return 0;
		}

		// Token: 0x06004A79 RID: 19065 RVA: 0x0001AC40 File Offset: 0x00018E40
		[Token(Token = "0x60044AE")]
		[Address(RVA = "0x1015662C8", Offset = "0x15662C8", VA = "0x1015662C8")]
		public int GetSrcAmount(int index)
		{
			return 0;
		}

		// Token: 0x06004A7A RID: 19066 RVA: 0x0001AC58 File Offset: 0x00018E58
		[Token(Token = "0x60044AF")]
		[Address(RVA = "0x101565E10", Offset = "0x1565E10", VA = "0x101565E10")]
		public int GetLimitCount()
		{
			return 0;
		}

		// Token: 0x06004A7B RID: 19067 RVA: 0x0001AC70 File Offset: 0x00018E70
		[Token(Token = "0x60044B0")]
		[Address(RVA = "0x1015662EC", Offset = "0x15662EC", VA = "0x1015662EC")]
		public int GetTotalTradeCount()
		{
			return 0;
		}

		// Token: 0x06004A7C RID: 19068 RVA: 0x0001AC88 File Offset: 0x00018E88
		[Token(Token = "0x60044B1")]
		[Address(RVA = "0x101565DD0", Offset = "0x1565DD0", VA = "0x101565DD0")]
		public int GetLabelID()
		{
			return 0;
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06004A7D RID: 19069 RVA: 0x0001ACA0 File Offset: 0x00018EA0
		[Token(Token = "0x170004DC")]
		public int RemainNum
		{
			[Token(Token = "0x60044B2")]
			[Address(RVA = "0x101565E28", Offset = "0x1565E28", VA = "0x101565E28")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06004A7E RID: 19070 RVA: 0x0001ACB8 File Offset: 0x00018EB8
		[Token(Token = "0x170004DD")]
		public TimeSpan? DeadLineSpan
		{
			[Token(Token = "0x60044B3")]
			[Address(RVA = "0x101565E40", Offset = "0x1565E40", VA = "0x101565E40")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004A7F RID: 19071 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044B4")]
		[Address(RVA = "0x101566304", Offset = "0x1566304", VA = "0x101566304")]
		[Obsolete]
		public string GetSrcAmountString(int index)
		{
			return null;
		}

		// Token: 0x06004A80 RID: 19072 RVA: 0x0001ACD0 File Offset: 0x00018ED0
		[Token(Token = "0x60044B5")]
		[Address(RVA = "0x101565DFC", Offset = "0x1565DFC", VA = "0x101565DFC")]
		public bool IsAvailable()
		{
			return default(bool);
		}

		// Token: 0x06004A81 RID: 19073 RVA: 0x0001ACE8 File Offset: 0x00018EE8
		[Token(Token = "0x60044B6")]
		[Address(RVA = "0x101565ED8", Offset = "0x1565ED8", VA = "0x101565ED8")]
		public bool IsActiveSrc(int index)
		{
			return default(bool);
		}

		// Token: 0x06004A82 RID: 19074 RVA: 0x0001AD00 File Offset: 0x00018F00
		[Token(Token = "0x60044B7")]
		[Address(RVA = "0x101566388", Offset = "0x1566388", VA = "0x101566388")]
		public bool IsEnoughSrc(int index)
		{
			return default(bool);
		}

		// Token: 0x06004A83 RID: 19075 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044B8")]
		[Address(RVA = "0x1015663AC", Offset = "0x15663AC", VA = "0x1015663AC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005C6B RID: 23659
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40040DF")]
		private ShopManager.TradeData m_TradeData;

		// Token: 0x04005C6C RID: 23660
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40040E0")]
		public string m_TradeName;
	}
}
