﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F53 RID: 3923
	[Token(Token = "0x2000A59")]
	[StructLayout(3)]
	public class ShopTradeListUI : MenuUIBase
	{
		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06004A84 RID: 19076 RVA: 0x0001AD18 File Offset: 0x00018F18
		[Token(Token = "0x170004DE")]
		public ShopTradeListUI.eButton SelectButton
		{
			[Token(Token = "0x60044B9")]
			[Address(RVA = "0x101566400", Offset = "0x1566400", VA = "0x101566400")]
			get
			{
				return ShopTradeListUI.eButton.Chest;
			}
		}

		// Token: 0x140000D9 RID: 217
		// (add) Token: 0x06004A85 RID: 19077 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A86 RID: 19078 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000D9")]
		public event Action<int, int, int> OnClickExecuteTradeButton
		{
			[Token(Token = "0x60044BA")]
			[Address(RVA = "0x101566408", Offset = "0x1566408", VA = "0x101566408")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044BB")]
			[Address(RVA = "0x101566518", Offset = "0x1566518", VA = "0x101566518")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000DA RID: 218
		// (add) Token: 0x06004A87 RID: 19079 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A88 RID: 19080 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DA")]
		public event Action OnClickChestButton
		{
			[Token(Token = "0x60044BC")]
			[Address(RVA = "0x101566628", Offset = "0x1566628", VA = "0x101566628")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044BD")]
			[Address(RVA = "0x101566738", Offset = "0x1566738", VA = "0x101566738")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000DB RID: 219
		// (add) Token: 0x06004A89 RID: 19081 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004A8A RID: 19082 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DB")]
		public event Action OnClickQuestButton
		{
			[Token(Token = "0x60044BE")]
			[Address(RVA = "0x101566848", Offset = "0x1566848", VA = "0x101566848")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044BF")]
			[Address(RVA = "0x101566958", Offset = "0x1566958", VA = "0x101566958")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004A8B RID: 19083 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C0")]
		[Address(RVA = "0x101566A68", Offset = "0x1566A68", VA = "0x101566A68")]
		private void Start()
		{
		}

		// Token: 0x06004A8C RID: 19084 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C1")]
		[Address(RVA = "0x101566A6C", Offset = "0x1566A6C", VA = "0x101566A6C")]
		public void Setup(List<ShopManager.TradeData> tradeList)
		{
		}

		// Token: 0x06004A8D RID: 19085 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C2")]
		[Address(RVA = "0x1015679D8", Offset = "0x15679D8", VA = "0x1015679D8")]
		public void SetEnableQuestButton(bool flg)
		{
		}

		// Token: 0x06004A8E RID: 19086 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C3")]
		[Address(RVA = "0x101567A28", Offset = "0x1567A28", VA = "0x101567A28")]
		public void SetEnableChestButton(bool flg)
		{
		}

		// Token: 0x06004A8F RID: 19087 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C4")]
		[Address(RVA = "0x101567A78", Offset = "0x1567A78", VA = "0x101567A78")]
		public void SetChestBadgeValue(int value)
		{
		}

		// Token: 0x06004A90 RID: 19088 RVA: 0x0001AD30 File Offset: 0x00018F30
		[Token(Token = "0x60044C5")]
		[Address(RVA = "0x101567AB0", Offset = "0x1567AB0", VA = "0x101567AB0")]
		private ShopTradeListUI.eTab GetTabFromTradeData(ShopManager.TradeData tradeData)
		{
			return ShopTradeListUI.eTab.Feature;
		}

		// Token: 0x06004A91 RID: 19089 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C6")]
		[Address(RVA = "0x101566FBC", Offset = "0x1566FBC", VA = "0x101566FBC")]
		private void SetupTab()
		{
		}

		// Token: 0x06004A92 RID: 19090 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C7")]
		[Address(RVA = "0x101567C84", Offset = "0x1567C84", VA = "0x101567C84")]
		private void ClearHaveDispData()
		{
		}

		// Token: 0x06004A93 RID: 19091 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C8")]
		[Address(RVA = "0x101567CF8", Offset = "0x1567CF8", VA = "0x101567CF8")]
		private void SetupHaveItemDispList(List<ShopManager.TradeData> tradeList, ShopTradeListUI.eTab tab)
		{
		}

		// Token: 0x06004A94 RID: 19092 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044C9")]
		[Address(RVA = "0x101567F90", Offset = "0x1567F90", VA = "0x101567F90")]
		private void ApplyHaveItemDisp(int pageIdx)
		{
		}

		// Token: 0x06004A95 RID: 19093 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CA")]
		[Address(RVA = "0x10156852C", Offset = "0x156852C", VA = "0x10156852C")]
		public void Refresh(List<ShopManager.TradeData> tradeList)
		{
		}

		// Token: 0x06004A96 RID: 19094 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CB")]
		[Address(RVA = "0x1015677A8", Offset = "0x15677A8", VA = "0x1015677A8")]
		private void ApplyTab(ShopTradeListUI.eTab tab)
		{
		}

		// Token: 0x06004A97 RID: 19095 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CC")]
		[Address(RVA = "0x101568798", Offset = "0x1568798", VA = "0x101568798")]
		private void OnChangeTabCallBack(int idx)
		{
		}

		// Token: 0x06004A98 RID: 19096 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CD")]
		[Address(RVA = "0x10156879C", Offset = "0x156879C", VA = "0x10156879C")]
		private void Update()
		{
		}

		// Token: 0x06004A99 RID: 19097 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CE")]
		[Address(RVA = "0x101568BD0", Offset = "0x1568BD0", VA = "0x101568BD0")]
		private void ChangeStep(ShopTradeListUI.eStep step)
		{
		}

		// Token: 0x06004A9A RID: 19098 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044CF")]
		[Address(RVA = "0x101568E54", Offset = "0x1568E54", VA = "0x101568E54", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004A9B RID: 19099 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D0")]
		[Address(RVA = "0x101568EF4", Offset = "0x1568EF4", VA = "0x101568EF4", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004A9C RID: 19100 RVA: 0x0001AD48 File Offset: 0x00018F48
		[Token(Token = "0x60044D1")]
		[Address(RVA = "0x101568F94", Offset = "0x1568F94", VA = "0x101568F94")]
		private int GetTradeNum()
		{
			return 0;
		}

		// Token: 0x06004A9D RID: 19101 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044D2")]
		[Address(RVA = "0x1015690CC", Offset = "0x15690CC", VA = "0x1015690CC")]
		public string GetSelectingSrcName()
		{
			return null;
		}

		// Token: 0x06004A9E RID: 19102 RVA: 0x0001AD60 File Offset: 0x00018F60
		[Token(Token = "0x60044D3")]
		[Address(RVA = "0x1015690FC", Offset = "0x15690FC", VA = "0x1015690FC", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004A9F RID: 19103 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D4")]
		[Address(RVA = "0x10156910C", Offset = "0x156910C", VA = "0x10156910C")]
		public void OnClickRecipeCallBack(ShopManager.TradeData tradeData)
		{
		}

		// Token: 0x06004AA0 RID: 19104 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D5")]
		[Address(RVA = "0x10156928C", Offset = "0x156928C", VA = "0x10156928C")]
		public void OnClickMaterialSelectMaterialButton()
		{
		}

		// Token: 0x06004AA1 RID: 19105 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D6")]
		[Address(RVA = "0x101569C5C", Offset = "0x1569C5C", VA = "0x101569C5C")]
		public void OnClickExecuteButton()
		{
		}

		// Token: 0x06004AA2 RID: 19106 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D7")]
		[Address(RVA = "0x101569CC0", Offset = "0x1569CC0", VA = "0x101569CC0")]
		public void OnClickExecuteButton(int recipeId, int idx)
		{
		}

		// Token: 0x06004AA3 RID: 19107 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D8")]
		[Address(RVA = "0x101569D38", Offset = "0x1569D38", VA = "0x101569D38")]
		public void OnBackConfirmCallBack(bool shortcut)
		{
		}

		// Token: 0x06004AA4 RID: 19108 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044D9")]
		[Address(RVA = "0x101568614", Offset = "0x1568614", VA = "0x101568614")]
		public void CloseConfirm()
		{
		}

		// Token: 0x06004AA5 RID: 19109 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044DA")]
		[Address(RVA = "0x101569E48", Offset = "0x1569E48", VA = "0x101569E48")]
		public void OnClickChestButtonCallBack()
		{
		}

		// Token: 0x06004AA6 RID: 19110 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044DB")]
		[Address(RVA = "0x101569E58", Offset = "0x1569E58", VA = "0x101569E58")]
		public void OnClickQuestButtonCallBack()
		{
		}

		// Token: 0x06004AA7 RID: 19111 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60044DC")]
		[Address(RVA = "0x101569E6C", Offset = "0x1569E6C", VA = "0x101569E6C")]
		public ShopManager.TradeData GetCurentTradeData()
		{
			return null;
		}

		// Token: 0x06004AA8 RID: 19112 RVA: 0x0001AD78 File Offset: 0x00018F78
		[Token(Token = "0x60044DD")]
		[Address(RVA = "0x101569E98", Offset = "0x1569E98", VA = "0x101569E98")]
		public bool isSendToPresentAfterTrade()
		{
			return default(bool);
		}

		// Token: 0x06004AA9 RID: 19113 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044DE")]
		[Address(RVA = "0x101569EF4", Offset = "0x1569EF4", VA = "0x101569EF4")]
		public ShopTradeListUI()
		{
		}

		// Token: 0x04005C6E RID: 23662
		[Token(Token = "0x40040E2")]
		private const float DISPHAVEITEM_DISPSEC = 3f;

		// Token: 0x04005C6F RID: 23663
		[Token(Token = "0x40040E3")]
		private const float DISPHAVEITEM_FADESEC = 3f;

		// Token: 0x04005C70 RID: 23664
		[Token(Token = "0x40040E4")]
		private const int DISPHAVEITEM_ROWMAX = 5;

		// Token: 0x04005C71 RID: 23665
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40040E5")]
		private ShopTradeListUI.eStep m_Step;

		// Token: 0x04005C72 RID: 23666
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40040E6")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005C73 RID: 23667
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40040E7")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005C74 RID: 23668
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40040E8")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04005C75 RID: 23669
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40040E9")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005C76 RID: 23670
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40040EA")]
		[SerializeField]
		private ShopTradeConfirmGroup m_ConfirmGroup;

		// Token: 0x04005C77 RID: 23671
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40040EB")]
		[SerializeField]
		private ShopTradeNumSelectGroup m_TradeNumSelectGroup;

		// Token: 0x04005C78 RID: 23672
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40040EC")]
		[SerializeField]
		private CanvasGroup m_HaveItemCanvasGroup;

		// Token: 0x04005C79 RID: 23673
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40040ED")]
		[SerializeField]
		private GameObject m_HaveItemParent;

		// Token: 0x04005C7A RID: 23674
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40040EE")]
		[SerializeField]
		private GameObject[] m_HaveItems;

		// Token: 0x04005C7B RID: 23675
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40040EF")]
		private ItemIcon[] m_HaveIcons;

		// Token: 0x04005C7C RID: 23676
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40040F0")]
		private Text[] m_HaveItemNumText;

		// Token: 0x04005C7D RID: 23677
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40040F1")]
		[SerializeField]
		private Text m_KPText;

		// Token: 0x04005C7E RID: 23678
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40040F2")]
		[SerializeField]
		private CustomButton m_QuestButton;

		// Token: 0x04005C7F RID: 23679
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40040F3")]
		[SerializeField]
		private CustomButton m_ChestButton;

		// Token: 0x04005C80 RID: 23680
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40040F4")]
		[SerializeField]
		private ChestBadge m_ChestBadge;

		// Token: 0x04005C81 RID: 23681
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40040F5")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04005C82 RID: 23682
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40040F6")]
		private List<int> m_DispHaveItemIDList;

		// Token: 0x04005C83 RID: 23683
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40040F7")]
		private bool m_EnableDispHaveKP;

		// Token: 0x04005C84 RID: 23684
		[Cpp2IlInjected.FieldOffset(Offset = "0xD4")]
		[Token(Token = "0x40040F8")]
		private int m_HaveItemCurrentIdx;

		// Token: 0x04005C85 RID: 23685
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40040F9")]
		private ShopTradeListUI.eDispState m_HaveItemDispState;

		// Token: 0x04005C86 RID: 23686
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x40040FA")]
		private float m_HaveItemDispStateTime;

		// Token: 0x04005C87 RID: 23687
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40040FB")]
		private List<ShopTradeListUI.eTab> m_TabList;

		// Token: 0x04005C88 RID: 23688
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40040FC")]
		private ShopTradeListUI.eTab m_Tab;

		// Token: 0x04005C89 RID: 23689
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40040FD")]
		private List<ShopManager.TradeData> m_TradeList;

		// Token: 0x04005C8A RID: 23690
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40040FE")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x04005C8B RID: 23691
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40040FF")]
		private ShopTradeListUI.eButton m_SelectButton;

		// Token: 0x02000F54 RID: 3924
		[Token(Token = "0x20011C0")]
		private enum eStep
		{
			// Token: 0x04005C90 RID: 23696
			[Token(Token = "0x4006ED6")]
			Hide,
			// Token: 0x04005C91 RID: 23697
			[Token(Token = "0x4006ED7")]
			In,
			// Token: 0x04005C92 RID: 23698
			[Token(Token = "0x4006ED8")]
			Idle,
			// Token: 0x04005C93 RID: 23699
			[Token(Token = "0x4006ED9")]
			Out,
			// Token: 0x04005C94 RID: 23700
			[Token(Token = "0x4006EDA")]
			End
		}

		// Token: 0x02000F55 RID: 3925
		[Token(Token = "0x20011C1")]
		public enum eDispState
		{
			// Token: 0x04005C96 RID: 23702
			[Token(Token = "0x4006EDC")]
			None,
			// Token: 0x04005C97 RID: 23703
			[Token(Token = "0x4006EDD")]
			In,
			// Token: 0x04005C98 RID: 23704
			[Token(Token = "0x4006EDE")]
			Idle,
			// Token: 0x04005C99 RID: 23705
			[Token(Token = "0x4006EDF")]
			Out
		}

		// Token: 0x02000F56 RID: 3926
		[Token(Token = "0x20011C2")]
		public enum eTab
		{
			// Token: 0x04005C9B RID: 23707
			[Token(Token = "0x4006EE1")]
			Feature,
			// Token: 0x04005C9C RID: 23708
			[Token(Token = "0x4006EE2")]
			Summon,
			// Token: 0x04005C9D RID: 23709
			[Token(Token = "0x4006EE3")]
			Upgrade,
			// Token: 0x04005C9E RID: 23710
			[Token(Token = "0x4006EE4")]
			LimitBreak,
			// Token: 0x04005C9F RID: 23711
			[Token(Token = "0x4006EE5")]
			Evolution,
			// Token: 0x04005CA0 RID: 23712
			[Token(Token = "0x4006EE6")]
			Weapon,
			// Token: 0x04005CA1 RID: 23713
			[Token(Token = "0x4006EE7")]
			Etc,
			// Token: 0x04005CA2 RID: 23714
			[Token(Token = "0x4006EE8")]
			Num
		}

		// Token: 0x02000F57 RID: 3927
		[Token(Token = "0x20011C3")]
		public enum eButton
		{
			// Token: 0x04005CA4 RID: 23716
			[Token(Token = "0x4006EEA")]
			None = -1,
			// Token: 0x04005CA5 RID: 23717
			[Token(Token = "0x4006EEB")]
			Chest,
			// Token: 0x04005CA6 RID: 23718
			[Token(Token = "0x4006EEC")]
			Quest
		}
	}
}
