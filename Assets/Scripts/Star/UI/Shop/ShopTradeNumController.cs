﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F58 RID: 3928
	[Token(Token = "0x2000A5A")]
	[StructLayout(3)]
	public class ShopTradeNumController : MonoBehaviour
	{
		// Token: 0x06004AAA RID: 19114 RVA: 0x0001AD90 File Offset: 0x00018F90
		[Token(Token = "0x60044DF")]
		[Address(RVA = "0x101563954", Offset = "0x1563954", VA = "0x101563954")]
		public static int HowManyTimesCanYouTrade(ShopManager.TradeData tradeData, int srcIndex = -1)
		{
			return 0;
		}

		// Token: 0x06004AAB RID: 19115 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E0")]
		[Address(RVA = "0x101569F9C", Offset = "0x1569F9C", VA = "0x101569F9C")]
		public void Setup()
		{
		}

		// Token: 0x06004AAC RID: 19116 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E1")]
		[Address(RVA = "0x10156A0BC", Offset = "0x156A0BC", VA = "0x10156A0BC")]
		public void Apply(ShopManager.TradeData tradeData, int srcIndex)
		{
		}

		// Token: 0x06004AAD RID: 19117 RVA: 0x0001ADA8 File Offset: 0x00018FA8
		[Token(Token = "0x60044E2")]
		[Address(RVA = "0x10156A150", Offset = "0x156A150", VA = "0x10156A150")]
		public int GetTradeNum()
		{
			return 0;
		}

		// Token: 0x06004AAE RID: 19118 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E3")]
		[Address(RVA = "0x10156A1F0", Offset = "0x156A1F0", VA = "0x10156A1F0")]
		public ShopTradeNumController()
		{
		}

		// Token: 0x04005CA7 RID: 23719
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004103")]
		[SerializeField]
		private PrefabCloner m_NumberSelectCloner;

		// Token: 0x04005CA8 RID: 23720
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004104")]
		private RangeSelector m_NumberSelect;
	}
}
