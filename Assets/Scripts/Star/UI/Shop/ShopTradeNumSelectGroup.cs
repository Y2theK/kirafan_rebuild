﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F59 RID: 3929
	[Token(Token = "0x2000A5B")]
	[StructLayout(3)]
	public class ShopTradeNumSelectGroup : UIGroup
	{
		// Token: 0x140000DC RID: 220
		// (add) Token: 0x06004AAF RID: 19119 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004AB0 RID: 19120 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DC")]
		public event Action evOnClickExecute
		{
			[Token(Token = "0x60044E4")]
			[Address(RVA = "0x10156769C", Offset = "0x156769C", VA = "0x10156769C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044E5")]
			[Address(RVA = "0x10156A1F8", Offset = "0x156A1F8", VA = "0x10156A1F8")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004AB1 RID: 19121 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E6")]
		[Address(RVA = "0x101567938", Offset = "0x1567938", VA = "0x101567938")]
		public void SetParent(ShopTradeListUI parent)
		{
		}

		// Token: 0x06004AB2 RID: 19122 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E7")]
		[Address(RVA = "0x101567940", Offset = "0x1567940", VA = "0x101567940")]
		public void Setup()
		{
		}

		// Token: 0x06004AB3 RID: 19123 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E8")]
		[Address(RVA = "0x10156937C", Offset = "0x156937C", VA = "0x10156937C")]
		public void Apply(ShopManager.TradeData tradeData, int index)
		{
		}

		// Token: 0x06004AB4 RID: 19124 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044E9")]
		[Address(RVA = "0x10156A304", Offset = "0x156A304", VA = "0x10156A304")]
		private void ApplyItem(int itemID, int num)
		{
		}

		// Token: 0x06004AB5 RID: 19125 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044EA")]
		[Address(RVA = "0x10156A4A8", Offset = "0x156A4A8", VA = "0x10156A4A8")]
		private void ApplyChara(int charaID)
		{
		}

		// Token: 0x06004AB6 RID: 19126 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044EB")]
		[Address(RVA = "0x10156A638", Offset = "0x156A638", VA = "0x10156A638")]
		private void ApplyWeapon(int weaponID, int num = -1)
		{
		}

		// Token: 0x06004AB7 RID: 19127 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044EC")]
		[Address(RVA = "0x10156A7E0", Offset = "0x156A7E0", VA = "0x10156A7E0")]
		private void ApplyGold(int num)
		{
		}

		// Token: 0x06004AB8 RID: 19128 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044ED")]
		[Address(RVA = "0x10156A970", Offset = "0x156A970", VA = "0x10156A970")]
		private void ApplyKRPoint(int num)
		{
		}

		// Token: 0x06004AB9 RID: 19129 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044EE")]
		[Address(RVA = "0x10156AB00", Offset = "0x156AB00", VA = "0x10156AB00")]
		private void ApplyPackage(int packageID, int num)
		{
		}

		// Token: 0x06004ABA RID: 19130 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044EF")]
		[Address(RVA = "0x10156ACA4", Offset = "0x156ACA4", VA = "0x10156ACA4", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004ABB RID: 19131 RVA: 0x0001ADC0 File Offset: 0x00018FC0
		[Token(Token = "0x60044F0")]
		[Address(RVA = "0x101569030", Offset = "0x1569030", VA = "0x101569030")]
		public int GetNum()
		{
			return 0;
		}

		// Token: 0x06004ABC RID: 19132 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044F1")]
		[Address(RVA = "0x10156B0CC", Offset = "0x156B0CC", VA = "0x10156B0CC")]
		public void OnClickExecute()
		{
		}

		// Token: 0x06004ABD RID: 19133 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044F2")]
		[Address(RVA = "0x10156BC34", Offset = "0x156BC34", VA = "0x10156BC34")]
		private void OnConfirmExec(int btn)
		{
		}

		// Token: 0x06004ABE RID: 19134 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044F3")]
		[Address(RVA = "0x10156BC48", Offset = "0x156BC48", VA = "0x10156BC48")]
		public ShopTradeNumSelectGroup()
		{
		}

		// Token: 0x04005CA9 RID: 23721
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004105")]
		[SerializeField]
		private ItemDetailDisplay m_ItemDetailDisp;

		// Token: 0x04005CAA RID: 23722
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004106")]
		[SerializeField]
		private CharaDetailDisplay m_CharaDetailDisp;

		// Token: 0x04005CAB RID: 23723
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004107")]
		[SerializeField]
		private WeaponInfo m_WeaponInfo;

		// Token: 0x04005CAC RID: 23724
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004108")]
		[SerializeField]
		private ShopTradeSrc m_Src;

		// Token: 0x04005CAD RID: 23725
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004109")]
		[SerializeField]
		private Text m_WeaponHaveNumText;

		// Token: 0x04005CAE RID: 23726
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400410A")]
		[SerializeField]
		private Text m_ItemHaveNumText;

		// Token: 0x04005CAF RID: 23727
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400410B")]
		[SerializeField]
		private Text m_SrcHaveNumSrc;

		// Token: 0x04005CB0 RID: 23728
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400410C")]
		[SerializeField]
		private Text m_SelectingNumText;

		// Token: 0x04005CB1 RID: 23729
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400410D")]
		[SerializeField]
		private ShopTradeNumController m_TradeNumController;

		// Token: 0x04005CB2 RID: 23730
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x400410E")]
		private ShopTradeListUI m_Parent;

		// Token: 0x04005CB4 RID: 23732
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004110")]
		private ePresentType m_FixedDstType;

		// Token: 0x04005CB5 RID: 23733
		[Cpp2IlInjected.FieldOffset(Offset = "0xE4")]
		[Token(Token = "0x4004111")]
		private int m_FixedDstID;

		// Token: 0x04005CB6 RID: 23734
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004112")]
		private int m_FixedDstAmount;

		// Token: 0x04005CB7 RID: 23735
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004113")]
		private ShopManager.TradeData m_TradeData;

		// Token: 0x04005CB8 RID: 23736
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004114")]
		private int m_index;
	}
}
