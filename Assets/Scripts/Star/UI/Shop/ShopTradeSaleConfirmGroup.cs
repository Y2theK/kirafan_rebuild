﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F5A RID: 3930
	[Token(Token = "0x2000A5C")]
	[StructLayout(3)]
	public class ShopTradeSaleConfirmGroup : UIGroup
	{
		// Token: 0x140000DD RID: 221
		// (add) Token: 0x06004ABF RID: 19135 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004AC0 RID: 19136 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DD")]
		public event Action<int, int> OnExecuteSaleButton
		{
			[Token(Token = "0x60044F4")]
			[Address(RVA = "0x10156BC5C", Offset = "0x156BC5C", VA = "0x10156BC5C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044F5")]
			[Address(RVA = "0x10156BD68", Offset = "0x156BD68", VA = "0x10156BD68")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000DE RID: 222
		// (add) Token: 0x06004AC1 RID: 19137 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004AC2 RID: 19138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DE")]
		public event Action OnCancelSaleButton
		{
			[Token(Token = "0x60044F6")]
			[Address(RVA = "0x10156BE74", Offset = "0x156BE74", VA = "0x10156BE74")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60044F7")]
			[Address(RVA = "0x10156BF80", Offset = "0x156BF80", VA = "0x10156BF80")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004AC3 RID: 19139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044F8")]
		[Address(RVA = "0x10156C08C", Offset = "0x156C08C", VA = "0x10156C08C")]
		public void Open(int itemID)
		{
		}

		// Token: 0x06004AC4 RID: 19140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044F9")]
		[Address(RVA = "0x10156C54C", Offset = "0x156C54C", VA = "0x10156C54C")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x06004AC5 RID: 19141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FA")]
		[Address(RVA = "0x10156C5D0", Offset = "0x156C5D0", VA = "0x10156C5D0")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x06004AC6 RID: 19142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FB")]
		[Address(RVA = "0x10156C3B8", Offset = "0x156C3B8", VA = "0x10156C3B8")]
		private void OnChangeValueCallBack()
		{
		}

		// Token: 0x06004AC7 RID: 19143 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FC")]
		[Address(RVA = "0x10156C768", Offset = "0x156C768", VA = "0x10156C768")]
		public ShopTradeSaleConfirmGroup()
		{
		}

		// Token: 0x04005CB9 RID: 23737
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004115")]
		[SerializeField]
		private NumberSelect m_NumSelect;

		// Token: 0x04005CBA RID: 23738
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004116")]
		[SerializeField]
		private ShopTradeSaleListItem m_Item;

		// Token: 0x04005CBB RID: 23739
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004117")]
		[SerializeField]
		private Text m_BeforeGold;

		// Token: 0x04005CBC RID: 23740
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004118")]
		[SerializeField]
		private Text m_AfterGold;

		// Token: 0x04005CBF RID: 23743
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400411B")]
		private int m_ItemID;

		// Token: 0x04005CC0 RID: 23744
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400411C")]
		private ShopTradeSaleListItemData m_Data;

		// Token: 0x04005CC1 RID: 23745
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400411D")]
		private bool m_IsDonePrepare;
	}
}
