﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F5B RID: 3931
	[Token(Token = "0x2000A5D")]
	[StructLayout(3)]
	public class ShopTradeSaleListItem : ScrollItemIcon
	{
		// Token: 0x06004AC8 RID: 19144 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FD")]
		[Address(RVA = "0x10156C778", Offset = "0x156C778", VA = "0x10156C778", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004AC9 RID: 19145 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FE")]
		[Address(RVA = "0x10156C5DC", Offset = "0x156C5DC", VA = "0x10156C5DC")]
		public void ChangeHaveNum(int useNum)
		{
		}

		// Token: 0x06004ACA RID: 19146 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60044FF")]
		[Address(RVA = "0x10156C970", Offset = "0x156C970", VA = "0x10156C970")]
		public ShopTradeSaleListItem()
		{
		}

		// Token: 0x04005CC2 RID: 23746
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400411E")]
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04005CC3 RID: 23747
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400411F")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005CC4 RID: 23748
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004120")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04005CC5 RID: 23749
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004121")]
		[SerializeField]
		private Text m_SaleAmountText;
	}
}
