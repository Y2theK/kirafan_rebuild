﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F5C RID: 3932
	[Token(Token = "0x2000A5E")]
	[StructLayout(3)]
	public class ShopTradeSaleListItemData : ScrollItemData
	{
		// Token: 0x140000DF RID: 223
		// (add) Token: 0x06004ACB RID: 19147 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004ACC RID: 19148 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000DF")]
		public event Action<int> OnClick
		{
			[Token(Token = "0x6004500")]
			[Address(RVA = "0x10156C978", Offset = "0x156C978", VA = "0x10156C978")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004501")]
			[Address(RVA = "0x10156CA84", Offset = "0x156CA84", VA = "0x10156CA84")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004ACD RID: 19149 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004502")]
		[Address(RVA = "0x10156C2B0", Offset = "0x156C2B0", VA = "0x10156C2B0")]
		public ShopTradeSaleListItemData(int itemID)
		{
		}

		// Token: 0x06004ACE RID: 19150 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004503")]
		[Address(RVA = "0x10156CB90", Offset = "0x156CB90", VA = "0x10156CB90", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005CC6 RID: 23750
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004122")]
		public int m_ItemID;

		// Token: 0x04005CC7 RID: 23751
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004123")]
		public string m_NameText;

		// Token: 0x04005CC8 RID: 23752
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004124")]
		public int m_HaveNum;

		// Token: 0x04005CC9 RID: 23753
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004125")]
		public int m_SaleAmount;
	}
}
