﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F5D RID: 3933
	[Token(Token = "0x2000A5F")]
	[StructLayout(3)]
	public class ShopTradeSaleListUI : MenuUIBase
	{
		// Token: 0x140000E0 RID: 224
		// (add) Token: 0x06004ACF RID: 19151 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004AD0 RID: 19152 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E0")]
		public event Action<int, int> OnClickExecuteSaleButton
		{
			[Token(Token = "0x6004504")]
			[Address(RVA = "0x10156CBE4", Offset = "0x156CBE4", VA = "0x10156CBE4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004505")]
			[Address(RVA = "0x10156CCF0", Offset = "0x156CCF0", VA = "0x10156CCF0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004AD1 RID: 19153 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004506")]
		[Address(RVA = "0x10156CDFC", Offset = "0x156CDFC", VA = "0x10156CDFC")]
		public void Setup()
		{
		}

		// Token: 0x06004AD2 RID: 19154 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004507")]
		[Address(RVA = "0x10156D634", Offset = "0x156D634", VA = "0x10156D634")]
		public void Refresh()
		{
		}

		// Token: 0x06004AD3 RID: 19155 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004508")]
		[Address(RVA = "0x10156D250", Offset = "0x156D250", VA = "0x10156D250")]
		private void ApplyTab(ShopTradeSaleListUI.eTab tab)
		{
		}

		// Token: 0x06004AD4 RID: 19156 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004509")]
		[Address(RVA = "0x10156D6C0", Offset = "0x156D6C0", VA = "0x10156D6C0")]
		private void OnChangeTabCallBack(int idx)
		{
		}

		// Token: 0x06004AD5 RID: 19157 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600450A")]
		[Address(RVA = "0x10156D6C4", Offset = "0x156D6C4", VA = "0x10156D6C4")]
		private void Update()
		{
		}

		// Token: 0x06004AD6 RID: 19158 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600450B")]
		[Address(RVA = "0x10156D8D8", Offset = "0x156D8D8", VA = "0x10156D8D8")]
		private void ChangeStep(ShopTradeSaleListUI.eStep step)
		{
		}

		// Token: 0x06004AD7 RID: 19159 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600450C")]
		[Address(RVA = "0x10156DB5C", Offset = "0x156DB5C", VA = "0x10156DB5C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004AD8 RID: 19160 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600450D")]
		[Address(RVA = "0x10156DBFC", Offset = "0x156DBFC", VA = "0x10156DBFC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004AD9 RID: 19161 RVA: 0x0001ADD8 File Offset: 0x00018FD8
		[Token(Token = "0x600450E")]
		[Address(RVA = "0x10156DC9C", Offset = "0x156DC9C", VA = "0x10156DC9C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004ADA RID: 19162 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600450F")]
		[Address(RVA = "0x10156DCAC", Offset = "0x156DCAC", VA = "0x10156DCAC")]
		public void OnClickSaleListButtonCallBack(int itemID)
		{
		}

		// Token: 0x06004ADB RID: 19163 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004510")]
		[Address(RVA = "0x10156DE2C", Offset = "0x156DE2C", VA = "0x10156DE2C")]
		public void OnClickExecuteButton(int itemID, int num)
		{
		}

		// Token: 0x06004ADC RID: 19164 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004511")]
		[Address(RVA = "0x10156DE94", Offset = "0x156DE94", VA = "0x10156DE94")]
		public void OnCancelSaleButtonCallBack()
		{
		}

		// Token: 0x06004ADD RID: 19165 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004512")]
		[Address(RVA = "0x10156DFA4", Offset = "0x156DFA4", VA = "0x10156DFA4")]
		public void OnBackConfirmCallBack(bool shortcut)
		{
		}

		// Token: 0x06004ADE RID: 19166 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004513")]
		[Address(RVA = "0x10156DE98", Offset = "0x156DE98", VA = "0x10156DE98")]
		public void CloseConfirm()
		{
		}

		// Token: 0x06004ADF RID: 19167 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004514")]
		[Address(RVA = "0x10156E040", Offset = "0x156E040", VA = "0x10156E040")]
		public ShopTradeSaleListUI()
		{
		}

		// Token: 0x04005CCB RID: 23755
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004127")]
		private ShopTradeSaleListUI.eStep m_Step;

		// Token: 0x04005CCC RID: 23756
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004128")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005CCD RID: 23757
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004129")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005CCE RID: 23758
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400412A")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04005CCF RID: 23759
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400412B")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005CD0 RID: 23760
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400412C")]
		[SerializeField]
		private ShopTradeSaleConfirmGroup m_ConfirmGroup;

		// Token: 0x04005CD1 RID: 23761
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x400412D")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04005CD2 RID: 23762
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400412E")]
		private ShopTradeSaleListUI.eTab m_Tab;

		// Token: 0x04005CD3 RID: 23763
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400412F")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x02000F5E RID: 3934
		[Token(Token = "0x20011C4")]
		private enum eStep
		{
			// Token: 0x04005CD6 RID: 23766
			[Token(Token = "0x4006EEE")]
			Hide,
			// Token: 0x04005CD7 RID: 23767
			[Token(Token = "0x4006EEF")]
			In,
			// Token: 0x04005CD8 RID: 23768
			[Token(Token = "0x4006EF0")]
			Idle,
			// Token: 0x04005CD9 RID: 23769
			[Token(Token = "0x4006EF1")]
			Out,
			// Token: 0x04005CDA RID: 23770
			[Token(Token = "0x4006EF2")]
			End
		}

		// Token: 0x02000F5F RID: 3935
		[Token(Token = "0x20011C5")]
		public enum eTab
		{
			// Token: 0x04005CDC RID: 23772
			[Token(Token = "0x4006EF4")]
			None = -1,
			// Token: 0x04005CDD RID: 23773
			[Token(Token = "0x4006EF5")]
			Upgrade,
			// Token: 0x04005CDE RID: 23774
			[Token(Token = "0x4006EF6")]
			LimitBreak,
			// Token: 0x04005CDF RID: 23775
			[Token(Token = "0x4006EF7")]
			Evolution,
			// Token: 0x04005CE0 RID: 23776
			[Token(Token = "0x4006EF8")]
			Weapon
		}
	}
}
