﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F60 RID: 3936
	[Token(Token = "0x2000A60")]
	[StructLayout(3)]
	public class ShopTradeSrc : MonoBehaviour
	{
		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06004AE0 RID: 19168 RVA: 0x0001ADF0 File Offset: 0x00018FF0
		// (set) Token: 0x06004AE1 RID: 19169 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x170004DF")]
		public bool IsEnoughSrc
		{
			[Token(Token = "0x6004515")]
			[Address(RVA = "0x101564E70", Offset = "0x1564E70", VA = "0x101564E70")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6004516")]
			[Address(RVA = "0x10156E048", Offset = "0x156E048", VA = "0x10156E048")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06004AE2 RID: 19170 RVA: 0x0001AE08 File Offset: 0x00019008
		[Token(Token = "0x170004E0")]
		public bool IsUseGem
		{
			[Token(Token = "0x6004517")]
			[Address(RVA = "0x101564E5C", Offset = "0x1564E5C", VA = "0x101564E5C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004AE3 RID: 19171 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004518")]
		[Address(RVA = "0x101562C68", Offset = "0x1562C68", VA = "0x101562C68")]
		public void Setup()
		{
		}

		// Token: 0x06004AE4 RID: 19172 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004519")]
		[Address(RVA = "0x10156E050", Offset = "0x156E050", VA = "0x10156E050")]
		private void Update()
		{
		}

		// Token: 0x06004AE5 RID: 19173 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600451A")]
		[Address(RVA = "0x1015642C4", Offset = "0x15642C4", VA = "0x1015642C4")]
		public void Apply(ePresentType src, int id, int useNum, bool interactable)
		{
		}

		// Token: 0x06004AE6 RID: 19174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600451B")]
		[Address(RVA = "0x10156E058", Offset = "0x156E058", VA = "0x10156E058")]
		private void UpdateDisp(bool forceUpdate)
		{
		}

		// Token: 0x06004AE7 RID: 19175 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600451C")]
		[Address(RVA = "0x1015646A0", Offset = "0x15646A0", VA = "0x1015646A0")]
		public string GetName(bool withUseNum = true)
		{
			return null;
		}

		// Token: 0x06004AE8 RID: 19176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600451D")]
		[Address(RVA = "0x10156E4E8", Offset = "0x156E4E8", VA = "0x10156E4E8")]
		public ShopTradeSrc()
		{
		}

		// Token: 0x04005CE1 RID: 23777
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004131")]
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x04005CE2 RID: 23778
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004132")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005CE3 RID: 23779
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004133")]
		[SerializeField]
		private Text m_HaveNum;

		// Token: 0x04005CE4 RID: 23780
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004134")]
		[SerializeField]
		private Text m_UseNum;

		// Token: 0x04005CE5 RID: 23781
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004135")]
		[SerializeField]
		private CustomButton m_ExecButton;

		// Token: 0x04005CE6 RID: 23782
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004136")]
		private int m_Use;

		// Token: 0x04005CE7 RID: 23783
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004137")]
		private long m_Have;

		// Token: 0x04005CE8 RID: 23784
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004138")]
		private ePresentType m_SrcPresentType;

		// Token: 0x04005CE9 RID: 23785
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x4004139")]
		private int m_SrcID;

		// Token: 0x04005CEA RID: 23786
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400413A")]
		private bool m_Interactable;
	}
}
