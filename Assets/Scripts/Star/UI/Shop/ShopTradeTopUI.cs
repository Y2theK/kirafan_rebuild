﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Quest;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F61 RID: 3937
	[Token(Token = "0x2000A61")]
	[StructLayout(3)]
	public class ShopTradeTopUI : MenuUIBase
	{
		// Token: 0x06004AE9 RID: 19177 RVA: 0x0001AE20 File Offset: 0x00019020
		[Token(Token = "0x600451E")]
		[Address(RVA = "0x10156E4F0", Offset = "0x156E4F0", VA = "0x10156E4F0")]
		public static int eMainButtonToInteger(ShopTradeTopUI.eMainButton mainButton)
		{
			return 0;
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06004AEA RID: 19178 RVA: 0x0001AE38 File Offset: 0x00019038
		[Token(Token = "0x170004E1")]
		[Obsolete]
		public ShopTradeTopUI.eMainButton SelectButton
		{
			[Token(Token = "0x600451F")]
			[Address(RVA = "0x10156E4F4", Offset = "0x156E4F4", VA = "0x10156E4F4")]
			get
			{
				return ShopTradeTopUI.eMainButton.Trade;
			}
		}

		// Token: 0x140000E1 RID: 225
		// (add) Token: 0x06004AEB RID: 19179 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004AEC RID: 19180 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E1")]
		public event Action OnClickMainButton
		{
			[Token(Token = "0x6004520")]
			[Address(RVA = "0x10156E514", Offset = "0x156E514", VA = "0x10156E514")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004521")]
			[Address(RVA = "0x10156E620", Offset = "0x156E620", VA = "0x10156E620")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004AED RID: 19181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004522")]
		[Address(RVA = "0x10156E72C", Offset = "0x156E72C", VA = "0x10156E72C")]
		private void Start()
		{
		}

		// Token: 0x06004AEE RID: 19182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004523")]
		[Address(RVA = "0x10156E730", Offset = "0x156E730", VA = "0x10156E730")]
		public void Setup(bool existLimitTrade, [Optional] ShopTradeTopUI.ShopTradeTopUISelectData selectData)
		{
		}

		// Token: 0x06004AEF RID: 19183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004524")]
		[Address(RVA = "0x10156ED00", Offset = "0x156ED00", VA = "0x10156ED00")]
		private void Update()
		{
		}

		// Token: 0x06004AF0 RID: 19184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004525")]
		[Address(RVA = "0x10156F01C", Offset = "0x156F01C", VA = "0x10156F01C")]
		private void ChangeStep(ShopTradeTopUI.eStep step)
		{
		}

		// Token: 0x06004AF1 RID: 19185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004526")]
		[Address(RVA = "0x10156F36C", Offset = "0x156F36C", VA = "0x10156F36C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004AF2 RID: 19186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004527")]
		[Address(RVA = "0x10156F374", Offset = "0x156F374", VA = "0x10156F374", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004AF3 RID: 19187 RVA: 0x0001AE50 File Offset: 0x00019050
		[Token(Token = "0x6004528")]
		[Address(RVA = "0x10156F470", Offset = "0x156F470", VA = "0x10156F470", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004AF4 RID: 19188 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6004529")]
		[Address(RVA = "0x10156F480", Offset = "0x156F480", VA = "0x10156F480")]
		public ShopTradeTopUI.ShopTradeTopUISelectData GetTradeTopUISelectData()
		{
			return null;
		}

		// Token: 0x06004AF5 RID: 19189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452A")]
		[Address(RVA = "0x10156F488", Offset = "0x156F488", VA = "0x10156F488")]
		[Obsolete]
		public void OnClickMainButtonCallBack(int btn)
		{
		}

		// Token: 0x06004AF6 RID: 19190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452B")]
		[Address(RVA = "0x10156F48C", Offset = "0x156F48C", VA = "0x10156F48C")]
		public void OnClickMainButtonCallBack(ShopTradeTopUI.eMainButton parameter)
		{
		}

		// Token: 0x06004AF7 RID: 19191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452C")]
		[Address(RVA = "0x10156F530", Offset = "0x156F530", VA = "0x10156F530")]
		public void OnClickStandardButton(int parameter)
		{
		}

		// Token: 0x06004AF8 RID: 19192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452D")]
		[Address(RVA = "0x102439910", Offset = "0x2439910", VA = "0x102439910")]
		private void OnClickButtonCallBackInternal<ParameterType>(ParameterType parameter) where ParameterType : IComparable
		{
		}

		// Token: 0x06004AF9 RID: 19193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452E")]
		[Address(RVA = "0x10156F65C", Offset = "0x156F65C", VA = "0x10156F65C")]
		public void OnClickBackButton()
		{
		}

		// Token: 0x06004AFA RID: 19194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600452F")]
		[Address(RVA = "0x10156F6C0", Offset = "0x156F6C0", VA = "0x10156F6C0")]
		public ShopTradeTopUI()
		{
		}

		// Token: 0x04005CEC RID: 23788
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400413C")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005CED RID: 23789
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400413D")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005CEE RID: 23790
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400413E")]
		[SerializeField]
		protected FixScrollRect m_TopScroll;

		// Token: 0x04005CEF RID: 23791
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400413F")]
		[SerializeField]
		protected ScrollViewBase m_LimitTradeScroll;

		// Token: 0x04005CF0 RID: 23792
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004140")]
		[SerializeField]
		private CustomButton m_LimitButton;

		// Token: 0x04005CF1 RID: 23793
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004141")]
		[Obsolete]
		private UIGroup m_CurrentGroup;

		// Token: 0x04005CF2 RID: 23794
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004142")]
		[Obsolete]
		private UIGroup m_NextGroup;

		// Token: 0x04005CF3 RID: 23795
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004143")]
		private ShopTradeTopUI.TradeTopCategoryDataControllerRoot m_ButtonInfoParent;

		// Token: 0x04005CF4 RID: 23796
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004144")]
		private ShopTradeTopUI.eStep m_Step;

		// Token: 0x04005CF5 RID: 23797
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004145")]
		private ShopTradeTopUI.ShopTradeTopUISelectData m_SelectData;

		// Token: 0x04005CF6 RID: 23798
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004146")]
		private ShopTradeTopUI.TradeTopCategoryDataControllerBase m_CurrentButtonInfo;

		// Token: 0x04005CF7 RID: 23799
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004147")]
		private ShopTradeTopUI.TradeTopCategoryDataControllerBase m_NextCategoryDataController;

		// Token: 0x02000F62 RID: 3938
		[Token(Token = "0x20011C6")]
		private enum eStep
		{
			// Token: 0x04005CFA RID: 23802
			[Token(Token = "0x4006EFA")]
			Hide,
			// Token: 0x04005CFB RID: 23803
			[Token(Token = "0x4006EFB")]
			BGLoad,
			// Token: 0x04005CFC RID: 23804
			[Token(Token = "0x4006EFC")]
			In,
			// Token: 0x04005CFD RID: 23805
			[Token(Token = "0x4006EFD")]
			Idle,
			// Token: 0x04005CFE RID: 23806
			[Token(Token = "0x4006EFE")]
			GroupOut,
			// Token: 0x04005CFF RID: 23807
			[Token(Token = "0x4006EFF")]
			Out,
			// Token: 0x04005D00 RID: 23808
			[Token(Token = "0x4006F00")]
			End
		}

		// Token: 0x02000F63 RID: 3939
		[Token(Token = "0x20011C7")]
		public enum eMainButton
		{
			// Token: 0x04005D02 RID: 23810
			[Token(Token = "0x4006F02")]
			None = -1,
			// Token: 0x04005D03 RID: 23811
			[Token(Token = "0x4006F03")]
			Trade,
			// Token: 0x04005D04 RID: 23812
			[Token(Token = "0x4006F04")]
			Limit,
			// Token: 0x04005D05 RID: 23813
			[Token(Token = "0x4006F05")]
			Sale,
			// Token: 0x04005D06 RID: 23814
			[Token(Token = "0x4006F06")]
			Chest
		}

		// Token: 0x02000F64 RID: 3940
		[Token(Token = "0x20011C8")]
		private abstract class ScrollWrapper
		{
			// Token: 0x06004AFB RID: 19195 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600627E")]
			[Address(RVA = "0x10156FA24", Offset = "0x156FA24", VA = "0x10156FA24")]
			protected void ScrollWrapper_SetupInternal()
			{
			}

			// Token: 0x06004AFC RID: 19196 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600627F")]
			[Address(RVA = "0x10156FA28", Offset = "0x156FA28", VA = "0x10156FA28")]
			public void Open()
			{
			}

			// Token: 0x06004AFD RID: 19197 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006280")]
			[Address(RVA = "0x10156FB4C", Offset = "0x156FB4C", VA = "0x10156FB4C")]
			public void Close()
			{
			}

			// Token: 0x06004AFE RID: 19198 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006281")]
			[Address(RVA = "0x10156FA30", Offset = "0x156FA30", VA = "0x10156FA30")]
			private void SetActive(bool active)
			{
			}

			// Token: 0x06004AFF RID: 19199
			[Token(Token = "0x6006282")]
			[Address(Slot = "4")]
			protected abstract Component GetScroll();

			// Token: 0x06004B00 RID: 19200
			[Token(Token = "0x6006283")]
			[Address(Slot = "5")]
			public abstract void RemoveAll();

			// Token: 0x06004B01 RID: 19201
			[Token(Token = "0x6006284")]
			[Address(Slot = "6")]
			public abstract void AddItem(ScrollItemData item);

			// Token: 0x06004B02 RID: 19202 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006285")]
			[Address(RVA = "0x10156F704", Offset = "0x156F704", VA = "0x10156F704")]
			protected ScrollWrapper()
			{
			}
		}

		// Token: 0x02000F65 RID: 3941
		[Token(Token = "0x20011C9")]
		private class FixScrollRectWrapper : ShopTradeTopUI.ScrollWrapper
		{
			// Token: 0x06004B03 RID: 19203 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006286")]
			[Address(RVA = "0x10156EA7C", Offset = "0x156EA7C", VA = "0x10156EA7C")]
			public void Setup(FixScrollRect scroll)
			{
			}

			// Token: 0x06004B04 RID: 19204 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006287")]
			[Address(RVA = "0x10156F6F4", Offset = "0x156F6F4", VA = "0x10156F6F4", Slot = "5")]
			public override void RemoveAll()
			{
			}

			// Token: 0x06004B05 RID: 19205 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006288")]
			[Address(RVA = "0x10156F6F8", Offset = "0x156F6F8", VA = "0x10156F6F8", Slot = "6")]
			public override void AddItem(ScrollItemData item)
			{
			}

			// Token: 0x06004B06 RID: 19206 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006289")]
			[Address(RVA = "0x10156F6FC", Offset = "0x156F6FC", VA = "0x10156F6FC", Slot = "4")]
			protected override Component GetScroll()
			{
				return null;
			}

			// Token: 0x06004B07 RID: 19207 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600628A")]
			[Address(RVA = "0x10156EA74", Offset = "0x156EA74", VA = "0x10156EA74")]
			public FixScrollRectWrapper()
			{
			}

			// Token: 0x04005D07 RID: 23815
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F07")]
			private FixScrollRect m_Scroll;
		}

		// Token: 0x02000F66 RID: 3942
		[Token(Token = "0x20011CA")]
		private class ScrollViewBaseWrapper : ShopTradeTopUI.ScrollWrapper
		{
			// Token: 0x06004B08 RID: 19208 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600628B")]
			[Address(RVA = "0x10156EA8C", Offset = "0x156EA8C", VA = "0x10156EA8C")]
			public void Setup(ScrollViewBase scroll)
			{
			}

			// Token: 0x06004B09 RID: 19209 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600628C")]
			[Address(RVA = "0x10156F8C8", Offset = "0x156F8C8", VA = "0x10156F8C8", Slot = "5")]
			public override void RemoveAll()
			{
			}

			// Token: 0x06004B0A RID: 19210 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600628D")]
			[Address(RVA = "0x10156F968", Offset = "0x156F968", VA = "0x10156F968", Slot = "6")]
			public override void AddItem(ScrollItemData item)
			{
			}

			// Token: 0x06004B0B RID: 19211 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x600628E")]
			[Address(RVA = "0x10156FA1C", Offset = "0x156FA1C", VA = "0x10156FA1C", Slot = "4")]
			protected override Component GetScroll()
			{
				return null;
			}

			// Token: 0x06004B0C RID: 19212 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600628F")]
			[Address(RVA = "0x10156EA84", Offset = "0x156EA84", VA = "0x10156EA84")]
			public ScrollViewBaseWrapper()
			{
			}

			// Token: 0x04005D08 RID: 23816
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F08")]
			private ScrollViewBase m_Scroll;
		}

		// Token: 0x02000F67 RID: 3943
		[Token(Token = "0x20011CB")]
		private abstract class ButtonInfoBase
		{
			// Token: 0x06004B0D RID: 19213 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006290")]
			[Address(RVA = "0x10156F6C8", Offset = "0x156F6C8", VA = "0x10156F6C8")]
			protected void SetupInternal(ShopTradeTopUI.ButtonInfoBase.eGenType genType)
			{
			}

			// Token: 0x06004B0E RID: 19214 RVA: 0x0001AE68 File Offset: 0x00019068
			[Token(Token = "0x6006291")]
			[Address(RVA = "0x10156F6D0", Offset = "0x156F6D0", VA = "0x10156F6D0")]
			public ShopTradeTopUI.ButtonInfoBase.eGenType GetGenType()
			{
				return ShopTradeTopUI.ButtonInfoBase.eGenType.Error;
			}

			// Token: 0x06004B0F RID: 19215 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006292")]
			[Address(RVA = "0x10156F6D8", Offset = "0x156F6D8", VA = "0x10156F6D8")]
			protected ButtonInfoBase()
			{
			}

			// Token: 0x04005D09 RID: 23817
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F09")]
			private ShopTradeTopUI.ButtonInfoBase.eGenType m_GenType;

			// Token: 0x02000F68 RID: 3944
			[Token(Token = "0x2001350")]
			public enum eGenType
			{
				// Token: 0x04005D0B RID: 23819
				[Token(Token = "0x40075BC")]
				Error,
				// Token: 0x04005D0C RID: 23820
				[Token(Token = "0x40075BD")]
				Parent,
				// Token: 0x04005D0D RID: 23821
				[Token(Token = "0x40075BE")]
				MainButton,
				// Token: 0x04005D0E RID: 23822
				[Token(Token = "0x40075BF")]
				Int
			}

			// Token: 0x02000F69 RID: 3945
			[Token(Token = "0x2001351")]
			public static class GenTypeToeGenType<GT>
			{
				// Token: 0x06004B11 RID: 19217 RVA: 0x0001AE80 File Offset: 0x00019080
				[Token(Token = "0x600651C")]
				[Address(RVA = "0x1016D13A0", Offset = "0x16D13A0", VA = "0x1016D13A0")]
				public static ShopTradeTopUI.ButtonInfoBase.eGenType GeteGenType()
				{
					return ShopTradeTopUI.ButtonInfoBase.eGenType.Error;
				}

				// Token: 0x04005D0F RID: 23823
				[Token(Token = "0x40075C0")]
				private static ShopTradeTopUI.ButtonInfoBase.eGenType m_GenType;
			}
		}

		// Token: 0x02000F6A RID: 3946
		[Token(Token = "0x20011CC")]
		private class ButtonInfoParent : ShopTradeTopUI.ButtonInfoBase
		{
			// Token: 0x06004B12 RID: 19218 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006293")]
			[Address(RVA = "0x10156F6E0", Offset = "0x156F6E0", VA = "0x10156F6E0")]
			public void Setup()
			{
			}

			// Token: 0x06004B13 RID: 19219 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006294")]
			[Address(RVA = "0x10156F6EC", Offset = "0x156F6EC", VA = "0x10156F6EC")]
			public ButtonInfoParent()
			{
			}
		}

		// Token: 0x02000F6B RID: 3947
		[Token(Token = "0x20011CD")]
		private abstract class ButtonInfoBaseGen<GenType> : ShopTradeTopUI.ButtonInfoBase
		{
			// Token: 0x06004B14 RID: 19220 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006295")]
			[Address(RVA = "0x1016D1850", Offset = "0x16D1850", VA = "0x1016D1850")]
			protected ButtonInfoBaseGen()
			{
			}

			// Token: 0x06004B15 RID: 19221 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006296")]
			[Address(RVA = "0x1016D187C", Offset = "0x16D187C", VA = "0x1016D187C")]
			protected void SetupInternal(GenType parameter)
			{
			}

			// Token: 0x06004B16 RID: 19222 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x6006297")]
			[Address(RVA = "0x1016D1848", Offset = "0x16D1848", VA = "0x1016D1848")]
			public GenType GetParameter()
			{
				return null;
			}

			// Token: 0x04005D10 RID: 23824
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F0A")]
			private GenType m_Parameter;
		}

		// Token: 0x02000F6C RID: 3948
		[Token(Token = "0x20011CE")]
		private abstract class MainButtonInfo : ShopTradeTopUI.ButtonInfoBaseGen<ShopTradeTopUI.eMainButton>
		{
			// Token: 0x06004B17 RID: 19223 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006298")]
			[Address(RVA = "0x10156F76C", Offset = "0x156F76C", VA = "0x10156F76C")]
			protected MainButtonInfo()
			{
			}
		}

		// Token: 0x02000F6D RID: 3949
		[Token(Token = "0x20011CF")]
		private abstract class NoChildrenMainButtonInfo : ShopTradeTopUI.MainButtonInfo
		{
			// Token: 0x06004B18 RID: 19224 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006299")]
			[Address(RVA = "0x10156F7BC", Offset = "0x156F7BC", VA = "0x10156F7BC")]
			protected NoChildrenMainButtonInfo()
			{
			}
		}

		// Token: 0x02000F6E RID: 3950
		[Token(Token = "0x20011D0")]
		private class TradeMainButtonInfo : ShopTradeTopUI.NoChildrenMainButtonInfo
		{
			// Token: 0x06004B19 RID: 19225 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629A")]
			[Address(RVA = "0x10156FDBC", Offset = "0x156FDBC", VA = "0x10156FDBC")]
			public void Setup()
			{
			}

			// Token: 0x06004B1A RID: 19226 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629B")]
			[Address(RVA = "0x10156FE10", Offset = "0x156FE10", VA = "0x10156FE10")]
			public TradeMainButtonInfo()
			{
			}
		}

		// Token: 0x02000F6F RID: 3951
		[Token(Token = "0x20011D1")]
		private class LimitTradeMainButtonInfo : ShopTradeTopUI.MainButtonInfo
		{
			// Token: 0x06004B1B RID: 19227 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629C")]
			[Address(RVA = "0x10156F714", Offset = "0x156F714", VA = "0x10156F714")]
			public void Setup()
			{
			}

			// Token: 0x06004B1C RID: 19228 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629D")]
			[Address(RVA = "0x10156F768", Offset = "0x156F768", VA = "0x10156F768")]
			public LimitTradeMainButtonInfo()
			{
			}
		}

		// Token: 0x02000F70 RID: 3952
		[Token(Token = "0x20011D2")]
		private class SaleMainButtonInfo : ShopTradeTopUI.NoChildrenMainButtonInfo
		{
			// Token: 0x06004B1D RID: 19229 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629E")]
			[Address(RVA = "0x10156F870", Offset = "0x156F870", VA = "0x10156F870")]
			public void Setup()
			{
			}

			// Token: 0x06004B1E RID: 19230 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600629F")]
			[Address(RVA = "0x10156F8C4", Offset = "0x156F8C4", VA = "0x10156F8C4")]
			public SaleMainButtonInfo()
			{
			}
		}

		// Token: 0x02000F71 RID: 3953
		[Token(Token = "0x20011D3")]
		private abstract class StandardButtonInfo : ShopTradeTopUI.ButtonInfoBaseGen<int>
		{
			// Token: 0x06004B1F RID: 19231 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A0")]
			[Address(RVA = "0x10156F820", Offset = "0x156F820", VA = "0x10156F820")]
			protected StandardButtonInfo()
			{
			}
		}

		// Token: 0x02000F72 RID: 3954
		[Token(Token = "0x20011D4")]
		private abstract class NoChildrenStandardButtonInfo : ShopTradeTopUI.StandardButtonInfo
		{
			// Token: 0x06004B20 RID: 19232 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A1")]
			[Address(RVA = "0x10156F7C0", Offset = "0x156F7C0", VA = "0x10156F7C0")]
			public void Setup(int parameter)
			{
			}

			// Token: 0x06004B21 RID: 19233 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A2")]
			[Address(RVA = "0x10156F710", Offset = "0x156F710", VA = "0x10156F710")]
			protected NoChildrenStandardButtonInfo()
			{
			}
		}

		// Token: 0x02000F73 RID: 3955
		[Token(Token = "0x20011D5")]
		private class LimitTradeAvailableEventQuestButtonInfo : ShopTradeTopUI.NoChildrenStandardButtonInfo
		{
			// Token: 0x06004B22 RID: 19234 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A3")]
			[Address(RVA = "0x10156F70C", Offset = "0x156F70C", VA = "0x10156F70C")]
			public LimitTradeAvailableEventQuestButtonInfo()
			{
			}
		}

		// Token: 0x02000F74 RID: 3956
		[Token(Token = "0x20011D6")]
		private abstract class TradeTopCategoryChildrenDataControllerBase
		{
			// Token: 0x06004B23 RID: 19235 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A4")]
			[Address(RVA = "0x10156FE1C", Offset = "0x156FE1C", VA = "0x10156FE1C")]
			protected TradeTopCategoryChildrenDataControllerBase()
			{
			}

			// Token: 0x06004B24 RID: 19236 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A5")]
			[Address(RVA = "0x10156FE30", Offset = "0x156FE30", VA = "0x10156FE30")]
			protected void TradeTopCategoryChildrenDataControllerBase_SetupInternal(ShopTradeTopUI.ButtonInfoBase.eGenType genType)
			{
			}

			// Token: 0x06004B25 RID: 19237 RVA: 0x0001AE98 File Offset: 0x00019098
			[Token(Token = "0x60062A6")]
			[Address(RVA = "0x10156FEF0", Offset = "0x156FEF0", VA = "0x10156FEF0")]
			public ShopTradeTopUI.ButtonInfoBase.eGenType GetGenType()
			{
				return ShopTradeTopUI.ButtonInfoBase.eGenType.Error;
			}

			// Token: 0x06004B26 RID: 19238
			[Token(Token = "0x60062A7")]
			[Address(Slot = "4")]
			public abstract int HowManyControllers();

			// Token: 0x06004B27 RID: 19239
			[Token(Token = "0x60062A8")]
			[Address(Slot = "5")]
			protected abstract ShopTradeTopUI.TradeTopCategoryDataController GetControllerFromIndexBaseInternal(int index);

			// Token: 0x04005D11 RID: 23825
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F0B")]
			private ShopTradeTopUI.ButtonInfoBase.eGenType m_GenType;
		}

		// Token: 0x02000F75 RID: 3957
		[Token(Token = "0x20011D7")]
		private abstract class TradeTopCategoryChildrenDataController : ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase
		{
			// Token: 0x06004B28 RID: 19240 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062A9")]
			[Address(RVA = "0x10156FE14", Offset = "0x156FE14", VA = "0x10156FE14")]
			protected TradeTopCategoryChildrenDataController()
			{
			}

			// Token: 0x06004B29 RID: 19241 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062AA")]
			[Address(RVA = "0x10156FE24", Offset = "0x156FE24", VA = "0x10156FE24")]
			protected void TradeTopCategoryChildrenDataController_SetupInternal(ShopTradeTopUI.ButtonInfoBase.eGenType genType, ShopTradeTopUI.ScrollWrapper scroll)
			{
			}

			// Token: 0x06004B2A RID: 19242 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062AB")]
			[Address(RVA = "0x10156FE38", Offset = "0x156FE38", VA = "0x10156FE38", Slot = "6")]
			public virtual void Scroll_Open()
			{
			}

			// Token: 0x06004B2B RID: 19243 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062AC")]
			[Address(RVA = "0x10156FE68", Offset = "0x156FE68", VA = "0x10156FE68", Slot = "7")]
			public virtual void Scroll_Close()
			{
			}

			// Token: 0x06004B2C RID: 19244 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062AD")]
			[Address(RVA = "0x10156FE98", Offset = "0x156FE98", VA = "0x10156FE98")]
			protected void Scroll_RemoveAll()
			{
			}

			// Token: 0x06004B2D RID: 19245 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062AE")]
			[Address(RVA = "0x10156FECC", Offset = "0x156FECC", VA = "0x10156FECC")]
			protected void Scroll_AddItem(ScrollItemData item)
			{
			}

			// Token: 0x06004B2E RID: 19246 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062AF")]
			[Address(RVA = "0x10156FEE4", Offset = "0x156FEE4", VA = "0x10156FEE4")]
			public ShopTradeTopUI.TradeTopCategoryDataController GetControllerFromIndexBase(int index)
			{
				return null;
			}

			// Token: 0x04005D12 RID: 23826
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006F0C")]
			private ShopTradeTopUI.ScrollWrapper m_Scroll;
		}

		// Token: 0x02000F76 RID: 3958
		[Token(Token = "0x20011D8")]
		private abstract class TradeTopCategoryChildrenDataControllerGen<ParameterType> : ShopTradeTopUI.TradeTopCategoryChildrenDataController where ParameterType : IComparable
		{
			// Token: 0x06004B2F RID: 19247 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062B0")]
			[Address(RVA = "0x1016D1A58", Offset = "0x16D1A58", VA = "0x1016D1A58")]
			public TradeTopCategoryChildrenDataControllerGen()
			{
			}

			// Token: 0x06004B30 RID: 19248 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062B1")]
			[Address(RVA = "0x1016D1A84", Offset = "0x16D1A84", VA = "0x1016D1A84")]
			protected void TradeTopCategoryChildrenDataControllerGen_SetupInternal(ShopTradeTopUI.ScrollWrapper scroll, List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> controllers)
			{
			}

			// Token: 0x06004B31 RID: 19249 RVA: 0x0001AEB0 File Offset: 0x000190B0
			[Token(Token = "0x60062B2")]
			[Address(RVA = "0x1016D1FEC", Offset = "0x16D1FEC", VA = "0x1016D1FEC", Slot = "4")]
			public override int HowManyControllers()
			{
				return 0;
			}

			// Token: 0x06004B32 RID: 19250 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062B3")]
			[Address(RVA = "0x1016D1BC8", Offset = "0x16D1BC8", VA = "0x1016D1BC8")]
			protected List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> GetControllers()
			{
				return null;
			}

			// Token: 0x06004B33 RID: 19251 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062B4")]
			[Address(RVA = "0x1016D2058", Offset = "0x16D2058", VA = "0x1016D2058", Slot = "5")]
			protected override ShopTradeTopUI.TradeTopCategoryDataController GetControllerFromIndexBaseInternal(int index)
			{
				return null;
			}

			// Token: 0x06004B34 RID: 19252 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062B5")]
			[Address(RVA = "0x1016D1C78", Offset = "0x16D1C78", VA = "0x1016D1C78")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType> GetControllerFromParameter(ParameterType parameter)
			{
				return null;
			}

			// Token: 0x06004B35 RID: 19253 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062B6")]
			[Address(RVA = "0x1016D1CF0", Offset = "0x16D1CF0", VA = "0x1016D1CF0")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType> GetControllerFromIndex(int index)
			{
				return null;
			}

			// Token: 0x06004B36 RID: 19254 RVA: 0x0001AEC8 File Offset: 0x000190C8
			[Token(Token = "0x60062B7")]
			[Address(RVA = "0x1016D1DD8", Offset = "0x16D1DD8", VA = "0x1016D1DD8")]
			public int ParameterToIndex(ParameterType parameter)
			{
				return 0;
			}

			// Token: 0x04005D13 RID: 23827
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F0D")]
			private List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> m_Controllers;
		}

		// Token: 0x02000F77 RID: 3959
		[Token(Token = "0x20011D9")]
		private abstract class TradeTopConstantScrollCategoryChildrenDataController<ParameterType> : ShopTradeTopUI.TradeTopCategoryChildrenDataControllerGen<ParameterType> where ParameterType : IComparable
		{
			// Token: 0x06004B37 RID: 19255 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062B8")]
			[Address(RVA = "0x1016D48E4", Offset = "0x16D48E4", VA = "0x1016D48E4")]
			public TradeTopConstantScrollCategoryChildrenDataController()
			{
			}

			// Token: 0x06004B38 RID: 19256 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062B9")]
			[Address(RVA = "0x1016D4920", Offset = "0x16D4920", VA = "0x1016D4920")]
			public void Setup(ShopTradeTopUI.ScrollWrapper scroll, List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> controllers)
			{
			}

			// Token: 0x04005D14 RID: 23828
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F0E")]
			private List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> m_Controllers;
		}

		// Token: 0x02000F78 RID: 3960
		[Token(Token = "0x20011DA")]
		private abstract class TradeTopVariableScrollCategoryChildrenDataController<ParameterType, ScrollItemDataType> : ShopTradeTopUI.TradeTopCategoryChildrenDataControllerGen<ParameterType> where ParameterType : IComparable where ScrollItemDataType : ScrollItemData
		{
			// Token: 0x06004B39 RID: 19257 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062BA")]
			[Address(RVA = "0x1016D4DA0", Offset = "0x16D4DA0", VA = "0x1016D4DA0")]
			public TradeTopVariableScrollCategoryChildrenDataController()
			{
			}

			// Token: 0x06004B3A RID: 19258 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062BB")]
			[Address(RVA = "0x1016D4DDC", Offset = "0x16D4DDC", VA = "0x1016D4DDC", Slot = "6")]
			public override void Scroll_Open()
			{
			}

			// Token: 0x06004B3B RID: 19259 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062BC")]
			[Address(RVA = "0x1016D4EB8", Offset = "0x16D4EB8", VA = "0x1016D4EB8", Slot = "7")]
			public override void Scroll_Close()
			{
			}

			// Token: 0x06004B3C RID: 19260
			[Token(Token = "0x60062BD")]
			[Address(Slot = "8")]
			protected abstract ScrollItemDataType IndexToScrollItemDataType(int index);

			// Token: 0x04005D15 RID: 23829
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F0F")]
			private List<ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType>> m_Controllers;
		}

		// Token: 0x02000F79 RID: 3961
		[Token(Token = "0x20011DB")]
		private class TradeTopRootChildrenDataController : ShopTradeTopUI.TradeTopConstantScrollCategoryChildrenDataController<ShopTradeTopUI.eMainButton>
		{
			// Token: 0x06004B3D RID: 19261 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062BE")]
			[Address(RVA = "0x101570474", Offset = "0x1570474", VA = "0x101570474")]
			public TradeTopRootChildrenDataController()
			{
			}
		}

		// Token: 0x02000F7A RID: 3962
		[Token(Token = "0x20011DC")]
		private class TradeTopLimitTradeCategoryChildrenDataController : ShopTradeTopUI.TradeTopVariableScrollCategoryChildrenDataController<int, TradeTopLimitTradeItemData>
		{
			// Token: 0x06004B3E RID: 19262 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062BF")]
			[Address(RVA = "0x1015705B0", Offset = "0x15705B0", VA = "0x1015705B0")]
			public TradeTopLimitTradeCategoryChildrenDataController()
			{
			}

			// Token: 0x06004B3F RID: 19263 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062C0")]
			[Address(RVA = "0x101570600", Offset = "0x1570600", VA = "0x101570600")]
			public void Setup(ShopTradeTopUI.ScrollWrapper scroll, Action<int> childrenAction)
			{
			}

			// Token: 0x06004B40 RID: 19264 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062C1")]
			[Address(RVA = "0x101570B38", Offset = "0x1570B38", VA = "0x101570B38", Slot = "8")]
			protected override TradeTopLimitTradeItemData IndexToScrollItemDataType(int index)
			{
				return null;
			}

			// Token: 0x04005D16 RID: 23830
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006F10")]
			private Action<int> m_ChildrenAction;
		}

		// Token: 0x02000F7B RID: 3963
		[Token(Token = "0x20011DD")]
		private abstract class TradeTopCategoryDataControllerBase
		{
			// Token: 0x06004B41 RID: 19265 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062C2")]
			[Address(RVA = "0x10156FF00", Offset = "0x156FF00", VA = "0x10156FF00")]
			protected TradeTopCategoryDataControllerBase()
			{
			}

			// Token: 0x06004B42 RID: 19266 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062C3")]
			[Address(RVA = "0x10156FF80", Offset = "0x156FF80", VA = "0x10156FF80")]
			protected void TradeTopCategoryDataControllerBase_SetupInternal(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent, ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase children)
			{
			}

			// Token: 0x06004B43 RID: 19267
			[Token(Token = "0x60062C4")]
			[Address(Slot = "4")]
			public abstract ShopTradeTopUI.ButtonInfoBase.eGenType GetGenType();

			// Token: 0x06004B44 RID: 19268 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062C5")]
			[Address(RVA = "0x10156F6B8", Offset = "0x156F6B8", VA = "0x10156F6B8")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerBase GetParent()
			{
				return null;
			}

			// Token: 0x06004B45 RID: 19269 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062C6")]
			[Address(RVA = "0x10156FF88", Offset = "0x156FF88", VA = "0x10156FF88")]
			protected ShopTradeTopUI.TradeTopCategoryChildrenDataController GetChildren()
			{
				return null;
			}

			// Token: 0x06004B46 RID: 19270 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062C7")]
			[Address(RVA = "0x10156F344", Offset = "0x156F344", VA = "0x10156F344")]
			public void OpenCategory()
			{
			}

			// Token: 0x06004B47 RID: 19271 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062C8")]
			[Address(RVA = "0x10156F31C", Offset = "0x156F31C", VA = "0x10156F31C")]
			public void CloseCategory()
			{
			}

			// Token: 0x06004B48 RID: 19272 RVA: 0x0001AEE0 File Offset: 0x000190E0
			[Token(Token = "0x60062C9")]
			[Address(RVA = "0x10156F444", Offset = "0x156F444", VA = "0x10156F444")]
			public int HowManyChildren()
			{
				return 0;
			}

			// Token: 0x06004B49 RID: 19273 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062CA")]
			[Address(RVA = "0x101570088", Offset = "0x1570088", VA = "0x101570088", Slot = "5")]
			[Obsolete]
			public virtual ShopTradeTopUI.TradeTopCategoryDataController GetChild(int cursor)
			{
				return null;
			}

			// Token: 0x06004B4A RID: 19274 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062CB")]
			[Address(RVA = "0x10156FF44", Offset = "0x156FF44", VA = "0x10156FF44")]
			public ShopTradeTopUI.TradeTopCategoryDataController GetChildFromIndexBase(int index)
			{
				return null;
			}

			// Token: 0x06004B4B RID: 19275 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062CC")]
			[Address(RVA = "0x1015700C4", Offset = "0x15700C4", VA = "0x1015700C4")]
			protected ShopTradeTopUI.TradeTopCategoryDataController GetChildFromIndexBaseInternal(int index)
			{
				return null;
			}

			// Token: 0x04005D17 RID: 23831
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F11")]
			private ShopTradeTopUI.TradeTopCategoryDataControllerBase m_Parent;

			// Token: 0x04005D18 RID: 23832
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006F12")]
			private ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase m_Children;
		}

		// Token: 0x02000F7C RID: 3964
		[Token(Token = "0x20011DE")]
		private abstract class TradeTopCategoryDataController : ShopTradeTopUI.TradeTopCategoryDataControllerBase
		{
			// Token: 0x06004B4C RID: 19276 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062CD")]
			[Address(RVA = "0x10156FEF8", Offset = "0x156FEF8", VA = "0x10156FEF8")]
			protected TradeTopCategoryDataController()
			{
			}

			// Token: 0x06004B4D RID: 19277 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062CE")]
			[Address(RVA = "0x10156FF08", Offset = "0x156FF08", VA = "0x10156FF08", Slot = "5")]
			[Obsolete]
			public override ShopTradeTopUI.TradeTopCategoryDataController GetChild(int cursor)
			{
				return null;
			}
		}

		// Token: 0x02000F7D RID: 3965
		[Token(Token = "0x20011DF")]
		private abstract class TradeTopCategoryDataControllerGen<ParameterType> : ShopTradeTopUI.TradeTopCategoryDataController where ParameterType : IComparable
		{
			// Token: 0x06004B4E RID: 19278 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062CF")]
			[Address(RVA = "0x1016D3898", Offset = "0x16D3898", VA = "0x1016D3898")]
			protected TradeTopCategoryDataControllerGen()
			{
			}

			// Token: 0x06004B4F RID: 19279 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062D0")]
			[Address(RVA = "0x1016D38C4", Offset = "0x16D38C4", VA = "0x1016D38C4")]
			protected void TradeTopCategoryDataControllerGen_SetupInternal(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent, ShopTradeTopUI.ButtonInfoBaseGen<ParameterType> buttonInfo, ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase children)
			{
			}

			// Token: 0x06004B50 RID: 19280 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062D1")]
			[Address(RVA = "0x1016D3914", Offset = "0x16D3914", VA = "0x1016D3914")]
			protected ShopTradeTopUI.ButtonInfoBaseGen<ParameterType> GetButtonInfo()
			{
				return null;
			}

			// Token: 0x06004B51 RID: 19281 RVA: 0x0001AEF8 File Offset: 0x000190F8
			[Token(Token = "0x60062D2")]
			[Address(RVA = "0x1016D3A80", Offset = "0x16D3A80", VA = "0x1016D3A80", Slot = "4")]
			public override ShopTradeTopUI.ButtonInfoBase.eGenType GetGenType()
			{
				return ShopTradeTopUI.ButtonInfoBase.eGenType.Error;
			}

			// Token: 0x06004B52 RID: 19282 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062D3")]
			[Address(RVA = "0x1016D3AD4", Offset = "0x16D3AD4", VA = "0x1016D3AD4")]
			public ParameterType GetParameter()
			{
				return null;
			}

			// Token: 0x04005D19 RID: 23833
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F13")]
			private ShopTradeTopUI.ButtonInfoBaseGen<ParameterType> m_ButtonInfo;
		}

		// Token: 0x02000F7E RID: 3966
		[Token(Token = "0x20011E0")]
		private abstract class TradeTopCategoryDataControllerExistChildrenBase<ParameterType> : ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType> where ParameterType : IComparable
		{
			// Token: 0x06004B53 RID: 19283 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062D4")]
			[Address(RVA = "0x1016D2870", Offset = "0x16D2870", VA = "0x1016D2870")]
			protected TradeTopCategoryDataControllerExistChildrenBase()
			{
			}

			// Token: 0x06004B54 RID: 19284 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062D5")]
			[Address(RVA = "0x1016D28AC", Offset = "0x16D28AC", VA = "0x1016D28AC")]
			protected void TradeTopCategoryDataControllerExistChildrenBase_SetupInternal(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent, ShopTradeTopUI.ButtonInfoBaseGen<ParameterType> buttonInfo, ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase children)
			{
			}

			// Token: 0x06004B55 RID: 19285
			[Token(Token = "0x60062D6")]
			[Address(Slot = "6")]
			public abstract ShopTradeTopUI.TradeTopCategoryDataController GetSelectedChildren();

			// Token: 0x06004B56 RID: 19286 RVA: 0x0001AF10 File Offset: 0x00019110
			[Token(Token = "0x60062D7")]
			[Address(RVA = "0x1016D291C", Offset = "0x16D291C", VA = "0x1016D291C")]
			public int GetSelectIndex()
			{
				return 0;
			}

			// Token: 0x06004B57 RID: 19287 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062D8")]
			[Address(RVA = "0x1016D2924", Offset = "0x16D2924", VA = "0x1016D2924")]
			protected void SetSelectIndex(int selectIndex)
			{
			}

			// Token: 0x04005D1A RID: 23834
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F14")]
			private int m_SelectIndex;
		}

		// Token: 0x02000F7F RID: 3967
		[Token(Token = "0x20011E1")]
		private abstract class TradeTopCategoryDataControllerExistChildren<ParameterType, ChildrenParameterType> : ShopTradeTopUI.TradeTopCategoryDataControllerExistChildrenBase<ParameterType> where ParameterType : IComparable where ChildrenParameterType : IComparable
		{
			// Token: 0x06004B58 RID: 19288 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062D9")]
			[Address(RVA = "0x1016D29E8", Offset = "0x16D29E8", VA = "0x1016D29E8")]
			protected TradeTopCategoryDataControllerExistChildren()
			{
			}

			// Token: 0x06004B59 RID: 19289 RVA: 0x0001AF28 File Offset: 0x00019128
			[Token(Token = "0x60062DA")]
			[Address(RVA = "0x1016D2A24", Offset = "0x16D2A24", VA = "0x1016D2A24")]
			public int ParameterToIndex(ChildrenParameterType parameter)
			{
				return 0;
			}

			// Token: 0x06004B5A RID: 19290 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062DB")]
			[Address(RVA = "0x1016D2B28", Offset = "0x16D2B28", VA = "0x1016D2B28")]
			public void OnSelectChildren(ChildrenParameterType parameter)
			{
			}

			// Token: 0x06004B5B RID: 19291 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062DC")]
			[Address(RVA = "0x1016D2BA0", Offset = "0x16D2BA0", VA = "0x1016D2BA0", Slot = "6")]
			public override ShopTradeTopUI.TradeTopCategoryDataController GetSelectedChildren()
			{
				return null;
			}

			// Token: 0x06004B5C RID: 19292 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062DD")]
			[Address(RVA = "0x1016D2C10", Offset = "0x16D2C10", VA = "0x1016D2C10")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ChildrenParameterType> GetChildFromParameter(ChildrenParameterType parameter)
			{
				return null;
			}

			// Token: 0x06004B5D RID: 19293 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062DE")]
			[Address(RVA = "0x1016D2C88", Offset = "0x16D2C88", VA = "0x1016D2C88")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ChildrenParameterType> GetChildFromIndex(int index)
			{
				return null;
			}
		}

		// Token: 0x02000F80 RID: 3968
		[Token(Token = "0x20011E2")]
		private abstract class TradeTopCategoryDataControllerNotExistParentAndExistChildren<ChildrenParameterType> : ShopTradeTopUI.TradeTopCategoryDataControllerBase where ChildrenParameterType : IComparable
		{
			// Token: 0x06004B5E RID: 19294 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062DF")]
			[Address(RVA = "0x1016D4090", Offset = "0x16D4090", VA = "0x1016D4090")]
			protected TradeTopCategoryDataControllerNotExistParentAndExistChildren()
			{
			}

			// Token: 0x06004B5F RID: 19295 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062E0")]
			[Address(RVA = "0x1016D40BC", Offset = "0x16D40BC", VA = "0x1016D40BC")]
			protected void TradeTopCategoryDataControllerNotExistParentAndExistChildren_SetupInternal(ShopTradeTopUI.TradeTopCategoryChildrenDataControllerBase children)
			{
			}

			// Token: 0x06004B60 RID: 19296 RVA: 0x0001AF40 File Offset: 0x00019140
			[Token(Token = "0x60062E1")]
			[Address(RVA = "0x1016D3D34", Offset = "0x16D3D34", VA = "0x1016D3D34")]
			public int ParameterToIndex(ChildrenParameterType parameter)
			{
				return 0;
			}

			// Token: 0x06004B61 RID: 19297 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062E2")]
			[Address(RVA = "0x1016D3E38", Offset = "0x16D3E38", VA = "0x1016D3E38")]
			public void OnSelectChildren(ChildrenParameterType parameter)
			{
			}

			// Token: 0x06004B62 RID: 19298 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062E3")]
			[Address(RVA = "0x1016D3EB0", Offset = "0x16D3EB0", VA = "0x1016D3EB0")]
			public ShopTradeTopUI.TradeTopCategoryDataController GetSelectedChildren()
			{
				return null;
			}

			// Token: 0x06004B63 RID: 19299 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062E4")]
			[Address(RVA = "0x1016D3F20", Offset = "0x16D3F20", VA = "0x1016D3F20")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ChildrenParameterType> GetChildFromParameter(ChildrenParameterType parameter)
			{
				return null;
			}

			// Token: 0x06004B64 RID: 19300 RVA: 0x00002052 File Offset: 0x00000252
			[Token(Token = "0x60062E5")]
			[Address(RVA = "0x1016D43D4", Offset = "0x16D43D4", VA = "0x1016D43D4")]
			public ShopTradeTopUI.TradeTopCategoryDataControllerGen<ChildrenParameterType> GetChildFromIndex(int index)
			{
				return null;
			}

			// Token: 0x06004B65 RID: 19301 RVA: 0x0001AF58 File Offset: 0x00019158
			[Token(Token = "0x60062E6")]
			[Address(RVA = "0x1016D4080", Offset = "0x16D4080", VA = "0x1016D4080")]
			public int GetSelectIndex()
			{
				return 0;
			}

			// Token: 0x06004B66 RID: 19302 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062E7")]
			[Address(RVA = "0x1016D4088", Offset = "0x16D4088", VA = "0x1016D4088")]
			protected void SetSelectIndex(int selectIndex)
			{
			}

			// Token: 0x04005D1B RID: 23835
			[Cpp2IlInjected.FieldOffset(Offset = "0x0")]
			[Token(Token = "0x4006F15")]
			private int m_SelectIndex;
		}

		// Token: 0x02000F81 RID: 3969
		[Token(Token = "0x20011E3")]
		private class TradeTopCategoryDataControllerRoot : ShopTradeTopUI.TradeTopCategoryDataControllerNotExistParentAndExistChildren<ShopTradeTopUI.eMainButton>
		{
			// Token: 0x06004B67 RID: 19303 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062E8")]
			[Address(RVA = "0x10156EAE4", Offset = "0x156EAE4", VA = "0x10156EAE4")]
			public void Setup(ShopTradeTopUI.ScrollWrapper scroll, Action<ShopTradeTopUI.eMainButton> childrenAction, ShopTradeTopUI.ScrollWrapper limitTradeScroll, Action<int> limitTradeChildrenAction)
			{
			}

			// Token: 0x06004B68 RID: 19304 RVA: 0x0001AF70 File Offset: 0x00019170
			[Token(Token = "0x60062E9")]
			[Address(RVA = "0x1015704C4", Offset = "0x15704C4", VA = "0x1015704C4", Slot = "4")]
			public override ShopTradeTopUI.ButtonInfoBase.eGenType GetGenType()
			{
				return ShopTradeTopUI.ButtonInfoBase.eGenType.Error;
			}

			// Token: 0x06004B69 RID: 19305 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062EA")]
			[Address(RVA = "0x10156EA94", Offset = "0x156EA94", VA = "0x10156EA94")]
			public TradeTopCategoryDataControllerRoot()
			{
			}
		}

		// Token: 0x02000F82 RID: 3970
		[Token(Token = "0x20011E4")]
		private abstract class TradeTopNoChildrenCategoryDataController<ParameterType> : ShopTradeTopUI.TradeTopCategoryDataControllerGen<ParameterType> where ParameterType : IComparable
		{
			// Token: 0x06004B6A RID: 19306 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062EB")]
			[Address(RVA = "0x1016D4A98", Offset = "0x16D4A98", VA = "0x1016D4A98")]
			protected TradeTopNoChildrenCategoryDataController()
			{
			}

			// Token: 0x06004B6B RID: 19307 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062EC")]
			[Address(RVA = "0x1016D4AD4", Offset = "0x16D4AD4", VA = "0x1016D4AD4")]
			protected void SetupInternal(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent, ShopTradeTopUI.ButtonInfoBaseGen<ParameterType> buttonInfo)
			{
			}
		}

		// Token: 0x02000F83 RID: 3971
		[Token(Token = "0x20011E5")]
		private abstract class TradeTopConstantChildrenCategoryDataController<ParameterType, ChildrenParameterType> : ShopTradeTopUI.TradeTopCategoryDataControllerExistChildren<ParameterType, ChildrenParameterType> where ParameterType : IComparable where ChildrenParameterType : IComparable
		{
			// Token: 0x06004B6C RID: 19308 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062ED")]
			[Address(RVA = "0x1016D48A8", Offset = "0x16D48A8", VA = "0x1016D48A8")]
			protected TradeTopConstantChildrenCategoryDataController()
			{
			}
		}

		// Token: 0x02000F84 RID: 3972
		[Token(Token = "0x20011E6")]
		private abstract class TradeTopVariableChildrenCategoryDataController<ParameterType, ChildrenParameterType, ScrollItemDataType> : ShopTradeTopUI.TradeTopCategoryDataControllerExistChildren<ParameterType, ChildrenParameterType> where ParameterType : IComparable where ChildrenParameterType : IComparable where ScrollItemDataType : ScrollItemData
		{
			// Token: 0x06004B6D RID: 19309 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062EE")]
			[Address(RVA = "0x1016D4C38", Offset = "0x16D4C38", VA = "0x1016D4C38")]
			protected TradeTopVariableChildrenCategoryDataController()
			{
			}
		}

		// Token: 0x02000F85 RID: 3973
		[Token(Token = "0x20011E7")]
		private abstract class TradeTopVariableChildrenMainCategoryDataController<ChildrenParameterType, ScrollItemDataType> : ShopTradeTopUI.TradeTopVariableChildrenCategoryDataController<ShopTradeTopUI.eMainButton, ChildrenParameterType, ScrollItemDataType> where ChildrenParameterType : IComparable where ScrollItemDataType : ScrollItemData
		{
			// Token: 0x06004B6E RID: 19310 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062EF")]
			[Address(RVA = "0x1016D4CEC", Offset = "0x16D4CEC", VA = "0x1016D4CEC")]
			protected TradeTopVariableChildrenMainCategoryDataController()
			{
			}
		}

		// Token: 0x02000F86 RID: 3974
		[Token(Token = "0x20011E8")]
		private abstract class TradeTopNoChildrenMainCategoryDataController : ShopTradeTopUI.TradeTopNoChildrenCategoryDataController<ShopTradeTopUI.eMainButton>
		{
			// Token: 0x06004B6F RID: 19311 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F0")]
			[Address(RVA = "0x101570BEC", Offset = "0x1570BEC", VA = "0x101570BEC")]
			protected TradeTopNoChildrenMainCategoryDataController()
			{
			}
		}

		// Token: 0x02000F87 RID: 3975
		[Token(Token = "0x20011E9")]
		private class TradeTopTradeCategoryDataController : ShopTradeTopUI.TradeTopNoChildrenMainCategoryDataController
		{
			// Token: 0x06004B70 RID: 19312 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F1")]
			[Address(RVA = "0x101570158", Offset = "0x1570158", VA = "0x101570158")]
			public void Setup(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent)
			{
			}

			// Token: 0x06004B71 RID: 19313 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F2")]
			[Address(RVA = "0x101570100", Offset = "0x1570100", VA = "0x101570100")]
			public TradeTopTradeCategoryDataController()
			{
			}
		}

		// Token: 0x02000F88 RID: 3976
		[Token(Token = "0x20011EA")]
		private class TradeTopLimitTradeCategoryDataController : ShopTradeTopUI.TradeTopVariableChildrenMainCategoryDataController<int, QuestCategoryItemData>
		{
			// Token: 0x06004B72 RID: 19314 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F3")]
			[Address(RVA = "0x1015701E8", Offset = "0x15701E8", VA = "0x1015701E8")]
			public void Setup(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent, ShopTradeTopUI.ScrollWrapper scroll, Action<int> childrenAction)
			{
			}

			// Token: 0x06004B73 RID: 19315 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F4")]
			[Address(RVA = "0x101570104", Offset = "0x1570104", VA = "0x101570104")]
			public TradeTopLimitTradeCategoryDataController()
			{
			}
		}

		// Token: 0x02000F89 RID: 3977
		[Token(Token = "0x20011EB")]
		private class TradeTopSaleCategoryDataController : ShopTradeTopUI.TradeTopNoChildrenMainCategoryDataController
		{
			// Token: 0x06004B74 RID: 19316 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F5")]
			[Address(RVA = "0x1015703E4", Offset = "0x15703E4", VA = "0x1015703E4")]
			public void Setup(ShopTradeTopUI.TradeTopCategoryDataControllerBase parent)
			{
			}

			// Token: 0x06004B75 RID: 19317 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F6")]
			[Address(RVA = "0x101570154", Offset = "0x1570154", VA = "0x101570154")]
			public TradeTopSaleCategoryDataController()
			{
			}
		}

		// Token: 0x02000F8A RID: 3978
		[Token(Token = "0x20011EC")]
		private abstract class TradeTopVariableChildrenStandardCategoryDataController<ParameterType, ChildrenParameterType, ScrollItemDataType> : ShopTradeTopUI.TradeTopVariableChildrenCategoryDataController<ParameterType, ChildrenParameterType, ScrollItemDataType> where ParameterType : IComparable where ChildrenParameterType : IComparable where ScrollItemDataType : ScrollItemData
		{
			// Token: 0x06004B76 RID: 19318 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F7")]
			[Address(RVA = "0x1016D4D64", Offset = "0x16D4D64", VA = "0x1016D4D64")]
			protected TradeTopVariableChildrenStandardCategoryDataController()
			{
			}
		}

		// Token: 0x02000F8B RID: 3979
		[Token(Token = "0x20011ED")]
		private abstract class TradeTopNoChildrenStandardCategoryDataController<ParameterType> : ShopTradeTopUI.TradeTopNoChildrenCategoryDataController<ParameterType> where ParameterType : IComparable
		{
			// Token: 0x06004B77 RID: 19319 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F8")]
			[Address(RVA = "0x1016D4BC0", Offset = "0x16D4BC0", VA = "0x1016D4BC0")]
			protected TradeTopNoChildrenStandardCategoryDataController()
			{
			}
		}

		// Token: 0x02000F8C RID: 3980
		[Token(Token = "0x20011EE")]
		private class TradeTopLimitTradeAvailableEventQuestCategoryDataController : ShopTradeTopUI.TradeTopNoChildrenStandardCategoryDataController<int>
		{
			// Token: 0x06004B78 RID: 19320 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062F9")]
			[Address(RVA = "0x1015704CC", Offset = "0x15704CC", VA = "0x1015704CC")]
			public void Setup(Action<int> action, int parameter)
			{
			}

			// Token: 0x06004B79 RID: 19321 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FA")]
			[Address(RVA = "0x101570560", Offset = "0x1570560", VA = "0x101570560")]
			public TradeTopLimitTradeAvailableEventQuestCategoryDataController()
			{
			}
		}

		// Token: 0x02000F8D RID: 3981
		[Token(Token = "0x20011EF")]
		public class ShopTradeTopUISelectData
		{
			// Token: 0x06004B7A RID: 19322 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FB")]
			[Address(RVA = "0x10156F520", Offset = "0x156F520", VA = "0x10156F520")]
			public ShopTradeTopUISelectData()
			{
			}

			// Token: 0x06004B7B RID: 19323 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FC")]
			[Address(RVA = "0x10156FB54", Offset = "0x156FB54", VA = "0x10156FB54")]
			public ShopTradeTopUISelectData(int groupId)
			{
			}

			// Token: 0x06004B7C RID: 19324 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FD")]
			[Address(RVA = "0x10156F528", Offset = "0x156F528", VA = "0x10156F528")]
			public void OnClickMainButton(ShopTradeTopUI.eMainButton mainButton)
			{
			}

			// Token: 0x06004B7D RID: 19325 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FE")]
			[Address(RVA = "0x10156F5D0", Offset = "0x156F5D0", VA = "0x10156F5D0")]
			public void PushParameter(int parameter)
			{
			}

			// Token: 0x06004B7E RID: 19326 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60062FF")]
			[Address(RVA = "0x10156FC0C", Offset = "0x156FC0C", VA = "0x10156FC0C")]
			public void PopParameter()
			{
			}

			// Token: 0x06004B7F RID: 19327 RVA: 0x0001AF88 File Offset: 0x00019188
			[Token(Token = "0x6006300")]
			[Address(RVA = "0x10156E50C", Offset = "0x156E50C", VA = "0x10156E50C")]
			public ShopTradeTopUI.eMainButton GetClickedMainButton()
			{
				return ShopTradeTopUI.eMainButton.Trade;
			}

			// Token: 0x06004B80 RID: 19328 RVA: 0x0001AFA0 File Offset: 0x000191A0
			[Token(Token = "0x6006301")]
			[Address(RVA = "0x10156FCA4", Offset = "0x156FCA4", VA = "0x10156FCA4")]
			public int HowManyParameters()
			{
				return 0;
			}

			// Token: 0x06004B81 RID: 19329 RVA: 0x0001AFB8 File Offset: 0x000191B8
			[Token(Token = "0x6006302")]
			[Address(RVA = "0x10156FD1C", Offset = "0x156FD1C", VA = "0x10156FD1C")]
			public int GetParameter(int cursor)
			{
				return 0;
			}

			// Token: 0x06004B82 RID: 19330 RVA: 0x0001AFD0 File Offset: 0x000191D0
			[Token(Token = "0x6006303")]
			[Address(RVA = "0x10156FB8C", Offset = "0x156FB8C", VA = "0x10156FB8C")]
			private bool AcquireParameterList()
			{
				return default(bool);
			}

			// Token: 0x04005D1C RID: 23836
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006F16")]
			private ShopTradeTopUI.eMainButton m_MainButton;

			// Token: 0x04005D1D RID: 23837
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006F17")]
			private List<int> m_Parameter;
		}

		// Token: 0x02000F8E RID: 3982
		[Token(Token = "0x20011F0")]
		public class TransitLimitTradeData : ShopTradeTopUI.ShopTradeTopUISelectData
		{
			// Token: 0x06004B83 RID: 19331 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006304")]
			[Address(RVA = "0x101570C74", Offset = "0x1570C74", VA = "0x101570C74")]
			public TransitLimitTradeData(int groupId)
			{
			}
		}

		// Token: 0x02000F8F RID: 3983
		[Token(Token = "0x20011F1")]
		public class TransitChestData : ShopTradeTopUI.ShopTradeTopUISelectData
		{
			// Token: 0x06004B84 RID: 19332 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x6006305")]
			[Address(RVA = "0x101570C3C", Offset = "0x1570C3C", VA = "0x101570C3C")]
			public TransitChestData(int chestId)
			{
			}
		}
	}
}
