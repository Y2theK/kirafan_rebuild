﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F90 RID: 3984
	[Token(Token = "0x2000A62")]
	[StructLayout(3)]
	public class ShopWeaponCreateConfirmGroup : UIGroup
	{
		// Token: 0x140000E2 RID: 226
		// (add) Token: 0x06004B85 RID: 19333 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004B86 RID: 19334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E2")]
		public event Action OnCancel
		{
			[Token(Token = "0x6004530")]
			[Address(RVA = "0x101570CAC", Offset = "0x1570CAC", VA = "0x101570CAC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004531")]
			[Address(RVA = "0x101570DBC", Offset = "0x1570DBC", VA = "0x101570DBC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000E3 RID: 227
		// (add) Token: 0x06004B87 RID: 19335 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004B88 RID: 19336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E3")]
		public event Action OnClickExecute
		{
			[Token(Token = "0x6004532")]
			[Address(RVA = "0x101570ECC", Offset = "0x1570ECC", VA = "0x101570ECC")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004533")]
			[Address(RVA = "0x101570FDC", Offset = "0x1570FDC", VA = "0x101570FDC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004B89 RID: 19337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004534")]
		[Address(RVA = "0x1015710EC", Offset = "0x15710EC", VA = "0x1015710EC")]
		public void Setup()
		{
		}

		// Token: 0x06004B8A RID: 19338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004535")]
		[Address(RVA = "0x1015711A8", Offset = "0x15711A8", VA = "0x1015711A8")]
		public void Destroy()
		{
		}

		// Token: 0x06004B8B RID: 19339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004536")]
		[Address(RVA = "0x101571244", Offset = "0x1571244", VA = "0x101571244")]
		public void Open(int recipeID)
		{
		}

		// Token: 0x06004B8C RID: 19340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004537")]
		[Address(RVA = "0x101571274", Offset = "0x1571274", VA = "0x101571274")]
		private void Refresh()
		{
		}

		// Token: 0x06004B8D RID: 19341 RVA: 0x0001AFE8 File Offset: 0x000191E8
		[Token(Token = "0x6004538")]
		[Address(RVA = "0x101571B38", Offset = "0x1571B38", VA = "0x101571B38")]
		private bool IsEnableCreate()
		{
			return default(bool);
		}

		// Token: 0x06004B8E RID: 19342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004539")]
		[Address(RVA = "0x101571B60", Offset = "0x1571B60", VA = "0x101571B60")]
		public void OnClickExecuteButtonCallBack()
		{
		}

		// Token: 0x06004B8F RID: 19343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600453A")]
		[Address(RVA = "0x101571FC8", Offset = "0x1571FC8", VA = "0x101571FC8")]
		public void OnConfirmExec(int btn)
		{
		}

		// Token: 0x06004B90 RID: 19344 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600453B")]
		[Address(RVA = "0x101571FDC", Offset = "0x1571FDC", VA = "0x101571FDC")]
		public void StartEffect()
		{
		}

		// Token: 0x06004B91 RID: 19345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600453C")]
		[Address(RVA = "0x101572378", Offset = "0x1572378", VA = "0x101572378", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004B92 RID: 19346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600453D")]
		[Address(RVA = "0x1015726A8", Offset = "0x15726A8", VA = "0x1015726A8")]
		private void OnFinish(int btn)
		{
		}

		// Token: 0x06004B93 RID: 19347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600453E")]
		[Address(RVA = "0x101572740", Offset = "0x1572740", VA = "0x101572740")]
		public ShopWeaponCreateConfirmGroup()
		{
		}

		// Token: 0x04005D1E RID: 23838
		[Token(Token = "0x4004149")]
		private const float MOVESPAN = 0.1f;

		// Token: 0x04005D1F RID: 23839
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400414A")]
		[SerializeField]
		private WeaponNamePlate m_WeaponNamePlate;

		// Token: 0x04005D20 RID: 23840
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400414B")]
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04005D21 RID: 23841
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400414C")]
		[SerializeField]
		private Text m_CostText;

		// Token: 0x04005D22 RID: 23842
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400414D")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04005D23 RID: 23843
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400414E")]
		[SerializeField]
		private RecipeMaterials m_RecipeMaterials;

		// Token: 0x04005D24 RID: 23844
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400414F")]
		[SerializeField]
		private WeaponMaterialIcon[] m_UseMaterialIcons;

		// Token: 0x04005D25 RID: 23845
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004150")]
		[SerializeField]
		private Text m_AtkText;

		// Token: 0x04005D26 RID: 23846
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004151")]
		[SerializeField]
		private Text m_MgcText;

		// Token: 0x04005D27 RID: 23847
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004152")]
		[SerializeField]
		private Text m_DefText;

		// Token: 0x04005D28 RID: 23848
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004153")]
		[SerializeField]
		private Text m_MDefText;

		// Token: 0x04005D29 RID: 23849
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004154")]
		[SerializeField]
		private SkillInfo m_SkillInfo;

		// Token: 0x04005D2A RID: 23850
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004155")]
		[SerializeField]
		private Text m_GoldAmount;

		// Token: 0x04005D2B RID: 23851
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004156")]
		[SerializeField]
		private CustomButton m_ExecButton;

		// Token: 0x04005D2C RID: 23852
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004157")]
		[SerializeField]
		private MixWpnCreateEffectScene m_MixEffectScene;

		// Token: 0x04005D2D RID: 23853
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004158")]
		private int m_RecipeID;

		// Token: 0x04005D2E RID: 23854
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x4004159")]
		private bool m_IsShortOfGold;

		// Token: 0x04005D2F RID: 23855
		[Cpp2IlInjected.FieldOffset(Offset = "0xFD")]
		[Token(Token = "0x400415A")]
		private bool m_IsShortOfMaterial;

		// Token: 0x04005D30 RID: 23856
		[Cpp2IlInjected.FieldOffset(Offset = "0xFE")]
		[Token(Token = "0x400415B")]
		private bool m_IsLimitWeapon;

		// Token: 0x04005D31 RID: 23857
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x400415C")]
		private ShopWeaponCreateConfirmGroup.eEffectStep m_Step;

		// Token: 0x02000F91 RID: 3985
		[Token(Token = "0x20011F2")]
		private enum eEffectStep
		{
			// Token: 0x04005D35 RID: 23861
			[Token(Token = "0x4006F19")]
			None,
			// Token: 0x04005D36 RID: 23862
			[Token(Token = "0x4006F1A")]
			WaitPrepare,
			// Token: 0x04005D37 RID: 23863
			[Token(Token = "0x4006F1B")]
			Move,
			// Token: 0x04005D38 RID: 23864
			[Token(Token = "0x4006F1C")]
			FinalEffect,
			// Token: 0x04005D39 RID: 23865
			[Token(Token = "0x4006F1D")]
			WaitFinalEffect
		}
	}
}
