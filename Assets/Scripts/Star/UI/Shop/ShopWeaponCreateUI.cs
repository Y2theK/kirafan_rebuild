﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F92 RID: 3986
	[Token(Token = "0x2000A63")]
	[StructLayout(3)]
	public class ShopWeaponCreateUI : MenuUIBase
	{
		// Token: 0x140000E4 RID: 228
		// (add) Token: 0x06004B94 RID: 19348 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004B95 RID: 19349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E4")]
		public event Action<int> OnClickExecuteButton
		{
			[Token(Token = "0x600453F")]
			[Address(RVA = "0x101572750", Offset = "0x1572750", VA = "0x101572750")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004540")]
			[Address(RVA = "0x10157285C", Offset = "0x157285C", VA = "0x10157285C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004B96 RID: 19350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004541")]
		[Address(RVA = "0x101572968", Offset = "0x1572968", VA = "0x101572968")]
		private void Start()
		{
		}

		// Token: 0x06004B97 RID: 19351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004542")]
		[Address(RVA = "0x10157296C", Offset = "0x157296C", VA = "0x10157296C")]
		public void Setup()
		{
		}

		// Token: 0x06004B98 RID: 19352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004543")]
		[Address(RVA = "0x101572C60", Offset = "0x1572C60", VA = "0x101572C60", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004B99 RID: 19353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004544")]
		[Address(RVA = "0x101572C58", Offset = "0x1572C58", VA = "0x101572C58")]
		public void ChangeTab(int idx)
		{
		}

		// Token: 0x06004B9A RID: 19354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004545")]
		[Address(RVA = "0x101572C90", Offset = "0x1572C90", VA = "0x101572C90")]
		private void Listup(eClassType classType)
		{
		}

		// Token: 0x06004B9B RID: 19355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004546")]
		[Address(RVA = "0x10157311C", Offset = "0x157311C", VA = "0x10157311C")]
		private void Update()
		{
		}

		// Token: 0x06004B9C RID: 19356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004547")]
		[Address(RVA = "0x101573330", Offset = "0x1573330", VA = "0x101573330")]
		private void ChangeStep(ShopWeaponCreateUI.eStep step)
		{
		}

		// Token: 0x06004B9D RID: 19357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004548")]
		[Address(RVA = "0x1015735B4", Offset = "0x15735B4", VA = "0x1015735B4", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004B9E RID: 19358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004549")]
		[Address(RVA = "0x101573654", Offset = "0x1573654", VA = "0x101573654", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004B9F RID: 19359 RVA: 0x0001B000 File Offset: 0x00019200
		[Token(Token = "0x600454A")]
		[Address(RVA = "0x1015736F4", Offset = "0x15736F4", VA = "0x1015736F4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004BA0 RID: 19360 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600454B")]
		[Address(RVA = "0x101573704", Offset = "0x1573704", VA = "0x101573704")]
		public void OnClickRecipeCallBack(int recipeID)
		{
		}

		// Token: 0x06004BA1 RID: 19361 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600454C")]
		[Address(RVA = "0x10157388C", Offset = "0x157388C", VA = "0x10157388C")]
		public void OnClickExecuteButtonCallBack()
		{
		}

		// Token: 0x06004BA2 RID: 19362 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600454D")]
		[Address(RVA = "0x1015738E0", Offset = "0x15738E0", VA = "0x1015738E0")]
		public void StartCreateEffect()
		{
		}

		// Token: 0x06004BA3 RID: 19363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600454E")]
		[Address(RVA = "0x101573910", Offset = "0x1573910", VA = "0x101573910")]
		public void OnBackConfirmCallBack(bool shortcut)
		{
		}

		// Token: 0x06004BA4 RID: 19364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600454F")]
		[Address(RVA = "0x101573988", Offset = "0x1573988", VA = "0x101573988")]
		public void CloseConfirm()
		{
		}

		// Token: 0x06004BA5 RID: 19365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004550")]
		[Address(RVA = "0x101573AA0", Offset = "0x1573AA0", VA = "0x101573AA0")]
		public ShopWeaponCreateUI()
		{
		}

		// Token: 0x04005D3A RID: 23866
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400415F")]
		private ShopWeaponCreateUI.eStep m_Step;

		// Token: 0x04005D3B RID: 23867
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004160")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005D3C RID: 23868
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004161")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005D3D RID: 23869
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004162")]
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04005D3E RID: 23870
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004163")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04005D3F RID: 23871
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004164")]
		[SerializeField]
		private ShopWeaponCreateConfirmGroup m_ConfirmGroup;

		// Token: 0x04005D40 RID: 23872
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004165")]
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x04005D41 RID: 23873
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004166")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04005D42 RID: 23874
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004167")]
		private int m_RecipeID;

		// Token: 0x04005D43 RID: 23875
		[Cpp2IlInjected.FieldOffset(Offset = "0x84")]
		[Token(Token = "0x4004168")]
		private int m_CurrentTab;

		// Token: 0x02000F93 RID: 3987
		[Token(Token = "0x20011F3")]
		private enum eStep
		{
			// Token: 0x04005D46 RID: 23878
			[Token(Token = "0x4006F1F")]
			Hide,
			// Token: 0x04005D47 RID: 23879
			[Token(Token = "0x4006F20")]
			In,
			// Token: 0x04005D48 RID: 23880
			[Token(Token = "0x4006F21")]
			Idle,
			// Token: 0x04005D49 RID: 23881
			[Token(Token = "0x4006F22")]
			Out,
			// Token: 0x04005D4A RID: 23882
			[Token(Token = "0x4006F23")]
			End
		}
	}
}
