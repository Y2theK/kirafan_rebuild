﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F94 RID: 3988
	[Token(Token = "0x2000A64")]
	[StructLayout(3)]
	public class ShopWeaponEvolutionGroup : UIGroup
	{
		// Token: 0x06004BA6 RID: 19366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004551")]
		[Address(RVA = "0x101573AB0", Offset = "0x1573AB0", VA = "0x101573AB0")]
		public void Destroy()
		{
		}

		// Token: 0x06004BA7 RID: 19367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004552")]
		[Address(RVA = "0x101573AE8", Offset = "0x1573AE8", VA = "0x101573AE8")]
		public void CleanUp()
		{
		}

		// Token: 0x06004BA8 RID: 19368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004553")]
		[Address(RVA = "0x101573CC8", Offset = "0x1573CC8", VA = "0x101573CC8")]
		public void Setup(long weaponMngID)
		{
		}

		// Token: 0x06004BA9 RID: 19369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004554")]
		[Address(RVA = "0x1015759A0", Offset = "0x15759A0", VA = "0x1015759A0", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004BAA RID: 19370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004555")]
		[Address(RVA = "0x101575EE4", Offset = "0x1575EE4", VA = "0x101575EE4")]
		public void OnClickEvolutionButton()
		{
		}

		// Token: 0x06004BAB RID: 19371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004556")]
		[Address(RVA = "0x101575F3C", Offset = "0x1575F3C", VA = "0x101575F3C")]
		public void EnaleDecdieButton(bool interactable = true)
		{
		}

		// Token: 0x06004BAC RID: 19372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004557")]
		[Address(RVA = "0x101576068", Offset = "0x1576068", VA = "0x101576068")]
		public void DisableDecdieButton()
		{
		}

		// Token: 0x06004BAD RID: 19373 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004558")]
		[Address(RVA = "0x101574794", Offset = "0x1574794", VA = "0x101574794")]
		private void MakeLevelInfo(int weaponNowLv, int weaponMaxLv, int weaponEvoMaxLv, int skillNowLv, int skillMaxLv, int skillEvoMaxLevel)
		{
		}

		// Token: 0x06004BAE RID: 19374 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004559")]
		[Address(RVA = "0x101575048", Offset = "0x1575048", VA = "0x101575048")]
		private void MakeScrollPanel(UserWeaponData userWeaponData, WeaponListDB_Param weaponParam, WeaponListDB_Param evolutionWeaponParam)
		{
		}

		// Token: 0x06004BAF RID: 19375 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600455A")]
		[Address(RVA = "0x10157634C", Offset = "0x157634C", VA = "0x10157634C")]
		private void AddStatus(int[] status, int[] add)
		{
		}

		// Token: 0x06004BB0 RID: 19376 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600455B")]
		[Address(RVA = "0x10157651C", Offset = "0x157651C", VA = "0x10157651C")]
		private SkillInfo AddActiveSkill(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, long remainExp = -1L, bool changeLevelColor = false)
		{
			return null;
		}

		// Token: 0x06004BB1 RID: 19377 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600455C")]
		[Address(RVA = "0x1015769D4", Offset = "0x15769D4", VA = "0x1015769D4")]
		private SkillInfo AddPassiveSkill(int skillID, eElementType ownerElement)
		{
			return null;
		}

		// Token: 0x06004BB2 RID: 19378 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600455D")]
		[Address(RVA = "0x101576AE0", Offset = "0x1576AE0", VA = "0x101576AE0")]
		public SkillInfo AddPassiveSkillLocked(int evolutionCount)
		{
			return null;
		}

		// Token: 0x06004BB3 RID: 19379 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600455E")]
		[Address(RVA = "0x101576C5C", Offset = "0x1576C5C", VA = "0x101576C5C")]
		public SkillInfo AddNoSkill()
		{
			return null;
		}

		// Token: 0x06004BB4 RID: 19380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600455F")]
		[Address(RVA = "0x101576298", Offset = "0x1576298", VA = "0x101576298")]
		private void AddSeparatorWeaponEffect()
		{
		}

		// Token: 0x06004BB5 RID: 19381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004560")]
		[Address(RVA = "0x101576468", Offset = "0x1576468", VA = "0x101576468")]
		private void AddSeparatorWeaponSkill()
		{
		}

		// Token: 0x06004BB6 RID: 19382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004561")]
		[Address(RVA = "0x101576920", Offset = "0x1576920", VA = "0x101576920")]
		private void AddSeparatorPassiveSkill()
		{
		}

		// Token: 0x06004BB7 RID: 19383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004562")]
		[Address(RVA = "0x101576670", Offset = "0x1576670", VA = "0x101576670")]
		private void AddSeparatorArrow()
		{
		}

		// Token: 0x06004BB8 RID: 19384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004563")]
		[Address(RVA = "0x101576724", Offset = "0x1576724", VA = "0x101576724")]
		private void AttachNewBadge(SkillInfo skillInfo)
		{
		}

		// Token: 0x06004BB9 RID: 19385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004564")]
		[Address(RVA = "0x101576EC0", Offset = "0x1576EC0", VA = "0x101576EC0")]
		public void StartEffect()
		{
		}

		// Token: 0x06004BBA RID: 19386 RVA: 0x0001B018 File Offset: 0x00019218
		[Token(Token = "0x6004565")]
		[Address(RVA = "0x101576ECC", Offset = "0x1576ECC", VA = "0x101576ECC")]
		public bool IsPlayingEffect()
		{
			return default(bool);
		}

		// Token: 0x06004BBB RID: 19387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004566")]
		[Address(RVA = "0x1015759C8", Offset = "0x15759C8", VA = "0x1015759C8")]
		private void UpdateEffect()
		{
		}

		// Token: 0x06004BBC RID: 19388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004567")]
		[Address(RVA = "0x10157751C", Offset = "0x157751C", VA = "0x10157751C")]
		public ShopWeaponEvolutionGroup()
		{
		}

		// Token: 0x04005D4B RID: 23883
		[Token(Token = "0x400416A")]
		private const float MOVESPAN = 0.1f;

		// Token: 0x04005D4C RID: 23884
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400416B")]
		[SerializeField]
		private WeaponNamePlate m_WeaponNamePlate;

		// Token: 0x04005D4D RID: 23885
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400416C")]
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04005D4E RID: 23886
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400416D")]
		[SerializeField]
		private Text m_Price;

		// Token: 0x04005D4F RID: 23887
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400416E")]
		[SerializeField]
		private ContentSizeFitter m_WeaponLevelFitter;

		// Token: 0x04005D50 RID: 23888
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400416F")]
		[SerializeField]
		private Text m_WeaponLevel;

		// Token: 0x04005D51 RID: 23889
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004170")]
		[SerializeField]
		private Text m_BeforeLevel;

		// Token: 0x04005D52 RID: 23890
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004171")]
		[SerializeField]
		private Text m_AfterLevel;

		// Token: 0x04005D53 RID: 23891
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004172")]
		[SerializeField]
		private ContentSizeFitter m_WeaponSkillLevelFitter;

		// Token: 0x04005D54 RID: 23892
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004173")]
		[SerializeField]
		private Text m_WeaponSkillLevel;

		// Token: 0x04005D55 RID: 23893
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004174")]
		[SerializeField]
		private Text m_BeforeSkillLevel;

		// Token: 0x04005D56 RID: 23894
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004175")]
		[SerializeField]
		private Text m_AfterSkillLevel;

		// Token: 0x04005D57 RID: 23895
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004176")]
		[SerializeField]
		private ContainerScrollRect m_ContainerScroll;

		// Token: 0x04005D58 RID: 23896
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x4004177")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04005D59 RID: 23897
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x4004178")]
		[SerializeField]
		private Image m_DecideButtonOKImage;

		// Token: 0x04005D5A RID: 23898
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x4004179")]
		[SerializeField]
		private Text m_DecideButtonNGText;

		// Token: 0x04005D5B RID: 23899
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x400417A")]
		[SerializeField]
		private Text m_DecideButtonWarningText;

		// Token: 0x04005D5C RID: 23900
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x400417B")]
		[SerializeField]
		private RecipeMaterials m_RecipeMaterials;

		// Token: 0x04005D5D RID: 23901
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x400417C")]
		[SerializeField]
		private WeaponMaterialIcon[] m_UseMaterialIcons;

		// Token: 0x04005D5E RID: 23902
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x400417D")]
		[SerializeField]
		private MixWpnUpgradeEffectScene m_MixEffectScene;

		// Token: 0x04005D5F RID: 23903
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x400417E")]
		[SerializeField]
		private PrefabCloner m_UpgradePopUpCloner;

		// Token: 0x04005D60 RID: 23904
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x400417F")]
		[SerializeField]
		private WeaponStatus m_WeaponStatusPrefab;

		// Token: 0x04005D61 RID: 23905
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004180")]
		[SerializeField]
		private SkillInfo m_SkillInfoPrefab;

		// Token: 0x04005D62 RID: 23906
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004181")]
		[SerializeField]
		private GameObject m_SeparatorWeaponEffectPrefab;

		// Token: 0x04005D63 RID: 23907
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004182")]
		[SerializeField]
		private GameObject m_SeparatorWeaponSkillPrefab;

		// Token: 0x04005D64 RID: 23908
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4004183")]
		[SerializeField]
		private GameObject m_SeparatorWeaponSkillAutoPrefab;

		// Token: 0x04005D65 RID: 23909
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x4004184")]
		[SerializeField]
		private GameObject m_SeparatorArrowPrefab;

		// Token: 0x04005D66 RID: 23910
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x4004185")]
		[SerializeField]
		private Badge m_NewBadgePrefab;

		// Token: 0x04005D67 RID: 23911
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x4004186")]
		private Vector2 m_BadgePosition;

		// Token: 0x04005D68 RID: 23912
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x4004187")]
		private ShopWeaponEvolutionGroup.eEffectStep m_Step;

		// Token: 0x04005D69 RID: 23913
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x4004188")]
		private List<Badge> m_NewBadgeList;

		// Token: 0x04005D6A RID: 23914
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4004189")]
		private WeaponEvolutionListDB_Param? m_EvolutionParam;

		// Token: 0x04005D6B RID: 23915
		[Cpp2IlInjected.FieldOffset(Offset = "0x1A8")]
		[Token(Token = "0x400418A")]
		private long m_MngID;

		// Token: 0x04005D6C RID: 23916
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B0")]
		[Token(Token = "0x400418B")]
		private int m_RecipeID;

		// Token: 0x04005D6D RID: 23917
		[Cpp2IlInjected.FieldOffset(Offset = "0x1B8")]
		[Token(Token = "0x400418C")]
		public Action<long, int> OnExecute;

		// Token: 0x02000F95 RID: 3989
		[Token(Token = "0x20011F4")]
		private enum eEffectStep
		{
			// Token: 0x04005D6F RID: 23919
			[Token(Token = "0x4006F25")]
			None,
			// Token: 0x04005D70 RID: 23920
			[Token(Token = "0x4006F26")]
			Init,
			// Token: 0x04005D71 RID: 23921
			[Token(Token = "0x4006F27")]
			WaitInit,
			// Token: 0x04005D72 RID: 23922
			[Token(Token = "0x4006F28")]
			WaitMove,
			// Token: 0x04005D73 RID: 23923
			[Token(Token = "0x4006F29")]
			PlayEffect,
			// Token: 0x04005D74 RID: 23924
			[Token(Token = "0x4006F2A")]
			WaitEffect,
			// Token: 0x04005D75 RID: 23925
			[Token(Token = "0x4006F2B")]
			Finish
		}

		// Token: 0x02000F96 RID: 3990
		[Token(Token = "0x20011F5")]
		public enum eSkillInfoType
		{
			// Token: 0x04005D77 RID: 23927
			[Token(Token = "0x4006F2D")]
			ActiveSkill,
			// Token: 0x04005D78 RID: 23928
			[Token(Token = "0x4006F2E")]
			PassiveSkill,
			// Token: 0x04005D79 RID: 23929
			[Token(Token = "0x4006F2F")]
			PassiveSkillLocked
		}
	}
}
