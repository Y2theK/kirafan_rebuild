﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000F97 RID: 3991
	[Token(Token = "0x2000A65")]
	[StructLayout(3)]
	public class ShopWeaponEvolutionUI : MenuUIBase
	{
		// Token: 0x140000E5 RID: 229
		// (add) Token: 0x06004BBD RID: 19389 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004BBE RID: 19390 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E5")]
		public event Action<long, int> OnClickExecuteButton
		{
			[Token(Token = "0x6004568")]
			[Address(RVA = "0x1015775C0", Offset = "0x15775C0", VA = "0x1015775C0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004569")]
			[Address(RVA = "0x1015776CC", Offset = "0x15776CC", VA = "0x1015776CC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004BBF RID: 19391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456A")]
		[Address(RVA = "0x1015777D8", Offset = "0x15777D8", VA = "0x1015777D8")]
		private void Start()
		{
		}

		// Token: 0x06004BC0 RID: 19392 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456B")]
		[Address(RVA = "0x1015777DC", Offset = "0x15777DC", VA = "0x1015777DC")]
		public void Setup(long mngID)
		{
		}

		// Token: 0x06004BC1 RID: 19393 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456C")]
		[Address(RVA = "0x1015778AC", Offset = "0x15778AC", VA = "0x1015778AC")]
		public void Refresh()
		{
		}

		// Token: 0x06004BC2 RID: 19394 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456D")]
		[Address(RVA = "0x101577B04", Offset = "0x1577B04", VA = "0x101577B04", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004BC3 RID: 19395 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456E")]
		[Address(RVA = "0x101577B30", Offset = "0x1577B30", VA = "0x101577B30")]
		private void Update()
		{
		}

		// Token: 0x06004BC4 RID: 19396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600456F")]
		[Address(RVA = "0x101577D00", Offset = "0x1577D00", VA = "0x101577D00")]
		private void ChangeStep(ShopWeaponEvolutionUI.eStep step)
		{
		}

		// Token: 0x06004BC5 RID: 19397 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004570")]
		[Address(RVA = "0x101577EBC", Offset = "0x1577EBC", VA = "0x101577EBC", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004BC6 RID: 19398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004571")]
		[Address(RVA = "0x101577F5C", Offset = "0x1577F5C", VA = "0x101577F5C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004BC7 RID: 19399 RVA: 0x0001B030 File Offset: 0x00019230
		[Token(Token = "0x6004572")]
		[Address(RVA = "0x101578018", Offset = "0x1578018", VA = "0x101578018", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004BC8 RID: 19400 RVA: 0x0001B048 File Offset: 0x00019248
		[Token(Token = "0x6004573")]
		[Address(RVA = "0x101578028", Offset = "0x1578028", VA = "0x101578028")]
		public bool IsIdle()
		{
			return default(bool);
		}

		// Token: 0x06004BC9 RID: 19401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004574")]
		[Address(RVA = "0x101578038", Offset = "0x1578038", VA = "0x101578038")]
		private void OnClickExecuteButtonCallBack(long managedID, int recipeID)
		{
		}

		// Token: 0x06004BCA RID: 19402 RVA: 0x0001B060 File Offset: 0x00019260
		[Token(Token = "0x6004575")]
		[Address(RVA = "0x1015779D8", Offset = "0x15779D8", VA = "0x1015779D8")]
		private ShopWeaponEvolutionUI.EvolutionFailCondition CheckEvolutionCondition(long weaponMngID)
		{
			return ShopWeaponEvolutionUI.EvolutionFailCondition.NONE;
		}

		// Token: 0x06004BCB RID: 19403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004576")]
		[Address(RVA = "0x10157830C", Offset = "0x157830C", VA = "0x10157830C")]
		private void OpenEvolutionConditionWindow()
		{
		}

		// Token: 0x06004BCC RID: 19404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004577")]
		[Address(RVA = "0x10157862C", Offset = "0x157862C", VA = "0x10157862C")]
		public void StartEvolutionEffect()
		{
		}

		// Token: 0x06004BCD RID: 19405 RVA: 0x0001B078 File Offset: 0x00019278
		[Token(Token = "0x6004578")]
		[Address(RVA = "0x10157865C", Offset = "0x157865C", VA = "0x10157865C")]
		public bool IsPlayingEvolutionEffect()
		{
			return default(bool);
		}

		// Token: 0x06004BCE RID: 19406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004579")]
		[Address(RVA = "0x10157869C", Offset = "0x157869C", VA = "0x10157869C")]
		public void Lock()
		{
		}

		// Token: 0x06004BCF RID: 19407 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600457A")]
		[Address(RVA = "0x10157878C", Offset = "0x157878C", VA = "0x10157878C")]
		public void Unlock()
		{
		}

		// Token: 0x06004BD0 RID: 19408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600457B")]
		[Address(RVA = "0x101578820", Offset = "0x1578820", VA = "0x101578820")]
		public void UnlockInput()
		{
		}

		// Token: 0x06004BD1 RID: 19409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600457C")]
		[Address(RVA = "0x1015788B8", Offset = "0x15788B8", VA = "0x1015788B8")]
		public ShopWeaponEvolutionUI()
		{
		}

		// Token: 0x04005D7A RID: 23930
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x400418D")]
		private ShopWeaponEvolutionUI.eStep m_Step;

		// Token: 0x04005D7B RID: 23931
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x400418E")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005D7C RID: 23932
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x400418F")]
		[SerializeField]
		private ShopWeaponEvolutionGroup m_ConfirmGroup;

		// Token: 0x04005D7D RID: 23933
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004190")]
		private long m_MngID;

		// Token: 0x04005D7F RID: 23935
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004192")]
		private ShopWeaponEvolutionUI.EvolutionFailCondition m_EvolutionConditionFlag;

		// Token: 0x02000F98 RID: 3992
		[Token(Token = "0x20011F6")]
		[Flags]
		private enum EvolutionFailCondition
		{
			// Token: 0x04005D81 RID: 23937
			[Token(Token = "0x4006F31")]
			NONE = 0,
			// Token: 0x04005D82 RID: 23938
			[Token(Token = "0x4006F32")]
			SHORTAGE_COST = 1,
			// Token: 0x04005D83 RID: 23939
			[Token(Token = "0x4006F33")]
			NOT_LEVEL_MAX = 2,
			// Token: 0x04005D84 RID: 23940
			[Token(Token = "0x4006F34")]
			NOT_SKILL_LEVEL_MAX = 4,
			// Token: 0x04005D85 RID: 23941
			[Token(Token = "0x4006F35")]
			SHORTAGE_MATERIAL = 8,
			// Token: 0x04005D86 RID: 23942
			[Token(Token = "0x4006F36")]
			IMPOSSIBLE = 128
		}

		// Token: 0x02000F99 RID: 3993
		[Token(Token = "0x20011F7")]
		private enum eStep
		{
			// Token: 0x04005D88 RID: 23944
			[Token(Token = "0x4006F38")]
			Hide,
			// Token: 0x04005D89 RID: 23945
			[Token(Token = "0x4006F39")]
			In,
			// Token: 0x04005D8A RID: 23946
			[Token(Token = "0x4006F3A")]
			Idle,
			// Token: 0x04005D8B RID: 23947
			[Token(Token = "0x4006F3B")]
			Out,
			// Token: 0x04005D8C RID: 23948
			[Token(Token = "0x4006F3C")]
			End
		}
	}
}
