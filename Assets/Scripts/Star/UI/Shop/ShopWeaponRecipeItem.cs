﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F9B RID: 3995
	[Token(Token = "0x2000A66")]
	[StructLayout(3)]
	public class ShopWeaponRecipeItem : ScrollItemIcon
	{
		// Token: 0x06004BD4 RID: 19412 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600457D")]
		[Address(RVA = "0x101578948", Offset = "0x1578948", VA = "0x101578948", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004BD5 RID: 19413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600457E")]
		[Address(RVA = "0x101578F70", Offset = "0x1578F70", VA = "0x101578F70")]
		public ShopWeaponRecipeItem()
		{
		}

		// Token: 0x04005D90 RID: 23952
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4004193")]
		[SerializeField]
		private PrefabCloner m_WeaponIconPrefabCloner;

		// Token: 0x04005D91 RID: 23953
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4004194")]
		[SerializeField]
		private ColorGroup m_CantExecCG;

		// Token: 0x04005D92 RID: 23954
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4004195")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04005D93 RID: 23955
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4004196")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04005D94 RID: 23956
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4004197")]
		[SerializeField]
		private Text m_AmountText;

		// Token: 0x04005D95 RID: 23957
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4004198")]
		private bool m_CantExec;
	}
}
