﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000F9C RID: 3996
	[Token(Token = "0x2000A67")]
	[StructLayout(3)]
	public class ShopWeaponRecipeItemData : ScrollItemData
	{
		// Token: 0x140000E6 RID: 230
		// (add) Token: 0x06004BD6 RID: 19414 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004BD7 RID: 19415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E6")]
		public event Action<int> OnClickRecipe
		{
			[Token(Token = "0x600457F")]
			[Address(RVA = "0x101573010", Offset = "0x1573010", VA = "0x101573010")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004580")]
			[Address(RVA = "0x101578F78", Offset = "0x1578F78", VA = "0x101578F78")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004BD8 RID: 19416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004581")]
		[Address(RVA = "0x101572EF0", Offset = "0x1572EF0", VA = "0x101572EF0")]
		public ShopWeaponRecipeItemData(int recipeID)
		{
		}

		// Token: 0x06004BD9 RID: 19417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004582")]
		[Address(RVA = "0x101579084", Offset = "0x1579084", VA = "0x101579084", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005D96 RID: 23958
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004199")]
		public int m_RecipeID;

		// Token: 0x04005D97 RID: 23959
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400419A")]
		public int m_Amount;

		// Token: 0x04005D98 RID: 23960
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400419B")]
		public int m_WpnID;

		// Token: 0x04005D99 RID: 23961
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400419C")]
		public string m_WpnName;

		// Token: 0x04005D9A RID: 23962
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400419D")]
		public eRare m_Rare;
	}
}
