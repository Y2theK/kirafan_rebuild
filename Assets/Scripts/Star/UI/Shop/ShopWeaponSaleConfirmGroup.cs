﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F9D RID: 3997
	[Token(Token = "0x2000A68")]
	[StructLayout(3)]
	public class ShopWeaponSaleConfirmGroup : UIGroup
	{
		// Token: 0x140000E7 RID: 231
		// (add) Token: 0x06004BDA RID: 19418 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004BDB RID: 19419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E7")]
		public event Action<List<long>> OnExecute
		{
			[Token(Token = "0x6004583")]
			[Address(RVA = "0x1015790D8", Offset = "0x15790D8", VA = "0x1015790D8")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004584")]
			[Address(RVA = "0x1015791E4", Offset = "0x15791E4", VA = "0x1015791E4")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x140000E8 RID: 232
		// (add) Token: 0x06004BDC RID: 19420 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004BDD RID: 19421 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E8")]
		public event Action OnCancel
		{
			[Token(Token = "0x6004585")]
			[Address(RVA = "0x1015792F0", Offset = "0x15792F0", VA = "0x1015792F0")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004586")]
			[Address(RVA = "0x1015793FC", Offset = "0x15793FC", VA = "0x1015793FC")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004BDE RID: 19422 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004587")]
		[Address(RVA = "0x101579508", Offset = "0x1579508", VA = "0x101579508")]
		public void Open(List<long> weaponMngIDs)
		{
		}

		// Token: 0x06004BDF RID: 19423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004588")]
		[Address(RVA = "0x1015798D0", Offset = "0x15798D0", VA = "0x1015798D0")]
		public void OnClickExecuteButtonCallBack()
		{
		}

		// Token: 0x06004BE0 RID: 19424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004589")]
		[Address(RVA = "0x101579D44", Offset = "0x1579D44", VA = "0x101579D44")]
		private void OnConfirmRareWeapon(int btn)
		{
		}

		// Token: 0x06004BE1 RID: 19425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600458A")]
		[Address(RVA = "0x101579DB8", Offset = "0x1579DB8", VA = "0x101579DB8")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004BE2 RID: 19426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600458B")]
		[Address(RVA = "0x101579DC4", Offset = "0x1579DC4", VA = "0x101579DC4")]
		public ShopWeaponSaleConfirmGroup()
		{
		}

		// Token: 0x04005D9C RID: 23964
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400419F")]
		[SerializeField]
		private RectTransform m_IconParent;

		// Token: 0x04005D9D RID: 23965
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40041A0")]
		[SerializeField]
		private WeaponIconWithFrame m_WeaponIconPrefab;

		// Token: 0x04005D9E RID: 23966
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40041A1")]
		private List<WeaponIconWithFrame> m_IconList;

		// Token: 0x04005DA1 RID: 23969
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40041A4")]
		public Text m_GetAmountText;

		// Token: 0x04005DA2 RID: 23970
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40041A5")]
		private List<long> m_WeaponMngIDs;
	}
}
