﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000F9E RID: 3998
	[Token(Token = "0x2000A69")]
	[StructLayout(3)]
	public class ShopWeaponSaleUI : MenuUIBase
	{
		// Token: 0x140000E9 RID: 233
		// (add) Token: 0x06004BE3 RID: 19427 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004BE4 RID: 19428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000E9")]
		public event Action<List<long>> OnClickExecuteButton
		{
			[Token(Token = "0x600458C")]
			[Address(RVA = "0x101579E34", Offset = "0x1579E34", VA = "0x101579E34")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x600458D")]
			[Address(RVA = "0x101579F40", Offset = "0x1579F40", VA = "0x101579F40")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004BE5 RID: 19429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600458E")]
		[Address(RVA = "0x10157A04C", Offset = "0x157A04C", VA = "0x10157A04C")]
		private void Start()
		{
		}

		// Token: 0x06004BE6 RID: 19430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600458F")]
		[Address(RVA = "0x10157A050", Offset = "0x157A050", VA = "0x10157A050")]
		public void Setup()
		{
		}

		// Token: 0x06004BE7 RID: 19431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004590")]
		[Address(RVA = "0x10157A1A8", Offset = "0x157A1A8", VA = "0x10157A1A8")]
		public void CloseConfirm()
		{
		}

		// Token: 0x06004BE8 RID: 19432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004591")]
		[Address(RVA = "0x10157A1DC", Offset = "0x157A1DC", VA = "0x10157A1DC")]
		private void Update()
		{
		}

		// Token: 0x06004BE9 RID: 19433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004592")]
		[Address(RVA = "0x10157A3E0", Offset = "0x157A3E0", VA = "0x10157A3E0")]
		private void ChangeStep(ShopWeaponSaleUI.eStep step)
		{
		}

		// Token: 0x06004BEA RID: 19434 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004593")]
		[Address(RVA = "0x10157A614", Offset = "0x157A614", VA = "0x10157A614")]
		public void PlayIn(List<long> list)
		{
		}

		// Token: 0x06004BEB RID: 19435 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004594")]
		[Address(RVA = "0x10157A6D0", Offset = "0x157A6D0", VA = "0x10157A6D0", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004BEC RID: 19436 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004595")]
		[Address(RVA = "0x10157A770", Offset = "0x157A770", VA = "0x10157A770", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004BED RID: 19437 RVA: 0x0001B090 File Offset: 0x00019290
		[Token(Token = "0x6004596")]
		[Address(RVA = "0x10157A85C", Offset = "0x157A85C", VA = "0x10157A85C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004BEE RID: 19438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004597")]
		[Address(RVA = "0x10157A86C", Offset = "0x157A86C", VA = "0x10157A86C")]
		public void OnClickExecuteButtonCallBack(List<long> mngIDs)
		{
		}

		// Token: 0x06004BEF RID: 19439 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004598")]
		[Address(RVA = "0x10157A8CC", Offset = "0x157A8CC", VA = "0x10157A8CC")]
		public ShopWeaponSaleUI()
		{
		}

		// Token: 0x04005DA3 RID: 23971
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041A6")]
		public int MAX_SELECT;

		// Token: 0x04005DA4 RID: 23972
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x40041A7")]
		private ShopWeaponSaleUI.eStep m_Step;

		// Token: 0x04005DA5 RID: 23973
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041A8")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005DA6 RID: 23974
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041A9")]
		[SerializeField]
		private ShopWeaponSaleConfirmGroup m_ConfirmGroup;

		// Token: 0x04005DA7 RID: 23975
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041AA")]
		[SerializeField]
		private Text m_SaleAmountText;

		// Token: 0x04005DA8 RID: 23976
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40041AB")]
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x02000F9F RID: 3999
		[Token(Token = "0x20011F9")]
		private enum eStep
		{
			// Token: 0x04005DAB RID: 23979
			[Token(Token = "0x4006F41")]
			Hide,
			// Token: 0x04005DAC RID: 23980
			[Token(Token = "0x4006F42")]
			In,
			// Token: 0x04005DAD RID: 23981
			[Token(Token = "0x4006F43")]
			Idle,
			// Token: 0x04005DAE RID: 23982
			[Token(Token = "0x4006F44")]
			Out,
			// Token: 0x04005DAF RID: 23983
			[Token(Token = "0x4006F45")]
			End
		}
	}
}
