﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000FA0 RID: 4000
	[Token(Token = "0x2000A6A")]
	[StructLayout(3)]
	public class ShopWeaponSkillLvUPGroup : UIGroup
	{
		// Token: 0x06004BF0 RID: 19440 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004599")]
		[Address(RVA = "0x10157A8DC", Offset = "0x157A8DC", VA = "0x10157A8DC")]
		public void Setup(long wepMngID, int itemID)
		{
		}

		// Token: 0x06004BF1 RID: 19441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600459A")]
		[Address(RVA = "0x10157B020", Offset = "0x157B020", VA = "0x10157B020")]
		public void Destroy()
		{
		}

		// Token: 0x06004BF2 RID: 19442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600459B")]
		[Address(RVA = "0x10157ADD0", Offset = "0x157ADD0", VA = "0x10157ADD0")]
		public void UpdateData()
		{
		}

		// Token: 0x06004BF3 RID: 19443 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600459C")]
		[Address(RVA = "0x10157B8EC", Offset = "0x157B8EC", VA = "0x10157B8EC", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004BF4 RID: 19444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600459D")]
		[Address(RVA = "0x10157B914", Offset = "0x157B914", VA = "0x10157B914")]
		private void UpdateEffect()
		{
		}

		// Token: 0x06004BF5 RID: 19445 RVA: 0x0001B0A8 File Offset: 0x000192A8
		[Token(Token = "0x600459E")]
		[Address(RVA = "0x10157B2AC", Offset = "0x157B2AC", VA = "0x10157B2AC")]
		public bool IsEnableSkillLvUp()
		{
			return default(bool);
		}

		// Token: 0x06004BF6 RID: 19446 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600459F")]
		[Address(RVA = "0x10157B3A4", Offset = "0x157B3A4", VA = "0x10157B3A4")]
		private void UpdateWeaponData()
		{
		}

		// Token: 0x06004BF7 RID: 19447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A0")]
		[Address(RVA = "0x10157B5B4", Offset = "0x157B5B4", VA = "0x10157B5B4")]
		private void UpdateWeaponSkillData()
		{
		}

		// Token: 0x06004BF8 RID: 19448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A1")]
		[Address(RVA = "0x10157C0E4", Offset = "0x157C0E4", VA = "0x10157C0E4")]
		private void UpdateSimulateInfo()
		{
		}

		// Token: 0x06004BF9 RID: 19449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A2")]
		[Address(RVA = "0x10157C740", Offset = "0x157C740", VA = "0x10157C740")]
		public void StartEffect()
		{
		}

		// Token: 0x06004BFA RID: 19450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A3")]
		[Address(RVA = "0x10157BC38", Offset = "0x157BC38", VA = "0x10157BC38")]
		private void StartGaugeAnim()
		{
		}

		// Token: 0x06004BFB RID: 19451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A4")]
		[Address(RVA = "0x10157CB6C", Offset = "0x157CB6C", VA = "0x10157CB6C")]
		private void OnUseItemNumChange(int value)
		{
		}

		// Token: 0x06004BFC RID: 19452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A5")]
		[Address(RVA = "0x10157CB80", Offset = "0x157CB80", VA = "0x10157CB80")]
		private void OnClickWpnDetailBtn()
		{
		}

		// Token: 0x06004BFD RID: 19453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A6")]
		[Address(RVA = "0x10157CBE4", Offset = "0x157CBE4", VA = "0x10157CBE4")]
		private void OnClickDecideBtn()
		{
		}

		// Token: 0x06004BFE RID: 19454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A7")]
		[Address(RVA = "0x10157CE0C", Offset = "0x157CE0C", VA = "0x10157CE0C")]
		public ShopWeaponSkillLvUPGroup()
		{
		}

		// Token: 0x04005DB0 RID: 23984
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40041AD")]
		[SerializeField]
		private WeaponIcon m_weaponIcon;

		// Token: 0x04005DB1 RID: 23985
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40041AE")]
		[SerializeField]
		private CustomButton m_btnWeaponInfo;

		// Token: 0x04005DB2 RID: 23986
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40041AF")]
		[SerializeField]
		private Text m_txtWeaponName;

		// Token: 0x04005DB3 RID: 23987
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40041B0")]
		[SerializeField]
		private Text m_txtWeaponLv;

		// Token: 0x04005DB4 RID: 23988
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40041B1")]
		[SerializeField]
		private SkillLvUPInfo m_skillInfo;

		// Token: 0x04005DB5 RID: 23989
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40041B2")]
		[SerializeField]
		private ShopWeaponSkillLvUPItem m_skillLvUPItem;

		// Token: 0x04005DB6 RID: 23990
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40041B3")]
		[SerializeField]
		private CustomButton m_btnDecide;

		// Token: 0x04005DB7 RID: 23991
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40041B4")]
		[SerializeField]
		private MixWpnUpgradeEffectScene m_MixEffectScene;

		// Token: 0x04005DB8 RID: 23992
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40041B5")]
		[SerializeField]
		private PrefabCloner m_UpgradePopUpCloner;

		// Token: 0x04005DB9 RID: 23993
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40041B6")]
		private int m_skillLvUPItemID;

		// Token: 0x04005DBA RID: 23994
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40041B7")]
		private long m_MngWeaponID;

		// Token: 0x04005DBB RID: 23995
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40041B8")]
		private UserWeaponData m_userWeaponData;

		// Token: 0x04005DBC RID: 23996
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40041B9")]
		private int m_consumeItemNum;

		// Token: 0x04005DBD RID: 23997
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40041BA")]
		private long m_stackNowExp;

		// Token: 0x04005DBE RID: 23998
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40041BB")]
		private int m_stackNowLv;

		// Token: 0x04005DBF RID: 23999
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x40041BC")]
		private ShopWeaponSkillLvUPGroup.eStateEffect m_effState;

		// Token: 0x04005DC0 RID: 24000
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x40041BD")]
		public Action<long> m_OnClickWpnDetailBtn;

		// Token: 0x04005DC1 RID: 24001
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x40041BE")]
		public Action<long, int, int> m_OnClickDecideBtn;

		// Token: 0x02000FA1 RID: 4001
		[Token(Token = "0x20011FA")]
		private enum eStateEffect
		{
			// Token: 0x04005DC3 RID: 24003
			[Token(Token = "0x4006F47")]
			Stay,
			// Token: 0x04005DC4 RID: 24004
			[Token(Token = "0x4006F48")]
			Move,
			// Token: 0x04005DC5 RID: 24005
			[Token(Token = "0x4006F49")]
			MixEffect
		}
	}
}
