﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000FA2 RID: 4002
	[Token(Token = "0x2000A6B")]
	[StructLayout(3)]
	public class ShopWeaponSkillLvUPItem : MonoBehaviour
	{
		// Token: 0x06004BFF RID: 19455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A8")]
		[Address(RVA = "0x10157C904", Offset = "0x157C904", VA = "0x10157C904")]
		public void SetEnableInput(bool input)
		{
		}

		// Token: 0x06004C00 RID: 19456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045A9")]
		[Address(RVA = "0x10157AB28", Offset = "0x157AB28", VA = "0x10157AB28")]
		public void Setup(int itemID)
		{
		}

		// Token: 0x06004C01 RID: 19457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045AA")]
		[Address(RVA = "0x10157B0BC", Offset = "0x157B0BC", VA = "0x10157B0BC")]
		public void UpdateData(long maxUseNum)
		{
		}

		// Token: 0x06004C02 RID: 19458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045AB")]
		[Address(RVA = "0x10157CF60", Offset = "0x157CF60", VA = "0x10157CF60")]
		private void OnSliderValueChanged()
		{
		}

		// Token: 0x06004C03 RID: 19459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045AC")]
		[Address(RVA = "0x10157CE24", Offset = "0x157CE24", VA = "0x10157CE24")]
		private void UpdateValueText(int value)
		{
		}

		// Token: 0x06004C04 RID: 19460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045AD")]
		[Address(RVA = "0x10157C9B4", Offset = "0x157C9B4", VA = "0x10157C9B4")]
		public void PlayEffect()
		{
		}

		// Token: 0x06004C05 RID: 19461 RVA: 0x0001B0C0 File Offset: 0x000192C0
		[Token(Token = "0x60045AE")]
		[Address(RVA = "0x10157BC0C", Offset = "0x157BC0C", VA = "0x10157BC0C")]
		public bool isPlayingEffect()
		{
			return default(bool);
		}

		// Token: 0x06004C06 RID: 19462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045AF")]
		[Address(RVA = "0x10157CFF4", Offset = "0x157CFF4", VA = "0x10157CFF4")]
		public ShopWeaponSkillLvUPItem()
		{
		}

		// Token: 0x04005DC6 RID: 24006
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40041BF")]
		[SerializeField]
		private PrefabCloner m_ItemIconCloner;

		// Token: 0x04005DC7 RID: 24007
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40041C0")]
		[SerializeField]
		private WeaponMaterialIcon m_wepMatIcon;

		// Token: 0x04005DC8 RID: 24008
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40041C1")]
		[SerializeField]
		private Text m_txtItemName;

		// Token: 0x04005DC9 RID: 24009
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40041C2")]
		[SerializeField]
		private Text m_txtItemHaveNum;

		// Token: 0x04005DCA RID: 24010
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40041C3")]
		[SerializeField]
		private Text m_txtItemUseNum;

		// Token: 0x04005DCB RID: 24011
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041C4")]
		[SerializeField]
		private PrefabCloner m_NumberSelectCloner;

		// Token: 0x04005DCC RID: 24012
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041C5")]
		private int m_itemID;

		// Token: 0x04005DCD RID: 24013
		[Cpp2IlInjected.FieldOffset(Offset = "0x4C")]
		[Token(Token = "0x40041C6")]
		private int m_itemNum;

		// Token: 0x04005DCE RID: 24014
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041C7")]
		private ItemIconWithFrame m_ItemIconWithFrame;

		// Token: 0x04005DCF RID: 24015
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041C8")]
		private RangeSelector m_NumberSelect;

		// Token: 0x04005DD0 RID: 24016
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40041C9")]
		public Action<int> m_OnValueChange;
	}
}
