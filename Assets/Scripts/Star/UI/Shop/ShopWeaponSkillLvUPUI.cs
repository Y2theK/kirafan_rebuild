﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.WeaponList;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000FA3 RID: 4003
	[Token(Token = "0x2000A6C")]
	[StructLayout(3)]
	public class ShopWeaponSkillLvUPUI : MenuUIBase
	{
		// Token: 0x06004C07 RID: 19463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B0")]
		[Address(RVA = "0x10157CFFC", Offset = "0x157CFFC", VA = "0x10157CFFC")]
		public void Setup(long wepMngID)
		{
		}

		// Token: 0x06004C08 RID: 19464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B1")]
		[Address(RVA = "0x10157D110", Offset = "0x157D110", VA = "0x10157D110", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004C09 RID: 19465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B2")]
		[Address(RVA = "0x10157D13C", Offset = "0x157D13C", VA = "0x10157D13C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004C0A RID: 19466 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B3")]
		[Address(RVA = "0x10157D300", Offset = "0x157D300", VA = "0x10157D300", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004C0B RID: 19467 RVA: 0x0001B0D8 File Offset: 0x000192D8
		[Token(Token = "0x60045B4")]
		[Address(RVA = "0x10157D340", Offset = "0x157D340", VA = "0x10157D340", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004C0C RID: 19468 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B5")]
		[Address(RVA = "0x10157D350", Offset = "0x157D350", VA = "0x10157D350")]
		private void Update()
		{
		}

		// Token: 0x06004C0D RID: 19469 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B6")]
		[Address(RVA = "0x10157D144", Offset = "0x157D144", VA = "0x10157D144")]
		private void ChangeStep(ShopWeaponSkillLvUPUI.eStep step)
		{
		}

		// Token: 0x06004C0E RID: 19470 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B7")]
		[Address(RVA = "0x10157D3F8", Offset = "0x157D3F8", VA = "0x10157D3F8")]
		private void OnClickWeaponDetailButton(long wpnMngID)
		{
		}

		// Token: 0x06004C0F RID: 19471 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B8")]
		[Address(RVA = "0x10157D4F8", Offset = "0x157D4F8", VA = "0x10157D4F8")]
		private void OnClickDecideButton(long wpnMngID, int itemNum, int consumeNum)
		{
		}

		// Token: 0x06004C10 RID: 19472 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045B9")]
		[Address(RVA = "0x10157D864", Offset = "0x157D864", VA = "0x10157D864")]
		private void OnCloseWindowFunc(int btn)
		{
		}

		// Token: 0x06004C11 RID: 19473 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045BA")]
		[Address(RVA = "0x10157D9D0", Offset = "0x157D9D0", VA = "0x10157D9D0")]
		public void OnResponceSkillLVUP(ShopDefine.eWeaponSkillLvUPResult result, string errMsg)
		{
		}

		// Token: 0x06004C12 RID: 19474 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045BB")]
		[Address(RVA = "0x10157DAB0", Offset = "0x157DAB0", VA = "0x10157DAB0")]
		public ShopWeaponSkillLvUPUI()
		{
		}

		// Token: 0x04005DD1 RID: 24017
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041CA")]
		[SerializeField]
		private ShopWeaponSkillLvUPGroup m_ConfirmGroup;

		// Token: 0x04005DD2 RID: 24018
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041CB")]
		[SerializeField]
		private WeaponDetailGroup m_weaponDetailGroup;

		// Token: 0x04005DD3 RID: 24019
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041CC")]
		private ShopWeaponSkillLvUPUI.eStep m_Step;

		// Token: 0x04005DD4 RID: 24020
		[Cpp2IlInjected.FieldOffset(Offset = "0x54")]
		[Token(Token = "0x40041CD")]
		private int m_skillLvUPItemID;

		// Token: 0x04005DD5 RID: 24021
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041CE")]
		private long m_WeaponMngID;

		// Token: 0x04005DD6 RID: 24022
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40041CF")]
		private int m_consumeItemNum;

		// Token: 0x02000FA4 RID: 4004
		[Token(Token = "0x20011FB")]
		private enum eStep
		{
			// Token: 0x04005DD8 RID: 24024
			[Token(Token = "0x4006F4B")]
			Hide,
			// Token: 0x04005DD9 RID: 24025
			[Token(Token = "0x4006F4C")]
			In,
			// Token: 0x04005DDA RID: 24026
			[Token(Token = "0x4006F4D")]
			Idle,
			// Token: 0x04005DDB RID: 24027
			[Token(Token = "0x4006F4E")]
			Out,
			// Token: 0x04005DDC RID: 24028
			[Token(Token = "0x4006F4F")]
			End
		}
	}
}
