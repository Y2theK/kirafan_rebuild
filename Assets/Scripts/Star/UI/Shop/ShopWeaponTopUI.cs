﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000FA5 RID: 4005
	[Token(Token = "0x2000A6D")]
	[StructLayout(3)]
	public class ShopWeaponTopUI : MenuUIBase
	{
		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06004C13 RID: 19475 RVA: 0x0001B0F0 File Offset: 0x000192F0
		[Token(Token = "0x170004E2")]
		public ShopWeaponTopUI.eMainButton SelectButton
		{
			[Token(Token = "0x60045BC")]
			[Address(RVA = "0x10157DAC4", Offset = "0x157DAC4", VA = "0x10157DAC4")]
			get
			{
				return ShopWeaponTopUI.eMainButton.Create;
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06004C14 RID: 19476 RVA: 0x0001B108 File Offset: 0x00019308
		[Token(Token = "0x170004E3")]
		public eClassType SelectClassType
		{
			[Token(Token = "0x60045BD")]
			[Address(RVA = "0x10157DACC", Offset = "0x157DACC", VA = "0x10157DACC")]
			get
			{
				return eClassType.Fighter;
			}
		}

		// Token: 0x140000EA RID: 234
		// (add) Token: 0x06004C15 RID: 19477 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004C16 RID: 19478 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000EA")]
		public event Action OnClickMainButton
		{
			[Token(Token = "0x60045BE")]
			[Address(RVA = "0x10157DAD4", Offset = "0x157DAD4", VA = "0x10157DAD4")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60045BF")]
			[Address(RVA = "0x10157DBE0", Offset = "0x157DBE0", VA = "0x10157DBE0")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004C17 RID: 19479 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C0")]
		[Address(RVA = "0x10157DCEC", Offset = "0x157DCEC", VA = "0x10157DCEC")]
		public void Setup()
		{
		}

		// Token: 0x06004C18 RID: 19480 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C1")]
		[Address(RVA = "0x10157E03C", Offset = "0x157E03C", VA = "0x10157E03C")]
		private void Update()
		{
		}

		// Token: 0x06004C19 RID: 19481 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C2")]
		[Address(RVA = "0x10157E26C", Offset = "0x157E26C", VA = "0x10157E26C")]
		private void ChangeStep(ShopWeaponTopUI.eStep step)
		{
		}

		// Token: 0x06004C1A RID: 19482 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C3")]
		[Address(RVA = "0x10157E54C", Offset = "0x157E54C", VA = "0x10157E54C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004C1B RID: 19483 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C4")]
		[Address(RVA = "0x10157E554", Offset = "0x157E554", VA = "0x10157E554", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004C1C RID: 19484 RVA: 0x0001B120 File Offset: 0x00019320
		[Token(Token = "0x60045C5")]
		[Address(RVA = "0x10157E5F4", Offset = "0x157E5F4", VA = "0x10157E5F4", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004C1D RID: 19485 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C6")]
		[Address(RVA = "0x10157E604", Offset = "0x157E604", VA = "0x10157E604")]
		public void OnClickMainButtonCallBack(int btn)
		{
		}

		// Token: 0x06004C1E RID: 19486 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045C7")]
		[Address(RVA = "0x10157E614", Offset = "0x157E614", VA = "0x10157E614")]
		public ShopWeaponTopUI()
		{
		}

		// Token: 0x04005DDD RID: 24029
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041D0")]
		private ShopWeaponTopUI.eStep m_Step;

		// Token: 0x04005DDE RID: 24030
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041D1")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005DDF RID: 24031
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041D2")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04005DE0 RID: 24032
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041D3")]
		[SerializeField]
		private CustomButton m_WeaponEvolutionButton;

		// Token: 0x04005DE1 RID: 24033
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40041D4")]
		[SerializeField]
		private GameObject m_WeaponEvolutionMask;

		// Token: 0x04005DE2 RID: 24034
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40041D5")]
		[SerializeField]
		private CustomButton m_WeaponSkillLvUpButton;

		// Token: 0x04005DE3 RID: 24035
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40041D6")]
		[SerializeField]
		private GameObject m_WeaponSkillLvUpMask;

		// Token: 0x04005DE4 RID: 24036
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40041D7")]
		private ShopWeaponTopUI.eMainButton m_SelectButton;

		// Token: 0x04005DE5 RID: 24037
		[Cpp2IlInjected.FieldOffset(Offset = "0x7C")]
		[Token(Token = "0x40041D8")]
		private eClassType m_SelectClassType;

		// Token: 0x04005DE6 RID: 24038
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40041D9")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x02000FA6 RID: 4006
		[Token(Token = "0x20011FC")]
		private enum eStep
		{
			// Token: 0x04005DE9 RID: 24041
			[Token(Token = "0x4006F51")]
			Hide,
			// Token: 0x04005DEA RID: 24042
			[Token(Token = "0x4006F52")]
			BGLoad,
			// Token: 0x04005DEB RID: 24043
			[Token(Token = "0x4006F53")]
			In,
			// Token: 0x04005DEC RID: 24044
			[Token(Token = "0x4006F54")]
			Idle,
			// Token: 0x04005DED RID: 24045
			[Token(Token = "0x4006F55")]
			Out,
			// Token: 0x04005DEE RID: 24046
			[Token(Token = "0x4006F56")]
			End
		}

		// Token: 0x02000FA7 RID: 4007
		[Token(Token = "0x20011FD")]
		public enum eMainButton
		{
			// Token: 0x04005DF0 RID: 24048
			[Token(Token = "0x4006F58")]
			None = -1,
			// Token: 0x04005DF1 RID: 24049
			[Token(Token = "0x4006F59")]
			Create,
			// Token: 0x04005DF2 RID: 24050
			[Token(Token = "0x4006F5A")]
			Upgrade,
			// Token: 0x04005DF3 RID: 24051
			[Token(Token = "0x4006F5B")]
			Sale,
			// Token: 0x04005DF4 RID: 24052
			[Token(Token = "0x4006F5C")]
			LimitExtend,
			// Token: 0x04005DF5 RID: 24053
			[Token(Token = "0x4006F5D")]
			Evolution,
			// Token: 0x04005DF6 RID: 24054
			[Token(Token = "0x4006F5E")]
			SKillLvUp
		}
	}
}
