﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000FA8 RID: 4008
	[Token(Token = "0x2000A6E")]
	[StructLayout(3)]
	public class ShopWeaponUpgradeConfirmGroup : UIGroup
	{
		// Token: 0x140000EB RID: 235
		// (add) Token: 0x06004C1F RID: 19487 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004C20 RID: 19488 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000EB")]
		public event Action<long, List<KeyValuePair<int, int>>> OnExecute
		{
			[Token(Token = "0x60045C8")]
			[Address(RVA = "0x10157E624", Offset = "0x157E624", VA = "0x10157E624")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60045C9")]
			[Address(RVA = "0x10157E730", Offset = "0x157E730", VA = "0x10157E730")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004C21 RID: 19489 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CA")]
		[Address(RVA = "0x10157E83C", Offset = "0x157E83C", VA = "0x10157E83C")]
		public void Setup()
		{
		}

		// Token: 0x06004C22 RID: 19490 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CB")]
		[Address(RVA = "0x10157E8DC", Offset = "0x157E8DC", VA = "0x10157E8DC")]
		public void Destroy()
		{
		}

		// Token: 0x06004C23 RID: 19491 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CC")]
		[Address(RVA = "0x10157E978", Offset = "0x157E978", VA = "0x10157E978")]
		public void Open(long weaponMngID)
		{
		}

		// Token: 0x06004C24 RID: 19492 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CD")]
		[Address(RVA = "0x10157EA94", Offset = "0x157EA94", VA = "0x10157EA94")]
		private void Refresh()
		{
		}

		// Token: 0x06004C25 RID: 19493 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CE")]
		[Address(RVA = "0x10157F4D0", Offset = "0x157F4D0", VA = "0x10157F4D0")]
		private void Apply()
		{
		}

		// Token: 0x06004C26 RID: 19494 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045CF")]
		[Address(RVA = "0x10157EFC4", Offset = "0x157EFC4", VA = "0x10157EFC4")]
		private void ApplyInternal(List<KeyValuePair<int, int>> list)
		{
		}

		// Token: 0x06004C27 RID: 19495 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D0")]
		[Address(RVA = "0x10157F510", Offset = "0x157F510", VA = "0x10157F510")]
		private void OnMaterialChanged()
		{
		}

		// Token: 0x06004C28 RID: 19496 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D1")]
		[Address(RVA = "0x10157F548", Offset = "0x157F548", VA = "0x10157F548")]
		private void ReceiveUseItem(int itemID, int itemNum)
		{
		}

		// Token: 0x06004C29 RID: 19497 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D2")]
		[Address(RVA = "0x10157F954", Offset = "0x157F954", VA = "0x10157F954")]
		public void OnClickExecuteButtonCallBack()
		{
		}

		// Token: 0x06004C2A RID: 19498 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D3")]
		[Address(RVA = "0x10157FABC", Offset = "0x157FABC", VA = "0x10157FABC")]
		private void OnConfirmExec(int btn)
		{
		}

		// Token: 0x06004C2B RID: 19499 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D4")]
		[Address(RVA = "0x10157FB50", Offset = "0x157FB50", VA = "0x10157FB50")]
		public void OnClickResetButton()
		{
		}

		// Token: 0x06004C2C RID: 19500 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D5")]
		[Address(RVA = "0x10157FB90", Offset = "0x157FB90", VA = "0x10157FB90")]
		public void StartEffect(int lv)
		{
		}

		// Token: 0x06004C2D RID: 19501 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D6")]
		[Address(RVA = "0x10157FC94", Offset = "0x157FC94", VA = "0x10157FC94", Slot = "10")]
		public override void LateUpdate()
		{
		}

		// Token: 0x06004C2E RID: 19502 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D7")]
		[Address(RVA = "0x101580654", Offset = "0x1580654", VA = "0x101580654")]
		private void OnFinish(int btn)
		{
		}

		// Token: 0x06004C2F RID: 19503 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045D8")]
		[Address(RVA = "0x101580748", Offset = "0x1580748", VA = "0x101580748")]
		public ShopWeaponUpgradeConfirmGroup()
		{
		}

		// Token: 0x04005DF7 RID: 24055
		[Token(Token = "0x40041DB")]
		private const float MOVESPAN = 0.1f;

		// Token: 0x04005DF8 RID: 24056
		[Token(Token = "0x40041DC")]
		private const int MATERIAL_NUM = 5;

		// Token: 0x04005DF9 RID: 24057
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40041DD")]
		[SerializeField]
		private WeaponNamePlate m_WeaponNamePlate;

		// Token: 0x04005DFA RID: 24058
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40041DE")]
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04005DFB RID: 24059
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40041DF")]
		[SerializeField]
		private WeaponMaterialIcon[] m_UseMaterialIcons;

		// Token: 0x04005DFC RID: 24060
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40041E0")]
		[SerializeField]
		private UpgradeMaterialPanel m_MaterialPanel;

		// Token: 0x04005DFD RID: 24061
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40041E1")]
		[SerializeField]
		private Text m_BeforeLv;

		// Token: 0x04005DFE RID: 24062
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40041E2")]
		[SerializeField]
		private Text m_AfterLv;

		// Token: 0x04005DFF RID: 24063
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40041E3")]
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04005E00 RID: 24064
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40041E4")]
		[SerializeField]
		private WeaponStatus m_WeaponStatus;

		// Token: 0x04005E01 RID: 24065
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40041E5")]
		[SerializeField]
		private MixWpnUpgradeEffectScene m_MixEffectScene;

		// Token: 0x04005E02 RID: 24066
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40041E6")]
		[SerializeField]
		private PrefabCloner m_UpgradePopUpCloner;

		// Token: 0x04005E03 RID: 24067
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40041E7")]
		private ShopWeaponUpgradeConfirmGroup.eEffectStep m_Step;

		// Token: 0x04005E04 RID: 24068
		[Cpp2IlInjected.FieldOffset(Offset = "0xDC")]
		[Token(Token = "0x40041E8")]
		private int m_SuccessLv;

		// Token: 0x04005E05 RID: 24069
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40041E9")]
		private long m_WeaponMngID;

		// Token: 0x04005E06 RID: 24070
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40041EA")]
		private int m_StartLv;

		// Token: 0x04005E07 RID: 24071
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40041EB")]
		private long m_StartExp;

		// Token: 0x02000FA9 RID: 4009
		[Token(Token = "0x20011FE")]
		private enum eEffectStep
		{
			// Token: 0x04005E0A RID: 24074
			[Token(Token = "0x4006F60")]
			None,
			// Token: 0x04005E0B RID: 24075
			[Token(Token = "0x4006F61")]
			WaitPrepare,
			// Token: 0x04005E0C RID: 24076
			[Token(Token = "0x4006F62")]
			Move,
			// Token: 0x04005E0D RID: 24077
			[Token(Token = "0x4006F63")]
			FinalEffect,
			// Token: 0x04005E0E RID: 24078
			[Token(Token = "0x4006F64")]
			WaitFinalEffect
		}
	}
}
