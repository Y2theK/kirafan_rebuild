﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000FAA RID: 4010
	[Token(Token = "0x2000A6F")]
	[StructLayout(3)]
	public class ShopWeaponUpgradeUI : MenuUIBase
	{
		// Token: 0x140000EC RID: 236
		// (add) Token: 0x06004C30 RID: 19504 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004C31 RID: 19505 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000EC")]
		public event Action<long, List<KeyValuePair<int, int>>> OnClickExecuteButton
		{
			[Token(Token = "0x60045D9")]
			[Address(RVA = "0x101580758", Offset = "0x1580758", VA = "0x101580758")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60045DA")]
			[Address(RVA = "0x101580864", Offset = "0x1580864", VA = "0x101580864")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004C32 RID: 19506 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045DB")]
		[Address(RVA = "0x101580970", Offset = "0x1580970", VA = "0x101580970")]
		private void Start()
		{
		}

		// Token: 0x06004C33 RID: 19507 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045DC")]
		[Address(RVA = "0x101580974", Offset = "0x1580974", VA = "0x101580974")]
		public void Setup(long mngID)
		{
		}

		// Token: 0x06004C34 RID: 19508 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045DD")]
		[Address(RVA = "0x101580AA4", Offset = "0x1580AA4", VA = "0x101580AA4", Slot = "7")]
		public override void Destroy()
		{
		}

		// Token: 0x06004C35 RID: 19509 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045DE")]
		[Address(RVA = "0x101580AD0", Offset = "0x1580AD0", VA = "0x101580AD0")]
		private void Update()
		{
		}

		// Token: 0x06004C36 RID: 19510 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045DF")]
		[Address(RVA = "0x101580CE4", Offset = "0x1580CE4", VA = "0x101580CE4")]
		private void ChangeStep(ShopWeaponUpgradeUI.eStep step)
		{
		}

		// Token: 0x06004C37 RID: 19511 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E0")]
		[Address(RVA = "0x101580F74", Offset = "0x1580F74", VA = "0x101580F74", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x06004C38 RID: 19512 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E1")]
		[Address(RVA = "0x101581014", Offset = "0x1581014", VA = "0x101581014", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x06004C39 RID: 19513 RVA: 0x0001B138 File Offset: 0x00019338
		[Token(Token = "0x60045E2")]
		[Address(RVA = "0x1015810D0", Offset = "0x15810D0", VA = "0x1015810D0", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x06004C3A RID: 19514 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E3")]
		[Address(RVA = "0x1015810E0", Offset = "0x15810E0", VA = "0x1015810E0")]
		private void OnClickExecuteButtonCallBack(long weaponMngID, List<KeyValuePair<int, int>> list)
		{
		}

		// Token: 0x06004C3B RID: 19515 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E4")]
		[Address(RVA = "0x101581148", Offset = "0x1581148", VA = "0x101581148")]
		public void StartUpgradeEffect(int successLv)
		{
		}

		// Token: 0x06004C3C RID: 19516 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E5")]
		[Address(RVA = "0x10158117C", Offset = "0x158117C", VA = "0x10158117C")]
		public ShopWeaponUpgradeUI()
		{
		}

		// Token: 0x04005E0F RID: 24079
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041ED")]
		private ShopWeaponUpgradeUI.eStep m_Step;

		// Token: 0x04005E10 RID: 24080
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041EE")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04005E11 RID: 24081
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041EF")]
		[SerializeField]
		private ShopWeaponUpgradeConfirmGroup m_ConfirmGroup;

		// Token: 0x04005E12 RID: 24082
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041F0")]
		[SerializeField]
		private long m_MngID;

		// Token: 0x02000FAB RID: 4011
		[Token(Token = "0x20011FF")]
		private enum eStep
		{
			// Token: 0x04005E15 RID: 24085
			[Token(Token = "0x4006F66")]
			Hide,
			// Token: 0x04005E16 RID: 24086
			[Token(Token = "0x4006F67")]
			In,
			// Token: 0x04005E17 RID: 24087
			[Token(Token = "0x4006F68")]
			Idle,
			// Token: 0x04005E18 RID: 24088
			[Token(Token = "0x4006F69")]
			Out,
			// Token: 0x04005E19 RID: 24089
			[Token(Token = "0x4006F6A")]
			End
		}
	}
}
