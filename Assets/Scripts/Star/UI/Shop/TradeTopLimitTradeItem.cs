﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000FAD RID: 4013
	[Token(Token = "0x2000A71")]
	[StructLayout(3)]
	public class TradeTopLimitTradeItem : ScrollItemIcon
	{
		// Token: 0x06004C41 RID: 19521 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045EA")]
		[Address(RVA = "0x101581184", Offset = "0x1581184", VA = "0x101581184", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004C42 RID: 19522 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045EB")]
		[Address(RVA = "0x10158165C", Offset = "0x158165C", VA = "0x10158165C")]
		public TradeTopLimitTradeItem()
		{
		}

		// Token: 0x04005E1C RID: 24092
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40041F4")]
		[SerializeField]
		private ADVLibraryIcon m_Icon;

		// Token: 0x04005E1D RID: 24093
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40041F5")]
		[SerializeField]
		private Sprite m_StorySprite;

		// Token: 0x04005E1E RID: 24094
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40041F6")]
		[SerializeField]
		private Text m_SpanText;
	}
}
