﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Shop
{
	// Token: 0x02000FAC RID: 4012
	[Token(Token = "0x2000A70")]
	[StructLayout(3)]
	public class TradeTopLimitTradeItemData : ScrollItemData
	{
		// Token: 0x140000ED RID: 237
		// (add) Token: 0x06004C3D RID: 19517 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004C3E RID: 19518 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x140000ED")]
		public event Action<int> OnClick
		{
			[Token(Token = "0x60045E6")]
			[Address(RVA = "0x101581664", Offset = "0x1581664", VA = "0x101581664")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x60045E7")]
			[Address(RVA = "0x101581770", Offset = "0x1581770", VA = "0x101581770")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004C3F RID: 19519 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E8")]
		[Address(RVA = "0x10158187C", Offset = "0x158187C", VA = "0x10158187C")]
		public TradeTopLimitTradeItemData(int eventType, Action<int> callback)
		{
		}

		// Token: 0x06004C40 RID: 19520 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045E9")]
		[Address(RVA = "0x1015818BC", Offset = "0x15818BC", VA = "0x1015818BC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x04005E1A RID: 24090
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40041F2")]
		public int m_EventType;
	}
}
