﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000FAE RID: 4014
	[Token(Token = "0x2000A72")]
	[StructLayout(3)]
	public class WeaponMaterialIcon : MonoBehaviour
	{
		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06004C43 RID: 19523 RVA: 0x0001B150 File Offset: 0x00019350
		[Token(Token = "0x170004E4")]
		public bool IsPlaying
		{
			[Token(Token = "0x60045EC")]
			[Address(RVA = "0x101577514", Offset = "0x1577514", VA = "0x101577514")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004C44 RID: 19524 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045ED")]
		[Address(RVA = "0x10157500C", Offset = "0x157500C", VA = "0x10157500C")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x06004C45 RID: 19525 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045EE")]
		[Address(RVA = "0x101574D50", Offset = "0x1574D50", VA = "0x101574D50")]
		public void SetStartPosition()
		{
		}

		// Token: 0x06004C46 RID: 19526 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045EF")]
		[Address(RVA = "0x101574FD4", Offset = "0x1574FD4", VA = "0x101574FD4")]
		public void Apply(int itemID)
		{
		}

		// Token: 0x06004C47 RID: 19527 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F0")]
		[Address(RVA = "0x101576EE8", Offset = "0x1576EE8", VA = "0x101576EE8")]
		public void MoveTo(float delay)
		{
		}

		// Token: 0x06004C48 RID: 19528 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F1")]
		[Address(RVA = "0x101581910", Offset = "0x1581910", VA = "0x101581910")]
		public void OnCompleteMove()
		{
		}

		// Token: 0x06004C49 RID: 19529 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F2")]
		[Address(RVA = "0x101581984", Offset = "0x1581984", VA = "0x101581984")]
		public void OnUpdateAlpha(float value)
		{
		}

		// Token: 0x06004C4A RID: 19530 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60045F3")]
		[Address(RVA = "0x101581A48", Offset = "0x1581A48", VA = "0x101581A48")]
		public WeaponMaterialIcon()
		{
		}

		// Token: 0x04005E1F RID: 24095
		[Token(Token = "0x40041F7")]
		private const float MOVEDURATION = 0.4f;

		// Token: 0x04005E20 RID: 24096
		[Token(Token = "0x40041F8")]
		private const float FADEDURATION = 0.2f;

		// Token: 0x04005E21 RID: 24097
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40041F9")]
		[SerializeField]
		private RectTransform m_EndLocator;

		// Token: 0x04005E22 RID: 24098
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40041FA")]
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04005E23 RID: 24099
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40041FB")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04005E24 RID: 24100
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40041FC")]
		private Vector3 m_StartPosition;

		// Token: 0x04005E25 RID: 24101
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40041FD")]
		private RectTransform m_RectTransform;

		// Token: 0x04005E26 RID: 24102
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40041FE")]
		private GameObject m_GameObject;

		// Token: 0x04005E27 RID: 24103
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40041FF")]
		private bool m_IsDonePrepare;

		// Token: 0x04005E28 RID: 24104
		[Cpp2IlInjected.FieldOffset(Offset = "0x51")]
		[Token(Token = "0x4004200")]
		private bool m_IsPlaying;
	}
}
