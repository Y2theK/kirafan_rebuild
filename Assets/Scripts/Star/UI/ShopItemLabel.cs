﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CFA RID: 3322
	[Token(Token = "0x20008E3")]
	[StructLayout(3)]
	public class ShopItemLabel : ASyncImage
	{
		// Token: 0x06003CAB RID: 15531 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003768")]
		[Address(RVA = "0x101581A50", Offset = "0x1581A50", VA = "0x101581A50")]
		public void Apply(int id)
		{
		}

		// Token: 0x06003CAC RID: 15532 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003769")]
		[Address(RVA = "0x101581B60", Offset = "0x1581B60", VA = "0x101581B60", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CAD RID: 15533 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600376A")]
		[Address(RVA = "0x101581B8C", Offset = "0x1581B8C", VA = "0x101581B8C")]
		public ShopItemLabel()
		{
		}

		// Token: 0x04004BED RID: 19437
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400355B")]
		protected int m_LabelID;
	}
}
