﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0A RID: 3594
	[Token(Token = "0x20009A2")]
	[StructLayout(3)]
	public class ShopPackageContentDetail : ScrollItemIcon
	{
		// Token: 0x0600425C RID: 16988 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD8")]
		[Address(RVA = "0x101581B9C", Offset = "0x1581B9C", VA = "0x101581B9C")]
		public void Apply(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x0600425D RID: 16989 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CD9")]
		[Address(RVA = "0x101581C90", Offset = "0x1581C90", VA = "0x101581C90")]
		private void ApplyItem(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x0600425E RID: 16990 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDA")]
		[Address(RVA = "0x101581F48", Offset = "0x1581F48", VA = "0x101581F48")]
		private void ApplyWeapon(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x0600425F RID: 16991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDB")]
		[Address(RVA = "0x101581DF0", Offset = "0x1581DF0", VA = "0x101581DF0")]
		private void ApplyChara(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x06004260 RID: 16992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDC")]
		[Address(RVA = "0x1015820A4", Offset = "0x15820A4", VA = "0x1015820A4")]
		private void ApplyGold(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x06004261 RID: 16993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDD")]
		[Address(RVA = "0x1015821D0", Offset = "0x15821D0", VA = "0x1015821D0")]
		private void ApplyKRRPoint(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x06004262 RID: 16994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDE")]
		[Address(RVA = "0x101582300", Offset = "0x1582300", VA = "0x101582300")]
		private void ApplyRoomObj(PackageItemContentsDB_Param param)
		{
		}

		// Token: 0x06004263 RID: 16995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CDF")]
		[Address(RVA = "0x101582418", Offset = "0x1582418", VA = "0x101582418")]
		public void OnClick()
		{
		}

		// Token: 0x06004264 RID: 16996 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE0")]
		[Address(RVA = "0x101582554", Offset = "0x1582554", VA = "0x101582554")]
		public ShopPackageContentDetail()
		{
		}

		// Token: 0x0400526C RID: 21100
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A3F")]
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x0400526D RID: 21101
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A40")]
		[SerializeField]
		private Text m_txtItemValue;

		// Token: 0x0400526E RID: 21102
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A41")]
		[SerializeField]
		private Text m_txtItemName;

		// Token: 0x0400526F RID: 21103
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A42")]
		private PackageItemContentsDB_Param m_data;
	}
}
