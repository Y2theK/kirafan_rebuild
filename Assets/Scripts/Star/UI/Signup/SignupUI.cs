﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Signup
{
	// Token: 0x02000E68 RID: 3688
	[Token(Token = "0x20009D0")]
	[StructLayout(3)]
	public class SignupUI : MenuUIBase
	{
		// Token: 0x1400008D RID: 141
		// (add) Token: 0x060044B8 RID: 17592 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x060044B9 RID: 17593 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400008D")]
		public event Action OnClickDecide
		{
			[Token(Token = "0x6003F24")]
			[Address(RVA = "0x10158255C", Offset = "0x158255C", VA = "0x10158255C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003F25")]
			[Address(RVA = "0x101582668", Offset = "0x1582668", VA = "0x101582668")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x060044BA RID: 17594 RVA: 0x00002052 File Offset: 0x00000252
		// (set) Token: 0x060044BB RID: 17595 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700049D")]
		public string UserName
		{
			[Token(Token = "0x6003F26")]
			[Address(RVA = "0x101582774", Offset = "0x1582774", VA = "0x101582774")]
			get
			{
				return null;
			}
			[Token(Token = "0x6003F27")]
			[Address(RVA = "0x1015827A4", Offset = "0x15827A4", VA = "0x1015827A4")]
			set
			{
			}
		}

		// Token: 0x060044BC RID: 17596 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F28")]
		[Address(RVA = "0x1015827DC", Offset = "0x15827DC", VA = "0x1015827DC")]
		private void Update()
		{
		}

		// Token: 0x060044BD RID: 17597 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F29")]
		[Address(RVA = "0x1015827E0", Offset = "0x15827E0", VA = "0x1015827E0")]
		private void UpdateStep()
		{
		}

		// Token: 0x060044BE RID: 17598 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F2A")]
		[Address(RVA = "0x101582980", Offset = "0x1582980", VA = "0x101582980")]
		private void ChangeStep(SignupUI.eStep step)
		{
		}

		// Token: 0x060044BF RID: 17599 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F2B")]
		[Address(RVA = "0x101582A88", Offset = "0x1582A88", VA = "0x101582A88")]
		public void Setup()
		{
		}

		// Token: 0x060044C0 RID: 17600 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F2C")]
		[Address(RVA = "0x101582A8C", Offset = "0x1582A8C", VA = "0x101582A8C", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x060044C1 RID: 17601 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F2D")]
		[Address(RVA = "0x101582B5C", Offset = "0x1582B5C", VA = "0x101582B5C", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x060044C2 RID: 17602 RVA: 0x00019A58 File Offset: 0x00017C58
		[Token(Token = "0x6003F2E")]
		[Address(RVA = "0x101582C3C", Offset = "0x1582C3C", VA = "0x101582C3C", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x060044C3 RID: 17603 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F2F")]
		[Address(RVA = "0x101582C4C", Offset = "0x1582C4C", VA = "0x101582C4C")]
		public void OnClickDecideButtonCallBack()
		{
		}

		// Token: 0x060044C4 RID: 17604 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003F30")]
		[Address(RVA = "0x101582C58", Offset = "0x1582C58", VA = "0x101582C58")]
		public SignupUI()
		{
		}

		// Token: 0x040055A6 RID: 21926
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003C4A")]
		private SignupUI.eStep m_Step;

		// Token: 0x040055A7 RID: 21927
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003C4B")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040055A8 RID: 21928
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003C4C")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040055A9 RID: 21929
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003C4D")]
		[SerializeField]
		private TextInput m_TextInput;

		// Token: 0x040055AA RID: 21930
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003C4E")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x02000E69 RID: 3689
		[Token(Token = "0x200115E")]
		private enum eStep
		{
			// Token: 0x040055AD RID: 21933
			[Token(Token = "0x4006CA6")]
			Hide,
			// Token: 0x040055AE RID: 21934
			[Token(Token = "0x4006CA7")]
			In,
			// Token: 0x040055AF RID: 21935
			[Token(Token = "0x4006CA8")]
			Out,
			// Token: 0x040055B0 RID: 21936
			[Token(Token = "0x4006CA9")]
			Idle,
			// Token: 0x040055B1 RID: 21937
			[Token(Token = "0x4006CAA")]
			End,
			// Token: 0x040055B2 RID: 21938
			[Token(Token = "0x4006CAB")]
			Num
		}
	}
}
