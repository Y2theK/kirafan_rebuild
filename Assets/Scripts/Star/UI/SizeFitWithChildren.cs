﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D41 RID: 3393
	[Token(Token = "0x2000919")]
	[StructLayout(3)]
	public class SizeFitWithChildren : MonoBehaviour
	{
		// Token: 0x06003E42 RID: 15938 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038FA")]
		[Address(RVA = "0x101582C60", Offset = "0x1582C60", VA = "0x101582C60")]
		private void Start()
		{
		}

		// Token: 0x06003E43 RID: 15939 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038FB")]
		[Address(RVA = "0x101582C64", Offset = "0x1582C64", VA = "0x101582C64")]
		public void Rebuild()
		{
		}

		// Token: 0x06003E44 RID: 15940 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038FC")]
		[Address(RVA = "0x101582F98", Offset = "0x1582F98", VA = "0x101582F98")]
		public SizeFitWithChildren()
		{
		}

		// Token: 0x04004D7A RID: 19834
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003684")]
		[SerializeField]
		private RectTransform m_FitTarget;

		// Token: 0x04004D7B RID: 19835
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003685")]
		[SerializeField]
		private bool m_Width;

		// Token: 0x04004D7C RID: 19836
		[Cpp2IlInjected.FieldOffset(Offset = "0x21")]
		[Token(Token = "0x4003686")]
		[SerializeField]
		private bool m_Height;

		// Token: 0x04004D7D RID: 19837
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003687")]
		[SerializeField]
		private SizeFitWithChildren.Padding m_Padding;

		// Token: 0x02000D42 RID: 3394
		[Token(Token = "0x20010EF")]
		[Serializable]
		public class Padding
		{
			// Token: 0x06003E45 RID: 15941 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061FB")]
			[Address(RVA = "0x101582FA8", Offset = "0x1582FA8", VA = "0x101582FA8")]
			public Padding()
			{
			}

			// Token: 0x04004D7E RID: 19838
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006A45")]
			public float m_Left;

			// Token: 0x04004D7F RID: 19839
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006A46")]
			public float m_Right;

			// Token: 0x04004D80 RID: 19840
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A47")]
			public float m_Top;

			// Token: 0x04004D81 RID: 19841
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006A48")]
			public float m_Bottom;
		}
	}
}
