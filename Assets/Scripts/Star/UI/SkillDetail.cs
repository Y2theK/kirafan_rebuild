﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D43 RID: 3395
	[Token(Token = "0x200091A")]
	[StructLayout(3)]
	public class SkillDetail : MonoBehaviour
	{
		// Token: 0x06003E46 RID: 15942 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038FD")]
		[Address(RVA = "0x101582FB0", Offset = "0x1582FB0", VA = "0x101582FB0")]
		public void Apply(SkillDetail.eSkillCategory category, int skillID, int skillLv = 0)
		{
		}

		// Token: 0x06003E47 RID: 15943 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60038FE")]
		[Address(RVA = "0x10158339C", Offset = "0x158339C", VA = "0x10158339C")]
		public SkillDetail()
		{
		}

		// Token: 0x04004D82 RID: 19842
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003688")]
		[SerializeField]
		private SkillIcon m_SkillIcon;

		// Token: 0x04004D83 RID: 19843
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003689")]
		[SerializeField]
		private Text m_SkillName;

		// Token: 0x04004D84 RID: 19844
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400368A")]
		[SerializeField]
		private Text m_SkillDetail;

		// Token: 0x04004D85 RID: 19845
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400368B")]
		[SerializeField]
		private Text m_SkillLevel;

		// Token: 0x02000D44 RID: 3396
		[Token(Token = "0x20010F0")]
		public enum eSkillCategory
		{
			// Token: 0x04004D87 RID: 19847
			[Token(Token = "0x4006A4A")]
			PL,
			// Token: 0x04004D88 RID: 19848
			[Token(Token = "0x4006A4B")]
			EN,
			// Token: 0x04004D89 RID: 19849
			[Token(Token = "0x4006A4C")]
			MST,
			// Token: 0x04004D8A RID: 19850
			[Token(Token = "0x4006A4D")]
			WPN
		}
	}
}
