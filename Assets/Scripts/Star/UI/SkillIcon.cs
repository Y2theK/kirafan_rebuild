﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CFB RID: 3323
	[Token(Token = "0x20008E4")]
	[StructLayout(3)]
	public class SkillIcon : MonoBehaviour
	{
		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x06003CAE RID: 15534 RVA: 0x00018750 File Offset: 0x00016950
		// (set) Token: 0x06003CAF RID: 15535 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x17000449")]
		public bool DefaultBlack
		{
			[Token(Token = "0x600376B")]
			[Address(RVA = "0x1015833A4", Offset = "0x15833A4", VA = "0x1015833A4")]
			[CompilerGenerated]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x600376C")]
			[Address(RVA = "0x1015833AC", Offset = "0x15833AC", VA = "0x1015833AC")]
			[CompilerGenerated]
			set
			{
			}
		}

		// Token: 0x06003CB0 RID: 15536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600376D")]
		[Address(RVA = "0x1015833B4", Offset = "0x15833B4", VA = "0x1015833B4")]
		public void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement)
		{
		}

		// Token: 0x06003CB1 RID: 15537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600376E")]
		[Address(RVA = "0x101583DB4", Offset = "0x1583DB4", VA = "0x101583DB4")]
		public void ApplyPassive(int skillID, eElementType ownerElement)
		{
		}

		// Token: 0x06003CB2 RID: 15538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600376F")]
		[Address(RVA = "0x101583E8C", Offset = "0x1583E8C", VA = "0x101583E8C")]
		public void ApplyOrbBuff()
		{
		}

		// Token: 0x06003CB3 RID: 15539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003770")]
		[Address(RVA = "0x101583F64", Offset = "0x1583F64", VA = "0x101583F64")]
		public void ApplyAbilitySphere()
		{
		}

		// Token: 0x06003CB4 RID: 15540 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003771")]
		[Address(RVA = "0x10158383C", Offset = "0x158383C", VA = "0x10158383C")]
		public static List<eElementType> GetSkillElement(int skillID, SkillIcon.eSkillDBType skillDBType, eElementType ownerType)
		{
			return null;
		}

		// Token: 0x06003CB5 RID: 15541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003772")]
		[Address(RVA = "0x10158403C", Offset = "0x158403C", VA = "0x10158403C")]
		public SkillIcon()
		{
		}

		// Token: 0x04004BEE RID: 19438
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400355C")]
		[SerializeField]
		private Image m_SkillIcon;

		// Token: 0x04004BEF RID: 19439
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400355D")]
		[SerializeField]
		private Sprite m_UniqueSprite;

		// Token: 0x04004BF0 RID: 19440
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400355E")]
		[SerializeField]
		private Sprite m_AtkSprite;

		// Token: 0x04004BF1 RID: 19441
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400355F")]
		[SerializeField]
		private Sprite m_MgcSprite;

		// Token: 0x04004BF2 RID: 19442
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003560")]
		[SerializeField]
		private Sprite m_RecoverSprite;

		// Token: 0x04004BF3 RID: 19443
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003561")]
		[SerializeField]
		private Sprite m_BuffSprite;

		// Token: 0x04004BF4 RID: 19444
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003562")]
		[SerializeField]
		private Sprite m_DebuffSprite;

		// Token: 0x04004BF5 RID: 19445
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003563")]
		[SerializeField]
		private Sprite m_PassiveSprite;

		// Token: 0x04004BF6 RID: 19446
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003564")]
		[SerializeField]
		private Sprite m_OrbBuffSprite;

		// Token: 0x04004BF7 RID: 19447
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003565")]
		[SerializeField]
		private Sprite m_AbilitySprite;

		// Token: 0x04004BF8 RID: 19448
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003566")]
		[SerializeField]
		private Sprite m_CharacterAttackSprite;

		// Token: 0x04004BF9 RID: 19449
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003567")]
		[SerializeField]
		private Sprite m_CharacterRecoverSprite;

		// Token: 0x04004BFA RID: 19450
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003568")]
		[SerializeField]
		private Sprite m_CharacterBuffSprite;

		// Token: 0x04004BFB RID: 19451
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x4003569")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x02000CFC RID: 3324
		[Token(Token = "0x20010DE")]
		public enum eSkillDBType
		{
			// Token: 0x04004BFE RID: 19454
			[Token(Token = "0x40069E2")]
			Player,
			// Token: 0x04004BFF RID: 19455
			[Token(Token = "0x40069E3")]
			Enemy,
			// Token: 0x04004C00 RID: 19456
			[Token(Token = "0x40069E4")]
			Master,
			// Token: 0x04004C01 RID: 19457
			[Token(Token = "0x40069E5")]
			Weapon,
			// Token: 0x04004C02 RID: 19458
			[Token(Token = "0x40069E6")]
			Card
		}
	}
}
