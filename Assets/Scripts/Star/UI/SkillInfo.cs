﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CA5 RID: 3237
	[Token(Token = "0x20008AC")]
	[StructLayout(3)]
	public class SkillInfo : MonoBehaviour
	{
		// Token: 0x06003B43 RID: 15171 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003605")]
		[Address(RVA = "0x101584044", Offset = "0x1584044", VA = "0x101584044")]
		protected PrefabCloner GetCloner()
		{
			return null;
		}

		// Token: 0x06003B44 RID: 15172 RVA: 0x00018480 File Offset: 0x00016680
		[Token(Token = "0x6003606")]
		[Address(RVA = "0x10158404C", Offset = "0x158404C", VA = "0x10158404C")]
		private SkillListDB_Param ApplyExistCommon(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement)
		{
			return default(SkillListDB_Param);
		}

		// Token: 0x06003B45 RID: 15173 RVA: 0x00018498 File Offset: 0x00016698
		[Token(Token = "0x6003607")]
		[Address(RVA = "0x101584540", Offset = "0x1584540", VA = "0x101584540")]
		private PassiveSkillListDB_Param? ApplyExistCommonPassive(int skillID, eElementType ownerElement)
		{
			return null;
		}

		// Token: 0x06003B46 RID: 15174 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003608")]
		[Address(RVA = "0x101584A30", Offset = "0x1584A30", VA = "0x101584A30", Slot = "4")]
		public virtual void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, long remainExp = -1L, bool isAroused = false)
		{
		}

		// Token: 0x06003B47 RID: 15175 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003609")]
		[Address(RVA = "0x101584EE0", Offset = "0x1584EE0", VA = "0x101584EE0", Slot = "5")]
		public virtual void ApplyPassive(int skillID, eElementType ownerElement, bool adjustPassiveSize = true)
		{
		}

		// Token: 0x06003B48 RID: 15176 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600360A")]
		[Address(RVA = "0x10158533C", Offset = "0x158533C", VA = "0x10158533C", Slot = "6")]
		public virtual void ApplyEmpty([Optional] string message)
		{
		}

		// Token: 0x06003B49 RID: 15177 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600360B")]
		[Address(RVA = "0x101576DA0", Offset = "0x1576DA0", VA = "0x101576DA0")]
		public void ChangeLevelColor()
		{
		}

		// Token: 0x06003B4A RID: 15178 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600360C")]
		[Address(RVA = "0x101584E44", Offset = "0x1584E44", VA = "0x101584E44")]
		public void SetLevelText(int lv, int maxlv, bool isAroused, bool isBold = false)
		{
		}

		// Token: 0x06003B4B RID: 15179 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600360D")]
		[Address(RVA = "0x10158557C", Offset = "0x158557C", VA = "0x10158557C")]
		public SkillInfo()
		{
		}

		// Token: 0x040049EA RID: 18922
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400340E")]
		[SerializeField]
		protected Text m_NameText;

		// Token: 0x040049EB RID: 18923
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400340F")]
		[SerializeField]
		protected PrefabCloner m_SkillIconCloner;

		// Token: 0x040049EC RID: 18924
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003410")]
		[SerializeField]
		protected Sprite[] m_IconSprites;

		// Token: 0x040049ED RID: 18925
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003411")]
		[SerializeField]
		protected Text m_DetailText;

		// Token: 0x040049EE RID: 18926
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003412")]
		[SerializeField]
		protected Text m_LvText;

		// Token: 0x040049EF RID: 18927
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003413")]
		[SerializeField]
		protected Text m_ExpRemainText;

		// Token: 0x040049F0 RID: 18928
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003414")]
		[SerializeField]
		protected Text m_ExistFreeText;

		// Token: 0x040049F1 RID: 18929
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003415")]
		[SerializeField]
		protected Text m_NoExistText;

		// Token: 0x040049F2 RID: 18930
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003416")]
		[SerializeField]
		protected GameObject m_ExistCommon;

		// Token: 0x040049F3 RID: 18931
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003417")]
		[SerializeField]
		protected GameObject m_Exist;

		// Token: 0x040049F4 RID: 18932
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003418")]
		[SerializeField]
		protected GameObject m_ExistFree;

		// Token: 0x040049F5 RID: 18933
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003419")]
		[SerializeField]
		protected GameObject m_NoExist;

		// Token: 0x040049F6 RID: 18934
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400341A")]
		[SerializeField]
		protected TextField m_SkillNameField;

		// Token: 0x040049F7 RID: 18935
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400341B")]
		[SerializeField]
		protected Image m_IconBG;

		// Token: 0x040049F8 RID: 18936
		[Token(Token = "0x400341C")]
		public static readonly float PASSIVE_SKILL_ADJUST_HEIGHT;

		// Token: 0x040049F9 RID: 18937
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400341D")]
		protected Vector2? m_PrevFrameSize;

		// Token: 0x040049FA RID: 18938
		[Cpp2IlInjected.FieldOffset(Offset = "0x94")]
		[Token(Token = "0x400341E")]
		protected Vector2? m_PrevPositionIconBG;

		// Token: 0x040049FB RID: 18939
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400341F")]
		protected Vector2? m_PrevPositionCloner;

		// Token: 0x02000CA6 RID: 3238
		[Token(Token = "0x20010C1")]
		public enum eSkillIconSprite
		{
			// Token: 0x040049FD RID: 18941
			[Token(Token = "0x400692F")]
			Unique,
			// Token: 0x040049FE RID: 18942
			[Token(Token = "0x4006930")]
			Atk,
			// Token: 0x040049FF RID: 18943
			[Token(Token = "0x4006931")]
			Mgc,
			// Token: 0x04004A00 RID: 18944
			[Token(Token = "0x4006932")]
			Recover,
			// Token: 0x04004A01 RID: 18945
			[Token(Token = "0x4006933")]
			Buff,
			// Token: 0x04004A02 RID: 18946
			[Token(Token = "0x4006934")]
			Debuff
		}
	}
}
