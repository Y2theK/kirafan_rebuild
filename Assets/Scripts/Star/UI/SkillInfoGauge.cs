﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CA7 RID: 3239
	[Token(Token = "0x20008AD")]
	[StructLayout(3)]
	public class SkillInfoGauge : SkillInfo
	{
		// Token: 0x06003B4D RID: 15181 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600360F")]
		[Address(RVA = "0x1015855D8", Offset = "0x15855D8", VA = "0x1015855D8")]
		public void SetActiveSkillExpGauge(bool isActive)
		{
		}

		// Token: 0x06003B4E RID: 15182 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003610")]
		[Address(RVA = "0x1015856A0", Offset = "0x15856A0", VA = "0x1015856A0")]
		public void SetActiveSkillNextText(bool isActive)
		{
		}

		// Token: 0x06003B4F RID: 15183 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003611")]
		[Address(RVA = "0x101585768", Offset = "0x1585768", VA = "0x101585768")]
		public void SetActiveLockIcon(bool isActive)
		{
		}

		// Token: 0x06003B50 RID: 15184 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003612")]
		[Address(RVA = "0x10157BF7C", Offset = "0x157BF7C", VA = "0x10157BF7C")]
		public void ApplyGauge(int lv = -1, int maxlv = 1, long nowExp = -1L, long nextExp = -1L)
		{
		}

		// Token: 0x06003B51 RID: 15185 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003613")]
		[Address(RVA = "0x101585928", Offset = "0x1585928", VA = "0x101585928")]
		public void ApplyNextText(long remainExp)
		{
		}

		// Token: 0x06003B52 RID: 15186 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003614")]
		[Address(RVA = "0x101585AF4", Offset = "0x1585AF4", VA = "0x101585AF4", Slot = "4")]
		public override void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, long remainExp = -1L, bool isAroused = false)
		{
		}

		// Token: 0x06003B53 RID: 15187 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003615")]
		[Address(RVA = "0x101585B78", Offset = "0x1585B78", VA = "0x101585B78", Slot = "6")]
		public override void ApplyEmpty([Optional] string message)
		{
		}

		// Token: 0x06003B54 RID: 15188 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003616")]
		[Address(RVA = "0x101585BB4", Offset = "0x1585BB4", VA = "0x101585BB4", Slot = "5")]
		public override void ApplyPassive(int skillID, eElementType ownerElement, bool adjustPassiveSize = true)
		{
		}

		// Token: 0x06003B55 RID: 15189 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003617")]
		[Address(RVA = "0x101585FD4", Offset = "0x1585FD4", VA = "0x101585FD4", Slot = "7")]
		public virtual void ApplyPassiveLearning(bool adjustPassiveSize, string message)
		{
		}

		// Token: 0x06003B56 RID: 15190 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003618")]
		[Address(RVA = "0x101586314", Offset = "0x1586314", VA = "0x101586314")]
		public SkillInfoGauge()
		{
		}

		// Token: 0x04004A03 RID: 18947
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003420")]
		[SerializeField]
		protected ExpGauge m_ExpGauge;

		// Token: 0x04004A04 RID: 18948
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003421")]
		[SerializeField]
		private Image m_LockIcon;

		// Token: 0x04004A05 RID: 18949
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003422")]
		[SerializeField]
		private ColorGroup m_ColorGroup;
	}
}
