﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CA8 RID: 3240
	[Token(Token = "0x20008AE")]
	[StructLayout(3)]
	public class SkillInfoMasterOrb : MonoBehaviour
	{
		// Token: 0x06003B57 RID: 15191 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003619")]
		[Address(RVA = "0x10158637C", Offset = "0x158637C", VA = "0x10158637C")]
		public void Apply(int skillId)
		{
		}

		// Token: 0x06003B58 RID: 15192 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361A")]
		[Address(RVA = "0x10158658C", Offset = "0x158658C", VA = "0x10158658C")]
		public void ApplyLocked(int lv)
		{
		}

		// Token: 0x06003B59 RID: 15193 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361B")]
		[Address(RVA = "0x101586874", Offset = "0x1586874", VA = "0x101586874")]
		public void ApplyBuff(eTitleType titleType)
		{
		}

		// Token: 0x06003B5A RID: 15194 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361C")]
		[Address(RVA = "0x101586B80", Offset = "0x1586B80", VA = "0x101586B80")]
		public SkillInfoMasterOrb()
		{
		}

		// Token: 0x04004A06 RID: 18950
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003423")]
		[SerializeField]
		private Text m_SkillNameText;

		// Token: 0x04004A07 RID: 18951
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003424")]
		[SerializeField]
		private PrefabCloner m_SkillIconCloner;

		// Token: 0x04004A08 RID: 18952
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003425")]
		[SerializeField]
		private ColorGroup m_IconColorGroup;

		// Token: 0x04004A09 RID: 18953
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003426")]
		[SerializeField]
		private GameObject m_IsLockedObj;

		// Token: 0x04004A0A RID: 18954
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003427")]
		[SerializeField]
		private Text m_LockedText;

		// Token: 0x04004A0B RID: 18955
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003428")]
		[SerializeField]
		private Text m_DetailText;
	}
}
