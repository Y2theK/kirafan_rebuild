﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CA9 RID: 3241
	[Token(Token = "0x20008AF")]
	[StructLayout(3)]
	public class SkillLvUPInfo : SkillInfoGauge
	{
		// Token: 0x06003B5B RID: 15195 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361D")]
		[Address(RVA = "0x10157C5A4", Offset = "0x157C5A4", VA = "0x10157C5A4")]
		public void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, int afterlv = -1, long remainExp = -1L, bool increase = false)
		{
		}

		// Token: 0x06003B5C RID: 15196 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361E")]
		[Address(RVA = "0x101586B88", Offset = "0x1586B88", VA = "0x101586B88")]
		private void ApplyNextText(long remainExp, bool increase)
		{
		}

		// Token: 0x06003B5D RID: 15197 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600361F")]
		[Address(RVA = "0x10157C638", Offset = "0x157C638", VA = "0x10157C638")]
		public void ApplyGaugeSimulate(int lv = -1, int simulateLv = -1, int maxlv = 1, long nowExp = -1L, long nextExp = -1L)
		{
		}

		// Token: 0x06003B5E RID: 15198 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003620")]
		[Address(RVA = "0x10157C828", Offset = "0x157C828", VA = "0x10157C828")]
		public void SetSimulateOff()
		{
		}

		// Token: 0x06003B5F RID: 15199 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003621")]
		[Address(RVA = "0x10157CA3C", Offset = "0x157CA3C", VA = "0x10157CA3C")]
		public void StartGaugeAnim(long rewardExp, int beforeLevel, long beforeNowExp, int afterLevel, long afterNowExp, int maxLevel, long[] needExp)
		{
		}

		// Token: 0x06003B60 RID: 15200 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003622")]
		[Address(RVA = "0x101586DA8", Offset = "0x1586DA8", VA = "0x101586DA8")]
		public void SetAfterLevelText(int baseLv, int lv, int maxlv, bool isAroused, bool isBold = false)
		{
		}

		// Token: 0x06003B61 RID: 15201 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003623")]
		[Address(RVA = "0x101586E54", Offset = "0x1586E54", VA = "0x101586E54")]
		public SkillLvUPInfo()
		{
		}

		// Token: 0x04004A0C RID: 18956
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003429")]
		[SerializeField]
		private Text m_txtSkillLvAfter;
	}
}
