﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D45 RID: 3397
	[Token(Token = "0x200091B")]
	[StructLayout(3)]
	public class SlideMenu : MonoBehaviour
	{
		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06003E48 RID: 15944 RVA: 0x000189D8 File Offset: 0x00016BD8
		[Token(Token = "0x17000465")]
		public bool IsOpen
		{
			[Token(Token = "0x60038FF")]
			[Address(RVA = "0x101586E58", Offset = "0x1586E58", VA = "0x101586E58")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06003E49 RID: 15945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003900")]
		[Address(RVA = "0x101586E60", Offset = "0x1586E60", VA = "0x101586E60")]
		private void Start()
		{
		}

		// Token: 0x06003E4A RID: 15946 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003901")]
		[Address(RVA = "0x101586E70", Offset = "0x1586E70", VA = "0x101586E70")]
		public void Prepare()
		{
		}

		// Token: 0x06003E4B RID: 15947 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003902")]
		[Address(RVA = "0x10158705C", Offset = "0x158705C", VA = "0x10158705C")]
		public void ToggleSlideMenu()
		{
		}

		// Token: 0x06003E4C RID: 15948 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003903")]
		[Address(RVA = "0x101587074", Offset = "0x1587074", VA = "0x101587074")]
		public void Open()
		{
		}

		// Token: 0x06003E4D RID: 15949 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003904")]
		[Address(RVA = "0x101587108", Offset = "0x1587108", VA = "0x101587108")]
		public void Close()
		{
		}

		// Token: 0x06003E4E RID: 15950 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003905")]
		[Address(RVA = "0x101587198", Offset = "0x1587198", VA = "0x101587198")]
		private void PlayInSlideMenu()
		{
		}

		// Token: 0x06003E4F RID: 15951 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003906")]
		[Address(RVA = "0x1015875E4", Offset = "0x15875E4", VA = "0x1015875E4")]
		private void PlayOutSlideMenu()
		{
		}

		// Token: 0x06003E50 RID: 15952 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003907")]
		[Address(RVA = "0x101587A30", Offset = "0x1587A30", VA = "0x101587A30")]
		public SlideMenu()
		{
		}

		// Token: 0x04004D8B RID: 19851
		[Token(Token = "0x400368C")]
		private const float SLIDE_DURATION = 0.2f;

		// Token: 0x04004D8C RID: 19852
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400368D")]
		private GameObject m_GameObject;

		// Token: 0x04004D8D RID: 19853
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400368E")]
		private RectTransform m_RectTransform;

		// Token: 0x04004D8E RID: 19854
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400368F")]
		private Vector3 m_ShowPos;

		// Token: 0x04004D8F RID: 19855
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003690")]
		private Vector3 m_HidePos;

		// Token: 0x04004D90 RID: 19856
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003691")]
		[SerializeField]
		private RectTransform m_ToggleImageRectTransform;

		// Token: 0x04004D91 RID: 19857
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003692")]
		private bool m_IsDonePrepare;

		// Token: 0x04004D92 RID: 19858
		[Cpp2IlInjected.FieldOffset(Offset = "0x49")]
		[Token(Token = "0x4003693")]
		private bool m_IsOpenSlideMenu;
	}
}
