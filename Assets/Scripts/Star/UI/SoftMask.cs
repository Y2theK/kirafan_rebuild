﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0B RID: 3595
	[Token(Token = "0x20009A3")]
	[StructLayout(3)]
	public class SoftMask : MonoBehaviour
	{
		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x06004265 RID: 16997 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000485")]
		private Material material
		{
			[Token(Token = "0x6003CE1")]
			[Address(RVA = "0x101587A38", Offset = "0x1587A38", VA = "0x101587A38")]
			get
			{
				return null;
			}
		}

		// Token: 0x170004DB RID: 1243
		// (get) Token: 0x06004266 RID: 16998 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000486")]
		private Sprite maskSprite
		{
			[Token(Token = "0x6003CE2")]
			[Address(RVA = "0x101587A40", Offset = "0x1587A40", VA = "0x101587A40")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004267 RID: 16999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE3")]
		[Address(RVA = "0x101587A70", Offset = "0x1587A70", VA = "0x101587A70")]
		private void Start()
		{
		}

		// Token: 0x06004268 RID: 17000 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE4")]
		[Address(RVA = "0x101587E10", Offset = "0x1587E10", VA = "0x101587E10")]
		public void Destroy()
		{
		}

		// Token: 0x06004269 RID: 17001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE5")]
		[Address(RVA = "0x101587EC0", Offset = "0x1587EC0", VA = "0x101587EC0")]
		private void OnDestory()
		{
		}

		// Token: 0x0600426A RID: 17002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE6")]
		[Address(RVA = "0x101587AFC", Offset = "0x1587AFC", VA = "0x101587AFC")]
		public void ApplyMaterial(Material softMaskMaterial)
		{
		}

		// Token: 0x0600426B RID: 17003 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE7")]
		[Address(RVA = "0x1015880CC", Offset = "0x15880CC", VA = "0x1015880CC")]
		public void Init(Image img)
		{
		}

		// Token: 0x0600426C RID: 17004 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CE8")]
		[Address(RVA = "0x101587F98", Offset = "0x1587F98", VA = "0x101587F98")]
		private void GetCanvas()
		{
		}

		// Token: 0x0600426D RID: 17005 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003CE9")]
		[Address(RVA = "0x101588148", Offset = "0x1588148", VA = "0x101588148")]
		private Transform GetParentTransform(Transform t)
		{
			return null;
		}

		// Token: 0x0600426E RID: 17006 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CEA")]
		[Address(RVA = "0x101588178", Offset = "0x1588178", VA = "0x101588178")]
		private void LateUpdate()
		{
		}

		// Token: 0x0600426F RID: 17007 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CEB")]
		[Address(RVA = "0x10158817C", Offset = "0x158817C", VA = "0x10158817C")]
		private void SetMask()
		{
		}

		// Token: 0x06004270 RID: 17008 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CEC")]
		[Address(RVA = "0x101588F6C", Offset = "0x1588F6C", VA = "0x101588F6C")]
		public SoftMask()
		{
		}

		// Token: 0x04005270 RID: 21104
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A43")]
		[SerializeField]
		private Material m_SoftMaskMaterial;

		// Token: 0x04005271 RID: 21105
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A44")]
		public Image m_maskImage;

		// Token: 0x04005272 RID: 21106
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A45")]
		[Attribute(Name = "RangeAttribute", RVA = "0x1001279F8", Offset = "0x1279F8")]
		public float m_cutOff;

		// Token: 0x04005273 RID: 21107
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003A46")]
		private Material m_mat;

		// Token: 0x04005274 RID: 21108
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003A47")]
		private Canvas m_canvas;

		// Token: 0x04005275 RID: 21109
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A48")]
		private RectTransform m_myRect;

		// Token: 0x04005276 RID: 21110
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A49")]
		private RectTransform m_maskArea;

		// Token: 0x04005277 RID: 21111
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003A4A")]
		private Vector2 m_alphaUV;

		// Token: 0x04005278 RID: 21112
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A4B")]
		private Vector2 m_min;

		// Token: 0x04005279 RID: 21113
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A4C")]
		private Vector2 m_max;

		// Token: 0x0400527A RID: 21114
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A4D")]
		private Vector2 m_p;

		// Token: 0x0400527B RID: 21115
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A4E")]
		private Vector2 m_siz;

		// Token: 0x0400527C RID: 21116
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003A4F")]
		private Rect m_maskRect;

		// Token: 0x0400527D RID: 21117
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003A50")]
		private Rect m_contentRect;

		// Token: 0x0400527E RID: 21118
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003A51")]
		private Vector4 m_atlasUV;

		// Token: 0x0400527F RID: 21119
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003A52")]
		private Vector4 m_maskAtlasUV;

		// Token: 0x04005280 RID: 21120
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003A53")]
		private Vector2 m_center;

		// Token: 0x04005281 RID: 21121
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003A54")]
		private bool m_isText;

		// Token: 0x04005282 RID: 21122
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003A55")]
		private Image m_image;

		// Token: 0x04005283 RID: 21123
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003A56")]
		private bool m_init;
	}
}
