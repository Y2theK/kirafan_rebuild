﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0C RID: 3596
	[Token(Token = "0x20009A4")]
	[Attribute(Name = "RequireComponent", RVA = "0x10011D26C", Offset = "0x11D26C")]
	[StructLayout(3)]
	public class SoftMaskParent : MonoBehaviour
	{
		// Token: 0x06004271 RID: 17009 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CED")]
		[Address(RVA = "0x101588FE4", Offset = "0x1588FE4", VA = "0x101588FE4")]
		private void Start()
		{
		}

		// Token: 0x06004272 RID: 17010 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CEE")]
		[Address(RVA = "0x101588FE8", Offset = "0x1588FE8", VA = "0x101588FE8")]
		public void ApplyAllChildren()
		{
		}

		// Token: 0x06004273 RID: 17011 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CEF")]
		[Address(RVA = "0x101589188", Offset = "0x1589188", VA = "0x101589188")]
		public void AddChild(Image child)
		{
		}

		// Token: 0x06004274 RID: 17012 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF0")]
		[Address(RVA = "0x1015892A0", Offset = "0x15892A0", VA = "0x1015892A0")]
		public void AddChild(Text child)
		{
		}

		// Token: 0x06004275 RID: 17013 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF1")]
		[Address(RVA = "0x1015893B8", Offset = "0x15893B8", VA = "0x1015893B8")]
		public void RemoveChild(SoftMask removeChild)
		{
		}

		// Token: 0x06004276 RID: 17014 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF2")]
		[Address(RVA = "0x101589450", Offset = "0x1589450", VA = "0x101589450")]
		public SoftMaskParent()
		{
		}

		// Token: 0x04005284 RID: 21124
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A57")]
		[SerializeField]
		private Material m_SoftMaskMaterial;

		// Token: 0x04005285 RID: 21125
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A58")]
		[SerializeField]
		private Material m_SoftMaskMaterialText;

		// Token: 0x04005286 RID: 21126
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A59")]
		private Image m_Image;
	}
}
