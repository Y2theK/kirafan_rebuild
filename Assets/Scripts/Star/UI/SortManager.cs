﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CC1 RID: 3265
	[Token(Token = "0x20008BC")]
	[StructLayout(3)]
	public class SortManager
	{
		// Token: 0x06003BE3 RID: 15331 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A1")]
		[Address(RVA = "0x101589458", Offset = "0x1589458", VA = "0x101589458")]
		public void SetupDefault()
		{
		}

		// Token: 0x06003BE4 RID: 15332 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A2")]
		[Address(RVA = "0x1015895D4", Offset = "0x15895D4", VA = "0x1015895D4")]
		public void SetDefault(SortManager.eListType type)
		{
		}

		// Token: 0x06003BE5 RID: 15333 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x60036A3")]
		[Address(RVA = "0x1015895FC", Offset = "0x15895FC", VA = "0x1015895FC")]
		public SortManager.SortParam GetParam(SortManager.eListType type)
		{
			return null;
		}

		// Token: 0x06003BE6 RID: 15334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A4")]
		[Address(RVA = "0x10158957C", Offset = "0x158957C", VA = "0x10158957C")]
		private void SetDefault(SortManager.SortParam sortParam, SortManager.eListType type)
		{
		}

		// Token: 0x06003BE7 RID: 15335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60036A5")]
		[Address(RVA = "0x10158964C", Offset = "0x158964C", VA = "0x10158964C")]
		public SortManager()
		{
		}

		// Token: 0x04004ADB RID: 19163
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40034BE")]
		public SortManager.SortParam[] m_SortParams;

		// Token: 0x02000CC2 RID: 3266
		[Token(Token = "0x20010CC")]
		public enum eListType
		{
			// Token: 0x04004ADD RID: 19165
			[Token(Token = "0x400696D")]
			None = -1,
			// Token: 0x04004ADE RID: 19166
			[Token(Token = "0x400696E")]
			Chara,
			// Token: 0x04004ADF RID: 19167
			[Token(Token = "0x400696F")]
			Weapon,
			// Token: 0x04004AE0 RID: 19168
			[Token(Token = "0x4006970")]
			Support,
			// Token: 0x04004AE1 RID: 19169
			[Token(Token = "0x4006971")]
			Num
		}

		// Token: 0x02000CC3 RID: 3267
		[Token(Token = "0x20010CD")]
		public enum eSortOrderType
		{
			// Token: 0x04004AE3 RID: 19171
			[Token(Token = "0x4006973")]
			Ascending,
			// Token: 0x04004AE4 RID: 19172
			[Token(Token = "0x4006974")]
			Descending,
			// Token: 0x04004AE5 RID: 19173
			[Token(Token = "0x4006975")]
			Num
		}

		// Token: 0x02000CC4 RID: 3268
		[Token(Token = "0x20010CE")]
		public enum eSortCategoryChara
		{
			// Token: 0x04004AE7 RID: 19175
			[Token(Token = "0x4006977")]
			Level,
			// Token: 0x04004AE8 RID: 19176
			[Token(Token = "0x4006978")]
			Rare,
			// Token: 0x04004AE9 RID: 19177
			[Token(Token = "0x4006979")]
			Cost,
			// Token: 0x04004AEA RID: 19178
			[Token(Token = "0x400697A")]
			Hp,
			// Token: 0x04004AEB RID: 19179
			[Token(Token = "0x400697B")]
			Atk,
			// Token: 0x04004AEC RID: 19180
			[Token(Token = "0x400697C")]
			Mgc,
			// Token: 0x04004AED RID: 19181
			[Token(Token = "0x400697D")]
			Def,
			// Token: 0x04004AEE RID: 19182
			[Token(Token = "0x400697E")]
			MDef,
			// Token: 0x04004AEF RID: 19183
			[Token(Token = "0x400697F")]
			Spd,
			// Token: 0x04004AF0 RID: 19184
			[Token(Token = "0x4006980")]
			Title,
			// Token: 0x04004AF1 RID: 19185
			[Token(Token = "0x4006981")]
			GetTime
		}

		// Token: 0x02000CC5 RID: 3269
		[Token(Token = "0x20010CF")]
		public enum eSortCategoryWeapon
		{
			// Token: 0x04004AF3 RID: 19187
			[Token(Token = "0x4006983")]
			Level,
			// Token: 0x04004AF4 RID: 19188
			[Token(Token = "0x4006984")]
			Rare,
			// Token: 0x04004AF5 RID: 19189
			[Token(Token = "0x4006985")]
			Cost,
			// Token: 0x04004AF6 RID: 19190
			[Token(Token = "0x4006986")]
			Atk,
			// Token: 0x04004AF7 RID: 19191
			[Token(Token = "0x4006987")]
			Mgc,
			// Token: 0x04004AF8 RID: 19192
			[Token(Token = "0x4006988")]
			Def,
			// Token: 0x04004AF9 RID: 19193
			[Token(Token = "0x4006989")]
			MDef,
			// Token: 0x04004AFA RID: 19194
			[Token(Token = "0x400698A")]
			Get
		}

		// Token: 0x02000CC6 RID: 3270
		[Token(Token = "0x20010D0")]
		public enum eSortCategorySupport
		{
			// Token: 0x04004AFC RID: 19196
			[Token(Token = "0x400698C")]
			Level,
			// Token: 0x04004AFD RID: 19197
			[Token(Token = "0x400698D")]
			Rare,
			// Token: 0x04004AFE RID: 19198
			[Token(Token = "0x400698E")]
			Cost,
			// Token: 0x04004AFF RID: 19199
			[Token(Token = "0x400698F")]
			HP,
			// Token: 0x04004B00 RID: 19200
			[Token(Token = "0x4006990")]
			Atk,
			// Token: 0x04004B01 RID: 19201
			[Token(Token = "0x4006991")]
			Mgc,
			// Token: 0x04004B02 RID: 19202
			[Token(Token = "0x4006992")]
			Def,
			// Token: 0x04004B03 RID: 19203
			[Token(Token = "0x4006993")]
			MDef,
			// Token: 0x04004B04 RID: 19204
			[Token(Token = "0x4006994")]
			Spd
		}

		// Token: 0x02000CC7 RID: 3271
		[Token(Token = "0x20010D1")]
		public class SortParam
		{
			// Token: 0x06003BE8 RID: 15336 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x60061F5")]
			[Address(RVA = "0x101589574", Offset = "0x1589574", VA = "0x101589574")]
			public SortParam()
			{
			}

			// Token: 0x04004B05 RID: 19205
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x4006995")]
			public SortManager.eListType m_Type;

			// Token: 0x04004B06 RID: 19206
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x4006996")]
			public int m_Idx;

			// Token: 0x04004B07 RID: 19207
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006997")]
			public SortManager.eSortOrderType m_OrderType;
		}
	}
}
