﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Filter;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000DB2 RID: 3506
	[Token(Token = "0x2000964")]
	[StructLayout(3)]
	public class SortUI : MenuUIBase
	{
		// Token: 0x06004099 RID: 16537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B27")]
		[Address(RVA = "0x1015896AC", Offset = "0x15896AC", VA = "0x1015896AC")]
		public void Setup(UISettings.eUsingType_Sort usingType, Action OnExecute)
		{
		}

		// Token: 0x0600409A RID: 16538 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B28")]
		[Address(RVA = "0x101589714", Offset = "0x1589714", VA = "0x101589714")]
		private void Update()
		{
		}

		// Token: 0x0600409B RID: 16539 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B29")]
		[Address(RVA = "0x1015898C4", Offset = "0x15898C4", VA = "0x1015898C4")]
		private void ChangeStep(SortUI.eStep step)
		{
		}

		// Token: 0x0600409C RID: 16540 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B2A")]
		[Address(RVA = "0x1015899CC", Offset = "0x15899CC", VA = "0x1015899CC", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600409D RID: 16541 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B2B")]
		[Address(RVA = "0x101589A88", Offset = "0x1589A88", VA = "0x101589A88", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600409E RID: 16542 RVA: 0x00019080 File Offset: 0x00017280
		[Token(Token = "0x6003B2C")]
		[Address(RVA = "0x101589B28", Offset = "0x1589B28", VA = "0x101589B28", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600409F RID: 16543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003B2D")]
		[Address(RVA = "0x101589B38", Offset = "0x1589B38", VA = "0x101589B38")]
		public SortUI()
		{
		}

		// Token: 0x04004FC3 RID: 20419
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003833")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04004FC4 RID: 20420
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003834")]
		[SerializeField]
		private SortWindow m_SortWindow;

		// Token: 0x04004FC5 RID: 20421
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003835")]
		private SortUI.eStep m_Step;

		// Token: 0x04004FC6 RID: 20422
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003836")]
		public Action OnStartOut;

		// Token: 0x02000DB3 RID: 3507
		[Token(Token = "0x2001114")]
		private enum eStep
		{
			// Token: 0x04004FC8 RID: 20424
			[Token(Token = "0x4006ADA")]
			Hide,
			// Token: 0x04004FC9 RID: 20425
			[Token(Token = "0x4006ADB")]
			In,
			// Token: 0x04004FCA RID: 20426
			[Token(Token = "0x4006ADC")]
			Idle,
			// Token: 0x04004FCB RID: 20427
			[Token(Token = "0x4006ADD")]
			Out,
			// Token: 0x04004FCC RID: 20428
			[Token(Token = "0x4006ADE")]
			End
		}
	}
}
