﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000DFF RID: 3583
	[Token(Token = "0x200099D")]
	[StructLayout(3)]
	public class SortUIArg : OverlayUIArgBase
	{
		// Token: 0x06004231 RID: 16945 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CB5")]
		[Address(RVA = "0x101589B40", Offset = "0x1589B40", VA = "0x101589B40")]
		public SortUIArg(UISettings.eUsingType_Sort usingType, Action OnExecute)
		{
		}

		// Token: 0x04005231 RID: 21041
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x4003A25")]
		public UISettings.eUsingType_Sort m_UsingType;

		// Token: 0x04005232 RID: 21042
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A26")]
		public Action m_OnFilterExecute;
	}
}
