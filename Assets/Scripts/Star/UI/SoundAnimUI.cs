﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C71 RID: 3185
	[Token(Token = "0x200088E")]
	[StructLayout(3)]
	public class SoundAnimUI : AnimUIBase
	{
		// Token: 0x060039EC RID: 14828 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D3")]
		[Address(RVA = "0x101589B7C", Offset = "0x1589B7C", VA = "0x101589B7C", Slot = "9")]
		protected override void ExecutePlayIn()
		{
		}

		// Token: 0x060039ED RID: 14829 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D4")]
		[Address(RVA = "0x101589B8C", Offset = "0x1589B8C", VA = "0x101589B8C", Slot = "10")]
		protected override void ExecutePlayOut()
		{
		}

		// Token: 0x060039EE RID: 14830 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D5")]
		[Address(RVA = "0x101589B9C", Offset = "0x1589B9C", VA = "0x101589B9C", Slot = "5")]
		public override void Hide()
		{
		}

		// Token: 0x060039EF RID: 14831 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D6")]
		[Address(RVA = "0x101589BA4", Offset = "0x1589BA4", VA = "0x101589BA4", Slot = "4")]
		public override void Show()
		{
		}

		// Token: 0x060039F0 RID: 14832 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D7")]
		[Address(RVA = "0x101589BAC", Offset = "0x1589BAC", VA = "0x101589BAC", Slot = "8")]
		protected override void Update()
		{
		}

		// Token: 0x060039F1 RID: 14833 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60034D8")]
		[Address(RVA = "0x101589D1C", Offset = "0x1589D1C", VA = "0x101589D1C")]
		public SoundAnimUI()
		{
		}

		// Token: 0x040048C7 RID: 18631
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400332F")]
		[SerializeField]
		private eSoundSeListDB m_InSound;

		// Token: 0x040048C8 RID: 18632
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003330")]
		[SerializeField]
		private float m_DelayInSe;

		// Token: 0x040048C9 RID: 18633
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003331")]
		[SerializeField]
		private eSoundSeListDB m_OutSound;

		// Token: 0x040048CA RID: 18634
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x4003332")]
		[SerializeField]
		private float m_DelayOutSe;

		// Token: 0x040048CB RID: 18635
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003333")]
		private bool m_ReservePlayIn;

		// Token: 0x040048CC RID: 18636
		[Cpp2IlInjected.FieldOffset(Offset = "0x41")]
		[Token(Token = "0x4003334")]
		private bool m_ReservePlayOut;

		// Token: 0x040048CD RID: 18637
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003335")]
		private float m_TimeCount;
	}
}
