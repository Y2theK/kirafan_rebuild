﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E1F RID: 3615
	[Token(Token = "0x20009AE")]
	[StructLayout(3)]
	public class SpecialTweet : MonoBehaviour
	{
		// Token: 0x060042EF RID: 17135 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D67")]
		[Address(RVA = "0x101589D24", Offset = "0x1589D24", VA = "0x101589D24")]
		private void OnDestroy()
		{
		}

		// Token: 0x060042F0 RID: 17136 RVA: 0x00019608 File Offset: 0x00017808
		[Token(Token = "0x6003D68")]
		[Address(RVA = "0x101589D34", Offset = "0x1589D34", VA = "0x101589D34")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x060042F1 RID: 17137 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D69")]
		[Address(RVA = "0x101589D44", Offset = "0x1589D44", VA = "0x101589D44")]
		public void Play(long mngID, int tweetID)
		{
		}

		// Token: 0x060042F2 RID: 17138 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D6A")]
		[Address(RVA = "0x101589F90", Offset = "0x1589F90", VA = "0x101589F90")]
		public void PlayVoiceOnly(long mngID, eSoundVoiceListDB voiceID)
		{
		}

		// Token: 0x060042F3 RID: 17139 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D6B")]
		[Address(RVA = "0x10158A15C", Offset = "0x158A15C", VA = "0x10158A15C")]
		private void Update()
		{
		}

		// Token: 0x060042F4 RID: 17140 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D6C")]
		[Address(RVA = "0x10158A59C", Offset = "0x158A59C", VA = "0x10158A59C")]
		private void PlayOut()
		{
		}

		// Token: 0x060042F5 RID: 17141 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D6D")]
		[Address(RVA = "0x10158A644", Offset = "0x158A644", VA = "0x10158A644")]
		public void OnClickSkipButtonCallBack()
		{
		}

		// Token: 0x060042F6 RID: 17142 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D6E")]
		[Address(RVA = "0x10158A658", Offset = "0x158A658", VA = "0x10158A658")]
		public SpecialTweet()
		{
		}

		// Token: 0x0400533B RID: 21307
		[Token(Token = "0x4003ADE")]
		private const float SKIP_TIME = 2f;

		// Token: 0x0400533C RID: 21308
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003ADF")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x0400533D RID: 21309
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003AE0")]
		[SerializeField]
		private AnimUIPlayer m_DarkBG;

		// Token: 0x0400533E RID: 21310
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003AE1")]
		[SerializeField]
		private AnimUIPlayer m_CharaAnim;

		// Token: 0x0400533F RID: 21311
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003AE2")]
		[SerializeField]
		private AnimUIPlayer m_TextAnim;

		// Token: 0x04005340 RID: 21312
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003AE3")]
		[SerializeField]
		private Text m_TalkText;

		// Token: 0x04005341 RID: 21313
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003AE4")]
		private float m_TimeCount;

		// Token: 0x04005342 RID: 21314
		[Cpp2IlInjected.FieldOffset(Offset = "0x44")]
		[Token(Token = "0x4003AE5")]
		private int m_CharaID;

		// Token: 0x04005343 RID: 21315
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003AE6")]
		private bool m_IsVoiceOnly;

		// Token: 0x04005344 RID: 21316
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003AE7")]
		private string m_VoiceCueName;

		// Token: 0x04005345 RID: 21317
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003AE8")]
		private eSoundVoiceListDB m_VoiceID;

		// Token: 0x04005346 RID: 21318
		[Cpp2IlInjected.FieldOffset(Offset = "0x5C")]
		[Token(Token = "0x4003AE9")]
		private int m_ReqVocieID;

		// Token: 0x04005347 RID: 21319
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003AEA")]
		private SpecialTweet.eStep m_Step;

		// Token: 0x04005348 RID: 21320
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003AEB")]
		public Action OnComplete;

		// Token: 0x02000E20 RID: 3616
		[Token(Token = "0x2001137")]
		public enum eStep
		{
			// Token: 0x0400534A RID: 21322
			[Token(Token = "0x4006BA7")]
			None,
			// Token: 0x0400534B RID: 21323
			[Token(Token = "0x4006BA8")]
			Loading,
			// Token: 0x0400534C RID: 21324
			[Token(Token = "0x4006BA9")]
			Play,
			// Token: 0x0400534D RID: 21325
			[Token(Token = "0x4006BAA")]
			PlayOut
		}
	}
}
