﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0D RID: 3597
	[Token(Token = "0x20009A5")]
	[StructLayout(3)]
	public class StaminaConfirmGroup : UIGroup
	{
		// Token: 0x1400007B RID: 123
		// (add) Token: 0x06004277 RID: 17015 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004278 RID: 17016 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007B")]
		public event Action<int, int> OnConfirm
		{
			[Token(Token = "0x6003CF3")]
			[Address(RVA = "0x10158A66C", Offset = "0x158A66C", VA = "0x10158A66C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003CF4")]
			[Address(RVA = "0x10158A778", Offset = "0x158A778", VA = "0x10158A778")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004279 RID: 17017 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF5")]
		[Address(RVA = "0x10158A884", Offset = "0x158A884", VA = "0x10158A884")]
		public void Apply(int itemID, int useNum = 1)
		{
		}

		// Token: 0x0600427A RID: 17018 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF6")]
		[Address(RVA = "0x10158AFF0", Offset = "0x158AFF0", VA = "0x10158AFF0")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x0600427B RID: 17019 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF7")]
		[Address(RVA = "0x10158B044", Offset = "0x158B044", VA = "0x10158B044")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x0600427C RID: 17020 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CF8")]
		[Address(RVA = "0x10158B050", Offset = "0x158B050", VA = "0x10158B050")]
		public StaminaConfirmGroup()
		{
		}

		// Token: 0x04005287 RID: 21127
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003A5A")]
		private int m_ItemID;

		// Token: 0x04005288 RID: 21128
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003A5B")]
		private int m_UseNum;

		// Token: 0x04005289 RID: 21129
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003A5C")]
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x0400528A RID: 21130
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003A5D")]
		[SerializeField]
		private Text m_BeforeText;

		// Token: 0x0400528B RID: 21131
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003A5E")]
		[SerializeField]
		private Text m_AfterText;
	}
}
