﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0E RID: 3598
	[Token(Token = "0x20009A6")]
	[StructLayout(3)]
	public class StaminaItemConfirmGroup : UIGroup
	{
		// Token: 0x1400007C RID: 124
		// (add) Token: 0x0600427D RID: 17021 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x0600427E RID: 17022 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007C")]
		public event Action<int, int> OnConfirm
		{
			[Token(Token = "0x6003CF9")]
			[Address(RVA = "0x10158B060", Offset = "0x158B060", VA = "0x10158B060")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003CFA")]
			[Address(RVA = "0x10158B16C", Offset = "0x158B16C", VA = "0x10158B16C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x0600427F RID: 17023 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CFB")]
		[Address(RVA = "0x10158B278", Offset = "0x158B278", VA = "0x10158B278")]
		public void Setup()
		{
		}

		// Token: 0x06004280 RID: 17024 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CFC")]
		[Address(RVA = "0x10158B324", Offset = "0x158B324", VA = "0x10158B324")]
		public void Apply(int itemID)
		{
		}

		// Token: 0x06004281 RID: 17025 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CFD")]
		[Address(RVA = "0x10158B580", Offset = "0x158B580", VA = "0x10158B580")]
		private void UpdateValue()
		{
		}

		// Token: 0x06004282 RID: 17026 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CFE")]
		[Address(RVA = "0x10158B90C", Offset = "0x158B90C", VA = "0x10158B90C")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x06004283 RID: 17027 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003CFF")]
		[Address(RVA = "0x10158B960", Offset = "0x158B960", VA = "0x10158B960")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x06004284 RID: 17028 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D00")]
		[Address(RVA = "0x10158B96C", Offset = "0x158B96C", VA = "0x10158B96C")]
		private void SliderValueCangeCallBack()
		{
		}

		// Token: 0x06004285 RID: 17029 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D01")]
		[Address(RVA = "0x10158B970", Offset = "0x158B970", VA = "0x10158B970")]
		public StaminaItemConfirmGroup()
		{
		}

		// Token: 0x0400528D RID: 21133
		[Token(Token = "0x4003A60")]
		private const int MIN_VALUE = 1;

		// Token: 0x0400528E RID: 21134
		[Token(Token = "0x4003A61")]
		private const int MAX_VALUE = 99;

		// Token: 0x0400528F RID: 21135
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4003A62")]
		private int m_ItemID;

		// Token: 0x04005290 RID: 21136
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4003A63")]
		private int m_UseNum;

		// Token: 0x04005291 RID: 21137
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4003A64")]
		[SerializeField]
		private Text m_BeforeText;

		// Token: 0x04005292 RID: 21138
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4003A65")]
		[SerializeField]
		private Text m_AfterText;

		// Token: 0x04005293 RID: 21139
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4003A66")]
		[SerializeField]
		private Text m_ItemNameText;

		// Token: 0x04005294 RID: 21140
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4003A67")]
		[SerializeField]
		private Text m_ItemNumText;

		// Token: 0x04005295 RID: 21141
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4003A68")]
		[SerializeField]
		private Text m_UseNumText;

		// Token: 0x04005296 RID: 21142
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003A69")]
		[SerializeField]
		private VariableIconWithFrame m_ItemIcon;

		// Token: 0x04005297 RID: 21143
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003A6A")]
		[SerializeField]
		private RangeSelector m_ItemNumSlider;
	}
}
