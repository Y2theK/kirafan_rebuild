﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000E0F RID: 3599
	[Token(Token = "0x20009A7")]
	[StructLayout(3)]
	public class StaminaRecipeItem : ScrollItemIcon
	{
		// Token: 0x06004286 RID: 17030 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D02")]
		[Address(RVA = "0x10158B980", Offset = "0x158B980", VA = "0x10158B980", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004287 RID: 17031 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D03")]
		[Address(RVA = "0x10158BBDC", Offset = "0x158BBDC", VA = "0x10158BBDC")]
		public StaminaRecipeItem()
		{
		}

		// Token: 0x04005299 RID: 21145
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A6C")]
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x0400529A RID: 21146
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A6D")]
		[SerializeField]
		private Text m_TitleNameText;

		// Token: 0x0400529B RID: 21147
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A6E")]
		[SerializeField]
		private Text m_HavenumText;

		// Token: 0x0400529C RID: 21148
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A6F")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400529D RID: 21149
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x4003A70")]
		[SerializeField]
		private GameObject m_GemNumObj;
	}
}
