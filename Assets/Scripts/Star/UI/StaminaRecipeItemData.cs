﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000E10 RID: 3600
	[Token(Token = "0x20009A8")]
	[StructLayout(3)]
	public class StaminaRecipeItemData : ScrollItemData
	{
		// Token: 0x06004288 RID: 17032 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D04")]
		[Address(RVA = "0x10158BBE4", Offset = "0x158BBE4", VA = "0x10158BBE4")]
		public StaminaRecipeItemData(int itemID, Action<int> onClick)
		{
		}

		// Token: 0x06004289 RID: 17033 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D05")]
		[Address(RVA = "0x10158C250", Offset = "0x158C250", VA = "0x10158C250", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x0400529E RID: 21150
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003A71")]
		public int ITEM_USE_NUM;

		// Token: 0x0400529F RID: 21151
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003A72")]
		public bool m_Interactable;

		// Token: 0x040052A0 RID: 21152
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A73")]
		public string m_Title;

		// Token: 0x040052A1 RID: 21153
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A74")]
		public int m_ItemID;

		// Token: 0x040052A2 RID: 21154
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003A75")]
		public string m_HaveNumText;

		// Token: 0x040052A3 RID: 21155
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003A76")]
		public string m_DetailText;

		// Token: 0x040052A4 RID: 21156
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A77")]
		private Action<int> OnClickCallBack;
	}
}
