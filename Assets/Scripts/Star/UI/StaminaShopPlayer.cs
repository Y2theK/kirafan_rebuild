﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Meige;

namespace Star.UI
{
	// Token: 0x02000E11 RID: 3601
	[Token(Token = "0x20009A9")]
	[StructLayout(3)]
	public class StaminaShopPlayer : SingletonMonoBehaviour<StaminaShopPlayer>
	{
		// Token: 0x0600428A RID: 17034 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D06")]
		[Address(RVA = "0x10158C2A4", Offset = "0x158C2A4", VA = "0x10158C2A4", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x0600428B RID: 17035 RVA: 0x000194B8 File Offset: 0x000176B8
		[Token(Token = "0x6003D07")]
		[Address(RVA = "0x10158C2F4", Offset = "0x158C2F4", VA = "0x10158C2F4")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x0600428C RID: 17036 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D08")]
		[Address(RVA = "0x10158C304", Offset = "0x158C304", VA = "0x10158C304")]
		public void Open(int type, Action OnEnd)
		{
		}

		// Token: 0x0600428D RID: 17037 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D09")]
		[Address(RVA = "0x10158C400", Offset = "0x158C400", VA = "0x10158C400")]
		public void Close()
		{
		}

		// Token: 0x0600428E RID: 17038 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D0A")]
		[Address(RVA = "0x10158C4B8", Offset = "0x158C4B8", VA = "0x10158C4B8")]
		public void ForceHide()
		{
		}

		// Token: 0x0600428F RID: 17039 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D0B")]
		[Address(RVA = "0x10158C4C0", Offset = "0x158C4C0", VA = "0x10158C4C0")]
		private void Update()
		{
		}

		// Token: 0x06004290 RID: 17040 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D0C")]
		[Address(RVA = "0x10158CB6C", Offset = "0x158CB6C", VA = "0x10158CB6C")]
		private void OnRequestCallBack(int itemID, int useNum)
		{
		}

		// Token: 0x06004291 RID: 17041 RVA: 0x000194D0 File Offset: 0x000176D0
		[Token(Token = "0x6003D0D")]
		[Address(RVA = "0x10158CB70", Offset = "0x158CB70", VA = "0x10158CB70")]
		private bool Request_StaminaAdd(int itemID, int useNum)
		{
			return default(bool);
		}

		// Token: 0x06004292 RID: 17042 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D0E")]
		[Address(RVA = "0x10158CD28", Offset = "0x158CD28", VA = "0x10158CD28")]
		private void OnResponse_StaminaAdd(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x06004293 RID: 17043 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D0F")]
		[Address(RVA = "0x10158D0C4", Offset = "0x158D0C4", VA = "0x10158D0C4")]
		public StaminaShopPlayer()
		{
		}

		// Token: 0x040052A5 RID: 21157
		[Token(Token = "0x4003A78")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.StaminaShopUI;

		// Token: 0x040052A6 RID: 21158
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x4003A79")]
		private StaminaShopPlayer.eStep m_Step;

		// Token: 0x040052A7 RID: 21159
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003A7A")]
		private StaminaShopUI m_UI;

		// Token: 0x040052A8 RID: 21160
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003A7B")]
		private Action m_OnEndCallBack;

		// Token: 0x02000E12 RID: 3602
		[Token(Token = "0x200112E")]
		private enum eStep
		{
			// Token: 0x040052AA RID: 21162
			[Token(Token = "0x4006B77")]
			Hide,
			// Token: 0x040052AB RID: 21163
			[Token(Token = "0x4006B78")]
			LoadUI,
			// Token: 0x040052AC RID: 21164
			[Token(Token = "0x4006B79")]
			LoadWait,
			// Token: 0x040052AD RID: 21165
			[Token(Token = "0x4006B7A")]
			SetupAndPlayIn,
			// Token: 0x040052AE RID: 21166
			[Token(Token = "0x4006B7B")]
			Main,
			// Token: 0x040052AF RID: 21167
			[Token(Token = "0x4006B7C")]
			UnloadWait
		}
	}
}
