﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000E13 RID: 3603
	[Token(Token = "0x20009AA")]
	[StructLayout(3)]
	public class StaminaShopUI : MenuUIBase
	{
		// Token: 0x170004DC RID: 1244
		// (get) Token: 0x06004294 RID: 17044 RVA: 0x000194E8 File Offset: 0x000176E8
		[Token(Token = "0x17000487")]
		public StaminaShopUI.eButton SelectButton
		{
			[Token(Token = "0x6003D10")]
			[Address(RVA = "0x10158D114", Offset = "0x158D114", VA = "0x10158D114")]
			get
			{
				return StaminaShopUI.eButton.Back;
			}
		}

		// Token: 0x1400007D RID: 125
		// (add) Token: 0x06004295 RID: 17045 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004296 RID: 17046 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1400007D")]
		public event Action<int, int> OnRequest
		{
			[Token(Token = "0x6003D11")]
			[Address(RVA = "0x10158CA60", Offset = "0x158CA60", VA = "0x10158CA60")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003D12")]
			[Address(RVA = "0x10158D11C", Offset = "0x158D11C", VA = "0x10158D11C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004297 RID: 17047 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D13")]
		[Address(RVA = "0x10158C7EC", Offset = "0x158C7EC", VA = "0x10158C7EC")]
		public void Setup()
		{
		}

		// Token: 0x06004298 RID: 17048 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D14")]
		[Address(RVA = "0x10158CE60", Offset = "0x158CE60", VA = "0x10158CE60")]
		public void Refresh()
		{
		}

		// Token: 0x06004299 RID: 17049 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D15")]
		[Address(RVA = "0x10158D228", Offset = "0x158D228", VA = "0x10158D228")]
		private void Update()
		{
		}

		// Token: 0x0600429A RID: 17050 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D16")]
		[Address(RVA = "0x10158D3F8", Offset = "0x158D3F8", VA = "0x10158D3F8")]
		private void ChangeStep(StaminaShopUI.eStep step)
		{
		}

		// Token: 0x0600429B RID: 17051 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D17")]
		[Address(RVA = "0x10158D520", Offset = "0x158D520", VA = "0x10158D520", Slot = "4")]
		public override void PlayIn()
		{
		}

		// Token: 0x0600429C RID: 17052 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D18")]
		[Address(RVA = "0x10158D5DC", Offset = "0x158D5DC", VA = "0x10158D5DC", Slot = "5")]
		public override void PlayOut()
		{
		}

		// Token: 0x0600429D RID: 17053 RVA: 0x00019500 File Offset: 0x00017700
		[Token(Token = "0x6003D19")]
		[Address(RVA = "0x10158D698", Offset = "0x158D698", VA = "0x10158D698", Slot = "6")]
		public override bool IsMenuEnd()
		{
			return default(bool);
		}

		// Token: 0x0600429E RID: 17054 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1A")]
		[Address(RVA = "0x10158D6A8", Offset = "0x158D6A8", VA = "0x10158D6A8")]
		public void OnCloseButtonCallBack()
		{
		}

		// Token: 0x0600429F RID: 17055 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1B")]
		[Address(RVA = "0x10158D6B4", Offset = "0x158D6B4", VA = "0x10158D6B4")]
		public void OnClickRecipeButtonCallBack(int itemID)
		{
		}

		// Token: 0x060042A0 RID: 17056 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1C")]
		[Address(RVA = "0x10158D794", Offset = "0x158D794", VA = "0x10158D794")]
		public void OnConfirmCallBack(int itemID, int useNum)
		{
		}

		// Token: 0x060042A1 RID: 17057 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1D")]
		[Address(RVA = "0x10158D81C", Offset = "0x158D81C", VA = "0x10158D81C")]
		public void OnClickItemRecipeButtonCallBack(int itemID)
		{
		}

		// Token: 0x060042A2 RID: 17058 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1E")]
		[Address(RVA = "0x10158D87C", Offset = "0x158D87C", VA = "0x10158D87C")]
		public void OnItemConfirmCallBack(int itemID, int useNum)
		{
		}

		// Token: 0x060042A3 RID: 17059 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D1F")]
		[Address(RVA = "0x10158D904", Offset = "0x158D904", VA = "0x10158D904")]
		public void CloseConfirm()
		{
		}

		// Token: 0x060042A4 RID: 17060 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003D20")]
		[Address(RVA = "0x10158D938", Offset = "0x158D938", VA = "0x10158D938")]
		public StaminaShopUI()
		{
		}

		// Token: 0x040052B0 RID: 21168
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003A7C")]
		private StaminaShopUI.eStep m_Step;

		// Token: 0x040052B1 RID: 21169
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003A7D")]
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040052B2 RID: 21170
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003A7E")]
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040052B3 RID: 21171
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003A7F")]
		[SerializeField]
		private StaminaConfirmGroup m_ConfirmGroup;

		// Token: 0x040052B4 RID: 21172
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003A80")]
		[SerializeField]
		private StaminaItemConfirmGroup m_ItemConfirmGroup;

		// Token: 0x040052B5 RID: 21173
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003A81")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x040052B6 RID: 21174
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003A82")]
		private StaminaShopUI.eButton m_SelectButton;

		// Token: 0x02000E14 RID: 3604
		[Token(Token = "0x200112F")]
		private enum eStep
		{
			// Token: 0x040052B9 RID: 21177
			[Token(Token = "0x4006B7E")]
			Hide,
			// Token: 0x040052BA RID: 21178
			[Token(Token = "0x4006B7F")]
			In,
			// Token: 0x040052BB RID: 21179
			[Token(Token = "0x4006B80")]
			Idle,
			// Token: 0x040052BC RID: 21180
			[Token(Token = "0x4006B81")]
			Out,
			// Token: 0x040052BD RID: 21181
			[Token(Token = "0x4006B82")]
			End
		}

		// Token: 0x02000E15 RID: 3605
		[Token(Token = "0x2001130")]
		public enum eButton
		{
			// Token: 0x040052BF RID: 21183
			[Token(Token = "0x4006B84")]
			None = -1,
			// Token: 0x040052C0 RID: 21184
			[Token(Token = "0x4006B85")]
			Back
		}
	}
}
