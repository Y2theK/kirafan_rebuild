﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D79 RID: 3449
	[Token(Token = "0x200093C")]
	[StructLayout(3)]
	public class SubOfferDetailWindow : UIGroup
	{
		// Token: 0x06003F93 RID: 16275 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3C")]
		[Address(RVA = "0x10158DA1C", Offset = "0x158DA1C", VA = "0x10158DA1C")]
		public void SetupParam(OfferManager.OfferData offerData, Action<long> onClickDecideButtonCallback)
		{
		}

		// Token: 0x06003F94 RID: 16276 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3D")]
		[Address(RVA = "0x10158E9AC", Offset = "0x158E9AC", VA = "0x10158E9AC")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06003F95 RID: 16277 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3E")]
		[Address(RVA = "0x10158E9FC", Offset = "0x158E9FC", VA = "0x10158E9FC")]
		public void OnClickCancelButton()
		{
		}

		// Token: 0x06003F96 RID: 16278 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A3F")]
		[Address(RVA = "0x10158EA08", Offset = "0x158EA08", VA = "0x10158EA08")]
		public void OnClickCloseButton()
		{
		}

		// Token: 0x06003F97 RID: 16279 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A40")]
		[Address(RVA = "0x10158EA14", Offset = "0x158EA14", VA = "0x10158EA14")]
		public SubOfferDetailWindow()
		{
		}

		// Token: 0x04004EDC RID: 20188
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400378A")]
		[SerializeField]
		private Text m_NumberText;

		// Token: 0x04004EDD RID: 20189
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400378B")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x04004EDE RID: 20190
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x400378C")]
		[SerializeField]
		private Text m_ConditionText;

		// Token: 0x04004EDF RID: 20191
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400378D")]
		[SerializeField]
		private VariableIconWithFrame m_RewardIcon;

		// Token: 0x04004EE0 RID: 20192
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400378E")]
		[SerializeField]
		private Text m_RewardText;

		// Token: 0x04004EE1 RID: 20193
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400378F")]
		[SerializeField]
		private Text m_RewardBlankText;

		// Token: 0x04004EE2 RID: 20194
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4003790")]
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04004EE3 RID: 20195
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4003791")]
		[SerializeField]
		private GameObject m_DecideButton;

		// Token: 0x04004EE4 RID: 20196
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4003792")]
		[SerializeField]
		private GameObject m_CancelButton;

		// Token: 0x04004EE5 RID: 20197
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4003793")]
		[SerializeField]
		private GameObject m_CloseButton;

		// Token: 0x04004EE6 RID: 20198
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4003794")]
		private long m_CharaOfferMngID;

		// Token: 0x04004EE7 RID: 20199
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4003795")]
		private Action<long> m_OnClickDecideButtonCallback;
	}
}
