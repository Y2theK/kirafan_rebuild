﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D7A RID: 3450
	[Token(Token = "0x200093D")]
	[StructLayout(3)]
	public class SubOfferListItemIcon : ScrollItemIcon
	{
		// Token: 0x06003F98 RID: 16280 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A41")]
		[Address(RVA = "0x10158EA24", Offset = "0x158EA24", VA = "0x10158EA24", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06003F99 RID: 16281 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A42")]
		[Address(RVA = "0x10158EDD8", Offset = "0x158EDD8", VA = "0x10158EDD8")]
		public SubOfferListItemIcon()
		{
		}

		// Token: 0x04004EE8 RID: 20200
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x4003796")]
		[SerializeField]
		private GameObject m_ContentObject;

		// Token: 0x04004EE9 RID: 20201
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x4003797")]
		[SerializeField]
		private Text m_NumberText;

		// Token: 0x04004EEA RID: 20202
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x4003798")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x04004EEB RID: 20203
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x4003799")]
		[SerializeField]
		private OfferStateIcon m_OfferStateIcon;

		// Token: 0x04004EEC RID: 20204
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x400379A")]
		[SerializeField]
		private Text m_LabelText;

		// Token: 0x04004EED RID: 20205
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x400379B")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04004EEE RID: 20206
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400379C")]
		[SerializeField]
		private GameObject m_LockObject;

		// Token: 0x04004EEF RID: 20207
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400379D")]
		[SerializeField]
		private Text m_LockText;

		// Token: 0x02000D7B RID: 3451
		[Token(Token = "0x2001103")]
		public class SubOfferListItemData : ScrollItemData
		{
			// Token: 0x06003F9A RID: 16282 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620A")]
			[Address(RVA = "0x10158EDE0", Offset = "0x158EDE0", VA = "0x10158EDE0", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x06003F9B RID: 16283 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620B")]
			[Address(RVA = "0x10158EE34", Offset = "0x158EE34", VA = "0x10158EE34")]
			public SubOfferListItemData()
			{
			}

			// Token: 0x04004EF0 RID: 20208
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A9B")]
			public OfferManager.OfferData m_OfferData;

			// Token: 0x04004EF1 RID: 20209
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006A9C")]
			public bool m_IsDark;

			// Token: 0x04004EF2 RID: 20210
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006A9D")]
			public Action<OfferManager.OfferData> OnClickItem;
		}
	}
}
