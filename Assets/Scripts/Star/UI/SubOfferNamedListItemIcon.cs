﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D7C RID: 3452
	[Token(Token = "0x200093E")]
	[StructLayout(3)]
	public class SubOfferNamedListItemIcon : ScrollItemIcon
	{
		// Token: 0x06003F9C RID: 16284 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A43")]
		[Address(RVA = "0x10158EE3C", Offset = "0x158EE3C", VA = "0x10158EE3C", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06003F9D RID: 16285 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A44")]
		[Address(RVA = "0x10158EE70", Offset = "0x158EE70", VA = "0x10158EE70", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06003F9E RID: 16286 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A45")]
		[Address(RVA = "0x10158F1BC", Offset = "0x158F1BC", VA = "0x10158F1BC")]
		public SubOfferNamedListItemIcon()
		{
		}

		// Token: 0x04004EF3 RID: 20211
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400379E")]
		[SerializeField]
		private Text m_CharacterName;

		// Token: 0x04004EF4 RID: 20212
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400379F")]
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x04004EF5 RID: 20213
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40037A0")]
		[SerializeField]
		private Badge m_Badge;

		// Token: 0x04004EF6 RID: 20214
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40037A1")]
		[SerializeField]
		private GameObject m_LockObject;

		// Token: 0x04004EF7 RID: 20215
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40037A2")]
		[SerializeField]
		private Text m_LockText;

		// Token: 0x04004EF8 RID: 20216
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40037A3")]
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x02000D7D RID: 3453
		[Token(Token = "0x2001104")]
		public class SubOfferNamedListItemData : ScrollItemData
		{
			// Token: 0x06003F9F RID: 16287 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620C")]
			[Address(RVA = "0x10158F1C4", Offset = "0x158F1C4", VA = "0x10158F1C4", Slot = "4")]
			public override void OnClickItemCallBack()
			{
			}

			// Token: 0x06003FA0 RID: 16288 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600620D")]
			[Address(RVA = "0x10158F21C", Offset = "0x158F21C", VA = "0x10158F21C")]
			public SubOfferNamedListItemData()
			{
			}

			// Token: 0x04004EF9 RID: 20217
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x4006A9E")]
			public int m_Index;

			// Token: 0x04004EFA RID: 20218
			[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
			[Token(Token = "0x4006A9F")]
			public int m_CharaID;

			// Token: 0x04004EFB RID: 20219
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4006AA0")]
			public string m_Name;

			// Token: 0x04004EFC RID: 20220
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4006AA1")]
			public eCharaNamedType m_NamedType;

			// Token: 0x04004EFD RID: 20221
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4006AA2")]
			public int m_BadgeCount;

			// Token: 0x04004EFE RID: 20222
			[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
			[Token(Token = "0x4006AA3")]
			public bool m_IsDark;

			// Token: 0x04004EFF RID: 20223
			[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
			[Token(Token = "0x4006AA4")]
			public Action<eCharaNamedType, int> m_OnClickItemCallback;
		}
	}
}
