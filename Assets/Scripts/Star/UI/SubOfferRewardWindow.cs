﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D7E RID: 3454
	[Token(Token = "0x200093F")]
	[StructLayout(3)]
	public class SubOfferRewardWindow : UIGroup
	{
		// Token: 0x06003FA1 RID: 16289 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A46")]
		[Address(RVA = "0x10158F224", Offset = "0x158F224", VA = "0x10158F224")]
		public void Setup(RewardSet[] rewardSet)
		{
		}

		// Token: 0x06003FA2 RID: 16290 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A47")]
		[Address(RVA = "0x10158F9A4", Offset = "0x158F9A4", VA = "0x10158F9A4")]
		public void OnClickDecideButton()
		{
		}

		// Token: 0x06003FA3 RID: 16291 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A48")]
		[Address(RVA = "0x10158F9B0", Offset = "0x158F9B0", VA = "0x10158F9B0")]
		public SubOfferRewardWindow()
		{
		}

		// Token: 0x04004F00 RID: 20224
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40037A4")]
		[SerializeField]
		private Text m_SubTitle;

		// Token: 0x04004F01 RID: 20225
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40037A5")]
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x04004F02 RID: 20226
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40037A6")]
		[SerializeField]
		private Text m_Message;

		// Token: 0x04004F03 RID: 20227
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40037A7")]
		[SerializeField]
		private GameObject m_DecideButton;
	}
}
