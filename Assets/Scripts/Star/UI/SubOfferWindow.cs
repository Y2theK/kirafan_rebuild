﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D7F RID: 3455
	[Token(Token = "0x2000940")]
	[StructLayout(3)]
	public class SubOfferWindow : UIGroup
	{
		// Token: 0x06003FA4 RID: 16292 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A49")]
		[Address(RVA = "0x10158F9B8", Offset = "0x158F9B8", VA = "0x10158F9B8")]
		public void Setup()
		{
		}

		// Token: 0x06003FA5 RID: 16293 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A4A")]
		[Address(RVA = "0x10158FA88", Offset = "0x158FA88", VA = "0x10158FA88")]
		public void SetupParam(ContentRoomState_Main ownerState, ContentRoomUI ownerUI, Sprite bgSprite)
		{
		}

		// Token: 0x06003FA6 RID: 16294 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A4B")]
		[Address(RVA = "0x1015901D8", Offset = "0x15901D8", VA = "0x1015901D8")]
		public void Open(Sprite bgSprite)
		{
		}

		// Token: 0x06003FA7 RID: 16295 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A4C")]
		[Address(RVA = "0x101590228", Offset = "0x1590228", VA = "0x101590228")]
		public void Destroy()
		{
		}

		// Token: 0x06003FA8 RID: 16296 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A4D")]
		[Address(RVA = "0x101590308", Offset = "0x1590308", VA = "0x101590308", Slot = "11")]
		protected override void OnDestroy()
		{
		}

		// Token: 0x06003FA9 RID: 16297 RVA: 0x00018E10 File Offset: 0x00017010
		[Token(Token = "0x6003A4E")]
		[Address(RVA = "0x101590330", Offset = "0x1590330", VA = "0x101590330")]
		public bool IsTopPage()
		{
			return default(bool);
		}

		// Token: 0x06003FAA RID: 16298 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A4F")]
		[Address(RVA = "0x101590340", Offset = "0x1590340", VA = "0x101590340")]
		public void Back()
		{
		}

		// Token: 0x06003FAB RID: 16299 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A50")]
		[Address(RVA = "0x10158FBEC", Offset = "0x158FBEC", VA = "0x10158FBEC")]
		private void SetActiveUI(SubOfferWindow.eMode mode, int scrollItemIndex)
		{
		}

		// Token: 0x06003FAC RID: 16300 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A51")]
		[Address(RVA = "0x101590364", Offset = "0x1590364", VA = "0x101590364")]
		private void ChangeListMode(SubOfferWindow.eMode mode, eCharaNamedType namedType, eCharaNamedType lastSelectedNamedType)
		{
		}

		// Token: 0x06003FAD RID: 16301 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003A52")]
		[Address(RVA = "0x101590530", Offset = "0x1590530", VA = "0x101590530")]
		private List<OfferCharaIcon.OfferCharaData> CreateOfferCharaTableData(eCharaNamedType namedType)
		{
			return null;
		}

		// Token: 0x06003FAE RID: 16302 RVA: 0x00018E28 File Offset: 0x00017028
		[Token(Token = "0x6003A53")]
		[Address(RVA = "0x1015909D4", Offset = "0x15909D4", VA = "0x1015909D4")]
		private int SelectCurrentCharacterID(List<OfferCharaIcon.OfferCharaData> tableDataList)
		{
			return 0;
		}

		// Token: 0x06003FAF RID: 16303 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A54")]
		[Address(RVA = "0x101590BC0", Offset = "0x1590BC0", VA = "0x101590BC0")]
		private void ApplyOfferListWindow(int currentCharaID)
		{
		}

		// Token: 0x06003FB0 RID: 16304 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A55")]
		[Address(RVA = "0x101590CA0", Offset = "0x1590CA0", VA = "0x101590CA0")]
		private void ApplyOfferScrollList(int currentCharaID, bool isPlayIn)
		{
		}

		// Token: 0x06003FB1 RID: 16305 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A56")]
		[Address(RVA = "0x101590FF0", Offset = "0x1590FF0", VA = "0x101590FF0")]
		private void OnComplateOfferScrollIn()
		{
		}

		// Token: 0x06003FB2 RID: 16306 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A57")]
		[Address(RVA = "0x101590120", Offset = "0x1590120", VA = "0x101590120")]
		public void UpdateItemAmount()
		{
		}

		// Token: 0x06003FB3 RID: 16307 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A58")]
		[Address(RVA = "0x10158FD7C", Offset = "0x158FD7C", VA = "0x10158FD7C")]
		public void UpdateNamedList()
		{
		}

		// Token: 0x06003FB4 RID: 16308 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A59")]
		[Address(RVA = "0x10159106C", Offset = "0x159106C", VA = "0x10159106C")]
		public void UpdateOfferList(int currentCharaID)
		{
		}

		// Token: 0x06003FB5 RID: 16309 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5A")]
		[Address(RVA = "0x101591140", Offset = "0x1591140", VA = "0x101591140")]
		private void OnClickNamedListItem(eCharaNamedType namedType, int index)
		{
		}

		// Token: 0x06003FB6 RID: 16310 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5B")]
		[Address(RVA = "0x1015911F0", Offset = "0x15911F0", VA = "0x1015911F0")]
		private void OnClickListItem(OfferManager.OfferData offerData)
		{
		}

		// Token: 0x06003FB7 RID: 16311 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A5C")]
		[Address(RVA = "0x101591228", Offset = "0x1591228", VA = "0x101591228")]
		public SubOfferWindow()
		{
		}

		// Token: 0x04004F04 RID: 20228
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40037A8")]
		[SerializeField]
		private Image m_BackGround;

		// Token: 0x04004F05 RID: 20229
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40037A9")]
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04004F06 RID: 20230
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40037AA")]
		[SerializeField]
		private AnimUIPlayer m_NpcAnimPlayer;

		// Token: 0x04004F07 RID: 20231
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40037AB")]
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04004F08 RID: 20232
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40037AC")]
		[SerializeField]
		private AnimUIPlayer m_CharaIllustAnimPlayer;

		// Token: 0x04004F09 RID: 20233
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40037AD")]
		[SerializeField]
		private VariableIcon m_UnlockItemIcon;

		// Token: 0x04004F0A RID: 20234
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40037AE")]
		[SerializeField]
		private Text m_UnlockItemText;

		// Token: 0x04004F0B RID: 20235
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40037AF")]
		[SerializeField]
		private ScrollViewBase m_NamedScroll;

		// Token: 0x04004F0C RID: 20236
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40037B0")]
		[SerializeField]
		private AnimUIPlayer m_NamedScrollAnimPlayer;

		// Token: 0x04004F0D RID: 20237
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40037B1")]
		[SerializeField]
		private ScrollViewBase m_OfferScroll;

		// Token: 0x04004F0E RID: 20238
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40037B2")]
		[SerializeField]
		private AnimUIPlayer m_OfferScrollAnimPlayer;

		// Token: 0x04004F0F RID: 20239
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40037B3")]
		[SerializeField]
		private OfferCharaTable m_CharaTable;

		// Token: 0x04004F10 RID: 20240
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40037B4")]
		private ContentRoomState_Main m_OwnerState;

		// Token: 0x04004F11 RID: 20241
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40037B5")]
		private ContentRoomUI m_OwnerUI;

		// Token: 0x04004F12 RID: 20242
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40037B6")]
		private SubOfferWindow.eMode m_Mode;

		// Token: 0x04004F13 RID: 20243
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x40037B7")]
		private eCharaNamedType m_LastSelectedNamed;

		// Token: 0x02000D80 RID: 3456
		[Token(Token = "0x2001105")]
		private enum eMode
		{
			// Token: 0x04004F15 RID: 20245
			[Token(Token = "0x4006AA6")]
			NamedList,
			// Token: 0x04004F16 RID: 20246
			[Token(Token = "0x4006AA7")]
			OfferList
		}
	}
}
