﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.UI.Edit;
using Star.UI.Global;

namespace Star.UI
{
	// Token: 0x02000D86 RID: 3462
	[Token(Token = "0x2000945")]
	[StructLayout(3)]
	public class SupportEditPlayer : SingletonMonoBehaviour<SupportEditPlayer>
	{
		// Token: 0x14000079 RID: 121
		// (add) Token: 0x06003FDB RID: 16347 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06003FDC RID: 16348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000079")]
		public event Action OnClose
		{
			[Token(Token = "0x6003A80")]
			[Address(RVA = "0x101591238", Offset = "0x1591238", VA = "0x101591238")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6003A81")]
			[Address(RVA = "0x101591344", Offset = "0x1591344", VA = "0x101591344")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06003FDD RID: 16349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A82")]
		[Address(RVA = "0x101591450", Offset = "0x1591450", VA = "0x101591450", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06003FDE RID: 16350 RVA: 0x00018ED0 File Offset: 0x000170D0
		[Token(Token = "0x6003A83")]
		[Address(RVA = "0x1015914A0", Offset = "0x15914A0", VA = "0x1015914A0")]
		public bool IsOpen()
		{
			return default(bool);
		}

		// Token: 0x06003FDF RID: 16351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A84")]
		[Address(RVA = "0x1015914B0", Offset = "0x15914B0", VA = "0x1015914B0")]
		public void Open(List<UserSupportData> supportDatas, string partyName, int supportLimit, PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.View)
		{
		}

		// Token: 0x06003FE0 RID: 16352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A85")]
		[Address(RVA = "0x101591668", Offset = "0x1591668", VA = "0x101591668")]
		public void Close()
		{
		}

		// Token: 0x06003FE1 RID: 16353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A86")]
		[Address(RVA = "0x1015917A8", Offset = "0x15917A8", VA = "0x1015917A8")]
		public void ForceHide()
		{
		}

		// Token: 0x06003FE2 RID: 16354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A87")]
		[Address(RVA = "0x1015917B0", Offset = "0x15917B0", VA = "0x1015917B0")]
		private void Update()
		{
		}

		// Token: 0x06003FE3 RID: 16355 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A88")]
		[Address(RVA = "0x101591AF4", Offset = "0x1591AF4", VA = "0x101591AF4")]
		private void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06003FE4 RID: 16356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A89")]
		[Address(RVA = "0x101591AF8", Offset = "0x1591AF8", VA = "0x101591AF8")]
		private void OnStartIllustView()
		{
		}

		// Token: 0x06003FE5 RID: 16357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8A")]
		[Address(RVA = "0x101591B70", Offset = "0x1591B70", VA = "0x101591B70")]
		private void OnEndIllustView()
		{
		}

		// Token: 0x06003FE6 RID: 16358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003A8B")]
		[Address(RVA = "0x101591C68", Offset = "0x1591C68", VA = "0x101591C68")]
		public SupportEditPlayer()
		{
		}

		// Token: 0x04004F42 RID: 20290
		[Token(Token = "0x40037D9")]
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.SupportEditUI;

		// Token: 0x04004F43 RID: 20291
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40037DA")]
		private SupportEditPlayer.eStep m_Step;

		// Token: 0x04004F44 RID: 20292
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40037DB")]
		private SupportEditUI m_UI;

		// Token: 0x04004F45 RID: 20293
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40037DC")]
		private List<UserSupportData> m_SupportDatas;

		// Token: 0x04004F46 RID: 20294
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40037DD")]
		private string m_PartyName;

		// Token: 0x04004F47 RID: 20295
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40037DE")]
		private int m_SupportLimit;

		// Token: 0x04004F48 RID: 20296
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40037DF")]
		private PartyEditUIBase.eMode m_Mode;

		// Token: 0x04004F49 RID: 20297
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40037E0")]
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04004F4A RID: 20298
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40037E1")]
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x02000D87 RID: 3463
		[Token(Token = "0x2001107")]
		private enum eStep
		{
			// Token: 0x04004F4D RID: 20301
			[Token(Token = "0x4006AB3")]
			Hide,
			// Token: 0x04004F4E RID: 20302
			[Token(Token = "0x4006AB4")]
			UILoad,
			// Token: 0x04004F4F RID: 20303
			[Token(Token = "0x4006AB5")]
			LoadWait,
			// Token: 0x04004F50 RID: 20304
			[Token(Token = "0x4006AB6")]
			SetupAndPlayIn,
			// Token: 0x04004F51 RID: 20305
			[Token(Token = "0x4006AB7")]
			Main,
			// Token: 0x04004F52 RID: 20306
			[Token(Token = "0x4006AB8")]
			UnloadWait
		}
	}
}
