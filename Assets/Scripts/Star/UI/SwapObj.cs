﻿using UnityEngine;

namespace Star.UI {
    public class SwapObj : MonoBehaviour {
        [Tooltip("CustomButtonから操作する場合、0=base, 1=press, 2=notInteractable, 3=decide")]
        [SerializeField] private GameObject[] m_Objs;
        [SerializeField] private bool m_IgnoreCustomButton;

        private int m_CurrentIdx;

        public bool IgnoreCustomButton => m_IgnoreCustomButton;

        private void Start() {
            foreach (GameObject obj in m_Objs) {
                if (obj.activeSelf) {
                    obj.SetActive(false);
                }
            }
            m_Objs[m_CurrentIdx].SetActive(true);
        }

        public int GetTargetObjNum() {
            return m_Objs.Length;
        }

        public void Swap() {
            Swap(m_CurrentIdx);
        }

        public void Swap(int idx) {
            if (idx > m_Objs.Length) {
                idx = 0;
            }
            if (m_CurrentIdx == idx) { return; }
            m_Objs[m_CurrentIdx].SetActive(false);
            if (idx < m_Objs.Length && m_Objs[idx] != null) {
                m_CurrentIdx = idx;
            } else {
                m_CurrentIdx = 0;
            }
            m_Objs[m_CurrentIdx].SetActive(true);
        }
    }
}
