﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000D47 RID: 3399
	[Token(Token = "0x200091D")]
	[StructLayout(3)]
	public class SwitchFade : MonoBehaviour
	{
		// Token: 0x06003E57 RID: 15959 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600390E")]
		[Address(RVA = "0x101592000", Offset = "0x1592000", VA = "0x101592000")]
		private void Start()
		{
		}

		// Token: 0x06003E58 RID: 15960 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600390F")]
		[Address(RVA = "0x101592014", Offset = "0x1592014", VA = "0x101592014")]
		public void SetTarget(CanvasGroup[] targets)
		{
		}

		// Token: 0x06003E59 RID: 15961 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003910")]
		[Address(RVA = "0x10159247C", Offset = "0x159247C", VA = "0x10159247C")]
		private void Update()
		{
		}

		// Token: 0x06003E5A RID: 15962 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003911")]
		[Address(RVA = "0x1015925A8", Offset = "0x15925A8", VA = "0x1015925A8")]
		private void UpdateObjs()
		{
		}

		// Token: 0x06003E5B RID: 15963 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003912")]
		[Address(RVA = "0x10159226C", Offset = "0x159226C", VA = "0x10159226C")]
		private void ApplyAlpha(int idx, float alpha)
		{
		}

		// Token: 0x06003E5C RID: 15964 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003913")]
		[Address(RVA = "0x101592764", Offset = "0x1592764", VA = "0x101592764")]
		public SwitchFade()
		{
		}

		// Token: 0x04004D96 RID: 19862
		[Token(Token = "0x4003697")]
		private const float DEFAULT_DISPTIME_SEC = 1.5f;

		// Token: 0x04004D97 RID: 19863
		[Token(Token = "0x4003698")]
		private const float DEFAULT_FADETIME_SEC = 0.25f;

		// Token: 0x04004D98 RID: 19864
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003699")]
		[SerializeField]
		private CanvasGroup[] m_Targets;

		// Token: 0x04004D99 RID: 19865
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400369A")]
		private GameObject[] m_TargetObjs;

		// Token: 0x04004D9A RID: 19866
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400369B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001251B0", Offset = "0x1251B0")]
		private float m_DispTimeSec;

		// Token: 0x04004D9B RID: 19867
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x400369C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001251FC", Offset = "0x1251FC")]
		private float m_FadeTimeSec;

		// Token: 0x04004D9C RID: 19868
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x400369D")]
		[SerializeField]
		private bool m_Sync;

		// Token: 0x04004D9D RID: 19869
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400369E")]
		private int m_Idx;

		// Token: 0x04004D9E RID: 19870
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x400369F")]
		private SwitchFade.eStep m_Step;

		// Token: 0x04004D9F RID: 19871
		[Cpp2IlInjected.FieldOffset(Offset = "0x3C")]
		[Token(Token = "0x40036A0")]
		private float m_StepTime;

		// Token: 0x02000D48 RID: 3400
		[Token(Token = "0x20010F1")]
		private enum eStep
		{
			// Token: 0x04004DA1 RID: 19873
			[Token(Token = "0x4006A4F")]
			Hide,
			// Token: 0x04004DA2 RID: 19874
			[Token(Token = "0x4006A50")]
			In,
			// Token: 0x04004DA3 RID: 19875
			[Token(Token = "0x4006A51")]
			Idle,
			// Token: 0x04004DA4 RID: 19876
			[Token(Token = "0x4006A52")]
			Out
		}
	}
}
