﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI {
    public class TabButton : MonoBehaviour {
        private const float MARGIN = 24f;
        private const float IMAGE_HEIGHT = 32f;
        private const float INACTIVE_POSITION_Y = -4f;

        [SerializeField] private CustomButton m_Button;
        [SerializeField] private Image m_Image;
        [SerializeField] private Text m_Text;

        private int m_Idx = -1;
        private RectTransform m_RectTransform;

        public event Action<int> OnClick;

        public CustomButton Button => m_Button;

        public RectTransform RectTransform {
            get {
                if (m_RectTransform == null) {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        public void Setup(int idx, string text) {
            m_Idx = idx;
            m_Text.text = text;
            if (m_Image != null) {
                m_Image.gameObject.SetActive(false);
            }
            if (m_Text != null) {
                m_Text.gameObject.SetActive(true);
            }
        }

        public void Setup(int idx, Sprite sprite) {
            m_Idx = idx;
            m_Image.sprite = sprite;
            Vector2 size = GetLowestSize();
            size = new Vector2(size.x - MARGIN, size.y);
            m_Image.GetComponent<RectTransform>().sizeDelta = size;
            if (m_Image != null) {
                m_Image.gameObject.SetActive(true);
            }
            if (m_Text != null) {
                m_Text.text = string.Empty;
                m_Text.gameObject.SetActive(false);
            }
        }

        public void SetWidth(float width) {
            RectTransform.sizeDelta = new Vector2(width, RectTransform.sizeDelta.y);
        }

        public Vector2 GetLowestSize() {
            if (m_Image != null && m_Image.sprite != null) {
                Rect rect = m_Image.sprite.rect;
                float ratio = 1f;
                if (rect.height > IMAGE_HEIGHT) {
                    ratio = IMAGE_HEIGHT / rect.height;
                }
                return new Vector2(rect.width * ratio + MARGIN, rect.height * ratio);
            }
            return new Vector2(m_Text.preferredWidth + MARGIN, m_Text.preferredHeight);
        }

        public void OnClickButtonCallBack() {
            OnClick.Call(m_Idx);
        }
    }
}
