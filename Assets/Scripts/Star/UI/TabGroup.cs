﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI {
    public class TabGroup : MonoBehaviour {
        private const float SCROLL_SIZEDELTA = -24f;
        private const float SCROLL_SIZEDELTA_SCROLLMODE = -112f;
        private const float MARGIN = 8f;

        [SerializeField] private TabButton m_TabButtonPrefab;
        [SerializeField] private RectTransform m_ButtonParent;
        [SerializeField] private ScrollRect m_ScrollRect;
        [SerializeField] private CustomButton m_LeftButton;
        [SerializeField] private CustomButton m_RightButton;
        [SerializeField] private Sprite[] m_ButtonSprites;
        [SerializeField] private bool m_DisableScroll;
        
        public UnityEvent m_OnChangeIdx;

        private string[] m_Texts;
        private List<TabButton> m_TabButtonList = new List<TabButton>();
        private int m_SelectIdx;

        public int SelectIdx {
            get => m_SelectIdx;
            set => ChangeIdx(value);
        }

        public event Action<int> OnChangeIdx;

        public void Setup(string[] buttonTexts, float buttonWidth = -1f) {
            if (m_ButtonParent.TryGetComponent(out HorizontalLayoutGroup layout)) {
                layout.enabled = false;
            }
            RectTransform scrollTransform = m_ScrollRect.GetComponent<RectTransform>();
            scrollTransform.anchorMin = Vector2.zero;
            scrollTransform.anchorMax = Vector2.one;
            scrollTransform.sizeDelta = Vector2.zero;
            m_Texts = buttonTexts;
            float width = 0f;
            for (int i = 0; i < m_Texts.Length; i++) {
                TabButton tab = Instantiate(m_TabButtonPrefab, m_ButtonParent, false);
                m_TabButtonList.Add(tab);
                tab.Setup(i, m_Texts[i]);
                tab.OnClick += OnClickTabButtonCallBack;
                Vector2 lowestSize = tab.GetLowestSize();
                if (width < lowestSize.x) {
                    width = lowestSize.x;
                }
            }
            if (buttonWidth > 0f) {
                width = buttonWidth;
            }
            m_ButtonParent.sizeDelta = new Vector2(width * m_TabButtonList.Count + 2f * MARGIN, m_ButtonParent.sizeDelta.y);
            m_TabButtonList[m_SelectIdx].Button.IsDecided = true;
            m_TabButtonList[m_SelectIdx].Button.IsEnableInput = false;
            RectTransform viewTransform = m_ScrollRect.viewport.GetComponent<RectTransform>();
            viewTransform.sizeDelta = new Vector2(SCROLL_SIZEDELTA, viewTransform.sizeDelta.y);
            viewTransform.pivot = new Vector2(0.5f, 0f);
            viewTransform.anchoredPosition = Vector2.zero;
            bool showButtons = true;
            if (m_DisableScroll || width * m_TabButtonList.Count <= viewTransform.rect.width) {
                m_ScrollRect.horizontal = false;
                width = (viewTransform.rect.width - 2f * MARGIN) / m_TabButtonList.Count;
                if (buttonWidth > 0f) {
                    width = buttonWidth;
                }
                showButtons = false;
            } else {
                m_ScrollRect.horizontal = true;
                viewTransform.sizeDelta = new Vector2(SCROLL_SIZEDELTA_SCROLLMODE, viewTransform.sizeDelta.y);
            }
            for (int i = 0; i < m_TabButtonList.Count; i++) {
                m_TabButtonList[i].RectTransform.pivot = Vector2.zero;
                m_TabButtonList[i].SetWidth(width);
                m_TabButtonList[i].RectTransform.anchoredPosition = new Vector2(MARGIN + i * width, 0f);
            }
            if (m_LeftButton != null) {
                m_LeftButton.gameObject.SetActive(showButtons);
            }
            if (m_RightButton != null) {
                m_RightButton.gameObject.SetActive(showButtons);
            }
            m_TabButtonList[m_SelectIdx].Button.IsDecided = true;
            m_TabButtonList[m_SelectIdx].Button.IsEnableInput = false;
            m_TabButtonList[m_SelectIdx].RectTransform.SetAsLastSibling();
            UpdateLeftRightButton();
        }

        public void Setup(float buttonWidth = -1f) {
            if (m_ButtonParent.TryGetComponent(out HorizontalLayoutGroup layout)) {
                layout.enabled = false;
            }
            RectTransform scrollTransform = m_ScrollRect.GetComponent<RectTransform>();
            scrollTransform.anchorMin = Vector2.zero;
            scrollTransform.anchorMax = Vector2.one;
            scrollTransform.sizeDelta = Vector2.zero;
            m_Texts = null;
            float width = 0f;
            for (int i = 0; i < m_ButtonSprites.Length; i++) {
                TabButton tab = Instantiate(m_TabButtonPrefab, m_ButtonParent, false);
                m_TabButtonList.Add(tab);
                tab.Setup(i, m_ButtonSprites[i]);
                tab.OnClick += OnClickTabButtonCallBack;
                Vector2 lowestSize = tab.GetLowestSize();
                if (width < lowestSize.x) {
                    width = lowestSize.x;
                }
            }
            if (buttonWidth > 0f) {
                width = buttonWidth;
            }
            LayoutGroup layoutGroup = GetComponentInParent<LayoutGroup>();
            if (layoutGroup != null) {
                if (layoutGroup.TryGetComponent<RectTransform>(out var layoutTransform)) {
                    LayoutRebuilder.ForceRebuildLayoutImmediate(layoutTransform);
                }
            }
            RectTransform viewTransform = m_ScrollRect.viewport.GetComponent<RectTransform>();
            viewTransform.sizeDelta = new Vector2(SCROLL_SIZEDELTA, viewTransform.sizeDelta.y);
            viewTransform.pivot = new Vector2(0.5f, 0f);
            viewTransform.anchoredPosition = Vector2.zero;
            bool showButtons = false;
            if (width * m_TabButtonList.Count <= viewTransform.rect.width) {
                m_ScrollRect.horizontal = false;
                width = (viewTransform.rect.width - 2f * MARGIN) / m_TabButtonList.Count;
                if (buttonWidth > 0f) {
                    width = buttonWidth;
                }
            } else {
                m_ScrollRect.horizontal = true;
                viewTransform.sizeDelta = new Vector2(SCROLL_SIZEDELTA_SCROLLMODE, viewTransform.sizeDelta.y);
                showButtons = true;
            }
            for (int i = 0; i < m_TabButtonList.Count; i++) {
                m_TabButtonList[i].RectTransform.pivot = Vector2.zero;
                m_TabButtonList[i].SetWidth(width);
                m_TabButtonList[i].RectTransform.anchoredPosition = new Vector2(MARGIN + i * width, 0f);
            }
            if (m_LeftButton != null) {
                m_LeftButton.gameObject.SetActive(showButtons);
            }
            if (m_RightButton != null) {
                m_RightButton.gameObject.SetActive(showButtons);
            }
            m_TabButtonList[m_SelectIdx].Button.IsDecided = true;
            m_TabButtonList[m_SelectIdx].Button.IsEnableInput = false;
            m_TabButtonList[m_SelectIdx].RectTransform.SetAsLastSibling();
            UpdateLeftRightButton();
        }

        public void SetIntaractable(bool flg, int idx) {
            m_TabButtonList[idx].Button.Interactable = flg;
        }

        public void SetButtonActive(bool flg, int idx) {
            m_TabButtonList[idx].gameObject.SetActive(flg);
        }

        public int GetButtonNum() {
            return m_TabButtonList.Count;
        }

        public TabButton GetButton(int idx) {
            return m_TabButtonList[idx];
        }

        private void OnClickTabButtonCallBack(int idx) {
            ChangeIdx(idx);
        }

        public void ChangeIdx(int idx) {
            if (idx < 0 || idx >= m_TabButtonList.Count || idx == m_SelectIdx) {
                return;
            }
            m_TabButtonList[m_SelectIdx].Button.IsDecided = false;
            m_TabButtonList[m_SelectIdx].Button.IsEnableInput = true;
            m_SelectIdx = idx;
            m_TabButtonList[m_SelectIdx].Button.IsDecided = true;
            m_TabButtonList[m_SelectIdx].Button.IsEnableInput = false;
            m_TabButtonList[m_SelectIdx].RectTransform.SetAsLastSibling();
            if (m_ScrollRect.horizontal) {
                RectTransform buttonTransform = m_TabButtonList[m_SelectIdx].RectTransform;
                float position = buttonTransform.localPosition.x - m_ButtonParent.rect.width * 0.5f;
                float width = m_ButtonParent.rect.width - m_ScrollRect.viewport.rect.width;
                float normalized = Mathf.Clamp01(position / width + 0.5f);
                m_ScrollRect.horizontalNormalizedPosition = normalized;
            }
            if (m_OnChangeIdx != null) {
                m_OnChangeIdx.Invoke();
            }
            OnChangeIdx.Call(idx);
            UpdateLeftRightButton();
        }

        private void UpdateLeftRightButton() {
            if (m_LeftButton != null) {
                m_LeftButton.Interactable = m_SelectIdx > 0;
            }
            if (m_RightButton != null) {
                m_RightButton.Interactable = m_SelectIdx < m_TabButtonList.Count - 1;
            }
        }

        public void OnClickRightButtonCallBack() {
            int check = 0;
            int idx = m_SelectIdx;
            while (check < m_TabButtonList.Count) {
                idx++;
                if (idx >= m_TabButtonList.Count) {
                    idx = 0;
                }
                if (m_TabButtonList[idx].Button.Interactable) {
                    break;
                }
                check++;
            }
            ChangeIdx(idx);
        }

        public void OnClickLeftButtonCallBack() {
            int check = 0;
            int idx = m_SelectIdx;
            while (check < m_TabButtonList.Count) {
                idx--;
                if (idx < 0) {
                    idx = m_TabButtonList.Count - 1;
                }
                if (m_TabButtonList[idx].Button.Interactable) {
                    break;
                }
                check++;
            }
            ChangeIdx(idx);
        }
    }
}
