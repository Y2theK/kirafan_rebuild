﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CAA RID: 3242
	[Token(Token = "0x20008B0")]
	[StructLayout(3)]
	public class TextField : MonoBehaviour
	{
		// Token: 0x06003B62 RID: 15202 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003624")]
		[Address(RVA = "0x101595570", Offset = "0x1595570", VA = "0x101595570")]
		public void SetTitle(string text)
		{
		}

		// Token: 0x06003B63 RID: 15203 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003625")]
		[Address(RVA = "0x1015955B0", Offset = "0x15955B0", VA = "0x1015955B0")]
		public void SetValue(string text)
		{
		}

		// Token: 0x06003B64 RID: 15204 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003626")]
		[Address(RVA = "0x1015955F0", Offset = "0x15955F0", VA = "0x1015955F0")]
		public void SetTitleBGWidth(float width)
		{
		}

		// Token: 0x06003B65 RID: 15205 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003627")]
		[Address(RVA = "0x101595668", Offset = "0x1595668", VA = "0x101595668")]
		public TextField()
		{
		}

		// Token: 0x04004A0D RID: 18957
		[Token(Token = "0x400342A")]
		private const float TITLEBG_MARGIN = 24f;

		// Token: 0x04004A0E RID: 18958
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400342B")]
		[SerializeField]
		private RectTransform m_TitleBG;

		// Token: 0x04004A0F RID: 18959
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400342C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001233D4", Offset = "0x1233D4")]
		private Text m_Title;

		// Token: 0x04004A10 RID: 18960
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400342D")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100123420", Offset = "0x123420")]
		private Text m_Value;
	}
}
