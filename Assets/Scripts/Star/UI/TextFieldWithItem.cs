﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000CAB RID: 3243
	[Token(Token = "0x20008B1")]
	[StructLayout(3)]
	public class TextFieldWithItem : MonoBehaviour
	{
		// Token: 0x06003B66 RID: 15206 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003628")]
		[Address(RVA = "0x101595670", Offset = "0x1595670", VA = "0x101595670")]
		public void SetTitle(string text)
		{
		}

		// Token: 0x06003B67 RID: 15207 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003629")]
		[Address(RVA = "0x1015956C0", Offset = "0x15956C0", VA = "0x1015956C0")]
		public void SetValue(string text)
		{
		}

		// Token: 0x06003B68 RID: 15208 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362A")]
		[Address(RVA = "0x101595710", Offset = "0x1595710", VA = "0x101595710")]
		public void SetIcon(VariableIcon.eType type, int id)
		{
		}

		// Token: 0x06003B69 RID: 15209 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362B")]
		[Address(RVA = "0x1015958D4", Offset = "0x15958D4", VA = "0x1015958D4")]
		private void ApplyRoomObject(int accessKey)
		{
		}

		// Token: 0x06003B6A RID: 15210 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362C")]
		[Address(RVA = "0x101595938", Offset = "0x1595938", VA = "0x101595938")]
		public TextFieldWithItem()
		{
		}

		// Token: 0x04004A11 RID: 18961
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400342E")]
		[SerializeField]
		private TextField m_TextField;

		// Token: 0x04004A12 RID: 18962
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400342F")]
		[SerializeField]
		private VariableIcon m_VariableIcon;
	}
}
