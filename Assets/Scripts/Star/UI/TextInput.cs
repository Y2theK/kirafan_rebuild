﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000CAC RID: 3244
	[Token(Token = "0x20008B2")]
	[StructLayout(3)]
	public class TextInput : MonoBehaviour
	{
		// Token: 0x06003B6B RID: 15211 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362D")]
		[Address(RVA = "0x101595940", Offset = "0x1595940", VA = "0x101595940")]
		private void Start()
		{
		}

		// Token: 0x06003B6C RID: 15212 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362E")]
		[Address(RVA = "0x101595B28", Offset = "0x1595B28", VA = "0x101595B28")]
		private void LateUpdate()
		{
		}

		// Token: 0x06003B6D RID: 15213 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600362F")]
		[Address(RVA = "0x101595B64", Offset = "0x1595B64", VA = "0x101595B64")]
		public void SetDecideInteractable(bool flg)
		{
		}

		// Token: 0x06003B6E RID: 15214 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003630")]
		[Address(RVA = "0x101595B9C", Offset = "0x1595B9C", VA = "0x101595B9C")]
		public void SetText(string text)
		{
		}

		// Token: 0x06003B6F RID: 15215 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x6003631")]
		[Address(RVA = "0x101595CB8", Offset = "0x1595CB8", VA = "0x101595CB8")]
		public string GetText()
		{
			return null;
		}

		// Token: 0x06003B70 RID: 15216 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003632")]
		[Address(RVA = "0x101595CF0", Offset = "0x1595CF0", VA = "0x101595CF0")]
		private void OnValueChanged(string text)
		{
		}

		// Token: 0x06003B71 RID: 15217 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003633")]
		[Address(RVA = "0x101595D60", Offset = "0x1595D60", VA = "0x101595D60")]
		private void OnEndEdit(string text)
		{
		}

		// Token: 0x06003B72 RID: 15218 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003634")]
		[Address(RVA = "0x101595D64", Offset = "0x1595D64", VA = "0x101595D64")]
		private void FixText(string text)
		{
		}

		// Token: 0x06003B73 RID: 15219 RVA: 0x000184B0 File Offset: 0x000166B0
		[Token(Token = "0x6003635")]
		[Address(RVA = "0x101596474", Offset = "0x1596474", VA = "0x101596474")]
		private bool CheckEmptyString(string text)
		{
			return default(bool);
		}

		// Token: 0x06003B74 RID: 15220 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003636")]
		[Address(RVA = "0x1015965B8", Offset = "0x15965B8", VA = "0x1015965B8")]
		public TextInput()
		{
		}

		// Token: 0x04004A13 RID: 18963
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003430")]
		private readonly char[,] INVALID_CODE;

		// Token: 0x04004A14 RID: 18964
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4003431")]
		private readonly char[] VALID_CODE;

		// Token: 0x04004A15 RID: 18965
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4003432")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012348C", Offset = "0x12348C")]
		private int m_InputLimit;

		// Token: 0x04004A16 RID: 18966
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4003433")]
		[SerializeField]
		private int m_RowLimit;

		// Token: 0x04004A17 RID: 18967
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4003434")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x1001234E8", Offset = "0x1234E8")]
		private int m_TmpLimit;

		// Token: 0x04004A18 RID: 18968
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x4003435")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x100123534", Offset = "0x123534")]
		private bool m_IsEnableEmpty;

		// Token: 0x04004A19 RID: 18969
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4003436")]
		[SerializeField]
		private InputField m_InputField;

		// Token: 0x04004A1A RID: 18970
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4003437")]
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04004A1B RID: 18971
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4003438")]
		[SerializeField]
		private Text m_DisplayFixedText;

		// Token: 0x04004A1C RID: 18972
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4003439")]
		private int m_BeforeCaret;
	}
}
