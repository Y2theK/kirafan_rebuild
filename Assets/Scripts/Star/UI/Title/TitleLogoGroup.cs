﻿using UnityEngine;

namespace Star.UI.Title {
    public class TitleLogoGroup : UIGroup {
        private const float LOGO_DISPLAY_TIME = 1.1f;
        private const float LOGO_FADE_TIME = 0.3f;
        private readonly Color LOGO_FADE_COLOR = Color.white;

        [SerializeField] private GameObject[] m_LogoObjs;
        [SerializeField] private TitleLogoFadeManager m_TitleLogoFadeManager;

        private int m_CurrentIndex;
        private float m_DispTimeCount;
        private int m_RequestCueSheetIndex = -1;
        private int m_RequestNamedIndex = -1;
        private int m_VoiceCtrlReqID = -1;
        private bool m_IsEnd;
        private bool m_IsSkipped;

        private eLogoStep m_LogoStep;

        public override void Open() {
            base.Open();
            m_IsEnd = false;
            m_IsSkipped = false;
            m_CurrentIndex = 0;
            for (int i = 0; i < m_LogoObjs.Length; i++) {
                m_LogoObjs[i].SetActive(false);
            }
            PlayInLogo();
        }

        public override void Update() {
            base.Update();
            if (State == eState.Idle) {
                switch (m_LogoStep) {
                    case eLogoStep.In:
                        if (m_TitleLogoFadeManager.IsComplete()) {
                            PlayLogoVoice();
                            m_DispTimeCount = 0f;
                            m_LogoStep = eLogoStep.Idle;
                        }
                        break;
                    case eLogoStep.Idle:
                        m_DispTimeCount += Time.deltaTime;
                        if (m_DispTimeCount > LOGO_DISPLAY_TIME) {
                            PlayOutLogo();
                        }
                        break;
                    case eLogoStep.Out:
                        if (m_TitleLogoFadeManager.IsComplete()) {
                            m_LogoObjs[m_CurrentIndex].SetActive(false);
                            m_LogoStep = eLogoStep.End;
                        }
                        break;
                    case eLogoStep.End:
                        if (m_IsSkipped) {
                            Close();
                        } else {
                            m_CurrentIndex++;
                            PlayInLogo();
                        }
                        break;
                }
            }
        }

        private void PlayInLogo() {
            if (m_CurrentIndex < m_LogoObjs.Length) {
                m_LogoObjs[m_CurrentIndex].SetActive(true);
                m_TitleLogoFadeManager.SetFadeRatio(1f);
                m_TitleLogoFadeManager.FadeIn(LOGO_FADE_COLOR, LOGO_FADE_TIME, null);
                m_LogoStep = eLogoStep.In;
            } else {
                m_IsEnd = true;
            }
            if (m_IsEnd) {
                Close();
            }
        }

        private void PlayOutLogo() {
            m_TitleLogoFadeManager.FadeOut(LOGO_FADE_COLOR, LOGO_FADE_TIME, null);
            m_LogoStep = eLogoStep.Out;
        }

        private void PlayLogoVoice() {
            eSoundVoiceControllListDB id = eSoundVoiceControllListDB.Logo_A;
            switch (m_CurrentIndex) {
                case 0:
                    id = eSoundVoiceControllListDB.Logo_A;
                    break;
                case 1:
                    id = eSoundVoiceControllListDB.Logo_B;
                    break;
                case 2:
                    id = eSoundVoiceControllListDB.Logo_C;
                    break;
                case 3:
                    id = eSoundVoiceControllListDB.Logo_D;
                    break;
            }
            if (APIUtility.IsNeedSignup()) {
                m_RequestCueSheetIndex = 0;
                m_RequestNamedIndex = -1;
            }
            VoiceController.SoundVoiceControllResult voiceResult;
            if (m_RequestCueSheetIndex == -1 && m_RequestNamedIndex == -1) {
                voiceResult = GameSystem.Inst.VoiceCtrl.Request(id);
                m_RequestCueSheetIndex = voiceResult.m_RequestCueSheetIndex;
                m_RequestNamedIndex = voiceResult.m_RequestNamedIndex;
            } else {
                voiceResult = GameSystem.Inst.VoiceCtrl.Request(id, m_RequestCueSheetIndex, m_RequestNamedIndex);
            }
            m_VoiceCtrlReqID = voiceResult.m_UniqueID;
        }

        public bool IsSkipped() {
            return m_IsSkipped;
        }

        public void OnClick() {
            if (m_LogoStep <= eLogoStep.Idle) {
                PlayOutLogo();
            }
            m_IsEnd = true;
            m_IsSkipped = true;
            if (m_VoiceCtrlReqID != -1) {
                GameSystem.Inst.VoiceCtrl.StopRequest(m_VoiceCtrlReqID);
                m_VoiceCtrlReqID = -1;
            }
        }

        private enum eLogoStep {
            Hide,
            In,
            Idle,
            Out,
            End
        }
    }
}
