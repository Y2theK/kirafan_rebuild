﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Title {
    public class TitleUI : MonoBehaviour {
        [SerializeField] private AnimUIPlayer[] m_UIAnimList;
        [SerializeField] private UIGroup m_MainGroup;
        [SerializeField] private DataInheritenceUIGroup m_DataInheritanceWindowGroup;
        [SerializeField] private UIGroup m_DataClearWindowGroup;
        [SerializeField] private UIGroup m_AgreeWindowGroup;
        [SerializeField] private UIGroup m_CreatingOfflineDataWindowGroup;

        [SerializeField] private CustomButton m_CacheClearButton;
        [SerializeField] private CustomButton m_DataClearBotton;
        [SerializeField] private CustomButton m_CreateOfflineDataBotton;

        [SerializeField] private Text m_VersionText;
        [SerializeField] private Text m_IdText;

        [Tooltip("")]
        [SerializeField] private Text m_DataClearWindowMessage;

        [Tooltip("")]
        [SerializeField] private Text m_CreatingOfflineDataWindowOutLineMessage;

        [Tooltip("")]
        [SerializeField] private Text m_CreatingOfflineDataWindowMessage;

        [Tooltip("")]
        [SerializeField] private Image m_CreatingOfflineDataWindowGauge;

        [SerializeField] private CustomButton m_GoToNextSheet;
        [SerializeField] private GameObject m_InputBlock;
        [SerializeField] private CustomButton m_DetectTouch;
        [SerializeField] private WebViewWindow WebView;

        private int m_RequestCueSheetIndex = -1;
        private int m_RequestNamedIndex = -1;
        private int m_RequestCharaIdForSelector = -1;
        private bool m_IsRequestedTitleCall;
        private bool m_IsRequestedTitleStart;
        private bool m_IsAgreed;
        private bool[] m_IsDisplayUIAnimList = new bool[(int)eTitleUIAnim.Num];
        private bool m_IsObserveTitleLogo;

        public event Action OnClickGameStartButton;
        public event Action OnClickPlayerResetButton;
        public event Action<string, string> OnClickPlayerMoveButton;
        public event Action OnClickCacheClearButton;
        public event Action OnClickCreateOfflineDataButton;
        public event Action OnSuccessPlayerMoveCallback;
        public event Action OnClickDetectTouch;

        private event Action<bool> OnCloseAgreement;
        private event Action OnQuitCreatingOfflineData;

        public void Setup() {
            SetGoToNextSheetActive(false);
            m_IsRequestedTitleCall = false;
            m_IsAgreed = false;
            for (int i = 0; i < m_IsDisplayUIAnimList.Length; i++) {
                m_IsDisplayUIAnimList[i] = false;
            }
            m_IsObserveTitleLogo = false;
            m_DataInheritanceWindowGroup.SetOnClickButtonCallback(OnClickDataInheritanceWindowButtonCallback);
            m_DataInheritanceWindowGroup.SetOnPlayerMoveSuccessCallback(OnSuccessPlayerMove);
            m_MainGroup.Open();
            for (int i = 0; i < m_UIAnimList.Length; i++) {
                m_UIAnimList[i].Hide();
            }
        }

        public void SetRequestVoice(int requestCueSheetIndex, int requestNamedIndex, int requestCharaIdForSelector) {
            m_RequestCueSheetIndex = requestCueSheetIndex;
            m_RequestNamedIndex = requestNamedIndex;
            m_RequestCharaIdForSelector = requestCharaIdForSelector;
        }

        private void RequestTitleCall() {
            if (!m_IsRequestedTitleCall) {
                if (m_RequestCueSheetIndex == -1 && m_RequestNamedIndex == -1) {
                    var result = GameSystem.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleCall);
                    m_RequestCueSheetIndex = result.m_RequestCueSheetIndex;
                    m_RequestNamedIndex = result.m_RequestNamedIndex;
                    m_RequestCharaIdForSelector = -1;
                } else {
                    GameSystem.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleCall, m_RequestCueSheetIndex, m_RequestNamedIndex, m_RequestCharaIdForSelector);
                }
                m_IsRequestedTitleCall = true;
            }
        }

        private void RequestTitleStart() {
            if (!m_IsRequestedTitleStart) {
                if (m_RequestCueSheetIndex == -1 && m_RequestNamedIndex == -1) {
                    VoiceController.SoundVoiceControllResult result = GameSystem.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleStart);
                    m_RequestCueSheetIndex = result.m_RequestCueSheetIndex;
                    m_RequestNamedIndex = result.m_RequestNamedIndex;
                    m_RequestCharaIdForSelector = -1;
                } else {
                    GameSystem.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleStart, m_RequestCueSheetIndex, m_RequestNamedIndex, m_RequestCharaIdForSelector);
                }
                m_IsRequestedTitleStart = true;
            }
        }

        public void SetupUserInfoText() {
            DatabaseManager dbMng = GameSystem.Inst.DbMng;
            m_VersionText.text = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainUIVersionHeadText) + " 3.6.0";
            m_IdText.text = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainUIIDHeadText);
            if (!string.IsNullOrEmpty(AccessSaveData.Inst.MyCode)) {
                m_IdText.text += AccessSaveData.Inst.MyCode;
            }
            m_IdText.gameObject.SetActive(!string.IsNullOrEmpty(AccessSaveData.Inst.MyCode));
            string clearFront = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageFrontCount);
            string clear = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageCount);
            string clearAfter = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageAfterCount);
            m_DataClearWindowMessage.text = clearFront + clear + clearAfter;
            m_DataClearBotton.Interactable = APIUtility.IsNeedSignup();
            m_CreateOfflineDataBotton.Interactable = APIUtility.IsNeedSignup();
        }

        public void SetUUIDNotFoundState() {
            m_DataClearBotton.Interactable = false;
            m_CreateOfflineDataBotton.Interactable = false;
        }

        public void ShowUIOnSkip() {
            ShowUIAnim(eTitleUIAnim.TitleLogo);
            ShowUIAnim(eTitleUIAnim.Footer);
            m_IsObserveTitleLogo = false;
            RequestTitleCall();
        }

        public void ShowUIHeader() {
            ShowUIAnim(eTitleUIAnim.Header);
        }

        public void PlayUITitleLogo() {
            PlayUIAnim(eTitleUIAnim.TitleLogo);
            m_IsObserveTitleLogo = true;
        }

        public void PlayUIFooter() {
            PlayUIAnim(eTitleUIAnim.Footer);
        }

        public bool IsDisplay(eTitleUIAnim e_index) {
            return m_IsDisplayUIAnimList[(int)e_index];
        }

        public bool IsCompleteUIAnim(eTitleUIAnim e_index) {
            return m_UIAnimList[(int)e_index].IsEnd;
        }

        private void PlayUIAnim(eTitleUIAnim e_index) {
            m_UIAnimList[(int)e_index].PlayIn();
            m_IsDisplayUIAnimList[(int)e_index] = true;
        }

        private void ShowUIAnim(eTitleUIAnim e_index) {
            m_UIAnimList[(int)e_index].Show();
            m_IsDisplayUIAnimList[(int)e_index] = true;
        }

        private void Update() {
            if (m_IsObserveTitleLogo && IsCompleteUIAnim(eTitleUIAnim.TitleLogo)) {
                RequestTitleCall();
                m_IsObserveTitleLogo = false;
            }
        }

        public void UpdateCreatingOfflineDataState(float upNum, float max) {
            m_CreatingOfflineDataWindowGauge.fillAmount = upNum / max;
            if (m_CreatingOfflineDataWindowGauge.fillAmount > 1f) {
                m_CreatingOfflineDataWindowGauge.fillAmount = 1f;
            }
            m_CreatingOfflineDataWindowMessage.text = $"{upNum}/{max} {m_CreatingOfflineDataWindowGauge.fillAmount:P0}";
        }

        public void SetCreatingOfflineDataWindowOutLineMessage(string msg) {
            m_CreatingOfflineDataWindowOutLineMessage.text = msg;
        }

        public void SetCreatingOfflineDataLoading() {
            m_CreatingOfflineDataWindowMessage.text = "ロード中・・・";
        }

        public void SetDetectTouchActive(bool active) {
            if (m_DetectTouch != null) {
                m_DetectTouch.gameObject.SetActive(active);
            }
        }

        public void SetGoToNextSheetActive(bool active) {
            if (m_GoToNextSheet != null) {
                m_GoToNextSheet.gameObject.SetActive(active);
            }
            if (m_InputBlock != null) {
                m_InputBlock.SetActive(!active);
            }
        }

        public void OnClickDetectTouchCallback() {
            OnClickDetectTouch.Call();
        }

        public void OnClickStartButtonCallBack() {
            RequestTitleStart();
            GameSystem.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_TITLE_DECISION);
            SetGoToNextSheetActive(false);
            OnClickGameStartButton.Call();
        }

        public void OpenAgreement(Action<bool> onCloseAgreementCallback) {
            OnCloseAgreement = onCloseAgreementCallback;
            m_AgreeWindowGroup.OnEnd += OnEndAgreementWindow;
            m_AgreeWindowGroup.Open();
        }

        public void OnClickAgreementWindowButtonCallback(int index) {
            if (index == 0) {
                m_AgreeWindowGroup.Close();
                m_IsAgreed = true;
            } else if (index == 1) {
                m_AgreeWindowGroup.Close();
                OnCloseAgreement.Call(false);
            } else if (index == 2) {
                string url = WebDataListUtil.GetIDToAdress(1000);
                if (url != null) {
                    GameSystem.Inst.WebView.Open(GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.WVAgreementTitle), url);
                }
            }
        }

        private void OnEndAgreementWindow() {
            if (m_IsAgreed) {
                OnCloseAgreement.Call(true);
            }
            m_AgreeWindowGroup.OnEnd -= OnEndAgreementWindow;
        }

        public CustomButton GetCacheClearButton() {
            return m_CacheClearButton;
        }

        public void OnClickCacheClearButtonCallback() {
            string title = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainCacheClearTitle);
            string message = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainCacheClearMessage);
            GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, title, message, string.Empty, OnClickCheckCacheClearWindowButtonCallback);
        }

        public void OnClickCheckCacheClearWindowButtonCallback(int index) {
            if (index == 0) {
                OnClickCacheClearButton.Call();
            }
        }

        public void OnClickDataInheritanceButtonCallback() {
            m_DataInheritanceWindowGroup.Open();
        }

        private void OnSuccessPlayerMove() {
            OnSuccessPlayerMoveCallback.Call();
        }

        private void OnClickDataInheritanceWindowButtonCallback(int index) {
            if (index != 0) {
                m_DataInheritanceWindowGroup.Close();
                return;
            }
            OnClickPlayerMoveButton.Call(m_DataInheritanceWindowGroup.GetIdString(), m_DataInheritanceWindowGroup.GetPasswordString());
        }

        public void OnClickDataClearButtonCallback() {
            m_DataClearWindowGroup.Open();
        }

        public void OnClickDataClearWindowButtonCallback(int index) {
            m_DataClearWindowGroup.Close();
            if (index == 0) {
                string title = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearFinalTitle);
                string message = GameSystem.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearFinalMessage);
                GameSystem.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, title, message, string.Empty, OnClickDataClearFinalWindowButtonCallback);
            }
        }

        public void OnClickDataClearFinalWindowButtonCallback(int index) {
            if (index == 0) {
                OnClickPlayerResetButton();
            }
        }

        public void OnClickCreateOfflineDataCallback() {
            OnClickCreateOfflineDataButton();
        }

        public void OpenCreatingOfflineData(Action onQuitCreatingOfflineData) {
            OnQuitCreatingOfflineData = onQuitCreatingOfflineData;
            m_CreatingOfflineDataWindowMessage.text = string.Empty;
            m_CreatingOfflineDataWindowGauge.fillAmount = 0f;
            m_CreatingOfflineDataWindowGroup.Open();
        }

        public void CloseCreatingOfflineData() {
            OnQuitCreatingOfflineData = null;
            m_CreatingOfflineDataWindowMessage.text = string.Empty;
            m_CreatingOfflineDataWindowGauge.fillAmount = 0f;
            m_CreatingOfflineDataWindowGroup.Close();
        }

        public void OnClickCreatingOfflineDataQuitButtonCallback() {
            m_CreatingOfflineDataWindowGroup.Close();
            OnQuitCreatingOfflineData.Call();
        }

        public enum eTitleUIAnim {
            Header,
            TitleLogo,
            Footer,
            Num
        }
    }
}
