﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000CFD RID: 3325
	[Token(Token = "0x20008E5")]
	[StructLayout(3)]
	public class TitleIcon : ASyncImage
	{
		// Token: 0x06003CB6 RID: 15542 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003773")]
		[Address(RVA = "0x101599C90", Offset = "0x1599C90", VA = "0x101599C90")]
		public void Apply(eTitleType titleType)
		{
		}

		// Token: 0x06003CB7 RID: 15543 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003774")]
		[Address(RVA = "0x101599DA0", Offset = "0x1599DA0", VA = "0x101599DA0", Slot = "12")]
		public override void Destroy()
		{
		}

		// Token: 0x06003CB8 RID: 15544 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003775")]
		[Address(RVA = "0x101599DCC", Offset = "0x1599DCC", VA = "0x101599DCC")]
		public TitleIcon()
		{
		}

		// Token: 0x04004C03 RID: 19459
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x400356B")]
		protected eTitleType m_TitleType;
	}
}
