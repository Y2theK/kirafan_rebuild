﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D4B RID: 3403
	[Token(Token = "0x2000920")]
	[StructLayout(3)]
	public class TitleImage : MonoBehaviour
	{
		// Token: 0x06003E77 RID: 15991 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600392E")]
		[Address(RVA = "0x101599DDC", Offset = "0x1599DDC", VA = "0x101599DDC")]
		public void Setup()
		{
		}

		// Token: 0x06003E78 RID: 15992 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600392F")]
		[Address(RVA = "0x101599DE0", Offset = "0x1599DE0", VA = "0x101599DE0")]
		public void Destroy()
		{
		}

		// Token: 0x06003E79 RID: 15993 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003930")]
		[Address(RVA = "0x101599E9C", Offset = "0x1599E9C", VA = "0x101599E9C")]
		private void Update()
		{
		}

		// Token: 0x06003E7A RID: 15994 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003931")]
		[Address(RVA = "0x10159A01C", Offset = "0x159A01C", VA = "0x10159A01C")]
		public void Prepare(eTitleType title)
		{
		}

		// Token: 0x06003E7B RID: 15995 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003932")]
		[Address(RVA = "0x101599F8C", Offset = "0x1599F8C", VA = "0x101599F8C")]
		public void Display()
		{
		}

		// Token: 0x06003E7C RID: 15996 RVA: 0x00018A68 File Offset: 0x00016C68
		[Token(Token = "0x6003933")]
		[Address(RVA = "0x101599EDC", Offset = "0x1599EDC", VA = "0x101599EDC")]
		public bool IsTitleImageAvailable()
		{
			return default(bool);
		}

		// Token: 0x06003E7D RID: 15997 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003934")]
		[Address(RVA = "0x10159A140", Offset = "0x159A140", VA = "0x10159A140")]
		public void SetTitleImageEnabled(bool enabled)
		{
		}

		// Token: 0x06003E7E RID: 15998 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003935")]
		[Address(RVA = "0x10159A178", Offset = "0x159A178", VA = "0x10159A178")]
		public void AutoDisplay()
		{
		}

		// Token: 0x06003E7F RID: 15999 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003936")]
		[Address(RVA = "0x10159A1B8", Offset = "0x159A1B8", VA = "0x10159A1B8")]
		public TitleImage()
		{
		}

		// Token: 0x04004DBD RID: 19901
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036B9")]
		[SerializeField]
		private Image m_TitleImage;

		// Token: 0x04004DBE RID: 19902
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036BA")]
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04004DBF RID: 19903
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40036BB")]
		private bool m_autoDisplay;

		// Token: 0x04004DC0 RID: 19904
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40036BC")]
		private eTitleType m_TitleType;
	}
}
