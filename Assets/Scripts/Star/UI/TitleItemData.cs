﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI
{
	// Token: 0x02000C46 RID: 3142
	[Token(Token = "0x2000868")]
	[StructLayout(3)]
	public class TitleItemData : ScrollItemData
	{
		// Token: 0x060038C8 RID: 14536 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B2")]
		[Address(RVA = "0x10159A1C8", Offset = "0x159A1C8", VA = "0x10159A1C8", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x060038C9 RID: 14537 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033B3")]
		[Address(RVA = "0x10159A23C", Offset = "0x159A23C", VA = "0x10159A23C")]
		public TitleItemData()
		{
		}

		// Token: 0x0400480A RID: 18442
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4003289")]
		public eTitleType m_type;

		// Token: 0x0400480B RID: 18443
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x400328A")]
		public bool m_isNew;

		// Token: 0x0400480C RID: 18444
		[Cpp2IlInjected.FieldOffset(Offset = "0x1D")]
		[Token(Token = "0x400328B")]
		public bool m_isCurrent;

		// Token: 0x0400480D RID: 18445
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400328C")]
		public Action<int> m_CBOnclickPanel;
	}
}
