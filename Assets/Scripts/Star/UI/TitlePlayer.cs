﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000C4B RID: 3147
	[Token(Token = "0x200086D")]
	[StructLayout(3)]
	public class TitlePlayer : MonoBehaviour
	{
		// Token: 0x060038E3 RID: 14563 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033CD")]
		[Address(RVA = "0x10159A244", Offset = "0x159A244", VA = "0x10159A244")]
		private void Update()
		{
		}

		// Token: 0x060038E4 RID: 14564 RVA: 0x00017DA8 File Offset: 0x00015FA8
		[Token(Token = "0x60033CE")]
		[Address(RVA = "0x10159A360", Offset = "0x159A360", VA = "0x10159A360")]
		public bool IsPlaying()
		{
			return default(bool);
		}

		// Token: 0x060038E5 RID: 14565 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033CF")]
		[Address(RVA = "0x10159A370", Offset = "0x159A370", VA = "0x10159A370")]
		public void Play()
		{
		}

		// Token: 0x060038E6 RID: 14566 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D0")]
		[Address(RVA = "0x10159A2A4", Offset = "0x159A2A4", VA = "0x10159A2A4")]
		public void SetEnableRender(bool flg)
		{
		}

		// Token: 0x060038E7 RID: 14567 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60033D1")]
		[Address(RVA = "0x10159A3B0", Offset = "0x159A3B0", VA = "0x10159A3B0")]
		public TitlePlayer()
		{
		}

		// Token: 0x04004829 RID: 18473
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40032A8")]
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x0400482A RID: 18474
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40032A9")]
		[SerializeField]
		private bool m_RenderOffOnPlayEnd;

		// Token: 0x0400482B RID: 18475
		[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
		[Token(Token = "0x40032AA")]
		private TitlePlayer.eMode m_Mode;

		// Token: 0x0400482C RID: 18476
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40032AB")]
		private GameObject m_GameObject;

		// Token: 0x02000C4C RID: 3148
		[Token(Token = "0x20010A6")]
		private enum eMode
		{
			// Token: 0x0400482E RID: 18478
			[Token(Token = "0x40068D4")]
			None = -1,
			// Token: 0x0400482F RID: 18479
			[Token(Token = "0x40068D5")]
			Playing
		}
	}
}
