﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000D4C RID: 3404
	[Token(Token = "0x2000921")]
	[StructLayout(3)]
	public class ToggleImage : MonoBehaviour
	{
		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06003E80 RID: 16000 RVA: 0x00018A80 File Offset: 0x00016C80
		// (set) Token: 0x06003E81 RID: 16001 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x1700046A")]
		public bool Enable
		{
			[Token(Token = "0x6003937")]
			[Address(RVA = "0x10159A3C0", Offset = "0x159A3C0", VA = "0x10159A3C0")]
			get
			{
				return default(bool);
			}
			[Token(Token = "0x6003938")]
			[Address(RVA = "0x10159A3C8", Offset = "0x159A3C8", VA = "0x10159A3C8")]
			set
			{
			}
		}

		// Token: 0x06003E82 RID: 16002 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003939")]
		[Address(RVA = "0x10159A414", Offset = "0x159A414", VA = "0x10159A414")]
		public ToggleImage()
		{
		}

		// Token: 0x04004DC1 RID: 19905
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40036BD")]
		[SerializeField]
		private bool m_Enable;

		// Token: 0x04004DC2 RID: 19906
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40036BE")]
		[SerializeField]
		private Image m_TargetImage;

		// Token: 0x04004DC3 RID: 19907
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40036BF")]
		[SerializeField]
		private Sprite m_SpriteOff;

		// Token: 0x04004DC4 RID: 19908
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40036C0")]
		[SerializeField]
		private Sprite m_SpriteOn;
	}
}
