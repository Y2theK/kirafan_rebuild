﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI
{
	// Token: 0x02000CAD RID: 3245
	[Token(Token = "0x20008B3")]
	[StructLayout(3)]
	public class ToggleSwitch : MonoBehaviour
	{
		// Token: 0x06003B75 RID: 15221 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003637")]
		[Address(RVA = "0x10159A41C", Offset = "0x159A41C", VA = "0x10159A41C")]
		private void Start()
		{
		}

		// Token: 0x06003B76 RID: 15222 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003638")]
		[Address(RVA = "0x10159A4F0", Offset = "0x159A4F0", VA = "0x10159A4F0")]
		public void SetDecided(bool isOn)
		{
		}

		// Token: 0x06003B77 RID: 15223 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6003639")]
		[Address(RVA = "0x10159A554", Offset = "0x159A554", VA = "0x10159A554")]
		public void SetInteractable(bool interactable)
		{
		}

		// Token: 0x06003B78 RID: 15224 RVA: 0x000184C8 File Offset: 0x000166C8
		[Token(Token = "0x600363A")]
		[Address(RVA = "0x10159A5B8", Offset = "0x159A5B8", VA = "0x10159A5B8")]
		public bool IsOn()
		{
			return default(bool);
		}

		// Token: 0x06003B79 RID: 15225 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600363B")]
		[Address(RVA = "0x10159A5E8", Offset = "0x159A5E8", VA = "0x10159A5E8")]
		public void OnClickOnButtonCallBack()
		{
		}

		// Token: 0x06003B7A RID: 15226 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600363C")]
		[Address(RVA = "0x10159A624", Offset = "0x159A624", VA = "0x10159A624")]
		public void OnClickOffButtonCallBack()
		{
		}

		// Token: 0x06003B7B RID: 15227 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600363D")]
		[Address(RVA = "0x10159A660", Offset = "0x159A660", VA = "0x10159A660")]
		public ToggleSwitch()
		{
		}

		// Token: 0x04004A1D RID: 18973
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400343A")]
		[SerializeField]
		private CustomButton m_OnButton;

		// Token: 0x04004A1E RID: 18974
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400343B")]
		[SerializeField]
		private CustomButton m_OffButton;

		// Token: 0x04004A1F RID: 18975
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x400343C")]
		[SerializeField]
		private UnityEvent m_OnToggle;
	}
}
