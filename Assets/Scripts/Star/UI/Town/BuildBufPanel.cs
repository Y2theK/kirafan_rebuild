﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200102B RID: 4139
	[Token(Token = "0x2000AC0")]
	[StructLayout(3)]
	public class BuildBufPanel : MonoBehaviour
	{
		// Token: 0x06004F61 RID: 20321 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E6")]
		[Address(RVA = "0x10159A668", Offset = "0x159A668", VA = "0x10159A668")]
		public void SetActive(bool flg)
		{
		}

		// Token: 0x06004F62 RID: 20322 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E7")]
		[Address(RVA = "0x10159A6A4", Offset = "0x159A6A4", VA = "0x10159A6A4")]
		public void Apply(string type, float value)
		{
		}

		// Token: 0x06004F63 RID: 20323 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E8")]
		[Address(RVA = "0x10159A80C", Offset = "0x159A80C", VA = "0x10159A80C")]
		public BuildBufPanel()
		{
		}

		// Token: 0x040061C4 RID: 25028
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004490")]
		[SerializeField]
		private Text m_TypeTextObj;

		// Token: 0x040061C5 RID: 25029
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004491")]
		[SerializeField]
		private Text m_ValueTextObj;
	}
}
