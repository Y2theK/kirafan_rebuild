﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200102C RID: 4140
	[Token(Token = "0x2000AC1")]
	[StructLayout(3)]
	public class BuildGauge : MonoBehaviour
	{
		// Token: 0x06004F64 RID: 20324 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E9")]
		[Address(RVA = "0x10159A814", Offset = "0x159A814", VA = "0x10159A814")]
		private void Start()
		{
		}

		// Token: 0x06004F65 RID: 20325 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048EA")]
		[Address(RVA = "0x10159A8CC", Offset = "0x159A8CC", VA = "0x10159A8CC")]
		private void Update()
		{
		}

		// Token: 0x06004F66 RID: 20326 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048EB")]
		[Address(RVA = "0x10159A9CC", Offset = "0x159A9CC", VA = "0x10159A9CC")]
		public void Apply(float value)
		{
		}

		// Token: 0x06004F67 RID: 20327 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048EC")]
		[Address(RVA = "0x10159AA7C", Offset = "0x159AA7C", VA = "0x10159AA7C")]
		public void SetActive(bool flg)
		{
		}

		// Token: 0x06004F68 RID: 20328 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048ED")]
		[Address(RVA = "0x10159AAB8", Offset = "0x159AAB8", VA = "0x10159AAB8")]
		public BuildGauge()
		{
		}

		// Token: 0x040061C6 RID: 25030
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004492")]
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x040061C7 RID: 25031
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004493")]
		[SerializeField]
		private Image m_CompleteImage;

		// Token: 0x040061C8 RID: 25032
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004494")]
		private float m_Value;

		// Token: 0x040061C9 RID: 25033
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x4004495")]
		private bool m_IsDirty;
	}
}
