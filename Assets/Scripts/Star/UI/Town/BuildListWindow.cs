﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200102D RID: 4141
	[Token(Token = "0x2000AC2")]
	[StructLayout(3)]
	public class BuildListWindow : UIGroup
	{
		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x06004F69 RID: 20329 RVA: 0x0001BAE0 File Offset: 0x00019CE0
		[Token(Token = "0x170004FE")]
		public bool IsHaveList
		{
			[Token(Token = "0x60048EE")]
			[Address(RVA = "0x10159AAC0", Offset = "0x159AAC0", VA = "0x10159AAC0")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06004F6A RID: 20330 RVA: 0x0001BAF8 File Offset: 0x00019CF8
		[Token(Token = "0x170004FF")]
		public BuildListWindow.eButton SelectButton
		{
			[Token(Token = "0x60048EF")]
			[Address(RVA = "0x10159AAD0", Offset = "0x159AAD0", VA = "0x10159AAD0")]
			get
			{
				return BuildListWindow.eButton.Build;
			}
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06004F6B RID: 20331 RVA: 0x0001BB10 File Offset: 0x00019D10
		[Token(Token = "0x17000500")]
		public int SelectObjID
		{
			[Token(Token = "0x60048F0")]
			[Address(RVA = "0x10159AAD8", Offset = "0x159AAD8", VA = "0x10159AAD8")]
			get
			{
				return 0;
			}
		}

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06004F6C RID: 20332 RVA: 0x0001BB28 File Offset: 0x00019D28
		[Token(Token = "0x17000501")]
		public long SelectObjMngID
		{
			[Token(Token = "0x60048F1")]
			[Address(RVA = "0x10159AAE0", Offset = "0x159AAE0", VA = "0x10159AAE0")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06004F6D RID: 20333 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F2")]
		[Address(RVA = "0x10159AAE8", Offset = "0x159AAE8", VA = "0x10159AAE8")]
		public void Setup()
		{
		}

		// Token: 0x06004F6E RID: 20334 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F3")]
		[Address(RVA = "0x10159AB88", Offset = "0x159AB88", VA = "0x10159AB88", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F6F RID: 20335 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F4")]
		[Address(RVA = "0x10159AB90", Offset = "0x159AB90", VA = "0x10159AB90")]
		public void Open(TownBuildList.eCategory category)
		{
		}

		// Token: 0x06004F70 RID: 20336 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F5")]
		[Address(RVA = "0x10159B0C0", Offset = "0x159B0C0", VA = "0x10159B0C0", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004F71 RID: 20337 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F6")]
		[Address(RVA = "0x10159BDEC", Offset = "0x159BDEC", VA = "0x10159BDEC")]
		public void OnClickBuildCallBack(int objID)
		{
		}

		// Token: 0x06004F72 RID: 20338 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F7")]
		[Address(RVA = "0x10159BE04", Offset = "0x159BE04", VA = "0x10159BE04")]
		public void OnClickPlacementCallBack(long objMngID)
		{
		}

		// Token: 0x06004F73 RID: 20339 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F8")]
		[Address(RVA = "0x10159BE64", Offset = "0x159BE64", VA = "0x10159BE64")]
		public void OnClickSellCallBack(long objMngID)
		{
		}

		// Token: 0x06004F74 RID: 20340 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048F9")]
		[Address(RVA = "0x10159BE80", Offset = "0x159BE80", VA = "0x10159BE80")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004F75 RID: 20341 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048FA")]
		[Address(RVA = "0x10159BEA0", Offset = "0x159BEA0", VA = "0x10159BEA0")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004F76 RID: 20342 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048FB")]
		[Address(RVA = "0x10159B150", Offset = "0x159B150", VA = "0x10159B150")]
		private void Apply()
		{
		}

		// Token: 0x06004F77 RID: 20343 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048FC")]
		[Address(RVA = "0x10159C01C", Offset = "0x159C01C", VA = "0x10159C01C")]
		public BuildListWindow()
		{
		}

		// Token: 0x040061CA RID: 25034
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x4004496")]
		private BuildListWindow.eButton m_SelectButton;

		// Token: 0x040061CB RID: 25035
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x4004497")]
		private int m_SelectObjID;

		// Token: 0x040061CC RID: 25036
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x4004498")]
		private long m_SelectObjMngID;

		// Token: 0x040061CD RID: 25037
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004499")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x040061CE RID: 25038
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x400449A")]
		[SerializeField]
		private Text m_TitleTextObj;

		// Token: 0x040061CF RID: 25039
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x400449B")]
		[SerializeField]
		private Text m_HeaderText;

		// Token: 0x040061D0 RID: 25040
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x400449C")]
		[SerializeField]
		private TextField m_StockNum;

		// Token: 0x040061D1 RID: 25041
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x400449D")]
		private TownBuildList.eCategory m_Category;

		// Token: 0x040061D2 RID: 25042
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x400449E")]
		private TownBuildList m_TownBuildList;

		// Token: 0x040061D3 RID: 25043
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x400449F")]
		private bool m_IsDonePrepare;

		// Token: 0x0200102E RID: 4142
		[Token(Token = "0x200122D")]
		public enum eButton
		{
			// Token: 0x040061D5 RID: 25045
			[Token(Token = "0x400706C")]
			Build,
			// Token: 0x040061D6 RID: 25046
			[Token(Token = "0x400706D")]
			Placement,
			// Token: 0x040061D7 RID: 25047
			[Token(Token = "0x400706E")]
			Sell,
			// Token: 0x040061D8 RID: 25048
			[Token(Token = "0x400706F")]
			Cancel,
			// Token: 0x040061D9 RID: 25049
			[Token(Token = "0x4007070")]
			Back
		}
	}
}
