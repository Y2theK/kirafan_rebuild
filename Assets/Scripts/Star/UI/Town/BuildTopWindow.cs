﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x0200102F RID: 4143
	[Token(Token = "0x2000AC3")]
	[StructLayout(3)]
	public class BuildTopWindow : UIGroup
	{
		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x06004F78 RID: 20344 RVA: 0x0001BB40 File Offset: 0x00019D40
		[Token(Token = "0x17000502")]
		public BuildTopWindow.eButton SelectButton
		{
			[Token(Token = "0x60048FD")]
			[Address(RVA = "0x10159C0A0", Offset = "0x159C0A0", VA = "0x10159C0A0")]
			get
			{
				return BuildTopWindow.eButton.Area;
			}
		}

		// Token: 0x06004F79 RID: 20345 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048FE")]
		[Address(RVA = "0x10159C0A8", Offset = "0x159C0A8", VA = "0x10159C0A8", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F7A RID: 20346 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048FF")]
		[Address(RVA = "0x10159C290", Offset = "0x159C290", VA = "0x10159C290")]
		public void OnButtonCallBack(int buttonIdx)
		{
		}

		// Token: 0x06004F7B RID: 20347 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004900")]
		[Address(RVA = "0x10159C2A0", Offset = "0x159C2A0", VA = "0x10159C2A0")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004F7C RID: 20348 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004901")]
		[Address(RVA = "0x10159C2B4", Offset = "0x159C2B4", VA = "0x10159C2B4")]
		public BuildTopWindow()
		{
		}

		// Token: 0x040061DA RID: 25050
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044A0")]
		private BuildTopWindow.eButton m_SelectButton;

		// Token: 0x040061DB RID: 25051
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044A1")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x02001030 RID: 4144
		[Token(Token = "0x200122E")]
		public enum eButton
		{
			// Token: 0x040061DD RID: 25053
			[Token(Token = "0x4007072")]
			Area,
			// Token: 0x040061DE RID: 25054
			[Token(Token = "0x4007073")]
			Product,
			// Token: 0x040061DF RID: 25055
			[Token(Token = "0x4007074")]
			Buff,
			// Token: 0x040061E0 RID: 25056
			[Token(Token = "0x4007075")]
			Have,
			// Token: 0x040061E1 RID: 25057
			[Token(Token = "0x4007076")]
			Cancel
		}
	}
}
