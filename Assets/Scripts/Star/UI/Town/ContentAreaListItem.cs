﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001031 RID: 4145
	[Token(Token = "0x2000AC4")]
	[StructLayout(3)]
	public class ContentAreaListItem : ScrollItemIcon
	{
		// Token: 0x06004F7D RID: 20349 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004902")]
		[Address(RVA = "0x10159C2BC", Offset = "0x159C2BC", VA = "0x10159C2BC", Slot = "4")]
		protected override void Awake()
		{
		}

		// Token: 0x06004F7E RID: 20350 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004903")]
		[Address(RVA = "0x10159C2C4", Offset = "0x159C2C4", VA = "0x10159C2C4")]
		public void Apply()
		{
		}

		// Token: 0x06004F7F RID: 20351 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004904")]
		[Address(RVA = "0x10159C2D0", Offset = "0x159C2D0", VA = "0x10159C2D0")]
		private void Update()
		{
		}

		// Token: 0x06004F80 RID: 20352 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004905")]
		[Address(RVA = "0x10159C3E0", Offset = "0x159C3E0", VA = "0x10159C3E0", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004F81 RID: 20353 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004906")]
		[Address(RVA = "0x10159C9A8", Offset = "0x159C9A8", VA = "0x10159C9A8", Slot = "6")]
		public override void Destroy()
		{
		}

		// Token: 0x06004F82 RID: 20354 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004907")]
		[Address(RVA = "0x10159CA68", Offset = "0x159CA68", VA = "0x10159CA68")]
		public ContentAreaListItem()
		{
		}

		// Token: 0x040061E2 RID: 25058
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40044A2")]
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040061E3 RID: 25059
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40044A3")]
		[SerializeField]
		private TownObjectIcon m_objIcon;

		// Token: 0x040061E4 RID: 25060
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40044A4")]
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040061E5 RID: 25061
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40044A5")]
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x040061E6 RID: 25062
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40044A6")]
		[SerializeField]
		private GameObject m_Builded;

		// Token: 0x040061E7 RID: 25063
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40044A7")]
		[SerializeField]
		private Text m_textBuildLv;

		// Token: 0x040061E8 RID: 25064
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044A8")]
		private SpriteHandler m_SpriteHandler;
	}
}
