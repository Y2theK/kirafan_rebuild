﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Town
{
	// Token: 0x02001032 RID: 4146
	[Token(Token = "0x2000AC5")]
	[StructLayout(3)]
	public class ContentAreaListItemData : ScrollItemData
	{
		// Token: 0x14000105 RID: 261
		// (add) Token: 0x06004F83 RID: 20355 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004F84 RID: 20356 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000105")]
		public event Action<int, long> OnClick
		{
			[Token(Token = "0x6004908")]
			[Address(RVA = "0x10159CA70", Offset = "0x159CA70", VA = "0x10159CA70")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004909")]
			[Address(RVA = "0x10159CB7C", Offset = "0x159CB7C", VA = "0x10159CB7C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004F85 RID: 20357 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600490A")]
		[Address(RVA = "0x10159CC88", Offset = "0x159CC88", VA = "0x10159CC88")]
		public ContentAreaListItemData()
		{
		}

		// Token: 0x06004F86 RID: 20358 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600490B")]
		[Address(RVA = "0x10159CCBC", Offset = "0x159CCBC", VA = "0x10159CCBC", Slot = "4")]
		public override void OnClickItemCallBack()
		{
		}

		// Token: 0x040061E9 RID: 25065
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044A9")]
		public int m_ObjID;

		// Token: 0x040061EA RID: 25066
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044AA")]
		public long m_ObjMngID;

		// Token: 0x040061EB RID: 25067
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40044AB")]
		public int m_PlaceNum;
	}
}
