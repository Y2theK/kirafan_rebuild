﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001033 RID: 4147
	[Token(Token = "0x2000AC6")]
	[StructLayout(3)]
	public class ContentAreaListWindow : UIGroup
	{
		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06004F87 RID: 20359 RVA: 0x0001BB58 File Offset: 0x00019D58
		[Token(Token = "0x17000503")]
		public ContentAreaListWindow.eButton SelectButton
		{
			[Token(Token = "0x600490C")]
			[Address(RVA = "0x10159CD14", Offset = "0x159CD14", VA = "0x10159CD14")]
			get
			{
				return ContentAreaListWindow.eButton.Build;
			}
		}

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x06004F88 RID: 20360 RVA: 0x0001BB70 File Offset: 0x00019D70
		[Token(Token = "0x17000504")]
		public int SelectObjID
		{
			[Token(Token = "0x600490D")]
			[Address(RVA = "0x10159CD1C", Offset = "0x159CD1C", VA = "0x10159CD1C")]
			get
			{
				return 0;
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x06004F89 RID: 20361 RVA: 0x0001BB88 File Offset: 0x00019D88
		[Token(Token = "0x17000505")]
		public long SelectObjMngID
		{
			[Token(Token = "0x600490E")]
			[Address(RVA = "0x10159CD24", Offset = "0x159CD24", VA = "0x10159CD24")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x06004F8A RID: 20362 RVA: 0x0001BBA0 File Offset: 0x00019DA0
		[Token(Token = "0x17000506")]
		public bool IsSwapMode
		{
			[Token(Token = "0x600490F")]
			[Address(RVA = "0x10159CD2C", Offset = "0x159CD2C", VA = "0x10159CD2C")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004F8B RID: 20363 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004910")]
		[Address(RVA = "0x10159CD34", Offset = "0x159CD34", VA = "0x10159CD34")]
		public void Setup()
		{
		}

		// Token: 0x06004F8C RID: 20364 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004911")]
		[Address(RVA = "0x10159CDD4", Offset = "0x159CDD4", VA = "0x10159CDD4")]
		public void Open(bool swapMode)
		{
		}

		// Token: 0x06004F8D RID: 20365 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004912")]
		[Address(RVA = "0x10159D068", Offset = "0x159D068", VA = "0x10159D068", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F8E RID: 20366 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004913")]
		[Address(RVA = "0x10159D070", Offset = "0x159D070", VA = "0x10159D070", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004F8F RID: 20367 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004914")]
		[Address(RVA = "0x10159D2C8", Offset = "0x159D2C8", VA = "0x10159D2C8", Slot = "13")]
		public override void OnFinishPlayOut()
		{
		}

		// Token: 0x06004F90 RID: 20368 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004915")]
		[Address(RVA = "0x10159D30C", Offset = "0x159D30C", VA = "0x10159D30C")]
		public void OnClickItemCallBack(int objID, long objMngID)
		{
		}

		// Token: 0x06004F91 RID: 20369 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004916")]
		[Address(RVA = "0x10159D320", Offset = "0x159D320", VA = "0x10159D320")]
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06004F92 RID: 20370 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004917")]
		[Address(RVA = "0x10159D340", Offset = "0x159D340", VA = "0x10159D340")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004F93 RID: 20371 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004918")]
		[Address(RVA = "0x10159D0F0", Offset = "0x159D0F0", VA = "0x10159D0F0")]
		private void Apply()
		{
		}

		// Token: 0x06004F94 RID: 20372 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004919")]
		[Address(RVA = "0x10159D360", Offset = "0x159D360", VA = "0x10159D360")]
		public ContentAreaListWindow()
		{
		}

		// Token: 0x040061ED RID: 25069
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044AD")]
		private ContentAreaListWindow.eButton m_SelectButton;

		// Token: 0x040061EE RID: 25070
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x40044AE")]
		private int m_SelectObjID;

		// Token: 0x040061EF RID: 25071
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044AF")]
		private long m_SelectObjMngID;

		// Token: 0x040061F0 RID: 25072
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40044B0")]
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x040061F1 RID: 25073
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40044B1")]
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x040061F2 RID: 25074
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40044B2")]
		[SerializeField]
		private Text m_MessageText;

		// Token: 0x040061F3 RID: 25075
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40044B3")]
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x040061F4 RID: 25076
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40044B4")]
		private TownBuildList m_TownBuildList;

		// Token: 0x040061F5 RID: 25077
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40044B5")]
		private bool m_IsDonePrepare;

		// Token: 0x040061F6 RID: 25078
		[Cpp2IlInjected.FieldOffset(Offset = "0xC1")]
		[Token(Token = "0x40044B6")]
		private bool m_IsSwapMode;

		// Token: 0x02001034 RID: 4148
		[Token(Token = "0x200122F")]
		public enum eButton
		{
			// Token: 0x040061F8 RID: 25080
			[Token(Token = "0x4007078")]
			Build,
			// Token: 0x040061F9 RID: 25081
			[Token(Token = "0x4007079")]
			Cancel,
			// Token: 0x040061FA RID: 25082
			[Token(Token = "0x400707A")]
			Back
		}
	}
}
