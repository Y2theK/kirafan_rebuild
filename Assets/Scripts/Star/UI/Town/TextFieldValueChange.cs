﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200102A RID: 4138
	[Token(Token = "0x2000ABF")]
	[StructLayout(3)]
	public class TextFieldValueChange : MonoBehaviour
	{
		// Token: 0x06004F5B RID: 20315 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E0")]
		[Address(RVA = "0x10159D3DC", Offset = "0x159D3DC", VA = "0x10159D3DC")]
		public void Apply(int before, int after)
		{
		}

		// Token: 0x06004F5C RID: 20316 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E1")]
		[Address(RVA = "0x10159D584", Offset = "0x159D584", VA = "0x10159D584")]
		public void ApplyBuff(int before, int after)
		{
		}

		// Token: 0x06004F5D RID: 20317 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E2")]
		[Address(RVA = "0x10159D638", Offset = "0x159D638", VA = "0x10159D638")]
		public void ApplyPercent(float before, float after)
		{
		}

		// Token: 0x06004F5E RID: 20318 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E3")]
		[Address(RVA = "0x10159D6E0", Offset = "0x159D6E0", VA = "0x10159D6E0")]
		public void ApplyBuffPercent(float before, float after)
		{
		}

		// Token: 0x06004F5F RID: 20319 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E4")]
		[Address(RVA = "0x10159D43C", Offset = "0x159D43C", VA = "0x10159D43C")]
		public void Apply(string before, string after, float sub)
		{
		}

		// Token: 0x06004F60 RID: 20320 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x60048E5")]
		[Address(RVA = "0x10159D7A0", Offset = "0x159D7A0", VA = "0x10159D7A0")]
		public TextFieldValueChange()
		{
		}

		// Token: 0x040061C2 RID: 25026
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x400448E")]
		[SerializeField]
		private Text m_BeforeValue;

		// Token: 0x040061C3 RID: 25027
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x400448F")]
		[SerializeField]
		private Text m_AfterValue;
	}
}
