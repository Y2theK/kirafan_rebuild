﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using Star.Town;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02001037 RID: 4151
	[Token(Token = "0x2000AC9")]
	[StructLayout(3)]
	public class TownBufWindow : UIGroup
	{
		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x06004F9A RID: 20378 RVA: 0x0001BBB8 File Offset: 0x00019DB8
		[Token(Token = "0x17000507")]
		public TownBufWindow.eButton SelectButton
		{
			[Token(Token = "0x600491F")]
			[Address(RVA = "0x10159D7A8", Offset = "0x159D7A8", VA = "0x10159D7A8")]
			get
			{
				return TownBufWindow.eButton.Cancel;
			}
		}

		// Token: 0x06004F9B RID: 20379 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004920")]
		[Address(RVA = "0x10159D7B0", Offset = "0x159D7B0", VA = "0x10159D7B0", Slot = "5")]
		public override void Open()
		{
		}

		// Token: 0x06004F9C RID: 20380 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004921")]
		[Address(RVA = "0x10159E118", Offset = "0x159E118", VA = "0x10159E118")]
		public void OnCancelButtonCallBack()
		{
		}

		// Token: 0x06004F9D RID: 20381 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004922")]
		[Address(RVA = "0x10159D874", Offset = "0x159D874", VA = "0x10159D874")]
		private void Apply()
		{
		}

		// Token: 0x06004F9E RID: 20382 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004923")]
		[Address(RVA = "0x10159E128", Offset = "0x159E128", VA = "0x10159E128")]
		private void AddTitle(string title)
		{
		}

		// Token: 0x06004F9F RID: 20383 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004924")]
		[Address(RVA = "0x10159E344", Offset = "0x159E344", VA = "0x10159E344")]
		private void AddObj(string title, TownBuff.BuffParam param)
		{
		}

		// Token: 0x06004FA0 RID: 20384 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004925")]
		[Address(RVA = "0x10159E230", Offset = "0x159E230", VA = "0x10159E230")]
		private void AddObj(string title, string value)
		{
		}

		// Token: 0x06004FA1 RID: 20385 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004926")]
		[Address(RVA = "0x10159E704", Offset = "0x159E704", VA = "0x10159E704")]
		public TownBufWindow()
		{
		}

		// Token: 0x040061FD RID: 25085
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044B9")]
		private TownBufWindow.eButton m_SelectButton;

		// Token: 0x040061FE RID: 25086
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044BA")]
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x040061FF RID: 25087
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40044BB")]
		[SerializeField]
		private RectTransform m_NoEffectPrefab;

		// Token: 0x04006200 RID: 25088
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40044BC")]
		[SerializeField]
		private TownBuffItem m_TownBuffItemTitlePrefab;

		// Token: 0x04006201 RID: 25089
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40044BD")]
		[SerializeField]
		private TownBuffItem m_TownBuffItemPrefab;

		// Token: 0x04006202 RID: 25090
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40044BE")]
		private List<RectTransform> m_ItemList;

		// Token: 0x02001038 RID: 4152
		[Token(Token = "0x2001230")]
		public enum eButton
		{
			// Token: 0x04006204 RID: 25092
			[Token(Token = "0x400707C")]
			Cancel
		}
	}
}
