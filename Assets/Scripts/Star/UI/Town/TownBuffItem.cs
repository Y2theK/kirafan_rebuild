﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001036 RID: 4150
	[Token(Token = "0x2000AC8")]
	[StructLayout(3)]
	public class TownBuffItem : MonoBehaviour
	{
		// Token: 0x06004F98 RID: 20376 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600491D")]
		[Address(RVA = "0x10159E620", Offset = "0x159E620", VA = "0x10159E620")]
		public void Apply(string title, [Optional] string value)
		{
		}

		// Token: 0x06004F99 RID: 20377 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600491E")]
		[Address(RVA = "0x10159E774", Offset = "0x159E774", VA = "0x10159E774")]
		public TownBuffItem()
		{
		}

		// Token: 0x040061FB RID: 25083
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044B7")]
		[SerializeField]
		private Text m_TitleTextObj;

		// Token: 0x040061FC RID: 25084
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044B8")]
		[SerializeField]
		private Text m_ValueTextObj;
	}
}
