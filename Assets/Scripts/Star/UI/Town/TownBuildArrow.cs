﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02001039 RID: 4153
	[Token(Token = "0x2000ACA")]
	[StructLayout(3)]
	public class TownBuildArrow : MonoBehaviour
	{
		// Token: 0x06004FA2 RID: 20386 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004927")]
		[Address(RVA = "0x10159E77C", Offset = "0x159E77C", VA = "0x10159E77C")]
		private void Start()
		{
		}

		// Token: 0x06004FA3 RID: 20387 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004928")]
		[Address(RVA = "0x10159E7F4", Offset = "0x159E7F4", VA = "0x10159E7F4")]
		private void Update()
		{
		}

		// Token: 0x06004FA4 RID: 20388 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004929")]
		[Address(RVA = "0x10159E908", Offset = "0x159E908", VA = "0x10159E908")]
		public void SetActive(bool flg)
		{
		}

		// Token: 0x06004FA5 RID: 20389 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600492A")]
		[Address(RVA = "0x10159E944", Offset = "0x159E944", VA = "0x10159E944")]
		public TownBuildArrow()
		{
		}

		// Token: 0x04006205 RID: 25093
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044BF")]
		[SerializeField]
		private float m_Radius;

		// Token: 0x04006206 RID: 25094
		[Cpp2IlInjected.FieldOffset(Offset = "0x1C")]
		[Token(Token = "0x40044C0")]
		private Vector3 m_ArrowPos;

		// Token: 0x04006207 RID: 25095
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40044C1")]
		private Vector3 m_WorkVec;

		// Token: 0x04006208 RID: 25096
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40044C2")]
		private RectTransform m_RectTransform;
	}
}
