﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Town
{
	// Token: 0x0200103A RID: 4154
	[Token(Token = "0x2000ACB")]
	[StructLayout(3)]
	public class TownBuildList
	{
		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x06004FA6 RID: 20390 RVA: 0x0001BBD0 File Offset: 0x00019DD0
		[Token(Token = "0x17000508")]
		public bool IsAvailable
		{
			[Token(Token = "0x600492B")]
			[Address(RVA = "0x10159B148", Offset = "0x159B148", VA = "0x10159B148")]
			get
			{
				return default(bool);
			}
		}

		// Token: 0x06004FA7 RID: 20391 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600492C")]
		[Address(RVA = "0x10159B0B0", Offset = "0x159B0B0", VA = "0x10159B0B0")]
		public void Open(TownBuildList.eCategory category)
		{
		}

		// Token: 0x06004FA8 RID: 20392 RVA: 0x0001BBE8 File Offset: 0x00019DE8
		[Token(Token = "0x600492D")]
		[Address(RVA = "0x10159B5BC", Offset = "0x159B5BC", VA = "0x10159B5BC")]
		public bool UpdateOpen()
		{
			return default(bool);
		}

		// Token: 0x06004FA9 RID: 20393 RVA: 0x0001BC00 File Offset: 0x00019E00
		[Token(Token = "0x600492E")]
		[Address(RVA = "0x10159E9E4", Offset = "0x159E9E4", VA = "0x10159E9E4")]
		private static int CompareTownBuildSort(TownBuildList.TownObj A, TownBuildList.TownObj B)
		{
			return 0;
		}

		// Token: 0x06004FAA RID: 20394 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600492F")]
		[Address(RVA = "0x10159BEEC", Offset = "0x159BEEC", VA = "0x10159BEEC")]
		public TownBuildList.TownObj GetItem(int idx)
		{
			return null;
		}

		// Token: 0x06004FAB RID: 20395 RVA: 0x0001BC18 File Offset: 0x00019E18
		[Token(Token = "0x6004930")]
		[Address(RVA = "0x10159BFA4", Offset = "0x159BFA4", VA = "0x10159BFA4")]
		public int GetItemNum()
		{
			return 0;
		}

		// Token: 0x06004FAC RID: 20396 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004931")]
		[Address(RVA = "0x10159C098", Offset = "0x159C098", VA = "0x10159C098")]
		public TownBuildList()
		{
		}

		// Token: 0x04006209 RID: 25097
		[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
		[Token(Token = "0x40044C3")]
		private bool m_IsAvailable;

		// Token: 0x0400620A RID: 25098
		[Cpp2IlInjected.FieldOffset(Offset = "0x11")]
		[Token(Token = "0x40044C4")]
		private bool m_IsOpen;

		// Token: 0x0400620B RID: 25099
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044C5")]
		private List<TownBuildList.TownObj> m_ObjList;

		// Token: 0x0400620C RID: 25100
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044C6")]
		private TownShopListDB m_ShopListDB;

		// Token: 0x0400620D RID: 25101
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40044C7")]
		private TownBuildList.eCategory m_Category;

		// Token: 0x0200103B RID: 4155
		[Token(Token = "0x2001231")]
		public class TownObj
		{
			// Token: 0x06004FAD RID: 20397 RVA: 0x00002050 File Offset: 0x00000250
			[Token(Token = "0x600632B")]
			[Address(RVA = "0x10159E9DC", Offset = "0x159E9DC", VA = "0x10159E9DC")]
			public TownObj()
			{
			}

			// Token: 0x0400620E RID: 25102
			[Cpp2IlInjected.FieldOffset(Offset = "0x10")]
			[Token(Token = "0x400707D")]
			public int objID;

			// Token: 0x0400620F RID: 25103
			[Cpp2IlInjected.FieldOffset(Offset = "0x14")]
			[Token(Token = "0x400707E")]
			public int sortID;

			// Token: 0x04006210 RID: 25104
			[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
			[Token(Token = "0x400707F")]
			public long mngID;

			// Token: 0x04006211 RID: 25105
			[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
			[Token(Token = "0x4007080")]
			public int cost;

			// Token: 0x04006212 RID: 25106
			[Cpp2IlInjected.FieldOffset(Offset = "0x24")]
			[Token(Token = "0x4007081")]
			public int costKP;

			// Token: 0x04006213 RID: 25107
			[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
			[Token(Token = "0x4007082")]
			public int lv;

			// Token: 0x04006214 RID: 25108
			[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
			[Token(Token = "0x4007083")]
			public int buildNum;
		}

		// Token: 0x0200103C RID: 4156
		[Token(Token = "0x2001232")]
		public enum eCategory
		{
			// Token: 0x04006216 RID: 25110
			[Token(Token = "0x4007085")]
			Area,
			// Token: 0x04006217 RID: 25111
			[Token(Token = "0x4007086")]
			Product,
			// Token: 0x04006218 RID: 25112
			[Token(Token = "0x4007087")]
			Buff,
			// Token: 0x04006219 RID: 25113
			[Token(Token = "0x4007088")]
			Have
		}
	}
}
