﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x0200103D RID: 4157
	[Token(Token = "0x2000ACC")]
	[StructLayout(3)]
	public class TownBuildSelectItem : ScrollItemIcon
	{
		// Token: 0x06004FAE RID: 20398 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004932")]
		[Address(RVA = "0x10159EA44", Offset = "0x159EA44", VA = "0x10159EA44")]
		public void Apply()
		{
		}

		// Token: 0x06004FAF RID: 20399 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004933")]
		[Address(RVA = "0x10159EA50", Offset = "0x159EA50", VA = "0x10159EA50", Slot = "5")]
		protected override void ApplyData()
		{
		}

		// Token: 0x06004FB0 RID: 20400 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004934")]
		[Address(RVA = "0x10159F1B0", Offset = "0x159F1B0", VA = "0x10159F1B0")]
		public void OnClickBuildCallBack()
		{
		}

		// Token: 0x06004FB1 RID: 20401 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004935")]
		[Address(RVA = "0x10159F354", Offset = "0x159F354", VA = "0x10159F354")]
		public void OnClickPlacementCallBack()
		{
		}

		// Token: 0x06004FB2 RID: 20402 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004936")]
		[Address(RVA = "0x10159F450", Offset = "0x159F450", VA = "0x10159F450")]
		public void OnClickSellCallBack()
		{
		}

		// Token: 0x06004FB3 RID: 20403 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004937")]
		[Address(RVA = "0x10159F54C", Offset = "0x159F54C", VA = "0x10159F54C")]
		public TownBuildSelectItem()
		{
		}

		// Token: 0x0400621A RID: 25114
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x40044C8")]
		[SerializeField]
		private TownObjectIcon m_Icon;

		// Token: 0x0400621B RID: 25115
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x40044C9")]
		[SerializeField]
		private Text m_HaveNumTextObj;

		// Token: 0x0400621C RID: 25116
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x40044CA")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x0400621D RID: 25117
		[Cpp2IlInjected.FieldOffset(Offset = "0x70")]
		[Token(Token = "0x40044CB")]
		[SerializeField]
		private GameObject m_BuildObj;

		// Token: 0x0400621E RID: 25118
		[Cpp2IlInjected.FieldOffset(Offset = "0x78")]
		[Token(Token = "0x40044CC")]
		[SerializeField]
		private CustomButton m_BuildButton;

		// Token: 0x0400621F RID: 25119
		[Cpp2IlInjected.FieldOffset(Offset = "0x80")]
		[Token(Token = "0x40044CD")]
		[SerializeField]
		private GameObject m_CostGoldObj;

		// Token: 0x04006220 RID: 25120
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044CE")]
		[SerializeField]
		private Text m_CostGoldTextObj;

		// Token: 0x04006221 RID: 25121
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044CF")]
		[SerializeField]
		private GameObject m_CostKPObj;

		// Token: 0x04006222 RID: 25122
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40044D0")]
		[SerializeField]
		private Text m_CostKPTextObj;

		// Token: 0x04006223 RID: 25123
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40044D1")]
		[SerializeField]
		private GameObject m_StoreObj;

		// Token: 0x04006224 RID: 25124
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40044D2")]
		[SerializeField]
		private Text m_textBuildLv;

		// Token: 0x04006225 RID: 25125
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40044D3")]
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x04006226 RID: 25126
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40044D4")]
		[SerializeField]
		private CustomButton m_SellButton;

		// Token: 0x04006227 RID: 25127
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40044D5")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;
	}
}
