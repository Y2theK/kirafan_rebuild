﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;

namespace Star.UI.Town
{
	// Token: 0x0200103E RID: 4158
	[Token(Token = "0x2000ACD")]
	[StructLayout(3)]
	public class TownBuildSelectItemData : ScrollItemData
	{
		// Token: 0x06004FB4 RID: 20404 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004938")]
		[Address(RVA = "0x10159BEC0", Offset = "0x159BEC0", VA = "0x10159BEC0")]
		public TownBuildSelectItemData()
		{
		}

		// Token: 0x04006228 RID: 25128
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044D6")]
		public int m_ObjID;

		// Token: 0x04006229 RID: 25129
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044D7")]
		public long m_ObjMngID;

		// Token: 0x0400622A RID: 25130
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40044D8")]
		public bool m_IsStore;

		// Token: 0x0400622B RID: 25131
		[Cpp2IlInjected.FieldOffset(Offset = "0x2C")]
		[Token(Token = "0x40044D9")]
		public int m_PlaceNum;

		// Token: 0x0400622C RID: 25132
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40044DA")]
		public int m_Cost;

		// Token: 0x0400622D RID: 25133
		[Cpp2IlInjected.FieldOffset(Offset = "0x34")]
		[Token(Token = "0x40044DB")]
		public int m_CostKP;

		// Token: 0x0400622E RID: 25134
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40044DC")]
		public int m_BuildLv;

		// Token: 0x0400622F RID: 25135
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40044DD")]
		public Action<int> OnClickBuild;

		// Token: 0x04006230 RID: 25136
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x40044DE")]
		public Action<long> OnClickPlacement;

		// Token: 0x04006231 RID: 25137
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x40044DF")]
		public Action<long> OnClickSell;
	}
}
