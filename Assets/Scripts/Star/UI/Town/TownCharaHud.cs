﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x0200103F RID: 4159
	[Token(Token = "0x2000ACE")]
	[StructLayout(3)]
	public class TownCharaHud : MonoBehaviour
	{
		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x06004FB5 RID: 20405 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x17000509")]
		public RectTransform CacheTransform
		{
			[Token(Token = "0x6004939")]
			[Address(RVA = "0x10159F554", Offset = "0x159F554", VA = "0x10159F554")]
			get
			{
				return null;
			}
		}

		// Token: 0x06004FB6 RID: 20406 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600493A")]
		[Address(RVA = "0x10159F55C", Offset = "0x159F55C", VA = "0x10159F55C")]
		private void Awake()
		{
		}

		// Token: 0x06004FB7 RID: 20407 RVA: 0x00002052 File Offset: 0x00000252
		[Token(Token = "0x600493B")]
		[Address(RVA = "0x10159F600", Offset = "0x159F600", VA = "0x10159F600")]
		public TownCharaTweet GetCharaTweet()
		{
			return null;
		}

		// Token: 0x06004FB8 RID: 20408 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600493C")]
		[Address(RVA = "0x10159F608", Offset = "0x159F608", VA = "0x10159F608")]
		public TownCharaHud()
		{
		}

		// Token: 0x04006232 RID: 25138
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044E0")]
		[SerializeField]
		private TownCharaTweet m_CharaTweet;

		// Token: 0x04006233 RID: 25139
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044E1")]
		private RectTransform m_Transform;
	}
}
