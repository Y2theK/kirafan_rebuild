﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001040 RID: 4160
	[Token(Token = "0x2000ACF")]
	[StructLayout(3)]
	public class TownCharaTweet : MonoBehaviour
	{
		// Token: 0x06004FB9 RID: 20409 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600493D")]
		[Address(RVA = "0x10159F610", Offset = "0x159F610", VA = "0x10159F610")]
		public void Open(string text)
		{
		}

		// Token: 0x06004FBA RID: 20410 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600493E")]
		[Address(RVA = "0x10159F684", Offset = "0x159F684", VA = "0x10159F684")]
		public void Close()
		{
		}

		// Token: 0x06004FBB RID: 20411 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600493F")]
		[Address(RVA = "0x10159F6BC", Offset = "0x159F6BC", VA = "0x10159F6BC")]
		public TownCharaTweet()
		{
		}

		// Token: 0x04006234 RID: 25140
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044E2")]
		[SerializeField]
		private Text m_Text;

		// Token: 0x04006235 RID: 25141
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044E3")]
		private RectTransform m_Transform;
	}
}
