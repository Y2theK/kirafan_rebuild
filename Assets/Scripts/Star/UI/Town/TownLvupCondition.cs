﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001041 RID: 4161
	[Token(Token = "0x2000AD0")]
	[StructLayout(3)]
	public class TownLvupCondition : MonoBehaviour
	{
		// Token: 0x06004FBC RID: 20412 RVA: 0x0001BC30 File Offset: 0x00019E30
		[Token(Token = "0x6004940")]
		[Address(RVA = "0x10159F6C4", Offset = "0x159F6C4", VA = "0x10159F6C4")]
		public bool Apply(int objID, int afterLv)
		{
			return default(bool);
		}

		// Token: 0x06004FBD RID: 20413 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004941")]
		[Address(RVA = "0x10159FCAC", Offset = "0x159FCAC", VA = "0x10159FCAC")]
		public TownLvupCondition()
		{
		}

		// Token: 0x04006236 RID: 25142
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x40044E4")]
		[SerializeField]
		private Text m_ConditionText;

		// Token: 0x04006237 RID: 25143
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x40044E5")]
		[SerializeField]
		private Text m_CostTimeText;

		// Token: 0x04006238 RID: 25144
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x40044E6")]
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x04006239 RID: 25145
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x40044E7")]
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x0400623A RID: 25146
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x40044E8")]
		[SerializeField]
		private Text m_CostKPText;

		// Token: 0x0400623B RID: 25147
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x40044E9")]
		[SerializeField]
		private Text m_HaveKPText;
	}
}
