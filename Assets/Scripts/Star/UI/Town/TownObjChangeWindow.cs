﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001042 RID: 4162
	[Token(Token = "0x2000AD1")]
	[StructLayout(3)]
	public class TownObjChangeWindow : UIGroup
	{
		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x06004FBE RID: 20414 RVA: 0x0001BC48 File Offset: 0x00019E48
		[Token(Token = "0x1700050A")]
		public TownObjChangeWindow.eButton SelectButton
		{
			[Token(Token = "0x6004942")]
			[Address(RVA = "0x10159FCB4", Offset = "0x159FCB4", VA = "0x10159FCB4")]
			get
			{
				return TownObjChangeWindow.eButton.Yes;
			}
		}

		// Token: 0x06004FBF RID: 20415 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004943")]
		[Address(RVA = "0x10159FCBC", Offset = "0x159FCBC", VA = "0x10159FCBC")]
		public void Open(long basemngID, int targetobjID)
		{
		}

		// Token: 0x06004FC0 RID: 20416 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004944")]
		[Address(RVA = "0x10159FFBC", Offset = "0x159FFBC", VA = "0x10159FFBC")]
		public void OnClickYesButtonCallBack()
		{
		}

		// Token: 0x06004FC1 RID: 20417 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004945")]
		[Address(RVA = "0x10159FFCC", Offset = "0x159FFCC", VA = "0x10159FFCC")]
		public void OnClickNoButtonCallBack()
		{
		}

		// Token: 0x06004FC2 RID: 20418 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004946")]
		[Address(RVA = "0x10159FD90", Offset = "0x159FD90", VA = "0x10159FD90")]
		private void Apply()
		{
		}

		// Token: 0x06004FC3 RID: 20419 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004947")]
		[Address(RVA = "0x10159FFE0", Offset = "0x159FFE0", VA = "0x10159FFE0")]
		public TownObjChangeWindow()
		{
		}

		// Token: 0x0400623C RID: 25148
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044EA")]
		private long m_BaseObjMngID;

		// Token: 0x0400623D RID: 25149
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044EB")]
		private int m_TargetObjID;

		// Token: 0x0400623E RID: 25150
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40044EC")]
		[SerializeField]
		private TownObjectIcon m_BaseIcon;

		// Token: 0x0400623F RID: 25151
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40044ED")]
		[SerializeField]
		private Text m_BaseName;

		// Token: 0x04006240 RID: 25152
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40044EE")]
		[SerializeField]
		private TownObjectIcon m_TargetIcon;

		// Token: 0x04006241 RID: 25153
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40044EF")]
		[SerializeField]
		private Text m_TargetName;

		// Token: 0x04006242 RID: 25154
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40044F0")]
		private TownObjChangeWindow.eButton m_SelectButton;

		// Token: 0x02001043 RID: 4163
		[Token(Token = "0x2001233")]
		public enum eButton
		{
			// Token: 0x04006244 RID: 25156
			[Token(Token = "0x400708A")]
			Yes,
			// Token: 0x04006245 RID: 25157
			[Token(Token = "0x400708B")]
			No
		}
	}
}
