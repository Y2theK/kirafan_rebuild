﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001044 RID: 4164
	[Token(Token = "0x2000AD2")]
	[StructLayout(3)]
	public class TownObjConfirmWindow : UIGroup
	{
		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x06004FC4 RID: 20420 RVA: 0x0001BC60 File Offset: 0x00019E60
		[Token(Token = "0x1700050B")]
		public TownObjConfirmWindow.eButton SelectButton
		{
			[Token(Token = "0x6004948")]
			[Address(RVA = "0x10159FFE8", Offset = "0x159FFE8", VA = "0x10159FFE8")]
			get
			{
				return TownObjConfirmWindow.eButton.None;
			}
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06004FC5 RID: 20421 RVA: 0x0001BC78 File Offset: 0x00019E78
		[Token(Token = "0x1700050C")]
		public TownObjConfirmWindow.eMode Mode
		{
			[Token(Token = "0x6004949")]
			[Address(RVA = "0x10159FFF0", Offset = "0x159FFF0", VA = "0x10159FFF0")]
			get
			{
				return TownObjConfirmWindow.eMode.Build;
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06004FC6 RID: 20422 RVA: 0x0001BC90 File Offset: 0x00019E90
		[Token(Token = "0x1700050D")]
		public long MngID
		{
			[Token(Token = "0x600494A")]
			[Address(RVA = "0x10159FFF8", Offset = "0x159FFF8", VA = "0x10159FFF8")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x06004FC7 RID: 20423 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600494B")]
		[Address(RVA = "0x1015A0000", Offset = "0x15A0000", VA = "0x1015A0000")]
		private void HideAllObj()
		{
		}

		// Token: 0x06004FC8 RID: 20424 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600494C")]
		[Address(RVA = "0x1015A0158", Offset = "0x15A0158", VA = "0x1015A0158")]
		public void SetupBuild(int objID, bool tutorial = false)
		{
		}

		// Token: 0x06004FC9 RID: 20425 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600494D")]
		[Address(RVA = "0x1015A1330", Offset = "0x15A1330", VA = "0x1015A1330", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004FCA RID: 20426 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600494E")]
		[Address(RVA = "0x1015A1564", Offset = "0x15A1564", VA = "0x1015A1564")]
		public void SetupPlacement(long mngID)
		{
		}

		// Token: 0x06004FCB RID: 20427 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600494F")]
		[Address(RVA = "0x1015A16F8", Offset = "0x15A16F8", VA = "0x1015A16F8")]
		public void SetupSwapArea(int objID)
		{
		}

		// Token: 0x06004FCC RID: 20428 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004950")]
		[Address(RVA = "0x1015A18A4", Offset = "0x15A18A4", VA = "0x1015A18A4")]
		public void SetupQuick(long mngID)
		{
		}

		// Token: 0x06004FCD RID: 20429 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004951")]
		[Address(RVA = "0x1015A1C3C", Offset = "0x15A1C3C", VA = "0x1015A1C3C")]
		public void SetupLevelUp(long mngID)
		{
		}

		// Token: 0x06004FCE RID: 20430 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004952")]
		[Address(RVA = "0x1015A3014", Offset = "0x15A3014", VA = "0x1015A3014")]
		public void SetupSell(long mngID)
		{
		}

		// Token: 0x06004FCF RID: 20431 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004953")]
		[Address(RVA = "0x1015A3458", Offset = "0x15A3458", VA = "0x1015A3458")]
		public void OnClickYesCallBack()
		{
		}

		// Token: 0x06004FD0 RID: 20432 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004954")]
		[Address(RVA = "0x1015A3598", Offset = "0x15A3598", VA = "0x1015A3598")]
		public void OnClickNoCallBack()
		{
		}

		// Token: 0x06004FD1 RID: 20433 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004955")]
		[Address(RVA = "0x1015A35AC", Offset = "0x15A35AC", VA = "0x1015A35AC")]
		public TownObjConfirmWindow()
		{
		}

		// Token: 0x04006246 RID: 25158
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x40044F1")]
		[SerializeField]
		private Text m_Title;

		// Token: 0x04006247 RID: 25159
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x40044F2")]
		[SerializeField]
		private TownObjInfo m_ObjInfo;

		// Token: 0x04006248 RID: 25160
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x40044F3")]
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04006249 RID: 25161
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x40044F4")]
		[SerializeField]
		private Text m_CostTimeText;

		// Token: 0x0400624A RID: 25162
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x40044F5")]
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x0400624B RID: 25163
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x40044F6")]
		[SerializeField]
		private Text m_CostHaveGoldText;

		// Token: 0x0400624C RID: 25164
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x40044F7")]
		[SerializeField]
		private Text m_CostKPText;

		// Token: 0x0400624D RID: 25165
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x40044F8")]
		[SerializeField]
		private Text m_CostHaveKPText;

		// Token: 0x0400624E RID: 25166
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x40044F9")]
		[SerializeField]
		private Text m_SellGoldText;

		// Token: 0x0400624F RID: 25167
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x40044FA")]
		[SerializeField]
		private Text m_SellHaveGoldText;

		// Token: 0x04006250 RID: 25168
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x40044FB")]
		[SerializeField]
		private Text m_SellKPText;

		// Token: 0x04006251 RID: 25169
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x40044FC")]
		[SerializeField]
		private Text m_SellHaveKPText;

		// Token: 0x04006252 RID: 25170
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x40044FD")]
		[SerializeField]
		private Text m_RemainTimeText;

		// Token: 0x04006253 RID: 25171
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x40044FE")]
		[SerializeField]
		private Text m_CostGemText;

		// Token: 0x04006254 RID: 25172
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x40044FF")]
		[SerializeField]
		private Text m_HaveGemText;

		// Token: 0x04006255 RID: 25173
		[Cpp2IlInjected.FieldOffset(Offset = "0x100")]
		[Token(Token = "0x4004500")]
		[SerializeField]
		private TownLvupCondition m_LvCondition;

		// Token: 0x04006256 RID: 25174
		[Cpp2IlInjected.FieldOffset(Offset = "0x108")]
		[Token(Token = "0x4004501")]
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x04006257 RID: 25175
		[Cpp2IlInjected.FieldOffset(Offset = "0x110")]
		[Token(Token = "0x4004502")]
		[SerializeField]
		private GameObject m_BuildInfoTitle;

		// Token: 0x04006258 RID: 25176
		[Cpp2IlInjected.FieldOffset(Offset = "0x118")]
		[Token(Token = "0x4004503")]
		[SerializeField]
		private GameObject m_SellInfoTitle;

		// Token: 0x04006259 RID: 25177
		[Cpp2IlInjected.FieldOffset(Offset = "0x120")]
		[Token(Token = "0x4004504")]
		[SerializeField]
		private GameObject m_CostTimeObj;

		// Token: 0x0400625A RID: 25178
		[Cpp2IlInjected.FieldOffset(Offset = "0x128")]
		[Token(Token = "0x4004505")]
		[SerializeField]
		private GameObject m_CostGold;

		// Token: 0x0400625B RID: 25179
		[Cpp2IlInjected.FieldOffset(Offset = "0x130")]
		[Token(Token = "0x4004506")]
		[SerializeField]
		private GameObject m_CostKP;

		// Token: 0x0400625C RID: 25180
		[Cpp2IlInjected.FieldOffset(Offset = "0x138")]
		[Token(Token = "0x4004507")]
		[SerializeField]
		private GameObject m_SellGold;

		// Token: 0x0400625D RID: 25181
		[Cpp2IlInjected.FieldOffset(Offset = "0x140")]
		[Token(Token = "0x4004508")]
		[SerializeField]
		private GameObject m_SellKP;

		// Token: 0x0400625E RID: 25182
		[Cpp2IlInjected.FieldOffset(Offset = "0x148")]
		[Token(Token = "0x4004509")]
		[SerializeField]
		private GameObject m_CostRemainTime;

		// Token: 0x0400625F RID: 25183
		[Cpp2IlInjected.FieldOffset(Offset = "0x150")]
		[Token(Token = "0x400450A")]
		[SerializeField]
		private GameObject m_CostGem;

		// Token: 0x04006260 RID: 25184
		[Cpp2IlInjected.FieldOffset(Offset = "0x158")]
		[Token(Token = "0x400450B")]
		[SerializeField]
		private GameObject m_LvUp;

		// Token: 0x04006261 RID: 25185
		[Cpp2IlInjected.FieldOffset(Offset = "0x160")]
		[Token(Token = "0x400450C")]
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x04006262 RID: 25186
		[Cpp2IlInjected.FieldOffset(Offset = "0x168")]
		[Token(Token = "0x400450D")]
		private TownObjConfirmWindow.eMode m_Mode;

		// Token: 0x04006263 RID: 25187
		[Cpp2IlInjected.FieldOffset(Offset = "0x16C")]
		[Token(Token = "0x400450E")]
		private int m_ObjID;

		// Token: 0x04006264 RID: 25188
		[Cpp2IlInjected.FieldOffset(Offset = "0x170")]
		[Token(Token = "0x400450F")]
		private long m_MngID;

		// Token: 0x04006265 RID: 25189
		[Cpp2IlInjected.FieldOffset(Offset = "0x178")]
		[Token(Token = "0x4004510")]
		private TownObjConfirmWindow.eButton m_Button;

		// Token: 0x02001045 RID: 4165
		[Token(Token = "0x2001234")]
		public enum eMode
		{
			// Token: 0x04006267 RID: 25191
			[Token(Token = "0x400708D")]
			Build,
			// Token: 0x04006268 RID: 25192
			[Token(Token = "0x400708E")]
			Placement,
			// Token: 0x04006269 RID: 25193
			[Token(Token = "0x400708F")]
			Quick,
			// Token: 0x0400626A RID: 25194
			[Token(Token = "0x4007090")]
			Levelup,
			// Token: 0x0400626B RID: 25195
			[Token(Token = "0x4007091")]
			Sell
		}

		// Token: 0x02001046 RID: 4166
		[Token(Token = "0x2001235")]
		public enum eButton
		{
			// Token: 0x0400626D RID: 25197
			[Token(Token = "0x4007093")]
			None,
			// Token: 0x0400626E RID: 25198
			[Token(Token = "0x4007094")]
			Yes,
			// Token: 0x0400626F RID: 25199
			[Token(Token = "0x4007095")]
			No
		}
	}
}
