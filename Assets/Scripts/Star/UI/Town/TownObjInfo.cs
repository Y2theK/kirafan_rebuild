﻿using System;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001048 RID: 4168
	[Token(Token = "0x2000AD4")]
	[StructLayout(3)]
	public class TownObjInfo : MonoBehaviour
	{
		// Token: 0x06004FD5 RID: 20437 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004959")]
		[Address(RVA = "0x1015A0854", Offset = "0x15A0854", VA = "0x1015A0854")]
		public void Apply(long objMngID)
		{
		}

		// Token: 0x06004FD6 RID: 20438 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600495A")]
		[Address(RVA = "0x1015A08B4", Offset = "0x15A08B4", VA = "0x1015A08B4")]
		public void Apply(int objID, int lv)
		{
		}

		// Token: 0x06004FD7 RID: 20439 RVA: 0x0001BCA8 File Offset: 0x00019EA8
		[Token(Token = "0x600495B")]
		[Address(RVA = "0x1015A3A24", Offset = "0x15A3A24", VA = "0x1015A3A24")]
		public int GetMoneyMaxStock(TownObjectLevelUpDB_Param param)
		{
			return 0;
		}

		// Token: 0x06004FD8 RID: 20440 RVA: 0x0001BCC0 File Offset: 0x00019EC0
		[Token(Token = "0x600495C")]
		[Address(RVA = "0x1015A3A8C", Offset = "0x15A3A8C", VA = "0x1015A3A8C")]
		public int GetMoneySpeed(TownObjectLevelUpDB_Param param)
		{
			return 0;
		}

		// Token: 0x06004FD9 RID: 20441 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600495D")]
		[Address(RVA = "0x1015A1E8C", Offset = "0x15A1E8C", VA = "0x1015A1E8C")]
		public void ApplyLvUp(long objMngID, int beforeLv, int afterLv)
		{
		}

		// Token: 0x06004FDA RID: 20442 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600495E")]
		[Address(RVA = "0x1015A35C4", Offset = "0x15A35C4", VA = "0x1015A35C4")]
		private void ApplyBaseInfo(int objID, int lv)
		{
		}

		// Token: 0x06004FDB RID: 20443 RVA: 0x0001BCD8 File Offset: 0x00019ED8
		[Token(Token = "0x600495F")]
		[Address(RVA = "0x1015A3A14", Offset = "0x15A3A14", VA = "0x1015A3A14")]
		private int BuffTypeToIdx(eTownObjectBuffType buffType)
		{
			return 0;
		}

		// Token: 0x06004FDC RID: 20444 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004960")]
		[Address(RVA = "0x1015A3AFC", Offset = "0x15A3AFC", VA = "0x1015A3AFC")]
		public TownObjInfo()
		{
		}

		// Token: 0x04006271 RID: 25201
		[Cpp2IlInjected.FieldOffset(Offset = "0x18")]
		[Token(Token = "0x4004512")]
		[SerializeField]
		private TownObjectIcon m_Icon;

		// Token: 0x04006272 RID: 25202
		[Cpp2IlInjected.FieldOffset(Offset = "0x20")]
		[Token(Token = "0x4004513")]
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04006273 RID: 25203
		[Cpp2IlInjected.FieldOffset(Offset = "0x28")]
		[Token(Token = "0x4004514")]
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04006274 RID: 25204
		[Cpp2IlInjected.FieldOffset(Offset = "0x30")]
		[Token(Token = "0x4004515")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E1CC", Offset = "0x12E1CC")]
		private TextField m_LvText;

		// Token: 0x04006275 RID: 25205
		[Cpp2IlInjected.FieldOffset(Offset = "0x38")]
		[Token(Token = "0x4004516")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E218", Offset = "0x12E218")]
		private TextFieldValueChange m_LvValueChange;

		// Token: 0x04006276 RID: 25206
		[Cpp2IlInjected.FieldOffset(Offset = "0x40")]
		[Token(Token = "0x4004517")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E264", Offset = "0x12E264")]
		private GameObject m_EffectObj;

		// Token: 0x04006277 RID: 25207
		[Cpp2IlInjected.FieldOffset(Offset = "0x48")]
		[Token(Token = "0x4004518")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E2B0", Offset = "0x12E2B0")]
		private Text m_EffectText;

		// Token: 0x04006278 RID: 25208
		[Cpp2IlInjected.FieldOffset(Offset = "0x50")]
		[Token(Token = "0x4004519")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E2FC", Offset = "0x12E2FC")]
		private GameObject m_BuffObj;

		// Token: 0x04006279 RID: 25209
		[Cpp2IlInjected.FieldOffset(Offset = "0x58")]
		[Token(Token = "0x400451A")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E348", Offset = "0x12E348")]
		private TextField[] m_StatusTexts;

		// Token: 0x0400627A RID: 25210
		[Cpp2IlInjected.FieldOffset(Offset = "0x60")]
		[Token(Token = "0x400451B")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E394", Offset = "0x12E394")]
		private TextFieldValueChange[] m_ValueChanges;

		// Token: 0x0400627B RID: 25211
		[Cpp2IlInjected.FieldOffset(Offset = "0x68")]
		[Token(Token = "0x400451C")]
		[SerializeField]
		[Attribute(Name = "TooltipAttribute", RVA = "0x10012E3E0", Offset = "0x12E3E0")]
		private Text m_BuffConditionText;
	}
}
