﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Cpp2IlInjected;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02001049 RID: 4169
	[Token(Token = "0x2000AD5")]
	[StructLayout(3)]
	public class TownObjSelectWindow : UIGroup
	{
		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06004FDD RID: 20445 RVA: 0x0001BCF0 File Offset: 0x00019EF0
		[Token(Token = "0x1700050E")]
		public TownObjSelectWindow.eButton SelectButton
		{
			[Token(Token = "0x6004961")]
			[Address(RVA = "0x1015A3B04", Offset = "0x15A3B04", VA = "0x1015A3B04")]
			get
			{
				return TownObjSelectWindow.eButton.None;
			}
		}

		// Token: 0x14000106 RID: 262
		// (add) Token: 0x06004FDE RID: 20446 RVA: 0x00002050 File Offset: 0x00000250
		// (remove) Token: 0x06004FDF RID: 20447 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x14000106")]
		public event Action OnComplete
		{
			[Token(Token = "0x6004962")]
			[Address(RVA = "0x1015A3B0C", Offset = "0x15A3B0C", VA = "0x1015A3B0C")]
			[CompilerGenerated]
			add
			{
			}
			[Token(Token = "0x6004963")]
			[Address(RVA = "0x1015A3C1C", Offset = "0x15A3C1C", VA = "0x1015A3C1C")]
			[CompilerGenerated]
			remove
			{
			}
		}

		// Token: 0x06004FE0 RID: 20448 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004964")]
		[Address(RVA = "0x1015A3D2C", Offset = "0x15A3D2C", VA = "0x1015A3D2C")]
		public void Open(int buildPointIdx)
		{
		}

		// Token: 0x06004FE1 RID: 20449 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004965")]
		[Address(RVA = "0x1015A4100", Offset = "0x15A4100", VA = "0x1015A4100")]
		private void OpenRoom()
		{
		}

		// Token: 0x06004FE2 RID: 20450 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004966")]
		[Address(RVA = "0x1015A458C", Offset = "0x15A458C", VA = "0x1015A458C")]
		private void OpenBuff()
		{
		}

		// Token: 0x06004FE3 RID: 20451 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004967")]
		[Address(RVA = "0x1015A4294", Offset = "0x15A4294", VA = "0x1015A4294")]
		private void OpenContents(eTitleType titleType)
		{
		}

		// Token: 0x06004FE4 RID: 20452 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004968")]
		[Address(RVA = "0x1015A4910", Offset = "0x15A4910", VA = "0x1015A4910")]
		private void OpenMenu()
		{
		}

		// Token: 0x06004FE5 RID: 20453 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004969")]
		[Address(RVA = "0x1015A4AA8", Offset = "0x15A4AA8", VA = "0x1015A4AA8")]
		private void SetUpBuildLevelButton()
		{
		}

		// Token: 0x06004FE6 RID: 20454 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496A")]
		[Address(RVA = "0x1015A4CB4", Offset = "0x15A4CB4", VA = "0x1015A4CB4", Slot = "7")]
		public override void Close()
		{
		}

		// Token: 0x06004FE7 RID: 20455 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496B")]
		[Address(RVA = "0x1015A4CC8", Offset = "0x15A4CC8", VA = "0x1015A4CC8", Slot = "15")]
		public override void OnFinishFinalize()
		{
		}

		// Token: 0x06004FE8 RID: 20456 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496C")]
		[Address(RVA = "0x1015A4D08", Offset = "0x15A4D08", VA = "0x1015A4D08", Slot = "9")]
		public override void Update()
		{
		}

		// Token: 0x06004FE9 RID: 20457 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496D")]
		[Address(RVA = "0x1015A4E14", Offset = "0x15A4E14", VA = "0x1015A4E14")]
		public void OnClickRoomInButtonCallBack()
		{
		}

		// Token: 0x06004FEA RID: 20458 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496E")]
		[Address(RVA = "0x1015A4E20", Offset = "0x15A4E20", VA = "0x1015A4E20")]
		public void OnClickScheduleButtonCallBack()
		{
		}

		// Token: 0x06004FEB RID: 20459 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x600496F")]
		[Address(RVA = "0x1015A4E2C", Offset = "0x15A4E2C", VA = "0x1015A4E2C")]
		public void OnClickInfoButtonCallBack()
		{
		}

		// Token: 0x06004FEC RID: 20460 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004970")]
		[Address(RVA = "0x1015A4E38", Offset = "0x15A4E38", VA = "0x1015A4E38")]
		public void OnClickLevelUpButtonCallBack()
		{
		}

		// Token: 0x06004FED RID: 20461 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004971")]
		[Address(RVA = "0x1015A4E44", Offset = "0x15A4E44", VA = "0x1015A4E44")]
		public void OnClickQuickButtonCallBack()
		{
		}

		// Token: 0x06004FEE RID: 20462 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004972")]
		[Address(RVA = "0x1015A4E50", Offset = "0x15A4E50", VA = "0x1015A4E50")]
		public void OnClickStoreButtonCallBack()
		{
		}

		// Token: 0x06004FEF RID: 20463 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004973")]
		[Address(RVA = "0x1015A4E5C", Offset = "0x15A4E5C", VA = "0x1015A4E5C")]
		public void OnClickMoveButtonCallBack()
		{
		}

		// Token: 0x06004FF0 RID: 20464 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004974")]
		[Address(RVA = "0x1015A4E68", Offset = "0x15A4E68", VA = "0x1015A4E68")]
		public void OnClickSwapButtonCallBack()
		{
		}

		// Token: 0x06004FF1 RID: 20465 RVA: 0x00002050 File Offset: 0x00000250
		[Token(Token = "0x6004975")]
		[Address(RVA = "0x1015A4E74", Offset = "0x15A4E74", VA = "0x1015A4E74")]
		public TownObjSelectWindow()
		{
		}

		// Token: 0x0400627C RID: 25212
		[Cpp2IlInjected.FieldOffset(Offset = "0x88")]
		[Token(Token = "0x400451D")]
		private TownObjSelectWindow.eMode m_Mode;

		// Token: 0x0400627D RID: 25213
		[Cpp2IlInjected.FieldOffset(Offset = "0x8C")]
		[Token(Token = "0x400451E")]
		private TownObjSelectWindow.eButton m_SelectButton;

		// Token: 0x0400627E RID: 25214
		[Cpp2IlInjected.FieldOffset(Offset = "0x90")]
		[Token(Token = "0x400451F")]
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x0400627F RID: 25215
		[Cpp2IlInjected.FieldOffset(Offset = "0x98")]
		[Token(Token = "0x4004520")]
		[SerializeField]
		private GameObject m_Room;

		// Token: 0x04006280 RID: 25216
		[Cpp2IlInjected.FieldOffset(Offset = "0xA0")]
		[Token(Token = "0x4004521")]
		[SerializeField]
		private GameObject m_Schedule;

		// Token: 0x04006281 RID: 25217
		[Cpp2IlInjected.FieldOffset(Offset = "0xA8")]
		[Token(Token = "0x4004522")]
		[SerializeField]
		private GameObject m_Info;

		// Token: 0x04006282 RID: 25218
		[Cpp2IlInjected.FieldOffset(Offset = "0xB0")]
		[Token(Token = "0x4004523")]
		[SerializeField]
		private GameObject m_LvUp;

		// Token: 0x04006283 RID: 25219
		[Cpp2IlInjected.FieldOffset(Offset = "0xB8")]
		[Token(Token = "0x4004524")]
		[SerializeField]
		private GameObject m_Quick;

		// Token: 0x04006284 RID: 25220
		[Cpp2IlInjected.FieldOffset(Offset = "0xC0")]
		[Token(Token = "0x4004525")]
		[SerializeField]
		private GameObject m_Store;

		// Token: 0x04006285 RID: 25221
		[Cpp2IlInjected.FieldOffset(Offset = "0xC8")]
		[Token(Token = "0x4004526")]
		[SerializeField]
		private GameObject m_Move;

		// Token: 0x04006286 RID: 25222
		[Cpp2IlInjected.FieldOffset(Offset = "0xD0")]
		[Token(Token = "0x4004527")]
		[SerializeField]
		private GameObject m_Swap;

		// Token: 0x04006287 RID: 25223
		[Cpp2IlInjected.FieldOffset(Offset = "0xD8")]
		[Token(Token = "0x4004528")]
		[SerializeField]
		private CustomButton m_LevelUpButton;

		// Token: 0x04006288 RID: 25224
		[Cpp2IlInjected.FieldOffset(Offset = "0xE0")]
		[Token(Token = "0x4004529")]
		[SerializeField]
		private CustomButton m_BuffStoreButton;

		// Token: 0x04006289 RID: 25225
		[Cpp2IlInjected.FieldOffset(Offset = "0xE8")]
		[Token(Token = "0x400452A")]
		[SerializeField]
		private CustomButton m_BuffMoveButton;

		// Token: 0x0400628A RID: 25226
		[Cpp2IlInjected.FieldOffset(Offset = "0xF0")]
		[Token(Token = "0x400452B")]
		[SerializeField]
		private CustomButton m_RoomInButton;

		// Token: 0x0400628B RID: 25227
		[Cpp2IlInjected.FieldOffset(Offset = "0xF8")]
		[Token(Token = "0x400452C")]
		private int m_SelectBuildPointIdx;

		// Token: 0x0400628C RID: 25228
		[Cpp2IlInjected.FieldOffset(Offset = "0xFC")]
		[Token(Token = "0x400452D")]
		private int m_ReservePointIdx;

		// Token: 0x0200104A RID: 4170
		[Token(Token = "0x2001236")]
		private enum eMode
		{
			// Token: 0x0400628F RID: 25231
			[Token(Token = "0x4007097")]
			Room,
			// Token: 0x04006290 RID: 25232
			[Token(Token = "0x4007098")]
			BuffClose,
			// Token: 0x04006291 RID: 25233
			[Token(Token = "0x4007099")]
			BuffOpen,
			// Token: 0x04006292 RID: 25234
			[Token(Token = "0x400709A")]
			Contents
		}

		// Token: 0x0200104B RID: 4171
		[Token(Token = "0x2001237")]
		public enum eButton
		{
			// Token: 0x04006294 RID: 25236
			[Token(Token = "0x400709C")]
			None,
			// Token: 0x04006295 RID: 25237
			[Token(Token = "0x400709D")]
			RoomIn,
			// Token: 0x04006296 RID: 25238
			[Token(Token = "0x400709E")]
			Schedule,
			// Token: 0x04006297 RID: 25239
			[Token(Token = "0x400709F")]
			Info,
			// Token: 0x04006298 RID: 25240
			[Token(Token = "0x40070A0")]
			LevelUp,
			// Token: 0x04006299 RID: 25241
			[Token(Token = "0x40070A1")]
			Quick,
			// Token: 0x0400629A RID: 25242
			[Token(Token = "0x40070A2")]
			Store,
			// Token: 0x0400629B RID: 25243
			[Token(Token = "0x40070A3")]
			Move,
			// Token: 0x0400629C RID: 25244
			[Token(Token = "0x40070A4")]
			Swap
		}
	}
}
